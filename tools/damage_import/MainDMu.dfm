object MainDM: TMainDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 192
  Top = 109
  Height = 292
  Width = 420
  object SourceConn: TADOConnection
    ConnectionTimeout = 10
    KeepConnection = False
    LoginPrompt = False
    Left = 32
    Top = 8
  end
  object QMConn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=TestDB;Data Source=LESH'
    ConnectionTimeout = 10
    KeepConnection = False
    Provider = 'SQLOLEDB.1'
    Left = 278
    Top = 8
  end
  object SourceDamage: TADOQuery
    Connection = SourceConn
    Parameters = <
      item
        Name = 'loDamageID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'hiDamageID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select damage.*, member.*, memcode.*, facility.*, fac_size.*, ma' +
        'terial.*, dmg_code.*,'
      
        '       dmg_rsn.*, equiptyp.*, invest_status.*, cntrctr.contracto' +
        'rname, excnequip.excavation_equipment_desc'
      'from adls.damage damage'
      
        'left join adls.member member on (member.memberid = damage.member' +
        'id)'
      
        'left join adls.memcode memcode on (memcode.membercode = damage.m' +
        'embercode)'
      
        'left join adls.facility facility on (facility.facilitytypeid = d' +
        'amage.facilitytypeid)'
      
        'left join adls.fac_size fac_size on (fac_size.facilitysizeid = d' +
        'amage.facilitysizeid)'
      
        'left join adls.material material on (material.materialtypeid = d' +
        'amage.materialtypeid)'
      
        'left join adls.dmg_code dmg_code on (dmg_code.damagecodeid = dam' +
        'age.damagecodeid)'
      
        'left join adls.dmg_rsn dmg_rsn on (dmg_rsn.damagereasonid = dama' +
        'ge.damagereasonid)'
      
        'left join adls.equiptyp equiptyp on (equiptyp.equiptypeid = dama' +
        'ge.equiptypeid)'
      
        'left join adls.invest_status invest_status on (invest_status.inv' +
        'estigation_status_id = damage.investigation_status_id)'
      
        'left join adls.cntrctr cntrctr on (cntrctr.contractorid = damage' +
        '.contractorid)'
      
        'left join adls.exca_equip excnequip on (excnequip.excavation_equ' +
        'ipment_id = damage.excavation_equipment_id)'
      'where damage.damageid between :loDamageID and :hiDamageID'
      'order by damage.damageid')
    Left = 32
    Top = 64
  end
  object LastIdentity: TADOQuery
    Connection = QMConn
    Parameters = <>
    SQL.Strings = (
      '--SELECT MAX(damage_id) "LastID" FROM damage'
      'select @@identity "LastID"')
    Left = 248
    Top = 128
  end
  object GenericCommand: TADOQuery
    Connection = QMConn
    ExecuteOptions = [eoExecuteNoRecords]
    Parameters = <>
    Left = 320
    Top = 128
  end
  object SourceDamageNote: TADOQuery
    Connection = SourceConn
    Parameters = <
      item
        Name = 'damageid'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select * from adls.damageremarks'
      'where damageid = :damageid'
      'order by type, seqno')
    Left = 40
    Top = 120
  end
  object GenericDataset: TADODataSet
    Connection = QMConn
    Parameters = <>
    Left = 248
    Top = 192
  end
  object SourceTicket: TADOQuery
    Connection = SourceConn
    Parameters = <
      item
        Name = 'jobid'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select job.* from adls.job job'
      'where job.jobid = :jobid')
    Left = 32
    Top = 184
  end
  object QMDamage: TADOQuery
    Connection = QMConn
    ExecuteOptions = [eoExecuteNoRecords]
    Parameters = <
      item
        Name = 'profit_center'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'office_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'damage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'damage_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'uq_notified_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'notified_by_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_phone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_claim_id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'size_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'utility_co_damaged'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'remarks'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'investigator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'investigator_departure'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'investigator_narrative'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'excavator_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'excavator_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'excavation_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'locate_marks_present'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'locate_requested'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'site_hand_dig'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_mechanized_equip'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_boring'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_grading'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_open_trench'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'disc_repairs_were'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_exc_were'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_other1_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other1_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other1_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_other2_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other2_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other2_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'exc_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'exc_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'exc_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'exc_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'exc_resp_response'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'uq_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'uq_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'spc_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'spc_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'spc_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'spc_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'modified_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'closed_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_ess_step'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'facility_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'locator_experience'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'locate_equipment'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'was_project'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'claim_status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'claim_status_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'invoice_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'added_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'accrual_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'utilistar_id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO damage (profit_center, office_id, damage_type, damag' +
        'e_date, due_date, uq_notified_date, notified_by_person, notified' +
        '_by_company, notified_by_phone, client_code, client_id, client_c' +
        'laim_id, size_type,'
      
        'location, city, county, state, utility_co_damaged, remarks, inve' +
        'stigator_id, investigator_departure, investigator_narrative, exc' +
        'avator_company, excavator_type, excavation_type, locate_marks_pr' +
        'esent,'
      
        'locate_requested, ticket_id, site_hand_dig, site_mechanized_equi' +
        'p, site_exc_boring, site_exc_grading, site_exc_open_trench, disc' +
        '_repairs_were, disc_repair_person,'
      
        'disc_repair_contact, disc_repair_comment, disc_exc_were, disc_ex' +
        'c_person, disc_exc_contact, disc_exc_comment, disc_other1_person' +
        ', disc_other1_contact, disc_other1_comment,'
      
        'disc_other2_person, disc_other2_contact, disc_other2_comment, ex' +
        'c_resp_code, exc_resp_type, exc_resp_other_desc, exc_resp_detail' +
        's, exc_resp_response, uq_resp_code, uq_resp_type,'
      
        'uq_resp_other_desc, uq_resp_details, spc_resp_code, spc_resp_typ' +
        'e, spc_resp_other_desc, spc_resp_details, modified_date, closed_' +
        'date, modified_by, uq_resp_ess_step, facility_type,'
      
        'facility_size, locator_experience, locate_equipment, was_project' +
        ', claim_status, claim_status_date, invoice_code, facility_materi' +
        'al, added_by, accrual_date, utilistar_id)'
      
        'VALUES (:profit_center, :office_id, :damage_type, :damage_date, ' +
        ':due_date, :uq_notified_date, :notified_by_person, :notified_by_' +
        'company, :notified_by_phone, :client_code, :client_id, :client_c' +
        'laim_id, :size_type,'
      
        ':location, :city, :county, :state, :utility_co_damaged, :remarks' +
        ', :investigator_id, :investigator_departure, :investigator_narra' +
        'tive, :excavator_company, :excavator_type, :excavation_type, :lo' +
        'cate_marks_present,'
      
        ':locate_requested, :ticket_id, :site_hand_dig, :site_mechanized_' +
        'equip, :site_exc_boring, :site_exc_grading, :site_exc_open_trenc' +
        'h, :disc_repairs_were, :disc_repair_person, '
      
        ':disc_repair_contact, :disc_repair_comment, :disc_exc_were, :dis' +
        'c_exc_person, :disc_exc_contact, :disc_exc_comment, :disc_other1' +
        '_person, :disc_other1_contact, :disc_other1_comment,'
      
        ':disc_other2_person, :disc_other2_contact, :disc_other2_comment,' +
        ' :exc_resp_code, :exc_resp_type, :exc_resp_other_desc, :exc_resp' +
        '_details, :exc_resp_response, :uq_resp_code, :uq_resp_type,'
      
        ':uq_resp_other_desc, :uq_resp_details, :spc_resp_code, :spc_resp' +
        '_type, :spc_resp_other_desc, :spc_resp_details, :modified_date, ' +
        ':closed_date, :modified_by, :uq_resp_ess_step, :facility_type,'
      
        ':facility_size, :locator_experience, :locate_equipment, :was_pro' +
        'ject, :claim_status, :claim_status_date, :invoice_code, :facilit' +
        'y_material, :added_by, :accrual_date, :utilistar_id)')
    Left = 246
    Top = 64
  end
  object QMTicket: TADOQuery
    Connection = QMConn
    ExecuteOptions = [eoExecuteNoRecords]
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'parsed_ok'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'active'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'kind'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'map_page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'revision'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'transmit_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3500
        Value = Null
      end
      item
        Name = 'work_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'work_county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_address_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'work_address_number_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'work_address_street'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'work_cross'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'work_subdivision'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'work_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 90
        Value = Null
      end
      item
        Name = 'work_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_notc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_remarks'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1200
        Value = Null
      end
      item
        Name = 'priority'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'legal_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'legal_good_thru'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'legal_restake'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'respond_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'duration'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'con_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'con_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'con_zip'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'call_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'caller'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'caller_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'caller_phone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_cellular'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_fax'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_altcontact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_altphone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_email'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'operator'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'channel'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_lat'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'work_long'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'modified_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'ticket_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 38
        Value = Null
      end
      item
        Name = 'legal_due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'parent_ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'do_not_mark_before'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'route_area_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'watch_and_protect'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'service_area_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'explosives'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'serial_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'map_ref'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'followup_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'do_not_respond_before'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'recv_manager_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ward'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'image'
        Attributes = [paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO ticket(ticket_number, parsed_ok, [active], ticket_fo' +
        'rmat, kind, status, map_page, revision, transmit_date, due_date,' +
        ' work_description, work_state, work_county, work_city, work_addr' +
        'ess_number, work_address_number_2, work_address_street, work_cro' +
        'ss, work_subdivision, work_type, work_date, work_notc, work_rema' +
        'rks, priority, legal_date, legal_good_thru, legal_restake, respo' +
        'nd_date, duration, company, con_type, con_name, con_address, con' +
        '_city, con_state, con_zip, call_date, caller, caller_contact, ca' +
        'ller_phone, caller_cellular, caller_fax, caller_altcontact, call' +
        'er_altphone, caller_email, operator, channel, work_lat, work_lon' +
        'g, modified_date, ticket_type, legal_due_date, parent_ticket_id,' +
        ' do_not_mark_before, route_area_id, watch_and_protect, service_a' +
        'rea_code, explosives, serial_number, map_ref, followup_type_id, ' +
        'do_not_respond_before, recv_manager_id, ward, image)'
      
        'VALUES(:ticket_number, :parsed_ok, :active, :ticket_format, :kin' +
        'd, :status, :map_page, :revision, :transmit_date, :due_date, :wo' +
        'rk_description, :work_state, :work_county, :work_city, :work_add' +
        'ress_number, :work_address_number_2, :work_address_street, :work' +
        '_cross, :work_subdivision, :work_type, :work_date, :work_notc, :' +
        'work_remarks, :priority, :legal_date, :legal_good_thru, :legal_r' +
        'estake, :respond_date, :duration, :company, :con_type, :con_name' +
        ', :con_address, :con_city, :con_state, :con_zip, :call_date, :ca' +
        'ller, :caller_contact, :caller_phone, :caller_cellular, :caller_' +
        'fax, :caller_altcontact, :caller_altphone, :caller_email, :opera' +
        'tor, :channel, :work_lat, :work_long, :modified_date, :ticket_ty' +
        'pe, :legal_due_date, :parent_ticket_id, :do_not_mark_before, :ro' +
        'ute_area_id, :watch_and_protect, :service_area_code, :explosives' +
        ', :serial_number, :map_ref, :followup_type_id, :do_not_respond_b' +
        'efore, :recv_manager_id, :ward, :image)')
    Left = 320
    Top = 64
  end
end
