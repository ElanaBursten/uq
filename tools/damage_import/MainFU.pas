unit MainFU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    LogMemo: TMemo;
    Label2: TLabel;
    StartingDamageID: TEdit;
    EndingDamageID: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    SourceServerName: TLabel;
    SourceDBName: TLabel;
    QMServerName: TLabel;
    QMDBName: TLabel;
    ImportDamages: TButton;
    StatusBar: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure ImportDamagesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure InitializeImport;
    procedure LogDBConfigInfo;
  end;

  procedure LogMessage(const Msg: string;  const TimeStamp: Boolean = False);

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
uses MainDMu;

procedure TMainForm.FormShow(Sender: TObject);
begin
  InitializeImport;
end;

procedure TMainForm.InitializeImport;
begin
  Assert(Assigned(MainDM));
  StatusBar.Panels[0].Text := 'Ver. ' + VersionID;
  SourceServerName.Caption := 'Server: ' + MainDM.SourceServerName;
  SourceDBName.Caption := 'Database: ' + MainDM.SourceDBName;
  QMServerName.Caption := 'Server: ' + MainDM.QMServerName;
  QMDBName.Caption := 'Database: ' + MainDM.QMDBName;
  if ParamCount = 2 then
  begin
    StartingDamageID.Text := ParamStr(1);
    EndingDamageID.Text := ParamStr(2);
  end;
end;

procedure TMainForm.ImportDamagesClick(Sender: TObject);
var
  StartingID: Integer;
  EndingID: Integer;
begin
  ImportDamages.Enabled := False;
  try
    LogMemo.Lines.Clear;
    MainDM.ValidateDataMappings;
    StartingID := StrToIntDef(StartingDamageID.Text, -1);
    EndingID := StrToIntDef(EndingDamageID.Text, -1);
    if StartingID < 0 then
      raise Exception.Create('The Starting Damage ID must be a number greater than or equal 0');
    if EndingID < 0 then
      raise Exception.Create('The Ending Damage ID must be a number greater than or equal 0');

    LogDBConfigInfo;
    LogMessage(Format('Requested damage ID range: %d thru %d', [StartingID, EndingID]), True);
    MainDM.ImportDamageData(StartingID, EndingID);
  finally
    if LogMemo.Lines.Count > 0 then
      LogMemo.Lines.SaveToFile('DamageImport-' + FormatDateTime('yyyy-mm-dd-hhnnss', Now) + '.txt');
    ImportDamages.Enabled := True;
  end;
end;

procedure TMainForm.LogDBConfigInfo;
begin
  LogMessage('Source: ' + MainDM.SourceServerName + '/' + MainDM.SourceDBName);
  LogMessage('Destination: ' + MainDM.QMServerName + '/' + MainDM.QMDBName);
end;

procedure LogMessage(const Msg: string; const TimeStamp: Boolean);
begin
  Assert(Assigned(MainForm));
  if TimeStamp then
    MainForm.LogMemo.Lines.Add(FormatDateTime('mm-dd-yy hh:nn ', Now)  + Msg)
  else
    MainForm.LogMemo.Lines.Add(Msg);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  MainDM := TMainDM.Create(Self);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(MainDM);
end;

end.
