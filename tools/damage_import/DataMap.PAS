unit DataMap;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, JCLStrings;

type
  TDataMapItem = class (TObject)
  public
    QMFieldName: string;
    QMTableName: string;
    SourceFieldName: string;
    SourceTableName: string;
    constructor Create;
    destructor Destroy; override;
  end;

  TDataMap = class (TObject)
  private
    FDataMapItems: TStringList;
    procedure LoadDataMapFile(FileName: string);
    procedure ClearDataMap;
  protected
    function GetDataMapItemCount: Integer;
    function GetDataMapItems(Index: Integer): TDataMapItem;
  public
    constructor Create; overload;
    constructor Create(FileName: string); overload;
    destructor Destroy; override;
    function AddDataMapItem(const SourceTable, SourceField, QMTable, QMField:
            string): Integer;
    function QMFieldName(const SourceTableName, SourceFieldName: string):
            string;
    property DataMapItemCount: Integer read GetDataMapItemCount;
    property DataMapItems[Index: Integer]: TDataMapItem read GetDataMapItems;
  end;


implementation

{
********************************* TDataMapItem *********************************
}
constructor TDataMapItem.Create;
begin
  inherited Create;
end;

destructor TDataMapItem.Destroy;
begin
  inherited Destroy;
end;

{
*********************************** TDataMap ***********************************
}
constructor TDataMap.Create;
begin
  inherited Create;
  FDataMapItems := TStringList.Create;
  FDataMapItems.Sorted := True;
end;

constructor TDataMap.Create(FileName: string);
begin
  Create;
  if FileName <> '' then
    LoadDataMapFile(FileName);
end;

destructor TDataMap.Destroy;
begin
  ClearDataMap;
  FreeAndNil(FDataMapItems);
  inherited Destroy;
end;

function TDataMap.AddDataMapItem(const SourceTable, SourceField, QMTable,
        QMField: string): Integer;
var
  Item: TDataMapItem;
begin
  if (SourceTable = '') or (SourceField = '') or (QMTable = '') or (QMField = '') then begin
    Result := -1;
    Exit;
  end;

  Item := TDataMapItem.Create;
  Item.SourceTableName := SourceTable;
  Item.SourceFieldName := SourceField;
  Item.QMTableName := QMTable;
  Item.QMFieldName := QMField;
  Result := FDataMapItems.AddObject(Item.SourceTableName + '.' + Item.SourceFieldName, Item)
end;

function TDataMap.GetDataMapItemCount: Integer;
begin
  Result := FDataMapItems.Count;
end;

function TDataMap.GetDataMapItems(Index: Integer): TDataMapItem;
begin
  Result := TDataMapItem(FDataMapItems.Objects[Index]);
end;

procedure TDataMap.LoadDataMapFile(FileName: string);
var
  RecordList: TStringList;
  FieldList: TStringList;
  RecordIndex: Integer;
begin
  RecordList := TStringList.Create;
  try
    RecordList.LoadFromFile(FileName);
    FieldList := TStringList.Create;
    try
      for RecordIndex := 0 to RecordList.Count - 1 do begin
        StrToStrings(RecordList.Strings[RecordIndex], #09, FieldList, True);
        if FieldList.Count >= 4 then
          AddDataMapItem(FieldList.Strings[0], Fieldlist.Strings[1], FieldList.Strings[2], FieldList.Strings[3]);
      end;
    finally
      FreeAndNil(FieldList);
    end;
  finally
    FreeAndNil(RecordList);
  end;
end;

function TDataMap.QMFieldName(const SourceTableName, SourceFieldName: string):
        string;
var
  Index: Integer;
begin
  if FDataMapItems.Find(Trim(SourceTableName) + '.' + Trim(SourceFieldName), Index) then
    Result := DataMapItems[Index].QMTableName + '.' + DataMapItems[Index].QMFieldName
  else
   Result := '';
end;

procedure TDataMap.ClearDataMap;
var
  Index: Integer;
begin
  Assert(Assigned(FDataMapItems));
  for Index := 0 to DataMapItemCount - 1 do
    FDataMapItems.Objects[Index].Free;

  FDataMapItems.Clear;
end;

end.
