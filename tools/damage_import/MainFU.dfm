object MainForm: TMainForm
  Left = 536
  Top = 246
  Width = 640
  Height = 428
  Caption = 'Damage Import'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 174
    Height = 380
    Align = alLeft
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 8
      Width = 93
      Height = 13
      Caption = 'Starting Damage ID'
    end
    object Label2: TLabel
      Left = 6
      Top = 35
      Width = 90
      Height = 13
      Caption = 'Ending Damage ID'
    end
    object StartingDamageID: TEdit
      Left = 104
      Top = 4
      Width = 66
      Height = 21
      TabOrder = 0
    end
    object EndingDamageID: TEdit
      Left = 104
      Top = 31
      Width = 66
      Height = 21
      TabOrder = 1
    end
    object GroupBox1: TGroupBox
      Left = 5
      Top = 64
      Width = 164
      Height = 73
      Caption = ' Source Database '
      TabOrder = 2
      object SourceServerName: TLabel
        Left = 8
        Top = 20
        Width = 93
        Height = 13
        Caption = 'SourceServerName'
      end
      object SourceDBName: TLabel
        Left = 8
        Top = 40
        Width = 77
        Height = 13
        Caption = 'SourceDBName'
      end
    end
    object GroupBox2: TGroupBox
      Left = 5
      Top = 142
      Width = 164
      Height = 73
      Caption = ' QM Database '
      TabOrder = 3
      object QMServerName: TLabel
        Left = 10
        Top = 20
        Width = 76
        Height = 13
        Caption = 'QMServerName'
      end
      object QMDBName: TLabel
        Left = 10
        Top = 44
        Width = 60
        Height = 13
        Caption = 'QMDBName'
      end
    end
    object ImportDamages: TButton
      Left = 6
      Top = 222
      Width = 161
      Height = 25
      Caption = 'Import Damages'
      TabOrder = 4
      OnClick = ImportDamagesClick
    end
  end
  object Panel2: TPanel
    Left = 174
    Top = 0
    Width = 458
    Height = 380
    Align = alClient
    BevelOuter = bvLowered
    Caption = ' '
    TabOrder = 1
    object LogMemo: TMemo
      Left = 1
      Top = 1
      Width = 456
      Height = 378
      Align = alClient
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 380
    Width = 632
    Height = 19
    Panels = <
      item
        Width = 75
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
end
