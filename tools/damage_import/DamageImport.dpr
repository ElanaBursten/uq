program DamageImport;

uses
  Forms,
  MainFU in 'MainFU.pas' {MainForm},
  MainDMu in 'MainDMu.pas' {MainDM: TDataModule},
  DataMap in 'DataMap.PAS',
  CrossReference in 'CrossReference.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Damage Import';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
