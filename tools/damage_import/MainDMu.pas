unit MainDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, IniFiles, Dialogs, Forms, Variants, JclStrings,
  OdAdoUtils, OdHourglass, DataMap, CrossReference;

const
  VersionID = '0.0.0.9';

type
  TMainDM = class(TDataModule)
    SourceConn: TADOConnection;
    QMConn: TADOConnection;
    SourceDamage: TADOQuery;
    LastIdentity: TADOQuery;
    GenericCommand: TADOQuery;
    SourceDamageNote: TADOQuery;
    GenericDataset: TADODataSet;
    SourceTicket: TADOQuery;
    QMDamage: TADOQuery;
    QMTicket: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    QMDataMapping: TDataMap;
    CrossReference: TCrossReference;
    FirstQMDamageID: Integer;
    LastQMDamageID: Integer;
    procedure QMConnect;
    procedure SourceConnect;
    procedure LoadCrossReference;
    procedure LoadDataMap;
    function OpenDataset(const sql: string): TDataset;
    procedure ExecuteSQL(const sql: string);
    function CreateIniFile: TIniFile;
    function GetIniSetting(const Section, SettingName: string;
      Default: string): string;
    function IniFileName: string;
    procedure CopySourceDamageToQM;
    procedure AddDamageEstimate(const QMDamageID, AddedBy: Integer; const EstimateAmt: Currency; const EstimateDate: string);
    function GetLastID: Integer;
    function DamageRemarkType(const NoteType: string): Variant;
    function ExcavatorCompany: Variant;
    function InvestigationReportDate: Variant;
    function ModifiedDate: Variant;
    function ClientID(const ProfitCenter: string): Variant;
    procedure ClearParameterValues(Dataset: TADOQuery);
    procedure AddDamageInvoices(const QMDamageID, AddedBy: Integer);
    function DamageLocation: Variant;
    function HasText(const SearchForText, SearchInText: string): Boolean;
    function AddTicket: Integer;
    function IsFieldEmpty(Field: TField): Boolean;
    function EmpIDFromOperator(const OperatorID: Variant): Variant;
    function DamageDueDate: TDateTime;
    function SourceDamageID: Integer;
    function TicketDueDate: TDateTime;
    function OfficeID(const ProfitCenter: string): Variant;
    procedure CreateMissingReferenceCodes(const ReferenceType, FieldName: string);
    function LocateWasRequested: string;
  public
    SourceServerName: string;
    SourceDBName: string;
    QMServerName: string;
    QMDBName: string;
    DamageSourcePrefix: string;
    DefaultClientID: Integer;
    DefaultEmpID: Integer;
    procedure Initialize;
    procedure ValidateDataMappings;
    procedure ImportDamageData(const StartingDamageID, EndingDamageID: Integer);
  end;

var
  MainDM: TMainDM;

implementation

uses MainFU;


{$R *.dfm}
procedure TMainDM.Initialize;
begin
  LoadDataMap;
  LoadCrossReference;
  QMConnect;
  SourceConnect;
  DamageSourcePrefix := GetIniSetting('DamageImport', 'DamageSourcePrefix', 'KT');
  DefaultEmpID := StrToIntDef(GetIniSetting('DamageImport', 'DefaultEmployee', '3510'), 3510);
  DefaultClientID := StrToIntDef(GetIniSetting('DamageImport', 'DefaultClientID', '9999'), 9999);
end;

procedure TMainDM.LoadCrossReference;
var
  CrossReferenceFile: string;
begin
  CrossReferenceFile := GetINISetting('DamageImport', 'CrossReferenceFile', 'data\crossref.tsv');
  Assert(FileExists(CrossReferenceFile), 'Missing QM code transformation data (' + CrossReferenceFile + ')');
  CrossReference := TCrossReference.Create(CrossReferenceFile);
end;

procedure TMainDM.LoadDataMap;
var
  DataMapFile: string;
begin
  DataMapFile := GetINISetting('DamageImport', 'DataMapFile', 'data\datamap.tsv');
  Assert(FileExists(DataMapFile), 'Missing QM data map (' + DataMapFile + ')');
  QMDataMapping := TDataMap.Create(DataMapFile);
end;

procedure TMainDM.QMConnect;
begin
  QMServerName := GetIniSetting('QM Database', 'Server', '');
  QMDBName := GetIniSetting('QM Database', 'DB', '');
  QMConn.KeepConnection := True;
  QMConn.Connected := False;
  QMConn.ConnectionString := MakeConnectionStringFromInI(IniFileName, 'QM Database');
  QMConn.Open;
end;

procedure TMainDM.SourceConnect;
begin
  SourceServerName := GetIniSetting('Source Database', 'Server', '');
  SourceDBName := GetIniSetting('Source Database', 'DB', '');
  SourceConn.KeepConnection := True;
  SourceConn.Connected := False;
  SourceConn.ConnectionString := MakeConnectionStringFromInI(IniFileName, 'Source Database');
  SourceConn.Open;
end;

function TMainDM.CreateIniFile: TIniFile;
begin
  Result := TIniFile.Create(IniFileName);
end;

function TMainDM.IniFileName: string;
begin
  Result := ChangeFileExt(ParamStr(0), '.ini');
end;

function TMainDM.GetIniSetting(const Section, SettingName: string; Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := CreateIniFile;
  try
    Result := Ini.ReadString(Section, SettingName, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TMainDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(QMDataMapping);
end;

procedure TMainDM.DataModuleCreate(Sender: TObject);
begin
  Initialize;
end;

procedure TMainDM.ValidateDataMappings;
begin
  Assert(Assigned(QMDataMapping));
  Assert(Assigned(CrossReference));

  CreateMissingReferenceCodes('dmgco', 'utility_co_damaged');
  CreateMissingReferenceCodes('locequip', 'locate_eqiupment');
end;

procedure TMainDM.ImportDamageData(const StartingDamageID, EndingDamageID: Integer);
var
  Hourglass: IInterface;
  RecordCount: Integer;
  CreatedCount: Integer;
begin
  Assert(StartingDamageID <= EndingDamageID);
  LogMessage('Importing damage data...');
  Hourglass := ShowHourGlass;
  QMDamage.Prepared := True;
  QMTicket.Prepared := True;
  SourceDamage.Parameters.ParamValues['loDamageID'] := StartingDamageID;
  SourceDamage.Parameters.ParamValues['hiDamageID'] := EndingDamageID;
  SourceDamage.Open;
  try
    SourceDamage.First;
    RecordCount := 0;
    FirstQMDamageID := 0;
    LastQMDamageID := -1;
    while not SourceDamage.Eof do begin
      CopySourceDamageToQM;
      SourceDamage.Next;
      Inc(RecordCount);
      if RecordCount mod 50 = 0 then
        Application.ProcessMessages;
      if RecordCount mod 500 = 0 then
        LogMessage(IntToStr(RecordCount) + '...');
    end;
  finally
    SourceDamage.Close;
    QMDamage.Prepared := False;
    QMTicket.Prepared := False;
  end;
  CreatedCount := LastQMDamageID - FirstQMDamageID + 1;
  LogMessage(Format('Requested %d damages for import to QM.', [RecordCount]));
  LogMessage(Format('Created %d damages in QM.', [CreatedCount]));
  if CreatedCount > 0 then
    LogMessage(Format('Created QM Damage IDs %d thru %d', [FirstQMDamageID, LastQMDamageID]));
  if RecordCount = 0 then
    LogMessage('WARNING - No damage records were imported', True)
  else if CreatedCount <> RecordCount then
    LogMessage('WARNING - variance detected in damages requested vs. QM damages created', True)
  else
    LogMessage('Damage import completed ok.', True);
end;

procedure TMainDM.CopySourceDamageToQM;
var
  Index: Integer;
  SourceFld: string;
  SourceTbl: string;
  QMFld: string;
  Remarks: Variant;
  DamageCode: string;
  AddedByEmp: Variant;
  Investigator: Variant;
  ProfitCenter: string;
  TicketID: Integer;
begin
  SourceDamageNote.Parameters.ParamByName('damageid').Value := SourceDamageID;
  SourceDamageNote.Open;
  ClearParameterValues(QMDamage);
  try
    try
      // all the fields that are a straight copy
      for Index := 0 to QMDataMapping.DataMapItemCount - 1 do begin
        if SameText(QMDataMapping.DataMapItems[Index].QMTableName, 'damage') then begin
          SourceTbl := QMDataMapping.DataMapItems[Index].SourceTableName;
          SourceFld := QMDataMapping.DataMapItems[Index].SourceFieldName;
          QMFld := QMDataMapping.DataMapItems[Index].QMFieldName;
          if (QMFld <> '*') and (QMFld <> '') then begin
            if IsFieldEmpty(SourceDamage.FieldByName(SourceFld)) then
              QMDamage.Parameters.ParamByName(QMFld).Value := Null
            else begin
              if QMDamage.Parameters.ParamByName(QMFld).DataType = ftString then
                QMDamage.Parameters.ParamByName(QMFld).Value := Copy(Trim(SourceDamage.FieldByName(SourceFld).AsString), 1, QMDamage.Parameters.ParamByName(QMFld).Size)
              else
                QMDamage.Parameters.ParamByName(QMFld).Value := SourceDamage.FieldByName(SourceFld).Value;
            end;
          end;
        end;
      end;

      // special case fields
      DamageCode := Trim(SourceDamage.FieldByName('damagecode').AsString);
      if DamageCode = 'A' then begin      // UQ at fault
        QMDamage.Parameters.ParamByName('uq_resp_details').Value := DamageRemarkType('FAULTREM');
        QMDamage.Parameters.ParamByName('uq_resp_code').Value := CrossReference.NewValue('uq_resp_code', SourceDamage.FieldByName('damagereasoncode').AsString);
      end
      else if DamageCode = 'B' then begin // Excavator at fault
        QMDamage.Parameters.ParamByName('exc_resp_details').Value := DamageRemarkType('FAULTREM');
        QMDamage.Parameters.ParamByName('exc_resp_code').Value := CrossReference.NewValue('exc_resp_code', SourceDamage.FieldByName('damagereasoncode').AsString);
      end
      else if DamageCode = 'C' then begin // Utility at fault
        QMDamage.Parameters.ParamByName('spc_resp_details').Value := DamageRemarkType('FAULTREM');
        QMDamage.Parameters.ParamByName('spc_resp_code').Value := CrossReference.NewValue('spc_resp_code', SourceDamage.FieldByName('damagereasoncode').AsString);
      end
      else if DamageCode <> '' then
        raise Exception.Create('Unexpected DamageCode value of ' + DamageCode);

      ProfitCenter := CrossReference.NewValue('profit_center', SourceDamage.FieldByName('regionid').AsString);

      QMDamage.Parameters.ParamByName('investigator_departure').Value := InvestigationReportDate;
      QMDamage.Parameters.ParamByName('modified_date').Value := ModifiedDate;
      QMDamage.Parameters.ParamByName('excavator_company').Value := ExcavatorCompany;
      QMDamage.Parameters.ParamByName('investigator_narrative').Value := DamageRemarkType('NARRATIVEREM');
      QMDamage.Parameters.ParamByName('disc_exc_comment').Value := DamageRemarkType('CONTRACTORREM');
      QMDamage.Parameters.ParamByName('client_id').Value := ClientID(ProfitCenter);
      QMDamage.Parameters.ParamByName('location').Value := DamageLocation;
      Investigator := EmpIDFromOperator(SourceDamage.FieldByName('invstgtrpayrollnbr').Value);
      AddedByEmp := EmpIDFromOperator(SourceDamage.FieldByName('operatorid').Value);
      if Investigator <> Null then
        QMDamage.Parameters.ParamByName('investigator_id').Value := Investigator;
      if AddedByEmp <> Null then
        QMDamage.Parameters.ParamByName('modified_by').Value := AddedByEmp;
      QMDamage.Parameters.ParamByName('due_date').Value := DamageDueDate;
      QMDamage.Parameters.ParamByName('office_id').Value := OfficeID(ProfitCenter);
      QMDamage.Parameters.ParamByName('locate_requested').Value := LocateWasRequested;

      QMDamage.Parameters.ParamByName('damage_type').Value := CrossReference.NewValue('damage_type', SourceDamage.FieldByName('investigation_status_id').AsString);
      QMDamage.Parameters.ParamByName('claim_status').Value := CrossReference.NewValue('claim_status', SourceDamage.FieldByName('pay_status_id').AsString);
      QMDamage.Parameters.ParamByName('invoice_code').Value := CrossReference.NewValue('invoice_code', SourceDamage.FieldByName('pay_status_id').AsString);
      QMDamage.Parameters.ParamByName('facility_type').Value := CrossReference.NewValue('facility_type', SourceDamage.FieldByName('facilitytypeid').AsString);
      QMDamage.Parameters.ParamByName('facility_material').Value := CrossReference.NewValue('facility_material', SourceDamage.FieldByName('materialtypeid').AsString);
      QMDamage.Parameters.ParamByName('facility_size').Value := CrossReference.NewValue('facility_size', SourceDamage.FieldByName('facilitysizeid').AsString);
      QMDamage.Parameters.ParamByName('locate_equipment').Value := CrossReference.NewValue('locate_equipment', SourceDamage.FieldByName('equiptypeid').AsString);
      QMDamage.Parameters.ParamByName('utility_co_damaged').Value := CrossReference.NewValue('utility_co_damaged', SourceDamage.FieldByName('memberid').AsString);

      QMDamage.Parameters.ParamByName('profit_center').Value := ProfitCenter;

      // null KorTerra pay_status_id and non-UQ fault default to NOPAY
      if (QMDamage.Parameters.ParamByName('invoice_code').Value = Null) and ((DamageCode = 'B') or (DamageCode = 'C')) then
        QMDamage.Parameters.ParamByName('invoice_code').Value := 'NOPAY';
      if (QMDamage.Parameters.ParamByName('claim_status').Value = Null) and ((DamageCode = 'B') or (DamageCode = 'C')) then
        QMDamage.Parameters.ParamByName('claim_status').Value := 'CLOWOPAY';

      if SourceDamage.FieldByName('dps_experience').AsInteger > 24 then
        QMDamage.Parameters.ParamByName('locator_experience').Value := '24+'
      else
        QMDamage.Parameters.ParamByName('locator_experience').Value := CrossReference.NewValue('locator_experience', SourceDamage.FieldByName('dps_experience').AsString);
      QMDamage.Parameters.ParamByName('site_exc_open_trench').Value := HasText('trench', SourceDamage.FieldByName('excavation_equipment_desc').AsString);
      QMDamage.Parameters.ParamByName('site_exc_boring').Value := HasText('bore', SourceDamage.FieldByName('excavation_equipment_desc').AsString);

      Remarks := DamageRemarkType('unknown');
      if Remarks = Null then
        Remarks := 'IMPORTED FROM KORTERRA DAMAGE ID ' + IntToStr(SourceDamageID)
      else
        Remarks := 'IMPORTED FROM KORTERRA DAMAGE ID ' + IntToStr(SourceDamageID) + AnsiLineBreak + Remarks;
      QMDamage.Parameters.ParamByName('remarks').Value := Remarks;

      QMDamage.Parameters.ParamByName('utilistar_id').Value := DamageSourcePrefix + IntToStr(SourceDamageID);

      if Trim(SourceDamage.FieldByName('ticketnbr').AsString) <> '' then begin
        TicketID := AddTicket;
        if TicketID > 0 then
          QMDamage.Parameters.ParamByName('ticket_id').Value := TicketID
        else begin
          Remarks := Remarks + AnsiLineBreak + 'REFERENCED TICKET (' + Trim(SourceDamage.FieldByName('ticketnbr').AsString) + ') IS NOT AVAILABLE.';
          QMDamage.Parameters.ParamByName('remarks').Value := Remarks;
        end;
      end;

      // Hardcoded defaults for fields that need a QM value but don't have a KorTerra value
      QMDamage.Parameters.ParamByName('notified_by_company').Value := 'N/A';
      QMDamage.Parameters.ParamByName('county').Value := 'N/A';
      QMDamage.Parameters.ParamByName('locate_marks_present').Value := 'U';

      QMDamage.ExecSQL;
      LastQMDamageID := GetLastID;
      if FirstQMDamageID < 1 then
        FirstQMDamageID := LastQMDamageID;

      if AddedByEmp = Null then
        AddedByEmp := Investigator;
      if AddedByEmp = Null then begin
        AddedByEmp := DefaultEmpID;
        LogMessage('No operatorid and no investigator; estimate added_by set to ' + IntToStr(DefaultEmpID));
      end;
      if SourceDamage.FieldByName('estcost').AsCurrency <> 0 then
        AddDamageEstimate(LastQMDamageID, AddedByEmp, SourceDamage.FieldByName('estcost').AsCurrency, ModifiedDate);

      AddDamageInvoices(LastQMDamageID, AddedByEmp);
    except
      on E: Exception do begin
        LogMessage('Damage NOT imported - Damage # ' + IntToStr(SourceDamageID) + ' ' + E.Message);
      end;
    end;
  finally
    SourceDamageNote.Close;
  end;
end;

procedure TMainDM.AddDamageEstimate(const QMDamageID, AddedBy: Integer; const EstimateAmt: Currency; const EstimateDate: string);
const
  sql = 'INSERT INTO damage_estimate (damage_id, amount, emp_id, modified_date) VALUES (%d, %f, %d, %s)';
begin
  try
    ExecuteSQL(Format(sql, [QMDamageID, EstimateAmt, AddedBy, EstimateDate]));
  except
    on E: Exception do begin
      LogMessage('Error importing damage estimate for QM Damage ID #' + IntToStr(QMDamageID) + ' ' + E.Message);
    end;
  end;
end;

procedure TMainDM.AddDamageInvoices(const QMDamageID, AddedBy: Integer);
const
  sql = 'INSERT INTO damage_invoice (damage_id, amount, invoice_num, received_date, paid_date, paid_amount, damage_city, damage_state, damage_date, invoice_code) ' +
    'SELECT damage_id, %f, %s, %s, %s, %s, city, state, damage_date, invoice_code FROM damage where damage_id = %d';
var
  Index: Integer;
  AmountFieldName, InvoiceFieldName, InvoiceDate, PaidDate, PaidAmt: string;
  ActualCostTotal: Currency;
begin
  ActualCostTotal := 0;
  if SourceDamage.FieldByName('invdtdate').IsNull then
    InvoiceDate := 'null'
  else
    InvoiceDate := QuotedStr(FormatDateTime('yyyy-mm-dd', SourceDamage.FieldByName('invdtdate').AsDateTime));
  if SourceDamage.FieldByName('pymntprocdt').IsNull then
    PaidDate := 'null'
  else
    PaidDate := QuotedStr(FormatDateTime('yyyy-mm-dd', SourceDamage.FieldByName('pymntprocdt').AsDateTime));
  // up to 5 invoices
  for Index := 1 to 5 do begin
    AmountFieldName := 'actualcost' + IntToStr(Index);
    if SourceDamage.FieldByName('pay_status_id').AsString = '7' then
      PaidAmt := FloatToStrF(SourceDamage.FieldByName(AmountFieldName).AsCurrency, ffFixed, 15, 2)
    else
      PaidAmt := 'null';
    if SourceDamage.FieldByName(AmountFieldName).AsCurrency <> 0 then begin
      InvoiceFieldName := 'actualcostdesc' + IntToStr(Index);
      try
        ExecuteSQL(Format(sql, [SourceDamage.FieldByName(AmountFieldName).AsCurrency,
          QuotedStr(Copy(SourceDamage.FieldByName(InvoiceFieldName).AsString, 1, 30)),
          InvoiceDate,
          PaidDate,
          PaidAmt,
          QMDamageID]));
        ActualCostTotal := ActualCostTotal + SourceDamage.FieldByName(AmountFieldName).AsCurrency;
      except
        on E: Exception do begin
          LogMessage('Error importing damage invoice for QM Damage ID #' + IntToStr(QMDamageID) + ' ' + E.Message);
        end;
      end;
    end;
  end;

  // create a new estimate using the current date that matches the total of the invoices stored
  try
    if ActualCostTotal <> 0 then
      AddDamageEstimate(QMDamageID, AddedBy, ActualCostTotal, 'getdate()');
  except
    on E: Exception do begin
      LogMessage('Error adding acutal cost estimate for QM Damage ID #' + IntToStr(QMDamageID) + ' ' + E.Message);
    end;
  end;

end;

function TMainDM.AddTicket: Integer;
var
  TicketNumber: string;
  Index: Integer;
  SourceTbl, SourceFld: string;
  QMFld: string;
begin
  Result := -1;
  TicketNumber := Trim(SourceDamage.FieldByName('ticketnbr').AsString);
  SourceTicket.Parameters.ParamByName('jobid').Value := TicketNumber;
  try
    SourceTicket.Open;
    if SourceTicket.IsEmpty then begin
      LogMessage('Damage # ' + IntToStr(SourceDamageID) + ' - ticket not in source database. Ticket # ' + TicketNumber);
      SourceTicket.Close;
      Exit;
    end;

    ClearParameterValues(QMTicket);
    for Index := 0 to QMDataMapping.DataMapItemCount - 1 do begin
      if SameText(QMDataMapping.DataMapItems[Index].QMTableName, 'ticket') then begin
        SourceTbl := QMDataMapping.DataMapItems[Index].SourceTableName;
        SourceFld := QMDataMapping.DataMapItems[Index].SourceFieldName;
        QMFld := QMDataMapping.DataMapItems[Index].QMFieldName;
        if (QMFld <> '*') and (QMFld <> '') then begin
          if IsFieldEmpty(SourceTicket.FieldByName(SourceFld)) then
            QMTicket.Parameters.ParamByName(QMFld).Value := Null
          else begin
            if QMTicket.Parameters.ParamByName(QMFld).DataType = ftString then
              QMTicket.Parameters.ParamByName(QMFld).Value := Copy(Trim(SourceTicket.FieldByName(SourceFld).AsString), 1, QMTicket.Parameters.ParamByName(QMFld).Size)
            else
              QMTicket.Parameters.ParamByName(QMFld).Value := SourceTicket.FieldByName(SourceFld).Value;
          end;
        end;
      end;
    end;

    if not SourceTicket.FieldByName('latestxmitdttime').IsNull then
      QMTicket.Parameters.ParamByName('transmit_date').Value := QMTicket.Parameters.ParamByName('transmit_date').Value + SourceTicket.FieldByName('latestxmitdttime').AsDateTime;
    if not SourceTicket.FieldByName('origdttime').IsNull then
      QMTicket.Parameters.ParamByName('call_date').Value := QMTicket.Parameters.ParamByName('call_date').Value + SourceTicket.FieldByName('origdttime').AsDateTime;
    if not SourceTicket.FieldByName('wtbdttime').IsNull then
      QMTicket.Parameters.ParamByName('work_date').Value := QMTicket.Parameters.ParamByName('work_date').Value + SourceTicket.FieldByName('wtbdttime').AsDateTime;

    QMTicket.Parameters.ParamByName('ticket_type').Value := SourceTicket.FieldByName('priority').AsString;
    QMTicket.Parameters.ParamByName('due_date').Value := TicketDueDate;
    QMTicket.Parameters.ParamByName('kind').Value := 'DONE';
    QMTicket.Parameters.ParamByName('status').Value := 'IMPORTED';
    QMTicket.Parameters.ParamByName('parsed_ok').Value := True;
    QMTicket.Parameters.ParamByName('active').Value := False;
    QMTicket.Parameters.ParamByName('revision').Value := '';
    QMTicket.Parameters.ParamByName('modified_date').Value := Now;
    QMTicket.Parameters.ParamByName('image').Value := 'IMPORTED TICKET - NO IMAGE AVAILABLE';
    QMTicket.ExecSQL;
    Result := GetLastID;
  finally
    SourceTicket.Close;
  end;
end;

procedure TMainDM.ClearParameterValues(Dataset: TADOQuery);
var
  Index: Integer;
begin
  Dataset.Close;
  for Index := 0 to Dataset.Parameters.Count - 1 do
    Dataset.Parameters.Items[Index].Value := Null;
end;

function TMainDM.GetLastID: Integer;
begin
  LastIdentity.Open;
  try
    Result := LastIdentity.FieldByName('LastID').AsInteger;
  finally
    LastIdentity.Close;
  end;
end;

function TMainDM.TicketDueDate: TDateTime;
begin
  if not SourceDamage.FieldByName('ticketduedtdate').IsNull then begin
    Result := SourceDamage.FieldByName('ticketduedtdate').Value;
    if not SourceDamage.FieldByName('ticketduedttime').IsNull then
      Result := Result + SourceDamage.FieldByName('ticketduedttime').Value;
  end
  else
    Result := QMTicket.Parameters.ParamByName('transmit_date').Value + 3;
end;

function TMainDM.InvestigationReportDate: Variant;
begin
  Result := Null;
  if not SourceDamage.FieldByName('invstgnreportdtdate').IsNull then
    if SourceDamage.FieldByName('invstgnreportdttime').IsNull then
      Result := SourceDamage.FieldByName('invstgnreportdtdate').AsDateTime
    else
      Result := SourceDamage.FieldByName('invstgnreportdtdate').AsDateTime + SourceDamage.FieldByName('invstgnreportdttime').AsDateTime;
end;

function TMainDM.DamageDueDate: TDateTime;
var
  CreateDateTime: TDateTime;
begin
  CreateDateTime := SourceDamage.FieldByName('creationdate').AsDateTime + SourceDamage.FieldByName('creationtime').AsDateTime;
  Result := CreateDateTime + 3; // always due 3 days (72 hours) from create date
end;

function TMainDM.ModifiedDate: Variant;
begin
  Result := Null;
  if SourceDamage.FieldByName('lastchangeddate').IsNull then begin
    if not SourceDamage.FieldByName('creationdate').IsNull then
      if SourceDamage.FieldByName('creationtime').IsNull then
        Result := SourceDamage.FieldByName('creationdate').AsDateTime
      else
        Result := SourceDamage.FieldByName('creationdate').AsDateTime + SourceDamage.FieldByName('creationtime').AsDateTime;
  end
  else begin
    if SourceDamage.FieldByName('lastchangedtime').IsNull then
      Result := SourceDamage.FieldByName('lastchangeddate').AsDateTime
    else
      Result := SourceDamage.FieldByName('lastchangeddate').AsDateTime + SourceDamage.FieldByName('lastchangedtime').AsDateTime;
  end
end;

function TMainDM.EmpIDFromOperator(const OperatorID: Variant): Variant;
const
  sql = 'select emp_id from employee where emp_number=%s';
var
  EmpID: string;
  NewEmp: Variant;
  DS: TDataset;
begin
  Result := Null;
  if (OperatorID = Null) then
    Exit;

  EmpID := Trim(OperatorID);
  if (EmpID = '') then
    Exit;

  if not StrConsistsOfNumberChars(EmpID) then begin
    NewEmp := CrossReference.NewValue('emp_id', EmpID);
    if NewEmp = Null then
      Exit
    else
      EmpId := NewEmp;
  end;

  if StrConsistsOfNumberChars(EmpID) then begin
    DS := OpenDataset(Format(sql, [QuotedStr(EmpID)]));
    try
      if not DS.IsEmpty then
        Result := DS.Fields[0].Value
      else
        LogMessage('Damage # ' + IntToStr(SourceDamageID) + ' - Employee number not in QM employee table - ' + EmpID + '. Stored in QM with Null');
    finally
      DS.Close;
    end;

  end
  else
    LogMessage('Damage # ' + IntToStr(SourceDamageID) + ' - Non-numeric employee encountered - ' + EmpID + '. Stored in QM with Null');
end;

function TMainDM.ExcavatorCompany: Variant;
begin
  if SourceDamage.FieldByName('contractorname').IsNull then
    Result := Null
  else
    Result := Copy(SourceDamage.FieldByName('contractorname').Value, 1, 30);
end;

function TMainDM.DamageRemarkType(const NoteType: string): Variant;
begin
  Result := Null;
  if NoteType = 'unknown' then
    SourceDamageNote.Filter := 'type<>''NARRATIVEREM'' and type<>''CONTRACTORREM'' and type <> ''FAULTREM'''
  else
    SourceDamageNote.Filter := 'type=' + QuotedStr(NoteType);
  SourceDamageNote.Filtered := True;
  try
    SourceDamageNote.First;
    if not SourceDamageNote.IsEmpty then begin
      Result := Trim(SourceDamageNote.FieldByName('text').AsString);
      SourceDamageNote.Next;
    end;
    while not SourceDamageNote.Eof do begin
      Result := Result + Trim(SourceDamageNote.FieldByName('text').AsString);
      SourceDamageNote.Next;
    end;
  finally
    SourceDamageNote.Filtered := False;
    SourceDamageNote.Filter := '';
  end;
end;

function TMainDM.ClientID(const ProfitCenter: string): Variant;
const
  sql = 'select c.client_id, o.profit_center from client c left join office o on (o.office_id = c.office_id)'
    + ' where c.oc_code=%s and c.active=1';
var
  ds: TDataset;
  msg: string;
begin
  Result := Null;
  if (SourceDamage.FieldByName('membercode').IsNull) or (Trim(SourceDamage.FieldByName('membercode').AsString) = '') then
    Exit;

  msg := '';
  ds := OpenDataset(Format(sql, [QuotedStr(Trim(SourceDamage.FieldByName('membercode').AsString))]));
  try
    if ds.IsEmpty then
      msg := Format('Damage # %d - Unknown member code %s', [SourceDamageID, SourceDamage.FieldByName('membercode').AsString])
    else if ds.RecordCount > 1 then begin
      while not ds.Eof do begin
        if ds.FieldByName('profit_center').AsString = ProfitCenter then begin
          Result := ds.FieldByName('client_id').AsInteger;
          Break;
        end;
        ds.Next;
      end;
      if Result = Null then
        msg := Format('Damage # %d - Unknown member code %s', [SourceDamageID, SourceDamage.FieldByName('membercode').AsString]);
    end
    else
      Result := ds.FieldByName('client_id').AsInteger;
    if msg <> '' then begin
      LogMessage(msg);
      Result := DefaultClientID;
    end;
  finally
    ds.Close;
  end;
end;

function TMainDM.DamageLocation: Variant;
begin
  Result := Trim(SourceDamage.FieldByName('address').AsString) + ' ' +
    Trim(SourceDamage.FieldByName('street').AsString) + ' ' +
    Trim(SourceDamage.FieldByName('city').AsString) + ', ' + Trim(SourceDamage.FieldByName('state').AsString);
  if Trim(SourceDamage.FieldByName('nearinter').AsString) <> '' then
    Result := Copy(Trim(Result + ' NEAR: ' + Trim(SourceDamage.FieldByName('nearinter').AsString)), 1, 80);
end;

procedure TMainDM.ExecuteSQL(const sql: string);
begin
  GenericCommand.ExecuteOptions := [eoExecuteNoRecords];
  GenericCommand.SQL.Text := sql;
  GenericCommand.ExecSQL;
  CheckForADOError(GenericCommand.Connection);
end;

function TMainDM.OpenDataset(const sql: string): TDataset;
begin
  GenericDataset.Close;
  GenericDataset.ExecuteOptions := [];
  GenericDataset.CommandText := sql;
  GenericDataset.Open;
  CheckForADOError(GenericDataset.Connection);
  result := GenericDataset;
end;

function TMainDM.HasText(const SearchForText, SearchInText: string): Boolean;
begin
  Result := Pos(Lowercase(SearchForText), Lowercase(SearchInText)) > 0;
end;

function TMainDM.IsFieldEmpty(Field: TField): Boolean;
begin
  if Field.IsNull then
    Result := True
  else if Trim(Field.AsString) = '' then
    Result := True
  else
    Result := False;
end;

function TMainDM.SourceDamageID: Integer;
begin
  Assert(Assigned(SourceDamage) and SourceDamage.Active);
  Result := SourceDamage.FieldByName('damageid').AsInteger;
end;

procedure TMainDM.CreateMissingReferenceCodes(const ReferenceType, FieldName: string);
const
  SelectSQL = 'SELECT ref_id FROM reference WHERE type=''%s''';
  InsertSQL = 'INSERT INTO reference (type, description, code) values (''%s'', ''%s'', %s)';
var
  Index: Integer;
  DS: TDataset;
  ReferenceCode: string;
  SQL: string;
begin
  for Index := 0 to CrossReference.ItemCount - 1 do begin
    if SameText(CrossReference.Items[Index].FieldName, FieldName) then begin
      ReferenceCode := CrossReference.Items[Index].NewValue;
      if ReferenceType = 'dmgco' then
        SQL := SelectSQL + ' AND description=''%s'''
      else
        SQL := SelectSQL + ' AND code=''%s''';
      DS := OpenDataset(Format(SQL, [ReferenceType, ReferenceCode]));
      try
        if DS.IsEmpty then
          if ReferenceType = 'dmgco' then
            ExecuteSQL(Format(InsertSQL, [ReferenceType, ReferenceCode, 'null']))
          else
            ExecuteSQL(Format(InsertSQL, [ReferenceType, ReferenceCode, QuotedStr(Copy(ReferenceCode, 1, 15))]));
      finally
        DS.Close;
      end;
    end;
  end;
end;

function TMainDM.OfficeID(const ProfitCenter: string): Variant;
const
  sql = 'select office_id from office where profit_center=%s';
var
  ds: TDataset;
  msg: string;
begin
  Result := Null;
  msg := '';
  ds := OpenDataset(Format(sql, [QuotedStr(ProfitCenter)]));
  try
    if ds.IsEmpty then
      msg := Format('Damage # %d - Profit center %s not in QM offices', [SourceDamageID, ProfitCenter])
    else if ds.RecordCount > 1 then
      msg := Format('Damage # %d - Multiple offices for profit center %s', [SourceDamageID, ProfitCenter])
    else
      Result := ds.FieldByName('office_id').AsInteger;
    if msg <> '' then
      LogMessage(msg);
  finally
    ds.Close;
  end;
end;

function TMainDM.LocateWasRequested: string;
begin
  if SourceDamage.FieldByName('ticketnbr').IsNull or (Trim(SourceDamage.FieldByName('ticketnbr').AsString) = '') then
    Result := 'U'
  else
    Result := 'Y';
end;

end.
