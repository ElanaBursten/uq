program SampleTicketPlatPlugin;

uses
  Forms,
  uSamplePlatGui in 'uSamplePlatGui.pas' {SampleGuiForm},
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  MSXML2_TLB in '..\..\common\MSXML2_TLB.pas',
  OdMSXMLUtils in '..\..\common\OdMSXMLUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdVclUtils in '..\..\common\OdVclUtils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Sample Q Manager Ticket Attachment Plugin';
  Application.CreateForm(TSampleGuiForm, SampleGuiForm);
  Application.Run;
end.
