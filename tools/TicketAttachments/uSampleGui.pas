unit uSampleGui;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TCalledFrom = (cfTicket, cfDamage);

  TSampleGuiForm = class(TForm)
    PC: TPageControl;
    InputSheet: TTabSheet;
    AttachmentsSheet: TTabSheet;
    OutputSheet: TTabSheet;
    InputDataMemo: TMemo;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    SaveButton: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    InputFileLabel: TLabel;
    OutputFileLabel: TLabel;
    UserNameLabel: TLabel;
    PasswordLabel: TLabel;
    EmpNumberLabel: TLabel;
    InputMessageLabel: TLabel;
    Label13: TLabel;
    OutputDataMemo: TMemo;
    Label7: TLabel;
    ExistingAttachmentsLV: TListView;
    NewAttachmentsLV: TListView;
    Label8: TLabel;
    Label9: TLabel;
    AttachButton: TButton;
    OpenDialog: TOpenDialog;
    Label1: TLabel;
    Label18: TLabel;
    TicketNumberLabel: TLabel;
    Label19: TLabel;
    TicketIdLabel: TLabel;
    Label17: TLabel;
    AttachmentFolderLabel: TLabel;
    ActivitiesTab: TTabSheet;
    AddActButton: TButton;
    ActivityDetails1: TEdit;
    ActivityDetails2: TEdit;
    Details2Label: TLabel;
    ActivityList: TListView;
    Label21: TLabel;
    DamageNumberLabel: TLabel;
    Label23: TLabel;
    DamageIDLabel: TLabel;
    Details1Label: TLabel;
    DamageGroupBox: TGroupBox;
    Label22: TLabel;
    DamageStreet: TEdit;
    Label24: TLabel;
    DamageCity: TEdit;
    Label25: TLabel;
    DamageCounty: TEdit;
    Label26: TLabel;
    DamageState: TEdit;
    TicketGroupBox: TGroupBox;
    Label10: TLabel;
    StreetNumber1Edit: TEdit;
    Label20: TLabel;
    StreetNumber2Edit: TEdit;
    StreetNameEdit: TEdit;
    Label11: TLabel;
    CityEdit: TEdit;
    Label12: TLabel;
    StateEdit: TEdit;
    Label14: TLabel;
    LatitudeEdit: TEdit;
    Label15: TLabel;
    LongitudeEdit: TEdit;
    Label16: TLabel;
    Label27: TLabel;
    DamageLatitude: TEdit;
    Label28: TLabel;
    DamageLongitude: TEdit;
    LocatesTab: TTabSheet;
    Locates: TListView;
    ImageSheet: TTabSheet;
    Label29: TLabel;
    TicketImageMemo: TMemo;
    GroupBox1: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    ActionEdit: TEdit;
    SketchFileEdit: TEdit;
    BaseImageFileEdit: TEdit;
    Label31: TLabel;
    EmpIDLabel: TLabel;
    procedure SaveButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ValuesChanged(Sender: TObject);
    procedure AttachButtonClick(Sender: TObject);
    procedure InputFileLabelClick(Sender: TObject);
    procedure AddActButtonClick(Sender: TObject);
  private
    FUpdating: Boolean;
    FCalledFrom: TCalledFrom;
    procedure LoadInputData;
    procedure GenerateOutputXml;
    procedure ParseInputData;
  end;

var
  SampleGuiForm: TSampleGuiForm;

implementation

uses
  OdMiscUtils, MSXML2_TLB, OdMSXMLUtils, OdIsoDates, OdVclUtils;

{$R *.dfm}

function GetParam(const Param: string; Required: Boolean): string;
var
  ParamIndex: Integer;
begin
  Result := '';
  ParamIndex := GetCmdLineParameterIndex(Param);
  if Required and ((ParamIndex < 1) or ((ParamIndex + 1) > ParamCount)) then begin
    raise Exception.Create(Param + ' not specified');
  end;
  if ((ParamIndex < 1) or ((ParamIndex + 1) > ParamCount)) then begin
    raise Exception.Create(Param + ' value not specified');
  end;
  Result := ParamStr(ParamIndex + 1);
end;

{ TSampleGuiForm }

procedure TSampleGuiForm.AttachButtonClick(Sender: TObject);
var
  ListItem: TListItem;
begin
  if OpenDialog.Execute then begin
    ListItem := NewAttachmentsLV.Items.Add;
    ListItem.Caption := OpenDialog.FileName;
    GenerateOutputXml;
  end;
end;

procedure TSampleGuiForm.FormShow(Sender: TObject);
begin
  TicketGroupBox.Enabled := False;
  DamageGroupBox.Enabled := False;
  PC.ActivePageIndex := 0;
  StatusBar1.Panels[0].Text := 'Version: ' + AppVersionShort;
  LoadInputData;
end;

procedure TSampleGuiForm.LoadInputData;
begin
  InputMessageLabel.Caption := 'Loading...';
  try
    InputFileLabel.Caption  := GetParam('-i', True);
    OutputFileLabel.Caption := GetParam('-o', True);
    UserNameLabel.Caption   := GetParam('-u', True);
    PasswordLabel.Caption   := GetParam('-p', True);
    EmpNumberLabel.Caption  := GetParam('-e', True);
    EmpIDLabel.Caption      := GetParam('-d', True);

    if not FileExists(InputFileLabel.Caption) then
      raise Exception.Create('Input file does not exist');

    InputDataMemo.Lines.LoadFromFile(InputFileLabel.Caption);
    InputMessageLabel.Caption := 'Input Data Loaded.';

    ParseInputData;
    NewAttachmentsLV.Items.Clear;
    GenerateOutputXml;

    InputMessageLabel.Caption := 'Input Data Parsed.';
  except
    on E: Exception do begin
      InputMessageLabel.Caption := E.Message;
    end;
  end;
  FUpdating := False;
end;

procedure TSampleGuiForm.ParseInputData;
var
  Root: string;
  Doc: IXMLDomDocument;
  TicketElem, Elem2: IXMLDOMElement;
  DamageElem: IXMLDOMElement;
  AttachElem: IXMLDOMElement;
  LocateElem: IXMLDOMElement;
  AttachmentNodes: IXMLDOMNodeList;
  LocateNodes: IXMLDOMNodeList;
  i: Integer;
  ListItem: TListItem;
  S: string;
  Text: WideString;
  DetailElem: IXMLDOMElement;

  procedure ReadElement(EditBox: TEdit; XmlPath: string);
  var
    Elem: IXMLDOMElement;
  begin
    Elem := Doc.selectSingleNode(XmlPath) as IXMLDOMElement;
    EditBox.Text := '';
    if Assigned(Elem) then
      if Elem.hasChildNodes then
        EditBox.Text := Elem.text
  end;

  procedure GetTicketData(const TicketRoot: string);
  begin
    TicketNumberLabel.Caption := TicketElem.getAttribute('Number');
    TicketIdLabel.Caption := TicketElem.getAttribute('ID');

    ReadElement(StreetNumber1Edit, TicketRoot + '/Address1');
    ReadElement(StreetNumber2Edit, TicketRoot + '/Address2');
    ReadElement(StreetNameEdit, TicketRoot + '/Street');
    ReadElement(CityEdit, TicketRoot + '/City');
    ReadElement(StateEdit, TicketRoot + '/State');
    ReadElement(LatitudeEdit, TicketRoot + '/Latitude');
    ReadElement(LongitudeEdit, TicketRoot + '/Longitude');
  end;

begin
  FUpdating := True;
  FCalledFrom := cfTicket;

  Doc := ParseXml(InputDataMemo.Text);
  Root := '/QManager/Ticket';

  TicketElem := Doc.selectSingleNode(Root) as IXMLDOMElement;
  if Assigned(TicketElem) then begin
    if not Assigned(TicketElem) then
      raise Exception.Create('Expected either a Ticket or Damage formatted input xml file');
    GetTicketData(Root);
  end else begin
    Root := '/QManager/Damage';
    DamageElem := Doc.selectSingleNode(Root) as IXMLDOMElement;
    if Assigned(DamageElem) then begin
      FCalledFrom := cfDamage;
      DamageNumberLabel.Caption := DamageElem.getAttribute('Number');
      DamageIdLabel.Caption := DamageElem.getAttribute('ID');
      ReadElement(DamageStreet, Root + '/Street');
      ReadElement(DamageCity, Root + '/City');
      ReadElement(DamageState, Root + '/State');
      ReadElement(DamageCounty, Root + '/County');
      DamageLatitude.Text := '0';
      DamageLongitude.Text := '0';
      TicketElem := Doc.selectSingleNode(Root + '/Ticket') as IXMLDOMElement;
      if Assigned(TicketElem) then
        GetTicketData(Root + '/Ticket');
    end;
  end;

  SetEnabledOnControlAndChildren(DamageGroupBox, (FCalledFrom = cfDamage));
  SetEnabledOnControlAndChildren(TicketGroupBox, (FCalledFrom = cfTicket));

  AttachElem := Doc.selectSingleNode(Root + '/NewAttachmentFolder') as IXMLDOMElement;
  if Assigned(AttachElem) then
    AttachmentFolderLabel.Caption := AttachElem.text
  else
    raise Exception.Create('NewAttachmentFolder was not specified');

  ExistingAttachmentsLV.Items.Clear;
  AttachmentNodes := Doc.selectNodes(Root + '/Attachments/Attachment');
  for i := 0 to AttachmentNodes.length-1 do begin
    AttachElem := AttachmentNodes.item[i] as IXMLDOMElement;
    ListItem := ExistingAttachmentsLV.Items.Add;

    ListItem.Caption := AttachElem.getAttribute('ID');
    ListItem.SubItems.Add(AttachElem.getAttribute('Source'));

    Elem2 := AttachElem.selectSingleNode('OriginalFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) then
      if Elem2.hasChildNodes then
        S := Elem2.text;
    ListItem.SubItems.Add(S);

    Elem2 := AttachElem.selectSingleNode('FileName') as IXMLDOMElement;
    S := '';
    if Assigned(Elem2) then
      if Elem2.hasChildNodes then
        S := Elem2.text;
    ListItem.SubItems.Add(S);

    if FileExists(S) then
      ListItem.SubItems.Add('OK')
    else
      ListItem.SubItems.Add('-');
  end;

  AttachmentNodes := Doc.selectNodes(Root + '/Attachments/Action');
  if AttachmentNodes.length > 0 then begin
    AttachElem := AttachmentNodes.item[0] as IXMLDOMElement;
    ActionEdit.Text := AttachElem.getAttribute('type');
    Elem2 := AttachElem.selectSingleNode('SketchFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) and Elem2.hasChildNodes then
      S := Elem2.text;
    SketchFileEdit.Text := S;
    Elem2 := AttachElem.selectSingleNode('BaseImageFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) and Elem2.hasChildNodes then
      S := Elem2.text;
    BaseImageFileEdit.Text := S;
  end;

  LocateNodes := Doc.selectNodes(Root + '/Locates/Locate');
  for i := 0 to LocateNodes.length - 1 do begin
    LocateElem := LocateNodes.item[i] as IXMLDOMElement;
    ListItem := Locates.Items.Add;

    ListItem.Caption := LocateElem.getAttribute('ID');
    Text := '';
    GetNodeText(LocateElem, 'Term', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Client', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Status', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Utility', Text);
    ListItem.SubItems.Add(Text);
  end;

  DetailElem := Doc.selectSingleNode(Root + '/Detail') as IXMLDOMElement;
  if Assigned(DetailElem) then
    TicketImageMemo.Text := DetailElem.text
  else
    raise Exception.Create('Detail was not specified');

  FUpdating := False;
end;

procedure TSampleGuiForm.GenerateOutputXml;
var
  XML: IXMLDOMDocument;
  QMRoot: IXMLDOMElement;
  AddinRoot: IXMLDOMElement;
  AttachRoot: IXMLDOMElement;
  ActivityRoot: IXMLDOMElement;
  AttachElem: IXMLDOMElement;
  FileNameElem: IXMLDOMElement;
  ActivityElem: IXMLDOMElement;
  i: Integer;

  procedure AddChildElement(Root: IXMLDomElement; const Name, Value: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
  begin
    Element := XML.createElement(Name);
    TextNode := XML.createTextNode(Value);
    Element.appendChild(TextNode);
    Root.appendChild(Element);
  end;

begin
  if FUpdating then
    Exit;

  XML := CoDOMDocument.Create;
  XML.async := False;
  QMRoot := XML.createElement('QManager');
  XML.appendChild(QMRoot);

  if FCalledFrom = cfTicket then begin
    AddinRoot := XML.createElement('Ticket');
    AddinRoot.setAttribute('ID', TicketIdLabel.Caption);
    AddinRoot.setAttribute('Number', TicketNumberLabel.Caption);

    AddChildElement(AddinRoot, 'Address1',  StreetNumber1Edit.Text);
    AddChildElement(AddinRoot, 'Address2',  StreetNumber2Edit.Text);
    AddChildElement(AddinRoot, 'Street',    StreetNameEdit.Text);
    AddChildElement(AddinRoot, 'City',      CityEdit.Text);
    AddChildElement(AddinRoot, 'State',     StateEdit.Text);
    AddChildElement(AddinRoot, 'Latitude',  LatitudeEdit.Text);
    AddChildElement(AddinRoot, 'Longitude', LongitudeEdit.Text);
  end else begin
    AddinRoot := XML.createElement('Damage');
    AddinRoot.setAttribute('ID', DamageIdLabel.Caption);
    AddinRoot.setAttribute('Number', DamageNumberLabel.Caption);

    AddChildElement(AddinRoot, 'Street', DamageStreet.Text);
    AddChildElement(AddinRoot, 'City',   DamageCity.Text);
    AddChildElement(AddinRoot, 'County', DamageCounty.Text);
    AddChildElement(AddinRoot, 'State',  DamageState.Text);
    AddChildElement(AddinRoot, 'Latitude',  DamageLatitude.Text);
    AddChildElement(AddinRoot, 'Longitude', DamageLongitude.Text);
  end;
  QMRoot.appendChild(AddinRoot);

  AttachRoot := XML.createElement('Attachments');
  AddinRoot.appendChild(AttachRoot);

  // For each attachment added:
  for i := 0 to NewAttachmentsLV.Items.Count - 1 do begin
    AttachElem := XML.createElement('NewAttachment');
    AttachRoot.appendChild(AttachElem);
    FileNameElem := XML.createElement('FileName');
    FileNameElem.text := ExtractFileName(NewAttachmentsLV.Items[i].Caption);
    AttachElem.appendChild(FileNameElem);
  end;

  ActivityRoot := XML.createElement('Activities');
  // For each activity added:
  for i := 0 to ActivityList.Items.Count - 1 do begin
    ActivityElem := XML.createElement('Activity');
    AddChildElement(ActivityElem, 'Timestamp', ActivityList.Items[i].Caption);
    AddChildElement(ActivityElem, 'Details1', ActivityList.Items[i].SubItems[0]);
    AddChildElement(ActivityElem, 'Details2', ActivityList.Items[i].SubItems[1]);
    ActivityRoot.appendChild(ActivityElem);
  end;
  if ActivityRoot.hasChildNodes then
    QMRoot.appendChild(ActivityRoot);

  OutputDataMemo.Text := PrettyPrintXml(XML);
end;

procedure TSampleGuiForm.InputFileLabelClick(Sender: TObject);
begin
  OdShellExecute(ExtractFilePath((Sender as TLabel).Caption));
end;

procedure TSampleGuiForm.SaveButtonClick(Sender: TObject);
var
  i: Integer;
  FileName, FileNameOnly: string;
begin
  GenerateOutputXml;

  // Write output file:
  OutputDataMemo.Lines.SaveToFile(OutputFileLabel.Caption);

  // Copy the attachments in to place:
  for i := 0 to NewAttachmentsLV.Items.Count - 1 do begin
    FileName := NewAttachmentsLV.Items[i].Caption;
    FileNameOnly := ExtractFileName(FileName);

    if not FileExists(FileName) then
      raise Exception.Create('File to attach does not exist: ' + FileName);

    CopyFile(PChar(FileName), PChar(AddSlash(AttachmentFolderLabel.Caption) + FileNameOnly), False);
  end;

  Close;
end;

procedure TSampleGuiForm.ValuesChanged(Sender: TObject);
begin
  GenerateOutputXml;
end;

procedure TSampleGuiForm.AddActButtonClick(Sender: TObject);
var
  Activity: TListItem;
begin
  Activity := ActivityList.Items.Add;
  Activity.Caption := IsoDateTimeToStr(Now);
  Activity.SubItems.Add(ActivityDetails1.Text);
  Activity.SubItems.Add(ActivityDetails2.Text);
  GenerateOutputXml;
end;

end.
