object SampleGuiForm: TSampleGuiForm
  Left = 404
  Top = 237
  BorderIcons = [biSystemMenu]
  Caption = 'Test Q Manager Plat Addin'
  ClientHeight = 585
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PC: TPageControl
    Left = 0
    Top = 47
    Width = 580
    Height = 519
    ActivePage = InputSheet
    Align = alClient
    TabOrder = 0
    object InputSheet: TTabSheet
      Caption = 'Input Data'
      ImageIndex = 1
      DesignSize = (
        572
        491)
      object Label2: TLabel
        Left = 5
        Top = 47
        Width = 49
        Height = 13
        Caption = 'Input File:'
      end
      object Label4: TLabel
        Left = 5
        Top = 83
        Width = 56
        Height = 13
        Caption = 'User Name:'
      end
      object Label5: TLabel
        Left = 5
        Top = 102
        Width = 50
        Height = 13
        Caption = 'Password:'
      end
      object Label6: TLabel
        Left = 5
        Top = 120
        Width = 68
        Height = 13
        Caption = 'Emp. Number:'
      end
      object InputFileLabel: TLabel
        Left = 103
        Top = 47
        Width = 4
        Height = 13
        Caption = '-'
        OnClick = InputFileLabelClick
      end
      object UserNameLabel: TLabel
        Left = 103
        Top = 83
        Width = 4
        Height = 13
        Caption = '-'
      end
      object PasswordLabel: TLabel
        Left = 103
        Top = 102
        Width = 4
        Height = 13
        Caption = '-'
      end
      object EmpNumberLabel: TLabel
        Left = 103
        Top = 120
        Width = 4
        Height = 13
        Caption = '-'
      end
      object InputMessageLabel: TLabel
        Left = 5
        Top = 22
        Width = 561
        Height = 28
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = '---'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label13: TLabel
        Left = 5
        Top = 157
        Width = 96
        Height = 13
        Caption = 'Input File Contents:'
      end
      object Label1: TLabel
        Left = 5
        Top = 3
        Width = 338
        Height = 13
        Caption = 'Usage: -i InputXML -u UserName -p Password -e EmpNumber -d EmpID'
      end
      object Label31: TLabel
        Left = 5
        Top = 138
        Width = 42
        Height = 13
        Caption = 'Emp. ID:'
      end
      object EmpIDLabel: TLabel
        Left = 102
        Top = 138
        Width = 4
        Height = 13
        Caption = '-'
      end
      object InputDataMemo: TMemo
        Left = 5
        Top = 176
        Width = 560
        Height = 312
        Anchors = [akLeft, akTop, akRight, akBottom]
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
    object AttachmentsSheet: TTabSheet
      Caption = 'Fields and Attachments'
      ImageIndex = 2
      DesignSize = (
        572
        491)
      object Label8: TLabel
        Left = 8
        Top = 356
        Width = 105
        Height = 13
        Caption = 'Existing Attachments:'
      end
      object ExistingAttachmentsLV: TListView
        Left = 8
        Top = 375
        Width = 561
        Height = 94
        Anchors = [akLeft, akTop, akRight]
        Columns = <
          item
            Caption = 'ID'
            Width = 55
          end
          item
            Caption = 'Source'
            Width = 55
          end
          item
            Caption = 'Original File Name'
            Width = 125
          end
          item
            Caption = 'Downloaded Path / File'
            Width = 250
          end
          item
            Caption = 'OK?'
            Width = 35
          end>
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
      end
      object DamageGroupBox: TGroupBox
        Left = 260
        Top = 0
        Width = 218
        Height = 208
        Caption = ' Damage Fields '
        TabOrder = 1
        object Label22: TLabel
          Left = 8
          Top = 14
          Width = 34
          Height = 13
          Caption = 'Street:'
        end
        object Label24: TLabel
          Left = 8
          Top = 38
          Width = 23
          Height = 13
          Caption = 'City:'
        end
        object Label25: TLabel
          Left = 8
          Top = 86
          Width = 30
          Height = 13
          Caption = 'State:'
        end
        object Label26: TLabel
          Left = 8
          Top = 62
          Width = 39
          Height = 13
          Caption = 'County:'
        end
        object Label27: TLabel
          Left = 8
          Top = 110
          Width = 43
          Height = 13
          Caption = 'Latitude:'
        end
        object Label28: TLabel
          Left = 8
          Top = 134
          Width = 51
          Height = 13
          Caption = 'Longitude:'
        end
        object DamageStreet: TEdit
          Left = 88
          Top = 12
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object DamageCity: TEdit
          Left = 88
          Top = 35
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object DamageCounty: TEdit
          Left = 88
          Top = 59
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object DamageState: TEdit
          Left = 88
          Top = 83
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
        object DamageLatitude: TEdit
          Left = 88
          Top = 107
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 4
        end
        object DamageLongitude: TEdit
          Left = 88
          Top = 131
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 5
        end
      end
      object TicketGroupBox: TGroupBox
        Left = 8
        Top = 0
        Width = 241
        Height = 208
        Caption = ' Ticket Fields '
        TabOrder = 2
        object Label10: TLabel
          Left = 10
          Top = 14
          Width = 45
          Height = 13
          Caption = 'Street #:'
        end
        object Label20: TLabel
          Left = 147
          Top = 14
          Width = 4
          Height = 13
          Caption = '-'
        end
        object Label11: TLabel
          Left = 10
          Top = 38
          Width = 34
          Height = 13
          Caption = 'Street:'
        end
        object Label12: TLabel
          Left = 10
          Top = 62
          Width = 23
          Height = 13
          Caption = 'City:'
        end
        object Label14: TLabel
          Left = 10
          Top = 86
          Width = 30
          Height = 13
          Caption = 'State:'
        end
        object Label15: TLabel
          Left = 10
          Top = 110
          Width = 43
          Height = 13
          Caption = 'Latitude:'
        end
        object Label16: TLabel
          Left = 10
          Top = 134
          Width = 51
          Height = 13
          Caption = 'Longitude:'
        end
        object Label3: TLabel
          Left = 10
          Top = 158
          Width = 56
          Height = 13
          Caption = 'Work Type:'
        end
        object Label7: TLabel
          Left = 10
          Top = 182
          Width = 73
          Height = 13
          Caption = 'Work Remarks:'
        end
        object StreetNumber1Edit: TEdit
          Left = 97
          Top = 11
          Width = 44
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object StreetNumber2Edit: TEdit
          Left = 157
          Top = 11
          Width = 61
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object StreetNameEdit: TEdit
          Left = 97
          Top = 36
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object CityEdit: TEdit
          Left = 97
          Top = 59
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
        object StateEdit: TEdit
          Left = 97
          Top = 83
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 4
        end
        object LatitudeEdit: TEdit
          Left = 97
          Top = 107
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 5
        end
        object LongitudeEdit: TEdit
          Left = 97
          Top = 131
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 6
        end
        object WorkTypeEdit: TEdit
          Left = 97
          Top = 155
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 7
        end
        object WorkRemarksEdit: TEdit
          Left = 97
          Top = 179
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 8
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 214
        Width = 470
        Height = 85
        Caption = ' Action Attachment '
        TabOrder = 3
        object Label30: TLabel
          Left = 10
          Top = 16
          Width = 34
          Height = 13
          Caption = 'Action:'
        end
        object Label32: TLabel
          Left = 10
          Top = 38
          Width = 55
          Height = 13
          Caption = 'Sketch File:'
        end
        object Label33: TLabel
          Left = 10
          Top = 62
          Width = 79
          Height = 13
          Caption = 'Base Image File:'
        end
        object ActionEdit: TEdit
          Left = 97
          Top = 13
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object SketchFileEdit: TEdit
          Left = 97
          Top = 36
          Width = 364
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object BaseImageFileEdit: TEdit
          Left = 97
          Top = 59
          Width = 364
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
      end
    end
    object ImageSheet: TTabSheet
      Caption = 'Ticket Image'
      ImageIndex = 5
      DesignSize = (
        572
        491)
      object Label29: TLabel
        Left = 3
        Top = 3
        Width = 65
        Height = 13
        Caption = 'Ticket Image:'
      end
      object TicketImageMemo: TMemo
        Left = 3
        Top = 22
        Width = 566
        Height = 466
        Anchors = [akLeft, akTop, akRight, akBottom]
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
    end
    object LocatesTab: TTabSheet
      Caption = 'Locates'
      ImageIndex = 4
      object Locates: TListView
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 566
        Height = 485
        Align = alClient
        Columns = <
          item
            Caption = 'Locate ID'
            Width = 65
          end
          item
            Caption = 'Term'
            Width = 90
          end
          item
            Caption = 'Client'
            Width = 200
          end
          item
            Caption = 'Status'
            Width = 60
          end
          item
            Caption = 'Utility'
            Width = 80
          end>
        TabOrder = 0
        ViewStyle = vsReport
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 566
    Width = 580
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 580
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      580
      47)
    object Label18: TLabel
      Left = 8
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Ticket #:'
    end
    object TicketNumberLabel: TLabel
      Left = 57
      Top = 8
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 168
      Top = 8
      Width = 46
      Height = 13
      Caption = 'Ticket ID:'
    end
    object TicketIdLabel: TLabel
      Left = 217
      Top = 8
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 8
      Top = 26
      Width = 54
      Height = 13
      Caption = 'Damage #:'
    end
    object DamageNumberLabel: TLabel
      Left = 68
      Top = 26
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 168
      Top = 26
      Width = 57
      Height = 13
      Caption = 'Damage ID:'
    end
    object DamageIDLabel: TLabel
      Left = 228
      Top = 26
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CloseButton: TButton
      Left = 465
      Top = 6
      Width = 104
      Height = 25
      Anchors = [akTop]
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
    end
  end
end
