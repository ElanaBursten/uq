unit uSamplePlatGui;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, ExtCtrls;

type
  TCalledFrom = (cfTicket, cfDamage);

  TSampleGuiForm = class(TForm)
    PC: TPageControl;
    InputSheet: TTabSheet;
    AttachmentsSheet: TTabSheet;
    InputDataMemo: TMemo;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    CloseButton: TButton;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    InputFileLabel: TLabel;
    UserNameLabel: TLabel;
    PasswordLabel: TLabel;
    EmpNumberLabel: TLabel;
    InputMessageLabel: TLabel;
    Label13: TLabel;
    ExistingAttachmentsLV: TListView;
    Label8: TLabel;
    Label1: TLabel;
    Label18: TLabel;
    TicketNumberLabel: TLabel;
    Label19: TLabel;
    TicketIdLabel: TLabel;
    Label21: TLabel;
    DamageNumberLabel: TLabel;
    Label23: TLabel;
    DamageIDLabel: TLabel;
    DamageGroupBox: TGroupBox;
    Label22: TLabel;
    DamageStreet: TEdit;
    Label24: TLabel;
    DamageCity: TEdit;
    Label25: TLabel;
    DamageCounty: TEdit;
    Label26: TLabel;
    DamageState: TEdit;
    TicketGroupBox: TGroupBox;
    Label10: TLabel;
    StreetNumber1Edit: TEdit;
    Label20: TLabel;
    StreetNumber2Edit: TEdit;
    StreetNameEdit: TEdit;
    Label11: TLabel;
    CityEdit: TEdit;
    Label12: TLabel;
    StateEdit: TEdit;
    Label14: TLabel;
    LatitudeEdit: TEdit;
    Label15: TLabel;
    LongitudeEdit: TEdit;
    Label16: TLabel;
    Label27: TLabel;
    DamageLatitude: TEdit;
    Label28: TLabel;
    DamageLongitude: TEdit;
    LocatesTab: TTabSheet;
    Locates: TListView;
    ImageSheet: TTabSheet;
    Label29: TLabel;
    TicketImageMemo: TMemo;
    GroupBox1: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    ActionEdit: TEdit;
    SketchFileEdit: TEdit;
    BaseImageFileEdit: TEdit;
    Label31: TLabel;
    EmpIDLabel: TLabel;
    Label3: TLabel;
    WorkTypeEdit: TEdit;
    Label7: TLabel;
    WorkRemarksEdit: TEdit;
    procedure CloseButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure InputFileLabelClick(Sender: TObject);
  private
    FUpdating: Boolean;
    FCalledFrom: TCalledFrom;
    procedure LoadInputData;
    procedure ParseInputData;
  end;

var
  SampleGuiForm: TSampleGuiForm;

implementation

uses
  OdMiscUtils, MSXML2_TLB, OdMSXMLUtils, OdIsoDates, OdVclUtils;

{$R *.dfm}

function GetParam(const Param: string; Required: Boolean): string;
var
  ParamIndex: Integer;
begin
  Result := '';
  ParamIndex := GetCmdLineParameterIndex(Param);
  if Required and ((ParamIndex < 1) or ((ParamIndex + 1) > ParamCount)) then begin
    raise Exception.Create(Param + ' not specified');
  end;
  if ((ParamIndex < 1) or ((ParamIndex + 1) > ParamCount)) then begin
    raise Exception.Create(Param + ' value not specified');
  end;
  Result := ParamStr(ParamIndex + 1);
end;

{ TSampleGuiForm }

procedure TSampleGuiForm.FormShow(Sender: TObject);
begin
  TicketGroupBox.Enabled := False;
  DamageGroupBox.Enabled := False;
  PC.ActivePageIndex := 0;
  LoadInputData;
end;

procedure TSampleGuiForm.LoadInputData;
begin
  InputMessageLabel.Caption := 'Loading...';
  try
    InputFileLabel.Caption  := GetParam('-i', True);
    UserNameLabel.Caption   := GetParam('-u', True);
    PasswordLabel.Caption   := GetParam('-p', True);
    EmpNumberLabel.Caption  := GetParam('-e', True);
    EmpIDLabel.Caption      := GetParam('-d', True);

    if not FileExists(InputFileLabel.Caption) then
      raise Exception.Create('Input file does not exist');

    InputDataMemo.Lines.LoadFromFile(InputFileLabel.Caption);
    InputMessageLabel.Caption := 'Input Data Loaded.';

    ParseInputData;
    InputMessageLabel.Caption := 'Input Data Parsed.';
  except
    on E: Exception do begin
      InputMessageLabel.Caption := E.Message;
    end;
  end;
  FUpdating := False;
end;

procedure TSampleGuiForm.ParseInputData;
var
  Root: string;
  Doc: IXMLDomDocument;
  TicketElem, Elem2: IXMLDOMElement;
  DamageElem: IXMLDOMElement;
  AttachElem: IXMLDOMElement;
  LocateElem: IXMLDOMElement;
  AttachmentNodes: IXMLDOMNodeList;
  LocateNodes: IXMLDOMNodeList;
  i: Integer;
  ListItem: TListItem;
  S: string;
  Text: WideString;
  DetailElem: IXMLDOMElement;

  procedure ReadElement(EditBox: TEdit; XmlPath: string);
  var
    Elem: IXMLDOMElement;
  begin
    Elem := Doc.selectSingleNode(XmlPath) as IXMLDOMElement;
    EditBox.Text := '';
    if Assigned(Elem) then
      if Elem.hasChildNodes then
        EditBox.Text := Elem.text
  end;

  procedure GetTicketData(const TicketRoot: string);
  begin
    TicketNumberLabel.Caption := TicketElem.getAttribute('Number');
    TicketIdLabel.Caption := TicketElem.getAttribute('ID');

    ReadElement(StreetNumber1Edit, TicketRoot + '/Address1');
    ReadElement(StreetNumber2Edit, TicketRoot + '/Address2');
    ReadElement(StreetNameEdit, TicketRoot + '/Street');
    ReadElement(CityEdit, TicketRoot + '/City');
    ReadElement(StateEdit, TicketRoot + '/State');
    ReadElement(LatitudeEdit, TicketRoot + '/Latitude');
    ReadElement(LongitudeEdit, TicketRoot + '/Longitude');
    ReadElement(WorkTypeEdit, TicketRoot + '/WorkType');
    ReadElement(WorkRemarksEdit, TicketRoot + '/WorkRemarks');
  end;

begin
  FUpdating := True;
  FCalledFrom := cfTicket;

  Doc := ParseXml(InputDataMemo.Text);
  Root := '/QManager/Damage';

  DamageElem := Doc.selectSingleNode(Root) as IXMLDOMElement;
  if Assigned(DamageElem) then begin
    FCalledFrom := cfDamage;
    DamageNumberLabel.Caption := DamageElem.getAttribute('Number');
    DamageIdLabel.Caption := DamageElem.getAttribute('ID');
    ReadElement(DamageStreet, Root + '/Street');
    ReadElement(DamageCity, Root + '/City');
    ReadElement(DamageState, Root + '/State');
    ReadElement(DamageCounty, Root + '/County');
    DamageLatitude.Text := '0';
    DamageLongitude.Text := '0';
    TicketElem := Doc.selectSingleNode(Root + '/Ticket') as IXMLDOMElement;
    if Assigned(TicketElem) then
      GetTicketData(Root + '/Ticket');
  end else begin
    Root := '/QManager/Ticket';
    TicketElem := Doc.selectSingleNode(Root) as IXMLDOMElement;
    if not Assigned(TicketElem) then
      raise Exception.Create('Expected either a Ticket or Damage formatted input xml file');
    GetTicketData(Root);
  end;

  SetEnabledOnControlAndChildren(DamageGroupBox, (FCalledFrom = cfDamage));
  SetEnabledOnControlAndChildren(TicketGroupBox, (FCalledFrom = cfTicket));

  ExistingAttachmentsLV.Items.Clear;
  AttachmentNodes := Doc.selectNodes(Root + '/Attachments/Attachment');
  for i := 0 to AttachmentNodes.length-1 do begin
    AttachElem := AttachmentNodes.item[i] as IXMLDOMElement;
    ListItem := ExistingAttachmentsLV.Items.Add;

    ListItem.Caption := AttachElem.getAttribute('ID');
    ListItem.SubItems.Add(AttachElem.getAttribute('Source'));

    Elem2 := AttachElem.selectSingleNode('OriginalFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) then
      if Elem2.hasChildNodes then
        S := Elem2.text;
    ListItem.SubItems.Add(S);

    Elem2 := AttachElem.selectSingleNode('FileName') as IXMLDOMElement;
    S := '';
    if Assigned(Elem2) then
      if Elem2.hasChildNodes then
        S := Elem2.text;
    ListItem.SubItems.Add(S);

    if FileExists(S) then
      ListItem.SubItems.Add('OK')
    else
      ListItem.SubItems.Add('-');
  end;

  AttachmentNodes := Doc.selectNodes(Root + '/Attachments/Action');
  if AttachmentNodes.length > 0 then begin
    AttachElem := AttachmentNodes.item[0] as IXMLDOMElement;
    ActionEdit.Text := AttachElem.getAttribute('type');
    Elem2 := AttachElem.selectSingleNode('SketchFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) and Elem2.hasChildNodes then
      S := Elem2.text;
    SketchFileEdit.Text := S;
    Elem2 := AttachElem.selectSingleNode('BaseImageFileName') as IXMLDOMElement;
    S := '-';
    if Assigned(Elem2) and Elem2.hasChildNodes then
      S := Elem2.text;
    BaseImageFileEdit.Text := S;
  end;

  LocateNodes := Doc.selectNodes(Root + '/Locates/Locate');
  for i := 0 to LocateNodes.length - 1 do begin
    LocateElem := LocateNodes.item[i] as IXMLDOMElement;
    ListItem := Locates.Items.Add;

    ListItem.Caption := LocateElem.getAttribute('ID');
    Text := '';
    GetNodeText(LocateElem, 'Term', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Client', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Status', Text);
    ListItem.SubItems.Add(Text);
    Text := '';
    GetNodeText(LocateElem, 'Utility', Text);
    ListItem.SubItems.Add(Text);
  end;

  DetailElem := Doc.selectSingleNode(Root + '/Detail') as IXMLDOMElement;
  if Assigned(DetailElem) then
    TicketImageMemo.Text := DetailElem.text
  else
    raise Exception.Create('Detail was not specified');

  FUpdating := False;
end;

procedure TSampleGuiForm.InputFileLabelClick(Sender: TObject);
begin
  OdShellExecute(ExtractFilePath((Sender as TLabel).Caption));
end;

procedure TSampleGuiForm.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

end.
