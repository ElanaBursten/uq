program SampleTicketAttachmentPlugin;

{$R '..\..\QMIcon.res'}
{$R '..\..\QMVersion.res' '..\..\QMVersion.rc'}

uses
  Forms,
  uSampleGui in 'uSampleGui.pas' {SampleGuiForm},
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  MSXML2_TLB in '..\..\common\MSXML2_TLB.pas',
  OdMSXMLUtils in '..\..\common\OdMSXMLUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdVclUtils in '..\..\common\OdVclUtils.pas';

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Sample Q Manager Ticket Attachment Plugin';
  Application.CreateForm(TSampleGuiForm, SampleGuiForm);
  Application.Run;
end.
