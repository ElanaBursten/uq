unit empMain;
//QM-588  a means to sync QM employees with CertusView.
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, System.json, REST.Types,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IPPeerClient, Vcl.StdCtrls, Data.DB, Data.Win.ADODB, Vcl.ComCtrls, Vcl.ExtCtrls,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, System.DateUtils;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String;
  end;  //TLogResults

type
  TfrmMainEmp = class(TForm)
    ADOConn: TADOConnection;
    pnlTop: TPanel;
    pnlBottom: TPanel;
    StatusBar1: TStatusBar;
    btnVerify: TButton;
    spVerifyEmp: TADOStoredProc;
    Memo1: TMemo;
    RESTResponse1: TRESTResponse;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    qryCount: TADOQuery;
    qryCountHowMany: TIntegerField;
    procedure btnVerifyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }

    AuthKind:string;
    AuthValue:string;
    AuthName:string;
    lookBack:TDateTime;
    fLookBack:integer;
    fBaseURL:String;
    connStr: String;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    LogResult: TLogResults;
    flogDir: wideString;
    HowMany:integer;
    procedure ReadIni;
    function connectDB: boolean;

    function ProcessRest: boolean;
    procedure clearLogRecord;

    function GetAppVersionStr: string;
    function EnDeCrypt(const Value: String): String;
    procedure ProcessEmployee;

    function GetCall(OUT iStatus:integer; baseURL:string):string;
    function AddPair(JsonString, s1, s2: string): String;
    function PutCall(OUT iStatus:integer; baseURL, sBody:string):string;
    function GetTemplate(out iStatus: integer; baseURL: string): string;
    function PostNewRecord(OUT iStatus:integer; baseURL, sBody:string):string;
    procedure DecisionTime(Sender: TObject);
  public
    { Public declarations }
      Logpath: String;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
  end;

var
  frmMainEmp: TfrmMainEmp;

  chrunThread:TThread;

CONST            //fBaseURL                EmpNum(6)   Co(3)
  GET_ERP =       '%s/v1/CTProjectEmployee/%s?company_id=%s';
  PUT_ERP=        '%s/v1/CTProjectEmployee/%s?company_id=%s'; //update
  POST_ERP=       '%s/v1/CTProjectEmployees?company_id=%s';   //new
  GET_TEMPLATE =  '%s/v1/PJEmployTemplate?company_id=%s';


implementation
uses System.StrUtils,System.IniFiles;
{$R *.dfm}

{ TfrmMainEmp }

procedure TfrmMainEmp.btnVerifyClick(Sender: TObject);
begin
  chrunThread := TThread.CreateAnonymousThread(ProcessEmployee);
  chrunThread.Start();
//  ProcessEmployee;
end;

procedure TfrmMainEmp.ProcessEmployee;
const
  PROJ_ID= '0000000000';    //qm-591
var
  i: integer;
  TaskID, ManEmpNumber, EmpNumber, EmpName, EmpType, Company: string;
  iStatus:integer;
  baseURL: string;
  sentJSON, reTurnJSON: string;
  HireDate:TDateTime;
  isoHireDate:String;
begin
  i:=0;
  try
    while not spVerifyEmp.eof do
    begin
      with spVerifyEmp do
      begin
        inc(i);
        EmpNumber := '';
        ManEmpNumber := '';
        TaskID := '';
        reTurnJSON := '';
        Company := '';
        EmpName := '';
        EmpType := '';
        HireDate := FieldByName('Hire_Date').AsDateTime;
        isoHireDate := DateToISO8601(HireDate,True);
        EmpName := FieldByName('EmpName').AsString;
        EmpType := FieldByName('EmpType').AsString;
        EmpNumber := FieldByName('emp_number').AsString;
        ManEmpNumber := FieldByName('ManEmpNumber').AsString;
        TaskID := FieldByName('TaskID').AsString;
        Company:= FieldByName('Company').AsString;
      end;
      baseURL := Format(GET_ERP, [fBaseURL, EmpNumber, Company]);
      Memo1.Lines.Add('------------------BaseURL---------------------------');
      Memo1.Lines.Add(baseURL);
      Memo1.Lines.Add('------------------Get Return JSON--------------------');
      iStatus := -99;
      reTurnJSON := GetCall(iStatus, baseURL);
//      Memo1.Lines.Add(reTurnJSON);
      if iStatus = 200 then
      begin
        Memo1.Lines.Add('---------------AddPair--------------------------');
        reTurnJSON := AddPair(reTurnJSON, 'user1', PROJ_ID);    //qm-591
        reTurnJSON := AddPair(reTurnJSON, 'user2', TaskID);
        reTurnJSON := AddPair(reTurnJSON, 'manager1', ManEmpNumber);
        reTurnJSON := AddPair(reTurnJSON, 'emp_type_cd', EmpType);
//        reTurnJSON := AddPair(reTurnJSON, 'date_hired', isoHireDate);
//        Memo1.Lines.Add(reTurnJSON);
        baseURL := Format(PUT_ERP, [fBaseURL, EmpNumber, Company]);
        Memo1.Lines.Add('---------------PutCall--------------------------');
        iStatus := -99;
        PutCall(iStatus, baseURL, reTurnJSON);
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Update Record';
        LogResult.DataStream := reTurnJSON;
        WriteLog(LogResult);
      end
      else
      if iStatus = 404 then
      begin
        Memo1.Lines.Add('---------------Add Record--------------------------');
//        Memo1.Lines.Add(reTurnJSON);

        baseURL := Format(GET_TEMPLATE, [fBaseURL, Company]);
        Memo1.Lines.Add('---------------GET_Template--------------------------');
        iStatus := -99;
        reTurnJSON := GetTemplate(iStatus, baseURL);
//        Memo1.Lines.Add(reTurnJSON);
        reTurnJSON := AddPair(reTurnJSON, 'employee', EmpName);
        reTurnJSON := AddPair(reTurnJSON, 'user1', PROJ_ID);      //qm-591
        reTurnJSON := AddPair(reTurnJSON, 'user2', TaskID);
        reTurnJSON := AddPair(reTurnJSON, 'manager1', ManEmpNumber);
        reTurnJSON := AddPair(reTurnJSON, 'emp_type_cd', EmpType);
//        reTurnJSON := AddPair(reTurnJSON, 'date_hired', isoHireDate);
        Memo1.Lines.Add('---------------PostNewRecord--------------------------');
        iStatus := -99;
        PostNewRecord(iStatus, baseURL, reTurnJSON);
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'PostNewRecord';
        LogResult.DataStream := reTurnJSON;
        WriteLog(LogResult);
        Memo1.Refresh;
      end;
      StatusBar1.Panels[7].Text:= IntToStr(i);
    spVerifyEmp.Next;
    end;  //while not spVerifyEmp.eof do
  finally
    application.Terminate;
  end;
end;

function TfrmMainEmp.ProcessRest: boolean;
begin
  RESTRequest1.Params.AddItem(AuthName,AuthValue,TRestRequestParameterKind.pkHTTPHEADER);
end;

function TfrmMainEmp.GetCall(OUT iStatus:integer; baseURL:string):string;
begin
  RESTRequest1.ClearBody;
  RESTClient1.ContentType := 'application/json';
  RESTRequest1.Method := TRESTRequestMethod.rmGET;
  RESTClient1.BaseURL:= baseURL;

  try
    RESTRequest1.Execute;
    result:= RESTResponse1.Content;
    iStatus:=  RESTResponse1.StatusCode;
  except
    on E: Exception do
    begin
      Memo1.Lines.Add(E.Message);
      LogResult.LogType := ltError;
      LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
      LogResult.DataStream := RESTResponse1.Content;
      WriteLog(LogResult);
    end;
  end;  //try-except

  Memo1.Lines.Add('-------------RESTResponse GetCall-----------------');
  Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
  Memo1.Lines.Add('-------------------------------------------------');
end;


function TfrmMainEmp.PutCall(OUT iStatus:integer; baseURL, sBody:string):string;
begin   //Updates: Use PUT /v1/CTProjectEmployee/{employee_number}
  RESTRequest1.ClearBody;
  RESTClient1.ContentType := 'application/json';
  RESTRequest1.AddBody(sBody, ctAPPLICATION_JSON);
  RESTRequest1.Method := TRESTRequestMethod.rmPUT;
  RESTClient1.BaseURL:= baseURL;

  try
    RESTRequest1.Execute;
    result:= RESTResponse1.Content;
    iStatus := RESTResponse1.StatusCode;
  except
    on E: Exception do
    begin
      Memo1.Lines.Add(E.Message);
      LogResult.LogType := ltError;
      LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
      LogResult.DataStream := RESTResponse1.Content;
      WriteLog(LogResult);
    end;
  end;  //try-except

  Memo1.Lines.Add('-------------RESTResponse PutCall ----------------');
  Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
  Memo1.Lines.Add('-------------------------------------------------');
end;


function TfrmMainEmp.PostNewRecord(out iStatus: integer; baseURL, sBody: string): string;
begin
  RESTRequest1.ClearBody;
  RESTClient1.ContentType := 'application/json';
  RESTRequest1.AddBody(sBody, ctAPPLICATION_JSON);
  RESTRequest1.Method := TRESTRequestMethod.rmPOST;
  RESTClient1.BaseURL:= baseURL;

  try
    RESTRequest1.Execute;
    result:= RESTResponse1.Content;
    iStatus := RESTResponse1.StatusCode;
  except
    on E: Exception do
    begin
      Memo1.Lines.Add(E.Message);
      LogResult.LogType := ltError;
      LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
      LogResult.DataStream := RESTResponse1.Content;
      WriteLog(LogResult);
    end;
  end;  //try-except

  Memo1.Lines.Add('-------------REST Response PostNewRecord------------------------');
  Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
  Memo1.Lines.Add('-------------------------------------------------');

end;



function TfrmMainEmp.GetTemplate(OUT iStatus:integer; baseURL:string):string;
begin   //Updates: Use Get /v1/CTProjectEmployee/{employee_number}
  RESTRequest1.ClearBody;
  RESTClient1.ContentType := 'application/json';
  RESTRequest1.Method := TRESTRequestMethod.rmGet;
  RESTClient1.BaseURL:= baseURL;

  try
    RESTRequest1.Execute;
    result:= RESTResponse1.Content;
    iStatus := RESTResponse1.StatusCode;
  except
    on E: Exception do
    begin
      Memo1.Lines.Add(E.Message);
      LogResult.LogType := ltError;
      LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
      LogResult.DataStream := RESTResponse1.Content;
      WriteLog(LogResult);
    end;
  end;  //try-except

  Memo1.Lines.Add('-------------RESTResponse GetTemplate------------------------');
  Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
  Memo1.Lines.Add('-------------------------------------------------');

end;


function TfrmMainEmp.AddPair(JsonString, s1, s2: string): String;
var
  root: TJSonObject;
  Value: TJSonObject;
  aPair:TJSONPair;
  prev: string;

begin // ParseJSONValue(const Data: string; UseBool: Boolean): TJSONValue;
  root := TJSonObject.ParseJSONValue(JsonString, False) as TJSonObject;
  try //remove and add pair always
    if pos(s1, JsonString) > 0 then
    begin
      prev := root.GetValue<string>(s1);
      if not SameText(prev, 'false') then
      begin
        root.removePair(s1).Free;
      end;
    end;

    aPair:= TJSONPair.Create(s1, s2);
    Value := TJSonObject.Create;
    root.AddPair(aPair);
    result := root.ToString;

  finally
    root.Free;
    Value.Free;
  end;
end;


procedure TfrmMainEmp.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMainEmp.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  Result := false;
  ADOConn.Connected:= false;
  try

    try
          LogConnStr := 'Provider=SQLNCLI11;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
           ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
          if LeftStr(apassword, 4) = DECRYPT then
          begin
            apassword := copy(apassword, 5, maxint);
            apassword := EnDeCrypt(apassword);
          end
          else
          begin
            Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
          end;

          connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
           ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
          ADOConn.ConnectionString := connStr;
          ADOConn.Open;
    except on E: Exception do
       Showmessage('connStr: '+connStr +' '+e.Message);
    end;
  finally
     Result := ADOConn.Connected;
    if Result then
      StatusBar1.Panels[5].Text :=  aServer;
  end;
end;

function TfrmMainEmp.EnDeCrypt(const Value: String): String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TfrmMainEmp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmMainEmp.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  spVerifyEmp.close;
  ADOConn.Close;
  CanClose:= true;
end;

procedure TfrmMainEmp.FormCreate(Sender: TObject);
var
  s1: string;
begin
  StatusBar1.Panels[1].Text:= GetAppVersionStr;
  ReadIni;
  ProcessRest;
  If connectDB then
  begin
    HowMany:=0;
    spVerifyEmp.Parameters.ParamByName('@check_date').Value := lookBack;
    spVerifyEmp.Open;
  end;
  DecisionTime(Sender);
end;

procedure TfrmMainEmp.DecisionTime(Sender: TObject);
begin
   if ParamStr(1)<>'GUI' then
  begin
    btnVerifyClick(Sender);
  end;
end;

function TfrmMainEmp.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure TfrmMainEmp.ReadIni;
var
  filepath: String;
  EmployeesAreGood: TIniFile;
const
  INI_FILE = 'EmployeesAreGood';
begin
  try
    try
      filePath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
      EmployeesAreGood := TIniFile.Create(filePath + INI_FILE + '.ini');
      aServer := EmployeesAreGood.ReadString('Database', 'Server', '');
      aDatabase := EmployeesAreGood.ReadString('Database', 'DB', 'QM');
      ausername := EmployeesAreGood.ReadString('Database', 'UserName', '');
      apassword := EmployeesAreGood.ReadString('Database', 'Password', '');
      aTrusted  := EmployeesAreGood.ReadString('Database', 'Trusted', '1');
      fBaseURL  := EmployeesAreGood.ReadString('REST', 'BASEURL', '');
      fLookBack := EmployeesAreGood.ReadInteger('REST', 'LookBack', 1);
      AuthName  := EmployeesAreGood.ReadString('REST', 'AuthName', 'X-API-KEY');
      AuthValue := EmployeesAreGood.ReadString('REST', 'AuthValue', '85VTnoK58579708ujKOUQaSxXbLY7jm1');
      AuthKind  := EmployeesAreGood.ReadString('REST', 'AuthKind', 'pkHTTPHEADER');

      LogPath :=  EmployeesAreGood.ReadString('Paths', 'LogPath', 'C:\QM\Logs');
      ForceDirectories(LogPath);
    except
      On E:Exception do
      begin
        Raise exception.Create('Failed to read from Ini file: ' + E.Message);
      end;
    end;
    lookBack := (NOW - fLookBack);
    StatusBar1.Panels[3].Text:= DateTimeToStr(lookBack);
  finally
    FreeAndNil(EmployeesAreGood);
  end;
end;

procedure TfrmMainEmp.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(LogPath) +'EmpVerify_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

end.
