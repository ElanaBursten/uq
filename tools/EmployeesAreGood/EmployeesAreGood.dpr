program EmployeesAreGood;
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

{$R '..\..\QMIcon.res'}
//QM-588  a means to sync QM employees with CertusView.
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  empMain in 'empMain.pas' {frmMainEmp};

var
   MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1)='GUI' then //qm-591
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
  Application.Title := '';
  Application.CreateForm(TfrmMainEmp, frmMainEmp);
  Application.Run;
  end
  else
    Application.Terminate;
end.
