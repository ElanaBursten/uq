object frmMainEmp: TfrmMainEmp
  Left = 0
  Top = 0
  Caption = 'Employees Are Good'
  ClientHeight = 566
  ClientWidth = 1155
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 1155
    Height = 17
    Align = alTop
    TabOrder = 0
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 17
    Width = 1155
    Height = 549
    Align = alClient
    TabOrder = 1
    object StatusBar1: TStatusBar
      Left = 1
      Top = 529
      Width = 1153
      Height = 19
      Panels = <
        item
          Text = 'version'
          Width = 50
        end
        item
          Width = 100
        end
        item
          Text = 'From'
          Width = 50
        end
        item
          Width = 140
        end
        item
          Text = 'Server'
          Width = 50
        end
        item
          Width = 200
        end
        item
          Text = 'Count'
          Width = 50
        end
        item
          Width = 50
        end
        item
          Text = 'of'
          Width = 20
        end
        item
          Width = 100
        end>
    end
    object btnVerify: TButton
      Left = 1064
      Top = 30
      Width = 75
      Height = 25
      Caption = 'Verify'
      TabOrder = 1
      OnClick = btnVerifyClick
    end
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 1057
      Height = 528
      Align = alLeft
      ScrollBars = ssVertical
      TabOrder = 2
    end
  end
  object ADOConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLNCLI11.1;Integrated Security=SSPI;Persist Security I' +
      'nfo=False;User ID="";Initial Catalog=QM;Data Source=SSDS-UTQ-QM-' +
      '02-DV;Use Procedure for Prepare=1;Auto Translate=True;Packet Siz' +
      'e=4096;Workstation ID=DYE1B-DQMBAS1;Initial File Name="";Use Enc' +
      'ryption for Data=False;Tag with column collation when possible=F' +
      'alse;MARS Connection=False;DataTypeCompatibility=0;Trust Server ' +
      'Certificate=False;Application Intent=READWRITE'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 32
    Top = 216
  end
  object spVerifyEmp: TADOStoredProc
    Connection = ADOConn
    CursorType = ctStatic
    ProcedureName = 'Employee_Verifier'
    Parameters = <
      item
        Name = '@check_date'
        DataType = ftDateTime
        Value = Null
      end>
    Prepared = True
    Left = 32
    Top = 281
  end
  object RESTResponse1: TRESTResponse
    Left = 808
    Top = 184
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 
      'https://erp-api-proxy-qa.services.certusview.com/api/v1/CTProjec' +
      'tEmployee/000001?company_id=UTQ'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 808
    Top = 72
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Params = <>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 688
    Top = 56
  end
  object qryCount: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <
      item
        Name = 'check_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select count(*) HowMany'
      'from dbo.employee '
      'where modified_date>= :check_date'
      'and ad_username is not null'
      'and active = 1')
    Left = 104
    Top = 281
    object qryCountHowMany: TIntegerField
      FieldName = 'HowMany'
      ReadOnly = True
    end
  end
end
