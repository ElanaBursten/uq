  set nocount on

  declare @emps table (
    emp_id int primary key,
    short_name varchar(50)
  )

  declare @tickets_all table (
    ticket_number varchar(20),
    ticket_id integer,
    due_date datetime,
    ticket_type varchar(38),
    ticket_count integer,
    locate_id integer,
    kind varchar(20),
    ticket_format varchar(20),
    transmit_date datetime,
    [source] varchar(12),
    workload_date datetime
  )

  insert into @emps
  select h_emp_id, h_short_name
  from dbo.get_report_hier3(@ManagerId, 0, 99, 2)

  insert into @tickets_all
  select
    ticket.ticket_number,
    ticket.ticket_id,
    ticket.due_date,
    ticket.ticket_type,
    0 ticket_count,
    locate.locate_id,
    ticket.kind,
    ticket.ticket_format,
    ticket.transmit_date,
    ticket.source,
	(select min(a.workload_date) from locate l2
    inner join assignment a on (l2.locate_id = a.locate_id and a.active = 1)
    where l2.ticket_id = ticket.ticket_id) workload_date
  from locate
    inner join ticket on ticket.ticket_id = locate.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join @emps emps on emps.emp_id = locate.assigned_to
    left join event_log on (aggregate_id = locate.ticket_id and aggregate_type = 1 and version_num = 1)
  where locate.closed = 0
    and event_log.event_id is null and ticket.kind <> 'DONE'
    and (ticket.status is null or ticket.status not like 'MANUAL%%')

  --------
  -- mark ticket count as 1 for every unique ticket in a set of locates
  --------
update @tickets_all
    set ticket_count = 1
  where locate_id in (select locate_id from (
    select locate_id, ticket_number,
      row_number() over (partition by ticket_number order by locate_id) rnum
    from @tickets_all) s
    where rnum = 1)

  select ticket_id, ticket_number, kind, ticket_type, transmit_date,
    due_date, workload_date, ticket_format, [source]
    from @tickets_all
    where ticket_count > 0
