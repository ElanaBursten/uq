program QMGenTicketEvents;

{$APPTYPE CONSOLE}

{$R 'QMGenTicketEventsResources.res' 'QMGenTicketEventsResources.rc'}

uses
  System.SysUtils,
  MainDMu in 'MainDMu.pas' {MainDM: TDataModule},
  QMLogic2ServiceLib in '..\..\server2\gen-delphi\QMLogic2ServiceLib.pas',
  EventSource in '..\..\server2\EventSource.pas',
  MSSqlEventSource in '..\..\server2\MSSqlEventSource.pas',
  EventUtils in '..\..\server2\EventUtils.pas';

{$R 'QMVersion.res' '..\..\QMVersion.rc'}
{$R '..\..\QMIcon.res'}

var
  DM: TMainDM;
begin
  try
    DM := TMainDM.Create(nil);
    try
      DM.Execute;
    finally
      FreeAndNil(DM);
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
