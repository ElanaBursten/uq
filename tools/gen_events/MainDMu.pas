unit MainDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync, uADPhysMSSQL,
  uADPhysManager, uADStanParam, uADDatSManager, uADDAptIntf, uADDAptManager,
  Data.DB, uADCompDataSet, uADCompClient, QMLogic2ServiceLib, EventSource,
  MSSqlEventSource;

type
  TCriteria = class
  public
    ManagerId: string;
    constructor Create(const AManagerId: string); overload; virtual;
  end;

  TMainDM = class(TDataModule)
    Conn: TADConnection;
    Tickets: TADQuery;
    Locates: TADQuery;
    AddNotice: TADStoredProc;
  private
    Criteria: TCriteria;
    TicketCnt: Integer;
    SystemUserEmpID: Integer;
    function Setup: Boolean;
    procedure PrintUsage;
    procedure Process;
    procedure Log(const Msg: string);
    procedure AddTicketReceivedEvent(Key: IAggrKey; EventList: IEventList);
    procedure AddLocateAssignedEvent(Key: IAggrKey; EventList: IEventList);
    procedure AddTicketScheduledEvent(Key: IAggrKey; EventList: IEventList);
    procedure AddNewEventNotice(Key: IAggrKey);
  public
    procedure Execute;
  end;

implementation

uses
  IniFiles, OdMiscUtils, OdIsoDates, AdUtils, Thrift, Thrift.Collections;

const
  LocateSQL = 'select l.locate_id, l.client_id, l.client_code, c.client_name, ' +
    'l.status, l.closed_date, a.locator_id, a.insert_date, a.added_by from locate l ' +
    'inner join client c on l.client_id = c.client_id ' +
    'inner join assignment a on l.locate_id = a.locate_id and a.active = 1 ' +
    'where l.status not in (''-N'', ''-P'') and ticket_id = :TicketID';
  SystemUserEmpIdSQL = 'select convert(int, value) as value ' +
    'from configuration_data where name = ''SystemUserEmpID''';

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TCriteria }
constructor TCriteria.Create(const AManagerId: string);
begin
  if AManagerId = '' then
    ManagerId := '2676'
  else
    ManagerId := AManagerId;
end;

{ TMainDM }

procedure TMainDM.Execute;
var
  Proceed: Boolean; //readability
begin
  Proceed := Setup;
  if Proceed then begin
    Log('');
    Log('Starting Ticket event generation');
    Log(Format('Adding events for Tickets received for Manager Id %s ', [Criteria.ManagerId]));
    Process;
    Log(Format('Finished. Added events for %d Tickets.', [TicketCnt]));
  end;
end;

procedure TMainDM.Log(const Msg: string);
begin
  WriteLn(FormatDateTime('yyyy-mm-dd hh:nn:ss ', Now), Msg);
end;

procedure TMainDM.Process;

  function GetTicketKey: IAggrKey;
  begin
    Result := TAggrKeyImpl.Create;
    Result.AggrType := TAggregateType.atTicket;
    Result.AggrID := Tickets.FieldByName('ticket_id').Value;
  end;

   function GetSQLFromResource(const ResName: string): string;
  begin
    Result := GetResourceString(ResName);
    Result := StringReplace(Result, '@ManagerId', Criteria.ManagerId, [rfReplaceAll]);
  end;

var
  Store: IEventStore;
  EventList: IEventList;
  Key: IAggrKey;
begin
  TicketCnt := 0;
  Store := TMSSQLEventStore.Create(nil, Conn);
  EventList := TEventListImpl.Create;
  EventList.Items := TThriftListImpl<IEvent>.Create;


  try
    SystemUserEmpID := Conn.ExecSQLScalar(SystemUserEmpIdSQL);
    Tickets.Open(GetSQLFromResource('GET_GENTICKETEVENTS_SQL'));
    while not Tickets.Eof do begin
      Conn.StartTransaction;
      try
        Log(Format('Adding events for %d', [Tickets.FieldByName('ticket_id').AsInteger]));
        Key := GetTicketKey;
        AddTicketReceivedEvent(Key, EventList);
        AddTicketScheduledEvent(Key, EventList);
        AddLocateAssignedEvent(Key, EventList);
        Store.SaveEvents(Key, EventList, 0);
        Conn.Commit;
        Inc(TicketCnt);
      except
        on E: Exception do begin
          Log('Unexpected error - ' + E.Message);
          Log('Rolling back current ticket''s events.');
          if Conn.InTransaction then
            Conn.Rollback;
          raise;
        end;
      end;
      EventList.Items.Clear;
      Tickets.Next;
    end;
    // If any events were added, add a new event notice to the SQL Broker queue.
    if TicketCnt > 0 then
      AddNewEventNotice(Key);
  finally
    Conn.Close;
    Log('Closed db connection');
  end;
end;

procedure TMainDM.AddTicketReceivedEvent(Key: IAggrKey; EventList: IEventList);
var
  Event: IEvent;
  Loc: ILocateSummary;
begin
  Event := TEventImpl.Create;
  Event.AggregateKey := Key;
  Event.Union := TEventUnionImpl.Create;
  Event.Union.TicketReceived := TTicketReceivedImpl.Create;
  Event.Union.TicketReceived.TicketNumber := Tickets.FieldByName('ticket_number').AsString;
  Event.Union.TicketReceived.TicketFormat := Tickets.FieldByName('ticket_format').AsString;
  Event.Union.TicketReceived.Source := Tickets.FieldByName('source').AsString;
  Event.Union.TicketReceived.Kind := Tickets.FieldByName('kind').AsString;
  Event.Union.TicketReceived.TicketType := Tickets.FieldByName('ticket_type').AsString;
  Event.Union.TicketReceived.TransmitDate := IsoDateTimeToStr(Tickets.FieldByName('transmit_date').AsDateTime);
  Event.Union.TicketReceived.DueDate := IsoDateTimeToStr(Tickets.FieldByName('due_date').AsDateTime);
  Event.Union.TicketReceived.LocateList := TThriftListImpl<ILocateSummary>.Create;
  Locates.Close;
  Locates.Open(LocateSQL, [Tickets.Fields[0].Value]);
  while not Locates.Eof do begin
    Loc := TLocateSummaryImpl.Create;
    Loc.ID := Locates.FieldByName('locate_id').AsInteger;
    Loc.ClientID := Locates.FieldByName('client_id').AsInteger;
    Loc.ClientCode := Locates.FieldByName('client_code').AsString;
    Loc.ClientName := Locates.FieldByName('client_name').AsString;
    Loc.Status := Locates.FieldByName('status').AsString;
    Event.Union.TicketReceived.LocateList.Add(Loc);
    Locates.Next;
  end;
  EventList.Items.Add(Event);
end;

procedure TMainDM.AddTicketScheduledEvent(Key: IAggrKey; EventList: IEventList);
var
  Event: IEvent;
begin
  Event := TEventImpl.Create;
  Event.AggregateKey := Key;
  Event.Union := TEventUnionImpl.Create;
  Event.Union.TicketScheduled := TTicketScheduledImpl.Create;
  Event.Union.TicketScheduled.WorkloadDate := IsoDateTimeToStr(Tickets.FieldByName('workload_date').AsDateTime);
  Event.Union.TicketScheduled.ChangedByID := SystemUserEmpID;
  Event.Union.TicketScheduled.ChangedDate := IsoDateTimeToStr(Tickets.FieldByName('transmit_date').AsDateTime);
  EventList.Items.Add(Event);
end;

procedure TMainDM.AddLocateAssignedEvent(Key: IAggrKey; EventList: IEventList);
var
  Event: IEvent;
begin
  Locates.Close;
  Locates.Open(LocateSQL, [Tickets.Fields[0].Value]);
  while not Locates.Eof do begin
    Event := TEventImpl.Create;
    Event.AggregateKey := Key;
    Event.Union := TEventUnionImpl.Create;
    Event.Union.LocateAssigned := TLocateAssignedImpl.Create;
    Event.Union.LocateAssigned.LocateID := Locates.FieldByName('locate_id').AsInteger;
    Event.Union.LocateAssigned.ToLocatorID := Locates.FieldByName('locator_id').AsInteger;
    if IsInteger(Locates.FieldByName('added_by').AsString) then
      Event.Union.LocateAssigned.ChangedByID := Locates.FieldByName('added_by').AsInteger
    else
      Event.Union.LocateAssigned.ChangedByID := SystemUserEmpID;
    Event.Union.LocateAssigned.ChangedDate := IsoDateTimeToStr(Tickets.FieldByName('transmit_date').AsDateTime);
    EventList.Items.Add(Event);
    Locates.Next;
  end;
end;

procedure TMainDM.AddNewEventNotice(Key: IAggrKey);
begin
  AddNotice.ParamByName('@AggrType').Value := Integer(Key.AggrType);
  AddNotice.ParamByName('@AggrID').Value := Key.AggrID;
  AddNotice.ParamByName('@Version').Value := 1;
  AddNotice.ExecProc;
end;

function TMainDM.Setup: Boolean;
var
  IniFileName: string;
  Ini: TIniFile;
  ReadVar: string;
begin
  Result := True;
  IniFileName := ExtractFilePath(ParamStr(0)) + 'QMServer.ini';
  try
    if (ParamCount = 1) and (Pos('?',ParamStr(1))>0) then begin
      PrintUsage;
      Result := False;
      Exit;
    end;

    if ParamCount = 0 then begin
      WriteLn('No Manager Id was provided.');
      WriteLn('Do you want to process ALL open tickets?  Y or N');
      WriteLn('');
      Readln(ReadVar);
      if (UpperCase(Readvar)<>'Y') then begin
        Result := False;
        Exit;
      end;
    end;


    Criteria := TCriteria.Create(ParamStr(1));
    if not FileExists(IniFileName) then
      raise Exception.Create('A QMServer.ini file with a valid [Database] section is required.');

    Log('Configuring db connection from ' + IniFileName);
    Ini := TiniFile.Create(IniFileName);
    try
      ConnectADConnectionWithIni(Conn, Ini, 'Database');
      Log('Connected to ' + Conn.ConnectionDefName);
    finally
      FreeAndNil(Ini);
    end;
  except
    on E: Exception do begin
      PrintUsage;
      raise;
    end;
  end;
end;

procedure TMainDM.PrintUsage;
begin
  WriteLn('');
  WriteLn('QMGenTicket adds TicketReceived, TicketScheduled, and LocateAssigned');
  WriteLn('events to the QM event store.' );
  WriteLn('Open tickets are processed based on the Manager Id or all when no parameter is supplied.');
  WriteLn('The parameter are optional.');
  WriteLn('');
  WriteLn('Usage:      QMGenTicketEvents ManagerId ');
  WriteLn('Example *1: QMGenTicketEvents');
  WriteLn('Example *2: QMGenTicketEvents 2676');
  WriteLn('');
  WriteLn('*1. No parameter will process all open tickets.');
  WriteLn('*2. Providing a ManagerId will process all open tickets for all employees reporting to Manager');
end;

end.
