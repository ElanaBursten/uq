object MainDM: TMainDM
  OldCreateOrder = False
  Height = 150
  Width = 215
  object Conn: TADConnection
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDateTimeStamp
        TargetDataType = dtDateTime
      end>
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 32
    Top = 16
  end
  object Tickets: TADQuery
    Connection = Conn
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    Left = 32
    Top = 80
  end
  object Locates: TADQuery
    Connection = Conn
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    Left = 88
    Top = 80
  end
  object AddNotice: TADStoredProc
    Connection = Conn
    StoredProcName = 'add_new_event_notice'
    Left = 152
    Top = 81
    ParamData = <
      item
        Name = '@AggrType'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = '@AggrID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = '@Version'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
end
