unit SCSMainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SuperObject, WinHttp_TLB, Sockets, DB, AxCtrls;

type
  TMainForm = class(TForm)
    JSONResultMemo: TMemo;
    Label2: TLabel;
    Label1: TLabel;
    edtURLCV: TEdit;
    edtBrowseCV: TEdit;
    Button1: TButton;
    btnPostCV: TButton;
    procedure Button1Click(Sender: TObject);
    procedure btnPostCVClick(Sender: TObject);
 end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}


function FileToOleVariant(Filename: string): OleVariant;
var
  AStream: TFileStream;
  MyBuffer: Pointer;
begin
  AStream := TFileStream.Create(FileName, fmOpenRead );
  try
    Result := VarArrayCreate( [0, AStream.Size - 1], VarByte );
    MyBuffer := VarArrayLock( Result );
    AStream.ReadBuffer( MyBuffer^, AStream.Size );
    VarArrayUnlock( Result );
  finally
    AStream.Free;
  end;
end;

procedure OleVariantToFile(const OV: OleVariant; Filename: string);
var
  Data: PByteArray;
  Size: integer;
  FS: TFileStream;
begin
  FS := TFileStream.Create(Filename, fmCreate);
  try
    Size := VarArrayHighBound (OV, 1) - VarArrayLowBound
    (OV, 1) + 1;
    Data := VarArrayLock(OV);
    try
      FS.WriteBuffer(Data^, Size);
    finally
      VarArrayUnlock(OV);
    end;
  except
    FS.Free;
  end;
end;



procedure TMainForm.btnPostCVClick(Sender: TObject);
var
  JsonData: ISuperObject;
  Url, FileName: string;
  I: Integer;
  ListRequest: IWinHttpRequest;

  ContentFileUrl: string;
  FileRequest: IWinHttpRequest;

begin
  // File to unencrypt
  FileName := edtBrowseCV.Text;
  Url := edtURLCV.Text;

  //Use WinHttp, imported type library
  ListRequest := CoWinHttpRequest.Create;
  ListRequest.open('POST', Url, False);

  //set the mime type
  ListRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');

  {Now set the client certificate.  Had to run the C# app and use it's client certificate
  tester to figure out the certificate store name and certificate subject name and certificate
  store location to use.  Took some trial and error.  Ended up being "CURRENT_USER\My\ReadOnly CVTSCS".
  (For more detailed info about this, see: http://msdn.microsoft.com/en-us/library/windows/desktop/aa384076(v=vs.85).aspx )}
  //Other useful links:
  //http://msdn.microsoft.com/en-us/library/windows/desktop/bb648706(v=vs.85).aspx
  //http://msdn.microsoft.com/en-us/library/windows/desktop/aa384076(v=vs.85).aspx

    try
    ListRequest.SetClientCertificate('CURRENT_USER\My\ReadOnly CVTSCS');
    //Post the file to dycom server
    ListRequest.Send(FileToOleVariant(FileName));

    //Now process the response
    //todo    if Http.Status = 200 then begin
    //todo 200? don't know if that's the right code to check for
    // parse the response using JSON
    JsonData := SO(ListRequest.ResponseText);
    JSONResultMemo.Clear;

    for I := 0 to JsonData.AsArray.Length - 1 do begin
      JSONResultMemo.Lines.Add(JsonData.AsArray.S[I]);
    end;

    for I := 0 to JsonData.AsArray.Length - 1 do begin
      ContentFileUrl := JsonData.AsArray.S[I];
      // IF the url matches a wildcard...
      if Pos('manifest',ContentFileUrl)>0 then begin
        FileRequest := CoWinHttpRequest.Create;
        FileRequest.open('GET', ContentFileUrl, False);
        FileRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');
        FileRequest.SetClientCertificate('CURRENT_USER\My\ReadOnly CVTSCS');
        FileRequest.Send(EmptyParam);

        OleVariantToFile(FileRequest.ResponseBody, '1.jpg');
        JSONResultMemo.Lines.Add('wrote file');
      end;
    end;
  except
   on E: Exception do
     ShowMessage(E.Message);
  end;
end;

procedure TMainForm.Button1Click(Sender: TObject);
var
  openDialog: TOpenDialog;
begin
  openDialog := TOpenDialog.Create(Self);
  openDialog.Filter := '@CV files|*.@cv| All files|*.*';
  if openDialog.Execute then
    edtBrowseCV.Text := openDialog.FileName;

  openDialog.Free;

end;

end.
