program SecuredContentService;

uses
  Forms,
  SCSMainUnit in 'SCSMainUnit.pas' {MainForm},
  WinHttp_TLB in 'WinHttp_TLB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
