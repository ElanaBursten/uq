object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Secured Content Service'
  ClientHeight = 548
  ClientWidth = 827
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 19
    Top = 182
    Width = 84
    Height = 52
    Caption = 'Parsed JSON response from Secured Content Service:'
    WordWrap = True
  end
  object Label1: TLabel
    Left = 32
    Top = 40
    Width = 87
    Height = 13
    Caption = '@CV Extract URL:'
  end
  object JSONResultMemo: TMemo
    Left = 136
    Top = 179
    Width = 489
    Height = 86
    TabOrder = 0
  end
  object edtURLCV: TEdit
    Left = 136
    Top = 37
    Width = 577
    Height = 21
    TabOrder = 1
    Text = 
      'https://securedcontentservice.certusview.com/SecuredContent/Extr' +
      'actSecuredContentFile'
  end
  object edtBrowseCV: TEdit
    Left = 136
    Top = 82
    Width = 577
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 19
    Top = 80
    Width = 100
    Height = 25
    Caption = 'Browse for @CV'
    TabOrder = 3
    OnClick = Button1Click
  end
  object btnPostCV: TButton
    Left = 19
    Top = 136
    Width = 100
    Height = 25
    Caption = 'Post @CV file'
    TabOrder = 4
    OnClick = btnPostCVClick
  end
end
