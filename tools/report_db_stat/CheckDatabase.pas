unit CheckDatabase;

interface

uses
  SysUtils, Classes, DB, Forms, ActiveX, ADODB, IniFiles, OdAdoUtils,
  OdMiscUtils;

type
  EMissingIniFile = class(Exception);
  TDatabaseChecker = class(TDataModule)
    Connection: TADOConnection;
  private
  public
    MinimumAvailableDbs: Integer;
    function AvailableDBCount: Integer;
    procedure Log(Msg: string; NewLine: Boolean=False);
  end;

  function CreateDatabaseChecker(const Timeout, Minimum: Integer): TDatabaseChecker;

implementation

{$R *.dfm}

const
  IniFileName = 'ReportEngineCGI.ini';
  ReportDbCountSection = 'Database';
  ReportDbCountSetting = 'NumberOfReportingDBs';
  ReportDbSectionName = 'ReportingDatabase';

function CreateDatabaseChecker(const Timeout, Minimum: Integer): TDatabaseChecker;
begin
  Result := TDatabaseChecker.Create(nil);
  Result.Connection.ConnectionTimeout := Timeout;
  Result.MinimumAvailableDbs := Minimum;
end;

procedure TDatabaseChecker.Log(Msg: string; NewLine: Boolean);
begin
  if NewLine then
    WriteLn(Msg)
  else
    Write(Msg);
end;

function TDatabaseChecker.AvailableDBCount: Integer;
var
  ConnString: string;
  Ini: TIniFile;
  DbSectionName: string;
  Status: string;
  UpCount, DbCount: Integer;
  I: Integer;
begin
  Log(Format('ReportDbStat using: Connection Timeout=%d, Minimum Up Dbs=%d',
    [Connection.ConnectionTimeout, MinimumAvailableDbs]), True);
  DbCount := 0;
  UpCount := 0;
  Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + IniFileName);
  try
    if not FileExists(Ini.FileName) then
      raise EMissingIniFile.Create('Unable to get settings from ' + Ini.FileName);

    DbCount := Ini.ReadInteger(ReportDbCountSection, ReportDbCountSetting, 0);
    for I := 1 to DbCount do begin
      DbSectionName := ReportDbSectionName + IntToStr(I);
      ConnString := MakeConnectionStringFromIni(Ini.FileName, DbSectionName);
      Connection.ConnectionString := ConnString;
      Log('Checking ' + DbSectionName + '...');
      try
        Connection.Connected := True;
        Log('UP', True);
        Inc(UpCount);
      except
        on E: Exception do begin
          // can't connect to the requested db.
          if Pos('timeout', Lowercase(E.Message)) > 0 then
            Log('? (timeout)', True)
          else
            Log('DOWN', True);
        end;
      end;
      Connection.Connected := False;
    end;
  finally
    FreeAndNil(Ini);
  end;
  Status := 'OK';
  if UpCount < MinimumAvailableDbs then
    Status := 'NOT ' + Status;

  Log(Format('%d of %d QM Report %s accessible - %s', [UpCount, DbCount,
    AddSIfNot1(UpCount, 'database'), Status]), True);
  Result := UpCount;
end;

initialization
  CoInitialize(nil);

finalization
  CoUninitialize;

end.
