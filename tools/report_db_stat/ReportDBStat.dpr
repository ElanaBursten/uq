program ReportDBStat;

{$APPTYPE CONSOLE}
{$WARN SYMBOL_PLATFORM OFF}

{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  SysUtils,
  OdAdoUtils in '..\..\common\OdAdoUtils.pas',
  CheckDatabase in 'CheckDatabase.pas' {DatabaseChecker: TDataModule},
  OdDbUtils in '..\..\common\OdDbUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  UQDbConfig in '..\..\common\UQDbConfig.pas',
  OdExceptions in '..\..\common\OdExceptions.pas',
  QMConst in '..\..\client\QMConst.pas';

var
  Count: Integer;
  Timeout: Integer;
  Minimum: Integer;
  DbChecker: TDatabaseChecker;

  procedure ProgramHelp;
  begin
    WriteLn('Version: ' + AppVersionShort);
    WriteLn('');
    WriteLn('ReportDBStat checks the number of available QM reporting databases. Syntax:');
    WriteLn('ReportDBStat.exe /t:5 /m:2');
    WriteLn(' /t:5 = seconds to wait for db to connect (optional; default is 5)');
    WriteLn(' /m:2 = minimum number of accessible dbs for success (optional; default is 2)');
    WriteLn(' /? = shows program syntax');
    WriteLn('');
    WriteLn(' errorlevel=0 for success or 200-299 for failures');
    WriteLn('');
  end;

  function GetValueFromCmdLine(SwitchName: string; Default: Integer): Integer;
  var
    I: Integer;
    SwitchLen: Integer;
  begin
    Result := Default;
    SwitchLen := Length(SwitchName);
    for I := 1 to ParamCount do
      if SameText(Copy(ParamStr(I), 1, SwitchLen), SwitchName) then begin
        Result := StrToIntDef(Copy(ParamStr(I), SwitchLen+2, Length(ParamStr(I))), Default);
        Break;
      end;
  end;

begin
  if Pos('?', CmdLine) > 0 then begin
    ProgramHelp;
    Exit;
  end;

  try
    Timeout := GetValueFromCmdLine('/t', 5); // seconds to wait for db to connect
    Minimum := GetValueFromCmdLine('/m', 2); // minimum # of up dbs for success
    DbChecker := CreateDatabaseChecker(Timeout, Minimum);
    try
      Count := DbChecker.AvailableDBCount;
      if Count >= Minimum then
        ExitCode := 0
      else
        ExitCode := 200;
    finally
      FreeAndNil(DbChecker);
    end;
  except
    on E:Exception do begin
      ExitCode := 299;
      WriteLn('Unexpected Error: ', E.Classname, ': ', E.Message);
    end;
  end;
end.
