program UTAJammer;
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  utaJammerMain in 'utaJammerMain.pas' {frmUTAJammer};

var
   MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
  Application.Title := '';
  Application.CreateForm(TfrmUTAJammer, frmUTAJammer);
  Application.Run;
  end
  else
    Application.Terminate;
end.
