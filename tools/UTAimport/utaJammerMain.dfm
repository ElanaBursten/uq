object frmUTAJammer: TfrmUTAJammer
  Left = 0
  Top = 0
  Caption = 'UAT TimeEntry Data Jammer'
  ClientHeight = 515
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  TextHeight = 15
  object UATTimeEntryGrid: TDBGrid
    Left = 8
    Top = 8
    Width = 569
    Height = 209
    DataSource = dsTimeByDateEmpID
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EmpNo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WorkDate'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WorkTime'
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WorkDateTime'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Emp_ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'On_Off'
        Visible = True
      end>
  end
  object btnInsert: TButton
    Left = 432
    Top = 456
    Width = 145
    Height = 25
    Caption = 'Insert To Table'
    TabOrder = 1
    OnClick = btnInsertClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 248
    Width = 569
    Height = 202
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object btnParseFile: TButton
    Left = 8
    Top = 456
    Width = 75
    Height = 25
    Caption = 'Parse File'
    TabOrder = 3
    OnClick = btnParseFileClick
  end
  object qryEmpIDWorkDate: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      #9'UTA.[WorkDate],'
      #9'UTA.[Emp_ID],'
      #9'Case E.[timerule_id] When 591 Then '#39'True'#39' End as IsCalif,'
      #9'E.[type_id],'
      #9'dbo.get_employee_pc(UTA.Emp_id, 1) as ProfitCenter'
      'FROM [dbo].[UTAtime_entry] UTA'
      'Left Outer Join employee E on (E.emp_id= UTA.Emp_id)'
      'Left Outer Join [employee_right] ER on (ER.Emp_id= UTA.emp_id )'
      'where ER.right_id =237'
      
        'Group by UTA.[WorkDate] ,UTA.[Emp_ID], E.[timerule_id], E.[type_' +
        'id]'
      'order by UTA.[WorkDate]')
    Left = 216
    Top = 16
  end
  object qryTimeByDateEmpID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'EmpID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'WorkDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 10
        Size = 6
        Value = Null
      end>
    SQL.Strings = (
      
        '  SELECT [EmpNo],[WorkDate] ,Left(WorkTime,5) as WorkTime,[WorkD' +
        'ateTime] ,[Emp_ID] ,[On_Off]'
      '  FROM [dbo].[UTAtime_entry]'
      '  where Emp_ID=:EmpID'
      '  and WorkDate =:WorkDate'
      '  order by [WorkDateTime]')
    Left = 472
    Top = 296
  end
  object dsTimeByDateEmpID: TDataSource
    DataSet = qryTimeByDateEmpID
    Left = 472
    Top = 368
  end
  object insTimesheetEntry: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'work_emp_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'reg_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'ot_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'dt_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'Emp_Type_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'Work_PC_Code'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [dbo].[timesheet_entry]'
      '([work_emp_id],[work_date],[modified_date],[status],[entry_date]'
      
        ',[entry_date_local] ,[entry_by],[work_start1],[work_stop1],[work' +
        '_start2]'
      
        ',[work_stop2],[work_start3],[work_stop3],[work_start4],[work_sto' +
        'p4]'
      ',[work_start5],[work_stop5],[work_start6],[work_stop6]'
      
        ',[reg_hours],[ot_hours],[dt_hours],[vehicle_use],[Emp_Type_id], ' +
        '[Work_PC_Code],[source]'
      
        ',[pto_hours],[per_diem],[floating_holiday],[callout_hours],[vac_' +
        'hours],[leave_hours]'
      ',[br_hours],[hol_hours],[jury_hours])'
      '     VALUES'
      
        '(:work_emp_id,:work_date,GetDate(),'#39'ACTIVE'#39',GetDate(),GetDate(),' +
        '20,'
      
        ':work_start1,:work_stop1,:work_start2,:work_stop2,:work_start3,:' +
        'work_stop3,'
      
        ':work_start4,:work_stop4,:work_start5,:work_stop5,:work_start6,:' +
        'work_stop6,'
      
        ':reg_hours,:ot_hours, :dt_hours,'#39'NA'#39',:Emp_Type_id,:Work_PC_Code,' +
        ' '#39'UTA'#39','
      '0.00,'#9'  0,'#9#9'0,'#9#9' 0.00,'#9#9' 0.00,'#9#9'0.00,'#9#9'0.00,'#9#9'0.00,'#9#9'0.00'
      ')')
    Left = 384
    Top = 88
  end
  object updActiveToOld: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'WorkDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EmpID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' update [dbo].[timesheet_entry]'
      'Set Status = '#39'OLD'#39
      'where work_date =:WorkDate'
      'and Work_Emp_id = :EmpID')
    Left = 448
    Top = 8
  end
  object ADOConn: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 344
    Top = 16
  end
  object insUTAtimeEntry: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'empno'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'workdate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'worktime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'On_Off'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'USE [QM]'
      ''
      'Declare @EmpNo varchar(6);'
      'Set @EmpNo =:EmpNo'
      'INSERT INTO [dbo].[UTAtime_entry]'
      '           ([EmpNo]'
      '           ,[Emp_ID]'
      '           ,[WorkDate]'
      '           ,[WorkTime]'
      '           ,[On_Off])'
      '     VALUES'
      '           (@EmpNo'
      '           ,(Select dbo.GetActiveEmpID(@EmpNo))'
      '           ,:WorkDate'
      '           ,:WorkTime'
      '           ,:On_Off)'
      '')
    Left = 92
    Top = 104
  end
  object truncateTable: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'truncate table UTAtime_entry')
    Left = 88
    Top = 152
  end
  object OpenDialog1: TOpenDialog
    Left = 40
    Top = 24
  end
end
