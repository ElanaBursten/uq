unit utaJammerMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  System.types, System.IOUtils, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String;
  end; // TLogResults

type
  TfrmUTAJammer = class(TForm)
    qryEmpIDWorkDate: TADOQuery;
    qryTimeByDateEmpID: TADOQuery;
    dsTimeByDateEmpID: TDataSource;
    UATTimeEntryGrid: TDBGrid;
    btnInsert: TButton;
    Memo1: TMemo;
    insTimesheetEntry: TADOQuery;
    updActiveToOld: TADOQuery;
    ADOConn: TADOConnection;
    btnParseFile: TButton;
    insUTAtimeEntry: TADOQuery;
    truncateTable: TADOQuery;
    OpenDialog1: TOpenDialog;
    procedure btnInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnParseFileClick(Sender: TObject);
  private
    LogResult: TLogResults;
    flogDir: wideString;
    connStr: String;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    procedure ProcessEmployeeforWorkDate(WorkDate: TDateTime; EmpID: Integer; CalRules: boolean; ProfitCenter: string;
      EmpType_id: Integer);
    procedure ReadInUATEntry;
    procedure ReadIni;
    function connectDB: boolean;
    function EnDeCrypt(const Value: String): String;
    procedure clearLogRecord;
    procedure LoadCSV(filename: TFileName);
    { Private declarations }
  public
    { Public declarations }
    TimeSheetPath: String;
    ErrPath: String;
    ProcPath: String;
    InPath: String;
    InputFileName: TFileName;
    Logpath: String;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
  end;

var
  frmUTAJammer: TfrmUTAJammer;

implementation

Uses System.DateUtils, StrUtils, System.inifiles;
{$R *.dfm}

procedure TfrmUTAJammer.ReadIni;
var
  filepath: String;
  UTAJammer: TIniFile;
const
  INI_FILE = 'UTAJammer';
begin
  try
    try
      filepath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
      UTAJammer := TIniFile.Create(filepath + INI_FILE + '.ini');
      aServer := UTAJammer.ReadString('Database', 'Server', '');
      aDatabase := UTAJammer.ReadString('Database', 'DB', 'QM');
      ausername := UTAJammer.ReadString('Database', 'UserName', '');
      apassword := UTAJammer.ReadString('Database', 'Password', '');
      aTrusted := UTAJammer.ReadString('Database', 'Trusted', '1');
      Logpath := UTAJammer.ReadString('Paths', 'LogPath', 'C:\QM\Logs');

      ErrPath := UTAJammer.ReadString('Paths', 'ErrPath', '');
      ForceDirectories(ErrPath);
      ProcPath := UTAJammer.ReadString('Paths', 'ProcPath', '');
      ForceDirectories(ProcPath);
      InPath := UTAJammer.ReadString('Paths', 'InPath', '');

      ForceDirectories(Logpath);
    except
      On E: Exception do
      begin
        Raise Exception.Create('Failed to read from Ini file: ' + E.Message);

        LogResult.LogType := ltError;
        LogResult.Status:='ReadIni';
        LogResult.DataStream:= UTAJammer.ToString;
        LogResult.ExcepMsg:= E.Message;
        WriteLog(LogResult);
      end;
    End;
    LogResult.LogType := ltInfo;
    LogResult.Status:='Processed ReadIni';
    WriteLog(LogResult);

  finally
    FreeAndNil(UTAJammer);
  end;
end;

procedure TfrmUTAJammer.ReadInUATEntry;
var
  WorkDate: TDateTime;
  EmpID: Integer;
  CalRules: boolean;
  ProfitCenter: string;
  EmpType_id: Integer;
begin
  try
    try
      EmpID := 0;
      qryEmpIDWorkDate.Open;
      with qryEmpIDWorkDate do
      begin
        first;
        while NOT EOF do
        begin
          WorkDate := FieldByName('WorkDate').AsDateTime;
          EmpID := FieldByName('Emp_ID').AsInteger;
          CalRules := FieldByName('IsCalif').AsBoolean;
          ProfitCenter := FieldByName('ProfitCenter').AsString;
          EmpType_id := FieldByName('type_id').AsInteger;
          ProcessEmployeeforWorkDate(WorkDate, EmpID, CalRules, ProfitCenter, EmpType_id);
          Next;
        end;
      end;
    finally
      qryEmpIDWorkDate.close;
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'ReadInUATEntry';
      LogResult.DataStream := qryEmpIDWorkDate.SQL.Text;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmUTAJammer.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackslash(Logpath) + 'UTAJammer_' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmUTAJammer.btnParseFileClick(Sender: TObject);
  function GetCSV(const PathName: String): String; // InPath
  var
    S: String;
  begin
    for S in TDirectory.GetFiles(IncludeTrailingBackslash(PathName), '*.*', TSearchOption.soTopDirectoryOnly) do
      result := S;
  end;

begin
  try
    InputFileName := GetCSV(InPath);
    truncateTable.ExecSQL;
    LoadCSV(InputFileName);
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Load CSV';
      LogResult.ExcepMsg := E.Message + ' File may not be there or is held open';
      WriteLog(LogResult);
      Application.Terminate;
    end;
  end;
end;

procedure TfrmUTAJammer.LoadCSV(filename: TFileName);
Var
  tFile: TextFile;
  sLine: String;
  empno: string;
  WorkDate: String;
  worktime: String;
  onoff: String;
  seppos: Integer;
  function IsNumber(N: String): boolean;
  var
    I: Integer;
  begin
    result := True;
    if Trim(N) = '' then
      Exit(False);
    for I := 1 to Length(N) do
    begin
      if not(N[I] in ['0' .. '9']) then
      begin
        result := False;
        Break;
      end;
    end;
  end;

begin
  try
    if FileExists(filename) then
    begin
      AssignFile(tFile, filename);
      reset(tFile);

      while not EOF(tFile) do
      begin
        readLn(tFile, sLine);
        sLine:= StringReplace(sLine, #34,'',[rfReplaceAll]);
        empno := Copy(sLine, 0, 6);
        If not IsNumber(empno) then
          continue;
        seppos := pos(',', sLine);

        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        WorkDate := Copy(sLine, 1, seppos - 1);
        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        worktime := Copy(sLine, 1, seppos - 1);
        delete(sLine, 1, seppos);
        seppos := pos(',', sLine);
        onoff := Copy(sLine, 1, seppos - 1);

        Memo1.Lines.AddPair('empno', empno);
        Memo1.Lines.AddPair('workdate', WorkDate);
        Memo1.Lines.AddPair('worktime', worktime);
        Memo1.Lines.AddPair('onoff', onoff);

        insUTAtimeEntry.Parameters.ParamByName('empno').Value := empno;
        insUTAtimeEntry.Parameters.ParamByName('workdate').Value := WorkDate;
        insUTAtimeEntry.Parameters.ParamByName('worktime').Value := worktime;
        insUTAtimeEntry.Parameters.ParamByName('on_off').Value := onoff;
        insUTAtimeEntry.ExecSQL;
      end;
    end
    else
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'LoadCSV';
      LogResult.ExcepMsg := 'Cannnot find any CSV';
      WriteLog(LogResult);
      application.Terminate;
    end;
    CloseFile(tFile);
    Memo1.Lines.Add('File Added');
    MoveFile(PChar(filename), PChar(IncludeTrailingBackslash(ProcPath) + ExtractFileName(filename)));
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Parsed CSV';
    LogResult.DataStream := 'Moved '+ExtractFileName(filename)+' to processed folder';
    WriteLog(LogResult);
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'LoadCSV failed';
      LogResult.ExcepMsg := E.Message  + ' File may not be there or is held open';
      WriteLog(LogResult);
      MoveFile(PChar(filename), PChar(IncludeTrailingBackslash(ErrPath) + ExtractFileName(filename)));
      application.Terminate;
    end;
  end;
end;

procedure TfrmUTAJammer.btnInsertClick(Sender: TObject);
begin
  ReadInUATEntry;
  Application.Terminate;
end;

procedure TfrmUTAJammer.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmUTAJammer.ProcessEmployeeforWorkDate(WorkDate: TDateTime; EmpID: Integer; CalRules: boolean; ProfitCenter: string;
  EmpType_id: Integer);
var
  oWork, fWork: Integer;
  work_start, work_stop: TDateTime;
  TotalHours, RegHours, OThours, DThours: Double;
  cnt: Integer;
begin
  Memo1.Clear;
  oWork := 0;
  fWork := 0;
  TotalHours := 0.0;
  RegHours := 0.0;
  OThours := 0.0;
  DThours := 0.0;
  qryTimeByDateEmpID.close;
  qryTimeByDateEmpID.Parameters.ParamByName('EmpID').Value := EmpID;
  qryTimeByDateEmpID.Parameters.ParamByName('WorkDate').Value := WorkDate;
  qryTimeByDateEmpID.Open;
  cnt  :=  qryTimeByDateEmpID.RecordCount;
  if odd(cnt) then
  begin
    LogResult.LogType := ltError;
    LogResult.Status:='ProcessEmployeeforWorkDate';
    LogResult.DataStream:= 'EmpID: '+IntToStr(EmpID) +' for '+ DateTimeToStr(WorkDate) +' has improper time entry';
    LogResult.ExcepMsg:= 'All Work starts must include work stops.  Not adding to TimesheetEntry table';
    WriteLog(LogResult);
    qryTimeByDateEmpID.close;
    exit;
  end;
  updActiveToOld.Parameters.ParamByName('WorkDate').Value := WorkDate;
  updActiveToOld.Parameters.ParamByName('EmpID').Value := EmpID;
  updActiveToOld.ExecSQL;

  qryTimeByDateEmpID.first;

  work_start := 0.0;
  work_stop := 0.0;

  insTimesheetEntry.Parameters.ParamByName('Work_Stop1').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Stop2').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Stop3').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Stop4').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Stop5').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Stop6').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start1').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start2').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start3').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start4').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start5').Value := NULL;
  insTimesheetEntry.Parameters.ParamByName('Work_Start6').Value := NULL;

  while not qryTimeByDateEmpID.EOF do
  begin
    If qryTimeByDateEmpID.FieldByName('ON_Off').AsString = 'ON' then
    begin
      inc(oWork);
      work_start := qryTimeByDateEmpID.FieldByName('WorkTime').AsDateTime;
      Memo1.Lines.Add('Work_Start' + IntToStr(oWork) + ' ' + qryTimeByDateEmpID.FieldByName('WorkTime').AsString);
      insTimesheetEntry.Parameters.ParamByName('Work_Start' + IntToStr(oWork)).Value :=
        qryTimeByDateEmpID.FieldByName('WorkTime').AsString;
    end;
    If qryTimeByDateEmpID.FieldByName('ON_Off').AsString = 'OFF' then
    begin
      inc(fWork);
      work_stop := qryTimeByDateEmpID.FieldByName('WorkTime').AsDateTime;
      Memo1.Lines.Add('Work_Stop' + IntToStr(fWork) + ' ' + qryTimeByDateEmpID.FieldByName('WorkTime').AsString);
      insTimesheetEntry.Parameters.ParamByName('Work_Stop' + IntToStr(fWork)).Value :=
        qryTimeByDateEmpID.FieldByName('WorkTime').AsString;
      TotalHours := TotalHours + HourSpan(work_start, work_stop);
    end;

    Memo1.Lines.Add('EmpNo: ' + qryTimeByDateEmpID.FieldByName('EmpNo').AsString);
    Memo1.Lines.Add('WorkDate ' + qryTimeByDateEmpID.FieldByName('WorkDate').AsString);
    Memo1.Lines.Add('WorkDateTime ' + qryTimeByDateEmpID.FieldByName('WorkDateTime').AsString);
    qryTimeByDateEmpID.Next;
  end;
  Memo1.Lines.Add('RegHours: ' + FloatToStr(TotalHours));

  if CalRules then
  begin
    if (TotalHours <= 8) then
      RegHours := TotalHours
    else if ((TotalHours > 8) and (TotalHours < 12)) then
    begin
      OThours := TotalHours - 8;
      RegHours := 8;
    end
    else if (TotalHours > 12) then // e.g TotalHours = 15
    begin
      DThours := TotalHours - 12; // 15TotHr -12Non-DT = 3
      OThours := TotalHours - (DThours + 8); // 15TotHr  - (3DThours+8RegTime) = 4
      RegHours := 8;
    end;
  end
  else
    RegHours := TotalHours;
  // TotalHours, RegHours, OThours, DThours
  insTimesheetEntry.Parameters.ParamByName('work_emp_id').Value := EmpID;
  insTimesheetEntry.Parameters.ParamByName('work_date').Value := WorkDate;
  insTimesheetEntry.Parameters.ParamByName('reg_hours').Value := RegHours;
  insTimesheetEntry.Parameters.ParamByName('ot_hours').Value := OThours;
  insTimesheetEntry.Parameters.ParamByName('dt_hours').Value := DThours;
  insTimesheetEntry.Parameters.ParamByName('Emp_Type_id').Value := EmpType_id;
  insTimesheetEntry.Parameters.ParamByName('Work_PC_Code').Value := ProfitCenter;
  insTimesheetEntry.ExecSQL;

end;

function TfrmUTAJammer.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  result := False;
  ADOConn.Connected := False;
  try

    try
      LogConnStr := 'Provider=SQLNCLI11;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
        ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer + ';Trusted_Connection =' + aTrusted + ';';
      if LeftStr(apassword, 4) = DECRYPT then
      begin
        apassword := Copy(apassword, 5, maxint);
        apassword := EnDeCrypt(apassword);
      end
      else
      begin
        LogResult.LogType := ltWarning;
        LogResult.Status:='Connect to DB';
        LogResult.DataStream:= 'You are using a Clear Text Password.  Please contact IT for the correct Password';
        WriteLog(LogResult);
      end;

      connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
        ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer + ';Trusted_Connection =' + aTrusted + ';';
      ADOConn.ConnectionString := connStr;
      ADOConn.Open;
    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.Status:='Connect to DB';
        LogResult.DataStream:= connStr;
        LogResult.ExcepMsg:= E.Message;
        WriteLog(LogResult);
      end;
    end;
  finally
    result := ADOConn.Connected;
  end;
end;

function TfrmUTAJammer.EnDeCrypt(const Value: String): String;
var
  CharIndex: Integer;
begin
  result := Value;
  for CharIndex := 1 to Length(Value) do
    result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TfrmUTAJammer.FormCreate(Sender: TObject);
begin
  ReadIni;
  connectDB;
  if paramCount<1 then
  begin
    btnParseFileClick(Sender);
    btnInsertClick(Sender);
    application.Terminate;
  end;

end;

end.
