object frmUpdateEmpType: TfrmUpdateEmpType
  Left = 0
  Top = 0
  Caption = 'Update Employee Type'
  ClientHeight = 488
  ClientWidth = 1009
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MasterPanel: TPanel
    Left = 0
    Top = 0
    Width = 1009
    Height = 488
    Align = alClient
    TabOrder = 0
    DesignSize = (
      1009
      488)
    object lblImport: TLabel
      Left = 11
      Top = 6
      Width = 60
      Height = 13
      AutoSize = False
      Caption = 'MDR_Import'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 106
      Top = 42
      Width = 118
      Height = 13
      AutoSize = False
      Caption = 'Crtl/Shift+Click to select'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEmployee: TLabel
      Left = 531
      Top = 5
      Width = 60
      Height = 14
      AutoSize = False
      Caption = 'Employee'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Shape1: TShape
      Left = 488
      Top = 0
      Width = 22
      Height = 63
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Brush.Color = clCream
      Pen.Style = psDash
    end
    object MDRImportEmpGrid: TcxGrid
      Left = 1
      Top = 60
      Width = 1007
      Height = 427
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object MDRImportEmpView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.First.Visible = True
        Navigator.Buttons.PriorPage.Visible = True
        Navigator.Buttons.Prior.Visible = True
        Navigator.Buttons.Next.Visible = True
        Navigator.Buttons.NextPage.Visible = True
        Navigator.Buttons.Last.Visible = True
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = True
        Navigator.Buttons.GotoBookmark.Visible = True
        Navigator.Buttons.Filter.Visible = True
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmManual
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCustomDrawCell = MDRImportEmpViewCustomDrawCell
        DataController.DataSource = ImportEmpSource
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelectMode = msmPersistent
        OptionsSelection.ShowCheckBoxesDynamically = True
        OptionsView.NoDataToDisplayInfoText = '<No data to display. Click New to add data.>'
        OptionsView.DataRowHeight = 18
        OptionsView.GroupByBox = False
        object MDRImportEmpViewWorkAssignmentCategory: TcxGridDBColumn
          DataBinding.FieldName = 'WorkAssignmentCategory'
          Width = 134
        end
        object MDRImportEmpViewWorkerNumber: TcxGridDBColumn
          DataBinding.FieldName = 'WorkerNumber'
          Width = 89
        end
        object MDRImportEmpViewworkerid: TcxGridDBColumn
          Caption = 'WorkerID'
          DataBinding.FieldName = 'workerid'
          Width = 71
        end
        object MDRImportEmpViewFirstName: TcxGridDBColumn
          DataBinding.FieldName = 'FirstName'
          Width = 97
        end
        object MDRImportEmpViewLastName: TcxGridDBColumn
          DataBinding.FieldName = 'LastName'
          Width = 95
        end
        object ImportEmployeeSplitter: TcxGridDBColumn
          DataBinding.IsNullValueType = True
          MinWidth = 22
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringWithFindPanel = False
          Options.Focusing = False
          Options.IgnoreTimeForFiltering = False
          Options.IncSearch = False
          Options.FilteringAddValueItems = False
          Options.FilteringFilteredItemsList = False
          Options.FilteringFilteredItemsListShowFilteredItemsOnly = False
          Options.FilteringMRUItemsList = False
          Options.FilteringPopup = False
          Options.FilteringPopupMultiSelect = False
          Options.AutoWidthSizable = False
          Options.ExpressionEditing = False
          Options.GroupFooters = False
          Options.Grouping = False
          Options.HorzSizing = False
          Options.Moving = False
          Options.ShowCaption = False
          Options.Sorting = False
          Width = 22
        end
        object MDRImportEmpViewEmpType: TcxGridDBColumn
          DataBinding.FieldName = 'EmpType'
          Width = 70
        end
        object MDRImportEmpViewemp_number: TcxGridDBColumn
          Caption = 'Emp Number'
          DataBinding.FieldName = 'emp_number'
          Width = 86
        end
        object MDRImportEmpViewemp_id: TcxGridDBColumn
          Caption = 'Emp ID'
          DataBinding.FieldName = 'emp_id'
        end
        object MDRImportEmpViewfirst_name: TcxGridDBColumn
          Caption = 'First Name'
          DataBinding.FieldName = 'first_name'
          Width = 85
        end
        object MDRImportEmpViewlast_name: TcxGridDBColumn
          Caption = 'Last Name'
          DataBinding.FieldName = 'last_name'
          Width = 82
        end
        object MDRImportEmpViewshort_name: TcxGridDBColumn
          Caption = 'Short Name'
          DataBinding.FieldName = 'short_name'
          Width = 110
        end
      end
      object MDRImportEmpGridLevel: TcxGridLevel
        GridView = MDRImportEmpView
      end
    end
    object btnUpdateEmpType: TcxButton
      Left = 294
      Top = 13
      Width = 175
      Height = 24
      Action = Action1
      Anchors = [akRight]
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001B744558745469746C65004E6578743B506C61793B4172726F773B52
        6967687416E40EAE000002AF49444154785EA5925D6895751CC73FCFD14D46BE
        B41AF6C20A870425152C2F82A08B0ABD295D05416F572184045D2644082A425A
        831A62128E2E448C59108D9430D84A1C81DBD4D6B6D05C3B739B5B3BB1EDAC73
        CEF33CFFDF4BCFE1807076DB173E7CF95FFC3E7C2FFE91BB136501D600D556C0
        3F3839EC8262062A86A9A3E2881B224AE9DF949E032F90A396B5877A46C3BEAF
        0687DF3DDABB0B683CBAE7A99C064352458212AA484053218943F55D3BA4969C
        A8B2E7C5C71E3FF7EBFAEFDEEBFAA56FE1D6C4FECFF73E7D1908638BC1821A6A
        10D439DCD58FA8D7092255A3A5B9898EE71F213FBBF9B99F06365D7CFBE0F9AF
        676E0C1FDED6DC70030803B3154B122388616AF582204A2A30B562DCD5BC8137
        3AB6716DEC9ED7FBD76F7C75C3E6B3DD7F0EF67EFCCC834D7380EC78BFD7D457
        2D90E0A46A9412A5583688E081875A78B3F5DEC6CBD75AF636346D7AAB65EBB3
        9DBF5F387EE242D7AE2520D0E59EBBB320552AC1A8A4423938C58A905F4C3302
        AD5BEE67F72BDB373EB1BDFDC0932F7D34D4FE5AE73B4063DD822408A5D4AB20
        6224197195580966AC6B8878B47D0B0F6FBDAFB5E754F205701A48EE082AA540
        A124148A29E6206A04ABC9725184C6C6D0C84D4606AF2FAE14663E03A47E811A
        CB15214E0D7343CC897211EBD6464C8C4F73F5D278BCFCF7ADEEF93F7A3B97A7
        06E680A44E90A6CA526CC4C18822674D2EC7FCC43C570646AD3033F5CD3F13FD
        870AD7CFFD05C480B5ED3CEEAB048195B262EE141796B87A7194DBF97CDFCAF4
        9583B77F3B35045400FDF0C7593F7DE45B4484D50B982B14B3C33126C76F8E94
        1646F74F0F7ED9079401D9F7C3AC03A880AA60AB7EA2A6B1F27DF7F9A9CAE2E4
        91C94B9F9C014A4068DB71CCCD9C339F9EA5DAA686BB634E9DA0FCF38997EFA6
        961848DB761E7337C7CC5071DC0C73C7AB4419A60044EECEFFC97FFDEAC21326
        FC988F0000000049454E44AE426082}
      OptionsImage.Layout = blGlyphRight
      OptionsImage.Spacing = 10
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clTeal
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = btnUpdateEmpTypeClick
    end
    object Button1: TButton
      Left = 3
      Top = 38
      Width = 98
      Height = 19
      Caption = 'Clear Selection'
      TabOrder = 2
      OnClick = Button1Click
    end
    object chkboxImportEmpSearch: TCheckBox
      Left = 531
      Top = 38
      Width = 97
      Height = 16
      Anchors = [akTop, akRight]
      Caption = 'Show Find Panel'
      TabOrder = 3
      OnClick = chkboxImportEmpSearchClick
    end
  end
  object ImportEmpSource: TDataSource
    DataSet = qryImportEmp
    Left = 84
    Top = 196
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 79
    Top = 128
  end
  object qryImportEmp: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Import.WorkAssignmentCategory, Import.WorkerNumber, '
      'Import.WorkerID, Import.FirstName, import.LastName, '
      'refs.code as [EmpType], emp.emp_number,emp.emp_id,'
      'emp.first_name, emp.last_name,  emp.short_name  '
      'from QM.dbo.employee emp'
      'join UQVIEW.dbo.MDR_Import import '
      'on import.WorkerNumber = emp.emp_number'
      'join QM.dbo.reference refs '
      'on emp.type_id = refs.ref_id and refs.type = '#39'emptype'#39
      'where  emp.active = 1 and emp.ad_username is not null '
      'and import.WorkAssignmentCategory <> refs.code'
      'order by emp_id')
    Left = 200
    Top = 200
  end
  object ActionList: TActionList
    Left = 166
    Top = 136
    object Action1: TAction
      Caption = 'Update Employee EmpType'
      OnUpdate = Action1Update
    end
  end
  object UpdateEmpType: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'empnumber'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ' UPDATE QM.dbo.Employee'
      'SET type_id = (select ref_id'
      'from QM.dbo.reference ref where'
      ' ref.code = :code'
      'and ref.type = '#39'emptype'#39')'
      ' where emp_number= :empnumber')
    Left = 248
    Top = 144
  end
end
