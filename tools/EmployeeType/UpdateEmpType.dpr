program UpdateEmpType;

uses
  Vcl.Forms,
  SysUtils,
  UpdateEmpTypeMain in 'UpdateEmpTypeMain.pas' {frmUpdateEmpType},
  GlobalSU in 'GlobalSU.pas';

  {$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
   MyInstanceName := ExtractFileName(Application.ExeName);
   LocalInstance:=MyInstanceName;
    if CreateSingleInstance(MyInstanceName) then
    begin
      Application.Initialize;
      Application.Title := '';
      Application.CreateForm(TfrmUpdateEmpType, frmUpdateEmpType);
  Application.Run;
    end else
      Application.Terminate;
end.





