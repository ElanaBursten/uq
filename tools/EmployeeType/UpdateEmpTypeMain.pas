unit UpdateEmpTypeMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations,
  Data.DB, cxDBData, Data.Win.ADODB, Vcl.ExtCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.StdCtrls, Vcl.Menus, cxButtons, iniFiles, System.Actions,
  Vcl.ActnList;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

type
  TfrmUpdateEmpType = class(TForm)
    ImportEmpSource: TDataSource;
    MasterPanel: TPanel;
    MDRImportEmpGrid: TcxGrid;
    MDRImportEmpView: TcxGridDBTableView;
    MDRImportEmpGridLevel: TcxGridLevel;
    ADOConn: TADOConnection;
    lblImport: TLabel;
    btnUpdateEmpType: TcxButton;
    qryImportEmp: TADOQuery;
    Button1: TButton;
    Label4: TLabel;
    chkboxImportEmpSearch: TCheckBox;
    ActionList: TActionList;
    Action1: TAction;
    UpdateEmpType: TADOQuery;
    ImportEmployeeSplitter: TcxGridDBColumn;
    MDRImportEmpViewWorkAssignmentCategory: TcxGridDBColumn;
    MDRImportEmpViewWorkerNumber: TcxGridDBColumn;
    MDRImportEmpViewworkerid: TcxGridDBColumn;
    MDRImportEmpViewFirstName: TcxGridDBColumn;
    MDRImportEmpViewLastName: TcxGridDBColumn;
    MDRImportEmpViewEmpType: TcxGridDBColumn;
    MDRImportEmpViewemp_number: TcxGridDBColumn;
    MDRImportEmpViewemp_id: TcxGridDBColumn;
    MDRImportEmpViewfirst_name: TcxGridDBColumn;
    MDRImportEmpViewlast_name: TcxGridDBColumn;
    MDRImportEmpViewshort_name: TcxGridDBColumn;
    lblEmployee: TLabel;
    Shape1: TShape;
    procedure btnUpdateEmpTypeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Action1Update(Sender: TObject);
    procedure chkboxImportEmpSearchClick(Sender: TObject);
    procedure MDRImportEmpViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    LogResult: TLogResults;
    flogDir: wideString;
    function connectDB: boolean;
    function ProcessINI: boolean;
    function GetAppVersionStr: string;
    procedure SetScrollBarSize(AGridView: TcxCustomGridView; ASize: Integer);
  public
    { Public declarations }
    procedure clearLogRecord;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmUpdateEmpType: TfrmUpdateEmpType;
  LocalInstance:string;

implementation

{$R *.dfm}



procedure TfrmUpdateEmpType.chkboxImportEmpSearchClick(Sender: TObject);
begin
  if chkboxImportEmpSearch.Checked then
     MDRImportEmpView.Controller.ShowFindPanel
  else
    MDRImportEmpView.Controller.HideFindPanel;
end;

procedure TfrmUpdateEmpType.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmUpdateEmpType.connectDB: boolean;
var
  connStr, connStrLog: String;
begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;

end;

procedure TfrmUpdateEmpType.SetScrollBarSize(AGridView: TcxCustomGridView;
  ASize: Integer);
begin
  with AGridView.Site do
  begin
    if VScrollBar <> nil then
      VScrollBar.Width := ASize;
    if HScrollBar <> nil then
      HScrollBar.Height := ASize;
  end;
  AGridView.LayoutChanged;
end;

procedure TfrmUpdateEmpType.Action1Update(Sender: TObject);
begin
  btnUpdateEmpType.Enabled := MDRImportEmpView.Controller.SelectedRowCount > 0;
end;

procedure TfrmUpdateEmpType.btnUpdateEmpTypeClick(Sender: TObject);
var i: Integer;
  v: variant;
begin
  With MDRImportEmpView do
  begin
    if Controller.SelectedRowCount > 0 then
    begin
      BeginUpdate;
      DataController.BeginLocate;
      for i := 0 to PRED(Controller.SelectedRowCount ) do
      begin
        v := DataController.GetRecordId(Controller.SelectedRecords[I].RecordIndex);
        DataController.DataSet.Locate(DataController.KeyFieldNames, v, []);
        try
          With updateemptype.Parameters do
          begin
             ParamByName('code').Value := qryImportEmp.FieldByName('EmpType').AsString;
             ParamByName('empnumber').Value := qryImportEmp.FieldByName('emp_number').AsString;
          end;
          updateemptype.ExecSQL;
          LogResult.LogType := ltInfo;
          LogResult.Status :=  'Employee Type Update';
          LogResult.DataStream := 'Emp Number ' + qryImportEmp.FieldByName('WorkerNumber').AsString +
            ' Type updated to ' +   qryImportEmp.FieldByName('EmpType').AsString;
           WriteLog(LogResult);
        except
         On E:Exception do
          begin
           LogResult.LogType := ltWarning;
           LogResult.Status :=  'Employee Type Update';
           LogResult.DataStream := 'Employee Number ' + qryImportEmp.FieldByName('WorkerNumber').AsString +
            'type update failure: ' + E.Message;
           WriteLog(LogResult);
          end;
        end;
        DataController.EndLocate;
      end;
      EndUpdate;
      qryImportEmp.Requery();
    end;
  end;
end;

procedure TfrmUpdateEmpType.Button1Click(Sender: TObject);
begin
  MDRImportEmpView.Controller.ClearSelection;
end;

procedure TfrmUpdateEmpType.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  ProcessINI;
  connectDB;
  qryImportEmp.Open;
  SetScrollBarSize(MDRImportEmpView, 12);
end;

function TfrmUpdateEmpType.ProcessINI: boolean;
var
   EmpTypeIni: TIniFile;
begin
  try
    EmpTypeIni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := EmpTypeIni.ReadString('Database', 'Server', '');
    aDatabase := EmpTypeIni.ReadString('Database', 'DB', 'QM');
    ausername := EmpTypeIni.ReadString('Database', 'UserName', '');
    apassword := EmpTypeIni.ReadString('Database', 'Password', '');

    flogDir := EmpTypeIni.ReadString('LogPaths', 'LogPath', '');


    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    EmpTypeIni.Free;
  end;
end;


procedure TfrmUpdateEmpType.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + LocalInstance +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

function TfrmUpdateEmpType.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;


procedure TfrmUpdateEmpType.MDRImportEmpViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  If AViewInfo.Item.Name = 'ImportEmployeeSplitter' then
  ACanvas.Brush.Color := clCream;
end;

end.
