object Form1: TForm1
  Left = 263
  Top = 171
  Width = 871
  Height = 729
  Caption = 'Damage Attachment Fixer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    863
    700)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 49
    Top = 125
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Attach Date Range'
  end
  object Label2: TLabel
    Left = 92
    Top = 11
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'DB Server'
  end
  object Label3: TLabel
    Left = 95
    Top = 35
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Database'
  end
  object Label4: TLabel
    Left = 93
    Top = 59
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Username'
  end
  object Label5: TLabel
    Left = 95
    Top = 83
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Password'
  end
  object FixDatesButton: TButton
    Left = 152
    Top = 184
    Width = 175
    Height = 25
    Caption = 'Fix Upload Dates'
    TabOrder = 0
    OnClick = FixDatesButtonClick
  end
  object BeginDate: TDateTimePicker
    Left = 152
    Top = 120
    Width = 175
    Height = 21
    CalAlignment = dtaLeft
    Date = 38320.6859006018
    Time = 38320.6859006018
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 1
  end
  object EndDate: TDateTimePicker
    Left = 152
    Top = 144
    Width = 175
    Height = 21
    CalAlignment = dtaLeft
    Date = 38320.6859371296
    Time = 38320.6859371296
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 2
  end
  object Server: TEdit
    Left = 152
    Top = 8
    Width = 175
    Height = 21
    TabOrder = 3
    Text = 'QManagerDBTest1'
  end
  object DB: TEdit
    Left = 152
    Top = 32
    Width = 175
    Height = 21
    TabOrder = 4
    Text = 'QMTesting'
  end
  object User: TEdit
    Left = 152
    Top = 56
    Width = 175
    Height = 21
    TabOrder = 5
    Text = 'sa'
  end
  object Pass: TEdit
    Left = 152
    Top = 80
    Width = 175
    Height = 21
    TabOrder = 6
    Text = 'doggy183'
  end
  object Log: TMemo
    Left = 8
    Top = 232
    Width = 842
    Height = 455
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 7
    WordWrap = False
  end
  object Connection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=TestDB;Data Source=localhost'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 344
    Top = 56
  end
  object SetDate: TADOCommand
    Connection = Connection
    Parameters = <>
    Left = 344
    Top = 96
  end
  object NoUploadDate: TADODataSet
    Connection = Connection
    CommandText = 
      'select attachment_id, filename, foreign_type, foreign_id, orig_f' +
      'ilename, '#13#10'  extension, orig_file_mod_date, attach_date, upload_' +
      'date, active'#13#10'from attachment'#13#10'where (attach_date >= :BeginDate)' +
      ' and (attach_date <= :EndDate)'#13#10'  and upload_date is null'#13#10'  and' +
      ' foreign_type=2'
    Parameters = <
      item
        Name = 'BeginDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EndDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 344
    Top = 136
  end
end
