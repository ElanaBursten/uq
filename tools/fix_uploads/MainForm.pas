unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    FixDatesButton: TButton;
    BeginDate: TDateTimePicker;
    EndDate: TDateTimePicker;
    Label1: TLabel;
    Server: TEdit;
    DB: TEdit;
    User: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Pass: TEdit;
    Label5: TLabel;
    Connection: TADOConnection;
    SetDate: TADOCommand;
    Log: TMemo;
    NoUploadDate: TADODataSet;
    procedure FormCreate(Sender: TObject);
    procedure FixDatesButtonClick(Sender: TObject);
  private
    procedure AddLog(const Msg: string);
  end;

var
  Form1: TForm1;

implementation

uses DMu, IdFTP, OdDbUtils, OdAdoUtils, SyncEngineU, DateUtils;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  BeginDate.DateTime := Now - 30;
  EndDate.DateTime := Now + 1;
end;

procedure TForm1.FixDatesButtonClick(Sender: TObject);
var
  FTP: TIdFTP;
  ForeignTypeDir: string;
  YearDir: string;
  IDDir: string;
  ForeignID: Integer;
  ServerFileName: string;
  AttachmentID: Integer;
  Found: Boolean;
  AttachDate: TDateTime;
begin
  DM := TDM.Create(nil);
  DM.Engine.Free;
  DM.Engine := nil;
  DM.Engine := TLocalCache.CreateWithDatabase(DM.Database1);
  Connection.ConnectionString := OdAdoUtils.MakeConnectionString(Server.Text, DB.Text, User.Text, Pass.Text, False);
  Connection.Open;
  NoUploadDate.Parameters.ParamValues['BeginDate'] := BeginDate.DateTime;
  NoUploadDate.Parameters.ParamValues['EndDate'] := EndDate.DateTime;
  NoUploadDate.Open;
  FTP := DM.CreateFTPDownloader; // This pulls in all kinds of garbage, but is a bit easier for this hack...
  try
    AddLog('Connected to FTP server ' + FTP.Host);
    while not NoUploadDate.Eof do begin
      Found := False;
      AttachmentID := NoUploadDate.FieldByName('attachment_id').AsInteger;
      ForeignID := NoUploadDate.FieldByName('foreign_id').AsInteger;
      ServerFileName := NoUploadDate.FieldByName('filename').AsString;
      AttachDate := NoUploadDate.FieldByName('attach_date').AsDateTime;
      ForeignTypeDir := 'Damages';
      YearDir := IntToStr(YearOf(AttachDate));
      IDDir := IntToStr(ForeignID);
      try
        //AddLog('Changing dir to ' + ForeignTypeDir);
        FTP.ChangeDir(ForeignTypeDir);
        //AddLog('Changing dir to ' + YearDir);
        FTP.ChangeDir(YearDir);
        //AddLog('Changing dir to ' + IDDir);
        FTP.ChangeDir(IDDir);
        //AddLog('Checking size of ' + ServerFileName);
        if FTP.Size(ServerFileName) > -1 then begin
          Found := True;
          //AddLog('Found size for ' + ServerFileName);
        end;
      except on E: Exception do
        begin
          AddLog(Format(E.Message + ' - FTP error locating attachment %s (%d) for damage %d', [ServerFileName, AttachmentID, ForeignID]));
        end;
      end;
      if Found then begin
        NoUploadDate.Edit;
        NoUploadDate.FieldByName('upload_date').AsDateTime := EncodeDate(1980, 1, 1);
        NoUploadDate.Post;
        AddLog(Format('Fixed upload date for attachment %d', [AttachmentID]));
      end
      else
        AddLog(Format('Unable to fix upload date for atatchment %s (%d) for damage %d because the file does not exist', [ServerFileName, AttachmentID, ForeignID]));
      //AddLog('Resetting dir back to /a');
      FTP.ChangeDir('/a'); // Hardcoded right now...
      NoUploadDate.Next;
    end;
  finally
    FreeAndNil(FTP);
  end;
end;

procedure TForm1.AddLog(const Msg: string);
begin
  Log.Lines.Add(Msg);
end;

end.
