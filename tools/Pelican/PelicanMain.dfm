object frmPelicanResponder: TfrmPelicanResponder
  Left = 0
  Top = 0
  Caption = 'I Can. You Can.  We All Can With Pelican.'
  ClientHeight = 403
  ClientWidth = 735
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 735
    Height = 346
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnSendResponse: TButton
    Left = 264
    Top = 352
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 1
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 384
    Width = 735
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'SSDS-UTQ-QM-02-DV'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 25
      end
      item
        Width = 80
      end
      item
        Width = 200
      end>
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'get_pending_multi_responses_1_CA_NCA_PELI'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 192
    Top = 200
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 200
    Top = 264
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'respond_to'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to = :respond_to')
    Left = 480
    Top = 192
  end
  object RESTClientTicket: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 592
    Top = 8
  end
  object RESTRequestTicket: TRESTRequest
    Client = RESTClientTicket
    Method = rmPOST
    Params = <
      item
        Kind = pkHTTPHEADER
        name = 'Authorization'
        Options = [poDoNotEncode]
        Value = 
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1h' +
          'cy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRl' +
          'bnRpZmllciI6ImFmZDBlNzhmLWZkNmYtNGVjOS1iZmY2LTEwMzRmYmY2NWJlYiIs' +
          'Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkv' +
          'Y2xhaW1zL25hbWUiOlsidXRpbGlxdWVzdC50ZXN0IiwidXRpbGlxdWVzdC50ZXN0' +
          'Il0sImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVu' +
          'dGl0eS9jbGFpbXMvcm9sZSI6IkRlbGVnYXRlUE9TUiIsIlJhd1JvbGUiOiJVc2Vy' +
          'IiwiUm9sZUhpZXJhcmNoeU51bWJlciI6MTEsImh0dHA6Ly9zY2hlbWFzLnhtbHNv' +
          'YXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL2VtYWlsYWRkcmVzcyI6' +
          'InV0aWxpcXVlc3QudGVzdCIsIkNvbXBhbnlJZCI6IjRjZjAyZjYyLWFhN2YtMTFl' +
          'Yy1iZTE4LTEyOWM4ZmYxOTFmNSIsIkNvbXBhbnlOYW1lIjoiQnJpZ2h0IEhvdXNl' +
          'IE5ldHdvcmtzIC0gQmFrZXJzZmllbGQiLCJGaXJzdE5hbWUiOiJTdGV2ZW4iLCJM' +
          'YXN0TmFtZSI6Ikh1bGwgUG9zcmVzIiwiQ29tcGFueUVtYWlsIjoicnlhbi5saW5k' +
          'c2F5QGNoYXJ0ZXIuY29tIiwiVXNlckVtYWlsIjoibmljay53aGl0ZUB1c2FuLm9y' +
          'ZyIsIkNvbXBhbnlJc0ludGVybmFsIjoiRmFsc2UiLCJDb21wYW55SXNEZWZhdWx0' +
          'IjoiRmFsc2UiLCJDcmVkZW50aWFsT3duZXJJZCI6IjRjZjAyZjYyLWFhN2YtMTFl' +
          'Yy1iZTE4LTEyOWM4ZmYxOTFmNSIsIlJvbGVQZXJtaXNzaW9uIjoie1wiTmFtZVwi' +
          'OlwiRGVsZWdhdGVQT1NSXCIsXCJJZFwiOlwiM2IzNzM3ZDktOGM4Mi00YzIxLTg4' +
          'ZjEtMmQ2NGVlZmM0Mzg2XCIsXCJJc0N1c3RvbVJvbGVcIjp0cnVlLFwiVXNlclNl' +
          'cnZpY2VcIjpbe1wiU2VydmljZU5hbWVcIjpcIlBDQWRtaW5cIixcIlNlcnZpY2VJ' +
          'ZFwiOjExLFwiUm9sZVBlcm1pc3Npb25zXCI6W3tcIk1vZHVsZU5hbWVcIjpcIlNl' +
          'dHRpbmdzOkRlbGVnYXRpb25cIixcIkFjY2Vzc1R5cGVcIjowLFwiSXNEZWxlZ2F0' +
          'ZWRcIjpmYWxzZX0se1wiTW9kdWxlTmFtZVwiOlwiT3BlcmF0aW9uczpFeHBsb3Jl' +
          'WW91ckRhdGFcIixcIkFjY2Vzc1R5cGVcIjoxLFwiSXNEZWxlZ2F0ZWRcIjpmYWxz' +
          'ZX0se1wiTW9kdWxlTmFtZVwiOlwiT3BlcmF0aW9uczpQb3NpdGl2ZVJlc3BvbnNl' +
          'XCIsXCJBY2Nlc3NUeXBlXCI6MSxcIklzRGVsZWdhdGVkXCI6ZmFsc2V9XX0se1wi' +
          'U2VydmljZU5hbWVcIjpcIlBPU1JcIixcIlNlcnZpY2VJZFwiOjEzLFwiUm9sZVBl' +
          'cm1pc3Npb25zXCI6W3tcIk1vZHVsZU5hbWVcIjpcIlBPU1I6VXNlclwiLFwiQWNj' +
          'ZXNzVHlwZVwiOjEsXCJJc0RlbGVnYXRlZFwiOmZhbHNlfSx7XCJNb2R1bGVOYW1l' +
          'XCI6XCJQT1NSOlN1YnNjcmlwdGlvbk5vdGlmaWNhdGlvblwiLFwiQWNjZXNzVHlw' +
          'ZVwiOjEsXCJJc0RlbGVnYXRlZFwiOmZhbHNlfV19LHtcIlNlcnZpY2VOYW1lXCI6' +
          'XCJPbmVDYWxsXCIsXCJTZXJ2aWNlSWRcIjoxMCxcIlJvbGVQZXJtaXNzaW9uc1wi' +
          'OltdfV0sXCJSb2xlVGFnc1wiOltdLFwiVXNlclJvbGVUeXBlc1wiOjV9IiwiTUZB' +
          'S2V5IjoiIiwic3ViIjoidXRpbGlxdWVzdC50ZXN0IiwianRpIjoiOTkyOWIzY2Ut' +
          'OTBkYS00Yjk0LWIwZTQtOTJhZTkwZjNkZTk1IiwiaWF0IjoxNjU5OTkwOTY5LCJy' +
          'ZWZyZXNoX3Rva2VuIjoiMWY4YThjMWIwOThlNGNiYjgxZjViNWQwZmYwOWU2ZTMi' +
          'LCJuYmYiOjE2NTk5ODkxNjgsImV4cCI6MTY2MDA3NzM2OCwiaXNzIjoiUGVsaWNh' +
          'bkNvcnBUb2tlbklzc3VlciIsImF1ZCI6IlBlbGljYW5Db3JwQXBwcyJ9.RUUI4Jk' +
          '-K_o7fJBUZPi8oRaeRGIpXAJDNJgdxWKRbHA'
      end
      item
        Kind = pkREQUESTBODY
        name = 'body'
        Options = [poDoNotEncode]
        ContentType = ctAPPLICATION_JSON
      end>
    Response = RESTResponseTicket
    SynchronizedEvents = False
    Left = 592
    Top = 72
  end
  object RESTResponseTicket: TRESTResponse
    Left = 592
    Top = 136
  end
  object RESTClientToken: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 
      'https://appscapreprod.undergroundservicealert.org/posr/services/' +
      'webservice/api/Token'
    ContentType = 'application/json'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 48
    Top = 16
  end
  object RESTRequestToken: TRESTRequest
    Client = RESTClientToken
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        name = 'body'
        Options = [poDoNotEncode]
        ContentType = ctAPPLICATION_JSON
      end>
    Response = RESTResponseToken
    SynchronizedEvents = False
    Left = 48
    Top = 72
  end
  object RESTResponseToken: TRESTResponse
    ContentType = 'application/json'
    Left = 48
    Top = 160
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 160
    Top = 3
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvTLSv1, sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 271
  end
  object IdLogEvent1: TIdLogEvent
    Left = 400
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 504
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 208
    Top = 64
  end
  object getResponseID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select top 1 [response_id] as ResponseId'
      'from response_log'
      'where Locate_ID = :LocateID'
      'and Call_Center= :CallCenter'
      'order by response_id desc')
    Left = 296
    Top = 216
  end
  object updRelatedResponseID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'responseLogId'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'update responder_multi_queue'
      'set related_response_id = :responseLogId'
      'where locate_id=:LocateID'
      'and respond_to ='#39'att'#39)
    Left = 296
    Top = 280
  end
end
