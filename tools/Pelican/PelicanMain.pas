unit PelicanMain;
//QM-596
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, IdMessage, IdIntercept, IdLogBase, IdLogEvent, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;  //TLogResults


type
  TfrmPelicanResponder = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    delResponse: TADOQuery;
    RESTClientTicket: TRESTClient;
    RESTRequestTicket: TRESTRequest;
    RESTResponseTicket: TRESTResponse;
    RESTClientToken: TRESTClient;
    RESTRequestToken: TRESTRequest;
    RESTResponseToken: TRESTResponse;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    qryEMailRecipients: TADOQuery;
    getResponseID: TADOQuery;
    updRelatedResponseID: TADOQuery;

    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    tokenCustomBody:string;
    fToken:WideString;
    baseURL: string;
    tokenURL: string;
    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;

    FRespondto: string;
    fCallCenter: string;
    FPosturl: string;
    fOC_Code: string;

    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure ProcessRest;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    function GetToken: WideString;
    function GetGuid: WideString;
    procedure SetUpRecipients;
    { Private declarations }
  public
    { Public declarations }
    property OC_Code:string read fOC_Code write fOC_Code;
    property CallCenter: string read fCallCenter write fCallCenter;
    property Respondto: string read FRespondto write FRespondto;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
    property Posturl: string read FPosturl write FPosturl;
  end;

var
  frmPelicanResponder: TfrmPelicanResponder;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmPelicanResponder.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;


procedure TfrmPelicanResponder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmPelicanResponder.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  Respondto := ParamStr(1);
  CallCenter:= ParamStr(2);
  AppVer:='';
  ProcessINI;
  AppVer  := GetAppVersionStr;
  if (Respondto='') or (CallCenter='')  then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'processParams';
    LogResult.Status := 'MISSING Params';
    LogResult.DataStream:='RespondTo: '+ ParamStr(1) + '  CallCenter: '+ParamStr(2);
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'processParams';
    LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
    WriteLog(LogResult);
  end;
  connectDB;
  ProcessRest;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  GetToken;
  SetUpRecipients;
  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

function TfrmPelicanResponder.GetToken:WideString;
begin
  RESTRequestToken.ClearBody;
  RESTClientToken.ContentType := 'application/json';
  RESTRequestToken.AddBody(tokenCustomBody, ctAPPLICATION_JSON);
  RESTRequestToken.Method := TRESTRequestMethod.rmPOST;
  try
    RESTRequestToken.Execute;   //TESTING ONLY
    fToken  := getData(RESTResponseToken.Content, 'token');
  except
    on E: Exception do
    begin
      Memo1.Lines.Add(E.Message);
      LogResult.LogType := ltError;
      LogResult.MethodName:= 'GetToken';
      LogResult.ExcepMsg := E.Message+ ' Body Sent ' + tokenCustomBody;;
      LogResult.Status := IntToStr(RESTResponseToken.StatusCode) + ' ' + RESTResponseToken.StatusText;
      LogResult.DataStream := RESTResponseToken.Content;
      WriteLog(LogResult);
    end;
  end;  //try-except

end;

function TfrmPelicanResponder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmPelicanResponder.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmPelicanResponder.ProcessINI: boolean;
var
  PelicanINI: TIniFile;
  baseCAURL,baseNVURL, tokenCAURL, tokenNVURL:string;


begin
  try
    PelicanINI := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := PelicanINI.ReadString('Database', 'Server', '');
    aDatabase := PelicanINI.ReadString('Database', 'DB', 'QM');
    ausername := PelicanINI.ReadString('Database', 'UserName', '');
    apassword := PelicanINI.ReadString('Database', 'Password', '');

    baseCAURL      := PelicanINI.ReadString('PelicanRest', 'baseCAURL', '');    //QM-596
    baseNVURL      := PelicanINI.ReadString('PelicanRest', 'baseNVURL', '');
    tokenCAURL     := PelicanINI.ReadString('PelicanRest', 'tokenCAURL', '');
    tokenNVURL     := PelicanINI.ReadString('PelicanRest', 'tokenNVURL', '');

    if CallCenter = 'NCA1' then
    begin
      baseURL   := baseCAURL;
      tokenURL  := tokenCAURL;
      OC_Code := PelicanINI.ReadString('EMailAlerts', 'CARecipientsOC_Code', 'NCA1');
    end
    else if CallCenter = 'NVA1' then
    begin
      baseURL   := baseNVURL;
      tokenURL  := tokenNVURL;
      OC_Code := PelicanINI.ReadString('EMailAlerts', 'NVRecipientsOC_Code', 'NVA1');
    end
    else if CallCenter = 'LCA1' then
    begin
      baseURL   := baseCAURL;
      tokenURL  := tokenCAURL;
      OC_Code := PelicanINI.ReadString('EMailAlerts', 'CARecipientsOC_Code', 'LCA1');
    end;


    StatusBar1.Panels[10].Text:= RespondTo;
    flogDir    := PelicanINI.ReadString('LogPaths', 'LogPath', '');

    tokenCustomBody:= PelicanINI.ReadString('RestToken','CustomBody','{"userName": "utiliquest.test","password": "utiliquest.test"}');

    with IdMessage1.From do  //QM-552
    begin
      Address:=PelicanINI.ReadString('EMailAlerts', 'FromAddress', '');
      Domain:=PelicanINI.ReadString('EMailAlerts', 'FromDomain', '');
      Name:=PelicanINI.ReadString('EMailAlerts', 'FromName', '');
      Text:=PelicanINI.ReadString('EMailAlerts', 'FromText', '');
      User:=PelicanINI.ReadString('EMailAlerts', 'FromUser', '');
    end;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    PelicanINI.Free;
  end;
end;

procedure TfrmPelicanResponder.ProcessRest;
begin
 RESTClientToken.BaseURL:=tokenURL;
 memo1.Lines.Add('tokenURL: '+tokenURL);
 RESTClientTicket.BaseURL:=baseURL;
 memo1.Lines.Add('baseURL: '+baseURL);
end;

procedure TfrmPelicanResponder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + RespondTo +'_Pelican-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;
procedure TfrmPelicanResponder.btnSendResponseClick(Sender: TObject);
//locate_id	status	outgoing_status	ticket_format	ticket_id	ticket_number	    client_code	ticket_type	        status_explanation	  api_key	          url
//771092489	M	         010          NCA1	        142039487	2022080400006-000	BHNBFD	    NEW NORM POLY LREQ	Locate area marked  	Bearer	https://appscapre.../webservice/member/LocateCode
const
  DBL_QUOTES = '"';
  PRE_TOKEN = 'Bearer ';
PELICAN_TICKET_POST_REQUEST =
'{ '+
'  "requestNumber": "%s",'+   //"2022080400006-000",
'  "station": "%s",'+    //take from ticket client code
'  "locateCode": "%s",'+  //"010",
'  "reason": "%s",'+    // "this is a UTQ note",
'  "isNoteVisibleToContractor": true,'+
'  "accountNameOfPositiveResponseProvider": "UtiliQuest",'+
'  "trailId": "%s"'+    //59b70a62-1759-11ed-861d-0242ac120002
'} ';
  ELIPSIS='...';
var
  TicketNo:string;
  TicketStatus:string;
  OutStatus:string;
  LocateID:integer;
  PubTicketNote:string;
  URL:string;
  ClientCode:string;
  ResponseBody:string;
  cntGood, cntBad :integer;
  sGuid:Widestring;
  responseLogId:integer;
  revision:string;
  i:integer;
begin
  OutStatus:='';
  TicketNo:='';
  LocateID:=0;
  responseLogId :=0;
  PubTicketNote :='';
  ResponseBody:='';
  URL :='';
  ClientCode:='';
  revision:='';
  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value:= Respondto;
    Parameters.ParamByName('@CallCenter').Value:= CallCenter;
    open;
    while not eof do
    begin
      LocateID      := FieldByName('locate_id').AsInteger;
      TicketNo      := Trim(FieldByName('ticket_number').AsString);
      ClientCode    := Trim(FieldByName('Client_Code').AsString);
      TicketStatus  := Trim(FieldByName('status').AsString);
      OutStatus     := Trim(FieldByName('outgoing_status').AsString);
      PubTicketNote := 'Response by UtiliQuest';
      revision      := Trim(FieldByName('revision').AsString);
      if FieldByName('revision').IsNull or (revision='') then revision := '000';

      TicketNo:=TicketNo+'-'+revision;

      if length(PubTicketNote) > 2000 then
      begin
        PubTicketNote := (Copy(FieldByName('PubLocateNote').AsString,0,1997))+ELIPSIS;
      end;

      sGuid := GetGuid;
      i:= pos(#123,sGuid);
      System.delete(sGuid,i,1);
      i:= pos(#125,sGuid);
      System.delete(sGuid,i,1);

      ResponseBody := format(PELICAN_TICKET_POST_REQUEST, [TicketNo, ClientCode, OutStatus, PubTicketNote, sGuid]);

      if (OutStatus = 'SKIP') then
      begin
        delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
        delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
        memo1.Lines.Add(delResponse.SQL.Text);
        try
         delResponse.ExecSQL;   //commented out for testing
         begin
            LogResult.LogType := ltInfo;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.status:='Skip UQ status TktNbr: '+TicketNo+ ' Locate: '+IntToStr(LocateID);
            WriteLog(LogResult);
         end;
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
        next;
        continue;
      end;

      if ((LeftStr(upperCase(TicketNo), 1) = 'W') or (LeftStr(upperCase(TicketNo), 1) = 'X')) then
      begin
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'X or W tickets';
        LogResult.status:='Skipping UQ status or X, W TktNbr: '+TicketNo+ ' Locate: '+IntToStr(LocateID);
        WriteLog(LogResult);
        next;
        continue;
      end;

      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequestTicket.Params.Clear;
      RESTRequestTicket.AddAuthParameter('Authorization',PRE_TOKEN+fToken,pkHTTPHEADER,[poDoNotEncode]);

      RESTRequestTicket.ClearBody;
      RESTClientTicket.ContentType := 'application/json';
      memo1.Lines.Add('RESTClientTicket.BaseURL:'+RESTClientTicket.BaseURL);
      RESTRequestTicket.AddBody(ResponseBody, ctAPPLICATION_JSON);

      try
        RESTRequestTicket.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseBody;;
          LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +TicketNo;
          LogResult.DataStream := RESTResponseTicket.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponseTicket.StatusCode > 199) and (RESTResponseTicket.StatusCode < 300) then   //good results
      begin
        memo1.Lines.Add('ResponseBody: '+ResponseBody);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket';
        LogResult.DataStream := ResponseBody;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText + ' TicketNo: ' +TicketNo;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);
        // pull back the response_id and write it to ... update multi Q
        if ((upperCase(ClientCode)= 'NEVBEL')
        or (upperCase(ClientCode) = 'ATTDNEVADA') //qm-770 sr
        or (upperCase(ClientCode) = 'PACBEL')
        or (upperCase(ClientCode) = 'ATTDNORCAL') //qm-770 sr

        ) then
        begin
          getResponseID.Parameters.ParamByName('CallCenter').Value := CallCenter;
          getResponseID.Parameters.ParamByName('LocateID').Value := LocateID;
          getResponseID.Open;
          responseLogId:=getResponseID.FieldByName('ResponseId').AsInteger;
          getResponseID.close;

          updRelatedResponseID.Parameters.ParamByName('LocateID').Value := LocateID;
          updRelatedResponseID.Parameters.ParamByName('responseLogId').Value := responseLogId;
          updRelatedResponseID.ExecSQL;

          delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
          delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
          memo1.Lines.Add(delResponse.SQL.Text);

          try
           delResponse.ExecSQL;   //comment out for testing
          except on E: Exception do
            begin
              LogResult.LogType := ltError;
              LogResult.MethodName := 'delResponse';
              LogResult.DataStream := delResponse.SQL.Text;
              LogResult.ExcepMsg:=e.Message;
              WriteLog(LogResult);
            end;
          end;
        end
        else
        begin
          delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
          delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
          memo1.Lines.Add(delResponse.SQL.Text);

          try
           delResponse.ExecSQL;   //comment out for testing
          except on E: Exception do
            begin
              LogResult.LogType := ltError;
              LogResult.MethodName := 'delResponse';
              LogResult.DataStream := delResponse.SQL.Text;
              LogResult.ExcepMsg:=e.Message;
              WriteLog(LogResult);
            end;
          end;
        end;
      end  //StatusCode > 199 <300   good
      else if RESTResponseTicket.StatusCode > 399 then    //bad results
      begin
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        inc(cntBad);

        //Add EMail notices to all Delphi Responders
        IdMessage1.Body.Add('Failed request no '+IntToStr(cntBad)+' at '+formatdatetime('yyyy-mm-dd hh:mm:ss', Now));
        IdMessage1.Body.Add(IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +TicketNo);
        IdMessage1.Body.Add('Sent '+ResponseBody);
        IdMessage1.Body.Add('Returned ' + RESTResponseTicket.Content);
        IdMessage1.Body.Add('-----------------------------------------------------');


        LogResult.LogType := ltError;
        LogResult.MethodName := 'Deleting Status '+IntToStr(RESTResponseTicket.StatusCode);
        LogResult.ExcepMsg := 'ResponseBody '+ResponseBody;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +TicketNo;
        LogResult.DataStream := 'Returned ' + RESTResponseTicket.Content;
        WriteLog(LogResult);
        AddResponseLogEntry(insertResponseLog);

        if ((upperCase(ClientCode)='NEVBEL') or (upperCase(ClientCode) = 'PACBEL')) then
        begin   //these tickets require
          getResponseID.Parameters.ParamByName('CallCenter').Value := CallCenter;
          getResponseID.Parameters.ParamByName('LocateID').Value := LocateID;
          getResponseID.Open;
          responseLogId:=getResponseID.FieldByName('ResponseId').AsInteger;
          getResponseID.close;

          updRelatedResponseID.Parameters.ParamByName('LocateID').Value := LocateID;
          updRelatedResponseID.Parameters.ParamByName('responseLogId').Value := responseLogId;
          updRelatedResponseID.ExecSQL;

          if (ansipos('CANCELLED', uppercase(RESTResponseTicket.Content))>0) or  //qm=673
             (ansipos('JOBCLOSED', uppercase(RESTResponseTicket.Content))>0) or  //qm=673
             (ansipos('JOBEXPIRED', uppercase(RESTResponseTicket.Content))>0)   //qm=734
          then
          begin
            delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
            delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
            delResponse.ExecSQL;
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream:=  RESTResponseTicket.Content;
            LogResult.Status:=RESTResponseTicket.StatusText;
            WriteLog(LogResult);
          end;
        end
        else
        begin  //if not 'ATTDNEVADA' or 'ATTDNORCAL' still test for Cancels  //qm-770 sr
          if (ansipos('CANCELLED', uppercase(RESTResponseTicket.Content))>0) or  //qm=673
             (ansipos('JOBCLOSED', uppercase(RESTResponseTicket.Content))>0) or  //qm=673
             (ansipos('JOBEXPIRED', uppercase(RESTResponseTicket.Content))>0)   //qm=734
          then
          begin
            delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
            delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
            delResponse.ExecSQL;
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream:=  RESTResponseTicket.Content;
            LogResult.Status:=RESTResponseTicket.StatusText;
            WriteLog(LogResult);
          end;
        end;
      end //StatusCode = 403    bad and delete
      else
      begin  // if an exception is thrown that is not a 400 status.  They are logged and left in the multi queue
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg :=  'ResponseBody '+ResponseBody;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +TicketNo;
        LogResult.DataStream := 'Returned ' + RESTResponseTicket.Content;
        WriteLog(LogResult);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponseTicket.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF

  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

  if cntBad>0 then
  begin
    idSMTP1.Connect;
    idSMTP1.Send(IdMessage1);
    idSMTP1.Disconnect();

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Sending email';
    LogResult.Status := 'Some responses failed';
    LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
    WriteLog(LogResult);
  end;

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmPelicanResponder.SetUpRecipients;
begin
  try
    idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
    qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
    qryEMailRecipients.Open;
    if not qryEMailRecipients.eof then
    idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
    else
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SetUpRecipients';
      LogResult.Status:= 'No recipients to send email to';
      WriteLog(LogResult);
   end;
  finally
    qryEMailRecipients.close;
  end;
end;

procedure TfrmPelicanResponder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmPelicanResponder.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog  := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;


function TfrmPelicanResponder.GetGuid:WideString;
var
  MyGuid1 :TGuid;
begin
  if CreateGUID(MyGuid1) <> 0 then
     Memo1.Lines.Add('Creating GUID failed!')
  else
  begin
    result:= GUIDToString(MyGuid1);
     Memo1.Lines.Add('The generated guid is: ' + result);
  end;

end;

end.
