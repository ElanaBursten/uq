object frmEmerRequeue: TfrmEmerRequeue
  Left = 0
  Top = 0
  Caption = '-- Emergency Requeue'
  ClientHeight = 349
  ClientWidth = 622
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 311
    Width = 45
    Height = 13
    Caption = 'database'
  end
  object Label2: TLabel
    Left = 488
    Top = 312
    Width = 35
    Height = 13
    Caption = 'version'
  end
  object Label3: TLabel
    Left = 176
    Top = 312
    Width = 49
    Height = 13
    Caption = 'processed'
  end
  object Label4: TLabel
    Left = 280
    Top = 312
    Width = 51
    Height = 13
    Caption = 'succeeded'
  end
  object Label5: TLabel
    Left = 368
    Top = 312
    Width = 26
    Height = 13
    Caption = 'failed'
  end
  object btnRun: TButton
    Left = 264
    Top = 279
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 0
    OnClick = btnRunClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 622
    Height = 265
    Align = alTop
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 330
    Width = 622
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object BitBtn1: TBitBtn
    Left = 539
    Top = 271
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 3
  end
  object cbTestMode: TCheckBox
    Left = 8
    Top = 283
    Width = 97
    Height = 17
    Hint = 'When checked, the ticket WILL NOT be written '
    Caption = 'Test mode'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
  end
  object ADOConn: TADOConnection
    CommandTimeout = 60
    ConnectionTimeout = 30
    DefaultDatabase = 'QM'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 40
    Top = 32
  end
  object qryTicketAck: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'Use QM;'
      
        'select distinct ta.[ticket_id] ,t.ticket_number, ta.[insert_date' +
        '], t.work_city, t.work_state,'
      'ta.[locator_emp_id],ta.[ticket_version_id], E.[short_name]'
      'from ticket_ack ta WITH (NOLOCK)'
      'join ticket t WITH (NOLOCK) on  (ta.ticket_id = t.ticket_id)'
      'join locate L WITH (NOLOCK) on  (t.ticket_id = L.ticket_id)'
      'left outer join employee E on (E.[emp_id] = L.assigned_to_id)'
      'where DATEDIFF ( hh , ta.insert_date ,GetDate() )<=24'
      'and DATEDIFF ( hh , ta.ack_date ,GetDate() )>1.5'
      'and  has_ack = 1'
      'and t.kind  <> '#39'DONE'#39
      'and t.ticket_type like '#39'%EMER%'#39
      'and L.status in ('#39'-R'#39', '#39'ACK'#39')'
      'and (ta.ticket_id not in (select ticket_id'
      #9#9#9#9#9#9'from ticket_ack ta2 WITH (NOLOCK)'
      #9#9#9#9#9#9'where  DATEDIFF ( hh , ta2.insert_date ,GetDate() )<=24'
      #9#9#9#9#9#9'and  has_ack = 0 ))'
      ''
      'and (ta.ticket_id not  in (select ja.ticket_id'
      #9#9#9#9#9#9'   from [dbo].[jobsite_arrival] ja WITH (NOLOCK)'
      #9#9#9#9#9#9'   where ja.ticket_id =  ta.ticket_id))'
      'and t.ticket_format not in ('#39'NCA1'#39','#39'SCA1'#39')')
    Left = 120
    Top = 32
  end
  object spInsTicketAck: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'insert_ticket_ack'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 120
    Top = 96
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = ' qmanager@utiliquest.com '
        Name = 'qmanager'
        Text = 'qmanager <" qmanager"@utiliquest.com >'
        Domain = 'utiliquest.com '
        User = ' qmanager'
      end>
    From.Address = ' qmanager@utiliquest.com '
    From.Name = 'qmanager'
    From.Text = 'qmanager <" qmanager"@utiliquest.com >'
    From.Domain = 'utiliquest.com '
    From.User = ' qmanager'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 566
    Top = 208
  end
  object IdLogEvent1: TIdLogEvent
    Active = True
    Left = 566
    Top = 104
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 566
    Top = 160
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 504
    Top = 107
  end
  object spGetNotificationEmail: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'GetNotificationEmail'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmpID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmailType'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    Left = 528
    Top = 48
  end
  object qryGetTicketImage: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Use QM;'
      'select image'
      'from ticket'
      'where ticket_id = :ticketID'
      '')
    Left = 120
    Top = 168
  end
end
