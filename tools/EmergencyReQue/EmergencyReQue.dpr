program EmergencyReQue;
{QM-377 This tool will re-queue emergency tickets if the locator has not responded in 2 hours}
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
 //qm-1064 takes ticket_format and Time After transmit_date hours
uses
  Vcl.Forms,
  Windows,
  SysUtils,
  requeueMain in 'requeueMain.pas' {frmEmerRequeue},
  GlobalSU in '..\..\common\GlobalSU.pas';

var
  MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    if ParamCount = 0 then Application.ShowMainForm := False;
    Application.CreateForm(TfrmEmerRequeue, frmEmerRequeue);
    reportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Run;
  end
  else
    Application.Terminate;
end.
