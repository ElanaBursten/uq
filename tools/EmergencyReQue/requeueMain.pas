unit requeueMain;
// QM-377  1054 1063  1064

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdSSL, IdSSLOpenSSL, IdIntercept, IdLogBase, IdLogEvent, IdBaseComponent,
  IdMessage, Vcl.Buttons, Vcl.ComCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;

type
  TfrmEmerRequeue = class(TForm)
    ADOConn: TADOConnection;
    qryTicketAck: TADOQuery;
    btnRun: TButton;
    Memo1: TMemo;
    spInsTicketAck: TADOStoredProc;
    IdMessage1: TIdMessage; // qm-1054 sr
    IdLogEvent1: TIdLogEvent; // qm-1054 sr
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL; // qm-1054 sr
    IdSMTP1: TIdSMTP;
    StatusBar1: TStatusBar;
    BitBtn1: TBitBtn;
    cbTestMode: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    spGetNotificationEmail: TADOStoredProc;
    qryGetTicketImage: TADOQuery; // qm-1054 sr
    procedure btnRunClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    LogPath: string;
    LogResult: TLogResults;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    sBaseURL: string;

    FromAddress: string;
    FromName: string;
    FromDomain: string;
    FromUser: string;

    smtpHost: string;
    smtpPort:integer;
    eMailType: string;
    procedure ProcessTicketAcks;
    function ProcessINI: boolean;
    procedure clearLogRecord;
    function GetAppVersionStr: string;
    procedure WriteLog;
    function connectDB: boolean;
    procedure ConfigureSMTP;
    { Private declarations }
  public
    { Public declarations }
    APP_VER: string;

  end;

var
  frmEmerRequeue: TfrmEmerRequeue;

implementation

uses
  Inifiles, System.StrUtils, System.DateUtils;
{$R *.dfm}

procedure TfrmEmerRequeue.btnRunClick(Sender: TObject);
begin
  ProcessTicketAcks;
end;

procedure TfrmEmerRequeue.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:= caFree;
  ADOConn.Close;
end;

procedure TfrmEmerRequeue.FormCreate(Sender: TObject);
begin
  APP_VER := GetAppVersionStr;
  StatusBar1.Panels[4].Text:=APP_VER;
  ProcessINI;
  connectDB;
  StatusBar1.Panels[0].Text:=aServer;
  ConfigureSMTP;
  if ParamCount = 0 then
  begin
    ProcessTicketAcks;
    Application.Terminate;
  end;
end;

procedure TfrmEmerRequeue.ProcessTicketAcks;
var
  ticketId, locatorEmpId, ticketVersionId: integer;
  insertDate: TDateTime;
  ticketNo, WkCity, WkState: string; // t.work_city, t.work_state,
  processed, failed: integer;
  emerRequeueEmail, shortName, tktImage: string; // qm-1054 1063 sr
const // ticket_no        city/st
  MAIL_MSG = 'Emergency Ticket %s was requeued in %s, %s. Locator assigned: %s';
begin
  try
    processed := 0;
    failed := 0;
    try
      with qryTicketAck do
      begin
        Open;
        while not Eof do
        begin
          ticketNo := '';
          emerRequeueEmail := ''; // qm-1054 sr
          WkCity := '';
          WkState := '';
          tktImage :='';   // qm-1063 sr
          ticketId := FieldByName('ticket_id').AsInteger;

          locatorEmpId := FieldByName('locator_emp_id').AsInteger;
          ticketVersionId := FieldByName('ticket_version_id').AsInteger;
          insertDate := FieldByName('insert_date').AsDateTime;


          try
            qryGetTicketImage.Parameters.ParamByName('ticketID').Value:=ticketId;
            qryGetTicketImage.Open;
            tktImage := qryGetTicketImage.FieldByName('image').AsString;  // qm-1063 sr
          finally
            qryGetTicketImage.close;
          end;

          spGetNotificationEmail.Parameters.ParamByName('@EmpID').Value:=locatorEmpId; //@EmailType
          spGetNotificationEmail.Parameters.ParamByName('@EmailType').Value:=eMailType;
          spGetNotificationEmail.Open;
          emerRequeueEmail := spGetNotificationEmail.FieldByName('email_destination').AsString;
          // qm-1054 sr
          if not cbTestMode.Checked then  //if not test mode
          begin
            SPinsTicketAck.Parameters.ParamByName('@TicketID').Value := ticketId;
            SPinsTicketAck.ExecProc;     //comment for test
          end
          else
          begin
            LogResult.LogType := ltInfo;
            LogResult.MethodName := 'ProcessTicketAcks';
            LogResult.Status := 'TEST MODE';
            LogResult.DataStream := IntToStr(ticketId) + ' Requeued test';
            WriteLog;
          end;

          WkCity := FieldByName('work_city').AsString;
          WkState := FieldByName('work_state').AsString;
          ticketNo := FieldByName('ticket_number').AsString;
          shortName := FieldByName('short_name').AsString;
          IdMessage1.ClearBody;
          IdMessage1.Subject := ExtractFileName(Application.ExeName) + ' for '
            + ticketNo;

          if emerRequeueEmail <> '' then
          begin
            IdMessage1.Recipients.EMailAddresses := emerRequeueEmail;
            IdMessage1.Body.Add(Format(MAIL_MSG, [ticketNo, WkCity, WkState,
              shortName])); // qm-1054
            try
              idSMTP1.Connect;
              idSMTP1.Send(IdMessage1);
              idSMTP1.Disconnect();
            except
              on E: Exception do
              begin
                LogResult.LogType := ltError;
                LogResult.MethodName := 'emerRequeueEmail';
                LogResult.DataStream := 'Body: ' +
                  (Format(MAIL_MSG,[ticketNo, WkCity, WkState, shortName])); // qm-1054
                LogResult.ExcepMsg := E.Message;
                WriteLog;
              end;
            end;
          end;

          inc(processed);
          Memo1.Lines.Add(IntToStr(processed) + ' Requeued ' +
            IntToStr(ticketId));
          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'ProcessTicketAcks';
          LogResult.DataStream := IntToStr(processed) + ' Requeued ' +
            IntToStr(ticketId);
          WriteLog;
          next;
        end; // while
      end; // with
    except
      on E: Exception do
      begin
        inc(failed);
        Memo1.Lines.Add(IntToStr(processed) + ' Requeued failed for ' +
          IntToStr(ticketId));
        LogResult.LogType := ltError;
        LogResult.MethodName := 'ProcessTicketAcks';
        LogResult.DataStream := IntToStr(processed) + ' Requeued failed for ' +
          IntToStr(ticketId);
        LogResult.ExcepMsg := E.Message;
        WriteLog;
      end;
    end;

  finally
    qryTicketAck.Close;

    statusbar1.Panels[1].Text:=intToStr(processed);
    statusbar1.Panels[2].Text:=intToStr(processed - failed);
    statusbar1.Panels[3].Text:=intToStr(failed);

    Memo1.Lines.Add(IntToStr(processed) + ' processed');
    Memo1.Lines.Add(IntToStr(processed - failed) + ' succeeded');
    Memo1.Lines.Add(IntToStr(failed) + ' failed');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Process Complete';
    LogResult.Status := IntToStr(processed - failed) +
      ' Successfully processed and ' + IntToStr(failed) + ' Failure(s)';
    WriteLog;

  end;

end;

procedure TfrmEmerRequeue.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackslash(LogPath) + 'EmerReQueLog' + '_' +
    FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
    WriteLn(myFile, 'EmerReQue Version: ' + APP_VER);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmEmerRequeue.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmEmerRequeue.connectDB: boolean;
var
  connStr: String;
begin
  ADOConn.Close;
  ADOConn.ConnectionString := '';
  result := false;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
    ADOConn.Open;
    result := True;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
    end;

  except
    on E: Exception do
    begin
      result := false;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStr;
      LogResult.ExcepMsg := E.Message;
      WriteLog;
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;


function TfrmEmerRequeue.ProcessINI: boolean;
var
  EmerRequeueIni: TIniFile;
  myPath: string;
begin
  result := True;
  try
    myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
    try
      EmerRequeueIni := TIniFile.Create(myPath + 'EmerRequeue.ini');
      LogPath := EmerRequeueIni.ReadString('LogPaths', 'LogPath', '');
      ForceDirectories(LogPath);

      //Database
      aServer := EmerRequeueIni.ReadString('Database', 'Server', '');
      aDatabase := EmerRequeueIni.ReadString('Database', 'DB', 'QM');
      ausername := EmerRequeueIni.ReadString('Database', 'UserName', '');
      apassword := EmerRequeueIni.ReadString('Database', 'Password', '');

      //smtp
      FromAddress:= EmerRequeueIni.ReadString('SMTP', 'FromAddress', 'qmanager@utiliquest.com');
      FromName:= EmerRequeueIni.ReadString('SMTP', 'FromName', 'qmanager');
      FromDomain:= EmerRequeueIni.ReadString('SMTP', 'FromDomain', 'utiliquest.com');
      FromUser:= EmerRequeueIni.ReadString('SMTP', 'FromUser', 'qmanager');
      smtpHost:= EmerRequeueIni.ReadString('SMTP', 'smtpHost', 'smtp.dynutil.com');
      smtpPort:= EmerRequeueIni.Readinteger('SMTP', 'smtpPort', 25);

      //eMailChain
      eMailType:= EmerRequeueIni.ReadString('eMailChain', 'eMailType', 'time_warning');

      LogResult.LogType := ltInfo;
      LogResult.Status := 'Ini processed';
      LogResult.MethodName := 'ProcessINI';
      WriteLog;
    finally
      FreeAndNil(EmerRequeueIni);
    end;
  except
    on E: Exception do
    begin
      result := false;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ProcessINI';
      LogResult.ExcepMsg := E.Message;
      WriteLog;
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

procedure TfrmEmerRequeue.ConfigureSMTP;
begin
  IdSMTP1.Host:= smtpHost;
  IdSMTP1.Port:= smtpPort;

  IdMessage1.From.Address:=FromAddress;
  IdMessage1.From.Domain :=FromDomain;
  IdMessage1.From.User :=FromUser;
  IdMessage1.From.Name :=FromName;
end;


function TfrmEmerRequeue.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;
end.
