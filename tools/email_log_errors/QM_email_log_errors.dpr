program QM_email_log_errors;

// Packages: rtl;vcl

{$APPTYPE CONSOLE}

{$R '..\..\QMIcon.res'}
{$R '..\..\QMVersion.res' '..\..\QMVersion.rc'}

uses
  SysUtils,
  Classes,
  Windows,
  IniFiles,
  IdMessage,
  IdSMTP,
  IdText,
  IdStack,
  IdGlobalProtocols,
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdExceptions in '..\..\common\OdExceptions.pas',
  OdInternetUtil in '..\..\common\OdInternetUtil.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas';

type
  TEmailLogErrors = class (TComponent)
    private
      FStripTime,           // True to strip time and 2 numeric fields from beginning of error lines
      FSendZeroErrors,      // True to send an email even if 0 errors would found for the date range
      FShowZeroSummaries:   // True if summaries with a count of 0 should be included in the output
        Boolean;
      FCountErrorsTotal,    // Total number of error lines found
      FCountErrorsIgnored:  // Number of error lines that were ignored due to filtering
        Integer;
      FReportDateFrom,      // Start date to generate the report for
      FReportDateTo:        // End date to generate the report for
        TDateTime;
      FLogPath,             // Path where server log files are kept
      FEmailTo,             // Email address(es) to send the message to
      FIniFileName: string;
      FIgnore,              // Contains fiter stings to indicate which errors should be ignored
      FErrors,              // List of all error lines read in from log files
      FMsgASCII,            // Email message body in ASCII text format
      FSummaryTitles,       // Titles for summary counters
      FSummarySearch:       // Search text for each summary counter
        TStringList;
      FSummaryCount:        // Found counters for summaries
        array of Integer;

      function ErrorCount_Get: Integer;

    protected
      procedure Clear;

      procedure Write(const Txt: string); overload;
      procedure Write(const Txt: string; Tag: string); overload;
      procedure WriteBR(const Txt: string = '');
      procedure WriteNameValue(const Name, Value: string);
      procedure WriteException(const E: Exception; const Where: string);

      procedure AddSummary(const Title, SearchText: string);

      procedure IniRead(const IniFileName: string);
      procedure FileRead(const LogFileName: string);
      function  FileLineParse(Line: string): Boolean;

      procedure ReadLogFiles;
      procedure OutputSummary;
      procedure OutputDuplicates;
      procedure OutputErrors;
      procedure SendEmail;

      property Ignore: TStringList read FIgnore;
      property Errors: TStringList read FErrors;

      property ReportDateFrom: TDateTime read FReportDateFrom;
      property ReportDateTo: TDateTime read FReportDateTo;
      property LogPath: string read FLogPath;
      property ErrorCount: Integer read ErrorCount_Get;
      property StripTime: Boolean read FStripTime;
      property SendZeroErrors: Boolean read FSendZeroErrors;
      property ShowZeroSummaries: Boolean read FShowZeroSummaries;

    public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;

      procedure Execute(const DateFrom, DateTo: TDateTime; const LogPath: string);
    end;

procedure GetFileTimes(const FileName: string; var Created, Accessed, Modified: TDateTime);
var
  h: THandle;
  Info1, Info2, Info3: TFileTime;
  SysTimeStruct: SYSTEMTIME;
  TimeZoneInfo: TTimeZoneInformation;
  Bias: Double;
begin
  Bias   := 0;
  h      := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
  if h > 0 then begin
    try
      if GetTimeZoneInformation(TimeZoneInfo) <> $FFFFFFFF then
        Bias := TimeZoneInfo.Bias / 1440; // 60x24
      GetFileTime(h, @Info1, @Info2, @Info3);
      if FileTimeToSystemTime(Info1, SysTimeStruct) then
        Created := SystemTimeToDateTime(SysTimeStruct) - Bias;
      if FileTimeToSystemTime(Info2, SysTimeStruct) then
        Accessed := SystemTimeToDateTime(SysTimeStruct) - Bias;
      if FileTimeToSystemTime(Info3, SysTimeStruct) then
        Modified := SystemTimeToDateTime(SysTimeStruct) - Bias;
    finally
      FileClose(h);
    end;
  end;
end;

function ParamValue(ParamName: string; const DefaultValue: string = ''): string;
var
  Count: LongInt;
  Param: string;
begin
  Result := UpperCase(Trim(DefaultValue));
  ParamName := '/' + UpperCase(Trim(ParamName)) + '=';
  for Count := 1 to ParamCount do begin
    Param := ParamStr(Count);
    if (Pos(ParamName, UpperCase(Param)) = 1)
      then Result := Copy(Param, Length(ParamName)+1, Length(Param));
  end;
end;

function DeleteFirstToken(const ErrorLine: string): string;
var
  Position: Integer;
begin
  Result := ErrorLine;
  Position := Pos(' ', Result);
  if (Position > 0) then
    Result := Trim(Copy(Result, Position + 1, MaxInt));
end;

function LeftPadNumber(const Value, StrWidth: Integer): string;
begin
  Result := IntToStr(Value);
  Result := StringOfChar(' ', StrWidth - Length(Result)) + Result;
end;

function SortDescending(List: TStringList; Index1, Index2: Integer): Integer;
begin
  Result := AnsiCompareText(List.Strings[Index2], List.Strings[Index1]);
end;

function SortErrorMessages(List: TStringList; Index1, Index2: Integer): Integer;
begin
  Result := AnsiCompareText(
    DeleteFirstToken(DeleteFirstToken(DeleteFirstToken(List.Strings[Index1]))),
    DeleteFirstToken(DeleteFirstToken(DeleteFirstToken(List.Strings[Index2])))
  );
end;

function StringToLen(const Str: string; const Len: Integer; const PadChar: Char = '.'):string;
begin
  Result := Copy(Str, 1, Len);
  Result := Result + StringOfChar(PadChar, Len - Length(Result));
end;


//-----------------------------------------------------------------------------
// CLASS: TEmailLogErrors
//-----------------------------------------------------------------------------

// Public methods
constructor TEmailLogErrors.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FIgnore   := TStringList.Create;
  FErrors   := TStringList.Create;
  FMsgASCII := TStringList.Create;
  FSummaryTitles := TStringList.Create;
  FSummarySearch := TStringList.Create;
  FSummaryCount := nil;
end;

destructor TEmailLogErrors.Destroy;
begin
  FreeAndNil(FIgnore);
  FreeAndNil(FErrors);
  FreeAndNil(FMsgASCII);
  FreeAndNil(FSummaryTitles);
  FreeAndNil(FSummarySearch);
  FSummaryCount := nil;
  inherited Destroy;
end;

procedure TEmailLogErrors.Execute(const DateFrom, DateTo: TDateTime; const LogPath: string);
begin
  Clear;
  if (ParamCount = 1) and (ParamStr(1) = '/v') then begin
    WriteLn('Version: ' + AppVersionShort);
    Exit;
  end;

  FIniFileName := ChangeFileExt(ParamStr(0), '.ini');
  IniRead(FIniFileName);

  FLogPath := IncludeTrailingPathDelimiter(Trim(LogPath));
  FReportDateFrom := Trunc(DateFrom);
  FReportDateTo   := Trunc(DateTo);

  // Output to console
  WriteLn('Email logged QManager server errors');
  if (ReportDateFrom = ReportDateTo) then
    WriteLn(' - When........... ' + IsoDateToStr(ReportDateFrom))
  else begin
    WriteLn(' - When From...... ' + IsoDateToStr(ReportDateFrom));
    WriteLn(' - When To........ ' + IsoDateToStr(ReportDateTo));
  end;
  WriteLn(' - Log Path....... ' + LogPath);
  WriteLn(' - Ignore Count... ' + IntToStr(Ignore.Count));
  WriteLn;

  // Output to message body
  if (ReportDateFrom = ReportDateTo) then
    Write('QManager Logged Errors for ' + FormatDateTime('dddd, mmmm d, yyyy',ReportDateFrom),'h1')
  else
    Write('QManager Logged Errors from ' + FormatDateTime('dddd, mmmm d, yyyy',ReportDateFrom) + ' to ' + FormatDateTime('dddd, mmmm d, yyyy',ReportDateTo),'h1');
  WriteBR;

  // Perform all required tasks
  ReadLogFiles;

  WriteLn('Found ' + IntToStr(ErrorCount) + ' Total Errors');
  WriteLn;

  OutputSummary;
  OutputDuplicates;
  OutputErrors;
  SendEmail;

  // Output to console
  WriteLn;
  WriteLn('Processing complete');
end;

// Protected methods
procedure TEmailLogErrors.Clear;
begin
  FErrors.Clear;
  FMsgASCII.Clear;
end;

procedure TEmailLogErrors.Write(const Txt: string);
begin
  FMsgASCII.Add(Txt);
end;

procedure TEmailLogErrors.Write(const Txt: string; Tag: string);
begin
  Tag := Trim(Tag);
  Assert(Tag <> '');
  // Formerly used Tag as the HTML tag
  FMsgASCII.Add(Txt);
end;

procedure TEmailLogErrors.WriteBR(const Txt: string = '');
begin
  FMsgASCII.Add(Txt);
end;

procedure TEmailLogErrors.WriteNameValue(const Name, Value: string);
begin
  FMsgASCII.Add(' - ' + StringToLen(Name, 30, '.') + '.. ' + Value);
end;

procedure TEmailLogErrors.WriteException(const E: Exception; const Where: string);
begin
  // Output to email body
  WriteBR;
  WriteBR;
  WriteBR('*** EXCEPTION [' + Where + '] -- ' + E.Message + ' (' + E.ClassName + ')');
  WriteBR;

  // Output to console
  WriteLn;
  WriteLn('*** EXCEPTION [' + Where + '] -- ' + E.Message + ' (' + E.ClassName + ')');
  WriteLn;
end;

procedure TEmailLogErrors.AddSummary(const Title, SearchText: string);
begin
  if (Trim(Title) = '') or (Trim(SearchText) = '') then begin
    WriteLn('Invalid summary -- Title: "' + Title + '", Search: "' + SearchText + '"');
    Exit;
  end;

  Assert(FSummaryTitles <> nil);
  Assert(FSummarySearch <> nil);
  FSummaryTitles.Add(Trim(Title));
  FSummarySearch.Add(UpperCase(Trim(SearchText)));
  Assert(FSummaryTitles.Count = FSummarySearch.Count);
  SetLength(FSummaryCount, FSummaryTitles.Count);
end;

procedure TEmailLogErrors.IniRead(const IniFileName: string);
var
  Ini: TIniFile;
  IniFile: TextFile;
  Index: Integer;
  Line: string;
  Lines: TStringList;
  State: Char;
begin
  if not FileExists(IniFileName) then begin
    Writeln('INI file "' + IniFileName + '" does not exist, copy it from the sample and configure for your needs.');
    Halt(1);
  end;

  Ini := TIniFile.Create(IniFileName);
  try
    FEmailTo := Ini.ReadString('Email', 'To', '');    FStripTime      := Ini.ReadBool('Email', 'StripTime', False);
    FSendZeroErrors := Ini.ReadBool('Email', 'SendZeroErrors', True);
    FShowZeroSummaries := Ini.ReadBool('Email', 'ShowZeroSummaries', False);

    Lines := TStringList.Create;
    try
      Ini.ReadSection('Summary', Lines);
      for Index := 0 to Lines.Count - 1 do
        AddSummary(Lines.Strings[Index], Ini.ReadString('Summary', Lines.Strings[Index], ''));
    finally
      FreeAndNil(Lines);
    end;
  finally
    FreeAndNil(Ini);
  end;

  // Read Ignore section of INI file -- note that this section really should be
  // the last one contained in the INI file, although there is logic present to
  // exit if a later INI section is found.
  AssignFile(IniFile, IniFileName);
  Reset(IniFile);
  State := ' ';
  try
    while not EOF(IniFile) do begin
      ReadLn(IniFile, Line);
      Line := Trim(Line);
      if (Line = '')
        then Continue;
      if State = 'I' then
        begin
          if (Pos('[', Line) = 1) and (Pos(']', Line) > 0)
            then Break;
          Ignore.Add(UpperCase(Line));
        end;
      if SameText(Line, '[Ignore]') then
        State := 'I';
    end;
  finally
    CloseFile(IniFile);
  end;
end;

procedure TEmailLogErrors.FileRead(const LogFileName: string);
var
  LogFile: TStreamReader;
  Line: string;
  FoundErrors: Boolean;
begin
  try
    WriteLn(' - Log file ' + ExtractFileName(LogFileName));
    LogFile := TStreamReader.Create(TFileStream.Create(LogFileName, fmOpenRead or fmShareDenyNone),
      TEncoding.ANSI, True);
    try
      FoundErrors := False;
      while not LogFile.EndOfStream do begin
        Line := LogFile.ReadLine;
        FoundErrors := FileLineParse(Line) or FoundErrors;
      end;
    finally
      FreeAndNil(LogFile);
    end;
  except
    on E: Exception do
      begin
        WriteBR;
        WriteBR;
        WriteBR('*** ERROR: ' + E.Message + ' (' + E.ClassName + ')');
        WriteBR('*** Parsing log file ' + LogFileName);
        WriteBR;
      end;
  end;
end;

function TEmailLogErrors.FileLineParse(Line: string): Boolean;
var
  Skip: Boolean;
  Index: Integer;
  LineDate: TDateTime;
begin
  Result := False;
  Line := Trim(Line);
  if (Line = '') then
    Exit;
  try
    LineDate := IsoStrToDate(Copy(Line, 1, 10));
    if (LineDate >= ReportDateFrom) and (LineDate <= ReportDateTo) then
      if (StrContainsText('error', Line) or StrContainsText('fail', Line)) then begin

        FCountErrorsTotal := FCountErrorsTotal + 1;

        // Check for summary search text and increment when found
        for Index := 0 to FSummarySearch.Count - 1 do
          if StrContainsText(FSummarySearch.Strings[Index], Line) then
            FSummaryCount[Index] := FSummaryCount[Index] + 1;

        // Determine if the error line should be ignored
        Skip := False;
        for Index := 0 to Ignore.Count - 1 do
          if StrContainsText(Ignore.Strings[Index], Line) then begin
            Skip := True;
            FCountErrorsIgnored := FCountErrorsIgnored + 1;
            Break;
          end;

        if not Skip then begin
          // Add to Errors list without the leading date
          FErrors.Add(Copy(Line, 12, MaxInt));
          Result := True;
        end;
      end;
  except
    on E: EConvertError do
      ;
  end;
end;

{$WARN SYMBOL_PLATFORM OFF}

procedure TEmailLogErrors.ReadLogFiles;
var
  Index: Integer;
  Search: TSearchRec;
  FileList: TStringList;
  FileName: string;
  FileDate: TDateTime;
begin
  try
    FileList := TStringList.Create;
    try
      WriteLn('Scanning log files');
      if FindFirst(LogPath + 'Logic-*.log', faArchive + faReadOnly, Search) = 0 then
        try
          repeat
            FileList.Add(Search.Name);
          until FindNext(Search) <> 0;
        finally
          SysUtils.FindClose(Search);
        end;

      // Sort the file list and begin processing from the end (most recent files first)
      FileList.Sort;
      FileDate := 0;
      Index := FileList.Count - 1;

      // Skip any log files newer than the target date
      while (Index >= 0) do
        begin
          FileName := FileList.Strings[Index];
          try
            FileDate := IsoStrToDate(Copy(FileName, 7, 10));
            if (FileDate <= ReportDateTo) then
              Break;
            Index := Index - 1;
          except
            on E:EConvertError do
              Writeln('ERROR: Unable to parse date from log file name [1] ' + FileName);
          end;
        end;

      // Process any log files that were created on the target date
      while (Index >= 0) do
        begin
          FileName := FileList.Strings[Index];
          Index := Index - 1;
          try
            FileDate := IsoStrToDate(Copy(FileName, 7, 10));
            FileRead(LogPath + FileName);
            if (FileDate < ReportDateFrom) then
              Break;  // We've processed all log files that were created on the target date
          except
            on E: EConvertError do
              Writeln('ERROR: Unable to parse date from log file name [2] ' + FileName);
          end;
        end;

      // Process all log files from the date most prior to the target date
      while (FileDate > 0) and (Index >= 0) do
        begin
          FileName := FileList.Strings[Index];
          Index := Index - 1;
          try
            if (IsoStrToDate(Copy(FileName, 7, 10)) <> FileDate) then
              Break;  // We've processed all log files that were created on FileDate
            FileRead(LogPath + FileName);
          except
            on E: EConvertError do
              Writeln('ERROR: Unable to parse date from log file name [3] ' + FileName);
          end;
        end;
    finally
      FreeAndNil(FileList);
    end;
    WriteLn;
  except
    on E: Exception do
      WriteException(E, 'Reading Log Files');
  end;
end;

procedure TEmailLogErrors.OutputSummary;
var
  Index: Integer;
begin
  try
    WriteLn('Processing Summary Counts');
    WriteLn;

    WriteBR;
    Write('Error Summary', 'h2');
    WriteNameValue('Total Errors', IntToStr(FCountErrorsTotal));
    WriteNameValue('Ignored Errors', IntToStr(FCountErrorsIgnored));
    for Index := 0 to FSummaryTitles.Count - 1 do
      if ShowZeroSummaries or (FSummaryCount[Index] > 0) then
        WriteNameValue(FSummaryTitles.Strings[Index], IntToStr(FSummaryCount[Index]));
    WriteBR;
  except
    on E: Exception do
      WriteException(E, 'Output Summaries');
  end;
end;

procedure TEmailLogErrors.OutputDuplicates;
var
  Count: Integer;
  Index: Integer;
  Index2: Integer;
  All: TStringList;
  Dup: TStringList;
  CurrentLine: string;
begin
  try
    WriteLn('Checking for duplicate errors');

    All := TStringList.Create;
    Dup := TStringList.Create;
    try
      // Copy all errors into All.  Discard the time and the 2 numbers (or hyphen seperated blanks)
      // that follow it so we can easily find any exact duplicates
      for Index := 0 to Errors.Count - 1 do
        All.Add(DeleteFirstToken(DeleteFirstToken(DeleteFirstToken(Errors.Strings[Index]))));

      // Identify any errors that occured more than once
      while (All.Count > 0) do begin
        // Remove the next error to process from the list
        CurrentLine := All.Strings[0];
        All.Delete(0);

        // Find and remove duplicates, incrementing count for each one
        Count := 1;
        Index2 := All.IndexOf(CurrentLine);
        while (Index2 >= 0) do begin
          Count := Count + 1;
          All.Delete(Index2);
          Index2 := All.IndexOf(CurrentLine);
        end;

        // Record if there were duplicates
        if (Count > 1) then begin
          Dup.Add('  ' + LeftPadNumber(Count, 5) + ' - ' + CurrentLine);
        end;
      end;

      Dup.CustomSort(SortDescending);

      if (Dup.Count = 0) then
        WriteLn(' - No duplicates were found')
      else
        WriteLn(' - Found ' + IntToStr(Dup.Count) + ' duplicate errors');
      WriteLn;

      // Output duplicates to the message body
      if (Dup.Count > 0) then begin
        WriteBR;
        Write('Duplicate Errors', 'h2');
        for Index := 0 to Dup.Count - 1 do begin
          WriteBR(Dup.Strings[Index]);
          WriteBR;
        end;
      end;
    finally
      FreeAndNil(Dup);
      FreeAndNil(All);
    end;
  except
    on E: Exception do
      WriteException(E, 'Output Duplicates');
  end;
end;

procedure TEmailLogErrors.OutputErrors;
var
  Index: Integer;
begin
  try
    WriteBR;
    Write(IntToStr(ErrorCount) + ' Logged Errors', 'h2');
    Errors.CustomSort(SortErrorMessages);
    for Index := 0 to Errors.Count - 1 do begin
      if StripTime then
        WriteBR(DeleteFirstToken(DeleteFirstToken(DeleteFirstToken(Errors.Strings[Index]))))
      else
        WriteBR(Errors.Strings[Index]);
      WriteBR;
    end;
  except
    on E: Exception do
      WriteException(E, 'Output Errors');
  end;
end;

procedure TEmailLogErrors.SendEmail;
var
  EmailSubject: string;

begin
  if (not SendZeroErrors) and (ErrorCount = 0) then begin
    WriteLn('No email was sent because no errors were found to be reported');
    Exit;
  end;

  if (ReportDateFrom = ReportDateTo) then
    EmailSubject := 'QM Errors (' + IntToStr(ErrorCount) + ') on ' + FormatDateTime('yyyy-mm-dd', ReportDateFrom)
  else
    EmailSubject := 'QM Errors (' + IntToStr(ErrorCount) + ') from ' + FormatDateTime('yyyy-mm-dd', ReportDateFrom) + ' to ' + FormatDateTime('yyyy-mm-dd', ReportDateTo);

  SendEmailWithIniSettings(FIniFileName, 'email', FEmailTo,'', EmailSubject, FMsgASCII.Text);
end;

// Private Methods
function TEmailLogErrors.ErrorCount_Get: Integer;
begin
  Result := FErrors.Count;
end;

var
  Comp: TEmailLogErrors;
  DateFrom: TDateTime;
  DateTo: TDateTime;
  Path: string;
  Param: string;
begin
  try
    Comp := TEmailLogErrors.Create(nil);
    try
      // Default to yesterday
      DateFrom := Trunc(Now) - 1;
      DateTo   := DateFrom;

      // Handle any supplied command line parameters
      Param := ParamValue('date');
      if (Param <> '') then begin
        DateFrom := IsoStrToDate(Param);
        DateTo   := DateFrom;
      end;

      Param := ParamValue('from');
      if (Param <> '') then
        DateFrom := IsoStrToDate(Param);

      Param := ParamValue('to');
      if (Param <> '') then
        DateTo := IsoStrToDate(Param);

      Path := ParamValue('path', ExtractFilePath(ParamStr(0)));

      // Generate and send the report
      Comp.Execute(DateFrom, DateTo, Path);
    finally
      FreeAndNil(Comp);
    end;
  except
    on E: Exception do
      WriteLn('ERROR: ' + E.Message + ' (' + E.ClassName + ')');
  end;
end.

