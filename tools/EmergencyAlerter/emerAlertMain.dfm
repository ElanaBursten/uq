object frmEmerAlert: TfrmEmerAlert
  Left = 0
  Top = 0
  Caption = '-- Emergency Alerter --'
  ClientHeight = 348
  ClientWidth = 618
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 311
    Width = 45
    Height = 13
    Caption = 'database'
  end
  object Label2: TLabel
    Left = 488
    Top = 312
    Width = 35
    Height = 13
    Caption = 'version'
  end
  object Label3: TLabel
    Left = 176
    Top = 312
    Width = 49
    Height = 13
    Caption = 'processed'
  end
  object Label4: TLabel
    Left = 280
    Top = 312
    Width = 51
    Height = 13
    Caption = 'succeeded'
  end
  object Label5: TLabel
    Left = 368
    Top = 312
    Width = 26
    Height = 13
    Caption = 'failed'
  end
  object Label6: TLabel
    Left = 24
    Top = 259
    Width = 53
    Height = 13
    Caption = 'Call Center'
  end
  object Label7: TLabel
    Left = 101
    Top = 259
    Width = 51
    Height = 13
    Caption = 'Alert After'
  end
  object Label8: TLabel
    Left = 157
    Top = 277
    Width = 15
    Height = 13
    Caption = 'hrs'
  end
  object Label9: TLabel
    Left = 432
    Top = 259
    Width = 47
    Height = 13
    Caption = 'Look back'
  end
  object Label10: TLabel
    Left = 488
    Top = 277
    Width = 23
    Height = 13
    Caption = 'days'
  end
  object btnRun: TButton
    Left = 256
    Top = 263
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 0
    OnClick = btnRunClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 618
    Height = 257
    Align = alTop
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 329
    Width = 618
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object BitBtn1: TBitBtn
    Left = 552
    Top = 271
    Width = 62
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 3
  end
  object edtCallCenter: TEdit
    Left = 24
    Top = 273
    Width = 53
    Height = 21
    TabOrder = 4
    OnChange = edtCallCenterChange
  end
  object edtAlertHours: TEdit
    Left = 98
    Top = 273
    Width = 53
    Height = 21
    NumbersOnly = True
    TabOrder = 5
    Text = '1.5'
    OnChange = edtAlertHoursChange
  end
  object edtLookBackDays: TEdit
    Left = 429
    Top = 273
    Width = 53
    Height = 21
    NumbersOnly = True
    TabOrder = 6
  end
  object ADOConn: TADOConnection
    CommandTimeout = 60
    ConnectionTimeout = 30
    DefaultDatabase = 'QM'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 40
    Top = 32
  end
  object qryTicketAck: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'HoursBack'
        DataType = ftBCD
        Size = -1
        Value = Null
      end
      item
        Name = 'LookBackDays'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Use QM;'
      ''
      
        'select distinct t.ticket_id,t.ticket_number, t.ticket_type,t.tra' +
        'nsmit_date,'
      't.work_city, t.work_state,'
      'l.assigned_to_id,e.short_name, ja.arrival_date'
      'from ticket t with (nolock)'
      'join locate l on l.ticket_id = t.ticket_id'
      'join employee e on e.emp_id = l.assigned_to_id'
      'left join jobsite_arrival ja on ja.ticket_id = t.ticket_id'
      'where t.ticket_format = :CallCenter'
      'and ticket_type like '#39'%EMER%'#39
      'and l.status in ('#39'-R'#39','#39'ACK'#39')'
      'and DBO.get_ticket_age_dec_hours(t.ticket_id)> :HoursBack'
      'and t.transmit_date <=(GetDate())'
      
        'and abs(DATEDIFF ( dd , t.transmit_date ,GetDate() ))<=:LookBack' +
        'Days'
      'and ja.arrival_date is null')
    Left = 120
    Top = 32
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = ' qmanager@utiliquest.com '
        Name = 'qmanager'
        Text = 'qmanager <" qmanager"@utiliquest.com >'
        Domain = 'utiliquest.com '
        User = ' qmanager'
      end>
    From.Address = ' qmanager@utiliquest.com '
    From.Name = 'qmanager'
    From.Text = 'qmanager <" qmanager"@utiliquest.com >'
    From.Domain = 'utiliquest.com '
    From.User = ' qmanager'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 566
    Top = 208
  end
  object IdLogEvent1: TIdLogEvent
    Active = True
    Left = 566
    Top = 104
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 566
    Top = 160
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 504
    Top = 107
  end
  object spGetNotificationEmail: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'GetNotificationEmail'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmpID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmailType'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    Left = 528
    Top = 48
  end
  object qryGetTicketImage: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Use QM;'
      'select image'
      'from ticket'
      'where ticket_id = :ticketID'
      '')
    Left = 120
    Top = 96
  end
end
