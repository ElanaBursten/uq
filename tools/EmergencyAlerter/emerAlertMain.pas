unit emerAlertMain;
// QM-377  1054 1063  1064

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdSSL, IdSSLOpenSSL, IdIntercept, IdLogBase, IdLogEvent, IdBaseComponent,
  IdMessage, Vcl.Buttons, Vcl.ComCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;

type
  TfrmEmerAlert = class(TForm)
    ADOConn: TADOConnection;
    qryTicketAck: TADOQuery;
    btnRun: TButton;
    Memo1: TMemo;
    IdMessage1: TIdMessage; // qm-1054 sr
    IdLogEvent1: TIdLogEvent; // qm-1054 sr
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL; // qm-1054 sr
    IdSMTP1: TIdSMTP;
    StatusBar1: TStatusBar;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    spGetNotificationEmail: TADOStoredProc;
    qryGetTicketImage: TADOQuery;
    Label6: TLabel;
    edtCallCenter: TEdit;
    Label7: TLabel;
    edtAlertHours: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    edtLookBackDays: TEdit;
    Label10: TLabel; // qm-1054 sr
    procedure btnRunClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtCallCenterChange(Sender: TObject);
    procedure edtAlertHoursChange(Sender: TObject);
  private
    LogPath: string;
    LogResult: TLogResults;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    sBaseURL: string;

    FromAddress: string;
    FromName: string;
    FromDomain: string;
    FromUser: string;

    smtpHost: string;
    smtpPort:integer;
    eMailType: string;

    lookbackDays:integer;

    CallCenter:string;
    AlertAfterHours:double;
    sAlertAfterHours:string;
    procedure ProcessParams(pCallCenter:string;phoursDelay:double);
    procedure ProcessTicketAcks;
    function ProcessINI: boolean;
    procedure clearLogRecord;
    function GetAppVersionStr: string;
    procedure WriteLog;
    function connectDB: boolean;
    procedure ConfigureSMTP;
    { Private declarations }
  public
    { Public declarations }
    APP_VER: string;

  end;

var
  frmEmerAlert: TfrmEmerAlert;

implementation

uses
  Inifiles, System.StrUtils, System.DateUtils;
{$R *.dfm}



procedure TfrmEmerAlert.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:= caFree;
  ADOConn.Close;
end;

procedure TfrmEmerAlert.FormCreate(Sender: TObject);
begin

  APP_VER := GetAppVersionStr;
  StatusBar1.Panels[4].Text := APP_VER;
  ProcessINI;
  connectDB;
  StatusBar1.Panels[0].Text := aServer;
  ConfigureSMTP;

  if ParamCount < 2 then
  begin
    showmessage('Not enough command line parameters to run.  Shutting down.');
    LogResult.LogType := ltError;
    LogResult.MethodName := 'Program Start Up';
    LogResult.ExcepMsg :=
      'Not enough command line parameters to run.  Shutting down.';
    WriteLog;
    Application.Terminate;

  end
  else
  begin
    edtCallCenter.Text:=paramstr(1);
    edtAlertHours.Text:=paramstr(2);
    sAlertAfterHours:=paramstr(2);
    ProcessParams(paramstr(1), StrToFloat(paramstr(2)));
  end;

  If paramstr(3) <> 'GUI' then
  begin
    ProcessTicketAcks;
    Application.Terminate;
  end;

end;

procedure TfrmEmerAlert.btnRunClick(Sender: TObject);
begin
  ProcessTicketAcks;
end;

procedure TfrmEmerAlert.ProcessTicketAcks;
var
  ticketId, locatorEmpId: integer;
  insertDate: TDateTime;
  ticketNo, WkCity, WkState: string; // t.work_city, t.work_state,
  processed, failed: integer;
  emerRequeueEmail, shortName: string; // qm-1054 1063 sr
const // ticket_no        city/st
  MAIL_MSG = 'Emergency Ticket %s was requeued in %s, %s. Locator: %s has not arrived within %s hours.';
begin
  try
    processed := 0;
    failed := 0;
    try
      with qryTicketAck do
      begin
        qryTicketAck.Parameters.ParamByName('CallCenter').Value:= CallCenter;
        qryTicketAck.Parameters.ParamByName('HoursBack').Value:= AlertAfterHours;
        qryTicketAck.Parameters.ParamByName('LookBackDays').Value:= LookBackDays;
        Open;
        while not Eof do
        begin
          ticketNo := '';
          emerRequeueEmail := ''; // qm-1054 sr
          WkCity := '';
          WkState := '';

          ticketId := FieldByName('ticket_id').AsInteger;
          locatorEmpId := FieldByName('assigned_to_id').AsInteger;
          ticketNo := FieldByName('ticket_number').AsString;

          spGetNotificationEmail.Parameters.ParamByName('@EmpID').Value:=locatorEmpId; //@EmailType
          spGetNotificationEmail.Parameters.ParamByName('@EmailType').Value:=eMailType;
          spGetNotificationEmail.open;
          emerRequeueEmail := spGetNotificationEmail.FieldByName('email_destination').AsString;

          WkCity := FieldByName('work_city').AsString;
          WkState := FieldByName('work_state').AsString;
          ticketNo := FieldByName('ticket_number').AsString;
          shortName := FieldByName('short_name').AsString;
          IdMessage1.ClearBody;
          IdMessage1.Subject := ExtractFileName(Application.ExeName) + ' for '
            + ticketNo;

          if emerRequeueEmail <> '' then
          begin
            IdMessage1.Recipients.EMailAddresses := emerRequeueEmail;
            IdMessage1.Body.Add(Format(MAIL_MSG, [ticketNo, WkCity, WkState,
              shortName, sAlertAfterHours])); // qm-1054
            try
              idSMTP1.Connect;
              idSMTP1.Send(IdMessage1);
              memo1.Lines.Add(Format(MAIL_MSG, [ticketNo, WkCity, WkState,
              shortName, sAlertAfterHours]));
              memo1.Lines:=IdMessage1.Body;
              idSMTP1.Disconnect();
            except
              on E: Exception do
              begin
                LogResult.LogType := ltError;
                LogResult.MethodName := 'emerAlert';
                LogResult.DataStream := 'Body: ' +
                  (Format(MAIL_MSG,[ticketNo, WkCity, WkState, shortName])); // qm-1054
                LogResult.ExcepMsg := E.Message;
                WriteLog;
              end;
            end;
          end;

          inc(processed);
          Memo1.Lines.Add(IntToStr(processed) + ' Alerted ' +
            IntToStr(ticketId));
          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'ProcessTicketAcks';
          LogResult.DataStream := IntToStr(processed) + ' Alerted ' +
            IntToStr(ticketId);
          WriteLog;
          next;
        end; // while
      end; // with
    except
      on E: Exception do
      begin
        inc(failed);
        Memo1.Lines.Add(IntToStr(processed) + ' Alerted for ' +
          IntToStr(ticketId));
        LogResult.LogType := ltError;
        LogResult.MethodName := 'ProcessTicketAcks';
        LogResult.DataStream := IntToStr(processed) + ' Alert for ' +
          IntToStr(ticketId);
        LogResult.ExcepMsg := E.Message;
        WriteLog;
      end;
    end;

  finally
    qryTicketAck.Close;

    statusbar1.Panels[1].Text:=intToStr(processed);
    statusbar1.Panels[2].Text:=intToStr(processed - failed);
    statusbar1.Panels[3].Text:=intToStr(failed);

    Memo1.Lines.Add(IntToStr(processed) + ' processed');
    Memo1.Lines.Add(IntToStr(processed - failed) + ' succeeded');
    Memo1.Lines.Add(IntToStr(failed) + ' failed');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Process Complete';
    LogResult.Status := IntToStr(processed - failed) +
      ' Successfully processed and ' + IntToStr(failed) + ' Failure(s)';
    WriteLog;

  end;

end;

procedure TfrmEmerAlert.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackslash(LogPath) + 'EmerAlertLog' + '_' +
    FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
    WriteLn(myFile, 'EmerReQue Version: ' + APP_VER);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmEmerAlert.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmEmerAlert.connectDB: boolean;
var
  connStr: String;
begin
  ADOConn.Close;
  ADOConn.ConnectionString := '';
  result := false;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
    ADOConn.Open;
    result := True;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
    end;

  except
    on E: Exception do
    begin
      result := false;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStr;
      LogResult.ExcepMsg := E.Message;
      WriteLog;
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;


procedure TfrmEmerAlert.edtAlertHoursChange(Sender: TObject);
begin
  AlertAfterHours:=StrToFloat(edtAlertHours.Text);
  sAlertAfterHours:=edtAlertHours.Text;
end;

procedure TfrmEmerAlert.edtCallCenterChange(Sender: TObject);
begin
  CallCenter:=edtCallCenter.Text;
end;

function TfrmEmerAlert.ProcessINI: boolean;
var
  EmergencyAlert: TIniFile;
  myPath: string;
begin
  result := True;
  try
    myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
    try
      EmergencyAlert := TIniFile.Create(myPath + 'EmergencyAlert.ini');
      LogPath := EmergencyAlert.ReadString('LogPaths', 'LogPath', '');
      ForceDirectories(LogPath);

      //Database
      aServer := EmergencyAlert.ReadString('Database', 'Server', '');
      aDatabase := EmergencyAlert.ReadString('Database', 'DB', 'QM');
      ausername := EmergencyAlert.ReadString('Database', 'UserName', '');
      apassword := EmergencyAlert.ReadString('Database', 'Password', '');

      //smtp
      FromAddress:= EmergencyAlert.ReadString('SMTP', 'FromAddress', 'qmanager@utiliquest.com');
      FromName:= EmergencyAlert.ReadString('SMTP', 'FromName', 'qmanager');
      FromDomain:= EmergencyAlert.ReadString('SMTP', 'FromDomain', 'utiliquest.com');
      FromUser:= EmergencyAlert.ReadString('SMTP', 'FromUser', 'qmanager');
      smtpHost:= EmergencyAlert.ReadString('SMTP', 'smtpHost', 'smtp.dynutil.com');
      smtpPort:= EmergencyAlert.Readinteger('SMTP', 'smtpPort', 25);

      //eMailChain
      eMailType:= EmergencyAlert.ReadString('eMailChain', 'eMailType', 'time_warning');

      lookbackDays:=EmergencyAlert.Readinteger('operation', 'LookBackDays', 8);
      AlertAfterHours:=EmergencyAlert.ReadFloat('operation', 'AlertAfterHrs', 1.5);

      edtLookBackDays.Text:=IntToStr(lookbackDays);
      LogResult.LogType := ltInfo;
      LogResult.Status := 'Ini processed';
      LogResult.MethodName := 'ProcessINI';
      WriteLog;
    finally
      FreeAndNil(EmergencyAlert);
    end;
  except
    on E: Exception do
    begin
      result := false;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ProcessINI';
      LogResult.ExcepMsg := E.Message;
      WriteLog;
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

procedure TfrmEmerAlert.ProcessParams(pCallCenter:string; pHoursDelay:double);
begin
  CallCenter:= pCallCenter;
  AlertAfterHours:=pHoursDelay;
end;

procedure TfrmEmerAlert.ConfigureSMTP;
begin
  IdSMTP1.Host:= smtpHost;
  IdSMTP1.Port:= smtpPort;

  IdMessage1.From.Address:=FromAddress;
  IdMessage1.From.Domain :=FromDomain;
  IdMessage1.From.User :=FromUser;
  IdMessage1.From.Name :=FromName;
end;


function TfrmEmerAlert.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;
end.
