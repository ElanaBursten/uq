program EmergencyAlert;
//QM-1064
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

 //qm-1064 takes ticket_format and Time After transmit_date hours
uses
  Vcl.Forms,
  Windows,
  SysUtils,
  emerAlertMain in 'emerAlertMain.pas' {frmEmerAlert},
  GlobalSU in 'D:\common\GlobalSU.pas';

var
  MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    if ParamCount = 0 then Application.ShowMainForm := False;
    Application.CreateForm(TfrmEmerAlert, frmEmerAlert);
  reportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Run;
  end
  else
    Application.Terminate;
end.
