object frmMainATT: TfrmMainATT
  Left = 0
  Top = 0
  Caption = 'UATT '
  ClientHeight = 485
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object Label1: TLabel
    Left = 280
    Top = 355
    Width = 53
    Height = 13
    Caption = 'Call Center'
  end
  object btnParseXML: TButton
    Left = 56
    Top = 350
    Width = 105
    Height = 25
    Caption = 'Parse XML'
    TabOrder = 0
    OnClick = btnParseXMLClick
  end
  object Memo1: TMemo
    Left = 24
    Top = 24
    Width = 561
    Height = 289
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object edtCallCenterName: TEdit
    Left = 352
    Top = 352
    Width = 121
    Height = 21
    TabOrder = 2
    OnChange = edtCallCenterNameChange
  end
  object btnSendResponse: TButton
    Left = 56
    Top = 415
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 3
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 466
    Width = 635
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'SSDS-UTQ-QM-02-DV'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object btnQueueLocates: TButton
    Left = 56
    Top = 384
    Width = 105
    Height = 25
    Caption = 'Queue Locates'
    TabOrder = 5
    OnClick = btnQueueLocatesClick
  end
  object RESTResponse1: TRESTResponse
    Left = 160
    Top = 320
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        Name = 'body'
        Options = [poDoNotEncode]
        ContentTypeStr = 'application/json'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 384
    Top = 128
  end
  object RESTClient1: TRESTClient
    Authenticator = HTTPBasicAuthenticator1
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://test-alcs.att.com/closeouts/se'
    ContentType = 'application/json'
    Params = <>
    RaiseExceptionOn500 = False
    SynchronizedEvents = False
    Left = 368
    Top = 320
  end
  object HTTPBasicAuthenticator1: THTTPBasicAuthenticator
    Username = 'utiuat'
    Password = 'Welcome@123'
    Left = 512
    Top = 328
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to= :ResponseTo'
      '')
    Left = 480
    Top = 144
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'get_pending_multi_responses_9GA'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 120
    Top = 160
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    ConnectionTimeout = 60
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 48
    Top = 144
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
  object spMoveToResponderMultiQueue: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'move_to_responder_multi_queue'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@InsertDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ClientCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@RespondToList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8000
        Value = Null
      end
      item
        Name = '@NumInsertsExpected'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 88
  end
  object spGetResponderQueueItems: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'get_responder_queue_items '
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 128
    Top = 32
  end
  object delCleanUpMultiQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'ClientList'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'and respond_to= ResponseTo'
      'and client_code <> ClientList')
    Left = 480
    Top = 216
  end
  object delPreCleanMultiQ: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'USE QM'
      'delete'
      'from responder_multi_queue'
      'where locate_id in ('
      #9#9#9#9#9'(select rmq.locate_id'
      '          from responder_multi_queue rmq'
      #9#9#9#9#9'join locate l on l.locate_id = rmq.locate_id'
      #9#9#9#9#9'where l.added_by <> '#39'PARSER'#39'))'
      '')
    Left = 352
    Top = 56
  end
  object delPreCleanResponderQ: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'USE QM'
      'delete'
      'from responder_queue'
      'where locate_id in ('
      #9#9#9#9#9'(select rq.locate_id'
      #9#9#9#9#9'from responder_queue rq'
      #9#9#9#9#9'join locate l on l.locate_id = rq.locate_id'
      #9#9#9#9#9'where l.added_by <> '#39'PARSER'#39'))')
    Left = 352
  end
  object Timer1: TTimer
    Interval = 300000
    OnTimer = Timer1Timer
    Left = 448
    Top = 400
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 208
    Top = 64
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 504
  end
  object IdLogEvent1: TIdLogEvent
    Left = 400
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvTLSv1, sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 271
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 160
    Top = 3
  end
end
