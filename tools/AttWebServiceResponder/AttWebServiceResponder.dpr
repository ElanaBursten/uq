program AttWebServiceResponder;
//QM-468 AT&T Responder used in Ga.
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  uATTMain in 'uATTMain.pas' {frmMainATT};

var
  MyInstanceName: string;
// takes params "C:\Trunk1\tools\AttWebServiceResponder\config.xml" "3003" "GUI" with GUI optional
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName)+'_'+ParamStr(2);
  uATTMain.InstanceName:= MyInstanceName;  //QM-1031
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.CreateForm(TfrmMainATT, frmMainATT);
    Application.Run;
  end
  else
    Application.Terminate;

end.
