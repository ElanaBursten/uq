unit uMainCanYouDigIt; // qm-743  4IQ DigTix

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, REST.Types,
  REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope, Data.Win.ADODB, Data.DB, IdMessage, IdIntercept,
  IdLogBase, IdLogEvent, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdSMTPBase, IdSMTP, IPPeerClient;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: TDatetime;
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: boolean;
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end; // TResponseLogEntry

type
  TReturnedResponse = record
    success: boolean;
    message: string;
    data: integer;
  end;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String[255];
  end; // TLogResults

type
  TfrmCanYouDigIt = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    ADOConn: TADOConnection;
    spGetPendingResponses: TADOStoredProc;
    insResponseLog: TADOQuery;
    delResponse: TADOQuery;
    qryEMailRecipients: TADOQuery;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    ReturnedResponse: TReturnedResponse;
    LogResult: TLogResults;
    insertResponseLog: TResponseLogEntry;
    flogDir: wideString;
    fOC_Code: string;
    FRespondto: string;
    FPosturl: string;
    fCallCenter: string;
    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;

    function GetAppVersionStr: string;
    function deleteMultiQueueRecord(RespondTo: string;
      LocateID: integer): boolean;
    procedure SetUpRecipients;
    function ProcessParams: boolean;
    function getData(JsonString, Field: String): String;
  public
    { Public declarations }
    property CallCenter: string read fCallCenter write fCallCenter;
    property OC_Code: string read fOC_Code write fOC_Code; // QM-552
    property RespondTo: string read FRespondto write FRespondto;
    property logDir: wideString read flogDir;
    property Posturl: string read FPosturl write FPosturl;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmCanYouDigIt: TfrmCanYouDigIt;
  LocaInstance:string;
implementation

uses INIFiles, StrUtils, System.JSON;
{$R *.dfm}
{ TfrmCanYouDigIt }

function TfrmCanYouDigIt.AddResponseLogEntry(insertResponseLog
  : TResponseLogEntry): boolean;
begin
  insResponseLog.Parameters.ParamByName('LocateID').Value := insertResponseLog.LocateID;
  insResponseLog.Parameters.ParamByName('ResponseDate').Value   := insertResponseLog.ResponseDate;
  insResponseLog.Parameters.ParamByName('CallCenter').Value := insertResponseLog.CallCenter;
  insResponseLog.Parameters.ParamByName('Status').Value := insertResponseLog.Status;
  insResponseLog.Parameters.ParamByName('ResponseSent').Value  := insertResponseLog.ResponseSent;
  insResponseLog.Parameters.ParamByName('Success').Value   := insertResponseLog.Sucess;
  insResponseLog.Parameters.ParamByName('Reply').Value   := insertResponseLog.Reply;
  insResponseLog.ExecSQL;
end;

procedure TfrmCanYouDigIt.btnSendResponseClick(Sender: TObject);
var
  pUsername, pPassword, pState, pTicketID, pRevision, pMemberCode,
  pResponseStatus, pDate, pNotes, pUser: string;
  ResponseBody: string;
  locate_status: string;
  LocateID, cntBad, cntGood:integer;
begin
  ResponseBody := '';
  cntBad  := 0;
  cntGood  := 0;
  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value := RespondTo;
    Parameters.ParamByName('@CallCenter').Value := CallCenter;
    Parameters.ParamByName('@ParsedLocatesOnly').Value := 0;
    Parameters.ParamByName('@ResponseDelayMinutes').Value := 0;

    open;
    while not EOF do
    begin
      ReturnedResponse.success:=false;
      ReturnedResponse.data:=-1;
      ReturnedResponse.message:='';
      pUsername := Trim(FieldByName('user_name').AsString);
      pPassword := Trim(FieldByName('password').AsString);
      pState := Trim(FieldByName('work_state').AsString);
      pTicketID := Trim(FieldByName('ticket_number').AsString);
      LocateID  := FieldByName('Locate_ID').AsInteger;
      pRevision := '0';
      pMemberCode := Trim(FieldByName('client_code').AsString);
      pDate :=FormatDateTime('yyyy-mm-dd hh:nn:ss',StrToDateTime(FieldByName('closed_date').AsString));
      pNotes := 'Response by UtiliQuest';// Trim(FieldByName('PubLocateNote').AsString);
      pUser := Trim(FieldByName('Added_by').AsString);
      pResponseStatus := Trim(FieldByName('outgoing_status').AsString);
      locate_status := Trim(FieldByName('status').AsString);
      with RESTRequest1.Params do
      begin
        BeginUpdate;
        ParameterByName('username').Value:=pUsername;
        ParameterByName('password').Value:=pPassword;
        ParameterByName('state').Value:=pState;
        ParameterByName('ticketID').Value:=pTicketID;
        ParameterByName('revision').Value:=pRevision;
        ParameterByName('memberCode').Value:=pMemberCode;
        ParameterByName('responseStatus').Value:=pResponseStatus;
        ParameterByName('date').Value:=pDate;
        ParameterByName('notes').Value:=pNotes;
        ParameterByName('user').Value:=pUser;
        EndUpdate;
      end;

      Memo1.Lines.AddPair('user_name ',pUsername);
      Memo1.Lines.AddPair('password ',pPassword);
      Memo1.Lines.AddPair('work_state ',pState);
      Memo1.Lines.AddPair('ticket_number ',pTicketID);
      Memo1.Lines.AddPair('Revision ',pRevision);
      Memo1.Lines.AddPair('MemberCode ',pMemberCode);
      Memo1.Lines.AddPair('ResponseStatus ',pResponseStatus);
      Memo1.Lines.AddPair('Date ',pDate);
      Memo1.Lines.AddPair('Notes ',pNotes);
      Memo1.Lines.AddPair('User ',pUser);

      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message + ' Body ' + ResponseBody;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +
            RESTResponse1.StatusText + ' TicketNo: ' + pTicketID;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end; // try-except
      ResponseBody:= RESTResponse1.Content;
      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if GetData(RESTResponse1.Content, 'data')='0' then
      begin
        ReturnedResponse.success:=True;
      end
      else
      begin
        ReturnedResponse.success:=False;
      end;

      ReturnedResponse.message:=GetData(RESTResponse1.Content, 'message');
      ReturnedResponse.data:=StrToInt(GetData(RESTResponse1.Content, 'data'));
      Memo1.Lines.Add('Returned Response Body: '+RESTResponse1.Content);
      Memo1.Lines.Add('ReturnedResponse.success: '+BoolToStr(ReturnedResponse.success));
      Memo1.Lines.Add('ReturnedResponse.message: '+ReturnedResponse.message);
      Memo1.Lines.Add('ReturnedResponse.data: '+IntToStr(ReturnedResponse.data));


{LEAVE -1 = unknown error
DELETE 0 = success
LEAVE 1 = invalid request
LEAVE 2 = authentication failure
LEAVE 3 = user locked out due to ten consecutive authentication failures
LEAVE 4 = response permission not granted to specified user
DELETE 5 = ticket not found
DELETE 6 = response status not found
DELETE 7 = ticket is already completed
DELETE 8 = operator qualification has expired for the specified user
DELETE 9 = ticket is not assigned to the responding user
DELETE 10 = requirements not met}
      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin  //good
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response sent '+ResponseBody;
        WriteLog(LogResult);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response returned '+RESTResponse1.Content;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +pTicketID;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := Now;

        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := pResponseStatus;

        insertResponseLog.Sucess              := ReturnedResponse.success;
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        Case ReturnedResponse.data of
        -1,1,3,4: Begin  // keep
                  //desd code but here in case change

                  end;
      0,2,5..10: Begin  //delete
                    deleteMultiQueueRecord(RespondTo, LocateID);
                  End;


        End;
      end  //StatusCode > 199 <300   good
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +pTicketID;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        WriteLog(LogResult);

        //Add EMail notices to all Delphi Responders
        IdMessage1.Body.Add('Failed request no '+IntToStr(cntBad)+' at '+formatdatetime('yyyy-mm-dd hh:nn:ss', Now));
        IdMessage1.Body.Add(IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +pTicketID);
        IdMessage1.Body.Add('Sent '+ResponseBody);
        IdMessage1.Body.Add('Returned ' + RESTResponse1.Content);


        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := Now;
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := pResponseStatus;
        insertResponseLog.Sucess              := ReturnedResponse.success;
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
      StatusBar1.Panels[3].Text:=IntToStr(cntGood);
      StatusBar1.Panels[5].Text:=IntToStr(cntBad);
    end; //while not EOF do
    StatusBar1.Panels[3].Text:=IntToStr(cntGood);
    StatusBar1.Panels[5].Text:=IntToStr(cntBad);
    StatusBar1.Panels[7].Text:=IntToStr(cntGood+cntBad);
  end; //with spGetPendingResponses do
end;


function TfrmCanYouDigIt.getData(JsonString: String; Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

procedure TfrmCanYouDigIt.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmCanYouDigIt.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try

    ADOConn.open;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text:=aServer;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

function TfrmCanYouDigIt.deleteMultiQueueRecord(RespondTo: string;
  LocateID: integer): boolean;
begin
  delResponse.Parameters.ParamByName('LocateID').Value  :=  LocateID;
  delResponse.Parameters.ParamByName('respond_to').Value  :=RespondTo;
  delResponse.ExecSQL;  //uncomment for prod
  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'deleteMultiQueueRecord';
  LogResult.DataStream := 'RespondTo: '+RespondTo+'  LocateID: '+IntToStr(LocateID);

  WriteLog(LogResult);
  Memo1.Lines.Add('delete record from MultiQueue');
  Memo1.Lines.Add('RespondTo: '+RespondTo+'  LocateID: '+IntToStr(LocateID));
end;

procedure TfrmCanYouDigIt.FormCreate(Sender: TObject);
begin
  ProcessParams;
  StatusBar1.Panels[9].Text:= GetAppVersionStr;
  ProcessINI;
  connectDB;
  SetUpRecipients;
  if uppercase(ParamStr(3)) <> 'GUI' then
  begin
    btnSendResponseClick(application);
    application.Terminate;
  end;

end;

function TfrmCanYouDigIt.ProcessParams:boolean;
begin
  Callcenter:=ParamStr(2);
  RespondTo:=ParamStr(1);
end;

function TfrmCanYouDigIt.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmCanYouDigIt.ProcessINI: boolean;
var
  DigTixUni: TIniFile;
begin
  try
    DigTixUni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := DigTixUni.ReadString('Database', 'Server', '');
    aDatabase := DigTixUni.ReadString('Database', 'DB', 'QM');
    ausername := DigTixUni.ReadString('Database', 'UserName', '');
    apassword := DigTixUni.ReadString('Database', 'Password', '');
    flogDir := DigTixUni.ReadString('LogPaths', 'LogPath', '');

    OC_Code := DigTixUni.ReadString('EMailAlerts', 'RecipientsOC_Code', 'NJN');
    // QM-552

    with IdMessage1.From do // QM-552
    begin
      Address := DigTixUni.ReadString('EMailAlerts', 'FromAddress', '');
      Domain := DigTixUni.ReadString('EMailAlerts', 'FromDomain', '');
      Name := DigTixUni.ReadString('EMailAlerts', 'FromName', '');
      Text := DigTixUni.ReadString('EMailAlerts', 'FromText', '');
      User := DigTixUni.ReadString('EMailAlerts', 'FromUser', '');
    end;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    DigTixUni.Free;
  end;
end;

procedure TfrmCanYouDigIt.SetUpRecipients;
begin
  try
    idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
    qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
    qryEMailRecipients.Open;
    if not qryEMailRecipients.eof then
    idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
    else
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SetUpRecipients';
      LogResult.Status:= 'No recipients to send email to';
      WriteLog(LogResult);
   end;
  finally
    qryEMailRecipients.close;
  end;
end;

procedure TfrmCanYouDigIt.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackslash(flogDir) + LocaInstance +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

end.
