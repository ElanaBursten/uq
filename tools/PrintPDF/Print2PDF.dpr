program Print2PDF;
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
{$R '..\..\QMIcon.res'}
uses
  Vcl.Forms,
  uMainPrintPDF in 'uMainPrintPDF.pas' {dmPrint2PDF: TDataModule};

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmPrint2PDF, dmPrint2PDF);
  Application.Run;
end.
