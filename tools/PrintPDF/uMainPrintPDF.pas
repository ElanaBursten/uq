unit uMainPrintPDF;

interface

uses
  System.SysUtils, System.Classes, ppDB, ppDBPipe, Data.DB, dbisamtb, ppParameter, ppDesignLayer, ppPrnabl, ppClass, ppStrtch,
  ppRichTx, ppCache, ppBands, ppComm, ppRelatv, ppProd, ppReport, ppCtrls, ppVar;

type
  TdmPrint2PDF = class(TDataModule)
    dsTicketVersion: TDataSource;
    ppReport1: TppReport;
    ppDetailBand1: TppDetailBand;
    ppDBRichText1: TppDBRichText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppParameterList1: TppParameterList;
    qryTicketVersion: TDBISAMQuery;
    DBISAMDatabase1: TDBISAMDatabase;
    ppDBPipeline1: TppDBPipeline;
    ppTitleBand1: TppTitleBand;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPrint2PDF: TdmPrint2PDF;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmPrint2PDF.DataModuleCreate(Sender: TObject);
var
  sTktVerId:string;
  TktNo, TktRev:string;
begin
  qryTicketVersion.Close;
  DBISAMDatabase1.Close;
  DBISAMDatabase1.Directory:=paramStr(1);
  DBISAMDatabase1.Connected:=true;
  sTktVerId :=paramStr(2);
  qryTicketVersion.ParamByName('TicketVersionID').value := StrToInt(sTktVerId);


  qryTicketVersion.Open;
  TktNo:= qryTicketVersion.FieldByName('ticket_number').AsString;
  TktRev:= qryTicketVersion.FieldByName('ticket_revision').AsString;
  ppReport1.TextFileName:= '($MyDocuments)\Report_'+sTktVerId+'_'+TktNo+'-'+TktRev+'.pdf';

  ppReport1.Print;

end;

procedure TdmPrint2PDF.DataModuleDestroy(Sender: TObject);
begin
  qryTicketVersion.Close;
  DBISAMDatabase1.Close;
end;

end.
