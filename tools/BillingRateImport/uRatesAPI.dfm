�
 TFRMRATESAPI 0�Y  TPF0TfrmRatesAPIfrmRatesAPILeft Top CaptionE                                                  Raters Got To Rate ClientHeightiClientWidth<Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSegoe UI
Font.Style 
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreate
TextHeight 	TSplitter	Splitter1Left Top� Width<Height	CursorcrVSplitAlignalTop  
TStatusBar
StatusBar1Left TopVWidth<HeightPanelsTextConnected toWidthP TextNot ConnectedWidth�  TextSuccessWidth2 Width TextFailedWidth# Width TextTotalWidth& Width TextverWidth Width2  ExplicitTopUExplicitWidth8  TPanelPanel1Left Top Width<Height� AlignalTopTabOrderExplicitWidth8 TcxGridcxGrid1LeftTopWidth:Height� AlignalClientTabOrder LookAndFeel.Kind
lfOffice11LookAndFeel.SkinNameDevExpressStyleExplicitWidth6 TcxGridDBTableViewcxGrid1DBTableView1Navigator.Buttons.CustomButtons &ScrollbarAnnotations.CustomAnnotations DataController.DataSourcedsRateTableDataController.KeyFieldNamesbr_id/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups OptionsData.CancelOnExitOptionsData.Deleting OptionsData.DeletingConfirmationOptionsData.EditingOptionsData.InsertingOptionsSelection.CellSelectOptionsView.GroupByBoxOptionsView.Indicator	Styles.UseOddEvenStylesbTrueOnColumnSizeChanged$cxGrid1DBTableView1ColumnSizeChanged TcxGridDBColumncxGrid1DBTableView1br_idDataBinding.FieldNamebr_idDataBinding.IsNullValueType	Width/  TcxGridDBColumncxGrid1DBTableView1call_centerCaptionCall centerDataBinding.FieldNamecall_centerDataBinding.IsNullValueType	MinWidthWidthC  TcxGridDBColumncxGrid1DBTableView1CWICodeDataBinding.FieldNameCWICodeDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGrid1DBTableView1AddlCWICodeDataBinding.FieldNameAddlCWICodeDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGrid1DBTableView1billing_ccCaptionClientDataBinding.FieldName
billing_ccDataBinding.IsNullValueType	  TcxGridDBColumncxGrid1DBTableView1statusDataBinding.FieldNamestatusDataBinding.IsNullValueType	Width'  TcxGridDBColumncxGrid1DBTableView1partiesDataBinding.FieldNamepartiesDataBinding.IsNullValueType	Width*  TcxGridDBColumncxGrid1DBTableView1rateDataBinding.FieldNamerateDataBinding.IsNullValueType	Width/  TcxGridDBColumn"cxGrid1DBTableView1additional_rateCaption
Add'l RateDataBinding.FieldNameadditional_rateDataBinding.IsNullValueType	WidthA  TcxGridDBColumncxGrid1DBTableView1bill_typeCaption	Bill typeDataBinding.FieldName	bill_typeDataBinding.IsNullValueType	  TcxGridDBColumncxGrid1DBTableView1Rpt_glCaption
Rpt GLcodeDataBinding.FieldNameRpt_glDataBinding.IsNullValueType	WidthI  TcxGridDBColumn$cxGrid1DBTableView1Additional_Rpt_glCaptionAdd'l Rpt GLcodeDataBinding.FieldNameAdditional_Rpt_glDataBinding.IsNullValueType		FixedKindfkLeftDynamicWidthc  TcxGridDBColumn!cxGrid1DBTableView1line_item_textCaptionLine Item textDataBinding.FieldNameline_item_textDataBinding.IsNullValueType	Width�   TcxGridDBColumn,cxGrid1DBTableView1additional_line_item_textCaptionAdd'l Line Item textDataBinding.FieldNameadditional_line_item_textDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGrid1DBTableView1bill_codeCaption	Bill codeDataBinding.FieldName	bill_codeDataBinding.IsNullValueType	  TcxGridDBColumncxGrid1DBTableView1area_nameCaptionAreaDataBinding.FieldName	area_nameDataBinding.IsNullValueType	Width2  TcxGridDBColumncxGrid1DBTableView1work_countyCaptionCountyDataBinding.FieldNamework_countyDataBinding.IsNullValueType	WidthF  TcxGridDBColumncxGrid1DBTableView1work_cityCaptionCityDataBinding.FieldName	work_cityDataBinding.IsNullValueType	Width.  TcxGridDBColumn cxGrid1DBTableView1modified_dateCaptionLast ChangedDataBinding.FieldNamemodified_dateDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView1which_invoiceCaptionInvoiceDataBinding.FieldNamewhich_invoiceDataBinding.IsNullValueType	   TcxGridLevelcxGrid1Level1GridViewcxGrid1DBTableView1    TPanelPanel2Left TopWidth<HeightAlignalClientTabOrderExplicitWidth8ExplicitHeight TLabelLabel3LeftTopWidthOHeightCaptionAfter ChangesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSegoe UI
Font.StylefsBold 
ParentFont  TcxGridcxGrid2LeftTopWidth:HeightAlignalClientTabOrder LookAndFeel.Kind
lfOffice11LookAndFeel.SkinNameDevExpressStyleExplicitWidth6ExplicitHeight TcxGridDBTableViewcxGridDBTableView2Navigator.Buttons.CustomButtons &ScrollbarAnnotations.CustomAnnotations DataController.DataSourcedsRateTableAfterDataController.KeyFieldNamesbr_id/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups OptionsData.CancelOnExitOptionsData.Deleting OptionsData.DeletingConfirmationOptionsData.EditingOptionsData.InsertingOptionsSelection.CellSelectOptionsView.GroupByBoxOptionsView.Indicator	Styles.UseOddEvenStylesbTrueOnColumnSizeChanged#cxGridDBTableView2ColumnSizeChanged TcxGridDBColumncxGridDBTableView2br_idDataBinding.FieldNamebr_idDataBinding.IsNullValueType	Width)  TcxGridDBColumncxGridDBTableView2call_centerCaptionCall centerDataBinding.FieldNamecall_centerDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2CWICodeDataBinding.FieldNameCWICodeDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGridDBTableView2AddlCWICodeDataBinding.FieldNameAddlCWICodeDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGridDBTableView2billing_ccCaptionClientDataBinding.FieldName
billing_ccDataBinding.IsNullValueType	Width>  TcxGridDBColumncxGridDBTableView2statusDataBinding.FieldNamestatusDataBinding.IsNullValueType	Width%  TcxGridDBColumncxGridDBTableView2partiesDataBinding.FieldNamepartiesDataBinding.IsNullValueType	Width-  TcxGridDBColumncxGridDBTableView2rateDataBinding.FieldNamerateDataBinding.IsNullValueType	Width+  TcxGridDBColumn!cxGridDBTableView2additional_rateCaption
Add'l rateDataBinding.FieldNameadditional_rateDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2bill_typeCaption	bill typeDataBinding.FieldName	bill_typeDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2Rpt_glCaption
Rpt GLcodeDataBinding.FieldNameRpt_glDataBinding.IsNullValueType	WidthI  TcxGridDBColumn#cxGridDBTableView2Additional_Rpt_glCaptionAdd'l Rpt GLcodeDataBinding.FieldNameAdditional_Rpt_glDataBinding.IsNullValueType	Widthp  TcxGridDBColumn cxGridDBTableView2line_item_textCaptionLine Item TextDataBinding.FieldNameline_item_textDataBinding.IsNullValueType	Width�   TcxGridDBColumn+cxGridDBTableView2additional_line_item_textCaptionAdd'l Line item textDataBinding.FieldNameadditional_line_item_textDataBinding.IsNullValueType	Width�   TcxGridDBColumncxGridDBTableView2bill_codeCaption	bill codeDataBinding.FieldName	bill_codeDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2area_nameCaptionAreaDataBinding.FieldName	area_nameDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2work_countyCaptionCountyDataBinding.FieldNamework_countyDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2work_cityCaptionCityDataBinding.FieldName	work_cityDataBinding.IsNullValueType	Width0  TcxGridDBColumncxGridDBTableView2modified_dateCaptionLast ChangedDataBinding.FieldNamemodified_dateDataBinding.IsNullValueType	  TcxGridDBColumncxGridDBTableView2which_invoiceCaptioninvoiceDataBinding.FieldNamewhich_invoiceDataBinding.IsNullValueType	   TcxGridLevelcxGridLevel2GridViewcxGridDBTableView2    TPanelPanel3Left TopWidth<HeightPAlignalBottomLocked	TabOrderExplicitTopExplicitWidth8 TLabelLabel1LeftkTopWidth:HeightCaptionCall Center  TLabelLabel2Left TopWidthbHeightCaptionBilling Client Code  TButtonbtnLoadJsonLeft�TopWidthqHeightCaption
Load RatesTabOrderOnClickbtnLoadJsonClick  TcxComboBoxcbCallCentersLeftkTopEnabledProperties.ReadOnlyProperties.OnCloseUpcbCallCentersPropertiesCloseUpTabOrder Widthy  TcxComboBoxcbBillingClientsLeft TopEnabledProperties.ReadOnlyProperties.OnCloseUp!cbBillingClientsPropertiesCloseUpTabOrderWidthb  TRadioGroup	rgCompanyLeftYTopWidthxHeight%Caption        CompanyColumns	ItemIndex Items.StringsUTQLOC TabOrderOnClickrgCompanyClick  	TCheckBoxcbActCallCentersLeftjTop;WidthzHeightCaptionActive Call CentersChecked	State	cbCheckedTabOrderOnClickcbActCallCentersClick  	TCheckBoxcbActClientCodesLeft Top<WidthzHeightCaptionActive Client CodesChecked	State	cbCheckedTabOrderOnClickcbActClientCodesClick  	TCheckBox
cbTestJSONLeftTop WidthaHeightCaption	Test JSONTabOrderVisibleOnClickcbTestJSONClick   TDataSourcedsRateTableDataSetqryRateTableLeft�Top�   TADOConnectionADOConnLoginPromptProviderMSOLEDBSQL.1Left@TopP  	TADOQueryqryRateTable
ConnectionADOConn
CursorTypectStatic
ParametersName
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue3003 Name	BillingCC
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
ValueMID70  SQL.Stringsselect *FROM [QM].[dbo].[billing_rate]where call_center = :CallCenterand billing_cc = :BillingCCorder by br_id  Left�Top�   	TADOQueryinsRate
ConnectionADOConn
ParametersName
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  Name	BillingCC
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value  NameStatus
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  NameParties
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NameRate
AttributespaSigned
paNullable DataTypeftBCDNumericScale	PrecisionSizeValue  NameBillCode
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value  NameBillType
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value  NameAreaName
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value  Name
WorkCounty
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value  NameLineItemText
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size<Value  NameWorkCity
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value  NameModifiedDate
Attributes
paNullable DataType
ftDateTimeNumericScale	PrecisionSizeValue  NameWhichInvoice
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  NameAdditionalRate
AttributespaSigned
paNullable DataTypeftBCDNumericScale	PrecisionSizeValue  NameAdditionalLineItemText
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size<Value  NameCWICode
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� SizeValue  NameRptGl
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� SizeValue   Prepared	SQL.Strings INSERT INTO [dbo].[billing_rate]           ([call_center]           ,[billing_cc]           ,[status]           ,[parties]           ,[rate]           ,[bill_code]           ,[bill_type]           ,[area_name]           ,[work_county]           ,[line_item_text]           ,[work_city]           ,[modified_date]           ,[which_invoice]           ,[additional_rate]'           ,[additional_line_item_text]           ,[CWICode]           ,[Rpt_gl])     VALUES (:CallCenter           ,:BillingCC           ,:Status           ,:Parties           ,:Rate           ,:BillCode           ,:BillType           ,:AreaName           ,:WorkCounty           ,:LineItemText           ,:WorkCity           ,:ModifiedDate           ,:WhichInvoice           ,:AdditionalRate#           ,:AdditionalLineItemText           ,:CWICode           ,:RptGl)         LeftTop  TRESTClientRESTClient1Accept5application/json, text/plain; q=0.9, text/html;q=0.8,AcceptCharsetutf-8, *;q=0.8BaseURL1https://erp-api-proxy-uat.services.certusview.comParams SynchronizedEventsLeft�TopP  TRESTRequestRESTRequest1AssignedValuesrvConnectTimeoutrvReadTimeout ClientRESTClient1ParamsKindpkHTTPHEADERName	X-API-KEYValue 85VTnoK58579708ujKOUQaSxXbLY7jm1  ResponseRESTResponse1SynchronizedEventsLeftPTopP  TRESTResponseRESTResponse1ContentTypeapplication/jsonLeft�TopP  	TADOQuerydelRate
ConnectionADOConn
ParametersName
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  Name	BillingCC
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value   Prepared	SQL.Stringsdeletefrom billing_ratewhere Call_Center=:CallCenterand Billing_CC= :BillingCC LeftHTop�   	TADOQueryupdRate
ConnectionADOConn
ParametersNameAddlCWICode
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size(Value  NameAdditionalRptGL
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� SizeValue  NameAdditionalLineItemText
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size<Value  NameAdditionalRate
AttributespaSigned
paNullable DataTypeftBCDNumericScale	PrecisionSizeValue  Name
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  Name	BillingCC
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value  NameStatus
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  NameParties
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NameBillType
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value  NameAreaName
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value  NameWorkCity
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value  Name
WorkCounty
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size(Value   Prepared	SQL.StringsUSE [QM]    UPDATE [dbo].[billing_rate]   SET "       [AddlCWICode]= :AddlCWICode.      ,[Additional_Rpt_gl]=   :AdditionalRptGL<      ,[additional_line_item_text] = :AdditionalLineItemText)      ,[additional_rate]= :AdditionalRate WHERE     [call_center] = :CallCenterand [billing_cc] = :BillingCCand [status] = :Statusand [Parties] =:Partiesand [bill_type]= :BillType    and [area_name]   = :AreaNameand [work_city]   = :WorkCityand [work_county] = :WorkCounty         Left@Top  TdxSkinControllerdxSkinController1SkinNameDevExpressStyleLeft�Top@  	TADOQueryqryCallCenters
ConnectionADOConn
ParametersNameCompany
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NameActive
Attributes
paNullable DataType	ftBooleanNumericScale� 	Precision� SizeValue   SQL.Strings+ SELECT distinct c.[call_center] CallCenter  FROM [dbo].[client] cJ  inner join [QM].[dbo].[call_center] cc on (cc.[cc_code]=c.[call_center])B  inner join [dbo].[customer] cus on cus.customer_id=c.customer_id)  where cus.[locating_company] = :Company  and c.customer_id <> 1987&  and c.client_name not like '%Audit%'  and c.[active] >:Activeorder by CallCenter  Left� Top�   	TADOQueryqryBillingClients
ConnectionADOConn
ParametersName
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  NameActive
Attributes
paNullable DataType	ftIntegerNumericScale� 	Precision� SizeValue   SQL.Strings'SELECT distinct([oc_code]) as BillingCC  FROM [QM].[dbo].[client]#  where [call_center] = :CallCenter  and active > :Active  order by [oc_code]  Left Top�   	TADOQueryqryRateTableAfter
ConnectionADOConn
CursorTypectStatic
ParametersName
CallCenter
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue  Name	BillingCC
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size
Value   SQL.Stringsselect *from billing_ratewhere call_center = :CallCenterand billing_cc = :BillingCCorder by br_id  LeftxTopp  TDataSourcedsRateTableAfterDataSetqryRateTableAfterLeft�Topp  	TADOQueryupdFlatRate
ConnectionADOConn
ParametersNameFlatRate
AttributespaSigned
paNullable DataTypeftBCDNumericScale	PrecisionSizeValue  NameClientID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   Prepared	SQL.StringsUSE [QM]  update BOC  Set [flat_fee] = :FlatRate-  From [QM].[dbo].[billing_output_config] BOCI  inner join [dbo].[client_customer] CC on CC.customer_id=BOC.customer_idE  inner join [dbo].[customer] CUS on BOC.customer_id =CUS.customer_id9  inner join [dbo].[client] C on CC.client_id=C.client_id  where CUS.active =1  and C.client_id = :ClientID         LeftxTop  TcxStyleRepositorycxStyleRepository1Left� Top�PixelsPerInch` TcxStylecxStyle1AssignedValuessvColor Color���   TcxStylecxStyle2AssignedValuessvColorsvFontsvTextColor Color�pQ Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 	TextColorclWhite  TcxStylecxStyle3AssignedValuessvColorsvFontsvTextColor Color�pQ Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 	TextColorclWhite  TcxStylecxStyle4AssignedValuessvColorsvFontsvTextColor ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclBlack  TcxStylecxStyle5AssignedValuessvColorsvFontsvTextColor Color¿� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclBlack  TcxStylecxStyle6AssignedValuessvColorsvFontsvTextColor Color��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclBlack  TcxStylecxStyle7AssignedValuessvColorsvFontsvTextColor Color�pQ Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclWhite  TcxStylecxStyle8AssignedValuessvColorsvFontsvTextColor Color��x Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclWhite  TcxStylecxStyle9AssignedValuessvColorsvFontsvTextColor Color¿� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 	TextColorclBlack  TcxStyle	cxStyle10AssignedValuessvColorsvFontsvTextColor Color��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.StylefsItalic 	TextColorclMaroon  TcxStyle	cxStyle11AssignedValuessvColorsvFontsvTextColor Color��a Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 	TextColorclWhite  TcxStyle	cxStyle12AssignedValuessvColor Color���   TcxStyle	cxStyle13AssignedValuessvColorsvFontsvTextColor Color޸� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 	TextColorclBlack  TcxStyle	cxStyle14AssignedValuessvColorsvFontsvTextColor Color޸� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 	TextColorclBlack  TcxStyle	cxStyle15AssignedValuessvColorsvFontsvTextColor Color��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.Style 	TextColorp   TcxStyle	cxStyle16AssignedValuessvColorsvFontsvTextColor Color�ϕ Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.Style 	TextColorclBlack  TcxStyle	cxStyle17AssignedValuessvColorsvFontsvTextColor Color��z Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.Style 	TextColorclBlack  TcxStyle	cxStyle18AssignedValuessvColorsvFontsvTextColor Color޸� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 	TextColorclBlack  TcxStyle	cxStyle19AssignedValuessvColorsvFontsvTextColor Color�y? Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 	TextColorclWhite  TcxStyle	cxStyle20AssignedValuessvColorsvFontsvTextColor Color޸� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 	TextColorclBlack  TcxStyle	cxStyle21AssignedValuessvColorsvFontsvTextColor ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 	TextColorclOlive  TcxStyle	cxStyle22AssignedValuessvColorsvFontsvTextColor Color_@! Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 	TextColorclWhite  TcxTreeListStyleSheetTreeListStyleSheetBrickCaptionBrickStyles.ContentcxStyle4Styles.InactivecxStyle8Styles.Selection	cxStyle11Styles.BandBackgroundcxStyle1Styles.BandHeadercxStyle2Styles.ColumnHeadercxStyle3Styles.ContentEvencxStyle5Styles.ContentOddcxStyle6Styles.FootercxStyle7Styles.IndicatorcxStyle9Styles.Preview	cxStyle10BuiltIn	  TcxTreeListStyleSheetTreeListStyleSheetUserFormat1CaptionUserFormat1Styles.Content	cxStyle15Styles.Inactive	cxStyle19Styles.Selection	cxStyle22Styles.BandBackground	cxStyle12Styles.BandHeader	cxStyle13Styles.ColumnHeader	cxStyle14Styles.ContentEven	cxStyle16Styles.ContentOdd	cxStyle17Styles.Footer	cxStyle18Styles.Indicator	cxStyle20Styles.Preview	cxStyle21BuiltIn	    