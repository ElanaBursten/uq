program BillingRatesImporter;
 //qm-848
uses
  Vcl.Forms,
  SysUtils,
  uRatesAPI in 'uRatesAPI.pas' {frmRatesAPI},
  GlobalSU in '..\..\common\GlobalSU.pas';

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName+'_'+ParamStr(1));
  LocalInstance:=MyInstanceName;
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1) = 'GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm := True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm := False;
    end;
    Application.Title := '';

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmRatesAPI, frmRatesAPI);
  Application.Run;
  end
  else
    Application.Terminate;
end.

