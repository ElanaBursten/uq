unit uRatesAPI;
 //qm-948   was 848
interface
{
'X-API-KEY: Cr6WnEOy31o82Ko2vQk4RBeg25rX7n3u'
Base URL: 'https://erp-api-proxy-uat.services.certusview.com
End Point: /api/v1/UTQCWIRates
}
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,System.JSON,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, REST.Types,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, cxDBData, cxCurrencyEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Vcl.ExtCtrls, cxCalendar, dxSkinBasic, dxCore,
  dxSkinsForm, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCheckBox, dxSkinDevExpressStyle, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinWXI, cxTL;


  type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

type
  TfrmRatesAPI = class(TForm)
    dsRateTable: TDataSource;
    ADOConn: TADOConnection;
    qryRateTable: TADOQuery;
    insRate: TADOQuery;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    delRate: TADOQuery;
    updRate: TADOQuery;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    btnLoadJson: TButton;
    dxSkinController1: TdxSkinController;
    qryCallCenters: TADOQuery;
    qryBillingClients: TADOQuery;
    cbCallCenters: TcxComboBox;
    cbBillingClients: TcxComboBox;
    Label1: TLabel;
    Label2: TLabel;
    rgCompany: TRadioGroup;
    cbActCallCenters: TCheckBox;
    cbActClientCodes: TCheckBox;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    qryRateTableAfter: TADOQuery;
    dsRateTableAfter: TDataSource;
    Label3: TLabel;
    updFlatRate: TADOQuery;
    cbTestJSON: TCheckBox;
    cxGrid1DBTableView1br_id: TcxGridDBColumn;
    cxGrid1DBTableView1call_center: TcxGridDBColumn;
    cxGrid1DBTableView1billing_cc: TcxGridDBColumn;
    cxGrid1DBTableView1status: TcxGridDBColumn;
    cxGrid1DBTableView1parties: TcxGridDBColumn;
    cxGrid1DBTableView1rate: TcxGridDBColumn;
    cxGrid1DBTableView1bill_code: TcxGridDBColumn;
    cxGrid1DBTableView1bill_type: TcxGridDBColumn;
    cxGrid1DBTableView1area_name: TcxGridDBColumn;
    cxGrid1DBTableView1work_county: TcxGridDBColumn;
    cxGrid1DBTableView1line_item_text: TcxGridDBColumn;
    cxGrid1DBTableView1work_city: TcxGridDBColumn;
    cxGrid1DBTableView1modified_date: TcxGridDBColumn;
    cxGrid1DBTableView1additional_rate: TcxGridDBColumn;
    cxGrid1DBTableView1additional_line_item_text: TcxGridDBColumn;
    cxGrid1DBTableView1CWICode: TcxGridDBColumn;
    cxGrid1DBTableView1AddlCWICode: TcxGridDBColumn;
    cxGrid1DBTableView1Rpt_gl: TcxGridDBColumn;
    cxGrid1DBTableView1Additional_Rpt_gl: TcxGridDBColumn;
    cxGridDBTableView2br_id: TcxGridDBColumn;
    cxGridDBTableView2call_center: TcxGridDBColumn;
    cxGridDBTableView2billing_cc: TcxGridDBColumn;
    cxGridDBTableView2status: TcxGridDBColumn;
    cxGridDBTableView2parties: TcxGridDBColumn;
    cxGridDBTableView2rate: TcxGridDBColumn;
    cxGridDBTableView2bill_code: TcxGridDBColumn;
    cxGridDBTableView2bill_type: TcxGridDBColumn;
    cxGridDBTableView2area_name: TcxGridDBColumn;
    cxGridDBTableView2work_county: TcxGridDBColumn;
    cxGridDBTableView2line_item_text: TcxGridDBColumn;
    cxGridDBTableView2work_city: TcxGridDBColumn;
    cxGridDBTableView2modified_date: TcxGridDBColumn;
    cxGridDBTableView2which_invoice: TcxGridDBColumn;
    cxGridDBTableView2additional_rate: TcxGridDBColumn;
    cxGridDBTableView2additional_line_item_text: TcxGridDBColumn;
    cxGridDBTableView2CWICode: TcxGridDBColumn;
    cxGridDBTableView2AddlCWICode: TcxGridDBColumn;
    cxGridDBTableView2Rpt_gl: TcxGridDBColumn;
    cxGridDBTableView2Additional_Rpt_gl: TcxGridDBColumn;
    cxGridDBTableView1which_invoice: TcxGridDBColumn;
    Splitter1: TSplitter;
    cxStyleRepository1: TcxStyleRepository;
    TreeListStyleSheetBrick: TcxTreeListStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    TreeListStyleSheetUserFormat1: TcxTreeListStyleSheet;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure btnLoadJsonClick(Sender: TObject);
    procedure cbCallCentersPropertiesCloseUp(Sender: TObject);
    procedure rgCompanyClick(Sender: TObject);
    procedure cbBillingClientsPropertiesCloseUp(Sender: TObject);
    procedure cbActCallCentersClick(Sender: TObject);
    procedure cbActClientCodesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure cbTestJSONClick(Sender: TObject);
    procedure cxGrid1DBTableView1ColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure cxGridDBTableView2ColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    flogDir: wideString;
    BASE_URL:wideString;
    Company:string;
    CompanyID:integer;
    fActiveCallCentersOnly:integer;  //-1 is all.  0 is active only
    fActiveClientCodesOnly:integer;  //-1 is all.  0 is active only
    function GetAppVersionStr: string;
    function ProcessINI: boolean;
    function connectDB: boolean;
    procedure clearLogRecord;
    procedure WriteLog(LogResult: TLogResults);
    function PostRateToDatabase(retJSON : WideString): boolean;
    procedure PopCallCenterDropdown;
    procedure PopBillingCC;
    function EnDeCrypt(const Value: String): String;

  public
    { Public declarations }
    UseGUI:boolean;

    ActiveCallCenter, ActiveBillingCC:string;
    ActiveClientID, ActiveCustomerID:integer;
  end;

var
  frmRatesAPI: TfrmRatesAPI;
  LocalInstance:string;
  testJSON:TStringList;
CONST
 SPC=#32; //%20
 //REQUEST_URL ='UTQCWIRates?company_id=UTQ&limit=500&page=1&restriction=%22billing_cc%22%20%3D%20%22%s%22%20and%20%22call_center%22%20%3D%20%22%s%22;';
 REQUEST_URL ='UTQCWIRates?company_id=%s&limit=500&page=1&restriction="call_center" = "%s" and "billing_cc" = "%s"';
 //REQUEST_URL ='https://erp-api-proxy-uat.services.certusview.com/api/v1/UTQCWIRates?company_id=UTQ&limit=5&page=1&restriction="call_center" = "%s" and "Modified_date" >= "%s"';
 //REQUEST_URL ='https://erp-api-proxy-qa.services.certusview.com/api/v1/UTQCWIRates?company_id=UTQ&limit=500&page=1&restriction=%22billing_cc%22%20%3D%20%22%s%22%20and%20%22call_center%22%20%3D%20%22%s%22';
  //'https://erp-api-proxy-qa.services.certusview.com/api/v1/UTQCWIRates?company_id=UTQ&limit=50&page=1&restriction=%22billing_cc%22%20%3D%20%22GPU%22%20and%20%22call_center%22%20%3D%20%22NewJersey%22';
 DBLQUOTES = #34;  //%22

implementation
uses IniFiles,System.DateUtils, StrUtils;
{$R *.dfm}


procedure TfrmRatesAPI.FormActivate(Sender: TObject);
begin
  rgCompanyClick(nil);
//  PopCallCenterDropdown;
end;

procedure TfrmRatesAPI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryRateTable.close;
  qryRateTableAfter.close;
  Action:= caFree;
end;

procedure TfrmRatesAPI.FormCreate(Sender: TObject);
begin
  Company:='UTQ';
  CompanyID := 1;
  ActiveCallCenter:='';
  ActiveBillingCC:='';
  StatusBar1.Panels[9].Text:= GetAppVersionStr;
  fActiveCallCentersOnly:= 0;
  fActiveClientCodesOnly:= 0;
  forcedirectories(extractfiledir(paramstr(0))+'\JSONS\');
  ProcessINI;
  ConnectDB;
  if paramstr(1) = 'test' then cbTestJSON.Visible:=true;

  self.show;
end;

procedure TfrmRatesAPI.btnLoadJsonClick(Sender: TObject);
var
  MyBigURL, retJSON: wideString;
  testCallCenter, testClient:string;
begin
  ActiveCallCenter:='';
  ActiveBillingCC:='';
  ActiveCallCenter:=trim(cbCallCenters.Text);
  ActiveBillingCC:=trim(cbBillingClients.text);

  MyBigURL := '';
  retJSON := '';
  if cbTestJSON.Checked=False then
  begin
    MyBigURL := BASE_URL+Format(REQUEST_URL, [Company, ActiveCallCenter, ActiveBillingCC]);

    RESTClient1.BaseURL := MyBigURL;
  //  RESTClient1.BaseURL :='https://erp-api-proxy-qa.services.certusview.com/api/v1/UTQCWIRates?company_id=UTQ&limit=50&page=1&restriction=%22billing_cc%22%20%3D%20%22GPU%22%20and%20%22call_center%22%20%3D%20%22NewJersey%22';

    RESTRequest1.Execute;

    retJSON := RESTResponse1.Content;
  end
  else
  begin
    testJson:=TStringList.Create;
    testJson.LoadFromFile(extractfiledir(paramstr(0))+'\JSONS\testJSON.json');
    testJson.Text;
//    testCallCenter, testClient
    RESTResponse1.StatusCode:=200;
    RESTResponse1.StatusText:='Test JSON';
    retJSON:=testJson.Text;
    testJson.Free;
  end;

  LogResult.LogType := ltInfo;
  LogResult.MethodName:='LoadJson';
  LogResult.Status:= RESTClient1.BaseURL+'  '+'RESTResponse1.StatusText: ' + RESTResponse1.StatusText;
  LogResult.DataStream := retJSON;;
  WriteLog(LogResult);


  if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
  begin
    try
      PostRateToDatabase(retJSON);
    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.ExcepMsg := E.Message;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
        LogResult.DataStream := RESTResponse1.Content;
        WriteLog(LogResult);
      end;
    end;
  end
  else
  begin
    LogResult.LogType := ltError;
    LogResult.ExcepMsg := ' Body Sent ' + retJSON;
    LogResult.DataStream := RESTResponse1.Content;
    WriteLog(LogResult);
  end;
    qryRateTableAfter.Parameters.ParamByName('BillingCC').Value:= ActiveBillingCC;
    qryRateTableAfter.Parameters.ParamByName('CallCenter').Value:= ActiveCallCenter;
    qryRateTableAfter.Open;
    PopCallCenterDropdown;
end;

Function TfrmRatesAPI.PostRateToDatabase(retJSON: wideString): boolean;
var
  ja, jaAU: TJSONArray;
  JSonValue,JSonValueAU: TJSonValue;
  i,ii: integer;
  aCWICode,sBilltype: string;
  ExcelDate:double;
  ModDate:TDateTime;
  cntGood, cntBad, cntDelRate, cntInsRate, cntUpdRate: integer;
  function getData(JSonValue: TJSonValue; Field: String): String;
  begin
    Result := '';
    Result := JSonValue.GetValue<string>(Field);
  end;
  function StripAU(const InputStr: string): string;
  var
    Position: Integer;
  begin
    Position := Pos('-AU', InputStr);
    if Position > 0 then
      Result := Copy(InputStr, 1, Position - 1)
    else
      Result := InputStr;
  end;
const
  sDeleteRate = 'DeleteRate';
begin
  sBilltype:='';
  ja := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(retJSON), 0)
    as TJSONArray;
  jaAU := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(retJSON), 0)
    as TJSONArray;

  i :=0;
  for i := ja.count - 1 downto 0 do
  begin
    aCWICode := '';
    JSonValue := ja.Items[i];
    aCWICode := getData(JSonValue, 'CWICode');

    if pos('-AU', aCWICode)>0 then
    begin
      ja.Remove(i).free;  //Remove AU from this array
    End;
  end;
  i :=0;
  for i := jaAU.count - 1 downto 0 do
  begin
    aCWICode := '';
    JSonValueAU := jaAU.Items[i];
    aCWICode := getData(JSonValueAU, 'CWICode');

    if (pos('-AU', aCWICode)<1) then
    begin
      jaAU.Remove(i).Free; //Remove NON-AU from this array
    End;
  End;

  cntGood := 0;
  cntBad := 0;
  cntDelRate := 0;
  cntInsRate := 0;
  cntUpdRate := 0;

    try
    //Clean-out ALL rates for this call_center and billingCC
      ii:=0;
      delRate.Parameters.ParamByName('CallCenter').Value := ActiveCallCenter;
      delRate.Parameters.ParamByName('billingCC').Value := ActiveBillingCC;
      ii:=delRate.ExecSQL;
    except on E: Exception do
    begin
          LogResult.LogType := ltError;
          LogResult.Status := 'Not able to delRate Rates for: ' + ActiveCallCenter +' '+ActiveBillingCC;
          LogResult.ExcepMsg := 'exception: ' + E.Message;
          WriteLog(LogResult);
      end;
    end;

    for i := 0 to ja.Size - 1 do  //Non-AU CWICodes
    begin
      JSonValue := ja.Items[i];
      If getData(JSonValue, 'line_item_text') = sDeleteRate then
      begin
      {if this rate it to be deleted, do not add it back in.
       this is still being debatebut there can never be a Additional units
       for it, if it is being deleted.
      }
        inc(cntDelRate);
        continue;
      End;
      aCWICode:= getData(JSonValue, 'CWICode');
      insRate.Parameters.ParamByName('CallCenter').Value :=
        getData(JSonValue, 'call_center');
      insRate.Parameters.ParamByName('BillingCC').Value :=
        getData(JSonValue, 'billing_cc');
      insRate.Parameters.ParamByName('Status').Value :=
        getData(JSonValue, 'Status');
      insRate.Parameters.ParamByName('Parties').Value :=
        StrToInt(getData(JSonValue, 'parties'));
      insRate.Parameters.ParamByName('Rate').Value :=
        StrToFloat(getData(JSonValue, 'RATE'));
      insRate.Parameters.ParamByName('BillCode').Value :=
        getData(JSonValue, 'bill_code');
      sBilltype:=getData(JSonValue, 'bill_type');
      if sBilltype='NORM' then insRate.Parameters.ParamByName('BillType').Value :='NORMAL'
      else
      If sBilltype='AFTE' then insRate.Parameters.ParamByName('BillType').Value :='AFTER'
      else
      if sBilltype='EMER' then insRate.Parameters.ParamByName('BillType').Value :='EMERG' //qm-1087 sr
      else
      insRate.Parameters.ParamByName('BillType').Value :=sBilltype;

      insRate.Parameters.ParamByName('AreaName').Value :=
        getData(JSonValue, 'area_name');
      insRate.Parameters.ParamByName('WorkCounty').Value :=
        getData(JSonValue, 'work_county');
      insRate.Parameters.ParamByName('LineItemText').Value :=
        getData(JSonValue, 'line_item_text');
      insRate.Parameters.ParamByName('WorkCity').Value :=
        getData(JSonValue, 'work_city');
      If StrToFloatEx((getData(JSonValue, 'Modified_date')),ExcelDate) then
      ModDate :=ExcelDate
      else ModDate := ISO8601ToDate(getData(JSonValue, 'Modified_date'));
      insRate.Parameters.ParamByName('ModifiedDate').Value :=ModDate;
      insRate.Parameters.ParamByName('WhichInvoice').Value :=
        getData(JSonValue, 'which_invoice');
      insRate.Parameters.ParamByName('AdditionalRate').Value :=NULL;
      insRate.Parameters.ParamByName('AdditionalLineItemText').Value :=
        getData(JSonValue,'line_item_text');
      insRate.Parameters.ParamByName('CWICode').Value := aCWICode;
      insRate.Parameters.ParamByName('RptGl').Value :=
        getData(JSonValue, 'Rpt_GL');

      try
        ii:=0;
        ii:=insRate.ExecSQL;
        inc(cntGood);
        inc(cntInsRate);
      except
        on E: Exception do
        begin
          inc(cntBad);
          dec(cntGood);
          LogResult.LogType := ltError;
          LogResult.Status := 'Not able to insRate Rate: ' + aCWICode;
          LogResult.ExcepMsg := 'exception: ' + E.Message;
          WriteLog(LogResult);
          continue;
        end;
      end;
    End; //for i := 0 to ja.Size - 1 do
    ja.Free;

    i := 0;
    for i := 0 to jaAU.Size - 1 do    //AU CWICodes
    begin
      JSonValueAU := jaAU.Items[i];
//      showmessage(getData(JSonValueAU, 'CWICode'));
      var a:string;
      a:=(getData(JSonValueAU, 'RATE'));  //test
      sBilltype:='';
//      updRate.Parameters.ParamByName('CWICode').Value := getData(JSonValueAU,'CWICode'); //adding
      updRate.Parameters.ParamByName('CallCenter').Value :=
        getData(JSonValueAU, 'call_center');
      updRate.Parameters.ParamByName('BillingCC').Value :=
        getData(JSonValueAU, 'billing_cc');
      updRate.Parameters.ParamByName('Status').Value :=StripAU(
        getData(JSonValueAU, 'Status'));
      updRate.Parameters.ParamByName('Parties').Value :=
        StrToInt(getData(JSonValueAU, 'parties'));

      sBilltype:= getData(JSonValueAU, 'bill_type');
      if sBilltype='NORM' then updRate.Parameters.ParamByName('BillType').Value :='NORMAL'
      else If sBilltype='AFTE' then updRate.Parameters.ParamByName('BillType').Value :='AFTER'
      else updRate.Parameters.ParamByName('BillType').Value :=sBilltype;

      updRate.Parameters.ParamByName('AreaName').Value :=
        getData(JSonValueAU, 'area_name');
      updRate.Parameters.ParamByName('WorkCity').Value :=
        getData(JSonValueAU, 'work_city');
      updRate.Parameters.ParamByName('WorkCounty').Value :=
        getData(JSonValueAU, 'work_county');

      //End Where Clause parameters
//      updRate.Parameters.ParamByName('Rate').Value :=
//        StrToFloat(getData(JSonValueAU, 'RATE'));
      updRate.Parameters.ParamByName('AdditionalRate').Value :=StrToFloat(getData(JSonValueAU, 'RATE'));
      updRate.Parameters.ParamByName('AdditionalLineItemText').Value :=
        getData(JSonValueAU,'line_item_text'); //12/2/24
      updRate.Parameters.ParamByName('AdditionalRptGL').Value :=
        getData(JSonValueAU,'Rpt_GL');
      updRate.Parameters.ParamByName('AddlCWICode').Value :=  getData(JSonValueAU,'CWICode');  //with AU

      try
        updRate.ExecSQL;
        inc(cntGood);
        inc(cntUpdRate);
        except
      on E: Exception do
        begin
          inc(cntBad);
          dec(cntGood);
          LogResult.LogType := ltError;
          LogResult.Status := 'Not able to Update Rate: ' + aCWICode;
          LogResult.ExcepMsg := 'exception: ' + E.Message;
          WriteLog(LogResult);
          continue;
        end;
      end;
    end;//for i := 0 to jaAU.Size - 1 do
    jaAU.Free;

    StatusBar1.Panels[3].Text := IntToStr(cntGood);
    StatusBar1.Panels[5].Text := IntToStr(cntBad);
    StatusBar1.Panels[7].Text := IntToStr(cntGood+cntBad);

    LogResult.LogType := ltInfo;
    LogResult.MethodName:='Finished PostRateToDatabase';
    LogResult.Status:= ' processed with '+IntToStr(cntInsRate)+' successful '+IntToStr(cntBad)+' failed. ';
    WriteLog(LogResult);

    LogResult.LogType := ltInfo;
    LogResult.MethodName:='Final Counts';
    LogResult.Status:= ' Updated '+IntToStr(cntUpdRate)+' Inserted '+IntToStr(cntBad)+'  failed. ';
    WriteLog(LogResult);
    cntDelRate :=0;
    cntInsRate :=0;
    cntUpdRate :=0;

end;

var
  SyncingColumns: Boolean = False;

procedure SyncColumnWidths(SourceView, TargetView: TcxGridDBTableView);
var
  i: Integer;
begin
  if SyncingColumns then
    Exit; // Prevent recursion-if not it runs forever  LOL

  SyncingColumns := True;
  try
    for i := 0 to SourceView.ColumnCount - 1 do
    begin
      if i < TargetView.ColumnCount then
        TargetView.Columns[i].Width := SourceView.Columns[i].Width;
    end;
  finally
    SyncingColumns := False;
  end;
end;

procedure TfrmRatesAPI.cxGrid1DBTableView1ColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SyncColumnWidths(cxGrid1DBTableView1, cxGridDBTableView2);
end;

procedure TfrmRatesAPI.cxGridDBTableView2ColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SyncColumnWidths(cxGridDBTableView2, cxGrid1DBTableView1);
end;

procedure TfrmRatesAPI.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmRatesAPI.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + LocalInstance +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

function TfrmRatesAPI.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  Result := True;
  ADOConn.close;

  if LeftStr(apassword, 4) = DECRYPT then
  begin
    apassword := Copy(apassword, 5, maxint);
    apassword := EnDeCrypt(apassword);
  end
  else
  begin
    LogResult.LogType := ltWarning;
    LogResult.Status:='Connect to DB';
    LogResult.DataStream:= 'You are using a Clear Text Password.  Please contact IT for the correct Password';
    WriteLog(LogResult);
  end;

  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  LogConnStr := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;

      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := LogConnStr;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aServer;
  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := LogConnStr;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
    end;
  end;
end;

function TfrmRatesAPI.EnDeCrypt(const Value: String): String;
var
  CharIndex: Integer;
begin
  result := Value;
  for CharIndex := 1 to Length(Value) do
    result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TfrmRatesAPI.PopCallCenterDropdown;
begin
  try

    cbCallCenters.Text:='';
    cbCallCenters.Properties.Items.Clear;
    qryCallCenters.close;
    cbBillingClients.Text:='';
    cbBillingClients.Properties.Items.Clear;
    qryCallCenters.Parameters.ParamByName('Active').Value:= fActiveCallCentersOnly; //-1 All 0 ActiveOnly
    qryCallCenters.Parameters.ParamByName('Company').Value:=CompanyID;
    qryCallCenters.open;
    with qryCallCenters do
    begin
      if not(IsEmpty) then
      begin
        while not(eof) do
        begin //
          if FieldByName('CallCenter').asstring<>'' then
          cbCallCenters.Properties.Items.Add(FieldByName('CallCenter').asstring);
          next;
        end;
      end;
    end;
  finally
    qryCallCenters.close;
    cbCallCenters.Enabled:=true;
  end;
end;

procedure TfrmRatesAPI.PopBillingCC;
var
  ClientCode:string;
begin    //fActiveClientCodesOnly:integer;  //-1 is all.  0 is active only
  qryBillingClients.Parameters.ParamByName('Active').value :=fActiveClientCodesOnly;
  qryBillingClients.Parameters.ParamByName('CallCenter').value := cbCallCenters.Text;
  ClientCode:='';
  try
    cbBillingClients.Text:='';
    cbBillingClients.Properties.Items.Clear;
    qryBillingClients.open;

    with qryBillingClients do
    begin
      if not(IsEmpty) then
      begin
        while not(eof) do
        begin //
          if FieldByName('BillingCC').asstring <> '' then
          begin
            ClientCode := FieldByName('BillingCC').AsString;
            cbBillingClients.Properties.Items.Add(ClientCode);
          end;
          next;
        end;
      end;
    end;
  finally
    qryBillingClients.close;
    cbBillingClients.Enabled:=true;
  end;
end;

procedure TfrmRatesAPI.cbActCallCentersClick(Sender: TObject);
begin
  //fActiveCallCentersOnly  -1 is all.  0 is active only
  If cbActCallCenters.Checked then fActiveCallCentersOnly:=0
  else fActiveCallCentersOnly:=-1;
end;

procedure TfrmRatesAPI.cbActClientCodesClick(Sender: TObject);
begin
  //fActiveCallCentersOnly  -1 is all.  0 is active only
  If cbActClientCodes.Checked then fActiveClientCodesOnly:=0
  else fActiveClientCodesOnly:=-1;
end;

procedure TfrmRatesAPI.cbBillingClientsPropertiesCloseUp(Sender: TObject);
begin
  qryRateTableAfter.close;
  ActiveBillingCC  := cbBillingClients.Text;
  qryRateTable.close;
  qryRateTable.Parameters.ParamByName('CallCenter').Value := ActiveCallCenter;
  qryRateTable.Parameters.ParamByName('BillingCC').Value := ActiveBillingCC;
  qryRateTable.open;
end;

procedure TfrmRatesAPI.cbCallCentersPropertiesCloseUp(Sender: TObject);
begin
  ActiveCallCenter :=cbCallCenters.Text;
  PopBillingCC;
end;

procedure TfrmRatesAPI.cbTestJSONClick(Sender: TObject);
begin
 if (MessageDlg('This is a test feature that is available with the start-up parameter ''test'' on the comand line.  Place a JSON '+#13+#10+'file in the JSONS directory that I have created under the running directory.  The file MUST be named testJSON.json.'+#13+#10+'Instead of accessing the rest server,  I will read this JSON and process as normal.'+#13+#10+'Then select the proper call center and client and run normally.'+#13+#10+#13+#10+'Select OK to test.'+#13+#10+'Select Abort to not test and load JSON from Rest Server.', mtInformation, [mbOK, mbAbort], 0) in [mrAbort]) then
 cbTestJSON.Checked:=false;
 exit;

end;

function TfrmRatesAPI.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmRatesAPI.ProcessINI: boolean;
var
  BillRateIni: TIniFile;
begin
  try
    BillRateIni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer   := BillRateIni.ReadString('Database', 'Server', '');
    aDatabase := BillRateIni.ReadString('Database', 'DB', 'QM');
    ausername := BillRateIni.ReadString('Database', 'UserName', '');
    apassword := BillRateIni.ReadString('Database', 'Password', '');
    BASE_URL  := BillRateIni.ReadString('Rest', 'BASE_URL', '');
    flogDir := BillRateIni.ReadString('LogPaths', 'LogPath', '');


    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);
  finally
    BillRateIni.Free;
  end;
end;

procedure TfrmRatesAPI.rgCompanyClick(Sender: TObject);
begin
  case rgCompany.ItemIndex of
    - 1:
      Company := '';
    0:
      begin
        Company := 'UTQ';
        CompanyID := 1;
      end;
    1:
      begin
        Company := 'LOC';
        CompanyID := 7;
      end;
  end;
  PopCallCenterDropdown;
end;

end.
