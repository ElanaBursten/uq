object frmPuraImportMain: TfrmPuraImportMain
  Left = 0
  Top = 0
  Caption = 'Pura importer'
  ClientHeight = 564
  ClientWidth = 1199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 1199
    Height = 25
    Align = alTop
    TabOrder = 0
  end
  object pnlBody: TPanel
    Left = 0
    Top = 25
    Width = 1199
    Height = 453
    Align = alClient
    TabOrder = 1
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 1197
      Height = 451
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 478
    Width = 1199
    Height = 67
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 471
      Top = 18
      Width = 50
      Height = 13
      Caption = 'How Many'
    end
    object Label2: TLabel
      Left = 2
      Top = 48
      Width = 63
      Height = 13
      Caption = 'Connected '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 154
      Top = 48
      Width = 42
      Height = 13
      Caption = 'Version'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 256
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Process'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 451
      Top = 48
      Width = 60
      Height = 13
      Caption = 'Start Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 572
      Top = 48
      Width = 60
      Height = 13
      Caption = 'Done Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnProcess: TButton
      Left = 121
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Process'
      TabOrder = 0
      OnClick = btnProcessClick
    end
    object btnOpen: TButton
      Left = 40
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 1
      OnClick = btnOpenClick
    end
    object edtHowMany: TEdit
      Left = 527
      Top = 6
      Width = 57
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      Text = '0'
    end
    object cbUseCount: TCheckBox
      Left = 378
      Top = 6
      Width = 73
      Height = 17
      Caption = 'Use Count'
      TabOrder = 3
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 545
    Width = 1199
    Height = 19
    Panels = <
      item
        Text = 'Not Connected'
        Width = 150
      end
      item
        Text = 'Unknown'
        Width = 100
      end
      item
        Text = 'Process'
        Width = 200
      end
      item
        Text = 'Start Time'
        Width = 120
      end
      item
        Text = 'TimeFinish'
        Width = 120
      end>
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Filter = 'Tab Delimited|*.txt'
    Left = 48
    Top = 160
  end
  object ADOConn: TADOConnection
    CommandTimeout = 120
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 40
    Top = 57
  end
  object insLocate: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ClientID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'INSERT INTO [dbo].[locate]'
      '           ('
      '             ticket_id'
      '            ,client_code'
      '            ,client_id'
      '            ,status'
      '            ,active'
      '            ,invoiced'
      '            ,assigned_to'
      '            ,added_by'
      '            ,assigned_to_id'
      ''
      '           )'
      '     VALUES'
      '           ( :ticket_id'
      '            ,'#39'PURA'#39
      '            ,:ClientID'
      '            ,'#39'-R'#39
      '            ,1'
      '            ,0'
      '            ,7389'
      '            ,3512'
      '            ,7389'
      '           )')
    Left = 328
    Top = 57
  end
  object qryTicket: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticketNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticketFormat'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Select top 1 *'
      'From ticket'
      'where ticket_number = :ticketNumber'
      'and ticket_format =:ticketFormat'
      'order by modified_date desc')
    Left = 240
    Top = 56
  end
  object spAssignLocate: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 120
    ProcedureName = 'assign_locate'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkloadDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 408
    Top = 121
  end
  object insTicket: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'parsed_ok'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'revision'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_description'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_state'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_county'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_city'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_address_street'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_type'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_remarks'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'WoNumber'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'company'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'con_name'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'TicketType'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'route_order'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'insert into ticket'
      
        '(ticket_number,parsed_ok,ticket_format,kind,revision,transmit_da' +
        'te,'
      'due_date,work_description,work_state,work_county,work_city,'
      'work_address_street,work_type,work_remarks,wo_number,'
      'company,con_name,image,ticket_type,recv_manager_id,route_order)'
      ''
      
        'values(:ticket_number,:parsed_ok,:ticket_format,'#39'NORMAL'#39',:revisi' +
        'on ,getdate(),'
      
        'getdate()+2,:work_description,:work_state, :work_county,:work_ci' +
        'ty,'
      ':work_address_street,:work_type,:work_remarks,:WoNumber,'
      ':company,:con_name,'#39'NO IMAGE'#39',:TicketType,7389,:route_order)'
      ''
      ''
      '')
    Left = 176
    Top = 56
  end
  object qryLastLocate: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ClientID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select locate_id'
      'from locate'
      'where ticket_id = :TicketID'
      'and client_id =:ClientID')
    Left = 408
    Top = 56
  end
  object qryClientID: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'Select client_id'
      'From client'
      'where oc_code='#39'PURA'#39
      ''
      '')
    Left = 176
    Top = 144
  end
  object insTicketVersion: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'TicketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'USE QM'
      'insert into ticket_version'
      
        'select ticket_id,revision,ticket_number,ticket_type,transmit_dat' +
        'e,transmit_date,transmit_date,null,'
      'null,'#39'excel'#39','#39'1421'#39','#39'1421'#39
      'from ticket with (nolock)'
      'where ticket_id=:TicketID')
    Left = 320
    Top = 257
  end
end
