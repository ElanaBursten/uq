program TicketPURAimporter;
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  TicketPURAmain in 'TicketPURAmain.pas' {frmPuraImportMain};

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPuraImportMain, frmPuraImportMain);
  Application.Run;
end.
