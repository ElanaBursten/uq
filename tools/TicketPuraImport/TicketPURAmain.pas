unit TicketPURAmain; // qm-639

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.Types,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.ComCtrls, Winapi.Activex,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtDlgs, Vcl.Buttons;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TfrmPuraImportMain = class(TForm)
    pnlTop: TPanel;
    pnlBody: TPanel;
    pnlBottom: TPanel;
    OpenTextFileDialog1: TOpenTextFileDialog;
    Memo1: TMemo;
    ADOConn: TADOConnection;
    btnProcess: TButton;
    btnOpen: TButton;
    insLocate: TADOQuery;
    qryTicket: TADOQuery;
    edtHowMany: TEdit;
    Label1: TLabel;
    cbUseCount: TCheckBox;
    spAssignLocate: TADOStoredProc;
    StatusBar1: TStatusBar;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    insTicket: TADOQuery;
    qryLastLocate: TADOQuery;
    qryClientID: TADOQuery;
    insTicketVersion: TADOQuery;
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);

  private
    LogResult: TLogResults;
    FilePath: string;
    csvFileName: string;
    clientID:integer;
    function OpenFile: String;
    function ConnectDB: boolean;
    function AddLocateRecord(ticketID: integer): boolean;

    procedure WriteLog;
    procedure clearLogRecord;
    procedure ProcessSheet;

    { Private declarations }
  public
    { Public declarations }
    logPath:string;
    fileData: TStrings;
    HotStop: boolean;
    excelPage: TFilename;
    ToProcess: integer;
    function GetAppVersionStr: string;
  end;

var
  frmPuraImportMain: TfrmPuraImportMain;

implementation

uses StrUtils, System.IniFiles;
{$R *.dfm}

procedure TfrmPuraImportMain.btnOpenClick(Sender: TObject);
begin
  OpenFile;

  self.Caption := self.Caption + '  ' + csvFileName;
end;

procedure TfrmPuraImportMain.FormActivate(Sender: TObject);
begin
    try
      qryClientID.Open;
      ClientID := qryClientID.FieldByName('client_id').AsInteger;
    finally
      qryClientID.Close;
    end;

  StatusBar1.Panels[1].text := GetAppVersionStr;
end;

procedure TfrmPuraImportMain.FormCreate(Sender: TObject);
begin
  ConnectDB;
  fileData := TStringList.Create;
  fileData.Delimiter := #9;
  fileData.StrictDelimiter := true;
  fileData.QuoteChar := #34;
end;

procedure TfrmPuraImportMain.FormDestroy(Sender: TObject);
begin
  fileData.Free;
  ADOConn.Close;
end;

function TfrmPuraImportMain.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmPuraImportMain.OpenFile: String;
var
  cnt: integer;
begin
  if OpenTextFileDialog1.Execute(self.Handle) then
  begin
    try
      excelPage := OpenTextFileDialog1.FileName;
      csvFileName:= extractFileName(excelPage);
      fileData.LoadFromFile(excelPage);
      cnt := fileData.Count;
      memo1.Lines.Add('Opened File '+csvFileName+' for processing');
    except
      On E: Exception do
        result := E.Message;
    end;

  end;
end;

procedure TfrmPuraImportMain.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName :=IncludeTrailingBackslash(LogPath) + 'Pura' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmPuraImportMain.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmPuraImportMain.btnProcessClick(Sender: TObject);
begin
   CoInitialize(nil);
   TThread.CreateAnonymousThread(
   procedure
   begin
  ProcessSheet;
   end).Start;
end;

procedure TfrmPuraImportMain.ProcessSheet;
const
  WorkType = 'Bore verification';
  TicketType = 'Bore verification';
  WorkDescription = 'ROAD BORE VERIFICATION';
  WorkDoneFor = 'Parkside';
  RecvManagerId = 7389;

  TicketFormat = '1421';
  TicketKind = 'NORMAL';
  TicketRevision = '';

  TicketDueDate = '11/30/2022';
  ticketImage = 'NO IMAGE';
  CompanyDoingWork = 'Parkside';
  TicketCompany = 'Parkside';
  TicketConName = 'Parkside';
var

  i, ii, cnt, howmany: integer;
  stAddress: string;

  savedPath: string;
  TicketNumber: string;
  ticketID: integer;
  locateID: integer;
  oneStr: TStringDynArray;
  val: double;
  x: integer;
begin
  howmany := 0;
  cnt := 0;
  i := 0;
  ii := 0;
  ToProcess := 0;
  StatusBar1.Panels[2].text := 'Loading ' + csvFileName;
  StatusBar1.Panels[3].text := TimeToStr(Time);
  StatusBar1.Refresh;
  try

    ToProcess := fileData.Count - 1;
    self.Caption := self.Caption + ' Records to process:  ' + IntToStr(ToProcess);
    try

      for i := 1 to fileData.Count - 1 do
      begin
        oneStr := SplitString(fileData[i], #9);
        TicketNumber := oneStr[0];
        If TryStrToFloat(TicketNumber, val) then
        begin
          Memo1.Lines.Add('TicketNo: ' + oneStr[0] + ' ' + 'Job Date: ' + oneStr[1] + ' ' + 'Job Start: ' + oneStr[2] + ' ' +
            'Address: ' + oneStr[3] + ' ' + 'Crew: ' + oneStr[4] + ' ' + 'Project: ' + oneStr[5] + ' ' + 'Town: ' + oneStr[6] + ' '
            + 'County: ' + oneStr[7] + ' ' + 'Route Order: ' + oneStr[8] + ' ' + 'Tkt Count: ' + oneStr[9] + ' ' + 'Note: ' +
            oneStr[10]);
          // extractAddress

          howmany := StrToInt(oneStr[9]);
          x := -1;
          x := pos(#44, oneStr[3]);
          stAddress := Copy(oneStr[3], 2, x - 2); // parse out street
          for ii := 1 to howmany do
          begin
            with insTicket.Parameters do
            begin
              TicketNumber := oneStr[0];
              TicketNumber := TicketNumber + '-' + IntToStr(ii);
              ParamByName('ticket_number').Value := TicketNumber;
              ParamByName('parsed_ok').Value := true;
              ParamByName('ticket_format').Value := TicketFormat;
              ParamByName('revision').Value := TicketRevision;
              ParamByName('work_description').Value := WorkDescription;
              ParamByName('work_state').Value := 'CT';
              ParamByName('work_county').Value := oneStr[7];
              ParamByName('work_city').Value := oneStr[6];
              ParamByName('work_address_street').Value := stAddress;
              ParamByName('work_type').Value := WorkType;
              ParamByName('work_remarks').Value := oneStr[10];
              ParamByName('company').Value := TicketCompany;
              ParamByName('con_name').Value := TicketConName;
              ParamByName('TicketType').Value := TicketType;
              ParamByName('route_order').Value := StrToInt(oneStr[8]);
              ParamByName('WoNumber').Value := oneStr[5];
            end; // with insTicket.Parameters do
            // write ticket
            If insTicket.ExecSQL > 0 then
            begin
              Memo1.Lines.Add('Added Ticket ' + TicketNumber + ' to ticket table');
              qryTicket.Parameters.ParamByName('ticketNumber').Value := TicketNumber;
              qryTicket.Parameters.ParamByName('ticketFormat').Value := TicketFormat;
              try
                ticketID := 0;
                qryTicket.Open;
                ticketID := qryTicket.FieldByName('ticket_id').AsInteger;
                Memo1.Lines.Add('Pulled back Ticket ID for Ticket Number ' + TicketNumber + ' as ' + IntToStr(ticketID));
              finally
                qryTicket.Close; // pull back ticketID
              end;
              If AddLocateRecord(ticketID) then
              begin
                insTicketVersion.Parameters.ParamByName('TicketID').Value:= ticketID;
                insTicketVersion.ExecSQL;
                LogResult.LogType := ltInfo;
                LogResult.MethodName := 'Added ticket '+TicketNumber+' to Ticket Version';
                WriteLog;
               end;
            end;
          end; // for ii := 1 to HowMany do
        end; // If TryStrToFloat(TicketNumber, val)then
      end; // for I := 1 to fileData.Count-1 do
    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process ticket '+TicketNumber;
        LogResult.ExcepMsg:=e.Message;
        WriteLog;
      end;
    end;
  finally

    StatusBar1.Panels[4].text := TimeToStr(Time);
    StatusBar1.Refresh;
    Memo1.Lines.Add(IntToStr(i) + ' Pura records added');
     ForceDirectories(IncludeTrailingBackSlash(FilePath)+'Proc\');
     savedPath:= IncludeTrailingBackSlash(FilePath)+'Proc\'+csvFileName;
     if MoveFile(PChar(excelPage), PChar(savedPath))
     then
     memo1.Lines.add(excelPage+'  moved to '+savedPath);

    CoUnInitialize;
  end; // try-finally

end;

function TfrmPuraImportMain.AddLocateRecord(ticketID: integer): boolean;
var
  locateID: integer;
begin
  try
    locateID := 0;
    with insLocate.Parameters do
    begin
      ParamByName('ticket_id').Value := ticketID;
    end;
    try
      If insLocate.ExecSQL > 0 then
      begin
        qryLastLocate.Parameters.ParamByName('ticketID').Value := ticketID;
        qryLastLocate.Parameters.ParamByName('ClientID').Value := ClientID;
        qryLastLocate.Open;
        locateID := qryLastLocate.FieldByName('locate_id').AsInteger;

        spAssignLocate.Parameters.ParamByName('@LocateID').Value := locateID;
        spAssignLocate.Parameters.ParamByName('@LocatorID').Value := '7389';
        spAssignLocate.Parameters.ParamByName('@AddedBy').Value := 3512;
        spAssignLocate.Parameters.ParamByName('@WorkloadDate').Value := NOW;
        spAssignLocate.ExecProc;
        result := true;
      end;

    finally
      qryLastLocate.Close;
    end;

  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'AddLocateRecord';
      LogResult.DataStream := insLocate.SQL.text;
      LogResult.ExcepMsg := 'Ticket: ' + IntToStr(ticketID) + ' ' + E.Message;
      Memo1.Lines.Add(LogResult.ExcepMsg);
      WriteLog;
      result := false;
    end;
  end;
end;



function TfrmPuraImportMain.ConnectDB: boolean;
var
  myPath: string;
  connStr, LogConnStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  TicketPURAimporter: TIniFile;

begin
  result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
  try
    TicketPURAimporter := TIniFile.Create(myPath + 'TicketPURAimporter.ini');
    // connStr := BillingIni.ReadString('Database', 'ConnectionString', '');   //local machine

    aServer := TicketPURAimporter.ReadString('Database', 'Server', '');
    aDatabase := TicketPURAimporter.ReadString('Database', 'DB', 'QM');
    ausername := TicketPURAimporter.ReadString('Database', 'UserName', '');
    apassword := TicketPURAimporter.ReadString('Database', 'Password', '');
    logPath :=  TicketPURAimporter.ReadString('LogPaths', 'LogPath', '');

    LogConnStr := connStr;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(LogConnStr);
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
    result := ADOConn.Connected;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Connect DB';
    LogResult.DataStream := connStr;
    WriteLog;

    if result then
    begin
      Memo1.Lines.Add('Connected to database');
      StatusBar1.Panels[0].text := aServer;
    end
    else
      Memo1.Lines.Add('Could not connect to DB');
  finally
    TicketPURAimporter.Free;
  end;
end;

end.
