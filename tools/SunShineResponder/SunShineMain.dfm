object frmSunshineResponder: TfrmSunshineResponder
  Left = 0
  Top = 0
  Caption = 'Sunshine Responders'
  ClientHeight = 403
  ClientWidth = 735
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 735
    Height = 346
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnSendResponse: TButton
    Left = 264
    Top = 352
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 1
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 384
    Width = 735
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'SSDS-UTQ-QM-02-DV'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'get_pending_multi_responses_10'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@ParsedLocatesOnly'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@ResponseDelayMinutes'
        DataType = ftInteger
        Value = Null
      end>
    Left = 120
    Top = 160
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 48
    Top = 224
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        Size = 1
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'respond_to'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to = :respond_to')
    Left = 480
    Top = 192
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 160
    Top = 3
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 295
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 504
  end
  object IdLogEvent1: TIdLogEvent
    Left = 400
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 160
    Top = 232
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json;q=0.9,text/plain;q=0.9,text/html'
    AcceptCharset = 'UTF-8'
    BaseURL = 
      'https://exactix.sunshine811.com/api/external/positiveresponse/Re' +
      'spond{'
    Params = <>
    RaiseExceptionOn500 = False
    SynchronizedEvents = False
    Left = 32
    Top = 8
  end
  object RESTRequest1: TRESTRequest
    AssignedValues = [rvAccept, rvAcceptCharset, rvConnectTimeout, rvReadTimeout]
    Accept = 'application/json;q=0.9,text/plain;q=0.9,text/html'
    AcceptCharset = 'UTF-8'
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        Name = 'body'
        Options = [poDoNotEncode]
        ContentTypeStr = 'application/json'
      end>
    Response = RESTResponse1
    ConnectTimeout = 0
    ReadTimeout = 0
    SynchronizedEvents = False
    Left = 32
    Top = 56
  end
  object RESTResponse1: TRESTResponse
    Left = 32
    Top = 104
  end
  object qryTicketInfo: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select TI.*,'
      
        'L.closed_date as ContactDateTime, '#39'574-850-9132'#39' as LocatorPhone' +
        ', '#39'UtiliQuest Rep'#39' as LocatorName'
      'From ticket_info TI'
      'join locate L on (L.Ticket_id= TI.Ticket_id)'
      'where L.locate_id = :LocateID')
    Left = 472
    Top = 272
  end
end
