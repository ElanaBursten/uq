program TwentyTwoAndMe;
// QM 410 -B.P.  Automates WinScp to check for client updates.
{$APPTYPE CONSOLE}

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  System.SysUtils,  Winapi.Windows, IniFiles, System.Classes, System.IOUtils, GlobalSU;

  type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  type
    TLogResults = record
      LogType: TLogType;
      MethodName: String[40];
      Status: String[90];
      ExcepMsg: String[255];
      DataStream: String[255];
  end;

var
  MyInstanceName: string;
  WinScpPath: string;
  ftpHost: string;
  ActiveDoFile: String;
  LocalPathFTP: String;
  PathForFTP: String;
  PrivateKeyStr: String;
  PrivateKeyDir: String;
  FTPLogpath: String;
  ListItem: Integer;
  KeyList: TStringList;
  LogResult: TLogResults;

procedure ReadINI;
var
  IniFile: TIniFile;
begin
   IniFile := TIniFile.Create(TPath.ChangeExtension(GetModuleName(HInstance), '.ini'));
   ftpHost := IniFile.ReadString('remoteFTP', 'RemoteFtpHost', 'sftp.mcview.com/ -hostkey="ssh-rsa 2048 2EZdNsDac9uazNmBhc1EblcxN0Kpn8OeBNEdHAAw/gk="');
   WinScpPath := IniFile.ReadString('remoteFTP', 'WinScpPath', 'C:\Program Files (x86)\WinSCP\');
   PrivateKeyDir := IniFile.ReadString('remoteFTP', 'PrivateKeyDir', 'C:\Program Files (x86)\UtiliQuest\Q Manager\');
   FtpLogPath := IniFile.ReadString('remoteFTP', 'ftpLogPath', 'C:\QM\Logs');
   PrivateKeyDir := IncludeTrailingBackSlash(PrivateKeyDir);
   {Run parameter is local folder where files are to be downloaded}
   LocalPathFTP := IncludeTrailingBackslash(ParamStr(1));
   PathForFTP := '/';  {Root directory of files on server}
   IniFile.Free;
   ForceDirectories(FtpLogPath);
end;

procedure clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  {If Ini file log path doesn't exist, defaults to C:/QM/Logs. If neither paths exist, skip logging}
  if not DirectoryExists(FTPLogPath) then Exit;
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FTPLogPath) + 'TwentyTwoAndMe' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

function CreateDOfile(PrivKeyFile: String): Boolean;
//builds and saves a script file to process a private key
const
  LINE1 = 'option batch on';
  LINE2 = 'option confirm off';
  LINE3 = 'open ';
  LINE4 = 'synchronize local ';
  LINE5 = 'exit';
  DO_FILE = 'do.txt';
var
  doList: TStringList;
  PrivKey: String;
  HostPrefix: String;
  LocalKeyFolder: String;
begin
  Result := True;
  PrivKey := ChangeFileExt(PrivKeyFile, '');
  LocalKeyFolder := LocalPathFTP + PrivKey;
  try
    {create subdirectories named after private key in local folder}
    ForceDirectories(LocalKeyFolder);
    try
      ActiveDoFile := DO_FILE;
      doList := TStringList.Create;
      HostPrefix := 'sftp://' + PrivKey + '@';
      PrivateKeyStr :=  ' -privatekey=' + '"' + PrivateKeyDir + PrivKey + '.ppk' + '"';
      doList.add(LINE1);
      doList.add(LINE2);
      doList.add(LINE3 + HostPrefix + FTPHost + PrivateKeyStr);
      doList.Add(LINE4 + '"' + LocalKeyFolder + '" ' + '"' +PathForFTP + '"');
      doList.Add(LINE5);
      doList.SaveToFile(IncludeTrailingBackSlash(WinScpPath) + ActiveDoFile, TEncoding.UTF8);
    finally
      DoList.Free;
    end;
  except
    Result := False;
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed';
    LogResult.ExcepMsg := 'Error Creating Do File for ' + 'PrivKey: ' + SysErrorMessage(Error);
    LogResult.MethodName := 'CreateDoFile';
    WriteLog(LogResult);
  end;
end;


function ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
    StartupInfo.dwFlags := CREATE_NO_WINDOW;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
    WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
    repeat
      Sleep(0);
       until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;


Procedure PullFromServer(PrivKeyFile:String);
{synchronizes file transfer from server to local directory for a private key file;
 uses createprocess to run winscp.bat with the script file as parameter}
var
  FileName, Parameters, WorkingFolder: string;
  OK: Boolean;
  Error: integer;
const
  WIN_SCP = 'WinSCP.bat';
begin

  FileName := IncludeTrailingBackslash(WinScpPath) + WIN_SCP;
  Parameters := ActiveDoFile;

  LogResult.LogType := ltInfo;
  LogResult.Status := 'Calling ExecuteProcess for ' + PrivKeyFile + 'download';
  LogResult.MethodName := 'DownloadtoClient';
  WriteLog(LogResult);

  OK := ExecuteProcess(FileName, Parameters, WorkingFolder, true, False, true, Error);

  if OK then
  begin
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Download for' + PrivKeyFile + 'to Client Complete';
    LogResult.DataStream:= 'FileName: '+FileName+ ' Param: '+ Parameters+ ' Folder: '+ WorkingFolder+' Error: '+IntToStr(Error);
    LogResult.MethodName := 'DownloadtoClient';
    WriteLog(LogResult);
  end else
  begin
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed for ' + PrivKeyFile + 'download';
    LogResult.ExcepMsg := 'Error: ' + SysErrorMessage(Error);
    LogResult.MethodName := 'DownloadtoClient';
    WriteLog(LogResult);
  end;
end;

procedure RunPutty(KeyFile: String);
//runs the winscp command to generate .ppk files
var
  OK: Boolean;
  WorkingFolder, Parameters, PuttyCmd: String;
  Error: Integer;
begin
  PuttyCmd := IncludeTrailingBackslash(WinScpPath) + 'WinScp.com';
  Parameters := ' /keygen ' + '"' + PrivateKeyDir + KeyFile + '"' + ' /output=' + '"' + TPath.ChangeExtension(PrivateKeyDir + KeyFile, '.ppk') + '"';
  OK := ExecuteProcess(PuttyCmd, Parameters, WorkingFolder, true, False, true, Error);

  if OK then
  begin
    LogResult.LogType := ltInfo;
    LogResult.Status := 'ExecuteProcess Complete. Convert Key File: ' + KeyFile;
    LogResult.MethodName := 'ConvertPrivateKey';
    WriteLog(LogResult);
  end else
  begin
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed';
    LogResult.ExcepMsg := 'Error Converting Key ' + KeyFile + ': ' + SysErrorMessage(Error);
    LogResult.MethodName := 'ConvertPrivateKey';
    WriteLog(LogResult);
  end;
end;

function GetPrivateKeyFiles(const PrivKeyDir: String; const FileExt: String; const List: TStrings): Boolean;
{iterates through private key folder and assigns private keys to List
uses FileExt parameter for a filter (*.ppk,*.*) }
var
  SRec: TSearchRec;
  Res: Integer;
begin
  if not Assigned(List) then
  begin
    Result := False;
    Exit;
  end;
  Res := FindFirst(PrivKeyDir + FileExt, faAnyfile, SRec);
  if Res = 0 then
  try
    while res = 0 do
    begin
      if (SRec.Attr and faDirectory <> faDirectory) then
        List.Add( SRec.Name );
      Res := FindNext(SRec);
    end;
  finally
    System.SysUtils.FindClose(SRec);
  end;
  Result := (List.Count > 0);
end;

function PopulateKeyList(const List: TStrings): Boolean;
{retrieves key files in private key folder. If key file has no extension or
 is .pem, a .ppk is generated(an existing .ppk will be overwritten)}
var
  I: Integer;
  FileExt: String;
begin
  List.Clear;
  Result := False;
  if GetPrivateKeyFiles(PrivateKeyDir, '*.*', List) then
  begin
    for I := 0 to KeyList.Count-1 do
    begin
      FileExt := TPath.GetExtension(List[I]);
      If (FileExt = '') or (FileExt = '.pem') then
        RunPutty(List[I]);
    end;
    List.Clear;
    if GetPrivateKeyFiles(PrivateKeyDir, '*.ppk', List) then
      Result := True;
  end;
end;

begin
  MyInstanceName := ExtractFileName(GetModuleName(HInstance));
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamCount < 1 then
    begin
      Exit;
    end;
    try
      try
        ReadIni;

        LogResult.LogType := ltInfo;
        LogResult.Status := '--------------------- StartUp Twenty Two And Me ----------------------------------';
        WriteLog(LogResult);

       KeyList := TStringList.Create;
       {retrieves private key files and generates .ppk files if needed}
       If PopulateKeyList(KeyList) then
       begin
         for ListItem := 0 to KeyList.Count-1 do
         begin
           {On failure to create winscp script file, logs error and continues to next key}
           If not CreateDOfile(KeyList[ListItem]) then Continue;
           {Initates file transfer}
           PullFromServer(KeyList[ListItem]);
         end;
       end;
      except
        on E: Exception do
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'TwentyTwoAndMe Console';
          LogResult.ExcepMsg := E.Message;
          WriteLog(LogResult);
          Raise;
        end
      end;
    finally
      KeyList.Free;
    end;
  end;
end.
