unit GlobalSU;

interface
 function IsAppRunning: Boolean;
 function CreateSingleInstance(const InstanceName: string): boolean;
implementation

uses
 Windows, SysUtils, Forms;

function IsAppRunning: Boolean;
var
 rtn : Cardinal;
begin
  result := False;
  CreateMutex(nil, False, PWideChar(ExtractFileName(Application.ExeName)));
  rtn := GetLastError;
  if rtn = ERROR_ALREADY_EXISTS then
   result := True;
end;


function CreateSingleInstance(const InstanceName: string): boolean;
var
  MutexHandle: THandle;
begin

    MutexHandle := CreateMutex(nil, false, PChar(InstanceName));
    // if MutexHandle created check if already exists
    if (MutexHandle <> 0) then
      begin
        if GetLastError = ERROR_ALREADY_EXISTS then
          begin
            Result := false;
            CloseHandle(MutexHandle);
          end
        else Result := true;
      end
    else Result := false;

end;

end.
