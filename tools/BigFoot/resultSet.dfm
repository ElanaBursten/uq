object frmResults: TfrmResults
  Left = 0
  Top = 0
  Caption = 'Here is what you got!!'
  ClientHeight = 499
  ClientWidth = 982
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 15
  object Panel1: TPanel
    Left = 0
    Top = 24
    Width = 982
    Height = 433
    Align = alTop
    TabOrder = 0
    object cxGrid1: TcxGrid
      AlignWithMargins = True
      Left = 11
      Top = 4
      Width = 960
      Height = 408
      Margins.Left = 10
      Margins.Right = 10
      Margins.Bottom = 20
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsResultSet
        DataController.KeyFieldNames = 'locate_id'
        DataController.MasterKeyFieldNames = 'Ticket_id, locate_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            FieldName = 'units_marked'
            DisplayText = 'Units marked'
          end
          item
            Kind = skSum
            FieldName = 'qty_marked'
            DisplayText = 'Qty marked'
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Indicator = True
        Styles.UseOddEvenStyles = bTrue
        object cxGrid1DBTableView1ticket_id: TcxGridDBColumn
          DataBinding.FieldName = 'ticket_id'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1locate_id: TcxGridDBColumn
          DataBinding.FieldName = 'locate_id'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1client_code: TcxGridDBColumn
          DataBinding.FieldName = 'client_code'
          DataBinding.IsNullValueType = True
          Width = 86
        end
        object cxGrid1DBTableView1work_date: TcxGridDBColumn
          DataBinding.FieldName = 'work_date'
          DataBinding.IsNullValueType = True
          Width = 79
        end
        object cxGrid1DBTableView1emp_id: TcxGridDBColumn
          DataBinding.FieldName = 'emp_id'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1short_name: TcxGridDBColumn
          DataBinding.FieldName = 'short_name'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1units_marked: TcxGridDBColumn
          DataBinding.FieldName = 'units_marked'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1qty_marked: TcxGridDBColumn
          DataBinding.FieldName = 'qty_marked'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1HoursStatus: TcxGridDBColumn
          DataBinding.FieldName = 'HoursStatus'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1LocStatus: TcxGridDBColumn
          DataBinding.FieldName = 'LocStatus'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1unit_conversion_id: TcxGridDBColumn
          DataBinding.FieldName = 'unit_conversion_id'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1first_unit_factor: TcxGridDBColumn
          DataBinding.FieldName = 'first_unit_factor'
          DataBinding.IsNullValueType = True
        end
        object cxGrid1DBTableView1rest_units_factor: TcxGridDBColumn
          DataBinding.FieldName = 'rest_units_factor'
          DataBinding.IsNullValueType = True
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 982
    Height = 24
    Align = alTop
    TabOrder = 1
  end
  object Panel3: TPanel
    Left = 0
    Top = 457
    Width = 982
    Height = 42
    Align = alClient
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 882
      Top = 6
      Width = 75
      Height = 25
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 0
    end
  end
  object qryResultSet: TADOQuery
    Connection = frmBigFoot.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'LocateHoursId'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select L.[ticket_id], LH.[locate_id], LH.[emp_id], E.[short_name' +
        '], LH.[units_marked], L.[qty_marked], LH.[status] as HoursStatus' +
        ', L.[status] as LocStatus,'
      
        'LH.[unit_conversion_id], BUC.first_unit_factor,BUC.rest_units_fa' +
        'ctor,'
      'L.[client_code], LH.[work_date]'
      'from locate_hours LH'
      'join locate L on (LH.[locate_id]=L.[locate_id])'
      'join client C on (L.[client_code] =C.oc_code)'
      
        'left outer join [dbo].[billing_unit_conversion] BUC on(BUC.[unit' +
        '_conversion_id]=C.[unit_conversion_id])'
      'left join employee E on (E.emp_id = LH.[emp_id])'
      'where LH.locate_hours_id > :LocateHoursId'
      'order by  L.[ticket_id], LH.[locate_id] desc')
    Left = 48
    Top = 376
  end
  object dsResultSet: TDataSource
    DataSet = qryResultSet
    Left = 136
    Top = 376
  end
  object dxSkinController1: TdxSkinController
    Left = 480
    Top = 264
  end
end
