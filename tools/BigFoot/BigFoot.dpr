program BigFoot;
// qm-999

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  Vcl.Forms,
  SysUtils,
  uBigFoot in 'uBigFoot.pas' {frmBigFoot},
  GlobalSU in 'C:\common\GlobalSU.pas',
  xqLengthConverter in 'C:\common\xqLengthConverter.pas',
  resultSet in 'resultSet.pas' {frmResults};

//{$R *.res}

var
  MyInstanceName: string;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmBigFoot, frmBigFoot);
  Application.Run;
  end
  else
    Application.Terminate;

end.
