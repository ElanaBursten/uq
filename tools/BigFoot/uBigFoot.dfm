object frmBigFoot: TfrmBigFoot
  Left = 0
  Top = 0
  Caption = 'Big Foot '
  ClientHeight = 456
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  TextHeight = 15
  object Splitter1: TSplitter
    Left = 0
    Top = 366
    Width = 694
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 369
    Width = 694
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 694
    Height = 41
    Align = alTop
    TabOrder = 0
  end
  object pnlMid: TPanel
    Left = 0
    Top = 41
    Width = 694
    Height = 325
    Align = alTop
    TabOrder = 1
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 692
      Height = 323
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
      StyleName = 'Windows'
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 372
    Width = 694
    Height = 69
    Align = alTop
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 48
      Height = 15
      Caption = 'Database'
    end
    object Label3: TLabel
      Left = 224
      Top = 48
      Width = 29
      Height = 15
      Caption = 'Good'
    end
    object Label4: TLabel
      Left = 280
      Top = 48
      Width = 31
      Height = 15
      Caption = 'Failed'
    end
    object Label1: TLabel
      Left = 520
      Top = 48
      Width = 38
      Height = 15
      Caption = 'Version'
    end
    object Label5: TLabel
      Left = 160
      Top = 48
      Width = 26
      Height = 15
      Caption = 'Total'
    end
    object Label6: TLabel
      Left = 328
      Top = 48
      Width = 24
      Height = 15
      Caption = 'Start'
    end
    object Label7: TLabel
      Left = 432
      Top = 48
      Width = 24
      Height = 15
      Caption = 'Stop'
    end
    object btnProcess: TButton
      Left = 328
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Process'
      TabOrder = 0
      OnClick = btnProcessClick
    end
    object BitBtn1: TBitBtn
      Left = 608
      Top = 6
      Width = 75
      Height = 25
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object btnResultSet: TButton
      Left = 504
      Top = 6
      Width = 75
      Height = 25
      Caption = 'View Results'
      Enabled = False
      TabOrder = 2
      OnClick = btnResultSetClick
    end
    object dxRatingControl1: TdxRatingControl
      Left = 1
      Top = 6
      Properties.Glyph.SourceDPI = 96
      Properties.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
        61796572312220786D6C6E733D22687474703A2F2F7777772E77332E6F72672F
        323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F7777
        772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D22
        307078222076696577426F783D2230203020333220333222207374796C653D22
        656E61626C652D6261636B67726F756E643A6E6577203020302033322033323B
        2220786D6C3A73706163653D227072657365727665223E262331333B26233130
        3B20203C7374796C6520747970653D22746578742F6373732220786D6C3A7370
        6163653D227072657365727665223E2E426C61636B262331333B262331303B20
        2020207B262331333B262331303B20202020202066696C6C3A23373237323732
        3B262331333B262331303B202020202020666F6E742D66616D696C793A266170
        6F733B64782D666F6E742D69636F6E732661706F733B3B262331333B26233130
        3B202020202020666F6E742D73697A653A333270783B262331333B262331303B
        202020207D262331333B262331303B20203C2F7374796C653E0D0A3C74657874
        20783D22302220793D2233322220636C6173733D22426C61636B223EEEB5943C
        2F746578743E0D0A3C2F7376673E0D0A}
      Properties.HoverGlyph.SourceDPI = 96
      Properties.HoverGlyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
        61796572312220786D6C6E733D22687474703A2F2F7777772E77332E6F72672F
        323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F7777
        772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D22
        307078222076696577426F783D2230203020333220333222207374796C653D22
        656E61626C652D6261636B67726F756E643A6E6577203020302033322033323B
        2220786D6C3A73706163653D227072657365727665223E262331333B26233130
        3B20203C7374796C6520747970653D22746578742F6373732220786D6C3A7370
        6163653D227072657365727665223E2E426C61636B262331333B262331303B20
        2020207B262331333B262331303B20202020202066696C6C3A23373237323732
        3B262331333B262331303B202020202020666F6E742D66616D696C793A266170
        6F733B64782D666F6E742D69636F6E732661706F733B3B262331333B26233130
        3B202020202020666F6E742D73697A653A333270783B262331333B262331303B
        202020207D262331333B262331303B20203C2F7374796C653E0D0A3C74657874
        20783D22302220793D2233322220636C6173733D22426C61636B223EEEB5943C
        2F746578743E0D0A3C2F7376673E0D0A}
      TabOrder = 3
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 441
    Width = 694
    Height = 15
    Align = alClient
    Panels = <
      item
        Width = 160
      end
      item
        Width = 60
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=sa;Init' +
      'ial Catalog="";Data Source=GREATWHITE;Initial File Name="";Serve' +
      'r SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    OnRollbackTransComplete = ADOConnRollbackTransComplete
    Left = 32
    Top = 65
  end
  object qryLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ClientCode'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'TicketID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select L.locate_ID, L.Status, L.assigned_to_id, L.entry_date'
      'From Locate L'
      'join Client C on (C.oc_code = L.client_code)'
      'Where C.oc_code = :ClientCode'
      'and L.Ticket_id = :TicketID'
      'and C.BigFoot='#39'True'#39)
    Left = 120
    Top = 81
  end
  object delLocateFootage: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Value = Null
      end>
    SQL.Strings = (
      'update locate_hours'
      'set units_marked = 0'
      '  where Locate_id = :LocateID'
      '  and BigFoot =0')
    Left = 120
    Top = 281
  end
  object spAddLocateFootage: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'AddLocateHoursAndUpdateLocate'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ticket_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Work_Date'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Entry_Date'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Client_Code'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Units_marked'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 120
    Top = 145
  end
  object qryTopLocateHours: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'select MAX([locate_hours_id]) LocateHoursID'
      'from [dbo].[locate_hours]')
    Left = 224
    Top = 81
  end
end
