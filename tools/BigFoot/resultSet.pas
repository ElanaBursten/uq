unit resultSet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinBasic, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Data.Win.ADODB, Vcl.ExtCtrls, dxCore, dxSkinsForm, Vcl.StdCtrls, Vcl.Buttons;

type
  TfrmResults = class(TForm)
    Panel1: TPanel;
    qryResultSet: TADOQuery;
    dsResultSet: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxSkinController1: TdxSkinController;
    cxGrid1DBTableView1ticket_id: TcxGridDBColumn;
    cxGrid1DBTableView1locate_id: TcxGridDBColumn;
    cxGrid1DBTableView1emp_id: TcxGridDBColumn;
    cxGrid1DBTableView1units_marked: TcxGridDBColumn;
    cxGrid1DBTableView1qty_marked: TcxGridDBColumn;
    cxGrid1DBTableView1HoursStatus: TcxGridDBColumn;
    cxGrid1DBTableView1LocStatus: TcxGridDBColumn;
    cxGrid1DBTableView1unit_conversion_id: TcxGridDBColumn;
    cxGrid1DBTableView1first_unit_factor: TcxGridDBColumn;
    cxGrid1DBTableView1rest_units_factor: TcxGridDBColumn;
    cxGrid1DBTableView1client_code: TcxGridDBColumn;
    cxGrid1DBTableView1work_date: TcxGridDBColumn;
    Panel2: TPanel;
    Panel3: TPanel;
    cxGrid1DBTableView1short_name: TcxGridDBColumn;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResults: TfrmResults;

implementation

{$R *.dfm}
uses uBigFoot;

procedure TfrmResults.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryResultSet.close;
  Action := caFree;
end;

procedure TfrmResults.FormCreate(Sender: TObject);
begin
  qryResultSet.Parameters.ParamByName('LocateHoursId').Value :=frmBigFoot.LocateHoursID;
  qryResultSet.open;
end;

end.
