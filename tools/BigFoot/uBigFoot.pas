unit uBigFoot;
//qm-999
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, System.json, System.IniFiles,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Vcl.Buttons, resultSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxRatingControl;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;  //TLogResults

TLocateFootage = Record
  TicketID: integer;
  postDate: TDateTime;
  LocateID: integer;
  ClientName: string;
  ClientFeet: integer;
end;


type
  TfrmBigFoot = class(TForm)
    pnlTop: TPanel;
    pnlMid: TPanel;
    pnlBottom: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    StatusBar1: TStatusBar;
    Memo1: TMemo;
    ADOConn: TADOConnection;
    btnProcess: TButton;
    qryLocate: TADOQuery;
    BitBtn1: TBitBtn;
    delLocateFootage: TADOQuery;
    spAddLocateFootage: TADOStoredProc;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btnResultSet: TButton;
    dxRatingControl1: TdxRatingControl;
    qryTopLocateHours: TADOQuery;
    procedure btnProcessClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnResultSetClick(Sender: TObject);
    procedure ADOConnRollbackTransComplete(Connection: TADOConnection; const Error: Error; var EventStatus: TEventStatus);
  private
    { Private declarations }

    EncodedDateTime: TDateTime;
    LocateFootageArray: array of TLocateFootage; // Dynamic array of TLocateFootage
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;

    flogDir: wideString;
    pathFileName:widestring;

    ActiveJSON:String;

    fOutPath: string;
    fInPath: string;
    fErrorPath: string;

    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;



    function GetAppVersionStr: string;
    procedure ClearFootageArray;
    procedure AddLocateFootage(ATicketID: integer; APostDate: TDateTime;
                                       ALocateID: integer;
                                       AClientName: string; AClientFeet: integer);

    procedure PostLocateFootage;
    function EnDeCrypt(const Value: String): String;

    function DateToISO8601_XQ(const ADate: TDateTime; LocalOffset:integer; AInputIsUTC: Boolean = true): string;
  public
    { Public declarations }
    TotalCount,  cntGood, cntBad:integer;
    LocateHoursID:integer;
    procedure WriteLog(LogResult: TLogResults);
    property InPath: string read fInPath write fInPath;
    property OutPath: string read fOutPath write fOutPath;
    property ErrorPath: string read fErrorPath write fErrorPath;
    property logDir: wideString read flogDir;
  end;

var
  frmBigFoot: TfrmBigFoot;

implementation
uses
  System.IOUtils,System.DateUtils,StrUtils,System.Types;
{$R *.dfm}

{ TForm2 }

procedure TfrmBigFoot.ADOConnRollbackTransComplete(Connection: TADOConnection; const Error: Error; var EventStatus: TEventStatus);
begin
  Memo1.Lines.Add('Roll back of last entry due to :'+Error.Description)
end;

procedure TfrmBigFoot.BitBtn1Click(Sender: TObject);
begin
  ADOConn.Connected := false;
end;

procedure TfrmBigFoot.btnProcessClick(Sender: TObject);
var
  JSONfileName:string;
  TicketID:integer;
  JSONFileDate:TDateTime;

    function LoadJSONFromFile(const FileName: string): string;
    begin
      try
        Result := TFile.ReadAllText(FileName, TEncoding.UTF8);
      except
        on E: Exception do
          Writeln('Error reading file: ' + E.Message);
      end;
    end;

    procedure ParseFileName(InputStr: string);
    var
      StrList: TStringList;
      TicketNo:string;
      Year, Month, Day, Hour, Minute, Second :word;
    begin
      StrList := TStringList.Create;
      try
        StrList.Delimiter := '-';
        StrList.DelimitedText := InputStr;

        if StrList.Count = 10 then
        begin
          TicketID  := StrToInt(StrList[0]);
          TicketNo  := StrList[1];
          Year      := StrToInt(StrList[2]);
          Month     := StrToInt(StrList[3]);
          Day       := StrToInt(StrList[4]);
          Hour      := StrToInt(StrList[5]);
          Minute    := StrToInt(StrList[6]);
          Second    := StrToInt(StrList[7]);

          EncodedDateTime := EncodeDate(Year, Month, Day) +
                               EncodeTime(Hour, Minute, Second, 0);
          Memo1.lines.add('TicketID: ' + IntToStr(TicketID));
          Memo1.lines.add('Encoded TDateTime: '+ DateTimeToStr(EncodedDateTime));

        end
        else
        begin
          Writeln('Error: Filename format not as expected.');
        end;
      finally
        StrList.Free;
      end;
    end;

    procedure MoveFileToDirectory(const SourceFile, DestinationDir: string);
    var
      DestinationFile: string;
    begin
      try
        DestinationFile := TPath.Combine(DestinationDir, TPath.GetFileName(SourceFile));

        TFile.Move(SourceFile, DestinationFile);

        Memo1.Lines.add('File moved successfully to: ' + DestinationFile);
      except
        on E: Exception do
          Memo1.Lines.add('Error moving file: ' + E.Message);
      end;
    end;

    procedure ParseJSON(const JSONString: string);
    var
      JSONObject: TJSONObject;
      UtilityLine, ClientCodeMetrics: TJSONArray;
      ClientCodeItem: TJSONValue;
      ClientName: string;
      ClientFeet:integer;
      LocateID:integer;
      I: Integer;

    begin

      JSONObject := TJSONObject.ParseJSONValue(JSONString) as TJSONObject;
      try
        UtilityLine := JSONObject.GetValue<TJSONObject>('utilityLine')
          .GetValue<TJSONArray>('utilityLineMetricsByClientCode');

        for I := 0 to UtilityLine.Count - 1 do
        begin
          ClientCodeItem := UtilityLine.Items[I];
          ClientName := ClientCodeItem.GetValue<TJSONObject>.GetValue<string>('name');
          ClientFeet := trunc(ClientCodeItem.GetValue<TJSONObject>.GetValue<double>('value')); //qm-1041

          qryLocate.close;
          qryLocate.Parameters.ParamByName('ClientCode').Value  := ClientName;
          qryLocate.Parameters.ParamByName('TicketID').Value    := TicketID;
          qryLocate.Open;
          LocateID:=-1;
          LocateID:=qryLocate.FieldByName('locate_ID').AsInteger;
//          delLocateFootage.Parameters.ParamByName('LocateID').Value:=LocateID;
//          delLocateFootage.ExecSQL;
          try
             if LocateID>0 then
             AddLocateFootage(TicketID, EncodedDateTime, LocateID,  ClientName, ClientFeet)
             else
             begin
               inc(cntBad);
               StatusBar1.Panels[3].Text:= IntToStr(cntBad);
               StatusBar1.Refresh;

               LogResult.LogType := ltError;
               LogResult.MethodName:='AddLocateFootage';
               LogResult.Status:='Invalid JSON Entry';
               LogResult.DataStream:='TicketID: '+IntToStr(TicketID)+' LocateID: '+IntToStr(LocateID) +' ClientName: '+ ClientName+' ClientFeet: '+IntToStr(ClientFeet);
               WriteLog(LogResult);

             end;
            except on E: Exception do
            begin
              LogResult.LogType := ltError;
              LogResult.MethodName:='AddLocateFootage';
              LogResult.ExcepMsg:=E.Message;
              LogResult.DataStream:='TicketID: '+IntToStr(TicketID) +' LocateID: '+IntToStr(LocateID)+' ClientName: '+ ClientName+' ClientFeet: '+IntToStr(ClientFeet);
              WriteLog(LogResult);
              MoveFileToDirectory(pathFileName,ErrorPath);
//              exit;
            end;
          end;
          Memo1.Lines.add('Client Name: ' + ClientName + ', ClientFeet: ' + IntToStr(ClientFeet));
        end;

      finally
        qryLocate.Close;
        JSONObject.Free;
      end;
    end;

    function CountFilesInDirectory(const Directory: string): integer;
    var
      SearchRec: TSearchRec;
      FileCount: integer;
    begin
      FileCount := 0;
      if FindFirst(Directory + '\*.*', faAnyFile, SearchRec) = 0 then
      begin
        repeat
          if (SearchRec.Attr and faDirectory) = 0 then
            Inc(FileCount);
        until FindNext(SearchRec) <> 0;
        FindClose(SearchRec);
      end;
      Result := FileCount;
    end;

    procedure CopyFilesWithProcessing(const SourceDir, TargetDir: string);
    var
      FilesArray: TStringDynArray;
      FileName: string;
      SourceFile, DestFile: string;
    begin
      if not TDirectory.Exists(SourceDir) then
        raise Exception.Create('Source directory does not exist');


      if not TDirectory.Exists(TargetDir) then
        TDirectory.CreateDirectory(TargetDir);


      FilesArray := TDirectory.GetFiles(SourceDir);
      TotalCount := Length(FilesArray);
      Memo1.Lines.Add(IntToStr(TotalCount)+' Total Elements in array');
      StatusBar1.Panels[1].Text:= IntToStr(CountFilesInDirectory(SourceDir));
      StatusBar1.Refresh;
      for FileName in FilesArray do
      begin
        SourceFile := TPath.Combine(SourceDir, TPath.GetFileName(FileName));
        DestFile := TPath.Combine(TargetDir, TPath.GetFileName(FileName));
        JSONfileName:=TPath.GetFileNameWithoutExtension(SourceFile);
        ActiveJSON:= LoadJSONFromFile(SourceFile);

        ParseFileName(JSONfileName);
        ParseJSON(ActiveJSON);
        PostLocateFootage;

        // After processing, copy the file to the target directory
        TFile.Copy(SourceFile, DestFile, True);  // The "True" flag overwrites existing files
        TFile.Delete(SourceFile);  // incase already there

        Memo1.lines.add('Copied file: '+ SourceFile+ ' to '+ DestFile);
      end;
      SetLength(FilesArray, 0); //frees array memory
    end;

  begin
    cntGood := 0;
    cntBad := 0;
    StatusBar1.Panels[4].Text := TimeToStr(TimeOf(Now));
    CopyFilesWithProcessing(InPath, OutPath);
    StatusBar1.Panels[5].Text := TimeToStr(TimeOf(Now));
    Memo1.Lines.Add('All Processed.');
    btnResultSet.Enabled := true;
  end;

  procedure TfrmBigFoot.btnResultSetClick(Sender: TObject);
  begin
    frmResults := TfrmResults.Create(Self);
    frmResults.Show;
  end;

procedure TfrmBigFoot.PostLocateFootage;
var
  i:integer;
begin

  try
     for i := 0 to High(LocateFootageArray) do
      begin
        with spAddLocateFootage, spAddLocateFootage.Parameters do
        begin
          ParamByName('@Ticket_ID').Value:= LocateFootageArray[i].TicketID;
          ParamByName('@Work_Date').Value:= DateOf(LocateFootageArray[i].postDate);
          ParamByName('@Entry_date').Value:= LocateFootageArray[i].postDate;
          ParamByName('@Units_marked').Value:= LocateFootageArray[i].ClientFeet;
          ParamByName('@Client_Code').Value:= LocateFootageArray[i].ClientName;
          ExecProc;
          Inc(cntGood);
          Statusbar1.Panels[2].Text:= intToStr(cntGood);
          Statusbar1.Refresh;
        end;
      end;
  except on E: Exception do
  begin
//    TFile.Move(SourceFile,ErrorPath);

    inc(cntBad);
    Statusbar1.Panels[3].Text:= intToStr(cntBad);
    Statusbar1.Refresh;
    raise;
  end;
  end;
  ClearFootageArray;
end;

procedure TfrmBigFoot.AddLocateFootage(ATicketID: integer; APostDate: TDateTime;
                                       ALocateID: integer;
                                       AClientName: string; AClientFeet: integer);
var
  NewIndex: integer;
begin
  NewIndex := Length(LocateFootageArray);
  SetLength(LocateFootageArray, NewIndex + 1);
  with LocateFootageArray[NewIndex] do
  begin
    TicketID   := ATicketID;
    postDate   := APostDate;
    LocateID   := ALocateID;
    ClientName := AClientName;
    ClientFeet := AClientFeet;
  end;
end;

procedure TfrmBigFoot.ClearFootageArray;
begin
  SetLength(LocateFootageArray, 0);
end;


function TfrmBigFoot.EnDeCrypt(const Value: String): String;
var
  CharIndex: Integer;
begin
  result := Value;
  for CharIndex := 1 to Length(Value) do
    result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function TfrmBigFoot.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  Result := True;
  ADOConn.close;

  if LeftStr(apassword, 4) = DECRYPT then
  begin
    apassword := Copy(apassword, 5, maxint);
    apassword := EnDeCrypt(apassword);
  end
  else
  begin
    LogResult.LogType := ltWarning;
    LogResult.Status:='Connect to DB';
    LogResult.DataStream:= 'You are using a Clear Text Password.  Please contact IT for the correct Password';
    WriteLog(LogResult);
  end;

  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  LogConnStr := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;

      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := LogConnStr;
      WriteLog(LogResult);
      StatusBar1.Panels[0].Text := aServer;
  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := LogConnStr;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[0].Text := 'Failed Connection';
    end;
  end;
end;

procedure TfrmBigFoot.FormCreate(Sender: TObject);
begin
  StatusBar1.Panels[6].Text := GetAppVersionStr;
  ProcessINI;
  connectDB;
  If Paramstr(1) <> 'GUI' then
    btnProcessClick(Sender)
  else
  begin
    try
      qryTopLocateHours.Open;
      LocateHoursID := qryTopLocateHours.FieldByName('LocateHoursID')
        .AsInteger;
    finally
      qryTopLocateHours.close;
    end;
  end;
end;

function TfrmBigFoot.DateToISO8601_XQ(const ADate: TDateTime; LocalOffset:integer; AInputIsUTC: Boolean = true): string;
const   //qm-999
  SDateFormat: string = '%.4d-%.2d-%.2dT%.2d:%.2d:%.2d.%.3dZ'; { Do not localize }
  SOffsetFormat: string = '%s%s%.02d:%.02d'; { Do not localize }
  Neg: array[Boolean] of string = ('+', ''); { Do not localize }
var
  y, mo, d, h, mi, se, ms: Word;
  Bias: Integer;
  TimeZone: TTimeZone;
begin
  DecodeDate(ADate, y, mo, d);
  DecodeTime(ADate, h, mi, se, ms);
  Result := Format(SDateFormat, [y, mo, d, h, mi, se, ms]);
  if not AInputIsUTC then
  begin
    TimeZone := TTimeZone.Local;
    Bias := LocalOffset*60;
    if Bias <> 0 then
    begin
      // Remove the Z, in order to add the UTC_Offset to the string.
      SetLength(Result, Result.Length - 1);
      Result := Format(SOffsetFormat, [Result, Neg[Bias < 0], Bias div MinsPerHour,
        Bias mod MinsPerHour]);
    end
  end;
end;

function TfrmBigFoot.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build

end;

function TfrmBigFoot.ProcessINI: boolean;
var
  BigFootINI: TIniFile;
begin
  try
    BigFootINI := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := BigFootINI.ReadString('Database', 'Server', '');
    aDatabase := BigFootINI.ReadString('Database', 'DB', 'QM');
    ausername := BigFootINI.ReadString('Database', 'UserName', '');
    apassword := BigFootINI.ReadString('Database', 'Password', '');

    flogDir    := BigFootINI.ReadString('LogPaths', 'LogPath', '');

    InPath:= BigFootINI.ReadString('JSONFILES', 'IN_PATH', '');
    OutPath:= BigFootINI.ReadString('JSONFILES', 'OUT_PATH', '');
    ErrorPath:= BigFootINI.ReadString('JSONFILES', 'ERROR_PATH', '');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    BigFootINI.Free;
  end;

end;

procedure TfrmBigFoot.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + 'BigFoot-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

procedure TfrmBigFoot.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

end.
