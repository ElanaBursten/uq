unit Boss811Main;
//QM-503  611
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, IdMessage, IdIntercept, IdLogBase, IdLogEvent, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;  //TLogResults


type
  TfrmBoss811Responder = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    RESTResponse1: TRESTResponse;
    RESTClient1: TRESTClient;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    RESTRequest1: TRESTRequest;
    delResponse: TADOQuery;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    qryEMailRecipients: TADOQuery;

    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;

    FRespondto: string;
    fCallCenter: string;
    FPosturl: string;
    fOC_Code: string;
    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    procedure SetUpRecipients;
    { Private declarations }
  public
    { Public declarations }
    property OC_Code:string read fOC_Code write fOC_Code;
    property CallCenter: string read fCallCenter write fCallCenter;
    property Respondto: string read FRespondto write FRespondto;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
    property Posturl: string read FPosturl write FPosturl;
  end;

var
  frmBoss811Responder: TfrmBoss811Responder;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmBoss811Responder.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;


procedure TfrmBoss811Responder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmBoss811Responder.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  Respondto := ParamStr(1);
  CallCenter:= ParamStr(2);
  AppVer:='';
  ProcessINI;
  AppVer  := GetAppVersionStr;
  if (Respondto='') or (CallCenter='')  then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'processParams';
    LogResult.Status := 'MISSING Params';
    LogResult.DataStream:='RespondTo: '+ ParamStr(1) + '  CallCenter: '+ParamStr(2);
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'processParams';
    LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
    WriteLog(LogResult);
  end;
  connectDB;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  SetUpRecipients;

  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

function TfrmBoss811Responder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmBoss811Responder.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmBoss811Responder.ProcessINI: boolean;
var
  Boss811Uni: TIniFile;
begin
  try
    Boss811Uni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := Boss811Uni.ReadString('Database', 'Server', '');
    aDatabase := Boss811Uni.ReadString('Database', 'DB', 'QM');
    ausername := Boss811Uni.ReadString('Database', 'UserName', '');
    apassword := Boss811Uni.ReadString('Database', 'Password', '');

    flogDir    := Boss811Uni.ReadString('LogPaths', 'LogPath', '');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    Boss811Uni.Free;
  end;
end;

procedure TfrmBoss811Responder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + RespondTo +'_Boss811Uni-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;
procedure TfrmBoss811Responder.btnSendResponseClick(Sender: TObject);
//https://nisource.boss811.com/api/v2/tickets/by_center/{one_call_center}/by_number/{insert_ticket_number}/ticket_responses
//https://nisource.boss811.com/api/v2/tickets/by_center/{one_call_center}/by_number/{insert_ticket_number}/close
const
  DBL_QUOTES = '"';

BOSS811_TICKET_STATUS =
'{ '+
'  "ticket_responses": [    '+
'    {                         '+
'      "service_area": "%S",  '+   //client code
'      "code": "%S"  '+ //   out going status
'    } '+
'  ] '+
'}                        ';
  ONE_CALL_PLACEHOLDER= '{one_call_center}';
  TICKET_NUMBER_PLACEHOLDER= '{insert_ticket_number}';
var
  TicketNo:string;
  TicketStatus:string;
  OutStatus:string;
  LocateID:integer;
  formattedURL, URL:string;
  OneCallCenter : string;
  CustBody:string;
  ClientCode:string;
  ResponseBody:string;
  cntGood, cntBad :integer;
  requestID:string;
begin
  OutStatus:='';
  TicketNo:='';
  LocateID:=0;
  CustBody:='';
  formattedURL:='';
  ResponseBody:='';
  URL :='';
  OneCallCenter := '';
  ClientCode:='';
  requestID:='';
  with spGetPendingResponses do
  begin
    try
      Parameters.ParamByName('@RespondTo').Value := Respondto;
      Parameters.ParamByName('@CallCenter').Value := CallCenter;
      open;
    except    //QM- 611  stops the hang if no connection
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'spGetPendingResponses';
        LogResult.Status := 'Existing since no connection';
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        exit;    //if i can connect, get out of here
      end;
    end;
    while not eof do
    begin
      LocateID      := FieldByName('locate_id').AsInteger;
      TicketNo      := Trim(FieldByName('ticket_number').AsString);
      ClientCode    := Trim(FieldByName('Client_Code').AsString);
      URL           := Trim(FieldByName('URL').AsString);
      OneCallCenter := Trim(FieldByName('one_call_center').AsString);
      TicketStatus  := Trim(FieldByName('status').AsString);
      OutStatus     := Trim(FieldByName('outgoing_status').AsString);

      if OutStatus = 'SKIP' then
      begin
        delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
        delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

        memo1.Lines.Add(delResponse.SQL.Text);

        try
         delResponse.ExecSQL;   //commented out for testing
         begin
            LogResult.LogType := ltInfo;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.status:='Skip UQ status '+TicketStatus+' TktNbr: '+TicketNo+ ' Locate: '+IntToStr(LocateID);
            WriteLog(LogResult);
         end;
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
        next;
        continue;
      end;


      URL  := StringReplace(url, TICKET_NUMBER_PLACEHOLDER, TicketNo,[rfReplaceAll, rfIgnoreCase]);
      formattedURL  := StringReplace(url, ONE_CALL_PLACEHOLDER, OneCallCenter,[rfReplaceAll, rfIgnoreCase]);

      if Respondto = 'IN_NIP_BOSS_S' then
      ResponseBody  :=Trim(Format(BOSS811_TICKET_STATUS,[ClientCode, OutStatus]))
      else
      ResponseBody:='';

      if Respondto = 'IN_NIP_BOSS_C' then
      OutStatus  := 'CLOSE';

      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';
      RESTClient1.BaseURL :=  formattedURL;
      RESTRequest1.AddBody(ResponseBody, ctAPPLICATION_JSON);
      if Respondto = 'IN_NIP_BOSS_S' then
      RESTRequest1.Method := TRESTRequestMethod.rmPut //status change
      else
      RESTRequest1.Method := TRESTRequestMethod.rmPATCH;  //ticket close

      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseBody;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin
        ResponseBody:= RESTResponse1.Content;
        memo1.Lines.Add('ResponseBody: '+ResponseBody);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket';
        LogResult.DataStream := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
        delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

        memo1.Lines.Add(delResponse.SQL.Text);

        try
         delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
      end  //StatusCode > 199 <300   good
      else if RESTResponse1.StatusCode = 403 then
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Deleting Status 403';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        AddResponseLogEntry(insertResponseLog);
        try
         delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
      end //StatusCode = 403    bad and delete
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;

        WriteLog(LogResult);
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF

  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

  if cntBad>0 then
  begin
    idSMTP1.Connect;
    idSMTP1.Send(IdMessage1);
    idSMTP1.Disconnect();

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Sending email';
    LogResult.Status := 'Some responses failed';
    LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
    WriteLog(LogResult);
  end;


  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmBoss811Responder.SetUpRecipients;
begin
  try
  idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
  qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
  qryEMailRecipients.Open;
  if not qryEMailRecipients.eof then
  idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
  else
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'SetUpRecipients';
    LogResult.Status:= 'No recipients to send email to';
    WriteLog(LogResult);
   end;
  finally
  qryEMailRecipients.close;
  end;
end;

procedure TfrmBoss811Responder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmBoss811Responder.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog  := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
