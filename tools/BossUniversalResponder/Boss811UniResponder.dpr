program Boss811UniResponder;
//QM-503 Boss811 Universal Responder   SR
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  Boss811Main in 'Boss811Main.pas' {frmBoss811Responder},
  GlobalSU in 'GlobalSU.pas';

var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName)+'_'+ParamStr(2);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.Title := '';
  Application.CreateForm(TfrmBoss811Responder, frmBoss811Responder);
  Application.Run;
  end
  else
    Application.Terminate;
end.
