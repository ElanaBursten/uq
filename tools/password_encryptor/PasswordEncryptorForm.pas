unit PasswordEncryptorForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, ExtCtrls;

type
  TPasswordEncryptor_Form = class(TForm)
    ADO: TADOConnection;
    BottomPanel: TPanel;
    TopPanel: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Proceed: TButton;
    LogPanel: TPanel;
    LogMemo: TMemo;
    procedure ProceedClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure Log(const Txt: string);
  end;

var
  PasswordEncryptor_Form: TPasswordEncryptor_Form;

implementation

{$R *.dfm}

uses
  OdAdoUtils, PasswordRules;

procedure TPasswordEncryptor_Form.ProceedClick(Sender: TObject);
var
  SQL: string;
  Hash: string;
  Query: TADOQuery;
  Count: Integer;
begin
  Proceed.Enabled := False;
  try
    if (not LogPanel.Visible) then begin
      ClientHeight := ClientHeight + LogPanel.Height;
      LogPanel.Visible := True;
    end;

    ADO.Connected := False;
    ADO.ConnectionString := MakeConnectionStringFromIni(ExtractFilePath(ParamStr(0)) + 'QMServer.ini');
    try
      try
        ADO.Open;
      except
        on E: Exception do
          begin
            Log('You must have a valid QMServer.ini file in the same directory as the QMPasswordEncryptor');
            Log(' - ' + E.ClassName + ' - ' + E.Message);
            MessageDlg('You must have a valid QMServer.ini file in the same directory as the QMPasswordEncryptor' + #13 + E.ClassName + ' - ' + E.Message,mtError,[mbOk],0);
            raise EAbort.Create('');
          end;
      end;

      Query := TADOQuery.Create(nil);
      try
        Query.Connection := ADO;
        Query.SQL.Text := 'select uid, login_id, password from users';
        try
          Query.Open;
        except
          on E: Exception do
          begin
            Log('Unable to access the users table');
            Log(' - ' + E.ClassName + ' - ' + E.Message);
            MessageDlg('Unable to access the users table' + #13 + E.ClassName + ' - ' + E.Message,mtError,[mbOk],0);
            raise EAbort.Create('');
          end;
        end;

        while not (Query.EOF) do begin
          if (Query.FieldByName('password').AsString <> '') and (Length(Query.FieldByName('password').AsString) <> 40) then begin
            Hash := GeneratePasswordHash(Query.FieldByName('login_id').AsString,Query.FieldByName('password').AsString);
            SQL := 'update users set password = ' + QuotedStr(Hash) + ' where (uid = ' + Query.FieldByName('uid').AsString + ')';
            try
              ADO.Execute(SQL,Count);
              if (Count <> 1) then begin
                Log('Error encrypting password for user UID ' + Query.FieldByName('uid').AsString + ' "' + Query.FieldByName('login_id').AsString + '"');
                Log(' - Count was ' + IntToStr(Count));
                Log(' - SQL was ' + SQL);
              end;
            except
              on E:Exception do begin
                Log('Error updating user UID ' + Query.FieldByName('uid').AsString + ' "' + Query.FieldByName('login_id').AsString + '"');
                Log(' - ' + E.ClassName + ' - ' + E.Message);
                Log(' - SQL was ' + SQL);
              end;
            end;
           end;
          Query.Next;
        end;
      finally
        FreeAndNil(Query);
      end;
    finally
      ADO.Connected := False;
    end;
  finally
    Log('');
    Log('Processing finished');
    Log('');
    Proceed.Enabled := True;
  end;
end;

procedure TPasswordEncryptor_Form.Log(const Txt: string);
begin
  LogMemo.Lines.Add(Txt);
end;

procedure TPasswordEncryptor_Form.FormCreate(Sender: TObject);
begin
  ClientHeight := ClientHeight - LogPanel.Height;
end;

end.
