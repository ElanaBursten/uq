program QMPasswordEncryptor;

uses
  Forms,
  PasswordEncryptorForm in 'PasswordEncryptorForm.pas' {PasswordEncryptor_Form},
  OdAdoUtils in '..\..\common\OdAdoUtils.pas',
  PasswordRules in '..\..\common\PasswordRules.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  OdExceptions in '..\..\common\OdExceptions.pas',
  OdDbUtils in '..\..\common\OdDbUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  Hashes in '..\..\thirdparty\Hashes.pas',
  UQDbConfig in '..\..\common\UQDbConfig.pas',
  QMConst in '..\..\client\QMConst.pas';

begin
  Application.Initialize;
  Application.CreateForm(TPasswordEncryptor_Form, PasswordEncryptor_Form);
  Application.Run;
end.
