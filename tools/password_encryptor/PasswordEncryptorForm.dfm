object PasswordEncryptor_Form: TPasswordEncryptor_Form
  Left = 273
  Top = 110
  Width = 522
  Height = 327
  Caption = 'QManager Password Database Encryptor'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 14
  object BottomPanel: TPanel
    Left = 0
    Top = 252
    Width = 514
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Proceed: TButton
      Left = 219
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Proceed'
      TabOrder = 0
      OnClick = ProceedClick
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 12
      Top = 12
      Width = 73
      Height = 22
      Caption = 'NOTICE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 12
      Top = 40
      Width = 437
      Height = 22
      Caption = 'This tool will encrypt all passwords in the database.  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 12
      Top = 68
      Width = 488
      Height = 22
      Caption = 'Passwords that were previously encrypted will be skipped.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
  object LogPanel: TPanel
    Left = 0
    Top = 105
    Width = 514
    Height = 147
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    TabOrder = 2
    Visible = False
    object LogMemo: TMemo
      Left = 8
      Top = 8
      Width = 498
      Height = 131
      Align = alClient
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
      WordWrap = False
    end
  end
  object ADO: TADOConnection
    Left = 12
    Top = 108
  end
end
