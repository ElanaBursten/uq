{
 * Diagnostic Application - wrapper for Powershell
 * Uses #7: ReDabbler using Pipes (Using Console library)
 * Kelly Caldwell
}

unit PSMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, StrUtils,

  ActiveX, ComObj,

  PJPipe, PJConsoleApp, ComCtrls;

type
  TfrmPSMain = class(TForm)
    btnRun: TButton;
    Label1: TLabel;
    InputMemo: TMemo;
    OutputMemo: TMemo;
    Label2: TLabel;
    lblTime1: TLabel;
    lblWindowsTime1: TLabel;
    btnClear: TButton;
    lblTime2: TLabel;
    lblWindowsTime2: TLabel;
    lblPSNow: TLabel;
    lblPSBootupTime: TLabel;
    lblWmiBootup: TLabel;
    lbWmiNowTime: TLabel;
    DateTimePicker1: TDateTimePicker;
    btnGetDateTime: TButton;
    lblPSVersion: TLabel;
    lblVersionOutput: TLabel;
    lblTimeFormat: TLabel;
    lblFormatOutputL: TLabel;
    lblFormatOutputS: TLabel;
    DateTimePicker2: TDateTimePicker;
    lblOSVersion: TLabel;
    lblOSOutput: TLabel;
    lblTimeZone1: TLabel;
    lblTimeZone: TLabel;
    lblComputerName: TLabel;
    btnSendToFile: TButton;
    procedure btnRunClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnGetDateTimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSendToFileClick(Sender: TObject);
  private
    fTextToUse: string;
    fOutPipe: TPJPipe;
    fOutStream: TStream;
    fOutStringList: TStringlist;
    fSystemFormat: TFormatSettings;
    function UtcToDateTime(const V : OleVariant): TDateTime;
    function GetWMIBootup: string;
    procedure GetDateTimes;
    procedure ClearAll;
    procedure WorkHandler(Sender: TObject);
    procedure ParseOutput(OutputLoc: integer);
    procedure RunPS(PSCommand: string; Outputloc: integer);
    procedure SendToTextFile;
  end;

var
  frmPSMain: TfrmPSMain;

const
  P = '$P = ';
  GetBootupTimeCommand = 'Get-CimInstance -ClassName win32_operatingsystem | select lastbootuptime | Format-Table -HideTableHeaders'; //-expandproperty lastbootuptime';
  GetBootupTimeCommandO = 'Get-WmiObject -Class win32_operatingsystem -Property LastBootupTime';
  GetLocalTime = 'Get-Date';
  PSVersion = '$PSVersionTable.PSVersion | ft -HideTableHeaders';
  OSVersion = '[System.Environment]::OSVersion.Version | ft -HideTableHeaders';
  TimeZone = 'Get-TimeZone | Select DisplayName | ft -HideTableHeaders';
  ComputerName = '$env:computername';
  WriteOutput =#13#10 + 'Write-Output "<<<" $P ">>>"';
  PSDateFormat = 'dddd, mmmm dd, yyyy h:nn:ss AM/PM';
  
implementation

{$R *.dfm}

procedure TfrmPSMain.btnClearClick(Sender: TObject);
begin
  ClearAll;
end;

procedure TfrmPSMain.btnSendToFileClick(Sender: TObject);
begin
  SendToTextFile;
end;

procedure TfrmPSMain.btnRunClick(Sender: TObject);
begin
  RunPS(fTextToUse,1);
  fOutStringList.Clear;
  //Memo2.Text:= '';
  RunPS(P+GetLocalTime+WriteOutput,2);
  fOutStringList.Clear; //Memo2.Text :='';
  RunPS(P+PSVersion+WriteOutput,3);
  fOutStringList.Clear; //Memo2.Text :='';
  RunPS(P+OSVersion+WriteOutput,4);
  fOutStringList.Clear; //Memo2.Text := '';
  RunPS(P+TimeZone+WriteOutput,5);
  fOutStringList.Clear; //Memo2.Text := '';
  RunPS(P+ComputerName+WriteOutput,6);

  btnSendToFile.Enabled := True;
end;

procedure TfrmPSMain.btnGetDateTimeClick(Sender: TObject);
begin
  GetDateTimes;
end;

procedure TfrmPSMain.ClearAll;
begin
  OutputMemo.Text:= '';
  fOutStringList.Clear;
  lblTime1.Caption:= '--';
  lblWindowsTime1.Caption := '--'; 
  lblTime2.Caption := '--';
  lblWindowsTime2.Caption := '--';
  btnSendToFile.Enabled := False;
  lblComputerName.Caption;
  lblTimeZone.Caption:= '';
  lblOSOutput.Caption:= '';
  lblVersionOutput.Caption:= '';
  lblFormatOutputL.Caption:= '';
  lblFormatOutputS.Caption:= '';
  lblComputerName.Caption:= '';
end;

procedure TfrmPSMain.FormCreate(Sender: TObject);
begin
  fOutStringList := TStringList.Create;
  fTextToUse := P + GetBootupTimeCommand + WriteOutput;
  InputMemo.Text := fTextToUse;
  fSystemFormat.Create('en-US');
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fSystemFormat);
  lblFormatOutputL.Caption := 'Long: ' + fSystemFormat.LongDateFormat + ' ' +
                             fSystemFormat.LongTimeFormat;
  lblFormatOutputS.Caption := 'Short: ' + fSystemFormat.ShortDateFormat + ' ' +
                             fSystemFormat.ShortTimeFormat;

end;

procedure TfrmPSMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(fOutStringList);
end;

procedure TfrmPSMain.RunPS(PSCommand: string; Outputloc: integer);
var
  App: TPJConsoleApp;
  Text: string;
  InPipe: TPJPipe;
begin
  fOutStream := nil;
  fOutPipe := nil;
  // Write memo 1 contents as Ansi text into read pipe. Must be ANSI text
  // because Echoer.exe requires Ansi input.
  if PSCommand <> '' then
    Text := PSCommand
  else
    Text := InputMemo.Text;
    
  InPipe := TPJPipe.Create(Length(Text));
  try
    {$IFDEF UNICODE}
    InPipe.WriteBytes(TEncoding.Default.GetBytes(Text));
    {$ELSE}
    InPipe.WriteData(PChar(Text)^, Length(Text));
    {$ENDIF}
    InPipe.CloseWriteHandle;
    // Create out pipe and stream that receives out pipe's data
    fOutPipe := TPJPipe.Create;
    fOutStream := TMemoryStream.Create;
    // Execute the application
    App := TPJConsoleApp.Create;
    try
      App.TimeSlice := 2; // forces more than one OnWork event
      App.OnWork := WorkHandler;
      App.StdIn := InPipe.ReadHandle;
      App.StdOut := fOutPipe.WriteHandle;

      if not App.Execute('Powershell') then
        raise Exception.CreateFmt(
          'Error %X: %s', [App.ErrorCode, App.ErrorMessage]
        );
    finally
      App.Free;
    end;
    {Load the Output stream into the fOutStringList
      Echoer.exe writes output as ANSI text.
      OK on Unicode Delphis because following LoadFromStream call
      defaults to Default (ANSI) encoding if no encoding specified.
    }
    fOutStream.Position := 0;
    fOutStringList.LoadFromStream(fOutStream);
    OutputMemo.Lines.AddStrings(fOutStringList);

  finally
    FreeAndNil(InPipe);
    FreeAndNil(fOutPipe);
    FreeAndNil(fOutStream);
  end;

  ParseOutput(OutputLoc);
end;

procedure TfrmPSMain.SendToTextFile;
var
  FileOut: TStringList;
const
  AFile = 'PSTest';
begin
  FileOut := TStringList.Create;
  try
    FileOut.Add('Machine:    ' + lblComputerName.Caption);
    FileOut.Add('TimeZone:   ' + lblTimeZone.Caption);
    FileOut.Add('OS Version: ' + lblOSOutput.Caption);
    FileOut.Add('PS Version: ' + lblVersionOutput.Caption);
    FileOut.Add('--------------------------------');
    FileOut.Add('PS BootUpTime:  ' + lblTime1.Caption );
    FileOut.Add('WMI BootUpTime: ' + lblWindowsTime1.Caption);
    FileOut.Add('--------------------------------');
    FileOut.Add('PS Time:  ' + lblTime2.Caption);
    FileOut.Add('WMI Time: ' + lblWindowsTime2.Caption);
    FileOut.Add('--------------------------------');
    FileOut.Add('Machine DateTime Format (Long):  ' + lblFormatOutputL.Caption);
    FileOut.Add('Machine DateTime Format (Short): ' + lblFormatOutputS.Caption);
    FileOut.SaveToFile(AFile + lblComputerName.Caption + '.txt');

  finally
    FreeAndNil(FileOut);
  end;
end;

procedure TfrmPSMain.ParseOutput(OutputLoc: integer);
var
  i, DTStartPos, DTEndPos: integer;
  OutStr: string;
  NowDT: TDateTime;
begin
    DTStartPos := LastDelimiter('<<<', fOutStringList.Text);
    DTEndPos :=   LastDelimiter('>>>', fOutStringList.Text);
    if DTStartPos > 0 then begin
      OutStr := copy(fOutStringList.Text, DTStartPos, DTEndPos);
      OutStr := ReplaceStr(OutStr, '<',  '');
     //
      DTEndPos := AnsiPos('>>>', OutStr);
      OutStr := copy(OutStr, 0, DTEndPos);
      OutStr := ReplaceStr(OutStr, '>',  '');
      OutStr := Trim(OutStr);
    end;

    case OutputLoc of
      1: 
      begin
        lblTime1.Caption := OutStr;
        lblWindowsTime1.Caption := GetWMIBootup;
      end;
      2: 
      begin
        lblTime2.Caption := OutStr;
        NowDT := Now;
        lblWindowsTime2.Caption := formatDateTime('mmmm dd, yyyy hh:nn:ss am/pm',NowDT);
      end;
      3:  lblVersionOutput.Caption := OutStr;

      4:  lblOSOutput.Caption := OutStr;

      5:  lblTimeZone.Caption := OutStr;

      6:  lblComputerName.Caption := OutStr;

    end;
end;

procedure TfrmPSMain.WorkHandler(Sender: TObject);
begin
  fOutPipe.CopyToStream(fOutStream, 0);
end;

//Universal Time (UTC) format of YYYYMMDDHHMMSS.MMMMMM(+-)OOO.
//20091231000000.000000+000
function TfrmPSMain.UtcToDateTime(const V : OleVariant): TDateTime;
var
  Dt : OleVariant;
begin
  Result:=0;
  if VarIsNull(V) then exit;
  Dt:=CreateOleObject('WbemScripting.SWbemDateTime');
  Dt.Value := V;
  Result:=Dt.GetVarDate;
end;

procedure TfrmPSMain.GetDateTimes; {Convert to our TDateTime}
var 
  S, ReFormatStr: string;
  DT: TDateTime;
  FmtSettings: TFormatSettings;
begin
  try
    FmtSettings.Create('en-US');
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, FmtSettings);
    fSystemFormat := FmtSettings;
//    ShowMessage(FmtSettings.LongMonthNames[2]);
//    FmtSettings.LongDateFormat := 'dddd, mmmm dd, yyyy';
//    FmtSettings.LongTimeFormat:= 'h:nn:ss AM/PM';
//    FmtSettings.ShortDateFormat := 'dddd, mmmm dd, yyyy';
//    FmtSettings.ShortTimeFormat := 'h:nn:ss AM/PM';
//    FmtSettings.TimeAMString := 'AM';
//    FmtSettings.TimePMString := 'PM';
//    FmtSettings.TimeSeparator := ':';
//    FmtSettings.DateSeparator :='';

    S := FormatDateTime('', Now, FmtSettings);
    DT := StrToDateTime(S, FmtSettings);
    DateTimePicker1.DateTime := StrToDateTime(lblTime1.Caption, FmtSettings);
  finally

  end;

end;

function TfrmPSMain.GetWMIBootup: string;
const
  WbemUser            ='';
  WbemPassword        ='';
  WbemComputer        ='localhost';
  wbemFlagForwardOnly = $00000020;
var
  FSWbemLocator : OLEVariant;
  FWMIService   : OLEVariant;
  FWbemObjectSet: OLEVariant;
  FWbemObject   : OLEVariant;
  oEnum         : IEnumvariant;
  iValue        : LongWord;
  SysFormatStr  : string;
begin;
  FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  FWMIService   := FSWbemLocator.ConnectServer(WbemComputer, 'root\CIMV2', WbemUser, WbemPassword);
  FWbemObjectSet:= FWMIService.ExecQuery('SELECT * FROM Win32_OperatingSystem','WQL',wbemFlagForwardOnly);
  oEnum         := IUnknown(FWbemObjectSet._NewEnum) as IEnumVariant;
  if oEnum.Next(1, FWbemObject, iValue) = 0 then
  begin
    SysFormatStr := fSystemFormat.ShortDateFormat + ' ' + fSystemFormat.ShortTimeFormat;
    Result := Format('%s',[formatDateTime(SysFormatStr,UtcToDateTime(FWbemObject.LastBootUpTime))]);// Datetime
  end;
end;


end.

