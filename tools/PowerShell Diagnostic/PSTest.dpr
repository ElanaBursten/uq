{
 * Project file for DelphiDabbler Console Application Runner Classes demo
 * program #7: Redirecting standard i/o using pipes.
 *
 * $Rev: 1358 $
 * $Date: 2013-03-25 03:31:31 +0000 (Mon, 25 Mar 2013) $
 *
 * Any copyright in this file is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
}

program PSTest;

uses
  Forms,
  PSMain in 'PSMain.pas' {frmPSMain};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmPSMain, frmPSMain);
  Application.Run;
end.

