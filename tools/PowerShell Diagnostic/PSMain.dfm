object frmPSMain: TfrmPSMain
  Left = 257
  Top = 159
  BorderStyle = bsSingle
  Caption = 'POWERSHELL - PIPE DATA PROTOTYPE'
  ClientHeight = 511
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 24
    Width = 415
    Height = 15
    AutoSize = False
    Caption = 'POWERSHELL COMMAND (after it has been initiated)'
    WordWrap = True
  end
  object Label2: TLabel
    Left = 8
    Top = 132
    Width = 415
    Height = 31
    AutoSize = False
    Caption = 'POWERSHELL OUTPUT'
    WordWrap = True
  end
  object lblTime1: TLabel
    Left = 8
    Top = 334
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblWindowsTime1: TLabel
    Left = 8
    Top = 396
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTime2: TLabel
    Left = 472
    Top = 334
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblWindowsTime2: TLabel
    Left = 472
    Top = 396
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblPSNow: TLabel
    Left = 472
    Top = 318
    Width = 194
    Height = 16
    Caption = 'PowerShell System Time (NOW):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblPSBootupTime: TLabel
    Left = 8
    Top = 318
    Width = 146
    Height = 16
    Caption = 'PowerShell Bootup Time:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblWmiBootup: TLabel
    Left = 8
    Top = 382
    Width = 108
    Height = 16
    Caption = 'WMI Bootup Time:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lbWmiNowTime: TLabel
    Left = 472
    Top = 382
    Width = 156
    Height = 16
    Caption = 'WMI System Time (NOW):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblPSVersion: TLabel
    Left = 8
    Top = 423
    Width = 114
    Height = 16
    Caption = 'Powershell Version:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblVersionOutput: TLabel
    Left = 8
    Top = 439
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTimeFormat: TLabel
    Left = 472
    Top = 449
    Width = 133
    Height = 16
    Caption = 'Computer Time Format'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblFormatOutputL: TLabel
    Left = 472
    Top = 467
    Width = 8
    Height = 16
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblFormatOutputS: TLabel
    Left = 472
    Top = 483
    Width = 8
    Height = 16
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblOSVersion: TLabel
    Left = 240
    Top = 423
    Width = 70
    Height = 16
    Caption = 'OS Version:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblOSOutput: TLabel
    Left = 240
    Top = 439
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTimeZone1: TLabel
    Left = 8
    Top = 463
    Width = 56
    Height = 16
    Caption = 'TimeZone'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object lblTimeZone: TLabel
    Left = 8
    Top = 485
    Width = 10
    Height = 18
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblComputerName: TLabel
    Left = 8
    Top = 297
    Width = 111
    Height = 16
    Caption = 'Computer Name: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnRun: TButton
    Left = 760
    Top = 8
    Width = 74
    Height = 30
    Caption = 'Run'
    Default = True
    TabOrder = 0
    OnClick = btnRunClick
  end
  object InputMemo: TMemo
    Left = 8
    Top = 41
    Width = 824
    Height = 87
    Lines.Strings = (
      
        '$P = Get-CimInstance -ClassName win32_operatingsystem | select l' +
        'astbootuptime -expandproperty lastbootuptime '
      'Write-Output "<<<" $P ">>>"')
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
  end
  object OutputMemo: TMemo
    Left = 8
    Top = 148
    Width = 826
    Height = 144
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
  end
  object btnClear: TButton
    Left = 680
    Top = 8
    Width = 74
    Height = 30
    Caption = 'Clear'
    TabOrder = 3
    OnClick = btnClearClick
  end
  object DateTimePicker1: TDateTimePicker
    Left = 8
    Top = 353
    Width = 305
    Height = 23
    Date = 43930.518211168980000000
    Format = 'MM/dd/yyyy hh:mm:ss'
    Time = 43930.518211168980000000
    DateFormat = dfLong
    TabOrder = 4
  end
  object btnGetDateTime: TButton
    Left = 349
    Top = 353
    Width = 74
    Height = 23
    Caption = 'Get Date'
    Enabled = False
    TabOrder = 5
    OnClick = btnGetDateTimeClick
  end
  object DateTimePicker2: TDateTimePicker
    Left = 472
    Top = 353
    Width = 305
    Height = 23
    Date = 43930.518211168980000000
    Format = 'MM/dd/yyyy hh:mm:ss'
    Time = 43930.518211168980000000
    DateFormat = dfLong
    Enabled = False
    TabOrder = 6
  end
  object btnSendToFile: TButton
    Left = 728
    Top = 298
    Width = 106
    Height = 23
    Caption = 'Send To File'
    Enabled = False
    TabOrder = 7
    OnClick = btnSendToFileClick
  end
end
