object frmMainPar: TfrmMainPar
  Left = 0
  Top = 0
  Caption = '               Jack Rabbit Always Wins'
  ClientHeight = 355
  ClientWidth = 888
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Visible = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 888
    Height = 225
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnRun: TButton
    Left = 360
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 1
    OnClick = btnRunClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 336
    Width = 888
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'Not Connected'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object cbRunOnce: TCheckBox
    Left = 728
    Top = 272
    Width = 97
    Height = 17
    Caption = 'Run Once'
    TabOrder = 3
  end
  object qryRabbitMQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'QueueName'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select *'
      'from [dbo].[rabbitmq_in]'
      'where [processed]=0'
      'and status = '#39'NEW'#39
      'and queue_name= :QueueName')
    Left = 40
    Top = 120
  end
  object spAssignLocate: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'assign_locate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkloadDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 40
    Top = 200
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 40
    Top = 40
  end
  object qryFiberCheck: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'select locate_id'
      'from locate '
      'where ticket_id=123456'
      'and client_code in ('#39'SB2357'#39', '#39'SB2455'#39')')
    Left = 232
    Top = 136
  end
  object updRabbitMQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'processed'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'MsgID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [dbo].[rabbitmq_in]'
      '   SET [processed] = :processed'
      '      ,[status] = :status'
      ' WHERE message_id = :MsgID')
    Left = 392
    Top = 80
  end
  object delRabbitMQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'MsgID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'delete '
      'from [dbo].[rabbitmq_in]'
      ' WHERE message_id = :MsgID')
    Left = 392
    Top = 160
  end
  object qryBucket: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'drop table if exists #TempTable'
      ''
      
        'Select top 1 [dbo].[extractNumbers](short_name)+'#39'..WLM-PAR'#39' as W' +
        'ML,'
      'L.locate_id, L.status'
      'into #TempTable'
      'from locate L'
      'join employee E on L.assigned_to_id = E.emp_id'
      ''
      'where ticket_id=:TicketID'
      'and E.active=1'
      'and e.can_receive_tickets=1'
      'and upper(L.status)<>'#39'-P'#39
      ''
      
        'select emp_ID as WLM_EmpID, short_name as WML_ShortName, TT.loca' +
        'te_id as LocateID, TT.Status'
      'from employee E'
      'join #TempTable TT on (E.short_name=TT.WML)')
    Left = 504
    Top = 80
  end
  object qryTicketID_WO: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'WorkOrderNumber'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select ticket_id'
      'from ticket'
      'where ticket_number=:WorkOrderNumber'
      'and ticket_format ='#39'FPL1'#39)
    Left = 656
    Top = 272
  end
  object insTicketNote: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'empID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [dbo].[notes]'
      '           ([foreign_type]'
      '           ,[foreign_id]'
      '           ,[entry_date]'
      '           ,[modified_date]'
      '           ,[uid]'
      '           ,[active]'
      '           ,[note]'
      '           ,[sub_type])'
      '     VALUES'
      '           (1'
      '           ,:ticketID'
      '           ,GETDATE()'
      '           ,GETDATE()'
      '           ,:empID'
      '           ,1'
      '           ,:note'
      '           ,101)')
    Left = 616
    Top = 80
  end
end
