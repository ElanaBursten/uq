program ParForTheCourse;
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
//qm-677
uses
  Vcl.Forms,
  SysUtils,
  ParMain in 'ParMain.pas' {frmMainPar},
  GlobalSU in 'GlobalSU.pas';

var
  MyInstanceName: string;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);

  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1) = 'GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm := True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm := False;
    end;
    Application.Title := '';

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmMainPar, frmMainPar);
  Application.Run;
  end
  else
    Application.Terminate;

end.
