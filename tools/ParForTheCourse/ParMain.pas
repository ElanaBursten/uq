unit ParMain;
//QM-773 Create a flag on the client table to keep it from getting assigned to the PAR bucket

// qm-677
interface

uses
  Winapi.Windows, Winapi.Messages, Winapi.ActiveX, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.StdCtrls,
  System.JSON, Vcl.ComCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

  TBucketInfo = record
    WLM_EmpID: integer;
    WML_ShortName: string[20];
    LocateID: integer;
    Status: string[6] end;

  type
    TfrmMainPar = class(TForm)
      qryRabbitMQ: TADOQuery;
      Memo1: TMemo;
      btnRun: TButton;
      spAssignLocate: TADOStoredProc;
      ADOConn: TADOConnection;
      qryFiberCheck: TADOQuery;
      updRabbitMQ: TADOQuery;
      delRabbitMQ: TADOQuery;
      qryBucket: TADOQuery;
      StatusBar1: TStatusBar;
      qryTicketID_WO: TADOQuery;
      cbRunOnce: TCheckBox;
      insTicketNote: TADOQuery;
      procedure btnRunClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure Button1Click(Sender: TObject);
    private
      SkipList: TStrings;
      aDatabase: String;
      aServer: String;
      ausername: string;
      apassword: string;
      BucketInfo: TBucketInfo;
      LogResult: TLogResults;
      flogDir: wideString;
      QueueName:string;
      function InsertTicketNote(TicketID, EmpID:integer; Note:string): boolean;  //qm-769
      function ProcessINI: boolean;
      function connectDB: boolean;
      procedure clearLogRecord;
      function getData(JsonString, Field: String): String;
      function FiberChecked(ticketID: integer): boolean;
      procedure ProcessPars;
      procedure DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
      function GetBucket(ticketID: integer): TBucketInfo;
      function ReAssignTicket(InsertDate: TDateTime; BucketInfo: TBucketInfo): boolean;
      function GetAppVersionStr: string;
      { Private declarations }

    public
      { Public declarations }
      procedure WriteLog(LogResult: TLogResults);
    end;

  var
    frmMainPar: TfrmMainPar;

implementation

uses dateutils, StrUtils, iniFiles;

{$R *.dfm}

function TfrmMainPar.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  try
    Result := '';
    // create TJSonObject from string
    JSonValue := TJSonObject.ParseJSONValue(JsonString);

    Result := JSonValue.GetValue<string>(Field);
    JSonValue.Free;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'getData';
      LogResult.DataStream := JSonValue.ToString;
      LogResult.ExcepMsg := E.Message;
      LogResult.Status := 'Failed';
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmMainPar.btnRunClick(Sender: TObject);
begin
  CoInitialize(nil);
  TThread.CreateAnonymousThread(
    procedure
    begin
      ProcessPars;
    end).Start;
end;

function TfrmMainPar.FiberChecked(ticketID: integer): boolean;
const
  IS_IT_FIBER = 'select locate_id ' + 'from locate ' + 'where ticket_id=''%d'' ' + 'and client_code in (''%s'') ';

begin
  qryFiberCheck.SQL.Clear;
  qryFiberCheck.SQL.Text := Format(IS_IT_FIBER, [ticketID, SkipList.CommaText]);
  try
    qryFiberCheck.open;
    Result := qryFiberCheck.FieldByName('locate_id').IsNull;
  finally
    qryFiberCheck.close;
  end;
end;

procedure TfrmMainPar.ProcessPars;
var
  parImage: string;
  sTicketID: string;
  ticketID: integer;
  EmpID:integer; //qm-769
  MsgID: integer;
  InsertDate: TDateTime;
  DueDate: TDateTime;
  sDueDate: string;
  cnt, good, fail: integer;
  HPAttributes,LangAITags:string;  //qm-769
const
  FIBER = 'Fiber';
  DONE = 'PROCESSED';
  FAILED = 'FAILED';
  NO_DUE_DATE = 'No Due Date In Ticket'; // qm-740
begin
  cnt := 0;
  good := 0;
  fail := 0;
  InsertDate := 0.0;
  sDueDate := '';
  DueDate := 0.0;
  HPAttributes:='';  //qm-769
  LangAITags :='';  //qm-769
  EmpID:=10; //qm-769
  try
    MsgID := 0;
    try
      qryRabbitMQ.Parameters.ParamByName('QueueName').Value:=  QueueName;
      qryRabbitMQ.open;
      while not(qryRabbitMQ.Eof) do
      begin
        inc(cnt);
        MsgID := qryRabbitMQ.FieldByName('message_id').AsInteger;
        InsertDate := qryRabbitMQ.FieldByName('insert_date').AsDateTime;
        parImage := qryRabbitMQ.FieldByName('message_payload').AsString;

        Memo1.Lines.Add(parImage);
        try
          sTicketID := getData(parImage, 'WorkOrderId');
          sDueDate := getData(parImage, 'DueDate'); // qm-714 sr
//          if pos('HighProfileAttributes',parImage) then
//          HPAttributes:= getData(parImage, 'HighProfileAttributes'); //qm-769
//          if pos('LangAITags',parImage) then
//          LangAITags:= getData(parImage, 'LangAITags'); //qm-769
//
//          if HPAttributes<>'' then  InsertTicketNote(TicketID, EmpID,HPAttributes);  //qm-769
//          if LangAITags<>'' then  InsertTicketNote(TicketID, EmpID,LangAITags);  //qm-769

          if sDueDate <> NO_DUE_DATE then // qm-740
            DueDate := VarToDateTime(sDueDate)
          else
          begin
            LogResult.LogType := ltWarning; // qm-740
            LogResult.MethodName := 'MsgID: ' + IntToStr(MsgID) + ' getData Due Date';
            LogResult.DataStream := 'parImage: ' + parImage;
            LogResult.Status := NO_DUE_DATE;
            WriteLog(LogResult);
          end;

        except
          on E: Exception do
          begin
            DeleteFromMQ(MsgID, FAILED, False);
            LogResult.LogType := ltError;
            LogResult.MethodName := 'getData';
            LogResult.DataStream := 'parImage: ' + parImage;
            LogResult.Status := 'Invalid JSON format';
            WriteLog(LogResult);
            qryRabbitMQ.Next;
            continue;
          end;
        end;

        if (sDueDate <> NO_DUE_DATE) and (DueDate < NOW) then // qm-714 sr
        begin
          DeleteFromMQ(MsgID, FAILED, False);
          LogResult.LogType := ltWarning;
          LogResult.MethodName := 'Due date Check';
          LogResult.DataStream := 'parImage: ' + parImage;
          LogResult.Status := 'Late ticket';
          WriteLog(LogResult);
          qryRabbitMQ.Next;
          continue;
        end;

        ticketID := StrToInt(sTicketID); // overriden for testing
        if not(FiberChecked(ticketID)) then // fiber on ticket
        begin
          DeleteFromMQ(MsgID, FIBER, False);

          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'FiberChecked';
          LogResult.DataStream := 'Ticket ID: ' + sTicketID;
          LogResult.Status := 'Skip due to fiber';
          WriteLog(LogResult);

          qryRabbitMQ.Next;
          continue;
        end;

        If ReAssignTicket(InsertDate, GetBucket(ticketID)) then
        begin
          DeleteFromMQ(MsgID, DONE, True);
        end
        else
        Begin
          DeleteFromMQ(MsgID, FAILED, False);
          LogResult.LogType := ltWarning;
          LogResult.MethodName := 'ReAssignTicket';
          LogResult.DataStream := 'payload: ' + parImage;
          LogResult.Status := 'Failed';
          WriteLog(LogResult);
        End;

        qryRabbitMQ.Next;
        inc(good);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'ProcessPars';
        LogResult.DataStream := 'payload: ' + parImage;
        LogResult.Status := 'Processed';
        WriteLog(LogResult);

        StatusBar1.Panels[3].Text := IntToStr(good);
        if cbRunOnce.Checked then
          exit;

      end; // while not eof
    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'ProcessPars';
        LogResult.Status := 'Ticket ID: ' + sTicketID;
        LogResult.DataStream := 'Ticket ID: ' + sTicketID;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        StatusBar1.Panels[5].Text := IntToStr(fail);
      end;
    end;
  finally
    StatusBar1.Panels[7].Text := IntToStr(cnt);
    qryRabbitMQ.close;
    CoUnInitialize;
  end;

end;
function TfrmMainPar.InsertTicketNote(TicketID, EmpID:integer; Note:string): boolean;
begin
  insTicketNote.Parameters.ParamByName('ticketID').Value := TicketID;
  insTicketNote.Parameters.ParamByName('EmpID').Value := EmpID; //check for UID
  insTicketNote.Parameters.ParamByName('Note').Value := Note;
  result:=insTicketNote.ExecSQL>1;
end;

function TfrmMainPar.ReAssignTicket(InsertDate: TDateTime; BucketInfo: TBucketInfo): boolean;
begin
  Result := False;
  spAssignLocate.Parameters.ParamByName('@LocateID').Value := BucketInfo.LocateID;
  spAssignLocate.Parameters.ParamByName('@LocatorID').Value := BucketInfo.WLM_EmpID;
  spAssignLocate.Parameters.ParamByName('@AddedBy').Value := 23;
  spAssignLocate.Parameters.ParamByName('@WorkloadDate').Value := InsertDate;
  spAssignLocate.ExecProc;
  If spAssignLocate.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then
  begin
    Result := True;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ReAssignTicket';
    LogResult.DataStream := 'Locate ID: ' + IntToStr(BucketInfo.LocateID);
    LogResult.Status := 'Assigned to: ' + IntToStr(BucketInfo.WLM_EmpID);
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'ReAssignTicket';
    LogResult.DataStream := 'Locate ID: ' + IntToStr(BucketInfo.LocateID);
    WriteLog(LogResult);
    Result := False;
    Memo1.Lines.Add('ReAssignTicket Error: ' + IntToStr(BucketInfo.LocateID));
  end;

end;

function TfrmMainPar.GetBucket(ticketID: integer): TBucketInfo;
begin
  try
    qryBucket.Parameters.ParamByName('TicketID').Value := ticketID;
    qryBucket.open;
    Result.WLM_EmpID := qryBucket.FieldByName('WLM_EmpID').AsInteger;
    Result.WML_ShortName := qryBucket.FieldByName('WML_ShortName').AsString;
    Result.LocateID := qryBucket.FieldByName('LocateID').AsInteger;
    Result.Status := qryBucket.FieldByName('Status').AsString;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'GetBucket';
    LogResult.Status := 'Locate ID: ' + IntToStr(Result.LocateID);
    LogResult.DataStream := 'Short Name :' + Result.WML_ShortName + 'WLM_EmpID :' + IntToStr(Result.WLM_EmpID);
    WriteLog(LogResult);
  finally
    qryBucket.close;
  end;
end;

procedure TfrmMainPar.DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
begin
  updRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  updRabbitMQ.Parameters.ParamByName('processed').Value := process;
  updRabbitMQ.Parameters.ParamByName('status').Value := Status;
  if updRabbitMQ.ExecSQL > 0 then
    delRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  delRabbitMQ.ExecSQL;
end;

procedure TfrmMainPar.Button1Click(Sender: TObject);
begin
  //
end;

procedure TfrmMainPar.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMainPar.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try

    ADOConn.open;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aDatabase;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

procedure TfrmMainPar.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  SkipList := TStringList.Create;
  SkipList.Delimiter := #44;
  AppVer := GetAppVersionStr;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  StatusBar1.Panels[9].Text := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  ProcessINI;
  connectDB;
  QueueName := ParamStr(1);
  if QueueName='' then showmessage('Rabbit MQ queue name is required as the first command line parameter');

  if uppercase(ParamStr(2)) <> 'GUI' then
  begin
    ProcessPars;
    application.Terminate;
  end;
end;

procedure TfrmMainPar.FormDestroy(Sender: TObject);
begin
  SkipList.Free;
end;

function TfrmMainPar.ProcessINI: boolean;
var
  ParForTheCourse: TIniFile;
begin
  try
    ParForTheCourse := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := ParForTheCourse.ReadString('Database', 'Server', '');
    aDatabase := ParForTheCourse.ReadString('Database', 'DB', 'QM');
    ausername := ParForTheCourse.ReadString('Database', 'UserName', '');
    apassword := ParForTheCourse.ReadString('Database', 'Password', '');
    SkipList.DelimitedText := ParForTheCourse.ReadString('SkipList', 'NoGo', '');
    flogDir := ParForTheCourse.ReadString('LogPaths', 'LogPath', '');
    Memo1.Lines := SkipList;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    ParForTheCourse.Free;
  end;
end;

procedure TfrmMainPar.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName := IncludeTrailingBackslash(flogDir) + 'ParForTheCourse_'+QueueName +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

function TfrmMainPar.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
