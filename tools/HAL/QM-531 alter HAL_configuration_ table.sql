USE [QM]  
GO
/****** Object:  Table [dbo].[hal]    Script Date: 2/27/2023 5:51:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
DROP TABLE [dbo].[hal]
GO
CREATE TABLE [dbo].[hal](
	[hal_id] [int] IDENTITY(1,1) NOT NULL,
	[hal_module] [varchar](20) NOT NULL,
	[daysback] [int] NOT NULL,
	[emails] [varchar](200) NOT NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[hal] ON 

INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (1, N'attachments', 7, N'brian.pike@usa.net', 1, CAST(0x0000AE6A00D9FA04 AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (8, N'assigned_tickets', 9, N'emailtkts.2051@tickets.utiliquest.com', 1, CAST(0x0000AE7E00A1667D AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (9, N'call_center', 7, N'brian.pike@usa.net', 1, CAST(0x0000AF0000477CC2 AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (11, N'active_users', 0, N'brian.pike@usa.net', 1, CAST(0x0000AF2700FA32DB AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (1012, N'blank_locators', 360, N'brian.pike@usa.net', 1, CAST(0x0000AFB6003E1B42 AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (1011, N'epr_response', 0, N'brian.pike@usa.net', 1, CAST(0x0000AFA701417251 AS DateTime))
SET IDENTITY_INSERT [dbo].[hal] OFF
ALTER TABLE [dbo].[hal] ADD  CONSTRAINT [DF_hal_active]  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[hal] ADD  CONSTRAINT [DF_hal_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO
