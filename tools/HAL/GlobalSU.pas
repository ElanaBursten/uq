unit GlobalSU;

interface
 function IsAppRunning: Boolean;
 function CreateSingleInstance(const InstanceName: string): boolean;
implementation

uses
 WinApi.Windows, SysUtils, Forms;

function IsAppRunning: Boolean;
var
 rtn : Cardinal;
begin
  result := False;
  CreateMutex(nil, False, PWideChar(ExtractFileName(Application.ExeName)));
  rtn := GetLastError;
  if rtn = ERROR_ALREADY_EXISTS then
   result := True;
end;


function CreateSingleInstance(const InstanceName: string): Boolean;
var
  MutexHandle: THandle;
begin
  MutexHandle := CreateMutex(nil, False, PChar(InstanceName));
  // if MutexHandle created check if already exists
  if (MutexHandle <> 0) then
  begin
    if GetLastError = ERROR_ALREADY_EXISTS then
    begin
      result := False;
      CloseHandle(MutexHandle);
    end
    else
      result := True;
  end
  else
    result := False;
end;


function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := paramStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
