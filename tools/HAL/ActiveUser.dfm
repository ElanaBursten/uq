object ActiveUsersForm: TActiveUsersForm
  Left = 0
  Top = 0
  Caption = 'Active Users'
  ClientHeight = 604
  ClientWidth = 1015
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 305
    Width = 1015
    Height = 8
    Cursor = crVSplit
    Align = alTop
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 49
    Width = 1015
    Height = 256
    Align = alTop
    TabOrder = 0
    object EmpByCoGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Enabled = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Enabled = False
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Enabled = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Enabled = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = DMHalSentinel.dsEmpbyCompany
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object EmpByCoGridViewfn: TcxGridDBColumn
        Caption = 'first name'
        DataBinding.FieldName = 'fn'
        Width = 83
      end
      object EmpByCoGridViewln: TcxGridDBColumn
        Caption = 'last name'
        DataBinding.FieldName = 'ln'
        Width = 96
      end
      object EmpByCoGridViewmiddle_init: TcxGridDBColumn
        Caption = 'middle init'
        DataBinding.FieldName = 'middle_init'
        Width = 60
      end
      object EmpByCoGridViewshort_name: TcxGridDBColumn
        Caption = 'short name'
        DataBinding.FieldName = 'short_name'
        Width = 155
      end
      object EmpByCoGridViewad_username: TcxGridDBColumn
        Caption = 'ad username'
        DataBinding.FieldName = 'ad_username'
        Width = 90
      end
      object EmpByCoGridViewemail_address: TcxGridDBColumn
        Caption = 'email address'
        DataBinding.FieldName = 'email_address'
        Width = 248
      end
      object EmpByCoGridViewemp_number: TcxGridDBColumn
        Caption = 'emp number'
        DataBinding.FieldName = 'emp_number'
      end
      object EmpByCoGridViewemp_id: TcxGridDBColumn
        Caption = 'emp id'
        DataBinding.FieldName = 'emp_id'
        Width = 81
      end
      object EmpByCoGridViewaddress_street: TcxGridDBColumn
        Caption = 'address street'
        DataBinding.FieldName = 'address_street'
        Width = 82
      end
      object EmpByCoGridViewaddress_state: TcxGridDBColumn
        Caption = 'address state'
        DataBinding.FieldName = 'address_state'
        Width = 81
      end
      object EmpByCoGridViewaddress_zip: TcxGridDBColumn
        Caption = 'address zip'
        DataBinding.FieldName = 'address_zip'
        Width = 71
      end
      object EmpByCoGridViewphone: TcxGridDBColumn
        DataBinding.FieldName = 'phone'
        Width = 108
      end
      object EmpByCoGridViewcontact_phone: TcxGridDBColumn
        Caption = 'contact phone'
        DataBinding.FieldName = 'contact_phone'
      end
      object EmpByCoGridViewcomp_name: TcxGridDBColumn
        Caption = 'company name'
        DataBinding.FieldName = 'comp_name'
        Width = 113
      end
      object EmpByCoGridViewactive: TcxGridDBColumn
        DataBinding.FieldName = 'active'
      end
      object EmpByCoGridViewtimesheet_num: TcxGridDBColumn
        Caption = 'timesheet num'
        DataBinding.FieldName = 'timesheet_num'
        Width = 91
      end
      object EmpByCoGridViewuid: TcxGridDBColumn
        DataBinding.FieldName = 'uid'
      end
      object EmpByCoGridViewgrp_id: TcxGridDBColumn
        Caption = 'grp id'
        DataBinding.FieldName = 'grp_id'
      end
      object EmpByCoGridViewlogin_id: TcxGridDBColumn
        Caption = 'login id'
        DataBinding.FieldName = 'login_id'
        Width = 64
      end
      object EmpByCoGridViewpassword: TcxGridDBColumn
        DataBinding.FieldName = 'password'
      end
      object EmpByCoGridViewchg_pwd: TcxGridDBColumn
        Caption = 'chg pwd'
        DataBinding.FieldName = 'chg_pwd'
        Width = 85
      end
      object EmpByCoGridViewchg_pwd_date: TcxGridDBColumn
        Caption = 'chg pwd date'
        DataBinding.FieldName = 'chg_pwd_date'
      end
      object EmpByCoGridViewlast_login: TcxGridDBColumn
        DataBinding.FieldName = 'last_login'
        Width = 71
      end
      object EmpByCoGridViewactive_ind: TcxGridDBColumn
        DataBinding.FieldName = 'active_ind'
      end
      object EmpByCoGridViewcan_view_notes: TcxGridDBColumn
        DataBinding.FieldName = 'can_view_notes'
        Width = 55
      end
      object EmpByCoGridViewcan_view_history: TcxGridDBColumn
        DataBinding.FieldName = 'can_view_history'
        Width = 53
      end
      object EmpByCoGridViewmodified_date: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
      end
      object EmpByCoGridViewetl_access: TcxGridDBColumn
        DataBinding.FieldName = 'etl_access'
        Width = 67
      end
      object EmpByCoGridViewapi_key: TcxGridDBColumn
        DataBinding.FieldName = 'api_key'
      end
      object EmpByCoGridViewaccount_disabled_date: TcxGridDBColumn
        DataBinding.FieldName = 'account_disabled_date'
        Width = 122
      end
      object EmpByCoGridViewtype_id: TcxGridDBColumn
        DataBinding.FieldName = 'type_id'
      end
      object EmpByCoGridViewstatus_id: TcxGridDBColumn
        DataBinding.FieldName = 'status_id'
      end
      object EmpByCoGridViewtimesheet_id: TcxGridDBColumn
        DataBinding.FieldName = 'timesheet_id'
        Width = 85
      end
      object EmpByCoGridViewcreate_date: TcxGridDBColumn
        DataBinding.FieldName = 'create_date'
      end
      object EmpByCoGridViewcreate_uid: TcxGridDBColumn
        DataBinding.FieldName = 'create_uid'
      end
      object EmpByCoGridViewmodified_uid: TcxGridDBColumn
        DataBinding.FieldName = 'modified_uid'
        Width = 73
      end
      object EmpByCoGridViewcharge_cov: TcxGridDBColumn
        DataBinding.FieldName = 'charge_cov'
        Width = 44
      end
      object EmpByCoGridViewcan_receive_tickets: TcxGridDBColumn
        DataBinding.FieldName = 'can_receive_tickets'
        Width = 46
      end
      object EmpByCoGridViewtimerule_id: TcxGridDBColumn
        DataBinding.FieldName = 'timerule_id'
      end
      object EmpByCoGridViewdialup_user: TcxGridDBColumn
        DataBinding.FieldName = 'dialup_user'
        Width = 77
      end
      object EmpByCoGridViewcompany_car: TcxGridDBColumn
        DataBinding.FieldName = 'company_car'
        Width = 74
      end
      object EmpByCoGridViewrepr_pc_code: TcxGridDBColumn
        DataBinding.FieldName = 'repr_pc_code'
      end
      object EmpByCoGridViewpayroll_pc_code: TcxGridDBColumn
        DataBinding.FieldName = 'payroll_pc_code'
      end
      object EmpByCoGridViewcrew_num: TcxGridDBColumn
        DataBinding.FieldName = 'crew_num'
        Width = 57
      end
      object EmpByCoGridViewautoclose: TcxGridDBColumn
        DataBinding.FieldName = 'autoclose'
      end
      object EmpByCoGridViewcompany_id: TcxGridDBColumn
        DataBinding.FieldName = 'company_id'
        Width = 88
      end
      object EmpByCoGridViewrights_modified_date: TcxGridDBColumn
        DataBinding.FieldName = 'rights_modified_date'
      end
      object EmpByCoGridViewticket_view_limit: TcxGridDBColumn
        DataBinding.FieldName = 'ticket_view_limit'
      end
      object EmpByCoGridViewhire_date: TcxGridDBColumn
        DataBinding.FieldName = 'hire_date'
      end
      object EmpByCoGridViewshow_future_tickets: TcxGridDBColumn
        DataBinding.FieldName = 'show_future_tickets'
      end
      object EmpByCoGridViewincentive_pay: TcxGridDBColumn
        DataBinding.FieldName = 'incentive_pay'
        Width = 53
      end
      object EmpByCoGridViewlocal_utc_bias: TcxGridDBColumn
        DataBinding.FieldName = 'local_utc_bias'
        Width = 72
      end
      object EmpByCoGridViewtest_com_login: TcxGridDBColumn
        DataBinding.FieldName = 'test_com_login'
      end
      object EmpByCoGridViewadp_login: TcxGridDBColumn
        DataBinding.FieldName = 'adp_login'
      end
      object EmpByCoGridViewhome_lat: TcxGridDBColumn
        DataBinding.FieldName = 'home_lat'
      end
      object EmpByCoGridViewhome_lng: TcxGridDBColumn
        DataBinding.FieldName = 'home_lng'
      end
      object EmpByCoGridViewalt_lat: TcxGridDBColumn
        DataBinding.FieldName = 'alt_lat'
      end
      object EmpByCoGridViewalt_lng: TcxGridDBColumn
        DataBinding.FieldName = 'alt_lng'
      end
      object EmpByCoGridViewplat_update_date: TcxGridDBColumn
        DataBinding.FieldName = 'plat_update_date'
      end
      object EmpByCoGridViewplat_age: TcxGridDBColumn
        DataBinding.FieldName = 'plat_age'
        Options.Editing = False
      end
      object EmpByCoGridViewchangeby: TcxGridDBColumn
        DataBinding.FieldName = 'changeby'
        Options.Editing = False
        Width = 133
      end
      object EmpByCoGridViewphoneco_ref: TcxGridDBColumn
        DataBinding.FieldName = 'phoneco_ref'
        Options.Editing = False
        Width = 76
      end
      object EmpByCoGridViewatlas_number: TcxGridDBColumn
        DataBinding.FieldName = 'atlas_number'
        Options.Editing = False
        Width = 110
      end
      object EmpByCoGridViewfirst_task_reminder: TcxGridDBColumn
        DataBinding.FieldName = 'first_task_reminder'
        Options.Editing = False
      end
      object EmpByCoGridViewterminated: TcxGridDBColumn
        DataBinding.FieldName = 'terminated'
        Options.Editing = False
      end
      object EmpByCoGridViewtermination_date: TcxGridDBColumn
        DataBinding.FieldName = 'termination_date'
        Options.Editing = False
        Width = 106
      end
      object EmpByCoGridViewlogo_filename: TcxGridDBColumn
        DataBinding.FieldName = 'logo_filename'
        Options.Editing = False
        Width = 109
      end
      object EmpByCoGridViewaddress_city: TcxGridDBColumn
        DataBinding.FieldName = 'address_city'
        Options.Editing = False
      end
      object EmpByCoGridViewbilling_footer: TcxGridDBColumn
        DataBinding.FieldName = 'billing_footer'
        Options.Editing = False
        Width = 105
      end
      object EmpByCoGridViewfloating_holidays_per_year: TcxGridDBColumn
        DataBinding.FieldName = 'floating_holidays_per_year'
        Options.Editing = False
      end
      object EmpByCoGridViewpayroll_company_code: TcxGridDBColumn
        DataBinding.FieldName = 'payroll_company_code'
        Options.Editing = False
      end
      object EmpByCoGridViewreport_to: TcxGridDBColumn
        DataBinding.FieldName = 'report_to'
        Options.Editing = False
      end
      object EmpByCoGridViewreport_to_number: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_number'
        Options.Editing = False
        Width = 101
      end
      object EmpByCoGridViewreport_to_first_name: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_first_name'
        Options.Editing = False
      end
      object EmpByCoGridViewreport_to_last_name: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_last_name'
        Options.Editing = False
        Width = 143
      end
      object EmpByCoGridViewmanager_number: TcxGridDBColumn
        DataBinding.FieldName = 'manager_number'
        Options.Editing = False
      end
      object EmpByCoGridViewmanager_first_name: TcxGridDBColumn
        DataBinding.FieldName = 'manager_first_name'
        Options.Editing = False
      end
      object EmpByCoGridViewmanager_last_name: TcxGridDBColumn
        DataBinding.FieldName = 'manager_last_name'
        Options.Editing = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = EmpByCoGridView
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1015
    Height = 49
    Align = alTop
    TabOrder = 1
    object cbCompany: TDBLookupComboBox
      Left = 96
      Top = 13
      Width = 145
      Height = 21
      KeyField = 'name'
      ListField = 'name'
      ListSource = DMHalSentinel.dsCompanies
      TabOrder = 0
      OnCloseUp = cbCompanyCloseUp
    end
    object btnEmpByCompany: TButton
      Left = 344
      Top = 13
      Width = 121
      Height = 25
      Caption = 'List Employees'
      Enabled = False
      TabOrder = 1
      OnClick = btnEmpbyCompanyClick
    end
    object cbEmpByCompanySearch: TCheckBox
      Left = 518
      Top = 17
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 2
      OnClick = cbEmpByCompanySearchClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 377
    Width = 1015
    Height = 40
    Align = alTop
    TabOrder = 2
    object btnInactiveEmp: TButton
      Left = 48
      Top = 8
      Width = 217
      Height = 25
      Caption = 'List Past 7 Days Inactive Employees'
      TabOrder = 0
      OnClick = btnInactiveEmpClick
    end
    object cbInactiveSearch: TCheckBox
      Left = 518
      Top = 13
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbInactiveSearchClick
    end
    object btnInactiveEmpInfo: TButton
      Left = 328
      Top = 9
      Width = 111
      Height = 25
      Caption = 'Emp Info'
      TabOrder = 2
      OnClick = btnInactiveEmpInfoClick
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 417
    Width = 1015
    Height = 187
    Align = alClient
    TabOrder = 3
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object InactiveGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DMHalSentinel.dsInactiveEmps
      DataController.Filter.MaxValueListCount = 10
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.MRUItemsList = False
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.CellEndEllipsis = True
      OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object InactiveGridViewemp_id: TcxGridDBColumn
        Caption = 'emp id'
        DataBinding.FieldName = 'emp_id'
        Options.Editing = False
      end
      object InactiveGridViewemp_number: TcxGridDBColumn
        Caption = 'emp number'
        DataBinding.FieldName = 'emp_number'
        Options.Editing = False
        Width = 82
      end
      object InactiveGridViewfirst_name: TcxGridDBColumn
        Caption = 'first name'
        DataBinding.FieldName = 'first_name'
        Options.Editing = False
        Width = 93
      end
      object InactiveGridViewlast_name: TcxGridDBColumn
        Caption = 'last name'
        DataBinding.FieldName = 'last_name'
        Options.Editing = False
        Width = 97
      end
      object InactiveGridViewshort_name: TcxGridDBColumn
        Caption = 'short name'
        DataBinding.FieldName = 'short_name'
        Options.Editing = False
        Width = 96
      end
      object InactiveGridViewtype_id: TcxGridDBColumn
        Caption = 'type id'
        DataBinding.FieldName = 'type_id'
        Options.Editing = False
      end
      object InactiveGridViewtimesheet_id: TcxGridDBColumn
        Caption = 'timesheet id'
        DataBinding.FieldName = 'timesheet_id'
        Options.Editing = False
        Width = 85
      end
      object InactiveGridViewcompany_id: TcxGridDBColumn
        Caption = 'company id'
        DataBinding.FieldName = 'company_id'
        Options.Editing = False
        Width = 87
      end
      object InactiveGridViewemp_active: TcxGridDBColumn
        Caption = 'active'
        DataBinding.FieldName = 'emp_active'
        Options.Editing = False
        Width = 42
      end
      object InactiveGridViewaccount_disabled_date: TcxGridDBColumn
        Caption = 'disabled date'
        DataBinding.FieldName = 'account_disabled_date'
        Options.Editing = False
        Width = 147
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = InactiveGridView
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 313
    Width = 1015
    Height = 64
    Align = alTop
    TabOrder = 4
    object btnADInfo: TButton
      AlignWithMargins = True
      Left = 10
      Top = 18
      Width = 95
      Height = 25
      Action = btnAction
      Caption = 'AD Info'
      TabOrder = 0
      OnClick = btnADInfoClick
    end
    object DBGrid1: TDBGrid
      Left = 120
      Top = 6
      Width = 885
      Height = 50
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object ActionList1: TActionList
    Left = 880
    Top = 120
    object btnAction: TAction
      Caption = 'btnAction'
      OnUpdate = btnActionUpdate
    end
  end
end
