unit HalSentinelForm;
// QM-565  B.P.
//User Interface for HalSentinel Test (ParamStr(2) = 'GUI')

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Samples.Spin, Data.Win.ADODB,
  Vcl.OleCtrls, SHDocVw, ActiveX, MSHTML;

type
  THalForm = class(TForm)
    HalGrid: TDBGrid;
    WebBrowser1: TWebBrowser;
    Panel1: TPanel;
    pnlCallCenter: TPanel;
    labldaysback: TLabel;
    DBNavigator1: TDBNavigator;
    SpinEditDateAdd: TSpinEdit;
    btnRefresh: TButton;
    lblCallCenter: TLabel;
    edtCallCenter: TEdit;
    lblThreshhold: TLabel;
    edtThreshhold: TEdit;
    btnCallCenter: TButton;
    SpinEditCount: TSpinEdit;
    lablVersionCount: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCallCenterClick(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    procedure WB_LoadHTML(HTMLCode: string);
  end;

var
  HalForm: THalForm;

implementation

{$R *.dfm}
uses
  HalData;

procedure THalForm.btnCallCenterClick(Sender: TObject);
var
  CallCenter, Threshhold: String;
begin
  CallCenter := Trim(edtCallCenter.Text);
  Threshhold := Trim(edtThreshhold.Text);
  DMHalSentinel.daysback := SpinEditDateAdd.Value;
  if (CallCenter <> '') and (Threshhold <> '')  then
  begin
    DMHalSentinel.Call_Center.Close;
    HalGrid.DataSource := nil;
    DMHalSentinel.Call_Center.CommandText := 'Select cc_code from call_center where active = 1';
    DMHalSentinel.CheckCallCenter(CallCenter, StrtoIntDef(Threshhold, 1200));
  end;
end;

procedure THalForm.btnRefreshClick(Sender: TObject);
begin
  With DMHalSentinel do
  begin
    dsHalModule.DataSet.Close;
    if DMHalSentinel.RunModule = Ticket_Versions then
    begin
      TADODataset(dsHalModule.DataSet).Parameters.ParamByName('startdateoffset').Value :=
        spinEditDateAdd.Value;
      TADODataset(dsHalModule.DataSet).Parameters.ParamByName('count').Value :=
        spinEditCount.Value;
    end else
      TADODataset(dsHalModule.DataSet).Parameters.ParamByName('daysback').Value :=
        spinEditDateAdd.Value;
    dsHalModule.DataSet.Open;
  end;
end;

procedure THalForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   pnlCallCenter.Visible := False;
   btnRefresh.Visible := True;
end;

procedure THalForm.FormShow(Sender: TObject);
begin
  Caption := DMHalSentinel.Module;
  SpinEditDateAdd.Value := DMHalSentinel.daysback;

  HalGrid.DataSource := DMHalSentinel.dsHalModule;
  DBNavigator1.DataSource := DMHalSentinel.dsHalModule;
  //panel for Call Center
  if DMHalSentinel.RunModule = DMHalSentinel.EPR then
  begin
    pnlCallCenter.Visible := False;
    btnRefresh.Visible := False;
    labldaysback.Visible := False;
    spinEditDateAdd.Visible := False;
    DBNavigator1.Visible := False;
  end else
  if DMHalSentinel.RunModule = DMHalSentinel.Call_Center then
  begin
    pnlCallCenter.Visible := True;
    btnRefresh.Visible := False;
  end else
    if DMHalSentinel.RunModule = DMHalSentinel.Attachments then
     labldaysback.Caption := '#days from attach date:'
  else
    if DMHalSentinel.RunModule = DMHalSentinel.Ticket_Versions then
    begin
      LablVersionCount.Visible := True;
      SpinEditCount.Visible := True;
    end;
   { else
   WB_LoadHTML(DMHalSentinel.datasetTableProducer.content)};
end;

//to view HTML- make twebbrowser visible and uncomment calls
procedure THalForm.WB_LoadHTML(HTMLCode: string);
var
  sl: TStringList;
  ms: TMemoryStream;
  Doc: IHTMLDocument2;
begin
  WebBrowser1.Navigate('about:blank');
  while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do
   Application.ProcessMessages;

  if Assigned(WebBrowser1.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := HTMLCode;
        sl.SaveToStream(ms);
        ms.Seek(0, 0);
        (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms));
      finally
        ms.Free;
      end;
    finally
      sl.Free;
      Doc :=  WebBrowser1.Document as IHTMLDocument2;
    end;
  end;
end;

end.
