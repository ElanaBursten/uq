unit TermEmp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DB, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxLabel, cxDBLabel, Data.Win.ADODB;

type
  TTermEmployeeInfoForm = class(TForm)
    lblDamages: TLabel;
    lblTickets: TLabel;
    lblWorkOrders: TLabel;
    OkBtn: TButton;
    WorkOrders: TADODataSet;
    dbLWorkOrders: TcxDBLabel;
    dsWorkOrders: TDataSource;
    Tickets: TADODataSet;
    dsTickets: TDataSource;
    dbLblTickets: TcxDBLabel;
    dbLblDamages: TcxDBLabel;
    Damages: TADODataSet;
    dsDamages: TDataSource;
    Areas: TADODataSet;
    dsAreas: TDataSource;
    lblArea: TLabel;
    dbLblArea: TcxDBLabel;
    qryShortName: TADOQuery;
  private
    { Private declarations }
    EmpID: Integer;
  public
    { Public declarations }
  protected
    procedure GetTerminatedEmpData;
  end;

procedure DisplayTermEmpInfo(const empid: Integer);

implementation

uses HalData;

{$R *.dfm}

procedure DisplayTermEmpInfo(const empid: Integer);
var
  Dialog: TTermEmployeeInfoForm;
begin
  Dialog := TTermEmployeeInfoForm.Create(nil);
  try
    Dialog.EmpID := empid;
    Dialog.GetTerminatedEmpData;
    Dialog.ShowModal;
  finally
    Dialog.WorkOrders.Close;
    Dialog.Tickets.Close;
    Dialog.Damages.Close;
    Dialog.Areas.Close;
    Dialog.qryShortName.Close; //qm-374  sr
    FreeAndNil(Dialog);
  end;
end;

{ TTermEmployeeInfoForm }

procedure TTermEmployeeInfoForm.GetTerminatedEmpData;
begin

  WorkOrders.Parameters.ParamByName('locator_id').Value := EmpID;
  Tickets.Parameters.ParamByName('locator_id').Value := EmpID;
  Damages.Parameters.ParamByName('locator_id').Value := EmpID;
  Areas.Parameters.ParamByName('locator_id').Value := EmpID;
  qryShortName.Parameters.ParamByName('empId').Value := EmpID; //qm-374  sr
  WorkOrders.Open;
  Tickets.Open;
  Damages.Open;
  Areas.Open;
  qryShortName.Open; //qm-374  sr
  self.Caption := 'Workload for: '+qryShortName.FieldByName('short_name').AsString; //qm-374  sr
end;

end.
