program HalSentinel;
// QM-565  B.P.
uses
  Vcl.Forms,
  SysUtils,
  Dialogs,
  GlobalSU in 'GlobalSU.pas',
  HalSentinelForm in 'HalSentinelForm.pas' {HalForm},
  HALData in 'HALData.pas' {DMHalSentinel: TDataModule},
  uHALLogger in 'uHALLogger.pas',
  ActiveUser in 'ActiveUser.pas' {ActiveUsersForm},
  TermEmp in 'TermEmp.pas';

  {$R '..\..\QMIcon.res'}
  {$R 'QMVersion.res' '..\..\QMVersion.rc'}


 var
   MyInstanceName: string;

begin
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
   // ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    if ParamCount < 1 then
    begin
      LogResult.LogType := ltWarning;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'first Parameter: Module Name' +#13#10 +
      'Second parameter (optional): days back, (overrides Hal table) or ' + #13#10 +
      'Second parameter is ''GUI''(for testing}';
      LogResult.MethodName := 'HalSentinel program';
      WriteLog(LogResult);
      Application.Terminate;
    end;
    Application.MainFormOnTaskbar := True;
    Application.ShowMainForm := (ParamStr(2) = 'GUI');
    try
     Application.CreateForm(TDMHalSentinel, DMHalSentinel);
  //process Hal modules and emails.
     DMHalSentinel.Run;
     If (ParamStr(2) = 'GUI') then
     begin
       if ParamStr(1) = 'active_users' then
         Application.CreateForm(TActiveUsersForm, ActiveUsersForm) else
       Application.CreateForm(THalForm, HalForm);
       Application.Run;
     end;

    except
      On E:Exception do
      begin
        If ParamStr(2) = 'GUI' then
          showmessage(E.Message);
      end;
    end;
  end else
    Application.Terminate;
end.
