object TermEmployeeInfoForm: TTermEmployeeInfoForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Terminated Employee'
  ClientHeight = 241
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblArea: TLabel
    Left = 37
    Top = 132
    Width = 78
    Height = 13
    Caption = 'Assigned Areas:'
  end
  object lblWorkOrders: TLabel
    Left = 37
    Top = 23
    Width = 94
    Height = 13
    Caption = 'Open Work Orders:'
  end
  object lblTickets: TLabel
    Left = 37
    Top = 58
    Width = 66
    Height = 13
    Caption = 'Open Tickets:'
  end
  object lblDamages: TLabel
    Left = 37
    Top = 94
    Width = 77
    Height = 13
    Caption = 'Open Damages:'
  end
  object OkBtn: TButton
    Left = 86
    Top = 173
    Width = 75
    Height = 29
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object dbLblDamages: TcxDBLabel
    Left = 146
    Top = 92
    DataBinding.DataField = 'COLUMN1'
    DataBinding.DataSource = dsDamages
    ParentColor = False
    Properties.Alignment.Horz = taCenter
    Properties.ShadowedColor = clBtnText
    Style.Color = clGradientInactiveCaption
    Height = 21
    Width = 54
    AnchorX = 173
  end
  object dbLblArea: TcxDBLabel
    Left = 146
    Top = 130
    DataBinding.DataField = 'COLUMN1'
    DataBinding.DataSource = dsAreas
    ParentColor = False
    Properties.Alignment.Horz = taCenter
    Properties.ShadowedColor = clBtnText
    Style.Color = clGradientInactiveCaption
    Height = 21
    Width = 54
    AnchorX = 173
  end
  object dbLblTickets: TcxDBLabel
    Left = 146
    Top = 56
    DataBinding.DataField = 'COLUMN1'
    DataBinding.DataSource = dsTickets
    ParentColor = False
    Properties.Alignment.Horz = taCenter
    Properties.ShadowedColor = clInactiveCaption
    Style.Color = clGradientInactiveCaption
    Height = 21
    Width = 54
    AnchorX = 173
  end
  object dbLWorkOrders: TcxDBLabel
    Left = 146
    Top = 21
    DataBinding.DataField = 'COLUMN1'
    DataBinding.DataSource = dsWorkOrders
    ParentColor = False
    Properties.Alignment.Horz = taCenter
    Properties.ShadowedColor = clInactiveCaption
    Style.Color = clGradientInactiveCaption
    Height = 21
    Width = 54
    AnchorX = 173
  end
  object WorkOrders: TADODataSet
    Connection = DMHalSentinel.ADOConn
    CursorType = ctStatic
    CommandText = 
      '  select count(*)  from work_order wo'#13#10'  where wo.assigned_to_id' +
      ' = :locator_id'#13#10'    and wo.closed = 0'#13#10'    and wo.active = 1'
    Parameters = <
      item
        Name = 'locator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 233
  end
  object dsWorkOrders: TDataSource
    DataSet = WorkOrders
    Left = 303
  end
  object Tickets: TADODataSet
    Connection = DMHalSentinel.ADOConn
    CursorType = ctStatic
    CommandText = 
      'select count(*) from ticket'#13#10'where ticket_id in ('#13#10'   select tic' +
      'ket_id from locate'#13#10'    where assigned_to = :locator_id'#13#10'    and' +
      ' active = 1'#13#10'  and closed = 0)'
    Parameters = <
      item
        Name = 'locator_id'
        DataType = ftWideString
        Size = 5
        Value = Null
      end>
    Left = 229
    Top = 49
  end
  object dsTickets: TDataSource
    DataSet = Tickets
    Left = 294
    Top = 53
  end
  object Damages: TADODataSet
    Connection = DMHalSentinel.ADOConn
    CursorType = ctStatic
    CommandText = 
      'select count(*) from damage d where'#13#10'd.investigator_id = :locato' +
      'r_id'#13#10'    and d.damage_type in ('#39'INCOMING'#39', '#39'PENDING'#39')'#13#10'    and ' +
      'd.active = 1'
    Parameters = <
      item
        Name = 'locator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 233
    Top = 101
  end
  object dsDamages: TDataSource
    DataSet = Damages
    Left = 295
    Top = 102
  end
  object Areas: TADODataSet
    Connection = DMHalSentinel.ADOConn
    CursorType = ctStatic
    Filtered = True
    CommandText = 'select count(*) from area where locator_id = :locator_id'
    FieldDefs = <
      item
        Name = 'COLUMN1'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end>
    Parameters = <
      item
        Name = 'locator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    StoreDefs = True
    Left = 232
    Top = 154
  end
  object dsAreas: TDataSource
    DataSet = Areas
    Left = 294
    Top = 154
  end
  object qryShortName: TADOQuery
    Connection = DMHalSentinel.ADOConn
    Parameters = <
      item
        Name = 'empID'
        DataType = ftInteger
        Value = Null
      end>
    SQL.Strings = (
      ' select short_name'
      ' from [dbo].[employee]'
      ' where emp_id = :empID')
    Left = 232
    Top = 208
  end
end
