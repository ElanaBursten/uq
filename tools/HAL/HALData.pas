unit HALData;
 // QM-565  B.P.
 //Data Module for HAL module processing
interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, IniFiles, Dialogs, odExceptions, odMiscUtils,
  Web.HTTPApp, Web.DBWeb, mshtml, IdBaseComponent, IdComponent, IdServerIOHandler, IdSSL, IdSSLOpenSSL,
  IdSSLOpenSSLHeaders, IdSMTP, IdMessage, IdText,IdExplicitTLSClientServerBase, StrUtils, Contnrs;

type
  TEmployee = Class
    emp_id: Integer;
    emp_number: String;
    first_name: String;
    last_name: String;
    ad_username: String;
    active: Boolean;
    sam_name: String;
    short_name: String;
    adp_login: String;
    company_id: Integer;
    company_name: String;
    email: String;
    phone: String;
    workerstatusdescription: String;
    reporttoworkerid: Integer;
    center: String;
    e_type: String;
  end;

  TADInfo = Class
    isActive: Boolean;
    lastLogon: String;
    lastLogonTimeStamp: String;
    empID: Integer;
    returnCode: Integer;
    name: String;
    company: String;
  end;


  TDMHalSentinel = class(TDataModule)
    ADOConn: TADOConnection;
    Attachments: TADODataSet;
    HalConfig: TADODataSet;
    Assigned_Tickets: TADODataSet;
    dsHalModule: TDataSource;
    DataSetTableProducer: TDataSetTableProducer;
    Attachmentsshort_name: TStringField;
    AttachmentsPhone: TStringField;
    AttachmentsPending: TIntegerField;
    Assigned_Ticketsshort_name: TStringField;
    Assigned_Ticketsticket_number: TStringField;
    Assigned_Ticketsclient_code: TStringField;
    Assigned_Ticketsstatus: TStringField;
    Assigned_Ticketsdue_date: TDateTimeField;
    Assigned_Ticketsticket_format: TStringField;
    Call_Center: TADODataSet;
    ADOConnADS: TADOConnection;
    dsReportTo: TDataSource;
    dsMDRInfo: TDataSource;
    dsADActiveUsers: TDataSource;
    dsEmpbyCompany: TDataSource;
    ReportTo: TADODataSet;
    ReportTofirstname: TStringField;
    ReportTolastname: TStringField;
    MDRInfo: TADODataSet;
    MDRInfoWorkerstatusDescription: TStringField;
    MDRInfoReportToWorkerId: TLargeintField;
    ADActiveUsers: TADODataSet;
    EmpbyCompany: TADODataSet;
    Companies: TADODataSet;
    TicketCt: TADODataSet;
    DamageCt: TADODataSet;
    WOCt: TADODataSet;
    InactiveEmps: TADODataSet;
    AreaCt: TADODataSet;
    dsCompanies: TDataSource;
    dsInactiveEmps: TDataSource;
    EPR: TADODataSet;
    Blank_Locators: TADODataSet;
    Blank_Locatorslocate_id: TAutoIncField;
    Blank_Locatorsticket_id: TIntegerField;
    Blank_Locatorsclient_code: TStringField;
    Blank_Locatorsstatus: TStringField;
    Blank_Locatorsassigned_to: TIntegerField;
    Blank_Locatorsmodified_date: TDateTimeField;
    Ticket_Versions: TADODataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure ReadINI;
    procedure SendEmails;
    procedure SendEmailWithIniSettings(IniFileName, IniSection, EmailTo,
      Subject, Body: string);
    procedure SendEmail(const ToAddr, FromAddr, Subject, Body, SMTPHost: string;
      SMTPUser, SMTPPassword: string; SMTPPort: Integer; UseSSL,
      SendHTML: Boolean);
    procedure ConfigureSecureSMTP(SMTP: TIdSMTP; const Username,
      Password: string);
    function EnDeCrypt(const Value: String): String;
    { Private declarations }
  public
    { Public declarations }
    RunModule:TADODataSet;
    Module: String;
    daysback: Integer;
    tvcount: Integer;
    procedure Run;
    function CheckCallCenter(callcenter: string; threshhold: integer): boolean;
  private
    callcenter: String;
    threshhold: Integer;
    EmailTo: String;
    IniFileName: String;
    ConnStr: String;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    ActiveUserHTML: String;
    FireCrackerDir: String;
    IsFileModule: Boolean;
    function ConnectDB: Boolean;
    procedure GetCompanies(CompanyList: TStringList);
    function GetEmployeeList(company: String; EmployeeList:TObjectList): Boolean;
    function RunCompany(listToCheck: TObjectList; company: String): String;
    procedure qryByCompany;
    procedure CheckIsActiveEmployee(Emp: TEmployee; ADInfo: TADInfo);
  //  procedure getMDRInfo(emp: TEmployee);
   // function GetReportTo(reportToID: String): String;
    procedure notifyInactive;
    procedure GetInactiveList(InactiveList: TObjectList);
    function ComposeEmail(Count: Integer; empType, empNumber, shortname,
      first, last, center: String; ticketCt, areaCt, woCt,
      DCt: Integer): String;
    function GetAreaCt(EmpID: Integer): Integer;
    function GetTicketCt(EmpID: Integer): Integer;
    function GetWOCt(EmpID: Integer): Integer;
    function GetDamageCt(EmpID: Integer): Integer;
  end;

var
  DMHalSentinel: TDMHalSentinel;

implementation


{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDMHalSentinel }
uses uHalLogger, HalSentinelForm;

procedure TDMHalSentinel.DataModuleCreate(Sender: TObject);
begin
  try
    ReadIni;
    ConnectDB;
  except
    if ParamStr(1) = 'GUI' then
     raise;
  end;
end;

procedure TDMHalSentinel.ReadINI;
var
  IniFile: TIniFile;
begin
  try
    try
     IniFile := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
     IniFilename := IniFile.FileName;


     aServer := IniFile.ReadString('Database', 'Server', '');
     aDatabase := IniFile.ReadString('Database', 'DB', 'QM');
     ausername := IniFile.ReadString('Database', 'UserName', '');
     apassword := IniFile.ReadString('Database', 'Password', '');
     aTrusted  := IniFile.ReadString('Database', 'Trusted', '1');

     FireCrackerDir := IniFile.ReadString('FireCracker', 'FireCrackerCdn', '1');

     LogPath :=  IniFile.ReadString('Paths', 'LogPath', 'C:\QM\Logs');
     ForceDirectories(LogPath);
    except
      if ParamStr(1) <> 'GUI' then
      begin
        LogResult.LogType := ltError;
        LogResult.Status := 'Process Failed';
        LogResult.ExcepMsg := 'Failed to read from Ini file';
        LogResult.MethodName := 'DMHalSentinel.Run';
        WriteLog(LogResult);
      end;
      Raise;
    end;
  finally
    IniFile.Free;
  end;
end;

procedure TDMHalSentinel.Run;
var
  AMessage: String;
begin
  LogResult.MethodName := 'DMHalSentinel.Run';
  LogResult.Status := '******************' + ParamStr(1) + ' Start Up ******************';
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  try
    //retrieve Hal module from HAL Table (specified in paramstr(1))
    HalConfig.Parameters.ParamByName('hmodule').Value := ParamStr(1);
    HalConfig.Open;
    if HalConfig.EOF then
      raise Exception.Create('Module: ' + ParamStr(1) + ' not found in Hal table');

    emailto := Trim(HalConfig.FieldByName('emails').AsString);
    daysback := HalConfig.FieldByName('daysback').AsInteger;
    IsFileModule := False;

    if ParamStr(2) <> 'GUI' then
    begin
      if ParamStr(1) = 'call_center' then
      begin
        if ParamCount < 3 then
          raise Exception.Create('Call Center: Missing parameters: 1st parameter=module,  ' +
          '2nd parameter=call center, 3rd parameter=threshhold, 4th parameter(optional)=daysback');
        callcenter := ParamStr(2);
        threshhold := StrToIntDef(ParamStr(3),1200);
        if ParamStr(4) <> '' then
          daysback := StrToIntDef(ParamStr(4),7);
      end else
      if (ParamStr(1) = 'ticket_versions') then
      begin
       if ParamCount < 3 then
       raise Exception.Create('Ticket Version: Missing parameters: 1st parameter=module,  ' +
       '2nd parameter=minimum count, 3rd parameter=days for ticket versions');
       tvcount := StrToIntDef(ParamStr(2),1);
       daysback := StrToIntDef(ParamStr(2),5);
      end else
      if (ParamStr(1) <> 'call_center') and (ParamStr(1) <> 'active_users')
        and (ParamStr(1) <> 'epr_response') and (ParamStr(1) <> 'firecracker') then
        if ParamStr(2) <> '' then
          daysback := StrToIntDef(ParamStr(2),7);
    end;

    RunModule := nil;
    if HalConfig.FieldByName('hal_module').AsString = 'missing_attachments' then
    begin
      Module := 'Attachments';
      RunModule := Attachments;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'assigned_tickets' then
    begin
      Module := 'Assigned Tickets';
      RunModule := Assigned_Tickets;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'call_center' then
    begin
      dsHalModule.DataSet := nil;
      Module := 'Call Center';
      RunModule := Call_Center;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'epr_response' then
    begin
      Module := 'EPR';
      RunModule := EPR;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'blank_locators' then
    begin
      Module := 'Blank Locators';
      RunModule := Blank_Locators;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'ticket_versions' then
    begin
      Module := 'Ticket Versions';
      RunModule := Ticket_Versions;
    end else
    if HalConfig.FieldByName('hal_module').AsString = 'active_users' then
    begin
      ADOConnADS.Open();
      Module := 'Active Users';
      RunModule := ADActiveUsers;
    end else
    if (HalConfig.FieldByName('hal_module').AsString = 'firecracker') or
       (HalConfig.FieldByName('hal_module').AsString = 'tkattachcv') or
       (HalConfig.FieldByName('hal_module').AsString = 'parser_log_errors')
    then IsFileModule := True;

    HalConfig.Close;

    if not IsFileModule then
    begin
      if RunModule = nil then
        raise Exception.Create('Module ' + ParamStr(1) + ' not available');

      if (RunModule <> Call_Center) and (RunModule <> ADActiveUsers) and (RunModule <> EPR) then
      begin
        if RunModule = Ticket_Versions then
        begin
          RunModule.Parameters.ParamByName('startdateoffset').Value := daysback;
          RunModule.Parameters.ParamByName('count').Value := tvcount;
        end else
          RunModule.Parameters.ParamByName('daysback').Value := daysback;
        RunModule.Open;
      end else
      begin
        If ParamStr(2) <> 'GUI' then
        begin
          if (RunModule = Call_Center) then
          begin
            If not CheckCallCenter(callcenter,threshhold) then exit
          end
          else
          if (RunModule = ADActiveUsers) then
          begin
            qrybyCompany;
            notifyInactive;
          end;
        end;
      end;
    end;

    //ParamStr(2) specifies 'GUI' for the test form.
    if ParamStr(2) = 'GUI' then
    begin
      if (RunModule <> Call_Center) and (RunModule <> ADActiveUsers) then
      begin
        datasetTableProducer.DataSet := RunModule;
        dsHalModule.DataSet := RunModule;
      end;
      Exit;
    end;
    //Send out emails to list in Hal table email list
    If RunModule <> ADActiveUsers then
      SendEmails;
    RunModule.Close;
    LogResult.MethodName := 'DMHalSentinel.Run';
    LogResult.Status := '******************' + ParamStr(1) + ' process complete ******************';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  except
    On E:Exception do
    begin
      HalConfig.Close;
      if ParamStr(2) = 'GUI' then
         raise else
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'DMHalSentinel.Run';
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
      end;
    end;
  end;
end;

procedure TDMHalSentinel.NotifyInactive;
var
  InactiveList: TObjectList;
  I: Integer;
  AreaCt, TicketCt, WOct, DCt: Integer;
  Emp: TEmployee;
  InactiveHTML: String;
begin
 try
   try
     LogResult.MethodName := 'DMHalSentinel.NotifyInactive';
     LogResult.Status := 'Starting checkActiveUsers notify inactive...';
     LogResult.LogType := ltInfo;
     WriteLog(LogResult);

     InactiveList := TObjectList.Create;

     GetInactiveList(InactiveList);

     LogResult.MethodName := 'DMHalSentinel.NotifyInactive';
     LogResult.Status := 'Found ' + InactiveList.Count.ToString + ' past 7 day inactives.';
     LogResult.LogType := ltInfo;
     WriteLog(LogResult);

     for I := 0 to InactiveList.Count-1 do
     begin
       Emp := TEmployee(InactiveList[I]);
       AreaCt := GetAreaCt(Emp.emp_id);
       TicketCt := GetTicketCt(Emp.emp_id);
       WOCt := GetWOCt(Emp.emp_id);
       DCt := GetDamageCt(Emp.emp_id);
       InactiveHTML := InactiveHTML +
         ComposeEmail(InactiveList.Count, Emp.e_type, Emp.emp_number,
         Emp.short_name, Emp.first_name, Emp.last_name, Emp.center, TicketCt,
         AreaCt, WOCt, DCt);
     end;
     SendEmails;
   finally
     InactiveList.Free;
   end;
 except
   on E:Exception do
   begin
     LogResult.LogType := ltError;
     LogResult.Status := 'Process Failed';
     LogResult.ExcepMsg := 'Error In GetInactiveList: ' + E.Message;
     LogResult.MethodName := 'DMHalSentinel.NotifyInactive';
     WriteLog(LogResult);
   end;
 end;
end;

Function TDMHalSentinel.GetAreaCt(EmpID: Integer): Integer;
var
  AreaCtSQL: String;
begin
  try
    try
      AreaCtSQL := 'Select count(area_id) As count FROM area where locator_id = ' + EmpID.ToString;
      AreaCt.CommandText := AreaCtSQL;
      AreaCt.Open;
      Result := AreaCt.FieldByName('count').AsInteger;
    finally
      AreaCt.Close;
    end;
  except
    On E:Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'Error In GetAreaCt, EmpID: ' + InttoStr(Empid) + E.Message;
      LogResult.MethodName := 'DMHalSentinel.GetAreaCt';
      WriteLog(LogResult);
      Result := -1;
    raise;
    end;
  end;
end;

function TDMHalSentinel.GetTicketCt(EmpID: Integer): Integer;
var
  TicketCtSQL: String;
begin
  try
    try
      TicketCtSQL :=
       'select count(*) As count from ticket where ticket_id in ( ' +
       'select ticket_id from locate where assigned_to = ' + inttostr(empid) + ' ' +
       'and active = 1 and closed = 0)';
      TicketCt.CommandText := TicketCtSQL;
      TicketCt.Open;
      Result := TicketCt.FieldByName('count').AsInteger;
    finally
      TicketCt.Close;
    end;
  except
    On E:Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'Error In GetTicketCt, EmpID: ' + InttoStr(Empid) + E.Message;
      LogResult.MethodName := 'DMHalSentinel.GetTicketCt';
      WriteLog(LogResult);
    end;
  end;
end;

function TDMHalSentinel.GetDamageCt(EmpID: Integer): Integer;
var
  DamageCtSQL: String;
begin
  try
    try
      DamageCtSQL :=
       'Select count(damage_id) As count from damage where active = 1 ' +
       'and damage_type in (''INCOMING'', ''PENDING'') and ' +
       'investigator_id = ' + EmpID.ToString;

      DamageCt.CommandText := DamageCtSQL;
      DamageCt.Open;
      Result := DamageCt.FieldByName('count').AsInteger;
    finally
       DamageCt.Close;
    end;
  except
    On E:Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'Error In GetDamageCt, EmpID: ' + InttoStr(Empid) + E.Message;
      LogResult.MethodName := 'DMHalSentinel.GetDamageCt';
      WriteLog(LogResult);
    end;
  end;
end;

function TDMHalSentinel.GetWOCt(EmpID: Integer): Integer;
var
  WOCtSQL: String;
begin
  try
    try
      WOCtSQL := 'Select COUNT(wo_id) As count from work_order where ' +
      'assigned_to_id = ' + EmpID.ToString + ' and closed = 0 and active = 1';
      WOCt.CommandText := WOCtSQL;
      WOCt.Open;
      Result := WOCt.FieldByName('count').AsInteger;
    finally
       WOCt.Close;
    end;
  except
    On E:Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'Error In GetWOCt, EmpID: ' + InttoStr(Empid) + E.Message;
      LogResult.MethodName := 'DMHalSentinel.GetWOCt';
      WriteLog(LogResult);
    end;
  end;
end;

procedure TDMHalSentinel.GetInactiveList(InactiveList: TObjectList);
var
  InactiveEmpSQL: String;
  Employee: TEmployee;
begin
  try
    InactiveEmpSQL := 'select e.emp_id, e.emp_number, e.first_name, e.last_name, e.short_name, ' +
     'e.type_id, e.timesheet_id, e.active as emp_active, e.company_id, u.account_disabled_date from users u ' +
     'inner join employee e on u.emp_id = e.emp_id where DATEDIFF(D,account_disabled_date, GETDATE()) >= 7 ' +
     'and e.active = 1 order by short_name';
    InactiveEmps.CommandText := InactiveEmpSQL;
    InactiveEmps.Open;
    while not InactiveEmps.EOF do
    begin
      Employee := TEmployee.Create;

      With Employee, InactiveEmps do
      begin
        emp_id := fieldbyname('emp_id').AsInteger;
        emp_number := fieldbyname('emp_number').AsString;
        short_name := fieldbyname('short_name').AsString;
        first_name := fieldbyname('first_name').AsString;
        last_name := fieldbyname('last_name').AsString;
        company_id := fieldbyname('company_id').AsInteger;
        center := fieldbyname('timesheet_id').AsString;
        e_type := fieldbyname('type_id').AsString;
      end;
      InactiveList.Add(Employee);
      InactiveEmps.Next;
    end;
  finally
    InactiveEmps.Close;
  end;
end;

function TDMHalSentinel.ComposeEmail(Count: Integer; empType: String; empNumber: String; shortname: String; first: String;
 last: String; center: String; ticketCt: Integer; areaCt: Integer; woCt: Integer; DCt: Integer): String;
begin
  If empType = '1342' then
    result := result + '<br><font color=''blue''>Employee number ' + empNumber + ' ' +
    shortname + ' (' + first + ',' + last + '), center: ' + center +
    ' still active after 7 days wait and is leave of absence (LOA)</font><br>'
  else
    result := result + '<br>Employee number ' + empNumber + ' ' + shortname +
    ' (' + first + ',' + last + '), center: ' + center + ' still active after 7 days wait<br>';

  if areaCt > 0 then
    result := result + '<font color=''red''>Employee number ' + empNumber + ' ' + shortname +
    ' (' + first + ',' + last + '), center: ' + center + ' still has areas assigned</font><br>';

   if ticketCt > 0 then
     result := result + '<font color=''red''>Employee number ' + empNumber + ' ' + shortname +
     ' (' + first + ',' + last + '), center: ' + center + ' still has tickets assigned</font><br>';

   if woCt > 0 then
     result := result + '<font color=''red''>Employee number ' + empNumber + ' ' + shortname +
     ' (' + first + ',' + last + '), center: ' + center + ' still has work orders assigned</font><br>';

   if DCt > 0 then
     result := result + '<font color=''red''>Employee number ' + empNumber + ' ' + shortname +
     ' (' + first + ',' + last + '), center: ' + center + 'still has PENDING damages assigned</font><br>';
end;


procedure TDMHalSentinel.ConfigureSecureSMTP(SMTP: TIdSMTP; const Username, Password: string);
var
  I: Integer;
begin
  Assert(Assigned(SMTP), 'SMTP connection is undefined.');
  Assert(SMTP.Connected, 'Connect SMTP before configuring security.');
  for I := 0 to SMTP.Capabilities.Count-1 do begin
    // other types of authentication may also be used if available
    if StrContainsText('PLAIN', SMTP.Capabilities[i]) then begin
      SMTP.AuthType := satDefault;
      Break;
    end;
  end;
  SMTP.Username := Username;
  SMTP.Password := Password;
  if not SMTP.Authenticate then
    raise Exception.Create('Unable to authenticate SMTP user.');
end;

//Ports for sending TSL emails are 587 or 465 depending on what the outgoing server allows.
{example ini for rackspace:
[Email]
SMTPServer=secure.emailsrvr.com
SMTPPort=587
SendHTML=1
UseSSL=1
SMTPUser=emailtkts.2051@tickets.utiliquest.com
SMTPPassword=emailtkts2051
}

procedure TDMHalSentinel.SendEmail(const ToAddr, FromAddr, Subject, Body, SMTPHost: string;
   SMTPUser: string; SMTPPassword: string; SMTPPort: Integer;
   UseSSL: Boolean; SendHTML: Boolean);
var
  NotifyMsg: TIdMessage;
  SMTP: TIdSMTP;
  SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
  i: Integer;
  wsHTML, wsPlain: TIdText;
begin
  SMTP := TIdSMTP.Create(nil);
  NotifyMsg := TIdMessage.Create(nil);
  try
    SMTP.Host := SMTPHost;
    SMTP.ReadTimeout := 30000;
    SMTP.ConnectTimeout := 15000;
    SMTP.Port := SMTPPort;

    if UseSSL then begin
      SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create;
      //uses all SSL versions
      SSLHandler.SSLOptions.SSLVersions := [sslvSSLv2, sslvSSLv23, sslvSSLv3, sslvTLSv1,sslvTLSv1_1,sslvTLSv1_2];
      SSLHandler.SSLOptions.Mode := sslmUnassigned;
      SMTP.AuthType := satDefault;
      SMTP.IOHandler := SSLHandler;
      SMTP.UseTLS := utUseRequireTLS;
    end;

    if SendHTML then begin
      NotifyMsg.ContentType := 'multipart/mixed';

      wsHTML := TIdText.Create(NotifyMsg.MessageParts);
      wsHTML.ContentType := 'text/html';
      wsHTML.Body.Text := '<pre>' + Body + '</pre>';

      wsPlain := TIdText.Create(NotifyMsg.MessageParts);
      wsPlain.ContentType := 'text/plain';
      wsPlain.Body.Text := '';
    end else begin
      NotifyMsg.Body.Text := Body;
    end;

    NotifyMsg.Subject := Subject;
    NotifyMsg.From.Text := FromAddr;
    NotifyMsg.Recipients.EMailAddresses := ToAddr;


    SMTP.Connect;
    if NotEmpty(SMTPUser) and NotEmpty(SMTPPassword) then
      ConfigureSecureSMTP(SMTP, SMTPUser, SMTPPassword);

    SMTP.Send(NotifyMsg);
    SMTP.Disconnect;
  finally
    FreeAndNil(SMTP);
    FreeAndNil(NotifyMsg);
    if UseSSL then
      FreeAndNil(SSLHandler);
  end;
end;

//callcenter taken from 2nd program parameter; threshold is 3rd program parameter;
// (daysback program parameter is optional and will override daysback in the Hal table)
function TDMHalSentinel.CheckCallCenter(callcenter: string; threshhold:integer): boolean;
var
  Centers: Tstringlist;
  QueryStr: String;
  whichFieldName: String;
  ccExists: Boolean;
  AMessage: String;
begin
   Result := true;
   try
	  Centers := TStringlist.Create;
	  ccExists := False;
	  Call_Center.Open;
	  While not Call_Center.EOF do
	  begin
	    If Call_Center.fieldbyname('cc_code').AsString = callcenter then CCExists := true;
        Centers.add(Call_Center.fieldbyname('cc_code').AsString);
	  	Call_Center.next;
	  end;
	  Call_Center.Close;

    If not CCExists then
    begin
      Call_Center.Close;
      AMessage := WrapText('Invalid or inactive Call Center provided in the command line arguments, ' +
        'the list to choose from:  ' + #13#10 + #13#10 + Centers.CommaText, #13#10, [','], 60);
      if ParamStr(2) = 'GUI' then
      begin
        MessageDlg(AMessage, mtError, [mbOK], 0);
        Exit;
      end else
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'CheckCallCenter';
        LogResult.ExcepMsg := 'Invalid or inactive Call Center ' + Paramstr(2) + ' provided in the command line arguments';
        WriteLog(LogResult);
        Result := false;
        Exit;
      end;
    end;
  except
    On E:Exception do
    begin
      raise Exception.Create('Unable to find a list of call centers, reason:' +  E.Message)
    end;
  end;

  //sanity check for a more than a day
  If threshhold > 1440 then
    raise Exception.Create('Greater than 1,440 minutes in argument (One Day), exiting');

  //Currently NCA1 is hosed up. See JIRA QMAN-3234 for the reason why.  What we have to do as one
  //of our only options for now is to look at the ticket_format field instead of the source field
  //in the ticket table if NCA1.
  //This will work for now, but do note that it limits our ability somewhat to monitor Northern CA
  //because other call centers like NCA2 (AT&T) and NCA4 (CVIN) update NCA1.  More work will be
  //needed later on to look at other aspects of NCA1 to truly monitor that center.

  If callcenter = 'NCA1' then
    whichFieldName := 'ticket_format'
  else whichFieldName := 'source';

  QueryStr := 'DECLARE @hal TABLE (lastTicket_version_id int, callCenter varchar(12)); '+
    'INSERT INTO @hal (lastTicket_version_id, callCenter) ' +
    'select max(ticket_version_id), ' + whichFieldName + ' ' +
    'from ticket_version ' +
    'where ' + whichFieldName + ' = (' + QuotedStr(callcenter) + ') ' +
    'and transmit_date between getdate()-' + inttostr(daysback) + ' and getdate()-1 ' +
    'group by ' + whichFieldName + ' ' +
    'select ticket_version.source, call_Center.cc_name, lastTicket_version_id,' +
    'ticket_version.ticket_number, ticket_version.arrival_date, ticket_version.ticket_type,' +
    'DateDiff(minute,ticket_version.arrival_date,getDate()) as delta ' +
    'from @hal h ' +
    'inner join ticket_version on ticket_version.ticket_version_id = h.lastTicket_version_id ' +
    'left join call_center on call_Center.cc_code = ticket_version.' + whichFieldName ;

  dsHalModule.DataSet := RunModule;
  if ParamStr(2) = 'GUI' then
   HalForm.HalGrid.DataSource := dsHalModule;
  DataSetTableProducer.DataSet := RunModule;
  Call_Center.CommandText := QueryStr;
  Call_Center.Open;
  //HalForm.WB_LoadHTML(DMHalSentinel.datasetTableProducer.content);
end;

//emailTo parameter- uses email entries of HAL table: If EmaiTo key of Ini file exists, this is added to the list.
procedure TDMHalSentinel.SendEmailWithIniSettings(IniFileName, IniSection, EmailTo, Subject, Body: string);
var
  Host, EmailFrom: string;
  IniEmailTo: string;
  Ini: TIniFile;
  Ignore: Boolean;
  UseSSL: Boolean;
  SMTPUser: string;
  SMTPPassword: string;
  SMTPPort: Integer;
  SendHTML: Boolean;
begin
  Ini := TIniFile.Create(IniFileName);
  try
    IniEmailTo := Ini.ReadString(IniSection, 'To', '');

    // Semicolon seperate the addresses if both are present
    if (EmailTo <> '') and (IniEmailTo <> '') then
      EmailTo := EmailTo + ';';

    EmailTo := EmailTo + IniEmailTo;

    Host := Ini.ReadString(IniSection, 'SMTPServer', '');
    EmailFrom := Ini.ReadString(IniSection, 'From', '');
    SMTPUser := Ini.ReadString(IniSection, 'SMTPUser', '');
    SMTPPassword := Ini.ReadString(IniSection, 'SMTPPassword', '');
    SMTPPort := Ini.ReadInteger(IniSection, 'SMTPPort', 25);
    UseSSL := Ini.ReadBool(IniSection, 'UseSSL', False);
    SendHTML := Ini.ReadBool(IniSection, 'SendHTML', False);
    // Ignore is for development/test machines where we don't want to generate emails
    Ignore := Ini.ReadBool(IniSection, 'Ignore', False);
  finally
    FreeAndNil(Ini);
  end;

  if Ignore then
    Exit;

  // If not configured, complain
  if (EmailTo = '') or (Host = '') or (EmailFrom = '') then
    raise EODConfigError.Create('Could not load To/From/SMTPServer email configuration data from ' + IniFileName + ' / [' + IniSection + ']');

  //Indy may use incorrect SSL dll's- prevented by keeping libeay.dll and ssleay.dll in the .exe directory.
  //IdOpenSSLSetLibPath('C:\Program Files (x86)\Embarcadero\Studio\19.0\bin\subversion');

  SendEmail(Emailto, EmailFrom, Subject, Body, Host, SMTPUser, SMTPPassword, SMTPPort, UseSSL, SendHTML);
end;

procedure TDMHalSentinel.SendEmails;
var
  HTMLStr: String;
begin
  //format HTML for emails.
  DataSetTableProducer.DataSet := RunModule;
   if RunModule = assigned_tickets then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 5;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;
  if RunModule = Attachments then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 0;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;
  if RunModule = EPR then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 0;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;
  if RunModule = Call_Center then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 0;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;
  if RunModule = Blank_Locators then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 0;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;
  if RunModule = Ticket_Versions then
  begin
    DataSetTableProducer.TableAttributes.CellSpacing := 0;
    DataSetTableProducer.TableAttributes.Width := 100;
  end;

 // RunModule.First;
  try
    if RunModule = ADActiveUsers then
      HTMLStr := ActiveUserHTML else
    HTMLStr := DataSetTableProducer.Content;
    //Email Settings in Ini File
    SendEmailWithIniSettings(IniFileName, 'Email', '', 'Attachments', HTMLStr);
  except
    on E: Exception do begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SendEmails';
      LogResult.ExcepMsg :='Error sending emails: ' + E.Message + '.';
      WriteLog(LogResult);
    end;
  end;
end;

function TDMHalSentinel.EnDeCrypt(const Value: String): String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function TDMHalSentinel.ConnectDB: Boolean;
const
  DECRYPT = 'DEC_';
begin
  Result := false;
  ADOConn.Connected:= false;
  try
    try
      if LeftStr(apassword, 4) = DECRYPT then
      begin
        apassword := copy(apassword, 5, maxint);
        apassword := EnDeCrypt(apassword);
      end
      else
      begin
        Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
      end;
      connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
        ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
      ADOConn.Open;
    except
      on E: Exception do
      begin
        if ParamStr(1) = 'GUI' then
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'connectDB';
          LogResult.DataStream := ADOConn.ConnectionString;
          LogResult.ExcepMsg := 'Connection failed ' + E.Message;
          WriteLog(LogResult);
        end;
        Raise;
      end;
    end;
  finally
    Result := ADOConn.Connected;
  end;
end;

procedure TDMHalSentinel.GetCompanies(CompanyList: TStringList);
var
  CompanySQL: String;
begin
  try
   //Companies := 'select distinct name from locating_company order by name';
    Companies.Open;

    While not Companies.EOF do
    begin
      CompanyList.Add(Companies.fieldbyname('name').AsString);
      Companies.next;
    end;
    LogResult.MethodName := 'DMHalSentinel.GetCompanies';
    LogResult.Status := 'Processing ' + CompanyList.Count.ToString + ' companies.';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  except
    On E:Exception do
    begin
      if ParamStr(2) <> 'GUI' then
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'DMHalSentinel.GetCompanies';
        LogResult.ExcepMsg := 'Error in GetCompanies: ' + E.Message;
        WriteLog(LogResult);
      end;
      Raise;
    end;
  end;
  Companies.Close;
end;

{procedure TDMHalSentinel.getMDRInfo(emp: TEmployee);
var
  SQL:String;
begin
  try
    try
      SQL := 'select WorkerstatusDescription, ReportToWorkerId from [UQVIEW].[dbo].[MDR_Import]' +
             ' where WorkerNumber = ' + QuotedStr(emp.adp_login);
      MDRInfo.CommandText := SQL;
      MDRInfo.Open;
      emp.WorkerStatusDescription := MDRInfo.fieldByName('workerstatusdescription').AsString;
      emp.reporttoworkerid := MDRInfo.fieldByName('ReportToWorkerId').AsInteger;
    except
      On E:Exception do
      begin
        if ParamStr(2) = 'GUI' then
           raise else
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'DMHalSentinel.GetMDRInfo';
          LogResult.ExcepMsg := 'Error in GetMDRInfo: ' + E.Message;
          WriteLog(LogResult);
        end;
      end;
    end;
  finally
    MDRInfo.Close;
  end;
end;  }

{function TDMHalSentinel.GetReportTo(reportToID: String): String;
var
  SQL:String;
begin
  try
    try
    SQL := 'select firstname, lastname from [UQVIEW].[dbo].[MDR_Import] where workerid = ' + ReportToID;
    ReportTo.CommandText := SQL;
    ReportTo.Open;
    Result := ReportTo.fieldbyname('firstname').AsString + ' ' +
     ReportTo.fieldbyname('lastname').AsString + '(wrkid: ' + reportToID + ')';
    except
      On E:Exception do
      begin
        if ParamStr(2) = 'GUI' then
           raise else
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'DMHalSentinel.GetReportTo';
          LogResult.ExcepMsg := 'Error in GetReportTo: ' + E.Message;
          WriteLog(LogResult);
        end;
      end;
    end;
  finally
    ReportTo.Close;
  end;
end;  }

function TDMHalSentinel.GetEmployeeList(company: String; EmployeeList:TObjectList): Boolean;
var
  SQL: String;
  Employee: TEmployee;
begin
  try
    try
      Result := True;
      SQL :=
      'select lc.name as comp_name, uqv.timesheet_num as timesheet_num, e.first_name as fn, ' +
      'e.last_name as ln, * from users u inner join employee e on u.emp_id = e.emp_id ' +
      'join locating_company lc on lc.company_id = e.company_id inner join uqview_employees uqv ' +
      'on uqv.employee_number = e.emp_number where u.active_ind = 1 and u.login_id IS NOT NULL ' +
      'and e.ad_username IS NOT NULL and e.ad_username <> '''' and e.ad_username <> ''NULL'' '  +
      'and lc.name = ''' + company + ''' order by uqv.timesheet_num';

      EmpbyCompany.CommandText := SQL;
      EmpbyCompany.Open;
      while not EmpbyCompany.EOF do
      begin
        Employee := TEmployee.Create;
        With Employee, EmpbyCompany do
        begin
          emp_id := fieldbyname('emp_id').AsInteger;
          emp_number := fieldbyname('emp_number').AsString;
          first_name := fieldbyname('fn').AsString;
          last_name := fieldbyname('ln').AsString;
          ad_username := fieldbyname('ad_username').AsString;
          adp_login := fieldbyname('adp_login').AsString;
          short_name := fieldbyname('short_name').AsString;
          active := fieldbyname('active_ind').AsBoolean;
          company_name := fieldbyname('comp_name').AsString;
          email := fieldbyname('email_address').AsString;
          phone := fieldbyname('contact_phone').AsString;
          center := fieldbyname('timesheet_num').AsString;
        end;
        EmployeeList.Add(Employee);
        EmpbyCompany.Next;
      end;
      LogResult.MethodName := 'DMHalSentinel.GetEmployeeList';
      LogResult.Status := EmployeeList.Count.ToString + ' users found that are active in the users table with proper employee.adUserName and ADPLogins.';
      LogResult.LogType := ltInfo;
      WriteLog(LogResult);
    except
      On E:Exception do
      begin
        if ParamStr(2) = 'GUI' then
           raise else
        begin
          Result := False;
          LogResult.LogType := ltError;
          LogResult.MethodName := 'DMHalSentinel.GetEmployeeList';
          LogResult.ExcepMsg := 'Error in GetEmployeeList: ' + 'Company: '+ Company + ', ' + E.Message;
          WriteLog(LogResult);
          Result := False;
        end;
      end;
    end;
  finally
    EmpbyCompany.Close;
  end;
end;

procedure TDMHalSentinel.CheckIsActiveEmployee(Emp: TEmployee; ADInfo: TADInfo);
begin
  try
    try
      ADActiveUsers.CommandText :=
       'select lastlogon, lastlogontimestamp, samaccountname, ' +
       'useraccountcontrol, givenname, sn, company ' +
       'from ''LDAP://ou=Sales,dc=MyDomain,dc=com'' ' +
       'where sAMAccountName= '' ' + emp.ad_username +'';
      ADActiveUsers.Open;
      if not ADActiveUsers.EOF then
      begin
        ADInfo.Name := ADActiveUsers.FieldByName('givenname').asString +
        ADActiveUsers.fieldByName('sn').asstring;
        ADInfo.Company := ADActiveUsers.FieldByName('company').AsString;
        ADInfo.Lastlogon := ADActiveUsers.FieldByName('lastlogon').AsString;
        ADInfo.lastLogonTimeStamp := ADActiveUsers.FieldByName('lastlogontimestamp').AsString;
        ADInfo.IsActive := ADActiveUsers.FieldByName('company').AsString = '512';
      end else
        ADInfo.returnCode := -1;
    finally
      ADActiveUsers.Close;
    end;
  except
    On E:Exception do
    begin
      ADActiveUsers.Close;
      if ParamStr(2) <> 'GUI' then
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'DMHalSentinel.CheckIsActiveEmployee';
        LogResult.ExcepMsg := 'Error in CheckIsActiveEmployee: ' + E.Message;
        WriteLog(LogResult);
        ADInfo.returnCode := -2;
      end else
      Raise;
    end;
  end;
end;

procedure TDMHalSentinel.qryByCompany;
var
  ListOfCompanies: TStringList;
  EmployeeListbyCompany: TObjectList;
  I: Integer;
begin
  if ParamStr(2) <> 'GUI' then
  begin
    LogResult.MethodName := 'DMHalSentinel.qrybyCompany';
    LogResult.Status := 'Starting CheckActiveUsers ';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  end;
  try
    ListOfCompanies := TStringList.Create;
    GetCompanies(ListofCompanies);
    for I := 0 to ListofCompanies.Count-1 do
    begin
      EmployeeListbyCompany := TObjectList.Create;
      If GetEmployeeList(ListofCompanies[I], EmployeeListByCompany) then
        ActiveUserHTML := ActiveUserHTML + RunCompany(EmployeeListbyCompany, ListofCompanies[I]);
      EmployeeListbyCompany.Free;
    end;
  finally
    ListOfCompanies.Free;
  end;
end;

function TDMHalSentinel.RunCompany(listToCheck: TObjectList; company: String): String;
// pwed: String = ':6r-((f4' + Chr(34) + 'Ud$gW.d'; // 'Possible escape issue.  orig = :6r-((f4\"Ud$gW.d
var
  InvalidEmails: TObjectList;
  InvalidADPLogins: TObjectList;
  InvalidPhones: TObjectList;
  isActiveEmpADActive: String;
  IsInactiveEmp: String;
  Inactivect: Integer;
  TotalChecked: Integer;
  ReportToString: String;
  I, J: Integer;
  Emp: TEmployee;
  empP: Temployee;
  ADInfo: TADInfo;
  finalemailtext: String;
  title, emaillist, noResults: String;
begin
  try
    try
      InvalidEmails := TObjectList.Create(False);
      InvalidADPLogins := TObjectList.Create(False);
      InvalidPhones := TObjectList.Create(False);
      TotalChecked := ListtoCheck.Count;
      Inactivect := 0;
      Result := '<tr><td><M=/td></tr><tr><td><b>Company: ';
      for I := 0 to ListtoCheck.Count-1 do
      begin
        Emp := TEmployee(ListtoCheck[I]);
        if Trim(Emp.email) = '' then
          InvalidEmails.Add(Emp);
        if (Trim(Emp.adp_login) = '') or
         (UpperCase(Emp.adp_login) = 'NULL') then
           InvalidADPLogins.Add(Emp);
        if Trim(Emp.phone)  = '' then
          InvalidPhones.Add(Emp);
        if Emp.Active then
          isActiveEmpADActive := 'TRUE'
        else
          isActiveEmpADActive := 'FALSE';
        ADInfo := TADInfo.Create;
        CheckIsActiveEmployee(Emp, ADInfo);
        If ADInfo.isActive = False then
        begin
          inc(inactivect);
          LogResult.MethodName := 'DMHalSentinel.RunCompany';
          LogResult.DataStream := 'NOT ACTIVE IN AD!  ADUser: ' + emp.ad_username + ' ' +
            ADInfo.name + ' ' + ADInfo.company + ' ( Center: ' + emp.center + '). ' +
            'Active status in QMManager: ' + isActiveEmpADActive + ' lastlogon: ' + ADInfo.lastLogon;
          LogResult.LogType := ltInfo;
          WriteLog(LogResult);
          //MDR References removed.
         { getMDRInfo(emp);
          If trim(emp.company_name) <> '' then
          begin
            ReportToString := GetReportTo(emp.reporttoworkerid.ToString());
            Result := Result + '<tr><td><b>' +  emp.ad_username.PadRight(9) +
             '</b>&nbsp;&nbsp;</td><td>&nbsp;' + emp.emp_number + '&nbsp;</td><td>' + ADInfo.name +
             '&nbsp;(' + emp.company_name +  ') ( Center: ' + emp.center + ')</td><td>&nbsp;MDR Status ' +
              emp.workerstatusdescription +  '</td><td>&nbsp; Report To: ' + ReportToString + '</td></tr>';
          end else
          begin
            Result := Result +  '<tr><td><b>' +  emp.ad_username.PadRight(9) +
            '</b>&nbsp;&nbsp;</td><td>&nbsp;' + emp.emp_number + '&nbsp;</td><td>' + ADInfo.name +
            '</td><td>&nbsp;MDR Status ' + emp.workerstatusdescription +
            '</td><td>&nbsp; Report To Id ' + emp.reporttoworkerid.ToString;
          end; }
        end;

        If ADInfo.returnCode = -1 then
        begin
          LogResult.MethodName := 'DMHalSentinel.RunCompany';
          LogResult.DataStream := 'NOT FOUND IN AD!  ADUser: ' + emp.ad_username +
          'Active status in QMManager: ' + isActiveEmpADActive + ' lastlogon: ' + ADInfo.lastLogon;
          LogResult.LogType := ltInfo;
          WriteLog(LogResult);
          Result := Result +  '<tr><th>NOT FOUND IN AD!  ADUser: '  + emp.ad_username +
          '. Active status in QMManager: ' + isActiveEmpADActive + ' </th></tr>';
        end;

        If ADInfo.returnCode = -2 then
        begin
          LogResult.MethodName := 'DMHalSentinel.RunCompany';
          LogResult.DataStream := 'ERROR in testing AD status for: ' + emp.ad_username;
          LogResult.LogType := ltError;
          WriteLog(LogResult);
          Result := Result + '<tr><th>ERROR in testing AD status for: ' + emp.ad_username + '</th></tr>';
        end;
      end;

      if ParamStr(2) <> 'GUI' then
      begin
        LogResult.MethodName := 'DMHalSentinel.RunCompany';
        LogResult.Status := 'AD user check processed finished';
        LogResult.LogType := ltInfo;
        WriteLog(LogResult);
      end;

      If inactivect > 0 then
      begin
        title := '<tr><td></td></tr><tr><td><b><font color=''red''>WARNING!</font></b></td></tr>';

        emailList := title + '<tr><td>I''ve queried the AD database against <b>'  +
          totalChecked.ToString + '</b> active Qmanager employees for company ' + company +
          'and found <b>' + inactivect.toString + '</b> that are out of compliance.  ' +
          'Please rectify.</td></tr><tr><td>&nbsp;</td></tr></tbody></table><table><tbody>' +
        result + '</tbody></table>';
        finalEmailText := emailList;
      end else
      begin
        noResults := '<tr><td></td></tr><tr><td><font color=''green''>No inactive AD employees for ' +
        company + '</font></td></tr>';
        finalEmailText := noResults;
      end;
      //END FIRST EMAIL SECTION

      If invalidADPLogins.Count > 0 then
      begin
        finalEmailText := finalEmailText + '<table><tbody><tr><td>&nbsp;</td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>Company: ' + company + '</b></td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>List of active users with invalid ADP logins. ' +
        'Count:</td><td>' + invalidADPLogins.Count.ToString + '</b></td></tr>';
        finalEmailText := finalEmailText + '</tbody></table><table><tbody>';
        finalEmailText := finalEmailText +
        '<tr><td><b>ADUsername</b></td><td><b>Emp #</b></td><td><b>Full Name/Company</b></td></tr>';

        For J := 0 to InvalidADPLogins.Count-1 do
        begin
          emp := TEmployee(InvalidADPLogins[J]);
          If Trim(emp.company_name) <> '' then
            finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
            '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td><td>' + emp.first_name + ' ' +
            emp.last_name + '&nbsp;(' + emp.company_name + ')( Center: ' + emp.center + ')</td></tr>'
          else
            finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
              '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td></tr>';
        end;
        finalEmailText := finalEmailText + '</tbody></table>'
      end;

      If invalidEmails.Count > 0 then
      begin
        finalEmailText := finalEmailText + '<table><tbody><tr><td>&nbsp;</td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>Company: ' + company + '</b></td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>List of active users with invalid emails. '+
          'Count:</td><td>' + invalidEmails.Count.ToString() + '</b></td></tr>';
        finalEmailText := finalEmailText + '</tbody></table><table><tbody>';

        finalEmailText := finalEmailText +
         '<tr><td><b>ADUsername</b></td><td><b>Emp #</b></td><td><b>Full Name/Company</b></td></tr>';

        For J := 0 to InvalidEmails.Count-1 do
        begin
          emp := TEmployee(InvalidEmails[J]);
          If Trim(emp.company_name) <> '' then
            finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
            '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td><td>' + emp.first_name + ' ' +
            emp.last_name + '&nbsp;(' + emp.company_name + ')( Center: ' + emp.center + ')</td></tr>'
          else
            finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
             '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td></tr>';
        end;
        finalEmailText := finalEmailText + '</tbody></table>';
      end;

      If InvalidPhones.Count > 0 then
      begin
        finalEmailText := finalEmailText + '<table><tbody><tr><td>&nbsp;</td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>Company: ' + company + '</b></td></tr>';
        finalEmailText := finalEmailText + '<tr><td><b>List of active users with invalid phone numbers. ' +
          'Count:</td><td>' + InvalidPhones.Count.ToString + '</b></td></tr>';
        finalEmailText := finalEmailText + '</tbody></table><table><tbody>';

        finalEmailText := finalEmailText +
          '<tr><td><b>ADUsername</b></td><td><b>Emp #</b></td><td><b>Full Name/Company</b></td></tr>';
        For J := 0 to InvalidPhones.Count-1 do
        begin
          emp := TEmployee(InvalidPhones[J]);
          If Trim(emp.company_name) <> '' then
            finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
            '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td><td>' + emp.first_name + ' ' +
            emp.last_name + '&nbsp;(' + emp.company_name + ')( Center: ' + emp.center + ')</td></tr>'
          else
          finalEmailText := finalEmailText + '<tr><td><b>' + UpperCase(emp.ad_username) +
            '</b></td><td>&nbsp;' + emp.emp_number + '&nbsp;</td></tr>';
        end;
        finalEmailText := finalEmailText + '</tbody></table>';
      end;
      Result := finalEmailText;

      LogResult.MethodName := 'DMHalSentinel.RunCompany';
      LogResult.Status := inactivect.ToString + ' number of active QManager employees that are inactive in AD';
      LogResult.LogType := ltInfo;
      WriteLog(LogResult);
    finally
      InvalidADPLogins.Free;
      InvalidEmails.Free;
      InvalidPhones.Free;
      ADInfo.Free;
    end;
  except
    On E:Exception do
    begin
      if ParamStr(2) = 'GUI' then
         raise else
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'DMHalSentinel.RunCompany';
        LogResult.ExcepMsg := 'Error in RunCompany: ' + company + ', ' + E.Message;
        WriteLog(LogResult);
      end;
    end;
  end;
end;

end.






              }
