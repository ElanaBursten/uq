unit ActiveUser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.DBCtrls, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Vcl.ComCtrls, Vcl.ExtCtrls,
  System.Actions, Vcl.ActnList;

type
  TActiveUsersForm = class(TForm)
    EmpByCoGridView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    EmpByCoGridViewcomp_name: TcxGridDBColumn;
    EmpByCoGridViewtimesheet_num: TcxGridDBColumn;
    EmpByCoGridViewfn: TcxGridDBColumn;
    EmpByCoGridViewln: TcxGridDBColumn;
    EmpByCoGridViewuid: TcxGridDBColumn;
    EmpByCoGridViewgrp_id: TcxGridDBColumn;
    EmpByCoGridViewemp_id: TcxGridDBColumn;
    EmpByCoGridViewlogin_id: TcxGridDBColumn;
    EmpByCoGridViewpassword: TcxGridDBColumn;
    EmpByCoGridViewchg_pwd: TcxGridDBColumn;
    EmpByCoGridViewlast_login: TcxGridDBColumn;
    EmpByCoGridViewactive_ind: TcxGridDBColumn;
    EmpByCoGridViewcan_view_notes: TcxGridDBColumn;
    EmpByCoGridViewcan_view_history: TcxGridDBColumn;
    EmpByCoGridViewchg_pwd_date: TcxGridDBColumn;
    EmpByCoGridViewmodified_date: TcxGridDBColumn;
    EmpByCoGridViewemail_address: TcxGridDBColumn;
    EmpByCoGridViewetl_access: TcxGridDBColumn;
    EmpByCoGridViewapi_key: TcxGridDBColumn;
    EmpByCoGridViewaccount_disabled_date: TcxGridDBColumn;
    EmpByCoGridViewtype_id: TcxGridDBColumn;
    EmpByCoGridViewstatus_id: TcxGridDBColumn;
    EmpByCoGridViewtimesheet_id: TcxGridDBColumn;
    EmpByCoGridViewemp_number: TcxGridDBColumn;
    EmpByCoGridViewshort_name: TcxGridDBColumn;
    EmpByCoGridViewmiddle_init: TcxGridDBColumn;
    EmpByCoGridViewreport_to: TcxGridDBColumn;
    EmpByCoGridViewcreate_date: TcxGridDBColumn;
    EmpByCoGridViewcreate_uid: TcxGridDBColumn;
    EmpByCoGridViewmodified_uid: TcxGridDBColumn;
    EmpByCoGridViewcharge_cov: TcxGridDBColumn;
    EmpByCoGridViewactive: TcxGridDBColumn;
    EmpByCoGridViewcan_receive_tickets: TcxGridDBColumn;
    EmpByCoGridViewtimerule_id: TcxGridDBColumn;
    EmpByCoGridViewdialup_user: TcxGridDBColumn;
    EmpByCoGridViewcompany_car: TcxGridDBColumn;
    EmpByCoGridViewrepr_pc_code: TcxGridDBColumn;
    EmpByCoGridViewpayroll_pc_code: TcxGridDBColumn;
    EmpByCoGridViewcrew_num: TcxGridDBColumn;
    EmpByCoGridViewautoclose: TcxGridDBColumn;
    EmpByCoGridViewcompany_id: TcxGridDBColumn;
    EmpByCoGridViewrights_modified_date: TcxGridDBColumn;
    EmpByCoGridViewticket_view_limit: TcxGridDBColumn;
    EmpByCoGridViewhire_date: TcxGridDBColumn;
    EmpByCoGridViewshow_future_tickets: TcxGridDBColumn;
    EmpByCoGridViewincentive_pay: TcxGridDBColumn;
    EmpByCoGridViewcontact_phone: TcxGridDBColumn;
    EmpByCoGridViewlocal_utc_bias: TcxGridDBColumn;
    EmpByCoGridViewad_username: TcxGridDBColumn;
    EmpByCoGridViewtest_com_login: TcxGridDBColumn;
    EmpByCoGridViewadp_login: TcxGridDBColumn;
    EmpByCoGridViewhome_lat: TcxGridDBColumn;
    EmpByCoGridViewhome_lng: TcxGridDBColumn;
    EmpByCoGridViewalt_lat: TcxGridDBColumn;
    EmpByCoGridViewalt_lng: TcxGridDBColumn;
    EmpByCoGridViewplat_update_date: TcxGridDBColumn;
    EmpByCoGridViewplat_age: TcxGridDBColumn;
    EmpByCoGridViewchangeby: TcxGridDBColumn;
    EmpByCoGridViewphoneco_ref: TcxGridDBColumn;
    EmpByCoGridViewatlas_number: TcxGridDBColumn;
    EmpByCoGridViewfirst_task_reminder: TcxGridDBColumn;
    EmpByCoGridViewterminated: TcxGridDBColumn;
    EmpByCoGridViewtermination_date: TcxGridDBColumn;
    EmpByCoGridViewlogo_filename: TcxGridDBColumn;
    EmpByCoGridViewaddress_street: TcxGridDBColumn;
    EmpByCoGridViewaddress_city: TcxGridDBColumn;
    EmpByCoGridViewaddress_state: TcxGridDBColumn;
    EmpByCoGridViewaddress_zip: TcxGridDBColumn;
    EmpByCoGridViewphone: TcxGridDBColumn;
    EmpByCoGridViewbilling_footer: TcxGridDBColumn;
    EmpByCoGridViewfloating_holidays_per_year: TcxGridDBColumn;
    EmpByCoGridViewpayroll_company_code: TcxGridDBColumn;
    EmpByCoGridViewreport_to_number: TcxGridDBColumn;
    EmpByCoGridViewreport_to_first_name: TcxGridDBColumn;
    EmpByCoGridViewreport_to_last_name: TcxGridDBColumn;
    EmpByCoGridViewmanager_number: TcxGridDBColumn;
    EmpByCoGridViewmanager_first_name: TcxGridDBColumn;
    EmpByCoGridViewmanager_last_name: TcxGridDBColumn;
    Panel1: TPanel;
    cbCompany: TDBLookupComboBox;
    btnEmpByCompany: TButton;
    Splitter1: TSplitter;
    Panel2: TPanel;
    btnInactiveEmp: TButton;
    cbInactiveSearch: TCheckBox;
    btnInactiveEmpInfo: TButton;
    cxGrid2: TcxGrid;
    InactiveGridView: TcxGridDBTableView;
    InactiveGridViewemp_id: TcxGridDBColumn;
    InactiveGridViewemp_number: TcxGridDBColumn;
    InactiveGridViewfirst_name: TcxGridDBColumn;
    InactiveGridViewlast_name: TcxGridDBColumn;
    InactiveGridViewshort_name: TcxGridDBColumn;
    InactiveGridViewtype_id: TcxGridDBColumn;
    InactiveGridViewtimesheet_id: TcxGridDBColumn;
    InactiveGridViewcompany_id: TcxGridDBColumn;
    InactiveGridViewemp_active: TcxGridDBColumn;
    InactiveGridViewaccount_disabled_date: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cbEmpByCompanySearch: TCheckBox;
    ActionList1: TActionList;
    btnAction: TAction;
    Panel3: TPanel;
    btnADInfo: TButton;
    DBGrid1: TDBGrid;
    procedure btnEmpbyCompanyClick(Sender: TObject);
    procedure btnADInfoClick(Sender: TObject);
    procedure btnReportToClick(Sender: TObject);
    procedure btnMDRInfoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbEmpByCompanySearchClick(Sender: TObject);
    procedure cbInactiveSearchClick(Sender: TObject);
    procedure btnInactiveEmpInfoClick(Sender: TObject);
    procedure btnInactiveEmpClick(Sender: TObject);
    procedure cbCompanyCloseUp(Sender: TObject);
    procedure btnActionUpdate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ActiveUsersForm: TActiveUsersForm;

implementation
uses
  HALData, TermEmp;

{$R *.dfm}

procedure TActiveUsersForm.btnEmpbyCompanyClick(Sender: TObject);
begin
 With DMHalSentinel do
 begin
   Screen.Cursor := crHourGlass;
   EmpbyCompany.Close;
   EmpbyCompany.Parameters.ParamByName('company').Value := cbCompany.Text;
   EmpbyCompany.Open;
   Screen.Cursor := crDefault;
 end;
end;

procedure TActiveUsersForm.btnInactiveEmpClick(Sender: TObject);
begin
  DMHalSentinel.InactiveEmps.Open;
end;

procedure TActiveUsersForm.btnInactiveEmpInfoClick(Sender: TObject);
begin
  With DMHalSentinel do
 begin
   if InactiveEmps.Active and not InactiveEmps.EOF then
     DisplayTermEmpInfo(InactiveEmps.FieldByName('emp_id').AsInteger);
 end;
end;

procedure TActiveUsersForm.btnMDRInfoClick(Sender: TObject);
begin
 With DMHalSentinel do
 begin
   MDRInfo.Parameters.ParamByName('adplogin').Value := EmpByCompany.FieldByName('adp_login').AsInteger;
   MDRInfo.Open;
 end;
end;

procedure TActiveUsersForm.btnActionUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := not DMHalSentinel.EmpbyCompany.EOF;
end;

procedure TActiveUsersForm.btnADInfoClick(Sender: TObject);
begin
  With DMHalSentinel do
  begin
    ADActiveUsers.Parameters.ParamByName('adusername').Value :=
      DMHalSentinel.EmpByCompany.FieldByName('ad_username').AsString;
    ADActiveUsers.Open;
  end;
end;

procedure TActiveUsersForm.btnReportToClick(Sender: TObject);
begin
  With DMHalSentinel do
  begin
    ReportTo.Parameters.ParamByName('reporttoid').Value := EmpByCompany.FieldByName('adp_login').AsString;
    ReportTo.Open;
  end;
end;

procedure TActiveUsersForm.cbCompanyCloseUp(Sender: TObject);
begin
  if cbCompany.Text <> '' then
    btnEmpByCompany.Enabled := True;
end;

procedure TActiveUsersForm.cbEmpByCompanySearchClick(Sender: TObject);
begin
  if cbEmpByCompanySearch.Checked then
    EmpByCoGridView.Controller.ShowFindPanel
  else
    EmpByCoGridView.Controller.HideFindPanel;
end;

procedure TActiveUsersForm.cbInactiveSearchClick(Sender: TObject);
begin
  if cbInactiveSearch.Checked then
    InactiveGridView.Controller.ShowFindPanel
  else
    InactiveGridView.Controller.HideFindPanel;
end;

procedure TActiveUsersForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMHalSentinel.Companies.Close;
end;

procedure TActiveUsersForm.FormShow(Sender: TObject);
begin
  DMHalSentinel.Companies.Open;
end;

end.
