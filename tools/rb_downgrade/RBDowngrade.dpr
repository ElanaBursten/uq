program RBDowngrade;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  Windows,
  OdMiscUtils in '..\..\common\OdMiscUtils.pas';

procedure Error(const Msg: string);
begin
  MessageBeep(0);
  WriteLn('ERROR: ' + Msg);
  Sleep(5000);
  Halt;
end;

procedure Downgrade(const FileName: string);
const
  RemoveStrings: array[0..5] of string =
    ('HyperlinkColor = clBlue',
     'PrinterSetup.SaveDeviceSettings = False',
     'PDFSettings.EmbedFontOptions = []',
     'HideWhenOneDetail = False',
     'NewFile = False',
     'StartOnOddPage = False');
  ReplacePaperName: array[0..1] of string =
    ('Carta - 21,59 x 27,94 cm (8,5 x 11 pol.) ',
     'Carta - 21,59 x 27,94 cm (8,5 x 11 pol.)');
  NotAllowedIdentifiers: array[0..0] of string =
    ('raCodMod');//This identifier means that RAP scripting language (Enterprise version) is being used. Do not commit reports with RAP, we use Pro version.
//Now that we've upgraded to ReportBuilder 14.01 these identifiers no longer apply.
//In future, add identifiers as needed. These are left as example.
//    ('TraCodeModule');
//     'TppParameterList');
var
  Contents: TStringList;
  i: Integer;
begin
  Writeln('Processing: ' + FileName);
  Contents := TStringList.Create;
  try
    Contents.LoadFromFile(FileName);
    for i := Low(RemoveStrings) to High(RemoveStrings) do
      RemoveStringFromList(RemoveStrings[i], Contents, True);
    for i := Low(ReplacePaperName) to High(ReplacePaperName) do
      Contents.Text := StringReplace(Contents.Text, ReplacePaperName[i], 'Letter 8 1/2 x 11 in', [rfIgnoreCase, rfReplaceAll]);
    ReplaceSubStringToEndOfLineInList('Version = ', 'Version = ''10.07''', Contents, True);
    for i := Low(NotAllowedIdentifiers) to High(NotAllowedIdentifiers) do
      if StrContains(NotAllowedIdentifiers[i], Contents.Text) then
        Error('Identifier ' + NotAllowedIdentifiers[i] + ' not allowed in ' + FileName);

    Contents.SaveToFile(FileName);
  finally
    FreeAndNil(Contents);
  end;
end;

var
  FileName: string;
  i: Integer;
begin
  try
    if ParamCount < 1 then
      raise Exception.Create('Command Syntax: RBDowngrade.exe filename.dfm [filename2.dfm] ...');
    for i := 1 to ParamCount do begin
      FileName := ParamStr(i);

      if not SameText(ExtractFileExt(FileName), '.dfm') then
        Error('Not a .dfm file: ' + FileName);
      if not FileExists(FileName) then
        Error('DFM file does not exist: ' + FileName);
      Downgrade(FileName);
    end;
  finally
    Sleep(2000);
  end;
end.
