unit ExcelConverter;

interface

uses SysUtils, ActiveX, OdExcel, Excel2000;

function RunConvert: Boolean;

implementation

function RunConvert: Boolean;
var
  InFile, OutFile: string;
  InPath: string;
  F: TSearchRec;
  FindStatus: Integer;
  Excel: TExcelApplication;
begin
  Result := False;
  if ParamCount < 1 then begin
    WriteLn('TSVFileToExcel makes xls files from tab delimited files (wildcards are allowed)');
    WriteLn('Syntax: ');
    WriteLn('  TSVFileToExcel InputFileName.tsv');
    Exit;
  end;

  Excel := StartExcel;
  try
    InFile := ParamStr(1);
    InPath := ExtractFilePath(InFile);
    if InPath = '' then
      InPath := ExtractFilePath(ParamStr(0));
    InFile := InPath + ExtractFileName(InFile);

    FindStatus := FindFirst(InFile, faAnyFile, F);
    try
      while FindStatus = 0 do begin
        InFile := InPath + F.Name;
        OutFile := ChangeFileExt(InFile, '.xls');
        ConvertTSVFileToExcel(Excel, InFile, OutFile);
        WriteLn(' converted ' + ExtractFileName(InFile) + ' to ' + ExtractFileName(OutFile));
        FindStatus := FindNext(F);
      end;
      Result := True;
    finally
      FindClose(F);
    end;
  finally
    StopExcel(Excel);
    FreeAndNil(Excel);
  end;
end;

initialization
  CoInitialize(nil);

finalization
  CoUninitialize;

end.
