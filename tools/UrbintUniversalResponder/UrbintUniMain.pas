unit UrbintUniMain;
//QM-503
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, IdIntercept, IdLogBase, IdLogEvent, IdMessage, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;  //TLogResults


type
  TfrmUrbintUniResponder = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    delResponse: TADOQuery;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdMessage1: TIdMessage;
    IdLogEvent1: TIdLogEvent;
    qryEMailRecipients: TADOQuery;

    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;

    FRespondto: string;
    fCallCenter: string;
    FPosturl: string;
    fOC_Code: string;
    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    function deleteMultiQueueRecord(RespondTo: string; LocateID: integer): boolean;
    procedure SetUpRecipients;
    { Private declarations }
  public
    { Public declarations }

    property CallCenter: string read fCallCenter write fCallCenter;
    property OC_Code:string read fOC_Code write fOC_Code; //QM-552
    property Respondto: string read FRespondto write FRespondto;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
    property Posturl: string read FPosturl write FPosturl;
  end;

var
  frmUrbintUniResponder: TfrmUrbintUniResponder;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmUrbintUniResponder.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;


procedure TfrmUrbintUniResponder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmUrbintUniResponder.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  Respondto := ParamStr(1);
  CallCenter:= ParamStr(2);
  AppVer:='';
  ProcessINI;
  AppVer  := GetAppVersionStr;
  if (Respondto='') or (CallCenter='')  then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'processParams';
    LogResult.Status := 'MISSING Params';
    LogResult.DataStream:='RespondTo: '+ ParamStr(1) + '  CallCenter: '+ParamStr(2);
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'processParams';
    LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
    WriteLog(LogResult);
  end;
  connectDB;
  SetUpRecipients;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);


  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;


function TfrmUrbintUniResponder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmUrbintUniResponder.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmUrbintUniResponder.ProcessINI: boolean;
var
  UrbintUni: TIniFile;
begin
  try
    UrbintUni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := UrbintUni.ReadString('Database', 'Server', '');
    aDatabase := UrbintUni.ReadString('Database', 'DB', 'QM');
    ausername := UrbintUni.ReadString('Database', 'UserName', '');
    apassword := UrbintUni.ReadString('Database', 'Password', '');

    flogDir    :=UrbintUni.ReadString('LogPaths', 'LogPath', '');

    OC_Code := UrbintUni.ReadString('EMailAlerts', 'RecipientsOC_Code', '3008');   //QM-552

    with IdMessage1.From do  //QM-552
    begin
      Address:=UrbintUni.ReadString('EMailAlerts', 'FromAddress', '');
      Domain:=UrbintUni.ReadString('EMailAlerts', 'FromDomain', '');
      Name:=UrbintUni.ReadString('EMailAlerts', 'FromName', '');
      Text:=UrbintUni.ReadString('EMailAlerts', 'FromText', '');
      User:=UrbintUni.ReadString('EMailAlerts', 'FromUser', '');
    end;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

    StatusBar1.panels[1].Text:= aServer;
  finally
    UrbintUni.Free;
  end;
end;

procedure TfrmUrbintUniResponder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + RespondTo +'_UrbintUni-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;
{
ticket_id - our ticket id as string
pr_code - ?
pr_label - ?
pr_description - ?
member_code  - ?
tranmitted_datetime  - date time snet to them as UTC in ISO-8601 format
}

procedure TfrmUrbintUniResponder.btnSendResponseClick(Sender: TObject);
//https://api.external.urbint.com/api/locate-response
const
  PARSER = 'PARSER'; //qm-544
  SKIP_STATUS = 'SKIP';
  DBL_QUOTES = '"';
  COMPLETE = 'CLOSE';
  UNI_BODY =
          '{ '+
          '"locate_status":"%s", '+
          '"ticket_id":"%s", '+
          '"pr_code":"%s", '+
          '"pr_label":"%s", '+
          '"pr_description":"%s", '+
          '"member_code":"%s", '+
          '"transmitted_datetime":"%s",  '+
          '"Call_Center":"%s", '+
          '"locator": "Urbint-UtiliQuest", '+     //qm-688
          '"remarks": "%s" '+    //qm-937
          '}';

var
  ticket_number :string; //ticket_id
  OutStatus:string;   //pr_code
  status_explanation:string;//pr_label
  pr_description:string;
  LocateID:integer;
  URL:string;
  Call_Center : string;  //  one_call_center
  EPR_Link :string;    //qm-937
  locate_status:string;  //CLOSE if complete true
  api_key:string;
  member_code:string; //client_code :;
  ResponseBody:string;
  cntGood, cntBad :integer;
  requestID:string;
  FormattedTransmitDateTime:string;
  Fmt: TFormatSettings;
  ISO8601date: string;
  localDT: TDateTime;
  utcDate: TDateTime;
  AddedBy: string; //qm-544 sr

  function GetUTC(dt: TDateTime): TDateTime;
  begin
    result := TTimeZone.Local.ToUniversalTime(dt);
  end;

begin
  OutStatus:='';
  status_explanation:='';
  pr_description:='';
  ticket_number :='';
  LocateID:=0;
  api_key:='';
  FormattedTransmitDateTime :='';
  ResponseBody:='';
  URL :='';
  Call_Center := '';
  EPR_Link :='';    //qm-937
  member_code:='';
  requestID:='';
  locate_status:='';
  AddedBy:=''; //qm-544 sr
  ISO8601date := '';
  localDT := 0.0;
  utcDate := 0.0;
  localDT := Now;



  utcDate:= GetUTC(localDT);
  //utcDate := GetUTC(StrToDateTime(copy(dateString, length(TICKET_DATETIME) + 5, length(dateString) - 22), Fmt));
  Fmt.ShortDateFormat := 'yyyy-mm-dd';
  Fmt.DateSeparator := '-';
  Fmt.LongTimeFormat := 'hh:nn:ss';
  Fmt.TimeSeparator := ':';
  FormattedTransmitDateTime:= DateTimeToStr(utcDate, Fmt);

  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value:= Respondto;
    Parameters.ParamByName('@CallCenter').Value:= CallCenter;
    Parameters.ParamByName('@ParsedLocatesOnly').Value:= 0; //qm-544 sr
    open;
    while not eof do
    begin
      locate_status := Trim(FieldByName('status').AsString);
      LocateID      := FieldByName('locate_id').AsInteger;
      api_key   := Trim(FieldByName('api_key').AsString);
      AddedBy :=    Trim(FieldByName('added_by').AsString); //qm-544 sr
      status_explanation := Trim(FieldByName('status_explanation').AsString);   // pr_label
      pr_description :=  status_explanation;
      ticket_number := Trim(FieldByName('ticket_number').AsString);
      member_code   := AnsiUpperCase(Trim(FieldByName('Client_Code').AsString)); //member_code
      URL           := Trim(FieldByName('URL').AsString);
      Call_Center   := Trim(FieldByName('one_call_center').AsString);
      EPR_Link      := Trim(FieldByName('EPR_Link').AsString);    //qm-937
      If FieldByName('complete').AsInteger = 1 then locate_status:=COMPLETE;  //locate_status
      OutStatus     := AnsiUpperCase(Trim(FieldByName('outgoing_status').AsString));  //pr_code

      if AddedBy <> PARSER then  //qm-544 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-544 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed NON-Parser_Added Response';
        WriteLog(LogResult);

        next;
        continue;
      end;

      if OutStatus = SKIP_STATUS then  //qm-543 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-543 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed Skipped Outstatus Response';
        LogResult.DataStream := 'Ticket no: '+ ticket_number+ ' Member Code: '+member_code+ ' LocateID: '+IntToStr(LocateID);
        WriteLog(LogResult);

        next;
        continue;
      end;

      ResponseBody  := format(UNI_BODY,[locate_status,ticket_number,OutStatus, status_explanation, pr_description, member_code,FormattedTransmitDateTime,Call_Center,EPR_Link]);

      RESTRequest1.Params.Clear;
      RESTRequest1.AddAuthParameter('Authorization',api_key,pkHTTPHEADER);

      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';

      RESTClient1.BaseURL :=  URL;
      RESTRequest1.AddBody(ResponseBody, ctAPPLICATION_JSON);

      RESTRequest1.Method := TRESTRequestMethod.rmPost; //status change


      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseBody;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticket_number;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin
//        LogResult.LogType := ltInfo;
//        LogResult.MethodName := 'Process Ticket locate';
//        LogResult.DataStream := 'Response sent '+ResponseBody;
//        WriteLog(LogResult);
                                  //ResponseBody
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'sendResponse: '+ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +ticket_number;
        WriteLog(LogResult);

        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        deleteMultiQueueRecord(RespondTo,LocateID);
      end  //StatusCode > 199 <300   good
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticket_number;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        WriteLog(LogResult);

        //QM-552 Add EMail notices to all Delphi Responders
        IdMessage1.Body.Add('Failed request no '+IntToStr(cntBad)+' at '+formatdatetime('yyyy-mm-dd hh:mm:ss', Now));
        IdMessage1.Body.Add(IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticket_number);
        IdMessage1.Body.Add('Sent '+ResponseBody);
        IdMessage1.Body.Add('Returned ' + RESTResponse1.Content);


        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF

  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

  if cntBad>0 then
  begin
    idSMTP1.Connect;
    idSMTP1.Send(IdMessage1);
    idSMTP1.Disconnect();

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Sending email';
    LogResult.Status := 'Some responses failed';
    LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
    WriteLog(LogResult);
  end;

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmUrbintUniResponder.SetUpRecipients;  //QM-552
begin
  try
    idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
    qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
    qryEMailRecipients.Open;
    if not qryEMailRecipients.eof then
    idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
    else
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SetUpRecipients';
      LogResult.Status:= 'No recipients to send email to';
      WriteLog(LogResult);
   end;
  finally
    qryEMailRecipients.close;
  end;
end;

function TfrmUrbintUniResponder.deleteMultiQueueRecord(RespondTo:string;LocateID:integer):boolean;
begin
  delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
  delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

  memo1.Lines.Add(delResponse.SQL.Text);

  try
   delResponse.ExecSQL;   //commented out for testing
  except on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'delResponse';
      LogResult.DataStream := delResponse.SQL.Text;
      LogResult.ExcepMsg:=e.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmUrbintUniResponder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmUrbintUniResponder.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog  := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
