program AboutTimeShifts;
//qm-775 Timesheet Approval


uses
  Vcl.Forms,
  SysUtils,
  TimeShiftsMain in 'TimeShiftsMain.pas' {frmAboutTime},
  GlobalSU in 'GlobalSU.pas';

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

var
  MyInstanceName: string;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName+'_'+ParamStr(1));
  LocaInstance:=MyInstanceName;
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(2) = 'GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm := True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm := False;
    end;
    Application.Title := '';

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmAboutTime, frmAboutTime);
  Application.Run;
  end
  else
    Application.Terminate;

end.
