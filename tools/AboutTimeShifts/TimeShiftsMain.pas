unit TimeShiftsMain;
//QM-775
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.JSON, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB, Data.Win.ADODB;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);
  TShiftRec = (AllShift, ShiftStart, ShiftEnd, BreakStart, BreakEnd);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults
type
  TfrmAboutTime = class(TForm)
    pnlTop: TPanel;
    pnlMiddle: TPanel;
    pnlBottom: TPanel;
    Memo1: TMemo;
    Splitter1: TSplitter;
    ADOConn: TADOConnection;
    qryRabbitMQ: TADOQuery;
    updRabbitMQ: TADOQuery;
    delRabbitMQ: TADOQuery;
    cbRunOnce: TCheckBox;
    StatusBar1: TStatusBar;
    btnParsejson: TButton;
    JsonMemo: TMemo;
    Splitter2: TSplitter;
    qryStaging: TADOQuery;
    insStagingTable: TADOQuery;
    qryEmpID: TADOQuery;
    spProcessRtasqTimeEntries: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure btnParsejsonClick(Sender: TObject);
  private
    { Private declarations }

      aDatabase: String;
      aServer: String;
      ausername: string;
      apassword: string;

      UTCOffset: String;
      EmpID: Integer;
      WorkerNumber: String;
      ShiftDateStr, ShiftIDStr,
      WorkerNbrStr: String;
      ShiftTypeidStr, NameStr: String;

      LogResult: TLogResults;
      flogDir: wideString;
      QueueName:string;
      function ProcessINI: boolean;
      function connectDB: boolean;
      procedure clearLogRecord;
      procedure getData(OrigObj: TJsonObject; Field: String);

      procedure ProcessPairs;
      procedure DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
      function GetAppVersionStr: string;
      function BuildStagingQuery(AStrList: TStringList; BrkStartIdx: Integer = 0; BrkEndIdx: Integer = -1): Boolean;
      function GetEmpID(WorkerNumher:String; out EmpID: Integer): Boolean;
    function ProcessRows: Boolean;
  public
    { Public declarations }
    cntGood, cntBad :integer;
    Strlist: TStringList;
    ShiftStartList: TStringlist;
    ShiftEndList: TstringList;
    BreakStartList: TStringList;
    BreakEndList: TStringList;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmAboutTime: TfrmAboutTime;
  LocaInstance:string;

implementation
uses dateutils, StrUtils, iniFiles;
{$R *.dfm}

{ TForm2 }

procedure TfrmAboutTime.btnParsejsonClick(Sender: TObject);
begin
  ProcessPairs;
end;

//BP  Process mqrabbit_in table
procedure TfrmAboutTime.ProcessPairs;
var
  Json_Data: String;
  JsonValue  : TJsonValue;
  OriginalObj : TJsonObject;
  QueueName: String;
  StageFields, I: Integer;
  Field: String;
  MessageID: Integer;

begin
  cntGood := 0;
  cntBad  := 0;
  try
    try
      ShiftStartList := TStringList.Create;
      ShiftEndList := TStringList.Create;
      BreakStartList := TStringList.Create;
      BreakEndList := TStringList.Create;
      qryStaging.Open;
      JsonMemo.Lines.Clear;
      //retrieve queuename
      QueueName := ParamStr(1);
      //utq-rtasq-events-approved-shifts
      qryRabbitmq.Parameters.ParamByName('QueueName').Value := QueueName;
      qryRabbitmq.Open;

      while not qryRabbitmq.Eof do
      begin
        MessageID :=  qryRabbitMQ.FieldbyName('message_id').AsInteger;
        Json_data := qryRabbitMQ.FieldbyName('message_payload').AsString;
        JsonValue := TJsonObject.ParseJSONValue(Json_data);
        OriginalObj := JsonValue as TJsonObject;
        //iterate through Json fields, Process Json strings and objects
        for I := 0 to OriginalObj.Count - 1 do
        begin
          Field :=  OriginalObj.Pairs[I].JsonString.Value;
          GetData(OriginalObj, Field);
        end;
        JsonMemo.Lines.Add('---------------------------------------------');

        //build staging table insert query
        if ProcessRows then
        begin
          DeleteFromMQ(MessageID, 'PROCESSED',True);
          inc(cntGood);
        end;
        ShiftStartList.Clear;
        ShiftEndList.Clear;
        BreakStartList.Clear;
        BreakEndList.Clear;
        JsonValue.Free;
        StatusBar1.Panels[3].Text:= IntToStr(cntGood);
        StatusBar1.Panels[5].Text:= IntToStr(cntBad);
        StatusBar1.Panels[7].Text:= IntToStr(cntBad+cntGood);
        if cbRunOnce.Checked then break;

        qryRabbitmq.Next;
      end;


    WriteLog(LogResult);
    memo1.Lines.Add('Process finished');
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'About Time ';
    LogResult.Status := 'Process finished';
    LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
    memo1.Lines.Add(LogResult.DataStream);
    WriteLog(LogResult);

    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'processPairs';
        LogResult.DataStream := JSonValue.ToString;
        LogResult.ExcepMsg := E.Message;
        LogResult.Status := 'Failed';
        WriteLog(LogResult);
      end;
    end;
  finally
    qryRabbitMQ.Close;
    qryStaging.Close;
    ShiftStartList.Free;
    ShiftEndList.Free;
    BreakStartList.Free;
    BreakEndList.Free;
  end;
end;

function TfrmAboutTime.ProcessRows: Boolean;
var
 I: Integer;
 BrkEnd: Integer;
 BrkStart: Integer;
 IdxArray: Array of Integer;

begin
  Result := True;
  try
    ADOConn.BeginTrans;

    //retrieve emp id from workernumber
    If not GetEmpID(WorkerNumber, EmpID) then
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Build staging query';
      LogResult.DataStream := 'emp ID for worker number ' + WorkerNumber + ' invalid or not found ';
      WriteLog(LogResult);
      Memo1.Lines.Add('error inserting staging table record');
      Result := False;
    end;

    BuildStagingQuery(ShiftStartList);
    BuildStagingQuery(ShiftEndList);

    //Add shifttype object fields to breaklists
    for I := 0 to  BreakStartList.Count-1 do
    begin
      if I = 0 then
      begin
        BreakStartList.Insert(I, ShiftTypeIDStr);
        BreakStartList.Insert(I, NameStr);
      end;
      if BreakStartList[I] = 'end' then
      begin
        BreakStartList.Insert(I+1, ShiftTypeIDStr);
        BreakStartList.Insert(I+1, NameStr);
      end;
    end;

    for I := 0 to  BreakEndList.Count-1 do
    begin
      if I = 0 then
      begin
        BreakEndList.Insert(I, ShiftTypeIDStr);
        BreakEndList.Insert(I, NameStr);
      end;
      if BreakEndList[I] = 'end' then
      begin
        BreakEndList.Insert(I+1, ShiftTypeIDStr);
        BreakEndList.Insert(I+1, NameStr);
      end;
    end;
    for I := 1 to 5 do
    begin
      BreakStartList.Delete(BreakStartList.Count-1);
      BreakEndList.Delete(BreakEndList.Count-1);
    end;

     //build staging queries for break row sections
    for I := 0 to  BreakStartList.Count-1 do
    begin
      if BreakStartList[I] = 'end' then
      begin
        SetLength(IdxArray, Length(IdxArray) + 1);
        IdxArray[High(IdxArray)] := I;
      end;
    end;
    BrkStart := 0;
    for I := Low(IdxArray) to High(IdxArray) do
    begin
      BrkEnd := IdxArray[I];
      BuildStagingQuery(BreakStartList, BrkStart, BrkEnd-1);
      if I = High(IdxArray) then break;
      BrkStart := Brkend + 1;
    end;
    SetLength(IdxArray, 0);
    for I := 0 to  BreakEndList.Count-1 do
    begin
      if BreakEndList[I] = 'end' then
      begin
        SetLength(IdxArray, Length(IdxArray) + 1);
        IdxArray[High(IdxArray)] := I;
      end;
    end;
    BrkStart := 0;
    for I := Low(IdxArray) to High(IdxArray) do
    begin
      BrkEnd := IdxArray[I];
      BuildStagingQuery(BreakEndList, BrkStart, BrkEnd-1);
      if I = High(IdxArray) then break;
      BrkStart := Brkend + 1;
    end;

    ADOConn.CommitTrans;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Build staging query';
      LogResult.ExcepMsg := E.Message;
      LogResult.DataStream := insStagingTable.SQL.Text;
      WriteLog(LogResult);
      Result := False;
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('error inserting staging table records');
      Result := False;
      ADOConn.RollbackTrans;
      inc(cntBad);
    end;
  end;
end;

//BP  Build the insert sql from stringlist key value pair (includes Source, insert_date and EmpID as added fields)
function TfrmAboutTime.BuildStagingQuery(AStrList: TStringList; BrkStartIdx: Integer = 0; BrkEndIdx: Integer = -1): Boolean;
var
 I: Integer;
 AFields, AValues: String;
 InsertSQL: String;
 EventTime: TDateTime;
 DateStr: String;
begin
  InsertSQL := 'Insert into rabbitmq_staging_rtime_shifts ';
  AFields := AFields + 'source,';
  AValues := AValues + QuotedStr('RTasq') +',';
  AFields := AFields +  'insert_date,';
  AValues := AValues + 'getdate(),';
  AFields := AFields + 'emp_id' +',';
  AValues := AValues + QuotedStr(InttoStr(EmpID)) + ',';
  if BrkEndIdx = -1 then
    BrkEndIdx := AStrList.Count-1;
  for I := BrkStartIdx to BrkEndIdx do
  begin
    AFields := AFields + AStrList.KeyNames[I] + ',';
    if AStrList.KeyNames[I] = 'EventTime' then
    begin
      DateStr := copy(AStrList.ValueFromIndex[I], 1,10) + ' ' +  copy(AStrList.ValueFromIndex[I], 12, 5);
      AValues := AValues + QuotedStr(DateStr) + ','
    end else
     AValues := AValues + QuotedStr(AStrList.ValueFromIndex[I]) + ',';
  end;

  Delete(AFields, Length(AFields), 1);
  Delete(AValues, Length(AValues), 1);
  InsertSQL := InsertSQL + '(' + AFields + ') values (' + AValues  + ')';
  insStagingTable.SQL.Text := InsertSQL;
  insStagingTable.ExecSQL;
end;

//BP Emp ID retrieved from WorkerNumber field
function TfrmAboutTime.GetEmpID(WorkerNumher:String; out EmpID:Integer): Boolean;
begin
  Result := True;
  qryEmpID.Parameters.ParamByName('EmpNumber').Value := WorkerNumber;
  qryEmpID.Open;
  if not qryEmpID.EOF then
    EmpID := qryEmpID.FieldByName('emp_id').AsInteger
  else Result := False;
  qryEmpID.Close;
end;

//BP  Process strings, objects(with nested subobjects and arrays)
procedure TfrmAboutTime.getData(OrigObj: TJsonObject; Field: String);
var
  jso: TJsonObject;
  jsPair, jsDataPair : TJsonPair;
  jsoNestedObj: TJsonObject;
  jsNestedPair: TJsonPair;
  jsoShiftObj: TJsonObject;
  jsShiftPair: TJsonPair;
  jsoLatLong: TJsonObject;
  jsLatLongPair: TJsonPair;
  OutputStr: String;
  JsOutputStr : String;
  I:Integer;
  jsArr: TJsonArray;
  jsoArr: TJsonObject;
  jsArrPair: TJsonPair;
  Shifttype: TShiftRec;
  jsArrNestedPair: TJsonPair;
  jsoArrNested: TJsonObject;
  ArrayElement: TJsonValue;


//BP  Reads into stringlist key value pair for building staging table sql
procedure GetStagingData(ShiftRec: TShiftRec; Field, Value: String);
begin
  if qryStaging.FindField(Field) <> nil then
  begin
    JsOutputStr := Field + '=' + Value;
    if Field = 'ShiftDate' then
      ShiftDatestr := JsOutputStr else
    if Field = 'ShiftId' then
      ShiftIDStr := JsOutputStr else
    if Field = 'WorkerNumber' then
      WorkerNbrStr := JsOutputStr else
    if Field = 'shift_type_id' then
      ShiftTypeIdStr := JsOutputStr else
    if Field = 'name' then
       NameStr := JsOutputStr;

    case Shiftrec of
      ShiftStart: ShiftStartList.Add(JsOutputStr);
      ShiftEnd:  ShiftEndList.Add(JsOutputStr);
      BreakStart: BreakStartList.Add(JsOutputStr);
      BreakEnd: BreakEndList.Add(JsOutputStr);
      AllShift:
      begin
        ShiftStartList.Add(JsOutputStr);
        ShiftEndList.Add(JsOutputStr);
        BreakStartList.Add(JsOutputStr);
        BreakEndList.Add(JsOutputStr);
      end;
    end;
  end;
end;

begin
  jsDataPair := OrigObj.Get(Field);
  If (jsDataPair.jsonValue is TJsonObject)
   and (jsDataPair.JsonString.Value = 'data') then
  begin
    JsonMemo.Lines.Add('Data:');
    jso := jsDataPair.jsonValue as TJsonObject;
    for jsPair in jso do
    begin
      If (jsPair.jsonValue is TJsonObject)
        and (jsPair.JsonString.Value = 'Shift') or (jsPair.JsonString.Value = 'shift_type') then
      begin
        OutputStr := '      ' + jsPair.JsonString.Value + ':';;
        JsonMemo.Lines.Add(OutputStr);
        jsoShiftObj := jsPair.jsonValue as TJsonObject;
        for jsShiftPair in jsoShiftObj do
        begin
          if (jsShiftPair.jsonValue is TJsonObject) and
             (jsShiftPair.JsonString.Value <> 'BreakTotal') then
          begin
            OutputStr := '      ' + jsShiftPair.JsonString.Value + ':';
            JsonMemo.Lines.Add(OutputStr);
            jsoNestedObj := jsShiftPair.jsonValue as TJsonObject;

            ShiftType := AllShift;
            if jsShiftPair.JsonString.Value = 'ShiftStart' then
              ShiftType :=  ShiftStart;
            if jsShiftPair.JsonString.Value = 'ShiftEnd' then
              ShiftType :=  ShiftEnd;

            for jsNestedPair in jsoNestedObj do
            begin
              if jsNestedPair.JsonString.Value <> 'Acknowledgement' then
              begin
                if jsNestedPair.jsonValue is TJsonObject then
                begin
                  OutputStr := '      ' + jsNestedPair.JsonString.Value + ':';;
                  JsonMemo.Lines.Add(OutputStr);
                  jsoLatLong := jsNestedPair.jsonValue as TJsonObject;
                  for jsLatLongPair in jsoLatLong do
                  begin
                    if (jsLatLongPair.JsonValue is TJsonString)then
                    begin
                      OutputStr := '           ' + jsLatLongPair.JsonString.Value + ': ' + jsLatLongPair.JsonValue.Value;
                      JsonMemo.Lines.Add(OutputStr);
                      GetStagingData(ShiftType,jsLatLongPair.JsonString.Value, jsLatLongPair.JsonValue.Value);
                    end;
                  end;
                end else
                if (jsNestedPair.jsonValue is TJsonString) and
                   (jsNestedPair.JsonString.Value <> 'WorkerNumber') then
                begin
                  OutputStr := '           ' + jsNestedPair.JsonString.Value + ': ' +
                    jsNestedPair.JsonValue.Value;
                  JsonMemo.Lines.Add(OutputStr);
                  GetStagingData(ShiftType, jsNestedPair.JsonString.Value, jsNestedPair.JsonValue.Value);
                end;
              end;
            end;
          end else
          if (jsShiftPair.jsonValue is TJsonString) then
          begin
            if jsShiftPair.JsonString.Value = 'WorkerNumber' then
              WorkerNumber := jsShiftPair.JsonValue.Value;

            OutputStr := '           ' + jsShiftPair.JsonString.Value + ': ' + jsShiftPair.JsonValue.Value;
            JsonMemo.Lines.Add(OutputStr);
            GetStagingData(AllShift, jsShiftPair.JsonString.Value, jsShiftPair.JsonValue.Value);
          end else
          if jsShiftPair.JsonValue is TJsonArray then
          begin
            OutputStr := '      ' + jsShiftPair.JsonString.Value + ':';
            JsonMemo.Lines.Add(OutputStr);
            jsArr := jsShiftPair.JsonValue as TJsonArray;
            for ArrayElement in jsArr do
            begin
              if ArrayElement is TJsonObject then
              begin
                jsoArr := ArrayElement as TJsonObject;
                for jsArrPair in jsoArr do
                begin
                   if (jsArrPair.jsonValue is TJsonString) and
                   (jsArrPair.JsonString.Value <> 'WorkerNumber') then
                  begin
                    OutputStr := '           ' + jsArrPair.JsonString.Value + ': ' + jsArrPair.JsonValue.Value;
                    JsonMemo.Lines.Add(OutputStr);
                    GetStagingData(ShiftType, jsArrPair.JsonString.Value, jsArrPair.JsonValue.Value);
                  end else
                  if (jsArrPair.jsonValue is TJsonObject) then
                  begin
                    If jsArrPair.JsonString.Value = 'BreakStart' then
                      ShiftType :=  BreakStart;
                    if jsArrPair.JsonString.Value = 'BreakEnd' then
                      ShiftType :=  BreakEnd;
                    OutputStr := '      ' + jsArrPair.JsonString.Value + ':';
                    JsonMemo.Lines.Add(OutputStr);

                    jsoArrNested := jsArrPair.JsonValue as TJsonObject;
                    for jsArrNestedPair in jsoArrNested do
                    begin
                      if jsArrNestedPair.JsonString.Value <> 'Acknowledgement' then
                      begin
                        if (jsArrNestedPair.JsonValue is TJsonString) and
                          (jsArrNestedPair.JsonString.Value <> 'WorkerNumber') then
                        begin
                          OutputStr := '           ' + jsArrNestedPair.JsonString.Value + ': ' + jsArrNestedPair.JsonValue.Value;
                          JsonMemo.Lines.Add(OutputStr);
                          GetStagingData(ShiftType, jsArrNestedPair.JsonString.Value, jsArrNestedPair.JsonValue.Value);
                        end else
                        if jsArrNestedPair.JsonValue is TJsonObject then
                        begin
                          OutputStr := '      ' + jsArrNestedPair.JsonString.Value + ':';
                          JsonMemo.Lines.Add(OutputStr);
                          jsoLatLong := jsArrNestedPair.JsonValue as TJsonObject;

                          for jsLatLongPair in jsoLatLong do
                          begin
                            if (jsLatLongPair.jsonValue is TJsonString)then
                            begin
                              OutputStr := '           ' + jsLatLongPair.JsonString.Value + ': ' + jsLatLongPair.JsonValue.Value;
                              JsonMemo.Lines.Add(OutputStr);
                             GetStagingData(ShiftType, jsLatLongPair.JsonString.Value, jsLatLongPair.JsonValue.Value);
                            end;
                          end;
                        end;
                      end;
                    end;
                    //add shift level fields to break row sections of breaklists.
                    If jsArrPair.JsonString.Value = 'BreakStart' then
                    begin
                      BreakStartList.Add('end');
                      BreakStartList.Add(ShiftDateStr);
                      BreakStartList.Add(ShiftIDStr);
                      BreakStartList.Add(WorkerNbrStr);
                    end else
                    If jsArrPair.JsonString.Value = 'BreakEnd' then
                    begin
                      BreakEndList.Add('end');
                      BreakEndList.Add(ShiftDateStr);
                      BreakEndList.Add(ShiftIDStr);
                      BreakEndList.Add(WorkerNbrStr);
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end else
      if (jsPair.jsonValue is TJsonString) then
      begin
        OutputStr := '           ' + jsPair.JsonString.Value + ': ' + jsPair.JsonValue.Value;
        JsonMemo.Lines.Add(OutputStr);
        GetStagingData(AllShift, jsPair.JsonString.Value, jsPair.JsonValue.Value);
      end;
    end;
  end else
  begin
    if (jsDataPair.jsonValue is TJsonString) and
      (jsDatapair.JsonString.Value <> 'EventType') then
    begin
      OutputStr := '           ' + jsDataPair.JsonString.Value + ': ' + jsDataPair.JsonValue.Value;
      JsonMemo.Lines.Add(OutputStr);
        GetStagingData(AllShift, jsDataPair.JsonString.Value, jsDataPair.JsonValue.Value);
    end;
  end;
end;

procedure TfrmAboutTime.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmAboutTime.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aServer;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

// update the rabbitmq record and delete. delete trigger inserts record into the history table
procedure TfrmAboutTime.DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
begin
  updRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  updRabbitMQ.Parameters.ParamByName('processed').Value := process;
  updRabbitMQ.Parameters.ParamByName('status').Value := Status;
  if updRabbitMQ.ExecSQL > 0 then
    delRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  delRabbitMQ.ExecSQL;
end;

procedure TfrmAboutTime.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  StatusBar1.Panels[9].Text := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  QueueName := ParamStr(1);
  ProcessINI;
  connectDB;

  if QueueName='' then
  begin
    showmessage('Rabbit MQ queue name is required as the first command line parameter');
    Application.Terminate;
  end;

  if uppercase(ParamStr(2)) <> 'GUI' then
  begin
    ProcessPairs;
    application.Terminate;
  end;
end;

function TfrmAboutTime.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;


function TfrmAboutTime.ProcessINI: boolean;
var
  AboutTime: TIniFile;
begin
  try
    AboutTime := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := AboutTime.ReadString('Database', 'Server', '');
    aDatabase := AboutTime.ReadString('Database', 'DB', 'QM');
    ausername := AboutTime.ReadString('Database', 'UserName', '');
    apassword := AboutTime.ReadString('Database', 'Password', '');

    flogDir := AboutTime.ReadString('LogPaths', 'LogPath', '');


    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    AboutTime.Free;
  end;
end;


procedure TfrmAboutTime.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + LocaInstance +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

end.
