program ScannaWorker;
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  ScannaWorkers in 'ScannaWorkers.pas' {FrmScannaWorkers};

//{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmScannaWorkers, FrmScannaWorkers);
  Application.Run;
end.
