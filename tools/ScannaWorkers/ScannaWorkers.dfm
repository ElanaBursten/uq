object FrmScannaWorkers: TFrmScannaWorkers
  Left = 0
  Top = 0
  Caption = 'Load Scanna Workers'
  ClientHeight = 461
  ClientWidth = 789
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 423
    Width = 46
    Height = 13
    Caption = 'Database'
  end
  object Label2: TLabel
    Left = 274
    Top = 423
    Width = 54
    Height = 13
    Caption = 'Count pass'
  end
  object Label3: TLabel
    Left = 352
    Top = 423
    Width = 46
    Height = 13
    Caption = 'Count fail'
  end
  object Label4: TLabel
    Left = 536
    Top = 423
    Width = 35
    Height = 13
    Caption = 'Version'
  end
  object Label5: TLabel
    Left = 216
    Top = 423
    Width = 24
    Height = 13
    Caption = 'Total'
  end
  object btnOpen: TButton
    Left = 160
    Top = 336
    Width = 112
    Height = 25
    Caption = 'Open File'
    TabOrder = 0
    OnClick = btnOpenClick
  end
  object btnSave: TButton
    Left = 302
    Top = 335
    Width = 112
    Height = 25
    Caption = 'Save to Tickets'
    Enabled = False
    TabOrder = 1
    OnClick = btnSaveClick
  end
  object btnKill: TBitBtn
    Left = 706
    Top = 336
    Width = 75
    Height = 25
    Caption = 'Kill Switch'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btnKillClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 442
    Width = 789
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 70
      end
      item
        Width = 70
      end
      item
        Width = 70
      end
      item
        Width = 200
      end>
  end
  object topPnl: TPanel
    Left = 0
    Top = 0
    Width = 789
    Height = 330
    Align = alTop
    TabOrder = 4
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 787
      Height = 328
      Align = alClient
      DataSource = dsScannaFile
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object BitBtn1: TBitBtn
    Left = 704
    Top = 408
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 5
  end
  object OpenDialog1: TOpenDialog
    Left = 719
    Top = 242
  end
  object FileADOConnection: TADOConnection
    ConnectionString = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 711
    Top = 296
  end
  object qryScannaFile: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from [Export Worksheet$]')
    Left = 627
    Top = 249
    object qryScannaFilePREMISE_NO: TFloatField
      FieldName = 'PREMISE_NO'
    end
    object qryScannaFileHOUSE_NO: TWideStringField
      DisplayWidth = 25
      FieldName = 'HOUSE_NO'
      Size = 255
    end
    object qryScannaFileSTREET_PREFIX: TWideStringField
      DisplayWidth = 25
      FieldName = 'STREET_PREFIX'
      Size = 255
    end
    object qryScannaFileSTREET_NAME: TWideStringField
      DisplayWidth = 25
      FieldName = 'STREET_NAME'
      Size = 255
    end
    object qryScannaFileSTREET_TYPE: TWideStringField
      DisplayWidth = 20
      FieldName = 'STREET_TYPE'
      Size = 255
    end
    object qryScannaFileSTREET_SUFFIX: TWideStringField
      DisplayWidth = 20
      FieldName = 'STREET_SUFFIX'
      Size = 255
    end
    object qryScannaFileSTREET_LOCATION_1: TWideStringField
      DisplayWidth = 25
      FieldName = 'STREET_LOCATION_1'
      Size = 255
    end
    object qryScannaFileSTREET_LOCATION_2: TWideStringField
      DisplayWidth = 25
      FieldName = 'STREET_LOCATION_2'
      Size = 255
    end
    object qryScannaFileADDRESS_OVERFLOW: TWideStringField
      DisplayWidth = 25
      FieldName = 'ADDRESS_OVERFLOW'
      Size = 255
    end
    object qryScannaFileCITY: TWideStringField
      DisplayWidth = 25
      FieldName = 'CITY'
      Size = 255
    end
    object qryScannaFileNAME: TWideStringField
      DisplayWidth = 20
      FieldName = 'NAME'
      Size = 255
    end
    object qryScannaFileMETER_LIST: TWideStringField
      DisplayWidth = 15
      FieldName = 'METER_LIST'
      Size = 255
    end
    object qryScannaFileLAT_Y: TFloatField
      DisplayWidth = 15
      FieldName = 'LAT_Y'
    end
    object qryScannaFileLONG_X: TFloatField
      DisplayWidth = 15
      FieldName = 'LONG_X'
    end
  end
  object dsScannaFile: TDataSource
    AutoEdit = False
    DataSet = qryScannaFile
    Left = 591
    Top = 304
  end
  object InsertLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'qty_marked'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'price'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'closed'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'closed_by_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'closed_how'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'closed_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'modified_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'active'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'assigned_to'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'regular_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'overtime_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'added_by'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'high_profile'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'high_profile_reason'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'seq_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'assigned_to_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mark_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'alert'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'entry_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'gps_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status_changed_by_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '  Insert  into Locate ('
      '  ticket_id,  '
      '  client_code,  '
      '  client_id,  '
      '  status,  '
      '  qty_marked,  '
      '  price,  '
      '  closed,  '
      '  closed_by_id,  '
      '  closed_how,  '
      '  closed_date,'
      '  modified_date,'
      '  active,'
      '  assigned_to,  '
      '  regular_hours,'
      '  overtime_hours,  '
      '  added_by,  '
      '  high_profile,'
      '  high_profile_reason,  '
      '  seq_number,  '
      '  assigned_to_id,  '
      '  mark_type,  '
      '  alert,  '
      '  entry_date, '
      '  gps_id,  '
      '  status_changed_by_id'
      ')'
      'Values ('
      ' :ticket_id,  '
      ' :client_code,  '
      ' :client_id,  '
      ' :status,  '
      ' :qty_marked,  '
      ' :price,  '
      ' :closed,'
      ' :closed_by_id,  '
      ' :closed_how,  '
      ' :closed_date,  '
      ' :modified_date,'
      ' :active,'
      ' :assigned_to,  '
      ' :regular_hours,'
      ' :overtime_hours,  '
      ' :added_by,  '
      ' :high_profile,'
      ' :high_profile_reason,  '
      ' :seq_number,  '
      ' :assigned_to_id,'
      ' :mark_type,  '
      ' :alert,  '
      ' :entry_date, '
      ' :gps_id,  '
      ' :status_changed_by_id'
      ')')
    Left = 500
    Top = 356
  end
  object qryTicketID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select top 1 * from ticket where ticket_number = :ticket_number'
      'order by ticket_id desc'
      ''
      '')
    Left = 604
    Top = 348
  end
  object UpdateTicketImage: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'image'
        Attributes = [paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Update Ticket set image = :image where ticket_id = :ticket_id')
    Left = 524
    Top = 244
  end
  object InsertTicket: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'parsed_ok'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'kind'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'map_page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'revision'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'transmit_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'legal_due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3500
        Value = Null
      end
      item
        Name = 'work_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'work_county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_address_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'work_address_street'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'work_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 90
        Value = Null
      end
      item
        Name = 'work_remarks'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1200
        Value = Null
      end
      item
        Name = 'company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'con_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'call_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'caller_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'channel'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_lat'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'work_long'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'image'
        Attributes = [paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'ticket_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 38
        Value = Null
      end
      item
        Name = 'recv_manager_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'wo_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'modified_date'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = 'active'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Insert into Ticket ('
      '  ticket_number,'
      '  parsed_ok,'
      '  ticket_format,'
      '  kind,'
      '  status,'
      '  map_page,'
      '  revision,'
      '  transmit_date,'
      '  due_date,'
      '  legal_due_date,'
      '  work_description,'
      '  work_state,'
      '  work_county,'
      '  work_city,'
      '  work_address_number,'
      '  work_address_street,'
      '  work_type,'
      '  work_remarks,'
      '  company,'
      '  con_name,'
      '  call_date,'
      '  caller_contact,'
      '  channel,'
      '  work_lat,'
      '  work_long,'
      '  image,'
      '  ticket_type,'
      '  recv_manager_id,'
      '  wo_number,'
      '  modified_date,'
      '  active'
      ')'
      'Values ('
      ' :ticket_number,'
      ' :parsed_ok,'
      ' :ticket_format,'
      ' :kind,'
      ' :status,'
      ' :map_page,'
      ' :revision,'
      ' :transmit_date,'
      ' :due_date,'
      ' :legal_due_date,'
      ' :work_description,'
      ' :work_state,'
      ' :work_county,'
      ' :work_city,'
      ' :work_address_number,'
      ' :work_address_street,'
      ' :work_type,'
      ' :work_remarks,'
      ' :company,'
      ' :con_name,'
      ' :call_date,'
      ' :caller_contact,'
      ' :channel,'
      ' :work_lat,'
      ' :work_long,'
      ' :image,'
      ' :ticket_type,'
      ' :recv_manager_id,'
      ' :wo_number,'
      ' :modified_date,'
      ':active )')
    Left = 504
    Top = 304
  end
  object spAssignLocate: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'assign_locate'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkloadDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 512
    Top = 185
  end
  object qryLastLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select locate_id '
      'from locate'
      'where ticket_id = :TicketID')
    Left = 600
    Top = 184
  end
  object qryDupTicketNbr: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'TicketNumber'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Select top 1 ticket_id'
      'from ticket '
      'where ticket_number = :TicketNumber'
      'order by ticket_id desc')
    Left = 696
    Top = 184
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 56
    Top = 336
  end
  object insTicketVersion: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_revision'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_number'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_type'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'transmit_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'processed_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'arrival_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'serial_number'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'image'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'filename'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftWideString
        Size = -1
        Value = Null
      end
      item
        Name = 'source'
        DataType = ftWideString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [dbo].[ticket_version]'
      '           ([ticket_id]'
      '           ,[ticket_revision]'
      '           ,[ticket_number]'
      '           ,[ticket_type]'
      '           ,[transmit_date]'
      '           ,[processed_date]'
      '           ,[arrival_date]'
      '           ,[serial_number]'
      '           ,[image]'
      '           ,[filename]'
      '           ,[ticket_format]'
      '           ,[source])'
      '     VALUES'
      '           (:ticket_id'
      '           ,:ticket_revision'
      '           ,:ticket_number'
      '           ,:ticket_type'
      '           ,:transmit_date'
      '           ,:processed_date'
      '           ,:arrival_date'
      '           ,:serial_number'
      '           ,:image'
      '           ,:filename'
      '           ,:ticket_format'
      '           ,:source)'
      ''
      '')
    Left = 424
    Top = 336
  end
end
