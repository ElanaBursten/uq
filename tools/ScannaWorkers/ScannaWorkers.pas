unit ScannaWorkers;
//QM-309/984 tool to process SCANA Service Cared Spreadsheets
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Data.Win.ADODB, IniFiles, Vcl.ComCtrls, DateUtils, Vcl.Buttons, Vcl.ExtCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TFrmScannaWorkers = class(TForm)
    btnOpen: TButton;
    btnSave: TButton;
    OpenDialog1: TOpenDialog;
    FileADOConnection: TADOConnection;
    qryScannaFile: TADOQuery;
    dsScannaFile: TDataSource;
    qryScannaFilePREMISE_NO: TFloatField;
    qryScannaFileHOUSE_NO: TWideStringField;
    qryScannaFileSTREET_PREFIX: TWideStringField;
    qryScannaFileSTREET_NAME: TWideStringField;
    qryScannaFileSTREET_TYPE: TWideStringField;
    qryScannaFileSTREET_SUFFIX: TWideStringField;
    qryScannaFileSTREET_LOCATION_1: TWideStringField;
    qryScannaFileSTREET_LOCATION_2: TWideStringField;
    qryScannaFileADDRESS_OVERFLOW: TWideStringField;
    qryScannaFileCITY: TWideStringField;
    qryScannaFileNAME: TWideStringField;
    qryScannaFileMETER_LIST: TWideStringField;
    qryScannaFileLAT_Y: TFloatField;
    qryScannaFileLONG_X: TFloatField;
    InsertLocate: TADOQuery;
    qryTicketID: TADOQuery;
    UpdateTicketImage: TADOQuery;
    InsertTicket: TADOQuery;
    spAssignLocate: TADOStoredProc;
    qryLastLocate: TADOQuery;
    qryDupTicketNbr: TADOQuery;
    btnKill: TBitBtn;
    StatusBar1: TStatusBar;
    topPnl: TPanel;
    DBGrid1: TDBGrid;
    ADOConn: TADOConnection;
    insTicketVersion: TADOQuery;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnKillClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    AppPath, ScannaFile: String;
    countiesList:tstringList;
    LogResult: TLogResults;
    TotalRecords:integer;
    connStr: String;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    procedure WriteLog;
    procedure clearLogRecord;
    procedure OpenFile;
    procedure ConnectDB;
    procedure SaveTicket;
    function DataSetToXML(DataSet: TDataSet): String;
    function GetAppVersionStr: string;
    function ProcessINI: Boolean;
    { Private declarations }
  public
    { Public declarations }
    logPath:string;
  end;

var
  FrmScannaWorkers: TFrmScannaWorkers;

implementation

{$R *.dfm}

procedure TFrmScannaWorkers.btnCloseClick(Sender: TObject);
begin
  qryScannaFile.Close;
end;

function TFrmScannaWorkers.DataSetToXML(DataSet: TDataSet): String;
const
  ROOT = 'NewTicket';
  tab = #9;
var
  I: integer;

  function MakeTag(TagName, value: String): string;
  begin
    result := '<' + TagName + '>' + value + '</' + TagName + '>';
  end;

begin
  result := '';
  result := result + '<' + ROOT + '>';
  DataSet.First;
  while not DataSet.EOF do
  begin
    result := result + '<RECORD>'; //each record is a result
    for I := 0 to DataSet.Fields.Count - 1 do
      result := result + MakeTag(DataSet.Fields[I].DisplayName,
    DataSet.Fields[I].Text);
    result := result + '</RECORD>';
    DataSet.Next;
  end;
  result := result + '</' + ROOT + '>'; //the entire XML are the Results
end;

procedure TFrmScannaWorkers.btnKillClick(Sender: TObject);
begin
  StatusBar1.Panels[3].Text :=  'Processed Killed';
  btnKill.ModalResult:=mrAbort;
end;

procedure TFrmScannaWorkers.btnOpenClick(Sender: TObject);
var
 Msg: String;
begin
  try
    self.Cursor := crSQLWait;
    if qryScannaFile.Active then
      qryScannaFile.Close;
    OpenFile;
    self.Cursor := crDefault;
    btnOpen.Enabled := false;
    btnSave.Enabled := True;
  except
    on E:Exception do
    begin
      Msg :=  'connection to file error: ' + E.Message;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'OpenFile';
      LogResult.ExcepMsg := Msg;
      WriteLog;
      btnSave.Enabled := False;
      raise Exception.Create(Msg);
    end;
  end;
end;

procedure TFrmScannaWorkers.btnSaveClick(Sender: TObject);
begin
    TThread.CreateAnonymousThread(procedure begin
    SaveTicket;
    End).Start;

  btnSave.Enabled := False;
end;

procedure TFrmScannaWorkers.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TFrmScannaWorkers.SaveTicket;
var
  TicketID: integer;
  TicketNbr: String;
  LocateID: integer;
  PremiseNo: String;
  IsDup: Boolean;
  cnt, fail: integer;
  DataSetXMl: String;
  PostDate: TDateTime;
  LatDec, LngDec: double;
const
  // ticket insert
  TICKET_FORMAT = '1421';
  WORK_TYPE = 'GAS SERVICE CARD CREATION';
  CLIENT_CODE = 'DOMSVC';
  CLIENT_ID = 4908;
  TICKET_TYPE = 'GAS SERVICE CARD CREATION';
  RECV_MANAGER_ID = 17476;

  // insert locate
  Status = '-R';
  ASSIGNED_TO = 27617;
  ADDED_BY = 'PARSER';
  ASSIGNED_TO_ID = 27617;

  // for spAssignLocate
  ASSIGN_LOCATE_ID = '27617';
  ADDED_BY_ASSIGN_LOCATE = 3512;

begin
  try
    cnt := 0;
    fail := 0;
    With qryScannaFile do
    begin
      While not EOF do
      begin
        DataSetXMl := '';
        PostDate := Date;
        LatDec := 0.0;
        LngDec := 0.0;

        PremiseNo := qryScannaFile.FieldByName('PREMISE_NO').AsString;
        if Trim(PremiseNo) = '' then
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'SaveTicket';
          LogResult.ExcepMsg := 'Premise No field is blank';
          WriteLog;
          inc(fail);
          StatusBar1.Panels[3].Text := IntToStr(fail);
          Next;
          Continue;
        end;
        IsDup := false;
        TicketNbr := 'DOM-' + PremiseNo;
        qryDupTicketNbr.Parameters.ParamByName('TicketNumber').value :=
          TicketNbr;
        qryDupTicketNbr.open;
        IsDup := not(qryDupTicketNbr.Fields[0].IsNull);
        qryDupTicketNbr.Close;

        if IsDup then
        begin
          LogResult.LogType := ltWarning;
          LogResult.MethodName := 'Dupe Check';
          LogResult.Status := 'Duplicate Ticket No: ' + TicketNbr + ' found';
          WriteLog;
          Next;
          Continue;
        end;

        try
          ADOConn.BeginTrans;
          With InsertTicket.Parameters, qryScannaFile do
          begin
            ParamByName('ticket_number').value := TicketNbr;
            ParamByName('parsed_ok').value := True;
            ParamByName('ticket_format').value := '1421';
            ParamByName('kind').value := 'NORMAL';
            ParamByName('status').value := null;
            ParamByName('map_page').value := '';
            ParamByName('revision').value := '0';
            ParamByName('transmit_date').value := PostDate;
            ParamByName('due_date').value := IncYear(PostDate, 1);
            ParamByName('legal_due_date').value := IncYear(PostDate, 1);
            ParamByName('work_description').value :=
              'Create service card utilizing ' + 'premise number ' + PremiseNo +
              ' from Tapping TEE to residential service meter';
            ParamByName('work_state').value := 'SC';
            ParamByName('work_county').value := FieldByName('NAME').AsString;
            ParamByName('work_city').value := FieldByName('CITY').AsString;
            ParamByName('work_address_number').value :=
              FieldByName('HOUSE_NO').AsString;
            ParamByName('work_address_street').value :=
              FieldByName('STREET_PREFIX').AsString + ' ' +
              FieldByName('STREET_NAME').AsString + ' ' +
              FieldByName('STREET_TYPE').AsString + ' ' +
              FieldByName('STREET_SUFFIX').AsString;
            ParamByName('work_type').value := WORK_TYPE;
            ParamByName('work_remarks').value :=
              FieldByName('METER_LIST').AsString;
            ParamByName('company').value := 'DOMINION';
            ParamByName('con_name').value := 'DOMINION';
            ParamByName('call_date').value := now;
            ParamByName('caller_contact').value := null;
            ParamByName('channel').value := 0;

            If TryStrToFloat(FieldByName('LAT_Y').AsString, LatDec) then
              ParamByName('work_lat').value := LatDec
            else
              ParamByName('work_lat').value := null;

            If TryStrToFloat(FieldByName('LONG_X').AsString, LngDec) then
              ParamByName('work_long').value := LngDec
            else
              ParamByName('work_long').value := null;

            ParamByName('image').value := '';
            ParamByName('ticket_type').value := TICKET_TYPE;
            ParamByName('recv_manager_id').value := RECV_MANAGER_ID;
            ParamByName('wo_number').value := PremiseNo;
            ParamByName('modified_date').value := now;
            ParamByName('active').value := 1;
            InsertTicket.ExecSQL;

            LogResult.LogType := ltInfo;
            LogResult.MethodName := 'InsertTicket';
            LogResult.Status := 'Ticket No: ' + TicketNbr + ' inserted into DB';
            WriteLog;
          end;

          qryTicketID.Parameters.ParamByName('ticket_number').value :=
            TicketNbr;
          qryTicketID.open;
          if qryTicketID.EOF then
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'qryTicketID';
            LogResult.Status := 'Ticket ID was not found for ' +
              'Premise(Ticket) Number: ' + TicketNbr;
            WriteLog;
            inc(fail);
            StatusBar1.Panels[3].Text := IntToStr(fail);
            qryTicketID.Close;
            ADOConn.RollbackTrans;
            Next;
            Continue;
          end;

          TicketID := qryTicketID.FieldByName('ticket_id').AsInteger;
          DataSetXMl := DataSetToXML(qryTicketID);
          UpdateTicketImage.Parameters.ParamByName('ticket_id').value
            := TicketID;
          UpdateTicketImage.Parameters.ParamByName('image').value := DataSetXMl;

          UpdateTicketImage.ExecSQL;

          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'OpenFile';
          LogResult.Status := 'Ticket Image updated for ' + TicketNbr;
          WriteLog;

          qryTicketID.Close;

          With insTicketVersion.Parameters do
          begin
            ParamByName('ticket_id').value := TicketID;
            ParamByName('ticket_revision').value := '0';
            ParamByName('ticket_number').value := TicketNbr;
            ParamByName('ticket_type').value := TICKET_TYPE;
            ParamByName('transmit_date').value := PostDate;
            ParamByName('processed_date').value := PostDate;
            ParamByName('arrival_date').value := PostDate;
            ParamByName('serial_number').value := null;
            ParamByName('image').value := DataSetXMl;
            ParamByName('filename').value := ExtractFileName(ScannaFile);
            ParamByName('ticket_format').value := '1421';
            ParamByName('source').value := '1421';
          end;
          insTicketVersion.ExecSQL;

          With InsertLocate.Parameters, qryScannaFile do
          begin
            ParamByName('ticket_id').value := TicketID;
            ParamByName('client_code').value := CLIENT_CODE;
            ParamByName('client_id').value := CLIENT_ID;
            ParamByName('status').value := Status;
            ParamByName('qty_marked').value := 0;
            ParamByName('price').value := null;
            ParamByName('closed').value := 0;
            ParamByName('closed_by_id').value := null;
            ParamByName('closed_how').value := null;
            ParamByName('closed_date').value := null;
            ParamByName('modified_date').value := now;
            ParamByName('active').value := 1;
            ParamByName('assigned_to').value := ASSIGNED_TO;
            ParamByName('regular_hours').value := null;
            ParamByName('overtime_hours').value := null;
            ParamByName('added_by').value := 'PARSER';
            ParamByName('high_profile').value := 0;
            ParamByName('high_profile_reason').value := 0;
            ParamByName('seq_number').value := null;
            ParamByName('assigned_to_id').value := ASSIGNED_TO_ID;
            ParamByName('mark_type').value := null;
            ParamByName('alert').value := null;
            ParamByName('entry_date').value := null;
            ParamByName('gps_id').value := null;
            ParamByName('status_changed_by_id').value := null;
          end;

          try
            If InsertLocate.ExecSQL > 0 then
            begin
              qryLastLocate.Parameters.ParamByName('ticketID').value :=
                TicketID;
              qryLastLocate.open;
              LocateID := qryLastLocate.FieldByName('locate_id').AsInteger;

              spAssignLocate.Parameters.ParamByName('@LocateID').value
                := LocateID;
              spAssignLocate.Parameters.ParamByName('@LocatorID').value :=
                ASSIGN_LOCATE_ID;
              spAssignLocate.Parameters.ParamByName('@AddedBy').value :=
                ADDED_BY_ASSIGN_LOCATE;
              spAssignLocate.Parameters.ParamByName('@WorkloadDate')
                .value := now;
              spAssignLocate.ExecProc;

              LogResult.LogType := ltInfo;
              LogResult.MethodName := 'spAssignLocate';
              LogResult.Status := 'spAssignLocate run ' + TicketNbr +
                '  TicketID: ' + IntToStr(TicketID);
              WriteLog;

            end;
          finally
            qryLastLocate.Close;
          end;

          if btnKill.ModalResult = mrAbort then
          begin
            LogResult.LogType := ltWarning;
            LogResult.MethodName := 'SaveTicket';
            LogResult.Status := 'Stopped due to kill switch';
            LogResult.DataStream := 'Rolling back All Records';
            WriteLog;
            ADOConn.RollbackTrans;
            Showmessage('Stopping!!!');
            exit;
          end;

          ADOConn.CommitTrans;

          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'InsertLocate';
          LogResult.Status := TicketNbr + '  TicketID: ' + IntToStr(TicketID) +
            ' Committed to DB';
          WriteLog;
          inc(cnt);
          StatusBar1.Panels[2].Text := IntToStr(cnt);
          Next;
        except
          On E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.ExcepMsg := 'Transaction error for Premise ID: ' +
              TicketNbr + ' ' + E.Message + ' Transaction cancelled';
            LogResult.Status := 'RollbackTrans';
            WriteLog;
            ADOConn.RollbackTrans;
            inc(fail);
            StatusBar1.Panels[3].Text := IntToStr(fail);

            Next;
            Continue;
          end;
        end; // except
      end; // not EOF
    end; // with qryScannaFile
    Showmessage('All records loaded');
  finally
    qryScannaFile.Close;
  end;
end;

procedure TFrmScannaWorkers.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName :=IncludeTrailingBackslash(LogPath) + 'ScannaWorkers' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TFrmScannaWorkers.FormActivate(Sender: TObject);
begin
  StatusBar1.Panels[4].Text := GetAppVersionStr;
end;

procedure TFrmScannaWorkers.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qryScannaFile.Close;
  FileADOConnection.Close;
  ADOConn.Close;
  Action := caFree;
end;

procedure TFrmScannaWorkers.FormCreate(Sender: TObject);
begin
  AppPath := ExtractFilePath(paramstr(0));
  try
    ProcessINI;
    ConnectDB;
  except
    btnOpen.Enabled := False;
    raise;
  end;
end;

function TFrmScannaWorkers.ProcessINI: Boolean;
var
  iniScannaWorkers: TIniFile;
begin
  try
    iniScannaWorkers := TIniFile.Create(AppPath + 'ScannaWorker.ini');
    aServer := iniScannaWorkers.ReadString('Database', 'Server',
      'SSDS-UTQ-QM-02-DV');
    aDatabase := iniScannaWorkers.ReadString('Database', 'DB', 'QM');
    ausername := iniScannaWorkers.ReadString('Database', 'UserName', '');
    apassword := iniScannaWorkers.ReadString('Database', 'Password', '');
    logPath := iniScannaWorkers.ReadString('LogPaths', 'LogPath', 'E:\QM\Logs\');
  finally
    iniScannaWorkers.Free;
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    LogResult.Status := 'Success';
    WriteLog;

  end;
end;

procedure TFrmScannaWorkers.ConnectDB;
var
  LogConnStr:string;
begin
    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +';Persist Security Info=True;User ID=' + ausername +';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
  LogConnStr:= 'Provider=SQLNCLI11.1;Password=Not shown; Persist Security Info=True; User ID=' + ausername +';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    try
      ADOConn.ConnectionString := Connstr;
      ADOConn.Open;
      StatusBar1.Panels[0].Text := 'Connected to: '+aServer;
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'ConnectDB';
      LogResult.DataStream := LogConnStr;
      LogResult.Status := 'Success';
      WriteLog;
    except
      on E: Exception do
      begin
        StatusBar1.Panels[0].Text := 'Connection failed';
        LogResult.LogType := ltError;
        LogResult.MethodName := 'ConnectDB';
        LogResult.DataStream := LogConnStr;
        LogResult.ExcepMsg:=  E.Message;
        LogResult.Status := 'Failed';
        WriteLog;
        raise exception.Create('Could not connect to database: ' + E.Message);
      end;
    end;

end;

function TFrmScannaWorkers.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure TFrmScannaWorkers.OpenFile;
var
 Connstr: String;
 SheetList: TStringList;
begin
  TotalRecords:=0;
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  OpenDialog1.InitialDir := AppPath;
  openDialog1.Filter := 'Excel files|*.xls*';
  if OpenDialog1.Execute then
  try
    ScannaFile := OpenDialog1.FileName;
    Self.Caption := Self.Caption +'  ---  '+ ExtractFileName(ScannaFile);
    FileADOConnection.Close;
    Connstr := Connstr + OpenDialog1.FileName;
    try
      FileADOConnection.ConnectionString := Connstr;
      FileADOConnection.Open;
      SheetList := TStringlist.Create;
      FileADOConnection.GetTableNames(SheetList);
      qryScannaFile.SQL.Clear;
      qryScannaFile.SQL.Add('Select * from [' + SheetList[0] + ']');
      qryScannaFile.Open;
      TotalRecords:=qryScannaFile.RecordCount;
      StatusBar1.Panels[1].text:= IntToStr(TotalRecords);
    except
      raise;
    end;
 finally
    FreeandNil(SheetList);
    btnSave.Enabled := True;
  end;
end;

end.
