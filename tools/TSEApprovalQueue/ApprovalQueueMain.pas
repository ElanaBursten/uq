unit ApprovalQueueMain;
// QM-831
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, System.Json, System.DateUtils, IniFiles,
  System.Contnrs, System.Math, System.Generics.Collections, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Winapi.Activex,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, IdMessage,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdIntercept, IdLogBase, IdLogEvent, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL;

  {
    "Schema Exception: None is not of type 'integer'\n\nFailed validating 'type' in schema
    ['properties']['Shift']['properties']['WorkerInfo']['properties']['worker_id']

    "Schema Exception: None is not of type 'string'\n\nFailed validating 'type' in schema
    ['properties']['Shift']['properties']['WorkerInfo']['properties']['worker_number']
}


type
        /// <url>element://model:project::TSEApprovalQueue/design:view:::pm77iz3cfq_v</url>
        TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);
        TShiftRec = (AllShift, ShiftStart, ShiftEnd, BreakStart, BreakEnd);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

type
  TShiftType = (Regular, Callout);   //qm-897    ONWRK for Call Out and WRK for Regular

  TfrmApprovalQueue = class(TForm)
    ADOConn: TADOConnection;
    InsRabbitMQout: TADOCommand;
    jsonMemo: TMemo;
    Splitter1: TSplitter;
    pnlBottom: TPanel;
    cbRunOnce: TCheckBox;
    btnjson: TButton;
    StatusBar1: TStatusBar;
    qryTSEWorkTimes: TADOQuery;
    qryTSECalloutTImes: TADOQuery;
    qryTSERegularBreaks: TADOQuery;
    qryTSEApprovalQueue: TADOQuery;
    qryEmpName: TADOQuery;
    qryTSECalloutBreaks: TADOQuery;
    updTSEApprovalQueue: TADOQuery;
    IdSMTP1: TIdSMTP;
    IdMessage1: TIdMessage;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    edtStart: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtStop: TEdit;
    qryLocation: TADOQuery;
    procedure btnjsonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    LogResult: TLogResults;
    flogDir: wideString;

    function BuildJson(EntryID: Integer; ShiftDataset: TADOQuery;
      WorkDate: TDateTime; WorkEmpID: Integer; AShift: TShiftType): String;
    procedure ProcessBreaks(AShift: TShiftType; BreakDataSet: TADOQuery;
      BreaksArray: TJsonArray; EntryID: Integer; WorkDate: TDateTime;
      WorkerNumber: String; WorkerName: String;  ADName: String; Lat,Lng:double;
      out BreakTotal: Double);
    procedure FormatJSON(JSONValue: TJSONValue; OutputStrings: TStrings;
      indent: Integer = 0);
    procedure FormatJsonPair(JSONValue: TJSONPair; OutputStrings: TStrings;
      last: boolean; indent: Integer);
    procedure FormatJsonArray(JSONValue: TJsonArray; OutputStrings: TStrings;
      last: boolean; indent: Integer);
    function connectDB: boolean;
    procedure clearLogRecord;
    procedure WriteLog(LogResult: TLogResults);
    function ProcessINI: boolean;
    procedure ProcessApprovalQueue;
    function GetAppVersionStr: string;
    function DateToISO8601_XQ(const ADate: TDateTime; LocalOffset:integer; AInputIsUTC: Boolean = true): string;  //qm-917
    { Private declarations }
  public
    cntGood, cntBad: Integer;
      LocalUTCoffset :Integer;   //qm-897
    { Public declarations }
  end;

var
  frmApprovalQueue: TfrmApprovalQueue;

implementation

{$R *.dfm}

const
  indent_size = 2;
  QueueName = 'approval_queue';


function TfrmApprovalQueue.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' +
    ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
    aDatabase + ';Data Source=' + aServer;

  jsonMemo.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;
    if (ParamCount > 0) then
    begin
      jsonMemo.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aServer;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
      jsonMemo.Lines.Add('Could not connect to DB');
    end;
  end;
end;

function TfrmApprovalQueue.ProcessINI: boolean;
var
  ApprovalQueueIni: TIniFile;
begin
  try
    ApprovalQueueIni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := ApprovalQueueIni.ReadString('Database', 'Server', '');
    aDatabase := ApprovalQueueIni.ReadString('Database', 'DB', 'QM');
    ausername := ApprovalQueueIni.ReadString('Database', 'UserName', '');
    apassword := ApprovalQueueIni.ReadString('Database', 'Password', '');

    flogDir := ApprovalQueueIni.ReadString('LogPaths', 'LogPath', '');

    with IdMessage1.From do // QM-552
    begin
      Address := ApprovalQueueIni.ReadString('EMailAlerts', 'FromAddress', '');
      Domain  := ApprovalQueueIni.ReadString('EMailAlerts', 'FromDomain', '');
      Name    := ApprovalQueueIni.ReadString('EMailAlerts', 'FromName', '');
      Text    := ApprovalQueueIni.ReadString('EMailAlerts', 'FromText', '');
      User    := ApprovalQueueIni.ReadString('EMailAlerts', 'FromUser', '');
    end;


    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);
  finally
    ApprovalQueueIni.Free;
  end;
end;

procedure TfrmApprovalQueue.btnjsonClick(Sender: TObject);
begin
//   CoInitialize(nil);
//   TThread.CreateAnonymousThread(
//   procedure
//   begin
     ProcessApprovalQueue;
//   end).Start;
end;

procedure TfrmApprovalQueue.ProcessApprovalQueue;
var
  JsonStr: String;
  EntryID: Integer;
  WorkDate: TDateTime;
  WorkEmpID: Integer;
  // testdate: String;
  iAffectedRows: Integer;
  RunOnce: boolean;
  Shift: String;

  function InsertPayload(JsonStr: String): boolean;
  var
    iAffectedRows: Integer;
  begin
    Result := True;
    InsRabbitMQout.Parameters.ParamByName('queue').Value := QueueName;
    InsRabbitMQout.Parameters.ParamByName('payload').Value := JsonStr;
    InsRabbitMQout.Execute(iAffectedRows, EmptyParam);
    if iAffectedRows = 0 then
      Result := False;
  end;

begin
  cntGood := 0;
  cntBad := 0;
  idMessage1.ClearBody;
  qryTSEApprovalQueue.open;
  edtStart.Text :=TimeToStr(time);
  With qryTSEApprovalQueue do
  begin
    while not EOF do
    begin
      WorkEmpID:=0;
      EntryID:=0;
      try
        EntryID := FieldByName('TSE_Entry_id').AsInteger;
        WorkDate := FieldByName('WorkDate').AsDateTime;
        WorkEmpID := FieldByName('work_emp_id').AsInteger;
        RunOnce := True;
        if FieldByName('QR').AsBoolean then
        begin
          Shift := 'Regular';
          JsonStr := BuildJson(EntryID, qryTSEWorkTimes, WorkDate, WorkEmpID,
            Regular);
          If not InsertPayload(JsonStr) then
            raise Exception.Create('Error in insert: ');
          if FieldByName('QC').AsBoolean then
            RunOnce := False;
        end; //if FieldByName('QR').AsBoolean then
        if FieldByName('QC').AsBoolean then
        begin
          Shift := 'Callout';
          JsonStr := BuildJson(EntryID, qryTSECalloutTImes, WorkDate,
            WorkEmpID, Callout);
          If not InsertPayload(JsonStr) then
            raise Exception.Create('Error in insert: ');
          RunOnce := True;
        end; // if FieldByName('QC').AsBoolean then
        inc(cntGood);
        With updTSEApprovalQueue do
        begin
          Parameters.ParamByName('Status').Value := 'POSTED';
          Parameters.ParamByName('tseEntryID').Value :=  EntryID;
          Parameters.ParamByName('note').Value := '';
          ExecSQL;
        end;
        StatusBar1.Panels[3].Text := InttoStr(cntGood);
        StatusBar1.Panels[5].Text := InttoStr(cntBad);
        StatusBar1.Panels[7].Text := InttoStr(cntBad + cntGood);
        StatusBar1.Refresh;
      except
        On E: Exception do
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'ProcessApprovalQueue';
          LogResult.ExcepMsg := 'Error in processing Timesheet ID:  ' +
            InttoStr(EntryID) + ', Emp ID: ' + InttoStr(WorkEmpID) +
            ', Shift Type: ' + Shift + ', Work Date: ' +
            FormatDateTime('yyyy-mm-dd', WorkDate) + ' ' + E.Message;
          LogResult.Status := 'Failed';
          WriteLog(LogResult);
          inc(cntBad);
          With UpdTSEApprovalQueue do
          begin
            Parameters.ParamByName('Status').Value := 'FAILED';
            Parameters.ParamByName('tseEntryID').Value :=  EntryID;
            Parameters.ParamByName('note').Value := E.Message;
            IdMessage1.Body.Add('Failed TSE Process for entry id: '+IntToStr(EntryID)+' Error:'+ E.Message+' for: '+IntToStr(WorkEmpID));
            ExecSQL;
          end;
          if cbRunOnce.Checked and RunOnce then
            break;

          Next;
          Continue;
        end; //On E: Exception do
      end; //try-except
      if cbRunOnce.Checked and RunOnce then
        break;
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'ProcessApprovalQueue';
      LogResult.DataStream := 'Successfully processed: Timesheet ID:  ' +
        InttoStr(EntryID) + ', Emp ID: ' + InttoStr(WorkEmpID) +
        ', Shift Type: ' + Shift + ', Work Date: ' +
        FormatDateTime('yyyy-mm-dd', WorkDate);
      LogResult.Status := 'Succeeded';
      WriteLog(LogResult);
      Next;
    end; //while not EOF do
    close;
  end; //With qryTSEApprovalQueue do
  edtStop.Text :=TimeToStr(time);
//  CoUnInitialize;
  If not(idMessage1.IsBodyEmpty) then
  begin
    idSmtp1.connect;
    idSmtp1.Send(idMessage1);
    idSmtp1.Disconnect;
  end;
  StatusBar1.Panels[3].Text := InttoStr(cntGood);
  StatusBar1.Panels[5].Text := InttoStr(cntBad);
  StatusBar1.Panels[7].Text := InttoStr(cntBad + cntGood);
  jsonMemo.Lines.Add('Process finished');
  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'ProcessApprovalQueue';
  LogResult.Status := 'Process finished';
  LogResult.DataStream := InttoStr(cntGood) + ' successfully posted: ' +
    InttoStr(cntBad) + ' failed of ' + InttoStr(cntBad + cntGood);
  WriteLog(LogResult);
  jsonMemo.Lines.Add('');
  jsonMemo.Lines.Add('---------------------------------');
  jsonMemo.Lines.Add(LogResult.DataStream);
end;

procedure TfrmApprovalQueue.ProcessBreaks(AShift: TShiftType;
  BreakDataSet: TADOQuery; BreaksArray: TJsonArray; EntryID: Integer;
  WorkDate: TDateTime; WorkerNumber: String; WorkerName: String;  ADName: String;
  Lat,Lng:double;   //qm-1068 sr
  out BreakTotal: Double);
var
  BreakObject: TJsonObject;
  BreakStartObj, BreakEndObj: TJsonObject;
  LatLongObj: TJsonObject;
  AdjustmentsArr: TJsonArray;
begin

  With BreakDataSet do
  begin
    Parameters.ParamByName('EntryID').Value := EntryID;
    Parameters.ParamByName('WorkDate').Value := WorkDate;
    open;
    First;
    if not EOF then
      BreakTotal := RoundTo(FieldByName('Total').AsFloat, -2);
    while not EOF do
    begin
      BreakObject := TJsonObject.Create;
      BreaksArray.Add(BreakObject);
      BreakStartObj := TJsonObject.Create;
      With BreakStartObj do
      begin
        if AShift = Regular then
        begin
          AddPair('EventId', EntryID);
          AddPair('EventTime', DateToISO8601_XQ(FieldByName('WorkStart').AsDateTime, LocalUTCoffset, False))  //qm-917
        end
        else if AShift = Callout then
        begin
          AddPair('EventId', EntryID);     //removed QC
          AddPair('EventTime', DateToISO8601_XQ(FieldByName('CalloutStart').AsDateTime, LocalUTCoffset, False));  //qm-917
        end;
        AddPair('EventType', 'Start_Break');
        AdjustmentsArr := TJsonArray.Create;
        AddPair('Adjustments', AdjustmentsArr);
        AddPair('WorkerNumber', WorkerNumber);
        AddPair('CreatedBy', ADName); //qm-897
        AddPair('CreatedByWorkerNumber', WorkerNumber);
        LatLongObj := TJsonObject.Create;

        if Lat>0.0 then
        LatLongObj.AddPair('Latitude', Lat)//qm-1068 sr    TJsonNull.Create
        else LatLongObj.AddPair('Latitude', TJsonNull.Create);
        if Lng>0.0 then
        LatLongObj.AddPair('Longitude', Lng)
        else LatLongObj.AddPair('Longitude', TJsonNull.Create);//qm-1068 sr

//        LatLongObj.AddPair('Latitude', Lat);    //qm-1068 sr
//        LatLongObj.AddPair('Longitude', Lng);     //qm-1068 sr
        AddPair('DeviceLocation', LatLongObj);

        AddPair('LateEntryFlag', TJsonBool.Create(False));
        AddPair('Acknowledgement', TJsonNull.Create);
        AddPair('EditReasonCode', TJsonNull.Create);
        AddPair('EditReasonStatement', TJsonNull.Create);
        AddPair('EditReasonShortDescription', TJsonNull.Create);
      end;
      BreakObject.AddPair('BreakStart', BreakStartObj);

      BreakEndObj := TJsonObject.Create;
      With BreakEndObj do
      begin
        if AShift = Regular then
        begin
          AddPair('EventId', (EntryID));
          AddPair('EventTime', DateToISO8601_XQ(FieldByName('WorkStop').AsDateTime, LocalUTCoffset, False))  //qm-917
        end
        else if AShift = Callout then
        begin
          AddPair('EventId', EntryID); //removed QC  3/5
          AddPair('EventTime', DateToISO8601_XQ(FieldByName('CalloutStop').AsDateTime, LocalUTCoffset, False));   //qm-917
        end;
        AddPair('EventType', 'End_Break');
        AdjustmentsArr := TJsonArray.Create;
        AddPair('Adjustments', AdjustmentsArr);
        AddPair('WorkerNumber', WorkerNumber);
        AddPair('CreatedBy', ADName);
        AddPair('CreatedByWorkerNumber', WorkerNumber);
        LatLongObj := TJsonObject.Create;

        AddPair('DeviceLocation', LatLongObj);

        if Lat>0.0 then
        LatLongObj.AddPair('Latitude', Lat)//qm-1068 sr    TJsonNull.Create
        else LatLongObj.AddPair('Latitude', TJsonNull.Create);
        if Lng>0.0 then
        LatLongObj.AddPair('Longitude', Lng)
        else LatLongObj.AddPair('Longitude', TJsonNull.Create);//qm-1068 sr


//        LatLongObj.AddPair('Latitude',  Lat);    //qm-1068 sr
//        LatLongObj.AddPair('Longitude',  Lng);    //qm-1068 sr
        AddPair('LateEntryFlag', TJsonBool.Create(false));
        AddPair('Acknowledgement', TJsonNull.Create);
        AddPair('EditReasonCode', TJsonNull.Create);
        AddPair('EditReasonStatement', TJsonNull.Create);
        AddPair('EditReasonShortDescription', TJsonNull.Create);
      end;
      BreakObject.AddPair('BreakEnd', BreakEndObj);
      Next;
    end;
    close;
  end;
end;

function TfrmApprovalQueue.BuildJson(EntryID: Integer; ShiftDataset: TADOQuery;
  WorkDate: TDateTime; WorkEmpID: Integer; AShift: TShiftType): String;
var
  RootObject: TJsonObject;
  DataObject: TJsonObject;
  ShiftObject: TJsonObject;
  ShiftEndObject: TJsonObject;
  ShiftStartObject: TJsonObject;
  LatLongObj: TJsonObject;
  WorkerInfoObj: TJsonObject;
  ShiftStart, ShiftEnd: TDateTime;
  TotalWorkHours: Double;
  WorkerName: String;
  FirstName: String;
  LastName: String;
  WorkerNumber: String;
  CompanyNumber: string;
  CompanyID:Integer;
  EmpType: String;   //qm-897
  WorkerStatusCode:String;
  BreaksArray: TJsonArray;
  BreakTotal: Double;
  PerDiemObject: TJsonObject;
  PerDiemTypeObject: TJsonObject;
  CurrentPerDiemTypeObject: TJsonObject;
  AdjustmentsArr: TJsonArray;
  TicketsArr: TJsonArray;
  ReviewHistoryArr: TJsonArray;
  ShiftTypeObject: TJsonObject;
  VariablesObject: TJsonObject;
  EmpIdErr: boolean;
  ADName:string;
  ActLng, ActLat:double;
//  LocalUTCoffset:integer;    //qm-897
begin
  EmpIdErr := False;
  qryEmpName.Parameters.ParamByName('emp_id').Value := WorkEmpID;
  qryEmpName.open;

  WorkerName := '';
  WorkerNumber := '';
  FirstName := '';
  LastName :='';
  CompanyNumber :='';
  CompanyID:=0;
  EmpType := '';     //qm-897
  ADName :='';       //qm-897
  WorkerStatusCode:='';
  LocalUTCoffset:=0;
  ActLng :=0.0;   //qm-1068 sr
  ActLat :=0.0;   //qm-1068 sr
  if not qryEmpName.IsEmpty then
  begin
    WorkerName := qryEmpName.FieldByName('name').AsString;
    WorkerNumber := qryEmpName.FieldByName('WorkerNumber').AsString;
    FirstName := qryEmpName.FieldByName('First_Name').AsString;
    LastName := qryEmpName.FieldByName('Last_Name').AsString;
    EmpType := qryEmpName.FieldByName('EmpType').AsString;   //qm-897
    CompanyID := qryEmpName.FieldByName('Company_ID').AsInteger;   //qm-897
    ADName :=  qryEmpName.FieldByName('ad_username').AsString;   //qm-897
    LocalUTCoffset:= qryEmpName.FieldByName('LocalUTCoffset').AsInteger;   //qm-897

    if CompanyID =1 then  //qm-897
    CompanyNumber:= 'UTQ' //qm-897
    else if CompanyID =7   //qm-897
    then CompanyNumber:= 'LOC'  //qm-897
    else CompanyNumber:=  qryEmpName.FieldByName('CompanyNo').AsString;   //qm-897

    //qryEmpName.FieldByName('CompanyNumber').AsString;
    WorkerStatusCode:= '1';//qryEmpName.FieldByName('WorkerStatusCode').AsString;
    qryEmpName.close;
  end
  else
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'BuildJson';
    LogResult.Status := 'EntryId: ' + InttoStr(EntryID) + ': empID:' +
      InttoStr(WorkEmpID) + ' not found';;
    WriteLog(LogResult);
    qryEmpName.close;
    EmpIdErr := True;
  end;

  try
    qryLocation.Parameters.ParamByName('empID').Value := WorkEmpID; //qm-1068 sr
    qryLocation.Parameters.ParamByName('WorkDate').Value := DateOf(WorkDate); //qm-1068 sr
    qryLocation.Open; //qm-1068 sr
    if not (qryLocation.IsEmpty) then
    begin
      ActLng :=qryLocation.FieldByName('lat').AsFloat; //qm-1068 sr
      ActLat :=qryLocation.FieldByName('lng').AsFloat; //qm-1068 sr
    end;

  finally
    qryLocation.Close;
  end;

  With ShiftDataset do
  begin
    Parameters.ParamByName('EntryID').Value := EntryID;
    Parameters.ParamByName('WorkDate').Value := WorkDate;
    open;
    First;
    if AShift = Regular then
      ShiftStart := FieldByName('WorkStart').AsDateTime
    else if AShift = Callout then
      ShiftStart := FieldByName('CalloutStart').AsDateTime;
    last;
    if AShift = Regular then
      ShiftEnd := FieldByName('WorkStop').AsDateTime
    else if AShift = Callout then
      ShiftEnd := FieldByName('CalloutStop').AsDateTime;
    TotalWorkHours := RoundTo(FieldByName('Total').AsFloat, -2);
    close;
  end; // With ShiftDataset do

  RootObject := TJsonObject.Create();
  RootObject.Owned := True;
  With RootObject do
  begin
    AddPair('emitting_context', 'wa.utq');
    AddPair('event_type', 'Approved_Shift');
  end;

  DataObject := TJsonObject.Create();
  RootObject.AddPair('Data', DataObject);
  ShiftObject := TJsonObject.Create();

  With ShiftObject do
  begin
    AddPair('ShiftDate', FormatDateTime('yyyy-mm-dd', WorkDate));
    If AShift = Regular then
      AddPair('ShiftId', 'QR' + InttoStr(EntryID))
    else If AShift = Callout then
      AddPair('ShiftId', 'QC' + InttoStr(EntryID));
    AddPair('WorkTotal', TJsonNumber.Create(TotalWorkHours));
    AddPair('WorkerNumber', WorkerNumber);
  end; //With ShiftObject do
  DataObject.AddPair('Shift', ShiftObject);

  ShiftEndObject := TJsonObject.Create();
  With ShiftEndObject do
  begin
    If AShift = Regular then
      AddPair('ShiftId', 'QR' + InttoStr(EntryID))
    else If AShift = Callout then
      AddPair('ShiftId', 'QC' + InttoStr(EntryID));

    AddPair('EventTime', DateToISO8601_XQ(ShiftEnd, LocalUTCoffset, False));   //qm-917
    AddPair('EventType', 'End_Shift');
    AdjustmentsArr := TJsonArray.Create;
    AddPair('Adjustments', AdjustmentsArr);
    AddPair('WorkerNumber', WorkerNumber);
    AddPair('CreatedBy', ADName);
    AddPair('CreatedByWorkerNumber', WorkerNumber);

    LatLongObj := TJsonObject.Create;
    AddPair('DeviceLocation', LatLongObj);
    if ActLat>0.0 then
    LatLongObj.AddPair('Latitude', ActLat)//qm-1068 sr    TJsonNull.Create
    else LatLongObj.AddPair('Latitude', TJsonNull.Create);
    if ActLng>0.0 then
    LatLongObj.AddPair('Longitude', ActLng)
    else LatLongObj.AddPair('Longitude', TJsonNull.Create);//qm-1068 sr

    AddPair('LateEntryFlag', TJsonBool.Create(False));
    AddPair('Acknowledgement', TJsonNull.Create);
    AddPair('EditReasonCode', TJsonNull.Create);
    AddPair('EditReasonStatement', TJsonNull.Create);
    AddPair('EditReasonShortDescription', TJsonNull.Create);
  end;//With ShiftEndObject do
  ShiftObject.AddPair('ShiftEnd', ShiftEndObject);

  ShiftStartObject := TJsonObject.Create();
  With ShiftStartObject do
  begin
    If AShift = Regular then
      AddPair('ShiftId', 'QR' + InttoStr(EntryID))
    else If AShift = Callout then
      AddPair('ShiftId', 'QC' + InttoStr(EntryID));
    AddPair('EventTime', DateToISO8601_XQ(ShiftStart, LocalUTCoffset, False));   //qm-917
    AddPair('EventType', 'Start_Shift');
    AdjustmentsArr := TJsonArray.Create;
    AddPair('Adjustments', AdjustmentsArr);
    AddPair('WorkerNumber', WorkerNumber);
    AddPair('CreatedBy', ADName);
    AddPair('CreatedByWorkerNumber', WorkerNumber);

    LatLongObj := TJsonObject.Create;
    AddPair('DeviceLocation', LatLongObj);
    AddPair('DeviceLocation', LatLongObj);

    if ActLat>0.0 then
    LatLongObj.AddPair('Latitude', ActLat)//qm-1068 sr    TJsonNull.Create
    else LatLongObj.AddPair('Latitude', TJsonNull.Create);
    if ActLng>0.0 then
    LatLongObj.AddPair('Longitude', ActLng)
    else LatLongObj.AddPair('Longitude', TJsonNull.Create);//qm-1068 sr

//    LatLongObj.AddPair('Latitude', ActLat); //qm-1068 sr
//    LatLongObj.AddPair('Longitude', ActLng);//qm-1068 sr
    AddPair('LateEntryFlag', TJsonBool.Create(false));
    AddPair('Acknowledgement', TJsonNull.Create);
    AddPair('EditReasonCode', TJsonNull.Create);
    AddPair('EditReasonStatement', TJsonNull.Create);
    AddPair('EditReasonShortDescription', TJsonNull.Create);
  end;//With ShiftStartObject do
  ShiftObject.AddPair('ShiftStart', ShiftStartObject);

  BreaksArray := TJsonArray.Create;
  ShiftObject.AddPair('Breaks', BreaksArray);

  if AShift = Regular then
    ProcessBreaks(Regular, qryTSERegularBreaks, BreaksArray, EntryID, WorkDate,
      WorkerNumber, WorkerName,ADName, BreakTotal, ActLat,ActLng)  //qm-1068 sr
  else
    ProcessBreaks(Callout, qryTSECalloutBreaks, BreaksArray, EntryID, WorkDate,
      WorkerNumber, WorkerName, ADName, BreakTotal, ActLat,ActLng);  //qm-1068 sr
  ShiftObject.AddPair('BreakTotal', TJsonNumber.Create(BreakTotal));

  WorkerInfoObj := TJsonObject.Create;
  With WorkerInfoObj do
  begin
    AddPair('worker_id', TJsonNumber.Create(0));
    AddPair('worker_number', WorkerNumber);
    AddPair('first_name', TJsonNull.Create);
    AddPair('last_name', TJsonNull.Create);
    AddPair('worker_display_name', WorkerName);
    AddPair('sol_emp_id', TJsonNull.Create);
    AddPair('company_code', TJsonNull.Create);      //QM-828  CompanyNumber
    AddPair('worker_type_code', TJsonNull.Create);
    AddPair('status_code', TJsonNull.Create);    //QM-828  WorkerStatusCode
    AddPair('state', TJsonNull.Create);
    AddPair('emp_type', EmpType);  //qm-897
  end; //With WorkerInfoObj do
  ShiftObject.AddPair('WorkerInfo', WorkerInfoObj);

  ShiftObject.AddPair('PreviousShiftEndTimestamp', TJsonNull.Create);
  ShiftObject.AddPair('NextShiftStartTimestamp', TJsonNull.Create);
  ShiftObject.AddPair('EventTimeUTCOffset', TJsonNumber.Create(LocalUTCoffset));  //qm-897 sr was -4

  TicketsArr := TJsonArray.Create;
  DataObject.AddPair('Tickets', TicketsArr);
  DataObject.AddPair('subsidiary_code', CompanyNumber);

  PerDiemObject := TJsonObject.Create;
  With PerDiemObject do
  begin
    AddPair('per_diem_id', TJsonNumber.Create(2200222));
    AddPair('worker_id', TJsonNumber.Create(0));
    AddPair('per_diem_type_id', TJsonNumber.Create(1));

    PerDiemTypeObject := TJsonObject.Create;
    PerDiemTypeObject.AddPair('per_diem_type_id', TJsonNumber.Create(1));
    PerDiemTypeObject.AddPair('name', 'None');
    PerDiemTypeObject.AddPair('description', 'Not Qualified');
    AddPair('per_diem_type', PerDiemTypeObject);

    AddPair('per_diem_date', FormatDateTime('yyyy-mm-dd', WorkDate));
    AddPair('approved_flag', TJsonBool.Create(False));
    AddPair('exported_flag', TJsonBool.Create(False));

    CurrentPerDiemTypeObject := TJsonObject.Create;
    CurrentPerDiemTypeObject.AddPair('per_diem_type_id', TJsonNumber.Create(1));
    CurrentPerDiemTypeObject.AddPair('name', 'None');
    CurrentPerDiemTypeObject.AddPair('description', 'Not Qualified');
    AddPair('current_per_diem_type', CurrentPerDiemTypeObject);

    AdjustmentsArr := TJsonArray.Create;
    AddPair('Adjustments', AdjustmentsArr);

    AddPair('create_username', 'Q1 Service');
    AddPair('create_timestamp', DateToISO8601(WorkDate));
    AddPair('last_update_username', 'Q1 Service');
    AddPair('last_update_timestamp', DateToISO8601(WorkDate));

    ReviewHistoryArr := TJsonArray.Create;
    AddPair('review_history', ReviewHistoryArr);
  end;//With PerDiemObject do
  DataObject.AddPair('per_diem', PerDiemObject);

  ShiftTypeObject := TJsonObject.Create;
  With ShiftTypeObject do        //qm-897    ONWRK for Call Out and WRK for Regular
  begin
    AddPair('shift_type_id', TJsonNumber.Create(1));
    if AShift = Regular then
      AddPair('name', 'WRK')  //qm-897  WRK for Regular
    else if AShift = Callout then
      AddPair('name', 'ONWRK');    //qm-897    ONWRK for Call Out

    AddPair('default_type_flag', TJsonBool.Create(True));
  end; //With ShiftTypeObject do
  DataObject.AddPair('shift_type', ShiftTypeObject);

  DataObject.AddPair('event_type', 'Shift_Processed');
  DataObject.AddPair('generated_timestamp', DateToISO8601(WorkDate));

  VariablesObject := TJsonObject.Create;
  With VariablesObject do
  begin
    AddPair('worker_company', CompanyNumber);
    AddPair('event_type', 'Approved_Shift');
  end;
  RootObject.AddPair('variables', VariablesObject);

  RootObject.AddPair('exchange', 'event.v1');
  RootObject.AddPair('routing_key', 'event.v1.wa.utq.approved_shift');
  Result := RootObject.ToString;
  FormatJSON(RootObject, jsonMemo.Lines);
  jsonMemo.Lines.Add('');
  jsonMemo.Lines.Add('---------------------------------');
  jsonMemo.Lines.Add('');
  RootObject.Free;
  if EmpIdErr then
    raise Exception.Create('Employee not found');
end;//With VariablesObject do

procedure TfrmApprovalQueue.FormatJSON(JSONValue: TJSONValue;
  OutputStrings: TStrings; indent: Integer = 0);
var
  I: Integer;
begin
  if JSONValue is TJsonObject then
  begin
    OutputStrings.Add(StringOfChar(' ', indent * indent_size) + '{');
    for I := 0 to TJsonObject(JSONValue).Count - 1 do
      FormatJsonPair(TJsonObject(JSONValue).Pairs[I], OutputStrings,
        I = TJsonObject(JSONValue).Count - 1, indent + 1);
    OutputStrings.Add(StringOfChar(' ', indent * indent_size) + '}');
  end
  else if JSONValue is TJsonArray then
    FormatJsonArray(TJsonArray(JSONValue), OutputStrings,
      I = TJsonObject(JSONValue).Count - 1, indent + 1)
  else
    OutputStrings.Add(StringOfChar(' ', indent * indent_size) +
      JSONValue.ToString);
end;

procedure TfrmApprovalQueue.FormatJsonArray(JSONValue: TJsonArray;
  OutputStrings: TStrings; last: boolean; indent: Integer);
var
  I: Integer;
begin
  OutputStrings.Add(StringOfChar(' ', indent * indent_size) + '[');
  for I := 0 to JSONValue.Count - 1 do
  begin
    FormatJSON(JSONValue.Items[I], OutputStrings, indent + 1);
    if I < JSONValue.Count - 1 then
      OutputStrings[OutputStrings.Count - 1] :=
        OutputStrings[OutputStrings.Count - 1] + ',';
  end;
  OutputStrings.Add(StringOfChar(' ', indent * indent_size) + ']');
end;

procedure TfrmApprovalQueue.FormatJsonPair(JSONValue: TJSONPair;
  OutputStrings: TStrings; last: boolean; indent: Integer);
const
  Template = '%s : %s';
var
  line: string;
  newList: TStringList;
begin
  newList := TStringList.Create;
  try
    FormatJSON(JSONValue.JSONValue, newList, indent);
    line := format(Template, [JSONValue.JsonString.ToString,
      Trim(newList.Text)]);
  finally
    newList.Free;
  end;

  line := StringOfChar(' ', indent * indent_size) + line;
  if not last then
    line := line + ',';
  OutputStrings.Add(line);
end;

procedure TfrmApprovalQueue.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  flogDir := 'C:\';
  ProcessINI;
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  StatusBar1.Panels[9].Text := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

//  ProcessINI;
  connectDB;

  if uppercase(ParamStr(1)) <> 'GUI' then
  begin
    ProcessApprovalQueue;
    application.Terminate;
  end;
end;

procedure TfrmApprovalQueue.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmApprovalQueue.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + ExtractFileName(Application.ExeName) + '_' +
    FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

function TfrmApprovalQueue.DateToISO8601_XQ(const ADate: TDateTime; LocalOffset:integer; AInputIsUTC: Boolean = true): string;  //qm-917
const   //qm-917
  SDateFormat: string = '%.4d-%.2d-%.2dT%.2d:%.2d:%.2d.%.3dZ'; { Do not localize }
  SOffsetFormat: string = '%s%s%.02d:%.02d'; { Do not localize }
  Neg: array[Boolean] of string = ('+', ''); { Do not localize }
var
  y, mo, d, h, mi, se, ms: Word;
  Bias: Integer;
  TimeZone: TTimeZone;
begin
  DecodeDate(ADate, y, mo, d);
  DecodeTime(ADate, h, mi, se, ms);
  Result := Format(SDateFormat, [y, mo, d, h, mi, se, ms]);
  if not AInputIsUTC then
  begin
    TimeZone := TTimeZone.Local;
    Bias := LocalOffset*60;
    if Bias <> 0 then
    begin
      // Remove the Z, in order to add the UTC_Offset to the string.
      SetLength(Result, Result.Length - 1);
      Result := Format(SOffsetFormat, [Result, Neg[Bias < 0], Bias div MinsPerHour,
        Bias mod MinsPerHour]);
    end
  end;
end;

function TfrmApprovalQueue.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;



end.
