program TSEApprovalQueue;
//QM-816 - Epic
//QM-831 - Delphi App
//QM-830 - UDFs
//QM-827 - Tables/Triggers
uses
  Vcl.Forms,
  SysUtils,
  ApprovalQueueMain in 'ApprovalQueueMain.pas' {frmApprovalQueue},
  GlobalSU in 'GlobalSU.pas';

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
//  LocalInstance:=MyInstanceName;
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1) = 'GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm := True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm := False;
    end;
    Application.Title := '';

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmApprovalQueue, frmApprovalQueue);
  Application.Run;
  end
  else
    Application.Terminate;
end.


