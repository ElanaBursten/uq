object frmApprovalQueue: TfrmApprovalQueue
  Left = 0
  Top = 0
  Caption = 'Approval Queue'
  ClientHeight = 395
  ClientWidth = 585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 363
    Width = 628
    Height = 11
    Align = alNone
  end
  object jsonMemo: TMemo
    Left = 0
    Top = 0
    Width = 585
    Height = 326
    Align = alTop
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 326
    Width = 585
    Height = 69
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 97
      Top = 18
      Width = 46
      Height = 13
      Caption = 'TimeStart'
    end
    object Label2: TLabel
      Left = 239
      Top = 18
      Width = 44
      Height = 13
      Caption = 'TimeStop'
    end
    object cbRunOnce: TCheckBox
      Left = 509
      Top = 6
      Width = 97
      Height = 17
      Caption = 'Run Once'
      TabOrder = 0
    end
    object btnjson: TButton
      Left = 16
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Build Json'
      TabOrder = 1
      OnClick = btnjsonClick
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 49
      Width = 583
      Height = 19
      Panels = <
        item
          Text = 'Connected to'
          Width = 80
        end
        item
          Text = 'Not Connected'
          Width = 130
        end
        item
          Text = 'Success'
          Width = 50
        end
        item
          Width = 30
        end
        item
          Text = 'Failed'
          Width = 35
        end
        item
          Width = 30
        end
        item
          Text = 'Total'
          Width = 38
        end
        item
          Width = 30
        end
        item
          Text = 'ver'
          Width = 23
        end
        item
          Width = 50
        end>
    end
    object edtStart: TEdit
      Left = 149
      Top = 10
      Width = 84
      Height = 21
      TabOrder = 3
    end
    object edtStop: TEdit
      Left = 289
      Top = 10
      Width = 88
      Height = 21
      TabOrder = 4
    end
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 8
  end
  object InsRabbitMQout: TADOCommand
    CommandText = 
      'Insert into rabbitmq_out (queue_name, message_payload) '#13#10'values(' +
      ':queue, :payload)'
    Connection = ADOConn
    Parameters = <
      item
        Name = 'queue'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 255
        Value = Null
      end
      item
        Name = 'payload'
        Attributes = [paNullable, paLong]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 1073741823
        Value = Null
      end>
    Left = 88
    Top = 224
  end
  object qryTSEWorkTimes: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'EntryID'
        Size = -1
        Value = Null
      end
      item
        Name = 'workdate'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select  *, SUM(hours) OVER () AS Total '
      'from  getWorkDatesForTimeSheetEntry (:EntryID, :WorkDate)'
      'where TypeTime = '#39'Regular'#39)
    Left = 248
    Top = 72
  end
  object qryTSECalloutTImes: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'EntryID'
        Size = -1
        Value = Null
      end
      item
        Name = 'workdate'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select  *, SUM(hours) OVER () AS Total from  '
      'getCallOutsForTimeSheetEntry (:EntryID, :workdate)'
      'where TypeTime = '#39'Callout'#39)
    Left = 88
    Top = 72
  end
  object qryTSERegularBreaks: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'EntryID'
        Size = -1
        Value = Null
      end
      item
        Name = 'workdate'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select  *, SUM(hours) OVER () AS Total '
      'from getWorkDatesForTimeSheetEntry (:EntryID, :WorkDate)'
      'where TypeTime = '#39'Break'#39)
    Left = 248
    Top = 136
  end
  object qryTSEApprovalQueue: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'Select * '
      'from TSE_ApprovalQueue'
      'where Status = '#39'NEW'#39)
    Left = 160
    Top = 8
  end
  object qryEmpName: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'Use QM;'
      ''
      
        'select e.First_Name, e.Last_Name, e.First_Name+'#39' '#39'+e.Last_Name a' +
        's Name,'
      'qm.dbo.udfLeftSQLPadding(e.emp_number,6,0) as WorkerNumber,'
      ' r.code as EmpType, e.company_id,LC.name as CompanyNo,'
      
        'e.emp_id, ad_username, ((local_utc_bias/60-1) *-1) as LocalUTCof' +
        'fset'
      'from employee E'
      
        'left outer join locating_company LC on (lc.company_id=E.company_' +
        'id)'
      'join reference r on r.ref_id = e.type_id'
      'where emp_id = :emp_id'
      '')
    Left = 200
    Top = 208
  end
  object qryTSECalloutBreaks: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'EntryID'
        Size = -1
        Value = Null
      end
      item
        Name = 'workdate'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select  *, SUM(hours) OVER () AS Total'
      'from getCallOutsForTimeSheetEntry  (:EntryID, :WorkDate)'
      'where TypeTime = '#39'Break'#39)
    Left = 88
    Top = 136
  end
  object updTSEApprovalQueue: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'Status'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'tseEntryID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update TSE_ApprovalQueue'
      'set status = :Status, note = :note '
      'where TSE_Entry_Id= :tseEntryID')
    Left = 312
    Top = 208
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    Left = 440
    Top = 88
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
      end>
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <
      item
        Address = 'qmanagerdev@utiliquest.com'
        Name = 'QManager Managers'
        Text = 'QManager Managers <qmanagerdev@utiliquest.com>'
        Domain = 'utiliquest.com'
        User = 'qmanagerdev'
      end>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 512
    Top = 8
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 295
  end
  object IdLogEvent1: TIdLogEvent
    Left = 424
  end
  object qryLocation: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'WorkDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'empID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'Use QM;'
      ''
      'select top 1 *'
      'from [dbo].[employee_activity] EA'
      'where  (cast(EA.activity_date as date)=:WorkDate) '
      'and (EA.lat is not null and EA.lat<>0.000000) '
      'and (EA.lng is not null and EA.lat<>0.000000) '
      'and EA.emp_id = :empID'
      'order by [emp_activity_id] '
      '')
    Left = 200
    Top = 264
  end
end
