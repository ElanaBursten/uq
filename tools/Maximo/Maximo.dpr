program Maximo;
//QM-643 Maximo   SR

{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  MaximoMain in 'MaximoMain.pas' {frmMaximoResponder},
  GlobalSU in 'GlobalSU.pas';

var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName)+'_'+ParamStr(2);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(1)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.Title := '';
  Application.CreateForm(TfrmMaximoResponder, frmMaximoResponder);
  Application.Run;
  end
  else
    Application.Terminate;
end.
