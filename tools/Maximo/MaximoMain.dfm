object frmMaximoResponder: TfrmMaximoResponder
  Left = 0
  Top = 0
  Caption = 'Maximo maximus'
  ClientHeight = 402
  ClientWidth = 731
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 731
    Height = 346
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 0
    OnDblClick = Memo1DblClick
  end
  object btnSendResponse: TButton
    Left = 304
    Top = 352
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 1
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 383
    Width = 731
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'Not Connected'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=sa;Init' +
      'ial Catalog=QM;Data Source=DESKTOP-5P21N7F\SQLEXPRESS;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 64
    Top = 256
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 128
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'respond_to'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to = :respond_to')
    Left = 480
    Top = 192
  end
  object RESTClientTicket: TRESTClient
    BaseURL = 
      'https://apigw-pod1.dm-us.informaticacloud.com/t/3bdp8kvlz7lcd3x3' +
      'm8rws8.com/PF-Utiliquest-MX-WorkOrder-Publisher'
    Params = <>
    SecureProtocols = [TLS11, TLS12]
    SynchronizedEvents = False
    OnAuthEvent = RESTClientTicketAuthEvent
    Left = 584
    Top = 16
  end
  object RESTRequestTicket: TRESTRequest
    AssignedValues = [rvConnectTimeout, rvReadTimeout]
    Client = RESTClientTicket
    Method = rmPOST
    Params = <
      item
        Kind = pkHTTPHEADER
        Name = 'Authorization'
        Options = [poDoNotEncode]
        Value = 
          'Bearer eyJraWQiOiI1TWpFV3pTUlJjRWtkcXk2dEZUSlFLIiwidHlwIjoiSldUI' +
          'iwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3Tm1Ydm43VUs0bGNvdjNxV09lRjIxIi' +
          'wiYXVkIjoiQVBJTSIsIm9yZ19pZCI6IjE0SXl0bXVoamxiZk92djZrOEdzNDciLC' +
          'JzY29wZSI6WyJleGVjdXRlIl0sImFkZGl0aW9uYWxfY2xhaW0iOiJleUpoY0dsel' +
          'IybGtJam9pTkVKVFNrRk1XVzVqWW10a1IwYzNjRGREZFhsMWNDSXNJbUZzYkVGd2' +
          'FYTWlPbVpoYkhObExDSjFjMlZ5VG1GdFpTSTZJa0ZRU1Y5WFFVMWZTVTVVTVNJc0' +
          'luVnpaWEpKWkNJNklqa3pSVkpWZVROMGVsbzNhRlExTm1vMWFGUmhWMllpZlE9PS' +
          'IsImlzcyI6Imh0dHBzOlwvXC9kbS11cy5pbmZvcm1hdGljYWNsb3VkLmNvbVwvYX' +
          'V0aHotc2VydmljZSIsImV4cCI6MTY2NTg2NDkxNSwiY2xpZW50X25hbWUiOiJVdG' +
          'lsaXF1ZXN0IiwiaWF0IjoxNjY1ODYxMzE1LCJqdGkiOiJtLVFKeVNJUjVYS2lTZU' +
          'hyM1NfRGhBU0FVa1EiLCJjbGllbnRfaWQiOiI3Tm1Ydm43VUs0bGNvdjNxV09lRj' +
          'IxIn0.LOpabXsc7rGYF7KNrsjSQqHH8ZSBxyVw4jUtnlAe5UVnfHdLgW7wtWadR_' +
          'Q93GaQWlYY0efqkPka8XKIm_Er4of5bevpMimKzgCUqwGQ654IlYYMvZ2lWh8fLm' +
          'yjiDQuyBotEuWoBh3B11kUpsQy-CEXXpXqew_--9wWIliKt1lyDcloEqsoCb_fDu' +
          'NXhKn934bHlmIB7WuIhSEJcqsY2sR3XVTVr65vo61DQE7VxtABG5UrVB3WmwVBJO' +
          'TVRnXMVA08sOrbeuW6IqlzTGlvWVzhWAuBri42uFm_ZXYyr7Dm-IX0Hij5FeKOg-' +
          'usVGNdt4c5F0CAimPawJR0JtEbrw'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'bodyC3C6F3712C0F4F299E370330D1E2B909'
        Value = 
          '{'#13#10'   "date": "2022-10-06 07:15:00",'#13#10'  "Ticket": {'#13#10'    "work_e' +
          'xtent": "CURB TO ENTIRE PROPERTY",'#13#10'    "work_city": "Some City"' +
          ','#13#10'    "con_name": "PERCY COMMUNICATION ,INC",'#13#10'    "work_date":' +
          ' "2022-10-06 07:15:00",'#13#10'    "transmit_date": "2022-09-30 15:36:' +
          '00",'#13#10'    "excavator_caller_cell": "555-555-1212",'#13#10'    "excavat' +
          'or_caller_phone":"555-555-1212",'#13#10'    "work_cross": "ALDEBURGH A' +
          'VE",'#13#10'    "due_date": "2022-10-05 23:59:59",'#13#10'    "work_type": "' +
          'BURY CABLE",'#13#10'    "work_address_street": "SAINT GILES CT",'#13#10'    ' +
          '"work_county": "SOMERSET",'#13#10'    "excavator_caller_email": "Me@So' +
          'meWhere.com",'#13#10'    "highrisk": "true",'#13#10'    "excavator_caller_ph' +
          'one":"555-555-1212",'#13#10'     "work_address":"Here and There",'#13#10'   ' +
          '  "work_phone":"555-555-1212",'#13#10'     "excavator_company":"Red Ad' +
          'air",'#13#10'    "ticket_number": "222732273",'#13#10'    "work_state": "NJ"' +
          ','#13#10'    "excavator_caller":"larry",'#13#10'    "abnormalOperatingCondit' +
          'ion": "none",'#13#10'    "work_company": "HOMEOWNER",'#13#10'    "work_addre' +
          'ss_number": "450"'#13#10'  },'#13#10'  "sub_work_type": "Abnormal Operating ' +
          'Area",'#13#10'  "type": "Leaking Company or Customer Piping",'#13#10'  "date' +
          '": "2022-10-03 17:42:04",'#13#10'  "site": "NJNG",'#13#10'  "org": "NJR",'#13#10' ' +
          ' "sourcesysid": "UTILIQUEST",'#13#10'  "Employee": {'#13#10'    "contact_pho' +
          'ne": "225-268-2016",'#13#10'    "short_name": "007.Steven Hull"'#13#10'  },'#13 +
          #10'  "postalcode": "08030",'#13#10'   "Locate": {'#13#10'      "Operators": ['#13 +
          #10'         {'#13#10'            "client_description": "New Jersey Natur' +
          'al Gas",'#13#10'            "client_code": "NJN"'#13#10'         }'#13#10'      ]'#13 +
          #10'   },'#13#10'   "type": "gas leak",'#13#10'   "sourcesysid": "Utiliquest",'#13 +
          #10'   "Notes": {'#13#10'      "note": "hello"'#13#10'   },'#13#10'   "status": "new"' +
          #13#10'}'
        ContentTypeStr = 'application/json'
      end>
    Response = RESTResponseTicket
    SynchronizedEvents = False
    Left = 584
    Top = 112
  end
  object RESTResponseTicket: TRESTResponse
    Left = 592
    Top = 200
  end
  object qryMaximoResponse: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'use QM;'
      'select'
      ' t.ticket_id'
      ',L.Locate_id'
      ',th.header_id'
      ',bcm.[billing_cc] as NJRUQBILLINGCODE'
      
        ',IsNull([dbo].[extractNumbers](t.caller_phone), '#39'excavator_calle' +
        'r_phone'#39') as '#39'excavator_caller_phone'#39
      ',IsNull(t.ticket_type, '#39'ticket_type'#39') as '#39'ticket_type'#39
      ',[dbo].[CR_LF_stripper] (work_extent)  as '#39'work_extent'#39
      ',IsNull(t.work_city, '#39'work_city'#39') as '#39'work_city'#39
      ',IsNull(t.con_name, '#39'con_name'#39') as '#39'con_name'#39
      ',IsNull(t.work_date, '#39#39') as '#39'work_date'#39
      ',IsNull(t.transmit_date, '#39#39') as '#39'transmit_date'#39
      
        ',IsNull(td.udf07, '#39'excavator_caller_cell'#39')  as '#39'excavator_caller' +
        '_cell'#39
      ',IsNull(t.work_cross, '#39'work_cross'#39') as '#39'work_cross'#39
      ',IsNull(t.due_date, '#39#39') as '#39'due_date'#39
      ',IsNull(t.work_type, '#39'work_type'#39') as '#39'work_type'#39
      
        ',IsNull([dbo].[extractNumbers](t.caller_phone), '#39'work_phone'#39')  a' +
        's '#39'work_phone'#39
      
        ',IsNull(td.udf01, '#39'work_address_street'#39')  as '#39'work_address_stree' +
        't'#39
      ',IsNull(t.work_county, '#39'work_county'#39') as '#39'work_county'#39
      
        ',IsNull(t.caller_email, '#39'excavator_caller_email'#39') as '#39'excavator_' +
        'caller_email'#39
      ',IsNull(td.udf10, '#39'highrisk'#39')  as '#39'highrisk'#39
      ',IsNull(t.ticket_number, '#39'ticket_number'#39') as '#39'ticket_number'#39
      ',IsNull(t.company, '#39'work_company'#39')  as '#39'work_company'#39
      ',IsNull(td.udf11, '#39'AOC'#39') as '#39'abnormalOperatingCondition'#39
      ',IsNull(td.udf06, '#39'excavator_caller'#39')  as '#39'excavator_caller'#39
      ',IsNull(t.work_state, '#39'work_state'#39') as '#39'work_state'#39
      ',IsNull(t.con_address, '#39'work_address'#39')  as '#39'work_address'#39
      
        ',IsNull(td.udf00, '#39'work_address_number'#39')  as '#39'work_address_numbe' +
        'r'#39
      ',IsNull(t.company, '#39'excavator_company'#39')  as '#39'excavator_company'#39
      ',[dbo].[CR_LF_stripper](t.work_description)  as '#39'description'#39
      
        ',IsNull(td.udf12, '#39'enroute_instructions'#39')  as '#39'enroute_instructi' +
        'ons'#39
      ',t.expiration_date as '#39'expiry_date'#39
      ',IsNull(td.udf13, '#39#39') as '#39'EPRLink'#39
      
        ',REPLACE(REPLACE(REPLACE(t.work_remarks, CHAR(13), '#39' '#39'), CHAR(10' +
        '), '#39' '#39'), CHAR(34), '#39' inch'#39')   as '#39'remarks'#39
      ',IsNull(t.con_address, '#39#39')  as '#39'excavator_address'#39
      
        ',IsNull(t.con_city, '#39#39') +'#39', '#39'+ IsNull(t.con_state, '#39#39')+'#39' '#39'+ IsNu' +
        'll(t.con_zip, '#39#39') as '#39'excavator_city'#39
      ''
      ','#39'NJNG'#39' as '#39'site'#39
      ',th.form_name as '#39'sub_work_type'#39
      ','#39'NJR'#39' as '#39'org'#39
      ','#39'00000'#39' as '#39'postalcode'#39
      ',IsNull(td.udf11, '#39'type'#39')  as '#39'type'#39
      ','#39'UTILIQUEST'#39' as '#39'sourcesysid'#39
      ','#39'NEW'#39' as '#39'status'#39
      
        ',IsNull([dbo].[extractNumbers](td.udf09), '#39'contact_phone'#39')  as '#39 +
        'contact_phone'#39
      ','#39'NJNG DISTRIBUTION GROUP'#39'  as '#39'short_name'#39
      ',IsNull(td.udf08, '#39'email'#39')  as  '#39'email'#39
      ''
      '  FROM [QM].[dbo].[responder_multi_queue] rmq'
      '  join [dbo].[tema_header] th on (rmq.locate_id=th.header_id)'
      '  join [dbo].[tema_details] td on (th.header_id=td.header_id)'
      '  join [dbo].[ticket] t on (t.ticket_id=th.ticket_id)'
      '  join [dbo].[locate] L on (t.ticket_id=L.ticket_id)'
      
        '  join [dbo].[billing_cc_map] bcm on (L.client_code=bcm.[client_' +
        'code]) and (t.work_city=bcm.[work_city]) and (t.work_county=bcm.' +
        '[work_county])'
      '  where rmq.[respond_to]= '#39'MAXIMO'#39
      '  and L.[client_code] = '#39'NJN'#39
      '')
    Left = 184
    Top = 32
  end
  object qryClients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ''
      'select l.client_code, c.client_name as '#39'client_description'#39
      'from locate l'
      'join client c on (c.[oc_code] = l.client_code)'
      'join [dbo].[tema_header] th on (l.ticket_id = th.ticket_id)'
      'where th.ticket_id = :ticketID'
      'order by l.ticket_id desc'
      'for JSON PATH, ROOT('#39'Operators'#39')')
    Left = 192
    Top = 96
  end
  object qryNotes: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select REPLACE(REPLACE(REPLACE(note, CHAR(13), '#39' '#39'), CHAR(10), '#39 +
        ' '#39'), CHAR(34), '#39' inch'#39')   as '#39'Note'#39
      'from Notes N'
      'where foreign_id = :ticketID'
      'and active = 1'
      'and foreign_type = 1'
      'and sub_type<200')
    Left = 192
    Top = 168
  end
end
