unit MaximoMain;
//QM-596
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.json,
  Data.DB, System.Net.URLClient,
  Data.Win.ADODB, REST.Types,
  IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls;

type
  TResponseLogEntry = record
    LocateID:integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;  //TLogResults


type
  TfrmMaximoResponder = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    delResponse: TADOQuery;
    RESTClientTicket: TRESTClient;
    RESTRequestTicket: TRESTRequest;
    RESTResponseTicket: TRESTResponse;
    qryMaximoResponse: TADOQuery;
    qryClients: TADOQuery;
    qryNotes: TADOQuery;

    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure RESTClientTicketAuthEvent(const Sender: TObject;
      AnAuthTarget: TAuthTargetType; const ARealm, AURL: string; var AUserName,
      APassword: string; var AbortAuth: Boolean;
      var Persistence: TAuthPersistenceType);
    procedure Memo1DblClick(Sender: TObject);
  private
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;


    fToken:WideString;
    vClientId:string;
    vClientSecret:string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;

    FRespondto: string;
    fCallCenter: string;

    fTicketURL: string;
    fTokenURL: string;
    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function GetAppVersionStr: string;
    function GetToken: WideString;
    function GetClientJSON(ticketID:integer): string;
    function GetNoteJSON(ticketID: integer): string;

    { Private declarations }
  public
    { Public declarations }

    property TokenURL: string  read fTokenURL write fTokenURL;
    property TicketURL: string read fTicketURL write fTicketURL;
    property CallCenter: string read fCallCenter write fCallCenter;
    property Respondto: string read FRespondto write FRespondto;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;

  end;

var
  frmMaximoResponder: TfrmMaximoResponder;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmMaximoResponder.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;


procedure TfrmMaximoResponder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmMaximoResponder.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  Respondto := 'Maximo';
  CallCenter:= 'NJN';
  AppVer:='';
  ProcessINI;
  AppVer  := GetAppVersionStr;
  if (Respondto='') or (CallCenter='')  then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'processParams';
    LogResult.Status := 'MISSING Params';
    LogResult.DataStream:='RespondTo: '+ Respondto + '  CallCenter: '+CallCenter;
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'processParams';
    LogResult.DataStream:= Respondto + '  '+ParamStr(2)+'  '+CallCenter;
    WriteLog(LogResult);
  end;
  connectDB;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  fToken:=GetToken;

  if ParamStr(1)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

function TfrmMaximoResponder.GetToken:WideString;
var
  aExpire: string;
  LClient: TRESTClient;
  LRequest: TRESTRequest;
  LResponse: TRESTResponse;

  GaTokenExpire:integer;
  GaToken :string;
  GaTokenType:string;
begin
  LClient := TRESTClient.Create(TokenURL);
  try
    LRequest := TRESTRequest.Create(LClient);
    try
      LResponse := TRESTResponse.Create(LClient);
      try
        LRequest.Client := LClient;
        LRequest.Response := LResponse;

        LRequest.Method := rmPOST;
        LRequest.AddAuthParameter('grant_type', 'client_credentials', pkGETorPOST);
        LRequest.AddAuthParameter('client_id', vClientId, pkGETorPOST);
        LRequest.AddAuthParameter('client_secret', vClientSecret, pkGETorPOST);


        LRequest.Execute;

        LResponse.GetSimpleValue('access_token', GaToken);
        LResponse.GetSimpleValue('token_type', GaTokenType);
        LResponse.GetSimpleValue('expires_in', aExpire);
        GaTokenExpire := StrToInt(aExpire);
        Result := GaToken;
        Memo1.Clear;
        Memo1.Lines.Add(IntToStr(LResponse.StatusCode) + ' / ' + LResponse.StatusText);
        Memo1.Lines.Add('------------   R A W   R E S P O N S E   ---------------------');
        Memo1.Lines.Add(LResponse.Headers.Text);
        Memo1.Lines.Add('--------------------------------------------------------------');
        Memo1.Lines.Add(LResponse.Content);
        Memo1.Lines.Add('--------------------  T O K E N  -----------------------------');
        Memo1.Lines.Add(GAToken);
      finally
        LResponse.Free;
      end;
    finally
      LRequest.Free;
    end;
  finally
    LClient.Free;
  end;
end;


procedure TfrmMaximoResponder.Memo1DblClick(Sender: TObject);
begin
  memo1.Clear;
end;

function TfrmMaximoResponder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmMaximoResponder.ProcessINI: boolean;
var
  MaximoINI: TIniFile;
begin
  try
    MaximoINI := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer   := MaximoINI.ReadString('Database', 'Server', '');
    aDatabase := MaximoINI.ReadString('Database', 'DB', 'QM');
    ausername := MaximoINI.ReadString('Database', 'UserName', '');
    apassword := MaximoINI.ReadString('Database', 'Password', '');

    flogDir    := MaximoINI.ReadString('LogPaths', 'LogPath', '');

    TokenURL      :=  MaximoINI.ReadString('RestToken', 'BaseURL', '');
    TicketURL     :=  MaximoINI.ReadString('RestTicket', 'BaseURL', '');
    vClientId     :=  MaximoINI.ReadString('RestTicket', 'ClientId', '');
    vClientSecret :=  MaximoINI.ReadString('RestTicket', 'ClientSecret', '');


    RestClientTicket.BaseURL := TicketURL;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    MaximoINI.Free;
  end;
end;

procedure TfrmMaximoResponder.RESTClientTicketAuthEvent(const Sender: TObject;
  AnAuthTarget: TAuthTargetType; const ARealm, AURL: string; var AUserName,
  APassword: string; var AbortAuth: Boolean;
  var Persistence: TAuthPersistenceType);
begin
   AbortAuth:=true;
end;

procedure TfrmMaximoResponder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir)+ RespondTo+' ' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;




procedure TfrmMaximoResponder.btnSendResponseClick(Sender: TObject);
const
  DBL_QUOTES = '"';
  PRE_TOKEN = 'Bearer ';
MAXIMO_TICKET_RETURN =
'{'+
'  "date": "%s",'+
'  "billing_code": "%s",'+      //qm-758
'    "Ticket": {'+
'    "excavator_caller_phone":"%s",'+
'    "ticket_type":"%s", '+  //qm-785
//'    "work_extent": "%s",'+  //qm-815
'    "work_city": "%s",'+
'    "con_name": "%s",'+
'    "work_date": "%s",'+
'    "transmit_date": "%s",'+
'    "excavator_caller_cell": "%s",'+
'    "work_cross": "%s",'+
'    "due_date": "%s",'+
'    "work_type": "%s",'+
//'    "work_phone":"%s",'+ //qm-815
'    "work_address_street": "%s",'+
'    "work_county": "%s",'+
'    "excavator_caller_email": "%s",'+
'    "highrisk": "%s",'+
'    "ticket_number": "%s",'+
'    "work_company": "%s",'+
'    "abnormalOperatingCondition": "%s",'+
'    "excavator_caller":"%s",'+
'    "work_state": "%s",'+
//'    "work_address":"%s",'+  //qm-815
'    "work_address_number": "%s",'+
'    "excavator_company":"%s",'+
'    "description":"%s",'+         //mod
'    "enroute_instructions":"%s",'+
'    "expiry_date":"%s",'+
'    "remarks":"%s",'+
'    "excavator_address":"%s",'+     //mod
'    "excavator_city":"%s"'+         //mod     (t.con_city, '') +', '+ IsNull(t.con_state, '')+' '+ (t.con_zip)
'  },'+
'  "site": "%s",'+
'  "sub_work_type": "%s",'+
'  "Employee": {'+
'    "contact_phone": "%s",'+
'    "short_name": "%s",'+
'    "email": "%s"'+
'  }, '+
'  "org": "%s",'+
'  "postalcode": "%s",'+
'   "locate": %s '+
'   ,'+
'  "type": "%s",'+
'  "sourcesysid": "%s",'+
'  "EPRLINK": { '+          //QM-901
'    "LINK": "%s" }, '+
'   "Notes": {'+
'      "note": "%s"'+
'   },'+
'   "status": "%s"'+
'}';

  ELIPSIS='...';
var
  ticketID, locateID:integer;
  OutStatus:string;
  HeaderID:integer;

  ResponseBody:string;
  ResponseSent:string;
  cntGood, cntBad :integer;
jNJRUQBILLINGCODE,    //qm-758
jWork_extent, jWork_city, jCon_name, jWork_date, jTransmit_date,
jExcavator_caller_cell, jExcavator_caller_phone, jTicket_type,  //qm-785
jWork_cross,  jDue_date,
jWork_type, jWork_address_street,  jWork_county,
jExcavator_caller_email, jHighrisk, jWork_address, jWork_phone,
jExcavator_company, jTicket_number, jWork_state,
jExcavator_caller, jAbnormalOperatingCondition,
jWork_company, jWork_address_number,
jdescription, jenroute_instructions, jexpiry_date, jremarks:string;
jexcavator_address,jexcavator_city:string;   //mod


jSub_work_type, jType, jDate, jSite, jOrg:string;

 jSourcesysid, jContact_phone, jShort_name, jEmail,
 jPostalcode, jOperators,jEPRLinks, jNotes, jStatus:string;  //QM-901

 function RemoveMilliSec(s:string):string;
 var
 X:integer;
 begin
   x:=pos(#46,s);
   result:=copy(s,0,X-1);
 end;
begin
  cntGood :=0;
  cntBad :=0;
  with qryMaximoResponse do
  begin
//some parameters
    open;
    while not eof do
    begin
      ticketID := FieldByName('ticket_id').asinteger;
      locateID := FieldByName('locate_id').asinteger;
      HeaderID  := FieldByName('header_id').asinteger;
      jNJRUQBILLINGCODE:=FieldByName('NJRUQBILLINGCODE').AsString;
      jDate:=RemoveMilliSec(DateToISO8601(today,false));

      jExcavator_caller_phone:=Trim(FieldByName('excavator_caller_phone').AsString);
      jTicket_type:=Trim(FieldByName('ticket_type').AsString);   //qm-785
      jWork_extent:=Trim(FieldByName('work_extent').AsString);
      jWork_city:=Trim(FieldByName('work_city').AsString);
      jCon_name:=Trim(FieldByName('con_name').AsString);
      jWork_date:= RemoveMilliSec(DateToISO8601(StrToDateTime(FieldByName('work_date').AsString), false));
      jTransmit_date:=RemoveMilliSec(DateToISO8601(StrToDateTime(FieldByName('transmit_date').AsString)));
      jExcavator_caller_cell:=Trim(FieldByName('excavator_caller_cell').AsString);
      jWork_cross:=Trim(FieldByName('work_cross').AsString);
      jDue_date:=RemoveMilliSec(DateToISO8601(StrToDateTime(FieldByName('due_date').AsString), false));
      jWork_type:=Trim(FieldByName('work_type').AsString);
      jWork_phone:=Trim(FieldByName('work_phone').AsString);
      jWork_address_street:=Trim(FieldByName('work_address_street').AsString);
      jWork_county:=Trim(FieldByName('work_county').AsString);
      jExcavator_caller_email:=Trim(FieldByName('excavator_caller_email').AsString);
      if Trim(FieldByName('highrisk').AsString) = '1' then
      jHighrisk:='true'
      else
      jHighrisk:='false';
      jTicket_number:=Trim(FieldByName('ticket_number').AsString);
      jWork_company:=Trim(FieldByName('work_company').AsString);
      if Trim(FieldByName('abnormalOperatingCondition').AsString) = 'NOAOC' then
      jAbnormalOperatingCondition:='no'
      else
      jAbnormalOperatingCondition:='yes';
      jExcavator_caller:=Trim(FieldByName('excavator_caller').AsString);
      jWork_state:=Trim(FieldByName('work_state').AsString);
      jWork_address:=Trim(FieldByName('work_address').AsString);
      jWork_address_number:=Trim(FieldByName('work_address_number').AsString);
      jExcavator_company:=Trim(FieldByName('excavator_company').AsString);

      jdescription            := Trim(FieldByName('description').AsString);
      jenroute_instructions   := Trim(FieldByName('enroute_instructions').AsString);
      jexpiry_date            := RemoveMilliSec(DateToISO8601(FieldByName('expiry_date').AsDateTime,false));
      jremarks                := Trim(FieldByName('remarks').AsString);
      jEPRLinks               := Trim(FieldByName('EPRLink').AsString);  //QM-901

      jexcavator_address      := Trim(FieldByName('excavator_address').AsString);//mod
      jexcavator_city         := Trim(FieldByName('excavator_city').AsString); //mod

      jSite:=Trim(FieldByName('site').AsString);
      jSub_work_type:=Trim(FieldByName('sub_work_type').AsString);

      jContact_phone:=Trim(FieldByName('contact_phone').AsString);
      jShort_name:=FieldByName('short_name').AsString;
      jEmail:= Trim(FieldByName('email').AsString);

      jOrg:=Trim(FieldByName('org').AsString);
      jPostalcode:=Trim(FieldByName('postalcode').AsString);

      jOperators:=GetClientJSON(ticketID);

      jType:=Trim(FieldByName('type').AsString);
      jSourcesysid:=Trim(FieldByName('sourcesysid').AsString);

      jNotes:=GetNoteJSON(ticketID);

      jStatus:=Trim(FieldByName('status').AsString);

      ResponseBody := format(MAXIMO_TICKET_RETURN, [
      jDate,
      jNJRUQBILLINGCODE,
      jExcavator_caller_phone,
      jTicket_type,  //qm-785
//      jWork_extent,  //qm-815
      jWork_city,
      jCon_name,
      jWork_date,
      jTransmit_date,
      jExcavator_caller_cell,
      jWork_cross,
      jDue_date,
      jWork_type,
//      jWork_phone,  //qm-815
      jWork_address_street,
      jWork_county,
      jExcavator_caller_email,
      jHighrisk,
      jTicket_number,
      jWork_company,
      jAbnormalOperatingCondition,
      jExcavator_caller,
      jWork_state,
//      jWork_address,   //qm-815
      jWork_address_number,
      jExcavator_company,
      jdescription,
      jenroute_instructions,
      jexpiry_date,
      jremarks,
      jexcavator_address,   //mod
      jexcavator_city,      //mod
      jSite,
      jSub_work_type,

      jContact_phone,
      jShort_name,
      jEmail,
      jOrg,
      jPostalcode,
      jOperators,

      jType,
      jSourcesysid,
      jEPRLinks,     //QM-901
      jNotes,

      jStatus]);

      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequestTicket.Params.Clear;
      RESTRequestTicket.AddAuthParameter('Authorization',PRE_TOKEN+fToken,pkHTTPHEADER,[poDoNotEncode]);

      RESTRequestTicket.ClearBody;
      RESTClientTicket.ContentType := 'application/json';
      RESTRequestTicket.AddBody(ResponseBody, ctAPPLICATION_JSON);
      ResponseSent:='';
      ResponseSent:=ResponseBody;
      try
        RESTRequestTicket.Execute;     //comment out for test
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseSent;
          LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +jTicket_Number;
          LogResult.DataStream := RESTResponseTicket.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponseTicket.StatusCode > 199) and (RESTResponseTicket.StatusCode < 300) then
      begin
        ResponseBody:= RESTResponseTicket.Content;
        memo1.Lines.Add('ResponseBody: '+ResponseBody);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket - Data Sent';
        LogResult.DataStream := ResponseSent;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText + ' TicketNo: ' +jTicket_Number;
        WriteLog(LogResult);
        inc(cntGood);


        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.LocateId            := locateID;
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := RESTResponseTicket.StatusText ;
        insertResponseLog.ResponseSent        := IntToStr(HeaderID);
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponseTicket.StatusCode) + '-OK ' +  RESTResponseTicket.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        memo1.Lines.Add(delResponse.SQL.Text);

        try
         delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
         delResponse.Parameters.ParamByName('LocateID').Value :=HeaderID;
         delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
      end  //StatusCode > 199 <300   good
      else if RESTResponseTicket.StatusCode > 399 then
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Deleting Status '+IntToStr(RESTResponseTicket.StatusCode);
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +jTicket_Number;
        LogResult.DataStream := 'Response Sent '+ResponseSent;
        AddResponseLogEntry(insertResponseLog);
        try
         delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
         delResponse.Parameters.ParamByName('LocateID').Value :=HeaderID;
         delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
      end //StatusCode = 403    bad and delete
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponseTicket.StatusCode) + ' ' + RESTResponseTicket.StatusText + ' TicketNo: ' +jTicket_Number;
        LogResult.DataStream := 'Returned ' + RESTResponseTicket.Content;

        WriteLog(LogResult);

        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := RESTResponseTicket.StatusText;
        insertResponseLog.ResponseSent        := OutStatus;
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponseTicket.StatusCode) + ' ' +  RESTResponseTicket.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponseTicket.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF
    close;
  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);



  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

function TfrmMaximoResponder.GetClientJSON(ticketID: integer): string;
begin
  try
    qryClients.Parameters.ParamByName('ticketid').Value := ticketID;
    qryClients.open;
    result := qryClients.Fields[0].AsString;
    memo1.Lines.Add(result);
  finally
    qryClients.Close;
  end;

end;

function TfrmMaximoResponder.GetNoteJSON(ticketID: integer): string;
var
  someNotes: string;
begin
  try
    qryNotes.Parameters.ParamByName('ticketid').Value := ticketID;
    qryNotes.open;
    while not qryNotes.eof do
    begin
      someNotes := someNotes + qryNotes.FieldByName('note').AsString + ' ';
      qryNotes.Next;
    end;
  finally
    qryNotes.Close;
    result:= someNotes;
    memo1.Lines.Add(result);
  end;

end;

procedure TfrmMaximoResponder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMaximoResponder.connectDB: boolean;
var
  connStr, connStrLog: String;
begin
  result := True;
  ADOConn.Close;

  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog  := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        StatusBar1.Panels[1].Text:= aServer;
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
