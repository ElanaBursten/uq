unit AttachmentData;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, SysUtils, Classes, ADODB, DB, OdLog, ServerAttachmentDMu;

type
  TAttachmentDM = class(TDataModule)
    Conn: TADOConnection;
    Employee: TADODataSet;
    Attachment: TADODataSet;
    procedure DataModuleDestroy(Sender: TObject);
  private
    FLogger: TOdLog;
    FServerAttachment: TServerAttachment;
    FAttachedByEmpID: Integer;
    function AddAttachment(const FullFileName: string): Boolean;
    procedure Log(const Msg: string);
    procedure LogError(const Msg: string);
    procedure AddSummaryToLog(const FileCount, AddedCount: Integer);
    function GetTopLevelEmpID: Integer;
    procedure LogAttachMessage(Sender: TObject; const Msg: string);
    function FileIsUnattached(const FileName: string): Boolean;
    procedure ParseAttachmentFileName(const FileName: string; var ForeignType,
      ForeignID: Integer);
  public
    procedure Execute(const AttachmentFolder: string; const BeginDate, EndDate: TDateTime; const SaveChanges: Boolean);
  end;

implementation

uses
  Variants, OdAdoUtils, OdIsoDates, OdDbUtils, QMConst, OdMiscUtils,
  JclFileUtils, JclStrings;

{$R *.dfm}
var
  FBeginDate, FEndDate: TDateTime;

function FilterFilesByDate(const Attr: Integer; const FileInfo: TSearchRec): Boolean;
var
  FileDate: TDateTime;
begin
  // exclude directories & files outside of selected date
  FileDate := FileDateToDateTime(FileInfo.Time);
  Result := (FileDate >= FBeginDate) and (FileDate <= FEndDate) and
    ((FileInfo.Attr and FILE_ATTRIBUTE_DIRECTORY) = 0);
end;

procedure TAttachmentDM.Execute(const AttachmentFolder: string;
  const BeginDate, EndDate: TDateTime; const SaveChanges: Boolean);
var
  Files: TStringList;
  IniFileName, LogFileName: string;
  I: Integer;
  MissingCount: Integer;
  AddedCount: Integer;
begin
  MissingCount := 0;
  AddedCount := 0;
  if not DirectoryExists(AttachmentFolder) then
    raise Exception.CreateFmt('The specified attachment folder %s does not exist.',
      [AttachmentFolder]);

  IniFileName := 'qmserver.ini';
  if not FileExists(IniFileName) then
    raise Exception.CreateFmt('Cannot find the configuration file %s', [IniFileName]);

  if BeginDate > EndDate then
    raise Exception.Create('EndDate cannot occur before the BeginDate');

  FBeginDate := BeginDate;
  FEndDate := EndDate;
  LogFileName := 'RecoverAttachments-' + FormatDateTime('yyyy-mm-dd-hhnnss', Now) + '.log';
  InitializeHandler(LogFileName, IniFileName);
  FLogger := TOdlog.Create(LogFileName, IniFileName);
  try
    Files := TStringList.Create;
    try
      // start writing to the log file
      FLogger.OpenLog;
      Log('RecoverAttachments process starting ' + DateTimeToStr(Now));
      Log(' Attachment folder: ' + AttachmentFolder);
      Log(' Date Range: ' + IsoDateToStr(BeginDate) + ' thru ' + IsoDateToStr(EndDate));
      if SaveChanges then
        Log(' Mode: Save missing attachments to database')
      else
        Log(' Mode: Only show missing attachments - No database updates');

      Log(CRLF + 'Connecting to database');
      ConnectAdoConnectionWithIni(Conn, IniFileName);
      FAttachedByEmpID := GetTopLevelEmpID;
      FServerAttachment := TServerAttachment.Create(Conn, FAttachedByEmpID, []);
      FServerAttachment.OnLogMessage := LogAttachMessage;

      Log('Building file list');
      AdvBuildFileList(IncludeTrailingBackslash(AttachmentFolder) + '*.*',
        faAnyFile, Files, amCustom, [flFullNames, flRecursive], '', FilterFilesByDate);

      Conn.BeginTrans;
      Log('List of missing attachments: ');
      for i := 0 to Files.Count - 1 do begin
        if FileIsUnattached(Files[i]) then begin
          Inc(MissingCount);
          Log(' ' + Files[i]);
          if SaveChanges then begin
            if AddAttachment(Files[i]) then
              Inc(AddedCount);
          end;
        end;
      end;
      Conn.CommitTrans;
      AddSummaryToLog(MissingCount, AddedCount);
    finally
      FreeAndNil(Files);
    end;
  except
    on E: Exception do begin
      LogError(E.Message);
      if Conn.InTransaction then begin
        Log('Rolling back all changes');
        Conn.RollbackTrans;
      end;
    end;
  end;
  Log('RecoverAttachments complete ' + DateTimeToStr(Now));
  Sleep(5000);
end;

function TAttachmentDM.FileIsUnattached(const FileName: string): Boolean;
var
  ForeignID: Integer;
  ForeignType: Integer;
begin
  ParseAttachmentFileName(FileName, ForeignType, ForeignID);
  try
    Attachment.Parameters.ParamByName('foreign_id').Value := ForeignID;
    Attachment.Parameters.ParamByName('foreign_type').Value := ForeignType;
    Attachment.Parameters.ParamByName('filename').Value := ExtractFileName(FileName);
    Attachment.Open;
    Result := Attachment.IsEmpty;
  finally
    Attachment.Close;
  end;
end;

function TAttachmentDM.AddAttachment(const FullFileName: string): Boolean;
var
  ForeignID: Integer;
  ForeignType: Integer;
  FileName: string;
begin
  Assert(Assigned(FServerAttachment), 'ServerAttachment object is not assigned in AddAttachment');
  Result := False;
  try
    FileName := ExtractFileName(FullFileName);
    ParseAttachmentFileName(FullFileName, ForeignType, ForeignID);
    FServerAttachment.AttachUploadedFileToRecord(ForeignType, ForeignID,
      FileName, FullFileName, 'Data Recovery', '');
    Result := True;
  except
    on E: Exception do
      LogError(E.Message);
  end;
end;

procedure TAttachmentDM.ParseAttachmentFileName(const FileName: string; var ForeignType, ForeignID: Integer);
var
  NamePrefix: string;
  PathParts: TStringList;
begin
  NamePrefix := Copy(ExtractFileName(FileName), 1, 1);
  if NamePrefix = 'T' then
    ForeignType := qmftTicket
  else if NamePrefix = 'D' then
    ForeignType := qmftDamage
  else if NamePrefix = 'P' then
    ForeignType := qmft3rdParty
  else if NamePrefix = 'L' then
    ForeignType := qmftLitigation
  else
    raise Exception.Create('Unexpected filename prefix for ' + FileName);

  PathParts := TStringList.Create;
  try
    StrToStrings(ExtractFilePath(FileName), '\', PathParts, False);
    if PathParts.Count < 1 then
      raise Exception.Create('Skipping file ' + FileName);

    ForeignID := StrToInt(PathParts[PathParts.Count-1]);
  finally
    FreeAndNil(PathParts);
  end;
end;

procedure TAttachmentDM.AddSummaryToLog(const FileCount, AddedCount: Integer);
begin
  Log(CRLF + '---------  SUMMARY  ---------');
  Log(Format('Attachments Missing   : %5d', [FileCount]));
  Log(Format('Attachments Added     : %5d', [AddedCount]));
  Log('-----------------------------');
end;

procedure TAttachmentDM.DataModuleDestroy(Sender: TObject);
begin
  FLogger.CloseLog;
  UnInitializeHandler;
  FreeAndNil(FLogger);
  FreeAndNil(FServerAttachment);
end;

function TAttachmentDM.GetTopLevelEmpID: Integer;
const
  Select = 'select emp_id, emp_number, short_name from employee where emp_id = (' +
    'select max(emp_id) from employee where report_to is null and active = 1)';
begin
  Result := -1;
  Employee.CommandText := Select;
  Employee.Open;
  try
    if Employee.IsEmpty then
      raise Exception.Create('Cannot find a top level employee');
    Result := Employee.FieldByName('emp_id').Value;
    Log(Format('Attaching as %s; Emp Id %d', [Employee.FieldByName('short_name').AsString,
      Result]));
  finally
    Employee.Close;
  end;
end;

procedure TAttachmentDM.LogAttachMessage(Sender: TObject; const Msg: string);
begin
  Log(Msg);
end;

procedure TAttachmentDM.Log(const Msg: string);
begin
  FLogger.Write(Msg);
  {$IFNDEF TEST_MODE}
  WriteLn(Msg);
  {$ENDIF}
end;

procedure TAttachmentDM.LogError(const Msg: string);
begin
  FLogger.Write('* Error: ' + Msg);
  {$IFNDEF TEST_MODE}
  WriteLn('* Error: ' + Msg);
  {$ENDIF}
end;

end.
