object AttachmentDM: TAttachmentDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 113
  Width = 316
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=canucanoe;Persist Security Info=Tru' +
      'e;User ID=sa;Initial Catalog=TestDB;Data Source=localhost'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 8
    Top = 16
  end
  object Employee: TADODataSet
    CacheSize = 500
    Connection = Conn
    Parameters = <>
    Left = 52
    Top = 49
  end
  object Attachment: TADODataSet
    CacheSize = 500
    Connection = Conn
    CommandText = 
      'select * from attachment '#13#10'where foreign_id = :foreign_id '#13#10'and ' +
      'foreign_type = :foreign_type '#13#10'and filename = :filename'
    Parameters = <
      item
        Name = 'foreign_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'foreign_type'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'filename'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end>
    Left = 111
    Top = 50
  end
end
