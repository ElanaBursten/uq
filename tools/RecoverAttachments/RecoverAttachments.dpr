program RecoverAttachments;

{$APPTYPE CONSOLE}

{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  SysUtils,
  ActiveX,
  AttachmentData in 'AttachmentData.pas' {AttachmentDM: TDataModule},
  OdAdoUtils in '..\..\common\OdAdoUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  OdLog in '..\..\common\OdLog.pas' {OdExceptionDialog},
  QMConst in '..\..\client\QMConst.pas',
  OdDbUtils in '..\..\common\OdDbUtils.pas',
  OdExceptions in '..\..\common\OdExceptions.pas',
  OdVclUtils in '..\..\common\OdVclUtils.pas',
  OdDBISAMUtils in '..\..\common\OdDBISAMUtils.pas',
  ServerAttachmentDMu in '..\..\server\ServerAttachmentDMu.pas',
  BaseAttachmentDMu in '..\..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  OdUqInternet in '..\..\common\OdUqInternet.pas',
  OdSecureHash in '..\..\common\OdSecureHash.pas',
  OdFtp in '..\..\common\OdFtp.pas',
  Hashes in '..\..\thirdparty\Hashes.pas',
  UQDbConfig in '..\..\common\UQDbConfig.pas',
  CodeLookupList in '..\..\common\CodeLookupList.pas';

procedure ShowHelp;
  begin
    WriteLn('');
    WriteLn('Version: ' + AppVersionShort);
    WriteLn('');
    WriteLn('RecoverAttachments is a utility for adding attachments to a QM database.');
    WriteLn('It scans a folder containing files uploaded from QManager. Any files');
    WriteLn('uploaded within a range of dates that do not exist in the attachment');
    WriteLn('table are listed & optionally inserted into the table.');
    WriteLn('');
    WriteLn('Usage: AttachmentFolder BeginDate EndDate [Fix]');
    WriteLn('  AttachmentFolder is the location of a folder containing uploaded attachments.');
    WriteLn('  BeginDate is the starting upload date to check in yyyy-mm-dd format.');
    WriteLn('  EndDate is the inclusive ending date to check in yyyy-mm-dd format.');
    WriteLn('  Fix can be included to add all the missing attachments.');
    WriteLn('  A QMServer.ini file provides the database connection & log location info.');
  end;

var
  DM: TAttachmentDM;
  SaveChanges: Boolean;
  BeginDate: TDateTime;
  EndDate: TDateTime;
begin
  if ParamCount < 3 then begin
    ShowHelp;
    Exit;
  end;

  try
    BeginDate := IsoStrToDate(ParamStr(2));
    EndDate := IsoStrToDate(ParamStr(3));

    CoInitialize(nil);
    DM := TAttachmentDM.Create(nil);
    try
      if ParamCount > 3 then
        SaveChanges := (SameText(ParamStr(4), 'Fix'))
      else
        SaveChanges := False;

      DM.Execute(ParamStr(1), BeginDate, EndDate, SaveChanges);
    finally
      FreeAndNil(DM);
      CoUninitialize;
    end;
  except
    on E: Exception do begin
      WriteLn('ERROR:' + E.Message);
      ShowHelp;
    end;
  end;
end.
