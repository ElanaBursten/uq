program UrbintUniResponderVA;
//QM-513
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  UrbintUniMainVA in 'UrbintUniMainVA.pas' {frmUrbintUniResponderVA};

var
  MyInstanceName: string;
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName)+'_'+ParamStr(2);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.Title := '';
  Application.CreateForm(TfrmUrbintUniResponderVA, frmUrbintUniResponderVA);
  Application.Run;
  end
  else
    Application.Terminate;
end.
