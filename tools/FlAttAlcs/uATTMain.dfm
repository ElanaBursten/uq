object frmMainATT: TfrmMainATT
  Left = 0
  Top = 0
  Caption = 'UATT '
  ClientHeight = 399
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 24
    Width = 561
    Height = 289
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnSendResponse: TButton
    Left = 80
    Top = 327
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 1
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 380
    Width = 635
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object RESTResponse1: TRESTResponse
    Left = 512
    Top = 128
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        name = 'body'
        Options = [poDoNotEncode]
        ContentType = ctAPPLICATION_JSON
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 512
    Top = 16
  end
  object RESTClient1: TRESTClient
    Authenticator = HTTPBasicAuthenticator1
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://test-alcs.att.com/closeouts/se'
    ContentType = 'application/json'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 512
    Top = 72
  end
  object HTTPBasicAuthenticator1: THTTPBasicAuthenticator
    Username = 'utiuat'
    Password = 'Welcome@123'
    Left = 512
    Top = 192
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to= :ResponseTo'
      '')
    Left = 376
    Top = 144
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 60
    ProcedureName = 'get_pending_multi_responses_9FL'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Value = Null
      end>
    Left = 120
    Top = 160
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    ConnectionTimeout = 60
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 48
    Top = 144
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
  object delCleanUpMultiQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'ClientList'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'and respond_to= ResponseTo'
      'and client_code <> ClientList')
    Left = 392
    Top = 232
  end
  object delPreCleanMultiQ: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'USE QM'
      'delete'
      'from responder_multi_queue'
      'where locate_id in ('
      #9#9#9#9#9'(select rmq.locate_id'
      '          from responder_multi_queue rmq'
      #9#9#9#9#9'join locate l on l.locate_id = rmq.locate_id'
      #9#9#9#9#9'where l.added_by <> '#39'PARSER'#39'))'
      '')
    Left = 352
    Top = 56
  end
  object delPreCleanResponderQ: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'USE QM'
      'delete'
      'from responder_queue'
      'where locate_id in ('
      #9#9#9#9#9'(select rq.locate_id'
      #9#9#9#9#9'from responder_queue rq'
      #9#9#9#9#9'join locate l on l.locate_id = rq.locate_id'
      #9#9#9#9#9'where l.added_by <> '#39'PARSER'#39'))')
    Left = 352
  end
  object Timer1: TTimer
    Interval = 300000
    OnTimer = Timer1Timer
    Left = 448
    Top = 400
  end
  object qryRespondTo: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT url, user_name, password'
      'FROM [QM].[dbo].[respond_to]'
      'where respond_to =:RespondTo')
    Left = 96
    Top = 40
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 160
    Top = 3
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 208
    Top = 64
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvTLSv1, sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 271
  end
  object IdLogEvent1: TIdLogEvent
    Left = 400
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 504
  end
end
