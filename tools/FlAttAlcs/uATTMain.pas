unit uATTMain;
//qm-662
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,System.Types,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, Vcl.ExtCtrls, IdMessage, IdIntercept, IdLogBase, IdLogEvent,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: String[12];
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: boolean;
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
 TSerialNumber=record //qm-662
   System:string[10];
   SerialNo:String[12];
   Term:String[8];
 end;


type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[255];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;  //TLogResults

type
  TfrmMainATT = class(TForm)
    Memo1: TMemo;
    RESTResponse1: TRESTResponse;
    RESTRequest1: TRESTRequest;
    RESTClient1: TRESTClient;
    HTTPBasicAuthenticator1: THTTPBasicAuthenticator;
    delResponse: TADOQuery;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    btnSendResponse: TButton;
    insResponseLog: TADOQuery;
    StatusBar1: TStatusBar;
    delCleanUpMultiQ: TADOQuery;
    delPreCleanMultiQ: TADOQuery;
    delPreCleanResponderQ: TADOQuery;
    Timer1: TTimer;
    qryRespondTo: TADOQuery;
    IdSMTP1: TIdSMTP;
    qryEMailRecipients: TADOQuery;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    PostPW      : String;
    PostUserName: String;
    PostUrl     : String;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    parsedSerialNumber:TSerialNumber;
    flogDir: wideString;
    RespondTo : string;
    CallCenter: string;
    fOC_Code: string;
    function connectDB: boolean;
    function ProcessINI:boolean;
    procedure clearLogRecord;

    procedure InitializeRest;
    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    function CleanHouse: integer;
    function SpliceSerialNumber(serialNo: string): TSerialNumber;
    function DeleteSingleRecord(iLocateID: integer; RespondTo: string): boolean;
    procedure PreCleanResponderQueues;
    procedure SetUpRecipients;
  public
    { Public declarations }
    property OC_Code:string read fOC_Code write fOC_Code;
    function processParams:boolean;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
  end;

var
  frmMainATT: TfrmMainATT;

implementation
uses  dateutils, StrUtils, iniFiles;

{$R *.dfm}

procedure TfrmMainATT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmMainATT.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  AppVer:='';

  processParams;

  ProcessINI;
  connectDB;

  try
    qryRespondTo.Parameters.ParamByName('RespondTo').Value := RespondTo;
    qryRespondTo.Open;           //url, user_name, password
    PostUrl:=qryRespondTo.FieldByName('url').AsString;
    PostUserName:= qryRespondTo.FieldByName('user_name').AsString;
    PostPW:=qryRespondTo.FieldByName('password').AsString;
  finally
    qryRespondTo.Close;
  end;


  InitializeRest;
  AppVer  := GetAppVersionStr;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

//  PreCleanResponderQueues;  //QM-628 sr
  SetUpRecipients;

  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;



procedure TfrmMainATT.PreCleanResponderQueues;    //QM-628 sr
var
  Cnt1,Cnt2:integer;
begin
  Cnt1:=0;Cnt2:=0;
  Cnt1:=delPreCleanResponderQ.ExecSQL;
  Cnt2:=delPreCleanMultiQ.ExecSQL;

  if Cnt1+Cnt2>0 then
  begin
    LogResult.MethodName := 'PreCleanResponderQueues';
    LogResult.DataStream := 'Deleted '+intToStr(Cnt1+Cnt2)+' records from Queues';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  end;
end;

procedure TfrmMainATT.InitializeRest;
begin
  HTTPBasicAuthenticator1.Password:=  PostPW;
  HTTPBasicAuthenticator1.Username:=  PostUserName;
  RESTClient1.BaseURL             :=  PostUrl;
  LogResult.MethodName := 'InitializeRest';
  LogResult.DataStream := RESTClient1.BaseURL;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
end;

procedure TfrmMainATT.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + Respondto + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMainATT.btnSendResponseClick(Sender: TObject);//qm-662
//705562540|3014|600|Response by UtiliQuest|091421|0108A|UTI
//TICKET_ID|ACTION_CODE|FACILITY|LOCATOR_REMARKS|DATE|TIME|VENDOR_NAME
const
  SEP = '|';
   Vendorname = 'uti';
   Facility   = '600';
   Defaultnote = 'Response by UtiliQuest';
var
  SerialNumber:string;
  TicketNbr:string;
  TicketID, LocateID:string;
  CustBody:string;
  ClosedDate:string;
  Status:string;
  retStatus:string;
  sendResponse:string;
  cntGood, cntBad :integer;
  listInx:integer;
  ResponseBody:string;
  requestID:string;
  delCnt:integer;
  aSerialNo:TSerialNumber;
  iLocateID:integer;
begin
  SerialNumber:='';
  TicketID:='';
  TicketNbr:='';
  LocateID:='';
  CustBody:='';
  retStatus:='';
  Status:='';
  requestID:='';
  delCnt:=0;

  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value:= Respondto;
    Parameters.ParamByName('@CallCenter').Value:= CallCenter;
    open;
    while not eof do
    begin
      TicketID:= FieldByName('ticket_id').AsString;
      TicketNbr:= FieldByName('ticket_number').AsString;
      iLocateID:=0;
      LocateID:= FieldByName('locate_id').AsString;
      iLocateID:= FieldByName('locate_id').AsInteger;
      SerialNumber:= FieldByName('serial_number').AsString;
      if pos(SEP,SerialNumber)>1 then
      begin
        aSerialNo := SpliceSerialNumber(SerialNumber);  //QM-608 AT&T 5G Responder Support sr
        SerialNumber:= Trim(aSerialNo.SerialNo);  //QM-608 AT&T 5G Responder Support sr
      end;
      ClosedDate  := FormatDateTime('mmddyy|hhnnA/P', FieldByName('closed_date').AsDateTime);
      Status   := FieldByName('status').AsString;
      memo1.Lines.Add('status: '+Status);

//      memo1.Lines.Add('listInx '+IntToStr(listInx));
      retStatus := trim(FieldByName('outgoing_status').AsString);
      memo1.Lines.Add('retStatus: '+retStatus);

      //  705562540|3014|600|Response by UtiliQuest|091421|0108A|UTI



      sendResponse:= SerialNumber+SEP+retStatus+SEP+Facility+SEP+Defaultnote+SEP+ClosedDate+SEP+Vendorname;
      memo1.Lines.Add('sendResponse: '+sendResponse);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';
      RESTRequest1.AddBody(sendResponse, ctAPPLICATION_JSON);
      RESTRequest1.Method := TRESTRequestMethod.rmPOST;
      try
        RESTRequest1.Execute;   //Comment out for TESTING
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.MethodName:='RESTRequest1.Execute';
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + sendResponse;;
          LogResult.Status := 'Shutting down';
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          application.Terminate;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if RESTResponse1.StatusCode = 200 then
      begin
        ResponseBody:= RESTResponse1.Content;
        memo1.Lines.Add('ResponseBody: '+ResponseBody);
        requestID:=getData(ResponseBody, 'requestId');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process';
        Logresult.DataStream := sendResponse;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketID: ' +TicketID
                            +' TktNbr: '+TicketNbr+' requestID: '+requestID;    //qm-638
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.Status              := Status;
        insertResponseLog.ResponseSent        := retStatus;
        insertResponseLog.Sucess              := True;
        insertResponseLog.Reply               := IntToStr(RESTResponse1.StatusCode)+'-'+SerialNumber;    //qm-638
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Add to ResponseLog';
        Logresult.DataStream := insertResponseLog.Reply;
        WriteLog(LogResult);


        if DeleteSingleRecord(iLocateID,Respondto) then  //qm-625  sr
        begin
          LogResult.LogType := ltinfo;
          LogResult.MethodName := 'DeleteSingleRecord';
          LogResult.Status:='Deleted locateID '+intToStr(iLocateID)+ ' from multiQ after successful send';
          WriteLog(LogResult);
        end;
      end
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        LogResult.ExcepMsg := 'Body ' + retStatus;
        WriteLog(LogResult);
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.Status              := Status;
        insertResponseLog.ResponseSent        := retStatus;
        insertResponseLog.Sucess              := False;
        insertResponseLog.Reply               := RESTResponse1.StatusText;

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);
        Memo1.Lines.Add(retStatus);
        Next;
        Continue;
      end;
      Next;  //response
    end; //while-not EOF

  end; //with

//  delCnt:=CleanHouse;
//  begin
//      LogResult.LogType := ltInfo;
//      LogResult.MethodName := 'Clean House';
//      LogResult.Status := 'Deleted '+intToStr(delCnt)+ ' not eligible clients from multi-Q';
//      WriteLog(LogResult);
//  end;

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

	if cntBad>0 then
	begin
		idSMTP1.Connect;
		idSMTP1.Send(IdMessage1);
		idSMTP1.Disconnect();

		LogResult.LogType := ltInfo;
		LogResult.MethodName := 'Sending email';
		LogResult.Status := 'Some responses failed';
		LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
		WriteLog(LogResult);
	end;

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmMainATT.SetUpRecipients;
begin
  try
  idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
  qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
  qryEMailRecipients.Open;
  if not qryEMailRecipients.eof then
  idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
  else
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'SetUpRecipients';
    LogResult.Status:= 'No recipients to send email to';
    WriteLog(LogResult);
   end;
  finally
  qryEMailRecipients.close;
  end;
end;

function TfrmMainATT.SpliceSerialNumber(serialNo:string):TSerialNumber;  //QM-608 AT&T 5G Responder Support sr
var
  strList:TStringDynArray;
const
  SEP = '|';
begin
  strList:=SplitString(serialNo, SEP);
  result.System:=strList[0];
  result.SerialNo:=strList[1];
  result.Term:=strList[2];
end;

procedure TfrmMainATT.Timer1Timer(Sender: TObject);
begin    //qm-656
  LogResult.LogType := ltError;
  LogResult.MethodName := 'Timer Timed out';
  LogResult.Status := 'Shutting down';
  WriteLog(LogResult);
  application.Terminate;
end;

function TfrmMainATT.CleanHouse():integer;
const
DEL_NON_CLIENTS =
            'delete '+
            'FROM [QM].[dbo].[responder_multi_queue] '+
            'where (respond_to= %s ) '+
            'and (client_code not in (%s) ) ' ;

  function QuoteCommaSetStr(S: string): string;    //qm-624   sr
  var
    SL: TStringList;
    i: Integer;
  begin
    SL := TStringList.Create;
    try
      SL.CommaText := S;
      for i := 0 to SL.Count-1 do
        SL[i] := QuotedStr(SL[i]);
      Result := SL.CommaText;
    finally
      FreeAndNil(SL);
    end;
  end;

begin
  try
      delCleanUpMultiQ.sql.clear;
//      delCleanUpMultiQ.sql.add(Format(DEL_NON_CLIENTS,[QuotedStr(Respondto), QuoteCommaSetStr(Clients.CommaText)]));   //qm-624   sr
      result :=delCleanUpMultiQ.ExecSQL;
  except on E: Exception do
  begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Clean House';
      LogResult.DataStream := delCleanUpMultiQ.sql.text;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
  end;
  end;
end;

function TfrmMainATT.DeleteSingleRecord(iLocateID:integer;RespondTo:string):boolean;  //qm-625   sr
begin
  delResponse.Parameters.ParamByName('ResponseTo').Value := Respondto;
  delResponse.Parameters.ParamByName('LocateID').Value := iLocateID;
  if delResponse.ExecSQL>0 then result := true
  else
  Begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'DeleteSingleRecord';
      LogResult.DataStream := delResponse.sql.text;
      LogResult.ExcepMsg := 'Failed to delete record Locate ID '+IntToStr(iLocateID) +' from multiQ';
      WriteLog(LogResult);
  End;
  memo1.Lines.Add(delResponse.SQL.Text);
end;

function TfrmMainATT.getData(JsonString: String; Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmMainATT.AddResponseLogEntry(insertResponseLog:TResponseLogEntry):boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;

procedure TfrmMainATT.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMainATT.connectDB: boolean;
var
  connStr, connStrLog: String;
begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog:='Provider=SQLNCLI11.1;Password=Not Logged ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;
    StatusBar1.panels[9].Text:=  aServer;
    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.Status := 'Shutting down';
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
        application.Terminate;
      end;
    end;
end;

function TfrmMainATT.ProcessINI: boolean;
var
  FL_ATT_ALCS_Resp: TIniFile;
begin
  try
    FL_ATT_ALCS_Resp := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := FL_ATT_ALCS_Resp.ReadString('Database', 'Server', '');
    aDatabase := FL_ATT_ALCS_Resp.ReadString('Database', 'DB', 'QM');
    ausername := FL_ATT_ALCS_Resp.ReadString('Database', 'UserName', '');
    apassword := FL_ATT_ALCS_Resp.ReadString('Database', 'Password', '');

    flogDir := FL_ATT_ALCS_Resp.ReadString('LogPaths', 'LogPath', '');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);


  finally
    FL_ATT_ALCS_Resp.Free;
  end;
end;

function TfrmMainATT.processParams: boolean;
begin
  RespondTo   :=ParamStr(1);
  Callcenter := ParamStr(2);
end;

function TfrmMainATT.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
