program FL_ATT_ALCS_Resp;
//qm-662
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  uATTMain in 'uATTMain.pas' {frmMainATT};

var
  MyInstanceName: string;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.Title := '';
  Application.CreateForm(TfrmMainATT, frmMainATT);
  Application.Run;
  end
  else
    Application.Terminate;
end.
