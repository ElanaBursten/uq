program Rodl2Pas;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  uRODL,
  uRORes,
  uROResWriter,
  uRODLToIntf,
  uRODLToInvk,
  uRODLToXML,
  OdMSXMLUtils,
  rodlXMLtoThrift in 'rodlXMLtoThrift.pas',
  ActiveX,
  RODLTopSort in 'RODLTopSort.pas',
  OdMiscUtils,
  Windows;

var
  FileName: string;
  ThriftOutputFileName : string;
  ROToThriftMapFileName: string;

function GenUnit(aConverterClass: TRODLConverterClass; aLibrary: TRODLLibrary): Boolean;
var
  Conv: TRODLConverter;
  UnitName: string;
begin
  Conv := nil;
  try
    Conv := aConverterClass.Create(nil);
    UnitName := ExtractFilePath(FileName) + Conv.GetTargetFileName(aLibrary);
    Conv.Convert(aLibrary);
    Conv.Buffer.SaveToFile(UnitName);
    Result := True;
  finally
    FreeAndNil(Conv);
  end;
end;

function GenerateRESFromRODL(const RODLFileName: string): Boolean;
var
  ResName: string;
  ResData, RodlData: TFileStream;
begin
  RodlData := nil;
  ResData := nil;
  try
    ResName := IncludeTrailingPathDelimiter(ExtractFilePath(RODLFileName)) + res_RODLFile + '.res';
    ResData := TFileStream.Create(ResName, fmCreate);
    RodlData := TFileStream.Create(RODLFileName, fmOpenRead + fmShareDenyNone);
    RodlData.Position := 0;
    WriteRes(RodlData, ResData, res_RODLFile);
    Result := FileExists(ResName);
  finally
    FreeAndNil(RodlData);
    FreeAndNil(ResData);
  end;
end;

function GenerateThriftFromRODL: Boolean;
var
  ThriftFile: string;
begin
  //check for existence of the command file since we don't auto generate the main QMApi file anymore.
  ThriftFile := AppendTextToFilename(ThriftOutputFileName, 'Command');
  if FileExists(ThriftFile) then
    DeleteFile(PAnsiChar(ThriftFile)); //sr
  //Attempting to use the RO library to read the objects caused numerous issues-
  //so parse the RODL as XML.
  try
    CoInitialize(nil);
    try
      ConvertToThriftFromXML(FileName, ROToThriftMapFileName, ThriftOutputFileName)
    finally
      CoUninitialize;
    end;
  except
    on E: Exception do begin
      ExitCode := 1;
      Writeln(Format('[%s] %s', [E.ClassName, E.Message]));
    end;
  end;
  Result := FileExists(ThriftFile);
end;

procedure MakePasFiles(const RodlFile: string);
var
  Lib: TRODLLibrary;
begin
  Writeln('Generating .pas files from ' + RodlFile);
  Lib := ReadRODLFromFile(TXMLToRODL, RodlFile);
  Writeln('Generating Intf file from ' + RodlFile);
  GenUnit(TRODLToIntf, Lib);
  Writeln('Generating Inkv file from ' + RodlFile);
  GenUnit(TRODLToInvk, Lib);
  Writeln('Generating Res file from ' + RodlFile);
  GenerateRESFromRODL(RodlFile);
end;

begin
  try
    //Example: "C:\Oasis Digital\QManager\uq-qm\server\QMServerLibrary.rodl"
    if ParamCount < 1 then
      raise Exception.Create('Command Syntax: rodl2pas.exe filename.rodl' + #13#10 +
                             ' or, to also generate thrift files use ' +#13#10 +
                             'Command Syntax: rodl2pas.exe filename.rodl ThriftOutputFileName.thrift RODLToThriftMappingsFileName.ini');
    FileName := ParamStr(1);
    if not SameText(ExtractFileExt(FileName), '.rodl') then
      raise Exception.Create('Not a .rodl file: ' + FileName);
    if not FileExists(FileName) then
      raise Exception.Create('RODL file does not exist: ' + FileName);
    //Only check for the thrift parameters if another parameter exists
    //Example: "C:\Oasis Digital\QManager\uq-qm\server\QMServerLibrary.rodl" "C:\Oasis Digital\QManager\uq-qm\server2\QMApi.Thrift" "C:\Oasis Digital\QManager\uq-qm\server\RODLToThrift.ini"
    if ParamCount > 1 then begin
      ThriftOutputFileName := ParamStr(2);
      if not SameText(ExtractFileExt(ThriftOutputFileName), '.thrift') then
        raise Exception.Create('Not a .thrift file: ' + ThriftOutputFileName);
      ROToThriftMapFileName := ParamStr(3);
      if not SameText(ExtractFileExt(ROToThriftMapFileName), '.ini') then
        raise Exception.Create('Not an .ini file: ' + ROToThriftMapFileName);
      if not FileExists(ROToThriftMapFileName) then
        raise Exception.Create('RODL to Thrift mapping file does not exist: ' + ROToThriftMapFileName);
    end;

    MakePasFiles(FileName);
    if NotEmpty(ThriftOutputFileName) then begin
      if GenerateThriftFromRODL then
        Writeln('Finished generating .thrift files from ' + FileName)
      else
        Writeln('Problem generating .thrift files from ' + FileName);
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

