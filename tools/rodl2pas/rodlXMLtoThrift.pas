unit rodlXMLtoThrift;

interface

uses
  Classes, MSXML2_TLB, OdMSXMLUtils, SysUtils, RODLTopSort, OdMiscUtils, StrUtils,
  IniFiles;

const
{ * Possible Thrift types; are case sensitive:
 *  bool        Boolean, one byte
 *  byte        Signed byte
 *  i16         Signed 16-bit integer
 *  i32         Signed 32-bit integer
 *  i64         Signed 64-bit integer
 *  double      64-bit floating point value
 *  string      String
 *  binary      Blob (byte array)
 *  map<t1,t2>  Map from one type to another
 *  list<t1>    Ordered list of one type
 *  set<t1>     Set of unique elements of one type

  + a custom datatype we add for Datetime
 * datetimestring Corresponds to Delphi datetime}
  ThriftDataTypes: array[0..10] of string = ('bool','byte','i16','i32','i64','double','string','binary','map','list','set');
  CustomThriftDataTypes: array[0..1] of string = ('datetimestring','CurrencyScaled');

procedure AddThriftFileHeader(Thrift: TStrings; const ThriftFileName: string);
procedure AddThriftIncludeSection(Thrift: TStrings; const ThriftOutputFileName: string);
procedure AddThriftIncludeSharedSection(Thrift: TStrings; const ThriftOutputFileName: string);
procedure AddStructsComments(Thrift: TStrings);
procedure AddSectionComments(SectionName: string; Thrift: TStrings);
//If we need to map more than query names then the map file would be better saved as JSON. Due to time, just use name=value text file for now.
procedure ConvertToThriftFromXML(XMLFileName:string;RODLToThriftMapFileName,ThriftOutputFileName:string);
procedure ParseRODLtoTasks(XMLFileName:string; TaskList: TRODLTaskList;RODLToThriftMapFileName,ThriftOutputFileName:string);
procedure AddCustomTypeDefs(Thrift: TStrings);
function TranslateDelphiToThriftType(DelphiType:string; SharedPrefix:string=''):string;

var
  ServiceName: string;

implementation
{TODO source comments from Kyle to address:
  - We need to remove NVPairList: We still have the scourge of all of these name value pair lists and lists thereof. I'm not sure how to effectively move forward from these. Of course what we really want is to replace them with type data, and to make sure we can access that typed data in a generic way where needed, while in a typed way where that makes more sense. Rather than give up all of it and just pass this generic type all over.
  - Need to add Thrift exception: I think we need to handle success and failure in a more global way. Is there a notion of exceptiosn to indicate failure in Thrift?
So far I have been thinking that a command either succeeds or fails; and that a compound command also either succeeds or fails as a single transaction. I am leery of the 2^n-1 failure modes if each piece of a compound command can success or fail.
  - Regarding struct SyncDown6Query, 7: string TicketInCachePacked: I think I have really good news here. I think we can disregard this past thing now, and instead use thrift to encode the list normally. If we find we want to encode more tightly, we can use a more compact thrift encoder, rather than have that code to manually encode this ticket list.
  - Regarding QMApiShared.NVPairList AdditionalInfo: I wonder if we could flatten that additional info back out into the structure, rather than leave it as arbitrary name value pairs.
 }

function TranslateDelphiToThriftType(DelphiType, SharedPrefix:string):string;
begin
  if SameText(DelphiType,'Integer') then
    Result := 'i32'
  else if SameText(DelphiType,'String') then
    Result := 'string'
  else if SameText(DelphiType,'Boolean') then
    Result := 'bool'
  else if SameText(DelphiType,'DateTime') then
    Result := 'datetimestring'
  else if SameText(DelphiType,'Double') then
    Result := 'double'
  {Per Kyle, how we will handle currency: But, here is a reasonable thing for us to do: use the i64 type,
  and store a scaled integer (scale of 10000, allowing 4 digits to the right of the decimal point). Scaled
  integers are a common way to effectively store currency, and avoid the numerous problems with floating point.
  Floating point is unsuitable for currency. Pages worth a quick read:
  http://blog.rietta.com/blog/2012/03/03/best-data-types-for-currencymoney-in/
  http://www.postgresql.org/docs/9.3/static/datatype-money.html}
  else if SameText(DelphiType,'Currency') then
    Result := 'CurrencyScaled'
  else
    Result := DelphiType;

  if NotEmpty(SharedPrefix) then begin
    //if not in thrift types, then the type lives in the shared file and must be referenced there
    if (NOT StringInArray(Result, ThriftDataTypes)) then
      Result := SharedPrefix + '.' + Result;
  end;
end;

procedure AddThriftFileHeader(Thrift: TStrings; const ThriftFileName: string);
begin
  Thrift.Add('//*********************************************************');
  Thrift.Add(format('//                   %s                    ',[ThriftFileName]));
  Thrift.Add('//*********************************************************');
  Thrift.Add('');
  Thrift.Add('# Thrift IDL file for a service. This file contains the shared types, structs, etc.');
  Thrift.Add('#');
  Thrift.Add('# This file is used by the Thrift compiler to generate Delphi code shared');
  Thrift.Add('# between servers and clients.');
  Thrift.Add('#');
  Thrift.Add('# Before running this file, you will need to have installed the thrift compiler');
  Thrift.Add('# (the version we are using is located in \bin).');
  Thrift.Add('#');
  Thrift.Add('# Sample command to generate the source:');
  Thrift.Add('# ..\bin\thrift-0.9.1.exe -verbose -r -o .\ --gen delphi:register_types QMApi.thrift');
  Thrift.Add('#');
  Thrift.Add('# Follow Thrift best practices here: http://diwakergupta.github.io/thrift-missing-guide/#_best_practices');
  Thrift.Add('# Most importantly, do not change the numeric tags for any existing fields!');
  Thrift.Add('');
end;

procedure AddThriftIncludeSection(Thrift: TStrings; const ThriftOutputFileName: string);
begin
  Thrift.Add('/*');
  Thrift.Add(' * Thrift files can reference other Thrift files to include common struct');
  Thrift.Add(' * and service definitions. These are found using the current path, or by');
  Thrift.Add(' * searching relative to any paths specified with the -I compiler flag.');
  Thrift.Add(' *');
  Thrift.Add(' * Included objects are accessed using the name of the .thrift file as a');
  Thrift.Add(' * prefix. i.e. MyServiceShared.SharedObject');
  Thrift.Add(' */');
  Thrift.Add(Format('include "%sShared.thrift"',[ChangeFileExt(ThriftOutputFileName,'')]));
  Thrift.Add(Format('include "%sCommand.thrift"',[ChangeFileExt(ThriftOutputFileName,'')]));
  Thrift.Add(Format('include "%sQuery.thrift"',[ChangeFileExt(ThriftOutputFileName,'')]));
  Thrift.Add('');
end;

procedure AddThriftIncludeSharedSection(Thrift: TStrings; const ThriftOutputFileName: string);
begin
  Thrift.Add('/*');
  Thrift.Add('');
  Thrift.Add(' * Thrift files can reference other Thrift files to include common struct');
  Thrift.Add(' * and service definitions. These are found using the current path, or by');
  Thrift.Add(' * searching relative to any paths specified with the -I compiler flag.');
  Thrift.Add(' *');
  Thrift.Add(' * Included objects are accessed using the name of the .thrift file as a');
  Thrift.Add(' * prefix. i.e. MyServiceShared.SharedObject');
  Thrift.Add(' */');
  Thrift.Add(Format('include "%sShared.thrift"',[ChangeFileExt(ThriftOutputFileName,'')]));
  Thrift.Add('');
end;

procedure AddStructsComments(Thrift: TStrings);
begin
  Thrift.Add('');
  Thrift.Add('//*********************************************************');
  Thrift.Add('//# Structs');
  Thrift.Add('//*********************************************************');
  Thrift.Add('//TODO: We need to analyze and come to consensus on which fields should be marked required.');
  Thrift.Add('// It will be problematic to change a required field to an optional field:');
  Thrift.Add('// "old readers will consider messages without this field to be incomplete and may reject or drop them //unintentionally. You should consider writing application-specific custom validation routines');
  Thrift.Add('// for your //buffers instead. Some have come the conclusion that using required does more harm than good; they prefer //to use only optional. However, this view is not universal."  ');
  Thrift.Add('//http://diwakergupta.github.io/thrift-missing-guide/#_includes');
  Thrift.Add('');
end;

procedure AddSectionComments(SectionName: string; Thrift: TStrings);
begin
  Thrift.Add('');
  Thrift.Add('//*********************************************************');
  Thrift.Add(format('# %s',[SectionName]));
  Thrift.Add('//*********************************************************');
  Thrift.Add('');
end;

procedure AddCustomTypeDefs(Thrift: TStrings);
begin
  Thrift.Add('//Thrift does not have a DateTime data type, so we will use a string and create');
  Thrift.Add('// an alternate name for the type that makes it easier to read');
  Thrift.Add('typedef string datetimestring');
  Thrift.Add('');
  Thrift.Add('//Thrift does not have a Currency data type, so we will use i64 and create');
  Thrift.Add('// an alternate name for the type that makes it easier to read');
  Thrift.Add('typedef i64 CurrencyScaled');
  Thrift.Add('');
end;

procedure ConvertToThriftFromXML(XMLFileName:string; RODLToThriftMapFileName, ThriftOutputFileName:string);
var
  ThriftBuffer : TStringList;
  ThriftCommandBuffer: TStringList;
  ThriftQueryBuffer: TStringList;
  ThriftSharedBuffer: TStringList;

  ThriftRecursiveBuffer: TStringList;

  //Vars needed for sorting
  TaskList : TRODLTaskList;
  RecursiveTasks: TStringArray;
  i: Integer;

  procedure WriteIncludedThriftFile(IncludeFileName: string; IncludeFileBuffer: TStrings; IncludeShared: Boolean=True);
  begin
    //Now reset the ThriftBuffer, construct and save the Shared file:
    ThriftBuffer.Clear;
    AddThriftFileHeader(ThriftBuffer,ExtractFileName(IncludeFileName));
    if IncludeShared then //not the shared file, so add it as an include
      AddThriftIncludeSharedSection(ThriftBuffer,ExtractFileName(ThriftOutputFileName))
    else begin
      //Custom TypeDefs go in the shared file, and must be referenced from other thrift files like: QMApiShared.datetimestring
      AddSectionComments('Custom Typedefs(type translations), Structs, and Containers', ThriftBuffer);
      AddCustomTypeDefs(ThriftBuffer);
    end;
    ThriftBuffer.AddStrings(IncludeFileBuffer);
    ThriftBuffer.SaveToFile(IncludeFileName);
  end;
begin
  TaskList := TRODLTaskList.Create;

  //Write 4 different files:
  ThriftBuffer := TStringList.Create;
  ThriftCommandBuffer:= TStringList.Create;
  ThriftQueryBuffer:= TStringList.Create;
  ThriftSharedBuffer:= TStringList.Create;

  //Keep services and recursive tasks together
  ThriftRecursiveBuffer := TStringList.Create;
  try
    AddThriftFileHeader(ThriftBuffer,ExtractFileName(ChangeFileExt(XMLFileName, '.thrift')));
    AddThriftIncludeSection(ThriftBuffer,ExtractFileName(ThriftOutputFileName));

    ParseRODLtoTasks(XMLFileName, TaskList, RODLToThriftMapFileName,ThriftOutputFileName);

    //Perform the sort; it only knows about the RO struct, array, and operation names.
    //It sorts those names so that dependent items occur first. i.e. wheels must occur before car.
    RecursiveTasks := TaskList.Sort;

    //Now write the RO structs, types, operations in topological order to the buffer.
    //Keep the service operations together, though still in topological order.
    for i := 0 to TaskList.Count - 1 do begin
      if StringInArray(TaskList.Task[i].Name, RecursiveTasks) then
        TaskList.Task[i].WriteTask(ThriftRecursiveBuffer)
      else begin
        case TaskList.Task[i].KindType of
          ktShared: TaskList.Task[i].WriteTask(ThriftSharedBuffer);
          ktCommand: TaskList.Task[i].WriteTask(ThriftCommandBuffer);
          ktQuery: TaskList.Task[i].WriteTask(ThriftQueryBuffer);
          ktApi: TaskList.Task[i].WriteTask(ThriftBuffer);
        end;
      end;
    end;

    //Finally append the problem area: recursive tasks that need handled manually (i.e. fix these in the Manual.thrift file):
    ThriftBuffer.Add('');
    if ThriftRecursiveBuffer.Count > 0 then begin
      ThriftBuffer.Add(' /*');
      AddSectionComments('The following tasks are recursive and must be manually changed to work with the Thrift Compiler', ThriftBuffer);
      ThriftBuffer.AddStrings(ThriftRecursiveBuffer);
      ThriftBuffer.Add(' */');
    end;

    //Save the ThriftBuffer to the main thrift file
//We will no longer generate the main QMApi.thrift file; instead it will be manually maintained.    ThriftBuffer.SaveToFile(ThriftOutputFileName);
    //Now reset the ThriftBuffer, construct and save the Shared file:
    WriteIncludedThriftFile(ChangeFileExt(ThriftOutputFileName, 'Shared.thrift'), ThriftSharedBuffer, False);
    //Now reset the ThriftBuffer, construct and save the Commands file:
    WriteIncludedThriftFile(ChangeFileExt(ThriftOutputFileName, 'Command.thrift'), ThriftCommandBuffer);
    //Now reset the ThriftBuffer, construct and save the Query file:
    WriteIncludedThriftFile(ChangeFileExt(ThriftOutputFileName, 'Query.thrift'), ThriftQueryBuffer);

  finally
    TaskList.Free;
    ThriftRecursiveBuffer.Free;
    ThriftCommandBuffer.Free;
    ThriftQueryBuffer.Free;
    ThriftSharedBuffer.Free;
    ThriftBuffer.Free;
  end;
end;

procedure ParseRODLtoTasks(XMLFileName:string; TaskList: TRODLTaskList;RODLToThriftMapFileName,ThriftOutputFileName:string);
const
  SelectStructNodes = '/Library/Structs/Struct';
  SelectArrayNodes = '/Library/Arrays/Array';
  SelectServiceNodes = '/Library/Services/Service';
  SelectOperationsNodes = '/Library/Services/Service/Interfaces/Interface/Operations/Operation';
  StructElementListTag = 'Element';
  ArrayElementListTag = 'ElementType';
var
  //Parsing
  XMLDoc: IXMLDOMDocument;
  TopNodeList : IXMLDOMNodeList;
  SubNodeList : IXMLDOMNodeList;
  OperationParamList: IXMLDOMNodeList;
  ANode: IXMLDOMNode;
  AnElement: IXMLDOMNode;
  AttributeText: string;
  OrigAttributeText: string;
  TopNodeCount,SubNodeCount, ParamCount: integer;
  ElementName, ElementFlag, ElementDataType: string;
  ExcludeTasksArray: TStringArray;
  ExcludeFieldsArray: TStringArray;
  //Sorting & writing
  ROElementItem : TROElementItem;
  ATask, ADependentTask: IRODLSortableTask;
//TODO:Comment these out for now, we will manage manually.  CommandUnionTask, CommandResponseUnionTask: IRODLSortableTask;
  ACQTask,ACQResponseTask: IRODLSortableTask;
  ArrayTask: IRODLSortableTask;
  MappedOpName: string;
  IniFile: TIniFile;
  ExcludeOps: TStringList;
  ExcludeFields: TStringList;
  QueryOps : TStringList;
  ChangeListOps: TStringList;
  ReplaceWithManualOps: TStringList;
  NewManualCommandList: TStringList;
  NewManualResponsesList: TStringList;
  PurposeCommentList: TStringList;
  CommandOps: TStringList;
  StructToCmdList: TStringList;
  ROOpName: string;
//  i: Integer;  //used by logic for unions. Commenting out to avoid hints/warnings
//  WorkStr: string;

  procedure CheckForDependentTask(aTask: IRODLSortableTask; var ElementDataType: string);
  begin
    if ( (NOT StringInArray(ElementDataType, ThriftDataTypes)) and (NOT StringInArray(ElementDataType, CustomThriftDataTypes)) ) then begin //this task depends on the ElementDataType
      ADependentTask := TaskList.getTaskByName(ElementDataType);
      //The Thrift compiler is case sensitive; it will fail even if the tasks are
      //in the right topological order - if the case does not match. Handle that:
      if ADependentTask <> nil then
        ElementDataType := ADependentTask.Name;

      //Create the dependent task if it does not exist yet
      if ADependentTask = nil then begin
        ADependentTask := TRODLTask.Create(ElementDataType);
        TaskList.Add(ADependentTask);
      end;
      //Now add the dependency
      if Assigned(ADependentTask) then
        aTask.AddDependency(ADependentTask);
    end;
  end;

  function MakeTask(Name, Kind: string; KindType: TROKindType): IRODLSortableTask;
  begin
    Result := nil;
    //First see if the task already exists, if not then create it and add to the list.
    Result := TaskList.getTaskByName(Name);
    if Result = nil then begin
      Result := TRODLTask.Create(Name);
      TaskList.Add(Result);
    end;
    Result.Kind := Kind;
    Result.KindType := KindType;
  end;

  procedure AddElementToTask(Task: IRODLSortableTask; Name, DataType: string; Flag: string = '');
  begin
    ROElementItem := Task.ElementList.Add;
    ROElementItem.Name := Name;
    ROElementItem.ElementDataType := DataType;
    if not IsEmpty(Flag) then
      ROElementItem.Flag := Flag;
  end;

  procedure UnfoldListType(var ElementDataType, ElementName: string);
  begin
    //Unfold ChangeList/List data types
    if (ChangeListOps.IndexOf(ElementDataType) > -1) then begin
      ArrayTask := TaskList.getTaskByName(ElementDataType); //This is why we need the array task objects even though we don't write them.
      if ArrayTask <> nil then begin
        ElementDataType := ArrayTask.DataType;
        if ArrayTask.DataType = 'i32' then
          //If it's a former integer list, they are a little different. We don't want 'Newi32'.
          //Instead use the element name but strip "Array" if it's present.
          ElementName := StringReplace(ElementName, 'Array', '',[rfReplaceAll,rfIgnoreCase])
        else
          ElementName := ArrayTask.DataType;
      end;
    end;
  end;

  procedure SetTaskComment(theTask: IRODLSortableTask; ROSourceComment: string);
  var
    PurposeComment: string;
  begin
    theTask.Comment := ROSourceComment;
    if PurposeCommentList.IndexOfName(theTask.Name) > -1 then begin
      PurposeComment := WrapText('/* ' +
        StringReplace(PurposeCommentList.ValueFromIndex[PurposeCommentList.IndexOfName(theTask.Name)], '#13#10', #13#10,[rfReplaceAll])
        + '*/', 100);
      theTask.Comment := theTask.Comment + #13#10 + PurposeComment;
    end;
  end;
begin
  XMLDoc := LoadXMLFile(XMLFileName);
  IniFile := TIniFile.Create(RODLToThriftMapFileName); 
  ExcludeOps := TStringList.Create;
  ExcludeFields := TStringList.Create;
  QueryOps := TStringList.Create;
  ChangeListOps := TStringList.Create;
  ReplaceWithManualOps := TStringList.Create;
  CommandOps := TStringList.Create;
  StructToCmdList := TStringList.Create;
  NewManualCommandList := TStringList.Create;
  NewManualResponsesList := TStringList.Create;
  PurposeCommentList := TStringList.Create;
  try
    //Build list of Ops, structs, etc. from ini file that must be massaged in various ways
    IniFile.ReadSectionValues('Query',QueryOps);
    IniFile.ReadSection('Exclude',ExcludeOps); //Just need the names
    IniFile.ReadSectionValues('ExcludeFields', ExcludeFields);
    IniFile.ReadSection('ChangeList',ChangeListOps);
    IniFile.ReadSectionValues('ReplaceWithManual',ReplaceWithManualOps);
    IniFile.ReadSectionValues('Command',CommandOps);
    IniFile.ReadSectionValues('ROStructToThriftCommand', StructToCmdList);
    IniFile.ReadSectionValues('NewManualCommands', NewManualCommandList);
    IniFile.ReadSectionValues('NewManualCommandResponses', NewManualResponsesList);
    IniFile.ReadSectionValues('PurposeComment', PurposeCommentList);

    //Exclude these elements 
    ExcludeTasksArray := StringsToStringArray(ExcludeOps);

{//No need to manually add these wrapper structs anymore. Now they are placed in the main QMApi.thrift which we will not autogenerate anymore.
    //Use QMApi Structs vs outer union. The purpose of these structs are to permit serialization/deserialization methods which require
    //a descendent of IBase. i.e if we just set these up as a couple typedef's - IThriftList cannot be used by the serialization/
    //deserialization methods.
    aTask := MakeTask('QMApiCommands', 'Struct', ktApi); //Add struct task; the struct name is the name of the task
    AddElementToTask(aTask, 'BatchCommands', 'list<QMApiCommand.Command>');
    aTask := MakeTask('QMApiResponses', 'Struct', ktApi); //Add struct task; the struct name is the name of the task
    AddElementToTask(aTask, 'BatchCommandResponses', 'list<QMApiCommand.CommandResponse>');}

    //UNIONS - these are added for Thrift and do not exist in RemObjects (RO).
    //Add Command union and CommandResult union. Every command should have an element in the CommandUnionTask.

//    CommandUnionTask := MakeTask('CommandUnion', 'Union', ktCommand);
//    CommandResponseUnionTask := MakeTask('CommandResponseUnion', 'Union', ktCommand);

{//No need to manually add these wrapper structs anymore. Now they are placed in the main QMApi.thrift which we will not autogenerate anymore.
    aTask := MakeTask('Command', 'Struct', ktCommand);
    AddElementToTask(aTask, 'EmpID', 'i32');
    AddElementToTask(aTask, 'CreateDateTime', 'QMApiShared.datetimestring');
    AddElementToTask(aTask, 'Union', 'QMApiManual.CommandUnion');

    aTask := MakeTask('CommandResponse', 'Struct', ktCommand);
    AddElementToTask(aTask, 'Success', 'bool');
    AddElementToTask(aTask, 'Union', 'QMApiManual.CommandResponseUnion'); }

  //RO ARRAYS. Correspond to Thrift Typedefs
    TopNodeList := XMLDoc.selectNodes(SelectArrayNodes);
    for TopNodeCount:= 0 to TopNodeList.length - 1 do begin
      ANode := (TopNodeList[TopNodeCount] as IXMLDOMNode);
      if Assigned(ANode) then begin
        GetAttributeTextFromNode(ANode, 'Name', AttributeText);//Array Name
        aTask := MakeTask(AttributeText, 'Array', ktShared); //Add array task; the array name is the name of the task
        SetTaskComment(aTask, 'Replaces RemObjects Array: ' + AttributeText);
        //We need to exclude writing some arrays, but we still need the tasks for later analysis
        aTask.Excluded := StringInArray(AttributeText,ExcludeTasksArray);

        //Iterate array elements and gather for each: DataType
        SubNodeList := (TopNodeList[TopNodeCount] as IXMLDOMElement).getElementsByTagName(ArrayElementListTag);
        for SubNodeCount := 0 to SubNodeList.length - 1 do begin
          AnElement := (SubNodeList.item[SubNodeCount] as IXMLDOMNode);
          if Assigned(AnElement) then begin
            GetAttributeTextFromNode(AnElement, 'DataType', ElementDataType);
            ElementDataType := TranslateDelphiToThriftType(ElementDataType);
            //Now look for task dependencies in the element loop
            CheckForDependentTask(aTask, ElementDataType);
            //No need to collect elements(or flag) with array type; just get the datatype to store in the Task.
            aTask.DataType := ElementDataType;
          end; //if array element assigned
        end;//array elements list
      end;//if array node assigned
    end;//Arrays

  //RO STRUCTS. Loop through RO Structs and make them Thrift structs.
  //TODO: We need to carefully decide which struct elements should be required or not.
    TopNodeList := XMLDoc.selectNodes(SelectStructNodes);
    for TopNodeCount:= 0 to TopNodeList.length - 1 do begin
      ANode := (TopNodeList[TopNodeCount] as IXMLDOMNode);
      if Assigned(ANode) then begin
        GetAttributeTextFromNode(ANode, 'Name', AttributeText);//Struct Name
        if (StructToCmdList.IndexOfName(AttributeText) > -1) then begin//rename structs to commands if designated in ini file
          OrigAttributeText := AttributeText;
          AttributeText := StructToCmdList.ValueFromIndex[StructToCmdList.IndexOfName(AttributeText)];
          //and in this case, the struct is considered a command; specify ktCommand so it gets written to Command thrift file.
          aTask := MakeTask(AttributeText + 'Command', 'Struct', ktCommand); //Add struct task; the struct name is the name of the task
          SetTaskComment(aTask, 'Replaces RemObjects Struct: ' + OrigAttributeText);
          //Add to command union
//          AddElementToTask(CommandUnionTask, AttributeText, AttributeText + 'Command');
//          CommandUnionTask.AddDependency(aTask);
        end
        else begin
          aTask := MakeTask(AttributeText, 'Struct', ktShared); //Add struct task; the struct name is the name of the task
          SetTaskComment(aTask, 'Replaces RemObjects Struct: ' + AttributeText);
        end;
        aTask.Excluded := StringInArray(AttributeText,ExcludeTasksArray); //Exclude some structs that are being replaced such as NewTicket

        // Iterate struct elements and gather for each: Name, Datatype
        SubNodeList := (TopNodeList[TopNodeCount] as IXMLDOMElement).getElementsByTagName(StructElementListTag);
        for SubNodeCount := 0 to SubNodeList.length - 1 do begin
          AnElement := (SubNodeList.item[SubNodeCount] as IXMLDOMNode);
          if Assigned(AnElement) then begin
            GetAttributeTextFromNode(AnElement, 'Name', ElementName);
            if StringInArray(ElementName,ExcludeTasksArray) then //Exclude some struct elements like SecSession
              Continue;
            //Exclude some fields, but only from some structs
            if (ExcludeFields.IndexOfName(aTask.Name) > -1) then begin
              ExcludeFieldsArray := DelimitedStringToStringArray(ExcludeFields.ValueFromIndex[ExcludeFields.IndexOfName(aTask.Name)]);
              if StringInArray(ElementName, ExcludeFieldsArray) then
                Continue;
            end;
            GetAttributeTextFromNode(AnElement, 'DataType', ElementDataType);
            UnfoldListType(ElementDataType, ElementName);
            if aTask.KindType = ktShared then
              ElementDataType := TranslateDelphiToThriftType(ElementDataType)
            else
              ElementDataType := TranslateDelphiToThriftType(ElementDataType, ExtractFileName(ChangeFileExt(ThriftOutputFileName, 'Shared')));
            //Sort structure: Now look for task dependencies in the element loop
            CheckForDependentTask(aTask, ElementDataType);
            //Writing structure
            AddElementToTask(aTask,ElementName,ElementDataType);
          end;//if struct element exists
        end;//struct elements list
      end;//if struct exists
    end;//Structs

  //ENUMS - We don't have any in our current RODL file so did not implement

  //RO OPERATIONS. Loop through RO Ops and make them Thrift Structs.
  //Command structs are unionized into a batch service for more efficient calls.
  //Query structs - it is not necessary to unionize these. Simply serialize and deserialize.
    TopNodeList := XMLDoc.selectNodes(SelectServiceNodes);
    for TopNodeCount:= 0 to TopNodeList.length - 1 do begin
      ANode := (TopNodeList[TopNodeCount] as IXMLDOMNode);
      if Assigned(ANode) then begin
        GetAttributeTextFromNode(ANode, 'Name', AttributeText); //Service Name
        ServiceName := AttributeText;
      end;

      //RO OPERATIONS fyi: Service functions are the thrift equivalent of RODL operations
      //Operation Name: Parameter Name, Parameter Flag, Parameter Datatype, NumReturnFlags
      SubNodeList := XMLDoc.selectNodes(SelectOperationsNodes);
      for SubNodeCount := 0 to SubNodeList.length - 1 do begin
        ANode := (SubNodeList[SubNodeCount] as IXMLDOMNode);
        if Assigned(ANode) then begin
          GetAttributeTextFromNode(ANode, 'Name', AttributeText); //Operation Name
          if StringInArray(AttributeText,ExcludeTasksArray) then //Exclude some ops that are being replaced like SaveNewTickets
            Continue;
          ROOpName := AttributeText;

          //Create command or query task
          ACQResponseTask := nil;
          MappedOpName := QueryOps.ValueFromIndex[QueryOps.IndexOfName(AttributeText)]; //Determine if it's a Command or Query operation.
          if NotEmpty(MappedOpName) then begin //Is Query Operation
            //Determine if a custom Response struct is needed based on the result parameter. If not string or integer, then we need
            // to accomodate result types like LoginData, LoginData2
            OperationParamList := (SubNodeList[SubNodeCount] as IXMLDOMElement).getElementsByTagName('Parameter');
            for ParamCount := 0 to OperationParamList.length - 1 do begin
              AnElement := (OperationParamList.item[ParamCount] as IXMLDOMNode);
              if Assigned(AnElement) then begin
                GetAttributeTextFromNode(AnElement, 'Flag', ElementFlag);
                if ElementFlag='Result' then begin
                  GetAttributeTextFromNode(AnElement, 'Name', ElementName);
                  GetAttributeTextFromNode(AnElement, 'DataType', ElementDataType);
                  ElementDataType := TranslateDelphiToThriftType(ElementDataType, ExtractFileName(ChangeFileExt(ThriftOutputFileName, 'Shared')));
                  if ((ElementDataType <> 'string') and (ElementDataType <> 'i32')) then begin
                    //Add a result task for this op; i.e. handle LoginData, LoginData2, and other such results
                    ACQResponseTask := MakeTask(MappedOpName + 'Response', 'Struct', ktQuery);
                    SetTaskComment(ACQResponseTask, 'Derived from Result Flag in RemObjects Operation: ' + ROOpName);
                    //Add the special response type element
                    CheckForDependentTask(ACQResponseTask, ElementDataType);
                    AddElementToTask(ACQResponseTask, ElementName, ElementDataType, ElementFlag);
                  end;//if not basic string or integer
                end; //if ElementFlag='Result'
              end; //if Assigned(AnElement)
            end;//for OperationParamList

            //Now we need a task for the query struct
            ACQTask := MakeTask(MappedOpName, 'Struct', ktQuery);//Use name from map file
          end
          else begin//is Command operation
            //Add operation tasks- each operation will become a Command and maybe a CommandResponse if the RO has Result or Out params
            //Check to see if Op Command needs to be renamed
            if (CommandOps.IndexOfName(AttributeText) > -1) then
              AttributeText := CommandOps.ValueFromIndex[CommandOps.IndexOfName(AttributeText)];
            ACQTask := MakeTask(AttributeText + 'Command', 'Struct', ktCommand);//(the OpName+Command struct)

            //Now that we know about the operation structs, add each to their respective union
            //Command
//            AddElementToTask(CommandUnionTask, AttributeText, AttributeText + 'Command');
            //Add operation/struct dependency for union task so that union will sort after operations/structs
//            CommandUnionTask.AddDependency(ACQTask);

            //For now, a Command Response is needed. Determine if any parameter's flag is "Result" or "Out"
            OperationParamList := (SubNodeList[SubNodeCount] as IXMLDOMElement).getElementsByTagName('Parameter');
            for ParamCount := 0 to OperationParamList.length - 1 do begin
              AnElement := (OperationParamList.item[ParamCount] as IXMLDOMNode);
              if Assigned(AnElement) then begin
                GetAttributeTextFromNode(AnElement, 'Flag', ElementFlag);
                if ((ElementFlag='Result') or (ElementFlag='Out')) then begin
                  //write the Command Response struct
                  ACQResponseTask := MakeTask(AttributeText + 'Response', 'Struct', ktCommand);//(the OpName+Response struct)
                  SetTaskComment(ACQResponseTask, 'Derived from Result and/or Out Flag in RemObjects Operation: ' + ROOpName);
{For now using a list of CommandResponse unions after all. Current client ops expect a result and each of these must be handled case-by-case
to remove the expected result and refresh the user's page appropriately. Once that is cleaned up the command response union can be removed.}
                  //Add the command response to the Command response union
//                  AddElementToTask(CommandResponseUnionTask, 'Response' + AttributeText, AttributeText + 'Response');
//                  CommandResponseUnionTask.AddDependency(ACQResponseTask);
                  Break; //Break out of the loop now that response struct was added
                end;//if Result or Out param
              end;//if Assigned an op Parameter
            end;//for
          end;
          SetTaskComment(ACQTask, 'Replaces RemObjects Operation: ' + ROOpName);

          //Loop the operation's Parameter list and place the elements in the appropriate Command or Query struct.
          OperationParamList := (SubNodeList[SubNodeCount] as IXMLDOMElement).getElementsByTagName('Parameter');
          for ParamCount := 0 to OperationParamList.length - 1 do begin
            AnElement := (OperationParamList.item[ParamCount] as IXMLDOMNode);
            if Assigned(AnElement) then begin
              GetAttributeTextFromNode(AnElement, 'Name', ElementName);
              if StringInArray(ElementName,ExcludeTasksArray) then
                Continue;
              GetAttributeTextFromNode(AnElement, 'DataType', ElementDataType);
              //Unfold ChangeList/List data types
              UnfoldListType(ElementDataType, ElementName);
              //Check for and handle NvPairLists; replace lists with structs created manually. These are in the manual thrift file.
              if (ReplaceWithManualOps.IndexOfName(ElementDataType) > -1) then
                ElementDataType := ReplaceWithManualOps.ValueFromIndex[ReplaceWithManualOps.IndexOfName(ElementDataType)]
              else //otherwise check the shared type translation
                ElementDataType := TranslateDelphiToThriftType(ElementDataType, ExtractFileName(ChangeFileExt(ThriftOutputFileName, 'Shared')));
              GetAttributeTextFromNode(AnElement, 'Flag', ElementFlag);

              //Now build the Command or Query struct elements
              //Don't write result element to query structs; otherwise write the element to the relevant struct
              if not ((ACQTask.KindType = ktQuery) and (ElementFlag = 'Result')) then begin
                if (((ElementFlag = 'Result') or (ElementFlag ='Out')) and assigned(ACQResponseTask)) then begin
                  CheckForDependentTask(ACQResponseTask, ElementDataType);
                  AddElementToTask(ACQResponseTask, ElementName, ElementDataType, ElementFlag);
                end
                else begin
                  CheckForDependentTask(ACQTask, ElementDataType);
                  AddElementToTask(ACQTask, ElementName, ElementDataType, ElementFlag);
                end;
              end;
            end;//if param exists
          end;//Operation parameter list
        end;//if op node exists
      end;//Operations

      //Add the generic Query response structs at the end
      //String results:
      ACQResponseTask := MakeTask('QueryStringResponse', 'Struct', ktQuery);
      AddElementToTask(ACQResponseTask,'Result','string');

      //Integer results:
      ACQResponseTask := MakeTask('QueryIntegerResponse', 'Struct', ktQuery);
      AddElementToTask(ACQResponseTask,'Result','i32');

{For now, we will not generate the command union and command union response. We will manage these manually.
      //Add the manual replacement commands to the command union.
      for i := 0 to ReplaceWithManualOps.Count - 1 do begin
        //remove the prefix and 'Command'
        WorkStr := StringReplace(ReplaceWithManualOps.ValueFromIndex[i],'Command','',[rfReplaceAll,rfIgnoreCase]);
        WorkStr := copy(WorkStr, pos('.',WorkStr)+1, length(WorkStr));
        AddElementToTask(CommandUnionTask, WorkStr, ReplaceWithManualOps.ValueFromIndex[i]);
      end;

      //Add the manual tasks to Command union; these are specified in the mapping ini file. Do this at the end in consideration of index numbering.
      for i := 0 to NewManualCommandList.Count - 1 do
        AddElementToTask(CommandUnionTask, NewManualCommandList.Names[i], NewManualCommandList.ValueFromIndex[i]);

      //Add manual responses to the Command result union, these are defined in the mapping ini file.
      for i := 0 to NewManualResponsesList.Count - 1 do
        AddElementToTask(CommandResponseUnionTask, NewManualResponsesList.Names[i], NewManualResponsesList.ValueFromIndex[i]);
}
    end;//Services
  finally
    FreeAndNil(IniFile);
    FreeAndNil(ExcludeOps);
    FreeAndNil(ExcludeFields);
    FreeAndNil(QueryOps);
    FreeAndNil(ChangeListOps);
    FreeAndNil(ReplaceWithManualOps);
    FreeAndNil(CommandOps);
    FreeAndNil(StructToCmdList);
    FreeAndNil(NewManualCommandList);
    FreeAndNil(NewManualResponsesList);
    FreeAndNil(PurposeCommentList);
  end;
end;

end.
