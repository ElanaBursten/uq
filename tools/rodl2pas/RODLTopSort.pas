unit RODLTopSort;
{Implementation of a topological sort in order to sort tasks so that dependencies are sorted to occur first.
Borrowed and adapted from Julian Bucknall's Topological Sort implementation in Issue# 114, February 2005 of Delphi Magazine

In this implementation- recursive (or cyclical) tasks are handled by returning them from the sort call (for the developer to handle),
instead of failing the sort. These recursive tasks are still returned in the sort result- which should also be handled by
the developer (either omitted or checked for in processing) if that is an issue.}

interface

uses
  SysUtils,
  Classes,
  OdMiscUtils;

//Classes to collect the relevant sub-elements from each RODL Struct, Array, Operation.
type
  TROKindType = (ktShared, ktQuery, ktCommand, ktApi);

type
  TROElementItem = class(TCollectionItem)
  private
    FName: string;
    FElementDataType: string;
    FFlag: string;
  published
    property Name: string read FName write FName;
    property ElementDataType: string read FElementDataType write FElementDataType;
    property Flag: string read FFlag write FFlag;
  end;

type
  TROElementList = class(TCollection)
  private
    function GetItem(Index: Integer): TROElementItem;
  public
    function Add: TROElementItem;
    property Item[Index: Integer]: TROElementItem read GetItem;
  end;

//Sorting types: RO Task information for Topological Sort; also might as well collect the info
//as we parse the RO XML. Then once sorted, we can simply write the thrift file from the sorted list.
type
  IRODLSortableTask = interface
    procedure AddDependency(aTask : IRODLSortableTask); overload;
    function GetDependencyCount : integer;
    property DependencyCount : integer read GetDependencyCount;
    function GetDependentTask(aIndex : integer) : IRODLSortableTask;
    property DependentTask[aIndex : integer] : IRODLSortableTask read getDependentTask;
    procedure WriteTask(ToBuffer: TStrings);
    function getName : string;
    property Name : string read getName;
    function GetKind: string;
    procedure SetKind(Value: string);
    property Kind: string read GetKind write SetKind;  //struct, array, union, operation
    function GetKindType: TROKindType;
    procedure SetKindType(Value: TROKindType);
    property KindType: TROKindType read GetKindType write SetKindType; //Command, Query, or Shared
    function GetDataType: string;
    procedure SetDataType(Value: string);
    property DataType: string read GetDataType write SetDataType;
    function GetElementList: TROElementList;
    procedure SetElementList(Value: TROElementList);
    property ElementList: TROElementList read GetElementList write SetElementList;
    function GetComment: string;
    procedure SetComment(Value: string);
    property Comment: string read GetComment write SetComment;  //a comment to be written to the Thrift file just before the task
    function GetExcluded: Boolean;
    procedure SetExcluded(Value: Boolean);
    property Excluded: Boolean read GetExcluded write SetExcluded;  //Determines if a task should be written or excluded
  end;

type
  //Implement the interface; each "Task" corresponds to a RODL Struct, Array, or Operation
  TRODLTask = class (TInterfacedObject, IRODLSortableTask)
    private
      FName : string;
      FKind: string;
      FKindType: TROKindType;
      FDataType: string;
      FElementList: TROElementList;
      FComment: string;
      FExcluded: Boolean;
    protected
      FDependencies : TInterfaceList;
    public
      constructor Create(aName : string); virtual;
      destructor Destroy; override;
      procedure AddDependency(aTask : IRODLSortableTask);
      function getDependencyCount : integer;
      property DependencyCount : integer read getDependencyCount;
      function getDependentTask(aIndex : integer) : IRODLSortableTask;
      property DependentTask[aIndex : integer] : IRODLSortableTask
                  read getDependentTask;
      procedure WriteTask(ToBuffer: TStrings);
      function getName : string;
      property Name : string read getName;
      function GetKind: string;
      procedure SetKind(Value: string);
      property Kind: string read GetKind write SetKind;
      function GetKindType: TROKindType;
      procedure SetKindType(Value: TROKindType);
      property KindType: TROKindType read GetKindType write SetKindType; //Command or Query
      function GetDataType: string;
      procedure SetDataType(Value: string);
      property DataType: string read GetDataType write SetDataType;
      function GetElementList: TROElementList;
      procedure SetElementList(Value: TROElementList);
      property ElementList: TROElementList read GetElementList write SetElementList;
      function GetComment: string;
      procedure SetComment(Value: string);
      property Comment: string read GetComment write SetComment;  //a comment to be written to the Thrift file just before the task
      function GetExcluded: Boolean;
      procedure SetExcluded(Value: Boolean);
      property Excluded: Boolean read GetExcluded write SetExcluded;  //Determines if a task should be written or excluded
  end;

type
  //Sorting information about each task
  TRODLTaskTracker = class
    private
      FTask : IRODLSortableTask;
      FBeginTime : integer;
      FEndTime : integer;
    protected
      procedure SetBeginTime(aValue : integer);
      procedure SetEndTime(aValue : integer);
    public
      constructor Create(aTask : IRODLSortableTask);
      function BeingProcessed : boolean;
      property Task : IRODLSortableTask read FTask;
      property BeginTime : integer read FBeginTime write setBeginTime;
      property EndTime : integer read FEndTime write setEndTime;
  end;

type
  CircularReferenceException = class(Exception);

type
  //List of tasks to sort
  TRODLTaskList = class
    private
    protected
      FList : TList;
      FRecursiveValues: TStringArray;
      function GetTask(aIndex : integer) : IRODLSortableTask;
      function FindTracker(aTask: IRODLSortableTask) : TRODLTaskTracker;
      function ProcessDependents(aTracker: TRODLTaskTracker; aTime: integer): integer;
      function ProcessTracker(aTracker : TRODLTaskTracker; aTime: integer): integer;
      procedure QsortTrackers(aFirst, aLast : integer);
      procedure SortTrackers;
    public
      constructor Create;
      destructor Destroy; override;
      procedure Add(aTask: IRODLSortableTask);
      function Sort: TStringArray; 
      function Count : integer;
      function GetTaskByName(Name: string): IRODLSortableTask;
      property Task[aIndex : integer] : IRODLSortableTask read getTask;
  end;

implementation

{TRODLTask}
constructor TRODLTask.Create(aName : string);
begin
  inherited Create;
  FName := aName;
  FDependencies := TInterfaceList.Create;
  FElementList := TROElementList.Create(TROElementItem);
  Excluded := False;
end;

destructor TRODLTask.Destroy;
begin
  if FElementList <> nil then begin
    FElementList.Clear;
    FElementList.Free;
  end;
  inherited;
end;

function TRODLTask.getName : string;
begin
  Result := FName;
end;

function TRODLTask.GetKind: string;
begin
  Result := FKind;
end;

procedure TRODLTask.SetKind(Value: string);
begin
  FKind := Value;
end;

function TRODLTask.GetKindType: TROKindType;
begin
  Result := FKindType;
end;

procedure TRODLTask.SetKindType(Value: TROKindType);
begin
  FKindType := Value;
end;

function TRODLTask.GetDataType: string;
begin
  Result := FDataType;
end;

procedure TRODLTask.SetDataType(Value: string);
begin
  FDataType := Value;
end;

function TRODLTask.GetElementList: TROElementList;
begin
  Result := FElementList;
end;

procedure TRODLTask.SetElementList(Value: TROElementList);
begin
  FElementList := Value;
end;

function TRODLTask.GetComment: string;
begin
  Result := FComment;
end;

procedure TRODLTask.SetComment(Value: string);
begin
  FComment := Value;
end;

function TRODLTask.GetExcluded: Boolean;
begin
  Result := FExcluded;
end;

procedure TRODLTask.SetExcluded(Value: Boolean);
begin
  FExcluded := Value;
end;

procedure TRODLTask.AddDependency(aTask : IRODLSortableTask);
begin
  FDependencies.Add(aTask);
end;

function TRODLTask.getDependencyCount : integer;
begin
  Result := FDependencies.Count;
end;

function TRODLTask.getDependentTask(aIndex : integer) : IRODLSortableTask;
begin
  Result := IRODLSortableTask(FDependencies[aIndex]);
end;

procedure TRODLTask.WriteTask(ToBuffer: TStrings);

  procedure WriteStruct;
  var
    i : integer;
  begin
    ToBuffer.Add(Format('struct %s {',[Name]));
    for i := 0 to FElementList.Count - 1 do begin
      ToBuffer.Add(format('  %s: %s %s; ',[IntToStr(i+1), FElementList.Item[i].ElementDataType, FElementList.Item[i].Name]));
    end;
    ToBuffer.Add('}');
  end;

  procedure WriteArray;
  begin
    ToBuffer.Add(format('typedef list<%s> %s', [DataType, Name]));
  end;

  procedure WriteOperation;//a.k.a. Thrift function
  var
    i : integer;
    Counter: integer;
  begin
    ToBuffer.Add(format('%s %s(',[DataType, Name]));//begin the operation
    Counter := 0;
    for i := 0 to ElementList.Count - 1 do begin
      if ElementList.Item[i].Name <> 'Result' then begin
        ToBuffer.Add(format('  %s:%s %s;',[IntToStr(Counter+1), ElementList.Item[i].ElementDataType, ElementList.Item[i].Name]));
        Inc(Counter);
      end;
    end;
    ToBuffer.Add(')');//end of the operation
  end;

  procedure WriteUnion;
  var
    i : integer;
  begin
    ToBuffer.Add('//Note that when a union field is set, all other fields are cleared.');
    ToBuffer.Add(format('union %s {',[Name]));
    for i := 0 to FElementList.Count - 1 do begin
      ToBuffer.Add(format('  %s: %s %s; ',[IntToStr(i+1), FElementList.Item[i].ElementDataType, FElementList.Item[i].Name]));
    end;
    ToBuffer.Add('}');
  end;

begin
  if not Excluded then begin
    if NotEmpty(Comment) then
      ToBuffer.add(Format('// %s ', [Comment]));
    if Kind = 'Array' then
      WriteArray
    else if Kind = 'Struct' then
      WriteStruct
    else if Kind = 'Operation' then
      WriteOperation
    else if Kind = 'Union' then
      WriteUnion;
  end;
end;

{TRODLTaskTracker}
constructor TRODLTaskTracker.Create(aTask : IRODLSortableTask);
begin
  inherited Create;
  FTask := aTask;
  FBeginTime := 0;
  FEndTime := 0;
end;

procedure TRODLTaskTracker.SetBeginTime(aValue : integer);
begin
  if (FBeginTime = 0) then
    FBeginTime := aValue;
end;

procedure TRODLTaskTracker.SetEndTime(aValue : integer);
begin
  if (FBeginTime <> 0) and (FEndTime = 0) then
    FEndTime := aValue;
end;

function TRODLTaskTracker.BeingProcessed : boolean;
begin
  Result := (FBeginTime <> 0) and (FEndTime = 0);
end;


{TRODLTaskList}
constructor TRODLTaskList.Create;
begin
  inherited Create;
  FList := TList.Create;
end;

destructor TRODLTaskList.Destroy;
var
  i : integer;
begin
  if (FList <> nil) then begin
    for i := 0 to FList.Count - 1 do
      TRODLTaskTracker(FList[i]).Free;
    FList.Free;
  end;
  inherited Destroy;
end;

procedure TRODLTaskList.Add(aTask : IRODLSortableTask);
begin
  FList.Add(TRODLTaskTracker.Create(aTask));
end;

function TRODLTaskList.Count : integer;
begin
  Result := FList.Count;
end;

function TRODLTaskList.GetTask(aIndex : integer) : IRODLSortableTask;
begin
  Result := TRODLTaskTracker(FList[aIndex]).Task;
end;

function TRODLTaskList.GetTaskByName(Name: string): IRODLSortableTask;
var
  i: integer;
begin
  Result := nil;
  for I := 0 to FList.Count - 1 do begin
    if SameText(TRODLTaskTracker(FList[i]).Task.Name,Name) then begin //FYI:SameText catches some case inconsistencies in the RODL file; return the task even if case does not match
      Result := TRODLTaskTracker(FList[i]).Task;
      break;
    end;
  end;
end;

function TRODLTaskList.Sort: TStringArray;
var
  i : integer;
  tracker : TRODLTaskTracker;
  time : integer;
begin
  time := 0;
  for i := 0 to FList.Count - 1 do begin
    tracker := TRODLTaskTracker(FList[i]);
    if (tracker.EndTime = 0) then begin
      time := ProcessTracker(tracker, time);
    end;
  end;
  sortTrackers;
  Result := FRecursiveValues;
end;

function TRODLTaskList.FindTracker(aTask : IRODLSortableTask) : TRODLTaskTracker;
var
  i : integer;
begin
  for i := 0 to FList.Count - 1 do begin
    Result := TRODLTaskTracker(FList[i]);
    if (Result.Task = aTask) then
      Exit;
  end;
  Result := nil;
end;

function TRODLTaskList.ProcessTracker(aTracker : TRODLTaskTracker; aTime: integer): integer;
begin
  inc(aTime);
  aTracker.BeginTime := aTime;
  aTime := ProcessDependents(aTracker, aTime);
  inc(aTime);
  aTracker.EndTime := aTime;
  Result := aTime;
end;

function TRODLTaskList.ProcessDependents(aTracker: TRODLTaskTracker; aTime: integer) : integer;
var
  task : IRODLSortableTask;
  i    : integer;
  tracker : TRODLTaskTracker;
  DepTasks: string;
  j: integer;
begin
  task := aTracker.Task;
  for i := 0 to task.DependencyCount - 1 do begin
    tracker := FindTracker(task.DependentTask[i]);
    if (tracker.BeginTime = 0) then begin
      aTime := ProcessTracker(tracker, aTime);
    end
    else if (tracker.EndTime = 0) then begin
      //Instead of raising an exception, take note of the recursive tasks so that the
      //sort can proceed and the developer will know which ones require handling.
      SetLength(FRecursiveValues, length(FRecursiveValues)+1 );
      FRecursiveValues[length(FRecursiveValues)-1] := task.Name;
      for j := 0 to task.DependencyCount - 1 do begin
        DepTasks := DepTasks + ', ' + task.DependentTask[j].Name;
        SetLength(FRecursiveValues, length(FRecursiveValues)+1 );
        FRecursiveValues[length(FRecursiveValues)-1] := task.DependentTask[j].Name;
      end;
    end;
  end;
  Result := aTime;
end;

procedure TRODLTaskList.SortTrackers;
begin
  QsortTrackers(0, FList.Count - 1);
end;

function CompareTrackers(a, b : pointer) : integer;
begin
  Result := TRODLTaskTracker(a).EndTime - TRODLTaskTracker(b).EndTime;
end;

procedure TRODLTaskList.QsortTrackers(aFirst, aLast : integer);
var
  L, R  : integer;
  Pivot : pointer;
  Temp  : pointer;
begin
  {while there are at least two items to sort}
  while (aFirst < aLast) do begin
    {the pivot is the middle item}
    Pivot := FList[(aFirst+aLast) div 2];
    {set indexes and partition}
    L := pred(aFirst);
    R := succ(aLast);
    while true do begin
      repeat dec(R); until (CompareTrackers(FList[R], Pivot) <= 0);
      repeat inc(L); until (CompareTrackers(FList[L], Pivot) >= 0);
      if (L >= R) then Break;
      Temp := FList[L];
      FList[L] := FList[R];
      FList[R] := Temp;
    end;
    {quicksort the first subfile}
    if (aFirst < R) then
      QsortTrackers(aFirst, R);
    {quicksort the second subfile - recursion removal}
    aFirst := succ(R);
  end;
end;

{ TROElementList }
function TROElementList.Add: TROElementItem;
begin
  result := inherited Add as TROElementItem;
end;

function TROElementList.GetItem(Index: Integer): TROElementItem;
begin
  result := inherited Items[Index] as TROElementItem;
end;

end.
