object frmPlatsParser: TfrmPlatsParser
  Left = 0
  Top = 0
  Caption = 'frmPlatsParser'
  ClientHeight = 505
  ClientWidth = 867
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = -27
    Top = 32
    Width = 11
    Height = 473
    Align = alRight
    Color = clBtnFace
    ParentColor = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 867
    Height = 32
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 640
      Top = 10
      Width = 42
      Height = 14
      Caption = 'Sheets:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 144
      Top = 8
      Width = 3
      Height = 13
    end
    object Label3: TLabel
      Left = 9
      Top = 10
      Width = 112
      Height = 14
      Caption = 'Client Directories:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 354
      Top = 10
      Width = 37
      Height = 14
      Caption = 'Search'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cboSheets: TComboBox
      Left = 690
      Top = 5
      Width = 137
      Height = 22
      ParentCustomHint = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cboSheetsClick
      OnCloseUp = cboSheetsCloseUp
    end
    object edtSearch: TEdit
      Left = 403
      Top = 5
      Width = 147
      Height = 22
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edtSearchChange
    end
    object cboClients: TComboBox
      Left = 134
      Top = 6
      Width = 156
      Height = 22
      ParentCustomHint = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnCloseUp = cboClientsCloseUp
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 32
    Width = 371
    Height = 473
    Align = alClient
    TabOrder = 1
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 369
      Height = 471
      Align = alClient
      Columns = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      ViewStyle = vsList
      OnDblClick = ListView1DblClick
    end
  end
  object Panel4: TPanel
    Left = -16
    Top = 32
    Width = 883
    Height = 473
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 881
      Height = 452
      Align = alClient
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
    end
    object Panel1: TPanel
      Left = 1
      Top = 453
      Width = 881
      Height = 19
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 1
      object lblFetched: TLabel
        Left = 18
        Top = 1
        Width = 44
        Height = 14
        Caption = 'records:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTiffsParser
    Left = 720
    Top = 296
  end
  object FileADOConnection: TADOConnection
    ConnectionString = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 655
    Top = 240
  end
  object qryTiffsParser: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    OnFilterRecord = qryTiffsParserFilterRecord
    Parameters = <>
    SQL.Strings = (
      '')
    Left = 635
    Top = 297
  end
end
