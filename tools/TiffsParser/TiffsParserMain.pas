unit TiffsParserMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Inifiles, Vcl.ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, StrUtils, ShlObj, Commctrl;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);
type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

  type
  TShowHourGlass = class(TInterfacedObject, IInterface)
  private
    fOldCursor: TCursor;
  public
    constructor Create;
    destructor Destroy; override;
  end;

type
  TfrmPlatsParser = class(TForm)
    DataSource1: TDataSource;
    FileADOConnection: TADOConnection;
    qryTiffsParser: TADOQuery;
    Panel2: TPanel;
    cboSheets: TComboBox;
    Panel3: TPanel;
    ListView1: TListView;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    edtSearch: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    cboClients: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    lblFetched: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtSearchChange(Sender: TObject);
    procedure qryTiffsParserFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure cboSheetsCloseUp(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GetClients(IniFile: TInifile; ClientPaths: TStrings);
    procedure cboClientsCloseUp(Sender: TObject);
    procedure cboSheetsClick(Sender: TObject);
  private
    TiffsPath:string;
    SheetsIdx: Integer;
    ClientPaths: TStringList;
    IndexFilePath: string;
    LogResult: TLogResults;
    flogDir: wideString;
    procedure FindAllDirectories;
    procedure AutoSizeColDBGrid(DBGrid: TDBGrid);
    procedure ExtractFileData;
    function GetFolderDialog(Handle: Integer; Caption: string;
      var strFolder: string): Boolean;
    procedure BrowseFiles;
    function GetAppVersionStr: string;
    procedure ReadIni;
    procedure clearLogRecord;
    procedure WriteLog(LogResult: TLogResults);
    function ShowHourGlass: IInterface;
    procedure VerizonFiles;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlatsParser: TfrmPlatsParser;

implementation

{$R *.dfm}

constructor TShowHourGlass.Create;
begin
  fOldCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
end;

destructor TShowHourGlass.Destroy;
begin
  Screen.Cursor := fOldCursor;
end;

procedure TfrmPlatsParser.qryTiffsParserFilterRecord(DataSet: TDataSet;
var Accept: Boolean);
var
  I: Integer;
begin
  for I := 0 to DataSet.FieldCount - 1 do
  begin
    Accept := Pos(UpperCase(edtSearch.Text),
      UpperCase(DataSet.Fields[I].AsString))=1;
    if Accept then exit;
  end;
end;

procedure TfrmPlatsParser.VerizonFiles;
begin
  ListView1.Clear;
  qryTiffsParser.Close;
  FindAllDirectories;
end;

procedure TfrmPlatsParser.cboClientsCloseUp(Sender: TObject);
begin
     If cboClients.ItemIndex >= 0 then
   begin
     ListView1.Clear;
     qryTiffsParser.Close;
     cboSheets.Enabled := True;
     TiffsPath := ClientPaths[cboClients.ItemIndex];
     if UpperCase(cboClients.Items[cboClients.ItemIndex]) = 'VERIZON' then
     begin
       Panel4.Width := 540;
       Self.Repaint;
       VerizonFiles;
     end else
     begin
       Panel4.Width := 883;
       Self.Repaint;
       BrowseFiles;
     end;
   end;
end;

procedure TfrmPlatsParser.cboSheetsClick(Sender: TObject);
begin
   SheetsIdx := cboSheets.ItemIndex;
end;

procedure TfrmPlatsParser.cboSheetsCloseUp(Sender: TObject);
var
   Cursor: IInterface;
begin
  if cboSheets.ItemIndex <> SheetsIdx then
  begin
    if cboSheets.Items.Count = 0 then Exit;
    edtSearch.Text := '';
    qryTiffsParser.Close;
    qryTiffsParser.SQL.Text := 'Select * from [' + cboSheets.Items[cboSheets.ItemIndex]+'$' + ']';
    ShowHourGlass;
    qryTiffsParser.Open;
    AutoSizeColDBGrid(DBGrid1);
    lblfetched.Caption := 'records: ' +  inttostr(qryTiffsParser.RecordCount);
  end;
end;

procedure TfrmPlatsParser.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  r1,r2: TRect;
  s1,s2: string;
begin
  if (edtSearch.Text = '') or
   (Pos(UpperCase(edtSearch.Text),
    UpperCase(Column.Field.AsString)) <> 1) then exit;
  r1 := Rect; r2 := Rect;
  s1 := copy(Column.Field.AsString,1,length(edtSearch.Text));
  DBGrid1.Canvas.FillRect(Rect);
  DBGrid1.Canvas.Font.Color := clRed;
  DBGrid1.Canvas.Font.Style := [fsbold];
  DrawText(DBGrid1.Canvas.Handle,PChar(s1),Length(s1), r1,DT_CALCRECT);
  DBGrid1.Canvas.TextOut(r1.Left,r2.Top,s1);
  DBGrid1.Canvas.Font.Assign(DBGrid1.Font);
  s2 := StringReplace(Column.Field.AsString,s1,'',[]);
  r2.Left := r1.Right;
  DrawText(DBGrid1.Canvas.Handle,pchar(s2),length(s2), r2,0);
end;

procedure TfrmPlatsParser.edtSearchChange(Sender: TObject);
begin
  qryTiffsParser.Filtered := false;
  qryTiffsParser.Filtered := edtSearch.Text <> '';
end;

Procedure TfrmPlatsParser.AutoSizeColDBGrid(DBGrid:TDBGrid);
var
  I, ColWidth, ColTextWidth:integer;
begin
  if DBGrid.DataSource.DataSet.Active then
  begin
    DBGrid.DataSource.DataSet.DisableControls;
    for I:= 0 to DBGrid.Columns.Count-1 do
    begin
      ColWidth:=DBGrid.Canvas.TextWidth(DBGrid.Columns[I].Field.DisplayLabel);
      DBGrid.DataSource.DataSet.First;
      while not DBGrid.DataSource.DataSet.EOF do
      begin
        ColTextWidth:=DBGrid.Canvas.TextWidth(DBGrid.Columns[I].Field.DisplayText);
        if (ColTextWidth > ColWidth) then
        begin
          ColWidth := ColTextWidth;
        end;
        DBGrid.DataSource.DataSet.Next;
      end;{while}
      DBGrid.Columns[I].Width:=ColWidth+10;
    end;{for}
   DBGrid.DataSource.DataSet.EnableControls;
   DBGrid.DataSource.DataSet.First;
  end;
end;

procedure TfrmPlatsParser.FindAllDirectories;
var
  searchResult : TSearchRec;
  Item: Tlistitem;
  ClientPath: String;
begin
  try
    if GetFolderDialog(Application.Handle, 'Select a client folder', TiffsPath) then
    begin
      if (Findfirst(TiffsPath + '\*', faDirectory, searchResult) = 0) then
      begin
        while FindNext(searchResult) = 0 do
        begin
          if (searchResult.Attr and faDirectory > 0) and
           (searchResult.Name<>'.') and (searchResult.Name<>'..') then
          begin
            Item := ListView1.Items.Add;
            Item.Caption := searchResult.Name;
          end;
        end;
      end;
      FindClose(SearchResult);
    end;
  except
    raise;
  end;
end;

procedure TfrmPlatsParser.FormClose(Sender: TObject; var Action: TCloseAction);
var
   Cursor: IInterface;
begin
  ShowHourglass;
  qryTiffsParser.Close;
  FileADOConnection.Close;
  ClientPaths.Free;
end;

procedure TFrmPlatsParser.ReadIni;
var
  IniFile: TIniFile;
begin
  try
    ClientPaths :=  TStringList.Create;
    iniFile := TIniFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
    GetClients(Inifile, ClientPaths);
    flogDir := IniFile.ReadString('LogPath', 'LogPath', '');
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);
  finally
    IniFile.Free;
  end;
end;

procedure TfrmPlatsParser.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  ReadIni;
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  ListView_SetColumnWidth(Listview1.Handle, 0,400);
end;

procedure TfrmPlatsParser.BrowseFiles;
var
  ConnStr: String;
  Sheetlist: TStringlist;
  OpenDialog: TOpenDialog;
  CBOSheetName: String;
  ASource: String;
  SheetName: String;
  I: Integer;
  Cursor: IInterface;
begin
try
  try
    SheetList := TStringlist.Create;
    edtSearch.Clear;
    OpenDialog := TOpenDialog.Create(self);
    OpenDialog.Filter := 'Excel Files|*.xls*';
    OpenDialog.InitialDir :=  TiffsPath;

    if OpenDialog.Execute then
    begin
      ASource := OpenDialog.FileName;
      OpenDialog.Free;
    end else
    begin
      OpenDialog.Free;
      Exit;
    end;

    qryTiffsParser.Close;
    FileADOConnection.Close;

    if not FileADOConnection.Connected then
    begin
      Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0; Extended Properties= Excel 12.0 XML; Data Source=';
      FileADOConnection.ConnectionString := Connstr + ASource;
      FileADOConnection.Open;
    end;
     Cursor := ShowHourGlass;
    FileADOConnection.GetTableNames(SheetList);
    Self.Repaint;
    cboSheets.Items.Clear;
    for I := 0 to SheetList.Count -1 do
    begin
      SheetName := Copy(SheetList[I],1,Pos('$',SheetList[I])-1);
      if Copy(SheetName,1,1) = '''' then
        Delete(SheetName,1,1);

      cboSheets.Items.Add(SheetName);
      if I = 0 then
       cboSheetName := SheetName;
    end;

    cboSheets.ItemIndex := cboSheets.Items.IndexOf(cboSheetName);
    qryTiffsParser.SQL.Text := 'Select * from [' + cboSheetName + '$]';
    Self.Repaint;

    qryTiffsParser.Open;
    AutoSizeColDBGrid(DBGrid1);
    lblfetched.Caption := 'records: ' +  inttostr(qryTiffsParser.RecordCount);
  except
     On E:Exception do
     begin
      qryTiffsParser.Close;
      cboSheets.Items.Clear;
      cboSheets.Text := '';
      ShowMessage(E.Message + ': Failed to extract data. Excel file may be corrupted. Call your Supervisor');
      LogResult.LogType := ltError;
      LogResult.MethodName := 'BrowseFiles';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
     end;
  end
 finally
    SheetList.Free;
 end;
end;

procedure TfrmPlatsParser.ExtractFileData;
var
  ConnStr: String;
  ASource: String;
  Sheetlist: TStringlist;
  I: Integer;
  TabIndex: Integer;
  searchResult : TSearchRec;
  SheetName: String;
  CBOSheetName: String;
  OpenDialog: TOpenDialog;
  Indexpath: string;
begin
  try
   try
     SheetList := TStringlist.Create;
     Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0; Extended Properties=Excel 12.0 XML; Data Source=';
     ASource :=  IncludeTrailingBackslash(TiffsPath) + ListView1.Selected.Caption + '\';
     SetCurrentDir(ASource);
     edtSearch.Clear;

       if findfirst('Index', faDirectory, searchResult) = 0 then
       begin
         ASource := ASource + '\Index';
         setCurrentDir(ASource);
         FindClose(searchResult);
       end else
       begin
         if findfirst('Indexes', faDirectory, searchResult) = 0 then
         begin
           ASource := ASource + '\Indexes';
           setCurrentDir(ASource);
           FindClose(searchResult);
         end;
       end;

       if findfirst('*INDEX*.xls', faAnyFile, searchResult) = 0 then
         ASource := ASource + '\' + searchResult.Name
       else
         raise Exception.Create('Index File in ' + ASource + ' not found');

     qryTiffsParser.Close;
     FileADOConnection.Close;

     FileADOConnection.ConnectionString := Connstr + ASource;

     FileADOConnection.Open;
     FileADOConnection.GetTableNames(SheetList);

     cboSheets.Items.Clear;

     for I := 0 to SheetList.Count -1 do
     begin
       SheetName := Copy(SheetList[I],1,Pos('$',SheetList[I])-1);
       if Copy(SheetName,1,1) = '''' then
         Delete(SheetName,1,1);

       cboSheets.Items.Add(SheetName);
       if I = 0 then
        cboSheetName := SheetName;
       if UpperCase(SheetList[I]).Contains('STREET') then
         cboSheetName := SheetName;
     end;

     cboSheets.ItemIndex := cboSheets.Items.IndexOf(cboSheetName);
     qryTiffsParser.SQL.Text := 'Select * from [' + cboSheetName + '$]';

     qryTiffsParser.Open;
     AutoSizeColDBGrid(DBGrid1);
    lblfetched.Caption := 'records: ' +  inttostr(qryTiffsParser.RecordCount);
   except
     On E:Exception do
     begin
      qryTiffsParser.Close;
      cboSheets.Items.Clear;
      cboSheets.Text := '';
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ExtactFileData';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      ShowMessage(E.Message + ': Failed to extract data. Index file may be corrupted. Call your Supervisor');
     end;
   end;
  finally
    SheetList.Free;
  end;
end;

procedure TfrmPlatsParser.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected <> nil then
   ExtractFileData;
end;

function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData: LPARAM): Integer; stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
  BrowseCallbackProc := 0;
end;

function TfrmPlatsParser.GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;
const
  BIF_STATUSTEXT           = $0004;
  BIF_NEWDIALOGSTYLE       = $0040;
  BIF_RETURNONLYFSDIRS     = $0080;
  BIF_SHAREABLE            = $0100;
  BIF_USENEWUI             = BIF_EDITBOX or BIF_NEWDIALOGSTYLE;
var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  JtemIDList: PItemIDList;
  Path: PWideChar;
begin
  Result := False;
  Path := StrAlloc(MAX_PATH);
  SHGetSpecialFolderLocation(Handle, CSIDL_DRIVES, JtemIDList);
  with BrowseInfo do
  begin
    ulFlags := ulFlags and BIF_NONEWFOLDERBUTTON;
    hwndOwner := GetActiveWindow;
    pidlRoot := JtemIDList;
    SHGetSpecialFolderLocation(hwndOwner, CSIDL_DRIVES, JtemIDList);

    { return display name of item selected }
    pszDisplayName := StrAlloc(MAX_PATH);

    { set the title of dialog }
    lpszTitle := PChar(Caption);//'Select the folder';
    { flags that control the return stuff }
    lpfn := @BrowseCallbackProc;
    { extra info that's passed back in callbacks }
    lParam := LongInt(PChar(strFolder));
  end;

  ItemIDList := SHBrowseForFolder(BrowseInfo);

  if (ItemIDList <> nil) then
    if SHGetPathFromIDList(ItemIDList, Path) then
    begin
      strFolder := Path;
      Result := True
    end;
end;

function TfrmPlatsParser.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure TfrmPlatsParser.GetClients(IniFile: TInifile; ClientPaths: TStrings);
var
  ClientList: TStringList;
  I: Integer;
  N: String;
  V: String;
begin
  try
    ClientList := TStringList.Create;
    cboClients.Items.Clear;
    IniFile.ReadSection('DataPath', ClientList);
    for I := 0 to ClientList.Count-1 do
    begin
      N:= ClientList[I]; //Name
      V := IniFile.ReadString('DataPath', N, ''); //Value
      ClientPaths.Add(V);
      cboClients.Items.Add(N);
    end;
  finally
    ClientList.Free;
  end;
end;

function TfrmPlatsParser.ShowHourGlass: IInterface;
begin
  Result := TShowHourGlass.Create;
end;

procedure TfrmPlatsParser.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + 'PlatsParser_' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmPlatsParser.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

end.
