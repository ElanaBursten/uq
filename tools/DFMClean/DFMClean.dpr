program DFMClean;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  Windows,
  JclFileUtils,
  OdMiscUtils in '..\..\common\OdMiscUtils.pas';

procedure Error(const Msg: string);
begin
  MessageBeep(0);
  WriteLn('ERROR: ' + Msg);
  Sleep(5000);
  Halt;
end;

procedure CleanDFM(const FileName: string);
const
  RemovePrefixStrings: array[0..3] of string =
    ('ExplicitTop = ',
     'ExplicitLeft = ',
     'ExplicitWidth = ',
     'ExplicitHeight = ');
  RemoveStrings: array[0..0] of string =
    ('PDFSettings.EmbedFontOptions = []');
  ReplacePaperName: array[0..3] of string =
    ('''Carta - 21,59 x 27,94 cm (8,5 x 11 pol.) ''',
     '''Carta - 21,59 x 27,94 cm (8,5 x 11 pol.)''',
     '''Letter 8.5x11in.''',
     '''Letter''');
  NotAllowedIdentifiers: array[0..0] of string =
    ('TraCodeModule');
  IgnorePathNames: array[0..0] of string =
    ('\thirdparty\');
var
  Contents: TStringList;
  OrigContents: TStringList;
  i: Integer;
begin
  for i := Low(IgnorePathNames) to High(IgnorePathNames) do
    if StrContainsText(IgnorePathNames[i], FileName) then
      Exit;

  OrigContents := nil;
  Contents := TStringList.Create;
  try
    OrigContents := TStringList.Create;
    Contents.LoadFromFile(FileName);
    OrigContents.Assign(Contents);
    for i := Low(RemoveStrings) to High(RemoveStrings) do
      RemoveStringFromList(RemoveStrings[i], Contents, True);

    for i := Low(RemovePrefixStrings) to High(RemovePrefixStrings) do
      RemoveStringWithPrefixFromList(RemovePrefixStrings[i], Contents, True);

    for i := Low(ReplacePaperName) to High(ReplacePaperName) do
      Contents.Text := StringReplace(Contents.Text, ReplacePaperName[i],
        QuotedStr('Letter (8 1/2 x 11 in)'), [rfIgnoreCase, rfReplaceAll]);
    //ReplaceSubStringToEndOfLineInList('Version = ', 'Version = ''10.07''', Contents, True);
    for i := Low(NotAllowedIdentifiers) to High(NotAllowedIdentifiers) do
      if StrContains(NotAllowedIdentifiers[i], Contents.Text) then
        Error('Identifier ' + NotAllowedIdentifiers[i] + ' not allowed in ' + FileName);

    if Trim(Contents.Text) <> Trim(OrigContents.Text) then begin
      Writeln('Modifying: ' + FileName);
      Contents.SaveToFile(FileName);
    end;
  finally
    FreeAndNil(Contents);
    FreeAndNil(OrigContents);
  end;
end;

procedure CleanDir(const DirName: string);
var
  Files: TStringList;
  FileName: string;
  i: Integer;
begin
  Files := TStringList.Create;
  try
    JclFileUtils.AdvBuildFileList(AddSlash(DirName) + '*.dfm', faAnyFile, Files, amAny, [flFullNames, flRecursive]);
    for i := 0 to Files.Count - 1 do begin
      FileName := ExpandFileName(Files[i]);
      CleanDFM(FileName);
    end;
  finally
    FreeAndNil(Files);
  end;
end;
var
  DirName: string;
  SilentMode: Boolean;
  i: Integer;
begin
  SilentMode := CheckCmdLineParameter('silent');
  try
    if (ParamCount = 0) or ((ParamCount = 1) and (ParamStr(1) = 'silent')) then DirName := '..';
    if ParamCount > 0 then begin
      for i := 1 to ParamCount do begin
        if not SameText(ParamStr(i), 'silent') then
          DirName := DirName + ' ' +ParamStr(i);
      end;
      DirName := Trim(DirName);
    end;

    DirName := ExpandFileName(DirName);
    if DirectoryExists(DirName) then
      CleanDir(DirName)
    else
      Error('Directory does not exist: ' + DirName);

    WriteLn('Done.');
  finally
    if not SilentMode then begin
      WriteLn('Press enter to continue');
      ReadLn;
    end;
  end;
end.
