object frmWhatOnIrth: TfrmWhatOnIrth
  Left = 0
  Top = 0
  Caption = 'What on Irth'
  ClientHeight = 443
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  TextHeight = 15
  object Splitter1: TSplitter
    Left = 0
    Top = 353
    Width = 632
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object PnlTop: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 353
    Align = alTop
    TabOrder = 0
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 630
      Height = 249
      Align = alTop
      TabOrder = 0
    end
  end
  object PnlBot: TPanel
    Left = 0
    Top = 356
    Width = 632
    Height = 87
    Align = alClient
    TabOrder = 1
    object btnPost: TButton
      Left = 280
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Post'
      TabOrder = 0
      OnClick = btnPostClick
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 67
      Width = 630
      Height = 19
      Panels = <
        item
          Text = 'Connected to'
          Width = 80
        end
        item
          Text = 'SSDS-UTQ-QM-02-DV'
          Width = 130
        end
        item
          Text = 'Success'
          Width = 50
        end
        item
          Width = 30
        end
        item
          Text = 'Failed'
          Width = 35
        end
        item
          Width = 30
        end
        item
          Text = 'Total'
          Width = 38
        end
        item
          Width = 30
        end
        item
          Text = 'ver'
          Width = 23
        end
        item
          Width = 100
        end
        item
          Width = 50
        end>
    end
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=SA;Init' +
      'ial Catalog=QM;Data Source=GreatWhite;Use Procedure for Prepare=' +
      '1;Auto Translate=True;Packet Size=4096;Workstation ID=GREATWHITE' +
      ';Initial File Name="";Use Encryption for Data=False;Tag with col' +
      'umn collation when possible=False;MARS Connection=False;DataType' +
      'Compatibility=0;Trust Server Certificate=False;Server SPN="";App' +
      'lication Intent=READWRITE;'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 32
    Top = 104
  end
  object getPendingResponses: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'get_pending_multi_responses_10;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RespondTo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ParsedLocatesOnly'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Prepared = True
    Left = 64
    Top = 184
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 400
    Top = 152
  end
  object IdLogEvent1: TIdLogEvent
    Left = 400
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvTLSv1, sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 271
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 160
    Top = 3
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 312
    Top = 64
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'RespondTo'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to= :RespondTo'
      '')
    Left = 512
    Top = 144
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 504
  end
  object qryLocate: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 400
    Top = 208
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://www.irth.com/IRTHNet/WebServices/IrthNetService.asmx'
    Params = <>
    RaiseExceptionOn500 = False
    SynchronizedEvents = False
    Left = 168
    Top = 176
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        Name = 'IrthRequestBody'
        Options = [poDoNotEncode]
        ContentTypeStr = 'text/xml'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 176
    Top = 56
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'text/xml'
    Left = 168
    Top = 112
  end
end
