unit uWOEmain;     //qm-726  qm-882

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  System.RegularExpressions,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.StdCtrls,
  Data.DB, Data.Win.ADODB, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL, IdBaseComponent, IdIntercept, IdLogBase, IdLogEvent, IdMessage,
  REST.Types, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;
type
  TSplitStatus = record
    NumType: String;
    AlphaType: String;
  end;  //TLogResults
type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: boolean;
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TReturnedResponse = record
    Result:string[14];
    ResultCode:string[4];
    ResultMessage:string[200];
  end;

type
  TfrmWhatOnIrth = class(TForm)
    PnlTop: TPanel;
    PnlBot: TPanel;
    Splitter1: TSplitter;
    btnPost: TButton;
    Memo1: TMemo;
    ADOConn: TADOConnection;
    getPendingResponses: TADOStoredProc;
    insResponseLog: TADOQuery;
    IdLogEvent1: TIdLogEvent;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdSMTP1: TIdSMTP;
    qryEMailRecipients: TADOQuery;
    delResponse: TADOQuery;
    IdMessage1: TIdMessage;
    qryLocate: TADOQuery;
    StatusBar1: TStatusBar;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    procedure btnPostClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    RespondTo:string;
    CallCenter:string;
    LogPath: string;
    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    ReturnedResponse:TReturnedResponse;
    function ProcessINI:boolean;
    procedure clearLogRecord;
    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    procedure WriteLog(LogResult: TLogResults);
    function connectDB: boolean;
    function ProcessParams: boolean;

    procedure InsertResponse(LocateId: integer; ResponseDate: TDateTime;
      CallCenter, Status, ResponseSent: string; Success: Boolean;
      Reply: string);
    function deleteMultiQueueRecord(RespondTo: string; LocateID: integer): boolean;

    { Private declarations }
  public
    { Public declarations }
        APP_VER: string;
  end;

var
  frmWhatOnIrth: TfrmWhatOnIrth;


const
xmlContractLocatorResponse=
  '<?xml version="1.0" encoding="utf-8"?> '+
  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" '+
  'xmlns:tem="http://tempuri.org/"> '+
  '  <soapenv:Header/>'+
  '  <soapenv:Body> '+
  '   <tem:AddContractLocatorResponses> '+
  '     <tem:xmlContractLocatorResponses> '+
  '       <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd"> '+
  '         <RESPONSE_INFO> '+
  '           <One_Call_Center>%s</One_Call_Center> '+    //One call center
  '           <CDC>%s</CDC> '+                            //client code
  '           <Authentication_Code>%s</Authentication_Code> '+
  '           <Ticket_Number>%s</Ticket_Number> '+
  '           <Version_Number>%s</Version_Number> '+
  '           <Response_Code>%s</Response_Code> '+
  '           <Locate_DateTime>%s</Locate_DateTime> '+    //03/15/2023 12:35:21 PM
  '           <Locator_Name>%s</Locator_Name> '+  //Utiliquest Rep
  '           %s '+ //Ongoing
  '         </RESPONSE_INFO> '+
  '       </ContractLocatorResponseDataSet> '+
  '     </tem:xmlContractLocatorResponses> '+
  '   </tem:AddContractLocatorResponses> '+
  '  </soapenv:Body> '+
  '</soapenv:Envelope>';

ADDITIONAL_RESP =
  '<Addl_PosResp_Center_Info>['+
  '{"DisplayName": "Name of Person Attempted to Contact", '+
  '"OccFieldName": "NameOfPersonAttemptedToContact", '+
  '"Value": "%s"}, '+                                      //PersonToContact      ONGOCONT
  '{"DisplayName": "Required Comment", '+
  '"OccFieldName": "RequiredComment", '+
  '"Value": "%s"}, '+                                      //NOTE
  '{"DisplayName": "Attempted Contact Date Time", '+
  '"OccFieldName": "AttemptedContactDateTime", '+
  '"Value": "%s"}, '+                                      //ContactDT
  '{"DisplayName": "Attempted Contact Method", '+
  '"OccFieldName": "AttemptedContactMethod", '+
  '"Value": "%s"}, '+                                      //ContactMethod
  '{"DisplayName": "Phone Or Email Of Person", '+
  '"OccFieldName": "PhoneOrEmailOfPerson", '+
  '"Value": "%s"}, '+                                      //contactPhone
  '{"DisplayName": "Locator Name", '+
  '"OccFieldName": "LocatorName", '+
  '"Value": "%s"}, '+                                      //LocatorName
  '{"DisplayName": "Phone Or Email Of Locator", '+
  '"OccFieldName": "PhoneOrEmailOfLocator", '+
  '"Value": "%s" }, '+                                       //PhoneOrEmail
  '{"DisplayName": "Proposed Date Time Of Completion", '+
  '"OccFieldName": "ProposedDateTimeOfCompletion", '+
  '"Value": "%s"} '+                                       //DateTimeOfCompletion
  ']</Addl_PosResp_Center_Info>';

implementation

{$R *.dfm}

uses
  Inifiles, System.StrUtils, System.DateUtils;

function TfrmWhatOnIrth.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;

procedure TfrmWhatOnIrth.btnPostClick(Sender: TObject);
CONST
  LOCATOR_NAME='Utiliquest Rep';
  NOTE='Response by Utiliquest';
//  ONGOING_Array: TArray<String> = ['3C','3F','3G','6'];  //Ongoing statuses
  ONGOING ='ONGOING';
  PARSER = 'PARSER'; //qm-657
  SKIP_STATUS = 'SKIP';
  DBL_QUOTES = '"';
var
  CDC, OneCallCenter, AuthCode, TicketNumber, TktVersion, ResponseCode, LocateDateTime, LocateName, OngoingData, AddedBy :string;
//ongoing vars
  ContactDT,ContactMethod,PersonToContact,contactPhone, LocatorName, PhoneOrEmail,DateTimeOfCompletion,OutStatus :string;
  AdditionalResponse:string;
  StatusExplanation:string;
  URL: string;
  locate_status:string;
  LocateID:integer;
  Response: string;
  ResponseBody: String;
  cntGood, cntBad, ii,iii :integer;

  function StringInArray(const S: string; const SArray: array of string): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to Length(SArray) - 1 do
      if SameText(S, SArray[i]) then begin
        Result := True;
        Break;
      end;
  end;

  function ExtractNumberInString ( sChaine: String ): String ;
  var
      i: Integer ;
  begin
      Result := '' ;
      for i := 1 to length( sChaine ) do
      begin
          if sChaine[ i ] in ['0'..'9'] then
          Result := Result + sChaine[ i ] ;
      end ;
  end;

begin
  cntGood:=0;
  with getPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value := RespondTo;
    Parameters.ParamByName('@CallCenter').Value := CallCenter;
    Parameters.ParamByName('@ParsedLocatesOnly').Value := 1;
    Open;
    First;
    OneCallCenter:=''; CDC:=''; AuthCode:=''; TicketNumber:=''; TktVersion:=''; ResponseCode:=''; LocateDateTime:='';  locate_status:='';
    ContactDT:=''; ContactMethod:=''; PersonToContact:=''; contactPhone:=''; LocatorName:=''; PhoneOrEmail:=''; DateTimeOfCompletion:='';
    StatusExplanation:='';
    AdditionalResponse:='';
    ResponseBody:='';
    LocateID:=0;
    ii:=0;
    iii:=0;
    while not EOF do
    begin
      AddedBy       := Trim(FieldByName('added_by').AsString);
      CDC := trim(FieldByName('client_code').AsString);    //'AEPIN';
      AuthCode := trim(FieldByName('api_key').AsString);//'833-zLz2D9cKco';     //sAuthorization;
      URL:= trim(FieldByName('url').AsString);  //https://www.irth.com/IRTHNet/WebServices/IrthNetService.asmx
      TicketNumber := trim(FieldByName('ticket_number').AsString);//'2401070045';
      OneCallCenter :=trim(FieldByName('one_call_center').AsString);  //'IUPPS'
      TktVersion:= '0';//ExtractNumberInString(trim(FieldByName('revision').AsString));   //'00A'
      OutStatus := trim(AnsiUpperCase(Trim(FieldByName('outgoing_status').AsString)));
      ResponseCode  := OutStatus; //getStatus(OutStatus).NumType;
      StatusExplanation:= trim(AnsiUpperCase(Trim(FieldByName('status_explanation').AsString)));
      LocateDateTime:= trim(FieldByName('work_date').AsString); //  2024-01-22 07:00:00.000
      LocateID      := FieldByName('locate_id').AsInteger;
      locate_status := trim(AnsiUpperCase(Trim(FieldByName('status').AsString)));
      if StatusExplanation=ONGOING then
      begin
        ContactDT               := trim(FieldByName('work_date').AsString);
        ContactMethod           := trim(FieldByName('contact_method_irth').AsString);
        PersonToContact         := trim(FieldByName('contact').AsString);  //used to be talk_to_person
        contactPhone            := trim(FieldByName('contact_phone').AsString);
        LocatorName             := LOCATOR_NAME;
        PhoneOrEmail            := '(225)268-2016';
        DateTimeOfCompletion    := trim(FieldByName('scheduled_meet_datetime').AsString);
        AdditionalResponse:= (Format(ADDITIONAL_RESP,[PersonToContact, NOTE, ContactDT, ContactMethod, contactPhone,
          LocatorName, PhoneOrEmail, DateTimeOfCompletion]));
      end;

      if AddedBy <> PARSER then  //qm-544 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-544 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed NON-Parser_Added Response';
        WriteLog(LogResult);

        next;
        continue;
      end;

      if OutStatus = SKIP_STATUS then  //qm-543 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-543 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed Skipped Outstatus Response '+OutStatus;
        LogResult.DataStream := 'Ticket no: '+ TicketNumber+ ' LocateID: '+IntToStr(LocateID);
        WriteLog(LogResult);

        next;
        continue;
      end;

       //AdditionalResponse,
      ResponseBody:=(Format(xmlContractLocatorResponse,[OneCallCenter, CDC, AuthCode,
                  TicketNumber, TktVersion, ResponseCode, LocateDateTime, LOCATOR_NAME, AdditionalResponse]));


      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequest1.Params.ParameterByName('IrthRequestBody').Value:= ResponseBody;
      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseBody;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      if pos( 'successful', RESTResponse1.Content)>0 then
      begin
        ReturnedResponse.ResultMessage:=  'successful';
        ReturnedResponse.ResultCode:= '1';
      end
      else if pos( 'Failed', RESTResponse1.Content)>0 then
      begin
        ii:=pos( 'Failed', RESTResponse1.Content);
        iii:=pos( '</AddContractLocatorResponsesResult>', RESTResponse1.Content);
        ReturnedResponse.ResultMessage:=copy(RESTResponse1.Content,ii,(iii-ii));
        ReturnedResponse.ResultCode:= '-1';
      end
      else if pos('Registration', RESTResponse1.Content)>0 then
      begin
        ReturnedResponse.ResultMessage:=  'successful';
        ReturnedResponse.ResultCode:= '1';
        Memo1.Lines.Add('Validation_Code;113/Validation_Code');
			  Memo1.Lines.Add('Validation_Message; Registration Code Not Found&lt;/Validation_Message');
        LogResult.LogType := ltWarning;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
        LogResult.MethodName := 'Responder Response';
        LogResult.DataStream := 'Validation_Code;113/Validation_Code___alidation_Message; Registration Code Not Found&lt;/Validation_Message';
        WriteLog(LogResult);
      end;


      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('RESTResponse1.Content: '+RESTResponse1.Content);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response sent '+ResponseBody;
        WriteLog(LogResult);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response returned '+RESTResponse1.Content;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
        WriteLog(LogResult);
        inc(cntGood);
        StatusBar1.Panels[3].text := IntToStr(cntGood);
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        if RESTResponse1.StatusCode>0 then
        insertResponseLog.Sucess              := True
        else
        insertResponseLog.Sucess              := False;
        insertResponseLog.Reply               := ReturnedResponse.ResultCode+' '+ReturnedResponse.ResultMessage;
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);
        {
1  Successful    delete
0  Canceled: The response was recorded but the Ticket has been canceled.  delete
-1 Failed: The Ticket Number, Service Area Code, Response, or Utility Type is invalid or not found. The reason is included in the ErrorMessage field of the response (if supported by the message format being used).
-2 UserPassInvalid: The UserID or Password is invalid.
-3 ServerError: A Server error occurred. The details of the error have been loggedfor technical support and may also be included in the response. The response message can be re-submitted but should wait several minutes before re-attempting.
}
        if (ReturnedResponse.ResultCode='1') or (ReturnedResponse.ResultCode='0') then
        deleteMultiQueueRecord(RespondTo,LocateID);
      end  //StatusCode > 199 <300   good
      else
      begin
        inc(cntBad);
        StatusBar1.Panels[5].text := IntToStr(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        WriteLog(LogResult);

        //QM-552 Add EMail notices to all Delphi Responders
        IdMessage1.Body.Add('Failed request no '+IntToStr(cntBad)+' at '+formatdatetime('yyyy-mm-dd hh:mm:ss', Now));
        IdMessage1.Body.Add(IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber);
        IdMessage1.Body.Add('Sent '+ResponseBody);
        IdMessage1.Body.Add('Returned ' + RESTResponse1.Content);
        IdMessage1.Body.Add('-----------------------------------------------------');

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        if (ReturnedResponse.ResultCode='1') or (ReturnedResponse.ResultCode='0') then
        insertResponseLog.Sucess              := True
        else
        insertResponseLog.Sucess              := False;
        insertResponseLog.Reply               := ReturnedResponse.ResultMessage;
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF

  end; //with
  Statusbar1.Panels[7].Text:=InttoStr(cntGood+cntBad);

end;

procedure TfrmWhatOnIrth.InsertResponse(LocateId: integer;
  ResponseDate: TDateTime; CallCenter, Status, ResponseSent: string;
  Success: boolean; Reply: string);
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateId').Value := LocateId;
    ParamByName('ResponseDate').Value := ResponseDate;
    ParamByName('CallCenter').Value := CallCenter;
    ParamByName('Status').Value := Status;
    ParamByName('ResponseSent').Value := ResponseSent;
    ParamByName('Success').Value := Success;
    ParamByName('Reply').Value := Reply;
  end;
  insResponseLog.ExecSQL;
end;


function TfrmWhatOnIrth.deleteMultiQueueRecord(RespondTo:string;LocateID:integer):boolean;
begin
  result := true;
  delResponse.Parameters.ParamByName('RespondTo').Value :=  Respondto;
  delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

  memo1.Lines.Add(delResponse.SQL.Text);

  try
   delResponse.ExecSQL;   //commented out for testing
  except on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'delResponse';
      LogResult.DataStream := delResponse.SQL.Text;
      LogResult.ExcepMsg:=e.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmWhatOnIrth.FormCreate(Sender: TObject);
begin
  APP_VER:=GetAppVersionStr;
  Statusbar1.Panels[9].Text:= APP_VER;
  if ProcessParams then
  ProcessINI;

  ConnectDB;
  If ParamStr(3) <> 'GUI' then
  begin
    btnPostClick(Sender);
    application.Terminate;
  end;
end;

function TfrmWhatOnIrth.ProcessParams: boolean;
begin
  result:=false;
  RespondTo:= '';
  CallCenter:='';


  RespondTo:= ParamStr(1);
  CallCenter:=ParamStr(2);

  if ((RespondTo<>'') and (CallCenter<>'')) then result:=true;

end;

function TfrmWhatOnIrth.ProcessINI: boolean;
var
  WhatOnIrth: TIniFile;
  myPath: string;
begin
  result := true;
  try
    myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
    try
      WhatOnIrth := TIniFile.Create(myPath + 'WhatOnIrth.ini');
      LogPath := WhatOnIrth.ReadString('LogPaths', 'LogPath', '');
      ForceDirectories(LogPath);

      aServer   := WhatOnIrth.ReadString('Database', 'Server', '');
      aDatabase := WhatOnIrth.ReadString('Database', 'DB', 'QM');
      ausername := WhatOnIrth.ReadString('Database', 'UserName', '');
      apassword := WhatOnIrth.ReadString('Database', 'Password', '');

      assert((aServer<>'') and (aDatabase<>'') and (ausername<>'') and (apassword<>''));


      LogResult.LogType := ltInfo;
      LogResult.Status := 'Ini processed';
      LogResult.MethodName := 'ProcessINI';
      WriteLog(LogResult);
    finally
      FreeAndNil(WhatOnIrth);
    end;
  except
    on E: Exception do
    begin
      result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ProcessINI';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      Memo1.Lines.Add('Could not process INI');
    end;
  end;
end;


function TfrmWhatOnIrth.connectDB: boolean;
var
  connStr: String;
begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ADOConn.Connected) then
      begin
        Memo1.Lines.Add('Connected to database');
        StatusBar1.Panels[1].Text :=aServer;
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStr;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

procedure TfrmWhatOnIrth.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackSlash(LogPath)+'WhatOnIrth '+RespondTo +' '+CallCenter +'_'+ FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
    WriteLn(myFile, 'WhatOnIrth Version: ' + APP_VER);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmWhatOnIrth.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmWhatOnIrth.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmWhatOnIrth.getData(JsonString, Field: String): String;
begin

end;

end.
