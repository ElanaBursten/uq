object frmMainATTCal: TfrmMainATTCal
Cal  Left = 0
  Top = 0
  Caption = 'UATT '
  ClientHeight = 449
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 280
    Top = 355
    Width = 53
    Height = 13
    Caption = 'Call Center'
  end
  object btnParseXML: TButton
    Left = 56
    Top = 350
    Width = 105
    Height = 25
    Caption = 'Parse XML'
    TabOrder = 0
    OnClick = btnParseXMLClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 635
    Height = 289
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object edtCallCenterName: TEdit
    Left = 352
    Top = 352
    Width = 121
    Height = 21
    TabOrder = 2
    OnChange = edtCallCenterNameChange
  end
  object btnSendResponse: TButton
    Left = 56
    Top = 381
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 3
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 430
    Width = 635
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'SSDS-UTQ-QM-02-DV'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object RESTResponse1: TRESTResponse
    Left = 344
    Top = 304
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        name = 'body'
        Options = [poDoNotEncode]
        ContentType = ctAPPLICATION_JSON
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 416
    Top = 304
  end
  object RESTClient1: TRESTClient
    Authenticator = HTTPBasicAuthenticator1
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://test-alcs.att.com/closeouts/se'
    ContentType = 'application/json'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 288
    Top = 304
  end
  object HTTPBasicAuthenticator1: THTTPBasicAuthenticator
    Username = 'utiuat'
    Password = 'Welcome@123'
    Left = 504
    Top = 304
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to= :ResponseTo'
      '')
    Left = 480
    Top = 192
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'get_pending_multi_responses_10'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 120
    Top = 96
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 56
    Top = 176
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
end
