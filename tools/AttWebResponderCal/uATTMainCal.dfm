object frmMainATTCal: TfrmMainATTCal
  Left = 0
  Top = 0
  Caption = 'California AT&T Responder'
  ClientHeight = 360
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 416
    Top = 298
    Width = 53
    Height = 13
    Caption = 'Call Center'
  end
  object btnParseXML: TButton
    Left = 8
    Top = 295
    Width = 105
    Height = 25
    Caption = 'Parse XML'
    TabOrder = 0
    OnClick = btnParseXMLClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 635
    Height = 289
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object edtCallCenterName: TEdit
    Left = 488
    Top = 295
    Width = 121
    Height = 21
    TabOrder = 2
    OnChange = edtCallCenterNameChange
  end
  object btnSendResponse: TButton
    Left = 136
    Top = 295
    Width = 105
    Height = 25
    Caption = 'Send Response'
    TabOrder = 3
    OnClick = btnSendResponseClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 341
    Width = 635
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Text = 'SSDS-UTQ-QM-02-DV'
        Width = 130
      end
      item
        Text = 'Success'
        Width = 50
      end
      item
        Width = 30
      end
      item
        Text = 'Failed'
        Width = 35
      end
      item
        Width = 30
      end
      item
        Text = 'Total'
        Width = 38
      end
      item
        Width = 30
      end
      item
        Text = 'ver'
        Width = 23
      end
      item
        Width = 50
      end>
  end
  object RESTResponse1: TRESTResponse
    Left = 520
    Top = 88
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkREQUESTBODY
        name = 'body'
        Options = [poDoNotEncode]
        ContentType = ctAPPLICATION_JSON
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 312
    Top = 16
  end
  object RESTClient1: TRESTClient
    Authenticator = HTTPBasicAuthenticator1
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://test-alcs.att.com/closeouts/se'
    ContentType = 'application/json'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 408
    Top = 16
  end
  object HTTPBasicAuthenticator1: THTTPBasicAuthenticator
    Username = 'utiuat'
    Password = 'Welcome@123'
    Left = 504
    Top = 16
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[responder_multi_queue]'
      'where locate_id = :LocateID'
      'and respond_to= :ResponseTo'
      '')
    Left = 480
    Top = 192
  end
  object spGetPendingResponses: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'get_pending_multi_responses_9CA'
    Parameters = <
      item
        Name = '@RespondTo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 128
    Top = 96
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParse' +
      'rUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial Fi' +
      'le Name="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 56
    Top = 176
  end
  object insResponseLog: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ResponseDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ResponseSent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Reply'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[response_log]'
      '           ([locate_id]'
      '           ,[response_date]'
      '           ,[call_center]'
      '           ,[status]'
      '           ,[response_sent]'
      '           ,[success]'
      '           ,[reply])'
      '     VALUES'
      '           (:LocateId'
      '           ,:ResponseDate'
      '           ,:CallCenter'
      '           ,:Status'
      '           ,:ResponseSent'
      '           ,:Success'
      '           ,:Reply)'
      '')
    Left = 296
    Top = 152
  end
  object delMultiQDups: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ResponseTo'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'delete rmq'
      'from responder_multi_queue rmq'
      'join locate l on l.locate_id = rmq.locate_id'
      'where rmq.respond_to = '#39'att'#39
      'and l.added_by <> '#39'PARSER'#39)
    Left = 400
    Top = 88
  end
  object IdSMTP1: TIdSMTP
    Intercept = IdLogEvent1
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    UseEhlo = False
    Host = 'smtp.dynutil.com'
    SASLMechanisms = <>
    ValidateAuthLoginCapability = False
    Left = 8
    Top = 3
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.dynutil.com:25'
    Host = 'smtp.dynutil.com'
    Intercept = IdLogEvent1
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvTLSv1, sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 55
  end
  object IdLogEvent1: TIdLogEvent
    Left = 200
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      '')
    BccList = <>
    CharSet = 'us-ascii'
    CCList = <>
    ContentType = 'text/html'
    Encoding = meDefault
    FromList = <
      item
        Address = 'Larry.Killen@Utiliquest.com'
        Name = 'xqTrix Team'
        Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
        Domain = 'Utiliquest.com'
        User = 'Larry.Killen'
      end>
    From.Address = 'Larry.Killen@Utiliquest.com'
    From.Name = 'xqTrix Team'
    From.Text = 'xqTrix Team <Larry.Killen@Utiliquest.com>'
    From.Domain = 'Utiliquest.com'
    From.User = 'Larry.Killen'
    Organization = 'Utiliquest'
    Priority = mpHighest
    Recipients = <>
    ReplyTo = <
      item
      end>
    ConvertPreamble = True
    Left = 128
  end
  object qryEMailRecipients: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'OCcode'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [responder_email]'
      '  FROM [QM].[dbo].[call_center]'
      '  where cc_code =:OCcode')
    Left = 56
    Top = 48
  end
end
