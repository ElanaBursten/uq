unit uATTMainCal;
//qm-498
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, IdMessage, IdIntercept, IdLogBase, IdLogEvent, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: String[12];
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry


type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;  //TLogResults
 type
    TCallCenter=class
  private
    FRespondto: string;
    FInitials: string;
    FID: string;
    FPosturl: string;
    FInclude_epr_link: boolean;
    FInclude_explanation_notes: boolean;
    FInclude_public_locate_notes: boolean;
    FVendorname: String;
    FFacility: String;
    FAllversions: boolean;
    FDefaultnote: string;
    FClients: TStringList;
    FCodeMappings: TStringList;
    FStatusCodeSkip: TStringList;
    CallCenter: TCallCenter;
    fCallCenterName: string;
    fCenter: string;
    {Get/Set methods}
  published
    property CallCenterName: string read fCallCenterName write fCallCenterName;
    property Respondto: string read FRespondto write FRespondto;
    property Initials: string read FInitials write FInitials;
    property ID: string read FID write FID;
    property Posturl: string read FPosturl write FPosturl;
    property Include_epr_link: boolean read FInclude_epr_link write FInclude_epr_link;
    property Include_explanation_notes: boolean read FInclude_explanation_notes write FInclude_explanation_notes;
    property Include_public_locate_notes: boolean read FInclude_public_locate_notes write FInclude_public_locate_notes;
    property Vendorname: string read FVendorname write FVendorname;
    property Facility: String read FFacility write FFacility;
    property Allversions: boolean read FAllversions write FAllversions;
    property Defaultnote: string read FDefaultnote write FDefaultnote;
    property Center:string read fCenter write fCenter;
    property Clients: TStringList read FClients write FClients;
    property Codemappings: TStringList read FCodemappings write FCodeMappings;
    property StatusCodeSkip: TStringList read FStatusCodeSkip write FStatusCodeSkip;
  end;   //TCallCenter
type
  TfrmMainATTCal = class(TForm)
    btnParseXML: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    edtCallCenterName: TEdit;
    RESTResponse1: TRESTResponse;
    RESTRequest1: TRESTRequest;
    RESTClient1: TRESTClient;
    HTTPBasicAuthenticator1: THTTPBasicAuthenticator;
    delResponse: TADOQuery;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    btnSendResponse: TButton;
    insResponseLog: TADOQuery;
    StatusBar1: TStatusBar;
    delMultiQDups: TADOQuery;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    qryEMailRecipients: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btnParseXMLClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtCallCenterNameChange(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;
    fconfigXML: tFilename;
    CallCenter: TCallCenter;
    fOC_Code: string;
    function connectDB: boolean;
    function ProcessINI:boolean;
    function ProcessConfigXML(xmlFile: tFilename): boolean;
    procedure clearLogRecord;

    function FindCCnode(scallcenter: String; doc: IXMLDOMDocument; out Node: IXMLDOMNode; out nodemapattr: IXMLDOMNamedNodeMap): Boolean;
    function FillList(doc: IXMLDOMDocument; scallcenter: String; NodeValues: TStringList): Boolean;
    procedure InitializeRest;
    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    procedure SetUpRecipients;
//    function CleanHouse: integer;
  public
    { Public declarations }

    function processParams:boolean;
    procedure WriteLog(LogResult: TLogResults);
    property OC_Code:string read fOC_Code write fOC_Code;
    property logDir: wideString read flogDir;
    property configXML: tFilename read fconfigXML;
  end;

var
  frmMainATTCal: TfrmMainATTCal;

implementation
uses  dateutils, StrUtils, iniFiles;

{$R *.dfm}

procedure TfrmMainATTCal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmMainATTCal.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  AppVer:='';
  ProcessINI;
  CallCenter := TCallCenter.Create;

  CallCenter.Clients := TStringlist.Create;
  CallCenter.Codemappings := TStringlist.Create;
  CallCenter.StatusCodeSkip := TStringlist.Create;

  processParams;

  btnParseXMLClick(Sender);
  connectDB;
  InitializeRest;
  AppVer  := GetAppVersionStr;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  SetUpRecipients;

  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

procedure TfrmMainATTCal.InitializeRest;
begin
  HTTPBasicAuthenticator1.Password:=  CallCenter.ID;
  HTTPBasicAuthenticator1.Username:=  CallCenter.Initials;
  RESTClient1.BaseURL :=  CallCenter.Posturl;
  LogResult.MethodName := 'InitializeRest';
  LogResult.DataStream := RESTClient1.BaseURL;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
end;

procedure TfrmMainATTCal.FormDestroy(Sender: TObject);
begin
  CallCenter.Clients.Free;
  CallCenter.StatusCodeSkip.Free;
  CallCenter.Codemappings.Free;
  CallCenter.Free;
end;

function TfrmMainATTCal.FindCCnode(scallcenter: String; doc: IXMLDOMDocument; out node: IXMLDOMNode; out nodemapattr: IXMLDOMNamedNodeMap): Boolean;
var
  ccnodelist: IXMLDOMNodeList;
  nodelist: IXMLDOMNodeList;
  nodemap: IXMLDOMNamedNodeMap;
  ccnode: IXMLDOMNode;
  I: Integer;
begin
  Result := false;
   ccnodelist := doc.selectNodes('//responder/alcs/callcenter');
   for I := 0 to ccNodeList.length - 1 do
   begin
     nodemap := ccNodeList[I].attributes;
     ccnode := nodemap.getNamedItem('name');
     if ccnode <> nil then
     begin
       if ccnode.nodeValue = scallcenter then
       begin
         node := ccNodeList[I];
         nodemapattr := nodemap;
         Result := true;
         break;
       end;
     end;
   end;
end;

function TfrmMainATTCal.FillList(doc: IXMLDOMDocument; scallcenter: String; NodeValues: TStringList): Boolean;
var
 nodemap: IXMLDOMNamedNodeMap;
 clientmap: IXMLDOMNamedNodeMap;
 nodelist, clientlist, statuscodeskiplist, codemappingslist: IXMLDOMNodeList;
 CCnode, Clientnode, StatusCodeSkip, Codemappings, SkipCode, ClientTerm: IXMLDOMNode;
 uqcode, response: IXMLDOMNode;
 warningmsg: string;
 I: Integer;
begin
   Result := true;
   If not FindCCnode(scallcenter, doc, CCNode, nodemap) then
   begin
     warningmsg := 'call center ' + edtCallCenterName.text + ' not found';
     showmessage(warningmsg);
     LogResult.MethodName := 'ParseXML';
     LogResult.Status := warningmsg;
     LogResult.LogType := ltWarning;
     WriteLog(LogResult);
     Result := false;
     Application.Terminate;
   end;

   With CallCenter do
   begin
     CallCenterName               := Nodemap.getNamedItem('name').NodeValue;
     Respondto                    := 'att';//Nodemap.getNamedItem('respond_to').NodeValue;
     Initials                     := Nodemap.getNamedItem('initials').NodeValue;
     ID                           := Nodemap.getNamedItem('id').NodeValue;
     Posturl                      := Nodemap.getNamedItem('post_url').NodeValue;
     Include_epr_link             := Bool(Nodemap.getNamedItem('include_epr_link').NodeValue);
     Include_explanation_notes    := Bool(Nodemap.getNamedItem('include_explanation_notes').NodeValue);
     Include_public_locate_notes  := Bool(Nodemap.getNamedItem('include_public_locate_notes').NodeValue);
     Vendorname                   := UpperCase(Nodemap.getNamedItem('vendor_name').NodeValue);
     Facility                     := Nodemap.getNamedItem('facility').NodeValue;
     AllVersions                  := Bool(Nodemap.getNamedItem('all_versions').NodeValue);
     Defaultnote                  := Nodemap.getNamedItem('default_note').NodeValue;
     Center                       := Nodemap.getNamedItem('center').NodeValue;

      memo1.Lines.AddPair( 'call center Name', CallCenterName);
      memo1.Lines.AddPair('Respondto',Respondto);
      memo1.Lines.AddPair('Initials',Initials);
      memo1.Lines.AddPair('ID',ID);
      memo1.Lines.AddPair('Posturl',Posturl);
      memo1.Lines.AddPair('Include_epr_link',BooltoStr(Include_epr_link));
      memo1.Lines.AddPair('Include_explanation_notes',BooltoStr(Include_explanation_notes));
      memo1.Lines.AddPair('Include_public_locate_notes',BooltoStr(Include_public_locate_notes));
      memo1.Lines.AddPair('Vendorname',Vendorname);
      memo1.Lines.AddPair('Facility',Facility);
      memo1.Lines.AddPair('AllVersions',BooltoStr(AllVersions));
      memo1.Lines.AddPair('Defaultnote',Defaultnote);
      memo1.Lines.AddPair('Center',Center);
    end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('code_mappings');
   Codemappings := CCnode.SelectSingleNode('code_mappings');
   if Codemappings <> nil then
   begin
     Codemappingslist := CodeMappings.selectNodes('mapping');
     for i := 0 to CodeMappingslist.length - 1 do
     begin
       uqcode := IXMLDOMNode(CodeMappingslist[I]).attributes.getNamedItem('uq_code');  //status
       Response := IXMLDOMNode(CodeMappingslist[I]).attributes.getNamedItem('response'); //status sent
       if (uqcode <> nil) and (response <> nil) then
       begin
         CallCenter.Codemappings.Add(uqcode.NodeValue+'='+response.NodeValue);
         Memo1.Lines.Add('uqcode: '+uqcode.NodeValue+'    Response: '+Response.NodeValue);
        end;
     end;
     Memo1.Lines.Add('');
     Statuscodeskip :=  CCnode.selectSingleNode('status_code_skip');
     if Statuscodeskip <> nil then
     begin
       Statuscodeskiplist := Statuscodeskip.selectNodes('code');
       Memo1.Lines.Add('status_code_skip');
       for i := 0 to Statuscodeskiplist.length - 1 do
       begin
         SkipCode := IXMLDOMNode(Statuscodeskiplist[I]).attributes.getNamedItem('value');
         if SkipCode <> nil then
         begin
           CallCenter.StatusCodeSkip.Add(SkipCode.NodeValue);
           Memo1.Lines.Add('code: '+SkipCode.NodeValue);
         end;
       end;
     end;
   end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('clients');
   ClientNode :=  CCnode.selectSingleNode('clients');
   if ClientNode <> nil then
   begin
     clientlist :=  ClientNode.selectNodes('term');
     for i := 0 to Clientlist.length - 1 do
     begin
       ClientTerm := IXMLDOMNode(ClientList[I]).attributes.getNamedItem('name');
       if ClientTerm <> nil then
       begin
        CallCenter.Clients.Add(ClientTerm.NodeValue);
        Memo1.Lines.Add('term: ' +ClientTerm.NodeValue);
       end;
     end;
   end;
end;

function TfrmMainATTCal.ProcessConfigXML(xmlFile: tFilename):boolean;
var
  XMLDOMDocument: IXMLDOMDocument;
  XMLDOMNodeList: IXMLDOMNodeList;
  XMLDOMNode: IXMLDOMNode;
  aXMLDOMNode: IXMLDOMNode;
  logNode: IXMLDOMNode;
  xml: TStringList;
begin
  result := false;
    try
      xml := TStringList.Create;
      xml.LoadFromFile(xmlFile);
      XMLDOMDocument := CoDOMDocument.Create;
      try
        XMLDOMDocument.loadXML(xml.Text);
        if (XMLDOMDocument.parseError.ErrorCode <> 0) then
          raise Exception.CreateFmt('Error in Xml Data %s', [XMLDOMDocument.parseError]);

        logNode :=  XMLDOMDocument.selectSingleNode('//ticketparser/logdir');
        flogDir := logNode.attributes.getNamedItem('name').Text;
        If not DirectoryExists(flogDir) then flogDir := 'C:\QM\Logs';
        ForceDirectories(LogDir);


        If FillList(XMLDOMDocument, edtCallCenterName.text, xml) then
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.Status := 'XML successfully parsed';
          LogResult.LogType := ltInfo;
          WriteLog(LogResult);
        end;
      except
        on E: Exception do
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.ExcepMsg := E.Message;
          LogResult.DataStream := XMLDOMDocument.Text;
          LogResult.LogType := ltError;
          WriteLog(LogResult);
          exit;
        end;
      end;
      result := true;
    finally
      XMLDOMDocument := nil;
      if assigned(xml) then
      FreeAndNil(xml);
      btnParseXML.Enabled:=false;
    end;
end;

procedure TfrmMainATTCal.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir)  + CallCenter.Respondto + '-' + CallCenter.CallCenterName+'-'+FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMainATTCal.btnParseXMLClick(Sender: TObject);
begin
  if edtCallCenterName.Text = '' then
  begin
    showmessage('Call Center Required');
    Exit;
  end;

 ProcessConfigXML(fconfigXML);
end;

procedure TfrmMainATTCal.btnSendResponseClick(Sender: TObject);

const
  SEP = '|';
  API_KEY='&apikey=24643-';
  ELIPSIS='...';  //qm-617
var
  SerialNo:string;
  LocateID:string;
  eprLink:string;
  ClosedDate:string;
  PubTicketNote:string;  //qm-617
  Status:string;
  retStatus:string;
  sendResponse:string;
  relatedResponseSent:string;
  cntGood, cntBad :integer;
  listInx:integer;
  ResponseBody:string;
  requestID:string;
  Qty_Marked:string;
  delCnt:integer;
begin
  delCnt:=0;
  relatedResponseSent:='';
  PubTicketNote := '';
  LocateID:='';
  eprLink:='';
  retStatus:='';
  Status:='';
  Qty_Marked:='';
  requestID:='';

  try
    begin
      delCnt := delMultiQDups.ExecSQL;
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'delMultiQDups';
      LogResult.Status := 'deleted ' + intToStr(delCnt) + ' dups from multi-Q';
      WriteLog(LogResult);
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'delMultiQDups';
      LogResult.DataStream := delMultiQDups.sql.Text;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
  delCnt:=0;
  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value:=CallCenter.Respondto;   //hard coded
    Parameters.ParamByName('@CallCenter').Value:= CallCenter.CallCenterName;
    open;
    while not eof do
    begin
      If CallCenter.Clients.IndexOf(FieldByName('client_code').AsString)<0 then
      begin
        next;
        Memo1.lines.add('client code: '+FieldByName('client_code').AsString+ 'not returned');
        continue;
      end;
      SerialNo:= FieldByName('Serial_number').AsString;
      eprLink:= FieldByName('EPR_Link').AsString;
      LocateID:= FieldByName('locate_id').AsString;
      PubTicketNote := AnsiString(Trim(FieldByName('PubTicketNote').AsString)); //qm-617

      if length(PubTicketNote) > 400 then // //qm-617 there limit is 400 so we make it 397...
      begin
        PubTicketNote := (Copy(FieldByName('PubTicketNote').AsString,0,397))+ELIPSIS;
      end;
      Qty_Marked:= FieldByName('Qty_Marked').AsString;
      if length(Qty_Marked)=1 then Qty_Marked := '0'+Qty_Marked;
      ClosedDate  := FormatDateTime('mmddyy|hhnnA/P', FieldByName('closed_date').AsDateTime);
      relatedResponseSent:= FieldByName('related_response_sent').AsString;
      Status   := FieldByName('status').AsString;
      memo1.Lines.Add('PubTicketNote: '+PubTicketNote);
      memo1.Lines.Add('status: '+Status);
      listInx  :=CallCenter.Codemappings.IndexOfName(Status);
      memo1.Lines.Add('listInx '+IntToStr(listInx));
      if listInx<0 then
      begin
        next;
        Memo1.lines.add('Status: '+Status+ 'not returned');
        continue;
      end;
      Memo1.Lines.Add('Qty_Marked '+Qty_Marked);
      retStatus:= CallCenter.Codemappings.ValueFromIndex[CallCenter.Codemappings.IndexOfName(Status)];
      memo1.Lines.Add('Mapped retStatus: '+retStatus);
      retStatus := ReplaceText(retStatus, '##', Qty_Marked);
      Memo1.Lines.Add('Adding Qty to Status if ##: '+retStatus);



      //707956986|USAN|3010||112121|2200P|10|112121|UTI|https://qmcluster.utiliquest.com/epr/default.aspx?tid=136059310&dt=20211222&db=QM&code=b3db822f5ae1d20afd4e3bc9c61e1296857a90ec&pid=0&apikey=24643-707956986
      if eprLink='' then
      sendResponse:= SerialNo+SEP+CallCenter.Center+SEP+retStatus+SEP+PubTicketNote+SEP+ClosedDate+SEP+relatedResponseSent+SEP+ClosedDate+SEP+'UTI'+SEP
      else
      sendResponse:= SerialNo+SEP+CallCenter.Center+SEP+retStatus+SEP+PubTicketNote+SEP+ClosedDate+SEP+relatedResponseSent+SEP+ClosedDate+SEP+'UTI'+SEP+eprLink+API_KEY+LocateID;
      memo1.Lines.Add('sendResponse: '+sendResponse);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';
      RESTRequest1.AddBody(sendResponse, ctAPPLICATION_JSON);
      RESTRequest1.Method := TRESTRequestMethod.rmPOST;
      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + sendResponse;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' SerialNo: ' +SerialNo;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if RESTResponse1.StatusCode = 200 then
      begin
        ResponseBody:= RESTResponse1.Content;
        memo1.Lines.Add('ResponseBody: '+ResponseBody);
        requestID:=getData(ResponseBody, 'requestId');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process';
        Logresult.DataStream := sendResponse;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' SerialNo: ' +SerialNo;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter.CallCenterName;
        insertResponseLog.Status              := Status;
        insertResponseLog.ResponseSent        := retStatus;
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := IntToStr(RESTResponse1.StatusCode)+'-'+SerialNo; //qm-638
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Add to ResponseLog';
        Logresult.DataStream := insertResponseLog.Reply;
        WriteLog(LogResult);

        delResponse.Parameters.ParamByName('ResponseTo').Value := CallCenter.Respondto;
        delResponse.Parameters.ParamByName('LocateID').Value := LocateID;
        memo1.Lines.Add(delResponse.SQL.Text);

        try
          delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.ExcepMsg:=e.Message
          end;
        end;
      end
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content+' SerialNo '+SerialNo;
        LogResult.ExcepMsg := 'Body ' + retStatus;
        WriteLog(LogResult);
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter.CallCenterName;
        insertResponseLog.Status              := Status;
        insertResponseLog.ResponseSent        := retStatus;
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := RESTResponse1.StatusText;

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);
        Memo1.Lines.Add(retStatus);
        Next;
        Continue;
      end;
      Next;  //response
    end; //while-not EOF

  end; //with

//  delCnt:=CleanHouse;
//  begin
//      LogResult.LogType := ltInfo;
//      LogResult.MethodName := 'Clean House';
//      LogResult.Status := 'Deleted '+intToStr(delCnt)+ ' not eligible clients from multi-Q';
//      WriteLog(LogResult);
//  end;

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

	if cntBad>0 then
	begin
		idSMTP1.Connect;
		idSMTP1.Send(IdMessage1);
		idSMTP1.Disconnect();

		LogResult.LogType := ltInfo;
		LogResult.MethodName := 'Sending email';
		LogResult.Status := 'Some responses failed';
		LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
		WriteLog(LogResult);
	end;

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmMainATTCal.SetUpRecipients;
begin
  try
  idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+CallCenter.RespondTo+' '+CallCenter.CallCenterName;
  qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
  qryEMailRecipients.Open;
  if not qryEMailRecipients.eof then
  idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
  else
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'SetUpRecipients';
    LogResult.Status:= 'No recipients to send email to';
    WriteLog(LogResult);
   end;
  finally
  qryEMailRecipients.close;
  end;
end;

//function TfrmMainATTCal.CleanHouse():integer;
//const
//DEL_NON_CLIENTS =
//            'Delete '+
//            'FROM [QM].[dbo].[responder_multi_queue] '+
//            'where respond_to= ''%s'' '+
//            'and client_code not in (''%s'') ' ;
//
//begin
//  try
//      delCleanUpMultiQ.sql.text := Format(DEL_NON_CLIENTS,[CallCenter.Respondto, CallCenter.Clients.CommaText]);
//      result :=delCleanUpMultiQ.ExecSQL;
//  except on E: Exception do
//    begin
//        LogResult.LogType := ltError;
//        LogResult.MethodName := 'Clean House';
//        LogResult.Status := 'Deleted query failed';
//        LogResult.ExcepMsg := E.Message;
//        WriteLog(LogResult);
//    end;
//  end;
//end;

function TfrmMainATTCal.getData(JsonString: String; Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmMainATTCal.AddResponseLogEntry(insertResponseLog:TResponseLogEntry):boolean;
begin
  with insResponseLog.Parameters do   //clean out if success
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;

procedure TfrmMainATTCal.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMainATTCal.connectDB: boolean;
var
  connStr, connStrLog: String;
begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog:='Provider=SQLNCLI11.1;Password=Not Logged ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

procedure TfrmMainATTCal.edtCallCenterNameChange(Sender: TObject);
begin
  CallCenter.CallCenterName := edtCallcenterName.Text;
end;

function TfrmMainATTCal.ProcessINI: boolean;
var
  AttWebServer: TIniFile;
begin
  try
    AttWebServer := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := AttWebServer.ReadString('Database', 'Server', '');
    aDatabase := AttWebServer.ReadString('Database', 'DB', 'QM');
    ausername := AttWebServer.ReadString('Database', 'UserName', '');
    apassword := AttWebServer.ReadString('Database', 'Password', '');

//    LogResult.LogType := ltInfo;
//    LogResult.MethodName := 'ProcessINI';
//    WriteLog(LogResult);


  finally
    AttWebServer.Free;
  end;
end;

function TfrmMainATTCal.processParams: boolean;
begin
  fconfigXML:=ParamStr(1);
  edtCallcenterName.Text:= ParamStr(2);
//can log until after Respond_to is known
// LogResult.LogType := ltInfo;
//  LogResult.MethodName := 'processParams';
//  LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
//  WriteLog(LogResult);
end;

function TfrmMainATTCal.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
