program AttCalWebResponder;
//QM-498 AT&T Responder for ATTDsouth,ATTDNORCAL, ATTDNEVADA and PacBel
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  GlobalSU in 'GlobalSU.pas',
  uATTMainCal in 'uATTMainCal.pas' {frmMainATTCal};

var
  MyInstanceName: string;
// takes params "C:\Trunk1\tools\AttCalWebResponder\config.xml" "SCA1" "GUI" with GUI optional
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.CreateForm(TfrmMainATTCal, frmMainATTCal);
  Application.Run;
  end
  else
    Application.Terminate;

end.
