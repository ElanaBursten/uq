unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, ExtCtrls, DB, ADODB, Grids, DBGrids;

type
  TFormMain = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    ConvertButton: TButton;
    Log: TMemo;
    Convert: TADOCommand;
    Connection: TADOConnection;
    Pass: TEdit;
    User: TEdit;
    DB: TEdit;
    Server: TEdit;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    SourceProfitCenters: TDataSource;
    ProfitCenterButton: TButton;
    ProfitCenterGrid: TDBGrid;
    ProfitCenters: TADODataSet;
    Label6: TLabel;
    procedure ConvertButtonClick(Sender: TObject);
    procedure ProfitCenterButtonClick(Sender: TObject);
    procedure ProfitCenterGridCellClick(Column: TColumn);
  private
    procedure UpdateProfitCenterList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses OdAdoUtils;

{$R *.dfm}

procedure TFormMain.ConvertButtonClick(Sender: TObject);
var
  ProfitCenterID : integer;
  i: integer;
begin
  Assert(Connection.Connected, 'Not connected to DB');
  ProfitCenterID := 0;

  if ProfitCenterGrid.SelectedRows.Count > 0 then
    with ProfitCenterGrid.DataSource.DataSet do
      for i := 0 to ProfitCenterGrid.SelectedRows.Count -1 do begin
        GotoBookmark(pointer(ProfitCenterGrid.SelectedRows.Items[i]));
        try
          ProfitCenterID := ProfitCenters.FieldByName('profit_center').AsInteger;
          Log.Lines.Add('Changing damage_type COMPLETED to PENDING APPROVAL for profit center' + IntToStr(ProfitCenterID));
          Convert.Parameters.ParamValues['profit_center'] := ProfitCenterID;
          Convert.Execute;
          Log.Lines.Add('Profit Center: ' + IntToStr(ProfitCenterID) + ' successfully converted. ');
        except on E: Exception do
          Log.Lines.Add(Format(E.Message + ' error updating profit center %d', [ProfitCenterID]));
        end;
      end;//for

  UpdateProfitCenterList;
end;

procedure TFormMain.UpdateProfitCenterList;
begin
  ConvertButton.Enabled := False;

  try
    Connection.Close;
    Connection.ConnectionString := OdAdoUtils.MakeConnectionString(Server.Text, DB.Text, True, User.Text, Pass.Text, False);
    Connection.Open;

    ProfitCenters.Close;
    ProfitCenters.Open;
  except on E:Exception do
    Log.Lines.Add(E.Message);
  end;
end;

procedure TFormMain.ProfitCenterButtonClick(Sender: TObject);
begin
  UpdateProfitCenterList;
end;

procedure TFormMain.ProfitCenterGridCellClick(Column: TColumn);
begin
  ConvertButton.Enabled := (ProfitCenterGrid.SelectedRows.Count > 0);
end;

end.
