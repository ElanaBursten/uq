object FormMain: TFormMain
  Left = 250
  Top = 230
  Width = 733
  Height = 626
  Caption = 'Convert Damage Type'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 209
    Width = 717
    Height = 379
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 135
      Height = 78
      Caption = 
        'Profit Centers remaining that have  "COMPLETED" damages to conve' +
        'rt to "PENDING APPROVAL".  Select one or more profit centers to ' +
        'convert:'
      WordWrap = True
    end
    object Label6: TLabel
      Left = 304
      Top = 8
      Width = 18
      Height = 13
      Caption = 'Log'
    end
    object ConvertButton: TButton
      Left = 224
      Top = 152
      Width = 75
      Height = 25
      Caption = 'Convert'
      Enabled = False
      TabOrder = 2
      OnClick = ConvertButtonClick
    end
    object Log: TMemo
      Left = 304
      Top = 24
      Width = 385
      Height = 329
      TabOrder = 0
    end
    object ProfitCenterGrid: TDBGrid
      Left = 24
      Top = 104
      Width = 193
      Height = 249
      DataSource = SourceProfitCenters
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = ProfitCenterGridCellClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 209
    Align = alTop
    TabOrder = 1
    object Label5: TLabel
      Left = 95
      Top = 83
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Password'
    end
    object Label4: TLabel
      Left = 93
      Top = 59
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Username'
    end
    object Label3: TLabel
      Left = 95
      Top = 35
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Database'
    end
    object Label2: TLabel
      Left = 92
      Top = 11
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'DB Server'
    end
    object Pass: TEdit
      Left = 152
      Top = 80
      Width = 175
      Height = 21
      TabOrder = 3
      Text = 'doggy183'
    end
    object User: TEdit
      Left = 152
      Top = 56
      Width = 175
      Height = 21
      TabOrder = 2
      Text = 'sa'
    end
    object DB: TEdit
      Left = 152
      Top = 32
      Width = 175
      Height = 21
      TabOrder = 1
      Text = 'QMTesting'
    end
    object Server: TEdit
      Left = 152
      Top = 8
      Width = 175
      Height = 21
      TabOrder = 0
      Text = 'QManagerDBTest1'
    end
    object ProfitCenterButton: TButton
      Left = 160
      Top = 152
      Width = 145
      Height = 25
      Caption = 'Update Profit Centers'
      TabOrder = 4
      OnClick = ProfitCenterButtonClick
    end
  end
  object Convert: TADOCommand
    CommandText = 
      'update damage set damage_type = '#39'PENDING APPROVAL'#39' where damage_' +
      'type = '#39'COMPLETED'#39' and profit_center = :profit_center'
    Connection = Connection
    Parameters = <
      item
        Name = 'profit_center'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 344
    Top = 96
  end
  object Connection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=QMTestDB;Data Source=localhost;'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 344
    Top = 56
  end
  object SourceProfitCenters: TDataSource
    DataSet = ProfitCenters
    Left = 385
    Top = 136
  end
  object ProfitCenters: TADODataSet
    Connection = Connection
    CursorType = ctStatic
    CommandText = 
      '    select profit_center,count(*) as Count from damage '#13#10'    whe' +
      're damage_type = '#39'COMPLETED'#39' '#13#10'    group by profit_center'#13#10#13#10
    Parameters = <>
    Left = 344
    Top = 136
  end
end
