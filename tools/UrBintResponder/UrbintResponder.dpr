program UrbintResponder;
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
{$R '..\..\QMIcon.res'}
//qm-319/404
uses
  Vcl.Forms,
  Windows,
  SysUtils,
  uMain in 'uMain.pas' {Form1},
  GlobalSU in '..\..\common\GlobalSU.pas';

var
  MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    if ParamCount = 0 then Application.ShowMainForm := False;
    Application.CreateForm(TForm1, Form1);
    reportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Run;
  end
  else
    Application.Terminate;
end.
