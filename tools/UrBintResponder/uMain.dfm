object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 585
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 249
    Width = 657
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 657
    Height = 249
    Align = alTop
    TabOrder = 0
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 655
      Height = 247
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
      OnDblClick = Memo1DblClick
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 252
    Width = 657
    Height = 333
    Align = alClient
    TabOrder = 1
    object StatusBar1: TStatusBar
      Left = 1
      Top = 313
      Width = 655
      Height = 19
      Panels = <
        item
          Text = 'start'
          Width = 50
        end
        item
          Width = 100
        end
        item
          Text = 'count'
          Width = 50
        end
        item
          Width = 80
        end
        item
          Text = 'Finish'
          Width = 50
        end
        item
          Width = 100
        end
        item
          Text = 'Item'
          Width = 50
        end
        item
          Width = 50
        end>
    end
    object Memo2: TMemo
      Left = 1
      Top = 1
      Width = 655
      Height = 271
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 1
      OnDblClick = Memo2DblClick
    end
    object Panel3: TPanel
      Left = 1
      Top = 272
      Width = 655
      Height = 41
      Align = alBottom
      TabOrder = 2
      object btnRun: TButton
        Left = 248
        Top = 6
        Width = 75
        Height = 25
        Caption = 'Run'
        TabOrder = 0
        OnClick = btnRunClick
      end
      object btnRunOne: TButton
        Left = 368
        Top = 6
        Width = 75
        Height = 25
        Caption = 'Run One'
        TabOrder = 1
        OnClick = btnRunOneClick
      end
    end
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 40
    Top = 72
  end
  object delResponse: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'itemId'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Delete'
      '  FROM [QM].[dbo].[ticket_version_responder_queue]'
      'where  [queue_item_id] = :itemId')
    Left = 488
    Top = 152
  end
  object qryResponse: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '       [queue_item_id]'
      '      ,[respond_to]'
      '      ,[ticket_version_id]'
      '      ,[response]'
      '      ,[insert_date]'
      '      ,[ticket_format]'
      '  FROM [QM].[dbo].[ticket_version_responder_queue]'
      'order by [queue_item_id]')
    Left = 120
    Top = 168
  end
  object RESTClient1: TRESTClient
    Authenticator = SimpleAuthenticator1
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 128
    Top = 88
  end
  object SimpleAuthenticator1: TSimpleAuthenticator
    Left = 320
    Top = 88
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 216
    Top = 88
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'application/json'
    Left = 440
    Top = 80
  end
end
