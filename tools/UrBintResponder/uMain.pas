unit uMain;
//qm-319/404
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, IPPeerClient, REST.Client,
  REST.Authenticator.Simple, Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, REST.Types, JSON;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;

type
  TForm1 = class(TForm)
    ADOConn: TADOConnection;
    delResponse: TADOQuery;
    pnlTop: TPanel;
    pnlBottom: TPanel;
    Splitter1: TSplitter;
    Memo1: TMemo;
    qryResponse: TADOQuery;
    StatusBar1: TStatusBar;
    Memo2: TMemo;
    Panel3: TPanel;
    btnRun: TButton;
    RESTClient1: TRESTClient;
    SimpleAuthenticator1: TSimpleAuthenticator;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    btnRunOne: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRunClick(Sender: TObject);
    procedure btnRunOneClick(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure Memo2DblClick(Sender: TObject);
  private
    LogPath: string;
    LogResult: TLogResults;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    sBaseURL: string;
    sAuthorization: string;
    bConvertToUTC:integer;
    function ProcessINI:boolean;
    procedure clearLogRecord;
    function GetAppVersionStr: string;
    procedure WriteLog;
    function connectDB: boolean;
    procedure Process(Sender: TObject);
    procedure RestSetup;
    { Private declarations }
  public
    { Public declarations }
    APP_VER: string;
  end;

var
  Form1: TForm1;

implementation

uses
  Inifiles, System.StrUtils, System.DateUtils;
{$R *.dfm}

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ADOConn.Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  APP_VER := GetAppVersionStr;
  ProcessINI;
  connectDB;
  RestSetup;
  if ParamCount=0 then
  begin
    Process(Sender);
    Application.Terminate;
  end;
end;

procedure TForm1.RestSetup;
begin
  RESTRequest1.Params.AddHeader('Authorization',sAuthorization);
  RESTClient1.BaseURL:=sBaseURL;
end;

function TForm1.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure TForm1.Memo1DblClick(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm1.Memo2DblClick(Sender: TObject);
begin
  memo2.Clear;
end;

procedure TForm1.Process(Sender: TObject);
const
  TICKET_DATETIME = '"ticket_datetime"';
  TRANSMIT_DATETIME = '"transmit_datetime"';
  WORK_START_DATETIME = '"work_start_datetime"';
  SEP = ': "';
  DBL_QUOTES = #34;
var
  body, preJSON, retJSON: string;
  Fmt: TFormatSettings;
  cntGood, cntBad, I, J, CNT_DBL_QUOTES: integer;
  BodyList: TStringList;
  idxTICKET, idxTRANSMIT, idxWORK: integer;
  dateString: string;
  localDT: TDateTime;
  utcDate: TDateTime;

  function Util_StrLst_GetLineFromSubstr(iStrlst: TStringList; iSubstr: string): integer;
  begin
    for result := 0 to iStrlst.Count - 1 do
      if (Pos(iSubstr, iStrlst[result]) > 0) then
        Exit;
    result := -1;
  end;

  function GetUTC(dt: TDateTime): TDateTime;
  begin
    result := TTimeZone.Local.ToUniversalTime(dt);
  end;

begin
  try
    cntGood := 0;
    cntBad := 0;
    Fmt.ShortDateFormat := 'yyyy-mm-dd';
    Fmt.DateSeparator := '-';
    Fmt.LongTimeFormat := 'hh:nn:ss';
    Fmt.TimeSeparator := ':';

    BodyList := TStringList.Create;
    BodyList.Delimiter := #44;
    BodyList.StrictDelimiter := true;
    StatusBar1.Panels[1].Text := TimeToStr(time);
    qryResponse.Open;
    while not qryResponse.eof do
    begin
      BodyList.Clear;
      CNT_DBL_QUOTES := 0;
      body := '';
      dateString := '';
      localDT := 0.0;
      utcDate := 0.0;
      J := 0;
      idxTICKET := 0;
      idxTRANSMIT := 0;
      idxWORK := 0;

      body := qryResponse.FieldByName('response').AsString;
      StatusBar1.Panels[7].Text := qryResponse.FieldByName('queue_item_id').AsString;
      Memo1.Lines.Clear;
      Memo1.Lines.Text := body;
      if bConvertToUTC = 1 then // qm-404  sr
      begin
        try
          for J := 0 to length(body) - 1 do
          begin
            if body[J] = DBL_QUOTES then
              inc(CNT_DBL_QUOTES);
            if Odd(CNT_DBL_QUOTES) and (body[J] = #44) then
              body[J] := ' ';
          end;

          BodyList.DelimitedText := body;
          idxTICKET := Util_StrLst_GetLineFromSubstr(BodyList, TICKET_DATETIME);
          idxTRANSMIT := Util_StrLst_GetLineFromSubstr(BodyList, TRANSMIT_DATETIME);
          idxWORK := Util_StrLst_GetLineFromSubstr(BodyList, WORK_START_DATETIME);

          dateString := BodyList.Strings[idxTICKET];
          utcDate := GetUTC(StrToDateTime(copy(dateString, length(TICKET_DATETIME) + 5, length(dateString) - 22), Fmt));
          BodyList[idxTICKET] := TICKET_DATETIME + SEP + DateTimeToStr(utcDate, Fmt) + '"';

          dateString := BodyList.Strings[idxTRANSMIT];
          utcDate := GetUTC(StrToDateTime(copy(dateString, length(TRANSMIT_DATETIME) + 5, length(dateString) - 22), Fmt));
          BodyList[idxTRANSMIT] := TRANSMIT_DATETIME + SEP + DateTimeToStr(utcDate, Fmt) + '"';

          dateString := BodyList.Strings[idxWORK];
          utcDate := GetUTC(StrToDateTime(copy(dateString, length(WORK_START_DATETIME) + 5, length(dateString) - 22), Fmt));
          BodyList[idxWORK] := WORK_START_DATETIME + SEP + DateTimeToStr(utcDate, Fmt) + '"';
          preJSON := '';
          retJSON := '';
          for I := 0 to BodyList.Count - 1 do
            preJSON := preJSON + BodyList[I] + ',';

          retJSON := preJSON.Substring(0, preJSON.length - 1);
        except
          on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'Response scrub';
            LogResult.ExcepMsg := E.Message;
            LogResult.Status := 'Date '+dateString;
            LogResult.DataStream := 'body '+body;
            WriteLog;
            qryResponse.Next;
            continue;
          end;
        end;

        Memo1.Lines.Clear;
        Memo1.Text := 'JSON sent: ' + retJSON;
        Memo1.refresh;
      end
      else
        retJSON := body;

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';
      RESTRequest1.AddBody(retJSON, ctAPPLICATION_JSON);
      RESTRequest1.Method := TRESTRequestMethod.rmPOST;
      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo2.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + retJSON;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' ticket ver id:' +
            qryResponse.FieldByName('ticket_version_id').AsString;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog;
          qryResponse.Next;
          continue;
        end;

      end;

      Memo2.Lines.Add('-------------RESTResponse------------------------');
      Memo2.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo2.Lines.Add('-------------------------------------------------');

      if RESTResponse1.StatusCode = 201 then
      begin
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process';
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' ticket ver id:' +
          qryResponse.FieldByName('ticket_version_id').AsString;
        WriteLog;
        inc(cntGood);
        delResponse.Parameters.ParamByName('itemid').Value := qryResponse.FieldByName('queue_item_id').AsInteger;
        delResponse.ExecSQL;
      end
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        LogResult.ExcepMsg := 'Body ' + body;
        WriteLog;
        Memo2.Lines.Add('Returned Content' + RESTResponse1.Content);
        Memo2.Lines.Add(body);
      end;

      qryResponse.Next;
      StatusBar1.Panels[3].Text := IntToStr(cntGood);
      StatusBar1.refresh;
      Memo1.refresh;
      Memo2.refresh;
      Sleep(200);
      if Sender = btnRunOne then
        Exit;
    end;

  finally
    qryResponse.Close;
    BodyList.Free;
    StatusBar1.Panels[5].Text := TimeToStr(time);
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Process Complete';
    LogResult.Status := IntToStr(cntGood) + ' Successfully processed and ' + IntToStr(cntBad) + ' Failure(s)';
    WriteLog;
    // Memo1.Lines.Clear;
    // Memo2.Lines.Clear;
  end;
end;

procedure TForm1.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackSlash(LogPath)+'UrbintLog' + '_' + FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
    WriteLn(myFile, 'UrbintResp Version: ' + APP_VER);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TForm1.btnRunClick(Sender: TObject);
begin
  Process(Sender);
end;

procedure TForm1.btnRunOneClick(Sender: TObject);
begin
  Process(Sender);
end;

procedure TForm1.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TForm1.ProcessINI: boolean;
var
  UrbintRespIni: TIniFile;
  myPath: string;
begin
  result := true;
  try
    myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
    try
      UrbintRespIni := TIniFile.Create(myPath + 'UrbintResp.ini');
      LogPath := UrbintRespIni.ReadString('LogPaths', 'LogPath', '');
      ForceDirectories(LogPath);

      aServer := UrbintRespIni.ReadString('Database', 'Server', '');
      aDatabase := UrbintRespIni.ReadString('Database', 'DB', 'QM');
      ausername := UrbintRespIni.ReadString('Database', 'UserName', '');
      apassword := UrbintRespIni.ReadString('Database', 'Password', '');
      sBaseURL  := UrbintRespIni.ReadString('RestParams', 'BaseURL', '');
      sAuthorization := UrbintRespIni.ReadString('RestParams', 'Authorization', '');
      bConvertToUTC := UrbintRespIni.ReadInteger('RestParams', 'ConvertToUTC', 1);

      LogResult.LogType := ltInfo;
      LogResult.Status := 'Ini processed';
      LogResult.MethodName := 'ProcessINI';
      WriteLog;
    finally
      FreeAndNil(UrbintRespIni);
    end;
  except
    on E: Exception do
    begin
      result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ProcessINI';
      LogResult.ExcepMsg := E.Message;
      WriteLog;
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

function TForm1.connectDB: boolean;
var
  connStr: String;
begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStr;
        LogResult.ExcepMsg := E.Message;
        WriteLog;
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
