unit AttachmentFolderOrganizer;

interface

uses
  SysUtils, Classes, OdMiscUtils;

type
  TAttachmentFolderOrganizer = class(TObject)
  private
    StartingDir: string;
    FoldersToMoveList: TStringList;
    FOnLog: TLogEvent;
    FolderMoveCount: Integer;
    PassLimit: Integer;
    procedure HandleFolder(const FileName: string);
    function FindMisplacedFolders: Boolean;
    procedure MoveFoldersToRightDir;
    procedure Log(const Msg: string);
    function GetFoldersToMoveCount: Integer;
    procedure LogFoldersToMoveList;
  public
    constructor Create(const RootDir: string; const MaxPassCount: Integer);
    destructor Destroy; override;

    function Check: Boolean;
    procedure Fix;

    property OnLog: TLogEvent read FOnLog write FOnLog;
    property FoldersToMoveCount: Integer read GetFoldersToMoveCount;
  end;

  procedure DoCheckDirStructure(const RootDir: string);
  procedure DoFixDirStructure(const RootDir: string; const PassLimit: Integer=0);

implementation

{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

uses JclStrings, JclFileUtils;

procedure DoCheckDirStructure(const RootDir: string);
var
  AttachOrganizer: TAttachmentFolderOrganizer;
begin
  try
    AttachOrganizer := TAttachmentFolderOrganizer.Create(RootDir, 0);
    try
      AttachOrganizer.Check;
    finally
      FreeAndNil(AttachOrganizer);
    end;
  except
    on E: Exception do begin
      WriteLn('Could not complete CheckDirs request.');
      WriteLn(E.Message);
    end;
  end;
end;

procedure DoFixDirStructure(const RootDir: string; const PassLimit: Integer);
var
  AttachOrganizer: TAttachmentFolderOrganizer;
begin
  try
    AttachOrganizer := TAttachmentFolderOrganizer.Create(RootDir, PassLimit);
    try
      AttachOrganizer.Fix;
    finally
      FreeAndNil(AttachOrganizer);
    end;
  except
    on E: Exception do begin
      WriteLn('Could not complete FixDirs request.');
      WriteLn(E.Message);
    end;
  end;
end;

{ TAttachmentFolderOrganizer }

const
  AttachmentTypeArray: array[0..3] of string = ('Damages','Tickets','ThirdParty','Litigation');

constructor TAttachmentFolderOrganizer.Create(const RootDir: string; const MaxPassCount: Integer);
begin
  inherited Create;
  FoldersToMoveList := TStringList.Create;
  StartingDir := ExpandFileName(RootDir);
  PassLimit := MaxPassCount;
  Assert(StartingDir <> '', 'Missing StartingDir');
  Assert(PassLimit > -1, 'PassLimit should be 0 or greater');
  if not DirectoryExists(RootDir) then
    raise Exception.Create('The selected BaseDir does not exist.');
end;

destructor TAttachmentFolderOrganizer.Destroy;
begin
  FreeAndNil(FoldersToMoveList);
  inherited;
end;

function TAttachmentFolderOrganizer.Check: Boolean;
begin
  Assert(Assigned(FoldersToMoveList));
  Log(DateTimeToStr(Now) + ' Started check (NO UPDATES) for misplaced attachment folders under ' + StartingDir);
  if FindMisplacedFolders then
    LogFoldersToMoveList;
  Result := FoldersToMoveList.Count = 0;
  Log(DateTimeToStr(Now) + ' Attachment folder check is done.');
end;

procedure TAttachmentFolderOrganizer.Fix;
var
  PassCount: Integer;
begin
  Assert(Assigned(FoldersToMoveList));
  Log(DateTimeToStr(Now) + ' Started fixing misplaced attachment folders under ' + StartingDir);
  FolderMoveCount := 0;
  PassCount := 0;
  while FindMisplacedFolders do begin
    MoveFoldersToRightDir;
    Inc(PassCount);
    Log(' Completed pass ' + IntToStr(PassCount));
    if (PassLimit > 0) and (PassCount >= PassLimit) then begin
      Log(' Stopped because the pass limit was reached. There may be more files left to move.');
      Break;
    end;
  end;
  Log(' Moved ' + IntToStr(FolderMoveCount) + ' folders.');
  Log(DateTimeToStr(Now) + ' Attachment folder fix done.');
end;

procedure TAttachmentFolderOrganizer.MoveFoldersToRightDir;
var
  I: Integer;
  FolderName: string;
  DestFolder: string;
begin
  Assert(Assigned(FoldersToMoveList));
  for I := 0 to FoldersToMoveList.Count - 1 do begin
    try
      FolderName := RemoveSlash(FoldersToMoveList[I]);
      DestFolder := RemoveSlash(PathExtractPathDepth(FolderName, Integer(FoldersToMoveList.Objects[I])));
      if DirectoryExists(FolderName) then begin
        if not MoveDirectory(Foldername, DestFolder) then
          Log('Error: Cannot move ' + FolderName + ' to ' + DestFolder)
        else begin
          Log('Moved ' + FolderName + ' to ' + DestFolder);
          Inc(FolderMoveCount);
          if FolderMoveCount mod 10 = 0 then
            Log(' moved ' + IntToStr(FolderMoveCount) + ' folders as of ' + DateTimeToStr(Now));
        end;
      end;
    except
      on E: Exception do
        Log('Error moving folder ' + FolderName + '. ' + E.Message);
    end;
  end;
end;

function TAttachmentFolderOrganizer.FindMisplacedFolders: Boolean;
begin
  Assert(StartingDir <> '');
  Assert(Assigned(FoldersToMoveList));
  Log('Looking for misplaced folders...');
  FoldersToMoveList.Clear;
  FoldersToMoveList.BeginUpdate;
  try
    EnumDirectories(StartingDir, HandleFolder);
  finally
    FoldersToMoveList.EndUpdate;
  end;
  Result := FoldersToMoveCount > 0;
  if Result then
    Log(DateTimeToStr(Now) + ' Found ' + IntToStr(FoldersToMoveCount) + ' misplaced folders')
  else
    Log(DateTimeToStr(Now) + ' No misplaced attachment folders');
end;

procedure TAttachmentFolderOrganizer.HandleFolder(const FileName: string);
var
  NeedToMove: Boolean;
  I: Integer;
  FolderName: string;
  BaseFolderDepth: Integer;
  PathFolders: TStringList;
begin
  Assert(Assigned(FoldersToMoveList));
  if not DirectoryExists(FileName) then
    Exit;

  NeedToMove := False;
  BaseFolderDepth := 0;
  PathFolders := TStringList.Create;
  try
    StrToStrings(FileName, '\', PathFolders, True);
    // check to see if a base folder name appears more than once in the path
    for I := 0 to PathFolders.Count-1 do begin
      FolderName := PathFolders[I];
      if StringInArray(FolderName, AttachmentTypeArray) then begin
        if BaseFolderDepth > 0 then begin
          NeedToMove := True;
          Break;
        end else
          BaseFolderDepth := I-1;
      end;
    end;
  finally
    FreeAndNil(PathFolders);
  end;
  if NeedToMove then begin
    if PathIsUNC(FileName) then
      BaseFolderDepth := BaseFolderDepth - 2; // account for the extra servername slashes
    Assert(BaseFolderDepth > -1);
    FoldersToMoveList.AddObject(FileName, TObject(BaseFolderDepth));
  end;
end;

procedure TAttachmentFolderOrganizer.LogFoldersToMoveList;
var
  i: Integer;
begin
  for i := 0 to FoldersToMoveList.Count-1 do
    Log(' ' + FoldersToMoveList[i]);
end;

function TAttachmentFolderOrganizer.GetFoldersToMoveCount: Integer;
begin
  Result := FoldersToMoveList.Count;
end;

procedure TAttachmentFolderOrganizer.Log(const Msg: string);
begin
  if Assigned(FOnLog) then
    FOnLog(Self, Msg)
  else
    WriteLn(Msg);
end;

end.
