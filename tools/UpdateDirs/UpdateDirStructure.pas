unit UpdateDirStructure;

interface

procedure Initialize;
procedure Finalize;
procedure DoUpdateDirStructure(BaseDir: string; ForeignType: Integer);

implementation

{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

uses SysUtils, QMConst, OdMiscUtils, Windows, Classes, ADODB, DB,
     OdAdoUtils, DateUtils, ActiveX, FileCtrl, JclFileUtils;

var
  Conn: TADOConnection;
  DataSet: TADODataSet;
  FilesMoved: Integer;

procedure Log(LogText: string);
begin
  writeln(LogText);
end;

////

procedure Initialize;
begin
  CoInitialize(nil);
  Conn := TADOConnection.Create(nil);
  ConnectAdoConnectionWithIni(Conn, PathExtractFileNameNoExt(ParamStr(0))+'.ini');
  Dataset := TADODataSet.Create(nil);
  Dataset.Connection := Conn;
  Dataset.CommandType := cmdText;
  FilesMoved := 0;
end;

////

procedure Finalize;
begin
  try
    FreeAndNil(DataSet);
    Conn.Connected := False;
    FreeAndNil(Conn);
  finally
    CoUninitialize;
  end;
end;

////

function IsManualTicket(TicketId: Integer): Boolean;
const
  SQL = 'select status from ticket where ticket_id = %d';
begin
  try
    DataSet.CommandText := Format(SQL, [TicketId, TicketStatusManual]);
    DataSet.Open;
    Result := StrContains(TicketStatusManual, Dataset.FieldByName('status').AsString);
  finally
    DataSet.Close;
  end;
end;

////

procedure DirNamesFromForeignTypeAndID(ForeignType, ForeignID: Integer; var YearDir, DateDir: string);
const
  SQL = 'select top 1 * from %s where %s = %d %s';
var
  TableName: string;
  KeyName: string;
  CreateDateField: string;
  OrderByClause: string;
begin
  case ForeignType of
    qmftTicket:
      begin
        if IsManualTicket(ForeignID) then begin
          TableName := 'ticket';
          KeyName := 'ticket_id';
          CreateDateField := 'transmit_date';
          OrderByClause := '';
        end else begin
          TableName := 'ticket_version';
          KeyName := 'ticket_id';
          CreateDateField := 'arrival_date';
          OrderByClause := 'order by ticket_version_id';
        end
      end;
    qmftDamage:
      begin
        TableName := 'damage';
        KeyName := 'damage_id';
        CreateDateField := 'uq_notified_date';
        OrderByClause := '';
      end;
    qmft3rdParty:
      begin
        TableName := 'damage_third_party';
        KeyName := 'third_party_id';
        CreateDateField := 'uq_notified_date';
        OrderByClause := '';
      end;
    qmftLitigation:
      begin
        TableName := 'damage_litigation';
        KeyName := 'litigation_id';
        CreateDateField := 'summons_date';
        OrderByClause := '';
      end;
    else Log('Invalid foreign type: ForeignID is '+IntToStr(ForeignID));
  end;

  DataSet.CommandText := Format(SQL, [TableName, KeyName, ForeignID, OrderByClause]);
  try
    DataSet.Open;
    if DataSet.Eof then
      Log(Format('%s id %d not located to get upload year', [TableName, ForeignID]));
    YearDir := IntToStr(YearOf(DataSet.FieldByName(CreateDateField).AsDateTime));
    DateDir := FormatDateTime(DirFormatForAttachments,DataSet.FieldByName(CreateDateField).AsDateTime);
  finally
    DataSet.Close;
  end
end;

//////
// * GetIDDirs retrieves all directories which belongs to the old attachments
//   directories structure. These directories do not contain the character '-'
// * The result set contains only the directory name, not the full path
//
// Note: For performance reasons, I wrote this special version of the BuildFileList
//       function
////

function GetIDDirs(Path: string; const List: TStrings): Boolean;
var
  SearchRec: TSearchRec;
  R: Integer;
begin
  Assert(List <> nil);
  R := FindFirst(Path + '*', faDirectory, SearchRec);
  Result := R = 0;
  List.BeginUpdate;
  try
    if Result then
    begin
      while R = 0 do
      begin
        if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') and
           ((SearchRec.Attr and faDirectory) = faDirectory) and (not StrContains('-', SearchRec.Name)) then
          List.Add(SearchRec.Name);
        R := FindNext(SearchRec);
      end;
      Result := List.Count > 0;
    end;
  finally
    SysUtils.FindClose(SearchRec);
    List.EndUpdate;
  end;
end;

// The JCL BuildFileList does not respect the Attr parameter (a bug?),
// so we need to duplicate the code here with a fix for this problems
function BuildFileList(const Path: string; const Attr: Integer; const List: TStrings): Boolean;
var
  SearchRec: TSearchRec;
  R: Integer;
begin
  Assert(List <> nil);
  R := FindFirst(Path + '*', Attr, SearchRec);
  Result := R = 0;
  List.BeginUpdate;
  try
    if Result then
    begin
      while R = 0 do
      begin
        if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') and ((SearchRec.Attr and Attr) = Attr ) then
          List.Add(SearchRec.Name);
        R := FindNext(SearchRec);
      end;
      Result := List.Count > 0;// R = ERROR_NO_MORE_FILES;
    end;
  finally
    SysUtils.FindClose(SearchRec);
    List.EndUpdate;
  end;
end;

////

procedure MoveFiles(BaseDir, OldYearDir, IDDir: string; ForeignType: Integer);
var
  FileList: TStringList;
  i: Integer;
  NewYearDir, NewDateDir: string;
  ExistingFileName, NewFileName: string;
begin
  BaseDir := AddSlash(BaseDir);
  FileList := TStringList.Create;
  try
    if not BuildFileList(AddSlash(BaseDir + OldYearDir + '\' + IDDir), faArchive, FileList) then begin
      Log('No files to process under ' + BaseDir + IDDir);
      Exit;
    end;

    DirNamesFromForeignTypeAndID(ForeignType, StrToInt(IDDir), NewYearDir, NewDateDir);

    try
      ForceDirectories(BaseDir + NewYearDir + '\' + NewDateDir + '\' + IDDir);
    except
      on E: Exception do
        raise Exception.Create('Unable to create directory ' + BaseDir + NewYearDir + '\' + NewDateDir + '\' + IDDir + ': '+ E.Message);
    end;

    for i := 0 to FileList.Count-1 do begin
      ExistingFileName := AddSlash(BaseDir + OldYearDir + '\' + IDDir) + FileList[i];
      NewFileName := AddSlash(BaseDir + NewYearDir + '\' + NewDateDir + '\' + IDDir) + FileList[i];
      try
        FileMove(ExistingFileName, NewFileName, False);
        Inc(FilesMoved);
      except
        on E: Exception do
          Log('Unable to move file ' + ExistingFileName + ': '+ E.Message);
      end;
    end;

    try
      RemoveDir(BaseDir + OldYearDir + '\' + IDDir);
    except
      on E: Exception do
        Log('Unable to delete dir ' + BaseDir + OldYearDir + '\' + IDDir + ': '+ E.Message);
    end
  finally
    FreeAndNil(FileList);
  end;
end;

////

procedure DoUpdateDirStructure(BaseDir: string; ForeignType: Integer);
const
  Damages = 'Damages';
  Tickets = 'Tickets';
  ThirdParty = 'ThirdParty';
  Litigation = 'Litigation';
var
  Dir: string;
  YearDirectories: TStringList;
  IDDirectories: TStringList;
  i, j: Integer;
begin
  case ForeignType of
    qmftTicket: Dir := AddSlash(BaseDir) + Tickets;
    qmftDamage: Dir := AddSlash(BaseDir) + Damages;
    qmft3rdParty: Dir := AddSlash(BaseDir) + ThirdParty;
    qmftLitigation: Dir := AddSlash(BaseDir) + Litigation;
  else
    raise Exception.Create('Bad ForeignType');
  end;

  IDDirectories := TStringList.Create;
  YearDirectories := TStringList.Create;

  try
    Dir := AddSlash(Dir);
    if not BuildFileList(Dir, faDirectory, YearDirectories) then begin
      Log('No directories to process under ' + Dir);
      Exit;
    end;

    for i := 0 to YearDirectories.Count - 1 do begin
      IDDirectories.Clear;
      if not GetIDDirs(AddSlash(Dir + YearDirectories[i]), IDDirectories) then begin
        Log('No directories to process under ' + Dir + YearDirectories[i]);
        Continue;
      end;

      for j := 0 to IDDirectories.Count - 1 do begin
        try
          MoveFiles(Dir, YearDirectories[i], IDDirectories[j], ForeignType);
        except
          on E: Exception do
            Log('Unable to move files for ' + IDDirectories[j] + ': '+ E.Message);
        end;
        if (FilesMoved mod 1000) = 0 then
          Log(FormatFloat('000000000000', FilesMoved)+' files moved until now - '+DateTimeToStr(Now));
      end;
    end;
  finally
    FreeAndNil(IDDirectories);
    FreeAndNil(YearDirectories);
  end;
end;

end.
