program UpdateDirs;

{$APPTYPE CONSOLE}

{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  SysUtils,
  QMConst in '..\..\client\QMConst.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  UpdateDirStructure in 'UpdateDirStructure.pas',
  AttachmentFolderOrganizer in 'AttachmentFolderOrganizer.pas',
  OdAdoUtils in '..\..\common\OdAdoUtils.pas',
  OdDbUtils in '..\..\common\OdDbUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  UQDbConfig in '..\..\common\UQDbConfig.pas',
  OdExceptions in '..\..\common\OdExceptions.pas';

procedure ShowUsage;
var
  ExeName: string;
begin
  ExeName := ExtractFileName(ParamStr(0));
  WriteLn('');
  WriteLn('QM Attachment Directory Utility');
  WriteLn('Version: ' + AppVersionShort);
  WriteLn('Checks or fixes problems with the uploaded attachments directory structure.');
  WriteLn('');
  WriteLn('Syntax is: ' + ExeName + ' BaseDir RunMode [PassLimit]');
  WriteLn('');
  WriteLn('Command line options:');
  WriteLn('  /?      Show this help');
  WriteLn('');
  WriteLn('  BaseDir The starting attachment folder to process ');
  WriteLn('          (like c:\inetpub\ftproot\utl_attach\ or some dir below that).');
  WriteLn('');
  WriteLn('  RunMode MigrateDirs  Moves folders from old year layout to year\date layout.');
  WriteLn('          CheckDirs    Reports misplaced attachment folders (no updates).');
  WriteLn('          FixDirs      Relocates misplaced attachment folders.');
  WriteLn('');
  WriteLn('  PassLimit is an optional number of passes to limit the FixDirs operation.');
end;

// Directories:  ..\..\thirdparty\BetterADO

procedure RunUpdateDirStructure;
var
  Limit: Integer;
begin
  if ParamCount = 0 then
    ShowUsage
  else if ParamStr(1) = '/?' then
    ShowUsage
  else if (ParamCount >= 2) then begin
    if SameText(ParamStr(2), 'MigrateDirs') then begin
      Initialize;
      try
        DoUpdateDirStructure(ParamStr(1), qmftDamage);
        DoUpdateDirStructure(ParamStr(1), qmftTicket);
        DoUpdateDirStructure(ParamStr(1), qmft3rdParty);
        DoUpdateDirStructure(ParamStr(1), qmftLitigation);
      finally
        Finalize;
      end;
    end else if SameText(ParamStr(2), 'CheckDirs') then
      DoCheckDirStructure(ParamStr(1))
    else if SameText(ParamStr(2), 'FixDirs') then begin
      Limit := StrToIntDef(ParamStr(3), 0);
      DoFixDirStructure(ParamStr(1), Limit);
    end else
      ShowUsage;
  end else
    ShowUsage;
end;

begin
  RunUpdateDirStructure;
  Sleep(3000);
end.
