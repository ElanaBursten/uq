program PTOSickBalanceImport;

{$R 'QMVersion.res' '..\..\QMVersion.rc'}
{$R '..\..\QMIcon.res'}
uses
  Vcl.Forms,
  SysUtils,
  IniFiles,
  PTOSickBalance in 'PTOSickBalance.pas' {FormPTOSickBalance},
  GlobalSU in 'GlobalSU.pas',
  uPTOSickBalanceLogger in 'uPTOSickBalanceLogger.pas';


var
   MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;

    Application.Initialize;
    Application.ShowMainForm := True;
    Application.CreateForm(TFormPTOSickBalance, FormPTOSickBalance);
  Application.MainFormOnTaskbar := True;
    Application.Run;
  end else Application.Terminate;
end.
