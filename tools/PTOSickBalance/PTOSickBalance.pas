unit PTOSickBalance;
 //QM-579   bp
 //track sick balance for users
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations,
  Data.DB, cxDBData, Vcl.ComCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  System.ImageList, Vcl.ImgList, Data.Win.ADODB, StrUtils, Vcl.StdCtrls,
  Vcl.ExtCtrls, uPTOSickBalanceLogger, IniFiles, odHourGlass, OdMiscUtils,
  cxTextEdit, Vcl.Menus, cxButtons, comObj, dxSkinsCore, dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans,
  dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinOffice2019Black,
  dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TFormPTOSickBalance = class(TForm)
    Grid: TcxGrid;
    GridView: TcxGridDBTableView;
    GridViewCompany_Code: TcxGridDBColumn;
    GridViewEmployee_Number: TcxGridDBColumn;
    GridViewemp_id: TcxGridDBColumn;
    GridViewFirst_Name: TcxGridDBColumn;
    GridViewLast_Name: TcxGridDBColumn;
    GridViewEmployment_Status: TcxGridDBColumn;
    GridViewSICK_Option: TcxGridDBColumn;
    GridViewSICK_Balance: TcxGridDBColumn;
    GridViewPTO_Option: TcxGridDBColumn;
    GridViewPTO_Balance: TcxGridDBColumn;
    GridViewPTO_Accrual: TcxGridDBColumn;
    GridViewWeeksRemaining: TcxGridDBColumn;
    GridViewAccrual_to_EOY: TcxGridDBColumn;
    GridViewEst_Bal_at_EOY: TcxGridDBColumn;
    GridViewSupervisor_Name: TcxGridDBColumn;
    GridViewEMail_Sup: TcxGridDBColumn;
    GridLevel: TcxGridLevel;
    pcImportProblems: TPageControl;
    EmpIDProblems: TTabSheet;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridDBColumn22: TcxGridDBColumn;
    cxGridDBColumn23: TcxGridDBColumn;
    cxGridDBColumn24: TcxGridDBColumn;
    cxGridDBColumn25: TcxGridDBColumn;
    cxGridDBColumn26: TcxGridDBColumn;
    cxGridDBColumn27: TcxGridDBColumn;
    cxGridDBColumn28: TcxGridDBColumn;
    cxGridDBColumn29: TcxGridDBColumn;
    cxGridDBColumn30: TcxGridDBColumn;
    cxGridDBColumn31: TcxGridDBColumn;
    cxGridDBColumn32: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    AdNameProblems: TTabSheet;
    ImageList: TImageList;
    ADOConn: TADOConnection;
    FileADOConnection: TADOConnection;
    PTOSickBalance: TADOQuery;
    PTOSickBalanceCompanyCode: TWideStringField;
    PTOSickBalanceEmployeeNumber: TWideStringField;
    PTOSickBalanceFirstName: TWideStringField;
    PTOSickBalanceLastName: TWideStringField;
    PTOSickBalanceEmploymentStatus: TWideStringField;
    PTOSickBalanceSICKOption: TWideStringField;
    PTOSickBalanceSICKBalance: TFloatField;
    PTOSickBalancePTOOption: TWideStringField;
    PTOSickBalancePTOBalance: TFloatField;
    PTOSickBalancePTOAccrual: TFloatField;
    PTOSickBalanceWeeksRemaining: TFloatField;
    PTOSickBalanceAccrualtoEOY: TFloatField;
    PTOSickBalanceEstBalatEOY: TFloatField;
    PTOSickBalanceSupervisorNameFirstMILastSuffix: TWideStringField;
    PTOSickBalanceEmailSupervisor: TWideStringField;
    TmpTableEmpID: TADODataSet;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    btnImportFile: TButton;
    EmpBalance: TADODataSet;
    qryEmpid: TADOQuery;
    TmpTableADName: TADODataSet;
    qryEmpIDProblems: TADOQuery;
    dsTmpTableEmpID: TDataSource;
    dsTmpTableADName: TDataSource;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    EmpBalanceSearch: TCheckBox;
    dsEmpBalance: TDataSource;
    insEmpBalToHistory: TADOQuery;
    truncateEmpBal: TADOCommand;
    PopupMenuExport: TPopupMenu;
    mnuExport: TMenuItem;
    procedure btnImportFileClick(Sender: TObject);
    procedure OpenFile;
    procedure FormCreate(Sender: TObject);
    procedure EmpBalanceSearchClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mnuExportClick(Sender: TObject);
  private
    connStr: String;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    function LeftPad(value: string; length: integer=6; pad: char='0'): string;
    procedure ImportExcelData;
    procedure FillProblemTable(EmpNumber: String; TempDataSet: TADODataset);
    function connectDB: boolean;
    procedure ReadIni;
    function EnDeCrypt(const Value: String): String;
    procedure ExportToExcel(Query: TDataSet);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPTOSickBalance: TFormPTOSickBalance;

implementation

{$R *.dfm}

procedure TFormPTOSickBalance.btnImportFileClick(Sender: TObject);
begin
 OpenFile;
end;

function TFormPTOSickBalance.LeftPad(value: string; length: integer=6; pad: char='0'): string;
begin
  result := RightStr(StringOfChar(pad,length) + value, length );
end;

procedure TFormPTOSickBalance.OpenFile;
var
  Connstr: String;
  SheetList: TStringList;
  FilePath: TFileName;
  SheetName: String;
  I: Integer;
begin
   Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  //first program parameter is file path, file dialog for UI mode

   openDialog1.Filter := 'Excel files|*.xlsx';
   if OpenDialog1.Execute then
   begin
     If OpenDialog1.FileName<>'' then
       FilePath:= OpenDialog1.FileName
     else
      ShowMessage('Please select the correct file');
   end;

  FileADOConnection.Close;
  Connstr := Connstr + FilePath;

  try
    FileADOConnection.ConnectionString := Connstr;
    FileADOConnection.Open;
    SheetList := TStringlist.Create;
    FileADOConnection.GetTableNames(SheetList);
    SheetName := '';
    for I := 0 to SheetList.Count-1 do
    begin
      if SheetList[I] = '''UTQ & LOC$''' then
      begin
        SheetName := SheetList[I];
        break;
      end;
    end;

    if SheetName = '' then
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Process Failed';
      LogResult.ExcepMsg := 'Excel Sheet "UTQ & LOC" not found';
      LogResult.MethodName := 'OpenFile';
      WriteLog(LogResult);

      raise Exception.Create('Excel Sheet "UTQ & LOC"not found');
    end;

    PTOSickBalance.SQL.Text := 'Select * from [' + SheetName + ']';
    PTOSickBalance.Open;
    SheetList.Free;

     //clear the employee_balance table
    EmpBalance.Close;

    If insEmpBalToHistory.ExecSQL > 0 then
    ADOConn.Execute('truncate table employee_balance');
    EmpBalance.Open;

    ImportExcelData;

    except
      raise;
    end;
  end;

  procedure TFormPTOSickBalance.FillProblemTable(EmpNumber: String; TempDataSet: TADODataset);
  begin
    TempDataSet.Open;
    TempDataSet.Append;
    TempDataSet.FieldValues['Employee_Number'] := EmpNumber;
    TempDataSet.FieldValues['Company_Code'] := PTOSickBalance.FieldValues['Company Code'];
    TempDataSet.FieldValues['First_Name'] := PTOSickBalance.FieldValues['First Name'];
    TempDataSet.FieldValues['Last_Name'] := PTOSickBalance.FieldValues['Last Name'];
    TempDataSet.FieldValues['Employment_Status'] := PTOSickBalance.FieldValues['Employment Status'];
    TempDataSet.FieldValues['SICK_Option'] := PTOSickBalance.FieldValues['SICK Option'];
    TempDataSet.FieldValues['SICK_Balance'] := PTOSickBalance.FieldValues['SICK Balance'];
    TempDataSet.FieldValues['PTO_Option'] := PTOSickBalance.FieldValues['PTO Option'];
    TempDataSet.FieldValues['PTO_Balance'] := PTOSickBalance.FieldValues['PTO Balance'];
    TempDataSet.FieldValues['PTO_Accrual'] := PTOSickBalance.FieldValues['PTO Accrual'];
    TempDataSet.FieldValues['WeeksRemaining'] := PTOSickBalance.FieldValues['WeeksRemaining'];
    TempDataSet.FieldValues['Accrual_to_EOY'] := PTOSickBalance.FieldValues['Accrual to EOY'];
    TempDataSet.FieldValues['Est_Bal_at_EOY'] := PTOSickBalance.FieldValues['Est Bal at EOY'];
    TempDataSet.FieldValues['Supervisor_Name'] := PTOSickBalance.FieldValues['Supervisor Name (First MI Last Suffix)'];
    TempDataSet.FieldValues['Email_Sup'] := PTOSickBalance.FieldValues['Email (Supervisor)'];
    TempDataSet.Post;
  end;

procedure TFormPTOSickBalance.FormDestroy(Sender: TObject);
begin
  ADOConn.Close;
  PTOSickBalance.Close;
  FileADOConnection.Close;
end;

procedure TFormPTOSickBalance.FormCreate(Sender: TObject);
var
  VersionString:string;
begin
  if GetFileVersionNumberString(Application.ExeName, VersionString) then
      StatusBar1.Panels[3].Text := VersionString;
  ReadIni;
  Connectdb;
end;

procedure TFormPTOSickBalance.ReadIni;
var
  filepath: String;
  PTOSickBalanceINI: TIniFile;
const
  INI_FILE = 'PTOSickBalance';
begin
  try
    try
      filePath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
      PTOSickBalanceINI := TIniFile.Create(filePath + INI_FILE + '.ini');
      aServer := PTOSickBalanceINI.ReadString('Database', 'Server', '');
      aDatabase := PTOSickBalanceINI.ReadString('Database', 'DB', 'QM');
      ausername := PTOSickBalanceINI.ReadString('Database', 'UserName', '');
      apassword := PTOSickBalanceINI.ReadString('Database', 'Password', '');
      aTrusted  := PTOSickBalanceINI.ReadString('Database', 'Trusted', '1');

//      connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +';Persist Security Info=True;User ID=' + ausername +';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
      StatusBar1.Panels[1].Text :=  aServer;
//      ADOConn.ConnectionString := connStr;
      LogPath :=  PTOSickBalanceINI.ReadString('Paths', 'LogPath', 'C:\QM\Logs');
      ForceDirectories(LogPath);
    except
      On E:Exception do
      begin
        Raise exception.Create('Failed to read from Ini file: ' + E.Message);
      end;
    end;
  finally
    FreeAndNil(PTOSickBalanceINI);
  end;
end;

function TFormPTOSickBalance.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  Result := false;
  try

    try
          LogConnStr := 'Provider=SQLNCLI11;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
           ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
          if LeftStr(apassword, 4) = DECRYPT then
          begin
            apassword := copy(apassword, 5, maxint);
            apassword := EnDeCrypt(apassword);
          end
          else
          begin
            Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
          end;

          connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
           ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
          ADOConn.ConnectionString := connStr;
          ADOConn.Open;
    except on E: Exception do
       Showmessage('connStr: '+connStr +' '+e.Message);
    end;
  finally
     Result := ADOConn.Connected;
    if Result then
      StatusBar1.Panels[1].Text :=  aServer;
  end;
end;

function TFormPTOSickBalance.EnDeCrypt(const Value: String): String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TFormPTOSickBalance.EmpBalanceSearchClick(Sender: TObject);
begin
 inherited;
  if EmpBalanceSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TFormPTOSickBalance.mnuExportClick(Sender: TObject);
begin
   if pcImportProblems.ActivePage = ADNameProblems then
      ExporttoExcel(TmpTableADName) else
   if pcIMportProblems.ActivePage = EmpIDProblems then
       ExporttoExcel(TmpTableEmpID);
end;

procedure TFormPTOSickBalance.ExportToExcel(Query: TDataSet);

var
  Cursor: IInterface;
  x: integer;
  Range, Worksheet, Excel, Data: Variant;
  sValue: WideString;

procedure KillExcel(var App: Variant);
var
  ProcID: DWORD;
  hProc: THandle;
  hW: HWND;
begin
  hW := App.Application.Hwnd;
  // close with usual methods
  App.DisplayAlerts := False;
  App.Workbooks.Close;
  App.Quit;
  App := Unassigned;
  // close with WinApi
  if not IsWindow(hW) then Exit; // already closed?
  GetWindowThreadProcessId(hW, ProcID);
  hProc := OpenProcess(PROCESS_TERMINATE, False, ProcID);
  TerminateProcess(hProc, 0);
end;

begin
  if (not Query.Active) or (Query.IsEmpty) then
  begin
    MessageDlg('Query results unavailable', mtInformation, [mbOk], 0, mbOk);
    Exit;
  end;

  try
    try
      try
       Excel := CreateOleObject( 'Excel.Application' );
      except
       on E: Exception do
         raise Exception.Create( 'Error starting Excel. Details: ' + E.Message );
      end;
      cursor := ShowHourGlass;

      Excel.Workbooks.Add;
      WorkSheet := Excel.Sheets.Add;
     // translate data into a VarArray
      Data := VarArrayCreate( [0, Query.RecordCount, 0, Query.FieldCount-1], varVariant);

      for x := 0 to Query.FieldCount - 1 do
        Data[0, x] := Query.Fields[x].FieldName;

      System.Variants.NullStrictConvert := False;
      Query.First;
      while not Query.Eof do begin
        for x := 0 to Query.FieldCount - 1 do
        begin
          sValue := Query.Fields[x].Value; //Use Widestring to prevent type errors in Excel
          Data[Query.RecNo, x] := sValue;
        end;
        Query.Next;
      end;

      Range := WorkSheet.Range[ WorkSheet.Cells[1,1], WorkSheet.Cells[Query.RecordCount + 1, Query.FieldCount] ];
     {copy data from allocated variant array}
      Range.Value := Data;
      Excel.Visible := true;
    except
      On E:Exception do
      begin
        KillExcel(Excel); //Force Excel COM Object to be released
        ShowMessage(E.Message);
      end;
    end;
  finally
     Data := Unassigned;
     Range := Unassigned;
     WorkSheet := Unassigned;
     Excel := Unassigned;
  end;
end;

procedure TFormPTOSickBalance.ImportExcelData;
var
  I: Integer;
  EmpNumber: String;
  Cursor: IInterface;
  TempIndex: Integer;
begin
  //Import the EXCEL data into the table
  try
    ADOConn.BeginTrans;

      Cursor := ShowHourGlass;
      TmpTableADName.Close;
      TmpTableEmpID.Close;
      for TempIndex := 1 to 2 do
      begin   //Temp tables for problem data (no matching emp id or ad_username is null)
        ADOConn.Execute('If Object_ID(''TEMPDB..#Temp' + IntToStr(TempIndex) + ''') is not NULL Drop Table #Temp' + IntToStr(TempIndex) +'');
        ADOConn.Execute('Create Table #Temp' + IntToStr(TempIndex)+ '(Company_Code nchar(10),Employee_Number nchar(6), ' +
        'First_Name varchar(20), Last_Name varchar(30), Employment_Status nchar(12), SICK_Option nchar(10), ' +
        'SICK_Balance decimal (12,6), PTO_Option nchar (10), PTO_Balance decimal(12, 6), PTO_Accrual decimal(12, 6), ' +
        'WeeksRemaining decimal(12, 8), Accrual_to_EOY decimal(12, 6), Est_Bal_at_EOY decimal(12, 8), ' +
        'Supervisor_Name nvarchar(50), EMail_Sup nvarchar(50))');
      end;
      TmpTableADName.Open;
      TmpTableEmpID.Open;

    if PTOSickBalance.RecordCount>0 then
    begin
      StatusBar1.Panels[7].Text := IntToStr(PTOSickBalance.RecordCount-1);
      PTOSickBalance.First;
      for I := 0 to PTOSickBalance.RecordCount-1 do
      begin
        EmpNumber := PTOSickBalance.FieldValues['Employee Number'];
        EmpNumber := LeftPad(empnumber);
        qryEmpID.Close;
        qryEmpid.Parameters.ParamByName('empnumber').Value := EmpNumber;
        qryEmpId.Open;
        if not qryEmpId.EOF then
        begin
          EmpBalance.Append;
          EmpBalance.FieldValues['emp_id'] := qryEmpID.FieldByName('emp_id').AsInteger;
          qryEmpID.Close;
        end
        else
        begin
          qryEmpIDProblems.Parameters.ParamByName('empnumber').Value := EmpNumber;
          qryEmpIDProblems.Open;
          if qryEmpIDProblems.EOF then
          begin
            FillProblemTable(EmpNumber, TmpTableEmpID);
          end else
          begin
            FillProblemTable(EmpNumber, TmpTableADName);
          end;
          qryEmpIDProblems.Close;

          EmpBalance.Cancel;
          PTOSickBalance.Next;
          Continue;
        end;
        //fill employee_balance table
        EmpBalance.FieldValues['Employee_Number'] := EmpNumber;
        EmpBalance.FieldValues['Company_Code'] := PTOSickBalance.FieldValues['Company Code'];
        EmpBalance.FieldValues['First_Name'] := PTOSickBalance.FieldValues['First Name'];
        EmpBalance.FieldValues['Last_Name'] := PTOSickBalance.FieldValues['Last Name'];
        EmpBalance.FieldValues['Employment_Status'] := PTOSickBalance.FieldValues['Employment Status'];
        EmpBalance.FieldValues['SICK_Option'] := PTOSickBalance.FieldValues['SICK Option'];
        EmpBalance.FieldValues['SICK_Balance'] := PTOSickBalance.FieldValues['SICK Balance'];
        EmpBalance.FieldValues['PTO_Option'] := PTOSickBalance.FieldValues['PTO Option'];
        EmpBalance.FieldValues['PTO_Balance'] := PTOSickBalance.FieldValues['PTO Balance'];
        EmpBalance.FieldValues['PTO_Accrual'] := PTOSickBalance.FieldValues['PTO Accrual'];
        EmpBalance.FieldValues['WeeksRemaining'] := PTOSickBalance.FieldValues['WeeksRemaining'];
        EmpBalance.FieldValues['Accrual_to_EOY'] := PTOSickBalance.FieldValues['Accrual to EOY'];
        EmpBalance.FieldValues['Est_Bal_at_EOY'] := PTOSickBalance.FieldValues['Est Bal at EOY'];
        EmpBalance.FieldValues['Supervisor_Name'] := PTOSickBalance.FieldValues['Supervisor Name (First MI Last Suffix)'];
        EmpBalance.FieldValues['Email_Sup'] := PTOSickBalance.FieldValues['Email (Supervisor)'];
        EmpBalance.Post;

        Application.ProcessMessages;
        StatusBar1.Panels[5].Text := IntToStr(I);
        PTOSickBalance.Next;
      end;
    end;
      LogResult.MethodName := 'ImportExcelData';
      LogResult.Status := '****************** Import Complete ****************';
      LogResult.LogType := ltInfo;
      WriteLog(LogResult);

    ADOConn.CommitTrans;
  except
    On E:Exception do
    begin
     ADOConn.RollbackTrans;

     begin
       LogResult.LogType := ltError;
       LogResult.Status := 'Excel import failed';
       LogResult.ExcepMsg := 'Database Error: ' + E.Message;
       LogResult.MethodName := 'dbConnect';
       WriteLog(LogResult);
       raise;
     end;
    end;
  end;
end;


end.
