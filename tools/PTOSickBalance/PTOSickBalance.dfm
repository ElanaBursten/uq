object FormPTOSickBalance: TFormPTOSickBalance
  Left = 0
  Top = 0
  Caption = 'PTO Sick Balance'
  ClientHeight = 682
  ClientWidth = 956
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 377
    Width = 956
    Height = 9
    Cursor = crVSplit
    Align = alTop
  end
  object Grid: TcxGrid
    Left = 0
    Top = 35
    Width = 956
    Height = 342
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object GridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Enabled = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Enabled = False
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Enabled = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Enabled = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = dsEmpBalance
      DataController.Filter.MaxValueListCount = 10
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.MRUItemsList = False
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsCustomize.ColumnFiltering = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object GridViewCompany_Code: TcxGridDBColumn
        Caption = 'Company Code'
        DataBinding.FieldName = 'Company_Code'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 83
      end
      object GridViewEmployee_Number: TcxGridDBColumn
        Caption = 'Employee Nbr'
        DataBinding.FieldName = 'Employee_Number'
        Width = 75
      end
      object GridViewemp_id: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        Width = 47
      end
      object GridViewFirst_Name: TcxGridDBColumn
        Caption = 'First Name'
        DataBinding.FieldName = 'First_Name'
        Width = 59
      end
      object GridViewLast_Name: TcxGridDBColumn
        Caption = 'Last Name'
        DataBinding.FieldName = 'Last_Name'
      end
      object GridViewEmployment_Status: TcxGridDBColumn
        Caption = 'Emp Status'
        DataBinding.FieldName = 'Employment_Status'
        Width = 63
      end
      object GridViewSICK_Option: TcxGridDBColumn
        Caption = 'SICK Option'
        DataBinding.FieldName = 'SICK_Option'
      end
      object GridViewSICK_Balance: TcxGridDBColumn
        DataBinding.FieldName = 'SICK_Balance'
      end
      object GridViewPTO_Option: TcxGridDBColumn
        Caption = 'PTO Option'
        DataBinding.FieldName = 'PTO_Option'
      end
      object GridViewPTO_Balance: TcxGridDBColumn
        Caption = 'PTO Balance'
        DataBinding.FieldName = 'PTO_Balance'
        Width = 68
      end
      object GridViewPTO_Accrual: TcxGridDBColumn
        Caption = 'PTO Accrual'
        DataBinding.FieldName = 'PTO_Accrual'
      end
      object GridViewWeeksRemaining: TcxGridDBColumn
        DataBinding.FieldName = 'WeeksRemaining'
        Width = 89
      end
      object GridViewAccrual_to_EOY: TcxGridDBColumn
        Caption = 'Accrual to EOY'
        DataBinding.FieldName = 'Accrual_to_EOY'
        Width = 81
      end
      object GridViewEst_Bal_at_EOY: TcxGridDBColumn
        Caption = 'Est Bal at EOY'
        DataBinding.FieldName = 'Est_Bal_at_EOY'
        Width = 79
      end
      object GridViewSupervisor_Name: TcxGridDBColumn
        Caption = 'Supervisor Name'
        DataBinding.FieldName = 'Supervisor_Name'
        Width = 89
      end
      object GridViewEMail_Sup: TcxGridDBColumn
        Caption = 'EMail Sup'
        DataBinding.FieldName = 'EMail_Sup'
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object pcImportProblems: TPageControl
    Left = 0
    Top = 386
    Width = 956
    Height = 272
    ActivePage = EmpIDProblems
    Align = alClient
    Images = ImageList
    PopupMenu = PopupMenuExport
    TabOrder = 1
    object AdNameProblems: TTabSheet
      Caption = 'AD Name Problems'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 948
        Height = 243
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Insert.Enabled = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Enabled = False
          Navigator.Buttons.Delete.Enabled = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Enabled = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Enabled = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Enabled = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Enabled = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataModeController.SmartRefresh = True
          DataController.DataSource = dsTmpTableADName
          DataController.Filter.MaxValueListCount = 10
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.MRUItemsList = False
          Filtering.ColumnMRUItemsList = False
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsCustomize.ColumnFiltering = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'Company Code'
            DataBinding.FieldName = 'Company_Code'
            Width = 84
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = 'Employee Nbr'
            DataBinding.FieldName = 'Employee_Number'
            Width = 78
          end
          object cxGridDBColumn4: TcxGridDBColumn
            Caption = 'First Name'
            DataBinding.FieldName = 'First_Name'
          end
          object cxGridDBColumn5: TcxGridDBColumn
            Caption = 'Last Name'
            DataBinding.FieldName = 'Last_Name'
            Width = 61
          end
          object cxGridDBColumn6: TcxGridDBColumn
            Caption = 'Emp Status'
            DataBinding.FieldName = 'Employment_Status'
            Width = 67
          end
          object cxGridDBColumn7: TcxGridDBColumn
            Caption = 'SICK Option'
            DataBinding.FieldName = 'SICK_Option'
            Width = 68
          end
          object cxGridDBColumn8: TcxGridDBColumn
            Caption = 'SICK Balance'
            DataBinding.FieldName = 'SICK_Balance'
            Width = 74
          end
          object cxGridDBColumn9: TcxGridDBColumn
            Caption = 'PTO Option'
            DataBinding.FieldName = 'PTO_Option'
          end
          object cxGridDBColumn10: TcxGridDBColumn
            Caption = 'PTO Balance'
            DataBinding.FieldName = 'PTO_Balance'
            Width = 67
          end
          object cxGridDBColumn11: TcxGridDBColumn
            Caption = 'PTO Accrual'
            DataBinding.FieldName = 'PTO_Accrual'
          end
          object cxGridDBColumn12: TcxGridDBColumn
            DataBinding.FieldName = 'WeeksRemaining'
            Width = 87
          end
          object cxGridDBColumn13: TcxGridDBColumn
            Caption = 'Accrual to EOY'
            DataBinding.FieldName = 'Accrual_to_EOY'
            Width = 86
          end
          object cxGridDBColumn14: TcxGridDBColumn
            Caption = 'Est Bal at EOY'
            DataBinding.FieldName = 'Est_Bal_at_EOY'
            Width = 81
          end
          object cxGridDBColumn15: TcxGridDBColumn
            Caption = 'Supervisor Name'
            DataBinding.FieldName = 'Supervisor_Name'
            Width = 86
          end
          object cxGridDBColumn16: TcxGridDBColumn
            Caption = 'EMail Sup'
            DataBinding.FieldName = 'EMail_Sup'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object EmpIDProblems: TTabSheet
      Caption = 'Emp ID problems'
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 948
        Height = 243
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Insert.Enabled = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Enabled = False
          Navigator.Buttons.Delete.Enabled = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Enabled = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Enabled = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Enabled = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Enabled = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataModeController.SmartRefresh = True
          DataController.DataSource = dsTmpTableEmpID
          DataController.Filter.MaxValueListCount = 10
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.MRUItemsList = False
          Filtering.ColumnMRUItemsList = False
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsCustomize.ColumnFiltering = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object cxGridDBColumn17: TcxGridDBColumn
            Caption = 'Company Code'
            DataBinding.FieldName = 'Company_Code'
            Width = 86
          end
          object cxGridDBColumn18: TcxGridDBColumn
            Caption = 'Employee Nbr'
            DataBinding.FieldName = 'Employee_Number'
            Width = 84
          end
          object cxGridDBColumn20: TcxGridDBColumn
            Caption = 'First Name'
            DataBinding.FieldName = 'First_Name'
          end
          object cxGridDBColumn21: TcxGridDBColumn
            Caption = 'Last Name'
            DataBinding.FieldName = 'Last_Name'
          end
          object cxGridDBColumn22: TcxGridDBColumn
            Caption = 'Emp Status'
            DataBinding.FieldName = 'Employment_Status'
            Width = 65
          end
          object cxGridDBColumn23: TcxGridDBColumn
            Caption = 'SICK Option'
            DataBinding.FieldName = 'SICK_Option'
            Width = 69
          end
          object cxGridDBColumn24: TcxGridDBColumn
            Caption = 'SICK Balance'
            DataBinding.FieldName = 'SICK_Balance'
            Width = 75
          end
          object cxGridDBColumn25: TcxGridDBColumn
            Caption = 'PTO Option'
            DataBinding.FieldName = 'PTO_Option'
            Width = 71
          end
          object cxGridDBColumn26: TcxGridDBColumn
            Caption = 'PTO Balance'
            DataBinding.FieldName = 'PTO_Balance'
            Width = 69
          end
          object cxGridDBColumn27: TcxGridDBColumn
            Caption = 'PTO Accrual'
            DataBinding.FieldName = 'PTO_Accrual'
            Width = 73
          end
          object cxGridDBColumn28: TcxGridDBColumn
            DataBinding.FieldName = 'WeeksRemaining'
            Width = 90
          end
          object cxGridDBColumn29: TcxGridDBColumn
            Caption = 'Accrual to EOY'
            DataBinding.FieldName = 'Accrual_to_EOY'
            Width = 88
          end
          object cxGridDBColumn30: TcxGridDBColumn
            Caption = 'Est Bal at EOY'
            DataBinding.FieldName = 'Est_Bal_at_EOY'
            Width = 91
          end
          object cxGridDBColumn31: TcxGridDBColumn
            Caption = 'Supervisor Name'
            DataBinding.FieldName = 'Supervisor_Name'
            Width = 88
          end
          object cxGridDBColumn32: TcxGridDBColumn
            Caption = 'EMail Sup'
            DataBinding.FieldName = 'EMail_Sup'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 956
    Height = 35
    Align = alTop
    TabOrder = 2
    object btnImportFile: TButton
      Left = 48
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Import File'
      TabOrder = 0
      OnClick = btnImportFileClick
    end
    object EmpBalanceSearch: TCheckBox
      Left = 392
      Top = 10
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = EmpBalanceSearchClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 658
    Width = 956
    Height = 24
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Width = 200
      end
      item
        Text = ' Version'
        Width = 50
      end
      item
        Width = 100
      end
      item
        Text = 'Count'
        Width = 50
      end
      item
        Width = 50
      end
      item
        Alignment = taCenter
        Text = 'of'
        Width = 30
      end
      item
        Width = 50
      end>
  end
  object ImageList: TImageList
    Left = 816
    Top = 144
    Bitmap = {
      494C010101000400A00010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      000000000000000000000000000000000000000000004047940030357D003035
      7D0030357D0030357D0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E6100363B
      7F0000000000000000000000000000000000313790008287DC00888FF5006D75
      F3006169E0005159CB0030357D0030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A005D64
      C200757DF6005D64D2004D54BD00333CA600262C9F00A3A8FF00AEB3FF00949A
      FF007D85FF006972FE004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC00A1A7FF00989EFF007D85FF003842D100181FA2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004F57C90030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00646B
      D000A3A8FF009EA3F900757EFF003742CE001A21A200A1A7FF00ABB0FF008E95
      FF00757EFF00666FF900535BD6002F347C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F00565C
      B4007179EA006D75F3006770F1003842D100181FA2005D63B1004D53A8005158
      C4005158C4004C54C600474EB90030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E61005051
      7D002F3587004C53B1005158C40031379000272E8D0030358100000000000000
      000000000000000000002D32760030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E61000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF009FFF0000000000009FFF000000000000
      9FFF0000000000009FFF0000000000009FFF0000000000009F83000000000000
      8F00000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      803C0000000000009FFF00000000000000000000000000000000000000000000
      000000000000}
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 168
  end
  object FileADOConnection: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 63
    Top = 96
  end
  object PTOSickBalance: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select * from ['#39'UTQ & LOC$'#39']')
    Left = 347
    Top = 113
    object PTOSickBalanceCompanyCode: TWideStringField
      FieldName = 'Company Code'
      Size = 255
    end
    object PTOSickBalanceEmployeeNumber: TWideStringField
      FieldName = 'Employee Number'
      Size = 255
    end
    object PTOSickBalanceFirstName: TWideStringField
      FieldName = 'First Name'
      Size = 255
    end
    object PTOSickBalanceLastName: TWideStringField
      FieldName = 'Last Name'
      Size = 255
    end
    object PTOSickBalanceEmploymentStatus: TWideStringField
      FieldName = 'Employment Status'
      Size = 255
    end
    object PTOSickBalanceSICKOption: TWideStringField
      FieldName = 'SICK Option'
      Size = 255
    end
    object PTOSickBalanceSICKBalance: TFloatField
      FieldName = 'SICK Balance'
    end
    object PTOSickBalancePTOOption: TWideStringField
      FieldName = 'PTO Option'
      Size = 255
    end
    object PTOSickBalancePTOBalance: TFloatField
      FieldName = 'PTO Balance'
    end
    object PTOSickBalancePTOAccrual: TFloatField
      FieldName = 'PTO Accrual'
    end
    object PTOSickBalanceWeeksRemaining: TFloatField
      FieldName = 'WeeksRemaining'
    end
    object PTOSickBalanceAccrualtoEOY: TFloatField
      FieldName = 'Accrual to EOY'
    end
    object PTOSickBalanceEstBalatEOY: TFloatField
      FieldName = 'Est Bal at EOY'
    end
    object PTOSickBalanceSupervisorNameFirstMILastSuffix: TWideStringField
      FieldName = 'Supervisor Name (First MI Last Suffix)'
      Size = 255
    end
    object PTOSickBalanceEmailSupervisor: TWideStringField
      FieldName = 'Email (Supervisor)'
      Size = 255
    end
  end
  object TmpTableEmpID: TADODataSet
    Connection = ADOConn
    CommandText = 'select * from #Temp1'
    Parameters = <>
    Left = 712
    Top = 200
  end
  object OpenDialog1: TOpenDialog
    Left = 712
    Top = 138
  end
  object EmpBalance: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 'select * from employee_balance'
    Parameters = <>
    Left = 200
    Top = 112
  end
  object qryEmpid: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'empnumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select emp_id from employee where '
      'emp_number = :empnumber '
      'and ad_username is not null'
      'and ad_username <> '#39#39)
    Left = 480
    Top = 104
  end
  object TmpTableADName: TADODataSet
    Connection = ADOConn
    CommandText = 'Select * from #Temp2'
    Parameters = <>
    Left = 816
    Top = 200
  end
  object qryEmpIDProblems: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'empnumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select emp_id  from employee where emp_number = :empnumber')
    Left = 576
    Top = 104
  end
  object dsTmpTableEmpID: TDataSource
    DataSet = TmpTableEmpID
    Left = 704
    Top = 248
  end
  object dsTmpTableADName: TDataSource
    DataSet = TmpTableADName
    Left = 816
    Top = 248
  end
  object dsEmpBalance: TDataSource
    DataSet = EmpBalance
    Left = 200
    Top = 168
  end
  object insEmpBalToHistory: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'Insert into [employee_balance_history]'
      'SELECT *, GETDATE() as [Date_Copied] '
      '  FROM [dbo].[employee_balance]')
    Left = 336
    Top = 232
  end
  object truncateEmpBal: TADOCommand
    CommandText = 'Truncate table employee_balance'
    Connection = ADOConn
    Parameters = <>
    Left = 336
    Top = 184
  end
  object PopupMenuExport: TPopupMenu
    Left = 488
    Top = 272
    object mnuExport: TMenuItem
      Caption = 'Export To Excel'
      OnClick = mnuExportClick
    end
  end
end
