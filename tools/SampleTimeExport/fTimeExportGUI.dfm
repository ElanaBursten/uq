object SampleGuiForm: TSampleGuiForm
  Left = 404
  Top = 237
  BorderIcons = [biSystemMenu]
  Caption = 'Test Q Manager Time Export'
  ClientHeight = 585
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PC: TPageControl
    Left = 0
    Top = 37
    Width = 672
    Height = 529
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object ActivitiesTab: TTabSheet
      Caption = 'ActivitiesTab'
      ImageIndex = 3
      DesignSize = (
        664
        501)
      object Details2Label: TLabel
        Left = 6
        Top = 35
        Width = 49
        Height = 13
        Caption = 'Date/Time'
      end
      object Details1Label: TLabel
        Left = 6
        Top = 13
        Width = 44
        Height = 13
        Caption = 'Emp Num'
      end
      object Label1: TLabel
        Left = 6
        Top = 60
        Width = 63
        Height = 13
        Caption = 'Activity Type'
      end
      object AddActButton: TButton
        Left = 96
        Top = 84
        Width = 75
        Height = 25
        Caption = 'Add'
        TabOrder = 0
        OnClick = AddActButtonClick
      end
      object EmpNum: TEdit
        Left = 77
        Top = 5
        Width = 105
        Height = 21
        TabOrder = 1
        Text = '00777'
      end
      object ActivityList: TListView
        Left = 10
        Top = 115
        Width = 642
        Height = 373
        Anchors = [akLeft, akTop, akRight, akBottom]
        Columns = <
          item
            Caption = 'Emp Num'
            Width = 180
          end
          item
            Caption = 'Timestamp'
            Width = 145
          end
          item
            Caption = 'Activity'
            Width = 180
          end>
        TabOrder = 2
        ViewStyle = vsReport
      end
      object ActivityType: TComboBox
        Left = 96
        Top = 57
        Width = 169
        Height = 21
        AutoDropDown = True
        ItemHeight = 13
        TabOrder = 3
        Items.Strings = (
          'Call In'
          'Call Out'
          'Lunch In '
          'Lunch Out'
          'Personal In'
          'Personal Out'
          'Work In'
          'Work Out')
      end
      object ActivityDate: TcxDateEdit
        Left = 96
        Top = 32
        Properties.DateButtons = [btnClear, btnToday]
        Properties.SaveTime = False
        Properties.ShowTime = False
        TabOrder = 4
        Width = 105
      end
      object ActivityTime: TcxTimeEdit
        Left = 207
        Top = 32
        EditValue = 0d
        Properties.SpinButtons.Visible = False
        TabOrder = 5
        Width = 58
      end
    end
    object OutputSheet: TTabSheet
      Caption = 'Output Data'
      ImageIndex = 3
      DesignSize = (
        664
        501)
      object Label7: TLabel
        Left = 3
        Top = 3
        Width = 104
        Height = 13
        Caption = 'Output File Contents:'
      end
      object OutputDataMemo: TMemo
        Left = 3
        Top = 22
        Width = 658
        Height = 476
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Copy Test Files'
      ImageIndex = 2
      object Label4: TLabel
        Left = 5
        Top = 46
        Width = 68
        Height = 13
        Caption = 'Test Files Dir: '
      end
      object Label3: TLabel
        Left = 5
        Top = 80
        Width = 191
        Height = 13
        Caption = 'Seconds to pause after each line write: '
      end
      object CopyFilesFromEdit: TEdit
        Left = 79
        Top = 46
        Width = 460
        Height = 21
        TabOrder = 0
        Text = '%LocalAppData%\Q Manager\Time Import\TestFiles\'
      end
      object TestFilesButton: TButton
        Left = 539
        Top = 46
        Width = 17
        Height = 21
        Caption = '...'
        TabOrder = 1
        OnClick = TestFilesButtonClick
      end
      object SecondsEdit: TEdit
        Left = 201
        Top = 80
        Width = 40
        Height = 21
        TabOrder = 2
        Text = '2'
      end
      object Button1: TButton
        Left = 173
        Top = 256
        Width = 185
        Height = 25
        Caption = 'Copy then do Tempo processing'
        TabOrder = 3
        OnClick = Button1Click
      end
      object GroupBox1: TGroupBox
        Left = 23
        Top = 115
        Width = 516
        Height = 105
        Caption = 'Individual functions:'
        TabOrder = 4
        DesignSize = (
          516
          105)
        object Label5: TLabel
          Left = 382
          Top = 76
          Width = 40
          Height = 13
          Caption = 'Seconds'
        end
        object CopyToImportDirButton: TButton
          Left = 56
          Top = 41
          Width = 104
          Height = 25
          Anchors = [akTop]
          Caption = 'Copy To Import Dir'
          TabOrder = 0
          OnClick = CopyToImportDirButtonClick
        end
        object SimulateTempoButton: TButton
          Left = 288
          Top = 42
          Width = 185
          Height = 25
          Caption = 'Simulate Tempo Shutdown'
          TabOrder = 1
          OnClick = SimulateTempoButtonClick
        end
        object TempoSecondsEdit: TEdit
          Left = 344
          Top = 73
          Width = 33
          Height = 21
          TabOrder = 2
          Text = '300'
        end
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 484
        Width = 664
        Height = 17
        Align = alBottom
        Max = 300000
        Step = 1000
        TabOrder = 5
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Post Time Entry To QM'
      ImageIndex = 3
      object Label8: TLabel
        Left = 16
        Top = 24
        Width = 83
        Height = 13
        Caption = 'Time Import URL:'
      end
      object Label9: TLabel
        Left = 3
        Top = 177
        Width = 51
        Height = 13
        Caption = 'Response:'
      end
      object TimeImportFileLabel: TLabel
        Left = 16
        Top = 46
        Width = 76
        Height = 26
        Caption = 'Time Import File to POST:'
        WordWrap = True
      end
      object Label6: TLabel
        Left = 103
        Top = 101
        Width = 528
        Height = 13
        Caption = 
          '-OR- Select a folder to process multiple files from (if valid fo' +
          'lder is present then file selected above is ignored):'
      end
      object TimeImportURLEdit: TEdit
        Left = 103
        Top = 21
        Width = 524
        Height = 21
        TabOrder = 0
        Text = 'http://localhost:9236/TimeImport'
      end
      object PostTimeImportFileToQMButton: TButton
        Left = 280
        Top = 147
        Width = 75
        Height = 25
        Caption = 'Post XML'
        TabOrder = 1
        OnClick = PostTimeImportFileToQMButtonClick
      end
      object TimeImportFileText: TEdit
        Left = 103
        Top = 52
        Width = 524
        Height = 21
        TabOrder = 2
      end
      object SelectTimeImportFileDialog: TButton
        Left = 628
        Top = 52
        Width = 17
        Height = 21
        Caption = '...'
        TabOrder = 3
        OnClick = SelectTimeImportFileDialogClick
      end
      object TimeImportFileDirText: TEdit
        Left = 103
        Top = 120
        Width = 524
        Height = 21
        TabOrder = 4
      end
      object TimeImportFilesDirButton: TButton
        Left = 628
        Top = 120
        Width = 17
        Height = 21
        Caption = '...'
        TabOrder = 5
        OnClick = TimeImportFilesDirButtonClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 195
        Width = 664
        Height = 306
        Align = alBottom
        TabOrder = 6
        object TimeImportResponseMemo: TMemo
          Left = 1
          Top = 1
          Width = 662
          Height = 304
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object RefreshStatusButton: TButton
        Left = 530
        Top = 147
        Width = 97
        Height = 25
        Caption = 'Refresh Status'
        TabOrder = 7
        OnClick = RefreshStatusButtonClick
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 566
    Width = 672
    Height = 19
    Panels = <
      item
        Width = 500
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      672
      37)
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 67
      Height = 13
      Caption = 'Output Folder'
    end
    object OutputFolder: TLabel
      Left = 81
      Top = 8
      Width = 4
      Height = 13
      Caption = '-'
      OnClick = OutputFolderClick
    end
    object SaveButton: TButton
      Left = 547
      Top = 6
      Width = 104
      Height = 25
      Anchors = [akTop]
      Caption = 'Save and Close'
      TabOrder = 0
      OnClick = SaveButtonClick
    end
  end
  object TempoTimer: TTimer
    Enabled = False
    OnTimer = TempoTimerTimer
    Left = 504
    Top = 216
  end
  object OpenTimeImportFileDialog: TOpenDialog
    DefaultExt = '.xml'
    InitialDir = '.'
    Left = 640
    Top = 136
  end
end
