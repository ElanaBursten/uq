program SampleTimeExport;

{$R '..\..\QMIcon.res'}
{$R '..\..\QMVersion.res' '..\..\QMVersion.rc'}

uses
  Forms,
  fTimeExportGUI in 'fTimeExportGUI.pas' {SampleGuiForm},
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  MSXML2_TLB in '..\..\common\MSXML2_TLB.pas',
  OdMSXMLUtils in '..\..\common\OdMSXMLUtils.pas',
  OdIsoDates in '..\..\common\OdIsoDates.pas',
  OdVclUtils in '..\..\common\OdVclUtils.pas',
  OdUtcDates in '..\..\common\OdUtcDates.pas',
  WbemScripting_TLB in '..\..\common\WbemScripting_TLB.pas',
  WinHttp_TLB in '..\..\common\WinHttp_TLB.pas';

var
  SampleGuiForm: TSampleGuiForm;
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Sample Q Manager Time Export';
  Application.CreateForm(TSampleGuiForm, SampleGuiForm);
  Application.Run;
end.
