﻿unit fTimeExportGUI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxSpinEdit, cxTimeEdit, ShlObj, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, dxCore, cxDateUtils;
{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}
type
  TSampleGuiForm = class(TForm)
    PC: TPageControl;
    OutputSheet: TTabSheet;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    SaveButton: TButton;
    OutputDataMemo: TMemo;
    Label7: TLabel;
    ActivitiesTab: TTabSheet;
    AddActButton: TButton;
    EmpNum: TEdit;
    Details2Label: TLabel;
    ActivityList: TListView;
    Details1Label: TLabel;
    Label1: TLabel;
    ActivityType: TComboBox;
    ActivityDate: TcxDateEdit;
    ActivityTime: TcxTimeEdit;
    Label2: TLabel;
    OutputFolder: TLabel;
    TabSheet1: TTabSheet;
    Label4: TLabel;
    CopyFilesFromEdit: TEdit;
    TestFilesButton: TButton;
    Label3: TLabel;
    SecondsEdit: TEdit;
    TempoTimer: TTimer;
    Button1: TButton;
    GroupBox1: TGroupBox;
    CopyToImportDirButton: TButton;
    SimulateTempoButton: TButton;
    ProgressBar1: TProgressBar;
    TempoSecondsEdit: TEdit;
    Label5: TLabel;
    TabSheet2: TTabSheet;
    TimeImportURLEdit: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    PostTimeImportFileToQMButton: TButton;
    OpenTimeImportFileDialog: TOpenDialog;
    TimeImportFileLabel: TLabel;
    TimeImportFileText: TEdit;
    SelectTimeImportFileDialog: TButton;
    TimeImportFileDirText: TEdit;
    Label6: TLabel;
    TimeImportFilesDirButton: TButton;
    Panel2: TPanel;
    TimeImportResponseMemo: TMemo;
    RefreshStatusButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure AddActButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure OutputFolderClick(Sender: TObject);
    procedure CopyToImportDirButtonClick(Sender: TObject);
    procedure CopyTestFiles(WorkDate: TDateTime; CopyFromFolder, SaveToFolder: string);
    procedure TestFilesButtonClick(Sender: TObject);
    procedure SelectADirectory(ATextBox: TEdit);
    procedure SimulateTempoButtonClick(Sender: TObject);
    procedure TempoTimerTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PostTimeImportFileToQMButtonClick(Sender: TObject);
    procedure PostTimeImportFile(Filename: string);
    procedure SelectTimeImportFileDialogClick(Sender: TObject);
    procedure TimeImportFilesDirButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefreshStatusButtonClick(Sender: TObject);
  private
    fStatusUrls: TStringList;
    procedure GenerateOutputXml;
    function FormattedActivityTime: string;
    function ActivityAsXML(const Idx: Integer): string;
    function ActivityXMLFileName(const ActivityDateString: string): string;
    procedure ShutdownWindows;
    procedure DoHibernate;
  end;

{
  The SetSuspendState function suspends the system by shutting power down.
  Depending on the Hibernate parameter,
  the system either enters a suspend (sleep) state or hibernation.

   Parameters:
   Hibernate: If this parameter is TRUE, the system hibernates.
              If the parameter is FALSE, the system is suspended.
   ForceCritical: If this parameter is TRUE, the system suspends operation immediately;
                  if it is FALSE, the system broadcasts a PBT_APMQUERYSUSPEND event to
                  each application to request permission to suspend operation.
   DisableWakeEvent: If this parameter is TRUE, the system disables all wake events.
                     If the parameter is FALSE, any system wake events remain enabled.

  Windows NT/2000/XP: Included in Windows 2000 and later.
  Windows 95/98/Me: Included in Windows 98 and later.
}
function SetSuspendState(Hibernate, ForceCritical, DisableWakeEvent: Boolean): Boolean;

var
  _SetSuspendState: function (Hibernate, ForceCritical, DisableWakeEvent: BOOL): BOOL stdcall = nil;
  function LinkAPI(const module, functionname: string): Pointer; forward;

implementation

uses
  OdMiscUtils, MSXML2_TLB, OdMSXMLUtils, OdIsoDates, OdVclUtils, JCLFileUtils,
  FileCtrl, WbemScripting_TLB, ActiveX, WinHttp_TLB;

{$R *.dfm}

{ TSampleGuiForm }

procedure TSampleGuiForm.FormShow(Sender: TObject);
var
  s: array[0..MAX_PATH] of char;
begin
  PC.ActivePageIndex := 0;
  ActivityDate.Date := Date;
  ActivityTime.Time := Time;
  ExpandEnvironmentStrings(PChar(IncludeTrailingPathDelimiter(ParamStr(1))), @s, MAX_PATH);
  if ParamCount > 0 then
    OutputFolder.Caption := s
  else
    OutputFolder.Caption := ExtractFilePath(Application.ExeName);

  TimeImportFileDirText.Text := OutputFolder.Caption;
  StatusBar1.Panels[1].Text := 'Version: ' + AppVersionShort;
end;

procedure TSampleGuiForm.GenerateOutputXml;
var
  i: Integer;
begin
  // For each activity added:
  OutputDataMemo.Clear;
  for i := 0 to ActivityList.Items.Count - 1 do begin
    OutputDataMemo.Lines.Add('--- ' +
      ActivityXMLFileName(ActivityList.Items[i].SubItems[0]) + ' ---');
    OutputDataMemo.Lines.Add(ActivityAsXML(i));
  end;
end;

procedure TSampleGuiForm.OutputFolderClick(Sender: TObject);
begin
  OdShellExecute(ExtractFilePath((Sender as TLabel).Caption));
end;

function TSampleGuiForm.ActivityAsXML(const Idx: Integer): string;
var
  XML: IXMLDOMDocument;
  QMRoot: IXMLDOMElement;
  ActivityElem: IXMLDOMElement;

  procedure AddChildElement(Root: IXMLDomElement; const Name, Value: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
  begin
    Element := XML.createElement(Name);
    TextNode := XML.createTextNode(Value);
    Element.appendChild(TextNode);
    Root.appendChild(Element);
  end;

begin
  XML := CoDOMDocument.Create;
  XML.async := False;
  QMRoot := XML.createElement('QManager');
  QMRoot.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  QMRoot.setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
  XML.appendChild(QMRoot);
  ActivityElem := XML.createElement('ClockEntry');
  ActivityElem.setAttribute('EmployeeNumber', ActivityList.Items[Idx].Caption);
  ActivityElem.setAttribute('Time', ActivityList.Items[Idx].SubItems[0]);
  ActivityElem.setAttribute('ClockType', ActivityList.Items[Idx].SubItems[1]);
  ActivityElem.setAttribute('ClockAttribute', 'Normal'); // QM doesn't use this, so it's value can be anything
  QMRoot.appendChild(ActivityElem);
  Result := PrettyPrintXml(XML);
end;

procedure TSampleGuiForm.SaveButtonClick(Sender: TObject);
var
  FileName: string;
  i: Integer;
  SaveToFolder: string;
begin
  SaveToFolder := IncludeTrailingPathDelimiter(OutputFolder.Caption);
  // Write output files:
  for i := 0 to ActivityList.Items.Count - 1 do begin
    FileName := SaveToFolder + ActivityXMLFileName(ActivityList.Items[i].SubItems[0]);
    SaveDebugFile(FileName, ActivityAsXML(i));
    SHChangeNotify(SHCNE_CREATE, SHCNF_PATH, PChar(FileName), nil);
  end;

  Close;
end;

procedure TSampleGuiForm.SimulateTempoButtonClick(Sender: TObject);
{Tempo does write directly to the Import directory.

The sequence of events between QM and Tempo are:
1) After Tempo processes it's last Work Out entry for the day - Tempo sets itself to be the topmost screen.
2) Tempo waits 5 mins so that QM can do the import, syncing, attachment upload.
3) Tempo then either calls Hibernate or Shutdown. If the day is Mon-Thu then Hibernate is called. If the day is Friday then Shutdown is called. Stand By is currently not used.}
begin
  if StrToIntDef(TempoSecondsEdit.Text,0) > 0 then
    ProgressBar1.Max := StrToInt(TempoSecondsEdit.Text) * 1000
  else//default to actual Tempo behavior - 5 mins
    ProgressBar1.Max := 300000;

//make test utility topmost form:
  with Self do
    SetWindowPos(Handle, // handle to window
                 HWND_TOPMOST, // placement-order handle {*}
                 Left,  // horizontal position
                 Top,   // vertical position
                 Width,
                 Height,
                 // window-positioning options
                 SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);

  TempoTimer.Enabled := True;//wait 5 minutes to give QM time to do it's work
  StatusBar1.Panels[0].Text :='Tempo Timer running.';
end;

procedure TSampleGuiForm.DoHibernate;
begin
  StatusBar1.Panels[0].Text :='DoHibernate called. ';

  SetSuspendState(True, False, False);
end;

function SetSuspendState(Hibernate, ForceCritical,
  DisableWakeEvent: Boolean): Boolean;
begin
  if not Assigned(_SetSuspendState) then
    @_SetSuspendState := LinkAPI('POWRPROF.dll', 'SetSuspendState');
  if Assigned(_SetSuspendState) then
    Result := _SetSuspendState(Hibernate, ForceCritical,
      DisableWakeEvent)
  else
    Result := False;
end;

function LinkAPI(const module, functionname: string): Pointer;
var
  hLib: HMODULE;
begin
  hLib := GetModulehandle(PChar(module));
  if hLib = 0 then
    hLib := LoadLibrary(PChar(module));
  if hLib <> 0 then
    Result := getProcAddress(hLib, PChar(functionname))
  else
    Result := nil;
end;

procedure TSampleGuiForm.ShutdownWindows;
var
  wmiLocator:   ISWbemLocator;
  wmiServices:  ISWbemServices;
  wmiObjectSet: ISWbemObjectSet;
  wmiObject:    ISWbemObject;
  Enum:         IEnumVariant;
  ovVar:        OleVariant;
  lwValue:      LongWord;
begin
  StatusBar1.Panels[0].Text :='ShutdownWindows called.';

  wmiLocator  := CoSWbemLocator.Create;
  WMIServices := WMILocator.ConnectServer('.', 'root\cimv2','', '', '', '', 0, nil);

  wmiServices.Security_.Privileges.Add(wbemPrivilegeShutdown, True);
  wmiObjectSet := wmiServices.ExecQuery('SELECT * FROM Win32_OperatingSystem WHERE Primary=True', 'WQL', wbemFlagReturnImmediately, nil);
  Enum :=  (wmiObjectSet._NewEnum) as IEnumVariant;
  while (Enum.Next(1, ovVar, lwValue) = S_OK) do  begin
    wmiObject := IUnknown(ovVar) as SWBemObject;
    wmiObject.ExecMethod_('Shutdown', nil, 0, nil);
  end;

end;

procedure TSampleGuiForm.TempoTimerTimer(Sender: TObject);
begin
  ProgressBar1.StepIt;
  if ProgressBar1.Position = ProgressBar1.Max then begin
    StatusBar1.Panels[0].Text :='Tempo Timer stopped.';
    TempoTimer.Enabled := False;

    case DayOfWeek(Date) of
     2,3,4,5: DoHibernate; //Mon-Thur
     6: ShutdownWindows; //Fri
     //else do nothing on the weekend
    end;
  end;
end;

procedure TSampleGuiForm.TestFilesButtonClick(Sender: TObject);
begin
  SelectADirectory(CopyFilesFromEdit);
end;

procedure TSampleGuiForm.TimeImportFilesDirButtonClick(Sender: TObject);
begin
  SelectADirectory(TimeImportFileDirText);
end;

procedure TSampleGuiForm.SelectADirectory(ATextBox: TEdit);
const
  SELDIRHELP = 1000;
var
  Directory: String;
begin
  Directory := 'C:';
  if SelectDirectory(Directory, [sdAllowCreate,sdPerformCreate, sdPrompt], SELDIRHELP) then
    ATextBox.Text := Directory;
end;

procedure TSampleGuiForm.CopyToImportDirButtonClick(Sender: TObject);
var
  s: array[0..MAX_PATH] of char;
  SaveToFolder: string;
  CopyFromFolder: string;
begin
  Screen.Cursor := crHourGlass;
  SaveToFolder := IncludeTrailingPathDelimiter(OutputFolder.Caption);
  ExpandEnvironmentStrings(PChar(IncludeTrailingPathDelimiter(CopyFilesFromEdit.text)), @s, MAX_PATH);
  CopyFromFolder := string(PChar(@s));
  Assert(SysUtils.DirectoryExists(CopyFromFolder), 'The copy from folder must be a valid directory.');
  Assert(CopyFromFolder <> SaveToFolder, 'The copy from folder cannot be the same as the save to folder.');

  CopyTestFiles(Date-1, CopyFromFolder, SaveToFolder);//previous day, used to test situations where user is granted/not granted permission
  CopyTestFiles(Date, CopyFromFolder, SaveToFolder);

  StatusBar1.Panels[0].Text := 'No files processing.';
  Screen.Cursor := crDefault;
end;

procedure TSampleGuiForm.CopyTestFiles(WorkDate: TDateTime; CopyFromFolder, SaveToFolder: string);
var
  i: Integer;
  FileList: TStringList;
  FilePattern: string;
  FRead: TextFile;
  FWrite: TextFile;
  ReadFileName: String;
  WriteFileName: String;
  S: String;
begin
  FileList := TStringList.Create;
  try
    FilePattern := CopyFromFolder + FormatDateTime('yyyy-mm-dd_', WorkDate) + '*.xml';
    AdvBuildFileList(FilePattern, faArchive, FileList, amSubSetOf);
    StatusBar1.Panels[0].Text :=Format('Processing %d files for WorkDate: %s', [FileList.Count, DateTimeToStr(WorkDate)]);

    // Assumes the files are named so they are processed in chronological order
    for i := 0 to FileList.Count-1 do begin
      ReadFileName := CopyFromFolder + FileList[i];
      WriteFileName := SaveToFolder + FileList[i];

      AssignFile(FRead, ReadFileName);
      Reset(FRead);
      AssignFile(FWrite, WriteFileName);
      Rewrite(FWrite);

      while not EOF(FRead) do begin
        ReadLn(FRead, S);
        WriteLn(FWrite, S);
        Sleep(StrToInt(SecondsEdit.Text) * 1000);
      end;

      CloseFile(FWrite);
      CloseFile(FRead);
      SHChangeNotify(SHCNE_CREATE, SHCNF_PATH, PChar(WriteFileName), nil);
      StatusBar1.Panels[0].Text := Format('%d files remaining for WorkDate: %s', [(FileList.Count - i), DateTimeToStr(WorkDate)]);
    end;
  finally
    FreeAndNil(FileList);
  end;
end;


function TSampleGuiForm.ActivityXMLFileName(const ActivityDateString: string): string;
var
  ActivityDate: TDateTime;
begin
  ActivityDate := IsoStrToDateTime(Copy(ActivityDateString, 1, 23));
  Result := FormatDateTime('yyyy-mm-dd_hh-nn-ss', ActivityDate) + '.xml';
end;

function TSampleGuiForm.FormattedActivityTime: string;
begin
  Result := IsoDateTimeToStr(ActivityDate.Date + ActivityTime.Time) + '-04:00';
end;

procedure TSampleGuiForm.FormCreate(Sender: TObject);
begin
  fStatusUrls := TStringList.Create;
end;

procedure TSampleGuiForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(fStatusUrls);
end;

procedure TSampleGuiForm.AddActButtonClick(Sender: TObject);
var
  Activity: TListItem;
begin
  Activity := ActivityList.Items.Add;
  Activity.Caption := EmpNum.Text;
  Activity.SubItems.Add(FormattedActivityTime);
  Activity.SubItems.Add(ActivityType.Text);
  GenerateOutputXml;
end;

procedure TSampleGuiForm.Button1Click(Sender: TObject);
begin
  CopyToImportDirButton.Click;
  SimulateTempoButton.Click;
end;

procedure TSampleGuiForm.PostTimeImportFileToQMButtonClick(Sender: TObject);
var
  i: integer;
  FileList: TStringList;
  FilePattern: string;
  TimeImportFileName: String;
begin
  fStatusUrls.Clear;
  TimeImportResponseMemo.Lines.Clear;

  if (TimeImportFileDirText.Text <> '') and SysUtils.DirectoryExists(TimeImportFileDirText.Text) then begin
    FileList := TStringList.Create;
    try
      FilePattern := IncludeTrailingPathDelimiter(TimeImportFileDirText.Text) + '*.xml'; //process all xml files regardless of date for testing purposes
      AdvBuildFileList(FilePattern, faArchive, FileList, amSubSetOf);
      for i := 0 to FileList.Count-1 do begin
        TimeImportFileName := IncludeTrailingPathDelimiter(TimeImportFileDirText.Text) + FileList[i];
        PostTimeImportFile(TimeImportFileName);
      end;
      if FileList.Count = 0 then
        TimeImportResponseMemo.Lines.Add('No XML files in directory to process.');
    finally
      FreeAndNil(FileList);
    end;
  end
  else
    PostTimeImportFile(TimeImportFileText.Text);
end;

procedure TSampleGuiForm.RefreshStatusButtonClick(Sender: TObject);
var
  i: Integer;
  FileName: string;
  StatusRequest: IWinHttpRequest;
begin
  if fStatusUrls.Count > 0 then begin
    for i := 0 to fStatusUrls.Count - 1 do begin
      FileName := Copy(fStatusUrls[i], LastDelimiter('?', fStatusUrls[i]) + 1, Length(fStatusUrls[i]) - 1) + '.xml';
      StatusRequest := CoWinHttpRequest.Create;
      StatusRequest.Open('GET', fStatusUrls[i], False);
      StatusRequest.Send('');
      TimeImportResponseMemo.Lines.Add(FileName + ': ' + StatusRequest.ResponseText);
    end;
    TimeImportResponseMemo.Lines.Add('_________________________________________');
  end else
    ShowMessage('No statuses to refresh.');
end;

procedure TSampleGuiForm.PostTimeImportFile(Filename: string);
var
  TimeImportRequest, StatusRequest: IWinHttpRequest;
begin
  TimeImportResponseMemo.Lines.Add('Posting file: ' + Filename);
  try
    TimeImportRequest := CoWinHttpRequest.Create;
    TimeImportRequest.Open('POST', TimeImportURLEdit.Text, False);
    TimeImportRequest.SetRequestHeader('Content-Type', 'text/xml');
    TimeImportRequest.Send(FileToOleVariant(Filename));
    StatusRequest := CoWinHttpRequest.Create;
    StatusRequest.Open('GET', TimeImportRequest.ResponseText, False);
    StatusRequest.Send('');
    fStatusUrls.Add(TimeImportRequest.ResponseText);

    TimeImportResponseMemo.Lines.Add(IntToStr(TimeImportRequest.Status) + ': ' + TimeImportRequest.StatusText);
    TimeImportResponseMemo.Lines.Add(StatusRequest.ResponseText);
    TimeImportResponseMemo.Lines.Add('_________________________________________');
  except on e:exception do
    ShowMessage(e.Message);
  end;
end;

procedure TSampleGuiForm.SelectTimeImportFileDialogClick(Sender: TObject);
begin
  if OpenTimeImportFileDialog.Execute then
    TimeImportFileText.Text := OpenTimeImportFileDialog.FileName;
end;


end.
