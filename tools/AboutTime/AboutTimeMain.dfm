object frmAboutTime: TfrmAboutTime
  Left = 0
  Top = 0
  Caption = '                       -- It'#39's About Time --'
  ClientHeight = 442
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object Splitter1: TSplitter
    Left = 0
    Top = 371
    Width = 628
    Height = 11
    Cursor = crVSplit
    Align = alTop
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 628
    Height = 25
    Align = alTop
    TabOrder = 0
  end
  object pnlMiddle: TPanel
    Left = 0
    Top = 25
    Width = 628
    Height = 346
    Align = alTop
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 1
      Top = 171
      Width = 626
      Height = 8
      Cursor = crVSplit
      Align = alTop
    end
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 626
      Height = 170
      Align = alTop
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object JsonMemo: TMemo
      Left = 1
      Top = 179
      Width = 626
      Height = 166
      Align = alClient
      Lines.Strings = (
        '')
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 382
    Width = 628
    Height = 60
    Align = alClient
    TabOrder = 2
    object cbRunOnce: TCheckBox
      Left = 347
      Top = 9
      Width = 97
      Height = 17
      Caption = 'Run Once'
      TabOrder = 0
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 40
      Width = 626
      Height = 19
      Panels = <
        item
          Text = 'Connected to'
          Width = 80
        end
        item
          Text = 'Not Connected'
          Width = 130
        end
        item
          Text = 'Success'
          Width = 50
        end
        item
          Width = 30
        end
        item
          Text = 'Failed'
          Width = 35
        end
        item
          Width = 30
        end
        item
          Text = 'Total'
          Width = 38
        end
        item
          Width = 30
        end
        item
          Text = 'ver'
          Width = 23
        end
        item
          Width = 50
        end>
    end
    object btnParsejson: TButton
      Left = 128
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Parse JSON'
      TabOrder = 2
      OnClick = btnParsejsonClick
    end
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=uqweb;I' +
      'nitial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial File Nam' +
      'e="";Server SPN="";'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 40
    Top = 40
  end
  object qryRabbitMQ: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'QueueName'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 255
        Value = 'utq-rtasq-events-all'
      end>
    Prepared = True
    SQL.Strings = (
      'select * from [dbo].[rabbitmq_in]'
      'where [processed]=0'
      'and status = '#39'NEW'#39
      'and queue_name= :QueueName'
      '')
    Left = 40
    Top = 120
  end
  object updRabbitMQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'processed'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'MsgID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [dbo].[rabbitmq_in]'
      '   SET [processed] = :processed'
      '      ,[status] = :status'
      ' WHERE message_id = :MsgID')
    Left = 472
    Top = 56
  end
  object delRabbitMQ: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'MsgID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'delete '
      'from [dbo].[rabbitmq_in]'
      ' WHERE message_id = :MsgID')
    Left = 384
    Top = 56
  end
  object qryStaging: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'select * from rabbitmq_staging_rtime')
    Left = 472
    Top = 120
  end
  object insStagingTable: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 384
    Top = 120
  end
  object qryEmpID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'empnumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select emp_id from employee where '
      'emp_number = :empnumber and active=1 '
      'and NULLIF(LTRIM(RTRIM(ad_username)), '#39#39') IS NOT NULL'
      ''
      ''
      '')
    Left = 304
    Top = 56
  end
  object spProcessRtasqTimeEntries: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'process_rtasq_timeentries'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Prepared = True
    Left = 544
    Top = 49
  end
end
