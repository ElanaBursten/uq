unit AboutTimeMain;
//QM-760
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.JSON, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB, Data.Win.ADODB;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults
type
  TfrmAboutTime = class(TForm)
    pnlTop: TPanel;
    pnlMiddle: TPanel;
    pnlBottom: TPanel;
    Memo1: TMemo;
    Splitter1: TSplitter;
    ADOConn: TADOConnection;
    qryRabbitMQ: TADOQuery;
    updRabbitMQ: TADOQuery;
    delRabbitMQ: TADOQuery;
    cbRunOnce: TCheckBox;
    StatusBar1: TStatusBar;
    btnParsejson: TButton;
    JsonMemo: TMemo;
    Splitter2: TSplitter;
    qryStaging: TADOQuery;
    insStagingTable: TADOQuery;
    qryEmpID: TADOQuery;
    spProcessRtasqTimeEntries: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure btnParsejsonClick(Sender: TObject);
  private
    { Private declarations }

      aDatabase: String;
      aServer: String;
      ausername: string;
      apassword: string;

      UTCOffset: String;
      EmpID: Integer;
      WorkerNumber: String;

      LogResult: TLogResults;
      flogDir: wideString;
      QueueName:string;
      function ProcessINI: boolean;
      function connectDB: boolean;
      procedure clearLogRecord;
      procedure getData(OrigObj: TJsonObject; Field: String);

      procedure ProcessPairs;
      procedure DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
      function GetAppVersionStr: string;
      function BuildStagingQuery(UTCOffset: String; WorkerNumber: String): Boolean;
      function GetEmpID(WorkerNumher:String; out EmpID: Integer): Boolean;
  public
    { Public declarations }
    cntGood, cntBad :integer;
    Strlist: TStringList;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmAboutTime: TfrmAboutTime;

implementation
uses dateutils, StrUtils, iniFiles;
{$R *.dfm}

{ TForm2 }


procedure TfrmAboutTime.btnParsejsonClick(Sender: TObject);

begin
  ProcessPairs;
end;


//BP  Process mqrabbit_in table
procedure TfrmAboutTime.ProcessPairs;
var
  Json_Data: String;
  JsonValue  : TJsonValue;
  OriginalObj : TJsonObject;
  QueueName: String;
  StageFields, I: Integer;
  Field: String;
  MessageID: Integer;

begin
  cntGood := 0;
  cntBad  := 0;
  try
    try
      StrList := TStringList.Create;
      StrList.Duplicates := dupIgnore;
      qryStaging.Open;
      JsonMemo.Lines.Clear;
      //retrieve queuename
      QueueName := ParamStr(1);
      qryRabbitmq.Parameters.ParamByName('QueueName').Value := QueueName;
      qryRabbitmq.Open;

      while not qryRabbitmq.Eof do
      begin
        MessageID :=  qryRabbitMQ.FieldbyName('message_id').AsInteger;
        Json_data := qryRabbitMQ.FieldbyName('message_payload').AsString;
        JsonValue := TJsonObject.ParseJSONValue(Json_data);
        OriginalObj := JsonValue as TJsonObject;
        //iterate thrugh Json fields, Process Json strings and objects
        for I := 0 to OriginalObj.Count - 1 do
        begin
          Field :=  OriginalObj.Pairs[I].JsonString.Value;
          GetData(OriginalObj, Field);
        end;
        JsonMemo.Lines.Add('---------------------------------------------');

        //build staging table insert query
        If BuildStagingQuery(UTCOffset, WorkerNumber) then
          DeleteFromMQ(MessageID, 'PROCESSED',True);

        StrList.Clear;
        JsonValue.Free;
        inc(cntGood);
        StatusBar1.Panels[3].Text:= IntToStr(cntGood);
        StatusBar1.Panels[5].Text:= IntToStr(cntBad);
        StatusBar1.Panels[7].Text:= IntToStr(cntBad+cntGood);
        if cbRunOnce.Checked then break;

        qryRabbitmq.Next;
      end;


    WriteLog(LogResult);
    memo1.Lines.Add('Process finished');
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'About Time ';
    LogResult.Status := 'Process finished';
    LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
    memo1.Lines.Add(LogResult.DataStream);
    WriteLog(LogResult);
  try
    spProcessRtasqTimeEntries.ExecProc;
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'spProcessRtasqTimeEntries';
    LogResult.Status := 'Process Rtasq Time Entries Sucessfully run';
    memo1.Lines.Add(LogResult.Status);
    WriteLog(LogResult);

  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'spProcessRtasqTimeEntries';
      LogResult.ExcepMsg := E.Message;
      LogResult.Status := 'Process Rtasq Time Entries Failed';
      WriteLog(LogResult);

      memo1.Lines.Add('Process Rtasq Time Entries Failed');
    end;

  end;

    except
      on E: Exception do
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'processPairs';
        LogResult.DataStream := JSonValue.ToString;
        LogResult.ExcepMsg := E.Message;
        LogResult.Status := 'Failed';
        WriteLog(LogResult);
      end;
    end;
  finally
    qryRabbitMQ.Close;
    qryStaging.Close;
    StrList.Free;
  end;
end;
//BP  Build the insert sql from stringlist key value pair (includes Source and EmpID as added fields)
function TfrmAboutTime.BuildStagingQuery(UTCOffset, WorkerNumber: String): Boolean;
var
 I: Integer;
 AFields, AValues: String;
 InsertSQL: String;
begin
  try
    Result := True;
    InsertSQL := 'Insert into rabbitmq_staging_rtime ';
    AFields := AFields + 'source,';
    AValues := AValues + QuotedStr('RTasq') +',';
    for I := 0 to StrList.Count-1 do
    begin
      AFields := AFields + StrList.KeyNames[I] + ',';

      if StrList.KeyNames[I] =  'EventTime' then
        AValues := AValues + 'DateAdd(hour, ' + UTCOffset + ', CAST(' + QuotedStr(StrList.ValueFromIndex[I]) + ' as Datetime2)),'
      else
        AValues := AValues + QuotedStr(StrList.ValueFromIndex[I]) + ',';

     //Validate and retrieve Emp ID, if missing/invalid, logs the error and skips to next record.
      if StrList.KeyNames[I] =  'WorkerNumber' then
      begin
        If not GetEmpID(StrList.ValueFromIndex[I], EmpID) then
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'Build staging query';
          LogResult.DataStream := 'emp ID for worker number ' + StrList.ValueFromIndex[I] + ' invalid or not found ';
          WriteLog(LogResult);
          Memo1.Lines.Add('error inserting staging table record');
          Result := False;
        end else
        begin
          AFields := AFields + 'EmpID,';
          AValues := AValues + QuotedStr(InttoStr(EmpID)) + ',';
        end;
      end;
    end;
    Delete(AFields, Length(AFields), 1);
    Delete(AValues, Length(AValues), 1);

    if Result then
    begin
      InsertSQL := InsertSQL + '(' + AFields + ') values (' + AValues  + ')';
      insStagingTable.SQL.Text := InsertSQL;
      insStagingTable.ExecSQL;
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Build staging query';
      LogResult.ExcepMsg := E.Message;
      LogResult.DataStream := insStagingTable.SQL.Text;
      WriteLog(LogResult);
      Result := False;
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('error inserting staging table records');
    end;
  end;
end;

//BP Emp ID retrieved from Json field WorkerNumber
function TfrmAboutTime.GetEmpID(WorkerNumher:String; out EmpID:Integer): Boolean;
begin
  Result := True;
  qryEmpID.Parameters.ParamByName('EmpNumber').Value := WorkerNumber;
  qryEmpID.Open;
  if not qryEmpID.EOF then
    EmpID := qryEmpID.FieldByName('emp_id').AsInteger
  else Result := False;
  qryEmpID.Close;
end;

//BP  Process strings, objects(with any nested subobject)
procedure TfrmAboutTime.getData(OrigObj: TJsonObject; Field: String);
var
  jso: TJsonObject;
  jsPair : TJsonPair;
  jsNestedPair: TJsonPair;
  jsoNestedObj: TJsonObject;
  OutputStr: String;
  ObjTitle: String;
  NestedObjTitle: String;
  JsOutputStr : String;
  I:Integer;

//BP  Reads into stringlist key value pair for building staging table sql
procedure GetStagingData(Field, Value: String);
begin
  if qryStaging.FindField(Field) <> nil then
  begin
    JsOutputStr := Field + '=' + Value;
    StrList.Add(JsOutputStr);
  end;
end;

begin
  jspair := OrigObj.Get(Field);
  if JsPair.JsonValue is TJsonString then
  begin
    OutputStr := '   ' + jsPair.JsonString.Value + ': ' + jsPair.JsonValue.Value;
    JsonMemo.Lines.Add(OutputStr);
    if jsPair.JsonString.Value <> 'event_type' then
      GetStagingData(jsPair.JsonString.Value, jsPair.JsonValue.Value);
  end
  else if jsPair.jsonValue is TJsonObject then
  begin
    ObjTitle := '   ' + jsPair.JsonString.Value + ':';
    JsonMemo.Lines.Add(ObjTitle);
    jso := jsPair.jsonValue as TJsonObject;
    for jsPair in jso do
    begin
      if jsPair.jsonValue is TJsonString then
      begin
        OutputStr := '      ' + jsPair.JsonString.Value + ': ' + jsPair.JsonValue.Value;
        JsonMemo.Lines.Add(OutputStr);
        //Event Time Offset is saved and added to the Eventtime field in SQL
        if jsPair.JsonString.Value = 'EventTimeUTCOffset' then
           UTCOffset := jsPair.JsonValue.Value;
         if jsPair.JsonString.Value = 'WorkerNumber' then
           WorkerNumber := jsPair.JsonValue.Value;
        GetStagingData(jsPair.JsonString.Value, jsPair.JsonValue.Value);
      end else
      if jsPair.jsonValue is TJsonObject then
      begin
        NestedObjTitle := '      ' + jsPair.JsonString.Value + ':';
        JsonMemo.Lines.Add(NestedObjTitle);
        jsoNestedObj := jsPair.jsonValue as TJsonObject;
        for jsNestedPair in jsoNestedObj do
        begin
          OutputStr := '           ' + jsNestedPair.JsonString.Value + ': ' + jsNestedPair.JsonValue.Value;
          JsonMemo.Lines.Add(OutputStr);
          GetStagingData(jsNestedPair.JsonString.Value, jsNestedPair.JsonValue.Value);
        end;
      end;
    end;
  end;
end;

procedure TfrmAboutTime.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmAboutTime.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
    ADOConn.open;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aServer;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

// update the rabbitmq record and delete. delete trigger inserts record into the history table
procedure TfrmAboutTime.DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
begin
  updRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  updRabbitMQ.Parameters.ParamByName('processed').Value := process;
  updRabbitMQ.Parameters.ParamByName('status').Value := Status;
  if updRabbitMQ.ExecSQL > 0 then
    delRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  delRabbitMQ.ExecSQL;
end;

procedure TfrmAboutTime.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  StatusBar1.Panels[9].Text := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  ProcessINI;
  connectDB;
  QueueName := ParamStr(1);
  if QueueName='' then
  begin
    showmessage('Rabbit MQ queue name is required as the first command line parameter');
    Application.Terminate;
  end;

  if uppercase(ParamStr(2)) <> 'GUI' then
  begin
    ProcessPairs;
    application.Terminate;
  end;
end;

function TfrmAboutTime.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;


function TfrmAboutTime.ProcessINI: boolean;
var
  AboutTime: TIniFile;
begin
  try
    AboutTime := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := AboutTime.ReadString('Database', 'Server', '');
    aDatabase := AboutTime.ReadString('Database', 'DB', 'QM');
    ausername := AboutTime.ReadString('Database', 'UserName', '');
    apassword := AboutTime.ReadString('Database', 'Password', '');

    flogDir := AboutTime.ReadString('LogPaths', 'LogPath', '');


    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    AboutTime.Free;
  end;
end;


procedure TfrmAboutTime.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);

  LogName := IncludeTrailingBackslash(flogDir) + 'AboutTime_'+QueueName +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

end.
