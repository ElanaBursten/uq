unit TicketPSIDmain; // qm-639

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.Types,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.ComCtrls, Winapi.Activex,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtDlgs, Vcl.Buttons, Vcl.CheckLst, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, dxSkinsCore;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TfrmPSIDImportMain = class(TForm)
    pnlTop: TPanel;
    pnlBody: TPanel;
    pnlBottom: TPanel;
    OpenTextFileDialog1: TOpenTextFileDialog;
    Memo1: TMemo;
    ADOConn: TADOConnection;
    btnProcess: TButton;
    btnOpen: TButton;
    insLocate: TADOQuery;
    qryTicket: TADOQuery;
    spAssignLocate: TADOStoredProc;
    StatusBar1: TStatusBar;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    insTicket: TADOQuery;
    qryLastLocate: TADOQuery;
    qryClientID: TADOQuery;
    insTicketVersion: TADOQuery;
    updTicketImage: TADOQuery;
    Label7: TLabel;
    Label8: TLabel;
    btnKillSwitch: TBitBtn;
    cbCounties: TcxComboBox;
    cbGeoCode: TCheckBox;
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnKillSwitchClick(Sender: TObject);

  private
    LogResult: TLogResults;
    FilePath: string;
    csvFileName: string;
    clientID:integer;
    selectCounty: String;
    AssignedLocator:String;
    RecvManagerId:integer;
    AddedBy:integer;
    function OpenFile: String;
    function ConnectDB: boolean;
    function AddLocateRecord(ticketID: integer): boolean;

    procedure WriteLog;
    procedure clearLogRecord;
    procedure ProcessSheet;
    function DataSetToXML(DataSet: TDataSet): AnsiString;
    function DataSetToText(DataSet: TDataSet): AnsiString;

    { Private declarations }
  public
    { Public declarations }

    logPath:string;
    fileData: TStringList;
    HotStop: boolean;
    excelPage: TFilename;
    ToProcess: integer;
    function GetAppVersionStr: string;
  end;
  function GetGeoCode(StrAddress: Shortstring): Shortstring; stdcall; external 'Geocode.dll';
var
  frmPSIDImportMain: TfrmPSIDImportMain;
  KillProcess:boolean;
implementation

uses StrUtils, System.IniFiles;
{$R *.dfm}

procedure TfrmPSIDImportMain.btnKillSwitchClick(Sender: TObject);
begin
  KillProcess:=true;
  btnKillSwitch.Enabled:= false;
end;

procedure TfrmPSIDImportMain.btnOpenClick(Sender: TObject);
begin
  OpenFile;
  btnProcess.Enabled:=true;
end;

procedure TfrmPSIDImportMain.FormActivate(Sender: TObject);
begin
    try
      qryClientID.Open;
      ClientID := qryClientID.FieldByName('client_id').AsInteger;
    finally
      qryClientID.Close;
    end;

  StatusBar1.Panels[1].text := GetAppVersionStr;
end;

procedure TfrmPSIDImportMain.FormCreate(Sender: TObject);
begin
  ConnectDB;
  fileData := TStringList.Create;
  fileData.Delimiter := #9;
  fileData.StrictDelimiter := true;
  fileData.QuoteChar := #34;
  cbCounties.Properties.Items.LoadFromFile('OH_Counties.txt');
end;

procedure TfrmPSIDImportMain.FormDestroy(Sender: TObject);
begin
  fileData.Free;
  ADOConn.Close;
end;

function TfrmPSIDImportMain.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmPSIDImportMain.OpenFile: String;
var
  cnt: integer;
begin
  OpenTextFileDialog1.InitialDir:=Paramstr(0);
  if OpenTextFileDialog1.Execute(self.Handle) then
  begin
    try
      excelPage := OpenTextFileDialog1.FileName;
      csvFileName:= extractFileName(excelPage);
      fileData.Sorted:=true;
      fileData.Duplicates:=dupIgnore;
      fileData.LoadFromFile(excelPage);
//      fileData.
      cnt := fileData.Count;
      StatusBar1.Panels[7].Text := IntToStr(cnt);
      StatusBar1.Panels[2].text := 'Loading ' + csvFileName;
      memo1.Lines.Add('Opened File '+csvFileName+' for processing');

      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'OpenFile';
      LogResult.Status := csvFileName;
      WriteLog;

    except
      On E: Exception do
        result := E.Message;
    end;

  end;
end;

procedure TfrmPSIDImportMain.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName :=IncludeTrailingBackslash(LogPath) + 'CGO_PSID' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmPSIDImportMain.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmPSIDImportMain.btnProcessClick(Sender: TObject);
begin
  CoInitialize(nil);
  TThread.CreateAnonymousThread(
    procedure
    begin
      KillProcess := false;
      ProcessSheet;
    end).Start;
end;

procedure TfrmPSIDImportMain.ProcessSheet;
const
  WorkType = 'CREATE GAS SERVICE CARD';
  TicketType = 'GAS SERVICE CARD CREATION';
  WorkDescription = 'Create Service Card for %s. The Gas meter is located %s';
  workRemarks='Create Gas service card for: ';


  TicketPrefix='CGO-';
  TicketFormat = 'OUPS1';
  TicketKind = 'NORMAL';
  TicketRevision = '000';

  COMPANY ='UtiliQuest';

  STREET_ADDRESS = '%s %s, %s, %s';
var

  i: integer;
  stAddress: string;
  ticketImage : string;
  TicketNumber: string;
  ticketID: integer;
  locateID: integer;
  oneStr: TStringDynArray;
  county:string;
  SelectedCounty:string;
  LatLng, sLat, sLng: String;

  procedure GeoCodeAddress(sAddress: string);
  begin
    sLat:='';
    sLng:='';
    if sAddress <> '' then
    begin
      LatLng := GetGeoCode(sAddress);
      if LeftStr(LatLng, 7) = '[ERROR]' then
      begin
        sLat := '0';
        sLng := '0';
      end
      else
      begin
        sLat := copy(LatLng, 0, pos(',', LatLng) - 1);
        sLng := copy(LatLng, (pos(',', LatLng) + 1), maxChar);
      end;
    end;
  end;

begin
  i := 0;
  ToProcess := 0;
  SelectedCounty  :='';
  SelectedCounty :=  cbcounties.Text;
  StatusBar1.Panels[3].text := TimeToStr(Time);
  StatusBar1.Refresh;
  try
    ToProcess := fileData.Count - 1;
    try
      for i := 1 to fileData.Count - 1 do
      begin
        oneStr := SplitString(fileData[i], #9);
        TicketNumber := TicketPrefix+oneStr[0];

        county := trim(OneStr[6]);
        if county <> SelectedCounty then
        begin
          if KillProcess then
          begin
            KillProcess := false;
            btnKillSwitch.Enabled := true;
            break;
          end;
          continue;
        end;

          Memo1.Lines.Add('PSID: ' + oneStr[0] + ' ' + 'TCC: ' + oneStr[1] + ' ' + 'PremiseStat: ' + oneStr[2] + ' ' +
            'StreetNumber: ' + oneStr[3] + ' ' + 'StreetAddr: ' + oneStr[4] + ' ' + 'City: ' + oneStr[5] + ' ' + 'County: ' + oneStr[6] + ' '
            + 'TapCode: ' + oneStr[7] + ' ' + 'MeterLoc: ' + oneStr[8] + ' ' + 'MeterLocDesc: ' + oneStr[9] + ' ' + 'Completed By: ' +
            oneStr[10] + ' ' + 'Date: ' + oneStr[11]);


          stAddress := Format(STREET_ADDRESS,[oneStr[3], oneStr[4], oneStr[5] , 'OH']);
          try
            If cbGeoCode.Checked then GeoCodeAddress(stAddress);
          except
            on E: Exception do
            begin
              LogResult.LogType := ltError;
              LogResult.MethodName := 'GeoCodeAddress';
              LogResult.DataStream := stAddress;
              LogResult.ExcepMsg := E.Message;
              WriteLog;
            end;
          end;

            with insTicket.Parameters do
            begin
              ParamByName('ticket_number').Value := TicketNumber; //sr
              ParamByName('parsed_ok').Value := true; //sr
              ParamByName('ticket_format').Value := TicketFormat; //sr
              ParamByName('kind').Value := 'Normal'; //sr
              ParamByName('revision').Value := TicketRevision; //sr
              ParamByName('work_description').Value := Format(WorkDescription,[oneStr[0], oneStr[9]]);  //sr
              ParamByName('work_state').Value := 'OH';
              ParamByName('work_county').Value := oneStr[6];  //sr
              ParamByName('work_city').Value := oneStr[5];   //sr
              ParamByName('work_address_street').Value := oneStr[4];  //sr
              ParamByName('work_type').Value := WorkType; //sr
              ParamByName('work_remarks').Value := workRemarks+oneStr[1];
              ParamByName('Wo_Number').Value :=  oneStr[0];//sr
              ParamByName('work_address_number').Value := oneStr[3];  //sr
              ParamByName('image').Value := 'No Ticet Image Yet';
              ParamByName('Ticket_Type').Value := TicketType;
              ParamByName('recv_manager_id').Value := RecvManagerId;   //sr
              ParamByName('work_extent').Value := oneStr[9];   //sr
              if sLat<>'0' then
              ParamByName('work_lat').Value  :=  StrToFloat(sLat);   //sr
              if sLng<>'0' then
              ParamByName('work_long').Value  :=  StrToFloat(sLng);   //sr
              ParamByName('priority').Value :=  'Normal';
              ParamByName('active').Value := True;
              ParamByName('source').Value := 'OUPS1';
              ParamByName('serial_number').Value := oneStr[0];  //sr
            end; // with insTicket.Parameters do
            // write ticket
            Adoconn.BeginTrans;
            If insTicket.ExecSQL > 0 then
            begin
              Memo1.Lines.Add('Added Ticket ' + TicketNumber + ' to ticket table');
              qryTicket.Parameters.ParamByName('ticketNumber').Value := TicketNumber;
              qryTicket.Parameters.ParamByName('ticketFormat').Value := TicketFormat;
              try
                ticketID := 0;
                qryTicket.Open;
                ticketID := qryTicket.FieldByName('ticket_id').AsInteger;

                ticketImage := '';
                ticketImage := DataSetToText(qryTicket);

                updTicketImage.Parameters.ParamByName('tktImage').Value := ticketImage;
                updTicketImage.Parameters.ParamByName('ticketID').Value := ticketID;
                updTicketImage.ExecSQL;

                Memo1.Lines.Add('Pulled back Ticket ID for Ticket Number ' + TicketNumber + ' as ' + IntToStr(ticketID));
              finally
                qryTicket.Close; // pull back ticketID
              end;

              If AddLocateRecord(ticketID) then
              begin
                insTicketVersion.Parameters.ParamByName('TicketID').Value:= ticketID;
                insTicketVersion.ExecSQL;
                LogResult.LogType := ltInfo;
                LogResult.MethodName := 'Added ticket '+TicketNumber+' to Ticket Version table';
                WriteLog;
               end;
            end;


        StatusBar1.Panels[5].Text:=IntToStr(i);
        Adoconn.CommitTrans;
        if KillProcess then
        begin
          KillProcess:=false;
          btnKillSwitch.Enabled:= true;
          break;
        end;
      end; // for I := 1 to fileData.Count-1 do
    except
      on E: Exception do
      begin
        Adoconn.RollbackTrans;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process ticket '+TicketNumber;
        LogResult.Status:='Rolling back Transaction';
        LogResult.ExcepMsg:=e.Message;
        WriteLog;
      end;
    end;
  finally
    StatusBar1.Panels[4].text := TimeToStr(Time);
    StatusBar1.Refresh;
    Memo1.Lines.Add(IntToStr(i) + ' PSID records added');
    CoUnInitialize;
  end; // try-finally

end;

function TfrmPSIDImportMain.DataSetToXML(DataSet: TDataSet): AnsiString;
const
  ROOT = 'NewTicket';
var
  i: integer;
  function MakeTag(TagName, Value: String): string;
  begin
    result := '<' + TagName + '>' + Value + '</' + TagName + '>';
  end;

begin
  try
    result := '';
    if (not DataSet.Active) or (DataSet.IsEmpty) then
      Exit;
    result := result + '<' + ROOT + '>';
    DataSet.First;
    while not DataSet.eof do
    begin
      result := result + '<Record>';
      for i := 0 to DataSet.Fields.Count - 1 do
      begin
        if ((DataSet.Fields[i].Value <> null) or
          (DataSet.Fields[i].DisplayName <> 'image')) then
          result := result + MakeTag(DataSet.Fields[i].DisplayName,
            DataSet.Fields[i].Text);
      end;
      result := result + '</Record>';
      DataSet.Next;
    end;
    result := result + '</' + ROOT + '>';
  finally
    Memo1.Lines.Add('Created XML image');
  end;
end;

function TfrmPSIDImportMain.DataSetToText(DataSet: TDataSet): AnsiString;
var
  i: integer;
  ticketFields: TStringList;
begin
  try
    ticketFields := TStringList.Create;
    result := '';
    with DataSet do
    begin
      ticketFields.AddPair('ticket_number', FieldByName('ticket_number').AsString);
      ticketFields.AddPair('parsed_ok', FieldByName('parsed_ok').AsString);
      ticketFields.AddPair('ticket_format', FieldByName('ticket_format').AsString);
      ticketFields.AddPair('kind', FieldByName('kind').AsString);
      ticketFields.AddPair('revision', FieldByName('revision').AsString);
      ticketFields.AddPair('work_description', FieldByName('work_description').AsString);
      ticketFields.AddPair('work_state', FieldByName('work_state').AsString);
      ticketFields.AddPair('work_county', FieldByName('work_county').AsString);
      ticketFields.AddPair('work_city', FieldByName('work_city').AsString);
      ticketFields.AddPair('work_address_street', FieldByName('work_address_street').AsString);
      ticketFields.AddPair('work_type', FieldByName('work_type').AsString);
      ticketFields.AddPair('work_remarks', FieldByName('work_remarks').AsString);
      ticketFields.AddPair('Wo_Number', FieldByName('Wo_Number').AsString);
      ticketFields.AddPair('work_address_number', FieldByName('work_address_number').AsString);
      ticketFields.AddPair('Ticket_Type', FieldByName('Ticket_Type').AsString);
      ticketFields.AddPair('recv_manager_id', FieldByName('recv_manager_id').AsString);
      ticketFields.AddPair('work_extent', FieldByName('work_extent').AsString);
      ticketFields.AddPair('work_lat', FieldByName('work_lat').AsString);
      ticketFields.AddPair('work_long', FieldByName('work_long').AsString);
      ticketFields.AddPair('priority', FieldByName('priority').AsString);
      ticketFields.AddPair('active', FieldByName('active').AsString);
      ticketFields.AddPair('source', FieldByName('source').AsString);
      ticketFields.AddPair('serial_number', FieldByName('serial_number').AsString);
    end;
    result := ticketFields.text;
  finally
    Memo1.Lines.Add('Created Text Ticket image');

    Memo1.Lines.Add(result);
    ticketFields.Free;
  end;
end;


function TfrmPSIDImportMain.AddLocateRecord(ticketID: integer): boolean;
var
  locateID: integer;
begin
  try
    locateID := 0;
    with insLocate.Parameters do
    begin
      ParamByName('ticket_id').Value := ticketID;
      ParamByName('ClientID').Value := ClientID;
    end;
    try
      If insLocate.ExecSQL > 0 then
      begin
        qryLastLocate.Parameters.ParamByName('ticketID').Value := ticketID;
        qryLastLocate.Parameters.ParamByName('ClientID').Value := ClientID;
        qryLastLocate.Open;
        locateID := qryLastLocate.FieldByName('locate_id').AsInteger;

        spAssignLocate.Parameters.ParamByName('@LocateID').Value := locateID;
        spAssignLocate.Parameters.ParamByName('@LocatorID').Value := AssignedLocator;
        spAssignLocate.Parameters.ParamByName('@AddedBy').Value := AddedBy;
        spAssignLocate.Parameters.ParamByName('@WorkloadDate').Value := NOW;
        spAssignLocate.ExecProc;
        result := true;
      end;

    finally
      qryLastLocate.Close;

      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'AddLocateRecord';
      LogResult.DataStream :=  'TicketID: ' + IntToStr(ticketID)+ ' LocateID: ' + IntToStr(locateID);
      WriteLog;
      Memo1.Lines.Add('Added Locate Record');
      Memo1.Lines.Add( 'TicketID: ' + IntToStr(ticketID)+ ' LocateID: ' + IntToStr(locateID));

    end;

  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'AddLocateRecord';
      LogResult.DataStream := insLocate.SQL.text;
      LogResult.ExcepMsg := 'Ticket: ' + IntToStr(ticketID) + ' ' + E.Message;
      Memo1.Lines.Add(LogResult.ExcepMsg);
      WriteLog;
      result := false;
    end;
  end;
end;



function TfrmPSIDImportMain.ConnectDB: boolean;
var
  myPath: string;
  connStr, LogConnStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  Trusted:integer;
  TicketPSIDimporter: TIniFile;
begin
  result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
  try
    TicketPSIDimporter := TIniFile.Create(myPath + 'TicketPSIDimporter.ini');

    aServer := TicketPSIDimporter.ReadString('Database', 'Server', '');
    aDatabase := TicketPSIDimporter.ReadString('Database', 'DB', 'QM');
    ausername := TicketPSIDimporter.ReadString('Database', 'UserName', '');
    apassword := TicketPSIDimporter.ReadString('Database', 'Password', '');
    Trusted  := TicketPSIDimporter.ReadInteger('Database', 'Trusted', 0);
    logPath :=  TicketPSIDimporter.ReadString('LogPaths', 'LogPath', '');

    AssignedLocator := TicketPSIDimporter.ReadString('Process', 'AssignedLocator', '34731');
    AddedBy         := TicketPSIDimporter.ReadInteger('Process', 'AddedBy', 23);
    RecvManagerId   := TicketPSIDimporter.ReadInteger('Process', 'RecvManagerId', 7389);

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    LogConnStr:= 'Provider=SQLNCLI11.1;+Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(LogConnStr);
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
    result := ADOConn.Connected;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Connect DB';
    LogResult.DataStream := LogConnStr;
    WriteLog;

    if result then
    begin
      Memo1.Lines.Add('Connected to database: '+aServer);
      StatusBar1.Panels[0].text := aServer;
    end
    else
      Memo1.Lines.Add('Could not connect to DB');
  finally
    TicketPSIDimporter.Free;
  end;
end;

end.
