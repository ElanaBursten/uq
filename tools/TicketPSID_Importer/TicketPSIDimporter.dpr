program TicketPSIDimporter;
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}

uses
  Vcl.Forms,
  TicketPSIDmain in 'TicketPSIDmain.pas' {frmPSIDImportMain};

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPSIDImportMain, frmPSIDImportMain);
  Application.Run;
end.
