object frmPSIDImportMain: TfrmPSIDImportMain
  Left = 0
  Top = 0
  Caption = '       -- No PSIDs on Tap--'
  ClientHeight = 564
  ClientWidth = 1199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 1199
    Height = 25
    Align = alTop
    TabOrder = 0
  end
  object pnlBody: TPanel
    Left = 0
    Top = 25
    Width = 1199
    Height = 453
    Align = alClient
    TabOrder = 1
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 1008
      Height = 451
      Align = alLeft
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object cbCounties: TcxComboBox
      Left = 1015
      Top = 6
      TabStop = False
      Properties.DropDownRows = 30
      Properties.IncrementalFiltering = True
      TabOrder = 1
      Text = 'Select One County'
      Width = 175
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 478
    Width = 1199
    Height = 67
    Align = alBottom
    TabOrder = 2
    object Label2: TLabel
      Left = 2
      Top = 48
      Width = 63
      Height = 13
      Caption = 'Connected '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 130
      Top = 48
      Width = 42
      Height = 13
      Caption = 'Version'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 308
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Process'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 571
      Top = 48
      Width = 74
      Height = 13
      Caption = 'Time Started'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 692
      Top = 48
      Width = 85
      Height = 13
      Caption = 'Time Complete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 812
      Top = 48
      Width = 33
      Height = 13
      Caption = 'Count'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 892
      Top = 48
      Width = 29
      Height = 13
      Caption = 'Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnProcess: TButton
      Left = 277
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Process'
      Enabled = False
      TabOrder = 0
      OnClick = btnProcessClick
    end
    object btnOpen: TButton
      Left = 40
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 1
      OnClick = btnOpenClick
    end
    object btnKillSwitch: TBitBtn
      Left = 1024
      Top = 6
      Width = 89
      Height = 25
      Caption = 'Kill Switch'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000020000000C05031A46110852AB190C76E31D0E89FF1C0E89FF190C
        76E4120852AD06031B4D0000000E000000030000000000000000000000000000
        000301010519130A55A9211593FF2225AEFF2430C2FF2535CBFF2535CCFF2430
        C3FF2225AFFF211594FF140B58B20101051E0000000400000000000000020101
        03151C1270CD2522A6FF2D3DCCFF394BD3FF3445D1FF2939CDFF2839CDFF3344
        D0FF394AD4FF2D3CCDFF2523A8FF1C1270D20101051D00000003000000091912
        5BA72A27AAFF2F41D0FF3541C7FF2726ABFF3137BCFF384AD3FF384BD3FF3137
        BCFF2726ABFF3540C7FF2E40D0FF2927ACFF1A115EB10000000D08061C3D3129
        A2FD2C3CCCFF3842C6FF5F5DBDFFEDEDF8FF8B89CEFF3337B9FF3437B9FF8B89
        CEFFEDEDF8FF5F5DBDFF3741C6FF2B3ACDFF3028A4FF0907204A1E185F9F373B
        BCFF3042D0FF2621A5FFECE7ECFFF5EBE4FFF8F2EEFF9491D1FF9491D1FFF8F1
        EDFFF3E9E2FFECE6EBFF2621A5FF2E3FCFFF343ABEFF201A66B0312A92E03542
        CBFF3446D1FF2C2FB5FF8070ADFFEBDBD3FFF4EAE4FFF7F2EDFFF8F1EDFFF4E9
        E2FFEADAD1FF7F6FACFF2B2EB5FF3144D0FF3040CBFF312A95E53E37AEFA3648
        D0FF374AD3FF3A4ED5FF3234B4FF8A7FB9FFF6ECE7FFF5ECE6FFF4EBE5FFF6EB
        E5FF897DB8FF3233B4FF384BD3FF3547D2FF3446D1FF3E37AEFA453FB4FA4557
        D7FF3B50D5FF4C5FDAFF4343B7FF9189C7FFF7EFE9FFF6EEE9FFF6EFE8FFF7ED
        E8FF9087C5FF4242B7FF495DD8FF394CD4FF3F52D4FF443FB3FA403DA1DC5967
        DAFF5B6EDDFF4F4DBAFF8F89CAFFFBF6F4FFF7F1ECFFEDE1D9FFEDE0D9FFF7F0
        EAFFFAF5F2FF8F89CAFF4E4DB9FF576ADCFF5765D9FF403EA4E12E2D70987C85
        DDFF8798E8FF291D9BFFE5DADEFFF6EEEBFFEDDFDAFF816EA9FF816EA9FFEDDF
        D8FFF4ECE7FFE5D9DCFF291D9BFF8494E7FF7A81DDFF33317BAC111125356768
        D0FC9EACEDFF686FCEFF5646A1FFCCB6BCFF7A68A8FF4C4AB6FF4D4BB7FF7A68
        A8FFCBB5BCFF5646A1FF666DCCFF9BAAEEFF696CD0FD1212273F000000043B3B
        79977D84DFFFA5B6F1FF6D74D0FF2D219BFF5151B9FF8EA2ECFF8EA1ECFF5252
        BBFF2D219BFF6B72D0FFA2B3F0FF8086E0FF404183A700000008000000010303
        050C4E509DBC8087E2FFAEBDF3FFA3B6F1FF9DAFF0FF95A9EEFF95A8EEFF9BAD
        EFFFA2B3F0FFACBCF3FF838AE3FF4F52A0C10303051100000002000000000000
        000100000005323464797378D9F8929CEAFFA1AEEFFFB0BFF3FFB0BFF4FFA2AE
        EFFF939DE9FF7479DAF83234647D000000080000000200000000000000000000
        000000000000000000031213232D40437D935D61B5D07378DFFC7378DFFC5D61
        B5D040437D951212223000000004000000010000000000000000}
      TabOrder = 2
      OnClick = btnKillSwitchClick
    end
    object cbGeoCode: TCheckBox
      Left = 448
      Top = 16
      Width = 81
      Height = 17
      Caption = 'GeoCode'
      TabOrder = 3
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 545
    Width = 1199
    Height = 19
    Panels = <
      item
        Text = 'Not Connected'
        Width = 130
      end
      item
        Text = 'version'
        Width = 180
      end
      item
        Text = 'Process'
        Width = 260
      end
      item
        Text = 'Start Time'
        Width = 120
      end
      item
        Text = 'TimeFinish'
        Width = 120
      end
      item
        Text = 'count'
        Width = 60
      end
      item
        Alignment = taCenter
        Text = 'of'
        Width = 24
      end
      item
        Text = 'total'
        Width = 80
      end>
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Filter = 'Tab Delimited|*.txt'
    Left = 48
    Top = 160
  end
  object ADOConn: TADOConnection
    CommandTimeout = 120
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 40
    Top = 57
  end
  object insLocate: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ClientID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'INSERT INTO [dbo].[locate]'
      '           ('
      '             ticket_id'
      '            ,client_code'
      '            ,client_id'
      '            ,status'
      '            ,active'
      '            ,invoiced'
      '            ,assigned_to'
      '            ,added_by'
      '            ,assigned_to_id'
      ''
      '           )'
      '     VALUES'
      '           ( :ticket_id'
      '            ,'#39'CGOSVC'#39
      '            ,:ClientID'
      '            ,'#39'-R'#39
      '            ,1'
      '            ,0'
      '            ,10564'
      '            ,3512'
      '            ,10564'
      '           )')
    Left = 328
    Top = 57
  end
  object qryTicket: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticketNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticketFormat'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Select top 1 *'
      'From ticket'
      'where ticket_number = :ticketNumber'
      'and ticket_format =:ticketFormat'
      'order by modified_date desc')
    Left = 240
    Top = 56
  end
  object spAssignLocate: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 120
    ProcedureName = 'assign_locate'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkloadDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 408
    Top = 121
  end
  object insTicket: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'parsed_ok'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'kind'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'revision'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_description'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_state'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_county'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_city'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_address_street'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_type'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_remarks'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Wo_Number'
        Size = -1
        Value = Null
      end
      item
        Name = 'work_address_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'image'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Ticket_Type'
        Size = -1
        Value = Null
      end
      item
        Name = 'recv_manager_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'work_extent'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'work_lat'
        DataType = ftBCD
        Size = -1
        Value = Null
      end
      item
        Name = 'work_long'
        DataType = ftBCD
        Size = -1
        Value = Null
      end
      item
        Name = 'priority'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'active'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'source'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'serial_number'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'insert into ticket'
      
        '(ticket_number,parsed_ok,ticket_format,kind,revision,transmit_da' +
        'te,modified_date,work_date,'
      'due_date,legal_date,legal_due_date, Expiration_date,'
      
        'work_description,work_state,work_county,work_city,company, con_n' +
        'ame,'
      
        'work_address_street,work_type,work_remarks,wo_number,work_addres' +
        's_number,image,'
      
        'ticket_type,recv_manager_id,work_extent, work_lat, work_long, pr' +
        'iority, active,source,serial_number)'
      'values'
      
        '(:ticket_number,:parsed_ok,:ticket_format,:kind,:revision,getdat' +
        'e(),getdate(),getdate(),'
      'getdate()+1080, getdate()+1080,getdate()+1080,getdate()+1080,'
      
        ':work_description,:work_state, :work_county,:work_city,'#39'Colombia' +
        ' Gas'#39','#39'UtiliQuest'#39','
      
        ':work_address_street,:work_type,:work_remarks,:Wo_Number,:work_a' +
        'ddress_number, :image,'
      
        ':Ticket_Type,:recv_manager_id,:work_extent, :work_lat, :work_lon' +
        'g, :priority, :active,:source,:serial_number)'
      ''
      ''
      '')
    Left = 176
    Top = 56
  end
  object qryLastLocate: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ClientID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select locate_id'
      'from locate'
      'where ticket_id = :TicketID'
      'and client_id =:ClientID')
    Left = 408
    Top = 56
  end
  object qryClientID: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    SQL.Strings = (
      'Select client_id'
      'From client'
      'where oc_code='#39'CGOSVC'#39
      ''
      '')
    Left = 176
    Top = 144
  end
  object insTicketVersion: TADOQuery
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'TicketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'USE QM'
      'insert into ticket_version'
      
        'select ticket_id,revision,ticket_number,ticket_type,transmit_dat' +
        'e,transmit_date,transmit_date,null,'
      'null,'#39'excel'#39','#39'OUPS1'#39','#39'OUPS1'#39
      'from ticket with (nolock)'
      'where ticket_id=:TicketID')
    Left = 320
    Top = 257
  end
  object updTicketImage: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'tktImage'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'ticketID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update ticket'
      'set image = :tktImage'
      'where ticket_id = :ticketID')
    Left = 280
    Top = 208
  end
end
