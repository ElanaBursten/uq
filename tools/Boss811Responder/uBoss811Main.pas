unit uBoss811Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls, System.json,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, REST.Types,
  cxDBLookupEdit, cxDBLookupComboBox, MSXML, cxDBExtLookupComboBox, IPPeerClient, REST.Client, REST.Authenticator.Basic,
  Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ComCtrls, IdMessage, IdIntercept, IdLogBase, IdLogEvent, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: String[1];
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry


type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;  //TLogResults
 type
    TCallCenter=class
  private
    FRespondto: string;
    FInitials: string;
    FID: string;
    FPosturl: string;

    FClients: TStringList;
    FCodeMappings: TStringList;
    FStatusCodeSkip: TStringList;
    CallCenter: TCallCenter;
    fCallCenterName: string;
    {Get/Set methods}
  published
    property CallCenterName: string read fCallCenterName write fCallCenterName;
    property Respondto: string read FRespondto write FRespondto;
    property Initials: string read FInitials write FInitials;
    property ID: string read FID write FID;
    property Posturl: string read FPosturl write FPosturl;
    property Clients: TStringList read FClients write FClients;
    property Codemappings: TStringList read FCodemappings write FCodeMappings;
    property StatusCodeSkip: TStringList read FStatusCodeSkip write FStatusCodeSkip;
  end;   //TCallCenter

type
  TfrmBoss811Responder = class(TForm)
    Label1: TLabel;
    btnParseXML: TButton;
    Memo1: TMemo;
    edtCallCenterName: TEdit;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    RESTResponse1: TRESTResponse;
    RESTClient1: TRESTClient;
    delResponse: TADOQuery;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    RESTRequest1: TRESTRequest;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdLogEvent1: TIdLogEvent;
    IdMessage1: TIdMessage;
    qryEMailRecipients: TADOQuery;
    procedure btnParseXMLClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
  private

    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;
    fconfigXML: tFilename;
    CallCenter: TCallCenter;
    fOC_Code: string;
    function connectDB: boolean;
    function ProcessConfigXML(xmlFile: tFilename): boolean;
    function ProcessINI: boolean;
    procedure clearLogRecord;
    function FindCCnode(scallcenter: String; doc: IXMLDOMDocument; out Node: IXMLDOMNode; out nodemapattr: IXMLDOMNamedNodeMap): Boolean;
    function FillList(doc: IXMLDOMDocument; scallcenter: String; NodeValues: TStringList): Boolean;
    procedure InitializeRest;
    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    procedure SetUpRecipients;
    { Private declarations }
  public
    { Public declarations }
    property OC_Code:string read fOC_Code write fOC_Code;
    function processParams:boolean;
    procedure WriteLog(LogResult: TLogResults);
    property logDir: wideString read flogDir;
    property configXML: tFilename read fconfigXML;
  end;

var
  frmBoss811Responder: TfrmBoss811Responder;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmBoss811Responder.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;

procedure TfrmBoss811Responder.btnParseXMLClick(Sender: TObject);
begin
  if edtCallCenterName.Text = '' then
  begin
    showmessage('Call Center Required');
    Exit;
  end;

 ProcessConfigXML(fconfigXML);
end;

function TfrmBoss811Responder.FillList(doc: IXMLDOMDocument; scallcenter: String; NodeValues: TStringList): Boolean;
var
 nodemap: IXMLDOMNamedNodeMap;
 clientmap: IXMLDOMNamedNodeMap;
 nodelist, clientlist, statuscodeskiplist, codemappingslist: IXMLDOMNodeList;
 CCnode, Clientnode, StatusCodeSkip, Codemappings, SkipCode, ClientTerm: IXMLDOMNode;
 uqcode, response: IXMLDOMNode;
 warningmsg: string;
 I: Integer;
begin
//https://nisource.boss811.com/api/v2/tickets/by_center/537/by_number/2110052248/ticket_due_at_revisions
   Result := true;
   If not FindCCnode(scallcenter, doc, CCNode, nodemap) then
   begin
     warningmsg := 'call center ' + edtCallCenterName.text + ' not found';
     showmessage(warningmsg);
     LogResult.MethodName := 'ParseXML';
     LogResult.Status := warningmsg;
     LogResult.LogType := ltWarning;
     WriteLog(LogResult);
     Result := false;
     Exit;
   end;

   With CallCenter do
   begin
     CallCenterName               := Nodemap.getNamedItem('name').NodeValue;
     Respondto                    := Nodemap.getNamedItem('respond_to').NodeValue;
     Initials                     := Nodemap.getNamedItem('initials').NodeValue;
     ID                           := Nodemap.getNamedItem('id').NodeValue;
     Posturl                      := Nodemap.getNamedItem('post_url').NodeValue;

      memo1.Lines.AddPair( 'call center Name', CallCenterName);
      memo1.Lines.AddPair('Respondto',Respondto);
      memo1.Lines.AddPair('Initials',Initials);
      memo1.Lines.AddPair('ID',ID);
      memo1.Lines.AddPair('Posturl',Posturl);

    end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('code_mappings');
   Codemappings := CCnode.SelectSingleNode('code_mappings');
   if Codemappings <> nil then
   begin
     Codemappingslist := CodeMappings.selectNodes('mapping');
     for i := 0 to CodeMappingslist.length - 1 do
     begin
       uqcode := IXMLDOMNode(CodeMappingslist[I]).attributes.getNamedItem('uq_code');
       Response := IXMLDOMNode(CodeMappingslist[I]).attributes.getNamedItem('response');
       if (uqcode <> nil) and (response <> nil) then
       begin
         CallCenter.Codemappings.Add(uqcode.NodeValue+'='+response.NodeValue);
         Memo1.Lines.Add('uqcode: '+uqcode.NodeValue+'    Response: '+Response.NodeValue);
        end;
     end;
     Memo1.Lines.Add('');
     Statuscodeskip :=  CCnode.selectSingleNode('status_code_skip');
     if Statuscodeskip <> nil then
     begin
       Statuscodeskiplist := Statuscodeskip.selectNodes('code');
       Memo1.Lines.Add('status_code_skip');
       for i := 0 to Statuscodeskiplist.length - 1 do
       begin
         SkipCode := IXMLDOMNode(Statuscodeskiplist[I]).attributes.getNamedItem('value');
         if SkipCode <> nil then
         begin
           CallCenter.StatusCodeSkip.Add(SkipCode.NodeValue);
           Memo1.Lines.Add('code: '+SkipCode.NodeValue);
         end;
       end;
     end;
   end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('clients');
   ClientNode :=  CCnode.selectSingleNode('clients');
   if ClientNode <> nil then
   begin
     clientlist :=  ClientNode.selectNodes('term');
     for i := 0 to Clientlist.length - 1 do
     begin
       ClientTerm := IXMLDOMNode(ClientList[I]).attributes.getNamedItem('name');
       if ClientTerm <> nil then
       begin
        CallCenter.Clients.Add(ClientTerm.NodeValue);
        Memo1.Lines.Add('term: ' +ClientTerm.NodeValue);
       end;
     end;
   end;
end;

function TfrmBoss811Responder.FindCCnode(scallcenter: String; doc: IXMLDOMDocument; out Node: IXMLDOMNode;
  out nodemapattr: IXMLDOMNamedNodeMap): Boolean;

var
  ccnodelist: IXMLDOMNodeList;
  nodelist: IXMLDOMNodeList;
  nodemap: IXMLDOMNamedNodeMap;
  ccnode: IXMLDOMNode;
  I: Integer;
begin
  Result := false;
   ccnodelist := doc.selectNodes('//responder/boss811/callcenter');
   for I := 0 to ccNodeList.length - 1 do
   begin
     nodemap := ccNodeList[I].attributes;
     ccnode := nodemap.getNamedItem('name');
     if ccnode <> nil then
     begin
       if ccnode.nodeValue = scallcenter then
       begin
         node := ccNodeList[I];
         nodemapattr := nodemap;
         Result := true;
         break;
       end;
     end;
   end;
end;

procedure TfrmBoss811Responder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmBoss811Responder.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  AppVer:='';
  ProcessINI;
  CallCenter := TCallCenter.Create;
  processParams;
  connectDB;
  SetUpRecipients;
  CallCenter.Clients := TStringlist.Create;
  CallCenter.Codemappings := TStringlist.Create;
  CallCenter.StatusCodeSkip := TStringlist.Create;
  btnParseXMLClick(Sender);
  InitializeRest;
  AppVer  := GetAppVersionStr;

  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);


  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

procedure TfrmBoss811Responder.FormDestroy(Sender: TObject);
begin
  CallCenter.Clients.Free;
  CallCenter.StatusCodeSkip.Free;
  CallCenter.Codemappings.Free;
  CallCenter.Free;
end;

function TfrmBoss811Responder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmBoss811Responder.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

procedure TfrmBoss811Responder.InitializeRest;
begin
//  RESTClient1.BaseURL :=  CallCenter.Posturl;
  LogResult.MethodName := 'InitializeRest';
  LogResult.DataStream := RESTClient1.BaseURL;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
end;

function TfrmBoss811Responder.ProcessConfigXML(xmlFile: tFilename):boolean;
var
  XMLDOMDocument: IXMLDOMDocument;
  XMLDOMNodeList: IXMLDOMNodeList;
  XMLDOMNode: IXMLDOMNode;
  aXMLDOMNode: IXMLDOMNode;
  logNode: IXMLDOMNode;
  xml: TStringList;
begin
  result := false;
    try
      xml := TStringList.Create;
      xml.LoadFromFile(xmlFile);
      XMLDOMDocument := CoDOMDocument.Create;
      try
        XMLDOMDocument.loadXML(xml.Text);
        if (XMLDOMDocument.parseError.ErrorCode <> 0) then
          raise Exception.CreateFmt('Error in Xml Data %s', [XMLDOMDocument.parseError]);

        logNode :=  XMLDOMDocument.selectSingleNode('//ticketparser/logdir');
        flogDir := logNode.attributes.getNamedItem('name').Text;
        If not DirectoryExists(flogDir) then flogDir := 'C:\QM\Logs';
        ForceDirectories(LogDir);


        If FillList(XMLDOMDocument, edtCallCenterName.text, xml) then
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.Status := 'XML successfully parsed';
          LogResult.LogType := ltInfo;
          WriteLog(LogResult);
        end;
      except
        on E: Exception do
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.ExcepMsg := E.Message;
          LogResult.DataStream := XMLDOMDocument.Text;
          LogResult.LogType := ltError;
          WriteLog(LogResult);
          exit;
        end;
      end;
      result := true;
    finally
      XMLDOMDocument := nil;
      if assigned(xml) then
      FreeAndNil(xml);
      btnParseXML.Enabled:=false;
    end;
end;

function TfrmBoss811Responder.ProcessINI: boolean;
var
  Boss811WebServer: TIniFile;
begin
  try
    Boss811WebServer := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := Boss811WebServer.ReadString('Database', 'Server', '');
    aDatabase := Boss811WebServer.ReadString('Database', 'DB', 'QM');
    ausername := Boss811WebServer.ReadString('Database', 'UserName', '');
    apassword := Boss811WebServer.ReadString('Database', 'Password', '');

    OC_Code := Boss811WebServer.ReadString('EMailAlerts', 'RecipientsOC_Code', 'NCA1');

    with IdMessage1.From do  //QM-552
    begin
      Address:=Boss811WebServer.ReadString('EMailAlerts', 'FromAddress', '');
      Domain:=Boss811WebServer.ReadString('EMailAlerts', 'FromDomain', '');
      Name:=Boss811WebServer.ReadString('EMailAlerts', 'FromName', '');
      Text:=Boss811WebServer.ReadString('EMailAlerts', 'FromText', '');
      User:=Boss811WebServer.ReadString('EMailAlerts', 'FromUser', '');
    end;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    Boss811WebServer.Free;
  end;
end;

function TfrmBoss811Responder.processParams: boolean;
begin
  fconfigXML:=ParamStr(1);
  edtCallcenterName.Text:= ParamStr(2);

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'processParams';
  LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
  WriteLog(LogResult);
end;

procedure TfrmBoss811Responder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + 'BOSS811Response' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;
procedure TfrmBoss811Responder.btnSendResponseClick(Sender: TObject);
{
  "rescheduled":
    "new_due_at": "2021-10-20 08:17:00",             --due_date
    "contact_name": "John Doe",                      --contact
    "contact_phone": "123-456-7890",                 --contact_phone
    "reason": "why was locate request rescheduled"   --reschedule_reason
}

const
  DBL_QUOTES = '"';

 BOSS811 = '{ '+
           ' "rescheduled": {  '+
           '   "new_due_at": %s,  '+  //2021-10-20 08:17:00
           '   "contact_name": %s,  '+  //John Doe
           '   "contact_phone": %s,  '+  //123-456-7890
           '   "reason": %s '+ //why was locate request rescheduled
           '} '+
          '} ';

  PLACEHOLDER=   '{insert_ticket_number}';
var
  SerialNumber:string;
  TicketNo:string;
  TicketStatus:string;
  LocateID:integer;
  formattedURL:string;
  ContactName, ContactPhone:string;
  RSReason:string;
  CustBody:string;
  NewDueDate:string;
  sendResponse:string;
  cntGood, cntBad :integer;
  ResponseBody:string;
  requestID:string;
begin

  TicketNo:='';
  LocateID:=0;
  CustBody:='';

  requestID:='';
  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value:= CallCenter.Respondto;
    Parameters.ParamByName('@CallCenter').Value:= CallCenter.CallCenterName;
    open;
    while not eof do
    begin

      TicketNo    := FieldByName('ticket_number').AsString;
      LocateID    := FieldByName('locate_id').AsInteger;
      ContactPhone:= FieldByName('contact_phone').AsString;
      TicketStatus:= FieldByName('status').AsString;
      ContactName := FieldByName('contact').AsString;
      NewDueDate  := FormatDateTime('yyyy-mm-dd hh:nn:ss', FieldByName('scheduled_meet_datetime').AsDateTime);
      RSReason    := StringReplace(FieldByName('reschedule_reason').AsString,'/', ' or ',[rfReplaceAll, rfIgnoreCase]);
      formattedURL:= StringReplace(CallCenter.Posturl, PLACEHOLDER, TicketNo,[rfReplaceAll, rfIgnoreCase]);

      sendResponse:= Format(BOSS811,[DBL_QUOTES+NewDueDate+DBL_QUOTES, DBL_QUOTES+ContactName+DBL_QUOTES, DBL_QUOTES+ContactPhone+DBL_QUOTES, DBL_QUOTES+RSReason+DBL_QUOTES]);
      memo1.Lines.Add('sendResponse: '+sendResponse);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';
      RESTClient1.BaseURL :=  formattedURL;
      RESTRequest1.AddBody(sendResponse, ctAPPLICATION_JSON);
      RESTRequest1.Method := TRESTRequestMethod.rmPOST;
      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + sendResponse;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except

      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin
        ResponseBody:= RESTResponse1.Content;
        memo1.Lines.Add('ResponseBody: '+ResponseBody);
        requestID:=getData(ResponseBody, 'id');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket';
        LogResult.DataStream := sendResponse;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter.CallCenterName;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := 'BOSS811';
        insertResponseLog.Sucess              := '1';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);

        delResponse.Parameters.ParamByName('ResponseTo').Value := CallCenter.Respondto;
        delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

        memo1.Lines.Add(delResponse.SQL.Text);

        try
         delResponse.ExecSQL;   //commented out for testing
        except on E: Exception do
          begin
            LogResult.LogType := ltError;
            LogResult.MethodName := 'delResponse';
            LogResult.DataStream := delResponse.SQL.Text;
            LogResult.ExcepMsg:=e.Message;
            WriteLog(LogResult);
          end;
        end;
      end
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +TicketNo;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;

        WriteLog(LogResult);
        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter.CallCenterName;
        insertResponseLog.status              := TicketStatus;
        insertResponseLog.ResponseSent        := 'BOSS811';
        insertResponseLog.Sucess              := '0';
        insertResponseLog.Reply               := LeftStr(IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText, 40);
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;
      Next;  //response
    end; //while-not EOF

  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);

  if cntBad>0 then
  begin
    idSMTP1.Connect;
    idSMTP1.Send(IdMessage1);
    idSMTP1.Disconnect();

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Sending email';
    LogResult.Status := 'Some responses failed';
    LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
    WriteLog(LogResult);
  end;

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmBoss811Responder.SetUpRecipients;
begin
  try
    idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+CallCenter.RespondTo+' '+CallCenter.CallCenterName;
    qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
    qryEMailRecipients.Open;
    if not qryEMailRecipients.eof then
    idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
    else
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SetUpRecipients';
      LogResult.Status:= 'No recipients to send email to';
      WriteLog(LogResult);
   end;
  finally
    qryEMailRecipients.close;
  end;
end;

procedure TfrmBoss811Responder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmBoss811Responder.connectDB: boolean;
var
  connStr, connStrLog: String;
begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog:='Provider=SQLNCLI11.1;Password=Not Logged ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
