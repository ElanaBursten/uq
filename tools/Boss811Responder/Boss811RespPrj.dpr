program Boss811RespPrj;
//QM-478 Boss811 Responder   SR
{$R '..\..\QMIcon.res'}
{$R 'QMVersion.res' '..\..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  uBoss811Main in 'uBoss811Main.pas' {frmBoss811Responder},
  GlobalSU in 'GlobalSU.pas';

var
  MyInstanceName: string;
// takes params "C:\Trunk1\tools\Boss811Responder\config.xml" "1851" "GUI" with GUI optional
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName)+'_'+ParamStr(2);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(3)='GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm:=True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm:=False;
    end;
    Application.Title := '';
  Application.CreateForm(TfrmBoss811Responder, frmBoss811Responder);
    Application.Run;
  end
  else
    Application.Terminate;
end.
