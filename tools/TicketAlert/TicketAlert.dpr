program TicketAlert;
// QM-778
uses
  Vcl.Forms,
  SysUtils,
  TicketAlertMain in 'TicketAlertMain.pas' {frmTicketAlert},
  GlobalSU in '..\..\common\GlobalSU.pas';

{$R 'QMVersion.res' '..\..\QMVersion.rc'}
{$R '..\..\QMIcon.res'}

var
  MyInstanceName: string;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName+'_'+ParamStr(1));
  RunningInstance:=MyInstanceName;
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamStr(2) = 'GUI' then
    begin
      Application.MainFormOnTaskbar := True;
      Application.ShowMainForm := True;
    end
    else
    begin
      Application.MainFormOnTaskbar := False;
      Application.ShowMainForm := False;
    end;
    Application.Title := '';

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmTicketAlert, frmTicketAlert);
    Application.Run;
  end
  else
    Application.Terminate;
end.
