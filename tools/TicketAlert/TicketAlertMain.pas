unit TicketAlertMain;
// QM-760
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.JSON, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB, Data.Win.ADODB;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults
type
  TfrmTicketAlert = class(TForm)
    pnlTop: TPanel;
    pnlMiddle: TPanel;
    pnlBottom: TPanel;
    Memo1: TMemo;
    Splitter1: TSplitter;
    ADOConn: TADOConnection;
    qryRabbitMQ: TADOQuery;
    updRabbitMQ: TADOQuery;
    delRabbitMQ: TADOQuery;
    cbRunOnce: TCheckBox;
    StatusBar1: TStatusBar;
    btnParsejson: TButton;
    JsonMemo: TMemo;
    Splitter2: TSplitter;
    qryStaging: TADOQuery;
    insStagingTable: TADOQuery;
    qryAlertType: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btnParsejsonClick(Sender: TObject);
  private
    { Private declarations }

      aDatabase: String;
      aServer: String;
      ausername: string;
      apassword: string;

      LogResult: TLogResults;
      flogDir: wideString;
      QueueName:string;
      function ProcessINI: boolean;
      function connectDB: boolean;
      procedure clearLogRecord;
      procedure getData(OrigObj: TJsonObject; Field: String);

      procedure ProcessPairs;
      procedure DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
      function GetAppVersionStr: string;
      function BuildStagingQuery: Boolean;
      function GetAlertType(out AlertType: String): Boolean;
  public
    { Public declarations }
    Strlist: TStringList;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmTicketAlert: TfrmTicketAlert;
  RunningInstance:string;
implementation
uses dateutils, StrUtils, iniFiles;
{$R *.dfm}

{ TForm2 }


procedure TfrmTicketAlert.btnParsejsonClick(Sender: TObject);
begin
  ProcessPairs;
end;

//BP  Process mqrabbit_in table
procedure TfrmTicketAlert.ProcessPairs;
var
  Json_Data: String;
  JsonValue  : TJsonValue;
  OriginalObj : TJsonObject;
  StageFields, I: Integer;
  Field: String;
  MessageID: Integer;
begin
  try
    try
      StrList := TStringList.Create;
      StrList.Duplicates := dupIgnore;
      qryStaging.Open;
      JsonMemo.Lines.Clear;
      qryRabbitmq.Parameters.ParamByName('QueueName').Value := QueueName;
      qryRabbitmq.Open;

      while not qryRabbitmq.Eof do
      begin
        MessageID :=  qryRabbitMQ.FieldbyName('message_id').AsInteger;
        Json_data := qryRabbitMQ.FieldbyName('message_payload').AsString;
        JsonValue := TJsonObject.ParseJSONValue(Json_data);
        OriginalObj := JsonValue as TJsonObject;
        //iterate thrugh Json fields, Process Json strings and objects
        for I := 0 to OriginalObj.Count - 1 do
        begin
          Field :=  OriginalObj.Pairs[I].JsonString.Value;
          GetData(OriginalObj, Field);
        end;
        JsonMemo.Lines.Add('---------------------------------------------');

        //build staging table insert query
        If BuildStagingQuery then
          DeleteFromMQ(MessageID, 'PROCESSED',True);

        StrList.Clear;
        JsonValue.Free;

        if cbRunOnce.Checked then break;

        qryRabbitmq.Next;
      end;
    except
      on E: Exception do
      begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'processPairs';
        LogResult.DataStream := JSonValue.ToString;
        LogResult.ExcepMsg := E.Message;
        LogResult.Status := 'Failed';
        WriteLog(LogResult);
      end;
    end;
  finally
    qryRabbitMQ.Close;
    qryStaging.Close;
    StrList.Free;
  end;
end;
//BP  Build the insert sql from stringlist key value pair (includes alert_source as added field)
function TfrmTicketAlert.BuildStagingQuery: Boolean;
var
 I: Integer;
 AFields, AValues: String;
 InsertSQL: String;
 AlertType: String;
 TicketID: String;
begin
  try
    Result := True;
    InsertSQL := 'Insert into ticket_alert ';
    if qryStaging.FindField('alert_source') <> nil then
    begin
      AFields := AFields + 'alert_source,';
      AValues := AValues + QuotedStr(QueueName) +','
    end;
    for I := 0 to StrList.Count-1 do
    begin
      if StrList.KeyNames[I] = 'ticket_id' then
      begin
        TicketID := StrList.ValueFromIndex[I];
        AFields := AFields + StrList.KeyNames[I] + ',';
        AValues := AValues + TicketID + ',';
      end else
      //Retrieve Alert type from reference table, if missing, logs the error and skips to next record
      if StrList.KeyNames[I] = 'alert_type_code' then
      begin
        If not GetAlertType(AlertType) then
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'Build staging query';
          LogResult.DataStream := 'alert type code ' + StrList.ValueFromIndex[I] + ' not found ';
          WriteLog(LogResult);
          Memo1.Lines.Add('error inserting staging table record');
          Result := False;
        end else
        begin
          AFields := AFields + StrList.KeyNames[I] + ',';
          AValues := AValues + QuotedStr(AlertType) + ',';
        end;
      end else
      if StrList.KeyNames[I] = 'alert_desc' then
      begin
        AFields := AFields + StrList.KeyNames[I] + ',';
        AValues := AValues + QuotedStr('HighProfileAttributes: ' + StrList.ValueFromIndex[I]) + ',';
      end;
    end;
    Delete(AFields, Length(AFields), 1);
    Delete(AValues, Length(AValues), 1);

    if Result then
    begin
      InsertSQL := InsertSQL + '(' + AFields + ') values (' + AValues  + ')';
      insStagingTable.SQL.Text := InsertSQL;
      insStagingTable.ExecSQL;

      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'BuildStagingQuery';
      LogResult.Status := 'Ticket: ' + TicketID + ' inserted';
      WriteLog(LogResult);
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Build staging query';
      LogResult.ExcepMsg := E.Message;
      LogResult.DataStream := insStagingTable.SQL.Text;
      WriteLog(LogResult);
      Result := True;
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('error inserting staging table records');
    end;
  end;
end;

//BP Alert Type retrieved from Json field
function TfrmTicketAlert.GetAlertType(out AlertType: String): Boolean;
begin
  Result := True;
  qryAlertType.Open;
  if not qryAlertType.EOF then
    AlertType := qryAlertType.FieldByName('code').AsString
  else Result := False;
  qryAlertType.Close;
end;

//BP  Process json strings
procedure TfrmTicketAlert.getData(OrigObj: TJsonObject; Field: String);
var
  jso: TJsonObject;
  jsPair : TJsonPair;
  jsNestedPair: TJsonPair;
  jsoNestedObj: TJsonObject;
  OutputStr: String;
  ObjTitle: String;
  NestedObjTitle: String;
  JsOutputStr : String;
  I:Integer;

//BP  Reads into stringlist key value pair for building staging table sql
procedure GetStagingData(Field, Value: String);
begin
  if qryStaging.FindField(Field) <> nil then
  begin
    JsOutputStr := Field + '=' + Value;
    StrList.Add(JsOutputStr);
  end;
end;

begin
  jspair := OrigObj.Get(Field);
  if JsPair.JsonValue is TJsonString then
  begin
    OutputStr := '   ' + jsPair.JsonString.Value + ': ' + jsPair.JsonValue.Value;
    JsonMemo.Lines.Add(OutputStr);
    if jsPair.JsonString.Value = 'TicketId' then
      GetStagingData('ticket_id', jsPair.JsonValue.Value) else
    if jsPair.JsonString.Value = 'SourceSystem' then
      GetStagingData('alert_type_code', jsPair.JsonValue.Value) else
    if jsPair.JsonString.Value = 'HighProfileAttributes' then
      GetStagingData('alert_desc', jsPair.JsonValue.Value);
  end
end;

procedure TfrmTicketAlert.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmTicketAlert.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  Result := True;
  ADOConn.close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog='
    + aDatabase + ';Data Source=' + aServer;

  connStrLog := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
    ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  Memo1.Lines.Add(connStr);
  ADOConn.ConnectionString := connStr;
  try
   // Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParserUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02-DV;Initial File Name="";Server SPN="";
    ADOConn.open;
    if (ParamCount > 0) then
    begin
      Memo1.Lines.Add('Connected to database');
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'connectDB';
      LogResult.Status := 'Connected to database';
      LogResult.DataStream := connStrLog;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := aServer;
    end;

  except
    on E: Exception do
    begin
      Result := False;
      LogResult.LogType := ltError;
      LogResult.MethodName := 'connectDB';
      LogResult.DataStream := connStrLog;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      StatusBar1.Panels[1].Text := 'Failed';
      Memo1.Lines.Add('Could not connect to DB');
    end;
  end;
end;

// update the rabbitmq record and delete. delete trigger inserts record into the history table
procedure TfrmTicketAlert.DeleteFromMQ(MsgID: integer; Status: string; process: boolean);
begin
  updRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  updRabbitMQ.Parameters.ParamByName('processed').Value := process;
  updRabbitMQ.Parameters.ParamByName('status').Value := Status;
  if updRabbitMQ.ExecSQL > 0 then
    delRabbitMQ.Parameters.ParamByName('MsgID').Value := MsgID;
  delRabbitMQ.ExecSQL;
end;

procedure TfrmTicketAlert.FormCreate(Sender: TObject);
var
  AppVer: string;
begin
  //retrieve queuename
  QueueName := ParamStr(1);
  if QueueName='' then
  begin
    showmessage('Rabbit MQ queue name is required as the first command line parameter');
    Application.Terminate;
  end;
  ProcessINI;
  AppVer := GetAppVersionStr;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status := AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  StatusBar1.Panels[9].Text := AppVer;
  connectDB;
  if uppercase(ParamStr(2)) <> 'GUI' then
  begin
    ProcessPairs;
    application.Terminate;
  end;
end;

function TfrmTicketAlert.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;


function TfrmTicketAlert.ProcessINI: boolean;
var
  TicketAlert: TIniFile;
begin
  try
    TicketAlert := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := TicketAlert.ReadString('Database', 'Server', '');
    aDatabase := TicketAlert.ReadString('Database', 'DB', 'QM');
    ausername := TicketAlert.ReadString('Database', 'UserName', '');
    apassword := TicketAlert.ReadString('Database', 'Password', '');

    flogDir := TicketAlert.ReadString('LogPaths', 'LogPath', '');
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);
  finally
    TicketAlert.Free;
  end;
end;


procedure TfrmTicketAlert.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName := IncludeTrailingBackslash(flogDir) + RunningInstance +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

end.
