program FTPTickets;

uses
  Vcl.Forms,
  System.SysUtils,
  Inifiles,
  GlobalSU in 'GlobalSU.pas',
  uFTPTickets in 'uFTPTickets.pas' {frmFTP};

{$R *.res}

 var
   MyInstanceName: string;
   AIniFile: TInifile;
   Account: String;
begin
  AIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  Account := AIniFile.ReadString('remoteFTP', 'PrivKeyName', '');
  MyInstanceName := ChangeFileExt(ExtractFileName(Application.ExeName), '') + '_' + Account;
  AIniFile.Free;
  if CreateSingleInstance(MyInstanceName) then
  begin
   ReportMemoryLeaksOnShutdown := DebugHook <> 0;
   Application.ShowMainForm := (ParamCount = 1);
   Application.MainFormOnTaskBar := true;
   Application.CreateForm(TfrmFTP, frmFTP);
   if ParamCount = 0 then
     frmFTP.DownloadFiles else
   Application.Run;

  end;
end.
