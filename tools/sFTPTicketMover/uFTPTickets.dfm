object frmFTP: TfrmFTP
  Left = 0
  Top = 0
  Caption = 'FTP Files'
  ClientHeight = 265
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 18
    Top = 17
    Width = 37
    Height = 14
    Caption = 'INI File'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 329
    Top = 16
    Width = 79
    Height = 14
    Caption = 'Script (do.txt)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnDownload: TButton
    Left = 245
    Top = 216
    Width = 129
    Height = 25
    Caption = 'Download Files'
    TabOrder = 0
    OnClick = btnDownloadClick
  end
  object MemoScript: TMemo
    Left = 325
    Top = 32
    Width = 297
    Height = 169
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object MemoIni: TMemo
    Left = 22
    Top = 32
    Width = 290
    Height = 166
    ScrollBars = ssBoth
    TabOrder = 2
  end
end
