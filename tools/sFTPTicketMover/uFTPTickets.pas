unit uFTPTickets;
//-BP QM-
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IniFiles, Vcl.StdCtrls, Vcl.ExtCtrls;

  type
    TLogType = (ltError, ltInfo, ltNotice, ltWarning);
    TLogResults = record
      LogType: TLogType;
      MethodName: String[40];
      Status: String[90];
      ExcepMsg: String[255];
      DataStream: String[255];
  end;

  type
  TfrmFTP = class(TForm)
    btnDownload: TButton;
    MemoScript: TMemo;
    MemoIni: TMemo;
    Label2: TLabel;
    procedure btnDownloadClick(Sender: TObject);
  private
   { Private declarations }
    MyInstanceName: string;
    WinScpPath: string;
    ftpHost: string;
    ActiveDoFile: String;
    LocalPathFTP: String;
    PathForFTP: String;
    PrivateKeyDir: String;
    PrivKeyName: String;
    FTPLogpath: String;
    LogResult: TLogResults;
    procedure clearLogRecord;
    function CreateDOfile(PrivKeyName: String): Boolean;
    procedure WriteLog(LogResult: TLogResults);
    procedure PullFromServer(AccountName: String);
    function ExecuteProcess(const FileName, Params: string; Folder: string;
      WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
      var ErrorCode: integer): Boolean;
  public
    { Public declarations }
    procedure DownloadFiles;
    procedure ReadINI;
  end;


var
  frmFTP: TfrmFTP;

implementation

{$R *.dfm}

procedure TfrmFTP.btnDownloadClick(Sender: TObject);
begin
  DownloadFiles;
end;

procedure TfrmFTP.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmFTP.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FTPLogPath) + 'WinFTP' + '-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmFTP.ReadINI;
var
  IniFile: TIniFile;
begin
  try
    if ParamCount > 0  then
      MemoIni.Lines.LoadFromFile(ChangeFileExt(Application.ExeName, '.ini'));
   IniFile :=  TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
   ftpHost := IniFile.ReadString('remoteFTP', 'RemoteFtpHost', 'sftp.mcview.com/ -hostkey="ssh-rsa 2048 2EZdNsDac9uazNmBhc1EblcxN0Kpn8OeBNEdHAAw/gk="');
   {directory of files on server}
   PathForFTP := IniFile.ReadString('remoteFTP', 'PathForFTP', 'USAS');

   {local folder where files are to be downloaded}
   LocalPathFTP := IniFile.ReadString('remoteFTP', 'LocalPathFTP', 'C:\TicketDownload');
   LocalPathFTP := IncludeTrailingBackSlash(LocalPathFTP);

   WinScpPath := IniFile.ReadString('remoteFTP', 'WinScpPath', 'C:\Program Files (x86)\WinSCP\');
   WinScpPath := IncludeTrailingBackSlash(WinScpPath);

   PrivateKeyDir := IniFile.ReadString('remoteFTP', 'PrivateKeyDir', 'C:\KeyFiles');
   PrivateKeyDir := IncludeTrailingBackSlash(PrivateKeyDir);

   {private key name is also the account name}
   PrivKeyName := IniFile.ReadString('remoteFTP', 'PrivKeyName', '');
    if not FileExists(PrivateKeyDir+PrivKeyName) then
      raise Exception.Create('Private Key not found');
   FtpLogPath := IncludeTrailingBackslash(IniFile.ReadString('remoteFTP', 'ftpLogPath', 'C:\QM\Logs'));
   ForceDirectories(FtpLogPath);
  finally
    IniFile.Free;
  end;
end;

procedure TfrmFTP.DownloadFiles;
begin
  try
    ReadIni;
    LogResult.LogType := ltInfo;
    LogResult.Status := '--------------------- StartUp DownloadFiles ----------------------------------';
    WriteLog(LogResult);
    If not CreateDOfile(PrivKeyName) then
      raise Exception.Create('Unable to create WinScp script');
    if ParamCount > 0  then
      MemoScript.Lines.LoadFromFile(WinScpPath + 'do.txt');
    {Initates file transfer}
    PullFromServer(PrivKeyName);
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'DownloadFile';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
      if ParamCount=0 then
         ShowMessage(E.Message);
    end
  end;
end;

function TfrmFTP.CreateDOfile(PrivKeyName: String): Boolean;
//builds and saves a script file to process a private key
const
  LINE1 = 'option batch on';
  LINE2 = 'option confirm off';
  LINE3 = 'open ';
  LINE4 = 'cd ';
  LINE5 = 'get ';
  LINE6 = 'exit';
  DO_FILE = 'do.txt';
var
  doList: TStringList;
  HostPrefix: String;
  PrivateKeyStr: String;
begin
  Result := True;
  try
    try
      ActiveDoFile := DO_FILE;
      doList := TStringList.Create;
      HostPrefix := 'sftp://' + PrivKeyName + '@';
      PrivateKeyStr :=  ' -privatekey=' + '"' + PrivateKeyDir + PrivKeyName + '.ppk' + '"';
      doList.add(LINE1);
      doList.add(LINE2);
      doList.add(LINE3 + HostPrefix + FTPHost + PrivateKeyStr);


      //get -delete (with wild card),
      //"gets file 1, then deletes it on success, gets file 2, deletes it on success, etc"
      // -latest parameter to synchronize
      doList.Add(LINE4 + PathForFTP);
      doList.Add(LINE5 +  '-delete -neweronly "*.txt"' + ' "' + LocalPathFTP + '"'  );
      doList.Add(LINE6);
      doList.SaveToFile(IncludeTrailingBackSlash(WinScpPath) + ActiveDoFile, TEncoding.UTF8);
    finally
      DoList.Free;
    end;
  except
    Result := False;
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed';
    LogResult.ExcepMsg := 'Error Creating Do File for PrivKey' + PrivKeyName + ': ' + SysErrorMessage(Error);
    LogResult.MethodName := 'CreateDoFile';
    WriteLog(LogResult);
  end;
end;


Procedure TfrmFTP.PullFromServer(AccountName:String);
  {synchronizes file transfer to local directory for a private key file and
  deletes each file from server on success; createprocess to run winscp.bat
  with do.txt script file as parameter and myscript.log as winscp log}
var
  FileName, Parameters, WorkingFolder: string;
  OK: Boolean;
  Error: integer;
const
  WIN_SCP = 'WinSCP.bat';
begin

  FileName := IncludeTrailingBackslash(WinScpPath) + WIN_SCP;
  Parameters := ActiveDoFile;

  LogResult.LogType := ltInfo;
  LogResult.Status := 'Calling ExecuteProcess for ' + AccountName + 'download';
  LogResult.MethodName := 'PullFromServer';
  WriteLog(LogResult);

  OK := ExecuteProcess(FileName, Parameters, WorkingFolder, true, False, true, Error);

  if OK then
  begin
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Download for' + PrivKeyName + 'to Client Complete';
    LogResult.DataStream:= 'FileName: '+FileName+ ' Param: '+ Parameters+ ' Folder: '+ WorkingFolder+' Error: '+IntToStr(Error);
    LogResult.MethodName := 'PullFromServer';
    WriteLog(LogResult);
  end else
  begin
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed for ' + PrivKeyName + 'download';
    LogResult.ExcepMsg := 'Error: ' + SysErrorMessage(Error);
    LogResult.MethodName := 'PullFromServer';
    WriteLog(LogResult);
  end;
end;

function TfrmFTP.ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
    StartupInfo.dwFlags := CREATE_NO_WINDOW;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
    repeat
      Application.ProcessMessages;
    until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;


end.
