program Exactix;

uses
  Vcl.Forms,
  ExactixMain in 'ExactixMain.pas' {frmExactix};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmExactix, frmExactix);
  Application.Run;
end.
