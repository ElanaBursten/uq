unit ExactixMain;
//qm-1075
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,System.json,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, REST.Types, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, IdIntercept,
  IdLogBase, IdLogEvent, IdMessage, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP, Data.Win.ADODB,
  Data.DB, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TResponseLogEntry = record
    LocateID: integer;
    ResponseDate: String[22];
    CallCenter: String[12];
    Status: String[5];
    ResponseSent: String[15];
    Sucess: boolean;
    Reply: String[40];
    ResponseDateIsDST: String[2];
  end;  //TResponseLogEntry

type
  TReturnedResponse = record
    Result:string[14];
    ResultCode:string[4];
    ResultMessage:string[40];
  end;


type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[60];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;  //TLogResults

type
  TSplitStatus = record
    NumType: String;
    AlphaType: String;
  end;  //TLogResults
  TfrmExactix = class(TForm)
    Memo1: TMemo;
    btnSendResponse: TButton;
    StatusBar1: TStatusBar;
    spGetPendingResponses: TADOStoredProc;
    ADOConn: TADOConnection;
    insResponseLog: TADOQuery;
    delResponse: TADOQuery;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdMessage1: TIdMessage;
    IdLogEvent1: TIdLogEvent;
    qryEMailRecipients: TADOQuery;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    qryTicketInfo: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btnSendResponseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;

    LogResult: TLogResults;
    insertResponseLog:TResponseLogEntry;
    flogDir: wideString;

    FRespondto: string;
    fCallCenter: string;
    FPosturl: string;
    fOC_Code: string;
    SplitStatus: TSplitStatus;
    ReturnedResponse:TReturnedResponse;
    function getStatus(status:string):TSplitStatus;
    function connectDB: boolean;

    function ProcessINI: boolean;
    procedure clearLogRecord;

    function AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
    function getData(JsonString: String; Field: String): String;
    function GetAppVersionStr: string;
    function deleteMultiQueueRecord(RespondTo: string; LocateID: integer): boolean;
    procedure SetUpRecipients;
    { Private declarations }
  public
    { Public declarations }

    property CallCenter: string read fCallCenter write fCallCenter;
    property OC_Code:string read fOC_Code write fOC_Code; //QM-552
    property Respondto: string read FRespondto write FRespondto;
    property logDir: wideString read flogDir;
    property Posturl: string read FPosturl write FPosturl;
    procedure WriteLog(LogResult: TLogResults);
  end;

var
  frmExactix: TfrmExactix;

implementation
uses  dateutils, StrUtils, iniFiles;
{$R *.dfm}

function TfrmExactix.AddResponseLogEntry(insertResponseLog: TResponseLogEntry): boolean;
begin
  with insResponseLog.Parameters do
  begin
    ParamByName('LocateID').Value :=           insertResponseLog.LocateID;
    ParamByName('ResponseDate').Value :=       insertResponseLog.ResponseDate;
    ParamByName('CallCenter').Value :=         insertResponseLog.CallCenter;
    ParamByName('Status').Value :=             insertResponseLog.Status;
    ParamByName('ResponseSent').Value :=       insertResponseLog.ResponseSent;
    ParamByName('Success').Value :=            insertResponseLog.Sucess;
    ParamByName('Reply').Value :=              insertResponseLog.Reply;

  end;
  result := (insResponseLog.ExecSQL>0);
end;


procedure TfrmExactix.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConn.Close;
    Action:= caFree;
end;

procedure TfrmExactix.FormCreate(Sender: TObject);
var
  AppVer:string;
begin
  Respondto := ParamStr(1);
  CallCenter:= ParamStr(2);
  AppVer:='';
  ProcessINI;
  AppVer  := GetAppVersionStr;
  if (Respondto='') or (CallCenter='')  then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'processParams';
    LogResult.Status := 'MISSING Params';
    LogResult.DataStream:='RespondTo: '+ ParamStr(1) + '  CallCenter: '+ParamStr(2);
    WriteLog(LogResult);
  end
  else
  begin
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'processParams';
    LogResult.DataStream:= ParamStr(1) + '  '+ParamStr(2)+'  '+ParamStr(3);
    WriteLog(LogResult);
  end;
  connectDB;
  SetUpRecipients;
  LogResult.MethodName := 'GetAppVersionStr';
  LogResult.Status  :=AppVer;
  StatusBar1.panels[9].Text:= AppVer;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  statusbar1.Panels[10].Text:= Respondto;

  if ParamStr(3)<>'GUI' then
  begin
    btnSendResponseClick(Sender);
    application.Terminate;
  end;
  StatusBar1.Refresh;
end;

function TfrmExactix.getStatus(status: string): TSplitStatus;
var
  i :integer;
begin
  for i := 1 to length(status) do
  begin
    if status[i] in ['0'..'9'] then
     result.NumType:= result.NumType+status[i]
     else
     result.AlphaType:= result.AlphaType+status[i];
  end;
end;

function TfrmExactix.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
    result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo,  // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi,  // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmExactix.getData(JsonString, Field: String): String;
var
  JSonValue: TJSonValue;
begin
  Result :='';

  // create TJSonObject from string
  JsonValue := TJSonObject.ParseJSONValue(JsonString);

  Result := JsonValue.GetValue<string>(Field);
  JsonValue.Free;
end;

function TfrmExactix.ProcessINI: boolean;
var
  SunshineUni: TIniFile;
begin
  try
    SunshineUni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    aServer := SunshineUni.ReadString('Database', 'Server', '');
    aDatabase := SunshineUni.ReadString('Database', 'DB', 'QM');
    ausername := SunshineUni.ReadString('Database', 'UserName', '');
    apassword := SunshineUni.ReadString('Database', 'Password', '');
    flogDir    :=SunshineUni.ReadString('LogPaths', 'LogPath', '');

    OC_Code := SunshineUni.ReadString('EMailAlerts', 'RecipientsOC_Code', '3008');   //QM-552

    with IdMessage1.From do  //QM-552
    begin
      Address:=SunshineUni.ReadString('EMailAlerts', 'FromAddress', '');
      Domain:=SunshineUni.ReadString('EMailAlerts', 'FromDomain', '');
      Name:=SunshineUni.ReadString('EMailAlerts', 'FromName', '');
      Text:=SunshineUni.ReadString('EMailAlerts', 'FromText', '');
      User:=SunshineUni.ReadString('EMailAlerts', 'FromUser', '');
    end;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    SunshineUni.Free;
  end;
end;

procedure TfrmExactix.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName :=IncludeTrailingBackslash(FLogDir) + RespondTo +'_SunshineUni-' + FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmExactix.btnSendResponseClick(Sender: TObject);

const
  PARSER = 'PARSER'; //qm-657
  SKIP_STATUS = 'SKIP';
  DBL_QUOTES = '"';

  CONMAEP  =  'CONMAEP';
  ONGOCONT =  'ONGOCONT';
  SMDT     =  'SMDT';
  RSREASON =  'RSREASON';
  CONPHNE  =  'CONPHNE';
  TLKPOS   =  'TLKPOS';

 SUNSHINE_BODY=
 '{'+
 '    "OneCallCenterCode": "%s", '+  //One_Call_Center
 '    "UserID":"%s", '+            //user_name
 '    "Password":"%s", '+           //password
 '    "ServiceAreaCode":"%s", '+   //Service_Area_Code
 '    "UtilityType":"%s", '+         //utility_ytpe
 '    "TicketNumber":"%s", '+   //Ticket_Number
 '    "ResponseCode":"%s", '+           //Outgoing_status numeric
 '    "Comment": "%s" '+ //PubLocateNote
'}';


 INDIAN_BODY_BODY=
 '{'+
 '    "OneCallCenterCode": "%s", '+  //One_Call_Center
 '    "UserID":"%s", '+            //user_name
 '    "Password":"%s", '+           //password
 '    "ServiceAreaCode":"%s", '+   //Service_Area_Code
 '    "UtilityType":"%s", '+         //utility_ytpe
 '    "TicketNumber":"%s", '+   //Ticket_Number
 '    "ResponseCode":"%s", '+           //Outgoing_status numeric
 '    "Comment": "%s", '+
 '    %s '+
'}';



//https://sandbox-v31.exactix811.com/api/external/positiveresponse/IN811
//required for 3C, 3F, 3G
ONGOING =           //qm-877
   ' "Data": { '+
   '     "AttemptedContactDateTime": "%s", '+        //ContactDateTime
   '     "AttemptedContactMethod": "%s", '+          //CONMAEP  ContactMethod
   '     "NameOfPersonAttemptedToContact": "%s", '+  //TLKPOS
   '     "PhoneOrEmailOfPerson": "%s", '+            //CONPHNE
   '     "LocatorName": "%s", '+                      //LocatorName
   '     "PhoneOrEmailOfLocator": "%s",'+             //LocatorPhone
   '     "ProposedDateTimeOfCompletion": "%s" '+     //SMDT  ProposedDate
   '} ';


  ONGOING_Array: TArray<String> = ['3C', '3F', '3G']; // Ongoing statuses     //qm-877
var
  OneCallCenterCode: string;
  UserID: string;
  Password: string;
  ServiceAreaCode: string;
  UtilityType: string;
  TicketNumber: string;
  ResponseCode: string; // Outgoing_status numeric
  ResponseCategory: string; // Outgoing_status alpha
  Comment: string;

  OutStatus: string;
  LocateID: integer;
  URL: string;
  Call_Center: string; // one_call_center
  locate_status: string;

  ResponseBody: string;
  cntGood, cntBad: integer;
  requestID: string;
  AddedBy: string; // qm-544 sr

  InfoType: string;
  Info: string;
  ContactReason:string;
  ProposedDate: string;
  ContactMethod: string;
  ContactPerson: string;
  ContactDateTime: string;
  OngoingContact: string;
  ContactPhone: string;
  LocatorPhone: string;
  LocatorName: string;

  ongoingPayload:string;
  function StringInArray(const S: string;
    const SArray: array of string): boolean;
  var
    i: integer;
  begin
    Result := False;
    for i := 0 to length(SArray) - 1 do
      if SameText(S, SArray[i]) then
      begin
        Result := True;
        Break;
      end;
  end;

begin
  OutStatus := '';
  TicketNumber := '';
  LocateID := 0;
  ResponseBody := '';
  URL := '';
  Call_Center := '';
  requestID := '';
  locate_status := '';
  AddedBy := ''; // qm-544 sr
  UtilityType := '';
  ServiceAreaCode := '';

  InfoType := '';
  Info := '';
  ContactReason:='';
  ProposedDate:='';
  ContactMethod :='';
  ContactDateTime := '';   //closed_date
  OngoingContact:='';
  ContactPhone:='';
  LocatorPhone := '';
  LocatorName := '';
  ongoingPayload := '';
  with spGetPendingResponses do
  begin
    Parameters.ParamByName('@RespondTo').Value := RespondTo;
    Parameters.ParamByName('@CallCenter').Value := CallCenter;
    Parameters.ParamByName('@ParsedLocatesOnly').Value := 0;
    Parameters.ParamByName('@ResponseDelayMinutes').Value := 0;
    open;
    while not eof do
    begin
      ReturnedResponse.Result := '';
      ReturnedResponse.ResultCode := '';
      ReturnedResponse.ResultMessage := '';
      locate_status := Trim(FieldByName('status').AsString);
      LocateID := FieldByName('locate_id').AsInteger;

      OneCallCenterCode := FieldByName('One_Call_Center').AsString;
//      ContactDateTime:= FieldByName('closed_date').AsString;
      UserID := FieldByName('user_name').AsString;
      Password := FieldByName('password').AsString;
      If FieldByName('utility_type').AsString = 'phon' then
        UtilityType := 'telephone'
      else if FieldByName('utility_type').AsString = 'ugas' then
        UtilityType := 'gas'
      else
        UtilityType := FieldByName('utility_type').AsString;

      AddedBy := Trim(FieldByName('added_by').AsString);
      Comment := FieldByName('ResponderComment').AsString;
      TicketNumber := Trim(FieldByName('ticket_number').AsString);
      ServiceAreaCode := (FieldByName('Client_Code').AsString);
      URL := Trim(FieldByName('URL').AsString);
      Call_Center := Trim(FieldByName('one_call_center').AsString);
      OutStatus := AnsiUpperCase(Trim(FieldByName('outgoing_status').AsString));
      ResponseCode := OutStatus; // getStatus(OutStatus).NumType;

      ContactDateTime:= FieldByName('ResponderComment').AsString;

      if ((CallCenter = '1851') and (StringInArray(OutStatus, ONGOING_Array)))
      then
      begin   //qm-877
        try
          qryTicketInfo.Parameters.ParamByName('LocateID').Value := LocateID;
          qryTicketInfo.open;
          while not(qryTicketInfo.eof) do
          begin
            InfoType := qryTicketInfo.FieldByName('info_type').AsString;
            CASE IndexStr(InfoType, [CONMAEP, ONGOCONT, SMDT, RSREASON, CONPHNE,
              TLKPOS]) of
              0:  //CONMAEP  ContactMethod
                begin
                  ContactMethod := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                END;
              1:  //ONGOCONT   OngoingContact
                begin
                  OngoingContact := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                end;
              2:   //SMDT  //ProposedDate
                begin
                  ProposedDate := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                end;
              3:  //RSREASON
                begin
                  ContactReason := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                END;
              4:  //CONPHNE   ContactPhone
                begin
                  ContactPhone := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                end;
              5:  //TLKPOS  ContactPerson
                begin
                  ContactPerson := qryTicketInfo.FieldByName('Info').AsString;
                  ContactDateTime := qryTicketInfo.FieldByName
                    ('ContactDateTime').AsString;
                  LocatorPhone := qryTicketInfo.FieldByName
                    ('LocatorPhone').AsString;
                  LocatorName := qryTicketInfo.FieldByName
                    ('LocatorName').AsString;
                end;
            end; // case

            qryTicketInfo.Next;


          end; //while not(qryTicketInfo.eof) do
        finally
          ongoingPayload:=  Format(ONGOING,[ContactDateTime,ContactMethod,ContactPerson, ContactPhone,LocatorName, LocatorPhone, ProposedDate ]);
          qryTicketInfo.Close;
        end;
      end; //if ((CallCenter = 'IN811')

      if AddedBy <> PARSER then  //qm-544 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-544 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed NON-Parser_Added Response';
        WriteLog(LogResult);

        next;
        continue;
      end;

      if OutStatus = SKIP_STATUS then  //qm-543 sr
      begin  //cleans out multiQ that are not inserted by parser
        deleteMultiQueueRecord(RespondTo,LocateID);    //qm-543 sr
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.Status := 'Removed Skipped Outstatus Response '+OutStatus;
        LogResult.DataStream := 'Ticket no: '+ ticketNumber+ ' LocateID: '+IntToStr(LocateID);
        WriteLog(LogResult);

        next;
        continue;
      end;

      if ((CallCenter = '1851') and (StringInArray(OutStatus, ONGOING_Array)))
      then
      begin
        ResponseBody  := format(INDIAN_BODY_BODY,[OneCallCenterCode,UserID,password,ServiceAreaCode, UtilityType,ticketNumber, ResponseCode, Comment,ongoingPayload]);
      end
      else
      ResponseBody  := format(SUNSHINE_BODY,[OneCallCenterCode,UserID,password,ServiceAreaCode, UtilityType,ticketNumber, ResponseCode, Comment]);

      RESTRequest1.Params.Clear;
      memo1.Lines.Add('sendResponse: '+ResponseBody);

      RESTRequest1.ClearBody;
      RESTClient1.ContentType := 'application/json';

      RESTClient1.BaseURL :=  URL;
      RESTRequest1.AddBody(ResponseBody, ctAPPLICATION_JSON);

      RESTRequest1.Method := TRESTRequestMethod.rmPost; //status change


      try
        RESTRequest1.Execute;
      except
        on E: Exception do
        begin
          Memo1.Lines.Add(E.Message);
          LogResult.LogType := ltError;
          LogResult.ExcepMsg := E.Message+ ' Body Sent ' + ResponseBody;;
          LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
          LogResult.DataStream := RESTResponse1.Content;
          WriteLog(LogResult);
          next;
          continue;
        end;
      end;  //try-except


      ReturnedResponse.Result:=GetData(RESTResponse1.Content, 'Result');
      ReturnedResponse.ResultCode:=GetData(RESTResponse1.Content, 'ResultCode');
      ReturnedResponse.ResultMessage:=LeftStr(GetData(RESTResponse1.Content, 'ErrorMessage'),40);


      Memo1.Lines.Add('-------------RESTResponse------------------------');
      Memo1.Lines.Add('RESTResponse: ' + IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText);
      Memo1.Lines.Add('RESTResponse1.Content: '+RESTResponse1.Content);
      Memo1.Lines.Add('-------------------------------------------------');

      if (RESTResponse1.StatusCode > 199) and (RESTResponse1.StatusCode < 300) then
      begin
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response sent '+ResponseBody;
        WriteLog(LogResult);

        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'Process Ticket locate';
        LogResult.DataStream := 'Response returned '+RESTResponse1.Content;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' +  RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
        WriteLog(LogResult);
        inc(cntGood);

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        if StrToInt(ReturnedResponse.ResultCode)>0 then
        insertResponseLog.Sucess              := True
        else
        insertResponseLog.Sucess              := False;
        insertResponseLog.Reply               := ReturnedResponse.ResultCode+' '+ReturnedResponse.ResultMessage;
        insertResponseLog.ResponseDateIsDST   := '';
        AddResponseLogEntry(insertResponseLog);
        {
1  Successful    delete
0  Canceled: The response was recorded but the Ticket has been canceled.  delete
-1 Failed: The Ticket Number, Service Area Code, Response, or Utility Type is invalid or not found. The reason is included in the ErrorMessage field of the response (if supported by the message format being used).
-2 UserPassInvalid: The UserID or Password is invalid.
-3 ServerError: A Server error occurred. The details of the error have been loggedfor technical support and may also be included in the response. The response message can be re-submitted but should wait several minutes before re-attempting.
}
        if StrToInt(ReturnedResponse.ResultCode)>0 then
        deleteMultiQueueRecord(RespondTo,LocateID);
      end  //StatusCode > 199 <300   good
      else
      begin
        inc(cntBad);
        LogResult.LogType := ltError;
        LogResult.MethodName := 'Process';
        LogResult.ExcepMsg := ResponseBody;
        LogResult.Status := IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber;
        LogResult.DataStream := 'Returned ' + RESTResponse1.Content;
        WriteLog(LogResult);

        //QM-552 Add EMail notices to all Delphi Responders
        IdMessage1.Body.Add('Failed request no '+IntToStr(cntBad)+' at '+formatdatetime('yyyy-mm-dd hh:mm:ss', Now));
        IdMessage1.Body.Add(IntToStr(RESTResponse1.StatusCode) + ' ' + RESTResponse1.StatusText + ' TicketNo: ' +ticketNumber);
        IdMessage1.Body.Add('Sent '+ResponseBody);
        IdMessage1.Body.Add('Returned ' + RESTResponse1.Content);
        IdMessage1.Body.Add('-----------------------------------------------------');

        insertResponseLog.LocateID            := LocateID;
        insertResponseLog.ResponseDate        := formatdatetime('yyyy-mm-dd hh:mm:ss', Now);
        insertResponseLog.CallCenter          := CallCenter;
        insertResponseLog.status              := locate_status;
        insertResponseLog.ResponseSent        := OutStatus;
        if StrToInt(ReturnedResponse.ResultCode)>0 then
        insertResponseLog.Sucess              := True
        else
        insertResponseLog.Sucess              := False;
        insertResponseLog.Reply               := ReturnedResponse.ResultMessage;
        insertResponseLog.ResponseDateIsDST   := '';

        AddResponseLogEntry(insertResponseLog);
        Memo1.Lines.Add('Returned Content' + RESTResponse1.Content);

        Next;
        Continue;
      end;  //other bad but left in multi q
      Next;  //response
    end; //while-not EOF

  end; //with

  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Send Responses to Server';
  LogResult.Status := 'Process Complete';
  LogResult.DataStream := IntToStr(cntGood)+' successfully returned '+IntToStr(cntBad)+' failed of '+IntToStr(cntBad+cntGood);
  WriteLog(LogResult);
	if cntBad>0 then
	begin
		idSMTP1.Connect;
		idSMTP1.Send(IdMessage1);
		idSMTP1.Disconnect();

		LogResult.LogType := ltInfo;
		LogResult.MethodName := 'Sending email';
		LogResult.Status := 'Some responses failed';
		LogResult.DataStream :=  'Sent email to '+idMessage1.Recipients.EMailAddresses;
		WriteLog(LogResult);
	end; //if cntBad>0 then

  StatusBar1.panels[3].Text:= IntToStr(cntGood);
  StatusBar1.panels[5].Text:= IntToStr(cntBad);
  StatusBar1.panels[7].Text:= IntToStr(cntBad+cntGood);
end;

procedure TfrmExactix.SetUpRecipients;  //QM-552
begin
  try
    idMessage1.Subject:=ExtractFileName(Application.ExeName)+' '+RespondTo+' '+CallCenter;
    qryEMailRecipients.Parameters.ParamByName('OCcode').Value:=OC_Code;
    qryEMailRecipients.Open;
    if not qryEMailRecipients.eof then
    idMessage1.Recipients.EMailAddresses:=qryEMailRecipients.FieldByName('responder_email').AsString
    else
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'SetUpRecipients';
      LogResult.Status:= 'No recipients to send email to';
      WriteLog(LogResult);
   end;
  finally
    qryEMailRecipients.close;
  end;
end;


function TfrmExactix.deleteMultiQueueRecord(RespondTo:string;LocateID:integer):boolean;
begin
  delResponse.Parameters.ParamByName('respond_to').Value :=  Respondto;
  delResponse.Parameters.ParamByName('LocateID').Value := LocateID;

  memo1.Lines.Add(delResponse.SQL.Text);

  try
   delResponse.ExecSQL;   //commented out for testing
  except on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'delResponse';
      LogResult.DataStream := delResponse.SQL.Text;
      LogResult.ExcepMsg:=e.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmExactix.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmExactix.connectDB: boolean;
var
  connStr, connStrLog: String;

begin
  result := True;
  ADOConn.Close;
  connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

  connStrLog  := 'Provider=SQLNCLI11.1;Password=Not Displayed ' + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;


    Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try

      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        Memo1.Lines.Add('Connected to database');
        LogResult.LogType := ltInfo;
        LogResult.MethodName := 'connectDB';
        LogResult.Status := 'Connected to database';
        LogResult.DataStream := connStrLog;
        WriteLog(LogResult);
      end;

    except
      on E: Exception do
      begin
        result := False;
        LogResult.LogType := ltError;
        LogResult.MethodName := 'connectDB';
        LogResult.DataStream := connStrLog;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
        Memo1.Lines.Add('Could not connect to DB');
      end;
    end;
end;

end.
