# ticket_transfer.py
# Created: 22 Jan 2002, Hans Nowak (original name emailsender.py)
# Last update: 29 Jan 2002, Hans Nowak

import ticket_transfer_config as config
import os
import smtplib
import time
import sys
import string
import ftplib
from classes import *

__version__ = "1.4"

class TicketTransfer:

    def __init__(self):
        self.ftpcache = {}
        print "Ticket Transfer", __version__, "started"

    def readdir(self, dir):
        """ Scan config.SRC_DIR for files. Return the list of filenames
            found. """
        shortnames = os.listdir(dir)
        fullnames = [os.path.join(dir, name) for name in shortnames]
        print len(shortnames), "files found in", dir
        return fullnames

    def readlog(self, logfile):
        """ Read config.LOGFILE to construct a list of files already
            processed. """
        try:
            f = open(logfile, "r")
        except IOError:
            print >> sys.stderr, "No log file found. A new one is created."
            self.log = {}
            return
        lines = f.readlines()
        f.close()

        # Filenames extracted from the log file are stored in a dictionary.
        # The list of logged files can be had through self.log.keys(), while
        # a certain file's date can be had through self.log[filename].
        self.log = {}
        for line in lines:
            line = string.strip(line)
            if line:
                filename, date = string.split(line, "\t")
                filename = string.lower(filename)
                self.log[filename] = date

        print len(self.log), "files read from log file."

    def writelog(self, logfile, filename):
        """ Write the given filename to the log file, using the current time
            and date. """
        path, filename = os.path.split(filename)    # do not use path
        f = open(logfile, "a+")
        timestamp = time.asctime(time.localtime(time.time()))
        s = "%s\t%s\n" % (filename, timestamp)
        f.write(s)
        f.close()

    def sendmail(self, emaildest, filename):
        """ Send <filename>, which is assumed to be a text file, to
            config.EMAIL. Message body will be the text of the filename. """
        f = open(filename, "rb")    # opened as binary because I'll read
        data = f.read()             # all data as one chunk
        f.close()

        # Remove any funny characters from message text
        for key, value in config.SPECIAL.items():
            if key in data:
                data = string.replace(data, key, value)

        # Prepare message
        fromaddr = config.SENDER
        toaddrs = string.split(emaildest.address, ",")
        shortname = os.path.split(filename)[1]
        msg = "From: %s\r\nTo: %s\r\nSubject:%s\r\n\r\n" % (
         fromaddr, string.join(toaddrs, ", "),
         "TicketTransfer: <%s>" % (shortname))
        msg = msg + data

        smtp = smtplib.SMTP(config.SMTP_SERVER)
        print "Sending", filename, "to", emaildest.address, "...",
        smtp.sendmail(fromaddr, toaddrs, msg)
        print "OK"

    def sendftp(self, ftpdest, filename):
        """ Upload <filename>, which is assumed to be a text file, to
            the configured FTP server. """
        f = open(filename, "rb")
        shortname = os.path.split(filename)[1]

        # We cannot remove special characters here:
        #for key, value in config.SPECIAL.items():
        #    if key in data:
        #        data = string.replace(data, key, value)

        # If possible, get an existing FTP connection from the "FTP cache",
        # so we don't have to log in again for every file found
        key = ftpdest.host + "/" + ftpdest.login
        if self.ftpcache.has_key(key):
            ftp = self.ftpcache[key]
        else:
            # log on to FTP server
            ftp = ftplib.FTP(ftpdest.host)
            ftp.login(ftpdest.login, ftpdest.password)
            print ftp.getwelcome()
            ftp.cwd(ftpdest.dir)   # go to desired directory
            self.ftpcache[key] = ftp

        print "Uploading", filename, "...",
        ftp.storbinary("STOR " + shortname, f)
        print "OK"

    def move(self, filename):
        """ Move file from config.SRC_DIR to config.DEST_DIR. """
        # only move files if config.DEST_DIR is set. if it's an empty
        # string, we don't do anything.
        if config.DEST_DIR:
            path, shortname = os.path.split(filename)
            target = os.path.join(config.DEST_DIR, shortname)
            os.rename(filename, target)

    def go(self):
        """ Do everything we need to do, once. """
        for source in config.SOURCES:
            filenames = self.readdir(source.dir)
            self.readlog(source.logfile)
            sent = skipped = 0
            self.ftpcache = {}

            for filename in filenames:
                path, shortname = os.path.split(filename)
                shortname = string.lower(shortname)
                # if these are in the log file, don't process them; I assume
                # they do need to be moved, though
                if self.log.has_key(shortname):
                    #print "File", filename, "ignored (already in log file)"
                    skipped = skipped + 1
                else:
                    for dest in source.destinations:
                        if isinstance(dest, EmailDestination):
                            self.sendmail(dest, filename)
                        if isinstance(dest, FTPDestination):
                            self.sendftp(dest, filename)
                    self.writelog(source.logfile, filename)
                    sent = sent + 1
                self.move(filename)
            print "Process completed."
            print "Total files sent =", sent
            print "Total files skipped =", skipped

            # clear ftpcache
            for ftpobj in self.ftpcache.items():
                try:
                    ftpobj.quit()
                except:
                    pass
            self.ftpcache = {}

    def wait(self):
        """ Wait for config.WAIT seconds. """
        time.sleep(config.WAIT)

    def run(self):
        """ Run the script. Depending on whether config.RUNONCE is set, this
            either runs once, or infinitely. """
        if config.RUNONCE:
            self.go()
        else:
            while 1:
                self.go()
                self.wait()


if __name__ == "__main__":

    es = TicketTransfer()
    es.run()

