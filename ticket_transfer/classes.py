# classes.py
# Helper classes for configuration file.
# Created: 15 Mar 2002, Hans Nowak
# Last update: 15 Mar 2002, Hans Nowak

class Source:
    def __init__(self, dir="", logfile="", destinations=()):
        self.dir = dir
        self.logfile = logfile
        self.destinations = destinations

class EmailDestination:
    def __init__(self, address="", **other):
        self.address = address
        self.__dict__.update(other)

class FTPDestination:
    def __init__(self, host="", login="", password="", dir=""):
        self.host = host
        self.login = login
        self.password = password
        self.dir = dir


