# ticket_transfer_config.py
# Configuration file for ticket_transfer.py.
# 22 Jan 2002, Hans Nowak

from classes import *   # mandatory

WAIT = 60
# Wait this number of seconds, then try again

RUNONCE = 1
# Run the script once, or loop infinitely.

DEST_DIR = ''
# Target directory; files from SRC_DIR are moved here after being processed.
# Files will be moved if this is set. If it's an empty string, nothing will
# be moved.

SOURCES = [
    Source(dir="D:\\blms\\tickarc\\fwp1", logfile="c:\\tickettransfer\\fwp1_log.txt", destinations=[
	FTPDestination("192.168.1.2", "ticketupload", "ti2342", "fwp1"),
    ]),
    Source(dir="D:\\blms\\tickarc\\fwp2", logfile="c:\\tickettransfer\\fwp2_log.txt", destinations=[
	FTPDestination("192.168.1.2", "ticketupload", "ti2342", "fwp2"),
    ]),
    Source(dir="D:\\blms\\tickarc\\fwp3", logfile="c:\\tickettransfer\\fwp3_log.txt", destinations=[
	FTPDestination("192.168.1.2", "ticketupload", "ti2342", "fwp3"),
    ]),
]

# Sender of the email, appearing in the email's "From:" header.
SENDER = "sender@sender.com"

# SMTP server information
SMTP_SERVER = "smtp.earthlink.net"

# Handling of special characters. A dictionary with the special character as
# key, and its replacement as value.
SPECIAL = {
    chr(12): "~~~~~",
}


