program FaxStatusUpdate;

{$APPTYPE CONSOLE}

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  ActiveX,
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  ReportConfig in '..\reportengine\ReportConfig.pas',
  FaxStatusUpdateDMu in 'FaxStatusUpdateDMu.pas' {FaxStatusUpdateDM: TDataModule},
  UQDbConfig in '..\common\UQDbConfig.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  QMConst in '..\client\QMConst.pas';

begin
  CoInitialize(nil);
  with TFaxStatusUpdateDM.Create(nil) do
    try
      Run;
    finally
      Free;
    end;
  CoUninitialize;

  // The package list for deployment is:
  // rtl;vcl;vclx;dbrtl;vcldb;rbRCL1417;rbCT1417;rbDAD1417;adortl

end.

