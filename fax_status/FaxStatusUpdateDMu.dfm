object FaxStatusUpdateDM: TFaxStatusUpdateDM
  OldCreateOrder = False
  Height = 316
  Width = 379
  object SetSuccess: TADOCommand
    CommandText = 
      'update fax_message'#13#10'  set fax_status = '#39'Success'#39','#13#10'      complet' +
      'e_date = :date'#13#10'  where fm_id = :id'
    Connection = Conn
    Parameters = <
      item
        Name = 'date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 72
    Top = 32
  end
  object SetFailure: TADOCommand
    CommandText = 
      'update fax_message'#13#10'  set fax_status = '#39'Failed'#39','#13#10'      complete' +
      '_date = :date,'#13#10'      error_details = :details'#13#10'  where fm_id = ' +
      ':id'#13#10
    Connection = Conn
    Parameters = <
      item
        Name = 'date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 72
    Top = 88
  end
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=doggy183;Persist Security Info=True' +
      ';User ID=sa;Initial Catalog=QMTesting;Data Source=10.1.1.175;Use' +
      ' Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Wo' +
      'rkstation ID=KYLEDESKTOP;Use Encryption for Data=False;Tag with ' +
      'column collation when possible=False'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 16
    Top = 24
  end
  object LogResp: TADOStoredProc
    Connection = Conn
    ProcedureName = 'log_fax_as_response;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FmId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@StatusDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 72
    Top = 152
  end
end
