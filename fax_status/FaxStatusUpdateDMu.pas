unit FaxStatusUpdateDMu;

interface

uses
  SysUtils, Windows, Classes, ADODB, DB, ReportConfig;

type
  TFaxStatusUpdateDM = class(TDataModule)
    SetSuccess: TADOCommand;
    SetFailure: TADOCommand;
    Conn: TADOConnection;
    LogResp: TADOStoredProc;
  private
    FileList: TStrings;
    Config: TReportConfig;
    IniFileName: string;
    QueueDir: string;
    Contents: TStringList;
    function GetFileList: TStrings;
    procedure ProcessFiles;
    procedure Log(Msg: string);
    procedure ConnectIfNeeded;
    procedure LogAsResponse(FmId: Integer; StatusDate: TDateTime; Success: Integer);
  public
    procedure Run;
  end;

implementation

uses OdAdoUtils;

{$R *.dfm}

{$WARN SYMBOL_PLATFORM OFF}

{ TFaxStatusUpdateDM }

procedure TFaxStatusUpdateDM.ConnectIfNeeded;
begin
  if not Conn.Connected then begin
    Log('Connecting to ' + Config.MainDB.Server);
    ConnectAdoConnectionWithConfig(Conn, Config.MainDB);
  end;
end;

function TFaxStatusUpdateDM.GetFileList: TStrings;
var
  sr: TSearchRec;
  FileAttrs: Integer;
begin
  if not DirectoryExists(QueueDir) then
    raise Exception.Create('Can not access directory: ' + QueueDir);

  Result := TStringList.Create;
  FileAttrs := SysUtils.faReadOnly;  // get those also

  if FindFirst(QueueDir + '*.*', FileAttrs, sr) = 0 then begin
    repeat
      Result.Add(sr.Name);
    until FindNext(sr) <> 0;
    SysUtils.FindClose(sr);
  end;
end;

procedure TFaxStatusUpdateDM.Log(Msg: string);
begin
  WriteLn(Msg);
end;

procedure TFaxStatusUpdateDM.LogAsResponse(FmId: Integer;
  StatusDate: TDateTime; Success: Integer);
begin
  LogResp.Parameters.ParamByName('@FmId').value := FmId;
  LogResp.Parameters.ParamByName('@StatusDate').value := StatusDate;
  LogResp.Parameters.ParamByName('@Success').value := Success;
  LogResp.ExecProc;
end;

procedure TFaxStatusUpdateDM.ProcessFiles;
var
  FN, FullFN: string;
  I, P: Integer;
  Ext: string;
  FmId: Integer;
  Total, Updates: Integer;
  StatusFileDate: TDateTime;
begin
  Log('Q Manager Fax Status Updater');
  Log('Starting at ' + DateTimeToStr(Now));

  IniFileName := ExtractFilePath(ParamStr(0))  + 'ReportEngineCGI.ini';
  Log('Loading configuration from: ' + IniFileName);
  Total := 0;
  Updates := 0;
  try
    Config := LoadConfig(IniFileName);
    Contents := TStringList.Create;
    ConnectIfNeeded;
    QueueDir := IncludeTrailingPathDelimiter(Config.FaxQueueDir);
    Log('FAX Queue Directory: ' + QueueDir);

    Log('Getting list of files');
    FileList := GetFileList;
    Log('Total files in directory: ' + IntToStr(FileList.Count));
    for I := 0 to FileList.Count-1 do begin
      FN := FileList[I];
      FullFN := QueueDir + FN;

      Inc(Total);
      if not FileExists(FullFN) then   // file might have disappeared during run
        Continue;

      P := Pos('.', FN);
      if P<1 then   // if the filename doesn't have an extension
        Continue;

      Ext := LowerCase(Copy(FN, P+1, 3));   // only consider up to 3 letters

      FmId := StrToIntDef(Copy(FN, 1, P-1), -1);

      if FmId = -1 then      // if not a numerical file name
        Continue;

      FileAge(FullFN, StatusFileDate);
      if Ext = 'ok' then begin
        ConnectIfNeeded;
        Log('                Success: ' + FN);
        SetSuccess.Parameters.ParamByName('id').value := FmId;
        SetSuccess.Parameters.ParamByName('date').value := StatusFileDate;
        SetSuccess.Execute;
        LogAsResponse(FmId, StatusFileDate, 1);
        Sysutils.DeleteFile(FullFN);
        Inc(Updates);
      end;

      if Ext = 'err' then begin
        ConnectIfNeeded;
        Log('                                           Error: ' + FN);
        Contents.LoadFromFile(QueueDir + FN);
        SetFailure.Parameters.ParamByName('id').value := FmId;
        SetFailure.Parameters.ParamByName('details').value := Contents.Text;
        SetFailure.Parameters.ParamByName('date').value := StatusFileDate;
        SetFailure.Execute;
        LogAsResponse(FmId, StatusFileDate, 0);
        Sysutils.DeleteFile(FullFN);
        Inc(Updates);
      end;
    end;
    Log('Done');
    Log('Updated Processed: ' + IntToStr(Updates));
    Log('Files Skipped: ' + IntToStr(Total - Updates));
    Log('');
  finally
    FileList.Free;
    Config.Free;
    Contents.Free;
  end;
end;

procedure TFaxStatusUpdateDM.Run;
begin
  try
    ProcessFiles;
  except
    on E: Exception do begin
      Log('********** ERROR **************');
      Log(E.Message);
    end;
  end;
end;

end.

