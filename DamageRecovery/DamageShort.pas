unit DamageShort;
//qm-283  791 provides a short damage report for each damage sr
interface

uses
  SysUtils, Windows, BaseReport, DB, ppDB, ppDBPipe, ppCtrls, ppBands,
  ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  Classes, ppStrtch, ppMemo, Graphics, ppModule, daDataModule, ppSubRpt,
  ppRegion, myChkBox, ppTypes, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageShortDM = class(TBaseReportDM)
    dsDamageShort: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppDetailBand1: TppDetailBand;
    MasterPipe: TppDBPipeline;
    ppSummaryBand1: TppSummaryBand;
    DamageSection: TppSubReport;
    ppChildReport9: TppChildReport;
    ppDetailBand10: TppDetailBand;
    DamageFooterBand: TppFooterBand;
    ppShape2: TppShape;
    ppSystemVariable6: TppSystemVariable;
    ppCalc3: TppCalc;
    ppLabel105: TppLabel;
    ppLine1: TppLine;
    ppLabel1: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText15: TppDBText;
    ppDBText14: TppDBText;
    ppDBText13: TppDBText;
    ppDBText2: TppDBText;
    ppShape1: TppShape;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppLabel8: TppLabel;
    ppDBText8: TppDBText;
    ppLabel9: TppLabel;
    ppDBText9: TppDBText;
    ppLabel10: TppLabel;
    ppDBText12: TppDBText;
    ppLabel13: TppLabel;
    ppLabel17: TppLabel;
    ppDBText17: TppDBText;
    ppLabel18: TppLabel;
    ppDBText18: TppDBText;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppDBText20: TppDBText;
    ppLabel21: TppLabel;
    ppDBText21: TppDBText;
    ppLabel22: TppLabel;
    ppDBText22: TppDBText;
    ppLabel23: TppLabel;
    ppDBText23: TppDBText;
    ppLabel24: TppLabel;
    ppDBText24: TppDBText;
    ppLabel25: TppLabel;
    ppLabel3: TppLabel;
    ppDBText19: TppDBText;
    ppDBText16: TppDBText;
    ExcavatorRegion: TppRegion;
    ppShape7: TppShape;
    ppDBText29: TppDBText;
    ppLabel32: TppLabel;
    ppDBText30: TppDBText;
    ppLabel33: TppLabel;
    ppDBText31: TppDBText;
    ppLabel34: TppLabel;
    ppDBText33: TppDBText;
    ppLabel36: TppLabel;
    ppDBText34: TppDBText;
    ppLabel37: TppLabel;
    ppLabel5: TppLabel;
    ppShape3: TppShape;
    ppLabel4: TppLabel;
    ppLabel12: TppLabel;
    ppLabel26: TppLabel;
    ppDBText32: TppDBText;
    ppLabel27: TppLabel;
    ppLabel30: TppLabel;
    ppLabel35: TppLabel;
    ppDBText36: TppDBText;
    ppDBText37: TppDBText;
    ppDBText38: TppDBText;
    SiteInfoRegion: TppRegion;
    ppShape5: TppShape;
    ppDBText47: TppDBText;
    ppLabel50: TppLabel;
    ppDBText50: TppDBText;
    ppLabel53: TppLabel;
    ppLabel11: TppLabel;
    ppLabel31: TppLabel;
    ppDBText10: TppDBText;
    ppLabel49: TppLabel;
    ppDBText11: TppDBText;
    ppLabel76: TppLabel;
    ppDBText25: TppDBText;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    ppDBText35: TppDBText;
    ppDBText39: TppDBText;
    ppLabel40: TppLabel;
    ppDBText40: TppDBText;
    ppLabel42: TppLabel;
    ppDBText42: TppDBText;
    ppLabel43: TppLabel;
    ppDBText48: TppDBText;
    DamageHeaderBand: TppHeaderBand;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppLabel89: TppLabel;
    ppFooterBand5: TppFooterBand;
    ppGroup1: TppGroup;
    DamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand6: TppGroupFooterBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    DamageShort: TADOQuery;
  end;
  var
     DamageShortDM:TDamageShortDM;
implementation

{$R *.dfm}

uses
  ODDBUtils, OdRbMemoFit, OdExceptions, ODMiscUtils,
  QMConst, OdPdf, odADOutils;




end.
