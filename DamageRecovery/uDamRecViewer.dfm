object frmDamRecViewer: TfrmDamRecViewer
  Left = 0
  Top = 0
  Caption = '   --  Damage Recovery Table Viewer'
  ClientHeight = 577
  ClientWidth = 962
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnActivate = FormActivate
  OnClose = FormClose
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 962
    Height = 25
    Align = alTop
    TabOrder = 0
  end
  object pnlMiddle: TPanel
    Left = 0
    Top = 25
    Width = 962
    Height = 400
    Align = alTop
    TabOrder = 1
    object damRecGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 960
      Height = 398
      Align = alClient
      TabOrder = 0
      object damRecGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmAlways
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.KeyFieldNames = 'recovery_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object damRecGridDBTableView1recovery_id: TcxGridDBColumn
          DataBinding.FieldName = 'recovery_id'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1damage_id: TcxGridDBColumn
          DataBinding.FieldName = 'damage_id'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1paid_date: TcxGridDBColumn
          DataBinding.FieldName = 'paid_date'
          DataBinding.IsNullValueType = True
          Visible = False
          GroupIndex = 0
        end
        object damRecGridDBTableView1damage_state: TcxGridDBColumn
          DataBinding.FieldName = 'damage_state'
          DataBinding.IsNullValueType = True
          Width = 46
        end
        object damRecGridDBTableView1recovery_status: TcxGridDBColumn
          DataBinding.FieldName = 'recovery_status'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1export_date: TcxGridDBColumn
          DataBinding.FieldName = 'export_date'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1TotalAttachments: TcxGridDBColumn
          DataBinding.FieldName = 'TotalAttachments'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1recovery_amount: TcxGridDBColumn
          DataBinding.FieldName = 'recovery_amount'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1recovery_company: TcxGridDBColumn
          DataBinding.FieldName = 'recovery_company'
          DataBinding.IsNullValueType = True
          Width = 173
        end
        object damRecGridDBTableView1recovery_date: TcxGridDBColumn
          DataBinding.FieldName = 'recovery_date'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1modified_date: TcxGridDBColumn
          DataBinding.FieldName = 'modified_date'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1modified_by: TcxGridDBColumn
          DataBinding.FieldName = 'modified_by'
          DataBinding.IsNullValueType = True
        end
        object damRecGridDBTableView1comment: TcxGridDBColumn
          DataBinding.FieldName = 'comment'
          DataBinding.IsNullValueType = True
        end
      end
      object damRecGridLevel1: TcxGridLevel
        GridView = damRecGridDBTableView1
      end
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 433
    Width = 962
    Height = 144
    Align = alClient
    TabOrder = 2
    DesignSize = (
      962
      144)
    object btnExit: TBitBtn
      Left = 839
      Top = 14
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Kind = bkClose
      NumGlyphs = 2
      ParentDoubleBuffered = True
      TabOrder = 0
    end
    object btnRecentDamages: TButton
      Left = 32
      Top = 16
      Width = 145
      Height = 25
      Caption = 'Insert Recent Damages'
      TabOrder = 1
      OnClick = btnRecentDamagesClick
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 425
    Width = 962
    Height = 8
    AlignSplitter = salTop
  end
end
