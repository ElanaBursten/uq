unit uMainFTP;
//qm-791
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, OdUqInternet, ShellAPI,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  Winapi.SHFolder, Winapi.SHLObj, Winapi.KnownFolders, Winapi.ActiveX,
  System.IOUtils, System.Types, Vcl.Grids, Vcl.ValEdit,
  Vcl.ComCtrls, aBaseAttachmentDMu, Vcl.CheckLst,
  Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxDBEdit, dxCore, cxDateUtils, Vcl.ExtCtrls, Vcl.AppEvnts, Vcl.WinXPickers;
//  ,DamageDetails;
//
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);
  TDBType =(dtNone, dtQM,dtLocQM);
type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end;

type
  TfrmMainFTP = class(TForm)
    btnRun: TButton;
    Memo1: TMemo;
    ProgressBar1: TProgressBar;
    Label3: TLabel;
    edtRecordCount: TEdit;
    edtProcessed: TEdit;
    Label4: TLabel;
    TrayIcon1: TTrayIcon;
    ApplicationEvents1: TApplicationEvents;
    ckbxSendToFTP: TCheckBox;
    dpPaidDate: TDatePicker;
    btnReset: TButton;
    DRATStatusBar: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    btnRecTable: TButton;
    btnPrintAllReports: TButton;
    cbNoWarn: TCheckBox;
    cbBulkSave: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ADOConnAfterConnect(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure ApplicationEvents1Minimize(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnRecTableClick(Sender: TObject);
    procedure btnPrintAllReportsClick(Sender: TObject);

  private

    fIncludeFileTypes: string;
    fExcludeFileNames: string;
    fPathForFTP: string;
    fLocalFileDrive: string;
    fConnString: string;

    fPathPaidDate: string;
    fPathDamageID: string;
    fPath4App: string;
    fActiveDoFile: string;
    fWinScpPath: string;
    connectedTo:string;
    fWhichDB:string;
    fawsConn: string;
    fBulkZipSave: string;

    function ReadINI: Boolean;
    function connectToDB: Boolean;

    procedure clearLogRecord;

    function getAppVer: String;

    procedure RunProcess(Sender: TObject);
    function downloadAttachmentsForDamage(attID: integer; FileName, origFilename, damageIDPath: string): integer;

    procedure InitData;

    function copyAttachments(damageID: integer; damageIDPath: string): integer;

    procedure ProcessCV(damagePath, sTicket: string);
    procedure MakeXMLFile(damageID: integer; damageIDPath: string);
    procedure MakeSummaryXML(PathPaidDate: string; PaidDate: TDateTime);
    function ZipPaidDate(ZipPath: string): Boolean;
    function DelDir(dir: string): Boolean;

    procedure PushToCMR;

    function ExecuteProcess(const FileName, Params: string; Folder: string;
      WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
    function CountFiles(FilePath: string): integer;
    procedure SetUpTrayIcons;
    function getParam: string;

    { Private declarations }
  public
    { Public declarations }
    LogResult: TLogResults;
    LocalAppDataFolder: string;
    dbType :TDBType;
    fPaidDate: TDate;
    fFtpAttLog: string;
    ftpHost: string;
    IniName: string;
    imageDM: TimageDM;
    FinalZipName: string;
    SevenZip: string;
    property awsConn: string read fawsConn write fawsConn;
    property ConnString: string read fConnString write fConnString;
    property PathForFTP: string read fPathForFTP write fPathForFTP;
    property BulkZipSave: string read fBulkZipSave write fBulkZipSave;
    property PathPaidDate: string read fPathPaidDate write fPathPaidDate;
    property PathDamageID: string read fPathDamageID write fPathDamageID;
    property Path4App: string read fPath4App write fPath4App;
    property WinScpPath: string read fWinScpPath write fWinScpPath;
    property ActiveDoFile: string read fActiveDoFile write fActiveDoFile;
    property appVer: String read getAppVer;

    property FtpAttLog: string read fFtpAttLog write fFtpAttLog;

    property IncludeFileTypes: string read fIncludeFileTypes write fIncludeFileTypes;
    property ExcludeFileNames: string read fExcludeFileNames write fExcludeFileNames;
    property LocalFileDrive: string read fLocalFileDrive write fLocalFileDrive;

    property PaidDate: TDate read fPaidDate write fPaidDate;
    procedure CreateDOfile;
    procedure WriteLog(LogResult: TLogResults);
  end;

function GetKnownFolderPath(const Folder: KNOWNFOLDERID): string;
function GetAppVersionStr: string;

// const
// BASE_PATH_UTL_ATT = '\\dynutil.com\utq\Qmanager\ftp\california\ATT\';

var
  frmMainFTP: TfrmMainFTP;

implementation

uses DateUtils, inifiles, miniCVUtils, System.zip, uDamRecViewer, DamageShort;
{$R *.dfm}

procedure TfrmMainFTP.ADOConnAfterConnect(Sender: TObject);
begin
  LogResult.LogType := ltInfo;
  LogResult.Status := 'Connect: ' + BoolToStr(imageDM.ADOConn.Connected);
  LogResult.MethodName := 'ADOConnAfterConnect';
  WriteLog(LogResult);
end;

procedure TfrmMainFTP.btnPrintAllReportsClick(Sender: TObject);
begin   //qm-283
  if ImageDM.PrintReportAll(PathForFTP) then
  memo1.Lines.Add('All Submitted Reports Printed.');
end;

procedure TfrmMainFTP.btnRecTableClick(Sender: TObject);
begin
  frmDamRecViewer:= TfrmDamRecViewer.Create(self);
  imageDM.tblDamRec.Open;
  frmDamRecViewer.show;
end;

procedure TfrmMainFTP.btnResetClick(Sender: TObject);
var
  reSetDate:TdateTime;
begin
  reSetDate := dpPaidDate.Date;
  if (cbNoWarn.Checked) or (MessageDlg('This will set the salient entries in the damage_recovery to NULL for the Paid Date: '
  +DateTimeToStr(reSetDate)
  +#13+#10+'It may span multiple damages.  To restore, you will need to run DRAT again.  '
  +#13+#10+''+#13+#10+'DRAT will ALWAYS process the oldest Piad Date whose recovery status is set to PENDING.',
  mtWarning, [mbOK, mbCancel], 0) = mrOK) then
  begin
    imageDM.resetDamageRecovery.Parameters.ParamByName('PaidDate').value := reSetDate;
    imageDM.resetDamageRecovery.ExecSQL;
  end
  else
  begin
    ShowMessage('Reset Cancelled for: '+DateTimeToStr(reSetDate));
  end;
end;

procedure TfrmMainFTP.btnRunClick(Sender: TObject);
begin
  btnReset.Enabled:= false;
  RunProcess(Sender);
end;

procedure TfrmMainFTP.RunProcess(Sender: TObject);
const
  DamagePaidDate = 'DPD-';
  DamagePaidDateL = 'DPD_L-';
  stopName = 'DO_NOT_RUN.TXT';
var
  damageID: integer;
  totalAtt, cnt: integer;
  mtPaidDate : TDatetime;
  exitFile: TextFile;
  KillSwitch:boolean;
begin
  try
    KillSwitch := false;
    mtPaidDate := StrToDate('6/6/1944');
    damageID := 0;
    cnt := 0;

    try
      imageDM.qryDamageRecovery.open;
      imageDM.qryDamageRecovery.first;
      PaidDate := imageDM.qryDamageRecovery.FieldByName('paid_date').AsDateTime;
      if PaidDate <= mtPaidDate then
      begin
        AssignFile(exitFile, stopName);
        memo1.Lines.Add('exitFile assigned');
        Rewrite(exitFile);
        CloseFile(exitFile);
        KillSwitch := true;
        exit;
      end;


      imageDM.updPreUpdateDamageRecovery.Parameters.ParamByName('PaidDate').Value:= PaidDate;
      imageDM.updPreUpdateDamageRecovery.ExecSQL;

      DRATStatusBar.Panels[1].Text := DateTimeToStr(PaidDate);

      Memo1.lines.add('Processing Damages for : ' + DateTimeToStr(PaidDate));

      PathPaidDate := IncludeTrailingBackslash(PathForFTP + DamagePaidDate + FormatDateTime('yyyymmdd', PaidDate));
      DelDir(PathPaidDate);

      LogResult.LogType := ltInfo;
      LogResult.Status := '***********************Processing****************' + PathPaidDate;
      LogResult.MethodName := 'RunProcess';
      WriteLog(LogResult);
      with imageDM do // data module
      begin
        while not(qryDamageRecovery.eof) do // returns summary XML by Paid Date
        begin
          inc(cnt);
          edtRecordCount.Text := inttostr(cnt);

          damageID := qryDamageRecovery.FieldByName('damage_id').AsInteger;
          DRATStatusBar.Panels[2].Text := inttostr(damageID);
          DRATStatusBar.Refresh;
          PathPaidDate := IncludeTrailingBackslash(PathForFTP + DamagePaidDate +
            FormatDateTime('yyyymmdd', PaidDate));
          PathDamageID := IncludeTrailingBackslash(PathPaidDate + 'DamageID_' +
            inttostr(damageID));
          Memo1.Lines.Add('MakeXMLFile - damageID: ' + inttostr(damageID) +
            ' PathDamageID: ' + PathDamageID);
          MakeXMLFile(damageID, PathDamageID);
          if imageDM.PrintReport(damageID, PathDamageID) then   //qm-283  sr
            Memo1.Lines.Add('Report printed for: ' + inttostr(damageID) +
              ' PathDamageID: ' + PathDamageID)
          else
          begin
            Memo1.Lines.Add('Report printed FAILED for: ' + inttostr(damageID) +
              ' PathDamageID: ' + PathDamageID);
            LogResult.LogType := ltError;
            LogResult.ExcepMsg := 'Exception: Report printed FAILED for: ' +
              inttostr(damageID) + ' PathDamageID: ' + PathDamageID;
            LogResult.MethodName := 'PrintReport';
            WriteLog(LogResult);
          end;
          totalAtt := 0;
          Memo1.Lines.Add('before copyAttachments - damageID: ' +
            inttostr(damageID) + 'PathDamageID: ' + PathDamageID);
          totalAtt := copyAttachments(damageID, PathDamageID);
          Memo1.Lines.Add('after copyAttachments - damageID: ' +
            inttostr(damageID) + 'PathDamageID: ' + PathDamageID + ' totalAtt: '
            + inttostr(totalAtt));
          try
            Memo1.Lines.Add('before ProcessCV - PathDamageID: ' + PathDamageID);
            ProcessCV(PathDamageID, '');
            Memo1.Lines.Add('after ProcessCV - PathDamageID: ' + PathDamageID);
          except
            on E: Exception do
            begin
              LogResult.LogType := ltError;
              LogResult.ExcepMsg := 'Exception: ' + E.Message;
              Memo1.Lines.Add( 'Crack @CV exception: '+E.Message);
              LogResult.MethodName := 'Crack @CV';
              WriteLog(LogResult);
            end;
          end;
          totalAtt := CountFiles(PathDamageID);
          Memo1.Lines.Add('After CountFiles - totalAtt: ' + inttostr(totalAtt));
          qryDamageRecovery.next;
//          if ckbxSendToFTP.Checked then
//          begin
          imageDM.updDamageRecovery.Parameters.ParamByName('RecStatus').value
            := 'SUBMITTED';
          imageDM.updDamageRecovery.Parameters.ParamByName('damageID').value
            := damageID;
          imageDM.updDamageRecovery.Parameters.ParamByName('TotAtt').value
            := totalAtt;
          imageDM.updDamageRecovery.Parameters.ParamByName('PaidDate').value
            := PaidDate;
          imageDM.updDamageRecovery.ExecSQL;
//          end;

        end; // while qryDamageRecovery.eof do
      end; // with imageDM do
    finally
      begin
        DRATStatusBar.Panels[2].Text :='';
        DRATStatusBar.Refresh;
        imageDM.qryDamageRecovery.close;
        if not KillSwitch then
        begin
          MakeSummaryXML(PathPaidDate, PaidDate);
          ZipPaidDate(PathForFTP);
//          if ckbxSendToFTP.Checked then
//          begin
            CreateDOfile;
//            PushToCMR;
//          end;
          LogResult.LogType := ltInfo;
          LogResult.Status := 'Processing ' + PathPaidDate + ' Complete';
          LogResult.MethodName := 'RunProcess';
          WriteLog(LogResult);
          btnReset.Enabled:= true;
        end
        else
        begin
          LogResult.LogType := ltInfo;
          LogResult.Status := 'All Damages to date processed.  By By';
          LogResult.MethodName := 'RunProcess';
          WriteLog(LogResult);
          Application.Terminate;
        end;
        if ParamCount < 2 then
        Application.Terminate;
      end;
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := 'Exception: ' + E.Message;
      LogResult.MethodName := 'RunProcess';
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmMainFTP.TrayIcon1DblClick(Sender: TObject);
begin
  { Hide the tray icon and show the window,
    setting its state property to wsNormal. }
  TrayIcon1.Visible := False;
  Show();
  WindowState := wsNormal;
  Application.BringToFront();

end;

procedure TfrmMainFTP.PushToCMR;
var
  FileName, Parameters, WorkingFolder: string;
  OK: Boolean;
  Error: integer;
const
  WIN_SCP = 'WinSCP.bat';
begin
  FileName := IncludeTrailingBackslash(WinScpPath) + WIN_SCP;
  Parameters := ActiveDoFile;
  DRATStatusBar.Panels[3].Text :=  'Push to CMR';
  DRATStatusBar.Refresh;

  LogResult.LogType := ltInfo;
  LogResult.Status := 'Calling ExecuteProcess';
  LogResult.MethodName := 'PushToCMR';
  WriteLog(LogResult);


  OK := ExecuteProcess(FileName, Parameters, WorkingFolder, true, False, true, Error);
  LogResult.LogType := ltInfo;
  LogResult.DataStream:= 'FileName: '+FileName+ ' Param: '+ Parameters+ ' Folder: '+ WorkingFolder+' Error: '+IntToStr(Error);
  LogResult.MethodName := 'PushToCMR Complete';
  WriteLog(LogResult);

  DelDir(PathPaidDate);
  if not OK then
  begin
    LogResult.LogType := ltError;
    LogResult.Status := 'Process failed';
    LogResult.ExcepMsg := 'Error: ' + SysErrorMessage(Error);
    LogResult.MethodName := 'PushToCMR';
    WriteLog(LogResult);
  end;
end;

function TfrmMainFTP.ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
      repeat
        Application.ProcessMessages;
      until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

procedure TfrmMainFTP.MakeSummaryXML(PathPaidDate: string; PaidDate: TDateTime);
var
  S: TStrings;
  xmlStr: string;
  XML_Name: string;

const
  EXT = '.xml';
  XML_NAME_LEAD = 'PaidDateSummary';
  ENCODE = '<?xml version=''1.0'' encoding=''UTF-8'' ?>';
begin
  DRATStatusBar.Panels[3].Text :=  'Summary XML';
  DRATStatusBar.Refresh;
  ForceDirectories(PathPaidDate);
  try
    S := TStringList.Create();
    with imageDM do
    begin
      qryDamageSummary.Parameters.ParamByName('PaidDate').value := PaidDate;
      qryDamageSummary.open;
      xmlStr := qryDamageSummary.Fields[0].AsWideString;
    end;
    XML_Name := PathPaidDate + XML_NAME_LEAD + EXT;

    S.add(ENCODE);
    S.add(xmlStr);
    S.SaveToFile(XML_Name, TEncoding.UTF8);
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Made Summary.XML for: ' + DateToStr(PaidDate);
    LogResult.MethodName := 'MakeSummaryXML';
    WriteLog(LogResult);

  finally
    imageDM.qryDamageSummary.close;
    S.free;
  end;
end;

Function TfrmMainFTP.ZipPaidDate(ZipPath: string): Boolean;
const
  DamagePaidDate = 'DPD-';
  DamagePaidDateL = 'DPD_L-';
  ZIP_EXT = '.zip';
var
  FileName, Parameters, WorkingFolder, ZipUpFolder: string;
  OK: Boolean;
  Error: integer;
begin
  DRATStatusBar.Panels[3].Text :=  'Zipping Paid Date';
  DRATStatusBar.Refresh;
  LogResult.LogType := ltInfo;
  LogResult.Status := 'Before Zipping: ' + ZipPath;
  LogResult.MethodName := 'ZipPaidDate';
  WriteLog(LogResult);
  Result := true;
  try
    setCurrentDir(ZipPath);
    if fWhichDB = 'QM' then
    begin
      FinalZipName := DamagePaidDate + FormatDateTime('yyyymmdd', PaidDate) + ZIP_EXT;
      ZipUpFolder :=  IncludeTrailingBackslash(ZipPath);
    end
    else
    if fWhichDB = 'LocQM' then
    begin
      FinalZipName := DamagePaidDateL + FormatDateTime('yyyymmdd', PaidDate) + ZIP_EXT;
      ZipUpFolder :=  IncludeTrailingBackslash(ZipPath);
    end;

    FileName:= SevenZip;  //'C:\Program Files\7-Zip\7z.exe';
    Parameters := ' a -tzip '+ZipPath+FinalZipName+' '+ZipUpFolder;
    OK := ExecuteProcess(FileName, Parameters, WorkingFolder, true, False, true, Error);
    Assert(OK);
    if cbBulkSave.Checked then
    begin
      copyfile(PWideChar(ZipPath+FinalZipName), PWideChar(BulkZipSave+FinalZipName),False);
      DelDir(ZipPath);
    end;
    LogResult.LogType := ltInfo;
    LogResult.Status := 'After Zipping up directory: ' + PathPaidDate;
    LogResult.MethodName := 'ZipPaidDate';
    WriteLog(LogResult);
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Error Zipping up directory: ' + PathPaidDate;
      LogResult.ExcepMsg := E.Message;
      LogResult.MethodName := 'ZipPaidDate';
      WriteLog(LogResult);
      Result := False
    end;
  end;
end;

procedure TfrmMainFTP.MakeXMLFile(damageID: integer; damageIDPath: string);
var
  xmlStr: WideString;
  S: TStrings;
const
  EXT = '.xml';
  XML_Name = 'DamageData';
  ENCODE = '<?xml version=''1.0'' encoding=''UTF-8'' ?>';
begin
  DRATStatusBar.Panels[3].Text := 'Make XML';
  DRATStatusBar.Refresh;
  xmlStr := '';
  ForceDirectories(damageIDPath);
  Memo1.Lines.Add('ForceDirectories: ' + damageIDPath);
  try
    S := TStringList.Create();
    with imageDM do
    begin
      try
        qryDamageData.Parameters.ParamByName('DamageID').value := damageID;
        Memo1.Lines.Add('qryDamageData: ' + inttostr(damageID));
        qryDamageData.Open;
        xmlStr := qryDamageData.FieldByName('DamageXmlOut').AsString;
        Memo1.Lines.Add('-- xmlStr: ' + xmlStr);

      except
        on E: Exception do
        begin
          Memo1.Lines.Add('exception: ' + E.Message);
          LogResult.LogType := ltError;
          LogResult.Status := 'MakeXMLFile for ' + inttostr(damageID);
          LogResult.MethodName := 'qryDamageData';
          LogResult.ExcepMsg := E.Message;
          WriteLog(LogResult);
        end;
      end;
    end;

    S.Add(ENCODE);
    S.Add(xmlStr);

    try
      S.SaveToFile(damageIDPath + XML_Name + EXT, TEncoding.UTF8);
    except
      on E: Exception do
      begin
        Memo1.Lines.Add('SaveToFile DamageData Exception: ' + inttostr(damageID));
        LogResult.LogType := ltError;
        LogResult.Status := 'SaveToFile for ' + inttostr(damageID);
        LogResult.MethodName := 'MakeXMLFile';
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
      end;
    end;
    LogResult.LogType := ltInfo;
    LogResult.Status := 'MakeXMLFile for ' + inttostr(damageID);
    LogResult.MethodName := 'MakeXMLFile';
    WriteLog(LogResult);

  finally
    imageDM.qryDamageData.close;
    S.free;
    Memo1.Lines.Add('MakeXMLFile Complete');
  end;
End;

function TfrmMainFTP.copyAttachments(damageID: integer; damageIDPath: string): integer;
var
  FileName: string;
  origFilename: string;
  uploadDate: TDateTime;
  attachmentID: integer;
  i: integer;
begin
  attachmentID := 0;
  i := 0;
  DRATStatusBar.Panels[3].Text :=  'Copy Images';
  DRATStatusBar.Refresh;
  try
    try
      with imageDM do
      begin
        qryAttachments.Parameters.ParamByName('DamageID').value := damageID;
        qryAttachments.open;
        qryAttachments.first;
        while not qryAttachments.eof do
        begin
          Application.ProcessMessages;

          FileName := qryAttachments.FieldByName('filename').AsString;
          origFilename := qryAttachments.FieldByName('orig_filename').AsString;
          uploadDate := qryAttachments.FieldByName('upload_date').AsDateTime;
          attachmentID := qryAttachments.FieldByName('attachment_id').AsInteger;

          Memo1.lines.add('calling downloadAttachmentsForDamage ' + inttostr(damageID));
          inc(i);
          downloadAttachmentsForDamage(attachmentID, FileName, origFilename, damageIDPath);
          Memo1.lines.add('attachmentID: ' + inttostr(attachmentID));
          Memo1.lines.add('filename: ' + FileName);
          Memo1.lines.add('origFilename: ' + origFilename);
          Memo1.lines.add('DamageIDPath: ' + damageIDPath);

          qryAttachments.next;

          edtProcessed.text := inttostr(i);
          LogResult.LogType := ltInfo;
          LogResult.Status := 'Processed attachments ' + origFilename + ' for Damage ' + inttostr(damageID);
          LogResult.MethodName := 'copyAttachments';
          WriteLog(LogResult);
          ProgressBar1.StepIt;
        end; // while not qryAttachments.eof do
      end; // with imageDM do

    except
      on E: Exception do
      begin
        Memo1.lines.add('copyAttachments: '+E.Message);
        dec(i);
      end;

    end;
  finally
    begin
      ProgressBar1.Position := 0;
      DRATStatusBar.Panels[3].Text :=  '';
      DRATStatusBar.Refresh;
      imageDM.qryAttachments.close;
      Memo1.lines.add('Completed transferring ' + inttostr(i) + ' images for ' + inttostr(damageID));
      LogResult.LogType := ltInfo;
      LogResult.Status := 'Processed attachments ' + origFilename + ' for Damage ' + inttostr(damageID);
      LogResult.MethodName := 'copyAttachments';
      WriteLog(LogResult);

      Result := i;
    end;
  end;
end;

procedure TfrmMainFTP.CreateDOfile;
const
  LINE1 = 'option batch on';
  LINE2 = 'option confirm off';
  LINE3 = 'open ';
  LINE4 = 'lcd ';
  LINE5 = 'cd /';
  LINE6 = '-binary ';
  LINE7 = 'put -delete -nopreservetime -nopermissions ';
  LINE8 = 'exit';

  DO_FILE = 'do';
var
  doLIst: TStringList;
begin
  try
    try
      ActiveDoFile := DO_FILE;
      doLIst := TStringList.Create;
      doLIst.add(LINE1);
      doLIst.add(LINE2);
      doLIst.add(LINE3 + ftpHost);
      doLIst.add(LINE4 + LocalFileDrive);
      doLIst.add(LINE5);
      doLIst.add(LINE6); // transfer options
      doLIst.add(LINE7 + PathForFTP + FinalZipName);
      doLIst.add(LINE8);
      doLIst.SaveToFile(WinScpPath + ActiveDoFile, TEncoding.UTF8);
      Memo1.lines.add('-----------------do File start-------------------------');
      Memo1.lines.add(doLIst.text);
      Memo1.lines.add('-----------------do File end-------------------------');
      DRATStatusBar.Panels[3].Text :=  'Create DO File';
    except on E: Exception do
    begin
        LogResult.LogType := ltError;
        LogResult.ExcepMsg := 'Exception: '+e.Message;
        LogResult.Status := 'Path: ' + WinScpPath;
        LogResult.MethodName := 'Create Do file';
        WriteLog(LogResult);
        Memo1.lines.add('WinScpPath Path: ' + WinScpPath);
    end;
end;

  finally
    freeandnil(doLIst);
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Path: ' + WinScpPath;
    LogResult.MethodName := 'Create Do file';
    WriteLog(LogResult);
    Memo1.lines.add('WinScpPath Path: ' + WinScpPath);

  end;

end;

procedure TfrmMainFTP.ProcessCV(damagePath, sTicket: string);
var
  searchResult: TSearchRec;
  oldJpegName, newJpegName: string;
begin
  Memo1.lines.add('Looking for @CV to crack in ' + damagePath);
  setCurrentDir(damagePath);
  DRATStatusBar.Panels[3].Text := 'Cracking @CVs';
  DRATStatusBar.Refresh;
  if findFirst('*.@CV', faAnyFile, searchResult) = 0 then
  begin
    repeat
      Memo1.lines.add('Line 757: '+searchResult.Name + ' to crack');

//      try
        If ReplaceCVs(damagePath + searchResult.Name, sTicket, IniName) then
        begin
          LogResult.LogType := ltInfo;
          LogResult.Status := 'Cracked ' + searchResult.Name;
          LogResult.MethodName := 'ReplaceCVs';
        end
        else
        begin
          LogResult.LogType := ltError;
          LogResult.Status := 'Failed to Crack ' + searchResult.Name;
          LogResult.MethodName := 'ReplaceCVs';
          DeleteFile(damagePath + searchResult.Name);
        end;
      WriteLog(LogResult);
    until FindNext(searchResult) <> 0;
    FindClose(searchResult);
  end;
  setCurrentDir(ExtractFilePath(Application.ExeName));
end;

procedure TfrmMainFTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TrayIcon1.free;
  imageDM.free;
  DamageShortDM.Free;
  Action := caFree;
end;

procedure TfrmMainFTP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if imageDM.ADOConn.Connected then
    imageDM.ADOConn.close;
  CanClose := not imageDM.ADOConn.Connected;
end;

function TfrmMainFTP.getParam:string;
var
  s : string;
begin
  s := paramStr(1);
  if ((s = 'QM') or (s = 'LocQM')) then
  begin
    if s='QM' then dbType:=dtQM;
    if s='LocQM' then dbType:=dtLocQM;
  result := s;
  end
  else
  begin
    dbType:=dtNone;
    ShowMessage('Invalid Command line parameter.  First argument must be ''QM'' or ''LocQM''.  ');
    Close;
  end;
end;

procedure TfrmMainFTP.FormCreate(Sender: TObject);
begin
  fWhichDB:= getParam();
  Memo1.lines.add('FormCreate');
  DRATStatusBar.Panels[0].Text := 'Not Connected';
  DRATStatusBar.Panels[4].Text := fWhichDB;
  DRATStatusBar.Panels[5].Text := appVer;
  DRATStatusBar.Refresh;
  imageDM := TimageDM.Create(Self);
  Memo1.lines.add('Created ImageDM');
  DamageShortDM:=TDamageShortDM.Create(self);
  ReadINI; // ReadINI for FTP settings
  SetUpTrayIcons;
  Memo1.lines.add('ReadINI for FTP settings');
  Memo1.lines.add('ProcessParams');
  InitData;

  Memo1.lines.add('InitData');

  if imageDM.GetAWSConstants then
    Memo1.lines.add('GetAWSConstants succeeded')
  else
    Memo1.lines.add('GetAWSConstants failed');

  if imageDM.GetAWSCredentials then
    Memo1.lines.add('GetAWSCredentials succeeded')
  else
    Memo1.lines.add('GetAWSCredentials failed');

  if ParamCount < 2 then
  begin
    TrayIcon1.Visible := true;
    TrayIcon1.Animate := true;
    TrayIcon1.ShowBalloonHint;
    RunProcess(Sender);
  end
  else
    ckbxSendToFTP.Checked := false;
end;

procedure TfrmMainFTP.SetUpTrayIcons;
var
  MyIcon: TIcon;
begin
  try
    { Load the tray icons. }
    TrayIcon1.Icons := TImageList.Create(Self);
    MyIcon := TIcon.Create;

    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\TONGUE.ICO');
    TrayIcon1.Icon.Assign(MyIcon);
    TrayIcon1.Icons.AddIcon(MyIcon);
    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\HAPPY.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);
    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\OOH.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);
    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\FRAZZLED.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);

    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\HAPPY.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);

    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\SHADES.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);

    MyIcon.LoadFromFile(IncludeTrailingBackslash(Path4App) + 'Icons\SAD.ICO');
    TrayIcon1.Icons.AddIcon(MyIcon);

    { Set up a hint message and the animation interval. }
    TrayIcon1.Hint := 'Oh Drat!';
    TrayIcon1.AnimateInterval := 200;

    { Set up a hint balloon. }
    TrayIcon1.BalloonTitle := 'Restoring the window.';
    TrayIcon1.BalloonHint := 'Double click the system tray icon to restore the window.';
    TrayIcon1.BalloonFlags := bfInfo;
  finally
    freeandnil(MyIcon);
  end;

end;

function TfrmMainFTP.getAppVer: String;
begin
  Result := GetAppVersionStr;
end;

function TfrmMainFTP.downloadAttachmentsForDamage(attID: integer; FileName, origFilename, damageIDPath: string): integer;
var
  attPath: string;
begin
  myAttachmentData.attachmentID := attID;
  myAttachmentData.LocalFilename := FileName;
  myAttachmentData.origFilename := origFilename;
  ForceDirectories(damageIDPath);
  attPath := damageIDPath + myAttachmentData.origFilename;
  try
    imageDM.DownloadAttachmentToFileAWS(myAttachmentData, attPath);
  except
    on E: Exception do
    begin
      Memo1.lines.add('downloadAttachmentsForDamage: ' + E.Message);
    end;
  end;
end;

function TfrmMainFTP.CountFiles(FilePath: string): integer;
var
  SearchRec: TSearchRec;
  fileCount: integer;
begin
  fileCount := 0;
  if FilePath[Length(FilePath)] <> '\' then
    FilePath := FilePath + '\';
  if findFirst(FilePath + '*.*', faAnyFile and not faDirectory, SearchRec) = 0 then
  begin
    inc(fileCount);
    while FindNext(SearchRec) = 0 do
    begin
      inc(fileCount);
    end;
  end;
  FindClose(SearchRec);
  Result := fileCount;
end;

procedure TfrmMainFTP.InitData;
begin
  If not connectToDB then
  begin
    LogResult.LogType := ltError;
    LogResult.MethodName := 'connectToDB';
    LogResult.Status := 'Connection Failed';
    LogResult.DataStream := ConnString;
    WriteLog(LogResult);
    Application.Terminate;
  end;
end;

function TfrmMainFTP.connectToDB: Boolean;
var
  S: string;
  i: integer;
const
  DRV_SQLOLEDB='SQLOLEDB';
  DRV_NATIVE='SQLNCLI11';
  SERVER_NAT='Server=';
  SERVER_OLEDB='Source=';

begin
  if imageDM.connectToDB(ConnString) then
    Memo1.lines.add('connectToDB result: succeeded')
  else
    Memo1.lines.add('connectToDB result: failed');

  Result := imageDM.ADOConn.Connected;
  if Result = true then
  begin
    if  POS(SERVER_OLEDB, ConnString)>0 then
    S := copy(ConnString, POS(SERVER_OLEDB, ConnString) + 7, maxChar)
    else
    if  POS(SERVER_NAT, ConnString)>0 then
    S := copy(ConnString, POS(SERVER_NAT, ConnString) + 7, maxChar);
    i := POS(';', S);
    if i <> 0 then
      connectedTo := copy(S, 0, POS(';', S) - 1)
    else connectedTo := S;
    DRATStatusBar.Panels[0].Text := connectedTo;
  end
  else
  begin
    DRATStatusBar.Panels[0].Text := 'Not Connected';
  end;
    DRATStatusBar.Refresh;
end;


function TfrmMainFTP.ReadINI: Boolean;
var
  IniFile: TIniFile;
begin
  try
    DRATStatusBar.Panels[3].Text :=  'Read INI';
    DRATStatusBar.Refresh;
    Path4App := ExtractFilePath(Application.ExeName);
    if fWhichDB = 'QM' then
    IniFile := TIniFile.Create(Path4App + 'damageCRM.ini')
    else
    if fWhichDB = 'LocQM' then
    IniFile := TIniFile.Create(Path4App + 'damageCRMLocQM.ini');

    SevenZip := IniFile.ReadString('remoteFTP', 'SEVEN_ZIP', 'C:\Program Files\7-Zip\7z.exe');
    FtpAttLog := IniFile.ReadString('remoteFTP', 'ftpAttLog', 'E:\QM\Logs');
    ForceDirectories(FtpAttLog);
    ftpHost := IniFile.ReadString('remoteFTP', 'ftpHost',
      'sftp://utq-prod-cmr@sftp.mcview.com/ -hostkey="ssh-rsa 2048 2EZdNsDac9uazNmBhc1EblcxN0Kpn8OeBNEdHAAw/gk=" -privatekey="C:\Program Files (x86)\UtiliQuest\Q Manager\DamageRecovery\utq-prod-cmr.ppk" ');
    PathForFTP := IncludeTrailingBackslash(IniFile.ReadString('remoteFTP', 'PathForFTP', 'e:\utq\CMR\'));
    BulkZipSave :=IncludeTrailingBackslash(IniFile.ReadString('remoteFTP', 'BulkZipSave', 'e:\utq'));
    WinScpPath := IniFile.ReadString('remoteFTP', 'WinScpPath', 'C:\Trunk1\DamageRecovery\winscp');
    ConnString := IniFile.ReadString('remoteFTP', 'ConnString',
      'Provider=SQLOLEDB.1;Password=ihjidojo3;Persist Security Info=True;User ID=QMParserUTL;Initial Catalog=QM;Data Source=DYATL-PENTDB02');
    awsConn    := IniFile.ReadString('remoteFTP', 'AwsConn',
      'SQLNCLI11.1;Integrated Security="";Persist Security Info=False;User ID=uqweb;Password=ihjidojo03;Initial Catalog=QM; Data Source=SSDS-UTQ-QM-01-DV;Initial File Name="";Server SPN="";Application Intent=READONLY');

    ExcludeFileNames := IniFile.ReadString('remoteFTP', 'ExcludeFileNames', 'Screenshot*');

    IniName := IncludeTrailingBackslash(ExtractFileDir(Application.ExeName)) + 'eprCertusViewConn.ini';
    DelDir(PathForFTP); //defensive
    ForceDirectories(PathForFTP);
    LocalFileDrive := ExtractFileDrive(PathForFTP);
  finally
    begin
      Memo1.lines.add('IniFile ' + IniFile.ToString);
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'ReadINI for FTP settings';
      LogResult.Status := 'IniFile processed';
      WriteLog(LogResult);
      freeandnil(IniFile);
    end;
  end;
end;

procedure TfrmMainFTP.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := IncludeTrailingBackslash(FtpAttLog) + 'DamageRecoveryLog' + '-' + FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if not(FileExists(LogName)) then
  begin
    WriteLn(myFile, 'DRAT Version: ' + appVer);
  end;

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMainFTP.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TfrmMainFTP.DelDir(dir: string): Boolean;
var
  fos: TSHFileOpStruct;
begin
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_DELETE;
    fFlags := FOF_SILENT or FOF_NOCONFIRMATION;
    pFrom := PChar(dir + #0);
  end;
  Result := (0 = ShFileOperation(fos));
end;

function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := paramStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function GetKnownFolderPath(const Folder: KNOWNFOLDERID): string;
var
  Path: LPWSTR;
begin
  if SUCCEEDED(SHGetKnownFolderPath(Folder, 0, 0, Path)) then
  begin
    try
      Result := Path;
    finally
      CoTaskMemFree(Path);
    end;
  end
  else
    Result := '';
end;

procedure TfrmMainFTP.ApplicationEvents1Minimize(Sender: TObject);
begin
  { Hide the window and set its state variable to wsMinimized. }
  Hide();
  WindowState := wsMinimized;

  { Show the animated tray icon and also a hint balloon. }
  TrayIcon1.Visible := true;
  TrayIcon1.Animate := true;
  TrayIcon1.ShowBalloonHint;
end;

end.
