unit uDamRecViewer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxSplitter, Vcl.ExtCtrls,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData, Vcl.StdCtrls, Vcl.Buttons, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, uMainFTP, dxSkinsCore,
  dxScrollbarAnnotations;

type
  TfrmDamRecViewer = class(TForm)
    pnlTop: TPanel;
    pnlMiddle: TPanel;
    pnlBottom: TPanel;
    cxSplitter1: TcxSplitter;
    damRecGridDBTableView1: TcxGridDBTableView;
    damRecGridLevel1: TcxGridLevel;
    damRecGrid: TcxGrid;
    damRecGridDBTableView1recovery_id: TcxGridDBColumn;
    damRecGridDBTableView1damage_id: TcxGridDBColumn;
    damRecGridDBTableView1paid_date: TcxGridDBColumn;
    damRecGridDBTableView1damage_state: TcxGridDBColumn;
    damRecGridDBTableView1recovery_status: TcxGridDBColumn;
    damRecGridDBTableView1recovery_amount: TcxGridDBColumn;
    damRecGridDBTableView1recovery_company: TcxGridDBColumn;
    damRecGridDBTableView1recovery_date: TcxGridDBColumn;
    damRecGridDBTableView1comment: TcxGridDBColumn;
    damRecGridDBTableView1export_date: TcxGridDBColumn;
    damRecGridDBTableView1TotalAttachments: TcxGridDBColumn;
    damRecGridDBTableView1modified_date: TcxGridDBColumn;
    damRecGridDBTableView1modified_by: TcxGridDBColumn;
    btnExit: TBitBtn;
    btnRecentDamages: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRecentDamagesClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDamRecViewer: TfrmDamRecViewer;

implementation

{$R *.dfm}

uses aBaseAttachmentDMu;

procedure TfrmDamRecViewer.btnRecentDamagesClick(Sender: TObject);
var
  damThread: Tthread;
begin
  if (MessageDlg('This method will add any new damages to the recovery table.'
  +#13+#10+
  'The process takes some time.  Expect 3 to 5 minutes.'
  +#13+#10+
  'This form will close when complete', mtWarning, [mbOK, mbCancel], 0) = mrCancel) then exit;

//  damThread := Tthread.CreateAnonymousThread(
//    procedure
//    begin
      btnRecentDamages.Cursor := crSQLWait;
      frmMainFTP.imageDM.insRecentDamages.ExecSQL;
      btnRecentDamages.Cursor := crDefault;
      ShowMessage('All New Damages have been added to the Damage Recovery Table');
      frmDamRecViewer.Close;
//    end);
//  damThread.Start;
end;

procedure TfrmDamRecViewer.FormActivate(Sender: TObject);
begin
  damRecGridDBTableView1.DataController.DataSource := frmMainFTP.imageDM.dsDamRec;
  frmMainFTP.imageDM.tblDamRec.Active:= True;
end;

procedure TfrmDamRecViewer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmMainFTP.imageDM.tblDamRec.Close;
  Action := caFree;
end;

end.
