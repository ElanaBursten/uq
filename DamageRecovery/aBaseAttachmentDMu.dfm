inherited imageDM: TimageDM
  OldCreateOrder = True
  Height = 519
  Width = 540
  object qryAPI_storage_credentials: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'WhichDM'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[storage_type]'
      '      ,[enc_access_key]'
      '      ,[enc_secret_key]'
      '      ,[unc_date_created]'
      '      ,[modified_date]'
      '      ,[unc_date_expired]'
      '      ,[api_storage_constants_id]'
      '      ,[description]'
      '    FROM [QM].[dbo].[api_storage_credentials]'
      '  where id = :WhichDM')
    Left = 304
    Top = 284
  end
  object qryAPI_storage_constants: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'WhichDM'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[host]'
      '      ,[rest_url]'
      '      ,[service]'
      '      ,[bucket]'
      '      ,[bucket_folder]'
      '      ,[region]'
      '      ,[algorithm]'
      '      ,[signed_headers]'
      '      ,[content_type]'
      '      ,[accepted_values]'
      '      ,[environment]'
      '      ,[description]'
      '      ,[unc_date_modified]'
      '      ,[modified_date]'
      '  FROM [QM].[dbo].[api_storage_constants]'
      '  where id = :WhichDM')
    Left = 96
    Top = 284
  end
  object ADOConn: TADOConnection
    CommandTimeout = 900
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=ihjidojo3;Persist Security Info=Tru' +
      'e;User ID=QMParserUTL;Initial Catalog=QM;Data Source=SSDS-UTQ-QM' +
      '-02-DV'
    ConnectionTimeout = 3000
    DefaultDatabase = 'QM'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 388
    Top = 8
  end
  object spGetDamageData: TADOStoredProc
    Connection = ADOConn
    CursorType = ctStatic
    ProcedureName = 'get_damageForCMR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@DamageID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 24
  end
  object qryAttachments: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'DamageID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Declare @DamageID int;'
      'set @DamageID =  :DamageID'
      'select *'
      #9#9'from attachment'
      #9#9'where (foreign_id = @DamageID and foreign_type = 2)'
      #9#9'and orig_filename not like '#39'Screenshot%'#39
      #9#9'and active = 1'
      ''
      'union'
      'select *'
      #9#9'from attachment'
      
        #9#9'where (foreign_id IN (select ticket_id from damage where damag' +
        'e_id = @DamageID) and foreign_type = 1)'
      #9#9'and orig_filename not like '#39'Screenshot%'#39
      #9#9'and active = 1'
      ''
      'union'
      'select *'
      #9#9'from attachment'
      
        #9#9'where (foreign_type = 3 and foreign_id in (select third_party_' +
        'id from damage_third_party where damage_id = @DamageID))'
      #9#9'and orig_filename not like '#39'Screenshot%'#39
      #9#9'and active = 1'
      ''
      'union'
      'select *'
      #9#9'from attachment'
      
        #9#9'where (foreign_type = 4 and foreign_id in (select litigation_i' +
        'd from damage_litigation where damage_id = @DamageID))'
      #9#9'and orig_filename not like '#39'Screenshot%'#39
      #9#9'and active = 1'
      ''
      '')
    Left = 392
    Top = 96
  end
  object qryDamageRecovery: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DR.damage_id, DR.paid_date, DI.paid_amount'
      '  FROM [damage_recovery] DR'
      '  inner join damage_invoice DI on DR.damage_id=DI.damage_id'
      '  where DR.[paid_date] = (Select min(paid_date)'
      '                         FROM [damage_recovery]'
      '                         Where recovery_status = '#39'PENDING'#39')'
      'and DI.paid_date = DR.paid_date'
      'and recovery_status = '#39'PENDING'#39
      '')
    Left = 72
    Top = 136
  end
  object updDamageRecovery: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'RecStatus'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'TotAtt'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'damageID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'PaidDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'update damage_recovery'
      'set recovery_status = :RecStatus,'
      'export_date=getDate(),'
      'TotalAttachments= :TotAtt'
      'where damage_id = :damageID'
      'and paid_date = :PaidDate'
      '')
    Left = 224
    Top = 136
  end
  object qryDamageSummary: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'PaidDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      
        'Select Cast((SELECT DR.damage_id, DR.paid_date, DR.damage_state,' +
        'DI.Company, DI.paid_amount, DR.TotalAttachments'
      'FROM [damage_recovery] DR'
      '  inner join damage_invoice DI on DR.damage_id=DI.damage_id'
      '  where DR.[paid_date] = :PaidDate'
      '  For XML RAW, TYPE, ROOT('#39'Summary'#39'), ELEMENTS XSINIL)'
      '  As VarChar(MAX)) as xmlSummary'
      ''
      '')
    Left = 64
    Top = 200
    object qryDamageSummaryxmlSummary: TMemoField
      FieldName = 'xmlSummary'
      ReadOnly = True
      BlobType = ftMemo
    end
  end
  object resetDamageRecovery: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'PaidDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [dbo].[damage_recovery]'
      '   SET'
      '       [recovery_status] ='#39'PENDING'#39
      '      ,[TotalAttachments] = NULL'
      '      ,[export_date] = NULL'
      ' WHERE [paid_date] = :PaidDate')
    Left = 384
    Top = 160
  end
  object dsDamRec: TDataSource
    DataSet = tblDamRec
    Left = 456
    Top = 280
  end
  object tblDamRec: TADOTable
    Connection = ADOConn
    CursorType = ctStatic
    TableName = 'damage_recovery'
    Left = 456
    Top = 232
  end
  object updPreUpdateDamageRecovery: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'PaidDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'update damage_recovery'
      'set recovery_status = '#39'FAILED'#39
      'where paid_date = :PaidDate'
      'and recovery_status = '#39'PENDING'#39
      '')
    Left = 216
    Top = 200
  end
  object insRecentDamages: TADOQuery
    Connection = ADOConn
    CommandTimeout = 600
    Parameters = <>
    SQL.Strings = (
      'declare @ThisDate Date,'
      '@recoveryStatus varchar(20);'
      ''
      'set @recoveryStatus = '#39'PENDING'#39';'
      'set @ThisDate = GetDate();'
      ''
      
        'insert into [dbo].[damage_recovery]([damage_id], [paid_date], [d' +
        'amage_state], [recovery_status])'
      'select damage_id, paid_date, [damage_state], @recoveryStatus'
      'from [damage_invoice]'
      
        'where  convert(varchar,damage_id)+replace(convert(varchar,paid_d' +
        'ate,101),'#39'/'#39','#39#39') not in'
      
        '                    (SELECT convert(varchar,damage_id)+replace(c' +
        'onvert(varchar,paid_date,101),'#39'/'#39','#39#39')'
      #9#9#9#9#9#9#9#9#9#9'FROM [QM].[dbo].[damage_recovery])'
      'and ('
      
        '   ([damage_state] = '#39'CA'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'CO'#39' and datediff(M, paid_date, @ThisDate)<' +
        '25)'
      
        'or ([damage_state] = '#39'DC'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'DE'#39' and datediff(M, paid_date, @ThisDate)<' +
        '25)'
      
        'or ([damage_state] = '#39'GA'#39' and datediff(M, paid_date, @ThisDate)<' +
        '238)'
      
        'or ([damage_state] = '#39'IN'#39' and datediff(M, paid_date, @ThisDate)<' +
        '25)'
      
        'or ([damage_state] = '#39'MD'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'NJ'#39' and datediff(M, paid_date, @ThisDate)<' +
        '73)'
      
        'or ([damage_state] = '#39'NY'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'OH'#39' and datediff(M, paid_date, @ThisDate)<' +
        '25)'
      
        'or ([damage_state] = '#39'OR'#39' and datediff(M, paid_date, @ThisDate)<' +
        '73)'
      
        'or ([damage_state] = '#39'SC'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'TN'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37)'
      
        'or ([damage_state] = '#39'TX'#39' and datediff(M, paid_date, @ThisDate)<' +
        '25)'
      
        'or ([damage_state] = '#39'VA'#39' and datediff(M, paid_date, @ThisDate)<' +
        '61)'
      
        'or ([damage_state] = '#39'WA'#39' and datediff(M, paid_date, @ThisDate)<' +
        '37))'
      'order by paid_date'
      '')
    Left = 224
    Top = 32
  end
  object qryDamageData: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <
      item
        Name = 'DamageID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'USE [QM]'
      'Declare @DamageID int;'
      ''
      'set @DamageID =  :DamageID'
      ''
      'Select Cast(('
      '  select damage_id'
      #9#9',office_id'
      #9#9',damage_inv_num'
      #9#9',damage_date'
      #9#9',uq_notified_date'
      #9#9',notified_by_person'
      #9#9',notified_by_company'
      #9#9',notified_by_phone'
      #9#9',client_code'
      
        #9#9',(select [client_name] from [dbo].[client] where [client].clie' +
        'nt_id = damage.client_id) as ClientName'
      #9#9',client_claim_id'
      #9#9',date_mailed'
      #9#9',date_faxed'
      #9#9',sent_to'
      #9#9',size_type'
      #9#9',location'
      #9#9',page'
      #9#9',grid'
      #9#9',city'
      #9#9',county'
      #9#9',state'
      #9#9',utility_co_damaged'
      #9#9',diagram_number'
      #9#9',remarks'
      #9#9',investigator_id'
      #9#9',investigator_arrival'
      #9#9',investigator_departure'
      #9#9',investigator_est_damage_time'
      #9#9',investigator_narrative'
      #9#9',excavator_company'
      #9#9',excavator_type'
      #9#9',excavation_type'
      #9#9',locate_marks_present'
      #9#9',locate_requested'
      #9#9',site_mark_state'
      #9#9',site_sewer_marked'
      #9#9',site_water_marked'
      #9#9',site_catv_marked'
      #9#9',site_gas_marked'
      #9#9',site_power_marked'
      #9#9',site_tel_marked'
      #9#9',site_other_marked'
      #9#9',site_pictures'
      #9#9',site_marks_measurement'
      #9#9',site_buried_under'
      #9#9',site_clarity_of_marks'
      #9#9',site_hand_dig'
      #9#9',site_mechanized_equip'
      #9#9',site_paint_present'
      #9#9',site_flags_present'
      #9#9',site_exc_boring'
      #9#9',site_exc_grading'
      #9#9',site_exc_open_trench'
      #9#9',site_img_35mm'
      #9#9',site_img_digital'
      #9#9',site_img_video'
      #9#9',disc_repair_techs_were'
      #9#9',disc_repairs_were'
      #9#9',disc_repair_person'
      #9#9',disc_repair_contact'
      #9#9',disc_repair_comment'
      #9#9',disc_exc_were'
      #9#9',disc_exc_person'
      #9#9',disc_exc_contact'
      #9#9',disc_exc_comment'
      #9#9',disc_other1_person'
      #9#9',disc_other1_contact'
      #9#9',disc_other1_comment'
      #9#9',disc_other2_person'
      #9#9',disc_other2_contact'
      #9#9',disc_other2_comment'
      #9#9',exc_resp_code'
      #9#9',exc_resp_type'
      #9#9',exc_resp_other_desc'
      #9#9',exc_resp_details'
      #9#9',exc_resp_response'
      #9#9',uq_resp_code'
      #9#9',uq_resp_type'
      #9#9',uq_resp_other_desc'
      #9#9',uq_resp_details'
      #9#9',spc_resp_code'
      #9#9',spc_resp_type'
      #9#9',spc_resp_other_desc'
      #9#9',spc_resp_details'
      #9#9',modified_date'
      #9#9',active'
      #9#9',profit_center'
      #9#9',due_date'
      #9#9',utilistar_id'
      #9#9',closed_date'
      #9#9',modified_by'
      #9#9',uq_resp_ess_step'
      #9#9',facility_type'
      #9#9',facility_size'
      #9#9',facility_material'
      #9#9',facility_type_2'
      #9#9',facility_type_3'
      #9#9',facility_material_2'
      #9#9',facility_material_3'
      #9#9',facility_size_2'
      #9#9',facility_size_3'
      #9#9',locator_experience'
      #9#9',locate_equipment'
      #9#9',was_project'
      #9#9',claim_status'
      #9#9',claim_status_date'
      #9#9',invoice_code'
      #9#9',added_by'
      #9#9',accrual_date'
      #9#9',invoice_code_initial_value'
      #9#9',uq_damage_id'
      #9#9',estimate_locked'
      #9#9',site_marked_in_white'
      #9#9',site_tracer_wire_intact'
      #9#9',site_pot_holed'
      #9#9',site_nearest_fac_measure'
      #9#9',excavator_doing_repairs'
      #9#9',offset_visible'
      #9#9',offset_qty'
      #9#9',claim_coordinator_id'
      #9#9',locator_id'
      #9#9',estimate_agreed'
      #9#9',approved_by_id'
      #9#9',approved_datetime'
      #9#9',reason_changed'
      #9#9',work_priority_id'
      #9#9',user_changed_pc'
      #9#9',marks_within_tolerance'
      #9#9',intelex_num'
      '  ,'
      #9#9'  (select'
      #9#9#9' estimate_id'
      #9#9#9',invoice_num'
      #9#9#9',amount'
      #9#9#9',company'
      #9#9#9',received_date'
      #9#9#9',damage_city'
      #9#9#9',damage_state'
      #9#9#9',damage_date'
      #9#9#9',satisfied'
      #9#9#9',satisfied_by_emp_id'
      #9#9#9',satisfied_date'
      #9#9#9',comment'
      #9#9#9',modified_date'
      #9#9#9',approved_date'
      #9#9#9',approved_amount'
      #9#9#9',paid_date'
      #9#9#9',paid_amount'
      #9#9#9',invoice_code'
      #9#9#9',cust_invoice_date'
      #9#9#9',packet_request_date'
      #9#9#9',packet_recv_date'
      #9#9#9',invoice_approved'
      #9#9#9',litigation_id'
      #9#9#9',third_party_id'
      #9#9#9',added_by'
      #9#9#9',payment_code'
      #9#9'  from damage_invoice'
      #9#9'  where damage_id=@DamageID'
      
        #9#9'  For XML RAW, TYPE, ROOT('#39'damage_invoice'#39') , ELEMENTS XSINIL)' +
        ','
      #9#9#9'  (select damage_bill_id'
      #9#9#9#9#9' ,billing_period_date'
      #9#9#9#9#9' ,billable'
      #9#9#9#9#9' ,modified_date'
      #9#9#9#9#9' ,invoiced'
      #9#9#9'  from damage_bill'
      #9#9#9'  where damage_id=@DamageID'
      #9#9#9'  For XML RAW, TYPE, ROOT('#39'damage_bill'#39') , ELEMENTS XSINIL),'
      #9#9#9#9'(Select ticket_id'
      #9#9#9#9',ticket_number'
      #9#9#9#9',company'
      #9#9#9#9',con_type'
      #9#9#9#9',con_name'
      #9#9#9#9',con_address'
      #9#9#9#9',con_city'
      #9#9#9#9',con_state'
      #9#9#9#9',con_zip'
      #9#9#9#9',call_date'
      #9#9#9#9',caller'
      #9#9#9#9',caller_contact'
      #9#9#9#9',caller_phone'
      #9#9#9#9',caller_cellular'
      #9#9#9#9',caller_fax'
      #9#9#9#9',caller_altcontact'
      #9#9#9#9',caller_altphone'
      #9#9#9#9',caller_email'
      #9#9#9#9',image'
      #9#9#9#9'from ticket'
      
        #9#9#9#9'where ticket.ticket_id = (select ticket_id from damage where' +
        ' damage_id =@DamageID)'
      
        '                For XML RAW, TYPE, ROOT('#39'ticket'#39') , ELEMENTS XSI' +
        'NIL),'
      #9#9#9#9'  (select litigation_id'
      #9#9#9#9#9#9',file_number'
      #9#9#9#9#9#9',plaintiff'
      #9#9#9#9#9#9',carrier_id'
      #9#9#9#9#9#9',attorney'
      #9#9#9#9#9#9',party'
      #9#9#9#9#9#9',demand'
      #9#9#9#9#9#9',accrual'
      #9#9#9#9#9#9',settlement'
      #9#9#9#9#9#9',closed_date'
      #9#9#9#9#9#9',comment'
      #9#9#9#9#9#9',defendant'
      #9#9#9#9#9#9',venue'
      #9#9#9#9#9#9',venue_state'
      #9#9#9#9#9#9',service_date'
      #9#9#9#9#9#9',summons_date'
      #9#9#9#9#9#9',carrier_notified'
      #9#9#9#9#9#9',claim_status'
      #9#9#9#9#9#9',added_by'
      #9#9#9#9#9#9',modified_date'
      #9#9#9#9'  from damage_litigation'
      #9#9#9#9'  where damage_id= @DamageID'
      
        #9#9#9#9'  For XML RAW, TYPE, ROOT('#39'damage_litigation'#39') , ELEMENTS XS' +
        'INIL),'
      #9#9#9#9#9'  (select third_party_id'
      #9#9#9#9#9#9#9',claimant'
      #9#9#9#9#9#9#9',address1'
      #9#9#9#9#9#9#9',address2'
      #9#9#9#9#9#9#9',city'
      #9#9#9#9#9#9#9',state'
      #9#9#9#9#9#9#9',zipcode'
      #9#9#9#9#9#9#9',phone'
      #9#9#9#9#9#9#9',claim_desc'
      #9#9#9#9#9#9#9',uq_notified_date'
      #9#9#9#9#9#9#9',carrier_id'
      #9#9#9#9#9#9#9',carrier_notified'
      #9#9#9#9#9#9#9',demand'
      #9#9#9#9#9#9#9',claim_status'
      #9#9#9#9#9#9#9',added_by'
      #9#9#9#9#9#9#9',modified_date'
      #9#9#9#9#9'  from damage_third_party'
      #9#9#9#9#9'  where damage_id=@DamageID'
      
        #9#9#9#9#9'  For XML RAW, TYPE, ROOT('#39'damage_third_party'#39') , ELEMENTS ' +
        'XSINIL),'
      #9#9#9#9#9#9'  (select notes_id'
      #9#9#9#9#9#9#9#9',entry_date'
      #9#9#9#9#9#9#9#9',modified_date'
      #9#9#9#9#9#9#9#9',uid'
      #9#9#9#9#9#9#9#9',active'
      #9#9#9#9#9#9#9#9',note'
      #9#9#9#9#9#9#9#9',sub_type'
      #9#9#9#9#9#9'  ,case foreign_type'
      #9#9#9#9#9#9#9'when 2 then 0'
      #9#9#9#9#9#9#9'when 3 then 1'
      #9#9#9#9#9#9#9'when 4 then 2'
      #9#9#9#9#9#9#9'else null end as damage_notes_type,'
      #9#9#9#9#9#9'   @DamageID as damage_id'
      #9#9#9#9#9#9'  from notes'
      #9#9#9#9#9#9'  where (foreign_id = @DamageID and foreign_type = 2)'
      
        #9#9#9#9#9#9'   or (foreign_id in (select third_party_id from damage_th' +
        'ird_party'
      #9#9#9#9#9#9'   where damage_id = @DamageID) and foreign_type = 3)'
      
        #9#9#9#9#9#9'   or (foreign_id in (select litigation_id from damage_lit' +
        'igation'
      #9#9#9#9#9#9'   where damage_id = @DamageID) and foreign_type = 4)'
      #9#9#9#9#9#9'  For XML RAW, TYPE, ROOT('#39'Notes'#39') , ELEMENTS XSINIL)'
      ''
      '    from damage'
      '  where damage_id=@DamageID'
      '  For XML RAW, TYPE, ROOT('#39'damage'#39') , ELEMENTS XSINIL)'
      '  As VarChar(MAX)) as DamageXmlOut')
    Left = 72
    Top = 80
  end
  object qryAllSubmittedDamages: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DR.damage_id'
      '  FROM [damage_recovery] DR'
      'where recovery_status = '#39'SUBMITTED'#39
      '')
    Left = 88
    Top = 376
  end
end
