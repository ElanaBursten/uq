inherited DamageShortDM: TDamageShortDM
  OldCreateOrder = True
  Height = 193
  Width = 359
  object dsDamageShort: TDataSource
    DataSet = DamageShort
    Left = 120
    Top = 24
  end
  object Pipe: TppDBPipeline
    DataSource = dsDamageShort
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'Pipe'
    Left = 197
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 10160
    PrinterSetup.mmMarginRight = 10160
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'PDF'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    ThumbnailSettings.PageHighlight.Width = 3
    ThumbnailSettings.ThumbnailSize = tsSmall
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PDFSettings.PDFAFormat = pafNone
    PreviewFormSettings.PageBorder.mmPadding = 0
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowCancelDialog = False
    ShowPrintDialog = False
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 248
    Top = 24
    Version = '20.01'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppTitleBand1: TppTitleBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 9260
      mmPrintPosition = 0
      object ppLabel89: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label89'
        HyperlinkEnabled = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Damage Investigation Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Book Antiqua'
        Font.Size = 20
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 9260
        mmLeft = 265
        mmTop = 0
        mmWidth = 195527
        BandType = 1
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object DamageSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'DamageSection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 265
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport9: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          NoDataBehaviors = [ndBlankReport]
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object DamageHeaderBand: TppHeaderBand
            Visible = False
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand10: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 164042
            mmPrintPosition = 0
            object ppLabel1: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'INVESTIGATOR:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 28693
              mmTop = 2646
              mmWidth = 21844
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel14: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label14'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'DATE MAILED:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 30375
              mmTop = 6350
              mmWidth = 19897
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel15: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label15'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'DATE FAXED:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 31730
              mmTop = 10054
              mmWidth = 18542
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel16: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label16'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'SENT TO:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 37233
              mmTop = 13758
              mmWidth = 13039
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText15: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText15'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'sent_to'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3705
              mmLeft = 52917
              mmTop = 13758
              mmWidth = 529
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText14: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText14'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'date_faxed'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 10054
              mmWidth = 529
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText13: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText13'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'date_mailed'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 6350
              mmWidth = 14287
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText2: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText2'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'investigator_name'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 53181
              mmTop = 2646
              mmWidth = 31221
              BandType = 4
              LayerName = Foreground3
            end
            object ppShape1: TppShape
              DesignLayer = ppDesignLayer4
              UserName = 'Shape1'
              mmHeight = 5027
              mmLeft = 265
              mmTop = 23813
              mmWidth = 195527
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText5: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText3'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'damage_date'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 53711
              mmWidth = 12700
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel6: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label3'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'DATE DAMAGE DISCOVERED:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 9631
              mmTop = 53711
              mmWidth = 40640
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText6: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText4'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'uq_notified_date'
              DataPipeline = Pipe
              DisplayFormat = 'm/d/yyyy h:nn AM/PM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 57679
              mmWidth = 23548
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel7: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label4'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'LOCATING COMPANY NOTIFIED:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 6244
              mmTop = 57679
              mmWidth = 44027
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText7: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText5'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'notified_by_person'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 49742
              mmWidth = 21695
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel8: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label5'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'NOTIFIED BY CONTACT:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 107114
              mmTop = 49742
              mmWidth = 32851
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText8: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText6'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'notified_by_company'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 53711
              mmWidth = 25929
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel9: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label6'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'NOTIFIED BY COMPANY:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 106522
              mmTop = 53711
              mmWidth = 33443
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText9: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText9'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'notified_by_phone'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 57679
              mmWidth = 17991
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel10: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label10'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'PHONE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 129382
              mmTop = 57679
              mmWidth = 10583
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText12: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText12'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'client_claim_id'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 29898
              mmWidth = 3968
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel13: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label7'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'CLIENT CLAIM ID:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 115835
              mmTop = 29898
              mmWidth = 24130
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel17: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label17'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'LINE SIZE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 35962
              mmTop = 33867
              mmWidth = 14309
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText17: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText17'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'location'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3705
              mmLeft = 52917
              mmTop = 37835
              mmWidth = 15346
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel18: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label18'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'DAMAGE LOCATION:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 21823
              mmTop = 37835
              mmWidth = 28448
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText18: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText18'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'page'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 41804
              mmWidth = 16668
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel19: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label19'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'MAP PAGE/AREA:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 115750
              mmTop = 41804
              mmWidth = 24215
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel20: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label20'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'MAP GRID:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 125148
              mmTop = 45773
              mmWidth = 14817
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText20: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText20'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'city'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 41804
              mmWidth = 19579
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel21: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label21'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'CITY:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 42820
              mmTop = 41804
              mmWidth = 7451
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText21: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText21'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'county'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 45773
              mmWidth = 10848
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel22: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label22'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'COUNTY:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 37571
              mmTop = 45773
              mmWidth = 12700
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText22: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText22'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'state'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 49742
              mmWidth = 3439
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel23: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label23'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'STATE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 40111
              mmTop = 49742
              mmWidth = 10160
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText23: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText23'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'utility_co_damaged'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 29898
              mmWidth = 36512
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel24: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label24'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'CLIENT DAMAGED:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 24195
              mmTop = 29898
              mmWidth = 26077
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText24: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText24'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'diagram_number'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 33867
              mmWidth = 529
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel25: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label25'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'CLIENT MAP:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3302
              mmLeft = 122016
              mmTop = 33867
              mmWidth = 17949
              BandType = 4
              LayerName = Foreground3
            end
            object ppLabel3: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label8'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'REPORTED DAMAGE INFORMATION'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 3969
              mmTop = 24342
              mmWidth = 62177
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText19: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText19'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'grid'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 142082
              mmTop = 45773
              mmWidth = 529
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText16: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText16'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'facility_size_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3704
              mmLeft = 52917
              mmTop = 33867
              mmWidth = 14023
              BandType = 4
              LayerName = Foreground3
            end
            object ExcavatorRegion: TppRegion
              DesignLayer = ppDesignLayer4
              UserName = 'ExcavatorRegion'
              Brush.Style = bsClear
              Caption = 'ExcavatorRegion'
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              Transparent = True
              mmHeight = 39158
              mmLeft = 0
              mmTop = 64029
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground3
              mmBottomOffset = 1270
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppShape7: TppShape
                DesignLayer = ppDesignLayer4
                UserName = 'Shape7'
                ParentWidth = True
                mmHeight = 5027
                mmLeft = 0
                mmTop = 79375
                mmWidth = 195580
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText29: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText29'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'excavator_company'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 68527
                mmWidth = 45509
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel32: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label32'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATION COMPANY:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 14120
                mmTop = 68528
                mmWidth = 34036
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText30: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText30'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'excavator_type_desc'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 68527
                mmWidth = 20902
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel33: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label33'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR TYPE:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 111241
                mmTop = 68528
                mmWidth = 26501
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText31: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText31'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'excavation_type_desc'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 73290
                mmWidth = 23548
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel34: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label34'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATION TYPE:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 110564
                mmTop = 73290
                mmWidth = 27178
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText33: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText33'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'locate_requested'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 85990
                mmWidth = 2117
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel36: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label36'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'LOCATE REQUESTED:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 17761
                mmTop = 85990
                mmWidth = 30395
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText34: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText34'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'ticket_number'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 85990
                mmWidth = 14287
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel37: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label37'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'TICKET NUMBER:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 113612
                mmTop = 85990
                mmWidth = 24130
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel5: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label9'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'TICKET INFORMATION'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 1852
                mmTop = 79904
                mmWidth = 38629
                BandType = 4
                LayerName = Foreground3
              end
              object ppShape3: TppShape
                DesignLayer = ppDesignLayer4
                UserName = 'Shape3'
                ParentWidth = True
                mmHeight = 5027
                mmLeft = 0
                mmTop = 62177
                mmWidth = 195580
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel4: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label11'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR INFORMATION'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 2117
                mmTop = 62177
                mmWidth = 48154
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel12: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label12'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EQUIPMENT TYPE:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 22585
                mmTop = 73290
                mmWidth = 25569
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel26: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label26'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'DATE REQUSTED:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 23517
                mmTop = 89959
                mmWidth = 24638
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText32: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText32'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'transmit_date'
                DataPipeline = MasterPipe
                DisplayFormat = 'm/d/yyyy h:nn AM/PM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'MasterPipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 89959
                mmWidth = 24077
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel27: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label27'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'DATE COMPLETED:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 21399
                mmTop = 93927
                mmWidth = 26755
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel30: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label30'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'LOCATE PERFORMED BY:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 102289
                mmTop = 89959
                mmWidth = 35560
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel35: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label35'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'OFFICE:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 126758
                mmTop = 93927
                mmWidth = 11091
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText36: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText36'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'completed_date'
                DataPipeline = MasterPipe
                DisplayFormat = 'm/d/yyyy h:nn AM/PM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'MasterPipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 93927
                mmWidth = 24077
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText37: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText37'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'locator_name'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 89959
                mmWidth = 19050
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText38: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText38'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'profit_center_name'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 93927
                mmWidth = 19844
                BandType = 4
                LayerName = Foreground3
              end
            end
            object SiteInfoRegion: TppRegion
              DesignLayer = ppDesignLayer4
              UserName = 'SiteInfoRegion'
              KeepTogether = True
              Brush.Style = bsClear
              Caption = 'SiteInfoRegion'
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              Transparent = True
              mmHeight = 41804
              mmLeft = 0
              mmTop = 101865
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground3
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppShape5: TppShape
                DesignLayer = ppDesignLayer4
                UserName = 'Shape5'
                ParentWidth = True
                mmHeight = 5027
                mmLeft = 0
                mmTop = 100012
                mmWidth = 195580
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText47: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText47'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'site_clarity_of_marks'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139965
                mmTop = 106627
                mmWidth = 7937
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel50: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label50'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'CLARITY OF MARKS:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 108955
                mmTop = 106627
                mmWidth = 28787
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText50: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText50'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'site_paint_present'
                DataPipeline = Pipe
                DisplayFormat = 'Yes;No'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 106627
                mmWidth = 5292
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel53: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label53'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'PAINT MARKS PRESENT:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 14035
                mmTop = 106627
                mmWidth = 34121
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel11: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label13'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'SITE INFORMATION'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 2117
                mmTop = 100541
                mmWidth = 33867
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel31: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label31'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'ARRIVAL TIME:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3175
                mmLeft = 4763
                mmTop = 135732
                mmWidth = 20902
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText10: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText10'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'investigator_arrival'
                DataPipeline = Pipe
                DisplayFormat = 'm/d/yyyy h:nn AM/PM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 27252
                mmTop = 135732
                mmWidth = 529
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel49: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label49'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'DEPARTURE TME:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3175
                mmLeft = 57679
                mmTop = 135732
                mmWidth = 24871
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText11: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText11'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'investigator_departure'
                DataPipeline = Pipe
                DisplayFormat = 'm/d/yyyy h:nn AM/PM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 84931
                mmTop = 135732
                mmWidth = 530
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel76: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label76'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'ESTIMATE DAMAGE TIME:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3175
                mmLeft = 118269
                mmTop = 135732
                mmWidth = 35983
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText25: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText25'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'investigator_est_damage_time'
                DataPipeline = Pipe
                DisplayFormat = 'm/d/yyyy h:nn AM/PM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 155575
                mmTop = 135732
                mmWidth = 529
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel38: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label38'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'TRACER WIRE WAS INTACT:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 9293
                mmTop = 114565
                mmWidth = 38862
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel39: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label39'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'LOCATE AREA MARKED ON:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 9378
                mmTop = 118534
                mmWidth = 38777
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText35: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText35'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'tracer_wire_intact_desc'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 114565
                mmWidth = 529
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText39: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText39'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'site_buried_under'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 50800
                mmTop = 118534
                mmWidth = 4233
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel40: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label40'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'AREA WAS MARKED IN WHITE:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 95684
                mmTop = 114565
                mmWidth = 42164
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText40: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText40'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'marked_in_white_desc'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 139700
                mmTop = 114565
                mmWidth = 3440
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel42: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label42'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'DISTANCE FROM DAMAGE TO NEAREST FACILITY:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 12943
                mmTop = 125413
                mmWidth = 69342
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText42: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText42'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'site_nearest_fac_measure'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 84931
                mmTop = 125413
                mmWidth = 3175
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel43: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label43'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'WHO DID THE EXCAVATION FOR THE REPAIR:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3175
                mmLeft = 5821
                mmTop = 129382
                mmWidth = 76729
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText48: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText48'
                HyperlinkEnabled = False
                AutoSize = True
                Border.mmPadding = 0
                DataField = 'excavator_doing_repairs'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 84931
                mmTop = 129382
                mmWidth = 25930
                BandType = 4
                LayerName = Foreground3
              end
            end
          end
          object DamageFooterBand: TppFooterBand
            Visible = False
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 6615
            mmPrintPosition = 0
            object ppShape2: TppShape
              DesignLayer = ppDesignLayer4
              UserName = 'Shape14'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 1058
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground3
            end
            object ppSystemVariable6: TppSystemVariable
              DesignLayer = ppDesignLayer4
              UserName = 'SystemVariable5'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 171715
              mmTop = 1588
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground3
            end
            object ppCalc3: TppCalc
              DesignLayer = ppDesignLayer4
              UserName = 'Calc2'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 48419
              mmTop = 1588
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground3
            end
            object ppLabel105: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label109'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Damage Details'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3302
              mmLeft = 529
              mmTop = 1588
              mmWidth = 20405
              BandType = 8
              LayerName = Foreground3
            end
          end
          object ppGroup1: TppGroup
            BreakName = 'damage_id'
            DataPipeline = Pipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            KeepTogether = True
            OutlineSettings.CreateNode = True
            StartOnOddPage = False
            UserName = 'Group1'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'Pipe'
            NewFile = False
            object DamageIDGroupHeaderBand: TppGroupHeaderBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 8202
              mmPrintPosition = 0
              object ppLine1: TppLine
                DesignLayer = ppDesignLayer4
                UserName = 'Line1'
                Border.mmPadding = 0
                Pen.Width = 2
                ParentWidth = True
                Weight = 1.500000000000000000
                mmHeight = 1588
                mmLeft = 0
                mmTop = 6614
                mmWidth = 195580
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
              object ppDBText1: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText1'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_damage_id'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 12
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4995
                mmLeft = 31485
                mmTop = 1323
                mmWidth = 32544
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
              object ppLabel2: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label1'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'INVESTIGATION #:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3440
                mmLeft = 265
                mmTop = 2381
                mmWidth = 28046
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
            end
            object ppGroupFooterBand6: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object ppDesignLayers4: TppDesignLayers
            object ppDesignLayer4: TppDesignLayer
              UserName = 'Foreground3'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppFooterBand5: TppFooterBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object ppSummaryBand1: TppSummaryBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object MasterPipe: TppDBPipeline
    DataSource = dsDamageShort
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'MasterPipe'
    Left = 197
    Top = 87
  end
  object DamageShort: TADOQuery
    Connection = imageDM.ADOConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'damageId'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'USE [QM]'
      'SET ANSI_NULLS ON'
      'SET QUOTED_IDENTIFIER ON'
      ''
      'declare'
      #9'@damage_id int'
      ''
      'set @damage_id = :damageID'
      ''
      'set nocount on'
      ''
      '-- return damage data'
      'select damage.*, ticket.ticket_number,  ticket.transmit_date,'
      '  Coalesce(employee.short_name, '#39#39') as investigator_name,'
      '  Coalesce(pc_name, profit_center) as '#39'profit_center_name'#39','
      '  r1.description as '#39'excavator_type_desc'#39','
      '  r2.description as '#39'excavation_type_desc'#39','
      
        '  Coalesce(r3.description, damage.size_type, '#39#39') as '#39'facility_si' +
        'ze_desc'#39','
      '  r4.description as '#39'tracer_wire_intact_desc'#39','
      '  r5.description as '#39'marked_in_white_desc'#39','
      '  r6.description as '#39'site_pot_holed_desc'#39','
      '  r7.description as '#39'marks_within_tolerance_desc'#39','
      '  Coalesce(locator.short_name, '#39#39') as locator_name,'
      '  Coalesce(approver.short_name, '#39#39') as approver_name,'
      
        '  case when damage.damage_type = '#39'APPROVED'#39' then '#39'Yes'#39' else '#39'No'#39 +
        ' end as approved,'
      '  damage.approved_datetime,'
      '  r8.description as '#39'exc_resp_desc'#39','
      '  r9.description as '#39'exc_resp_type_desc'#39','
      '  r10.description as '#39'uq_resp_desc'#39','
      '  r11.description as '#39'uq_resp_type_desc'#39','
      '  r12.description as '#39'spc_resp_desc'#39','
      '  r13.description as '#39'spc_resp_type_desc'#39', ticket.ticket_id,'
      '  (select  completed_date = (select min(l.closed_date)'
      '  from locate l, assignment a'
      '  where l.ticket_id = ticket.ticket_id'
      '  and l.locate_id = a.locate_id'
      '  and a.active = 1'
      '  and l.closed = 1)) as completed_date'
      'from damage'
      
        '  left join profit_center on profit_center.pc_code = damage.prof' +
        'it_center'
      '  left join ticket on ticket.ticket_id = damage.ticket_id'
      '  left join employee on employee.emp_id = damage.investigator_id'
      
        '  left join reference r1 on (r1.type='#39'excrtype'#39' and r1.code=dama' +
        'ge.excavator_type)'
      
        '  left join reference r2 on (r2.type='#39'excntype'#39' and r2.code=dama' +
        'ge.excavation_type)'
      
        '  left join reference r3 on (r3.type='#39'facsize'#39' and r3.code=damag' +
        'e.facility_size and r3.modifier=damage.facility_type)'
      
        '  left join reference r4 on (r4.type='#39'yesnoblank'#39' and r4.code=da' +
        'mage.site_tracer_wire_intact)'
      
        '  left join reference r5 on (r5.type='#39'yesnoblank'#39' and r5.code=da' +
        'mage.site_marked_in_white)'
      
        '  left join reference r6 on (r6.type='#39'ynub'#39' and r6.code=damage.s' +
        'ite_pot_holed)'
      
        '  left join reference r7 on (r7.type='#39'yesnoblank'#39' and r7.code=da' +
        'mage.marks_within_tolerance)'
      
        '  left join reference r8 on (r8.type='#39'exres'#39' and r8.code=damage.' +
        'exc_resp_code)'
      
        '  left join reference r9 on (r9.type in ('#39'ex1resp'#39','#39'ex2resp'#39','#39'ex' +
        '7resp'#39') and r9.code=damage.exc_resp_type)'
      
        '  left join reference r10 on (r10.type='#39'uqres'#39' and r10.code=dama' +
        'ge.uq_resp_code)'
      
        '  left join reference r11 on (r11.type='#39'uqresp'#39' and r11.code=dam' +
        'age.uq_resp_type)'
      
        '  left join reference r12 on (r12.type='#39'utres'#39' and r12.code=dama' +
        'ge.spc_resp_code)'
      
        '  left join reference r13 on (r13.type='#39'spresp'#39' and r13.code=dam' +
        'age.spc_resp_type)'
      
        '  left join employee locator on (locator.emp_id = dbo.get_locato' +
        'r_for_damage(damage_id))'
      
        '  left join employee approver on (approver.emp_id = damage.appro' +
        'ved_by_id)'
      'where damage.damage_id = @damage_id'
      '')
    Left = 32
    Top = 24
  end
end
