program DRAT;
{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  Vcl.Forms,
  Windows,
  SysUtils,
  uMainFTP in 'uMainFTP.pas' {frmMainFTP},
  GlobalSU in 'GlobalSU.pas',
  miniCVUtils in 'miniCVUtils.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  ServerAttachmentDMu in '..\server\ServerAttachmentDMu.pas' {ServerAttachment: TDataModule},
  XSuperObject in '..\thirdparty\XSuperObject\XSuperObject.pas',
  XSuperJSON in '..\thirdparty\XSuperObject\XSuperJSON.pas',
  QMConst in '..\client\QMConst.pas',
  OdUqInternet in '..\common\OdUqInternet.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  aBaseAttachmentDMu in 'aBaseAttachmentDMu.pas' {imageDM: TDataModule},
  uDamRecViewer in 'uDamRecViewer.pas' {frmDamRecViewer},
  DamageShort in 'DamageShort.pas' {DamageShortDM: TDataModule};

var
  MyInstanceName: string;

begin
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName+'_'+paramstr(1));
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamCount < 2 then
    begin
      Application.ShowMainForm := false;
    end
    else
    begin
      Application.ShowMainForm := true;

    end;
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Title := MyInstanceName;
    Application.CreateForm(TfrmMainFTP, frmMainFTP);
  Application.Run;
  end;

end.
