program Rerouter;
// qm-286  Brian Pike
{$APPTYPE CONSOLE}
{$R '..\QMIcon.res'}
{$R 'QMVersion.res' '..\QMVersion.rc'}
uses
  System.SysUtils,
  IniFiles,
  ADODB,
  ActiveX,
  GlobalSU in 'GlobalSU.pas',
  uRerouterLogger in 'uRerouterLogger.pas';

var
  ADOConn: TADOConnection;
  cmdUnrouter: TADOQuery;
  ticketids: TADOQuery;
  counter: Integer;
  empID: Integer;
  RowsAffected: Integer;
  MyInstanceName: string;
  AppPath: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  logConnStr:String;
const
  TicketIDSQL: String = 'select ticket_id from locate where locate.assigned_to= %s'; //qm-706 BP
  UnrouteSQL: String = 'set nocount on ' +
    'declare @locate_ids table (locate_id int not null) ' +
    'insert into @locate_ids ' +
    'select locate_id from locate where locate.assigned_to= %s ' +
    'update assignment set active = 0 where active = 1 and locate_id in (select locate_id from @locate_ids) ' +
    'set nocount off ' +
    'update locate SET status=''-P'', closed=0, assigned_to=null ' +
    'where locate_id in (select locate_id from @locate_ids)';

procedure ReadINI;
var
  ReroutingINI: TIniFile;
  connStr: String;
  IniFilename: TFileName;
begin
  try
    try
     AppPath := ExtractFilePath(paramstr(0));
     ReroutingINI := TIniFile.Create(AppPath + 'Rerouter.ini');
     IniFilename := ReroutingINI.FileName;

     aServer := ReroutingINI.ReadString('Database', 'Server', '');
     aDatabase := ReroutingINI.ReadString('Database', 'DB', 'QM');
     ausername := ReroutingINI.ReadString('Database', 'UserName', '');
     apassword := ReroutingINI.ReadString('Database', 'Password', '');

     LogPath :=  ReroutingINI.ReadString('Paths', 'LogPath', 'C:\QM\Logs');  //qm-706 BP
     ForceDirectories(LogPath);

    except
      raise;
    end;
  finally
    ReroutingINI.Free;
  end;
end;

function ConnectDB: Boolean;
var
  AppPath: String;
  connStr: String;

  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  EEmpIDError: Exception;
begin
  try
    AppPath := ExtractFilePath(paramstr(0));
    ADOConn := TADOConnection.Create(nil);
    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +
      ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
      aDatabase + ';Data Source=' + aServer;
    logConnStr := 'Provider=SQLNCLI11.1;Password=' + 'Not Shown' +           //qm-706 BP
      ';Persist Security Info=True;User ID=' + ausername + ';Initial Catalog=' +
      aDatabase + ';Data Source=' + aServer;
    ADOConn.ConnectionString:= connStr;
    ADOConn.Open;
  except
    raise;
  end;
end;

begin
  // ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  MyInstanceName := ExtractFileName(paramstr(0));
  if CreateSingleInstance(MyInstanceName) then
  begin
    LogPath := ExtractFilePath(Paramstr(0));
    empID := 0;
    if (ParamCount >= 1) then
      empID := StrtoIntDef(paramstr(1), 0);
    if empID = 0 then
    begin
      LogResult.LogType := ltWarning;
      LogResult.Status := 'Process failure';
      LogResult.DataStream:= logConnStr;
      LogResult.ExcepMsg:= 'Emp Id parameter is missing or invalid';
      WriteLog(LogResult);
      Halt(0);
    end;

    try
      CoInitialize(nil);
      try
        ReadIni;
      except
        on E: Exception do
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'ReadIni';
          LogResult.DataStream:= logConnStr;
          LogResult.Status := 'Process Failed';
          LogResult.ExcepMsg := 'Failed to read from Ini file: ' + E.Message;
          WriteLog(LogResult);
          Exit;
        end;
      end;

      try
        ConnectDB;
      except
        on E: Exception do
        begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'connectDB';
          LogResult.DataStream := logConnStr;
          LogResult.Status := 'Connection failed ';
          LogResult.ExcepMsg := E.Message;
          WriteLog(LogResult);
          Exit;
        end;
      end;

      cmdUnrouter := TADOQuery.Create(nil);
      cmdUnrouter.Connection := ADOConn;
      cmdUnrouter.SQL.Text := format(UnrouteSQL, [paramstr(1)]);

      ticketids := TADOQuery.Create(nil);
      ticketids.Connection := ADOConn;
      ticketids.SQL.Text :=  format(TicketIDSQL, [paramstr(1)]);

      ADOConn.BeginTrans;
      try
        ticketIDs.Open;
        RowsAffected := cmdUnrouter.ExecSQL;
        ClearLogRecord;
        LogResult.LogType := ltInfo;
        LogResult.DataStream := 'EmpID# ' + inttoStr(empID) + ': ' + inttoStr(RowsAffected) +
          ' Rows Affected' + slinebreak + 'Tickets: ' +slinebreak;

        ticketids.First;
        counter := 1;
        while not ticketids.EOF do
        begin
           LogResult.DataStream := LogResult.DataStream +
               Ticketids.FieldbyName('ticket_id').Asstring;
          if (counter <> Ticketids.RecordCount) then
          begin
            If (counter mod 10) <> 0 then
             LogResult.DataStream := LogResult.DataStream + ','
            else
              LogResult.DataStream := LogResult.DataStream + slinebreak;
          end;
          ticketids.Next;
          Inc(counter);
        end;

        WriteLog(LogResult);
        ADOConn.CommitTrans;
      except
        on E: Exception do
        begin
          ADOConn.RollbackTrans;
          ClearLogRecord;
          LogResult.LogType := ltError;
          LogResult.Status := 'Transaction failure';
          LogResult.ExcepMsg := E.Message;
          LogResult.DataStream := 'EmpID# ' + inttoStr(empID);
          WriteLog(LogResult);
        end;
      end;
    finally
      cmdUnrouter.Free;
      ticketids.Free;
      ADOConn.Close;
      ADOConn.Free;
      CoUninitialize;
    end;
  end;

end.
