program SVGlogview;

{$APPTYPE CONSOLE}

uses
  Forms,
  LogConvert in 'LogConvert.pas',
  SvgWriter in 'SvgWriter.pas',
  LogicLogParser in 'LogicLogParser.pas';

{$R *.res}

begin
  if ParamCount<1 then begin
    WriteLn('Please specify the log file name');
    Exit;
  end;

  RenderLogAsSvg(ParamStr(1));

end.
