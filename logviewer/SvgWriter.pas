unit SvgWriter;

interface

uses
  Classes, SysUtils;

type
  TSvgWriter = class(TObject)
  private
    output: TStringList;
    FFontFamily: string;
    FFontSize: Single;
    procedure InitOutput;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddCircle(x, y, r: Single; Color: string);
    procedure AddLine(x1, y1, x2, y2: Single; Stroke: string);
    procedure AddRectangle(x, y, w, h: Single; Color: string);
    procedure AddOutlinedRectangle(x, y, w, h: Single; Color: string);
    procedure AddRoundRect(x, y, w, h, rx, ry: Single; Color: string);
    procedure SetFont(FontFamily: string; FontSize: Single);
    procedure AddText(x, y: Single; Text, Color: string);
    procedure AddTextClipped(x, y, w, h: Single; Text, Color: string);

    procedure SaveToFile(const FileName: string);
  end;


implementation

{ TSvgWriter }

constructor TSvgWriter.Create;
begin
  output := TStringList.Create;
  InitOutput;
end;

procedure TSvgWriter.AddRectangle(x, y, w, h: Single; Color: string);
begin
  Output.Add(Format('<rect x="%g" y="%g" width="%g" height="%g" fill="%s"/>',
    [x, y, w, h, Color]));
end;

procedure TSvgWriter.AddOutlinedRectangle(x, y, w, h: Single; Color: string);
begin
  Output.Add(Format('<rect x="%g" y="%g" width="%g" height="%g" fill="%s" stroke="red" stroke-width="1.2"/>',
    [x, y, w, h, Color]));
end;

procedure TSvgWriter.AddRoundRect(x, y, w, h, rx, ry: Single; Color:
  string);
begin
  Output.Add(Format('<rect x="%g" y="%g" rx="%g" ry="%g" width="%g" height="%g" fill="%s"/>', [x, y, rx, ry, w, h, Color]));
end;

procedure TSvgWriter.AddLine(x1, y1, x2, y2: Single; Stroke: string);
begin
  Output.Add(Format('<line x1="%g" y1="%g" x2="%g" y2="%g" stroke="%s" stroke-width="0.5"/>', [x1,
    y1, x2, y2, Stroke]));
end;

procedure TSvgWriter.AddCircle(x, y, r: Single; Color: string);
begin
  Output.Add(Format('<circle cx="%g" cy="%g" r="%g" fill="%s"/>', [x, y, r,
    Color]));
end;

destructor TSvgWriter.Destroy;
begin
  output.Free;
  inherited;
end;

procedure TSvgWriter.SaveToFile(const FileName: string);
begin
  Output.Add('</svg>');
  Output.SaveToFile(FileName);
  InitOutput;
end;

procedure TSvgWriter.InitOutput;
begin
  Output.Clear;
  Output.Add('<?xml version="1.0" standalone="no"?>');
  Output.Add('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"');
  Output.Add('"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">');
  Output.Add('<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" zoomAndPan="magnify" style="overflow:scroll;">');
end;

procedure TSvgWriter.AddText(x, y: Single; Text, Color: string);
begin
  output.Add(format('<text x="%g" y="%g" style="font-size:5; font-family:Verdana; fill:%s" text-anchor="start">%s</text>', [x, y, Color, text]));
end;

procedure TSvgWriter.AddTextClipped(x, y, w, h: Single; Text,
  Color: string);
begin
  output.Add('<svg x="%g" y="%g" width="100" height="100" style="overflow:scroll;">');

  output.Add('</svg>');
end;

procedure TSvgWriter.SetFont(FontFamily: string;
  FontSize: Single);
begin
  FFontFamily := FontFamily;
  FFontSize := FontSize;
end;


end.
