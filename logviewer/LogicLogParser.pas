unit LogicLogParser;

interface

uses
  Classes, SysUtils;

type
  TLogicLogParser = class
  private
    FileContents: TStringList;
    PreviousOps: array of Integer;
    procedure LoadData;
    procedure Arrange;
  public
    StartTime: TDateTime;
    CpuMeasurements: TList;
    Operations: TList;
    constructor Create(FileName: string);
    destructor Destroy; override;
  end;

  TCpuMeasurement = class
    When: TDateTime;
    Usage: Single;
    constructor Create(When: TDateTime; Usage: Single);
  end;

  TOperation = class
    OpId: string;
    Start: TDateTime;
    Stop: TDateTime;
    OpName: string;
    Success: Boolean;
    FailureMessage: string;
    BarX, BarY: Single;
  end;

const
  SecondsInDay = 60 * 60 * 24;

function AddSeconds(InTime: TDateTime; sec: Single): TDateTime;

implementation

uses Math;

{ TLogicLogParser }

function ParseEventDateFromLine(LogLine: string): TDateTime;
begin
  Result := StrToDateTime(Format('%s/%s/%s %s', [Copy(LogLine, 6, 2),
    Copy(LogLine, 9, 2), Copy(LogLine, 1, 4), Copy(LogLine, 12, 8)]));
end;

function CompareOperationsByStartDate(Item1, Item2: Pointer): Integer;
var
  Op1, Op2: TOperation;
begin
  Op1 := Item1;
  Op2 := Item2;
  Result := Sign(Op1.Start - Op2.Start);
end;

procedure TLogicLogParser.Arrange;
begin
  Operations.Sort(CompareOperationsByStartDate);
end;

constructor TLogicLogParser.Create(FileName: string);
var
  I: Integer;
begin
  FileContents := TStringList.Create;
  SetLength(PreviousOps, 2000000);
  for I := Low(PreviousOps) to High(PreviousOps) do
    PreviousOps[I] := -1;

  CpuMeasurements := TList.Create;
  Operations := TList.Create;

  FileContents.LoadFromFile(FileName);
  StartTime := ParseEventDateFromLine(FileContents[0]);
  LoadData;
  Arrange;
end;

destructor TLogicLogParser.Destroy;
begin
  FileContents.Free;
  CpuMeasurements.Free;
  Operations.Free;
  inherited;
end;

function AddSeconds(InTime: TDateTime; sec: Single): TDateTime;
begin
  Result := InTime + (sec / (SecondsInDay));
end;

function ParseTime(LogTime: string): TDateTime;
// convert string of format 00:06.850
begin
  Result := (StrToInt(Copy(LogTime, 1, 2)) * 60 + StrToFloat(Copy(LogTime, 4,
    6))) / SecondsInDay;
end;

procedure TLogicLogParser.LoadData;
var
  Fields: TStringList;
  i: Integer;
  Line: string;
  Stop: TDateTime;
  Op: TOperation;
  PrevIndex: Integer;
  OpIdInt: Integer;
begin
  Fields := TStringList.Create;
  try
    Fields.Delimiter := ' ';
    Fields.QuoteChar := '"';
    for i := 1 to FileContents.Count - 1 do begin
      try
        Line := FileContents[i];
        if Length(Line)<20 then
          Continue;

        if StrToIntDef(Copy(Line, 1, 4), 0) < 1990 then  // it does not start with a year
          Continue;

        Fields.DelimitedText := Line;
        Stop := ParseEventDateFromLine(Line);
        if Pos('CPU Usage', Line) > 0 then begin
          CpuMeasurements.Add(TCpuMeasurement.Create(Stop,
            StrToFloat(Fields[7])));
        end
        else begin
          Op := TOperation.Create;
          Op.Start := Stop - ParseTime(Fields[5]);
          Op.Stop := Stop;
          Op.OpName := Fields[4];
          Op.OpId := Fields[2];
          Op.Success := True;
          Op.FailureMessage := '';

          if Pos('QM Server Error', Fields[8])>0 then begin
            Op.Success := False;
            Op.FailureMessage := Fields[8];
          end;

          OpIdInt := StrToInt(Op.OpId);
          PrevIndex := PreviousOps[OpIdInt];
          if PrevIndex > -1 then begin
            Operations.Items[PrevIndex] := Op;
            // just ignore the mem leak of the old one.
          end else begin
            Operations.Add(Op);
            PreviousOps[OpIdInt] := Operations.Count-1;
          end;
        end;
      except
        on E: Exception do begin
          E.Message := E.Message + Format(' on line %d (%s)', [i + 1, Line]);
          raise;
        end;
      end;
    end;
  finally
    Fields.Free;
  end;
end;

{ TCpuMeasurement }

constructor TCpuMeasurement.Create(When: TDateTime; Usage: Single);
begin
  Self.When := When;
  Self.Usage := Usage;
end;

{ TOperation }

end.

