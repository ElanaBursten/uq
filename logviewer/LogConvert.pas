unit LogConvert;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  DateUtils, Dialogs, StdCtrls, OleCtrls, SHDocVw, ExtCtrls;

procedure RenderLogAsSvg(LogFileName: string);

implementation

uses
  SvgWriter, LogicLogParser;

type
  TLogSvgRenderer = class
  private
    SVG: TSvgWriter;
    Parser: TLogicLogParser;
    RowX: array[0..512] of Single; // Left most available pixel in a row;
    OpColorMap: TStrings;
    function FindAvailableRow(X: Single): Integer;
    procedure AllocRow(Row: Integer; X: Single);
    function TransDateX(Date: TDateTime): Single;
    procedure AddCpuMark(Time: TDateTime; Percent: Single);
    procedure AddOperation(Op: TOperation);
    procedure ShowErrorText(Op: TOperation);
    procedure DrawMinuteMarks;
  public
    procedure RenderLogAsSvg(LogFileName, SvgFileName: string);
    constructor Create;
    destructor Destroy; override;
  end;

const
  GraphBottomY = 70;
  OperationY = 85;
  TimeToGraph = 7200; { In seconds }
  BarHeight = 7;
  BarGap = 2;

procedure RenderLogAsSvg(LogFileName: string);
var
  R: TLogSvgRenderer;
begin
  R := TLogSvgRenderer.Create;
  try
    R.RenderLogAsSvg(LogFileName, ChangeFileExt(LogFileName, '.svg'));
  finally
    R.Free;
  end;
end;

procedure TLogSvgRenderer.RenderLogAsSvg(LogFileName, SvgFileName: string);

var
  i: Integer;
  C: TCpuMeasurement;
  MaxDate: TDateTime;
  Op: TOperation;
begin
  SVG := TSvgWriter.Create;
  try
    Parser := TLogicLogParser.Create(LogFileName);
    try
      DrawMinuteMarks;
      MaxDate := AddSeconds(Parser.StartTime, TimeToGraph);

      for I := 0 to Parser.CpuMeasurements.Count-1 do begin
        C := Parser.CpuMeasurements[I];
        if C.When < MaxDate then
          AddCpuMark(C.When, C.Usage);
      end;

      for I := 0 to Parser.Operations.Count-1 do begin
        Op := Parser.Operations[I];
        if Op.Start < MaxDate then
          AddOperation(Op);
      end;

      for I := 0 to Parser.Operations.Count-1 do begin
        Op := Parser.Operations[I];
        if Op.Start < MaxDate then
          ShowErrorText(Op);
      end;


    finally
      Parser.Free;
    end;
    SVG.SaveToFile(SvgFileName);
  finally
    SVG.Free;
  end;
end;

function TLogSvgRenderer.TransDateX(Date: TDateTime): Single;
begin
  Result := (Date - Parser.StartTime) * SecondsInDay;
end;

procedure TLogSvgRenderer.AddCpuMark(Time: TDateTime; Percent: Single);
begin
  SVG.AddRectangle(TransDateX(Time), GraphBottomY - (Percent/2), 2, (Percent/2),
    'green');
end;

procedure TLogSvgRenderer.AddOperation(Op: TOperation);
var
  I: Integer;
  Color: string;
begin
  I := FindAvailableRow(TransDateX(Op.Start));
  if I >= 0 then begin
    AllocRow(I, TransDateX(Op.Stop));

    Color := OpColorMap.Values[Op.OpName];
    if Color='' then
      Color := 'Black';

    Op.BarX := TransDateX(Op.Start);
    Op.BarY := OperationY + I * (BarHeight+BarGap);

    if Op.Success then begin
      SVG.AddRectangle(Op.BarX, Op.BarY, (Op.Stop - Op.Start) * SecondsInDay, BarHeight, Color);
      // only label >2 second
      if (Op.Stop-Op.Start) * SecondsInDay > 2 then
        SVG.AddText(Op.BarX + 1.0, Op.BarY + BarHeight - 1, Op.OpName, 'Black');
    end else begin
      SVG.AddOutlinedRectangle(Op.BarX, Op.BarY, (Op.Stop - Op.Start) * SecondsInDay, BarHeight, Color);
      SVG.AddText(Op.BarX + 1.0, Op.BarY + BarHeight - 1, Op.OpName, 'Black');
    end;

  end;
  // else: don't render it, not enough height
end;

procedure TLogSvgRenderer.ShowErrorText(Op: TOperation);
begin
  if not Op.Success then begin
    SVG.AddText(Op.BarX + 1.0, Op.BarY + BarHeight +7, Op.FailureMessage, 'Black');
  end;
end;

procedure TLogSvgRenderer.AllocRow(Row: Integer; X: Single);
begin
  RowX[Row] := X + 1; // a small buffer zone
end;

function TLogSvgRenderer.FindAvailableRow(X: Single): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := Low(RowX) to High(RowX) do
    if X > RowX[I] then begin
      Result := I;
      Break;
    end;
end;

constructor TLogSvgRenderer.Create;
begin
  OpColorMap := TStringList.Create;
  // cat Logic-2004-08-11-1504.log | cut -d " " -f 6 | sort | uniq
  OpColorMap.Add('AckDamageTicket=Aqua');
  OpColorMap.Add('AckTicket=Aqua');
  OpColorMap.Add('ApproveTimesheets=Aqua');
  OpColorMap.Add('CompletionReport=Brown');
  OpColorMap.Add('DamageSearch6=Aqua');
  OpColorMap.Add('GetAssets=Aqua');
  OpColorMap.Add('GetDamage=Aqua');
  OpColorMap.Add('GetHierarchy=Aqua');
  OpColorMap.Add('GetRights=Aqua');
  OpColorMap.Add('GetTicket=DarkSeaGreen');
  OpColorMap.Add('GetTicketHistory=Aqua');
  OpColorMap.Add('GetTicketsForLocator=Aqua');
  OpColorMap.Add('Login=GoldenRod');
  OpColorMap.Add('MoveTickets=Aqua');
  OpColorMap.Add('MultiOpenTotals=Aqua');
  OpColorMap.Add('PostTimesheets=Aqua');
  OpColorMap.Add('ReportErrors=Aqua');
  OpColorMap.Add('ReportStatus=Aqua');
  OpColorMap.Add('SaveAttachments=Aqua');
  OpColorMap.Add('SaveEmployeeAssets=Aqua');
  OpColorMap.Add('SearchTimesheets=Aqua');
  OpColorMap.Add('StatusTicketsLocates5=Red');
  OpColorMap.Add('SyncDown=Blue');
  OpColorMap.Add('SyncDown2=Blue');
  OpColorMap.Add('TicketSearch4=Coral');

end;

destructor TLogSvgRenderer.Destroy;
begin
  OpColorMap.Free;
  inherited;
end;

procedure TLogSvgRenderer.DrawMinuteMarks;
var
  i: Integer;
  D: TDateTime;
  Hour, Min, Sec, MSec: Word;
begin
  for i := 0 to TimeToGraph do begin
    D := AddSeconds(Parser.StartTime, i);
    DecodeTime(D, Hour, Min, Sec, MSec);
    if Sec=0 then begin
      SVG.AddLine(i, 0, i, 400, 'gray');
      SVG.AddText(i+1, 10, TimeToStr(D), 'Black');
    end;
  end;


end;

end.

