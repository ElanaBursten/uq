import businesslogic

def getlogic(call_center, logic_dict, tdb, holiday_list, geocoder=None):
    """ Get a BusinessLogic instance from the collection dict, or create
        and add one first if necessary. """
    if logic_dict.has_key(call_center):
        return logic_dict[call_center]
    else:
        logic = businesslogic.getbusinesslogic(call_center, tdb, holiday_list,
         geocoder)
        logic_dict[call_center] = logic
        return logic
