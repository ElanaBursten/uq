# killall.py
# Kill all Python processes.  Use on Python programs that are stuck and
# cannot be terminated by quitall.py.

import os
import sys
import win32api

def killall():
    mypid = os.getpid()
    pin, pout, perr = os.popen3("tlist")
    for line in pout:
        pid, process = line.split()[:2]
        pid = int(pid)
        if process == 'python.exe':
            # ignore pythonw.exe for now
            if pid == mypid:
                # no suicide
                continue
            print "Killing process:", line
            handle = win32api.OpenProcess(255, False, pid)
            win32api.TerminateProcess(handle, 1)


if __name__ == "__main__":

    killall()


