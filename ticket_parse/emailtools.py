# emailtools.py

import copy
import poplib
import smtplib
import sys
#
import tools

class Email:
    """ Holds email addresses for a call center. """
    FIELDNAMES = ["admin", "emergency", "summary", "message", "warning",
                  "responder", "noclient"]
    def __init__(self):
        for name in self.__class__.FIELDNAMES:
            setattr(self, name, [])

# XXX takes a TicketDB instance, but a DBInterfaceADO would suffice!
# Changing this involves updating a lot of code, though...
class Emails:
    """ Holds email addresses per call center. """

    def __init__(self, tdb):
        self.tdb = tdb
        self.emails = self.read_emails()

    def read_emails_1(self):
        d = {}
        data = self.tdb.dbado.getrecords("call_center")
        for row in data:
            cc_code = row["cc_code"]
            e = Email()
            for name in e.FIELDNAMES:
                data = row.get(name + "_email", "")
                if data:
                    emails = [email.strip() for email in data.split(";")]
                else:
                    emails = []
                setattr(e, name, emails)
            d[cc_code] = e

        return d

    def read_emails_2(self):
        d = {}
        data = self.tdb.getrecords("notification_email")
        for row in data:
            call_center = row['call_center']
            notification_type = row['notification_type']
            email_address = row['email_address']

            if not d.has_key(call_center):
                d[call_center] = Email()

            # get list for Email.warning, Email.admin, etc
            try:
                lst = getattr(d[call_center], notification_type)
            except AttributeError:
                print >> sys.stderr, "Invalid attribute:", notification_type
                continue

            lst.append(email_address)

        return d

    def __getitem__(self, name):
        return self.emails[name]

    # XXX change this to work with notification_email table

    read_emails = read_emails_1

def get_POP3_class(use_ssl=0):
    return poplib.POP3_SSL if use_ssl else poplib.POP3

def get_SMTP_class(use_ssl=0):
    return smtplib.SMTP_SSL if use_ssl else smtplib.SMTP

class SMTPInfo:
    fields = ["host", "port", "from_address", "use_ssl"] # for repr
    def __init__(self, host, port=25, from_address=None, use_ssl=0,
                       user=None, password=None):
        self.__dict__.update(locals())
    def __repr__(self):
        return "SMTPInfo(%s)" % tools.autorepr(self, self.fields)
    def clone(self, **kwargs):
        newsi = self.__class__(self.host)
        for fname in self.fields + ["user", "password"]:
            setattr(newsi, fname, copy.deepcopy(getattr(self, fname)))
        newsi.__dict__.update(kwargs)
        return newsi

#
# email inspection tools

def is_quoted_printable(headers):
    for line in headers:
        if line.lower().find("quoted-printable") > -1:
            return 1
    return 0

def body_is_quoted_printable(data):
    """ Check if the mail's *body* is in quoted-printable, even if
        headers don't indicate this. """
    exhibit1 = data.lower().find("quoted-printable") > -1
    exhibit2 = data.count('=0D') >= 2 or data.count('=0A') >= 2
    return exhibit1 and exhibit2

def find_header_delimiter(lines):
    """ Return the number of the line where the email headers end (and the
        body begins). Return -1 if not found.
        (Note: These lines do NOT end with a newline.) """
    idx = -1
    for i in range(len(lines)):
        if not lines[i].strip('\r\n'):
            idx = i
            break
    return idx

def remove_mark_lines(lines):
    def is_qp_line(line):
        return line.startswith("--Mark=") \
            or line.startswith("Content-Type:") \
            or line.startswith("This is a multi-part message") \
            or line.startswith("Content-Transfer-Encoding:")
    return [l for l in lines if not is_qp_line(l)]

