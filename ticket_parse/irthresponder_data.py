# irthresponder_data.py
# Callcenter-specific data for IRTH responder.
# Created: 2002.06.29 HN

import abstractresponderdata
import string

def getresponderdata(config, call_center, raw_responderdata):
    # look up and instantiate the ResponderData class for this call center.
    # if no special class is found, use the default ResponderData.
    if call_center[0] not in string.letters:
        lcc = 'c' + call_center
    else:
        lcc = call_center
    try:
        classname = globals()[lcc + 'ResponderData']
    except KeyError:
        ird = ResponderData()
    else:
        ird = classname()

    # load configuration data from config.xml
    if raw_responderdata is None:
        # fetch from config.xml -- for testing purposes only
        try:
            raw_responderdata = config.responders[call_center]
        except KeyError:
            raise KeyError, "No responder data for " + call_center

    ird.response_codes = raw_responderdata["mappings"]
    for attr in ('name', 'server', 'port', 'clientbased', 'logins', 'mappings',
                 'translations', 'skip', 'all_versions', 'clients',
                 'send_emergencies', 'ack_dir', 'ack_proc_dir','send_3hour'):
        setattr(ird, attr, raw_responderdata[attr])
    try:
        ird.client_based_login = int(raw_responderdata["clientbased"])
    except ValueError:
        ird.client_based_login = 0

    # override 'skip' to use values in ResponderConfigData
    ird.skip = config.responderconfigdata.get_skip_data(call_center)
    ird.status_code_skip = config.responderconfigdata.get_status_code_skip_data(call_center)

    return ird


# these call centers used to use the modem responder
MODEM_RESPONDERS = ('OCC1', 'FCV1')

class ResponderData(abstractresponderdata.AbstractResponderData):
    response_codes = {}

    client_based_login = 0
    # set this if the logins are client codes, used to filter pending responses

    # these are acceptable return codes; everything else is an error and should
    # be treated as such
    acceptable_codes = ["250", "251", "252", "450", "451", "452", "453", "454",
                        "455", "456", "457"]

    # codes_ok: these codes are OK (whether they indicate success or not),
    # delete from queue, continue
    codes_ok = [
     "250", # OK
     "251", # Duplicate responses
     "252", # Ticket has been cancelled
     "452", # Ticket has expired
     "454", # Member not on ticket
     "457", # Ticket has expired
    ]

    # codes_reprocess: continue, don't delete from queue
    codes_reprocess = [
     "425", #
     "522", # Session expired
     "",
    ]

    # codes_error: continue, notify admin, delete from queue
    codes_error = [
     "450", # Insufficient information
     "451", # Invalid ticket
     "453", # Invalid member code
     "455", # Invalid response code
     "456", # Invalid explanation code
    ]

    def make_response(self, ticket, membercode, responsecode, explanation):
        s = "DATA %s,%s,UQ Responder,%s" % (ticket, membercode, responsecode)
        if explanation:
            s = s + ",%s" % (explanation)
        s = s + "\r\n"
        return s

    def tweak_ticket_number(self, row):
        call_center = row.get('ticket_format', '')
        ticket_number = row.get('ticket_number', '')
        if call_center in MODEM_RESPONDERS:
            ticket_number = "A0" + ticket_number
        return ticket_number


    #
    # telnet communication

    login_regexen = ["220.*?\r\n",]
    user_string = "password.\r\n"
    password_regexen = ["proceed.\r\n",
                        "Not logged in.\r\n",
                        "220.*?\r\n"]
    response_until = "\r\n"


_translations = {
    # NOTE: these have been moved to config.xml
    "__OCC1": {
        "ADCC01": "ADL902",
        "ADCC02": "ADL903",
        "CLG01": "CGV903",
        "COX01": "COX901",
        "CTL03": "VZN902",
        "NTEL07": "NTL901",
        "PTE05": "ALP905",
        "SHG01": "SHG909",
        "TVA01": "VZN901",
        "TVA02": "VZN907",
        "VAW01": "VAW901",
        "VPW02": "DMN400",
        "VPW03": "DMN400",
        "VPW04": "DMN400",
        "VPW08": "DMN400",
        "VPW09": "DMN400",
        "VPW11": "DMN400",
        "VPW51": "DMN400",
        "VPW61": "DMN400",
        "VPW01": "DMN800",
        "WGL04": "WGL904",
    },
    "__FCV1": {
        "ADCC01": "ADL902",
        "CLG02": "CGV902",
        "NWE49": "NNE949",
        "PTE05": "ALP905",
        "RPE01": "REC901",
        "TVA01": "VZN901",
        "TVA03": "VZN903",
        "TVA04": "VZN904",
        "VPW01": "DMN800",
        "VPW07": "DMN800",
        "VPW09": "DMN400",
        "VPW18": "DMN800",
        "WNS01": "WOA901",
        "CMG01": "CGV901",
    },
}

# XXX OBSOLETE
def translation(call_center, client_code):
    """ Look up the translation for the client code for a given call center.
        Return None if nothing found. """
    try:
        d = _translations[call_center]
        try:
            return d[client_code]
        except KeyError:
            return None
    except KeyError:
        return None

#
#

class FAQ1ResponderData(ResponderData):

    login_regexen = ["220.*?\r",]
    user_string = "password.\r"
    password_regexen = ["proceed.\r",
                        "Not logged in.\r",
                        "220.*?\r"]
    response_until = "\r"

    def make_response(self, ticket, membercode, responsecode, explanation=None):
        """ DATA <request number>, <responder name>, <type>, <response code>,
                 <explanation code>
        """
        type = "1" # default type when creating a request
        s = "DATA %s,%s,%s,%s" % (ticket, "UQ Responder", type, responsecode)
        if explanation:
            s = s + ",%s" % (explanation)
        s = s + "\r\n"
        return s

FAQ2ResponderData = FAQ1ResponderData
FAQ3ResponderData = FAQ1ResponderData
FAQ4ResponderData = FAQ1ResponderData
c1101ResponderData = FAQ1ResponderData
c1102ResponderData = FAQ1ResponderData
c1103ResponderData = FAQ1ResponderData

