# response_ack_context.py

import sqlmap
import sqlmaptypes as smt
import static_tables

class ResponseAckContext(sqlmap.SQLMap):
    __table__ = "response_ack_context"
    __key__ = "filename" # actually (call_center, filename, locate_id)
    __fields__ = static_tables.response_ack_context

    def __init__(self, data=()):
        sqlmap.SQLMap.__init__(self)
        if data:
            self.setdata(data)

    # simulate dictionary access:
    def __getitem__(self, name):
        if name == 'resp_id': return self['response_id'] # hack
        return self._data[name]
    def __setitem__(self, name, value):
        if name == 'resp_id': name = 'response_id' # hack
        self._data[name] = value

