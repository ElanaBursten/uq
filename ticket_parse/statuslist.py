# statuslist.py

class StatusList:

    def __init__(self, tdb):
        self.tdb = tdb
        self.data = self._load()

    def _load(self):
        d = {}
        rows = self.tdb.dbado.getrecords('statuslist')
        for row in rows:
            status = row['status']
            d[status] = row
        return d

    def must_be_closed(self, status):
        """ Return true if the given status must be closed when statusing a
            locate.  (For example, when setting a locate to M, this returns 1,
            indicating that closed should be set to 1.  The opposite is true
            for certain other statuses. """
        value = self.data[status]['complete']
        return int(value)

    def lookup_description(self, status):
        return self.data[status]['status_name']

