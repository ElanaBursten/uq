# responder_config_data.py
# Keep data about all responders.

class ResponderConfigData:

    def __init__(self):
        self.reset()

    def reset(self):
        self.data = [] # list of tuples (call_center, type, responder_data);
                       # multiple responders per call center possible
        self.skip = {} # clients that we skip, per call center
        self.status_code_skip = {} # skip these status codes

    def add(self, call_center, type, data, unique=False):
        """ Add a responder configuration. If unique is True, then we make sure
            that this will be the only entry for this call center / responder
            type combination. """
        if unique:
            # only one entry per call center / type
            self.data = [(cc, t, respdata)
                         for (cc, t, respdata) in self.data
                         if cc != call_center or t != type]
        self.data.append((call_center, type, data))

    def get_responders(self, call_center=None, type=None, kind=None):
        """ Get responder data as a list of tuples (call_center, type, data).
            Filters by call center and/or type, so it's possible to get all
            responders for call center X, or all FTP responders, etc.
        """
        return [(cc, t, data) for (cc, t, data) in self.data
                if (cc == call_center or call_center is None)
                and (t == type or type is None)
                and (kind is None or data.get('kind', None) == kind)]

    def gather_skip_data(self):
        """ Gather skip data per call center.  Should only be called when all
            responder data has been added. """
        for (call_center, type, data) in self.data:
            skip = data.get('skip', [])
            if not self.skip.has_key(call_center):
                self.skip[call_center] = []
            existing = self.skip[call_center]
            self.skip[call_center] = sorted(existing + skip)

    def get_skip_data(self, call_center):
        return self.skip.get(call_center, [])

    def gather_status_code_skip_data(self):
        """ Gather status code skip data per call center.  Should only be
            called when all responder data has been added. """
        for (call_center, type, data) in self.data:
            status_code_skip = data.get('status_code_skip', [])
            if not self.status_code_skip.has_key(call_center):
                self.status_code_skip[call_center] = []
            existing = self.status_code_skip[call_center]
            self.status_code_skip[call_center] = sorted(existing + status_code_skip)

    def get_status_code_skip_data(self, call_center):
        return self.status_code_skip.get(call_center, [])

