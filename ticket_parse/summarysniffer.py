# summarysniffer.py
# Created: 12 Apr 2002, Hans Nowak
# XXX probably not used anymore?

import getopt
import os
import sys
#
import config
import filetools
import ticket_db
import ticketparser
import recognize
import summarycollection
import summaryparsers
import errorhandling2
import tools
import ticketloader

__usage__ = """\
summarysniffer.py [options] begin_date end_date [processed_dir]

(Re)process all summaries in the given directory. Ignore non-summaries. If no
directory is specified, "processed" is assumed. Only stores summaries if they
are between the given date margin.

Options:
    -c cfgfile  Use an alternate configuration file.
    -f format   Use the given format (not: call center) to parse the summaries.
    -t          (test) Do everything except actually posting the summaries.
"""

COLLECTION = "summaries.db"

class SummarySniffer:

    def __init__(self, directory, begin_date, end_date, configfile="",
                 format="", test=0, verbose=1):
        self.config = self._getconfiguration(configfile)
        self.format = format
        self.test = test
        self.begin_date = begin_date
        self.end_date = end_date

        self.directory = directory
        self.tdb = ticket_db.TicketDB()
        self.sc = summarycollection.SummaryCollection(COLLECTION) # FIXME

        # create and configure the logger
        self.log = self._createlogger()
        self.handler = ticketparser.TicketHandler()
        self.verbose = verbose

    def run(self):
        files = os.listdir(self.directory)
        files = [f for f in files if not os.path.isdir(f)]
        files.sort()
        count = 0
        if self.format:
            formats = [self.format]
        else:
            formats = []

        for i in range(len(files)):
            file = files[i]
            fullname = os.path.join(self.directory, file)
            if self.verbose:
                print "Processing: %s (file %d of %d)" % (fullname, i+1,
                 len(files))
            tl = ticketloader.TicketLoader(fullname)
            for raw in tl.tickets:
                # find out if data is a summary of the desired format(s)
                fmt = recognize.recognize_summary(raw)
                if (not fmt):
                    continue    # not a summary; ignore
                if fmt not in formats:
                    print >> sys.stderr, fmt, "summary found, expected:", \
                     formats
                    continue

                try:
                    summ = self.handler.parse(raw, formats)
                except:
                    ep = errorhandling2.errorpacket(rawticket=raw,
                     formats=formats)
                    self.log.log(ep, write=1, dump=1, send=0)
                    continue

                count = count + 1

                if not self.test:
                    if self.begin_date <= summ.summary_date <= self.end_date:
                        self.sc.add(summ)
                        # check if there are any complete summaries we can post
                        self.check_complete_summaries()
                        # and if there are any incomplete ones that we must post
                        self.check_incomplete_summaries()
                    else:
                        if self.verbose:
                            print "Summary has date %s," % (summ.summary_date),
                            print "ignored (outside of date range)"

        if self.verbose:
            print "Processed", count, "summaries"

    def check_complete_summaries(self):
        self.tdb.check_complete_summaries(self.sc, self.log)

    def check_incomplete_summaries(self):
        incomplete = self.sc.find_incomplete_and_done()
        for call_center, client_code in incomplete:
            parts = self.sc.get(call_center, client_code)
            if self.verbose:
                print "Posting %s summary for %s (%d part(s)) -- INCOMPLETE" % (
                 parts[0].call_center, parts[0].client_code, len(parts))
            self.tdb.storesummaries(parts, self.log)
            self.sc.remove(call_center, client_code)

    def _getconfiguration(self, configfile):
        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            c = config.Configuration(configfile)
            c.setConfiguration(self.config)
            return c
        else:
            return config.getConfiguration()

    def _createlogger(self):
        log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
              smtpinfo=self.config.smtp_accs[0], me="summarysniffer",
              admin_emails=self.config.admins) # doesn't log per call center
        log.subject = "summarysniffer.py Error Notification"
        log.defer_mail = 1
        log.lock = 1   # don't send email
        return log



if __name__ == "__main__":

    configfile = ""
    format = ""
    test = 0

    opts, args = getopt.getopt(sys.argv[1:], "c:f:t?")
    for o, a in opts:
        if o == "-c":
            configfile = a
        elif o == "-f":
            format = a
        elif o == "-t":
            test = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)

    if len(args) == 2:
        begin_date, end_date, dir = args[0], args[1], "processed"
    elif len(args) == 3:
        begin_date, end_date, dir = args[:3]
    else:
        print >> sys.stderr, __usage__
        raise SystemExit

    filetools.remove_file(COLLECTION)    # for now

    z = SummarySniffer(dir, begin_date, end_date, configfile=configfile,
        format=format, test=test)
    z.run()


