# ticket_db.py
# Tools for managing the ticket database. This is placed here to separate it
# from the generic code in dbinterface.py.
#
# XXX prime candidate for refactoring
# It would probably be best to start creating new modules that use the
# new-style connection (using DBEngine, etc), using their own transactions,
# etc, and integrate this with existing code.

from __future__ import with_statement

import logging
import new
import string
import time

# logging for debugging purposes
log_debug = logging.getLogger("ticketrouter.ticket_db")

#
import assignment
import attachment
import businesslogic
import client
import date
import dbinterface # new
import dbinterface_ado
import errorhandling2
import geocoder
import locate
import locate_status
import sqlgatherer
import sqlmap
import tabledefs
import ticket as ticketmod
import ticket_grid
import ticket_hp
import ticketsummary
import tools
import update_call_centers as ucc
import static_tables
from transaction import Transaction

# XXX replace static_tables usage with TableDefs

geocode_address_fields = ['work_address_number', 'work_address_street',
 'work_cross', 'work_city', 'work_county', 'work_state']
geocode_result_fields = ['work_lat', 'work_long', 'geocode_precision']
blank_geocode_precision = -10 # Can't use None, since nulls don't sync down
 
class TicketDB(object):
    """ This is nothing more than a large collection of non-generic database
        calls (non-generic meaning that these methods always have to do with
        tickets, summaries etc). (Generic database routines that know nothing
        about the UtiliQuest system are in dbinterface.py.)
    """

    def __init__(self, dbado=None):
        if dbado:
            self.dbado = dbado
        else:
            self.dbado = dbinterface_ado.DBInterfaceADO()   # ADO connection
        self.sg = sqlgatherer.SQLGatherer(self)
        self.tabledefs = tabledefs.TableDefs(self.dbado)

    # seems to be mostly for decorative purposes
    def getticketcount(self):
        """ Return the number of tickets currently in the ticket table. """
        rows = self.dbado.runsql_result("select count(*) as cnt from ticket")
        return int(rows[0]['cnt'])

    def isassigned(self, locate_id):
        """ Check if a locate record is assigned. (If it is, its status field
            must be locate_status.assigned.
            This does *NOT* check if there is an assignment record present.
        """
        sql = """
         select * from locate
         where locate_id = %d
        """
        sql = sql % int(locate_id)
        result = self.dbado.runsql_result(sql)
        if not result:
            raise Exception("locate %s not found" % locate_id)
        for row in result[:1]:
            st = row["status"]
            return st == locate_status.assigned

    def add_assignment(self, locate_id, locator_id, added_by="unknown"):
        a = assignment.Assignment()
        a.set(locate_id=locate_id,
              locator_id=locator_id,
              added_by=added_by)
        asg_id = a.insert(self)
        return asg_id

    #
    # inserting tickets

    #@with_transaction
    def insertticket_ado_sg(self, ticket, parent_ticket_id=None, whodunit=""):
        assert not ticket.ticket_id
        self.sg.reset() # don't allow any existing SQL here

        # prepare ticket for inserting
        if parent_ticket_id:
            ticket.parent_ticket_id = int(parent_ticket_id)
        if ticket.image is None:
            ticket.image = "" # always post this
        ticket.image = tools.remove_crud(ticket.image)
        for loc in ticket.locates:
            if whodunit and not loc.added_by:
                loc.added_by = whodunit

        # insert the ticket
        ticket_id, locate_ids = ticket.insert_sg(self.sg)

        # store grids
        ticket_grid.insert_grids(self, ticket_id, ticket.grids)
        # XXX roll this into Ticket.insert[_sg]!

        # store plat, if any
        for loc in ticket.locates:
            if hasattr(loc, '_plat'):
                plat = getattr(loc, '_plat', None)
                if plat:
                    self.add_plat(loc.locate_id, plat)

        return ticket_id

    insertticket = insertticket_ado_sg

    #
    # update ticket

    def updateticket_sg(self, ticket_id, ticket, newlocates=(), reassign=None,
                        whodunit="", locates_only=0, update_grids=(),
                        post_image=1, log=None):

        if reassign is None:
            reassign = {}
        # <ticket> is the new, incoming ticket
        # <ticket_id> is the id of the existing ticket in the database that
        # must be updated
        # in the body, <t> is the existing ticket that we will update and
        # post.

        # XXX refactor later; we probably won't need all these parameters;
        # for now, make sure it works like the SQLXML version!

        self.sg.reset() # don't allow any pre-existing SQL

        newticket = ticket
        assert newticket.geocode_precision is None
                    
        t = ticketmod.Ticket.load(self, ticket_id) # get original ticket
        # we will update this ticket and post it.

        old_values = {}
        for fld in geocode_address_fields + geocode_result_fields + \
         ['map_page']:  
            old_values[fld] = t[fld]
        
        # (NewJersey only)
        if ticket.service_area_code and ticket.service_area_code.strip() \
        and ticket.ticket_format.startswith("NewJersey"):
            t.service_area_code = newticket.service_area_code

        if newticket.serial_number and newticket.serial_number.strip():
            t.serial_number = newticket.serial_number

        if not locates_only:
            updatedata = None
            if newticket._update_call_center:
                updatedata = ucc.get_updatedata(newticket._update_call_center)

            new_parsed_lat_long = False
            # then, inject (almost) all Ticket attributes...
            for attrname, attrvalue in newticket._data.items():
                if attrname.startswith("_") \
                 or attrname in t.SPECIAL \
                 or attrvalue is None:
                    continue # skip this field

                # if this is an update call center, only store certain fields
                if updatedata and attrname not in updatedata.fields:
                    continue

                if attrname in ['work_lat', 'work_long'] and attrvalue and \
                 attrvalue <> ticketmod.LAT_LONG_DEFAULT:
                    new_parsed_lat_long = True

                old = getattr(t, attrname)
                # only set the value if it is different in the new ticket
                # and the new ticket value is not null
                if attrvalue != old and attrvalue:
                    setattr(t, attrname, attrvalue)

            if old_values['geocode_precision'] >= 0:
                if new_parsed_lat_long:
                    t.geocode_precision = blank_geocode_precision
                else:
                    # todo(dan) Only do this if it's consistent with new map_page?
                    for fld in geocode_result_fields:
                        t[fld] = old_values[fld]
        else:
            #if the update has an alert, set it
            if newticket.alert:
                t.alert = newticket.alert

        # prepare new locates
        for loc in newlocates:
            loc.added_by = loc.added_by or whodunit
            t.locates.append(loc)
            # FIXME: we can get this info by comparing existing and new tickets

        # handle reassignable locates
        for locate_id, locdict in reassign.items():
            for loc in t.locates:
                if loc.locate_id == locate_id:
                    loc.status = locate_status.blank
                    loc.closed = 0
                    # update seq_number as well
                    for currloc in newticket.locates:
                        if currloc.client_code == loc.client_code:
                            if hasattr(currloc, 'seq_number') and currloc.seq_number:
                                loc.seq_number = currloc.seq_number

        if post_image:
            t.image = tools.remove_crud(newticket.image)

        # do the update
        ticket_id, locate_ids = t.update_sg(self.sg)

        # todo(dan) If any of geocode_address_fields +
        # geocode_result_fields + ['map_page'] are in updatedata.fields,
        # then probably all of them should be. Might want to enforce
        # this somewhere.
        # todo(dan) Should we send a message if the original wasn't geocoded?
        if old_values['geocode_precision'] >= 0 and log:
            changed = []
            for fld in geocode_address_fields + \
             geocode_result_fields + ['map_page']:
                if old_values[fld] <> t[fld]:
                    changed.append('old ' + fld + ': ' +
                     str(old_values[fld]))
                    changed.append('new ' + fld + ': ' + str(t[fld]))
            if changed:
                warning_msg = 'Unexpected updates to geocoded ticket'
                msg = [warning_msg, 'ticket number: ' + t.ticket_number] + \
                 changed
                ep = errorhandling2.errorpacket()
                ep.add(info=string.join(msg, '\n'))
                log.log_warning(ep, call_center=t.ticket_format, send=1,
                 write=1, dump=1, subject_hint=warning_msg)                      

        if update_grids:
            ticket_grid.insert_grids(self, ticket_id, update_grids)

        # store plat, if any
        for loc in ticket.locates:
            if hasattr(loc, '_plat'):
                plat = getattr(loc, '_plat', None)
                if plat:
                    # get locate_id for this locate (must use separate query
                    # for this, SQLXML does not have locate_ids) and set
                    # plat for each locate
                    stored_loc = self.dbado.getrecords('locate',
                                 ticket_id=ticket_id,
                                 client_code=loc.client_code)
                    if stored_loc:
                        self.add_plat(stored_loc[0]['locate_id'], plat)

        return ticket_id
    updateticket = updateticket_sg

    def updateticket_by_id_3(self, ticket_id, ticket, whodunit="",
                             locates_only=0, update_grids=()):
        for locate in ticket.locates:
            if not locate.locate_id:
                # Insert new locates
                locate.ticket_id = ticket_id
                locate.added_by = whodunit
                locate.insert(self)
            if locate.locate_id and not locate._existing and locate._assignment:
                locate.save_assignment(self, locate.locate_id)

        return self.updateticket_sg(ticket_id, ticket, whodunit=whodunit,
               locates_only=locates_only, update_grids=update_grids)

    updateticket_2 = updateticket_by_id_3

    #
    #

    def getticket_3(self, ticket_id):
        t = ticketmod.Ticket.load(self, ticket_id)
        t.grids = ticket_grid.get_grids(self, ticket_id)
        return t

    getticket = getticket_3


    def getticketimages(self, ids):
        """ Like _getticketimages(), but makes sure it doesn't request too many
            images at once. """
        MAXLENGTH = 30
        ids = ids[:]
        results = []
        while ids:
            spam = self._getticketimages(ids[:MAXLENGTH])
            results.extend(spam)
            ids = ids[MAXLENGTH:]
        return results

    def _getticketimages(self, ids):
        """ Get only the raw ticket images from the tickets with the given IDs.
            Return a list of tuples (ticket_id, images) so the caller can
            determine which image belongs to which ticket.
        """
        sql = """
         select ticket_id, image from ticket
         where ticket_id in (%s)
        """
        sql = sql % (string.join(map(str, ids), ","))
        result = self.dbado.runsql_result(sql)

        d = []
        for row in result:
            ticket_id = row['ticket_id']
            image = row['image']
            d.append((ticket_id, image))

        return d

    def get_locates(self, ticket_id):
        sql = """
         select * from locate
         where ticket_id = %d
        """ % (int(ticket_id),)
        results = self.dbado.runsql_result(sql)
        return results

    @staticmethod
    def _ticketnode_to_ticket(ticketnode):
        """ (Helper function, used by getticket and gettickets.)
            Convert a "rich" ticketnode (i.e. one that contains data of the
            ticket and possibly locate records) to a Ticket instance.
            Returns as much info as possible, because we cannot always assume
            that the caller already has this info. Therefore we return a dict
            {ticket_id, ticket, locates}. (locates is a dict locate_id: client.}

            Assumption: <locate> tags are inside the <ticket> tag.
        """
        # create an "empty" Ticket instance
        t = ticketmod.Ticket()

        # get all relevant Ticket attributes and inject them in Ticket instance
        if isinstance(t, sqlmap.SQLMap):
            fields = [f[0] for f in t.__fields__]
        else:
            fields = tools.get_fields(t)
        for key in fields:
            nodes = ticketnode.getElementsByTagName(key)[:1]
            for node in nodes:
                s = ""
                for childnode in node.childNodes:
                    s = s + childnode.nodeValue
                setattr(t, key, s)

        # locates needs special treatment...
        locdict = {}
        locatenodes = ticketnode.getElementsByTagName("locate")
        for locatenode in locatenodes:
            loc = locate.Locate()
            # determine fields, old and new methods
            if isinstance(loc, sqlmap.SQLMap):
                fields = [f[0] for f in loc.__fields__]
            else:
                fields = tools.get_fields(loc)

            for attr in fields:
                # this includes the locate_id
                for subnode in locatenode.getElementsByTagName(attr)[:1]:
                    for childnode in subnode.childNodes:
                        setattr(loc, attr, childnode.nodeValue)

            # extract an assignment, if any
            asgnodes = locatenode.getElementsByTagName("assignment")
            for asgnode in asgnodes[:1]:
                asg = assignment.Assignment()
                if isinstance(asg, sqlmap.SQLMap):
                    fieldnames = [field for (field, sqltype) in asg.__fields__]
                else:
                    fieldnames = tools.get_fields(asg)
                for attr in fieldnames:
                    for subnode in asgnode.getElementsByTagName(attr)[:1]:
                        for childnode in subnode.childNodes:
                            setattr(asg, attr, childnode.nodeValue)
                    # NOTE: this code looks much the same as the locate stuff...
                    # maybe we should refactor it?
                if asg.locate_id:
                    loc._assignment = asg

            for subnode in locatenode.getElementsByTagName("locate_id")[:1]:
                for childnode in subnode.childNodes:
                    locate_id = childnode.nodeValue
                    locdict[locate_id] = {"client_code": loc.client_code,
                     "status": loc.status, "seq_number": loc.seq_number,
                     "client_id": loc.client_id}
            t.locates.append(loc)

        # image needs a bit of trimming...
        t.image = t.image.strip()

        # get the ticket_id from the XML...
        for node in ticketnode.getElementsByTagName("ticket_id")[:1]:
            s = ""
            for childnode in node.childNodes:
                s = s + childnode.nodeValue
            ticket_id = s

        # we're going to return a dict with the ticket id, the Ticket instance
        # we just built, and a dict with locate ids...
        d = {"ticket": t, "ticket_id": ticket_id, "locates": locdict}
        return d
        # TODO: It may be a lot clearer if we just returned a Ticket, which
        # would have the ticket_id set, and its locates would have their
        # locate_id set...


    def find_grid_area_locator(self, latitude, longitude):
        """ Query database with longitude and latitude, getting back locator
            info. We take only the first record returned. If nothing was
            returned, return an empty list.
        """
        sql = "exec find_grid_area_locator '%s', '%s'" % (latitude, longitude)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_munic_area_locator(self, state, county, municipality):
        """ Query database with state, county and municipality (strings),
            getting back locator info. We only take the first record returned.
            If nothing was returned, return an empty list.
        """
        sql = "exec find_munic_area_locator '%s', '%s', '%s'" % (
              state, county, municipality)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_munic_area_locator_2(self, state, county, municipality, call_center):
        sql = "exec find_munic_area_locator_2 '%s', '%s', '%s', '%s'" % (
              state, county, municipality, call_center)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_map_area_locator(self, mapcode, page, x, y, county=None):
        '''
        Tries to find a locator based on call center (mapcode),
        mapsco page, cell within the page, and the county.
        mapcode = the call center
        page = the page in the book. In the current db schema, and int
        x = cell on the page
        y = either a letter or 1. On some dense areas, mapsco breaks up a page into several pages
            identified as "MAPSCO 123A,U" instead of MAPSCO 123,U
        county = the county name. A typical mapsco will be for a metropolitan area such as Dallas
                 or Fort Worth. This identifies which mapsco county
        '''
        log_debug.debug("mapcode = %s, page = %s, x = %s, y = %s, county = %s" % (mapcode, page, x, y, county))
        if county is None:
            sql = "exec find_map_area_locator '%s',%s,'%s','%s'"
            sql = sql % (mapcode, page, x, y)
        else:
            sql = "exec find_map_area_locator_with_cnty '%s',%s,'%s','%s','%s'"
            sql = sql % (mapcode, page, x, y, county)
        log_debug.debug("sql: %s" % (sql,))
        result = self.dbado.runsql_result(sql)
        log_debug.debug("result: %s" % (str(result)))
        return result[:1]

    def find_map_area_locator_2(self, mapcode, page, x, y):
        # new version - map_page_num is varchar
        # new but unused
        sql = "exec find_map_area_locator_2 '%s','%s','%s','%s'"
        sql = sql % (mapcode, page, x, y)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_augusta_va(self, mapgrid):
        sql = "exec find_augusta_va '%s'" % (mapgrid,)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_qtrmin(self, lat, longitude):
        sql = "exec find_qtrmin '%s', '%s'" % (lat, longitude)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_qtrmin_2(self, lat, long, call_center):
        sql = "exec find_qtrmin_2 '%s', '%s', '%s'" % (lat, long, call_center)
        result = self.dbado.runsql_result(sql)
        return result[:1]

    def find_county_code(self, state, county):
        sql = "select ticket_code from map "\
         "where state = '%s' and county = '%s' "
        sql = sql % (state, county)
        results = self.dbado.runsql_result(sql)
        if results:
            return results[0].get("ticket_code", "")
        return None

    def find_township_baseline_2(self, baseline, township, range, section,
                                 call_center):
        result = self.dbado.call_result("find_township_baseline_2",
                 baseline, township, range, section, call_center)
        return result[:1]

    # This may only be used in tests
    def storesummary(self, summary):
        with Transaction(self.dbado):
            if summary._has_header:
                return self.insertsummary(summary)
            else:
                return self.updatesummary(summary)
            # NOTE: Both must return ids!

    # NOT wrapped in a transaction. Will be wrapped by calling routine.
    def storesummaries(self, summaries, log):
        """ Store a list of summaries. The first one in the list will be
            inserted, the other ones will be updated. Returns a list of tuples
            (hid, dids) of the newly posted records. """
        if not summaries:
            return []
        ids = []
        for i, summ in enumerate(summaries):
            if i == 0 and summ._has_header:
                hid, dids = self.insertsummary(summ)
            else:
                hid, dids = self.updatesummary(summ)
            ids.append((hid, dids))

        return ids

    def storesummaries_update(self, summaries, summary_header_id, log):
        """ Store a set of (related) summaries, by attaching them to an
            existing summary_header_id. Only for 'updating' summaries, which
            doesn't work for all call centers. """
        # get original summary from database
        summ = ticketsummary.TicketSummary.load(self, summary_header_id)
        # add details from <summaries> to this original
        for subsumm in summaries:
            data = subsumm.data
            summ.data.extend(data)
        # update
        summ.save(self)
    #storesummaries_update = with_transaction(storesummaries_update)

    def insertsummary(self, summary):
        summary.set_time(summary.summary_date)
        summary.modified_date = date.Date().isodate()
        # maybe these should be moved to TicketSummary.insert()?
        hid, dids = summary.insert(self)
        return hid, dids
    #insertsummary = with_transaction(insertsummary)

    def updatesummary(self, summary):
        header = self.find_latest_summary(summary.call_center,
                 summary.client_code) # this is a TicketSummary as well

        if not header:
            raise Exception("No header found for follow-up summary")

        # add details of this summary to header
        header.data.extend(summary.data)

        # if the summary has a footer (and thus an 'expected tickets'
        # section), set the expected_tickets field for the summary header
        if summary._has_footer:
            header.expected_tickets = summary.expected_tickets

        # update everything
        hid, dids = header.save(self)
        return hid, dids
    #updatesummary = with_transaction(updatesummary)

    def find_latest_summary(self, call_center, client_code):
        """ Return the the latest summary found that matches the given
            call_center and client_code. Return None if nothing found.
        """
        sql = """
         select top 1 summary_header_id from summary_header
         where call_center = '%s' and client_code = '%s'
         order by summary_header_id desc
        """ % (call_center, client_code)
        # we assume that the higher the id, the later the post
        result = self.dbado.runsql_result(sql)

        if result:
            summary_header_id = result[0]['summary_header_id']
            summ = ticketsummary.TicketSummary.load(self, summary_header_id)
            return summ
        else:
            return None

    def find_duplicate_summaries(self, summ):
        """ Given a TicketSummary instance, find any duplicates of it in the
            database. A duplicate is defined as a summary that has the same
            date, call center, and client code.
        """
        sql = """
         select * from summary_header
         where call_center = '%s'
         and client_code = '%s'
         and summary_date = '%s'
        """
        sql = sql % (summ.call_center, summ.client_code, summ.summary_date)
        results = self.dbado.runsql_result(sql)
        return results

    def isclosed(self, ticket_id):
        """ Return true if all locates of a ticket are closed. """
        sql = """
         select locate_id from locate
         where ticket_id = %d
         and closed = 0
        """
        sql = sql % (int(ticket_id),)
        results = self.dbado.runsql_result(sql)
        return (len(results) == 0)

    def find_clients_for_ticket(self, ticket_id):   # XXX SOON OBSOLETE
        """ Find any locates of the given ticket that are clients. We're
            looking in the database for this. Such records may not have been
            passed to the router when doing a reparsing. """
        #sql = """
        # select * from locate
        # where ticket_id = '%s'
        # and not (status in ('-N', '-P'))
        #"""

        # new SQL, may perform better?
        sql = """
          select * from locate
          where ticket_id = %d
          and status <> '-N' and status <> '-P'
        """
        ticket_id = int(ticket_id) # in case it's a string or so
        sql = sql % (ticket_id,)
        results = self.dbado.runsql_result(sql)
        return results

    def get_BEC01_ids(self):
        """ Get the client IDs of BEC01 and BEC02 for FCL1. """
        sql = """
         select client_id from client
         where call_center = 'FCL1'
         and oc_code in ('BEC01', 'BEC02')
        """
        results = self.dbado.runsql_result(sql)
        ids = [tools.udecode(row["client_id"]) for row in results]
        return ids

    def get_customers(self):
        """ Return a list of customers from the client table.
            (Used by router.)
        """
        sql = "select oc_code, call_center, client_id from client "\
              "where oc_code is not null and active = 1"
        result = self.dbado.runsql_result(sql)
        return result

    ###
    ### ticket_ack routines

    def insert_ticket_ack(self, ticket_id):
        sql = "exec insert_ticket_ack %s" % (ticket_id,)
        self.runsql(sql)

    def update_ticket_work_priority(self, ticket_id, work_priority):
        """
        Insert the work priority into the ticket (if not None)
        """
        if work_priority is not None:
            self.dbado.update('ticket', 'ticket_id', ticket_id, work_priority_id=work_priority)

    ###
    ### autorouting

    def get_autorouting_locators(self):
        results = self.dbado.getrecords("billing_exclude_work_for")
        # contains billing_term_id and work_done_for
        return results

    ###
    ### misc

    def get_emails(self, fieldname):
        """ Get email addresses from a field in the call_center table. Return a
            dict with call center code as key and a list of emails as value.
            Field currently supported:
            - admin_email
            - emergency_email
        """
        sql = """
         select cc_code, %s from call_center
        """ % (fieldname,)
        results = self.dbado.runsql_result(sql)
        d = {}
        for row in results:
            key = row["cc_code"]
            value = row.get(fieldname, None)
            if value:
                value = value.split(";")
            else:
                value = []
            d[key] = value
        return d

    def check_connection(self):
        """ Used to check database connection. Will raise an error if the
            connection doesn't work, which should be caught by the caller.
        """
        sql = "select top 1 * from office"
        self.dbado.runsql(sql)

    def add_ticket_version(self, ticket_id, ticket, filename):
        import ticketversion
        tv = ticketversion.TicketVersion()
        tv.set(ticket_id=ticket_id,
               ticket_number=ticket.ticket_number,
               serial_number=ticket.serial_number,
               ticket_type=ticket.ticket_type,
               ticket_revision=ticket.revision,
               transmit_date=ticket.transmit_date,
               processed_date=date.Date().isodate(),
               filename=filename,
               ticket_format=ticket.ticket_format,
               image=ticket.image,
               source=ticket.source,
               arrival_date=ticket._filedate)
        tvid = tv.insert(self)
        return tvid

    def get_ticket_versions(self, ticket_id):
        """ Return a list TicketVersion objects for the given ticket_id. """
        import ticketversion
        rows = self.getrecords("ticket_version", ticket_id=ticket_id)
        return ticketversion.TicketVersion.load_from_rows(rows)

    def read_holiday_records(self):
        return self.dbado.getrecords("holiday")

    def get_damage_without_due_date(self, n=50):
        sql = """
         select top %d *
         from damage
         where due_date is null
        """ % (n,)
        results = self.dbado.runsql_result(sql)
        return results

    def get_locator_by_area(self, area_id):
        """ Find and return the locator associated with a given area.  If
            nothing is found, return None.  Only finds locators that are
            active."""
        sql = """
         select area.locator_id from area, employee
         where area_id = %s
         and employee.emp_id = area.locator_id
         and employee.active = 1
         and employee.can_receive_tickets = 1
        """ % (area_id,)
        results = self.dbado.runsql_result(sql)
        if results:
            return results[0].get("locator_id", None)
        else:
            return None

    def add_locate(self, ticket_id, client_code, client_id, whodunit=""):
        loc = locate.Locate()
        loc.set(ticket_id=ticket_id,
                client_code=client_code,
                client_id=client_id,
                status=locate_status.blank,
                added_by=whodunit,
                closed=0)
        locate_id = loc.insert(self)
        return locate_id

    def get_employee_name(self, emp_id):
        """ Get an employee's name. """
        sql = """
         select short_name from employee
         where emp_id = %d
        """ % (int(emp_id),)
        results = self.dbado.runsql_result(sql)
        if not results:
            raise KeyError, "No name found for employee %s" % (emp_id,)
        else:
            return results[0].get("short_name", "")

    def set_locate_status(self, locate_id, status):
        sql = """
         update locate
         set status = '%s'
         where locate_id = %d
        """ % (status, int(locate_id))
        self.dbado.runsql(sql)

    def find_existing_locators(self, ticket_id):
        """ Inspect all locates of the given ticket, and return a list of
            locators assigned. """
        sql = """
         select a.locator_id
         from ticket t, locate l, assignment a
         where t.ticket_id = l.ticket_id
         and l.locate_id = a.locate_id
         and t.ticket_id = %d
         and a.active <> 0
         and l.closed = 0
        """ % (int(ticket_id),)
        results = self.dbado.runsql_result(sql)
        locators = []
        for row in results:
            locator = row.get("locator_id", None)
            if locator and locator not in locators:
                locators.append(locator)
        return locators

    def is_a_client(self, client_code, call_center):
        """ Return true if a client code of a given call center is a UQ client.
        """
        sql = """
         select count(*) as c from client
         where call_center = '%s'
         and oc_code = '%s'
         and active = 1
        """ % (call_center, client_code)
        results = self.dbado.runsql_result(sql)
        return int(results[0]["c"]) > 0

    def get_clients_dict(self):
        """ Get all active UQ clients, as a dictionary with call center as key.
            Value is a list of Client instances.
            NOTE: Does *NOT* do anything special with update call centers! """
        sql = """
         select oc_code, call_center, client_id, update_call_center,
                grid_email, alert, client_name
         from client
         where oc_code is not null
         and call_center is not null
         and active = 1
        """
        results = self.dbado.runsql_result(sql)
        d = {}
        for row in results:
            c = client.Client.load_from_row(row)
            call_center = row.get("call_center", "")
            if not d.has_key(call_center):
                d[call_center] = []
            d[call_center].append(c)
        return d

    def get_ticket_alert_keywords(self):
        '''
        Extracts the keywords and ticket_type to be used in setting ticket based alerts
        '''
        d = {}

        if static_tables.alert_ticket_keyword \
        and static_tables.alert_ticket_type:
            sql = '''
                  select call_center,
                         ticket_type,
                         atk.keyword
                  from alert_ticket_type AS att
                  inner join alert_ticket_keyword AS atk
                  on atk.alert_ticket_type_id = att.alert_ticket_type_id
                  '''
            results = self.dbado.runsql_result(sql)
            for row in results:
                cc = row['call_center']
                keyword = row['keyword']
                ticket_type = row['ticket_type']
                if d.has_key(cc):
                    if d[cc].has_key(keyword) and ticket_type not in d[cc][keyword]:
                        d[cc][keyword].append(ticket_type)
                    else:
                        d[cc][keyword] = [ticket_type,]
                else:
                    d[cc] = {keyword:[ticket_type,],}
        return d


    @staticmethod
    def has_uq_client(locates_on_ticket, logic, clients):
        """ Return true (a value that evaluates to true) if the set of locates
            contains a UQ client.
            <logic> is the appropriate BusinessLogic instance for the given
            call center.
            <clients> is simply a list of client codes.
        """
        for locate in locates_on_ticket:
            if locate in clients:
                return 10   # client found
            elif not logic.do_no_client_check(locate):
                return 11   # don't do the "no client check" for this one
                            # IOW, it counts as a client

        if not logic.do_no_client_check(""):
            return 12   # don't do the check for _anything_
                        # IOW, pretend we have a client

        return 0    # all attempts failed

    def has_no_uq_clients(self, ticket, ticket_id, logic, clients, update):
        """ Returns true if the given ticket has no UQ clients. """
        r1 = r2 = 0

        # try finding a client in the ticket's locates
        locates = [loc.client_code for loc in ticket.locates]
        r1 = self.has_uq_client(locates, logic, clients)

        # we only continue our inspection if the ticket is an update _and_ if
        # we didn't find a client yet
        if update and not r1:
            locates_raw = self.get_locates(ticket_id)
            locates = [row["client_code"] for row in locates_raw]
            r2 = self.has_uq_client(locates, logic, clients)

        return (not (r1 or r2))

    def locate_is_assigned(self, locate_id):
        """ Return true if the given locate is assigned, i.e. has an active
            assignment record. """
        sql = """
         select * from assignment
         where locate_id = %s
         and active = 1
        """ % (locate_id,)
        results = self.dbado.runsql_result(sql)
        return (len(results) > 0)

    # XXX maybe this should be moved to summarycollection? or a separate module
    # that deals with database/summaries?
    def check_complete_summaries(self, summcoll, log):
        ids = []
        z = summcoll.find_complete()
        for call_center, client_code in z:
            logic = businesslogic.getbusinesslogic(call_center, self, [])
            parts = summcoll.get(call_center, client_code)
            summcoll.mark_complete(parts)    # set 'complete' field (0 or 1)
            s_dups = self.find_duplicate_summaries(parts[0])
            try:
                with Transaction(self.dbado):
                    if s_dups and logic.allow_summary_updates():
                        summary_header_id = s_dups[0]["summary_header_id"]
                        log.log_event("Updating existing summary id %s..." % (
                         summary_header_id,))
                        self.storesummaries_update(parts, summary_header_id, log)
                    else:
                        ids = self.storesummaries(parts, log)
                    summcoll.remove_summaries_from_db(call_center, client_code)
                summcoll.remove_summaries_from_dict(call_center, client_code)
            except:
                ep = errorhandling2.ErrorPacket()
                ep.set_traceback()
                ep.add(summaries=parts, call_center=call_center,
                 client_code=client_code)
                log.log(ep, send=1, write=1, dump=1)
            # XXX one could wonder if we should catch exceptions here... raise
            # them, OK, but catching them is more something for a higher-level
            # program...

        return ids

    def set_client_id(self, locate_id, client_id):
        """ Set the client_id of a given locate. Normally this method shouldn't
            be necessary, but we (temporarily) need it now for FCO1
            pre-routing. """
        sql = """
         update locate
         set client_id = %s
         where locate_id = %s
        """ % (client_id, locate_id)
        self.dbado.runsql(sql)

    def insert_ticket_note(self, ticket_id, emp_id, transaction_date, note):
        note = note.replace("'", "")
        note = note.replace('"', '"')
        sql = """
         exec insert_ticket_note %s, %s, '%s', '%s'
        """ % (ticket_id, emp_id, transaction_date, note)
        self.dbado.runsql(sql)

    def insert_locate_note(self, locate_id, emp_id, transaction_date, note):
        sql = """
         insert notes
         (foreign_type, foreign_id, entry_date, uid, note)
         values (5, %s, '%s', '%s', '%s')
        """ % (locate_id, transaction_date, emp_id, note)
        self.runsql(sql)

    def get_ticket_notes(self, ticket_id):
        """ Return a list of notes for the given ticket. """
        rows = self.dbado.getrecords("notes", foreign_type=1,
               foreign_id=ticket_id, active=1)
        return [row['note'] for row in rows]

    #
    # Note: ticket_notes is a view that returns notes for tickets and their
    # associated locates.

    def get_all_notes(self, ticket_id, locate_id):
        """ Return a list of notes for the given ticket and associated
            locates.
        """
        sql = """
              select * from ticket_notes where ticket_id = %s
              """ % (ticket_id,)
        rows = self.dbado.runsql_result(sql)
        notes = []
        for row in rows:
            if row['foreign_type'] == '1':
                # ticket note
                notes.append(row['note'])
            elif row['foreign_type'] == '5' and row['foreign_id'] == locate_id:
                # locate note for this locate
                notes.append(row['note'])
        return notes

    def get_locate_notes(self, ticket_id, locate_id):
        """ Return a list of notes for the locates associated with the ticket.
        """
        sql = """
              select * from ticket_notes where ticket_id = %s order by notes_id
              """ % (ticket_id,)
        rows = self.dbado.runsql_result(sql)
        notes = []
        for row in rows:
            if row['foreign_type'] == '5' and row['foreign_id'] == locate_id:
                # locate note for this locate
                notes.append(row['note'])
        return notes

    def get_tickets_for_dups_2(self, sql):
        """ Execute SQL to find duplicates, return a list of strange stuff
            (whatever _ticketnode_to_ticket returns).  SQL statement is set
            by caller and is expected to return three datasets: tickets,
            locates, assignments.
        """
        datasets = self.dbado.runsql_result_multiple(sql)
        tickets, locates, assignments = datasets
        return tools.datasets_to_tickets(tickets, locates, assignments)

    def insert_responder_queue(self, locate_id):
        sql = "exec insert_responder_queue %s" % (locate_id,)
        self.dbado.runsql(sql)

    def add_attachment_record(self, **kwargs):
        """ Wrapper function for inserting a new attachment record.  <kwargs>
            must consist of fields present in the attachment table. """
        fieldnames = [f[0] for f in attachment.Attachment.__fields__]
        for attrname in kwargs.keys():
            assert attrname in fieldnames
        att = attachment.Attachment()
        att.set(**kwargs)
        att_id = att.insert(self)
        return att_id

    def get_additional_ticket_info(self, ticket_id, fieldnames=()):
        rows = self.getrecords("ticket", ticket_id=ticket_id)
        if not rows:
            raise KeyError, "Ticket not found: ticket_id=%s" % (ticket_id,)

        d = {}
        row = rows[0]
        if fieldnames:
            for fieldname in fieldnames:
                d[fieldname] = row[fieldname]
            return d
        else:
            return row # return all fields

    def add_plat(self, locate_id, plat):
        sql = "exec add_plat %d, '%s'" % (int(locate_id), plat)
        self.dbado.runsql(sql)

    #
    # ADO ticket routines

    def get_last_id_for_table(self, table):
        #rows = self.dbado.runsql_result("select scope_identity() as id")
        sql = "select ident_current('%s') as id" % (table,)
        rows = self.dbado.runsql_result(sql)
        return rows[0]['id']

    def get_ticket_data(self, ticket_id):
        sql = "exec get_ticket_data %s" % (ticket_id,)
        results = self.dbado.runsql_result_multiple(sql)
        assert len(results) == 3
        return tuple(results)
        #return (results + [[], [], []])[:3]

    def get_upload_locations(self):
        """ Return a dictionary with upload locations.  Key is the
            description, lowercased. """
        rows = self.getrecords("upload_location")
        d = {}
        for row in rows:
            key = row['description'].lower().strip()
            d[key] = row
        return d

    def get_employee_with_installer_number(self, ins_num):
        sql = """
         select emp_id from employee
         where installer_number = '%s'
         order by emp_id
        """ % (ins_num,)
        # note: the 'order by' is there to make the query deterministic, in
        # case someone entered multiple records with the same installer number.

        rows = self.runsql_result(sql)
        if rows:
            emp_id = rows[0]['emp_id']
            return emp_id
        else:
            return None # no such employee found

    def pagenum_is_varchar(self):
        """ Return true if map_page.map_page_num (and, is the assumption,
            map_cell.map_page_num and map_geo.map_page_num) is a varchar.
        """
        sql = """
         select data_type from information_schema.columns
         where table_name = 'map_page'
         and column_name = 'map_page_num'
        """
        rows = self.runsql_result(sql)
        if rows:
            t = rows[0].get('data_type')
            return (t == 'varchar')
        else:
            return False

    def get_latest_serial_number(self, ticket_id, source=None):
        """ Get all the versions of a given ticket, and return the serial_number
            of the newest one, if possible.  Otherwise, return None.
            (Used for NCA1/SCA1 responders.)
        """
        source_clause = ""
        if source:
            if isinstance(source, list):
                srclist = string.join(["'%s'" % src for src in source], ", ")
                source_clause = "and source in (%s)" % srclist
            else:
                source_clause = "and source = '%s'" % (source,)
        sql = """
         select serial_number from ticket_version
         where ticket_id = %s
         %s
         order by ticket_version_id
        """ % (ticket_id, source_clause)
        rows = self.runsql_result(sql)
        if rows:
            return rows[-1]['serial_number'] or None
        else:
            return None

    def get_high_profile_reasons_fiber(self):
        """ Get reference ids for high profile reasons indicating Fiber.
            Covers both single and multi high profile reasons. [QMAN-3370]
            Returns a tuple (r, m) with the relevant ref_ids as lists of 
            integers; r for hpreason, m for hpmulti.
        """
        reasons = []
        for type in ['hpreason', 'hpmulti']:
            sql = """
                select * from reference 
                where type = '%s'
                and (code like '%%fiber%%' or description like '%%fiber%%')
            """ % type
            rows = self.runsql_result(sql)
            reasons.append([int(row['ref_id']) for row in rows])
        return tuple(reasons)

    def get_multi_hp_reasons(self, call_center):
        """ Return a list of multiple HP reasons for the given call center.
            Result might be empty, in which case the center can still have a
            single-reason HP. """
        return self.getrecords('call_center_hp', call_center=call_center,
               active=1)

    def find_hp_reason_note(self, ticket_id, client_code):
        """ Find a HP reason note for the given ticket_id and client.
            The note is expected to be in a format like HPR-36,37,40,42
            ABC123, with the client code as the last "field".
            If no note is found, return None.
        """
        sql = """
         select * from ticket_notes
         where ticket_id = %s and note like 'HPR-%%'
         order by notes_id desc
        """ % (ticket_id,)
        rows = self.runsql_result(sql)
        for row in rows:
            note = row['note']
            parts = note.split()
            if parts and parts[-1] == client_code:
                return note
        return None

    #
    # responder_ack

    def insert_response_ack_waiting(self, resp_id, call_center, value):
        sql = """
         insert response_ack_waiting
         (response_id, reference_number, call_center)
         values (%s, '%s', '%s')
        """ % (resp_id, value, call_center)
        self.runsql(sql)

    def find_response_ack_waiting(self, call_center, value):
        sql = """
         select top 1 * from response_ack_waiting
         where call_center = '%s'
         and reference_number = '%s'
        """ % (call_center, value)
        rows = self.runsql_result(sql)
        if rows:
            return rows[0]['response_id']
        else:
            raise KeyError("response_ack_waiting: value not found: (%s, %s)" % (
                  call_center, value))

    def delete_response_ack_waiting(self, resp_id):
        sql = """
         delete response_ack_waiting
         where response_id = %s
        """ % (resp_id,)
        self.runsql(sql)

    def update_response_log(self, response_id, reply, success=1):
        """ Update an existing response_log record (indicated by response_id).
            Used to set the success flag and message (e.g. by responders). """
        width = static_tables.response_log[7][1].width # XXX unsafe! >=(
        self.dbado.update('response_log', 'response_id', response_id,
         success=int(success), # int to convert bool to 0 or 1
         reply=reply[:width])
         # truncate at 40, because that's the max length of the field :-/

    def assoc_ticket_work_order(self, ticket_id, wo_id):
        """ Associate a ticket with a work order, by adding a record to the
            work_order_ticket table.
            Return True if everything went OK, False if the record already
            exists.
        """
        sql = """
          insert work_order_ticket (wo_id, ticket_id)
          values (%d, %d)
        """ % (int(wo_id), int(ticket_id))
        try:
            self.dbado.runsql(sql)
        except Exception, e:
            if e.args and 'PRIMARY KEY constraint' in e.args[0]:
                return False # record already exists
            raise # otherwise, it's a different error
        return True

    def assign_locate(self, locate_id, locator_id):
        """ Assign a locator using the assign_locate SP. """
        sql = """
          exec assign_locate %d, '%s'
        """ % (int(locate_id), locator_id)
        self.dbado.runsql_result_multiple(sql)

    def set_due_date_manually(self, ticket_id, due_date):
        sql = """
          update ticket
          set legal_due_date = '%s', due_date = '%s'
          where ticket_id = %d
        """ % (due_date, due_date, int(ticket_id))
        self.dbado.runsql(sql)

    def add_ticket_hp_info(self, ticket):
        for line in ticket._hp_info:
            thp = ticket_hp.TicketHPInfo()
            thp.set(ticket_id=ticket.ticket_id, data=line)
            thp.insert(self)

#
# Wrappers around some DBInterface[ADO] methods.  When using these, the
# profiler times the method calls (as opposed to when using constructs
# like tdb.dbado.runsql() etc.

# XXX [hn] These can probably be replaced with decorators.

ADO_WRAPPER_NAMES = [
    'count',
    'delete',
    'deleterecords',
    'getrecords',
    'getrecords_ordered',
    'insert',
    'insertrecord',
    'runsql',
    'runsql_result',
    'runsql_result_multiple',
    'runsql_with_parameters',
    'update',
    'updaterecord',
]

def add_ado_wrapper(name):
    def f(self, *args, **kwargs):
        method = getattr(self.dbado, name)
        return method(*args, **kwargs)
    _ = new.function(f.func_code, f.func_globals, name, f.func_defaults,
        f.func_closure)
    # make sure we don't accidentally shadow TicketDB's methods
    if hasattr(TicketDB, name):
        raise AttributeError, "Wrapper %s shadows existing method!" % (name,)
    setattr(TicketDB, name, _)

for name in ADO_WRAPPER_NAMES:
    add_ado_wrapper(name)


