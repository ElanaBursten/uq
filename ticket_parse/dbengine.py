# dbengine.py
# Abstract database engine, to be subclassed to implement different ways of
# accessing databases: ADO/win32, ODBC, IronPython, etc.
#
# A DBEngine needs to provide a minimum of methods to be used by DBInterface.

import config

class Error(Exception):
    def __init__(self, msg, **kwargs):
        Exception.__init__(self, msg)
        self.msg = msg
        self.data = kwargs
        self.args = (msg, self.data)

class RunSQLException(Error):
    """ General exception raised in a RunSQL routine. RunSQLException's used to
        be DBException's. And this class was added to maintain the existing
        behavior of the code in main.py, that drops files into error
        directories. """

class DBException(Error):
    """ A database exception. Ideally this would only include exceptions
        originating in the DB server. But the ADO interface code currently
        includes all COM exceptions coming from the OLE DB provider. """

class DBFatalException(DBException):
    """ A fatal database exception, e.g. a network error. This is only raised
        by the automated tests. """

class DBTimeoutError(DBException):
    """ DBException caused by a command (not a connection) timeout. """
TimeoutError = DBTimeoutError

class DBEngine(object):

    def __init__(self):
        self._conn = None # low-level connection object
        self.timeout = config.DEFAULT_DB_TIMEOUT_SECONDS

    #
    # abstract interface

    def runsql(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_result(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_result_multiple(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_with_parameters(self, sql, params, timeout=None):
        raise NotImplementedError

    def begin_transaction(self):
        raise NotImplementedError

    def rollback_transaction(self):
        raise NotImplementedError

    def commit_transaction(self):
        raise NotImplementedError


