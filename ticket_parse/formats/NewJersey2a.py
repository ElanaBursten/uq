# NewJersey2a.py

import NewJersey2
import HarlingenTESS
from re_shortcut import R

class Parser(NewJersey2.Parser):
    pass

class SummaryParser(HarlingenTESS.SummaryParser):
    re_ticket = R("(\*?)(\d+) (GSUPLS\d+)-(\d+) (\d\d:\d\d)")
    re_date = R("for the period ([0-9/]+) ([0-9:]+) thru ([0-9/]+) ([0-9:]+)")
    DEFAULT_CLIENT_CODE = 'GSUPLS'
