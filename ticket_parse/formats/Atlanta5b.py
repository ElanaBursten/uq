# Atlanta5b.py
# Like Atlanta5/5a, but the audits are different ("SENTRi")

import Atlanta5
import audit_sentri

class Parser(Atlanta5.Parser):
    call_center = 'Atlanta5b'

class SummaryParser(audit_sentri.SummaryParser):
    pass

