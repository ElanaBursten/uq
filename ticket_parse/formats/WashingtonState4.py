# WashingtonState4.py

import WashingtonState2

class Parser(WashingtonState2.Parser):
    def __init__(self, *args, **kwargs):
        WashingtonState2.Parser.__init__(self, *args, **kwargs)
        self.term_translations = self.term_translator.get_translations(
         self.ticket.ticket_format)
        
    def exclude_locate(self, client_code):
        # This assumes client_code only translates to 1 term
        term_translation = self.term_translations.get(client_code, [])
        if term_translation:
            return term_translation[0] in self.exclude
        return client_code in self.exclude
        
class SummaryParser(WashingtonState2.SummaryParser):
    pass
