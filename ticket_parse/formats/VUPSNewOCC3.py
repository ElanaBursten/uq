# VUPSNewOCC3.py

import VUPSNewMeetSpecial
import Richmond3
import re
from re_shortcut import R

class Parser(VUPSNewMeetSpecial.Parser):
    PARSE_GRIDS = True

    re_work_description = R("^Excavation area: *(.*?)\nInstructions:", re.DOTALL)
    re_work_remarks = R("^Instructions: *(.*?)\nWhitelined", re.DOTALL)

    def find_grids(self):
        if not self.PARSE_GRIDS:
            return []

        grids = []
        lines = self.data.split('\n')
        for line in lines:
            if line.startswith("Grids: "):
                line = line[6:]
                parts = line.split()
                for part in parts:
                    idx = part.find('-')
                    if idx >= 0:
                        part = part[:idx]
                    grids.append(part)

        return grids

class SummaryParser(Richmond3.SummaryParser):
    pass
