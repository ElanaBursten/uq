# TennesseeKorterra.py
import string
import Tennessee
import HoustonKorterra

class Parser(Tennessee.Parser):
    def find_work_remarks(self):
        stub = []
        ok = 0
        for line in string.split(self.data, "\n"):
            line = string.strip(line)
            if ok:
                if line.startswith("["):
                    stub.append(line[1:-1])
                else:
                    break
            else:
                if line.startswith("LOCATION INFORMATION"):
                    ok = 1

        return string.join(map(string.strip, stub), " ")

class SummaryParser(HoustonKorterra.SummaryParser):
    pass
