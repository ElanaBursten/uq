# Utah.py

import NorthCarolina
from re_shortcut import R
import Alabama
import summarydetail
from summaryparsers import SummaryError

class Parser(NorthCarolina.Parser):
    # sometimes it's hardly worth the trouble to inherit from another
    # parser... :-(
    re_ticket_number = R("^Ticket : (.*?) (Rev|Taken):")
    re_call_date = R("^Ticket :.*Taken: ([0-9/]+) ([0-9:]+)")
    re_transmit_date = R("\d+ UTAH[a-z] ([0-9/]+) ([0-9:]+)")
    re_ticket_type = R(
     "[A-Z0-9*]+ +\d+ +UTAH[a-z] +[0-9/]+ +[0-9:]+ [A-Z0-9\-]+ +(.*?) *$")
    re_work_date = R("Update By: ([0-9/]+) ([0-9:]+)")
    re_work_city = R("^State.*Place: (.*)(\s+|$)")
    re_work_address_street = R("^Street +: +(.*?) *$")
    re_work_cross = R("^Cross 1 : +(.*?)( *$| *Within)")
    re_work_type = R("P&D.*Work type: (.*) *$")
    re_con_name = R("^Company : (.*?) +Phone")
    re_caller_phone = R("^Caller.*Phone: (\S+) *Type")
    re_caller_fax = R("^Fax +: *(\S+)")

class SummaryParser(Alabama.SummaryParser):
    LENGTH = len("00007 C60090085-00C 08:20 R ")
    RECOG_HEADER_2 = "THIS IS YOUR END OF OF DAY SUMMARY FOR"

    re_footer = R("^\s+TOTAL\s+- (\d+)")
    re_recog_header_1 = R("BAJA +? \*(SUM|EOD)\* UTAH[a-z]")
    re_summary_date = R("THIS IS YOUR END OF OF DAY SUMMARY FOR ([0-9/]+)\.")
    re_ticket = R("(\d+) ([A-Z]\d+)-(\d+[A-Z]) (\d\d:\d\d)? ?(.?)")

    def get_summary_date(self, data):
        m = self.re_summary_date.search(data)
        if m:
            d = m.group(1)
            month, day, year = d.split("/")
            return "20%s-%s-%s 00:00:00" % (year, month, day)
        else:
            # all the other summary parsers just set the date to an empty string,
            # why raise an exception here?
            raise SummaryError, "Could not determine summary date"

    def read(self, data):
        tickets = []
        results = self.re_ticket.findall(data)
        for r in results:
            seqnr, ticket_number, revision, time, indicator = r
            t = [seqnr, ticket_number, time, revision, indicator.strip()]
            detail = summarydetail.SummaryDetail(data=t)
            tickets.append(detail)
        return tickets
