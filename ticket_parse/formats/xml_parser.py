# xml_parser.py

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from parsers import BaseParser
from summaryparsers import BaseSummaryParser
from tools import PseudoMatch

class Finder(object):
    def __init__(self, root, pattern, attribute=None, namespaces={}):
        self.root = root
        self.pattern = pattern
        self.attribute = attribute
        self.namespaces = namespaces

    def findall(self, data):
        pattern_match = self.root.findall(self.pattern, self.namespaces)
        values = []
        if pattern_match is not None:
            for element in pattern_match:
                if self.attribute is not None:
                    if element.attrib.has_key(self.attribute):
                        values.append(element.attrib[self.attribute])
                else:
                    values.append(element.text)
        return values

    def search(self, data):
        pattern_match = self.root.find(self.pattern, self.namespaces)
        if pattern_match is not None:
            # apparently ElementTree returns None for the text for nodes that
            # do exist, but are empty; we need the empty string instead
            if pattern_match.text is None:
                pattern_match.text = ""
            if self.attribute is not None \
            and pattern_match.attrib.has_key(self.attribute):
                return PseudoMatch(pattern_match.attrib[self.attribute] or "")
            else:
                return PseudoMatch(pattern_match.text or "")
        else:
            return None

def find_all_with_attribute(element,pattern, attribute, value):
    '''
    ElementTree will have this capability in 1.3 (Python 2.5 ships with 1.2.7)
    '''
    match_elements = element.findall(pattern)
    found = []
    for match_element in match_elements:
        if match_element.attrib.has_key(attribute) \
        and match_element.attrib[attribute] == value:
            found.append(match_element)
    return found

class XmlParser(BaseParser):
    NAMESPACES = {}
    def __init__(self, *args, **kwargs):
        BaseParser.__init__(self, *args, **kwargs)
        self.data = self.data.strip() # no surrounding whitespace allowed
        cstr_file = StringIO(self.data)
        self.tree = ET.parse(cstr_file)
        self.root = self.tree.getroot()
    def find(self, path, *args, **kwargs):
        """ Find an XML element and return its value as a string. If not
            found, return None. """
        xmlf = Finder(self.root, path, namespaces=self.NAMESPACES, *args, **kwargs)
        match = xmlf.search(self.data)
        if match is not None:
            return match.group(1)
        else:
            return None

class XMLSummaryParser(BaseSummaryParser):
    NAMESPACES = {}
    def __init__(self, *args, **kwargs):
        BaseSummaryParser.__init__(self, *args, **kwargs)
        self.root = None
    def parse(self, data):
        data = data.strip() # no leading/trailing whitespace
        self.root = ET.parse(StringIO(data)).getroot()
        return BaseSummaryParser.parse(self, data)

