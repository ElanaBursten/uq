# Nevada.py

import NorthCalifornia
from re_shortcut import R

class Parser(NorthCalifornia.Parser):
    SPECIAL_LOCATES = []
    re_company = R("^ *Done for: +(.*?) *Explosives:")
    re_caller_phone = R("^Telephone: +(.*?) *Fax:")
    re_caller_fax = R("^Telephone: +.* *Fax: *([\d-]+)")

class SummaryParser(NorthCalifornia.SummaryParser):
    re_recog_header_1 = R("[A-Z0-9]+ +USA[NS] +[0-9/]+")
