# Albuquerque2.py
import re
import Albuquerque
import Lanham
from re_shortcut import R

from summaryparsers import BaseSummaryParser
import string
import tools
import locate
import summarydetail


class Parser(Albuquerque.Parser):

    required = Lanham.Parser.required[:] # without serial_number

class SummaryParser(Lanham.SummaryParser):
    re_header = R("THE FOLLOWING MESSAGE NUMBERS", re.DOTALL|re.MULTILINE)
    re_footer = R("EVERYONE HAVE A GOOD DAY")
    re_total = R("TOTAL = (\d+)")
    re_date = R("TO YOU TODAY +([0-9/]+)")
    re_client_code = R("STATION: (\S+)")
    BLOCK_START = "(NEW MEXICO"
    dummy_client_code = "FAQ2"

    def has_header(self, data):
        return not not self.re_header.search(data)

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_expected_tickets(self, data):
        m = self.re_total.search(data)
        if m:
            try:
                return int(m.group(1))
            except ValueError:
                return 0
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            d = tools.isodate(m.group(1))
            #d.resettime()
            return d
        else:
            d = Date()
            d.resettime()
            return d.isodate()

    def get_client_code(self, data):
        # there is no client code on this type of summaries, so we make one up
        #return self.dummy_client_code
        m = self.re_client_code.search(data)
        if m:
            return m.group(1)
        else:
            return "FAQ2"

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        status = 0
        for line in lines:
            if status == 0 and line.startswith(self.BLOCK_START):
                status = 1
            elif status == 1 and not line.strip():
                status = 2
            elif status == 2:
                if line.startswith("TOTAL"):
                    break
                parts = line.split()
                for part in parts:
                    seq_number, ticket_number = part.split("-")
                    t = [seq_number, ticket_number, '', '', '']
                    detail = summarydetail.SummaryDetail(data=t)
                    tickets.append(detail)

        return tickets

