# DIGGTESS.py

import re
from parsers import BaseParser, ParserError
from re_shortcut import R
import Dallas1
import datetime
import tools
import string
import locate
from static_tables import ticket

class Parser(BaseParser):

    re_ticket_number = R("^Ticket Number:\s*(\S+)")
    re_operator = R("By:[ \t]+(\S.+\S)\s*$")
    re_channel = R("^Source:[ \t]+(\S+)")
    re_work_notc = R("Hours Notice:[ \t]+(\d+)")
    re_ticket_type = R("^Type:\s*(.+)\s*Date:")
    re_call_date = R("^Type:\s*.*\s*Date:\s*(\S+)\s+(\S+)\s+(\S+)")
    re_transmit_date = re_call_date # The same?

    #re_map_ref = R("^Map Reference:[ \t]+(\S.+\S)\s*Company")
    re_map_ref = R("^Map Reference:[ \t]+(.*?)\s*Company")
    # Company section
    re_con_name = R("(\S.*?)\s+Type:\s+\S+\s*?$")
    re_con_address = R("(^\S.*\S)\s+Contact:")
    re_con_city = R("(^\S.+?),\s+?\S+?\s+?\d+?\s+Caller:")
    re_con_state = R("^\S.+?,\s+?(\S+?)\s+?\d+?\s+Caller:")
    re_con_zip = R("^\S.+?,\s+?\S+?\s+?(\d+?)\s+Caller:")
    re_caller = R("Caller:[ \t]*(\S.*\S)")
    re_caller_contact = R("Contact:[ \t]*(\S.*\S)")
    re_caller_phone = R("Caller Phone:[ \t]*(\S.*\S)")
    re_caller_fax = R("^Fax:\s*(\S.*\S)\s*Callback:")
    re_caller_email = R("^Caller Email:\s*([a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6})")
    re_caller_altcontact = R("^Alt Contact:\s+(\S.*\S)\s*Caller")

    re_work_state = R("^State:\s*(\S.*?)\s*Work Date:")
    re_work_county = R("^County:\s*(\S.*?)\s*Type:")
    re_work_city = R("^Place:\s*(\S.*?)\s*Done For:")
    re_work_address_number = R("^Street: *(\d\w*) *")
    re_work_address_street = R("^Street: *\d\w* *(\S.*\S)")
    re_work_date = R("Work Date:\s*(\S+) at (\S+)")
    re_work_type = R("^Nature of Work:[ \t]+(\S.*\S)")
    re_company = R("Done For:\s*(\S.*\S)")
    re_work_remarks = R("^Remarks.*-+(.*?).*Members")
    re_work_cross = R("^Intersection:[ \t]*(.*?)\s*$")
    re_explosives = R("^Explosives:\s+(\S+)\s+Deeper")
    re_map_page = R("((MAPSCO|KEYMAP) \d+[A-Z]*,[A-Z0-9]+)")

    def f_call_date(self, match):
        cdate, ctime, ampm = match.group(1), match.group(2), match.group(3)
        month, day, year = [int(item) for item in cdate.split("/")]
        if year < 100:
            year += 2000
        hour, minute, second = [int(item) for item in ctime.split(':')]
        if ampm == 'PM' and hour < 12:
            hour += 12
        return "%02d-%02d-%02d %02d:%02d:%02d" % (year, month,day, hour,
                                                  minute, second)

    f_transmit_date = f_call_date

    def f_work_date(self, match):
        month, day, year = [int(item) for item in match.group(1).split("/")]
        if year < 100:
            year += 2000
        hour = int(match.group(2)[0:2])
        minute = int(match.group(2)[2:])
        return "%02d-%02d-%02d %02d:%02d:%02d" % (year, month,day, hour,
                                                  minute, 0)

    _re_work_lat1 = R("^Latitude:\s*(\S+)")
    _re_work_lat2 = R("^Second Latitude:\s*(\S+)")
    def find_work_lat(self):
        m1 = self._re_work_lat1.search(self.data)
        m2 = self._re_work_lat2.search(self.data)
        lat1 = lat2 = 0.0
        if m1:
            lat1 = float(m1.group(1))
        if m2:
            lat2 = float(m2.group(1))
        else:
            lat2 = lat1
        return (lat1+lat2)/2.0

    _re_work_long1 = R("^Latitude:\s*\S+\s*Longitude:\s*(\S+)")
    _re_work_long2 = R("^Second Latitude:\s*\S+\s*Second Longitude:\s*(\S+)")
    def find_work_long(self):
        m1 = self._re_work_long1.search(self.data)
        m2 = self._re_work_long2.search(self.data)
        long1 = long2 = 0.0
        if m1:
            long1 = float(m1.group(1))
        if m2:
            long2 = float(m2.group(1))
        else:
            long2 = long1
        return (long1+long2)/2.0

    _re_termid = R("DIG-TESS Locate Request For (\S+)")

    def find_locates(self):
        locates = []
        m = self._re_termid.search(self.data)
        if m:
            locates.append(m.group(1))

        return [locate.Locate(z) for z in locates]

    def find_work_remarks(self):
        pieces = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines,
               lambda s: s.startswith("Remarks"))
        if idx1 > -1:
            for i in range(idx1+2, len(lines)):
                line = lines[i]
                if not line.strip():
                    break
                if line.startswith("Members"):
                    break
                pieces.append(line.strip())
        return string.join(pieces, "\n")

    def is_fttp(self):
        if self.__class__ is Parser:
            return self.ticket.company.find('VERIZON OSP') >= 0
        else:
            return False

    _re_meet = re.compile('MEET', re.MULTILINE | re.IGNORECASE)
    def post_process(self):
        m = self._re_meet.search(self.ticket.work_remarks)
        if m:
            self.ticket.ticket_type = self.ticket.ticket_type.strip() + '-MEET'
        BaseParser.post_process(self)

from summaryparsers import BaseSummaryParser
from re_shortcut import R as R_sum
import summarydetail

class SummaryParser(BaseSummaryParser):

    S_HEADER = "FROM TESS"
    S_FOOTER = "TOTAL FOR"

    def has_header(self, data):
        return string.find(data.upper(), self.S_HEADER) > -1

    def has_footer(self, data):
        return string.find(data.upper(), self.S_FOOTER) > -1

    def get_client_code(self, data):
        m = R_sum("^TOTAL FOR (.*):").search(data.upper())
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = R_sum("^AUDIT FOR ([0-9/]+)").search(data.upper())
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = R_sum("^TOTAL FOR [A-Z0-9]+: (\d+)").search(data.upper())
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].upper().startswith("TYPE  SEQ#"):
                idx = i
                break
        if idx >= 0:
            idx = idx + 2   # skip a line
            while idx < len(lines):
                if not lines[idx].strip() \
                or lines[idx].upper().strip().startswith("FOR CODE"):
                    break   # empty line ends this session
                line = lines[idx][5:]
                parts = string.split(line)
                assert len(parts) >= 3, "Unparseable line: %r" % (parts,)
                number, ticket_number, status = parts[:3]
                detail = summarydetail.SummaryDetail(
                         [number, ticket_number, "00:00", "", ""])
                tickets.append(detail)

                idx = idx + 1

        return tickets

