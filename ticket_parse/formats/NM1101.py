# NM1101.py

import Albuquerque2
from re_shortcut import R
import re

class Parser(Albuquerque2.Parser):
    re_caller_fax = R("Fax No: +(\S+) *$", re.DOTALL)

class SummaryParser(Albuquerque2.SummaryParser):
    re_client_code = R("STATION: (\S+)")
    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        if m:
            return m.group(1)
        else:
            # there is no client code on this type of summaries, so we make one up
            return "unknown"
