# GreenvilleNew.py
# NOTE: Supports two different date formats.

from parsers import BaseParser
from re_shortcut import R
import summaryparsers
import locate
import re
import string
import tools
from static_tables import ticket as ticket_table

import Tennessee

# format may support two kinds of dates, one with AM/PM indicator, one without
DATE_MATCH_1 = "([0-9/]+)\s+([0-9:]+)\s+(AM|PM)"
DATE_MATCH_2 = "([0-9/]+)\s+([0-9:]+)"

def _datefinder(base):
    """ Make a custom method that finds a date, in one of the two formats,
        starting at the given base. """
    def _f_(self):
        for s in (DATE_MATCH_1, DATE_MATCH_2):
            re_date = R(base + s)
            m = re_date.search(self.data)
            if m:
                return tools.isodate(string.join(m.groups(), " "))
    return _f_

class Parser(BaseParser):
    MAX_LOCATES = 50

    # hairy regex, because the different date formats are in the way =/
    re_ticket_type = R("\w+\s+\d+\s+PUPS\s+.*?[0-9/]+\s+[0-9:].*?\d{5,}\s+(.*?)\s*$")
    re_update_of = R("^Old Ticket Number: (\d+)")

    re_ticket_number = R("^Ticket Number: (\d+)")
    re_work_state = R("^State: (\S+)")
    re_work_county = R("^State.*?County: (.*?) *$")
    re_work_city = R("^Place: (.*?) *$")
    re_work_address_number = R("^Address Number: (.*?)$")
    re_work_address_street = R("^Street: *(.*?)(?: *Address In Instructions.*| *)$")
    re_work_cross = R("^Inters St: (.*?) *$")
    re_work_type = R("^Type of Work: (.*?) *$")
    re_company = R("^Work Done By: (.*?) *$")
    re_explosives = R("^Boring/Drilling: [YN] Blasting: ([YN]) White Lined:")

    re_caller = R("^Name: (.*?)     ")
    re_con_name = R("^Name: .*?     \s+(.*?) *$")
    re_con_address = R("^Address: (.*?) *$")
    re_con_city = R("^City: (.*?) State:")
    re_con_state = R("^City.*?State: (\S+)")
    re_con_zip = R("^City.*?Zip: (\S+)")
    re_caller_phone = R("^Phone: (.*?) Ext:")
    re_caller_fax = R("^Fax: (.*?) Caller Email:")
    re_caller_email = R("Caller Email: (\w*?) *$")
    re_work_remarks = R("^Remarks/Instructions: (.*?)\nCaller Information",
                      re.DOTALL)
    re_work_description = re_work_remarks

    _re_locates = R("^Members Involved: (.*?)Map Link", re.DOTALL|re.MULTILINE)

    f_transmit_date = lambda s, m: tools.isodate(string.join(m.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def f_work_description(self, match):
        # FIXME: should not rely on ticket_table >=(
        if len(match.group(1)) > ticket_table[10][1].width:
            return match.group(1)[:ticket_table[10][1].width - 12] \
                   + " (More ...)"
        else:
            return match.group(1)

    find_transmit_date = _datefinder("\w+\s+\d+\s+PUPS\s+.*?\s+")
    find_call_date = _datefinder("^Created Date:\s+")
    find_work_date = _datefinder("^Work Date/Time:\s+")

    def find_locates(self):
        m = self._re_locates.search(self.data)
        if not m:
            return []
        parts = m.group(1).split()
        return [locate.Locate(x) for x in parts]

    def post_process(self):
        # convert lat/long to qtrmin value
        lines = self.data.split("\n")
        latlonglines = [L for L in lines if L.startswith("Lat/Long")]
        if latlonglines:
            parts = latlonglines[0].split()
            latstring = parts[1][:-1]
            longstring = parts[2]
            try:
                lat = float(latstring)
                long = float(longstring)
            except:
                self.ticket.map_page = ""
            else:
                if lat > 0 and long < 0:    # !
                    self.ticket.map_page = tools.decimal2grid(lat, long)
                    self.ticket.work_lat = lat
                    self.ticket.work_long = long
                else:
                    self.ticket.map_page = ""

        # remove spaces from work_address_number if necessary
        number = self.ticket.work_address_number
        #parts = map(string.strip, number.split(" ", 1))
        parts = [s.strip() for s in number.split(" ", 1)]
        if len(parts) > 1:
            self.ticket.work_address_number = parts[0]
            self.ticket.work_address_number_2 = parts[1]

        # remove superfluous whitespace
        self.ticket.work_remarks = self.ticket.work_remarks.strip()
        self.ticket.work_description = self.ticket.work_description.strip()

        BaseParser.post_process(self)

class SummaryParser(Tennessee.SummaryParser):

    S_HEADER = "PUPS AUDIT"
    S_AUDIT_DATE = R("^(?:AUDIT FOR|PUPS AUDIT)\s+([0-9/]+)")

    def has_header(self, data):
        return string.find(data.upper(), self.S_HEADER) > -1

