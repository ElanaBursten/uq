# SouthCalifornia.py

from parsers import BaseParser
from re_shortcut import R
import re
import string
import locate
import tools
import quadrilateral
import _SanDiegoSummary

def parse_lat_long(line):
    """
    Parse and return lat/long pairs
    """
    parts = line.split(':')[1]
    pair = parts.split() # Split on whitespace
    lat1,long1 = pair[0].split('/')
    lat2,long2 = pair[1].split('/')
    return float(lat1),float(long1),float(lat2),float(long2)

class Parser(BaseParser):

    re_transmit_date = R("[A-Z0-9]+\s+\d+[A-Z]+ USAS ([0-9/]+) ([0-9:]+)")
    re_ticket_type = R(
     "[A-Z0-9]+\s+\d+[A-Z]+ USAS [0-9/]+ [0-9:]+ \S+ *(.*?) *$")
    re_ticket_number = R("^ *Ticket : (\S+)")
    re_update_of = R("Old Tkt:\s+(\S+)")
    re_call_date = R("^ *Ticket : +\S+ +Date: ([0-9/]+) Time: ([0-9:]+)")
    re_operator = R("^ *Ticket.*?Oper: (\S+)")
    re_revision = R("Revision: (\S+)")

    re_work_state = R("^ *State: (\S+)")
    re_work_county = R("^ *State:.*?County: *(.*?) *Place")
    re_work_city = R("^ *State.*?Place: (.*?) *$")
    re_work_address_street = R("^ *Address: (.*?) *$")
    re_work_cross = R("^ *X/ST.*?: (.*?) *$", re.DOTALL)

    re_work_description = R("^ *Instruct +: *(.*?) *Permit")

    re_work_type = R("^ *Work : (.*?) *$")
    re_work_date = R("^ *Work date: ([0-9/]+) Time: ([0-9:]+)")
    re_company = R("^ *Done for : (.*?) *$")
    re_con_name = R("^ *Company: (.*?) *Caller")
    re_caller = R("^ *Company.*?Caller: (.*?) *$")
    re_caller_email = R("^ *Email: (.*?) *$")
    re_con_city = R("^ *City&St: (.*?),")
    re_con_state = R("^ *City&St:.*?, (\S+)")
    re_con_zip = R("Zip: (\S+)")
    re_caller_fax = R("Fax: (.*?) *$")
    re_caller_phone = R("^ *Phone: (.*?) *($|Ext)")
    re_map_page = R("^ *Grids: (\S+)")
    re_priority = R("Priority: (\S+) *$")

    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    keyword_section = R("COMMENTS(.*)Mbrs", re.DOTALL)

    # location, work description, members/locates, remarks...

    def find_con_address(self):
        # separate function to allow both versions.
        re_con_address_1 = R("^ *Company.*?Address: (.*?) *$", re.DOTALL)
        re_con_address_2 = R("^ *Co Addr: (.*?) *$", re.DOTALL)
        for line in self.data.split('\n'):
            m = re_con_address_2.search(line)
            if m:
                return m.group(1)

        m = re_con_address_1.search(self.data)
        if m:
            return m.group(1)

        return ""

    def find_work_remarks(self):
        desc = ""
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.lstrip().startswith("Locat:"))
        idx2 = tools.findfirst(lines[idx1+1:],
               lambda s: not s.lstrip().startswith(":"))
        for i in range(idx1, idx2+idx1+1):
            desc = desc + lines[i].lstrip().lstrip(":").lstrip() + " "
        if desc.startswith("Locat:"):
            desc = desc[6:].lstrip()

        # there might be a REMARKS block (as seen in SouthCalifornia):
        idx1 = tools.findfirst(lines, lambda s: s.lstrip().strip() == "REMARKS")
        if idx1 > -1:
            idx2 = tools.findfirst(lines[idx1+2:], lambda s: not s.strip())
            if idx2 > -1:
                lines2 = lines[idx1+2:idx2+2+idx1]
                blurb = string.join(lines2, "")
                desc = desc + "\n" + blurb

        return desc.strip()

    # XXX needs to be moved to call center specific code
    SDG_MAIN = "SDG09"
    SDG_SUB = ["SDG04", "SDG05", "SDG06", "SDG07", "SDG08"]

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.lstrip().startswith("Mbrs :"))
        for i in range(idx1, len(lines)):
            line = lines[i]
            if not line.strip():
                break   # done!
            if line.lstrip().startswith("Mbrs :"):
                line = line[6:]
            parts = line.split()
            locates.extend(parts)

        # QMAN-3417: add 'City of Corona' locates
        if self.__class__ is Parser:
            if 'UCOR19' in locates:
                locates.extend(['UCOR19-S', 'UCOR19-RW', 'UCOR19-E', 'UCOR19-SL', 'UCOR19-SD','UCOR19-FIB','UCOR19-SIG'])

        locates = map(locate.Locate, locates)
        return locates

    # TODO: move to a TicketAdapter for SCA1
    def handle_SDG_locates(self, locates):
        # Mantis #1279: If SDG09 is on the ticket, and any of SDG04..08, then
        # we *don't* add SDG09, but the first one of SDG04..08 that appears.
        # we also add a 'G' term, e.g. if we add SDG06, then also SDG06G.

        # only do this for SCA1, not for other centers
        if self.__class__ is Parser:
            # take all SDGs out and put them in a separate list
            sdglocates = []
            # add SDG locates to separate list, in the order that they appear
            for loc in locates:
                if loc in [self.SDG_MAIN] + self.SDG_SUB:
                    sdglocates.append(loc)
            # remove SDG locates from locates list afterwards
            for sdgloc in sdglocates:
                locates.remove(sdgloc)
            # is SDG_MAIN in our list? if so, add the first locate from
            # SDG_SUB that we find
            if self.SDG_MAIN in sdglocates:
                sdglocates.remove(self.SDG_MAIN)
                if sdglocates:
                    locates.append(sdglocates[0])
                    locates.append(sdglocates[0]+'G')
        return locates

    def find_kind(self):
        if self.ticket.priority == "1" and self.data.find("EMER") > -1:
            return "EMERGENCY"
        elif self.ticket.priority == "0":
            return "EMERGENCY"
        # this is really businesslogic and should be moved elsewhere... except
        # that the businesslogic class has no notion about the ticket image.
        return BaseParser.find_kind(self)

    def mod_ticket_type(self):
        _re_meet = re.compile('MEET', re.MULTILINE | re.IGNORECASE)
        _re_fiber = re.compile('fiber', re.MULTILINE | re.IGNORECASE)
        if self.ticket.kind == "EMERGENCY":
            # Add -EMER to type
            self.ticket.ticket_type = self.ticket.ticket_type.strip() + '-EMER'
        m = _re_meet.search(self.ticket.work_remarks)
        if m:
            self.ticket.ticket_type = self.ticket.ticket_type.strip() + '-MEET'
        m = _re_fiber.search(self.data)
        if m and 'VERIZON' in self.ticket.company.upper():
            self.ticket.ticket_type = self.ticket.ticket_type.strip() + '-FTTP'

    def post_process(self):
        self.mod_ticket_type()
        # there may be a number buried in work_address_street; get it out of
        # there

        s = self.ticket.work_address_street
        a1, sep, a2 = s.partition('Street:')  # new-style format (Mantis 2702)
        if sep:
            self.ticket.work_address_number = a1.strip()
            self.ticket.work_address_street = a2.strip()

        elif self.ticket.work_address_street \
        and self.ticket.work_address_street[:1] in "0123456789":
            parts = string.split(self.ticket.work_address_street)
            self.ticket.work_address_number = parts[0]
            rest = string.join(parts[1:], " ")
            self.ticket.work_address_street = rest

        # if there is no cell number, assume A01 (e.g. '014' -> '014A01')
        m = re.match("^\d+$", self.ticket.map_page)
        if m:
            self.ticket.map_page += "A01"

        BaseParser.post_process(self)

    def work_lat_long(self):
        """
        Determine the work latitude and longitude
        Expecting something like this:
        Lat/Long  : 32.573448/-117.058623 32.573448/-117.057489
                  : 32.567181/-117.058623 32.567181/-117.057489
        """
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.lstrip().startswith("Lat/Long"))
        if idx > -1:
            # Found it
            line = lines[idx]
            try:
                lat1,long1,lat2,long2 = parse_lat_long(line)
                # Get the next line
                line = lines[idx+1]
                lat3,long3,lat4,long4 = parse_lat_long(line)
                # Extract the geometric centroid of the quadrilateral
                q = quadrilateral.Quadrilateral([(lat1,long1),
                                                 (lat2,long2),
                                                 (lat3,long3),
                                                 (lat4,long4)])
                return q.centroid()
            except:
                return 0.0, 0.0
        else:
            return 0.0, 0.0

    def find_work_long(self):
        """
        Find the work longitude
        """
        lat,long = self.work_lat_long()
        return long

    def find_work_lat(self):
        """
        Find the work latitude
        """
        lat,long = self.work_lat_long()
        return lat

class SummaryParser(_SanDiegoSummary.SummaryParser):
    pass
