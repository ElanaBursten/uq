# GeorgiaXML.py

import date
import string
#
import locate
import summarydetail
import tools
import xml_parser

class Parser(xml_parser.XmlParser):

    PARSE_GRIDS = False
    NAMESPACES = {'ns': "http://tempuri.org/TicketDataSet.xsd"}

    def __init__(self, *args, **kwargs):
        xml_parser.XmlParser.__init__(self, *args, **kwargs)

        # helper function
        def find(path, *args, **kwargs):
            return xml_parser.Finder(self.root, path,
                   namespaces=self.NAMESPACES, *args, **kwargs)

        self.re_ticket_number = find("./ns:TICKET/ns:Ticket_Number")
        self.re_revision = find("./ns:TICKET/ns:Version_Number")
        self.re_ticket_type = find("./ns:TICKET/ns:Ticket_Type")
        self.re_call_date = find("./ns:TICKET/ns:Ticket_Taken_DateTime")
        self.re_work_date = find("./ns:TICKET/ns:Work_Start_DateTime")
        self.re_legal_date = find("./ns:TICKET/ns:Legal_DateTime")
        self.re_respond_date = find("./ns:TICKET/ns:Response_Due_DateTime")
        self.re_legal_restake = find("./ns:TICKET/ns:RestakeDate")

        self.re_duration = find("./ns:TICKET/ns:Duration_Of_Work")
        self.re_work_description = find("./ns:TICKET/ns:Location_Of_Work")
        self.re_work_type = find("./ns:TICKET/ns:Type_Of_Work")
        self.re_company = find("./ns:TICKET/ns:Work_Done_For")
        self.re_explosives = find("./ns:TICKET/ns:Explosives")
        self.re_work_remarks = find("./ns:TICKET/ns:Remarks")
        self.re_update_of = find("./ns:TICKET/ns:Reference_Ticket_Number")

        self.re_con_name = find("./ns:TICKET/ns:Excavator/ns:Company")
        self.re_con_address = find("./ns:TICKET/ns:Excavator/ns:Address")
        self.re_con_city = find("./ns:TICKET/ns:Excavator/ns:City")
        self.re_con_state = find("./ns:TICKET/ns:Excavator/ns:State")
        self.re_con_zip = find("./ns:TICKET/ns:Excavator/ns:Zip")
        self.re_caller_contact = find("./ns:TICKET/ns:Excavator/ns:Contact/ns:Name")
        self.re_caller_phone = find("./ns:TICKET/ns:Excavator/ns:Contact/ns:Primary_Phone")
        self.re_caller_fax = find("./ns:TICKET/ns:Excavator/ns:Contact/ns:Fax")
        self.re_caller_email = find("./ns:TICKET/ns:Excavator/ns:Contact/ns:Email")

        self.re_work_state = find("./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:State")
        self.re_work_county = find("./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:County")
        self.re_work_city = find("./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:Place")
        self.re_work_address_number = find("./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:Street/ns:From_Address")
        self.re_work_address_number_2 = find("./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:Street/ns:To_Address")

        self.re_map_page = find("./ns:TICKET/ns:Dig_Site/ns:Grids/ns:Grid/ns:GridItem/ns:Value")

    def find_transmit_date(self):
        d = self.find("./ns:TICKET/ns:Transmission_DateTime") \
         or self.find("./ns:TICKET/ns:Ticket_Taken_DateTime")
        return d

    def find_work_address_street(self):
        src = "./ns:TICKET/ns:Dig_Site/ns:Address_Info/ns:Street/"
        elems = ["Direction_Suffix", "Street_Name", "Street_Type"]
        values = [self.find(src + "ns:" + elem) for elem in elems]
        return string.join(values, " ")

    def find_locates(self):
        #print self.root.find("./{http://tempuri.org/TicketDataSet.xsd}TICKET")
        elem1 = self.root.find("./ns:TICKET/ns:Primary_CDC/ns:CDC", self.NAMESPACES)
        elems = self.root.findall("./ns:TICKET/ns:Additional_CDCs/ns:Additional_CDC",
        self.NAMESPACES)
        if elem1 is not None: elems.insert(0, elem1)
        return [locate.Locate(elem.text) for elem in elems]

    def find_work_lat(self):
        e = self.find('./ns:TICKET/ns:Dig_Site/ns:Polygon/ns:Point')
        if e:
            parts = e.split(",")
            return float(parts[1])
    def find_work_long(self):
        e = self.find('./ns:TICKET/ns:Dig_Site/ns:Polygon/ns:Point')
        if e:
            parts = e.split(",")
            return float(parts[0])

    def post_process(self):
        xml_parser.XmlParser.post_process(self)

        if self.ticket.work_description:
            self.ticket.work_description = self.ticket.work_description.strip()

        # make sure dates are in the usual format
        for datefield in ["call_date", "transmit_date", "work_date",
                          "due_date", "legal_due_date", "legal_date",
                          "respond_date", "legal_restake"]:
            old = getattr(self.ticket, datefield)
            if old is not None:
                new = old.replace("/", "-")
                setattr(self.ticket, datefield, new)

class SummaryParser(xml_parser.XMLSummaryParser):
    # depends on self.root being set by self.parse()
    # alternatively, we could pass the root object as a parameter instead of
    # 'data'.

    # according to the audit sample, the root is <xmlAudit> containing
    # <AuditDataSet>. this differs from the sample tickets.
    # IDEA: just try to find <AuditDataSet> and set it as root?

    NAMESPACES = {'ns': "http://tempuri.org/AuditDataSet.xsd"}

    def has_header(self, data):
        return self.root.find('./ns:AuditDataSet/ns:AUDIT/ns:Audit_DateTime', self.NAMESPACES).text > ""

    def has_footer(self, data):
        return True # implied; is not available for XML tickets

    def get_client_code(self, data):
        return self.root.find('./ns:AuditDataSet/ns:AUDIT/ns:CDC', self.NAMESPACES).text

    def get_summary_date(self, data):
        ds = self.root.find('./ns:AuditDataSet/ns:AUDIT/ns:Audit_DateTime', self.NAMESPACES).text
        m, d, y = ds.split()[0].split("/")
        return "%s-%s-%s 00:00:00" % (y, m, d)

    def get_expected_tickets(self, data):
        return len(self.root.findall('./ns:AuditDataSet/ns:AUDIT/ns:Audit_Item',
        self.NAMESPACES))

    def read(self, data):
        details = []
        for node in self.root.findall('./ns:AuditDataSet/ns:AUDIT/ns:Audit_Item',
        self.NAMESPACES):
            seq_no = node.find('ns:Sequence_Number', self.NAMESPACES).text
            ticket_number = node.find('ns:Ticket_Number', self.NAMESPACES).text
            detail = summarydetail.SummaryDetail([seq_no, ticket_number,
                     '00:00', "", ""])
            details.append(detail)
        return details


