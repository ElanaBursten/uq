# Baltimore.py

import string
#
import dbinterface_ado
import Delaware1
import locate
import Lanham
from re_shortcut import R
import tools

class Parser(Delaware1.Parser):  # was: Lanham

    _APPEND_UPDATE = False # if True, append 'UPDATE' to ticket.kind

    re_caller = R("^Contact Name *: +(.*?) +(Con|Fax)")
    re_caller_contact = re_caller
    re_caller_altcontact = R("^Job Site Contact: *(.*?) +Job")
    re_work_city = R("^Place      : +(.*)$")
    re_con_state = R("^State      : ([A-Z][A-Z])")
    re_work_state = re_con_state
    re_revision = R("^Ticket No:.*Update No: (\S+)")

    #dbado = dbinterface_ado.DBInterfaceADO() # XXX FIXME >=(

    def find_locates(self):
        # get the lines we want, using an unorthodox filtering mechanism :-)
        lines = self.data.split("\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:") or li.startswith(" ")]

        locates = []
        for line in lines:
            wholeline = line
            line = line[:19]
            if line.startswith("Send"):
                line = line[len("Send To:"):]
            data = string.join(line.split(), "")

            # find seq_number
            parts = wholeline.split()
            if wholeline.startswith("  "):
                seq_number = wholeline[19:40].strip() or None   # ken dit?
            else:
                try:
                    idx = parts.index("Seq")
                except ValueError:
                    seq_number = None
                else:
                    seq_number = parts[idx+2]

            locates.append((data, seq_number))

        locates = [locate.Locate(name, seq_number=seq_number)
                   for (name, seq_number) in locates if name.strip()]

        for loc in locates:
            if loc.client_code.startswith("BGE") \
            and not loc.client_code.endswith("G"):
                newname = loc.client_code + "G"
                # only add this if it isn't there already
                if newname not in [loc2.client_code for loc2 in locates]:
                    locates.append(locate.Locate(newname))

        return locates

    def post_process(self):
        Delaware1.Parser.post_process(self)

        if not self.ticket.map_page and self.ticket.work_lat > 0 and \
         self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat, 
             self.ticket.work_long)
        
        if self.ticket.map_page:
            self.ticket.grids = [self.ticket.map_page]

        # add UPDATE to ticket.kind, but only for old-style tickets
        if self._APPEND_UPDATE:
            if self.ticket.update_of and 'UPDATE' not in self.ticket.kind \
            and 'Ticket Mapping' not in self.ticket.image:
                self.ticket.kind += ' UPDATE'

class SummaryParser(Lanham.SummaryParser):
    prepend_zeroes = 0  # as of 2004-07-22, this has changed

