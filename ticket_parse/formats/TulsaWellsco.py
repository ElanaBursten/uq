import Tulsa
import NewJersey
from re_shortcut import R
import string
import summarydetail
from date import Date

class Parser(Tulsa.Parser):
    pass
    # NOTE that this is just the same as Tulsa, but the summary parser is not!

class SummaryParser(NewJersey.SummaryParser):
    LENGTH = len("03031215261613 0089  ")
    rheader = R("^WELLSCO audit for CDC of")
    rfooter = R("^Total number of Messages for \S+ +\d+")
    rclient = R("WELLSCO audit for CDC of (.*) +on")
    rsummarydate = R("WELLSCO audit for.*on (.*)$")
    rexpected = R("Total number of Messages for \S* +(.*)$")

    # New Jersey summaries can be multipart; possibly TulsaWellsco summaries
    # can be as well.

    def get_summary_date(self, data):
        m = self.rsummarydate.search(data)
        sdate = m and m.group(1) or ""
        if sdate:
            sdate = "20%s-%s-%s 00:00:00" % (sdate[:2], sdate[2:4], sdate[4:])
            d = Date(sdate)
            d.dec() # very necessary
            #while d.isweekend():
            #    d.dec()
            sdate = d.isodate()
        return sdate

    def read(self, data):
        tickets = []
        for line in string.split(data, "\n"):
            line = string.strip(line)
            if line and line[0] in "0123456789":
                while line:
                    l2 = line[:self.LENGTH]
                    parts = string.split(l2)
                    if len(parts) == 2:
                        ticket_number, number = parts[:2]
                        detail = summarydetail.SummaryDetail(
                                 [number, ticket_number, "", "", ""])
                        tickets.append(detail)
                    # inspect rest of line
                    line = string.strip(line[self.LENGTH:])

        return tickets

    # XXX not sure if we need this for Dallas1 as well.
    def post_process(self, ts):
        NewJersey.SummaryParser.post_process(self, ts)

        # New Jersey has special rules for setting the summary_date_end
        # field...
        if ts.summary_date:
            d = Date(ts.summary_date)
            if d.isweekend():
                while d.isweekend():
                    d.dec() # return to last Friday
                ts.summary_date = d.isodate()
                d.inc(3)    # move to next Monday
                ts.summary_date_end = d.isodate()
            else:
                ts.summary_date_end = self._nextmorning(ts.summary_date)

        return ts
