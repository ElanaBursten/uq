# Oregon.py

from re_shortcut import R
import Lanham
import locate
import string
import tools

class Parser(Lanham.Parser):

    start_of_locates = "Additional Members"
    # marks the beginning of the locates section

    re_ticket_type = R("^Ticket No: *\d+ *(.*?)(   |$)")
    re_transmit_date = R("^Transmit *Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM|am|pm)")
    re_call_date = R("^Original Call Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM|am|pm)")
    re_work_date = R("^Work to Begin Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM|am|pm)")
    re_work_county = R("County: (.*?) *Place:")
    re_work_city = R("Place: (.*?) *$")
    re_map_page = R("Twp: (.*?) *Rng: (.*?) *Sect-Qtr: (.*?),? *$")
    re_con_name = R("^Company *: (.*?)( *Best Time|$)")
    re_caller_contact = R("^Contact Name: (.*?) *Phone")
    re_caller_phone = R("^Contact Name.*Phone: (.*?) *$")
    re_caller_altcontact = R("^Alt. Contact: (.*?) *Phone")
    re_caller_altphone = R("^Alt. Contact.*Phone: (.*?) *$")
    re_caller_email = R("^Cont. Email :\s*(\S+)")
    re_update_of = R("^Update Of:\s*(\S+)")

    def f_ticket_type(self, match):
        s = match.group(1)
        while s.startswith('='):
            s = s[1:]
        while s.endswith('='):
            s = s[:-1]
        return s

    def f_transmit_date(self, match):
        return tools.isodate(string.join(match.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def f_map_page(self, match):
        try:
            township, range, section = match.groups()
            section = section.split(",")[0]
            section = section.replace("-", "")
            return "%s%s%s" % (township, range, section)
        except (ValueError, IndexError):
            return string.join(match.groups())

    def find_locates(self):
        # copied from LanhamParser, with minor changes

        lines = self.data.split("\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:")]

        locates = []
        for line in lines:
            wholeline = line
            line = line[:19]
            if line.startswith("Send"):
                line = line[len("Send To:"):]
            data = string.join(string.split(line), "")

            # find seq_number
            parts = wholeline.split()
            if wholeline.startswith("  "):
                seq_number = wholeline[19:40].strip() or None   # ken dit?
            else:
                try:
                    idx = parts.index("Seq")
                except ValueError:
                    seq_number = None
                else:
                    seq_number = parts[idx+2]

            locates.append(locate.Locate(data, seq_number))

        # parse member area
        locates2 = self._parse_member_area()
        locates.extend(locates2)

        return locates

    _re_work_lat = R("Lat: (\d+\.\d+)")
    _re_work_long = R("Lon:\s*(-\d+\.\d+)")

    def find_work_long(self):
        """
        Find the work longitude
        """
        try:
            longNW, longSE = [float(item) for item in self._re_work_long.findall(self.data)]
            long = (longNW+longSE)/2.0
            if long < 0.0:
                return long
            else:
                return 0.0
        except:
            return 0.0

    def find_work_lat(self):
        """
        Find the work latitude
        """
        try:
            latNW, latSE = [float(item) for item in self._re_work_lat.findall(self.data)]
            lat = (latNW+latSE)/2.0
            if lat > 0.0:
                return lat
            else:
                return 0.0
        except:
            return 0.0

    def _parse_member_area(self):
        locates = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith(self.start_of_locates))
        for i in range(idx1+1, len(lines)):
            line = lines[i].strip()
            if not line:
                break
            parts = line.split()
            locates.extend([locate.Locate(p) for p in parts if p.strip()])
        return locates

class SummaryParser(Lanham.SummaryParser):
    prepend_zeroes = 0

