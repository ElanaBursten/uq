# WashingtonXML.py

import FairfaxXML
import VUPSNewFMW3

class Parser(FairfaxXML.Parser):
    def post_process(self):
        FairfaxXML.Parser.post_process(self)
        if self.ticket.update_of and 'UPDATE' not in self.ticket.kind:
            self.ticket.kind += ' UPDATE'

class SummaryParser(FairfaxXML.SummaryParser):
    pass
