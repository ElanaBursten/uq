# PA7501Meet.py

import re
#
import PA7501
from tools import R

class Parser(PA7501.Parser):
    re_ticket_number = R("Meeting Request Number--\[(.*?)\]")
    re_serial_number = re_ticket_number
    re_work_date = R("(?:Proposed Meeting Date and Time)--\[(.*?)\] \[(.*?)\]")
    re_work_type = R("Type of Work--\s*\[(.*?)\]", re.DOTALL)
    re_caller_email = R("Email--\[(.*?)\]")

class SummaryParser(PA7501.SummaryParser):
    pass

