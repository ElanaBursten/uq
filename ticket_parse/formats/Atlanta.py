# Atlanta.py

from parsers import BaseParser
#from re_shortcut import R
import string
import tools
import locate
import re

from summaryparsers import BaseSummaryParser
import summarydetail
import date

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | re.IGNORECASE | flags)

class Parser(BaseParser):

    only_one = ["ticket_number", "transmit_date", "call_date", "ticket_type",
                "work_type"]

    def __init__(self, *args, **kwargs):
        BaseParser.__init__(self, *args, **kwargs)
        self.check_max_locates = 0  # no check for Atlanta

    re_ticket_number = R("^Ticket : +(\S+) +")
    re_update_of = R("^Old Tkt: +(\S+)")
    re_call_date = R("^Ticket .*Date: ([0-9/]+) +Time: ([0-9:]+)")
    re_revision = R("Revision:\s*(\S+)\s*$")

    re_work_state = R("^State: ([A-Z][A-Z]) +")
    re_work_county = R("County: +(.*?) +Place")
    re_work_city = R("^State: .*Place: *(.*?) *$")

    re_work_address_number = R("^Addr +: From: +(.*?) +To:")
    re_work_address_number_2 = R("^Addr +:.*To: +(.*?) +Name")
    # TODO: Distinguish between From and To
    re_work_address_street = R("^Addr +:.*Name: +(.*)$")
    re_work_cross = R("^Cross: From: *(.*?) *To: *(.*?) *Name: *(.*)$")
    # XXX Cross may be split like this too

    re_work_subdivision = R("Subdivision: *(.*)$")

    re_work_type = R("^Work type: +(.*)$")
    re_work_date = R("^Work date: +([0-9/]+) +Time: +([0-9:]+) +")
    re_work_notc = R("notc: +(.*) +Priority")
    re_priority = R("Priority: +(.*)$")

    re_legal_date = R("^Legal day: +([0-9/]+) +Time: +([0-9:]+) +Good")
    re_legal_good_thru = R("Good thru: +([0-9/]+) +Restake")
    re_legal_restake = R("Restake by: +([0-9/]+)$")

    re_respond_date = R("^RespondBy: +([0-9/]+) +Time: +([0-9:]+) Duration")
    re_duration = R("Duration : *(.*)$")
    re_company = R("^Done for : +(.*?) *$")

    re_con_name = R("^Company : *(.*?) *Type")
    re_con_type = R("Company :.*Type: *(.*?) *$")
    re_con_address = R("^Co addr : *(.*?) *$")
    re_con_city = R("^City +: +(.*?) *State")
    re_con_state = R("State +: (..) Zip")
    re_con_zip = R("Zip: (\d\d\d\d\d)$")

    re_caller = R("^Caller +: (.*?) *Phone")
    re_caller_phone = R("Phone +: *(.*?) *$")
    re_caller_fax = R("^Fax +: *(.*?) *Alt")
    re_caller_altphone = R("Alt. Ph.: *(.*?) *$")
    re_caller_email = R("Email +: +(.*?) *$")
    re_caller_cellular = R("Cellular: +(.*?)( +Fax|$)")
    re_caller_contact = R("^Contact : +(.*?)\s*(Phone|Cellular|Fax|$)")

    re_operator = R("Oper: (.*) +Chan")
    re_channel = R("Chan: *(.*)$")

    #re_transmit_date = R("^Submitted date: ([0-9/]+) Time: ([0-9:]+)")
    re_transmit_date = R("GAUPC ([0-9/]+) ([0-9:]+) \d")
    re_ticket_type = R("GAUPC .*? [0-9-]+ (.*)$")

    # this attribute requires more than m.group(1), so we define a function
    # to handle this:
    f_call_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))
    f_transmit_date = f_work_date = f_legal_date = f_respond_date = f_call_date

    f_legal_good_thru = lambda s, m: s.isodate(m.group(1))
    f_legal_restake = f_legal_good_thru

    f_work_cross = lambda s, m: \
     tools.singlespaced(string.join(
      [m.group(1), m.group(2), m.group(3)], " "))
     # may need reformatting

    f_work_address_street = lambda s, m: tools.singlespaced(m.group(1))

    # special functions can be written for attributes that cannot be
    # (easily) found with regular expressions:
    def find_locates(self):
        lines = self.data.split("\n")
        locates = ""
        for line in lines:
            line = line.strip()
            if line.upper().startswith("MBRS :"):
                locates = line[7:].strip()
                continue
            if locates:
                if line.startswith(": "):
                    locates = locates + line[1:].rstrip()
                else:
                    break
        return map(locate.Locate, locates.split())

    def find_work_remarks(self):
        lines = self.data.split("\n")
        remarks = ""
        for line in lines:
            line = line.strip()
            if line.upper().startswith("REMARKS :"):
                remarks = line[10:].strip()
                continue
            if remarks:
                if line.startswith(": "):
                    remarks = remarks + line[1:].rstrip()
                else:
                    break
        return remarks
    # I sense a general pattern here...

    def find_work_description(self):
        lines = self.data.split("\n")
        loctype = ""
        for line in lines:
            line = line.strip()
            u = line.upper()
            if u.startswith("LOCAT:") or u.startswith('LOCATE:'):
                loctype = line[7:].strip()
                continue
            if loctype:
                if line.startswith(": "):
                    loctype = loctype + line[1:].rstrip()
                else:
                    break
        return loctype

    def find_kind(self):
        # At this point, priority should already be known
        if self.ticket.priority == "1":
            return "EMERGENCY"
        else:
            return "NORMAL"

    def _validate_caller_fax(self):
        # a fax number must have at least 10 digits [#3379]
        if self.ticket.caller_fax:
            num_digits = len([1 for c in self.ticket.caller_fax 
                              if c in '0123456789'])
            if num_digits < 10:
                self.ticket.caller_fax = None

    def post_process(self):
        if not self.ticket.work_state:
            self.ticket.work_state = 'GA'
        self._validate_caller_fax()
        BaseParser.post_process(self)

    ###
    ### Finding longitudes/latitudes in grids

    def find_work_lat(self):
        return self._compute_longlats()[0]

    def find_work_long(self):
        return self._compute_longlats()[1]

class SummaryParser(BaseSummaryParser):
    LENGTH = len("00002 01302-106-039-001 00:09 L")
    RECOG_HEADER_2 = "UTILITIES PROTECTION CENTER SIGNING OFF"

    re_footer = R("^\s+TOTAL +- (\d+) *$")
    re_recog_header_1 = R("\s+\*SUM\* GAUPC")
    re_recog_data = R("^\d{5}\s\S+\s\d{2}:\d{2}")

    def has_header(self, data):
        #return string.find(data, self.RECOG_HEADER_2) > -1
        return self.RECOG_HEADER_2 in data

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    '''
    def get_summary_date(self, data):
        date = ""
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = string.split(line)
                date1 = parts[3] # + " " + parts[4]
                date = tools.isodate(date1)
                date = (Date(date) - 1).isodate()
                break
        return date
    '''
    def get_summary_date(self, data):
        d = ""
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = line.split()
                d = parts[3] + " " + parts[4]
                d = tools.isodate(d)
                d = date.Date(d)
                # if the summary date on the audit is after midnight but before
                # 5 AM, assume it is for the previous day
                if d.hours < 5 \
                  and (d.hours > 0 or d.minutes > 0 or d.seconds > 0):
                    d.dec()
                d.resettime()
                return d.isodate()

        # as a last resort, return current date/time
        return date.Date()

    def get_client_code(self, data):
        client = ""
        lines = data.split("\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = line.split()
                client = parts[0]
                break
        return client

    def read(self, data):
        tickets = []
        for line in data.split("\n"):
            line = line.strip()
            if line:
                m = self.re_recog_data.search(line)
                if m:
                    while line:
                        l2 = line[:self.LENGTH]
                        parts = l2.split()
                        number, ticket_number, time = parts[:3] # ignore token
                        # get token as well
                        if len(parts) == 4:
                            token = parts[3]
                        else:
                            token = ""
                        # split ticket number and revision
                        (ticket_number, revision) = (ticket_number[:-4],
                         ticket_number[-3:])
                        detail = summarydetail.SummaryDetail()
                        detail.setdata([number, ticket_number, time, revision,
                         token])
                        tickets.append(detail)
                        # inspect rest of line
                        line = line[self.LENGTH:].strip()

        return tickets

