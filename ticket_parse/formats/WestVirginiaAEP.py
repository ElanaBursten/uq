import WestVirginia
import HarlingenTESS
from re_shortcut import R

class Parser(WestVirginia.Parser):
    CALL_CENTER = 'FWP4' # see post_process

class SummaryParser(HarlingenTESS.SummaryParser):
    re_ticket = R("(\*?)(\d+) WVMISS(\d+)-(\d+) (\d\d:\d\d)")
    # note: we don't want "WVMISS" as part of the number!
    DEFAULT_CLIENT_CODE = 'WVMISS'
