import BatonRouge1

class Parser(BatonRouge1.Parser):
    pass

import NewJersey3

class SummaryParser(NewJersey3.SummaryParser):

    def get_client_code(self, data):
        # there is no client code on the ticket; use a dummy value
        return "FBL2"
