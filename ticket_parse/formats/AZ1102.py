# AZ1102.py

import string
#
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import Arizona
import summarydetail
import tools

class Parser(Arizona.Parser):
    pass

class SummaryParser(BaseSummaryParser):
    LENGTH = len("00002 01302-106-039-001 00:09 L")
    RECOG_HEADER_2 = "DESTINATION AUDIT FOR"

    re_no_tickets = R("^\<No Tickets\>")
    re_footer = R("^Total Tickets: (\d+)")
    re_recog_header_1 = R("\s+\*SUM\* GAUPC")
    re_client_code = R("DESTINATION AUDIT FOR (\S+)")
    re_date = R("LIST OF ALL TICKETS SENT BETWEEN "\
                "\S+ \S+ \S+ AND (\d\d\\/\d\d\/\d\d\d\d) \S+ \S+")

    def has_header(self, data):
        return string.find(data, self.RECOG_HEADER_2) > -1

    def has_footer(self, data):
        return bool(self.re_footer.search(data)
                 or self.re_no_tickets.search(data))

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            a = m.group(1)
            month, day, year = map(int, a.split("/"))
            return "%04d-%02d-%02d 00:00:00" % (year, month, day)
        else:
            return Date().isodate()

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        if m:
            return m.group(1)
        else:
            return "unknown"

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("----------+----"))

        if idx1 == -1: # no tickets
            return []

        for i, line in enumerate(lines):
            if i <= idx1:
                continue
            if not line.strip():
                break # end of block
            parts = line.split("|")
            seq_number = parts[0].strip()
            if not seq_number.strip():
                continue # empty line
            ticket_number, revision = parts[1].strip().split(".", 1)
            type_code = parts[2].strip()
            detail = summarydetail.SummaryDetail(
                     [seq_number, ticket_number, "00:00", revision, type_code])
            tickets.append(detail)

        return tickets

