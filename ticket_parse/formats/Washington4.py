# Washington4.py

import Washington
from summaryparsers import BaseSummaryParser
from re_shortcut import R
import re
import summarydetail
from date import Date

class Parser(Washington.Parser):
    pass # update call center

class SummaryParser(BaseSummaryParser):
    # This should be the basis for all new "ClickScreener" audits.

    REC_HEADER = "ClickScreener EOD"
    REC_FOOTER = "Total Tickets"
    DUMMY_CLIENT_CODE = "FMW4"

    re_date = R(r"for (\d+\/\d+\/\d\d\d\d)")
    re_expected = R(r"([0-9,]+)\s+Total Tickets") # may contain comma
    re_client_code = R(r"^(\S+)\s+.*?ClickScreener")

    def __init__(self, *args, **kwargs):
        BaseSummaryParser.__init__(self, *args, **kwargs)
        self._skip_tickets = 0

    def has_header(self, data):
        return self.REC_HEADER in data

    def has_footer(self, data):
        return self.REC_FOOTER in data

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        if m:
            return m.group(1)
        else:
            return self.DUMMY_CLIENT_CODE

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            date_raw = m.group(1)
            m, d, y = map(int, date_raw.split('/'))
            date_long = "%04d-%02d-%02d" % (y, m, d)
            return Date(date_long).isodate()
        else:
            raise ValueError("Could not extract audit date")

    def get_expected_tickets(self, data):
        m = self.re_expected.search(data)
        if m:
            n = int(m.group(1).replace(",", ""))
            return n - self._skip_tickets
        else:
            raise ValueError("Could not extract expected number of tickets")

    def read(self, data):
        tickets = []
        ok = 0

        for line in data.split('\n'):
            if ok == 0 and line.startswith('==='):
                ok = 1

            elif ok == 1 and re.search(r"^\d+", line):
                # it's a line with ticket info
                #parts = line.split()
                ticket_number = line[0:13].strip()
                revision = line[15:18].strip()
                item_number = line[20:26].strip()
                type = line[31:54].strip()[:20] # max. length of field
                atime = line[107:112].strip() or "00:00"
                sent = line[56:94].strip()

                # we ignore tickets that are not UQ's
                if 'utiliquest' not in sent.lower():
                    # decrease number of expected tickets
                    self._skip_tickets += 1
                    continue # don't keep this line

                detail = summarydetail.SummaryDetail()
                detail.setdata([item_number, ticket_number, atime, revision, type])
                tickets.append(detail)

            elif ok == 1 and line.startswith('==='):
                break

        return tickets


