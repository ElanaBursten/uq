# SouthCaliforniaSDGKorterra.py

import string
import locate
import date
import tools
import SouthCalifornia
import Delaware3
from re_shortcut import R
import summarydetail

class Parser(SouthCalifornia.Parser):
    call_center = 'SCA7'
    re_transmit_date = R("KORTERRA JOB \S+ \w+ Seq: \d+ ([0-9/]+) ([0-9:]+)")
    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))

    def find_locates(self):
        _re_locate = R("KORTERRA JOB (\w+?)-")
        m = _re_locate.search(self.data)
        locates = []
        if m:
            locates.extend([m.group(1)])
        return map(locate.Locate, locates)

class SummaryParser(Delaware3.SummaryParser):
    r_client_code = R("KORTERRA AUDIT (\w+)-")

    def get_client_code(self, data):
        m = self.r_client_code.search(data)
        if m:
            return m.group(1)
        else:
            return ""

    def read(self, data):
        tickets = []
        collecting = 0
        lines = data.split("\n")
        for line in lines:
            if line.startswith("----") and not collecting:
                collecting = 1 # begin collecting
            elif (not line.strip()) and collecting:
                collecting = 0 # end collecting
            elif line.startswith("No Tickets") and collecting:
                collecting = 0 # end collecting
            elif collecting:
                try:
                    ticket_number, seq_number = line.split()[:2]
                except (IndexError, ValueError):
                    continue
                detail = summarydetail.SummaryDetail([
                         seq_number, ticket_number, "", "", ""])
                tickets.append(detail)

        return tickets
