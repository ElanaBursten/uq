# WV1181Korterra.py

import re
import string
#
import WV1181, NewJersey2010B
import parsers
from tools import R
import tools
import locate

class Parser(parsers.BaseParser):

    re_ticket_number = R("^Ticket Number:\s*(\S+)")
    re_channel = R("^Source:[ \t]+(\S+)")
    re_work_notc = R("Hours Notice:[ \t]+(\d+)")
    re_ticket_type = R("^Type:\s*(.+)\s*Date:")
    re_call_date = R("^Type:\s*.*\s*Date:\s*(\S+)\s+(\S+)")
    re_transmit_date = R("KORTERRA JOB.*Seq: \S+ (\S+)\s+(\S+)")
    re_serial_number = R("KORTERRA JOB UTILIQUEST\S+\s+(\S+)")

    #re_map_ref = R("^Map Reference:[ \t]+(\S.+\S)\s*Company")
    re_map_ref = R("^Map Reference:[ \t]+(.*?)\s*Company")
    # Company section
    re_con_name = R("(\S.*?)\s+Type:\s+\S+\s*?$")
    re_con_address = R("(^.*?)\s+Company Phone:")
    re_con_city = R("(^\S.+?),\s+?\S+?\s+?\d+?\s+Company Fax:")
    re_con_state = R("^\S.+?,\s+?(\S+?)\s+?\d+?\s+Company Fax:")
    re_con_zip = R("^\S.+?,\s+?\S+?\s+?(\d+?)\s+Company Fax:")

    re_work_state = R("^State:\s*(\S.*?)\s*Work Date:")
    re_work_county = R("^County:\s+(.*?)\s+Done For:")
    re_work_address_street = R("^Street:\s+(.*?)\s*$")
    re_work_date = R("Work Date:\s*(\S+)\s+(\S+)")
    re_work_type = R("^Nature of Work:[ \t]+(\S.*\S)")
    re_company = R("Done For:\s*(\S.*\S)")
    re_work_remarks = R("^Remarks.*-+(.*?).*Members")
    re_work_cross = R("^Intersection:[ \t]*(.*?)\s*$")
    re_map_page = R("((MAPSCO|KEYMAP) \d+[A-Z]*,[A-Z0-9]+)")

    re_caller = R("Caller Name:[ \t]*(.*?)\s*(Caller Phone|$)")
    re_caller_contact = R("Contact Name:[ \t]*(.*?)\s*(Contact Phone|$)")
    re_caller_email = R("^Contact Email:\s*(\S+)\s*$")
    re_caller_fax = R("Company Fax:\s+(.*?)\s*$")
    re_caller_phone = R("Caller Phone:\s+(\S+ \S+)")
    re_work_city = R("^Place:\s+(.*?)\s*Depth:")
    re_explosives = R("Explosives:\s*(\S+)")

    def f_work_date(self, match):
        month, day, year = [int(item) for item in match.group(1).split("/")]
        if year < 100:
            year += 2000
        hour, minute = [int(item) for item in match.group(2).split(":")][:2]
        return "%02d-%02d-%02d %02d:%02d:%02d" % (year, month,day, hour,
                                                  minute, 0)

    f_call_date = f_transmit_date = f_work_date

    _re_work_lat1 = R("^Latitude:\s*(\S+)")
    _re_work_lat2 = R("^Second Latitude:\s*(\S+)")
    def find_work_lat(self):
        m1 = self._re_work_lat1.search(self.data)
        m2 = self._re_work_lat2.search(self.data)
        lat1 = lat2 = 0.0
        if m1:
            lat1 = float(m1.group(1))
        if m2:
            lat2 = float(m2.group(1))
        else:
            lat2 = lat1
        return (lat1+lat2)/2.0

    _re_work_long1 = R("^Latitude:\s*\S+\s*Longitude:\s*(\S+)")
    _re_work_long2 = R("^Second Latitude:\s*\S+\s*Second Longitude:\s*(\S+)")
    def find_work_long(self):
        m1 = self._re_work_long1.search(self.data)
        m2 = self._re_work_long2.search(self.data)
        long1 = long2 = 0.0
        if m1:
            long1 = float(m1.group(1))
        if m2:
            long2 = float(m2.group(1))
        else:
            long2 = long1
        return (long1+long2)/2.0

    def find_locates(self):
        lines = self.data.split('\n')
        idx = tools.findfirst(lines, lambda s: s.startswith('Code'))
        if idx > -1:
            locates = []
            idx += 2
            while True:
                line = lines[idx]
                if line.strip():
                    locates.append(line.split()[0])
                    idx += 1
                else:
                    break
        return [locate.Locate(z) for z in locates]


    def find_work_remarks(self):
        pieces = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines,
               lambda s: s.startswith("Remarks"))
        if idx1 > -1:
            for i in range(idx1+2, len(lines)):
                line = lines[i]
                if not line.strip():
                    break
                if line.startswith("Members"):
                    break
                pieces.append(line.strip())
        return string.join(pieces, "\n")

    def post_process(self):
        #lines = self.data.split('\n')
        #idx = tools.findfirst(lines, lambda s: s.startswith("Company Informat"))
        #if idx > -1:
        #    self.ticket.con_address = lines[idx+3].strip()

        # remove leading 'WV'
        while self.ticket.serial_number \
        and self.ticket.serial_number[0] in string.uppercase:
            self.ticket.serial_number = self.ticket.serial_number[1:]

        parsers.BaseParser.post_process(self)


class SummaryParser(NewJersey2010B.SummaryParser):
    # Korterra audit. Audit report must check for tickets with PE or PEB
    # clients.
    def get_client_code(self, data):
        return "1181" # dummy
    def read(self, data):
        # 2969: ticket numbers on audit have suffix PE/PEB; this needs to be
        # removed for the ticket to be recognized in the audit report
        tickets = NewJersey2010B.SummaryParser.read(self, data)
        for t in tickets:
            t.ticket_number = t.ticket_number.rstrip(string.ascii_uppercase)
        return tickets

