# SC1444.py

import locate
import re
#
import SC1421
from tools import R

class Parser(SC1421.Parser):
    re_ticket_type = R("^Type:\s*(.*?)\s*$")
    re_con_name = R("^Name:\s+(.*?)\s*$")
    re_caller_contact = R("^Contact:\s*(.*?)\s*Email:")
    _re_locates = R("^Code Member(.*?)Color Code", re.DOTALL|re.MULTILINE)

    def find_locates(self):
        m = self._re_locates.search(self.data)
        if not m:
            return []
        parts = m.group(1).strip().split('\n')
        return [locate.Locate(x.split()[0]) for x in parts]

    find_transmit_date = SC1421.Parser.find_call_date

class SummaryParser(SC1421.SummaryParser):
    pass


