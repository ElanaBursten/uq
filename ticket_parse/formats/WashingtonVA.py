# WashingtonVA.py

import Richmond3
from re_shortcut import R

class Parser(Richmond3.Parser):
    PARSE_GRIDS = False
    re_service_area_code = R("VUPS\d+\s+(.*?)\s*\d\d\d\d/\d\d/\d\d")
    re_serial_number = R("(VUPS\d+)")
    def post_process(self):
        Richmond3.Parser.post_process(self)
        if self.ticket.update_of and 'UPDATE' not in self.ticket.kind:
            self.ticket.kind += ' UPDATE'

class SummaryParser(Richmond3.SummaryParser):
    pass
