# NorthCaliforniaATT.py

import NorthCalifornia
import SouthCaliforniaATT
import summarydetail
from re_shortcut import R
import locate

class Parser(NorthCalifornia.Parser):
    # like NorthCalifornia, but lines start with a space, among other things

    SPECIAL_LOCATES = []
    re_caller_phone = R("^ *Business Tel: +(.*?) *Fax:")
    re_caller_email = R("^ *Email Address: +(\S+)")
    re_caller_fax = R("^ *Business Tel.*Fax: +(.*?) *$")
    re_company = R("^ *Done for: +(.*?) *Explosives:")
    re_caller_phone = R("^ *Business Tel:\s+(\S+)")
    re_caller_fax = R("^ *Business Tel.*Fax: +(\S+)")
    re_serial_number = R("^ *At&t Ticket Id: (\S+)")
    re_revision = R("OCC Tkt No:\s*\d+-(\S+)")

    _re_plat = R("clli code:\s+(\S+)")
    _re_hp_section = R("(PACBEL:\s*.*?)\s*$")

    def find_locates(self):
        # use the term id, not the members section
        term_id = self._find_termid()
        return [locate.Locate(term_id)]

    def post_process(self):
        # add plat...
        m = self._re_plat.search(self.data)
        if m:
            for loc in self.ticket.locates:
                loc._plat = m.group(1)

        # add HP section, if any
        m = self._re_hp_section.search(self.data)
        if m:
            hp_info = m.group(1).strip()[:150]
            self.ticket._hp_info.append(hp_info)

        NorthCalifornia.Parser.post_process(self)


class SummaryParser(SouthCaliforniaATT.SummaryParser):

    def get_client_code(self, data):
        return "NCA2"

    def read(self, data):
        re_line = R("^(\d\d\d\d\d)\s+([A-Z0-9-]+)\s+(\S+)\s+([0-9:]*)")
        # groups: 1: seq number, 2: ticket number; 3: client code; 4: time
        # (time is optional as of 2010-03-18)
        tickets = []
        lines = data.split("\n")
        for line in lines:
            m = re_line.search(line)
            if m:
                spam = [m.group(1), m.group(2), m.group(4), ""]
                t = summarydetail.SummaryDetail(spam)
                tickets.append(t)

        return tickets

