# SanDiego.py
import string
import Atlanta
from re_shortcut import R

class SummaryParser(Atlanta.SummaryParser):
    """ Another summary parser that derives from the mighty Atlanta format. """
    LENGTH = len("00001 A0010229-00A 07:37   ")
    RECOG_HEADER_2 = "THIS IS YOUR DAILY AUDIT FOR"

    re_recog_header_1 = R("\s+\*(SUM|EOD)\*[A-Z]? USAS")

    def get_summary_date(self, data):
        date = Atlanta.SummaryParser.get_summary_date(self, data)
        re_date = R("THIS IS YOUR DAILY AUDIT FOR (\d\d/\d\d/\d\d)\.")
        m = re_date.search(data)
        if m:
            raw_date = m.group(1)
            m, d, y = string.split(raw_date, "/")
            date = "20%s-%s-%s 00:00:00" % (y, m, d)
        return date
