# FairfaxXML.py

import date
import string
#
import locate
import summarydetail
import tools
import xml_parser

class Parser(xml_parser.XmlParser):

    PARSE_GRIDS = False

    def __init__(self, *args, **kwargs):
        xml_parser.XmlParser.__init__(self, *args, **kwargs)

        # helper function
        def find(path, *args, **kwargs):
            return xml_parser.Finder(self.root, path, *args, **kwargs)

        self.re_transmit_date = find("./delivery/transmitted")
        self.re_ticket_number = find("./delivery/ticket")
        self.re_work_state = find("./tickets/state")
        self.re_call_date = find("./tickets/started")
        self.re_work_date = find("./tickets/replace_by_date")
        self.re_work_county = find("./tickets/county")
        self.re_work_city = find("./tickets/place")
        self.re_work_type = find("./tickets/work_type")
        self.re_work_cross = find("./tickets/cross1")
        self.re_work_address_number = find("./tickets/st_from_address")
        self.re_work_address_street = find("./tickets/street")

        self.re_company = find("./tickets/done_for")
        self.re_con_name = find("./tickets/name")
        self.re_con_address = find("./tickets/address1")
        self.re_con_city = find("./tickets/city")
        self.re_con_state = find("./tickets/cstate")
        self.re_con_zip = find("./tickets/zip")

        self.re_caller_phone = find("./tickets/caller_phone")
        self.re_caller_contact = find("./tickets/caller")
        self.re_caller_fax = find("./tickets/fax")
        self.re_caller_email = find("./tickets/email")
        self.re_explosives = find("./tickets/blasting")
        self.re_map_ref = find("./tickets/reference")

        self.re_work_remarks = find("./tickets/remarks")
        self.re_work_description = find("./tickets/location")

        self.re_work_lat = find("./tickets/centroid/coordinate/latitude")
        self.re_work_long = find("./tickets/centroid/coordinate/longitude")

        # although there is a <map_reference> field, we use the first grid
        self.re_map_page = find("./tickets/gridlist/grid")

        # Mantis #2739: parse due date
        self.re_due_date = find("./tickets/response_due")
        self.re_legal_due_date = self.re_due_date

        # Find the version number
        self.re_revision = find("./tickets/revision")



    # convert lat/long to floats
    def f_work_lat(self, match):
        return match and float(match.group(1))
    f_work_long = f_work_lat

    def find_ticket_type(self):
        elems = ["priority", "type", "lookup", "category"]
        values = [self.find("./tickets/" + elem) for elem in elems]
        if (self.find("./tickets/meet") == 'Y'
         or self.find("./tickets/derived_type") == "MEETING"):
            values.insert(0, "MEET")
        return string.join(values, " ")

    def find_grids(self):
        if not self.PARSE_GRIDS: return []
        elems = self.root.findall("./tickets/gridlist/grid")
        grids = [elem.text for elem in elems]
        for idx, grid in enumerate(grids):
            if '-' in grid:
                i = grid.find('-')
                grids[idx] = grid[:i]
        return grids

    def find_locates(self):
        elems = self.root.findall("./tickets/memberlist/memberitem/member")
        return [locate.Locate(elem.text) for elem in elems]

    def post_process(self):
        xml_parser.XmlParser.post_process(self)

        # the regular find_kind() is executed before find_ticket_type() and
        # therefore doesn't return the right result; so we call it again
        # afterwards. (this is kludgy, but it only happens in the rare case
        # that a call center has a find_ticket_type() method.
        self.ticket.kind = self.find_kind()

        # if the 'replace_by_date' field isn't there, use 'response_due'
        # (apparently only for emergencies)
        if not self.ticket.work_date:
            f = xml_parser.Finder(self.root, './tickets/response_due')
            m = f.search(self.data)
            if m:
                self.ticket.work_date = m.group(1)

        # make sure dates are in the usual format
        for datefield in ["call_date", "transmit_date", "work_date",
                          "due_date", "legal_due_date"]:
            old = getattr(self.ticket, datefield)
            if old is not None:
                new = old.replace("T", " ")
                setattr(self.ticket, datefield, new)

class SummaryParser(xml_parser.XMLSummaryParser):
    # depends on self.root being set by self.parse()
    # alternatively, we could pass the root object as a parameter instead of
    # 'data'.

    def has_header(self, data):
        return self.root.find('./delivery/center').text > ""

    def has_footer(self, data):
        return True # implied; is not available for XML tickets

    def get_client_code(self, data):
        return self.root.find('./delivery/recipient').text

    def get_summary_date(self, data):
        ds = self.root.find('./delivery/transmitted').text
        d = date.Date(ds) # already in ISO format ^_^

        # if the summary date on the audit is after midnight but before
        # 10 AM, assume it is for the previous day
        # 10 AM due to FLPS using GMT time as transmitted date
        if d.hours < 10 and (d.hours > 0 or d.minutes > 0 or d.seconds > 0):
            d.dec()
        d.resettime()
        return d.isodate()

    def get_expected_tickets(self, data):
        return len(self.root.findall('./audit'))

    def read(self, data):
        details = []
        for node in self.root.findall('./audit'):
            seq_no = node.find('seq_num').text
            ticket_number = node.find('ticket').text
            time = node.find('delivered').text[-5:]
            revision = node.find('revision').text
            detail = summarydetail.SummaryDetail([seq_no, ticket_number,
                     time, revision, ""])
            details.append(detail)
        return details

