# Richmond3.py

from parsers import BaseParser, ParserError
from re_shortcut import R
import locate
import re
import string
import tools

import Atlanta

class Parser(BaseParser):
    PARSE_GRIDS = True

    def __init__(self, *args, **kwargs):
        BaseParser.__init__(self, *args, **kwargs)
        self.check_max_locates = 0

    re_transmit_date = R("\d+ VUPS[a-z] ([0-9/]+) ([0-9:]+)")
    re_ticket_type = R("\d+ VUPS[a-z] [0-9/]+ [0-9:]+ \S+ *(.*) *$")
    re_ticket_number = R("^Ticket : ([A-Z]?\d+)")
    re_revision = R("Rev: (\S+)")
    re_call_date = R("^Ticket.*?Taken: ([0-9/]+) ([0-9:]+)(AM|PM)")

    re_work_state = R("^State: (..)")
    re_work_county = R("^State.*?Cnty: *(.*?) *Place")
    re_work_city = R("^State.*Place: *(.*?) *$")

    re_work_address_number = R("^Address : *(.*?) *Street")
    re_work_address_street = R("^Address.*?Street: (.*?) *$")
    re_work_cross = R("^Cross 1 : (.*?) *Intersection")
    re_work_type = R("^WorkType: (.*?) *$")
    re_company = R("^Done for: (.*?) *$")
    re_explosives = R("^Lot side.*Blasting:\s+(\S+)")

    re_work_date = R("^Work date: ([0-9/]+) ([0-9:]+)(AM|PM)")
    re_due_date = R("Due by +: ([0-9/]+) ([0-9:]+)(AM|PM)")

    re_work_lat = R("^Grids: (\S+)")
    re_work_long = re_work_lat
    re_map_page = R("^Grids: (\S+)")

    re_con_name = R("^Company : (.*?) *Type")
    re_con_address = R("^Co addr : (.*?) *Phone:")
    #re_con_phone = R("^Co addr.*?Phone: (.*?) *$")
    re_con_city = R("^City +: (.*?) *State")
    re_con_state = R("^City.*?State: (..)")
    re_con_zip = R("^City.*?Zip: (.*?) *$")
    re_caller = R("^Caller +: (.*?) *Phone")
    re_caller_phone = R("^Caller.*?Phone: (.*?) *$")
    re_caller_email = R("Email: (.*?) *$")
    re_caller_contact = R("^Field contact : (.*?) *Phone")
    re_caller_altphone = R("^Field contact.*?Phone: (.*?) *$")
    re_caller_fax = R("^Fax.*: (.*?) *?$")

    f_transmit_date = lambda s, m: \
     tools.isodate(m.group(1) + " " + m.group(2))
    f_call_date = lambda s, m: \
     tools.isodate(m.group(1) + " " + m.group(2) + " " + m.group(3))
    f_work_date = f_call_date
    f_due_date = f_call_date

    def f_work_lat(self, match):
        if match:
            s = match.group(1)
            if "-" in s:
                idx = s.find("-")
                s = s[:idx]
            try:
                return tools.grid2decimal(s)[0]
            except AssertionError:
                return 0

    def f_work_long(self, match):
        if match:
            s = match.group(1)
            if "-" in s:
                idx = s.find("-")
                s = s[:idx]
            try:
                return -(tools.grid2decimal(s)[1])
            except AssertionError:
                return 0

    def _find_something(self, linestart):
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.startswith(linestart))
        if idx > -1:
            lst = [lines[idx][len(linestart):].strip()]
            for i in range(idx+1, len(lines)):
                line = lines[i].strip()
                if line.startswith(":"):
                    lst.append(line[2:])
                else:
                    break
            return string.join(lst, " ")
        else:
            return ""

    def find_work_description(self):
        return self._find_something("Instruct:")

    def find_work_remarks(self):
        return self._find_something("Location:")

    def find_locates(self):
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.startswith("Members:"))
        locates = []
        if idx:
            for i in range(idx+1, len(lines)):
                line = lines[i]
                if not line.strip():
                    break   # stop at empty line
                while line:
                    part, line = line[:38], line[38:] # get first 40 characters
                    parts = part.split("=")
                    loc = parts[0].strip()
                    locates.append(locate.Locate(loc))

        return locates

    def find_grids(self):
        if not self.PARSE_GRIDS:
            return []

        grids = []
        lines = self.data.split('\n')
        for line in lines:
            if line.startswith("Grids: "):
                parts = line.split()[1:]
                for part in parts:
                    idx = part.find('-')
                    if idx >= 0:
                        part = part[:idx]
                    grids.append(part)

        return grids

    def sanitycheck_before(self):
        BaseParser.sanitycheck_before(self)
        re_header = "\s+VUPS[a-z]\s+"
        occ = re.findall(re_header, self.data)
        if len(occ) > 1:
            raise ParserError, \
                  "Multiple VUPSa occurrences -- ticket probably invalid"

class SummaryParser(Atlanta.SummaryParser):
    LENGTH = len("03268 A217200071-00A 08:42   ")
    RECOG_HEADER_2 = "THIS IS YOUR END OF DAY TICKET SUMMARY"

    re_footer = R("^Total: *(\d+) *$")
    re_footer_2 = R("^\s+TOTAL\s+-\s+(\d+)")
    re_date = R("END OF DAY TICKET SUMMARY FOR ([0-9/]+)")
    re_recog_header_1 = R("\s+\*(SUM|EOD)\*\s+VUPS[a-z]\s+")

    # get_client_code is derived from AtlantaSummaryParser

    def get_summary_date(self, data):
        sdate = Atlanta.SummaryParser.get_summary_date(self, data)
        m = self.re_date.search(data)
        if m:
            sdate = tools.isodate(m.group(1))
        return sdate

    def get_expected_tickets(self, data):
        m1 = self.re_footer.search(data)
        if m1:
            xp = m1.group(1)
            expected = xp and int(xp) or 0
        else:
            m2 = self.re_footer_2.search(data)
            if m2:
                xp = m2.group(1)
                expected = xp and int(xp) or 0
        return expected

    def has_footer(self, data):
        m1 = self.re_footer.search(data)
        m2 = self.re_footer_2.search(data)
        return not not (m1 or m2)
