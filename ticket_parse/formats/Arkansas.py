# Arkansas.py

import string
import re
#
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import summarydetail
import Tennessee
import tools

class Parser(Tennessee.Parser):
    re_transmit_date = R("\d+ +AROCS +([0-9/]+) +([0-9:]+)")

class SummaryParser(BaseSummaryParser):
    def has_header(self, data):
        return string.find(data, "From AROCS") > -1 \
            or string.find(data, "FROM AROCS") > -1

    def has_footer(self, data):
        return string.find(data, "Total for") > -1 \
            or string.find(data, "TOTAL FOR") > -1

    def get_client_code(self, data):
        m = R("^For Code (.*) *$", re.IGNORECASE).search(data)
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = R("^Audit for ([0-9/]+)", re.IGNORECASE).search(data)
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = R("^Total for [A-Z0-9]*: (\d+)", re.IGNORECASE).search(data)
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].lower().startswith("type  seq#"):
                idx = i
                break
        if idx >= 0:
            idx = idx + 2   # skip 2 lines
            while idx < len(lines):
                if not lines[idx].strip():
                    break   # empty line ends this session
                line = lines[idx][5:]
                parts = string.split(line)
                assert len(parts) >= 3
                number, ticket_number, status = parts[:3]
                detail = summarydetail.SummaryDetail(
                         [number, ticket_number, "00:00", "", ""])
                tickets.append(detail)

                idx = idx + 1

        return tickets
