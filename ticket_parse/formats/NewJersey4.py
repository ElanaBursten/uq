# NewJersey4.py

import re
#
import date
import locate
import parsers
import summarydetail
import summaryparsers
import tools; R = tools.R

class Parser(parsers.BaseParser):

    # this format allows spaces in term ids :-/
    VALID_LOCATE_CHARS = parsers.BaseParser.VALID_LOCATE_CHARS + " "

    re_transmit_date = R("Transmitted:\s+([0-9/]+)\s+([0-9:]+)")
    re_call_date = R("Taken:\s+([0-9/]+)\s+([0-9:]+)")
    re_ticket_number = R("^Ticket:\s+([0-9-]+)")
    re_ticket_type = R("^Ticket.*Type:\s+(.*?)\s+Previous Ticket:")
    re_work_state = R("^State:\s+(\S+)")
    re_work_county = R("^State.*County:\s+(.*?)\s+Place:")
    re_work_city = R("^State.*Place:\s+(.*?)\s*$")
    re_work_address_number = R("^Addr:\s+From:\s+(.*?)\s+To:")
    re_work_address_street = R("^Addr:.*?Name:\s+(.*?)\s*$")
    re_work_cross = R("^Cross:\s+From:\s+(.*?)\s+To:.*?Name:(.*?)$")
    re_work_description = R("^(Locate:.*?Means of.*?)$", re.DOTALL|re.MULTILINE)
    re_work_type = R("^Work Type:\s+(.*?)\s*$")
    re_work_date = R("Start Date and Time:\s+([0-9/]+)\s+([0-9:]+)")
    re_caller = R("^Contact Name:\s+(.*?)\s*$")
    re_con_name = R("^Company:\s+(.*?)\s*$")
    re_con_address = R("^Addr1:\s+(.*?)\s+Addr2:")
    re_con_city = R("^City:\s+(.*?)\s+State:")
    re_con_state = R("^City.*?State:\s+(\S+)")
    re_con_zip = R("^City.*?Zip:\s+(\S+)")
    re_caller_phone = R("^Phone:\s+(.*?)\s+Fax:")
    re_caller_fax = R("^Phone.*?Fax:[ \t]*(.*?)[ \t]*$")
    re_caller_cellular = R("^Cell Phone:\s+(.*?)\s*(?:Fax|$)")
    re_caller_email = R("^Email:\s+(\S+)\s*$")
    re_caller_contact = R("^Field Contact:\s+(.*?)\s*$")
    re_company = R("^Working for:\s+(.*?)\s*$")
    re_work_remarks = R("^Comments:\s+(.*?)\s*$")

    # there's no response date on the ticket, so we use the work date for now
    re_respond_date = re_work_date

    #f_transmit_date = lambda s, m: \
    # s.isodate(m.group(1) + " " + m.group(2)[:2] + ":" + m.group(2)[2:])
    def f_transmit_date(self, match):
        return tools.isodate(match.group(1) + " " + match.group(2))
    f_call_date = f_respond_date = f_work_date = f_transmit_date

    def f_work_cross(f, match):
        s = match.group(1) + " " + match.group(2)
        return tools.singlespaced(s)

    def find_locates(self):
        prelude = "Members: "
        L = len("CBLVSN WARWICK                   ")
        locates = []
        lines = self.data.split('\n')
        lines.append("") # make sure there's an empty line after "Members:"
        member_lines = tools.find_block(lines,
                       lambda s: s.startswith("Members:"),
                       lambda s: not s.strip())
        member_lines = [s[len(prelude):] for s in member_lines]

        for line in member_lines:
            while line.strip():
                s = line[:L].strip()
                line = line[L:]
                if s:
                    s = s.replace(' ', '')[:10] # TEMPORARY SOLUTION
                    locates.append(s)

        return map(locate.Locate, locates)

    def work_lat_long(self):
        # QMAN-3502
        # line looks like:
        # Boundary: n 41.165305    s 41.161645    w -73.939092    e -73.934250 

        line = R("^(Boundary:.*?)$")
        m = line.search(self.data)
        if m is None:
            return 0, 0
        else:
            parts = m.group(1).split()
            try:
                avg_lat = (float(parts[2]) + float(parts[4])) / 2
                avg_long = (float(parts[6]) + float(parts[8])) / 2
                return avg_lat, avg_long
            except:
                return 0, 0

    def find_work_long(self):
        lat, long = self.work_lat_long()
        return long

    def find_work_lat(self):
        lat, long = self.work_lat_long()
        return lat

    def post_process(self):
        # "single-space" fields
        for attr in ['work_address_street']:
            x = getattr(self.ticket, attr)
            setattr(self.ticket, attr, tools.singlespaced(x))

        parsers.BaseParser.post_process(self)


class SummaryParser(summaryparsers.BaseSummaryParser):

    HEADER = R("TICKET AUDIT FOR TRANSMISSIONS ON: ([0-9/]+)")
    FOOTER = R("^\s+TOTAL\s+-\s+(\d+)")
    TERMID = R("^Service Area:\s+(.*?)\sID:")

    def has_header(self, data):
        return bool(self.HEADER.search(data))

    def has_footer(self, data):
        return bool(self.FOOTER.search(data))

    def get_expected_tickets(self, data):
        m = self.FOOTER.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.HEADER.search(data)
        if m:
            a = m.group(1)
            month, day, year = map(int, a.split("/"))
            return "%04d-%02d-%02d 00:00:00" % (year, month, day)
        else:
            return Date().isodate()

    def get_client_code(self, data):
        m = self.TERMID.search(data)
        if m:
            # remove spaces, cut off at 10
            s = m.group(1).strip().replace(' ', '')[:10]
            return s or 'unknown'
        else:
            return "unknown"

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("********"))

        if idx1 == -1: # no tickets
            return []

        idx1 += 2 # skip two lines

        for i, line in enumerate(lines):
            if i <= idx1:
                continue
            if not line.strip():
                continue
            if ("RETRANSMIT" in line) or ("RESENT" in line) \
            or ("NORMAL" in line) or ("EMERGENCY" in line) \
            or ("TOTAL" in line):
                break

            while line.strip():
                chunk = line[:27].strip()
                line = line[27:].strip()
                if not re.search("^\d+-", chunk):
                    break # not a valid line

                parts = chunk.split()
                if len(parts) == 2:
                    ticket_number = parts[0]
                    seq_number = parts[1]
                    type_code = revision = ""
                elif len(parts) >= 3:
                    ticket_number = parts[0]
                    type_code = parts[1]
                    seq_number = parts[2]
                    revision = ""
                else:
                    break # not a valid line

                detail = summarydetail.SummaryDetail(
                  [seq_number, ticket_number, "00:00", revision, type_code])
                tickets.append(detail)

        return tickets

