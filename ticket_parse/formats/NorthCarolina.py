# NorthCarolina.py

import re
import string
#
from parsers import BaseParser
from re_shortcut import R
import locate
from summaryparsers import BaseSummaryParser
import summarydetail
import tools

class Parser(BaseParser):

    MAX_LOCATES = 60

    re_ticket_number = R("^Ticket : (.*?) Date:")
    re_update_of = R("Old Tkt:\s*(\S+)")
    re_call_date = R("^Ticket :.*Date: ([0-9/]+) Time: ([0-9:]+) Oper")
    re_operator = R("^Ticket :.*Oper: (.*) Chan")
    re_channel = R("^Ticket :.*Chan:(.*)$")
    re_revision = R("Old Tkt:.*Rev :(.*)$")
    #re_ticket_type = R("Priority: (.*?) *$")
    re_ticket_type = R(
     "[A-Z0-9*]+ +\d+ +NCOC[a-z] +[0-9/]+ +[0-9:]+ [A-Z0-9\-]+ +(.*?) *$")

    re_work_state = R("^State: (..)")
    re_work_county = R("^State.*Cnty: (.*) +Place")
    re_work_city = R("^State.*Place: (.*) +In")
    re_work_subdivision = R("^Subdivision: +(.*)$")

    re_work_address_number = R("^Address : +(.*)$")
    re_work_address_street = R("^Street +: +(.*?) +Intersection")
    re_work_cross = R("^Cross 1 : +(.*)$")

    re_work_type = R("^Work type:(.*)$")
    re_work_date = R("^Work date:([0-9/]+) +Time: ([0-9:]+)")
    re_work_notc = R("^Work date.*Hours notice: (.*?) +Priority")
    re_priority = R("^Work date.*Priority: (.*)$")
    re_duration = R("^Duration: (.*?) +Done")

    re_company = R("^Duration.*Done for: (.*)$")
    re_con_name = R("^Company : (.*?) +Type")
    re_con_type = R("^Company.*Type: (.*)$")
    re_con_address = R("^Co addr : (.*)$")
    re_con_city = R("^City +: (.*?) +State")
    re_con_state = R("^City.*State: (..)")
    re_con_zip = R("^City.*Zip: (.*)$")
    #re_explosives = R("^Ug/Oh/Both: \S+ +Blasting: ([YN])  Boring: N")
    re_explosives = R("^Ug/Oh/Both.*?Blasting:\s*(\S+)")

    re_caller = R("^Caller +: (.*?) +Phone")
    re_caller_phone = R("^Caller.*Phone: (.*)$")
    re_caller_email = R("^Email\s*:\s*(\S+)")
    re_caller_cellular = R("^Cellular: (.*)$")
    re_transmit_date = R("\d+ NCOC[a-z] ([0-9/]+) ([0-9:]+)")
    re_caller_contact = R("^Contact : +(.*?) *Phone")
    re_map_page = R("^Grids +: *(\S+)") # only the first one counts

    f_call_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))
    f_work_date = f_transmit_date = f_call_date

    # Multiline fields in North Carolina tickets differ from those in the
    # previous two. Every next line is part of the field until a line with
    # only a ":" is encountered.

    re_work_description = re.compile("^Location: (.*?)^:$",
     re.MULTILINE | re.DOTALL)
    #f_work_description = lambda s, m: tools.singlespaced(m.group(1))

    re_work_remarks = re.compile("^Remarks : (.*?)^:$",
     re.MULTILINE | re.DOTALL)

    f_work_description = lambda s, m: m.group(1).strip()
    f_work_remarks = f_work_description

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        lines = [line for line in lines if line.startswith("Members:")]
        for line in lines:
            members = line.split()[1:]    # [0] is "Members:"
            for member in members:
                if member.endswith("*"):
                    member = member[:-1]
                locates.append(member)
        return map(locate.Locate, locates)

    def find_grids(self):
        TRIGGER = "Grids   :"
        grids = []
        for line in self.data.split("\n"):
            if line.startswith(TRIGGER):
                line = line[len(TRIGGER):]
                parts = line.split()
                for part in parts:
                    while part.endswith("-"):
                        part = part[:-1]
                    if part not in grids:
                        grids.append(part)
        return grids

    def post_process(self):
        if not self.ticket.caller_contact:
            self.ticket.caller_contact = self.ticket.caller
        BaseParser.post_process(self)

    ###
    ### Finding longitudes/latitudes in grids

    def find_work_lat(self):
        return self._compute_longlats()[0]

    def find_work_long(self):
        return self._compute_longlats()[1]

    def _get_grids(self):
        # chop off grids after 10 characters to avoid errors
        grids = BaseParser._get_grids(self)
        grids = [grid[:10] for grid in grids]
        return grids

class SummaryParser(BaseSummaryParser):
    LENGTH = len("00001 C070330069-00C 07:47")
    LENGTH2 = len("00001 A011536475-00A   ")

    def has_header(self, data):
        rheader = R("^\* Summary for \d\d/\d\d/\d\d -")
        return not not rheader.search(data)

    def has_footer(self, data):
        rfooter = R("^Total: \d+ *$")
        return not not rfooter.search(data)

    def get_client_code(self, data):
        m = R("^\* Summary for \S+ - (\S+)").search(data)
        client_code = m and m.group(1) or ""
        if client_code.endswith("*"):
            client_code = client_code[:-1]
        return client_code

    def get_summary_date(self, data):
        m = R("^\* Summary for (\S+)").search(data)
        d = m and m.group(1) or ""
        if d:
            d = tools.isodate(d)
        return d

    def get_expected_tickets(self, data):
        m = R("^Total: (\d+)").search(data)
        expected_tickets = m and int(m.group(1)) or 0
        return expected_tickets

    def read(self, data):
        rline = R("^\d\d\d\d\d ")
        tickets = []
        for line in data.split("\n"):
            line = line.strip()
            if line and rline.search(line):
                # there are two kinds of summaries; one doesn't have times
                while line:
                    if ":" in line:
                        l2 = line[:self.LENGTH+1]
                        # get 1 more than the required length, to catch
                        # out-of-bound ticket numbers
                        parts = l2.split()
                        number, ticket_number, time = parts[:3]
                        ticket_number, revision = (ticket_number[:-4],
                                                   ticket_number[-3:])
                        detail = summarydetail.SummaryDetail(
                                 [number, ticket_number, time, revision, ""])
                        tickets.append(detail)

                        # inspect rest of line
                        line = line[self.LENGTH+1:].strip()
                    else:
                        l2 = line[:self.LENGTH2]
                        parts = l2.split()
                        number, ticket_number = parts[:2]
                        ticket_number, revision = (ticket_number[:-4],
                                                   ticket_number[-3:])
                        detail = summarydetail.SummaryDetail(
                                 [number, ticket_number, "00:00", revision, ""])
                        tickets.append(detail)
                        line = line[self.LENGTH2:].strip()

        return tickets

