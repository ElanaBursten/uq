# Alaska.py

from parsers import BaseParser
from re_shortcut import R
import ticketparser

from summaryparsers import BaseSummaryParser
import string
import tools
import locate
import summarydetail

class Parser(BaseParser):

    # allow spaces in term ids
    VALID_LOCATE_CHARS = BaseParser.VALID_LOCATE_CHARS + " "

    re_ticket_number = R("^Ticket No: *(\S+)")
    re_ticket_type = R("Header Code: (.*?) *$")
    re_transmit_date = R(
     "^Transmit Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_call_date = R(
     "^Original Call Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_work_date = R(
     "^Locate By Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_con_name = R("Company:\s+(.*?) *$")
    re_caller_contact = R("^Contact Name: *(.*?) *Contact Phone")
    re_caller_phone = R("Contact Phone: *(\S+)")
    re_caller_fax = R("Fax No: *(\S+)")
    re_work_city = R("Locate City/Area: (.*?) *$")
    re_work_address_number = R("^Address: *(\S+) *Unit:")
    re_work_address_street = R("^Address.*Unit:.*, (.*?) *$")
    re_work_cross = R("^Nearest Intersecting Street: (.*?) *$")
    re_map_page = R("^MAP PAGE: (.*?) *(MAP PAGE|$)")
    re_work_type = R("^Type of Work: (.*?) *$")

    re_company = re_con_name
    # this field doesn't appear on the ticket, so I duplicate con_name

    def f_transmit_date(self, match):
        return tools.isodate(string.join(match.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def find_work_state(self):
        return "AK"

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Sending to:"))
        if idx1 > -1:
            for i in range(idx1+1, len(lines)):
                line = lines[i]
                if not line.strip():
                    break
                while line.strip() and not line.startswith("  "):
                    chunk, line = line[:17], line[17:]
                    chunk = chunk.replace("*", "").strip() # no asterisks
                    if chunk:
                        locates.append(chunk)
                else:
                    continue

        # fix name longer than 10 characters (not a LocInc client)
        for i, name in enumerate(locates):
            if name == "GCI - FIBER":
                locates[i] = "GCI-FIBER"

        return [locate.Locate(x) for x in locates]

    s_work_description = "Dig Info:"
    s_work_remarks = "Remarks:"

    def find_work_description(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_description):
                desc = line[len(self.s_work_description):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def find_work_remarks(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_remarks):
                desc = line[len(self.s_work_remarks):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

class SummaryParser(BaseSummaryParser):
    LENGTH = len("0002-2005170346 ")
    RECOG_HEADER_2 = "THE FOLLOWING MESSAGE NUMBERS WERE TRANSMITTED"

    re_footer = R("^::TOTAL = (\d+) *$")
    re_recog_header_1 = R("\s+\*SUM\* GAUPC") # not used?
    re_header_date = R("THE FOLLOWING MESSAGE NUMBERS WERE TRANSMITTED TO YOU TODAY +([0-9/]+)")
    re_client_code = R("STATION: (.*?) *$")

    def has_header(self, data):
        return string.find(data, self.RECOG_HEADER_2) > -1

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_header_date.search(data)
        if m:
            s = m.group(1)
            parts = map(int, s.split("/"))
            return "%04d-%02d-%02d 00:00:00" % (parts[2], parts[0], parts[1])
        else:
            raise SummaryError, "No date found on audit"

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        if m:
            return m.group(1).strip()
        else:
            return "?"

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        status = 0
        for line in lines:
            if status == 0 and line.find("STATION:") > -1:
                status = 1
                continue
            if status == 1 and line.find("TOTAL =") > -1:
                break
            if status:
                if line.startswith("::"):
                    line = line[2:] # strip leading "::"
                while line.strip():
                    chunk, line = line[:self.LENGTH], line[self.LENGTH:]
                    chunk = chunk.strip()
                    if len(chunk) > 10:
                        try:
                            seq_number, ticket_number = chunk.split("-", 1)
                            detail = summarydetail.SummaryDetail([seq_number,
                                     ticket_number, "00:00", "", ""])
                            tickets.append(detail)
                        except:
                            pass

        return tickets

