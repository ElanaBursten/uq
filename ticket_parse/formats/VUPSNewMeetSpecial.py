# VUPSNewFCV3.py

import _VUPSNew
import tools
import string

class Parser(_VUPSNew.Parser):

    _re_meet_date = _VUPSNew.R("^Meet\s+Date: ([0-9/]+)\s+Time: ([0-9:]+) (AM|PM)")
    def find_work_date(self):
        """
        If a string such as
        Meet Date: 03/04/08 Time: 10:00 AM
        is found, replace work_date with Meet Date
        """
        m = self._re_meet_date.search(self.data)
        if m and self.ticket.ticket_type.find("MEET") != -1:
            # Set the work date to the Meet Date
            return tools.isodate(string.join(m.groups(), " "))
        else:
            return self.ticket.work_date
