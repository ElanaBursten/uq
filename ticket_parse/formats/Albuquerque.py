# Albuquerque.py

from re_shortcut import R
import Lanham
import tools
import string
import locate
import Washington2
from date import Date

class Parser(Lanham.Parser):
    # based on Lanham, but not quite the same.

    required = Lanham.Parser.required[:]
    required.append("serial_number")

    re_serial_number = R("(NMOC\d+)\s+\d+")
    re_ticket_type = R("Header Code: (.*?) *$")
    re_transmit_date = R(
     "^Transmit Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_call_date = R(
     "^Original Call Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_work_date = R(
     "^Work to Begin Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_con_name = R("Company:\s+(.*?) *$")
    re_work_county = R("County: (.*?) *City:")
    re_work_city = R("City: (.*?) *$")
    re_work_address_number = R("^Address:\s+(.*?)\s*,")
    re_work_address_street = R("^Address:.*?,\s*(.*?) *$")

    def f_transmit_date(self, match):
        return tools.isodate(string.join(match.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    s_work_description = "Location of Work:"

    def find_work_description(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_description):
                desc = line[len(self.s_work_description):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def find_work_remarks(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith("Remarks: "):
                desc = line[len("Remarks:"):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Sending to:"))
        if idx1 > -1:
            for i in range(idx1+1, len(lines)):
                line = lines[i]
                if line.strip() and not line.startswith("  "):
                    line = line.replace("*", "") # no asterisks in locate names
                    parts = line.split()
                    locates.extend(parts)
                else:
                    break

        for i, name in enumerate(locates):
            # if there's an "=" in the name, remove it and everything after it
            idx = name.find("=")
            if idx > -1:
                locates[i] = name[:idx]

        return [locate.Locate(x) for x in locates]

    _re_townshipline = R("^(Town.*?$)")

    def find_map_page(self):
        m = self._re_townshipline.search(self.data)
        if m:
            line = m.group()
            parts = line.split()
            map_page = ""
            if len(parts) >= 2:
                map_page += parts[1]
            if len(parts) >= 4:
                map_page += parts[3]
            if len(parts) >= 7:
                map_page += string.join(parts[6:8], "")
            return map_page
        else:
            return ""

class SummaryParser(Washington2.SummaryParser):
    re_ticket = R("(\*?)(\d+) (NMOC\d+) (\d\d:\d\d)")

    def get_client_code(self, data):
        # there is no client code on this type of summaries, so we make one up
        return "NMOC"

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            d = Date(m.group(3) + " " + m.group(4))
        else:
            d = Date()
        if d.hours <= 7:
            d.dec()
        d.resettime()
        return d.isodate()
        # how well does this work for weekends? find out.
