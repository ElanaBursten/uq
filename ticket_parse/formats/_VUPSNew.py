# _VUPSNew.py

from parsers import BaseParser
from re_shortcut import R
import locate
import re
import string
import tools

class Parser(BaseParser):
    """ "Abstract" class for the new VUPS format. """

    MAX_LOCATES = 60
    PARSE_GRIDS = False
    # subclasses can set this to true to turn parsing on.  currently only for
    # the VUPSNew* branch of formats.

    required = BaseParser.required[:]
    required.remove("work_date")

    re_ticket_number = R("^Ticket No:\s+([A-Z0-9]+)-0")
    re_update_of = R("Old Tkt No:\s+([A-Z0-9]+)")
    re_revision = R("^Ticket No:\s+[A-Z0-9]+-(0\S+)")
    re_transmit_date = R("^Transmit\s+Date: ([0-9/]+)\s+Time: ([0-9:]+) (AM|PM)")
    re_call_date = R("^Call\s+Date: ([0-9/]+)\s+Time: ([0-9:]+) (AM|PM)")
    re_work_date = R("^Update By\s+Date: ([0-9/]+)\s+Time: ([0-9:]+) (AM|PM)")
    re_ticket_type = R("^Ticket No:\s+\S+\s+(.*?) *$")
    re_work_county = R("^City/Co:(.*?) *Place:")
    re_work_city = R("^City/.*Place:(.*?) *State:")
    re_work_state = R("^City/.*State: *(..)")
    re_work_address_number = R("^Address: *(.*?) *Street")
    re_work_address_street = R("^Address.*Street: *(.*?) *$")
    re_work_cross = R("^Cross 1: *(.*?) *(Intersection|$)")
    re_work_type = R("^Type of Work: *(.*?) *$")
    re_company = R("^Work Done For: *(.*?) *$")
    re_work_remarks = R("^Location: *(.*?)\nInstructions:", re.DOTALL)
    re_work_description = R("^Instructions: *(.*?)\nWhitelined", re.DOTALL)
    re_con_name = R("^Company: *(.*?) *Type:")
    re_con_address = R("^Co\. Address: *(.*?) *(First Time|$)")
    re_con_city = R("^City: *(.*?) *State:")
    re_con_state = R("^City:.*State:(..)")
    re_con_zip = R("^City:.*Zip:(\S+)")
    re_caller_phone = R("^Company Phone: *(\S+)")
    re_caller_contact = R("^Contact Name: *(.*?) *Contact Phone")
    re_caller_altphone = R("^Contact Name.*Contact Phone:(\S+)")
    re_caller_email = R("^Email:\s+(\S+)")
    re_caller_fax = R("Contact Fax *:(\S+)")
    re_map_page = R("^Grids: *(\S+)")
    re_explosives = R("Blasting:\s+(\S+)")

    f_transmit_date = lambda s, m: tools.isodate(string.join(m.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def pre_process(self):
        self.data = self.data.replace('\t','') # strip tabs

    def find_locates(self):
        # kludgy way to determine old or revised format
        if self.data.count("Requested By:") > 0 \
        or self.data.count("First Time:") == 0:
            cutoff = 38
        else:
            cutoff = 40

        locates = []
        lines = string.split(self.data, "\n")
        lines.append("")    # add an empty line in case the locates block ends
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members:"))
        if idx1 > -1:
            idx2 = tools.findfirst(lines[idx1+1:], lambda s: not s.strip())
            if idx2 > -1:
                for line in lines[idx1+1:idx1+1+idx2]:
                    while line:
                        chunk = line[:cutoff]   # first 38 characters
                        code, desc = string.split(chunk, "=")
                        loc = locate.Locate(string.strip(code))
                        locates.append(loc)
                        line = line[cutoff:]    # get rest of line

        return locates

    def find_grids(self):
        if not self.PARSE_GRIDS:
            return []

        grids = []
        lines = self.data.split('\n')
        for line in lines:
            if line.startswith("Grids: "):
                line = line[10:]
                parts = line.split()
                for part in parts:
                    idx = part.find('-')
                    if idx >= 0:
                        part = part[:idx]
                    grids.append(part)

        return grids

    def _compute_longlats(self):
        # compute longitude/latitude based on qtrmin-grids (1234A5678B, etc.)
        if hasattr(self, "_latlong"):
            return self._latlong
        else:
            grids = map(tools.grid2decimal, self.ticket.grids)
            latitudes = [t[0] for t in grids]
            longitudes = [-t[1] for t in grids]
            avg_latitude = tools.average(latitudes)
            avg_longitude = tools.average(longitudes)
            self._latlong = (avg_latitude, avg_longitude)
            return self._latlong

    def post_process(self):
        if self.ticket.grids:
            self.ticket.work_lat = self._compute_longlats()[0]
            self.ticket.work_long = self._compute_longlats()[1]
        BaseParser.post_process(self)
