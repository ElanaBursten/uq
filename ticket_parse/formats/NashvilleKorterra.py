# NashvilleKorterra.py

import string
import Tennessee
import SouthCaliforniaSDGKorterra
from re_shortcut import R
import locate

class Parser(Tennessee.Parser):
    call_center = 'FNV3'
    re_transmit_date = R("KORTERRA JOB \S+ \w+ Seq: \d+ ([0-9/]+) ([0-9:]+)")
    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))

class SummaryParser(SouthCaliforniaSDGKorterra.SummaryParser):
    r_client_code = R("KORTERRA AUDIT \w+(\w{3})-")
