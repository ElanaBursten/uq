# AlabamaNew.py

import Tennessee
from re_shortcut import R
import string, re

class Parser(Tennessee.Parser):
    re_transmit_date = R("\d+ +..OCS? +([0-9/]+) +([0-9:]+)")
    re_caller_email = R("EMAIL--\[(.*?)\]")
    re_work_remarks = R("REMARKS--+\[(.*?)\]", re.DOTALL)

    def post_process(self):
        Tennessee.Parser.post_process(self)
        if not self.ticket.work_address_number.strip() \
        and self.ticket.work_address_street:
            # check if number is in street address; if so, move it
            numberstr, street = "", self.ticket.work_address_street
            while street and street[0] in string.digits:
                numberstr += street[0]
                street = street[1:]
            if numberstr:
                self.ticket.work_address_number = numberstr
                self.ticket.work_address_street = street.strip()

class SummaryParser(Tennessee.SummaryParser):

    def has_header(self, data):
        return "FROM ALOC" in data
