# WV1181Alt.py
# Same as WV1181, but the audit format is different.

import WV1181Korterra
import NewJersey2010B
from tools import R

class Parser(WV1181Korterra.Parser):
    # 2012-02-15: we will now use this parser to handle Korterra tickets that
    # lack the Korterra header but are otherwise the same.
    re_transmit_date = R("^Type.*Date:\s+(\S+) (\S+)")

class SummaryParser(WV1181Korterra.SummaryParser):
    # Korterra audit. Audit report must check for tickets with PE or PEB
    # clients.
    def get_client_code(self, data):
        return "1181" # dummy

