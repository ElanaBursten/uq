# NewJersey.py

from parsers import BaseParser
from re_shortcut import R
import locate
import re
import string
import ticketparser

from summaryparsers import BaseSummaryParser
import tools
import summarydetail
from date import Date

class Parser(BaseParser):
    only_one = BaseParser.only_one[:]; only_one.remove("ticket_number")

    re_transmit_date = R("Transmit: +Date: ([0-9/]+) +At: ([0-9]+)")
    re_ticket_number = R("Request No.: ([0-9]+)")
    re_update_of = R("Request No.: \S+ Of Request No.:? ([0-9]+)")
    re_ticket_type = R("^\*\*\*(.*?)\*\*\* Req")

    # call_date isn't known, so for now, I assume call_date == transmit_date
    re_call_date = re_transmit_date

    re_work_county = R("County: (.*?) +Muni")
    re_work_city = R("Municipality: (.*?) *$")
    re_work_subdivision = R("Subdivision.Community: (.*?) *$")
    re_work_address_street = R("Street: +(.*?) *$")
    re_work_cross = R("Nearest Intersection: +(.*?) *$")
    re_work_type = R("Type of Work : +(.*?) *$")
    re_work_description = R("Extent of Work: +(.*?) +DEPTH")

    re_work_date = R("Start Date.Time: +([0-9/]+) +A[tT] ([0-9]+)")

    re_company = R("Working For: +(.*?) *$")
    # Some fields follow which have no equivalent (yet) in Ticket

    re_caller = R("Caller: +(.*?)( +Title| *$)")
    re_caller_phone = R("Caller.*?\n +Phone: +(.*?) *?$", re.DOTALL)

    re_con_name = R("Excavator: +(.*) *$")
    re_con_address = R("Excavator.*Address: +(.*?) *$", re.DOTALL)
    re_con_city = R("Excavator.*City: +(.*?),", re.DOTALL)
    re_con_state = R("Excavator.*City: +.*?, (..) +", re.DOTALL)
    re_con_zip = R("Excavator.*City:.*?, .. +(.*?) *$", re.DOTALL)
    re_caller_fax = R("Excavator.*?Fax: +(.*?) *$", re.DOTALL)
    re_caller_cellular = R("Excavator.*?Cellular: +(.*?)( *WORK|$)", re.DOTALL)
    re_caller_email = R("Excavator.*?Email: +(.*?) *$", re.DOTALL)

    re_respond_date = re_work_date
    # FIXME; I only included this to get respond_date filled!

    f_transmit_date = lambda s, m: \
     s.isodate(m.group(1) + " " + m.group(2)[:2] + ":" + m.group(2)[2:])
    f_call_date = f_respond_date = f_work_date = f_transmit_date

    f_ticket_type = lambda s, m: string.join(m.group(1).split(), "")

    def find_work_state(self): return "NJ"

    def find_work_remarks(self):
        lines = self.data.split("\n")
        remarks = ""
        for line in lines:
            if line.strip().startswith("Remarks:"):
                remarks = "!"
                continue
            if remarks:
                if not line.strip():
                    break
                if line.startswith(" "):    # indentation
                    remarks = remarks + line.strip() + " "
                else:
                    break   # end of it
        return remarks[1:]  # remove "!"

    def find_locates(self):
        re_loc = R("(^|^ )New Jersey One Call.*CDC = +(\S+)")
        m = re_loc.search(self.data)
        if m:
            loc = m.group(2)
            if loc == "XXX":
                # get complete locates list, a bit like Atlanta
                reloc2 = R("\S+=/")
                ok = 0
                locates = ["XXX"]
                for line in self.data.split('\n'):
                    if ok:
                        line = line.strip()
                        if line:
                            names = re.findall("(\S+)=/", line)
                            locates.extend(names)
                        else:
                            break
                    else:
                        if line.startswith("Operators Notified:"):
                            ok = 1
                return map(locate.Locate, locates)
            else:
                # return only one locate
                return [locate.Locate(loc)]
        else:
            return []

    def sanitycheck_before(self):
        # the 'End Request' line must always be present, or the ticket is
        # probably incomplete
        BaseParser.sanitycheck_before(self)
        if 'End Request' not in self.data:
            raise ticketparser.TicketError, "No 'End Request' found"

    def post_process(self):
        # try to extract street number from address
        parts = self.ticket.work_address_street.split()
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_street = string.join(parts[1:], " ")
            self.ticket.work_address_number = parts[0]

        BaseParser.post_process(self)

class SummaryParser(BaseSummaryParser):
    LENGTH = len("020721019 0634   1314")
    # For multipart summaries, do not want to recognize the header
    # so multipart collections will work
    rheader = R("^NJ1C audit for CDC of +\S+ +on +\d{6}(?! +\(Page)")
    rfooter = R("^Total number of Messages for \S+ +\d+")
    rclient = R("NJ1C audit for CDC of (.*) +on")
    rsummarydate = R("NJ1C audit for.*on (\d{6})")
    rexpected = R("Total number of Messages for \S* +(.*)$")

    # New Jersey summaries can be seriously multipart.

    @classmethod
    def count_received(cls, summ_data):
        '''
        Only count the received tickets with a item (sequence) number
        '''
        total = 0
        for item in summ_data:
            if len(item.item_number):
                total += 1
        return total

    def has_header(self, data):
        return not not self.rheader.search(data)

    def has_footer(self, data):
        return not not self.rfooter.search(data)

    def get_client_code(self, data):
        m = self.rclient.search(data)
        client = m and m.group(1) or ""
        if not client:
            m = R("SEQUENCE NUMBER \d* +CDC = (.*)$").search(data)
            client = m and m.group(1) or ""
        return client

    def get_summary_date(self, data):
        m = self.rsummarydate.search(data)
        date = m and m.group(1) or ""
        if date:
            date = "20%s-%s-%s 00:00:00" % (date[:2], date[2:4], date[4:])
        return date

    def get_expected_tickets(self, data):
        m = self.rexpected.search(data)
        expected_tickets = m and int(m.group(1)) or 0
        return expected_tickets

    def read(self, data):
        tickets = []
        for line in data.split("\n"):
            line = line.strip()
            if line and line[0] in "0123456789":
                while line:
                    l2 = line[:self.LENGTH]
                    parts = l2.split()
                    if len(parts) == 3:
                        ticket_number, number, time = parts[:3]
                        time = time[:2] + ":" + time[2:]
                        detail = summarydetail.SummaryDetail(
                                 [number, ticket_number, time, "", ""])
                        tickets.append(detail)
                    # inspect rest of line
                    line = line[self.LENGTH:].strip()

        return tickets

    def post_process(self, ts):
        BaseSummaryParser.post_process(self, ts)

        # New Jersey has special rules for setting the summary_date_end
        # field...
        if ts.summary_date:
            d = Date(ts.summary_date)
            if d.isweekend():
                while d.isweekend():
                    d.dec() # return to last Friday
                ts.summary_date = d.isodate()
                d.inc(3)    # move to next Monday
                ts.summary_date_end = d.isodate()
            else:
                ts.summary_date_end = self._nextmorning(ts.summary_date)

        return ts

