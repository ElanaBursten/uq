# Nashville2013.py

import re
import string
#
import locate
import parsers
import summaryparsers
from tools import R, findfirst
import summarydetail
import tools

class Parser(parsers.BaseParser):
    re_ticket_number = R("TICKET NUMBER: (\S+)")
    re_update_of = R("OLD TICKET NUM: (\S+)")
    re_ticket_type = R("Message Type:\s*(.*?)\s*For Code:")
    re_call_date = R("Taken Date:\s*([0-9/]+) ([0-9:]+)")
    re_transmit_date = re_call_date

    re_work_date = R("Work To Begin:\s*([0-9/]+) AT ([0-9:]+)")
    re_con_name = R("Excavator:\s*(.*?)\s*Excavator Phone")
    re_con_address = R("Address:\s*(.*?)\s*Caller:")
    re_con_city = R("City, St, Zip:\s*(.*?),")
    re_con_state = R("City, St, Zip:.*?, (\S+)")
    re_con_zip = R("City, St, Zip:.*?, .. (\d+)")
    re_caller_phone = R("Caller Phone:\s*(.*?)\s*$")
    re_caller_altphone = R("Contact Phone:\s*(.*?)\s*$")
    re_caller_fax = R("Contact Fax:\s*(.*?)\s*Contact:")
    re_caller_email = R("Contact Email:\s*(.*?)\s*Contact Phone:")
    re_caller = R("Caller:\s*(.*?)\s*$")

    re_work_state = R("^State:\s+(\S+)")
    re_work_county = R("^County:\s+(.*?)\s*Update Date:")
    re_work_city = R("^Place:\s+(.*?)\s*Expire Date")
    re_work_address_street = R("Place.*Address:\s*(.*?)\s*Intersection",
                             re.DOTALL)
    re_work_cross = R("Intersection:\s*(.*?)\s*$")
    re_work_type = R("Work Type:\s*(.*?)\s*Explosives:")
    re_company = R("Done For:\s*(.*?)\s*Directional")
    re_explosives = R("Explosives: (.)")

    re_work_lat = R("Latitude:\s*([0-9.-]+)")
    re_work_long = R("Longitude:\s*([0-9.-]+)")

    re_work_remarks = R("REMARKS\)\s*-{10,}(.*?)\s*-{10,}", re.DOTALL)
    re_map_page = R("Grids:\s+\[(\S+)\s*\]")

    def f_call_date(self, match):
        m, d, y = match.group(1).split("/")
        return "20%s-%s-%s %s:00" % (y, m, d, match.group(2))
    f_transmit_date = f_call_date
    f_work_date = f_call_date

    def find_locates(self):
        lines = self.data.split('\n')
        idx = findfirst(lines, lambda s: s.strip() == "Utilities Notified")
        termids = []
        if idx > -1:
            lines = lines[idx+4:]
            for line in lines:
                if line.strip():
                    parts = line.split()
                    if parts and parts[0][0] not in " -":
                        termids.append(parts[0])
        return [locate.Locate(x) for x in termids]

    def post_process(self):
        parsers.BaseParser.post_process(self)

        self.ticket.work_lat = float(self.ticket.work_lat)
        self.ticket.work_long = float(self.ticket.work_long)
        self.ticket.work_remarks = self.ticket.work_remarks.strip()
        #self.ticket.work_description = self.ticket.work_remarks

        # try to get address number
        s = self.ticket.work_address_street
        parts = string.split(s)
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_number = parts[0]
            self.ticket.work_address_street = string.join(parts[1:], " ")


class SummaryParser(summaryparsers.BaseSummaryParser):

    S_HEADER = R("From (TOCS|TNOCS)", re.IGNORECASE)
    S_FOOTER = R("^Total\s*:\s*(\d+)")
    S_AUDIT_DATE = R("^Audit For ([0-9/]+)")
    #S_CLIENT_CODE = R("^FOR CODE\s+(.*)\s*$")

    def has_header(self, data):
        return bool(self.S_HEADER.search(data))

    def has_footer(self, data):
        return bool(self.S_FOOTER.search(data))

    def get_client_code(self, data):
        return 'UTILNASH'

    def get_summary_date(self, data):
        m = self.S_AUDIT_DATE.search(data)
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        #m = R("^TOTAL FOR [A-Z0-9]+: (\d+)").search(data.upper())
        m = self.S_FOOTER.search(data)
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].upper().startswith("TYPE  SEQ#"):
                idx = i
                break
        if idx >= 0:
            idx = idx + 2   # skip a line
            while idx < len(lines):
                if not lines[idx].strip() \
                or lines[idx].upper().strip().startswith("COMPLIANT"):
                    break   # empty line ends this session
                line = lines[idx][5:]
                parts = line.split()
                assert len(parts) >= 3, "Unparseable line: %s" % (parts,)
                number, ticket_number, status = parts[:3]
                detail = summarydetail.SummaryDetail(
                         [number, ticket_number, "00:00", "", ""])
                tickets.append(detail)

                idx = idx + 1

        return tickets


