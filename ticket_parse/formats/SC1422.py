# SC1422.py

import string
from parsers import BaseParser
from re_shortcut import R
import locate
import GreenvilleNew
import Dallas4
import tools

class Parser(GreenvilleNew.Parser):
    # NOTE: MUST use _datefinder for date parsing; using re_transmit_date here
    # will NOT work; use find_transmit_date = _datefinder(...) instead!

    _re_locates = R("(\w+) \d+ PUPS .*? [0-9/]+ [0-9:]+ [AP]M")
    _re_facility = R("^Facility: (\w+)")

    find_transmit_date = GreenvilleNew._datefinder("From:\s+IRTHNet\s+At:\s+")

    def find_locates(self):
        m = self._re_locates.search(self.data)
        if not m:
            return []
        term_id = m.group(1)
        return [locate.Locate(term_id)]

    _re_work_lat1 = R("^Lat/Long:\s*(\S+),")
    _re_work_lat2 = R("^Secondary:\s*(\S+),")
    def find_work_lat(self):
        m1 = self._re_work_lat1.search(self.data)
        m2 = self._re_work_lat2.search(self.data)
        lat1 = lat2 = 0.0
        if m1:
            lat1 = float(m1.group(1))
        if m2:
            lat2 = float(m2.group(1))
            if lat2 == 0.0:
                lat2 = lat1
        else:
            lat2 = lat1
        return (lat1+lat2)/2.0

    _re_work_long1 = R("^Lat/Long:\s*\S+, *(\S+)")
    _re_work_long2 = R("^Secondary:\s*\S+, *(\S+)")
    def find_work_long(self):
        m1 = self._re_work_long1.search(self.data)
        m2 = self._re_work_long2.search(self.data)
        long1 = long2 = 0.0
        if m1:
            long1 = float(m1.group(1))
        if m2:
            long2 = float(m2.group(1))
            if long2 == 0.0:
                long2 = long1
        else:
            long2 = long1
        return (long1+long2)/2.0

    def post_process(self):
        if self.ticket.work_lat > 0 and self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat,
                                   self.ticket.work_long)

        # remove spaces from work_address_number if necessary
        number = self.ticket.work_address_number
        parts = [s.strip() for s in number.split(" ", 1)]
        if len(parts) > 1:
            self.ticket.work_address_number = parts[0]
            self.ticket.work_address_number_2 = parts[1]

        # remove superfluous whitespace
        self.ticket.work_remarks = self.ticket.work_remarks.strip()
        self.ticket.work_description = self.ticket.work_description.strip()

        BaseParser.post_process(self)

class SummaryParser(Dallas4.SummaryParser):
    CLIENT_CODE = 'PUPS'

