import string
from parsers import BaseParser
import NorthCarolina
import Dallas4
from re_shortcut import R

class Parser(NorthCarolina.Parser):
    def post_process(self):
        # Strip alphabetic characters from revision
        self.ticket.revision = str(int(string.join([num for num in self.ticket.revision if num.isdigit()],'')))
        BaseParser.post_process(self)

class SummaryParser(Dallas4.SummaryParser):
    CLIENT_CODE = 'NCOC'

