# GA3004.py

import re
import GA3003
from re_shortcut import R
import tools

class Parser(GA3003.Parser):

    # matches the HP section (if any) at the end of the ticket
    _re_hp_section = R("-{30,}(.*)$", re.DOTALL|re.MULTILINE)

    def post_process(self):
        GA3003.Parser.post_process(self)

        # Mantis #2845: set locate.alert for those locates (AGL only) that are
        # found in the "HP" section at the end of the ticket.
        hp_section = tools.re_get(self._re_hp_section, self.data)
        for x in self.ticket.locates:
            if not x.client_code.startswith('AGL'):
                continue
            if x.client_code + "-HP" in hp_section:
                x.alert = self.ticket.alert = 'A'

        # Mantis #2869: store this section in self._hp_info
        for line in hp_section.split('\n'):
            line = line.strip().replace('\t', '    ')
            if line.strip():
                self.ticket._hp_info.append(line.strip())

class SummaryParser(GA3003.SummaryParser):
    re_recog_header_1 = R("\s+\*SUM\* UPCA")

