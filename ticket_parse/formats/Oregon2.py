# Oregon2.py

import Oregon
from re_shortcut import R

class Parser(Oregon.Parser):
    # we don't parse this for Oregon2 (LOR1)

    # modifiers, like CANCELLATION or DUPLICATION, are part of the ticket type

    #def find_kind(self):
    #    if self.ticket.ticket_type.upper().find("CANCELLATION") >= 0:
    #        return "EMERGENCY"
    #    return OregonParser.find_kind(self)
    s_work_description = "Location of Work:"

    def _parse_member_area(self):
        return []

    re_ticket_type = R("^Ticket No: *\d+ *(.*?)$")

class SummaryParser(Oregon.SummaryParser):
    pass
