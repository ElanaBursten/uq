# Wisconsin1.py

from parsers import BaseParser
from re_shortcut import R
import locate
import re
import string
import tools
import Albuquerque2

class Parser(BaseParser):

    re_ticket_number = R("^Ticket #: *(\d+)")
    re_ticket_type = R("^Header:\s*(.*?)\s*Type: *(.*?) *Operator")
    re_work_date = R("^Start Date *: *([0-9/]+) *Time: ([0-9:]+) (AM|PM)")
    re_call_date = R("^Call Date *: *([0-9/]+) *Time: ([0-9:]+) (AM|PM)")
    re_transmit_date = R("^Transmit Date *: *([0-9/]+) *Time: ([0-9:]+) (AM|PM)")
    re_caller_contact = R("^Caller: (.*?) *Phone:")
    re_caller_phone = R("^Caller.*Phone: (.*?) *$")
    re_con_name = R("^Caller.*?\n *(.*?) *(Fax|$)", re.DOTALL)
    re_con_address = R("^Caller.*?\n.*?\n *(.*?) *(Cell|$)", re.DOTALL)
    re_con_city = R("^Caller.*?\n.*?\n.*?\n *(.*?) *(Pager|$)", re.DOTALL)
    re_con_state = R("^Caller.*?\n.*?\n.*?\n.*?\n *(\S+) *", re.DOTALL)
    re_con_zip = R("^Caller.*?\n.*?\n.*?\n.*?\n *\S+ *(.*?) *$", re.DOTALL)

    re_work_county = R("^County *: *(.*?) *$")
    re_work_city = R("^Place *: *(.*?) *$")
    re_work_address_street = R("^Street *: *(.*?) *$")
    re_work_cross = R("^Intersection 1: *(.*?) *$")
    re_work_type = R("^Type of Work: *(.*?) *$")
    re_company = R("^Working For *: *(.*?) *$")
    re_work_description = R("^Marking Instructions:.*?\n(.*?)\nRemarks", re.DOTALL)
    re_work_remarks = R("^Remarks:.*?\n(.*?)\n *Block:", re.DOTALL)
    _re_work_latNW = R("Latitude NW\s+(\d+\.\d+)")
    _re_work_latSE = R("Latitude SE\s+(\d+\.\d+)")
    _re_work_longNW = R("Longitude NW\s+(-\d+\.\d+)")
    _re_work_longSE = R("Longitude SE\s+(-\d+\.\d+)")

    f_work_date = lambda s, m: tools.isodate(string.join(m.groups(), ' '))
    f_call_date = f_work_date
    f_transmit_date = f_work_date

    def f_ticket_type(self, match):
        return string.join(match.groups(), ' ')

    # match the last string between parentheses; ignore any paren groups
    # before that
    _re_termid = R("\(([^()]*?)\) *$")

    def find_work_state(self):
        return "WI"

    def find_locates(self):
        CUTOFF = 40
        locates = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members Notified:"))
        if idx1 > -1:
            for line in lines[idx1+1:]:
                while line:
                    chunk = line[:CUTOFF]   # first N characters
                    m = self._re_termid.search(chunk)
                    if m:
                        code = m.group(1)
                        loc = locate.Locate(code.strip())
                        locates.append(loc)
                        line = line[CUTOFF:]    # get rest of line

        return locates

    def find_kind(self):
        if self.ticket.ticket_type.find("EMER") > -1:
            return "EMERGENCY"
        return "NORMAL"

    def post_process(self):
        if not self.ticket.ticket_type:
            # Unclear how the following line would ever be executed
            self.ticket.ticket_type = "-"
        BaseParser.post_process(self)

    def find_work_long(self):
        """
        Find the work longitude
        """
        m = self._re_work_longNW.search(self.data)
        if m:
            longNW = float(m.group(1))
        else:
            return 0.0
        m = self._re_work_longSE.search(self.data)
        if m:
            longSE = float(m.group(1))
            long = (longNW+longSE)/2.0
            if long < 0.0:
                return long
            else:
                return 0.0
        else:
            if longNW < 0.0:
                return longNW
            else:
                return 0.0

    def find_work_lat(self):
        """
        Find the work latitude
        """
        m = self._re_work_latNW.search(self.data)
        if m:
            latNW = float(m.group(1))
        else:
            return 0.0
        m = self._re_work_latSE.search(self.data)
        if m:
            latSE = float(m.group(1))
            lat = (latNW+latSE)/2.0
            if lat > 0.0:
                return lat
            else:
                return 0.0
        else:
            if latNW > 0.0:
                return latNW
            else:
                return 0.0

class SummaryParser(Albuquerque2.SummaryParser):
    re_termid = R("RECEIVING TERMINAL:\s+(\S+)")
    BLOCK_START = "(TELDIG SYSTEMS"

    def get_client_code(self, data):
        m = self.re_termid.search(data)
        if m:
            return m.group(1)
        else:
            return "FWI1"   # dummy
