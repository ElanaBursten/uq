# SouthCaliforniaCOX.py

from summaryparsers import BaseSummaryParser
from re_shortcut import R
import parsers
import summarydetail
import tools
import date
import SouthCalifornia
import locate

class Parser(SouthCalifornia.Parser):
    _re_term_id = R("(COX\S+)\s+\d+[A-Z]\s+USAS \d\d")
    def find_locates(self):
        m = self._re_term_id.search(self.data)
        if m:
            return [locate.Locate(m.group(1))]
        else:
            raise parsers.ParserError, "No term id found"


class SummaryParser(BaseSummaryParser):

    r_summary_date = R("since ([0-9/]+) ([0-9:]+) (AM|PM) \(Eastern")
    r_total = R("^Total Items Sent: (\d+)")
    r_client_code = R("KORTERRA AUDIT (\S+)")

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        for line in lines:
            if line and line[0] in "0123456789" and line.index("COX") >= 0:
                parts = line.split()
                try:
                    seq_number, ticket_number = parts[:2]
                except (IndexError, ValueError):
                    continue
                detail = summarydetail.SummaryDetail([
                         seq_number, ticket_number, "", "", ""])
                tickets.append(detail)

        return tickets

    def get_client_code(self, data):
        # audit covers both COX01 and COX04
        return "COX"

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            d = date.Date(tools.isodate(m.group(1) + " " + m.group(2) + " " + m.group(3)))
            if d.hours < 6:
                d.dec()
            elif d.hours >= 18:
                d.inc()
            d.hours = d.minutes = d.seconds = 0
            return d.isodate()
        else:
            return date.Date().isodate()    # take today, I guess

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            try:
                total = int(m.group(1))
            except ValueError:
                return 0
            else:
                return total
        else:
            return 0

    # override these methods in subclasses
    def has_header(self, data):
        return bool(self.r_summary_date.search(data))

    def has_footer(self, data):
        return bool(self.r_total.search(data)) \
            or data.index("No items were sent") >= 0


