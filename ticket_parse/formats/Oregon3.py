# Oregon3.py

import Oregon2
import Dallas4
import summarydetail

class Parser(Oregon2.Parser):
    s_work_description = "Location of Work:"

class SummaryParser(Dallas4.SummaryParser):
    CLIENT_CODE = 'LCC1'

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        while lines and not lines[0].startswith("--------"):
            del lines[0]
        del lines[0] # remove "----"

        for line in lines:
            if line.strip():
                parts = line.split()
                seq_num, ticket_number, code = parts[:3] # this is all we need
                detail = summarydetail.SummaryDetail(
                         [seq_num, ticket_number, "00:00", "", code])
                tickets.append(detail)
            else:
                break # done!

        return tickets
