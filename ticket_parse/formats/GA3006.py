# GA3006.py

import GA3003
from tools import R
from summaryparsers import BaseSummaryParser
import summarydetail
import date

class Parser(GA3003.Parser):
    # mostly the same as GA3003
    re_work_cross = R("^Near\s*:\s*Name: *(.*?) *$")

class SummaryParser(BaseSummaryParser):

    re_header = R("EOD ATTGA \S+ (\S+) (\d+) [0-9:]+ \S+ (\d+)")
    re_footer = R("Sent (\d+) messages")
    re_recog_data = R("^([0-9-]+) (\d+) \S+ \S+ \d+ [0-9:]+ CDT \d+")

    def has_header(self, data):
        return bool(self.re_header.search(data))

    def has_footer(self, data):
        return bool(self.re_footer.search(data))

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_header.search(data)
        if m:
            monthname, ds, ys = m.groups()[:3]
            m = date.get_month_number(monthname)
            s = "%04d-%02d-%02d 00:00:00" % (int(ys), m, int(ds))
            return s
        else:
            return date.Date().isodate() # last resort

    def get_client_code(self, data):
        return "ATTGA"

    def read(self, data):
        tickets = []
        for line in data.split("\n"):
            line = line.strip()
            m = self.re_recog_data.search(line)
            if m:
                ticket_number, seq = m.group(1), m.group(2)
                detail = summarydetail.SummaryDetail()
                detail.setdata([seq, ticket_number, "00:00", "", ""])
                tickets.append(detail)
        return tickets

