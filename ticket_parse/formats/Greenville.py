import SouthCarolina

class Parser(SouthCarolina.Parser):

    def post_process(self):
        SouthCarolina.Parser.post_process(self)

        # if 'no show' appears anywhere in the ticket, set ticket_type NO SHOW
        if self.data.lower().find('no show') > -1:
            self.ticket.ticket_type = 'NO SHOW'

class SummaryParser(SouthCarolina.SummaryParser):
    pass
