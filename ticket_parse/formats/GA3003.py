# GA3003.py

import GA3001
import string
import re
import tools

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | re.IGNORECASE | flags)

class Parser(GA3001.Parser):
    # meet/dig tickets

    call_center = 'GA3003'

    re_ticket_number = R("^Notice : +(\S+) +")
    re_update_of = R("^Old Tkt: +(\S+)")
    re_call_date = R("^Notice .*Date: ([0-9/]+) +Time: ([0-9:]+)")
    re_work_date = R("^(?:Meeting Date|Start date)\s*: +([0-9/]+) +Time: +([0-9:]+) +")
    re_work_state = R("^State *: ([A-Z][A-Z]) +")
    re_work_city = R("^State *: .*Place: *(.*?) *$")
    re_work_address_street = R("^Addr *:.*Name: +(.*)$")
    re_work_cross = R("^Cross1: Name: *(.*?) *$")
    re_work_type = R("^Work type\s*: +(.*?) *$")
    re_con_zip = R("Zip: (\d\d\d\d\d)\s*$")
    re_caller_phone = R("^Caller.*Phone +: *(.*?) *$")
    re_respond_date = R("^RespondBy : +([0-9/]+) +Time: +([0-9:]+) Duration")
    re_company = R("^Done for  : +(.*?) *$")
    re_duration = R("Duration\s*:\s*(.*?)\s*($|Priority)")

    def f_work_cross(self, match):
        return string.join(match.group(1).split(), " ")

    def find_work_description(self):
        lines = self.data.split('\n')
        idx = tools.findfirst(lines, lambda s: s.startswith("Locate:"))
        if idx > -1:
            desc_lines = [lines[idx]]
            idx += 1
            while re.search(r"^\s*:", lines[idx]):
                desc_lines.append(lines[idx].lstrip().lstrip(':')[2:])
                idx += 1
            desc = tools.singlespaced(string.join(desc_lines, ""))
            desc = desc[7:].strip() # strip leading "Locate:"
            return desc
        else:
            return ""


class SummaryParser(GA3001.SummaryParser):
    re_recog_header_1 = tools.R("\s+\*SUM\* UPCA")

