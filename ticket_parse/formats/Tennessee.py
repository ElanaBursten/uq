# Tennessee.py

import string
import re
from parsers import BaseParser
import tools
import locate

from summaryparsers import BaseSummaryParser
from re_shortcut import R as R_sum
import summarydetail

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | re.DOTALL | re.IGNORECASE | flags)

class Parser(BaseParser):

    re_ticket_number = R("TICKET\sNUMBER--\[(.*?)\]")
    re_update_of = R("OLD TICKET NUM-\[(.*?)\]")
    re_ticket_type = R("MESSAGE\sTYPE--+\[(.*?)\]")
    re_call_date = R("PREPARED-+\[(.*?)\].*?\[(.*?)\]")
    re_operator = R("PREPARED-+.*BY\s+\[(.*?)\]")
    re_transmit_date = R("\d+\s+(?:TNOCS|TOCS)\s+([0-9/]+)\s+([0-9:]+)")

    re_con_name = R("CONTRACTOR--\[(.*?)\]")
    re_caller = R("CALLER--\[(.*?)\]")
    re_con_address = R("ADDRESS-----\[(.*?)\]")
    re_con_city = R("^CITY--------\[(.*?)\]")
    re_con_state = R("CITY.*?STATE--\[(.*?)\]")
    re_con_zip = R("CITY.*?ZIP--\[(.*?)\]")
    re_caller_phone = R("CALL\sBACK.*?PHONE--\[(.*?)\]")
    re_caller_contact = R("^CONTACT-----\[(.*?)\]")
    re_caller_altphone = R("^CONTACT--.*?PHONE--\[(.*?)\]")
    re_caller_fax = R("CONTACT FAX-\[(.*?)\]")

    re_work_date = R("^WORK TO BEGIN--\[(.*?)\]\s+AT\s+\[(.*?)\]")
    re_work_state = R("^WORK TO BEGIN.*?STATE--\[(.*?)\]")
    re_work_county = R("COUNTY---\[(.*?)\]")
    re_work_city = R("COUNTY.*?PLACE--\[(.*?)\]")

    re_work_address_number = R("^ADDRESS--\[(.*?)\]")
    re_work_address_street = R(
     "^ADDRESS.*?STREET--\[(.*?)\]\[(.*?)\]\[(.*?)\]\[(.*?)\]")
    re_work_cross = R("NEAREST INTERSECTION-+\[(.*?)\]")

    re_work_type = R("WORK TYPE--\[(.*?)\]")
    re_company = R("DONE FOR--\[(.*?)\]")

    re_map_page = R("^GRIDS-----\s+\[(.*?)\s+\]")

    re_explosives = R("EXPLOSIVES--\[(.*?)\]")

    # these are used by methods, rather than being computed directly
    _re_work_lat = R("LATITUDE--\[(.*?)\]")
    _re_work_long = R("LONGITUDE--\[(.*?)\]")

    def f_company(self, match):
        # Strip out CR/LF from field
        company = match.group(1).replace('\n',' ')
        return company

    def f_call_date(self,match):
        time = match.group(2).split(':')
        if len(time) == 1:
            time = [time[0][:2],time[0][2:4],"00"]
        date_time = [match.group(1),]
        date_time.extend([string.join(time,':')])
        return tools.isodate(string.join(date_time, " "))

    f_work_date = f_call_date

    # note that transmit_date is different from the rest
    def f_transmit_date(self,match):
        return tools.isodate(string.join(match.groups(), " "))

    f_work_address_street = lambda s, m: \
     string.join(filter(None, m.groups()), " ")

    def find_work_description(self):    # or is this work_remarks?
        stub = []
        ok = 0
        for line in string.split(self.data, "\n"):
            line = string.strip(line)
            if ok:
                if line.startswith("[") and line.endswith("]"):
                    stub.append(line[1:-1])
                else:
                    break
            else:
                if line.startswith("LOCATION INFORMATION"):
                    ok = 1

        return string.join(map(string.strip, stub), " ")

    def find_locates(self):
        locates = []
        lines = string.split(self.data, "\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].upper().startswith("UTILITIES NOTIFIED"):
                idx = i
                break
        idx = idx + 3
        while idx < len(lines):
            line = lines[idx]
            if not line.strip():
                break   # empty lines end this
            if line.startswith((" ", "*")):
                code1 = line[2:2+11].strip()
                code2 = line[42:42+11].strip()
            else:
                # updated format, Mantis #3110
                code1 = line[:11].strip()
                code2 = "" #line[40:51].strip()
            if code1:
                locates.append(code1)
            if code2:
                locates.append(code2)

            idx = idx + 1

        # does KUB appear in the list?
        if "KUB" in locates:
            locates.remove("KUB")
            locates.extend(["KUBW", "KUBE", "KUBG", "KUBS"])

        return map(locate.Locate, locates)

    def find_work_lat(self):
        latitude = 0
        latitudes = self._re_work_lat.findall(self.data)
        if latitudes:
            try:
                latitude = tools.average([float(x) for x in latitudes if x != '0'])
            except:
                latitude = 0.0
        return latitude

    def find_work_long(self):
        longitude = 0.0
        longitudes = self._re_work_long.findall(self.data)
        if longitudes:
            try:
                longitude = tools.average([float(x) for x in longitudes if x != '0'])
            except:
                longitude = 0.0
        return longitude

class SummaryParser(BaseSummaryParser):

    S_HEADER = re.compile("From (TOCS|TNOCS)", re.IGNORECASE)
    S_FOOTER = "TOTAL FOR"
    S_AUDIT_DATE = R_sum("^AUDIT FOR ([0-9/]+)")
    S_CLIENT_CODE = R_sum("^FOR CODE\s+(.*)\s*$")

    def has_header(self, data):
        return self.S_HEADER.search(data) is not None

    def has_footer(self, data):
        return string.find(data.upper(), self.S_FOOTER) > -1

    def get_client_code(self, data):
        m = self.S_CLIENT_CODE.search(data.upper())
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = self.S_AUDIT_DATE.search(data.upper())
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = R_sum("^TOTAL FOR [A-Z0-9]+: (\d+)").search(data.upper())
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].upper().startswith("TYPE  SEQ#"):
                idx = i
                break
        if idx >= 0:
            idx = idx + 2   # skip a line
            while idx < len(lines):
                if not lines[idx].strip() \
                or lines[idx].upper().strip().startswith("FOR CODE"):
                    break   # empty line ends this session
                line = lines[idx][5:]
                parts = string.split(line)
                assert len(parts) >= 3, "Unparseable line: %s" % (parts,)
                number, ticket_number, status = parts[:3]
                detail = summarydetail.SummaryDetail(
                         [number, ticket_number, "00:00", "", ""])
                tickets.append(detail)

                idx = idx + 1

        return tickets

