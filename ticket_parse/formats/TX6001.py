# TX6001.py

import Harlingen
from re_shortcut import R

class Parser(Harlingen.Parser):
    call_center = 'TX6001'
    re_map_page = R("^Map Cross Reference : (.*?) *$")

    def is_fttp(self):
        #if self.__class__.__name__.startswith("TX6001"):
        if getattr(self.__class__, 'call_center', '') == 'TX6001':
            return self.ticket.company.find('VERIZON OSP') >= 0
        else:
            return False

class SummaryParser(Harlingen.SummaryParser):
    pass
