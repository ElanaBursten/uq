import string
import Tennessee
from re_shortcut import R

class Parser(Tennessee.Parser):
    MAX_LOCATES = 60
    re_transmit_date = R("\d+ +OKOCS +([0-9/]+) +([0-9:]+)")

class SummaryParser(Tennessee.SummaryParser):

    def has_header(self, data):
        return string.find(data, "FROM OKOCS") > -1
