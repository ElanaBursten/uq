# WestVirginia.py

import re
#
from parsers import BaseParser
from re_shortcut import R
import date
import locate
import tools

from summaryparsers import BaseSummaryParser
import summarydetail

class Parser(BaseParser):

    MAX_LOCATES = 50
    CALL_CENTER = 'FWP1' # see post_process

    # Note: Many lines in this ticket start with a space.

    re_ticket_number = R("^ NOTICE: +(\d*)")
    re_ticket_type = R("^ NOTICE:.*? -->(.*?) *(Lead|$)")
    re_work_county = R("^ County: +(.*?) +Town")
    re_work_city = R("^ County.*?Town: +(.*)$")
    re_work_address_street = R("^ Street: +(.*?) *$")
    re_work_cross = R("^ Near Inter: +(.*?) *$")
    re_company = R("^ Work for: +(.*?) *$")
    re_work_type = R("^ Type of Work: +(.*?) *$")
    re_work_date = R("^ Start Date: +(\S*) +Time: +(\d*)")

    re_con_name = R("^ Contractor: +(.*?) *$")
    re_caller_phone = R("^ Phone: +(.*?) *$")
    re_con_address = R("^ Address: +(.*?);")
    re_con_city = R("^ Address.*?; (.*?) \S\S \d\d\d\d\d *$")
    re_con_state = R("^ Address.*? (\S\S) \d\d\d\d\d *$")
    re_con_zip = R("^ Address.* (\d\d\d\d\d) *$")
    re_caller_contact = R("^ Contact: +(.*?) +Contact Phone")
    re_caller_fax = R("Fax: +(.*?) *$")
    re_caller_altphone = R("Alt Phone: + (.*?) *$")
    re_caller = R("^ Caller: +(.*?) +Prepared")
    re_call_date = R("^ Caller.*?Prepared: +(\S*?)@(\d*) *$")
    re_transmit_date = re_call_date

    def find_locates(self):
        lines = self.data.split("\n")
        splitter = re.compile("[+=]")

        # find the first line after the @-section
        a_found = 0
        for i in range(len(lines)):
            line = lines[i]
            if a_found:
                if not line.strip().startswith("@"):
                    break
            else:
                if line.strip().startswith("@"):
                    a_found = 1

        locates = []

        idx = i+1
        for i in range(idx, len(lines)):
            #print i, lines[i]
            if not lines[i].strip():
                break
            line = lines[i][1:] # skip first space
            while line:
                chunk, line = line[:20], line[20:]
                #print repr(chunk)
                if chunk.strip():
                    name = splitter.split(chunk)[0].strip()
                    locates.append(name)

        return [locate.Locate(name) for name in locates]

    find_work_state = lambda s: "WV"

    def f_work_date(self, match):
        """ Date is in format like 19-MAR-02, and needs to be converted. """
        try:
            d, t = match.group(1), match.group(2)
            if d == "UNKNOWN" or t == "":
                return ""   # no date here, folks
        except:
            return ""   # will be fixed by post_process
        else:
            dd, dm, dy = d.split("-")
            try:
                dm = date.get_month_number(dm) # 1, 2, ..., 12
            except IndexError:
                raise ParserError, "Unknown month '%s'" % (dm)
            newdate = "20%s-%02d-%s" % (dy, dm, dd)
            newtime = t[:2] + ":" + t[2:] + ":00"
            return newdate + " " + newtime
    f_call_date = f_work_date
    f_transmit_date = f_call_date

    def find_work_remarks(self):
        remarks = ""
        for line in self.data.split("\n"):
            line = line.strip()
            if remarks:
                if not line:
                    break   # end of Remarks section
                else:
                    remarks = remarks + line + " "
            else:
                if line.startswith("Remarks:"):
                    remarks = "!"
        return remarks[1:-1]

    def post_process(self):
        # nasty bit of hackery... the parser should not have to know about due
        # dates, or even about call centers
        if not self.ticket.work_date:
            d = date.Date(self.ticket.call_date)
            import duedatecatalog
            dc = duedatecatalog.DueDateCatalog(self.CALL_CENTER, [])
            d = dc.inc_n_days(d, 2)
            self.ticket.work_date = d.isodate()

        BaseParser.post_process(self)

class SummaryParser(BaseSummaryParser):

    def has_header(self, data):
        return "Audit for CDC of" in data

    def has_footer(self, data):
        rfooter = R("^Total number of Messages for \S+ +\d+ *$")
        return not not rfooter.search(data)

    def read(self, data):
        tickets = []
        for line in data.split("\n"):
            line = line.strip()
            if line and line[0] in "0123456789":
                parts = line.split()
                while parts:
                    a, b = parts[:2]
                    del parts[:2]
                    ticket_number = a[:-2]
                    type_code = a[-2:]
                    if type_code == "ds":
                        continue    # I think this one doesn't count
                    item_number = b
                    detail = summarydetail.SummaryDetail(
                             [item_number, ticket_number, "00:00", "", type_code])
                    tickets.append(detail)
        return tickets

    def get_client_code(self, data):
        m = R("^SEQUENCE NUMBER \d+ +CDC = (\S+)").search(data)
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = R("^Audit for CDC of \S+ +Last Audit was on (\d+)").search(data)
        d = m and m.group(1) or ""
        if 0 and d:
            # XXX let's ignore that date for now... it tells us when the *last*
            # audit was, not when this one is sent
            d = "20%s-%s-%s 00:00:00" % (d[:2], d[2:4], d[4:])
        else:
            now = date.Date()
            if now.hours < 8:
                now.dec()   # if it arrived before 8 AM, assume the previous day
            now.resettime() # I think
            d = now.isodate()

        return d

    def get_expected_tickets(self, data):
        m = R("Total number of Messages for \S+ +(\d+)").search(data)
        xt = m and int(m.group(1)) or 0
        return xt
