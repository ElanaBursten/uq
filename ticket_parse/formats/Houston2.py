# Houston2.py
import string
import Houston1
from summaryparsers import BaseSummaryParser
from re_shortcut import R
import summarydetail
import tools
from date import Date

class Parser(Houston1.Parser):
    pass

class SummaryParser(BaseSummaryParser):
    # NOT the same as Houston1.

    r_summary_date = R("Page \d+ - ([0-9/]+)")
    r_total = R("Total Number of Tickets: (\d+)")
    default_client_code = 'FHL2'

    def read(self, data):
        """ Read and return a list of ticket data. """
        L = len("17102074  537      ")
        tickets = []
        collecting = 0
        lines = string.split(data, "\n")
        for line in lines:
            if line.startswith("---") and not collecting:
                collecting = 1  # begin collecting
            elif (not line.strip()) and collecting:
                collecting = 0  # end collecting
            elif collecting:
                while line.strip():
                    chunk, line = line[:L], line[L:] # get first L characters
                    ticket_number = chunk[:8]
                    if len(ticket_number) != 8:
                        break
                    seq_number = chunk[10:14].strip()
                    detail = summarydetail.SummaryDetail(
                             [seq_number, ticket_number, "", "", ""])
                    tickets.append(detail)

        return tickets

    def get_client_code(self, data):
        return self.default_client_code
        # there is no client code on the summary!

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            d = tools.isodate(m.group(1))
            return Date(d).isodate()
        else:
            return Date().isodate()    # take today, I guess

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            try:
                total = int(m.group(1))
            except ValueError:
                return 0
            else:
                return total
        else:
            return 0

    # override these methods in subclasses
    def has_header(self, data):
        return not not self.r_summary_date.search(data)

    def has_footer(self, data):
        return not not self.r_total.search(data)
