# VUPSNewFMW3.py

import _VUPSNew
import Richmond3
from tools import R

class Parser(_VUPSNew.Parser):
    PARSE_GRIDS = True

    # parse due date; same as VUPSNewFDE2
    re_due_date = R("^Due By\s+Date:\s+([0-9/]+)\s+Time:\s+([0-9:]+) (AM|PM)")
    re_legal_due_date = re_due_date
    f_due_date = _VUPSNew.Parser.f_transmit_date
    f_legal_due_date = f_due_date

    def post_process(self):
        _VUPSNew.Parser.post_process(self)
        if self.ticket.update_of and 'UPDATE' not in self.ticket.kind:
            self.ticket.kind += ' UPDATE'

class SummaryParser(Richmond3.SummaryParser):
    pass
