# dbinterface.py
# Interface module for databases. 
#
# This module provides a class (DBInterface) and various useful helper
# functions for manipulating data in SQL Server databases.
# todo(hans): dbinterface_ado.py relies on this, should eventually be
# refactored.

import inspect
import os
import string
import sys
import time
import types
#
import config
import tools

from dbinterface import RunSQLException, DBException, DBFatalException, \
                        TimeoutError

class DBInterface:
    """ Generic interface class to talk to a database. """

    def __init__(self):
        self._lastresult = ""
        # Last unparsed XML retrieved by runsql.
        self.config = config.getConfiguration()

        # timer stuff
        self._start = 0
        self._stop = 0
        self.timed_events = []


    ###
    ### built-in timer

    def starttimer(self):
        self._start = time.time()
        self._stop = 0

    def stoptimer(self):
        assert self._start, "Timer stopped but not started"
        self._stop = time.time()

    def register_timer(self, method, info):
        frames = inspect.stack()[1:]
        frame_info = [f[1:-1] for f in frames]
        t = (self._start, self._stop, method, info, frame_info)
        self.timed_events.append(t)

    ###
    ### a simple "report" function

    # NOTE: This code may better be moved to a separate module or class.

    def simple_profiler(self, f=sys.stdout):
        for i in range(len(self.timed_events)):
            event = self.timed_events[i]
            start, stop, method, info, frame_info = event
            s = "Event %d: started %s, stopped %s, duration %.2f sec" % (
             i+1,
             "%02d:%02d:%02d" % (time.localtime(start)[3:6]),
             "%02d:%02d:%02d" % (time.localtime(stop)[3:6]),
             stop - start,
            )
            print >> f, s
            print >> f, "Type:", method
            print >> f, "Called with:", info[:100],
            print >> f, len(info) > 100 and "..." or ""
            print >> f, "Called by:"
            for frame in frame_info:
                path, fname = os.path.split(frame[0])
                print >> f, " - %s, line %s: %s" % (fname, frame[1], frame[2])
            print >> f
        print >> f, "Total time spent in database: %.2f seconds" % (
         self.time_elapsed())

    def simple_profiler_csv(self, filename):
        """ Write a tab-delimited file with profiler results. """
        f = open(filename, "wb")
        for i in range(len(self.timed_events)):
            event = self.timed_events[i]
            start, stop, method, info, frame_info = event
            print >> f, "%02d:%02d:%02d" % (time.localtime(start)[3:6]), "\t",
            print >> f, "%02d:%02d:%02d" % (time.localtime(stop)[3:6]), "\t",
            print >> f, stop - start, "\t",
            print >> f, info, "\t",
            callers = [frame[2] for frame in frame_info]
            print >> f, string.join(callers, ", ")
        f.close()
        print "CSV-file", filename, "written."

    def time_elapsed(self):
        """ Compute the time elapsed for all the events that are currently in
            the timed_events list. """
        elapsed = reduce(lambda a, b: a+b,
         [e[1] - e[0] for e in self.timed_events], 0)
        return elapsed




