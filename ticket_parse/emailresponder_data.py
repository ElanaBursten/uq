# emailresponder_data.py

import abstractresponderdata
import callcenters
import date
import traceback

class EmailResponderError(Exception):
    pass

class EmailResponderData(abstractresponderdata.AbstractResponderData):
    client_based_login = 0

    acceptable_codes = ["invalid id/cdc combination",
                        "invalid serial number",
                        "member not on ticket",
                        "invalid response code",
                        "response past due",
                        "response posted"]

    codes_ok = ["ok", "250 ok", "250 OK"]
    codes_reprocess = []
    codes_error = ["invalid id/cdc combination", "invalid serial number",
                   "member not on ticket", "invalid response code"]

    def response_is_success(self, row, resp):
        raise NotImplementedError

    def make_email_line(self, names, tdb, version):
        raise NotImplementedError

    def filter_versions(self, versions):
        return versions


def getresponderdata(config, call_center, raw_responderdata):
    """ Try to get and instantiate an XMLHTTPResponderData class for the given
        call center.  If not found, raise an XMLHTTPResponderDataError.  Looks
        for the given class in two ways: in the namespace of this module, and
        in the 'callcenters' package.
        NOTE: The first lookup method is obsolete now, but I'll leave it in
        because it's useful for testing.
    """
    prefix = call_center
    if prefix[0] in "01234567890":
        prefix = "c" + prefix
    klassname = prefix + "EmailResponderData"
    try:
        klass = globals()[klassname]
    except KeyError:
        try:
            klass = callcenters.get_emailresponderdata(prefix)
        except:
            traceback.print_exc()
            raise EmailResponderError, "Could not find EmailResponderData class for %s" % (call_center,)

    rd = klass()

    # inject data provided or from configuration
    rd.config = config
    if not raw_responderdata:
        # only for testing purposes
        try:
            raw_responderdata = config.emailresponders[call_center]
        except KeyError:
            raise KeyError, "No responder data for " + call_center

    rd.response_codes = raw_responderdata["mappings"]
    for attr in ('name', 'tech_code', 'resp_email', 'mappings', 'sender',
                 'subject', 'translations', 'skip', 'all_versions', 'clients',
                 'send_emergencies', 'ack_dir', 'ack_proc_dir', 'accounts'):
        setattr(rd, attr, raw_responderdata[attr])

    try:
        rd.client_based_login = int(raw_responderdata["clientbased"])
        # do client-based XMLHTTP responders even exist?
    except (ValueError, KeyError):
        rd.client_based_login = 0

    # override 'skip' to use values in ResponderConfigData
    rd.skip = config.responderconfigdata.get_skip_data(call_center)
    rd.status_code_skip = config.responderconfigdata.get_status_code_skip_data(call_center)

    return rd
