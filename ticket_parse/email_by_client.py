# email_by_client.py
# Interface to the notification_email_by_client table.

class EmailByClient:

    def __init__(self, tdb):
        self.tdb = tdb
        self.load()

    def load(self):
        self.data = {}
        sql = """
         select n.*, c.oc_code, c.call_center
         from notification_email_by_client n
         inner join client c on n.client_id = c.client_id
        """
        data = self.tdb.runsql_result(sql)
        for row in data:
            t = (row['notification_type'], row['call_center'], row['oc_code'])
            try:
                lst = self.data[t]
            except KeyError:
                lst = self.data[t] = []
            email = row['email_address']

            # allow multiple addresses in one field
            if ';' in email:
                emails = email.split(';')
            else:
                emails = [email]

            lst.extend(emails)

    def get_emails(self, call_center, term_id, type):
        t = (type, call_center, term_id)
        try:
            return self.data[t]
        except:
            return []


