# polygon_routing.py

import os
import re
#
import config
import datadir
import polygon
import tools

re_mif_polygon_coord = re.compile("^(-?\d+\.\d+)\s+(-?\d+\.\d+)\s*$",
                       re.DOTALL|re.MULTILINE)

def read_mif(filename):
    data = open(filename, 'r').read()
    chunks = data.split('Region')
    del chunks[0]

    polygons = []

    for chunk in chunks:
        lines = chunk.split('\n')
        try:
            num_angles = int(lines[1])
        except:
            continue

        coords = []
        for match in re_mif_polygon_coord.finditer(chunk):
            long, lat = map(float, match.groups())
            coords.append((long, lat))
        if len(coords) == 0:
            continue
        assert len(coords) >= num_angles, \
         "Error reading polygon data: %d coordinates found, %d expected" % (
         len(coords), num_angles)

        polygons.append(coords)

    return polygons

def read_table(filename):
    """ Read the MapInfo table that contains polygons.  The "table" should
        have been exported in a tab-delimited format, without field names.
        The first field should be the area_id (numerical).  The second field
        can be a description or the name of the area.  Other fields don't
        matter for the routing.

        Returns a list of pairs (area_id, description) for each record, in the
        order they were found.  (This last point is important.)
    """
    area_ids = []
    lines = open(filename, 'r').readlines()
    for line in lines:
        fields = line.split('\t')
        fields = map(tools.unquote, fields)
        area_ids.append(fields[0:2])

    return area_ids

class RoutingPolygon(tools.Struct):
    __fields__ = ["coords", "area_id", "name"]

def make_routing_list(polygons, area_ids):
    if len(polygons) != len(area_ids):
        raise ValueError, "Polygon and area data must have the same length"

    routingpolygons = []
    for coords, (area_id, name) in zip(polygons, area_ids):
        rp = RoutingPolygon(coords=coords, area_id=area_id, name=name)
        routingpolygons.append(rp)

    return routingpolygons

class PolygonRoutingList:

    def __init__(self, polygons, area_ids):
        if isinstance(polygons, str):
            polygons = read_mif(datadir.datadir.get_filename(polygons))
        if isinstance(area_ids, str):
            area_ids = read_table(datadir.datadir.get_filename(area_ids))
        self.routinglist = make_routing_list(polygons, area_ids)

    def find_area(self, (long, lat)):
        for rp in self.routinglist:
            if polygon.inside((long, lat), rp.coords):
                return rp.area_id
        return None

class PolygonRoutingCache:

    def __init__(self):
        self.data = {}

    def add(self, call_center, routinglist):
        self.data[call_center] = routinglist

    def get(self, call_center):
        try:
            return self.data[call_center]
        except KeyError:
            try:
                self.load(call_center)
                return self.data[call_center]
            except:
                return None

    def __getitem__(self, call_center):
        return self.get(call_center)

    def __setitem__(self, call_center, routinglist):
        self.add(call_center, routinglist)

    def load(self, call_center):
        cfg = config.getConfiguration()

        # get files from polygon_dir
        mif_filename = os.path.join(cfg.polygon_dir, call_center+".MIF")
        txt_filename = os.path.join(cfg.polygon_dir, call_center+".txt")

        # create routinglist; datadir will be used here
        routinglist = PolygonRoutingList(mif_filename, txt_filename)

        self.add(call_center, routinglist)

    def find_area(self, call_center, (long, lat)):
        routinglist = self.get(call_center)
        if routinglist:
            return routinglist.find_area((long, lat))
        else:
            return None


if __name__ == "__main__":

    print "Testing polygon files (does not affect system)..."
    import config, string, sets, sys

    cfg = config.getConfiguration()
    if sys.argv[1:]:
        cfg.polygon_dir = sys.argv[1]

    # what files do we have?
    filenames = map(string.lower, os.listdir(cfg.polygon_dir))
    filenames = [fn for fn in filenames
                 if fn.endswith(".txt") or fn.endswith(".mif")]
    d = sets.Set()
    for fn in filenames:
        call_center = fn[:-4]
        d.add(call_center)

    prc = PolygonRoutingCache()
    for call_center in d:
        print call_center, prc.get(call_center)

