# receiver.py
# UtiliQuest email receiver

from __future__ import with_statement
import cStringIO
import email
import getopt
import glob
import os
import poplib
import random
import socket
import string
import sys
import time
import xml.etree.ElementTree as ET
#
import attachment
import config
import datadir
import date
import emailtools
import errorhandling2
import errorhandling_special
import et_tools
import filetools
import listener
import program_status
import tools
import version
import xmlconfigreader

class Error(Exception):
    pass
    
class HeaderDelimeterError(Error):
    pass

__usage__ = """\
receiver.py [options]

Receives tickets through email.

Options:
    -1          Only download one email message.
    -n N        Download at most N email messages.
    -c cfgfile  Specify an alternate configuration file.
    -C/--check  Validate XML.
    -d          Download messages, but don't delete them from the server.
    -h          Leave email headers (default is to strip them).
    -r filename Store raw emails, separated by ^L, in <filename>.
    -w          Wait when done processing. [for testing purposes]
    -v          (verbose) Print configuration info, then quit.
"""

PROCESS_NAME = "receiver"
POP_TIMEOUT = 30

socket.setdefaulttimeout(POP_TIMEOUT)

def U(x):
    if isinstance(x, unicode):
        return x.encode("utf-8")
    else:
        return x

def today():
    """ The current date, in the string representation "YYYY-MM-DD". """
    return date.today().isodate()[:10]

class ReceiverConfiguration:
    """
    Account fields:
        "type" - 'email' or 'db'
        "destination" - From process's "incoming"
        "call_center" - From process's "format"
        "attachment_dir" - From process's "attachment_dir". This is where they
            would be stored.
        "server"
        "username"
        "password"
        "full" - If set, extract both attachment and text
        "store_attachments" - If set, store attachments
        "ignore_attachments" - Ignore attachments
        "file_ext" - If set, attachments with filenames ending in this
            extension will be considered tickets, like they were stored
            in the email body (used for e.g. LAM01)
        "aggregate_file" - If set, stores raw emails in the file
            specified here, but only if the file's size is
            < MAX_AGG_FILE_SIZE.
        "use_ssl" - If set, use POP3_SSL
        "port"
    """

    def __init__(self, configuration, call_center, accounts=[], db_accounts=[]):
        self.config = configuration

        self.accounts = accounts
        self.db_accounts = db_accounts

        self.smtp_accs = self.config.smtp_accs
        if len(self.smtp_accs):
            self.smtpinfo = self.smtp_accs[0]
        else:
            self.smtpinfo = None

        self.logdir = self.config.logdir
        self.attachment_dir = self.config.attachment_dir
        self.admins = self.config.admins

class ReceiverOptions:
    restriction = 0
    nodelete = 0
    strip_headers = 1
    raw_file = ""
    verbose = 1
    wait = 0
    validate_xml = 0
    print_config = 0

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

class EmailReceiver:
    """ Connect to an email server, fetch emails. """

    def __init__(self, account, log):
        self.account = account # dict with POP3 account info
        self.log = log
        self.conn = None

    def connect(self):
        self.conn = self._get_pop3_conn(self.account)
        return self.conn

    def disconnect(self):
        self.conn.quit()

    # todo(dan) Can we start removing some of this old code?
    def get_emails(self, n=1):
        """ Get up to <n> emails and return them as a list. """
        # NOTE: no deleting or disconnecting is done here.
        raw_messages = []

        resp, msgs, _ = self.conn.list()
        num_msg = len(msgs)
        print num_msg, "messages found"
        for i in range(min(num_msg, n)):
            print "  Downloading message", i+1, "...",
            resp, lines, _ = self.conn.retr(i+1)
            print "OK (%d lines)" % (len(lines))
            raw_messages.append(lines)

            # listen for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break
                # this only breaks out of the for loop; we need another
                # check elsewhere

        return raw_messages

    def get_next_email(self):
        # An exception will be raised if uidl() fails
        resp, msgs, _ = self.conn.uidl()
        num_msgs = len(msgs)
        print num_msgs, "messages found"

        uid_skip_list = self.account.get('uid_skip_list', [])
        for i in range(num_msgs):
            msg = msgs[i].split()
            if len(msg) <> 2:
                raise Exception('Invalid POP3 unique-id listing: ' + msgs[i])
            # todo(dan) Should we check if the message # is valid? And can it
            # contain separators (e.g. 1,000 or 1.000)?
            msg_num = msg[0]
            msg_id = msg[1]                
            if msg_id in uid_skip_list:
                print "  Skipping message", msg_num
            else:
                print "  Downloading message", msg_num, "...",
                # An exception will be raised if retr() fails
                resp, lines, _ = self.conn.retr(msg_num)
                print "OK (%d lines)" % (len(lines))
                return msg_num, msg_id, lines
        return '', '', []

    def delete(self, n):
        self.conn.dele(n)

    def _get_pop3_conn(self, account):
        try:
            if account['use_ssl']:
                mbox = poplib.POP3_SSL(account["server"], account["port"])
            else:
                mbox = poplib.POP3(account["server"])
            mbox.sock.settimeout(POP_TIMEOUT)
            mbox.user(account["username"])
            mbox.pass_(account["password"])
            print mbox.getwelcome()
            return mbox
        except socket.timeout:
            ep = errorhandling2.errorpacket()
            ep.add(info="Mailbox %s on %s cannot be reached (timeout)" % (
             account["username"], account["server"]))
            self.log.log(ep, send=0, write=1, dump=1)
            return None
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Mailbox %s on %s cannot be reached" % (
             account["username"], account["server"]))
            self.log.log(ep, send=1, write=1, dump=1)
            return None

class EmailProcessor:
    """ Process raw emails; return raw tickets and attachments. """

    def __init__(self, log):
        self.log = log

    def get_parts(self, lines, sep_index, full=0, ignore_attachments=0,
                  file_ext="", strip_headers=1):
        """ Get the parts from an email message (given as a list of lines, the
            way poplib returns it).  sep_index is the index of the line that
            separates headers from body.  If 'full' is set, we extract
            both attachments and plain text (off by default).
            If 'file_ext' is set, then attachments whose filenames have the
            specified extension (e.g. ".xml"), are decoded and treated as text
            rather than attachments. [New in Mantis #2787]
            Return a list of tuples (type, text, filename) where type can be
            'attachment' or 'body'; text is the raw data; filename will only
            be filled if it's an attachment, otherwise it's None.
            A 'body' part may have headers, an attachment never has this.
        """
        parts = []
        raw = string.join(lines, "\n")  # get the original message text
        attachments = 0
        msg = email.message_from_string(raw)
        all_parts = [p for p in msg.walk()]
        headers = tools.parse_headers(lines)

        if (len(all_parts) >= 2
        and all_parts[0].get_content_type() == 'multipart/mixed'
        and all_parts[1].get_content_type() == 'text/plain'
        and ignore_attachments
        and not full):
            # this is a special case, currently only for FDX1.  all_parts[0]
            # contains *the whole mail*, all_parts[1:] contain the parts.
            # We're only interested in the 'real' text part, which should be in
            # all_parts[1].
            # Note that it only works if ignore_attachments=1 and full=0.
            self.log.log_event("Ignoring attachments...")
            attachments += 1
            data = all_parts[1].get_payload(decode=1)
            parts.append(('text', data, None))
        else:
            for part in all_parts:
                if part.get_content_type() == 'application/octet-stream' \
                or part.get_content_type() == 'application/pdf':
                    attachments += 1
                    data = part.get_payload(decode=1)
                    filename = part.get_filename() \
                            or tools.get_attachment_filename(part.as_string())
                            # HACK to work around bug (?) in stdlib
                    if file_ext and (
                    (filename.lower().endswith(file_ext.lower())) or
                    (file_ext.lower().endswith('xml') and '<?xml' in data)):
                        self.log.log_event("Attachment treated as ticket: %s"
                         % filename)
                        parts.append(('text', data, None))
                    else:
                        parts.append(('attachment', data, filename))
                elif part.get_content_type() == 'text/plain':
                    if full:
                        data = part.get_payload(decode=1)
                        parts.append(('text', data, None))

        # remove empty parts
        parts = [p for p in parts if p[1].strip()]

        if not attachments:
            # do headers indicate that this mail is in quoted-printable?
            qp = emailtools.is_quoted_printable(lines[:sep_index])
            if not qp and emailtools.body_is_quoted_printable(raw):
                # if the headers don't indicate quoted-printable, but the body
                # does, then decode it as well
                rawbody = string.join(lines[sep_index:], "\n")
                rawbody = rawbody.decode('quoted-printable')
                rawbody = rawbody.replace("\r", "")  # fix newline problem
                lines = lines[:sep_index] + rawbody.split("\n")
                lines = emailtools.remove_mark_lines(lines)
            if strip_headers:
                body = lines[sep_index:]
            else:
                body = lines[:]
            if qp:
                blurb = string.join(body, "\n")
                qp_blurb = blurb.decode('quoted-printable')
                body = qp_blurb.split('\n')

            data = string.join(body, "\n")
            data = data.replace("\r", "")  # \r should never occur in image
            parts.append(('body', data, None))

        # find parts that are in base64 and decode them
        for i, (ptype, data, name) in enumerate(parts):
            if ptype != 'attachment':
                if headers.get('Content-Transfer-Encoding') == 'base64':
                    data = tools.safe_convert_base64(data) or data
                    parts[i] = (ptype, data, name)

        return parts


class Receiver:
    """ UtiliQuest email receiver. Checks for new email and downloads the
        emails, of which the bodies are supposed to be raw tickets. """

    def __init__(self, config, options=None):
        self.config = config
        self.options = options or ReceiverOptions()

        if self.options.validate_xml:
            et_tools.validate(self.config._xml)

        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtpinfo, me="TicketReceiver",
                   cc_emails={}, admin_emails=self.config.admins,
                   subject="Ticket Receiver Error Notification")
        # the receiver doesn't care about call centers, so it doesn't need to
        # use the Emails class
        self.log.defer_mail = 1
        self.log.verbose = self.options.verbose

        self.count = 0  # number of mails retrieved
        self._email_receiver = None

        self.prepare()

        # start logging and write program status
        tools.log_info(self.log, "receiver.py")
        program_status.write_status(PROCESS_NAME + ":" +
         datadir.datadir.get_id())

        for account in self.config.accounts:  
            account['uid_skip_list'] = []
        self.parsable_message_num = ''
        
        for db_account in self.config.db_accounts:  
            db_account['last_item_id_read'] = '-999'

    def prepare(self):
        pass # can be overridden in tests

    def check_directories(self, account, output_subdir=''):
        # todo(dan) Should this just check 'attachment_dir'?
        # make sure that relevant directories exist
        if account.get('store_attachments', False) or \
         (account.get('type', '') == 'db' and account.get('attachment_dir', '')):
            out_dir = os.path.join(account['attachment_dir'], output_subdir)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)

    def clean_up_output_dirs(self, account):
        if account['destination'] and config.RECEIVER_OUTPUT_SUBDIR:
            for filename in glob.glob(os.path.join(account['destination'],
             config.RECEIVER_OUTPUT_SUBDIR, "*%s*.txt" % (account['call_center']))):
                filetools.remove_file(filename)
        if account['attachment_dir'] and config.RECEIVER_OUTPUT_SUBDIR:
            for filename in glob.glob(os.path.join(account['attachment_dir'],
             config.RECEIVER_OUTPUT_SUBDIR, "*%s*.txt" % (account['call_center']))):
                filetools.remove_file(filename)

    def check_account(self, account):
        self.check_directories(account)
        recv = EmailReceiver(account, self.log)
        mbox = recv.connect()
        if mbox is None:
            return

        max_emails = self.options.restriction or 100
        messages = recv.get_emails(max_emails)
        for msg_no, raw_msg in enumerate(messages, 1):
            can_delete = self.process_message(raw_msg, account)
            if can_delete and not self.options.nodelete:
                recv.delete(msg_no)
                print "message", msg_no, "deleted from server"
            else:
                print "message", msg_no, "left on server"

            # listen for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

        recv.disconnect()

    def process_next_email_in_account(self, account):
        """ Returns an empty list, if unable to connect to the POP3 server, or
            if no new messages were found. Returns a non-empty list of parsable filenames, if a new message was read, and parsable parts were found.
            Returns 0, if a new message was read, but skipped, because no
            parsable parts were found. """

        self.parsable_message_num = ''
        parsable_filenames = []
        self.clean_up_output_dirs(account)
        self.check_directories(account, config.RECEIVER_OUTPUT_SUBDIR)

        # todo(dan) Is it ok to just abandon the connection, if not committing?
        self._email_receiver = EmailReceiver(account, self.log)
        if self._email_receiver.connect():
            msg_num, msg_id, message = self._email_receiver.get_next_email()
            if message:
                parsable_filenames = self.process_message(message,
                 account, config.RECEIVER_OUTPUT_SUBDIR)
                if parsable_filenames:
                    self.parsable_message_num = msg_num
                elif parsable_filenames == 0:
                    msg = "Parser for call center '%s' halted. Email " + \
                     "received with no header delimeter (message uid = '%s'," + \
                     " server = '%s', username = '%s')"
                    # todo(dan) If this happens, the same error notice will
                    # probably be sent continuously, until the problem's fixed.
                    raise HeaderDelimeterError(msg % (
                     account.get('call_center', ''), msg_id,
                     account.get('server', ''), account.get('username', '')))
                else:
                    account['uid_skip_list'].append(msg_id)
                    msg = "No parsable text found. Leaving message in " + \
                     "mailbox (message uid = '%s', server = '%s', username " + \
                     "= '%s')" 
                    self.log.log_event(msg % (msg_id, account.get('server', ''),
                     account.get('username', '')))
                    return 0
        return parsable_filenames

    def get_next_filename_list(self):
        """ Attempts to process or skip 1 email message. Does not delete
            messages, or commit POP transactions. Checks available accounts,
            until 1 message is successfully processed, 1 message is skipped, or
            all accounts have been checked one time.

            Returns a non-empty list of parsable filenames, if processing was
            successful. Returns 0, if a message was skipped. Returns an empty
            list, if all accounts were tried without successful processing or
            skipping. """
            
        parsable_filenames = []
        max_acct = len(self.config.accounts) - 1
        if max_acct < 0:
            next_acct = -1
        else:
            next_acct = random.randint(0, max_acct)
            
        for _ in range(max_acct + 1):
            if next_acct < max_acct:
                next_acct += 1
            else:
                next_acct = 0
            account = self.config.accounts[next_acct]

            msg = "Checking %s@%s..." % (account["username"], account["server"])
            self.log.log_event(msg)
            
            parsable_filenames = self.process_next_email_in_account(account)

            if parsable_filenames:
                print "A message was successfully processed"
                break

            if parsable_filenames == 0:
                print "A message was skipped"
                break
                    
            # listen for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break
                
        self.log.force_send()
        if self.options.wait:
            raw_input("[-w option specified] Press Enter to finish.")
        return parsable_filenames

    def confirm_filename_list(self):
        assert self._email_receiver, "Email receiver does not exist"
        assert self.parsable_message_num, "Parsable message number is blank"
        self._email_receiver.delete(self.parsable_message_num)
        # todo(dan) Should we log a message if this fails? No exception will
        # be raised.
        self._email_receiver.disconnect()

    def check(self):
        """ Check all available accounts. """
        for account in self.config.accounts:
            msg = "Checking %s@%s..." % (account["username"], account["server"])
            self.log.log_event(msg)
            self.check_account(account)
            # listen for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break
        print "A total of", self.count, "mails was retrieved."
        self.log.force_send()

        if self.options.wait:
            raw_input("[-w option specified] Press Enter to finish.")

    def _write_raw_data(self, filename, lines, account):
        agg = filetools.AggregateFile(filename)
        data = string.join(lines, '\n') + '\n'
        ok = agg.write(data, sep=chr(12)) # ^L separator
        if ok:
            print "Raw email appended to", filename

    def process_message(self, lines, account, output_subdir=''):
        """ Process an email message.  <lines> is a list of lines of the
            message, as returned by POP3.retr().  <account> is a dict
            containing the POP3 account info.
            
            Returns 0, if no header delimeter was found. Otherwise returns a
            list of parsable filenames.
        """
        parsable_filenames = []
        proc = EmailProcessor(self.log)
        destination = os.path.join(account["destination"], output_subdir)
        if not os.path.exists(destination):
            os.makedirs(destination)

        has_attachments = 0

        # write the raw data to a file for later inspection
        if self.options.raw_file:
            self._write_raw_data(self.options.raw_file, lines, account)
        if account.get('aggregate_file', ''):
            self._write_raw_data(account['aggregate_file'], lines, account)

        # a message always needs a header, attachments or not
        idx = emailtools.find_header_delimiter(lines)
        if idx == -1:
            print "Invalid email message -- no header"
            return 0    # will not be marked for deletion

        full = account.get('full', False)
        store_attachments = account.get('store_attachments', False)
        ignore_attachments = account.get('ignore_attachments', False)
        file_ext = account.get('file_ext', '') or ''
        parts = proc.get_parts(lines, idx, full, ignore_attachments, file_ext,
                self.options.strip_headers)

        basename = ''
        num_attachments = 0
        for i in range(len(parts)):
            ptype, data, afilename = parts[i]
            if ptype == 'attachment':
                if ignore_attachments:
                    continue
                num_attachments += 1
                suffix = "-A%02d" % num_attachments
                if not basename:
                    basename = self.make_basename(account)
            else: # body, presumedly
                suffix = ""
                basename = self.make_basename(account)
                # if the message encoding is base64, then convert it
                #if headers.get('Content-Transfer-Encoding') == 'base64':
                #    data = tools.safe_convert_base64(data) or data

            if ptype == 'attachment' and store_attachments \
            and not ignore_attachments:
                # store attachment in attachment_dir
                outdir = os.path.join(account['attachment_dir'], output_subdir)
                outfilename = os.path.join(outdir,
                              self.make_outfilename(basename, suffix))
                if self.options.verbose:
                    print "  Writing (%s):" % (ptype,), outfilename, "..."
                attachment.write_attachment(outfilename, afilename, data)
            else: # todo(dan) What if it's an attachment?
                outfilename = os.path.join(destination,
                              self.make_outfilename(basename, suffix))
                if self.options.verbose:
                    print "  Writing (%s):" % (ptype,), outfilename, "..."
                self.write_message_raw(outfilename, data)
                parsable_filenames.append(outfilename)

                # log dates and filename
                self.klog(lines[:idx], outfilename)

        # if the email has attachments, or is suspected to have them, write the
        # original email so we can inspect it later, if necessary
        if num_attachments or self.may_have_attachments(lines):
            self.write_original_email(lines, account['call_center'])

        return parsable_filenames # will be marked for deletion

    def klog(self, headers, outfilename):
        """ Log dates and filename written. """
        date1 = "<unknown>"
        for line in headers:
            if line.startswith("Date:"):
                date1 = line[6:].strip()
        now = date.Date().isodate()
        s = "%s\t%s\t%s" % (date1, now, outfilename)
        self.log.log_event(s)

    def write_message(self, filename, lines, quoted_printable=0):
        with open(filename, "w") as f:
            for line in lines:
                if quoted_printable:
                    line = line.decode("quoted-printable")
                print >> f, line

    def write_message_raw(self, filename, data):
        with open(filename, "w") as f:
            f.write(data)


    def make_outfilename(self, base, suffix=""):
        """ Generate a name for the next file to be written. """
        filename = "%s%s.txt" % (base, suffix)
        return filename

    def make_basename(self, account):
        """ Generate and return a new "basename", i.e. the date/time and some
            random values, without the suffix or the ".txt". Will be used for
            the next ticket *and* any attachments it might have (with -A01
            suffix).
            Changed in Mantis #3059.
        """
        time.sleep(0.001)
        t = time.time()
        t9 = time.localtime(t)
        ts = "%04d-%02d-%02d-%02d-%02d-%02d" % t9[:6] # YYYY-MM-DD-HH-MM-SS
        ms = int(abs(t - int(t)) * 1000)
        stuf = string.digits + string.uppercase
        sentinel = ''.join([random.choice(stuf) for i in range(5)])
        filename = "%s-%s-%03d-%s" % (account["call_center"], ts, ms, sentinel)
        return filename

    def write_original_email(self, lines, call_center):
        """ Write a complete, original email message to the attachment dir. """
        filename = self.make_temp_filename(call_center)
        filename = os.path.join(self.config.attachment_dir, filename)
        with datadir.datadir.open(filename, "w") as f:
            self.log.log_event("Writing attachment email: %r" % filename)
            for line in lines:
                print >> f, line

    def make_temp_filename(self, call_center):
        t = time.time()
        lt = time.localtime(t)
        frac = int(1000 * (t - int(t)))
        filename = "%s-%04d%02d%02d-%02d%02d%02d-%03d.txt" % (call_center,
                   lt[0], lt[1], lt[2], lt[3], lt[4], lt[5], frac)
        return filename

    def may_have_attachments(self, lines):
        """ Return true if the message might have attachments, even if a
            regular analysis (email package) does not find anything.
        """
        hits = [
            "Content-Disposition: attachment",
            # Content-Disposition: attachment; filename=2005170289.PNG
        ]
        for line in lines:
            for hit in hits:
                if line.find(hit) >= 0:
                    return True

        return False

    def get_next_incoming_item(self, dbado):
        if not self.config.db_accounts:
            return None, None
        db_account = self.config.db_accounts[0]
        
        if not db_account.get('destination', ''):
            return None, None

        # Initialize and clean up destination subdir
        destination_subdir = os.path.join(db_account['destination'],
         config.RECEIVER_OUTPUT_SUBDIR)
        if not os.path.exists(destination_subdir):
            os.makedirs(destination_subdir)
        self.clean_up_output_dirs(db_account)
        self.check_directories(db_account, config.RECEIVER_OUTPUT_SUBDIR)

        incoming_items = dbado.runsql_result("""
         select top 1 item_id, item_type, item_text from incoming_item
         where
           cc_code = %s
           and item_id > %s
         order by item_id
        """ % (db_account['call_center'], db_account['last_item_id_read']))

        if incoming_items:
            # Save to file in destination_subdir
            filename = os.path.join(destination_subdir,
             self.make_outfilename(self.make_basename(db_account)))
            if self.options.verbose:
                print "  Writing (item_text):", filename, "..."
            self.write_message_raw(filename, incoming_items[0]['item_text'])
            # Keep track of the last item_id read, so we can skip items that
            # fail to process correctly, and not hold up processing other items.
            db_account['last_item_id_read'] = incoming_items[0]['item_id']
            return incoming_items[0]['item_id'], filename
        else:
            return None, None
          
    def delete_incoming_item(self, dbado, item_id):
        dbado.runsql("""
         delete from incoming_item where item_id = %s
        """ % (item_id))

#)
# todo(dan) Is any of this still needed?
"""
if __name__ == "__main__":

    options = ReceiverOptions()

    opts, args = getopt.getopt(sys.argv[1:], "c:Cn:1dh?r:vw", ["check="])
    for o, a in opts:
        elif o == "-c":
            options.configfile = a
        elif o in ('-C', '--check'):
            options.validate_xml = 1
        elif o == "-1":
            options.restriction = 1
        elif o == "-n":
            options.restriction = int(a)
        elif o == "-d":
            options.nodelete = 1
        elif o == "-h":
            options.strip_headers = 0
        elif o == "-r":
            options.raw_file = a
        elif o == '-w':
            options.wait = 1
        elif o == '-v':
            options.print_config = 1

    cfg = ReceiverConfiguration()
    cfg.get_configuration(options.configfile)

    if options.print_config:
        import pprint; pprint.pprint(cfg.__dict__)
        sys.exit(2)

    try:
        # only once instance of this program is allowed to run at a time
        mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
        with mutex.MutexLock(mutex_name):
            rec = Receiver(cfg, options)
            rec.check()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        # XXX original version of receiver had/has no toplevel logging; fix this
        import traceback; traceback.print_exc()
        #ep = errorhandling.errorpacket()
        #ep.add(info="Uncaught toplevel error")
        #log.log(ep, send=1, write=1, dump=1)
"""
