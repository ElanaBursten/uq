# make-tximport1.py
# Generate BCP file.

import sys; sys.path.append("..")
import string

f = open("tximport1.txt", "r")
data = f.read()
f.close()
lines = data.split("\n")
lines = lines[1:]   # drop first line, containing headers

g = open("txbcp1.tsv", "w")

call_center = '6001'
for line in lines:
    parts = line.split("\t")
    if len(parts) == 3:
        qtrmin_lat = parts[1][:5]
        qtrmin_long = parts[1][5:10]
        area_id = parts[2]
        data = [qtrmin_lat, qtrmin_long, area_id, 1, call_center]
        out = string.join(map(str, data), "\t")
        print >> g, out
        sys.stdout.write(".")

g.close()
print
