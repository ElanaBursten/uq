#

import string

d = {}

def readtsv(filename):
    f = open(filename)
    data = f.read()
    f.close()
    lines = data.split('\n')
    for line in lines:
        parts = line.split('\t')
        if len(parts) == 5:
            key = (parts[0], parts[1])
            d[key] = parts

readtsv("txbcp2.tsv")
readtsv("txbcp1.tsv")

print "Dictionary has", len(d), "keys"

items = d.items()
items.sort()
g = open('txbcp.tsv', 'w')
for (key, value) in items:
    print >> g, string.join(value, '\t')
g.close()
