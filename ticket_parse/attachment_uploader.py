import os
import glob

import config
import dbinterface_ado
import attachment
import mutex
import errorhandling2
import errorhandling_special

class AttachmentUploader:
    def __init__(self):
        self.config = config.getConfiguration()
        # todo(dan) Finish/review configuration
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
           smtpinfo=self.config.smtp_accs[0],
           me="AttachmentUploader",
           subject="Attachment Uploader Error Notification")
        self.dbado = dbinterface_ado.DBInterfaceADO()

    def process_attachment_dir(self, attachment_dir):
        self.log.log_event("Processing batch files in %r" % attachment_dir)
        for attachment_batch_filename in glob.glob(os.path.join(attachment_dir,
         attachment.ATTACHMENT_BATCH_FILENAME_PREFIX + '*.bat')):
            mutex_name = attachment.upload_mutex_name(attachment_batch_filename)
            try:
                with mutex.MutexLock(self.dbado, mutex_name):
                    attachment.upload_attachment(attachment_batch_filename,
                     self.log)
            except mutex.SingleInstanceError:
                ep = errorhandling2.errorpacket()
                ep.add(info="Locked mutex: %r" % repr(mutex_name))
                ep.add(batchfile=attachment_batch_filename)
                self.log.log(ep, send=1, dump=1 and self.log.verbose, write=1)

    def run(self):
        for process in self.config.processes:
            # todo(dan) Need to check anything besides attachment_dir?
            if process['attachment_dir']:
                self.process_attachment_dir(process['attachment_dir'])

if __name__ == "__main__":
    try:
        log = errorhandling_special.toplevel_logger("", "AttachmentUploader",
         "Attachment Uploader Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "AttachmentUploader")
        sys.exit(1)

    try:
        attachment_uploader = AttachmentUploader()
        attachment_uploader.run()
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught top level error")
        log.log(ep, send=1, write=1, dump=1)

