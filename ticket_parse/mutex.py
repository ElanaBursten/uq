"""mutex.py

Implements a MutexLock object that can be used like this:

    with MutexLock(dbado, "name"):
        ...do stuff...

...which acquires and locks the mutex "name", executes the code, and releases
the mutex.
"""

import re

import detect

class SingleInstanceError(Exception):
    """ Raised if more than one instance of a given class (or piece of code)
        would be active at the same time. """

if detect.is_ironpython():
    raise NotImplementedError

else:
    def mutex_is_locked(dbado, name):
        sql = """
         select
           applock_mode('public', '%s', 'Session') as mode_result,
           applock_test('public', '%s', 'Exclusive', 'Session') as test_result
        """ % (name, name)

        result = dbado.runsql_result(sql, as_strings=False)[0]
        return result['mode_result'] <> 'NoLock' or result['test_result'] == 0

    def locked_mutexes(dbado):
        sql = """
         select resource_description from sys.dm_tran_locks where
           resource_type = 'application' and
           request_mode = 'x' and
           request_status = 'grant' and
           request_owner_type = 'session'
        """

        re_name = re.compile("\:\[(.*?)\]\:\(")
        return [re_name.search(row['resource_description']).group(1) for row in dbado.runsql_result(sql, as_strings=False)]

    class MutexLock(object):
        def __init__(self, dbado, mutex_name):
            self.dbado = dbado
            self.mutex_name = mutex_name
            self.mutex_locked = False

        def __enter__(self):
            self.mutex_locked = self.get_lock()
            if not self.mutex_locked:
                raise SingleInstanceError("mutex already exists: %r" %
                 self.mutex_name)
            return self

        def __exit__(self, type, value, traceback):
            if self.mutex_locked:
                if self.release_lock():
                    self.mutex_locked = False
            return False

        def get_lock(self):
            sql = """
             if applock_mode('public', '%s', 'Session') = 'NoLock'
             begin
               declare @result int;
               exec @result = sp_getapplock
                @Resource = '%s',
                @LockMode = 'Exclusive',
                @LockOwner = 'Session',
                @LockTimeout = 0;
               select @result as result
             end
             else
               select -11 as result
            """ % (self.mutex_name, self.mutex_name)

            return self.dbado.runsql_result(sql,
             as_strings=False)[0]['result'] >= 0

        def release_lock(self):
            sql = """
             declare @result int;
             exec @result = sp_releaseapplock
              @Resource = '%s',
              @LockOwner = 'Session'
             select @result as result
            """ % self.mutex_name

            return self.dbado.runsql_result(sql,
             as_strings=False)[0]['result'] >= 0
