
USW01  00095 GAUPC 12/31/01 10:14:39 12311-109-056-000 NORMAL RESTAKE
Underground Notification
Ticket : 12311-109-056 Date: 12/31/01 Time: 10:11 Revision: 000
Old Tkt: 12141-300-044 Date: 12/19/01 Time: 00:12

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SPRING GARDEN                  DR   SW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE BOTH SIDES OF THE STREET    AT THE INT. OF 'QUAKER STREET' GOING
: 200 FT. IN ALL DIRECTIONS - INCLUDING ALL SERVICES IN BETWEEN

Grids    : 3341C8424B   3341C8424A   3341D8424B   3341D8424A
Work type: NEW GAS RENEWAL REPLACING GAS
Work date: 11/02/01 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 01/04/02 Time: 07:00 Good thru: 01/21/02 Restake by: 01/16/02
RespondBy: 01/03/02 Time: 23:59 Duration : 2 WKS
Done for : AGL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : RESTAKE OF 10301-300-011
: RESTAKE OF 11141-300-020
: RESTAKE OF 11291-300-020
: RESTAKE OF: 12141-300-044
: * MEA70 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
: *** WILL BORE BOTH SIDES OF ROAD

Company : GENERAL PIPELINE COMPANY                  Type: CONT
Co addr : 4090 BONSAL ROAD
City    : CONLEY                         State   : GA Zip: 30027
Caller  : TONYA MASON                    Phone   : 404-361-0005
Fax     : 404-361-1509                   Alt. Ph.:
Cellular: 404-361-0005
Submitted date: 12/31/01 Time: 10:11 Oper: 109 Chan: 999
Mbrs : AGL18  ATT02  ATTBB  BSCA   DOTI   EPT50  EPT52  FUL02  GP145  MEA70
: OMN01  TCG01  USW01
-------------------------------------------------------------------------------
TKD31100.529 #00099
TKD31100.529
Page & Grid: 299 A2

USW01  00141 GAUPC 12/20/01 13:28:08 12201-046-036-000 NORMAL
Underground Notification
Ticket : 12201-046-036 Date: 12/20/01 Time: 13:14 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    CHEROKEE                       AV   SE
Cross: From:        To:        Name:    WOODWARD                       AV   SE
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ENTIRE INTERS 200 FT ALL DIRECTIONS   BOTH SIDES OF RD

Grids    : 3344B8422C   3344A8422C
Work type: PLACE ANCHORS
Work date: 12/27/01 Time: 07:00 Hrs notc: 161 Priority: 3
Legal day: 12/27/01 Time: 07:00 Good thru: 01/10/02 Restake by: 01/07/02
RespondBy: 12/26/01 Time: 23:59 Duration : UNK
Done for : BELLSOUTH
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : JOB 1FC00373N

Company : ANSCO AND ASSOCIATES                      Type: CONT
Co addr : 262 ESTORIA ST
City    : LITHIA SPRINGS                 State   : GA Zip: 30122
Caller  : KEYONA HARRISON                Phone   : 404-653-1015
Fax     :                                Alt. Ph.:
Email   : AWARD@ANSCOINC.COM
Contact : JOHN

Submitted date: 12/20/01 Time: 13:14 Oper: 046 Chan: WEB
Mbrs : AGL10  AGL11  AGL18  ATT02  ATTBB  BSCA   DOTI   EPC01  FUL02  GP104
: IFN01  MCI02  MT104  NU104  OMN01  QWEST8 USW01  WCI01
-------------------------------------------------------------------------------
TKD20133.104 #00148
TKD20133.104
Page & Grid: 275 E5

USW01  00143 GAUPC 12/20/01 13:28:22 12201-689-007-000 NORMAL
Underground Notification
Ticket : 12201-689-007 Date: 12/20/01 Time: 13:25 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 528    To:        Name:    CASCADE HILLS                  CT
Cross: From:        To:        Name:
Offset:
Subdivision: SUMMIT @ CASCADE HILLS
RR Subdivision:            RR Marker:            Mile Marker:
Locat: SUMMIT @ CASCADE HILL L38 ON 528 CASCADE HILLS CT

Grids    : 3343C8433B
Work type: INSTALLING UNDERGROUND SERVICE
Work date: 12/27/01 Time: 07:00 Hrs notc: 161 Priority: 3
Legal day: 12/27/01 Time: 07:00 Good thru: 01/10/02 Restake by: 01/07/02
RespondBy: 12/26/01 Time: 23:59 Duration :
Done for : GREYSTONE POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : LOCATE ENTIRE LOT.  MAIN AND SERVICES.
: *** NEAR STREET ***   CASCADE HWY
: *** LOOKUP BY MANUAL

Company : GREYSTONE POWER CORPORATION               Type: MEMB
Co addr : 4040 BANKHEAD HWY
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : AMY SMITH                      Phone   : 770-370-2281
Fax     :                                Alt. Ph.:
Contact : DICK STEPHENS                  Phone   : 678-898-7530

Submitted date: 12/20/01 Time: 13:25 Oper: 689 Chan: 999
Mbrs : AGL10  AGL18  ATTBB  BSCA   DOTI   FUL02  GP131  GRS70  OMN01  USW01
-------------------------------------------------------------------------------
TKD20133.118 #00150
TKD20133.118
Page & Grid: 272 H8

USW01  00104 GAUPC 12/31/01 10:48:51 12311-095-014-000 NORMAL
Underground Notification
Ticket : 12311-095-014 Date: 12/31/01 Time: 10:22 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 3107   To:        Name:    GRANT                          WAY  SE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY

Grids    : 3339A8429C   3340D8429C   3340C8429C   3340B8429C   3340A8429C
Grids    : 3339A8429B   3340D8429B   3340C8429B   3340B8429B   3340A8429B
Grids    : 3339A8429A   3340D8429A   3340C8429A   3340B8429A   3340A8429A
Grids    : 3339A8430D   3340D8430D   3340C8430D   3340B8430D   3340A8430D
Grids    : 3339A8430C   3340D8430C   3340C8430C   3340B8430C   3340A8430C
Work type: BURY SVC WIRE
Work date: 01/04/02 Time: 07:00 Hrs notc: 092 Priority: 3
Legal day: 01/04/02 Time: 07:00 Good thru: 01/21/02 Restake by: 01/16/02
RespondBy: 01/03/02 Time: 23:59 Duration : UNKNOWN
Done for : BELLSOUTH
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   STONE RD
: *** LOOKUP BY MANUAL

Company : BELLSOUTH COMPANY                         Type: CONT
Co addr : 700 POWERS FERRY RD
City    : MARIETTA                       State   : GA Zip: 30067
Caller  : GAYLA WILDER                   Phone   : 770-578-3114
Fax     :                                Alt. Ph.:

Submitted date: 12/31/01 Time: 10:22 Oper: 095 Chan: WEB
Mbrs : AGL18  ATTBB  BSCA   DOTI   EPT50  EPT52  FUL02  GP131  OMN01  SNG80
: USW01
-------------------------------------------------------------------------------
TKD31103.941 #00108
TKD31103.941
Page & Grid: 297 H6

BGAWA  01357 GAUPC 02/24/04 17:01:29 02244-061-060-000 EMERG
Underground Notification
Ticket : 02244-061-060 Date: 02/24/04 Time: 16:57 Revision: 000

State: GA  County: LOWNDES      Place: LAKE PARK
Addr : From:        To:        Name:    I 75

Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOC THE NORTHBOUND R/O/W GOING SOUTH FROM THE INTERSECTION OF I75 AND
     : BELLEVILLE RD ALL THE WAY BEHIND THE T AND A TRUCK STOP.

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3038C8311C   3038B8311C   3038C8311D   3038B8311D  
Work type: CLEANING DIESAL CONTAMINATED SOIL
Work date: 02/24/04 Time: 17:03 Hrs notc: 000 Priority: 1
Legal day: 02/24/04 Time: 17:03 Good thru: 02/24/04 Restake by: 02/24/04
RespondBy: 02/24/04 Time: 17:03 Duration : 2 DAYS
Done for : FIREMANS FUND INSURANCE COMPANY
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : AREA IS ROPED OFF
        : EMERGENCY: DIESAL IS CONTAMINATING SOIL AND MIGHT FLOW INTO
SURFACE
        : WATER /// CREW IS IN ROUTE
        : *** NEAR STREET ***   BELLVILLE RD
        : *** LOOKUP BY MANUAL

Company : SOIL REMEDIATION                          Type: CONT
Co addr : COUNTY RD 329
City    : RAY CITY                       State   : GA Zip: 31645
Caller  : MEREDITH LANCASTER             Phone   : 229-455-2300

Fax     :                                Alt. Ph.: 
Contact : BOB MOORE                      Cellular: 229-563-6609


Submitted date: 02/24/04 Time: 16:57 Oper: 061 Chan: 999
Mbrs : ALLBB8 BGAWA  CLQ71  LOW99  TCI04 

