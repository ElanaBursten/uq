FROM AROCS
VALOR TELECOMMUNICATIONS, LLC
VALOR
AUDIT FOR 01/10/05

FOR CODE VALOR

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
!     0001  050110-0038               DELIVERED
      0002  050110-0082               DELIVERED
!     0003  050110-0135               DELIVERED
*!    0004  050110-0038               DELIVERED
      0005  050110-0187               DELIVERED
      0006  050110-0309               DELIVERED
      0007  050110-0313               DELIVERED
      0008  050110-0315               DELIVERED
      0009  050110-0323               DELIVERED
      0010  050110-0330               DELIVERED
      0011  050110-0334               DELIVERED
      0012  050110-0339               DELIVERED
      0013  050110-0341               DELIVERED
      0014  050110-0345               DELIVERED
      0015  050110-0359               DELIVERED
*     0016  050107-0110               DELIVERED
*     0017  050107-0163               DELIVERED
*     0018  050107-0212               DELIVERED
*     0019  050107-0216               DELIVERED
*     0020  050107-0220               DELIVERED
*     0021  050107-0222               DELIVERED
*     0022  050107-0223               DELIVERED
*     0023  050107-0609               DELIVERED
*     0024  050107-0610               DELIVERED
*!    0025  050110-0038               DELIVERED
*!    0026  050110-0135               DELIVERED
*!    0027  050110-0038               DELIVERED
*     0028  050110-0082               DELIVERED
*     0029  050110-0187               DELIVERED
*     0030  050110-0309               DELIVERED
*     0031  050110-0313               DELIVERED
*     0032  050110-0315               DELIVERED
*     0033  050110-0323               DELIVERED
*     0034  050110-0330               DELIVERED
*     0035  050110-0334               DELIVERED
*     0036  050110-0339               DELIVERED
*     0037  050110-0341               DELIVERED
*     0038  050110-0345               DELIVERED
*     0039  050110-0359               DELIVERED
      0040  050110-0446               DELIVERED
      0041  050110-0611               DELIVERED
      0042  050110-0626               DELIVERED
      0043  050110-0759               DELIVERED

NORMAL    : 37
RESEND    : 25
EMERGENCY : 6
FAILED    : 0
TOTAL FOR VALOR: 43




LEGEND
-----------------------
  - NORMAL
* - RESEND
! - EMERGENCY

