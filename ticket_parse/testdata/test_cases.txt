field name	explanation																	
file	what file is the ticket it?	atlanta-1.txt	atlanta-1.txt	atlanta-3.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt	atlanta-2.txt
index	what order in that file?	1	4	9	2	3	4	5	6	7	8	9	10	11	12	13	14	15
testing?		Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N
ticket_number		12311-109-056	12311-095-014	10291-028-011														 
call_date		2001-12-31 10:11:00	12/31/2001 10:22	2001-11-01 00:09:00														 
revision		000	000	001														 
work_state		GA	GA	GA														 
work_county		FULTON	FULTON	FULTON														 
work_city		ATLANTA	ATLANTA	ATLANTA														 
work_address_number			LOCATE ENTIRE PROPERTY	180														 
work_address_number_2				180														 
work_address_street		SPRING GARDEN DR SW		NORTHSIDE DR SW														 
work_cross																		 
work_subdivision																		 
work_description		LOCATE BOTH SIDES OF THE STREET   AT THE INT. OF QUAKER STREET GOING 200 FT. IN ALL DIRECTIONS - INCLUDING ALL SERVICES IN BETWEEN																 
work_type		NEW GAS RENEWAL REPLACING GAS		INSTALLING DOWN GUYS AND POLES														 
work_date		2001-11-02 07:00:00		2001-11-01 07:00:00														 
work_notc		000		006														 
priority		3	bla	3	a	a	a	a	a	a	a	a	a	a	a	a	a	a
legal_date		2002-01-04 07:00:00		2001-11-01 07:00:00														 
legal_good_thru		2002-01-21 00:00:00		2001-11-19 00:00:00														 
legal_restake		2002-01-16 00:00:00		2001-11-14 00:00:00														 
respond_date		2002-01-03 23:59:00		2001-10-31 23:59:00														 
duration		2 WKS		3 DAYS														 
company		AGL																 
work_remarks		RESTAKE OF 10301-300-011 RESTAKE OF 11141-300-020 RESTAKE OF 11291-300-020 RESTAKE OF: 12141-300-044 * MEA70 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET *** WILL BORE BOTH SIDES OF ROAD																 
con_name		GENERAL PIPELINE COMPANY		GEORGIA POWER COMPANY														 
con_address		4090 BONSAL ROAD		760 RALPH MCGILL BLVD														 
con_city		CONLEY		ATLANTA														 
con_state		GA		GA														 
con_zip		30027		30308														 
caller		TONYA MASON		CASEY PREECE														 
caller_phone		404-361-0005		404-572-7715														 
caller_fax		404-361-1509																 
caller_altphone																		 
operator		109		028														 
channel		999		WEB														 
locates		AGL18  ATT02  ATTBB BSCA  DOTI   EPT50  EPT52  FUL02  GP145  MEA70 OMN01  TCG01  USW01																 
kind		NORMAL		NORMAL														 
