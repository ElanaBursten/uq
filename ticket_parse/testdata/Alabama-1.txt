BYER04 00004 AOC 09/23/02 07:40:59 0264170-000 NORM NEW PLCE

Ticket : 0264170 Rev: 00 Date: 09/23/02 Time: 07:18 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: DAPHNE In/Out: I
Addr : 27591  Street: CLAIRBORN CIR   CPG: N
Xst  : SEHOY BLVD  Int: N

Locat: LOCATE ALONG THE RIGHT AWAY ON BOTH SIDES OF THE STREET ALONG CLAIRBORN
CIR FOR APPROX 200FT FROM THE INTERSECTION OF SEHOY
:
Grids:
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:18  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : ATTT01 BYER04 DPHN01 FRHO01 GULF01 MCIT01 MDCM02 MDCM04 MRCM01 RVUT01
Mbrs : SCMB01 WLCM01










































BYER04 00005 AOC 09/23/02 07:41:08 0264173-000 NORM NEW GRID

Ticket : 0264173 Rev: 00 Date: 09/23/02 Time: 07:20 Opr: JDF Chn: 3
State: AL County: MOBILE     Place: IRVINGTON In/Out: O
Addr : 7320  Street: N HIGHGATE DR   CPG: Y
Xst  : ROY E RAY AIRPORT RD  Int: N

Locat: LOCATE AT THE WHITE STAKE WITH ORANGE FLAGGING RIBBON APPROX 100FT DOWN
THE PROPERTY LINE -- JOB -- 65520-40-00822
:
Grids: T07SR02W05**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:20  Good Thru: 10/09/02  Update: 10/07/02
Work Type: SETTING A POLE
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: AL POWER - THEODORE

Company: AL POWER - THEODORE
Address: 5832 US HWY 90 W
City&St: THEODORE, AL                   Zip: 36582
Caller : TERRY FARMER Phone: 251-434-5437
Callback: N
Fax: 251-434-5432
Email: tnfarmer@southernco.com
Mbrs : APC55B BYER04 CTAL01 GSPL01 MOBG01 UULC02









































BYER04 00006 AOC 09/23/02 07:41:16 0264175-000 NORM NEW STRT

Ticket : 0264175 Rev: 00 Date: 09/23/02 Time: 07:23 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: FAIRHOPE In/Out: I
Addr : 201  Street: DRIFTWOOD AVE   CPG: N
Xst  : RIVER OAKS DR  Int: Y

Locat: LOCATE THE ENTIRE INTERSECTION
:
Grids: T06SR02E20** T06SR02E21**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:23  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : AZED01 BYER04 FRHO01 MDCM04 RVUT01 SCMB01 SLLC01












































BYER04 00007 AOC 09/23/02 07:41:24 0264176-000 NORM NEW STRT

Ticket : 0264176 Rev: 00 Date: 09/23/02 Time: 07:24 Opr: JDF Chn: 3
State: AL County: MOBILE     Place: CODEN In/Out: I
Addr : 8181  Street: HIGHWAY 188   CPG: N
Xst  : SUNNY SIDE LN  Int: N

Locat: LOCATE AT THE WHITE STAKE WITH ORANGE FLAGGING RIBBON ON THE RIGHT SIDE
OF THE TRAILER -- JOB -- 65520-14-01242
:
Grids: T07SR03W35SE T07SR03W36SW T08SR02W02S* T08SR02W03** T08SR02W04**
Grids: T08SR02W05** T08SR02W06** T08SR03W01N* T08SR03W02NE
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:24  Good Thru: 10/09/02  Update: 10/07/02
Work Type: SETTING A POLE
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: AL POWER - THEODORE

Company: AL POWER - THEODORE
Address: 5832 US HWY 90 W
City&St: THEODORE, AL                   Zip: 36582
Caller : TERRY FARMER Phone: 251-434-5437
Callback: N
Fax: 251-434-5432
Email: tnfarmer@southernco.com
Mbrs : APC55B BLBU01 BYER04 COPI01 CTAL01 DIGP01 GSNG01 MCWS01 MDCM01 MOBG01
Mbrs : TRPL04 UULC02







































BYER04 00008 AOC 09/23/02 07:41:34 0264177-000 NORM NEW STRT

Ticket : 0264177 Rev: 00 Date: 09/23/02 Time: 07:24 Opr: MLT Chn: 15
State: AL County: BALDWIN    Place: ORANGE BEACH In/Out: I
Addr : 26243  Street: CARONDELETTE DR   CPG: N
Xst  : COTTON BAYOU DR  Int: N

Locat: LOCATE AT THE WHITE FLAGS IN FRONT OF THIS RESIDENCE
:
Grids: T09SR05E09**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:24  Good Thru: 10/09/02  Update: 10/07/02
Work Type: SETTING NEW SERVICE
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: ORANGE BEACH WATER-A

Company: ORANGE BEACH WATER-AUTHORITY
Address: 25097 CANAL ROAD
City&St: ORANGE BEACH, AL               Zip: 36561
Caller : LEE Phone: 251-981-4233
Callback: N
Mbrs : BEMC01 BYER04 CAOP01 CMGD01 GULF01 MDCM02 OBWA01












































BYER04 00009 AOC 09/23/02 07:41:42 0264178-000 NORM NEW STRT

Ticket : 0264178 Rev: 00 Date: 09/23/02 Time: 07:25 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: FAIRHOPE In/Out: I
Addr : 143  Street: OAKWOOD AVE   CPG: N
Xst  : SAGEBRUSH LP  Int: N

Locat: LOCATE FROM THE ABOVE ADDRESS TO SAGEBRUSH LP AND DOWN SAGEBRUSH LP
APPROX 200FT
:
Grids: T06SR02E20** T06SR02E21**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:25  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : AZED01 BYER04 FRHO01 MDCM04 RVUT01 SCMB01 SLLC01











































BYER04 00010 AOC 09/23/02 07:41:50 0264182-000 NORM NEW STRT

Ticket : 0264182 Rev: 00 Date: 09/23/02 Time: 07:27 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: FAIRHOPE In/Out: I
Addr : 132  Street: PEMBERTON LP   CPG: N
Xst  : MANLEY RD  Int: N

Locat: LOCATE ENTIRE FRONT OF PROPERTY APPROX 150FT
:
Grids: T06SR02E28*W
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:27  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BEMC01 BYER04 FRHO01 MDCA01 MDCM04 RVUT01 SCMB01












































BYER04 00011 AOC 09/23/02 07:41:57 0264185-000 NORM NEW STRT

Ticket : 0264185 Rev: 00 Date: 09/23/02 Time: 07:28 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: DAPHNE In/Out: I
Addr :   Street: LAWSON RD   CPG: N
Xst  : RIDGEWOOD DR  Int: N

Locat: LOCATE FROM THE ABOVE INTERSECTION APPROX 200FT DOWN LAWSON RD ON BOTH
SIDES OF THE RD
:
Grids: T05SR02E03** T05SR02E04*E
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:28  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BYER04 DPHN01 FRHO01 MDCM04 RVUT01 SCMB01 WLCM01











































BYER04 00012 AOC 09/23/02 07:42:05 0264186-000 NORM NEW STRT

Ticket : 0264186 Rev: 00 Date: 09/23/02 Time: 07:29 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: DAPHNE In/Out: I
Addr : 9576  Street: CANTERBURY CT   CPG: N
Xst  : HIGHWAY 27  Int: N

Locat: LOCATE APPROX 100FT IN FRONT OF THE ABOVE ADDRESS
:
Grids: T05SR02E04**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:29  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BYER04 DPHN01 MDCM04 RVUT01 SCMB01












































BYER04 00013 AOC 09/23/02 07:42:13 0264188-000 NORM NEW STRT

Ticket : 0264188 Rev: 00 Date: 09/23/02 Time: 07:28 Opr: MLT Chn: 15
State: AL County: MOBILE     Place: SEMMES In/Out: B
Addr : 1831  Street: WOODSTONE CT W   CPG: N
Xst  : SCHILLINGER RD  Int: N
Subd : WOODLAND HILLS SUBD  Lot #: 39

Locat: THIS IS A NEW HOME - LOCATE THE ENTIRE FRONTAGE OF THIS LOT
:
Grids: T04SR03W01**
Work date : 09/25/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:28  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALLING U/G SEWER
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: L AND T CONSTRUCTION

Company: L AND T CONSTRUCTION
Address: 326 BENTLEY RD
City&St: CREOLA, AL                     Zip: 36525
Caller : GENE GARNER Phone: 251-679-7029
Callback: N
  Cell/Pager: 334-209-4114
Mbrs : APC55B BYER04 GSPL01 MDCM01 MOBG01 MOBW01 SCMB01 SOAL01 UULC02










































BYER04 00014 AOC 09/23/02 07:42:21 0264189-000 NORM NEW STRT

Ticket : 0264189 Rev: 00 Date: 09/23/02 Time: 07:30 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: FAIRHOPE In/Out: I
Addr : 20327  Street: OBERG RD   CPG: N
Xst  : FAIRHOPE AVE  Int: N

Locat: OBERG RD AKA CO RD 13---LOCATE FROM THE ABOVE ADDRESS APPROX 300FT IN
EACH  DIRECTION
:
Grids: T06SR02E15** T06SR02E16** T06SR02E21** T06SR02E22**
Work date : 09/25/02 Time: 07:45  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:30  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BEMC01 BYER04 FRHO01 MDCM04 RVUT01 SCMB01











































BYER04 00015 AOC 09/23/02 07:42:29 0264193-000 NORM NEW STRT

Ticket : 0264193 Rev: 00 Date: 09/23/02 Time: 07:32 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: FAIRHOPE In/Out: I
Addr : 10  Street: PADDOCK DR   CPG: N
Xst  : S MOBILE ST  Int: N

Locat: LOCATE APPROX 300FT FROM S MOBILE ST ALONG PADDOCK DR AT THE ABOVE
ADDRESS
:
Grids: T06SR02E19*W T06SR02E37SW
Work date : 09/25/02 Time: 07:45  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:32  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BYER04 FRHO01 MDCM04 RVUT01 SCMB01











































BYER04 00016 AOC 09/23/02 07:42:37 0264196-000 NORM NEW STRT

Ticket : 0264196 Rev: 00 Date: 09/23/02 Time: 07:33 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: DAPHNE In/Out: I
Addr : 29150  Street: LAKE FOREST BLVD   CPG: N

Locat: LOCATE IN FRONT OF THE ABOVE ADDRESS APPROX 250FT
:
Grids: T05SR02E06SW T05SR02E07N*
Work date : 09/25/02 Time: 07:45  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:33  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : BCSS01 BYER04 DPHN01 FRHO01 MDCM02 MDCM04 RVUT01 SCMB01













































BYER04 00017 AOC 09/23/02 07:42:44 0264199-000 NORM NEW STRT

Ticket : 0264199 Rev: 00 Date: 09/23/02 Time: 07:35 Opr: YDH Chn: 9
State: AL County: BALDWIN    Place: BAY MINETTE In/Out: I
Addr :   Street: ARCHIE FLOWERS RD   CPG: N
Xst  : PINEGROVE RD  Int: N

Locat: LOCATE ON BOTH SIDES OF THE STREET ON ARCHIE FLOWERS RD APPROX 400FT FROM
THE INTERSECTION OF PINEGROVE RD
:
Grids: T02SR03E34SE T02SR03E35SW
Work date : 09/25/02 Time: 07:45  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 09/25/02 Time: 07:35  Good Thru: 10/09/02  Update: 10/07/02
Work Type: INSTALL U/G CATV
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: NEW CENTURY CABLE

Company: NEW CENTURY CABLE
Address: POB 1737
City&St: CHESTWORTH, GA                 Zip: 30705
Caller : MICHAEL Phone: 404-427-0379
Callback: N
Mbrs : APC534 BEMC01 BMUT01 BYER04 FRHO01 GSPL01 GSPL02 MDCM04 SCMB01









