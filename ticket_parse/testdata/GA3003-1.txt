
BSCA  00001 GAUPC 06/25/07 02:20:50 06217-400-001-000 LPMEET       
Large Project Meeting Notification   
Notice : 06217-400-001 Date: 06/21/07  Time: 03:32  Revision: 000 

State : GA County: FULTON       Place: HAPEVILLE                               
Addr  : From:        To:        Name:    Sunset                         Ave     
Cross1: Name: N  Central                        Ave     
Cross2: Name:    Victoria                       Ln      
Near  : Name:                                       
Subdivision:                                         

Locate: Locate sunset ave the entire rd from the intersection of n central ave 
      :  to the intersection of victoria ln both sides of rd including the r/o/w

Grids       : 3339D8423A 3339C8423A 3339B8423A 3339D8424D 3339C8424D 
Grids       : 
Work type   : replace main and service sewer                                      
ScopeOfWork : replacement of existing sanitary sewer mains and service lines   

Meeting Date      : 06/26/07 Time: 10:00 
Meeting Respond By: 06/25/07 Time: 11:59
Meeting Location  : Hapeville City Hall
3468 N Fulton Ave
Hapeville, Ga 30354
LP Contact        : willis Stallings          Phone   : 7702512667 Ext:  
Contact Email     : willis@ronniedjones.com                                    

RespondBy : 06/28/07 Time: 11:59 Duration : 90 days    Priority: 1
Done for  : city of hapeville                       
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks : N Central Ave is also called US Hwy 19                              
        : *** WILL BORE None                          
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : RONNIE D JONES ENTERPRISES                Type: CONT                
Co addr : 315 MILLER FARMER IND BLVD               
City    : NEWNAN                         State   : GA Zip: 30263              
Caller  : WILLIS STALLINGS               Phone   :  770-251-2667              
Fax     :                                Alt. Ph.:                            
Email   : willis@ronniedjones.com                                             
Contact :                                                           

Submitted date: 06/21/07  Time: 03:32  Oper: 19864
Mbrs : BSCA GAUPC GPTRW MBL91 MCI02 ATT02 AGLN01 ATL01 ATL02 CWA02 
     : AGL108 GP141B GP143 HVL50 HVL51 QWEST8 
-------------------------------------------------------------------------------





BSCA  00001 GAUPC 06/29/07 01:45:00 06217-400-001-001 LPEXCA              
Underground Notification             
Notice : 06217-400-001 Date: 06/29/07  Time: 01:44  Revision: 001 

State : GA County: FULTON       Place: HAPEVILLE                               
Addr  : From:        To:        Name:    Sunset                         Ave     
Near  : Name:                                       
Subdivision:                                         

Locate: Locate sunset ave the entire rd from the intersection of n central ave 
      :  to the intersection of victoria ln both sides of rd including the r/o/w

Grids       : 3339D8423A 3339C8423A 3339B8423A 3339D8424D 3339C8424D 
Grids       : 
Work type   : replace main and service sewer                                      
Start date: 01/01/00 Time: 12:00 Hrs notc : 000
Legal day : 01/01/00 Time: 12:00 Good thru: 09/19/07 Restake by: 09/14/07
RespondBy : 06/28/07 Time: 11:59 Duration : 90 days    Priority:  
Done for  : city of hapeville                       
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks : N Central Ave is also called US Hwy 19                              
        : *** WILL BORE None                          

Company : RONNIE D JONES ENTERPRISES                Type: CONT                
Co addr : 315 MILLER FARMER IND BLVD               
City    : NEWNAN                         State   : GA Zip: 30263              
Caller  : WILLIS STALLINGS               Phone   :  770-251-2667              
Fax     :                                Alt. Ph.:                            
Email   : willis@ronniedjones.com                                             
Contact :                                                           

Submitted date: 06/29/07  Time: 01:44  Oper: 19830
Mbrs : GP143 AGL108 BSCA GP141B ATL02 AGLN01 ATL01 ATT02 CWA02 GAUPC 
     : HVL50 HVL51 GPTRW MBL91 
-------------------------------------------------------------------------------



