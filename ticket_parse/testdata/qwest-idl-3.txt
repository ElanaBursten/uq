IDL2004122300112	208743	2004/12/27 08:49:31	00022


Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD LOCATE
Ticket No:    2004520912 Seq. No: 2
Update of:
Original Call Date:     12/23/2004     Time:      11:08:13 AM  OP: 218
Transmit Date:          12/23/2004     Time:      11:10:34 AM
Locate By Date:         12/28/2004     Time:      10:08:00 AM

Company:           HOMEOWNER-LUNDGREN
Contact Name:      MELONY LUNDGREN            Contact Phone:   (208)746-5204
Alternate Contact:                            Alternate Phone:
Best Time to Call:                            Fax No:
Cell Phone:                                   Pager No:

State: ID       County: NEZ PERCE       City: LEWISTON
Address:    2228  , CEDAR AVE
To Address:
Nearest Intersecting Street: 22ND ST
2nd Intersecting Street:     23RD ST
Subdivision:

Location of Work: PLS LOC A 5` RADIUS AROUND THE RETAINING WALL ON
                  THE E SIDE OF THE PROPERTY AT THE ABOVE ADDRESS



Remarks: **REPAIRING EXISTING RETAINING WALL**


Type of Work: INSTALL RETAINING WALL
Private Property: Y    Street:            Y   Overhead Lines:     Blasting:
Easement:              Mechanical Boring:     Premarked:
Excavator/Owner: HOMEOWNER-LUNDGREN
Sending to: (listing of utilities tkt sent to)
*USWLEW         AVISTA35        CTYLEW35        QLNID35         TCILEW35
LOID35

                                 FOR MEMBER USE ONLY
Located by____________________________________Date of Location_________________
Remarks:_______________________________________________________________________
_______________________________________________________________________________
_______________________________________________________________________________
Excavator Notified (Not located)__________________ Who Notified________________
Notified by:__________________________________Date:______________Time:_________
---------------------------------------------------------------------------

