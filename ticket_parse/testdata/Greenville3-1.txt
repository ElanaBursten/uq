
DPCZ02 1 PUPS Voice 04/12/2006 7:05:40 AM 0604120024 Resend

Ticket Number: 0604120024
Old Ticket Number: 0604050091
Created By: R-JMT
Seq Number: 1

Created Date: 04/12/2006 7:05:37 AM
Work Date/Time: 04/12/2006 7:15:21 AM
Update:           Good Through:          

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 185
Street: WOODRUFF RD
Inters St: OLD WOODRUFF RD
Subd: 

Type of Work: SEWER, REPLACE MAIN
Duration: 2 WEEKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: CITY OF GREENVILLE

Remarks/Instructions: NEED TO MARK BOTH SIDES OF OLD WOODRUFF RD STARTING     
AT WOODRUFF RD ALL THE WAY TO HAYWOOD RD                                      
                                                                              
** RESENDING DUE TO CALLER STATES THAT THE LINES HAVE BEEN MARKED BUT THEY    
HAVE A RUN ACROSS A LINE THAT WAS NOT MARKED AND BELIEVES IT BELONGS TO       
CABLE OR PHONE PLEASE RESPOND TO THE SITE ASAP TO VERIFY THE LINE CREW IS     
ON SITE //                                                                    

Caller Information: 
Name: JULIE THOMPSON                        CITY OF GREENVILLE                    
Address: 360 SOUTH HUDSON STREET
City: GREENVILLE State: SC Zip: 29601
Phone: (864) 467-4335 Ext:  Type: Business
Fax: (864) 467-4303 Caller Email: 

Contact Information:
Contact:JULIE THOMPSON Email: THOMPSJ@GREATERGREENVILLE.COM
Call Back: Fax: (864) 467-4303

Grids: 
Lat/Long: 34.8311065786813, -82.347088293917
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCMZ41 CGR12 DPCZ02 PNGZ81                           


Map Link: (NEEDS DEVELOPMENT)






DPCZ02 10 PUPS Voice 04/12/2006 7:55:32 AM 0604120162 Normal

Ticket Number: 0604120162
Old Ticket Number: 
Created By: AKR
Seq Number: 10

Created Date: 04/12/2006 7:55:30 AM
Work Date/Time: 04/17/2006 8:00:25 AM
Update: 04/26/2006 Good Through: 05/01/2006

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 191
Street: MONTVERDE DR
Inters St: FLICKER DR
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: APPROX 30 MINS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: UNO CABLE

Remarks/Instructions: WORK FOR CHARTER CABLE//                                
                                                                              
MARK THE  ENTIRE PROPERTY//                                                   

Caller Information: 
Name: KIP BARRY                             UNO CABLE                             
Address: 104 MORNINGSIDE CIR
City: CONYERS State: GA Zip: 30094
Phone: (678) 283-0532 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:KIP BARRY Email: 
Call Back: Fax: 

Grids: 
Lat/Long: 34.9226613202611, -82.386452337729
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCMZ41 DPCZ02                                        


Map Link: (NEEDS DEVELOPMENT)






DPCZ02 31 PUPS Voice 04/12/2006 8:34:58 AM 0604120357 Normal

Ticket Number: 0604120357
Old Ticket Number: 
Created By: AKR
Seq Number: 31

Created Date: 04/12/2006 8:34:56 AM
Work Date/Time: 04/17/2006 8:45:57 AM
Update: 04/26/2006 Good Through: 05/01/2006

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 51
Street: WESTVIEW AVE
Inters St: SUMMIT DR
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: APPROX 30 MINS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: UNO CABLE

Remarks/Instructions: WORK FOR CHARTER CABLE//                                
                                                                              
MARK THE  ENTIRE PROPERTY//                                                   
                                                                              
                                                                              
                                                                              
                                                                              

Caller Information: 
Name: KIP BARRY                             UNO CABLE                             
Address: 104 MORNINGSIDE CIR
City: CONYERS State: GA Zip: 30094
Phone: (678) 283-0532 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:KIP BARRY Email: 
Call Back: Fax: 

Grids: 
Lat/Long: 34.8737121018684, -82.3852750741281
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CGR12 DPCZ02 PNGZ81                                  


Map Link: (NEEDS DEVELOPMENT)





