
NOTICE OF INTENT TO EXCAVATE      INSU-SHORT NOT                DUPLICATION
Ticket No: 36252079
Transmit      Date: 12/29/05      Time: 10:14AM  Op: TESSTO
Original Call Date: 12/28/05      Time: 10:23AM  Op: TESSTO
Work to Begin Date: 12/30/05      Time: 09:45AM

Place: NOCONA
Address:             Street: NOBLE RD
Nearest Intersecting Street: HWY 59

Type of Work: SETTING ONE POLE AND ANCHOR-POLE/SIGN INSTALLATION
Extent of Work: WORK DATE: 2 WORKING DAY NOTICE MAPSCO: SUBDIVISION: GATE
: CODE:   GRIDS: LAT/LONG: +33.672917-97.682278 FROM INTERSECTION HWY 59 A
: N D NOBLE RD, GO NORTH ON NOBLE RD FOR . 5 MILE. ON THE EAST SIDE OF RD
: WILL BE A CLEARING BESIDE A FENCE. FOLLOW THE CLEARING FOR APPROX. . 1 M
: ILE TO ANOTHER CLEARING THAT GOES SOUTH. AREA MARKED WITH WOODEN S TAKES
: AND PINK RIBBON. 333812.XML    BSHRIVER@WISEEC.COM

Remarks: MARK ALL FACILITIES AS NECESSARY
: TESSTOC: 053621647 Seq: 1480 Rcd: 12/28/05 10:32 WTBD: 12/30/05 09:45

Company     : WISE ELECTRIC COOPERATIVE
Contact Name: BRYAN SHRIVER         Contact Phone: (940)627-2167
Alt. Contact: BRYAN SHRIVER         Alt. Phone   : (940)393-2526
Work Being Done For: LEE ANN SMITH
State: TX              County: MONTAGUE
Map: TX    Page:       Grid Cells:
Explosives: N
ALTEL01
Send To: TEST06    Seq No: 0040   Map Ref:


NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  DUPLICATION
Ticket No: 36251601
Transmit      Date: 12/29/05      Time: 10:14AM  Op: TESSTO
Original Call Date: 12/28/05      Time: 09:44AM  Op: TESSTO
Work to Begin Date: 12/30/05      Time: 09:45AM

Place: NOCONA
Address:   2000      Street: NOBILE RD
Nearest Intersecting Street: FENOGLIO RD

Type of Work: SETTING ONE POLE-POLE/SIGN INSTALLATION
Extent of Work: WORK DATE: 2 WORKING DAY NOTICE MAPSCO: SUBDIVISION: GATE
: CODE:   GRIDS: LAT/LONG: +33.692000-97.684472 FROM INTERSECTION FENOGLIO
:  RD AND NOBILE RD, GO NORTH ON NOBILE RD FOR .6 MILE TO ADDRESS ABOVE.
: LOCATE IN THE BACK OF HOUSE BETWEEN AWNINGS. AREA MARKED WITH WOODEN  ST
: AKES AND PINK RIBBONS. 333813.XML      BSHRIVER@WISEEC.COM

Remarks: MARK ALL FACILITIES AS NECESSARY
: TESSTOC: 053621273 Seq: 1143 Rcd: 12/28/05 09:51 WTBD: 12/30/05 09:45

Company     : WISE ELECTRIC COOPERATIVE
Contact Name: BRYAN SHRIVER         Contact Phone: (940)627-2167
Alt. Contact: BRYAN SHRIVER         Alt. Phone   : (940)393-2526
Work Being Done For: TED BEGGS
State: TX              County: MONTAGUE
Map: TX    Page:       Grid Cells:
Explosives: N
ALTEL01
Send To: TEST06    Seq No: 0048   Map Ref:

