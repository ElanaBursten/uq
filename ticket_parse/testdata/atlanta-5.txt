USW01  00193 GAUPC 02/28/02 13:41:41 02222-989-003-002 NORMAL 2NDREQ
Underground Notification
Ticket : 02222-989-003 Date: 02/28/02 Time: 13:30 Revision: 002
Old Tkt: 02222-989-003 Date: 02/27/02 Time: 00:13

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    ROSWELL                        ROAD
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ROSWELL ROAD FROM OSNER DRIVE TO HARDEMAN ROAD. TO INCLUDE THE
     : INTERSECTIONS OF OSNER DRIVE, CHEMIN DE VIE, FOREST HILLS DRIVE, PARK
     : AVENUE AND HARDEMAN ROAD. PAINT AND STAKE BOTH SIDES OF STREET, ALL
     : UTILITIES, MAINS AND SERVICE LINES.
     : LOCATE ALL INTERSECTIONS 200 FEET IN ALL DIRECTIONS. LOCATE AS
     : REQUESTED. NO VERBAL MODIFICATIONS. POSSIBLE BORE.

Grids    : 3353B8422A
Work type: INSTALL / MAINTAIN GAS MAIN / SERVICES
Work date: 02/27/02 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 02/27/02 Time: 07:00 Good thru: 03/15/02 Restake by: 03/12/02
RespondBy: 02/26/02 Time: 23:59 Duration :
Done for : ATLANTA GAS LIGHT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.-- 2ND RQST TO UNITED WTR SERV -- 48 HRS ARE
        : UP -- CREW ON SITE-- CALLER CHECKED THE POSITIVE RESPONSE INFO SYS
        : AND IT STATED MARKED, BELLSOUTH RESPONSED BY SAYING THEY AND  THE
        : EXCAVATOR  HAVE AGREED TO MEET ON SITE- CALLER IS
        : DISPUTING THIS, THERE ARE NO MARKINGS-- 3RD RQST TO FULTON COUNTY
        : SWR, OMNI CABLE AND GA DOT  NO RESPONSE
        : *** THIS IS THE 2ND REQUEST FOR USW01.  PLEASE LOCATE ASAP.
        : *** NEAR STREET ***   OSNER DRIVE
        : *** WILL BORE ROAD, DRIVEWAY & SIDEWALK
        : *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : BONNIE GOLDTHORPE              Phone   : 770-942-8180
Fax     :                                Alt. Ph.:
Email   : TWYSNER@BENTON-GEORGIA.COM

Submitted date: 02/28/02 Time: 13:30 Oper: 989 Chan: 999
Mbrs : AGL123 ATTBB  BSNE   DOTI   FUL02  GP106  GP801B OMN01  USW01
-------------------------------------------------------------------------------
TK228133.059 #00199



USW01  00016 GAUPC 02/28/02 00:15:03 02252-096-096-001 NORMAL LATE
Underground Notification
Ticket : 02252-096-096 Date: 02/28/02 Time: 00:12 Revision: 001
Old Tkt: 02252-096-096 Date: 02/25/02 Time: 10:19

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name: W  PEACHTREE                      ST   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC THE N. CORNER OF PROPERTY. LOOK FOR THE BELL SOUTH BLDG #2

Grids    : 3346C8423D
Work type: INSTALLING TELE DUCT BANK
Work date: 02/28/02 Time: 07:00 Hrs notc: 006 Priority: 3
Legal day: 02/28/02 Time: 07:00 Good thru: 03/18/02 Restake by: 03/13/02
RespondBy: 02/27/02 Time: 23:59 Duration : 3DAY
Done for : BRASFIELF GORIE
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** USW01 IS LATE RESPONDING.  PLEASE RESPOND ASAP.
        : *** NEAR STREET ***   3RD ST
        : *** WILL BORE N0NE
        : *** LOOKUP BY MANUAL

Company : HENDERSON ELEC                            Type: CONT
Co addr : 4641 STONE GATE DR
City    : STONE MOUNTAIN                 State   : GA Zip: 30086
Caller  : TOMMY CRUMBLEY                 Phone   : 770-841-1609
Fax     :                                Alt. Ph.:

Submitted date: 02/28/02 Time: 00:12 Oper: 096 Chan: WEB
Mbrs : AGL103 ATTBB  BSCA   DOTI   FUL02  GP103  GP104  LEV3   LGN01  MCI02
     : MFN02  MT103  MT104  NU103  NU104  OMN01  TWT90  USW01  WCI01
-------------------------------------------------------------------------------
TK228000.422 #00022



USW01  00147 GAUPC 03/04/02 14:16:22 02272-935-019-002 NORMAL 2NDREQ
Underground Notification
Ticket : 02272-935-019 Date: 03/04/02 Time: 14:11 Revision: 002
Old Tkt: 02272-935-019 Date: 03/02/02 Time: 00:18

State: GA  County: FULTON       Place: ATLANTA
Addr : FromC:u~ 1462   To:        Name:    LAKEWOOD                       AVE  SE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY.  NEAR MILLER REED AVENUE. AERO MAP 845E10.
     : PAINT AND STAKE BOTH SIDES OF STREET, ALL UTILITIES, MAINS AND SERVICE
     : LINES. LOCATE AS REQUESTED. NO VERBAL MODIFICATIONS.  POSSIBLE BORE.

Grids    : 3342A8422A
Work type: INSTALL/MAINTAIN GAS  SERVICE
Work date: 03/04/02 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 03/04/02 Time: 07:00 Good thru: 03/20/02 Restake by: 03/15/02
RespondBy: 03/01/02 Time: 23:59 Duration :
Done for : ATLANTA GAS LIGHT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.///2ND REQUEST ON GA DOT, FULTON COUNTY
        : SEWER, AND OMNI CATV
        : *** THIS IS THE 2ND REQUEST FOR USW01.  PLEASE LOCATE ASAP.
        : *** NEAR STREET ***   MILLER REED AVE
        : *** WILL BORE ROAD, DRIVEWAY & SIDEWALK
        : *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : SHARLENE GREEN                 Phone   : 770-942-8180
Fax     :                                Alt. Ph.:
Email   : TWYSNER@BENTON-GEORGIA.COM

Submitted date: 03/04/02 Time: 14:11 Oper: 935 Chan: 999
Mbrs : AGL102 ATTBB  BSCA   DOTI   EPC01  FUL02  GP141B OMN01  USW01
-------------------------------------------------------------------------------
TK304140.531 #00147



USW01  00019 GAUPC 03/01/02 07:58:05 03012-935-010-000 NORMAL
Underground Notification
Ticket : 03012-935-010 Date: 03/01/02 Time: 07:53 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 1466   To:        Name:    LAKEWOOD                       AVE  SE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY.  PROPERTY IS NEAR MILLER REED AVENUE.  AERO MAP
     : 845E10.  PAINT AND STAKE BOTH SIDES OF STREET, ALL UTILITIES, MAINS AND
     : SERVICE LINES. LOCATE AS REQUESTED.  NO VERBAL MODIFICATIONS.  POSSIBLE
     : BORE.

Grids    : 3342A8422A
Work type: INSTALL/MAINTAIN GAS SERVICE
Work date: 03/06/02 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 03/06/02 Time: 07:00 Good thru: 03/22/02 Restake by: 03/19/02
RespondBy: 03/05/02 Time: 23:59 Duration :
Done for : ATLANTA GAS LIGHT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.
        : *** NEAR STREET ***   MILLER REED AVE
        : *** WILL BORE ROAD, DRIVEWAY & SIDEWALK
        : *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : SHARLENE GREEN                 Phone   : 770-942-8180
Fax     :                                Alt. Ph.:

Submitted date: 03/01/02 Time: 07:53 Oper: 935 Chan: 999
Mbrs : AGL102 ATTBB  BSCA   DOTI   EPC01  FUL02  GP141B OMN01  USW01
-------------------------------------------------------------------------------
TK301074.723 #00025



