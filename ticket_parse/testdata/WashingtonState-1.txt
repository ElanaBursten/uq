Ticket No:  4020012               2 FULL BUSINESS DAYS      DUPLICATION
Send To: 11111111   Seq No:    1  Map Ref:

Transmit      Date:  2/13/04   Time:  3:26 PM    Op: peter
Original Call Date:  1/30/04   Time: 12:24 PM    Op: peter
Work to Begin Date:  2/04/04   Time: 12:00 AM

State: MT            County: YELLOWSTONE             Place: WORDEN
Address:             Street: ROAD 15 N
Nearest Intersecting Street: OLD HWY 312

Twp: 3N      Rng29E       Sect-Q30:
Twp: 3N    Rng: 29E   Sect-Qtr: 19-SW-SE,31-NW-NE,30
Ex. Coord NW Lat: 45.9883830Lon: -108.1657900SE Lat: 45.9695900Lon: -108.1615380

Type of Work: INSTALL GUARDRAIL
Location of Work: FROM A POINT APX 1.3 MI N OF ABV INTER ON 15 MARK FROM
: NEW IRRIG BRIDGE ON BOTH SHOULDERS OF ROAD 15 APX 100 FT N AND S

Remarks: CALLER REQS MARK WITH FLAGS
: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
Overhead Lines: ?

Company     : OMO CONSTRUCTION                 Best Time:
Contact Name: CRAIG WARREN                     Phone: (406)256-0199
Email Address:
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: YELLOWSTONE COUNTY
Additional Members:
PROTEL01   USWEST66   YEVAEL01



Ticket No:  4078551               2 FULL BUSINESS DAYS
Send To: TEST03     Seq No:   67  Map Ref:

Transmit      Date:  3/26/04   Time:  3:09 PM    Op: jeffre
Original Call Date:  3/26/04   Time:  2:56 PM    Op: jeffre
Work to Begin Date:  3/31/04   Time: 12:00 AM

State: WA            County: KING                    Place: AUBURN
Address:   1901      Street: C ST SW
Nearest Intersecting Street: 15TH ST SW

Twp: 21N     Rng5E        Sect-Q19-SW,30-NW
Twp: 21N   Rng: 4E    Sect-Qtr: 25-NE,24-SE-NE
Ex. Coord NW Lat: 47.2966580Lon: -122.2334580SE Lat: 47.2832210Lon: -122.2334580

Type of Work: SOIL BORINGS
Location of Work: ABV ADD IS AT ABV INTER.  SITE IS LOC AT ABV ADD, AT
: WHAREHOUSE BUILDING NUMBER 7.  MARK 50FT RADIUS OF 5 WHITE X MARKS AT ABV
: ADD.  1ST X MARK IS APX 400FT S AND APX 50FT E OF NE BUILDING CORNER.2ND
: X MARK IS APX 310FT S AND APX 100FT E OF NE BUILDING CORNER.3RD X MARK

Remarks: IS APX 15FT S & APX 50FT E OF NE BUILDING CRNER.4TH IS APX 130FT N
: AND APX 25FT E OF NW BUILDING CORNER.5TH IS AT NW PORTION OF LOADING DOCK
Overhead Lines: N

Company     : EARTH CONSULTANTS INC            Best Time:
Contact Name: STEVE SWENSON                    Phone: (425)643-3780
Email Address:
Alt. Contact: STEVE--CELL NUMBER               Phone: (206)255-1924
Contact Fax :
Work Being Done For: ABSHER CONSTRUCTION CO.
Additional Members:
AUBURN01   KCMTRO01   MCI01      PSEELC34   PSEGAS32   USWEST45


Ticket No:  4085165               2 FULL BUSINESS DAYS
Send To: TEST03     Seq No:   19  Map Ref:

Transmit      Date:  4/01/04   Time:  8:34 AM    Op: elane
Original Call Date:  4/01/04   Time:  8:25 AM    Op: elane
Work to Begin Date:  4/06/04   Time: 12:00 AM

State: WA            County: PIERCE                  Place: PUYALLUP
Address:  17125      Street: MERIDIAN
Nearest Intersecting Street: 163RD

Twp: 19N     Rng4E        Sect-Q27:
Twp: 19N   Rng: 4E    Sect-Qtr: 22-SW-SE,21-SE,28-SE-NE,27
Ex. Coord NW Lat: 47.1103040Lon: -122.2929140SE Lat: 47.0980550Lon: -122.2830590

Type of Work: INSTALL SWR MAINLINE
Location of Work: ADD LOC AT ABV INTER. SITE LOC APX 300YDS E TO 1ST RIGHT,
: THEN APX 1000YDS S TO NW CORNER OF PROJECT. MARK ENTIRE PROP AT ABV SITE
: APX 480FT X 200FT - AREA MARKED IN WHITE FOR SPECIAL INTEREST AT SE
: CORNER OF PROP - ON S SIDE EXISTING HANGAR

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
: SITE LOC AT THUN FIELD
Overhead Lines: N

Company     : DON OLSON CONSTRUCTION           Best Time:
Contact Name: RANDY                            Phone: (253)735-0911
Email Address:
Alt. Contact: RANDY                            Phone: (253)606-6349
Contact Fax :
Work Being Done For: AUBURN LTD
Additional Members:
ATTCBL03   FIRH2001   PIERCE01   PSEELC45   PSEGAS41   SPRINT01   TACH2O01
 USWEST39   WSDOT10


Ticket No:  4090118               PRE-SURVEY
Send To: TEST03     Seq No:  185  Map Ref:

Transmit      Date:  4/05/04   Time:  2:49 PM    Op: michae
Original Call Date:  4/05/04   Time:  2:41 PM    Op: michae
Work to Begin Date:  4/07/04   Time:  9:00 AM

State: WA            County: PIERCE                  Place: GRAHAM
Address:             Street: CANYON ROAD E
Nearest Intersecting Street: 176TH ST E

Twp: 19N     Rng4E        Sect-Q31-NW
Twp:  *MORERng: 4E    Sect-Qtr: 6-NW,37-SW-NW
Ex. Coord NW Lat: 47.0966160Lon: -122.3695500SE Lat: 47.0670360Lon: -122.3561390

Type of Work: PRE SURVEY FOR PIPELINE INVESTIGATION
Location of Work: FROM ABV INTER MARK S ON BOTH SIDES OF CANYON ROAD TO
: 196TH ST, THEN MARK S THROUGH FIELD TO INTER OF 208TH ST E AND 46TH AVE.
: MARK AT ABV INTER.

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
: CALLER REQUESTS MARKS BY 04/07/2004 BY 09:00AM--NO GUAR
Overhead Lines: ?

Company     : LOCATING INC                     Best Time:
Contact Name: LONNIE PRIEST  253-377-2634      Phone: (425)396-6412
Email Address:
Alt. Contact: TIM BRYANT                       Phone: (360)815-2137
Contact Fax :
Work Being Done For: LOCATING INC
Additional Members:
ATTCBL03   MASTEL01   MCI01      PIERCE01   PSEELC45   PSEGAS40   PSEGAS41
 RAINVW01   TACH2O01   TACPWR01   USWEST39   WGPWA02    WLSCOM01


Ticket No:  4087071               PRIORITY                  UPDATE
Send To: TEST03     Seq No:   29  Map Ref:
Update Of:  4081966
Transmit      Date:  4/02/04   Time:  8:50 AM    Op: greg
Original Call Date:  4/02/04   Time:  8:43 AM    Op: greg
Work to Begin Date:  4/05/04   Time:  7:30 AM

State: WA            County: PIERCE                  Place: SUMNER
Address:   1202      Street: WOOD AVE
Nearest Intersecting Street: NORTH ST

Twp: 20N     Rng4E        Sect-Q24-SE
Twp: 20N   Rng: 4E    Sect-Qtr: 42-SW-SE,48
Ex. Coord NW Lat: 47.2059610Lon: -122.2389870SE Lat: 47.2041130Lon: -122.2369480

Type of Work: INSTALL GAS SERV
Location of Work: ADD IS APX 150FT N OF ABV INTER.  MARK FROM CENTER LINE
: OF SUMNER AVE (AT REAR OF PROP) E TO NEW BLDG AT ABV ADD.  AREA MARKED IN
: WHITE. ++CLR GAVE THOMAS GUIDE PAGE 805-J6 AND TRSQ++ ++CLR STATES HE
: WILL ALSO BE CONTACTING LOCATING COMPANY DIRECTLY++

Remarks: CALLER REQUESTS MARKS BY 04/05/2004 BY 07:30 AM ++ NO GUAR++
: UPDT TICKET -- LOCATES ARE GONE  -- NEEDS REMARKS
Overhead Lines: N

Company     : POTELCO                          Best Time:
Contact Name: MARK JANSSON                     Phone: (253)606-4256
Email Address:
Alt. Contact: JAN WOLF                         Phone: (253)841-6294
Contact Fax :
Work Being Done For: P S E
Additional Members:
ATT08      ATTCBL03   MCI01      PSEELC45   PSEGAS41   PUYLUP01   SPRINT01
 SUMNER01   USWEST39


