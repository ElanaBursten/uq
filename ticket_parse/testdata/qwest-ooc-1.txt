OOC4048157-00  541926  2004/03/31 00:24:10  00044

/

  OUNC
Ticket No:  4048157               =CREW ON SITE=
Send To: USW10      Seq No:    1  Map Ref:

Transmit      Date:  3/24/04   Time:  8:27 am    Op: lori
Original Call Date:  3/24/04   Time:  8:16 am    Op: jan
Work to Begin Date:  3/24/04   Time:  8:30 am

State: OR            County: LINN                    Place: MILLERSBURG
Address:             Street: TOWERY WAY
Nearest Intersecting Street: OLD SALEM HWY

Twp: 10S   Rng: 3W    Sect-Qtr: 16-SE,21-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL UTILITY SERVICES
Location of Work: SITE IS VACANT LOT ON SE CORNER OF ABV INTER. MARK FROM
: ABOVE INTER ALONG S SIDE OF TOWERY WAY TO 1ST POWER POLE, THEN MARK PROP
: E APX 50FT TO OLD SALEM HWY.  CALLER STATES TOWERY WAY IS NEW ROAD LOC
: APX 100YDS N OF CLEARWATER.

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P!== NO GUAR
: CALLER STATES CREW IS ON SITE NOW                                  OH: Y

Company     : WADSWORTH ENTERPRISES
Contact Name: DAMIAN FLOWERS                   Phone: (503)991-1144
Alt. Contact: JOE WADSWORTH                    Phone: (503)910-1428
Work Being Done For: REIM SCHNEIDER INVESTMENT
Additional Members:
ATT01      COA01      L3C01      NNG13      PPL13      QWEST02    SANTA04
 TCI06
OOC4045118-00  503647  2004/03/31 00:24:10  00045

/

  OUNC
Ticket No:  4045118               =CREW ON SITE=    CANCELLATION
Send To: USW23      Seq No:   21  Map Ref:
Update Of:  4039641
Transmit      Date:  3/19/04   Time: 12:58 pm    Op: seth
Original Call Date:  3/19/04   Time:  8:34 am    Op: bertha
Work to Begin Date:  3/19/04   Time:  8:45 am

State: OR            County: CLACKAMAS               Place: LAKE OSWEGO
Address: 12599       Street: FOSBERG ROAD
Nearest Intersecting Street: CARMAN DR

Twp: 2S    Rng: 1E    Sect-Qtr: 6-SE,5-SW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: LANDSCAPE
Location of Work: ADD APX 40YDS N OF ABV INTER, ON W SIDE OF FOSBERG ROAD..
: MARK ENTIRE PROP, AT THE ABV ADD. SITE IS DIRECTLY BEHIND DAYCARE AT
: FOSBERG ROAD, PARKVIEW DR INTER.   CLR GAVE THOMAS GUIDE PG 656 A5.

Remarks: == CANCELLED TO CORR ADD
: ==CALLER REQUESTS AREA MARKED A.S.A.P!==   NO GUAR                 OH: N

Company     : SUPERIOR LANDSCAPE
Contact Name: ROBERT LUSSIER                   Phone: (503)655-7377
Alt. Contact:                                  Phone:
Work Being Done For: JENNIFER NOBELS
Additional Members:
CLO01      ELI05      LGWD01     NNG01      PGE08      TCI21
OOC4042399-00  541770  2004/03/31 00:24:10  00046

/

  OUNC
Ticket No:  4042399               =CREW ON SITE=    CORRECTION
Send To: USW10      Seq No:   10  Map Ref:

Transmit      Date:  3/16/04   Time: 11:46 am    Op: john
Original Call Date:  3/16/04   Time: 11:28 am    Op: john
Work to Begin Date:  3/16/04   Time: 11:30 am

State: OR            County: LINN                    Place: ALBANY
Address:             Street: SE 1ST AVE
Nearest Intersecting Street: SE BAKER ST

Twp: 11S   Rng: 3W    Sect-Qtr: 7-NW,6-SW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL ANCHORS
Location of Work: MARK APX 20FT RADIUS OF POLE 63101, LOC ON SE CORNER OF
: ABV INTER. ==CREW IS ON SITE -- CLR STS NO MARKS AT SITE -- PLS MARK
: ASAP!!== //CORRECTED TICKET TO NOTE POLE IS MARKED IN WHITE ON SE CORNER
: OF ABV INTER.//

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P!== - NO GUAR
: UPDATE OF UNKOWN TICKET -- CLR STS NO MARKS ON SITE                OH: N

Company     : EVERGREEN UTILITY CONTRACTORS
Contact Name: AARON MORGAN                     Phone: (503)570-6257
Alt. Contact: PAUL DIX                         Phone: (503)209-4779
Work Being Done For: NOANET
Additional Members:
ATT01      COA01      MCIT01     NNG12      NNG13      PPL13      TCI06
OOC4050977-00  360693  2004/03/31 00:24:10  00049

/

OUNC
Ticket No:  4050977               ==EMERGENCY==
Send To: PNB01      Seq No:    4  Map Ref:

Transmit      Date:  3/27/04   Time:  4:19 pm    Op: ksped
Original Call Date:  3/27/04   Time:  4:12 pm    Op: ksped
Work to Begin Date:  3/29/04   Time:  7:00 am

State: OR            County: MULTNOMAH               Place: PORTLAND
Address: 5511        Street: NE 38TH AVE
Nearest Intersecting Street: KILLINGSWORTH

Twp: 1N    Rng: 1E    Sect-Qtr: 13-SE,24-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: EMERGENCY REPAIR OF SANITARY SEWER SERVICE
Location of Work: LOCATION IS IN NW CORNER OF ABOVE INTERSECTION. MARK FROM
: S SIDE OF HOUSE OUT TO THE STREET.

Remarks: --CUSTOMERS ARE WITHOUT SERVICE--CREW WILL BE ON SITE ON MONDAY,
: 3/29/04 AT 7:00 AM--                                               OH: N

Company     : ROTO-ROOTER PLUMBING SERVICES
Contact Name: TROY CLARK                       Phone: (503)682-9774
Alt. Contact: CELL                             Phone: (971)207-0052
Work Being Done For: BARBARA HEAD
Additional Members:
NNG01      PPL01      PTLD01     PTLD03

OOC4049795-00  503390  2004/03/31 00:24:10  00050

/

OUNC
Ticket No:  4049795               ==EMERGENCY==     CANCELLATION
Send To: USW32      Seq No:   15  Map Ref:

Transmit      Date:  3/25/04   Time: 12:58 pm    Op: caroly
Original Call Date:  3/25/04   Time: 12:44 pm    Op: donald
Work to Begin Date:  3/26/04   Time:  8:00 am

State: OR            County: MARION                  Place: SALEM
Address:             Street: FISHER ROAD NE
Nearest Intersecting Street: WATSON AVE NE

Twp: 7S    Rng: 2W    Sect-Qtr: 18-NW
Twp:  *MORERng: 3W    Sect-Qtr: 13-SE-NE
Legal Given:

Type of Work: REPAIR GAS SERVICE
Location of Work: SITE IS APX 200FT N OF ABV INTER. MARK AREA MARKED IN
: WHITE ON E SIDE OF FISHER ROAD BEHIND WAL MART STORE AT ABV SITE..

Remarks: CANCELLED TICKET AT CALLERS REQUEST
: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO              OH: Y

Company     : NW NATURAL
Contact Name: SHELLY NISLY X8165               Phone: (503)585-6611  Ext.: 8165
Alt. Contact: DENNIS WINDER X8147              Phone:
Work Being Done For: NW NATURAL
Additional Members:
NNG09      PGE09      SALEM01    TCI27
OOC4047739-00  503390  2004/03/31 00:24:10  00051

/

  OUNC
Ticket No:  4047739               ==EMERGENCY==     CORRECTION
Send To: USW32      Seq No:   24  Map Ref:

Transmit      Date:  3/23/04   Time: 12:56 pm    Op: kelly
Original Call Date:  3/23/04   Time: 12:43 pm    Op: tesa
Work to Begin Date:  3/23/04   Time: 12:45 pm

State: OR            County: MARION                  Place: SALEM
Address: 1045        Street: CAPITOL ST NE
Nearest Intersecting Street: D ST

Twp: 7S    Rng: 3W    Sect-Qtr: 23-SW
Twp: 7S    Rng: 3W    Sect-Qtr: 23-SW
Legal Given:

Type of Work: REPAIR SEWER MAIN
Location of Work: ADD IS APX 150FT S OF ABV INTER. MARK AREA MARKED IN
: WHITE IN ALLEY BEHIND ABV ADD. CALLER GAVE TRSQ.

Remarks: CORR:CHANGE ALT CONTACT, TYPE OF WORK, AND WHO WORK IS DONE FOR
: ==CALLER REQUESTS AREA MARKED A.S.A.P!== CUST OUT OF SERV          OH: N

Company     : CITY OF SALEM - STREET DEPT
Contact Name: LEE                              Phone: (503)588-6333
Alt. Contact: KEN RAYBURN                      Phone:
Work Being Done For: CITY OF SALEM SEWER
Additional Members:
ELI06      L3C01      NNG09      PGE09      QWEST02    SALEM01    SODGS01
 SODGS03    TCI27
OOC4049041-00  541382  2004/03/31 00:24:10  00052

/

  OUNC
Ticket No:  4049041               ==EMERGENCY==     DUPLICATION
Send To: USTOLL05   Seq No:   16  Map Ref:

Transmit      Date:  3/24/04   Time:  4:22 pm    Op: peter
Original Call Date:  3/24/04   Time:  3:27 pm    Op: peter
Work to Begin Date:  3/24/04   Time:  3:30 pm

State: OR            County: CLACKAMAS               Place: PORTLAND
Address: 8415        Street: SE KING ROAD
Nearest Intersecting Street: 82ND

Twp: 1S    Rng: 2E    Sect-Qtr: 28-SW,29-SE,32-NE,33-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: REPLACE WATER SERV
Location of Work: ABV ADD IS APX 100 YDS E FROM ABV INTER. MARK FROM WATER
: METER TO HOUSE AT ABV ADD

Remarks: CALLER GAVE THOMAS GUIDE PAGE 657 F2
: CALLERREQS MARKS ASAP                                              OH: N

Company     : JACK HOWK PLUMBING
Contact Name: JOHN DOERR                       Phone: (503)235-8784
Alt. Contact: CELL                             Phone: (503)572-4200
Work Being Done For: MEL JOHNSON
Additional Members:
CCDOT01    CCDOT02    CWD01      NNG01      ODOTRE01   PGE01      TCI26
 USW31
OOC4040670-00  503390  2004/03/31 00:24:10  00053

/

  OUNC
Ticket No:  4040670               ==EMERGENCY==
Send To: USW32      Seq No:    2  Map Ref:
Update Of:  4039869
Transmit      Date:  3/15/04   Time:  8:35 am    Op: jared
Original Call Date:  3/15/04   Time:  8:31 am    Op: jared
Work to Begin Date:  3/15/04   Time:  8:45 am

State: OR            County: MARION                  Place: WOODBURN
Address:             Street: SHENANDOAH LANE
Nearest Intersecting Street: FRONT ST

Twp: 5S    Rng: 1W    Sect-Qtr: 8-NW-NE,5-SW-SE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL STORM SERV
Location of Work: MARK FROM ABV INTER APX 600FT W ALONG FULL WIDTH
: SHENANDOAH

Remarks: =CREW ON SITE NOW=CALLER STATE DANGER OF BREAKING LINE
: CALLER REQUESTS QWEST ONLY NEED RESPOND-UNMARKED LINE PREVIOUSLY HIOH: Y

Company     : KERR CONTRACTORS, INC
Contact Name: KEVIN DONAHOO                    Phone: (503)692-5514
Alt. Contact: BILL CHISHOLM                    Phone: (971)235-5005
Work Being Done For: KERR CONTRACTORS, INC
Additional Members:
L3C01      NNG09      PGE09      QWEST02    WILBRB02   WOOD01
OOC4050690-00  541265  2004/03/31 00:24:10  00055

/

OUNC
Ticket No:  4050690               =MEET TIME=
Send To: USW13      Seq No:    2  Map Ref:

Transmit      Date:  3/26/04   Time:  3:26 pm    Op: debbie
Original Call Date:  3/26/04   Time: 12:53 pm    Op: tesa
Work to Begin Date:  3/30/04   Time:  1:30 am

State: OR            County: LINCOLN                 Place: NEWPORT
Address:             Street: NE 20TH STREET
Nearest Intersecting Street: NE CRESTVIEW DRIVE

Twp: 10S   Rng: 11W   Sect-Qtr: 32-SE
Twp:  *MORERng: 11W   Sect-Qtr: 5-NW-NE
Legal Given:

Type of Work: INSTALL 7 UNDERGROUND ELECTRIC  BOXES
Location of Work: REQUEST ALL UTILITIES TO MEET PUD AT THE INTERS. OF NE
: 20TH ST AND NE  CRESTVIEW DR FOR MARKING INSTRUCTIONS ON TUESDAY, MARCH
: 30 AT 1:3O - CALLER DID NOT PROVIDE SCOPE OF THE WORK AND WILL DO SO AT
: MEETING.

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO - CENTER
: USED THIS INFO TO PINPOINT AREA.  BEST INFO AVAILABLE.             OH: N

Company     : CENTRAL LINCOLN PUD
Contact Name: PAM PRESTON                      Phone: (541)574-2081
Alt. Contact: DON TUCKER                       Phone: (541)574-2622
Work Being Done For: PUD
Additional Members:
CLPUD02    LCRD01     NEW01      NNG16      ODOTRE02   TCI09
OOC4040883-00  541342  2004/03/31 00:24:10  00059

/

OUNC
Ticket No:  4040883               =MEET TIME=
Send To: USW01      Seq No:   34  Map Ref:
Update Of:  4040577
Transmit      Date:  3/15/04   Time:  9:48 am    Op: jennif
Original Call Date:  3/15/04   Time:  9:46 am    Op: jennif
Work to Begin Date:  3/16/04   Time:  1:30 pm

State: OR            County: LANE                    Place: EUGENE
Address: 999         Street: DIVISION AVE
Nearest Intersecting Street: BELTLINE HWY

Twp: 17S   Rng: 4W    Sect-Qtr: 12-SE,13-NE
Twp: 17S   Rng: 3W    Sect-Qtr: 7-SW,18-NW
Legal Given:

Type of Work: INSTALL WATER SRVC
Location of Work: SITE IS APX 200FT N OF INTER INSIDE GATE OF GRAVEL
: COMPANY. MARK FROM SAID POINT 2000FT E IN A 50FT WIDE SWATHE. CALLER
: REQUESTS LOCATORS TO MEET AT MAIN GATE OF ADDR AND HE WILL TAKE THEM TO
: SITE. ==UPDT TO CORRECT REQUESTED MEET TIME==

Remarks: CALLER REQUESTS MEET ON 03/16/2004 AT 01:30 PM - NO GUARANTEES
:                                                                    OH: Y

Company     : DELTA CONSTRUCTION
Contact Name: BILL MCKAY                       Phone: (541)688-2233
Alt. Contact: BILL MCKAY                       Phone: (541)228-1498
Work Being Done For: DELTA SAND AND GRAVEL
Additional Members:
EUGENE01   EWEB01     NNG08      ODOTD05    TCI02
OOC4048176-00  541382  2004/03/31 00:24:10  00061

/

  OUNC
Ticket No:  4048176               =NON-EMERGENCY=
Send To: USW16      Seq No:    3  Map Ref:
Update Of:  4043370
Transmit      Date:  3/24/04   Time:  7:38 am    Op: gayle
Original Call Date:  3/24/04   Time:  7:34 am    Op: gayle
Work to Begin Date:  3/24/04   Time:  7:45 am

State: OR            County: DESCHUTES               Place: SISTERS
Address: 68643       Street: HWY 20
Nearest Intersecting Street: HWY 126

Twp: 15S   Rng: 10E   Sect-Qtr: 9-SE-NE,10-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL FENCE
Location of Work: ADD IS APX 1/4MI S OF ABV INTER.  MARK THE HWY 20
: FRONTAGE OF THE PROP AND MARK FROM HWY 20 APX 100FT W DOWN ENTRANCE ROAD
: AT ABV ADD.  UPDATE CALLER STATES NEED TO MARK APX 15FT STRIP OFF W EDGE
: OF ROAD FOR APX 500FT S FROM SOUTHERNMOST DRIVEWAY OF ABV ADD

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P!==-NO GUARANTEES
: CREW WILL BE ON SITE BY 8:00AM TODAY                               OH: N

Company     : AMERICAN FENCE CO
Contact Name: DOUG TREISCH 541/948-0769        Phone: (541)923-5858
Alt. Contact: DOUG CELL                        Phone: (541)948-0769
Work Being Done For: SISTERS ATHLETIC CLUB
Additional Members:
BCC02      CEC01      SISTER01
OOC4042340-00  503362  2004/03/31 00:24:10  00062

/

  OUNC
Ticket No:  4042340               =NON-EMERGENCY=   CANCELLATION
Send To: USW07      Seq No:   52  Map Ref:
Update Of:  4040783
Transmit      Date:  3/16/04   Time: 11:33 am    Op: elane
Original Call Date:  3/16/04   Time: 10:55 am    Op: chrisj
Work to Begin Date:  3/16/04   Time: 11:00 am

State: OR            County: MARION                  Place: SALEM
Address: 165         Street: JAMESTOWN ST SE
Nearest Intersecting Street: INDEPENDENCE DR SE

Twp: 8S    Rng: 3W    Sect-Qtr: 11-SW-NW-SE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: REPLACE WATER SERVICE
Location of Work: ADD IS WITHIN 3 BLK W OF ABV INTER.  MARK AREA MARKED IN
: WHITE AT ABV ADD.SITE IS LOCATED N OF KUEBLER AND S W OF BATTLE CREEK
: RD AND IS NORTH OFF OF INDEPENDENCE.

Remarks: CALLER REQUESTING MARKS A.S.A.P    NO GUARANTEE
: CANCEL TKT - WRONG ADDRESS                                         OH: N

Company     : JUDSONS HEATING AND PLUMBING
Contact Name: LORI HUTCHINSON                  Phone: (503)363-4141
Alt. Contact: RENEE DICK                       Phone: (503)363-4141
Work Being Done For: LEONARD AND IRENE MONTGOMERY
Additional Members:
NNG09      PGE04      SALEM01    TCI36
OOC4041667-00  503390  2004/03/31 00:24:10  00063

/

  OUNC
Ticket No:  4041667               =NON-EMERGENCY=   CORRECTION
Send To: USW32      Seq No:   32  Map Ref:

Transmit      Date:  3/15/04   Time:  3:59 pm    Op: zach
Original Call Date:  3/15/04   Time:  3:53 pm    Op: zach
Work to Begin Date:  3/15/04   Time:  4:00 pm

State: OR            County: MARION                  Place: KEIZER
Address: 4821        Street: 10TH PL N
Nearest Intersecting Street: ESPANA

Twp: 7S    Rng: 3W    Sect-Qtr: 3
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: REPLACE LAWN
Location of Work: ADD IS AT INTER. MARK ENTIRE PROP.

Remarks: CORRECTED: PLEASE CALL BEFORE LOCATING.
: ==CALLER REQUESTS AREA MARKED A.S.A.P!== NO GUAR                   OH: N

Company     :
Contact Name: MIKE GERSZTYN                    Phone: (503)463-8381
Alt. Contact: SUSAN                            Phone:
Work Being Done For: MIKE GERSZTYN
Additional Members:
CKWD01     NNG09      PGE09      SALEM01    SELEC01    TCI27
OOC4001443-00  360693  2004/03/31 00:24:10  00064

/

  OUNC
Ticket No:  4001443               =NON-EMERGENCY=   DUPLICATION
Send To: PNB02      Seq No:    1  Map Ref:

Transmit      Date:  2/06/04   Time: 10:40 am    Op: jdasho
Original Call Date:  2/04/04   Time: 11:35 am    Op: jdasho
Work to Begin Date:  2/04/04   Time: 11:45 am

State: WA            County: YOUR COUNTY             Place: YOUR CITY
Address:             Street: PLEASE READ INSTRUCTIONS BELOW
Nearest Intersecting Street: PLEASE READ INSTRUCTIONS BELOW

Twp: 1N    Rng: 2W    Sect-Qtr: 3-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: THIS IS A TEST OF NEW CALL CENTER COMMUNICATIONS
Location of Work: THIS IS A TEST OF THE NEW CALL CENTER COMMUNICATIONS
: SOFTWARE/HARDWARE.  IN ORDER TO VERIFY THAT YOUR COMPANY IS ABLE TO
: RECEIVE FROM THE CENTER, PLEASE FAX ANY COPIES OF THIS TICKET BACK TO US
: AT 503-234-7254, INCLUDING ANY DUPLICATES. IF YOU WANT LARGER PRINT

Remarks: DARKER PRINT, ETC. PLEASE INDICATE THAT ON YOUR FAX BACK, PLEASE
: RESPOND, EVEN IF YOU HAVE RESPONDED TO 1ST TEST-THANKS  ATTN JENN DOH: N

Company     : ONE CALL CONCEPTS
Contact Name: JENN DASHO                       Phone: (503)232-1987  Ext.: 4001
Alt. Contact: SAME                             Phone: (877)668-4001
Work Being Done For: UTILITY NOTIFICATION CENTER
Additional Members:
ATT02      ATTCBL10   KANEB01    KCW11101   KENT01     MCI01      QWEST03
 QWEST05    SEASIG01   SPOKAN01   SPRINT01   SPRINT02   USP04      USW38
 USWEST34   XPRESS01
OOC4040578-00  503655  2004/03/31 00:24:10  00065

/

  OUNC
Ticket No:  4040578               =NON-EMERGENCY=
Send To: USW31      Seq No:    7  Map Ref:
Update Of:  4038693
Transmit      Date:  3/15/04   Time:  8:03 am    Op: adrian
Original Call Date:  3/15/04   Time:  7:58 am    Op: adrian
Work to Begin Date:  3/15/04   Time:  8:00 am

State: OR            County: CLACKAMAS               Place: WEST LINN
Address: 6548        Street: ARTEMIS CT
Nearest Intersecting Street: ARTEMIS ROAD

Twp: 2S    Rng: 1E    Sect-Qtr: 24-SW,23-SE,26-NE,25-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL SPRINKLER SYSTEM
Location of Work: ADD APX 40-60FT N FROM INTER. MARK ENTIRE PROP AT ABV
: ADD. UPDTD: CALLER STATES SITE NOT MARKED FOR NEEDED UTILITIES - ONLY
: WATER AND SEWER HAS BEEN MARKED - UTILITIES PLS LOCATE.

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P!==-NO GUAR-
: CALLER`S T.G. PG 686-J6 ------ DO NOT MARKED PAVED/PERMANENT SURFACOH: N

Company     : RAINMAN IRRIGATION
Contact Name: JERRY SPEARS                     Phone: (503)788-6411
Alt. Contact: JERRY SPEARS                     Phone: (503)849-9511
Work Being Done For: JON ROBINSON
Additional Members:
CWL01      NNG01      PGE08      TCI21OOC4047919-00  541382  2004/03/31 00:24:10  00066

/

OUNC
Ticket No:  4047919               PRE-SURVEY
Send To: USW16      Seq No:   45  Map Ref:

Transmit      Date:  3/23/04   Time:  2:34 pm    Op: gabrie
Original Call Date:  3/23/04   Time:  2:26 pm    Op: gabrie
Work to Begin Date:  4/06/04   Time:  2:30 pm

State: OR            County: DESCHUTES               Place: BEND
Address: 345         Street: SW CENTURY DR
Nearest Intersecting Street: SIMPSON

Twp: 18S   Rng: 12E   Sect-Qtr: 6-NE
Twp: 18S   Rng: 12E   Sect-Qtr: 6
Legal Given:

Type of Work: PRE-SURVEY TO DESIGN NEW CONSTR
Location of Work: ADD IS APX 500FT S OF ABV INTER.  MARK ENTIRE PROP, INCL
: ENTIRE ROAD IN FRONT OF ABV ADD.

Remarks: CALLER STATES FIELD MARKINGS ARE NECESSARY
: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO              OH: N

Company     : BAXTER LAND SURVEYING COR.
Contact Name: KEITH POWELL                     Phone: (541)382-1962
Alt. Contact: TIM CHAPMAN                      Phone:
Work Being Done For: ELVIN SPERLING
Additional Members:
BCC01      BEND01     CNG03      COID01     PPL20
OOC4037913-00  541342  2004/03/31 00:24:10  00067

/

  OUNC
Ticket No:  4037913               PRE-SURVEY        CANCELLATION
Send To: USW01      Seq No:   12  Map Ref:

Transmit      Date:  3/10/04   Time:  9:49 am    Op: tesa
Original Call Date:  3/10/04   Time:  9:38 am    Op: tesa
Work to Begin Date:  3/24/04   Time:  9:45 am

State: OR            County: LINN                    Place: HARRISBURG
Address:             Street: HWY 99 E
Nearest Intersecting Street: POWERLINE ROAD

Twp: 14S   Rng: 3W    Sect-Qtr: 18
Twp: 14S   Rng: 4W    Sect-Qtr: 35-SE-NE,36-SW-NW
Legal Given:

Type of Work: PRE-SURVEY TO DESIGN BRIDGE REPLACEMENT
Location of Work: SITE IS AT S SIDE OF ABV INTER. MARK HWY 99E FROM ABV
: INTER APX 1500FT N FROM W R.O.W. TO DITCH BETWEEN HWY AND RAILROAD
: TRACKS. CALLER STATES SITE IS MARKED WITH SILVER AT BEGINNING AND END OF
: SITE.

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO
: CALLER REQUESTS MARKS ONLY-NO MAPS NECESSARY CANCEL-WRONG TRSQ     OH: Y

Company     : ODOT
Contact Name: HEATH MCCONNEL                   Phone: (541)686-7849
Alt. Contact: CELL                             Phone: (541)954-8153
Work Being Done For: ODOT
Additional Members:
CPI02      L3C01      NNG13      PFL03      QWEST02
OOC4050370-00  503647  2004/03/31 00:24:10  00068

/
OUNC
Ticket No:  4050370               PRE-SURVEY        CORRECTION
Send To: USW23      Seq No:   17  Map Ref:

Transmit      Date:  3/26/04   Time:  9:25 am    Op: kelly
Original Call Date:  3/26/04   Time:  9:00 am    Op: kelly
Work to Begin Date:  4/09/04   Time: 10:15 am

State: OR            County: CLACKAMAS               Place: LAKE OSWEGO
Address: 16495       Street: HWY 43
Nearest Intersecting Street: BURNHAM ROAD

Twp: 2S    Rng: 1E    Sect-Qtr: 10-SE
Twp: 2S    Rng: 1E    Sect-Qtr: 10-SE,11-SW
Legal Given:

Type of Work: PRE SURVEY FOR DESIGN
Location of Work: ADD IS APX 400FT S OF ABV INTER, MARK ENTIRE HIGHWAY,
: R.O.W TO R.O.W, FROM APX 200FT S OF ABV INTER.. S APX 400FT
: CORR: AREA MAY BE MARKED IN WHITE

Remarks: HWY 43 IS AKA SW PACIFIC HWY          PLS SEND MAPS TO TIM A.
:   15580 SW JAY ST  STE 200 BEAVERTON, OR 97006/CLR GAVE TRSQ 10DD  OH: N

Company     : KURAHASHI AND ASSOCIATES
Contact Name: TIM ALCOVER                      Phone: (503)644-6842
Alt. Contact: MIKE RENNICK                     Phone: (503)644-6842
Work Being Done For: KURAHASHI AND ASSOCIATES
Additional Members:
CLO01      NNG01      PGE08      TCI21
OOC4047122-00  541672  2004/03/31 00:24:10  00069

/
OUNC
Ticket No:  4047122               PRE-SURVEY        DUPLICATION
Send To: USW29      Seq No:   17  Map Ref:

Transmit      Date:  3/23/04   Time: 10:49 am    Op: elane
Original Call Date:  3/23/04   Time:  7:38 am    Op: elane
Work to Begin Date:  4/06/04   Time:  9:00 am

State: OR            County: DOUGLAS                 Place: ROSEBURG
Address:             Street: GREENSIDING ROAD
Nearest Intersecting Street: HWY 99 S

Twp: 28S   Rng: 6W    Sect-Qtr: 11-NW
Twp: 28S   Rng: 6W    Sect-Qtr: 11-NW-NE
Legal Given:

Type of Work: PRE-SURVEY TO DESIGN STORM DRAIN
Location of Work: MARK FROM ABV INTER W APX 300FT TO RR TRACK - AREA MARKED
: IN WHITE - CALLER REQUEST MARKINGS

Remarks: ROAD 110 AKA GREENSIDING ROAD
: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO              OH: Y

Company     : DOUGLAS COUNTY PUB. WORKS ENG.
Contact Name: DUANE ANDRY                      Phone: (541)440-4481
Alt. Contact: PAULA                            Phone:
Work Being Done For: DOUGLAS COUNTY PUB. WORKS ENG.
Additional Members:
GSD01      JNS02      PPL26      QWEST02    RCWD01     USP01      WPNG04
OOC4039359-00  503647  2004/03/31 00:24:10  00071

/

OUNC
Ticket No:  4039359               PRIORITY
Send To: USW23      Seq No:   39  Map Ref:
Update Of:  4035692
Transmit      Date:  3/11/04   Time:  2:05 pm    Op: peter
Original Call Date:  3/11/04   Time:  2:03 pm    Op: peter
Work to Begin Date:  3/12/04   Time: 10:00 am

State: OR            County: MULTNOMAH               Place: PORTLAND
Address: 4215        Street: SW 44TH AVE
Nearest Intersecting Street: SW 47TH DR

Twp: 1S    Rng: 1E    Sect-Qtr: 7-SE,18-NE,17-NW,8-SW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: PLANTING/GARDENING
Location of Work: ADD IS APX 2 HOUSES N OF INTER. MARK ENTIRE PROPERTY AT
: THIS LOCATION

Remarks: UPDT=CALLER STATES ONLY GAS HAS MRKD=REQS MRX BY 10AM 3/12 NO GUAR
: BEST INFO AVAILABLE                                                OH: N

Company     :
Contact Name: JEANNIE MALLORY                  Phone: (503)291-0160
Alt. Contact: CELL                             Phone: (503)348-5194
Work Being Done For: JEANNIE MALLORY
Additional Members:
NNG01      PGE08      PTLD01     PTLD03     TCI21
OOC4037534-00  503655  2004/03/31 00:24:10  00072

/

OUNC
Ticket No:  4037534               PRIORITY          CANCELLATION
Send To: USW31      Seq No:   20  Map Ref:

Transmit      Date:  3/12/04   Time:  3:36 pm    Op: chrisj
Original Call Date:  3/09/04   Time:  3:33 pm    Op: david
Work to Begin Date:  3/10/04   Time:  9:00 am

State: OR            County: CLACKAMAS               Place: MILWAUKIE
Address: 6879        Street: SE MONROE ST
Nearest Intersecting Street: 72ND ST

Twp: 1S    Rng: 2E    Sect-Qtr: 32-NW-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL ALL UTILITY SERVICES
Location of Work: ADD IS APX 1/4MI W OF INTER.  MARK BOTH SIDES OF MONROE
: ST IN FRONT OF ABV ADD.

Remarks: CALLER REQUESTS MARKS BY 03/10/2004 BY 09:00 AM-NO GUAR
: TICKET CANCELED DUE TO WRONG ADDRESS.                              OH: Y

Company     :
Contact Name: VITALLY BATRANCHUK               Phone: (503)740-0520
Alt. Contact: MARIN NITA                       Phone: (503)209-0941
Work Being Done For: VITALLY BATRANCHUK
Additional Members:
CCDOT01    CCDOT02    CWD01      JNS01      NNG01      PGE01      TCI26
OOC4032402-00  503390  2004/03/31 00:24:10  00074

/

OUNC
Ticket No:  4032402               PRIORITY          DUPLICATION
Send To: USW32      Seq No:   21  Map Ref:

Transmit      Date:  3/02/04   Time: 10:42 am    Op: zach
Original Call Date:  3/02/04   Time: 10:23 am    Op: zach
Work to Begin Date:  3/03/04   Time: 10:00 am

State: OR            County: MARION                  Place: HUBBARD
Address:             Street: D ST
Nearest Intersecting Street: 5TH

Twp: 4S    Rng: 1W    Sect-Qtr: 33-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL STORM LINE
Location of Work: MARK AREA MARKED IN WHITE AT ABV INTER.

Remarks:
: CALLER REQUESTS MARKS BY 3/3/2004 BY 10AM - NO GUAR                OH: N

Company     : JEFF KERSEY CONSTRUCTION
Contact Name: JEFF KERSEY                      Phone: (503)351-4412
Alt. Contact: SAME                             Phone:
Work Being Done For: CITY OF HUBBARD
Additional Members:
HUB01      NNG09      PGE09      WILBRB02

OOC4050984-00  503362  2004/03/31 00:24:10  00075

/

OUNC
Ticket No:  4050984               PRIORITY
Send To: USW07      Seq No:    6  Map Ref:
Update Of:  4049449
Transmit      Date:  3/27/04   Time:  7:40 pm    Op: ksmike
Original Call Date:  3/27/04   Time:  7:32 pm    Op: ksmike
Work to Begin Date:  3/29/04   Time:  8:00 am

State: OR            County: MARION                  Place: SALEM
Address: 340         Street: 14TH ST SE
Nearest Intersecting Street: MILL ST

Twp: 7S    Rng: 3W    Sect-Qtr: 26-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSALL BULLARDS AROUND GAS METER
Location of Work: ADD IS 340 1/2 LOC APX 1/2 BLK N OF ABV INTER SITE IS
: ALLEY PROPERTY    ..   MARK FROM GAS METER TO EDGE OF PROPERTY LINE IN
: ALLEY AT ABV ADD .-MARKED THE WRONG ADDRESS ,MARK 340 1/2

Remarks: CALLER STATES THIS IS 340 1/2 14TH ST SE-CALLER STATES ONLY
: NW  NATURAL NEEDS TO RESPOND AND REMARK THE AREA.                  OH: N

Company     :
Contact Name: ROGER MONETTE                    Phone: (503)851-3370
Alt. Contact:                                  Phone: (503)390-4657
Work Being Done For: ROGER MONETTE
Additional Members:
L3C01      NNG09      PGE04      QWEST02    SALEM01    SODGS01    TCI36
OOC4001443-00  360693  2004/03/31 00:24:10  00076

/

  OUNC
Ticket No:  4001443               =NON-EMERGENCY=
Send To: PNB02      Seq No:    1  Map Ref:
Send To: USW38      Seq No:    1  Map Ref:

Transmit      Date:  2/04/04   Time: 11:45 am    Op: jdasho
Original Call Date:  2/04/04   Time: 11:35 am    Op: jdasho
Work to Begin Date:  2/04/04   Time: 11:45 am

State: WA            County: YOUR COUNTY             Place: YOUR CITY
Address:             Street: PLEASE READ INSTRUCTIONS BELOW
Nearest Intersecting Street: PLEASE READ INSTRUCTIONS BELOW

Twp: 1N    Rng: 2W    Sect-Qtr: 3-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: THIS IS A TEST OF NEW CALL CENTER COMMUNICATIONS
Location of Work: THIS IS A TEST OF THE NEW CALL CENTER COMMUNICATIONS
: SOFTWARE/HARDWARE.  IN ORDER TO VERIFY THAT YOUR COMPANY IS ABLE TO
: RECEIVE FROM THE CENTER, PLEASE FAX ANY COPIES OF THIS TICKET BACK TO US
: AT 503-234-7254, INCLUDING ANY DUPLICATES. IF YOU WANT LARGER PRINT

Remarks: DARKER PRINT, ETC. PLEASE INDICATE THAT ON YOUR FAX BACK, PLEASE
: RESPOND, EVEN IF YOU HAVE RESPONDED TO 1ST TEST-THANKS  ATTN JENN DOH: N

Company     : ONE CALL CONCEPTS
Contact Name: JENN DASHO                       Phone: (503)232-1987  Ext.: 4001
Alt. Contact: SAME                             Phone: (877)668-4001
Work Being Done For: UTILITY NOTIFICATION CENTER
Additional Members:
ATT02      ATTCBL10   KANEB01    KCW11101   KENT01     MCI01      QWEST03
 QWEST05    SEASIG01   SPOKAN01   SPRINT01   SPRINT02   USP04      USWEST34
 XPRESS01
OOC4049760-00  503362  2004/03/31 00:24:10  00077

/

OUNC
Ticket No:  4049760               48 HOUR NOTICE    CANCELLATION
Send To: USW07      Seq No:   30  Map Ref:

Transmit      Date:  3/25/04   Time:  3:29 pm    Op: seth
Original Call Date:  3/25/04   Time: 11:51 am    Op: elane
Work to Begin Date:  3/29/04   Time: 12:00 pm

State: OR            County: POLK                    Place: INDEPENDENCE
Address: 6564        Street: CORVALLIS RD
Nearest Intersecting Street: INDEPENDENCE WAY

Twp: 8S    Rng: 4W    Sect-Qtr: 28-SW
Twp: 8S    Rng: 4W    Sect-Qtr: 33-NW-NE,28
Legal Given:

Type of Work: INSTALLING SHORT SIDE GAS SERVICE
Location of Work: SITE APX 100FT SOUTH OF ABOVE INTERSECTION.  MARK FROM
: GAS METER  LOCATION ON RIGHT SIDE OF HOUSE OUT TO CORVALLIS RD.

Remarks: CALLER GAVE TSRQ===DIRECTIONS ARE WHEN FACING HOUSE FROM
: STREET=== ====    CANCELLED TO CORRECT ADD =====                   OH: Y

Company     : NW NATURAL
Contact Name: SHELLY NISLY X8165               Phone: (503)585-6611  Ext.: 8165
Alt. Contact: DENNIS WINDER X8147              Phone:
Work Being Done For: NW NATURAL
Additional Members:
COI01      FALCON15   MON01      MON02      NNG09      PPL14      USP01
OOC4050964-00  541926  2004/03/31 00:24:10  00078

/

OUNC
Ticket No:  4050964               48 HOUR NOTICE    CORRECTION
Send To: USW10      Seq No:    2  Map Ref:

Transmit      Date:  3/27/04   Time:  1:34 pm    Op: mnroxi
Original Call Date:  3/27/04   Time:  1:24 pm    Op: mnroxi
Work to Begin Date:  3/31/04   Time:  8:00 am

State: OR            County: LINN                    Place: ALBANY
Address: 2755        Street: 41ST AVE SE
Nearest Intersecting Street: MORAGA

Twp: 11S   Rng: 3W    Sect-Qtr: 20-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALLATION OF FENCE
Location of Work: ADDRESS IS APPROX 500FT N OF INTERSECTION. MARK ENTIRE
: REAR HALF OF LOT.

Remarks: CALLER STATES DOGS IN BACK YARD PLEASE XCALL BEFORE LOCATING
:                                                                    OH: N

Company     :
Contact Name: MARK HURLIMAN                    Phone: (541)905-1919
Alt. Contact: WORK                             Phone: (503)947-7437
Work Being Done For: MARK HURLIMAN
Additional Members:
COA01      CPI02      NNG13      PPL13      TCI06
OOC4050640-00  541276  2004/03/31 00:24:10  00079

/

OUNC
Ticket No:  4050640               48 HOUR NOTICE    DUPLICATION
Send To: USW02      Seq No:   11  Map Ref:

Transmit      Date:  3/26/04   Time: 12:44 pm    Op: greg
Original Call Date:  3/26/04   Time: 12:10 pm    Op: greg
Work to Begin Date:  3/30/04   Time: 12:15 pm

State: OR            County: UMATILLA                Place: HERMISTON
Address: 355         Street: W THEATER LANE
Nearest Intersecting Street: HWY 395

Twp: 4N    Rng: 28E   Sect-Qtr: 2-SW-NW,3-SE-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL SPRINKLER SYSTEM
Location of Work: ADD APX 800FT W OF ABV INTER, MARK ENTIRE EMPTY LOT BTWN
: THEATER LANE AND NEW CONSTRUCTION, CALLER STATES CONSTRUCTION SHACK
: SITTING ON THIS EMPTY LOT

Remarks: CENTER MAP SHOWS W THEATER LANE AS W THEATRE LANE
:                                                                    OH: N

Company     : ENVIRONMENTAL LANDSCAPE
Contact Name: MIKE MARTIN                      Phone: (541)476-9228
Alt. Contact: CELL                             Phone: (541)291-2375
Work Being Done For: STAN SMITH AND ASSOC
Additional Members:
CNG02      EORTEL01   HERM01     ODOTD12    TCI22      UECA01
OOC4042469-00  503655  2004/03/31 00:24:10  00080

OUNC
Ticket No:  4042469               48 HOUR NOTICE
Send To: USW31      Seq No:   18  Map Ref:
Update Of:  4039684
Transmit      Date:  3/16/04   Time: 12:15 pm    Op: tonily
Original Call Date:  3/16/04   Time: 12:13 pm    Op: tonily
Work to Begin Date:  3/18/04   Time: 12:15 pm

State: OR            County: CLACKAMAS               Place: MILWAUKIE
Address: 16891       Street: SE CORSAGE
Nearest Intersecting Street: JENNINGS

Twp: 2S    Rng: 2E    Sect-Qtr: 9-SW,8-SE,17-NE,16-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: REPLACE WATER SERV
Location of Work: ADD IS APX 1 BLK S OF ABV INTER. MARK FROM WATER METER TO
: HOUSE AT ABV ADD ON S SIDE OF PROP, INCL E ROW OF SE CORSAGE FOR LENGTH
: OF PROP AT ABV ADD.==UPD TKT==CHANGE CALLER INFO NO REMARKS ARE NEEDED.

Remarks: CALLER REQUESTS MARKS BY 03/15/2004 BY 12:00 PM==NO GUAR
:                                                                    OH: N

Company     : LINESCAPE
Contact Name: RICH                             Phone: (503)969-4409
Alt. Contact: DOUG                             Phone: (503)969-5204
Work Being Done For: KIRK HANSON
Additional Members:
CCDOT01    CCDOT02    CWD01      NNG01      OLWD01     PGE01      TCI26
OOC4050870-00  503275  2004/03/31 00:24:10  00081

/

OUNC
Ticket No:  4050870               SHORT NOTICE
Send To: PNB01      Seq No:   66  Map Ref:

Transmit      Date:  3/26/04   Time:  3:52 pm    Op: bryan
Original Call Date:  3/26/04   Time:  3:47 pm    Op: bryan
Work to Begin Date:  3/30/04   Time:  8:00 am

State: OR            County: MULTNOMAH               Place: PORTLAND
Address: 4835        Street: SE 37TH AVE
Nearest Intersecting Street: SE SCHILLER ST

Twp: 1S    Rng: 1E    Sect-Qtr: 13-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: SOIL BORINGS
Location of Work: ADD IS WITHIN APX 1/2BLK S OF INTER.  MARK ENTIRE PROP AT
: ABV ADD

Remarks: CALLER GAVE THOMAS GUIDE PAGE 627 B-3
: CALLER REQUESTS MARKS BY 03/30/2004 BY 08:00 AM==NO GUAR           OH: N

Company     : CASCADE ENVIRONMENTAL SERVICES
Contact Name: KARI ROBERTS                     Phone: (503)233-1193
Alt. Contact: SAME                             Phone:
Work Being Done For: ABRAHAMSON
Additional Members:
NNG01      PARA01     PGE01      PTLD01     PTLD03
OOC4006286-00  503275  2004/03/31 00:24:10  00082

OUNC
Ticket No:  4006286               SHORT NOTICE      CANCELLATION
Send To: PNB01      Seq No:   85  Map Ref:
Update Of:   236381
Transmit      Date:  1/16/04   Time:  2:39 pm    Op: mark
Original Call Date:  1/16/04   Time:  2:01 pm    Op: jared
Work to Begin Date:  1/20/04   Time:  2:15 pm

State: OR            County: MULTNOMAH               Place: PORTLAND
Address: 3031        Street: SE 174TH AVE
Nearest Intersecting Street: SE BROOKLYN ST

Twp: 1S    Rng: 3E    Sect-Qtr: 7
Twp: 1S    Rng: 3E    Sect-Qtr: 7-NW-NE
Legal Given:

Type of Work: 4-3117 SERVICE EM RES CONVERSION
Location of Work: MARK ENTIRE ROW AND LOT AT ABV ADD, SHORTSIDE GAS SERV

Remarks:
: CANCEL TICKET-NO REMARKS NEEDED                                    OH: ?

Company     : NW NATURAL/MT SCOTT CENTER
Contact Name: MIKE SCHMIDT                     Phone: (503)226-4211  Ext.: 6740
Alt. Contact:                                  Phone:
Work Being Done For: NW NATURAL/MT SCOTT CENTER
Additional Members:
NNG01      PARA01     PGE01      PTLD03     PVW01      ZNNG54
OOC4036641-00  541342  2004/03/31 00:24:10  00083

/

OUNC
Ticket No:  4036641               SHORT NOTICE      CORRECTION
Send To: USW01      Seq No:    7  Map Ref:

Transmit      Date:  3/09/04   Time:  8:00 am    Op: crysta
Original Call Date:  3/09/04   Time:  7:46 am    Op: crysta
Work to Begin Date:  3/10/04   Time:  8:00 am

State: OR            County: LANE                    Place: EUGENE
Address: 5361        Street: JEFFERY WAY
Nearest Intersecting Street: PRASLINE

Twp: 17S   Rng: 4W    Sect-Qtr: 20
Twp: 17S   Rng: 4W    Sect-Qtr: 20-NE
Legal Given:

Type of Work: INSTALL CATCH BASINS
Location of Work: ADD IS LOT 334 LOC AT ABV INTER. MARK ENTIRE LOT INCL ALL
: ROWS AND EASEMENTS.====CORR TO ADD CENTER FINDS PRASLIN=====

Remarks: CALLER REQUESTS MARKS BY 03/10/2004 BY 08:00 AM==NO GUAR
: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO              OH: N

Company     : NEWLINE CONSTRUCTION
Contact Name: BARBARA FOSTER                   Phone: (503)393-2900
Alt. Contact: PAUL                             Phone: (503)932-5202
Work Being Done For: FOOTE DEVELOPMENT
Additional Members:
EUGENE01   EWEB01     NNG08      TCI02
OOC4049985-00  541770  2004/03/31 00:24:10  00085

OUNC
Ticket No:  4049985               SHORT NOTICE
Send To: USW27      Seq No:   61  Map Ref:
Update Of:  4040308
Transmit      Date:  3/25/04   Time:  2:30 pm    Op: peter
Original Call Date:  3/25/04   Time:  2:27 pm    Op: peter
Work to Begin Date:  3/29/04   Time:  8:00 am

State: OR            County: JACKSON                 Place: MEDFORD
Address:             Street: HWY 62
Nearest Intersecting Street: POPLAR DR

Twp: 37S   Rng: 1W    Sect-Qtr: 18-SE
Twp: 37S   Rng: 1W    Sect-Qtr: 18-SW-NW
Legal Given:

Type of Work: INSTALL WOOD POLE/TRENCHING ELEC SERVICE
Location of Work: MARK AREA MARKED IN SILVER AT SE CORNER OF ABV INTER.

Remarks: UPDT TT-MRKS GONE-NEEDS REMARKS!!AS ACCURATE AS POSSIBLE PLEASE!!
: CALLER GAVE TRSQ=CALLER REQS MARKS BY 8AM ON 3/29=NO GUAR          OH: Y

Company     : ODOT
Contact Name: MIKE DENNIS                      Phone: (541)474-3149  Ext.: 5
Alt. Contact: MIKE CELL                        Phone: (541)621-2329
Work Being Done For: ODOT
Additional Members:
ATT01      BCVSA01    MEDFRD01   MWC01      ODOTD08    PPL25      TCI18
 WPNG03

OOC2004043000050	541276	2004/04/30 15:06:26	00001

OUNC
Ticket No:  4076717               48 HOUR NOTICE
Send To: USW02      Seq No:    1  Map Ref:

Transmit      Date:  4/30/04   Time:  7:17 am    Op: julie
Original Call Date:  4/30/04   Time:  7:13 am    Op: julie
Work to Begin Date:  5/04/04   Time:  8:00 am

State: OR            County: UMATILLA                Place: PENDLETON
Address:             Street: HIGHWAY 331
Nearest Intersecting Street: MISSION ROAD

Twp: 2N    Rng: 33E   Sect-Qtr: 9-SE-NE,10-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL TELE MAIN
Location of Work: MARK FROM SE CORNER OF INTER S ALONG E SIDE OF HWY APX
: 600FT.

Remarks: MISSION AKA MARKET
:                                                                    OH: N

Company     : C2 UTILITY CONTRACTORS INC
Contact Name: AARON FIELDEN                    Phone: (541)388-2933
Alt. Contact: LES LIGHT                        Phone: (541)419-5505
Work Being Done For: QWEST
Additional Members:
CNG02      COPEN01    CTUIR01    ODOTD12    PPL05      UCPWD01


