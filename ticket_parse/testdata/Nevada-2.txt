
CHACOM 00003 USAN 05/15/06 12:08:39 0166872 NORMAL NOTICE

Message Number: 0166872 Received by USAN at 11:50 on 05/15/06 by BMC

Work Begins:    05/17/06 at 12:00   Notice: 020 hrs      Priority: 2

Caller:         TIM BOYCE
Company:        BOYCE INC.
Address:        POB 547
City:           LOGANDALE                     State: NV Zip: 89021
Telephone:      702-303-7733                  Fax: 702-398-3170
Alt #(s):       702-398-3070
Nature of Work: DIG TO LOC SEPTIC TANK FOR REP
Done for:       P/O DEEDE                     Explosives: NO
Foreman:        ABV                           Area Marked in White Paint
Permit Type:    NO                            
Location: 
Street Address:         3065  LISTON AVE
  Cross Street:         SAINT JOSEPH ST
    WRK ON SOUTH SIDE OF PROP AT ADDR & EXT APP 50' INTO PROP AREA

Place: LOGANDALE, CO AREA           County: CLARK                State: NV

Map Book: 
Page Grid
0000 000
Long/Lat Long: -114.466278 Lat:  36.596245 Long: -114.456291 Lat:  36.598465 
State Grid  E:  0 N:  0 E:   0 N:  0

Sent to:
ATTNEV = AT&T COMM NEVADA             CHACOM = CHARTER COMMUNICATIONS       
MOAPA  = MOAPA VLY TELEPHONE          MOAWTR = MOAPA VALLEY WTR DIST        
MUDIRR = MUDDY VALLEY IRRIGATION      OVRPWR = OVERTON POWER DIST#5         


