
0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: CNTV01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  1067540-48HR |   5  1067732-UPDT |   9  1067782-UPDT |  12  1068413-ETKT |
   2  1067691-48HR |   6  1067734-UPDT |  10  1067836-ETKT |  13  1068460-ETKT |
   3  1067710-UPDT |   7  1067735-UPDT |  11  1068099-48HR |  14  1068656-UPDT |
   4  1067711-UPDT |   8  1067736-UPDT |

* indicates ticket # is repeated

Total Tickets: 14

48HR - STANDARD                       |    3
UPDT - UPDATE                         |    8
ETKT - EMERGENCY                      |    3

Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03



0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: DSTS45

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  1067507-48HR |  22  1067716-UPDT |  43  1068143-48HR |  64  1068238-UPDT |
   2  1067508-48HR |  23  1067722-UPDT |  44  1068149-48HR |  65  1068084-PTKT |
   3  1067509-48HR |  24  1067724-UPDT |  45  1068158-48HR |  66  1068258-UPDT |
   4  1067511-48HR |  25  1067727-UPDT |  46  1068163-48HR |  67  1068259-UPDT |
   5  1067512-48HR |  26  1067731-UPDT |  47  1068166-48HR |  68  1068260-UPDT |
   6  1067513-48HR |  27  1067732-UPDT |  48  1068170-48HR |  69  1068261-UPDT |
   7  1067515-48HR |  28  1067734-UPDT |  49  1068175-48HR |  70  1068262-UPDT |
   8* 1067516-48HR |  29  1067735-UPDT |  50  1068180-48HR |  71  1068278-UPDT |
   9  1067518-48HR |  30  1067736-UPDT |  51  1068185-48HR |  72  1067516-CANC |
  10  1067519-48HR |  31  1067737-UPDT |  52  1068202-UPDT |  73  1068250-48HR |
  11  1067521-48HR |  32  1067738-UPDT |  53  1068210-UPDT |  74  1068410-48HR |
  12  1067522-48HR |  33  1067740-UPDT |  54  1068215-UPDT |  75  1068418-48HR |
  13  1067523-48HR |  34  1067745-UPDT |  55  1068216-UPDT |  76  1068601-UPDT |
  14  1067524-48HR |  35  1067748-UPDT |  56  1068218-UPDT |  77  1068656-UPDT |
  15  1067540-48HR |  36  1067794-48HR |  57  1068220-UPDT |  78  1067257-CORR |
  16  1067546-UPDT |  37  1067862-ETKT |  58  1068224-UPDT |  79  1068719-48HR |
  17  1067543-ETKT |  38  1067414-CANC |  59  1068226-UPDT |  80  1068744-48HR |
  18  1065486-CANC |  39  1067845-48HR |  60  1068228-UPDT |  81  1068755-48HR |
  19  1067710-UPDT |  40  1067890-UPDT |  61  1068230-UPDT |  82  1068768-48HR |
  20  1067711-UPDT |  41  1068173-ETKT |  62  1068232-UPDT |  83  1068787-48HR |
  21  1067714-UPDT |  42  1068096-48HR |  63  1068234-UPDT |

* indicates ticket # is repeated

Total Tickets: 83

48HR - STANDARD                       |   35
UPDT - UPDATE                         |   40
ETKT - EMERGENCY                      |    3
CANC - CANCELLATION                   |    3
PTKT - INSUFFICIENT NOTICE            |    1
CORR - CORRECTION                     |    1

Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03



0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: KCBA01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  1067514-48HR |   4  1067778-UPDT |   7  1068240-48HR |  10  1068575-48HR |
   2  1067517-48HR |   5  1067785-UPDT |   8  1068569-48HR |  11  1068576-48HR |
   3  1067742-UPDT |   6  1067835-48HR |   9  1068573-48HR |  12  1068741-48HR |

* indicates ticket # is repeated

Total Tickets: 12

48HR - STANDARD                       |    9
UPDT - UPDATE                         |    3

Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03



0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: CNTV02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  

* indicates ticket # is repeated

Total Tickets: 0


Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03



0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: TNC01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  1067538-ETKT |   7  1067772-UPDT |  13  1067925-48HR |  19  1067316-CORR |
   2  1067759-ETKT |   8  1067782-UPDT |  14  1067932-48HR |  20  1068265-UPDT |
   3  1067691-48HR |   9  1067780-ETKT |  15  1067990-48HR |  21  1068266-UPDT |
   4  1067728-48HR |  10  1067836-ETKT |  16  1068046-48HR |  22  1068413-ETKT |
   5  1067768-UPDT |  11  1067831-48HR |  17  1068067-48HR |  23  1068368-48HR |
   6  1067770-UPDT |  12  1065012-CORR |  18  1068099-48HR |  24  1068460-ETKT |

* indicates ticket # is repeated

Total Tickets: 24

ETKT - EMERGENCY                      |    6
48HR - STANDARD                       |   10
UPDT - UPDATE                         |    6
CORR - CORRECTION                     |    2

Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03



0
Date/Time :  2-20-03 at  0:51

MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON  2/19/03

Date/Time: 02/20/03 12:30:01 AM
Receiving Terminal: CCI01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  

* indicates ticket # is repeated

Total Tickets: 0


Please call (410) 712-0056 if this data does
not match the tickets you received on  2/19/03




$
