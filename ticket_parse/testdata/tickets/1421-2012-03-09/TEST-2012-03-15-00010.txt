
SCGZ02 57 SC811 Voice 03/15/2012 02:56:00 PM 1203150551 Emergency 

Ticket Number:    1203150551        Old Ticket:                        
Sequence:         57                Created By:      LPW               

Created:          03/15/12 02:58 PM                                     
Work Date:        03/20/12 03:00 PM                                     
Update on:        04/05/2012        Good Through:    04/10/2012        

Caller Information:
123 FENCE
123 NOT REAL LN
COLUMBIA, SC 29210
Company Fax:                        Type:            Contractor        
Caller:           TESTY TESTERSOM   Phone:           (803) 555-5555 Ext:555
Caller Email:     TEST@DONOTRESPOND.EU                                  

Site Contact Information:
TESTY TESTERSOM                              Phone:(803) 555-5555 Ext:555
Site Contact Email:  TEST@DONOTRESPOND.EU                                  
CallBack:  

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        999 MAULDIN AVE                                       
Intersection:  STANDISH ST                                           
Subdivision:   COLLEGE PLACE                                         

Lat/Log: 34.045662,-81.021704
Second:  34.049842,-81.016217

Explosives:  N White Lined:  Y Drilling/Boring:  Y Near Railroad:  

Work Type:     FENCE, REPAIR                                           
Work Done By:  123 FENCE            Duration:        APPROX 20 MINS    

Instructions:
**THIS IS A TEST**

MARK THE ENTIRE PROPERTY AND BOTH SIDES OF THE ROAD       

Directions:
**THIS IS A TEST// DO NOT RESPOND**

N MAIN ST, TURN RIGHT ON ASHLEY ST, TURN 
RIGHT ON COLLEGE PLACE DR, TURN LEFT ON STANDISH; MAULDIN AVE IS 4TH STREET ON
LEFT                                                                          

Remarks:
**THIS IS A TEST**

**THIS IS CORRECTED INFO FROM CANCELLED EMERGENCY TICKET  
1203150550**

**EMERGENCY DUE TO : IMMEDIATE DANGER TO PROPERTY. CREW IS ON   
SITE. WORK IS IN PROGRESS                                                     

Member Utilities Notified:
COC82 BSZB45 MCI18 TWCZ40 SCEKZ42 SCGZ02 


