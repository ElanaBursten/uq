
SCGZ02 11 SC811 Voice 03/09/2012 12:05:00 PM 1203090064 Normal 

Ticket Number:    1203090064        Old Ticket:                        
Sequence:         11                Created By:      HD                

Created:          03/09/12 12:05                                        
Work Date:        03/14/12 12:15                                        
Update on:        03/30/2012        Good Through:    04/04/2012        

Caller Information
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:2
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
RHONDA DOTMAN                              Phone:(803) 939-1117 Ext:2
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  best time to reach is 7am to 4am Mon-Fri

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        SENATE ST                                             
Intersection:  HARDEN ST                                             
Subdivision:   PUPS PHASE 3                                          

Lat/Log: 33.99964,-81.023538
Second:  34.009348,-81.013373

Explosives:  N White Lined:  Y Drilling/Boring:  N Near Railroad:  Y

Work Type:     LAGOON, DIG                                             
Work Done By:  SC811                Duration:        ABOUT 10 DAYS     

Instructions:
PLEASE EMAIL ME IF ANY QUESTIONS                                              

Directions:
THIS IS A TEST TICKET FOR OUR MEMBERS TO TEST THE FORMAT                      

Remarks:
GOOD MORNING, THIS IS TO TEST THE FORMAT FOR OUR MEMBERS WITH OUR SYSTEM      
UPGRADE// THANK YOU // RHONDA                                                 

Member Utilities Notified:
COC82 DPT77 BSZB45 USC42 ATT09 LC393 QWC42 STG10 
TWCZ40 TWTZ26 SCEJZ40 ITC73 SCG02 SCGZ02 


