
SCEDZ65/SCGZ65 26 SC811 Voice 03/14/2012 10:05:00 AM 1203140139 Meet 

Ticket Number:    1203140139        Old Ticket:                        
Sequence:         26                Created By:      RMD               

Created:          03/14/12 10:06 AM                                     
Work Date:        03/19/12 10:15 AM                                     
Update on:        04/04/2012        Good Through:    04/09/2012        

Caller Information:
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:1
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
JOHN BLACK                              Phone:(803) 123-1234 Ext:1
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  

Excavation Information:
SC    County:  BEAUFORT             Place:  HILTON HEAD ISLAND
Street:        6 GREENWOOD CT                                        
Intersection:  GREENWOOD LN                                          
Subdivision:   none                                                  

Lat/Log: 32.25934,-80.923717
Second:  32.311688,-80.845909

Explosives:  N White Lined:  Y Drilling/Boring:  Y Near Railroad:  

Work Type:     LAGOON, DIG                                             
Work Done By:  SC811                Duration:        about 1 day       

Instructions:
IF FACING, MARK THE REAR AND RIGHT SIDE OF THE PROPERTY                       

Directions:
HWY 1 TO N ARROWWOD LANE, LEFT ON TO DUTCH SQUARE BLVD                        

Remarks:
THIS IS A TEST LOCATE FOR OUR MEMBERS                                         

Member Utilities Notified:
HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 

HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 TWBZ51 

HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 TWBZ51 HRBZ22 

HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 TWBZ51 HRBZ22 TWQZ64 

HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 TWBZ51 HRBZ22 TWQZ64 WCR44* 

HARZ12 BJW50 BCT31 ATT09* PEB52 EMBZ11 SCEDZ65 SCGZ65 TWBZ51 HRBZ22 TWQZ64 WCR44* 


