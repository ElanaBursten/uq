
UTIL15 00002 VUPSa 02/24/11 09:55:36 A105500505-00A          NORMAL

Ticket No:  A105500505-00A                        NEW  GRID NORM LREQ
Transmit        Date: 02/24/11   Time: 09:55 AM   Op: 1BRD
Call            Date: 02/24/11   Time: 09:47 AM
Due By          Date: 03/01/11   Time: 07:00 AM
Update By       Date: 03/15/11   Time: 11:59 PM
Expires         Date: 03/18/11   Time: 07:00 AM
Old Tkt No: A105500505
Original Call   Date: 02/24/11   Time: 09:47 AM   Op: 1BRD

City/Co:ARLINGTON             Place:                                    State:VA
Subdivision: ROACHES RUN
Address:     550              Street: S CLARK ST
Cross 1:     6TH ST S

Type of Work:   SEWER LINE - INSTALL
Work Done For:  ARLINGTON COUNTY
Excavation area:DRIVEWAY AND SIDEWALK INSTALL

                FACING THE PROPERTY NEED THE FRONT AND LEFT SIDE OF PROPERTY
Instructions:   CALLER MAP REF: NONE

Whitelined: Y   Blasting: N   Boring: N

Company:        ALPHA CONSTRUCTION                        Type: CONT
Co. Address:    7940 PENN RANDALL PL  First Time: N
City:           UPPER MARLBORO  State:MD  Zip:20772
Company Phone:  301-420-8420
Contact Name:   RAYMOND ROWE                Contact Phone:202-439-4499
Email:          tckxi1@aol.com
Contact Fax:    301-420-8424
Field Contact:  RAYMOND ROWE
Fld. Contact Phone:202-439-4499

Mapbook:  5648C2
Grids:    3851A7702A-00  3851A7702A-01  3851A7702A-02  3851A7702A-10
Grids:    3851A7702A-11  3852D7702A-41  3852D7702A-42

Members:
APW901 = ARLINGTON COUNTY DPW (APW)     ATT392 = AT&T (ATT)
CMC703 = COMCAST (CMC)                  DOM400 = DOMINION VIRGINIA POWER (DOM)
JUC371 = JONES UTILITIES CONSTRUCT(JUC) LTC903 = LEVEL 3 COMMUNICATIONS (LTC)
MCII81 = MCI (MCI)                      PEP904 = POTOMAC ELECTRIC POWER CO(PEP)
QGS901 = QWEST GOVERNMENT SERVICES(QGS) VZN104 = VERIZON (VZN)
WGL904 = WASHINGTON GAS (WGL)

Seq No:   2 A
