
New Jersey One Call System        SEQUENCE NUMBER 0022    CDC = CC4 

Transmit:  Date: 07/16/13   At: 10:10 

*** R O U T I N E         *** Request No.: 131970672 

Operators Notified: 
BAN     = VERIZON                       CC4     = COMCAST CABLEVISION OF NE      
GP9     = JERSEY CENTRAL POWER & LI     NJN     = NEW JERSEY NATURAL GAS CO      
RUM     = RUMSON BOROUGH                SCNJ7   = NJ AMER WTR                    

Start Date/Time:    07/20/13   At 00:15  Expiration Date: 09/18/13 

Location Information: 
County: MONMOUTH               Municipality: RUMSON 
Subdivision/Community:  
Street:               42 WARDELL AVE 
Nearest Intersection: RIVERSIDE DR 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL FENCE 
Block:                Lot:                Depth: 1FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  MARIA MAHER 

Working For: HOMEOWNER 
Address:     42 WARDELL AVE 
City:        RUMSON, NJ  07760 
Phone:       917-446-8099   Ext:  

Excavator Information: 
Caller:      TRACEY FEIN 
Phone:       866-363-3647   Ext: 280 

Excavator:   CANINE FENCE COMPANY 
Address:     493 DANBURY ROAD 
City:        WILTON, CT  06897 
Phone:       203-210-1559   Ext: 280      Fax:  203-762-2582 
Cellular:     
Email:       tfein@caninefence.com 
End Request 
