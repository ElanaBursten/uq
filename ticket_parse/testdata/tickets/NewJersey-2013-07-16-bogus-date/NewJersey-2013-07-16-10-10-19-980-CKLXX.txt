
New Jersey One Call System        SEQUENCE NUMBER 0043    CDC = ADC 

Transmit:  Date: 07/16/13   At: 10:09 

*** R O U T I N E         *** Request No.: 131970895 

Operators Notified: 
ADC     = COMCAST-TOMS RIVER OCEAN      BAN     = VERIZON                        
GPC     = JERSEY CENTRAL POWER & LI     LMU     = LACEY MUNICIPAL UTILITIES      
NJN     = NEW JERSEY NATURAL GAS CO      

Start Date/Time:    07/23/13   At 11:00  Expiration Date: 09/18/13 

Location Information: 
County: OCEAN                  Municipality: LACEY 
Subdivision/Community:  
Street:               624 BEACH BLVD 
Nearest Intersection: PASADENA DR 
Other Intersection:    
Lat/Lon: 
Type of Work: TERMITE TREATMENT 
Block:                Lot:                Depth: 2FT 
Extent of Work: 3FT PERIMETER OF HOUSE. 
Remarks:  
  Working For Contact:  BILL FINARELLI 

Working For: HOMEOWNER 
Address:     624 BEACH BLVD 
City:        FORKED RIVER, NJ  08731 
Phone:       609-661-2806   Ext:  

Excavator Information: 
Caller:      KYLE MOONEY 
Phone:       732-240-2066   Ext:  

Excavator:   CENTRAL TERMITE & PEST CONTROL 
Address:     1889 RT 9 UNIT 68 
City:        TOMS RIVER, NJ  08755 
Phone:       732-240-2066   Ext:          Fax:  732-240-4566 
Cellular:     
Email:        
End Request 
