
KORTERRA JOB UTILIQUEST_WV-SMTP-1 WV1133300460 Seq: 272 11/29/2011 11:03:42
Locate Request For PEB
----------------------------------------------------------------------------
Ticket Number:    1133300460        Old Ticket:
Source:           Email             Sequence:         5
Type:             Normal            Date:             11/29/11 08:32

Company Information
----------------------------------------------------------------------------
ALL-LINK COMMUNICATIONS LLC         Type:             Contractor
P.O BOX 209                         Company Phone:    (434) 985-4933
RUCKERSVILLE, VA 22968              Company Fax:      (434) 465-6850
Caller Name:      SANDRA KINDER     Caller Phone:     (434) 985-4933
Contact Name:     CORINNA REILLY    Contact Phone:    (434) 985-4933
Caller Email:     ZONE2LOCATES@ALLLINKCOMM.COM
Contact Email:    ZONE2LOCATES@ALLLINKCOMM.COM

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        12/01/11 08:45
County:           Berkeley          Done For:         COMCAST
Place:            MARTINSBURG       Depth:            2 FT
Street:           SOPWITH WAY
Intersection:     SHASTA LN
Nature of Work:
Explosives:       No                Boring:           No
White Lined:      No                Length:           3 HOURS

Driving Directions
----------------------------------------------------------------------------


Remarks
----------------------------------------------------------------------------
Lat/Long: 3944223 7796658
PLEASE LOCATE FROM THE NEAREST PED/TAP/POLE TO
INCLUDE EASEMENTS, HOMES AND BUILDINGS TO THE POWER METER BASE LOCATED ON THE
ABOVE MENTIONED LOCATION

2ND CROSSRDS--HACK WILSON WAY

Original File:
18a0dfa4_20111129073951.xml



Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually
----------------------------------------------------------------------------
BKC   Beckley County PSD                                No
PEB   Firstenergy Corp                                  No
AX    Frontier Communications(Formerly Verizon)         No
CEM   Mountaineer Gas Company                           No
MAR   City of Martinsburg Water Department              No
WCM   Comcast                                           No
BKC   Berkeley County PSD                               No


Location
----------------------------------------------------------------------------
Latitude:         39.441111         Longitude:        -77.968196
Second Latitude:  39.443715         Second Longitude: -77.965089

Grids
----------------------------------------------------------------------------
39264077578A 39264077578C 39264077580B 39264077580D 39266077580D




-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.