
ATTD14LAN 00002B USAS 02/29/12 08:07:59 B20600024-00B SHRT NEW GRID

Ticket : B20600024  Date: 02/29/12 Time: 08:07 Oper: JRB Chan: WEB
Old Tkt: B20600024  Date: 02/29/12 Time: 08:06 Oper: DIGEXPRESS Revision: 00B

Company: THE GAS COMPANY                Caller: ART PEREZ
Co Addr: 7711 CANOGA AVE
City&St: CANOGA PARK, CA                Zip: 91304
Phone: 818-701-3488 Ext:      Call back: 7:00AM-3:30PM
Formn: UNKNOWN              Phone: 818-701-3488
Email: APEREZ1@SEMPRAUTILITIES.COM

State: CA County: LOS ANGELES     Place: NORTHRIDGE
Delineated: Y
Delineated Method: WHITEPAINT
Address: 18325       Street:SUPERIOR ST
X/ST 1 : RATHBURN AVE
MPM 1:             MPM 2:
Locat: IN FRONT OF ADDRESS

Excav Enters Into St/Sidewalk: Y

Grids: 0500J0534
Lat/Long  : 34.247237/-118.533434 34.247237/-118.532040
          : 34.246056/-118.533434 34.246056/-118.532040
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE GAS SERVICE
Wkend: N  Night: N
Work date: 03/02/12 Time: 08:00 Hrs notc: 047 Work hrs: 047 Priority: 1
Instruct : MARK BY                        Permit: U-1281-0525
Done for : THE GAS COMPANY

Tkt Exp: 03/28/12

Mbrs : ATTD14LAN     CITYLA LAWP2  SCG4U2 UTWCNC10

At&t Ticket Id: 40958   clli code: NORGCA11   OCC Tkt No: B20600024-00B