
ATTD28SD 00313A USAS 02/08/12 16:35:33 A20391267-00A NORM UPDT GRID

Ticket : A20391267  Date: 02/08/12 Time: 16:34 Oper: NHR Chan: 100
Old Tkt: A13430847  Date: 01/04/12 Time: 09:28 Oper: KKP Revision: 00A

Company: FLATIRON WEST, INC             Caller: JEREMY
Co Addr: 1770 LA COSTA MEADOWS DR
City&St: SAN MARCOS, CA                 Zip: 92078      Fax: 858-486-4692
Phone: 760-916-9100 Ext:      Call back: ANYTIME
Formn: JEREMY               Phone: 760-443-6087

State: CA County: SAN DIEGO       Place: BONSALL
Delineated: Y
Delineated Method: OTHER
Address:             Street:MISSION RD
X/ST 1 : NORTH RIVER RD
X/ST 2 : HOLLY LN
MPM 1:             MPM 2:
Locat: DELINEATED WITH YELLOW RIBBON  BOTH SIDES OF HWY 76 AKA-MISSION RD  FROM
     : NORTH RIVER RD CONT S/TO HOLLY LN  ** MARKING APPROX 200FT E/OF AND
     : APPROX 200FT W/OF HWY 76 - THE ENTIRE DISTANCE

Excav Enters Into St/Sidewalk: N

Grids: 1067H0424    1067H052
Lat/Long  : 33.268088/-117.237286 33.267862/-117.235669
          : 33.262783/-117.238025 33.262557/-117.236408
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL UNDERGROUND UTILS & STORM DRAIN
Wkend: N  Night: N
Work date: 02/08/12 Time: 16:35 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : CALTRANS FCI

Tkt Exp: 03/07/12

                                  COMMENTS

REF EXP TICKET A013430847, UPDATE ONLY-WORK CONT PER JEREMY--[NHR 02/08/12
16:35]

Mbrs : ATTD28SD      COX04  RAI88  SDG01  UTWCCARL

At&t Ticket Id: 46149   clli code: VISTCA12   OCC Tkt No: A20391267-00A