
ATTD124OR 00026A USAS 02/29/12 08:56:53 A20600231-00A NORM NEW GRID

Ticket : A20600231  Date: 02/29/12 Time: 08:52 Oper: LRL Chan: 100
Old Tkt: A20600231  Date: 02/29/12 Time: 08:56 Oper: LRL Revision: 00A

Company: SC GAS                         Caller: TANIA
Co Addr: 1919 S STATE COLLEGE BLVD
City&St: ANAHEIM, CA                    Zip: 92806
Phone: 800-603-7060 Ext:      Call back: 6:30AM-3:30PM
Formn: UNK

State: CA County: ORANGE          Place: SANTA ANA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1202        Street:S HICKORY ST
X/ST 1 : E MCFADDEN AVE
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0829F052     0829G051
Lat/Long  : 33.733898/-117.861327 33.733843/-117.860115
          : 33.732780/-117.861378 33.732725/-117.860165
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR GAS LEAK
Wkend: N  Night: N
Work date: 03/02/12 Time: 08:53 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: UNKNOWN
Done for : SC GAS  (SAP #52-275795)

Tkt Exp: 03/28/12

Mbrs : ATTD124OR     MCISOCAL      MPWRLA SAN02  SCE12  SCG2XQ TWCCORG
USCE02

At&t Ticket Id: 42538   clli code: SNANCA01   OCC Tkt No: A20600231-00A