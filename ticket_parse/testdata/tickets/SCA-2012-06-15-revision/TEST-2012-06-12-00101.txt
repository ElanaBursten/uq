
ATTD124OR 00041A USAS 02/08/12 10:55:43 A20110854-01A NORM UPDT GRID

Ticket : A20110854  Date: 02/08/12 Time: 10:54 Oper: TEL Chan: 100
Old Tkt: A20110854  Date: 01/11/12 Time: 12:43 Oper: MLC Revision: 01A

Company: FARWEST CORROSION              Caller: BOB TAYLOR
Co Addr: 1480 W ARTESIA BLVD
City&St: GARDENA, CA                    Zip: 90248
Phone: 310-532-9524 Ext:      Call back: 7AM - 5PM
Formn: UNK
Email: BTAYLOR@FARWST.COM

State: CA County: ORANGE          Place: IRVINE
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:YALE AVE
X/ST 1 : UNIVERSITY DR
MPM 1:             MPM 2:
Locat: AT THE IRVINE RANCH WTR STATION LOC APPROX 317FT N/W OF YALE AVE & AT
     : APPROX 248FT S/OF UNIVERSITY DR

Excav Enters Into St/Sidewalk: N

Grids: 0890C01
Lat/Long  : 33.658430/-117.815264 33.657969/-117.814479
          : 33.657751/-117.815663 33.657290/-117.814877
Caller GPS: 33.657849/-117.815009

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : CATHODIC PROTECTION WELL - TRENCH FOR ELECT CONDUIT
Wkend: N  Night: N
Work date: 02/08/12 Time: 10:55 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : PALULUS ENGR

Tkt Exp: 03/07/12

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER BOB--[TEL 02/08/12 10:55]

Mbrs : ATTD124OR     COXRSM IRVTS  IRW01  MWD05  SCG2XQ USCE04

At&t Ticket Id: 44309   clli code: IRVNCA01   OCC Tkt No: A20110854-01A