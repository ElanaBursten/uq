
ATTD28SD 00150A USAS 02/17/12 13:26:38 A20330270-01A RUSH NOSH GRID

Ticket : A20330270  Date: 02/17/12 Time: 13:25 Oper: EMB Chan: 100
Old Tkt: A20330270  Date: 02/02/12 Time: 08:58 Oper: CLA Revision: 01A

Company: ARBOR WELL TREE SERV           Caller: LOUIS MASON-COAST LANDSCAPING
Co Addr: 5601 EASTGATE DR
City&St: SAN DIEGO, CA                  Zip: 92121      Fax: 760-436-8147
Phone: 760-535-0327 Ext: CELL Call back: 7AM  - 430PM
Formn: LOUIS MASON          Phone: 760-436-6804 Ext: OFC
Email: LOUIS@COASTLANDSCAPING.COM

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1601        Street:KETTNER BLVD
X/ST 1 : CEDAR ST
MPM 1:             MPM 2:
Locat: IN FRONT OF CONDOMINUMS MARK FRM SIDEWALK TO BLDG;
     : ADDRESS IS LOCATED IN LITTLE ITALY SECTION OF DOWNTOWN SAN DIEGO

Excav Enters Into St/Sidewalk: N

Grids: 1288J0224
Lat/Long  : 32.723022/-117.169502 32.722983/-117.168236
          : 32.721658/-117.169545 32.721618/-117.168279
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : LANDSCAPE & REMOVE 2 LARGE SYCAMORE TREES, PLANTING PLANTS
Wkend: N  Night: N
Work date: 02/17/12 Time: 13:26 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : NO SHOW                        Permit: NOT REQUIRED
Done for : COAST LANDSCAPING INC.

Tkt Exp: 03/16/12

                                  COMMENTS

**RESEND**CALLER ADVISES NO SHOW FROM GAS, ELECTRIC, WATER - PLEASE RESPOND ASAP
TO MARK LINE.. IF NO CONFLICT PLEASE CALL OR EMAIL PER LOUIS--[EMB 02/17/12
13:26]

Mbrs : ATTD28SD      COX01  MCISOCAL      MPWRSD SDG01  SDGET  SND01  TWCSD
UATLSD UNIFPORTSD    WESPACPL

At&t Ticket Id: 62623   clli code: SNDGCA01   OCC Tkt No: A20330270-01A