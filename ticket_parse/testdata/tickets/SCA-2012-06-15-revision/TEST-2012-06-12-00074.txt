
ATTD28SD 00062A USAS 02/09/12 09:39:08 A20400294-00A RUSH NEW GRID

Ticket : A20400294  Date: 02/09/12 Time: 09:33 Oper: EVA Chan: 100
Old Tkt: A20400294  Date: 02/09/12 Time: 09:39 Oper: EVA Revision: 00A

Company: HCI                            Caller: DERRICK - AT&T
Co Addr: UNK TO CALLER
City&St:                                Zip:
Phone: 858-268-2103 Ext:      Call back: AFT 7AM & BEFOR 5P
Formn: DERRICK              Phone: 858-336-8537

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:HUMMINGBIRD LN
X/ST 1 : MEADOW LARK DR
MPM 1:             MPM 2:
Locat: FRM FRONT OF 7863 HUMMINGBIRD LN TO ACROSS THE ST TO 7854 HUMMINGBIRD LN
     : X/ST W  MEADOW LARK DR

Excav Enters Into St/Sidewalk: Y

Grids: 1249B063
Lat/Long  : 32.792140/-117.156415 32.791856/-117.154018
          : 32.791197/-117.156526 32.790913/-117.154130
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : EMERGENCY REPLACE CABLE
Wkend: N  Night: N
Work date: 02/09/12 Time: 09:34 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : NOW                            Permit: NOT AVAILABLE
Done for : AT&T

Tkt Exp: 03/08/12

Mbrs : ATTD28SD      COX01  FREESD SDG01  SND01  TWCSD

At&t Ticket Id: 46639   clli code: SNDGCA03   OCC Tkt No: A20400294-00A