
ATTD19LAN 00022A USAS 02/08/12 09:52:40 A12900382-05A NORM UPDT GRID

Ticket : A12900382  Date: 02/08/12 Time: 09:51 Oper: LAC Chan: 100
Old Tkt: A12900382  Date: 10/17/11 Time: 10:06 Oper: RIA Revision: 05A

Company: LADWP CENTRAL                  Caller: ROBERT LOPEZ
Co Addr: 433 E TEMPLE ST
City&St: LOS ANGELES, CA                Zip: 90012      Fax: 213-367-6725
Phone: 213-367-8339 Ext:      Call back: 6.30AM-4PM
Formn: ROBERT               Phone: 213-798-6740
Email: ROBERT.LOPEZ3@LADWP.COM

State: CA County: LOS ANGELES     Place: LOS ANGELES
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:RAYNOL ST
X/ST 1 : REYNOLDS AVE
MPM 1:             MPM 2:
Locat: BOTH SIDES OF RAYNOL ST FRM REYNOLDS AVE W/TO THE END OF CUL-DE-SAC, BOTH
     : SIDES OF REYNOLDS AVE FRM RAYNOL ST S/TO END OF CUL-DE-SAC, MARK ENTIRE
     : INTER

Excav Enters Into St/Sidewalk: Y

Grids: 0595C063
Lat/Long  : 34.085119/-118.197890 34.083806/-118.196081
          : 34.083716/-118.198908 34.082403/-118.197099
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL 6INCH WRT MAIN
Wkend: N  Night: N
Work date: 02/08/12 Time: 09:52 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING                Permit: U10507874
Done for : LADWP CENTRAL

Tkt Exp: 03/07/12

                                  COMMENTS

**RESEND**CALLER ADVISES NO SHOW FROM GAS - NO SHOW GAS ON RAYNOL ST  PLEASE
RESPOND ASAP PER ROBERT LOPEZ--[MLC 10/31/11 07:58]
**RESEND**UPDATE ONLY-WORK CONT PER RUBEN--[ADS 11/22/11 10:25]
**RESEND**UPDATE ONLY-WORK CONT PER ROBERT LOPEZ--[CJD 12/16/11 07:22]
**RESEND**UPDATE ONLY-WORK CONT PER ROBERT--[LRL 01/12/12 07:47]
**RESEND**UPDATE ONLY-WORK CONT PER ROBERT LOPEZ--[LAC 02/08/12 09:52]

Mbrs : ATTD19LAN     CITYLA LAWP3  SCG4QH UCCT04

At&t Ticket Id: 43943   clli code: LSANCA23   OCC Tkt No: A12900382-05A