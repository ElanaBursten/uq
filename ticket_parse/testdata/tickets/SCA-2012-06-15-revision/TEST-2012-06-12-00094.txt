
ATTD01OR 00014A USAS 02/08/12 09:07:31 A20390289-00A NORM NEW GRID

Ticket : A20390289  Date: 02/08/12 Time: 09:03 Oper: NHR Chan: 100
Old Tkt: A20390289  Date: 02/08/12 Time: 09:07 Oper: NHR Revision: 00A

Company: HCI                            Caller: RALPH GARCIA
Co Addr: 3166 HORSELESS CARRIAGE DR
City&St: NORCO, CA                      Zip: 92860      Fax: 714-808-0751
Phone: 714-797-5788 Ext:      Call back: 7AM - 5PM
Formn: RALPH

State: CA County: ORANGE          Place: BUENA PARK
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:BEACH BLVD
X/ST 1 : 10TH ST
MPM 1:             MPM 2:
Locat: IN THE SIDEWALK AT THE  S/W COR OF BEACH BLVD AND 10TH ST  MOST SOUTHERLY
     : INTER

Excav Enters Into St/Sidewalk: Y

Grids: 0767H014
Lat/Long  : 33.864084/-117.998327 33.863624/-117.997538
          : 33.863404/-117.998724 33.862944/-117.997935
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE HANDHOLE
Wkend: N  Night: N
Work date: 02/10/12 Time: 09:04 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : AT&T

Tkt Exp: 03/07/12

Mbrs : ATTD01OR      BUP01  PARADYNE      SCG2XN TWCCORG       USCE03

At&t Ticket Id: 44012   clli code: BNPKCA11   OCC Tkt No: A20390289-00A