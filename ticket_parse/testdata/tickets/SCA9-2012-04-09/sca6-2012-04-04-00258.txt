
ATTD124OR 00049A USAS 04/03/12 14:54:50 A20941025-00A NORM NEW GRID

Ticket : A20941025  Date: 04/03/12 Time: 14:50 Oper: LYD Chan: 100
Old Tkt: A20941025  Date: 04/03/12 Time: 14:54 Oper: LYD Revision: 00A

Company: SCC01DIST - SOUTH COAST WATER DISTRICT Caller: BOB MORTON
Co Addr: 31592 WEST ST
City&St: LAGUNA BEACH, CA               Zip: 92677
Phone: 949-499-4555 Ext:      Call back: 7AM - 5PM
Formn: BRIAN LYNN           Phone: 949-289-0039

State: CA County: ORANGE          Place: DANA POINT
Delineated: Y
Delineated Method: WHITEPAINT
Address: 24622       Street:BENJAMIN CIR
X/ST 1 : JEREMIAH DR
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0971H033     0971H041
Lat/Long  : 33.485998/-117.700449 33.485495/-117.699458
          : 33.485003/-117.700954 33.484500/-117.699962
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : LOCATE WTR VALVE
Wkend: N  Night: N
Work date: 04/05/12 Time: 15:00 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: 03-464
Done for : SCC01DIST - SOUTH COAST WATER DISTRICT

Tkt Exp: 05/01/12

Mbrs : ATTD124OR     COE01  COXRSM DPT01  MNW01  NEXTGLAVEN    SCC01DIST
SCG2XK SCWDJRWSS     SDG01

At&t Ticket Id: 25146122   clli code: SJCPCA12