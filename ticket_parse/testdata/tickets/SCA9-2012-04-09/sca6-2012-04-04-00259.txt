
ATTD28SD 00226A USAS 04/03/12 14:55:22 A20660230-01A NORM UPDT GRID

Ticket : A20660230  Date: 04/03/12 Time: 14:54 Oper: CLA Chan: 100
Old Tkt: A20660230  Date: 03/06/12 Time: 08:50 Oper: MLC Revision: 01A

Company: A M ORTEGA CONSTRUCTION        Caller: LUIS CASTILLO
Co Addr: 10125 CHANNEL RD
City&St: LAKESIDE, CA                   Zip: 92040      Fax: 619-390-1941
Phone: 619-571-6468 Ext:      Call back: ANYTIME
Formn: LUIS CASTILLO
Email: DAVIDH@AMORTEGA.COM

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 510         Street:DODSON ST
X/ST 1 : MARKET ST
MPM 1:             MPM 2:
Locat: FRONT OF

Excav Enters Into St/Sidewalk: Y

Grids: 1289E033     1289E041
Lat/Long  : 32.711354/-117.132089 32.711376/-117.130790
          : 32.710396/-117.132073 32.710418/-117.130774
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : ELECTRICAL CONDUIT CONNECTION
Wkend: N  Night: N
Work date: 04/03/12 Time: 14:55 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : SDGE

Tkt Exp: 05/01/12

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER RON GOMEZ--[CLA 04/03/12 14:55]

Mbrs : ATTD28SD      COX01  LVL3CM NEXTG  SDG01  SDGET  SND01

At&t Ticket Id: 25146124   clli code: SNDGCA01