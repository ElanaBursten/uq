
ATTD20LAN 00028A USAS 04/03/12 14:49:10 A20941010-00A NORM NEW GRID

Ticket : A20941010  Date: 04/03/12 Time: 14:44 Oper: LRL Chan: 100
Old Tkt: A20941010  Date: 04/03/12 Time: 14:48 Oper: LRL Revision: 00A

Company: SC GAS                         Caller: JAIME BERRIDY
Co Addr: 3530 E FOOTHILL BLVD
City&St: PASADENA, CA                   Zip: 91107      Fax: 626-796-0183
Phone: 626-397-4966 Ext:      Call back: 7AM-3:30PM
Formn: UNK

State: CA County: LOS ANGELES     Place: PASADENA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 3738        Street:LAURITA AVE
X/ST 1 : N ROSEMEAD BLVD
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0566H0534    0566H0612
Lat/Long  : 34.137730/-118.072950 34.137759/-118.071780
          : 34.136330/-118.072915 34.136359/-118.071745
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE GAS LINE
Wkend: N  Night: N
Work date: 04/05/12 Time: 14:45 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: PENDING
Done for : SC GAS  (WORK REQUEST #2075339)

Tkt Exp: 05/01/12

Mbrs : ATTD20LAN     EPW01  LACOTS PAS02  SCG4QM UCHRCOM       USCE27

At&t Ticket Id: 25146090   clli code: PSDNCA11