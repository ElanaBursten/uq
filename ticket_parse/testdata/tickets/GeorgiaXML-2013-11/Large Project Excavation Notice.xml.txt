<TicketDataSet xmlns="http://tempuri.org/TicketDataSet.xsd"><TICKET><One_Call_Center>UPCA</One_Call_Center><Ticket_Number>07011-400-054</Ticket_Number><Reference_Ticket_Number /><Version_Number>2</Version_Number><Ticket_Type>Large Project Excavation Notice</Ticket_Type><TicketAction>NORMAL</TicketAction><Primary_CDC><CDC>GAUPC</CDC><Sequence_Number>7</Sequence_Number></Primary_CDC><Additional_CDCs /><Ticket_Taken_DateTime>2011/07/01 10:57:23</Ticket_Taken_DateTime><Transmission_DateTime /><Work_Start_DateTime>2011/07/19 00:00:00</Work_Start_DateTime><Work_End_DateTime>2011/10/10 00:00:00</Work_End_DateTime><Legal_DateTime>2011/07/15 07:00:00</Legal_DateTime><Response_Due_DateTime>2011/07/11 23:59:59</Response_Due_DateTime><RestakeDate>2011/10/05 00:00:00</RestakeDate><ExpirationDate>2011/10/10 00:00:00</ExpirationDate><Duration_Of_Work>2 Days</Duration_Of_Work><Location_Of_Work>starting from the intersection with US Hwy 82 locate the r/o/w on both sides of the dirt road and the roadway itself going south 2.25 miles </Location_Of_Work><Type_Of_Work>instl phone main</Type_Of_Work><Work_Done_For>Plant Telephone</Work_Done_For><Explosives>No</Explosives><Overhead>No</Overhead><Tunneling_Or_Boring>Yes</Tunneling_Or_Boring><White-lined>No</White-lined><Remarks>aubrey finder rd aka co rd 133</Remarks><Subdivision /><Excavator><Company>PLANT TELEPHONE</Company><Address>1703 US HWY 82 W</Address><City>TIFTON</City><State>GA</State><Zip>31793</Zip><Contact><Name>DAVID PARKER</Name><Primary_Phone>2293824227</Primary_Phone><Alternate_Phone>2293824227</Alternate_Phone><Fax /><Email>ptceng@planttel.net</Email><Priority>0</Priority></Contact></Excavator><Dig_Site><Lookup_By>MANUAL</Lookup_By><Address_Info><State>GA</State><County>ATKINSON</County><Place>WILLACOOCHEE</Place><In_Or_Out>No</In_Or_Out><Street><From_Address> </From_Address><To_Address /><Direction_Prefix /><Street_Name>County Road 133</Street_Name><Street_Type /><Direction_Suffix /></Street></Address_Info><Near_Street><Direction_Prefix /><Street_Name>US Hwy 82</Street_Name><Street_Type /><Direction_Suffix /></Near_Street><TypeOfUtility /><FacilityMarks /><FacilityDamaged /><DamagedMemberCodes /><FacilitiesMarkedCorrectly /><LineTypeDamaged /><TypeOfEquipmentUsed /><PropertyDamage /><CrewOnSiteDamage>False</CrewOnSiteDamage><ExtentDamage /><HasFieldContact>False</HasFieldContact><ServiceOut>No</ServiceOut><Grids><Grid><GridItem><Type>Grid</Type><Value>3119D8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3119D8300C</Value></GridItem><GridItem><Type>Grid</Type><Value>3119C8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3119C8300C</Value></GridItem><GridItem><Type>Grid</Type><Value>3119C8259A</Value></GridItem><GridItem><Type>Grid</Type><Value>3119B8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3119B8259A</Value></GridItem><GridItem><Type>Grid</Type><Value>3118D8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3118D8259A</Value></GridItem><GridItem><Type>Grid</Type><Value>3118C8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3118C8300C</Value></GridItem><GridItem><Type>Grid</Type><Value>3118C8259A</Value></GridItem><GridItem><Type>Grid</Type><Value>3118B8300C</Value></GridItem><GridItem><Type>Grid</Type><Value>3118A8300D</Value></GridItem><GridItem><Type>Grid</Type><Value>3118A8300C</Value></GridItem></Grid></Grids><Polygon><Point>-83.006609383023488, 31.316365389302067</Point><Point>-83.0072055599103, 31.30689084141089</Point><Point>-83.006446656438513, 31.306142078557873</Point><Point>-83.004351890374437, 31.30472175237573</Point><Point>-83.001574060282366, 31.304201973240918</Point><Point>-82.998625625678457, 31.301949464789079</Point><Point>-82.998315265240834, 31.303760193710627</Point><Point>-82.999937039752965, 31.304599280914658</Point><Point>-83.001169778453615, 31.305846988633856</Point><Point>-83.0031837159475, 31.306030120745557</Point><Point>-83.005085480629575, 31.307531154368753</Point><Point>-83.004667076526786, 31.310330131351648</Point><Point>-83.005301673491346, 31.313385937565457</Point><Point>-83.004403200015, 31.316180567402633</Point><Point>-83.002831575855851, 31.319051521686454</Point><Point>-83.001928911399546, 31.322175828161914</Point><Point>-83.00094641831852, 31.323980457108348</Point><Point>-82.997932224624975, 31.327003103788908</Point><Point>-83.001274631489608, 31.32851730413822</Point><Point>-83.002061070175742, 31.327040631149682</Point><Point>-83.002264271143787, 31.326135701655382</Point><Point>-83.006609383023488, 31.316365389302067</Point></Polygon></Dig_Site><Ticket_Text>GAUPC  00007 GAUPC 03/13/12 17:41:33 07011-400-054-002 LPEXCA
Large Project Excavation Notification
Notice : 07011-400-054 Date: 07/01/11  Time: 10:57  Revision: 002 

State : GA County: ATKINSON      Place: WILLACOOCHEE                            
Addr  : From:        To:        Name:    COUNTY ROAD 133                        
Near  : Name:    US HWY 82                          

Subdivision:                                         
Locate: STARTING FROM THE INTERSECTION WITH US HWY 82 LOCATE THE R/O/W ON BOTH 
      :  SIDES OF THE DIRT ROAD AND THE ROADWAY ITSELF GOING SOUTH 2.25 MILES   

Grids       : 3118A8300C 3118A8300D 3118B8300C 3118C8259A 3118C8300C 
Grids       : 3118C8300D 3118D8259A 3118D8300D 3119B8259A 3119B8300D 
Grids       : 3119C8259A 3119C8300C 3119C8300D 3119D8300C 3119D8300D 
Grids       : 
Work type   : INSTL PHONE MAIN                                                    

ScopeOfWork : INSTL PHONE MAIN                                                 

Meeting Date      : 07/07/11 Time: 9:00 AM
Meeting Respond By: 07/06/11 Time: 23:59
Meeting Location  : EXCAVATION DATE: 7-19-11 MEETING DATE: 7-7-11 MEETING AT PL
        : ANT TELEPHONE WILLACOOCHEE OFFICE AT SE CORNER OF FLORIDA R
        : D AND MCCRANIE RD WILACOOCHEE, GA                          
LP Contact        : DAVID PARKER              Phone   : 2293824227 Ext:  
Contact Email     : PTCENG@PLANTTEL.NET                                        

Start date: 07/19/11 Time: 00:00 Hrs notc : 000
Legal day : 07/15/11 Time: 07:00 Good thru: 10/10/11 Restake by: 10/05/11
RespondBy : 07/11/11 Time: 23:59 Duration : 2 DAYS     Priority: 1
Done for  : PLANT TELEPHONE                         
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : AUBREY FINDER RD AKA CO RD 133                                      
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : PLANT TELEPHONE                           Type: MEMB                
Co addr : 1703 US HWY 82 W                         
City    : TIFTON                          State   : GA Zip: 31793              
Caller  : DAVID PARKER                    Phone   :  229-382-4227              
Fax     :                                 Alt. Ph.:  229-382-4227              
Email   : PTCENG@PLANTTEL.NET                                                 
Contact :                                                           

Submitted date: 07/01/11  Time: 10:57  Oper: 198
Mbrs : GAUPC 
-------------------------------------------------------------------------------
</Ticket_Text><ProposedMeetingDate>2011-07-07T00:00:00-04:00</ProposedMeetingDate><ProposedMeetingTime>9:00 AM</ProposedMeetingTime><ProposedMeetingLocation>Excavation Date: 7-19-11
Meeting Date: 7-7-11
Meeting at Plant Telephone Willacoochee Office
at se corner of Florida Rd and McCranie Rd
Wilacoochee, GA </ProposedMeetingLocation><MeetingContactName>David Parker</MeetingContactName><MeetingContactPhone>2293824227</MeetingContactPhone><MeetingContactEmail>ptceng@planttel.net</MeetingContactEmail><ScopeOfWork>Instl phone main</ScopeOfWork><ItoICounty>ATKINSON</ItoICounty><ItoICity>WILLACOOCHEE</ItoICity><ItoIPrefix /><ItoIStreetName /><ItoIStreetType /><ItoISuffix /></TICKET></TicketDataSet>