<TicketDataSet xmlns="http://tempuri.org/TicketDataSet.xsd"><TICKET><One_Call_Center>UPCA</One_Call_Center><Ticket_Number>07021-300-032</Ticket_Number><Reference_Ticket_Number /><Version_Number>0</Version_Number><Ticket_Type>Normal Notice</Ticket_Type><TicketAction>Restake</TicketAction><Primary_CDC><CDC>AGL103</CDC><Sequence_Number>6</Sequence_Number></Primary_CDC><Additional_CDCs><Additional_CDC>ATL01</Additional_CDC><Additional_CDC>ATL02</Additional_CDC><Additional_CDC>BSCA</Additional_CDC><Additional_CDC>COMCEN</Additional_CDC><Additional_CDC>GAUPC</Additional_CDC><Additional_CDC>GP103</Additional_CDC><Additional_CDC>MCI02</Additional_CDC><Additional_CDC>MESXX</Additional_CDC><Additional_CDC>NU103</Additional_CDC><Additional_CDC>ST005</Additional_CDC></Additional_CDCs><Ticket_Taken_DateTime>2011/07/02 23:09:38</Ticket_Taken_DateTime><Transmission_DateTime /><Work_Start_DateTime>2011/07/08 07:00:00</Work_Start_DateTime><Work_End_DateTime>2011/07/26 00:00:00</Work_End_DateTime><Legal_DateTime>2011/07/08 07:00:00</Legal_DateTime><Response_Due_DateTime>2011/07/07 23:59:59</Response_Due_DateTime><RestakeDate>2011/07/21 00:00:00</RestakeDate><ExpirationDate>2011/07/26 00:00:00</ExpirationDate><Duration_Of_Work>60 days</Duration_Of_Work><Location_Of_Work>locate along the r/o/w on both sides of the rd - including the rd itself - from inter to inter </Location_Of_Work><Type_Of_Work>rpr man hole tops</Type_Of_Work><Work_Done_For>gpc</Work_Done_For><Explosives>No</Explosives><Overhead>No</Overhead><Tunneling_Or_Boring>Yes</Tunneling_Or_Boring><White-lined>No</White-lined><Remarks>Previous Ticket Number:05161-250-011

Previous Ticket Number:06011-254-020

Previous Ticket Number:06171-300-030</Remarks><Subdivision /><Excavator><Company>DILLARD SMITH CONSTRUCTION</Company><Address>548 LAKE MIRROR RD</Address><City>COLLEGE PARK</City><State>GA</State><Zip>30349</Zip><Contact><Name>Johnny Southern</Name><Primary_Phone>4047617924</Primary_Phone><Alternate_Phone>4047617924</Alternate_Phone><Fax /><Email>awright@dillardsmith.com</Email><Priority>0</Priority></Contact></Excavator><Dig_Site><Lookup_By>MANUAL</Lookup_By><Address_Info><State>GA</State><County>FULTON</County><Place>ATLANTA</Place><In_Or_Out>Yes</In_Or_Out><Street><From_Address> </From_Address><To_Address /><Direction_Prefix /><Street_Name>Trinity</Street_Name><Street_Type>Ave</Street_Type><Direction_Suffix>SW</Direction_Suffix></Street></Address_Info><Near_Street><Direction_Prefix /><Street_Name /><Street_Type /><Direction_Suffix /></Near_Street><TypeOfUtility /><FacilityMarks /><FacilityDamaged /><DamagedMemberCodes /><FacilitiesMarkedCorrectly /><LineTypeDamaged /><TypeOfEquipmentUsed /><PropertyDamage /><CrewOnSiteDamage>False</CrewOnSiteDamage><ExtentDamage /><HasFieldContact>False</HasFieldContact><ServiceOut>No</ServiceOut><Grids><Grid><GridItem><Type>Grid</Type><Value>3344A8423C</Value></GridItem><GridItem><Type>Grid</Type><Value>3344A8423B</Value></GridItem></Grid></Grids><Polygon><Point>-84.392159919954651, 33.748554395723424</Point><Point>-84.392148997136331, 33.748447032885082</Point><Point>-84.39211321068062, 33.748343513143055</Point><Point>-84.392053935895774, 33.748247814545763</Point><Point>-84.391973450728145, 33.748163614870215</Point><Point>-84.39187484821953, 33.748094149673619</Point><Point>-84.390901850920869, 33.74753515256004</Point><Point>-84.390881042256225, 33.7475236765345</Point><Point>-84.390554043314879, 33.747350677454158</Point><Point>-84.390438834183684, 33.747302226805324</Point><Point>-84.390314523823875, 33.747273482936869</Point><Point>-84.390185889351628, 33.747265550445441</Point><Point>-84.3900578738986, 33.747278734083586</Point><Point>-84.3899353970795, 33.747312527342373</Point><Point>-84.389823165464264, 33.747365631561031</Point><Point>-84.389725492063036, 33.747436005972709</Point><Point>-84.389646130321893, 33.74752094617061</Point><Point>-84.389588130171177, 33.747617187945764</Point><Point>-84.389553720502178, 33.747721032938429</Point><Point>-84.389544223784625, 33.747828490362089</Point><Point>-84.389560004977014, 33.747935430723352</Point><Point>-84.389600457842363, 33.748037744321223</Point><Point>-84.389664027821013, 33.748131499320486</Point><Point>-84.389748272025813, 33.748213092744557</Point><Point>-84.389849952955288, 33.74827938890936</Point><Point>-84.390166419403911, 33.74844681646622</Point><Point>-84.391129147820408, 33.748999916604234</Point><Point>-84.391242079389144, 33.749051978363617</Point><Point>-84.391364999657654, 33.749084635286238</Point><Point>-84.391493184862426, 33.749096632291234</Point><Point>-84.391621708856917, 33.749087508461969</Point><Point>-84.391745632422342, 33.749057614349638</Point><Point>-84.391860193136921, 33.749008098769409</Point><Point>-84.39196098854444, 33.748940864707713</Point><Point>-84.392044144987608, 33.748858495874259</Point><Point>-84.392106466894546, 33.748764157733625</Point><Point>-84.392145559251816, 33.74866147566302</Point><Point>-84.392159919954651, 33.748554395723424</Point></Polygon></Dig_Site><Ticket_Text>AGL103  00006 GAUPC 03/13/12 17:41:32 07021-300-032-000 NORMAL RESTAK
Underground Notification             
Notice : 07021-300-032 Date: 07/02/11  Time: 23:09  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From:        To:        Name:    TRINITY                        AVE  SW 
Cross1: Name:    CENTRAL                        AVE  SW 
Near  : Name:                                       

State : GA County: FULTON        Place: ATLANTA                                 
Cross2: Name:    WASHINGTON                     ST   SW 

Subdivision:                                         
Locate: LOCATE ALONG THE R/O/W ON BOTH SIDES OF THE RD - INCLUDING THE RD ITSEL
      :  F - FROM INTER TO INTER                                                

Grids       : 3344A8423B 3344A8423C 
Work type   : RPR MAN HOLE TOPS                                                   

Start date: 07/08/11 Time: 07:00 Hrs notc : 000
Legal day : 07/08/11 Time: 07:00 Good thru: 07/26/11 Restake by: 07/21/11
RespondBy : 07/07/11 Time: 23:59 Duration : 60 DAYS    Priority: 3
Done for  : GPC                                     
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : PREVIOUS TICKET NUMBER:05161-250-011  PREVIOUS TICKET NUMBER:06011-2
        : 54-020  PREVIOUS TICKET NUMBER:06171-300-030                        

Company : DILLARD SMITH CONSTRUCTION                Type: CONT                
Co addr : 548 LAKE MIRROR RD                       
City    : COLLEGE PARK                    State   : GA Zip: 30349              
Caller  : JOHNNY SOUTHERN                 Phone   :  404-761-7924              
Fax     :                                 Alt. Ph.:  404-761-7924              
Email   : AWRIGHT@DILLARDSMITH.COM                                            
Contact :                                                           

Submitted date: 07/02/11  Time: 23:09  Oper: 153
Mbrs : AGL103 ATL01 ATL02 BSCA COMCEN GAUPC GP103 MCI02 MESXX NU103 ST005 
-------------------------------------------------------------------------------
</Ticket_Text><ProposedMeetingDate>1900-01-01T23:59:59-05:00</ProposedMeetingDate><ProposedMeetingTime /><ProposedMeetingLocation /><MeetingContactName /><MeetingContactPhone /><MeetingContactEmail /><ScopeOfWork /><ItoICounty>FULTON</ItoICounty><ItoICity>ATLANTA</ItoICity><ItoIPrefix /><ItoIStreetName>Washington</ItoIStreetName><ItoIStreetType>St</ItoIStreetType><ItoISuffix>SW</ItoISuffix></TICKET></TicketDataSet>