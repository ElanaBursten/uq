Please locate and mark per the attached file(s).

If you have any questions or concerns, please contact rsnyder@sunesys.com.

SUNESYSLLC 00011A USAS 04/04/14 09:21:36 A40940218-00A NORM NEW GRID

Ticket : A40940218  Date: 04/04/14 Time: 09:21 Oper: WCA1 Chan: WEB
Old Tkt: A40940218  Date: 04/04/14 Time: 09:21 Oper: WCA1 Revision: 00A

Company: WEST COAST ARBORISTS           Caller: JUSTIN
Co Addr: 2200 E VIA BURTON
City&St: ANAHEIM, CA                    Zip: 92806      Fax: 714-991-7844
Phone: 714-956-4793 Ext: 130
Formn: JUSTIN               Phone: 714-956-4793 Ext: 124
Email: AMANOS@WCAINC.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 2509        Street:THISTLEWOOD LN
X/ST 1 : CHICORY AVE
MPM 1:             MPM 2:
Locat: (1) TREE BEING REMOVED IN FRONT OF THIS ADDRESS.

Excav Enters Into St/Sidewalk: N

Grids: 0742G063
Lat/Long  : 33.869905/-117.619998 33.870472/-117.619289
          : 33.868753/-117.619075 33.869320/-117.618367
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : TREE AND STUMP REMOVAL
Wkend: N  Night: N
Work date: 04/11/14 Time: 08:00 Hrs notc: 166 Work hrs: 118 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : CITY OF CORONA

Tkt Exp: 05/02/14

Mbrs : ATTDSOUTH     COR19  MWD06  NEXTGLAVEN    SCG1CO SUNESYSLLC    UCOR19
USCE34 UTWCWRIV


