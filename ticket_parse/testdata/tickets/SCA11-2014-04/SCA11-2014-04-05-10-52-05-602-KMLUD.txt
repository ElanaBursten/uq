Please locate and mark per the attached file(s).

If you have any questions or concerns, please contact rsnyder@sunesys.com.

SUNESYSLLC 00012A USAS 04/04/14 09:21:49 A40940219-00A NORM NEW GRID

Ticket : A40940219  Date: 04/04/14 Time: 09:18 Oper: BSV Chan: 100
Old Tkt: A40940219  Date: 04/04/14 Time: 09:21 Oper: BSV Revision: 00A

Company: A & A SEPTIC                   Caller: ANNA LAUING
Co Addr: P O BOX 283
City&St: APPLE VALLEY, CA               Zip: 92307      Fax: 760-247-1964
Phone: 760-240-6619 Ext:      Call back: 8AM-5PM
Formn: STEVE LAUING

State: CA County: SAN BERNARDINO  Place: APPLE VALLEY
Delineated: Y
Delineated Method: WHITEPAINT
Address: 12332       Street:CENTRAL RD
X/ST 1 : SIOUX RD
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 4388B053
Lat/Long  : 34.476364/-117.173880 34.476376/-117.172327
          : 34.474973/-117.173869 34.474985/-117.172316
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : SEPTIC REPAIR
Wkend: N  Night: N
Work date: 04/09/14 Time: 08:00 Hrs notc: 118 Work hrs: 070 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : OWNER- RON CROWE

Tkt Exp: 05/02/14

Mbrs : AVLY01 GSWTRAPV      SUNESYSLLC    SWG18  UCHARTERHD    USCE73 UVZVICT


