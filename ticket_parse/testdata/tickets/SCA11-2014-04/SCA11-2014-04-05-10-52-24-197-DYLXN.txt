Please locate and mark per the attached file(s).

If you have any questions or concerns, please contact rsnyder@sunesys.com.

SUNESYSLLC 00010A USAS 04/04/14 09:15:43 A40940200-00A NORM NEW GRID

Ticket : A40940200  Date: 04/04/14 Time: 09:05 Oper: KKP Chan: 100
Old Tkt: A40940200  Date: 04/04/14 Time: 09:15 Oper: KKP Revision: 00A

Company: CORE PROBE INTERNATIONAL INC   Caller: STEWART GRAHAM
Co Addr: 5075 WALNUT GROVE AVE
City&St: SAN GABRIEL, CA                Zip: 91776      Fax: 626-309-9978
Phone: 626-309-9932 Ext:      Call back: ANYTIME
Formn: STEWART GRAHAM       Phone: 626-327-2902
Email: INFO@COREPROBE.COM

State: CA County: VENTURA         Place: SIMI VALLEY
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:COCHRAN ST
X/ST 1 : SYCAMORE DR
MPM 1:             MPM 2:
Locat: 2941 COCHRAN ST UNIT 01 X/ST SYCAMORE DR *WORK IS WITHIN SYCAMORE SQUARE
     : SHOPPING CENTER IN PARKING STALL LOC IN FRONT OF UNIT 01, ALSO SIDEWALK
     : IN FRONT S/SIDE OF UNIT 01 *

Excav Enters Into St/Sidewalk: N

Grids: 0498C01
Lat/Long  : 34.282207/-118.743217 34.282207/-118.737648
          : 34.279057/-118.743217 34.279057/-118.737648
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : SOIL SAMPLES & VAPOR SAMPLES
Wkend: N  Night: N
Work date: 04/15/14 Time: 08:00 Hrs notc: 262 Work hrs: 166 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : FENIX DEVELOPMEN

Tkt Exp: 05/02/14

Mbrs : ATTDSOUTH     CMW52  GSWTRSIM      SCG4UO SUNESYSLLC    USCE16 UTWCNW39


