
BSEG  00245 GAUPC 04/26/12 08:16:14 04262-300-104-000 NORMAL
Underground Notification             
Notice : 04262-300-104 Date: 04/26/12  Time: 08:15  Revision: 000 

State : GA County: BALDWIN       Place: MILLEDGEVILLE                           
Addr  : From: 104    To:        Name:    VESTAL                         CIR  NE 
Near  : Name:    PEACEFUL COVE                  RD  

Subdivision:                                         
Locate: LOCATE THE R/O/W BOTH SIDES OF THE ROAD WITHIN THE WHITE LINED AREA.   

Grids       : 
Work type   : REPAIR WATER SERVICE                                                

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : BALDWIN COUNTY                          
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : BALDWIN COUNTY WATER                      Type: MEMB                
Co addr : P. O. BOX 9                              
City    : HARDWICK                        State   : GA Zip: 31034              
Caller  : LORNA HALL                      Phone   :  478-445-6490              
Fax     :  478-445-1699                   Alt. Ph.:  478-445-4237              
Email   : LHALL@BALDWINCOUNTYGA.COM                                           
Contact :                                                           

Submitted date: 04/26/12  Time: 08:15  Oper: 235
Mbrs : BCW90 BCW91 BSEG GAUPC ITM02 TRI70 WSMDVL 
-------------------------------------------------------------------------------

