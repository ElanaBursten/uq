
BSES  01921 GAUPC 04/26/12 13:56:15 04262-300-947-000 NORMAL
Underground Notification             
Notice : 04262-300-947 Date: 04/26/12  Time: 13:54  Revision: 000 

State : GA County: GLYNN         Place: BRUNSWICK                               
Addr  : From: 3116   To:        Name:    SHRINE                         RD      
Near  : Name:    WILDWOOD                       DR  

Subdivision:                                         
Locate:  AROUND AREA MARKED WITH WHITE FLAGS..LOCATE AREA ACROSS THE STREET NEX
      :  T TO MAILBOX..INSTALLING ANCHOR AND PULLING POLE                       

Grids       : 
Work type   : INSTALL ANCHORS                                                     

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 1 WEEK     Priority: 3
Done for  : GEORGIA POWER                           
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks : CONTACT MITCH CHORBA 912-717-3428                                   

Company : GEORGIA POWER                             Type: MEMB                
Co addr : 3951 COMMUNITY ROAD                      
City    : BRUNSWICK                       State   : GA Zip: 31525              
Caller  : STACY BELL                      Phone   :  912-267-5180              
Fax     :                                 Alt. Ph.:                            
Email   : SLBELL@SOUTHERNCO.COM                                               
Contact : MITCH CHORBA                    912-717-3428              

Submitted date: 04/26/12  Time: 13:54  Oper: 196
Mbrs : AGL105 BSES BWK50 BWK51 BWK52 CNT01 GAUPC GLY92 GP510 
-------------------------------------------------------------------------------

