
BGAWA  02636 GAUPC 04/26/12 15:52:19 04262-301-371-000 NORMAL
Underground Notification             
Notice : 04262-301-371 Date: 04/26/12  Time: 15:51  Revision: 000 

State : GA County: TIFT          Place: TIFTON                                  
Addr  : From: 2      To:        Name:    GENE                           LN      
Near  : Name:    FERRY LAKE                     RD  

Subdivision:                                         
Locate: LOCATE ENTIRE FRONT OF PROPERTY INCLUDING RIGHT OF WAY BOTH SIDES OF RO
      :  AD.                                                                    

Grids       : 
Work type   : INSTALL SEWER TAP                                                   

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 4 HOURS    Priority: 3
Done for  : CITY OF TIFTON                          
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : MARKED WITH GREEN PAINT                                             

Company : CITY OF TIFTON                            Type: MEMB                
Co addr : 204 N. RIDGE AVENUE                      
City    : TIFTON                          State   : GA Zip: 31794              
Caller  : DANA SUMNER                     Phone   :  229-391-3949              
Fax     :                                 Alt. Ph.:                            
Email   : DTABOR@TIFTON.NET                                                   
Contact :                                                           

Submitted date: 04/26/12  Time: 15:51  Oper: 210
Mbrs : BGAWA CLQ72 GAUPC GP730 TCI12 TFT50 TFT51 TFT52 TFT54 TFT55 PTC02 
-------------------------------------------------------------------------------

