
BSNW  00177 GAUPC 04/26/12 08:04:13 04262-300-073-000 NORMAL
Underground Notification             
Notice : 04262-300-073 Date: 04/26/12  Time: 08:02  Revision: 000 

State : GA County: CHEROKEE      Place: WOODSTOCK                               
Addr  : From: 113    To:        Name:    ALLATOONA                      DR      
Near  : Name:    LOVINGOOD                      DR  

Subdivision: ALLATOONA SHORES                        
Locate: PLEASE LOCATE ALL UTILITIES   FRONT LEFT QUADRANT    AREA WHITE LINED F
      :  OR STUMP GRINDING                                                      

Grids       : 
Work type   : STUMP GRINDING                                                      

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 1 WEEK     Priority: 3
Done for  : COBB EMC                                
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks : DOUG CUNNINGHAM                                                     

Company : COBB EMC                                  Type: MEMB                
Co addr : 1000 EMC PARKWAY                         
City    : MARIETTA                        State   : GA Zip: 30061              
Caller  : JULIE FOWLER                    Phone   :  678-355-3432              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 04/26/12  Time: 08:02  Oper: 192
Mbrs : AGL107 BSNW CKW01 CKW02 COB70 COMNOR GAUPC 
-------------------------------------------------------------------------------

