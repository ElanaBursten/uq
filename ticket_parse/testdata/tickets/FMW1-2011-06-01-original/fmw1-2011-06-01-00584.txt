
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11277304               Update Of: 11203912 
Transmit      Date: 06/01/11      Time: 08:08 AM           Op: wisuem 
Original Call Date: 06/01/11      Time: 08:06 AM           Op: wisuem 
Work to Begin Date: 06/04/11      Time: 12:01 AM 
Expiration    Date: 06/17/11      Time: 11:59 PM 

Place: SILVER SPRING 
Address:             Street: GEORGIA AVE 
Nearest Intersecting Street: RANDOLPH ROAD 

Type of Work: INST WTR,SWR,SANITARY,ELEC SVC 
Extent of Work: LOC GEORGIA AVE FROM RANDOLPH RD TO MASON ST AND FROM E CURB TO
: 100FT E 
Remarks: UPDATE FOR REMARK 

Company      : FLIPPO CONSTRUCTION COMPANY 
Contact Name : TIM JENNINGS                   Fax          : (703)842-7991 
Contact Phone: (301)967-6800                  Ext          : 267 
Caller Address: 3820 PENN BELT PLACE 
                FORESTVILLE, MD 20747 
Email Address:  tajennings@flippo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: MD STATE HWY 
State: MD              County: MONTGOMERY 
MPG:  Y 
Caller    Provided:  Map Name: MONT  Map#: 5286 Grid Cells: B1 
Computer Generated:  Map Name: MONT  Map#: 5286 Grid Cells: B1 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.0625027Lon: -77.0563031 SE Lat: 39.0562662Lon: -77.0500325 
Explosives: N 
PEPCOMC    TRU02      WGL06      WSS01 
Send To: VMG       Seq No: 0108   Map Ref:  
