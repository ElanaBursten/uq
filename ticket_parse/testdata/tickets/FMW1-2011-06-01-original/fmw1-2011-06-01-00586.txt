
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11277261               Update Of: 11253514 
Transmit      Date: 06/01/11      Time: 08:08 AM           Op: webusr 
Original Call Date: 06/01/11      Time: 08:00 AM           Op: webusr 
Work to Begin Date: 06/04/11      Time: 12:01 AM 
Expiration    Date: 06/17/11      Time: 11:59 PM 

Place: ROCKVILLE 
Address: 610         Street: WARFIELD DR 
Nearest Intersecting Street: GOLDSBOROUGH DR 

Type of Work: REMOVE & REPLACE CONCRETE, SIDEWALK, DRIVEWAY APRON, C & G 
Extent of Work: LOCATE AND MARK IN FRONT OF PROPERTY  FOR REMOVAL & REPLACEMENT
: OF CONCRETE SIDEWALK, DRIVEWAY APRON, CURB & GUTTER 
Remarks:  

Company      : ROMANO CONCRETE CONST INC 
Contact Name : PETER GONZALEZ                 Fax          : (301)362-8881 
Contact Phone: (301)362-0080                  Ext          :  
Caller Address: 8970 MAIER PLACE 
                LAUREL, MD 20723 
Email Address:  peter@romanoconcrete.com 
Alt. Contact : MIKE SCOTT                     Alt. Phone   : (301)362-0080 
Work Being Done For: CITY OF ROCKVILLE 
State: MD              County: MONTGOMERY 
MPG:  Y 
Caller    Provided:  Map Name: MONT  Map#: 5164 Grid Cells: D6 
Computer Generated:  Map Name: MONT  Map#: 5164 Grid Cells: D6 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.0937602Lon: -77.1687677 SE Lat: 39.0875073Lon: -77.1625112 
Explosives: N 
CTR05      MCI01      PEPCOMC    TRN01      VMG        WGL06      WSS01 
Send To: TRU01     Seq No: 0042   Map Ref:  
         TRU02             0073 
