
CVINET 00051 USAN 03/24/14 12:08:34 0111824 SHORT NOTICE 

Message Number: 0111824 Received by USAN at 12:00 on 03/24/14 by AMC

Work Begins:    03/25/14 at 09:00   Notice: 006 hrs      Priority: 1
Night Work: N    Weekend Work: N

Expires: 04/21/14 at 23:59   Update By: 04/17/14 at 16:59

Caller:         ALEX MARQUEZ             
Company:        V.C.I. TELCOM                      
Address:        1921 W.11TH ST                          
City:           UPLAND                        State: CA Zip: 91786
Business Tel:   909-946-0905                  Fax:                             
Email Address:  JCLAVEAU@VCICOM.COM                                         

Nature of Work: EMERG DIG TO REPL COLLAPSE PULL BOX     
Done for:       MCI                           Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 909-376-8089           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    AT A POINT 1500FT NE/O INT TRUXTUN AVE AND COMMERCIAL WAY ON NW/SI/O
    TRUXTUN AVE
    WRK LOC UND THE BRIDGE CONT 100'NE & 100'SW  

Place: BAKERSFIELD                  County: KERN                 State: CA

Long/Lat Long: -119.059582 Lat:  35.369087 Long: -119.055558 Lat:  35.372383 


Sent to:
BHNBFD = BRIGHT HOUSE NETWORKS        CWSBFD = CALIF WTR SVC-BAKERSFLD      
CTYBFD = CITY BAKERSFIELD             CVINET = CVIN LLC                     
EBNATR = E & B NATURAL RESOURCES      KRNWT2 = KERN CO WTR AGENCY 2         
MCIWSA = MCI WORLDCOM                 PACBEL = PACIFIC BELL                 
PGEBFD = PGE DISTR BAKERSFIELD        SJFMAN = SAN JOAQUIN FACILITIES M     
SCGBFD = SO CAL GAS BAKERSFIELD       TWTBFD = TW TELECOM - BAKERSFIELD