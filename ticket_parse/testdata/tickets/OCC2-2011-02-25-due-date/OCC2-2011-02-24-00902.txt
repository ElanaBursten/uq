
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2011-02-24T15:37:00</transmitted>
    <seq_num>511</seq_num>
    <ticket>B105501386</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B105501386</ticket>
    <revision>00B</revision>
    <started>2011-02-24T15:35:51</started>
    <account>WTDEANE</account>
    <original_ticket>B105501386</original_ticket>
    <original_date>2011-02-24T15:35:51</original_date>
    <original_account>WTDEANE</original_account>
    <replace_by_date>2011-03-15T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2011-03-01T07:00:00</response_due>
    <expires>2011-03-18T07:00:00</expires>
    <state>VA</state>
    <county>PRINCE WILLIAM</county>
    <st_from_address>5140</st_from_address>
    <st_to_address>5142</st_to_address>
    <street>RACE POINTE PL</street>
    <work_type>FIOS</work_type>
    <done_for>VERIZON</done_for>
    <reference>E7002320/8753/5845C</reference>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>Y</boring>
    <name>S &amp; N COMMUNICATIONS</name>
    <address1>3723 THREE NOTCH RD</address1>
    <city>LOUISA</city>
    <cstate>VA</cstate>
    <zip>23093</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>4345910241</phone>
    <caller>TRACIE DEANE</caller>
    <caller_phone>4345910241</caller_phone>
    <email>tdeane@sncommfo.com</email>
    <contact>SHARON BECK</contact>
    <contact_phone>5404506654</contact_phone>
    <map_reference>5991C2</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.675315</latitude>
        <longitude>-77.361084</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.675369</latitude>
        <longitude>-77.361496</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.675692</latitude>
        <longitude>-77.360908</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.675262</latitude>
        <longitude>-77.360672</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.674940</latitude>
        <longitude>-77.361260</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >WORK AREA WILL BE FROM HANDHOLE LOCATED REAR OF 5140 TO THE HOUSE OF 5142.
LOCATE ALL SERVICES,MAINS AND EASEMENTS IF ANY.</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE
SNNLOC  Y</remarks>
    <polygon>
      <coordinate>
        <latitude>38.675369</latitude>
        <longitude>-77.361496</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.675003</latitude>
        <longitude>-77.361145</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.675262</latitude>
        <longitude>-77.360672</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.675594</latitude>
        <longitude>-77.361061</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3840B7721B-41</grid>
      <grid>3840B7721B-42</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CMC703</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>VAW902</member>
        <group_code>VAW</group_code>
        <description>VIRGINIA AMERICAN WATER COMPAN</description>
      </memberitem>
      <memberitem>
        <member>VZN213</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
