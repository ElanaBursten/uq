
KORTERRA JOB UTILIQUEST-SMTP-1 NJ120530482 Seq: 139 02/22/2012 10:07:39
New Jersey One Call System        SEQUENCE NUMBER 0003    CDC = GP4

Transmit:  Date: 02/22/12   At: 10:05

*** U P D A T E           *** Request No.: 120530482  Of Request No.: 120480472

Operators Notified:
EG2     = ELIZABETHTOWN GAS COMPANY     GP4     = JERSEY CENTRAL POWER & LI
NEW     = TOWN OF NEWTON                SE1     = SERVICE ELECTRIC CABLE TV
SPW     = SPARTA TOWNSHIP WATER & S

Start Date/Time:    02/24/12   At 07:00  Expiration Date: 04/24/12

Location Information:
County: SUSSEX                 Municipality: SPARTA
Subdivision/Community:
Street:               0 TOWN CENTER DR
Nearest Intersection: CO RD 517
Other Intersection:
Lat/Lon:
Type of Work: INSTALL SERVICE UTILITIES
Block: 19.03          Lot:  48            Depth: 6FT
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 300FT S.  CURB
  TO 300FT BEHIND E CURB.
Remarks:
  CALLER STATES THAT JCP&L SAID THERE WAS NO CONLFLICT BUT THERE
  IS A TRANSFORMER
  Working For Contact:  FRANK CAPUTO

Working For: TM BRENNAN CONTRACTORS
Address:     3505 RTE 94
City:        HAMBURG, NJ  07419
Phone:       973-209-2545   Ext: 124

Excavator Information:
Caller:      BILL VAN WINGERDEN
Phone:       973-948-0551   Ext:

Excavator:   VAN WINGERDEN ASSOCIATES
Address:     47 AUGUSTA HILL RD
City:        AUGUSTA, NJ  07822
Phone:       973-948-0551   Ext:          Fax:  973-948-0580
Cellular:    973-445-5490
Email:       bvanwing@embarqmail.com
End Request



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.