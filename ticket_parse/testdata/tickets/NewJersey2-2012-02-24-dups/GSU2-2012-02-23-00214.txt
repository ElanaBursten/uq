
New Jersey One Call System        SEQUENCE NUMBER 0011    CDC = FPC3 

Transmit:  Date: 02/23/12   At: 10:14 

*** U P D A T E           *** Request No.: 120540554  Of Request No.: 120451719 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
FPC     = JCP&L (FIRST ENERGY)          NJN     = NEW JERSEY NATURAL GAS CO      

Start Date/Time:    02/18/12   At 00:15  Expiration Date: 04/19/12 

Location Information: 
County: MONMOUTH               Municipality: COLTS NECK 
Subdivision/Community:  
Street:               0 SADDLE RIDGE ROAD 
Nearest Intersection: SOUTH ST 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL GAS MAIN 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 2000FT W.
  CURB TO 10FT BEHIND BOTH CURBS.  CURB TO CURB. 
Remarks:  
  CALLER STATES VERIZON MISMARKED 
  Working For Contact:  DAN HUBER 

Working For: NJNG 
Address:     1415 WYCKOFF ROAD 
City:        WALL, NJ  07719 
Phone:       732-919-8156   Ext:  

Excavator Information: 
Caller:      NICOLE COLANDRO 
Phone:       732-222-4400   Ext: 243 

Excavator:   J F KIELY CONSTRUCTION COMPANY 
Address:     700 MCCLELLAN ST 
City:        LONG BRANCH, NJ  07740 
Phone:       732-222-4400   Ext:          Fax:  732-229-2353 
Cellular:     
Email:       ncolandro@jfkiely.com 
End Request 
