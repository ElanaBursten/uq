
KORTERRA JOB UTILIQUEST-SMTP-1 NJ120531020 Seq: 316 02/22/2012 12:43:03
New Jersey One Call System        SEQUENCE NUMBER 0050    CDC = GPS

Transmit:  Date: 02/22/12   At: 12:41

*** U P D A T E           *** Request No.: 120531020  Of Request No.: 120410492

Operators Notified:
AMG     = /AMERIGAS                     BAN     = VERIZON
CC3     = COMCAST CABLEVISION OF CE     GPS     = JERSEY CENTRAL POWER & LI
MOT     = MONROE TOWNSHIP UTIL. DEP     NN3     = NEW JERSEY NATURAL GAS CO
P29     = PUBLIC SERVICE ELECTRIC &     SP1     = /SBRBN RBNSV

Start Date/Time:    02/16/12   At 08:00  Expiration Date: 04/17/12

Location Information:
County: MIDDLESEX              Municipality: MONROE
Subdivision/Community: PRIMROSE ACRES
Street:               10 HANNAH CT
Nearest Intersection: SPOTSWOOD GRAVEL HILL ROAD
Other Intersection:   COREY DR
Lat/Lon:
Type of Work: INSTALL ELECTRIC SERVICE
Block: 53             Lot:  10.38         Depth: 7FT
Extent of Work: CURB TO ENTIRE PROPERTY.  LOT AND BLOCK IS POSTED.
Remarks:
  CALLER STATES THAT ALL THE UNTILITY COMPANIES MIS-MARKED
  Working For Contact:  MIKE STRIKE

Working For: REGAL HOMES
Address:     242 RTE 79 SUITE 9
City:        MORGANVILLE, NJ  07751
Phone:       732-656-3087   Ext:

Excavator Information:
Caller:      DONNA CURLEY
Phone:       732-780-5923   Ext:

Excavator:   R. CURLEY BACKHOE SERVICE
Address:     97 SCHIBANOFF ROAD
City:        FREEHOLD, NJ  07728
Phone:       732-780-5923   Ext:          Fax:  732-780-2447
Cellular:    732-780-5923
Email:
End Request



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.