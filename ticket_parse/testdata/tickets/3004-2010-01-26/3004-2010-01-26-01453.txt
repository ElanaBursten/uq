
AGL123  01927 GAUPC 01/26/10 15:56:01 01260-231-046-001 NORMAL CANCEL
Underground Notification             
Notice : 01260-231-046 Date: 01/26/10  Time: 15:52  Revision: 001 

State : GA County: DEKALB        Place: ATLANTA                                 
Addr  : From: 2665   To:        Name:    MEADOW                         CT      
Near  : Name:    FLINDT                         CT  

Subdivision: MEADOW WOOD CONDOS                      
Locate:  FRONT THE LEFT QUADRANT  OF 2667 AND LOC THE RIGHT SIDE OF 2665 *** PL
      :  EASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, P
      :  LEASE RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-4
      :  61-7271. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA THE SAM
      :  E METHODS. ***                                                         

Grids       : 3352A8416A 
Work type   : REPLACE DRAINAGE SYSTEM STORM DRAINAGE                              

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 DAYS     Priority: 3
Done for  : MEADOW WOOD CONDOS                      
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : CANCELED, PLEASE DISREGARD NEEDED RUSH  REFER TO 01260-231-046  DPA/
        : 231                                                                 

Company : ACE CONTRACTORS                           Type: CONT                
Co addr : 3080 MEADOW MERE WEST                    
City    : CHAMBLEE                        State   : GA Zip: 30341              
Caller  : SCOTT SHERFIELD                 Phone   :  678-644-3297              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:52  Oper: 198
Mbrs : AGL123 BSNE COMCEN DCWS01 DCWS02 GAUPC GP803 
-------------------------------------------------------------------------------
