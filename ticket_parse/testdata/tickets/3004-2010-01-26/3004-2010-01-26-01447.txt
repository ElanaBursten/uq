
AGL114  01918 GAUPC 01/26/10 15:52:08 01260-301-224-000 NORMAL
Underground Notification             
Notice : 01260-301-224 Date: 01/26/10  Time: 15:50  Revision: 000 

State : GA County: GWINNETT      Place: DULUTH                                  
Addr  : From:        To:        Name:    BOGGS                          RD      
Near  : Name:    I-85                           INTE

Subdivision:                                         
Locate: LOCATE STARTING IN THE NW CORNER OR THE SOUTHBOUND RAMP OF  I85 AND BOG
      :  GS RD. CONTINUE DOWN TO I85 - CALL ROSS FOR DETAIL INSTRUCTIONS @ (404)
      :  557-2268                                                               

Grids       : 3357A8405A 3357A8406C 3357A8406D 3357B8406C 3357B8406D 
Grids       : 
Work type   : INSTALLING TRAFFIC SIGNALS                                          

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 1 MONTH    Priority: 3
Done for  : GDOT                                    
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Road, Driveway & Sidewalk     

Company : RJ HAYNIE & ASSOCIATES INC                Type: CONT                
Co addr : PO BOX 1767                              
City    : FOREST PARK                     State   : GA Zip: 30298              
Caller  : JAN DANIELL                     Phone   :  404-361-0672              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:50  Oper: 214
Mbrs : AGL114 AGLN01 BSNE CBL01 COMCEN CPL81 GAUPC GP263 GP266 GWI90 
     : GWI91 JCK70 TWT90 
-------------------------------------------------------------------------------
