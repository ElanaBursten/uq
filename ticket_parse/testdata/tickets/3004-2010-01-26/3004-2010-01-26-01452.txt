
AGL111  01925 GAUPC 01/26/10 15:52:12 01260-220-042-000 NORMAL
Underground Notification             
Notice : 01260-220-042 Date: 01/26/10  Time: 15:51  Revision: 000 

State : GA County: FULTON        Place: ROSWELL                                 
Addr  : From: 8355   To:        Name:    RIVERBIRCH                     DR      
Near  : Name:    STEEPLE CHASE                  DR  

Subdivision:                                         
Locate:  REAR AND BOTH SIDES OF PROPERTY *** PLEASE NOTE: THIS TICKET WAS GENER
      :  ATED FROM THE EDEN SYSTEM. UTILITIES, PLEASE RESPOND TO POSITIVE RESPON
      :  SE VIA HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE CHEC
      :  K THE STATUS OF YOUR TICKET VIA THE SAME METHODS. ***                  

Grids       : 3358A8416A 3359D8416A 
Work type   : INSTALL DRAINAGE SYSTEM                                             

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : BOB KNOUSE                              
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : H G HASTINGS                              Type: CONT                
Co addr : 3920 PEACHTREE RD                        
City    : ATLANTA                         State   : GA Zip: 30319              
Caller  : MICHAEL TONER                   Phone   :  404-869-7448              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:51  Oper: 198
Mbrs : AGL111 BSNW COMNOR FUL01 FUL02 GAUPC GP814 SAW72 XOC90 
-------------------------------------------------------------------------------
