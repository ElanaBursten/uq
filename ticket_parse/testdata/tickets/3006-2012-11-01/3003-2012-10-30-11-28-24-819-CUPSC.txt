
------=_Part_12872_14460881.1351262984314
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 200 Transmitted: Fri Oct 26 09:49:44 CDT 2012

BGAWS  00210 GAUPC 10/26/12 10:46:40 10262-252-029-000 EMERG 
Underground Notification             
Notice : 10262-252-029 Date: 10/26/12  Time: 10:46  Revision: 000 

State : GA County: HOUSTON       Place: WARNER ROBINS                           
Addr  : From: 301    To:        Name:    TRACY                          TER     
Near  : Name:    RUSSELL                        PKWY

Subdivision:                                         
Locate:  FRONT OF PROPERTY INCLUDING RIGHT OF WAY                              

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3235A8338B 
Work type   : REPLACE SEWER SERVICE                                               

Start date: 10/26/12 Time: 00:00 Hrs notc : 000
Legal day : 10/26/12 Time: 10:43 Good thru: 10/31/12 Restake by: 10/26/12
RespondBy : 10/26/12 Time: 10:43 Duration : UNKNOWN    Priority: 1
Done for  : ROBERT CONCKLIN PLUMBING                
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : CREW EN ROUTE/1 HOUR EMERGENCY: SERVICE IS OUT/SEWAGE BACKING UP    

Company : D & W EXCAVATING                          Type: CONT                
Co addr : 223 FALCON CREST WAY                     
City    : BYRON                           State   : GA Zip: 31008              
Caller  : DWAYNE WAGNER                   Phone   :  478-951-1331              
Fax     :                                 Alt. Ph.:  478-951-1331              
Email   :                                                                     
Contact : DWAYNE WAGNER                   478-951-1331              

Submitted date: 10/26/12  Time: 10:46  Oper: 198
Mbrs : BGAWS CCM01 GAUPC WAR90 WAR91 WAR92 FLI71

-------------------------------------------------------------------------------



------=_Part_12872_14460881.1351262984314--