
------=_Part_12086_13488087.1351262422391
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 191 Transmitted: Fri Oct 26 09:40:22 CDT 2012

BGAWS  00185 GAUPC 10/26/12 10:24:08 10262-300-235-000 NORMAL
Underground Notification             
Notice : 10262-300-235 Date: 10/26/12  Time: 10:24  Revision: 000 

State : GA County: TROUP         Place: LAGRANGE                                
Addr  : From: 50     To:        Name:    SL WHITE BOULEVARD                     
Near  : Name:    CALLAWAY CHURCH RD                 

Subdivision:                                         
Locate: THIS IS THE ITW DAELIM BUILDING. ON THE SOUTH WEST CORNER OF THE BUILDI
      :  NG THERE IS A WHITELINED MARK. FOLLOW IT TO THE SOUTH ENTRANCE AND IT W
      :  ILL TURN AND HEAD EAST TO CALLAWAY CHURCH RD , AND ACROSS CALLAWAY CHUR
      :  CH RD TO A POLE WITH A RIBBON AROUND IT.  ALL AREA OF BORING IS WHITELI
      :  NED                                                                    

Grids       : 3302D8457A 
Work type   : INSTALLING POWER SERVICE                                            

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 3 DAYS     Priority: 3
Done for  : ELECTRIC DEPT                           
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks :                                                                     

Company : CITY OF LAGRANGE                          Type: MEMB                
Co addr : 303 LUKKEN INDUSTRIAL DR W               
City    : LAGRANGE                        State   : GA Zip: 30240              
Caller  : BILLY WHITTEN                   Phone   :  706-302-2816              
Fax     :                                 Alt. Ph.:                            
Email   : BWHITTEN@LAGRANGE.NET                                               
Contact :                                                           

Submitted date: 10/26/12  Time: 10:24  Oper: 154
Mbrs : AGL113 BGAWS CHC04 GAUPC LAG50 LAG51 LAG52 LAG53 LAG54 TRP70

-------------------------------------------------------------------------------



------=_Part_12086_13488087.1351262422391--