
------=_Part_12195_20904690.1351262502485
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 193 Transmitted: Fri Oct 26 09:41:42 CDT 2012

BGAWS  00202 GAUPC 10/26/12 10:39:03 10262-300-254-000 NORMAL
Underground Notification             
Notice : 10262-300-254 Date: 10/26/12  Time: 10:39  Revision: 000 

State : GA County: BUTTS         Place: JACKSON                                 
Addr  : From: 406    To:        Name: S  OAK                            ST      
Near  : Name: W  COLLEGE                        ST  

Subdivision:                                         
Locate: LOCATE FRONT OF PROPERTY INCLUDING 10' AROUND METER OR RISER AND BOTH S
      :  IDES OF THE ROAD                                                       

Grids       : 3317C8357A 3317B8357A 3317B8358D 3317C8358D 
Work type   : KILLING GAS SERVICE                                                 

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : AGL                                     
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : ATLANTA GAS LIGHT                         Type: MEMB                
Co addr : 5472 NEW FORSYTH RD                      
City    : MACON                           State   : GA Zip: 31210              
Caller  : ANDREA WILLIAMS                 Phone   :  478-476-2280              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 10/26/12  Time: 10:39  Oper: 185
Mbrs : AGL113 BGAWS GAUPC GP141S JAC50 JAC51 JAC52

-------------------------------------------------------------------------------



------=_Part_12195_20904690.1351262502485--