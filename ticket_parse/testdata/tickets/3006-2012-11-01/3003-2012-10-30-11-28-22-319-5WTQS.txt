
------=_Part_12500_31232153.1351262799750
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 197 Transmitted: Fri Oct 26 09:46:39 CDT 2012

BGAWS  00205 GAUPC 10/26/12 10:43:09 10262-300-260-000 NORMAL
Underground Notification             
Notice : 10262-300-260 Date: 10/26/12  Time: 10:43  Revision: 000 

State : GA County: SEMINOLE      Place: DONALSONVILLE                           
Addr  : From:        To:        Name:    NICHOLS SUBDIVISION            RD      
Cross1: Name:    GA HIGHWAY 253                         
Near  : Name:                                       

Subdivision:                                         
Locate: LOCATE ENTIRE R/W OF GA HWY 253 1,000 FEET EAST OF NICHOLS SUBDIVISION 
      :  ROAD AND 1,000 FEET WEST.  ALSO LOCATE ENTIRE R/W OF NICHOLS SUBDIVISIO
      :  N ROAD APPROXIMATELY 3,500 FEET TO CUL-DE-SAC. RIGHT OF WAY            

Grids       : 3051D8446D 3051C8446C 3051A8446C 3051B8446D 3051C8446D 
Grids       : 3051A8446D 3051B8446C 
Work type   : GRADING/PAVING TO WIDEN ROAD                                        

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 4 MONTHS+  Priority: 3
Done for  : SEMINOLE CO.                            
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** DrivingInstructions : NICHOLS SUBDIVISION ROAD LOCATED APPROXIMATELY 1 MILE WEST OF DECATU
        : R/SEMINOLE COUNTY LINE.                                             

Company : HATCHER TRACTOR SERVICE                   Type: CONT                
Co addr : 2230 BIRMINGHAM LANE                     
City    : ALBANY                          State   : GA Zip: 31705              
Caller  : KRISTY THORNTON                 Phone   :  229-446-1686              
Fax     :                                 Alt. Ph.:                            
Email   : HTS02@BELLSOUTH.NET                                                 
Contact : STEVE JULIAN                    229-446-1686              

Submitted date: 10/26/12  Time: 10:43  Oper: 170
Mbrs : BGAWS GAUPC GP710 THR70 WSDWSN

-------------------------------------------------------------------------------



------=_Part_12500_31232153.1351262799750--