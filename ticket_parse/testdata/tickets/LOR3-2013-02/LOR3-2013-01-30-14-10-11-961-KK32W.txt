OUNC 
Ticket No: 13017457               48 HOUR NOTICE 
Send To: QLNOR23    Seq No:   37  Map Ref:  

Transmit      Date:  1/29/13   Time: 12:07 pm    Op: ormary 
Original Call Date:  1/29/13   Time: 11:50 am    Op: XMLUSE 
Work to Begin Date:  1/31/13   Time: 12:00 pm 

State: OR            County: CLACKAMAS               Place: OREGON CITY 
Address: 12471       Street: LOCUST FARM CT 

Twp: 3S    Rng: 2E    Sect-Qtr: 6-SW-SE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.3363801 Lon:-122.6125717 SE Lat: 45.3348265 Lon:-122.6094389 

Type of Work: SIGN INSTALL 
Location of Work: LOCATE ONLY WITHIN 3 FEET OF WHITE MARKER FLAG OR COMPANY
: STAKE SIGN, OR IF NO FLAG, LOCATE ONLY A 5 FOOT SQUARE WHERE DRIVEWAY MEETS
: SIDEWALK.
: DO NOT PAINT ON PAVED OR PERMANENT SURFACES.
: DO NOT MARK FROM SOURCE.
: USE AS LITTLE PAINT AS POSSIBLE.
: NO REPLY NEEDED UNLESS CONFLICT FOUND. 

Remarks:  
:  

Company     : NW REALTY SIGN 
Contact Name: CAROL LAFATA                     Phone: (503)735-0445 
Cont. Email : orders@nwrealtysign.com 
Alt. Contact: SAME                             Phone:  
Contact Fax :  
Work Being Done For: NW REALTY SIGN 
Additional Members:  
BCT01      CMCST01    COC01      CWD01      NWN01      PGE01