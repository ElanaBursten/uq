OUNC 
Ticket No: 13017343               48 HOUR NOTICE 
Send To: TUO06      Seq No:    4  Map Ref:  

Transmit      Date:  1/29/13   Time: 10:30 am    Op: orjack 
Original Call Date:  1/29/13   Time: 10:27 am    Op: orjack 
Work to Begin Date:  1/31/13   Time: 10:30 am 

State: OR            County: LINN                    Place: LEBANON 
Address:             Street: S 2ND ST 
Nearest Intersecting Street: ACADEMY 

Twp: 12S   Rng: 2W    Sect-Qtr: 10 
Twp: 12S   Rng: 2W    Sect-Qtr: 10-SE-NE,11-SW-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.5446949 Lon:-122.9091909 SE Lat: 44.5414050 Lon:-122.9064076 

Type of Work: ADDITION 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: ADD APX 100 YARDS S OF ABV INTER. SITE IS AT THE GENERAL PURPOSE BLDG THAT
: IS BEHIND THE HOUSE AT ABV ADD- MARK ENTIRE E AND N SIDE YARDS OF ABV ADD 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : ST EDWARD CATHOLIC CHURCH 
Contact Name: KEITH HALE                       Phone: (541)619-5289 
Cont. Email :  
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: ST EDWARD CATHOLIC CHURCH 
Additional Members:  
CMCST02    LEB01      NWN01      ODOTRE02   PPL16