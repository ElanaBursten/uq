
BSNE  00721 GAUPC 01/11/11 16:32:08 01111-400-091-000 LPEXCA RESTAK
Large Project Excavation Notification
Notice : 01111-400-091 Date: 01/11/11  Time: 16:29  Revision: 000 
Old Tkt: 10180-400-042 Date: 10/18/10  Time: 09:50     

State : GA County: GWINNETT      Place: LILBURN                                 
Addr  : From:        To:        Name:    US RT 29                               
Near  : Name:    PLEASANT HILL                  RD  

Subdivision:                                         
Locate: LOCATE US RT 29/STATE ROUTE 8 FROM LUXOMNI RD TO RONALD REAGAN PKWY  AN
      :  D THEN PLEASANT HILL RD FROM US RT 29/STATE ROUTE 8 TO BURNS RD AND THE
      :  N LESTER RD FROM US RT 29/STATE ROUTE 8 FOR 800FT SOUTH, LUXOMNI FROM U
      :  S RT 29/ST RTE 8 FOR 150 FT SOUTH, VALLEY RD FOR 300FT SOUTH FROM THE I
      :  NTERSECTION OF US RT 29/ST RTE 8 ....PLEASE LOCATE BOTH SIDES OF THE RO
      :  AD ON ALL THE ABOVE STREETS *** PLEASE NOTE: THIS TICKET WAS GENERATED 
      :  FROM THE EDEN SYSTEM. UTILITIES, PLEASE RESPOND TO POSITIVE RESPONSE VI
      :  A HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE CHECK THE
      :  STATUS OF YOUR TICKET VIA THE SAME METHODS. ***                        

Grids       : 3354A8406A 3354B8406A 3354B8406B 3354C8406A 3354C8406B 
Grids       : 3354D8406A 3354D8406B 3355D8406A 3355D8406B 
Work type   : ROAD WIDENING, GRADING, MILLING, PIPES, SIGNS AND BORING            

ScopeOfWork : ROAD WIDENING, GRADING, MILLING, PIPES, SIGNS AND BORING         

Meeting Date      : 08/18/09 Time: 9:00 AM
Meeting Respond By: 08/13/09 Time: 23:59
Meeting Location  : AT THE ER SNELL OFFICE AT 1785 OAK RD SNELLVILLE, GA       
LP Contact        : BOB MANNING               Phone   : 7707229137 Ext:  
Contact Email     : BMANNING@ERSNELL.COM                                       

Start date: 08/26/09 Time: 00:00 Hrs notc : 000
Legal day : 01/16/11 Time: 00:00 Good thru: 04/11/11 Restake by: 04/06/11
RespondBy : 01/13/11 Time: 23:59 Duration : 2 YEARS    Priority: 1
Done for  : GA DOT                                  
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks : PREVIOUS TICKET NUMBER:08119-400-018  PREVIOUS TICKET NUMBER:11169-4
        : 00-016  PREVIOUS TICKET NUMBER:02080-400-023  PREVIOUS TICKET NUMBER
        : :05040-400-039  PREVIOUS TICKET NUMBER:07260-400-034  PREVIOUS TICKE
        : T NUMBER:10180-400-042                                              
        : *** WILL BORE Road, Driveway & Sidewalk     
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : ER SNELL                                  Type: CONT                
Co addr : PO BOX 306                               
City    : SNELLVILLE                      State   : GA Zip: 30078              
Caller  : BOB MANNING                     Phone   :  770-985-0600              
Fax     :                                 Alt. Ph.:  770-985-0600              
Email   : BMANNING@ERSNELL.COM                                                
Contact :                                                           

Submitted date: 01/11/11  Time: 16:29  Oper: 211
Mbrs : AGL114 BSNE COMCEN GAUPC GP266 GWI90 GWI91 JCK70 
-------------------------------------------------------------------------------

