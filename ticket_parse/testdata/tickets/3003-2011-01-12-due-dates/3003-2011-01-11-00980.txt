
PTC01  00359 GAUPC 01/11/11 15:28:04 01111-400-086-000 LPMEET
Large Project Meeting Notification   
Notice : 01111-400-086 Date: 01/11/11  Time: 15:25  Revision: 000 

State : GA County: TREUTLEN      Place: SOPERTON                                
Addr  : From:        To:        Name:    STATE HWY 29                           
Near  : Name:    COUNTY ROAD 152                    

Subdivision:                                         
Locate: LOCATE THE R/O/W ON THE NORTH SIDE OF STATE HWY 29 GOING FOR 8000 FT NO
      :  RTHWEST FROM COUNTY ROAD 152. ALSO LOCATE THE R/O/W ON THE NORTH SIDE O
      :  F COUNTY ROAD 152 FOR 1 MILE GOING EAST FROM STATE HWY 29 TO INCLUDE TH
      :  E ENTIRE PROPERTY AT THE WATER TOWER AND THE R/O/W ON BOTH SIDES OF THE
      :  ROAD                                                                   

Grids       : 3223A8236A 3223A8237C 3223A8237D 3224A8238A 3224A8238B 
Grids       : 3224B8238A 3224B8238B 3224B8238C 3224C8238C 3224C8238D 
Grids       : 3224D8237A 3224D8237B 3224D8237C 3224D8237D 3224D8238D 
Grids       : 
Work type   : INSTL WATER MAIN                                                    
ScopeOfWork : INSTL WATER MAIN FOR OVER 1 MILE                                 

Meeting Date      : 01/14/11 Time: 9:00 AM
Meeting Respond By: 01/13/11 Time: 23:59
Meeting Location  : OFFICE TRAILER AT THE NEW TREUTLEN COUNTY SCHOOL ON HWY 29 
LP Contact        : SCOTT DONALDSON           Phone   : 7066165351 Ext:  
Contact Email     : SCOTT.DONALDSON@TAYLORANDSONS.ORG                          

Start date: 01/27/11 Time: 00:00 Hrs notc : 000
Legal day : 01/20/11 Time: 00:00 Good thru: 04/11/11 Restake by: 04/06/11
RespondBy : 01/19/11 Time: 23:59 Duration : 45 DAYS    Priority: 1
Done for  : CITY OF SOPERTON                        
Crew on Site: N White-lined: N Blasting: Y  Boring: N

Remarks :  -- WILL BLAST                                                      
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : TAYLOR AND SONS                           Type: CONT                
Co addr : 1821 NEW BUCKEYE RD                      
City    : EAST DUBLIN                     State   : GA Zip: 31027              
Caller  : SCOTT DONALDSON                 Phone   :  478-272-4558              
Fax     :                                 Alt. Ph.:  706-616-5351              
Email   : SCOTT.DONALDSON@TAYLORANDSONS.ORG                                   
Contact :                                                           

Submitted date: 01/11/11  Time: 15:25  Oper: 198
Mbrs : AGL126 ATM70 GAUPC GP560 PTC01 
-------------------------------------------------------------------------------

