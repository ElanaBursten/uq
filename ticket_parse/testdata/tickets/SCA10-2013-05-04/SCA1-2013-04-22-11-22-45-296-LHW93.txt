
INLEMPUTA 00003B USAS 04/22/13 08:22:20 A31060399-01B NORM UPDT GRID

Ticket : A31060399  Date: 04/22/13 Time: 08:22 Oper: WEBU Chan: WEB
Old Tkt: A31060399  Date: 04/16/13 Time: 10:01 Oper: CJD Revision: 01B

Company: CASS CONSTRUCTION INC          Caller: MIKE
Co Addr: 1100 WAGNER DR
City&St: EL CAJON, CA                   Zip: 92020      Fax: 619-590-1202
Phone: 619-590-0929 Ext:      Call back: ANYTIME
Formn: MIKE                 Phone: 909-208-5151
Email: APARRAVANO@CASSCONSTRUCTION.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:N TURNER AVE
X/ST 1 : E INLAND EMPIRE BLVD
MPM 1:             MPM 2:
Locat: MARK PROPERTY LINE TO PROPERTY LINE ON N TURNER AVE (MOST EAST) STARTING
     : APPROX 60FT N/OF E INLAND EMPIRE BLVD CONT N/FOR APPROX 40FT;

Excav Enters Into St/Sidewalk: Y

Grids: 0603A064     0603A072
Lat/Long  : 34.070461/-117.585091 34.070720/-117.584236
          : 34.069707/-117.584862 34.069965/-117.584007
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL STROM DRAIN
Wkend: N  Night: N
Work date: 04/22/13 Time: 08:22 Hrs notc: 000 Work hrs: 001 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : BROOKFIELD HOMES

Tkt Exp: 05/20/13

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER ANGIE PARRAVANO--[WEBU 04/22/13 08:22]

Mbrs : ATTDSOUTH     CUC01  INLEMPUTA     ONTRAF ONTWTRSWR     SCG1OD TWTIE
UACRIV UROCMGA       USCE34 UTWCONT       UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org