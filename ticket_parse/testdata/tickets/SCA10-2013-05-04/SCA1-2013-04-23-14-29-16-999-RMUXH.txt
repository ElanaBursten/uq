
INLEMPUTA 00010A USAS 04/23/13 11:28:51 A31130600-00A NORM NEW GRID

Ticket : A31130600  Date: 04/23/13 Time: 11:24 Oper: ADS Chan: 100
Old Tkt: A31130600  Date: 04/23/13 Time: 11:28 Oper: ADS Revision: 00A

Company: C/OF RANCHO CUCAMONGA - STREET DEPT Caller: WILLIE GLENN
Co Addr: 9153 9TH ST
City&St: RANCHO CUCAMONGA, CA           Zip: 91730      Fax: 909-477-2732
Phone: 909-477-2730 Ext:      Call back: 6AM-4PM (M-THU)
Formn: WILLIE GLENN         Phone: 909-472-5950
Email: WILLIE.GLENN@CITYOFRC.US

State: CA County: SAN BERNARDINO  Place: RANCHO CUCAMONGA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:MARINE AVE
X/ST 1 : HUMBOLDT AVE
MPM 1:             MPM 2:
Locat: S/SIDE INTER/OF MARINE AVE & HUMBOLDT AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0603B033
Lat/Long  : 34.092829/-117.578262 34.092369/-117.577470
          : 34.092148/-117.578658 34.091688/-117.577866
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL SIGN POST
Wkend: N  Night: N
Work date: 04/25/13 Time: 12:00 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF RANCHO CUCAMONGA - STREET DEPT

Tkt Exp: 05/21/13

Mbrs : CUC01  INLEMPUTA     MWD04  SCG1OD UCHRTCM       UROCMGA       USCE34
USCE83RIV     UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org