
INLEMPUTA 00006A USAS 04/24/13 09:38:09 A31140374-00A NORM NEW GRID

Ticket : A31140374  Date: 04/24/13 Time: 09:24 Oper: CLA Chan: 100
Old Tkt: A31140374  Date: 04/24/13 Time: 09:38 Oper: CLA Revision: 00A

Company: STEINY AND COMPANY INC         Caller: DAN KIRBY
Co Addr: 12907 E GARVEY AVE
City&St: BALDWIN PARK, CA               Zip: 91706      Fax: 626-338-9607
Phone: 626-926-4184 Ext: CELL Call back: 7AM-4PM
Formn: DAN KIRBY            Phone: 626-338-9923 Ext: OFC
Email: DKIRBY@STEINYCO.COM

State: CA County: SAN BERNARDINO  Place: CHINO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:EUCALYPTUS AVE
X/ST 1 : CHAFFEY ST
MPM 1:             MPM 2:
Locat: IN SIDEWALK MARK ALL 4 CORS/OF INTER/OF EUCALYPTUS AVE & CHAFFEY ST;
     : CHAFFEY ST IS A NEW STREET,  NOT IN MAP LOC APPROXIMATELY 1000FT W/OF
     : MOUNTAIN AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0681J042     0682A041
Lat/Long  : 33.990855/-117.670890 33.990394/-117.670099
          : 33.990174/-117.671286 33.989713/-117.670495
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL TRAFFIC SIGNALS
Wkend: N  Night: N
Work date: 04/29/13 Time: 07:00 Hrs notc: 117 Work hrs: 069 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : LENNAR HOMES

Tkt Exp: 05/22/13

Mbrs : CIW    INLEMPUTA     SCG1OD UTWCCHNO      UVZPOM




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org