
INLEMPUTA 00005A USAS 04/22/13 13:37:07 A31121020-00A NORM NEW GRID

Ticket : A31121020  Date: 04/22/13 Time: 13:34 Oper: KRI Chan: 200
Old Tkt: A31121020  Date: 04/22/13 Time: 13:36 Oper: KRI Revision: 00A

Company: UPW01 - C/OF UPLAND WATER DEPT Caller: TONY
Co Addr: 1370 N BENSON AVE
City&St: UPLAND, CA                     Zip: 91785      Fax: 909-931-4274
Phone: 909-291-2950 Ext:      Call back: M-F ANYTIME
Formn: TONY
Email: TTREJO@CI.UPLAND.CA.US

State: CA County: SAN BERNARDINO  Place: UPLAND
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:E 14TH ST
X/ST 1 : N CAMPUS AVE
MPM 1:             MPM 2:
Locat: BOTH SIDES OF E 14TH ST 40FT W/OF N CAMPUS AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0602C012
Lat/Long  : 34.114875/-117.641907 34.114414/-117.641114
          : 34.114193/-117.642303 34.113733/-117.641510
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL WTR LINE
Wkend: N  Night: N
Work date: 04/24/13 Time: 13:34 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : UPW01 - C/OF UPLAND WATER DEPT

Tkt Exp: 05/20/13

Mbrs : INLEMPUTA     SANANTDIST    SCG1OD UPW01  USCE34 UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org