
INLEMPUTA 00006B USAS 04/22/13 08:28:36 A30850501-01B NORM UPDT GRID

Ticket : A30850501  Date: 04/22/13 Time: 08:28 Oper: WEBU Chan: WEB
Old Tkt: A30850501  Date: 03/26/13 Time: 10:01 Oper: EVA Revision: 01B

Company: CASS CONSTRUCTION              Caller: MIKE KELLERMAN
Co Addr: PO BOX 309
City&St: EL CAJON, CA                   Zip: 92022
Phone: 909-208-5151 Ext:      Call back: ANYTIME
Formn: MIKE
Email: APARRAVANO@CASSCONTRUCTION.COM

State: CA County: SAN BERNARDINO  Place: CHINO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 8545        Street:PINE AVE
X/ST 1 : E PRESERVE LOOP
MPM 1:             MPM 2:
Locat: WORKING ON PROPERTY IN FRONT OF THE FARM HOUSE  AT THE ADDRESS

Excav Enters Into St/Sidewalk: N

Grids: 0712F0124    0712F0224    0712G0113    0712G0213
Lat/Long  : 33.959630/-117.623466 33.959559/-117.617149
          : 33.950777/-117.623565 33.950706/-117.617248
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : DRY UTILITIES INSTALLATION
Wkend: N  Night: N
Work date: 04/22/13 Time: 08:28 Hrs notc: 000 Work hrs: 001 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : WESTERN NATIONAL CONSTRUCTORS

Tkt Exp: 05/20/13

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER ANGIE PARRAVANO--[WEBU 04/22/13 08:28]

Mbrs : INLEMPUTA     SAW01  SCG1OD USCE34 UVZPOM WMW01




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org