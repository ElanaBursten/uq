
INLEMPUTA 00004A USAS 04/24/13 08:27:59 A31140200-00A NORM NEW GRID

Ticket : A31140200  Date: 04/24/13 Time: 08:24 Oper: EVA Chan: 100
Old Tkt: A31140200  Date: 04/24/13 Time: 08:27 Oper: EVA Revision: 00A

Company: CALIFORNIA BORING, INC         Caller: JEREMY
Co Addr: 3030 E CORONADO ST
City&St: ANAHEIM, CA                    Zip: 92806      Fax: 714-666-8975
Phone: 714-632-1596 Ext:      Call back: ANYTIME
Formn: NICK                 Phone: 714-872-3869

State: CA County: SAN BERNARDINO  Place: CHINO HILLS
Delineated: Y
Delineated Method: WHITEPAINT
Address: 14250       Street:PEYTON DR
X/ST 1 : ENGLISH RD
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0681B0324    0681B0424
Lat/Long  : 33.997394/-117.733150 33.997408/-117.732159
          : 33.989426/-117.733036 33.989440/-117.732045
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : DRILLING TO PLACE CONDUIT FOR ELEC
Wkend: N  Night: N
Work date: 04/29/13 Time: 07:00 Hrs notc: 118 Work hrs: 070 Priority: 2
Instruct : MARK BY                        Permit: UNKNOWN
Done for : M B I EXCAVATING

Tkt Exp: 05/22/13

Mbrs : CHIHLS INLEMPUTA     NEXTGLAVEN    PACPIPK       SCE13  SCG1OD UACRIV
USCE34 UTWCCHNO      UVZPOM




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org