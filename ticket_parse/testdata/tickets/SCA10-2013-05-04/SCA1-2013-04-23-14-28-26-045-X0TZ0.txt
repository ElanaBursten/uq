
INLEMPUTA 00009A USAS 04/23/13 11:28:04 A31130598-00A RUSH NEW GRID

Ticket : A31130598  Date: 04/23/13 Time: 11:23 Oper: LYD Chan: 100
Old Tkt: A31130598  Date: 04/23/13 Time: 11:27 Oper: LYD Revision: 00A

Company: INTERNATIONAL LINE BUILDERS    Caller: MEMO
Co Addr: 2520 RUBIDOUX BLVD, PMB 3039
City&St: RIVERSIDE, CA                  Zip: 92519
Phone: 951-329-8051 Ext:      Call back: 7-5
Formn: ZEFERENO             Phone: 951-295-9318

State: CA County: SAN BERNARDINO  Place: CHINO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 11836       Street:BUTTERFIELD AVE
X/ST 1 : TWAIN ST
MPM 1:             MPM 2:
Locat: IN FRONT OF ADDRESS , CREW IS ON SITE

Excav Enters Into St/Sidewalk: Y

Grids: 0641F044
Lat/Long  : 34.038709/-117.698084 34.038758/-117.696712
          : 34.037733/-117.698049 34.037783/-117.696677
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : EMERGENCY REPAIR DAMAGED ELEC CABLE
Wkend: N  Night: Y
Work date: 04/23/13 Time: 11:24 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : NOW                            Permit: BLANKET
Done for : SC EDISON

Tkt Exp: 05/21/13

Mbrs : CHI01  INLEMPUTA     SCG1OD USCE34 UTWCCHNO      UVZPOM




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org