
New Jersey One Call System        SEQUENCE NUMBER 0174    CDC = SJG 

Transmit:  Date: 02/22/12   At: 14:08 

*** U P D A T E           *** Request No.: 120531277  Of Request No.: 120461724 
                          *** No Response Of Request No.:  120461724 

Operators Notified: 
AE2     = ATLANTIC CITY ELECTRIC CO     BAN     = VERIZON                        
SCV     = COMCAST CABLE (VINELAND)      SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    02/22/12   At 07:00  Expiration Date: 04/20/12 

Location Information: 
County: CAMDEN                 Municipality: WATERFORD 
Subdivision/Community:  
Street:               1212 - 1214 BROOKFIELD LANE 
Nearest Intersection: CONDO AVE 
Other Intersection:    
Lat/Lon: 
Type of Work: BURY CABLE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  CONSECUTIVE EVEN 
  CALLER STATES ATLANTIC CITY ELECTRIC DID NOT RESPOND 
  Working For Contact:  BILL ROTH 

Working For: VERIZON FIOS 
Address:     183 BROAD ST 
City:        RED BANK, NJ  07701 
Phone:       732-530-6760   Ext:  

Excavator Information: 
Caller:      BRANDY BLAZER 
Phone:       609-548-0514   Ext:  

Excavator:   J FLETCHER CREAMER & SON INC 
Address:     PO BOX 617 
City:        HAMMONTON, NJ  08037 
Phone:       609-548-0514   Ext:          Fax:  609-591-6507 
Cellular:     
Email:       jwethman@jfcson.com 
End Request 
