
New Jersey One Call System        SEQUENCE NUMBER 0002    CDC = CAM 

Transmit:  Date: 02/23/12   At: 08:17 

*** U P D A T E           *** Request No.: 120540144  Of Request No.: 120470650 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
GPS     = JERSEY CENTRAL POWER & LI     P31     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    02/23/12   At 08:00  Expiration Date: 04/23/12 

Location Information: 
County: MONMOUTH               Municipality: UPPER FREEHOLD 
Subdivision/Community: CREAM RIDGE 
Street:               30 LONG ACRE DR 
Nearest Intersection: EMLEY'S HILL ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: SOIL BORINGS 
Block: 31             Lot:  25            Depth: 12FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  CALLER STATES ELECTRIC COMPANY MISMARKED 
  Working For Contact:  DAVID ZARISH SR 

Working For: HOMEOWNER 
Address:     30 LONG ACRE DR 
City:        CREAM RIDGE, NJ  08514 
Phone:       609-577-4765   Ext:  

Excavator Information: 
Caller:      BRETT CARRACINO 
Phone:       800-677-2487   Ext:  

Excavator:   STATEWIDE ENVIRONMENTAL SERVIC 
Address:     PO BOX 52 
City:        RARITAN, NJ  08869 
Phone:       800-677-2487   Ext:          Fax:  908-636-2278 
Cellular:    908-938-8035 
Email:       OILTANKSERVICE@AOL.COM 
End Request 
