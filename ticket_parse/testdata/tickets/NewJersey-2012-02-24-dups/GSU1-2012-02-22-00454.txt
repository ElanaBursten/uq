
New Jersey One Call System        SEQUENCE NUMBER 0050    CDC = SJG 

Transmit:  Date: 02/22/12   At: 09:19 

*** U P D A T E           *** Request No.: 120530354  Of Request No.: 120510470 
                          *** Cancellation Of Request No.: 120510470 

Operators Notified: 
AE2     = ATLANTIC CITY ELECTRIC CO     BAN     = VERIZON                        
CNW     = AQUA NEW JERSEY, INC          GLT     = GLOUCESTER TOWNSHIP M.U.A      
GSC     = COMCAST CABLEVISION OF GA     SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    02/25/12   At 07:00  Expiration Date: 04/25/12 

Location Information: 
County: CAMDEN                 Municipality: GLOUCESTER TWP 
Subdivision/Community: ERIAL 
Street:               8 KIM LANE 
Nearest Intersection: WINFIELD ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL SWIMMING POOL 
Block:                Lot:                Depth: 10FT 
Extent of Work: CURB TO ENTIRE PROPERTY.
  CANCEL 02/22/12 09:19 am: JOB CANCEL 
Remarks:  
  Working For Contact:  CHARLES CALDERONE 

Working For: HOMEOWNER 
Address:     8 KIM LN 
City:        ERIAL, NJ  08081 
Phone:       856-783-5732   Ext:  

Excavator Information: 
Caller:      CHARLES CALDERONE 
Phone:       856-783-5732   Ext:  

Excavator:   CHARLES CALDERONE 
Address:     8 KIM LN 
City:        ERIAL, NJ  08081 
Phone:       856-783-5732   Ext:          Fax:  
Cellular:    609-634-7532 
Email:       chalie1976@hotmail.com 
End Request 
