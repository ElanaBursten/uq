
New Jersey One Call System        SEQUENCE NUMBER 0050    CDC = TK8 

Transmit:  Date: 02/22/12   At: 13:44 

*** U P D A T E           *** Request No.: 120531197  Of Request No.: 120481512 
                          *** Cancellation Of Request No.: 120481512 

Operators Notified: 
BAN     = VERIZON                       GPS     = JERSEY CENTRAL POWER & LI      
OBM     = OLD BRIDGE M.U.A.             P29     = PUBLIC SERVICE ELECTRIC &      
TK8     = CABLEVISION OF RARITAN VA      

Start Date/Time:    02/24/12   At 00:01  Expiration Date: 04/24/12 

Location Information: 
County: MIDDLESEX              Municipality: OLD BRIDGE 
Subdivision/Community:  
Street:               0 COMMUNITY CIR 
Nearest Intersection: STARLIGHT CT 
Other Intersection:    
Lat/Lon: 
Type of Work: STUMP GRINDING 
Block:                Lot:                Depth: 4FT 
Extent of Work: 20 AREA(S) MARKED IN WHITE.  WHITE MARKS ARE NOT LABELED.
  WHITE MARKS LOCATED FT N FROM C/L OF INTERSECTION ON N SIDE OF ROAD 2FT
  FROM CURB AND EXTEND(S) 50FT N AND 50FT S.
  CANCEL 02/22/12 01:43 pm: INCORRECT DEPTH AND NOT MARKED IN WHITE 
Remarks:  
  CALLER BETTE POLST 
  Working For Contact:  ROB FOSTER 

Working For: HOMEOWNER 
Address:     168 COMMUNITY CIRCLE 
City:        OLD BRIDGE, NJ  08857 
Phone:       917-886-5532   Ext:  

Excavator Information: 
Caller:      SUE CLAVIN 
Phone:       732-370-2776   Ext:  

Excavator:   CLAVIN TREE SERVICE 
Address:     28 TIOGA 
City:        HOWELL, NJ  07731 
Phone:       732-370-2776   Ext:          Fax:  732-370-0667 
Cellular:     
Email:       slclavin@aol.com 
End Request 
