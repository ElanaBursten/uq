
New Jersey One Call System        SEQUENCE NUMBER 0008    CDC = TCI 

Transmit:  Date: 02/23/12   At: 09:09 

*** U P D A T E           *** Request No.: 120540295  Of Request No.: 120460868 
                          *** No Response Of Request No.:  120460868 

Operators Notified: 
BAN     = VERIZON                       CC5     = COMCAST CABLEVISION OF ME      
CCO     = CLIFTON CITY, CITY OF         FTN     = FIBER TECHNOGIES NETWORKS      
MCI     = MCI                           P30     = PUBLIC SERVICE ELECTRIC &      
PVW     = PASSAIC VALLEY WATER COMM     TCI     = CABLEVISION OF OAKLAND, L      
UW4     = /UWNJ-NE                       

Start Date/Time:    02/22/12   At 07:00  Expiration Date: 04/20/12 

Location Information: 
County: PASSAIC                Municipality: CLIFTON 
Subdivision/Community:  
Street:               0 PEEKAY DR 
Nearest Intersection: RIVER ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL ELECTRIC SERVICE 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 1000FT S.
  CURB TO CURB. 
Remarks:  
  CALLER STATES PSE&G DID NOT RESPOND 
  Working For Contact:  DAVE POLONI 

Working For: IRON HILL CONSRTUCTION 
Address:     22 S COMMERCE WAY 
City:        BETHLEHEM, PA  18017 
Phone:       610-332-0550   Ext:  

Excavator Information: 
Caller:      ANGELA KACSUR 
Phone:       732-914-0372   Ext:  

Excavator:   OCEAN UTILITY CONTRACTING 
Address:     1497 LAKEWOOD ROAD 
City:        TOMS RIVER, NJ  08755 
Phone:       732-914-0372   Ext:          Fax:  732-244-4236 
Cellular:    732-620-3636 
Email:       OCRC@AOL.COM 
End Request 
