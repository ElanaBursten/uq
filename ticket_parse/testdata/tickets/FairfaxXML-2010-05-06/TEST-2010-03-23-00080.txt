
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>TEST05</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-03-23T12:45:39</transmitted>
    <seq_num>4</seq_num>
    <ticket>B008201003</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B008201003</ticket>
    <revision>00B</revision>
    <started>2010-03-23T12:17:27</started>
    <account>1SOF</account>
    <original_ticket>B008201003</original_ticket>
    <original_date>2010-03-23T12:17:27</original_date>
    <original_account>1SOF</original_account>
    <replace_by_date>2010-04-09T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2010-03-26T07:00:00</response_due>
    <expires>2010-04-14T07:00:00</expires>
    <state>VA</state>
    <county>ALEXANDRIA CITY</county>
    <st_from_address>811</st_from_address>
    <st_to_address>811</st_to_address>
    <street>ALBANY AV</street>
    <cross1>KING ST</cross1>
    <work_type>PLANT TREES OR SHRUBS</work_type>
    <done_for>HO/BLOCK/703-548-7419</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>MERRIFIELD GARDEN CENTER</name>
    <address1>2756 GALLOWS RD</address1>
    <city>VIENNA</city>
    <cstate>VA</cstate>
    <zip>22180</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7035606222</phone>
    <caller>JIMMY JAMES</caller>
    <caller_phone>7039279474</caller_phone>
    <email>jjames@mgcmail.com</email>
    <map_reference>5647H10</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.817553</latitude>
        <longitude>-77.077403</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.817707</latitude>
        <longitude>-77.077728</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817894</latitude>
        <longitude>-77.077286</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817399</latitude>
        <longitude>-77.077077</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817213</latitude>
        <longitude>-77.077519</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >ENTIRE PROPERTY</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.817513</latitude>
        <longitude>-77.077148</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817894</latitude>
        <longitude>-77.077286</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817707</latitude>
        <longitude>-77.077728</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817390</latitude>
        <longitude>-77.077576</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.817337</latitude>
        <longitude>-77.077225</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3849D7704B-31</grid>
      <grid>3849D7704B-32</grid>
      <grid>3849D7704B-41</grid>
      <grid>3849D7704B-42</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>ALX901</member>
        <group_code>ALX</group_code>
        <description>CITY OF ALEXANDRIA</description>
      </memberitem>
      <memberitem>
        <member>CMC780</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>FBL410</member>
        <group_code>FBL</group_code>
        <description>FIBERLIGHT LLC</description>
      </memberitem>
      <memberitem>
        <member>TEST05</member>
        <group_code>TES</group_code>
        <description>TEST ACCOUNT - RICK</description>
      </memberitem>
      <memberitem>
        <member>VAW902</member>
        <group_code>VAW</group_code>
        <description>VIRGINIA AMERICAN WATER COMPAN</description>
      </memberitem>
      <memberitem>
        <member>VZN102</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
