
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-06-04T10:14:31</transmitted>
    <seq_num>128</seq_num>
    <ticket>B015500443</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B015500443</ticket>
    <revision>00B</revision>
    <started>2010-06-04T10:12:57</started>
    <account>WDMYERS</account>
    <original_ticket>B015500443</original_ticket>
    <original_date>2010-06-04T10:12:57</original_date>
    <original_account>WDMYERS</original_account>
    <replace_by_date>2010-06-23T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2010-06-09T07:00:00</response_due>
    <expires>2010-06-28T07:00:00</expires>
    <state>VA</state>
    <county>ALEXANDRIA CITY</county>
    <st_from_address>300</st_from_address>
    <st_to_address>300</st_to_address>
    <street>COMMONWEALTH AV</street>
    <cross1>W MAPLE ST</cross1>
    <cross2>W LINDEN ST</cross2>
    <work_type>SIGN - INSTALL</work_type>
    <done_for>MCENEARNEY</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>REALTY SIGN POST CO</name>
    <address1>PO BOX 641</address1>
    <city>MCLEAN</city>
    <cstate>VA</cstate>
    <zip>221010641</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>2022560107</phone>
    <caller>DOUG MYERS</caller>
    <caller_phone>2022560107</caller_phone>
    <email>info@realtysignpost.com</email>
    <contact>DOUG MYERS</contact>
    <contact_phone>2022560107</contact_phone>
    <map_reference>5764A1</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.811044</latitude>
        <longitude>-77.061358</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.811516</latitude>
        <longitude>-77.061634</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.811590</latitude>
        <longitude>-77.061300</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.810567</latitude>
        <longitude>-77.061072</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.810493</latitude>
        <longitude>-77.061406</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >WHOLE PROPERTY OR ONLY PROPERTY SIDES THAT BORDER A STREET OR 5 FT AROUND POST,
USE BEST JUDGEMENT
FLAGS PREFERRED</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.811386</latitude>
        <longitude>-77.061605</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.811544</latitude>
        <longitude>-77.061507</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.811475</latitude>
        <longitude>-77.061275</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.810567</latitude>
        <longitude>-77.061072</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.810633</latitude>
        <longitude>-77.061364</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3848B7703B-11</grid>
      <grid>3848B7703B-21</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>ALX901</member>
        <group_code>ALX</group_code>
        <description>CITY OF ALEXANDRIA</description>
      </memberitem>
      <memberitem>
        <member>ATT392</member>
        <group_code>ATT</group_code>
        <description>AT&T</description>
      </memberitem>
      <memberitem>
        <member>CMC780</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>ETI901</member>
        <group_code>ETI</group_code>
        <description>ELANTIC TELECOM INC</description>
      </memberitem>
      <memberitem>
        <member>MCII81</member>
        <group_code>MCI</group_code>
        <description>MCI</description>
      </memberitem>
      <memberitem>
        <member>VAW902</member>
        <group_code>VAW</group_code>
        <description>VIRGINIA AMERICAN WATER COMPAN</description>
      </memberitem>
      <memberitem>
        <member>VZN102</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
