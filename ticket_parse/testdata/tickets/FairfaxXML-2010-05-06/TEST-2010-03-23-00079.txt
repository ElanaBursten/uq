
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>TEST05</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-03-23T12:45:38</transmitted>
    <seq_num>1</seq_num>
    <ticket>B008200902</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B008200902</ticket>
    <revision>00B</revision>
    <started>2010-03-23T11:41:11</started>
    <account>WRMCREYNOLDS</account>
    <original_ticket>B008200902</original_ticket>
    <original_date>2010-03-23T11:41:11</original_date>
    <original_account>WRMCREYNOLDS</original_account>
    <replace_by_date>2010-04-09T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2010-03-26T07:00:00</response_due>
    <expires>2010-04-14T07:00:00</expires>
    <state>VA</state>
    <county>ALEXANDRIA CITY</county>
    <place>ALEXANDRIA</place>
    <st_from_address>207</st_from_address>
    <st_to_address>207</st_to_address>
    <street>E CLIFFORD AVE</street>
    <cross1>TURNER RD</cross1>
    <work_type>GAS LEAK</work_type>
    <done_for>WASHINGTON GAS CO</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>WASHINGTON GAS</name>
    <address1>6801 INDUSTRIAL RD</address1>
    <city>SPRINGFIELD</city>
    <cstate>VA</cstate>
    <zip>22151</zip>
    <caller_type>UTIL</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7037501000</phone>
    <caller>ROGER MCREYNOLDS</caller>
    <caller_phone>7037507580</caller_phone>
    <email>rogermcreynolds@washgas.com</email>
    <contact>GEORGE BERZANSKY</contact>
    <contact_phone>7037504377</contact_phone>
    <map_reference>5648B7</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.831588</latitude>
        <longitude>-77.055969</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.831779</latitude>
        <longitude>-77.056229</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831823</latitude>
        <longitude>-77.055987</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831509</latitude>
        <longitude>-77.055931</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831465</latitude>
        <longitude>-77.056172</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >FROM THE  SIDE  OF ADDRESS FACING TURNER RD. TO THE STREET, CURB TO CURB, &amp; 50'
IN BOTH DIRECTIONS</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NV17G13</remarks>
    <polygon>
      <coordinate>
        <latitude>38.831779</latitude>
        <longitude>-77.056229</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831524</latitude>
        <longitude>-77.056183</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831509</latitude>
        <longitude>-77.055931</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.831810</latitude>
        <longitude>-77.055992</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3849A7703C-12</grid>
      <grid>3849A7703C-22</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>ALX901</member>
        <group_code>ALX</group_code>
        <description>CITY OF ALEXANDRIA</description>
      </memberitem>
      <memberitem>
        <member>CMC780</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>MCII81</member>
        <group_code>MCI</group_code>
        <description>MCI</description>
      </memberitem>
      <memberitem>
        <member>TEST05</member>
        <group_code>TES</group_code>
        <description>TEST ACCOUNT - RICK</description>
      </memberitem>
      <memberitem>
        <member>VAW902</member>
        <group_code>VAW</group_code>
        <description>VIRGINIA AMERICAN WATER COMPAN</description>
      </memberitem>
      <memberitem>
        <member>VZN102</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
