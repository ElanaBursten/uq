
New Jersey One Call System        SEQUENCE NUMBER 0000    CDC = CTC 

Transmit:  Date:  1/08/10   At: 16:38 

*** R O U T I N E         *** Request No.: 100040101 

Operators Notified: 
ADC     = COMCAST CABLEVISION OF TO     AE1     = ATLANTIC CITY ELECTRIC         
C10     = COMCAST CABLEVISION OF NE     C11     = COMCAST CABLEVISION OF NE      
CAM     = CABLEVISION OF MONMOUTH       CC1     = COMCAST CABLEVISION OF GL      
CC2     = COMCAST CABLEVISION OF BU     CC3     = COMCAST CABLEVISION OF CE      
CC4     = COMCAST CABLEVISION OF NE     CC5     = COMCAST CABLEVISION OF ME      
CC6     = COMCAST CABLEVISION OF NO     CC7     = COMCAST CABLEVISION OF NE      
CC9     = COMCAST CABLEVISION OF ME     CCC     = COMCAST CABLEVISION OF LO      
CCF     = COMCAST CABLE OF MERCER (     CTC     = COMCAST OF SOMERSET            
EG1     = ELIZABETHTOWN GAS COMPANY     GSC     = COMCAST CABLEVISION OF GA      
GSF     = COMCAST CABLEVISION OF GA     SCA     = COMCAST CABLEVISION OF SO      
SCV     = COMCAST CABLE (VINELAND)      SJG     = SOUTH JERSEY GAS COMPANY       
SUJ     = COMCAST CABLE (JAMISON, P     TK1     = CABLEVISION OF HAMILTON        
TK5     = CABLEVISION OF WARWICK        TK6     = COMCAST CABLE COMMUNICATI      
TK8     = CABLEVISION OF RARITAN VA     UNI     = EMBARQ                         
WCC     = TIME WARNER CABLE OF AVAL      

Start Date/Time:    01/14/10   At 00:01  Expiration Date: 03/13/10 

Location Information: 
County: ATLANTIC               Municipality: ATLANTIC CITY 
Subdivision/Community: TESTING 
Street:               1111 TESTING 
Nearest Intersection: TESTING 
Other Intersection:   TESTING 
Lat/Lon: 
Type of Work: TESTING 
Block: TEST           Lot:  TEST          Depth: TEST 
Extent of Work: TEST TICKET ONLY 
Remarks:  
  PLEASE DISREGARD THIS MESSAGE, FOR TESTING ONLY!!!!!! 

Working For: TESTING 
Address:     11111 TESTING 
City:        TESTING, NJ  11111 
Phone:       316-687-2102   Ext:  

Excavator Information: 
Caller:      MARCUS CORBIN 
Phone:       316-687-2102   Ext:  

Excavator:   OCC 
Address:     8100 
City:        WICHITA, KS  67226 
Phone:       316-687-2102   Ext:          Fax:  888-236-3465 
Cellular:    316-687-2102 
Email:       njtesttkts@occinc.com 
End Request 
