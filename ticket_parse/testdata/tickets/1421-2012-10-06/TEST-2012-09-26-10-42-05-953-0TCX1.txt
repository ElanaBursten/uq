
BSZT29 15 SC811 Voice 09/26/2012 10:41:00 AM 1209260026 Remark 

Notice Number:    1209260026        Old Notice:      1209260022        
Sequence:         15                Created By:      RGO               

Created:          09/26/12 10:41 AM                                     
Work Date:        09/26/12 10:41 AM                                     
Update on:                          Good Through:                      

Caller Information:
SC811
810 DUTCH SQUARE BLVD; SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Other             
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:0
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
CHARLEIGH ELEBASH                              Phone:(800) 290-2783 Ext:0
Site Contact Email:  CELEBASH@sc1pups.org                                  
CallBack:  8AM TO 5PM MON-FRI

Excavation Information:
SC    County:  ANDERSON             Place:  BELTON            
Street:        541 S MAIN ST                                         
Intersection:  MILTON DR                                             
Subdivision:   ST JAMES ESTATES                                      

Lat/Long: 34.515247,-82.482977
Second:  34.515247,-82.482977

Explosives:  N Premark:  Y Drilling/Boring:  Y Near Railroad:  Y

Work Type:     SEE INSTRUCTIONS                                        
Work Done By:  SC811                Duration:        2 HOURS           

Instructions:
LOCATE ENTIRE PROPERTY AND BOTH SIDES OF THE STREET FOR POSSIBLE ROAD BORE    

Directions:
FROM HWY 1 TO DEER ROAD, LEFT ON HOWELL STREET, THEN IT'S ON THE RIGH         

Remarks:
**TEST**                                                                      

Member Utilities Notified:
BSZT29 DPCZ04 LAU27* MCI18* PNAZ80 PNGZ81* CCMZ41* 
ARJ34* BHPW74 

