


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Tuesday, March 26, 2013 9:08 AM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13067550

Ticket No: 13067550               2 FULL BUSINESS DAYS      UPDATE 
Send To: NOANT02    Seq No:    1  Map Ref:  
Update Of: 12246893 
Transmit      Date:  3/26/13   Time:  9:07 AM    Op: orruth 
Original Call Date:  3/26/13   Time:  9:05 AM    Op: orruth 
Work to Begin Date:  3/29/13   Time: 12:00 AM 

State: WA            County: ASOTIN                  Place: CLARKSTON 
Address:   2635      Street: VALLEYVIEW DR 
Nearest Intersecting Street: WESTWARD HO DR 

Twp: 11N     Rng: 46E     Sect-Qtr: 29-NW,19-SW-SE,30-NW-SE-NE 
Twp:         Rng:         Sect-Qtr:        

Type of Work: REPLACE ELEC PRIMARY 
Location of Work: EXCAVATION SITE IS ON THE N SIDE OF THE ROAD. 
: ADD IS APX 1000FT NW OF ABV INTER.  FROM TRANS AVE042H6 IN FRONT OF ABV ADD
: MARK NW THEN CONTINUING SE APX 800FT TO ADD 2550 VALLEYVIEW DR, MARKING
: R.O.W. TO R.O.W. ON VALLEYVIEW DR.  AREA MARKED IN WHITE.  CALLER STATES
: WORKSITE IS WITHIN APX 400FT OF HAIRPIN TURN IN THE ROAD IN BOTH DIRECTIONS.
: ++UDPATE FOR REMARKING++ 

Remarks: AREA IS MARKED IN WHITE 
:  

Company     : AVISTA UTILITIES                 Best Time:  7-4 
Contact Name: BILL SPEARS                      Phone: (208)798-1472 
Alt. Contact: SANDY JONES                      Phone: (208)798-1472 
Contact Fax :  
Work Being Done For: AVISTA UTILITIES 
Additional Members:  
ATT02      PUDAS01    QLNWA02    TCI29      WAPOW02 


