


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Monday, March 25, 2013 10:51 AM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13066242

Ticket No: 13066242               2 FULL BUSINESS DAYS 
Send To: NOANT02    Seq No:    3  Map Ref:  

Transmit      Date:  3/25/13   Time: 10:51 AM    Op: orsusa 
Original Call Date:  3/25/13   Time: 10:38 AM    Op: orsusa 
Work to Begin Date:  3/28/13   Time: 12:00 AM 

State: WA            County: GARFIELD                Place: POMEROY 
Address:             Street: 20TH ST 
Nearest Intersecting Street: HWY 12 

Twp: 12N     Rng: 42E     Sect-Qtr: 32-SW 
Twp:         Rng:         Sect-Qtr:        

Type of Work: SHOP CONSTRUCTION
Location of Work: EXCAVATION SITE IS ON THE N SIDE OF THE ROAD. 
: SITE IS GRAIN ELEVATOR SITE LOCATED MIDWAY BETWEEN HWY 12 AND COLUMBIA DR,
: SITE IS ACROSS 20TH FROM ADD 2098 COLUMBIA DR.  MARK 2 AREAS MARKED IN WHITE
: AT THIS LOCATION, 1ST AREA IS APX 60 FT BY 60 FT IN MIDDLE OF GRAIN
: ELEVATORS, 2ND AREA IS APX 30 FT BY 30 FT LOCATED DUE EAST OF 1ST SITE.
: CALLER GAVE 82 DEGREES 18 24 EAST. 

Remarks: BEST INFORMATION AVAILABLE
:  

Company     : POMEROY GRAIN GROWERS            Best Time:   
Contact Name: KENT FLYNN                       Phone: (509)566-7012 
Alt. Contact: OFFICE                           Phone: (509)843-1694 
Contact Fax :  
Work Being Done For: POMEROY GRAIN GROWERS Additional Members:  
ATT02      PPL07      QLNWA12 


