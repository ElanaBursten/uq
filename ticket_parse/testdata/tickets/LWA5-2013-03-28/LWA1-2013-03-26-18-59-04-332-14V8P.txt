


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Tuesday, March 26, 2013 3:39 PM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13068389

Ticket No: 13068389               2 FULL BUSINESS DAYS 
Send To: NOANT01    Seq No:    5  Map Ref:  

Transmit      Date:  3/26/13   Time:  3:38 PM    Op: orcath 
Original Call Date:  3/26/13   Time:  3:35 PM    Op: orcath 
Work to Begin Date:  3/29/13   Time: 12:00 AM 

State: WA            County: STEVENS                 Place: COLVILLE 
Address:             Street: HIGHWAY 20 EAST 
Nearest Intersecting Street: SILKE 

Twp: 35N     Rng: 39E     Sect-Qtr: 10 
Twp:         Rng:         Sect-Qtr:        

Type of Work: INSTALL NEW 6" GAS MAIN 
Location of Work: EXCAVATION SITE IS ON THE S SIDE OF THE ROAD. 
: FROM THIS INTERSECTION EAST FOR APPROXIMATELY 2700FT
: MARK FROM THE INTERSECTION OF SILKE ROAD AND HWY 20 EAST, EAST FOR
: APPROXIMATELY 2700FT TO THE EAST SIDE OF THE INTERSECTION OF HWY 20 EAST AND
: EVERGREEN ROAD 

Remarks: START DATE 4-1-2013                 TICKET 2 OF 2 
: BEST INFORMATION AVAILABLE 

Company     : AVISTA UTILITIES                 Best Time:   
Contact Name: DEBBIE PETERS                    Phone: (509)685-6414 
Alt. Contact: DEBBIE PETERS                    Phone: (509)685-6413 
Contact Fax : (509)684-3029 
Work Being Done For: AVISTA UTILITIES 
Additional Members:  
AVISTA04   COCWSD01   QLNWA27    TCI31      WSDOT14 


