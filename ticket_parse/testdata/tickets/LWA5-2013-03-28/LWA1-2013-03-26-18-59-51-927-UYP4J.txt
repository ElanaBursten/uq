


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Tuesday, March 26, 2013 3:56 PM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13068458

Ticket No: 13068458               2 FULL BUSINESS DAYS 
Send To: NOANT01    Seq No:    6  Map Ref:  

Transmit      Date:  3/26/13   Time:  3:55 PM    Op: orcath 
Original Call Date:  3/26/13   Time:  3:51 PM    Op: orcath 
Work to Begin Date:  3/29/13   Time: 12:00 AM 

State: WA            County: STEVENS                 Place: COLVILLE 
Address:    180      Street: DEGRIEF ROAD 
Nearest Intersecting Street: EVERGREEN ROAD 

Twp: 35N     Rng: 39E     Sect-Qtr: 10-NE 
Twp: 35N     Rng: 39E     Sect-Qtr: 2-SW,3-SW-SE,11-SW-NW,10 

Type of Work: EXCAVATION 
Location of Work: 1/4 MILE NORTH OF INTERSECTION OF DEGRIEF AND EVERGREEN
: NEAR CITY PUMPHOUSE AT STAKED LOCATION AT TRANSFORMER POLE
: LOCATE A 20FT RADIUS
: MAP AVAILABLE FOR PICKUP 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : AVISTA UTILITIES                 Best Time:   
Contact Name: DEBBIE PETERS                    Phone: (509)685-6414 
Alt. Contact: DEBBIE PETERS                    Phone: (509)685-6413 
Contact Fax : (509)684-3029 
Work Being Done For: AVISTA UTILITIES 
Additional Members:  
AVISTA04   COCWSD01   QLNWA27    TCI31 


