
PBTHAN 00373 USAN 02/08/12 12:17:11 0044837 NORMAL NOTICE 

Message Number: 0044837 Received by USAN at 12:15 on 02/08/12 by MAB

Work Begins:    02/10/12 at 12:30   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         DONNA LINDNER            
Company:        UTILITY POLE TECHNOLOGIES          
Address:        708 BLAIR MILL RD                       
City:           WILLOW GROVE                  State: PA Zip: 19090
Business Tel:   559-972-0609                  Fax:                             
Email Address:  RONNYMONTGOMERY76@GMAIL.COM                                 

Nature of Work: HAND DIG FOR POLE INSPECTION            
Done for:       PG&E                          Explosives: N
Foreman:        RONNY MONTGOMERY         
Field Tel:                                    Cell Tel: 559-972-0609           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
AREA BOUNDED BY: CA RT 99, DOCKERY AVE AND E MOUNTAIN VIEW AVE
    (NO STS WITHIN AREA) 

Place: SELMA, CO AREA               County: FRESNO               State: CA

Long/Lat Long: -119.603574 Lat:  36.545892 Long: -119.593082 Lat:  36.553748 


Sent to:
CWSFNO = CALIF WTR SVC SELMA          CTYFNO = CITY FRESNO                  
CTYSEL = CITY SELMA                   COMFNO = COMCAST-FRESNO               
PBTHAN = PACIFIC BELL HANFORD         PGEFNO = PGE DISTR FRESNO             
SKFSAN = SELMA-KINGSBURG-FOWLER C     SCGVIS = SO CAL GAS VISALIA           
VEREXR = VERIZON CAL EXETER           




At&t Ticket Id: 46218   clli code: SELMCA11   OCC Tkt No: 0044837-000