
PBTHAN 00406 USAN 02/08/12 12:55:49 0044947 NORMAL NOTICE 

Message Number: 0044947 Received by USAN at 12:54 on 02/08/12 by JWH

Work Begins:    02/13/12 at 08:00   Notice: 024 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         TIM STEVENS              
Company:        PG&E                               
Address:        4101 WIBLE RD                           
City:           BAKERSFIELD                   State: CA Zip: 93308
Business Tel:   661-342-0787                  Fax: 661-398-5985                
Email Address:  TXSB@PGE.COM                                                

Nature of Work: HAND DIG TO INST BARRIER POST           
Done for:       SAME                          Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 661-342-0787           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         717 AIRPORT DR
  Cross Street:         OLIVE DR

    WRK IN REAR/O ADDR

Place: OILDALE, CO AREA             County: KERN                 State: CA

Long/Lat Long: -119.040392 Lat:  35.412948 Long: -119.039967 Lat:  35.413424 


Sent to:
BHNBFD = BRIGHT HOUSE NETWORKS        NRMWD  = N/O RIVER MUNI WATER         
NRISAN = NORTH OF RIVER SANI#1        OILWTR = OILDALE MUTUAL WTR           
PBTHAN = PACIFIC BELL HANFORD         PGEBFD = PGE DISTR BAKERSFIELD        
EQUBFD = SHELL PIPELINE CO, LP -      SCGBFD = SO CAL GAS BAKERSFIELD       




At&t Ticket Id: 46337   clli code: OLDLCA11   OCC Tkt No: 0044947-000