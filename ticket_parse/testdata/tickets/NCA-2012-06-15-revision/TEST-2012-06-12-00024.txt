
PBTCHI 00005 USAN 02/29/12 07:02:45 0009610 NORMAL NOTICE EXTENSION

Message Number: 0009610 Received by USAN at 07:02 on 02/29/12 by INTERNET

Work Begins:    01/12/12 at 09:45   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 04/03/12 at 23:59   Update By: 03/29/12 at 16:59

Caller:         STEVE BLACKSTOCK         
Company:        PACIFIC EXCAVATION                 
Address:        9796 KENT ST                            
City:           ELK GROVE                     State: CA Zip: 95624
Business Tel:   916-686-2800                  Fax: 916-686-2806                
Email Address:  SBLACKSTOCK@PACIFICEXCAVATION.COM                           

Nature of Work: DIRECTIONAL BORING, TR, AUGER ST LIGHT B
Done for:       CALTRANS                      Explosives: N
Foreman:        MARK TUDESKO             
Field Tel:                                    Cell Tel: 916-997-0707           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 

    B/SI/O E. 8TH ST FR INT/O THE S/BOUND CA RT 99 ON / OFF RAMPS (LOC
    200'W/O CA RT 99) GO 500'E (INCL ENT INT/O E. 8TH ST & THE N/BOUND
    CA RT 99 ON / OFF RAMPS) 

Place: CHICO                        County: BUTTE                State: CA

Long/Lat Long: -121.82211  Lat:  39.734523 Long: -121.817096 Lat:  39.738394 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:

#1 EXTEND TO 03/06/2012 RE-MARK NO ORIG DATE 01/10/2012-ISMB
02/01/2012
#2 EXTEND TO 04/03/2012 RE-MARK NO ORIG DATE 01/10/2012-ISMB
02/29/2012

Sent to:
CWSCHI = CALIF WTR SVC-CHICO          COMCHI = COMCAST - CHICO              
PBTCHI = PACIFIC BELL CHICO           PGECHI = PGE DISTR CHICO              




At&t Ticket Id: 41216   clli code: CHICCA01   OCC Tkt No: 0009610-002