
PBTMDO 00019 USAN 02/09/12 08:00:05 0045933 NORMAL NOTICE 

Message Number: 0045933 Received by USAN at 07:59 on 02/09/12 by KRS

Work Begins:    02/15/12 at 07:00   Notice: 029 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/08/12 at 23:59   Update By: 03/06/12 at 16:59

Caller:         RICK WILSON              
Company:        OVERAA CONSTRUCTION                
Address:        200 PARR BLVD                           
City:           RICHMOND                      State: CA Zip: 94801
Business Tel:   510-812-0794                  Fax:                             
Email Address:  RICKW@OVERAA.COM                                            

Nature of Work: EXC TO INST UNG UTILS                   
Done for:       CITY OF LODI                  Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 510-812-0794           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         144 S ROSE ST
       Between:         W OAK ST
         And:           W WALNUT ST

    WRK INSIDE THE FENCE IN FRT/O ADDR EXT APP 40' INTO PROP 

Place: LODI                         County: SAN JOAQUIN          State: CA

Long/Lat Long: -121.282722 Lat:  38.131331 Long: -121.280809 Lat:  38.133217 


Sent to:
CTYLOD = CITY LODI                    COMSTK = COMCAST-STOCKTON             
PBTMDO = PACIFIC BELL MODESTO         PGESTK = PGE DISTR STOCKTON           




At&t Ticket Id: 43757   clli code: LODICA01   OCC Tkt No: 0045933-000