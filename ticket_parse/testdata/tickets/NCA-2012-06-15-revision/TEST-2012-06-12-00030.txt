
PBTHAY 00022 USAN 02/29/12 08:47:47 0069936 NORMAL NOTICE RENEWAL

Message Number: 0069936 Received by USAN at 08:45 on 02/29/12 by JWH

Work Begins:    03/02/12 at 09:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/28/12 at 23:59   Update By: 03/26/12 at 16:59

Caller:         DANIEL TORRES            
Company:        STEINY & COMPANY                   
Address:        27 SHERIDAN ST                          
City:           VALLEJO                       State: CA Zip: 94590
Business Tel:   707-310-5176                  Fax:                             
Email Address:                                                              

Nature of Work: HORIZONTAL BORING TO INSTAL ELEC PIPE   
Done for:       CITY BERKELEY                 Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 707-310-5176           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: PENDING                  
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    AT A POINT 1000FT W/O INT POTTER ST AND BAY ST
    CONT 400' S ON THE E/SI/O OF THE "ON RAMP" (WRK IS LOC UNDER THE ON
    RAMP PER CALLER) 

Place: BERKELEY                     County: ALAMEDA              State: CA

Long/Lat Long: -122.302766 Lat:  37.846311 Long: -122.29424  Lat:  37.853072 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
RENEWAL OF TICKET RN#431875 ORIG DATE 12/29/2011 RE-MARK NO-JWH
02/29/2012

Sent to:
ATTRN2 = AT&T BROADBAND RING NETW     CTYBER = CITY BERKELEY                
CTYEME = CITY EMERYVILLE ENGR         CTYOAK = CITY OAKLAND CONST DEPT      
COMOAK = COMCAST-OAKLAND              COMRCH = COMCAST-RICHMOND             
EBWWST = EAST BAY MUNICIPAL UTILI     EBWCMS = EAST BAY WATER               
KAISER = KAISER FNDA HEALTH           KMERCH = KINDER MORGAN / SFPP-RCH     
LEVCAL = LEVEL 3 COMM - CALIF         MCIWSA = MCI WORLDCOM                 
PBTHAY = PACIFIC BELL HAYWARD         PGEOAK = PGE DISTR OAKLAND            
PGERCH = PGE DISTR RICHMOND           SPTTEL = QWEST COMM (CA)              
TCOSFO = TELEPORT COMM SFO            XOCOM2 = XO COMM SVCS DBA XO COMM     
XOCOMM = XO COMM SVCS DBA XO COMM     




At&t Ticket Id: 42680   clli code: BKLYCA01   OCC Tkt No: 0069936-000