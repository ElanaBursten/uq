
PBTCHI 00024 USAN 02/08/12 12:10:13 0394224 NORMAL NOTICE EXTENSION

Message Number: 0394224 Received by USAN at 12:09 on 02/08/12 by ADM

Work Begins:    11/23/11 at 17:00   Notice: 020 hrs      Priority: 2
Night Work: Y    Weekend Work: Y

Expires: 03/12/12 at 23:59   Update By: 03/08/12 at 16:59

Caller:         SCOTT BOND               
Company:        TULLIS INC                         
Address:        POB 493416, REDDING                     
City:           REDDING                       State: CA Zip: 96049
Business Tel:   530-241-5105                  Fax: 530-241-5570                
Email Address:                                                              

Nature of Work: HORIZONTAL BORING FOR STORM DRAIN       
Done for:       CALTRANS                      Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 530-227-7776           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 

    AT A POINT 0.5MI N/O INT I 5 AND S BONNYVIEW RD ON ALL/O I 5
    GO 1.5 MI N (EXT 125' E & W FR CTR/O I-5 CORRADOR FOR ENT DIST)

Place: REDDING                      County: SHASTA               State: CA

Long/Lat Long: -122.362573 Lat:  40.541763 Long: -122.350392 Lat:  40.570497 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
RENEWAL OF TICKET RN#182920 ORIG DATE 06/09/2011 RE-MARK YES-JWH
11/21/2011 - ALL MEMBERS PER CHERYL
#1 EXTEND TO 01/16/2012 RE-MARK YES ORIG DATE 11/21/2011-MJG
12/15/2011 - ALL MEMBERS RE-MARKING to be completed by 12/19/2011
10:15:00 AM - PER CHERYL
#2 EXTEND TO 02/13/2012 RE-MARK NO ORIG DATE 11/21/2011-JDR 01/10/2012
- PER CHERYL
#3 EXTEND TO 03/12/2012 RE-MARK NO ORIG DATE 11/21/2011-ADM 02/08/2012


Sent to:
ANDIRR = ANDERSON-COTTONWOOD IRR      CTYRE2 = CITY REDDING ELEC DEPT       
CTYRED = CITY REDDING PUB WRKS        FALRED = FALCON CTV REDDING           
PBTCHI = PACIFIC BELL CHICO           PGERED = PGE DISTR REDDING            
REDFLX = REDFLEX TRAFFIC SYSTEMS      




At&t Ticket Id: 46291   clli code: RDNGCA11   OCC Tkt No: 0394224-003