
PBTWAL 00130 USAN 02/14/12 14:09:09 0052126 SHORT NOTICE 

Message Number: 0052126 Received by USAN at 14:05 on 02/14/12 by PGS

Work Begins:    02/16/12 at 08:00   Notice: 013 hrs      Priority: 1
Night Work: N    Weekend Work: N

Expires: 03/13/12 at 23:59   Update By: 03/09/12 at 16:59

Caller:         DINA REED                
Company:        ADAM MORENO & SONS INC.            
Address:        6460 TRI LN                             
City:           RICHMOND                      State: CA Zip: 94803
Business Tel:   510-222-2873                  Fax: 510-223-9486                
Email Address:  MORENOBACKHOES@COMCAST.NET                                  

Nature of Work: EXC TO REP WTR SVC                      
Done for:       WEST VALLEY CONST             Explosives: N
Foreman:        NATE                     
Field Tel:                                    Cell Tel: 408-640-8308           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Street Address:         6533 KING WAY
  Cross Street:         TORY WAY

    ST & SI WLK IN FRT/O ADDR 

Place: DUBLIN                       County: ALAMEDA              State: CA

Long/Lat Long: -121.917259 Lat:  37.712612 Long: -121.916747 Lat:  37.713069 


Sent to:
ACOZO2 = ALAMEDA CO FC&WCD ZN 7#2     ACOZON = ALAMEDA CO FC&WCD ZONE 7     
CTYDUB = CITY DUBLIN                  COMLIV = COMCAST-LIVERMORE            
COALAM = COUNTY ALAMEDA               DERWA  = DERWA                        
DUBSVC = DUBLIN-S/RAMON SVCS DIST     PBTSJ2 = PACIFIC BELL SAN JOSE 2      
PBTWAL = PACIFIC BELL WALNUT CRK      PGEHAY = PGE DISTR HAYWARD            




At&t Ticket Id: 62030   clli code: SNRMCA11   OCC Tkt No: 0052126-000