
PBTMDO 00020 USAN 02/29/12 07:22:35 0383920 NORMAL NOTICE EXTENSION

Message Number: 0383920 Received by USAN at 07:21 on 02/29/12 by EDS

Work Begins:    11/16/11 at 07:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/30/12 at 23:59   Update By: 03/27/12 at 16:59

Caller:         TINA MAYHEW              
Company:        KNIFE RIVER CONSTRUCTION           
Address:        655 W.CLAY ST                           
City:           STOCKTON                      State: CA Zip: 95206
Business Tel:   209-948-0302                  Fax: 209-948-1597                
Email Address:  TINA.MAYHEW@KNIFERIVER.COM                                  

Nature of Work: EXC TO INST IRRIG L                     
Done for:       SSJID                         Explosives: N
Foreman:        DREW BORIOLO             
Field Tel:                                    Cell Tel: 209-649-7871           
Area Premarked: Y   Premark Method: WHITE PAINT, STAKES                        
Permit Type:    STATE                         Number: 2011900715               
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 

    N/SI E. MELTON RD 1900' E/O S. MANTECA RD GO S 300'  (XING MELTON RD)
    INTO FLD (INCL  A 100' WIDE PATH) 

Place: MANTECA, CO AREA             County: SAN JOAQUIN          State: CA

Long/Lat Long: -121.210682 Lat:  37.718711 Long: -121.205962 Lat:  37.722377 

Excavator Requests Operator(s) To Re-mark Their Facilities: Y
Operator(s) To Re-mark Their Facilities:
COMSTK = COMCAST-STOCKTON             PBTMDO = PACIFIC BELL MODESTO         
PGESTK = PGE DISTR STOCKTON           SSJIRR = SOUTH SAN JOAQUIN I D        
VERCAL = VERIZON (CA)                 

Comments:
RENEWAL OF TICKET RN#345167 ORIG DATE 10/12/2011 RE-MARK YES-MAB
11/11/2011 - ALL MEMBERS
#1 EXTEND TO 01/06/2012 RE-MARK NO ORIG DATE 11/11/2011-CAP 12/07/2011
PER KEVIN
#2 EXTEND TO 02/03/2012 RE-MARK YES ORIG DATE 11/11/2011-BMC
01/04/2012 - ALL MEMBERS RE-MARKING to be completed by 01/06/2012
08:15:00 AM
#3 EXTEND TO 03/02/2012 RE-MARK YES ORIG DATE 11/11/2011-TLF
02/01/2012 - ALL MEMBERS RE-MARKING to be completed by 02/03/2012
03:00:00 PM PER TINA
#4 EXTEND TO 03/30/2012 RE-MARK YES ORIG DATE 11/11/2011-EDS
02/29/2012 - ALL MEMBERS RE-MARKING to be completed by 03/02/2012
07:30:00 AM

Sent to:
COMSTK = COMCAST-STOCKTON             PBTMDO = PACIFIC BELL MODESTO         
PGESTK = PGE DISTR STOCKTON           SSJIRR = SOUTH SAN JOAQUIN I D        
VERCAL = VERIZON (CA)                 




At&t Ticket Id: 41048   clli code: TRACCA11   OCC Tkt No: 0383920-004