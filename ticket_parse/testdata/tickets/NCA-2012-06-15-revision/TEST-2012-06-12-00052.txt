
PBTHAN 00081 USAN 02/29/12 09:12:53 0069980 NORMAL NOTICE 

Message Number: 0069980 Received by USAN at 09:10 on 02/29/12 by KAR

Work Begins:    03/02/12 at 09:30   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/28/12 at 23:59   Update By: 03/26/12 at 16:59

Caller:         IVAN CLAVEL              
Company:        J.R. PIERCE PLUMBING COMPANY INC.  
Address:        3610 CINCINNATI AVE, ROCKLIN            
City:           ROCKLIN                       State: CA Zip: 95765
Business Tel:   916-434-9554 Ext: 104         Fax: 916-434-9092                
Email Address:  IVANC@JRPPLUMBING.COM                                       

Nature of Work: DIG TO INST SWR & WTR                   
Done for:       KB HOMES                      Explosives: N
Foreman:        SHANE B.                 
Field Tel:                                    Cell Tel: 559-647-7652           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         COCONUT ST
  Cross Street:         HACIENDA RD

    FRT & B/SI/O POSTED LOT/ADDR #296/1326  EXT APP 35'  INTO PROP L -
    - ST NAME COCONUT ST IS LOC ON THE S END/O HACIENDA RD WHEN IT TURNS
    W TOWARDS PERSIMMON ST PER CALLER

Place: MADERA, CO AREA              County: MADERA               State: CA

Long/Lat Long: -120.05281  Lat:  36.933661 Long: -120.015431 Lat:  36.953727 


Sent to:
ATTCAL = AT&T TRANSMISSION CAL        CTYMAD = CITY MADERA                  
COMFNO = COMCAST-FRESNO               KMEFNO = KINDER MORGAN / SFPP-FNO     
MADIRR = MADERA IRR DIST              PBTHAN = PACIFIC BELL HANFORD         
PARKSD = PARKSDALE, SA-3              PARKWD = PARKWOOD, MD-19              
PGEMER = PGE DISTR MERCED             SPTTEL = QWEST COMM (CA)              
SEBAST = SEBASTIAN                    SPRINT = SPRINT                       
TWLNGH = TW TELECOM - LONG HAUL       




At&t Ticket Id: 42211   clli code: MADRCA11   OCC Tkt No: 0069980-000