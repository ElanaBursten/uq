
PBTSAC 00024 USAN 02/29/12 07:29:36 0069770 NORMAL NOTICE 

Message Number: 0069770 Received by USAN at 07:22 on 02/29/12 by INTERNET

Work Begins:    03/02/12 at 07:30   Notice: 020 hrs      Priority: 2
Night Work: Y    Weekend Work: Y

Expires: 03/28/12 at 23:59   Update By: 03/26/12 at 16:59

Caller:         JERRY CARAANG            
Company:        O.C. COMMUNICATIONS                
Address:        2204 KAUSEN DR                          
City:           ELK GROVE                     State: CA Zip: 95758
Business Tel:   916-564-3606                  Fax: 916-564-3606                
Email Address:                                                              

Nature of Work: TRENCH TO INSTALL TV CABLE              
Done for:       COMCAST                       Explosives: N
Foreman:        ANTONIO ANDUJO           
Field Tel:                                    Cell Tel: 916-564-3606           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         1 SEA LION CT
  Cross Street:         POCKET RD

    FRONT OF PROPERTY 100 FEET IN

Place: SACRAMENTO                   County: SACRAMENTO           State: CA

Long/Lat Long: -121.546152 Lat:  38.483918 Long: -121.544162 Lat:  38.485307 


Sent to:
CTYSA3 = CITY SACRAMENTO DEPT/O T     CTYSA2 = CITY SACRAMENTO DEPT/O T     
CTYSA4 = CITY SACRAMENTO UTIL DEP     CTYSAC = CITY SACRAMENTO UTIL DEP     
COMSAC = COMCAST-SACRAMENTO           PBTSAC = PACIFIC BELL SACRAMENTO      
PGESAC = PGE DISTR SACRAMENTO         RIVELC = RIVERLAKE COM ASSOC          
SMUDSO = SMUD                         




At&t Ticket Id: 41018   clli code: SCRMCA03   OCC Tkt No: 0069770-000