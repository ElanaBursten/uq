
PBTSRO 00029 USAN 02/08/12 13:09:29 0044978 NORMAL NOTICE 

Message Number: 0044978 Received by USAN at 13:05 on 02/08/12 by CAP

Work Begins:    02/13/12 at 08:00   Notice: 023 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         E.J. VANDENBOSCH         
Company:        ED CLARK & ASSOCIATES INC          
Address:        POB 3039, ROHNERT PARK                  
City:           ROHNERT PARK                  State: CA Zip: 94927
Business Tel:   707-792-9500                  Fax: 707-792-9504                
Email Address:  EJV@SONIC.NET                                               

Nature of Work: VERTICAL BORING FOR SOIL & GROUND WTR SA
Done for:       JOHN WILLIAMSON               Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 707-291-8774           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    COUNTY                        Number: FR0010697                
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         14450 CA RT 1
  Cross Street:         VALLEY FORD ESTERO RD

    WRK IN FRT OF ADDR 1 LOC 

Place: VALLEY FORD, CO AREA         County: SONOMA               State: CA

Long/Lat Long: -122.925658 Lat:  38.318227 Long: -122.924729 Lat:  38.318709 


Sent to:
PBTSRO = PACIFIC BELL SANTA ROSA      PGESRO = PGE DISTR SANTA ROSA         




At&t Ticket Id: 46386   clli code: VYFRCA11   OCC Tkt No: 0044978-000