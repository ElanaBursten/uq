
KORTERRA JOB TURNPIKE-SMTP-1 NJ140310354 Seq: 13 02/05/2014 11:06:35
New Jersey One Call System        SEQUENCE NUMBER 0007    CDC = NJT

Transmit:  Date: 01/31/14   At: 09:28

*** R O U T I N E         *** Request No.: 140310354

Operators Notified:
BAN     = VERIZON                       CAN     = CABLEVISION OF NJ
CGD     = NEON TRANSCOM                 CP2     = COLONIAL PIPELINE COMPANY
GTMF1   = G4S TECHNOLOGY LLC            NJT     = NEW JERSEY TURNPIKE AUTHO
NW2     = NEWARK, CITY OF               POR     = PORT AUTHORITY OF NY & NJ
PSHR    = PUBLIC SERVICE ELECTRIC &     XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/06/14   At 07:00  Expiration Date: 04/07/14

Location Information:
County: ESSEX                  Municipality: NEWARK
Subdivision/Community:
Street:               0 DOREMUS AVE
Nearest Intersection: I- 78
Other Intersection:   I- 78
Lat/Lon:
Type of Work: INSTALL FOOTINGS
Block:                Lot:                Depth: 50FT
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 80FT N AND
  80FT S.  CURB TO 150FT BEHIND W CURB.
Remarks:
  Working For Contact:  ALEX CONSTANTINIDES

Working For: M & J ENGINERRING  P.C
Address:     426 HUDSON ST
City:        HACKENSACK, NJ  07601
Phone:       201-880-1058   Ext:

Excavator Information:
Caller:      ASHISH PATEL
Phone:       215-968-5031   Ext:

Excavator:   PKF MARK III
Address:     170 PHEASANT RUN
City:        NEWTOWN, PA  18940
Phone:       215-968-5031   Ext:          Fax:  201-880-9111
Cellular:    215-416-7734
Email:       adp@pkfm.com
End Request


