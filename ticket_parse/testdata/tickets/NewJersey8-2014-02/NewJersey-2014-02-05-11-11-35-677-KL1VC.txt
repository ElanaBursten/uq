
KORTERRA JOB PARKWAY-SMTP-1 NJ140351224 Seq: 51 02/05/2014 11:06:25
New Jersey One Call System        SEQUENCE NUMBER 0032    CDC = NJH

Transmit:  Date: 02/04/14   At: 15:19

*** R O U T I N E         *** Request No.: 140351224

Operators Notified:
BAN     = VERIZON                       EG1     = ELIZABETHTOWN GAS COMPANY
GPMF1   = G4S TECHNOLOGY                NJH     = GARDEN STATE PARKWAY
PSCE    = PUBLIC SERVICE ELECTRIC &     SCEW1   = NJ AMERICAN WATER
SU2     = SUNOCO LOGISTICS

Start Date/Time:    02/08/14   At 08:00  Expiration Date: 04/09/14

Location Information:
County: UNION                  Municipality: CRANFORD
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G138.02
Other Intersection:
Lat/Lon:
Type of Work: INSTALL GUARD RAIL
Block:                Lot:                Depth: 6FT
Extent of Work: M/O AT MILE MARKER 138.02 EXTENDING 100FT S AND 100FT N.
  CURB TO 15FT BEHIND W CURB.  WORKING SOUTHBOUND LANES
Remarks:
  Working For Contact:  JOHN HEFTY

Working For: PKF MARK III
Address:     170 PHEASANT RUN
City:        NEWTOWN, NJ  18940
Phone:       215-416-7734   Ext:

Excavator Information:
Caller:      JACK BLAZER
Phone:       609-567-2122   Ext: 25

Excavator:   HIGHWAY SAFETY SYSTEMS INC.
Address:     716 WHITE HORSE PIKE
City:        HAMMONTON, NJ  08037
Phone:       609-567-2122   Ext: 25       Fax:  609-567-4420
Cellular:    609-839-2008
Email:       JBLAZER@HIGHWAYSAFETYSYSTEMSINC.COM
End Request


