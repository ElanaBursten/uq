
KORTERRA JOB PARKWAY-SMTP-1 NJ140350558 Seq: 35 02/05/2014 11:06:07
New Jersey One Call System        SEQUENCE NUMBER 0013    CDC = NJH

Transmit:  Date: 02/04/14   At: 11:07

*** R O U T I N E         *** Request No.: 140350558

Operators Notified:
AE1     = ATLANTIC CITY ELECTRIC        BAN     = VERIZON
CMC     = CAPE MAY COUNTY MUNICIPAL     GPMF4   = G4S TECHNOLOGY
MDT     = MIDDLE TOWNSHIP               MG3     = SUBURBAN PROPANE COMPANY
NJH     = GARDEN STATE PARKWAY          SCNJ3   = NJ AMERICAN WATER
SCV     = COMCAST CABLE (VINELAND)      SJG     = SOUTH JERSEY GAS COMPANY

Start Date/Time:    02/08/14   At 00:15  Expiration Date: 04/09/14

Location Information:
County: CAPE MAY               Municipality: MIDDLE TWP
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G7.5
Other Intersection:   G12.0
Lat/Lon:
Type of Work: ROAD RECONSTRUCTION
Block:                Lot:                Depth: 10FT
Extent of Work: M/O FROM MILE MARKER 7.5 TO MILE MARKER 12.0.  CURB TO 25FT
  BEHIND BOTH CURBS.  CURB TO CURB.  NORTHBOUND AND SOUTHBOUND LANES
Remarks:
  Working For Contact:  HOWARD PIERSON

Working For: NEW JERSEY TURNPIKE AUTHORITY
Address:     581 MAIN STREET
City:        WOODBRIDGE, NJ  07095
Phone:       609-743-7175   Ext:

Excavator Information:
Caller:      PENNY STUBBS
Phone:       856-769-8244   Ext:

Excavator:   R E PIERSON CONSTRUCTION CO
Address:     PO BOX 430
City:        WOODSTOWN, NJ  08098
Phone:       856-769-8244   Ext:          Fax:  856-769-5630
Cellular:    856-769-8244
Email:       pstubbs@repierson.com
End Request


