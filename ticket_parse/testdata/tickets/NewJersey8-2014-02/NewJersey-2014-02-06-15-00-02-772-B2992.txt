
KORTERRA JOB PARKWAY-SMTP-1 NJ140370904 Seq: 4 02/06/2014 14:59:35New Jersey One Call System        SEQUENCE NUMBER 0007    CDC = NJH

Transmit:  Date: 02/06/14   At: 14:23

*** R O U T I N E         *** Request No.: 140370904

Operators Notified:
BAN     = VERIZON                       CC4     = COMCAST CABLEVISION OF NE
GP9     = JERSEY CENTRAL POWER & LI     HOL     = HOLMDEL, TOWNSHIP OF
NJH     = GARDEN STATE PARKWAY          NJN     = NEW JERSEY NATURAL GAS CO
SCNJ7   = NJ AMERICAN WATER             SHW     = SHORELANDS WATER COMPANY,

Start Date/Time:    02/12/14   At 00:15  Expiration Date: 04/11/14

Location Information:
County: MONMOUTH               Municipality: HOLMDEL
Subdivision/Community: SIGN SHOP/DIST 4 YARD
Street:               0 GARDEN STATE ART CTR
Nearest Intersection: CRAWFORDS CORNER RD
Other Intersection:
Lat/Lon:
Type of Work: INSTALL POLE(S)
Block:                Lot:                Depth: 8FT
Extent of Work: 2 AREA(S) MARKED IN WHITE.  WHITE MARKS ARE NOT LABELED.
  WHITE MARKS LOCATED 1245FT N FROM C/L OF INTERSECTION ON W SIDE OF ROAD
  427FT FROM CURB AND EXTEND(S) 543FT W.
Remarks:
  Working For Contact:  ROBERT MILLER

Working For: NEW JERSEY TURNPIKE AUTHORITY
Address:     PO BOX 5042
City:        WOODBRIDGE, NJ  07095
Phone:       732-735-3494   Ext:

Excavator Information:
Caller:      MICHAEL TEZBIR
Phone:       732-293-1126   Ext:

Excavator:   NEW JERSEY TURNPIKE AUTHORITY
Address:     PO BOX 5042
City:        WOODBRIDGE, NJ  07095
Phone:       732-293-1126   Ext:          Fax:  732-293-3672
Cellular:    732-735-3494
Email:       rdeyoung@turnpike.state.nj.us
End Request


