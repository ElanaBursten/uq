
KORTERRA JOB TURNPIKE-SMTP-1 NJ140340102 Seq: 20 02/05/2014 11:06:42
New Jersey One Call System        SEQUENCE NUMBER 0004    CDC = NJT

Transmit:  Date: 02/03/14   At: 08:45

*** R O U T I N E         *** Request No.: 140340102

Operators Notified:
BAN     = VERIZON                       CAN     = CABLEVISION OF NJ
CGD     = NEON TRANSCOM                 GTMF1   = G4S TECHNOLOGY LLC
MFR     = ZAYO GROUP                    NEITS01 = NORTHEASTERN ITS, LLC
NJT     = NEW JERSEY TURNPIKE AUTHO     NW2     = NEWARK, CITY OF
PSGT    = PUBLIC SERVICE ELECTRIC &     PSHR    = PUBLIC SERVICE ELECTRIC &
TGP     = TRANSCONTINENTAL GAS PIPE     USS     = SPRINT
XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/10/14   At 07:00  Expiration Date: 04/08/14

Location Information:
County: ESSEX                  Municipality: NEWARK
Subdivision/Community: NEWARK WATER DEPARTMENT
Street:               0 WILSON AVE
Nearest Intersection: HYATT AVE
Other Intersection:   AVE P
Lat/Lon:
Type of Work: INSTALL SERVICE UTILITIES
Block:                Lot:                Depth: 6FT
Extent of Work: M/O ENTIRE LENGTH OF WILSON AVE FROM C/L OF HYATT AVE TO C/L
  OF AVE P.  CURB TO 20FT BEHIND BOTH CURBS.  CURB TO CURB.
Remarks:
  Working For Contact:  ZAFAR ALDI

Working For: NEWARK WATER DEPT
Address:     255 CENTRAL AVE
City:        NEWARK, NJ  07103
Phone:       973-733-8414   Ext:

Excavator Information:
Caller:      LOUIS SANTOS
Phone:       908-862-8936   Ext: 17

Excavator:   UNDERGROUND UTILITIES CORP.
Address:     711 COMMERCE ROAD
City:        LINDEN, NJ  07036
Phone:       908-413-5215   Ext:          Fax:  908-862-8690
Cellular:    908-413-5215
Email:       lsantos@undergroundutilitiescorp.com
End Request


