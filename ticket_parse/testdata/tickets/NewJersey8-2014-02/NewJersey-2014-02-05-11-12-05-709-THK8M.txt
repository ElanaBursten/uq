
KORTERRA JOB TURNPIKE-SMTP-1 NJ140300954 Seq: 16 02/05/2014 11:06:38
New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = NJT

Transmit:  Date: 01/30/14   At: 14:16

*** R O U T I N E         *** Request No.: 140300954

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
CC7     = COMCAST CABLEVISION OF NE     CGD     = NEON TRANSCOM
FTN     = FIBER TECHNOLOGY NETWORKS     GTMF1   = G4S TECHNOLOGY LLC
LKT     = LEVEL 3 COMMUNICATIONS        NEITS01 = NORTHEASTERN ITS, LLC
NJT     = NEW JERSEY TURNPIKE AUTHO     PSJC    = PUBLIC SERVICE ELECTRIC &
QWS     = QWEST COMMUNICATIONS INTE     SEC     = SECAUCUS MUNICIPAL UTILIT
UW4     = UWNJ-NE                       XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/05/14   At 07:00  Expiration Date: 04/04/14

Location Information:
County: HUDSON                 Municipality: SECAUCUS
Subdivision/Community: ALEXANDER HAMILTON REST AREA
Street:               0 NEW JERSEY TPKE
Nearest Intersection: T111.7
Other Intersection:   T111.6
Lat/Lon:
Type of Work: INSTALL WATER AND SEWER SERVICE
Block:                Lot:                Depth: 10FT
Extent of Work: 2 AREA(S) MARKED IN WHITE.  WHITE MARKS ARE LABELED WATER &
  SEWER.  WHITE MARKS LOCATED BETWEEN MILE MARKER 111.7 AND MILE MARKER
  111.6 ON W SIDE(S) OF ROAD 600FT FROM CURB AND EXTEND 150FT W AND
  1000FT S.  WORKING ON SOUTHBOUND LANES.
Remarks:
  Working For Contact:  BOB KUDRICK

Working For: HALL BUILDING CORP
Address:     33 MAIN ST PO BOX 904
City:        FARMINGDALE, NJ  07727
Phone:       732-938-3399   Ext:

Excavator Information:
Caller:      ROB WINES
Phone:       732-938-7379   Ext:

Excavator:   K & R SITE WORK
Address:     23 YELLOWBROOK RD
City:        FREEHOLD, NJ  07728
Phone:       732-938-7379   Ext:          Fax:  732-938-7972
Cellular:    732-433-5753
Email:       krsiteworkinc@aol.com
End Request


