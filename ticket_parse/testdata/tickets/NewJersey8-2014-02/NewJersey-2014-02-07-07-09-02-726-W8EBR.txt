
KORTERRA JOB PARKWAY-SMTP-1 NJ140371498 Seq: 1 02/07/2014 07:08:32New Jersey One Call System        SEQUENCE NUMBER 0009    CDC = NJH

Transmit:  Date: 02/06/14   At: 21:08

*** R O U T I N E         *** Request No.: 140371498

Operators Notified:
BAN     = VERIZON                       CC7     = COMCAST CABLEVISION OF NE
CLT     = CLARK TOWNSHIP                EG1     = ELIZABETHTOWN GAS COMPANY
GPMF1   = G4S TECHNOLOGY                NJH     = GARDEN STATE PARKWAY
PSCE    = PUBLIC SERVICE ELECTRIC &     SCEW1   = NJ AMERICAN WATER

Start Date/Time:    02/12/14   At 00:01  Expiration Date: 04/11/14

Location Information:
County: UNION                  Municipality: CLARK
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G136.6
Other Intersection:   G136.9
Lat/Lon:
Type of Work: INSTALL FOOTINGS
Block:                Lot:                Depth: 50FT
Extent of Work: M/O FROM MILE MARKER 136.6 TO MILE MARKER 136.9.  CURB TO
  30FT BEHIND BOTH CURBS.  SOUTH BOUND
Remarks:
  Working For Contact:  ANDY MCCOANELL

Working For: NEW JERSEY TURNPIKE AUTHORITY
Address:     PO BOX 5042
City:        WOODBRIDGE, NJ  07095
Phone:       732-750-5300   Ext:

Excavator Information:
Caller:      JOSEPH WALSH
Phone:       201-522-6078   Ext:

Excavator:   J FLETCHER CREAMER AND SON
Address:     101 E BROADWAY
City:        HACKENSACK, NJ  07601
Phone:       201-522-6078   Ext:          Fax:  908-276-5693
Cellular:    201-522-6078
Email:       jtwalsh@jfcson.com
End Request


