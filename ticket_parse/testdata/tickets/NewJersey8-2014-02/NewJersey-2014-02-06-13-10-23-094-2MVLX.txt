
KORTERRA JOB PARKWAY-SMTP-1 NJ140370684 Seq: 2 02/06/2014 13:09:38New Jersey One Call System        SEQUENCE NUMBER 0004    CDC = NJH

Transmit:  Date: 02/06/14   At: 11:57

*** R O U T I N E         *** Request No.: 140370684

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
GPMF3   = G4S TECHNOLOGY                GPS     = JERSEY CENTRAL POWER & LI
MCI     = MCI                           MCU     = MIDDLESEX COUNTY UTILITIE
NJH     = GARDEN STATE PARKWAY          PRX     = PRAXAIR, INC.
PSNB    = PUBLIC SERVICE ELECTRIC &     SAY     = BOROUGH OF SAYREVILLE WAT
USS     = SPRINT

Start Date/Time:    02/12/14   At 00:15  Expiration Date: 04/11/14

Location Information:
County: MIDDLESEX              Municipality: SAYREVILLE
Subdivision/Community: NJ TURNPIKE SALT YARD
Street:               0 CHEVALIER AVE
Nearest Intersection: MAIN ST EXD
Other Intersection:
Lat/Lon:
Type of Work: INSTALL POLE(S)
Block:                Lot:                Depth: 8FT
Extent of Work: 1 AREA(S) MARKED IN WHITE.  WHITE MARKS ARE NOT LABELED.
  WHITE MARKS LOCATED 832FT E FROM C/L OF INTERSECTION ON S SIDE OF ROAD
  5FT FROM CURB AND EXTEND(S) 460FT S.
Remarks:
  Working For Contact:  ROBERT MILLER

Working For: NEW JERSEY TURNPIKE AUTHORITY
Address:     PO BOX 5042
City:        WOODBRIDGE, NJ  07095
Phone:       732-735-3494   Ext:

Excavator Information:
Caller:      MICHAEL TEZBIR
Phone:       732-293-1126   Ext:

Excavator:   NEW JERSEY TURNPIKE AUTHORITY
Address:     PO BOX 5042
City:        WOODBRIDGE, NJ  07095
Phone:       732-293-1126   Ext:          Fax:  732-293-3672
Cellular:    732-735-3494
Email:       rdeyoung@turnpike.state.nj.us
End Request


