
KORTERRA JOB TURNPIKE-SMTP-1 NJ140310629 Seq: 15 02/05/2014 11:06:37
New Jersey One Call System        SEQUENCE NUMBER 0018    CDC = NJT

Transmit:  Date: 01/31/14   At: 11:39

*** R O U T I N E         *** Request No.: 140310629

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
BUK     = BUCKEYE PARTNERS              CGD     = NEON TRANSCOM
EG1     = ELIZABETHTOWN GAS COMPANY     ELC     = ELIZABETH CITY
GTMF1   = G4S TECHNOLOGY LLC            MFR     = ZAYO GROUP
NEITS01 = NORTHEASTERN ITS, LLC         NJT     = NEW JERSEY TURNPIKE AUTHO
PSCE    = PUBLIC SERVICE ELECTRIC &     PSET    = PUBLIC SERVICE ELECTRIC &
PSGT    = PUBLIC SERVICE ELECTRIC &     SCLWC   = NJ AMERICAN WATER
TGP     = TRANSCONTINENTAL GAS PIPE     TK7     = CABLEVISION OF NJ
USS     = SPRINT

Start Date/Time:    02/06/14   At 00:01  Expiration Date: 04/07/14

Location Information:
County: UNION                  Municipality: ELIZABETH
Subdivision/Community:
Street:               0 NEW JERSEY TPKE
Nearest Intersection: T100.00
Other Intersection:
Lat/Lon:
Type of Work: INSTALL GUARD RAIL
Block:                Lot:                Depth: 6FT
Extent of Work: M/O AT MILE MARKER 100.00 EXTENDING 250FT S AND 200FT N.
  CURB TO CURB IN MEDIAN ONLY. WORKING IN THE NORTHBOUND CAR AND TRUCK
  LANES.
Remarks:
  Working For Contact:  JOHN HEFTY

Working For: PKF MARK 3
Address:     170 PHEASANT RUN
City:        NEWTOWN, PA  18940
Phone:       215-416-7734   Ext:

Excavator Information:
Caller:      JACK BLAZER
Phone:       609-567-2122   Ext: 25

Excavator:   HIGHWAY SAFETY SYSTEMS INC.
Address:     716 WHITE HORSE PIKE
City:        HAMMONTON, NJ  08037
Phone:       609-567-2122   Ext: 25       Fax:  609-567-4420
Cellular:    609-839-2008
Email:       JBLAZER@HIGHWAYSAFETYSYSTEMSINC.COM
End Request


