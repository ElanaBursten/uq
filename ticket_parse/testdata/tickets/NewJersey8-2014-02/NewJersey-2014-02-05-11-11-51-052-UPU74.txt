
KORTERRA JOB TURNPIKE-SMTP-1 NJ140350689 Seq: 11 02/05/2014 11:06:32
New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = NJT

Transmit:  Date: 02/04/14   At: 11:48

*** R O U T I N E         *** Request No.: 140350689

Operators Notified:
BAN     = VERIZON                       BCD     = BERGEN COUNTY D.P.W.
GTMF1   = G4S TECHNOLOGY LLC            MFR     = ZAYO GROUP
NJT     = NEW JERSEY TURNPIKE AUTHO     PSET    = PUBLIC SERVICE ELECTRIC &
PSOR    = PUBLIC SERVICE ELECTRIC &     RID     = RIDGEFIELD PARK VILLAGE
TWC     = TIME WARNER CABLE             UW4     = UWNJ-NE

Start Date/Time:    02/08/14   At 07:00  Expiration Date: 04/09/14

Location Information:
County: BERGEN                 Municipality: RIDGEFIELD PARK
Subdivision/Community:
Street:               0 BERGEN TPKE
Nearest Intersection: TEANECK RD
Other Intersection:
Lat/Lon:
Type of Work: SOIL BORINGS
Block:                Lot:                Depth: 50FT
Extent of Work: M/O BEGINS 1000FT S OF C/L OF INTERSECTION AND EXTENDS 400FT
  S. M/O BEGINS 100FT BEHIND W CURB AND EXTENDS 100FT BEHIND W CURB
Remarks:
  Working For Contact:  JACOB FRADKIN

Working For: MFS CONSULTING ENGINEERS
Address:     2780 HAMILTON BLVD
City:        SOUTH PLAINFIELD, NJ  07080
Phone:       908-922-4946   Ext:

Excavator Information:
Caller:      GABRIELLE MINARIK
Phone:       610-404-2570   Ext:

Excavator:   EARTHCORE SERVICES
Address:     1458 SHANER DR
City:        POTTSTOWN, PA  19465
Phone:       610-404-2570   Ext:          Fax:
Cellular:    610-404-2570
Email:       info@EarthcoreUSA.com
End Request


