
KORTERRA JOB PARKWAY-SMTP-1 NJ140350097 Seq: 26 02/05/2014 11:05:57
New Jersey One Call System        SEQUENCE NUMBER 0002    CDC = NJH

Transmit:  Date: 02/04/14   At: 07:41

*** E M E R G E N C Y     *** Request No.: 140350097

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
CC4     = COMCAST CABLEVISION OF NE     FRC     = 4 CONNECTIONS, L.L.C.
GP9     = JERSEY CENTRAL POWER & LI     GPMF3   = G4S TECHNOLOGY
MCI     = MCI                           MTS     = TOWNSHIP OF MIDDLETOWN SE
NJH     = GARDEN STATE PARKWAY          NJN     = NEW JERSEY NATURAL GAS CO
SCNJ7   = NJ AMERICAN WATER             USS     = SPRINT

Start Date/Time:    02/04/14   At 07:45  Expiration Date:

Location Information:
County: MONMOUTH               Municipality: MIDDLETOWN
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G109.8
Other Intersection:   G110.1
Lat/Lon:
Type of Work: EMERGENCY - REPAIR/REPLACE DAMAGED GUARD RAIL
Block:                Lot:                Depth: 10FT
Extent of Work: M/O FROM MILE MARKER 109.8 TO MILE MARKER 110.1.  CURB TO
  25FT BEHIND W CURB.  CURB TO CURB.  WORKING ON THE INNER NORTH BOUND
  EXPRESS LANES.
Remarks:
  PUBLIC SAFETY HAZARD
  Working For Contact:  BRANDY BLAZER

Working For: J FLETCHER CREAMER
Address:     1219 MAYS LANDING RD
City:        FLOSOM, NJ  08037
Phone:       609-561-2403   Ext:

Excavator Information:
Caller:      JOHN THOMAS
Phone:       609-561-3800   Ext: 22

Excavator:   M L RUBERTON CONSTRUCTION
Address:     1512 MAYS LANDING ROAD
City:        HAMMONTON, NJ  08037
Phone:       609-561-3800   Ext: 22       Fax:  609-567-8349
Cellular:    609-839-5936
Email:       johnt@ruberton.com
End Request


