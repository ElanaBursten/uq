
KORTERRA JOB TURNPIKE-SMTP-1 NJ140310583 Seq: 14 02/05/2014 11:06:36
New Jersey One Call System        SEQUENCE NUMBER 0014    CDC = NJT

Transmit:  Date: 01/31/14   At: 11:15

*** R O U T I N E         *** Request No.: 140310583

Operators Notified:
BAN     = VERIZON                       CAN     = CABLEVISION OF NJ
NJT     = NEW JERSEY TURNPIKE AUTHO     NW2     = NEWARK, CITY OF
POR     = PORT AUTHORITY OF NY & NJ     PSET    = PUBLIC SERVICE ELECTRIC &
PSGT    = PUBLIC SERVICE ELECTRIC &     PSHR    = PUBLIC SERVICE ELECTRIC &
SU2     = SUNOCO LOGISTICS              XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/06/14   At 00:15  Expiration Date: 04/07/14

Location Information:
County: ESSEX                  Municipality: NEWARK
Subdivision/Community:
Street:               0 I- 78
Nearest Intersection: MCCARTER HWY
Other Intersection:
Lat/Lon:
Type of Work: WELL DRILLING
Block: 5088.02        Lot:  162           Depth: 15FT
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 800FT E.  CURB
  TO 300FT BEHIND S CURB.
Remarks:
  Working For Contact:  RAY GLOVER

Working For: DRESDNER ROBBINS
Address:     371 WARREN AVE
City:        JERSEY CITY, NJ  07302
Phone:       201-926-7693   Ext:

Excavator Information:
Caller:      JACKIE HANSEN
Phone:       609-538-0580   Ext:

Excavator:   TALON DRILLING COMPANY
Address:     100 LEXINGTON AVE
City:        TRENTON, NJ  08618
Phone:       609-538-0580   Ext:          Fax:  609-538-0575
Cellular:    609-538-0580
Email:       talondrillingcompany@gmail.com
End Request


