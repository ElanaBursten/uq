
XZ     00007 POCS 02/07/12 13:44:20 20120385008-000 NEW  XCAV INSF

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============
Serial Number--[20120385008]-[000] Channel#--[1344900][0029]
Message Type--[NEW][EXCAVATION][INSUFFICIENT]

County--[BUCKS]           Municipality--[BENSALEM TWP]
Work Site--[539 DUNKSFERRY RD  ADDING CHARACTERS TO MAX THE FIELD COMPLETELY]
     Nearest Intersection--[NESHAMINY STATE PARK]
     Second Intersection--[MARSHALL LN]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [NEW ABOVE GROUND STORAGE TANKS HAVE BEEN INSTALLED. WORKING AROUND THE NEW
      TANKS BTWN STATE RD AND MARSHALL LN]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.082175/-74.928140,40.083037/-74.926758,40.078142/-74.922506,
      40.077502/-74.924105]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20120385008]
Type of Work--[INSUFFICIENT TEST]                            Depth--[3FT]
Extent of Excavation--[10 IN DIA]       Method of Excavation--[AUGERING]
Street--[ ] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[X] Other--[]

Lawful Start Dates--[10-Feb-12] thru [22-Feb-12] Response Due Date--[09-Feb-12]
      Scheduled Excavation Date--[07-Feb-12] Dig Time--[1500] Duration--[]

Caller--[JAMES HARRIS ADDING CHARACTERS TO MAX THE FIELD ENTIRELY]
Caller Phone--[609-387-4050]              Caller Ext--[]
Excavator--[HARRIS FENCE CORP ADDING CHARACTERS TO MAX THE FIELD OUT COMPLETELY]
Address--[4492  US HWY130 AM MAXING FIELD CHARACTERS]
City--[BERLINGTON MAXING FIELD CHARACTERS TEST-]   State--[NJ] Zip--[08016]
FAX--[609-387-0277]              Caller Type--[B]
Email--[]
Work For--[NRAC ADDING CHARACTERS TO MAXIMIZE THE FIELD LENGTH COMPLETELY FULL]

Person to Contact--[JAMES HARRIS  ADDING CHARACTERS TO MAX THE FIELD ALL THE]
Contact Phone--[609-387-4050]              Contact Ext--[]
Best Time to Call--[0800-1700]

Prepared--[07-Feb-12] at [1344] by [DONNA WILLIAMS]
Remarks--
     []

FP 0  FP =BUCKS CNTY W&SA  HR 0  HR =AQUA PA INC      HR10  HR1=BENSALEM TWP   
KD 0  KD =PECO WRTR        XZ 0  XZ =COMCAST CABLE B  YI 0  YI =VERIZON EAST 1

Serial Number--[20120385008]-[000]
========== Copyright (c) 2012 by Pennsylvania One Call System, Inc. ==========