
XZ     00002 POCS 02/07/12 13:38:28 20120385002-000 NEW  XCAV EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============
Serial Number--[20120385002]-[000] Channel#--[1335900][0211]
Message Type--[NEW][EXCAVATION][EMERGENCY]

County--[BUCKS]           Municipality--[BENSALEM TWP]
Work Site--[539 DUNKSFERRY RD  ADDING CHARACTERS TO MAX THE FIELD COMPLETELY]
     Nearest Intersection--[NESHAMINY STATE PARK]
     Second Intersection--[MARSHALL LN]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [NEW ABOVE GROUND STORAGE TANKS HAVE BEEN INSTALLED. WORKING AROUND THE NEW
      TANKS BTWN STATE RD AND MARSHALL LN]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.082384/-74.927447,40.079270/-74.924377,40.081365/-74.919789,
      40.085357/-74.921935]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20120385002]
Type of Work--[INSTL FENCE]                                  Depth--[3FT]
Extent of Excavation--[10 IN DIA]       Method of Excavation--[AUGERING]
Street--[ ] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[X] Other--[]

Lawful Start Dates--[         ] thru [         ] Response Due Date--[07-Feb-12]
      Scheduled Excavation Date--[07-Feb-12] Dig Time--[1345] Duration--[]

Caller--[JAMES HARRIS ADDING CHARACTERS TO MAX THE FIELD ENTIRELY]
Caller Phone--[609-387-4050]              Caller Ext--[]
Excavator--[HARRIS FENCE CORP ADDING CHARACTERS TO MAX THE FIELD OUT COMPLETELY]
Address--[4492  US HWY130 AM MAXING FIELD CHARACTERS]
City--[BERLINGTON MAXING FIELD CHARACTERS TEST-]   State--[NJ] Zip--[08016]
FAX--[609-387-0277]              Caller Type--[B]
Email--[]
Work For--[NRAC ADDING CHARACTERS TO MAXIMIZE THE FIELD LENGTH COMPLETELY FULL]

Person to Contact--[JAMES HARRIS  ADDING CHARACTERS TO MAX THE FIELD ALL THE]
Contact Phone--[609-387-4050]              Contact Ext--[]
Best Time to Call--[0800-1700]

Prepared--[07-Feb-12] at [1338] by [DONNA WILLIAMS]
Remarks--
     []

ATM0  ATM=AT&T ATLANTA     FP 0  FP =BUCKS CNTY W&SA  HR 0  HR =AQUA PA INC    
HR10  HR1=BENSALEM TWP     KD 0  KD =PECO WRTR        XZ 0  XZ =COMCAST CABLE B
YI 0  YI =VERIZON EAST 1

Serial Number--[20120385002]-[000]
========== Copyright (c) 2012 by Pennsylvania One Call System, Inc. ==========