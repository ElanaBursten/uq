New Jersey One Call System        SEQUENCE NUMBER 0025    CDC = SCNJ4 

Transmit:  Date: 08/13/14   At: 11:52 

*** R O U T I N E         *** Request No.: 142251126 

Operators Notified: 
BAN     = VERIZON                       CC4     = COMCAST CABLEVISION OF NE      
GPP     = JERSEY CENTRAL POWER & LI     NJN     = NEW JERSEY NATURAL GAS CO      
SCNJ4   = NJ AMERICAN WATER              

Start Date/Time:    08/19/14   At 08:00  Expiration Date: 10/17/14 

Location Information: 
County: OCEAN                  Municipality: BAY HEAD 
Subdivision/Community:  
Street:               675 EAST AVE 
Nearest Intersection: JOHNSON ST 
Other Intersection:    
Lat/Lon: 
Type of Work: LANDSCAPING 
Block:                Lot:                Depth: 3FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  BRUCE WESSON 

Working For: HOMEOWNER 
Address:     60 WHITTREDGE RD 
City:        SUMMIT, NJ  07901 
Phone:       732-233-7954   Ext:  

Excavator Information: 
Caller:      LYNDA BARROW 
Phone:       732-449-6220   Ext:  

Excavator:   BORAB LANDSCAPE  INC. 
Address:     2151 EVERGREEN AVE 
City:        SEA GIRT, NJ  08750 
Phone:       732-449-6220   Ext:          Fax:  732-449-6226 
Cellular:    732-996-4710 
Email:       george@borab.com 
End Request 

