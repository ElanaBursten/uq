
ZZQ06 58 PUPS Voice 07/27/2010 13:33:00 1007270081 Design 

Ticket Number: 1007270081
Old Ticket Number: 
Created By: RMD
Seq Number: 58

Created Date: 07/27/2010 13:35:01
Work Date/Time: 08/10/2010 13:45:20
Update: 08/27/2010 13:45:20 Good Through: 09/01/2010 13:45:20

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 220
Street: MANOR VIEW CT
Inters St: WOODCROSS DR
Subd: TEST

Type of Work: WATER, REPAIR SERVICE
Duration: 5 HOURS

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0762657787967, -81.1462062197249
Secondary: 34.0757069873268, -81.1446479638373
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CGT63 COC82 MID55 SCEKZ42 SCGZ05 TWCZ40              


Map Link: (NEEDS DEVELOPMENT)

Design tickets are courtesy tickets.  The utilities will mark the
requested area on a voluntary basis.  Members are to locate within
10 working days, please call requestor if you are unable to locate
within the 10 working days.


