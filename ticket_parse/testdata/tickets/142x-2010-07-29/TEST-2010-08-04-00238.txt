
ZZQ06 136 PUPS Voice 08/04/2010 03:46:00 PM 2208040407 Normal 

Ticket Number: 2208040407
Old Ticket Number: 
Created By: JDB
Seq Number: 136

Created Date: 08/04/2010 03:47:01 PM
Work Date/Time: 08/09/2010 04:00:37 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: DILLON
Place: LAKE VIEW
Address Number: 
Street: RICHARD TEMPLE BLVD
Inters St: W 3RD AVE
Subd: 

Type of Work: SEE REMARKS
Duration: 1 WEEK

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.3642826087405, -79.1988055664553
Secondary: 34.32675745867, -79.154802133148
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 PDEZ40 PETZ85 TWDZ11                          


Map Link: (NEEDS DEVELOPMENT)


