
ZZQ06 82 PUPS Voice 07/27/2010 15:01:00 1007270121 Normal 

Ticket Number: 1007270121
Old Ticket Number: 
Created By: WGS
Seq Number: 82

Created Date: 07/27/2010 15:02:07
Work Date/Time: 07/30/2010 15:15:13
Update: 08/18/2010 15:15:13 Good Through: 08/23/2010 15:15:13

Excavation Information:
State: SC     County: LEXINGTON
Place: OAK GROVE
Address Number: 
Street: I20 W
Inters St: SUNSET BLVD
Subd: 

Type of Work: ROAD CONSTRUCTION, INSTALL SIGNS
Duration: 1 DAY

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0181643251494, -81.1557494333855
Secondary: 34.0072317069938, -81.1405776367614
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CWC18 DIX54 PBT43 SCEDZ05 SCEJZ40 SCGZ05 SCTDZ01     
TLX24 TWCZ40 TWTZ26 WINZ08                                                    


Map Link: (NEEDS DEVELOPMENT)


