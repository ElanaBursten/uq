
ZZQ06 33 PUPS Web 07/27/2010 08:46:00 1007270031 Update 

Ticket Number: 1007270031
Old Ticket Number: 1007220019
Created By: RMD
Seq Number: 33

Created Date: 07/27/2010 08:46:52
Work Date/Time: 07/30/2010 09:00:46
Update: 08/18/2010 09:00:46 Good Through: 08/23/2010 09:00:46

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 1500
Street: MARION ST
Inters St: TAYLOR ST
Subd: 

Type of Work: AERATION
Duration: APPROX 5 HOURS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: CITY OF COLUMBIA WATER DEPARTMENT

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0249832910105, -81.0412873316091
Secondary: 33.9994383908052, -81.0290386728356
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 COC82 ITC73 LC393 SCA88 SCEJZ40 SCEKZ42 SCG02  
SCGZ02 STG10 TWCZ40 TWTZ26 USC42                                              


Map Link: (NEEDS DEVELOPMENT)


