
ZZQ35 78 PUPS Tck Project 08/04/2010 02:28:00 PM 2208040276 Update 

Ticket Number: 2208040276
Old Ticket Number: 
Created By: JDB
Seq Number: 78

Created Date: 08/04/2010 02:28:34 PM
Work Date/Time: 08/09/2010 02:30:09 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LANCASTER
Place: LESLIE
Address Number: 
Street: OLD HICKORY ROAD
Inters St: PORTER RANCH ROAD & DEER RUN ROAD
Subd: 

Type of Work: BRIDGE, CONSTRUCTION
Duration: 118 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8300899017988, -80.829842814526
Secondary: 34.8211454907575, -80.8275656285129
Lat/Long Caller Supplied: N

Members Involved: DPCZ72 LTC28                                                


Map Link: (NEEDS DEVELOPMENT)


