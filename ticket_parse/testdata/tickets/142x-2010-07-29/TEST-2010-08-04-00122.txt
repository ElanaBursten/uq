
ZZQ06 82 PUPS Voice 08/04/2010 10:40:00 AM 2208040210 Resend 

Ticket Number: 2208040210
Old Ticket Number: 
Created By: JDB
Seq Number: 82

Created Date: 08/04/2010 10:40:52 AM
Work Date/Time: 08/04/2010 10:45:37 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SUMTER
Place: SUMTER
Address Number: 
Street: HOLLY ST
Inters St: LAUREL ST
Subd: BASE HOUSING

Type of Work: SOIL TEST BORING(S)
Duration: APPROX 1 WEEK

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: TRANS SYSTEM

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9860245903799, -80.4868381804167
Secondary: 33.9858105435571, -80.4851157723889
Lat/Long Caller Supplied: N

Members Involved: FTCZ81 TWUZ46 VERZ06                                        


Map Link: (NEEDS DEVELOPMENT)


