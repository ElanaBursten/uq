
ZZQ06 33 PUPS Voice 08/04/2010 08:27:00 AM 2208040020 Remark 

Ticket Number: 2208040020
Old Ticket Number: 
Created By: JDB
Seq Number: 33

Created Date: 08/04/2010 08:28:13 AM
Work Date/Time: 08/04/2010 08:30:01 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: BEAUFORT
Place: HILTON HEAD ISLAND
Address Number: 4
Street: MATHEWS CT
Inters St: MATHEWS DR
Subd: 

Type of Work: ELECTRIC, REPLACE SECONDARY
Duration: 3-5 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: GCREEDEN@PALMETTO.COOP

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 32.2133368059613, -80.7032718487647
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: HHP61 HRGZ57 TWBZ51                                         


Map Link: (NEEDS DEVELOPMENT)


