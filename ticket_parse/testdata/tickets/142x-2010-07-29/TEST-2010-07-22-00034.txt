
ZZQ06 22 PUPS Email 07/22/2010 11:06:00 1007220034 Emergency 

Ticket Number: 1007220034
Old Ticket Number: 
Created By: RMD
Seq Number: 22

Created Date: 07/22/2010 11:07:25
Work Date/Time: 07/22/2010 11:15:48
Update: 08/10/2010 11:15:48 Good Through: 08/13/2010 11:15:48

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 310
Street: S EDISTO AVE
Inters St: ROSEWOOD DR
Subd: 

Type of Work: SEWER, TAP RENEWAL
Duration: 4 HOURS

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: CITY OF COLUMBIA WASTEWATER MAINTENANCE

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY // IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME //THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1       


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9867050571098, -81.0123638988386
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEJZ40 SCG02 TWCZ40                           


Map Link: (NEEDS DEVELOPMENT)


