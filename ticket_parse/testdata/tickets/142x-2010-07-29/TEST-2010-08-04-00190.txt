
ZZQ35 96 PUPS Email 08/04/2010 02:44:00 PM 2208040315 Normal 

Ticket Number: 2208040315
Old Ticket Number: 
Created By: JAC
Seq Number: 96

Created Date: 08/04/2010 02:45:14 PM
Work Date/Time: 08/09/2010 02:45:50 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: ANDERSON
Place: TOWNVILLE
Address Number: 8112
Street: HWY 24
Inters St: ONEAL FERRY RD
Subd: 

Type of Work: SEE REMARKS
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.5625438848728, -82.8782297245792
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ04 FTHSZ64                                


Map Link: (NEEDS DEVELOPMENT)


