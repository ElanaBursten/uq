
ZZQ35 84 PUPS Tck Project 08/04/2010 02:32:00 PM 2208040287 Normal 

Ticket Number: 2208040287
Old Ticket Number: 
Created By: JDB
Seq Number: 84

Created Date: 08/04/2010 02:33:08 PM
Work Date/Time: 08/09/2010 02:45:55 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CHESTER
Place: LOWRYS
Address Number: 
Street: COLONY ROAD
Inters St: DARBY DR
Subd: 

Type of Work: BRIDGE, CONSTRUCTION
Duration: 104 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8145532711758, -81.2020171408642
Secondary: 34.8109497141882, -81.1838658908525
Lat/Long Caller Supplied: N

Members Involved: ATT09 CHG31 CHT11 DPCZ06                                    


Map Link: (NEEDS DEVELOPMENT)


