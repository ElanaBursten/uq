
ZZQ06 20 PUPS Email 07/22/2010 11:03:00 1007220031 Survey 

Ticket Number: 1007220031
Old Ticket Number: 
Created By: RMD
Seq Number: 20

Created Date: 07/22/2010 11:04:27
Work Date/Time: 07/27/2010 11:15:58
Update: 08/13/2010 11:15:58 Good Through: 08/18/2010 11:15:58

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 230
Street: EDISTO AVE
Inters St: SENECA ST
Subd: 

Type of Work: DITCH, WIDEN
Duration: 6 MONTHS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: HAY HILL SERVICES

Remarks/Instructions: DIRS: FROM THE INTERSECTION OF S HARDEN ST AND ROSEWOOD 
DR, GO NORTH ON S HARDEN ST (TOWARD 5 POINTS), TAKE THE THIRD LEFT ONTO       
HEYWARD ST, TAKE THE SECOND RIGHT ONTO EDISTO AVE// LOCATE ENTIRE SITE        


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: LAUREN@HAYHILLSERVICES.COM

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9937165433628, -81.0159684754218
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCG02 TWCZ40                                   


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


