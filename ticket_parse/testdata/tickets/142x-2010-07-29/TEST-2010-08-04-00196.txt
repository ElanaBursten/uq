
ZZQ35 98 PUPS Email 08/04/2010 02:46:00 PM 2208040321 Resend 

Ticket Number: 2208040321
Old Ticket Number: 
Created By: JAC
Seq Number: 98

Created Date: 08/04/2010 02:47:36 PM
Work Date/Time: 08/04/2010 03:00:13 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: GREENVILLE
Place: GREER
Address Number: 
Street: N HWY 101
Inters St: LOCUST HILL RD
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN CABLE
Duration: 2 WEEKS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: MS0648@ATT.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9515596141656, -82.2658172071519
Secondary: 34.948044675675, -82.2633567502085
Lat/Long Caller Supplied: N

Members Involved: BSZT29 DPCZ08 GRR01                                         


Map Link: (NEEDS DEVELOPMENT)


