
ZZQ06 78 PUPS Walk-In 07/27/2010 14:57:00 1007270116 No Show 

Ticket Number: 1007270116
Old Ticket Number: 
Created By: DBL
Seq Number: 78

Created Date: 07/27/2010 14:59:01
Work Date/Time: 07/27/2010 15:00:44
Update:                     Good Through:                    

Excavation Information:
State: SC     County: KERSHAW
Place: ELGIN
Address Number: 2970
Street: HWY 1 S
Inters St: STEVEN CAMPBELL RD
Subd: 

Type of Work: ELECTRIC, UNDERGROUND
Duration: APPROX 24 HRS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1500527282386, -80.8180871425824
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CGT61 FAE07 SCG83 TWCZ40                             


Map Link: (NEEDS DEVELOPMENT)


