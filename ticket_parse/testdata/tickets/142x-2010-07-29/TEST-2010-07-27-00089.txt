
ZZQ06 72 PUPS Email 07/27/2010 14:48:00 1007270102 Update 

Ticket Number: 1007270102
Old Ticket Number: 
Created By: DBL
Seq Number: 72

Created Date: 07/27/2010 14:49:13
Work Date/Time: 07/30/2010 15:00:29
Update: 08/18/2010 15:00:29 Good Through: 08/23/2010 15:00:29

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 230
Street: EDISTO AVE
Inters St: SENECA ST
Subd: 

Type of Work: DITCH, WIDEN
Duration: 6 MONTHS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9937165433628, -81.0159684754218
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCG02 TWCZ40                                   


Map Link: (NEEDS DEVELOPMENT)


