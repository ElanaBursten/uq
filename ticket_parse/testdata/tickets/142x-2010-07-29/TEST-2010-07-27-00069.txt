
ZZQ06 56 PUPS Email 07/27/2010 13:30:00 1007270079 Survey 

Ticket Number: 1007270079
Old Ticket Number: 
Created By: RMD
Seq Number: 56

Created Date: 07/27/2010 13:31:55
Work Date/Time: 07/30/2010 13:45:16
Update: 08/18/2010 13:45:16 Good Through: 08/23/2010 13:45:16

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 
Street: TEST DR
Inters St: HUNT CLUB RD 
Subd: TEST

Type of Work: TELEPHONE, BURY CABLE
Duration: 1 WEEK 

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0702072557289, -80.9272735618127
Secondary: 34.0621721614611, -80.9193624021608
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ82 SCGZ02 TWCZ40 XXX09                    


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


