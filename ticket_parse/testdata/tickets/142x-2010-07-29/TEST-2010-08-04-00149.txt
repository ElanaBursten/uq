
ZZQ06 100 PUPS Tck Project 08/04/2010 02:17:00 PM 2208040252 Resend 

Ticket Number: 2208040252
Old Ticket Number: 
Created By: JAC
Seq Number: 100

Created Date: 08/04/2010 02:18:47 PM
Work Date/Time: 08/04/2010 02:30:20 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: KERSHAW
Place: ELGIN
Address Number: 1051
Street: OAKLAND DR
Inters St: WHITE POND RD
Subd: WHITE HILLS

Type of Work: FENCE, INSTALL
Duration: UNSURE

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1632816442665, -80.7888725839614
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CPLZ05 FAE07 SCG83 TWCZ40                            


Map Link: (NEEDS DEVELOPMENT)


