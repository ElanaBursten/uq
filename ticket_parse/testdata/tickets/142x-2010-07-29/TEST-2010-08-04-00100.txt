
ZZQ35 55 PUPS Tck Project 08/04/2010 10:21:00 AM 2208040163 Remark 

Ticket Number: 2208040163
Old Ticket Number: 
Created By: JAC
Seq Number: 55

Created Date: 08/04/2010 10:22:03 AM
Work Date/Time: 08/04/2010 10:30:26 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: OCONEE
Place: SENECA
Address Number: 307
Street: LAKE WINDS CT
Inters St: RIDGE POINTE CT
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7594307499674, -82.9402913280562
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 CSLW27 DPCZ25 FTHSZ64                         


Map Link: (NEEDS DEVELOPMENT)


