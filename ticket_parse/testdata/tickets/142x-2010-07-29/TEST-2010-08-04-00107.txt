
ZZQ35 59 PUPS Tck Project 08/04/2010 10:24:00 AM 2208040171 Cancel 

Ticket Number: 2208040171
Old Ticket Number: 
Created By: JAC
Seq Number: 59

Created Date: 08/04/2010 10:24:46 AM
Work Date/Time: 08/04/2010 10:30:30 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: ANDERSON
Place: ANDERSON
Address Number: 227
Street: SHADY LN
Inters St: OLD PEARMAN DAIRY RD
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 14 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.5461777911075, -82.7539210682134
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ04 PETZ81 PNAZ80 WAW10                    


Map Link: (NEEDS DEVELOPMENT)


