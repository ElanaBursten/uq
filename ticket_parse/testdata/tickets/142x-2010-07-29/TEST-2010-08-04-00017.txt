
ZZQ06 30 PUPS Web 08/04/2010 08:14:00 AM 2208040003 Resend 

Ticket Number: 2208040003
Old Ticket Number: 
Created By: JDB
Seq Number: 30

Created Date: 08/04/2010 08:15:24 AM
Work Date/Time: 08/04/2010 08:15:41 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: KERSHAW
Place: LUGOFF
Address Number: 768
Street: WILDWOOD LANE
Inters St: WATTS HILL RD AND WILDWOOD LANE
Subd: 

Type of Work: LANDSCAPE, OTHER
Duration: 2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: DUN RITE CONCRETE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: SGTCHAFFINS@YAHOO.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.2109799036092, -80.7541162403367
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 FAE07 TWCZ40                                         


Map Link: (NEEDS DEVELOPMENT)


