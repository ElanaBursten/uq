
ZZQ06 23 PUPS Web 07/27/2010 08:54:00 1007270049 Update 

Ticket Number: 1007270049
Old Ticket Number: 1007220036
Created By: RMD
Seq Number: 23

Created Date: 07/27/2010 08:54:55
Work Date/Time: 07/30/2010 09:00:53
Update: 08/18/2010 09:00:53 Good Through: 08/23/2010 09:00:53

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 
Street: PENDLETON ST
Inters St: S MAIN ST
Subd: 

Type of Work: SOIL TEST BORING(S)
Duration: APPROX 3 WEEKS

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY // IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME //THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1       


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9989650740345, -81.0337753146399
Secondary: 33.9971024366708, -81.0307497608268
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 COC82 ITC73 LC393 SCA88 SCEJZ40 SCG02 STG10    
TWCZ40 TWTZ26 USC42                                                           


Map Link: (NEEDS DEVELOPMENT)


