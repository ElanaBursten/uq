
ZZQ06 102 PUPS Tck Project 08/04/2010 02:20:00 PM 2208040258 Normal 

Ticket Number: 2208040258
Old Ticket Number: 
Created By: JDB
Seq Number: 102

Created Date: 08/04/2010 02:21:12 PM
Work Date/Time: 08/09/2010 02:30:48 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: BEAUFORT
Place: HILTON HEAD ISLAND
Address Number: 
Street: WILLIAM HILTON PKWY
Inters St: MATHEWS DR & FOLLY FIELD RD
Subd: 

Type of Work: CATV, REPAIR MAIN
Duration: 15 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 32.2018976870951, -80.699173595899
Secondary: 32.201346824895, -80.6990358803489
Lat/Long Caller Supplied: N

Members Involved: HHP61 HRGZ57 PEH51 TWBZ51                                   


Map Link: (NEEDS DEVELOPMENT)


