
ZZQ35 26 PUPS U.S. Mail 08/04/2010 09:10:00 AM 2208040040 Resend 

Ticket Number: 2208040040
Old Ticket Number: 
Created By: JAC
Seq Number: 26

Created Date: 08/04/2010 09:10:29 AM
Work Date/Time: 08/04/2010 09:15:06 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: CHEROKEE
Place: BLACKSBURG
Address Number: 351
Street: CHEROKEE FALLS RD
Inters St: W CHEROKEE ST 
Subd: 

Type of Work: SEE REMARKS
Duration: NOT SURE 

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.0935622061795, -81.5460361213563
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 CGT62 DPCZ60 GPW48 LC393 TBK23 WTGN02 YOR56   


Map Link: (NEEDS DEVELOPMENT)


