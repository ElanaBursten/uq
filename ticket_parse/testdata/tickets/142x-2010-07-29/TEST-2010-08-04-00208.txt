
ZZQ35 105 PUPS Voice 08/04/2010 03:13:00 PM 2208040357 Resend 

Ticket Number: 2208040357
Old Ticket Number: 
Created By: JDB
Seq Number: 105

Created Date: 08/04/2010 03:13:57 PM
Work Date/Time: 08/04/2010 03:15:36 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: ANDERSON
Place: WILLIAMSTON
Address Number: 619
Street: WILLIAMS ST
Inters St: CLEVELAND ST
Subd: 

Type of Work: SEWER, RELOCATE SERVICE LINE
Duration: APPROX. 30 MINS 

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.6374610995517, -82.4883766780147
Secondary: 34.602812291086, -82.4494283823671
Lat/Long Caller Supplied: N

Members Involved: BCH49 BSZT29 CCMZ41 DPCZ02 DPCZ04 FTHWZ65 LAU27 TWP86       


Map Link: (NEEDS DEVELOPMENT)


