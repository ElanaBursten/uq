
ZZQ06 48 PUPS Voice 07/27/2010 08:35:00 1007270004 Resend 

Ticket Number: 1007270004
Old Ticket Number: 1007220034
Created By: RMD
Seq Number: 48

Created Date: 07/27/2010 08:35:41
Work Date/Time: 07/27/2010 08:45:30
Update:                     Good Through:                    

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 310
Street: S EDISTO AVE
Inters St: ROSEWOOD DR
Subd: 

Type of Work: SEWER, TAP RENEWAL
Duration: 4 HOURS

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: CITY OF COLUMBIA WASTEWATER MAINTENANCE

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY // IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME //THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1       


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9867050571098, -81.0123638988386
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEJZ40 SCG02 TWCZ40                           


Map Link: (NEEDS DEVELOPMENT)


