
ZZQ06 17 PUPS Email 07/22/2010 10:56:00 1007220025 Design 

Ticket Number: 1007220025
Old Ticket Number: 
Created By: DBL
Seq Number: 17

Created Date: 07/22/2010 10:58:07
Work Date/Time: 08/05/2010 11:00:30
Update: 08/24/2010 11:00:30 Good Through: 08/27/2010 11:00:30

Excavation Information:
State: SC     County: LEXINGTON
Place: WEST COLUMBIA
Address Number: 377
Street: RIVERCHASE WAY
Inters St: RIVERCHASE CT 
Subd: 

Type of Work: ELECTRIC AND GAS, JOINT TRENCHING
Duration: 4 HRS 

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: CITY OF WEST COLUMBIA 

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0188986166405, -81.1518898545376
Secondary: 34.0131049892817, -81.1370581684992
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CWC18 DIX54 PBT43 SCEDZ05 SCGZ05 TLX24 TWCZ40 TWTZ26 
WINZ08                                                                        


Map Link: (NEEDS DEVELOPMENT)

Design tickets are courtesy tickets.  The utilities will mark the
requested area on a voluntary basis.  Members are to locate within
10 working days, please call requestor if you are unable to locate
within the 10 working days.


