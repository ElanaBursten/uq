
ZZQ06 20 PUPS Email 07/27/2010 08:48:00 1007270036 Update 

Ticket Number: 1007270036
Old Ticket Number: 1007220020
Created By: RMD
Seq Number: 20

Created Date: 07/27/2010 08:49:02
Work Date/Time: 07/30/2010 09:00:59
Update: 08/18/2010 09:00:59 Good Through: 08/23/2010 09:00:59

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 244
Street: GREYSTONE BLVD
Inters St: STONE RIDGE DR
Subd: 

Type of Work: BARN, INSTALL
Duration: 2 DAYS

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: GUARDIAN FENCE SUPPLIERS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0248604547366, -81.0787474041957
Secondary: 34.0124843864429, -81.0756906644364
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 LC393 SCEKZ42 SCGZ05 STG10 TWCZ40 TWTZ26       


Map Link: (NEEDS DEVELOPMENT)


