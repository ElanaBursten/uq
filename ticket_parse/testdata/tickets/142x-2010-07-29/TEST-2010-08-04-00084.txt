
ZZQ06 63 PUPS Email 08/04/2010 10:10:00 AM 2208040140 Design 

Ticket Number: 2208040140
Old Ticket Number: 
Created By: JAC
Seq Number: 63

Created Date: 08/04/2010 10:11:03 AM
Work Date/Time: 08/18/2010 10:15:39 AM
Update: 09/07/2010 Good Through: 09/10/2010

Excavation Information:
State: SC     County: FLORENCE
Place: FLORENCE
Address Number: 
Street: ESTATE RD
Inters St: OLD MARION HWY
Subd: 

Type of Work: ELECTRIC, REPLACE POLE(S)
Duration: 9 hours

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.2144758192771, -79.7327005512905
Secondary: 34.2046049008895, -79.7216878475671
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 SCG73 TWFZ66 VERZ06                           


Map Link: (NEEDS DEVELOPMENT)

Design tickets are courtesy tickets.  The utilities will mark the
requested area on a voluntary basis.  Members are to locate within
10 working days, please call requestor if you are unable to locate
within the 10 working days.


