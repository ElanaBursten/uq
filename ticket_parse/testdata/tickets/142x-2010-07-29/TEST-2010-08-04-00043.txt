
ZZQ35 28 PUPS Voice 08/04/2010 09:19:00 AM 2208040051 Resend 

Ticket Number: 2208040051
Old Ticket Number: 
Created By: JDB
Seq Number: 28

Created Date: 08/04/2010 09:20:12 AM
Work Date/Time: 08/04/2010 09:30:48 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 44
Street: DIXIE CIR
Inters St: LUKE LN
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: UNKNOWN

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: AT&T

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7798477365632, -82.4072898456551
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ02 PNGZ81                                 


Map Link: (NEEDS DEVELOPMENT)


