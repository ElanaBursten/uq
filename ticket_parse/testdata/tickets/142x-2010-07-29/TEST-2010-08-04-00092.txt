
ZZQ35 50 PUPS Web 08/04/2010 10:17:00 AM 2208040154 Cancel 

Ticket Number: 2208040154
Old Ticket Number: 
Created By: JDB
Seq Number: 50

Created Date: 08/04/2010 10:18:20 AM
Work Date/Time: 08/04/2010 10:30:23 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LANCASTER
Place: INDIAN LAND
Address Number: 17014
Street: LAURELMONT CT
Inters St: ARDREY KELL RD
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 2HRS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: GRAMSDELL@CTIS-INC.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7523165407266, -80.8287998854961
Secondary: 34.6977530439076, -80.7407942454655
Lat/Long Caller Supplied: N

Members Involved: CLPW94 DPCZ72 LAN20 LNWS52 LTC28                            


Map Link: (NEEDS DEVELOPMENT)


