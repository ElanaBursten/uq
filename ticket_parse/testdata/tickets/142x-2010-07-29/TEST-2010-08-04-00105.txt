
ZZQ06 71 PUPS U.S. Mail 08/04/2010 10:23:00 AM 2208040169 Survey 

Ticket Number: 2208040169
Old Ticket Number: 
Created By: JAC
Seq Number: 71

Created Date: 08/04/2010 10:24:00 AM
Work Date/Time: 08/09/2010 10:30:49 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: DARLINGTON
Place: HARTSVILLE
Address Number: 2965
Street: DANCE DR
Inters St: N CENTER RD
Subd: 

Type of Work: ELECTRIC, REPLACE POLE(S)
Duration: 7HRS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.3214715665063, -79.9692762879769
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05                                               


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


