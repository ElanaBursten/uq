
ZZQ06 95 PUPS Voice 08/04/2010 02:14:00 PM 2208040241 Update 

Ticket Number: 2208040241
Old Ticket Number: 
Created By: JAC
Seq Number: 95

Created Date: 08/04/2010 02:14:37 PM
Work Date/Time: 08/09/2010 02:15:13 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: SUMTER
Place: SUMTER
Address Number: 
Street: BROAD ST
Inters St: ALICE DR
Subd: 

Type of Work: CATV, INSTALL MAIN
Duration: TWO WEEKS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         
                                                                              


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9567297966664, -80.3878314020714
Secondary: 33.9566899231994, -80.3869940592651
Lat/Long Caller Supplied: N

Members Involved: CPLZ05 FTCZ81 SCG76 TWUZ46 VERZ06                           


Map Link: (NEEDS DEVELOPMENT)


