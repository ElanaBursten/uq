
ZZQ06 71 PUPS Voice 07/27/2010 14:47:00 1007270099 Normal 

Ticket Number: 1007270099
Old Ticket Number: 
Created By: WGS
Seq Number: 71

Created Date: 07/27/2010 14:47:51
Work Date/Time: 07/30/2010 15:00:16
Update: 08/18/2010 15:00:16 Good Through: 08/23/2010 15:00:16

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 328
Street: PEPPERCORN LN
Inters St: SORREL TREE DR
Subd: ALLANS MILL

Type of Work: GAS, INSTALL SERVICE
Duration: 15 DAYS

Boring/Drilling: Y
Blasting: Y
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      
                                                                              


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.083982487199, -80.8905117843022
Secondary: 34.0756228877052, -80.8786323534425
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ82 SCGZ02 SCTDZ01 TWCZ40 XXX09            


Map Link: (NEEDS DEVELOPMENT)


