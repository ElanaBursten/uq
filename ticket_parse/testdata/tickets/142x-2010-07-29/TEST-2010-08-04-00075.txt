
ZZQ06 58 PUPS Voice 08/04/2010 09:56:00 AM 2208040115 Normal 

Ticket Number: 2208040115
Old Ticket Number: 
Created By: JDB
Seq Number: 58

Created Date: 08/04/2010 09:56:53 AM
Work Date/Time: 08/09/2010 10:00:32 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: WILLIAMSBURG
Place: LANE
Address Number: 
Street: BROOMSTRAW RD
Inters St: PARSONS CIR
Subd: 

Type of Work: SEWER, INSTALL MAIN
Duration: 15 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.528122270859, -79.8924261882311
Secondary: 33.5264797929819, -79.889792559911
Lat/Long Caller Supplied: N

Members Involved: CPLZ05 FTCZ80 TWLZ82                                        


Map Link: (NEEDS DEVELOPMENT)


