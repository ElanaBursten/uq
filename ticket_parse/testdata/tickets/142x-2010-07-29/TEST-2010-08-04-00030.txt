
ZZQ35 21 PUPS Web 08/04/2010 08:41:00 AM 2208040032 Update 

Ticket Number: 2208040032
Old Ticket Number: 
Created By: JAC
Seq Number: 21

Created Date: 08/04/2010 08:42:30 AM
Work Date/Time: 08/09/2010 08:45:45 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 
Street: UKNOWN
Inters St: KEOWEE AVE
Subd: IVY GROVE

Type of Work: SEWER, INSTALL MAIN & SERVICE(S)
Duration: 2 MONTHS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: ROPERBROTHERSINC@BELLSOUTH.NET

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8257935209646, -82.406808250725
Secondary: 34.8232231252092, -82.4041721879248
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 CGR12 DPCZ02 PNGZ81                           


Map Link: (NEEDS DEVELOPMENT)


