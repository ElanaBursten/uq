
ZZQ06 6 PUPS Voice 07/22/2010 10:39:00 1007220013 Emergency 

Ticket Number: 1007220013
Old Ticket Number: 
Created By: DBL
Seq Number: 6

Created Date: 07/22/2010 10:40:19
Work Date/Time: 07/22/2010 10:45:23
Update: 08/10/2010 10:45:23 Good Through: 08/13/2010 10:45:23

Excavation Information:
State: SC     County: LEXINGTON
Place: CAYCE
Address Number: 710
Street: MICHAELMAS AVE
Inters St: 7TH ST
Subd: 

Type of Work: GREASE TRAP, INSTALL
Duration: 2-4 HOURS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: SCE&G (GAS)

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9785875739474, -81.0583520537362
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 SCEJZ40 SCGZ05 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


