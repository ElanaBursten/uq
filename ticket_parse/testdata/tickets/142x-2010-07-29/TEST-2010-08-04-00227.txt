
ZZQ06 126 PUPS Voice 08/04/2010 03:37:00 PM 2208040391 Normal 

Ticket Number: 2208040391
Old Ticket Number: 
Created By: JDB
Seq Number: 126

Created Date: 08/04/2010 03:37:43 PM
Work Date/Time: 08/09/2010 03:45:13 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CLARENDON
Place: MANNING
Address Number: 1343
Street: AUBURN DR
Inters St: KINGSTREE HWY  
Subd: 

Type of Work: ELECTRIC, INSTALL SERVICE
Duration: 1/2 DAY

Boring/Drilling: N Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.719775821446, -80.1538348960763
Secondary: 33.6478895654796, -80.0792187822884
Lat/Long Caller Supplied: N

Members Involved: FTCZ80 SECZ05 TWUZ46                                        


Map Link: (NEEDS DEVELOPMENT)


