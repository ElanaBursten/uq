
ZZQ06 123 PUPS Voice 08/04/2010 03:33:00 PM 2208040387 Resend 

Ticket Number: 2208040387
Old Ticket Number: 
Created By: JDB
Seq Number: 123

Created Date: 08/04/2010 03:34:32 PM
Work Date/Time: 08/04/2010 03:45:14 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: CHESTERFIELD
Place: CHERAW
Address Number: 312
Street: SUMMER LN
Inters St: RED HILL RD
Subd: DEERFIELD ESTATES

Type of Work: SEPTIC, INSTALL FIELD LINES
Duration: 1 1/2 TO 2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.6956605220445, -79.9206676059718
Secondary: 34.6362102850427, -79.8718334827203
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 DXP53 MCI18 SCG84 SCGT84 TWYZ52               


Map Link: (NEEDS DEVELOPMENT)


