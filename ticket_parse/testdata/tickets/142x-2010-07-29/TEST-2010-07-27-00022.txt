
ZZQ06 15 PUPS Voice 07/27/2010 08:46:00 1007270030 Remark 

Ticket Number: 1007270030
Old Ticket Number: 1007220015
Created By: RMD
Seq Number: 15

Created Date: 07/27/2010 08:46:26
Work Date/Time: 07/27/2010 09:00:24
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: WEST COLUMBIA
Address Number: 
Street: BRADLEY DR
Inters St: SAINT DAVIDS CHURCH RD 
Subd: 

Type of Work: SOD, LAYING
Duration: APPROX 2 MONTHS

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: SLOAN CONSTRUCTION, INC.

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9709074065379, -81.1607890230415
Secondary: 33.9517769440137, -81.1554419993546
Lat/Long Caller Supplied: N

Members Involved: CWC18 MID55 QWC42 SCEJZ40 SCGZ05 TWCZ40 WINZ08              


Map Link: (NEEDS DEVELOPMENT)


