
ZZQ06 37 PUPS Email 08/04/2010 09:14:00 AM 2208040045 Cancel 

Ticket Number: 2208040045
Old Ticket Number: 
Created By: JAC
Seq Number: 37

Created Date: 08/04/2010 09:14:34 AM
Work Date/Time: 08/04/2010 09:15:24 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: FLORENCE
Place: FLORENCE
Address Number: 1027
Street: E  OLD MARION HWY
Inters St: MALLOY ST
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: APPROX 1 HOUR

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.214228651307, -79.7417299091452
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 SCG73 TWFZ66 VERZ06                           


Map Link: (NEEDS DEVELOPMENT)


