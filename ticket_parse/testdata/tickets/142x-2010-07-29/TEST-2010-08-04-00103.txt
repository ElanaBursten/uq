
ZZQ35 57 PUPS Web 08/04/2010 10:22:00 AM 2208040166 Normal 

Ticket Number: 2208040166
Old Ticket Number: 
Created By: JDB
Seq Number: 57

Created Date: 08/04/2010 10:23:22 AM
Work Date/Time: 08/09/2010 10:30:01 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: GREENVILLE
Place: TRAVELERS REST
Address Number: 20
Street: SACHA LANE
Inters St: JACKSON GROVE RD
Subd: 

Type of Work: SEE REMARKS
Duration: 1 WEEK

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: INFO@RLPCONSTRUCTIONLLC.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9838275047782, -82.3752589296413
Secondary: 34.9783349487156, -82.371001277123
Lat/Long Caller Supplied: N

Members Involved: BRRW22 BSZT29 DPCZ02 PNGZ81                                 


Map Link: (NEEDS DEVELOPMENT)


