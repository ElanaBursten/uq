
ZZQ35 47 PUPS Web 08/04/2010 10:12:00 AM 2208040142 Meet 

Ticket Number: 2208040142
Old Ticket Number: 
Created By: JAC
Seq Number: 47

Created Date: 08/04/2010 10:12:47 AM
Work Date/Time: 08/09/2010 10:15:34 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: ANDERSON
Place: ANDERSON
Address Number: 112
Street: ELLIOTT CIR
Inters St: MARCHBANKS AVE
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 14 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.5282690723408, -82.6288344743469
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 DPCZ04 ECA47 PNAZ80                                  


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


