
ZZQ06 34 PUPS Email 07/27/2010 08:43:00 1007270023 Update 

Ticket Number: 1007270023
Old Ticket Number: 1007220011
Created By: RMD
Seq Number: 34

Created Date: 07/27/2010 08:44:11
Work Date/Time: 07/30/2010 08:45:09
Update: 08/18/2010 08:45:09 Good Through: 08/23/2010 08:45:09

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 217
Street: LATHERTON CT
Inters St: FENDOCK WAY
Subd: 

Type of Work: BURY ANIMAL
Duration: DAY

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: COMMUNICATION SERVICES INC

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9016435624704, -81.2283513923401
Secondary: 33.8980063694517, -81.223979022009
Lat/Long Caller Supplied: N

Members Involved: SCEDZ05 SCGZ05 TWCZ40 WINZ08                                


Map Link: (NEEDS DEVELOPMENT)


