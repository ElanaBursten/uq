
ZZQ35 17 PUPS Voice 08/04/2010 08:29:00 AM 2208040023 Resend 

Ticket Number: 2208040023
Old Ticket Number: 
Created By: JDB
Seq Number: 17

Created Date: 08/04/2010 08:30:51 AM
Work Date/Time: 08/04/2010 08:30:00 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: YORK
Place: FORT MILL
Address Number: 719
Street: DEERBROOK LN
Inters St: TEGA CAY DR
Subd: TEGA CAY

Type of Work: FIBER OPTIC, REPAIR CABLE
Duration: 2 HOURS

Boring/Drilling: N Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.0387187007679, -81.022445940609
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: DPCZ72F FMT20 YOR56                                         


Map Link: (NEEDS DEVELOPMENT)


