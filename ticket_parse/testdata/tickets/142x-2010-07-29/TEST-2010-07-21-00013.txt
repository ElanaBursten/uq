
ZZQ06 42 PUPS Remote 07/21/2010 8:23:10 AM 1007210181 Normal

Ticket Number: 1007210181
Old Ticket Number: 
Created By: R-DLA
Seq Number: 42

Created Date: 07/21/2010 8:23:09 AM
Work Date/Time: 07/26/2010 8:15:37 AM
Update: 08/11/2010 Good Through: 08/16/2010

Excavation Information:
State: SC     County: LEE
Place: LYNCHBURG
Address Number: 49
Street: W WILLOW GROVE HWY
Inters St: GRIFFIN ST
Subd: 

Type of Work: ELECTRIC, POLE(S) REMOVAL
Duration: 4 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: PROGRESS ENERGY

Remarks/Instructions: CHURCH STREET IS ANOTHER STREET IN AREA//MARK 30 FOOT   
RADIUS OF POLE DW36BU TO LEFT OF HOUSE//DIRECTIONS:HWY 341 TO LYNCHBURG       
FROM BISHOPVILLE//RIGHT ON WILLOW GROVE HWY//2 STORY HOUSE ON LEFT//MARKER    
CAME UP WEST OF GRIFFIN ST//WORK LOCATION IS EAST OF GRIFFIN ST SO            
RELOCATED  AREA ON MAP//WORK ORDER 0DDHK DAVIS MCCLAM                         

Caller Information: 
Name: DONNA ATKINSON                        PROGRESS ENERGY                       
Address: 1001 RAILROAD AVE
City: HARTSVILLE State: SC Zip: 29110
Phone: (843) 383-3561 Ext:  Type: Business
Fax: (843) 383-3566 Caller Email: 

Contact Information:
Contact:DONNA ATKINSON Email: DONNA.ATKINSON@PGNMAIL.COM
Call Back: Fax: 

Grids: 
Lat/Long: 34.0594587337647, -80.0728610959752
Secondary: 34.0593060391538, -80.0725916348972
Lat/Long Caller Supplied: N 

Members Involved: ATT09 CPLZ05 FTCZ81 TWFZ66                                  


Map Link: (NEEDS DEVELOPMENT)


