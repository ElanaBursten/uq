
BEC03  00002 NCOCc 06/21/10 09:49:08 C101720278-00C NORM NEW GRID LR

North Carolina 811

Ticket : C101720278 Date: 06/21/10 Time: 09:47 Oper: LS2891 Chan:WEB
Old Tkt: C101720278 Date: 06/21/10 Time: 09:48 Oper: LS2891 Rev :00C

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address : 755
Street  : HAVENWOOD LN   Intersection: N
Cross 1 : JOHNSONVILLE RD
Location: LOCATE BY 06/24/10 12:01 AM
LOCATE FROM TERMINAL ADDRESS TO AND INCLUDE ENTIRE PROPERTY//TA1001095
:
Grids   : 3459A8416C    3459A8416D    3500D8416C    3500D8416D

Work type:BURY PHONE DROP
Work date:06/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: ONE DAY  Done for: AT&T

Company : AT&T  Type: UTIL
Co addr : 4500 PINNACLE LN
City    : CHATTANOOGA State: TN Zip: 37415
Caller  : LEATTRICE STRICKLAND Phone: 423-875-9514
Contact : MIKE BROWN Phone: 615-848-0843
BestTime: 615-848-0843

Submitted date: 06/21/10 Time: 09:48
Members: BEC03  BRM01  SCB01*

View map at:
http://newtinc.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=C10172027800C&OPR=utolZPG3nTG2dICv
