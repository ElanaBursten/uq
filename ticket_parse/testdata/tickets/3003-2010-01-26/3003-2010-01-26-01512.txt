
JCK72  01523 GAUPC 01/26/10 16:00:02 01260-301-243-000 NORMAL
Underground Notification             
Notice : 01260-301-243 Date: 01/26/10  Time: 15:56  Revision: 000 

State : GA County: BARROW        Place: WINDER                                  
Addr  : From: 337    To:        Name:    HOLSENBECK SCHOOL              RD      
Near  : Name:    DUNAHOO                        RD  

Subdivision:                                         
Locate:  FRONT OF PROPERTY                                                     

Grids       : 3400A8340A 3400A8341D 3400B8340A 3400B8341D 
Work type   : INSTALLING POWER SERVICE                                            

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 HOURS    Priority: 3
Done for  : JACKSON EMC                             
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : IF QUESTIONS CALL ROB EDWARDS 706-367-6157                          
        : *** DrivingInstructions : LOCATE FROM EMC RISER POLE AT STREET TO NEW SHOP BUILDING.  INSTALLI
        : NG URD SVC FROM POLE TO NEW SERVICE ENTRANCE                        

Company : JACKSON EMC                               Type: MEMB                
Co addr : 850 COMMERCE HWY                         
City    : JEFFERSON                       State   : GA Zip: 30549              
Caller  : JANET DAVIS                     Phone   :  706-367-6170              
Fax     :  706-367-6492                   Alt. Ph.:                            
Email   : JANETDAVIS@JACKSONEMC.COM                                           
Contact :                                                           

Submitted date: 01/26/10  Time: 15:56  Oper: 165
Mbrs : BAR01 BENCH1 GAUPC GP240 JCK72 WND50 WND51 WSMONR 
-------------------------------------------------------------------------------


