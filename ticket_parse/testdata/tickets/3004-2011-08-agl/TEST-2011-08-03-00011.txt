
AGL108  01635 GAUPC 01/26/11 16:36:20 01261-267-031-000 NORMAL
Underground Notification             
Notice : 01261-267-031 Date: 01/26/11  Time: 16:33  Revision: 000 

State : GA County: CLAYTON       Place: HAMPTON                                 
Addr  : From: 10882  To:        Name:    TARA                           BLVD    
Near  : Name:    MCDONOUGH                      RD  

Subdivision:                                         
Locate:  ENTIRE PROPERTY AND WITHIN RIGHT OF WAY                               

Grids       : 3326A8419B 3326A8419C 3327D8419B 3327D8419C 
Work type   : INSTL WTR MAIN                                                      

Start date: 02/01/11 Time: 07:00 Hrs notc : 000
Legal day : 02/01/11 Time: 07:00 Good thru: 02/17/11 Restake by: 02/14/11
RespondBy : 01/31/11 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : FAMILY DOLLAR                           
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks : THIS IS AT A THE FAMILY DOLLAR                                      

Company : DIXON BACKHOE SERVICE                     Type: CONT                
Co addr : 12095 PANHANDLE RD                       
City    : HAMPTON                         State   : GA Zip: 30250              
Caller  : STEVEN DIXON                    Phone   :  770-318-9357              
Fax     :                                 Alt. Ph.:  770-286-3934              
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/11  Time: 16:33  Oper: 215
Mbrs : AGL108 ATT02 BSCA CHC01 COMPER CTD33 CTD35 CWA01 CWA02 GAUPC 
     : GP145 QWEST8 
-------------------------------------------------------------------------------
AGL108-HP : Places of Assembly	LOVEJOY
AGL108-HP : Places of Assembly	Lovejoy Soccer Complex
