
AGL104  01633 GAUPC 01/26/11 16:36:14 01261-301-392-000 NORMAL RESTAK
Underground Notification             
Notice : 01261-301-392 Date: 01/26/11  Time: 16:33  Revision: 000 
Old Tkt: 01071-301-315 Date: 01/07/11  Time: 16:55     

State : GA County: RICHMOND      Place: AUGUSTA                                 
Addr  : From:        To:        Name:    MORGAN                         RD      
Near  : Name:    TOBACCO                        RD  

Subdivision:                                         
Locate: GO 200FT N OF TOBACCO RD, LOC THE R/O/W ON B/S OF THE RD GOING NE FOR 1
      :  MILE                                                                   

Grids       : 3323A8205A 3323B8205A 3323B8206D 3323C8205A 3323C8206D 
Grids       : 3323D8205A 3323D8206D 
Work type   : HIGHWAY CONSTRUCTION                                                

Start date: 02/01/11 Time: 07:00 Hrs notc : 000
Legal day : 02/01/11 Time: 07:00 Good thru: 02/17/11 Restake by: 02/14/11
RespondBy : 01/31/11 Time: 23:59 Duration : 2 MONTHS   Priority: 3
Done for  : RICHMOND COUNTY                         
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : 2ND REQ TO AUG50    BSEG    JEF70    JIC01   TO REMARK   MARKING WER
        : E WASHED AWAY  CREW ON SITE    CONTACT:  DALE MCGEE   706-533-0543  
        : PREV TKT 01210-300-245  PREVIOUS TICKET NUMBER:02100-257-002  PREVIO
        : US TICKET NUMBER:02260-214-013  PREVIOUS TICKET NUMBER:03160-300-253
        : PREVIOUS TICKET NUMBER:03310-300-287  PREVIOUS TICKET NUMBER:04160-3
        : 00-960  *** THIS IS AN ADDITIONAL REQUEST FOR AUG50:AUGUSTA / RICHMO
        : ND COUNTY WATER, BSEG:ATT / D, JEF70:JEFFERSON ENERGY, JIC01:COMCAST
        : CABLEVISION OF GEORGIA . PLEASE LOCATE ASAP.  PREVIOUS TICKET NUMBER
        : :05040-300-397  PREVIOUS TICKET NUMBER:05200-300-269  PREVIOUS TICKE
        : T NUMBER:06070-301-343  PREVIOUS TICKET NUMBER:06230-300-761  PREVIO
        : US TICKET NUMBER:07090-301-181  PREVIOUS TICKET NUMBER:08130-300-430
        : PREVIOUS TICKET NUMBER:08310-301-567  PREVIOUS TICKET NUMBER:09160-3
        : 01-436  PREVIOUS TICKET NUMBER:10040-301-555  PREVIOUS TICKET NUMBER
        : :10200-301-614  PREVIOUS TICKET NUMBER:11050-300-869  PREVIOUS TICKE
        : T NUMBER:11220-302-345  PREVIOUS TICKET NUMBER:12080-301-544  PREVIO
        : US TICKET NUMBER:12220-301-371  PREVIOUS TICKET NUMBER:01071-301-315

Company : MABUS BROTHERS CONSTRUCTION               Type: CONT                
Co addr : 920 MOLLY POND RD                        
City    : AUGUSTA                         State   : GA Zip: 30901              
Caller  : KEVIN BALDWIN                   Phone   :  706-722-8941              
Fax     :                                 Alt. Ph.:  706-722-8941              
Email   :                                                                     
Contact : IVAN FRANZ                      706-220-8422              

Submitted date: 01/26/11  Time: 16:33  Oper: 206
Mbrs : AGL104 AUG50 AUG52 BSEG GAUPC GP381 JEF70 JIC01 KNOL2 
-------------------------------------------------------------------------------
AGL104-HP : Schools	MORGAN ROAD MIDDLE SCHOOL
