
AGL118  01670 GAUPC 01/07/11 16:48:21 01071-301-305-000 NORMAL
Underground Notification             
Notice : 01071-301-305 Date: 01/07/11  Time: 16:45  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 1686   To:        Name:    THIRD                          ST      
Near  : Name:    WOOD                           ST  

Subdivision:                                         
Locate:  RIGHT OF WAY ON BOTH SIDES OF ROAD                                    

Grids       : 3249C8338C 3249D8338C 
Work type   : TERMINATE WATER SERVICE                                             

Start date: 01/13/11 Time: 07:00 Hrs notc : 000
Legal day : 01/13/11 Time: 07:00 Good thru: 01/31/11 Restake by: 01/26/11
RespondBy : 01/12/11 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : MACON WATER AUTHORITY                   
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : SERVICE IS LOCATED IN DIRT PARKING LOT NEXT TO 1678 THIRD STREET CAL
        : L TERRY RICE @ 478-256-9400 IF ADDITIONAL INFO IS NEEDED            
        : *** WILL BORE Road                          

Company : MACON WATER AUTHORITY                     Type: MEMB                
Co addr : 790 SECOND ST.                           
City    : MACON                           State   : GA Zip: 31202              
Caller  : YVETTE LOVE                     Phone   :  478-447-2197              
Fax     :  478-745-9531                   Alt. Ph.:                            
Email   : YLOVE@MACONWATER.ORG                                                
Contact :                                                           

Submitted date: 01/07/11  Time: 16:45  Oper: 150
Mbrs : AGL118 BGAWM CCM01 GAUPC GP501 MAC01 MAC03 MWA01 MWA02 QWEST8 
     : 
-------------------------------------------------------------------------------
AGL118-HP : Bare Steel	1 1/4-34-S
AGL118-HP : Bare Steel	2-34-S
AGL118-HP : Bare Steel	4-2-S
AGL118-HP : Bare Steel	4-34-S
AGL118-HP : Bare Steel	6-34-S
AGL118-HP : Cast Iron	4-34-I
AGL118-HP : Places of Assembly	SECOND BAPTIST CHURCH
AGL118-HP : Schools	BURKE ELEMENTARY SCHOOL
AGL118-HP : Schools	INGRAM SCHOOL
