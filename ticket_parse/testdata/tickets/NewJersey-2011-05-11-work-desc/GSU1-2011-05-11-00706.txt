
New Jersey One Call System        SEQUENCE NUMBER 0032    CDC = GSC 

Transmit:  Date: 05/11/11   At: 10:00 

*** R O U T I N E         *** Request No.: 111310609 

Operators Notified: 
BAN     = VERIZON                       GPE     = JERSEY CENTRAL POWER & LI      
GSC     = COMCAST CABLEVISION OF GA     ISP     = INTERSTATE STORAGE & PIPE      
P34     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    05/17/11   At 08:00  Expiration Date: 07/15/11 

Location Information: 
County: BURLINGTON             Municipality: NEW HANOVER 
Subdivision/Community: FORT DIX 
Street:               5619 DOUGHBOY LOOP 
Nearest Intersection: W 8TH ST 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL ANCHOR(S) 
Block:                Lot:                Depth: 6FT 
Extent of Work: 15FT RADIUS OF POLES
  JC63462NH,JC63463NH,JC63467NH,JC63468NH. 
Remarks:  
  Working For Contact:  SHARRON BARNES 

Working For: JERSEY CENTRAL POWER & LIGHT 
Address:     PO BOX 229 
City:        WRIGHTSTOWN, NJ  08562 
Phone:       732-450-5572   Ext:  

Excavator Information: 
Caller:      SHARRON BARNES 
Phone:       732-450-5572   Ext:  

Excavator:   JERSEY CENTRAL POWER & LIGHT 
Address:     PO BOX 229 
City:        WRIGHTSTOWN, NJ  08562 
Phone:       732-450-5572   Ext:          Fax:  609-758-3788 
Cellular:    732-450-5572 
Email:       sbarnes@firstenergycorp.com 
End Request 
