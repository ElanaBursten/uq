
New Jersey One Call System        SEQUENCE NUMBER 0014    CDC = CC3 

Transmit:  Date: 05/11/11   At: 10:00 

*** R O U T I N E         *** Request No.: 111310630 

Operators Notified: 
BAN     = VERIZON                       CC3     = COMCAST CABLEVISION OF CE      
GPS     = JERSEY CENTRAL POWER & LI     MOT     = MONROE TOWNSHIP UTIL. DEP      
NN3     = NEW JERSEY NATURAL GAS CO     P29     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    05/17/11   At 08:00  Expiration Date: 07/15/11 

Location Information: 
County: MIDDLESEX              Municipality: MONROE 
Subdivision/Community:  
Street:               17 GLORIA LANE 
Nearest Intersection: MATCHAPONIX AVE 
Other Intersection:    
Lat/Lon: 
Type of Work: WELL DRILLING 
Block: 76             Lot:  13.16         Depth: 100FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  DAN WANG 

Working For: ROYAL REALTY, LLC 
Address:     109 WHITE OAK LANE 
City:        OLD BRIDGE, NJ  08857 
Phone:       732-607-3911   Ext:  

Excavator Information: 
Caller:      BARBARA KRAWCHUK 
Phone:       732-938-5300   Ext:  

Excavator:   PICKWICK WELL DRILLING 
Address:     10 WATER ST 
City:        FARMINGDALE, NJ  07727 
Phone:       732-938-5300   Ext:          Fax:  732-938-3060 
Cellular:     
Email:        
End Request 
