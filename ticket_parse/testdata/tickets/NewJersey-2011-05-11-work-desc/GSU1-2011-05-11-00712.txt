
New Jersey One Call System        SEQUENCE NUMBER 0126    CDC = NJN 

Transmit:  Date: 05/11/11   At: 10:01 

*** R O U T I N E         *** Request No.: 111310595 

Operators Notified: 
BAN     = VERIZON                       GPC     = JERSEY CENTRAL POWER & LI      
NJN     = NEW JERSEY NATURAL GAS CO     SPB     = BOROUGH OF SEASIDE PARK        

Start Date/Time:    05/17/11   At 00:15  Expiration Date: 07/15/11 

Location Information: 
County: OCEAN                  Municipality: SEASIDE PARK 
Subdivision/Community:  
Street:               0 C ST 
Nearest Intersection: SE CENTRAL AVE 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL SIGN(S) 
Block:                Lot:                Depth: 4FT 
Extent of Work: 2 AREA(S) MARKED IN WHITE.  WHITE MARKS ARE LABELED #20,
  #21.  WHITE MARKS LOCATED 31FT E FROM C/L OF INTERSECTION ON BOTH SIDES
  OF ROAD. 
Remarks:  
  Working For Contact:  DAVE DECKER 

Working For: OCEAN COUNTY ENGINEERING 
Address:     141 MAPLETREE RD 
City:        TOMS RIVER, NJ  08753 
Phone:       732-349-8165   Ext:  

Excavator Information: 
Caller:      DAVE DECKER 
Phone:       732-349-8165   Ext: 4584 

Excavator:   OCEAN COUNTY ENGINEERING 
Address:     141 MAPLETREE ROAD 
City:        TOMS RIVER, NJ  08753 
Phone:       732-349-8165   Ext: 4584     Fax:  732-349-4734 
Cellular:    732-575-6128 
Email:       ocengsignshop@co.ocean.nj.us 
End Request 
