
New Jersey One Call System        SEQUENCE NUMBER 0096    CDC = SJG 

Transmit:  Date: 05/11/11   At: 10:01 

*** R O U T I N E         *** Request No.: 111310585 

Operators Notified: 
AE1     = ATLANTIC CITY ELECTRIC        BAN     = VERIZON                        
BUK     = BUCKEYE PARTNERS              CPC     = COLONIAL PIPELINE COMPANY      
PAU     = BOROUGH OF PAULSBORO          SJG     = SOUTH JERSEY GAS COMPANY       
SU2     = SUNOCO PIPELINE                

Start Date/Time:    05/17/11   At 00:15  Expiration Date: 07/15/11 

Location Information: 
County: GLOUCESTER             Municipality: PAULSBORO 
Subdivision/Community:  
Street:               329 BILLINGSPORT ROAD 
Nearest Intersection: QUEEN ST 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL GAS SERVICE 
Block: 44             Lot:  23            Depth: 4FT 
Extent of Work: CURB TO CURB.  CURB TO ENTIRE PROPERTY.  CURB TO 30FT BEHIND
  OPPOSITE CURB. 
Remarks:  
  Working For Contact:  DINK MACNIEL 

Working For: SOUTH JERSEY GAS 
Address:     142 S MAIN ST 
City:        GLASSBORO, NJ  08028 
Phone:       856-881-7000   Ext:  

Excavator Information: 
Caller:      SIOBHAN GILLS 
Phone:       856-694-9200   Ext: 111 

Excavator:   CROWN PIPELINE CONSTRUCTION CO 
Address:     3345 DELSEA DR 
City:        FRANKLINVILLE, NJ  08322 
Phone:       856-694-9200   Ext: 111      Fax:  856-694-9201 
Cellular:    856-694-9200 
Email:       sgills@crownpipeline.com 
End Request 
