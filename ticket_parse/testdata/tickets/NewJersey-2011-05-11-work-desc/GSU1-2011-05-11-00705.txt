
New Jersey One Call System        SEQUENCE NUMBER 0031    CDC = GSC 

Transmit:  Date: 05/11/11   At: 10:00 

*** R O U T I N E         *** Request No.: 111310596 

Operators Notified: 
BAN     = VERIZON                       BUR     = BURLINGTON COUNTY              
DTS     = G4S TECHNOLOGY                GSC     = COMCAST CABLEVISION OF GA      
GSF     = COMCAST CABLEVISION OF GA     MHM     = MOUNT HOLLY MUNICIPAL UTI      
MOO     = MOORESTOWN TOWNSHIP           P34     = PUBLIC SERVICE ELECTRIC &      
TGP     = TRANSCONTINENTAL GAS PIPE      

Start Date/Time:    05/17/11   At 07:00  Expiration Date: 07/15/11 

Location Information: 
County: BURLINGTON             Municipality: MOORESTOWN 
Subdivision/Community: BURLINGTON COUNTY AGRICULTURAL CENTER 
Street:               500 CENTERTON ROAD 
Nearest Intersection: HARTFORD ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL SERVICE UTILITIES 
Block: 8801           Lot:  3             Depth: 6FT 
Extent of Work: CURB TO CURB.  CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  MARY RODDIE 

Working For: COUNTY OF BURLINGTON 
Address:     49 RANCOCAS ROAD 
City:        MOUNT HOLLY, NJ  08060 
Phone:       856-642-3850   Ext:  

Excavator Information: 
Caller:      HOWARD J MANN 
Phone:       609-239-8000   Ext:  

Excavator:   EAGLE CONSTRUCTION SERVICES IN 
Address:     1624 JACKSONVILLE ROAD 
City:        BURLINGTON, NJ  08016 
Phone:       609-239-8000   Ext:          Fax:  609-239-8008 
Cellular:    609-309-1888 
Email:       HJMANN@EAGLE1CONSTRUCTION.COM 
End Request 
