
New Jersey One Call System        SEQUENCE NUMBER 0047    CDC = CAM 

Transmit:  Date: 05/11/11   At: 10:02 

*** R O U T I N E         *** Request No.: 111310643 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
GPE     = JERSEY CENTRAL POWER & LI     JTM     = JACKSON TOWNSHIP M.U.A.        
NJN     = NEW JERSEY NATURAL GAS CO     OCE     = OCEAN COUNTY ENGINEERING       

Start Date/Time:    05/17/11   At 08:00  Expiration Date: 07/15/11 

Location Information: 
County: OCEAN                  Municipality: JACKSON 
Subdivision/Community:  
Street:               351 FREEHOLD ROAD 
Nearest Intersection: LEESVILLE ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL SPRINKLER SYSTEM 
Block:                Lot:                Depth: 1FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  ROBERT WHITE 

Working For: ROBERT WHITE 
Address:     351 FREEHOLD ROAD 
City:        JACKSON, NJ  08527 
Phone:       732-233-5404   Ext:  

Excavator Information: 
Caller:      DOT SCHRECK 
Phone:       732-681-1453   Ext:  

Excavator:   CYMRU LAWN SPRINKLERS 
Address:     1540 STATE HWY 138, SUITE 301 
City:        WALL, NJ  07719 
Phone:       732-681-1453   Ext:          Fax:  732-681-2135 
Cellular:    732-239-9227 
Email:       dots9647@aol.com 
End Request 
