
New Jersey One Call System        SEQUENCE NUMBER 0033    CDC = GSC 

Transmit:  Date: 05/11/11   At: 10:01 

*** R O U T I N E         *** Request No.: 111310615 

Operators Notified: 
BAN     = VERIZON                       DTS     = G4S TECHNOLOGY                 
GSC     = COMCAST CABLEVISION OF GA     GSF     = COMCAST CABLEVISION OF GA      
MSU     = TOWNSHIP OF MAPLE SHADE       P34     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    05/17/11   At 07:00  Expiration Date: 07/15/11 

Location Information: 
County: BURLINGTON             Municipality: MAPLE SHADE 
Subdivision/Community:  
Street:               531 STATE RTE 38 
Nearest Intersection: STATE RTE 73 
Other Intersection:    
Lat/Lon: 
Type of Work: DEMOLITION 
Block:                Lot:                Depth: 4FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  CHUCK KIMBLE 

Working For: QUALITY INN 
Address:     531 STATE RTE 38 W 
City:        MAPLE SHADE, NJ  08052 
Phone:       609-209-1629   Ext:  

Excavator Information: 
Caller:      APRIL FINNEGAN 
Phone:       856-232-8521   Ext:  

Excavator:   FOLCHER ASSOCIATES INC. 
Address:     146 BLACKWOOD BARNSBORO RD 
City:        SEWELL, NJ  08080 
Phone:       856-232-8521   Ext:          Fax:  856-232-2070 
Cellular:    609-929-2377 
Email:       ffolcher@aol.com 
End Request 
