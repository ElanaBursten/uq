
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085308                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       CEMC              
Hours Notice:  72                          Seq Num:        82                
Prepared By:   Maureen.babbs               Taken Date:     01/08/13 08:24    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     SCHOOLS SALES AND SERVICE   Excavator Phone:(615) 452-9250    
Address:       1023 S WATER ST             Caller:         GLENN SANDERS     
City, St, Zip: GALLITINE, TN 37066         Caller Phone:   (615) 452-9250    
Contact Fax:   (615) 452-9251              Contact:        GLENN SANDERS     
Contact Email:                             Contact Phone:  (615) 419-4500    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        ROBERTSON                   Update Date:    01/23/13 AT 00:00 
Place:         SPRINGFIELD                 Expire Date:    01/26/13 AT 00:00 

Address:       3746 HWY 49 W                                         
Intersection:  OLD COOPERTOWN RD                                     

Latitude:      36.437499                   Longitude:      -86.969155        
Secondary Lat: 36.438297                   Secondary Long: -86.968147        

Work Type:     PLAYGROUND EQUIPMENT, INSTL Explosives: No       WhitePaint: Yes      
Done For:      COOPERTOWN ELEM SCHOOL      Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROP IS AT INTER                                                              

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
FACING THE PROP MARK ON THE LEFT SIDE OF PROP BETWEEN BUILDING AND OLD        
PLAYGROUND AT WHITE PAINT... MARK IN WHITE PAINT SAYING DIG                   

--------------------------------------------------------------------------------
Grids:   [113I ] [113P ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
SCG             Springfield Gas System                            No                  
SWW             Springfield Water/Wastewater System               No                  
CEMC            Cumberland Electric Membership Coop               No                  


