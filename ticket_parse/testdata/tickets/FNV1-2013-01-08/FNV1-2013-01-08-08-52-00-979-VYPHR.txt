
                  ========//  TNOCS LOCATE REQUEST  //======== Emergency =====

TICKET NUMBER: 130085113                   OLD TICKET NUM:                   

Message Type:  Emergency                   For Code:       INTDI             
Hours Notice:  0                           Seq Num:        31                
Prepared By:   Judy.deidiker               Taken Date:     01/08/13 07:28    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     CITY OF WAVERLY             Excavator Phone:(931) 296-1410    
Address:       PO BOX 70                   Caller:         GARY DAVIS        
City, St, Zip: WAVERLY, TN 37185           Caller Phone:   (931) 296-1410    
Contact Fax:                               Contact:        GARY DAVIS        
Contact Email:                             Contact Phone:  (931) 209-8086    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/08/13 AT 07:30 
County:        HUMPHREYS                   Update Date:    01/17/13 AT 00:00 
Place:         WAVERLY                     Expire Date:    01/23/13 AT 00:00 

Address:       CHAMBERS SPRING RD                                    
Intersection:  E MAIN ST                                             

Latitude:      36.081219                   Longitude:      -87.77555         
Secondary Lat: 36.082587                   Secondary Long: -87.774938        

Work Type:     WATER LINE, REPAIR          Explosives: No       WhitePaint: Yes      
Done For:      CITY OF WAVERLY             Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER GO S 300FT...                                                      

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
EMERGENCY - EACH UTIL IS REQUIRED BY LAW TO MARK U/G FACILITIES WITHIN 2 HOURS
OF NOTIFICATION...CREW IS EN ROUTE...MARK ON BOTH SIDES OF THE RD AT THIS     
LOCATION...AREA MARKED IN WHITE PAINT...                                      

--------------------------------------------------------------------------------
Grids:   [64F  ] [64K  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
HYC             Humphreys County Utility                          No                  
US              Sprint - US                                       No                  
WAV             Waverly, City of                                  No                  
INTDI           Comcast - Dickson (utiliquest) - INTDI            No                  
KDL             Windstream KDL                                    No                  


