
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080015                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMFR            
Hours Notice:  72                          Seq Num:        15                
Prepared By:   gr.rita.c                   Taken Date:     01/08/13 06:49    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AT&T                        Excavator Phone:(423) 875-4346    
Address:       4500 PINNACLE LN            Caller:         RITA COOK         
City, St, Zip: CHATTANOOGA, TN 37415       Caller Phone:   (423) 875-2514    
Contact Fax:   (423) 875-2727              Contact:        RITA COOK         
Contact Email:                             Contact Phone:  (423) 875-2514    
Call Back:     423 875-2514                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:00 
County:        WILLIAMSON                  Update Date:                      
Place:         FRANKLIN                    Expire Date:                      

Address:       174 AZALEA LN                                         
Intersection:  DENBY CT                                              

Latitude:      35.8658509693177            Longitude:      -86.8622276027073 
Secondary Lat: 0                           Secondary Long: 0                 

Work Type:     TELEPHONE DROPS, BURYING    Explosives: No       WhitePaint: No       
Done For:      AT&T UTILITY OPERATIONS     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROPERTY AT ADDRESS AND BOTH SIDES OF ROAD ADJACENT TO ADDRESS.   
ALSO MARK PROPERTY ACROSS ROAD 100 FT IN EACH DIRECTION... ATT JOB            
TH1300045..BORE                                                               

--------------------------------------------------------------------------------
Grids:   [105M ] [105N ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
HBT             HB and TS Utility Dist                            No                  
MTEMFR          Middle Tenn Electric Membership Coop - Franklin...No                  
U09             Atmos Energy (United Cities Gas) - Franklin - U...No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  


