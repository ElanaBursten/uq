
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080013                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMFR            
Hours Notice:  72                          Seq Num:        14                
Prepared By:   gr.rita.c                   Taken Date:     01/08/13 06:46    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AT&T                        Excavator Phone:(423) 875-4346    
Address:       4500 PINNACLE LN            Caller:         RITA COOK         
City, St, Zip: CHATTANOOGA, TN 37415       Caller Phone:   (423) 875-2514    
Contact Fax:   (423) 875-2727              Contact:        RITA COOK         
Contact Email:                             Contact Phone:  (423) 875-2514    
Call Back:     423 875-2514                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 06:45 
County:        WILLIAMSON                  Update Date:                      
Place:         FRANKLIN                    Expire Date:                      

Address:       429 CALDWELL CT                                       
Intersection:  GROVE LN                                              

Latitude:      35.9014279771195            Longitude:      -86.8620221603944 
Secondary Lat: 0                           Secondary Long: 0                 

Work Type:     TELEPHONE DROPS, BURYING    Explosives: No       WhitePaint: No       
Done For:      AT&T UTILITY OPERATIONS     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROPERTY AT ADDRESS AND BOTH SIDES OF ROAD ADJACENT TO ADDRESS.   
ALSO MARK PROPERTY ACROSS ROAD 100 FT IN EACH DIRECTION... ATT JOB TH1300043  

--------------------------------------------------------------------------------
Grids:   [90C  ] [90D  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
MTEMFR          Middle Tenn Electric Membership Coop - Franklin...No                  
U09             Atmos Energy (United Cities Gas) - Franklin - U...No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  


