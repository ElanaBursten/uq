
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085100                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMMU/INTRU      
Hours Notice:  72                          Seq Num:        28                
Prepared By:   Stacy.rainey                Taken Date:     01/08/13 07:21    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     TOWN OF SMYRNA              Excavator Phone:(615) 459-9730    
Address:       315 S LOWRY ST              Caller:         JOE SARTINO       
City, St, Zip: SMYRNA, TN 37167            Caller Phone:   (615) 459-9730    
Contact Fax:                               Contact:        JOE SARTINO       
Contact Email: joe.sartino@townofsmyrna.orgContact Phone:  (615) 459-9730    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:30 
County:        RUTHERFORD                  Update Date:    01/23/13 AT 00:00 
Place:         SMYRNA                      Expire Date:    01/26/13 AT 00:00 

Address:       CLEARVIEW DR                                          
Intersection:  HAGER DR                                              

Latitude:      35.968084                   Longitude:      -86.51372         
Secondary Lat: 35.968084                   Secondary Long: -86.51372         

Work Type:     SIGNS, INSTL                Explosives: No       WhitePaint: Yes      
Done For:      TOWN OF SMYRNA              Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
LOCATION IS AT INTER                                                          

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK 10FT RADIUS OF WHITE PAINT AT EXISTING STOP SIGN AT ABOVE INTER          

--------------------------------------------------------------------------------
Grids:   [34A  ] [34H  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
MTEMMU          Middle Tenn Electric Membership Coop - Murfrees...No                  
SMU             Smyrna, Town of                                   No                  
INTRU           Comcast- Rutherford - INTRU                       No                  


