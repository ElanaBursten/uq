
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085370                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        94                
Prepared By:   Craig.ingram                Taken Date:     01/08/13 08:44    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AAA WATER HEATER & PLUMBING SERVICESExcavator Phone:(615) 977-0761    
Address:       107 PEBBLE CREEK DR         Caller:         BRANDON SPARKS    
City, St, Zip: HENDERSONVILLE, TN 37075    Caller Phone:   (615) 977-0761    
Contact Fax:                               Contact:        BRANDON SPARKS    
Contact Email:                             Contact Phone:  (615) 977-0761    
Call Back:     (615) 977-0761                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:45 
County:        SUMNER                      Update Date:    01/23/13 AT 00:00 
Place:         GOODLETTSVILLE              Expire Date:    01/26/13 AT 00:00 

Address:       512 LONG HOLLOW PIKE                                  
Intersection:  LORETTA DR                                            

Latitude:      36.332062                   Longitude:      -86.695709        
Secondary Lat: 36.332062                   Secondary Long: -86.695709        

Work Type:     WATER LINE, REPAIR          Explosives: No       WhitePaint: No       
Done For:      TREY CRESTMAN               Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROP IS APPX 150YDS NE OF THE INTER                                           

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE FRONT OF PROP                                                     

--------------------------------------------------------------------------------
Grids:   [143J ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
GDL             Goodlettsville Sewer Dept                         No                  
HVD             Hendersonville Utility Dist                       No                  
WHU             White House Utility District                      No                  


