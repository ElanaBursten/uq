
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085240                   OLD TICKET NUM: 123490930         

Message Type:  Normal                      For Code:       MTEMLE/INTWI      
Hours Notice:  72                          Seq Num:        65                
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 08:05    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     SITE TECH                   Excavator Phone:(615) 207-5375    
Address:       PO BOX 261                  Caller:         RICK JEWELL       
City, St, Zip: CHARLOTTE, TN 37036         Caller Phone:   (615) 207-5375    
Contact Fax:   (615) 763-2811              Contact:        RICK JEWELL       
Contact Email:                             Contact Phone:  (615) 207-5375    
Call Back:     615 207-5375                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        WILSON                      Update Date:    01/23/13 AT 00:00 
Place:         MOUNT JULIET                Expire Date:    01/26/13 AT 00:00 

Address:       E DIVISION ST                                         
Intersection:  N MOUNT JULIET RD                                     

Latitude:      36.198273                   Longitude:      -86.518972        
Secondary Lat: 36.199632                   Secondary Long: -86.516297        

Work Type:     WATER & SEWER, INSTL        Explosives: Yes      WhitePaint: Yes      
Done For:      WR NEWMAN CONTRACTORS       Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
**EXPLOSIVES BEING USED***. ALSO STORM DRAINAGE INSTL AND SITE PREP--FROM     
INTER MARK SE FOR 900FT ON BOTH SIDES OF RD.      ...UPDATE / REMARK PER RICK 
JEWELL TKT 123490930                                                          

--------------------------------------------------------------------------------
Grids:   [72I  ] [72P  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
MTEMLE          Middle Tenn Electric Membership Coop - Lebanon ...No                  
TT1             Tennessee Telephone - Mt Juliet - TT1             No                  
US              Sprint - US                                       No                  
WWILS           West Wilson Utility District                      No                  
INTWI           Comcast - Wilson - INTWI                          No                  
MTJUL           Mt Juliet, City of                                No                  


