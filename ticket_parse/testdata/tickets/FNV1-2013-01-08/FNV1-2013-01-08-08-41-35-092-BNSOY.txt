
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085123                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMFR            
Hours Notice:  72                          Seq Num:        36                
Prepared By:   Jana.haggard                Taken Date:     01/08/13 07:35    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     PINE ENTERPRISE             Excavator Phone:(770) 614-9664    
Address:       796 BROGDON RD              Caller:         LAUREN RUSSELL    
City, St, Zip: SUWANEE, GA 30024           Caller Phone:   (770) 614-9664    
Contact Fax:                               Contact:        LAUREN RUSSELL    
Contact Email:                             Contact Phone:  (770) 614-9664    
Call Back:     770-614-9664                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        WILLIAMSON                  Update Date:    01/23/13 AT 00:00 
Place:         FRANKLIN                    Expire Date:    01/26/13 AT 00:00 

Address:       722 WADESTONE TRL                                     
Intersection:  VALLEY VIEW DR                                        

Latitude:      35.904453                   Longitude:      -86.834643        
Secondary Lat: 35.906553                   Secondary Long: -86.831843        

Work Type:     WATER & SEWER REPAIR        Explosives: No       WhitePaint: No       
Done For:      NW PLUMBING                 Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROPS ARE LOCATED WITHIN 800FT SE OF INTER                                    

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
ALSO ADDR 728 WADESTONE TRL. MARK ENTIRE PROPS. LOT 112 & 115 CREEKSTONE SUBD.

--------------------------------------------------------------------------------
Grids:   [79O  ] [89B  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
MTEMFR          Middle Tenn Electric Membership Coop - Franklin...No                  
U09             Atmos Energy (United Cities Gas) - Franklin - U...No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  


