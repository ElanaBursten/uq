
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085331                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        86                
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 08:33    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     PIEDMONT NATURAL GAS        Excavator Phone:(615) 872-2449    
Address:       83 CENTURY BLVD             Caller:         MARK WHITE        
City, St, Zip: NASHVILLE, TN 37214         Caller Phone:   (615) 872-2449    
Contact Fax:   (615) 872-2374              Contact:        MARK WHITE        
Contact Email: DESIREE.COX@PIEDMONTNG.COM  Contact Phone:  (615) 872-2449    
Call Back:     615-872-2449                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:30 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         BELLE MEADE                 Expire Date:    01/26/13 AT 00:00 

Address:       4423 FORSYTHE PL                                      
Intersection:  BELLE MEADE BLVD                                      

Latitude:      36.091043                   Longitude:      -86.8581          
Secondary Lat: 36.092743                   Secondary Long: -86.85536         

Work Type:     GAS SVC, RETIRE             Explosives: No       WhitePaint: No       
Done For:      PIEDMONT NATURAL GAS        Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER ADDRESS IS SE APPX. 400FT                                          

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK THE ENTIRE PROP AND BOTH SIDES OF RD..                                   

--------------------------------------------------------------------------------
Grids:   [130G ] [130J ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
BELME           Belle Meade, City of                              No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


