
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085286                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        76                
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 08:17    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     SITE WORKS                  Excavator Phone:(615) 356-5430    
Address:       6124 DEAL AVE               Caller:         MIKE PERKINS      
City, St, Zip: NASHVILLE, TN 37209         Caller Phone:   (615) 356-5430    
Contact Fax:                               Contact:        MIKE PERKINS      
Contact Email:                             Contact Phone:  (615) 356-5430    
Call Back:     (615) 997-8908                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         BELLE MEADE                 Expire Date:    01/26/13 AT 00:00 

Address:       34 LYNNWOOD LN                                        
Intersection:  LYNNWOOD BLVD                                         

Latitude:      36.115904                   Longitude:      -86.847908        
Secondary Lat: 36.115944                   Secondary Long: -86.846988        

Work Type:     SWIMMING POOL, INSTL        Explosives: No       WhitePaint: No       
Done For:      ARON RESIDENCE              Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
ADDRESS IS E OF INTER WITHIN 60FT..                                           

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK BOTH SIDES AND THE REAR OF THE PROP..                                    

--------------------------------------------------------------------------------
Grids:   [116F ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
BELME           Belle Meade, City of                              No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


