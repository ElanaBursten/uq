
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085366                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        92                
Prepared By:   Gail.tabb                   Taken Date:     01/08/13 08:44    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     MERRYMAN-FARR MECHANICAL    Excavator Phone:(615) 477-4183    
Address:       305 HILL AVE                Caller:         STEPHEN BRAUN     
City, St, Zip: NASHVILLE, TN 37210         Caller Phone:   (615) 477-4183    
Contact Fax:                               Contact:        STEPHEN BRAUN     
Contact Email: SBRAUN@MERRYMAN-FARR.COM    Contact Phone:  (615) 477-4183    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:45 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       3066 SIDCO DR                                         
Intersection:  ARMORY DR                                             

Latitude:      36.098665                   Longitude:      -86.756611        
Secondary Lat: 36.098665                   Secondary Long: -86.756611        

Work Type:     Other                       Explosives: No       WhitePaint: No       
Done For:      ADVANCE COMPOSITES          Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROP S OF INTER APPX 1200FT                                                   

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
WORK TYPE: PROCESSED WATER TANK..MARK  RIGHT/N SIDE OF BLDG ....AREA IS MARKED
WITH 6 ORANGE BARACADES AND CAUTION TAPE ....                                 

--------------------------------------------------------------------------------
Grids:   [132D ] [132E ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
MWS             Metro Water & Sewer                               No                  
LVT3            Level 3 Communications - LVT3                     No                  


