
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085301                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       INTFR             
Hours Notice:  72                          Seq Num:        78                
Prepared By:   Jamie.morris                Taken Date:     01/08/13 08:21    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     DOUGLAS CABLE               Excavator Phone:(931) 967-5900    
Address:       227 FARRIS LN               Caller:         JIM DOUGLAS       
City, St, Zip: WINCHESTER, TN 37398        Caller Phone:   (931) 967-5900    
Contact Fax:                               Contact:        JIM DOUGLAS       
Contact Email:                             Contact Phone:  (931) 636-6200    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:30 
County:        FRANKLIN                    Update Date:    01/23/13 AT 00:00 
Place:         WINCHESTER                  Expire Date:    01/26/13 AT 00:00 

Address:       325 OAK CIR                                           
Intersection:  WOODMONT DR                                           

Latitude:      35.193859                   Longitude:      -86.143288        
Secondary Lat: 35.195023                   Secondary Long: -86.14246         

Work Type:     CATV DROPS, INSTL           Explosives: No       WhitePaint: No       
Done For:      COMCAST                     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
ADDR APPX 500FT E THEN S FROM INTER.                                          

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK FRONT OF PROP                                                            

--------------------------------------------------------------------------------
Grids:   [64E  ] [64L  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
DREDE           Duck River Elec Membership Coop - Decherd         No                  
ELK             Elk River Public Util Dist                        No                  
WUWS            Winchester Util Water & Sewer - WUWS              No                  
INTFR           Comcast - Franklin - INTFR                        No                  


