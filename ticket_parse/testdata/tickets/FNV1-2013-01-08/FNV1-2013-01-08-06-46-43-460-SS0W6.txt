
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085011                   OLD TICKET NUM: 123450959         

Message Type:  Normal                      For Code:       MTEMMU/INTRU      
Hours Notice:  72                          Seq Num:        7                 
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 05:43    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     LINK UTILITY                Excavator Phone:(270) 392-6783    
Address:       6446 RED RIVER SCHOOL RD    Caller:         STEVE LINK        
City, St, Zip: PORTLAND, TN 37148          Caller Phone:   (270) 392-6783    
Contact Fax:                               Contact:        STEVE LINK        
Contact Email:                             Contact Phone:  (270) 392-6783    
Call Back:     (270) 392-6783                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 05:45 
County:        RUTHERFORD                  Update Date:    01/23/13 AT 00:00 
Place:         SMYRNA                      Expire Date:    01/26/13 AT 00:00 

Address:       648 MILLER ESTATES RD                                 
Intersection:  BLAZER AVE                                            

Latitude:      35.944066                   Longitude:      -86.47803         
Secondary Lat: 35.944333                   Secondary Long: -86.47457         

Work Type:     POLE(S), INSTL AND/OR REPL  Explosives: No       WhitePaint: No       
Done For:      MIDDLE TENN ELEC            Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
POLE IN FRONT OF PROP  MARKED WITH ORANGE RIBBON AND ANOTHER  POLE LOCATED    
FRONT OF PROP AT  761 MILLER ESTATES MARKED WITH ORANGE RIBBON  ... THIS IS   
LOCATED APPX 600 FT E FROM THE ABOVE ADDRESS..PROPS ARE WITH IN 900 FT E OF   
INTER                                                                         

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
ARK A 25 FT RADIUS  OF BOTH POLES...UPDATE/REMARK PER STEVE LINK TKT         
123450959                                                                     

--------------------------------------------------------------------------------
Grids:   [49E  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
CW              Consolidated Util Dist                            No                  
MTEMMU          Middle Tenn Electric Membership Coop - Murfrees...Yes                 
SMU             Smyrna, Town of                                   No                  
INTRU           Comcast- Rutherford - INTRU                       No                  


