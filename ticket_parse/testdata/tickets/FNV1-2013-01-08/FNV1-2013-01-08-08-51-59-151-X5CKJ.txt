
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080079                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMLE            
Hours Notice:  72                          Seq Num:        47                
Prepared By:   gr.amy.goldschmidt          Taken Date:     01/08/13 07:41    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STANSELL ELECTRIC           Excavator Phone:(615) 329-4944    
Address:       860 VISCO DRIVE             Caller:         JOSH COUTS        
City, St, Zip: NASHVILLE, TN 37210         Caller Phone:   (615) 329-4944    
Contact Fax:   (615) 320-5236              Contact:        JOSH COUTS        
Contact Email:                             Contact Phone:  (615) 329-4944    
Call Back:     615 394 5138                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        WILSON                      Update Date:                      
Place:         MOUNT JULIET                Expire Date:                      

Address:       204 RIVERVIEW RD                                      
Intersection:  DAVIS CORNER RD                                       

Latitude:      36.2894034440771            Longitude:      -86.4853743392665 
Secondary Lat: 36.2868602907644            Secondary Long: -86.4820426112832 

Work Type:     DIRECTIONAL DRILLING / BORINGExplosives: No       WhitePaint: No       
Done For:      JAMES CHAMBERS              Directional Boring: Yes               
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
FROM INTER GO N APPX 1008' TO PROP...MARK 200' RADIUS OF DRIVEWAY AT FRONT OF 
PROP...THERE IS A DIRECTIONAL BORES...UPDATE / REMARK PER AMY GOLDSCHMIDT TKT 
123590051                                                                     

--------------------------------------------------------------------------------
Grids:   [28L  ] [28M  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
MTEMLE          Middle Tenn Electric Membership Coop - Lebanon ...No                  
WWILS           West Wilson Utility District                      No                  


