
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085217                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        58                
Prepared By:   Melody.goodman              Taken Date:     01/08/13 07:58    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     TDS Telecom                 Excavator Phone:(615) 641-1961    
Address:       PO BOX 100                  Caller:         JONATHAN MERCSAK  
City, St, Zip: LA VERGNE, TN 37087         Caller Phone:   (615) 754-5005    
Contact Fax:   (615) 793-9051              Contact:        JONATHAN MERCSAK  
Contact Email:                             Contact Phone:  (615) 924-3282    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:00 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         ANTIOCH                     Expire Date:    01/26/13 AT 00:00 

Address:       7347 OLE NOTTINGHAM BLVD                              
Intersection:  S HAMPTON BLVD                                        

Latitude:      36.068387                   Longitude:      -86.572825        
Secondary Lat: 36.068387                   Secondary Long: -86.572825        

Work Type:     FIBER OPTIC, BURYING        Explosives: No       WhitePaint: No       
Done For:      TDS Telecom                 Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
ADDR IS LOCATED ACROSS FROM ABOVE INTER....                                   

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
FACING HOUSE, MARK RIGHT SIDE OF PROP...TOC MAP SHOWS CITY LISTED AS          
NASHVILLE...                                                                  

--------------------------------------------------------------------------------
Grids:   [151J ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
MWS             Metro Water & Sewer                               No                  
TT2             Tennessee Telephone - Lavergne - TT2              No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


