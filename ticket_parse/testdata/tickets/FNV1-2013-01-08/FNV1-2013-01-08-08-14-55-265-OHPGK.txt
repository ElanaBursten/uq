
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080017                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       INTSU/CEMC        
Hours Notice:  72                          Seq Num:        17                
Prepared By:   gr.rita.c                   Taken Date:     01/08/13 06:52    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AT&T                        Excavator Phone:(423) 875-4346    
Address:       4500 PINNACLE LN            Caller:         RITA COOK         
City, St, Zip: CHATTANOOGA, TN 37415       Caller Phone:   (423) 875-2514    
Contact Fax:   (423) 875-2727              Contact:        RITA COOK         
Contact Email:                             Contact Phone:  (423) 875-2514    
Call Back:     423 875-2514                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:00 
County:        ROBERTSON                   Update Date:                      
Place:         GREENBRIER                  Expire Date:                      

Address:       2905 N MOUNT PLEASANT RD                              
Intersection:  MUGGIN LN                                             

Latitude:      36.4561333118819            Longitude:      -86.7459346326051 
Secondary Lat: 0                           Secondary Long: 0                 

Work Type:     TELEPHONE DROPS, BURYING    Explosives: No       WhitePaint: No       
Done For:      AT&T UTILITY OPERATIONS     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROPERTY AT ADDRESS AND BOTH SIDES OF ROAD ADJACENT TO ADDRESS.   
ALSO MARK PROPERTY ACROSS ROAD 100 FT IN EACH DIRECTION... ATT JOB TJ1300042  

--------------------------------------------------------------------------------
Grids:   [107M ] [107N ] [117C ] 
[117D ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
GC              Greenbrier, City of                               No                  
WHU             White House Utility District                      No                  
INTSU           Comcast - Gallatin - INTSU                        No                  
CEMC            Cumberland Electric Membership Coop               No                  


