
New Jersey One Call System        SEQUENCE NUMBER 0012    CDC = CAN 

Transmit:  Date: 01/06/14   At: 10:51 

*** E M E R G E N C Y     *** Request No.: 140060613 

Operators Notified: 
BAN     = VERIZON                       CAN     = CABLEVISION OF NJ              
JOM     = JOINT MEETING OF ESSEX &      NW2     = NEWARK, CITY OF                
PSHR    = PUBLIC SERVICE ELECTRIC &     XOC1    = XO NEW JERSEY, INC.            

Start Date/Time:    01/06/14   At 11:00  Expiration Date:  

Location Information: 
County: ESSEX                  Municipality: NEWARK 
Subdivision/Community:  
Street:               0 FABYAN PL 
Nearest Intersection: LYONS AVE 
Other Intersection:   SHAW AVE 
Lat/Lon:  Nad: 83    Lat: 40 42.910  Lon: -74 13.345 
Type of Work: EMERGENCY - REPAIR/REPLACE BROKEN POLE 
Block:                Lot:                Depth: 8FT 
Extent of Work: M/O LOCATED 350FT N OF C/L OF INTERSECTION.  20FT RADIUS OF
  POLE # 62818. 
Remarks:  
  PUBLIC SAFETY HAZARD 
  Working For Contact:  AL DEALESANDRO 

Working For: PSE&G 
Address:     938 CLINTON AVE 
City:        IRVINGTON, NJ  07111 
Phone:       973-430-8889   Ext:  

Excavator Information: 
Caller:      AL DEALESANDRO 
Phone:       973-430-8889   Ext:  

Excavator:   PSE&G 
Address:     938 CLINTON AVE 
City:        IRVINGTON, NJ  07111 
Phone:       973-430-8889   Ext:          Fax:  973-399-5978 
Cellular:    973-879-1158 
Email:       alfred.dealesandro@pseg.com 
End Request 
