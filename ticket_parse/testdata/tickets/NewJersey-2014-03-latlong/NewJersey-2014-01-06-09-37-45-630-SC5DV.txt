
New Jersey One Call System        SEQUENCE NUMBER 0028    CDC = EG1 

Transmit:  Date: 01/06/14   At: 09:36 

*** R O U T I N E         *** Request No.: 140060380 

Operators Notified: 
AGT     = ALGON/TEXAS EASTERN GAS T     BAN     = VERIZON                        
BUK     = BUCKEYE PARTNERS              CP2     = COLONIAL PIPELINE COMPANY      
EG1     = ELIZABETHTOWN GAS COMPANY     MCI     = MCI                            
PRX     = PRAXAIR, INC.                 PSCE    = PUBLIC SERVICE ELECTRIC &      
PSGT    = PUBLIC SERVICE ELECTRIC &     SCEW1   = NJ AMERICAN WATER              
TGP     = TRANSCONTINENTAL GAS PIPE      

Start Date/Time:    01/10/14   At 07:15  Expiration Date: 03/12/14 

Location Information: 
County: UNION                  Municipality: LINDEN 
Subdivision/Community:  
Street:               0 GRASSELLI RD 
Nearest Intersection: S WOOD AVE 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40.61969   Lon: -74.209080 
Type of Work: REPLACE POLE(S) 
Block:                Lot:                Depth: 9FT 
Extent of Work: M/O LOCATED 4500FT E OF C/L OF INTERSECTION.  25FT RADIUS OF
  POLE 5158.  M/O LOCATED 1850FT BEHIND N CURB. 
Remarks:  
  Working For Contact:  JONATHAN HALLENBECK 

Working For: PSEG 
Address:     472 WESTON CANAL RD 
City:        SOMERSET, NJ  08873 
Phone:       732-764-3243   Ext:  

Excavator Information: 
Caller:      JONATHAN HALLENBECK 
Phone:       732-764-3243   Ext:  

Excavator:   PSE&G 
Address:     472 WESTON CANAL ROAD 
City:        SOMERSET, NJ  08873 
Phone:       732-764-3243   Ext:          Fax:  732-356-1688 
Cellular:    732-764-3243 
Email:       jonathan.hallenbeck@pseg.com 
End Request 
