
New Jersey One Call System        SEQUENCE NUMBER 0053    CDC = CTC 

Transmit:  Date: 01/06/14   At: 18:42 

*** R O U T I N E         *** Request No.: 140061713 

Operators Notified: 
BAN     = VERIZON                       CTC     = COMCAST OF SOMERSET            
EW2     = NEW JERSEY AMERICAN WATER     MFR     = ZAYO GROUP                     
PSTN    = PUBLIC SERVICE ELECTRIC &     SNS     = SUNESYS, LLC                   
UNI     = CENTURY LINK                   

Start Date/Time:    01/10/14   At 07:00  Expiration Date: 03/12/14 

Location Information: 
County: SOMERSET               Municipality: MONTGOMERY 
Subdivision/Community: JOHNSON & JOHNSON 
Street:               199 GRANDVIEW RD 
Nearest Intersection: E RIDGE RD 
Other Intersection:   BLAWENBURG BELLE MEAD RD 
Lat/Lon:  Nad: 83    Lat: 40 25 34.8 Lon: -74 42 25. 
Type of Work: REPAIR WATER MAIN 
Block:                Lot:                Depth: 6FT 
Extent of Work: 4 AREA(S) MARKED IN WHITE.  AREA(S) MARKED IN WHITE LOCATED:
  FRONT OF PROPERTY.  WHITE MARKS ARE NOT LABELED.  THE FENCE IS LAYING
  BY WORKSITE 
Remarks:  
  Working For Contact:  ROBERT DEFAZIO 

Working For: JOHNSON & JOHNSON 
Address:     199 GRANDVIEW RD 
City:        MONTGOMERY, NJ  08558 
Phone:       848-565-6044   Ext:  

Excavator Information: 
Caller:      RUSSELL FEDDERSEN 
Phone:       609-259-2079   Ext:  

Excavator:   BENNETT BROTHERS MECHANICAL 
Address:     P O BOX 239 
City:        CLARKSBURG, NJ  08510 
Phone:       609-259-2079   Ext:          Fax:  609-259-3241 
Cellular:    732-489-3540 
Email:       BENBROMECH1@HOTMAIL.COM 
End Request 
