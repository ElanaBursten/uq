
New Jersey One Call System        SEQUENCE NUMBER 0172    CDC = AE2 

Transmit:  Date: 03/19/14   At: 15:05 

*** R O U T I N E         *** Request No.: 140781320 

Operators Notified: 
AE2     = ATLANTIC CITY ELECTRIC CO     BAN     = VERIZON                        
CG10    = COLUMBIA GAS TRANSMISSION     SCV     = COMCAST CABLE (VINELAND)       
SJG     = SOUTH JERSEY GAS COMPANY      SWB     = BOROUGH OF SWEDESBORO          

Start Date/Time:    03/25/14   At 10:30  Expiration Date: 05/22/14 

Location Information: 
County: GLOUCESTER             Municipality: SWEDESBORO 
Subdivision/Community:  
Street:               0 AUBURN AVE 
Nearest Intersection: HIGH HILL RD 
Other Intersection:   LEXINGTON MEWS 
Lat/Lon:  Nad: 83    Lat: 4399235.8  Lon: 472645.8      Zone: 18 
Type of Work: DIGGING TEST HOLES 
Block: 3              Lot:  7             Depth: 2FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 260FT W.  CURB
  TO 130FT BEHIND N CURB. 
Remarks:  
  Working For Contact:  PAUL KREISA 

Working For: STANTEC 
Address:     6110 FROST PLACE 
City:        LAUREL, MD  20707 
Phone:       301-982-2866   Ext:  

Excavator Information: 
Caller:      NANCY POWELL 
Phone:       240-542-3132   Ext:  

Excavator:   STANTEC 
Address:     6110 FROST PLACE 
City:        LAUREL, MD  20707 
Phone:       301-982-2866   Ext:          Fax:  
Cellular:     
Email:       nancy.powell@stantec.com 
End Request 
