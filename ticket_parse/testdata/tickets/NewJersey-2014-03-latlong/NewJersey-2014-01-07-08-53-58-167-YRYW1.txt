
New Jersey One Call System        SEQUENCE NUMBER 0011    CDC = SCV 

Transmit:  Date: 01/07/14   At: 08:53 

*** R O U T I N E         *** Request No.: 140070214 

Operators Notified: 
AE1     = ATLANTIC CITY ELECTRIC        BAN     = VERIZON                        
MGC     = MARGATE CITY PUBLIC WORKS     SCV     = COMCAST CABLE (VINELAND)       
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/13/14   At 07:00  Expiration Date: 03/13/14 

Location Information: 
County: ATLANTIC               Municipality: MARGATE 
Subdivision/Community:  
Street:               317 N ARGYLE AVE 
Nearest Intersection: VENTNOR AVE 
Other Intersection:   FREMONT AVE 
Lat/Lon:  Nad: 83    Lat: 39 20 17.1 Lon: -74 29 52. 
Type of Work: DEMOLITION 
Block:                Lot:                Depth: 3FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  CHRISTOPHER CRISWELL 

Working For: HOMEOWNER 
Address:     317 N ARGYLE AVE 
City:        MARGATE, NJ  08402 
Phone:       609-333-3204   Ext:  

Excavator Information: 
Caller:      JOHN DAVID HUGHES 
Phone:       609-335-5762   Ext:  

Excavator:   MASE ENTERPRISES LLC. 
Address:     552 W LEEDS AVE 
City:        ABSECON, NJ  08201 
Phone:       609-335-5762   Ext:          Fax:  609-344-4650 
Cellular:    609-335-5762 
Email:       davehughes@hotmail.com 
End Request 
