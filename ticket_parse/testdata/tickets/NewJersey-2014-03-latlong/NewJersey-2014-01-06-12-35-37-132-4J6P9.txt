
New Jersey One Call System        SEQUENCE NUMBER 0081    CDC = SCV 

Transmit:  Date: 01/06/14   At: 12:32 

*** R O U T I N E         *** Request No.: 140060913 

Operators Notified: 
BAN     = VERIZON                       EGC     = EGG HARBOR TOWNSHIP M.U.A      
SCNJ3   = NJ AMERICAN WATER             SCV     = COMCAST CABLE (VINELAND)       
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/10/14   At 09:00  Expiration Date: 03/12/14 

Location Information: 
County: ATLANTIC               Municipality: EGG HARBOR TWP 
Subdivision/Community:  
Street:               0 WRIGHT AVE 
Nearest Intersection: DAVINCI CT 
Other Intersection:   BLERIOT CT 
Lat/Lon:  Nad: 83    Lat: 39.443289  Lon: -74.580081 
Type of Work: REPAIR WATER VALVE 
Block:                Lot:                Depth: 6FT 
Extent of Work: 1 AREA(S) MARKED IN WHITE.  WHITE MARKS LOCATED 4FT W FROM
  C/L OF INTERSECTION ON N SIDE OF ROAD 6FT FROM CURB. 
Remarks:  
  Working For Contact:  DON MORGAN 

Working For: ISS FACILITY SERVICES 
Address:     ALL STAR BUILDING 306 
City:        ACC INT'L AIRPORT, NJ  08405 
Phone:       609-485-8426   Ext:  

Excavator Information: 
Caller:      DON MORGAN 
Phone:       609-485-8426   Ext:  

Excavator:   ISS FACILITY SERVICES 
Address:     ALL STAR BUILDING 306 
City:        ACC INT'L AIRPORT, NJ  08405 
Phone:       609-485-8426   Ext:          Fax:  609-485-6507 
Cellular:    609-955-0911 
Email:       donald.ctr.morgan@faa.gov 
End Request 
