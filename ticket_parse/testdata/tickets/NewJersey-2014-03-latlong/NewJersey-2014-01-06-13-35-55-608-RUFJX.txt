
New Jersey One Call System        SEQUENCE NUMBER 0028    CDC = CTC 

Transmit:  Date: 01/06/14   At: 13:35 

*** R O U T I N E         *** Request No.: 140061061 

Operators Notified: 
AGT     = ALGON/TEXAS EASTERN GAS T     BAN     = VERIZON                        
CTC     = COMCAST OF SOMERSET           GPF     = JERSEY CENTRAL POWER & LI      
PSSU    = PUBLIC SERVICE ELECTRIC &     SMC     = SOUTHEAST MORRIS COUNTY M      

Start Date/Time:    01/10/14   At 10:00  Expiration Date: 03/12/14 

Location Information: 
County: MORRIS                 Municipality: HARDING 
Subdivision/Community:  
Street:               505 VAN BEUREN RD 
Nearest Intersection: GRIFFIN LN 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40 45.103  Lon: -74 28.652 
Type of Work: INSTALL POLE(S) 
Block:                Lot:                Depth: 8FT 
Extent of Work: 15FT RADIUS OF POLE NUMBER JC801HD. 
Remarks:  
  Working For Contact:  GEORGE STROTHER 

Working For: VAN BEUREN FARMS 
Address:     505 VAN BEUREN RD 
City:        HARDING, NJ  07096 
Phone:       973-714-9768   Ext:  

Excavator Information: 
Caller:      JOHN PIERCE 
Phone:       973-401-8081   Ext:  

Excavator:   JCP&L 
Address:     300 MADISON AVE 
City:        MORRISTOWN, NJ  07962 
Phone:       973-401-8081   Ext:          Fax:  973-644-4259 
Cellular:    973-401-8081 
Email:       jfpierce@FIRSTENERGYCORP.COM 
End Request 
