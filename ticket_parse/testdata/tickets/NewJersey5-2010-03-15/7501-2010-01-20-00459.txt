
KE     00039 POCS 01/20/10 13:03:51 20100201081-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201081]-[000] Channel#--[1300026][0212]

Message Type--[NEW][EXCAVATION][ROUTINE]

County--[DELAWARE]        Municipality--[COLLINGDALE BORO]
Work Site--[CHESTNUT ST]
     Nearest Intersection--[WOODLAWN AVE]
     Second Intersection--[GIRARD AVE]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [WORKING AT 900, 902, 903, 904, 909 AND 919 CHESTNUT ST, BTWN WOODLAWN AVE
      AND GIRARD AVE.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.915281/-75.284862,39.915997/-75.283631,39.915860/-75.283522,
      39.915175/-75.284772]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201081]

Type of Work--[CURB BOX REHABILITATION]                      Depth--[5FT]
Extent of Excavation--[3FT X 3FT]       Method of Excavation--[HAND]
Street--[ ] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]

              Lawful Start Dates--[25-Jan-10] Through [03-Feb-10]
   Scheduled Excavation Date--[25-Jan-10] Dig Time--[0800] Duration--[3 DAYS]
                         Response Due Date--[22-Jan-10]

Caller--[AL HEIMBACH]                      Phone--[610-690-0301] Ext--[]
Excavator--[J FLETCHER CREAMER SON]         Homeowner/Business--[B]
Address--[101 E BROADWAY]
City--[HACKENSACK]                   State--[NJ] Zip--[07601]
FAX--[610-690-0302]  Email--[none]
Work Being Done For--[AQUA PA]

Person to Contact--[AL HEIMBACH]               Phone--[610-690-0301] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-Jan-10] at [1303] by [JODI HORN]

Job Number--[09-7568]
Remarks--
     []

ATM0  ATM=AT&T ATLANTA     CDB0  CDB=COLLINGDLE BORO  HT 0  HT =AQUA PA INC    
JQ10  JQ1=DARBY CREEK JA   JY 0  JY =COMCAST CABLE W  KE 0  KE =PECO MRTN      
YI 0  YI =VERIZON HRSM

Serial Number--[20100201081]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========