
JY     00031 POCS 01/20/10 13:05:06 20100201085-000 UPDT XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201085]-[000] Channel#--[1304012][0046]

Message Type--[UPDATE][EXCAVATION][ROUTINE]

County--[DELAWARE]        Municipality--[DARBY BORO]
Work Site--[118 S 4TH ST]
     Nearest Intersection--[WALNUT ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [MARK OUT ON WALNUT ST SIDE OF PROPERTY, THE SITE IS AT THE COR OF THE
      INTER.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.915356/-75.253610,39.914854/-75.253759,39.914709/-75.252995,
      39.915197/-75.252866]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201085]

Type of Work--[REPAIR WTR SVC]                               Depth--[6FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]

              Lawful Start Dates--[25-Jan-10] Through [03-Feb-10]
   Scheduled Excavation Date--[25-Jan-10] Dig Time--[0800] Duration--[3 DAYS]
                         Response Due Date--[22-Jan-10]

Caller--[DIANE SWEIGARD]                   Phone--[610-328-5065] Ext--[]
Excavator--[AQUA AMERICA]                   Homeowner/Business--[B]
Address--[700 W SPROUL RD]
City--[SPRINGFIELD]                  State--[PA] Zip--[19064]
FAX--[610-328-4065]  Email--[dsweigard@aquaamerica.com]
Work Being Done For--[AQUA ]

Person to Contact--[DIANE SWEIGARD]            Phone--[610-328-5065] Ext--[]
Best Time to Call--[0730-1600]

Prepared--[20-Jan-10] at [1304] by [JACKIE COLCLASER]

Remarks--
     [******=== UPDATE 20093640966-000 --1/20/2010 1304 JC2 12===******
      UPDATE REQUESTED BY: DIANE S
      REASON FOR UPDATE: WORK NOT STARTED
      REMARK LINES.]

ATM0  ATM=AT&T ATLANTA     DRB0  DRB=DARBY BOROUGH    HT 0  HT =AQUA PA INC    
JQ10  JQ1=DARBY CREEK JA   JY 0  JY =COMCAST CABLE W  KE 0  KE =PECO MRTN      
YI 0  YI =VERIZON HRSM

Serial Number--[20100201085]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========