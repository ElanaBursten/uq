
PACBEL 01468 USAN 10/04/12 12:57:19 0372304 NORMAL NOTICE 

Message Number: 0372304 Received by USAN at 12:51 on 10/04/12 by ADM

Work Begins:    10/09/12 at 13:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 11/01/12 at 23:59   Update By: 10/30/12 at 16:59

Caller:         ERIC SCHOLZ              
Company:        SIERRA PRECAST INC                 
Address:        1 LIVE OAK AVE                          
City:           MORGAN HILL                   State: CA Zip: 95037
Business Tel:   408-779-1000                  Fax: 408-778-1255                
Email Address:  ERIC.SCHOLZ@OLDCASTLE.COM                                   

Nature of Work: VERTICAL BORING FOR SOUND WALL          
Done for:       PG&E                          Explosives: N
Foreman:        KEVIN FAHEY              
Field Tel:                                    Cell Tel: 408-210-9623           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
NE Corner of:           BRANHAM LN
         And:           JARVIS AVE


    GO N ON E/SI/O JARVIS APP 80' & FR SAME BEG PT GO E ON N/SI/O
    BRANHAM APP 175' 

Place: SAN JOSE                     County: SANTA CLARA          State: CA

Long/Lat Long: -121.891735 Lat:  37.258213 Long: -121.888835 Lat:  37.260531 


Sent to:
CTYSJO = CITY SAN JOSE                COMSJO = COMCAST-SAN JOSE             
LEVCAL = LEVEL 3 COMM - CALIF         MCIWSA = MCI WORLDCOM                 
PACBEL = PACIFIC BELL                 PGESJO = PGE DISTR SAN JOSE           
SJOWT2 = SAN JOSE WATER CO. 2         SCLVLY = SANTA CLARA VALLEY WTR       
ZAYOCA = ZAYO - CA                    





At&t Ticket Id: 25652623   clli code: SNJSCA14   OCC Tkt No: 0372304-000
PACBEL: CONDUIT,FIBER,UNDERGROUND,FTTN,FIBER LANDMARK