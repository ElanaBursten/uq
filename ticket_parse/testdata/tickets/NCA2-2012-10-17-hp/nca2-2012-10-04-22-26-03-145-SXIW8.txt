
PACBEL 01459 USAN 10/04/12 12:51:43 0372290 NORMAL NOTICE 

Message Number: 0372290 Received by USAN at 12:43 on 10/04/12 by VAL

Work Begins:    10/10/12 at 08:00   Notice: 025 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 11/01/12 at 23:59   Update By: 10/30/12 at 16:59

Caller:         MIKE ZIMNICKI            
Company:        PG&E                               
Address:        615 7TH AVE, SANTA CRUZ                 
City:           SANTA CRUZ                    State: CA Zip: 95063
Business Tel:   831-479-3152                  Fax: 831-479-3079                
Email Address:  MJZ4@PGE.COM                                                

Nature of Work: HAND DIG TO REPL ELEC BOX               
Done for:       SAME                          Explosives: N
Foreman:        UNKNOWN                  
Field Tel:                                    Cell Tel:                        
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    UNKNOWN                       
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         401 SOQUEL SAN JOSE RD
  Cross Street:         ONEILL LN

    WRK NEAR THE BAND ROOM AT THE SOQUEL HIGH SCH 

Place: SOQUEL, CO AREA              County: SANTA CRUZ           State: CA

Long/Lat Long: -121.96282  Lat:  36.988295 Long: -121.957132 Lat:  36.994028 


Sent to:
CHAGIL = CHARTER COMMUNICATIONS 2     CTYSCZ = CITY SANTA CRUZ              
CTYSC2 = CITY SANTA CRUZ TRF MAIN     CTYSC3 = CITY SANTA CRUZ WASTEWTR     
COSCR2 = CO SANTA CRUZ TRF SIG        COMSCZ = COMCAST-SANTA CRUZ           
COSCRZ = COUNTY SANTA CRUZ PW         PACBEL = PACIFIC BELL                 
PGESAL = PGE DISTR SALINAS            SOQUEL = SOQUEL CREEK WTR DIST        





At&t Ticket Id: 25652604   clli code: SNCZCA11   OCC Tkt No: 0372290-000
PACBEL: CONDUIT