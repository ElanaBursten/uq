
SCEDZ05 3 SC811 Voice 02/08/2013 11:23:00 AM 1302080026 Extraord Circ 

Notice Number:    1302080026        Old Notice:                        
Sequence:         3                 Created By:      RGO               

Created:          02/08/13 11:27 AM                                     
Work Date:        02/08/13 11:23 AM                                     
Update on:        02/27/2013        Good Through:    03/04/2013        

Caller Information:
SC811
810 DUTCH SQ BLVD
COLUMBIA, SC 29210
Company Fax:                        Type:            Other             
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:0
Caller Email:     RDOTMAN@HOTMAIL.COM                                   

Site Contact Information:
RANDI OLSEN                              Phone:(803) 939-1117 Ext:0
Site Contact Email:  frontdesk@sc1pups.org                                 
CallBack:  8am to 5pm

Excavation Information:
SC    County:  BEAUFORT             Place:  BEAUFORT          
Street:  BROWN RD                   Address In Instructions:true              
Intersection:  CHARLESTON HWY                                        
Subdivision:   TESTING                                               

Lat/Long: 32.623012,-80.732904
Second:  0, 0

Explosives:  Y Premark:  N Drilling/Boring:  N Near Railroad:  Y

Work Type:     SEE INSTRUCTIONS                                        
Work Done By:  SC811                Duration:        DAY               
Work Done For: SC811-1                                               

Instructions:
THIS IS ONLY A TEST                                                           

Directions:
THIS IS ONLY A TEST                                                           

Remarks:
THIS IS ONLY A TEST                                                           

Member Utilities Notified:
HARZ12 CNNG15* BERZ20* SPSD29* ATT09* BAT64* BRD11* 
HOT21* MCI18* QWC42* EMBZ11 SCEDZ05* ITCC76 SRWS80* 
JMW83* HRPZ62 11TCH49* 

