
Locate Request For PEB
----------------------------------------------------------------------------
Ticket Number:    1204315325        Old Ticket:                         
Source:           Voice             Sequence:         1                 
Type:             Emergency         Date:             02/12/12 13:45    

Company Information
----------------------------------------------------------------------------
City of Martinsburg Water Departme  Type:             Member            
232 N QUEEN ST                      Company Phone:    (304) 264-2116    
MARTINSBURG, WV 25401               Company Fax:      (304) 264-2118    
Caller Name:      Tim Schottman     Caller Phone:     (304) 264-2116    
Contact Name:     Tim Schottman     Contact Phone:    (304) 264-2116    
Caller Email:                                                           
Contact Email:                                                          

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        02/12/12 13:45    
County:           BERKELEY          Done For:         City of Martinsbur
Place:            MARTINSBURG       Depth:            4 FT              
Street:           305 N Red Hill Rd                                     
Intersection:     North St                                              
Nature of Work:   Emer water line repair                                
Explosives:       No                Boring:           No                
White Lined:      No                Length:                             

Driving Directions
----------------------------------------------------------------------------


Remarks
----------------------------------------------------------------------------
Emer water line repair, crew on site, cust w/service, water is visible.       
Working in the street. Mark 20ft radius.                                      


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
PEB   Firstenergy Corp                                  No                  
AX    Frontier Communications(Formerly Verizon)         No                  
CEM   Mountaineer Gas Company                           No                  
MAR   City of Martinsburg Water Department              No                  
WCM   Comcast                                           No                  


Location
----------------------------------------------------------------------------
Latitude:         39.465267         Longitude:        -77.98358         
Second Latitude:  39.467003         Second Longitude: -77.981822        

