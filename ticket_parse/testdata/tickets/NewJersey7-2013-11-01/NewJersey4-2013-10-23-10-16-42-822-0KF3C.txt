
UtiliQ-NY  08233-170-006-00  10/23/2013 10:15:57  00001

Assigned to ORUUNY for 08/23/2013


DIG REQUEST from DSNY for: O&R UTILS / RCK-ORNG       Taken: 08/23/2013 08:41
To: O&R UTILS / DAY                  Transmitted: 08/23/2013 08:49 00015


Ticket: 08233-170-006-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: CONGERS  /P
Addr:  From: 250    To:        Name:    NY RT 303                           
Cross: From:        To:        Name:    HEMLOCK                        DR   
Offset:
------------------------------------------------------------------------------
Locate: MARK ENTIRE FRONT MAIN ENTRANCE WAY ON THE CONCRETE, ***DO NOT PAINT ON
      : THE STREET, ONLY THE CONCRETE ENTRANCE WAY AND NOTHING IN THE GARAGE
      : AREA***
NearSt: CORNER
Means of Excavation: MINI EXCAVATOR                          Blasting: N
Site marked with white: N
Boring/Directional Drilling: N
Within 25ft of Edge of Road: N


Work Type: REMOVE CONCRETE AND OLD PAVERS, LANDSCAPING
Duration: 10 DAYS
Depth of excavation: 3 FEET
Site dimensions:
Start Date and Time: 08/28/2013 07:30
Must Start By: 09/12/2013
------------------------------------------------------------------------------
Contact Name: MITCH TAYLOR
Company: R & M LANDSCAPE
Addr1: PO BOX 294                      Addr2:                                 
City: CONGERS                          State: NY    Zip: 10920
Phone: 914-906-0343                    Fax: 845-267-4898                    
Email:                                                                        
Field Contact: MITCH
Cell Phone: 914-906-0343               
Working for: PROPERTY OWNER
------------------------------------------------------------------------------
Comments: LENGTH AND WIDTH OF THE DIG SITE ARE UNKNOWN
        : DIG SITE ALSO AFFECTS: CLARKSTOWN /T
        : Lookup Type: MANUAL
------------------------------------------------------------------------------
Boundary: n 41.165305    s 41.161645    w -73.939092    e -73.934250
------------------------------------------------------------------------------


Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : TWN CLARKSTOWN SWR               UNI WTR NEW YRK                  
       : BELL-VALHALLA / HDSN VLY         
