
UtiliQ-NY  08233-040-073-00  10/23/2013 10:15:57  00006

Assigned to ORUUNY for 08/23/2013


DIG REQUEST from DSNY for: O&R UTILS / RCK-ORNG       Taken: 08/23/2013 08:56
To: O&R UTILS / DAY                  Transmitted: 08/23/2013 08:56 00020


Ticket: 08233-040-073-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: STONY POINT  /T
Addr:  From:        To:        Name:    RIVER                          RD   
Cross: From:        To:        Name:    FOURTH                         ST   
Offset:


State: NY  County: ROCKLAND             Place: STONY POINT  /T
Addr:  From:        To:        Name:    RIVER                          RD   
Cross: From:        To:        Name:    FIFTH                          ST   
Offset:
------------------------------------------------------------------------------
Locate: MARK CURB TO CURB TO 20' BEHIND EACH CURB ENTIRE BETWEEN CROSS ROADS
      : INCLUDING BOTH INTERSECTIONS
NearSt: FOURTH AND FIFTH
Means of Excavation: BACKHOE                                 Blasting: N
Site marked with white: N
Boring/Directional Drilling: N
Within 25ft of Edge of Road: Y


Work Type: GAS MAIN REPLACEMENT
Duration:
Depth of excavation: 5 FEET
Site dimensions:
Start Date and Time: 08/28/2013 07:00
Must Start By: 09/12/2013
------------------------------------------------------------------------------
Contact Name: ANNA ARAMINI
Company: COLONNELLI BROS
Addr1: 409 S RIVER ST                  Addr2:                                 
City: HACKENSACK                       State: NJ    Zip: 07601
Phone: 201-440-1118                    Fax: 201-440-8282                    
Email: AARAMINI@COLONNELLI.COM                                                
Field Contact: ANNA
Alt Phone: 201-440-1118                
Working for: O&R UTILITIES
------------------------------------------------------------------------------
Comments: LENGTH AND WIDTH OF THE DIG SITE ARE UNKNOWN
        : DIG SITE ALSO AFFECTS: HAVERSTRAW /T, STONY POINT /P
        : Lookup Type: BETWEEN INTERSECTION
------------------------------------------------------------------------------
Boundary: n 41.222217    s 41.220527    w -73.965609    e -73.963881
------------------------------------------------------------------------------


Members: CBLVSN W NYACK                   CON-ED                           
       : JNT REG SWR BRD                  O&R UTILS / RCK-ORNG             
       : TWN STONY POINT                  UNI WTR NEW YRK                  
       : BELL-VALHALLA / HDSN VLY         
