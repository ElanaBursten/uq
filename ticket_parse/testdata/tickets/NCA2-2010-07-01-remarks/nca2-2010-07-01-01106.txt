
                                                                                     
 PBTSJ2 00168 USAN 07/01/10 14:46:05 0133994 NORMAL NOTICE EXTENSION                 
                                                                                     
 Message Number: 0133994 Received by USAN at 14:45 on 07/01/10 by SMV                
                                                                                     
 Work Begins:    05/14/10 at 15:15   Notice: 020 hrs      Priority: 2                
 Night Work: N    Weekend Work: N                                                    
                                                                                     
 Expires: 08/04/10 at 17:00   Update By: 08/02/10 at 16:59                           
                                                                                     
 Caller:         LEAUNDRA KASPRZAK                                                   
 Company:        PRESTON PIPELINE INC.                                               
 Address:        133 BOTHELO AVE, SAN JOSE                                           
 City:           MILPITAS                      State: CA Zip: 95170                  
 Business Tel:   408-262-1418                  Fax: 408-262-1870                     
 Email Address:                                                                      
                                                                                     
 Nature of Work: TR TO INST WTR MAIN & SVCS                                          
 Done for:       SAN JOSE WTR                  Explosives: N                         
 Foreman:        UNKNOWN                                                             
 Field Tel:                                    Cell Tel:                             
 Area Premarked: Y   Premark Method: WHITE PAINT                                     
 Permit Type:    CITY                          Number: PENDING                       
 Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N        
 Excavation Enters Into Street Or Sidewalk Area: Y                                   
                                                                                     
 Location:                                                                           
                                                                                     
     AT A POINT 300FT NE/O INT LOS GATOS BLVD AND GATEWAY DR ON ALL/O LOS GATOS      
     BLVD                                                                            
     GO 2700' NE & EXT 600' NW FOR ENT DIST                                          
                                                                                     
 Place: LOS GATOS                    County: SANTA CLARA          State: CA          
                                                                                     
 Long/Lat Long: -121.962231 Lat:  37.246146 Long: -121.95073  Lat:  37.256223        
                                                                                     
 Excavator Requests Operator(s) To Re-mark Their Facilities: N                       
 Comments:                                                                           
 RENEWAL OF TICKET RN#371113 ORIG DATE 04/13/2010 RE-MARK NO-CMJ                     
 05/12/2010                                                                          
 #1 EXTEND TO 07/07/2010 RE-MARK NO ORIG DATE 05/12/2010-TLF 06/03/2010              
 PER LEAUNDRA                                                                        
 #2 EXTEND TO 08/04/2010 RE-MARK NO ORIG DATE 05/12/2010-SMV 07/01/2010              
 -PER DESIREE                                                                        
                                                                                     
 Sent to:                                                                            
 CTYCAM = CITY CAMPBELL                CTYSJO = CITY SAN JOSE                        
 COMSJO = COMCAST-SAN JOSE             PBTSJ3 = PACIFIC BELL SAN JOSE 3              
 PGECUP = PGE DISTR CUPERTINO          SJOWT2 = SAN JOSE WATER CO. 2                 
 SCLVLY = SANTA CLARA VALLEY WTR       TWNLGA = TOWN OF LOS GATOS                    
 VERSJO = VERIZON SAN JOSE             WVASAN = WEST VLY SANI DIST SCLA              
                                                                                     
                                                                                     
                                                                                     
 At&t Ticket Id: 23560017     clli code:   SNJSCA14                                  
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
                                                                                     
=