
JZ     00037 POCS 01/20/10 13:00:01 20100201070-000 NEW  XCAV EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201070]-[000] Channel#--[1254001][0346]

Message Type--[NEW][EXCAVATION][EMERGENCY]

County--[CHESTER]         Municipality--[COATESVILLE CITY]
Work Site--[117 S 12TH AVE]
     Nearest Intersection--[SANSOM ST ]
     Second Intersection--[OAK ST]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [SITE IS BTWN SANSOM ST AND OAK ST NEAR WALNUT ST.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.982825/-75.802408,39.982385/-75.802331,39.982910/-75.798840,
      39.983401/-75.798950]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201070]

Type of Work--[REPAIR WATER SERVICE LINE]                    Depth--[]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[X] Other--[]

              Lawful Start Dates--[         ] Through [         ]
    Scheduled Excavation Date--[20-Jan-10] Dig Time--[1300] Duration--[1 DAY]
                         Response Due Date--[20-Jan-10]

Caller--[JIM FRELIGH]                      Phone--[610-383-7042] Ext--[]
Excavator--[BULLDOG CONSTRUCTION]           Homeowner/Business--[B]
Address--[1120 VALLEY RD]
City--[COATESVILLE]                  State--[PA] Zip--[19320]
FAX--[610-380-1307]  Email--[none]
Work Being Done For--[PENNSYLVANIA AMERICAN WATER]

Person to Contact--[JIM FRELIGH]               Phone--[610-587-5905] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-Jan-10] at [1259] by [MARY ANNE YURTAL]

Remarks--
     [WATER IS RUNNING AT THIS LOCATION.   CREW IS AROUND THE COR WORKING ON
      ANOTHER WATER MAIN BREAK.   CALLER STATES THE AREA WAS MARKED FOR THE
      OTHER CONTRACTOR THIS MORNING.]

COT0  COT=COATESVILLE CIT  JZ 0  JZ =COMCAST CABLE    KF 0  KF =PECO CTVL      
VA 0  VA =PA AMERICAN WTR  VAW0  VAW=PA AMERICAN WTR  WG40  WG4=PAWC AFT HR-WG 
YJ 0  YJ =VERIZON

Serial Number--[20100201070]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========