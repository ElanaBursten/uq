
KA     00053 POCS 01/20/10 13:01:43 20100201076-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201076]-[000] Channel#--[1257018][0229]

Message Type--[NEW][EXCAVATION][ROUTINE]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]          Ward--[08]
Work Site--[1937 MANNING ST]
     Nearest Intersection--[S 19TH ST]
     Second Intersection--[S 20TH ST]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [WORKING ON MANNING ST BTWN S 19TH ST & S 20TH ST.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.948402/-75.174276,39.948195/-75.172713,39.948528/-75.172501,
      39.948807/-75.174182]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201076]

Type of Work--[REPAIR SWR LATERAL]                           Depth--[5-6FT]
Extent of Excavation--[4FT X 6FT]       Method of Excavation--[HAND,COMPRESSOR]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[]

              Lawful Start Dates--[25-Jan-10] Through [03-Feb-10]
    Scheduled Excavation Date--[25-Jan-10] Dig Time--[0800] Duration--[1 DAY]
                         Response Due Date--[22-Jan-10]

Caller--[NORMAN COLE]                      Phone--[267-819-7409] Ext--[]
Excavator--[NORMAN COLE PLUMBING & HTG]     Homeowner/Business--[B]
Address--[912 BULLOCK AVE]
City--[YEADON ]                      State--[PA] Zip--[19050]
FAX--[215-389-1514]  Email--[none]
Work Being Done For--[DAVID ALPINE]

Person to Contact--[NORMAN COLE]               Phone--[267-819-7409] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-Jan-10] at [1301] by [STACY SURMICK]

Remarks--
     []

ATM0  ATM=AT&T ATLANTA     BL 0  BL =COMCAST CABLE    EI 0  EI =AT&T LOCAL SVCS
KA 0  KA =PECO SCKL        LKC0  LKC=LEVEL 3 COMM     MI 0  MI =MCI/VERIZON BUS
MMF0  MMF=ABOVENET COMM    PD 0  PD =PHILA C WTR DPT  PSD0  PSD=PHILADELPHIA ST
PZ 0  PZ =PGW PHLA         SEP0  SEP=SEPTA            TQ 0  TQ =TRIGEN PHILA   
YA 0  YA =VERIZON PA CHNT

Serial Number--[20100201076]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========