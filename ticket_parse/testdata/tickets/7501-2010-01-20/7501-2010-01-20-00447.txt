
KD     00033 POCS 01/20/10 12:59:24 20100201066-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201066]-[000] Channel#--[1257WEB][0073]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[BUCKS]           Municipality--[BUCKINGHAM TWP]
Work Site--[ROUTE 263]
     Nearest Intersection--[SUGAR BOTTOM ROAD]
     Second Intersection--[EDISON FURLONG ROAD]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [RT 263 - TOTAL ROADWAY FROM OLD YORK ROAD IN WARMINSTER TWP THRU WARWICK
      TWP TO APPROX 1000 FT INTO BUCKINGHAM TWP]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.285270/-75.085690,40.280642/-75.085313,40.279693/-75.083655,
      40.285068/-75.083504,40.285068/-75.083542]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201066]

Type of Work--[ROADWAY-DRAINAGE-SIGNAL RECONSTRUCTION]       Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[ ] Other--[R/W-HWY]

              Lawful Start Dates--[         ] Through [         ]
                      Scheduled Excavation Date--[DESIGN]
                         Response Due Date--[03-Feb-10]

Caller--[KEVIN NEHF]                       Phone--[717-691-1340] Ext--[]
Excavator--[KCI TECHNOLOGIES INC]           Homeowner/Business--[B]
Address--[5001 LOUISE DR STE 201]
City--[MECHANICSBURG]                State--[PA] Zip--[17055]
FAX--[717-691-3470]  Email--[kevin.nehf@kci.com]
Work Being Done For--[PENN DOT]

Person to Contact--[SCOTT RABOCI]              Phone--[717-691-1340] Ext--[3103]
Best Time to Call--[0800-1600]

Prepared--[20-Jan-10] at [1259] by [KEVIN NEHF]

Remarks--
     [THIS IS ROADWAY AND DRAINAGE AND SIGNAL RECONSTRUCTION OF RT 263 FORM OLD
      YORK RD IN WARMINSTER TWP THRU WARWICK TWP AND APPROX 1000 FT INTO
      BUCKINGHAM TWP. PLEASE MARK UTILITIES IN THE FIELD AND SEND APPLICABLE
      PLANS.]

BTB0  BTB=BUCKINGHAM T     FP 0  FP =BUCKS CNTY W&SA  KD 0  KD =PECO WRTR      
SF 0  SF =COMCAST IVYLAND  YI 0  YI =VERIZON HRSM

Serial Number--[20100201066]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========