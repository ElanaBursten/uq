
KC     00057 POCS 01/20/10 13:01:16 20100201075-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201075]-[000] Channel#--[1257029][0203]

Message Type--[NEW][EXCAVATION][ROUTINE]

County--[MONTGOMERY]      Municipality--[LOWER PROVIDENCE TWP]
Work Site--[EAGLE STREAM DR]
     Nearest Intersection--[RIDGE PIKE]
     Second Intersection--[DEFFORD PL]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [WORKING ON EAGLE STREAM DR FROM RIDGE PIKE TO DEFFORD PL.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.157751/-75.404804,40.155718/-75.406936,40.154771/-75.405642,
      40.156957/-75.403510]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201075]

Type of Work--[EXCAVATE TO LOCATE STORM PIPE]                Depth--[5FT]
Extent of Excavation--[3FT X 300FT ]    Method of Excavation--[SMALL MACHINE]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[R/W]

              Lawful Start Dates--[25-Jan-10] Through [03-Feb-10]
    Scheduled Excavation Date--[25-Jan-10] Dig Time--[0900] Duration--[1 DAY]
                         Response Due Date--[22-Jan-10]

Caller--[SUSANNE CREVELING]                Phone--[610-222-0341] Ext--[]
Excavator--[HIGH TECH CONSTRUCTION]         Homeowner/Business--[B]
Address--[PO BOX 1210]
City--[WORCESTER]                    State--[PA] Zip--[19490]
FAX--[610-222-0351]  Email--[screveling@htccompany.com]
Work Being Done For--[HIGH TECH CONSTRUCTION]

Person to Contact--[SUSANNE CREVELING]         Phone--[484-576-7906] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-Jan-10] at [1301] by [MELISSA PERRIS]

Remarks--
     []

AP 0  AP =PAWC NORRISTOWN  CS 0  CS =COMCAST CABLE N  HRN0  HRN=COMCAST- FIBER 
KC 0  KC =PECO PLMG        LPR0  LPR=LOWER PROVIDENC  RW10  RW1=L PROVIDNCE TA 
YJ 0  YJ =VERIZON

Serial Number--[20100201075]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========