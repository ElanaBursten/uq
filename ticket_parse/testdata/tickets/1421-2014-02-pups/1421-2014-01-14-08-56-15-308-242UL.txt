
PEH51 5 SC811 Voice 12/31/2013 04:02:00 PM 1312311427 Emergency 

Notice Number:    1312311427        Old Notice:                        
Sequence:         5                 Created By:      RGO               

Created:          12/31/13 04:07 PM                                     
Work Date:        12/31/13 04:02 PM                                     
Update on:        01/17/2014        Good Through:    01/23/2014        

Caller Information:
PALMETTO ELECTIC
111 MATHEWS DRIVE
HILTON HEAD ISLAND, SC 29925
Company Fax:                        Type:            Excavator         
Caller:           JOYCE GRANT         Phone:         (843) 681-0078 Ext:
Caller Email:     jgrant@palmetto.coop                                  

Site Contact Information:
DAVID WHITE                              Phone:(843) 384-3104 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  BEAUFORT             Place:  HILTON HEAD ISLAND
Street:  11,13,12 TWIN PINES CT      Address In Instructions:false             
Intersection:  TWIN PINES RD                                         
Subdivision:   SEA PINES PLANTATION                                  

Lat/Long: 32.13785,-80.802827
Second:  32.139015,-80.802115

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     ELECTRIC, REPLACE SERVICE                                   
Work Done By:  PALMETTO ELECTIC AND INFRATECHDuration:        2-5 DAYS          
Work Done For:                                                       

Instructions:
FOR EACH MARK ENTIRE PROPERTY                                                 

Directions:
THESE ARE SIDE BY SIDE                                                        

Remarks:
**EMERGENCY SITUATION DUE TO:REPLACE ELECTRICAL SERVICE RESTORING EXISTING    
SERVICE-CREW ON SITE 730 AM 1/2/14 **                                         

Member Utilities Notified:
HRGZ57 SPSD29 PEH51 TWBZ51 

