
NOTICE OF INTENT TO EXCAVATE      OMBN 
Ticket No: 2202897 
Transmit   Date: 05/12/11         Time: 03:27 PM           Op: mdshan 
Release    Date: 05/12/11         Time: 03:27 PM           Op: mdshan 
Response Due By: 05/16/11         Time: 11:59 PM 
Expiration Date: 05/30/11         Time: 11:59 PM 

Place: TEST 
Address:             Street: TEST DR 
Nearest Intersecting Street: TEST RD 

Type of Work: TEST TICKET - PLEASE CONTACT SHANNON STULTZ, BELOW. 
Extent of Work: THIS IS A TEST FOR THE OMBN HEADER. PLEASE CONFIRM ACCURATE
: RECEIPT OF THIS TICKET BY EMAILING SHANNONSTULTZ@MISSUTIILTY.NET OR FAXING
: 410-712-0062. WRITTEN CONFIRMATION IS REQUIRED. 
Comments: COMMENTS 1 LINE 
: COMMENTS 2 LINE 

Company      : MISS UTILITY CENTER 
Contact Name : SHANNON STULTZ                           Fax: (410)712-0062 
Contact Phone: (410)782-2057                            Ext:  
Caller Address: 7223 PARKWAY DR 
                HANOVER, MD 21076 
Email Address:  shannonstultz@missutility.net 
Alt. Contact :  CALLER ID FOR SHANNON S ONLY !   Alt. Phone:  
Work Being Done For: YOUR CONFIRMATION IS REQUIRED 
State: MD              County: ANNE ARUNDEL 
MNG:  N 
Ticket Mapping    :  Map Name: AA    Map#: 5059 Grid Cells: H2 
Computer Generated:  Map Name: AA    Map#: 5059 Grid Cells: H2 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.1812885 Lon:-76.4562415 SE Lat: 39.1750164 Lon:-76.4500092 
Explosives: N 
CCL01      HCFN01     HCU01      KFD01      KFD01C     LTC01      LTC02 
 VAA 
Send To: VCH       Seq No: 0002   Map Ref:  
