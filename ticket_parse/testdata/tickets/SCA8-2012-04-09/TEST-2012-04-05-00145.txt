
NEVBEL 00041 USAN 04/04/12 14:06:22 0114315 NORMAL NOTICE 

Message Number: 0114315 Received by USAN at 13:47 on 04/04/12 by MHS

Work Begins:    04/09/12 at 08:00   Notice: 021 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 05/02/12 at 23:59   Update By: 04/30/12 at 23:59

Caller:         BENJAMIN FLORENCE        
Company:        FLORENCE FENCE                     
Address:        2597 NOWLIN RD                          
City:           MINDEN                        State: NV Zip: 89423
Business Tel:   775-267-9918                  Fax: 775-882-8117                
Email Address:                                                              

Nature of Work: VERTICAL BORING DIG FENCE POST          
Done for:       ROBERT EBBERT                 Explosives: N
Foreman:        MIKE BARREDO             
Field Tel:                                    Cell Tel: 775-315-7888           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         1822 MEADOWVALE WAY
  Cross Street:         19TH ST

    FRT & BACK/O ADDR 

Place: SPARKS                       County: WASHOE               State: NV

Long/Lat Long: -119.768328 Lat:  39.548085 Long: -119.76797  Lat:  39.548266 


Sent to:
CHARNO = CHARTER COMM-RENO            CTYSPA = CITY SPARKS                  
COWASH = COUNTY WASHOE                NENGNO = N V ENERGY - NV              
NEVBEL = NEVADA BELL                  TMWAUT = TRUCKEE MEADOWS WATER AU     




At&t Ticket Id: 25149723   clli code: SPRKNV11