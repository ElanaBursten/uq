
NEVBEL 00037 USAN 04/04/12 13:07:51 0114220 NORMAL NOTICE 

Message Number: 0114220 Received by USAN at 13:01 on 04/04/12 by DWS

Work Begins:    04/06/12 at 17:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 05/02/12 at 23:59   Update By: 04/30/12 at 23:59

Caller:         CASEY JONES              
Company:        TRUCKEE MEADOWS WATER AUTHORITY    
Address:        1355 CAPITOL BLVD                       
City:           RENO                          State: NV Zip: 89520
Business Tel:   775-834-8149                  Fax: 775-834-8050                
Email Address:                                                              

Nature of Work: DIG TO INVESTIGATE WTR LEAK             
Done for:       SAME                          Explosives: N
Foreman:        UNKNOWN                  
Field Tel:                                    Cell Tel:                        
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Street Address:         6490 ENCHANTED VALLEY DR
  Cross Street:         VALLEY CREEK RD

    WRK LOC ST AREA FRT/O ADDR EXT INTO PROP APP 10' FR ST

Place: RENO                         County: WASHOE               State: NV

Long/Lat Long: -119.891739 Lat:  39.523139 Long: -119.890705 Lat:  39.5241   


Sent to:
CHARNO = CHARTER COMM-RENO            CTYRNO = CITY RENO                    
COWASH = COUNTY WASHOE                NENGNO = N V ENERGY - NV              
NEVBEL = NEVADA BELL                  TMWAUT = TRUCKEE MEADOWS WATER AU     




At&t Ticket Id: 25149436   clli code: RENONV12