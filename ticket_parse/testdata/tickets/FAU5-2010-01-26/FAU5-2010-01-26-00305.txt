
AGL113  01934 GAUPC 01/26/10 15:56:06 01260-245-041-000 EMERG 
Underground Notification             
Notice : 01260-245-041 Date: 01/26/10  Time: 15:54  Revision: 000 

State : GA County: BUTTS         Place: JENKINSBURG                             
Addr  : From: 1993   To:        Name:    HIGHWAY 42                          N  
Near  : Name:    RIVERS                         RD  

Subdivision:                                         
Locate:  RIGHT OF WAY ADDRESS SIDE FROM THE ADDRESS GOING EAST FOR 700FT  *** P
      :  LEASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, 
      :  PLEASE RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-
      :  461-7271. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA THE SA
      :  ME METHODS. ***                                                        

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3319B8400A 3319B8400B 3319B8401C 3319B8401D 3319C8400A 
Grids       : 3319C8400B 3319C8401C 3319C8401D 
Work type   : RPR PHONE SERVICE                                                   

Start date: 01/26/10 Time: 00:00 Hrs notc : 000
Legal day : 01/26/10 Time: 15:51 Good thru: 01/29/10 Restake by: 01/26/10
RespondBy : 01/26/10 Time: 15:51 Duration : UNKNOWN    Priority: 1
Done for  : AT&T                                    
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : DURATION IS 7 HRS EMERGENCY: OUT OF SERVICE/CREW IS ON SITE         

Company : ANSCO AND ASSOCIATES                      Type: CONT                
Co addr : 1604 OLD ATLANTA RD                      
City    : GRIFFIN                         State   : GA Zip: 30233              
Caller  : KELLY TINGLE                    Phone   :  770-233-1741              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact : KELLY TINGLE                    678-588-9609              

Submitted date: 01/26/10  Time: 15:54  Oper: 198
Mbrs : AGL113 BGAWS BTC40 GAUPC GP141S JNKS50 
-------------------------------------------------------------------------------
