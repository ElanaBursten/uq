
ATTDSOUTH 00556A USAS 10/05/12 13:49:31 A22790739-00A NORM NEW GRID

Ticket : A22790739  Date: 10/05/12 Time: 13:41 Oper: KAW Chan: 201
Old Tkt: A22790739  Date: 10/05/12 Time: 13:49 Oper: KAW Revision: 00A

Company: SAN DIEGO TREE CARE            Caller: H/O - SUZANNE MILSTEIN
Co Addr:
City&St:                                Zip:
Phone: 858-459-2082 Ext:      Call back: ANYTIME
Formn: CLINT REED           Phone: 858-530-8733
Email: MILSTEINSUZANNE@YAHOO.COM

State: CA County: SAN DIEGO       Place: LA JOLLA
Delineated: N
Address: 6858        Street:AVENIDA ANDORRA
X/ST 1 : VIA DON BENITO
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 1247H012
Lat/Long  : 32.836147/-117.252769 32.836348/-117.251428
          : 32.834934/-117.252586 32.835135/-117.251245
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REMOVE LARGE PINE TREE
Wkend: N  Night: N
Work date: 10/17/12 Time: 08:00 Hrs notc: 282 Work hrs: 162 Priority: 2
Instruct : MARK BY                        Permit: UNKNOWN
Done for : H/O - SUZANNE MILSTEIN

Tkt Exp: 11/02/12

Mbrs : ATTDSOUTH     SDG01  SDGET  SND01  UTWCSD


At&t Ticket Id: 25656257   clli code: LAJLCA11   OCC Tkt No: A22790739-00A
ATTDSOUTH: CONDUIT