
PEHZ51 3 SC811 Voice 08/04/2014 09:09:00 AM 1408040503 Emergency 

Notice Number:    1408040503        Old Notice:                        
Sequence:         3                 Created By:      AOK               

Created:          08/04/14 09:14 AM                                     
Work Date:        08/04/14 09:09 AM                                     
Update on:        08/20/2014        Good Through:    08/25/2014        

Caller Information:
South Island PSD
P.O. BOX 5148
HILTON HEAD, SC 29938
Company Fax:                        Type:            Contractor        
Caller:           Pam Nelson          Phone:         (843) 671-2907 Ext:2
Caller Email:     OPERATIONS@SOUTHISLANDPSD.COM                         

Site Contact Information:
Pam Nelson                              Phone:(843) 671-2907 Ext:2
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  BEAUFORT             Place:  HILTON HEAD ISLAND
Street:  10 TRIMBLE STONE LN        Address In Instructions:false             
Intersection:  LONG COVE DR                                          
Subdivision:   LONG COVE CLUB PLANTATION                             

Lat/Long: 32.174954,-80.739544
Second:  0, 0

Explosives:  N Premark:  N Drilling/Boring:  N Near Railroad:  N

Work Type:     WATER, REPAIR VALVE                                     
Work Done By:  South Island PSD     Duration:        3 HOURS           
Work Done For:                                                       

Instructions:
MARK A 20 FT RADIUS OF VALVE IN THE MIDDLE OF ROAD IN FRONT OF THIS           
ADDRESS

**EMERGENCY SITUATION DUE TO:REPAIRING WATER VALVE & RESTORING       
EXISTING SERVICE// CREW IS IN ROUTE**                                         

Directions:
ADDITIONAL STREET:N/A                                                         

Remarks:

Member Utilities Notified:
HRGZ57 SPSD29 PEHZ51 TWBZ51 

