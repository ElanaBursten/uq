
------=_Part_5313_11442337.1349380558425
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 573 PUPS Voice 10/04/2012 03:41:00 PM 1210041831 Normal 

Ticket Number: 1210041831
Old Ticket Number: 
Created By: KJJ
Seq Number: 573

Created Date: 10/04/2012 03:45:10 PM
Work Date/Time: 10/09/2012 11:59:59 PM
Update: 10/25/2012 Good Through: 10/30/2012

Excavation Information:
State: SC     County: CHEROKEE
Place: BLACKSBURG
Address Number: 
Street: W CHEROKEE ST
Inters St: KILLIAN ST
Subd: 

Type of Work: TELEPHONE, INSTALL SPLICE PIT
Duration: APPROX 1DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: ANSCO & ASSOCIATES

Remarks/Instructions: ADD RD:INSTALLING TELEPHONE CABLE FOR ATT// ADD RD:W    
LIME ST// MARK THE ENTIRE INTERSECION 50FT IN ALL DIRECTIONS                  
                                                                              
CHEROKEE ST  AKA HWY 29                                                       


Caller Information: 
Name: RON REES                              ANSCO & ASSOCIATES                    
Address: 9008 FAIRFOREST RD
City: SPARTANBURG State: SC Zip: 29301
Phone: (864) 574-8805 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:RON REES Email: JANICE.REES@ANSCOLLC.COM
Call Back:BEST# 864-574-8805 (OFFICE) // ALT# 864-809-2173 (CELL) Fax: 

Grids: 
Lat/Long: 35.1124904708583, -81.5317909846935
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60 MCI18 TBK23 YOR56                      


Map Link: (NEEDS DEVELOPMENT)




------=_Part_5313_11442337.1349380558425--