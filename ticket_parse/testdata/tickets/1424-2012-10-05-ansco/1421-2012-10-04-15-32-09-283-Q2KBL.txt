
------=_Part_3848_1954366.1349379106811
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 540 PUPS Voice 10/04/2012 03:21:00 PM 1210041746 Normal 

Ticket Number: 1210041746
Old Ticket Number: 
Created By: JKG
Seq Number: 540

Created Date: 10/04/2012 03:26:56 PM
Work Date/Time: 10/09/2012 11:59:59 PM
Update: 10/25/2012 Good Through: 10/30/2012

Excavation Information:
State: SC     County: SPARTANBURG
Place: SPARTANBURG
Address Number: 7122
Street: LONE OAK RD
Inters St: CHARISMA DR
Subd: 

Type of Work: TELEPHONE, REPLACE POLE(S) & ANCHOR(S)
Duration: APPROX 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: ANSCO & ASSOCIATES

Remarks/Instructions: THIS IS FOR AT&T// ADDITIONAL ROAD: HWY I 85 AKA N PINE 
ST// MARK THE REAR OF THE PROPERTY// DRILLING// CALL CALLER IF ANY QUESTIONS  


Caller Information: 
Name: RON REES                              ANSCO & ASSOCIATES                    
Address: 9008 FAIRFOREST RD
City: SPARTANBURG State: SC Zip: 29301
Phone: (864) 574-8805 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:RON REES Email: JANICE.REES@ANSCOLLC.COM
Call Back:BEST# 864-574-8805 (OFFICE) // ALT# 864-809-2173 (CELL) Fax: 

Grids: 
Lat/Long: 34.98592704087, -81.9727489156601
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60 PNSZ82 SCTDZ03                         


Map Link: (NEEDS DEVELOPMENT)




------=_Part_3848_1954366.1349379106811--