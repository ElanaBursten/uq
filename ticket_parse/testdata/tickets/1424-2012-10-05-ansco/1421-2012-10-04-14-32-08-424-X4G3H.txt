
------=_Part_16_1046483.1349375483515
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 294 PUPS Remote 10/04/2012 11:33:00 AM 1210041021 Normal 

Ticket Number: 1210041021
Old Ticket Number: 
Created By: R-RLW
Seq Number: 294

Created Date: 10/04/2012 11:35:55 AM
Work Date/Time: 10/09/2012 11:59:59 PM
Update: 10/25/2012 Good Through: 10/30/2012

Excavation Information:
State: SC     County: PICKENS
Place: PICKENS
Address Number: 199
Street: RED BIRD HILL LANE
Inters St: WREN LANE
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: ANSCO & ASSOCIATES

Remarks/Instructions: WORK IS FOR ATT // JOB NUMBER SA1203606                 
                                                                              
MARK THE ENTIRE PROPERTY AND BOTH SIDES OF THE ROAD // TERMINAL ADDRESS IS    
4390 PUMPKINTOWN // DROP WILL BE BURIED FROM DPOST TO ONI // LARK TRAIL AND   
HIGHWAY 11 ARE ADDITIONAL ROADS IN THE AREA                                   


Caller Information: 
Name: REBECCA WAMPLER                       ANSCO & ASSOCIATES                    
Address: 43 SENTELL RD
City: GREENVILLE State: SC Zip: 29611
Phone: (864) 295-0235 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:REBECCA WAMPLER Email: REBECCALEEWAMPLER@GMAIL.COM
Call Back:8642950235 Fax: 

Grids: 
Lat/Long: 35.0360355041996, -82.6623394332117
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29                                                      


Map Link: (NEEDS DEVELOPMENT)




------=_Part_16_1046483.1349375483515--