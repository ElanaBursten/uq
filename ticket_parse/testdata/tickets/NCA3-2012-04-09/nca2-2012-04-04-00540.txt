
PBTWAL 00146 USAN 04/03/12 14:55:27 0112611 NORMAL NOTICE 

Message Number: 0112611 Received by USAN at 14:51 on 04/03/12 by VLERMA

Work Begins:    04/17/12 at 08:00   Notice: 093 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 05/01/12 at 23:59   Update By: 04/27/12 at 16:59

Caller:         JOHN FRITZ               
Company:        SWAN POOLS                         
Address:        1295 BOULEVARD WAY STE "C"              
City:           WALNUT CREEK                  State: CA Zip: 94595
Business Tel:   925-934-7926                  Fax: 925-934-5845                
Email Address:  JFRITZ@SWANNORCAL.COM                                       

Nature of Work: EXC FOR BK YD POOL                      
Done for:       P/O KERNER                    Explosives: N
Foreman:        MIKE MOREY               
Field Tel:                                    Cell Tel: 925-766-8249           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: 12BLD00189               
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         510 QUIMBY CT
  Cross Street:         CHANCERY WAY

    WRK AT THE BK YD/O THE PROP 

Place: SAN RAMON, CO AREA           County: CONTRA COSTA         State: CA

Long/Lat Long: -121.901648 Lat:  37.754739 Long: -121.901251 Lat:  37.755176 


Sent to:
CTYSRA = CITY SAN RAMON               COMLIV = COMCAST-LIVERMORE            
DERWA  = DERWA                        DUBSVC = DUBLIN-S/RAMON SVCS DIST     
PBTSJ2 = PACIFIC BELL SAN JOSE 2      PBTWAL = PACIFIC BELL WALNUT CRK      
PGEHAY = PGE DISTR HAYWARD            




At&t Ticket Id: 25146137   clli code: SNRMCA11