

From: IRTHNet  At: 03/13/13 03:22 PM  Seq No: 216

OUNC 
Ticket No: 13046863               ++EMERGENCY++ 
Send To: QLNOR14    Seq No:   35  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:21 pm    Op: orcbtm 
Original Call Date:  3/13/13   Time:  1:16 pm    Op: orcbtm 
Work to Begin Date:  3/13/13   Time:  1:30 pm 

State: OR            County: MARION                  Place: SALEM 
Address: 2224        Street: BREYMAN ST NE 
Nearest Intersecting Street: ROSE ST NE 

Twp: 7S    Rng: 3W    Sect-Qtr: 25-NW,26-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.9377395 Lon:-123.0122354 SE Lat: 44.9370385 Lon:-123.0114564 

Type of Work: REPAIR SEWER SERVICE 
Location of Work: EXCAVATION SITE IS ON THE S SIDE OF THE ROAD. 
: ADD IS APX 250 FT W OF ABV INTER. MARK ENTIRE FRONT YARD AND STREET IN FRONT
: OF ABV ADD. CUSTOMER OUT OF SERVICE. 

Remarks: BEST INFORMATION AVAILABLE 
: ++CALLER REQUESTS AREA MARKED A.S.A.P!++ 

Company     : ROTO-ROOTER 
Contact Name: RUSTY JOHNSON                    Phone: (503)682-9774 
Cont. Email : rjohnson@hswcorp.com 
Alt. Contact: RUSTY - CELL                     Phone: (503)209-5999 
Contact Fax : (503)685-9754 
Work Being Done For: WINDMILL INVESTMENTS 
Additional Members:  
CMCST02    NWN01      PGE04      SALEM01
