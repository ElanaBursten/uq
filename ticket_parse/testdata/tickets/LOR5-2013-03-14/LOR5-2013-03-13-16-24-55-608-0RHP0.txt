

From: IRTHNet  At: 03/13/13 03:24 PM  Seq No: 221

OUNC 
Ticket No: 13046595               48 HOUR NOTICE 
Send To: QLNOR16    Seq No:    1  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:22 am    Op: orshan 
Original Call Date:  3/13/13   Time: 10:15 am    Op: orshan 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: MORROW                  Place: LEXINGTON 
Address: 325         Street: W AIRPORT WAY 
Nearest Intersecting Street: HWY 207 

Twp: 1S    Rng: 25E   Sect-Qtr: 26-NW,27-SE-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.4553921 Lon:-119.6844948 SE Lat: 45.4506642 Lon:-119.6784124 

Type of Work: INSTALL DRAINAGE 
Location of Work: EXCAVATION SITE IS ON THE W SIDE OF THE ROAD. 
: SITE IS APX 150 YDS N FROM THE ABV INTER APX 250 YDS OFF AIRPORT WAY DOWN
: DRIVEWAY. MARK APX 6 FT OFF THE ENTIRE  N SIDE OF HOUSE AND APX 25 FT OFF
: ENTIRE FRONT OF GARAGE. AREA WILL BE MARKED IN WHITE AT BOUNDRIES 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : DAVE PIPER CONTRACTING 
Contact Name: DAVE PIPER                       Phone: (541)676-8336 
Cont. Email :  
Alt. Contact: PAM PIPER                        Phone:  
Contact Fax :  
Work Being Done For: JOHN BOYER 
Additional Members:  
CBEC01     POM01      PTI05      WWCOM01
