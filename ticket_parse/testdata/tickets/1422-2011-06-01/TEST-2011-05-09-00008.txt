
From: IRTHNet  At: 05/09/11 02:52 PM  Seq No: 8

SCA88 52 PUPS Voice 05/09/2011 02:48:00 PM 1105092442 Normal 

Ticket Number: 1105092442
Old Ticket Number: 
Created By: SLC
Seq Number: 52

Created Date: 05/09/2011 02:52:12 PM
Work Date/Time: 05/12/2011 03:00:36 PM
Update: 05/31/2011 Good Through: 06/03/2011

Excavation Information:
State: SC     County: BERKELEY
Place: DANIEL ISLAND
Address Number: 
Street: ITHECAW CT
Inters St: ITHECAW CREEK ST
Subd: 

Type of Work: WATER, INSTALL SERVICE
Duration: APPR 2 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: TIDELAND UTILITIES

Remarks/Instructions: THIS IS LOT 1201 // MARKING THE FRONT OF THE PROPERTY   
AND BOTH SIDES OF THE ROAD                                                    


Caller Information: 
Name: MARK DILL                             TIDELAND UTILITIES                    
Address: 98 PEYTONS WAY
City: SUMMERVILLE State: SC Zip: 29483
Phone: (843) 873-8047 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:MARK DILL Email: MARK@TIDELANDUTILITIES.COM
Call Back:(843) 200-1685 (CELL PHONE) Fax: 

Grids: 
Lat/Long: 32.8912941264699, -79.9186912619747
Secondary: 32.855833575987, -79.8869202485362
Lat/Long Caller Supplied: N

Members Involved: BSZN42 COMZ41 CPWZ69 HMT85 HOT21 ITCC76 MPT96 SCA88 SCEDZ95 
SCER09 SCGT90 SCGZ90 SCTDZ06                                                  


Map Link: (NEEDS DEVELOPMENT)