
From: IRTHNet  At: 05/09/11 02:53 PM  Seq No: 9

SCA88 53 PUPS Voice 05/09/2011 02:45:00 PM 1105092445 Normal 

Ticket Number: 1105092445
Old Ticket Number: 
Created By: WGS
Seq Number: 53

Created Date: 05/09/2011 02:52:28 PM
Work Date/Time: 05/12/2011 02:45:50 PM
Update: 05/31/2011 Good Through: 06/03/2011

Excavation Information:
State: SC     County: LEXINGTON
Place: CHAPIN
Address Number: 209
Street: COLUMBIA AVE
Inters St: CLARK STREET
Subd: 

Type of Work: SEE REMARKS
Duration: A COUPLE OF DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: WATTS BROTHERS CABLE CONSTRUCTION

Remarks/Instructions: MARK A 50 FT RADIUS AROUND POLE #53833 LOCATED IN FRONT 
OF PROPERTY//ADDITIONAL PEAK STREET//POSSIBLE DRILLING                        
                                                                              
**TYPE OF WORK:  PLACING A POLE AND REMOVING A POLE**                         
                                                                              
JOB #19D23050N                                                                


Caller Information: 
Name: DEBBIE MITCHUM                        WATTS BROTHERS CABLE CONSTRUCTION     
Address: 105 CORPORATE BLVD
City: W. COLUMBIA State: SC Zip: 29169
Phone: (803) 978-2846 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:DEBBIE MITCHUM Email: WATTSBROS@YAHOO.COM
Call Back: Fax: 

Grids: 
Lat/Long: 34.1703317458815, -81.3519880983196
Secondary: 34.1676561605957, -81.3322957906158
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCA88 SCEDZ08 SCGZ05 TWCZ40                    


Map Link: (NEEDS DEVELOPMENT)