
From: IRTHNet  At: 05/09/11 12:52 PM  Seq No: 2

SCA88 46 PUPS Web 05/09/2011 12:51:00 PM 1105091930 Normal 

Ticket Number: 1105091930
Old Ticket Number: 
Created By: RMD
Seq Number: 46

Created Date: 05/09/2011 12:52:00 PM
Work Date/Time: 05/12/2011 01:00:28 PM
Update: 05/31/2011 Good Through: 06/03/2011

Excavation Information:
State: SC     County: AIKEN
Place: AIKEN
Address Number: 334
Street: PENDLETON ST NW
Inters St: EDGEFIELD AVE NW, ABBEVILLE AVE NW
Subd: 

Type of Work: WATER, REPAIR LEAK
Duration: 8 HRS

Boring/Drilling: N Blasting: N White Lined: Y Near Railroad: N

Work Done By: CITY OF AIKEN

Remarks/Instructions: LOCATE FRONT OF THE PROPERTY, BOTH SIDES OF THE STREET, 
AREA IS WHITE-LINED, INCLUDING  MEDIAN                                        


Caller Information: 
Name: HEATHER HAWKINS                       CITY OF AIKEN                         
Address: 655 KERSHAW ST NE
City: AIKEN State: SC Zip: 29801
Phone: (803) 642-7617 Ext:  Type: Business
Fax:  Caller Email: HHAWKINS@CITYOFAIKENSC.GOV

Contact Information:
Contact:JOHN STONE Email: HHAWKINS@CITYOFAIKENSC.GOV
Call Back: Fax: 

Grids: 
Lat/Long: 33.5697885188357, -81.7238181672387
Secondary: 33.5619420875638, -81.7201912127377
Lat/Long Caller Supplied: N

Members Involved: ABB64 BSZC46 SCA88 SCEDZ36 SCGZ35                           


Map Link: (NEEDS DEVELOPMENT)