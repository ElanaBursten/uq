
From: IRTHNet  At: 05/09/11 03:50 PM  Seq No: 15

SCA88 59 PUPS Voice 05/09/2011 03:48:00 PM 1105092633 Cancel 

Ticket Number: 1105092633
Old Ticket Number: 1105092465
Created By: WGS
Seq Number: 59

Created Date: 05/09/2011 03:50:19 PM
Work Date/Time: 05/09/2011 04:00:55 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: CHAPIN
Address Number: 15
Street: COLUMBIA AVE
Inters St: ROLAND SHEALY COURT
Subd: 

Type of Work: ELECTRIC, INSTALL POLE(S) & ANCHOR(S)
Duration: A COUPLE OF DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: WATTS BROTHERS CABLE CONSTRUCTION

Remarks/Instructions: MARK A 75 FT RADIUS AROUND POLE #11553 LOCATED IN REAR  
OF PROPERTY//ADDITIONAL PEAK ST, PARK ST AND E BOUNDARY//POSSIBLE DRILLING    
                                                                              
                                                                              
                                                                              
JOB #19D23050N                                                                
                                                                              
**CANCEL TKT 1105092465 DUE TO: CHANGED TYPE OF WORK//NEW TKT                 
1105092627***(05-09-11)                                                       


Caller Information: 
Name: DEBBIE MITCHUM                        WATTS BROTHERS CABLE CONSTRUCTION     
Address: 105 CORPORATE BLVD
City: W. COLUMBIA State: SC Zip: 29169
Phone: (803) 978-2846 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:DEBBIE MITCHUM Email: WATTSBROS@YAHOO.COM
Call Back: Fax: 

Grids: 
Lat/Long: 34.1697372193207, -81.351534871065
Secondary: 34.1674897276806, -81.3329127974756
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCA88 SCEDZ08 SCGZ05 TWCZ40                    


Map Link: (NEEDS DEVELOPMENT)