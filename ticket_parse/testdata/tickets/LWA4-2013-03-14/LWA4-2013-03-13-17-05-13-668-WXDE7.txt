

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 331

IEUCC 
Ticket No: 13056362               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   94  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:10 pm    Op: webusr 
Original Call Date:  3/13/13   Time:  1:07 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 4815        Street: N ALLEN PL 
Nearest Intersecting Street: CIRCLE 

Twp: 26N   Rng: 42E   Sect-Qtr: 35-SE 
Twp: *MORE Rng: 42E   Sect-Qtr: 2-NE,1-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7027582 Lon:-117.4589632 SE Lat: 47.7010762 Lon:-117.4555568 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 167FT NORTHWEST OF CIRCLE ON THE SOUTHWEST SIDE OF
: ALLEN.  MARK 5FT RADIUS OF GAS METER LOCATED SOUTH OUT.  METER IS FLAGGED.
: CONTACT BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #442323192 
Additional Members:  
AVISTA08   SPOKAN01   SPOKAN02   SPOKAN03
