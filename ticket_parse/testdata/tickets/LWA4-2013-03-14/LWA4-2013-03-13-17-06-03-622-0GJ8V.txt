

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 349

IEUCC 
Ticket No: 13056080               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   52  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:42 am    Op: ortyle 
Original Call Date:  3/13/13   Time: 10:33 am    Op: ortyle 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE VALLEY 
Address: 5825        Street: N ARGONNE ROAD 
Nearest Intersecting Street: COLUMBIA DR 

Twp: 26N   Rng: 44E   Sect-Qtr: 31-NE,32-SW-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7112622 Lon:-117.2833300 SE Lat: 47.7085866 Lon:-117.2784267 

Type of Work: NEW CONST OF GARAGE 
Location of Work: EXCAVATION SITE IS ON THE W SIDE OF THE ROAD. 
: DRIVEWAY TO ADD IS APX 100FT N FROM ABV INTER. MARK UTILITIES ON NE SIDE OF
: HOUSE AT ABV ADD. CALLER STATES HE CANNOT CURRENTLY MARK IN WHITE BECAUSE
: THERE IS DEBRIS IN THE WAY. ++CALL PRIOR TO LOCATING TO MEET ELIOT,
: HOMEOWNER, TO BE SHOWN THE SITE LOCATION.++ 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : HOMEOWNER 
Contact Name: ELIOT MOHR                       Phone: (509)220-4166 
Cont. Email :  
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: ELIOT MOHR 
Additional Members:  
AVISTA12   CC7730     INLND02    PPIRR01    SPENG01    SPENG02
