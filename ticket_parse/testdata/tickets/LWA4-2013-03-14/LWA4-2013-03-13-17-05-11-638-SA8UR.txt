

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 329

IEUCC 
Ticket No: 13056357               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   91  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:07 pm    Op: webusr 
Original Call Date:  3/13/13   Time:  1:04 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 5911        Street: N FLEMING ST 
Nearest Intersecting Street: BISMARK 

Twp: 26N   Rng: 42E   Sect-Qtr: 35-NW 
Twp: 26N   Rng: 42E   Sect-Qtr: 35-NW-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7131343 Lon:-117.4676756 SE Lat: 47.7102189 Lon:-117.4628243 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 260FT SOUTHEAST OF BISMARK ON THE SOUTH SIDE OF
: FLEMING.  MARK 5FT RADIUS OF GAS METER LOCATED NORTHWEST OUT.  METER IF
: FLAGGED.   CONTACT BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #393322791 
Additional Members:  
AVISTA08   SPOKAN01   SPOKAN02   SPOKAN03   XO01
