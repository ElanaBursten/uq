

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 333

IEUCC 
Ticket No: 13056377               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   96  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:18 pm    Op: webusr 
Original Call Date:  3/13/13   Time:  1:14 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 1303        Street: W BISMARK 
Nearest Intersecting Street: ADAMS 

Twp: 26N   Rng: 43E   Sect-Qtr: 31-NW 
Twp: *MORE Rng: 43E   Sect-Qtr: 31-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7139130 Lon:-117.4336215 SE Lat: 47.7117078 Lon:-117.4291220 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 53FT WEST OF ADAMS ON THE SOUTH SIDE OF BISMARK.
: MARK 5FT RADIUS OF GAS  METER LOCATED SOUTH OUT.  METER IS FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #148322799 
Additional Members:  
AVISTA08   CC7730     SPENG01    SPENG02    SPOKAN01   SPOKAN02   SPOKAN03 
 XO01
