

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 325

IEUCC 
Ticket No: 13056331               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   86  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:57 pm    Op: webusr 
Original Call Date:  3/13/13   Time: 12:53 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 6124        Street: N POST ST 
Nearest Intersecting Street: DALKE 

Twp: 26N   Rng: 43E   Sect-Qtr: 31-NW 
Twp: 26N   Rng: 43E   Sect-Qtr: 31-NW-NE,30-SW-SE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7157639 Lon:-117.4249640 SE Lat: 47.7125993 Lon:-117.4221494 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 250FT NORTH OF DALKE ON THE EAST SIDE OF POST.
: MARK 5FT RADIUS OF GAS METER LOCATED SOUTHOUT.. METER IS FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #687322883 
Additional Members:  
AVISTA08   FBRLNK01   SPENG01    SPENG02    SPOKAN01   SPOKAN02   SPOKAN03 
 WHIT01     XO01
