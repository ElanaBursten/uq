

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 344

UULC 
Ticket No: 13056021               2 FULL BUSINESS 
Send To: QLNWA26    Seq No:    1  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:42 am    Op: ormary 
Original Call Date:  3/13/13   Time: 10:15 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SNOHOMISH               Place: EVERETT 
Address:             Street: OAKES AVE 
Nearest Intersecting Street: PACIFIC AVE 

Twp: 29N   Rng: 5E    Sect-Qtr: 20-SW,19-SE,30-NE,29-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.9771782 Lon:-122.2057674 SE Lat: 47.9759134 Lon:-122.2031848 

Type of Work: INSTALLATION OF TRAFFIC SIGNAL SYSTEM 
Location of Work: LOCATION OF WORK IS AT THE INTERSECTION OF OAKS AND PACIFIC
: AVE PLEASE LOCATE ENTIRE INTERSECTION WITHIN A 150 FOOT RADIUS INCLUDING ALL
: RIGHT OF WAYS AND EASEMENTS. 

Remarks:  
:  

Company     : SAIL ELECTRIC INC 
Contact Name: REID BARTON                      Phone: (360)383-0911 
Cont. Email : SHAWN@SAILELECTRICINC.COM 
Alt. Contact: DEAN MOORE                       Phone: (360)815-0069 
Contact Fax : (360)383-9911 
Work Being Done For: CITY OF EVERETT 
Additional Members:  
BLKRC01    CC7721     EVT01      GTE21      PUGG09     SNOPUD05
