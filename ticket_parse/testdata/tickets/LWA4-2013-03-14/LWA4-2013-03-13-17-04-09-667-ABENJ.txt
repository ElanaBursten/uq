

From: IRTHNet  At: 03/13/13 04:03 PM  Seq No: 316

UULC 
Ticket No: 13056405               2 FULL BUSINESS 
Send To: QLNWA30    Seq No:    6  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:28 pm    Op: webusr 
Original Call Date:  3/13/13   Time:  1:23 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: WHATCOM                 Place: BELLINGHAM 
Address: 1004        Street: KENOYER DR 
Nearest Intersecting Street: WILD ROSE CT 

Twp: 38N   Rng: 3E    Sect-Qtr: 33 
Twp: 38N   Rng: 3E    Sect-Qtr: 33-SW-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 48.7395539 Lon:-122.4372451 SE Lat: 48.7393159 Lon:-122.4363801 

Type of Work: INSTALL GAS SERVICE 
Location of Work: LOCATE ENTIRE LOT 8 AND ALL ROWS AND EASEMENTS TO LONG SIDE
: OF STREET, LOCATE BOTH SIDES OF STREET. CONTACT ROGER 509-728-3188 WITH
: QUESTIONS. 

Remarks:  
:  

Company     : SNELSON COMPANIES INC. 
Contact Name: BRENDA HENRY                     Phone: (360)856-6511  Ext.: 227 
Cont. Email : BHENRY@SNELSONCO.COM 
Alt. Contact: ROGER KINDLE                     Phone: (509)728-3188 
Contact Fax : (360)854-4268 
Work Being Done For: PAUL TAYLOR HOMES LLC 
Additional Members:  
BELLNH01   CC7770     CNG16      OLYPE01    PUGE11     TRNMTN01
