

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 353

IEUCC 
Ticket No: 13056126               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   57  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:53 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:49 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 1427        Street: S MADISON ST 
Nearest Intersecting Street: 15TH 

Twp: 25N   Rng: 43E   Sect-Qtr: 30-NW 
Twp: 25N   Rng: 43E   Sect-Qtr: 30-NW,19-SW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6433849 Lon:-117.4293449 SE Lat: 47.6410052 Lon:-117.4265342 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 94FT NORTH OF 15TH ON THE EAST SIDE OF MADISON.
: MARK 5FT RADIUS OF GAS METER LOCATED NORTH OUT.  METER IS FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #491323430 
Additional Members:  
AVISTA07   SPOKAN01   SPOKAN02   SPOKAN03   XO01
