

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 319

IEUCC 
Ticket No: 13056266               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   75  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:16 pm    Op: orruth 
Original Call Date:  3/13/13   Time: 12:08 pm    Op: orruth 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: LIBERTY LAKE 
Address: 3712        Street: N SPOKANE BRIDGE ROAD 
Nearest Intersecting Street: RIVERVIEW 

Twp: 25N   Rng: 46E   Sect-Qtr: 6 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6965022 Lon:-117.0436078 SE Lat: 47.6893530 Lon:-117.0417005 

Type of Work: INSTALL FENCE 
Location of Work: EXCAVATION SITE IS ON THE N SIDE OF THE ROAD. 
: ADD APX 1/8MI FROM ABV INTER.  THIS IS THE LAST HOUSE IN WASHINGTON.
: RIVERVIEW IS IN IDAHO.  MARK ALONG THE FRONT OF THE PROP ALONG N SPOKANE
: BRIDGE ROAD/RIVERVIEW AT ABV ADD.  CALLER STATES ALL WORK IS BEING DONE IN
: WASHINGTON. 

Remarks: BEST INFORMATION AVAILABLE 
: AREA WILL BE MARKED WITH REFLECTOR STAKES 

Company     : HOMEOWNER 
Contact Name: EDWARD LIMA                      Phone: (509)924-7049 
Cont. Email :  
Alt. Contact: EDWARD  - CELL                   Phone: (509)991-0145 
Contact Fax :  
Work Being Done For: HOMEOWNER 
Additional Members:  
AVISTA11   ELD01      FB02       KTEL01     SPENG01    WSDOT14
