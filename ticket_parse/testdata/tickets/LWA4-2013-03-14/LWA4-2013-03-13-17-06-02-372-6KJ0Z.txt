

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 347

IEUCC 
Ticket No: 13056092               2 FULL BUSINESS 
Send To: QLNWA34    Seq No:    9  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:37 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:34 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 1526        Street: E 19TH 
Nearest Intersecting Street: MADELIA 

Twp: 25N   Rng: 43E   Sect-Qtr: 28-NW 
Twp: 25N   Rng: 43E   Sect-Qtr: 29-NE,28-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6384981 Lon:-117.3898511 SE Lat: 47.6369283 Lon:-117.3850622 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 418FT WEST OF MADELIA ON THE SOUTH SIDE OF 19TH.
: MARK 5FT RADIUS OF GAS METER LOCATED SOUTH OUT.  METER IF FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #148324619 
Additional Members:  
AVISTA09   SPOKAN01   SPOKAN02   SPOKAN03
