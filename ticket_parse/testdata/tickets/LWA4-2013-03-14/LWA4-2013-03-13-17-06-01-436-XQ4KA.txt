

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 342

UULC 
Ticket No: 13056118               2 FULL BUSINESS 
Send To: QLNWA24    Seq No:   28  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:52 am    Op: orsusa 
Original Call Date:  3/13/13   Time: 10:49 am    Op: orsusa 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: PIERCE                  Place: EDGEWOOD 
Address:             Street: SR 161 
Nearest Intersecting Street: 22ND 

Twp: 20N   Rng: 4E    Sect-Qtr: 9-SE-NE,10-SW-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.2378260 Lon:-122.2945690 SE Lat: 47.2335922 Lon:-122.2931099 

Type of Work: REMOVE WATER MAIN 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: FROM ABV INTER MARK EAST SIDE OF SR 161 SOUTH APX 1000 FT TO APX 400 FT
: SOUTH OF 24TH, AREA MARKED IN WHITE. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : TRI-STATE CONSTRUCTION 
Contact Name: BRIAN LARSON                     Phone: (425)455-2570 
Cont. Email :  
Alt. Contact: DARREN BERNETHY - CA             Phone: (206)730-4979 
Contact Fax :  
Work Being Done For: D O T 
Additional Members:  
CC7711     EDGWD01    LEVL301    LKHAVN01   MILTON01   MTVEGW01   PUGE07 
 PUGG07     WSDOT10
