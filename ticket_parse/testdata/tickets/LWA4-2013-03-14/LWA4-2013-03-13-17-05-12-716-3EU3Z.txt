

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 328

IEUCC 
Ticket No: 13056349               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   90  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:04 pm    Op: webusr 
Original Call Date:  3/13/13   Time:  1:00 pm    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 1814        Street: W NORTHWEST BLVD 
Nearest Intersecting Street: YORK 

Twp: 25N   Rng: 42E   Sect-Qtr: 12-NE 
Twp: 25N   Rng: 42E   Sect-Qtr: 12-SE-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6817216 Lon:-117.4403453 SE Lat: 47.6792672 Lon:-117.4368293 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 150FT SOUTHEAST OF YORK ON THE NORTHEAST SIDE OF
: NORTHWEST BLVD.  MARK 5FT RADIUS OF GAS METER LOCATED NORTH OUT.  METER IS
: FLAGGED   CONTACT BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #736322882 
Additional Members:  
AVISTA10   CC7730     SPOKAN01   SPOKAN02   SPOKAN03   XO01
