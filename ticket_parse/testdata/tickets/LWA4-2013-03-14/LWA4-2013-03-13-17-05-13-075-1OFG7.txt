

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 330

IEUCC 
Ticket No: 13056338               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   92  Map Ref:  
Update Of: 13056238 
Transmit      Date:  3/13/13   Time:  1:09 pm    Op: ormary 
Original Call Date:  3/13/13   Time:  1:00 pm    Op: ormary 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 10014       Street: LINDEKE COURT 
Nearest Intersecting Street: WESTOVER ROAD 

Twp: 26N   Rng: 42E   Sect-Qtr: 14-SE,13-SW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7491932 Lon:-117.4527424 SE Lat: 47.7477430 Lon:-117.4493287 

Type of Work: SETTING A TEMPORARY POWER POLE 
Location of Work: LOCATE A 5FT RADIUS AROUND THE TRANSFORMER  TAKE EXIT 289
: OFF I-90  GO NORTH ON DIVISION  GO NORTH ON EGLIN  GO WEST ON FIVE MILE RD
: GO EAST WESTOVER -  GO NORTH ON LINDEKE
: ++UPDATED - TO CHANGE CROSS STREET+++ 

Remarks: LOT 6  BLOCK 2 
:  

Company     : PROVIDENT ELECTRIC 
Contact Name: JASON SCOTT                      Phone: (509)926-4779 
Cont. Email : SPOKANE@PROVIDENTELECTRIC.NET 
Alt. Contact:                                  Phone: (509)926-4779 
Contact Fax : (509)926-4863 
Work Being Done For: COPPER BASIN CONSTRUCTION 
Additional Members:  
AVISTA08   CC7730     MEADSD01   SPENG01    SPENG02    SPOKAN02   WHIT01 
 WLMSP06
