

From: IRTHNet  At: 03/13/13 04:03 PM  Seq No: 312

UULC 
Ticket No: 13056240               2 FULL BUSINESS 
Send To: QLNWA31    Seq No:    6  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:30 pm    Op: oradri 
Original Call Date:  3/13/13   Time: 11:51 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: THURSTON                Place: ROCHESTER 
Address: 19245       Street: ROSEMARY ST SW 
Nearest Intersecting Street: 193RD AVE SW 

Twp: 15N   Rng: 3W    Sect-Qtr: 3 
Twp: 15N   Rng: 3W    Sect-Qtr: 2-SW,3-SE,10-NE,11-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 46.8093392 Lon:-123.0365990 SE Lat: 46.8071904 Lon:-123.0338418 

Type of Work: INSTALLING CATV SERVICE 
Location of Work: ADDRESS AT NW CORNER OF INTERSECTION
: LOCATE FROM PWR METER GOING NE APPROX 80FT TO RIGHT OF WAY THEN GO NORTH
: APPROX 120FT TO CATV PED AREA IN WHITE 

Remarks:  
:  

Company     : RAINIER VALLEY CONSTRUCTION 
Contact Name: LISA WILLIAMS                    Phone: (360)446-2549 
Cont. Email : LISA@RAINIERVALLEYINC.COM 
Alt. Contact: JOHN CHOATE CELL - 1             Phone: (360)561-6849 
Contact Fax : (360)446-7641 
Work Being Done For: COMCAST 
Additional Members:  
CC7711     PUGE10     ROCWTR01   TCSSW01    THUDPW01
