
New Jersey One Call System        SEQUENCE NUMBER 0060    CDC = TK3 

Transmit:  Date: 06/30/11   At: 15:03 

*** R O U T I N E         *** Request No.: 111811576 

Operators Notified: 
BAN     = VERIZON                       GPF     = JERSEY CENTRAL POWER & LI      
MRT     = TOWNSHIP OF MORRIS 326        P28     = PUBLIC SERVICE ELECTRIC &      
SMC     = SOUTHEAST MORRIS COUNTY M     TK3     = CABLEVISION OF MORRIS, IN      

Start Date/Time:    07/07/11   At 08:00  Expiration Date: 09/02/11 

Location Information: 
County: MORRIS                 Municipality: MORRIS 
Subdivision/Community:  
Street:               10 ARMSTRONG ROAD 
Nearest Intersection: SPRINGBROOK ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL GAS SERVICE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  ERIC WEGER 

Working For: PUBLIC SERVICE ELECTRIC & GAS 
Address:     48 MIDDLE AVE 
City:        SUMMIT, NJ  07901 
Phone:       973-425-0294   Ext:  

Excavator Information: 
Caller:      ERIC WEGER 
Phone:       973-425-0294   Ext:  

Excavator:   PUBLIC SERVICE ELECTRIC & GAS 
Address:     48 MIDDLE AVE 
City:        SUMMIT, NJ  07901 
Phone:       973-425-0294   Ext:          Fax:  973-425-8982 
Cellular:    973-425-9428 
Email:       eric.weger@pseg.com 
End Request 
