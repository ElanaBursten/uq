From: IRTHNet  At: 10/22/12 04:13 PM  Seq No: 1472
Facility: Electric Primary

SCEKZ42 77 SC811 Voice 10/22/2012 04:11:00 PM 1210220030 Design 

Notice Number:    1210220030        Old Notice:                        
Sequence:         77                Created By:      rmd               

Created:          10/22/12 04:12 PM                                     
Work Date:        11/13/12 11:59 PM                                     
Update on:                          Good Through:                      

Caller Information:
SC 811
810 DUTCH SQUARE BLVD
COLUMBIA, SC 29210
Company Fax:                        Type:            Contractor        
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:3
Caller Email:     rdotman@sc1ups.org                                    

Site Contact Information:
RHONDA DOTMAN                              Phone:(803) 939-1117 Ext:3
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        810 DUTCH SQUARE BLVD                                 
Intersection:  N ARROWOOD DR                                         
Subdivision:                                                         

Lat/Long: 34.034156,-81.094714
Second:  34.034156,-81.094714

Explosives:  N Premark:  N Drilling/Boring:  N Near Railroad:  N

Work Type:     BRIDGE, CONSTRUCTION                                    
Work Done By:  SC 811               Duration:        1 hr              

Instructions:
TEST                                                                          

Directions:
TEST                                                                          

Remarks:
TEST                                                                          

Member Utilities Notified:
COC82 BSZB45 TWCZ40 SCEKZ42 SCGZ05 

Members are to respond within 15 full working days.