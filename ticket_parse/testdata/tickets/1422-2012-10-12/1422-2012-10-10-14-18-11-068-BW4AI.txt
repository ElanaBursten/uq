From: IRTHNet  At: 10/10/12 02:16 PM  Seq No: 788
Facility: Gas Distribution

SCGZ05 5 SC811 Remote 10/10/2012 02:15:00 PM 1210100301 Update 

Notice Number:    1210100301        Old Notice:      1210100298        
Sequence:         5                 Created By:      R-LMD             

Created:          10/10/12 02:15 PM                                     
Work Date:        10/15/12 11:59 PM                                     
Update on:                          Good Through:                      

Caller Information:
MID-CAROLINA ELECTRIC CO-OP
254 LONGS POND RD
LEXINGTON, SC 29072
Company Fax:                        Type:            Excavator         
Caller:           LEW DUBOSE          Phone:         (803) 749-6440 Ext:
Caller Email:     dispatchgroup@mcecoop.com                             

Site Contact Information:
LEW DUBOSE                              Phone:(803) 749-6440 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        810 DUTCH SQUARE BLVD                                 
Intersection:  UNKNOWN                                               
Subdivision:   MEADOWVIEW                                            

Lat/Long: 34.034156,-81.094714
Second:  34.034156,-81.094714

Explosives:  N Premark:  Y Drilling/Boring:  Y Near Railroad:  N

Work Type:     ELECTRIC, RELOCATE SECONDARY                                   
Work Done By:  MID-CAROLINA ELECTRIC CO-OPDuration:        APPROX 2 WEEKS    

Instructions:
MARK THE LEFT SIDE OF LUNA TRAIL AT LOT 26 AND MARK THROUGH TO LOT 30 // THIS 
IS FOR MCEC JOB TESTING & S.O. TESTING // THIS IS TO RELOCATE WIRE & SECONDARY
PEDESTALS DUE TO CHANGES IN LOT SIZES // POLE LOCATIONS ARE STAKED WITH PINK  
// POSSIBLE DRILLING AND BORING // TESTING TESTING TESTING                    

Directions:
ADDITIONAL ROAD: BALL PARK RD // LANDMARK: PLEASANT HILL MIDDLE SCHOOL //     
DIRS: FROM THE INTERSECTION OF RAWL RD AND BALL PARK RD , GO WEST ALONG RAWL  
RD APPROX 300FT AND THIS IS A NEW SUBD ON THE THE LEFT SIDE OF THE ROAD       

Remarks:

Member Utilities Notified:
COC82 BSZB45 TWCZ40 SCEKZ42 SCGZ05