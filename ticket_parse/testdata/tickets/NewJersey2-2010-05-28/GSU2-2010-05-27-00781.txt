
GSUPLS101472124-00	JC	2010/05/27 21:12:31	00742


New Jersey One Call System        SEQUENCE NUMBER 0054    CDC = GPP 

Transmit:  Date: 05/27/10   At: 21:12 

*** E M E R G E N C Y     *** Request No.: 101472124 

Operators Notified: 
BAN     = VERIZON                       BIT     = BRICK TOWNSHIP MUNICIPAL 
CC4     = COMCAST CABLEVISION OF NE     GPP     = JERSEY CENTRAL POWER & LI 
NJN     = NEW JERSEY NATURAL GAS CO     SCNJ4   = NJ AMERICAN WATER 

Start Date/Time:    05/27/10   At 21:15  Expiration Date: 

Location Information: 
County: OCEAN                  Municipality: MANTOLOKING 
Subdivision/Community: POINT PLEASANT BORO 
Street:               1057 OCEAN AVE 
Nearest Intersection: LYMAN AVE 
Other Intersection: 
Lat/Lon: 
Type of Work: EMERGENCY REPAIR - GAS LEAK 
Block:                Lot:                Depth: 6FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks: 

Working For: NEW JERSEY NATURAL GAS COMPANY 
Address:     1415 WYCKOFF ROAD 
City:        WALL, NJ  07719 
Phone:       732-938-1213   Ext: 

Excavator Information: 
Caller:      MARY BETH MCEVOY 
Phone:       732-938-1213   Ext: 

Excavator:   NEW JERSEY NATURAL GAS COMPANY 
Address:     1415 WYCKOFF ROAD 
City:        WALL, NJ  07719 
Phone:       732-938-1213   Ext:          Fax:  732-919-7955 
Cellular:    732-938-1213 
Email: 
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2010/05/27 09:12PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
101472124        0742  GSUPLS101472124-00   Emergency (Less  2010/05/27 09:15PM
