
GSUPLS101472115-00	JC	2010/05/27 20:45:39	00736

New Jersey One Call System        SEQUENCE NUMBER 0058    CDC = GPT 

Transmit:  Date: 05/27/10   At: 20:44 

*** R O U T I N E         *** Request No.: 101472115 

Operators Notified: 
BAN     = VERIZON                       DEN     = DENVILLE TOWNSHIP 
GPT     = JERSEY CENTRAL POWER & LI     NJN     = NEW JERSEY NATURAL GAS CO 
RKB     = BOROUGH OF ROCKAWAY           TK3     = CABLEVISION OF MORRIS, IN 

Start Date/Time:    06/03/10   At 08:00  Expiration Date: 08/02/10 

Location Information: 
County: MORRIS                 Municipality: DENVILLE 
Subdivision/Community: 
Street:               41 MAGNOLIA AVE 
Nearest Intersection: HEMLOCK DR 
Other Intersection: 
Lat/Lon: 
Type of Work: INSTALLING DRAINAGE 
Block:                Lot:                Depth: 6FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks: 

Working For: KATHY SCHOOLMAN 
Address:     41 MAGNOLIA 
City:        DENVILLE, NJ  07834 
Phone:       973-625-0777   Ext: 

Excavator Information: 
Caller:      JOSEPH VENITO 
Phone:       973-263-9717   Ext: 

Excavator:   ON LINE CONTRACTING 
Address:     112 MYRTLE AVE 
City:        BOONTON, NJ  07005 
Phone:       973-263-9717   Ext:          Fax:  973-263-9717 
Cellular:    973-332-7002 
Email:       ONLINECONTRACTING@OPTONLINE.NET 
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2010/05/27 08:45PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
101472115        0736  GSUPLS101472115-00   Regular Notice   2010/06/03 08:00AM
