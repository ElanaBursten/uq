
GSUPLS101472132-00	JC	2010/05/27 22:43:54	00743


New Jersey One Call System        SEQUENCE NUMBER 0136    CDC = GPC 

Transmit:  Date: 05/27/10   At: 22:43 

*** R O U T I N E         *** Request No.: 101472132 

Operators Notified: 
ADC     = COMCAST CABLEVISION OF TO     BAN     = VERIZON 
CAM     = CABLEVISION OF MONMOUTH       DVM     = TOMS RIVER MUNICIPAL UTIL 
GPC     = JERSEY CENTRAL POWER & LI     MEE     = MEENAN OIL COMPANY, L.P. 
NJN     = NEW JERSEY NATURAL GAS CO     UW7     = /UWTR-TOMS RIVER 

Start Date/Time:    06/03/10   At 09:00  Expiration Date: 08/02/10 

Location Information: 
County: OCEAN                  Municipality: TOMS RIVER 
Subdivision/Community: HOLIDAY CITY SILVERTON 
Street:               2349 MOUNT HOOD LANE 
Nearest Intersection: MOUNT ARARAT LANE 
Other Intersection: 
Lat/Lon: 
Type of Work: TERMITE TREATMENT 
Block:                Lot:                Depth: 4FT 
Extent of Work: 10FT PERIMETER OF THE HOUSE. 
Remarks: 

Working For: ELIZABETH BELDEN 
Address:     2349 MOUNT HOOD LANE 
City:        TOMS RIVER, NJ  08753 
Phone:       732-864-1914   Ext: 

Excavator Information: 
Caller:      DIANE GUAGLIARDO 
Phone:       732-270-4900   Ext: 

Excavator:   CERTIFIED EXTERMINATORS INC. 
Address:     97 YELLOW BANK ROAD 
City:        TOMS RIVER, NJ  08753 
Phone:       732-270-4900   Ext:          Fax:  732-270-1064 
Cellular:    732-330-7730 
Email: 
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2010/05/27 10:43PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
101472132        0743  GSUPLS101472132-00   Regular Notice   2010/06/03 09:00AM
