
GSUPLS101472116-00	JC	2010/05/27 21:09:38	00738


New Jersey One Call System        SEQUENCE NUMBER 0020    CDC = GPU 

Transmit:  Date: 05/27/10   At: 21:09 

*** R O U T I N E         *** Request No.: 101472116 

Operators Notified: 
BAN     = VERIZON                       CC6     = COMCAST CABLEVISION OF NO 
EG2     = ELIZABETHTOWN GAS COMPANY     GPU     = JERSEY CENTRAL POWER & LI 
UNI     = CENTURY LINK 

Start Date/Time:    06/03/10   At 00:15  Expiration Date: 08/02/10 

Location Information: 
County: WARREN                 Municipality: INDEPENDENCE 
Subdivision/Community: 
Street:               6 - 10 SHAKESPEARE ROAD 
Nearest Intersection: CHAUCER DR 
Other Intersection:   JOHNSON ROAD 
Lat/Lon: 
Type of Work: REPLACING 
Block:                Lot:                Depth: 2FT 
Extent of Work: CURB TO CURB. CURB TO 20FT BEHIND CURB. 
Remarks: 

Working For: COMCAST 
Address:     155 PORT MURRAY RD 
City:        PORT MURRAY, NJ  07865 
Phone:       908-689-0885   Ext: 

Excavator Information: 
Caller:      DIANE FRAZEE 
Phone:       908-362-5619   Ext: 

Excavator:   FRAZEE CABLE SERVICES INC. 
Address:     7 CHERRY 
City:        BLAIRSTOWN, NJ  07825 
Phone:       908-362-5619   Ext:          Fax:  908-362-1288 
Cellular:    973-417-0945 
Email:       sweetp918@yahoo.com 
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2010/05/27 09:09PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
101472116        0738  GSUPLS101472116-00   Regular Notice   2010/06/03 12:15AM
