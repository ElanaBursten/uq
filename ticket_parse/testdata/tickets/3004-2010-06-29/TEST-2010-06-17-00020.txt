
AGL103  01427 GAUPC 06/17/10 13:40:05 06170-220-031-000 NORMAL
Underground Notification             
Notice : 06170-220-031 Date: 06/17/10  Time: 13:36  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 355    To:        Name:    CAMERON                        ST   SE 
Near  : Name:    MEMORIAL                       DR  

Subdivision:                                         
Locate:  FRONT OF PROPERTY                                                     

Grids       : 3344B8421A 
Work type   : RPLC WTR SVC                                                        

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : UNKNOWN    Priority: 3
Done for  : KATHLEEN LEVY                           
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : THIS TICKET IS EXTENDING WORK FOR TICKET 06170-220-030              

Company : CODY'S PLUMBING                           Type: CONT                
Co addr : 2295 PINEWOOD DRIVE                      
City    : DECATUR                         State   : GA Zip: 30032              
Caller  : NORMAN CODY                     Phone   :  404-933-4030              
Fax     :                                 Alt. Ph.:  404-933-4030              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/17/10  Time: 13:36  Oper: 198
Mbrs : AGL103 ATL01 ATL02 BSCA GAUPC GP104 GP115T 
-------------------------------------------------------------------------------
