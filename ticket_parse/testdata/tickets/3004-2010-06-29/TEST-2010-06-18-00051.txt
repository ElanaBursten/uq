
AGL103  01127 GAUPC 06/18/10 12:48:10 06180-300-680-000 NORMAL
Underground Notification             
Notice : 06180-300-680 Date: 06/18/10  Time: 12:46  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 1067   To:        Name:    HIGH POINT                     DR   NE 
Near  : Name:    ZIMMER                         DR  

Subdivision:                                         
Locate: S. CLOUD 404-353-3127 PLEASE LOCATE ALL UTILITIES FRONT LEFT AND RIGHT 
      :  QUADRANTS TO METER AND TO INCLUDE THE RIGHT OF WAY.  PLEASE MARK ALL UT
      :  ILITIES AND NOTE ANY CONFLICTS.  FRONT AND BOTH SIDES OF PROPERTY      

Grids       : 3347B8421D 
Work type   : BURYING CATV SERVICE DROPS                                          

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : COMCAST                                 
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Driveway & Sidewalk           

Company : SPECTRUM TECHNOLOGY SOLUTIONS             Type: CONT                
Co addr : 20 GILL LN                               
City    : STOCKBRIDGE                     State   : GA Zip: 30281              
Caller  : ERIN KORESKI                    Phone   :  404-558-8704              
Fax     :  770-898-8901                   Alt. Ph.:                            
Email   : ERIN.SPECTRUM@MINDSPRING.COM                                        
Contact :                                                           

Submitted date: 06/18/10  Time: 12:46  Oper: 201
Mbrs : AGL103 AGLN01 ATL01 ATL02 BSCA COMCEN DCWS02 GAUPC GP104 
-------------------------------------------------------------------------------
