
AGL106  00603 GAUPC 06/18/10 10:16:56 06180-237-020-000 NORMAL
Underground and Overhead Notification
Notice : 06180-237-020 Date: 06/18/10  Time: 10:14  Revision: 000 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 209    To:        Name:    UNION                          RD      
Near  : Name:    AUSTIN                         CIR 

Subdivision:                                         
Locate:  REAR OF PROPERTY FOR APT D                                            

Grids       : 3334B8503A 3334C8503A 
Work type   : INSTL SATTELITE DISH POLE & CATV SVC                                

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 20 MINS    Priority: 3
Done for  : JACKIE NARENCO                          
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     
        : OVERHEAD WORK BEGIN DATE: 06/23/10  
        : OVERHEAD WORK COMPLETION DATE: 07/09/10  

Company : DISH NETWORK                              Type: CONT                
Co addr : 130 MALLORY LN                           
City    : TEMPLE                          State   : GA Zip: 30179              
Caller  : ANTONIO MOREIRA                 Phone   :  770-562-4268              
Fax     :                                 Alt. Ph.:  770-562-4268              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/18/10  Time: 10:14  Oper: 198
Mbrs : AGL106 BGAWR CAR50 CAR51 COMNOR CRL70 GAUPC GP620 OCRL70 OGP620 
     : SYNCH TMC02 
-------------------------------------------------------------------------------
