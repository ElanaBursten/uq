
AGL103  00527 GAUPC 06/18/10 10:00:49 06180-300-254-000 NORMAL RESTAK
Underground Notification             
Notice : 06180-300-254 Date: 06/18/10  Time: 09:59  Revision: 000 
Old Tkt: 06020-300-512 Date: 06/02/10  Time: 10:24     

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 1422   To:        Name:    PIEDMONT                       AVE  NE 
Near  : Name:    WESTMINSTER                    DR  

Subdivision:                                         
Locate: PLEASE LOCATE LEFT FRONT AND REAR QUADRANTS, 200' IN ALL DIRECTIONS OF 
      :  ALL INTERSECTIONS                                                      

Grids       : 3347B8422C 
Work type   : REPAIR GAS MAIN                                                     

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 3 WEEKS    Priority: 3
Done for  : ATLANTA GAS LIGHT                       
Crew on Site: Y White-lined: N Blasting: N  Boring: Y

Remarks : PREVIOUS TICKET NUMBER:05170-300-871  PREVIOUS TICKET NUMBER:06020-3
        : 00-512                                                              
        : *** WILL BORE Road, Driveway & Sidewalk     
        : *** DrivingInstructions : 836 A4                                                              

Company : SOUTHEAST CONNECTIONS INC                 Type: CONT                
Co addr : 2590 DOGWOOD DR                          
City    : CONYERS                         State   : GA Zip: 30013              
Caller  : TRACY TEEPLE                    Phone   :  404-659-1422              
Fax     :                                 Alt. Ph.:  404-659-1422              
Email   : TRACYT@SECONNECTIONS.COM                                            
Contact : DAVID MAULL                     404-427-6231              

Submitted date: 06/18/10  Time: 09:59  Oper: 224
Mbrs : AGL103 AGLN01 ATL01 ATL02 BSCA FBRLTE GAUPC GP104 LEV3 NU104 
     : QWEST8 XOC90 
-------------------------------------------------------------------------------
