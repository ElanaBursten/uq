
AGL106  00520 GAUPC 06/18/10 10:00:31 06070-244-048-001 NORMAL 2NDREQ
Underground Notification             
Notice : 06070-244-048 Date: 06/07/10  Time: 15:58  Revision: 001 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 1332   To: 1460   Name:    BANKHEAD                       HWY     
Near  : Name:    FRASHIER                       RD  

Subdivision:                                         
Locate: LOC FRONT AND B/S OF PROPERTY FROM THE RD TO THE BACK PROPERTY LINE  FO
      :  R EACH ADDRESS 1332 , 1372, 1396, 1418 AND 1460                        

Grids       : 3335A8501A 3335A8501B 3336C8501B 3336C8501C 3336D8501A 
Grids       : 3336D8501B 3336D8501C 
Work type   : INSTALL SWR MAIN                                                    

Start date: 06/10/10 Time: 07:00 Hrs notc : 000
Legal day : 06/10/10 Time: 07:00 Good thru: 06/28/10 Restake by: 06/23/10
RespondBy : 06/09/10 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : CITY OF  CARROLLTON                     
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks : 2ND REQUEST FOR ADDRESS 1460,TO AGL106 (3N), BGAWR (3N), CRL70 (1), 
        : COMNOR, CALLER DISPUTING PRIS, STATES CARROLL EMC MARKED BUT THE LIN
        : ES ARE INCOMPLETE (THERE IS A UTILITY BOX IN THE WORK SITE AREA THAT
        : WAS NOT MARKED)//PLEASE RESPOND ASAP, CREW IS ON SITE//PLEASE CALL R
        : YAN LAWLER @ 770-722-5661 AND HE WILL MEET WITH LOCATOR ON SITE.  **
        : * THIS IS AN ADDITIONAL REQUEST FOR AGL106:ATLANTA GAS LIGHT CARROLL
        : TON, BGAWR:ATT / D, COMNOR:COMCAST COMMUNICATIONS, CRL70:CARROLL EMC
        : . PLEASE LOCATE ASAP.                                               
        : *** WILL BORE Driveway                      
        : *** DrivingInstructions : HWY 61 AKA BANKHEAD HWY                                             

Company : LAWLER UTILITIES                          Type: CONT                
Co addr : 17302 B HIGHWAY 27                       
City    : ROOPVILLE                       State   : GA Zip: 30170              
Caller  : RYAN LAWLER                     Phone   :  770-854-5015              
Fax     :                                 Alt. Ph.:  770-722-5661              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/07/10  Time: 15:58  Oper: 198
Mbrs : AGL106 BGAWR CAR50 CAR51 CCW01 COMNOR CRL70 GAUPC PPL02 
-------------------------------------------------------------------------------
