
AGL106  01751 GAUPC 06/17/10 14:52:32 06170-301-282-000 NORMAL
Underground Notification             
Notice : 06170-301-282 Date: 06/17/10  Time: 14:49  Revision: 000 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 103    To:        Name:    BLACKBERRY CREEK               DR      
Near  : Name:    FOUR NOTCH                     RD  

Subdivision:                                         
Locate: LOCATE WHERE THE CABLE LAYS, INCLUDING THE ENTIRE RIGHT OF WAY ALL THE 
      :  WAY UP TO THE HOUSE WHETHER IN CONFLICT OR NOT                         

Grids       : 3337A8457A 3337A8458C 3337A8458D 3337B8457A 3337B8458C 
Grids       : 3337B8458D 
Work type   : BURYING CATV SERVICE DROPS                                          

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 1 HOUR     Priority: 3
Done for  : COMCAST                                 
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Driveway & Sidewalk           

Company : UNO CABLE                                 Type: CONT                
Co addr : 4280 LENORA CHURCH RD                    
City    : SNELLVILLE                      State   : GA Zip: 30039              
Caller  : TAMMY MCCALL                    Phone   :  770-972-9939              
Fax     :                                 Alt. Ph.:                            
Email   : TAMMY.MCCALL@UNOCABLE.COM                                           
Contact :                                                           

Submitted date: 06/17/10  Time: 14:49  Oper: 181
Mbrs : AGL106 BGAWR CCW01 COMNOR CRL70 GAUPC GRS70 
-------------------------------------------------------------------------------
