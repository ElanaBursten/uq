
AGL118  01038 GAUPC 06/18/10 12:08:14 06180-253-017-000 NORMAL
Underground Notification             
Notice : 06180-253-017 Date: 06/18/10  Time: 12:04  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 1620   To:        Name:    BASS                           RD      
Near  : Name:    NEW FORSYTH                    RD  

Subdivision:                                         
Locate: GO THE DUMPSTER BESIDE THE CAFETERIA AND SEE WHITE PAINT IN AREA. LOCAT
      :  E A 30FT RADIUS AROUND THE WHITE PAINT. THE WHITE PAINT IS IN BETWEEN T
      :  HE DUMPSTER AND THE CAFETERIA                                          

Grids       : 3255A8343A 3255A8343B 3255B8343A 
Work type   : INSTALLING A GREASE TRAP                                            

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 2 DAYS     Priority: 3
Done for  : GEORGIA FARM BUREAU                     
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : SCONYERS PLUMBING                         Type: CONT                
Co addr : P. O. BOX 5492                           
City    : MACON                           State   : GA Zip: 31208              
Caller  : WALT SCONYERS                   Phone   :  478-746-0858              
Fax     :                                 Alt. Ph.:  478-746-0858              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/18/10  Time: 12:04  Oper: 198
Mbrs : AGL118 BGAWM CCM01 CNT70 GAUPC GP500 JONG01 MWA01 MWA02 SNG85 
     : 
-------------------------------------------------------------------------------
