
AGL118  01235 GAUPC 06/17/10 12:44:47 06170-269-024-000 NORMAL
Underground Notification             
Notice : 06170-269-024 Date: 06/17/10  Time: 12:43  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 788    To:        Name:    IVY BROOK                      WAY     
Near  : Name:    TROON WEST                         

Subdivision:                                         
Locate: LOC THE REAR AND BOTH SIDES OF PROPERTY                                

Grids       : 3250A8342C 3251D8342C 
Work type   : INSTALL FENCE                                                       

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 2 DAYS     Priority: 3
Done for  :                                         
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company :                                           Type: HOME                
Co addr :                                          
City    :                                 State   : GA Zip:                    
Caller  : SHAWN CRAY                      Phone   :  614-216-5428              
Fax     :                                 Alt. Ph.:  614-476-5410              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/17/10  Time: 12:43  Oper: 215
Mbrs : AGL118 BGAWM CCM01 GAUPC GP501 MWA01 MWA02 
-------------------------------------------------------------------------------
