
AGL103  01175 GAUPC 06/18/10 13:08:36 06180-214-014-000 NORMAL
Underground Notification             
Notice : 06180-214-014 Date: 06/18/10  Time: 13:05  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 244    To:        Name:    13TH                           ST   NE 
Near  : Name:    JUNIPER                        ST  

Subdivision:                                         
Locate: LOC ENTIRE COURTYARD BETWEEN THE THREE BUILDINGS AT THIS ADDRESS       

Grids       : 3347D8422A 3347D8422B 
Work type   : REPAIR CATV MAIN                                                    

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1  DAY     Priority: 3
Done for  : COMCAST                                 
Crew on Site: Y White-lined: N Blasting: N  Boring: Y

Remarks : ACCESS TO THIS PROPERTY MUST BE MADE 24 HOUR IN ADVANCE PLS CALL PRO
        : PERTY MANAGER ~ MS MORGAN                                           
        : *** WILL BORE Driveway & Sidewalk           

Company : GA CABLE, INC                             Type: CONT                
Co addr : 4571 RIVER MANSION CT                    
City    : DULUTH                          State   : GA Zip: 30096              
Caller  : JOE CROGAN                      Phone   :  770-598-8660              
Fax     :                                 Alt. Ph.:  770-598-8660              
Email   :                                                                     
Contact : MS MORGAN                       678-595-7446              

Submitted date: 06/18/10  Time: 13:05  Oper: 198
Mbrs : AGL103 AGLN01 ATL01 ATL02 ATT02 BSCA COMCEN FBRLTE GAUPC GP104 
     : LEV3 MCI02 MFN02 NU104 XOC90 
-------------------------------------------------------------------------------
