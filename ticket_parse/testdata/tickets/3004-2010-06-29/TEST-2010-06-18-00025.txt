
AGL103  00371 GAUPC 06/18/10 09:16:31 06180-273-006-000 EMERG 
Underground Notification             
Notice : 06180-273-006 Date: 06/18/10  Time: 09:13  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 190    To:        Name:    14TH                           ST   NW 
Near  : Name:    ATLANTIC                       DR  

Subdivision:                                         
Locate: IF FACING FROM THE STREET...LOC LEFT AND RIGHT FRONT INCLUDING THE R/O/
      :  W ON B/S OF THE ROAD.                                                  

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3347D8423B 
Work type   : REPAIR SEWER SERVICE                                                

Start date: 06/18/10 Time: 00:00 Hrs notc : 000
Legal day : 06/18/10 Time: 09:10 Good thru: 06/23/10 Restake by: 06/18/10
RespondBy : 06/18/10 Time: 09:10 Duration : UNKNOWN    Priority: 1
Done for  : CITY OF ATLANTA                         
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks :  EMERGENCY: SVC IS OUT.                                             

Company : CITY OF ATLANTA SEWER                     Type: CONT                
Co addr : 360 ENGLEWOOD AVE                        
City    : ATLANTA                         State   : GA Zip: 30315              
Caller  : CORALENE DOWNING                Phone   :  404-954-6340              
Fax     :                                 Alt. Ph.:  404-954-6340              
Email   : 0000000000                                                          
Contact : GALVIN HENRY                    404-954-6340              

Submitted date: 06/18/10  Time: 09:13  Oper: 224
Mbrs : AGL103 AGLN01 ATL01 ATL02 BSCA COMCEN FBRLTE GAUPC GP103 MBL91 
     : MCI02 NU103 TWT90 XOC90 
-------------------------------------------------------------------------------
