
AGL118  01848 GAUPC 06/17/10 15:20:14 06170-263-042-000 DAMAGE
Underground Notification             
Notice : 06170-263-042 Date: 06/17/10  Time: 15:16  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 122    To:        Name:    ASHFORD                        PARK    
Near  : Name:    CLARENDON                      CT  

Subdivision:                                         
Locate: LOCATE 100 FEET RADIUS AROUND ATT SLICK SITE AT 122 ASHFORD PARK       

Grids       : 3252B8345C 3252B8345D 
Work type   : BURYING PHONE SERVICE LINE                                          

Start date: 06/17/10 Time: 00:00 Hrs notc : 000
Legal day : 06/17/10 Time: 00:00 Good thru: 06/17/10 Restake by: 06/17/10
RespondBy : 06/17/10 Time: 00:00 Duration : 5 DAYS     Priority: 6
Done for  : ATT                                     
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : DMG TO TKT 05250-301-114 // DMG TO COX CABLE LINE MAY BE DEAD UNSURE
        : //DMG FOUND ON THE R/O/W JUST PAST 120 ASHFORD PARK DR  // WIRE DMG 
        : BY A PLOW  / UNKNOWN IF SVC IS OUT / AREA WAS NOT MARKED  8RY62174N 
        : SERVICE/DROP LINE DAMAGE TO CCM01,COX CABLE MIDDLE GEORGIA **CITY ST
        : REET** **EXTENT: HOLES **                                           
        : DAMAGE TO SERVICE/DROP CCM01 EXTENT: HOLES

Company : DANELLA CONSTRUCTION                      Type: CONT                
Co addr : 100 WILLINGHAM DR                        
City    : BOLING BROKE                    State   : GA Zip: 31004              
Caller  : TERRY OWENS                     Phone   :  478-994-6712              
Fax     :                                 Alt. Ph.:  478-994-6712              
Email   :                                                                     
Contact : KENNY SCOTT                     478-994-6712              

Submitted date: 06/17/10  Time: 15:16  Oper: 198
Mbrs : AGL118 BGAWM CCM01 GAUPC GP501 JONG01 MWA01 MWA02 
-------------------------------------------------------------------------------
