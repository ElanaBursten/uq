
AGL103  01810 GAUPC 06/17/10 15:04:55 06170-301-324-000 NORMAL RESTAK
Underground Notification             
Notice : 06170-301-324 Date: 06/17/10  Time: 15:03  Revision: 000 
Old Tkt: 06020-218-018 Date: 06/02/10  Time: 09:22     

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 778    To:        Name:    ATLANTIC                       DR   NW 
Near  : Name:    4TH                            ST  

Subdivision:                                         
Locate: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W    PLEASE NOTE: THIS TICKET
      :  WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, PLEASE RESPOND TO POSITI
      :  VE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PL
      :  EASE CHECK THE STATUS OF YOUR TICKET VIA THE SAME METHODS. ***         

Grids       : 3346B8423A 3346C8423A 
Work type   : INSTL SANITARY SWR SVC LINES, STORM DRAINS, WTR SVC LINES , GRADING 

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 80 DAYS    Priority: 3
Done for  : TURNER CONSTRUCTION                     
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : WORKING THROUGHOUT THE PROPERTY RESTAKE OF IRTH TKT #12219-042-036  
        : -- IF ANY QUESTIONS, PLS CALL FIELD CONTACT FOR DETAILS & HE WILL ME
        : ET W/ YOU AND NARROW IT DOWN  PREVIOUS TICKET NUMBER:01060-244-022  
        : PREVIOUS TICKET NUMBER:01220-238-013  PREVIOUS TICKET NUMBER:02090-2
        : 58-019  PREVIOUS TICKET NUMBER:02250-268-020  PREVIOUS TICKET NUMBER
        : :03150-243-035  PREVIOUS TICKET NUMBER:03310-247-030  PREVIOUS TICKE
        : T NUMBER:04160-253-022  PREVIOUS TICKET NUMBER:04300-238-014  PREVIO
        : US TICKET NUMBER:05170-231-033  PREVIOUS TICKET NUMBER:06020-218-018

Company : RICHARD R HARP EXCAVATION                 Type: CONT                
Co addr : 240 INDUSTRIAL WAY                       
City    : FAYETTEVILLE                    State   : GA Zip: 30214              
Caller  : DIANNE PAGE                     Phone   :  770-460-7747              
Fax     :  770-460-9571                   Alt. Ph.:  404-427-7713              
Email   : DPAGE@HARPEX.COM                                                    
Contact : DAVE STUDSTILL                  678-618-0775              

Submitted date: 06/17/10  Time: 15:03  Oper: 226
Mbrs : AGL103 ATL01 ATL02 BSCA FBRLTE GAUPC GP103 IFN01 MBL91 MCI02 
     : MEA70 NU103 
-------------------------------------------------------------------------------
