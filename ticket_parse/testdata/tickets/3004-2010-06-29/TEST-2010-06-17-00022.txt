
AGL118  01469 GAUPC 06/17/10 13:45:06 06170-301-035-000 NORMAL
Underground Notification             
Notice : 06170-301-035 Date: 06/17/10  Time: 13:43  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 533    To:        Name:    GRENADA                        TER     
Near  : Name:    CRANFORD                       AVE 

Subdivision:                                         
Locate: LOCATE THE FRONT OF THE PROPERTY & THE ROW ON BOTH SIDES OF THE ROAD   

Grids       : 3247A8338A 3247A8338B 3248D8338A 3248D8338B 
Work type   : REPAIR WATER SERVICE                                                

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : MACON WATER AUTHORITY                   
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Road                          

Company : MACON WATER AUTHORITY                     Type: MEMB                
Co addr : 790 SECOND ST.                           
City    : MACON                           State   : GA Zip: 31202              
Caller  : FELECIA BUFFINGTON              Phone   :  478-464-5671              
Fax     :                                 Alt. Ph.:                            
Email   : FBUFFINGTON@MACONWATER.ORG                                          
Contact :                                                           

Submitted date: 06/17/10  Time: 13:43  Oper: 139
Mbrs : AGL118 BGAWM CCM01 GAUPC GP501 MAC01 MAC03 MWA01 MWA02 QWEST8 
     : 
-------------------------------------------------------------------------------
