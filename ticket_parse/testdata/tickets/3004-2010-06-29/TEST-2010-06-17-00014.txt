
AGL118  01132 GAUPC 06/17/10 12:08:07 06170-300-777-000 NORMAL
Underground Notification             
Notice : 06170-300-777 Date: 06/17/10  Time: 12:04  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 215    To:        Name:    ALEXANDRIA                     DR      
Near  : Name:    AUDUBON                        PL  

Subdivision:                                         
Locate: LOCATE THE FRONT OF THE PROPERTY & THE ROW ON BOTH SIDES OF THE ROAD   

Grids       : 3252B8342C 3252B8342D 
Work type   : REPAIR SEWER MAIN                                                   

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : MACON WATER AUTHORITY                   
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Road                          

Company : MACON WATER AUTHORITY                     Type: MEMB                
Co addr : 790 SECOND ST.                           
City    : MACON                           State   : GA Zip: 31202              
Caller  : FELECIA BUFFINGTON              Phone   :  478-464-5671              
Fax     :                                 Alt. Ph.:                            
Email   : FBUFFINGTON@MACONWATER.ORG                                          
Contact :                                                           

Submitted date: 06/17/10  Time: 12:04  Oper: 139
Mbrs : AGL118 BGAWM CCM01 GAUPC GP500 GP501 MAC01 MCI02 MWA01 MWA02 
     : QWEST8 
-------------------------------------------------------------------------------
