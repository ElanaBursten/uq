
AGL106  01058 GAUPC 06/18/10 12:16:16 06180-300-636-000 NORMAL
Underground Notification             
Notice : 06180-300-636 Date: 06/18/10  Time: 12:14  Revision: 000 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 3611   To:        Name: W  HIGHWAY 166                            
Near  : Name:    BURWELL                        RD  

Subdivision:                                         
Locate: LOCATE ATT EASEMENT @ 3611 WEST HWY 166.                               

Grids       : 3333B8508A 3333B8509D 
Work type   : INSTALLING PHONE MAIN                                               

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 3 WEEKS    Priority: 3
Done for  : ATT                                     
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : JOB # 0RY24572N SUPERVISOR OVER JOB BILLY RUDESEAL @7702311857      

Company : ANSCO AND ASSOCIATES                      Type: CONT                
Co addr : 29 SHORTER INDUSTRIAL BLVD NW            
City    : ROME                            State   : GA Zip: 30165              
Caller  : JENNIE MEADOR                   Phone   :  706-232-5665              
Fax     :                                 Alt. Ph.:                            
Email   : JMEADOR@ANSCOINC.COM                                                
Contact : BILLY RUDESEAL                  770-231-1857              

Submitted date: 06/18/10  Time: 12:14  Oper: 166
Mbrs : AGL106 BGAWR CAR50 CAR51 CCW01 COMNOR CRL70 GAUPC 
-------------------------------------------------------------------------------
