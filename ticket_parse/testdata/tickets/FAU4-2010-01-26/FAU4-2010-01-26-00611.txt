
BGAWA  00617 GAUPC 01/26/10 16:00:06 01260-301-245-000 NORMAL
Underground Notification             
Notice : 01260-301-245 Date: 01/26/10  Time: 15:56  Revision: 000 

State : GA County: DOUGHERTY     Place: ALBANY                                  
Addr  : From: 3800   To:        Name:    OLD DAWSON                     RD      
Near  : Name: N  DOUBLEGATE                     DR  

Subdivision: DOUBLEGATE COUNTRY CLUB                 
Locate: AREAS ARE FLAGGED AND PAINTED...LOCATE FROM THE EAST OF THE BOILER ROOM
      :  RETAINING WALL, TO OLD DAWSON RD AND ISLAND AT MAIN ENTRANCE, ACROSS OL
      :  D DAWSON RD. TO THE MARKED UTILITY POLE.                               

Grids       : 3136A8415B 3136A8415C 
Work type   : INSTALLING UNDERGROUND FIBEROPTICS CONDUIT                          

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : UNKNOWN    Priority: 3
Done for  : TELECOMMUNICATIONS DEPARTMENT           
Crew on Site: Y White-lined: Y Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Road, Driveway & Sidewalk     

Company : ALBANY WATER, GAS, LIGHT                  Type: MEMB                
Co addr : 207 PINE AVENUE                          
City    : ALBANY                          State   : GA Zip: 31701              
Caller  : JAY IVEY                        Phone   :  229-883-8330              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact : MAC DAVIS                       229-809-0560              

Submitted date: 01/26/10  Time: 15:56  Oper: 140
Mbrs : ALB90 ALB93 ALB94 ALB95 BGAWA GAUPC GP410 MIT71 SGG83 TCI06 
     : 
-------------------------------------------------------------------------------


