
BGAWR  00619 GAUPC 01/26/10 16:00:13 01260-232-029-000 NORMAL
Underground Notification             
Notice : 01260-232-029 Date: 01/26/10  Time: 15:56  Revision: 000 

State : GA County: GORDON        Place: CALHOUN                                 
Addr  : From:        To:        Name:    BLUE HOLLY                     DR   NE 
Near  : Name:    NEWTOWN CHURCH                 RD  

Subdivision:                                         
Locate: LOCATE AREA WILL BE MARKED WITH STAKES, LOCATE A 15 FT RADIUS AROUND TH
      :  E MARKED AREA, IT WILL BE ON THE R/O/W BOTH SIDE OF THE  ROAD. THE LOCA
      :  TE  STARTING  FROM THE INTERSECTION  WITH NEWTOWN CHURCH RD NE, AND LOC
      :  ATE GOING WEST FOR A DISTANCE OF 600 FT ON BLUE HOLLY DR. NE . *** PLEA
      :  SE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, PLE
      :  ASE RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-461
      :  -7271. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA THE SAME 
      :  METHODS. ***                                                           

Grids       : 3432C8454A 3432C8454B 3432D8454A 3432D8455D 
Work type   : INSTALL SIGN                                                        

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 1 MONTH    Priority: 3
Done for  : GEORGIA DOT                             
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :  COPY FROM TICKET#: 01260-232-022                                   

Company : NORTHWEST GEORGIA PAVING                  Type: CONT                
Co addr : 501 WEST MAY STREET                      
City    : CALHOUN                         State   : GA Zip: 30703              
Caller  : BRIAN PAYTON                    Phone   :  706-629-8255              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:56  Oper: 198
Mbrs : AGL122 BGAWR CAL51 CAL52 CCAST1 GAUPC NOG70 
-------------------------------------------------------------------------------


