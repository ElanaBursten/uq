
COMDSP 00012 USAN 08/22/13 14:38:04 0327669 NORMAL NOTICE 

Message Number: 0327669 Received by USAN at 14:35 on 08/22/13 by JWH

Work Begins:    08/26/13 at 14:45   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 09/19/13 at 23:59   Update By: 09/17/13 at 16:59

Caller:         COREY BURNETT            
Company:        EL DORADO IRRIGATION DISTRICT      
Address:        2890 MOSQUITO RD                        
City:           PLACERVILLE                   State: CA Zip: 95677
Business Tel:   530-919-5737                  Fax: 530-622-2528                
Email Address:  CBURNETT@EID.ORG                                            

Nature of Work: BK/HO & HAND DIG TO REP WTR LEAK        
Done for:       SAME                          Explosives: N
Foreman:        MATT HEAPE               
Field Tel:                                    Cell Tel: 530-642-4170           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Street Address:         3389 PARDI WAY
  Cross Street:         BIG CUT RD

    WRK ON ST IN FRT/O ADDR EXT 25' INTO PROP 

Place: PLACERVILLE                  County: EL DORADO            State: CA

Long/Lat Long: -120.799834 Lat:  38.719933 Long: -120.798587 Lat:  38.720862 


Sent to:
AGASPL = AMERIGAS PROPANE             CTYPLA = CITY PLACERVILLE             
COMDSP = COMCAST-DIAMOND SPRINGS      ELDIRR = EL DORADO IRRIG              
JSWELD = J. S. WEST PROPANE 2         PACBEL = PACIFIC BELL                 
PGEPLA = PGE DISTR PLACERVILLE        



