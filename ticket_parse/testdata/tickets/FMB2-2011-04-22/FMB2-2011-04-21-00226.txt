
KORTERRA JOB UTILIQUEST_MD-SMTP-1 MD11200372 Seq: 225 04/21/2011 16:48:54
NOTICE OF INTENT TO EXCAVATE
Ticket No: 11200372
Transmit      Date: 04/21/11      Time: 04:47 PM           Op: wimyra
Original Call Date: 04/21/11      Time: 04:36 PM           Op: wimyra
Work to Begin Date: 04/27/11      Time: 12:01 AM
Expiration    Date: 05/10/11      Time: 11:59 PM

Place: DAMASCUS
Address: 22335       Street: CANTERFIELD WAY
Nearest Intersecting Street: CANTERFIELD TER

Type of Work: REPLACING UNDERGROUND FEEDER CABLE N/E
Extent of Work: MARK THE ENTIRE AREA FROM 22336 TO 22337 CANTERFIELD WAY
Remarks: BEST INFORMATION CALLER COULD PROVIDE
: TICKET TAKEN PER CALLER

Company      : COMCAST
Contact Name : MICHAEL MCLAUGHLIN             Fax          :
Contact Phone: (301)531-6123                  Ext          :
Caller Address: 20 GUDE DR
                ROCKVILLE, MD 20850
Email Address:  nestor_menjivarcano@cable.comcast.com
Alt. Contact : NESTOR                         Alt. Phone   : (240)372-0899
Work Being Done For: KIRCHMAN
State: MD              County: MONTGOMERY
MPG:  Y
Caller    Provided:  Map Name: MONT  Map#: 4928 Grid Cells: J5
Computer Generated:  Map Name: MONT  Map#: 4928 Grid Cells: J5

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 39.2250067Lon: -77.2625185 SE Lat: 39.2187792Lon: -77.2562314
Explosives: N
ATM01      TRU01      VMG        WGL06      WSS01
Send To: PTE02     Seq No: 0157   Map Ref:



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.