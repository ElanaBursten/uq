
KORTERRA JOB UTILIQUEST_MD-SMTP-1 MD11200349 Seq: 221 04/21/2011 16:33:42
NOTICE OF INTENT TO EXCAVATE
Ticket No: 11200349
Transmit      Date: 04/21/11      Time: 04:33 PM           Op: wiches
Original Call Date: 04/21/11      Time: 04:25 PM           Op: wiches
Work to Begin Date: 04/27/11      Time: 12:01 AM
Expiration    Date: 05/10/11      Time: 11:59 PM

Place: HANCOCK
Address: 11          Street: GROVE CIR
Nearest Intersecting Street: N PENNSYLVANIA AVE

Type of Work: CABLE DROP INSTALLATION
Extent of Work: MARK THE BACK OF THE LOT.
Remarks: CALLER PROVIDED LAT/LONG AS 39.70891, -78.17808

Company      : ALL LINK COMMUNICATIONS
Contact Name : SANDRA KINDER                  Fax          :
Contact Phone: (434)985-4933                  Ext          :
Caller Address: PO BOX 209
                RUCKERSVILLE, VA 22968
Email Address:  zone2locates@alllinkcomm.com
Alt. Contact :                                Alt. Phone   :
Work Being Done For: COMCAST
State: MD              County: WASHINGTON
MPG:  N
Caller    Provided:  Map Name: WASH  Map#: 3    Grid Cells: G6
Computer Generated:  Map Name: WASH  Map#: 3    Grid Cells: G6

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 39.7103147Lon: -78.1783954 SE Lat: 39.7047120Lon: -78.1715800
Explosives: N
CGM01      CHAN01     LTC01      VWA
Send To: PTE01     Seq No: 0019   Map Ref:



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.