
KORTERRA JOB UTILIQUEST_MD-SMTP-1 MD11200400 Seq: 227 04/21/2011 17:01:39
NOTICE OF INTENT TO EXCAVATE
Ticket No: 11200400
Transmit      Date: 04/21/11      Time: 04:59 PM           Op: webusr
Original Call Date: 04/21/11      Time: 04:16 PM           Op: webusr
Work to Begin Date: 04/27/11      Time: 12:01 AM
Expiration    Date: 05/10/11      Time: 11:59 PM

Place: MOUNT AIRY
Address: 4010        Street: WINDERMERE WAY
Nearest Intersecting Street: TWIN POND

Type of Work: SETTING POLES & ANCHORS
Extent of Work: INSTALLING BASKETBALL GOAL ON SIDE OF DRIVEWAY ACROSS FROM
: GARAGE. PLEASE MARK 6 FEET BEYOND PERIMETER OF SIDE OF THE DRIVEWAY
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT

Company      : PLAY N LEARN INC
Contact Name : RANDI GOLDSTEIN                Fax          : (410)715-9287
Contact Phone: (410)992-0992                  Ext          :
Caller Address: 9133 RED BRANCH RD
                COLUMBIA, MD 21045
Email Address:  randi@playnlearn.com
Alt. Contact : ROB                            Alt. Phone   : (410)992-0992
Work Being Done For: MARK INGLE
State: MD              County: CARROLL
MPG:  Y
Caller    Provided:  Map Name: CARR  Map#: 27   Grid Cells: F10
Computer Generated:  Map Name: CARR  Map#: 4570 Grid Cells: J6,H6,J7,H7

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 39.4038010Lon: -77.1394841 SE Lat: 39.3977120Lon: -77.1319791
Explosives: N
BGECA      BGECAG     PRM01      VCR
Send To: PTE02     Seq No: 0158   Map Ref:



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.