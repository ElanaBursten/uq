
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2012-01-12T14:06:28</transmitted>
    <seq_num>388</seq_num>
    <ticket>B201200909</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B201200909</ticket>
    <revision>00B</revision>
    <started>2012-01-12T14:04:26</started>
    <account>WHGRAY</account>
    <original_ticket>B201200909</original_ticket>
    <original_date>2012-01-12T14:04:26</original_date>
    <original_account>WHGRAY</original_account>
    <replace_by_date>2012-02-02T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2012-01-19T07:00:00</response_due>
    <expires>2012-02-07T07:00:00</expires>
    <state>VA</state>
    <county>PRINCE WILLIAM</county>
    <st_from_address>4909</st_from_address>
    <st_to_address>4909</st_to_address>
    <street>WOLF RUN SHOALS RD</street>
    <cross1>RAMROD RD</cross1>
    <work_type>FIOS</work_type>
    <done_for>VERIZON FIOS/SN</done_for>
    <reference>E8105360\8753\5845C</reference>
    <white_paint>Y</white_paint>
    <blasting>N</blasting>
    <boring>Y</boring>
    <name>S&amp;N COMMUNICATIONS</name>
    <address1>3723 THREE NOTCH RD.</address1>
    <city>LOUSIA</city>
    <cstate>NC</cstate>
    <zip>23093</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>4345911080</phone>
    <caller>HEATHER GRAY</caller>
    <caller_phone>5407174413</caller_phone>
    <email>hgray@sncommfo.com</email>
    <contact>CHRIS ZERKEL</contact>
    <contact_phone>5407173369</contact_phone>
    <map_url>http://newtinb.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&amp;TRG=B20120090900B&amp;OPR=kgdYRNIzsXOCmVIwY</map_url>
    <map_reference>5875C6</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.712829</latitude>
        <longitude>-77.355942</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.713039</latitude>
        <longitude>-77.356602</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.713471</latitude>
        <longitude>-77.356204</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712620</latitude>
        <longitude>-77.355281</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712188</latitude>
        <longitude>-77.355679</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >FROM ABOVE ADDRESS FOLLOW THE WHITE LINES, MARKING ON EITHER SIDE 15 FT TO
FEEDING HH LOCATED AT FRONT OF 4913.

Y</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.712567</latitude>
        <longitude>-77.355409</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712725</latitude>
        <longitude>-77.355394</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.713338</latitude>
        <longitude>-77.356060</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.713317</latitude>
        <longitude>-77.356346</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712669</latitude>
        <longitude>-77.355791</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712398</latitude>
        <longitude>-77.355907</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712296</latitude>
        <longitude>-77.355579</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.712567</latitude>
        <longitude>-77.355410</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3842A7721C-32</grid>
      <grid>3842A7721C-42</grid>
      <grid>3842A7721C-43</grid>
      <grid>3842B7721C-02</grid>
      <grid>3842B7721C-03</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CMC703</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>PWS901</member>
        <group_code>PWS</group_code>
        <description>PRINCE WILLIAM COUNTY SERVICE </description>
      </memberitem>
      <memberitem>
        <member>VZN213</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
