

COX01  00085A USAS 12/15/11 14:17:00 A13490777-00A NORM NEW GRID

Ticket : A13490777  Date: 12/15/11 Time: 14:13 Oper: TEL Chan: 100
Old Tkt: A13490777  Date: 12/15/11 Time: 14:16 Oper: TEL Revision: 00A

Company: A M ORTEGA CONSTRUCTION        Caller: TOM GRAHAM
Co Addr: 10125 CHANNEL ROAD
City&St: LAKESIDE, CA                   Zip: 92040      Fax: 619-390-1941
Phone: 619-390-1988 Ext:      Call back: ANYTIME
Formn: TOM                  Phone: 619-520-8673
Email: TOM@AMORTEGA.COM

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:DIVISION ST
X/ST 1 : 61ST ST
X/ST 2 : PLAZA BLVD
MPM 1:             MPM 2:
Locat: 6120, 6130, 6212, 6222, 6223, 6232, 6233, 6242, 6243, 6252, 6253, 6263,
     : 6272, 6273, 6283 & 6292 DIVISION ST, MARK 7FT RADIUS AROUND GAS METER
     : ONLY   BTWN X/STS 61ST ST & PLAZA BLVD

Excav Enters Into St/Sidewalk: N

Grids: 1290C054     1290C062     1290D053     1290D061
Lat/Long  : 32.694467/-117.067445 32.695969/-117.066919
          : 32.692816/-117.062723 32.694318/-117.062198
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE GAS RISERS
Wkend: N  Night: N
Work date: 12/19/11 Time: 14:13 Hrs notc: 096 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : SDG&E

Tkt Exp: 01/12/12

Mbrs : ATTD28SD      COX01  NAT01  NEXTG  SDG01  SND01  SWT49
