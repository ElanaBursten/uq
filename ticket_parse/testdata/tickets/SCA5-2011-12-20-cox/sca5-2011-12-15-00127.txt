

COX01  00084A USAS 12/15/11 14:12:58 A13490766-00A NORM NEW GRID

Ticket : A13490766  Date: 12/15/11 Time: 14:09 Oper: TEL Chan: 100
Old Tkt: A13490766  Date: 12/15/11 Time: 14:12 Oper: TEL Revision: 00A

Company: A M ORTEGA CONSTRUCTION        Caller: TOM GRAHAM
Co Addr: 10125 CHANNEL ROAD
City&St: LAKESIDE, CA                   Zip: 92040      Fax: 619-390-1941
Phone: 619-390-1988 Ext:      Call back: ANYTIME
Formn: TOM                  Phone: 619-520-8673
Email: TOM@AMORTEGA.COM

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:AVA ST
X/ST 1 : AVA PL
MPM 1:             MPM 2:
Locat: 1465, 1474, 1475, 1485, 1494, 1495, 1525, 1555 & 1595 AVA ST, MARK 7FT
     : RADIUS AROUND GAS METER ONLY    X/ST AVA PL

Excav Enters Into St/Sidewalk: N

Grids: 1290C0624
Lat/Long  : 32.692907/-117.069626 32.692928/-117.068529
          : 32.690360/-117.069579 32.690381/-117.068482
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE GAS RISERS
Wkend: N  Night: N
Work date: 12/19/11 Time: 14:10 Hrs notc: 096 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : SDG&E

Tkt Exp: 01/12/12

Mbrs : ATTD28SD      COX01  NAT01  NEXTG  SDG01  SND01  SWT49
