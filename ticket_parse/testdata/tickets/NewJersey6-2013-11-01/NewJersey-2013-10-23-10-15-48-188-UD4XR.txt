
UtiliQ-NJ  NJ132350113  10/23/2013 10:15:57  00001

Assigned to ORUUNJ for 08/23/2013


New Jersey One Call System        SEQUENCE NUMBER 0003    CDC = REC 


Transmit:  Date: 08/23/13   At: 07:41 


*** E M E R G E N C Y     *** Request No.: 132350113 


Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
PSOK    = PUBLIC SERVICE ELECTRIC &     RAM     = RAMSEY WATER & SEWER, BOR      
REC     = ROCKLAND ELECTRIC COMPANY     TCI     = CABLEVISION OF OAKLAND, L      


Start Date/Time:    08/23/13   At 07:45  Expiration Date:  


Location Information: 
County: BERGEN                 Municipality: RAMSEY 
Subdivision/Community:  
Street:               160 CHURCH ST 
Nearest Intersection: GERTZEN PLAZA 
Other Intersection:   CHESTNUT ST 
Lat/Lon: 
Type of Work: EMERGENCY - REPAIR/REPLACE WATER FACILITY 
Block:                Lot:                Depth: 6FT 
Extent of Work: CURB TO CURB.  CURB TO 10FT BEHIND CURB. 
Remarks:  
  Working For Contact:  MARK MADSEN 


Working For: BOROUGH OF RAMSEY 
Address:     33 N CENTRAL AVE 
City:        RAMSEY, NJ  07446 
Phone:       201-825-3400   Ext: 272 


Excavator Information: 
Caller:      JOHN GARCIA 
Phone:       973-773-4544   Ext:  


Excavator:   JOHN GARCIA CONSTRUCTION COMPA 
Address:     183 FRIAR LANE 
City:        CLIFTON, NJ  07013 
Phone:       973-773-4544   Ext:          Fax:  973-773-2977 
Cellular:    973-390-3728 
Email:        
End Request
