
UQSTSO 00035B USAS 10/07/13 05:06:04 B32800020-00B NORM NEW GRID

Ticket : B32800020  Date: 10/07/13 Time: 04:49 Oper: SME Chan: WEB
Old Tkt: B32800020  Date: 10/05/13 Time: 16:19 Oper: DIGEXPRESS Revision: 00B

Company: MARC POTTER                    Caller: MARC POTTER
Co Addr: 3567 BELVEDERE CIRCLE
City&St: CORONA, CA                     Zip: 92882
Phone: 714-588-9308 Ext:      Call back: ANY
Formn: SELF
Email: MPOTTER1971@YAHOO.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: N
Address: 3567        Street:BELVEDERE CIR
X/ST 1 : HALF MOON WAY
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 0773C042
Lat/Long  : 33.836132/-117.574299 33.835742/-117.573934
          : 33.835664/-117.574799 33.835274/-117.574434
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALLING FENCE
Wkend: N  Night: N
Work date: 10/17/13 Time: 07:00 Hrs notc: 242 Work hrs: 168 Priority: 2
Instruct : "MARK BY"                      Permit: NOT REQUIRED
Done for : SELF

Tkt Exp: 11/04/13

Mbrs : ATTDSOUTH    UCOR19  SCG1CO USCE34 USCE83RIV     UTWCWRIV      UVZMENIF
UVZPERS
