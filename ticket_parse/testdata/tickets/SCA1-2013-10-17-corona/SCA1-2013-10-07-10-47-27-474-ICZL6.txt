
UQSTSO 00123A USAS 10/07/13 07:46:09 A32800108-00A NORM NEW GRID

Ticket : A32800108  Date: 10/07/13 Time: 07:42 Oper: TEL Chan: 100
Old Tkt: A32800108  Date: 10/07/13 Time: 07:46 Oper: TEL Revision: 00A

Company: VALVERDE CONSTRUCTION          Caller: BUEN
Co Addr: 10936 SHOEMAKER AVE
City&St: SANTA FE SPRINGS, CA           Zip: 90670      Fax: 562-906-1918
Phone: 562-906-1826 Ext:      Call back: 7AM-5PM
Formn: DANNY ORTIZ          Phone: 310-629-1105
Email: BUENHATA@YAHOO.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:E ONTARIO AVE
X/ST 1 : COMPTON AVE
MPM 1:             MPM 2:
Locat: INTER/OF COMPTON AVE & E ONTARIO AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0773G024     0773H023
Lat/Long  : 33.845552/-117.536558 33.845092/-117.535769
          : 33.844872/-117.536955 33.844412/-117.536166
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR WTR LINE
Wkend: N  Night: N
Work date: 10/09/13 Time: 07:43 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF CORONA

Tkt Exp: 11/04/13

Mbrs : ATTDSOUTH     UCOR19  EVW37  LVL3CM SAW01  SCG1CO SUNESYSLLC    TWTLH
USCE34 USCE83RIV     UTWCWRIV      UVZMENIF      UVZPERS
