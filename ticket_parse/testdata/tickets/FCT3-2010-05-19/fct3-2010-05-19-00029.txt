
BEC03  00027 NCOCa 05/19/10 09:01:11 A101390427-00A NORM NEW GRID MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390427 Date: 05/19/10 Time: 09:00 Oper: SSB Chan:777
Old Tkt: A101390427 Date: 05/19/10 Time: 09:01 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SR1316   Intersection: N
Cross 1 : SR1317
Cross 2 : RIVER HILL RD
Location: ST: MARION ADAMS RD
XST1:  WEST PINE RIDGE

LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET TO CROSS STREET
:
Grids   : 3507A8416A    3507A8416D    3507A8417A    3507A8417B    3507A8417C
Grids   : 3507A8417D    3507A8418D    3508D8416A    3508D8416B    3508D8416C
Grids   : 3508D8416D    3508D8417A    3508D8417B    3508D8417C    3508D8417D
Grids   : 3508D8418D

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNK  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 09:01
Members: BEC03  SCB01*

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10139042700A&OPR=vspkeRI2raPBmT6p
