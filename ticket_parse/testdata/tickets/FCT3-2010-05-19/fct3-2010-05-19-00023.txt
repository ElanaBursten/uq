
BEC03  00021 NCOCa 05/19/10 08:58:19 A101390407-00A NORM NEW GRID MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390407 Date: 05/19/10 Time: 08:58 Oper: SSB Chan:777
Old Tkt: A101390407 Date: 05/19/10 Time: 08:58 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SR1157   Intersection: N
Cross 1 : HWY294
Cross 2 : SR1150
Location: ST: GUY ELLER RD

XST2: CANDY MOUNTAIN

LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET TO CROSS STREET
:
Grids   : 3506A8415A    3506A8416C    3506A8416D    3506B8416B    3506B8416C
Grids   : 3507C8415B    3507C8415C    3507D8415A    3507D8415B    3507D8415C
Grids   : 3507D8416D

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNK  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 08:58
Members: BEC03  SCB01*

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10139040700A&OPR=xurmgTK5ufSGtYGz
