
BEC03  00024 NCOCa 05/19/10 08:59:22 A101390415-00A NORM NEW GRID MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390415 Date: 05/19/10 Time: 08:59 Oper: SSB Chan:777
Old Tkt: A101390415 Date: 05/19/10 Time: 08:59 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SUIT RD   Intersection: N
Cross 1 : SR1145
Location: ST: SUIT RD


LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET END OF RD
:
Grids   : 3504B8414A    3504C8414A    3504C8414B    3504D8414B    3504B8415D
Grids   : 3504C8415D

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNK  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 08:59
Members: BEC03  SCB01*

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10139041500A&OPR=ypshZKD2mRM8lQAl
