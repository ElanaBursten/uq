

WGL904 00451 VUPSb 05/11/10 21:49:43 B013101841-00B          NORMAL

Ticket No:  B013101841-00B                        NEW  GRID NORM LREQ
Transmit        Date: 05/11/10   Time: 09:49 PM   Op: WRPAZ2
Call            Date: 05/11/10   Time: 09:45 PM
Due By          Date: 05/14/10   Time: 07:00 AM
Update By       Date: 05/28/10   Time: 11:59 PM
Expires         Date: 06/03/10   Time: 07:00 AM
Old Tkt No: B013101841
Original Call   Date: 05/11/10   Time: 09:45 PM   Op: WRPAZ2

City/Co:  PRINCE WILLIAM      Place:                                State:  VA
Address:     12967-12977         Street: QUEEN CHAPEL RD
Cross 1:     QUANCE LN                                         

Type of Work:   FTTP
Work Done For:  VERIZON

Excavation area:PLEASE HEAVY PAINT & FLAG FRONT OF THE ABOVE PROPERTIES.

Instructions:   CALLER MAP REF: NONE
                PFA0CW/8759/2423.40/500'

Whitelined: Y   Blasting: N   Boring: Y

Company:        GENESIS FLOORING LLC                      Type: CONT
Co. Address:    1302 E MAPLE AVE  First Time: N
City:           STERLING  State:VA  Zip:20164
Company Phone:  703-499-3153
Contact Name:   REYNA PAZ                   Contact Phone:  703-499-3153
Email:          reyna1.paz@gmail.com
Field Contact:  REYNA
Fld. Contact Phone:7036561363

Mapbook:  5991B2
Grids:    3840B7722D-32  3840B7722D-33  3840B7722D-43  3840B7722D-44
Grids:    3840C7722D-03  3840C7722D-04

Members: 
CMC091 = COMCAST (CMC)                  NVE901 = NORTHERN VIRGINIA ELECTR (NVE) 
PWE792 = PRINCE WILLIAM COUNTY PU (PWE) VAW902 = VIRGINIA AMERICAN WATER  (VAW) 
VZN111 = VERIZON (VZN)                  WGL904 = WASHINGTON GAS (WGL) 

Seq No:   451 B

