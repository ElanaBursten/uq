
------=_Part_7179_21706725.1352152151761
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1616 SC811 Remote 11/05/2012 04:47:00 PM 1211052386 Update 

Notice Number:    1211052386        Old Notice:                        
Sequence:         1616              Created By:      R-TRS             

Created:          11/05/12 04:47 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        1749, 1755 GIBBS SHOALS RD                            
Intersection:  FOUR MEADOWS LN                                       
Subdivision:                                                         

Lat/Long: 34.895877,-82.243498
Second:  34.896493,-82.243033

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

MARK THE ENTIRE FRONT OF PROPERTIES OF    
1749 AND 1755 GIBBS SHOALS RD//THERE WILL BE DRIVEWAY BORES//APPROX TOTAL FT  
12500 ENDING AT W PHILLIPS RD                                                 

Directions:
OAK PLACE AND SUBER RD ARE OTHER ROADS IN AREA
                               

Remarks:

**WORK IS IN PROGRESS**
Updated from V2 notice.                              

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 CCMZ41 



------=_Part_7179_21706725.1352152151761--