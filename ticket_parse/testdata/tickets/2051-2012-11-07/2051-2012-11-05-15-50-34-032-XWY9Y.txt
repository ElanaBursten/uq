
------=_Part_2518_26112873.1352148333482
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 26 SC811 Remote 11/04/2012 10:03:00 PM 1211040043 Normal 

Notice Number:    1211040043        Old Notice:                        
Sequence:         26                Created By:      R-TRS             

Created:          11/04/12 10:12 PM                                     
Work Date:        11/07/12 11:59 PM                                     
Update on:        11/28/2012        Good Through:    12/03/2012        

Caller Information:
ANSCO & ASSOCIATES 
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Contractor        
Caller:            TAMMY STEGALL      Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  SPARTANBURG          Place:  GREER             
Street:        309 OLD WOODRUFF RD                                   
Intersection:  BOILING CIRCLE                                        
Subdivision:                                                         

Lat/Long: 34.920126,-82.212641
Second:  34.920948,-82.210967

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC, RELOCATE CABLE                                   
Work Done By:  ANSCO                Duration:        1 WEEK            

Instructions:
WORK IS FOR ATT//REF JOB 29F22288N

MARK ENTIRE FRONT OF PROPERTY             

Directions:
OTHER ROADS IN AREA: NEW WOODRUFF RD AND MAPLE DR                             

Remarks:

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 



------=_Part_2518_26112873.1352148333482--