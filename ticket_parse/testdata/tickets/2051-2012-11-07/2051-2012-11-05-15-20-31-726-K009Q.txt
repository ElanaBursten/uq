
------=_Part_213_29057056.1352146688242
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1354 SC811 Remote 11/05/2012 03:08:00 PM 1211051981 Normal 

Notice Number:    1211051981        Old Notice:                        
Sequence:         1354              Created By:      R-TRS             

Created:          11/05/12 03:15 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES, LLC
43 SENTELL ROAD
GREENVILLE, SC 29611
Company Fax:      (864) 295-8794    Type:            Excavator         
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        101, 103 PRESTON DR                                   
Intersection:  LANDRUM DR                                            
Subdivision:                                                         

Lat/Long: 34.944321,-82.266666
Second:  34.944759,-82.26657

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE, INSTALL SPLICE PIT                                   
Work Done By:  ANSCO                Duration:        3 DAYS            

Instructions:
WORK IS FOR ATT//REF JOB 29F22338N

MARK ENTIRE FRONT OF  PROPERTIES AND ROAD 
IN FRONT OF PROPERTIES OF 101 AND 103 PRESTON DR                              

Directions:
OTHER ROADS IN AREA: BRANNON AVE AND N BUNCOMBE RD                            

Remarks:

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 



------=_Part_213_29057056.1352146688242--