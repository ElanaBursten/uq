
------=_Part_2514_26256740.1352148327101
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 17 SC811 Remote 11/05/2012 07:13:00 AM 1211050026 Resend 

Notice Number:    1211050026        Old Notice:      1211040047        
Sequence:         17                Created By:      KBS               

Created:          11/05/12 07:14 AM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:                          Good Through:                      

Caller Information:
ANSCO & ASSOCIATES 
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Contractor        
Caller:            TAMMY STEGALL      Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  SPARTANBURG          Place:  GREER             
Street:        OLD WOODRUFF RD                                       
Intersection:  NEW WOODRUFF RD                                       
Subdivision:                                                         

Lat/Long: 34.919591,-82.212475
Second:  34.919591,-82.212475

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC, RELOCATE CABLE                                   
Work Done By:  ANSCO                Duration:        1 WEEK            

Instructions:
WORK IS FOR ATT//REF JOB 29F22288N

MARK ENTIRE INTERSECTION AND BOTH SIDES OF
ROAD FOR 50 FT IN ALL DIRECTIONS                                              

Directions:
OTHER ROADS IN AREA: MCELRATH RD AND MAPLE DR                                 

Remarks:
**RESENDING DUE TO: CORRECTING THE DATE ON REQUEST DUE TO SYSTEM              
MISCALCULATION**                                                              

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 



------=_Part_2514_26256740.1352148327101--