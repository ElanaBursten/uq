
------=_Part_5398_19992582.1352150509967
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1521 SC811 Remote 11/05/2012 04:19:00 PM 1211052252 Update 

Notice Number:    1211052252        Old Notice:                        
Sequence:         1521              Created By:      R-TRS             

Created:          11/05/12 04:20 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        GIBBS SHOALS RD                                       
Intersection:  W PHILLIPS RD                                         
Subdivision:                                                         

Lat/Long: 34.876424,-82.240068
Second:  34.876424,-82.240068

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

MARK THE ENTIRE INTERSECTION AND BOTH     
SIDES OF ROAD FOR 50 FT IN ALL DIRECTIONS//APPROX TOTAL FT 12500 ENDING AT W  
PHILLIPS RD                                                                   

Directions:
MEDFORD DR AND DILLARD RD ARE OTHER ROADS IN AREA                             

Remarks:


**WORK IS IN PROGRESS**
Updated from V2 notice.                             

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 PNGZ81 CCMZ41 



------=_Part_5398_19992582.1352150509967--