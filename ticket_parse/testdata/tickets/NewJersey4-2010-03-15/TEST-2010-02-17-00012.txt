
DIG REQUEST from UFPO for: UNI WTR NEW YRK            Taken: 02/17/2010 00:27
To:                                  Transmitted: 02/17/2010 11:54 00000

Ticket: 02170-146-001-00 Type: Emergency     Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: TAPPAN  /P
Addr:  From: 4      To:        Name:    INDEPENDENCE                   AVE  
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: MARK 50 FT SOUTH OF CHAPEL CT 
      : SEE CREW ON SITE
NearSt: CHAPEL CT AND LOWE LN
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: WATER MAIN REPAIR
Start Date and Time: 02/17/2010 00:29
------------------------------------------------------------------------------
Contact Name: CHRIS OTT
Company: UNITED WATER NEW YORK
Addr1: 360 W NYACK RD                  Addr2:                                 
City: WEST NYACK                       State: NY    Zip: 10994
Phone: 845-623-1500                    Fax: 845-620-3347                    
Email:                                                                        
Field Contact: CHRIS  **PLEASE RESPOND TO THESE #'S
Alt Phone: 845-358-3816                Fax: 845-358-4023
Working for: SELF
------------------------------------------------------------------------------
Comments: EMERGENCY, CREW IS ON SITE NOW, THIS IS A THREAT TO
        : LIFE/PROPERTY/VITAL UTILITY.
        : DIG SITE ALSO AFFECTS: ORANGETOWN /T, ORANGEBURG /P
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : TWN ORANGETOWN                   UNI WTR NEW YRK                  
       : BELL-VALHALLA / HDSN VLY         



