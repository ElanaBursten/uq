
DIG REQUEST from UFPO for: CBLVSN WARWICK             Taken: 02/16/2010 12:45
To:                                  Transmitted: 02/17/2010 11:53 00000

Ticket: 02160-040-017-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ORANGE               Place: WARWICK  /T
Addr:  From: 280    To:        Name:    STATE SCHOOL                   RD   
Cross: From:        To:        Name:    KINGS                          HWY  
Offset:
------------------------------------------------------------------------------
Locate: AS FACING THE BUILDING WRK ON THE LEFT SIDE AS FACING KINGS HWY
      : WHERE THE SERVICE COMES INTO THE BUILDING OUT TO THE ROAD
NearSt: CORNER
Means of Excavation: MINI EXCAVATOR                          Blasting: N

Work Type: REPLACING SEWER PIPE
Start Date and Time: 02/19/2010 07:30
------------------------------------------------------------------------------
Contact Name: MAUREEN JENNINGS
Company: MATERIAL PROCESSORS INC
Addr1: 280 STATE SCHOOL RD             Addr2:                                 
City: WARWICK                          State: NY    Zip: 10990
Phone: 845-986-1366                    Fax: 888-270-4572                    
Email:                                                                        
Field Contact: JEFF
Cell Phone: 914-447-4110               
Working for: SAME
------------------------------------------------------------------------------
Comments: Lookup Type: INTERSECTION
------------------------------------------------------------------------------

Members: CBLVSN WARWICK                   O&R UTILS / ORNG                 
       : VIL WARWICK                      



