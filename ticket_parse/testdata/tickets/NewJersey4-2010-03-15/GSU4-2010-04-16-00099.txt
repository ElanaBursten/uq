
DIG REQUEST from UFPO for: CBLVSN W NYACK             Taken: 04/16/2010 16:37
To: UTILIQUEST / PRIMARY             Transmitted: 04/16/2010 16:40 00098

Ticket: 04160-139-163-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: NEW CITY  /P
Addr:  From: 12     To:        Name:    DOLPHIN                        RD   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: STARING ON RIGHT SIDE OF DRIVEWAY AT  # 8 TERMINATING AT RIGHT SIDE OF
      : DRIVEWAY # 12, PED TO PED
      : MRKED IN WHITE
NearSt: FERNDALE RD
Means of Excavation: HAND, MISSILE                           Blasting: N

Work Type: INSTALLING CATV
Start Date and Time: 04/21/2010 07:00
------------------------------------------------------------------------------
Contact Name: DESMOND SHEPHERD
Company: H2O LANDSCAPE DESIGN
Addr1: 2600 NETHERLAND AVE             Addr2:                                 
City: RIVERDALE                        State: NY    Zip: 10463
Phone: 718-796-9099                    Fax: 914-472-4202                    
Email: SHEPHERD.D@H2O-NYC.COM                                                 
Field Contact: DESMOND-PLEASE CALL CELL # FIRST
Cell Phone: 914-346-9799               Fax: 914-472-4200
Working for: CABLEVISION
------------------------------------------------------------------------------
Comments: DIG SITE ALSO AFFECTS: CLARKSTOWN /T
        : Lookup Type: STREET
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : RCKLND CTY SWR 1                 T CLARKSTWN HWY                  
       : UNI WTR NEW YRK                  BELL-VALHALLA / HDSN VLY         



