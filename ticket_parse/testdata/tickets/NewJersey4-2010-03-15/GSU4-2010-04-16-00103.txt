
DIG REQUEST from UFPO for: CBLVSN W NYACK             Taken: 04/16/2010 20:03
To: UTILIQUEST / PRIMARY             Transmitted: 04/16/2010 20:04 00102

Ticket: 04160-154-028-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: CLARKSTOWN  /T
Addr:  From: 70     To:        Name:    LAKE                           RD   W
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: PLS LOCATE THE FRONT OF PROPERTY IN DRIVEWAY AREA
NearSt: NY RT 303 AND OLD HAVERSTRAW RD
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: NEW DRIVEWAY INSTALL
Start Date and Time: 04/21/2010 07:30
------------------------------------------------------------------------------
Contact Name: ERIC GABRIELSON
Company: ER GABRIELSON
Addr1: 221 N MIDLAND AVE               Addr2:                                 
City: NYACK                            State: NY    Zip: 10960
Phone: 845-358-4916                    Fax: 845-353-3668                    
Email: ERGABRIELSON@AOL.COM                                                   
Field Contact: ERIC GABRIELSON
Cell Phone: 914-490-7695
Working for: HOMEOWNER
------------------------------------------------------------------------------
Comments: DIG SITE ALSO AFFECTS: CONGERS /P
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : RCKLND CTY SWR 1                 T CLARKSTWN HWY                  
       : UNI WTR NEW YRK                  BELL-VALHALLA / HDSN VLY         



