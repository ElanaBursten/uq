
DIG REQUEST from UFPO for: CBLVSN WARWICK             Taken: 04/16/2010 17:33
To: UTILIQUEST / PRIMARY             Transmitted: 04/16/2010 17:37 00100

Ticket: 04160-154-017-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ORANGE               Place: CHESTER  /T
Addr:  From: 55     To:        Name:    RUTH                           LN   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: PLS LOCATE IN THE FRONT AND ALONG THE RIGHT PROPERTY LINE
NearSt: RIDGE RD
      : (ONLY CROSS STREET GIVEN)
Means of Excavation: EXCAVATOR                               Blasting: N

Work Type: TREE PLATING
Start Date and Time: 04/21/2010 08:00
------------------------------------------------------------------------------
Contact Name: KURT ALBERS
Company: EAGLE LANDSCAPING
Addr1: 7 ROMAN CT                      Addr2:                                 
City: WEST NYACK                       State: NY    Zip: 10994
Phone: 845-496-7191                    Fax: 845-638-0067                    
Email:                                                                        
Field Contact: KURT
Cell Phone: 914-329-6109
Working for: PATRICK DONOVAN
------------------------------------------------------------------------------
Comments: Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN WARWICK                   FRNTIER HDVLY E                  
       : O&R UTILS / ORNG                 T CHSTR-ORG CTY                  



