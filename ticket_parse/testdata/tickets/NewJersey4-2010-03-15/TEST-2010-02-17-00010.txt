
DIG REQUEST from UFPO for: CBLVSN WARWICK             Taken: 02/14/2010 10:43
To:                                  Transmitted: 02/17/2010 11:53 00000

Ticket: 02140-150-009-00 Type: Emergency     Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ORANGE               Place: FLORIDA  /V
Addr:  From: 15     To: 19     Name:    DUSSENBURY                     DR   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: LOCATE FRONT ROW OF PROPERTIES LISTED
NearSt: STURR LN AND STURR LN
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: WATER MAIN REPAIR
Start Date and Time: 02/14/2010 10:43
------------------------------------------------------------------------------
Contact Name: TIM BRUNSWICK
Company: VILLAGE OF FLORIDA
Addr1: 28 / 32 MEADOW RD               Addr2:                                 
City: FLORIDA                          State: NY    Zip: 10921
Phone: 845-651-4332                    Fax: 845-651-7643                    
Email:                                                                        
Field Contact: TIM
Cell Phone: 845-741-1325               
Working for: SELF
------------------------------------------------------------------------------
Comments: EMERGENCY, CREW IS ON WAY TO SITE NOW, THIS IS A THREAT TO
        : LIFE/PROPERTY/VITAL UTILITY.
        : DIG SITE ALSO AFFECTS: WARWICK /T
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN WARWICK                   O&R UTILS / ORNG                 
       : ORANGE CTY DPW                   VIL FLORIDA                      
       : VIL WARWICK                      WARWICK VLY TEL                  



