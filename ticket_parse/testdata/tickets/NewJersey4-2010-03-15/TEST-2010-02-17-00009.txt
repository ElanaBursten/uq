
DIG REQUEST from UFPO for: CBLVSN WARWICK             Taken: 02/15/2010 22:17
To:                                  Transmitted: 02/17/2010 11:53 00000

Ticket: 02150-154-015-00 Type: Emergency     Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ORANGE               Place: FLORIDA  /V
Addr:  From: 28     To:        Name:    NEW                            ST   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: PLS LOCATE CURB TO CURB, CURB TO FOUNDATION
NearSt: FARRIES AVE AND GLENMERE AVE
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: GAS LEAK REPAIR
Start Date and Time: 02/15/2010 22:17
------------------------------------------------------------------------------
Contact Name: DAWN WILLIAMS
Company: ORANGE & ROCKLAND SPRING VALLEY (EMER)
Addr1: 390 W NY RT 59                  Addr2:                                 
City: SPRING VALLEY                    State: NY    Zip: 10977
Phone: 845-577-3883                    Fax: 845-577-3635                    
Email:                                                                        
Field Contact: DAWN WILLIAMS
Alt Phone: 845-577-3883                
Working for: O&R
------------------------------------------------------------------------------
Comments: EMERGENCY, CREW IS ON SITE NOW, THIS IS A THREAT TO
        : LIFE/PROPERTY/VITAL UTILITY.
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN WARWICK                   O&R UTILS / ORNG                 
       : VIL FLORIDA                      WARWICK VLY TEL                  



