
UQSTSO 00554A USAS 01/03/11 10:17:00 A10030372-00A NORM NEW GRID

Ticket : A10030372  Date: 01/03/11 Time: 10:11 Oper: AVRWC Chan: WEB
Old Tkt: A10030372  Date: 01/03/11 Time: 10:16 Oper: AVRWC Revision: 00A

Company: APPLE VALLEY RANCHOS WATER CO. Caller: KEVIN PHILLIPS
Co Addr: 21760 OTTAWA RD
City&St: APPLE VALLEY, CA               Zip: 92308
Phone: 760-247-6484 Ext:      Call back: M-F 8AM TO 5PM
Formn: DAVE FORTIN          Phone: 760-240-8316
Email: KEVINP@AVRWATER.COM

State: CA County: SAN BERNARDINO  Place: APPLE VALLEY
Delineated: Y
Delineated Method: WHITEPAINT,OTHER
Address: 21291       Street:SAUVINGNON LN
X/ST 1 : TAMIANI RD
Locat: FRONT AT STREET

Excav Enters Into St/Sidewalk: Y

Grids: 4387G034     4387H033
Lat/Long  : 34.491924/-117.201996 34.491924/-117.198677
          : 34.490765/-117.201996 34.490765/-117.198677
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : WATER SERVICE REPLACEMENT
Wkend: N  Night: N
Work date: 01/05/11 Time: 10:12 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : APPLE VALLEY RANCHOS WATER CO

Tkt Exp: 01/31/11

Mbrs : AVLY01 AVR01  SWG18  UCHARTERHD    USCE73 UVZVICT
