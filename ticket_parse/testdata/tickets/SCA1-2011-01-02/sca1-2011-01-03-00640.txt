
UQSTSO 00555A USAS 01/03/11 10:17:21 A10030374-00A NORM NEW GRID

Ticket : A10030374  Date: 01/03/11 Time: 10:15 Oper: ADS Chan: 100
Old Tkt: A10030374  Date: 01/03/11 Time: 10:17 Oper: ADS Revision: 00A

Company: GSI                            Caller: RYAN WHITE-VERIZON
Co Addr: UNK
City&St:                                Zip:
Phone: 951-235-0592 Ext:      Call back: 7-330 M-F
Formn: ADAM HOLGUIN         Phone: 951-533-1011

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 13003       Street:EMPTY SADDLE CT
X/ST 1 : BLACK HORSE CIR
Locat: ADDRESS IS AT W/END OF ST (IN CUL-DE-SAC)

Excav Enters Into St/Sidewalk: N

Grids: 0835B041
Lat/Long  : 33.731538/-117.433476 33.731078/-117.432689
          : 33.730859/-117.433873 33.730399/-117.433086
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL FIBER OPTICS
Wkend: N  Night: N
Work date: 01/05/11 Time: 10:15 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : VERIZON OC3293064

Tkt Exp: 01/31/11

Mbrs : EVW37  SCG1CO TWCWRIV       USCE77 UVZMENIF
