
UQSTSO 00549A USAS 01/03/11 10:15:53 A10030369-00A NORM NEW GRID

Ticket : A10030369  Date: 01/03/11 Time: 10:07 Oper: EVA Chan: 100
Old Tkt: A10030369  Date: 01/03/11 Time: 10:15 Oper: EVA Revision: 00A

Company: DEMO UNLIMITED                 Caller: LINDA SMITH
Co Addr: 81750 AVENUE 50
City&St: INDIO, CA                      Zip: 92201      Fax: 760-342-5898
Phone: 760-775-5884 Ext:      Call back: ANYTIME
Formn: MIKE                 Phone: 760-275-9509
Email: DEMOLINDAB@AOL.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:UP RAILROAD TRACKS
X/ST 1 : S LAUREL AVE
Locat: FRM THE UP RAILROAD TRACKS TO 25FT N AND S OF THE  U P RAILROAD TRACKS
     : FRM LAUREL AVE ( IF EXTENDED   FRM W MAIN ST )  CONT E AND W FOR 50FT
     : (  UP RAILRAD TRACKS LOC APPROX  120FT N OF W MAIN ST  ON LAUREL AVE IF
     : EXTENDED N )

Excav Enters Into St/Sidewalk: N

Grids: 0642B012
Lat/Long  : 34.061854/-117.653005 34.061794/-117.651613
          : 34.061030/-117.653040 34.060970/-117.651649
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALLING CULVERT
Wkend: N  Night: N
Work date: 01/05/11 Time: 10:08 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : UNION PACIFIC RAILROAD

Tkt Exp: 01/31/11

Mbrs : GSUTWT KINDER4B00    LVL3CM MCISOCAL      MPWRLA ONTRAF ONTWTRSWR
SCG1OD USCE34 UVZRCC
