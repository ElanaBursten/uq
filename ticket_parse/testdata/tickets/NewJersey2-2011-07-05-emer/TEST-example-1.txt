New Jersey One Call System        SEQUENCE NUMBER 0003    CDC = GPU3 

Transmit:  Date: 07/04/11   At: 04:36 

*** E M E R G E N C Y     *** Request No.: 111850008 

Operators Notified: 
BAN     = VERIZON                       CNJ     = AQUA NEW JERSEY, INC           
EG2     = ELIZABETHTOWN GAS COMPANY     GPU     = JERSEY CENTRAL POWER & LI      
SE2     = SERVICE ELECTRIC CABLE TV     UNI     = CENTURY LINK                   

Start Date/Time:    07/04/11   At 04:30  Expiration Date:  

Location Information: 
County: WARREN                 Municipality: GREENWICH 
Subdivision/Community: STEWARTSVILLE 
Street:               6 GREENWICH CHURCH ROAD 
Nearest Intersection: BEATTYS ROAD 
Other Intersection:   STATE RTE 173 
Lat/Lon: 
Type of Work: EMERGENCY - BROKEN POLE 
Block:                Lot:                Depth: 10FT 
Extent of Work: 20FT RADIUS OF POLE #NJ498GW. 
Remarks:  
  Working For Contact:  MARILYN GNIRREP 

Working For: JCP&L 
Address:     300 MADISON AVE 
City:        MORRISTOWN, NJ  07960 
Phone:       800-843-8621   Ext:  

Excavator Information: 
Caller:      MARILYN GNIRREP 
Phone:       800-843-8621   Ext:  

Excavator:   JCP&L 
Address:     300 MADISON AVE 
City:        MORRISTOWN, NJ  07960 
Phone:       800-843-8621   Ext:          Fax:  973-644-4263 
Cellular:    800-843-8621 
Email:        
End Request 



