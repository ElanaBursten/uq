UULC 
Ticket No: 13020147               2 FULL BUSINESS 
Send To: QLNWA16    Seq No:  130  Map Ref:  

Transmit      Date:  1/29/13   Time: 12:02 pm    Op: orpaul 
Original Call Date:  1/29/13   Time: 11:53 am    Op: webusr 
Work to Begin Date:  2/01/13   Time: 12:00 am 

State: WA            County: KING                    Place: SEATTLE 
Address: 801         Street: AURORA AVE N 
Nearest Intersecting Street: VALLEY ST 

Twp: 25N   Rng: 4E    Sect-Qtr: 38-NE,37-SE-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6268889 Lon:-122.3440728 SE Lat: 47.6260476 Lon:-122.3430045 

Type of Work: FIRE HYDRANT REPAIR 
Location of Work: LOCATE HYDRANT MARKED IN WHITE NE CORNER 15 FT N OF VALLEY
: ST 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 564 J-3 
:  

Company     : SEATTLE PUBLIC UTILITIES 
Contact Name: CHARLES JACKSON                  Phone: (206)386-1826 
Cont. Email : CHARLES.JACKSON@SEATTLE.GOV 
Alt. Contact: CHARLES JACKSON-CELL             Phone: (206)972-2538 
Contact Fax :  
Work Being Done For: SEATTLE PUBLIC UTILITIES 
Additional Members:  
CC7700     KCMTRO01   LEVL301    MCI01      METRAN01   MTRMED01   PUGG03 
 SEACL01    SEAH2001   SEASIG01   SEAWW01