

From: IRTHNet  At: 11/05/13 05:34 PM  Seq No: 3

Ticket No: 13242962               48 HOUR NOTICE 
Send To: PGE04      Seq No:   43  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:34 PM    Op: orjodi 
Original Call Date: 11/05/13   Time:  2:27 PM    Op: orjodi 
Work to Begin Date: 11/07/13   Time:  2:30 PM 

State: OR            County: MARION                  Place: WOODBURN 
Address:             Street: NEWBERG HWY 
Nearest Intersecting Street: ARNEY ROAD 

Twp: 5S    Rng: 2W    Sect-Qtr: 12-SW-NW 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.1513602Lon: -122.8843377SE Lat: 45.1508799Lon: -122.8838378 

Type of Work: INSTALL MAINLINE COPPER TELE 
Location of Work: STARTING AT THE MANHOLE AT THE NW CORNER OF INTER MARK N ON
: W SIDE APX 20FT THEN CROSS ARNEY TO THE E SIDE TO PED NUMBER 100, INCLUDING
: APX 20FT RADIUS OF PED 

Remarks: AREA WILL BE MARKED IN WHITE 
:  

Company     : C2 UTILITY CONTRACTORS           Best Time:   
Contact Name: AARON MORGAN                     Phone: (541)741-2211 
Email Address:   
Alt. Contact: AARON'S CELL                     Phone: (503)932-7565 
Contact Fax : (541)741-2204 
Work Being Done For: CENTRYLINK 
Additional Members:  
GERTEL01   NWN01      ODOTRE02   QLNOR14    WAVE01     WOOD01
