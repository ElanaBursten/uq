

From: IRTHNet  At: 11/05/13 05:47 PM  Seq No: 13

Ticket No: 13242949               PRIORITY 
Send To: PGE01      Seq No:  212  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:20 PM    Op: orcbcj 
Original Call Date: 11/05/13   Time:  2:19 PM    Op: orcbcj 
Work to Begin Date: 11/06/13   Time: 10:00 AM 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address:   4008      Street: SE 33RD AVE 
Nearest Intersecting Street: SE GLADSTONE 

Twp: 1S    Rng: 1E    Sect-Qtr: 12-SW-SE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.4944752Lon: -122.6318241SE Lat: 45.4934559Lon: -122.6308648 

Type of Work: REPLACE WATER SERVICE 
Location of Work: EXCAVATION SITE IS ON AN UNKNOWN SIDE OF THE ROAD. 
: ADD APX .5BLK N OF THE ABV INTER. MARK THE ENTIRE FRONT OF THE PROP AT THE
: ABV ADD.
:  MADE AWARE OF STATE LAW/NO GUARANTEE/MUST WAIT FOR MARKINGS 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 627 A3 
: CALLER REQUESTS MARKS BY 11/06/2013 BY 10:00 AM 

Company     : LINESCAPE                        Best Time:   
Contact Name: BOB SMYTHE                       Phone: (503)969-4409 
Email Address:  LINESCAPE1@AOL.COM 
Alt. Contact: DOUG RUSSELL                     Phone: (503)969-5204 
Contact Fax :  
Work Being Done For: MOLTENI 
Additional Members:  
CMCST01    NWN01      PTLD03     QLNOR17
