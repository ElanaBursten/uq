
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11172207 
Transmit      Date: 04/07/11      Time: 04:54 PM           Op: webusr 
Original Call Date: 04/07/11      Time: 04:52 PM           Op: webusr 
Work to Begin Date: 04/12/11      Time: 12:01 AM 
Expiration    Date: 04/26/11      Time: 11:59 PM 

Place: LANDOVER HILLS 
Address: 3815        Street: 72ND AVE 
Nearest Intersecting Street: RT 450 

Type of Work: FENCE 
Extent of Work: FACING HOUSE LEFT SIDE 
Remarks:  

Company      : LONG FENCE 
Contact Name : SHIRLEY DAVIS                  Fax          : (301)336-0743 
Contact Phone: (301)350-2400                  Ext          :  
Caller Address: 8545 EDGEWORTH DR 
                CAPITOL HEIGHTS, MD 20743 
Email Address:  djessup@longfence.com 
Alt. Contact :                                Alt. Phone   : (301)350-2400 
Work Being Done For: ROBINSON 
State: MD              County: PRINCE GEORGES 
MPG:  Y 
Caller    Provided:  Map Name: PG    Map#: 5410 Grid Cells: J10 
Computer Generated:  Map Name: PG    Map#: 5410 Grid Cells: J10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 38.9437820Lon: -76.8875270 SE Lat: 38.9375013Lon: -76.8812744 
Explosives: N 
ATM01      LTC01      MCI01      PEPCOPG    QWESTM01   WGL06      WSS01 
Send To: BGEPG     Seq No: 0075   Map Ref:  
         BGEPGG            0075 
         VPG               0345 
