
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11172209               Update Of: 11172201
Transmit      Date: 04/07/11      Time: 04:59 PM           Op: mdsuza 
Original Call Date: 04/07/11      Time: 04:55 PM           Op: mdsuza 
Work to Begin Date: 04/12/11      Time: 12:01 AM 
Expiration    Date: 04/26/11      Time: 11:59 PM 

Place: FORESTVILLE 
Address: 5955        Street: SURREY SERVICE DRIVE 
Nearest Intersecting Street: REGENCY PARKWAY 

Type of Work: REMOVING & REPLACING SIDEWALK, CURB & GUTTER 
Extent of Work: LOC FRONT  OF THE PROPERTY AT  HOUSES # 5955 THROUGH 5951
: SURREY SERVICE DRIVE. 
Remarks: ALL WORK TAKING PLACE WITHIN CALLER PROVIDED GRID(S) 

Company      : V M P CONSTRUCTION INC 
Contact Name : JAMIE GARCIA                   Fax          : (301)699-7715 
Contact Phone: (301)699-5949                  Ext          :  
Caller Address: 9635 ANNAPOLIS RD 
                LANHAM, MD 20706 
Email Address:  vmpci06@gmail.com 
Alt. Contact : MR RAMOS CELL#                 Alt. Phone   : (240)375-1613 
Work Being Done For: PRINCE GEORGES COUNTY 
State: MD              County: PRINCE GEORGES 
MPG:  Y 
Caller    Provided:  Map Name: PG    Map#: 5650 Grid Cells: F5 
Computer Generated:  Map Name: PG    Map#: 5650 Grid Cells: F5 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 38.8500192Lon: -76.9062765 SE Lat: 38.8437686Lon: -76.9000250 
Explosives: N 
PEPCOPG    WGL06      WSS01 
Send To: JTV02     Seq No: 0316   Map Ref:  
         VPG               0346 
