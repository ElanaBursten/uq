
GSUPLS063540056-00	JC	2006/12/20 10:44:08	00032

New Jersey One Call System        SEQUENCE NUMBER 0027   CDC = GP4
     
Transmit:  Date: 12/20/06   At: 0728

*** E M E R G E N C Y     *** Request No.: 063540056

Operators Notified:
    SE1=/SVC EL CATV|RFM/ WVT=/WARWICK V TELCO/ UNI=/EMBARQ     |CLS/ 
    UW3=/UW-MID ATLANTIC/ EG2=/ELIZABTHTWN GAS/ GP4=/JCP&L      |CLS/ 

Start Date/Time:    12/20/06   At 0723   Expiration Date: 

Location Information:
   County: SUSSEX     Municipality: VERNON
   Subdivision/Community: 
   Street:               11 BALDWIN DR
   Nearest Intersection: MOTT DR 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPAIR WATER MAIN
   Extent of Work: CURB TO CURB,CURB TO 50FT BEHIND CURB    DEPTH: 6FT
   Remarks:

   Working For:  UNITED WATER NEW JERSEY
   Address:      200 LAKESHORE DR
   City:         HAWORTH, NJ  07871
   Phone:        201-599-6013

Excavator Information:
   Caller:       CHRIS SANDERS           
   Phone:        973-827-6661            

   Excavator:    HANK SANDERS INC.
   Address:      PO BOX 1571
   City:         MCAFEE, NJ  07428
   Phone:        973-827-6661            Fax:  973-827-2171
   Cellular:     973-296-0739
   Email:        
End Request


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.


GSUPLS063540081-00	JC	2006/12/20 10:44:08	00045

New Jersey One Call System        SEQUENCE NUMBER 0040   CDC = GPP
     
Transmit:  Date: 12/20/06   At: 0747

*** E M E R G E N C Y     *** Request No.: 063540081

Operators Notified:
    BIT=/BRICK TWP MUA  / CC4=/CMCST-MNMTH|CLS/ NJN=/NJ NATL GAS|UTQ/ 
    BAN=/VERIZON    |CLS/ GPP=/JCP&L      |CLS/ 

Start Date/Time:    12/20/06   At 0742   Expiration Date: 

Location Information:
   County: OCEAN     Municipality: BRICK
   Subdivision/Community: 
   Street:               0 HONEYSUCKLE CT
   Nearest Intersection: ORCHID LN 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPAIR WTR LEAK
   Extent of Work: CURB TO CURB CURB TO 15FT BEHIND ALL...  DEPTH: 5 FT
     CURBS   M/O BEGINS AT C/L OF INTERSECTION & EXTENDS 50FT IN ALL
     DIRECTIONS
   Remarks:
     FIRE HYDRANT LEAKING

   Working For:  BRICK UTILITIES
   Address:      1551 HWY 88 WEST
   City:         BRICK, NJ  08724
   Phone:        732-458-7000

Excavator Information:
   Caller:       LINDA MEYER             
   Phone:        732-458-7000 X238       

   Excavator:    BRICK UTILITIES
   Address:      1551 HWY 88 WEST
   City:         BRICK, NJ  08724
   Phone:        732-458-7000            Fax:  732-458-8246
   Cellular:     
   Email:        
End Request


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.


GSUPLS063540060-00	JC	2006/12/20 10:44:08	00034

New Jersey One Call System        SEQUENCE NUMBER 0030   CDC = GPC
     
Transmit:  Date: 12/20/06   At: 0729

*** E M E R G E N C Y     *** Request No.: 063540060

Operators Notified:
    SPB=/SEASIDE PK BORO/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    12/20/06   At 0726   Expiration Date: 

Location Information:
   County: OCEAN     Municipality: SEASIDE PARK
   Subdivision/Community: 
   Street:               102 C ST
   Nearest Intersection: BAY BLVD 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPL BROKEN WTR LINE
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 4FT
   Remarks:

   Working For:  JEFFERY CORDARO
   Address:      102 C ST
   City:         SEASIDE PARK, NJ 
   Phone:        732-684-8834

Excavator Information:
   Caller:       DAVID DERASMO           
   Phone:        732-803-2086            

   Excavator:    DERASMO EXCAVATING
   Address:      22 WALLIS CT
   City:         BRICK, NJ  08724
   Phone:        732-803-2086            Fax:  732-785-9740
   Cellular:     
   Email:        
End Request


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.

