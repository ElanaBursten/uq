
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 11180737 
Transmit      Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Original Call Date: 04/12/11      Time: 01:51 PM           Op: webusr 
Work to Begin Date: 04/15/11      Time: 12:01 AM 
Expiration    Date: 04/29/11      Time: 11:59 PM 

Place: JOPPA 
Address: 1613        Street: GUNPOWDER RIDGE RD 
Nearest Intersecting Street: WOLTHAM COURT 

Type of Work: VZN:E7169100/27309/5845C 
Extent of Work: PLEASE LOCATE FROM F1615 GUNPOWDER RIDGE RD THROUGH RIGHT OF
: WAY TO PROPERTY. PLEASE MARK ALL SIDES OF PROPERTY. PLEASE LOCATE MAIN LINE,
: DROPS, FOR GROUND RODS AND FIOS. 
Remarks: BSW WO#V0009148 

Company      : ARMSTRONG CONSTRUCTION 
Contact Name : MR ARMSTRONG                   Fax          :  
Contact Phone: (410)940-2032                  Ext          :  
Caller Address: 9435 WASHINGTON BLVD STE M & N 
                LAUREL, MD 20723 
Email Address:  armstrongconstructionfios@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: VERIZON 
State: MD              County: HARFORD 
MPG:  Y 
Caller    Provided:  Map Name: HARF  Map#: 4465 Grid Cells: A10 
Computer Generated:  Map Name: HARF  Map#: 4465 Grid Cells: A10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.4437415Lon: -76.3750000 SE Lat: 39.4375000Lon: -76.3688133 
Explosives: N 
ATM01      HCPW01     LTC01      MLV01      VHF 
Send To: BGEHR     Seq No: 0094   Map Ref:  
         BGEHRG            0094 
