
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 11180744 
Transmit      Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Original Call Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Work to Begin Date: 04/15/11      Time: 12:01 AM 
Expiration    Date: 04/29/11      Time: 11:59 PM 

Place: GAITHERSBURG 
Address: 10505       Street: WAYRIDGE DR 
Nearest Intersecting Street: MEADERIDGE PLACE 

Type of Work: VZN:E7168740/07740/5845C 
Extent of Work: PLEASE LOCATE FROM HH F 10505 WAYRIDGE DR THROUGH RIGHT OF WAY
: TO PROPERTY. PLEASE MARK ALL SIDES OF PROPERTY. PLEASE LOCATE MAIN LINE,
: DROPS, FOR GROUND RODS AND FIOS. 
Remarks: BSW WO#V0009132 

Company      : BV TECHNOLOGIES 
Contact Name : SHAWN BOYD                     Fax          :  
Contact Phone: (484)753-5462                  Ext          :  
Caller Address: 9435 WASHINGTON BLVD STE M & N 
                LAUREL, MD 20723 
Email Address:  bvtechnologiesfios@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: VERIZON 
State: MD              County: MONTGOMERY 
MPG:  Y 
Caller    Provided:  Map Name: MONT  Map#: 5047 Grid Cells: F3 
Computer Generated:  Map Name: MONT  Map#: 5047 Grid Cells: F3 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.1750497Lon: -77.2187656 SE Lat: 39.1687975Lon: -77.2125295 
Explosives: N 
CPM01      PEPCOMC    TRU01      VMG        WSS01 
Send To: WGL06     Seq No: 0767   Map Ref:  

