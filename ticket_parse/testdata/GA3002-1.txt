

AGL101 03042 GAUPC 02/15/05 22:03:40 02155-502-053-000 NORMAL
Underground Notification
Ticket : 02155-502-053 Date: 02/15/05 Time: 21:54 Revision: 000

State: GA  County: CLARKE       Place: ATHENS
Addr : From:        To:        Name:    FRITZ MAR                      LN   
Cross: From:        To:        Name:
Offset:
Subdivision: TOWNE SQUARE S/D PHASE 2
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: BEGIN AT TOWN SQUARE LANE & LOCATE EAST TO THE TOWER ON THE GA. POWER
     : TRANSMISSION LINE.  S/D SIDE OF THE ROAD ONLY.

Grids    : 3359D8323A   3359D8324D  
Work type: INSTALLING POWER MAIN
Work date: 02/21/05 Time: 07:00 Hrs notc: 129 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 2 WEEKS
Done for : GEORGIA POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   UNDEFINED TOWN SQUARE LN UNDEFINED
        : *** LOOKUP BY MANUAL

Company : ROWAN COMMUNICATIONS                      Type: CONT
Co addr : 598 BLUEBERRY LN
City    : DAWSONVILLE                    State   : GA Zip: 30534
Caller  : MARY  ROWAN                    Phone   : 706-265-7581                
Fax     : 706-265-7581                   Alt. Ph.: 
Contact : ED ROWAN                       
Cellular: 770-652-6467                 

Submitted date: 02/15/05 Time: 21:54 Oper: 502 Chan: 999
Mbrs : AGL101 ATH90  ATH91  BSEA   GP280  ITM03 
-------------------------------------------------------------------------------



AGL115 03041 GAUPC 02/15/05 22:03:32 02155-502-052-000 NORMAL
Underground Notification
Ticket : 02155-502-052 Date: 02/15/05 Time: 21:50 Revision: 000

State: GA  County: HALL         Place: FLOWERY BRANCH
Addr : From:        To:        Name:    PROMENADE                      CRT  
Cross: From:        To:        Name:
Offset:
Subdivision: STERLING ON THE LAKE - POD F
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: BEGIN AT VINTAGE DR. & LOCATE ENTIRE STREET INCLUDING CUL-DE-SAC, B/S OF
     : THE ROAD

Grids    : 3408D8353A   3408C8353A   3408B8353A   3408D8354D   3408C8354D  
Grids    : 3408D8354C  
Work type: INSTALLING POWER MAIN
Work date: 02/21/05 Time: 07:00 Hrs notc: 129 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 2 WEEKS
Done for : JACKSON EMC
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : LAKE CROSSING DR. IS THE ENTRANCE STREET INTO STERLING ON THE LAKE
        : S/D.  REGATTA WAY IS THE ENTRANCE STREET INTO POD F OF STERLING ON
        : THE LAKE S/D.  VINTAGE DR. IS THE 2ND STREET TO THE LEFT OFF REGATTA
        : WAY.  PROMENADE CRT IS THE 1ST STREET TO THE RIGHT OFF VINTAGE DR.
        : *** NEAR STREET ***   UNDEFINED BLACKJACK RD UNDEFINED
        : *** WILL BORE BOTH SIDES OF ROAD
        : *** LOOKUP BY MANUAL

Company : ROWAN COMMUNICATIONS                      Type: CONT
Co addr : 598 BLUEBERRY LN
City    : DAWSONVILLE                    State   : GA Zip: 30534
Caller  : MARY  ROWAN                    Phone   : 706-265-7581                
Fax     : 706-265-7581                   Alt. Ph.: 
Contact : ED ROWAN                       
Cellular: 770-652-6467                 

Submitted date: 02/15/05 Time: 21:50 Oper: 502 Chan: 999
Mbrs : AGL115 BSEA   BSNE   GCV01  GVL50  JCK71 
-------------------------------------------------------------------------------



AGL101 03043 GAUPC 02/15/05 22:25:07 02155-502-054-000 NORMAL
Underground Notification
Ticket : 02155-502-054 Date: 02/15/05 Time: 22:07 Revision: 000

State: GA  County: CLARKE       Place: ATHENS
Addr : From:        To:        Name:    TOWNE SQUARE                   CRT  
Cross: From:        To:        Name:
Offset:
Subdivision: TOWNE SQUARE S/D PHASE 2
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: BEGIN AT FRITZ MAR LANE & LOCATE TO END OF STREET, B/S OF THE ROAD.

Grids    : 3359D8323A   3358A8324D   3359D8324D  
Work type: INSTALLING POWER MAIN
Work date: 02/21/05 Time: 07:00 Hrs notc: 128 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 2 WEEKS
Done for : GEORGIA POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : TOWNE SQUARE COURT IS THE NEW STREET INTO PHASE 2 OF TOWNE SQUARE S/D
        : OFF FRITZ MAR LANE.
        : *** NEAR STREET ***   UNDEFINED FRITZ MAR LN UNDEFINED
        : *** LOOKUP BY MANUAL

Company : ROWAN COMMUNICATIONS                      Type: CONT
Co addr : 598 BLUEBERRY LN
City    : DAWSONVILLE                    State   : GA Zip: 30534
Caller  : MARY  ROWAN                    Phone   : 706-265-7581                
Fax     : 706-265-7581                   Alt. Ph.: 
Contact : ED ROWAN                       
Cellular: 770-652-6467                 

Submitted date: 02/15/05 Time: 22:07 Oper: 502 Chan: 999
Mbrs : AGL101 ATH90  ATH91  BSEA   GP280  ITM03 
-------------------------------------------------------------------------------



AGL111 03044 GAUPC 02/15/05 23:50:04 02155-502-055-000 NORMAL
Underground Notification
Ticket : 02155-502-055 Date: 02/15/05 Time: 23:46 Revision: 000

State: GA  County: FORSYTH      Place: CUMMING
Addr : From: 4885   To:        Name:    ARBOR MEADOWS                  DR   
Cross: From:        To:        Name:
Offset:
Subdivision: ARBOR MEADOWS - LOT 33
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: ENTIRE PROPERTY

Grids    : 3415D8408A   3414A8408A   3414A8409D  
Work type: INSTALLING WATER SERVICE
Work date: 02/21/05 Time: 07:00 Hrs notc: 127 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 3 HRS
Done for : TUGGLE RESIDENTIAL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   UNDEFINED SPOT RD UNDEFINED
        : *** LOOKUP BY MANUAL

Company : WATSON PLUMBING, INC.                     Type: CONT
Co addr : 3682 MCCONNELL RD
City    : CUMMING                        State   : GA Zip: 30040
Caller  : JOHNNY WATSON                  Phone   : 404-281-2302                
Fax     : 678-513-0301                   Alt. Ph.: 
Email   : JHSLWATSON@AOL.COM

Submitted date: 02/15/05 Time: 23:46 Oper: 502 Chan: 999
Mbrs : AGL111 BSEA   CUM50  CUM51  GP280  PRST01 SAW73 
-------------------------------------------------------------------------------



AGL111 00002 GAUPC 02/16/05 00:01:57 02155-502-057-000 NORMAL
Underground Notification
Ticket : 02155-502-057 Date: 02/15/05 Time: 23:52 Revision: 000

State: GA  County: FORSYTH      Place: CUMMING
Addr : From: 9025   To:        Name:    BLAKEWOOD                      CT   
Cross: From:        To:        Name:
Offset:
Subdivision: RIVERSTONE PLANTATION - LOT 169
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: ENTIRE PROPERTY

Grids    : 3416A8400D   3417D8400D   3416A8400C   3417D8400C  
Work type: INSTALLING WATER SERVICE
Work date: 02/21/05 Time: 07:00 Hrs notc: 127 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 3 HRS
Done for : T.G. BUILDERS
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   UNDEFINED JOTEM DOWN RD UNDEFINED
        : *** LOOKUP BY MANUAL

Company : WATSON PLUMBING, INC.                     Type: CONT
Co addr : 3682 MCCONNELL RD
City    : CUMMING                        State   : GA Zip: 30040
Caller  : JOHNNY WATSON                  Phone   : 404-281-2302                
Fax     : 678-513-0301                   Alt. Ph.: 
Email   : JHSLWATSON@AOL.COM

Submitted date: 02/15/05 Time: 23:52 Oper: 502 Chan: 999
Mbrs : AGL111 BSEA   CAN01  FOR01  PRST01 SAW73 
-------------------------------------------------------------------------------



AGL111 00001 GAUPC 02/16/05 00:01:57 02155-502-056-000 NORMAL
Underground Notification
Ticket : 02155-502-056 Date: 02/15/05 Time: 23:49 Revision: 000

State: GA  County: FORSYTH      Place: CUMMING
Addr : From: 4820   To:        Name:    ARBOR HILL                     PLAC 
Cross: From:        To:        Name:
Offset:
Subdivision: ARBOR MEADOWS - LOT 8
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: ENTIRE PROPERTY

Grids    : 3415C8407B   3415B8407B   3415C8407A  
Work type: INSTALLING WATER SERVICE
Work date: 02/21/05 Time: 07:00 Hrs notc: 127 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 3 HRS
Done for : TUGGLE RESIDENTIAL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   UNDEFINED SPOT RD UNDEFINED
        : *** LOOKUP BY MANUAL

Company : WATSON PLUMBING, INC.                     Type: CONT
Co addr : 3682 MCCONNELL RD
City    : CUMMING                        State   : GA Zip: 30040
Caller  : JOHNNY WATSON                  Phone   : 404-281-2302                
Fax     : 678-513-0301                   Alt. Ph.: 
Email   : JHSLWATSON@AOL.COM

Submitted date: 02/15/05 Time: 23:49 Oper: 502 Chan: 999
Mbrs : AGL111 BSEA   CAN01  CUM50  CUM51  GP280  PRST01 SAW73 
-------------------------------------------------------------------------------



AGL114 00004 GAUPC 02/16/05 00:28:44 02115-034-042-001 NORMAL LATE
Underground Notification
Ticket : 02115-034-042 Date: 02/16/05 Time: 00:13 Revision: 001
Old Tkt: 02115-034-042 Date: 02/11/05 Time: 14:03

State: GA  County: GWINNETT     Place: SUWANEE
Addr : From: 350    To:        Name:    PEACHTREE INDUSTRIAL           BLVD NE
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: THE ISLAND LOC 500SQ FT AROUND THE HYDRANT ISLAND IS AT THE
     : INTERSECTION..

Grids    : 3403C8403A   3403B8403A   3403C8404D   3403B8404D   3403C8403B  
Grids    : 3403B8403B  
Work type: FIRE HYDRANT RPR
Work date: 02/16/05 Time: 07:00 Hrs notc: 006 Priority: 3
Legal day: 02/16/05 Time: 07:00 Good thru: 03/04/05 Restake by: 03/01/05
RespondBy: 02/15/05 Time: 23:59 Duration : 2 WKS
Done for : ABR
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** AGL114 IS LATE RESPONDING.  PLEASE RESPOND ASAP.
        : *** NEAR STREET ***   LAWRENCEVILLE SUWANEE RD
        : *** LOOKUP BY MANUAL

Company : STONE  MOUNTAIN CONTRACTING               Type: CONT
Co addr : 2156  B WEST PARK CT
City    : STONE MOUNTAIN                 State   : GA Zip: 30087
Caller  : LEGINA HINES                   Phone   : 770-413-4290                
Fax     :                                Alt. Ph.: 
Email   : KHINES@STMTN.COM

Submitted date: 02/16/05 Time: 00:13 Oper: 034 Chan: WEB
Mbrs : AGL114 BENCH1 BSNE   CBL01  GP267  GWI90  GWI91  JCK70  QWEST8 SWNE50
-------------------------------------------------------------------------------



AGL123 00005 GAUPC 02/16/05 00:28:45 02115-039-011-001 NORMAL LATE
Underground Notification
Ticket : 02115-039-011 Date: 02/16/05 Time: 00:14 Revision: 001
Old Tkt: 02115-039-011 Date: 02/11/05 Time: 08:05

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 525    To:        Name:    GLENGATE                       COVE 
Cross: From:        To:        Name:
Offset:
Subdivision: GATES AT GLENRIDGE
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Grids    : 3355A8421B   3355A8421A   3356D8421A   3355A8422D   3356D8422D  
Work type: INSTALL IRRIGATION
Work date: 02/16/05 Time: 07:00 Hrs notc: 006 Priority: 3
Legal day: 02/16/05 Time: 07:00 Good thru: 03/04/05 Restake by: 03/01/05
RespondBy: 02/15/05 Time: 23:59 Duration : 1 DAY
Done for : CRN HOMES
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** AGL123 IS LATE RESPONDING.  PLEASE RESPOND ASAP.
        : *** NEAR STREET ***   ABERNATHY RD NE
        : *** WILL BORE DRIVEWAY
        : *** LOOKUP BY MANUAL

Company : KELLEY IRRIGATION                         Type: CONT
Co addr : 3630 SHALLOWFORD RD
City    : MARIETTA                       State   : GA Zip: 30062
Caller  : TIM KELLEY                     Phone   : 770-973-5105                
Fax     :                                Alt. Ph.: 

Submitted date: 02/16/05 Time: 00:14 Oper: 039 Chan: WEB
Mbrs : AGL123 AGLN01 ATL02  ATT02  BSNE   COMNOR GP801B GP804  ICG01  LEV3  
     : MCI02  NU804  XFG01  XOC90 
-------------------------------------------------------------------------------



AGL102 00007 GAUPC 02/16/05 06:41:54 02165-033-001-000 EMERG
Underground Notification
Ticket : 02165-033-001 Date: 02/16/05 Time: 06:39 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 2420   To:        Name:    BUTNER                         RD   SW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3341C8431D   3341B8431D   3341C8431C   3341B8431C   3341C8431B  
Work type: PUMPING SEPTIC TANK
Work date: 02/16/05 Time: 06:41 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 06:41 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 06:41 Duration : 1 DAYS
Done for : STACY BROWN
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SERVICE OUT   CREW EN ROUTE
        : *** NEAR STREET ***   CAMPBELLTON RD
        : *** LOOKUP BY MANUAL

Company : ROOTER PLUS                               Type: CONT
Co addr : 5950 SHILOH RD E
Co addr2: SUITE H
City    : ALPHARETTA                     State   : GA Zip: 30005
Caller  : RITA ROSE                      Phone   : 770-888-1931                
Fax     :                                Alt. Ph.: 
Contact : OFFICE                         

Submitted date: 02/16/05 Time: 06:39 Oper: 033 Chan: WEB
Mbrs : AGL102 AGL108 ATL01  ATL02  BSCA   CPL81  EPT50  EPT51  EPT52  EPT53 
     : GP131 
-------------------------------------------------------------------------------



AGL119 00010 GAUPC 02/16/05 06:44:48 02165-033-003-000 EMERG
Underground Notification
Ticket : 02165-033-003 Date: 02/16/05 Time: 06:43 Revision: 000

State: GA  County: COBB         Place: POWDER SPRINGS
Addr : From: 1133   To:        Name:    CAPRICE                        DR   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3355C8439D   3355B8439D   3355C8439C   3355B8439C  
Work type: PUMPING SEPTIC TANK
Work date: 02/16/05 Time: 06:44 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 06:44 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 06:44 Duration : 1 DAYS
Done for : RICHARD DAMMER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SERVICE OUT   CREW EN ROUTE
        : *** NEAR STREET ***   FRIENDSHIP CHURCH RD

Company : ROOTER PLUS                               Type: CONT
Co addr : 5950 SHILOH RD E
Co addr2: SUITE H
City    : ALPHARETTA                     State   : GA Zip: 30005
Caller  : RITA ROSE                      Phone   : 770-888-1931                
Fax     :                                Alt. Ph.: 
Contact : OFFICE                         

Submitted date: 02/16/05 Time: 06:43 Oper: 033 Chan: WEB
Mbrs : AGL119 BSNW   COB01  COB02  COB70  COMNOR
-------------------------------------------------------------------------------



AGL111 00008 GAUPC 02/16/05 06:43:38 02165-033-002-000 EMERG
Underground Notification
Ticket : 02165-033-002 Date: 02/16/05 Time: 06:42 Revision: 000

State: GA  County: FULTON       Place: ROSWELL
Addr : From: 5800   To:        Name:    PLANTATION                     DR   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3402C8422A   3402C8423D   3402C8423C  
Work type: PUMPING SEPTIC TANK
Work date: 02/16/05 Time: 06:43 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 06:43 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 06:43 Duration : 1 DAYS
Done for : JEFF BRIDGES
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SEWAGE ESCAPING  CREW EN ROUTE
        : *** NEAR STREET ***   LAKE CHARLES

Company : ROOTER PLUS                               Type: CONT
Co addr : 5950 SHILOH RD E
Co addr2: SUITE H
City    : ALPHARETTA                     State   : GA Zip: 30005
Caller  : RITA ROSE                      Phone   : 770-888-1931                
Fax     :                                Alt. Ph.: 
Contact : OFFICE                         

Submitted date: 02/16/05 Time: 06:42 Oper: 033 Chan: WEB
Mbrs : AGL111 BSNW   CBL01  COB70  FUL01  FUL02  GP813 
-------------------------------------------------------------------------------



AGL102 00012 GAUPC 02/16/05 06:48:03 02165-033-005-000 EMERG
Underground Notification
Ticket : 02165-033-005 Date: 02/16/05 Time: 06:46 Revision: 000

State: GA  County: CLAYTON      Place: FOREST PARK
Addr : From: 3989   To:        Name:    MAGNOLIA                       LN   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3338B8421B   3338A8421B  
Work type: PUMPING SEPTIC TANK
Work date: 02/16/05 Time: 06:47 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 06:47 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 06:47 Duration : 1 DAYS
Done for : JOHN BROWN
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SEWAGE ESCAPING  CREW EN ROUTE
        : *** NEAR STREET ***   SWEATBRIER LN

Company : ROOTER PLUS                               Type: CONT
Co addr : 5950 SHILOH RD E
Co addr2: SUITE H
City    : ALPHARETTA                     State   : GA Zip: 30005
Caller  : RITA ROSE                      Phone   : 770-888-1931                
Fax     :                                Alt. Ph.: 
Contact : OFFICE                         

Submitted date: 02/16/05 Time: 06:46 Oper: 033 Chan: WEB
Mbrs : AGL102 AGL108 ATL02  BSCA   COMCEN COMPER CTD33  CWA01  CWA02  GP141B
-------------------------------------------------------------------------------



AGL114 00016 GAUPC 02/16/05 07:09:47 02165-500-003-000 NORMAL RESTAKE
Underground Notification
Ticket : 02165-500-003 Date: 02/16/05 Time: 06:53 Revision: 000
Old Tkt: 01315-500-162 Date: 01/31/05 Time: 08:37

State: GA  County: GWINNETT     Place: SUWANEE
Addr : From: 4600   To: 4600   Name:    SUWANEE DAM                    RD   NW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE FOR 50 FEET ON EITHER SIDE OF DRIVEWAY ON THIS PROPERTY..

Grids    : 3404C8404C   3404B8404C  
Work type: INSTALL UNDERGROUND CABLE
Work date: 01/19/05 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 
Done for : GEORGIA POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : RESTAKE OF 01125-502-166
        : RESTAKE OF 01315-500-162
        : *** NEAR STREET ***   UNDEFINED TENCH RD UNDEFINED
        : *** LOOKUP BY MANUAL

Company : GEORGIA POWER SOUTHERN COMPANY            Type: MEMB
Co addr : 3825 ROGERS BRIDGE RD
City    : DULUTH                         State   : GA Zip: 30097
Caller  : TERRY SMITH                    Phone   : 770-995-4778                
Fax     : 770-995-4890                   Alt. Ph.: 770-995-4778                
Email   : BTSMITH@SOUTHERNCO.COM

Submitted date: 02/16/05 Time: 06:53 Oper: 500 Chan: 999
Mbrs : AGL114 BSNE   CBL01  GP267  GWI90  GWI91  SGR50 
-------------------------------------------------------------------------------



AGL114 00013 GAUPC 02/16/05 07:08:36 02165-020-001-000 NORMAL RESTAKE
Underground Notification
Ticket : 02165-020-001 Date: 02/16/05 Time: 07:06 Revision: 000
Old Tkt: 02015-098-040 Date: 02/01/05 Time: 11:12

State: GA  County: GWINNETT     Place: ATLANTA
Addr : From:        To:        Name:    MIMMS                          DR   NW
Cross: From:        To:        Name:    PLEASANTDALE                   RD   
Offset:

State: GA  County: GWINNETT     Place: ATLANTA
Addr : From:        To:        Name:    MIMMS                          DR   NW
Cross: From:        To:        Name:    BUTTON GWINNETT                DR   
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOC FROM THE INTERSECTION OF MIMMS DR AND PLEASANTDALE RD TO THE
     : INTERSECTION OF MIMMS DR AND BUTTON GWINNETT DR ON MIMMS DR BOTH SIDES
     : OF RD AND 200FT N ON PLEASANTDALE RD AND 200FT BEYOND BUTTON GWINNETT DR

Grids    : 3354A8414C   3355D8414C   3354A8414B   3355D8414B  
Work type: INSTL SEWER FORCE MAIN LINE
Work date: 02/04/05 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 2-3MTHS
Done for : GLOBAL FORUM
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : RESTAKE OF: 02015-098-040
        : *** WILL BORE DRIVEWAY
        : *** LOOKUP BY MANUAL

Company : EDK COMPANY                               Type: CONT
Co addr : 2424 LANCE CT
City    : LOGANVILLE                     State   : GA Zip: 30052
Caller  : DAVID EVANS                    Phone   : 770-466-3600                
Fax     :                                Alt. Ph.: 

Submitted date: 02/16/05 Time: 07:06 Oper: 020 Chan: WEB
Mbrs : AGL114 BSNE   GPC41B GWI90  GWI91  LEV3   PPL01  WCI01 
-------------------------------------------------------------------------------



AGL123 00015 GAUPC 02/16/05 07:09:47 02165-046-001-000 NORMAL
Underground Notification
Ticket : 02165-046-001 Date: 02/16/05 Time: 07:02 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 38     To:        Name:    SPRINGLAKE                     PL   NW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: SPRINGLAKE PL  APTS    LOC ALL R/O/W AROUND TEMP CABLES ON THE GROUND
     : FROM BLDG 10 TO ENTRANCE OF APT

Grids    : 3348A8424A   3348B8424A  
Work type: REPLACE MAIN LINE CATV
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : COMCAST
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** NEAR STREET ***   HOWELL MILL RD
        : *** WILL BORE DRIVEWAY

Company : HESS CONTRACTING                          Type: CONT
Co addr : 1314 HILLCREST WAY
City    : LAWRENCEVILLE                  State   : GA Zip: 30043
Caller  : BILL HESS                      Phone   : 770-963-3516                
Fax     :                                Alt. Ph.: 404-431-7745                

Submitted date: 02/16/05 Time: 07:02 Oper: 046 Chan: WEB
Mbrs : AGL123 ATL01  ATL02  BSCA   COMCEN GP105 
-------------------------------------------------------------------------------



AGL111 00014 GAUPC 02/16/05 07:09:46 02165-023-001-000 NORMAL
Underground Notification
Ticket : 02165-023-001 Date: 02/16/05 Time: 07:03 Revision: 000

State: GA  County: FORSYTH      Place: CUMMING
Addr : From: 3160   To:        Name:    HURT BRIDGE                    RD   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE THE ENTIRE R\O\W ON BOTH SIDES OF THE ROAD

Grids    : 3415D8411B   3415D8411A   3415C8411A  
Work type: INSTALL WATER METER
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : FORSYTH CNTY WATER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** NEAR STREET ***   ARRON SOSEBEE RD
        : *** WILL BORE BOTH SIDES OF ROAD
        : *** LOOKUP BY MANUAL

Company : FORSYTH COUNTY WTR DEPT/OR STEPHENS CONS  Type: MEMB
Co addr : 110 EAST MAIN ST
Co addr2: SUITE 150
City    : CUMMING                        State   : GA Zip: 30040
Caller  : GARY STEPHENS                  Phone   : 770-205-4505                
Fax     : 770-205-4515                   Alt. Ph.: 

Submitted date: 02/16/05 Time: 07:03 Oper: 023 Chan: 999
Mbrs : AGL111 BSEA   FOR01  SAW73 
-------------------------------------------------------------------------------



AGL114 00018 GAUPC 02/16/05 07:09:53 02165-098-001-000 EMERG
Underground Notification
Ticket : 02165-098-001 Date: 02/16/05 Time: 06:59 Revision: 000

State: GA  County: GWINNETT     Place: LITHONIA
Addr : From: 2883   To:        Name:    RAVENWOLFE                     WAY  SW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3348C8404C   3348B8404C   3348B8404B  
Work type: REPAIR WATER SERVICE LINE/REPLACE
Work date: 02/16/05 Time: 07:08 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 07:08 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 07:08 Duration : 1DY
Done for : BOB TURNER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: WATER IS ESCAPING
        : *** NEAR STREET ***   MOORINGS PKWY

Company : NORTHLAKE PLUMBING                        Type: CONT
Co addr : 5347 HWY 29
City    : LILBURN                        State   : GA Zip: 30047
Caller  : KEN BROWN                      Phone   : 770-921-2046                
Fax     :                                Alt. Ph.: 
Contact : CREW EN ROUTE/HAROLD           

Submitted date: 02/16/05 Time: 06:59 Oper: 098 Chan: WEB
Mbrs : AGL114 BSNE   COMCEN GP265  GWI90  GWI91  WAL70 
-------------------------------------------------------------------------------



AGL123 00019 GAUPC 02/16/05 07:10:24 02165-046-002-000 NORMAL
Underground Notification
Ticket : 02165-046-002 Date: 02/16/05 Time: 07:08 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SPRINGLAKE                     PL   NW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: SPRINGLAKE PL  APTS    LOC ALL R/O/W FROM BLDG 54 TO BLDG 56  AT TEMP
     : CABLE ON THE GROUND

Grids    : 3348A8424A   3348B8424A  
Work type: REPLACE MAIN LINE CATV
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : COMCAST
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** NEAR STREET ***   HOWELL MILL RD
        : *** WILL BORE DRIVEWAY

Company : HESS CONTRACTING                          Type: CONT
Co addr : 1314 HILLCREST WAY
City    : LAWRENCEVILLE                  State   : GA Zip: 30043
Caller  : BILL HESS                      Phone   : 770-963-3516                
Fax     :                                Alt. Ph.: 404-431-7745                

Submitted date: 02/16/05 Time: 07:08 Oper: 046 Chan: WEB
Mbrs : AGL123 ATL01  ATL02  BSCA   COMCEN GP105 
-------------------------------------------------------------------------------



AGL123 00020 GAUPC 02/16/05 07:10:24 02165-046-003-000 NORMAL
Underground Notification
Ticket : 02165-046-003 Date: 02/16/05 Time: 07:09 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SPRINGLAKE                     PL   NW
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: SPRINGLAKE PL  APTS    LOC ALL R/O/W FROM BLDG  76 TO BLDG 90  AT TEMP
     : CABLE ON THE GROUND

Grids    : 3348A8424A   3348B8424A  
Work type: REPLACE MAIN LINE CATV
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : COMCAST
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** NEAR STREET ***   HOWELL MILL RD
        : *** WILL BORE DRIVEWAY

Company : HESS CONTRACTING                          Type: CONT
Co addr : 1314 HILLCREST WAY
City    : LAWRENCEVILLE                  State   : GA Zip: 30043
Caller  : BILL HESS                      Phone   : 770-963-3516                
Fax     :                                Alt. Ph.: 404-431-7745                

Submitted date: 02/16/05 Time: 07:09 Oper: 046 Chan: WEB
Mbrs : AGL123 ATL01  ATL02  BSCA   COMCEN GP105 
-------------------------------------------------------------------------------



AGL102 00027 GAUPC 02/16/05 07:15:32 02165-046-004-000 NORMAL RESTAKE
Underground Notification
Ticket : 02165-046-004 Date: 02/16/05 Time: 07:12 Revision: 000
Old Tkt: 01315-020-011 Date: 02/03/05 Time: 00:10

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    JEFFERSON                      ST   NW
Cross: From:        To:        Name:    ASHBY                          ST   NW
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: AT THE INTERSECTION LOCATE  THE SW QUADRANT OF JEFFERSON ST AT
     : STRUCTURE# 9

Grids    : 3346C8425D   3346B8425D  
Work type: INSTL FOUNDATIONS FOR  POWER LINES
Work date: 12/20/04 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 3 WKS
Done for : GEORGIA POWER
Crew on Site: N  White-lined: Y  Railroad: N  Blasting: N  Boring: N

Remarks : AREA WILL BE MARKED WITH WHITE FLAGS AND SPRAY PAINT
        : RESTAKE OF: 12154-023-020
        : RESTAKE OF: 12304-017-035
        : RESTAKE OF: 01135-039-028
        : RESTAKE OF: 01315-020-011
        : *** LOOKUP BY MANUAL

Company : TRI STATE DRILLING                        Type: CONT
Co addr : PO BOX 252
City    : HAMMELL                        State   : MN Zip: 55340
Caller  : DOUG KLERSY                    Phone   : 763-553-1234                
Fax     :                                Alt. Ph.: 
Contact : DOUG KLERSY                    
Cellular: 612-817-6708                 

Submitted date: 02/16/05 Time: 07:12 Oper: 046 Chan: WEB
Mbrs : AGL102 AGLN01 ATL01  ATL02  BSCA   GP105  LEV3   MCI02  MESXX  NU105 
     : SPR99 
-------------------------------------------------------------------------------



AGL119 00024 GAUPC 02/16/05 07:15:31 02165-026-002-000 NORMAL
Underground Notification
Ticket : 02165-026-002 Date: 02/16/05 Time: 07:11 Revision: 000

State: GA  County: COBB         Place: ACWORTH
Addr : From: 1822   To:        Name:    BABBLING BROOK                      
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE BACK & BOTH SIDES OF PROPERTY

Grids    : 3404D8435A   3404D8436D   3404C8436D  
Work type: INSTALL FENCE
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : RESIDENT INGENITO
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   RAPIDS CIR

Company : AMERICAN LANDMARK FENCE                   Type: CONT
Co addr : 1501  CANTON RD
City    : MARIETTA                       State   : GA Zip: 30066
Caller  : RANDY GAMBLE                   Phone   : 770-795-9100                
Fax     :                                Alt. Ph.: 

Submitted date: 02/16/05 Time: 07:11 Oper: 026 Chan: WEB
Mbrs : AGL119 BSNW   COB01  COB02  COB70  COMNOR
-------------------------------------------------------------------------------



AGL114 00021 GAUPC 02/16/05 07:14:58 02165-098-002-000 EMERG
Underground Notification
Ticket : 02165-098-002 Date: 02/16/05 Time: 07:09 Revision: 000

State: GA  County: GWINNETT     Place: NORCROSS
Addr : From: 5186   To:        Name:    EDGEMOOR                       CIR  
Cross: From:        To:        Name:
Offset:
Subdivision: EDGEMOOR NORTH
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3356B8411C   3356A8411C   3356B8411B   3356A8411B   3356B8411D  
Grids    : 3356A8411D  
Work type: REPAIR SEWER SERVICE
Work date: 02/16/05 Time: 07:14 Hrs notc: 000 Priority: 1
Legal day: 02/16/05 Time: 07:14 Good thru: 02/16/05 Restake by: 02/16/05
RespondBy: 02/16/05 Time: 07:14 Duration : 1DY
Done for : GWINNETT COUNTY
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SEWAGE BACKING UP
        : *** NEAR STREET ***   EDGEMOOR DR
        : *** LOOKUP BY MANUAL

Company : GWINNETT COUNTY PUBLIC UTILITES           Type: MEMB
Co addr : 684 WINDER HWY
City    : LAWRENCEVILLE                  State   : GA Zip: 30045
Caller  : SHANDA  PERUGINI               Phone   : 678-376-7024                
Fax     :                                Alt. Ph.: 
Contact : EN ROUTE/DENNIS HOLT           

Submitted date: 02/16/05 Time: 07:09 Oper: 098 Chan: WEB
Mbrs : AGL114 BSNE   COMCEN GWI90  GWI91  JCK70  NOR50  NOR51  NOR52 
-------------------------------------------------------------------------------



AGL111 00025 GAUPC 02/16/05 07:15:31 02165-033-006-000 NORMAL
Underground Notification
Ticket : 02165-033-006 Date: 02/16/05 Time: 07:08 Revision: 000

State: GA  County: FORSYTH      Place: CUMMING
Addr : From: 4930   To:        Name:    MAGNOLIA CREEK                 DR   
Cross: From:        To:        Name:
Offset:
Subdivision: MAGNOLIA CREEK
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE LEFT SIDE OF PROPERTY OF LOT #7

Grids    : 3417B8408D   3417A8408D   3417B8408C   3417A8408C   3417B8408B  
Grids    : 3417A8408B  
Work type: INSTALL ELECTRICAL U/G SECONDARY
Work date: 02/21/05 Time: 07:00 Hrs notc: 119 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 2-4 HR
Done for : GEORGIA POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   HWY 369
        : *** LOOKUP BY MANUAL

Company : GEORGIA POWER COMPANY                     Type: MEMB
Co addr : 1675 WILLS RD
City    : ALPHARETTA                     State   : GA Zip: 30201
Caller  : JACK BENNETT                   Phone   : 770-740-7741                
Fax     :                                Alt. Ph.: 

Submitted date: 02/16/05 Time: 07:08 Oper: 033 Chan: WEB
Mbrs : AGL111 ALLFB4 BSEA   CAN01  CUM50  CUM51  FOR01  GP280  PRST01 SAW73 
-------------------------------------------------------------------------------



AGL119 00023 GAUPC 02/16/05 07:15:30 02165-026-001-000 NORMAL
Underground Notification
Ticket : 02165-026-001 Date: 02/16/05 Time: 06:57 Revision: 000

State: GA  County: COBB         Place: MARIETTA
Addr : From: 2055   To:        Name:    JOHN DODGEN                    WAY  
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE ENTIRE BACK & BOTH SIDES OF PROPERTY

Grids    : 3400D8427D   3359A8426A   3359A8427D  
Work type: INSTALL FENCE
Work date: 02/21/05 Time: 07:00 Hrs notc: 120 Priority: 3
Legal day: 02/21/05 Time: 07:00 Good thru: 03/09/05 Restake by: 03/04/05
RespondBy: 02/18/05 Time: 23:59 Duration : 1 DAY
Done for : RESIDENT MERKEL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   HOLLY MILL RUN

Company : AMERICAN LANDMARK FENCE                   Type: CONT
Co addr : 1501  CANTON RD
City    : MARIETTA                       State   : GA Zip: 30066
Caller  : RANDY GAMBLE                   Phone   : 770-795-9100                
Fax     :                                Alt. Ph.: 

Submitted date: 02/16/05 Time: 06:57 Oper: 026 Chan: WEB
Mbrs : AGL119 BSNW   COB01  COB02  COB70  COMNOR GP172 
-------------------------------------------------------------------------------


