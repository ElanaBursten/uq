
TESTDC     12    AROCS 12/30/08 08:47:00 081230-0174 EMERGENCY NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[081230-0174]   == E M E R G E N C Y ==
OLD TICKET NUM-[]  

MESSAGE TYPE---[EMERGENCY] LEAD TIME--[0]
PREPARED-------[12/30/08]  AT  [0847]  BY  [KMCCRAY]

CONTRACTOR--[MR ROOTER OF NORTHWEST ARKANSAS]   CALLER--[CHRISTINA SEYER]
ADDRESS-----[481 JANE MARIE STE C]
CITY--------[SPRINGDALE]   STATE--[AR]   ZIP--[72762]
CALL BACK---[SAME]   PHONE--[(479) 756-5898]
COMPANY FAX-[(479) 750-4889]
CONTACT-----[CHRISTINA SEYER]   PHONE--[(479) 756-5898]
EMAIL-------[]
CONTACT FAX-[(479) 750-4889]

WORK TO BEGIN--[12/30/08] AT [0900]      STATE--[AR]
COUNTY---[WASHINGTON]  PLACE--[SPRINGDALE]

ADDRESS--[3500]   STREET--[][LUVENE][AVE][]
NEAREST INTERSECTION--------[JEAN ST]
LATITUDE--[36.1674098968497]    LONGITUDE--[-94.1690499374906]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[EMERGENCY - CREW ON SITE NOW                                                  ]
[                                                                              ]
[RELOCATE WATER SCREN LINE                                                     ]
[                                                                              ]
[WORKING AT THE SE CORNER OF INT - FRONT AND LFT OF PPTY                       ]

WORK TYPE--[SEWER, REPLACE SERVICE LINE]  DONE FOR--[MR ROOTER OF NORTHWEST ARKANSAS]
EXTENT-----[2 DAYS]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]
DIRECTIONAL BORING--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  ATT01      AT&T ARKANSAS                AWG13      AWG - FAYETTEVILLE        
  COXSPR     COX COMMUNICATION - SPR...   OZARKELC   OZARKS ELECTRIC COOPERA...
  SPRGDALE   SPRINGDALE WATER UTILITIES   SWPC07     SWEPC0 - SPRINGDALE       




