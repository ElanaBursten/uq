
TESTDC     9     AROCS 12/30/08 08:20:00 081230-0097 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[081230-0097]  
OLD TICKET NUM-[081230-0095]  

MESSAGE TYPE---[CHANGE INFORMATION REQUEST] LEAD TIME--[2600]
PREPARED-------[12/30/08]  AT  [0820]  BY  [MMACE]

CONTRACTOR--[ROBBY PATTERSON PLUMBING, INC.]   CALLER--[JEAN CREWS]
ADDRESS-----[74 COUNTY ROAD 4021]
CITY--------[JONESBORO]   STATE--[AR]   ZIP--[72404]
CALL BACK---[(870) 926-9531]   PHONE--[(870) 972-1534]
COMPANY FAX-[(870) 933-1882]
CONTACT-----[JEAN CREWS]   PHONE--[(870) 926-8331]
EMAIL-------[RPATTERSONPLUMBING@RITTERMAIL.COM]
CONTACT FAX-[]

WORK TO BEGIN--[12/30/09] AT [0830]      STATE--[AR]
COUNTY---[LAWRENCE]  PLACE--[HOXIE]

ADDRESS--[429]   STREET--[][AFFINITY][ST][]
NEAREST INTERSECTION--------[SE 2ND ST]
LATITUDE--[36.0310443422201]    LONGITUDE--[-90.9862304573507]
SECONDARY LATITUDE--[36.0308491209146]    SECONDARY LONGITUDE--[-90.9811113208946]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[WORKING WITHIN APPX 975' E OR APPX 540' W OF INT - LOCATE REAR LFT SIDE AND   ]
[FRONT OF PPTY                                                                 ]
[                                                                              ]
[RENEWAL REQ - WORK NOT STARTED IN TEN WORKING DAYS -                          ]
[                                                                              ]
[CHANGE OF INFO - CALLER REQ - PLS MARK ASAP - CREW WILL BE ON SITE WHEN       ]
[LINES ARE MARKED                                                              ]

WORK TYPE--[SERV LINES - INSTALL WATER & SEWER]  DONE FOR--[RANDY MILES]
EXTENT-----[1 DAY]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]
DIRECTIONAL BORING--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  ATT01      AT&T ARKANSAS                ENT03      ENTERGY - WALNUT RIDGE    
  HOXIE      CITY OF HOXIE                REAJB075   CENTERPOINT ENERGY ARKL...




