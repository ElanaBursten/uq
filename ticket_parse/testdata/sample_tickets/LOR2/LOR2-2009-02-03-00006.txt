
Ticket No:  9019113               48 HOUR NOTICE 
Send To: FALCON10   Seq No:    3  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:40 AM    Op: ortyni 
Original Call Date:  2/03/09   Time: 10:30 AM    Op: ortyni 
Work to Begin Date:  2/05/09   Time: 10:45 AM 

State: OR            County: COOS                    Place: NORTH BEND 
Address:  68716      Street: HAUSER ROAD 
Nearest Intersecting Street: HWY 101 

Twp: 24S   Rng: 13W   Sect-Qtr: 14 
Twp: 24S   Rng: 13W   Sect-Qtr: 11-SW,10-SE,15-NE,14-NW 
Ex. Coord NW Lat: 43.4975529Lon: -124.2253166SE Lat: 43.4909334Lon: -124.2179469 

Type of Work: INSTALL WATER SRVC 
Location of Work: ADD IS APX 1/8MI S OF ABV INTER ON W SIDE OF ROAD. MARK
: AREA MARKED IN WHITE IN FRONT OF ABV ADD ON BOTH SIDES OF HAUSER ROAD.
: +CALLER STATES HAUSER ROAD RUNS PARALLEL TO HWY 101+
: +HAUSER ROAD IS SHOWN ON CENTER COMPUTER MAP AND GOOGLE AS HAUSER DR+ 

Remarks:  
: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO INCL SECTION 14-B 

Company     : COOS BAY/N BEND WATER BOARD      Best Time:   
Contact Name: BRIAN WOODS                      Phone: (541)267-3128 
Email Address:   
Alt. Contact: RON HOFFINE                      Phone: (541)267-3128 
Contact Fax :  
Work Being Done For: COOS BAY/N BEND WATER BOARD 
Additional Members:  
CBNBW01    CLPUD03    GTC09      ODOTD07 

