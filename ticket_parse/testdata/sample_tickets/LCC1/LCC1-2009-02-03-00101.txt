

Ticket No:  9022717               2 FULL BUSINESS DAYS 
Send To: CC7711     Seq No:   60  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:32 AM    Op: orandr 
Original Call Date:  2/03/09   Time: 10:30 AM    Op: orandr 
Work to Begin Date:  2/06/09   Time: 12:00 AM 

State: WA            County: PIERCE                  Place: MCCHORD AFB 
Address:             Street: ALDER 
Nearest Intersecting Street: LINCOLN BLVD 

Twp: 19N   Rng: 2E    Sect-Qtr: 15-SE-NE,14-SW-NW 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 47.1352741Lon: -122.5249288SE Lat: 47.1338507Lon: -122.5234854 

Type of Work: INSTALL SIGN 
Location of Work: SITE IS ON SW CORNER OF ABV INTER.  MARK AREA MARKED IN
: WHITE AT THIS LOCATION. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : E Q R                            Best Time:   
Contact Name: ED MITCHELL                      Phone: (253)377-9304 
Email Address:         
Alt. Contact: MAIN OFFICE                      Phone: (253)966-9675 
Contact Fax :  
Work Being Done For: EQR 
Additional Members:  
LKWOOD01   PSEELC46   PSEGAS40   QLNWA24

