

Ticket No:  9022694               2 FULL BUSINESS DAYS 
Send To: CC7711     Seq No:   59  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:32 AM    Op: orlynn 
Original Call Date:  2/03/09   Time: 10:26 AM    Op: orlynn 
Work to Begin Date:  2/06/09   Time: 12:00 AM 

State: WA            County: MASON                   Place: SHELTON 
Address:   1109      Street: WYANDOTTE AVE 
Nearest Intersecting Street: 7TH 

Twp: 20N   Rng: 4W    Sect-Qtr: 24-SE,25-NE 
Twp: 20N   Rng: 3W    Sect-Qtr: 19-SW,30-NW 
Ex. Coord NW Lat: 47.2048032Lon: -123.1180628SE Lat: 47.2005434Lon: -123.1107857 

Type of Work: BURY ELEC CONDUIT 
Location of Work: ADD IS APX 4 BLKS W OF INTER ON S SIDE OF WYANDOTTE. MARK
: FROM POWER PANEL IN GARAGE APX 40 FT TO SHED AT BACK CORNER OF HOUSE
: (LOCATED APX 50 FT BACK FROM WYANDOTTE AVE). AREA WILL BE MARKED IN ORANGE
: PAINT. 

Remarks:  
:  

Company     :                                  Best Time:   
Contact Name: DAVID DUDEK                      Phone: (360)701-1466 
Email Address:         
Alt. Contact: HOME                             Phone: (360)432-9660 
Contact Fax :  
Work Being Done For: DAVID DUDEK 
Additional Members:  
CNG05      MPUD301    QLNWA22    SHELT01

