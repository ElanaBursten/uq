

TST03  00038 NCOCa 05/07/09 10:39:47 A091270841-00A SHRT NEW GRID LR

North Carolina One Call

Ticket : A091270841 Date: 05/07/09 Time: 10:34 Oper: SFP Chan:999
Old Tkt: A091270841 Date: 05/07/09 Time: 10:39 Oper: SFP Rev :00A

State: NC Cnty: GRANVILLE Place: OXFORD In/Out: B
Subdivision:

Address : 117
Street  : SUMMIT AVE   Intersection: N
Cross 1 : RALEIGH ST
Location: LOCATE BY 05/12/09 12:01 AM
DISTANCE FROM CROSS STREET IS:  1/4
LOCATE   ENTIRE FRONT OF PROPERTY
:
Grids   : 3618D7834B   3618D7834C

Work type:REPLACE WATER LEAK
Work date:05/08/09  Time: 08:00  Hours notice: 7/7  Priority: SHRT
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 1 DAY  Done for: WATKINS PLUMBING

Company : WATKINS PLUMBING AND PIPING  Type: OTHR
Co addr : 4541 WATKINS RD
City    : OXFORD State: NC Zip: 27565
Caller  : JACK WATKINS Phone: 919-691-0747
Contact : SAME Phone:
BestTime:

Submitted date: 05/07/09 Time: 10:39
Members: CLS01  CPL10* CTT10* CVI04* PSG12  TST03
