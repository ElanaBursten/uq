
BGAWR  00028 GAUPC 04/16/09 00:46:21 04139-250-026-001 NORMAL LATE  
Underground Notification             
Notice : 04139-250-026 Date: 04/16/09  Time: 00:34  Revision: 001 

State : GA County: FLOYD         Place: ROME                                    
Addr  : From: 63     To:        Name:    HILLINDALE                     DR   SE 
Near  : Name:    CHULIO                         RD  

Subdivision:                                         
Locate: LOCATE FROM THE SERVICE POLE TO THE RD AND FROM POLE TO FENCE   *** PLE
      :  ASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, PL
      :  EASE RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-46
      :  1-7271. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA THE SAME
      :  METHODS. ***                                                           

Grids       : 3412A8506B 3412A8506C 3412B8506B 3412C8506B 3412D8506B 
Grids       : 
Work type   : INSTL ELEC AND TELE SVC                                             

Start date: 04/16/09 Time: 07:00 Hrs notc : 000
Legal day : 04/16/09 Time: 07:00 Good thru: 05/04/09 Restake by: 04/29/09
RespondBy : 04/15/09 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : METRO PCS                               
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : *** BGAWR,ROM50,ROM51 IS LATE RESPONDING.  PLEASE RESPOND ASAP.  THI
        : S IS A CELL TOWER SITE                                              

Company : HODGKINS CONSTRUCTION                     Type: CONT                
Co addr : 825 SCHOOL HOUSE RD                      
City    : CALHOUN                         State   : GA Zip: 30701              
Caller  : KEN BENNETT                     Phone   :  770-548-4520              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 04/16/09  Time: 00:34  Oper: 206
Mbrs : BGAWR CTV50 FLO50 GP600 PPL02 ROM50 ROM51 
-------------------------------------------------------------------------------



