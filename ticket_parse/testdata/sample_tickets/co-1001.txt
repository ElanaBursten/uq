ATDC00 00132 UNCC 07/30/02 09:35 AM 0459501-000 NORM UPDT GRID LREQ

Ticket Nbr: 0459501 Update of: 0420078
Original Call Date: 07/11/02  Time: 04:28 PM  Op: HDM
Contr Excav Date  : 08/02/02  Time: 07:00 AM  Extended job: N
Locate By Date    : 08/01/02  Time: 11:59 PM  Hrs Notice: 070  Meet: N
State: CO     County: DENVER           City: DENVER
Addr:   2450  Street: ALAMEDA AVE E
Grids: 04S068W12SW : 04S068W13NW              :               Legal: C
Type of Work: FENCE                                      Exp.: N  Boring: N
Location: PLEASE LOC ENTIRE LOT #8 PAINT FLAG LEAVE SKETCH ACCESS OPEN-UPDATE
        : DUE TO MARKS NEED REFRESHED- REAR OF LOT ONLY
Company : SPLIT RAIL FENCE
Caller  : HOLLY MEYER                Phone: (303)791-1997
Alt Cont: LOGAN GOOD                 Phone:
Fax: (303)791-1986  Email: SASST@SPLITRAILFENCECO.COM
Done for: HOMEOWNER
Remarks:

Members :ATDC00 :ATND03 :ICGTL1 :LVL311 :PCND00 :PSND03 :TACO01 :UQND00 :USND03

