

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ10
AUDIT FOR 7/6/2009

FOR CODE SCEDZ10

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060241                DELIVERED
      0002  0907060264                DELIVERED
      0003  0907060295                DELIVERED
      0004  0907060312                DELIVERED
      0005  0907060370                DELIVERED
      0006  0907060393                DELIVERED
      0007  0907060422                DELIVERED
      0008  0907060423                DELIVERED
      0009  0907060544                DELIVERED
      0010  0907060558                DELIVERED
      0011  0907060622                DELIVERED
      0012  0907061093                DELIVERED
FOR CODE SCEDZ10


NORMAL    : 12
RESEND    : 0
EMERGENCY : 0
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ10: 12




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

