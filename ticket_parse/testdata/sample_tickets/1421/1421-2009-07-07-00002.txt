

PUPS AUDIT
SCEG ELECTRIC 
SCEKZ82
AUDIT FOR 7/6/2009

FOR CODE SCEKZ82

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060027                DELIVERED
      0002  0907060046                DELIVERED
      0003  0907060050                DELIVERED
      0004  0907060058                DELIVERED
      0005  0907060059                DELIVERED
      0006  0907060062                DELIVERED
 U    0007  0907060084                DELIVERED
 U    0008  0907060085                DELIVERED
      0009  0907060224                DELIVERED
      0010  0907060351                DELIVERED
 N    0011  0907060362                DELIVERED
      0012  0907060364                DELIVERED
      0013  0907060385                DELIVERED
      0014  0907060394                DELIVERED
      0015  0907060450                DELIVERED
      0016  0907060510                DELIVERED
      0017  0907060690                DELIVERED
      0018  0907060716                DELIVERED
      0019  0907060754                DELIVERED
      0020  0907060773                DELIVERED
      0021  0907060785                DELIVERED
      0022  0907060794                DELIVERED
      0023  0907060800                DELIVERED
      0024  0907060807                DELIVERED
      0025  0907060812                DELIVERED
      0026  0907060825                DELIVERED
      0027  0907060832                DELIVERED
      0028  0907060842                DELIVERED
 U    0029  0907060854                DELIVERED
 U    0030  0907060860                DELIVERED
      0031  0907060861                DELIVERED
 U    0032  0907060862                DELIVERED
      0033  0907060876                DELIVERED
      0034  0907060887                DELIVERED
      0035  0907060894                DELIVERED
      0036  0907060898                DELIVERED
 C    0037  0907060906                DELIVERED
      0038  0907060917                DELIVERED
 C    0039  0907060921                DELIVERED
      0040  0907060928                DELIVERED
      0041  0907060937                DELIVERED
 C    0042  0907060948                DELIVERED
      0043  0907060959                DELIVERED
 C    0044  0907060980                DELIVERED
      0045  0907060999                DELIVERED
 C    0046  0907061007                DELIVERED
      0047  0907061020                DELIVERED
      0048  0907061030                DELIVERED
      0049  0907061034                DELIVERED
      0050  0907061041                DELIVERED
      0051  0907061050                DELIVERED
      0052  0907061059                DELIVERED
      0053  0907061083                DELIVERED
      0054  0907061107                DELIVERED
      0055  0907061113                DELIVERED
      0056  0907061136                DELIVERED
!     0057  0907061183                DELIVERED
      0058  0907061236                DELIVERED
      0059  0907061240                DELIVERED
      0060  0907061246                DELIVERED
      0061  0907061249                DELIVERED
      0062  0907061295                DELIVERED
      0063  0907061300                DELIVERED
      0064  0907061306                DELIVERED
      0065  0907061325                DELIVERED
      0066  0907061329                DELIVERED
      0067  0907061381                DELIVERED
      0068  0907061401                DELIVERED
      0069  0907061427                DELIVERED
      0070  0907061455                DELIVERED
      0071  0907061458                DELIVERED
      0072  0907061463                DELIVERED
      0073  0907061482                DELIVERED
      0074  0907061486                DELIVERED
      0075  0907061488                DELIVERED
      0076  0907061489                DELIVERED
      0077  0907061498                DELIVERED
      0078  0907061501                DELIVERED
      0079  0907061503                DELIVERED
      0080  0907061513                DELIVERED
      0081  0907061526                DELIVERED
      0082  0907061535                DELIVERED
      0083  0907061602                DELIVERED
      0084  0907061609                DELIVERED
      0085  0907061611                DELIVERED
      0086  0907061613                DELIVERED
      0087  0907061696                DELIVERED
      0088  0907061701                DELIVERED
      0089  0907061762                DELIVERED
      0090  0907061785                DELIVERED
      0091  0907062075                DELIVERED
      0092  0907062091                DELIVERED
      0093  0907062096                DELIVERED
      0094  0907062101                DELIVERED
      0095  0907062347                DELIVERED
      0096  0907062349                DELIVERED
      0097  0907062352                DELIVERED
      0098  0907062459                DELIVERED
      0099  0907062489                DELIVERED
      0100  0907062591                DELIVERED
      0101  0907062597                DELIVERED
      0102  0907062638                DELIVERED
      0103  0907062661                DELIVERED
      0104  0907062667                DELIVERED
      0105  0907062683                DELIVERED
      0106  0907062737                DELIVERED
FOR CODE SCEKZ82


NORMAL    : 94
RESEND    : 0
EMERGENCY : 1
CANCEL    : 5
MEET      : 0
NO SHOW   : 1
SURVEY    : 0
UPDATE    : 5
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEKZ82: 106




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

