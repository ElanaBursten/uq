

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ60
AUDIT FOR 7/6/2009

FOR CODE SCEDZ60

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060119                DELIVERED
 U    0002  0907060187                DELIVERED
      0003  0907060190                DELIVERED
      0004  0907060207                DELIVERED
      0005  0907060256                DELIVERED
      0006  0907061147                DELIVERED
      0007  0907061150                DELIVERED
      0008  0907061155                DELIVERED
      0009  0907061184                DELIVERED
 U    0010  0907061619                DELIVERED
 U    0011  0907061742                DELIVERED
      0012  0907062043                DELIVERED
      0013  0907062236                DELIVERED
      0014  0907062465                DELIVERED
      0015  0907062487                DELIVERED
      0016  0907062497                DELIVERED
      0017  0907062500                DELIVERED
      0018  0907062505                DELIVERED
      0019  0907062517                DELIVERED
      0020  0907062529                DELIVERED
      0021  0907062532                DELIVERED
      0022  0907062534                DELIVERED
      0023  0907062536                DELIVERED
FOR CODE SCEDZ60


NORMAL    : 20
RESEND    : 0
EMERGENCY : 0
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 3
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ60: 23




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

