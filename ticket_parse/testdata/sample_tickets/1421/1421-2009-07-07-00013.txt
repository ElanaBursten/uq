

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ42
AUDIT FOR 7/6/2009

FOR CODE SCEDZ42

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907062506                DELIVERED
FOR CODE SCEDZ42


NORMAL    : 1
RESEND    : 0
EMERGENCY : 0
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ42: 1




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

