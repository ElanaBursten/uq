

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ05
AUDIT FOR 7/6/2009

FOR CODE SCEDZ05

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060098                DELIVERED
      0002  0907060319                DELIVERED
!     0003  0907060321                DELIVERED
      0004  0907060337                DELIVERED
!     0005  0907060374                DELIVERED
      0006  0907060433                DELIVERED
!     0007  0907060447                DELIVERED
      0008  0907060494                DELIVERED
      0009  0907060495                DELIVERED
      0010  0907060639                DELIVERED
*     0011  0907060662                DELIVERED
!     0012  0907060669                DELIVERED
!     0013  0907060809                DELIVERED
      0014  0907060946                DELIVERED
!     0015  0907060976                DELIVERED
      0016  0907061042                DELIVERED
      0017  0907061065                DELIVERED
      0018  0907061145                DELIVERED
      0019  0907061151                DELIVERED
      0020  0907061158                DELIVERED
      0021  0907061346                DELIVERED
      0022  0907061436                DELIVERED
      0023  0907061581                DELIVERED
      0024  0907061641                DELIVERED
      0025  0907061743                DELIVERED
      0026  0907061752                DELIVERED
      0027  0907061757                DELIVERED
      0028  0907061761                DELIVERED
      0029  0907061766                DELIVERED
      0030  0907061771                DELIVERED
      0031  0907061776                DELIVERED
      0032  0907061782                DELIVERED
      0033  0907061790                DELIVERED
      0034  0907061795                DELIVERED
      0035  0907061893                DELIVERED
      0036  0907061914                DELIVERED
      0037  0907062086                DELIVERED
      0038  0907062213                DELIVERED
!     0039  0907062220                DELIVERED
      0040  0907062254                DELIVERED
FOR CODE SCEDZ05


NORMAL    : 32
RESEND    : 1
EMERGENCY : 7
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ05: 40




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

