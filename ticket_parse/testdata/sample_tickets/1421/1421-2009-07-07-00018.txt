

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ65
AUDIT FOR 7/6/2009

FOR CODE SCEDZ65

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060218                DELIVERED
      0002  0907060221                DELIVERED
      0003  0907060236                DELIVERED
 U    0004  0907060268                DELIVERED
 U    0005  0907060269                DELIVERED
 U    0006  0907060277                DELIVERED
      0007  0907060349                DELIVERED
      0008  0907060617                DELIVERED
 R    0009  0907060650                DELIVERED
      0010  0907060732                DELIVERED
      0011  0907060743                DELIVERED
      0012  0907061407                DELIVERED
      0013  0907061493                DELIVERED
 U    0014  0907061620                DELIVERED
      0015  0907061624                DELIVERED
      0016  0907061732                DELIVERED
 U    0017  0907061737                DELIVERED
 U    0018  0907061751                DELIVERED
!     0019  0907061808                DELIVERED
      0020  0907061941                DELIVERED
      0021  0907062377                DELIVERED
      0022  0907062435                DELIVERED
      0023  0907062469                DELIVERED
      0024  0907062475                DELIVERED
      0025  0907062525                DELIVERED
 R    0026  0907062744                DELIVERED
 R    0027  0907062745                DELIVERED
FOR CODE SCEDZ65


NORMAL    : 17
RESEND    : 0
EMERGENCY : 1
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 6
REMARK    : 3
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ65: 27




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

