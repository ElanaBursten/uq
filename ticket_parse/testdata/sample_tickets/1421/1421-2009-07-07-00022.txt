

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ08
AUDIT FOR 7/6/2009

FOR CODE SCEDZ08

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060049                DELIVERED
      0002  0907060078                DELIVERED
*     0003  0907060122                DELIVERED
      0004  0907060215                DELIVERED
      0005  0907060217                DELIVERED
      0006  0907060237                DELIVERED
      0007  0907060496                DELIVERED
      0008  0907060658                DELIVERED
      0009  0907060665                DELIVERED
      0010  0907060672                DELIVERED
      0011  0907060687                DELIVERED
      0012  0907060689                DELIVERED
      0013  0907060779                DELIVERED
 C    0014  0907060787                DELIVERED
      0015  0907060989                DELIVERED
      0016  0907061033                DELIVERED
      0017  0907061097                DELIVERED
      0018  0907061273                DELIVERED
      0019  0907061443                DELIVERED
      0020  0907061531                DELIVERED
      0021  0907061718                DELIVERED
      0022  0907061759                DELIVERED
      0023  0907061772                DELIVERED
      0024  0907062133                DELIVERED
 C    0025  0907062141                DELIVERED
      0026  0907062607                DELIVERED
*     0027  0907062612                DELIVERED
FOR CODE SCEDZ08


NORMAL    : 23
RESEND    : 2
EMERGENCY : 0
CANCEL    : 2
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ08: 27




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

