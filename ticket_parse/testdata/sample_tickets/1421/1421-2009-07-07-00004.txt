

PUPS AUDIT
SCEG ELECTRIC 
SCETZ03
AUDIT FOR 7/6/2009

FOR CODE SCETZ03

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060157                DELIVERED
!     0002  0907060998                DELIVERED
*     0003  0907061013                DELIVERED
FOR CODE SCETZ03


NORMAL    : 1
RESEND    : 1
EMERGENCY : 1
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCETZ03: 3




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

