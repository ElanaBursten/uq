

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326221 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:08     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:04     Op: latorran.455 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: GRANT 
Place: POLLOCK TOWN 
Address: 800         Street: TARPLEY RD 
Nearest Intersecting Street: STATE HWY 8 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 1 7/10
: MI NW OF INTERSECTION FOLLOWING DIRECTION OF RD. FOR MORE INFO CALL DELRIE @
: (318)481-1760. 

Remarks: LA0902507                SJB             7/31/12 
 

Work to Begin Date:  8/04/09            Time:  9:15 AM 
Mark By       Date:  8/04/09            Time:  9:15 AM 

Ex. Coord: NW Lat: 31.5754207Lon: -92.3657127SE Lat: 31.5741638Lon:  -92.3609687 

Additional Members:  POLWTR01 

Link To Map for AL01: http://la.itic.occinc.com/LT24-2N9-29Z-A9P 
Link To Map for CLEC02: http://la.itic.occinc.com/AT24-3NU-29Z-A9P 

Send To:  AL01      Seq No:  27 
Send To:  CLEC02    Seq No:  10 
