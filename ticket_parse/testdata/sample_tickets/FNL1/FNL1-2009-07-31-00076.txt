

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326095 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:37     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:33     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: WINN 
Place: DODSON VILLAGE 
Address: 830         Street: AUNT MARIES RD 
Nearest Intersecting Street: HATTEN RD 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 250 YDS
: W OF INTERSECTION. FOR MORE INFO CALL CARTER @ (318)481-5329. 

Remarks: LA0902497                SJB             7/31/2 
 

Work to Begin Date:  8/04/09            Time:  8:45 AM 
Mark By       Date:  8/04/09            Time:  8:45 AM 

Ex. Coord: NW Lat: 32.0603342Lon: -92.5674577SE Lat: 32.0576958Lon:  -92.5619239 

Additional Members:  ENTGY01 

Link To Map for AL01: http://la.itic.occinc.com/WNZ9-922-T2N-9A7 

Send To:  AL01      Seq No:  14 
