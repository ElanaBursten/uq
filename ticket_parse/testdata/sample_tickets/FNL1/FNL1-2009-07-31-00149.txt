

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326390 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:41     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:40     Op: lacandac.1054 

Company     : DEVINEY CONSTRUCTION COMPANY 
Contact Name: KEN SMITH                       Contact Phone: (318)361-5146 
Alt. Contact: KEN SMITH                       Alt. Phone   : (318)381-2303 

Type of Work : PLACE TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: UNION 
Place: FARMERVILLE TOWN 
Address:             Street: WEST PORT UNION RD. 
Nearest Intersecting Street: HAILE MALONE RD. 

Location:       BEGIN AT THE SOUTHSIDE OF THE INTERSECTION OF WEST PORT UNION
: RD.& HAILE MALONE RD.MARK THE SOUTHSIDE(FRONT OF PROPERTY)OF WEST PORT UNION
: RD.MARK 50 FT.TO THE WEST AND 50 FT TO THE EAST. 

Remarks:  
 

Work to Begin Date:  8/04/09            Time:  9:45 AM 
Mark By       Date:  8/04/09            Time:  9:45 AM 

Ex. Coord: NW Lat: 32.7337366Lon: -92.2737759SE Lat: 32.7327331Lon:  -92.2726905 

Additional Members:  

Link To Map for ARK02: http://la.itic.occinc.com/9ZZN-ATA-9K2-929 
Link To Map for MO01: http://la.itic.occinc.com/6ZGN-ATM-9K2-929 

Send To:  ARK02     Seq No:  16 
Send To:  MO01      Seq No:  32 
