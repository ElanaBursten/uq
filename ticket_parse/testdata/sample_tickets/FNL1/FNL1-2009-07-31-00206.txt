

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326782 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 11:03     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 11:01     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: CAROLYN NORMAN                  Contact Phone: (337)261-4595 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: BIENVILLE 
Place: BIENVILLE VILLAGE 
Address: 717         Street: JOHNSON ST 
Nearest Intersecting Street: STATE HWY 507 (MAIN ST) 

Location:       200 YDS S OF INTER--MARK ENTIRE PROPERTY--ALSO MARK FRONT OF
: PROPERTY FROM ADDRESS TO PED E7 ON HWY 507--FOLLOW TEMPORARY WIRE ON
: GROUND--TRLR AT END OF ST. 

Remarks: JOB#  LL0902317  CBN   07/31/30 
 

Work to Begin Date:  8/04/09            Time: 11:15 AM 
Mark By       Date:  8/04/09            Time: 11:15 AM 

Ex. Coord: NW Lat: 32.3554168Lon: -92.9697632SE Lat: 32.3545772Lon:  -92.9683960 

Additional Members:  GSBIS01, TLN02 

Link To Map for MO01: http://la.itic.occinc.com/CZGN-ATM-9B2-92X 

Send To:  MO01      Seq No:  51 
