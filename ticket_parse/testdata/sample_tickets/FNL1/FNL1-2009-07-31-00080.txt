

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326117 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:41     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:40     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: AVOYELLES 
Place: CENTER POINT 
Address: 346         Street: LOUIE HARMSON RD 
Nearest Intersecting Street: W BRYANT RD 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 6/10 MI
: ENE OF INTERSECTION FOLLOWING DIRECTION OF RD. FOR MORE INFO CALL CROSS @
: (318)613-4692. 

Remarks: LA0902499                SJB             7/31/4 
 

Work to Begin Date:  8/04/09            Time:  8:45 AM 
Mark By       Date:  8/04/09            Time:  8:45 AM 

Ex. Coord: NW Lat: 31.2782249Lon: -92.2059785SE Lat: 31.2766316Lon:  -92.2017589 

Additional Members:  APW1W01, CLEC03, ETM01 

Link To Map for AL01: http://la.itic.occinc.com/XT23-2N9-29Z-A9U 

Send To:  AL01      Seq No:  17 
