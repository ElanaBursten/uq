

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326181 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:51     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:50     Op: lacandac.1054 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: SABINE 
Place: FLORIEN VILLAGE 
Address: 1284        Street: S NOLAN TRC (US HWY 171) 
Nearest Intersecting Street: CHURCH ST 

Location:       DAVID COOK TRUCKS & EQUIPMENT--POSSIBLE ST BORE  MARK ENTIRE
: PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 400 YDS NW OF INTERSECTION. FOUND
: ON YAHOO MAPS. FOR MORE INFO CALL BEHAN @ (318)613-8439. 

Remarks: LA0902504                SJB             7/31/9 
 

Work to Begin Date:  8/04/09            Time:  9:00 AM 
Mark By       Date:  8/04/09            Time:  9:00 AM 

Ex. Coord: NW Lat: 31.4276671Lon: -93.4510200SE Lat: 31.4258226Lon:  -93.4495176 

Additional Members:  LIG04, TEXAR01, TLN02 

Link To Map for SH01: http://la.itic.occinc.com/6T2U-5NV-29Z-A9U 

Send To:  SH01      Seq No:  35 
