

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326432 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:45     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:44     Op: lajonath.1060 

Company     : DEVINEY CONSTRUCTION COMPANY 
Contact Name: KEN SMITH                       Contact Phone: (318)361-5146 
Alt. Contact: KEN SMITH                       Alt. Phone   : (318)381-2303 

Type of Work : PLACE TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: UNION 
Place: FARMERVILLE TOWN 
Address:             Street: MASHAW DR 
Nearest Intersecting Street: W PORT UNION RD 

Location:       LOCATE AREA IS ON THE SOUTHEAST SIDE OF THE INTERSECTION OF
: MASHAW DR.&WEST PORT UNION RD.MARK THE SOUTHSIDE(FRONT OF PROPERTY)OF WEST
: PORT UNION RD.FOR 150 FT.TO THE EAST. 

Remarks:  
 

Work to Begin Date:  8/04/09            Time:  9:45 AM 
Mark By       Date:  8/04/09            Time:  9:45 AM 

Ex. Coord: NW Lat: 32.7258058Lon: -92.2507725SE Lat: 32.7247780Lon:  -92.2490399 

Additional Members:  KMEP03, RELENE01 

Link To Map for ARK02: http://la.itic.occinc.com/D9A2-ZA9-Z2T-NP2 
Link To Map for MO01: http://la.itic.occinc.com/29A2-ZM9-G2T-NP2 

Send To:  ARK02     Seq No:  17 
Send To:  MO01      Seq No:  34 
