

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326412 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:42     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:42     Op: lajonath.1060 

Company     : DEVINEY CONSTRUCTION COMPANY 
Contact Name: KEN SMITH                       Contact Phone: (318)361-5146 
Alt. Contact: KEN SMITH                       Alt. Phone   : (318)381-2303 

Type of Work : PLACE TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: UNION 
Place: FARMERVILLE TOWN 
Address:             Street: W PORT UNION RD 
Nearest Intersecting Street: DUKE NOLAN RD 

Location:       LOCATE AREA IS 3200 FT. WEST OF THE INTERSECTION OF WEST PORT
: UNION RD.&DUKE NOLAN RD.MARK THE NORTH SIDE(FRONT OF PROPERTY)OF WEST PORT
: UNION RD.TO THE WEST FOR 150 FT. 

Remarks:  
 

Work to Begin Date:  8/04/09            Time:  9:45 AM 
Mark By       Date:  8/04/09            Time:  9:45 AM 

Ex. Coord: NW Lat: 32.7305999Lon: -92.2678577SE Lat: 32.7267166Lon:  -92.2633877 

Additional Members:  RELENE01 

Link To Map for MO01: http://la.itic.occinc.com/W9A2-ZM9-G2T-N66 

Send To:  MO01      Seq No:  33 
