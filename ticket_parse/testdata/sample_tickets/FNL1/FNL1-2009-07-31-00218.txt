

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326794 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 11:11     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 11:09     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: CAROLYN NORMAN                  Contact Phone: (337)261-4595 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: CALDWELL 
Place: COLUMBIA TOWN 
Address: 7623        Street: US HWY 165 
Nearest Intersecting Street: STATE HWY 4 

Location:       25 YDS NE OF INTER--CALDWELL INS ANGENCY--MARK ENTIRE
: PROPERTY--ALSO MARK FRONT OF PROPERTY FROM ADDRESS TO PED AT 7629 HWY
: 165--FOLLOW TEMPORARY WIRE ON GROUND. 

Remarks: JOB#  LL0902320  CBN   07/31/33 
 

Work to Begin Date:  8/04/09            Time: 11:15 AM 
Mark By       Date:  8/04/09            Time: 11:15 AM 

Ex. Coord: NW Lat: 32.1165513Lon: -92.0718481SE Lat: 32.1145113Lon:  -92.0701244 

Additional Members:  ECOLWT01, ENTGY01, TLN02 

Link To Map for MO01: http://la.itic.occinc.com/KZGN-ATM-9C2-92D 

Send To:  MO01      Seq No:  55 
