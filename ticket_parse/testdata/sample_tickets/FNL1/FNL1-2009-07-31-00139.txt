

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326243 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:33     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:31     Op: lajonath.1060 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: RAPIDES 
Place: BOYCE TOWN 
Address: 8815        Street: STATE HWY 1200 
Nearest Intersecting Street: GREENGATE LN 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 90 YDS
: NE OF INTERSECTION FOLLOWING DIRECTION OF RD. FOR MORE INFO CALL TEAL @
: (318)542-6201. 

Remarks: LA0902509                SJB             7/31/14 
 

Work to Begin Date:  8/04/09            Time:  9:45 AM 
Mark By       Date:  8/04/09            Time:  9:45 AM 

Ex. Coord: NW Lat: 31.3037119Lon: -92.7255426SE Lat: 31.2997403Lon:  -92.7232389 

Additional Members:  GARWTR01 

Link To Map for AL01: http://la.itic.occinc.com/EGZ9-922-T2N-9Z3 
Link To Map for CLEC02: http://la.itic.occinc.com/GGZ9-U22-T3N-9Z3 

Send To:  AL01      Seq No:  33 
Send To:  CLEC02    Seq No:  14 
