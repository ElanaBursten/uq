

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326089 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:31     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:31     Op: latorran.455 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: NATCHITOCHES 
Place: NATCHITOCHES CITY 
Address: 217         Street: MELISSA PL 
Nearest Intersecting Street: BEVERLY RISE PL 

Location:       HIDDEN HILLS TRLR PK  MARK ENTIRE REAR OF PROPERTY. FOR MORE
: INFO CALL KAISER @ (318)228-5328. 

Remarks: LA0902496                SJB             7/31/1 
 

Work to Begin Date:  8/04/09            Time:  8:45 AM 
Mark By       Date:  8/04/09            Time:  8:45 AM 

Ex. Coord: NW Lat: 31.7872785Lon: -93.0904893SE Lat: 31.7843439Lon:  -93.0885536 

Additional Members:  CNATCH01, CPTEL01, NATCAB01, TLN02, VEM01 

Link To Map for AL01: http://la.itic.occinc.com/RT2N-2N9-29Z-A95 

Send To:  AL01      Seq No:  11 
