NOTICE OF INTENT TO EXCAVATE
48HR-48 HOURS NOTICE                    NEW TICKET
Ticket No:   276217
Update Of:        0                     Updated By:         0
Transmit      Date:  9/19/02            Time: 12:51     Op: heathe.0
Original Call Date:  9/19/02            Time: 11:36     Op: heathe.0

Company     : BELL SOUTH
Contact Name: THERESA C DEAN                  Contact Phone: (337)
261-4613
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337)
261-4611

Type of Work : BURY TELEPHONE SERVICE WIRE
Work Done For: BELLSOUTH

State: LA       Parish: WEBSTER
Place: DIXIE INN VILLAGE
Address: 207         Street: STANLEY ST
Nearest Intersecting Street: MAIN

Location:       MARK ENTIRE PROPERTY
Remarks: TCD   LM0204783   09/19/35
CCB @ 12:28 9/19 HLB >> STANLEY IS THE SAME AS HWY 80

Work to Begin Date:  9/23/02            Time: 11:45 AM
Mark By       Date:  9/23/02            Time: 11:45 AM

Additional Members:  ARK02, ATT01, DIXINN01, DOTD04, ENTGY04

Send To:  SH01      Seq No: 104

Ex. Coord: NW Lat: 32 35 32  Lon: -93 20 33  SE Lat: 32 35 24  Lon:  -93
20 31

