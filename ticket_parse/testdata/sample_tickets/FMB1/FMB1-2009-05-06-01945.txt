
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 9209446 
Transmit      Date: 05/06/09      Time: 01:30 PM           Op: webusr 
Original Call Date: 05/06/09      Time: 01:22 PM           Op: webusr 
Work to Begin Date: 05/08/09      Time: 01:30 PM 

Place: ELLICOTT CITY 
Address: 9090        Street: TIBER RIDGE CT 
Nearest Intersecting Street: ST JOHNS LN		 

Type of Work: VERIZON/FIOS/MK/SAIND 
Extent of Work: LOCAT PED HH F9092 TIBER RIDGE UP TO AND INCLUDING 
: 9090 TIBER RIDGE CT.  LOCATE ALL MAINS AND SERVICES.  LOCATE ALL EXISTING
: VERIZON FIBER CONDUIT.  LOCATE ENTIRE PROPERTY 
Remarks:  

Company      : SAINDUMA CONTRACTOR INC 
Contact Name : WILFREDO SALAS                 Fax          :  
Contact Phone: (703)889-0651                  Ext          :  
Alt. Contact : WILFREDO'S SECONDARY           Alt. Phone   : (703)357-7161 
Work Being Done For:  
State: MD              County: HWD 
MPG:  Y 
Caller    Provided:  Map Name: HWD   Map#: 4815 Grid Cells: H7 
Computer Generated:  Map Name:       Map#:      Grid Cells:  

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 0         Lon: 0           SE Lat: 0         Lon: 0 
Explosives: N 
BGEHW      BGEHWG     HCU01      HTV02      VHW 
Send To: HTV01     Seq No: 0154   Map Ref:  

