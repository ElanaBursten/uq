
NOTICE OF INTENT TO EXCAVATE      FIOS                          CANCELLATION 
Ticket No: 9193914 
Transmit      Date: 05/04/09      Time: 04:19 PM           Op: webusr 
Original Call Date: 04/28/09      Time: 02:40 PM           Op: webusr 
Work to Begin Date: 04/30/09      Time: 02:45 PM 

Place: PHOENIX 
Address: 3527        Street: SOUTHSIDE AV 
Nearest Intersecting Street: 	MEREDITH RIDGE RD		 

Type of Work: VERIZON/FIOS/MK/OBCU 
Extent of Work: LOCATE PED F3527 SOUTHSIDE RD UP TO AND INCLUDING 3527
: SOUTHSIDE AV LOCATE ALL MAINS AND SERVICES. LOCATE ALL EXISTING VERIZON
: FIBER/ CONDUIT. LOCATE ENTIRE PROPERTY 
Remarks: CANCEL - NEW MISS U CALLED 

Company      : OB CABLE UNDERGROUND 
Contact Name : JESSICA ORELLANA               Fax          :  
Contact Phone: (443)445-3923                  Ext          :  
Alt. Contact : SEAN KENDIG                    Alt. Phone   : (443)474-0895 
Work Being Done For:  
State: MD              County: BALT 
MPG:  Y 
Caller    Provided:  Map Name: BALT  Map#: 19   Grid Cells: B10 
Computer Generated:  Map Name:       Map#:      Grid Cells:  

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 0         Lon: 0           SE Lat: 0         Lon: 0 
Explosives: N 
CBW04      CTV01      VBT 
Send To: BGEBA     Seq No: 0363   Map Ref:  
         BGEBAG            0363 

