
UULC2009020300380	206296	2009/02/03 12:46:00	00391

UULC 
Ticket No:  9022714               2 FULL BUSINESS 
Send To: QLNWA16    Seq No:  117  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:44 am    Op: ortris 
Original Call Date:  2/03/09   Time: 10:30 am    Op: ortris 
Work to Begin Date:  2/06/09   Time: 12:00 am 

State: WA            County: KING                    Place: FEDERAL WAY 
Address:             Street: 26TH AVE S 
Nearest Intersecting Street: S 349TH ST 

Twp: 21N   Rng: 4E    Sect-Qtr: 20 
Twp: 21N   Rng: 4E    Sect-Qtr: 28-NW-NE,21-SW-SE 
Legal Given:  

Type of Work: TEST PITS 
Location of Work: SITE IS AT THE SW CORNER OF THE INTER. MARK THE ENTIRE LOT,
: AT THIS SITE, APX 10,000-12,000 SQ FT. AREA WILL BE MARKED IN WHITE
: PARCEL NUMBER 2191601045 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : MCINTIRE CONSULTING 
Contact Name: LEIGH MCINTIRE                   Phone: (253)380-0007 
Cont. Email :  
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: JOEL HATCH 
Additional Members:  
CC7760     LKHAVN01   OLYPE01    PSEELC34   PSEGAS32   WSDOT12

