
UULC2009020300382	206296	2009/02/03 12:46:00	00392

UULC 
Ticket No:  9022754               2 FULL BUSINESS 
Send To: QLNWA16    Seq No:  119  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:44 am    Op: orjust 
Original Call Date:  2/03/09   Time: 10:42 am    Op: orjust 
Work to Begin Date:  2/06/09   Time: 12:00 am 

State: WA            County: KING                    Place: BELLEVUE 
Address:             Street: 112TH AVE SE 
Nearest Intersecting Street: SE 15TH ST 

Twp: 24N   Rng: 5E    Sect-Qtr: 5-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  

Type of Work: SOIL BORING 
Location of Work: WORKSITE IS APX 200FT N OF ABV INTER, ON E SIDE OF 112TH.
: FROM THIS POINT MARK AN APX 50FT RADIUS OF ORANGE FLAGGED AND WHITE PAINTED
: AREA THAT IS LOCATED APX 15FT OFF E SIDE OF 112TH AVE SE IN A GRASSY AREA. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : SHANNON AND WILSON, INC 
Contact Name: ANDREW CANEDAY                   Phone: (206)632-8020 
Cont. Email : EVP@SHANWIL.COM 
Alt. Contact: ANDREW CELL -                    Phone: (206)459-9071 
Contact Fax : (206)695-6777 
Work Being Done For: CITY OF BELLEVUE 
Additional Members:  
BELLVE01   CC7721     KCMTRO01   PSEELC50   PSEGAS50   TWTWA02

