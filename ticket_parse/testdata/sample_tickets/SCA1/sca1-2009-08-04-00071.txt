
UQSTSO 00063A USAS 08/04/09 07:09:51 A92040582-02A REMK REMK GRID

Ticket : A92040582 Date: 08/04/09 Time: 07:09 Oper: LAB Chan: 100
Old Tkt: A92040582 Date: 07/23/09 Time: 10:54 Oper: JMC Revision: 02A
State: CA County: RIVERSIDE            Place: PERRIS
Locat: ENTIRE INTER OF E ELLIS AVE AND GOETZ RD
Address: E ELLIS AVE
X/ST 1 : GOETZ RD
Grids: 0807H043                                             Delineated: Y
Lat/Long  : 33.772623/-117.223713 33.772163/-117.222926
          : 33.771943/-117.224111 33.771483/-117.223323
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: Y  Re-Mark Type: ALL

Work : LOWER TELE CONDUIT SYSTEM
Work date: 08/04/09 Time: 07:09 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : REQUEST REMARKS        Permit   : NOT AVAILABLE
Done for : VERIZON WO# 5838P0A0CT

Company: HCI                            Caller: TONY
Address: 33250 HOWARD RD
City&St: MENIFEE, CA                    Zip: 92584
Phone: 951-679-0062 Ext:      Call back: 6A- 4P
Formn: TONY                 Phone: 909-997-3682
Email: TBELTRAN@HCI-INC.COM

                                  COMMENTS

**RESEND**REQUEST REMARKS FROM TELEPHONE-WORK CONT REQ UTI TO RESPOND NOW ASAP
PER TONY--[CLA 07/31/09 08:44]
**RESEND**REQUEST REMARKS FROM ALL-WORK CONT FRM UTI-VERIZON PER TONY--[LAB
08/04/09 07:09]

Mbrs : ATTATL EMW01  PER01  SCG1OM TWCNRIV       USCE77 UVZMENIF      UVZPERS
