


NOTICE OF INTENT TO EXCAVATE 
EMER-EMERGENCY                          NEW TICKET 
Ticket No:   172956 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:52     Op: michelle.0 
Original Call Date:  6/14/02            Time:  9:46     Op: .0 

Company     : ROACH PLUMBING 
Contact Name: LEWIS ROACH                     Contact Phone: (318) 631-4533 
Alt. Contact:                                 Alt. Phone   : (318) 208-0420 

Type of Work : REPAIR A BROKEN SEWER LINE 
Work Done For: GARY CREOLE 

State: LA       Parish: BOSSIER 
Place: BOSSIER CITY CITY 
Address: 5511        Street: BAYOU DR  
Nearest Intersecting Street: SUSANNA DR  

Location:       MARK IN THE FRONT OF THE PROPERTY  
Remarks: EMERGENCY!!!!!RUSH!!!!ASAP 
CREW IS ON SITE NOW!!!! 

Work to Begin Date:  6/14/02            Time: 10:00 AM 
Mark By       Date:                     Time:          

Ex. Coord: NW Lat: 32 27 40  Lon: -93 38 53  SE Lat: 32 26 52  Lon:  -93 38 47 

Additional Members:  ARK01, EXS01, MAGNOL01, SWE01, TCIBOS01 

Send To:  SH01      Seq No:  25 



NOTICE OF INTENT TO EXCAVATE 
SHRT-SHORT NOTICE                       NEW TICKET 
Ticket No:   172951 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: carolyn.0 
Original Call Date:  6/14/02            Time:  9:45     Op: .0 

Company     : ENTERGY 
Contact Name: ERIC JONES                      Contact Phone: (318) 329-5508 
Alt. Contact: CELL #                          Alt. Phone   : (318) 366-6466 

Type of Work : BURY UNDERGROUND ELECTRIC SERVICE 
Work Done For: ENTERGY 

State: LA       Parish: OUACHITA 
Place: SWARTZ CDP 
Address: 164         Street: STUBBS VINSON RD  
Nearest Intersecting Street: US HWY 80 E 

Location:       MARK THE RIGHT SIDE OF PROP AS FACING FROM FRONT OF PROP  
: TO REAR OF PROP (APX 470FT IN DIST) 

Remarks:  
 

Work to Begin Date:  6/17/02            Time:  9:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 32 32 22  Lon: -91 58 26  SE Lat: 32 31 1   Lon:  -91 58 20 

Additional Members:  ENTGY05, GOW01, LAGN01, LRUTIL01, TIDWEL01 

Send To:  MO01      Seq No:  45 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172845 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: judy.0 
Original Call Date:  6/14/02            Time:  9:22     Op: judy.0 

Company     : NORTHEAST LA POWER CO-OP 
Contact Name: DONNIE SHIRLEY                  Contact Phone: (318) 435-4523 
Alt. Contact:                                                 

Type of Work : INSTALL 3 POLES AND 3 ANCHORS 
Work Done For: CHARLES MOORE 

State: LA       Parish: RICHLAND 
Place: BEE BAYOU 
Address:             Street: O QUINN RD  
Nearest Intersecting Street: MCCARTNEY  

Location:       ON O QUINN RD APX .6MI W OF MCCARTNEY RD--MARK IN FRONT  
: ON S SIDE OF RD GOING W APX 900FT--AREAS ARE MARKED WITH RED PAINT AND  
: YELLOW FLAGGS 

Remarks:  
 

Work to Begin Date:  6/18/02            Time:  9:30 AM 
Mark By       Date:  6/18/02            Time:  9:30 AM 

Ex. Coord: NW Lat: 32 26 57  Lon: -91 39 34  SE Lat: 32 26 54  Lon:  -91 39 9 

Additional Members:  

Send To:  MO01      Seq No:  46 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172850 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: judy.0 
Original Call Date:  6/14/02            Time:  9:24     Op: judy.0 

Company     : NORTHEAST LA POWER CO-OP 
Contact Name: DONNIE SHIRLEY                  Contact Phone: (318) 435-4523 
Alt. Contact:                                                 

Type of Work : INSTALL 1 POLE IN LINE 
Work Done For: STEWART KING 

State: LA       Parish: RICHLAND 
Place: ARCHIBALD 
Address: 169         Street: ANDY ALLEN RD  
Nearest Intersecting Street: CRAWFORD RD  

Location:       APX 1/2MI S OF CRAWFORD RD--MARK IN FRONT ON E SIDE OF  
: RD--AREA IS MARKED WITH RED PAINT AND YELLOW FLAG 

Remarks:  
 

Work to Begin Date:  6/18/02            Time:  9:30 AM 
Mark By       Date:  6/18/02            Time:  9:30 AM 

Ex. Coord: NW Lat: 32 22 26  Lon: -91 42 8   SE Lat: 32 22 13  Lon:  -91 41 54 

Additional Members:  CTC01 

Send To:  MO01      Seq No:  47 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172935 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: candice.0 
Original Call Date:  6/14/02            Time:  9:41     Op: .0 

Company     : RYLEE CONTRACTING 
Contact Name: RONNIE                          Contact Phone: (318) 619-9094 
Alt. Contact: N/A                                             

Type of Work : INSTALL A SEWER LINE 
Work Done For: CITY OF ALEXANDRIA 

State: LA       Parish: RAPIDES 
Place: ALEXANDRIA CITY 
Address:             Street: COLISEUM BLVD  
Nearest Intersecting Street: ESKEW  

Location:       FROM ADDRESS 5600 TO 5606 COLISEUM BLVD----MARK THE FRONT  
: ON BOTH SIDES OF COLISEUM BLVD 

Remarks:  
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 31 17 42  Lon: -92 30 47  SE Lat: 31 17 41  Lon:  -92 29 53 

Additional Members:  APTV01, CAL01, CLEC04, DOTD08, LIG01 

Send To:  AL01      Seq No:  22 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172943 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: casandra.0 
Original Call Date:  6/14/02            Time:  9:44     Op: casandra.0 

Company     : BELL SOUTH 
Contact Name: THERESA C DEAN                  Contact Phone: (337) 261-4613 
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337) 261-4611 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: BELLSOUTH 

State: LA       Parish: CLAIBORNE 
Place: LISBON VILLAGE 
Address: 2933        Street: HEBRON RD  
Nearest Intersecting Street: STATE HWY 2  

Location:         MARK ENTIRE PROPERTY-(NEW TRAILER ) 
Remarks: TCD   LM0202919       06/14/11 
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 48 36  Lon: -92 51 56  SE Lat: 32 47 43  Lon:  -92 51 9 

Additional Members:  DOTD04, DUKE03, ELPASO01, ENTGY02, PARMIN01, TLP03 

Send To:  SH01      Seq No:  26 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172944 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: casandra.0 
Original Call Date:  6/14/02            Time:  9:44     Op: casandra.0 

Company     : BELL SOUTH 
Contact Name: THERESA C DEAN                  Contact Phone: (337) 261-4613 
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337) 261-4611 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: BELLSOUTH 

State: LA       Parish: BOSSIER 
Place: HAUGHTON TOWN 
Address: 1170        Street: STEWART RD  
Nearest Intersecting Street: STATE HWY 614  

Location:         MARK ENTIRE PROPERTY 
Remarks: TCD   LM0202920       06/14/12 
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 32 49  Lon: -93 32 31  SE Lat: 32 32 37  Lon:  -93 32 27 

Additional Members:  ARK01, ATT01, DOTD04, ELPASO02, SWE01, VWAT01 

Send To:  SH01      Seq No:  27 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172945 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: allison.0 
Original Call Date:  6/14/02            Time:  9:44     Op: allison.0 

Company     : BELL SOUTH 
Contact Name: THERESA C DEAN                  Contact Phone: (337) 261-4613 
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337) 261-4611 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: BELLSOUTH 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address: 9401        Street: RED OAK LN  
Nearest Intersecting Street: STEWART DR  

Location:         MARK REAR OF PROPERTY 
Remarks: TCD   LM0202921        06/14/13 
CCB @ 9:45AM 6/14/02 ACB 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 24 5   Lon: -93 44 4   SE Lat: 32 23 56  Lon:  -93 43 49 

Additional Members:  ARK05, CSHREV01, MCI01, SCTV01, SWE01, TGN04 

Send To:  SH01      Seq No:  28 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172948 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: michelle.0 
Original Call Date:  6/14/02            Time:  9:44     Op: michelle.0 

Company     : ELLINGTON CONSTRUCTION CO 
Contact Name: BLAKE ADAMS                     Contact Phone: (318) 387-3351 
Alt. Contact: CELL#                           Alt. Phone   : (318) 381-7058 

Type of Work : INSTALL TELEPHONE CABLE 
Work Done For: BELL SOUTH 

State: LA       Parish: OUACHITA 
Place: MONROE CITY 
Address:             Street: STATE HWY 139  
Nearest Intersecting Street: SHENANDOAH  

Location:       MARK SE AND SW CORNER OF HWY 139 AND SHENANDOAH THEN  
: GOING W INTO PROPERTY FOR APX 1000 FT STOP AT LAKEVIEW PARK DR 

Remarks: UPDATE OF 159683 ON GOING 
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 33 20  Lon: -92 85 51  SE Lat: 32 33 2   Lon:  -92 53 53 

Additional Members:  CABLEM01, ENTGY05, GOW01, LAGN01, LATV01, LRUTIL01 
 TGN03, TIDWEL01 

Send To:  MO01      Seq No:  48 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172954 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: candice.0 
Original Call Date:  6/14/02            Time:  9:46     Op: .0 

Company     : RYLEE CONTRACTING 
Contact Name: RONNIE                          Contact Phone: (318) 619-9094 
Alt. Contact: N/A                                             

Type of Work : INSTALL A SEWER LINE 
Work Done For: CITY OF PINEVILLE 

State: LA       Parish: RAPIDES 
Place: PINEVILLE CITY 
Address:             Street: LORD OF LORDS  
Nearest Intersecting Street: KINGSOF KINGS  

Location:       MARK IN THE FRONT ON BOTH SIDES OF LORD OF LORDS FROM  
: KING OF KINGS GOING N FOR APX 200FT 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 31 21 48  Lon: -92 24 47  SE Lat: 31 21 45  Lon:  -92 24 46 

Additional Members:  APTV01, CLEC04, CPINE01, RPW01, TLP03 

Send To:  AL01      Seq No:  23 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172957 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: yvette.99 
Original Call Date:  6/14/02            Time:  9:46     Op: .0 

Company     : NATCHITOCHES WATER WKS DIST #2 
Contact Name: LES DUNN                        Contact Phone: (318) 352-9601 
Alt. Contact: ANYONE                          Alt. Phone   : (318) 471-9466 

Type of Work : INSTALLING WATER METER 
Work Done For: NATCHITOCHES WATER WKS DIST #2 

State: LA       Parish: NATCHITOCHES 
Place: MELROSE 
Address: 3300        Street: STATE HWY 119  
Nearest Intersecting Street: STATE HWY 493  

Location:       APX 1/2 MI N OF HWY 493-- MARK IN THE FRONT OF PROPERTY  
: -- AREA HAS A BLUE FLAG AT LOCATION "NEW DOUBLE TRAILER" 

Remarks: PRE-APPROVED TICKET #NATWTR-6-14-1 
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 31 36 22  Lon: -92 58 23  SE Lat: 31 36 12  Lon:  -92 58 13 

Additional Members:  DOTD08, NATWTR01, VEM01 

Send To:  AL01      Seq No:  24 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172958 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: allison.0 
Original Call Date:  6/14/02            Time:  9:47     Op: .0 

Company     : PETER ROCK BAPTIST CHURCH 
Contact Name: PRINCE STATEN                   Contact Phone: (318) 325-1598 
Alt. Contact:                                                 

Type of Work : SURVEY FOR FUTURE CONSTRUCTION 
Work Done For: PETER ROCK BAPTIST CHURCH 

State: LA       Parish: OUACHITA 
Place: MONROE CITY 
Address: 709         Street: S 14TH ST  
Nearest Intersecting Street: ORANGE ST  

Location:       MARK THE ENTIRE PROP  
Remarks: PLEASE CALL BEFORE GOING TO THE SITE 
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 32 29 57  Lon: -92 5 43   SE Lat: 32 29 45  Lon:  -92 5 32 

Additional Members:  ATT01, CM01, ENTGY05, LAGN01, LATV01 

Send To:  MO01      Seq No:  49 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172959 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: candice.0 
Original Call Date:  6/14/02            Time:  9:47     Op: .0 

Company     : RYLEE CONTRACTING 
Contact Name: RONNIE                          Contact Phone: (318) 619-9094 
Alt. Contact: N/A                                             

Type of Work : INSTALL A SEWER LINE 
Work Done For: CITY OF PINEVILLE 

State: LA       Parish: RAPIDES 
Place: PINEVILLE CITY 
Address:             Street: KING OF KINGS  
Nearest Intersecting Street: LORD OF LORDS  

Location:       MARK THE FRONT AND REAR OF PROPS ON THE N SIDE OF KING OF  
: KINGS FROM LORD OF LORDS GOING E FOR APX 400FT 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 31 21 45  Lon: -92 24 47  SE Lat: 31 21 44  Lon:  -92 24 44 

Additional Members:  APTV01, CLEC04, CPINE01, DOTD08, RPW01, TLP03 

Send To:  AL01      Seq No:  25 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172962 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: roun.0 
Original Call Date:  6/14/02            Time:  9:49     Op: .0 

Company     : MANNS CONSTRUCTION 
Contact Name: TAMMAY                          Contact Phone: (318) 249-4463 
Alt. Contact: RONNIE MEYERS                   Alt. Phone   : (318) 376-4401 

Type of Work : SETTING BASINS/POURING CONCRETE/SITE UTILITES 
Work Done For: MANNS CONSTRUCTION 

State: LA       Parish: OUACHITA 
Place: MONROE CITY 
Address: 3811        Street: DESIARD ST  
Nearest Intersecting Street: UNIVERSITY AVE  

Location:       MARK ENTIRE PROPERTY FROM FILHIOL HALL TO BIEDENHARN HALL   
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 32 31 28  Lon: -92 4 36   SE Lat: 32 31 24  Lon:  -92 4 28 

Additional Members:  CM01, ENTGY05, LAGN01, LATV01, LIG03, NLU01 

Send To:  MO01      Seq No:  50 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172941 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:53     Op: heathe.0 
Original Call Date:  6/14/02            Time:  9:44     Op: heathe.0 

Company     : BELL SOUTH 
Contact Name: THERESA C DEAN                  Contact Phone: (337) 261-4613 
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337) 261-4611 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: BELLSOUTH 

State: LA       Parish: BOSSIER 
Place: BENTON TOWN 
Address: 2011        Street: LUKE LN  
Nearest Intersecting Street: CHASE WAY  

Location:         MARK ENTIRE PROPERTY 
Remarks: TCD   LM0202917    06/14/09 
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 41 23  Lon: -93 41 12  SE Lat: 32 41 21  Lon:  -93 40 34 

Additional Members:  CBBWTR01, SWE01 

Send To:  SH01      Seq No:  29 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   172942 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:54     Op: casandra.0 
Original Call Date:  6/14/02            Time:  9:44     Op: casandra.0 

Company     : BELL SOUTH 
Contact Name: THERESA C DEAN                  Contact Phone: (337) 261-4613 
Alt. Contact: MELISSA PREJEAN                 Alt. Phone   : (337) 261-4611 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: BELLSOUTH 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address: 5745        Street: SPRINGLAKE DR  
Nearest Intersecting Street: GLEN ERICA ST  

Location:         MARK ENTIRE PROPERTY 
Remarks: TCD   LM0202918       06/14/10 
 

Work to Begin Date:  6/18/02            Time:  9:45 AM 
Mark By       Date:  6/18/02            Time:  9:45 AM 

Ex. Coord: NW Lat: 32 26 9   Lon: -93 44 9   SE Lat: 32 25 54  Lon:  -93 44 4 

Additional Members:  ARK04, CSHREV01, SCTV01, SWE01, TEXAR01 

Send To:  SH01      Seq No:  30 



NOTICE OF INTENT TO EXCAVATE 
24HR-24 HOURS NOTICE                    NEW TICKET 
Ticket No:   172963 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:54     Op: carolyn.0 
Original Call Date:  6/14/02            Time:  9:49     Op: .0 

Company     : CITY OF PINEVILLE 
Contact Name: WAYNE                           Contact Phone: (318) 449-5688 
Alt. Contact:                                 Alt. Phone   : (318) 449-5668 

Type of Work : INSTALLING WATER TAPS 
Work Done For: CITY OF PINEVILLE 

State: LA       Parish: RAPIDES 
Place: PINEVILLE CITY 
Address: 2826        Street: STATE HWY 107  
Nearest Intersecting Street: WAINWRIGHT  

Location:       MARK THE FRONT OF PROP ON RIGHT SIDE OF MAIL BOX AS  
: FACING--AREA IS MARKED BY 3 BLUE FLAGS 

Remarks:  
 

Work to Begin Date:  6/18/02            Time:  9:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 31 19 25  Lon: -92 23 29  SE Lat: 31 19 15  Lon:  -92 23 19 

Additional Members:  APTV01, CLEC04, CPINE01, DOTD08, TLP03 

Send To:  AL01      Seq No:  26 



NOTICE OF INTENT TO EXCAVATE 
EMER-EMERGENCY                          NEW TICKET 
Ticket No:   172969 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:54     Op: meltreka.0 
Original Call Date:  6/14/02            Time:  9:53     Op: .0 

Company     : CITY OF MONROE WATER DIST DEPT 
Contact Name: DANNY                           Contact Phone: (318) 329-2811 
Alt. Contact:                                 Alt. Phone   : (318) 329-2495 

Type of Work : REPAIR LEAK ON WATER MAIN 
Work Done For: CITY OF MONROE WATER DIST DEPT 

State: LA       Parish: OUACHITA 
Place: MONROE CITY 
Address:             Street: STONE AVE  
Nearest Intersecting Street: S 10TH ST  

Location:       MARK THE ENTIRE INTERS OF STONE AVE AND S 10TH ST 
Remarks: EMERGENCY!!!!!RUSH!!!!ASAP 
CREW IS ENROUTE TO SITE!!! 

Work to Begin Date:  6/14/02            Time: 10:00 AM 
Mark By       Date:                     Time:          

Ex. Coord: NW Lat: 32 30 6   Lon: -92 6 12   SE Lat: 32 30 4   Lon:  -92 6 9 

Additional Members:  ATT01, CM01, ENTGY05, LAGN01, LATV01 

Send To:  MO01      Seq No:  51 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    CORRECTION 
Ticket No:   172962 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time:  9:54     Op: roun.0 
Original Call Date:  6/14/02            Time:  9:49     Op: roun.0 

Company     : MANNS CONSTRUCTION 
Contact Name: TAMMAY                          Contact Phone: (318) 249-4463 
Alt. Contact: RONNIE MEYERS                   Alt. Phone   : (318) 376-4401 

Type of Work : SETTING BASINS/POURING CONCRETE/SITE UTILITES 
Work Done For: MANNS CONSTRUCTION 

State: LA       Parish: OUACHITA 
Place: MONROE CITY 
Address: 3811        Street: DESIARD ST  
Nearest Intersecting Street: UNIVERSITY AVE  

Location:       MARK ENTIRE PROPERTY FROM FILHIOL HALL TO BIEDENHARN HALL   
: --> ADDITION: IF ANY ??'S PLEASE CALL WILLIE HUDSON @ 376-1025 <-- CCB @  
: 9:54 6/14 HLK  

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 10:00 AM 
Mark By       Date:  6/18/02            Time: 10:00 AM 

Ex. Coord: NW Lat: 32 31 28  Lon: -92 4 36   SE Lat: 32 31 24  Lon:  -92 4 28 

Additional Members:  CM01, ENTGY05, LAGN01, LATV01, LIG03, NLU01 

Send To:  MO01      Seq No:  52 



