7ABYER04 00003 AOC 10/07/02 07:30:22 0276634-000 EMER NEW STRT

Ticket : 0276634 Rev: 00 Date: 10/07/02 Time: 07:24 Opr: MLT Chn: 15
State: AL County: MOBILE     Place: THEODORE In/Out: I
Addr :   Street: US HIGHWAY 90   CPG: N
Xst  : PLANTATION RD  Int: N

Locat: THIS SITE IS NEAR QUALITY AUTO GLASS AND EVIN RUDE BOAT MART - SEE THE
CREW ON JOBSITE FOR LOCATE INSTRUCTIONS  - PLEASE RESPOND ASAP- THE ROAD IS
BLOCK OFF AND THE SPILL IS MARKED WITH CONES
***EMERGENCY***
:
Grids: T05SR02W27SW T05SR02W34*W T06SR02W03*W T06SR02W04SE T06SR02W08SE
Grids: T06SR02W09** T06SR02W17NE T06SR03W19NE T06SR03W23SE T06SR03W24*W
Grids: T08SR02W32N*
Work date : 10/07/02 Time: 08:27  Hrs notc: 001/001  Priority: 0  Emerg: Y
Legal Date: 10/09/02 Time: 07:24  Good Thru: 10/23/02  Update: 10/21/02
Work Type: CLEANING UP SULPHER SPILL
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: US ENVIRONMENTAL

Company: US ENVIRONMENTAL
Address: 4230-A HALLS MILL RD
City&St: MOBILE, AL                     Zip: 36693
Caller : JEFF Phone: 251-662-3500
Callback: N
  Cell/Pager: 251-331-0644
Mbrs : APC55B ATTT01 BYER04 CASO01 CTAL01 CTAL1E GSPL01 MCIT01 MCWS01 MCWS1E
Mbrs : MDCM01 MOBG01 MOBW01 QWCM01 SCMB01 SOAL01 TCCI01 TRPL04 TRPL4E UULC02
Mbrs : WLCM01




































BYER04 00004 AOC 10/07/02 07:30:33 0276623-000 NORM NEW STRT

Ticket : 0276623 Rev: 00 Date: 10/07/02 Time: 07:23 Opr: KDS Chn: 16
State: AL County: MOBILE     Place: MOBILE In/Out: I
Addr : 4152  Street: BELVEDERE ST   CPG: N
Xst  : CRESTVIEW DR  Int: N
Subd : CRESTVIEW

Locat: LOCATE AROUND THE ENTIRE PERIMETER OF THE GARAGE
:
Grids: T05SR02W11NE
Work date : 10/09/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 10/09/02 Time: 07:23  Good Thru: 10/23/02  Update: 10/21/02
Work Type: TERMITE BAITING SYS
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: COOKS PEST CONTROL

Company: COOKS PEST CONTROL
Address: 4165 GOVERNMENT BLVD
City&St: MOBILE, AL                     Zip: 36693
Caller : EDDIE CLEMENTS Phone: 251-665-7378
Callback: N
Mbrs : APC55B ATTT01 BYER04 CCMO01 MOBG01 MOBL01 MOBL02 MOBW01 SCMB01 SOAL01
Mbrs : TCCI01 UULC02










































BYER04 00005 AOC 10/07/02 07:30:42 0276629-000 NORM UPDT STRT

Ticket : 0276629 Rev: 00 Date: 10/07/02 Time: 07:27 Opr: KDS Chn: 16
State: AL County: MOBILE     Place: PRICHARD In/Out: O
Addr :   Street: ONTARIO DR   CPG: N
Xst  : MYERS RD  Int: N

Locat: LOCATE BOTH SIDES OF ONTARIO DR FROM MYERS RD ALL THE WAY TO SUPERIOR DR
:
Remarks : <<<<< UPDATE OF TICKET #0000237281 >>>>> <<<<< UPDATE OF TICKET
#0000252262 >>>>> <<<<< UPDATE OF TICKET #0000265057 >>>>>
:
Grids: T03SR02W25SW T03SR02W26SE
Work date : 10/09/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 10/09/02 Time: 07:27  Good Thru: 10/23/02  Update: 10/21/02
Work Type: SEWER CONSTRUCTION
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: CONSTRUCTION LABOR

Company: CONSTRUCTION LABOR
Address: 2150 OLD GOVERMENT
City&St: MOBILE, AL                     Zip:
Caller : DON PORTER Phone: 251-473-5511
Callback: N
Mbrs : APC55B BYER04 CCMO01 FCMO01 GSPL01 LVL301 MOBG01 MOBL02 MOBW01 SCMB01
Mbrs : SOAL01 UULC02








































BYER04 00006 AOC 10/07/02 07:30:51 0276632-000 NORM UPDT STRT

Ticket : 0276632 Rev: 00 Date: 10/07/02 Time: 07:28 Opr: KDS Chn: 16
State: AL County: MOBILE     Place: PRICHARD In/Out: O
Addr :   Street: SUPERIOR DR   CPG: N
Xst  : MYERS RD  Int: N

Locat: LOCATE FROM MYERS RD ON SUPERIOR DR ALL THE WAY TO SHELTON BEACH RD
:
Remarks : <<<<< UPDATE OF TICKET #0000237284 >>>>> <<<<< UPDATE OF TICKET
#0000252264 >>>>> <<<<< UPDATE OF TICKET #0000265058 >>>>>
:
Grids: T03SR02W25SW T03SR02W26SE
Work date : 10/09/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 10/09/02 Time: 07:28  Good Thru: 10/23/02  Update: 10/21/02
Work Type: SEWER CONSTRUCTION
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: CONSTRUCTION LABOR

Company: CONSTRUCTION LABOR
Address: 2150 OLD GOVERMENT
City&St: MOBILE, AL                     Zip:
Caller : DON PORTER Phone: 251-473-5511
Callback: N
Mbrs : APC55B BYER04 CCMO01 FCMO01 GSPL01 LVL301 MOBG01 MOBL02 MOBW01 SCMB01
Mbrs : SOAL01 UULC02








































BYER04 00007 AOC 10/07/02 07:31:00 0276635-000 NORM UPDT STRT

Ticket : 0276635 Rev: 00 Date: 10/07/02 Time: 07:29 Opr: KDS Chn: 16
State: AL County: MOBILE     Place: PRICHARD In/Out: I
Addr :   Street: HURON CT   CPG: N
Xst  : SUPERIOR DRIVE  Int: N

Locat: LOCATE HURON COURT AND BOTH SIDES OF THE ROAD - ENTIRE RD
:
Remarks : <<<<< UPDATE OF TICKET #0000265061 >>>>>
:
Grids: T03SR02W26SE
Work date : 10/09/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 10/09/02 Time: 07:29  Good Thru: 10/23/02  Update: 10/21/02
Work Type: SEWER LINE CONSTRUCTION
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: CONSTRUCTION LABOR

Company: CONSTRUCTION LABOR
Address: 2150 OLD GOVERMENT
City&St: MOBILE, AL                     Zip:
Caller : DON PORTER Phone: 251-473-5511
Callback: N
Mbrs : APC55B BYER04 CCMO01 FCMO01 GSPL01 LVL301 MOBG01 MOBL02 MOBW01 SCMB01
Mbrs : SOAL01 UULC02









































BYER04 00008 AOC 10/07/02 07:31:09 0276641-000 NORM NEW STRT

Ticket : 0276641 Rev: 00 Date: 10/07/02 Time: 07:29 Opr: VDH Chn: 6
State: AL County: MOBILE     Place: MOBILE In/Out: I
Addr : 1505  Street: GOVERNMENT ST   CPG: N
Xst  : N CATHERINE ST  Int: N

Locat: LOCATE ENTIRE PERIMETER OF HOUSE
:
Grids: T04SR01W13SW T04SR01W14SE T04SR02W19N* T04SR02W20NW T04SR03W24SE
Work date : 10/09/02 Time: 07:30  Hrs notc: 048/048  Priority: 2  Emerg: N
Legal Date: 10/09/02 Time: 07:29  Good Thru: 10/23/02  Update: 10/21/02
Work Type: TERMITE BAITING SYS
White-lined:N  U/O/B:U  R/R:N  Blasting:N  Boring:N
Done for: COOKS PEST CONTROL

Company: COOKS PEST CONTROL
Address: 4165 GOVERNMENT BLVD
City&St: MOBILE, AL                     Zip: 36693
Caller : JAMES AMICK Phone: 251-665-7378
Callback: N
Mbrs : ADBS01 APC55B AZED01 BYER04 CCMO01 DFAP01 ESPR03 GULF01 ISFN01 LVL301
Mbrs : MCIT01 MDCM01 MOBG01 MOBL01 MOBL02 MOBW01 MRCM01 PRCL01 QWCM01 SCMB01
Mbrs : SLLC01 SOAL01 TCCI01 TLPK01 UULC02 WLCM01










































k
