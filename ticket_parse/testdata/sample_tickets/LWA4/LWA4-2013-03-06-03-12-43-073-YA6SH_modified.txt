

From: IRTHNet  At: 03/06/13 02:12 AM  Seq No: 1

UULC 
Ticket No: 13049101               ++EMERGENCY++ 
Send To: QLNWA99    Seq No:    1  Map Ref:  

Transmit      Date:  3/06/13   Time: 12:11 am    Op: wiambi 
Original Call Date:  3/06/13   Time: 12:05 am    Op: wiambi 
Work to Begin Date:  3/06/13   Time:  2:45 am 

State: WA            County: KING                    Place: SEATTLE 
Address:             Street: AIRPORT WAY S 
Nearest Intersecting Street: S NORFOLK ST 

Twp: 23N   Rng: 4E    Sect-Qtr: 44-SE-NE,3-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.5141623 Lon:-122.2887845 SE Lat: 47.5124004 Lon:-122.2866235 

Type of Work: EMERGENCY- REPLACE POLE 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: SITE IS APPROXIMATELY 100 FT S OF THE ABOVE INTERSECTION ON THE E SIDE OF
: AIRPORT WAY S. MARK WITHIN A 30 FT RADIUS OF THE BROKEN POLE LOCATED ON THE
: SHOULDER OF THE ROAD AT THIS POINT. 

Remarks: CALLER REQUESTS MARKS BY 03/05/2013 BY 02:45 AM 
:  

Company     : SEATTLE CITY LIGHT 
Contact Name: SAMUEL ALVAREZ                   Phone: (206)386-1659 
Cont. Email : samuel.alvarez@seattle.gov 
Alt. Contact: SAM ALVAREZ CELL                 Phone: (206)459-6032 
Contact Fax :  
Work Being Done For: SEATTLE CITY LIGHT 
Additional Members:  
360NET01   CC7760     ELCLT05    KCMTRO01   LEVL301    MCI01      PUGG03 
 QWEST05    SEACL01    SEAH2001   SEASIG01   SEAWW01    SPRINT03   STARCM01 
 STTL01     WSDOT12    XO02
