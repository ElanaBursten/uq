
BAJA   00014 UTAHc 04/03/08 11:28:20 C80940321-00C NORM UPDT GRID

Ticket : C80940321 Rev:00C Taken: 04/03/08 11:27
Old Tkt: C80850030 Taken: 03/25/08 07:36 Oper: _BLAKE
Submitted: 04/03/08 11:28 Oper: _BEN Chan:123
Legal date: 04/07/08 11:27
Good Thru : 04/17/08 11:27  Update By: 04/15/08 11:27

State: UT Cnty: WASHINGTON Place: HURRICANE
Subdivision: STONEBROOK TOWNHOME SQUARE

Address :
Street  : W 100 S
Cross 1 : S 1150 W  Within 1/4 mile: N
Side of St:     Side of Lot:    Digging in Rd: Y
Svc Side of St: SEE LOCATE FIELD  Depth:
Location: FROM GVN INTSXN: STK GOING WEST FOR APRX 1000FT THAN GOING NORTH FOR
APRX 500FT TO HWY 9/STATE ST. STKG BOTH SIDES, FRONT & LONG XING FOR GVN
STRETCH.
:
Remarks : >>> UPDATE OF TICKET #C080850030 <<<
:
Grids   : 3710B11318A  3710B11318B  3710C11318A  3710C11318B

P&D: N Work type: INSTL GAS MAIN
Done for  : QUESTAR GAS
Ug/Oh/Both: U  Expl/Blast: N  Boring: N  Railroad: U  Emergency: N  Meet: N

Company : NIELS FUGAL SONS COMPANY Phone: 435-634-1817
Co addr : 16 W WASHINGTON  CITY INDUSTRIAL DR
City    : WASHINGTON State: UT Zip: 84780
Caller  : GAYLE HANSEN Phone: 435-634-1817  Type: E
Contact : SEAN Phone: 435-669-7087
BestTime:
Fax     : 435-634-9677
Email   : GAYLEH@FUGAL.COM

Members: ASHCRK BAJA   HURCNL HURCTY OCLQS5 QLNUT3 RMPCED UDOT4C

View map at:
http://newtin.bluestakes.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=C08094032100C&OPR=jfSOBzqhUL5rQ5k

