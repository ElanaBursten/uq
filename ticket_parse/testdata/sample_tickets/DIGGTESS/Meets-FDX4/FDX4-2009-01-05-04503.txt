

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    090056012         Old Ticket:       083661870         
Priority:         Normal            By:               Maria B           
Source:           Voice             Hours Notice:     48                
Type:             Update            Date:             1/5/2009 5:29:21 PM
Sequence:         1469                                                  
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
NEMA 3 ELECTRIC                     Type:             Contractor        
4591 OLD HWY 67                     Contact:          BILL ASHER        
MIDLOTHIAN, TX 76065                Caller:           BILL ASHER        
Phone:            (972) 723-1180    Caller Phone:     (972) 723-1180    
Fax:              (972) 723-1181    Callback:         0000 - 2400       
Alt Contact:      (972) 880-3408                                        
Caller Email:     NEMA3ELE@AOL.COM                                      

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        01/07/09 at 1730  
County:           DALLAS            Type:             ELECTRIC          
Place:            IRVING            Done For:         CITY IRVING       
Street:           0 ROCK ISLAND RD                                      
Intersection:     IRVING BLVD                                           
Nature of Work:   TRENCHING/PEER FOR UG ELEC                            
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      No                Duration:         30 DAYS           
Mapsco:           MAPSCO 31B,T                                          

Remarks
----------------------------------------------------------------------------
UPDATE & REMARK-083661870- CALL ERIC 972-880-8411 REQUEST A MEET FOR 1-06-09
FOR ONCOR ELEC.                                                             
                                                                            
UPDATE & REMARK-083101620-CALL ERIC 9728808411- WILL BE ON SITE REQUEST A   
MEET ON JAN 5 09 **                                                         
MAPSCO:31B,X                                                                
WORK WILL TAKE PLACE WITH IN THE BOUNDIRES: S OF ROCK ISLAND RD W OF SOWERS 
RD N OF IRVING BLVD E OF LIBARY BLDG                                        
**THERE IS A FENCE, DONT LET FENCE BE BOUNDRY NEED LOCATES FROM STREETS     
REQUESTED**                                                                 
FOUND IN MAPSCO:31B,T**                                                     


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
ASI   FIBER LIGHT LLC   (FORMERLY XSPEDIUS)             No                  
ATL   ATT / T-TCG                                       No                  
ATT   ATT / T                                           No                  
CLG   TIME WARNER CABLE                                 No                  
GTL   VERIZON - TX - KELLER GRAPEVINE DISTRICT (METRO...No                  
IR4   ONCOR ELECTRIC DISTRIBUTION-SMP (IRVING)          No                  
MCW   MCI                                               No                  
TWT   TW TELECOM                                        No                  
U09   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  


Location
----------------------------------------------------------------------------
Latitude:         32.8160867640196  Longitude:        -96.9608785831164 
Second Latitude:  32.8127440685615  Second Longitude: -96.9517965426263

