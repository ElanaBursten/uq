

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    090051129         Old Ticket:                         
Priority:         Normal            By:               Cecelia C         
Source:           Email             Hours Notice:     48                
Type:             Normal            Date:             1/5/2009 9:21:35 AM
Sequence:         338                                                   
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
MASTEC NORTH AMERICA, INC.          Type:             Contractor        
4747 IRVING BLVD.                   Contact:          STEVE HARGROVE    
DALLAS, TX 75247                    Caller:           AMY HEGAR         
Phone:            (214) 571-2540    Caller Phone:     (214) 538-3188    
Fax:              (214) 571-2555    Callback:         0800 - 1700       
Alt Contact:      (214) 571-2540                                        
Caller Email:     AMALIA.HEGAR@MASTEC.COM                               

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        01/07/09 at 0930  
County:           DALLAS            Type:             POLE/SIGN INSTALLATION
Place:            DALLAS            Done For:         ONCOR ELECTRIC    
Street:           0 COCKRELL                                            
Intersection:     BELLEVIEW                                             
Nature of Work:   CUTTING IN POLE BOX                                   
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      No                Duration:         10 DAYS           
Mapsco:           MAPSCO 45,U                                           

Remarks
----------------------------------------------------------------------------
WORK DATE: 01-07-2009 09:30 AM                                              
MAPSCO: 45 U                                                                
LOCATES WILL BE AT THE ABOVE INTERSECTION. REQUEST MEET FOR WED. 01-07-09 AT
9:30AM. PLEASE CONTACT STEVE HARGROVE AT 214-538-3188 WITH ANY QUESTIONS.   
PR: <<PR=FAX>> <<PR=EMAIL>>                                                 
986169.XML                                                                  


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
DLE   ONCOR ELECTRIC DIST-SMP (DALLAS NORTHEAST SC)     No                  
GRE   ONCOR ELECTRIC DISTRIBUTION-SMP (DALLAS SOUTHWE...No                  
IXC   LEVEL 3 COMMUNICATIONS                            No                  
MCW   MCI                                               No                  
S97   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  
T09   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  
V36   TIME WARNER CABLE                                 No                  


Location
----------------------------------------------------------------------------
Latitude:         32.7698929926471  Longitude:        -96.7965454264706 
Second Latitude:  32.7671020661765  Second Longitude: -96.7932376617647

