
Ticket No:  9127890               48 HOUR NOTICE 
Send To: FALCON02   Seq No:    3  Map Ref:  

Transmit      Date:  6/29/09   Time:  6:35 AM    Op: orjuli 
Original Call Date:  6/29/09   Time:  6:34 AM    Op: orjuli 
Work to Begin Date:  7/01/09   Time:  8:00 AM 

State: OR            County: CLACKAMAS               Place: SANDY 
Address:             Street: SHELLEY AVE 
Nearest Intersecting Street: PROCTOR 

Twp: 2S    Rng: 4E    Sect-Qtr: 13 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.3978475Lon: -122.2624589SE Lat: 45.3967869Lon: -122.2607054 

Type of Work: TRAFFIC SIGNAL EXC 
Location of Work: MARK ENTIRE ROW ON BOTH STREETS AT ABV INTER. 

Remarks:  
:  

Company     : NORTHSTAR ELECTRICAL             Best Time:   
Contact Name: JESSE CULP                       Phone: (503)612-0840 
Email Address:   
Alt. Contact: DAVE                             Phone:  
Contact Fax :  
Work Being Done For: CITY OF SANDY 
Additional Members:  
GTC16      NWN01      ODOTRE01   PGE01      SANDY01 
