
Ticket No:  9128876               48 HOUR NOTICE 
Send To: PPL15      Seq No:   10  Map Ref:  

Transmit      Date:  6/29/09   Time:  3:26 PM    Op: ornick 
Original Call Date:  6/29/09   Time:  3:25 PM    Op: ornick 
Work to Begin Date:  7/01/09   Time:  3:30 PM 

State: OR            County: MARION                  Place: STAYTON 
Address:   1433      Street: WESTERN AVE 
Nearest Intersecting Street: WESTOWN DR 

Twp: 9S    Rng: 1W    Sect-Qtr: 9-NE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 44.8078211Lon: -122.8107575SE Lat: 44.8067746Lon: -122.8071201 

Type of Work: INSTALL FIBER OPTIC SRVC 
Location of Work: ADD IS APX 400FT E OF ABV INTER ON N SIDE. MARK ENTIRE PROP
: AT ABV ADD. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : VANDORN ENTERPRISES              Best Time:   
Contact Name: RICHARD DELK                     Phone: (503)769-4504 
Email Address:   
Alt. Contact: KEN'S CELL                       Phone: (503)871-4208 
Contact Fax :  
Work Being Done For: STAYTON TELE 
Additional Members:  
COFS01     FALCON02   NSC01      NWN01      SCT01 
