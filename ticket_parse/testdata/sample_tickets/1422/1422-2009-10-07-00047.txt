From: IRTHNet  At: 10/07/09 08:06 AM  Seq No: 46
Facility: Electric Primary

SCEJZ40 3 PUPS Voice 10/07/2009 8:01:56 AM 0910070075 Update

Ticket Number: 0910070075
Old Ticket Number: 0909160631
Created By: RGO
Seq Number: 3

Created Date: 10/07/2009 8:01:53 AM
Work Date/Time: 10/12/2009 8:15:46 AM
Update: 10/28/2009 Good Through: 11/02/2009

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 700
Street: SALUDA AVE
Inters St: BLOSSOM STREET
Subd: 

Type of Work: SITE WORK
Duration: APPROX:2 MO'S

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: LADD CORPORATION

Remarks/Instructions: MARK ENTIRE PROPERTY                                    
                                                                              
THIS A NEW WAL-GREEN THAT IS UNDER CONSTRUCTION                               
                                                                              

Caller Information: 
Name: LINDA HUTTO                           LADD CORPORATION                      
Address: PO BOX 2634
City: W COLUMBIA State: SC Zip: 29170
Phone: (803) 739-4483 Ext: N/A Type: Business
Fax: (803) 568-2197 Caller Email: 

Contact Information:
Contact:LINDA HUTTO Email: 
Call Back: Fax: (803) 568-2197

Grids: 
Lat/Long: 33.9980921107669, -81.0176895378112
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZB45 COC82 QWC42 SCEJZ40 SCG02 TWCZ40 USC42               


Map Link: (NEEDS DEVELOPMENT)