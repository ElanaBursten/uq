


-----Original Message-----
From: tickets@sc1pups.org [mailto:tickets@sc1pups.org]
Sent: Friday, April 26, 2002 2:10 PM
To: Avis Johnson
Subject: Notice #2138035 (Ref #5004566)


SOUTH CAROLINA PUPS NOTICE FOR TWJZ54
=========================
===========
Notice#: 2138035                        Create Date/Time: 4/26/02 02:09 
PM      
Seq#: 9                                 Work Date/Time: 5/1/02 02:15 PM  
       
Update#: 0                              Good Through: 5/15/02 11:59 PM   
       
Notice Type: Normal                     Must Update By: 5/10/02 11:59 PM 
       

EXCAVATION ADDRESS
===================
Address: 2270 SUNSET CIR                                                 
     
Intersecting Street: DOBYS BRIDGE RD                                     
     
City: FT MILL                                                            
     
County: York                                                             
     
State: SC                                                                
     

DESCRIPTION OF WORK
===================
MARK ENTIRE PROPERTY                                                     
     

Type of Work: INGROUND POOL                                              
     
Work Duration: 3Weeks                   Boring: N    Drilling: N  
Blasting: N  
Site Contact:                           Site Contact Phone:              
       

CALLER INFORMATION                      
==================                   
   
ROBERT FERGIONE                         
POOL & SPA TECH OF CHARLOTTE            
(704) 544-0424      (704) 544-0424      
13917 BALLANTYNE MEADOWS DR             
                                        
CHARLOTTE, NC  28277                    

REMARKS/HISTORY
===============
Normal
4/26/02 2:10:55 PM
Kathleen Walker


MEMBERS INVOLVED
================
FMT20    TWJZ54    YCWS68    YEC42    YOR56    
 





-----Original Message-----
From: tickets@sc1pups.org [mailto:tickets@sc1pups.org]
Sent: Friday, April 26, 2002 3:37 PM
To: Avis Johnson
Subject: Notice #2138199 (Ref #5005736)


SOUTH CAROLINA PUPS NOTICE FOR TWJZ54
=========================
===========
Notice#: 2138199                        Create Date/Time: 4/26/02 03:36 
PM      
Seq#: 10                                Work Date/Time: 5/1/02 03:30 PM  
       
Update#: 0                              Good Through: 5/15/02 11:59 PM   
       
Notice Type: Normal                     Must Update By: 5/10/02 11:59 PM 
       

EXCAVATION ADDRESS
===================
Address:  LYNNWOOD FARMS DR                                              
     
Intersecting Street: DOBY BRIDGE RD                                      
     
City: FT MILL                                                            
     
County: York                                                             
     
State: SC                                                                
     

DESCRIPTION OF WORK
===================
LYNNWOOD FARMS SUBD AT THE ENTRANCE // MARK BOTH SIDES OF LYNNWOOD FARMS 
DR 
OFF DOBY BRIDGE RD FOR 200FT                                             
     

Type of Work: FENCE INSTALL                                              
     
Work Duration: 3Days                    Boring: N    Drilling: Y  
Blasting: N  
Site Contact:                           Site Contact Phone:              
       

CALLER INFORMATION                      
==================                   
   
TERRY FELTON                            
ALLISON SUNBELT FENCE                   
(704) 334-2451      (704) 334-2451      
2908 MONROE RD                          
                                        
CHARLOTTE, SC  28205                    

REMARKS/HISTORY
===============
Normal
4/26/02 3:37:50 PM
Jessica Bozard


MEMBERS INVOLVED
================
FMT20    LAN20    RHT33    TWJZ54    YCWS68    YEC42    YOR56    
 





-----Original Message-----
From: tickets@sc1pups.org [mailto:tickets@sc1pups.org]
Sent: Friday, April 26, 2002 3:38 PM
To: Avis Johnson
Subject: Notice #2138225 (Ref #5005748)


SOUTH CAROLINA PUPS NOTICE FOR TWJZ54
=========================
===========
Notice#: 2138225                        Create Date/Time: 4/26/02 03:37 
PM      
Seq#: 11                                Work Date/Time: 5/1/02 03:45 PM  
       
Update#: 0                              Good Through: 5/15/02 11:59 PM   
       
Notice Type: Normal                     Must Update By: 5/10/02 11:59 PM 
       

EXCAVATION ADDRESS
===================
Address: 137 RIVERWOOD DR                                                
     
Intersecting Street: HWY 21                                              
     
City: FORT MILL                                                          
     
County: York                                                             
     
State: SC                                                                
     

DESCRIPTION OF WORK
===================
MARK BOTH SIDES AND THE REAR OF THE PROPERTY                             
     

Type of Work: INSTALLING A FENCE                                         
     
Work Duration: 2Days                    Boring: Y    Drilling: N  
Blasting: N  
Site Contact:                           Site Contact Phone:              
       

CALLER INFORMATION                      
==================                   
   
Charlie Johnson                         
Handyman Fence Co.                      
(803) 328-1267      (803) 328-1267      
823 Caraway Dr.                         
                                        
Rock Hill, SC  29732                    

REMARKS/HISTORY
===============
Normal
4/26/02 3:38:38 PM
Clayton Lear


MEMBERS INVOLVED
================
ATT09    DPZ72F    FMT20    TWJZ54    YCWS68    YEC42    YOR56    
 





-----Original Message-----
From: tickets@sc1pups.org [mailto:tickets@sc1pups.org]
Sent: Monday, April 29, 2002 10:55 AM
To: Avis Johnson
Subject: Notice #2004833 (Ref #5012144)


SOUTH CAROLINA PUPS NOTICE FOR TWJZ54
=========================
===========
Notice#: 2004833                        Create Date/Time: 4/29/02 10:54 
AM      
Seq#: 12                                Work Date/Time: 5/2/02 11:00 AM  
       
Update#: 1                              Good Through: 5/16/02 11:59 PM   
       
Notice Type: Normal                     Must Update By: 5/13/02 11:59 PM 
       

EXCAVATION ADDRESS
===================
Address:  Grace Street                                                   
     
Intersecting Street: Tom Hall St.                                        
     
City: Fort Mill                                                          
     
County: York                                                             
     
State: SC                                                                
     

DESCRIPTION OF WORK
===================
Fort Mill, SC, Site Zip: , Lot #: , City Limits: Inside, What Direction: 

None, Directions: Hwy. 160 (Tom Hall Street) toward Lancaster Co. Line, 
Mark: 
Both sides of road from Int. w/ Tom Hall Street, Intersecting within 1/4 

mile?: Yes, Distance > 1250?: No, Total Distance: , Subdivision: , 
Closest 
Roads: Banks St. & Doby Bridge                                           
     

Type of Work: Road Widening                                              
     
Work Duration: 10Months                 Boring: N    Drilling: N  
Blasting: N  
Site Contact: Chad Malone               Site Contact Phone:              
       

CALLER INFORMATION                      
==================                   
   
NANCY BOVENDER                          
BOGGS PAVING INC                        
(704) 289-8482      (704) 289-8482      
SAME                                    
                                        
SAME, SC  00000                         

REMARKS/HISTORY
===============
UPDATED
4/29/02 10:55:19 AM
Work Date: 04/18/02
Work Time: 08:45 AM
UPDATED BY NANCY 
Jessica Bozard
UPDATED
4/15/02 8:43:03 AM
Work Date: 04/04/02
Work Time: 09:30 AM

Rickie Brown
UPDATED
4/1/02 9:20:57 AM
Work Date: 03/21/02
Work Time: 10:00 AM
UPDATED BY NANCY BOVENDER
Amanda Slater
UPDATED
3/18/02 9:59:03 AM
Work Date: 03/07/02
Work Time: 09:15 AM

Kathleen Walker
UPDATED
3/4/02 9:07:50 AM
Work Date: 02/21/02
Work Time: 12:00 PM

Autumn Coker
UPDATED
2/18/02 11:49:03 AM
Work Date: 02/07/02
Work Time: 02:15 PM

Joy McCartha
UPDATED
2/4/02 2:03:09 PM
Work Date: 01/24/02
Work Time: 04:00 PM
UPDATED BY NANCY BOVENDER
Lynn Hudson
UPDATED
1/21/02 3:53:11 PM
Work Date: 01/10/02
Work Time: 09:30 AM

Clayton Lear
Normal
1/7/02 9:16:40 AM
Jessica Dassau


MEMBERS INVOLVED
================
DPZ72F    FMT20    QWC42    TWJZ54    YCWS68    YEC42    YOR56    
 


