
GSUPLS091102440-00	JC	2009/04/20 18:46:03	00954

 New Jersey One Call System        SEQUENCE NUMBER 1033   CDC = GPC

Transmit:  Date: 04/20/09   At: 1844

*** R O U T I N E         *** Request No.: 091102440

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CTV|UTQ/ AE1=/ATLNTC ELEC|UTQ/ 
    NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ GPC=/JCP&L      |UTQ/ 

Start Date/Time:    04/25/09   At 0800   Expiration Date: 06/23/09

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               301 LAUREL BLVD
   Nearest Intersection: SPRUCE RD 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTL FENCE
   Extent of Work: 1 AREA(S) MARKED IN WHITE                DEPTH: 3FT
     AREA(S) MARKED IN WHITE LOCATED: RIGHT SIDE OF PROPERTY
   Remarks:

   Working For:  ROBERT GEARHEART
   Address:      301 LAUREL BLVD
   City:         LANOKA HARBOR, NJ  08734
   Phone:        732-600-8294

Excavator Information:
   Caller:       ROBERT GEARHEART        
   Phone:        732-600-8294            

   Excavator:    ROBERT GEARHEART
   Address:      301 LAUREL BLVD
   City:         LANOKA HARBOR, NJ  08734
   Phone:        732-600-8294            Fax:  
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 06:46PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102440        0954  GSUPLS091102440-00   Regular Notice   2009/04/25 08:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
