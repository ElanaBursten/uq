
GSUPLS091102639-00	JC	2009/04/20 20:57:48	01007

 New Jersey One Call System        SEQUENCE NUMBER 1088   CDC = GPC

Transmit:  Date: 04/20/09   At: 2056

*** R O U T I N E         *** Request No.: 091102639

Operators Notified:
    BGT=/BRNEGAT TWP W&S/ AE1=/ATLNTC ELEC|UTQ/ NJN=/NJ NATL GAS|UTQ/ 
    BAN=/VERIZON   |ECSM/ GPC=/JCP&L      |UTQ/ CCC=/COMCST-TMSR|UTQ/ 

Start Date/Time:    04/24/09   At 0900   Expiration Date: 06/23/09

Location Information:
   County: OCEAN     Municipality: BARNEGAT
   Subdivision/Community: 
   Street:               13 4TH ST
   Nearest Intersection: W BAY AVE 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        UPGRADING CATV
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 2FT
   Remarks:

   Working For:  COMCAST
   Address:      830 HWY 37
   City:         TOMS RIVER, NJ  08755
   Phone:        732-281-3740

Excavator Information:
   Caller:       JOE VERDERROSA          
   Phone:        732-581-0133            

   Excavator:    J V INSTALLATIONS INC
   Address:      54 11TH ST
   City:         TOMS RIVER, NJ  08753
   Phone:        732-581-0133            Fax:  
   Cellular:     732-581-0133
   Email:        J&EUNDERGROUND@AOL.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 08:57PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102639        1007  GSUPLS091102639-00   Regular Notice   2009/04/24 09:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
