
New Jersey One Call System        SEQUENCE NUMBER 0011   CDC = GPU

Transmit:  Date: 04/20/09   At: 2219

*** E M E R G E N C Y     *** Request No.: 091102683

Operators Notified:
    UNI=/EMBARQ     |UTQ/ CC6=/COMCASTNWNJ|UTQ/ EG2=/ELIZABTHTWN GAS/ 
    GPU=/JCP&L      |UTQ/ BAN=/VERIZON   |ECSM/ 

Start Date/Time:    04/20/09   At 2211   Expiration Date: 

Location Information:
   County: WARREN     Municipality: WASHINGTON TWP
   Subdivision/Community: 
   Street:               162 MINE HILL RD
   Nearest Intersection: PINEHURST 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        BROKEN POLE
   Extent of Work: 10FT RADIUS OF POLE...                   DEPTH: 6FT
     #B35 LOCATED 3FT BEHIND CURB
   Remarks:
     MOTOR VEHICLE ACCIDENT

   Working For:  VERIZON
   Address:      131 BEAVERBROOK RD
   City:         LINCOLN PARK, NJ  07035
   Phone:        973-694-6471

Excavator Information:
   Caller:       RONALD MELILLO          
   Phone:        973-694-6471            

   Excavator:    VERIZON
   Address:      131 BEAVERBROOK RD
   City:         LINCOLN PARK, NJ  07035
   Phone:        973-694-6471            Fax:  973-694-4879
   Cellular:     201-259-1532
   Email:        
End Request

