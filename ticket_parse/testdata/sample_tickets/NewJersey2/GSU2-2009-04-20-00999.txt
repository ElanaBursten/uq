
GSUPLS091102509-00	JC	2009/04/20 19:25:18	00980

 New Jersey One Call System        SEQUENCE NUMBER 1060   CDC = GPU

Transmit:  Date: 04/20/09   At: 1924

*** R O U T I N E         *** Request No.: 091102509

Operators Notified:
    UNI=/EMBARQ     |UTQ/ CC6=/COMCASTNWNJ|UTQ/ EG2=/ELIZABTHTWN GAS/ 
    GPU=/JCP&L      |UTQ/ 

Start Date/Time:    04/24/09   At 0800   Expiration Date: 06/23/09

Location Information:
   County: WARREN     Municipality: WHITE
   Subdivision/Community: COUNTRY VIEW VILLAGE
   Street:               1 SUNSET CT
   Nearest Intersection: LAMPLIGHTER CIR 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPLACE CATV
   Extent of Work: 1 AREA(S) MARKED IN WHITE                DEPTH: 24IN
     AREA(S) MARKED IN WHITE LOCATED: LEFT SIDE OF PROPERTY
   Remarks:

   Working For:  COMCAST
   Address:      155 PORT MURRAY RD
   City:         PROT MURRAY, NJ  07865
   Phone:        908-689-0885

Excavator Information:
   Caller:       DIANE FRAZEE            
   Phone:        908-362-5619            

   Excavator:    FRAZEE CABLE SERVICES INC.
   Address:      7 CHERRY LN
   City:         BLAIRSTOWN, NJ  07825
   Phone:        908-362-5619            Fax:  908-362-1288
   Cellular:     973-417-0945
   Email:        SWEETP918@YAHOO.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 07:25PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102509        0980  GSUPLS091102509-00   Regular Notice   2009/04/24 08:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
