
GSUPLS091102435-00	JC	2009/04/20 18:46:03	00951

 New Jersey One Call System        SEQUENCE NUMBER 1030   CDC = GP9

Transmit:  Date: 04/20/09   At: 1844

*** R O U T I N E         *** Request No.: 091102435

Operators Notified:
    CC4=/CMCST-MNMTH|UTQ/ NJN=/NJ NATL GAS|UTQ/ GP9=/JCP&L      |UTQ/ 
    BAN=/VERIZON   |ECSM/ NJ7=/NJ AMER WTR    / 

Start Date/Time:    04/24/09   At 0700   Expiration Date: 06/23/09

Location Information:
   County: MONMOUTH     Municipality: MIDDLETOWN
   Subdivision/Community: 
   Street:               858 CHURCH LN
   Nearest Intersection: WALLACE RD 
   Other Intersection:   RED HILL RD
   Lat/Long: 
   Type of Work :        INSTALL IRRIGATION
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 1 FOOT
   Remarks:

   Working For:  MR RUSSOTO
   Address:      858 CHURCH LN
   City:         MIDDLETOWN, NJ  07748
   Phone:        908-461-8188

Excavator Information:
   Caller:       DENISE CONOVER          
   Phone:        732-367-4408            

   Excavator:    NJ LAWN & IRRIGATION
   Address:      P.O. BOX 610
   City:         JACKSON, NJ  08527
   Phone:        732-367-4408            Fax:  732-367-4622
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 06:46PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102435        0951  GSUPLS091102435-00   Regular Notice   2009/04/24 07:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
