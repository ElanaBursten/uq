
GSUPLS091102559-00	JC	2009/04/20 19:58:21	00986

 New Jersey One Call System        SEQUENCE NUMBER 1067   CDC = GPE

Transmit:  Date: 04/20/09   At: 1957

*** R O U T I N E         *** Request No.: 091102559

Operators Notified:
    CAM=/CABLVSN-MON|UTQ/ HOW=/HOWELL TWP SWR / PWC=/PARKWAY WTR CO / 
    NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ GPE=/JCP&L      |UTQ/ 
    NJ4=/NJ AMER WTR    / BIT=/BRICK TWP MUA  / 

Start Date/Time:    04/24/09   At 0200   Expiration Date: 06/23/09

Location Information:
   County: MONMOUTH     Municipality: HOWELL
   Subdivision/Community: 
   Street:               241 MOSES MILCH DR
   Nearest Intersection: RAMTOWN GREENVILLE RD 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        TERMITE TREATMENT
   Extent of Work: 10FT PERIMETER OF FOUNDATION OF...       DEPTH: 4FT
     BLDG/STRUCTURE
   Remarks:

   Working For:  JASON UMLAH
   Address:      241 MOSES MILCH DR
   City:         HOWELL, NJ  07731
   Phone:        732-840-3958

Excavator Information:
   Caller:       GLEN ALICCHIO           
   Phone:        732-493-6190            

   Excavator:    TERMINIX INTERNATIONAL
   Address:      1750 BRIELLE AVE
   City:         OCEAN TWP, NJ  07712
   Phone:        732-493-6190            Fax:  732-493-1360
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 07:58PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102559        0986  GSUPLS091102559-00   Regular Notice   2009/04/24 02:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
