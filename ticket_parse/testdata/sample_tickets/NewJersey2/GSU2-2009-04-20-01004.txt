
GSUPLS091102565-00	JC	2009/04/20 19:58:21	00989

 New Jersey One Call System        SEQUENCE NUMBER 1070   CDC = GPS

Transmit:  Date: 04/20/09   At: 1957

*** R O U T I N E         *** Request No.: 091102565

Operators Notified:
    CC3=/COMCST-CENT|UTQ/ MOT=/MONROE TWP MUA / P29=/PSE&G GAS DIV  / 
    NN3=/NJNG CENTRAL   / BAN=/VERIZON   |ECSM/ GPS=/JCP&L      |UTQ/ 

Start Date/Time:    04/24/09   At 0900   Expiration Date: 06/23/09

Location Information:
   County: MIDDLESEX     Municipality: MONROE
   Subdivision/Community: 
   Street:               4 GARVEY DR
   Nearest Intersection: SPOTSWOOD GRAVEL HILL RD 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTL INVISIBLE FENCE
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 4IN
   Remarks:

   Working For:  MICHELLE PEARSON
   Address:      4 GARVEY DR
   City:         MONROE, NJ  08831
   Phone:        732-605-1303

Excavator Information:
   Caller:       NICK BUSH               
   Phone:        908-369-6945            

   Excavator:    DOG GUARD OF CENTRAL JERSEY
   Address:      17 LORIEN PL
   City:         HILLSBOROUGH, NJ  08844
   Phone:        908-369-6945            Fax:  908-369-7664
   Cellular:     
   Email:        NBUSH@DOGGUARD.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 07:58PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102565        0989  GSUPLS091102565-00   Regular Notice   2009/04/24 09:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
