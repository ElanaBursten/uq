
GSUPLS091102683-00	JC	2009/04/20 22:21:51	01014

 New Jersey One Call System        SEQUENCE NUMBER 1102   CDC = GPU
     
Transmit:  Date: 04/20/09   At: 2220

*** E M E R G E N C Y     *** Request No.: 091102683

Operators Notified:
    UNI=/EMBARQ     |UTQ/ CC6=/COMCASTNWNJ|UTQ/ EG2=/ELIZABTHTWN GAS/ 
    GPU=/JCP&L      |UTQ/ BAN=/VERIZON   |ECSM/ 

Start Date/Time:    04/20/09   At 2211   Expiration Date: 

Location Information:
   County: WARREN     Municipality: WASHINGTON TWP
   Subdivision/Community: 
   Street:               162 MINE HILL RD
   Nearest Intersection: PINEHURST 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        BROKEN POLE
   Extent of Work: 10FT RADIUS OF POLE...                   DEPTH: 6FT
     #B35 LOCATED 3FT BEHIND CURB
   Remarks:
     MOTOR VEHICLE ACCIDENT

   Working For:  VERIZON
   Address:      131 BEAVERBROOK RD
   City:         LINCOLN PARK, NJ  07035
   Phone:        973-694-6471

Excavator Information:
   Caller:       RONALD MELILLO          
   Phone:        973-694-6471            

   Excavator:    VERIZON
   Address:      131 BEAVERBROOK RD
   City:         LINCOLN PARK, NJ  07035
   Phone:        973-694-6471            Fax:  973-694-4879
   Cellular:     201-259-1532
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 10:21PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102683        1014  GSUPLS091102683-00   Emergency (Less  2009/04/20 10:11PM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
