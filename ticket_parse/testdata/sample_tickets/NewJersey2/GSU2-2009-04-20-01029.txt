
GSUPLS091102638-00	JC	2009/04/20 20:57:48	01006

 New Jersey One Call System        SEQUENCE NUMBER 1087   CDC = GP9

Transmit:  Date: 04/20/09   At: 2056

*** R O U T I N E         *** Request No.: 091102638

Operators Notified:
    CC4=/CMCST-MNMTH|UTQ/ NJN=/NJ NATL GAS|UTQ/ GP9=/JCP&L      |UTQ/ 
    BAN=/VERIZON   |ECSM/ NJ7=/NJ AMER WTR    / TIN=/TINTON FALLS   / 

Start Date/Time:    04/24/09   At 0900   Expiration Date: 06/23/09

Location Information:
   County: MONMOUTH     Municipality: TINTON FALLS
   Subdivision/Community: 
   Street:               167 HANCE AVE
   Nearest Intersection: CHERRY ST 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTL INGROUND BASKETBALL HOOP
   Extent of Work: 1 AREA(S) MARKED IN WHITE                DEPTH: 4FT
     AREA(S) MARKED IN WHITE LOCATED: FRONT OF PROPERTY LEFT & RIGHT
     SIDES OF PROPERTY
   Remarks:

   Working For:  LISA SEARIGHT
   Address:      167 HANCE AVE
   City:         TINTON FALLS, NJ  07724
   Phone:        732-539-5993

Excavator Information:
   Caller:       KEITH KOFOED            
   Phone:        630-893-7200            

   Excavator:    GO CONFIGURE
   Address:      1001 WARRENVILLE RD STE 110
   City:         LISLE, IL  60532
   Phone:        630-893-7200            Fax:  630-893-8510
   Cellular:     
   Email:        TYLERS@DONTSHOOT.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 08:57PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102638        1006  GSUPLS091102638-00   Regular Notice   2009/04/24 09:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
