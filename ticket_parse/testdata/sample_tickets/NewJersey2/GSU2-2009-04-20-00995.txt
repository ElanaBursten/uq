
GSUPLS091102472-00	JC	2009/04/20 19:03:01	00975

 New Jersey One Call System        SEQUENCE NUMBER 1055   CDC = GP9

Transmit:  Date: 04/20/09   At: 1901

*** R O U T I N E         *** Request No.: 091102472

Operators Notified:
    CC4=/CMCST-MNMTH|UTQ/ NJN=/NJ NATL GAS|UTQ/ GP9=/JCP&L      |UTQ/ 
    BAN=/VERIZON   |ECSM/ NJ7=/NJ AMER WTR    / 

Start Date/Time:    04/24/09   At 0900   Expiration Date: 06/23/09

Location Information:
   County: MONMOUTH     Municipality: OCEANPORT
   Subdivision/Community: KIMBERLY WOODS
   Street:               106 SURREY CT
   Nearest Intersection: KIMBERLY WAY 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPAIR CABLE FAULT
   Extent of Work: CURB TO CURB                             DEPTH: 4FT
     30FT RADIUS OF TRANSFORMER #80 LOCATED 10FT BEHIND CURB
   Remarks:

   Working For:  JCP&L
   Address:      201 MONMOUTH RD
   City:         LONG BRANCH, NJ  07764
   Phone:        732-923-2310

Excavator Information:
   Caller:       WILL WRIGHT             
   Phone:        732-923-2310            

   Excavator:    JCP&L
   Address:      201 MONMOUTH RD
   City:         LONG BRANCH, NJ  07764
   Phone:        732-923-2310            Fax:  732-923-2323
   Cellular:     732-684-1468
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 07:03PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102472        0975  GSUPLS091102472-00   Regular Notice   2009/04/24 09:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
