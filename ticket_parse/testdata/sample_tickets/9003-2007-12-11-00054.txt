

SBF20  09453 CALL SUNSHINE 12/11/07 15:30:05ET 345709157-000  STREET
Ticket : 345709157 Rev:000 Taken: 12/11/07 15:29ET

State: FL Cnty: ORANGE GeoPlace: ORLANDO
CallerPlace: ORLANDO
Subdivision: COLONIALTOWN

Address : 1505 to 1505
Street  : PINECREST PL
Cross 1 : MILLS AVE
Within 1/4 mile: Y

Locat: ENTIRE REAR OF THE PROPERTY
:
Remarks : *** LOOKUP BY ADDRESS ***
:
Grids   : 2833C8121B   2833C8121A  

Work date: 12/13/07 Time: 23:59ET  Hrs notc: 056 Category: 3 Duration: 03 DAYS
Due Date : 12/13/07 Time: 23:59ET  Exp Date : 01/10/08 Time: 23:59ET
Work type: UNDERGROUND CONSTRUCTION SWIMMING POOL  Boring: N  White-lined: N
Ug/Oh/Both: U  Machinery: Y  Depth: 7 FT  Permits: Y  NYA
Done for : HOMEOWNER

Company : DREAM POOLS  Type: CONT
Co addr : PO BOX 2754
City    : WINDERMERE State: FL Zip: 34786
Caller  : AMANDA GWYNN Phone: 407-354-2549
BestTime: 9-12
Fax     : 407-895-0588
Email   : MYDREAMPOOL@AOL.COM

Submitted: 12/11/07 15:29ET Oper: AMG Chan: WEB
Mbrs :  COO600 CVCFTV FPC322 MCIU01 OUC553 OUC582 PGSORL SBF20  LS1104 ST1259

