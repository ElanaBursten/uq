
GSUPLS080320395-00	JC	2008/02/01 11:08:28	00147

 New Jersey One Call System        SEQUENCE NUMBER 0159   CDC = GPP

Transmit:  Date: 02/01/08   At: 1106

*** R O U T I N E         *** Request No.: 080320395

Operators Notified:
    WAT=/WALL TWP WTR&SW/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ 
    GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/07/08   At 0900   Expiration Date: 04/09/08

Location Information:
   County: MONMOUTH     Municipality: WALL
   Subdivision/Community: 
   Street:               1414 WILLIAMS DR
   Nearest Intersection: ALICIA DR 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTL FENCE
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 4FT
   Remarks:

   Working For:  TODD CARMER
   Address:      1414 WILLIAMS DR
   City:         WALL, NJ  07719
   Phone:        732-963-7607

Excavator Information:
   Caller:       PAUL CROOKS             
   Phone:        732-938-4355            

   Excavator:    TAYLOR FENCE CO. INC.
   Address:      P.O. BOX 126
   City:         RED BANK, NJ  07701
   Phone:        732-938-4355            Fax:  732-938-5671
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 11:08AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320395        0147  GSUPLS080320395-00   Regular Notice   2008/02/07 09:00AM

