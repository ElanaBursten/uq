
GSUPLS080320016-00	JC	2008/02/01 07:06:58	00006

 New Jersey One Call System        SEQUENCE NUMBER 0007   CDC = GPP

Transmit:  Date: 02/01/08   At: 0705

*** R O U T I N E         *** Request No.: 080320016

Operators Notified:
    SOM=/S MONMOUTH SWR / NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ 
    BSB=/LAKE COMO BORO / GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/07/08   At 0800   Expiration Date: 04/09/08

Location Information:
   County: MONMOUTH     Municipality: LAKE COMO
   Subdivision/Community: 
   Street:               300 NORTH BLVD
   Nearest Intersection: B ST 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        EXCAV CRAWLSPACE
   Extent of Work: CURB TO CURB,CURB TO 40FT BEHIND CURB    DEPTH: 3 FT
   Remarks:

   Working For:  JAS JR.
   Address:      PO BOX 50
   City:         SPRING LAKE HEIGHTS, NJ  07762
   Phone:        732-995-6024

Excavator Information:
   Caller:       ROBERT LAWRENCE         
   Phone:        732-280-0254            

   Excavator:    R J LAWRENCE CONSTRUCTION
   Address:      1401 BAY PLAZA
   City:         WALL, NJ  07719
   Phone:        732-280-0254            Fax:  732-280-0254
   Cellular:     732-859-6968
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 07:06AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320016        0006  GSUPLS080320016-00   Regular Notice   2008/02/07 08:00AM

