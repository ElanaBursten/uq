
GSUPLS080320099-00	JC	2008/02/01 08:28:08	00032

 New Jersey One Call System        SEQUENCE NUMBER 0034   CDC = GPP

Transmit:  Date: 02/01/08   At: 0827

*** R O U T I N E         *** Request No.: 080320099

Operators Notified:
    SOM=/S MONMOUTH SWR / CAM=/CABLVSN-MON|UTQ/ SLB=/SPRINGLAKE BORO/ 
    NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ NJ4=/NJ AMER WTR    / 
    GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/07/08   At 0800   Expiration Date: 04/09/08

Location Information:
   County: MONMOUTH     Municipality: SPRING LAKE
   Subdivision/Community: 
   Street:               4 SALEM AVE
   Nearest Intersection: OCEAN AVE 
   Other Intersection:   1ST AVE
   Lat/Long: 
   Type of Work :        INSTL YARD HYDRANT
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 10 FT
   Remarks:
     BLOCK 31 LOT#1.02 *POSTED

   Working For:  RICK HALL BUILDERS
   Address:      77 ROYAL DR
   City:         BRICK, NJ  08723
   Phone:        732-920-8893

Excavator Information:
   Caller:       GINA TEMPERIO           
   Phone:        732-270-6282            

   Excavator:    PRIMAK PLUMBING & HEATING
   Address:      215 MAPLE AVE
   City:         TOMS RIVER, NJ  08753
   Phone:        732-270-6282            Fax:  732-270-6228
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 08:28AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320099        0032  GSUPLS080320099-00   Regular Notice   2008/02/07 08:00AM

