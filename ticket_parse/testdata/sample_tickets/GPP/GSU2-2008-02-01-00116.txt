
GSUPLS080320273-00	JC	2008/02/01 10:19:31	00095

 New Jersey One Call System        SEQUENCE NUMBER 0095   CDC = GPP

Transmit:  Date: 02/01/08   At: 1018

*** R O U T I N E         *** Request No.: 080320273

Operators Notified:
    WAT=/WALL TWP WTR&SW/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ 
    GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/07/08   At 0700   Expiration Date: 04/09/08

Location Information:
   County: MONMOUTH     Municipality: WALL
   Subdivision/Community: 
   Street:               5004 STATE ROUTE 33
   Nearest Intersection: CATHERINE ST 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        SOIL BORINGS
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 50FT
   Remarks:

   Working For:  RENOVA
   Address:      256 MORRIS BLVD
   City:         MANAHAWKIN, NJ  08050
   Phone:        609-207-7000

Excavator Information:
   Caller:       TERRI FURRANTE          
   Phone:        856-858-8584            

   Excavator:    ENVIROPROBE SERVICE INC.
   Address:      221 HADDON AVE
   City:         WESTMONT, NJ  08108
   Phone:        856-858-8584            Fax:  856-858-0823
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 10:19AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320273        0095  GSUPLS080320273-00   Regular Notice   2008/02/07 07:00AM

