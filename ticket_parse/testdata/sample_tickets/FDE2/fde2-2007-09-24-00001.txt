
UTIL14 00001 VUPSb 09/24/07 07:26:10 B726700067-00B          NORMAL

Ticket No:  B726700067-00B                        NEW  GRID NORM LREQ
Transmit        Date: 09/24/07   Time: 07:26 AM   Op: WPLUCAS
Call            Date: 09/24/07   Time: 07:19 AM
Due By          Date: 09/27/07   Time: 07:00 AM
Update By       Date: 10/12/07   Time: 11:59 PM
Expires         Date: 10/17/07   Time: 07:00 AM
Old Tkt No: B726700067
Original Call   Date: 09/24/07   Time: 07:19 AM   Op: WPLUCAS

City/Co:ACCOMACK              Place:ONANCOCK                            State:VA
Address:     23586            Street: HOLLY COVE ROAD
Cross 1:     BAILEY NECK DRIVE

Type of Work:   ELECTRIC PRIMARY - REPAIR OR REPLACE
Work Done For:  ANEC
Excavation area:LOCATE FROM ANEC POLE 3-197A-22 GOING SOUTH ALONG HOLLY COVE
                ROAD TO ANEC TRANSFORMER 3-197A-22-T1 BEHIND CHICKEN HOUSES
Instructions:

Whitelined: N   Blasting: N   Boring: Y

Company:        LUCAS UNDERGROUND UTILITIES INC           Type: CONT
Co. Address:    28193 PHILLIPS DR  First Time: N
City:           MELFA  State:VA  Zip:23410
Company Phone:  757-787-3510
Contact Name:   PATTI LUCAS                 Contact Phone:757-787-3510
Email:          lucasuu@verizon.net

Mapbook:
Grids:    3743D7547A-04  3743D7547A-13  3743D7547A-14  3743D7547A-22
Grids:    3743D7547A-23  3743D7547A-31  3743D7547A-32  3743D7547A-41

Members:
ANE901 = A & N ELECTRIC COOPERATIV(ANE) VZN905 = VERIZON (VZN)

Seq No:   1 B

