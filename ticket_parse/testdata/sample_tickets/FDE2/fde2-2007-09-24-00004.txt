
UTIL14 00004 VUPSb 09/24/07 09:34:02 B726700516-00B          NORMAL

Ticket No:  B726700516-00B                        NEW  GRID NORM LREQ
Transmit        Date: 09/24/07   Time: 09:34 AM   Op: WDKONKEL
Call            Date: 09/24/07   Time: 09:28 AM
Due By          Date: 09/27/07   Time: 07:00 AM
Update By       Date: 10/12/07   Time: 11:59 PM
Expires         Date: 10/17/07   Time: 07:00 AM
Old Tkt No: B726700516
Original Call   Date: 09/24/07   Time: 09:28 AM   Op: WDKONKEL

City/Co:ACCOMACK              Place:                                    State:VA
Address:                      Street: WATTS BAY LANE
Cross 1:     WATTS BAY DR

Type of Work:   SEWER AND WATER - INSTALL
Work Done For:  WILLIAM MOORE
Excavation area:MARK FRONT OF PROPERTY ALSO IN BACK OF HOLE LOCATION
Instructions:   TURN EAST OFF OF LANKFORD HIGHWAY IN OAK HALL AT TRAFFIC LIGHT
                ON CHINCOTEGUE RD. GO APPROX 2 MILES, MAKE A RIGHT ON ATLANTIC
                RD. THEN A LEFT ON WATTS BAY DR. WATTS BAY LANE IS ON RIGHT LOT
                AT END ON CUL DE SAC STAKES ARE THERE FOR HOUSE LOCATION.

Whitelined: N   Blasting: N   Boring: N

Company:        BUNDICK WELL & PUMP CO                    Type: CONT
Co. Address:    PO BOX 15  First Time: N
City:           PAINTER  State:VA  Zip:23420
Company Phone:  757-442-5555
Contact Name:   DENISE KONKEL               Contact Phone:757-442-5555
Email:          bundickwellpump@verizon.net

Mapbook:
Grids:    3754A7529C-04  3754A7529D-00  3755D7529C-44  3755D7529D-40

Members:
DPE911 = DELMARVA POWER (DPE)           VZN905 = VERIZON (VZN)

Seq No:   4 B

