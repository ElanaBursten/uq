
UTIL14 00002 VUPSb 09/24/07 09:18:44 B726700450-00B          NORMAL

Ticket No:  B726700450-00B                        NEW  GRID NORM LREQ
Transmit        Date: 09/24/07   Time: 09:18 AM   Op: 1AMF
Call            Date: 09/24/07   Time: 09:12 AM
Due By          Date: 09/27/07   Time: 07:00 AM
Update By       Date: 10/12/07   Time: 11:59 PM
Expires         Date: 10/17/07   Time: 07:00 AM
Old Tkt No: B726700450
Original Call   Date: 09/24/07   Time: 09:12 AM   Op: 1AMF

City/Co:NORTHAMPTON           Place:CAPE CHARLES                        State:VA
Address:     409              Street: TAZEWELL AVE
Cross 1:     PLUM ST

Type of Work:   TANK - INSTALL BURIED TANK
Work Done For:  KENMAR LLC
Excavation area:AREA IS PRE-MARKED WITH RED PAINT;  THE BACK YARD OFF THE ALLEY
                INTO THE PROPERTY
Instructions:

Whitelined: Y   Blasting: N   Boring: N

Company:        FLOYD ENERGY                              Type: CONT
Co. Address:    36310 LANGFORD HWY  First Time: N
City:           BELLE HAVEN  State:VA  Zip:23306
Company Phone:  757-442-2444
Contact Name:   HERB THOM                   Contact Phone:757-710-0248
Contact Fax:    757-442-3330

Mapbook:
Grids:    3716D7600A-10  3716D7600A-11  3716D7600A-20  3716D7600A-21

Members:
BCC101 = BAY CREEK COMMUNICATIONS (BCC) CCH901 = TOWN OF CAPE CHARLES (CCH)
DPE911 = DELMARVA POWER (DPE)           VZN905 = VERIZON (VZN)

Seq No:   2 B

