
UTIL14 00003 VUPSb 09/24/07 09:24:16 B726700471-00B          NORMAL

Ticket No:  B726700471-00B                        NEW  GRID NORM LREQ
Transmit        Date: 09/24/07   Time: 09:24 AM   Op: 1JMW
Call            Date: 09/24/07   Time: 09:17 AM
Due By          Date: 09/27/07   Time: 07:00 AM
Update By       Date: 10/12/07   Time: 11:59 PM
Expires         Date: 10/17/07   Time: 07:00 AM
Old Tkt No: B726700471
Original Call   Date: 09/24/07   Time: 09:17 AM   Op: 1JMW

City/Co:ACCOMACK              Place:MELFA                               State:VA
Address:     29300            Street: LANKFORD HWY
Cross 1:     AIRPORT DR

Type of Work:   CONSTRUCTION - BUILDING
Work Done For:  EASTERN SHORE COMMUNITY COLLEGE
Excavation area:ENTIRE AREA BEHIND THE BACK OF THE BUILDING
Instructions:   GATE IS OPEN FROM 630AM-10PM WEEKDAYS

Whitelined: N   Blasting: N   Boring: N

Company:        MERVIN L BLADES AND SON INC               Type: CONT
Co. Address:    PO DRAWER 149  First Time: N
City:           POCOMOKE CITY  State:MD  Zip:21851
Company Phone:  410-957-3515
Contact Name:   JIM WERT                    Contact Phone:443-783-5448

Mapbook:
Grids:    3737A7544A     3737A7544B     3737A7545D     3738A7544A
Grids:    3738A7544B     3738A7544C     3738A7544D     3738B7544A
Grids:    3738B7544B     3738B7544C     3738C7544A     3738C7544B
Grids:    3738C7544C     3738D7544A     3738D7544B     3738A7545D
Grids:    3738B7545D     3738C7545C     3738C7545D     3738D7545C
Grids:    3738D7545D     3739C7544A     3739C7544B     3739C7544C
Grids:    3739C7544D     3739D7544A     3739D7544B     3739D7544C
Grids:    3739D7544D

Members:
ANE901 = A & N ELECTRIC COOPERATIV(ANE) DPE911 = DELMARVA POWER (DPE)
VZN905 = VERIZON (VZN)

Seq No:   3 B

