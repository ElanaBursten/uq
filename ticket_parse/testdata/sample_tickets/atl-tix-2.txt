ATL01  00284 GAUPC 03/05/02 15:22:01 03052-034-071-000 NORMAL
Underground Notification
Ticket : 03052-034-071 Date: 03/05/02 Time: 15:16 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 135    To:        Name:    EDGEWOOD                       AV
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY..BTH SIDES OF THE RD

Grids    : 3345D8422A   3345C8422A   3345D8423D   3345C8423D
Work type: INSTALL WATER METER
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : UNKNOWN
Done for : UNITED WATER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : ALL BORING
        : *** NEAR STREET ***   COURTLAND ST
        : *** WILL BORE SIDEWALK/DRIVE

Company : CREATIVE PIPELINE                         Type: CONT
Co addr : PO BOX 1081
City    : TUCKER                         State   : GA Zip: 30085
Caller  : TORY JOHNSON                   Phone   : 770-484-7020
Fax     : 770-484-4830                   Alt. Ph.:

Submitted date: 03/05/02 Time: 15:16 Oper: 034 Chan: WEB
Mbrs : ACS03  AGL103 ATL01  ATT02  ATTBB  BSCA   DOTI   EPC01  FUL02  GP103
     : GP104  ICG01  LEV3   LGN01  MCI02  MFN02  MT104  NU104  OMN01  QWEST8
     : ST005  TWT90  USW01  WCI01
-------------------------------------------------------------------------------
TK305151.048 #00276

ATL01  00285 GAUPC 03/05/02 15:22:08 03052-103-078-000 NORMAL
Underground Notification
Ticket : 03052-103-078 Date: 03/05/02 Time: 14:52 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 81     To: 87     Name:    PEACHTREE                      WAY  NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: THIS IS PEACHTREE HEIGHTS AREA -- LOC ENTIRE PROPERTIES

Grids    : 3349B8422B   3349B8422A   3349B8423D   3349B8423C
Work type: INSTL SWR SVC LINE
Work date: 03/08/02 Time: 07:00 Hrs notc: 064 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : UNKNOWN
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : CALL JIM COUCH AT 404 233-8945 OR 404 233-4877 (LEAVE MESSAGE/NUMBER
        : TO CALL YOU BACK) -- BEFORE LOCATING
        : *** NEAR STREET ***   W WESLEY RD

Company :                                           Type: HOME
Co addr :
City    :                                State   :
Caller  : RES/JIM COUCH                  Phone   : 404-233-8945
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 14:52 Oper: 103 Chan: WEB
Mbrs : AGL123 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP106  GP107  ICG01
     : LEV3   MCI02  MFN02  NU106  NU107  OMN01  TWT90  USW01
-------------------------------------------------------------------------------
TK305151.116 #00277

ATL01  00286 GAUPC 03/05/02 15:22:16 03052-104-080-000 NORMAL RESTAKE
Underground and Overhead Notification
Ticket : 03052-104-080 Date: 03/05/02 Time: 15:18 Revision: 000
Old Tkt: 02182-109-036 Date: 02/21/02 Time: 00:14

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 933    To:        Name:    PEACHTREE                      ST   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE THE EAST SIDE OF PEACHTREE ST FROM 8TH ST NORTHWARD TO THE
     : PRIVATE ALLEY JUST SOUTH OF 999 PEACHTREE ST -- THIS IS BOUNDED ON THE
     : EAST SIDE BY A PARKING DECK

Grids    : 3346B8422A   3346A8422A   3346B8423D   3346A8423D
Work type: GRADING/SETTING FOOTERS
Work date: 01/03/02 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 21 MONTHS
Done for : PEACHTREE LIMITED PARTNERSHIP
Crew on Site: N  White-lined: N  Railroad: N  Blasting: Y  Boring: N

Remarks : --WILL BLAST (POSSIBLE) -- OVERHEAD WORK BEGIN DATE: APPROX THE
        : MIDDLE OF JULY -- OVERHEAD WORK COMPLETION DATE: 21 MONTHS FROM BEGIN
        : DATE--
        : * LGN01 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : RESTAKE OF: 01152-022-026
        : RESTAKE OF: 01312-101-009
        : * AGL103 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : RESTAKE OF: 02182-109-036
        : * ATL01 AND ICG01 WERE NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : *** NEAR STREET ***   8TH ST
        : OVERHEAD WORK BEGIN DATE: 10/09/01
        : OVERHEAD WORK COMPLETION DATE: 10/25/01
        : --WILL BLAST   --ASAP PLS RUSH
        : *** LOOKUP BY MANUAL

Company : R J GRIFFIN/MCTYRE GRADING                Type: CONT
Co addr : 800 MT VERNON HWY  STE 200
City    : ATLANTA                        State   : GA Zip: 30328
Caller  : SUE BREEDLOVE                  Phone   : 770-551-8883
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:18 Oper: 104 Chan: WEB
Mbrs : ACS03  AGL103 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP103  GP104
     : GP106  GP107  HMGCT1 HMMED1 ICG01  LEV3   LGN01  MCI02  MFN02  MT104
     : MT106  NU103  NU104  NU107  OACS02 OGP101 OMN01  TWT90  USW01  WCI01
-------------------------------------------------------------------------------
TK305151.124 #00278

ATL01  00287 GAUPC 03/05/02 15:22:27 03052-916-002-000 NORMAL
Underground Notification
Ticket : 03052-916-002 Date: 03/05/02 Time: 15:16 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 564    To:        Name:    HOPE                           ST   SW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE LEFT SIDE OF DRIVEWAY AREA MARKED. AREA IS MARKED

Grids    : 3343A8424C   3343A8424B
Work type: SETTING POLES AND ANCHORS
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 1DAY
Done for : GEORGIA POWER CO.
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   HOBSON ST

Company : GEORGIA POWER COMPANY                     Type: MEMB
Co addr : 760 RALPH MCGILL BLVD
City    : ATLANTA                        State   : GA Zip: 30308
Caller  : EARL TOWNS                     Phone   : 404-572-7718
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:16 Oper: 916 Chan: 999
Mbrs : AGL102 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP103  MCI02  MEA70
     : MT103  OMN01  USW01  WCI01
-------------------------------------------------------------------------------
TK305151.135 #00279

ATL01  00288 GAUPC 03/05/02 15:22:34 03052-998-039-000 NORMAL
Underground Notification
Ticket : 03052-998-039 Date: 03/05/02 Time: 15:15 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 734    To:        Name: E  PACES FERRY                    RD   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: ENTIRE PROPERTY

Grids    : 3350B8421B   3350C8421A   3350B8421A   3350C8422D   3350C8422C
Grids    : 3350C8422B   3350C8422A
Work type: BURY SERVICE WIRE
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   PIEDMONT RD

Company : BELLSOUTH COMPANY                         Type: CONT
Co addr : 4400 PECAN ST
City    : LOGANVILLE                     State   : GA Zip: 30352
Caller  : CINDY SMITH                    Phone   : 770-466-3446
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:15 Oper: 998 Chan: 999
Mbrs : ACS03  AGL123 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP106  GP107
     : ICG01  LEV3   MCI02  MT107  NU106  NU107  OMN01  QWEST8 TCG01  USW01
     : WCI01
-------------------------------------------------------------------------------
TK305151.142 #00280

ATL01  00289 GAUPC 03/05/02 15:28:07 03052-106-022-000 DAMAGE
Underground Notification
Ticket : 03052-106-022 Date: 03/05/02 Time: 15:25 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 140    To:        Name:    PEACHTREE HILLS                AV   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC THE FRONT YARD**

Grids    : 3349D8422C   3349D8422B   3349D8422A   3349D8423D
Work type: INSTL IRRIGATION/PLANTING TREES SHRUBS/RETAINING WALL
Work date: 03/05/02 Time: 15:26 Hrs notc: 000 Priority: 6
Legal day: 03/05/02 Time: 15:26 Good thru: 03/05/02 Restake by: 03/05/02
RespondBy: 03/05/02 Time: 15:26 Duration : 1MON
Done for : RES/HANK HOLTER
Crew on Site: Y  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : DAMAGE OF 02122-036-001
        : DAMAGE TO AGL123 **FRONT OF PROPERTY ** **EXTENT: PER CALLER DOES NOT
        : KNOW  **
        : *** NEAR STREET ***   PEACHTREE RD
        : *** LOOKUP BY MANUAL

Company : OUTSIDE LANDSCAPE GROUP                   Type: CONT
Co addr : 635 OAK FARM LANE
City    : ALPHARETTA                     State   : GA Zip: 30004
Caller  : RICK KALDROVICS                Phone   : 770-754-1188
Fax     :                                Alt. Ph.:
Contact : RICK

Submitted date: 03/05/02 Time: 15:25 Oper: 106 Chan: 999
Mbrs : AGL123 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP106  GP107  LEV3
     : MCI02  NU106  NU107  OMN01  QWEST8 USW01  WCI01
-------------------------------------------------------------------------------
TK305151.655 #00281

ATL01  00290 GAUPC 03/05/02 15:28:15 03052-052-067-000 NORMAL
Underground Notification
Ticket : 03052-052-067 Date: 03/05/02 Time: 15:18 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 315    To:        Name:    CHESTER                        AV   SE
Cross: From:        To:        Name:
Offset:
Subdivision: UPN 69
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ON N SIDE NW CORNER OF BLDG 2---FROM BLDG N SIDE OUT FOR 15FT ON NW
     : SIDE FROM CRONER OUT FOR 30FT

Grids    : 3345D8421B   3344B8421B   3344A8421B
Work type: INST CONCRETE PADS/INST FOOTERS
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 1WK
Done for : UPN69
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   MEMORIAL DR
        : *** LOOKUP BY MANUAL

Company : THE JAMES GROUP INC                       Type: CONT
Co addr : 2060 FRANKLIN WAY          STE. 1000
City    : MARIETTA                       State   : GA Zip: 30067
Caller  : PATRICK KIBLINGER              Phone   : 770-951-9653
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:18 Oper: 052 Chan: WEB
Mbrs : ACS03  AGL103 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP104  ICG01
     : MCI02  MT104  OMN01  USW01  WCI01
-------------------------------------------------------------------------------
TK305151.723 #00282

ATL01  00291 GAUPC 03/05/02 15:28:22 03052-677-005-000 NORMAL
Underground Notification
Ticket : 03052-677-005 Date: 03/05/02 Time: 15:24 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 1211   To:        Name:    HERITAGE                       CT   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: FRONT AND LEFT SIDE OF PROPERTY

Grids    : 3351C8425B
Work type: DRAINAGE INSTALLATION
Work date: 03/11/02 Time: 07:00 Hrs notc: 135 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 2 DAYS
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   REGENCY RD

Company : THE TROTTER CO                            Type: CONT
Co addr : 2614 CHESTNUT DR N
City    : DORAVILLE                      State   : GA Zip: 30360
Caller  : KENT GANN                      Phone   : 770-458-0810
Fax     : 770-458-0968                   Alt. Ph.:
Email   : KTRAVLER@EUDORAMAIL.COM
Cellular: 770-722-5658

Submitted date: 03/05/02 Time: 15:24 Oper: 677 Chan: 999
Mbrs : AGL123 ATL01  ATTBB  BSCA   DOTI   FUL02  GP105  OMN01  USW01
-------------------------------------------------------------------------------
TK305151.730 #00283

ATL01  00292 GAUPC 03/05/02 15:42:36 03052-000-046-000 NORMAL
Underground Notification
Ticket : 03052-000-046 Date: 03/05/02 Time: 15:29 Revision: 000

State: GA  County: FULTON       Place: EAST POINT
Addr : From:        To:        Name:    MOUNT OLIVE                    RD
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE THE ENTIRE AREA OF MOUNT OLIVE RD JUST NORTH OF DRESDEN TRAIL
     : NEAR THE BUS STOP YOU WILL SEE WHERE PUBLIC WORKS HAS THE STREET BLOCKED
     : OFF LOCATE EVERYTHING WHETHER IN CONFLICT OR NOT WE WOULD LIKE TO BEGIN
     : WEDNESDAY AT  NOON 03 06 02

Grids    : 3339B8428B   3339A8428B   3340D8428B
Work type: REPAIRING BROKEN WATER MAINLINE
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Done for : CITY OF EAST POINT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   DRESDEN TRL

Company : CITY OF EAST POINT                        Type: CONT
Co addr : 3120 S MARTIN ST
City    : EAST POINT                     State   : GA Zip: 30344
Caller  : SHIRRIE NORRIS                 Phone   : 404-765-1027
Fax     : 404-761-1316                   Alt. Ph.: 404-765-1027
Email   : SNORRIS@EASTPOINT.ORG

Submitted date: 03/05/02 Time: 15:29 Oper: 000 Chan: 999
Mbrs : AGL108 ATL01  ATT02  ATTBB  BSCA   DOTI   EPT50  EPT52  FUL02  GP131
     : OMN01  USW01
-------------------------------------------------------------------------------
TK305153.123 #00284

ATL01  00298 GAUPC 03/05/02 15:43:21 03052-031-058-000 NORMAL RESTAKE
Underground Notification
Ticket : 03052-031-058 Date: 03/05/02 Time: 15:31 Revision: 000
Old Tkt: 02152-098-012 Date: 02/20/02 Time: 00:08

State: GA  County: DEKALB       Place: ATLANTA
Addr : From:        To:        Name:    LOCHLAND                       RD   SE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC BOTH SIDES OF RD ON LOCHLAND RD FROM BRADLEY AVE TO ATLANTA CITY
     : LIMITS

Grids    : 3343C8419A   3343B8419A   3343C8420D   3343B8420D   3343B8420C
Work type: INSTLWTR MAIN
Work date: 02/04/02 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 4 MONTHS
Done for : DEKALB CO
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : RESTAKE OF: 01302-026-062
        : * AGL102 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : RESTAKE OF: 02152-098-012
        : * ATL01 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : *** NEAR STREET ***   BRADLEY AVE
        : *** WILL BORE BOTH SIDES OF ROAD

Company : DAVIS CONTRACTING  COMPANY                Type: CONT
Co addr : 3859 FLOYD RD
City    : AUSTELL                        State   : GA Zip: 30001
Caller  : BRENT  DAVIS                   Phone   : 770-434-0095
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:31 Oper: 031 Chan: WEB
Mbrs : AGL102 ATL01  ATTBB  BSCA   DCTS01 DCWS01 DOTI   GP832  USW01
-------------------------------------------------------------------------------
TK305153.229 #00290

ATL01  00301 GAUPC 03/05/02 15:43:43 03052-677-006-000 NORMAL
Underground Notification
Ticket : 03052-677-006 Date: 03/05/02 Time: 15:28 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 708    To:        Name:    ANTONE                         ST   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: ENTIRE PROPERTY

Grids    : 3347A8424C   3347A8424B   3347A8424A   3347A8425D
Work type: DRAINAGE INSTALLATION
Work date: 03/12/02 Time: 07:00 Hrs notc: 159 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration : 2 DAYS
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   HOWELL MILL RD

Company : THE TROTTER CO                            Type: CONT
Co addr : 2614 CHESTNUT DR N
City    : DORAVILLE                      State   : GA Zip: 30360
Caller  : KENT GANN                      Phone   : 770-458-0810
Fax     : 770-458-0968                   Alt. Ph.:
Email   : KTRAVLER@EUDORAMAIL.COM
Cellular: 770-722-5658

Submitted date: 03/05/02 Time: 15:28 Oper: 677 Chan: 999
Mbrs : ACS04  AGL102 AGL103 ATL01  ATTBB  BSCA   DOTI   FUL02  GP105  GP106
     : MCI02  MEA70  OMN01  QWEST8 USW01
-------------------------------------------------------------------------------
TK305153.251 #00293

ATL01  00302 GAUPC 03/05/02 15:43:50 03052-998-042-000 NORMAL
Underground Notification
Ticket : 03052-998-042 Date: 03/05/02 Time: 15:33 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 3468   To:        Name:    RILMAN                         RD   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: ENTIRE PROPERTY

Grids    : 3350C8425D   3350B8425D   3350A8425D   3351D8425D   3350B8425C
Work type: BURY SERVICE WIRE
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   W PACES FERRY RD

Company : BELLSOUTH COMPANY                         Type: CONT
Co addr : 4400 PECAN ST
City    : LOGANVILLE                     State   : GA Zip: 30352
Caller  : CINDY SMITH                    Phone   : 770-466-3446
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:33 Oper: 998 Chan: 999
Mbrs : ACS04  AGL123 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP105  OMN01
     : USW01
-------------------------------------------------------------------------------
TK305153.258 #00294

ATL01  00305 GAUPC 03/05/02 16:03:53 03052-919-001-000 NORMAL
Underground Notification
Ticket : 03052-919-001 Date: 03/05/02 Time: 15:46 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 393    To: 397    Name: N  HIGHLAND                       AV   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: PLEASE LOCATE ALONG FRONT OF PROPERTIES FRONT OF PROPERTY

Grids    : 3347B8420A   3347A8420A   3345A8421D   3346D8421D   3346C8421D
Grids    : 3346B8421D   3346A8421D   3347D8421D   3347C8421D   3347B8421D
Grids    : 3347A8421D   3345B8421C   3345A8421C   3346A8421C   3347D8421C
Grids    : 3347C8421C   3347B8421C   3345B8421B   3345A8421B
Work type: UNDERGROUND ELECTRICAL SERVICE
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : UNDERGROUND SERVICE
        : *** NEAR STREET ***   WASHITA AV NE

Company : GEORGIA POWER COMPANY                     Type: MEMB
Co addr : 760 RALPH MCGILL BLVD
City    : ATLANTA                        State   : GA Zip: 30308
Caller  : CASEY PREECE                   Phone   : 404-572-7715
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:46 Oper: 919 Chan: 999
Mbrs : AGL102 AGL103 ATL01  ATT02  ATTBB  BSCA   DOTI   FUL02  GP104  GP107
     : GP833  MCI02  MT104  OMN01  QWEST8 USW01
-------------------------------------------------------------------------------
TK305155.301 #00297

ATL01  00306 GAUPC 03/05/02 16:04:01 03052-919-002-000 NORMAL
Underground Notification
Ticket : 03052-919-002 Date: 03/05/02 Time: 15:52 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 965    To: 969    Name:    AUSTIN                         AV   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: 2 NEW HOUSES ENTIRE PROPERTY

Grids    : 3345B8420A   3345A8420A   3345B8421D   3345A8421D   3345B8421C
Grids    : 3345A8421C   3345B8421B
Work type: SETTING POLE &amp; INSTALLING UNDERGROUND ELECTRICAL SERVICES
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : SETTING POLE &amp; INSTALLING UNDERGROUND ELECTRICAL SERVICES
        : *** NEAR STREET ***   SINCLAIR AV NE

Company : GEORGIA POWER COMPANY                     Type: MEMB
Co addr : 760 RALPH MCGILL BLVD
City    : ATLANTA                        State   : GA Zip: 30308
Caller  : CASEY PREECE                   Phone   : 404-572-7715
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 15:52 Oper: 919 Chan: 999
Mbrs : AGL102 AGL103 ATL01  ATT02  ATTBB  BSCA   DCTS01 DCWS01 DOTI   FUL02
     : GP104  GP832  IFN01  MCI02  MT104  MT832  OMN01  QWEST8 USW01  WCI01
-------------------------------------------------------------------------------
TK305155.309 #00298

ATL01  00307 GAUPC 03/05/02 16:04:09 03052-989-014-000 NORMAL
Underground Notification
Ticket : 03052-989-014 Date: 03/05/02 Time: 15:48 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    MCMILLAN                       STRE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE MCMILLAN STREET FROM TURNER PLACE TO 8TH STREET. TO INCLUDE
     : SERVICE TO THE CALDWELL HALL BUILDING AT GA. TECH. LOCATE ALL SIDES OF
     : BUILDING. PAINT AND STAKE BOTH SIDES OF STREET, ALL UTILITIES, MAINS AND
     : SERVICE LINES. LOCATE ALL
     : INTERSECTIONS 200 FEET IN ALL DIRECTIONS. LOCATE AS REQUESTED. NO VERBAL
     : MODIFICATIONS. POSSIBLE BORE.

Grids    : 3346B8424D   3346A8424D   3346B8424C   3346A8424C
Work type: INSTALL / MAINTAIN GAS MAIN / SERVICE
Work date: 03/08/02 Time: 07:00 Hrs notc: 063 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Done for : GEORGIA TECH
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.
        : *** NEAR STREET ***   TURNER PLACE
        : *** WILL BORE ROAD, DRIVEWAY &amp; SIDEWALK
        : *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : SHERRY GREEN                   Phone   : 770-942-8180
Fax     :                                Alt. Ph.:
Email   : SGREEN@BENTON-GEORGIA.COM

Submitted date: 03/05/02 Time: 15:48 Oper: 989 Chan: 999
Mbrs : AGL103 ATL01  ATTBB  BSCA   DOTI   FUL02  GP103  GP106  LEV3   MCI02
     : MEA70  NU103  OMN01  TWT90  USW01  WCI01
-------------------------------------------------------------------------------
TK305155.317 #00299

ATL01  00310 GAUPC 03/05/02 16:55:30 02152-020-030-003 NORMAL 2NDREQ
Underground Notification
Ticket : 02152-020-030 Date: 03/05/02 Time: 16:38 Revision: 003
Old Tkt: 02152-020-030 Date: 03/04/02 Time: 08:56

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SPRING                         ST   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE PKING LOT BEHIND THE TABERNACLE ADJACENT TO SPRING ST BTW LUCKIE
     : ST AND NASSAU ST - LOCATE THE AREAS MARKED WITH ORANGE PAINT

Grids    : 3345C8423C   3345B8423C   3345C8423B   3345B8423B
Work type: DIGGING TO LOCATE UG STORAGE TANKS
Work date: 02/20/02 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 02/20/02 Time: 07:00 Good thru: 03/08/02 Restake by: 03/05/02
RespondBy: 02/19/02 Time: 23:59 Duration : 2 DAYS
Done for : ATC ASSOCIATES
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : 3RD REQUEST TO GA POWER, FULTON CNTY WTR  &amp;  SWR AND AGL--CALLER
        : REQUESTS THAT THEY COME &amp; LOOK &amp; MARK THE LINES IN THE HOLE WHERE
        : THEY ARE WORKING HE WANTS TO KNOW WHO OWNS THE LINES &amp; IF THEY ARE
        : ACTIVE.
        : * ATL01 AND ICG01 WERE NOT ON THE ORIGINAL VERSION OF THIS TICKET
        : *** THIS IS THE 2ND REQUEST FOR ATL01.  PLEASE LOCATE ASAP.
        : *** NEAR STREET ***   LUCKIE ST
        : *** LOOKUP BY MANUAL

Company : ATC ASSOCIATES                            Type: CONT
Co addr : 1300 WILLIAMS DR
City    : MARIETTA                       State   : GA Zip: 30066
Caller  : JEFF BAHLING                   Phone   : 770-427-9456
Fax     :                                Alt. Ph.:
Contact : JEFF CAHLING
Cellular: 404-219-1227

Submitted date: 03/05/02 Time: 16:38 Oper: 020 Chan: WEB
Mbrs : ABS01  ACS03  AGL103 ATL01  ATT02  ATTBB  BSCA   DOTI   EPC01  FUL02
     : GP103  GP104  ICG01  LEV3   LGN01  MCI02  MEA70  MFN02  MT103  MT104
     : NU103  NU104  OMN01  QWEST8 ST005  TCG01  TWT90  USW01  WCI01  YIP01
-------------------------------------------------------------------------------
TK305164.417 #00302

ATL01  00313 GAUPC 03/05/02 16:55:56 03052-989-015-000 NORMAL
Underground Notification
Ticket : 03052-989-015 Date: 03/05/02 Time: 16:28 Revision: 000

State: GA  County: FULTON       Place: EAST POINT
Addr : From:        To:        Name:    DODSON DRIVE                   CONN
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE DODSON DRIVE CONNECTOR FROM THE NORTHERNMOST INTERSECTION OF EARL
     : GODFREY LANE TO WASHINGTON ROAD. TO INCLUDE BOTH INTERSECTIONS OF EARL
     : GODFREY LANE, AND THE INTERSECTIONS OF STONE ROAD, WEST RUGBY AVENUE AND
     : WASHINGTON ROAD. PAINT AND STAKE
     : BOTH SIDES OF STREET, ALL UTILITIES, MAINS AND SERVICE LINES. LOCATE ALL
     : INTERSECTIONS 200 FEET IN ALL DIRECTIONS. LOCATE AS REQUESTED. NO VERBAL
     : MODIFICATIONS. POSSIBLE BORE.

Grids    : 3339B8428C   3339A8428C   3340D8428C
Work type: INSTALL / MAINTAIN GAS MAIN / SERVICE
Work date: 03/08/02 Time: 07:00 Hrs notc: 062 Priority: 3
Legal day: 03/08/02 Time: 07:00 Good thru: 03/26/02 Restake by: 03/21/02
RespondBy: 03/07/02 Time: 23:59 Duration :
Done for : ATLANTA GAS LIGHT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.
        : *** NEAR STREET ***   WASHINGTON ROAD
        : *** WILL BORE ROAD, DRIVEWAY &amp; SIDEWALK
        : *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : SHERRY GREEN                   Phone   : 770-942-8180
Fax     :                                Alt. Ph.:
Email   : SGREEN@BENTON-GEORGIA.COM

Submitted date: 03/05/02 Time: 16:28 Oper: 989 Chan: 999
Mbrs : AGL108 ATL01  ATT02  ATTBB  BSCA   CPK50  CPK51  DOTI   EPT50  EPT52
     : FUL02  OMN01  USW01
-------------------------------------------------------------------------------
TK305164.504 #00305

ATL01  00315 GAUPC 03/05/02 17:19:15 03052-107-054-000 NORMAL
Underground Notification
Ticket : 03052-107-054 Date: 03/05/02 Time: 16:58 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 1343   To:        Name:    PEACHTREE BATTLE               AV   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: FROM THE ROAD TO THE HOUSE.  LOC THE FRONT OF PROPERTY.

Grids    : 3349B8425B   3349B8425A   3349B8426D
Work type: INSTL A SPRINKLER SYSTEM
Work date: 03/11/02 Time: 07:00 Hrs notc: 134 Priority: 3
Legal day: 03/11/02 Time: 07:00 Good thru: 03/27/02 Restake by: 03/22/02
RespondBy: 03/08/02 Time: 23:59 Duration : UNKNOWN
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SPECIAL ATTENTION TO GAS
        : *** NEAR STREET ***   BOHLER RD NW
        : *** WILL BORE SIDEWALK/DRIVE
        : *** LOOKUP BY MANUAL

Company :                                           Type: HOME
Co addr :
City    :                                State   :
Caller  : JOHN ADAMSON                   Phone   : 404-355-5814
Fax     :                                Alt. Ph.:

Submitted date: 03/05/02 Time: 16:58 Oper: 107 Chan: 999
Mbrs : AGL123 ATL01  ATTBB  BSCA   DOTI   FUL02  GP105  OMN01  USW01
-------------------------------------------------------------------------------
TK305170.823 #00307
