SEQUENCE NUMBER 1122   CDC = CR4
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TIME WARNER      ONCOR ELEC-UQ    ATMOS-MIDTX-UQ   RICHARDSON ISD 
ATT/D_(SMP-D)    LONE STAR XCHG

Locate Request No. 073570144

Prepared By TERRANCE M           On 23-DEC-07  At 1855

MapRef :                            Grid: 325700096470A  Footprint: D08

Location:     County: DALLAS  Town: DALLAS

             Address: 6732 ROUNDROCK RD 

Beginning Work Date 12/27/07 Time of Day: 12:00 am   Duration: 07 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : REPLACING FENCE               

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : GEORGE DORRELL
Company Name   : NORTH DALLAS FENCE & ARBOR
Work by NORTH DALLAS FENCE & ARBOR      For HOME OWNER

Person to Contact : GEORGE DORRELL

Phone No.  ( 469 )644-9593 /( 972 )417-9065 ( Hours: 08:00 am/05:00 pm )
Fax No.    (    )    
Email:     NORTHDALLASFENCE@YAHOO.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: MEADOWCREEK 
WORKING ON ENTIRE PROPERTY.  MAPSCO 15,C                              
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
NORTHDALLASFENCE@YAHOO.COM

Map Cross Reference : MAPSCO 15,D                         

FaxBack Requested ? NO     Lone Star Xref: 


073570144 to EMAIL ADDRESS at 18:58:26 on SUN, 12/23/07 for CR4 #1122