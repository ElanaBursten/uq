

SEQUENCE NUMBER 1777   CDC = AMP
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MIDTX-UQ   TIME WARNER      VERI-CARROLLTON  CITY LEWISVILLE
ATMOS-MT-HP-UQ   TX NEW MEX PWR   ATT/D_(SMP-D)    LEVEL 3 COMM     
LONE STAR XCHG

Locate Request No. 082123433

Prepared By TRAVIS S             On 30-JUL-08  At 1313

MapRef : LEWISVILLE EAS             Grid: 330200096593A  Footprint: D11

Location:     County: DENTON  Town: LEWISVILLE

             Address: 0 FOX AVE 

Beginning Work Date 08/01/08 Time of Day: 01:15 pm   Duration: 99 DAYS 

Fax-A-Locate Date          at 

Excavation Type : ROAD CONSTRUCTION             
Nature of Work  : EXCAVATION OF ROAD WAY        

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : MARIA BERUMEN
Company Name   : T & R EXCAVATION, INC.
Work by T & R EXCAVATION, INC.      For TISEO PAVING

Person to Contact : JOE FIGUEROA

Phone No.  ( 214 )869-2297 /(    )     ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 214 )267-1614
Email:     MARIAB@TREXCAVATION.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: VALLEY PARKWAY 
UPDATE & REMARK-071831112** REMARK, FRESH PAINTS, NEW FLAGS**  WORK DA
TE: 2 WORKING DAY NOTICE   MAPSCO: 651 L GIVEN; OP MEASURES FROM 650,L
/Q - 651,J IN MAPSCO/GEOCALL.  **OP CLD-PER FREDDY WORK WILL TAKE PLAC
E FROM FOX AVE & VALLEY PKWY TO I-35**  PLEASE MARK ALL UTILITIES, STA
RT ON VALLEY PKWY, FOX AVE GO E TO STEMMONS FRWY.  ANY ?'S PLESE CALL 
FREDDY ELIZONDO 214-837-5259- FROM 400-699 BLK  670095.XML            
                                                                      
MARIAB@TREXCAVATION.COM

Map Cross Reference : MAPSCO 650,L/Q - 651,J              

FaxBack Requested ? YES    Lone Star Xref: 


082123433 to EMAIL ADDRESS at 13:18:53 on WED, 07/30/08 for AMP #1777

