0
Date/Time :  7-01-02 at  9:14

MISSU VA
SUMM
DAILY AUDIT OF TICKETS SENT ON  7/01/02

Date/Time: 07/01/02 08:27:27 AM
Receiving Terminal: WGL04

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  2001121-ETKT |  13  2001122-48HR |  25  2001137-48HR |  37  2001165-48HR |
   2  2001098-ETKT |  14  2001123-48HR |  26  2001141-48HR |  38  2001166-48HR |
   3  2001103-ETKT |  15  2001125-48HR |  27  2001143-RELC |  39  2001167-48HR |
   4  2001104-ETKT |  16  2001126-UPDT |  28  2001150-48HR |  40  2001169-48HR |
   5  2001105-ETKT |  17  2001128-UPDT |  29  2001152-RELC |  41  2001168-48HR |
   6  2001106-ETKT |  18  2001129-UPDT |  30  2001153-RELC |  42  2001170-48HR |
   7  2001109-ETKT |  19  2001135-STKT |  31  2001155-48HR |  43  2001172-48HR |
   8  2001110-ETKT |  20  2001134-48HR |  32  2001158-48HR |  44  2001173-48HR |
   9  2001118-ETKT |  21  2001131-48HR |  33  2001160-48HR |  45  2001148-PTKT |
  10  2001124-ETKT |  22  2001130-48HR |  34  2001161-48HR |  46  2001174-48HR |
  11  2001132-ETKT |  23  2001138-48HR |  35  2001163-48HR |  47  2001176-48HR |
  12  2001133-STKT |  24  2001157-ETKT |  36  2001164-48HR |  48  2001177-48HR |

* indicates ticket # is repeated

Total Tickets: 48

ETKT - EMERGENCY                      |   12
STKT - SHORT NOTICE                   |    2
48HR - STANDARD                       |   27
UPDT - UPDATE                         |    3
RELC - RELOCATE                       |    3
PTKT - INSUFFICIENT NOTICE            |    1

Please call (410) 712-0056 if this data does
not match the tickets you received on  7/01/02
