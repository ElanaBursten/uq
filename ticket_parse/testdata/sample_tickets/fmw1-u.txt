\
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 00357291
Transmit      Date: 05/14/02      Time: 10:29    Op: ellen.
Original Call Date: 05/14/02      Time: 10:26    Op: ellen.
Work to Begin Date: 05/14/02      Time: 10AM

Place: NE
Address: 1732        Street: MONTELLO AV NE
Nearest Intersecting Street: MT OLIVE AV NE

Type of Work: REPAIRING ELEC SVC
Extent of Work:   SEE CREW ON SITE FOR LOC INSTRUCTIONS.
Remarks: EMERGENCY ** CREW ON SITE
:                                                        Fax: (301)699-7750

Company     : B FRANK JOY
Contact Name: ERICA WILSON          Contact Phone: (301)699-6000
Alt. Contact: ERICA WILSON          Alt. Phone   : (301)699-6029
Work Being Done For: PEPCO
State: DC              County: NE
Map: NE    Page: 010   Grid Cells: G11
Explosives: N
ATT01      DCC01      LTC02      MCI03      PEP06      PEP08
Send To: TDC   01  Seq No: 0120   Map Ref:
         PEP   07          0120
