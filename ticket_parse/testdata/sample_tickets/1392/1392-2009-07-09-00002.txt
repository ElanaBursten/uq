
From MSOCS
RC9
UTILIQUEST, MEMPHIS
Audit For 7/8/2009

For Code RC9
Type  Seq#  Ticket                    Status                                
----  ----  ------------------------  ------------------------------------- 
      0001  09070805140001            Delivered                           
      0002  09070807250019            Delivered                           
      0003  09070807250021            Delivered                           
      0004  09070807260022            Delivered                           
      0005  09070807270023            Delivered                           
      0006  09070807270024            Delivered                           
!     0007  09070807290026            Delivered                           
      0008  09070807360038            Delivered                           
      0009  09070807380041            Delivered                           
      0010  09070807380043            Delivered                           
      0011  09070807400046            Delivered                           
      0012  09070807400048            Delivered                           
      0013  09070807440050            Delivered                           
      0014  09070808000079            Delivered                           
      0015  09070808010082            Delivered                           
      0016  09070808050094            Delivered                           
      0017  09070808140114            Delivered                           
      0018  09070808160119            Delivered                           
      0019  09070808170121            Delivered                           
      0020  09070808200129            Delivered                           
      0021  09070808210131            Delivered                           
      0022  09070808330155            Delivered                           
      0023  09070808390165            Delivered                           
      0024  09070808590199            Delivered                           
      0025  09070809000201            Delivered                           
      0026  09070809060211            Delivered                           
!     0027  09070809210240            Delivered                           
      0028  09070809110222            Delivered                           
      0029  09070809140227            Delivered                           
      0030  09070809170233            Delivered                           
      0031  09070810070321            Delivered                           
      0032  09070810250350            Delivered                           
      0033  09070810280358            Delivered                           
      0034  09070810330369            Delivered                           
      0035  09070810380375            Delivered                           
      0036  09070810400380            Delivered                           
      0037  09070810510398            Delivered                           
      0038  09070811140453            Delivered                           
      0039  09070811310471            Delivered                           
      0040  09070811340475            Delivered                           
      0041  09070811350477            Delivered                           
      0042  09070811350478            Delivered                           
      0043  09070811360479            Delivered                           
      0044  09070811370481            Delivered                           
      0045  09070811520508            Delivered                           
      0046  09070813070620            Delivered                           
      0047  09070813210634            Delivered                           
      0048  09070813250636            Delivered                           
      0049  09070813320642            Delivered                           
      0050  09070813500668            Delivered                           
      0051  09070814100727            Delivered                           
      0052  09070814310756            Delivered                           
      0053  09070814390767            Delivered                           
      0054  09070814430775            Delivered                           
      0055  09070814440778            Delivered                           
      0056  09070815070823            Delivered                           
      0057  09070815110838            Delivered                           
      0058  09070815250859            Delivered                           
      0059  09070815260861            Delivered                           
      0060  09070815270862            Delivered                           
      0061  09070815290863            Delivered                           
      0062  09070815300865            Delivered                           
      0063  09070815310866            Delivered                           
      0064  09070815550894            Delivered                           
      0065  09070815550895            Delivered                           
      0066  09070816100904            Delivered                           
      0067  09070816110906            Delivered                           
      0068  09070817110948            Delivered                           
      0069  09070817170950            Delivered                           

Normal    : 67
Resend    : 0
Emergency : 2
Failed    : 0
Total for RC9: 69



Legend
-----------------------
  - Normal
* - Resend
! - Emergency

