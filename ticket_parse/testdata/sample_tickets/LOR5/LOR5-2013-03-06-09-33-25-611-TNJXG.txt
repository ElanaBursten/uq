

From: IRTHNet  At: 03/06/13 08:32 AM  Seq No: 2

OUNC 
Ticket No: 13041059               48 HOUR NOTICE 
Send To: QLNOR17    Seq No:  165  Map Ref:  

Transmit      Date:  3/05/13   Time: 11:48 pm    Op: wiambi 
Original Call Date:  3/05/13   Time: 11:40 pm    Op: wiambi 
Work to Begin Date:  3/08/13   Time:  8:00 am 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address: 828         Street: SW MOSS ST 
Nearest Intersecting Street: SW BURLINGAME AVE 

Twp: 1S    Rng: 1E    Sect-Qtr: 21-SE-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.4675996 Lon:-122.6877531 SE Lat: 45.4670413 Lon:-122.6871397 

Type of Work: REPLACE SIDEWALK, DRIVEWAY, AND STAIRS 
Location of Work: EXCAVATION SITE IS ON THE S SIDE OF THE ROAD. 
: MARK THE ENTIRE FRONT OF THE PROPERTY FROM LOT LINE TO LOT LINE. 

Remarks: ++PLEASE NOTIFY CALLER AFTER LOCATING AND/OR RESPONDING 
:  

Company     : TOM HOFFMAN CONSTRUCTION 
Contact Name: TOM HOFFMAN                      Phone: (503)292-3357 
Cont. Email : tommhoffman@yahoo.com 
Alt. Contact: TOM HOFFMAN - CELL -             Phone: (503)267-3527 
Contact Fax : (503)206-6937 
Work Being Done For: GRACE HERR 
Additional Members:  
CMCST01    NWN01      PGE01      PTLD03
