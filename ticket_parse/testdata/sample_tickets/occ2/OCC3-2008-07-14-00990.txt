
WGL904 00325 VUPSb 07/14/08 11:21:20 B819601072-00B          MEETING

Ticket No:  B819601072-00B                        NEW  GRID NORM LREQ MEET
Transmit        Date: 07/14/08   Time: 11:21 AM   Op: 2KAM
Call            Date: 07/14/08   Time: 11:12 AM
Due By          Date: 07/17/08   Time: 07:00 AM
Update By       Date:            Time:
Expires         Date:            Time:
Meet            Date: 07/16/08   Time: 10:00 AM
Old Tkt No: B819601072
Original Call   Date: 07/14/08   Time: 11:12 AM   Op: 2KAM

City/Co:ARLINGTON             Place:                                    State:VA
Address:                      Street: CARPENTER RD
Cross 1:     PATTON DR

Type of Work:   CONDUIT - INSTALL
Work Done For:  FORT MEYERS
Excavation area:PROPOSED EXCAVATION AREA: INTERSECTION OF CARPENTER RD AND
                PATTON DR LOCATED ON FR MEYERS, OFF OF 2ND ST FROM N  WASHINGTON
                BLVD,
                BLDG 483

                NEED DRIVERS LICENSE TO GET IN
Instructions:   MEETING LOCATION: AT THE INTERSECTION OF CARPENTER AND PATTON ON
                FT MEYERS. ANY PROBLEMS FINDING THIS LOCATION, CONTACT ROBERT AT
                301-335-4520.
                WILL BE DRIVING BLUE  CHEVY SILVERADO PICKUP TRUCK.
                CALLER MAP REF: NONE

Whitelined: N   Blasting: N   Boring: N

Company:        HALLMAN AND HALLMAN ENTERPRISES           Type: CONT
Co. Address:    5235 WEST VIEW DR SUITE 100  First Time: N
City:           FREDERICK  State:MD  Zip:21703
Company Phone:  301-335-4520
Contact Name:   ROBERT HALLMAN              Contact Phone:301-696-1820
Contact Fax:    877-655-8723

Mapbook:  17B5
Grids:    3852B7705C     3852B7705D

Members:
APW901 = ARLINGTON COUNTY DPW (APW)     ATT392 = AT&T - ATLANTA (ATT)
CMC903 = COMCAST CABLE (CMC)            CMC904 = COMCAST CABLE (CMC)
DOM400 = DOMINION VIRGINIA POWER (DOM)  MCII81 = MCI (MCI)
USSP22 = SPRINT NEXTEL (SLD)            VZN906 = VERIZON (VZN)
WGL904 = WASHINGTON GAS (WGL)

Seq No:   325 B

