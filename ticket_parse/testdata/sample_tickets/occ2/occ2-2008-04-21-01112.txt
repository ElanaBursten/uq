
UTIL13 00637 VUPSb 04/21/08 14:23:43 B811202153-00B          NORMAL

Ticket No:  B811202153-00B                        NEW  GRID NORM LREQ
Transmit        Date: 04/21/08   Time: 02:23 PM   Op: WAKHAN
Call            Date: 04/21/08   Time: 02:22 PM
Due By          Date: 04/24/08   Time: 07:00 AM
Update By       Date: 05/08/08   Time: 11:59 PM
Expires         Date: 05/13/08   Time: 07:00 AM
Old Tkt No: B811202153
Original Call   Date: 04/21/08   Time: 02:22 PM   Op: WAKHAN

City/Co:FAIRFAX               Place:                                    State:VA
Address:     5429-5501        Street: CLONMEL CT
Cross 1:     GREENDALE VILLAGE DR

Type of Work:   CABLE TV SERVICE DROP- INSTALL
Work Done For:  COX COMMUNICATIONS
Excavation area:MARK FROM PED FRONT LEFT OF 5429 ACROSS GREENDALE VILLAGE DR
                ENTIRE PROPERTY OF 5501 INCLUDE ANY DRIVEWAYS, SIDEWALKS, AND
                ROADS  - WILL BORE IF REQUIRED
Instructions:   CALLER MAP REF: NONE

Permit: 947-88145
Whitelined: N   Blasting: N   Boring: Y

Company:        W C C CABLE INC                           Type: CONT
Co. Address:    4809 EWEL RD  First Time: N
City:           FREDERICKSBURG  State:VA  Zip:22408
Company Phone:  540-898-9315
Contact Name:   APRIL KHAN                  Contact Phone:540-898-9315
Email:          akhan@wcccable.com
Field Contact:  APRIL KHAN
Fld. Contact Phone:540-898-9315

Mapbook:  23F11
Grids:    3846C7707A     3846C7707B

Members:
COX901 = COX INET FIBER (COX)           COX906 = COX COMMUNICATIONS (COX)
DOM400 = DOMINION VIRGINIA POWER (DOM)  FCU901 = FAIRFAX COUNTY DPW SEWER (FCU)
FCW902 = FAIRFAX WATER (FCW)            VZN906 = VERIZON (VZN)
WGL904 = WASHINGTON GAS (WGL)

Seq No:   637 B

