
UTIL13 00146 VUPSa 02/26/09 13:09:51 A905700614-00A          NORMAL

Ticket No:  A905700614-00A                        NEW  GRID NORM LREQ
Transmit        Date: 02/26/09   Time: 01:09 PM   Op: 1GTF
Call            Date: 02/26/09   Time: 01:03 PM
Due By          Date: 03/03/09   Time: 07:00 AM
Update By       Date: 03/17/09   Time: 11:59 PM
Expires         Date: 03/20/09   Time: 07:00 AM
Old Tkt No: A905700614
Original Call   Date: 02/26/09   Time: 01:03 PM   Op: 1GTF

City/Co:FAUQUIER              Place:BEALETON                            State:VA
Address:     12413            Street: HARPERS RUN RD
Cross 1:     SC 17

Type of Work:   WATER SERVICE - INSTALL
Work Done For:  QUAIL HOLLOW INVESTMENTS
Excavation area:FACING THE HOUSE, ENTIRE LEFT SIDE.
Instructions:   CALLER MAP REF: NONE

Whitelined: N   Blasting: N   Boring: N

Company:        CURTIS BROTHERS DRILLING AND PUMP SERV    Type: CONT
Co. Address:    14247 RAYMOND DR  First Time: N
City:           SUMERDUCK  State:VA  Zip:22742
Company Phone:  540-439-8377
Contact Name:   CRYSTAL SMITH               Contact Phone:540-219-2902
Contact Fax:    540-439-8376
Field Contact:  TONY CURTIS
Fld. Contact Phone:540-272-0414

Mapbook:  6221E7
Grids:    3831C7743C     3831C7743D     3831D7743C     3831D7743D

Members:
CMC009 = COMCAST (CMC)                  CPL901 = COLONIAL PIPELINE COMPANY(CPL)
DOM470 = DOMINION VIRGINIA POWER (DOM)  VZN210 = VERIZON (VZN)

Seq No:   146 A

