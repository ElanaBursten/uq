

CPLZ05 9 PUPS Voice 05/18/2009 10:10:05 AM 0905110322 No Show

Ticket Number: 0905110322
Old Ticket Number: 0905071401
Created By: SKR
Seq Number: 9

Created Date: 05/11/2009 9:07:44 AM
Work Date/Time: 05/11/2009 9:15:55 AM
Update:           Good Through:         

Excavation Information:
State: SC     County: FLORENCE
Place: FLORENCE
Address Number: 2205
Street: BROAD DR
Inters St: HWY 51
Subd: 

Type of Work: SEE REMARKS
Duration: APPROX 1/2 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: HENDRIX SEPTIC TANK

Remarks/Instructions: **TYPE OF WORK REPLACING SEPTIC LINES **                
MARK THE BACK YARD // DIGGING FROM THE DOOR TO THE EDGE OF THE CARPORT AND    
MAKING RIGHT TURN AND MAKING A LEFT TURN                                      
HWY 51 AKA PAMPLICO HWY                                                       
***2ND RESEND DUE TO THE CALLER STATES THAT THE 72 HOURS WERE UP ON 5-7 AND   
SHE IS UNSURE IF ALL THREE COMPANIES HAVE MARKED THEIR LINES ARE NOT AS SHE   
IS CONFINED TO THE HOUSE BUT HAS BEEN TOLD THERE AREA SOME RED MARKINGS OUT   
FRONT BUT IS NOT SURE THE PHONE AND CABLE LINES MARKINGS HAVE BEEN            
COMPLETED, THE CREW IS ON SITE NOW WITH EQUIPMENT READY TO DIG SO PLEASE      
CONTACT JULIA GYORKI AT THE ABOVE HOME NUMBER 843-667-9161 OR MOBILE NUMBER   
843617-8614 ASAP TO LET HER KNOW IF YOU HAVE COMPLETED YOUR MARKINGS          
PLEASE, THANKS****5-11-09*****                                                

Caller Information: 
Name: JULIA GYORKI                          JULIA GYORKI                          
Address: 2205 BROAD DR
City: FLORENCE State: SC Zip: 29505
Phone: (843) 667-9161 Ext:  Type: Home
Fax:  Caller Email: 

Contact Information:
Contact:JULIA GYORKI Email: 
Call Back:843-617-8614 MOBILE Fax: 

Grids: 
Lat/Long: 34.1634474775164, -79.7438575239296
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZU45 CPLZ05 TWFZ66                                        


Map Link: (NEEDS DEVELOPMENT)
