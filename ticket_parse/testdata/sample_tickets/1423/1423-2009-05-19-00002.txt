

CPLZ05 5 PUPS Remote 05/18/2009 10:09:00 AM 0905180050 Cancel

Ticket Number: 0905180050
Old Ticket Number: 0905140209
Created By: R-FLY
Seq Number: 5

Created Date: 05/18/2009 7:43:25 AM
Work Date/Time: 05/18/2009 7:45:42 AM
Update:          Good Through:         

Excavation Information:
State: SC     County: DARLINGTON
Place: HARTSVILLE
Address Number: 807
Street: S EIGHTH ST
Inters St: LINCOLN AVE & W WASHINGTON ST
Subd: 

Type of Work: ELECTRIC, REPLACE POLE(S)
Duration: 7HRS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: PIKE ELECTRIC INC

Remarks/Instructions: MARK 30' AROUND POLE EW27BW ON RD AT ADDRESS// W        
CAROLINA AVE/LEFT AT 8TH ACCT ON LEFT// BEFORE WASHINGTON ST// W/O #EO818     
                                                                              
**CANCEL TICKET 0905140209 TO CHANGE WORDING FROM ACCT TO LOT**               

Caller Information: 
Name: FELECIA YOUNG                         PIKE ELECTRIC INC                     
Address: 1820 TRADE ST
City: FLORENCE State: SC Zip: 29501
Phone: (843) 413-5000 Ext:  Type: Business
Fax: (843) 413-5001 Caller Email: 

Contact Information:
Contact:FELECIA YOUNG Email: FELICIA.YOUNG@PGNMAIL.COM
Call Back: Fax: (843) 413-5001

Grids: 
Lat/Long: 34.363186665677, -80.0770681189011
Secondary: 34.3610440382067, -80.076518727242
Lat/Long Caller Supplied: N 

Members Involved: BSZU45 CPLZ05 SCEGT01 SCG75 TWYZ52                          


Map Link: (NEEDS DEVELOPMENT)
