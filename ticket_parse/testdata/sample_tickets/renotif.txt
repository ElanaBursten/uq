

KI     00002 POCS 05/06/02 07:58:14 1260170-000 ROUTINE PLACE

===========PENNSYLVANIA ONE CALL SYSTEM - ACT 287 AS AMENDED REQUEST===========

Serial Number--[1260170]

Message Type--[ROUTINE] Lead Time--[0024]   Tape Channel #--[0747017][0228]

County--[WESTMORELAND] Municipality--[DERRY TWP]

Work Site--[ ** **SR 0982** ** ]

      Nearest Intersection--[LATROBE DERRY RD]
      Latitude/Longitude--[          /          ]

Location Information--
[AT BOX 238, BRADENVILLE, PA. 15620.  APPX 1/4 MILE FROM THE INTER ON THE S SIDE
 OF THE ST.]

Site Marked in White--[Y]    PennDOT Permit Number--[]

Type of Work--[INSTL NEW SWR & WTR LINES] Depth--[5-10FT]
Method of Excavation--[BH]  Extent of Excavation--[24IN WIDE]
Working in--
Street--[ ]    Sidewalk--[ ]    Public Prop--[ ]    Pvt Prop--[X]
      Other--[]  Owner/Done for--[DAVE KRAWTZ]

Proposed Dig Date--[09-MAY-02] Dig Time--[0800]
Lawful Dig Dates---[09-MAY-02][0800] Through [20-MAY-02][0800]

Contractor--[KELLYS EXCAVATING] Contractor/Utility--[Y]
Address-----[RR 1 BOX 270]
City--------[BRADENVILLE] State--[PA] Zip--[15620]

Caller--[MIKE KELLY] Phone--[724-537-2572]
E-mail address--[none]
Person to Contact--[MIKE KELLY] Phone--[724-537-2572]
Best Time to Call--[AFTER 2000]

Prepared--[06-MAY-02]  at  [0751]  by  [PATTI MCINTOSH]

ATM=AT&T ATLANTA    BB1=REA ENERGY COOP DEE=DERRY TWP       DR1=DERRY BMA
EO1=DERRY TMA NWDY  EU1=HIGHRIDGE WTR A FB =VERIZON PA-PGH  FX1=BLAIRSVILLE MA
GA =DOMINION GRBG   HU1=ADELPHIA HGLD   IC =BUCKEYE PL CRPL JUL=
KI =ALLEG PWR LTRB  LA =GPU INDIANA     LM1=LATROBE MA      LX =TEPPCO
MN =ALLTEL COMM     PEC=PENNECO PIPELIN QS =QWEST COMM      SP =SUNOCO PIPELINE
SST=STANDARD STEEL  SU =SUNOCO PIPELINE TRP=EQUITRANS LP    TX =TX E GAS PL
WP =TW PHILLIPS










KI     00001 POCS 05/08/02 08:26:17 1260170-001 INSUFFICIENT NOTICE RXMIT PLACE

           ********** == R E N O T I F I C A T I O N == **********

===========PENNSYLVANIA ONE CALL SYSTEM - ACT 287 AS AMENDED REQUEST===========

Serial Number--[1260170]

Message Type--[INSUFFICIENT NOTICE] Lead Time--[0024]   Tape Channel #--[0824015][0069]

County--[WESTMORELAND] Municipality--[DERRY TWP]

Work Site--[ ** **SR 0982** ** ]

      Nearest Intersection--[LATROBE DERRY RD]
      Latitude/Longitude--[          /          ]

Location Information--
[AT BOX 238, BRADENVILLE, PA. 15620.  APPX 1/4 MILE FROM THE INTER ON THE S SIDE
 OF THE ST.]

Site Marked in White--[Y]    PennDOT Permit Number--[]

Type of Work--[INSTL NEW SWR & WTR LINES] Depth--[5-10FT]
Method of Excavation--[BH]  Extent of Excavation--[24IN WIDE]
Working in--
Street--[ ]    Sidewalk--[ ]    Public Prop--[ ]    Pvt Prop--[X]
      Other--[]  Owner/Done for--[DAVE KRAWTZ]

Proposed Dig Date--[09-MAY-02] Dig Time--[0800]
Lawful Dig Dates---[09-MAY-02][0800] Through [20-MAY-02][0800]

Contractor--[KELLYS EXCAVATING] Contractor/Utility--[Y]
Address-----[RR 1 BOX 270]
City--------[BRADENVILLE] State--[PA] Zip--[15620]

Caller--[MIKE KELLY] Phone--[724-537-2572]
E-mail address--[none]
Person to Contact--[MIKE KELLY] Phone--[724-537-2572]
Best Time to Call--[AFTER 2000]

Prepared--[06-MAY-02]  at  [0751]  by  [PATTI MCINTOSH]

Remarks--
[********** == R E N O T I F I C A T I O N == **********
 ATTN UTILS: PLEASE RESPOND TO MARK ANY CONFLICTS & TO KARL SYSTEM. --05/08/2002
 08:24AM DCB CH 015]

BB1=REA ENERGY COOP DEE=DERRY TWP       EO1=DERRY TMA NWDY  FB =VERIZON PA-PGH
FX1=BLAIRSVILLE MA  GA =DOMINION GRBG   HU1=ADELPHIA HGLD   JUL=
KI =ALLEG PWR LTRB  PEC=PENNECO PIPELIN QS =QWEST COMM      SU =SUNOCO PIPELINE




