
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 9519581 
Transmit      Date: 10/14/09      Time: 01:25 PM           Op: mdalic 
Original Call Date: 10/14/09      Time: 01:18 PM           Op: mdalic 
Work to Begin Date: 10/16/09      Time: 01:30 PM 

Place: SE 
Address:             Street: 4TH ST SE 
Nearest Intersecting Street: K ST SE 

Type of Work: DIGGING FOOTERS & PIERS FOR HSES 
Extent of Work: 947 - 908  LOC ENTIRE FRONT OF PROPS 
: BTWN K ST & I ST 
Remarks:  

Company      : CUCO & SON 
Contact Name : CAROLINA LANAUZE               Fax          : (703)450-9735 
Contact Phone: (703)540-1855                  Ext          :  
Alt. Contact : TONY                           Alt. Phone   : (703)928-7507 
Work Being Done For: EYA 
State: DC              County: SE 
MPG:  N 
Caller    Provided:  Map Name: SE    Map#: 5528 Grid Cells: K10 
Computer Generated:  Map Name: SE    Map#: 5528 Grid Cells: K10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 38.8812506Lon: -77.0062838 SE Lat: 38.8750001Lon: -77.0000001 
Explosives: N 
ACSI04     DCC01      FBLD01     LTC02      MFN03      PEP06      QGS03 
 QWESTM02   TPC02 
Send To: VDC       Seq No: 0119   Map Ref:  
         WASA02            0114 
         WGL07             0119 
