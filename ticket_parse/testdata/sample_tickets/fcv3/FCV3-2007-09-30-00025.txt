
UTIL11 00015 VUPSb 09/30/07 14:48:11 B727300046-00B          NORMAL

Ticket No:  B727300046-00B                        NEW  GRID NORM LREQ
Transmit        Date: 09/30/07   Time: 02:48 PM   Op: WCSHOCKLEY1
Call            Date: 09/30/07   Time: 02:45 PM
Due By          Date: 10/04/07   Time: 07:00 AM
Update By       Date: 10/19/07   Time: 11:59 PM
Expires         Date: 10/24/07   Time: 07:00 AM
Old Tkt No: B727300046
Original Call   Date: 09/30/07   Time: 02:45 PM   Op: WCSHOCKLEY1

City/Co:BEDFORD               Place:MONETA                              State:VA
Address:     203              Street: OAK HOLLOW RD

Type of Work:   UNDER GROUND TO DUCK TO DAWN LIGHT
Work Done For:  AEP
Excavation area:122S T/L ON 655 (AT SHOP RITE) T/R 823 T/L MEADOW POINT RD
                (BEEDHWOOD SHORE SUBDIVISION) T/R BENT TREE T/R OAK HOLLOW RD
                2ND LOT ON THE RIGHT
Instructions:   LOCATE AND MARK 20' AROUND STAKE

Whitelined: N   Blasting: N   Boring: N

Company:        APPALACHIAN POWER                         Type: UTIL
Co. Address:    96 OLD FRANKLIN TPKE  First Time: N
City:           ROCKY MOUNT  State:VA  Zip:24151
Company Phone:  540-489-2553
Contact Name:   CATHY SHOCKLEY              Contact Phone:540-489-2553
Email:          clshockley@aep.com
Field Contact:  MIKD BOYD
Fld. Contact Phone:540-588-0028

Mapbook:
Grids:    3708A7938A     3708B7938A     3708C7938A     3708A7939C
Grids:    3708A7939D     3708B7939C     3708B7939D     3708C7939D

Members:
AEP111 = AEP-ROANOKE (AEP)              BCA176 = BEDFORD COUNTY PSA (BCA)
CMC320 = COMCAST (CMC)                  SEC027 = SOUTHSIDE ELECTRIC COOPER(SEC)
VZN172 = VERIZON COMM. (VZN)

Seq No:   15 B

