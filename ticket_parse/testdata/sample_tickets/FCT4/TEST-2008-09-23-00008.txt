
COMCEN  00001 GAUPC 09/23/08 12:46:47 08198-205-001-000 NORMAL       
UNDERGROUND NOTIFICATION             
Notice : 08198-205-001 Date: 08/19/08  Time: 16:16  Revision: 000 

State : GA County: GWINNETT      Place: DULUTH                                  
Addr  : From: 3400   To:        Name:    SUMMIT RIDGE                   PKWY    
Near  : Name:    PLEASANT HILL                  RD  

Subdivision:                                         
Locate:  ENTIRE PROPERTY                                                       

Grids       : 3359D8409A 3359C8409A 3359D8410D 3359C8410D 
Work type   : INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION                       

Start date: 08/22/08 Time: 07:00 Hrs notc : 000
Legal day : 08/22/08 Time: 07:00 Good thru: 09/09/08 Restake by: 09/04/08
RespondBy : 08/21/08 Time: 23:59 Duration : 1 YEAR     Priority: 3
Done for  : UTILITIES PROTECTION CENTER, INC        
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE
        : TS                                                                  
        : *** WILL BORE Road                          

Company : UPC                                       Type: CONT                
Co addr : 3400 SUMMIT RIDGE PKWY                   
City    : DULUTH                         State   : GA Zip: 30096              
Caller  : CC EXCAVATOR                   Phone   :  404-444-3606              
Fax     :                                Alt. Ph.:  770-476-6042              
Email   : IWEATHERS@GAUPC.COM                                                 
Contact :                                                           

Submitted date: 08/19/08  Time: 16:16  Oper: 209
Mbrs : AGLN01 GWI91 JCK70 GWI90 COMCEN BSNE AGL114 GP263 
-------------------------------------------------------------------------------



