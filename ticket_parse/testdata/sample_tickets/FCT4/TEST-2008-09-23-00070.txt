
AGL114  00006 GAUPC 09/23/08 12:46:51 08198-400-001-000 LPMEET       
LARGE PROJECT MEETING NOTIFICATION   
Notice : 08198-400-001 Date: 08/19/08  Time: 16:37  Revision: 000 

State : GA County: GWINNETT      Place: DULUTH                                  
Addr  : From: 3400   To:        Name:    SUMMIT RIDGE                   PKWY    
Near  : Name:    PLEASANT HILL                  RD  

Subdivision:                                         
Locate:  ENTIRE PROPERTY                                                       

Grids       : 3359D8409A 3359C8409A 3359D8410D 3359C8410D 
Work type   : INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION                       
ScopeOfWork : WORKING ON SUMMIT RIDGE PKWY AT PLEASANT HILL RD.                

Meeting Date      : 08/25/08 Time: 12:00 
Meeting Respond By: 08/22/08 Time: 23:59
Meeting Location  : MEETING LOCATION: WAFFLE HOUSE                             
LP Contact        : MICHAEL HOYT              Phone   : 7706234332 Ext:  
Contact Email     : MHOYT@GAUPC.COM                                            

RespondBy : 08/27/08 Time: 23:59 Duration : 1 YEAR     Priority: 1
Done for  : UTILITIES PROTECTION CENTER, INC        
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE
        : TS                                                                  
        : *** WILL BORE Road                          
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : UPC                                       Type: CONT                
Co addr : 3400 SUMMIT RIDGE PKWY                   
City    : DULUTH                         State   : GA Zip: 30096              
Caller  : CC EXCAVATOR                   Phone   :  404-444-3606              
Fax     :                                Alt. Ph.:  770-476-6042              
Email   : IWEATHERS@GAUPC.COM                                                 
Contact :                                                           

Submitted date: 08/19/08  Time: 16:37  Oper: 209
Mbrs : AGLN01 GWI91 JCK70 GWI90 COMCEN BSNE AGL114 GP263 
-------------------------------------------------------------------------------



