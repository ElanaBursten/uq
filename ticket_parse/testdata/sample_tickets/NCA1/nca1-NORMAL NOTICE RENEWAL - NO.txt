
COMSR2 00041 USAN 06/16/09 16:23:53 0178241 NORMAL NOTICE RENEWAL

Message Number: 0178241 Received by USAN at 16:23 on 06/16/09 by KRD

Work Begins:    06/18/09 at 16:45   Notice: 020 hrs      Priority: 2

Expires: 07/14/09 at 17:00   Update By: 07/10/09 at 16:59

Caller:         LEAUNDRA KASPRZAK        
Company:        PRESTON PIPELINE INCORPORATED      
Address:        133 BATHELO, SAN JOSE                   
City:           MILPITAS                      State: CA Zip: 95170
Business Tel:   408-262-1418                  Fax: 408-262-1870                
Email Address:                                                              

Nature of Work: TR TO INST STORM DR,SANI SWR,WELL,GAS*  
Done for:       BN BUILDERS                   Explosives: N
Foreman:        UNKNOWN                  
Field Tel:                                    Cell Tel:                        
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: PENDING                  
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
    FR INT/O LINCOLN ST & 2ND ST GO 500' S ON W/SI/O LINCOLN ST & FR
    SAME BEG PT GO 500' W ON S/SI/O 2ND ST (INCL ENT AREA WITH & THE ST
    & THE SI/WLK AREAS)

Place: SAN RAFAEL                   County: MARIN                State: CA

Long/Lat Long: -122.527313 Lat:  37.96825  Long: -122.522903 Lat:  37.971722 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
RENEWAL OF TICKET RN#001193 ORIG DATE 05/19/2009 RE-MARK NO-KRD
06/16/2009 


Sent to:
ATTCAL = AT&T TRANSMISSION CAL        BRIDGE = GOLDEN GATE BRIDGE           
COMSRF = COMCAST-SAN RAFAEL           CTYSRF = CITY SAN RAFAEL&SRSD         
MARNMU = MARIN MUNI WTR               PBTMRN = PACIFIC BELL MARIN           
PGESRF = PGE DISTR SAN RAFAEL         TERDEX = TERRADEX INC.                


