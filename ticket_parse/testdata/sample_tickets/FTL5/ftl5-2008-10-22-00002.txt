
--===SEE838999156780171===MIX===
Content-Type: text/plain
Content-Transfer-Encoding: quoted-printable





KORTERRA JOB MARY01-SMTP-1 082591423 Seq: 1 10/22/2008 10:52:50
U07        00029 TNOCS 09/15/08 12:03:00 082591423 EMERGENCY GRID

=3D=3D=3D=3D=3D=3D=3D=3D//  TNOCS LOCATE REQUEST  //=3D=3D=3D=3D=3D=3D=3D=3D

TICKET NUMBER--[082591423]   =3D=3D E M E R G E N C Y =3D=3D
OLD TICKET NUM-[]

MESSAGE TYPE---[EMERGENCY] LEAD TIME--[0]
PREPARED-------[09/15/08]  AT  [1203]  BY  [MELODYG]

CONTRACTOR--[CITY OF ALCOA]
CALLER--[JUNIOR NEELEY]
ADDRESS-----[441 NORTH HALL ROAD]
CITY--------[ALCOA]   STATE--[TN]   ZIP--[37701]
CALL BACK---[(865) 216-0603 CELL]
PHONE--[(865) 981-4156]
CONTACT-----[JUNIOR NEELEY]   PHONE--[(865) 981-4156]
CONTACT FAX-[(865) 981-4103]

WORK TO BEGIN--[09/15/08] AT [1215]      STATE--[TN]
COUNTY---[BLOUNT]  PLACE--[ALCOA]

ADDRESS--[]   STREET--[][WRIGHT][RD][]
NEAREST INTERSECTION--------[CHERRY ST]
LATITUDE--[35.7980239999992]    LONGITUDE--[-83.9751754078957]
SECONDARY LATITUDE--[35.7976062368411] SECONDARY LONGITUDE--[-83.974816855264]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[EMERGENCY - UTIL IS REQUIRED BY LAW TO MARK U/G FACILITIES WITHIN 2 HOURS     
[OF NOTIFICATION....CREW IS ON SITE...AT INTER, MARK BOTH SIDES OF RD          
[BETWEEN THE WHITE ARROWS...NOTE: TOC MAP SHOWS DIG ST AS N WRIGHT RD...       

WORK TYPE--[WATER MAIN, REPAIR AND/OR REPL]
DONE FOR--[CITY OF ALCOA]
EXTENT-----[]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [36D              ] [36E              ]

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME
  ---------- --------------------------   ---------- --------------------------
  ALU        ALCOA UTIL - ELECTRIC        ALW        ALCOA UTILITIES - WATER...
  B03        ATT/D-KNOX (615)661-3758     CM         MARYVILLE UTILITIES, CI...
  U07        ATMOS ENERGY (UNITED CI...



--===SEE838999156780171===MIX===--


