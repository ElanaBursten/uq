
NOTICE OF INTENT TO EXCAVATE                                    NO SHOW
Ticket No: 00169999
Transmit      Date: 03/14/02      Time: 01:03    Op: france
Original Call Date: 03/11/02      Time: 15:56    Op: france
Work to Begin Date: 03/13/02      Time: 04PM

Place: STAFFORD
Address: 5           Street: PIN OAK CT
Nearest Intersecting Street: KRISMATT CT

Type of Work: INST A FENCE
Extent of Work:   LOC ENTIRE PROP AT ABOVE ADDRESS
Remarks: NO T/TECH
:                                                        Fax: (540)775-2681

Company     : TRIPLE K FENCE CORP
Contact Name: KATHY LAKE            Contact Phone: (540)775-3154
Alt. Contact: ANSWERING SERVICE     Alt. Phone   : (540)374-9283
Work Being Done For: MR FLYNN
State: VA              County: STAFFORD
Map: FAUQ  Page: 019   Grid Cells: E06,E05
Explosives: N
CTL03      PRV01      SCU01
Send To: CMG   01  Seq No: 0001   Map Ref:
         VPW   01          0001