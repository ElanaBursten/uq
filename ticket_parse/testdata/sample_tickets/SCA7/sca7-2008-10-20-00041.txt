
SDG09  00040A USAS 10/20/08 14:31:18 A82120219-04A NORM UPDT GRID

Ticket : A82120219 Date: 10/20/08 Time: 14:30 Oper: ALFC Chan: WEB
Old Tkt: A82120219 Date: 07/30/08 Time: 08:28 Oper: ALFC Revision: 04A
State: CA County: SAN DIEGO            Place: RANCHO BERNARDO
Locat: WORKING 0 FT TO 10 FT ON THE EAST SHOULDER OF THE N/ BOUND 15 ESCONDIDO
     : FRWY. STARTING APPROX. 6,300 FT S/ OF RANCHO BERNARDO RD AND EXTENDING N/
     : 200 FT.
     : **PLEASE MARK DELINEATED AREA OR CONTACT KAREN AT 951-360-8103 TO CLEAR
     : AREA IF NO CONFLICT.
Address: ESCONDIDO 15 FWY NORTH
X/ST 1 : RANCHO BERNARDO RD
Grids: 1169J044     1169J052     1170A043     1170A051      Delineated: Y
Lat/Long  : 33.006668/-117.080609 33.006441/-117.079672
          : 33.005321/-117.080935 33.005093/-117.079999
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N

Re-Mark: N

Work : METAL BEAM GUARD RAIL
Work date: 10/20/08 Time: 14:30 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING        Permit   : NOT REQUIRED
Done for : FCI S5808-100

Company: ALCORN FENCE COMPANY           Caller: CARMEN LOPEZ
Address: 6445 PEDLEY RD
City&St: RIVERSIDE, CA                  Zip: 92509      Fax: 951-360-8105
Phone: 951-360-8103 Ext:      Call back: 7:30 - 4:00
Formn: UNKNOWN              Phone: 951-360-8103
Email: KPATIN@ALCORN-FENCE.COM

                                  COMMENTS

**RESEND**CALLER ADVISES NO SHOW FROM GAS, CITY, STREET LIGHTS, TRAFFIC LIGHTS,
FIBER - : NEXTG FIBER, SAN DIEGO GAS TRANSMISSION, SAN DIEGO G & E, CITY OF SAN
DIEGO STREET & TRAFFIC ELEC. CREW IS OUT ON SITE & DIGGING. PLEASE RESPOND NOW
TO SITE &MARK OR CALL CARMEN AT 951-360-8103 TO CLEAR IF NO CONFLICT. PER CARMEN
LOPEZ--[ALFC 08/05/08 11:58]
**RESEND**UPDATE ONLY-WORK CONT PER NORINE MOORE--[ALFC 08/29/08 07:52]
**RESEND**UPDATE ONLY-WORK CONT PER NORINE MOORE--[ALFC 09/24/08 12:14]
**RESEND**UPDATE ONLY-WORK CONT PER NORINE MOORE--[ALFC 10/20/08 14:30]

Mbrs : ATTD28SD      NEXTG  QWESTCA       SDG02  SDG05  SDG09  SND01  SND02
TWCSD  USDG09
