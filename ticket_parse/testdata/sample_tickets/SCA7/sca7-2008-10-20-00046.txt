
SDG09  00045A USAS 10/20/08 15:07:59 A82941381-00A NORM NEW GRID

Ticket : A82941381 Date: 10/20/08 Time: 15:03 Oper: ADS Chan: 100
Old Tkt: A82941381 Date: 10/20/08 Time: 15:07 Oper: ADS Revision: 00A
State: CA County: SAN DIEGO            Place: SAN DIEGO
Locat: CALLER STATES ADDRESS IS APPROX 1000FT S/OF X/ST, DOES NOT CORRESPOND
     : WITH MAP
Address: 9404 CABOT DR
X/ST 1 : ARJONS DR
Grids: 1209C063     1209C071                                Delineated: Y
Lat/Long  : 32.892310/-117.144293 32.891852/-117.143516
          : 32.891634/-117.144692 32.891175/-117.143915
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : INSTALL BACK FLOW
Work date: 10/23/08 Time: 08:00 Hrs notc: 064 Work hrs: 064 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : AMERICAN AUTOMATIC FIRE SUPPRESION

Company: P K EXCAVATION                 Caller: PAUL KAY
Address: 12125 LAKESIDE AVE
City&St: LAKESIDE, CA                   Zip: 92040      Fax: 619-390-0643
Phone: 619-390-8302 Ext:      Call back: 630AM - 5PM
Formn: PAUL                 Phone: 619-318-4648
Email: PKEXCAVATION@AOL.COM
Mbrs : ATTD28SD      LVL3CM SDG06  SDG09  SND01  SND02  TWCSD  UATLSD USDG09
