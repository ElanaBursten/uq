KN     00009 POCS 04/05/02 10:16:46 0930747-001 INSUFFICIENT NOTICE RXMIT PLACE

           ********** == R E N O T I F I C A T I O N == **********

===========PENNSYLVANIA ONE CALL SYSTEM - ACT 287 AS AMENDED REQUEST===========

Serial Number--[0930747]

Message Type--[INSUFFICIENT NOTICE] Lead Time--[0023]   Tape Channel #--[1014018][0129]

County--[WESTMORELAND] Municipality--[NORTH HUNTINGDON TWP]

Work Site--[ 606** **OAKHURST**DR** ]

      Nearest Intersection--[DAILY DR]
      Latitude/Longitude--[          /          ]

Location Information--
[LINCOLN POINTE DEVEL. OAKHURST DR IS A CUL-DE-SAC AT TOP OF HILL. THE NEW
 MAILBOX WILL BE AT SAME LOCATION OF OLD MAILBOX.]

Site Marked in White--[N]    PennDOT Permit Number--[]

Type of Work--[FOOTER FOR MAILBOX] Depth--[36IN]
Method of Excavation--[UNKNOWN]  Extent of Excavation--[UNKNOWN]
Working in--
Street--[ ]    Sidewalk--[ ]    Public Prop--[ ]    Pvt Prop--[X]
      Other--[FRONT]  Owner/Done for--[NICHOLAS COLINEAR]

Proposed Dig Date--[08-APR-02] Dig Time--[1000]
Lawful Dig Dates---[08-APR-02][1000] Through [17-APR-02][1000]

Contractor--[NICHOLAS COLINEAR] Contractor/Utility--[N]
Address-----[606 OAKHURST DR]
City--------[NORTH HUNTINGDON] State--[PA] Zip--[15642]

Caller--[NICHOLAS COLINEAR] Phone--[724-863-3577]
FAX--[412-566-2229]  E-mail address--[nc@mrpitt.com]
Person to Contact--[NICHOLAS COLINEAR] Phone--[412-566-2100] Ext--[226]
Best Time to Call--[0800-1730]

Prepared--[03-APR-02]  at  [1004]  by  [KAREN TURNELL]

Remarks--
[********** == R E N O T I F I C A T I O N == **********
 ATTN VERIZON AND DOMINION, YOU RESPONDED WITH A CONFLICT FOR THIS LOCATION,
 PLEASE MARK LINES IF INVOLVED AND UPDATE RESPONSE IN KARL SYSTEM. ALL OTHER
 UTILS TO MARK LINES AND RESPOND THRU THE KARL SYSTEM WHETHER INVOLVED OR NOT
 AND UPDATED RESPONSE IF ALREADY MARKED. --04/05/2002 10:15AM TLC CH 018]

ACC=ALGHNY COM CONN CGR=COL GAS GRNSBRG ED1=N HNTNGDN TMA   FA =VERIZON PA INC-
GA =DOMINION GRBG   JR =WSTMLD CMA GRBG JUL=                KN =ALLEG PWR JNTT
MI =WORLDCOM        NHT=N HUNTINGDN TWP PG1=DOMINION POCS   QS =QWEST COMM
