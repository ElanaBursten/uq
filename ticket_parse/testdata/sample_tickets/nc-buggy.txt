
DPC18*  00005 NCOCa 09/25/02 10:01:06 A0534673-00A NORM NEW PLCE

North Carolina One Call

Ticket : A0534673 Date: 09/25/02 Time: 09:58 Oper: CRH Chan:999
Old Tkt: A0534673 Date: 09/25/02 Time: 10:00 Oper: CRH Rev :00A

State: NC Cnty: UNION Place: WAXHAW In/Out: B
Subdivision: ALMA VILLAGE

Address :
Street  : CHRISTINE LN   Intersection: N
Cross 1 : ALMA BLVD
Location: OUT:   HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET?    1/4
MILE             LOCATE THE ENTIRE PROP OF LOT # 3-A  &  BOTH SIDES OF THE RD
WILL BE A RD BORE      WORKS TO BEGIN IN 48HRS
:
Grids   :

Work type:INSTALLING NAT. GAS
Work date:09/27/02  Time: 09:58  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: Y  Railroad: N     Emergency: N
Duration: 1 DAY  Done for: NCNG

Company : DISTRIBUTION CONSTRUCTION CO  Type: CONT
Co addr : 720 HILLSBORO ST
City    : FAYETTEVILLE State: NC Zip: 28301
Caller  : PAM CAIN Phone: 910-484-4440
Contact : SAME Phone:
BestTime:

Submitted date: 09/25/02 Time: 10:00
Members: ATT01  BEC01  CSR01  CVM01* DP72N* DPC18* GTE02* IDM01  MCT02* NCN07
Members: PDP02  DPC18*  SCR32


