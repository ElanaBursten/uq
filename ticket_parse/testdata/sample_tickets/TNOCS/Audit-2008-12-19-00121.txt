
From TOCS
UTILNASH
Utiliquest - (Nashville)
Audit For 12/19/2008

For Code UTILNASH
Type  Seq#  Ticket                    Status                                
----  ----  ------------------------  ------------------------------------- 
 *    0043  083430147                 Delivered                           
 *    0044  083430149                 Delivered                           
 *    0045  083430151                 Delivered                           
 *    0046  083430161                 Delivered                           
 *    0047  083430171                 Delivered                           
 *    0048  083430167                 Delivered                           
 *    0049  083430177                 Delivered                           
 *    0050  083430178                 Delivered                           
 *    0051  083430185                 Delivered                           
 *    0052  083430194                 Delivered                           
 *    0053  083430196                 Delivered                           
 *    0054  083430202                 Delivered                           
 *    0055  083430205                 Delivered                           
 *    0056  083430208                 Delivered                           
 *    0057  083430212                 Delivered                           
 *    0058  083430218                 Delivered                           
 *    0059  083430221                 Delivered                           
 *    0060  083430223                 Delivered                           
 *    0061  083430225                 Delivered                           
 *    0062  083430230                 Delivered                           
 *    0063  083430233                 Delivered                           
 *    0064  083430231                 Delivered                           
 *    0065  083430239                 Delivered                           
 *    0066  083430237                 Delivered                           
 *    0067  083430242                 Delivered                           
 *    0068  083430244                 Delivered                           
 *    0069  083430246                 Delivered                           
 *    0070  083430250                 Delivered                           
 *    0071  083430255                 Delivered                           
 *    0072  083430262                 Delivered                           
 *    0073  083430261                 Delivered                           
 *    0074  083430265                 Delivered                           
 *    0075  083430267                 Delivered                           
 *    0076  083430275                 Delivered                           
 *    0077  083430280                 Delivered                           
 *    0078  083430286                 Delivered                           
 *    0079  083430291                 Delivered                           
 *    0080  083430294                 Delivered                           
 *    0081  083430292                 Delivered                           
 *    0082  083430314                 Delivered                           
 *    0083  083430321                 Delivered                           
 *    0084  083430322                 Delivered                           
 *    0085  083430324                 Delivered                           
 *    0086  083430327                 Delivered                           
 *    0087  083430330                 Delivered                           
 *    0088  083430337                 Delivered                           
 *    0089  083430338                 Delivered                           
 *    0090  083430342                 Delivered                           
 *    0091  083430346                 Delivered                           
 *    0092  083430350                 Delivered                           
 *    0093  083430348                 Delivered                           
 *    0094  083430354                 Delivered                           
 *    0095  083430357                 Delivered                           
 *    0096  083430359                 Delivered                           
 *    0097  083430362                 Delivered                           
 *    0098  083430363                 Delivered                           
 *    0099  083430373                 Delivered                           
 *    0100  083430364                 Delivered                           
 *    0101  083430377                 Delivered                           
 *    0102  083430384                 Delivered                           
 *    0103  083430384                 Delivered                           
 *    0104  083430364                 Delivered                           
 *    0105  083430377                 Delivered                           
 *    0106  083430373                 Delivered                           
 *    0107  083430362                 Delivered                           
 *    0108  083430363                 Delivered                           
 *    0109  083430359                 Delivered                           
 *    0110  083430357                 Delivered                           
 *    0111  083430354                 Delivered                           
 *    0112  083430348                 Delivered                           
 *    0113  083430350                 Delivered                           
 *    0114  083430346                 Delivered                           
 *    0115  083430342                 Delivered                           
 *    0116  083430338                 Delivered                           
 *    0117  083430337                 Delivered                           
 *    0118  083430330                 Delivered                           
 *    0119  083430327                 Delivered                           
 *    0120  083430324                 Delivered                           
 *    0121  083430322                 Delivered                           
 *    0122  083430321                 Delivered                           
 *    0123  083430314                 Delivered                           
 *    0124  083430292                 Delivered                           
 *    0125  083430294                 Delivered                           
 *    0126  083430291                 Delivered                           
 *    0127  083430286                 Delivered                           
 *    0128  083430280                 Delivered                           
 *    0129  083430275                 Delivered                           
 *    0130  083430267                 Delivered                           
 *    0131  083430265                 Delivered                           
 *    0132  083430261                 Delivered                           
 *    0133  083430262                 Delivered                           
 *    0134  083430255                 Delivered                           
 *    0135  083430250                 Delivered                           
 *    0136  083430246                 Delivered                           
 *    0137  083430242                 Delivered                           
 *    0138  083430244                 Delivered                           
 *    0139  083430237                 Delivered                           
 *    0140  083430239                 Delivered                           
 *    0141  083430233                 Delivered                           
 *    0142  083430231                 Delivered                           
 *    0143  083430230                 Delivered                           
 *    0144  083430225                 Delivered                           
 *    0145  083430223                 Delivered                           
 *    0146  083430221                 Delivered                           
 *    0147  083430218                 Delivered                           
 *    0148  083430212                 Delivered                           
 *    0149  083430208                 Delivered                           
 *    0150  083430205                 Delivered                           
 *    0151  083430202                 Delivered                           
 *    0152  083430196                 Delivered                           
 *    0153  083430194                 Delivered                           
 *    0154  083430185                 Delivered                           
 *    0155  083430178                 Delivered                           
 *    0156  083430177                 Delivered                           
 *    0157  083430167                 Delivered                           
 *    0158  083430171                 Delivered                           
 *    0159  083430161                 Delivered                           
 *    0160  083430151                 Delivered                           
 *    0161  083430149                 Delivered                           
 *    0162  083430147                 Delivered                           

Normal    : 120
Resend    : 120
Emergency : 0
Failed    : 0
Total for UTILNASH: 120



Legend
-----------------------
  - Normal
* - Resend
! - Emergency


