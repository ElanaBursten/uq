

FROM TNOCS
UTILIQUEST - (NASHVILLE)
UTILNASH
AUDIT FOR 1/19/2009

FOR CODE UTILNASH

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  090190001                 DELIVERED
      0002  090190002                 DELIVERED
      0003  090190003                 DELIVERED
      0004  090190004                 DELIVERED
      0005  090190005                 DELIVERED
      0006  090190006                 DELIVERED
      0007  090190007                 DELIVERED
      0008  090190008                 DELIVERED
      0009  090190010                 DELIVERED
      0010  090190011                 DELIVERED
!     0011  090190012                 DELIVERED
!     0012  090190013                 DELIVERED
!     0013  090190014                 DELIVERED
!     0014  090190015                 DELIVERED
!     0015  090190029                 DELIVERED
!     0016  090190032                 DELIVERED
      0017  090190035                 DELIVERED
      0018  090190037                 DELIVERED
      0019  090190038                 DELIVERED
      0020  090190040                 DELIVERED
      0021  090190041                 DELIVERED
      0022  090190042                 DELIVERED
      0023  090190044                 DELIVERED
      0024  090190051                 DELIVERED
      0025  090190052                 DELIVERED
      0026  090190055                 DELIVERED
      0027  090190058                 DELIVERED
      0028  090190062                 DELIVERED
      0029  090190066                 DELIVERED
      0030  090190067                 DELIVERED
      0031  090190068                 DELIVERED
!     0032  090190071                 DELIVERED
      0033  090190073                 DELIVERED
      0034  090190078                 DELIVERED
      0035  090190081                 DELIVERED
      0036  090190082                 DELIVERED
      0037  090190085                 DELIVERED
      0038  090190087                 DELIVERED
      0039  090190092                 DELIVERED
      0040  090190096                 DELIVERED
      0041  090190104                 DELIVERED
      0042  090190112                 DELIVERED
      0043  090190117                 DELIVERED
!     0044  090190119                 DELIVERED
      0045  090190122                 DELIVERED
!     0046  090190126                 DELIVERED
      0047  090190128                 DELIVERED
      0048  090190132                 DELIVERED
      0049  090190136                 DELIVERED
      0050  090190137                 DELIVERED
      0051  090190141                 DELIVERED
      0052  090190143                 DELIVERED
      0053  090190146                 DELIVERED
      0054  090190147                 DELIVERED
      0055  090190148                 DELIVERED
      0056  090190149                 DELIVERED
      0057  090190152                 DELIVERED
      0058  090190154                 DELIVERED
      0059  090190155                 DELIVERED
      0060  090190158                 DELIVERED
      0061  090190159                 DELIVERED
      0062  090190160                 DELIVERED
      0063  090190161                 DELIVERED
      0064  090190165                 DELIVERED
      0065  090190168                 DELIVERED
      0066  090190169                 DELIVERED
      0067  090190172                 DELIVERED
!     0068  090190173                 DELIVERED
      0069  090190176                 DELIVERED
      0070  090190180                 DELIVERED
      0071  090190181                 DELIVERED
      0072  090190192                 DELIVERED
      0073  090190193                 DELIVERED
!     0074  090190196                 DELIVERED
!     0075  090190199                 DELIVERED
      0076  090190205                 DELIVERED
      0077  090190206                 DELIVERED
      0078  090190207                 DELIVERED
!     0079  090190216                 DELIVERED
      0080  090190218                 DELIVERED
      0081  090190221                 DELIVERED
      0082  090190223                 DELIVERED
      0083  090190231                 DELIVERED
!     0084  090190236                 DELIVERED
      0085  090190237                 DELIVERED
      0086  090190254                 DELIVERED
      0087  090190278                 DELIVERED
      0088  090190288                 DELIVERED
      0089  090190291                 DELIVERED
      0090  090190297                 DELIVERED
      0091  090190299                 DELIVERED
      0092  090190301                 DELIVERED
!     0093  090190306                 DELIVERED
      0094  090190308                 DELIVERED
      0095  090190317                 DELIVERED
      0096  090190319                 DELIVERED
      0097  090190324                 DELIVERED
!     0098  090190347                 DELIVERED
!     0099  090190349                 DELIVERED
      0100  090190350                 DELIVERED
!     0101  090190354                 DELIVERED
!     0102  090190357                 DELIVERED
      0103  090190362                 DELIVERED
!     0104  090190367                 DELIVERED
      0105  090190370                 DELIVERED
!     0106  090190375                 DELIVERED
      0107  090190376                 DELIVERED
      0108  090190378                 DELIVERED
      0109  090190379                 DELIVERED
      0110  090190380                 DELIVERED
      0111  090190381                 DELIVERED
      0112  090190383                 DELIVERED
      0113  090190384                 DELIVERED
      0114  090190386                 DELIVERED
      0115  090190387                 DELIVERED
      0116  090190390                 DELIVERED
      0117  090190391                 DELIVERED
      0118  090190393                 DELIVERED
      0119  090190394                 DELIVERED
      0120  090190397                 DELIVERED
      0121  090190398                 DELIVERED
      0122  090190404                 DELIVERED
      0123  090190405                 DELIVERED
      0124  090190406                 DELIVERED
      0125  090190411                 DELIVERED
      0126  090190413                 DELIVERED
      0127  090190415                 DELIVERED
      0128  090190418                 DELIVERED
      0129  090190422                 DELIVERED
!     0130  090190424                 DELIVERED
      0131  090190427                 DELIVERED
      0132  090190440                 DELIVERED
      0133  090190441                 DELIVERED
      0134  090190445                 DELIVERED
      0135  090190447                 DELIVERED
      0136  090190449                 DELIVERED
      0137  090190452                 DELIVERED
      0138  090190455                 DELIVERED
      0139  090190457                 DELIVERED
      0140  090190461                 DELIVERED
      0141  090190462                 DELIVERED
      0142  090190463                 DELIVERED
      0143  090190465                 DELIVERED
      0144  090190476                 DELIVERED
      0145  090190479                 DELIVERED
      0146  090190491                 DELIVERED
      0147  090190493                 DELIVERED
      0148  090190496                 DELIVERED
      0149  090190498                 DELIVERED
      0150  090190504                 DELIVERED
      0151  090190508                 DELIVERED
      0152  090190510                 DELIVERED
      0153  090190514                 DELIVERED
      0154  090190519                 DELIVERED
      0155  090190525                 DELIVERED
      0156  090190543                 DELIVERED
      0157  090190545                 DELIVERED
      0158  090190549                 DELIVERED
      0159  090190550                 DELIVERED
      0160  090190552                 DELIVERED
      0161  090190554                 DELIVERED
      0162  090190555                 DELIVERED
      0163  090190556                 DELIVERED
      0164  090190558                 DELIVERED
      0165  090190559                 DELIVERED
!     0166  090190563                 DELIVERED
      0167  090190564                 DELIVERED
      0168  090190565                 DELIVERED
      0169  090190568                 DELIVERED
      0170  090190569                 DELIVERED
      0171  090190571                 DELIVERED
      0172  090190573                 DELIVERED
      0173  090190576                 DELIVERED
      0174  090190577                 DELIVERED
      0175  090190578                 DELIVERED
      0176  090190579                 DELIVERED
      0177  090190580                 DELIVERED
      0178  090190581                 DELIVERED
      0179  090190584                 DELIVERED
      0180  090190585                 DELIVERED
      0181  090190586                 DELIVERED
      0182  090190588                 DELIVERED
      0183  090190589                 DELIVERED
      0184  090190590                 DELIVERED
      0185  090190593                 DELIVERED
      0186  090190594                 DELIVERED
      0187  090190596                 DELIVERED
      0188  090190597                 DELIVERED
      0189  090190599                 DELIVERED
      0190  090190600                 DELIVERED
      0191  090190601                 DELIVERED
!     0192  090190602                 DELIVERED
      0193  090190603                 DELIVERED
      0194  090190605                 DELIVERED
      0195  090190606                 DELIVERED
      0196  090190607                 DELIVERED
      0197  090190609                 DELIVERED
!     0198  090190610                 DELIVERED
      0199  090190611                 DELIVERED
      0200  090190612                 DELIVERED
      0201  090190614                 DELIVERED
      0202  090190615                 DELIVERED
      0203  090190616                 DELIVERED
      0204  090190620                 DELIVERED
      0205  090190623                 DELIVERED
      0206  090190624                 DELIVERED
      0207  090190628                 DELIVERED
      0208  090190630                 DELIVERED
      0209  090190631                 DELIVERED
      0210  090190633                 DELIVERED
      0211  090190635                 DELIVERED
      0212  090190636                 DELIVERED
      0213  090190637                 DELIVERED
      0214  090190639                 DELIVERED
      0215  090190641                 DELIVERED
      0216  090190643                 DELIVERED
      0217  090190644                 DELIVERED
      0218  090190646                 DELIVERED
      0219  090190647                 DELIVERED
      0220  090190648                 DELIVERED
      0221  090190649                 DELIVERED
      0222  090190651                 DELIVERED
      0223  090190652                 DELIVERED
      0224  090190654                 DELIVERED
      0225  090190655                 DELIVERED
      0226  090190657                 DELIVERED
      0227  090190660                 DELIVERED
      0228  090190661                 DELIVERED
      0229  090190663                 DELIVERED
      0230  090190664                 DELIVERED
      0231  090190666                 DELIVERED
      0232  090190669                 DELIVERED
      0233  090190671                 DELIVERED
      0234  090190672                 DELIVERED
      0235  090190673                 DELIVERED
      0236  090190676                 DELIVERED
      0237  090190677                 DELIVERED
      0238  090190678                 DELIVERED
      0239  090190680                 DELIVERED
      0240  090190681                 DELIVERED
      0241  090190683                 DELIVERED
      0242  090190686                 DELIVERED
      0243  090190687                 DELIVERED
      0244  090190688                 DELIVERED
      0245  090190691                 DELIVERED
      0246  090190692                 DELIVERED
      0247  090190693                 DELIVERED
      0248  090190695                 DELIVERED
      0249  090190696                 DELIVERED
      0250  090190703                 DELIVERED
      0251  090190704                 DELIVERED
      0252  090190707                 DELIVERED
      0253  090190708                 DELIVERED
      0254  090190710                 DELIVERED
      0255  090190712                 DELIVERED
      0256  090190713                 DELIVERED
!     0257  090190714                 DELIVERED
      0258  090190716                 DELIVERED
      0259  090190717                 DELIVERED
      0260  090190719                 DELIVERED
      0261  090190720                 DELIVERED
      0262  090190721                 DELIVERED
      0263  090190722                 DELIVERED
      0264  090190725                 DELIVERED
      0265  090190726                 DELIVERED
      0266  090190728                 DELIVERED
!     0267  090190738                 DELIVERED
      0268  090190741                 DELIVERED
      0269  090190742                 DELIVERED
      0270  090190743                 DELIVERED
      0271  090190744                 DELIVERED
      0272  090190745                 DELIVERED
      0273  090190747                 DELIVERED
      0274  090190748                 DELIVERED
      0275  090190751                 DELIVERED
      0276  090190753                 DELIVERED
      0277  090190754                 DELIVERED
      0278  090190755                 DELIVERED
      0279  090190757                 DELIVERED
      0280  090190758                 DELIVERED
      0281  090190759                 DELIVERED
      0282  090190760                 DELIVERED
      0283  090190763                 DELIVERED
      0284  090190764                 DELIVERED
      0285  090190766                 DELIVERED
      0286  090190767                 DELIVERED
      0287  090190769                 DELIVERED
      0288  090190771                 DELIVERED
      0289  090190772                 DELIVERED
      0290  090190775                 DELIVERED
      0291  090190779                 DELIVERED
      0292  090190782                 DELIVERED
      0293  090190783                 DELIVERED
      0294  090190785                 DELIVERED
      0295  090190786                 DELIVERED
!     0296  090190788                 DELIVERED
      0297  090190791                 DELIVERED
      0298  090190799                 DELIVERED
      0299  090190803                 DELIVERED
      0300  090190805                 DELIVERED
      0301  090190807                 DELIVERED
      0302  090190808                 DELIVERED
!     0303  090190809                 DELIVERED
      0304  090190810                 DELIVERED
      0305  090190813                 DELIVERED
      0306  090190814                 DELIVERED
      0307  090190815                 DELIVERED
      0308  090190816                 DELIVERED
      0309  090190817                 DELIVERED
      0310  090190818                 DELIVERED
      0311  090190819                 DELIVERED
      0312  090190820                 DELIVERED
!     0313  090190827                 DELIVERED
      0314  090190831                 DELIVERED
      0315  090190832                 DELIVERED
      0316  090190833                 DELIVERED
      0317  090190835                 DELIVERED
!     0318  090190836                 DELIVERED
      0319  090190837                 DELIVERED
      0320  090190843                 DELIVERED
!     0321  090190845                 DELIVERED
      0322  090190847                 DELIVERED
      0323  090190855                 DELIVERED
!     0324  090190856                 DELIVERED
!     0325  090190858                 DELIVERED
      0326  090190860                 DELIVERED
      0327  090190864                 DELIVERED
      0328  090190866                 DELIVERED
      0329  090190869                 DELIVERED
      0330  090190870                 DELIVERED
      0331  090190872                 DELIVERED
!     0332  090190876                 DELIVERED
      0333  090190877                 DELIVERED
      0334  090190878                 DELIVERED
      0335  090190880                 DELIVERED
!     0336  090190881                 DELIVERED
      0337  090190887                 DELIVERED
      0338  090190888                 DELIVERED
      0339  090190892                 DELIVERED
      0340  090190894                 DELIVERED
      0341  090190895                 DELIVERED
      0342  090190898                 DELIVERED
      0343  090190900                 DELIVERED
      0344  090190903                 DELIVERED
      0345  090190906                 DELIVERED
!     0346  090190907                 DELIVERED
      0347  090190908                 DELIVERED
      0348  090190909                 DELIVERED
      0349  090190912                 DELIVERED
      0350  090190914                 DELIVERED
      0351  090190919                 DELIVERED
      0352  090190921                 DELIVERED
      0353  090190930                 DELIVERED
!     0354  090190934                 DELIVERED
      0355  090190936                 DELIVERED
      0356  090190940                 DELIVERED
!     0357  090190941                 DELIVERED
      0358  090190942                 DELIVERED
      0359  090190945                 DELIVERED
!     0360  090190946                 DELIVERED
      0361  090190956                 DELIVERED
      0362  090190974                 DELIVERED
      0363  090190986                 DELIVERED
      0364  090190990                 DELIVERED
      0365  090190997                 DELIVERED
      0366  090191014                 DELIVERED
      0367  090191016                 DELIVERED
      0368  090191017                 DELIVERED
      0369  090191018                 DELIVERED
      0370  090191019                 DELIVERED
      0371  090191021                 DELIVERED
      0372  090191023                 DELIVERED
      0373  090191025                 DELIVERED
      0374  090191026                 DELIVERED
      0375  090191027                 DELIVERED
      0376  090191028                 DELIVERED
      0377  090191030                 DELIVERED
      0378  090191033                 DELIVERED
      0379  090191034                 DELIVERED
      0380  090191046                 DELIVERED
!     0381  090191048                 DELIVERED
!     0382  090191051                 DELIVERED
      0383  090191052                 DELIVERED
      0384  090191055                 DELIVERED
!     0385  090191057                 DELIVERED
      0386  090191058                 DELIVERED
      0387  090191073                 DELIVERED
      0388  090191076                 DELIVERED
      0389  090191077                 DELIVERED
      0390  090191079                 DELIVERED
      0391  090191080                 DELIVERED
      0392  090191081                 DELIVERED
      0393  090191082                 DELIVERED
      0394  090191085                 DELIVERED
      0395  090191089                 DELIVERED
      0396  090191091                 DELIVERED
      0397  090191093                 DELIVERED
      0398  090191095                 DELIVERED
      0399  090191110                 DELIVERED
!     0400  090191114                 DELIVERED
      0401  090191116                 DELIVERED
      0402  090191117                 DELIVERED
      0403  090191119                 DELIVERED
!     0404  090191122                 DELIVERED
      0405  090191123                 DELIVERED
      0406  090191132                 DELIVERED
      0407  090191133                 DELIVERED
      0408  090191137                 DELIVERED
!     0409  090191139                 DELIVERED
      0410  090191143                 DELIVERED
      0411  090191144                 DELIVERED
      0412  090191145                 DELIVERED
      0413  090191146                 DELIVERED
      0414  090191147                 DELIVERED
      0415  090191148                 DELIVERED
      0416  090191150                 DELIVERED
      0417  090191151                 DELIVERED
      0418  090191152                 DELIVERED
      0419  090191153                 DELIVERED
      0420  090191154                 DELIVERED
      0421  090191155                 DELIVERED
      0422  090191157                 DELIVERED
      0423  090191160                 DELIVERED
      0424  090191163                 DELIVERED
      0425  090191164                 DELIVERED
      0426  090191168                 DELIVERED
      0427  090191170                 DELIVERED
      0428  090191170                 DELIVERED
      0429  090191181                 DELIVERED
      0430  090191191                 DELIVERED
      0431  090191193                 DELIVERED
      0432  090191195                 DELIVERED
!     0433  090191200                 DELIVERED
      0434  090191202                 DELIVERED
      0435  090191203                 DELIVERED
      0436  090191205                 DELIVERED
      0437  090191206                 DELIVERED
      0438  090191207                 DELIVERED
      0439  090191209                 DELIVERED
      0440  090191210                 DELIVERED
      0441  090191212                 DELIVERED
      0442  090191213                 DELIVERED
      0443  090191215                 DELIVERED
      0444  090191217                 DELIVERED
      0445  090191218                 DELIVERED
      0446  090191220                 DELIVERED
      0447  090191222                 DELIVERED
      0448  090191223                 DELIVERED
!     0449  090191224                 DELIVERED
!     0450  090191225                 DELIVERED
      0451  090191226                 DELIVERED
      0452  090191227                 DELIVERED
      0453  090191229                 DELIVERED
      0454  090191230                 DELIVERED
      0455  090191233                 DELIVERED
      0456  090191234                 DELIVERED
      0457  090191239                 DELIVERED
      0458  090191241                 DELIVERED
      0459  090191242                 DELIVERED
      0460  090191243                 DELIVERED
      0461  090191244                 DELIVERED
      0462  090191245                 DELIVERED
      0463  090191246                 DELIVERED
      0464  090191247                 DELIVERED
      0465  090191248                 DELIVERED
      0466  090191249                 DELIVERED
      0467  090191250                 DELIVERED
      0468  090191251                 DELIVERED
      0469  090191254                 DELIVERED
      0470  090191255                 DELIVERED
!     0471  090191256                 DELIVERED
      0472  090191257                 DELIVERED
      0473  090191271                 DELIVERED
      0474  090191277                 DELIVERED
      0475  090191278                 DELIVERED
!     0476  090191281                 DELIVERED
!     0477  090191283                 DELIVERED
!     0478  090191284                 DELIVERED
!     0479  090191285                 DELIVERED

NORMAL    : 425
RESEND    : 0
EMERGENCY : 54
FAILED    : 0
TOTAL FOR UTILNASH: 479




LEGEND
-----------------------
  - NORMAL
* - RESEND
! - EMERGENCY


