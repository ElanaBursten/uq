
UTIL13 00662 VUPSb 04/15/09 17:01:02 B910502289-00B          NORMAL

Ticket No:  B910502289-00B                        NEW  GRID NORM LREQ
Transmit        Date: 04/15/09   Time: 05:01 PM   Op: WBRODRIQUEZ
Call            Date: 04/15/09   Time: 04:58 PM
Due By          Date: 04/20/09   Time: 07:00 AM
Update By       Date: 05/04/09   Time: 11:59 PM
Expires         Date: 05/07/09   Time: 07:00 AM
Old Tkt No: B910502289
Original Call   Date: 04/15/09   Time: 04:58 PM   Op: WBRODRIQUEZ

City/Co:FAIRFAX               Place:A COUNTRY PLACE                     State:VA
Address:     813              Street: AARON CT
Cross 1:     MINBURN ST

Type of Work:   VZN: FTTP
Work Done For:  VERIZON
Excavation area:START IN FRONT BUT IN BETWEEN 813 AND 812 AARON CT AND END
                BEHIND 808 AASON CT. MARK APPRX. 480'
Instructions:   CALLER MAP REF: NONE
                VZN: FTTP WORK ORDER NUMBER 8A10058/77012/5845C

Whitelined: N   Blasting: N   Boring: N

Company:        SUNNY SOLUTIONS LLC                       Type: CONT
Co. Address:    9715 COVERED WAGON DR, M  First Time: N
City:           LAUREL  State:MD  Zip:20723
Company Phone:  301-604-0588
Contact Name:   BENISH RODRIGUEZ            Contact Phone:301-996-1530
Email:          be26cool@verizon.net
Field Contact:  JOSE RODRIGUEZ
Fld. Contact Phone:240-401-2423

Mapbook:  5404D2
Grids:    3859B7717B-43  3859B7717B-44  3859C7717B-03  3859C7717B-04
Grids:    3859C7717B-13  3859C7717B-14  3859C7717B-24  3859C7717B-34

Members:
COX902 = COX COMMUNICATIONS (COX)       DOM400 = DOMINION VIRGINIA POWER (DOM)
FCU901 = FAIRFAX COUNTY (FCU)           FCW902 = FAIRFAX WATER (FCW)
VZN102 = VERIZON (VZN)                  WGL904 = WASHINGTON GAS (WGL)

Seq No:   662 B

