

COX01  00011A USAS 02/02/06 07:29:47 A0270427-01A  GRID

Ticket : A0270427 Date: 02/02/06 Time: 07:27 Oper: EVA Chan: 100
Old Tkt: A0270427 Date: 01/27/06 Time: 09:57 Oper: LOR Revision: 01A
State: CA County: SAN DIEGO            Place: SAN DIEGO
Locat: FRM MIDDLE MEDIUM TO W/SIDE OF  SIDEWALK ON DEL SUR BLVD START APPROX
     : 200FT S/OF THE 905 FWY THEN GO N/TO INTER/OF DEL SUR BLVD & DEL SOL BLVD
Address: DEL SUR BLVD
X/ST   : 905 FWY
Grids: 1350E0134    1350E02123                              Delineated: Y

Work : INSTALL 30IN RECYCLE PIPE LINE
Work date: 02/02/06 Time: 07:29 Hrs notc: 000 Work hrs: 001 Priority: 2
Instruct : REQUEST REMARKS        Permit   : NOT REQUIRED
Done for : OTAY WTR DIST

Company: ORTIZ CORP                     Caller: VICTOR
Address: 788 ENERGY WAY
City&St: CHULA VISTA, CA                Zip: 91911
Phone: 619-482-2076
Formn: OBDULIO MARTINEZ

                                  COMMENTS

Lat/Long  : 32.573448/-117.058623 32.573448/-117.057489
          : 32.567181/-117.058623 32.567181/-117.057489
Caller GPS:
REF EXP TICKET A003340910, UPDATE ONLY-WORK CONT PER LUCY ORTIZ--[LOR 01/27/06
09:57]
**RESEND**REQUEST REMARKS-WORK CONT CALL O MARTINEZ AT  858-967-6445  FOR REMARK
INSTRUCTION PER LUCY--[EVA 02/02/06 07:29]

Mbrs : ATTATL DANELAENG     PTT28  SDG07  SND01  SND02  SPRINT UCOX01


