
From TNOCS
Audit For 1/9/2013
Output UTILNASH Email (tickets.utiliquest.com)

For Comcast - Franklin - INTFR : INTFR
For Clarksville Dept Of Electric : CLKSEL
For Comcast - Gallatin - INTSU : INTSU
For Comcast- Rutherford - INTRU : INTRU
For Comcast - Wilson - INTWI : INTWI
For Comcast - Dickson (utiliquest) - INTDI : INTDI
For Cumberland Electric Membership Coop : CEMC
For Dickson Electric Dept : DED
For Middle Tenn Electric Membership Coop - Franklin - MTEMFR : MTEMFR
For Middle Tenn Electric Membership Coop - Lebanon - MTEMLE : MTEMLE
For Middle Tenn Electric Membership Coop - Murfreesbor - MTEMMU : MTEMMU
For Middle Tenn Electric Membership Coop - Woodbury - MTEMWO : MTEMWO
For Murfreesboro Electric (utiliquest) : ME
For Nashville Electric Svc (utiliquest) - NES : NES

Type  Seq#  Ticket                    Status                                
----  ----  ------------------------  ------------------------------------- 
!     0001  130095004                 Delivered                           
      0002  130095024                 Delivered                           
      0003  130095070                 Delivered                           
      0004  130095081                 Delivered                           
      0005  130095092                 Delivered                           
      0006  130095093                 Delivered                           
!     0007  130095115                 Delivered                           
      0008  130095113                 Delivered                           
      0009  130095094                 Delivered                           
      0010  130095100                 Delivered                           
      0011  130095104                 Delivered                           
      0012  130095107                 Delivered                           
      0013  130095109                 Delivered                           
      0014  130095112                 Delivered                           
      0015  130095118                 Delivered                           
      0016  130095121                 Delivered                           
      0017  130095124                 Delivered                           
      0018  130095145                 Delivered                           
      0019  130090032                 Delivered                           
      0020  130090033                 Delivered                           
      0021  130090034                 Delivered                           
      0022  130090035                 Delivered                           
      0023  130090036                 Delivered                           
      0024  130090037                 Delivered                           
      0025  130095165                 Delivered                           
!     0026  130095177                 Delivered                           
      0027  130095181                 Delivered                           
      0028  130095183                 Delivered                           
      0029  130095185                 Delivered                           
      0030  130095187                 Delivered                           
      0031  130090073                 Delivered                           
      0032  130095201                 Delivered                           
!     0033  130095214                 Delivered                           
      0034  130095204                 Delivered                           
      0035  130095206                 Delivered                           
      0036  130095208                 Delivered                           
      0037  130095210                 Delivered                           
      0038  130095212                 Delivered                           
      0039  130095218                 Delivered                           
      0040  130095224                 Delivered                           
      0041  130095228                 Delivered                           
      0042  130095231                 Delivered                           
      0043  130095233                 Delivered                           
      0044  130095236                 Delivered                           
      0045  130090130                 Delivered                           
      0046  130095239                 Delivered                           
      0047  130095240                 Delivered                           
      0048  130095244                 Delivered                           
      0049  130095246                 Delivered                           
      0050  130095249                 Delivered                           
      0051  130095250                 Delivered                           
      0052  130095251                 Delivered                           
!     0053  130095261                 Delivered                           
      0054  130095266                 Delivered                           
!     0055  130095271                 Delivered                           
      0056  130095273                 Delivered                           
      0057  130095276                 Delivered                           
      0058  130095280                 Delivered                           
      0059  130095284                 Delivered                           
      0060  130095295                 Delivered                           
      0061  130095285                 Delivered                           
      0062  130095300                 Delivered                           
      0063  130095312                 Delivered                           
      0064  130095314                 Delivered                           
      0065  130090140                 Delivered                           
      0066  130090141                 Delivered                           
      0067  130095316                 Delivered                           
      0068  130095323                 Delivered                           
      0069  130090158                 Delivered                           
      0070  130090159                 Delivered                           
      0071  130090160                 Delivered                           
      0072  130090161                 Delivered                           
      0073  130095329                 Delivered                           
      0074  130090162                 Delivered                           
      0075  130095337                 Delivered                           
      0076  130095357                 Delivered                           
      0077  130095368                 Delivered                           
      0078  130095378                 Delivered                           
      0079  130095380                 Delivered                           
      0080  130095381                 Delivered                           
      0081  130095383                 Delivered                           
      0082  130095388                 Delivered                           
      0083  130095390                 Delivered                           
      0084  130090179                 Delivered                           
      0085  130095394                 Delivered                           
      0086  130095398                 Delivered                           
      0087  130095400                 Delivered                           
      0088  130095401                 Delivered                           
      0089  130095403                 Delivered                           
      0090  130095404                 Delivered                           
      0091  130095406                 Delivered                           
      0092  130095407                 Delivered                           
      0093  130095409                 Delivered                           
      0094  130095410                 Delivered                           
      0095  130095411                 Delivered                           
      0096  130095413                 Delivered                           
      0097  130095414                 Delivered                           
      0098  130095415                 Delivered                           
      0099  130095418                 Delivered                           
      0100  130095419                 Delivered                           
      0101  130095421                 Delivered                           
      0102  130095423                 Delivered                           
      0103  130095424                 Delivered                           
!     0104  130095425                 Delivered                           
      0105  130095433                 Delivered                           
      0106  130095445                 Delivered                           
      0107  130095446                 Delivered                           
      0108  130095451                 Delivered                           
      0109  130095453                 Delivered                           
      0110  130095454                 Delivered                           
      0111  130095456                 Delivered                           
      0112  130095461                 Delivered                           
      0113  130095462                 Delivered                           
      0114  130095465                 Delivered                           
      0115  130095468                 Delivered                           
      0116  130095471                 Delivered                           
      0117  130095473                 Delivered                           
      0118  130095477                 Delivered                           
      0119  130095497                 Delivered                           
      0120  130095484                 Delivered                           
      0121  130095490                 Delivered                           
      0122  130095493                 Delivered                           
      0123  130090185                 Delivered                           
      0124  130090186                 Delivered                           
      0125  130090187                 Delivered                           
      0126  130095501                 Delivered                           
      0127  130090188                 Delivered                           
      0128  130095508                 Delivered                           
      0129  130095509                 Delivered                           
      0130  130095511                 Delivered                           
      0131  130095512                 Delivered                           
      0132  130095515                 Delivered                           
      0133  130095517                 Delivered                           
      0134  130095518                 Delivered                           
      0135  130095521                 Delivered                           
      0136  130095522                 Delivered                           
      0137  130095523                 Delivered                           
      0138  130095526                 Delivered                           
      0139  130095527                 Delivered                           
      0140  130095531                 Delivered                           
      0141  130095535                 Delivered                           
      0142  130095536                 Delivered                           
      0143  130095539                 Delivered                           
      0144  130095542                 Delivered                           
      0145  130095543                 Delivered                           
      0146  130095545                 Delivered                           
      0147  130095548                 Delivered                           
      0148  130095551                 Delivered                           
      0149  130095555                 Delivered                           
      0150  130095556                 Delivered                           
      0151  130095560                 Delivered                           
      0152  130095561                 Delivered                           
      0153  130095565                 Delivered                           
      0154  130095571                 Delivered                           
      0155  130095576                 Delivered                           
      0156  130095593                 Delivered                           
      0157  130095608                 Delivered                           
      0158  130095618                 Delivered                           
      0159  130090193                 Delivered                           
      0160  130090194                 Delivered                           
      0161  130095683                 Delivered                           
      0162  130090200                 Delivered                           
      0163  130095698                 Delivered                           
      0164  130095705                 Delivered                           
      0165  130090202                 Delivered                           
      0166  130090206                 Delivered                           
      0167  130090204                 Delivered                           
      0168  130095709                 Delivered                           
      0169  130090207                 Delivered                           
      0170  130095710                 Delivered                           
      0171  130095714                 Delivered                           
      0172  130095718                 Delivered                           
      0173  130095719                 Delivered                           
      0174  130095720                 Delivered                           
      0175  130095722                 Delivered                           
      0176  130095724                 Delivered                           
      0177  130095725                 Delivered                           
      0178  130095728                 Delivered                           
      0179  130095729                 Delivered                           
      0180  130095731                 Delivered                           
      0181  130090218                 Delivered                           
      0182  130090219                 Delivered                           
      0183  130090220                 Delivered                           
      0184  130095734                 Delivered                           
      0185  130095736                 Delivered                           
      0186  130090221                 Delivered                           
      0187  130090222                 Delivered                           
      0188  130090223                 Delivered                           
      0189  130090224                 Delivered                           
      0190  130090225                 Delivered                           
      0191  130095741                 Delivered                           
      0192  130095743                 Delivered                           
      0193  130095747                 Delivered                           
      0194  130095753                 Delivered                           
      0195  130090231                 Delivered                           
      0196  130090232                 Delivered                           
      0197  130095759                 Delivered                           
      0198  130095762                 Delivered                           
      0199  130090235                 Delivered                           
      0200  130090234                 Delivered                           
!     0201  130095771                 Delivered                           
!     0202  130095780                 Delivered                           
      0203  130095766                 Delivered                           
      0204  130095768                 Delivered                           
      0205  130095770                 Delivered                           
      0206  130090237                 Delivered                           
      0207  130090239                 Delivered                           
      0208  130090241                 Delivered                           
      0209  130095775                 Delivered                           
      0210  130090243                 Delivered                           
      0211  130090244                 Delivered                           
      0212  130095779                 Delivered                           
      0213  130095785                 Delivered                           
      0214  130095786                 Delivered                           
      0215  130090247                 Delivered                           
      0216  130095796                 Delivered                           
      0217  130095807                 Delivered                           
      0218  130095810                 Delivered                           
      0219  130095815                 Delivered                           
      0220  130095816                 Delivered                           
!     0221  130095824                 Delivered                           
      0222  130095822                 Delivered                           
      0223  130095827                 Delivered                           
      0224  130095839                 Delivered                           
      0225  130095842                 Delivered                           
!     0226  130095844                 Delivered                           
      0227  130095846                 Delivered                           
      0228  130095854                 Delivered                           
      0229  130095867                 Delivered                           
      0230  130095871                 Delivered                           
      0231  130095885                 Delivered                           
!     0232  130095887                 Delivered                           
      0233  130095889                 Delivered                           
      0234  130090253                 Delivered                           
      0235  130090254                 Delivered                           
      0236  130095897                 Delivered                           
      0237  130095915                 Delivered                           
      0238  130095906                 Delivered                           
      0239  130095907                 Delivered                           
      0240  130090255                 Delivered                           
      0241  130090257                 Delivered                           
      0242  130095943                 Delivered                           
      0243  130095944                 Delivered                           
      0244  130095945                 Delivered                           
!     0245  130095969                 Delivered                           
      0246  130095977                 Delivered                           
      0247  130095979                 Delivered                           
      0248  130095981                 Delivered                           
      0249  130095982                 Delivered                           
      0250  130095983                 Delivered                           
      0251  130095986                 Delivered                           
      0252  130095991                 Delivered                           
      0253  130096001                 Delivered                           
!     0254  130096003                 Delivered                           
      0255  130096008                 Delivered                           
      0256  130096005                 Delivered                           
      0257  130096012                 Delivered                           
      0258  130096019                 Delivered                           
      0259  130096022                 Delivered                           
      0260  130096025                 Delivered                           
      0261  130090264                 Delivered                           
      0262  130096031                 Delivered                           
!     0263  130096032                 Delivered                           
      0264  130096033                 Delivered                           
      0265  130096035                 Delivered                           
      0266  130096046                 Delivered                           
      0267  130096049                 Delivered                           
      0268  130096050                 Delivered                           
      0269  130096051                 Delivered                           
      0270  130096055                 Delivered                           
      0271  130096056                 Delivered                           

Compliant     : 246
Non-Compliant : 10
Emergency     : 15
Resend        : 0
Failed        : 0
Total         : 271



Legend
-----------------------
  - Normal
* - Resend
! - Emergency

