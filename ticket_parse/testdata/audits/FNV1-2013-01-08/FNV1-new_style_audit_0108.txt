
From TNOCS
Audit For 1/8/2013
Output UTILNASH Email (tickets.utiliquest.com)

For Comcast - Franklin - INTFR : INTFR
For Clarksville Dept Of Electric : CLKSEL
For Comcast - Gallatin - INTSU : INTSU
For Comcast- Rutherford - INTRU : INTRU
For Comcast - Wilson - INTWI : INTWI
For Comcast - Dickson (utiliquest) - INTDI : INTDI
For Cumberland Electric Membership Coop : CEMC
For Dickson Electric Dept : DED
For Middle Tenn Electric Membership Coop - Franklin - MTEMFR : MTEMFR
For Middle Tenn Electric Membership Coop - Lebanon - MTEMLE : MTEMLE
For Middle Tenn Electric Membership Coop - Murfreesbor - MTEMMU : MTEMMU
For Middle Tenn Electric Membership Coop - Woodbury - MTEMWO : MTEMWO
For Murfreesboro Electric (utiliquest) : ME
For Nashville Electric Svc (utiliquest) - NES : NES

Type  Seq#  Ticket                    Status                                
----  ----  ------------------------  ------------------------------------- 
!     0001  130080004                 Delivered                           
      0002  130085001                 Delivered                           
      0003  130085002                 Delivered                           
      0004  130085004                 Delivered                           
      0005  130085005                 Delivered                           
      0006  130085007                 Delivered                           
      0007  130085011                 Delivered                           
      0008  130085025                 Delivered                           
!     0009  130085030                 Delivered                           
!     0010  130085037                 Delivered                           
      0011  130085042                 Delivered                           
      0012  130080011                 Delivered                           
      0013  130080012                 Delivered                           
      0014  130080013                 Delivered                           
      0015  130080015                 Delivered                           
      0016  130080016                 Delivered                           
      0017  130080017                 Delivered                           
      0018  130080018                 Delivered                           
      0019  130080019                 Delivered                           
      0020  130085057                 Delivered                           
      0021  130080022                 Delivered                           
      0022  130080023                 Delivered                           
      0023  130080024                 Delivered                           
      0024  130080025                 Delivered                           
      0025  130085095                 Delivered                           
      0026  130085099                 Delivered                           
      0027  130080053                 Delivered                           
      0028  130085100                 Delivered                           
      0029  130085102                 Delivered                           
      0030  130085103                 Delivered                           
!     0031  130085113                 Delivered                           
      0032  130085114                 Delivered                           
!     0033  130085117                 Delivered                           
!     0034  130085120                 Delivered                           
      0035  130080068                 Delivered                           
      0036  130085123                 Delivered                           
      0037  130080070                 Delivered                           
      0038  130080071                 Delivered                           
      0039  130085126                 Delivered                           
      0040  130085128                 Delivered                           
      0041  130085132                 Delivered                           
      0042  130080076                 Delivered                           
      0043  130080077                 Delivered                           
!     0044  130085174                 Delivered                           
      0045  130085137                 Delivered                           
      0046  130085145                 Delivered                           
      0047  130080079                 Delivered                           
      0048  130080078                 Delivered                           
      0049  130085168                 Delivered                           
      0050  130080080                 Delivered                           
      0051  130080082                 Delivered                           
      0052  130085192                 Delivered                           
      0053  130080088                 Delivered                           
      0054  130080090                 Delivered                           
      0055  130080089                 Delivered                           
      0056  130085213                 Delivered                           
      0057  130085216                 Delivered                           
      0058  130085217                 Delivered                           
      0059  130080091                 Delivered                           
      0060  130085219                 Delivered                           
      0061  130085220                 Delivered                           
      0062  130085223                 Delivered                           
      0063  130085233                 Delivered                           
      0064  130085236                 Delivered                           
      0065  130085240                 Delivered                           
!     0066  130085244                 Delivered                           
      0067  130085248                 Delivered                           
      0068  130085250                 Delivered                           
      0069  130085254                 Delivered                           
      0070  130085256                 Delivered                           
!     0071  130085260                 Delivered                           
      0072  130085264                 Delivered                           
      0073  130080095                 Delivered                           
      0074  130080096                 Delivered                           
      0075  130085285                 Delivered                           
      0076  130085286                 Delivered                           
      0077  130085294                 Delivered                           
      0078  130085301                 Delivered                           
      0079  130080097                 Delivered                           
      0080  130085306                 Delivered                           
      0081  130085307                 Delivered                           
      0082  130085308                 Delivered                           
      0083  130085318                 Delivered                           
      0084  130085326                 Delivered                           
      0085  130085329                 Delivered                           
      0086  130085331                 Delivered                           
!     0087  130085336                 Delivered                           
      0088  130080102                 Delivered                           
      0089  130085342                 Delivered                           
      0090  130085346                 Delivered                           
      0091  130085363                 Delivered                           
      0092  130085366                 Delivered                           
      0093  130085367                 Delivered                           
      0094  130085370                 Delivered                           
!     0095  130085372                 Delivered                           
      0096  130085376                 Delivered                           
      0097  130085378                 Delivered                           
!     0098  130085379                 Delivered                           
      0099  130085384                 Delivered                           
      0100  130085385                 Delivered                           
      0101  130085394                 Delivered                           
      0102  130085396                 Delivered                           
      0103  130080105                 Delivered                           
      0104  130080104                 Delivered                           
      0105  130085409                 Delivered                           
      0106  130080107                 Delivered                           
      0107  130080106                 Delivered                           
      0108  130085422                 Delivered                           
      0109  130085426                 Delivered                           
      0110  130080108                 Delivered                           
      0111  130080109                 Delivered                           
      0112  130085435                 Delivered                           
      0113  130085438                 Delivered                           
      0114  130085440                 Delivered                           
      0115  130085452                 Delivered                           
      0116  130085460                 Delivered                           
      0117  130085461                 Delivered                           
      0118  130080111                 Delivered                           
      0119  130085466                 Delivered                           
!     0120  130085477                 Delivered                           
!     0121  130085478                 Delivered                           
!     0122  130085494                 Delivered                           
      0123  130085472                 Delivered                           
      0124  130080112                 Delivered                           
      0125  130085476                 Delivered                           
      0126  130085484                 Delivered                           
      0127  130080115                 Delivered                           
      0128  130085501                 Delivered                           
      0129  130080117                 Delivered                           
      0130  130080118                 Delivered                           
      0131  130080120                 Delivered                           
      0132  130080121                 Delivered                           
      0133  130085518                 Delivered                           
      0134  130085529                 Delivered                           
      0135  130085538                 Delivered                           
      0136  130085540                 Delivered                           
      0137  130080129                 Delivered                           
      0138  130080130                 Delivered                           
      0139  130080132                 Delivered                           
      0140  130080133                 Delivered                           
      0141  130085554                 Delivered                           
      0142  130080135                 Delivered                           
      0143  130080136                 Delivered                           
      0144  130085560                 Delivered                           
      0145  130085564                 Delivered                           
      0146  130085566                 Delivered                           
      0147  130085569                 Delivered                           
      0148  130085573                 Delivered                           
      0149  130085575                 Delivered                           
      0150  130085599                 Delivered                           
      0151  130085603                 Delivered                           
!     0152  130085634                 Delivered                           
      0153  130085606                 Delivered                           
      0154  130085608                 Delivered                           
      0155  130085612                 Delivered                           
      0156  130085615                 Delivered                           
      0157  130085617                 Delivered                           
      0158  130085619                 Delivered                           
      0159  130085620                 Delivered                           
      0160  130085622                 Delivered                           
      0161  130085632                 Delivered                           
      0162  130085640                 Delivered                           
      0163  130085647                 Delivered                           
      0164  130085651                 Delivered                           
      0165  130085652                 Delivered                           
      0166  130085654                 Delivered                           
      0167  130085655                 Delivered                           
      0168  130085665                 Delivered                           
!     0169  130085688                 Delivered                           
!     0170  130085695                 Delivered                           
      0171  130085686                 Delivered                           
      0172  130085696                 Delivered                           
      0173  130085698                 Delivered                           
!     0174  130085706                 Delivered                           
      0175  130085708                 Delivered                           
!     0176  130085709                 Delivered                           
      0177  130085711                 Delivered                           
      0178  130085712                 Delivered                           
      0179  130085731                 Delivered                           
      0180  130085734                 Delivered                           
      0181  130085737                 Delivered                           
      0182  130085740                 Delivered                           
      0183  130085744                 Delivered                           
      0184  130085748                 Delivered                           
      0185  130085757                 Delivered                           
      0186  130085758                 Delivered                           
      0187  130080163                 Delivered                           
      0188  130085759                 Delivered                           
      0189  130085762                 Delivered                           
      0190  130085767                 Delivered                           
      0191  130085775                 Delivered                           
      0192  130085778                 Delivered                           
      0193  130085789                 Delivered                           
      0194  130085799                 Delivered                           
      0195  130080167                 Delivered                           
      0196  130085808                 Delivered                           
!     0197  130085825                 Delivered                           
      0198  130080170                 Delivered                           
      0199  130085817                 Delivered                           
      0200  130085823                 Delivered                           
      0201  130085834                 Delivered                           
      0202  130080176                 Delivered                           
      0203  130085839                 Delivered                           
      0204  130085841                 Delivered                           
!     0205  130085855                 Delivered                           
!     0206  130085868                 Delivered                           
      0207  130085846                 Delivered                           
      0208  130085851                 Delivered                           
      0209  130085852                 Delivered                           
      0210  130085856                 Delivered                           
      0211  130080179                 Delivered                           
!     0212  130085878                 Delivered                           
!     0213  130085889                 Delivered                           
      0214  130085874                 Delivered                           
      0215  130085877                 Delivered                           
      0216  130085879                 Delivered                           
      0217  130085882                 Delivered                           
      0218  130080180                 Delivered                           
      0219  130080181                 Delivered                           
      0220  130080182                 Delivered                           
      0221  130085883                 Delivered                           
      0222  130085884                 Delivered                           
      0223  130085886                 Delivered                           
      0224  130085894                 Delivered                           
      0225  130085895                 Delivered                           
!     0226  130085911                 Delivered                           
      0227  130085906                 Delivered                           
      0228  130085896                 Delivered                           
      0229  130085900                 Delivered                           
      0230  130085901                 Delivered                           
      0231  130085904                 Delivered                           
      0232  130085910                 Delivered                           
      0233  130080192                 Delivered                           
      0234  130085923                 Delivered                           
      0235  130085931                 Delivered                           
      0236  130085930                 Delivered                           
      0237  130085932                 Delivered                           
      0238  130085944                 Delivered                           
!     0239  130085955                 Delivered                           
      0240  130085947                 Delivered                           
      0241  130085948                 Delivered                           
      0242  130085952                 Delivered                           
      0243  130085953                 Delivered                           
      0244  130085960                 Delivered                           
      0245  130085961                 Delivered                           
      0246  130080196                 Delivered                           
      0247  130085966                 Delivered                           
      0248  130080199                 Delivered                           
      0249  130080200                 Delivered                           
      0250  130085968                 Delivered                           
      0251  130080201                 Delivered                           
      0252  130080202                 Delivered                           
      0253  130085983                 Delivered                           
      0254  130085988                 Delivered                           
      0255  130085997                 Delivered                           
      0256  130085999                 Delivered                           
      0257  130086003                 Delivered                           
      0258  130086010                 Delivered                           
      0259  130080204                 Delivered                           
      0260  130080205                 Delivered                           
      0261  130086015                 Delivered                           
      0262  130086023                 Delivered                           
      0263  130080206                 Delivered                           
      0264  130080207                 Delivered                           
      0265  130080208                 Delivered                           
      0266  130086025                 Delivered                           
      0267  130080209                 Delivered                           
      0268  130080211                 Delivered                           
      0269  130086042                 Delivered                           
      0270  130080212                 Delivered                           
      0271  130086053                 Delivered                           
      0272  130086059                 Delivered                           
!     0273  130086087                 Delivered                           
      0274  130086063                 Delivered                           
      0275  130086064                 Delivered                           
      0276  130086070                 Delivered                           
      0277  130086071                 Delivered                           
      0278  130086075                 Delivered                           
      0279  130086077                 Delivered                           
      0280  130086079                 Delivered                           
      0281  130086081                 Delivered                           
      0282  130086085                 Delivered                           
      0283  130086086                 Delivered                           
      0284  130086088                 Delivered                           
      0285  130086089                 Delivered                           
      0286  130086090                 Delivered                           
      0287  130086093                 Delivered                           
      0288  130086094                 Delivered                           
      0289  130086096                 Delivered                           
      0290  130086097                 Delivered                           
      0291  130086098                 Delivered                           
      0292  130086099                 Delivered                           
      0293  130086102                 Delivered                           
      0294  130086108                 Delivered                           
      0295  130086111                 Delivered                           
      0296  130086112                 Delivered                           
      0297  130080213                 Delivered                           
      0298  130080217                 Delivered                           
      0299  130086123                 Delivered                           
      0300  130086127                 Delivered                           
      0301  130086128                 Delivered                           
      0302  130086134                 Delivered                           
      0303  130086135                 Delivered                           
      0304  130086136                 Delivered                           
      0305  130086137                 Delivered                           
      0306  130086141                 Delivered                           
      0307  130080220                 Delivered                           
      0308  130080221                 Delivered                           
      0309  130080223                 Delivered                           
      0310  130086146                 Delivered                           
      0311  130080224                 Delivered                           
      0312  130080226                 Delivered                           
      0313  130086150                 Delivered                           
      0314  130080228                 Delivered                           
      0315  130080233                 Delivered                           
      0316  130086155                 Delivered                           
      0317  130080235                 Delivered                           
      0318  130080237                 Delivered                           
      0319  130086166                 Delivered                           
      0320  130086168                 Delivered                           
      0321  130086171                 Delivered                           
!     0322  130086183                 Delivered                           
      0323  130086189                 Delivered                           
      0324  130086178                 Delivered                           
      0325  130086184                 Delivered                           
      0326  130086187                 Delivered                           
      0327  130086191                 Delivered                           
      0328  130086192                 Delivered                           
      0329  130086197                 Delivered                           
      0330  130086212                 Delivered                           
      0331  130080249                 Delivered                           
      0332  130086236                 Delivered                           
      0333  130086245                 Delivered                           
      0334  130086246                 Delivered                           
      0335  130080250                 Delivered                           
      0336  130080251                 Delivered                           
      0337  130086256                 Delivered                           
      0338  130080252                 Delivered                           
      0339  130080253                 Delivered                           
      0340  130086260                 Delivered                           
      0341  130080254                 Delivered                           
      0342  130080255                 Delivered                           
!     0343  130086270                 Delivered                           
      0344  130080256                 Delivered                           
      0345  130080258                 Delivered                           
      0346  130080260                 Delivered                           
      0347  130080259                 Delivered                           
      0348  130086274                 Delivered                           
      0349  130080269                 Delivered                           
      0350  130080272                 Delivered                           
      0351  130086288                 Delivered                           
      0352  130086289                 Delivered                           
      0353  130080276                 Delivered                           
      0354  130086295                 Delivered                           
      0355  130080279                 Delivered                           
      0356  130086301                 Delivered                           
      0357  130086316                 Delivered                           
      0358  130080282                 Delivered                           
      0359  130086303                 Delivered                           
      0360  130086305                 Delivered                           
      0361  130086308                 Delivered                           
      0362  130080285                 Delivered                           
      0363  130080286                 Delivered                           
      0364  130086314                 Delivered                           
      0365  130080287                 Delivered                           
      0366  130080288                 Delivered                           
      0367  130086321                 Delivered                           
      0368  130086322                 Delivered                           
      0369  130086326                 Delivered                           
      0370  130080293                 Delivered                           
      0371  130080295                 Delivered                           
      0372  130080297                 Delivered                           
      0373  130086329                 Delivered                           
      0374  130086331                 Delivered                           
      0375  130086332                 Delivered                           
      0376  130080298                 Delivered                           
      0377  130080299                 Delivered                           
      0378  130080300                 Delivered                           
      0379  130086333                 Delivered                           
      0380  130086341                 Delivered                           
      0381  130086346                 Delivered                           
      0382  130086347                 Delivered                           
      0383  130086349                 Delivered                           
      0384  130086353                 Delivered                           
      0385  130086360                 Delivered                           
      0386  130086372                 Delivered                           
      0387  130086380                 Delivered                           
      0388  130086388                 Delivered                           
      0389  130086389                 Delivered                           
      0390  130086398                 Delivered                           
      0391  130086399                 Delivered                           
      0392  130086425                 Delivered                           
      0393  130086427                 Delivered                           
      0394  130086434                 Delivered                           
      0395  130086470                 Delivered                           
!     0396  130086481                 Delivered                           
      0397  130086476                 Delivered                           
      0398  130086480                 Delivered                           
      0399  130086486                 Delivered                           
      0400  130086489                 Delivered                           
      0401  130086491                 Delivered                           
      0402  130086493                 Delivered                           
      0403  130086508                 Delivered                           
      0404  130086510                 Delivered                           
      0405  130086511                 Delivered                           
      0406  130086520                 Delivered                           
      0407  130086523                 Delivered                           
      0408  130086527                 Delivered                           
      0409  130086529                 Delivered                           
      0410  130086530                 Delivered                           
      0411  130086532                 Delivered                           
      0412  130086533                 Delivered                           
      0413  130086535                 Delivered                           
      0414  130086536                 Delivered                           
      0415  130086540                 Delivered                           
      0416  130086541                 Delivered                           
      0417  130086542                 Delivered                           
      0418  130086543                 Delivered                           
      0419  130086544                 Delivered                           
      0420  130086545                 Delivered                           
      0421  130086546                 Delivered                           
      0422  130086547                 Delivered                           
      0423  130086548                 Delivered                           
      0424  130086549                 Delivered                           
      0425  130086550                 Delivered                           
      0426  130086551                 Delivered                           
!     0427  130086553                 Delivered                           
      0428  130086554                 Delivered                           
      0429  130080322                 Delivered                           
!     0430  130086557                 Delivered                           
      0431  130080324                 Delivered                           
      0432  130086563                 Delivered                           
      0433  130086564                 Delivered                           
      0434  130080328                 Delivered                           
      0435  130080330                 Delivered                           
      0436  130086566                 Delivered                           
      0437  130080333                 Delivered                           
      0438  130080336                 Delivered                           
      0439  130086567                 Delivered                           
      0440  130080338                 Delivered                           
      0441  130086571                 Delivered                           
      0442  130086574                 Delivered                           
      0443  130086579                 Delivered                           
      0444  130086580                 Delivered                           
      0445  130086581                 Delivered                           
      0446  130086583                 Delivered                           
!     0447  130086585                 Delivered                           
      0448  130086586                 Delivered                           
      0449  130086587                 Delivered                           
      0450  130086588                 Delivered                           
      0451  130086592                 Delivered                           
      0452  130086595                 Delivered                           
      0453  130086597                 Delivered                           
      0454  130086599                 Delivered                           
      0455  130086601                 Delivered                           
      0456  130086602                 Delivered                           
      0457  130086604                 Delivered                           
      0458  130086605                 Delivered                           
      0459  130086606                 Delivered                           
      0460  130086607                 Delivered                           
      0461  130086608                 Delivered                           
      0462  130086609                 Delivered                           
      0463  130086610                 Delivered                           
      0464  130086611                 Delivered                           
      0465  130086612                 Delivered                           
      0466  130086613                 Delivered                           
      0467  130086614                 Delivered                           
      0468  130086615                 Delivered                           
      0469  130086616                 Delivered                           
      0470  130086617                 Delivered                           
      0471  130086618                 Delivered                           
      0472  130086623                 Delivered                           
      0473  130086624                 Delivered                           
      0474  130086628                 Delivered                           
      0475  130086630                 Delivered                           
      0476  130086632                 Delivered                           
      0477  130086633                 Delivered                           
      0478  130086634                 Delivered                           
      0479  130086635                 Delivered                           
      0480  130086636                 Delivered                           

Compliant     : 429
Non-Compliant : 17
Emergency     : 34
Resend        : 0
Failed        : 0
Total         : 480



Legend
-----------------------
  - Normal
* - Resend
! - Emergency

