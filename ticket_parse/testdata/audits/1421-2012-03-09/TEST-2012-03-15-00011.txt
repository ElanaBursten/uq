
From SC811
Audit For 3/15/2012

For SCEG Electric  : SCEDZ65, SCETZ03
For SCEG Gas : SCGZ02, SCGZ65

Type  Seq#  Ticket        Status       Codes                         
----  ----  ------------  -----------  ------------------------------
D     0049  1203150426    Delivered    SCGZ02                        
      0050  1203150427    Delivered    SCGZ02                        
U     0051  1203150428    Delivered    SCGZ02                        
S     0052  1203150429    Delivered    SCGZ02                        
!     0053  1203150431    Delivered    SCGZ02                        
!     0054  1203150432    Delivered    SCGZ02                        
      0055  1203150433    Delivered    SCGZ02                        
!     0056  1203150550    Delivered    SCGZ02                        
!     0057  1203150551    Delivered    SCGZ02                        

Normal        : 1
Resend        : 0
Emergency     : 4
Emger-Cancel  : 0
Emerg-NoShow  : 0
Emerg-Resend  : 0
Emerg-Remark  : 0
Cancel        : 0
Meet          : 0
No Show       : 0
Survey        : 1
Update        : 1
Remark        : 0
Design        : 1
Retransmit    : 0
Failed        : 0
Total         : 9



Legend
-----------------------
C  - Cancel
!  ? Emergency
!C ? Emger-Cancel
!N - Emerg-NoShow
!* - Emerg-Resend
!R - Emerg-Remark
M  - Meet
   - Normal
N  - No Show
*  - Resend
S  - Survey
V  - Void
U  - Update
R  - Remark
D  - Design

Output ZZQ06 - Testing

