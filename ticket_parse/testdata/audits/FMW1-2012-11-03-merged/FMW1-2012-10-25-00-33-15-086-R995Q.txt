
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: TRU02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570111-ETKT|  85  12570920-48HR| 168  12571534-48HR| 251  12572062-UPDT|
   2  12570127-FIOS|  86  12570932-ETKT| 169  12571537-48HR| 252  12572066-UPDT|
   3  12570128-FIOS|  87  12570928-48HR| 170  12571541-48HR| 253  12572070-UPDT|
   4  12570129-FIOS|  88  12570938-48HR| 171  12571490-CANC| 254  12572096-48HR|
   5  12570130-FIOS|  89  12570947-UPDT| 172  12571533-48HR| 255  12572099-48HR|
   6  12570131-FIOS|  90  12570982-48HR| 173  12571543-ETKT| 256  12572106-48HR|
   7  12570147-48HR|  91  12570991-UPDT| 174  12571488-48HR| 257  12572107-48HR|
   8  12570153-ETKT|  92  12570960-48HR| 175  12571494-48HR| 258  12572110-48HR|
   9  12570162-48HR|  93  12571025-UPDT| 176  12571515-48HR| 259  12571965-UPDT|
  10  12570169-UPDT|  94  12571026-UPDT| 177  12571550-48HR| 260  12571967-48HR|
  11  12570158-ETKT|  95  12571031-UPDT| 178  12571552-48HR| 261  12572135-UPDT|
  12  12555124-REMK|  96  12571033-UPDT| 179  12571553-48HR| 262  12572137-UPDT|
  13  12570172-48HR|  97  12571034-UPDT| 180  12571555-48HR| 263  12572143-UPDT|
  14  12570202-UPDT|  98  12571036-UPDT| 181  12571560-FIOS| 264  12572147-UPDT|
  15  12570233-48HR|  99  12571037-UPDT| 182  12571586-FIOS| 265  12572126-48HR|
  16  12570348-UPDT| 100  12571039-UPDT| 183  12571597-48HR| 266  12572182-FIOS|
  17  12570354-UPDT| 101  12571040-UPDT| 184  12571613-FIOS| 267  12572176-ETKT|
  18  12570370-UPDT| 102  12571043-UPDT| 185  12571616-FIOS| 268  12572202-UPDT|
  19  12570401-48HR| 103  12571046-UPDT| 186  12571622-48HR| 269  12572211-FIOS|
  20  12570381-ETKT| 104  12571049-UPDT| 187  12571627-48HR| 270  12572219-FIOS|
  21  12570413-48HR| 105  12571050-UPDT| 188  12571618-UPDT| 271  12572222-FIOS|
  22  12570421-48HR| 106  12571052-UPDT| 189  12571629-48HR| 272  12572177-48HR|
  23  12570442-FIOS| 107  12571054-UPDT| 190  12571631-48HR| 273  12572224-48HR|
  24  12570464-UPDT| 108  12571061-UPDT| 191  12571632-48HR| 274  12572225-48HR|
  25  12570465-ETKT| 109  12571063-UPDT| 192  12571639-48HR| 275  12572236-48HR|
  26  12570469-ETKT| 110  12571065-UPDT| 193  12571643-48HR| 276  12572267-48HR|
  27  12570468-ETKT| 111  12571067-UPDT| 194  12571647-48HR| 277  12572278-48HR|
  28  12570481-ETKT| 112  12571068-UPDT| 195  12571648-48HR| 278  12572304-FIOS|
  29  12570495-UPDT| 113  12571070-UPDT| 196  12571660-48HR| 279  12572318-48HR|
  30  12570491-ETKT| 114  12571074-UPDT| 197  12571676-48HR| 280  12572360-48HR|
  31  12570519-48HR| 115  12571077-UPDT| 198  12571685-48HR| 281  12572396-FIOS|
  32  12570520-48HR| 116  12571079-UPDT| 199  12571686-48HR| 282  12572402-FIOS|
  33  12570521-48HR| 117  12571081-UPDT| 200  12571695-48HR| 283  12572397-UPDT|
  34  12570528-48HR| 118  12571083-UPDT| 201  12571684-48HR| 284  12572436-48HR|
  35  12570532-48HR| 119  12571087-UPDT| 202  12571702-48HR| 285  12572520-UPDT|
  36  12570536-48HR| 120  12571091-UPDT| 203  12571710-UPDT| 286  12572546-48HR|
  37  12570541-48HR| 121  12571116-48HR| 204  12571714-UPDT| 287  12572571-ETKT|
  38  12570544-48HR| 122  12571140-ETKT| 205  12571704-48HR| 288  12572588-48HR|
  39  12570548-48HR| 123  12571160-UPDT| 206  12571715-48HR| 289  12572629-48HR|
  40  12570551-48HR| 124  12571169-48HR| 207  12571718-UPDT| 290  12572682-FIOS|
  41  12570556-48HR| 125  12571212-UPDT| 208  12571723-UPDT| 291  12572665-48HR|
  42  12570515-ETKT| 126  12571214-UPDT| 209  12571725-UPDT| 292  12572685-FIOS|
  43  12570552-ETKT| 127  12571244-UPDT| 210  12571727-UPDT| 293  12572688-ETKT|
  44  12570587-ETKT| 128  12571286-48HR| 211  12571730-UPDT| 294  12572698-FIOS|
  45  12570637-UPDT| 129  12571278-48HR| 212  12571697-48HR| 295  12572744-UPDT|
  46  12570624-48HR| 130  12571304-48HR| 213  12571731-UPDT| 296  12572755-48HR|
  47  12570649-48HR| 131  12571317-UPDT| 214  12571734-UPDT| 297  12572758-48HR|
  48  12570663-48HR| 132  12571318-UPDT| 215  12571736-UPDT| 298  12572772-FIOS|
  49  12570673-48HR| 133  12571328-UPDT| 216  12571738-UPDT| 299  12572780-FIOS|
  50  12570699-48HR| 134  12571334-UPDT| 217  12571745-48HR| 300  12572781-FIOS|
  51  12570689-48HR| 135  12571340-UPDT| 218  12571747-UPDT| 301  12572782-FIOS|
  52  12570702-UPDT| 136  12571341-UPDT| 219  12571735-48HR| 302  12572850-UPDT|
  53  12570719-48HR| 137  12571342-UPDT| 220  12571778-48HR| 303  12572851-UPDT|
  54  12570715-48HR| 138  12571343-UPDT| 221  12571737-48HR| 304  12572852-UPDT|
  55  12570755-ETKT| 139  12571345-UPDT| 222  12571797-UPDT| 305  12572854-UPDT|
  56  12570793-48HR| 140  12571347-UPDT| 223  12571800-UPDT| 306  12572855-UPDT|
  57  12570806-FIOS| 141  12571348-UPDT| 224  12571803-UPDT| 307  12572856-UPDT|
  58  12570816-48HR| 142  12571351-UPDT| 225  12571806-UPDT| 308  12572857-UPDT|
  59  12570817-48HR| 143  12571353-UPDT| 226  12571808-UPDT| 309  12572858-UPDT|
  60  12570822-48HR| 144  12571363-48HR| 227  12571811-UPDT| 310  12572859-UPDT|
  61  12570824-48HR| 145  12571355-ETKT| 228  12571790-48HR| 311  12572860-UPDT|
  62  12570825-UPDT| 146  12571370-FIOS| 229  12571876-ETKT| 312  12572861-UPDT|
  63  12570826-48HR| 147  12571374-FIOS| 230  12571904-UPDT| 313  12572863-UPDT|
  64  12570831-48HR| 148  12571383-48HR| 231  12571947-48HR| 314  12572864-UPDT|
  65  12570836-48HR| 149  12571372-48HR| 232  12571959-ETKT| 315  12572865-UPDT|
  66  12570837-48HR| 150  12571395-UPDT| 233  12571973-UPDT| 316  12572866-UPDT|
  67  12570838-48HR| 151  12571396-UPDT| 234  12571986-48HR| 317  12572867-UPDT|
  68  12570850-48HR| 152  12571397-UPDT| 235  12571988-48HR| 318  12572868-UPDT|
  69  12570852-UPDT| 153  12571414-48HR| 236  12572010-UPDT| 319  12572869-UPDT|
  70  12570856-48HR| 154  12571424-48HR| 237  12572017-UPDT| 320  12572870-UPDT|
  71  12570859-UPDT| 155  12571469-48HR| 238  12572022-UPDT| 321  12572871-UPDT|
  72  12570864-48HR| 156  12571382-48HR| 239  12572020-UPDT| 322  12572872-UPDT|
  73  12570870-48HR| 157  12571386-48HR| 240  12572029-UPDT| 323  12572873-UPDT|
  74  12570872-UPDT| 158  12571406-48HR| 241  12572028-UPDT| 324  12572874-UPDT|
  75  12570876-UPDT| 159  12571467-48HR| 242  12572035-UPDT| 325  12572876-UPDT|
  76  12570881-ETKT| 160  12571484-48HR| 243  12572037-UPDT| 326  12572877-UPDT|
  77  12570867-48HR| 161* 12571490-ETKT| 244  12572042-UPDT| 327  12572878-UPDT|
  78  12570879-48HR| 162  12571497-48HR| 245  12572045-UPDT| 328  12572879-UPDT|
  79  12570891-48HR| 163  12571498-48HR| 246  12572047-UPDT| 329  12572880-UPDT|
  80  12570895-48HR| 164  12571504-48HR| 247  12572051-48HR| 330  12572881-UPDT|
  81  12570899-48HR| 165  12571511-48HR| 248  12572054-UPDT| 331  12572883-UPDT|
  82  12570911-UPDT| 166  12571524-48HR| 249  12572058-UPDT| 332  12572908-FIOS|
  83  12570914-48HR| 167  12571528-48HR| 250  12572049-UPDT| 333  12572909-FIOS|
  84  12570907-48HR|

* indicates ticket # is repeated

Total Tickets: 333

ETKT - EMERGENCY                      |   24

FIOS - FIOS                           |   29

48HR - STANDARD                       |  136

UPDT - UPDATE                         |  142

REMK - REMARK                         |    1

CANC - CANCELLATION                   |    1


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


