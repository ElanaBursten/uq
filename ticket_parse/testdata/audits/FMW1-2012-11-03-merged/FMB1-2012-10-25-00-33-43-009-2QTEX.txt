
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: VHW

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570124-FIOS|  43  12571311-48HR|  85  12571566-CANC| 127  12572259-48HR|
   2  12570103-48HR|  44  12571375-48HR|  86  12572011-48HR| 128  12572246-48HR|
   3  12570159-ETKT|  45  12571362-48HR|  87  12572044-48HR| 129  12572271-48HR|
   4  12570265-48HR|  46  12571389-48HR|  88  12572050-48HR| 130  12572241-48HR|
   5  12570483-48HR|  47  12571450-48HR|  89  12572056-48HR| 131  12572273-48HR|
   6  12570563-UPDT|  48  12571516-48HR|  90  12572059-48HR| 132  12572280-48HR|
   7  12570567-UPDT|  49  12571548-48HR|  91  12572064-48HR| 133  12572281-48HR|
   8  12570572-UPDT|  50  12571531-48HR|  92  12572073-48HR| 134  12572299-48HR|
   9  12570607-UPDT|  51* 12571566-48HR|  93  12572076-48HR| 135  12572314-UPDT|
  10  12570648-48HR|  52  12571687-UPDT|  94  12572089-48HR| 136  12572359-48HR|
  11  12570657-48HR|  53  12571690-UPDT|  95  12572093-48HR| 137  12572408-48HR|
  12  12570668-48HR|  54  12571682-48HR|  96  12572111-48HR| 138  12572441-FIOS|
  13  12570684-ETKT|  55  12571662-48HR|  97  12572113-48HR| 139  12572421-ETKT|
  14  12570740-ETKT|  56  12571657-48HR|  98  12572114-48HR| 140  12572456-FIOS|
  15  12570756-48HR|  57  12571746-UPDT|  99  12572115-48HR| 141  12572475-48HR|
  16  12570767-UPDT|  58  12571774-UPDT| 100  12572116-48HR| 142  12572500-48HR|
  17  12570828-UPDT|  59  12571784-48HR| 101  12572117-48HR| 143  12572495-48HR|
  18  12570842-UPDT|  60  12571801-48HR| 102  12572119-48HR| 144  12572502-48HR|
  19  12570851-UPDT|  61  12571820-48HR| 103  12572120-48HR| 145  12572498-48HR|
  20  12570901-OMBN|  62  12571816-48HR| 104  12572121-48HR| 146  12572518-48HR|
  21  12570917-UPDT|  63  12571836-48HR| 105  12572124-48HR| 147  12572509-48HR|
  22  12570927-48HR|  64  12571854-48HR| 106  12572125-48HR| 148  12572527-48HR|
  23  12570940-48HR|  65  12571864-48HR| 107  12572133-48HR| 149  12572560-FTTP|
  24  12570955-UPDT|  66  12571874-48HR| 108  12572136-48HR| 150  12572551-STKT|
  25  12570952-48HR|  67  12571882-48HR| 109  12572139-48HR| 151  12572578-FIOS|
  26  12570962-UPDT|  68  12571887-UPDT| 110  12572148-48HR| 152  12572586-UPDT|
  27  12570964-UPDT|  69  12571911-48HR| 111  12572149-48HR| 153  12572589-UPDT|
  28  12570967-UPDT|  70  12571917-48HR| 112  12572151-48HR| 154  12572590-FTTP|
  29  12570785-48HR|  71  12571918-48HR| 113  12572152-48HR| 155  12572591-UPDT|
  30  12570987-48HR|  72  12571924-48HR| 114  12572154-48HR| 156  12572593-UPDT|
  31  12570993-48HR|  73  12571937-48HR| 115  12572155-48HR| 157  12572595-FTTP|
  32  12570995-48HR|  74  12571953-48HR| 116  12572157-48HR| 158  12572597-48HR|
  33  12571022-FIOS|  75  12571960-48HR| 117  12572164-48HR| 159  12572599-48HR|
  34  12571101-UPDT|  76  12571984-48HR| 118  12572165-48HR| 160  12572633-FIOS|
  35  12571102-ETKT|  77  12571991-48HR| 119  12572178-UPDT| 161  12572649-ETKT|
  36  12571107-PTKT|  78  12571993-48HR| 120  12572190-48HR| 162  12572791-FIOS|
  37  12569698-CANC|  79  12571998-48HR| 121  12572197-FIOS| 163  12572809-FIOS|
  38  12571143-48HR|  80  12571999-48HR| 122  12572218-48HR| 164  12572811-FIOS|
  39  12571108-48HR|  81  12572002-48HR| 123* 12572226-48HR| 165  12572813-FIOS|
  40  12571130-48HR|  82  12572004-FIOS| 124  12572238-48HR| 166  12572815-FIOS|
  41  12571242-48HR|  83  12572006-48HR| 125  12572226-CANC| 167  12572817-FIOS|
  42  12571309-48HR|  84  12572008-FIOS| 126  12572257-48HR| 168  12572827-FIOS|

* indicates ticket # is repeated

Total Tickets: 168

FIOS - FIOS                           |   16

48HR - STANDARD                       |  112

ETKT - EMERGENCY                      |    6

UPDT - UPDATE                         |   25

OMBN - OMBN                           |    1

PTKT - INSUFFICIENT NOTICE            |    1

CANC - CANCELLATION                   |    3

FTTP - FTTP                           |    3

STKT - SHORT NOTICE                   |    1


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


