
0 
Date/Time :  1-14-10 at 10:51 

New Jersey One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON 01/08/10

Date/Time: 01/14/2010 10:50:58 AM
Receiving Terminal: ADC

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1 100040101-ROUT |   2 100040102-EMER |

* indicates ticket # is repeated

Total Tickets: 2

ROUT - ROUTINE                        |    1

EMER - EMERGENCY                      |    1


Please call (800) 743-6302 if this data does
not match the tickets you received on 01/08/10
