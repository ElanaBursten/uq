���I��MISUTIL2003012200375	PG PIPELINE	2003/01/22 10:53:30	00369


NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01024981               Update Of: 01007158
Transmit      Date: 01/22/03      Time: 10:39    Op: tanya
Original Call Date: 01/17/03      Time: 08:01    Op: tanya
Work to Begin Date: 01/22/03      Time: 08AM

Place: DISTRICT HEIGHTS
Address:             Street: GOULD DR
Nearest Intersecting Street: CHAPPAREL DR

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB PLUS 30FT PAs: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 11:43AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: BILLIE HOOD           Contact Phone: (410)798-5416
Alt. Contact: PAUL SEEN             Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 019   Grid Cells: B07
Explosives: N
MTV01      TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0262   Map Ref:


MISUTIL2003012200376	PG PIPELINE	2003/01/22 10:53:34	00370

NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01024985               Update Of: 01007159
Transmit      Date: 01/22/03      Time: 10:39    Op: tanya
Original Call Date: 01/17/03      Time: 08:02    Op: tanya
Work to Begin Date: 01/22/03      Time: 08AM

Place: DISTRICT HEIGHTS
Address: 1517        Street: GOULD DR
Nearest Intersecting Street: CHAPPAREL DR

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB
Remarks: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 10:44AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: BILLIE HOOD           Contact Phone: (410)798-5416
Alt. Contact: PAUL SEEN             Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 019   Grid Cells: B07
Explosives: N
MTV01      TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0263   Map Ref:


MISUTIL2003012200378	PG PIPELINE	2003/01/22 10:53:37	00371


NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01028221               Update Of: 01011750
Transmit      Date: 01/22/03      Time: 10:41    Op: l.boot
Original Call Date: 01/20/03      Time: 14:25    Op: l.boot
Work to Begin Date: 01/23/03      Time: 07AM

Place: DISTRICT HEIGHTS
Address: 7109        Street: CHAPPARAL DR
Nearest Intersecting Street: GOULD DR

Type of Work: INST GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT, MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 11:44AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES
Contact Name: MIKE PAPPAS           Contact Phone: (410)798-5416
Alt. Contact: PAUL                  Alt. Phone   :
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 019   Grid Cells: B07
Explosives: N
MTV01      TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0265   Map Ref:


MISUTIL2003012200379	PG NON PIPELINE	2003/01/22 10:53:40	00372

NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01028949               Update Of: 01009218
Transmit      Date: 01/22/03      Time: 10:41    Op: l.boot
Original Call Date: 01/21/03      Time: 08:22    Op: l.boot
Work to Begin Date: 01/23/03      Time: 08AM

Place: UPPER MARLBORO
Address:             Street: BACK STRETCH BLVD
Nearest Intersecting Street: STARTING GATE

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB ADDRESSED AS: 5102, 5104 & 5106
: BACK STRETCH BLVD
Remarks: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 11:45AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: MIKE PAPPAS           Contact Phone: (410)798-5416
Alt. Contact:                       Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: K03
Explosives: N
TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0266   Map Ref:


MISUTIL2003012200380	PG NON PIPELINE	2003/01/22 10:53:43	00373

NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01028950               Update Of: 01009219
Transmit      Date: 01/22/03      Time: 10:41    Op: l.boot
Original Call Date: 01/21/03      Time: 08:22    Op: l.boot
Work to Begin Date: 01/23/03      Time: 08AM

Place: UPPER MARLBORO
Address: 5114        Street: BACK STRETCH BLVD
Nearest Intersecting Street: WINNERS CIR

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB
Remarks: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 11:45AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: MIKE PAPPAS           Contact Phone: (410)798-5416
Alt. Contact:                       Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: K03
Explosives: N
TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0267   Map Ref:


MISUTIL2003012200382	PG NON PIPELINE	2003/01/22 10:53:46	00374

NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01028955               Update Of: 01009221
Transmit      Date: 01/22/03      Time: 10:46    Op: l.boot
Original Call Date: 01/21/03      Time: 08:23    Op: l.boot
Work to Begin Date: 01/23/03      Time: 08AM

Place: UPPER MARLBORO
Address: 5100        Street: STARTING GATE DR
Nearest Intersecting Street: TRIPLE CROWN CT

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB
Remarks: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/03 11:45AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: MIKE PAPPAS           Contact Phone: (410)798-5416
Alt. Contact:                       Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: K03
Explosives: N
TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0269   Map Ref:


MISUTIL2003012200383	PG NON PIPELINE	2003/01/22 10:53:49	00375

NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01028957               Update Of: 01009222
Transmit      Date: 01/22/03      Time: 10:46    Op: l.boot
Original Call Date: 01/21/03      Time: 08:23    Op: l.boot
Work to Begin Date: 01/23/03      Time: 08AM

Place: UPPER MARLBORO
Address: 5210        Street: STARTING GATE DR
Nearest Intersecting Street: BACK STRETCH BLVD

Type of Work: INSTALL GAS SVC
Extent of Work:   LOC ENTIRE PROPERTY TO CURB
Remarks: PAINT & FLAG UTILITY MARKS ON SITE-MARK GAS A & B
: REMARK DUE TO WEATHER L.BOOTH 1/22/02 10:46AM PER CALL Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: MIKE PAPPAS           Contact Phone: (410)798-5416
Alt. Contact:                       Alt. Phone   : (410)798-5416
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: K03
Explosives: N
TPG01      WGL06      WSS01      XLII
Send To: PEP05     Seq No: 0270   Map Ref:


MISUTIL2003012200386	PG NON PIPELINE	2003/01/22 10:53:52	00376


NOTICE OF INTENT TO EXCAVATE      EMERGENCY                     CORRECTION
Ticket No: 01031312
Transmit      Date: 01/22/03      Time: 10:50    Op: lakish
Original Call Date: 01/22/03      Time: 08:00    Op: lakish
Work to Begin Date: 01/22/03      Time: 08AM

Place: UPPER MARLBORO
Address: 1024        Street: DREXELGATE LA
Nearest Intersecting Street: ORCHARD DR

Type of Work: REPAIRING BROKEN WTR MAIN
Extent of Work:   LOC IN FRONT OF PROP AND THE STREET IN FRONT
Remarks: EMERGENCY ** CREW ON SITE
: REMARK PROP ASAP                                       Fax: (301)206-8231

Company     : WSSC
Contact Name: CALVIN LEWTHER        Contact Phone: (301)206-8222
Alt. Contact: TONIGHT               Alt. Phone   : (301)206-8222
Work Being Done For: WSSC
State: MD              County: PG
Map: PG    Page: 019   Grid Cells: J06
Explosives: N
MTV01      TPG01      WGL06      WSS01
Send To: PEP05     Seq No: 0272   Map Ref:


MISUTIL2003012200377	PG NON PIPELINE	2003/01/22 10:53:56	00377

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01031950
Transmit      Date: 01/22/03      Time: 10:39    Op: diane
Original Call Date: 01/22/03      Time: 10:35    Op: diane
Work to Begin Date: 01/24/03      Time: 10AM

Place: ANDREW AIR FORCE
Address:             Street: E. PERIMETER RD
Nearest Intersecting Street: PEARL HARBOR DR

Type of Work: INSTL ELEC AND TELE BORE
Extent of Work:   MARK E. PERIMETER RDWAY PLUS 30FT BOTH SIDES START ABOVE
: INTER CONTINUE SOUTH FOR 200FT ....
Remarks:
:                                                        Fax: (703)450-6455

Company     : LOUDOUN TUNNELING CO CORP
Contact Name: JOHN UNDERWOOD        Contact Phone: (703)450-5656
Alt. Contact: TOM AYLESTOCK         Alt. Phone   :
Work Being Done For: PRIME NET INC
State: MD              County: PG
Map: PG    Page: 025   Grid Cells: G05
Explosives: N
MTV01      STS01      TPG01      WGL06      WSS01
Send To: PEP05     Seq No: 0264   Map Ref:


MISUTIL2003012200384	DC NON PIPELINE	2003/01/22 10:53:59	00378

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01031972               Update Of: 00748827
Transmit      Date: 01/22/03      Time: 10:47    Op: terrya
Original Call Date: 01/22/03      Time: 10:41    Op: terrya
Work to Begin Date: 01/24/03      Time: 10AM

Place: NE
Address: 2215        Street: 5TH ST NE
Nearest Intersecting Street: V ST NE

Type of Work: INST FENCE
Extent of Work:   LOC ENTIRE STORAGE AREA WHERE THERE ARE U-HAUL
: TRUCKS AT ABOVE ADDRESS(DIRECTIONS 2 1/2 BLOCKS ON THE LEFT FROM THE
: INTER OF 5TH ST NE AND RHODE ISLAND AV NE)ANY QUESTIONS CALL TOM AT
: 301-440-0907
Remarks: BEST INFORMATION CALLER COULD PROVIDE
:                                                        Fax: (301)441-8600

Company     : HERCULES FENCE
Contact Name: DANIEL                Contact Phone: (301)441-1600
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: AMERCO
State: DC              County: NE
Map: NE    Page: 010   Grid Cells: E11
Explosives: N
ATT01      DCC01      MCI03      NLINK01    PFNT03     QWESTM02   TDC01
 WASA02     WGL07
Send To: PEP06     Seq No: 0103   Map Ref:


MISUTIL2003012200381	PG NON PIPELINE	2003/01/22 10:54:02	00379


NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 01031980
Transmit      Date: 01/22/03      Time: 10:46    Op: kelley
Original Call Date: 01/22/03      Time: 10:43    Op: kelley
Work to Begin Date: 01/22/03      Time: 10AM

Place: CHILLUM
Address: 5910        Street: 15TH AV
Nearest Intersecting Street: MADSON AV

Type of Work: REPAIRING GAS LEAK
Extent of Work:   LOC FRONT OF PROP & RD R.O W
Remarks: EMERGENCY ** CREW ON SITE
:                                                        Fax: (703)750-7977

Company     : WASHINGTON GAS
Contact Name: LARRY DAVIS           Contact Phone: (703)750-4831
Alt. Contact: TONIGHT               Alt. Phone   : (703)750-4831
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 011   Grid Cells: H03
Explosives: N
JTV02      TPG01      WGL06      WSS01
Send To: PEP05     Seq No: 0268   Map Ref:


MISUTIL2003012200385	MONT NON PIPELINE	2003/01/22 10:54:05	00380

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01031994
Transmit      Date: 01/22/03      Time: 10:47    Op: webusr
Original Call Date: 01/22/03      Time: 08:54    Op: webusr
Work to Begin Date: 01/24/03      Time: 09AM

Place: ROCKVILLE
Address: 10805       Street: RED BARN LANE
Nearest Intersecting Street: GLEN MILL RD.

Type of Work: DIGBASEMENT,BACKFILL,FINEGRADE,INSTDRWY &CULVERT
Extent of Work:   MARK ENTIRE LOT# 42 - SUBD. HIGHGATE - TICKET#1014452
Remarks: BOTH FLAGS & PAINT
:                                                        Fax: (301)831-3015

Company     : VALLEY EXCAVATING
Contact Name: MARIE/JUANITA/JEN     Contact Phone: (301)831-6706
Alt. Contact: KATHY                 Alt. Phone   :
Work Being Done For: O`NEILL CONSTRUCTION
State: MD              County: MONT
Map: MONT  Page: 028   Grid Cells: B11
Explosives: N
TMT01      TRU01      WGL06
Send To: PEP05     Seq No: 0271   Map Ref:


MISUTIL2003012200387	MONT NON PIPELINE	2003/01/22 10:54:08	00381

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01031995
Transmit      Date: 01/22/03      Time: 10:50    Op: j.thom
Original Call Date: 01/22/03      Time: 10:47    Op: j.thom
Work to Begin Date: 01/24/03      Time: 11AM

Place: BETHESDA
Address: 7517        Street: ROYAL DOMINION DR
Nearest Intersecting Street: RIVER RD

Type of Work: REPAIR WTR SVC N/E
Extent of Work:   LOC ENTIRE PROP AT ABOVE ADDRESS
Remarks:
:                                                        Fax: (301)216-9538

Company     : GAITHERSBURG PLUMBING INC
Contact Name: TINA THOMAS           Contact Phone: (301)948-8856
Alt. Contact:                       Alt. Phone   :
Work Being Done For: KENNMANER
State: MD              County: MONT
Map: MONT  Page: 035   Grid Cells: B11
Explosives: N
LTC01      STAR01     TMT02      TRU02      WGL06      WSS01
Send To: PEP05     Seq No: 0273   Map Ref:


MISUTIL2003012200388	MONT NON PIPELINE	2003/01/22 10:54:11	00382

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01032000
Transmit      Date: 01/22/03      Time: 10:50    Op: webusr
Original Call Date: 01/22/03      Time: 08:54    Op: webusr
Work to Begin Date: 01/24/03      Time: 09AM

Place: POTOMAC
Address: 10808       Street: RED BARN LANE
Nearest Intersecting Street: GLEN MILL RD.

Type of Work: DIGBASEMENT,BACKFILL,FINEGRADE,INSTDRWY &CULVERT
Extent of Work:   MARK ENTIRE LOT# 34 - SUBD. HIGHGATE - TICKET#1014444
Remarks: BOTH FLAGS & PAINT
:                                                        Fax: (301)831-3015

Company     : VALLEY EXCAVATING
Contact Name: MARIE/JUANITA/JEN     Contact Phone: (301)831-6706
Alt. Contact: KATHY                 Alt. Phone   :
Work Being Done For: O`NEILL CONSTRUCTION
State: MD              County: MONT
Map: MONT  Page: 028   Grid Cells: B11
Explosives: N
TMT01      TRU01      WGL06
Send To: PEP05     Seq No: 0274   Map Ref:


MISUTIL2003012200389	MONT NON PIPELINE	2003/01/22 10:54:14	00383

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01032002
Transmit      Date: 01/22/03      Time: 10:50    Op: l.boot
Original Call Date: 01/22/03      Time: 10:48    Op: l.boot
Work to Begin Date: 01/24/03      Time: 11AM

Place: SILVER SPRING
Address: 3519        Street: GENTRY RIDGE CT
Nearest Intersecting Street: BRIGGS CHANEY RD

Type of Work: INST GAS MAIN & SVC
Extent of Work:   LOC ENTIRE PROP 20FT PAST OPPOSITE
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT-MARK GAS A & B
:                                                        Fax: (301)330-5750

Company     : LINEAL INDUSTRY((XLIN))
Contact Name: NEIMAN GARRETT        Contact Phone: (301)330-9077
Alt. Contact: BOB CACAMESE          Alt. Phone   : (301)330-9077
Work Being Done For: WASHINGTON GAS
State: MD              County: MONT
Map: MONT  Page: 032   Grid Cells: D09
Explosives: N
BGE05      TMT02      TRU02      WGL06      WSS01      XLIN
Send To: PEP05     Seq No: 0275   Map Ref:


�>��
