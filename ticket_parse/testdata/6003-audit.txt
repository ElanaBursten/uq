
0
Date/Time : 12-30-05 at  0:01

LONESTAR
SUMM
DAILY AUDIT OF TICKETS SENT ON 12/29/05

Date/Time: 12/30/2005 12:01:02 AM
Receiving Terminal: ALTEL01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1 36350210-ROUT |  11 36351596-ROUT |  21 36354050-ROUT |  31 36355516-ROUT |
   2 36350651-ROUT |  12 36351672-ROUT |  22 36354075-ROUT |  32 36355476-UPDA |
   3 36350541-ROUT |  13 36351719-ROUT |  23 36354283-ROUT |  33 36355490-UPDA |
   4 36350921-ROUT |  14 36351752-ROUT |  24 36354309-ROUT |  34 36355491-UPDA |
   5 36350922-ROUT |  15 36351785-REPA |  25 36354598-ROUT |  35 36355704-ROUT |
   6 36350928-ROUT |  16 36352210-ROUT |  26 36354960-ROUT |  36 36355682-UPDA |
   7 36351053-ROUT |  17 36352318-UPDA |  27 36355407-ROUT |  37 36355683-UPDA |
   8 36351214-ROUT |  18 36352512-ROUT |  28 36355408-ROUT |  38 36355852-ROUT |
   9 36351478-ROUT |  19 36353505-ROUT |  29 36355441-ROUT |  39 36355902-ROUT |
  10 36351145-ROUT |  20 36354032-ROUT |  30 36355457-ROUT |

* indicates ticket # is repeated

Total Tickets: 39

ROUT - NOTI-ROUTINE                   |   32

REPA - NOTI-REPAIR                    |    1

UPDA - NOTI-UPDATE                    |    6


Please call (713) 432-0365 if this data does
not match the tickets you received on 12/29/05

