\n\n\n\n\nSEQUENCE NUMBER 0004   CDC = DFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     TX NEW MEX POW   VALOR TELECOMM   TXU-GAS-UTQUEST


Locate Request No. 022945290

Prepared By AARON SANCHEZ        On 21-OCT-02  At 0921

MapRef : Deport                     Grid: 3331300951830  Footprint: D01

Location:     County: LAMAR  Town: DEPORT

             Address: 178 1ST SW

Beginning Work Date 10/23/02   Time of Day: 09:15 AM   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : TREE REMOVAL AND BURY STUMPS  
Nature of Work  : TREE REMOVAL AND BURY STUMPS  

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : PAULINE STANSEL
Company Name   : NOT PROVIDED
Work by DID NOT PROVIDE      For PAULINE STANSEL

Person to Contact : PAULINE STANSEL

Phone No.  ( 903 )737-0634 /( 903 )784-5192 ( Hours: 12:00 AM/12:00 PM )
Fax No.    ( 000 )000-0000
Email:     CALLER COULD NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: HWY 271 
LSTK=29400832 SEQNO=0195 DUR=2 DAYS MARK ENTIRE LOT. :EX.COORD: N     
W LAT: 33 31 52 LON: -95 18 56 SE LAT: 33 31 16 LON: -95 18MAP: T     
X PAGE: GRID CELLS: ALTCONTACT-2ND NUMBER ALTPHONE903-784-5192        
                                                                      
                                                                      
                                                                      
FAX #: 000-000-0000                                                   
CALLER COULD NOT PROVIDE EMAIL

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: T1021091.457


022945290 to 918702155584 at 09:24:34 on MON, 10/21/02 for DFD #0004


SEQUENCE NUMBER 0008   CDC = TKA
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
RELIANT / ARKLA  RELIANT ENERGY   BOWIE CASS ELEC  CABLE ONE - TEX
SWEPCO           VALOR TELECOMM   TEXARKANA WATER  LONE STAR XCHG

Locate Request No. 022940905

Prepared By APRIL SANDERS        On 21-OCT-02  At 0842

MapRef : Texarkana                  Grid: 3325000940600  Footprint: O09

Location:     County: BOWIE  Town: TEXARKANA

             Address: 0 US HWY 67 

Beginning Work Date 10/23/02 Time of Day: 08:57 AM   Duration: 21 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : DRILLING AND RD BORE          

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : MITCH BERRY
Company Name   : STRIPING TECHNOLOGY
Work by STRIPING TECHNOLOGY      For TXDOT/H&H HOWARD

Person to Contact : CAROL CARPENTER

Phone No.  ( 903 )521-7209 MBL /( 903 )595-6800 OFC ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 903 )595-6255
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: S WAKE VILLAGE RD 
LOC ENTIRE INTER GOING 100FT IN ALL DIRECTIONS                        
AREAS MARKED W/SILVER STAKES.                                         
                                                                      
                                                                      
UPDATE AND REMARK:022883007

Map Cross Reference : NONE

FaxBack Requested ? YES    Lone Star Xref: 


022940905 to 918702155584 at 09:25:03 on MON, 10/21/02 for TKA #0008


SEQUENCE NUMBER 0009   CDC = TKA
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
RELIANT / ARKLA  BOWIE CASS ELEC  RELIANT ENERGY   CABLE ONE - TEX
SWEPCO           VALOR TELECOMM   TEXARKANA WATER  LEVEL 3 COMM   
LONE STAR XCHG

Locate Request No. 022940908

Prepared By APRIL SANDERS        On 21-OCT-02  At 0843

MapRef : Texarkana                  Grid: 3324300940700  Footprint: O09

Location:     County: BOWIE  Town: TEXARKANA

             Address: 0 US HWY 67 

Beginning Work Date 10/23/02 Time of Day: 08:58 AM   Duration: 21 DAYS 

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : ST LIGHTS AND TRAFFIC SIGNS   

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : MITCH
Company Name   : STRIPING TECHNOLOGY
Work by STRIPING TECHNOLOGY      For TXDOT/H&H HOWARD

Person to Contact : CAROL CARPENTER

Phone No.  ( 903 )521-7209 MBL /( 903 )595-6800 OFC ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 903 )595-6255
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: F-M RD 989 
LOC ENTIRE INTER GOING 100FT IN ALL DIRECTIONS AND LOC GOING W ON THE 
S SIDE OF US 67 FOR APPX 1000FT. AREAS MARKED W/SILVER STAKES.        
                                                                      
UPDATE AND REMARK:022882994

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022940908 to 918702155584 at 09:25:11 on MON, 10/21/02 for TKA #0009


SEQUENCE NUMBER 0011   CDC = FFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
DUKE EN/CHAPARR  VALOR TELECOMM   LONE STAR XCHG

Locate Request No. 022940917

Prepared By JILL NELSON          On 21-OCT-02  At 0844

MapRef : Rosebud                    Grid: 3102000965230  Footprint: O09

Location:     County: MILAM  Town: CAMERON

             Address: 5496 COUNTY RD 278 

Beginning Work Date 10/23/02 Time of Day: 09:00 AM   Duration: 99 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : INSTL FENCE                   

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : EDWIN MOSS
Company Name   : HOMEOWNER
Work by EDWIN MOSS      For EDWIN MOSS

Person to Contact : EDWIN MOSS

Phone No.  ( 254 )583-2200 /(    )     ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 000 )000-0000
Email:     TAMU54MAAS@AOL.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: COUNTY RD 279 
WORK WILL BE DONE NEXT TO THE CATTLE CORAL APPX 300' FROM CR 278.     
CALLER REQUEST TO SET UP TIME TO MEET FOR THE ABOVE DATE/TIME IN FRONT
OF THE ABOVE ADDRESS.

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022940917 to 918702155584 at 09:25:27 on MON, 10/21/02 for FFD #0011


SEQUENCE NUMBER 0012   CDC = FFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENBRIDGE 2 INC   TXU LSP SOUTHEA  SHELL PIPELINE   VALOR TELECOMM 
NAVARRO CO ELEC  ONCOR-E/TXU-G-U  LONE STAR XCHG

Locate Request No. 022940932

Prepared By BILLY DEGUISTO       On 21-OCT-02  At 0846

MapRef : Richland                   Grid: 3152000962230  Footprint: O09

Location:     County: NAVARRO  Town: RICHLAND

             Address: 5107 SW PR 2384 

Beginning Work Date 10/23/02 Time of Day: 09:00 AM   Duration: 99 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCE                         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : KATHY BURNES
Company Name   : HOMEOWNER
Work by KATHY BURNES      For KATHY BURNES

Person to Contact : KATHY BURNES

Phone No.  ( 903 )962-3252 /(    )     ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 000 )000-0000
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: PR 2383 
MARK THE AREA ACROSS THE ST FROM THIS ADRESS 500FT E OR TO FIRST      
PROP PIN

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022940932 to 918702155584 at 09:25:35 on MON, 10/21/02 for FFD #0012


SEQUENCE NUMBER 0013   CDC = CCK
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
DIAMOND SHAMRCK  TXU LSP SOUTHEA  VALOR TELECOMM   MID-SOUTH ELE  
LONE STAR XCHG

Locate Request No. 022940968

Prepared By CORRINA GONZALES     On 21-OCT-02  At 0851

MapRef : Iola                       Grid: 3039300960600  Footprint: O09

Location:     County: GRIMES  Town: IOLA

             Address: 16756 F-M RD 244 

Beginning Work Date 10/23/02 Time of Day: 09:15 AM   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : RD BORE-WATER METER INSTALL   

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : ANNN YOUNG
Company Name   : WICKSON CREEK SUD
Work by WICKSON CREEK SUD      For WICKSON CREEK SUD

Person to Contact : ANN YOUNG

Phone No.  ( 979 )589-3030 OFFICE /(    )     ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 979 )589-3275
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: COUNTY RD 170 
WILL BE WORKING PARALLEL TO THE RD AND THE PRIVATE UTILITY EASEMENT   
*FROM INTERSECTION GO APPX 1.1MI NORTH OF CR 170

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022940968 to 918702155584 at 09:25:42 on MON, 10/21/02 for CCK #0013


SEQUENCE NUMBER 0014   CDC = HTN
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
VALOR TELECOMM   ONCOR-ELECTRIC   LONE STAR XCHG

Locate Request No. 022941007

Prepared By APRIL SANDERS        On 21-OCT-02  At 0858

MapRef : Cushing                    Grid: 3150300944900  Footprint: D04

Location:     County: NACOGDOCHES  Town: CUSHING

             Address: 0 FM 225 

Beginning Work Date 10/23/02 Time of Day: 09:15 AM   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : DITCH CLEANING                
Nature of Work  : CLEANING OUT DITCHES          

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JOE BURKE
Company Name   : TXDOT
Work by TXDOT      For TXDOT

Person to Contact : JOE BURKE

Phone No.  ( 936 )564-4871 /(    )     ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 936 )564-0378
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: STATE HWY 204 
FROM INTER GO N ON FM 225 FOR APX 2.25M TO THE COUNTY LINE AT RTE MKR 
328-START THERE AND GO S BACK TOWARDS CUSHING FOR 1M ON THE W SIDE    
OF FM 225

Map Cross Reference : NONE

FaxBack Requested ? YES    Lone Star Xref: 


022941007 to 918702155584 at 09:25:49 on MON, 10/21/02 for HTN #0014


SEQUENCE NUMBER 0015   CDC = FFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU LSP SOUTHEA  SM&P-F           HUNT PETRO CORP  ANADARKO PETRO 
VALOR TELECOMM   NAVASOTA VALLEY  ONCOR-ELEC-UQ    OCEAN ENERGY   
CHEVRON          LONE STAR XCHG

Locate Request No. 022941010

Prepared By RYAN MARTIN          On 21-OCT-02  At 0858

MapRef : Fairfield                  Grid: 3137000961100  Footprint: O09

Location:     County: FREESTONE  Town: FAIRFIELD

             Address: 0 COUNTY RD 660 

Beginning Work Date 10/23/02 Time of Day: 09:15 AM   Duration: 20 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OIL FIELD CONSTRUCTION        
Nature of Work  : BUILD WELL PAD                

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : RAY MOORE
Company Name   : FLINT ENERGY
Work by FLINT ENERGY      For XTO

Person to Contact : RAY MOORE

Phone No.  ( 903 )391-5148 MBL /( 903 )389-8716 OFC ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 903 )389-7495
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: US HWY 75 
FROM INTER GO W ON CR 660 FOR APPX 3 MI AND GO SE ON LEASE RD FOR APPX
2000FT. GO THROUGH FENCE AND TURN LEFT HEADING NE FOR APPX 600FT TO   
LOC. MARKED W/STEEL POST AND PINK FLAG. THIS IS THE CLARK #4.         
LOC RADIUS OF 400*400.

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022941010 to 918702155584 at 09:25:55 on MON, 10/21/02 for FFD #0015


SEQUENCE NUMBER 0017   CDC = FFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
HILCO ELEC COOP  VALOR TELECOMM   LONE STAR XCHG

Locate Request No. 022941096

Prepared By TERESA CREAGER       On 21-OCT-02  At 0909

MapRef : Whitney                    Grid: 3146300971600  Footprint: O09

Location:     County: HILL  Town: TYSON COMM

             Address: 322 CR 2215 

Beginning Work Date 10/23/02 Time of Day: 09:30 AM   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : INSTL 1 POLE                  

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : KERRY UPCHURCH
Company Name   : HILCO ELECTRIC COOPERATIVE, INC.
Work by HILCO ELECTRIC COOPERATIVE, INC./ RED SIMPSON      For HILCO ELECTRIC COOP

Person to Contact : KERRY UPCHURCH

Phone No.  ( 800 )338-6425 OFC /( 254 )687-2331 ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 254 )687-2515
Email:     BWRIGHT@HILCOZAP.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: CR 2203 
WORK WILL BE DONE ON THE W SIDE OF PROPERTY. AREA MARKED W/ STAKES    
& PINK RIBBON.

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022941096 to 918702155584 at 09:26:10 on MON, 10/21/02 for FFD #0017


SEQUENCE NUMBER 0018   CDC = NBN
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
BOWIE CASS ELEC  R/R ARMY DEPOT   TEXARKANA WATER  RED RIVER AUTH 
VALOR TELECOMM   LONE STAR XCHG

Locate Request No. 022941118

Prepared By RYAN MARTIN          On 21-OCT-02  At 0911

MapRef : Maud                       Grid: 3323300941830  Footprint: O09

Location:     County: BOWIE  Town: REDWATER

             Address: 0 COUNTY RD 1108 

Beginning Work Date 10/23/02 Time of Day: 09:30 AM   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCE                         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : TOMMY CARAWAY
Company Name   : TOMMY CARRAWAY
Work by TOMMY CARRAWAY      For MR. CHARELS W ARMSTRONG

Person to Contact : TOMMY CARAWAY

Phone No.  ( 903 )543-2206 HOME /(    )     ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 000 )000-0000
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: F-M RD 2149 
FROM INTER GO N ON CR 1108 FOR APPX 2 MI TO LOC ON THE S SIDE OF THE  
RD. LOC BETWEEN CATTLE GUARD AND PUMP STATION AND NEW FENCE POSTS.    
PLS LOC APPX 100FT PAST NEW FENCE.

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022941118 to 918702155584 at 09:26:16 on MON, 10/21/02 for NBN #0018


SEQUENCE NUMBER 0019   CDC = FFD
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU LSP SOUTHEA  TEX NEW MEX POW  VALOR TELECOMM   TXU - GAS      
ONCOR-ELECTRIC   LONE STAR XCHG

Locate Request No. 022941135

Prepared By JESSICA WILLIAMS     On 21-OCT-02  At 0913

MapRef : Crawford                   Grid: 3132000972630  Footprint: O09

Location:     County: MCLENNAN  Town: CRAWFORD

             Address: 0 9TH ST 

Beginning Work Date 10/23/02 Time of Day: 09:30 AM   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : INSTLL POLES/DOWNGUIDES       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : RONNIE PAYNE
Company Name   : TEXAS NEW MEXICO POWER COMPANY
Work by TEXAS NEW MEXICO POWER COMPANY      For TEXAS NEW MEXICO POWER CO

Person to Contact : RONNIE PAYNE

Phone No.  ( 254 )675-3908 X 110 /( 254 )386-2292 MBL ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 254 )675-2463
Email:     CALLER COULD NOT PROVIDE E-MAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: 2ND ST 
START LOC AT INTER AND CONT GOING S ON THE W SIDE OF 9TH ST FOR       
APPX 300'

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


022941135 to 918702155584 at 09:26:24 on MON, 10/21/02 for FFD #0019



