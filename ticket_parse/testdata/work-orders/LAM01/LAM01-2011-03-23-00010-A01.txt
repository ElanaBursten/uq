﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0002738</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>E7014890</JOBID>
  <ORDERNUMBER>BMDPS1O00</ORDERNUMBER>
  <DUEDATE>3/6/2011 11:00:00 PM</DUEDATE>
  <MON>MD00091441185</MON>
  <ISSUEDATE>2/28/2011 8:56:00 AM</ISSUEDATE>
  <CUSTOMERNAME>MICHAEL KILDUFF</CUSTOMERNAME>
  <CUSTOMERADDRESS>411 WINTERBERRY CT</CUSTOMERADDRESS>
  <CUSTOMERCOUNTY>HARFORD</CUSTOMERCOUNTY>
  <CUSTOMERCITY>EDGEWOOD</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>21040</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>(443) 253-5354</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>(A04) 084-8504</CUSTOMERBTN>
  <WIRECENTER>410538</WIRECENTER>
  <WORKCENTER>22</WORKCENTER>
  <CO>1222</CO>
  <SERVINGTERMINAL>HH F412 WINTERBERRY CT</SERVINGTERMINAL>
  <TERMINALGPS>39.431939 / -76.273800</TERMINALGPS>
  <MAP>4584</MAP>
  <GRID>G2</GRID>
  <CIRCUITID>11/BURY/PS1O00/ /VZMD</CIRCUITID>
  <F2CABLE>H2033</F2CABLE>
  <TERMINALPORT>2</TERMINALPORT>
  <F2PAIR>14</F2PAIR>
</XML>