﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0010943</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>M789055</JOBID>
  <ORDERNUMBER>CH02HJVS</ORDERNUMBER>
  <DUEDATE>4/4/2011 12:00:00 AM</DUEDATE>
  <MON />
  <ISSUEDATE>4/19/2011 12:00:00 AM</ISSUEDATE>
  <CUSTOMERNAME>MEMORIAL PARK GLEN H</CUSTOMERNAME>
  <CUSTOMERADDRESS>7231 RITCHIE HIGHWAY N</CUSTOMERADDRESS>
  <CUSTOMERCOUNTY>ANNE ARUNDEL</CUSTOMERCOUNTY>
  <CUSTOMERCITY>GLEN BURNIE</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>21061</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>1410-361-0146</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>1410-766-4950</CUSTOMERBTN>
  <WIRECENTER>410760</WIRECENTER>
  <WORKCENTER />
  <CO />
  <SERVINGTERMINAL>P3 F 304 7TH AV</SERVINGTERMINAL>
  <TERMINALGPS />
  <MAP>5057</MAP>
  <GRID>B3</GRID>
  <CIRCUITID />
  <F2CABLE>22</F2CABLE>
  <TERMINALPORT />
  <F2PAIR>1042</F2PAIR>
</XML>