﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0002950</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>E7021140</JOBID>
  <ORDERNUMBER>BMDBC2O00</ORDERNUMBER>
  <DUEDATE>3/9/2011 11:00:00 PM</DUEDATE>
  <MON>MD00091510397</MON>
  <ISSUEDATE>3/1/2011 2:03:00 PM</ISSUEDATE>
  <CUSTOMERNAME>PEGGY JACOBS</CUSTOMERNAME>
  <CUSTOMERADDRESS>1910 ROBERT BOWIE DR</CUSTOMERADDRESS>
  <CUSTOMERCOUNTY>PRINCE GEORGES</CUSTOMERCOUNTY>
  <CUSTOMERCITY>UPPER MARLBORO</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>20774</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>(202) 215-0892</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>(301) 218-9244</CUSTOMERBTN>
  <WIRECENTER>301249</WIRECENTER>
  <WORKCENTER>22</WORKCENTER>
  <CO>1222</CO>
  <SERVINGTERMINAL>PED 1908-1 ROBERT BOWIE DR</SERVINGTERMINAL>
  <TERMINALGPS>38.865178 / -76.790782</TERMINALGPS>
  <MAP>5652</MAP>
  <GRID>D2</GRID>
  <CIRCUITID>11/BURY/BC2O00/ /VZMD</CIRCUITID>
  <F2CABLE>H3022</F2CABLE>
  <TERMINALPORT>3</TERMINALPORT>
  <F2PAIR>87</F2PAIR>
</XML>