
FAXCFM 00007 KUPIa 07/11/2006 12:07:54 0607110011-01A NORM RXMT GRID

RETRANSMIT

Ticket : 0607110011 Date: 07/11/2006 Time: 11:43 Oper: DEFAULT Chan:000
Old Tkt: 0607110011 Date: 07/11/2006 Time: 11:04 Oper: Default Rev: 00A

State: KY Cnty: HART City: HORSE CAVE
Subdivision:

Address : 3399
Street  : UNO HORSE CAVE RD
Cross 1 : LONOKE RD
Location: FRONT
:
Boundary: n 37.196716    s 37.195320    w -85.839622    e -85.834114

Work type : INSTALL POLES
Done for  : TAMMIE ARVIN
Start date: 07/13/2006  Time: 11:15  Hours notice: 47/47  Priority: NORM
Ug/Oh/Both: U  Blasting: NO                          Emergency: N
Duration  : N/A           Depth: 1FT

Company : FISHEL CO  Type: CONT
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Contact : N/A Phone:
Fax     : (502)266-5743
Email   : BJRUNYAN@FISHELCO.COM

Remarks : THIS TICKET REPLACES 0607110001 DUE TO WRONG ADDR SUPPLIED BY CALLER
* KUPI TEST WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR DATABASE
* 0012 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:43
Members: 0012  


FAXCFM 00001 KUPIa 07/11/2006 12:07:51 0607100016-00A SHRT NEW  GRID

SHORT

Ticket : 0607100016 Date: 07/10/2006 Time: 15:42 Oper: DEFAULT Chan:000

State: KY Cnty: GREEN City: BUFFALO
Subdivision:

Address : 
Street  : WARDS RD
Cross 1 : GEORGE BELL RD
Location: THIS IS A TEST---PLEASE DO NOT LOCATE AREA---THIS IS A TEST---THANK
YOU
:
Boundary: n 37.444172    s 37.440323    w -85.609581    e -85.607307

Work type : INSTALLING MAIN
Done for  : LOUISVILLE GAS
Start date: 07/11/2006  Time: 14:00  Hours notice: 22/22  Priority: SHRT
Ug/Oh/Both: U  Blasting: NO                          Emergency: N
Duration  : N/A           Depth: 4 FEET

Company : LOUISVILLE GAS  Type: MEMB
Co addr : 10300 BALLARDSVILLE ROAD
City    : LOUISVILLE State: KY Zip: 40241
Caller  : LINDA Phone: (502)333-1915
Contact : LINDA Phone:
Fax     : (502)333-1823

Remarks : CONTRACTOR WOULD LIKE TO BEGIN DIGGING 7/11/06 AT 2PM.  HE WOULD
GREATLY APPRECIATE EARLY LOCATES.  THANK YOU!
* ALLTEL KENTUCKY INC WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR
DATABASE
:

Submitted date: 07/10/2006 Time: 15:42
Members: 0002   0015   0069   5005  


FAXCFM 00002 KUPIa 07/11/2006 12:07:52 0607110002-02A EMER DAMG GRID

RETRANSMIT

Ticket : 0607110002 Date: 07/11/2006 Time: 11:47 Oper: DEFAULT Chan:000
Old Tkt: 0607110002 Date: 07/11/2006 Time: 11:29 Oper: Default Rev: 01A

State: KY Cnty: HART City: HORSE CAVE
Subdivision:

Address : 1546
Street  : UNO HORSE CAVE RD
Cross 1 : LONOKE RD
Location: 2ND POLE ON L SIDE OF ADDRESS
:
Boundary: n 37.197304    s 37.192951    w -85.839546    e -85.830544

Work type : INST WATER
Done for  : SELF
Start date: 07/11/2006  Time: 09:30  Hours notice: 0/0  Priority: EMER
Ug/Oh/Both: U  Blasting: UNKNOWN                          Emergency: Y
Duration  : N/A           Depth: 6 FEET

Company : FISHEL CO  Type: CONT
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Fax     : (502)266-5743
Email   : BJRUNYAN@FISHELCO.COM

Remarks : TEST      2ND HOUSE ON R
HIT GAS LINE
* 0015 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
* KUPI TEST WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR DATABASE
* 0012 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:47
Members: 0012  


FAXCFM 00003 KUPIa 07/11/2006 12:07:53 0607110003-01A EMER RXMT GRID

RETRANSMIT

Ticket : 0607110003 Date: 07/11/2006 Time: 11:46 Oper: DEFAULT Chan:000
Old Tkt: 0607110003 Date: 07/11/2006 Time: 09:25 Oper: Default Rev: 00A

State: KY Cnty: HART City: HORSE CAVE
Subdivision:

Address : 
Street  : UNO HORSE CAVE RD
Cross 1 : LONOKE RD
Location: 2ND POLE ON L SIDE OF ADDRESS
:
Boundary: n 37.197304    s 37.192951    w -85.839546    e -85.830544

Work type : INST WATER
Done for  : SELF
Start date: 07/11/2006  Time: 09:30  Hours notice: 0/0  Priority: EMER
Ug/Oh/Both: U  Blasting: UNKNOWN                          Emergency: Y
Duration  : N/A           Depth: 6 FEET

Company : FISHEL CO  Type: CONT
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Fax     : (502)266-5743
Email   : BJRUNYAN@FISHELCO.COM

Remarks : TEST      2ND HOUSE ON R
HIT GAS LINE
* KUPI TEST WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR DATABASE
* 0012 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:46
Members: 0012  


FAXCFM 00005 KUPIa 07/11/2006 12:07:54 0607110006-01A NORM RXMT GRID

RETRANSMIT

Ticket : 0607110006 Date: 07/11/2006 Time: 11:23 Oper: DEFAULT Chan:000
Old Tkt: 0607110006 Date: 07/11/2006 Time: 09:30 Oper: Default Rev: 00A

State: KY Cnty: HART City: HORSE CAVE
Subdivision: HORSE CAVE  Lot: 14523

Address : 
Street  : UNO HORSE CAVE RD
Location: 2ND POLE ON L SIDE OF ADDRESS
:
Boundary: n 37.197304    s 37.192951    w -85.839546    e -85.830544

Work type : INST WATER
Done for  : HOMEOWNER
Start date: 07/13/2006  Time: 09:45  Hours notice: 46/46  Priority: NORM
Ug/Oh/Both: U  Blasting: POSSIBLE                          Emergency: N
Duration  : N/A           Depth: 6 INCHES

Company : FISHEL CO  Type: HOME
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Email   : B_JRUNYAN@FISHELCO.COM

Remarks : TEST      2ND HOUSE ON R
CREW ON SITE  PLEASE LOC ASAP

1234567890
* KUPI TEST AND SHELL PIPELINE COMPANY INC WERE MANUALLY ADDED TO THIS TICKET,
PLEASE CHECK YOUR DATABASE
* 0012 AND 0008 WERE NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:23
Members: 0008   0012  


FAXCFM 00004 KUPIa 07/11/2006 12:07:53 0607110009-01A INPR RXMT STRT

RETRANSMIT

Ticket : 0607110009 Date: 07/11/2006 Time: 11:44 Oper: DEFAULT Chan:000
Old Tkt: 0607110009 Date: 07/11/2006 Time: 09:54 Oper: Default Rev: 00A

State: KY Cnty: HART City: HORSE CAVE
Subdivision:

Address : 3299
Street  : UNO HORSE CAVE RD
Cross 1 : LONOKE RD
Location: FRONT
:
Boundary: n 37.196407    s 37.194687    w -85.832581    e -85.827576

Work type : INSTALL POLES
Done for  : TAMMIE ARVIN
Start date: 07/11/2006  Time: 10:10  Hours notice: 0/0  Priority: INPR
Ug/Oh/Both: U  Blasting: NO                          Emergency: N
Duration  : N/A           Depth: 1FT

Company : FISHEL CO  Type: CONT
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Contact : N/A Phone:
Fax     : (502)266-5743
Email   : BJRUNYAN@FISHELCO.COM

Remarks : * KUPI TEST WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR
DATABASE
* 0012 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:44
Members: 0012  


FAXCFM 00006 KUPIa 07/11/2006 12:07:54 0607110010-04A NORM RXMT STRT

RETRANSMIT THIRD NOTICE 

Ticket : 0607110010 Date: 07/11/2006 Time: 11:22 Oper: DEFAULT Chan:000
Old Tkt: 0607110010 Date: 07/11/2006 Time: 11:00 Oper: Default Rev: 03A

State: KY Cnty: HART City: HORSE CAVE
Subdivision:

Address : 3299
Street  : UNO HORSE CAVE RD
Cross 1 : LONOKE RD
Location: FRONT
:
Boundary: n 37.196407    s 37.194687    w -85.832581    e -85.827576

Work type : INSTALL POLES
Done for  : TAMMIE ARVIN
Start date: 07/13/2006  Time: 10:45  Hours notice: 47/47  Priority: NORM
Ug/Oh/Both: U  Blasting: NO                          Emergency: N
Duration  : N/A           Depth: 1FT

Company : FISHEL CO  Type: CONT
Co addr : 4508 BISHOP LN
City    : LOUISVILLE State: KY Zip: 40218
Caller  : BRENDA RUNYAN Phone: (502)456-2900
Contact : N/A Phone:
Fax     : (502)266-5743
Email   : BJRUNYAN@FISHELCO.COM

Remarks : PER BRENDA STILL NEED BELLSOUTH TO MARK PROP
* KUPI TEST WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR DATABASE
* 0012 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
:

Submitted date: 07/11/2006 Time: 11:22
Members: 0012  

0224   00010 KUPIa 07/17/2006 11:41:01 0607170046-01A NORM RXMT STRT

RETRANSMIT

Ticket : 0607170046 Date: 07/17/2006 Time: 11:37 Oper: DEFAULT Chan:000
Old Tkt: 0607170046 Date: 07/17/2006 Time: 08:04 Oper: JCOX Rev: 00A

State: KY Cnty: CHRISTIAN City: FORT CAMPBELL
Subdivision:

Address : 4528
Street  : BEERS ST
Cross 1 : HOLLAND ST
Location: APT A   LOC REAR EASEMENT
:
Boundary: n 36.672588    s 36.666508    w -87.465073    e -87.462761

Work type : BURY CATV LINES
Done for  : C CORE
Start date: 07/19/2006  Time: 08:15  Hours notice: 44/44  Priority: NORM
Ug/Oh/Both: U  Blasting: NO                          Emergency: N
Duration  : N/A           Depth: 12 IN

Company : D AND M TRENCHING  Type: CONT
Co addr : 6335 PAYNE RD
City    : PORTLAND State: TN Zip: 37148
Caller  : LISA SPIVEY Phone: (615)642-1021
Contact : DONALD SPIVEY Phone:
Fax     : (615)325-7221
Email   : VMTRENCHING@VERTEX.COM

Remarks : LOCATED IN MONTGOMERY COUNTY TENNESSEE
* BELLSOUTH TENNESSEE AND CHARTER COMMUNICATIONS WERE MANUALLY ADDED TO THIS
TICKET, PLEASE CHECK YOUR DATABASE
:

Submitted date: 07/17/2006 Time: 11:37
Members: 0010   0224   0333  

