

SEQUENCE NUMBER 0001   CDC = MH 
 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @
 @     @   @ @     @  @  @   @ @     @  @@ @       @
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @

 AW +VRZN WV CL-UTIL CW =MOUNT GAS/WHLNG MH +AEP/WHELING PWR MPW=AE ELE/WRT-UTIL
 VOB+BETHLEHEM VLG   TCB+AT&T-BRIDGEPORT

                  ------>  Miss Utility of West Virginia Notice <-----

 NOTICE: 3570001  -->EMERGENCY                          Lead Time:    0

 County:         OHIO                       Town:       BETHLEHEM
 Street:         93  CHAPEL RD 
 Near Inter:     PINE LN
 Excavation Length:                         Excavation Direction: 
 Excavation Depth : 3FT                     Blasting:   NO
 Work for:       VILLAGE OF BETHLEHEM
 Type of Work:   REPAIRING WATER MAIN
 In St:          X                          On Sidewk:  
 Other:          
 On Prop Location:                          UNKNOWN
 Dig Site Marked in White ?: NO     

 Start Date:     23-DEC-05                  Time:        0115
 DBLookup:       STREET GRIDS 15            Prepared By: DAVID KRANING

 Remarks:  Distance From Intersection:      Direction: 
 EMERGENCY-CREW IS EN ROUTE

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 Contractor:          BETHLEHEM, VILLAGE OF
 Phone:               304-233-9527
 Address:             1 COMMUNITY PARK RD; BETHLEHEM WV 26003
 Contact:             DALE BRUNO           Contact Phone:   304-233-9527
 Alt Contact Phone:                        Fax:             304-231-2187
 Alt Contact:                              Alt Phone:       
 Caller:              DALE BRUNO           Prepared:        23-DEC-05@0116

 Grids:
      40028-080416  40030-080418  40030-080416  40030-080414  40028-080418 
      40028-080414  40026-080418  40026-080416  40026-080414  40032-080418 
      40032-080416  40032-080414  40030-080412  40028-080412  40026-080412




SEQUENCE NUMBER 0045   CDC = PC 
 @@@@   @@@  @   @ @@@@@ @@@@@ @   @ @@@@@
 @   @ @   @ @   @   @     @   @@  @ @
 @@@@  @   @ @   @   @     @   @ @ @ @@@@
 @ @   @   @ @   @   @     @   @ @ @ @
 @  @  @   @ @   @   @     @   @  @@ @
 @   @  @@@   @@@    @   @@@@@ @   @ @@@@@

 AB =VERIZON W V-PRO CF =CABOT O&G/CR PL KH =MARKWEST HYDRO  PC =AEP-CHRLN-PRMRK
 CC =MOUNT GAS/PRMK  UC =UNCB CHARLESTON PEN=N COAST ENGY E  PKE=N COAST ENGY E 


             ------>  Miss Utility of West Virginia Notice <-----

 NOTICE: 3560111  -->ROUTINE                            Lead Time:   48

 County:         KANAWHA                    Town:       TORNADO
 Street:         137  BUCKNER RD 
 Near Inter:     SMITH RD
 Excavation Length: UNKNOWN                 Excavation Direction:  U
 Excavation Depth : 8 FT                    Blasting:   NO
 Work for:       AMERICAN ELECTRIC POWER
 Type of Work:   INSTALL GROUND RD
 In St:                                    On Sidewk:  X
 Other:          
 On Prop Location:                          UNKNOWN
 Dig Site Marked in White ?: UNKNOWN

 Start Date:     27-DEC-05                  Time:        1530
 DBLookup:       PINPOINT GRIDS 20          Prepared By: DEBBIE RIZZO

 Remarks:  Distance From Intersection: UNKNOWN        Direction: U
 NEAR POLE 38820208D20098 SMITH RD OUT OF SPRING HILL TO GORE
 ADDITION 2 ROAD TO RIGHT ON TO BUCKNER RD TO 137. NEW GARAGE ON
 LEFT.
 WORKING AT THE ABOVE ADDRESS.
 GRID PROVIDED: N 38.317138 - W 81.846128
 STS #10164856

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 Contractor:          AMERICAN ELECTRIC POWER COMPANY
 Phone:               304-746-2598
 Address:             PO BOX 1986; CHARLESTON WV 25327
 Contact:             PHIL LAUGHERY        Contact Phone:   304-746-2556
 Alt Contact Phone:                        Fax:             304-746-2605
 Alt Contact:                              Alt Phone:       304-543-6017
 Caller:              GAIL COLEMAN         Prepared:        22-DEC-05@1522

 Grids:
      38188-081504  38190-081504  38192-081504  38194-081504  38196-081504 
      38188-081506  38190-081506  38192-081506  38194-081506  38196-081506 
      38188-081508  38190-081508  38192-081508  38194-081508  38196-081508 
      38188-081510  38190-081510  38192-081510  38194-081510  38196-081510


