

AGL118 03394 GAUPC 11/09/06 17:13:14 11096-075-043-000 NORMAL
Underground Notification
Ticket : 11096-075-043 Date: 11/09/06 Time: 17:06 Revision: 000

State: GA  County: BIBB         Place: MACON
Addr : From: 224    To:        Name:    ORANGE                         ST   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: LOCATE THE ENTIRE FRONT OF THE PROPERTY INCLUDING  THE ROAD

Grids    : 3250B8338D  
Work type: REPAIR SEWER SERVICE
Work date: 11/15/06 Time: 07:00 Hrs notc: 133 Priority: 3
Legal day: 11/15/06 Time: 07:00 Good thru: 12/01/06 Restake by: 11/28/06
RespondBy: 11/14/06 Time: 23:59 Duration : 2 DAY
Done for : UPTON SINCLAIR
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : THIS TICKET IS EXTENDING THE WORK TIME FOR EMERGENCY TICKET
        : #11096-075-042
        : CREW IS ON STANDBY
        : *** NEAR STREET ***   WALNUT ST
        : *** WILL BORE N0NE

Company : WATSON PLUMBING AND ASSOCIATES            Type: CONT
Co addr : 2996 GRAY HWY
City    : MACON                          State   : GA Zip: 31211
Caller  : MATT NELSON                    Phone   : 478-788-0292                
Fax     :                                Alt. Ph.: 
Contact : MATT NELSON                    
Cellular: 478-731-4485                   
Submitted date: 11/09/06 Time: 17:06 Oper: 075 Chan: WEB
Mbrs : AGL118 ATT02  BGAWM  CCM01  GP500  MAC01  MAC02  MWA01  MWA02 
-------------------------------------------------------------------------------

