
CMSDC00 00087 UNCC 11/12/03 10:05 AM 0654878-000 NORM UPDT GRID LREQ

Ticket Nbr: 0654878 Update of: 0620838
Original Call Date: 10/24/03  Time: 02:12 PM  Op: JAL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: ARAPAHOE         City: AURORA
Addr:  15018  Street: E ARKANSAS DR
Grids: 04S066W19NW               :              :               Legal: C
Type of Work: REPL ELEC FEEDER                           Exp.: N  Boring: Y
Location: LOC ENTIRE LOT **ACCESS OPEN** *UPDATE/MARKS NEED REFRESHING*
Company : RIVIERA ELECTRICAL
Caller  : VAL                        Phone: (303)937-3294
Alt Cont: BOB CLARK                  Phone: (303)419-8066
Fax: (303)937-3296  Email: becky.pittman@rivieraies.com
Done for: SECIAL PROJECTS
Remarks:

Members :ADSTA1 :CMSDC00       :CMSSD28       :ICGTL1 :PCHPSD :PCSD00 :PSSD28
Members :QLNCSD00      :QLNCSD28


CMSDN00 00095 UNCC 11/12/03 10:05 AM 0654879-000 NORM NEW STRT LREQ

Ticket Nbr: 0654879
Original Call Date: 11/12/03  Time: 10:02 AM  Op: DEL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: JEFFERSON        City: ARVADA
Addr:   6816  Street: W 84TH CIR
Near Intersection(s): W 84TH WAY
Grids: 02S069W25SW  : 02S069W26NW               :               Legal: C
Type of Work: DROP BURY 1                                Exp.: N  Boring: N
Location: APT 40- LOC ENTIRE LOT TO INCLUDE FROM HOUSE TO PED **ACCESS OPEN**
        : *PAINT/FLAG/LV A SKETCH*
Company : 180 CONNECT
Caller  : ED VARGAS                  Phone: (303)591-8840
Fax: (303)291-1253  Email:
Done for: COMCAST BROADBAND
Remarks:

Members :CDOT06 :CMSDN00       :CMSND11       :LVL311 :PCQ00  :PSND11 :QLNCND00
Members :QLNCND11      :WSTM02


CMSDC00 00088 UNCC 11/12/03 10:05 AM 0654882-000 NORM UPDT GRID LREQ

Ticket Nbr: 0654882 Update of: 0620843
Original Call Date: 10/24/03  Time: 02:12 PM  Op: JAL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: ARAPAHOE         City: AURORA
Addr:  15053  Street: E IDAHO PL
Grids: 04S066W19NE               :              :               Legal: C
Type of Work: REPL ELEC FEEDER                           Exp.: N  Boring: Y
Location: LOC ENTIRE LOT **ACCESS OPEN* *UPDATE/MARKS NEED REFRESHING*
Company : RIVIERA ELECTRICAL
Caller  : VAL                        Phone: (303)937-3294
Alt Cont: BOB CLARK                  Phone: (303)419-8066
Fax: (303)937-3296  Email: becky.pittman@rivieraies.com
Done for: SECIAL PROJECTS
Remarks:

Members :CMSDC00       :CMSSD28       :PCHPSD :PCSD00 :PSSD28 :QLNCSD00
Members :QLNCSD28


CMSDN00 00096 UNCC 11/12/03 10:06 AM 0654884-000 NORM NEW STRT LREQ

Ticket Nbr: 0654884
Original Call Date: 11/12/03  Time: 10:05 AM  Op: DEL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: JEFFERSON        City: ARVADA
Addr:   7113  Street: ELLIS ST
Near Intersection(s): W 71ST PL
Grids: 03S069W06NW               :              :               Legal: C
Type of Work: DROP BURY 1                                Exp.: N  Boring: N
Location: LOC ENTIRE LOT TO INCLUDE FROM HOUSE TO PED **ACCESS OPEN**
        : *PAINT/FLAG/LV A SKETCH*
Company : 180 CONNECT
Caller  : ED VARGAS                  Phone: (303)591-8840
Fax: (303)291-1253  Email:
Done for: COMCAST BROADBAND
Remarks:

Members :CDOT06 :CMSDN00       :CMSND10       :PCQ00  :PSND10 :QLNCND00
Members :QLNCND10      :UCCT01


CMSDN00 00097 UNCC 11/12/03 10:06 AM 0654887-000 NORM NEW STRT LREQ

Ticket Nbr: 0654887
Original Call Date: 11/12/03  Time: 10:00 AM  Op: LMT
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 01:00 PM  Hrs Notice: 117  Meet: Y
Non-scheduling members please meet at requested date and time or call customer.
State: CO     County: ADAMS            City: WESTMINSTER
Addr:      0  Street: HARMONY PKWY
Near Intersection(s): W 130TH DR
Grids: 01S068W28SW               :              :               Legal: N
Type of Work: IRRIGATION                                 Exp.: N  Boring: N
Location: FRM INTERSECT LOC N/ ON W/SD OF HARMONY PKWY TO 131ST DR FOR A WIDTH
        : W/ TO VALLEJO CIR **ACCESS OPEN**MT @ INTERSECT
Company : URBAN FARMER
Caller  : GABE BENEDICT              Phone: (303)853-8585
Alt Cont: CELL                       Phone: (303)901-6435
Fax: (303)853-8584  Email:
Done for: NEWMAN HOMES
Remarks:

Members :CMSDN00       :CMSND22       :PCQ00  :PSND22 :QLNCND00      :QLNCND22
Members :WSTM02


CMSDC00 00089 UNCC 11/12/03 10:06 AM 0654888-000 NORM UPDT GRID LREQ

Ticket Nbr: 0654888 Update of: 0620844
Original Call Date: 10/24/03  Time: 02:13 PM  Op: JAL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: ARAPAHOE         City: AURORA
Addr:  15063  Street: E IDAHO PL
Grids: 04S066W19NE               :              :               Legal: C
Type of Work: REPL ELEC FEEDER                           Exp.: N  Boring: Y
Location: LOC ENTIRE LOT **ACCESS OPEN** *UPDATE/MARKS NEED REFRESHING*
Company : RIVIERA ELECTRICAL
Caller  : VAL                        Phone: (303)937-3294
Alt Cont: BOB CLARK                  Phone: (303)419-8066
Fax: (303)937-3296  Email: becky.pittman@rivieraies.com
Done for: SECIAL PROJECTS
Remarks:

Members :CMSDC00       :CMSSD28       :PCHPSD :PCSD00 :PSSD28 :QLNCSD00
Members :QLNCSD28


CMSDN00 00098 UNCC 11/12/03 10:07 AM 0654891-000 REQ1 NEW STRT LREQ

Ticket Nbr: 0654891
Original Call Date: 11/12/03  Time: 10:01 AM  Op: JLP
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: ADAMS            City: COMMERCE CITY
Addr:  14833  Street: E 119TH AVE
Grids: 02S066W06NE               :              :               Legal: C
Type of Work: REPR H20 SERV                              Exp.: N  Boring: N
Location: LOC ENT LOT TO INCL B/SDS OF LOT **ACCESS OPEN** REQ 1*
Company : EXCO INC.
Caller  : MANDY HANCOCK              Phone: (303)833-1179
Fax: (303)833-4124  Email: excoincorp@aol.com
Done for: RICHMOND HOMES
Remarks:

Members :CDOT06 :CMSDN00       :CMSND15       :LVL311 :PCQ00  :PSND15 :QLNCND00
Members :QLNCND15      :SCH27J :SPRN01 :UNPR06


CMSDW00 00080 UNCC 11/12/03 10:07 AM 0654892-000 REQ1 NEW STRT LREQ

Ticket Nbr: 0654892
Original Call Date: 11/12/03  Time: 10:04 AM  Op: ANG
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: JEFFERSON        City: GOLDEN
Addr:   1950  Street: MOUNT ZION DR
Grids: 03S070W33S*               :              :               Legal: C
Type of Work: NEW H20 & SEW SERV                         Exp.: N  Boring: N
Location: LOC ENTIRE LOT CONT XING ST  **ACCESS OPEN**  *PAINT/FLAG/WHISKER*
        : *REQ1 PER CUST*
Company : RENAUD EXCAVATING
Caller  : KATHI PAINE                Phone: (303)761-5822
Alt Cont: VALERIE RENAUD             Phone: (303)761-5822
Fax: (303)761-3396  Email: RENEXC@AOL.COM
Done for: ALPINE CRAFTSMAN
Remarks:

Members :CDOT06 :CMSDW00       :CMSND08       :COSMINE1      :PCND00 :PSND08
Members :QLNCND00      :QLNCND08


CMSDN00 00099 UNCC 11/12/03 10:07 AM 0654894-000 NORM NEW STRT LREQ

Ticket Nbr: 0654894
Original Call Date: 11/12/03  Time: 10:06 AM  Op: DEL
Contr Excav Date  : 11/17/03  Time: 07:00 AM  Extended job: N
Locate By Date    : 11/14/03  Time: 11:59 PM  Hrs Notice: 117  Meet: N
State: CO     County: JEFFERSON        City: ARVADA
Addr:  12630  Street: W 67TH PL
Near Intersection(s): XENON DR
Grids: 03S069W05SW               :              :               Legal: C
Type of Work: DROP BURY 1                                Exp.: N  Boring: N
Location: LOC ENTIRE LOT TO INCLUDE FROM HOUSE TO PED **ACCESS OPEN**
        : *PAINT/FLAG/LV A SKETCH*
Company : 180 CONNECT
Caller  : ED VARGAS                  Phone: (303)591-8840
Fax: (303)291-1253  Email:
Done for: COMCAST BROADBAND
Remarks:

Members :CDOT06 :CMSDN00       :CMSND10       :PCQ00  :PSND10 :QLNCND00
Members :QLNCND10


