UTI50  00001A USAS 09/13/02 05:05:35 A0057805-00A INSUFFICIENT NOTICE GRID

Ticket : A0057805 Date: 09/12/02 Time: 20:37 Oper: WBP812918149 Chan: WEB
Old Tkt: A0057805 Date: 09/12/02 Time: 20:46 Oper: WBP812918149 Revision: 00A
State: CA County: VENTURA              Place: NEWBURY PARK
Locat: LOCATE ALL LINES FROM OUSE TO CURB FROM PROP LINE TO PROP LINE
Address: 331 HUNTERS PT
X/ST   : ROLLING OAKS
Grids: 0556F02                                              Delineated: N

Work : SEWER REPAIR
Work date: 09/16/02 Time: 08:30 Hrs notc: 084 Work hrs: 025 Priority: 1
Instruct : LOCATE ALL LINES TO CURB   Permit   : N.A.
Done for : HOME OWNER

Company: WILLIAM BROTHERS PLUMBING      Caller: BOBBY
Address: 3541 OLD CONEJO RD
City&St: NEWBURY PARK, CA               Zip: 91320
Phone: 805-499-9335
Formn: FRED                 Phone: 805-499-9335
Mbrs : CAW03  CHRCMAL       CMW52  SCG4UO UFTV02 USCE16 UTI50  UVZCAM UVZMV2
VCF01  VZCAM  VZMDV2

UTI50  00002A USAS 09/13/02 05:05:43 A0057806-00A  GRID

Ticket : A0057806 Date: 09/12/02 Time: 20:49 Oper: WBP812918149 Chan: WEB
Old Tkt: A0057806 Date: 09/12/02 Time: 20:52 Oper: WBP812918149 Revision: 00A
State: CA County: VENTURA              Place: NEWBURY PARK
Locat: LOCATE ALL LINES FROM SIDE OF HOUSE TO CURB FROM PROP LINE TO PROP LINE
Address: 1545 EL MONTE
X/ST   : EL DORADO
Grids: 0526H06                                              Delineated: N

Work : SEWER REPAIR
Work date: 09/17/02 Time: 08:30 Hrs notc: 108 Work hrs: 049 Priority: 2
Instruct : LOCATE ALL LINES TO CURB   Permit   : N.A.
Done for : HOME OWNER

Company: WILLIAM BROTHERS PLUMBING      Caller: BOBBY
Address: 3541 OLD CONEJO RD
City&St: NEWBURY PARK, CA               Zip: 91320
Phone: 805-499-9335
Formn: FRED                 Phone: 805-499-9335
Mbrs : CAW03  SCG4UO USCE16 UTHO41 UTI50  UVZCAM UVZMV2 VCF01  VZCAM  VZMDV2

UTI50  00003A USAS 09/13/02 06:38:22 A0000192-04A  RXMIT GRID

Ticket : A0000192 Date: 09/13/02 Time: 06:07 Oper: LYD Chan: 100
Old Tkt: A0000192 Date: 07/19/02 Time: 09:01 Oper: MLS Revision: 04A
State: CA County: SANTA BARBARA        Place: SANTA MARIA
Locat: 600 BLOCK OF BLOSSER RD   FRM BOONE ST S/TO SMV RAILROAD TRACKS & BTWN
     : RUSSELL AV & BLOSSER RD  (NOTE WILL BE CROSSING BLOSSER RD)
Address: BLOSSER RD
X/ST   : BOONE ST
Grids: 0796F0113                                            Delineated: Y

Work : GRADE UNDERGROUND CONST
Work date: 09/13/02 Time: 06:07 Hrs notc: 096 Work hrs: 048 Priority: 2
Instruct : WORK CONTINUING        Permit   : NOT REQ
Done for : PEOPLES SELF HELP HOUSING

Company: MJ ROSS CONSTRUCTION           Caller: STAN HACHAN
Address: 100 5 CITIES DR
City&St: PISMO BEACH, CA                Zip: 93449      Fax: 805-773-8006
Phone: 805-773-8007 Ext:      Call back: 7AM-5PM
Formn: RANDY MANKINS        Phone: 805-431-0432

                                  REMARKS

**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[BES 08/02/02 06:40]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[PEG 08/16/02 06:05]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LMT 08/30/02 06:03]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LYD 09/13/02 06:07]

Mbrs : ATTATL CMC09  CSM01  PGE01  SCG4UK UTI50  UVZSTABAR     VZSTABAR

UTI50  00004A USAS 09/13/02 06:38:33 A0815126-11A  RXMIT GRID

Ticket : A0815126 Date: 09/13/02 Time: 06:07 Oper: LYD Chan: 100
Old Tkt: A0815126 Date: 04/17/02 Time: 13:09 Oper: CRH Revision: 11A
State: CA County: SANTA BARBARA        Place: GUADALUPE
Locat: FROM N/END OF CALLE CESAR CHAVEZ DR CONT N/FOR APPROX 800FT,  W/FOR
     : APPROX 75FT,  E/FOR APPROX 420FT;    FROM W/END OF SNOWY PLOVER LN CONT
     : W/FOR APPROX 800FT  *12 ACRE SITE*
Address: CALLE CESAR CHAVEZ DR
X/ST   : SNOWY PLOVER LN
Grids: 0774H0624                                            Delineated: Y

Work : INSTALL UNDERGROUND UTILITIES & GRADE
Work date: 09/13/02 Time: 06:08 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : WORK CONTINUING        Permit   : NOT REQ
Done for : PEOPLES SELF HELP HOUSING CORP

Company: MJ ROSS CONSTRUCTION           Caller: STAN HACHAN
Address: 100 5 CITIES DR
City&St: PISMO BEACH, CA                Zip: 93449      Fax: 805-773-8006
Phone: 805-773-8007 Ext:      Call back: 7AM-5PM
Formn: RANDY M              Phone: 805-431-0432

                                  REMARKS

**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LYD 05/01/02 06:43]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[MLS 05/15/02 07:01]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[MLS 05/29/02 06:13]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[PEG 06/12/02 06:23]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[AMY 06/26/02 06:08]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[AMY 07/10/02 06:10]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LYD 07/24/02 06:12]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[KDB 08/07/02 06:54]
**RESEND**UPDATE ONLY WORK-CONT  PER BRIAN CLARK--[MLS 08/21/02 07:32]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LYD 09/03/02 06:38]
**RESEND**UPDATE ONLY WORK-CONT  PER STAN--[LYD 09/13/02 06:08]

Mbrs : CHRCMSLO      PGE01  SCG4UK UNI03  UTI50  UVZSTABAR     VZSTABAR

UTI50  00005A USAS 09/13/02 06:59:17 A0044146-01A  RXMIT GRID

Ticket : A0044146 Date: 09/13/02 Time: 06:57 Oper: LMT Chan: 100
Old Tkt: A0044146 Date: 08/30/02 Time: 11:23 Oper: TEL Revision: 01A
State: CA County: VENTURA              Place: OXNARD/ CO/AREA OXNARD
Locat: BOTH SIDES/OF WOOLEY RD STARTING APPROX 800FT W/OF VICTORIA AV & GOING
     : W/TO EDISON CANAL
Address: WOOLEY RD
X/ST   : VICTORIA AVE
Grids: 0522A0734    0522B073                                Delineated: Y

Work : INSTALL DRY UTILITIES
Work date: 09/13/02 Time: 06:57 Hrs notc: 120 Work hrs: 048 Priority: 2
Instruct : WORK CONTINUING        Permit   : NOT AVAIL
Done for : SUNCAL CO

Company: SAM HILL & SONS                Caller: JAMIE
Address: P.O. BOX 5670
City&St: VENTURA, CA                    Zip: 93005      Fax: 805-644-2813
Phone: 805-644-6278 Ext:      Call back: 6AM-5:00PM
Formn: BRENT FRANKLIN

                                  REMARKS

**RESEND**UPDATE ONLY WORK-CONT  PER CARL--[LMT 09/13/02 06:57]

Mbrs : JON51  OXN01  OXN02  OXN03  SCG4UR USCE39 UTI50  UVZCAM UVZMV2 VZCAM
VZMDV2

