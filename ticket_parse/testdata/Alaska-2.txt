ALASKA DIGLINE INC.
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD

Ticket No:    2005180464 Seq. No: 31

Update of:              
Original Call Date:     04/27/2005     Time:      12:02:26 PM  OP: 138
Transmit Date:          04/27/2005     Time:      12:07:56 PM
Locate By Date:         04/29/2005     Time:      12:02:00 PM

Company:           LEE'S CUSTOM DESIGNS                    
Contact Name:      CINDY LINSCOTT             Contact Phone:   (907)694-8565

Alternate Contact: LEE RAYMOND                Alternate Phone: (907)244-6761

                                              Fax No:          (907)694-6363

                                              Pager No:

                                              Cellular No:

                                              WO/JOB #:        

Locate City/Area: EAGLE RIVER
Subdivision: 
Address:    11517  Unit:         , OLD GLENN HWY                           
To Address:       
Nearest Intersecting Street: CORONADO ST                             
2nd Intersecting Street:                                             
MAP PAGE: NW0151

Dig Info: PUTTING UP CACHE.   LOC  30' R AROUND THE COFFEE  
          SHOP SIGN @ FRONT OF PROPERTY.                    




Remarks:                                                   



Type of Work: SONO TUBES                                   
Dig deeper than 4 ft.: N   10 ft.: N   Easement: Y
Road R/O/W: Y   Railroad R/O/W: N   Front lot: Y   Back lot: N
Contact caller to arrange site meeting: N

Billing Info: LEE RAYMOND                                       
Member Page: NW0151
Sending to: (listing of utilities tkt sent to)
*MEA/GCI         AWWU             DOTSL            ENS A/E          PW TRFSG
PW SDSLTW        PW SD/ER         MTA ER/MV        MEA ER           GCI ER



ALASKA DIGLINE INC.
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD

Ticket No:    2005180463 Seq. No: 30

Update of:              
Original Call Date:     04/27/2005     Time:      11:58:45 AM  OP: 138
Transmit Date:          04/27/2005     Time:      12:02:19 PM
Locate By Date:         04/29/2005     Time:      11:58:00 AM

Company:           LEE'S CUSTOM DESIGNS                    
Contact Name:      CINDY LINSCOTT             Contact Phone:   (907)694-8565

Alternate Contact: LEE RAYMOND                Alternate Phone: (907)244-6761

                                              Fax No:          (907)694-6363

                                              Pager No:

                                              Cellular No:

                                              WO/JOB #:        

Locate City/Area: EAGLE RIVER
Subdivision: 
Address:    23307  Unit:         , EAGLE RIVER RD                          
To Address:       
Nearest Intersecting Street: HARMANY RANCH                           
2nd Intersecting Street:                                             
MAP PAGE: SW0160

Dig Info: LOC 50' OUT FROM GARAGE DOORS.                    





Remarks:                                                   



Type of Work: ADDITION TO BLDG                             
Dig deeper than 4 ft.: N   10 ft.: N   Easement: N
Road R/O/W: N   Railroad R/O/W: N   Front lot: N   Back lot: Y
Contact caller to arrange site meeting: N

Billing Info: LARRY MORAN                                       
Member Page: SW0160
Sending to: (listing of utilities tkt sent to)
*MEA/GCI         AWWU             ENS A/E          PW SD/ER         MTA
ER/MV
MEA ER           GCI ER           
ALASKA DIGLINE INC.
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD

Ticket No:    2005190433 Seq. No: 55

Update of:              
Original Call Date:     05/03/2005     Time:      12:26:45 PM  OP: 104
Transmit Date:          05/03/2005     Time:      12:27:40 PM
Locate By Date:         05/05/2005     Time:      12:26:00 PM

Company:           REDI ELECTRIC                           
Contact Name:      BUTCH SCOTT                Contact Phone:   (907)746-5328 
Alternate Contact:                            Alternate Phone:                  
                                              Fax No:          (907)746-5329 
                                              Pager No:                         
                                              Cellular No:     (907)354-8902 
                                              WO/JOB #:        31149/2

Locate City/Area: PALMER
Subdivision: GREATLAND TERRACE // LOT 5 BLK 7
Address:           Unit:         , S FELTON ST                             
To Address:       
Nearest Intersecting Street:                                         
2nd Intersecting Street:                                             
MAP PAGE: NE3915

Dig Info: BURY CORFLO FROM PED TO METER ON HOUSE            
          POLE # WA7-4                                      




Remarks:                                                   



Type of Work: ELECTRIC, U/G                                
Dig deeper than 4 ft.: N   10 ft.: N   Easement: Y
Road R/O/W: N   Railroad R/O/W: N   Front lot: N   Back lot: N
Contact caller to arrange site meeting: N

Billing Info: L&H CONSTRUCTION                                  
Member Page: NE3915
Sending to: (listing of utilities tkt sent to)
*MEA/GCI         ENS MV           MTA ER/MV        GCI MV           MEA MV
ARRC             GCI - FIBER      

