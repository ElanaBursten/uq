
CHACRE 00002 USAN 07/17/06 12:21:01 0252769 NORMAL NOTICE

Message Number: 0252769 Received by USAN at 12:08 on 07/17/06 by MLI

Work Begins:    07/19/06 at 12:30   Notice: 020 hrs      Priority: 2

Expires: 08/14/06 at 17:00   Update By: 08/10/06 at 16:59

Caller:         LOREN STONEBRINK
Company:        WARREN STONEBRINK CONSTRUCTION
Address:        516 HASSETT ST
City:           BROOKINGS                     State: OR Zip: 97415
Business Tel:   541-469-6341                  Fax: 541-469-6341
Email Address:  STONEBRINK@CHARTER.NET

Nature of Work: TR FOR GAS L
Done for:       P/O ZELLMER                   Explosives: N
Foreman:        CALLER                   
Area Premarked: Y   Premark Method: WHITE PAINT
Permit Type:    COUNTY                        Number: UNK
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: Y
Vac / Pwr Equip Contact: LOREN STONEBRINK
Contact Tel:    541-469-6341          Contact Fax: 541-469-6341
Contact Email Address:  STONEBRINK@CHARTER.NET
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         205  BOW LN
  Cross Street:         ARROWHEAD RD
    WRK IN THE BK/COR/O THE HOUSE

Place: CRESCENT CITY, CO AREA       County: DEL NORTE            State: CA

Long/Lat Long: -124.144290 Lat:  41.821731 Long: -124.141260 Lat:  41.827319 

Sent to:
CHACRE = CHARTER COMM-CRES CTY        CTYCRE = CITY CRESCENT CITY           
PPLCRE = PAC PWR & LIGHT CRE          VERWCO = VERIZON WEST COAST           


CHACRE 00001 USAN 07/17/06 10:10:54 0252285 NORMAL NOTICE
 
Message Number: 0252285 Received by USAN at 09:54 on 07/17/06 by BLM
 
Work Begins:    07/19/06 at 10:15   Notice: 020 hrs      Priority: 2
 
Expires: 08/14/06 at 17:00   Update By: 08/10/06 at 16:59
 
Caller:         NICK PHILLIPS
Company:        NORTH COAST MOWING & LANDSCAPING
Address:        9825 HWY 198 SP 65
City:           GASQUET                       State: CA Zip: 95543
Business Tel:   707-457-3038                  Fax: 
Email Address:  
 
Nature of Work: INST SPRINKLER SYSTEM & GRASS
Done for:       P/O                           Explosives: N
Foreman:        CALLER                   
Area Premarked: Y   Premark Method: WHITE PAINT
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         4020  WONDERSTUMP RD
  Cross Street:         ELK VALLEY CROSS RD
    ENT PROP
 
Place: CRESCENT CITY, CO AREA       County: DEL NORTE            State: CA
 
Long/Lat Long: -124.162430 Lat:  41.805180 Long: -124.156174 Lat:  41.813904
 
Sent to:
BSTARG = BLUE STAR GAS                CHACRE = CHARTER COMM-CRES CTY        
CODELN = COUNTY DEL NORTE             CTYCRE = CITY CRESCENT CITY           
PPLCRE = PAC PWR & LIGHT CRE          VERWCO = VERIZON WEST COAST           
 


