]
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 01034682
Transmit      Date: 01/24/03      Time: 02:00    Op: mnvond
Original Call Date: 01/24/03      Time: 01:50    Op: mnvond
Work to Begin Date: 01/24/03      Time: 02AM

Place: ROCKVILLE
Address:             Street: WOODSTON ROAD
Nearest Intersecting Street: LONGWOOD DR

Type of Work: EMERGENCY REPAIR OF WATER MAIN
Extent of Work:   MARK 100FT RADIUS OF INTERSECTION WOODSTON RD AND
: LONGWOOD DR.
Remarks: EMERGENCY ** CREW ON SITE
:                                                        Fax: (240)314-8589

Company     : CITY OF ROCKVILLE
Contact Name: DUANE PICKETT         Contact Phone: (240)314-8565
Alt. Contact: TONIGHT               Alt. Phone   : (240)876-7827
Work Being Done For: CITY OF ROCKVILLE
State: MD              County: MONT
Map: MONT  Page: 029   Grid Cells: D05
Explosives: N
PEP05
Send To: TMT   02  Seq No: 0011   Map Ref:
         TRU   02          0010
         WGL   06          0027



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01018730
Transmit      Date: 01/14/03      Time: 09:07    Op: l.boot
Original Call Date: 01/14/03      Time: 09:05    Op: l.boot
Work to Begin Date: 01/16/03      Time: 09AM

Place: GAITHERSBURG
Address: 409         Street: PLACID MEWS
Nearest Intersecting Street: PLACID ST

Type of Work: INST GAS MAIN & SVC
Extent of Work:   LOC ENTIRE PROP TO ALL OPPOSITE CURBS PLUS 50FT PAST ALL
: OPPOSITE CURBS TO UNCUDE ALL ALLEY WAYS
Remarks: MARK GAS A & B-FLAGS & PAINT ARE REQUESTED
:                                                        Fax: (301)330-5750

Company     : LINEAL INDUSTRY((XLIN))
Contact Name: SDY CAVISNESS         Contact Phone: (301)330-9077
Alt. Contact: BOB CASAMESE          Alt. Phone   : (301)330-9077
Work Being Done For: WASHINGTON GAS
State: MD              County: MONT
Map: MONT  Page: 018   Grid Cells: K13
Explosives: N
PEP05      XLIN
Send To: TMT   01  Seq No: 0064   Map Ref:
         TRU   01          0061
         WGL   06          0218
         WSS   01          0163


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01018735
Transmit      Date: 01/14/03      Time: 09:07    Op: l.boot
Original Call Date: 01/14/03      Time: 09:05    Op: l.boot
Work to Begin Date: 01/16/03      Time: 09AM

Place: GAITHERSBURG
Address: 449         Street: LYNETTE ST
Nearest Intersecting Street: MARKET ST WEST

Type of Work: INST GAS MAIN & SVC
Extent of Work:   LOC ENTIRE PROP TO ALL OPPOSITE CURBS PLUS 50FT PAST ALL
: OPPOSITE CURBS TO UNCUDE ALL ALLEY WAYS
Remarks: MARK GAS A & B-FLAGS & PAINT ARE REQUESTED
:                                                        Fax: (301)330-5750

Company     : LINEAL INDUSTRY((XLIN))
Contact Name: SDY CAVISNESS         Contact Phone: (301)330-9077
Alt. Contact: BOB CASAMESE          Alt. Phone   : (301)330-9077
Work Being Done For: WASHINGTON GAS
State: MD              County: MONT
Map: MONT  Page: 018   Grid Cells: K13
Explosives: N
PEP05      XLIN
Send To: TMT   01  Seq No: 0065   Map Ref:
         TRU   01          0062
         WGL   06          0219
         WSS   01          0164




