
 sent     01/02/03  08:02 AM sequence 0001 to OUPS station # 2972 for AGP

 *** ON THE JOB ***
 ticket   0102-028-010-00   xref-0020165           
 taken    01/02/03   7:57 AM
 county   OH WASHINGTON
 place    MARIETTA           class R
 locn     407 to 411 "5TH ST"
 where    LOCATE: IN THE  STREET
          NEAR ST: TUPPER ---- 10 FT  FROM

 begin    01/02/03   8:05 AM
 work     EMERGENCY  ---  SAN SEWER  REPAIR
 contact  STEVE ELLIOTT for CITY OF MARIETTA
          ph 740-373-3858   fax ph 740-373-8214   type C
 within   n 39 26.0    s 39 24.5    w 81 28.0    e 81 26.0    


 eom





 sent     01/02/03  12:19 PM sequence 0003 to OUPS station # 2972 for AGP
 ticket   0102-020-036-00   xref-0021342           
 taken    01/02/03  11:57 AM
 county   OH WASHINGTON
 place    MUSKINGUM TWP      class R
 locn     116 "LINCOLN DR"
 where    LOCATE: REAR AND BOTH SIDES OF THE PROPERTY - NEED THE REAR OF
          THE PROPERTY INCLUDING UP TO 10 FT ALONG B/S OF THE HOUSE
          NEAR ST: SNEARLY DR - REAR OF THE PROPERTY IS ALONG S R 60

 begin    01/06/03  12:15 PM
 work     FENCE REPAIR
 contact  MRS BRUCE BIRMINGHAM for SELF
          ph 740-373-3213   fax ph    type H
 within   n 39 28.5    s 39 27.5    w 81 28.5    e 81 27.5    

 eom




T

 sent     01/02/03  02:23 PM sequence 0004 to OUPS station # 2972 for AGP
 ticket   0102-002-020-00   xref-0021726           
 taken    01/02/03   2:03 PM
 county   OH WASHINGTON
 place    MARIETTA           class R
 locn     102 "STONE CREEK DR" at ~200 FT E of "COLEGATE WOODS DR"
 where    LOCATE: FRONT AND BOTH SIDES OF STREET
          STONE CREEK DR AKA STONE CREST

 begin    01/06/03   2:15 PM
 work     NEW GAS TAP
 contact  TIM BAUER for DOMINION EAST OHIO
          ph 740-374-4345   fax ph    type M
 within   n 39 27.0    s 39 26.5    w 81 27.0    e 81 26.5    

 eom





 sent     01/02/03  03:42 PM sequence 0005 to OUPS station # 2972 for AGP
 ticket   0102-003-117-00   xref-0022073           
 taken    01/02/03   3:24 PM
 county   OH WASHINGTON
 place    WESLEY TWP         class R
 locn     ~.6 MI S, 330 FT W of "TWP RD 206" and "S R 676"
 where    LOCATE: WEST SIDE OF THE ROAD -- GOING UP TO 330 FT W OFF OF THE
          ROAD

 begin    01/06/03   3:45 PM
 work     SETTING A NEW POLE AND ANCHOR
 contact  DAVID HACK for WASHINGTON ELECTRIC
          ph 740-373-2141   fax ph 740-373-2941   type M
 within   n 39 29.5    s 39 28.5    w 81 47.5    e 81 45.5    

 eom





 sent     01/09/03  11:08 AM sequence 0005 to OUPS station # 2972 for AGP
 ticket   0109-070-016-00   xref-0091013           
 taken    01/09/03  10:56 AM
 county   OH ATHENS
 place    ROME TWP           class R
 locn     8642 "S R 144" at ~.5 MI S, 400 YD NW of "S R 329"
 where    LOCATE: ** FEDERAL HOCKING HIGH SCHOOL
          REAR OF PROPERTY N/W OF THE EXISTING FOOTBALL FIELD -- WORKING UP
          TO 400 YARDS N/W OFF ROAD

 begin    01/13/03  11:00 AM
 work     GRADING FOR FIELD EXCAVATION
 contact  LEROY GUESS for FEDERAL HOCKING LOCAL SCHOOLS
          ph 740-662-6691 X111   fax ph    type C
 note     CELL PHONE: 740-517-2662
          * GTO WAS MANUALLY ADDED TO THIS TICKET, PLEASE CHECK YOUR
          DATABASE
 within   n 39 19.0    s 39 17.5    w 81 54.5    e 81 53.0    

 eom





 sent     01/08/03  09:55 AM sequence 0004 to OUPS station # 2972 for AGP
 ticket   0108-028-032-00   xref-0080646              <<<design message>>>
 taken    01/08/03   9:35 AM
 county   OH NOBLE
 place    JACKSON TWP        class R
 locn     ~1000 FT S, 300 FT E of "S R 339" and "TWP RD 72"
 where    EAST  SIDE OF  S R  339 --  300 FT EAST BACK OFF ROAD

 work     designing stages for: DESIGN STAGES FOR: SPRING  DEVELOPMENT
 contact  HARRY for NOBLE SOIL & WATER CONSERVATION
          ph 740-732-4318   fax ph 740-732-7141   type C
 note     PRE PLANNING INFORMATION IS DESIRED --- MAILING ADDRESS:
          NOBLE SOIL & WATER CONSERVATION
          18506 STATE RTE 78 E               
          CALDWELL
          OH 43724
          REQUESTING TELEPHONE RESPONSE AT THIS TIME
 within   n 39 37.0    s 39 36.5    w 81 35.0    e 81 34.0    

 mailing  pre planning information is desired -- please send drawings
          to:       NOBLE SOIL & WATER CONSERVATION
                    18506 STATE RTE 78 E
                    CALDWELL, OH 43724

<<<design message>>>
 members  AGP=ALLEGHENY POWER  BLK=BELDEN & BLAKE   GTO=VERIZON  - COLU 
          TCY=TRI CTY WAT/SEW  WAE=WASHINGTON ELEC 
 lbp's    GPC=GREENLAND PETRO  PWC=PURE WATER CO   
 eom






