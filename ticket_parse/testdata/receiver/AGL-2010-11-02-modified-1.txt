Return-Path: <SentriNotifications@aglresources.com>
X-Original-To: emailtkts.fau5@tickets.utiliquest.com
Delivered-To: emailtkts.fau5@tickets.utiliquest.com
Received: from obr.aglresources.com (obr.aglresources.com [65.243.68.157])
        by ticketssmtp1.utiliquest.com (Postfix) with ESMTP id 320FAA05A8
        for <emailtkts.fau5@tickets.utiliquest.com>; Tue,  2 Nov 2010 15:12:46 -0400 (EDT)
Received: from GAATLP514W ([10.2.55.224]) by obr.aglresources.com with Microsoft SMTPSVC(6.0.3790.4675);
         Tue, 2 Nov 2010 15:09:15 -0400
MIME-Version: 1.0
From: SentriNotifications@aglresources.com
Sender: SentriNotifications@aglresources.com
To: emailtkts.fau5@tickets.utiliquest.com
Date: 2 Nov 2010 15:12:45 -0400
Subject: Ticket Notification Mail - Damage Notice
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: base64
Message-ID: <GAAPPP04WOSdAwq8MZU0002ad30@obr.aglresources.com>
X-OriginalArrivalTime: 02 Nov 2010 19:09:15.0501 (UTC) FILETIME=[711A31D0:01CB7AC1]

AGL112  01751 GAUPC 11/01/10 14:04:14 11010-221-026-000 DAMAGE
Underground Notification             
Notice : 11010-221-888 Date: 11/01/10  Time: 14:00  Revision: 000 

State : GA County: PAULDING      Place: HIRAM                                   
Addr  : From:        To:        Name:    BILL CARRUTH                   BLVD    
Near  : Name:    US 278                             

Subdivision:                                         
Locate: LOCATE R/O/W ON BOTH SIDES OF THE ROAD FOR THE FOLLOWING ROADS:  BILL C
      :  ARRUTH BLVD -- FROM NORFOLK SOUTHERN RXR BRIDGE TO US 278/ SR 6 /// BER
      :  KLEIGH TRAILS DRIVE � FROM BILL CARRUTH BLVD TO 100 FEET WEST /// THOMP
      :  SON ROAD � FROM BILL CARRUTH BLVD TO 250 FEET EAST /// GREYSTONE PARKWA
      :  Y � FROM BILL CARRUTH BLVD TO 200 FEET EAST /// DARBY�S RUN DRIVE � FRO
      :  M BILL CARRUTH TO 100 FEET WEST                                        

Grids       : 3352A8446A 3352A8446B 3352A8447D 3353A8446A 3353A8446B 
Grids       : 3353B8446A 3353B8446B 3353C8446A 3353C8446B 3353C8447D 
Grids       : 3353D8446A 3353D8446B 3353D8447D 3354D8446A 3354D8446B 
Grids       : 
Work type   : ROADWAY WIDENING AND  RECONSTRUCTION                                

Start date: 11/01/10 Time: 00:00 Hrs notc : 000
Legal day : 11/01/10 Time: 00:00 Good thru: 11/01/10 Restake by: 11/01/10
RespondBy : 11/01/10 Time: 00:00 Duration : 18 MONTHS  Priority: 6
Done for  : PAULDING COUNTY DOT                     
Crew on Site: Y White-lined: Y Blasting: Y  Boring: N

Remarks : -- WILL BLAST DAMAGE TO TICKET  05190-400-036 , DAMAGE WITH PHONE SE
        : RVICE LINE WITH AT&T, DAMAGE IS LOCATED AT THE JOB TRAILER AT THE DO
        : T PROJECT WHICH IS LOCATED IN FRONT OF THE TRAILER AND NEAR INTERSEC
        : TION OF US 278 AND BILL CARRUTH, DAMAGE OCCURED WHEN DUMB TRUCK HIT 
        : OVERHEAD LINE AND IT PULLED IT DOWN SERVICE/DROP LINE DAMAGE TO ATT 
        : / D **UNKNOWN/OTHER** **EXTENT: CUT IN HALF**                       
        : DAMAGE TO SERVICE/DROP BSNW EXTENT: CUT IN HALF **SERVICE OUT

Company : AIKEN GRADING                             Type: CONT                
Co addr : PO BOX 106                               
City    : DALLAS                          State   : GA Zip: 30132              
Caller  : TRACY BONDS                     Phone   :  770-445-3224              
Fax     :                                 Alt. Ph.:  770-445-3224              
Email   : TBONDS@AIKENGRADING.COM                                             
Contact : MIKE FLANIGAN                   404-790-3682              

Submitted date: 11/01/10  Time: 14:00  Oper: 227
Mbrs : AGL112 BSNW COMNOR GAUPC GRS70 MCI02 PARKF PLD01 
-------------------------------------------------------------------------------