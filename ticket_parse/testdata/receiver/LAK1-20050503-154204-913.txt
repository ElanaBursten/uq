Return-Path: <lcc@akonecall.com>
Delivered-To: emailtkts.lak1@tickets.utiliquest.com
Received: (qmail 4504 invoked from network); 3 May 2005 19:12:40 -0000
Received: from mmp-2.gci.net (208.138.130.81)
  by tickets.utiliquest.com with SMTP; 3 May 2005 19:12:40 -0000
Received: from Lucy (15-0-178-69.gci.net [69.178.0.15])
 by mmp-2.gci.net (iPlanet Messaging Server 5.2 HotFix 1.14 (built Mar 18
 2003)) with SMTP id <0IFX0007HIIRUI@mmp-2.gci.net> for
 emailtkts.lak1@tickets.utiliquest.com; Tue, 03 May 2005 11:37:45 -0800 (AKDT)
Date: Tue, 03 May 2005 11:37:37 -0800
From: lcc@akonecall.com
Subject: Request 2005190401
To: emailtkts.lak1@tickets.utiliquest.com
Message-id: <0IFX0007IIISUI@mmp-2.gci.net>
MIME-version: 1.0
X-Mailer: CSMTPConnection v1.3
Content-type: multipart/mixed; boundary="Boundary_(ID_2NoDOk3hBTI0RJAXbEILiQ)"


--Boundary_(ID_2NoDOk3hBTI0RJAXbEILiQ)
Content-type: text/plain; charset=us-ascii
Content-transfer-encoding: 7BIT

ALASKA DIGLINE INC.
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD

Ticket No:    2005190401 Seq. No: 38

Update of:
Original Call Date:     05/03/2005     Time:      11:30:09 AM  OP: 104
Transmit Date:          05/03/2005     Time:      11:32:37 AM
Locate By Date:         05/05/2005     Time:      11:30:00 AM

Company:           MTA - CONSTRUCTION
Contact Name:      JIM JANSON                 Contact Phone:   (907)689-2521
Alternate Contact:                            Alternate Phone:
                                              Fax No:
                                              Pager No:
                                              Cellular No:     (907)355-1531
                                              WO/JOB #:

Locate City/Area: WASILLA
Subdivision: CHRIS // LOT 4
Address:           Unit:         , ADELE CIRCLE
To Address:
Nearest Intersecting Street:
2nd Intersecting Street:
MAP PAGE: NW3763

Dig Info: FROM MTA PED TO NEW BLDG ON LOT 4
          MTA PED # ZE0049.  POLE # WL121-4
          TIE IN TO NEW CRITERION GENERAL INC.



Remarks: SCHEDULED TO DIG 5/5/05



Type of Work: TELEPHONE, U/G
Dig deeper than 4 ft.: Y   10 ft.: N   Easement: Y
Road R/O/W: Y   Railroad R/O/W: N   Front lot: Y   Back lot: N
Contact caller to arrange site meeting: N

Billing Info:
Member Page: NW3763
Sending to: (listing of utilities tkt sent to)
*MEA/GCI         ENS MV           MTA ER/MV        GCI MV           MEA MV
CTYWASILLA




--Boundary_(ID_2NoDOk3hBTI0RJAXbEILiQ)
Content-type: application/octet-stream; name=2005190401.PCX
Content-transfer-encoding: base64
Content-disposition: attachment; filename=2005190401.PCX

CgUBAQAAAABRA0sESABIAAAAAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAABbAABAIAC4AEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAD//+z/AP//7P8A///s/wD//+z/AP//7P8A///s/wD//+z/AP//7P8A///s
/wD//+z/AOb/weB+D8HAwfwHwfPB+D+Dwf8/B8H89/8A5v/BwDwHgHgHwfPB8B8Bwf4+A8H89/8A
5v+GOMHnjHjB/8HjweGOOcH+PnHB+Pf/AOb/jxnB8554wf+DwePBznjB/DzB+cHg9/8A5v/B3xnB
8555wf8TwefBxnzB+DzB+cHE9/8A5/8ZwfM/OR8zwefBxHzB+TzB+MHM9/8A5/85wfM/MA/B88Hn
wcR8wfE8wfjB/Pf/AOb/wf45wfM/MAfB88HjwcR8weM8wfjB/Pf/AOb/wfx5wfM/O8HnwfPB4YR8
wec8wfjB/Pf/AOb/wfjB+cHzwj/B48HzwfAEfMHHPMH4wfz3/wDm/8HxwfnB88I/wePB88H8RHzB
wAzB+MH89/8A5v/Bw8H5wfMeP8HjwfPB/8HGfMHADMH5wfz3/wDm/8HHwfnB855zwefB88Hjwc58
wcAMwfnB/Pf/AOb/j8H4weOMccHHwfPB8Y44wf88ccH89/8A5v+AHAeAeAfB88HwHgHB/z4Dwfz3
/wDm/wAeD8HAwfwPwfPB+D8Dwf8/B8H89/8A///s/wD//+z/AP//6/9/AP//7P8A///s/wD//+z/
AP//7P8A///s/wD//+z/AP//7P8A///s/wD//+z/AP//3//B78z/AP//3//B78z/AP//3//B78z/
AP//3//B78z/AP//3//B78z/AP//3//B78z/AP//3//B78z/AP//7P8A///s/wD//9//we/M/wD/
/9//we/M/wD//9//we/M/wD//9//we/M/wD//9//we/M/wD//9//we/M/wD//9//we/M/wD//9r/
wf3R/wD//9r/wf3R/wD//9r/wf3R/wD//9r/wf3E/8HvzP8A///a/8H9xP/B78z/AP//2v/B/cT/
we/M/wD//9r/wf3E/8HvzP8A///a/8H9xP/B78z/AP//2v/B/cT/we/M/wD//9r/wf3E/8HvzP8A
///a/8H90f8A///a/8H90f8A///a/8H90f8A///a/8H9xP/B78z/AP//2v/B/cT/we/M/wD//9r/
wf3E/8HvzP8A///a/8H9xP/B78z/AP//2v/B/cT/we/M/wD//9r/wf3E/8HvzP8A///a/8H9xP/B
78z/AP//2v/B/dH/AP//2v/B/dH/AP//2v/B/dH/AP//2v/B/dH/AP//2v/B/cT/we/M/wD//9r/
wf3R/wD//9r/wf3R/wD//9r/wf3R/wD//9r/wf3E/8H3zP8A///a/8H90f8A///a/8H90f8A///s
/wD//+z/AP//3//B98z/AP//3//B98z/AP//3//B98z/AP//3//B98z/AP//3//B98z/AP//3//B
98z/AP//3//B98z/AP//7P8A///s/wD//9r/wf3E/8H3zP8A///a/8H9xP/B98H/wfvK/wD//9//
wffB/8Hzyv8A///f/8H3wf/B88r/AP//3//B98H/wfvK/wD//9//wffB/8Hzyv8A///f/8H3wfwD
yv8A///s/wD//+D/wfwDyv8A///g/8H9y/8A///g/8H8y/8A///f/8H3wfzL/wD//9//wffB/IPK
/wD//9//wffB/CPK/wD//9//wffB/DPK/wD//9//wffB/Mv/AP//3//B98H+n8r/AP//3//B98H+
A8r/AP//4P/B/APK/wD//9r/wf7G/8H5yv8A///a/8H+xP/B98z/AP//3//B98H/wfnK/wD//9r/
wf7E/8H3wfwDyv8A///a/8H+xP/B98z/AP//2v/B/sT/wffM/wD//9//wffM/wD//9r/wf7E/8H3
zP8A///a/8H+xf/B/D/K/wD//9r/wf7F/8H+P8r/AP//2v/B/sb/h8r/AP//2v/B/sT/wffB/gfK
/wD//9r/wf7E/8H3wf57yv8A///a/8H+xP/B98H8wf3K/wD//9r/wf7E/8H3wf5zyv8A///a/8H+
xP/B98H/B8r/AP//2v/B/sT/wffM/wD//9r/wf7F/8H+A8r/AP//2v/B/sX/wfyfyv8A///a/8H+
xf/B/MH7yv8A///a/8H+xP/B98H+c8r/AP//2v/B/sT/wffB/3PK/wD//9r/wf7E/8H3zP8A///f
/8H3zP8A///f/8H3zP8A///a/8H+xP/B98z/AP//2v/B/sT/wffM/wD//9r/wf7R/wD//9r/wf7R
/wD//9r/wf7R/wD//9r/wf7R/wD//9r/wf7E/8H3zP8A///a/8H+xP/B98z/AP//2v/B/sT/wffM
/wD//9r/wf7E/8H3zP8A///a/8H+xP/B98z/AP//2v/B/sT/wffM/wD//9r/wf7R/wD//9r/wf7R
/wD//9r/wf7R/wD//9r/wf7E/8H3zP8A///a/8H+xP/B98z/AP//2v/B/sT/wffM/wD//9r/wf7E
/8H3zP8A///a/8H+xP/B98z/AP//2v/B/sT/wffM/wD//9r/wf7R/wD//9r/wf7R/wD//9r/wf7R
/wD//9r/wf7R/wD//9r/wf7R/wD//9r/wf7E/8H3zP8A///a/8H+xP/B98z/AP//3//B98z/AP//
3//B98z/AP//3//B98z/AP//7P8A///s/wD//+z/AP//7P8A///f/8H3zP8A///f/8H3zP8A///f
/8H3zP8A///f/8H3zP8A///s/wD//+z/AP//7P8A///s/wD//+z/AP//3//B98z/AP//3//B98z/
AP//3//B+8z/AP//3//B+8z/AP//3//B+8z/AP//3//B+8z/AP//3//B+8z/AP//7P8A///s/wD/
/+z/AP//3//B+8z/AP//3//B+8z/AP//3//B+8z/AP//2/9/w//B+8z/AP//2/9/w//B+8z/AP//
2/9/w//B+8z/AP//2/9/w//B+8z/AP//2/9/0P8A///b/3/Q/wD//9v/f9D/AP//2/9/0P8A///b
/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A
///b/3/Q/wD//9v/f9D/AP//2/9/0P8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7
zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7y//B/ADB8BAr
wv8ABW/B3///0v9/0P8A///b/3/Q/wD//9v/f9D/AP//2/9/0P8A///b/3/D/8H7zP8A///b/3/D
/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b/3/D/8H7zP8A///b
/3/Q/wD//9v/f9D/AP//7P8A///b/3/D/8H7zP8A///f/8H7zP8A///b/3/D/8H7zP8A///s/wD/
/9v/f9D/AP//2/9/w//B/cz/AP//2/9/w//B/cz/AP//2/9/0P8A///b/3/Q/wD//+z/ANL/wfv/
/9n/AP//2/9/w//B+8z/AP//3//B/cz/AP//3//B/cz/AP//3//B+cz/AP//2/9/0P8A0v/B+///
2f8A///s/wD//9v/f9D/AP//2/9/0P8A///s/wD//9//wfnM/wDS/8H7///Z/wD//9//wf3M/wD/
/9//wf3M/wD//+z/AP//7P8A///s/wDS/8H7///Z/wD//+z/AP//3//B/cz/AP//3//B/cz/AP//
3//B/cz/ANL/wfv//8z/wfnM/wD//9//wf3M/wDl/3/0/7/D/8H9zP8A0v/B+87/wf7CAAe////G
/wDS/8H7yv/B/gAQD8T/v///xv8A0v/B+8b/vgAej///z/8A0v/B+8L/wf4AP///x//B/cz/ANL/
wfrS/8Hf9P+/0P8A0v/B+9L/wd/0/7/D/8H9zP8A0v/B+9L/wd/0/7/D/8H9zP8A0v/B+9L/wd/4
/8H9zP8A0v/B+9L/wd/4/8H9zP8A0v/B+9L/wd/0/7/D/8H9zP8A0v/B+///yP+/0P8A0v/B+///
yP+/0P8A0v/B+9L/n/T/v9D/ANL/wfvS/8Hf+P/B/cz/ANL/wfPS/8Hf9P+/w//B/cz/ANL/wfP/
/8j/v8P/wf3M/wDS/8H70v/B3/T/v9D/ANL/wfvS/8Hf9P+/w//B/cz/ANL/wfvS/8Hf9P+/w//B
/cz/ANL/wfvS/8Hf9P+/0P8A0v/B+9L/wd/0/7/Q/wDS/8H70v/B3/T/v9D/ANL/wfvS/8Hf9P+/
0P8A0v/B+9L/wd/0/7/Q/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H7
0v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B
3/T/v9D/ANL/wfvS/8Hf9P+/0P8A0v/B+9L/wd/0/7/Q/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H7
0v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B
3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/v8P/wf3M/wDS/8H70v/B3/T/
v9D/ANL/wfvS/8Hf9P+/0P8A0v/B+9L/wd/0/7/Q/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3/j/
wf3M/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B
3/j/wf3M/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3///xv8A0v/B+9L/wd///8b/ANL/wfvS/8Hf
///G/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3/j/wf3M/wDS/8H70v/B3/j/wf3M/wDR/8H0A9L/
wd/4/8H9zP8A5f/B3/j/wf3M/wDl/8Hf+P/B/cz/AMP/wf4EA9//wd///8b/AMP/wf4EA9//wd//
/8b/AOX/wd///8b/AOX/wd///8b/AOX/wd/4/8H9zP8A5f/B3/j/wf3M/wDl/8Hf+P/B/cz/AOX/
wd/4/8H9zP8A5f/B3/j/wf3M/wDl/8Hf+P/B/cz/AOX/wd///8b/AOX/wd///8b/AOX/wd///8b/
AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B
/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hf
0P8A5f/B3/T/wd/Q/wDl/8Hf9P/B39D/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/
wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hfw//B/sz/
AOX/wd/0/8Hfw//B/sz/AOX/wd/0/8Hf0P8A5f/B3/T/wd/Q/wDl/8Hf9P/B39D/AOX/wd/0/8Hf
w//B/sz/AOX/wd/0/8Hfw//B/sz/AOX/wc/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0
/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hf0P8A5f/B7/T/
wd/Q/wDl/8Hv9P/B39D/AOX/we///8b/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/
we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hf0P8A5f/B
7/T/wd/Q/wDl/8Hv9P/B39D/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hf
w//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0
/8Hfw//B/sz/AOX/we/0/8Hf0P8A///b/8Hf0P8A5f/B7/T/wd/Q/wDl/8Hv9P/B38P/wf7K/8H+
wf8A5f/B7/T/wd/D/8H+yv/B/sH/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0
/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hfw//B/sz/AOX/we/0/8Hf0P8A5f/B7///
xv8A5f/B7///xv8A5f/B7/j/wf7M/wDl/8Hv+P/B/sz/AOX/we/4/8H+zP8A5f/B7/j/wf7M/wDl
/8Hv+P/B/sz/AOX/we/4/8H+zP8A5f/B7/j/wf7M/wDl/8Hv///G/wDl/8Hv///G/wDl/8Hv9P/B
79D/AOX/we/4/8H+zP8A5f/B7/j/wf7M/wDl/8Hv+P/B/sz/AOX/we/4/8H+zP8A1f/B/s//we/4
/8H+zP8A1f8Gf87/we/0/8Hvw//B/sz/ANX/An/O/8Hv9P/B79D/ANX/cH/O/8Hv9P/B79D/ANX/
OH/O/8Hv///G/wDV/xx/zv/B7///xv8A1f+ef87/we/4/8H+zP8A5f/B7/j/wf7M/wDV/4DP/8Hv
+P/B/sz/ANX/Jn/O/8Hv9P/B78P/wf7M/wDV/2Z/zv/B7/j/wf7M/wDV/2Z/zv/B7/T/we/D/8H+
zP8A1f8Af87/we/v/8Hzwc/D/8Hv0P8A1f8Az//B7+//we/B98P/we/Q/wDl/8Hv7//B38H3w//B
79D/AOX/we/v/8HfwffD/8Hvw//B/sz/AOX/we/v/8HPwffD/8Hvw//B/sz/AOX/we/v/8HHwefD
/8Hvw//B/sz/AOX/we/t/8H8H8H2J8P/we/D/8H+zP8A5f/B7+3/wfwfwfwfw//B78P/wf7M/wDl
/8Hv7f/B98Hzxf/B78P/wf7M/wDl/8Hv7f/B57vF/8Hvw//B/sz/AOX/we/t/8HvwdnB/k/D/8Hv
w//B/sz/AOX/we/t/8HnucHcwffD/8Hv0P8A5f/B7+3/wfe7wd3B98P/we/Q/wDl/8Hv7f/B/H/B
2cH3w//B78P/wf7M/wDl/8Hv7//B2cH3w//B78P/wf7M/wDl/8Hv7//BwcH3w//B78T/f8v/AP//
1f/B38HNwefD/8HvxP9/y/8A5f/B7+7/l8H/we/D/8HvxP9/y/8A5f/B7+3/wfeHxf/B78T/f8v/
AOX/we/t/8H3wd/F/8HvxP9/y/8A5f/B7+3/wfnB38HzB8P/we/Q/wDl/8Hv7f/B/MHfwex3w//B
79D/AOX/we/u/x/B3MH3w//B79D/AOX/we/u/5/BzMH3w//B78T/f8v/AOX/we/v/8HMwfPD/8Hv
xP9/y/8A///W/8Hsd8P/we/E/3/L/wDl/8Hv7f/B/A/B/yfD/8HvxP9/y/8A5f/B7+3/wvPF/8Hv
xP9/y/8A5f/B9+3/wfPB8cX/we/E/3/L/wDl/8Hn7f/B78H5xf/B78T/f8v/AOX/we/t/8HvwfnB
/3/U/wD//9T/wefB8cH/f8P/we/Q/wD//9T/wffB88H/f8P/we/Q/wDl/8Hv7f/B+A/B/3/I/3/L
/wD//9f/f8j/f8v/AP//1/9/yP9/y/8A5f/B7+3/wf4Pwf8/yP9/y/8A///U/8Lzwf9/yP9/y/8A
5f/B7+3/wffB+cH/P8P/we/E/3/L/wDl/8Hn7f/B78H5wf9/w//B79D/AOX/we/t/8HvwfnW/wDl
/8Hv7f/B58H71v8A5f/B5+3/wfHB48X/we/Q/wDl/8H37f/B+cHnxf/B78T/f8v/AOX/we/5/3/L
/wD//9b/wfx/yP9/y/8A5f/B7+3/we/B/cHnyf9/y/8A5f/B5+3/we/B/cH3yf9/y/8A5f/B7+3/
wed9yv9/y/8A5f/B9+3/we951v8A5f/B9+3/we951v8A5f/B9+3/weAB1v8A5f/B9/D/v8j/f8v/
AOX/wffw/7/D/8HvxP9/y/8A5f/B9+//wee/w//B78T/f8v/AOX/wfft/8HnwfnB47/I/3/L/wDl
/8H37f/B5cH5wfG/f8f/f8v/AOX/wfft/8HtecH9vz/C/8H3xP9/y/8A5f/B9+3/we55wf4/v8L/
wffE/3/L/wDl/8H37f/B773B/z/Bz8L/wffE/3/G/8HHxP8A5f/B9+3/we/BycL/wfPC/8H3xP8O
D8r/AOX/wfft/8HvwfHC/8H8wv/B99D/AOX/wffv/8H3h8H/P8H/wffQ/wDl/8H37//B7HfB/8HP
wf/B99D/AOX/wffv/8HMwfvB/8Hzwf/B99D/AOX/wffv/8HMwfPB/8H80v8A5f/B9+//wczB98H/
wfx/wffQ/wDl/8H37//BzHfC/1/B99D/AOX/wffv/8HgB8L/wdfB99D/AOX/wffv/8HzB8L/wfHB
99D/AOX/wffz/8H5wffQ/wDl/8H38//B/n/Q/wDl/8H39P+30P8A5f/B9/T/wefQ/wDl/8H39P/B
8H/P/wDl/8H39P/B9D/P/wDl/8H39P/B9A/P/wDl/8H39P/B9sHGAH/N/wDl/8H39P/B98Hiwf8/
zf8A5f/B9/T/wffB8MH/P83/AOX/wff0/8H3wfzB/z/N/wDl/8H39P/B98H+wfg/zf8A5f/B9/T/
wffB+Dg/zf8A5f/B9/T/wffB+B8/zf8A5f/B9/T/wfA+wf8/zf8A5f/B9/T/wffB/sH/P83/AOX/
wff0/8H3wf7B/z/N/wDl/8H39P/B98H+AD/N/wDl/8H39P/B98H/wd/O/wDl/8H39P/B98H/we/O
/wDl/8H39P/B98H/we/O/wDl/8H39P/B98H/wffO/wDl/8H39P/B98H/wfeAzf8A5f/B9/T/wffB
/4GAzf8A5f/B9/T/wffB84HO/wDl/8H39P/B98Hzz/8A5f/B9/T/wffB/8H5zv8A5f/B9/T/wffB
/8H9zv8A5f/B9/T/wffB/8H8zv8A5f/B9/T/wffB/8H8x/+ACg/E/wDl/8H38v/B+MoAP8f/AOX/
wffq/8HAxQB/w//BwMH/wf7O/wDl/8H33v/Bz8IAQsUAH8z/gA/P/wDl/8H31f/B/MUABcH0B9b/
gAfP/wDl/8H3yP/B8MgAA8P/wf0ttsHbbbNnwf1/1f/B8IAHz/8A4v+0xwCAD8L/wfjIAAPh/8HA
gAfP/wDX/5hgxgA/wv+0xwCAD+3/wcCMB8//AM3/wfzB+8H+KSATxP+YYMYAP/n/wcDBxg/P/wDH
/8Hff8T/wfyLwf4pIBP//8f/wcHB99D/AMf/wcp////R/8HBwffC/8HvzP/B/ADB8MMAQP//1f/B
x8H3wv/B583/AMP/wf3//9b/we/B98L/wffN/wD//9D/wfvK/8H3wv/B883/AP//2//B98L/wfvN
/wDE/8Hz///W/8H3wv/B+83/AP//2//B98L/wf3N/wD//9v/wffC/8H8zf8A///b/8H3wv/B/M3/
AMX/wfP//9X/wffC/8H8zf8Axf/B8///1f/B98L/wf5/zP8Axf/B/b///9f/wf5/zP8Axv+////l
/wD//+z/AMH+///r/wDB/7/E/8H+///l/wDB/8Hn///q/wDB/8H9///d/8HvzP8Awv8/xP/B+///
1//B78z/AML/we///9X/wf4OwgfD/8H3zP8Awv/B+///yf/B/AcBgMHgMBzBz8Hfyv/B98z/AML/
wft//P/B+8HhwePB4Dwfxf/B/AeDwcHB4DAcwc/B38r/wfPM/wDD/1/1/wdzwcDB4MH4HMH/wfvB
4cHjweA8H9j/wfvM/wDD/8HX6P/B/heBwcDB8Dgfxv8Hc8HAweDB+Bzf/8H5zP8Aw//B99b/wffD
/8H+P4cDwcDB4Hgcf8X/wf4XAcHAwfA4H+v/wf3M/wDD/8H81v/B98HhwenB/cH8P4cDwcBgOBw/
9//B/cz/AMT/P8//X8HPwcDB+D4Of///xP/B/Mz/AMT/wc///9r/wf7M/wDE/8H7///a/8H+zP8A
xP/B/f//2/9/y/8Axf+////a/3/L/wD//+D/v8H/wfwBwfwAH4fE/wDF/8H7///V/5/Q/wDF/8H8
///U/8H+H9D/AMX/wf5////T/8H8D9D/AP//2v/B/A/Q/wD//9r/wfwPzv/B/n8Axv/B/f//0//B
/B/O/8H+fwDG/8H9///T/8H8H9D/AMf/f///0v/B/B/Q/wDH/8Hf///S/8H8O9D/AP//2v/B/jvQ
/wDH/8H7///T/3vQ/wDH/8H+///T/8H70P8A///b/8H70P8A7f/B/sHtuMHAwf+/cAueH8H4wePh
/8H70P8A7f/B/kAwQMH/vnALnA/B+EHh/8H70P8A7f/B/mAiXsH/nnHBy5jBz8HzCeH/wfvQ/wDt
/8H+aiN4wf+ec8HLmcHPwfcf4f/B+9D/AO3/wf5CMGDB/x5zwcuZwc/B90fh/8H70P8A7f/B/mI8
YMH/HnPBy5nBz8H3R+H/wfvQ/wDt/8H+Qj98wf9Oc8HLmcHPwcNf4f/B+9D/AO3/wfwHMGDB/sHg
A8HKAcHPwchB4f/B+9D/AO3/wfAPecHBwf/B8APB2gPBz+P/wfvQ/wD//9v/wfvQ/wD//9v/wfvQ
/wDB/f//2v/B+9D/AMH/f///2f/B+9D/AMH/we///9T/we/B/8H9BwOD0P8Awf/B6///y//B5wGA
weA4Hj/F/4cDg9D/AMH/wfr+/wPBwMHgcBwHwf/B08T/wecBwcDB8DgeP8f/wfvQ/wDB/8H+wd/x
/8HfwfvB/8H8PwcDwcDB4DgcBwPBwMHgcBwHwf/B09L/wfvQ/wDC/8Hf5P/B4cHkwfI4PMH/Y8HA
weBwHA4/wd/B+8H/wfw/BwPBwGA4HAfa/8H70P8A5//B4cHkwfI4HL8DgMHAcBwOP+b/wfvQ/wDC
/8H+3//B88H9P/X/wfvQ/wDD/7/Q/8HH///G/8H70P8Aw//B7///1//B+9D/AP//2//B+9D/AMP/
wfv//9f/wfHQ/wDD/8H+///X/8Hg0P8A///b/8HA0P8AxP+////W/8HAf8//AP//2//BwH/P/wD/
/9v/wcDQ/wD//9v/wcDQ/wD//9v/wcDQ/wD//9v/wcDQ/wD//9v/wcDQ/wD//9v/wcB/z/8A///b
/8Hgf8//AMX/wff//9X/weB/z/8A///b/8Hgf8//AP//2//B8H/P/wD//9v/wfB/z/8A///b/8H4
f8//AP//2//B+ND/AP//2//B+dD/AMb/wf3//9T/wfvQ/wDG/8H9///U/8H70P8A///s/wD//9v/
wfvQ/wD//9v/wfvQ/wD//9v/wfvQ/wDH/8H78//Bx+//APv/B97/wfvQ/wD7/wfe/8H70P8A+/8H
3v/B+9D/APv/B97/wfvP/8H+AMH7+v8HzP9/4f/B/gDB+/n/wfwHy//B/j/i/wDI/8H98f/BwAAf
yv/B/B/R/8H70P8AyP/B/cr/wfvm/8HAAB/K/8H4H9H/wfvQ/wDT/8H55v+AwgB/yf/B+D/i/wDB
/8HP0f/B/eb/gMIAA8n/wfh/4v8Awf/B9/n/g8IAD8j/wfh/0f/B/dD/AMH/wfn5/4PB/MIAf8f/
wfg/v9D/we3Q/wDB/8H8+f+Dwf/B4AADx//B+D4P0P/BxdD/ANT/wd/m/8HBwv/DAH7B+8T/wfg/
B9D/g9D/AML/v/j/wcHC/8H4xwABwfw/h9D/wcHQ/wD7/8HBw//B4McADB8H0P+B0P8Awv/B9/j/
wcHD/8HgyQAH0P+B0P8Awv/B+/j/wcHE/0DIAA/Q/4HQ/wDC/8H7+P/Bwcr/weDCAB/Q/4HQ/wDC
/8H++P/Bwcv/gAB/0P+B0P8A+//Bwcv/wf4B0f+D0P8A0f/B/un/wcHM/wfR/4PQ/wDR/8H+6f/B
wcz/B9H/j9D/APv/wcHM/4fR/8H90P8A1f/B3+X/wcHM/4fR/8H90P8A+//Bwcz/g9H/wf3Q/wDE
/z/2/8HBzP/Bw9H/wf3Q/wDE/5/2/8HBzP/Bw9H/wf3Q/wD7/8HBzP/Bw+L/AMT/wffN/8H76P/B
wcz/wcPi/wDE/8H3zf/B++j/wcHM/8HB0f/B/dD/AMT/wf7Q/8H+5f/Bwcz/wcHR/8H90P8AxP/B
/tD/wf7l/8HBzP/B4dH/wf3Q/wD7/8HBzP/B4eL/AMX/v/X/wcPM/8Hh0f/B/dD/APv/wcPM/8Hh
0f/B/dD/APv/g8z/weHR/8H90P8A0//B7+f/g8z/wfHR/8H90P8A1v/B7+T/g8z/wfHR/8H90P8A
xv9/z//B9+T/g8z/wfDi/wDW/8H75P+DzP/B8NH/wf3Q/wDG/8Hvz//B++T/g8L/wfvJ/8Hw0f/B
/dD/AMb/we/0/4fC/8Hgf8j/wfDR/8H90P8A+/+Hwv/B4AHI/8Hw0f/B/dD/AMb/wf70/4fC/8HA
AH/H/8Hw0f/B/dD/AMb/wf7N/3/m/4fC/8HAAH/H/8Hw0f/B/dD/ANf/f+P/h8L/wcAAf8f/wfDR
/8H90P8A1P+/wv+/4/8Hwv/B4AB/x//B8NH/wf3Q/wDU/8Hf5v8Hwv8QAMj/wfjR/8H90P8A1//B
3+P/B8H/wf4AQ8j/wfh/0P/B/dD/ANT/we/m/wfB/8H+AH/I/8H4f9D/wf3Q/wD7/wfB/8H+AH/I
/8H4f9D/wf3Q/wD7/wfC/wB/yP/B+H/P/8H+PdD/APv/B8L/AMn/wfh/z//B+B3Q/wD7/wfC/4DJ
/8H4f8//wfgd0P8A1//B/eP/B8L/wcHJ/8H4f8//wfgd0P8A1//B/eP/B8z/wfh/z//B+B3Q/wD7
/wfM/8H4f8//wfgd0P8A1f9/5f8DzP/B+H/P/8H4HdD/ANX/v8L/f+L/g8L/wfDJ/8H4f8//wfgd
0P8A1f/B3+X/g8L/wfB/yP/B/H/P/8H8DdD/APv/h8H/v8Hgf8j/wfw/z//B/AXQ/wD7/4PB/x/B
4H/I/8H8P8//wfwF0P8A1f/B9+X/g8H/B8Hgf8j/wfw/z//B/g3Q/wDV/8H75f+Dwf8DweB/yP/B
/D/Q/w3Q/wDV/8H75f+Dwf8DwfA/yP/B/D/Q/x3Q/wDV/8H9wv/B9+L/g8H/A8HwP8j/wfw/0P/B
/dD/ANX/wf3C/8H34v+Dwf+DwfA/yP/B/D/Q/8H90P8A1f/B/uX/g8H/g8H4P8j/wfw/0P/B/dD/
ANX/wf7l/4PB/8HPwfg/yP/B/j/Q/8H90P8A+/+Dwv/B+D/I/8H+H9D/wf3Q/wD7/4PC/8HwP8j/
wf4f0P/B/dD/APv/g8L/wfB/yP/B/h/Q/8H90P8A0v/B98b/f+H/wcPC/8Hwyf/B/h/Q/8H90P8A
1v/B38L/v+H/wcPM/8H+H9D/wf3Q/wDW/8Hv5P/BwcL/wfgPyP/B/h/Q/8H90P8A1v/B9+T/wcHC
/8HwD8j/wf4f4f8A0v/B/sP/wffk/8HBwv/B8A/J/x/Q/8H90P8A+//BwcL/weA/yf8P4f8A1v/B
/eT/wcHC/8Hwv8n/D+H/ANb/wf3C/8H74f/BwcL/wf3K/w/h/wDW/8H+wv/B++H/wcHN/w/h/wDW
/8H+f+P/wcHD/4fJ/w/h/wDT/8Hfw/9/4//B4MP/h8n/D+H/ANP/we/D/3/j/8Hgwv/B/gfJ/w/h
/wDX/7/C/3/g/8Hgwv/B+APJ/4/Q/8H90P8A+//B4ML/gAHJ/4fh/wD7/8Hgwf+AAAHJ/4fh/wDX
/8Hvwv/B3+D/weDB/8IAAcn/h9D/wf3Q/wDX/8H34//B4MH/wgDBwcn/h+H/ANr/we/g/8Hgwf8A
MMHgyf+H4f8A1//B+8L/wffg/8Hwwv/B8MHgyf+H0P/B/dD/ANf/wfvC/8H34P/B8ML/wfDB4Mn/
h8//wfw90P8A1//B/cL/wffg/8Hwwv/B8MHgyf+Hz//B+B3Q/wDX/8H84//B8ML/wfBgyf/Bx8//
wfgd0P8A1//B/uP/wfB/wf/B8GDJ/8HHz//B+B/Q/wD7/8Hwf8H/wfBgyf/Bx8//wfgf0P8Azv/B
/cX/we/D/3/B/8H+4P/B8H/B/8HwIcn/wcPP/8H4H9D/ANr/wf7g/8Hwf8H/wfAByf/Bw8//wfgf
0P8A2/9/3//B8H/B/8HwAcn/wcPP/8H4H9D/ANj/wd/C/7/f/8Hwf8H/wfgDyf/Bw8//wfwf0P8A
2/+/2//Bz8P/wfB/wf/B/A/J/8HDz//B/A/Q/wD2/8HwA8P/wfB/zP/B48//wfwf0P8A2P/B993/
wfjB38P/wfg/wf/B8cr/wePP/8H8P9D/APb/wf5Pw//B+D/B/8HgP8n/wePP/8H+f9D/ANv/wffb
/w/D/8H4P8H/weAGP8j/wePP/8H+ftD/ANv/wfPb/4/D/8H4P8H/wfAAH8j/weHQ/8H+0P8A1f+/
xf/B+9v/wd/D/8H4P8H/wfAAH8j/weHQ/8H+0P8A1f+/wv/B/sL/wfvf/8H4P8H/wfgAH8j/weHQ
/8H+0P8A2P/B/sL/wf3f/8H4P8L/AH/I/8Hh0P/B/tD/ANn/f8H/wf3f/8H4P8z/weHQ/8H+0P8A
1f/B7+X/wfg/zP/B4dD/wf7Q/wD7/8H4P8z/wfHQ/8H+0P8A2f/B3+H/wfg/wv/B/AfI/8Hx0P/B
/tD/ANz/n97/wfwfwf/B+DgDyP/B8dD/wf7Q/wDc/8Hf3v/B/D/B/8HwCAHI/8Hw0P/B/tD/ANX/
wf3G/8HP3v/B/D/B/8HwAAHI/8Hw0P/B/tD/ANX/wf7D/8H7wv/B797/wfwfwf/B4MIAyP/B8ND/
wf7Q/wDV/8H+w//B++H/wfwfwf/B4ABAyP/B8ND/wf7Q/wDZ/8H94f/B/B/B/8HwAMHAyP/B8ND/
wf7Q/wDZ/8H94f/B/B/B/8HwAMHgyP/B8ND/wf7Q/wDZ/8H+4f/B/B/B/8HwAMHgyP/B8ND/wf7Q
/wDZ/8H+wv/B+d7/wfwfwf/B8AHB4Mj/wfjQ/8H+0P8A2v9/wf/B/d7/wfwfwf/B+ADB4Mj/wfjQ
/8H+zv/B/sH/ANr/v8H/wf7e/8H8H8H/wfgAAcj/wfh/z/+C0P8A0P/B/sX/wffD/7/C/3/d/8H8
H8H/wfgAA8j/wfh/z/8C0P8A2v/B38L/f93/wfwfwf/B/DADyP/B+H/P/wLQ/wDW/8H7w//B7+D/
wfwfwf/B/cH+A8j/wfh/z/8C0P8A1v/B+8P/we/g/8H8H8z/wfh/z/8C0P8A1v/B/cP/wffg/8H8
H8z/wfh/z/8G0P8A0f/B38v/we/d/8H8H8z/wfh/z/8G0P8A0f/B38X/f8L/wfvC/8Hv3f/B/B/M
/8H4f8//wc7Q/wDX/3/C/8H7wv/B593/wfwfzP/B+H/P/8He0P8A0f/B98j/wf3C/8Hz3f/B/B/M
/8H8f8//wf7Q/wDR/8H3yP/B/cL/wfvd/8H8H8z/wfw/z//B/tD/ANr/wf7C/8H73f/B/B/M/8H8
P8//wf7Q/wDb/3/f/8H8H8z/wfw/z//B/tD/ANv/f9//wfwfzP/B/D/P/8H+0P8A0f/B/sn/v9//
wfwfzP/B/D/P/8H+0P8A1//B98P/wd/C/3/c/8H8H8z/wfw/z//B/tD/ANL/f8j/wd/f/8H8H8z/
wfw/z//B/tD/AN7/n9z/wfwfzP/B/D/P/8H+0P8A1//B/cP/wfff/8H8D8z/wfw/z//B/tD/ANf/
wf3D/8H3wv/Bz9z/wfwPyf/B/H/B/8H8P8//wf7Q/wDb/8H3wv/B79z/wfwfyf/B+D/B/8H+P8//
wf7Q/wDS/8HvyP/B+d//wfwfyf/B8D/B/8H+P8//wf7Q/wDS/8HnyP/B+cL/wfvc/8H+D8n/wfA/
wf/B/h/P/37Q/wDS/8H3y//B89z/wf4Pyf/B4H/B/8H+H8//ftD/AN7/wfvc/8H+D8n/weB/B8H+
H87/wfwe0P8A3P9/wf/B/dz/wf4Pyf/B4MH/A8H+H8H7j8z/wfwe0P8A0v/B/cv/wfzc/8H+D8n/
wePB/gPB/h/B8AfM/8H4HtD/ANL/wf7F/8Hvw/+/3v/B/g/D/4fG/8H+B8H+H8HwD8H/h8r/wfge
0P8A1/+PxP+f3v/B/g/D/wPH/z/B/h/B4A/B8AfB4cHDyP/B+B7Q/wDX/8HnwffD/8Hf3v/B/g/D
/4HJ/x/B8B/B4AfB4AHC/wDF/8H4PtD/ANf/wefE/8Hv3v/B/g/D/8HByf8fwfg/weAHwcABwf/B
/gB/wfgPwv/B/cH+0P8A1v/B+cHjxP/B78L/wd/c/w/D/8HAyf8fwf7B/8HgD8HAA8H/wf4Af8Hw
AD/C/8H+0P8A0//B38L/wfjB8cf/wc/B/3/a/w/D/8HAyf8Pwv/B4B/B4APB/8H+AH/B4AAfwv/B
/tD/ANb/vHHB/sP/wfvC/8HnuH/a/w/D/8HAyf8Pwv/B8B/B8AfC/wDB/8HgAB/C/8H+0P8A0//B
78L/nnjB/sP/wfvC/8H3kNv/D8P/weDJ/w/C/8H4f8H8D8L/weHB/8H8AB/C/8H+0P8A0//B78L/
HmDB/3/C/8H7wv/B88HDwd/a/w/D/8Hgyf8PyP/B98L/AB/C/8H+0P8A1v+PIcH/f8L/wf3C/8H7
wcfB39r/D8P/weDJ/w/L/8HoP8L/wf7Q/wDT/8Hzwv/Bxwd/w//B/sP/we8f2v8Pw//B4Mn/D8v/
wf7D/8H+0P8A0//B+8L/wccfP8P/wf7D/8H2f9r/B8P/wcDJ/w/P/8H+0P8A0//B/cL/weA/n8Hf
w/9/wv/B+Yfa/wfD/4DJ/w/g/wDT/8H9wv/B8MH/n8T/v8H/wf7B8MHv2v8Hw/8Byf8P4P8A1v/B
88H/wc/I/8Hb2v8Hw/8Ayf+Pz//B/tD/ANj/wefB98P/wd/C/3+z2v8Hw/8Af8j/h+D/ANT/f8P/
h8H7xv+/T9r/B8P/wfB/yP+Hz//B/n/P/wDY/xfB+8P/we/C/8H+GNr/h8P/wfB/yP+H0P9/z/8A
1P/B38L/wfwfxP/B98L/wf44f9n/h8P/AH/I/4fQ/3/P/wDU/8Hfwv/B+G/E/8H3w//Byz/Z/4PC
/8H+AH/I/4fQ/3/P/wDX/8LgyP/ByT/Z/4PC/8H8AMn/h9D/f8//ANf/wcPB4H/D/8H7wv/B98HZ
f9n/g8L/wfwByf+H0P9/z/8A1//Bx8H8wn/C/8H9wv/B98HJX9n/g8L/wfwHyf+H0P9/z/8A1P/B
98P/wf5/v8L/wf3D/8HDwc/Z/4PN/4fQ/3/P/wDX/8H8wf8/wd/C/8H+wv/B+8H/we/Z/4PN/4fQ
/3/P/wDX/8H5wf8/wd/D/3/C/8H+Z9n/wcPN/4fQ/3/P/wDX/8H5wf8/xP9/wv/B+3PZ/8HDxP8/
yP/Bx9D/f8//ANf/wfnB/3/E/7/C/8H5h9n/wcHD/8H+H8j/wcfQ/3/P/wDX/8H8wf53xP/B38L/
wf2f2f/BwcP/wcMfyP/Bw9D/f8//ANf/wfzB+MHnxP/B38L/vn/Z/8HBw/+BH8j/wcPQ/3/P/wDX
/8H8AMHnxP/B78L/vn/Z/8HBw/8AH8j/wcPQ/3/P/wDX/8H+AcHHwf3D/8Hnwv/B79r/wcHD/wAf
yP/Bw9D/f8//ANX/wd/C/4fBz8H9w//B98L/we/a/8HBw/8AH8j/wcPQ/3/P/wDV/8Hfwv/B+A/B
/sP/wfPC/8H32v/B4cP/AB/I/8HD0P9/z/8A2P/B+B/B/sP/wfvC/8H32v/B4cP/AD/I/8HD0P9/
z/8A2P/B8B/B/3/C/8H53f/B4MP/gD/I/8HD0P9/z/8A2P/B5x/E/8H9xP+P2P/B4MP/wcB/yP/B
w9D/f8//ANX/wfvC/8Hnnj/D/8H+w//B4D/Y/8Hgzf/B49D/f8//ANj/wfPBzH/D/8H+wv/B/cH+
2f/B4ML/wfgHP8j/wcPQ/3/P/wDV/8H9wv/B88Hhxf9/wf/B/sH82f/B4ML/weDCAMj/wePQ/3/P
/wDY/8H5wcPBz8T/v8L/wf3Z/8Hwwv/B4MIAyP/B49D/f8//ANX/wf7C/8H8D4fE/5/C/33B+dj/
wfDC/8HgwgDI/8Hh0P9/z/8A2P/B/B4fx//B/sHj2P/B8ML/wfXB/gHI/8Hh0P9/z/8A2P/B/Dgf
wfnD/8Hvwv/B/gfY/8Hwwv/B98H+AMj/weHQ/3/P/wDW/7/B/8H+wfB/wfvD/8Hvwv/B/h/Y/8Hw
xP+AyP/B4dD/f8//ANb/wd/C/8HBwcPE/8H3wv/B/jx/1//B8MP/wfwAyP/B4dD/f8//ANb/wd/C
/4fBwMH+w//B88L/we84P9f/wfDD/8HgAcj/weHQ/3/P/wDZ/4/BwMH+f8L/wfvC/8Hvmb/X/8Hw
w//B4API/8Hh0P9/z/8A1v/B78L/v8H8f8b/wfejn9f/wfDD/8HAD8j/weHQ/3/P/wDZ/8H9wf5/
P8L/wf3C/8H7wc+P1//B8MP/gD/I/8Hh4P8A2f/B+cH+P5/C/8H+wv/B+8H/wc/X/8Hwwv/B/gHJ
/8Hh4P8A2f/B8cH/f8j/j9f/wfDC/8H+B8n/weHg/wDZ/8Hzwf5/we/G/8H+N9f/wfDC/8H+wgAD
x//B4eD/ANb/wfzC/8Hxwfx/we/D/7/C/8H4wcPX/8Hww/9gAAHH/8Hh4P8A2f/B+cH4yP/B+Z/X
/8Hww//BwAABx//B8eD/ANf/f8H/wfxhwf/B88P/wd/C/z4/1//B8MP/gAADx//B8eD/ANf/P8H/
wfwBwf/B+8P/wc/C/xx81//B8MP/gAAHx//B8eD/ANf/v8H/wf4Dwf/B+cP/we/C/7141//B4MP/
wd/J/8Hh4P8A3P/B/MP/wffD/2HX/8Hgzf/B4eD/ANz/wf7D/8Hzwv/B7wPX/8Hhzf/B4eD/ANz/
wf7D/8H7w/8P1//B4M3/weHg/wDd/3/C/8H7w/+eP9b/weDN/8Hx4P8A3f+/wv/B/cP/wcyf1v/B
4M3/wfHg/wDd/7/C/8H+wv/B+cHDwc/W/8Hhzf/B8eD/ANf/wfvE/x/B38X/wf3B58HP1v/B4M3/
weHg/wDX/8H9xP+Pwe/D/3/B/8H+wf/B39X/wfwgzf/B4eD/ANz/j8Hvxf/B/n8f1f/B+AHN/8Hh
4P8A1//B/sP/wfPBx8H3w/+/wv/B/H/V/8H4AD/M/8Hh4P8A1//B/n/C/8HxwePB88P/n8L/wfzW
/8H4wgDM/8Hh4P8A2P9/wv9wwePB+8P/wc/C/8H91v/B+MIAA8v/weHg/wDa/8H+OMHhwfvD/8Hv
wv/B39f/wwAHyv/B4eD/ANr/wf44wcHE/8H3wv/B39f/weDEAMn/weHg/wDb/xxDxP/B98L/wc/X
/8HggMMAyf/B4eD/ANv/ng7H/8Hn1//B4MH/xAAPx//B4+D/ANj/we/C/8HMPH/G/8H3wfw/1f/B
4cH/wfzEAAfG/8Hj4P8A2//BwH5/w//B/cL/wfvBxZ/V/8Hxw//EAB/F/8HD4P8A2//B4cH/P7/G
/8HZn9X/wfvE/8H8wwAfxP/Bw+D/ANj/wfvC/8Hnwf8fwd/G/8Hln9v/wfzDAMT/wcPg/wDd/5/B
z8P/f8L/wfR/3P/B/sIAA8P/wcPg/wDY/8H9xP+Pwe/F/8H+wfHB993/wfjCAD/C/8HH4P8A2P/B
/sP/wf4/wffD/7/C/3/Bw97/weAAAcL/wcfQ/7/P/wDY/8H+w//B+D/B98P/v8L/fwfe/8HwwgAH
wf+H0P+/z/8A2f9/wv/B4HPH/74f3//BwMMAh9D/v8//ANn/P8L/wcHB88T/we/C/54/3//B+MMA
h9D/v8//ANn/v8L/g8HxxP/B78L/wdw/4P/B4MIAj9D/v8//ANz/D8HxxP/B9+X/wcAAj9D/v8//
ANn/wd/C/z94xP/B9+X/wf4DD9D/v8//ANz/wf48wf9/wv/B++f/D9D/v8//AN3/PMJ/xf/B9+T/
D9D/v8//ANz/wceeP7/C/8H9wv/B++T/D9D/v8//ANn/wfvC/8HjjD/B38L/wf7C/8H75P8f0P+/
z/8A3P/B48HAxf9/wf/B/eT/D9D/v8//ANn/wf3C/8HxwcPB/8Hvw/8/wf/B/uT/D9D/v8//ANz/
wfmHf8Hnw/+/2v/B8Mv/D9D/v8//ANz/wfgMB8H3w/+f2v/B8H/K/w/Q/7/P/wDa/3/B/8H4EAPB
+8P/n9r/wfB/yv8f0P+/z/8A2v9/wf/B/HBjwfvD/8Hf2v/B8H/K/z/Q/7/P/wDc/8H+weHB48H9
w//B78L/v9f/wfB/2/+/z/8A3f/B58Hxwf3D/8Hn2v/B+9z/v8//ANr/wd/C/8HHwfjB/sP/wfPC
/8HP9P+/z/8A2v/B78L/wc/B+MT/wfvC/8Hv9P+/z/8A3f/Bz8Hwwf9/wv/B+ff/v8//ANr/wffC
/8HHwcPCv8L/wf33/7/P/wDa/8H7wv/B5w8/w//B/vf/v8//ANr/wfvC/8HyHj/B38L/wf5/wf/B
/fT/v8//ANr/wf3C/8HwfH/E/3/B/8H89P+/z/8A2v/B/cL/wfnB/H/B78P/v8H/wf5/1f/B+APc
/7/P/wDa/8H+wv/B+cH4f8Hnw/+/wv9/1f/BwADc/7/P/wDa/8H+w//B+H/B98P/n8L/v9X/wgA/
2/+/z/8A2/9/wv/B8D/B+8P/wd/C/7/U/8H8wgAf2/+/z/8A2/8/wv/B4j3B+cP/we/X/8H8wgAP
2/+/z/8A2/+/wv/B5xnB/cP/wefX/8H4AHgP2/+/z/8A3v/BxwDE/8H31//B8A/B/4fb/7/P/wDb
/8Hvwv+AH8H+w//B+8L/we/U/8HgP8H/wcPb/7/P/wDb/8Hvwv/Bw8X/wf3C/8H31P/B4H/B/8Hr
2/+/z/8A5P/B/df/wcHC/8Hx2/+/z/8A5P/B/sL/wfvU/8HBwv/B+dv/v8//ANv/wfvL/8H91P/B
w8L/wfzb/7/P/wDb/8H9yf9/1v+Hwv/B/Nv/v8//ANv/wf3J/z/W/4fC/8H82/+/z/8A2//B/cn/
v8L/f9P/h8L/wf5/2v+/z/8A3P9/xP/B98P/n8L/f9P/h8L/wf5/2v+/z/8A3P9/w//C88P/wc/C
/z/T/4/C/8H+f9r/v8//ANz/f8P/wfPB+8P/we/C/7/T/4/C/8H+2/+/z/8A3P+/w//B98T/we/C
/8Hf0//Bz8L/wf6/2v+/z/8A3P/B39//wc/C/8H4P9r/v8//ANz/wd/I/8H71v/Bz8L/wcB/2v+/
z/8A3P/B78j/wfvW/8HHwf/B/gFf2v+/z/8A3P/B58P/wfB/w//B/db/wefB/8HwAF/a/7/P/wDc
/8H3w//B4z/D/8H9wv/B+9P/wefB/4QDwc/a/7/P/wDg/8Hnn8P/wf7W/8HjwfwAH8Hv2v+/z/8A
3P/B+8P/Z4/B38P/f9X/wfPB4AHB/8Hn2v+/z/8A3P/B/cL/wf5nj8HPw/8/wf/B/tP/wfAAA8H/
wefa/7/P/wDc/8H8wv/B/nPBz8Hvw/+/wf/B/tP/wfAAD9z/v8//ANz/wf5/wf/B/nOPwffD/5/V
/8HwAD/c/7/P/wDd/3/B/8H+cY/B88P/n9X/wfgD3f+/z/8A3f9/wf/B/nGfwfvD/8HP1f/B+B/d
/7/P/wDQ/8H+B8v/v8L/O8H/wfvD/8Hvwv/B39L/wfh/7f8A0P/B8ADO/wPB/8H9w//B59X/wf3C
/8H4H9r/v8//AND/wePB/D/K/8Hfwv+Hwf/B/sP/wffV/8H9wv/B4Afq/wDQ/4/B/x/K/8HvxP/B
/n/C/8H7wv/B99L/wfzC/8HAAdr/v8//AND/P8H/wc/Q/3/C/8H9wv/B+9L/wf7C/4Ch2v+/z/8A
0P9/wf/B58r/wffI/8H92P+BwfTa/7/P/wDP/8H+wv/B18r/wfvF/5/C/8H+1//B54fB+H/p/wDP
/8H+wv/B48r/wfvF/8Hfw/9/1f/B/gGHwfh/6f8Az//B/sL/wfPK/8H9xf/Bz8P/f8H/wf7T/8H4
AIfB/D/p/wDP/8H9wv/B88r/wf7F/8Hnw/+/wf/B/n/S/8H4AA/B/j/p/wDP/8H9wv/B88r/wf7F
/8Hnw/+fwv8/0v/B8AAfwf8/6f8Az//B/ML/wfnK/8H+f8T/wfPD/8Hfwv+/0v/B8Bwfwf8f6f8A
z//B/ML/wfnL/3/E/8H72f/B8HwPwf+f6f8Az//B/ML/wfnL/7/E/8H5w//B78L/wd/S/8Hgwf8P
wf/B3+n/AM//wf7C/8H5y/+/xP/B/cP/wffC/8Hf0v/B4cH/j8H/n9j/wfk/z/8Az//B/ML/wfnQ
/8H+2f/B48H/j8H/r9H/wfzCAAEKr9L/AM//wfzC/8H5y//B78T/wf5/wv/B+9X/wfXB/8HPwf/B
78v/wfzCAAPZ/38Az//B/ML/wfHL/8Hvxf9/wv/B/dX/wfPB/8HPwf+vw//B+gCAwgAD4P8Az//B
/MIAAcv/wffF/7/F/8H70v/B8cH/wcDEAAvm/wDP/8H9wv/B/cv/wfvF/7/C/8H+wv/B+c3/wfjD
AKmgwf/B58HAB+n/AM//wf3C/8H9y//B+8j/wf5/wf/B/cX/v8H+uMIAA8L/wfhBBBDB/8Hxwf/B
4gAH6f8Az//B/cL/wfnL/8H9yf9/wfjDAAgCwf+/wf64wgADx//B+cH/weAAA+n/AM//wf3C/8H5
y//B/Mn/v8H4wwAIAs7/wfjB/8HAAXvp/wDP/8H9wv/B/cv/wf7J/7/B/8H+0v/B+MH8AAfB++n/
AM//wf3C/8H9y//B/t7/wfjB/AB/wfnp/wDP/8H9z/9/yP/B39T/wfzB4AHB/8H96f8A3/8/yP/B
79T/wfwAD+v/AN//v8T/wf3G/8Hf0f/B/QC/6/8A0f/B+B/M/8HfxP/B/sP/wffC/8Hf0f/B/gPs
/wDQ/8HhwfGH0f/B/sb/wc/R/8H+D+z/AND/gGfB49L/f8X/we/R/8H+P+z/AND/Py/B88z/wffF
/7/C/8H9wv/B99L/P+z/AND/f4/B6cz/wffF/7/Y/z/s/wDP/8H+f5/B8dL/v9j/P8H/wf4B6f8A
z//B/sL/wfnM/8H9yf9/1P+fwf/B8AB/6P8Az//B/sH/v8H5zP/B/cn/f8H/wf3S/5/B/4DB8D/o
/wDR/7/B+cz/wf7L/8H80v/B38H8B8H+H+j/ANH/v8H5zP/B/sv/wf7S/8HPwfg/wf+P2P/B38//
ANL/wfnN/3/I/8Hfwf/B/n/R/8HPwcHC/4/Y/8Hfz/8Az//B/sL/wf3S/8H7xv9/0f/Bxg/C/8HP
2P/B38//AM//wf7C/8H9zf+/xP/B/cP/we/C/7/R/8HgP8L/wdfY/8Hfz/8Az//B/sL/wfnN/8Hf
xP/B/cP/wffU/8Hhw//B59j/wd/P/wDP/8H+wv/B+dL/wf7Y/8H3w//B59j/wd/P/wDP/8H+ACAB
zf/B78X/f8L/wfvC/8Hv0f/B98P/wePY/8Hfz/8A0P8AYcHN0/8/wv/B/cL/we/R/8Hzw//B89j/
wd/P/wDS/8H9zf/B98X/v8X/wffR/8H7w//B89j/wd/P/wDS/8H5zf/B88X/n8L/wf7C/8H30f/B
+8P/wePY/8Hfz/8A0v/B+c3/wfnF/8Hfwv/B/sL/wfPR/8H7w//B49j/wd/P/wDP/8H+wv/B/c3/
wf3F/8HPw/9/wf/B+9X/wePY/8Hfz/8Az//B/sL/wf3N/8H8xf/B78P/f8H/wfvV/8HD2P/B38//
AOD/wf7F/8Hnw/+/1//Bx9j/wd/P/wDh/3/E/8Hzw/+/1/8H2P/B38//AOb/wfvF/8H+1P/B/A/Y
/8Hfz/8Az//B/tH/v8T/wf3D/8HPwv9/0//B8B/Y/8Hfz/8Az//B/n/V/8H8w//B78L/f9P/gj/Y
/8Hfz/8A0P/CAB/O/8HfxP/B/sb/v9H/f8H8GX/Y/8Hfz/8A0v/Bx9P/wf5/wv/B98L/v9H/P8HA
A9n/wd/P/wDS/8Hjzv/B98X/P8L/wfvU/54AD9n/wd/P/wDS/8Hx1P+/wv/B+dT/gAB/2f/B38//
ANL/wfHO/8H7xf/B38L/wfnU/4AAf9n/wd/P/wDS/8H5zv/B+cX/wd/C/8H91P/BwBva/8Hfz/8A
0v/B+c7/wf3F/8Hfwv/B/ML/wffR/8HAf9r/wd/P/wDS/8H8zv/B/sj/wf7C/8H30f/Bx8L/wfDY
/8Hfz/8A0v/B/M7/wf7F/8Hvw/9/wf/B+9H/wcfC/8Hg2P/B38//ANL/wfzY/3/B/8H70f/B78L/
gB/X/8Hfz/8A0v/B/M//f8T/wffD/7/B/8H90f/B78L/Bw/X/8Hfz/8A0v/B+M//v8T/wffD/7/B
/8H90f/B98H/wf7Ch9f/wd/P/wDS/8H4z//B38j/wd/T/8H3wf/B/A/Bw9f/wd/P/wDP/8H+wv/B
88//wd/E/8H7w//Bz9X/wfwfwevX/8Hfz/8Az//B/sL/wcfP/8HvxP/B+8P/we/C/3/R/8H+wfw/
wfHX/8Hfz/8Az//B/sIAD8//wffI/8Hnwv8/0f/B4MH4P8H15/8Az//B/sIAD8//wffE/8H9w//B
98L/v9H/AMH4P8H51//B78//AND/ACB/z//B88T/wf3D/8Hzwv+f0P/B/AfB+D/B+ef/AOL/wfvE
/8H+w//B88L/wd/Q/8H8H8H4P8H51//B78//AOL/wf3E/8H+w//B+8L/wd/Q/8H+n8H4P8H55/8A
4v/B/cT/wf7D/8H51P8/wfg/wfjX/8Hvz/8Az//B/tL/wf7I/8H91P9/wfR/wfnn/wDP/8H+0v/B
/n/E/3/C/8H+1f/B9H/B8Nf/we/P/wDj/3/H/8H+wv/B99D/wf6/wfB/wfLn/wDj/3/H/8H+f8H/
wfPR/7/B+MH/weI/1v/B78//ANH/wfwH0P+/yP9/wf/B+9H/P8H4wf/Bw+f/AND/AcHxwfPQ/7/I
/7/B/8H70f8/weDB/5/X/8Hvz/8A0P8PwefB6dD/n8j/v8H/wf3R/1/B4MH+P+f/ANH/wefB+ND/
wd/I/5/T/4/Bwdn/we/P/wDR/8HHwfzQ/8HPyP/B39P/hwHp/wDP/8H+wf/Bz8H80P/Bz8j/wc/T
/8HAA9n/we/P/wDP/8H+wf+fwfzQ/8HvxP/B78P/we/T/8HAF+n/AM//wf7B/5/B/ND/we/E/8Hn
w//B78L/P9D/wfaf2f/B78//ANH/v8H80P/B58T/wffD/8H3wv8/+/8A0f8/wfzQ/8H3yP/B98L/
v+v/we/P/wDP/8L+f8H80P/B98T/wfvD/8H7wv+f+/8A0P9+wf/B+dD/wfPI/8H7wv/B3+v/we/P
/wDQ/zzB/8H50P/B+8j/wf3C/8Hf+/8A0P+Bwf/B8dD/wfnI/8H97v/B78//AND/wefB/8HF0P/B
/cj/wf7+/wDQ/8Hvwf8e0P/B/cj/wf7C/8H36//B78//AOP/wfzJ/3/B/8H3+/8A4//B/MT/wf7E
/3/B/8H76//B78//AOP/wf5/w//B/sT/P8H/wfv7/wDj/8H+f8T/f8P/v8H/wf3r/8Hvz/8A5P9/
xP9/w/+/wf/B/fv/AOT/f8T/f8X/wf3r/8Hvz/8A5P8/xP+/w//B3+3/we/P/wDk/7/I/8Hf7f/B
78//AOT/v8T/wd/D/8Hv7f/B78//AOT/n8T/wd/D/8Hvwv9/6v/B78//AOT/wd/I/8H37f/B78//
AMv/wePY/8HPxP/B78P/wffC/7/q/8Hvz/8Ay/8P2P/Bz8T/we/D/8H77f/B78//AMr/wfwP2P/B
78T/we/D/8H7wv/B3+r/we/P/wDK/8H4T9j/wefE/8H3w//B+8L/wd/q/8Hvz/8Ayv/B+A/Y/8Hn
xP/B98P/wf3t/8Hvz/8Ay/8H2P/B98T/wfvD/8H87f/B78//AMv/wcPY/8H3xP/B+8P/wfzt/8Hv
z/8Ay//B/dj/wfvE/8H7w//B/u3/we/P/wDk/8H7yP/B/n/B/8H36v/B78//AOT/wf3E/8H9xP9/
wf/B9+r/we/P/wDK/8H8Adj/wf3E/8H9xP9/7P/B78//AMr/wf7Z/8H9xP/B/sT/v8H/wfvq/8Hv
z/8A5P/B/sT/wf7E/7/s/8Hvz/8A5P/B/sn/n+z/we/P/wDk/8H+xf9/8P/B78//AOX/f8T/f8P/
wc/B/8H+6v/B78//AOX/f8T/f8P/we/B/8H+6v/B78//AOX/P8T/v8P/we/B/8H+f+n/we/P/wDl
/7/E/7/D/8Hnwv9/6f/B78//AOX/v8j/wffC/3/p/8Hvz/8A5f+/yP/B9+z/we/P/wDl/8HfyP/B
8+z/we/P/wDK/8H9xf/B/gA/0v/B38j/wfPs/8Hvz/8Ayv/B/cX/wfjB/g/S/8HfyP/B+9b/wfx/
1P/B78//AMr/wfwBxP/B88H/wcfS/8HvyP/B+cL/we/T/8HAP9T/we/P/wDK/8H9xf/B58H/wfPb
/8H9wv/B79P/k5/U/8Hvz/8Ayv/B/cX/wc/B/8Hx0v/B98T/wffD/8H9wv/B79P/mZ/U/8Hvz/8A
0P+/wf/B8dL/wffE/8H7w//B/Nb/wc8/5P8Ay//B88T/v8H/wfjS/8H3xP/B+8P/wf7C/8H30//B
zj/k/wDK/8H8wfHE/7/B/8H80v/B+8T/wfvD/8H+f8H/wffT/8HOf+T/AMr/wfzB8cT/f8H/wfzS
/8H7xP/B+8T/f8H/wff5/wDK/8H9wfnE/3/B/8H80v/B+8T/wf3E/3/7/wDK/8H9wfnE/3/B/8H+
0v/B+cT/wf3w/8H3z/8Ayv/B/cH5xP8/wf/B/n/R/8H9yf9/6//B98//AMr/wfzB8cT/P8H/wf5/
0f/B/cn/v+v/wffP/wDK/8H+B8T/v8H/wf5/0f/B/sn/v8H/wf35/wDQ/7/B/8H+f9H/wf7J/5/r
/8H3z/8Ay//B+cT/v8H/wf5/0f/B/sv/wf7p/8H3z/8Ay//Bw8T/v8H/wf5/0v9/yP/Bz+v/wffP
/wDL/w/E/4DCAH/S/3/E/z/D/8Hv6//B98//AMr/wfxvxP/B4ABvf9L/v8T/v8P/we/r/8H3z/8A
yv/B/GfE/8HgAsJ/0v+/xP+f//8Ayv/B/cHDxP9/wv9/0v+fxP+fw//B98L/v/j/AMv/wcHE/3/B
/8H+f9L/n8T/n8P/wffC/7/o/8H3z/8Ay//B8cT/f8L/f9L/wd/E/8Hfxv+/+P8Ay//B+Mf/f9L/
wd/E/8HPw//B+/v/AMr/wf44xP9/1f/Bz8T/we/D/8H7wv/B3+j/wffP/wDK/8H8g9r/we/E/8Hv
xv/B3+j/wffP/wDK/8H8wc/a/8HvxP/B58P/wf3r/8H3z/8Ayv/B/MHfxP9/1f/B58T/wffv/8H3
z/8Ayv/B/MHfxP8/1f/B98T/wfPD/8H+6//B98//AMr/wfwBxP/DAH/S/8HzxP/B+8P/wf7r/8H3
z/8Ayv/B/cX/f9X/wfvE/8H7w//B/sL/wffo/8H3z/8A0P9/1f/B+8T/wfnD/8H+f8H/wffo/8H3
z/8Ayv/B/sX/f8H/P9P/wfnE/8H5xP9/wf/B8+j/wffP/wDK/8H8xf9/wfw/0//B/cT/wf3E/3/B
/8H76P/B98//AMr/wfzB38X/wfh/0//B/MT/wf3E/7/B/8H76P/B98//AMr/wfzB08X/wfnU/8H8
xP/B/MT/v+r/wffP/wDK/8H8xv/B89T/wf7E/8H+xP+/6v/B98//AMr/wf3G/8Hn2f/B/n/u/8H3
z/8Ayv/B/Mb/wc/U/8H+yf/B3+r/wffP/wDR/x/V/3/E/3/D/8Hfwf/B/Oj/wffP/wDQ/8H+f9X/
f8T/P8X/wf7o/8H3z/8A0P/B/Nb/v8T/v8P/we/B/8H+6P/B98//AND/wfHW/7/E/5/F/8H+f+f/
wffP/wDQ/8Hjwv9/0/+fxP/B38b/f+f/wffP/wDQ/8HDwv9/0//B38T/wd/D/8H3wv9/5//B98//
AND/h2cgf9P/wd/E/8Hvw//B98L/f+f/wffP/wDQ/57CAH/T/8HPxP/B78P/wffq/8H3z/8A0P++
wgB/0//B78j/wfvq/8H3z/8A0/9/0//B78j/wfvq/8H3z/8A0/9/3P/B+8L/n+f/wffP/wDT/3/T
/8H3y//B3+f/wffP/wDQ/7/f/8H9wv/B3+f/wffP/wDQ/7/i/8HP5//B98//AOf/wfvE/8H9xv/B
7+f/wffP/wDQ/7/b/8H9w//B/ur/wffO/8HAAA==

--Boundary_(ID_2NoDOk3hBTI0RJAXbEILiQ)--

