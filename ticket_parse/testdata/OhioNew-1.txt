
Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00001
Ticket:  0730-500-670-01  Type:  On Job        Previous Ticket: 0730-500-670-00

State :  OH  County:  WASHINGTON         Place:  MARIETTA
Addr  :                   Name: WESTVIEW AV
Cross :                   Name:
Offset:                    and

Locate:  "WESTVIEW AV"

Where :  LOCATE BOTH SIDES OF WESTVIEW AVE FROM CTY RD 4 TO TWP RD 50. INCLUDE
         NW & SW CORNERS WITH CTY RD 4.
Subdivision:                                            Lot #  
Near St:      CTY RD 4
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 02:15 PM    Dig Up Type:                    Blasting: 
Work:   GAS LINE INSTALLATION                                  Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   ** CUSTOMER ADVISES THAT NONE OF THE MEMBER COMPANIES HAVE MARKED --
         REQUEST MEMBER COMPANIES RESPOND ASAP OR CALL WITH CLEARANCE
         REPEAT MESSAGE: 2004/08/03 13:18:25 SHARON K

Contact: JOHN MADY                                             Caller Type: C
Company: BILL HAWK INC
Addr1:   2421 N WOOSTER AVE
Addr2:   
City:    DOVER                   State:  OH           Zip:  44622
Phone:   330-602-2922            Fax:   330-343-3240  
Email:   
Working For:  DOMINION EAST OHIO
Done By:  

Within:  n 39 26.0    s 39 25.5    w 81 29.0    e 81 28.0    

mailing  BILL HAWK INC
         2421 N WOOSTER AVE
         DOVER, OH 44622

Members:  AGP   =ALLEGHENY POWER CMR   =MARIETTA CITY   EOK   =DOMINION EAST O 
          OBT   =SBC - ZANESVILL 
Lbps:     RAD   =RENO AREA WT/SW TCC   =CHARTER COMMUNI 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00003
Ticket:  0803-055-036-00  Type:  On Job        Previous Ticket: 0726-080-118-00

State :  OH  County:  WASHINGTON         Place:  BARLOW TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  DUNHAM TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and

Locate:  "S R 339" thru

 county   OH WASHINGTON
 place    DUNHAM TWP         class 
Locate:  "S R 339"

Where :  LOCATE BOTH SIDES OF S R 339 FROM S R 550  IN BARLOW TWP GOING SOUTH
         THROUGH DUNHAM TWP ENDING AT S R 618 IN BELPRE TWP --TOTAL DISTANCE
         APPROX  8 MI
Subdivision:                                            Lot #  
Near St:      NO NEAR STREET GIVEN
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 10:56 AM    Dig Up Type:                    Blasting: 
Work:   GUARD RAIL INSTALLATION                                Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   1 OF 2 TICKETS
         CALLER REQUESTS REMARKINGS
         PREVIOUS OUPS NUMBER: 0726-080-118

Contact: TODD                                                  Caller Type: C
Company: P D K  CONSTRUCTION INC.
Addr1:   34070 CREW RD
Addr2:   
City:    POMEROY                 State:  OH           Zip:  45769
Phone:   740-992-6451            Fax:   740-992-3074  
Email:   
Working For:  
Done By:  

Within:  n 39 27.0    s 39 18.5    w 81 40.5    e 81 39.0    

mailing  P D K  CONSTRUCTION INC.
         34070 CREW RD
         POMEROY, OH 45769

Members:  AGP   =ALLEGHENY POWER ALD   =WESTRN RESRVE - CTY   =COLMBIA GAS TRA 
          EOK   =DOMINION EAST O GAC   =GATHERCO INC    GTF   =VERIZON         
          GTO   =VERIZON  - COLU OBT   =SBC - ZANESVILL PIP   =COLUMBIA GAS- N 
          WAE   =WASHINGTON ELEC 
Lbps:     LHW   =LTLE HOCKING WA TCC   =CHARTER COMMUNI WCC   =WASHINGTON CTY  
          WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00004
Ticket:  0803-055-036-01  Type:  On Job        Previous Ticket: 0803-055-036-00

State :  OH  County:  WASHINGTON         Place:  BARLOW TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  DUNHAM TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and

Locate:  "S R 339" thru

 county   OH WASHINGTON
 place    DUNHAM TWP         class 
Locate:  "S R 339"

Where :  LOCATE BOTH SIDES OF S R 339 FROM S R 550  IN BARLOW TWP GOING SOUTH
         THROUGH DUNHAM TWP ENDING AT S R 618 IN BELPRE TWP --TOTAL DISTANCE
         APPROX  8 MI
Subdivision:                                            Lot #  
Near St:      NO NEAR STREET GIVEN
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 10:56 AM    Dig Up Type:                    Blasting: 
Work:   GUARD RAIL INSTALLATION                                Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   1 OF 2 TICKETS
         CALLER REQUESTS REMARKINGS
         PREVIOUS OUPS NUMBER: 0726-080-118
         **CALLER ALSO ADVISES THAT SBC NEVER FINISHED MARKING ALL LINES FROM
         PRIOR REQUESTS.
         REPEAT MESSAGE: 2004/08/03 11:01:28 LINDA S

Contact: TODD                                                  Caller Type: C
Company: P D K  CONSTRUCTION INC.
Addr1:   34070 CREW RD
Addr2:   
City:    POMEROY                 State:  OH           Zip:  45769
Phone:   740-992-6451            Fax:   740-992-3074  
Email:   
Working For:  
Done By:  

Within:  n 39 27.0    s 39 18.5    w 81 40.5    e 81 39.0    

mailing  P D K  CONSTRUCTION INC.
         34070 CREW RD
         POMEROY, OH 45769

Members:  AGP   =ALLEGHENY POWER ALD   =WESTRN RESRVE - CTY   =COLMBIA GAS TRA 
          EOK   =DOMINION EAST O GAC   =GATHERCO INC    GTF   =VERIZON         
          GTO   =VERIZON  - COLU OBT   =SBC - ZANESVILL PIP   =COLUMBIA GAS- N 
          WAE   =WASHINGTON ELEC 
Lbps:     LHW   =LTLE HOCKING WA TCC   =CHARTER COMMUNI WCC   =WASHINGTON CTY  
          WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00005
Ticket:  0803-055-037-00  Type:  On Job        Previous Ticket: 0726-080-119-00

State :  OH  County:  WASHINGTON         Place:  DUNHAM TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  BELPRE TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and

Locate:  "S R 339" thru

 county   OH WASHINGTON
 place    BELPRE TWP         class 
Locate:  "S R 339"

Where :  LOCATE BOTH SIDES OF S R 339 FROM S R 550  IN BARLOW TWP GOING SOUTH
         THROUGH DUNHAM TWP ENDING AT S R 618 IN BELPRE TWP --TOTAL DISTANCE
         APPROX  8 MI
Subdivision:                                            Lot #  
Near St:      NO NEAR STREET GIVEN
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 10:57 AM    Dig Up Type:                    Blasting: 
Work:   GUARD RAIL INSTALLATION                                Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   1 OF 2 TICKETS
         CALLER REQUESTS REMARKINGS
         PREVIOUS OUPS NUMBER: 0726-080-119

Contact: TODD                                                  Caller Type: C
Company: P D K  CONSTRUCTION INC.
Addr1:   34070 CREW RD
Addr2:   
City:    POMEROY                 State:  OH           Zip:  45769
Phone:   740-992-6451            Fax:   740-992-3074  
Email:   
Working For:  
Done By:  

Within:  n 39 21.5    s 39 16.5    w 81 40.5    e 81 39.0    

mailing  P D K  CONSTRUCTION INC.
         34070 CREW RD
         POMEROY, OH 45769

Members:  AGP   =ALLEGHENY POWER ALD   =WESTRN RESRVE - ATBP  =AT&T            
          EOK   =DOMINION EAST O GAC   =GATHERCO INC    GTO   =VERIZON  - COLU 
          OBT   =SBC - ZANESVILL PIP   =COLUMBIA GAS- N 
Lbps:     LHW   =LTLE HOCKING WA TCC   =CHARTER COMMUNI WCC   =WASHINGTON CTY  
          WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00006
Ticket:  0803-055-037-01  Type:  On Job        Previous Ticket: 0803-055-037-00

State :  OH  County:  WASHINGTON         Place:  DUNHAM TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  BELPRE TWP
Addr  :                   Name: S R 339
Cross :                   Name:
Offset:                    and

Locate:  "S R 339" thru

 county   OH WASHINGTON
 place    BELPRE TWP         class 
Locate:  "S R 339"

Where :  LOCATE BOTH SIDES OF S R 339 FROM S R 550  IN BARLOW TWP GOING SOUTH
         THROUGH DUNHAM TWP ENDING AT S R 618 IN BELPRE TWP --TOTAL DISTANCE
         APPROX  8 MI
Subdivision:                                            Lot #  
Near St:      NO NEAR STREET GIVEN
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 10:57 AM    Dig Up Type:                    Blasting: 
Work:   GUARD RAIL INSTALLATION                                Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   1 OF 2 TICKETS
         CALLER REQUESTS REMARKINGS
         PREVIOUS OUPS NUMBER: 0726-080-119
         **CALLER ALSO ADVISES THAT SBC NEVER FINISHED MARKING ALL LINES FROM
         PRIOR REQUESTS.
         REPEAT MESSAGE: 2004/08/03 10:59:06 LINDA S

Contact: TODD                                                  Caller Type: C
Company: P D K  CONSTRUCTION INC.
Addr1:   34070 CREW RD
Addr2:   
City:    POMEROY                 State:  OH           Zip:  45769
Phone:   740-992-6451            Fax:   740-992-3074  
Email:   
Working For:  
Done By:  

Within:  n 39 21.5    s 39 16.5    w 81 40.5    e 81 39.0    

mailing  P D K  CONSTRUCTION INC.
         34070 CREW RD
         POMEROY, OH 45769

Members:  AGP   =ALLEGHENY POWER ALD   =WESTRN RESRVE - ATBP  =AT&T            
          EOK   =DOMINION EAST O GAC   =GATHERCO INC    GTO   =VERIZON  - COLU 
          OBT   =SBC - ZANESVILL PIP   =COLUMBIA GAS- N 
Lbps:     LHW   =LTLE HOCKING WA TCC   =CHARTER COMMUNI WCC   =WASHINGTON CTY  
          WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00007
Ticket:  0803-007-008-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  WATERFORD TWP
Addr  :  63               Name: ANDERSON LA
Cross :                   Name:
Offset:                    and

Locate:  63 "ANDERSON LA"

Where :  LOCATE NEXT TO GARAGE AREA IS STAKED
Subdivision:                                            Lot #  
Near St:      S R 339
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 08:00 AM    Dig Up Type:                    Blasting: 
Work:   NEW WATER LINE FROM WELL                               Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: RODNEY HUCK                                           Caller Type: C
Company: JOE HUCK INC
Addr1:   RTE 2 BOX 16
Addr2:   
City:    WATERFORD               State:  OH           Zip:  45786
Phone:   740-984-2216            Fax:   740-984-2216  
Email:   
Working For:  
Done By:  

Within:  n 39 32.5    s 39 31.5    w 81 39.0    e 81 38.5    

mailing  JOE HUCK INC
         RTE 2 BOX 16
         WATERFORD, OH 45786

Members:  AGP   =ALLEGHENY POWER GTO   =VERIZON  - COLU PIP   =COLUMBIA GAS- N 
Lbps:     
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00008
Ticket:  0803-007-116-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  LAWRENCE TWP
Addr  :  1103             Name: MOSS RUN RD
Cross :                   Name:
Offset:                    and

Locate:  1103 "MOSS RUN RD"

Where :  LOCATE FRONT OF PROPERTY
Subdivision:                                            Lot #  
Near St:      S R 26 .5 MILE FROM
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 04:00 PM    Dig Up Type:                    Blasting: 
Work:   INSTALLING WATER  SERVICE                              Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: SHAYNE MOSSER                                         Caller Type: H
Company: 
Addr1:   1103 MOSS RUN RD
Addr2:   
City:    MARIETTA                State:  OH           Zip:  45750
Phone:   740-473-2291            Fax:                 
Email:   
Working For:  
Done By:  

Within:  n 39 31.0    s 39 28.0    w 81 20.5    e 81 18.5    

mailing  SHAYNE MOSSER
         1103 MOSS RUN RD
         MARIETTA, OH 45750

Members:  AGP   =ALLEGHENY POWER CTX   =COLMBIA GAS TRA EOK   =DOMINION EAST O 
          OBT   =SBC - ZANESVILL WAE   =WASHINGTON ELEC 
Lbps:     HRW   =HIGHLND RDGE WA 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00002
Ticket:  0803-055-035-00  Type:  On Job        Previous Ticket: 0726-080-117-00

State :  OH  County:  WASHINGTON         Place:  WATERFORD TWP
Addr  :                   Name: S R 339
Cross :                   Name: CTY RD 102
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  WATERTOWN TWP
Addr  :                   Name: S R 339
Cross :                   Name: TWP RD 1319
Offset:                    and

Locate:  "S R 339" between "CTY RD 102" and

 county   OH WASHINGTON
 place    WATERTOWN TWP      class 
Locate:  "TWP RD 1319"

Where :  LOCATE BOTH SIDES OF S R 339
Subdivision:                                            Lot #  
Near St:      
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/03/2004 10:55 AM    Dig Up Type:                    Blasting: 
Work:   INSTALL GUARD RAIL                                     Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   CALLER REQUESTS REMARKINGS
         PREVIOUS OUPS NUMBER: 0726-080-117

Contact: TODD                                                  Caller Type: C
Company: P D K  CONSTRUCTION INC.
Addr1:   34070 CREW RD
Addr2:   
City:    POMEROY                 State:  OH           Zip:  45769
Phone:   740-992-6451            Fax:   740-992-3074  
Email:   
Working For:  
Done By:  

Within:  n 39 33.0    s 39 27.0    w 81 40.0    e 81 37.5    

mailing  P D K  CONSTRUCTION INC.
         34070 CREW RD
         POMEROY, OH 45769

Members:  AGP   =ALLEGHENY POWER CTY   =COLMBIA GAS TRA EOK   =DOMINION EAST O 
          GAC   =GATHERCO INC    GTO   =VERIZON  - COLU OIL   =OH OIL GATHERIN 
          OPW   =OHIO POWER      PIP   =COLUMBIA GAS- N TCY   =TRI CTY WAT/SEW 
          WAE   =WASHINGTON ELEC 
Lbps:     LHW   =LTLE HOCKING WA WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00010
Ticket:  0803-028-060-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  WATERTOWN TWP
Addr  :  3790             Name: WATERTOWN RD
Cross :                   Name: TWP RD 107
Offset:  1.2 MI N          and  400 FT E          

Locate:  3790 "WATERTOWN RD" at ~1.2 MI N, 400 FT E of "TWP RD 107"

Where :  LOCATE WATERTOWN RD  AKA  CTY RD  109
         EAST SIDE OF THE STREET  --  400  FT EAST BACK  OFF ROAD
         LOCATIONS ARE  STAKED  - WHITE  RIBBON
Subdivision:                                            Lot #  
Near St:      TWP RD  107 AKA  KERN  --  1.2 MILE  NORTH
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 10:30 AM    Dig Up Type:                    Blasting: 
Work:   INSTALL  1  NEW  POLE  --  3  ANCHORS                  Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: CRAIG HOLMES                                          Caller Type: M
Company: ALLEGHENY POWER
Addr1:   1010 GREEN ST
Addr2:   
City:    MARIETTA                State:  OH           Zip:  45750
Phone:   740-568-2808            Fax:   740-568-2807  
Email:   
Working For:  
Done By:  

Within:  n 39 30.5    s 39 29.0    w 81 38.0    e 81 34.5    

mailing  ALLEGHENY POWER
         1010 GREEN ST
         MARIETTA, OH 45750

Members:  AGP   =ALLEGHENY POWER BLK   =BELDEN & BLAKE  CTY   =COLMBIA GAS TRA 
          EOI   =DOMINION EAST O GTO   =VERIZON  - COLU TCY   =TRI CTY WAT/SEW 
          WAE   =WASHINGTON ELEC 
Lbps:     LHW   =LTLE HOCKING WA WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00012
Ticket:  0803-044-008-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  WARREN TWP
Addr  :                   Name: CTY RD 126
Cross :                   Name: S R 676
Offset:  .1 MI W           and  

Locate:  ~.1 MI W of "CTY RD 126" and "S R 676"

Where :  LOCATE N/S OF C R 126 - AREA  MARKED
Subdivision:                                            Lot #  
Near St:      
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 09:00 AM    Dig Up Type:                    Blasting: 
Work:   SETTING 1 POLE                                         Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: DAVID HACK                                            Caller Type: M
Company: WASHINGTON ELECTRIC
Addr1:   406 COLGATE DR
Addr2:   
City:    MARIETTA                State:  OH           Zip:  45750
Phone:   740-373-2141            Fax:   740-373-2941  
Email:   
Working For:  
Done By:  

Within:  n 39 26.5    s 39 26.0    w 81 31.5    e 81 30.5    

mailing  WASHINGTON ELECTRIC
         406 COLGATE DR
         MARIETTA, OH 45750

Members:  AGP   =ALLEGHENY POWER EOK   =DOMINION EAST O GTO   =VERIZON  - COLU 
          OBT   =SBC - ZANESVILL WAE   =WASHINGTON ELEC 
Lbps:     TCC   =CHARTER COMMUNI WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00013
Ticket:  0803-044-082-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  AURELIUS TWP
Addr  :                   Name: TWP RD 462
Cross :                   Name:
Offset:                    and

Locate:  "TWP RD 462"

Where :  LOCATE BOTH SIDES OF T R 462 - STARTING IN FRONT OF ADDR 249 T R 462 &
         GOING EAST TO INTER OF T R 300 & T R 462
Subdivision:                                            Lot #  
Near St:      NO NEAR STREET GIVEN
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 03:30 PM    Dig Up Type:                    Blasting: 
Work:   BURYING PHONE CABLE                                    Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: CHARLIE                                               Caller Type: C
Company: J-TEC COMMUNICATIONS SERVICE
Addr1:   1210 CHENEY
Addr2:   
City:    MARION                  State:  OH           Zip:  43302
Phone:   740-432-2566            Fax:   740-382-8755  
Email:   
Working For:  VERIZON
Done By:  

Within:  n 39 39.0    s 39 38.5    w 81 28.0    e 81 27.0    

mailing  J-TEC COMMUNICATIONS SERVICE
         1210 CHENEY
         MARION, OH 43302

Members:  AGP   =ALLEGHENY POWER GTO   =VERIZON  - COLU NGO   =NATIONAL GAS&OI 
          WAE   =WASHINGTON ELEC 
Lbps:     PWC   =PURE WATER CO   
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00014
Ticket:  0803-044-083-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  AURELIUS TWP
Addr  :                   Name: TWP RD 300
Cross :                   Name: TWP RD 462
Offset:                    and
State :  OH  County:  WASHINGTON         Place:  AURELIUS TWP
Addr  :                   Name: TWP RD 300
Cross :                   Name: TWP RD 462
Offset:  1500 FT N         and  

Locate:  "TWP RD 300" between "TWP RD 462" and ~1500 FT N of "TWP RD 462"

Where :  LOCATE E/S OF T R 300
Subdivision:                                            Lot #  
Near St:      
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 03:30 PM    Dig Up Type:                    Blasting: 
Work:   BURYING PHONE CABLE                                    Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: CHARLIE                                               Caller Type: C
Company: J-TEC COMMUNICATIONS SERVICE
Addr1:   1210 CHENEY
Addr2:   
City:    MARION                  State:  OH           Zip:  43302
Phone:   740-432-2566            Fax:   740-382-8755  
Email:   
Working For:  VERIZON
Done By:  

Within:  n 39 39.0    s 39 38.5    w 81 28.0    e 81 27.0    

mailing  J-TEC COMMUNICATIONS SERVICE
         1210 CHENEY
         MARION, OH 43302

Members:  AGP   =ALLEGHENY POWER GTO   =VERIZON  - COLU NGO   =NATIONAL GAS&OI 
          WAE   =WASHINGTON ELEC 
Lbps:     PWC   =PURE WATER CO   
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00009
Ticket:  0803-022-009-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  ATHENS             Place:  ROME TWP
Addr  :  6825             Name: FROST HILL RD
Cross :                   Name:
Offset:                    and

Locate:  6825 "FROST HILL RD"

Where :  LOCATE ALL SIDES OF PROPERTY
         COULD BE DIGGING MORE THAN 200 FT BUT LESS THAN .25 MILE BACK OFF RD
Subdivision:                                            Lot #  
Near St:      CTY RD 26 AND S R 144
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 09:45 AM    Dig Up Type:                    Blasting: 
Work:   FENCE INSTALLATION & BULLDOZER WORK                    Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: ELLEN GRAMLING                                        Caller Type: H
Company: 
Addr1:   6825 FROST HILL RD
Addr2:   
City:    COOLVILLE               State:  OH           Zip:  45723
Phone:   740-667-7397            Fax:                 
Email:   
Working For:  
Done By:  

Within:  n 39 17.5    s 39 16.0    w 81 50.5    e 81 48.5    

mailing  ELLEN GRAMLING
         6825 FROST HILL RD
         COOLVILLE, OH 45723

Members:  AGP   =ALLEGHENY POWER ALD   =WESTRN RESRVE - ATBP  =AT&T            
          CGG   =COLUMBIA GAS -  CSP   =COLUMBUS SOUTHE GAC   =GATHERCO INC    
Lbps:     LHW   =LTLE HOCKING WA TUP   =TUPPERS PLAINS  
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00016
Ticket:  0803-076-079-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  BARLOW TWP
Addr  :  771              Name: BRACKENRIDGE RD
Cross :                   Name:
Offset:                    and

Locate:  771 "BRACKENRIDGE RD"

Where :  LOCATE ENTIRE PROPERTY AND B/S OF RD  PED 130  REF 7406782813
Subdivision:                                            Lot #  
Near St:      S R 339
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 07:45 PM    Dig Up Type:                    Blasting: 
Work:   BURY PHONE LINES                                       Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: NINA WILLIAMS                                         Caller Type: C
Company: BUTLER TELECOM
Addr1:   6615 BROTHERHOOD WY
Addr2:   
City:    FT WAYNE                State:  IN           Zip:  46825
Phone:   888-427-5117            Fax:   219-485-5624  
Email:   
Working For:  
Done By:  

Within:  n 39 23.0    s 39 21.0    w 81 40.5    e 81 39.5    

mailing  BUTLER TELECOM
         6615 BROTHERHOOD WY
         FT WAYNE, IN 46825

Members:  AGP   =ALLEGHENY POWER EOK   =DOMINION EAST O GAC   =GATHERCO INC    
          GTO   =VERIZON  - COLU PIP   =COLUMBIA GAS- N 
Lbps:     LHW   =LTLE HOCKING WA WCC   =WASHINGTON CTY  WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00011
Ticket:  0803-028-103-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  WARREN TWP
Addr  :  800              Name: LANG FARM RD
Cross :                   Name: S R 676
Offset:  .8 MI N           and  

Locate:  800 "LANG FARM RD" at ~.8 MI N of "S R 676"

Where :  LOCATE FRONT OF  PROPERTY AND BOTH SIDES OF THE  STREET
         LANG  FARM AKA TWP RD  184
Subdivision:                                            Lot #  
Near St:      S R  676 --  .8 MILE  NORTH  OF
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 02:30 PM    Dig Up Type:                    Blasting: 
Work:   REPAIR WATER  LEAK                                     Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   PLEASE MARK   SOONER  IF  POSSIBLE

Contact: DENNIS REZABEK                                        Caller Type: C
Company: WARREN COMMUNITY WATER ASSOC
Addr1:   RT 4 BOX 120
Addr2:   
City:    MARIETTA                State:  OH           Zip:  45750
Phone:   740-373-8476            Fax:   740-373-8476  
Email:   
Working For:  
Done By:  

Within:  n 39 27.5    s 39 26.5    w 81 32.5    e 81 30.0    

mailing  WARREN COMMUNITY WATER ASSOC
         RT 4 BOX 120
         MARIETTA, OH 45750

Members:  AGP   =ALLEGHENY POWER CTY   =COLMBIA GAS TRA EOK   =DOMINION EAST O 
          GTO   =VERIZON  - COLU OBT   =SBC - ZANESVILL WAE   =WASHINGTON ELEC 
Lbps:     TCC   =CHARTER COMMUNI WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00017
Ticket:  0803-080-013-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  NOBLE              Place:  OLIVE TWP
Addr  :  17421            Name: CTY RD 40
Cross :                   Name: CTY RD 7
Offset:  .25 MI N          and  

Locate:  17421 "CTY RD 40" at ~.25 MI N of "CTY RD 7"

Where :  LOCATE FRONT AND LEFT SIDE OF PROPERTY - FACING FRONT
         LEFT OF PROPERTY (FACING HOUSE ) THREE GAS METERS - GOING FROM THE GAS
         METERS PARALLEL THE RD UP OVER THE HILL TO THE HOUSE
Subdivision:                                            Lot #  
Near St:      
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 09:00 AM    Dig Up Type:                    Blasting: 
Work:   LAY GAS LINE                                           Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   CALLER ADVISES ESPECIALLY CONCERNED ABOUT  HOUSE  LINES -- IN AREA

Contact: STANLEY BATTEN                                        Caller Type: H
Company: 
Addr1:   17421 CTY RD 40
Addr2:   
City:    CALDWELL                State:  OH           Zip:  43724
Phone:   740-732-2237            Fax:                 
Email:   
Working For:  
Done By:  

Within:  n 39 43.5    s 39 42.5    w 81 31.5    e 81 30.5    

mailing  STANLEY BATTEN
         17421 CTY RD 40
         CALDWELL, OH 43724

Members:  AGP   =ALLEGHENY POWER CBC   =CABLEVISION COM EOI   =DOMINION EAST O 
          GTO   =VERIZON  - COLU NGO   =NATIONAL GAS&OI OPW   =OHIO POWER      
          WAE   =WASHINGTON ELEC 
Lbps:     CWC   =CLEAR WATER COR FCM   =ADELPHIA/ FRONT PWC   =PURE WATER CO   
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00018
Ticket:  0803-500-670-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  WASHINGTON         Place:  WARREN TWP
Addr  :                   Name: S R 550
Cross :                   Name:
Offset:                    and

Locate:  "S R 550"

Where :  LOCATE BOTH SIDES OF STREET 20.9MP
Subdivision:                                            Lot #  
Near St:      twp 273 a.k.a. dodd run road 500' west of
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 12:15 PM    Dig Up Type:                    Blasting: 
Work:   DRAINAGE                                               Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   AREA IS MARKED WITH WHITE PAINT.

Contact: JASON BROWNRIGG                                       Caller Type: C
Company: OHIO DEPT OF TRANSPORATION
Addr1:   1650 GREEN ST
Addr2:   
City:    MARIETTA                State:  OH           Zip:  45750
Phone:   740-373-0536            Fax:   740-373-7971  Cell Phone:  740-525-8076
Email:   
Working For:  
Done By:  

Within:  n 39 24.5    s 39 23.0    w 81 35.5    e 81 28.5    

mailing  OHIO DEPT OF TRANSPORATION
         1650 GREEN ST
         MARIETTA, OH 45750

Members:  AGP   =ALLEGHENY POWER CMR   =MARIETTA CITY   CTY   =COLMBIA GAS TRA 
          EOI   =DOMINION EAST O EOK   =DOMINION EAST O OBT   =SBC - ZANESVILL 
          WAE   =WASHINGTON ELEC 
Lbps:     TCC   =CHARTER COMMUNI WCW   =WARREN COMMUNIT 
eom


Dig Request from OUPS for: AGP     to: ~EMAIL_E xmit: 08/16/2004 03:30 PM 00015
Ticket:  0803-055-016-00  Type:  Regular       Previous Ticket: 

State :  OH  County:  MONROE             Place:  JACKSON TWP
Addr  :  35855            Name: S R 7
Cross :                   Name:
Offset:                    and

Locate:  35855 "S R 7"

Where :  LOCATE FRONT AND BOTH SIDES OF STREET
Subdivision:                                            Lot #  
Near St:      CTY RD 11 50FT N OF
On RR right of way: N      Milemarker: at/from 
Highway: N                 Milemarker: at/from 

Begin:  08/05/2004 09:30 AM    Dig Up Type:                    Blasting: 
Work:   RD BORE GOING APROX 6FT DEEP & INSTALL GAS LINE        Pre Markings: 
Means of Excavation:                                           Meet: 

Notes:   

Contact: DEBBIE STALEY                                         Caller Type: C
Company: R & R PIPELINE
Addr1:   155 DAYTON RD
Addr2:   
City:    NEWARK                  State:  OH           Zip:  43055
Phone:   740-345-3692            Fax:   740-349-1987  
Email:   
Working For:  
Done By:  

Within:  n 39 36.5    s 39 32.5    w 81 02.5    e 80 56.5    

mailing  R & R PIPELINE
         155 DAYTON RD
         NEWARK, OH 43055

Members:  AGP   =ALLEGHENY POWER CST   =CONSTITUTN GAS  EOK   =DOMINION EAST O 
          LEE   =OHIO & LEE TWP  MWD   =MONROE WATER SY NIE   =NORTHN INDUSTRI 
          OBT   =SBC - ZANESVILL WAE   =WASHINGTON ELEC 
Lbps:     
eom


