
ZZZ    00424 POCS 03/20/07 11:49:05 0795927-000 NEW  XCAV EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795927]-[000] Channel#--[1144028][0165]

Message Type--[NEW][EXCAVATION][EMERGENCY]

County--[ALLEGHENY]       Municipality--[SPRINGDALE BORO]
Work Site--[RAILROAD ST]
     Nearest Intersection--[HOEVELER ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [WORKING AT THE INTER. MARK FOR 100FT TOWARDS WALTERS WAY]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.540487/-79.773561,40.538909/-79.775242]

Type of Work--[REPAIR GAS LEAK]                              Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[TW PHILLIPS]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[20-MAR-07] Dig Time--[1200] Duration--[]

Contractor--[TW PHILLIPS GAS & OIL COMPANY]  Homeowner/Business--[B]
Address--[6085 FREEPORT RD]
City--[NATRONA HEIGHTS]              State--[PA] Zip--[15065]

Caller--[BARB KOWALSKI]                    Phone--[724-295-2600] Ext--[]
FAX--[724-295-3688]  Email--[none]
Person to Contact--[JOHN ZNACZKO]              Phone--[724-822-4914] Ext--[]
Best Time to Call--[0730-1630]

Prepared--[20-MAR-07] at [1146] by [DARLENE SPANO]

Remarks--
     [CREW EN ROUTE.]

BD 0  BD =VERIZON PA INC   DC 0  DC =DUQ LIGHT PGH    DTM0  DTM=ELANTIC NETWORK
EA 0  EA =EQU GAS CENTL    IC 0  IC =BUCKEYE PT CRPL  JB 0  JB =DOMINION TRANSM
PX 0  PX =DOM PEOPLES      SPR0  SPR=SPRINGDALE THE   VD 0  JUL=ALLEG PWR VDGT
WM 0  WM =COMCAST CABLE    WP 0  WP =TW PHILLIPS      ZZZ0  ZZZ=*

Serial Number--[0795927]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========


ZZZ    00425 POCS 03/20/07 11:49:05 0795928-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795928]-[000] Channel#--[1144027][0202]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[ALLEGHENY]       Municipality--[SPRINGDALE BORO]
Work Site--[HOEVELER ST]
     Nearest Intersection--[PITTSBURGH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING IN THE INTER. PLEASE MARK THE ENTIRE INTER AND 75FT S OF THE INTER
      ALONG HOEVELER ST.]
     Caller Lat/Lon--[]
     Mapped Type--[X] Mapped Lat/Lon--
     [40.541916/-79.775088]

Type of Work--[INSTL NEW WTR LINE]                           Depth--[5FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[SPRINGDALE BORO]

               Lawful Start Dates--[         ] Through [         ]
Proposed Dig Date--[DESIGN    ]

Contractor--[SPRINGDALE BORO WTR DEPT]       Homeowner/Business--[B]
Address--[PO BOX 153]
City--[SPRINGDALE]                   State--[PA] Zip--[15144-0153]

Caller--[GARY TAYLOR]                      Phone--[724-274-6800] Ext--[]
FAX--[724-274-4477]  Email--[none]
Person to Contact--[GARY TAYLOR]               Phone--[724-274-6800] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-MAR-07] at [1147] by [KELLY CRAWFORD]

Remarks--
     [CALLER IS REQUESTING MAPS, PLANS & VERBAL CONFIRMATION]

BD 0  BD =VERIZON PA INC   DC 0  DC =DUQ LIGHT PGH    DTM0  DTM=ELANTIC NETWORK
EA 0  EA =EQU GAS CENTL    IC 0  IC =BUCKEYE PT CRPL  PX 0  PX =DOM PEOPLES
SPR0  SPR=SPRINGDALE THE   VD 0  JUL=ALLEG PWR VDGT   WM 0  WM =COMCAST CABLE
WP 0  WP =TW PHILLIPS      ZZZ0  ZZZ=*                PXD0  PXD=DOM PEOPLES

Serial Number--[0795928]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========


ZZZ    00426 POCS 03/20/07 11:49:06 0795934-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795934]-[000] Channel#--[1147027][0057]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[ALLEGHENY]       Municipality--[SPRINGDALE BORO]
Work Site--[ORCHARD ST]
     Nearest Intersection--[PITTSBURGH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING IN THE INTER. PLEASE MARK THE ENTIRE INTER AND 75FT N OF THE INTER
      ALONG ORCHARD ST.]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.541952/-79.774744,40.542358/-79.775032]

Type of Work--[INSTL NEW WTR LINE]                           Depth--[5FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[SPRINGDALE BORO]

               Lawful Start Dates--[         ] Through [         ]
Proposed Dig Date--[DESIGN    ]

Contractor--[SPRINGDALE BORO WTR DEPT]       Homeowner/Business--[B]
Address--[PO BOX 153]
City--[SPRINGDALE]                   State--[PA] Zip--[15144-0153]

Caller--[GARY TAYLOR]                      Phone--[724-274-6800] Ext--[]
FAX--[724-274-4477]  Email--[none]
Person to Contact--[GARY TAYLOR]               Phone--[724-274-6800] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-MAR-07] at [1148] by [KELLY CRAWFORD]

Remarks--
     [CALLER IS REQUESTING MAPS, PLANS & VERBAL CONFIRMATION]

BD 0  BD =VERIZON PA INC   DC 0  DC =DUQ LIGHT PGH    DTM0  DTM=ELANTIC NETWORK
EA 0  EA =EQU GAS CENTL    IC 0  IC =BUCKEYE PT CRPL  PX 0  PX =DOM PEOPLES
SPR0  SPR=SPRINGDALE THE   VD 0  JUL=ALLEG PWR VDGT   WM 0  WM =COMCAST CABLE
WP 0  WP =TW PHILLIPS      ZZZ0  ZZZ=*                PXD0  PXD=DOM PEOPLES

Serial Number--[0795934]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========


ZZZ    00003 POCS 03/20/07 11:49:09 0665951-000 NEW  XCAV DSGN RSND

======================PENNSYLVANIA ONE CALL LOCATE REQUEST======================

Serial Number--[0665951]-[000] Channel#--[1329WEB][0041]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[DELAWARE]        Municipality--[MIDDLETOWN TWP]
Scope of Work--
     [DEER RUN RD, HUNTING HILLS]
Location Information--
     []
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.925302/-75.426432,39.926062/-75.427942,39.925590/-75.428823,
      39.925456/-75.428667,39.925769/-75.427961,39.925168/-75.426523]
     Map Graphic--[http://www.pa1call.org/graphics/find.aspx?n=0665951]

Type of Work--[CONDUIT VERIZON FTTP]

               Lawful Start Dates--[         ] Through--[         ]
                 Scheduled Excavation Date--[PRELIMINARY DESIGN]

Caller--[PAUL GEORGE]                      Phone--[781-769-0493] Ext--[120]
Excavator--[UTILITY CONSULTANTS INC]        Homeowner/Business--[B]
Address--[1418 BOSTON-PROVIDENCE HWY]
City--[NORWOOD]                      State--[MA] Zip--[02062]
FAX--[781-769-7120]  Email--[PGEORGE@UCINC.NET]
Project Owner--[VERIZON FTTP]

Person to Contact--[PAUL GEORGE]               Phone--[781-769-0493] Ext--[120]
Best Time to Call--[0800-1700]

Prepared--[07-MAR-07] at [1330] by [PAUL GEORGE]

Job Number--[JOB 12345678901]
Remarks--
     [REQUEST PLANS BE SENT DESIGN ONLY JOB MEDIA 4010]

ACW0  ACW=CHESTER WTR AUT  CR 0  CR =CHESTER WA       HT 0  HT =AQUA PA INC
IB 0  IB =BUCKEYE PT BWYN  JY 0  JY =COMCAST CABLE W  KE 0  KE =PECO MRTN
MD10  MD1=MIDDLETOWN TSA   SP 0  SP =SUNOCO PIPELINE  TX 0  TX =SPECTRA ENERGY
YI 0  YI =VERIZON HRSM

Serial Number--[0665951]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========

[Originally sent as sequence number 00027 at 03/07/2007 18:17]


ZZZ    00427 POCS 03/20/07 11:49:48 0795939-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795939]-[000] Channel#--[1148027][0050]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[ALLEGHENY]       Municipality--[SPRINGDALE BORO]
Work Site--[HARTREY ST]
     Nearest Intersection--[PITTSBURGH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING IN THE INTER. PLEASE MARK THE ENTIRE INTER AND 75FT S OF THE INTER
      ALONG HARTREY ST.]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.542058/-79.774467,40.541637/-79.774143]

Type of Work--[INSTL NEW WTR LINE]                           Depth--[5FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[SPRINGDALE BORO]

               Lawful Start Dates--[         ] Through [         ]
Proposed Dig Date--[DESIGN    ]

Contractor--[SPRINGDALE BORO WTR DEPT]       Homeowner/Business--[B]
Address--[PO BOX 153]
City--[SPRINGDALE]                   State--[PA] Zip--[15144-0153]

Caller--[GARY TAYLOR]                      Phone--[724-274-6800] Ext--[]
FAX--[724-274-4477]  Email--[none]
Person to Contact--[GARY TAYLOR]               Phone--[724-274-6800] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-MAR-07] at [1149] by [KELLY CRAWFORD]

Remarks--
     [CALLER IS REQUESTING MAPS, PLANS & VERBAL CONFIRMATION]

BD 0  BD =VERIZON PA INC   DC 0  DC =DUQ LIGHT PGH    DTM0  DTM=ELANTIC NETWORK
EA 0  EA =EQU GAS CENTL    IC 0  IC =BUCKEYE PT CRPL  JB 0  JB =DOMINION TRANSM
PX 0  PX =DOM PEOPLES      SPR0  SPR=SPRINGDALE THE   VD 0  JUL=ALLEG PWR VDGT
WM 0  WM =COMCAST CABLE    WP 0  WP =TW PHILLIPS      ZZZ0  ZZZ=*
PXD0  PXD=DOM PEOPLES

Serial Number--[0795939]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========


ZZZ    00428 POCS 03/20/07 11:50:18 0795940-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795940]-[000] Channel#--[1149027][0039]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[ALLEGHENY]       Municipality--[SPRINGDALE BORO]
Work Site--[JAMES ST]
     Nearest Intersection--[PITTSBURGH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING IN THE INTER. PLEASE MARK THE ENTIRE INTER AND 75FT S OF THE INTER
      ALONG JAMES ST.]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.541218/-79.780032,40.540631/-79.779961]

Type of Work--[INSTL NEW WTR LINE]                           Depth--[5FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[SPRINGDALE BORO]

               Lawful Start Dates--[         ] Through [         ]
Proposed Dig Date--[DESIGN    ]

Contractor--[SPRINGDALE BORO WTR DEPT]       Homeowner/Business--[B]
Address--[PO BOX 153]
City--[SPRINGDALE]                   State--[PA] Zip--[15144-0153]

Caller--[GARY TAYLOR]                      Phone--[724-274-6800] Ext--[]
FAX--[724-274-4477]  Email--[none]
Person to Contact--[GARY TAYLOR]               Phone--[724-274-6800] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-MAR-07] at [1150] by [KELLY CRAWFORD]

Remarks--
     [CALLER IS REQUESTING MAPS, PLANS & VERBAL CONFIRMATION]

BD 0  BD =VERIZON PA INC   DC 0  DC =DUQ LIGHT PGH    EA 0  EA =EQU GAS CENTL
IC 0  IC =BUCKEYE PT CRPL  SPR0  SPR=SPRINGDALE THE   VD 0  JUL=ALLEG PWR VDGT
WM 0  WM =COMCAST CABLE    WP 0  WP =TW PHILLIPS      ZZZ0  ZZZ=*

Serial Number--[0795940]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========


ZZZ    00429 POCS 03/20/07 11:50:47 0795943-000 NEW  XCAV EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[0795943]-[000] Channel#--[1147004][0166]

Message Type--[NEW][EXCAVATION][EMERGENCY]

County--[ALLEGHENY]       Municipality--[BETHEL PARK BORO]
Work Site--[2994 INDUSTRIAL BLVD]
     Nearest Intersection--[PROGRESS BLVD]
     Second Intersection--[LENTO BLVD]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     []
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [40.326277/-80.043598,40.328593/-80.038833,40.326716/-80.037629,
      40.324041/-80.042970]

Type of Work--[REPAIR GAS LEAK]                              Depth--[3FT]
Extent of Excavation--[5FT X 5FT]       Method of Excavation--[BH,HAND]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[COLUMBIA GAS]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[20-MAR-07] Dig Time--[1200] Duration--[1 DAY]

Contractor--[COLUMBIA GAS]                   Homeowner/Business--[B]
Address--[251 W MAIDEN ST]
City--[WASHINGTON]                   State--[PA] Zip--[15301]

Caller--[BEN HARDY]                        Phone--[724-250-2604] Ext--[]
FAX--[724-250-2627]  Email--[bhardy@nisource.com]
Person to Contact--[BEN HARDY]                 Phone--[724-250-2604] Ext--[]
Best Time to Call--[0800-1600]

Prepared--[20-MAR-07] at [1150] by [KATHY ULOKOVIC]

Remarks--
     [SVC TECH ON SITE, CREW EN ROUTE. HEATH CONSULTANTS NEED NOT RESPOND.]

BD 0  BD =VERIZON PA INC   BPB0  BPB=BETHEL PARK MUN  CE 0  CE =COLUMBIA GAS PA
DC 0  DC =DUQ LIGHT PGH    TV 0  TV =COMCAST BLPK     WE 0  WE =PAWC BETHELPARK
WZ 0  JUL=ALLEG PWR WASH   EM40  EM4=COLUMBIA GAS PA  ZZZ0  ZZZ=*
WG40  WG4=PAWC AFT HR-WG

Serial Number--[0795943]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========

KF     00163 POCS 06/05/07 16:09:46 1567521-000 UPDT XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[1567521]-[000] Channel#--[1608WEB][0069]

Message Type--[UPDATE][EXCAVATION][ROUTINE]

County--[CHESTER]         Municipality--[NEW LONDON TWP]
Work Site--[CREEK ROAD]
     Nearest Intersection--[THUNDER HILL]
     Second Intersection--[]
     Subdivision--[WYNSTONE]                      Site Marked in White--[N]
     Location Information--
     [FROM ABOVE INTERSECTION APPX. 700 FT NORTH EAST MARK ACROSS THE FIELD TO
      NORTH DEER RUN DR AND SOUTH EAST 2500 FT.   CHESTER CO ADC MAP PAGE 3908
      GRID J9 K9.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.760240/-75.885518,39.763730/-75.882452,39.760055/-75.878272,
      39.758368/-75.882422,39.760124/-75.885428,39.760217/-75.885428]

Type of Work--[WILKINSON]                                    Depth--[50 FT]
Extent of Excavation--[]                Method of Excavation--[TRENCHING]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[X] Other--[]
Owner/Work Being Done for--[EARTHMOVING INSTALL PIPE]

               Lawful Start Dates--[08-JUN-07] Through [19-JUN-07]
    Scheduled Excavation Date--[08-JUN-07] Dig Time--[0700] Duration--[]

Contractor--[TECHNIVATE INC]                 Homeowner/Business--[B]
Address--[1020 BROAD RUN RD]
City--[LANDENBERG]                   State--[PA] Zip--[19350]

Caller--[CHRISTINE SIMPSON]                Phone--[610-274-8009] Ext--[]
FAX--[610-274-3435]  Email--[cmsimpson@technivate.com]
Person to Contact--[MIKE FRY]                  Phone--[302-363-5329] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[05-JUN-07] at [1609] by [CHRISTINE SIMPSON]

Remarks--
     [=== UPDATE 1386482-000 --6/5/2007 1608 CHRISTINES WEB===
      UPDATE REQUESTED BY: CHRISTINE SIMPSON
      UPDATE AND REMARK LINES.   ANY QUESTIONS PLEASE CONTACT MIKE.   THANKS
      REMARK LINES.]

ACW0  ACW=CHESTER WTR AUT  CR 0  CR =CHESTER WA       JZ 0  JZ =COMCAST CABLE
KF 0  KF =PECO CTVL        YJ 0  YJ =VERIZON

Serial Number--[1567521]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========

