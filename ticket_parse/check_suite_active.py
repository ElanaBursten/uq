# check_suite_active.py
# Returns exit code 1 if any programs of the UQ suite are active, and 0
# otherwise. These programs include main.py, ticketrouter.py, etc.
# For usage in batch file (or other tools that can read the exit code).
#
# TODO: Rewrite this in more high-level terms, so identical code can be used
# for the IronPython version.

import sys
#
import mutex

# todo(dan) These probably need a biz id to be useful
MUTEX_PREFIXES = [
 "main.py:",
 "ticketrouter:",
 #"receiver.py",
 #"IRTHresponder.py"
]

if __name__ == "__main__":
    mutexes = mutex.locked_mutexes(dbinterface_ado.DBInterfaceADO())
    for mutex_prefix in MUTEX_PREFIXES:
        for mutx in mutexes:
            if mutx.startswith(mutex_prefix):
                print mutx, "is active"
                sys.exit(1)

    # apparently none of these were active
    print "no processes active"
    sys.exit(0)
