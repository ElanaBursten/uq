# abstractresponderdata.py

import string
from xml.sax.saxutils import escape
import xml.etree.ElementTree as ET

def clean_notes(notes_raw):
    notes = string.join(notes_raw, '\n')

    # convert > and < so we won't break the XML
    notes = escape(notes)

    if len(notes) > 250:
        # truncate to 240 and append message that indicates that there is more
        notes = notes[0:239]+"(More ...)\n"
        try:
            ET.fromstring('<note>' + notes + '</note>')
        except:
            # By truncating, we've munged up the escapes
            # back up to the last ; and get rid of the rest
            pos = notes.rfind(';')
            notes = notes[0:pos+1] + "(More ...)\n"
    return notes

class AbstractResponderData(object):
    queue = {'table': 'responder_queue'}

    # XXX note: these days, 'skipping' means to delete a record from queue
    # without processing it... this is bypassing the <skip> block, so use with
    # great caution!
    def do_skip(self, row):
        """ Return True if we skip this term id.  Override in subclasses
            in case we only want to process certain term ids, or when the
            set of terms we (don't) process is defined by complex rules. """
        # NOTE: separate from the <skip> tag in config.xml!
        return False

    def get_ticket_notes(self):
        """ Extract notes from ticket and locates for response. """
        # get ticket_id
        ticket_id = self.row['ticket_id']
        locate_id = self.row['locate_id']

        # then get notes for that ticket_id (including locate notes)
        notes_raw = self.tdb.get_all_notes(ticket_id, locate_id)
        if notes_raw:
            notes = clean_notes(notes_raw)
        else:
            notes = "Response by Utiliquest"
        return notes

    def get_raw_locate_notes(self, ticket_id, locate_id,
     default_notes=["Response by Utiliquest"]):
        """ Return list of locate notes. """
        return self.tdb.get_locate_notes(ticket_id, locate_id) or default_notes
        
    def get_locate_notes(self):
        """ Extract notes from locates for response. NOTE: Expects self.row to
            be set to the relevant response_queue record. """
        # get ticket_id
        ticket_id = self.row['ticket_id']
        locate_id = self.row['locate_id']

        # then get notes for the locate notes)
        notes_raw = self.tdb.get_locate_notes(ticket_id, locate_id)
        if notes_raw:
            notes = clean_notes(notes_raw)
        else:
            notes = "Response by Utiliquest"
        return notes

    #@staticmethod
    def check_notes(self, notes):
        """ Verify that the notes are valid xml. """
        try:
            ET.fromstring('<note>'+notes+'</note>')
        except:
            notes = escape(notes)
            # If the following raises an exception, it will be handled elsewhere
            ET.fromstring('<note>'+notes+'</note>')
        return notes

    def map_status(self, status):
        """ Override to implement custom status mappings that cannot be
            handled by the mappings in config.xml. Return response code and
            explanation; return None if there is no mapping. """
        return None 

