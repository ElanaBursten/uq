# ticketextractor.py

from __future__ import with_statement
import ftplib
import getopt
import os
import re
import string
import sys
import traceback
import xml.etree.ElementTree as ET
#
import config
import datadir
import date
import dbinterface_ado
import emailtools
import errorhandling2
import errorhandling_special
import et_tools
import mutex
import recognize

__usage__ = """\
%s [options] [arguments]

Find new tickets in certain files (see te_config.xml), write them to a target
directory, and upload them.

Options:
    -u                  Do not upload automatically.
    -t filename dir     Test with test filename and target dir.
    -h date             Generate a header file for the given date.
    -m header processed Output missing files by comparing a header file and a
                        processed file.
    -r processed date   "Repair" missing tickets that are not in the processed
                        file, for the given date.

""" % (os.path.split(sys.argv[0])[1])

def get_ticket_format(rawticket):
    for format in ("Colorado", "Illinois", "SouthCalifornia"):
        t = recognize.recognize_as(rawticket, format)
        if t:
            return format
    return ""

class TicketExtractorError(Exception): pass

def U(u):
    """ Simple Unicode-to-utf8 decoder. """
    if isinstance(u, unicode):
        return u.encode("utf-8")
    else:
        return u

class PositionDict(dict):
    def save(self, filename):
        f = open(filename, "w")
        items = self.items()
        items.sort()
        for key, value in items:
            print >> f, key, value
        f.close()
    def load(self, filename):
        self.clear()
        f = open(filename, "r")
        for line in f.readlines():
            if not line.strip():
                continue    # ignore empty lines
            parts = line.split()
            key, value = parts[0], int(parts[1])
            self.__setitem__(key, value)

PROCESS_NAME = "ticketextractor"

class TicketExtractor:
    """ Extract tickets from a file that grows (is appended to) over time. """

    positions_file = "positions.txt"
    ids_file = "ids.txt"
    config_file = "te_config.xml"

    prefix = "FCO1"
    chunksize = 10  # number of tickets grouped per output file

    def __init__(self, begin_marker, end_marker, upload=1, testmode=0,
                 verbose=1):

        self.begin_marker = begin_marker
        self.end_marker = end_marker

        self.upload = upload
        self.testmode = testmode
        self.verbose = verbose

        self.read_configuration(self.config_file)

        # create and configure the logger
        smtpinfo = emailtools.SMTPInfo(self.smtp_host, self.from_address)
        self.log = errorhandling2.ErrorHandler(logdir=self.logdir,
                   smtpinfo=smtpinfo, me="ticketextractor",
                   admin_emails=self.admins,  # self.admins set by read_configuration
                   subject="TicketExtractor Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = 0

        self._start()

    def _start(self):
        self.positions = PositionDict()
        if not self.testmode:
            try:
                self.positions.load(self.positions_file)
            except IOError:
                if self.verbose: print "File", self.positions_file, "not found."

        self.ids = PositionDict()
        if not self.testmode:
            try:
                self.ids.load(self.ids_file)
            except IOError:
                if self.verbose: print "File", self.ids_file, "not found."

        self.log.log_event("Start of ticketextractor run")

    def get_filename(self, template, fdate):
        """ Return the name (with full path) of the file containing the tickets
            of a certain date. """
        d = fdate[:4] + fdate[5:7] + fdate[8:10]
        return template % d

    def read_configuration(self, filename):
        """ Read the configuration file. """
        f = datadir.datadir.open(filename, "rb")
        data = f.read()
        f.close()

        dom = ET.fromstring(data)
        # I won't catch any exceptions that occur here, because they don't tell
        # me anything

        fn = et_tools.find(dom, 'ftp')
        for attrname in ("host", "login", "password", "start_dir"):
            setattr(self, attrname, U(fn.get(attrname)))

        sn = et_tools.find(dom, 'source_file')
        self.update_dir = sn.get('update_dir', '%s')
        files = []
        for fn in et_tools.findall(sn, 'file'):
            fdict = {}
            for attr in ("name", "position"):
                fdict[attr] = fn.get(attr)
            files.append(fdict)
        self.file_templates = files

        tn = et_tools.find(dom, 'target_dir')
        self.target_dir = tn.get('name', '')
        self.prefix = tn.get('prefix', '')

        en = et_tools.find(dom, 'errorhandling')
        for attr in ('logdir', 'smtp_host', 'from_address', 'admins'):
            setattr(self, attr, U(en.get(attr)))

        self.admins = [{"email": x, "ln": '1',}
                       for x in self.admins.split(";")
                       if x.strip()]

        # some sanity checks
        if not os.path.exists(self.target_dir):
            os.makedirs(self.target_dir)

    def get_position(self, key, date):
        """ Get the file position for the given date. """
        try:
            return self.positions[key + "/" + date]
        except KeyError:
            self.positions[key + "/" + date] = 0
            return 0

    def set_position(self, key, date, position):
        """ Set the file position for the given date, and save it. """
        self.positions[key + "/" + date] = position
        self.positions.save(self.positions_file)

    def get_id(self, date):
        """ Get a new id for a filename. Update the self.ids dictionary on the
            fly."""
        try:
            last_id = self.ids[date]
        except KeyError:
            last_id = 0
        new_id = last_id + 1
        self.ids[date] = new_id
        return new_id

    def get_filenames(self, date):
        """ Return a list of all filenames we need to process. """
        filenames = []
        for template in self.file_templates:
            if template["name"].find("%s") > -1:
                filename = self.get_filename(template["name"], date)
            else:
                filename = template["name"]
            filenames.append(filename)
        return filenames

    def get_tickets(self, date, filename=None):
        """ Get all new tickets from a file. Set the new position if tickets
            are found. """
        tickets = []

        for filename in self.get_filenames(date):
            position = self.get_position(filename, date)
            if self.verbose: print "Scanning:", filename, "..."

            pos, _tickets = self._get_tickets(filename, position)
            self.log.log_event("%d tickets found in %s" % (
             len(_tickets), filename))

            tickets.extend(_tickets)

            position = position + pos
            if pos > 0:
                self.set_position(filename, date, position)

        return tickets

    def _get_tickets(self, filename, position):
        """ Get tickets from <filename>. Reading starts at position <position>.
            Return a tuple (position, tickets) where <pos> is the position in
            the substring up until which we have scanned. (Note that this is
            relative to the initial position <position>.
        """
        tickets = []
        try:
            f = open(filename, "rb")
        except IOError:
            #traceback.print_exc()
            self.log.log_event("File " + filename + " cannot be read.")
            if self.verbose:
                print >> sys.stderr, "File", filename, "cannot be read."
            return (0, [])
        f.seek(position)
        data = f.read() # read all data after position
        f.close()

        if len(data) <= 1:
            return (0, [])

        pos = 0
        while 1:
            idx1 = data.find(self.begin_marker, pos)
            if idx1 > -1:
                idx2 = data.find(self.end_marker, idx1)
                idx3 = data.find(self.begin_marker, idx1+1)
                if idx3 > -1 and idx3 < idx2:
                    idx2 = idx3
                    if self.verbose: print "Incomplete ticket found"
                if idx2 > -1:
                    ticket = data[idx1:idx2+1]
                    self._log(ticket)
                    tickets.append(ticket)
                    pos = idx2
                    continue
            break

        return (pos, tickets)

    def filter_tickets(self, tickets):
        """ Change the tickets list in-place to remove unwanted formats. """
        count = 0
        for i in range(len(tickets)-1,-1,-1):
            ticket = tickets[i]
            format = get_ticket_format(ticket)
            if format != "Colorado":
                del tickets[i]
                count = count + 1
        if self.verbose: print count, "tickets removed (unwanted formats)"

    def write_tickets(self, tickets, date):
        """ Write tickets to the target directory. """
        chunks = self.join(tickets, "~~~~~", self.chunksize)
        for chunk in chunks:
            # make filename
            id = self.get_id(date)
            filename = "%s-%s-%04d.txt" % (self.prefix, date, id)
            if self.verbose:
                print "Writing", filename, "...",
                print "(" + get_ticket_format(chunk) +")",
            fullname = os.path.join(self.target_dir, filename)
            f = open(fullname, "wb")
            f.write(chunk)
            f.close()
            if self.verbose: print "OK"
        if self.verbose:
            print len(chunks), "files written", \
             "(%d tickets per file)" % (self.chunksize), \
             "ready for uploading."
        self.ids.save(self.ids_file)

    def run(self):
        now = date.Date()
        today = now.isodate()[:10]
        now.dec()
        yesterday = now.isodate()[:10]
        for day in (yesterday, today):
            try:
                tickets = self.get_tickets(day)
                if self.verbose: print len(tickets), "new tickets found."
                self.filter_tickets(tickets)
                self.write_tickets(tickets, day)
                if self.upload:
                    self.upload_tickets()
            except:
                # for now, this will do
                ep = errorhandling2.errorpacket()
                self.log.log(ep, send=1, write=1, dump=1)

        self.log.force_send()   # purge any error mails in queue
        self.log.log_event("End of ticketextractor run")

    def upload_tickets(self):
        """ Upload all files in the target directory. """
        filenames = os.listdir(self.target_dir)
        if not filenames:
            if self.verbose: print "Nothing to upload."
            return
        try:
            # connect
            if self.verbose: print "Connecting to", self.host, "..."
            ftp = ftplib.FTP()
            ftp.connect(self.host, 21)
            ftp.login(self.login, self.password)
            if self.verbose: print ftp.getwelcome()
            ftp.cwd(self.start_dir)
            # upload all these files
            for filename in filenames:
                if self.verbose: print "Uploading", filename, "...",
                fullname = os.path.join(self.target_dir, filename)
                try:
                    f = open(fullname, "rb")
                except IOError:
                    if self.verbose: print "Could not open", fullname
                    self.log.log_event("Could not upload " + fullname)
                    continue
                ftp.storbinary("STOR " + filename, f)
                f.close()
                if self.verbose: print "OK"
                os.remove(fullname)
            # disconnect
            ftp.quit()
        except:
            ep = errorhandling2.errorpacket(info="Problem during uploading")
            self.log.log(ep, send=1, dump=1, write=1)

    def join(self, tickets, separator, number):
        """ Join <number> tickets together in one string. Return the list of
            new "multi-ticket-strings". """
        chunks = self._distribute(tickets, number)
        chunks = [string.join(chunk, separator) for chunk in chunks]
        return chunks

    def _distribute(self, tickets, number):
        """ Divide a list of tickets into a list of sublists of at most
            <number> tickets each. """
        chunks = []
        chunk = []
        for i in range(len(tickets)):
            chunk.append(tickets[i])
            if len(chunk) == number:
                chunks.append(chunk)
                chunk = []
        if chunk:
            chunks.append(chunk)    # don't forget leftover tickets

        return chunks

    def _log(self, ticket):
        """ "Log" that we've found a ticket. A ticket here is just a chunk of
            raw text. """
        lines = ticket.split("\n")
        self.log.log_event("Found: " + lines[0][1:])

    def test(self, filename, target_dir):
        """ Test run. Extract tickets from <filename> into <target_dir>. Do not
        upload. Do not use the default positions and ids files. """
        self.positions_file = "test_positions.txt"
        self.ids_file = "test_ids.txt"

        # don't use existing ids and positions files
        self.positions = PositionDict()
        try:
            self.positions.load(self.positions_file)
        except IOError:
            if self.verbose:
                print "File", self.positions_file, "not found."  # it's alright

        self.ids = PositionDict()
        try:
            self.ids.load(self.ids_file)
        except IOError:
            if self.verbose: print "File", self.ids_file, "not found."

        self.prefix = "TEST"

        self.file_templates = [{"name": filename, "position": "P"}]
        self.target_dir = target_dir

        now = date.Date()
        today = now.isodate()[:10]
        try:
            tickets = self.get_tickets(today)
            if self.verbose: print len(tickets), "new tickets found."
            self.filter_tickets(tickets)
            self.write_tickets(tickets, today)
        except:
            # for now, this will do
            ep = errorhandling2.errorpacket()
            self.log.log(ep, send=1, write=1, dump=1)

    ###
    ### finding headers, missing tickets, etc.

    def find_missing(self, headers_file, processed_file):
        """ Compare headers file (tickets on the CO server) and processed file
            (tickets on the SW server). Return a list of tickets that appear on
            CO, but not on SW.
        """
        headers = open(headers_file, "r").readlines()
        processed = open(processed_file, "r").readlines()
        missing = []
        for header in headers:
            if header not in processed:
                missing.append(header)
        if self.verbose: print len(missing), "missing tickets found."
        return missing

    def write_missing_list(self, missing_list):
        f = open("missing.txt", "w")
        for header in missing_list:
            print >> f, header
        f.close()
        print "File missing.txt written."

    def get_all_ticket_headers(self, date):
        """ Get the headers (first lines) of all tickets in all files of a
            given date. This does not affect positions or ids. """
        headers = []
        for template in self.file_templates:
            if template["name"].find("%s") > -1:
                filename = self.get_filename(template["name"], date)
            else:
                filename = template["name"]
            if self.verbose: print "Scanning:", filename, "..."

            _, tickets = self._get_tickets(filename, 0)
            if self.verbose: print "  .. found:", len(tickets), "tickets"
            for ticket in tickets:
                lines = ticket.split("\n")
                headers.append(lines[0])

        if self.verbose: print "Total found:", len(headers), "headers"
        return headers

    def write_headers_file(self, date):
        headers = self.get_all_ticket_headers(date)
        f = open("headers-"+date+".txt", "w")
        for header in headers:
            print >> f, header
        f.close()
        if self.verbose: print "File", "headers-"+date+".txt", "written."

    def repair(self, processed_file, date):
        """ Look for headers that are not in processed_file, and write those
            tickets. """
        # get the processed headers
        repair_tickets = []
        found = 0
        processed_headers = open(processed_file, "r").readlines()
        processed_headers = [h.strip() for h in processed_headers]

        # traverse all tickets and see if they're in processed_headers
        for template in self.file_templates:
            if template["name"].find("%s") > -1:
                filename = self.get_filename(template["name"], date)
            else:
                filename = template["name"]
            if self.verbose: print "Scanning:", filename, "..."

            _, tickets = self._get_tickets(filename, 0)
            if self.verbose:
                print "  .. found:", len(tickets), "tickets (unfiltered)"
            for ticket in tickets:
                lines = ticket.split("\n")
                if lines[0].strip() not in processed_headers:
                    # apparently this was not sent or received
                    repair_tickets.append(ticket)
                else:
                    found = found + 1

        if self.verbose:
            print found, "tickets were sent correctly."
            print len(repair_tickets), "tickets found that were not processed."
            print "Writing to upload directory..."

        # now, write these tickets, eh? and *do* use the id file
        self.write_tickets(repair_tickets, date)
        #self.upload_tickets()


def main(klass, begin, end):
    # -u (don't upload)
    # -t (expects filename and target_dir as "normal" arguments)
    # -m ...?
    # -h date: produce a file of output headers, called headers-{DATE}.txt
    # -r (expects processed_file and date as "normal" arguments)

    upload = 1  # default: do upload
    testmode = 0
    test_filename = target_dir = ""
    headers_date = ""
    missing_mode = 0
    header_file = processed_file = ""
    repair_mode = 0

    opts, args = getopt.getopt(sys.argv[1:], "utmh:r?")
    for o, a in opts:
        if o == "-u":
            upload = 0
        elif o == "-t":
            testmode = 1
            test_filename, target_dir = args[:2]
        elif o == "-h":
            headers_date = a
        elif o == "-m":
            missing_mode = 1
            header_file, processed_file = args[:2]
        elif o == "-r":
            repair_mode = 1
            processed_file, processed_date = args[:2]
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)

    # try to create a logger for uncaught toplevel errors.
    # if that fails, try to extract admin and SMTP info from the config file in
    # a different way, and send it.
    try:
        log = errorhandling_special.toplevel_logger("config.xml",
              "TicketExtractor", "TicketExtractor warning")
    except config.ConfigurationError:
        errorhandling_special.log_config_error("config.xml", "TicketExtractor")
        sys.exit(1)

    mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
    try:
        with mutex.MutexLock(dbinterface_ado.DBInterfaceADO(), mutex_name):
            te = klass(begin, end, upload=upload, testmode=testmode)

            if testmode:
                te.test(test_filename, target_dir)
            elif headers_date:
                te.write_headers_file(headers_date)
            elif missing_mode:
                missing = te.find_missing(header_file, processed_file)
                te.write_missing_list(missing)
            elif repair_mode:
                te.repair(processed_file, processed_date)
            else:
                te.run()

    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)


if __name__ == "__main__":

    main(TicketExtractor, begin=chr(2), end=chr(3))

