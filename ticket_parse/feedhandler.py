# feedhandler.py

import os
import time
#
import call_centers
import config
import date
import errorhandling2
import filetools
import tools

MAX_PER_CALL_CENTER = 500
# should really be an option in config.xml

class FeedInfo(tools.Struct):
    __fields__ = ['call_center', 'incoming', 'processed', 'error',
                  'attachments', 'attachment_dir', 'attachment_proc_dir',
                  'upload_location', 'attachment_user']

class FileInfo(tools.Struct):
    __fields__ = ['time_t9', 'fullname', 'filename', 'incoming', 'processed',
                  'error', 'call_center', 'formats', 'attachment_dir',
                  'attachment_proc_dir', 'filedate', 'upload_location',
                  'attachment_user']

class FeedHandler:

    def __init__(self, configuration, log, call_center, suggested_format="", reparse_days=0,
                 verbose=1):
        self.config = configuration
        self.reparse_days = reparse_days
        self.log = log
        self.suggested_format = suggested_format # obsolete?
        self.verbose = verbose

        self.process = self.config.call_center_process(call_center)
        if not self.process:
            raise config.ConfigurationError,\
             "Process configuration for call center: %s not found" % (call_center)

        self.feed = FeedInfo(
            call_center = self.process['format'],
            incoming = self.process['incoming'],
            processed = self.process['processed'],
            error = self.process['error'],
            attachments = self.process['attachments'],
            attachment_dir = self.process.get('attachment_dir', ''),
            attachment_proc_dir = self.process.get('attachment_proc_dir', ''),
            attachment_user = self.process.get('attachment_user', ''),
            upload_location = self.process.get('upload_location', ''),
            )

    # todo(dan) This needs refactoring.
    def get_all_files(self, incoming_files=None):
        """ Get data of all ticket files waiting to be processed. """

        files = []

        if incoming_files is None:
            received_subdir = ''
        else:
            received_subdir = config.RECEIVER_OUTPUT_SUBDIR

        if self.reparse_days > 0:
            incoming = self.feed.error
        else:
            incoming = os.path.join(self.feed.incoming, received_subdir)

        # get acceptable formats for this call center
        formats = self._get_formats(self.feed.call_center, incoming)

        if incoming_files is None:
            try:
                dirlist = os.listdir(incoming)
            except Exception, e:
                ep = errorhandling2.errorpacket()
                self.log.log(ep, send=1, write=1, dump=1)
                return files
            # get only files, not directories:
            dirlist = [f for f in dirlist if not
             os.path.isdir(os.path.join(incoming, f))]
        else:
            # todo(dan) Refactor so removing the path isn't necessary
            dirlist = [os.path.basename(f) for f in incoming_files]

        for filename in dirlist:
            fullname = os.path.join(incoming, filename)
            atime, mtime, ctime = os.stat(fullname)[-3:]

            if self.reparse_days > 0 \
             and tools.file_age(fullname) > self.reparse_days:
                self.sayln("File %s skipped (%.1f days old)" % (
                 fullname, tools.file_age(fullname)))
                continue

            time_t9 = time.localtime(mtime)
            filedate = date.Date(time_t9).isodate()

            fileinfo = FileInfo(
                       time_t9=time_t9,
                       fullname=fullname,
                       filename=filename,
                       incoming=incoming,
                       processed=self.feed.processed,
                       error=self.feed.error,
                       call_center=self.feed.call_center,
                       attachment_dir=os.path.join(self.feed.attachment_dir,
                        received_subdir),
                       attachment_proc_dir=self.feed.attachment_proc_dir,
                       attachment_user=self.feed.attachment_user,
                       filedate=filedate,
                       upload_location=self.feed.upload_location,
                       formats=formats)
            # decorate
            files.append((fileinfo.time_t9, fileinfo.filename, fileinfo))

        files.sort()    # oldest files first
        files = [f[2] for f in files]   # undecorate

        if incoming_files is None:
            return files[:MAX_PER_CALL_CENTER]
        return files

    def get_call_center_formats(self, call_center):
        return self._get_formats(call_center, '')

    def _get_formats(self, call_center, incoming_dir):
        """ Try to determine the formats for a given call center (if any). """
        formats = []
        if call_center:
            try:
                if call_center in call_centers.work_order_centers:
                    formats = call_centers.work_order_centers[call_center]
                else:
                    # map call center to ticket format(s)
                    formats = call_centers.cc2format[call_center]
            except KeyError:
                logmsg = "Unknown call center (%s) for directory '%s'" % (
                         call_center, incoming_dir)
                self.log.log_event(logmsg, dump=1)
                call_center = ""
                # this error is ignored so we won't upset the parsing...
                # or we can continue...
                raise SystemExit
        else:
            formats = [] # no format suggestion from config file
        if not formats and self.suggested_format:
            formats = [self.suggested_format]

        return formats

    @staticmethod
    def move(filename, destination):
        filetools.move_with_benefits(filename, destination)

    #
    # auxiliary routines

    def say(self, *args):
        if self.verbose:
            for arg in args:
                print arg,

    def sayln(self, *args):
        if self.verbose:
            self.say(*args)
            print
