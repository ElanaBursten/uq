# qminterface.py

from __future__ import with_statement
import md5
import os
#
import config
import datadir
import tools

UTILITY_INI_TEMPLATE = """\
[Database]
Server=%(host)s
DB=%(database)s
UserName=%(login)s
Password=%(password)s
ConnectionString=%(conn_string)s
"""

def old_compare_ini_data(s1, s2):
    old_md5 = md5.md5(s1).hexdigest()
    new_md5 = md5.md5(s2).hexdigest()
    return old_md5 == new_md5

def compare_ini_data(s1, s2):
    return tools.singlespaced(s1) == tools.singlespaced(s2)

def write_utility_ini(if_changed=1):
    cfg = config.getConfiguration()
    data = UTILITY_INI_TEMPLATE % cfg.ado_database
    if if_changed:
        if utility_ini_exists():
            old_data = read_utility_ini()
            if compare_ini_data(old_data, data):
                return

    with datadir.datadir.open("utility.ini", 'w') as f:
        f.write(data)
    return True # indicates that a new file was written

def read_utility_ini():
    with datadir.datadir.open("utility.ini", 'r') as f:
        data = f.read()
    return data

def utility_ini_exists():
    path = datadir.datadir.get_filename("utility.ini")
    return os.path.exists(path)

