# wo_assignment.py
# XXX Might not be necessary in Python code, as long as we can use stored
# procs to update WO assignments.

import sqlmap
import sqlmaptypes as smt
import static_tables

class WorkOrderAssignment(sqlmap.SQLMap):

    __table__ = "wo_assignment"
    __key__ = "assignment_id" # sic
    __fields__ = static_tables.wo_assignment

    #def __init__(self, locator=""):
    #    sqlmap.SQLMap.__init__(self)
    #    self.locator_id = locator

    def __repr__(self):
        return "WorkOrderAssignment(%s)" % self.assigned_to_id

    def insert_sg(self, sg):
        pass


