# clean_old_files.py
# Cleanup old logs and attachments.

import os
import sys
import time
#
import config
import datadir
import emailtools
import errorhandling2
import errorhandling_special
import receiver
import tools
import windows_tools

DAY = 60L*60*24 # number of seconds in a day

class CleanerUpper:

    def __init__(self, num_days, configfile="", verbose=1):
        self.verbose = verbose
        self.num_days = num_days

        tools.check_python_version()

        # we need config.xml for logging what we're doing, and to figure out
        # where the old files are
        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        # install logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="FileCleaner",
                   cc_emails=None,
                   admin_emails=self.config.admins,
                   subject="FileCleaner Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.verbose

        self.log.log_event("Start of file cleaner")

    def run(self):
        pass
        # use: self.config.logdir and attachment_dir
        print "log dir:", self.config.logdir

        attachment_dir = self.config.attachment_dir
        print "attachment dir:", attachment_dir
        # both of these are in the datadir

        for dir in (self.config.logdir, attachment_dir):
            full_path = datadir.datadir.get_filename(dir)

            try:
                files = os.listdir(full_path)
            except WindowsError:
                continue

            for filename in files:
                #print filename
                full_name = os.path.join(full_path, filename)
                atime, mtime, ctime = os.stat(full_name)[-3:]

                # determine how old the file is
                diff = abs(time.time() - mtime) / DAY
                if diff >= self.num_days:
                    s = "Deleting: %s (%.1f days old)" % (filename, diff)
                    self.log.log_event(s, dump=1)

                    try:
                        os.remove(full_name)
                    except OSError, e:
                        s = "** Could not delete file; reason: %s" % (e.args,)
                        self.log.log_event(s, dump=1)


if __name__ == "__main__":

    __usage__ = """\
clean_old_files.py number_of_days

Delete logs and attachments older than <number_of_days>.
"""

    try:
        num_days = int(sys.argv[1])
    except:
        print >> sys.stderr, __usage__
        raise SystemExit

    try:
        log = errorhandling_special.toplevel_logger("", "CleanOldFiles",
              "CleanOldFiles Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "CleanOldFiles")
        sys.exit(1)

    windows_tools.setconsoletitle("clean_old_files")

    try:
        cu = CleanerUpper(num_days=num_days)
        cu.run()
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=0, write=1, dump=1)
