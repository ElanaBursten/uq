# ticketloader.py
# Created: 2002.10.24 HN (split off from ticketparser.py)

import re
import string
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
#
import filetools
import tools

# Maximum size of ticket. Anything larger than this is an error.
MAX_TICKET_SIZE = 10*1024   # 10K

def R(regex, flags=0):
    return re.compile(regex, flags)

class TicketLoaderError(Exception):
    def __init__(self, value, data="", filename=""):
        self.value = value
        self.data = data
        self.filename = filename
        self.args = (value,)

class TicketLoader:
    """ Quick and easy implementation of TicketLoader. Memory may be an
        issue with large (multi-megabyte) files and memory-starving
        machines.
    """

    SEPARATOR = chr(12) # ^L
    ALT_SEPARATORS = [chr(2), chr(3), chr(5)]
    CONVERT_TO_WHITESPACE = ["\xa0"]

    re_from = re.compile("^From: ", re.MULTILINE)
    re_to = re.compile("^To: ", re.MULTILINE)
    re_subject = re.compile("^Subject: ", re.MULTILINE)
    re_consolidated = R("Consolidated Tickets: \d+")
    re_consolidated_sep = R("CONSOLIDATED TICKET \d+ OF \d+:")

    def __init__(self, filename, separator=""):
        """ As of version 1.22, separator is now optional. If it's specified,
            the data is split on that separator. If not, then Ctrl-L is assumed,
            but before the data is split, any "~~~~~" occurrances are converted
            to Ctrl-L, so that both of the common separators work.
        """
        self.filename = filename
        self.data = filetools.read_file(filename, 'r')
        self.data = self.normalize_newlines(self.data)
        self.data = self.strip_email_headers(self.data)
        if not separator:
            # Ctrl-L is the default separator; replace anything in
            # ALT_SEPARATORS with Ctrl-L
            for altsep in self.ALT_SEPARATORS:
                self.data = self.data.replace(altsep, self.SEPARATOR)
            separator = self.SEPARATOR

        # remove "garbage", like modem command strings etc
        self.data = self.remove_garbage(self.data)
        self.data = self.convert_whitespace(self.data)

        if string.find(self.data, separator) == -1:
            #if self.is_consolidated(self.data):
            #    self.tickets = consolidated_split(self.data)
            #else:
            #    self.tickets = [self.data]  # whole bulk is considered a ticket
            self.tickets = [self.data]  # whole bulk is considered a ticket
        else:
            # Split bulk string on separator. Strip tickets from trailing
            # and leading whitespace, then remove empty strings; they don't
            # count as valid tickets.
            self.tickets = [s.strip() for s in self.data.split(separator)]
            self.tickets = filter(None, self.tickets)

        # remove "cruddy" tickets (line noise)
        self.tickets = [tools.remove_crud(t) for t in self.tickets
                        if not self._islinenoise(t)]

        # fix incorrect use of '&' in XML tickets
        self.tickets = [tools.fix_ampersands_in_xml(t)
                        if tools.is_xml(t) else t
                        for t in self.tickets]
        self._count = 0

        # check for "consolidated" tickets
        self.check_consolidated()


    @staticmethod
    def _islinenoise(rawdata):
        """ Return true if raw ticket data can be considered line noise
            (rather than a possibly valid ticket). """
        cstr_file = StringIO(rawdata)
        try:
            # If an xml file parses, unlikely to be filled with line noise
            ET.parse(cstr_file)
            return 0
        except:
            pass
        finally:
            cstr_file.close()
        lines = [s for s in rawdata.split('\n') if s.strip()]
        if len(lines) <= 1:
            return 1
        #if len(rawdata) <= 40:  # 40 bytes or less
        #    return 1
        return 0

    def getTicket(self):
        try:
            t = self.tickets[self._count]
        except IndexError:
            t = None
        self._count = self._count + 1
        return t

    def close(self):
        """ Legacy function; should be removed from tests, IF this
            TicketLoader gets approved. """

    def strip_email_headers(self, data):
        idx = self.find_email_header(data)
        if idx:
            data = data[idx:]

        return data

    @staticmethod
    def normalize_newlines(data):
        return data.replace('\r\n', '\n').replace('\r', '\n')

    def find_email_header(self, data):
        """ If the given chunk of data has an email header, return the index
            where the headers end. If no headers are found, return 0. """
        firstempty = string.find(data, "\n\n")  # find first empty line
        if firstempty == -1:
            firstempty = string.find(data, "\r\n\r\n")
            if firstempty == -1:
                return 0

        possible_headers = data[:firstempty]
        if self.re_from.search(possible_headers) \
        and self.re_to.search(possible_headers) \
        and self.re_subject.search(possible_headers):
            # yes, these can be considered headers
            return firstempty
        else:
            return 0

    def sanitycheck(self):
        for raw in self.tickets:
            if len(raw) > MAX_TICKET_SIZE:
                msg = "Ticket too large (%d bytes), possible separator problem"
                msg = msg % (len(raw),)
                raise TicketLoaderError(msg, data=raw, filename=self.filename)

    def findfirst(self, substr):
        """ Find and return the first raw ticket that contains <substr>. """
        for raw in self.tickets:
            if raw.find(substr) > -1:
                return raw
        raise ValueError, "No ticket found with substring %r" % (substr,)

    @staticmethod
    def remove_garbage(image):
        """ Remove "garbage" from an image, like modem command strings. """
        garbage_regexen = [
            R("NO CARRIER(\s+RING){1,}", re.DOTALL),
        ]

        for regex in garbage_regexen:
            image = regex.sub("", image)

        return image

    def convert_whitespace(self, data):
        for c in self.CONVERT_TO_WHITESPACE:
            data = data.replace(c, " ")
        return data

    def check_consolidated(self):
        for index, image in enumerate(self.tickets[:]):
            if self.is_consolidated(image):
                images = self.consolidated_split(image)
                # insert tickets at the same place, to preserve original order
                self.tickets[index:index+1] = images

    def is_consolidated(self, data):
        """ Return true if these data are from a consolidated ticket. """
        m = self.re_consolidated.search(data)
        return bool(m)

    def consolidated_split(self, data):
        parts = self.re_consolidated_sep.split(data)
        return parts[1:] # discard "header"

    def __iter__(self):
        return iter(self.tickets)

