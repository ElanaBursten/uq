# profiler.py
# A simple profiler class for storing times of events, summarizing them,
# generating reports, etc.
# Created: 2002.07.09 HN

import time
import string
import operator
#
import decorator

class ProfilerEntry:
    def __init__(self, name="", time=0.0, args=(), kwargs={}):
        self.name = name
        self.time = time
        self.args = args
        self.kwargs = kwargs
    def __repr__(self):
        s = "%s %s %s (%.2f seconds)" % (self.name, self.args, self.kwargs,
         self.time)
        return s

class Profiler:
    """ Generic class to keep a list of ProfilerEntry instances. """

    def __init__(self):
        self.data = []  # list of ProfilerEntry instances
        self.timer_start = self.timer_stop = 0.0

    def start(self):
        self.timer_start = time.time()

    def stop(self):
        self.timer_stop = time.time()

    def add(self, name="", args=(), kwargs={}):
        elapsed_time = self.timer_stop - self.timer_start
        pe = ProfilerEntry(name, elapsed_time, args, kwargs)
        self.data.append(pe)

    def time_elapsed(self):
        times = [entry.time for entry in self.data]
        total = reduce(operator.add, times)
        return total

    def write_text(self, filename):
        f = open(filename, "w")
        for i in range(len(self.data)):
            entry = self.data[i]
            print >> f, "[%d]" % (i+1),
            print >> f, entry.name, entry.args, entry.kwargs,
            print >> f, "(%.2f seconds)" % (entry.time)
        f.close()

    def write_csv(self, filename):
        f = open(filename, "w")
        for i in range(len(self.data)):
            entry = self.data[i]
            lst = [`i+1`, entry.name, `entry.args`, `entry.kwargs`, "%.2f" %
             (entry.time)]
            print >> f, string.join(lst, "\t")
        f.close()

"""
Instead of using tdb, we want to use a decorator around tdb. So instead of

    self.tdb = TicketDB(...)

we use

    tdb = TicketDB(...)
    self.tdb = Decorator(tdb, some_method_decorator)

and calls like self.tdb.dothis() will be resolved by the decorator.
"""

