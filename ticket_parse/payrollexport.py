# payrollexport.py
# Created: 2002.11.16 HN

import getopt
import sys
#
import dbinterface_ado

__usage__ = """\
payrollexport.py [options] filename week_ending num_weeks manager_emp_id
"""

class PayrollExporter:

    def __init__(self, filename, week_ending, num_weeks, manager_emp_id):
        self.dbado = dbinterface_ado.DBInterfaceADO()

        self.filename = filename
        self.week_ending = week_ending
        self.num_weeks = num_weeks
        self.manager_emp_id = manager_emp_id

    def get_data(self):
        sql = """
         select * from employee
        """
        # XXX replace this with "real" SQL

        rows = self.dbado.runsql_result(sql)
        return rows

    def run(self):
        # dummy "run" method. replace with something more sensible.
        rows = self.get_data()
        print len(rows), "employees found"


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "")

    if len(args) == 4:
        filename, week_ending, num_weeks, manager_emp_id = args
        num_weeks = int(num_weeks)
    else:
        print >> sys.stderr, __usage__
        raise SystemExit

    pe = PayrollExporter(filename, week_ending, num_weeks, manager_emp_id)
    pe.run()

