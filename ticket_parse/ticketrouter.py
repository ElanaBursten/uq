# ticketrouter.py
# Created: 11 Feb 2002, Hans Nowak

from __future__ import with_statement
import getopt
import logging
import logging.handlers
import os
import pprint
import socket
import string
import sys
import time
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import assignment
import blacklist
import call_centers
import config
import datadir
import date
import date_special
import dbinterface_ado
import dbinterface_old as dbinterface
import email_by_client
import emailtools
import errorhandling2
import errorhandling_special
import listener
import locate_status
import mail2
import mutex
import program_status
import ticket_db
import ticketdb_decorator
import ticketrouterdata
#import timezone
import tools
import trymanytimes
import version
import windows_tools

from ticket import Ticket
from locate import Locate
from show_profile_report import show_profile_report
from getlogic import getlogic
from check_python_version import check_python_version

# todo(dan) Change '0' to something useful?
__version__ = '0'

__usage__ = """\
ticketrouter.py [options]

Routes recently posted or updated tickets in database.

Options:
    -1          Only run router once.
    -c cfgfile  Specify an alternate configuration file. (default: config.xml)
    -g          Show gc debugging info.
    -p          Turn on profiling. (Writes file router_profiler.txt.)
"""

# Create a logger for debugging use only

log_debug = logging.getLogger("ticketrouter")

import gc
gc.enable()
gc.set_threshold(50000, 10, 10)

if "-g" in sys.argv[1:]:
    gc.set_debug(gc.DEBUG_STATS)

BUGFILE = "routing_error_skip.txt"
BAIL_OUT_TIME = 5 # in minutes; this is the time a router round takes before
                  # we start the next round, to fetch fresh emergencies


class TicketRouterError(Exception):
    pass

class TicketRouter:

    def __init__(self, wait, configfile="", verbose=1, debug=0, dbado=None):
        self.wait = wait
        self.verbose = verbose
        self.debug = debug

        tools.check_python_version()

        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        # these should use the same configuration as TicketRouter
        self.tdb = ticket_db.TicketDB(dbado=dbado)
        self.tdb = ticketdb_decorator.TicketDBDecorator(self.tdb,
                   ticketdb_decorator.TicketDBMethodDecorator)
        self.holiday_list = date_special.read_holidays(self.tdb)
        self.emails = emailtools.Emails(self.tdb)

        # install logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="Ticketrouter",
                   cc_emails=self.emails,
                   admin_emails=self.config.admins,
                   subject="Ticketrouter Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.verbose
        self.say("Loading notification_email_by_client emails...")
        self.ebc = email_by_client.EmailByClient(self.tdb)
        self.sayln("OK")
        self.log.log_event("Server configured with time zone %s" %
          self.config.server_timezone)

        log_debug.setLevel(logging.WARNING)
        # Merge the output with the production log
        handler = logging.StreamHandler(self.log.logger.logfile)
        form = logging.Formatter("%(levelname)s %(name)s %(asctime)s %(message)s")
        handler.setFormatter(form)
        log_debug.addHandler(handler)
        log_debug.debug("Ticketrouter debug logging enabled")

        self.logic = {} # collection of BusinessLogic instances

        self._BEC01_ids = self.tdb.get_BEC01_ids()
        # this is a cache that will be used for the BEC01/BEC02 checks

        self.blacklisted_ids = []  # don't route these ids (stored as integers)
        self.ignored = [] # ignored because of blacklisting (stored as strings)
        #self.clients = self.tdb.get_clients_dict()
        # XXX not used anymore

        self.time_elapsed = self.db_accessed = 0
        self.time_get_pending_locates = 0

    def say(self, *args):
        if self.verbose:
            for arg in args:
                print arg,

    def sayln(self, *args):
        if self.verbose:
            self.say(*args)
            print

    def run(self, runonce=0):
        self.log.log_event("ticketrouter.py (version %s) started with pid %s" %
          (version.__version__, os.getpid()))
        self.log.log_event("TicketRouter Starting (Routing Locates)")
        self.run_routing(runonce)
        self.log.log_event("TicketRouter Done")

    def run_routing(self, runonce=0):
        # router runs up to <numrounds> times.  if, at any start of a turn,
        # there are no pending locates, then the router will stop.
        numrounds = self.config.ticketrouter.get('numrounds', 1)
        for _round in range(numrounds):
            self.log.log_event("Start of ticketrouter (%s) run #%d" % (
             __version__, _round+1))
            # get list of outstanding locates/tickets
            self.blacklisted_ids = self.read_errorfile()

            # prepare & start timer
            self.tdb.dbado.timed_events = []
            timer = tools.Timer()
            timer.start()

            self.log.log_event("Getting pending locates...")
            try:
                result = self._get_locates() # a list of dicts
            except (KeyboardInterrupt, SystemExit):
                break # out of the loop
            except:
                # any unhandled exceptions here are unexpected; admins must be
                # notified
                ep = errorhandling2.errorpacket()
                ep.add(info="Could not get pending locates")
                self.log.log(ep, send=1, dump=1, write=1)
                break

            # if no locates found, then exit
            if not result:
                if runonce:
                    self.log.log_event("End of ticketrouter run [runonce]")
                    break   # we're done
                else:
                    self.wait_for_more()

            # collect ids of tickets that have been routed this round
            self.routed = []

            # process tickets
            try:
                self.task(result)
            except (KeyboardInterrupt, SystemExit):
                break
            except (dbinterface.DBFatalException, socket.error):
                ep = errorhandling2.errorpacket()
                # Check that the value has a find method (string)
                # It should
                if hasattr(ep.exception.args[0],"find"):
                    # if the db exception is a deadlock, don't send
                    if ep.exception.args[0].find('was deadlocked on lock resources') != -1:
                        send = 0
                self.log.log(ep, send=send, write=1, dump=1 and self.verbose)
                self.log.log(ep, send=1, dump=1, write=1)
                break   # don't stay in the loop
            except:
                # any unhandled exceptions here are unexpected; admins must be
                # notified
                ep = errorhandling2.errorpacket()
                self.log.log(ep, send=1, dump=1, write=1)

            self.log.force_send()   # should this be in the timer?

            timer.stop()
            if 1 or self.data._tickets:
                summary_msg = "%d ticket(s) processed in %.1f seconds "\
                "(%.1f in database, %.2f getting locates)" % (
                 len(self.routed),
                 timer.elapsed(),
                 self.tdb.profiler.time_elapsed() - self.time_elapsed,
                 self.time_get_pending_locates)

                self.log.log_event(summary_msg)
                self.sayln("(database accessed %d times)" % (
                 len(self.tdb.profiler.data) - self.db_accessed))
            else:
                self.sayln("No tickets found")

            self.time_elapsed = self.tdb.profiler.time_elapsed()
            self.db_accessed = len(self.tdb.profiler.data)

            self.log.log_event("End of ticketrouter run")

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

        else:
            self.log.log_event("Router stopping after %d rounds" % (numrounds,))

        self.log.log_event("End of ticketrouter routing")

    @staticmethod
    def _collect_ticket_ids(rows):
        ids = {}
        for row in rows:
            ids[row["ticket_id"]] = 1
        return ids.keys()

    def remove_blacklisted(self, rows):
        """ Removes blacklisted ids from the list of rows. Modified the list
            in-place, therefore doesn't return anything. """
        mentioned = []
        for i in range(len(rows)-1, -1, -1):
            row = rows[i]
            ticket_id = row["ticket_id"]
            if int(ticket_id) in self.blacklisted_ids:
                if ticket_id not in mentioned:
                    self.sayln("id", ticket_id, "is blacklisted -- skipped")
                    mentioned.append(ticket_id)
                    self.ignored.append(ticket_id)
                del rows[i]

    def wait_for_more(self):
        try:
            self.say("Waiting for", self.wait, "seconds...")
            # wait for self.wait seconds, while keeping an eye on signals
            start = time.time()
            while (time.time() - start) < self.wait:
                time.sleep(1)
                # check for 'quit' signal
                if 'quit' in listener.uqlistener.listen():
                    self.log.log_event("listener: 'quit' signal received, exiting")
                    raise SystemExit
            self.sayln("OK")
        except (KeyboardInterrupt, SystemExit):
            sys.exit(1)

    def route_tickets(self, rows):
        """ Collect data for the given tickets, handle special cases, route
            them, and store updates. """
        customers = self._get_customers()   # a dictionary keyed off of the client_code,call_center tuple

        count = 0
        grouped_results = self.group_results(rows)
        # force delete of long rows list... or so we hope
        del rows[:]
        del rows

        start = time.time()
        for group in grouped_results:

            # check time... did we spend more than BAIL_OUT_TIME minutes?
            diff = (time.time() - start) / 60.0 # difference in minutes
            if diff > BAIL_OUT_TIME:
                log.log_event("Round running more than " + str(BAIL_OUT_TIME) +
                 " minutes, bailing out...")
                break # out of loop

            ticket_id = group[0]['ticket_id']
            self.collect_data_for_group(group, customers)
            count += self.handle_BEC01_ticket(ticket_id)
            self.store_changes_for_ticket(ticket_id)
            self.data.delete(ticket_id)
            self.routed.append(ticket_id)

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

        if count > 0:
            self.sayln(count, "locates changed status (BEC01/BEC02)")

    def collect_data_for_group(self, group, customers):
        for row in group:
            try:
                ticket_id, locate_id = self.data.add_row(row)
            except KeyboardInterrupt:
                sys.exit(1)
            except (dbinterface.DBFatalException, socket.error):
                raise   # this should not be caught here
            except:
                # if an error occurs here, log it (& notify), but continue
                # with the next row
                ep = errorhandling2.ErrorPacket()
                ep.set_traceback()
                ep.add(_tickets=self.data._tickets.get(row['ticket_id']),
                 _locates=self.data._locates,
                 row=row,
                 ticket_id=row['ticket_id'],
                 additional_info="Put ticket_id in " + BUGFILE,
                 blacklisted=row['ticket_id'])
                ep.add(info="Error reading ticket data from row")
                self.log.log(ep, send=1, write=1, dump=1)

                # also, "blacklist" this ticket_id
                self.append_to_errorfile(row['ticket_id'])
                continue

            # add some more data for Prince
            biz_id = datadir.datadir.get_id()
            if biz_id.lower() == 'prince':
                d = self.tdb.get_additional_ticket_info(ticket_id)
                self.data._tickets[ticket_id].update(d)

            # try finding this customer in our list
            client_id = self._find_client_id(customers, row["client_code"],
                        row["ticket_format"])
            self.data._locate_client[locate_id] = client_id
            self.data._locate_client_code[locate_id] = row["client_code"]
            if client_id:
                # if mapinfo is already known for this this, we can
                # possibly save cycles by taking a previous locator,
                # rather than looking it up again

                # new: if a locator was already assigned, use this value
                logic = getlogic(row['ticket_format'], self.logic, self.tdb,
                        self.holiday_list)
                locator = area_id = None
                if row["ticket_format"] != "NewJersey" \
                and logic.use_prerouting():
                    locator = self.data._tickets[ticket_id]["locator"]
                if locator:
                    s = "Using existing locator %s for ticket %s/%s "\
                        "and locate %s"
                    s = s % (locator, row["ticket_number"], ticket_id,
                             locate_id)
                    self.log.log_event(s)
                # it's possible that we didn't find it, though... so
                # look it up then
                if locator is None:
                    xrow = row.copy()   # kludgy...
                    xrow.update(self.data._tickets[ticket_id])
                    try:
                        locator, area_id = self.find_locator(ticket_id, xrow)
                        self.data._tickets[ticket_id]["area_id"] = area_id
                    except Exception:
                        # an error occurred when trying to determine the
                        # locator, but we will go on anyway
                        ep = errorhandling2.errorpacket()
                        ep.add(info="An error occurred when trying to "\
                         "determine the locator")
                        self.log.log(ep, dump=1, write=1, send=1)
                        # XXX not very informative
                        logic = getlogic(row['ticket_format'], self.logic, self.tdb, self.holiday_list)
                        locator = logic.DEFAULT_LOCATOR

                # by here, the locator should be known
                assert locator != None
                # assign the locator [new]
                self.data._assignments[locate_id] = locator
                if not self.data._tickets[ticket_id]["locator"]:
                    self.data._tickets[ticket_id]["locator"] = locator
                # remember that this locate_id was assigned
                self.data._locates[locate_id] = locate_status.assigned
            else:
                # remember that this locate_id was not assigned because it's
                # not a customer
                self.data._locates[locate_id] = locate_status.not_a_customer

    def task(self, rows):
        """ Find unassigned locates, and either assign them or flag them as
            "not a customer". (Only customers, those in the client table,
            can and will be assigned.) Assigning means that a record is
            added in the assignment table, associating the locate with a
            locator.
            As of 2002.06.08, the set of records is now passed in as a
            parameter (this is done by run()).
        """
        self.data = ticketrouterdata.TicketRouterData()

        self.route_tickets(rows)

    def handle_special_cases(self, rows):
        # We've collected a lot of data here.
        # BEC01 and BEC02 are special cases for the NorthCarolina format (or
        # the FCL1/FCL2 call centers):
        # - if they are the only clients on a ticket, mark them -R
        # - otherwise, mark them -N
        try:
            num_changed = self.handle_BEC01(rows)
            if num_changed:
                self.sayln(num_changed, "locates changed status (BEC01/BEC02)")
        except Exception:
            ep = errorhandling2.ErrorPacket()
            ep.set_traceback()
            ep.add(info="Error handling BEC01/BEC02 routing")
            self.log.log(ep, send=1, write=1, dump=1)

    def store_changes(self):
        # Store all the collected info. Related tickets, locates and
        # assignments are grouped. This is done so if anything fails, the
        # whole transaction will roll back, rather than leaving some parts
        # in the database.
        # Note: IDs are mandatory everywhere (says SQL Server).

        for ticket_id in self.data._tickets.keys():
            self.store_changes_for_ticket(ticket_id)

        self.log.log_event("End of updates")

    def screen_locates(self, row, ticket_id):

        # get call center of ticket; time zone of center and of server;
        # defaulting to EST/EDT like before (if not entered in db)
        cc = call_centers.get_call_centers(self.tdb)
        ticket_cc = row['ticket_format']
        #orig_tz = self.config.server_timezone
        #cc_tz = cc.get(ticket_cc).get('timezone_code', orig_tz)

        for i, locate_id in enumerate(self.data._tickets[ticket_id]["locates"]):
            # XXX wrap in try..except
            # XXX move to Locate as a classmethod?
            newstatus = self.data._locates[locate_id]
            if newstatus != locate_status.not_a_customer:
                # Get the mgr of the assigned locator
                sql = """
                    select top 1 e.emp_id
                    from dbo.get_emp_superiors(%s) sup
                    inner join employee e on sup.emp_id = e.emp_id
                    inner join reference r on e.type_id = r.ref_id
                    where sup.distance < 0 and sup.active = 1
                    and r.modifier like '%%SCR%%'
                    order by sup.distance desc
                """ % row['locator']
                rows = self.tdb.runsql_result(sql)
                mgr_id = int(rows[0]['emp_id'])

                # HACK to work around timezone issues Mantis #2856:
                # use current date/time - 3 for CA; current d/t otherwise
                closed_date = date.Date()
                if ticket_cc.startswith(('NCA', 'SCA')):
                    closed_date.dec_time(hours=3)

                locate_args = {
                    'status': locate_status.screened,
                    'closed': 1,
                    'closed_by_id': mgr_id,
                    'closed_how': 'router',
                    'qty_marked': 1,
                    'closed_date': closed_date.isodate(),
                }
                tmt = trymanytimes.TryManyTimes(3, self.log)
                tmt(Locate.update_directly,
                    self.tdb, int(locate_id), timeout=30, **locate_args)
                s = "%s: ticket %s/%s, locate %s/%s screened, closed by %s" % (
                    row["ticket_format"], row["ticket_number"], ticket_id,
                    row["locate_names"][i], locate_id, mgr_id)
                self.log.log_event(s, self.verbose)

    def store_changes_for_ticket(self, ticket_id):
        # get appropriate business logic class
        row = self.data._tickets[ticket_id]
        call_center = row["ticket_format"]
        logic = getlogic(call_center, self.logic, self.tdb, self.holiday_list)

        self.post_updates_ado(ticket_id)

        # some locates need to be "screened" (Mantis 2319, 2534)
        t = self.tdb.getticket(ticket_id)
        if logic.screen(row, ticket_id, t.image):
            self.screen_locates(row, ticket_id)

        # insert or update record in ticket_ack
        # TODO: should really be rolled into the ADO transaction... we don't
        # want to ack the ticket if it's not updated!
        if logic.is_ticket_ack(row):
            try:
                self.tdb.insert_ticket_ack(ticket_id)
                self.log.log_event("Added ticket_id %s to ticket_ack" % (ticket_id,))
                # Update the work priority if > 0
                work_priority = logic.get_work_priority(row)
                if work_priority is not None:
                    self.tdb.update_ticket_work_priority(ticket_id, work_priority)
                    self.log.log_event("Changed work priority of ticket_id %s to %d" % (ticket_id, work_priority))
            except:
                ep = errorhandling2.errorpacket(
                     info="Could not add ticket to ticket_ack",
                     ticket_id=ticket_id)
                self.log.log(ep, send=1, write=1, dump=1)

        # NEW: if this is an emergency ticket, then we'll get the ticket
        # image, and the locator, and send it to emergency_emails.
        if logic.isemergency(row):
            try:
                self.handle_emergency_ticket(ticket_id)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info="Error when trying to send emergency ticket")
                self.log.log(ep, send=1, write=1, dump=1)

    #
    # ADO code for updating

    def post_updates_ado(self, ticket_id, do_not_mark_before_date=None):
        self.log.log_event("Updating ticket %s/#%s/%s..." % (
         self.data._tickets[ticket_id]["ticket_format"],
         self.data._tickets[ticket_id]["ticket_number"],
         ticket_id))

        self.tdb.dbado.conn.BeginTrans()
        try:
            self._post_updates_ado(ticket_id, do_not_mark_before_date,
             self.data._tickets[ticket_id]["due_date"])
        except:
            self.tdb.dbado.conn.RollbackTrans()
            # blacklist this ticket
            if not self.is_database_glitch():
                self.append_to_errorfile(ticket_id)
                # if we don't blacklist it, the router will try it again
                # next time

            # log error
            ep = errorhandling2.ErrorPacket()
            ep.set_traceback()
            ep.add(ticket_id=ticket_id) # what else can we log?
            send = 1
            # Check that the value has a find method (string)
            # It should
            if hasattr(ep.exception.args[0],"find"):
                # if the db exception is a deadlock, don't send
                if ep.exception.args[0].find('was deadlocked on lock resources') != -1:
                    send = 0
            self.log.log(ep, send=send, write=1, dump=1)
        else:
            self.tdb.dbado.conn.CommitTrans()
            self.log.log_event(" ...OK")

    def _post_updates_ado(self, ticket_id, do_not_mark_before_date, due_date):
        call_center = self.data._tickets[ticket_id]["ticket_format"]
        tmt = trymanytimes.TryManyTimes(3, self.log)

        # update ticket, if necessary (uses TicketDB.update)

        ticket_args = {}
        if call_center == "Atlanta":
            ticket_args['map_page'] = self.data._tickets[ticket_id]["mapinfo"]
        # set route_area_id, if area_id is known
        area_id = self.data._tickets[ticket_id]["area_id"]
        if area_id:
            ticket_args['route_area_id'] = int(area_id) # was: str
        if do_not_mark_before_date:
            ticket_args['do_not_mark_before'] = do_not_mark_before_date

        if ticket_args:
            tmt(Ticket.update_directly, self.tdb, int(ticket_id), timeout=30,
             **ticket_args)

        # locates and assignments

        for locate_id in self.data._tickets[ticket_id]["locates"]:
            newstatus = self.data._locates[locate_id]
            closed = (newstatus == locate_status.not_a_customer)
            locate_args = {
                'status': newstatus,
                'closed': int(closed), # boolean?
                'closed_how': closed and 'router' or '',
            }

            # set the client id, if appropriate
            if newstatus == locate_status.assigned:
                client_id = self.data._locate_client.get(locate_id, None)
                if client_id:
                    locate_args['client_id'] = client_id

            tmt(Locate.update_directly, self.tdb, int(locate_id), timeout=30,
             **locate_args)

            # assignments must be created.  we might as well use the
            # Assignment class for that.
            locator = self.data._assignments.get(locate_id, None)
            if locator:
                # Check that the locate and locator are in the db
                results = self.tdb.getrecords("locate", locate_id=locate_id)
                if not results:
                    # Need test!
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Locate does not exist", locate_id=locate_id)
                    self.log.log(ep, send=1, write=1, dump=1)
                    continue
                results = self.tdb.getrecords("employee", emp_id=locator)
                if not results:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Employee does not exist", emp_id=locator)
                    self.log.log(ep, send=1, write=1, dump=1)
                    continue
                asg = assignment.Assignment()
                asg.set(locate_id=locate_id, locator_id=locator,
                 added_by="router", workload_date=due_date)
                asg.insert(self.tdb)
            else:
                if newstatus == locate_status.assigned:
                    error = "WARNING: locate_id %s has status -R, but no assignment" % (locate_id,)
                    self.log.log_event(error)
                    self.routerwarning(error, "-R without assignment", ticket_id)


    ###
    ### Helper methods for the main task

    def routerwarning(self, error, subject_hint, ticket_id):
        """ Send a router warning with lots of information, hopefully
            providing clues to the cause of the mishap. """
        c = StringIO()
        print >> c, error
        print >> c
        print >> c, "Data:"
        print >> c, "-----"
        # we're only storing data of one ticket at a time, so this should work:
        print >> c, "Tickets:"
        pprint.pprint(self.data._tickets, stream=c)
        print >> c, "Locates:"
        pprint.pprint(self.data._locates, stream=c)
        print >> c, "Assignments:"
        pprint.pprint(self.data._assignments, stream=c)
        print >> c, "Clients and client codes:"
        pprint.pprint(self.data._locate_client, stream=c)
        pprint.pprint(self.data._locate_client_code, stream=c)

        self.log.log_warning(c.getvalue(),
         call_center=self.data._tickets[ticket_id]['ticket_format'],
         subject_hint=subject_hint)
        self.log.log_event("Sending router warning: " + subject_hint)

    def find_previous_locator(self, ticket_id):
        locator = None  # unknown for the moment
        if self.data._tickets[ticket_id]["mapinfo"]:
            # mapinfo for this ticket is already known. Find the
            # locator we assigned to one of its locates.
            for loc in self.data._tickets[ticket_id]["locates"]:
                locator = self.data._assignments.get(loc, None)
                if locator is None:
                    continue
        return locator

    def find_locator(self, ticket_id, row):
        """ Return (locator, area_id). """
        call_center = row["ticket_format"]
        logic = getlogic(call_center, self.logic, self.tdb, self.holiday_list)

        routingresult = logic.locator(row)

        # some call centers must update the map_page field
        if routingresult.mapinfo:
            self.data._tickets[ticket_id]["mapinfo"] = routingresult.mapinfo

        # tell the world which locator we have found, if any
        s = "%s: ticket %s/%s, locate %s/%s: locator %s found (%s)" % (
            call_center, row["ticket_number"], row["ticket_id"],
            row["client_code"], row["locate_id"],
            routingresult.locator or logic.DEFAULT_LOCATOR,
            routingresult.routing_method or "default")
        if routingresult.data:
            s = s + " (%s)" % (routingresult.data,)
        self.log.log_event(s, self.verbose)

        # return the locator, or, if None, the default locator
        return (routingresult.locator or logic.DEFAULT_LOCATOR,
                routingresult.area_id)

    ###
    ### Lookup methods

    def _get_locates(self):
        return self._get_locates_ado()

    def _get_locates_ado(self):
        timer = tools.Timer()
        timer.start()
        sql = "exec dbo.tickets_to_route_noxml"
        rows = self.tdb.runsql_result(sql, as_strings=False)
        self.log.log_event("There are %d pending locates found on %d tickets" %
         (len(rows), len(self._collect_ticket_ids(rows))))

        for row in rows:
            ticket_id = row['ticket_id'] = str(row['ticket_id'])
            if int(ticket_id) in self.blacklisted_ids:
                continue
            # Convert all values to strings
            for key, value in row.items():
                try:
                    row[key] = tools.ado_to_string(value)
                except:
                    ep = errorhandling2.ErrorPacket()
                    ep.set_traceback()
                    ep.add(field=key, field_value=value, row=row,
                     ticket_id=ticket_id, additional_info="Put ticket_id in "
                     + BUGFILE, blacklisted=ticket_id)
                    ep.add(info="Error converting field value to a string")
                    self.log.log(ep, send=1, write=1, dump=1)
                    # blacklist this ticket_id
                    self.append_to_errorfile(ticket_id)
                    self.blacklisted_ids.append(int(ticket_id))
                    break

        self.remove_blacklisted(rows)
        self.sayln(len(rows), "locates on",
         len(self._collect_ticket_ids(rows)),
         "tickets left after applying blacklist")

        rows_to_return = []
        '''
        If any locates have the high_profile bit set,
        gather up all of the locates on that ticket and add to rows_to_return,
        ignoring do_not_route_until. Consider doing this in sql or in a sp.
        '''
        # find the records in rows that are high profile
        high_profiles = filter(lambda a: int(Locate.load(self.tdb,
                               a['locate_id']).high_profile), rows)
        self.log.log_event("There are %d high profile locates" % len(high_profiles))

        # find the records in rows that have the same ticket_id as the high_profiles
        high_profile_rows = []
        for high_profile in high_profiles:
            for row in rows:
                if row not in high_profile_rows and row['ticket_id'] == high_profile['ticket_id']:
                    high_profile_rows.append(row)

        self.log.log_event("There are now %d high profile locates after association" % len(high_profile_rows))

        # return all rows not in high_profile_rows
        def not_in_high_profile_rows(a): # XXX FIXME
            return a not in high_profile_rows
        rows = filter(not_in_high_profile_rows, rows)
        rows_to_return.extend(high_profile_rows)
        # Check the remaining for do_not_route_until
        d = self.tdb.tabledefs.fields_as_dict('ticket')
        if d.has_key('do_not_route_until'):
            for row in rows:
                ticket = Ticket.load(self.tdb, row['ticket_id'])
                if ticket.do_not_route_until is not None \
                and ticket.kind != 'EMERGENCY':
                    if date.Date(ticket.do_not_route_until) <= date.Date():
                        rows_to_return.append(row)
                else:
                    rows_to_return.append(row)
        else:
            rows_to_return.extend(rows)
        self.log.log_event("There are %d pending locates after filtering" % (len(rows_to_return),))
        timer.stop()
        self.time_get_pending_locates = timer.elapsed()

        return rows_to_return

    def _get_customers(self):
        """ Return a list of customers. """
        self.say("Getting customer list...")
        customers = {}
        result = self.tdb.get_customers()
        if not result:
            raise Exception, "No client codes found"
        for row in result:
            # add to customers dict...
            try:
                client_code, call_center, client_id = \
                 [row["oc_code"], row["call_center"], row["client_id"]]
            except KeyError:
                continue
            try:
                customers[(client_code, call_center)] = client_id
            except:
                # duplicate
                pass

        self.sayln("OK (%d customers found)" % (len(customers)))
        return customers

    @staticmethod
    def _find_client_id(customers, client_code, call_center):
        """ Try and find the given client code and call center in the
            customer dict. When found, return the client id of the given client.
            If nothing found, return an empty string.
        """
        try:
            return customers[(client_code,call_center)]
        except:
            #Not found
            return ""

    ###
    ### handling of BEC01/BEC02 for FCL1

    def handle_BEC01(self):
        """ Inspect the internal dictionaries (_tickets, etc). If an FCL1
            ticket has *only* BEC01 and BEC02 as clients, leave them as such.
            If it has other clients besides these, then mark BEC01/02 as -N.
        """
        count = 0
        # check every ticket
        for ticket_id in self.data._tickets.keys():
            count += self.handle_BEC01_ticket(ticket_id)
        return count

    def handle_BEC01_ticket(self, ticket_id):
        if self.data._tickets[ticket_id]["ticket_format"] != "FCL1":
            return 0    # not for FCL1, can be ignored
        # check clients passed to the router
        locate_ids = self.data._tickets[ticket_id]["locates"]
        if not self._has_BECs(locate_ids, self._BEC01_ids):
            return 0    # no BECs here, don't bother
        BEC_only = self._BEC_only(locate_ids, self._BEC01_ids)

        # wait! there can be clients in the database
        rows = self._find_clients_in_db(ticket_id)
        rows = [row for row in rows
                if row["client_code"] not in ("BEC01", "BEC02")]
        if rows:
            BEC_only = 0    # yes, there are other clients!

        count = 0
        if not BEC_only:
            # BEC01 and BEC02's status must be changed to -N
            for locate_id in locate_ids:
                if self.data._locates[locate_id] == locate_status.assigned:
                    client_id = self.data._locate_client[locate_id]
                    if client_id in self._BEC01_ids:
                        # mark as "not a customer" (-N)
                        self.data._locates[locate_id] = locate_status.not_a_customer
                        self.data._assignments[locate_id] = None
                        count = count + 1

        return count

    def _BEC_only(self, locate_ids, BEC_ids):
        """ Check if BEC01/BEC02 are the only clients of the ticket with the
            given locate_ids. """
        for locate_id in locate_ids:
            if self.data._locates[locate_id] == locate_status.assigned:
                client_id = self.data._locate_client[locate_id]
                if client_id not in BEC_ids:
                    return 0    # aha, there are other clients
        # apparently we didn't find anything else
        return 1

    def _has_BECs(self, locate_ids, BEC_ids):
        for locate_id in locate_ids:
            if self.data._locate_client[locate_id] in BEC_ids:
                return 1
        return 0

    def _find_clients_in_db(self, ticket_id):
        """ Find any locates of the given ticket that are clients. We're
            looking in the database for this. Such records may not have been
            passed to the router when doing a reparsing. """
        return self.tdb.find_clients_for_ticket(ticket_id)

    def append_to_errorfile(self, ticket_id):
        b = blacklist.BlackList(self.log)
        b.write(ticket_id)

    def read_errorfile(self):
        """ Read the error file and return a list of 'blacklisted' ids. """
        b = blacklist.BlackList(self.log)
        return b.read()

    def handle_emergency_ticket(self, ticket_id):
        row = self.data._tickets[ticket_id]
        _, image = self.tdb.getticketimages([ticket_id])[0]
        blanket_email_sent = 0
        for i, locate_id in enumerate(row["locates"]):
            term_id = row['locate_names'][i]
            locator = self.data._assignments.get(locate_id, None)
            if locator:
                locator_name = self.tdb.get_employee_name(locator)
                self.send_emergency_email(row["ticket_number"], image,
                 row["ticket_format"], locator_name, term_id)
                # send "blanket" emergency email only once
                if not blanket_email_sent:
                    self.send_emergency_email(row['ticket_number'], image,
                     row['ticket_format'], locator_name) # no term id used here
                    blanket_email_sent = 1

    def send_emergency_email(self, ticket_number, image, call_center,
                             locator_name, term_id=None):
        """ Send an emergency email.
            If <term_id> is set to the name of a locate, then that locate
            will be looked up in the EmailByClient object (associated with the
            notification_email_by_client table) and we will get the email
            addresses from there.  Otherwise a "blanket" emergency email is
            sent, using the addresses in call_center.emergency_email.
        """
        if term_id:
            indicator = "(notifications for term id %s)" % (term_id,)
        else:
            indicator = "(general notification)"
        self.log.log_event("Sending emergency email for ticket %s (%s) (%s) %s" % (
         ticket_number, call_center, locator_name, indicator))
        subject = "Emergency ticket %s, assigned to %s" % (
                  ticket_number, locator_name)
        #host = self.config.smtp
        #fromaddr = self.config.from_address
        body = image

        # determine 'to' addresses
        if term_id:
            toaddrs = self.ebc.get_emails(call_center, term_id, 'emergency')
        else:
            # send to all emergency addresses
            toaddrs = self.emails[call_center].emergency
        if not toaddrs:
            #self.sayln("No emergency emails found for this call center!")
            return

        try:
            if self.log.lock:
                return  # don't send it
            mail2.sendmail(self.config.smtp_accs[0], toaddrs, subject, body)
        except socket.timeout:
            ep = errorhandling2.errorpacket()
            ep.add(info="Error when sending emergency email (timeout)")
            self.log.log(ep, send=0, dump=1 and self.verbose, write=1)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Error when sending emergency email")
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)

    def safe_is_ticket_ack(self, row):
        """ Attempts to determine whether the ticket/row matches the
            ticket_ack rules.  Because the row possibly has incomplete
            information, we wrap it in an exception.  If an error occurs,
            returns 0. """
        logic = getlogic(row['ticket_format'], self.logic, self.tdb, self.holiday_list)
        try:
            return logic.is_ticket_ack(row)
        except:
            return 0

    def group_results(self, rows):
        """ Group a list of records (as returned by get_locates_noxml) by
            ticket_id. Returns a list of lists. """
        d = {}
        for row in rows:
            ticket_id = row['ticket_id']
            if d.has_key(ticket_id):
                d[ticket_id].append(row)
            else:
                d[ticket_id] = [row]

        # sort by kind, so we get emergencies first.

        # each key in d is a ticket_id. each value is a list of dicts
        # containing ticket/locate info.

        def sort_value(value):
            if not value:
                return 3
            row = value[0] # first item in list; it's a dict or a ticket
            kind = row.get('kind', 'NORMAL')
            if kind.startswith('EMER'):
                return 0
            elif self.safe_is_ticket_ack(row):
                return 1
            else:
                return 2

        # decorate / sort / undecorate
        z = [(sort_value(value), key, value) for (key, value) in d.items()]
        z.sort()    # first on kind, then key
        y = [value for (kind, key, value) in z]
        return y

    @staticmethod
    def is_database_glitch():
        msg = errorhandling2.get_exception_message()
        return errorhandling2.is_database_glitch(msg)


if __name__ == "__main__":

    RUNONCE = 0
    configfile = ""
    profiler = 0

    opts, args = getopt.getopt(sys.argv[1:], "1c:gp?", ["data="])
    for o, a in opts:
        if o == "-1":
            RUNONCE = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)
        elif o == "-c":
            configfile = a
        elif o == "-p":
            RUNONCE = profiler = 1
            # profiling only makes sense if it doesn't run infinitely
        elif o == "--data":
            print "Data dir:", a

    # only one instance of this *program* is allowed
    biz_id = datadir.datadir.get_id()
    mutex_name = "ticketrouter:" + biz_id
    windows_tools.setconsoletitle("ticketrouter")

    try:
        log = errorhandling_special.toplevel_logger(configfile, "Ticketrouter",
              "Ticketrouter Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "Ticketrouter")
        sys.exit(1)

    check_python_version(log)
    try:
        tr = None
        dbado = dbinterface_ado.DBInterfaceADO()
        with mutex.MutexLock(dbado, mutex_name):
            tr = TicketRouter(60, configfile=configfile, dbado=dbado)
            program_status.write_status(mutex_name)
            tr.run(runonce=RUNONCE)
            if profiler:
                show_profile_report("router_profile_", '', tr.tdb)
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

    try:
        if tr and tr.log:
            tr.log.force_send()
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Error sending queued messages")
        log.log(ep, write=1, dump=1)
    