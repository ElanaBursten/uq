# config_dom.py
# Configuration file reader
# XXX DEPRECATED. Use config_etree.py instead.

from xml.dom.minidom import parseString
import getopt
import pprint
import re
import string
import sys
import traceback
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import datadir
import responder_config_data as rcd
import xmlconfigreader

class ConfigurationError(Exception):
    pass

def U(u):
    """ Simple Unicode-to-utf8 decoder. """
    if isinstance(u, unicode):
        return u.encode("utf-8")
    else:
        return u

class Configuration:

    def __init__(self, filename="config.xml", verbose=1):
        file = datadir.datadir.open(filename)
        data = file.read()
        file.close()
        self.raw_xml = data # keep for later use
        self.responderconfigdata = rcd.ResponderConfigData()

        try:
            dom = parseString(data)
        except Exception, e:
            if verbose:
                traceback.print_exc()
            c = StringIO()
            traceback.print_exc(file=c)
            errormsg = c.getvalue()

            # a bit of an ugly hack, but we need to raise a ConfigurationError,
            # *and* add the text of the "original" traceback to it
            try:
                raise ConfigurationError, \
                      "Error when parsing XML file (%s)" % (filename,)
            except ConfigurationError, e:
                e.errormsg = errormsg
                raise

        t = xmlconfigreader.node_as_tuple(dom, "database",
            ("host", "root", "login", "password"))
        self.host, self.root, self.login, self.password = t

        self.processes = []
        # contains sublists [incomingdir, format, processed, error]

        tpnode = dom.getElementsByTagName("ticketparser")[0]
        self.processes = xmlconfigreader.node_as_dictlist(tpnode, "process",
                         ("incoming", "format", "processed", "error", "group",
                          "attachments", "attachment_dir", "attachment_proc_dir",
                          "upload_location", "attachment_user"))
        # note: attachment_dir and attachment_proc_dir can be empty
        # set 'attachment' to 0 or 1 (integer, not string)
        for p in self.processes:
            a = p['attachments']
            if not a:
                a = '0'
            p['attachments'] = int(a)

        self.admins = xmlconfigreader.node_as_dictlist(dom, "admin",
                      ("name", "email", "ln"))
        for admin in self.admins:
            if not admin['ln']:
                admin['ln'] = '1' # default is ln=1

        self.logdir = xmlconfigreader.node_as_string(dom, "logdir", "name")
        self.polygon_dir = xmlconfigreader.node_as_string(dom, "polygon_dir", "name")

        self.schemafile = xmlconfigreader.node_as_string(dom, "schema", "name")

        self.read_smtp_data(dom)
        self.read_irthresponder_data(dom)
        self.read_ftpresponder_data(dom)
        self.read_xcelresponder_data(dom)
        self.read_xmlhttpresponder_data(dom)
        self.read_emailresponder_data(dom)
        self.determine_nodelete(dom)
        self.read_watchdog_data(dom)
        self.read_archiver_data(dom)
        self.read_ticketrouter_data(dom)
        self.read_ticketparser_data(dom)

        # ADO connection to database
        self.ado_database = xmlconfigreader.node_as_dict(dom, "ado_database",
                            ("host", "database", "login", "password"))

        # integrity check emails
        for anode in dom.getElementsByTagName("integrity_check"):
            email_list_raw = U(anode.getAttribute("emails"))
            self.ic_emails = string.split(email_list_raw, ";")

        # as of Mantis #3022, this can no longer be specified in config.xml,
        # but the attribute can still be overridden for testing purposes.
        self.cc_routing_file = default = "rules/cc_routing_list.tsv"
        #for z in dom.getElementsByTagName("ticketrouter"):
        #    d = xmlconfigreader.node_as_dict(z, "cc_routing_list", ["filename"])
        #    self.cc_routing_file = d.get("filename", default)

        # when everything is read, gather skip data
        self.responderconfigdata.gather_skip_data()
        self.responderconfigdata.gather_status_code_skip_data()

        # unlink so garbage collection can do its job
        dom.unlink()

    def validate(self):
        pass # implemented in ElementTree version

    def read_smtp_data(self, dom):
        smtp_servers = []
        for node in dom.getElementsByTagName("smtp"):
            smtp = U(node.getAttribute("host"))
            from_address = U(node.getAttribute("from")) \
                           or "ErrorHandler.One <logger@localhost>"
            smtp_servers.append({'host': smtp, 'from': from_address})
        if smtp_servers:
            self.smtp = smtp_servers[0]['host']
            self.from_address = smtp_servers[0]['from']
        self.smtp_servers = smtp_servers

    def read_irthresponder_data(self, dom):
        self.responders = {}
        for rnode in dom.getElementsByTagName("responder"):
            for inode in rnode.getElementsByTagName("irth"):
                for cnode in inode.getElementsByTagName("callcenter"):
                    r = {}
                    r["name"] = U(cnode.getAttribute("name"))
                    r["server"] = U(cnode.getAttribute("server"))
                    r["port"] = U(cnode.getAttribute("port"))
                    r["ack_dir"] = U(cnode.getAttribute("ack_dir"))
                    r["ack_proc_dir"] = U(cnode.getAttribute("ack_proc_dir"))
                    r["clientbased"] = int(U(cnode.getAttribute("clientbased"))
                     or "0")
                    r["all_versions"] = int(U(cnode.getAttribute("all_versions"))
                     or "0")
                    r["send_emergencies"] = int(U(cnode.getAttribute("send_emergencies")) or "1")
                    r["send_3hour"] = int(U(cnode.getAttribute("send_3hour")) or "1")
                    r["logins"] = []
                    for lnode in cnode.getElementsByTagName("logon"):
                        d = {}
                        d["login"] = U(lnode.getAttribute("login"))
                        d["password"] = U(lnode.getAttribute("password"))
                        r["logins"].append(d)

                    mappings = self._read_responder_mappings(cnode)
                    r["mappings"] = mappings

                    translations = self._read_responder_translations(cnode)
                    r["translations"] = translations

                    r['clients'] = self._read_responder_clients(cnode)

                    skip = self._read_responder_skip(cnode)
                    r['skip'] = skip

                    status_code_skip = self._read_responder_status_code_skip(cnode)
                    r['status_code_skip'] = status_code_skip

                    self.responders[r["name"]] = r
                    self.responderconfigdata.add(r['name'], 'irth', r)

    def _read_responder_mappings(self, node):
        """ Read the <code_mappings> section of a responder, and return a
            dictionary with the results. """
        mappings = {}
        for ccnode in node.getElementsByTagName("code_mappings"):
            for mnode in ccnode.getElementsByTagName("mapping"):
                uq_code = U(mnode.getAttribute("uq_code"))
                response = U(mnode.getAttribute("response"))
                explanation = U(mnode.getAttribute("explanation"))
                mappings[uq_code] = (response, explanation)
        return mappings

    def _read_responder_translations(self, node):
        translations = {}
        for trnode in node.getElementsByTagName("translate_term"):
            tfrom = U(trnode.getAttribute("from"))
            tto = U(trnode.getAttribute("to"))
            translations[tfrom] = tto
        return translations

    def _read_responder_skip(self, node):
        skip = []
        for trnode in node.getElementsByTagName("skip"):
            for termnode in trnode.getElementsByTagName("term"):
                name = U(termnode.getAttribute("name"))
                if name and name not in skip:
                    skip.append(name)
        return skip

    def _read_responder_status_code_skip(self, node):
        skip = []
        for trnode in node.getElementsByTagName("status_code_skip"):
            for termnode in trnode.getElementsByTagName("code"):
                name = U(termnode.getAttribute("value"))
                if name and name not in skip:
                    skip.append(name)
        return skip

    def _read_responder_clients(self, node):
        clients = []
        for trnode in node.getElementsByTagName("clients"):
            for termnode in trnode.getElementsByTagName("term"):
                name = U(termnode.getAttribute("name"))
                if name and name not in clients:
                    clients.append(name)
        return clients
        # XXX how do we express 'clients=*' other than having no <clients>
        # section?

    def read_xmlhttpresponder_data(self, dom):
        self.xmlhttpresponders = {}
        for rnode in dom.getElementsByTagName("responder"):
            for xnode in rnode.getElementsByTagName("xmlhttp"):
                for cnode in xnode.getElementsByTagName("callcenter"):
                    d = {}
                    d['name'] = U(cnode.getAttribute("name"))
                    d['id'] = U(cnode.getAttribute("id"))
                    d['initials'] = U(cnode.getAttribute("initials") or "UQ")
                    d['post_url'] = U(cnode.getAttribute("post_url"))
                    d['resp_url'] = U(cnode.getAttribute("resp_url"))
                    d["ack_dir"] = U(cnode.getAttribute("ack_dir"))
                    d["ack_proc_dir"] = U(cnode.getAttribute("ack_proc_dir"))
                    d['all_versions'] = int(U(cnode.getAttribute('all_versions')) or '0')
                    d['send_emergencies'] = int(U(cnode.getAttribute('send_emergencies')) or '1')
                    d['send_3hour'] = int(U(cnode.getAttribute('send_3hour')) or '1')

                    mappings = self._read_responder_mappings(cnode)
                    d["mappings"] = mappings
                    translations = self._read_responder_translations(cnode)
                    d["translations"] = translations
                    skip = self._read_responder_skip(cnode)
                    d['skip'] = skip
                    status_code_skip = self._read_responder_status_code_skip(cnode)
                    d['status_code_skip'] = status_code_skip
                    d['clients'] = self._read_responder_clients(cnode)

                    # ...more...
                    self.xmlhttpresponders[d['name']] = d
                    self.responderconfigdata.add(d['name'], 'xmlhttp', d)

    def read_emailresponder_data(self, dom):
        self.emailresponders = {}
        for rnode in dom.getElementsByTagName("responder"):
            for xnode in rnode.getElementsByTagName("email"):
                for cnode in xnode.getElementsByTagName("callcenter"):
                    d = {}
                    d['name'] = U(cnode.getAttribute("name")) # call center
                    d['tech_code'] = U(cnode.getAttribute("tech_code"))
                    d['resp_email'] = U(cnode.getAttribute('resp_email'))
                    d['sender'] = U(cnode.getAttribute('sender'))
                    d['subject'] = U(cnode.getAttribute('subject'))
                    d['all_versions'] = int(U(cnode.getAttribute('all_versions')) or '0')
                    d['send_emergencies'] = int(U(cnode.getAttribute('send_emergencies')) or '1')
                    d['send_3hour'] = int(U(cnode.getAttribute('send_3hour')) or '1')
                    d["ack_dir"] = U(cnode.getAttribute("ack_dir"))
                    d["ack_proc_dir"] = U(cnode.getAttribute("ack_proc_dir"))

                    mappings = self._read_responder_mappings(cnode)
                    d["mappings"] = mappings
                    translations = self._read_responder_translations(cnode)
                    d["translations"] = translations
                    skip = self._read_responder_skip(cnode)
                    d['skip'] = skip
                    status_code_skip = self._read_responder_status_code_skip(cnode)
                    d['status_code_skip'] = status_code_skip
                    d['clients'] = self._read_responder_clients(cnode)

                    # ...more...
                    self.emailresponders[d['name']] = d
                    self.responderconfigdata.add(d['name'], 'email', d)

    def determine_nodelete(self, dom):
        self.nodelete = []
        for rnode in dom.getElementsByTagName("responder"):
            for nnode in dom.getElementsByTagName("nodelete"):
                spam = xmlconfigreader.node_as_dictlist(nnode, "callcenter",
                       ["code"])
                self.nodelete = [x["code"] for x in spam]

        # responders that have configuration info, should always be in the
        # nodelete list. make sure this is so:
        allkeys = self.responders.keys() + self.ftpresponders.keys() + \
                  self.xcelresponders.keys() + self.xmlhttpresponders.keys()
        for cc in allkeys:
            if cc not in self.nodelete:
                self.nodelete.append(cc)

    def read_ftpresponder_data(self, dom):
        self.ftpresponders = {}
        self.ftp_output_dir = ""
        for rnode in dom.getElementsByTagName("responder"):
            for inode in rnode.getElementsByTagName("ftp"):
                # output dir
                for fnode in inode.getElementsByTagName("output_dir"):
                    self.ftp_output_dir = U(fnode.getAttribute('name'))

                # call center data
                for cnode in inode.getElementsByTagName("callcenter"):
                    r = xmlconfigreader.get_dict(cnode,
                        ('name', 'server', 'login', 'password', 'outgoing_dir',
                         'temp_dir', 'file_ext', 'method'))

                    # mappings, translations, skip, same as IRTH responder:

                    mappings = self._read_responder_mappings(cnode)
                    r["mappings"] = mappings

                    translations = self._read_responder_translations(cnode)
                    r["translations"] = translations

                    r['clients'] = self._read_responder_clients(cnode)

                    skip = self._read_responder_skip(cnode)
                    r['skip'] = skip
                    status_code_skip = self._read_responder_status_code_skip(cnode)
                    r['status_code_skip'] = status_code_skip

                    r['send_emergencies'] = int(U(cnode.getAttribute('send_emergencies')) or '1')
                    r['send_3hour'] = int(U(cnode.getAttribute('send_3hour')) or '1')

                    exclude_states = []
                    for trnode in cnode.getElementsByTagName("exclude_state"):
                        state = U(trnode.getAttribute("name"))
                        exclude_states.append(state)
                    r['exclude_states'] = exclude_states

                    self.ftpresponders[r['name']] = r
                    self.responderconfigdata.add(r['name'], 'ftp', r)

    def read_xcelresponder_data(self, dom):
        self.xcelresponders = {}
        for respnode in dom.getElementsByTagName("responder"):
            for xcelnode in respnode.getElementsByTagName("xcel"):
                # call center data
                for ccnode in xcelnode.getElementsByTagName("callcenter"):
                    r = xmlconfigreader.get_dict(ccnode,
                        ('name', 'output_dir', 'method'))

                    # get term ids
                    termids = []
                    for termnode in ccnode.getElementsByTagName("term"):
                        name = U(termnode.getAttribute("name"))
                        termids.append(name)
                    r['termids'] = termids
                    r['clients'] = r['termids']

                    # emails
                    z = xmlconfigreader.node_as_dictlist(ccnode, 'recipient',
                        ('name', 'email'))
                    r['recipients'] = z

                    self.xcelresponders[r['name']] = r
                    self.responderconfigdata.add(r['name'], 'xcel', r)

    def read_watchdog_data(self, dom):
        self.watchdog = {}
        try:
            watchdognode = xmlconfigreader.find_node(dom, ['root', 'watchdog'])
        except:
            return

        # <old_responses>
        for oldrespnode in watchdognode.getElementsByTagName('old_responses'):
            threshold = int(U(oldrespnode.getAttribute('threshold')))
            self.watchdog['old_responses'] = {'threshold': threshold,
                                              'ignore': []}
            for ignorenode in oldrespnode.getElementsByTagName('ignore'):
                callcenter = U(ignorenode.getAttribute('callcenter'))
                self.watchdog['old_responses']['ignore'].append(callcenter)

        filename = ""
        for onode in watchdognode.getElementsByTagName('output'):
            filename = U(onode.getAttribute('filename'))
        self.watchdog['output_filename'] = filename

    def read_archiver_data(self, dom):
        self.archiver = xmlconfigreader.node_as_dict(dom, "archiver",
                        ("root", "tar_path", "bzip2_path", "archive_dir"))
        dirs = []
        for anode in dom.getElementsByTagName('archiver'):
            for adnode in anode.getElementsByTagName('additional_dir'):
                dirname = U(adnode.getAttribute('path'))
                dirs.append(dirname)
        self.archiver['additional_dirs'] = dirs

    def read_ticketrouter_data(self, dom):
        numrounds = 1
        for routernode in dom.getElementsByTagName('ticketrouter'):
            numrounds = U(routernode.getAttribute('numrounds'))
        self.ticketrouter = {'numrounds': numrounds and int(numrounds) or 1}

    def read_ticketparser_data(self, dom):
        numrounds = 1
        wait = -1
        for parsernode in dom.getElementsByTagName('ticketparser'):
            numrounds = U(parsernode.getAttribute('numrounds'))
            wait = U(parsernode.getAttribute('wait'))
        self.ticketparser = {'numrounds': numrounds and int(numrounds) or 1,
                             'wait': wait and int(wait) or -1}

    def getIRTHresponders(self):
        """ Return a list of call centers that we have responders for. """
        return self.responders.keys()
        # XXX does this need to include FTP and XCEL responders as well? A: NO!

    def getXMLHTTPresponders(self):
        """ Return a list of call centers that we have responders for. """
        return self.xmlhttpresponders.keys()

    def get_groups(self):
        # get groups from configuration
        groups = {}
        for proc in self.processes:
            group = proc[4]
            groups[group] = groups.get(group, []) + [proc]

        groups_sorted = groups.keys()
        groups_sorted.sort()

        return groups_sorted

    def switch_smtp_servers(self):
        """ If there are multiple SMTP servers, 'rotate' the list and set
            a new self.smtp and self.from_address. """
        if len(self.smtp_servers) >= 2:
            self.smtp_servers = self.smtp_servers[1:] + self.smtp_servers[:1]
            self.smtp = self.smtp_servers[0]['host']
            self.from_address = self.smtp_servers[0]['from']
            return 1
        else:
            return 0

_configuration = None

def getConfiguration():
    global _configuration
    if _configuration == None:
        _configuration = Configuration()
    return _configuration

def setConfiguration(config):
    global _configuration
    _configuration = config


x_re_admin = re.compile(
 "<admin *name *= *['\"](.*?)['\"]\s+email *= *['\"](.*?)['\"]",
 re.MULTILINE)
x_re_smtp = re.compile(
 "<smtp *host *= *['\"](.*?)['\"]\s+from *= *['\"](.*?)['\"]",
 re.MULTILINE)

def extract_error_emails(data):
    """ Extract error emails in case the regular parsing doesn't work. """
    admins = []
    for name, email in re.findall(x_re_admin, data):
        d = {"name": name, "email": email}
        admins.append(d)
    return admins

def extract_smtp_data(data):
    """ Extract SMTP data from raw XML. Returns a tuple (smtphost,
        from_address). """
    m = x_re_smtp.search(data)
    if m:
        smtp = m.group(1)
        from_address = m.group(2)
        return smtp, from_address
    else:
        raise ConfigurationError, "SMTP data could not be retrieved"



if __name__ == '__main__':

    def default_printout():
        import pprint

        c = getConfiguration()
        print c.host, c.root
        print "processes:"
        pprint.pprint(c.processes)
        print "admins:"
        pprint.pprint(c.admins)
        print "logdir:", c.logdir
        print "Responder data:"
        pprint.pprint(c.responders)
        print "FTP responder output dir:", `c.ftp_output_dir`
        print "FTP responders:"
        pprint.pprint(c.ftpresponders)
        print "XCEL responders:"
        pprint.pprint(c.xcelresponders)
        print "XMLHTTP responders:"
        pprint.pprint(c.xmlhttpresponders)
        print ".", c.smtp
        print "schemafile:", c.schemafile
        print "These call centers use the IRTH responder:", c.getIRTHresponders()
        print "These call centers should not be deleted:", c.nodelete
        print "archiver:", c.archiver
        print "database:", c.host, c.root, `c.login`, `c.password`
        print "ado_database:", c.ado_database
        print "IC emails:", c.ic_emails
        print "cc_routing_file:", c.cc_routing_file
        print "Router:", c.ticketrouter
        print "Parser:", c.ticketparser
        print "Polygon dir:", `c.polygon_dir`

        print "--Extracted with regular expressions:"
        data = datadir.datadir.open("config.xml", "rb").read()
        print "Admin emails:", extract_error_emails(data)
        print "SMTP data:", extract_smtp_data(data)
        print "Responder config data:"
        pprint.pprint([t[0:2] for t in c.responderconfigdata.data])
        print "Responder skip data per call center:"
        pprint.pprint(c.responderconfigdata.skip)

    def print_callcenter(callcenter):
        cfg = getConfiguration()
        for proc in cfg.processes:
            if proc['format'] == callcenter:
                print proc

        # also print responder stuff, if possible

    def print_groups():
        cfg = getConfiguration()
        groups = {}
        for proc in cfg.processes:
            group = proc['group']
            groups[group] = groups.get(group, []) + [proc]

        groups_sorted = groups.keys()
        groups_sorted.sort()

        for group in groups_sorted:
            print "Group %s:" % (group,)
            for proc in groups[group]:
                print proc

    def print_watchdog():
        cfg = getConfiguration()
        pprint.pprint(cfg.watchdog)

    def print_archiver():
        cfg = getConfiguration()
        pprint.pprint(cfg.archiver)

    #
    # parse command line options, if any

    LONG_OPTIONS = [
        "groups",       # show all groups
        "group=",       # show only a certain group
        "callcenter=",  # show call center information
        "watchdog",
        "archiver",
        "xmlhttp",
    ]
    opts, args = getopt.getopt(sys.argv[1:], "", LONG_OPTIONS)

    if not (opts or args):
        default_printout()
        sys.exit(0)

    for o, a in opts:
        if o == '--groups':
            print_groups()
        elif o == "--group":
            pass
        elif o == "--callcenter":
            print_callcenter(a)
        elif o == "--watchdog":
            print_watchdog()
        elif o == "--archiver":
            print_archiver()
        elif o == "--xmlhttp":
            cfg = getConfiguration()
            pprint.pprint(cfg.xmlhttpresponders)

