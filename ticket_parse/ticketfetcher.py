# ticketfetcher.py
# Defines "ticket fetchers", which do:
# - access a source to get a raw ticket from it
# - processing of the ticket is left to other code
# - after processing, successful or not, the confirm() method needs to be
#   called to clean up. (this could mean e.g. remove the ticket from an email
#   box, move it to a different directory (if a file), etc.)

class TicketFetcher:
    def __init__(self):
        pass
    def get_next(self, all=False):
        raise NotImplementedError("Override in subclasses")
    def confirm(self):
        raise NotImplementedError("Override in subclasses")

class TicketFetcherFile(TicketFetcher):
    """ Get the next ticket as a file from a directory. """
    def __init__(self, feedhandler, call_center):
        self.feedhandler = feedhandler
        self.call_center = call_center
    def get_next(self, all=False):
        filenames = self.feedhandler.get_all_files()
        if all:
            return filenames
        if filenames:
            return filenames[0] # a FileInfo object
        else:
            return None # indicates no ticket is waiting
    def confirm(self, success):
        """ Confirm successful parsing by moving ticket to processed dir. """
        if success:
            pass
            # move to processed dir
        else:
            pass
            # move to error dir

class TicketFetcherEmail(TicketFetcher):
    """ Get the next ticket (plus any attachments) by checking an email
        account and fetching the next email, if any. """
    def __init__(self, pop3info):
        self.pop3info = pop3info
    def get_next(self, all=False):
        pass
    def confirm(self, success):
        """ Confirm successful parsing by deleting the ticket from the POP3
            server and closing the connection. """
        if success:
            pass
            # delete from mailbox
        else:
            pass
            # do nothing; leave connection open

