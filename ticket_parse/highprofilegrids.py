# highprofilegrids.py

class HighProfileGrids:
    """ Load records from the hp_grid table and check if grids on a ticket
        match them. """

    def __init__(self, tdb):
        self.data = {}
        self.tdb = tdb
        self.load()

    def load(self):
        rows = self.tdb.dbado.getrecords('hp_grid')
        for row in rows:
            call_center = row['call_center']
            client_code = row['client_code']
            grid = row['grid']
            d = self.data.get(call_center, {})
            try:
                d = self.data[call_center]
            except KeyError:
                self.data[call_center] = {client_code: [grid]}
            else:
                try:
                    e = d[client_code]
                except KeyError:
                    d[client_code] = [grid]
                else:
                    e.append(grid)

    def get(self, call_center, client_code):
        try:
            return self.data[call_center][client_code]
        except:
            return []

    def check(self, ticket, ticket_id, ticket_version_id, clients, callback):
        """ Check for each client on the given ticket if it has high profile
            grids, and if so, perform an action (by calling the <callback>
            function).

            - <ticket> is a valid Ticket instance.
            - <clients> is a *list* of clients (Client instances) for the
              appropriate call center.
            - <callback> is a function that will be called if high profile grids
              are found on this ticket.  It must have the following signature:
              f(ticket, ticket_id, locate, grids_found, grid_email).
        """
        call_center = ticket.ticket_format
        for c in ticket.locates:
            grids = self.get(call_center, c.client_code)
            if grids:
                # find high profile grids that are on ticket
                relevant_grids = []
                for grid in ticket.grids:
                    if grid in grids:
                        relevant_grids.append(grid)
                # if we find relevant tickets, call callback function
                if relevant_grids:
                    # find grid_email for this client
                    grid_email = ""
                    for cl in clients:
                        if cl.client_code == c.client_code:
                            grid_email = cl.grid_email or ""
                            break
                    # call callback function to "do something" (e.g. send mail)
                    result = callback(ticket, ticket_id, c, relevant_grids,
                             grid_email)
                    # log that we "did something", but only if result is ok
                    if result:
                        self.log(self.tdb, ticket_id, c.client_code,
                         ticket_version_id)

    def has_hp_grids(self, ticket, client_dict):
        """ Simple check to see if a ticket has any HP grids at all.  To be
            used by the ticket_alert system.  Returns the first grid found,
            which can be interpreted as a boolean as well.
            <client_dict> is a dict as returned by TicketDB.get_clients_dict.
            It's used to check if clients are active.  If None, this check is
            skipped.
        """
        call_center = ticket.ticket_format
        if client_dict is not None:
            clients = client_dict.get(call_center, [])
            client_names = [cl.client_code for cl in clients]

        for c in ticket.locates:
            # check if this client is in client_dict; if not, ignore
            # (this way we skip inactive clients)
            # however, if client_dict=None, skip this test.
            if client_dict is not None:
                if c.client_code not in client_names:
                    continue
            # get HP grids associated with this call center and client code
            hp_grids = self.get(call_center, c.client_code)
            for grid in ticket.grids:
                if grid in hp_grids:
                    return grid # aha, found one

        # didn't find anything
        return None

    def log(self, tdb, ticket_id, client_code, ticket_version_id):
        tdb.dbado.insertrecord('ticket_grid_log', ticket_id=ticket_id,
            client_code=client_code, ticket_version_id=ticket_version_id)

    def get_hp_grid_clients(self, ticket, client_dict):
        """ Return a list of locates (found on the ticket) that have HP grids
            associated with them.  (The list returns the actual Locate objects,
            not client codes.  This is done so we can do e.g. loc.alert = 1.)
            <client_dict> can be None, in which case it will be ignored, or
            it can be a list of clients, to check if a client is active.
        """
        found = []

        call_center = ticket.ticket_format
        if client_dict is not None:
            clients = client_dict.get(call_center, [])
            client_names = [cl.client_code for cl in clients]

        for loc in ticket.locates:
            # check if this client is in client_dict; if not, ignore
            # (this way we skip inactive clients)
            # however, if client_dict=None, skip this test.
            if client_dict is not None:
                if loc.client_code not in client_names:
                    continue
            # get HP grids associated with this call center and client code
            hp_grids = self.get(call_center, loc.client_code)
            for grid in ticket.grids:
                if grid in hp_grids:
                    found.append(loc)
                    break # go to next locate

        return found

