# attachment.py
# This code is not about sending email attachments; rather, it deals with
# uploading attachments to a server and writing attachment records to the
# database.

import cStringIO
import ftplib
import os
import random
import string
import uu
import tempfile
import subprocess
#
import date
import ftptools
import sqlmap
import static_tables
import errorhandling2
import filetools

ATTACHMENT_HEADER = "[Attachment]"
ATTACHMENT_BATCH_FILENAME_PREFIX = "upload-"

#
# reading and writing attachments (receiver)

def write_attachment(filename, att_filename, data):
    f = open(filename, 'w')
    print >> f, ATTACHMENT_HEADER
    print >> f, att_filename
    c = cStringIO.StringIO(data)
    uu.encode(c, f, name=att_filename)
    f.close()

def read_attachment(filename):
    f = open(filename, 'r')
    return read_attachment_from_file(f)

def read_attachment_from_file(f):
    header = f.readline()
    assert header.strip() == ATTACHMENT_HEADER, "Invalid attachment (no header)"
    filename = f.readline().strip()
    c = cStringIO.StringIO()
    data = uu.decode(f, c)
    return filename, c.getvalue()
    # FIXME (?) -- return an Attachment instance instead

def read_attachment_from_text(text):
    c = cStringIO.StringIO(text)
    return read_attachment_from_file(c)

#
# uploading attachments

def upload_mutex_name(att_batch_filename):
    return os.path.basename(att_batch_filename)

def upload_attachment(att_batch_filename, logger=None):
    """ Run an attachment batch file to upload an attachment. """

    if logger:
        assert isinstance(logger, errorhandling2.ErrorHandler), 'Invalid logger'
        logger.log_event("Running batch file to add attachment: %r" %
                         att_batch_filename)
    # run attachment importer and capture output
    # need temp file with unique name; StringIO is not allowed
    _, outfn = tempfile.mkstemp()
    with open(outfn, 'w') as out:
        error_level = subprocess.call(att_batch_filename, shell=False,
                      stdout=out) # write output to temp file 'outfn'
    with open(outfn, 'r') as f:
        # weave output into log
        logger.log_event("Output from attachment importer:")
        lines = f.readlines()
        for line in lines:
            logger.log_event("> " + line.rstrip())
    filetools.remove_file(outfn)

    if error_level in [0, 1, 2]:
        logger.log_event("Deleting attachment batch file: %s" % att_batch_filename)
        filetools.remove_file(att_batch_filename)
    else:
        if error_level == 3:
            info = """\
Error adding attachment, using batch file to run QMAttachmentImport.exe
(error_level 3). See QMAttachmentImport's log for more details. The
attachment_uploader.py cron process should automatically retry the batch file.
It can also be retried manually, then deleted if successful."""
        elif error_level == 4:
            info = """\
Error adding attachment, using batch file to run QMAttachmentImport.exe
(error_level 4). Probably due to invalid QMAttachmentImport commandline
arguments. See QMAttachmentImport's log for more details. The
attachment_uploader.py cron process should automatically retry the batch file.
It can also be retried manually, then deleted if successful."""
        else:
            info = "Error adding attachment using batch file (error_level %d)"\
              % (error_level)
        ep = errorhandling2.errorpacket()
        ep.add(info=info)
        ep.add(cmdline=att_batch_filename)
        logger.log(ep, send=1, dump=1 and logger.verbose, write=1)

def gen_unique_filename(ticket_id, extension):
    """ Generate a random filename of the form
        T<ticket_id>-<16 random alphanumeric characters>.<extension>
    """
    alpha = string.ascii_uppercase + string.digits
    if not extension.startswith("."):
        extension = "." + extension
    random_stuff = string.join([random.choice(alpha) for i in range(16)], "")
    return "T%s-%s%s" % (ticket_id, random_stuff, extension)

#
# registering attachment records

class Attachment(sqlmap.SQLMap):
    __table__ = "attachment"
    __key__ = "attachment_id"
    __fields__ = static_tables.attachment

def create_attachment_record(tdb, id, upload_filename, orig_filename,
                             extension, size, user, foreign_type=1):
    """ Create a record in the attachment table.  <upload_filename> must be
        unique (see gen_unique_filename()).
        By default, the attachment will be associated with a ticket (id ==
        ticket_id, foreign_type == 1), but this can be changed to e.g. work
        orders.
    """
    att = Attachment()
    now = date.Date().isodate()
    att.set(filename=upload_filename,
            foreign_type=foreign_type, # 1 = ticket, 7 = work order
            foreign_id=id, # e.g. ticket_id, wo_id, etc.
            orig_filename=orig_filename,
            extension=extension,
            orig_file_mod_date=now,
            attach_date=now,
            comment="Uploaded by ticket parser",
            upload_date=now,
            size=size,
            attached_by=user,
            modified_date=now,
            active=1)
    att_id = att.insert(tdb)
    return att_id

#
# finding attachments (used by main.py)

def find_attachments(dir, starts_with):
    """ Find attachment files in <dir> that start with the given string.
        Return a list of fully qualified filenames. """
    names = os.listdir(dir)
    names = [name for name in names if name.startswith(starts_with)]
    # create "full" filenames (including path)
    return [os.path.join(dir, name) for name in names]

