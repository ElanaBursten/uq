# import_grids.txt

import dbinterface_ado
import sys

call_center = "FCL1"
client = "PNG05"
filename = "c:/internet/download/grids1.txt"

server = "10.1.1.183"
database = "QM"
login = "sa"
password = "doggy183"

db = dbinterface_ado.DBInterfaceADO()
db.conn = db._make_connection(server, database, login, password)

done = []

for line in file(filename):
    grid = line.strip()
    if grid:
        grid = grid[:10]    # only first 10 characters
        if grid not in done:
            db.insertrecord('hp_grid', call_center=call_center,
                            client_code=client, grid=grid)
            done.append(grid)
            sys.stdout.write("o")
        else:
            sys.stdout.write(".")
print

