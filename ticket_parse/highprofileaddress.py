# highprofilegrids.py

def match(str1,str2):
    if str1.lstrip().rstrip().lower() == str2.lstrip().rstrip().lower():
        return True
    else:
        return False

def chk_address(addresses,ticket):
    relevant_address = {'street':ticket.work_address_street,
                        'city':ticket.work_city,
                        'county':ticket.work_county,
                        'state':ticket.work_state}
    # if we find relevant tickets, call callback function
    for address in addresses:
        if match(address['street'],relevant_address['street']) and \
           match(address['city'],relevant_address['city']) and \
           match(address['county'],relevant_address['county']) and \
           match(address['state'],relevant_address['state']):
            return relevant_address
    return None

class HighProfileAddress:
    """
    Load records from the hp_address table and check if grids on a ticket
    match them.
    """

    def __init__(self, tdb):
        self.data = {}
        self.tdb = tdb
        self.load()

    def load(self):
        rows = self.tdb.dbado.getrecords('hp_address')
        for row in rows:
            call_center = row['call_center']
            client_code = row['client_code']
            street = row['street']
            city = row['city']
            county = row['county']
            state = row['state']
            d = self.data.get(call_center, {})
            address_dict = {'street':street,
                            'city':city,
                            'county':county,
                            'state':state}
            try:
                d = self.data[call_center]
            except KeyError:
                self.data[call_center] = {client_code: [address_dict]}
            else:
                try:
                    e = d[client_code]
                except KeyError:
                    d[client_code] = [address_dict]
                else:
                    e.append(address_dict)

    def get(self, call_center, client_code):
        try:
            return self.data[call_center][client_code]
        except:
            return []

    def check(self, ticket, ticket_id, ticket_version_id, clients, callback):
        """
        Check for each client on the given ticket if it has a high profile
        address, and if so, perform an action (by calling the <callback>
        function).

        - <ticket> is a valid Ticket instance.
        - <clients> is a *list* of clients (Client instances) for the
            appropriate call center.
        - <callback> is a function that will be called if high profile addresses
            are found on this ticket.  It must have the following signature:
              f(ticket, ticket_id, locate, address_found, email).
        """
        call_center = ticket.ticket_format
        for c in ticket.locates:
            addresses = self.get(call_center, c.client_code)
            if addresses:
                # find high profile addresses that are on ticket
                address_found = chk_address(addresses,ticket)
                if address_found is not None:
                    # Match found
                    # find grid_email for this client
                    grid_email = ""
                    for cl in clients:
                        if cl.client_code == c.client_code:
                            grid_email = cl.grid_email or ""
                            break
                    # call callback function to "do something" (e.g. send mail)
                    result = callback(ticket, ticket_id, c, address_found,
                             grid_email)
                    # log that we "did something", but only if result is ok
                    if result:
                        self.log(self.tdb, ticket_id, c.client_code,
                         ticket_version_id)

    def has_hp_address(self, ticket, client_dict):
        """
            Simple check to see if a ticket has a HP address.  To be
            used by the ticket_alert system.  Returns the first address found,
            which can be interpreted as a boolean as well.
            <client_dict> is a dict as returned by TicketDB.get_clients_dict.
            It's used to check if clients are active.  If None, this check is
            skipped.
        """
        call_center = ticket.ticket_format
        if client_dict is not None:
            clients = client_dict.get(call_center, [])
            client_names = [cl.client_code for cl in clients]

        for c in ticket.locates:
            # check if this client is in client_dict; if not, ignore
            # (this way we skip inactive clients)
            # however, if client_dict=None, skip this test.
            if client_dict is not None:
                if c.client_code not in client_names:
                    continue
            # get HP addresses associated with this call center and client code
            addresses = self.get(call_center, c.client_code)
            address_found = chk_address(addresses,ticket)
            if address_found is not None:
                return address_found

        # didn't find anything
        return None

    def log(self, tdb, ticket_id, client_code, ticket_version_id):
        tdb.dbado.insertrecord('ticket_grid_log', ticket_id=ticket_id,
            client_code=client_code, ticket_version_id=ticket_version_id)

    def get_hp_address_clients(self, ticket, client_dict):
        """
            Return a list of locates (found on the ticket) that have HP addresses
            associated with them.  (The list returns the actual Locate objects,
            not client codes.  This is done so we can do e.g. loc.alert = 1.)
            <client_dict> can be None, in which case it will be ignored, or
            it can be a list of clients, to check if a client is active.
        """
        found = []

        call_center = ticket.ticket_format
        if client_dict is not None:
            clients = client_dict.get(call_center, [])
            client_names = [cl.client_code for cl in clients]

        for loc in ticket.locates:
            # check if this client is in client_dict; if not, ignore
            # (this way we skip inactive clients)
            # however, if client_dict=None, skip this test.
            if client_dict is not None:
                if loc.client_code not in client_names:
                    continue
            # get HP addresses associated with this call center and client code
            addresses = self.get(call_center, loc.client_code)
            address_found = chk_address(addresses,ticket)
            if address_found is not None:
                found.append(loc)
                break # go to next locate

        return found

