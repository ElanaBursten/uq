# event_log.py

import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib'))

import thrift.TSerialization
import thrift.protocol.TJSONProtocol
import QMLogic2ServiceLib.ttypes

max_retries = 9

class Error(Exception):
    pass

class EventLogInsertFailed(Error):
    pass

class EventLog:
    def __init__(self, tdb, log, store_events=0):
        self.tdb = tdb
        self.dbado = self.tdb.dbado
        self.log = log
        self.store_events = store_events

    def next_version(self, aggregate_type, aggregate_id):
        rows = self.dbado.runsql_result("""
         select max(version_num) as max_version from event_log
         where aggregate_type = %d and aggregate_id = %d
        """ % (aggregate_type, aggregate_id))
        if rows[0]['max_version']:
            return int(rows[0]['max_version']) + 1
        else:
            return 1

    # todo(dan) Test new error handling and new messages
    # todo(dan) Test that escaping special characters works
    # todo(dan) Reword messages?
    # todo(dan) Add a method comment
    def log_event(self, aggregate_type, aggregate_id, event_data,
     default_version=None, max_version=None):
        if not self.store_events:
            msg = 'Event logging turned off, so event_log record not ' +\
             'inserted for aggregate_type: %s, aggregate_id: %s.'
            self.log.log_event(msg % (aggregate_type, aggregate_id))
            return False, None

        if default_version is None:
            version = self.next_version(aggregate_type, aggregate_id)
        else:
            version = default_version

        for attempt in range(1, max_retries + 2):
            if max_version is not None and version > max_version:
                msg = 'Failed to insert event_log record for aggregate_type:' +\
                 ' %s, aggregate_id: %s, because next version: %s > ' +\
                 'max_version: %s.'
                self.log.log_event(msg % (aggregate_type, aggregate_id, version,
                 max_version))
                return False, version

            try:
                self.dbado.runsql("""
                 insert into event_log (aggregate_type, aggregate_id, version_num,
                 event_data) values (%d, %d, %d, '%s')
                """ % (aggregate_type, aggregate_id, version, event_data))
                msg = 'Event_log record inserted for aggregate_type: %s, ' +\
                 'aggregate_id: %s, as version: %s.'
                self.log.log_event(msg % (aggregate_type, aggregate_id, version))
                return True, version
            except Exception as e:
                if 'Cannot insert duplicate key' not in e.message:
                    raise

                msg = 'Attempt %s to insert event_log record failed because of ' +\
                 'a duplicate key: aggregate_type = %s, aggregate_id = %s, ' +\
                 'version = %s.' 
                self.log.log_event(msg % (attempt, aggregate_type, aggregate_id,
                 version))

                version = self.next_version(aggregate_type, aggregate_id)
        
        msg = 'Failed to insert event_log record in %s attempts, for ' +\
         'aggregate_type: %s, aggregate_id: %s.'
        raise EventLogInsertFailed(msg % (max_retries + 1, aggregate_type,
         aggregate_id))
 
    def serialize_thrift_object(self, thrift_object):
        return thrift.TSerialization.serialize(thrift_object,
         protocol_factory=thrift.protocol.TJSONProtocol.TJSONProtocolFactory())

    # todo(dan) Add a method comment
    def log_ticket_event(self, ticket_id, event_data, default_version=None,
     max_version=None):
        ttypes = QMLogic2ServiceLib.ttypes

        aggr_key = ttypes.AggrKey(AggrType=ttypes.AggregateType().atTicket,
         AggrID=ticket_id)
        if isinstance(event_data, ttypes.TicketReceived):
            union = ttypes.EventUnion(TicketReceived=event_data)
        else:
            raise ValueError('Unexpected type of event data')

        event = ttypes.Event(EventID=None, AggregateKey=aggr_key, Union=union)

        return self.log_event(ttypes.AggregateType().atTicket, ticket_id,
         self.serialize_thrift_object(event), default_version=default_version,
         max_version=max_version)
