# abstractresponder.py
#
# Contains generic code for responding, which is basically done like this:
# for call center, responder_data in list_of_known_responders:
#    login
#    get all pending responses
#    for locate in locates:
#        respond
#    logout

# XXX There needs to be a fool-proof way to specify test arguments on the
# command line, for all responders, and send a response based on these
# arguments. Right now this "sort of" works, except some responders try to find
# the specified ticket in the database, which in this case leads to an error.

from __future__ import with_statement
import cStringIO
import getopt
import os
import socket # for exceptions
import string
import sys
import traceback
#
import baseresponder
import businesslogic
import config
import datadir
import date
import dbinterface_old as dbinterface
import dbinterface_ado
import emailtools
import errorhandling2
import errorhandling_special
import listener
import mutex
import optimize
import responder_data
import statuslist
import ticket_db
import tools
import version
import windows_tools
from check_python_version import check_python_version

fmt = tools.fmt

__usage__ = """\
%s.py [options] [call_center ticket_number client_code status [kind]]

Options:
    -c cfgfile  Specify an alternate configuration file.
    -t          Test mode (do everything except actually sending a response).
    -f          ...
    -n number   Do a maximum of <number> responses.
    -d          In case of errors, don't delete responses from queue
    -C center   Only respond for this call center.
    -q          (quiet) Don't send any notification emails.
"""

class ResponderError(Exception):
    pass

class BadResponseError(IOError):
    """ Raised if we get no acceptable response from the server. """

class UnknownStatusError(Exception):
    """ Raised if we find no translation for a status code. """

class AbstractResponder(baseresponder.BaseResponder):
    name = "(AbstractResponder)"

    def __init__(self, configfile="", testmode=0, max_number=0, manual_mode=0,
                 verbose=1, nodelete=0, quiet=0, only_call_center=None):
        baseresponder.BaseResponder.__init__(self)
        self.setup_config(configfile) # sets self.config

        self.responderdata = {}
        self.only_call_center = only_call_center

        self.testmode = testmode
        self.bot = None
        self.manual_mode = manual_mode
        self.nodelete = nodelete
        self.verbose = verbose
        self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado) # only for emails
        self.emails = emailtools.Emails(self.tdb)
        self.max_number = max_number
        self.quiet = quiet
        self.count = 0  # number of responses attempted
        self._version = None

        self.statuslist = statuslist.StatusList(self.tdb)
        self.skipped_status_codes = []

        self.setup_logger(verbose)
        self.log_begin()

        if self.quiet:
            self.log.lock = True # don't send emails

    #
    # configuration

    def setup_logger(self, verbose):
        # create and configure the logger. requires self.config.
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0],
                   me=self.__class__.name,
                   admin_emails=self.config.admins,
                   subject=self.__class__.name + " Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = verbose

    def setup_config(self, configfile):
        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                      "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

    def check_skipped_status_codes(self, status):
        # Check that skipped status codes are valid
        if not self.statuslist.data.has_key(status):
            raise ValueError("Status code to skip not in status list")

    def load_responder_data(self, call_center, raw_responderdata):
        '''
        override to set self.responderdata to something useful. e.g. load
        from irthresponder_data.
        '''
        pass

    def get_logins(self, call_center):
        """ Return a list of logins for this call center.  If there are none,
            return an empty list. """
        return [] # override in subclasses

    #
    # logging (to general log file)

    def log_begin(self):
        s = self.__class__.name + " started on %s" % date.Date().isodate()
        self.log.log_event(s)

    def log_end(self):
        s = self.__class__.name + " stopped on %s" % date.Date().isodate()
        self.log.log_event(s)

    #
    # login / logout (empty by default)

    def login(self, call_center, login):
        pass # override in subclasses

    def logout(self):
        pass # override in subclasses

    #
    # get responses

    def get_pending_responses_raw(self, call_center):
        """ Get pending responses, without applying restrictions, filters,
            etc. """
        # does this call center use a special query? (e.g. FMW1)
        try:
            sql = responder_data.special_queries[call_center]
        except KeyError:
            if getattr(self.responderdata, 'queue', None) and \
             self.responderdata.queue['table'] == 'responder_multi_queue':
                sql = "exec get_pending_multi_responses_2 '%s', '%s'" % \
                 (self.responderdata.queue['respond_to'], call_center)
            else:
                sql = "exec get_pending_responses '%s'" % (call_center,)
        rows = self.dbado.runsql_result(sql)
        return rows

    def get_pending_responses(self, call_center):
        if self.verbose:
            print "Getting pending responses for", call_center, "..."

        rows = self.get_pending_responses_raw(call_center)

        # restrict, filter and consolidate records
        if self.max_number:
            rows = rows[:self.max_number]
        self.filter_responses(rows)
        rows = self.consolidate_responses(call_center, rows)
        if self.verbose:
            print "OK (%d records found)" % (len(rows),)
            self.log.log_event("%d records found for %s" % (
             len(rows), call_center))

        return rows

    def filter_responses(self, rows):
        """ Change the list of "raw" responses in-place, filtering out locates
            that should not be responded to. """
        # Get the statuses that correspond to meet only
        meet_status_rows = self.dbado.getrecords('statuslist',meet_only=1)
        meet_statuses = [meet_status_row['status']
                         for meet_status_row in meet_status_rows]
        for i in range(len(rows)-1,-1,-1):
            row = rows[i]
            if row["status"] in ["-R"]:
                self.delete_from_queue(row['locate_id'])
                self.log.log_event("Record skipped due to status %s: %s" % (
                 row['status'], row['locate_id']))
                del rows[i]
            elif row["status"] in meet_statuses:
                """
                If the status is for a meet ticket and the ticket_type
                is not MEET, then it's an error.
                """
                if row["ticket_type"].find("MEET") == -1:
                    self.delete_from_queue(row['locate_id'])
                    ep = errorhandling2.errorpacket(row=row)
                    ep.add(info="Record skipped due to status %s and ticket is not a MEET ticket, locate_id %s"
                                 % (row['status'], row['locate_id']))
                    self.log.log(ep, send=1, write=1, dump=self.verbose)
                    self.log.log_event("Record skipped due to status %s and ticket is not a MEET ticket, locate_id %s"
                                       %(row['status'], row['locate_id']))
                    del rows[i]

    def consolidate_responses(self, call_center, rows):
        """ Consolidates" responses if possible and appropriate, and
            returns a new set of rows. """
        logic = businesslogic.getbusinesslogic(call_center, self.tdb, [])
        if logic.consolidated_response_groups:
            return self._consolidate_responses(rows,
                   logic.consolidated_response_groups,
                   logic.consolidate_response_status)
        else:
            return rows

    def _consolidate_responses(self, rows, groups, statusfunc):
        groups = list(enumerate(groups))
        def find_in_groups(client_code):
            for idx, client_codes in groups:
                if client_code in client_codes:
                    return idx
            return -1
        d = {}
        ok = []
        for row in rows:
            client_code = row['client_code']
            ticket_id = row['ticket_id']
            idx = find_in_groups(client_code)
            if idx >= 0:
                key = (ticket_id, idx)
                if d.has_key(key):
                    d[key].append(row)
                else:
                    d[key] = [row]
            else:
                ok.append(row)

        # process records that can be "consolidated"
        for key, value in d.items():
            if len(value) == 1:
                ok.append(value[0])
            else:
                # find the consolidated status for these rows
                status = statusfunc(value)
                ticket_id, idx = key
                client_code = groups[idx][1][0]
                # add the row with the *first* client code in the group
                for row in value:
                    if row['client_code'] == client_code:
                        row['status'] = status
                        ok.append(row)
                        break

        return ok

    def responder_mutex_name(self, call_center):
        return self.__class__.__name__ + ':' + datadir.datadir.get_id() + \
         ':' + call_center
    #
    # general responding pattern

    def respond_all(self):
        # get all the responder data for the given type
        responders = self.config.responderconfigdata.get_responders(
                     type=self.responder_type)

        try:
            for call_center, _, raw_responderdata in responders:
                if self.only_call_center \
                and call_center != self.only_call_center:
                    continue
                mutex_name = self.responder_mutex_name(call_center)
                try:
                    with mutex.MutexLock(self.dbado, mutex_name):
                        # set self.responderdata
                        self.load_responder_data(call_center, raw_responderdata)

                        all_locates = self.get_pending_responses(call_center)
                        logins = self.get_logins(call_center)
                        if logins:
                            # respond per call center / login combination
                            for login in logins:
                                # self.count is (re)set by responder_for_call_center()
                                self.respond_for_call_center(call_center, all_locates,
                                 login)
                                # check for 'quit' signal
                                if 'quit' in listener.uqlistener.listen():
                                    self.log.log_event("listener: 'quit' signal received, exiting")
                                    break
                        else:
                            # respond for the whole call center, no login
                            self.respond_for_call_center(call_center, all_locates, None)

                        self.log.log_event(fmt(self.count, "responses sent"))
                        self.log.force_send()
                except mutex.SingleInstanceError:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Locked mutex: %s" % mutex_name)
                    self.log.log(ep, send=1, dump=self.log.verbose, write=1)

                # check for 'quit' signal
                if 'quit' in listener.uqlistener.listen():
                    self.log.log_event("listener: 'quit' signal received, exiting")
                    break
        except:
            ep = errorhandling2.errorpacket()
            self.log.log(ep, send=1, write=1, dump=1)

        # send any queued mail
        self.log.force_send()

    def respond_for_call_center(self, call_center, all_locates, login=None):
        try:
            self.count = 0
            # select only the locates for this login
            if login and self.responderdata.client_based_login:
                locates = self.filter_locates(all_locates, login["login"])
            else:
                locates = all_locates

            self.log.log_event("%d record(s) for %s" % (
             len(locates), login and login['login'] or "*"))

            if locates:
                if login:
                    login_name = login['login']
                else:
                    login_name = None
                self.respond_to_locates(call_center, locates, login_name)
            else:
                self.log.log_event(fmt("** Nothing to send for",
                 call_center + "/" + (login and login["login"] or "*")))

            # handle acknowledgements, if any
            self.handle_acknowledgements()

        except Exception, e:
            dontlog = (isinstance(e, socket.error)
                      and not isinstance(e, socket.gaierror))
            if dontlog:
                print "An error occurred (not logged):"
                c = cStringIO.StringIO()
                traceback.print_exc(file=c)
                self.log.log_event("Unlogged error:\n" + c.getvalue())
                self.log.log_event("Error when connecting/sending")
            else:
                ep = errorhandling2.errorpacket(login=login,
                     call_center=call_center)
                self.log.log(ep, send=1, write=1, dump=self.verbose)
                self.log.log_event("Error when connecting/sending")
                # if an error occurs here, we can continue with the next
                # login

    def respond_to_locates(self, call_center, response_list, login):
        """ Respond for the given call center. response_list is a list of
            dicts as returned by get_pending_responses (after filtering). """
        # set emails for call center: admin and responder_email
        allemails = [a['email'] for a in self.config.admins]
        allemails.extend(self.emails[call_center].responder)
        self.log.errorbag.emails = allemails

        s = "Responding for: %s/%s" % (call_center, login or '*')
        self.log.log_event(s)

        self.login(call_center, login)
        self.process_locates(call_center, response_list)
        self.logout()

    def process_locates(self, call_center, locates):
        self.count = 0
        self.pre_process_locates(call_center)

        for row in locates:
            self._row = row # row we're currently processing
            response_sent = self.process_locate(row)
            if response_sent:
                self.count = self.count + 1

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

        # Serve up an email that details the responses skipped due to status
        # codes
        if self.skipped_status_codes:
            msg = []
            for skipped_status_codes in self.skipped_status_codes:
                msg.append(string.join([
                  'ticket number: ',
                  skipped_status_codes['ticket_number'] or 'None',
                  'client_code: '  ,
                  skipped_status_codes['client_code'] or 'None',
                  'work_date: '    ,
                  skipped_status_codes['work_date'] or 'None',
                  'status: '       ,
                  skipped_status_codes['status'] or 'None'],
                ' '))
            ep = errorhandling2.errorpacket()
            ep.add(info=string.join(msg, '\n'))
            self.log.log_warning(ep, call_center=call_center, send=1,
              write=1, dump=1, subject_hint='Skipped status codes')

        self.post_process_locates(call_center)

    def pre_process_locates(self, call_center):
        pass # override in subclasses, if necessary

    def post_process_locates(self, call_center):
        pass # override in subclasses, if necessary

    @optimize.make_constants()
    def process_locate(self, row):
        """ Process a locate, i.e. respond to it, unless it must be skipped
            (deleted without being responded to) or ignored (left in queue).
            Handles logging, etc. """
        # add references for "downstream" use later
        self.responderdata.tdb = self.tdb
        self.responderdata.row = row

        try:
            call_center = row['ticket_format']
            client_code = row.get("client_code")
            locate_id = row["locate_id"]
            ticket_id = row['ticket_id']
            status = row.get('status', '')
            locator_name = row.get('locator_name', '')
            resp = None

            if self.manual_mode:
                # ticket info is bogus, don't bother looking it up, just send
                # it and see what the responder does
                resp = self.send_response_status(ticket_id,
                       row['ticket_number'],
                       client_code, status, "contact", locator_name)
                self.log_response(row, resp)
                return 0

            logic = businesslogic.getbusinesslogic(call_center, self.tdb, [])
            t = self.tdb.getticket(ticket_id)

            if logic.is3hr(t) \
            and not self.responderdata.send_3hour \
            and not logic.respond_to_original(t, locate_id):
                msg = string.join([
                  "Skipping 3 hour ticket %s" % (row['ticket_number']),
                  "    Reason:",
                  "    responder send_3hour = %d" % (self.responderdata.send_3hour),
                  "    respond_to_original is %s" % (logic.respond_to_original(t, locate_id))], '\n')
                self.log.log_event(msg)

            elif logic.isemergency(t) \
            and not self.responderdata.send_emergencies \
            and not logic.respond_to_original(t, locate_id):
                msg = string.join([
                  "Skipping emergency ticket %s" % row['ticket_number'],
                  "    Reason:",
                  "    responder send_emergencies = %d" % (self.responderdata.send_emergencies),
                  "    respond_to_original is %s" % (logic.respond_to_original(t, locate_id))], '\n')
                self.log.log_event(msg)

            elif client_code in self.responderdata.skip \
              or self.responderdata.do_skip(row):
                # skipping this term id means:
                # 1. do not respond
                # 2. delete from queue
                # 3. log
                self.log.log_event("Skipping: %s" % (client_code, ))
                resp = "**skipped**"
                # NOTE: deleting and logging is done automagically,
                # further on!

            elif status in self.responderdata.status_code_skip:
                # skipping this status code means:
                # 1. do not respond
                # 2. delete from queue
                # 3. log and add to skipped_status_codes
                self.check_skipped_status_codes(status)
                self.log.log_event("Skipping: %s" % (status, ))
                self.skipped_status_codes.append(row)
                # NOTE: deleting and logging is done automagically,
                # further on!

            elif client_code not in self.responderdata.clients \
             and self.responderdata.clients != []:
                # if the client code is neither in skip nor in clients, then
                # ignore it; this responder won't handle it
                self.log.log_event("Ignoring: %s" % (client_code,))
                resp = 'ignore'

            else:
                ticket_number = row.get('ticket_number', '')
                contact = row.get('contact', '')

                # fix ticket number if necessary
                ticket_number = self.tweak_ticket_number(row)

                # is there a translation for the client code?
                client_code = self.translate_term(client_code, row)

                # respond, then
                resp = self.send_response_status(ticket_id, ticket_number,
                       client_code, status, contact, locator_name)
                self.log_response(row, resp)

            #
            # what to do with this response?

            response_sent = False
            if locate_id:
                if self.testmode:
                    pass
                elif resp is None or resp == "**skipped**":
                    # emergency or skipped
                    self.delete_from_queue(locate_id)
                elif resp == 'ignore':
                    pass
                    # ignore this client, will most likely be handled elsewhere
                else:
                    code = self.get_code_from_response(resp[2])
                    if code in self.responderdata.codes_ok:
                        # delete from queue, continue
                        self.delete_from_queue(locate_id)
                        response_sent = True
                    elif code in self.responderdata.codes_error:
                        # notify admin, delete from queue
                        self.notify(locate_id, resp, row)
                        if not self.nodelete:
                            self.delete_from_queue(locate_id)
                    elif code in self.responderdata.codes_reprocess:
                        # continue, but don't delete from queue
                        self.log.log_event("...Ignored for now")
                    else:
                        # unknown code; bail out
                        raise BadResponseError, \
                              "Unexpected response: %r" % (code,)
            return response_sent

        except BadResponseError:
            raise   # break out of this loop and method
        except (socket.error, EOFError):
            raise   # ditto
        except UnknownStatusError:
            ep = errorhandling2.errorpacket(row=row)
            self.log.log(ep, send=1, dump=self.verbose, write=1)
            locate_id = row["locate_id"]
            if locate_id:
                self.delete_from_queue(locate_id)
        except dbinterface.DBFatalException:
            raise
        except Exception, e:
            # log it and continue
            ep = errorhandling2.errorpacket(row=row, name=e.__class__)
            self.log.log(ep, send=1, dump=self.verbose, write=1)
            # do not delete anything from queue

    def send_response_status(self, ticket_id, ticket_number, clientcode,
                             status, contact, locator_name):
        """ Send a response with a "status". The
            status is mapped to a response code and explanation code.
            Returns a triple (responsecode, explanation, response-from-server).
        """
        # translate locate's status to a response code & explanation code
        # (as defined in config.xml)
        try:
            z = self.responderdata.response_codes[status]
        except KeyError:
            raise UnknownStatusError, "Unknown status: %s" % (status,)
        responsecode, explanation = z

        # custom status mappings
        z = self.responderdata.map_status(status)
        if z is not None:
            responsecode, explanation = z

        # substitute contact information
        if explanation == 'CONTACT':
            explanation = contact

        if self.testmode:
            print "[testmode] Response: %s, %s, %s, %s" % (ticket_number,
                  clientcode, responsecode, explanation)
            return responsecode, explanation, "Test Mode - Not Really Sent - OK"
        else:
            # TODO: if we send all versions, do it here
            if getattr(self.responderdata, 'all_versions', False):
                return self.send_response_for_all_versions(ticket_id,
                       ticket_number, clientcode, responsecode, explanation,
                       locator_name)
            else:
                # This try-except is really only needed for a handful of tests
                try:
                    version = self.tdb.getrecords('ticket', ticket_id = ticket_id)[0]['revision']
                except:
                    version = ''
                return self.send_response_and_log(ticket_number, clientcode,
                       responsecode, explanation, locator_name, version=version)

    def send_response_and_log(self, ticket_number, clientcode, responsecode,
                              explanation='', locator_name='', version=''):
        """ Send a response by calling send_response(), logging before and
            after the call. """
        s = "Sending Response: %s, %s, %s, %s, %s, %s" % (
            ticket_number, clientcode, responsecode, explanation, locator_name,
            version)
        self.log.log_event(s)
        resp = self.send_response(ticket_number, clientcode, responsecode,
               explanation, locator_name, version)
        s = ("Response: %s" % (resp,)).strip()
        self.log.log_event(s)
        return (responsecode, explanation, resp)

    def send_response(self, ticket_number, client_code, response_code,
                      explanation, locator_name, version=''):
        """ Send a response and get a reply back.  Must return the string as
            returned by the server.  E.g. "250 OK" for IRTH. """
        raise NotImplementedError

    def send_response_for_all_versions(self, ticket_id, ticket_number,
                                       client_code, response_code, explanation,
                                       locator_name):
        # get all versions
        versions = self.tdb.get_ticket_versions(ticket_id)
        if hasattr(self.responderdata, 'filter_versions'):
            versions = self.responderdata.filter_versions(versions)
        result = ("?", "?", "?") # this should never be returned

        # respond to each version, using the same data (except the version no)
        if versions:
            for version in versions:
                self._version = version
                result = self.send_response_and_log(ticket_number, client_code,
                         response_code, explanation, locator_name,
                         version.ticket_revision)
        else:
            result = self.send_response_and_log(ticket_number, client_code,
                     response_code, explanation, locator_name, '0')

        return result

    def get_code_from_response(self, response):
        """ Get a code from the response from the server. """
        return response

    def filter_locates(self, locates, login):
        """ Given a list of locates, return only those locates of which the
            client code is the same as the login. """
        return [locate for locate in locates if locate["client_code"] == login]

    #
    # responder log

    def log_response(self, row, resp):
        """ Log a response by writing a record to the response_log table.
            <resp> is a 3-tuple (responsecode, explanationcode, response)
            e.g. ("250", "Skipped term", "250 OK")
            (code, explanation, string returned by server).
        """
        responsecode, explanationcode, response = resp
        successflag = self.response_is_success(row, resp)
        response_sent = "%s%s" % (responsecode, explanationcode)
        response_sent = response_sent[0:15]

        if not row["locate_id"]:
            # we came in through the 4-argument command line
            print "Response:", resp
            print "Response for database:", response_sent
            print "Success:", successflag
            return

        # if this center sends acknowledgements, then we set success to 0 and
        # the description to 'waiting'; to be set to success=1 when the
        # acknowledgement is received
        if self.has_acknowledgements():
            successflag = 0
            description = '(%s waiting)' % (self.__class__.responder_type,)
            # e.g. email waiting, xmlhttp waiting, etc
        else:
            description = response[:40]

        # insert response_log record
        sql = """
         insert response_log (locate_id, response_date, call_center, status,
                              response_sent, success, reply)
         values ('%s', '%s', '%s', '%s', '%s', %d, '%s')
        """ % (row['locate_id'], date.Date().isodate(), row['ticket_format'],
               row['status'], tools.quote_apostrophes(response_sent),
               successflag, tools.quote_apostrophes(description))
               # long replies must be truncated
        self.tdb.runsql(sql)

        if self.has_acknowledgements():
            # get response_id [FIXME: should be done better]
            sql = """
             select max(response_id) as r from response_log
             where locate_id = %s
            """ % (row['locate_id'],)
            rows = self.tdb.runsql_result(sql)
            resp_id = rows[0]['r']

            # insert response_ack_waiting record (used to find the right response_id
            # later)
            key = row.get('serial_number') or row['ticket_number']
            # FIXME: should depend on call center
            self.tdb.insert_response_ack_waiting(resp_id, row['ticket_format'], key)

    def response_is_success(self, row, response):
        raise NotImplementedError

    #
    # responding: tweaking

    def tweak_ticket_number(self, row):
        """ Override if we're changing the ticket number before sending it
            in a response. """
        if hasattr(self.responderdata, 'tweak_ticket_number'):
            return self.responderdata.tweak_ticket_number(row)
        else:
            return row.get('ticket_number', '')

    def translate_term(self, client_code, row):
        """ Look up a term in the translations dict.  If found, return this
            translation, otherwise return the original name.
            Side effect when a translation is found: logging, and
            row['translation'] is set.
        """
        t_client_code = self.responderdata.translations.get(client_code, None)
        if t_client_code:
            s = " ..Translation: %s -> %s" % (client_code, t_client_code)
            self.log.log_event(s)
            row['translation'] = t_client_code  # info for errors
            return t_client_code
        else:
            return client_code

    #
    # error handling

    def notify(self, locate_id, resp, row):
        """ Notify the admin of this error.
            resp is the 3-tuple returned by send_response_status().
            row is the record used for responding.
        """
        try:
            raise ResponderError, "Error code: " + str(resp[2])
        except ResponderError:
            ep = errorhandling2.errorpacket(locate_id=locate_id, resp=resp,
                 row=row)
            self.log.log(ep, send=1, dump=self.verbose, write=1)

    #
    # alternative calling methods

    def respond_one(self, ticket_info):
        """ Respond to one given ticket/client combination with status. """
        call_center, ticket_number, client_code, status = ticket_info[:4]
        if len(ticket_info) > 4:
            kind = ticket_info[4]
        else:
            kind = ''
        self.manual_mode = True
        mutex_name = self.responder_mutex_name(call_center)
        try:
            with mutex.MutexLock(self.dbado, mutex_name):
                # get responder data the old way (from config.xml, one per c.c.)
                if self.name == 'XMLHTTPResponder':
                    raw_responderdata = {'manual_mode': 1, 'kind': kind}
                else:
                    raw_responderdata = None
                self.load_responder_data(call_center, raw_responderdata)
                d = {"ticket_format": call_center,
                     "client_code": client_code,
                     "status": status,
                     "ticket_number": ticket_number,
                     "locate_id": 0,
                     "ticket_id": 0,
                     "serial_number": (len(ticket_info) > 4) and ticket_info[4] or "",
                     "ticket_type": "NORMAL",
                     "closed_date": date.Date().isodate(),
                     "locator_name": "UQ locator"
                    }

                try:
                    if self.responderdata.client_based_login:
                        login = client_code
                    else:
                        try:
                            login = self.config.responders[call_center]['logins'][0]['login']
                        except KeyError:
                            login = None
                    self.respond_to_locates(call_center, [d], login=login)
                except:
                    ep = errorhandling2.errorpacket(locate=d, login=client_code)
                    self.log.log(ep, send=1, write=1, dump=1)
                self.log.force_send()   # actually send emails
        except mutex.SingleInstanceError:
            ep = errorhandling2.errorpacket()
            ep.add(info="Locked mutex: %s" % mutex_name)
            self.log.log(ep, send=1, dump=self.log.verbose, write=1)

    def handle_acknowledgements(self):
        # the presence of responderdata.handle_acknowledgements() implies:
        # 1. when responding, we write result=0 and 'waiting' to response_log
        # 2. when an acknowledgement is received, we look up aforementioned
        #    record and set result=1 and remove the 'waiting' flag
        # NOTE: If no ack_dir is set, we assume that acknowledgements are
        # disabled!
        f = getattr(self.responderdata, 'handle_acknowledgements', None)
        g = getattr(self.responderdata, 'ack_dir', "").strip()
        if f and g:
            f(self.tdb, self.log)

    def has_acknowledgements(self):
        spam = getattr(self.responderdata, 'handle_acknowledgements', None)
        eggs = getattr(self.responderdata, 'ack_dir', "").strip()
        return bool(spam and eggs)

def run_toplevel(responderclass):
    respondername = responderclass.name
    opts, args = getopt.getopt(sys.argv[1:], "c:C:dqftn:?", ["data="])

    configfile = "" # use a different config.xml file
    config_response = 0 # use config_response.py
    testmode = 0
    max_number = 0
    nodelete = 0
    quiet = 0
    only_call_center = None

    for o, a in opts:
        if o == "-?":
            print >> sys.stderr, __usage__ % respondername
            sys.exit(0)
        if o == "-c":
            configfile = a
        elif o == "-f":
            config_response = 1
        elif o == "-t":
            testmode = 1
        elif o == "-n":
            max_number = int(a)
        elif o == "-d":
            nodelete = 1
        elif o == "-q":
            quiet = 1
        elif o == "-C":
            only_call_center = a
        elif o == "--data":
            print "Data dir:", a

    if args:
        if len(args) < 4:
            print >> sys.stderr, __usage__ % respondername
            sys.exit(1)

    try:
        log = errorhandling_special.toplevel_logger(configfile, respondername,
              respondername + " Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, respondername)
        sys.exit(1)

    check_python_version(log)

    try:
        resp = responderclass(configfile=configfile, testmode=testmode,
               max_number=max_number, nodelete=nodelete, quiet=quiet)
        resp.log.log_event(respondername
          + " (version %s) started with pid %s" %
            (version.__version__, os.getpid()))
        windows_tools.setconsoletitle(responderclass.__name__ + ":" +
         datadir.datadir.get_id())

        # only respond for this call center; pretend any others don't exist
        if only_call_center:
            resp.only_call_center = only_call_center
            resp.log.log_event("Responder only runs for call center %s" %
             only_call_center)

        if args:
            resp.respond_one(ticket_info=args)
        elif config_response:
            # todo(dan) Is this implemented, or can it be removed?
            resp.respond_config()
        else:
            resp.respond_all()

        resp.log_end()  # make sure ending time is logged

    except SystemExit:
        pass

    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

