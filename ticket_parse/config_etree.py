# config_etree.py
# Configuration module rewritten using ElementTree.

# NOTE:
# - Attributes Config.responders, Config.ftpresponders, etc, are deprecated
# and should be phased out. They allow only only one responder of a given type
# per call center. Use Config.responderconfigdata instead, which allows
# multiple responders of the same type for the same call center.
# (Also see: Mantis #2772, #2779)

from __future__ import with_statement
import os
import re
import sys
import traceback
import xml.etree.ElementTree as ET
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import datadir
import responder_config_data as rcd
import tools
#
import et_tools
from et_tools import elem_as_dict, elem_as_dictlist, safe_find

DEFAULT_CONFIG_FILENAME = 'config.xml'
DEFAULT_DB_TIMEOUT_SECONDS = 580
RECEIVER_OUTPUT_SUBDIR = 'receiver-temp'
RECEIVER_ATTACHMENT_DIR = 'attachments'

def U(u):
    """ Simple Unicode-to-utf8 decoder. """
    if isinstance(u, unicode):
        return u.encode("utf-8")
    else:
        return u

class ConfigurationError(Exception):
    pass

class Configuration:

    def __init__(self, filename=DEFAULT_CONFIG_FILENAME, verbose=1):
        self.filename = filename
        self.verbose = verbose

        with datadir.datadir.open(self.filename) as f:
            self._tree = self.parse(f) # complains if invalid
            # keep a copy of the original XML around
            f.seek(0); data = self._data = f.read()

        self._root = self._tree.getroot()
        self.responderconfigdata = rcd.ResponderConfigData()

        self.read_database_info()
        self.read_server_info() # timezone etc
        self.read_processes()
        self.read_admins()
        self.read_misc()
        self.read_smtp_data()
        self.read_irthresponder_data()
        self.read_xmlhttpresponder_data()
        self.read_emailresponder_data()
        self.read_ftpresponder_data()
        self.read_xcelresponder_data()
        self.read_wo_responder_data()
        self.read_soapresponder_data()
        self.determine_nodelete()
        self.read_watchdog_data()
        self.read_archiver_data()
        self.read_ticketparser_data()
        self.read_ticketrouter_data()
        self.read_msmq_data()
        self.read_geocoder_info()

        self.read_location()

        # when everything is read, gather skip data
        self.responderconfigdata.gather_skip_data()
        self.responderconfigdata.gather_status_code_skip_data()

    def validate(self):
        et_tools.validate(self._tree) # will raise error if not well-formed

    def parse(self, f):
        try:
            self._tree = ET.parse(f)
        except Exception, e:
            if self.verbose:
                traceback.print_exc()

            # create error message from traceback
            c = StringIO()
            traceback.print_exc(file=c)
            errormsg = c.getvalue()

            # a bit of an ugly hack, but we need to raise a ConfigurationError,
            # *and* add the text of the "original" traceback to it
            # FIXME: pass this as an argument to ConfigurationError() instead
            try:
                raise ConfigurationError(
                      "Error when parsing XML file: %s" % f.name)
            except ConfigurationError, e:
                e.errormsg = errormsg
                raise

        return self._tree

    def read_database_info(self):
        self.ado_database = d = elem_as_dict(
          self._root.find('ticketparser/ado_database'),
          ['host', 'database', 'login', 'password', 'timeout_seconds', 
           'conn_string'])
        if d['conn_string'] and d['host']:
            raise ConfigurationError("Please specify either connection string"\
                                     " or database info, not both")
        d['timeout_seconds'] = int(d['timeout_seconds'] or DEFAULT_DB_TIMEOUT_SECONDS)

    def read_server_info(self):
        self.server_timezone = 'EST/EDT' # default
        tznode = self._root.find('ticketparser/server/timezone')
        if tznode is not None:
            self.server_timezone = tznode.text
        else:
            print >> sys.stderr, "No timezone specified in %s; defaulting to EST/EDT" % (
                self.filename)

    def call_center_process(self, call_center):
        for process in self.processes:
            if process['format'] == call_center:
                return process
        return {}

    @staticmethod
    def receiver_accounts(raw_accounts, destination, call_center, attachment_dir):
        email_accounts = []
        db_accounts = []
        for account in raw_accounts:
            acct = {
                'destination': destination,
                'call_center': call_center,
                'attachment_dir': attachment_dir,
                'type': account.get('type', 'email').lower()
            }

            if acct['type'] == 'email':
                acct['server'] = account.get('server', '')
                acct['username'] = account.get('username', '')
                acct['password'] = account.get('password', '')
                acct['full'] = bool(int(account.get('full', '0')))
                acct['store_attachments'] = bool(int(account.get('store_attachments', '0')))
                acct['ignore_attachments'] = bool(int(account.get('ignore_attachments', '0')))
                acct['file_ext'] = account.get('file_ext', '')
                acct['aggregate_file'] = account.get('aggregate_file', '')
                acct['use_ssl'] = bool(int(account.get('use_ssl', '0')))
                acct['port'] = int(account.get('port', '110') or 110)
                email_accounts.append(acct)
            elif acct['type'] == 'db':
                db_accounts.append(acct)
        return email_accounts, db_accounts

    def read_processes(self):
        self.processes = []
        attrs = ['incoming', 'format', 'processed', 'error', 'group',
                 'attachments', 'attachment_dir', 'attachment_proc_dir',
                 'upload_location', 'attachment_user', 'geocode_latlong']
        self.processes = elem_as_dictlist(self._root,
          'ticketparser/process', attrs, '', ['account'])

        for process in self.processes:
            # if there is no attachment_dir specified, infer it from the
            # incoming dir (c:/foo/bar/in -> c:/foo/bar/attachments)
            if not process['attachment_dir']:
                path, name = os.path.split(process['incoming'])
                attdir = os.path.join(path, 'attachments')
                process['attachment_dir'] = attdir

            process['attachments'] = int(process['attachments'] or '0')
            process['geocode_latlong'] = int(process['geocode_latlong'] or '0')

            # Accounts are split into 2 types: email ('type' == 'email'), and
            # db ('type' == 'db'). Currently, there should only be 1 db account
            # per process. Email accounts are used for items (tickets, audits,
            # etc.) received the traditional way, by email. And the db account
            # is used for items stored in the incoming_item database table. The
            # incoming_item table is currently being populated by a web service. 
            process['accounts'], process['db_accounts'] = self.receiver_accounts(
                process['account'], process['incoming'], process['format'],
                process['attachment_dir'])
            del process['account']

    def read_admins(self):
        self.admins = elem_as_dictlist(self._root,
          'ticketparser/admin',
          ['name', 'email', 'ln'])
        for admin in self.admins:
            if not admin['ln']:
                admin['ln'] = '1'

    def read_misc(self):
        self.logdir = safe_find(self._root, 'ticketparser/logdir').get(
                      'name', '')
        self.attachment_dir = safe_find(self._root, 'ticketparser/attachment_dir').get(
                      'name', RECEIVER_ATTACHMENT_DIR)
        self.polygon_dir = safe_find(self._root, 'polygon_dir').get('name', '')
        self.schemafile = safe_find(self._root, 'ticketparser/schema').get(
                          'name', '')

        ic_emails = safe_find(self._root, 'integrity_check').get('emails', '')
        self.ic_emails = [e for e in ic_emails.split(';') if e.strip()]

        #default = "rules/cc_routing_list.tsv"
        #n = safe_find(self._root, 'ticketrouter/cc_routing_list'
        #    ).get('filename', '')
        # as of Mantis #3022, this can no longer be specified in config.xml,
        # but the attribute can still be overridden for testing purposes.
        self.cc_routing_file = "rules/cc_routing_list.tsv"

        # read command-line utilities, if any
        '''
        self.utilities = {}
        n = safe_find(self._root, 'ticketparser/utilities')
        if n not in (None, {}): # bleh
            for ch in n.getchildren():
                try:
                    name = ch.tag
                    value = ch.get('path', '')
                    self.utilities[name] = value
                except:
                    pass
        '''

        self.store_events = int(safe_find(self._root, 'event_log').get(
         'store_events', 0))

    def read_smtp_data(self):
        import emailtools
        smtp_servers = []   # OBSOLETE
        self.smtp_accs = [] # NEW 3026/3027

        for s in self._root.findall('ticketparser/smtp'):
            d = elem_as_dict(s, ['host', 'from', 'use_ssl', 'port',
                                 'user', 'password'])
            d['use_ssl'] = int(d.get('use_ssl', '0') or 0)
            d['port'] = int(d.get('port', '25') or 25)
            smtp_servers.append(d)

            smtpinfo = emailtools.SMTPInfo(host=d['host'],
                                           from_address=d['from'],
                                           password=d['password'],
                                           user=d['user'],
                                           use_ssl = d['use_ssl'],
                                           port = d['port'])
            self.smtp_accs.append(smtpinfo)

        # will become OBSOLETE soon
        if smtp_servers:
            self.smtp = smtp_servers[0]['host']
            self.from_address = smtp_servers[0]['from']
            self.smtp_use_ssl = smtp_servers[0]['use_ssl']
        self.smtp_servers = smtp_servers

    def read_irthresponder_data(self):
        self.responders = {}
        attrs = ['name', 'server', 'port', 'ack_dir', 'ack_proc_dir',
                 'clientbased', 'all_versions', 'send_emergencies',
                 'send_3hour', 'parsed_locates_only']
        for r in self._root.findall('responder/irth/callcenter'):
            d = elem_as_dict(r, attrs)
            d['clientbased'] = int(d['clientbased'] or '0')
            d['all_versions'] = int(d['all_versions'] or '0')
            d['send_emergencies'] = int(d['send_emergencies'] or 1)
            d['send_3hour'] = int(d['send_3hour'] or 1)
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            d['logins'] = self.read_responder_logins(r)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)
            self.responders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'irth', d)

    def read_xmlhttpresponder_data(self):
        self.xmlhttpresponders = {}
        attrs = ['name', 'id', 'initials', 'post_url', 'resp_url', 'ack_dir',
                 'ack_proc_dir', 'all_versions', 'send_emergencies',
                 'send_3hour', 'parsed_locates_only', 'kind']
        for r in self._root.findall('responder/xmlhttp/callcenter'):
            d = elem_as_dict(r, attrs)
            d['initials'] = d['initials'] or 'UQ'
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)
            d['all_versions'] = int(d['all_versions'] or 0)
            d['send_3hour'] = int(d['send_3hour'] or 1)
            d['send_emergencies'] = int(d['send_emergencies'] or 1)

            self.xmlhttpresponders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'xmlhttp', d)

    def read_emailresponder_data(self):
        self.emailresponders = {}
        attrs = ['name', 'tech_code', 'resp_email', 'sender', 'subject',
                 'all_versions', 'send_emergencies', 'send_3hour', 'ack_dir',
                 'ack_proc_dir', 'parsed_locates_only']
        for r in self._root.findall('responder/email/callcenter'):
            d = elem_as_dict(r, attrs, '', ['account'])
            d['all_versions'] = int(d['all_versions'] or 0)
            d['send_emergencies'] = int(d['send_emergencies'] or 1)
            d['send_3hour'] = int(d['send_3hour'] or 1)
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)

            d['accounts'], _ = self.receiver_accounts(d['account'], d['ack_dir'], d['name'], '')
            del d['account']

            self.emailresponders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'email', d)

    def read_ftpresponder_data(self):
        self.ftpresponders = {}
        z = self._root.find('responder/ftp/output_dir')
        self.ftp_output_dir = z.get('name', '') if z is not None else ''
        #self.ftp_output_dir = (z is not None) and z.get('name', '') or ''
        attrs = ['name', 'server', 'login', 'password', 'outgoing_dir',
                 'temp_dir', 'file_ext', 'method', 'send_emergencies',
                 'send_3hour', 'parsed_locates_only', 'id', 'kind']
        for r in self._root.findall('responder/ftp/callcenter'):
            d = elem_as_dict(r, attrs)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)
            d['exclude_states'] = self.read_responder_exclude_states(r)
            d['send_3hour'] = int(d['send_3hour'] or 1)
            d['send_emergencies'] = int(d['send_emergencies'] or 1)
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            d['id'] = d.get('id', '') # optional
            d['kind'] = d.get('kind', '') # optional

            self.ftpresponders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'ftp', d)

    def read_xcelresponder_data(self):
        self.xcelresponders = {}
        attrs = ['name', 'output_dir', 'method', 'parsed_locates_only']
        for r in self._root.findall('responder/xcel/callcenter'):
            d = elem_as_dict(r, attrs)
            d['termids'] = [t.get('name') for t in r.findall('term')]
            d['clients'] = r['termids']
            d['recipients'] = elem_as_dictlist(r, 'recipient',
                              ['name', 'email'])
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            self.xcelresponders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'xcel', d)

    def read_wo_responder_data(self):
        self.wo_responders = {}
        attrs = ['name', 'resp_dir', 'ack_dir']
        for r in self._root.findall('responder/work_order/callcenter'):
            d = elem_as_dict(r, attrs)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)
            d['exclude_states'] = self.read_responder_exclude_states(r)
            self.wo_responders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'work_order', d)

    def read_soapresponder_data(self):
        self.soap_responders = {}
        attrs = ['name', 'url',
                 'clientbased', 'all_versions', 'send_emergencies',
                 'send_3hour', 'parsed_locates_only']
        for r in self._root.findall('responder/soap/callcenter'):
            d = elem_as_dict(r, attrs)
            d['clientbased'] = int(d['clientbased'] or '0')
            d['all_versions'] = int(d['all_versions'] or '0')
            d['send_emergencies'] = int(d['send_emergencies'] or 1)
            d['send_3hour'] = int(d['send_3hour'] or 1)
            d['parsed_locates_only'] = int(d.get('parsed_locates_only', 1) or 1)
            d['logins'] = self.read_responder_logins(r)
            d['mappings'] = self.read_responder_mappings(r)
            d['translations'] = self.read_responder_translations(r)
            d['skip'] = self.read_responder_skip(r)
            d['status_code_skip'] = self.read_responder_status_code_skip(r)
            d['clients'] = self.read_responder_clients(r)
            self.soap_responders[d['name']] = d
            self.responderconfigdata.add(d['name'], 'soap', d)

    def determine_nodelete(self):
        self.nodelete = [r.get('code', '')
          for r in self._root.findall('responder/nodelete/callcenter')]

        # responders that have configuration info, should always be in the
        # nodelete list. make sure this is so:
        self.nodelete.extend(self.responders.keys() + self.ftpresponders.keys()
          + self.xcelresponders.keys() + self.xmlhttpresponders.keys())

        self.nodelete = tools.unique(self.nodelete)

    def read_watchdog_data(self):
        self.watchdog = {}
        w = self._root.find('watchdog')
        if w is None: return
        self.watchdog['output_filename'] = safe_find(w, 'output'
          ).get('filename', '')
        threshold = safe_find(w, 'old_responses').get('threshold', '60')
        ignore = []
        for r in w.findall('old_responses'):
            z = safe_find(r, 'ignore').get('call_center', None)
            if z: ignore.append(z)
        self.watchdog['old_responses'] = {
            'threshold': int(threshold),
            'ignore': ignore,
        }

    # XXX TEST
    def read_archiver_data(self):
        self.archiver = elem_as_dict(self._root.find('archiver'),
          ['root', 'tar_path', 'bzip2_path', 'archive_dir'])
        dirs = [d.get('path', '')
                for d in self._root.findall('archiver/additional_dir')]
        self.archiver['additional_dirs'] = dirs

    def read_msmq_data(self):
        """ <msmq>
              <method>OS</method>
              <server>MYSERVER</server>
              <queue>private$\\QueueName</queue>
            </msmq>
        """
        ELEMS = ['method', 'server', 'queue']
        self.msmq = {}
        node = self._root.find('msmq')
        if node is not None:
            for subelem in ELEMS:
                value = node.find(subelem)
                if value is not None:
                    self.msmq[subelem] = value.text or ''
                else:
                    self.msmq[subelem] = ''

    def read_geocoder_info(self):
        geocoder_element = self._root.find('ticketparser/geocoder')
        if geocoder_element:
            d = elem_as_dict(geocoder_element,
             ['timeout_seconds', 'max_retries', 'retry_wait_seconds',
             'min_precision'])
        else:
            d = {}
         
        self.geocoder = {}
        self.geocoder['timeout_seconds'] = int(d.get('timeout_seconds', 5))
        self.geocoder['max_retries'] = int(d.get('max_retries', 2))
        self.geocoder['retry_wait_seconds'] = int(d.get('retry_wait_seconds', 0))
        self.geocoder['min_precision'] = int(d.get('min_precision', 30))

    #
    # helper methods to read responder subsections

    def read_responder_mappings(self, elem):
        """ Read the <code_mappings> section of a responder, and return a
            dictionary with the results. """
        mappings = {}
        for m in elem.findall('code_mappings/mapping'):
            d = elem_as_dict(m, ['uq_code', 'response', 'explanation'])
            mappings[d['uq_code']] = (d['response'], d['explanation'])
        return mappings

    def read_status_translations_db(self, rows):
        """ Update status translations from a row of database records
            (status_translation). """
        rcd = self.responderconfigdata
        for row in rows:
            z = rcd.get_responders(call_center=row['cc_code'],
                type=row['responder_type'], kind=row['responder_kind'] or None)

            if not z:
                # this will happen for non-call centers, like WGL
                # TODO: needs better checking/handling!
                print "not found:", row['cc_code'], row['responder_type']
            else:
                cc, type, respdata = z[0]
                mappings = respdata['mappings']
                mappings[row['status']] = (row['outgoing_status'],
                                           row['explanation_code'])

    def read_responder_translations(self, elem):
        translations = {}
        for t in elem.findall('translate_term'):
            d = elem_as_dict(t, ['from', 'to'])
            translations[d['from']] = d['to']
        return translations

    def read_responder_skip(self, elem):
        skip = []
        s = elem.find('skip')
        if s is not None:
            for t in s.findall('term'):
                name = U(t.get('name', ''))
                if name and name not in skip:
                    skip.append(name)
        return skip

    def read_responder_status_code_skip(self, elem):
        skip = []
        s = elem.find('status_code_skip')
        if s is not None:
            for t in s.findall('code'):
                name = U(t.get('value', ''))
                if name and name not in skip:
                    skip.append(name)
        return skip

    def read_responder_clients(self, elem):
        skip = []
        s = elem.find('clients')
        if s is not None:
            for t in s.findall('term'):
                name = U(t.get('name', ''))
                if name and name not in skip:
                    skip.append(name)
        return skip

    def read_responder_exclude_states(self, elem):
        exclude_states = []
        for x in elem.findall('exclude_state'):
            name = x.get('name', '')
            if name:
                exclude_states.append(name)
        return exclude_states

    def read_responder_logins(self, elem):
        logons = []
        for l in elem.findall('logon'):
            d = elem_as_dict(l, ['login', 'password'])
            logons.append(d)
        return logons

    def read_ticketparser_data(self):
        numrounds = 1
        wait = -1
        for p in self._root.findall('ticketparser'):
            numrounds = int(p.get('numrounds', 0) or 1)
            wait = int(p.get('wait', 0) or -1)
        self.ticketparser = {'numrounds': numrounds, 'wait': wait}

    def read_ticketrouter_data(self):
        numrounds = 1
        for p in self._root.findall('ticketrouter'):
            numrounds = int(p.get('numrounds', 0) or 1)
        self.ticketrouter = {'numrounds': numrounds}

    def read_location(self):
        """ Read location of config.py; from there, infer location of bin
            directory and command-line utilities. """
        self.location = os.path.dirname(os.path.abspath(__file__))
        self.bin_location = os.path.join(self.location, "bin")
        utils = [
            ["copy_attachment", "QMCopyAttachments.exe"],
            ["attachment_import", "QMAttachmentImport.exe"],
        ]
        self.utilities = dict((k, os.path.join(self.bin_location, v))
                         for (k, v) in utils)

    #
    # misc

    def getIRTHresponders(self):
        """ Return a list of call centers that we have responders for. """
        return self.responders.keys()
        # XXX does this need to include FTP and XCEL responders as well? A: NO!

    def getXMLHTTPresponders(self):
        """ Return a list of call centers that we have responders for. """
        return self.xmlhttpresponders.keys()

    def get_groups(self):
        # get groups from configuration
        groups = {}
        for proc in self.processes:
            group = proc[4]
            groups[group] = groups.get(group, []) + [proc]

        groups_sorted = groups.keys()
        groups_sorted.sort()

        return groups_sorted

    def switch_smtp_servers(self):
        """ If there are multiple SMTP servers, 'rotate' the list and set
            a new self.smtp and self.from_address. """
        if len(self.smtp_accs) >= 2:
            self.smtp_accs = self.smtp_accs[1:] + self.smtp_accs[:1]
            #self.smtp = self.smtp_servers[0]['host']
            #self.from_address = self.smtp_servers[0]['from']
            #self.smtp_use_ssl = self.smtp_servers[0]['use_ssl']
            return 1
        else:
            return 0

    def compare_config(self, config_to_compare):
        #returns a list of differences between this config and the supplied one
        #for now it just looks at status code translations
        old_mappings = {}
        for d in self.responderconfigdata.data:
            old_mappings[d[2]['name']] = d[2]['mappings']

        new_mappings = {}
        for d in config_to_compare.responderconfigdata.data:
            new_mappings[d[2]['name']] = d[2]['mappings']

        msgs = []
        for cc in old_mappings:
            try:
                new = new_mappings[cc]
                del new_mappings[cc]
            except KeyError:
                msgs.append('%s not found in new config.\n' % cc)
            else:
                if old_mappings[cc] != new:
                    msgs.append('%s status translations do not match:\n'
                                'old: %s\n'
                                'new: %s\n' % (cc, old_mappings[cc], new))

        for cc in new_mappings:
            msgs.append('%s not found in old config.' % cc)
        return msgs

_configuration = None

def getConfiguration(reload=0):
    global _configuration
    if reload or _configuration == None:
        _configuration = Configuration()
    return _configuration

def get_XML_configuration(reload=0):
    return Configuration()

def setConfiguration(config):
    global _configuration
    _configuration = config

def reload():
    global _configuration
    _configuration = Configuration()
    return _configuration

x_re_admin = re.compile(
 "<admin *name *= *['\"](.*?)['\"]\s+email *= *['\"](.*?)['\"]",
 re.MULTILINE)
x_re_smtp = re.compile(
 "<smtp *host *= *['\"](.*?)['\"]\s+from *= *['\"](.*?)['\"]",
 re.MULTILINE)

def extract_error_emails(data):
    """ Extract error emails in case the regular parsing doesn't work. """
    admins = []
    for name, email in re.findall(x_re_admin, data):
        d = {"name": name, "email": email}
        admins.append(d)
    return admins

def extract_smtp_data(data):
    """ Extract SMTP data from raw XML. Returns a tuple (smtphost,
        from_address). """
    m = x_re_smtp.search(data)
    if m:
        smtp = m.group(1)
        from_address = m.group(2)
        return smtp, from_address
    else:
        raise ConfigurationError, "SMTP data could not be retrieved"


if __name__ == "__main__":

    import pprint
    cfg = Configuration()
    pprint.pprint(cfg.__dict__)
    print dir(cfg._root)
    print cfg._root.keys()
    print cfg._root.items()

