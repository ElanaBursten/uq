# schemaparser.py
# XXX OBSOLETE?

import re

re_table_header = "^\s*create\s+table\s+" # don't compile
re_identifier = re.compile("[_a-zA-Z][_a-zA-Z0-9]*")
re_varchar = re.compile("varchar\((\d+)\)")
re_decimal = re.compile("decimal\((\d+),\s*(\d+)\)")

field_types = ["varchar", "bit", "integer", "decimal", "datetime", "money",
               "text", "int", "tinyint"]

class SchemaError(Exception):
    pass

class SchemaParser:

    def __init__(self, schema):
        self.schema = schema
        self.lines = schema.split("\n")

    def extract_table_data(self, tablename):
        start, end = self.get_table_line_numbers(tablename)
        fields = self.get_fields(start+1, end)
        a = [(name, self.split_field_indicator(indic))
             for name, indic in fields]
        b = [(name, self.make_smt_string(si))
             for name, si in a]
        return b

    def get_table_line_numbers(self, tablename):
        s = re_table_header + tablename
        r = re.compile(s, re.IGNORECASE)

        start = -1
        for idx, line in enumerate(self.lines):
            if r.search(line):
                start = idx
                break
        if start < 0:
            raise SchemaError, "Could not find beginning of table %r" % (tablename,)
        for idx, line in enumerate(self.lines[start:]):
            if line.strip() == ")":
                end = idx + start
                break
        else:
            raise SchemaError, "Could not find end of table %r" % (tablename,)

        return start, end

    def get_fields(self, start, stop):
        """ Return a list of fields found between the given lines.  Items are
            of the form (name, indicator) where indicator is the field
            indicator found in the schema, e.g. "int" or "varchar(20)".
            NOTE: spaces are not allowed inside what we consider a "field";
            e.g. "decimal(6, 2)" will break!  Use decimal(6,2) instead.
            (This is a very limited parser.)
        """
        fields = []

        for line in self.lines[start:stop]:
            parts = line.split()
            if len(parts) < 2:
                continue # this is not a field definition
            if parts[0] == "/*":
                continue # this is a comment
            name = parts[0].lower().strip()
            indicator = parts[1].lower().strip()
            if indicator.endswith(","):
                indicator = indicator[:-1]
            if self.is_identifier(name) and self.is_field(indicator):
                # extract field info
                fields.append((name, indicator))
            else:
                print "** Cannot parse: %r" % (line,)

        return fields

    def is_field(self, s):
        s = s.lower()
        for t in field_types:
            if s.startswith(t):
                return True
        return False

    def is_identifier(self, s):
        m = re_identifier.match(s)
        return bool(m)

    def split_field_indicator(self, indicator):
        """ Split a field indicator into its parts.  The result is always
            a list with the field type as the first item.
            E.g. "varchar(20)" -> ["varchar", "20"]
                 "datetime" -> ["datetime"]
                 "decimal(5,9)" -> ["decimal", "5", "9"]
        """
        SIMPLE_TYPES = ["integer", "money", "bit", "datetime", "text", "int",
                        "tinyint"]
        if indicator in SIMPLE_TYPES:
            return [indicator]
        if indicator.startswith("varchar"):
            m = re_varchar.search(indicator)
            return ["varchar", m.group(1)]
        if indicator.startswith("decimal"):
            m = re_decimal.search(indicator)
            return ["decimal", m.group(1), m.group(2)]
        raise SchemaError, "Unknown field indicator: %r" % (indicator,)

    def make_smt_string(self, split_indicator):
        """ Return an "SMT string" from the given "split indicator".  A
            split indicator is a list as returned by split_field_indicator,
            e.g. ["varchar", "20"] or ["integer"].  An SMT string is the
            string representation of the field as defined in sqlmaptypes.py.
        """
        ftype, params = split_indicator[0], tuple(split_indicator[1:])
        if ftype in ("int", "integer"):
            return "smt.sqlint"
        elif ftype == "tinyint":
            return "smt.sqltinyint"
        elif ftype == "varchar":
            return "smt.sqlstring(%s)" % params
        elif ftype == "datetime":
            return "smt.sqldatetime"
        elif ftype == "decimal":
            return "smt.sqldecimal(%s,%s)" % params
        elif ftype == "text":
            return "smt.sqltext"
        elif ftype == "bit":
            return "smt.sqlbool"
        elif ftype == "money":
            return "smt.sqlcurrency"
        raise SchemaError, "Unknown field indicator: %r" % (split_indicator,)

