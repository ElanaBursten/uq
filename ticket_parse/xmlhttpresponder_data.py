# xmlhttpresponder_data.py

import abstractresponderdata
import callcenters
import traceback
import tools

class XMLHTTPResponderError(Exception):
    pass

class XMLHTTPResponderData(abstractresponderdata.AbstractResponderData):
    client_based_login = 0

    acceptable_codes = ["invalid id/cdc combination",
                        "invalid serial number",
                        "member not on ticket",
                        "invalid response code",
                        "response posted"]

    codes_ok = ["response posted","invalid mark date", "response past due"]
    codes_reprocess = []
    codes_error = ["invalid id/cdc combination", "invalid serial number",
                   "member not on ticket", "response past due", "invalid response code",
                   "duplicate response", "item closed or canceled"]

    def get_xml(self, names):
        raise NotImplementedError("Override get_xml method in subclasses")
        #return self.XML_TEMPLATE % locals()

    def process_response(self, reply):
        """ Overload in call center if necessary. """
        return reply.Text

    def response_is_success(self, row, resp):
        raise NotImplementedError

    def filter_versions(self, versions):
        return versions

def getresponderdata(config, call_center, raw_responderdata):
    """ Try to get and instantiate an XMLHTTPResponderData class for the given
        call center. If not found, raise an XMLHTTPResponderError. Looks
        for the given class in 3 ways: in the call center's registry; in the
        namespace of this module; and in the 'callcenters' package.
        NOTE: The 2nd lookup method is obsolete now, but I'll leave it in
        because it's useful for testing.
    """
    prefix = call_center
    if prefix[0] in "0123456789":
        prefix = "c" + prefix

    reg = callcenters.get_registry(prefix)
    if raw_responderdata:
        kind = raw_responderdata.get('kind', '')
        if raw_responderdata.get('manual_mode', 0):
            raw_responderdata = None
    else:
        kind = ''

    # Find the right responder in the registry. 'kind' is optional.
    klass = None
    for (_, type, rkind), rclass in reg.items():
        if (type == 'xmlhttp') and ((kind or '') == (rkind or '')):
            klass = rclass
            break

    if klass is None: # should not happen, unless in tests!
        klassname = prefix + "XMLHTTPResponderData"
        try:
            klass = globals()[klassname]
        except KeyError:
            try:
                klass = callcenters.get_xmlhttpresponderdata(prefix)
            except:
                traceback.print_exc()
                raise XMLHTTPResponderError(
                 "Could not find XMLHTTPResponderData class for %s" %
                 call_center)

    rd = klass()

    # inject data provided or from configuration
    if not raw_responderdata:
        # only for testing purposes
        try:
            raw_responderdata = config.xmlhttpresponders[call_center]
        except KeyError:
            raise KeyError, "No responder data for " + call_center

    rd.response_codes = raw_responderdata["mappings"]
    for attr in ('name', 'id', 'initials', 'post_url', 'resp_url', 'mappings',
                 'translations', 'skip', 'all_versions', 'clients',
                 'send_emergencies', 'ack_dir', 'ack_proc_dir','send_3hour'):
        setattr(rd, attr, raw_responderdata[attr])

    try:
        rd.client_based_login = int(raw_responderdata["clientbased"])
        # do client-based XMLHTTP responders even exist?
    except (ValueError, KeyError):
        rd.client_based_login = 0

    # override 'skip' to use values in ResponderConfigData
    old_skip = rd.skip
    new_skip = config.responderconfigdata.get_skip_data(call_center)
    rd.skip = tools.unique(old_skip + new_skip)
    rd.status_code_skip = config.responderconfigdata.get_status_code_skip_data(call_center)

    return rd
