# ticket_grid.py
# Created: 2003.07.30
# Handles storage, retrieval, etc of a ticket's grids.
#
# Note:
# ticket_grid: stores grids associated with a certain ticket
# hp_grid: used to match call center/client and grids

def insert_grids(tdb, ticket_id, grids):
    sql = "insert ticket_grid (ticket_id, grid) values (%s, '%s')"
    for grid in grids:
        sql2 = sql % (ticket_id, grid)
        tdb.dbado.runsql(sql2)

def get_grids(tdb, ticket_id):
    rows = tdb.dbado.getrecords('ticket_grid', ticket_id=ticket_id)
    result = [row['grid'] for row in rows]
    result.sort()
    return result

def update_grids(tdb, ticket_id, old_grids, new_grids):
    grids = update_diff(old_grids, new_grids)
    if grids:
        insert_grids(tdb, ticket_id, grids)

def update_diff(old_grids, new_grids):
    grids = []
    for grid in new_grids:
        if grid not in old_grids:
            grids.append(grid)
    return grids

def get_date_sent(tdb, call_center, ticket_number, client_code):
    """ Return the most recent date/time that we sent an email for this
        ticket. """
    sql = """
     select max(tgl.time_sent) as latest
     from ticket t, ticket_grid_log tgl
     where t.ticket_id = tgl.ticket_id
     and t.ticket_number = '%s'
     and t.ticket_format = '%s'
     and tgl.client_code = '%s'
    """ % (ticket_number, call_center, client_code)
    rows = tdb.dbado.runsql_result(sql)
    if rows:
        return rows[0]['latest'] or '1980-01-01 00:00:00'
    else:
        return "1980-01-01 00:00:00"

