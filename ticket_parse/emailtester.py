# emailtester.py
# Send a test email using a given address, SMTP server, etc.
# Not part of the test suite.
#
# Created: 2002.10.21 HN

import getopt
import string
import sys
import traceback
#
import config
import mail2

__usage__ = """\
emailtester.py [options]

Test an SMTP server.

Options:
    -h host     Specify SMTP host.
    -f email    Use a different 'from' address.
    -t emails   Use different 'to' addresses. This is a list separated by
                semicolons, e.g. test@test.com;john@doe.com ...etc.
    -p port     Specify SMTP port (default 25).
    -S          Use SSL.
    -u user     Username (if SMTP server requires login).
    -P pass     Password (if SMTP server requires login).
"""


if __name__ == "__main__":

    cfg = config.getConfiguration()
    smtpinfo = cfg.smtp_accs[0]
    toaddrs = [a["email"] for a in cfg.admins]
    subject = "Test email"
    body = "This is just a test email to check if the SMTP server works."

    opts, args = getopt.getopt(sys.argv[1:], "h:f:p:P:St:u:?")
    for o, a in opts:
        if o == "-h":
            smtpinfo.host = a
        elif o == "-f":
            smtpinfo.from_address = a
        elif o == "-t":
            toaddrs = string.split(a, ";")
        elif o == '-p':
            smtpinfo.port = int(a)
        elif o == '-S':
            smtpinfo.use_ssl = True
        elif o == '-u':
            smtpinfo.user = a
        elif o == '-P':
            smtpinfo.password = a
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)

    print "Sending test email -- using:"
    print "SMTP server:", smtpinfo
    print "To-addresses:", toaddrs

    body += "\n\n" + repr(smtpinfo) # show a little more info in the email

    print "Sending email..."
    try:
        mail2.sendmail(smtpinfo, toaddrs, subject, body)
    except:
        traceback.print_exc()
    else:
        print "Mail sent successfully!"

