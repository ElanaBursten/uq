import os,sys
import optparse
try:
    import pysvn
except:
    print("For this version of svn_deploy, the pysvn package is required")
    print("This package is available from http://pysvn.tigris.org/project_downloads.html")
    sys.exit(1)

parser = optparse.OptionParser()
parser.add_option('-u','--user', action="store", dest="deploy_user",help="subversion login")
parser.add_option('-p','--password', action="store", dest="deploy_password",help="subversion password")
parser.add_option('-s','--server', action="store", dest="deploy_server",help="subversion server")
(options, args) = parser.parse_args()

# get the environment
if options.deploy_user is None:
    # Try extracting it from the environment
    deploy_user = os.environ["DEPLOY_USER"]
else:
    deploy_user = options.deploy_user
if deploy_user is None:
    print("The subversion user has not been defined, either set DEPLOY_USER"+
          " in the environment or with the --user parameter")
if options.deploy_password is None:
    deploy_password = os.environ["DEPLOY_PASSWORD"]
else:
    deploy_password = options.deploy_password
if deploy_password is None:
    print("The subversion password has not been defined, either set DEPLOY_PASSWORD"+
          " in the environment or with the --password parameter")
if options.deploy_password is None:
    deploy_server = os.environ["DEPLOY_SERVER"]
else:
    deploy_server = options.deploy_server
if deploy_server is None:
    print("The subversion server has not been defined, either set DEPLOY_SERVER"+
          " in the environment or with the --server parameter")

# Get the version
try:
    if args[0].upper() == 'HEAD':
        version = pysvn.Revision( pysvn.opt_revision_kind.head )
    else:
        version = pysvn.Revision( pysvn.opt_revision_kind.number, int(args[0]) )
except:
    print("Desired subversion revision (either a revision number of HEAD) must be the first argument")
    sys.exit(1)

print("Desired version: %s" % (version,))

def get_login( realm, username, may_save ):
    return True, deploy_user, deploy_password, False

def ssl_server_trust_prompt( trust_dict ):
    return True, 100, False

status = None
completed_revision = None

def notify( event_dict ):
    '''
    The dictionary contains the following values:

        * path - the path of the action refers to
        * action - the events action, one of the wc_notify_action values
        * kind - the node kind, one of the pysvn.node_kind values
        * mime_type - the mime type
        * content_state - one of the pysvn.wc_notify_state values
        * prop_state - one of the pysvn.wc_notify_state values
        * revision - a Revision object
    '''
    global completed_revision
    if event_dict["error"] is not None:
        status = event_dict["error"]
    if event_dict["action"] == pysvn.wc_notify_action.update_completed:
        completed_revision = event_dict["revision"]

client = pysvn.Client()
client.exception_style = 0
client.callback_get_login = get_login
client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
client.callback_notify = notify

directories = ["ticket_parse",
               "callcenters",
               "formats",
               "lib"]

# Check the status
if os.path.exists("staging"):
    for directory in directories:
        # Does the directory exist
        staging_path = os.path.join("staging", directory)
        if os.path.exists(staging_path):
            try:
                # Cleanup
                client.cleanup(staging_path)
                # Update
                revision = \
                client.update( staging_path,
                               recurse=True,
                               revision=version)
            except pysvn.ClientError, e:
                # convert to a string
                print str(e)
                sys.exit(1)

        else:
            if directory == 'ticket_parse':
                try:
                    revision = \
                    client.checkout( deploy_server,
                                     staging_path,
                                     recurse=True,
                                     revision=version)
                except pysvn.ClientError, e:
                    # convert to a string
                    print str(e)
                    sys.exit(1)

            else:
                try:
                    revision = \
                    client.checkout( deploy_server + "/" + directory,
                                     staging_path,
                                     recurse=True,
                                     revision=version)
                except pysvn.ClientError, e:
                    # convert to a string
                    print str(e)
                    sys.exit(1)


else:
    # Staging directory doesn't exist, just do a checkout
    try:
        revision = \
        client.checkout( deploy_server,
                         "staging/ticket_parse",
                         recurse=True,
                         revision=version)
    except pysvn.ClientError, e:
        # convert to a string
        print str(e)
        sys.exit(1)


    for directory in directories[1:]: # We've already done ticket_parse
        try:
            revision = \
            client.checkout( deploy_server + "/" + directory,
                             "staging/" + directory,
                             recurse=True,
                             revision=version)
        except pysvn.ClientError, e:
            # convert to a string
            print str(e)
            sys.exit(1)


print("Error: %s" % (status,))
print("Revision: %s" % (completed_revision.number,))
# Write the version to version.dat
f = open("version.dat","w+")
f.write("%s" % (completed_revision.number,))
f.close()
