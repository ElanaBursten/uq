# watchdog.py
# Warns if incoming tickets are more than N minutes old, and a whole bunch of
# other stuff. :-)
# Created: 06 Jun 2002, Hans Nowak
# Woof.

from __future__ import with_statement
import datetime
import getopt
import os
import string
import sys
import time
import warnings
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import blacklist
import config
import configchecker
import datadir
import date
import dbinterface_ado
import errorhandling2
import errorhandling_special
import mail2
import mutex
import program_status
import responder_data
import showactive
import ticket_db
import ticketdb_decorator
import tools
import trackerfile
import version
import windows_tools
from check_python_version import check_python_version

PENDING_TICKETS_THRESHOLD = 300     # number of tickets
PENDING_TICKETS_AGE_THRESHOLD = 3   # number of hours tickets have been pending
PENDING_EMERGENCIES_THRESHOLD = 50  # number of emergencies
TICKETS_THRESHOLD = 9               # in minutes
INCOMING_ITEMS_THRESHOLD = 9        # in minutes
DAMAGE_THRESHOLD = 20

TRACKER_FILENAME = "watchdog-tracker.txt"
PROCESS_NAME = "watchdog"

def watchdog_mutex_name():
    return PROCESS_NAME + ":" + datadir.datadir.get_id()

class WatchdogOptions(tools.Bunch):
    threshold = TICKETS_THRESHOLD
    configfile = ""
    testmode = 0   # if set, don't send emails, don't run ConfigChecker
    verbose = 1
    wait = 0       # if set, wait for input before we end

class Watchdog:

    def __init__(self, options, dbado=None):
        self.options = options

        if self.options.configfile:
            self.config = config.Configuration(self.options.configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()
        self.watchdogcfg = self.config.watchdog

        if dbado:
            self.dbado = dbado
        else:
            self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado)
        self.tdb = ticketdb_decorator.TicketDBDecorator(self.tdb,
                   ticketdb_decorator.TicketDBMethodDecorator)

        # create and configure the logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="Watchdog",
                   admin_emails=self.config.admins,
                   subject="Watchdog Warning")
                   # doesn't log per call center
        self.log.defer_mail = 1
        self.log.verbose = self.options.verbose
        self.log.log_event("Watchdog started (version %s)" % (
         version.__version__,))

        # write program status (used by showactive.py)
        # XXX partially redundant! should MutexLock do this?
        program_status.write_status(watchdog_mutex_name())

        self.oldfiles = []  # 2-tuples (name, minutes_old)
        self.total = 0  # number of files in queue

        if not self.options.testmode:
            self.configchecker = configchecker.ConfigChecker(self.log)
            self.configchecker.check(self.config)

        # some configuration options -- may be set through command line options
        # later; for now, the test suite uses them
        self.dir_old_tickets = ""   # if set, override config.processes

    def say(self, *args):
        if self.options.verbose:
            for arg in args:
                print arg,

    def sayln(self, *args):
        if self.options.verbose:
            self.say(*args)
            print

    def scan_directories(self):
        """ Scan directories in config.xml for incoming tickets older than
            the given threshold value. """
        self.oldfiles = []
        self.total = 0
        if self.dir_old_tickets:
            dirs = [self.dir_old_tickets]
        else:
            # dirs should be absolute paths
            dirs = [p['incoming'] for p in self.config.processes]

        dirs = tools.unique(dirs) # don't scan the same directory multiple times
        for _dir in dirs:
            if os.path.exists(_dir):
                self.scan_directory(_dir)
            else:
                print >> sys.stderr, "Directory %s does not exist" % _dir

        self.sort_oldfiles()
        self.say(len(self.oldfiles), "files are older than")
        self.sayln(self.options.threshold, "minutes")

    def sort_oldfiles(self):
        o = [(age, filename, age) for (filename, age) in self.oldfiles]
        o.sort()
        o.reverse()
        self.oldfiles = [(filename, age) for (_, filename, age) in o]

    def scan_directory(self, incomingdir):
        self.sayln("Scanning:", incomingdir)
        files = os.listdir(incomingdir)
        fullnames = [os.path.join(incomingdir, name) for name in files]
        fullnames = [f for f in fullnames if not os.path.isdir(f)]
        self.total = self.total + len(fullnames)

        for filename in fullnames:
            try:
                atime, mtime, ctime = os.stat(filename)[-3:]
            except OSError:
                # this can happen if the file is being processed by the parser;
                # in that case, we just ignore this file
                continue
            # we probably need the ctime
            minutes_old = (time.time() - mtime) / 60
            if abs(minutes_old) > self.options.threshold:
                self.sayln("File %s too old (%.2f minutes)" % (filename,
                 minutes_old))
                self.oldfiles.append((filename, minutes_old))

    def send_warning(self, oldfiles, num_pending_tickets,
        num_pending_tickets_age, old_responses, num_pending_emergencies,
        pending_damages, blacklisted_tickets, running_procs,
        num_incoming_tickets, num_incoming_tickets_age, send=1):

        body = self._make_warning_mail_body(oldfiles, num_pending_tickets,
               num_pending_tickets_age, old_responses, num_pending_emergencies,
               pending_damages, blacklisted_tickets, running_procs, 
               num_incoming_tickets, num_incoming_tickets_age)

        tf = StringIO()
        try:
            if send:
                try:
                    self.log.log_warning(body, dump=1, write=1, send=1)
                except:
                    traceback.print_exc(file=tf)
                    raise
            else:
                self.sayln("This message would have been sent:")
                self.sayln('"""' + body + '"""')

        finally:
            # write report, regardless of whether the send was successful
            body = "Sent: " + date.Date().isodate() + os.linesep + os.linesep + body
            tfbody = tf.getvalue()
            if not tfbody:
                tfbody = mail2.last_timeout_error
            if tfbody.strip():
                body += "\n\nError when trying to send watchdog email:\n\n" + tfbody
            self.write_report(body)

        return body

    def _make_warning_mail_body(self, oldfiles, num_pending_tickets,
        num_pending_tickets_age, old_responses, num_pending_emergencies,
        pending_damages, blacklisted_tickets, running_procs,
        num_incoming_tickets, num_incoming_tickets_age):

        cs = StringIO()

        if oldfiles:
            print >> cs, "The following files are older than", \
                         self.options.threshold,
            print >> cs, "minutes:"
            print >> cs
            for filename, minutes_old in oldfiles[:50]:
                print >> cs, filename,
                print >> cs, "(%.2f minutes old)" % (minutes_old,)
            if len(oldfiles) > 50:
                print >> cs, "(...more results truncated...)"
            print >> cs
            print >> cs, "A total of", len(oldfiles), "is older than",
            print >> cs, self.options.threshold, "minutes."
            print >> cs, "Total files in queue:", self.total
            print >> cs

        if num_pending_tickets >= PENDING_TICKETS_THRESHOLD:
            print >> cs, \
                     "There are", \
                     num_pending_tickets, \
                     "tickets waiting", \
                     "to be routed."
            print >> cs

        if num_pending_tickets_age:
            print >> cs, \
                     "There are", \
                     num_pending_tickets_age, \
                     "tickets older than", \
                     PENDING_TICKETS_AGE_THRESHOLD,\
                     "hours waiting", \
                     "to be routed."
            print >> cs

        if num_incoming_tickets:
            # report this for completeness
            print >> cs, "There are", \
                     num_incoming_tickets, "items in the incoming_items table."
            print >> cs

        if num_incoming_tickets_age:
            print >> cs, \
                     "There are", num_incoming_tickets_age, \
                     "items older than", \
                     INCOMING_ITEMS_THRESHOLD, "minutes", \
                     "in the incoming_item table."
            print >> cs

        if num_pending_emergencies >= PENDING_EMERGENCIES_THRESHOLD:
            print >> cs, "There are", num_pending_emergencies, \
                  "emergencies waiting to be routed."
            print >> cs

        if old_responses:
            RESPONDER_THRESHOLD = self.watchdogcfg['old_responses']['threshold']
            print >> cs, "Responses older than", RESPONDER_THRESHOLD, \
                         "minutes:"
            print >> cs
            print >> cs, "Call center     oldest               # pending"
            print >> cs, "-----------     ------               ---------"
            FMT = "%-15s %-20s %s"
            for key, (oldest, number) in old_responses.items():
                oldest = date.Date(string.replace(oldest, "T", " ")).isodate()
                print >> cs, FMT % (key, oldest, number)
            print >> cs

        if pending_damages:
            print >> cs, "Damages older than", DAMAGE_THRESHOLD, "minutes",
            print >> cs, "with no due date:", len(pending_damages)
            print >> cs

        if blacklisted_tickets:
            print >> cs, "The following tickets are blacklisted:"
            print >> cs
            for row in blacklisted_tickets:
                print >> cs, "* ticket_id", row['ticket_id'],
                print >> cs, "call center:", row['ticket_format'],
                print >> cs, "ticket number:", row['ticket_number'],
                print >> cs, "ticket type:", row['ticket_type']
            print >> cs

        if running_procs:
            print >> cs, "The following processes are running on the server:"
            print >> cs
            print >> cs, "Name                            PID     Active since"
            print >> cs, "------------------------------  ------  -------------------"
            for proc in running_procs:
                name, pid, startdate = proc
                print >> cs, "%-30s  %-6s  %s" % (name, pid, startdate)
        print >> cs

        body = cs.getvalue()
        return body

    def write_report(self, data):
        # write report, regardless of whether the send was successful
        output_filename = self.watchdogcfg.get('output_filename', '')
        if output_filename:
            self.log.log_event("Writing %r..." % (output_filename,))
            f = datadir.datadir.open(output_filename, 'w')
            f.write(data)
            f.close()

    def get_old_files(self):
        self.scan_directories()
        return self.oldfiles

    def get_pending_tickets(self):
        self.say("Getting number of tickets to be routed...")
        sql = """
         select count(distinct ticket_id) as c
         from locate (NOLOCK)
         where status = '-P'
         and active = 1
         and closed = 0
        """
        results = self.tdb.runsql_result(sql)
        self.sayln("OK")
        count = int(results[0]["c"])
        self.sayln("There are", count, "ticket(s) waiting to be routed.")
        return count

    def get_pending_tickets_age(self):
        self.say("Getting number of tickets to be routed older than %s hours..."
         % PENDING_TICKETS_AGE_THRESHOLD)
        sql = """
         select *
         from locate (NOLOCK)
         where status = '-P'
         and active = 1
         and closed = 0
        """
        pending_locates = self.tdb.runsql_result(sql)
        count = 0
        threshold = datetime.timedelta(hours=PENDING_TICKETS_AGE_THRESHOLD)
        cutoff = datetime.datetime.now() - threshold
        for locate in pending_locates:
            # IronPython has no datetime.strptime; this seems to be the
            # recommended alternative:
            dt = datetime.datetime(*(time.strptime(locate['modified_date'],
                  "%Y-%m-%d %H:%M:%S")[0:6]))
            if dt < cutoff:
                count += 1
        self.sayln("OK")
        self.sayln("There are",
                   count,
                   "ticket(s) older than",
                   PENDING_TICKETS_AGE_THRESHOLD,
                   "hours waiting to be routed.")
        return count

    def get_incoming_tickets(self):
        """ Get any pending tickets in the incoming_item table. (QMAN-3436) """
        self.say("Getting tickets in incoming_item table...")
        sql = """
            select count(item_id) as c
            from incoming_item
        """
        rows = self.tdb.runsql_result(sql)
        numtix = int(rows[0]['c'])
        self.sayln("OK")
        self.sayln("There are", numtix, "tickets in the incoming_item table.")
        return numtix

    def get_incoming_tickets_age(self):
        # QMAN-3436 also
        self.say("Getting items in the incoming_item table",
                 "older than %s minutes..." % INCOMING_ITEMS_THRESHOLD)

        sql = """
          select count(item_id) as c from incoming_item
          where insert_date < DateAdd(minute, %d * -1, GetDate())
        """ % INCOMING_ITEMS_THRESHOLD
        rows = self.tdb.runsql_result(sql)
        count = int(rows[0]['c'])

        self.sayln("OK")
        self.sayln("There are",
                   count,
                   "ticket(s) older than",
                   INCOMING_ITEMS_THRESHOLD,
                   "minutes in the incoming_item table.")
        return count

    def get_pending_emergencies(self):
        self.say("Getting number of emergency tickets to be routed...")
        sql = """
         select count(distinct ticket.ticket_id) as c
         from locate (NOLOCK)
          INNER JOIN ticket (NOLOCK)
          ON locate.ticket_id = ticket.ticket_id
         where locate.status = '-P'
         and ticket.kind = 'EMERGENCY'
         and locate.active = 1
        """
        results = self.tdb.runsql_result(sql)
        self.sayln("OK")
        count = int(results[0]["c"])
        self.sayln("There are", count, "emergencies waiting to be routed.")
        return count

    def _get_old_responses_rows(self):
        # we use a separate query here, rather than reusing
        # get_pending_responses, because we're counting things.
        # we don't want/need to get thousands of records back.
        sql = """
         select rq.ticket_format, min(rq.insert_date) as oldest,
         count (*) as N
         from responder_queue rq
         inner join locate l on rq.locate_id = l.locate_id
         inner join ticket t on t.ticket_id = l.ticket_id
         where (rq.do_not_respond_before is null
             or rq.do_not_respond_before <= '%s')
             and t.ticket_number not like 'MAN%%'
         group by rq.ticket_format
        """ % (date.Date().isodate(),)

        results = self.tdb.runsql_result(sql)
        return results

    def get_old_responses(self):
        RESPONDER_THRESHOLD = self.watchdogcfg['old_responses']['threshold']
        self.say("Getting old responses...")
        old_responses = {}
        tracker = trackerfile.TrackerFile(TRACKER_FILENAME)

        # we use a separate query here, rather than reusing
        # get_pending_responses, because we're counting things.
        # we don't want/need to get thousands of records back.
        results = self._get_old_responses_rows()
        self.sayln("OK")

        for row in results:

            # when did we last check this for this call center?
            call_center = row['ticket_format']
            if call_center in self.watchdogcfg['old_responses']['ignore']:
                continue

            # if this call center has a special query defined, then use that
            # instead
            if responder_data.special_queries.has_key(call_center):
                special_query = responder_data.special_queries[call_center]
                special_rows = self.tdb.runsql_result(special_query)
                count = len(special_rows)
                if not count:
                    continue
                    # this result should not be displayed, because there are
                    # no old responses
                oldest_date = max([row['insert_date'] for row in special_rows])
            else:
                oldest_date = row['oldest']
                count = row['N']

            d = date.Date(oldest_date)
            now9 = time.localtime(time.time())   # a 9-tuple
            d9 = (d.year, d.month, d.day, d.hours, d.minutes, d.seconds) \
                 + now9[-3:]
            diff = time.mktime(now9) - time.mktime(d9)  # in seconds
            if (diff / 60.0) >= RESPONDER_THRESHOLD:
                old_responses[call_center] = (oldest_date, count)
                self.sayln(count, "responses found for",
                  call_center + ";", "oldest:", oldest_date)

        # when did we do the last check?
        now = date.Date()
        last_time = date.Date(tracker.get("old_responses"))
        diff = date.Date(now).diff(last_time)
        if abs(diff) < 3600:
            # skip, we checked this less than an hour ago
            self.sayln("Responses not sent, checked less than an hour ago")
            old_responses = {}

        # if we are going to send a watchdog warning, register the time of
        # the check
        if old_responses:
            tracker.update('old_responses', now.isodate())
            tracker.save()

        return old_responses

    def get_pending_damages(self):
        self.say("Getting pending damages...")
        sql = """
         select * from damage
         where due_date is null
         and modified_date <= '%s'
        """
        d = date.Date()
        d.dec_time(minutes=DAMAGE_THRESHOLD)
        sql = sql % (d.isodate(),)
        results = self.tdb.runsql_result(sql)
        self.sayln("OK")
        return results

    def get_blacklisted_tickets(self):
        self.say("Getting blacklisted tickets...")
        bl = blacklist.BlackList(self.log)
        ticket_ids = bl.read()

        # get more information about these tickets
        rows = []
        for ticket_id in ticket_ids:
            row = self.tdb.getrecords("ticket", ticket_id=ticket_id)
            try:
                rows.append(row[0])
            except IndexError:
                pass

        self.sayln("OK (%d blacklisted tickets found)" % (len(rows),))
        return rows

    def run(self):
        self.log.log_event("watchdog.py (version %s) started with pid %s" % (
         version.__version__, os.getpid(),))

        oldfiles = self.get_old_files()
        num_pending_tickets = self.get_pending_tickets()
        num_pending_tickets_age = self.get_pending_tickets_age()
        num_incoming_tickets = self.get_incoming_tickets()
        num_incoming_tickets_age = self.get_incoming_tickets_age()
        old_responses = self.get_old_responses()
        num_pending_emergencies = self.get_pending_emergencies()
        pending_damages = self.get_pending_damages()
        #blacklisted_tickets = self.get_blacklisted_tickets()
        blacklisted_tickets = []

        if (oldfiles
        or num_pending_tickets >= PENDING_TICKETS_THRESHOLD
        or num_pending_tickets_age
        or num_pending_emergencies >= PENDING_EMERGENCIES_THRESHOLD
        or num_incoming_tickets_age
        or old_responses
        or blacklisted_tickets
        or pending_damages):
            procs = showactive.get_running_processes(self.dbado)
            self.send_warning(oldfiles, num_pending_tickets,
             num_pending_tickets_age, old_responses,
             num_pending_emergencies, pending_damages, blacklisted_tickets,
             procs, num_incoming_tickets, num_incoming_tickets_age,
             send=not self.options.testmode)

        if self.options.wait:
            raw_input("[-w option specified] Press Enter to finish.")


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "c:n:tw")

    options = WatchdogOptions()

    for o, a in opts:
        if o == "-c":
            options.configfile = a
        elif o == "-n":
            options.threshold = int(a)
        elif o == "-t":
            options.testmode = 1
        elif o == "-w":
            options.wait = 1

    # try to create a logger for uncaught toplevel errors.
    # if that fails, try to extract admin and SMTP info from the config file in
    # a different way, and send it.
    try:
        log = errorhandling_special.toplevel_logger(options.configfile,
              "Watchdog", "Watchdog warning")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(options.configfile, "Watchdog")
        sys.exit(1)

    check_python_version(log)

    # XXX we could probably abstract most of this away if all programs had the
    # same interface:

    mutex_name = watchdog_mutex_name()
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        # only once instance of this program is allowed to run at a time
        with mutex.MutexLock(dbado, mutex_name):
            dog = Watchdog(options, dbado=dbado)
            windows_tools.setconsoletitle("watchdog.py")
            dog.run()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

