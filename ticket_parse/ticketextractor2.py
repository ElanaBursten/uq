# ticketextractor2.py
# Created: 2002.10.04 HN

import os
import re
import sys
import string
#
import datadir
import date
import ticketextractor

re_dirname = re.compile("\d\d-XX-\d\d")
re_filename = re.compile("\d\d-\d\d-\d\d\.txt", re.IGNORECASE)

class TicketExtractor2(ticketextractor.TicketExtractor):

    config_file = "te2_config.xml"
    positions_file = "positions2.txt"
    ids_file = "ids2.txt"
    prefix = "NCA1" # may be overridden by config file

    def is_valid_dirname(self, dirname):
        _, dirname = os.path.split(dirname)
        m = re_dirname.match(dirname)
        return not not m

    def is_valid_filename(self, filename):
        _, filename = os.path.split(filename)
        m = re_filename.match(filename)
        return not not m

    def get_filename_for_date(self, adate):
        d = date.Date(adate)
        return "%02d-%02d-%02d.txt" % (d.month, d.day, d.year - 2000)

    def get_dirname_for_date(self, adate):
        d = date.Date(adate)
        return "%02d-XX-%02d" % (d.month, d.year - 2000)

    def get_filenames(self, adate):
        # we don't need file templates for this, just a base directory that
        # tells us where to look
        # update_dir will do

        filenames = []

        # we're looking for file MM-DD-YY.txt in directory MM-XX-YY
        todays_filename = self.get_filename_for_date(adate)
        todays_dirname = self.get_dirname_for_date(adate)

        names = os.listdir(self.update_dir)
        dirnames = [os.path.join(self.update_dir, name) for name in names]
        dirnames = [d for d in dirnames if os.path.isdir(d)]
        for dir in dirnames:
            for name in os.listdir(dir):
                if name.lower() == todays_dirname.lower():
                    subdir = os.path.join(dir, name)
                    files = os.listdir(subdir)
                    for name2 in files:
                        if name2.lower() == todays_filename.lower():
                            full_filename = os.path.join(subdir, name2)
                            filenames.append(full_filename)

        # make filenames lowercase; an uniform "spelling" is desirable, because
        # they will be used as keys in the positions file
        return [f.lower() for f in filenames]

    def _get_tickets(self, filename, position):
        tickets = []
        try:
            f = open(filename, "rb")
        except IOError:
            #traceback.print_exc()
            self.log.log_event("File " + filename + " cannot be read.")
            if self.verbose:
                print >> sys.stderr, "File", filename, "cannot be read."
            return (0, [])
        f.seek(position)
        data = f.read() # read all data after position
        f.close()

        if len(data) <= 1:
            return (0, [])

        pos = 0
        while 1:
            idx1 = string.find(data, self.begin_marker, pos)
            if idx1 > -1:
                ticket = data[pos:idx1].lstrip()
                if ticket.startswith(self.begin_marker):
                    ticket = ticket[len(self.begin_marker):]
                self._log(ticket)
                tickets.append(ticket)
                pos = idx1+1
                continue
            break

        return (pos, tickets)

    def filter_tickets(self, tickets):
        """ Used to filter out unwanted tickets in the superclass, but we don't
            need to do that here. The <tickets> list is left unchanged. """
        pass


if __name__ == "__main__":

    ticketextractor.main(TicketExtractor2, begin=chr(12), end=None)

