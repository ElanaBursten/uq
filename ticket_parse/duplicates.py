# duplicates.py
# Finding duplicate tickets.
# Created: 2002.11.05 HN

import date
import locate_status
import ticket as ticketmod
import ticketparser
import tools

class DuplicateFinderResult(tools.Struct):
    __fields__ = ["dups", "latest", "latest_id", "newlocates", "reassign",
                  "parent_ticket_id"]
    def get_latest_dict(self):
        """ Return a dictionary with info about the latest duplicate found.
            Only works if DuplicateFinderResult has been populated with
            correct data. """
        for d in self.dups:
            if d['ticket']['ticket_id'] == self.latest_id:
                return d
        raise ValueError, "No entry found for latest_id %s" % (self.latest_id,)


class DuplicateFinder:

    def __init__(self, tdb):
        self.tdb = tdb

    #
    # queries for finding duplicates in the database

    def find_duplicate_tickets(self, ticket):
        d = date.Date(ticket.call_date)
        sql = "exec find_duplicates_2 '%s', '%s', '%s'" % (
              ticket.ticket_number, ticket.ticket_format, d.year)

        return self._get_tickets(sql)

    def find_duplicate_tickets_renotif(self, ticket):
        year = date.Date(ticket.call_date).year
        sql = "exec find_duplicates_renotif_2 '%s', '%s', '%s', '%s'" % (
              ticket.ticket_number, ticket.ticket_format, ticket.transmit_date,
              year)

        return self._get_tickets(sql)

    def _get_tickets(self, sql):
        return self.tdb.get_tickets_for_dups_2(sql)

    #
    # selecting duplicates

    def _select_duplicate(self, ticketinfo):
        """ Select a ticket from a list with duplicate ticket info. This is a
            list returned by _find_duplicate_tickets(), containing a dict
            {ticket_id, ticket, locates}.
        """
        assert len(ticketinfo) > 0
        if len(ticketinfo) == 1:
            return ticketinfo[0]    # only one record, only one choice

        selected = -1
        # do we have any tickets where ticket.kind != "DONE"? Pick the latest...
        for i in range(len(ticketinfo)):
            ticket = ticketinfo[i]["ticket"]
            if ticket.kind != "DONE":
                selected = i
        if selected >= 0:
            return ticketinfo[selected]

        # nothing found? return the latest, newest ticket
        return ticketinfo[-1]

    def _select_oldest_duplicate(self, ticketinfo):
        # as _select_duplicate, but selects the oldest ticket. for now, we
        # assume that this is the ticket with the lowest id.
        assert len(ticketinfo) > 0
        if len(ticketinfo) == 1:
            return ticketinfo[0]

        # TODO: this would be a good place to filter out any tickets where
        # parent_ticket_id is set...

        selected = -1
        curr_id = -1
        for i in range(len(ticketinfo)):
            ticket_id = int(ticketinfo[i]["ticket_id"])
            if curr_id == -1:
                curr_id = ticket_id
                selected = i
            else:
                if curr_id > ticket_id:
                    curr_id = ticket_id
                    selected = i

        if selected >= 0:
            return ticketinfo[selected]
        # nothing found?
        return ticketinfo[0]

    ### from main.py

    # NOTE: this is the main method that will be called
    def getduplicates(self, tick, is_no_show=0, is_renotification=0):
        assert isinstance(tick, ticketmod.Ticket)
        parent_ticket_id = None
        dups, latest, latest_id, newlocates, reassign = [], None, None, (), {}

        dups = self.find_duplicate_tickets(tick)

        if dups:
            latest, latest_id, newlocates, reassign = \
             self._get_dups_info(dups, tick)
            # if we're doing a renotification ticket, check if the original
            # ticket is closed... if so, use a special way to find
            # duplicate tickets, which overrides the old one. if not, use
            # the normal way.
            # as for now, almost all tickets are treated as such, even if
            # they're not renotifications... an exception are no show
            # tickets.
            if (1 or is_renotification) and not is_no_show:
                if self.tdb.isclosed(latest_id):
                    r_dups = self.find_duplicate_tickets_renotif(tick)
                    if r_dups:
                        # implies an update
                        latest, latest_id, newlocates, reassign = \
                         self._get_dups_info(r_dups, tick)
                    else:
                        # dups but no r_dups; this renotification ticket
                        # will be inserted; therefore, set parent_ticket_id
                        oldest = self._select_oldest_duplicate(dups)
                        parent_ticket_id = oldest["ticket_id"]
                    dups = r_dups

        return DuplicateFinderResult(dups=dups, latest=latest,
               latest_id=latest_id, newlocates=newlocates, reassign=reassign,
               parent_ticket_id=parent_ticket_id)

    def _get_dups_info(self, dups, tick):
        """ Extract information from a list of duplicate Tickets. """
        latest = self._select_duplicate(dups)
        latest_id = latest["ticket_id"]
        # and find any locates that are not posted yet
        newlocates = self._find_new_locates(tick, latest)
        reassign = self._find_reassignable_locates(latest)
        return latest, latest_id, newlocates, reassign

    def _find_new_locates(self, ticket, dup_ticket_info):
        """ Find any locates that are in ticket but not in dup_ticket_info. """
        newlocates = []
        for loc in ticket.locates:
            # Note: ticket.locates is a list of Locate instances.
            # dup_ticket_info["locates"] is a dict {id: client_code}.
            clients = [c["client_code"]
                       for c in dup_ticket_info["locates"].values()]
            if not loc.client_code in clients:
                newlocates.append(loc)
        return newlocates

    def _find_reassignable_locates(self, ticketinfo):
        """ Return a dict of locates that currently have status -N (not
            a customer) and are eligible for changing to status -P (unassigned).
            'ticketinfo' is a dict as returned by _ticketnode_to_ticket.
        """
        locdict = ticketinfo["locates"]
        d = {}
        for locate_id, data in locdict.items():
            if data["status"] == locate_status.not_a_customer:
                d[locate_id] = data
        return d

    def merge_duplicate(self, new_ticket, original_ticket):
        """ Compare an original and a new ticket (duplicate). Add the locates
            that are in the new, but not in the original one, to the original
            ticket.
            The goal is to have a ticket that has _all_ the locates, both on
            the new and on the old ticket. (We can then use this merged ticket
            to find existing locators, etc.)
        """
        client_codes = [loc.client_code for loc in original_ticket.locates]

        # first, remove all locates from the new ticket that are in the old
        # ticket as well
        for i in range(len(new_ticket.locates)-1,-1,-1):
            if new_ticket.locates[i].client_code in client_codes:
                del new_ticket.locates[i]

        # then, add all these again (they will now have a locate_id and can be
        # recognized as updates)
        for loc in original_ticket.locates:
            assert loc.locate_id    # cannot be None
            new_ticket.locates.append(loc)

