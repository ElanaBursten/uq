# listener.py
# "Listens" for certain "signals".  Currently, this is implemented with
# files (presence and size), but different implementations could be added in
# the future.

import os

QUIT_FILENAME = "__uq_quit__.txt"

class Listener:

    def __init__(self):
        self.signals = {}

    def register(self, name, filename, size):
        self.signals[name] = (filename, size)

    def listen(self):
        """ Listen for signals.  Return a list of signal names that were
            found. """
        found = []
        for name, (filename, size) in self.signals.items():
            if self.signal_found(filename, size):
                found.append(name)
        return found

    def signal_found(self, filename, size):
        try:
            thesize = os.path.getsize(filename)
        except OSError:
            return False
        else:
            return thesize == size

uqlistener = Listener()
uqlistener.register('quit', QUIT_FILENAME, 1)


if __name__ == "__main__":

    found = uqlistener.listen()
    print "Found these signals:", found

