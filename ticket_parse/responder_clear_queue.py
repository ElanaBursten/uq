# responder_clear_queue.py
# Records from call centers that are not in a list are removed from the queue.
# Created: 2002.06.26 HN

import string
import sys
#
import config
import datadir
import dbinterface_ado
import errorhandling2
import errorhandling_special
import mutex
import tools

PROCESS_NAME = 'ClearResponderQueue'

class ClearResponderQueue:

    def __init__(self, verbose=1, configfile="", dbado=None):

        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.cfg = config.Configuration(configfile)
            config.setConfiguration(self.cfg)
        else:
            self.cfg = config.getConfiguration()
        
        if dbado:
            self.db = dbado
        else:
            self.db = dbinterface_ado.DBInterfaceADO()
        
        # create and configure the logger
        self.log = errorhandling2.ErrorHandler(logdir=self.cfg.logdir,
                   smtpinfo=self.cfg.smtp_accs[0], me="ResponderClearQueue",
                   admin_emails=self.cfg.admins,
                   subject="ResponderClearQueue Warning") 
                   # doesn't log per call center
        self.log.defer_mail = 1
        self.verbose = verbose

    def _delete_from_queue(self, locate_id):
        self.db.runsql("delete from responder_queue where locate_id = %s" % (
         locate_id,))
         
    def delete_from_queue(self, row):
        locate_id = row["locate_id"]
        self.log.log_event("Removing locate (%s, %s) from queue: %s" % (
                      row["ticket_format"], row['client_code'], locate_id),
                      dump=self.verbose)
        self._delete_from_queue(locate_id)
    
    def clear_queue(self):
        deleted_rows = []
        skipped_rows = []
        
        self.log.log_event("Getting records...", dump=self.verbose)
        results = self.db.runsql_result("exec get_pending_responses ''")
        self.log.log_event("OK (%d records found)" % (len(results),),
         dump=self.verbose)
    
        skipped = 0
        count = 0
                
        for row in results:
            cc = row['ticket_format']
            term_id = row['client_code']
            
            if cc in self.cfg.nodelete:
            
                is_xcel_responder = self.cfg.xcelresponders.has_key(cc)
                is_xcel_termid = (is_xcel_responder 
                  and term_id in self.cfg.xcelresponders[cc]['termids'])
                
                # if this is a client-based call center, do delete those terms
                # that we don't reply to (they won't be handled by <skip>)
                is_irth_responder = self.cfg.responders.has_key(cc)
                if is_irth_responder:
                    irth_client_based = self.cfg.responders[cc]['clientbased']
                    if irth_client_based:
                        irth_term_ids = [d['login'] for d in self.cfg.responders[cc].get('logins', [])]
                        is_irth_termid = term_id in irth_term_ids
                    else:
                        is_irth_termid = 1
                else:
                    is_irth_termid = 1
            
                if is_xcel_responder and not is_xcel_termid:
                    pass # do delete
                elif is_irth_responder and not is_irth_termid:
                    pass # do delete
                else:
                    # don't delete
                    locate_id = row['locate_id']
                    self.log.log_event("Skipping locate: %s, id %s, term %s" % 
                      (cc, locate_id, term_id), dump=self.verbose)
                    skipped_rows.append(row)
                    skipped += 1
                    continue
            
            # default: delete
            self.delete_from_queue(row)
            deleted_rows.append(row)
            count += 1

        # delete locates that have added_by != 'PARSER'
        sql = """
          select rq.* from responder_queue rq
          inner join locate l on rq.locate_id = l.locate_id
          where l.added_by <> 'PARSER'
             or l.added_by is null
        """
        rows = self.db.runsql_result(sql)
        for row in rows:
            self.delete_from_queue(row)
            deleted_rows.append(row)
    
        self.log.log_event("Clear Queue complete:", dump=self.verbose)
        self.log.log_event("%d records deleted (%d were added by the parser), "\
         "%d records skipped (because they will be responded to)" %
         (len(deleted_rows), count, skipped), dump=self.verbose)
         
        return deleted_rows, skipped_rows


if __name__ == "__main__":
    configfile = ""

    try:
        log = errorhandling_special.toplevel_logger(configfile, 
              PROCESS_NAME, PROCESS_NAME + " Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, PROCESS_NAME)
        sys.exit(1)

    mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
    try:
        dbado = dbinterface_ado.DBInterfaceADO() 
        with mutex.MutexLock(dbado, mutex_name):
            crc = ClearResponderQueue(configfile=configfile, dbado=dbado)
            crc.clear_queue()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)
