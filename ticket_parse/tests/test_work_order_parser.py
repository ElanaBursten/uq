# test_work_order_parser.py

import os
import site; site.addsitedir('.')
import unittest
#
from formats import Lambert
import _testprep
import ticketloader
import work_order_parser

class TestWorkOrderParser(unittest.TestCase):

    def test_Lambert(self):
        """ Test Lambert XML work orders. """
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00009-A01.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)

        # parsed directly from XML
        ae = self.assertEquals
        ae(wo.client_wo_number, 'V0002850')
        ae(wo.kind, 'NORMAL')
        ae(wo.job_number, 'E7018430')
        ae(wo.client_order_num, 'BMD442O00')
        ae(wo.due_date, '2011-03-07 23:00:00')
        ae(wo.client_master_order_num, 'MD00091749136')
        ae(wo.transmit_date, '2011-02-28 19:27:00')
        ae(wo.caller_name, 'A WEBER')
        ae(wo.work_city, 'GAITHERSBURG')
        ae(wo.work_state, 'MD')
        ae(wo.work_zip, '20878')
        ae(wo.caller_phone, '(301) 975-2377')
        ae(wo.caller_altphone, '(A03) 892-6460')
        ae(wo.wire_center, '301251')
        ae(wo.work_center, '22')
        ae(wo.central_office, '1222')
        ae(wo.serving_terminal, 'HH F 21 TURNHAM LN')
        ae(wo.map_page, '5163')
        ae(wo.map_ref, 'D3')
        ae(wo.circuit_number, '11/BURY/442O00/ /VZM') # cut off
        ae(wo.f2_cable, 'H4091')
        ae(wo.terminal_port, '2')
        ae(wo.f2_pair, '266')

        # parsed after some manipulation
        ae(wo.wo_number, 'LAMV0002850')
        ae(wo.wo_source, 'LAM01')
        ae(wo.work_address_number, '25')
        ae(wo.work_address_street, 'TURNHAM LN')
        ae(str(wo.work_lat)[:9], '39.106959')
        ae(str(wo.work_long)[:10], '-77.231314')

    def test_Lambert_county(self):
        # <CUSTOMERCOUNTY> XML field was added later
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-23-00001-A01.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)

        self.assertEquals(wo.client_wo_number, 'V0002683')
        self.assertEquals(wo.work_county, 'HARFORD')

    def test_Lambert_copper(self):
        # Mantis #2778: "Lambert starting sending us tickets for copper drops
        # in addition to the fiber drops. The copper drops do not have the
        # <MON> </MON> or <CO> </CO> tags populated.

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-04-19-00214-A01.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)

        self.assertEquals(wo.client_wo_number, 'V0010937')
        self.assertEquals(wo.client_master_order_num, None)
        self.assertEquals(wo.central_office, None)

    def test_transmit_date(self):
        # Mantis #2786: The <ISSUEDATE> apparently isn't the same as a
        # transmit date, so the rules were changed: <ISSUEDATE> is now stored
        # in call_date, while a new field <TRANSMITDATE> was added.
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01_V0013484_20110505_1452.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)

        self.assertEquals(wo.client_wo_number, 'V0013484')
        self.assertEquals(wo.transmit_date, "2011-05-05 14:52:12")
        self.assertEquals(wo.call_date, "2011-05-03 00:00:00")
        self.assertEquals(wo.source_sent_attachment, 'Y') # Mantis #2820

        # Mantis #2789: added <DEFAULTEXTENTOFWORK> and <DROPTYPE>
        self.assertEquals(wo.work_description,
          "LOCATE FROM PROPERTY TO HAND HOLE/ST AS WHITE LINED.")
        self.assertEquals(wo.work_type, "BSW-COPPER")


    #
    # bulk testing

    WORK_ORDERS = [
        ("Lambert", "LAM01", Lambert.WorkOrderParser),
    ]

    def test_bulk(self):
        for wo_format, dirname, parser_class in self.WORK_ORDERS:
            path = os.path.join(_testprep.TICKET_PATH, "work-orders", dirname)
            files = [fn for fn in os.listdir(path) if fn.endswith(".txt")]
            wop = parser_class()
            for fn in files:
                tl = ticketloader.TicketLoader(os.path.join(path, fn))
                raw = tl.tickets[0]
                try:
                    wo = wop.parse(raw)
                except:
                    import traceback; traceback.print_exc()
                    self.fail("Could not parse: %r" % fn)



if __name__ == "__main__":
    unittest.main()

