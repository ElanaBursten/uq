# test_handle_damage.py

import site; site.addsitedir('.')
import unittest
#
import handle_damage
import ticket_db
import _testprep

DHO = handle_damage.DamageHandlerOptions

class TestHandleDamage(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.tdb.deleterecords('damage_due_date_rule')

    def test_001(self):
        """ Test if damage records get processed by the handler. """
        dh = handle_damage.DamageHandler(DHO(verbose=0))

        # clear damage table
        dh.tdb.deleterecords("damage")

        # grab a profit center, any profit center
        [row] = dh.tdb.runsql_result("select top 1 * from profit_center")
        pc = row['pc_code']

        # insert a damage record without a due date
        sql = """
         insert damage
          (damage_type, damage_date, uq_notified_date, profit_center)
         values
          ('CLOSED', '2002-01-01 00:00:00', '2002-08-05T14:15:16', '%s')
        """ % pc
        dh.tdb.runsql(sql)

        # take a sneak peek
        rows = dh.tdb.get_damage_without_due_date()
        self.assertEquals(len(rows), 1)

        sql = "select * from damage where due_date is null"
        rows = dh.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 1)

        dh.run()

        sql = "select * from damage where due_date is not null"
        rows = dh.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["due_date"], "2002-08-12 14:15:16")
        # +5 business days; skip a weekend

    def insert_damage(self, damage_date, uq_notified_date, profit_center,
                      company, damage_type='CLOSED'):
        sql = """
         insert damage
          (damage_type, damage_date, uq_notified_date, profit_center,
           utility_co_damaged)
         values
          ('CLOSED', '%s', '%s', '%s', '%s')
        """ % (damage_date, uq_notified_date, profit_center, company)
        self.tdb.runsql(sql)

    def test_rules(self, use_wildcards=False):
        """ Test damage due date rules. """

        # add custom rules
        _testprep.add_damage_due_date_rule(self.tdb, '000', 'UtiliQuest',
         'default', days=5)
        _testprep.add_damage_due_date_rule(self.tdb, '001', 'UtiliQuest',
         'second', days=4)
        if use_wildcards:
            _testprep.add_damage_due_date_rule(self.tdb, '*', 'UtiliQuest',
             'wildcard', days=6)

        rules = self.tdb.getrecords('damage_due_date_rule')
        self.assertEquals(len(rules), 3 if use_wildcards else 2)

        dh = handle_damage.DamageHandler(DHO(verbose=0))

        # clear damage table
        dh.tdb.deleterecords("damage")

        # insert damage records without a due date
        self.insert_damage('2012-01-30 00:00:00', '2012-01-30 12:13:14',
                           '002', 'UtiliQuest') # no rule
        self.insert_damage('2012-01-30 00:00:00', '2012-01-30 14:15:16',
                           '001', 'UtiliQuest') # rule: +4d

        # take a sneak peek
        rows = dh.tdb.get_damage_without_due_date()
        self.assertEquals(len(rows), 2)

        sql = "select * from damage where due_date is null"
        rows = dh.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 2)

        # process damages
        dh.run()

        # check calculated due dates
        rows = self.tdb.getrecords('damage')
        for row in rows:
            if row['profit_center'] == '001':
                self.assertEquals(row['due_date'], '2012-02-03 14:15:16')
            elif row['profit_center'] == '002':
                if use_wildcards:
                    # +6 days
                    self.assertEquals(row['due_date'], '2012-02-07 12:13:14')
                else:
                    # +5 days
                    self.assertEquals(row['due_date'], '2012-02-06 12:13:14')

    def test_rules_with_wildcard(self):
        self.test_rules(use_wildcards=True)



if __name__ == "__main__":

    unittest.main()
