# test_main_1422.py

from __future__ import with_statement
from StringIO import StringIO
import copy
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
import xml.etree.ElementTree as ET
#
import call_centers
import config
import et_tools
import main
import ticket_db
import ticketloader
import ticketparser
import _testprep
from mockery import *
from mockery_tools import *

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs

class Test1422(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

        self.handler = ticketparser.TicketHandler()

        # remove any preexisting clients that have 1422->SCA1
        self.tdb.deleterecords('client', call_center='SCA1',
                               update_call_center='1422')

        # make sure 1422 call center exists
        #sql = "select * from call_center where cc_code = '1422'"
        #rows = self.tdb.runsql_result(sql)
        #if not rows:
        #    _testprep.add_test_call_center(self.tdb, '1422')

        # add SDG clients
        #sql = "select * from client where oc_code like 'SDG%'"
        #rows = self.tdb.runsql_result(sql)
        #self.client_orig = {}
        #for row in rows:
        #    self.client_orig[row['client_id']] = row['update_call_center']

        #_testprep.set_call_center_active(self.tdb, '1421')
        #_testprep.set_call_center_active(self.tdb, '1422')

        # make sure we have SCANA01[EGP] for 1421<-1422
        for suffix in 'EGP':
            self.tdb.delete('client', call_center='1421',
            update_call_center='1422', oc_code='SCANA01'+suffix)
        _testprep.add_test_client(self.tdb, 'SCANA01E', 1421, active=1,
         update_call_center=1422)
        _testprep.add_test_client(self.tdb, 'SCANA01G', 1421, active=1,
         update_call_center=1422)
        _testprep.add_test_client(self.tdb, 'SCANA01P', 1421, active=1,
         update_call_center=1422)
        #for row in rows:
        #    self.tdb.updaterecord('client', 'client_id', row['client_id'],
        #     update_call_center='1422')

        # reload call centers so 1422 is an update call center for 1421
        call_centers.get_call_centers(self.tdb).reload()

    def prepare(self):
        if not self.cc_copy.update_call_center('1422'):
            self.cc_copy.ucc['1422'] = '1421'

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_1422',
          'format': '1422',
          'processed': 'test_processed_1422',
          'error': 'test_error_1422',
          'attachments': ''},
         {'incoming': 'test_incoming_1421',
          'format': '1421',
          'processed': 'test_processed_1421',
          'error': 'test_error_1421',
          'attachments': ''}
        ]
        import config; cfg = config.getConfiguration()
        cfg.processes = self.conf_copy.processes
        _testprep.set_call_center_active(self.tdb, '1421')
        _testprep.set_call_center_active(self.tdb, '1422')
        self.cc_copy.reload()

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        # Restore the update_call_center to what it was before
        #sql = """
        # update client
        # set update_call_center = '%s'
        # where client_id = %s
        #"""
        #for client_id, update_call_center in self.client_orig.iteritems():
        #    self.tdb.updaterecord('client', 'client_id', int(client_id),
        #     update_call_center=update_call_center)

    @with_config_copy
    @with_call_center_copy
    def test_1422_1(self):
        '''
        Verify that the ticket_format is changed
        '''

        self.prepare()
        import call_centers; cc = call_centers.get_call_centers(self.tdb)
        cc.reload()
        #print ">>", cc.ucc

        with open(os.path.join("..", "testdata", "sample_tickets", "1422",
             "1422-2009-04-14-00001.txt")) as f:
            raw = f.read()
            raw = raw.replace('SCEGT01', "SCANA01")

        drop("1422.txt", raw, dir='test_incoming_1422')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='1422')
        m.log.lock = 1
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.ticket_format, '1421')

    @with_config_copy
    @with_call_center_copy
    def test_1422_2(self):
        '''
        Test summary ticket
        '''
        self.prepare()

        with open(os.path.join("..", "testdata", "sample_tickets", "1422",
             "SummaryAudit-1422.txt")) as f:
            raw = f.read()
            raw = raw.replace('SCEGT01',"SCANA01")

        drop("1422_summary.txt", raw, dir='test_incoming_1422')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='1422')
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the summary header id of the ticket we just posted?
        sh_id, sd_ids = m.ids[-1]

        self.assertTrue(sh_id > 0)
        self.assertEquals(len(sd_ids), 50)

    @with_config_copy
    @with_call_center_copy
    def test_1422_3(self):
        '''
        Test AT&T summary ticket
        '''
        self.prepare()
        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "FGV1-2009-07-07-00002.txt"))
        raw = f.read()
        f.close()
        drop("1422_summary_1.txt", raw, dir = 'test_incoming_1422')

        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "FGV1-2009-07-09-00001.txt"))
        raw = f.read()
        f.close()
        drop("1422_summary_2.txt", raw, dir='test_incoming_1422')

        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "FGV1-2009-07-14-00002.txt"))
        raw = f.read()
        f.close()
        drop("1422_summary_3.txt", raw, dir='test_incoming_1422')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='1422')
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)

        shs = self.tdb.getrecords('summary_header')
        self.assertEquals(len(shs), 3)

    @with_config_copy
    @with_call_center_copy
    def test_1422_to_responder(self):
        '''
        Verify that the facility type is set correctly in the response
        '''
        self.prepare()
        # Gas ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "1422-2009-08-25-00043.txt"))
        raw = f.read()
        raw = raw.replace('SCGZ05',"SCANA01G")
        f.close()

        drop("1422_1.txt", raw, dir='test_incoming_1422')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='1422')
        m.log.lock = 1
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        ticket_id = m.ids[-1]

        # Get the ticket
        t1 = self.tdb.getticket(ticket_id)
        self.assertEquals(t1.ticket_format, '1421')
        # Just one locate
        self.assertEquals(len(t1.locates),1)

        # Electric ticket, same ticket number
        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "1422-2009-08-25-00044.txt"))
        raw = f.read()
        raw = raw.replace('SCEJZ40', "SCANA01E")
        f.close()

        drop("1422_2.txt", raw, dir='test_incoming_1422')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='1422')
        m.log.lock = 1
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        ticket_id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(ticket_id)
        self.assertEquals(t2.ticket_format, '1421')
         # Two locates now
        self.assertEquals(len(t2.locates),2)

        # Verify the version
        versions = self.tdb.get_ticket_versions(ticket_id)
        self.assertEquals(len(versions),2)

        results = self.tdb.getrecords("locate")

        import ticketrouter
        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        results = self.tdb.getrecords("locate")
        # Inspect locates
        self.assertTrue(reduce(lambda accum, stat: accum and stat == '-R',
                        [c['status'] for c in results]))

        # Set the utility type
        rec = self.tdb.getrecords('client', oc_code = 'SCANA01G')[0]
        self.tdb.updaterecord('client', 'client_id', int(rec['client_id']),
         utility_type='ugas')
        rec = self.tdb.getrecords('client', oc_code = 'SCANA01E')[0]
        self.tdb.updaterecord('client', 'client_id', int(rec['client_id']),
         utility_type='elec')

        # Run the responder
        cfg = config.getConfiguration()
        call_center = '1421'
        z = {'ack_dir':'',
             'ack_proc_dir':'',
             'mappings':{'M':('1', ''),'N':('2', '')},
             'clients':['SCANA01E', 'SCANA01G'],
             'all_versions':0,
             'id':'spam',
             'initials':'UQ',
             'name':'1421',
             'post_url':'https://www.irth.com/irthnet/webservices/irthnetservice.asmx',
             'resp_url':'http://tempuri.org/AddContractLocatorResponses',
             'send_3hour':1,
             'send_emergencies':1,
             'skip':[],
             'status_code_skip':[],
             'translations':{}}

        cfg.xmlhttpresponders[call_center] = z
        cfg.responderconfigdata.add(z['name'], 'xmlhttp', z)
        self.assert_(cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml" % call_center
        # change the status of the  locate so we get an entry in the
        # locate_status table (done by locate trigger)
        import date
        locate_close_date = date.Date().isodate()
        test_emp_id = _testprep.add_test_employee(self.tdb, "Test Employee")
        for loc in t2.locates:
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
                 closed_date = '%s'
             where locate_id = %s
            """ % (test_emp_id, locate_close_date, loc.locate_id,)
            self.tdb.runsql(sql)
        self.assertEquals(t2.locates[0].client_code, 'SCANA01G')
        self.assertEquals(t2.locates[1].client_code, 'SCANA01E')
        response_code = 'X'
        explanation = 'boo'

        import xmlhttpresponder_data as xhrd
        from test_xmlhttpresponder import create_mock_responder

        xr = create_mock_responder([], verbose=0)
        xr.log.logger.logfile = StringIO()
        xr.responderdata = xhrd.getresponderdata(cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 0)
        xr.log.lock = 1
        xr.respond_all()
        log = xr.log.logger.logfile.getvalue()
        xr.log.logger.logfile.close()
        self.assertTrue('Responding for: 1421' in log)
        self.assertTrue('Sending Response: 0908251056, SCANA01G, 1, ,' in log)
        self.assertTrue('closed date injected into xml:' in log)
        self.assertTrue('Response: response posted' in log)
        self.assertTrue(
         'Removing locate from queue: %s' % (t2.locates[0].locate_id,) in log)
        self.assertTrue(
         'Sending Response: 0908251056, SCANA01E, 1, ,' in log)
        self.assertTrue(
         'Removing locate from queue: %s' % (t2.locates[1].locate_id,) in log)
        self.assertTrue('2 responses sent' in log)

        self.assertEquals(len(xr.sent), 2)
        self.assertEquals(len(xr.xml_sent), 2)
        self.assertEquals(xr.sent[0],
         [t2.ticket_number, t2.locates[0].client_code, '1', '', ''])
        self.assertEquals(xr.sent[1],
         [t2.ticket_number, t2.locates[1].client_code, '1', '', ''])

        # Verify that <Locate_DateTime> is the locate_close_date set above
        doc = ET.fromstring(xr.xml_sent[0])
        data = et_tools.find(doc, 'Facility_Type').text
        self.assertEquals(str(data), 'gas distribution')

        data = et_tools.find(doc, 'CDC').text
        self.assertEquals(str(data), 'SCANA01G')

        doc = ET.fromstring(xr.xml_sent[1])
        data = et_tools.find(doc, 'Facility_Type').text
        self.assertEquals(str(data), 'electric primary')

        data = et_tools.find(doc, 'CDC').text
        self.assertEquals(str(data), 'SCANA01E')

if __name__ == "__main__":
    unittest.main()

