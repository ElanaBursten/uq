# test_responder.py

import os
import unittest
import site; site.addsitedir('.')
#
import IRTHresponder
import ticket_db
import ticketloader
import ticketparser
import _testprep

class TestResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_get_responses(self):
        """ Retrieval of responses """
        self.create_responses()
        r = IRTHresponder.IRTHresponder(verbose=0)
        rows = r.get_pending_responses("Atlanta")
        self.assertEquals(len(rows), 13)

    def create_responses(self):
        # grab a ticket and post it
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "Atlanta-1.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t, whodunit='parser')

        # create response records for every locate
        rows = self.tdb.getrecords("locate", ticket_id=id)
        self.assert_(rows)
        for row in rows:
            self.tdb.insert_responder_queue(row['locate_id'])

        responses = self.tdb.getrecords("responder_queue")
        self.assert_(responses)

    def test_get_responses_manual_tickets_1(self):
        self.create_responses()
        self.tdb.runsql("""
            update ticket
            set ticket_number = 'MAN-123-456'
        """)
        r = IRTHresponder.IRTHresponder(verbose=0)
        rows = r.get_pending_responses("Atlanta")
        self.assertEquals(len(rows), 0,
          "manual tickets should be ignored by responder")

    def test_get_responses_manual_tickets_2(self):
        self.create_responses()
        self.tdb.runsql("""
            update locate
            set added_by = 'user'
        """)
        r = IRTHresponder.IRTHresponder(verbose=0)
        rows = r.get_pending_responses("Atlanta")
        import pprint; pprint.pprint(rows)
        self.assertEquals(len(rows), 0,
          "tickets not added by parser should be ignored by responder")

    def test_log_response(self):
        self.tdb.deleterecords("response_log")
        self.create_responses()
        r = IRTHresponder.IRTHresponder(verbose=0)
        row = {'locate_id': 18000, 'ticket_format': 'Atlanta',
               'status': 'C'}
        resp = ('foo', 'bar', '200')
        r.log_response(row, resp)

        rows = self.tdb.getrecords("response_log")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['call_center'], 'Atlanta')
        self.assertEquals(rows[0]['locate_id'], '18000')


if __name__ == "__main__":

    unittest.main()
