# test_new_routing.py
# Created: 2002.09.16
# XXX probably obsolete

import httplib
import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import assignment
import businesslogic
import config
import dbinterface_old as dbinterface
import duplicates
import errorhandling2
import locate
import locate_status
import main
import router
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
import tools

class __TestNewRouting(unittest.TestCase):

    # XXX WRONG: do this in setUp()
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        # get the same configuration object as main.py uses
        # XXX why? fishy
        self.config = config.getConfiguration()
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         # and one for FCL2 specifically
         {'incoming': 'test_incoming_FCL2',
          'format': 'FCL2',
          'processed': 'test_processed_FCL2',
          'error': 'test_error_FCL2',
          'attachments': ''},
        ]

    def test_000_preparation(self):
        self.config.processes = [
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         {'incoming': 'test_incoming_FCL2',
          'format': 'FCL2',
          'processed': 'test_processed_FCL2',
          'error': 'test_error_FCL2',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'Atlanta',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         ]

        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()

    def send(self,xml):
        self.xml = xml

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        _testprep.clear_database(self.tdb)
        for process in self.config.processes:
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center "\
         "from client where oc_code = 'USW01'"
        self.result_USW01 = self.tdb.runsql_result(sql)[0]

        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'], "USW01")

    def tearDown(self):
        # Set it back inactive
        _testprep.set_client_inactive(self.tdb, self.result_USW01['call_center'], "USW01")

    def drop(self, filename, data):
        """ Write a file (containing one or more images) to the
            test_incoming directory. """
        fullname = os.path.join("test_incoming", filename)
        f = open(fullname, "wb")
        f.write(data)
        f.close()

    def clear_dir(self, dirname):
        """ Empty a directory. """
        filenames = os.listdir(dirname)
        for filename in filenames:
            fullname = os.path.join(dirname, filename)
            os.remove(fullname)

    ###)

    def test_001(self):

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        t.locates = [locate.Locate(s) for s in ["USW01", "D00K13"]]

        # create a Main instance so we can use its log, etc.
        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.lock = 1
        # XXX in future tests, if Main has a router instance, we can use that
        # too

        # route this ticket
        r = router.Router(m.tdb, m.log, m.holiday_list, m.clients, m.verbose)
        r.route(t)

        # the USW01 should have been routed to a locator, and it status should
        # be 'assigned'
        self.assertEquals(t.locates[0].client_code, "USW01")
        self.assert_(t.locates[0]._assignment)
        self.assert_(t.locates[0]._assignment.locator_id)
        self.assertEquals(t.locates[0].status, locate_status.assigned)

        # the other locate is not a client and should not have been assigned,
        # status should reflect this:
        self.assertEquals(t.locates[1]._assignment, None)
        self.assertEquals(t.locates[1].status,
         locate_status.not_a_customer)

    def test_prepare(self):
        # test if the prepare method does what it should do...

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        # add two locates; fake one assignment
        t.locates = [locate.Locate(s) for s in ["USW01", "D00K13"]]
        t.locates[0].status = locate_status.assigned
        t.locates[0]._assignment = assignment.Assignment("208")
        t.locates[1].status = locate_status.not_a_customer

        t.locates.extend([locate.Locate(s) for s in ["ATL01", "GN03F"]])

        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.lock = 1
        r = router.Router(m.tdb, m.log, m.holiday_list, m.clients, m.verbose)

        r.prepare(t)

        # the locate with the locator gets _existing = 1, the others are 0
        self.assertEquals(t.locates[0]._existing, 1)
        self.assertEquals(t.locates[1]._existing, 0)
        self.assertEquals(t.locates[2]._existing, 0)
        self.assertEquals(t.locates[3]._existing, 0)

        # the locate with the locator stays assigned, the others are blank
        self.assertEquals(t.locates[0].status, locate_status.assigned)
        self.assertEquals(t.locates[1].status, locate_status.blank)
        self.assertEquals(t.locates[2].status, locate_status.blank)
        self.assertEquals(t.locates[3].status, locate_status.blank)

    def test_002(self):
        try:
            call_center = 'Atlanta'
            ATL01_activated = False
            _testprep.add_test_client(self.tdb, 'ATL01', 'Atlanta')
            #results = self.tdb.getrecords("client", call_center=call_center,
            #          oc_code='ATL01', active=1)
            #if not results:
            _testprep.set_client_active(self.tdb, call_center, 'ATL01')
            ATL01_activated = True

            # get any old ticket with three locates; one's a client, others are not
            tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["Atlanta"])
            t.locates = [locate.Locate(s) for s in ["USW01", "A01", "BGAWA"]]

            # route this
            m = main.Main(verbose=0, call_center='Atlanta')
            m.log.lock = 1
            # remove ABS01 from m.clients:
            idx = tools.findfirst(m.clients["Atlanta"],
                  lambda c: c.client_code == "BGAWA")
            self.assert_(idx > -1)
            abs01 = m.clients["Atlanta"][idx]   # keep around for later
            del m.clients["Atlanta"][idx]

            r = router.Router(m.tdb, m.log, m.holiday_list, m.clients, m.verbose)
            r.route(t, whodunit="test")

            self.assertEquals(t.locates[0].status, locate_status.assigned)
            self.assertEquals(t.locates[1].status,
             locate_status.not_a_customer)
            self.assertEquals(t.locates[2].status,
             locate_status.not_a_customer)

            # post this
            id, loc_ids = t.insert(self.tdb)

            # get ticket from database, using getduplicates mechanism
            # -- or maybe getticket() works as well
            t2 = self.tdb.getticket(id) # was: getticket_2
            self.assert_(t2.locates[0].locate_id)
            self.assert_(t2.locates[0]._assignment)
            self.assert_(t2.locates[0]._assignment.locator_id)
            self.assertEquals(t2.locates[1]._assignment, None)

            # add two new locates to the ticket... one's a client, other's not,
            # and one of the existing locates suddenly becomes a client. :-)
            t2.locates.extend([locate.Locate(s) for s in ["ATL01", "A03"]])
            # now we have 5 locates: USW01 is already tagged as client; A01 is
            # not a client; ABS01 wasn't but is now; ATL01 is new and is a client
            # too; and A03 is new and isn't a client.

            # add ABS01 to clients again
            r.clients["Atlanta"].append(abs01)

            # XXX doesn't come merge into the picture somewhere?

            # prepare should leave USW01 alone, but the rest should be set to
            # 'blank' again...
            r.prepare(t2)
            self.assertEquals(t2.locates[0].status, locate_status.assigned)
            for i in range(1, 5):
                self.assertEquals(t2.locates[i].status, locate_status.blank)

            r.route(t2)
            for index, expected in (
             (0, locate_status.assigned),
             (1, locate_status.not_a_customer),
             (2, locate_status.assigned),
             (3, locate_status.assigned),
             (4, locate_status.not_a_customer),
            ):
                self.assertEquals(t2.locates[index].status, expected,
                 "%d is %s, should be %s" % (index, t2.locates[index].status,
                                             expected))

            # the first three ("old") locates should have a locate_id, the other
            # two ("new") should not
            self.assert_(t2.locates[0].locate_id)
            self.assert_(t2.locates[1].locate_id)
            self.assert_(t2.locates[2].locate_id)
            self.assertEquals(t2.locates[3].locate_id, None)
            self.assertEquals(t2.locates[4].locate_id, None)

            # USW01 should have _existing = 1, the others should not
            self.assertEquals(t2.locates[0]._existing, 1)
            self.assertEquals(t2.locates[1]._existing, 0)
            self.assertEquals(t2.locates[2]._existing, 0)
            self.assertEquals(t2.locates[3]._existing, 0)
            self.assertEquals(t2.locates[4]._existing, 0)
            # Note: ABS01 has _existing == 0 and its locate_id set, so it should
            # be updated by updateticket_2

            # update...
            self.tdb.updateticket_2(id, t2, "test")

            # how many locates are there now?
            locates = self.tdb.getrecords("locate", ticket_id=id)
            self.assertEquals(len(locates), 5)
            assignments = self.tdb.getrecords("assignment")
            self.assertEquals(len(assignments), 3)
        finally:
            if ATL01_activated:
                _testprep.set_client_inactive(self.tdb, call_center, 'ATL01')

    def __test_003(self):
        """ Test Main's new routing features (1) """
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        self.drop("t003.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta', routing=1)
        m.log.lock = 1
        m.run(runonce=1)

        # there should only be one ticket
        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), 1)
        ticket_id = tickets[0]["ticket_id"]

        # check locates...
        # apparently, only USW01 is a client, so it should be assigned
        # all other locates should be -N (not a customer)
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 13)

        a_locates = [loc for loc in locates
                     if loc["status"] == locate_status.assigned]
        self.assertEquals(len(a_locates), 1)
        n_locates = [loc for loc in locates
                     if loc["status"] == locate_status.not_a_customer]
        self.assertEquals(len(n_locates), 12)

        for loc in a_locates:
            locate_id = loc["locate_id"]
            assignments = self.tdb.getrecords("assignment",
                          locate_id=locate_id)
            self.assert_(assignments,
             "No assignment found for locate %s" % (loc,))
            self.assertEquals(assignments[0]["added_by"], "parser")

#)


if __name__ == "__main__":

    unittest.main()

