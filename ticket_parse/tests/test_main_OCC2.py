# test_main_OCC2.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import StringIO
import unittest

import main
import ticket_db
import config
import ticketloader
import ticketparser
import ticketrouter
import _testprep

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs
from mockery_tools import *

class TestMainOCC2(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

        rows = self.tdb.getrecords('call_center', cc_code='OCC2')
        if not rows:
            _testprep.add_test_call_center(self.tdb, 'OCC2')
            self.delete_call_center = True
        else:
            self.delete_call_center = False

        rows = self.tdb.getrecords('client', call_center='OCC2')

        # make sure we have these active clients for OCC2:
        term_ids = ['VZN102', 'DOM400']
        for term_id in term_ids:
            self.tdb.deleterecords('client', oc_code=term_id,
                                   call_center='OCC2')
            _testprep.add_test_client(self.tdb, term_id, 'OCC2')

    def prepare(self):

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_OCC2',
          'format': 'OCC2',
          'processed': 'test_processed_OCC2',
          'error': 'test_error_OCC2',
          'attachments': ''}]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        if self.delete_call_center:
            _testprep.delete_test_call_center(self.tdb, 'OCC2')

    @with_config_copy
    def test_OCC2_1(self):
        self.prepare()

        # The first ticket is a normal ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-15-01094.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC2_1.txt", raw, dir='test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-15-00911.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC3_1.txt", raw, dir = 'test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t1 = self.tdb.getticket(id)
        self.assertEquals(len(t1.locates),5)
        self.assertEquals(t1.locates[0].client_code,'COX902')
        self.assertEquals(t1.locates[0].added_by,'PARSER')
        self.assertEquals(t1.ticket_format, 'OCC2')
        self.assertEquals(t1.source, 'OCC2')

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        z = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(filter(lambda x: x['status'] == '-P',z)),0)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should *not* have been called
        self.assertEquals(rows, [])

        # Close the ticket
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)

        self.assertEquals(self.tdb.isclosed(id), 1)

        # The second ticket is a 3 HR ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-22-00155.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC2_2.txt", raw, dir = 'test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-22-00141.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC3_2.txt", raw, dir = 'test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()

        self.assertTrue('Added ticket_id %s to ticket_ack' % (id,) in log)

        z = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(filter(lambda x: x['status'] == '-P',z)),0)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")
        # Ack the ticket ack
        sql = """
         update ticket_ack set has_ack = 1
         where ticket_id = %s
        """ % (m.ids[-1],)
        self.tdb.runsql(sql)

        # insert_ticket_ack should have been called
        self.assertEquals(len(rows), 1)

        # The third ticket is a 3 HR ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-23-00100.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC2_3.txt", raw, dir = 'test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-23-00079.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC3_3.txt", raw, dir = 'test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.logger.logfile = StringIO.StringIO()

        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('Added ticket_id %s to ticket_ack' % (id,) in log)
        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should have been called
        self.assertEquals(len(rows), 2)


        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()

        self.assertFalse('Added ticket_id %s to ticket_ack' % (id,) in log)

        z = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(filter(lambda x: x['status'] == '-P',z)),0)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should have been called
        self.assertEquals(len(rows), 2)

    @with_config_copy
    def test_OCC2_2(self):
        self.prepare()

        # The first ticket is a normal ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-15-01094.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC2_1.txt", raw, dir = 'test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-15-00911.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC3_1.txt", raw, dir = 'test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t1 = self.tdb.getticket(id)
        self.assertEquals(len(t1.locates),5)
        self.assertEquals(t1.locates[0].client_code,'COX902')
        self.assertEquals(t1.locates[0].added_by,'PARSER')
        self.assertEquals(t1.ticket_format, 'OCC2')
        self.assertEquals(t1.source, 'OCC2')

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        z = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(filter(lambda x: x['status'] == '-P',z)),0)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should *not* have been called
        self.assertEquals(rows, [])

        # Close the ticket
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)

        self.assertEquals(self.tdb.isclosed(id), 1)

        # The second ticket is a 3 HR ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-22-00155.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC2_2.txt", raw, dir='test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-22-00141.txt"))
        raw = string.join(f.readlines(),'')
        f.close()

        drop("OCC3_2.txt", raw, dir='test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()

        self.assertTrue('Added ticket_id %s to ticket_ack' % (id,) in log)

        z = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(filter(lambda x: x['status'] == '-P',z)),0)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should have been called
        self.assertEquals(len(rows), 1)

        # The third ticket is a 3 HR ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC2-2009-04-23-00100.txt"))
        raw = f.read()
        f.close()

        drop("OCC2_3.txt", raw, dir='test_incoming_OCC2')

        # From update
        f = open(os.path.join("..", "testdata", "sample_tickets", "VA_3HR_Ack",
            "OCC3-2009-04-23-00079.txt"))
        raw = f.read()
        f.close()

        drop("OCC3_3.txt", raw, dir='test_incoming_OCC2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.log.logger.logfile = StringIO.StringIO()

        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('Added ticket_id %s to ticket_ack' % id in log)
        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should have been called
        self.assertEquals(len(rows), 1)

        # what's the id of the ticket we just posted?
        ticket_id = m.ids[-1]

        # Get the ticket version
        versions = self.tdb.get_ticket_versions(ticket_id)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")

        # insert_ticket_ack should have been called by main, not by the router
        self.assertEquals(len(rows), 1)
        # The ticket version will not be the last version since the
        # insert_ticket_ack sp orders the versions by transmit date and the
        # 2nd to the last and the last have equal transmit dates
        self.assertEquals(versions[2].ticket_version_id,
                          rows[0]['ticket_version_id'])



if __name__ == "__main__":

    unittest.main()
