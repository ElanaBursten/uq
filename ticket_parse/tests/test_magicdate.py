# test_magicdate.py

import unittest
#
import magicdate

class TestMagicDate(unittest.TestCase):

    def test_01(self):
        d1 = magicdate.magicdate("4th Jan 2003")
        self.assertEquals(str(d1), "2003-01-04")

        d2 = magicdate.magicdate("June 29, 2010")
        self.assertEquals(str(d2), "2010-06-29")
