# test_abstractresponderdata.py

import unittest
import site; site.addsitedir('.')
import xml.etree.ElementTree as ET
#
import abstractresponderdata

class tdb:
    def __init__(self,notes_in):
        self.notes = [notes_in]
    def get_all_notes(self,*args, **kwargs):
        return self.notes

class TestAbstractResponderData(unittest.TestCase):

    def test_get_ticket_notes_1(self):
        ard = abstractresponderdata.AbstractResponderData()
        ard.tdb = tdb('Notes &<>')
        ard.row = {'ticket_id':'1234',
                   'locate_id':'5678'}
        notes = ard.get_ticket_notes()
        self.assertEquals(notes,'Notes &amp;&lt;&gt;')

    def test_get_ticket_notes_2(self):
        ard = abstractresponderdata.AbstractResponderData()
        ard.tdb = tdb('&'*251)
        ard.row = {'ticket_id':'1234',
                   'locate_id':'5678'}
        notes = ard.get_ticket_notes()
        ET.fromstring('<note>' + notes + '</note>')


    def test_get_ticket_notes_3(self):
        note = 'spoke with mario vital on site needs only iners. Of 16 &17th & main this is all the work left for wtr & sewer will start in 8 days with st work new curbs & resufacing will update ticket when ready to start marked 4'' mw & 2- 3'' mw mains at 16 t'
        ard = abstractresponderdata.AbstractResponderData()
        ard.tdb = tdb(note)
        ard.row = {'ticket_id':'1234',
                   'locate_id':'5678'}
        notes = ard.get_ticket_notes()
        ET.fromstring('<note>' + notes + '</note>')

if __name__ == "__main__":
    unittest.main()
