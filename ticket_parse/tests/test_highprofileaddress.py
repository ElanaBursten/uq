# test_highprofileaddress.py

import site; site.addsitedir('.')
import unittest
#
import client
import date
import highprofileaddress as hpa
import locate
import ticket_db
import ticket_grid
import ticketparser
import ticket as ticketmod
import _testprep

class TestHighProfileAddress(unittest.TestCase):

    def setUp(self):
        """
        Set up for each test
        """
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()
        self.tdb.delete('hp_address', call_center='XYZ')
        self.tdb.delete('hp_address', call_center='ZYZ')
        _testprep.add_test_hp_address(self.tdb, 'XYZ', 'ABC', 'Linden Ln.',
         'Dallas', 'Dallas', 'TX')
        _testprep.add_test_hp_address(self.tdb, 'XYZ', 'ABC', 'Norwich Dr.',
         'Fort Worth', 'Tarrant', 'TX')
        _testprep.add_test_hp_address(self.tdb, 'XYZ', 'ABC', 'Hwy 322',
         'Kilgore', 'Rusk', 'TX')
        _testprep.add_test_hp_address(self.tdb, 'XYZ', 'DEF', 'Maumelle',
         'Plano', 'Collin', 'TX')
        _testprep.add_test_hp_address(self.tdb, 'ZYX', 'PQR', 'Alma', 'Dallas',
         'Dallas', 'TX')

    def tearDown(self):
        """
        Tear down for each test
        """
        self.tdb.delete('hp_address', call_center='XYZ')
        self.tdb.delete('hp_address', call_center='ZYZ')

    # NOTE: we will test Main's usage of high profile addresses in test_main.py

    def test_hp_addresses(self):
        """
        Test HighProfileAddress data structure
        """

        hpaddress = hpa.HighProfileAddress(self.tdb)
        self.assertEquals(hpaddress.data['XYZ']['ABC'],
                          [{'county': 'Dallas', 'city': 'Dallas', 'state': 'TX',
                            'street': 'Linden Ln.'},
                           {'county': 'Tarrant', 'city': 'Fort Worth',
                            'state': 'TX', 'street': 'Norwich Dr.'},
                           {'county': 'Rusk', 'city': 'Kilgore', 'state': 'TX',
                            'street': 'Hwy 322'}])
        self.assertEquals(hpaddress.get('XYZ', 'DEF'),
                          [{'county': 'Collin', 'city': 'Plano', 'state': 'TX',
                            'street': 'Maumelle'}])
        self.assertEquals(hpaddress.get('BLAH', 'BLAH'), [])
        self.assertEquals(hpaddress.get('XYZ', 'FOO'), [])

        self.assertEquals(len(hpaddress.data['XYZ']), 2)

    def test_checking(self):
        """
        Test checking of high profile addresses
        """

        # delete ticket_grid_log
        self.tdb.dbado.deleterecords('ticket_grid_log')

        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ABC"), locate.Locate("DEF")]
        t.work_address_street = "linden ln."
        t.work_city = "DALLAS"
        t.work_county = "dallas"
        t.work_state = "tx"
        t.image = "blah"
        id = self.tdb.insertticket(t)

        # XYZ has 5 clients
        clients = [client.Client(client_code=z)
                   for z in ["ABC", "DEF", "PQR", "FOO", "BAR"]]

        log = []
        def callback(ticket, ticket_id, client, relevant_addresses, grid_email):
            log.append((ticket, ticket_id, client, relevant_addresses, grid_email))
            return 1

        hpaddress = hpa.HighProfileAddress(self.tdb)
        hpaddress.check(t, id, -1, clients, callback)

        # For client ABC, there are three HP addresses; one
        # of these appear on the ticket; they are the "relevant addresses" that
        # will be passed to the callback.
        # For client DEF, there's one HP address, which doesn't appear
        # on the ticket.

        self.assertEquals(len(log), 1)
        self.assertEquals(log[0][2], t.locates[0])
        self.assertEquals(log[0][3], {'county': 'dallas', 'city': 'DALLAS',
                                      'state': 'tx', 'street': 'linden ln.'})
        self.assertEquals(log[0][4], "")

        # check has_hp_grids method
        x = hpaddress.has_hp_address(t, None)
        self.assertEquals(bool(x), True)

        # check ticket_grid_log table
        rows = self.tdb.dbado.getrecords('ticket_grid_log', ticket_id=id)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_code'], 'ABC')

        # latest "sent" date should be today
        #d = ticket_grid.get_date_sent(self.tdb, t.ticket_format, t.ticket_number, 'ABC')
        #today = date.Date().isodate()[:10]
        #self.assertEquals(d[:10], today)

        t.work_address_street = "prestoncrest"
        t.work_city = "DALLAS"
        t.work_county = "dallas"
        t.work_state = "tx"
        self.assertEquals(bool(hpaddress.has_hp_address(t, None)), False)

    def test_get_hp_address_clients(self):
        """
        Test get_hp_address_clients
        """

        # make dummy ticket
        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ABC"), locate.Locate("DEF")]
        t.work_address_street = "linden ln."
        t.work_city = "DALLAS"
        t.work_county = "dallas"
        t.work_state = "tx"
        t.image = "blah"

        hpaddress = hpa.HighProfileAddress(self.tdb)
        locates = hpaddress.get_hp_address_clients(t, None)
        self.assertEquals(locates, t.locates[:1]) # only 'ABC', not 'DEF'


if __name__ == "__main__":

    unittest.main()

