# test_main_FDX4.py

from StringIO import StringIO
import copy
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
#
from mockery import *
from mockery_tools import *
import _testprep
import call_centers
import config
import datadir
import locate_status
import main
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
#
from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs

class TestMainFDX4(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.TEST_DIRECTORIES = []

        self.handler = ticketparser.TicketHandler()
        sql = "select * from call_center where cc_code = 'FDX4'"
        rows = self.tdb.runsql_result(sql)
        if not rows:
            _testprep.add_test_call_center(self.tdb, 'FDX4')
            self.delete_call_center = True
        else:
            self.delete_call_center = False
        self.call_center = 'FDX1'
        self.update_call_center = 'FDX4'
        self.AMP_activated = False

        self.tdb.deleterecords("client", call_center=self.call_center,
                               oc_code="AMP")
        results = self.tdb.getrecords("client",
                  call_center=self.call_center,
                  oc_code='AMP',
                  active=1,
                  update_call_center=self.update_call_center)
        if not results:
            self.AMP_activated = True
            self.AMP_client_id = _testprep.add_test_client(self.tdb,
                                 'AMP',
                                 self.call_center,
                                 update_call_center=self.update_call_center)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        if self.AMP_activated:
            _testprep.delete_test_client(self.tdb, self.AMP_client_id)
        #_testprep.clear_database(self.tdb)

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_FDX4',
          'format': 'FDX4',
          'processed': 'test_processed_FDX4',
          'error': 'test_error_FDX4',
          'attachments': ''}]
        _testprep.set_call_center_active(self.tdb, 'FDX4')
        self.cc_copy.reload()

        # make sure FDX4 is an update call center of FDX1... for these tests
        if not self.cc_copy.update_call_center('FDX4'):
            self.cc_copy.ucc['FDX4'] = 'FDX1'

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming', 'processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)


    @with_config_copy
    @with_call_center_copy
    def test_fdx4_1(self):
        """ Test if FDX4 is loaded and routed properly. """

        self.prepare()

        # make sure FDX4 updates FDX1
        self.cc_copy.ucc['FDX4'] = 'FDX1'
        #rows = self.tdb.dbado.getrecords("client", call_center="FDX1",
        #       oc_code="AMP")
        #import pprint; pprint.pprint(rows)

        NAME = 'FDX.uxMEl zXJiuM'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        map_id = _testprep.add_test_map(self.tdb, "EZTESM", 'TX', 'TARRANT',
                 'FDX1')
        area_id = _testprep.add_test_area(self.tdb, '127.TARRANT.PG048',
                  emp_id, map_id = map_id)
        _testprep.add_test_map_page(self.tdb, map_id, 50)
        # Add map_cell row for A to Z
        _testprep.add_test_map_cell(self.tdb, map_id, 50, 'Y', 1, area_id)

        try:
            # check if find_map_area_locator finds this locator/area
            rows = self.tdb.find_map_area_locator('FDX1', 50, 'Y', 1,
                   county='TARRANT')
            self.assertEquals(len(rows), 1)
            self.assertEquals(rows[0]['area_id'], area_id)
            self.assertEquals(rows[0]['emp_id'], emp_id)

            datadir_path = datadir.datadir.path
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets", "DIGGTESS", "FDX4-2008-12-02-01631.txt"))
            raw = tl.tickets[0]
            drop("test_FDX4_with_routing.txt", raw, dir='test_incoming_FDX4')

            # store the ticket using main.py
            m = main.Main(verbose=0, call_center='FDX4')
            m.log.logger.logfile = StringIO()
            m.run(runonce=1)
            log = m.log.logger.logfile.getvalue()
            m.log.logger.logfile.close()

            # assert the ticket is in the database
            id = m.ids[-1]
            rows = self.tdb.getrecords("ticket")
            self.assertEquals(len(rows), 1)
            self.assertEquals(rows[0]["ticket_id"], id)

            # add one of the non-client locates as a new client
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()

            # Send the log to a string
            tr.log.logger.logfile = StringIO()

            found = 0
            for row in tr.clients["FDX1"]:
                if row.client_code == "AMP":
                    found = 1
            self.assert_(found, "AMP not found by router")

            # then run the router
            tr.run(runonce=1)
            log_str = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()

            # see if the new client has a client_id

            # inspect some locates
            r1, = self.tdb.getrecords("locate", ticket_id=id, client_code="AMP")
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            # Verify it is assigned to the right locator
            self.assertEquals(r1['assigned_to'],emp_id)
            # Verify that munic locator was used
            match_str = ("locator %s found " % emp_id) \
                      + "(BusinessLogic.route_map_area_with_code)"
            self.assertNotEquals(log_str.find(match_str), -1)

        finally:
            # Clean up
            for letter in string.ascii_uppercase:
                _testprep.delete_test_map_cell(self.tdb, map_id, 50, letter,
                  'Y', area_id)
            _testprep.delete_test_map_page(self.tdb, map_id, 50)
            _testprep.delete_test_map(self.tdb, "EZTESM", 'TX', 'TARRANT',
              'FDX1')
            _testprep.delete_test_area(self.tdb, NAME, emp_id)
            _testprep.delete_test_employee(self.tdb, NAME)
            #_testprep.clear_database(self.tdb)

    @with_config_copy
    @with_call_center_copy
    def test_fdx4_2(self):
        """ Change FDX4 to update FHL1 """

        self.prepare()

        # Make
        call_center = 'FHL1'
        update_call_center = 'FDX4'
        client_id = _testprep.add_test_client(self.tdb,
                    'ZZZ', call_center, update_call_center=update_call_center)

        self.cc_copy.ucc['FDX4'] = 'FHL1' # modifies our local copy ^_^
        NAME = 'FHL.Aaron Hebert'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        map_id = _testprep.add_test_map(self.tdb, "EZTESM", 'TX', 'TARRANT',
                 'FHL1')
        area_id = _testprep.add_test_area(self.tdb, '127.TARRANT.PG048', emp_id,
                  map_id=map_id)
        _testprep.add_test_map_page(self.tdb, map_id, 50)
        # Add map_cell row for A to Z
        _testprep.add_test_map_cell(self.tdb, map_id, 50, 'Y', 1, area_id)

        try:
            # check if find_map_area_locator finds this locator/area
            rows = self.tdb.find_map_area_locator('FHL1', 50, 'Y', 1,
                   county='TARRANT')
            self.assertEquals(len(rows), 1)
            self.assertEquals(rows[0]['area_id'], area_id)
            self.assertEquals(rows[0]['emp_id'], emp_id)

            datadir_path = datadir.datadir.path
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets","DIGGTESS","FDX4-2008-12-02-01631.txt"))
            raw = tl.getTicket()
            raw = raw.replace('AMP','ZZZ')
            drop("test_FDX4_with_routing.txt", raw, dir = 'test_incoming_FDX4')

            # store the ticket using main.py
            m = main.Main(verbose=0, call_center='FDX4')
            m.log.logger.logfile = StringIO()
            m.run(runonce=1)
            main_log = m.log.logger.logfile.getvalue()
            m.log.logger.logfile.close()

            self.assertTrue("...1 locate(s) on ticket: ['ZZZ']" in main_log)

            # assert the ticket is in the database
            tick_id = m.ids[-1]
            tick = self.tdb.getticket(tick_id)
            self.assertEquals(tick.ticket_id, tick_id)
            self.assertEquals(tick.ticket_format, 'FHL1')
            self.assertEquals(tick.source, 'FDX4')

            # Mock the FHL1 route_map_area_with_code method with
            # the FDX1 method

            def route_map_area_with_code(self, row):
                return self.lcatalog.map_grid_county_state_TX(row, code='FHL1')

            import callcenters
            bl = callcenters.get_businesslogic('FHL1')
            bl.route_map_area_with_code = route_map_area_with_code

            # add one of the non-client locates as a new client
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()

            # Send the log to a string
            tr.log.logger.logfile = StringIO()

            found = 0
            for row in tr.clients["FHL1"]:
                if row.client_code == "ZZZ":
                    found = 1
            self.assert_(found, "ZZZ not found by router")

            # then run the router
            tr.run(runonce=1)
            log_str = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()

            # see if the new client has a client_id

            # inspect some locates
            r1 = self.tdb.getrecords("locate",
                                     ticket_id=tick_id,
                                     client_code="ZZZ")[0]
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            # Verify it is assigned to the right locator
            self.assertEquals(r1['assigned_to'],emp_id)
            # Verify that munic locator was used
            match_str = ("locator %s found " % emp_id) \
                      + "(BusinessLogic.route_map_area_with_code)"
            self.assertTrue(match_str in log_str)
        finally:
            # Clean up
            for letter in string.ascii_uppercase:
                _testprep.delete_test_map_cell(self.tdb, map_id, 50, letter,
                 'Y', area_id)
            _testprep.delete_test_map_page(self.tdb, map_id, 50)
            _testprep.delete_test_map(self.tdb, "EZTESM", 'TX', 'TARRANT',
              'FDX1')
            _testprep.delete_test_area(self.tdb, NAME, emp_id)
            _testprep.delete_test_employee(self.tdb, NAME)
            _testprep.delete_test_client(self.tdb, client_id)
            #_testprep.clear_database(self.tdb)


if __name__ == "__main__":
    unittest.main()

