# test_email_by_client.py

import unittest
import site; site.addsitedir('.')
#
import _testprep
import email_by_client
import ticket_db

class TestEmailByClient(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.ids = []

    def tearDown(self):
        # delete test email notification records
        for id in self.ids:
            try:
                self.tdb.delete('notification_email_by_client',
                 notification_email_id=id)
            except:
                pass # don't worry about it

    def test_001(self):
        # add test data
        _testprep.add_test_call_center(self.tdb, 'XYZ')
        clients = [_testprep.add_test_client(self.tdb, 'XYZ0' + str(i+1), 'XYZ')
                   for i in range(3)]
        for notif_type, client_id, email in (
            ('emergency', clients[0], 'foo@bar.com'),
            ('emergency', clients[0], 'bar@baz.com'),
            ('emergency', clients[1], 'hiha@hahi.com'),
            ('highprofile', clients[2], 'blah@blah.com;zigazig@ah.com'),
        ):
            id = _testprep.add_test_notification_email(self.tdb, notif_type,
                 client_id, email)
            self.ids.append(id)

        # test test...
        ebc = email_by_client.EmailByClient(self.tdb)

        self.assertEquals(ebc.get_emails('XYZ', 'XYZ01', 'emergency'),
         ['foo@bar.com', 'bar@baz.com'])
        self.assertEquals(ebc.get_emails('XYZ', 'XYZ01', 'highprofile'),
         [])
        self.assertEquals(ebc.get_emails('XYZ', 'XYZ02', 'emergency'),
         ['hiha@hahi.com'])
        self.assertEquals(ebc.get_emails('XYZ', 'XYZ03', 'highprofile'),
         ['blah@blah.com', 'zigazig@ah.com'])
        self.assertEquals(ebc.get_emails('ABC', 'ABC03', 'blah'), [])


if __name__ == "__main__":

    unittest.main()
