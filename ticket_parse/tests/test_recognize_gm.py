# test_recognize_gm.py

import os
import site; site.addsitedir('.')
import unittest
#
import recognize
import ticketloader
import ticketparser
import _testprep

class TestRecognizeGM(unittest.TestCase):

    def test_recognize_goodmorning_old(self):
        """ Recognize good morning tickets. """
        # NOTE: To extend these tests, append new messages to
        # testdata/goodmorning.txt and append the appropriate format name to
        # the list below.
        # When recognize.recognize() would be ambiguous, use fixed=1 (e.g. for
        # the Lanham/Richmond1/Fairfax/etc family of formats.
        tl = ticketloader.TicketLoader("../testdata/goodmorning.txt")
        handler = ticketparser.TicketHandler()

        # sample "good morning" messages appear in goodmorning.txt in this
        # order
        formats = [
            # (format, fixed)
            ("Richmond2", 1),
            ("Orlando", 1),
            ("Richmond1", 1),
            ("Richmond2", 0),
            ("Richmond2", 0),
            ("NorthCarolina", 1),
            ("Pennsylvania", 1),
            ("Atlanta", 1),
            ("NorthCarolina", 1),
            ("Baltimore", 1),
            ("Richmond1", 1),
            ("Orlando", 1),
            ("Richmond1", 1),
            ("Richmond3", 1),
            ("SouthCalifornia", 1),
            ("Orlando", 1),
            ("NorthCalifornia", 1),
            ("Alabama", 1),
            ("SouthCalifornia", 1),
            ("Kentucky", 1),
            ("Alabama", 1),
            ("Chesapeake2", 1),
            ("Tulsa", 1),
            ("AlabamaMobileNew", 1),
            ("AlabamaMobileNew", 1),
            ("Tennessee", 1),
            ("ChattanoogaGA", 1),
            ("Wisconsin2", 1),
            ("Oregon", 1),
            ("Ohio", 1),
            ("Wisconsin2", 1),
            ("SouthCalifornia", 1),
            ("GreenvilleNew", 1),
            ("Arizona", 1),
            ("Oregon2", 1),
            ("SanDiego", 1),
            ("Albuquerque2", 1),
            ("Baltimore", 1),
            ("Delaware1", 1),
            ("Washington", 1),
            ("VA9301", 1),
            ("Washington", 1),
            ("ChattanoogaNC", 1),
            ("Utah", 1),
            ("Utah", 1),
            ("Houston1", 1),
            ("AR1393", 1),
            ("Arkansas", 1),
            ("Greenville", 1),
            ("GreenvilleNew", 1),
            ("BatonRouge2", 1),
            ("MississippiLA", 1),
            ("BatonRouge1", 1),
            ("Kentucky", 1),
            ("OregonFalcon", 1),
            ("Nevada", 1),
            ("KentuckyNew", 1),
            ("Kentucky", 1),
            ("BatonRouge1", 1),
            ("SC1421", 1),
        ]
        for format, fixed in formats:
            raw = tl.getTicket()
            # are they recognized by recognize_gm_message?
            if fixed:
                type = recognize.recognize_as(raw, format)
                self.assertEquals(type, "gm_message",
                                  "%s %s (%s)" % (format, type, raw))
                suggestion = [format]
            else:
                x = recognize.recognize_gm_message(raw)
                self.assertEquals(x, format)
                suggestion = []

            # are they recognized by TicketHandler?
            obj = handler.parse(raw, suggestion)
            self.assertEquals(isinstance(obj, ticketparser.GMMessage), 1)
            self.assertEquals(obj.format, format)

        raw = tl.getTicket()
        if raw is not None:
            self.fail("Redundant tickets in goodmorning.txt")

    GM_FILES = [
        ("LNV1-2010-09-20-00006.txt", "Nevada"),
    ]

    def __test_goodmorning_new(self):
        """ Test "good morning" tickets in testdata/gm directory. """

        handler = ticketparser.TicketHandler()

        for filename, format in self.GM_FILES:
            path = os.path.join(_testprep.TICKET_PATH, "gm", filename)
            tl = ticketloader.TicketLoader(path)
            raw = tl.tickets[0]
            type = recognize.recognize_as(raw, format)
            self.assertEquals(type, "gm_message",
              "Unrecognized GM message: %s (%s)" % (filename, format))

            # are they recognized by TicketHandler?
            obj = handler.parse(raw, [format])
            self.assertTrue(isinstance(obj, ticketparser.GMMessage))
            self.assertEquals(obj.format, format)

    def __test_gm_markers(self):
        # test a file with a "gm_marker" in it; it should be considered a "good
        # morning" message, no matter what the call center is
        path = os.path.join(_testprep.TICKET_PATH, "gm",
               "LNV1-2010-09-20-00006.txt")
        tl = ticketloader.TicketLoader(path)
        raw = tl.tickets[0]
        for format in ["Atlanta", "Chattanooga", "NewJersey"]:
            type = recognize.recognize_as(raw, format)
            self.assertEquals(type, "gm_message",
              "File with GM marker should be recognized as GM message")

if __name__ == "__main__":
    unittest.main()

