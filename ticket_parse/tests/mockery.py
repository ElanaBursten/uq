# mockery.py
# Very simple class (a "context manager") to allow mocking of an object's
# attribute while executing its block.
#
# with Mockery(object, 'foo', 42):
#     ...executes code with object.foo set to 42...
# ...outside of the block, object.foo has its original value...
#
# References:
# http://docs.python.org/reference/compound_stmts.html#with

from __future__ import with_statement

class Mockery(object):
    def __init__(self, target, attr, replacement):
        self.target = target
        self.attr = attr
        self.replacement = replacement
    def __enter__(self):
        self.old_value = getattr(self.target, self.attr) # must exist
        setattr(self.target, self.attr, self.replacement)
        return self
    def __exit__(self, type, value, traceback):
        setattr(self.target, self.attr, self.old_value)
        return type is None # reraise exception if there was any

# default dummy function for mocks that don't do anything
def mock_dummy(*args, **kwargs): return None

# use this inside a lambda to raise an exception:
def mock_raise(exc_type, arg): raise exc_type(arg)

def mock_returns(x):
    return lambda *args, **kwargs: x

def make_logger(result=None):
    log = []
    def L(*args, **kwargs):
        log.append((args, kwargs))
        return result
    L.log = log # :-)
    return L

class MockObject(object):
    def __init__(self):
        self._log = []
    def __getattr__(self, name):
        self._log.append(('__getattr__', name))
        return self
    def __call__(self, *args, **kwargs):
        self._log.append(('__call__', args, kwargs))
        return self

