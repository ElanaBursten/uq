# test_qminterface.py

import os
import site; site.addsitedir('.')
import unittest
#
import datadir
import qminterface
import _testprep

class TestQMInterface(unittest.TestCase):

    def test_write_utility_ini(self):
        _testprep.delete_utility_ini()
        written = qminterface.write_utility_ini()
        self.assertTrue(written)
        utility_ini_path = datadir.datadir.get_filename("utility.ini")
        self.assertTrue(os.path.exists(utility_ini_path))

        # utility.ini must only be written if necessary
        written = qminterface.write_utility_ini()
        self.assertFalse(written)

