# test_tables.py
# XXX Superseded by tabledefs.py.

import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import schemaparser
import sqlmaptypes as smt
import static_tables
import tools

# tables that we want to generate.  all tables that use SQLMap should be here.
TABLES = [
    "ticket",
    "locate",
    "assignment",
    "summary_header",
    "summary_detail",
    "ticket_version",
    "attachment",
    "client",
]

# XXX OBSOLETE?
class __TestTables(unittest.TestCase):

    def test_1(self):
        for table in TABLES:
            test = tables.extract_columns(table)
            for i,item in enumerate(test):
                self.assertEqual(self.t[table][i][0],item[0])
                self.assertEqual(type(self.t[table][i][1]),type(item[1]))

    def test_2(self):
        rows = tables.client
        self.assertEquals(rows[0][0], 'client_id')
        self.assertTrue(isinstance(rows[0][1], smt.SQLInt))


if __name__ == "__main__":

    unittest.main()
