# test_ccftpserver.py

import os
import sys
import subprocess
import traceback
import unittest
import ftpresponder


class Test_CCFTPClient(unittest.TestCase):
    #Test suite for verifying the behavior of the CCFTPServer class.
    __ServerInfo = {
        'name': 'ABC', # name of the call center
        'login': "testuser",
        'outgoing_dir': "test_ftp",
        'password': "123456789",
        'server': 'localhost',
    }

    ftp_server = None

    def setUp(self):
        whereami = os.path.dirname(os.path.abspath(__file__))
        ftp_path = os.path.join(whereami, "ftp_server.py")
        # note: assumes that python.exe is in path!
        try:
            Test_CCFTPClient.ftp_server = subprocess.Popen([sys.executable, ftp_path, "21"])
        except OSError:
            traceback.print_exc()

    def tearDown(self):
        try:
            Test_CCFTPClient.ftp_server.kill()
        except:
            traceback.print_exc()

    def test_inactivity_timeout(self):
        """Verifies that attempting to use a disconnected server
        will automatically issue a reconnect.
        """

        FTPServer = ftpresponder.CCFTPClient(None, Test_CCFTPClient.__ServerInfo, None)
        FTPServer.do_connect()
        FTPServer.disconnect()
        #Verify that performing server operations now attempts to connect if disconnected
        FTPServer.put("fakeFile.txt", "ABC123")
        FTPServer.disconnect()
        self.assertEqual('ABC123', FTPServer.get("fakefile.txt"))
        print FTPServer._getlist()
        #Two lines, because the first line is a header
        self.assertEqual(2, len(FTPServer._getlist()))
        FTPServer.disconnect()
        FTPServer.delete("fakeFile.txt")



if __name__ == "__main__":

    unittest.main()
