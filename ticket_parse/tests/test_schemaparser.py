# test_schemaparser.py

import os
import unittest
import site; site.addsitedir('.')
#
import schemaparser

# location is schema file is relative to this one
whereami = os.path.dirname(os.path.abspath(__file__))
schema_path = os.path.join(whereami, "..", "..", "schema", "uq_schema.sql")

class TestSchemaParser(unittest.TestCase):
    will_fail = False

    def test_line_numbers(self):
        schema = open(schema_path).read()
        sp = schemaparser.SchemaParser(schema)

        start, stop = sp.get_table_line_numbers("ticket")
        self.assertEquals(start, 231-1)
        self.assertEquals(stop, 309-1)

        start, stop = sp.get_table_line_numbers("locate")
        self.assertEquals(start, 382-1)
        self.assertEquals(stop, 415-1)
    #Since the schema changes, all this test does is flag when it changes
    test_line_numbers.will_fail = True

    def test_get_fields(self):
        schema = open(schema_path).read()
        sp = schemaparser.SchemaParser(schema)

        fields = sp.get_fields(139-1, 211-1)

        # XXX add tests...

    def test_split_field_indicator(self):
        schema = open(schema_path).read()
        sp = schemaparser.SchemaParser(schema)

        data = [
            ("varchar(10)", ["varchar", "10"]),
            ("decimal(5,9)", ["decimal", "5", "9"]),
            ("integer", ["integer"]),
            ("int", ["int"]),
            ("text", ["text"]),
            ("bit", ["bit"]),
        ]

        for indicator, result in data:
            self.assertEquals(sp.split_field_indicator(indicator), result)


if __name__ == "__main__":

    unittest.main()

