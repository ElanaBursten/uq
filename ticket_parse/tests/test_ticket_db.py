# test_ticket_db.py
# Created: 19 Mar 2002, Hans Nowak

import pythoncom
import site; site.addsitedir('.')
import unittest
#
import assignment
import businesslogic
import dbinterface # new
import duplicates
import locate
import locate_status
import ticket as ticketmod
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
import ticketsummary
import tools
import tptestcase
import work_order_version
import _testprep

class TestTicketDB(tptestcase.TPTestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_find_munic_area_locator(self):
        emp_id = _testprep.add_test_employee(self.tdb, 'munic1', 'Find',
                 'Munic')
        area_id = _testprep.add_test_area(self.tdb, 'BOGUS AREA', emp_id)
        _testprep.add_test_county_area(self.tdb, 'BERGEN', area_id, 'NJ',
         'NewJersey', 'FORT BOGUS', 602)

        result = self.tdb.find_munic_area_locator("NJ", "BERGEN", "FORT BOGUS")
        self.assertEquals(len(result), 1)
        row = result[0]
        self.assertEquals(row["map_id"], "602")
        self.assertEquals(row["area_name"], "BOGUS AREA")

    def test_find_map_area_locator(self):
        # test if TicketDB.find_map_area_locator works

        emp_id = _testprep.add_test_employee(self.tdb, 'tfmal')
        area_id = _testprep.add_test_area(self.tdb, 'tfmal', emp_id)
        map_id = _testprep.add_test_map(self.tdb, "TFMAL", "XX", "COUNTY", "TFMAL")
        _testprep.add_test_map_page(self.tdb, map_id, 8)
        _testprep.add_test_map_cell(self.tdb, map_id, 8, "A", 12, area_id)

        result = self.tdb.find_map_area_locator("TFMAL", 8, "A", 12)
        self.assertEquals(len(result), 1)
        row = result[0]
        self.assertEquals(row["emp_id"], emp_id)
        self.assertEquals(row["area_name"], "tfmal")
        self.assertEquals(row["area_id"], area_id)

    def test_find_augusta_va(self):
        result = self.tdb.find_augusta_va("*")
        self.assertEquals(len(result), 1)
        row = result[0]
        self.assertEquals(int(row["emp_id"]), 399)
        self.assertEquals(row["area_name"], "FXL.AUGUSTA")

    def test_find_county_code(self):
        result = self.tdb.find_county_code("VA", "STAFFORD")
        self.assertEquals(result, "STAF")

    def test_find_duplicate_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Atlanta"])
        self.assertEquals(isinstance(summ, ticketsummary.TicketSummary), 1)
        self.assertEquals(summ.call_center, "Atlanta")

        # store any old summary
        self.tdb.storesummary(summ)

        # check if the summary we just stored is found by
        # find_duplicate_summaries
        z = self.tdb.find_duplicate_summaries(summ)
        self.assertEquals(len(z), 1)

    def test_ticket_ack(self):
        # get any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        # store it and keep the id
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_ticket_ack.txt')
        # add a record to ticket_ack with this id
        self.tdb.insert_ticket_ack(id)
        # add the same record again -- this should not cause an error
        self.tdb.insert_ticket_ack(id)

    def test_update_ticket_work_priority(self):
        """
        test update_ticket_work_priority
        """
        # get any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        # store it and keep the id
        id = self.tdb.insertticket(t)
        self.tdb.update_ticket_work_priority(id,None)
        # Should be no change
        t = ticketmod.Ticket.load(self.tdb,id)
        self.assertEquals(t["work_priority_id"],None)

        self.tdb.update_ticket_work_priority(id,1)
        # Should be 1
        t = ticketmod.Ticket.load(self.tdb,id)
        self.assertEquals(t["work_priority_id"],"1")

    def test_find_oldest_duplicate(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-2.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)

        # insert two tickets (as separate records)
        id1 = self.tdb.insertticket(t)
        t.ticket_id = None # remove id
        id2 = self.tdb.insertticket(t)
        self.assertNotEqual(id1, id2)
        self.assert_(int(id1) < int(id2))

        # get duplicates (these same two tickets)
        df = duplicates.DuplicateFinder(self.tdb)
        # Use only the ado access
        dups = df.find_duplicate_tickets(t)
        self.assertEquals(len(dups), 2)
        #self.assertEquals(dups[0]["ticket_id"], id1)
        #self.assertEquals(dups[1]["ticket_id"], id2)
        # hmm, the order isn't guaranteed

        dup = df._select_oldest_duplicate(dups)
        self.assertEquals(dup["ticket_id"], id1)

    def test_add_locate(self):
        # first, get a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        id = self.tdb.insertticket(t)

        # now, insert a new locate
        locate_id = self.tdb.add_locate(id, "B00B13", 946)
        self.assert_(locate_id)

        rows = self.tdb.getrecords("locate", locate_id=locate_id)
        self.assertEquals(rows[0]['ticket_id'], id)
        self.assertEquals(rows[0]['closed'], '0')
        self.assertEquals(rows[0]['client_code'], 'B00B13')

        # test it another way
        import locate
        loc = locate.Locate.load(self.tdb, locate_id)
        self.assertEquals(loc.locate_id, locate_id)
        self.assertEquals(loc.ticket_id, id)

    def test_add_assignment(self):
        # first, get a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        id = self.tdb.insertticket(t)

        # now, insert a new locate
        locate_id = self.tdb.add_locate(id, "B00B13", 946)
        self.assert_(locate_id)

        # now insert an assignment
        asg_id = self.tdb.add_assignment(locate_id, "208")
        self.assert_(asg_id)
        self.tdb.set_locate_status(locate_id, locate_status.assigned)

        # XXX check this assignment record to see if it's correct
        # XXX also check if the locate status is correct

        # this is a good opportunity to test TicketDB.find_existing_locators
        locators = self.tdb.find_existing_locators(id)
        self.assert_("208" in locators)

    def test_is_a_client(self):
        _testprep.add_test_client(self.tdb, "SCHNOOBER", "Atlanta")

        self.assertEquals(self.tdb.is_a_client("SCHNOOBER", "Atlanta"), 1)
        self.assertEquals(self.tdb.is_a_client("C00K13", "Atlanta"), 0)

    def test_locate_is_assigned(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        id = self.tdb.insertticket(t)

        # get some locates; none of them should be assigned right now
        locates = self.tdb.get_locates(id)
        self.assert_(len(locates) > 1)
        for row in locates:
            locate_id = row["locate_id"]
            z = self.tdb.locate_is_assigned(locate_id)
            self.assertEquals(z, 0)

        locate_id = locates[0]["locate_id"]
        aid = self.tdb.add_assignment(locate_id, 208, added_by="test")
        self.assert_(aid)
        self.assertEquals(self.tdb.locate_is_assigned(locate_id), 1)

    def test_insertticket_2(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        t.locates = [
         locate.Locate("USW01"),
         locate.Locate("D00K13")
        ]
        theassignment = assignment.Assignment("208")
        theassignment.added_by = "test"
        t.locates[0].status = locate_status.assigned
        t.locates[0]._assignment = theassignment

        id, loc_ids = t.insert(self.tdb)

        results = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(len(results), 1)

        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 2)
        self.assertEquals(locates[0]["client_code"], "USW01")
        locate_id = locates[0]["locate_id"]

        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        self.assertEquals(assignments[0]["locator_id"], "208")

    def test_getticket(self):
        # grab any old ticket with locates and assignments
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])

        # add some locates and assignments
        t.locates = [locate.Locate(s) for s in ["USW01", "BLAH03"]]
        t.locates[0].status = locate_status.assigned
        t.locates[1].status = locate_status.not_a_customer
        t.locates[0]._assignment = assignment.Assignment("208")

        # store it
        id, loc_ids = t.insert(self.tdb)

        # check database
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 2)

        # then retrieve it and compare original and copy
        t2 = self.tdb.getticket(id) # was: getticket_2
        self.assertEquals(len(t2.locates), 2)
        self.assert_(t2.locates[0]._assignment)
        self.assertEquals(t2.locates[0]._assignment.locator_id, "208")
        self.assertEquals(t2.locates[1]._assignment, None)
        self.assertEquals(t2.locates[1].client_code, "BLAH03")

    def test_locator_finder(self):
        sql = """
         update employee
         set active = 1, can_receive_tickets = 1
         where emp_id = 201
        """
        self.tdb.dbado.runsql(sql)

        try:
            sql = """
             update area
             set locator_id = 201
             where area_id = 400
            """
            self.tdb.dbado.runsql(sql)

            # when locator 201 is active, it should be found
            id = self.tdb.get_locator_by_area(400)
            self.assertEquals(id, '201')

            sql = """
             update employee
             set active = 0
             where emp_id = 201
            """
            self.tdb.dbado.runsql(sql)

            # but when it's not active, it shouldn't!
            id = self.tdb.get_locator_by_area(400)
            self.assertEquals(id, None)

        finally:
            # always set employee 201 back to active; it's used in other tests
            sql = """
             update employee
             set active = 1, can_receive_tickets = 1
             where emp_id = 201
            """
            self.tdb.dbado.runsql(sql)

    def test_injected_methods(self):
        # Test if the DBInterfaceADO method wrappers actually work.
        rows = self.tdb.getrecords('ticket')
        # if this does not raise an error, it apparently works ^_^

    def test_get_ticket_data(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        ticket_id = self.tdb.insertticket(t)
        # note: no assignments

        sets = self.tdb.get_ticket_data(ticket_id)
        self.assertEquals(len(sets), 3)
        self.assertEquals(len(sets[0]), 1)
        self.assertEquals(len(sets[1]), len(t.locates))
        self.assertEquals(len(sets[2]), 0)

    def test_ticket_version(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        ticket_id = self.tdb.insertticket(t)

        # now add a ticket_version record by hand
        tvid = self.tdb.add_ticket_version(ticket_id, t, "test.txt")
        self.assert_(tvid)

        # check that record
        tv = self.tdb.getrecords("ticket_version", ticket_id=ticket_id)[0]
        self.assertEquals(tv['ticket_id'], ticket_id)
        self.assertEquals(tv['image'].strip(), t.image.strip())
        self.assertEquals(tv['ticket_number'], t.ticket_number)
        self.assertEquals(tv['ticket_format'], t.ticket_format)
        self.assertEquals(tv['serial_number'], t.serial_number)
        self.assertEquals(tv['filename'], "test.txt")
        self.assertEquals(tv['ticket_revision'], t.revision)
        self.assertEquals(tv['transmit_date'], t.transmit_date)
        self.assertEquals(tv['ticket_version_id'], tvid)

        # test get_ticket_versions
        v = self.tdb.get_ticket_versions(ticket_id)
        self.assertEquals(len(v), 1)

        # add another version
        tvid2 = self.tdb.add_ticket_version(ticket_id, t, "test2.txt")
        v = self.tdb.get_ticket_versions(ticket_id)
        self.assertEquals(len(v), 2)
        version_ids = [x.ticket_version_id for x in v]
        self.assert_(tvid in version_ids)
        self.assert_(tvid2 in version_ids)

    def test_insertticket_transaction(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])

        # verify that there are 0 tickets in the database
        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), 0)

        # "cripple" a locate by making the client code too long
        t.locates[0].client_code = "this is a very long locate name"

        # then try to post it
        try:
            self.tdb.insertticket(t)
        except dbinterface.RunSQLException:
            # make sure we still have 0 records
            tickets = self.tdb.getrecords("ticket")
            self.assertEquals(len(tickets), 0)
            locates = self.tdb.getrecords("locate")
            self.assertEquals(len(locates), 0)
        else:
            self.fail("RunSQLException expected")

        t.clear_id() # must be called explicitly
        self.assertEquals(t.ticket_id, None)
        for loc in t.locates:
            self.assertEquals(loc.ticket_id, None)
            self.assertEquals(loc.locate_id, None)

        # explanation: if the error is caught (and it should be), then the
        # whole transaction should be rolled back, i.e. there should be no
        # record in the ticket table

    def test_get_clients_dict_1(self):
        clients = self.tdb.get_clients_dict()
        atlanta_clients = clients['Atlanta']
        a = atlanta_clients[0]
        self.assert_(a.alert in ('0', '1'))
        # value is unimportant, but it should exist

    def test_get_clients_dict_2(self):
        # prepare test data
        self.tdb.delete("client", call_center='fubar')
        self.tdb.delete("call_center", cc_code='fubar')
        self.tdb.delete("call_center", cc_code='durak')
        try:
            _testprep.add_test_call_center(self.tdb, 'fubar')
            _testprep.add_test_call_center(self.tdb, 'durak')
            _testprep.add_test_client(self.tdb, 'X1', 'fubar')
            _testprep.add_test_client(self.tdb, 'X2', 'fubar',
             update_call_center='durak')

            clients = self.tdb.get_clients_dict()
            spam = clients['fubar']
            self.assertEquals(len(spam), 2)

            eggs = clients.get('durak', [])
            self.assertEquals(len(eggs), 0)
        finally:
            self.tdb.delete("client", call_center='fubar')
            self.tdb.delete("call_center", cc_code='fubar')
            self.tdb.delete("call_center", cc_code='durak')

    def test_update_ticket_sg(self):
        # using self.tdb.sg for this

        # insert a ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        for loc in t.locates:
            loc.status = 'M'
        i = tools.findfirst(t.locates, lambda x: x.client_code == 'USW01')
        t.locates[i].status = '-N'
        ticket_id = self.tdb.insertticket(t)

        # create an "update"
        t2 = self.handler.parse(raw, ["Atlanta"])
        t2.ticket_type = 'BOODLIEBOO'
        t2.locates.append(locate.Locate("ABC42"))
        t2.locates.append(locate.Locate("FRED128"))
        df = duplicates.DuplicateFinder(self.tdb)
        dfresult = df.getduplicates(t2)
        # check if dfresults are what we expect
        self.assertEquals(dfresult.latest_id, ticket_id)
        self.assertEquals([x.client_code for x in dfresult.newlocates],
                          ['ABC42', 'FRED128'])
        self.assertEquals(len(dfresult.reassign), 1)
        self.assertEquals(dfresult.reassign.values()[0]['client_code'], 'USW01')

        # do the actual update
        self.tdb.updateticket_sg(ticket_id, t2, dfresult.newlocates,
         dfresult.reassign, whodunit='TEST')

        t3 = ticketmod.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(t.locates)+2, len(t3.locates))
        self.assertEquals(t3.ticket_type, 'BOODLIEBOO')
        i = tools.findfirst(t3.locates, lambda x: x.client_code == 'USW01')
        self.assertEquals(t3.locates[i].status, '-P')
        i = tools.findfirst(t3.locates, lambda x: x.client_code == 'ABC42')
        self.assertEquals(t3.locates[i].status, '-P')
        i = tools.findfirst(t3.locates, lambda x: x.client_code == 'FRED128')
        self.assertEquals(t3.locates[i].status, '-P')

        for loc in t3.locates:
            self.assertEquals(loc.ticket_id, ticket_id)
            self.assert_(loc.locate_id)

    def test_add_plat(self):
        # get any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        # store it and keep the id, and a locate_id
        id = self.tdb.insertticket(t)
        locate_id = t.locates[0].locate_id

        self.tdb.add_plat(locate_id, 'foobar')
        spam = self.tdb.getrecords('locate_plat', locate_id=locate_id)
        self.assertEquals(len(spam), 1)
        self.assertEquals(spam[0]['locate_id'], locate_id)
        self.assertEquals(spam[0]['plat'], 'foobar')

        # add the same plat and verify that there is still one record
        self.tdb.add_plat(locate_id, 'foobar')
        spam = self.tdb.getrecords('locate_plat', locate_id=locate_id)
        self.assertEquals(len(spam), 1)

        # add a different plat for the same locate
        self.tdb.add_plat(locate_id, 'noggin')
        spam = self.tdb.getrecords('locate_plat', locate_id=locate_id)
        self.assertEquals(len(spam), 2)
        plats = sorted([x['plat'] for x in spam])
        self.assertEquals(plats, ['foobar', 'noggin'])

    def test_get_latest_serial_number(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['SouthCaliforniaATT'])
        ticket_id = self.tdb.insertticket(t)

        # add several versions for this puppy
        t.source = 'SCA6'
        t.serial_number = '789'
        self.tdb.add_ticket_version(ticket_id, t, 'one.txt')
        t.serial_number = '123'
        self.tdb.add_ticket_version(ticket_id, t, 'two.txt')
        t.serial_number = '456'
        self.tdb.add_ticket_version(ticket_id, t, 'three.txt')

        # add another one with source SCA1
        t.source = 'SCA1'
        t.serial_number = '666'
        self.tdb.add_ticket_version(ticket_id, t, 'four.txt')

        serial_number = self.tdb.get_latest_serial_number(ticket_id, 'SCA6')
        self.assertEquals(serial_number, '456')

        serial_number = self.tdb.get_latest_serial_number(ticket_id)
        self.assertEquals(serial_number, '666')

        # call with non-existent ticket
        serial_number = self.tdb.get_latest_serial_number(0)
        self.assertEquals(serial_number, None)

    def test_pagenum_is_varchar(self):
        # tests if this function works.  does not test the return value,
        # because the database could be in either state.
        self.tdb.pagenum_is_varchar()

    def test_get_ticket_notes(self):
        # insert a ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)

        # insert some notes for it
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        # get them
        notes = self.tdb.get_ticket_notes(ticket_id)
        self.assertEquals(notes, ['foo', 'bar', 'baz'])

    def test_get_all_notes(self):
        # insert a ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        # now, insert a new locate
        locate_id = self.tdb.add_locate(ticket_id, "B00B13", 946)
        self.assert_(locate_id)

        # now insert an assignment
        asg_id = self.tdb.add_assignment(locate_id, "208")
        self.assert_(asg_id)
        self.tdb.set_locate_status(locate_id, locate_status.assigned)

        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)

        # insert some ticket notes
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        # insert some locate notes
        d = '2007-01-01 12:13:14'
        self.tdb.insert_locate_note(locate_id, uid, d, 'locate foo')
        self.tdb.insert_locate_note(locate_id, uid, d, 'locate bar')
        self.tdb.insert_locate_note(locate_id, uid, d, 'locate baz')

        # get ticket notes
        notes = self.tdb.get_ticket_notes(ticket_id)
        self.assertEquals(notes, ['foo', 'bar', 'baz'])

        # get all notes
        notes = self.tdb.get_all_notes(ticket_id, locate_id)
        self.assertEquals(notes[:3], ['foo', 'bar', 'baz'])
        self.assertTrue(notes[3].endswith(': locate foo'))
        self.assertTrue(notes[4].endswith(': locate bar'))
        self.assertTrue(notes[5].endswith(': locate baz'))

        # get locate notes
        notes = self.tdb.get_locate_notes(ticket_id, locate_id)
        self.assertTrue(notes[0].endswith(': locate foo'))
        self.assertTrue(notes[1].endswith(': locate bar'))
        self.assertTrue(notes[2].endswith(': locate baz'))

    def test_get_high_profile_reasons_fiber(self):
        ref_ids = self.tdb.get_high_profile_reasons_fiber()
        self.assertEquals(sorted(ref_ids), [[585], [1217]]) # may change in future

    def test_find_hp_reason_note(self):
        # insert a ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        # now, insert a new locate
        locate_id = self.tdb.add_locate(ticket_id, "XYZ789", 946)
        self.assert_(locate_id)

        # now insert an assignment
        asg_id = self.tdb.add_assignment(locate_id, "208")
        self.assert_(asg_id)
        self.tdb.set_locate_status(locate_id, locate_status.assigned)

        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)

        # insert some ticket notes (last note will be considered "active")
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'HPR-1,2,3 XYZ789')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'HPR-4,5,6 ABC123')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'HPR-1,2,3,4 XYZ789')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'HPR-4,5,6,7 ABC123')

        # make sure we retrieve the right HP note
        note = self.tdb.find_hp_reason_note(ticket_id, 'XYZ789')
        self.assertEquals(note, 'HPR-1,2,3,4 XYZ789')

    #
    # responder_ack_waiting

    def test_responder_ack_waiting(self):
        self.tdb.insert_response_ack_waiting(60001, 'NCA1', '123456')
        self.tdb.insert_response_ack_waiting(60002, 'NCA1', '789012')

        a = self.tdb.find_response_ack_waiting('NCA1', '123456')
        self.assertEquals(int(a), 60001)

        try:
            b = self.tdb.find_response_ack_waiting('NCA1', '666777')
        except KeyError:
            pass
        else:
            self.fail("KeyError expected")

        self.tdb.delete_response_ack_waiting(60001)

        rows = self.tdb.getrecords('response_ack_waiting')
        self.assertEquals(len(rows), 1)


if __name__ == "__main__":

    unittest.main()
