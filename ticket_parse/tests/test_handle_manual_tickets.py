# test_handle_manual_tickets.py

import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import handle_manual_tickets as hmt
import ticket_db
import ticketloader
import ticketparser

class TestHandleManualTickets(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def test_UQ(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        for raw in tl.tickets:
            t = self.handler.parse(raw, ['Atlanta'])
            t.status = 'MANUAL SOMETHING'
            # due date is usually set by main.py, not here
            self.tdb.insertticket(t)

        h = hmt.ManualTicketHandler(verbose=0)
        rows = h.get_manual_tickets()
        self.assertEquals(len(rows), len(tl.tickets))

        h.run()

        # verify that due dates are set
        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), len(rows))
        for row in tickets:
            self.assert_(row['due_date'])
            self.assert_(row['legal_due_date'])

    def test_LocInc(self):
        tl = ticketloader.TicketLoader("../testdata/Oregon2-1.txt")
        for raw in tl.tickets:
            t = self.handler.parse(raw, ['Oregon2'])
            t.status = 'MANUAL SOMETHING'
            # due date is usually set by main.py, not here
            t.work_date = None # make sure
            self.tdb.insertticket(t)

        h = hmt.ManualTicketHandler(verbose=0)
        h.biz_id = 'LocInc' # or it won't work
        rows = h.get_manual_tickets()
        self.assertEquals(len(rows), len(tl.tickets))

        h.run()

        # verify that due dates are set
        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), len(rows))
        for row in tickets:
            self.assert_(row['due_date'])
            self.assert_(row['legal_due_date'])


    def test_misc_centers(self):
        # When we do various call centers, are the due dates correct?
        ids = []
        names = ["Atlanta", "PA7501", "NorthCarolina", "NewJersey"]
        for name in names:
            filename = os.path.join("..","testdata", name + "-1.txt")
            tl = ticketloader.TicketLoader(filename)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, [name])
            t.status = "MANUAL XYZ"
            id = self.tdb.insertticket(t)
            ids.append(id)

        h = hmt.ManualTicketHandler(verbose=0)
        h.run()

        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), len(names))
        due_dates = ["2002-01-03 23:59:00",
                     "2004-08-09 23:59:59",
                     "2002-02-08 23:59:59", # FCL1 is special (Mantis #2770)
                     "2002-03-19 23:59:59"]
        for idx, ticket in enumerate(tickets):
            self.assertEquals(ticket['due_date'], due_dates[idx])

    def test_workload_date(self):
        emp_id = _testprep.add_test_employee(self.tdb, 'test workload_date')
        
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        t.status = 'MANUAL SOMETHING'
        ticket_id = self.tdb.insertticket(t)
        locates = self.tdb.runsql_result(
         "select * from locate where ticket_id = %s" % ticket_id)
        for locate in locates:
            self.tdb.runsql("""    
             exec assign_locate %s, '%s', %s
             """ % (locate['locate_id'], emp_id, emp_id))

        assignments_sql = """
         select a.*, t.due_date
         from ticket t             
         join locate l on l.ticket_id = t.ticket_id
         join assignment a on a.locate_id = l.locate_id
         where
           t.ticket_id = %s
           and l.closed = 0
           and l.active = 1
           and a.active = 1
         """ % ticket_id
        
        assignments1 = self.tdb.runsql_result(assignments_sql)
        for a in assignments1:
            self.assertTrue(a['due_date'] is None)
            self.assertTrue(a['workload_date'] is None)
            
        self.assertTrue(len(assignments1) > 0)
             
        h = hmt.ManualTicketHandler(verbose=0)
        h.run()

        # Verify that workload_date was set to due_date, and other relevant
        # fields match the original assignment values
        assignments2 = self.tdb.runsql_result(assignments_sql)
        for a2 in assignments2:
            self.assertTrue(a2['due_date'])
            self.assertEquals(a2['workload_date'], a2['due_date'])
            for a1 in assignments1:
                if a1['locate_id'] == a2['locate_id']:
                    break
            self.assertEquals(a1['locate_id'], a2['locate_id'])
            self.assertEquals(a1['locator_id'], a2['locator_id'])
            self.assertEquals(a1['added_by'], a2['added_by'])
            
        self.assertEquals(len(assignments1), len(assignments2))
            
        _testprep.delete_test_employee(self.tdb, 'test workload_date')    

if __name__ == "__main__":
    unittest.main()
