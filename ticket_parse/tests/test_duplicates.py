# test_duplicates.py
# Created: 2002.11.05

import unittest
import site; site.addsitedir('.')
#
import assignment
import date
import duplicates
import locate
import locate_status
import ticket_db
import ticketloader
import ticketparser
import _testprep

class TestDuplicates(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def test_find_duplicate_tickets(self):
        """ Test finding of duplicate tickets """
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Atlanta'])
        t.ticket_number = "123-456"
        # post this ticket, so we have at least one such record
        id = self.tdb.insertticket(t)
        # check for duplicates
        df = duplicates.DuplicateFinder(self.tdb)
        tix = df.find_duplicate_tickets(t)
        tix_ids = [t["ticket_id"] for t in tix]
        self.assert_(id in tix_ids, "Duplicate expected but not found")

    def test_find_duplicate_tickets_2(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        t.ticket_number = "123-456"
        # post this ticket, so we have at least one such record
        id = self.tdb.insertticket(t)
        # check for duplicates
        df = duplicates.DuplicateFinder(self.tdb)
        tix = df.find_duplicate_tickets(t)
        self.assertEquals(len(tix), 1)
        theticketid = tix[0]['ticket_id']
        self.assertEquals(theticketid, id)
        theticket = tix[0]['ticket']
        self.assertEquals(len(theticket.locates), len(t.locates))
        thelocates = tix[0]['locates']
        self.assertEquals(len(thelocates), len(t.locates))

    def test_find_duplicate_tickets_2a(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        id = self.tdb.insertticket(t)
        rows = self.tdb.getrecords('ticket', ticket_id=id)
        self.assertEquals(len(rows), 1)

        sql = """
         select ticket_id from ticket
         where ticket_number = '%s' and ticket_format = '%s'
         and year(call_date) = '%s' and ticket.active = '1'
        """ % (t.ticket_number, t.ticket_format, t.call_date[:4])
        rows = self.tdb.dbado.runsql_result(sql)
        self.assertEquals(len(rows), 1)

        # test what we get by calling the SP directly
        sql = "exec find_duplicates_2 '%s', '%s', '%s'" % (
              t.ticket_number, t.ticket_format, t.transmit_date[:4])
        sets = self.tdb.dbado.runsql_result_multiple(sql)
        self.assertEquals(len(sets), 3)
        self.assert_(sets[0]) # there are tickets
        self.assert_(sets[1]) # locates too
        self.assert_(not sets[2]) # but no assignments

    def test_find_duplicate_tickets_FWP2(self):
        """ Test finding of duplicate tickets (FWP2) """
        """ As seen in the specs:
            1. The original Ticket # 1234567, sent on 3/4/2002
            2. An update to Ticket # 1234567, sent on 3/5/2002, which would
            update the existing ticket
            3. A RENOTIFICATION Ticket # 1234567, sent on 3/8/2002, which would
            become a new ticket (with the same number) in Q Manager.
            4. Another copy of the same RENOTIFICATION sent on 3/8/2002 could
            be processed later; this would update the second ticket, it would
            not create a third ticket.

            (For this complete test, see test_main.py.)
        """

        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt", chr(12))
        raw = tl.getTicket()
        ticket = self.handler.parse(raw)

        # post a ticket, to make sure it's there
        id = self.tdb.insertticket(ticket)

        ticket.ticket_type = "RENOTIFICATION"

        df = duplicates.DuplicateFinder(self.tdb)
        dups = df.find_duplicate_tickets_renotif(ticket)
        self.assert_(dups)  # there should be at least one duplicate

        #
        # test the second version

        dups = df.find_duplicate_tickets_renotif(ticket)
        self.assert_(dups)

    def test_merge_duplicates(self):
        """ Test merging of duplicates """
        # get any old ticket and post it
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        t.locates = [locate.Locate(s) for s in ["USW01", "GN03F"]]
        # USW01 is assigned to locator 208
        t.locates[0].status = locate_status.assigned
        t.locates[0]._assignment = assignment.Assignment("208")

        id = self.tdb.insertticket(t)

        # now pretend we have a new ticket (with different locates) that is an
        # update of the old one...
        del t.locates[1:]
        t.locates.append(locate.Locate("ATL01"))

        df = duplicates.DuplicateFinder(self.tdb)
        dfresult = df.getduplicates(t)
        self.assertEquals(len(dfresult.dups), 1)
        self.assertEquals(dfresult.latest_id, id)
        df.method = 1

        # merge old and new... there should be 3 locates on the ticket now...
        df.merge_duplicate(t, dfresult.latest["ticket"])
        self.assertEquals(len(t.locates), 3)

        # ATL01 is new and should not have a locate_id...
        self.assertEquals(t.locates[0].client_code, "ATL01")
        self.assertEquals(t.locates[0].locate_id, None)

        # but the other two should!
        self.assertEquals(t.locates[1].client_code, "USW01")
        self.assert_(t.locates[1].locate_id)
        self.assertEquals(t.locates[2].client_code, "GN03F")
        self.assert_(t.locates[2].locate_id)

    def test_another(self):
        # post a ticket.  then check if duplicates find this.
        tl = ticketloader.TicketLoader("../testdata/Richmond3-1.txt")
        raw = tl.getTicket()    # get any old ticket
        t = self.handler.parse(raw, ["Richmond3"])
        # set transmit_date a day back
        d = date.Date(t.transmit_date)
        d.dec()
        t.transmit_date = d.isodate()
        id = self.tdb.insertticket(t)

        # close the ticket or it won't work
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)

        self.assertEquals(self.tdb.isclosed(id), 1)

        t2 = self.handler.parse(raw, ["Richmond3"])
        df = duplicates.DuplicateFinder(self.tdb)
        dfresult = df.getduplicates(t2, is_renotification=1)
        self.assertEquals(dfresult.parent_ticket_id, id)

        r = df.find_duplicate_tickets(t2)
        self.assertEquals(len(r), 1)

        r = df.find_duplicate_tickets_renotif(t2)
        self.assertEquals(len(r), 0)

    def test_duplicates_3(self):
        # grab a ticket and insert it
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        tid = self.tdb.insertticket(t)

        # add assignments
        locates = self.tdb.getrecords('locate', ticket_id=tid)
        self.assertEquals(len(locates), len(t.locates))
        lid1 = locates[0]['locate_id']
        sql = "exec assign_locate %s, '200'" % (lid1,)
        self.tdb.runsql(sql)

        a = self.tdb.getrecords('assignment', locate_id=lid1)
        self.assertEquals(len(a), 1)

        df = duplicates.DuplicateFinder(self.tdb)
        df.method = 2
        dfresult = df.getduplicates(t)
        self.assertEquals(len(dfresult.dups), 1) # there should be 1 dup
        thedup = dfresult.dups[0]
        self.assertEquals(thedup['ticket'].ticket_number, t.ticket_number)
        self.assertEquals(thedup['ticket_id'], tid)
        self.assertEquals(len(thedup['locates']), len(t.locates))

        self.assertEquals(thedup['locates'][lid1]['assigned_to'], '200')
        loc1 = [loc for loc in thedup['ticket'].locates
                if loc.client_code == locates[0]['client_code']][0]
        self.assert_(isinstance(loc1._assignment, assignment.Assignment))
        self.assertEquals(loc1._assignment.locate_id, lid1)
        self.assertEquals(loc1._assignment.locator_id, '200')

    # TODO:
    # What happens if there are multiple locates?
    # Difference find_duplicates / find_duplicates_renotif?
    # etc...
    # ...more testing is in order before this goes "live"!


if __name__ == "__main__":

    unittest.main()
