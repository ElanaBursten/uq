# test_mapgrid.py

import os
import site; site.addsitedir('.')
import string
import unittest
#
import mapgrid

class TestMapInfo(unittest.TestCase):

    def assertCell(self, cell1, cell2):
        # for now, compare if pages are the same
        self.assert_(cell1[1] == cell2[1],
         "Pages don't match: %s != %s" % (cell1, cell2))
        self.assertEquals(cell1, cell2)

    def assertNotCell(self, cell1, cell2):
        self.assertNotEqual(cell1, cell2)

    def _test_ALEX(self):
        coords2map = mapgrid.coords2map
        self.assertCell(coords2map("ALEX", 35.809200, -81.041660),
         ("ALEX", 80, 3, 6))
        self.assertCell(coords2map("ALEX", 35.811111, -81.041660),
         ("ALEX", 80, 3, 6)) # ditto
        self.assertCell(coords2map("ALEX", 35.809200, -81.039000),
         ("ALEX", 80, 3, 6)) # ditto
        self.assertCell(coords2map("ALEX", 35.846, -81.041660),
         ("ALEX", 71, 1, 6)) # different: 71,1,6
        self.assertCell(coords2map("ALEX", 35.809100, -81.041660),
         ("ALEX", 80, 4, 6)) # different: 80,4,6
        self.assertCell(coords2map("ALEX", 35.814260, -81.041660),
         ("ALEX", 80, 2, 6)) # different: 80,2,6
        self.assertCell(coords2map("ALEX", 35.809201, -81.042660),
         ("ALEX", 80, 3, 5)) # different: 80,3,5

        # ALEX,80,A,1,35.817320,-81.061960,35.821380,-81.057900
        self.assertCell(coords2map("ALEX", 35.817320, -81.061960),
         ("ALEX", 80, 1, 1))

        # test some random values
        # ALEX,7,F,6,36.024380,-81.082260,36.028440,-81.078200
        self.assertCell(coords2map("ALEX", 36.024380, -81.082260),
         ("ALEX", 7, 6, 6))
        self.assertCell(coords2map("ALEX", 36.028439, -81.082250),
         ("ALEX", 7, 6, 6))
        self.assertNotCell(coords2map("ALEX", 36.024380, -81.082261),
         ("ALEX", 7, 6, 6)) # a little bit outside of the region
        self.assertCell(coords2map("ALEX", 36.024380, -81.078210),
         ("ALEX", 7, 6, 6)) # close to longitude_2

        # ALEX,65,D,4,35.833560,-81.293380,35.837620,-81.289320
        self.assertCell(coords2map("ALEX", 35.833560, -81.293380),
         ("ALEX", 65, 4, 4))
        self.assertCell(coords2map("ALEX", 35.837619, -81.293379),
         ("ALEX", 65, 4, 4))
        self.assertNotCell(coords2map("ALEX", 35.837630, -81.293390),
         ("ALEX", 65, 4, 4))    # a little outside of it

    def test_mapgrid(self):
        eps = 0.000005  # don't make it *too* small
        data = [
            # Enter lines from .dat files here. Put county abbrev between
            # quotes, and convert letter to number, e.g. A -> 1.
            ("ALEX",1,1,1,36.044680,-81.346160,36.048740,-81.342100),
            ("ALEX",19,3,1,35.979720,-81.346160,35.983780,-81.342100),
            ("ALEX",28,1,1,35.959420,-81.346160,35.963480,-81.342100),
            ("ALEX",74,7,4,35.792960,-81.293380,35.797020,-81.289320),
            ("ALEX",22,1,2,35.987840,-81.220300,35.991900,-81.216240),
            ("ALEX",31,1,2,35.959420,-81.220300,35.963480,-81.216240),
            ("ALEX",14,6,4,35.995960,-81.171580,36.000020,-81.167520),
            ("ALEX",14,5,5,36.000020,-81.167520,36.004080,-81.163460),
            ("ALEX",15,3,3,36.008140,-81.135040,36.012200,-81.130980),
            ("ALEX",90,4,6,35.776720,-81.001060,35.780780,-80.997000),
            ("ALEX",90,4,1,35.776720,-81.021360,35.780780,-81.017300),
            ("ALEX",52,7,8,35.878220,-81.074140,35.882280,-81.070080),
            ("ALEX",89,4,10,35.776720,-81.025420,35.780780,-81.021360),

            ("BURK",1,1,1,35.999908,-81.990784,36.003968,-81.986724),
            ("BURK",33,5,1,35.926828,-81.990784,35.930888,-81.986724),
            ("BURK",114,4,4,35.788788,-81.938004,35.792848,-81.933944),
            ("BURK",146,4,4,35.731948,-81.938004,35.736008,-81.933944),
            ("BURK",256,5,5,35.557368,-81.365544,35.561428,-81.361484),

            ("CABA",1,1,1,35.504291,-80.790041,35.508351,-80.785986),
            ("CABA",117,2,1,35.272872,-80.303441,35.276932,-80.299386),
            ("CABA",156,3,3,35.183552,-80.295331,35.187612,-80.291276),
            ("CABA",51,2,4,35.414971,-80.331826,35.419031,-80.327771),
            ("CABA",25,3,4,35.467751,-80.331826,35.471811,-80.327771),
            ("CABA",138,6,7,35.199792,-80.481861,35.203852,-80.477806),

            ("CALD",1,1,1,36.121820,-81.813060,36.125880,-81.809000),
            ("CALD",27,6,1,36.044680,-81.813060,36.048740,-81.809000),
            ("CALD",169,5,1,35.764540,-81.325860,35.768600,-81.321800),
            ("CALD",139,2,5,35.833560,-81.472020,35.837620,-81.467960),

            ("CATA",176,1,2,35.543640,-80.923040,35.547700,-80.918980),
            ("CATA",1,1,1,35.827840,-81.536100,35.831900,-81.532040),
            ("CATA",82,4,9,35.673560,-81.463020,35.677620,-81.458960),
            ("CATA",118,3,4,35.620780,-81.320920,35.624840,-81.316860),

            ("CLEV",1,1,1,35.581992,-81.766492,35.586052,-81.762457),
            ("CLEV",13,7,1,35.529217,-81.766492,35.533276,-81.762457),
            ("CLEV",158,5,3,35.196323,-81.718072,35.200383,-81.714037),
            ("CLEV",160,7,8,35.188204,-81.617198,35.192263,-81.613163),
            ("CLEV",115,3,4,35.318113,-81.512290,35.322173,-81.508255),
            ("CLEV",20,6,8,35.533276,-81.455800,35.537336,-81.451765),
            ("CLEV",94,2,5,35.379009,-81.387205,35.383068,-81.383171),
            ("CLEV",35,5,3,35.508918,-81.354925,35.512978,-81.350890),

            ("DVIS",1,1,1,36.024380,-80.481380,36.028440,-80.477320),
            ("DVIS",112,6,10,35.719880,-80.404240,35.723940,-80.400180),
            ("DVIS",209,4,9,35.500640,-80.042900,35.504700,-80.038840),
            ("DVIS",163,6,6,35.606200,-80.136280,35.610260,-80.132220),

            ("DVIE",1,1,1,36.048740,-80.708740,36.052800,-80.704680),
            ("DVIE",82,2,9,35.788900,-80.676260,35.792960,-80.672200),
            ("DVIE",49,3,9,35.898520,-80.554460,35.902580,-80.550400),
            ("DVIE",87,7,10,35.768600,-80.469200,35.772660,-80.465140),

            ("GAST",1,1,1,35.417722,-81.455847,35.421782,-81.451776),
            ("GAST",29,5,1,35.344634,-81.455847,35.348694,-81.451776),
            ("GAST",100,6,3,35.198458,-81.407000,35.202518,-81.402929),
            ("GAST",116,6,6,35.170035,-81.313376,35.174095,-81.309305),
            ("GAST",22,4,6,35.377117,-81.150552,35.381178,-81.146481),
            ("GAST",82,6,1,35.255304,-81.008081,35.259364,-81.004010),

            ("IRED",1,1,1,36.057274,-81.114240,36.061334,-81.110165),
            ("IRED",23,6,1,35.980131,-81.114240,35.984191,-81.110165),
            ("IRED",102,7,4,35.777123,-81.020523,35.781183,-81.016449),
            ("IRED",214,2,7,35.513212,-80.926806,35.517273,-80.922732),
            ("IRED",63,3,9,35.907048,-80.796418,35.911108,-80.792343),

            ("LINC",1,1,1,35.569660,-81.541040,35.573720,-81.536980),
            ("LINC",32,2,8,35.508760,-81.472020,35.512820,-81.467960),
            ("LINC",51,7,10,35.460040,-81.301500,35.464100,-81.297440),
            ("LINC",38,6,4,35.492520,-81.244660,35.496580,-81.240600),

            ("MECK",1,1,1,35.514679,-81.060962,35.518724,-81.056902),
            ("MECK",27,3,1,35.449957,-81.060962,35.454001,-81.056902),
            ("MECK",55,1,8,35.401414,-80.951348,35.405460,-80.947289),
            ("MECK",201,2,9,35.085893,-80.825496,35.089939,-80.821436),
            ("MECK",218,4,2,35.049487,-80.691523,35.053532,-80.687464),
            ("MECK",193,3,9,35.110164,-80.622508,35.114210,-80.618448),

            ("ROWA",1,1,1,35.858785,-80.771800,35.862848,-80.767740),
            ("ROWA",31,6,1,35.781595,-80.771800,35.785658,-80.767740),
            ("ROWA",17,3,2,35.822221,-80.727143,35.826284,-80.723083),
            ("ROWA",94,3,8,35.680029,-80.621589,35.684091,-80.617529),
            ("ROWA",161,6,4,35.554086,-80.353644,35.558149,-80.349584),

            ("STAN",1,1,1,35.499457,-80.509467,35.503531,-80.505350),
            ("STAN",25,4,1,35.430213,-80.509467,35.434286,-80.505350),
            ("STAN",3,4,1,35.487238,-80.427140,35.491311,-80.423024),
            ("STAN",150,5,2,35.141015,-80.299534,35.145087,-80.295417),
            ("STAN",44,6,6,35.393554,-80.200741,35.397627,-80.196624),

            ("UNIO",1,1,1,35.205197,-80.843904,35.209256,-80.839820),
            ("UNIO",43,4,1,35.107778,-80.843904,35.111838,-80.839820),
            ("UNIO",183,2,5,34.831760,-80.827567,34.835819,-80.823483),
            ("UNIO",61,1,6,35.091542,-80.660117,35.095601,-80.656033),

            # csv file sez: 600,1,A,2 -> Atlanta,1,2,1
            ("Atlanta",1,1,1,34.564948,-85.112534,34.572202,-85.103792),
            ("Atlanta",25,6,1,34.456151,-85.112534,34.463403,-85.103792),
            ("Atlanta",317,7,5,33.578518,-84.797853,33.585771,-84.789112),
            ("Atlanta",232,6,6,33.875898,-84.019891,33.883151,-84.011151),
            ("Atlanta",19,7,8,34.521429,-83.792622,34.528683,-83.783881),

        ]

        coords2map = mapgrid.coords2map

        for datum in data:
            county, page, x, y, lat1, long1, lat2, long2 = datum
            CELL = (county, page, x, y)
            # test first pair of coordinates
#            self.assertCell(coords2map(county, lat2, long1), CELL)
            # test second pair as well-- they should fall outside it
#            self.assertNotCell(coords2map(county, lat2, long2), CELL)
            # somewhere in the middle
            avg_lat = (lat1+lat2)/2
            avg_long = (long1+long2)/2
            self.assertCell(coords2map(county, avg_lat, avg_long),
             CELL)

            # something very close to the corners
            self.assertCell(coords2map(county, lat1+eps, long1+eps),
             CELL)
            self.assertCell(coords2map(county, lat2-eps, long1+eps),
             CELL)
            self.assertCell(coords2map(county, lat1+eps, long2-eps),
             CELL)
            self.assertCell(coords2map(county, lat2-eps, long2-eps),
             CELL)

            # this should fall outside of it...
#            self.assertNotCell(coords2map(county, lat1, long2), CELL)

    def test_mapgrid_computations_1(self):
        alexdata = mapgrid.counties["ALEX"]
        cell = "ALEX",1,1,1,36.044680,-81.346160,36.048740,-81.342100
        self.check_cell(cell, alexdata, 0, 0)

        # this is the page directly under the previous one... a page has 7
        # lat_slices... so its lat_slice should be 7. (counting from 0)
        cell = "ALEX",10,1,1,36.016260,-81.346160,36.020320,-81.342100
        self.check_cell(cell, alexdata, 7, 0)

        cell = "ALEX",1,1,2,36.044680,-81.342100,36.048740,-81.338040
        self.check_cell(cell, alexdata, 0, 1)

    def test_mapgrid_computations_2(self):
        alexdata = mapgrid.counties["ALEX"]
        lat_columns = alexdata[7]
        long_rows = alexdata[8]
        cell = "ALEX",1,1,1,36.044680,-81.346160,36.048740,-81.342100

        lat = cell[6]   # extreme longitude
        long = cell[5]  # extreme latitude

        lat_slice, long_slice = (
         mapgrid._lat_slice(lat, alexdata[0], alexdata[1], alexdata[2]),
         mapgrid._long_slice(long, alexdata[3], alexdata[4], alexdata[5]))
        self.assertEquals(lat_slice, 0)
        self.assertEquals(long_slice, 0)

        page, lat_c, long_c = \
         mapgrid._slices2page(lat_slice, long_slice, lat_columns, long_rows,
         alexdata[6])
        self.assertEquals(page, 1)
        self.assertEquals(lat_c, 0)
        self.assertEquals(long_c, 0)

        lat_spec = lat_slice - (lat_c * 7) + 1
        self.assertEquals(lat_spec, 1)

        self.assertEquals(mapgrid.coords2map(cell[0], lat, long), cell[:4])

    def test_mapgrid_computations_3(self):
        alexdata = mapgrid.counties["ALEX"]
        lat_columns = alexdata[7]
        long_rows = alexdata[8]
        cell = ("ALEX",19,3,1,35.979720,-81.346160,35.983780,-81.342100)

        lat = cell[6]   # extreme longitude
        long = cell[5]  # extreme latitude

        max_lat, min_lat, lat_slices = alexdata[0:3]
        max_long, min_long, long_slices = alexdata[3:6]

        z = ((lat - min_lat) / (max_lat - min_lat)) * lat_slices
        y = (lat_slices - z)
        #print z, y, `y`, int(y), round(y, 8), int(round(y, 8))

        z = ((long - min_long) / (max_long - min_long)) * long_slices
        y = int(long_slices - z)
        #print z, y, `y`, int(y)

        lat_slice, long_slice = (
         mapgrid._lat_slice(lat, alexdata[0], alexdata[1], alexdata[2]),
         mapgrid._long_slice(long, alexdata[3], alexdata[4], alexdata[5]))
        #print lat_slice, long_slice
        self.assertEquals(lat_slice, 16)
        self.assertEquals(long_slice, 0)

        page, lat_c, long_c = \
         mapgrid._slices2page(lat_slice, long_slice, lat_columns, long_rows,
         alexdata[6])
        #print page, lat_c, long_c
        self.assertEquals(page, 19)
        self.assertEquals(lat_c, 2)
        self.assertEquals(long_c, 0)

        lat_spec = lat_slice - (lat_c * 7) + 1
        #print lat_spec
        self.assertEquals(lat_spec, 3)

        #print mapgrid.coords2map(cell[0], lat, long), cell[:4]
        self.assertEquals(mapgrid.coords2map(cell[0], lat, long), cell[:4])

    def check_cell(self, cell, mapdata, lat_slice, long_slice):
        # create a point in the middle of this cell
        lat = (cell[4] + cell[6]) / 2
        long = (cell[5] + cell[7]) / 2
        # this should be slice 0
        self.assertEquals(
         mapgrid._lat_slice(lat, mapdata[0], mapdata[1], mapdata[2]),
         lat_slice)
        self.assertEquals(
         mapgrid._long_slice(long, mapdata[3], mapdata[4], mapdata[5]),
         long_slice)
        self.assertEquals(mapgrid.coords2map(cell[0], lat, long), cell[:4])





if __name__ == "__main__":

    unittest.main()

