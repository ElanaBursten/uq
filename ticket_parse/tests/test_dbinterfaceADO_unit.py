# test_dbinterfaceADO_unit.py
# XXX Lots of hairy interdependent stuff going on here; clean up
# XXX Skip these tests for IronPython

import site; site.addsitedir('.')
import unittest
#
import pythoncom
import pywintypes
import win32com.client
#
import dbinterface_old as dbinterface
import dbinterface_ado
import sqlmaptypes as smt
#
from mockery import *

def object_equal(self, other):
    for key, value in self.__dict__.items():
        if not key.startswith("_"):
            try:
                yours = getattr(other, key)
            except AttributeError:
                # if the other object lacks this attribute, it's
                # apparently not going to compare equal
                return False
            if not value == yours:
                return False
    # apparently we could not find any differences
    return True

class item:
    def __init__(self,name):
        self.Name = name
    def __eq__(self,other):
        return object_equal(self,other)

class Fields:
    def __init__(self,fields):
        self.fields = [item(field) for field in fields]
        self.Count = len(self.fields)
    def Item(self,index):
        return self.fields[index]

    def __eq__(self,other):
        return object_equal(self,other)

    def __len__(self):
        return len(self.fields)

    def __getitem__(self,key):
        return self.fields[key]

    def __setitem__(self,key,value):
        self.fields[key] = value

    def __delitem__(self,key):
        del self.fields[key]

    def __getslice__(self,i,j):
        return self.fields[i:j]

    def __setslice__(self,i,j,s):
        self.fields[i:j] = s

    def __delslice__(self,i,j):
        del self.fields[i:j]

class RecordSet:
    def __init__(self):
        self.Eof = False
        self.rows = (["val_a","val_a","val_a"],
                     ["val_b","val_b","val_b"],
                     [1,1,1],
                     [2,2,2])
        self.Fields = Fields(["a","b","c","m"])

    def NextRecordset(self):
        return (RecordSet(),1)

    def GetRows(self):
        return self.rows

    def __eq__(self,other):
        return object_equal(self,other)

class Param:
    def __init__(self, *args):
        self.Name = args[0] if args else None
        self.Type = args[1] if args[1:] else None
        self.Direction = args[2] if args[2:] else None
        self.Size = args[3] if args[3:] else None
        self.Value = args[4] if args[4:] else None

class Parameters:
    def __init__(self):
        self.params = []
    def Append(self,x):
        self.params.append(x)

def execute():
    return RecordSet(), 1

def CreateParameter(*args):
    return Param(*args)

err_msg = "Error msg"
err_source = None
err_id = None
def execute_raise():
    e = pythoncom.com_error(None, "Not here", None, -1)
    e.args = ((None,),(None,),(None,err_source,err_msg,None,None,err_id))
    raise e

class TestDBInterfaceADOMocked(unittest.TestCase):

    def dispatch(self,type):
        if type == "ADODB.Command":
            return self.ado_command
        elif type == "ADODB.Connection":
            return self.ado_connection

    def setUp(self):
        self.ado_connection = MockObject()
        self.ado_command = MockObject()
        self.keep = dbinterface_ado.win32com.client.Dispatch
        dbinterface_ado.win32com.client.Dispatch = self.dispatch
        self.ado_command.Execute = execute
        self.ado_command.CreateParameter = CreateParameter
        self.ado_command.Parameters = MockObject()
        self.ado_command.Parameters.Append = Parameters().Append

        a = {"host": "nevermore", "database": "test", "login": "joe",
             "password": "notso"}
        self.db = dbinterface_ado.DBInterfaceADO(dbdata=a)

    def tearDown(self):
        dbinterface_ado.win32com.client.Dispatch = self.keep

    def test__make_connection_string(self):
        connstr = self.db._make_connection_string("localhost", "test_db",
                  "user", "secret")
        self.assertEqual(connstr,
            'Provider=sqloledb; Data Source=localhost; Initial Catalog='
          + 'test_db; User Id=user; Password=secret;')

    def test__dontcall(self):
        self.assertRaises(AttributeError,self.db.gethttp)
        self.assertRaises(AttributeError,self.db.http_get)
        self.assertRaises(AttributeError,self.db.http_post)
        self.assertRaises(AttributeError,self.db.send_updategram)

    def test__checksql(self):
        x = "abcdef"
        self.assertEqual(self.db._checksql(x),x)

    def test__runsql_1(self):
        rs = RecordSet()
        spam = self.db._runsql("select * from nada", timeout=None)
        self.assertEqual(rs,spam)

    def test_runsql_1(self):
        rs = RecordSet()
        spam = self.db.runsql("select * from nada", timeout=None)
        self.assertEqual(rs,spam)

    def test__runsql_2(self):
        global err_msg
        err_msg = "Error"
        #self.ado_command.Execute.mock_returns_func = execute_raise
        self.ado_command.Execute = execute_raise
        self.assertRaises(Exception, self.db._runsql, "select * from nada",
         timeout=None)
        to = 1000
        self.assertRaises(Exception, self.db._runsql, "select * from nada",
         timeout=to)

    def test_runsql_2(self):
        global err_msg, err_source, err_id
        err_msg = "Timeout expired"
        err_source = dbinterface_ado.SQL_SERVER_PROVIDER_NAMES[0]
        err_id = dbinterface_ado.ADO_TIMEOUT_ERROR
        self.ado_command.Execute = execute_raise
        try:
            self.db.runsql("select * from nada", timeout=None)
        except dbinterface.TimeoutError,e:
            self.assertEqual(e.data['sql'], "select * from nada")
            self.assertEqual(e.data['timeout_seconds'], self.db.timeout)
        to = 1000
        try:
            self.db.runsql("select * from nada", timeout=to)
        except dbinterface.TimeoutError,e:
            self.assertEqual(e.data['sql'], "select * from nada")
            self.assertEqual(e.data['timeout_seconds'], to)
        err_msg = "was deadlocked on lock resources"
        err_id = None
        try:
            self.db.runsql("select * from nada", timeout=None)
        except dbinterface.DBException,e:
            self.assertEqual(e.data['sql'], "select * from nada")

    def test__runsql_result_1(self):
        rows = self.db._runsql_result("select * from nada", timeout=None)
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test__runsql_result_2(self):
        global err_msg
        err_msg = "Error"
        self.ado_command.Execute = execute_raise
        try:
            self.db._runsql_result("select * from nada", timeout=None)
        except Exception,e:
            self.assertEqual(e.args[2][2], err_msg)
        to = 1000
        try:
            self.db._runsql_result("select * from nada", timeout=to)
        except Exception,e:
            self.assertEqual(e.args[2][2], err_msg)

    def test_runsql_result_1(self):
        rows = self.db.runsql_result("select * from nada", timeout=None)
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test_runsql_result_2(self):
        global err_msg, err_source, err_id
        err_msg = "Timeout expired"
        err_source = dbinterface_ado.SQL_SERVER_PROVIDER_NAMES[0]
        err_id = dbinterface_ado.ADO_TIMEOUT_ERROR
        self.ado_command.Execute = execute_raise
        try:
            self.db.runsql_result("select * from nada", timeout=None)
        except dbinterface.TimeoutError,e:
            self.assertEqual(e.data.get('sql', ""), "select * from nada")
            self.assertEqual(e.data.get('timeout_seconds', 0), self.db.timeout)
        to = 1000
        try:
            self.db.runsql_result("select * from nada", timeout=to)
        except dbinterface.TimeoutError,e:
            self.assertEqual(e.data.get('sql', ''), "select * from nada")
            self.assertEqual(e.data.get('timeout_seconds', 0), to)
        err_msg = "was deadlocked on lock resources"
        err_id = None
        try:
            self.db.runsql_result("select * from nada", timeout=None)
        except dbinterface.DBException,e:
            self.assertEqual(e.data.get('sql', ''), "select * from nada")

    def test_runsql_result_3(self):
        class RecordSet2:

            def NextRecordset(self):
                return (RecordSet(),1)

        def execute():
            return (RecordSet2(),1)

        self.ado_command.Execute = execute
        rows = self.db.runsql_result("select * from nada", timeout=None)
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test_runsql_result_4(self):
        class RecordSet:
            def __init__(self):
                self.Eof = True

        def execute():
            return (RecordSet(),1)

        self.ado_command.Execute = execute
        rows = self.db.runsql_result("select * from nada", timeout=None)
        self.assertEqual(len(rows),0)

    def test__runsql_result_multiple_1(self):
        resultsets = self.db._runsql_result_multiple("select * from nada")
        self.assertEqual(len(resultsets),100)

    def test__runsql_result_multiple_2(self):
        global err_msg
        err_msg = "Error"
        self.ado_command.Execute = execute_raise
        self.assertRaises(Exception, self.db._runsql_result_multiple,
         "select * from nada", timeout=None)
        to = 1000
        self.assertRaises(Exception, self.db._runsql_result_multiple,
         "select * from nada", timeout=to)

    def test_runsql_result_multiple_1(self):
        resultsets = self.db.runsql_result_multiple("select * from nada")
        self.assertEqual(len(resultsets),100)

    def test_runsql_result_multiple_2(self):
        global err_msg, err_source, err_id
        err_msg = "was deadlocked on lock resources"
        err_source = dbinterface_ado.SQL_SERVER_PROVIDER_NAMES[0]
        err_id = None
        self.ado_command.Execute = execute_raise
        try:
            self.db.runsql_result_multiple("select * from nada", timeout=None)
        except dbinterface.DBException,e:
            self.assertEqual(e.data['sql'], "select * from nada")

    def test_runsql_result_multiple_3(self):
        class RecordSet2:

            def NextRecordset(self):
                return (None,1)

        def execute():
            return (RecordSet2(),1)

        self.ado_command.Execute = execute
        resultsets = self.db.runsql_result_multiple("select * from nada")
        self.assertEqual(len(resultsets),0)

    def test_runsql_result_multiple_4(self):
        class RecordSet:
            def __init__(self):
                self.Eof = True
            def NextRecordset(self):
                return (None,1)

        def execute():
            return (RecordSet(),1)

        self.ado_command.Execute = execute
        resultsets = self.db.runsql_result_multiple("select * from nada")
        self.assertEqual(len(resultsets[0]),0)

    def test__runsql_with_parameters_1(self):
        sql = "select * from client where oc_code = ? and active = ?"
        rows = self.db._runsql_with_parameters(sql, ["USW01", 1])
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test__runsql_with_parameters_2(self):
        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        rows = self.db._runsql_with_parameters(sql, values)
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test__runsql_with_parameters_3(self):
        class Parameters:
            def __init__(self):
                self.params = []
            def Append(self,x):
                raise AttributeError,"Attribute error"
        self.ado_command.Parameters.Append = Parameters().Append

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        self.assertRaises(Exception,self.db._runsql_with_parameters,sql, values)

    def test_runsql_with_parameters_1(self):
        class Parameters:
            def __init__(self):
                self.params = []
            def Append(self,x):
                raise AttributeError,"Attribute error"
        self.ado_command.Parameters.Append = Parameters().Append

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        try:
            self.db.runsql_with_parameters(sql, values)
        except Exception as e:
            self.assertEqual(e.message,
             '("Cannot set value for field \'client_name\' (\'blah\')",)')

    def test_runsql_with_parameters_2(self):
        class RecordSet2:

            def NextRecordset(self):
                return (RecordSet(),1)

        def execute():
            return (RecordSet2(),1)

        self.ado_command.Execute = execute

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        rows = self.db.runsql_with_parameters(sql, values)
        for row in rows:
            self.assertEqual(row['a'],"val_a")
            self.assertEqual(row['b'],"val_b")
            self.assertEqual(row['c'],"1")

    def test_runsql_with_parameters_3(self):
        class RecordSet2:

            def NextRecordset(self):
                raise AttributeError,"Attribute error"

        def execute():
            return (RecordSet2(),1)

        self.ado_command.Execute = execute

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        rows = self.db.runsql_with_parameters(sql, values)
        self.assertEqual(len(rows),0)

    def test_runsql_with_parameters_4(self):
        global err_msg, err_source, err_id
        err_msg = "was deadlocked on lock resources"
        err_source = dbinterface_ado.SQL_SERVER_PROVIDER_NAMES[0]
        err_id = None
        self.ado_command.Execute = execute_raise

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        try:
            self.db.runsql_with_parameters(sql, values)
        except dbinterface.DBException,e:
            self.assertEqual(e.msg, err_msg)

    def test_runsql_with_parameters_5(self):
        class RecordSet:
            def __init__(self):
                self.Eof = True
            def NextRecordset(self):
                return (None,1)

        def execute():
            return (RecordSet(),1)

        self.ado_command.Execute = execute

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        rows = self.db.runsql_with_parameters(sql, values)
        self.assertEqual(len(rows),0)

    def test_call(self):
        self.db.call("find_township_baseline", "CO", "01N","071W", 10)

    def test_call_result(self):
        self.db.call_result("find_township_baseline", "CO", "01N","071W", 10)

    def test_get_max_id(self):
        max = self.db.get_max_id('response_log', 'response_id')
        self.assertEqual(max,'2')

    def test_get_ids_after(self):
        vals = self.db.get_ids_after('employee', 'a', 1)
        for val in vals:
            self.assertEqual(val,'val_a')
        vals = self.db.get_ids_after('employee', 'a', '1')
        for val in vals:
            self.assertEqual(val,'val_a')


if __name__ == "__main__":

    unittest.main()
