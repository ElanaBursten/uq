# test_main_3.py
# Created: 03 Mar 2002, Hans Nowak
# Last update: 28 Mar 2002, Hans Nowak

from __future__ import with_statement
import os
import site; site.addsitedir('.')
import unittest
import datadir
import dbinterface_old as dbinterface
import mail2
import main
import ticket_db
import ticketloader
import ticketparser
import _testprep
import errorhandling2
import emailtools

from search_log_file import SearchLogFile
#
from mockery import *
from mockery_tools import with_config_copy

def make_error_raiser(err_msg):
    def execute_raise(*args, **kwargs):
        raise dbinterface.DBFatalException(err_msg)
    return execute_raise

class TestMain3(unittest.TestCase):

    ###
    ### Helper functions
    def send(self,xml):
        self.xml = xml

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def prepare(self, call_center=''):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
            {'incoming': 'test_incoming',
             'format': call_center,
             'processed': 'test_processed',
             'error': 'test_error',
             'attachments': ''},]
        self.handler = ticketparser.TicketHandler()

        mail2.TESTING = 1
        mail2._test_mail_sent = []

        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        _testprep.clear_database(self.tdb)

        for process in self.conf_copy.processes:
            #incoming, name, processed, error, group = process
            incoming = process['incoming']
            processed = process['processed']
            error = process['error']
            _testprep.clear(incoming)
            _testprep.clear(processed)
            _testprep.clear(error)

        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center "\
         "from client where oc_code = 'USW01'"
        self.result_USW01 = self.tdb.runsql_result(sql)[0]

        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'],
          "USW01")

    def tearDown(self):
        # Set it back inactive
        _testprep.set_client_inactive(self.tdb,
          self.result_USW01['call_center'], "USW01")
        _testprep.clear_database(self.tdb)

    def drop(self, filename, data, dir="test_incoming"):
        """ Write a file (containing one or more images) to the
            test_incoming directory. """
        fullname = os.path.join(dir, filename)
        f = open(fullname, "wb")
        f.write(data)
        f.close()

    def clear_dir(self, dirname):
        """ Empty a directory. """
        filenames = os.listdir(dirname)
        for filename in filenames:
            fullname = os.path.join(dirname, filename)
            os.remove(fullname)

    def add_fake_client(self, call_center, name):
        self.remove_fake_client(call_center, name)
        sql = """
         insert client
         (client_name, oc_code, office_id, call_center, active)
         values ('%s', '%s', 100, '%s', 1)
        """ % (name, name, call_center)
        self.tdb.runsql(sql)

    def remove_fake_client(self, call_center, name):
        self.tdb.deleterecords("client", call_center=call_center,
         oc_code=name)

    ###
    ### Pre- and postconditions

    @with_config_copy
    def test_ep_1(self):
        """
        Mock the insertticket method so it raises a DBFatalException.
        Verify that no email is sent if the exception is from a deadlock.
        """
        self.prepare('Atlanta')

        #Clear logs
        datadir_path = datadir.datadir.path
        log_dir = os.path.join(datadir_path, self.conf_copy.logdir)

        err_msg = "Transaction (Process ID 94) was deadlocked on lock"+\
                  " resources with another process and has been chosen as the"+\
                  " deadlock victim. Rerun the transaction."
        # create a ticket file
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        self.drop("test_ep1.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.logger.logfile.truncate(0)

        with Mockery(m.tdb, 'insertticket', make_error_raiser(err_msg)):
            m.run(runonce=1)

        logfile_name = m.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(logfile_name,
          "Mail notification sent to")
        self.assertEquals(next_log_entry,None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name,
          "raise dbinterface.DBFatalException")
        self.assertNotEquals(match, None)
        #del m

    @with_config_copy
    def test_ep_2(self):
        self.prepare('Atlanta')
        #Clear logs
        datadir_path = datadir.datadir.path
        log_dir = os.path.join(datadir_path, self.conf_copy.logdir)

        tl = ticketloader.TicketLoader("../testdata/sample_tickets/spam.txt")
        raw = tl.getTicket()
        self.drop("test_ep2.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.logger.logfile.truncate(0)
        m.run(runonce=1)
        logfile_name = m.log.logger.logfile.name
        match, next_log_entry = SearchLogFile(logfile_name, "Probable spam")
        self.assertNotEquals(match,None)

        #test error handling surrounding send_email
        #the following ensures that email errors are sent to both logs
        smtpinfo = emailtools.SMTPInfo('badhost', port=25)
        ep = errorhandling2.ErrorPacket("Fake Stack")
        m.log_email_error(ep, smtpinfo, 'toNonOne', 'really, really fake subject', 'Body Here', 'Email Type Here')
        match, next_log_entry = SearchLogFile(logfile_name, "really, really fake subject")
        self.assertNotEquals(match,None)
        logfile_name = m.email_error_handler.logger.logfile.name
        match, next_log_entry = SearchLogFile(logfile_name, "Fake Stack")
        self.assertNotEquals(match,None)
        #del m



if __name__ == "__main__":

    unittest.main()

