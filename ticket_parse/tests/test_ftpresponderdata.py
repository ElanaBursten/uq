# test_ftpresponderdata.py
# Test retrieving of the FTPResponderData classes, and the methods of
# these classes.

import copy
import site; site.addsitedir('.')
import unittest
#
import callcenters
import config
import ftpresponder_data as frd
import responder_config_data as rcd

# XML as returned by Korterra responses server.
XML_RESULT = """\
<?xml version="1.0" encoding="UTF-8"?>
<ackcollection>
	<ack completionid="123;456">
		<ackCode>0</ackCode>
		<ackText>Success</ackText>
	</ack>
	<ack completionid="234;567">
		<ackCode>0</ackCode>
		<ackText>Success</ackText>
	</ack>
	<ack completionid="345;678">
		<ackCode>2</ackCode>
		<ackText>Member code not found</ackText>
	</ack>
	<ack completionid="789;987">
		<ackCode>0</ackCode>
		<ackText>Success</ackText>
	</ack>
</ackcollection>
"""

class TestFTPResponderData(unittest.TestCase):

    RESPONDERS = ['FMW1', 'FMB1', 'FDE1', 'FHL1', 'FHL3']

    # dummy data to keep getresponderdata() happy
    # NOTE: if a new attribute is added to config.py, add it here as well
    CONFIG = {
        'clients': [],
        'exclude_states': [],
        'file_ext': '',
        'id': 'korterra',
        'login': 'blah',
        'mappings': {
               'C': ('1', ''),
               'M': ('2', ''),
        },
        'method': '42',
        'name': 'FHL1',
        'outgoing_dir': 'processed',
        'password': 'time2',
        'send_emergencies': 0,
        'send_3hour': 0,
        'server': 'my.tickets.com',
        'skip': ['THIS01', 'THAT02'],
        'status_code_skip' : ['ZZZ'],
        'temp_dir': '',
        'translations': {},
    }

    def setUp(self):
        self.conf_copy = copy.deepcopy(config.getConfiguration())

    def get_responderdata(self, name):
        class DummyConfig: pass
        config = DummyConfig()
        config.ftpresponders = {}
        config.responderconfigdata = rcd.ResponderConfigData()
        config.responderconfigdata.gather_skip_data()
        config.ftpresponders[name] = self.CONFIG
        # the following should not raise an error
        obj = frd.getresponderdata(config, name, self.CONFIG)
        return obj

    def test_get_ftpresponderdata(self):
        for resp in self.RESPONDERS:
            obj = self.get_responderdata(resp)
            self.assert_(obj.__class__.__name__.endswith("FTPResponderData"))
            self.assert_(obj)

    def test_FHL3(self):
        import config
        cfg = config.getConfiguration()
        rdata = frd.getresponderdata(cfg, 'FHL3', {})

        ok, errors = rdata.get_acknowledgements(XML_RESULT, [])
        self.assertEquals(len(ok), 4)
        self.assertEquals(len(errors), 0)

        self.assertEquals(ok[0].locate_id, '123')
        self.assertEquals(ok[0].resp_id, '456')
        self.assertEquals(ok[0].result, '0')
        self.assertEquals(ok[0].raw, 'Success')

        self.assertEquals(ok[2].locate_id, '345')
        self.assertEquals(ok[2].resp_id, '678')
        self.assertEquals(ok[2].result, '2')
        self.assertEquals(ok[2].raw, 'Member code not found')

    def test_FHL3_extra_data(self):
        rd = callcenters.get_ftpresponderdata('FHL3')()
        class tdb:
            def getrecords(self,*args, **kwargs):
                return [{'closed_date':None}]
        def get_ticket_notes():
            return 'Notes &<>'
        names = {'locate_id':'1234'}
        rd.get_ticket_notes = get_ticket_notes
        rd.tdb = tdb()
        extra_data = rd.extra_data(names)
        self.assertEquals(extra_data['remarks'],'Notes &amp;&lt;&gt;')

    # obsolete -- Mantis #2961
    def __test_FDE1_and_ilk(self):
        for cc in ['FDE1', 'FMB1', 'FMW1']:
            rd = callcenters.get_ftpresponderdata(cc)()
            self.assertEquals(rd.LEN_TICKET_NUMBER, 9)
            self.assertEquals(rd.TEMPLATE2 %
              {'ticket_number':'123456789', 'company':'ABC', 'status':1},
              "123456789|ABC|1||\n")

            raw = '9614167|BGEBC|2|12/13/09|10:12:23|00|Success|'
            ok, errors = rd.get_acknowledgements(raw,
             [{'locate_id':'123', 'resp_id':'456'},])
            self.assertEquals(len(ok), 1)
            self.assertEquals(len(errors), 0)

            self.assertEquals(ok[0].locate_id, '123')
            self.assertEquals(ok[0].resp_id, '456')
            self.assertEquals(ok[0].result, '00')
            self.assertEquals(ok[0].date, '12/13/09')
            self.assertEquals(ok[0].time, '10:12:23')

    def test_FBL1_and_ilk(self):
        cfg = config.getConfiguration()
        cfg = copy.deepcopy(cfg)
        cfg.ftpresponders['FNL1'] = cfg.ftpresponders['FBL1']
        cfg.ftpresponders['1411'] = cfg.ftpresponders['FBL1']

        for cc in ['FBL1', 'FNL1', '1411']:
            rd = frd.getresponderdata(cfg, cc, {})
            rd.method = ''
            self.assertEquals(rd.LEN_TICKET_NUMBER, 9)
            self.assertEquals(rd.template() % {
                'ticket_number':'123456789',
                'company':'ABC',
                'status': 1}, "123456789|ABC|1|\n")

            raw = '9614167|BGEBC|2|12/13/09|10:12:23|00|Success|'
            ok, errors = rd.get_acknowledgements(raw, [
             {'locate_id':'123', 'resp_id':'456'}])
            self.assertEquals(len(ok), 1)
            self.assertEquals(len(errors), 0)

            self.assertEquals(ok[0].locate_id, '123')
            self.assertEquals(ok[0].resp_id, '456')
            self.assertEquals(ok[0].result, '00')
            self.assertEquals(ok[0].date, '12/13/09')
            self.assertEquals(ok[0].time, '10:12:23')



if __name__ == "__main__":
    unittest.main()

