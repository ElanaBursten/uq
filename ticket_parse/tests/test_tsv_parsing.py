# test_tsv_parsing.py
# Created: 25 Jan 2002, Hans Nowak

import site; site.addsitedir('.')
import string
import unittest
#
import ticketparser, parsers
import ticketloader

class TestTSVParsing(unittest.TestCase):

    def parse_tsv(self, filename):
        f = open(filename, "rb")
        lines = f.readlines()
        f.close()

        # determine the number of columns by splitting the first line on
        # tabs; all other lines should have this same number of "fields"
        num_columns = len(lines[0].split("\t"))

        data = []

        for i in range(len(lines)):
            parts = lines[i].split("\t")
            if parts and parts[0].strip():
                # this comparison ^ seems awkward, but it's the only way
                # I see to weed out the last few lines, which are erratic.
                self.assert_(len(parts) == num_columns,
                 "Invalid number of columns for line " + `i` + ": " + `parts`)
                data.append([s.strip() for s in parts])

        # Fill a dictionary with test data. The key of the dict is a tuple
        # (filename, index); the value is a dict with attribute names as
        # keys, and expected values of these attributes as values.
        testdata = {}
        for i in range(2, num_columns):
            filename = data[1][i]   #; print filename,
            index = data[2][i]      #; print index,
            testing = data[3][i]    #; print testing

            # only process columns marked for testing:
            if testing.startswith("Y") or testing.startswith("y"):
                # make a dict for this one
                key = (filename, index)
                d = {}
                for row in range(4, len(data)):
                    attrname = data[row][0]
                    value = data[row][i]
                    d[attrname] = value
                testdata[key] = d

        return testdata

    def getTicket(self, filename, index):
        tl = ticketloader.TicketLoader("../testdata/" + filename)
        count = 1
        while 1:
            t = tl.getTicket()
            if not t: raise "no ticket found"
            if count == index:
                return t
            count = count + 1

    def test_tsv(self):
        # parse TSV-file, reading the contents in a 2-dimensional list... or
        # maybe a dict... this should be in a separate method since it's not
        # part of what we test.
        testdata = self.parse_tsv("../testdata/test_cases.txt")

        # If no columns are marked for testing, this is flagged as a failure
        # to bring it to the user's attention
        if not testdata:
            self.fail("No testdata found")

        for key, value in testdata.items():
            filename, index = key
            raw = self.getTicket(filename, int(index))
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw)

            # first, let's see if these are the same tickets
            self.assertEquals(value["ticket_number"], t.ticket_number)
            # make sure ticket number is not None or empty string
            self.assert_(value["ticket_number"] and t.ticket_number)

            # test some more data
            self.assertEquals(value["call_date"], t.call_date)
            self.assertEquals(value["revision"], t.revision)

            # TODO: more fields follow...

    def test_tsv_2(self):
        testdata = self.parse_tsv("../testdata/test_cases.txt")

        if not testdata:
            self.fail("No testdata found")

        # do not test these attributes:
        donttest = ["work_remarks", "work_description", "locates"]

        for key, value in testdata.items():
            filename, index = key
            raw = self.getTicket(filename, int(index))
            tp = ticketparser.TicketHandler()
            t = tp.parse(raw)

            '''
            # first, let's see if these are the same tickets
            self.assertEquals(value["ticket_number"], t.ticket_number)
            # make sure ticket number is not None or empty string
            self.assert_(value["ticket_number"] and t.ticket_number)

            # test some more data
            self.assertEquals(value["call_date"], t.call_date)
            self.assertEquals(value["revision"], t.revision)
            '''
            for attrname, attrvalue in value.items():
                if attrname not in donttest:
                    self.assert_(attrvalue == getattr(t, attrname),
                     "Ticket %s, %s: Excel: %s, parser: %s" % (t.ticket_number
                     or "unknown", attrname, repr(attrvalue),
                     repr(getattr(t, attrname))))



if __name__ == "__main__":

    unittest.main()
