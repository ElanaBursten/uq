# test_filetools.py

from __future__ import with_statement
import os
import shutil
import unittest
import site; site.addsitedir('.')
#
import filetools

class TestFileTools(unittest.TestCase):

    def test_get_filenames_1(self):
        try:
            os.mkdir('../testdata/foobar')
        except:
            pass

        try:
            os.mkdir('../testdata/foobar/spam')
        except:
            pass

        f = open('../testdata/foobar/blah.txt', 'w')
        print >> f, 'test'
        f.close()

        try:
            files = filetools.get_filenames('../testdata/foobar')
            self.assertEquals(len(files), 1)
        finally:
            shutil.rmtree('../testdata/foobar')


    def test_get_filenames_2(self):
        files = filetools.get_filenames('../testdata/responder_ack/NCA1')
        self.assertEquals(len(files), 3)

    def drop_file(self, path, contents):
        f = open(path, 'w')
        f.write(contents)
        f.close()

    def test_move_file_with_benefits(self):
        try:
            os.mkdir('../testdata/from')
        except:
            pass
        try:
            os.mkdir('../testdata/to')
        except:
            pass

        try:
            to_files = os.listdir('../testdata/to')
            self.assertEquals(to_files, [])

            # drop a file in from/ and move it... it should appear in to/
            self.drop_file('../testdata/from/bisiwog.txt', 'zzz')
            filetools.move_with_benefits('../testdata/from/bisiwog.txt',
             '../testdata/to')
            to_files = os.listdir('../testdata/to')
            self.assertEquals(to_files, ['bisiwog.txt'])

            # drop a file with the same name in from/ and move it... it should
            # appear in to/ as well, under an augmented name
            self.drop_file('../testdata/from/bisiwog.txt', 'xxx')
            filetools.move_with_benefits('../testdata/from/bisiwog.txt',
             '../testdata/to')
            to_files = os.listdir('../testdata/to')
            self.assertEquals(len(to_files), 2)
            for fn in to_files:
                self.assert_(fn.startswith("bisiwog.txt"))

        finally:
            shutil.rmtree('../testdata/from')
            shutil.rmtree('../testdata/to')

    def test_aggregate_file(self):
        try:
            shutil.rmtree('agg1.txt')
        except:
            pass

        agg = filetools.AggregateFile('agg1.txt', 100)
        agg.write('x'*60, sep='\n')
        agg.write('y'*60, sep='\n')
        agg.write('z'*60, sep='\n') # exceeds maximum size

        with open('agg1.txt', 'rb') as f:
            data = f.read()
            stuff = 'x'*60 + '\n' + 'y'*60 + '\n'
            self.assertEquals(data, stuff)


if __name__ == "__main__":
    unittest.main()
