# test_tickethandler.py
# Test the TicketHandler object.
#
# TODO: Test other types as well (lower priority).

import os
import site; site.addsitedir('.')
import unittest
#
import call_centers
import ticketloader
import ticketparser
import _testprep

class TestTicketHandler(unittest.TestCase):

    def setUp(self):
        self.handler = ticketparser.TicketHandler()

    def test_tickets(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3003-2010-01-26", "3003-2010-01-26-01511.txt"))
        raw = tl.tickets[0]
        x = self.handler.parse(raw, call_centers.cc2format['3003'],
            call_center='3003')

        # verify that a Ticket object comes out
        self.assertEquals(type(x).__name__, 'Ticket')

    def test_work_orders(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00009-A01.txt"))
        raw = tl.tickets[0]
        x = self.handler.parse(raw, ["Lambert"], call_center='LAM01')

        # verify that a WorkOrder object comes out
        self.assertEquals(type(x).__name__, 'WorkOrder')

    def test_work_order_audits(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-order-audits", "LAM01", "LAM01-2011-03-07-00129-A01.txt"))
        raw = tl.tickets[0]
        x = self.handler.parse(raw, ["Lambert"], call_center='LAM01')

        # verify that a WorkOrderAuditHeader object comes out
        self.assertEquals(type(x).__name__, 'WorkOrderAuditHeader')

if __name__ == "__main__":
    unittest.main()

