# test_configchecker.py

import unittest
import site; site.addsitedir('.')
#
import config
import configchecker

class TestConfigChecker(unittest.TestCase):

    def test_001(self):
        cfg = config.Configuration()    # uses ./config.xml
        #cfg.processes = [["nonsense", "blah", "bogus", "poing", "zzz"]]
        cfg.processes = [{
            'incoming': 'nonsense',
            'format': 'blah',
            'processed': 'bogus',
            'error': 'poing',
            'group': 'zzz',
            'attachments': '',
        }]

        cc = configchecker.ConfigChecker()
        try:
            cc.check(cfg)
        except configchecker.ConfigurationError, e:
            self.assertEquals(e.args[0], "Directory nonsense doesn't exist")
        else:
            self.fail("ConfigurationError expected")


if __name__ == "__main__":

    unittest.main()
