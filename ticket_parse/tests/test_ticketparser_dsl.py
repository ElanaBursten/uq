# test_ticketparser_dsl.py

import os
import site; site.addsitedir('.')
import string
import unittest
#
import dsl
import ticketparser
import tools
import _testprep

whereami = os.path.dirname(os.path.abspath(__file__))

class TestTicketParserDSL(unittest.TestCase):
    def setUp(self):
        self.handler = ticketparser.TicketHandler()

#
# read test files and inject methods into test case

def inject_method(testdsl, method_name):
    def _f_(self):
        testdsl.run_all_tests(self)
    _f_.__name__ = method_name
    setattr(TestTicketParserDSL, method_name, _f_)

ALLOWED_CHARS = string.lowercase + string.uppercase + string.digits + "_"
def normalize_filename(s):
    s = s.replace(" ", "_")
    s = s.replace("-", "_")
    return string.join([c for c in s if c in ALLOWED_CHARS], "")

ROOT = os.path.join(whereami, 'tickets')
test_files = tools.safeglob(os.path.join(ROOT, "*.txt"))
test_files = [fn for fn in test_files if not fn.startswith("_")]
for idx, filename in enumerate(test_files):
    d = dsl.TestDSL(filename)
    shortname = os.path.split(filename)[1]
    shortname = os.path.splitext(shortname)[0]
    method_name = 'test_' + normalize_filename(shortname)
    inject_method(d, method_name)

