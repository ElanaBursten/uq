# test_date_special.py
# XXX deprecated; add new tests to test_businesslogic_due_dates.py instead

import time
import unittest
import site; site.addsitedir('.')
#
import businesslogic
import config
import date
import date_special
import duedatecatalog
import ticket_db
import _testprep

class TestDateSpecial(unittest.TestCase):

    # XXX most of these tests deal with call center specific due date
    # calculations, rather than the routines defined in date_special.py.
    # they should be moved or replaced with coverage in test_businesslogic.py.

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def test_000_prepare(self):
        _testprep.set_cc_routing_file()

    def test_Atlanta_due_date_2(self):
        """ Atlanta due date """
        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])
        # with new implementation of Atlanta_due_date
        d1 = date.Date("2001-12-31 10:11:00")
        d1d = logic._due_date(d1)
        self.assertEquals(d1d.isodate(), "2001-12-31 23:59:00")

    def test_FCV1_due_date(self):
        """ FCV1 due date """
        # changed per 2002.07.01
        # rule: go to 07:00 of the next morning; then add 48 hours. always
        # discount weekends and holidays.
        for cc in ("FCV1", "FCV3"):
            # this works for both FCV1 and FCV3
            logic = businesslogic.getbusinesslogic(cc, self.tdb, [])
            d1 = date.Date("2002-04-17 15:48:02")    # a Wednesday -> Monday (!)
            d1d = logic._legal_due_date(d1)
            self.assertEquals(d1d.isodate(), "2002-04-22 07:00:00")

            d2 = date.Date("2002-04-19 16:23:00")    # a Friday -> Wednesday
            d2d = logic._legal_due_date(d2)
            self.assertEquals(d2d.isodate(), "2002-04-24 07:00:00")

    def test_FCL1_due_date(self):
        """
        FCL1 due date
        Changed 2008/6/24
        """
        logic = businesslogic.getbusinesslogic("FCL1", self.tdb, [])
        d1 = date.Date("2002-05-07 15:00:00")    # 07 May, a Tuesday
        d1d = logic._due_date(d1,d1)
        self.assertEquals(d1d.isodate(), "2002-05-09 23:59:59")

        d2 = date.Date("2002-05-09 14:00:00")    # 09 May, a Thursday
        d2d = logic._due_date(d2,d2)
        self.assertEquals(d2d.isodate(), "2002-05-13 23:59:59")

        d2 = date.Date("2002-05-10 14:00:00")    # 10 May, a Friday
        d2d = logic._due_date(d2,d2)
        self.assertEquals(d2d.isodate(), "2002-05-14 23:59:59")

        # emergencies are 2 hours later
        d3 = date.Date("2002-05-09 14:00:00")    # 09 May, a Thursday
        d3d = logic._due_date(d3,d3, emergency=1)
        self.assertEquals(d3d.isodate(), "2002-05-09 16:00:00")

    def test_FWP2_due_date(self):
        """ FWP2 due date """
        logic = businesslogic.getbusinesslogic("FWP2", self.tdb, [])
        d1 = date.Date("2002-05-07 15:00:00")    # 07 May, a Tuesday
        d1d = logic._due_date(d1)
        self.assertEquals(d1d.isodate(), "2002-05-09 23:59:59")
        # really 9 May, 23:59:59?

        d2 = date.Date("2002-05-09 14:32:00")    # 09 May, a Thursday
        d2d = logic._due_date(d2)
        self.assertEquals(d2d.isodate(), "2002-05-13 23:59:59")

    # XXX does not belong here
    def test_holidays(self):
        """ Test holiday mechanism """
        # May fail if we change the text-based holiday list to a database
        # table solution. """
        # insert new holidays
        holidays = [["2002-05-27", "*"]]   # only one for now
        logic = businesslogic.getbusinesslogic("FCL1", self.tdb, holidays)

        d = date.Date("2002-05-24 13:00:00") # Friday
        dd = logic._due_date(d,d)
        self.assertEquals(dd.isodate(), "2002-05-29 23:59:59")    # Wednesday

        d = date.Date("2002-05-23 14:30:00") # Thursday
        dd = logic._due_date(d,d)
        self.assertEquals(dd.isodate(), "2002-05-28 23:59:59")  # Tuesday

    def test_Fairfax_due_date(self):
        """ Fairfax due date """
        # changed 2002.07.10
        logic = businesslogic.getbusinesslogic("OCC1", self.tdb, [])
        d = date.Date("2002-05-30 13:23:12") # Thursday
        dd = logic._legal_due_date(d)
        self.assertEquals(dd.isodate(), "2002-06-04 07:00:00")

    def test_FCO1_due_date(self):
        """ FCO1 due date """
        logic = businesslogic.getbusinesslogic("FCO1", self.tdb, [])
        t = {"ticket_type": "NORMAL", "transmit_date": "2002-08-12 14:13:00",
        "call_date": "2002-08-12 14:13:00",
         "work_date": "2002-08-11 08:00:00", "kind": "SOMETHING"}
        td = logic.legal_due_date(t)
        self.assertEquals(td, "2002-08-14 23:59:59")

        t["ticket_type"] += " MEET" # emulate a "meet" ticket
        td = logic.legal_due_date(t)
        self.assertEquals(td, "2002-08-11 23:59:59")

        t["ticket_type"] = "EMER BLABLA NORMAL"
        td = logic.legal_due_date(t)
        self.assertEquals(td, "2002-08-12 16:13:00")

    def test_NCA1_due_date(self):
        """ NCA1 due date """
        for call_center in ("NCA1", "SCA1"):
            logic = businesslogic.getbusinesslogic(call_center, self.tdb, [])
            t = {"ticket_type": "NORMAL",
             "call_date": "2002-08-12 14:13:00",
             "work_date": "2002-08-14 08:00:00", "kind": "SOMETHING"}
            # August 12th is a Monday
            td = logic.legal_due_date(t)
            self.assertEquals(td, "2002-08-14 14:13:00")

            t["work_date"] = "2002-08-15 09:00:00"
            td = logic.legal_due_date(t)
            self.assertEquals(td, "2002-08-15 09:00:00")

    def test_FMB1_due_date(self):
        """ FMB1 due date """
        # XXX kind of redundant, use test_businesslogic_due_dates.py instead

        logic = businesslogic.getbusinesslogic("FMB1", self.tdb, [])
        t = {"kind": "NORMAL", "transmit_date": "2002-11-04 12:13:14",
             "call_date": "2002-11-04 12:13:14",
             "ticket_type": "FOOBLITZKY"}
        td = logic.legal_due_date(t)
        self.assertEquals(td, "2002-11-06 23:59:59")

        t["transmit_date"] = "2002-11-04 18:13:25"
        td = logic.legal_due_date(t)
        self.assertEquals(td, "2002-11-06 23:59:59")

    def test_FOL1_due_date(self):
        """ Test FOL1 due date computation """
        logic = businesslogic.getbusinesslogic("FOL1", self.tdb, [])
        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "transmit_date": "2002-09-23 10:00:00", "kind": "NORMAL"}
        self.assertEquals(logic.due_date(t), "2002-09-25 10:00:00")

        t["transmit_date"] = "2002-10-01 10:00:00"
        self.assertEquals(logic.due_date(t), "2002-10-03 23:59:59")

    def test_FWP1_due_date(self):
        logic = businesslogic.getbusinesslogic("FWP1", self.tdb, [])
        row = {"transmit_date": "2003-03-07 12:13:14",  # a Friday
               "ticket_type": "NORMAL",
               "kind": "NORMAL",
              }
        d = logic.legal_due_date(row)
        self.assertEquals(d, "2003-03-11 12:13:14")

        row["kind"] = "EMERGENCY"
        d = logic.legal_due_date(row)
        self.assertEquals(d, "2003-03-07 15:13:14")

    def test_FWP3_due_date(self):
        logic = businesslogic.getbusinesslogic("FWP3", self.tdb, [])
        row = {"call_date": "2003-03-07 12:13:14",  # a Friday
               "ticket_type": "NORMAL",
               "kind": "NORMAL",
              }
        d = logic.legal_due_date(row)
        self.assertEquals(d, "2003-03-11 12:13:14")

    def test_FBL1_due_date(self):
        logic = businesslogic.getbusinesslogic("FBL1", self.tdb, [])
        t = {"ticket_type": "2NDQ-2ND REQUEST",
             "transmit_date": "2000-01-01 14:15:16",
             "kind": "NORMAL"}
        # 2NDQ-2ND REQUEST is treated the same as EMERGENCY
        self.assertEquals(logic.legal_due_date(t), "2000-01-01 18:15:16")

    def test_LQW1_due_date(self):
        logic = businesslogic.getbusinesslogic("LQW1", self.tdb, [])
        t = {"ticket_type": "2NDQ-2ND REQUEST",
             "call_date": "2004-04-12 12:00:00",
             "work_date": "2004-04-14 17:00:00",
             "kind": "NORMAL"}
        self.assertEquals(logic.legal_due_date(t), "2004-04-14 17:00:00")
        # depends on work_date now

    def test_6001_due_date(self):
        logic = businesslogic.getbusinesslogic('6001', self.tdb, [])
        t = {'ticket_type': 'NORMAL', 'call_date': '2004-07-12 13:00:00',
             'kind': 'NORMAL'}
        self.assertEquals(logic.legal_due_date(t), "2004-07-14 13:00:00")

        t['kind'] = 'EMERGENCY'
        self.assertEquals(logic.legal_due_date(t), "2004-07-12 15:00:00")

        t['ticket_type'] = 'DIG-UP SOME MORE'
        t['kind'] = 'NORMAL'
        self.assertEquals(logic.legal_due_date(t), "2004-07-12 15:00:00")

    def test_7501_due_date(self):
        logic = businesslogic.getbusinesslogic('7501', self.tdb, [])
        t = {'ticket_type': 'NORMAL', 'call_date': '2004-11-16 15:11:00',
             'kind': 'NORMAL'}
        self.assertEquals(logic.legal_due_date(t), '2004-11-19 23:59:59')
        # 2 days, to midnight

        t['kind'] = 'EMERGENCY'
        self.assertEquals(logic.legal_due_date(t), '2004-11-16 17:11:00')
        # 2 hours later

    def test_9301_due_date(self):
        logic = businesslogic.getbusinesslogic('9301', self.tdb, [])
        t = {'ticket_type': 'NORMAL', 'calldate': '2005-02-07 12:00:00',
             'transmit_date': '2005-02-07 12:02:00', 'kind': 'NORMAL'}
        self.assertEquals(logic.legal_due_date(t), '2005-02-10 07:00:00')

        t['kind'] = 'EMERGENCY'
        self.assertEquals(logic.legal_due_date(t), '2005-02-07 15:02:00')

    def test_9001_due_date(self):
        logic = businesslogic.getbusinesslogic('9001', self.tdb, [])
        t = {'ticket_type': 'NORMAL'}

    def test_FCV1_due_date(self):
        logic = businesslogic.getbusinesslogic('FCV1', self.tdb, [])
        t = {'ticket_type': 'NORMAL', 'call_date': '2005-02-07 12:00:00',
             'transmit_date': '2005-02-07 12:02:00', 'kind': 'NORMAL'}
        self.assertEquals(logic.legal_due_date(t), '2005-02-10 07:00:00')
        t['kind'] = 'EMERGENCY'
        self.assertEquals(logic.legal_due_date(t), '2005-02-07 14:00:00')

    def test_FPK1_due_date(self):
        logic = businesslogic.getbusinesslogic('FPK1', self.tdb, [])
        t = {'ticket_type': 'NORMAL', 'call_date': '2005-02-07 12:00:00',
             'transmit_date': '2005-02-07 12:02:00', 'kind': 'NORMAL'}
        self.assertEquals(logic.legal_due_date(t), '2005-02-10 07:00:00')
        t['kind'] = 'EMERGENCY'
        self.assertEquals(logic.legal_due_date(t), '2005-02-07 14:00:00')

    def test_1102_due_date(self):
        logic = businesslogic.getbusinesslogic('1102', self.tdb, [])
        # create a manual ticket... no due date or work date
        t = {'ticket_type': 'NORMAL', 'call_date': '2005-10-19 14:33:30',
             'transmit_date': '2005-10-19 16:42:40', 'kind': 'NORMAL',
             'ticket_type': 'NORMAL'}
        d = logic.legal_due_date(t)
        self.assertEquals(d, '2005-10-21 14:33:30')

        t['work_date'] = '2005-10-24 10:00:00'
        d = logic.legal_due_date(t)
        self.assertEquals(d, "2005-10-24 10:00:00")

        t['kind'] = 'EMERGENCY'
        d = logic.legal_due_date(t)
        self.assertEquals(d, "2005-10-19 16:33:30")

if __name__ == "__main__":
    unittest.main()
