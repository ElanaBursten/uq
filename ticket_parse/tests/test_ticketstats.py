# test_ticketstats.py

import site; site.addsitedir('.')
import unittest
#
import datadir
import parser
import ticketloader
import ticketparser
import ticketstats

class TestTicketStats(unittest.TestCase):

    def testTicketStats1(self):
        from formats import Atlanta
        loader = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        stats = ticketstats.TicketStats()

        ticket = loader.getTicket()
        while ticket:
            a = Atlanta.Parser(ticket, 'Atlanta')
            a.parse()
            stats.feed(a.ticket)
            ticket = loader.getTicket()

        # The following results are extracted from atlanta-1.txt "by hand".
        self.assertEquals(stats.stats["caller"][0], 18)

        # ...more tests can be added if necessary...

    def test_schema_check(self):
        tl = ticketloader.TicketLoader('../testdata/Atlanta-1.txt')
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['Atlanta']) # does a check

        toolong = t.check_field_lengths()
        self.assertEquals(toolong, [])

        # give the work_state field a value that's too long
        t.work_state = 'XYZ'
        toolong = t.check_field_lengths()
        self.assertEquals(toolong, [('work_state', 2, 3, 'XYZ')])
        # field, allowed length, actual length, value

        t.work_state = 'GA'
        t.locates[0].client_code = 'THISISTOOLONG'
        toolong = t.locates[0].check_field_lengths()
        self.assertEquals(toolong, [('client_code', 10, 13, 'THISISTOOLONG')])



if __name__ == "__main__":

    unittest.main()

