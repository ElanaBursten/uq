# test_attachment.py

from __future__ import with_statement
import os
import site; site.addsitedir('.')
import string
import sys
import unittest
#
import attachment
import ticket_db
from mockery import *

class TestAttachment(unittest.TestCase):

    def test_read_and_write(self):
        data = string.join([chr(i) for i in range(0, 256)], "")
        fname = "$1$.txt"
        attachment.write_attachment(fname, "foobar.dat", data)

        filename, out = attachment.read_attachment("$1$.txt")
        self.assertEquals(filename, "foobar.dat")
        self.assertEquals(data, out)

        f = open(fname, 'r')
        lines = f.readlines()
        f.close()
        text = string.join(lines,'')
        filename, out = attachment.read_attachment_from_text(text)
        self.assertEquals(filename, "foobar.dat")
        self.assertEquals(data, out)
        os.remove(fname)

    def test_create_attachment_record(self):
        tdb = ticket_db.TicketDB()
        tdb.delete("attachment") # make sure table is empty
        att_id = attachment.create_attachment_record(tdb,
                 id=123456, # ?
                 upload_filename="upload_filename.txt",
                 orig_filename="orig_filename.txt",
                 extension=".txt",
                 size=1024,
                 user=210)
        self.assert_(att_id)

        ar = tdb.getrecords("attachment", attachment_id=att_id)[0]
        self.assertEquals(ar['foreign_id'], '123456')
        self.assertEquals(ar['filename'], 'upload_filename.txt')
        self.assertEquals(ar['extension'], '.txt')

    def test_gen_unique_filename(self):
        fname = attachment.gen_unique_filename(1234, ".txt")
        self.assert_(fname.startswith("T1234"))
        self.assertEqual(fname[-4:],".txt")
        fname = attachment.gen_unique_filename("1234", ".txt")
        self.assert_(fname.startswith("T1234"))
        self.assertEqual(fname[-4:],".txt")
        fname = attachment.gen_unique_filename("1234", "txt")
        self.assert_(fname.startswith("T1234"))
        self.assertEqual(fname[-4:],".txt")

if __name__ == "__main__":
    unittest.main()

