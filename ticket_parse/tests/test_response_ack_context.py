# test_response_ack_context.py

import site; site.addsitedir('.')
import os
import unittest
#
from ftpresponder import ResponderDBStorage
import _testprep
import datadir
import date
import mockery
import response_ack_context
import ticket_db
import ticketloader
import ticketparser

# sample record as returned by get_pending_responses, augmented with resp_id.
SAMPLE_LOCATE = {
  u'client_code': 'KCBA01',
  u'closed_date': None,
  u'contact': '',
  u'due_date': None,
  u'first_arrival_date': None,
  u'insert_date': '2012-06-09 07:00:02',
  u'locate_id': '185562',
  'resp_id': '65411',
  u'serial_number': None,
  u'service_area_code': None,
  u'status': '-P',
  u'ticket_format': 'FDE1',
  u'ticket_id': '38394',
  u'ticket_number': '01069281',
  u'ticket_type': 'SOMETHING',
  u'work_city': 'SMYRNA',
  u'work_county': 'KENT',
  u'work_date': '2003-02-20 12:00:00',
  u'work_state': 'DE'}

FDE1_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FDE1', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {'-P': ('1', ''), 'M': ('ispaint', '')},
    'outgoing_dir': '', 'clients': [], 'skip': [],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}


class TestResponseAckContext(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_response_ack_context(self):
        log = mockery.MockObject()
        rs = ResponderDBStorage(log, self.tdb)

        # since the database is involved here, we're going to need to set up
        # actual ticket, locate and response_log records:

        # post a ticket w/ locates
        tl = ticketloader.TicketLoader('../testdata/Delaware1-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        t.locates[0].client_code = 'KCBA01'
        t.ticket_number = '01069281'
        del t.locates[1:] # only keep one locate for simplicity
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # insert record into response_log
        self.tdb.insertrecord('response_log',
         status='C',
         response_date=date.Date().isodate(),
         locate_id=locate_id,
         call_center='FDE1',
         success=0, # will be 1 when result is received
         response_sent="blah",
         reply="(ftp waiting)",
         )

        rlogs = self.tdb.getrecords('response_log')
        self.assertEquals(len(rlogs), 1)

        # take the example row and update it to match the actual IDs
        row = SAMPLE_LOCATE.copy()
        row['ticket_id'] = locates[0]['ticket_id']
        row['locate_id'] = locates[0]['locate_id']
        row['resp_id'] = rlogs[0]['response_id']
        row['status'] = 'C'

        # we pretend we sent responses in a file called example2.xml
        rs.store([row], 'FDE1', "example2.xml")

        # try storing it
        ctx = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(ctx), 1)

        # retrieve it again
        rctx = rs.load('FDE1', 'example2.xml')[0]
        self.assertEquals(rctx.ticket_id, row['ticket_id'])
        self.assertEquals(rctx.locate_id, row['locate_id'])
        self.assertEquals(rctx.filename, 'example2') # '.xml' is stripped

        # delete it
        rs.delete('FDE1', 'example2.xml')
        self.assertEquals(len(self.tdb.getrecords('response_ack_context')), 0)


if __name__ == "__main__":
    unittest.main()

