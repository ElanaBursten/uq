# test_responder_clear_queue.py

import unittest
import site; site.addsitedir('.')
#
import config
import responder_clear_queue
import ticket_db
import ticketloader
import ticketparser
import _testprep

class TestResponderClearQueue(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

        _testprep.clear_database(self.tdb)
        self.tdb.deleterecords('responder_queue')

        #cfg = config.getConfiguration()
        cfg = config.reload()
        cfg.xcelresponders['FCO1'] = {
            'name': 'FCO1',
            'output_dir': '.',
            'termids': ['FOO01', 'FOO02'],
            'recipients': [{'name': 'Example User', 'email': 'email@email.com'},
                           {'name': 'XYZ', 'email': 'xyz@xyz.com'}],
        }
        if 'FCO1' not in cfg.nodelete:
            cfg.nodelete.append('FCO1')

    def store_tickets(self, filename, format):
        before = len(self.tdb.getrecords('locate'))
        tl = ticketloader.TicketLoader(filename)
        for raw in tl.tickets:
            t = self.handler.parse(raw, [format])
            self.tdb.insertticket(t, whodunit='parser')
        after = len(self.tdb.getrecords('locate'))

        return after - before   # number of locates inserted

    def test_adhoc(self):
        # store a number of tickets and locates
        num_atlanta = self.store_tickets('../testdata/Atlanta-1.txt', 'Atlanta')
        num_colorado = self.store_tickets('../testdata/Colorado2-1.txt', 'Colorado')
        num_orlando = self.store_tickets('../testdata/Orlando-1.txt', 'Orlando')

        # add an Atlanta locate that was not added by parser
        rows = self.tdb.runsql_result("""
          select top 1 l.* from locate l, ticket t
          where l.ticket_id = t.ticket_id
          and t.ticket_format = 'Atlanta'
        """)
        self.tdb.runsql("""
          insert into locate
          (ticket_id, added_by, status, client_id)
          values (%s, 'BOGUS', '-P', %s)
        """ % (rows[0]['ticket_id'], rows[0]['client_id'] or 'NULL'))

        # force insertion in responder_queue
        sql = "update locate set status='O'"
        self.tdb.runsql(sql)

        rows = self.tdb.getrecords("responder_queue")
        self.assertEquals(num_atlanta + num_colorado + num_orlando, 216)
        self.assertEquals(len(rows), 217) # 216 + extra one

        cfg = config.getConfiguration()
        cfg.xcelresponders['FCO1']['termids'].append('CMSDC00')
        crc = responder_clear_queue.ClearResponderQueue(verbose=0)
        deleted, skipped = crc.clear_queue()
        # should delete the locate with added_by != 'PARSER'

        rows = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(rows), 7) # NOT 8
        self.assertEquals(len(deleted), 217 - 7)
        self.assertEquals(len(skipped), 7)

        xcel_termids = cfg.xcelresponders['FCO1']['termids']

        # things to check:
        # - some Atlanta should still be in the queue (USW01)
        # - Orlando should not be in the queue (none of them)
        # - Colorado: CMSDC00 and other 'protected' term ids should still be
        #   there, others should be gone
        num_atlanta_deleted = num_atlanta_skipped = 0
        num_orlando_deleted = num_orlando_skipped = 0
        num_colorado_deleted = num_colorado_skipped = 0

        for row in deleted:
            if row['ticket_format'] == 'Atlanta':
                num_atlanta_deleted += 1
            elif row['ticket_format'] == 'FOL1':
                num_orlando_deleted += 1
            elif row['ticket_format'] == 'FCO1':
                if row['client_code'] in xcel_termids:
                    self.fail("FCO1 XCEL termid %s should not be deleted" %
                      row['client_code'])
                else:
                    num_colorado_deleted += 1

        for row in skipped:
            if row['ticket_format'] == 'Atlanta':
                num_atlanta_skipped += 1
            elif row['ticket_format'] == 'FOL1':
                num_orlando_skipped += 1
            elif row['ticket_format'] == 'FCO1':
                if row['client_code'] not in xcel_termids:
                    self.fail("FCO1 termid %s should have been deleted" %
                      row['client_code'])
                else:
                    num_colorado_skipped += 1

        self.assertEquals(num_atlanta_deleted, 53+1)
        self.assertEquals(num_atlanta_skipped, num_atlanta - 53)
        self.assertEquals(num_orlando_skipped, 0)
        self.assertEquals(num_orlando_deleted, num_orlando)
        self.assert_(num_colorado_deleted > 0)
        self.assert_(num_colorado_skipped > 0)

        # get responder_queue rows + added_by field
        rows = self.tdb.runsql_result("""
          select rq.*, l.added_by
          from responder_queue rq
          inner join locate l on l.locate_id = rq.locate_id
        """)
        self.assertEquals(len(rows), 7)
        self.assertTrue(all(row['added_by'].upper() == 'PARSER'
                            for row in rows))

    def tearDown(self):
        cfg = config.getConfiguration()
        z = cfg.xcelresponders['FCO1']['termids']
        if 'CMSDC00' in z:
            z.remove('CMSDC00')



if __name__ == "__main__":

    unittest.main()
