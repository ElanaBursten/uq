# mock_pop3.py

import os
import poplib

POP3_ACCOUNTS = {}


def add_account(server, username):
    global POP3_ACCOUNTS
    if (server, username) not in POP3_ACCOUNTS:
        POP3_ACCOUNTS[server, username] = []


def add_messages_to_account(server, username, msg_filenames, msg_file_path=''):
    msgs = []
    for i, msg_filename in enumerate(msg_filenames):
        lines = []
        msg_file = open(os.path.join(msg_file_path, msg_filename), 'r')
        try:
            for line in msg_file:
                if line[-1:] == '\n':
                    line = line[:-1]
                lines.append(line)
        finally:
            msg_file.close()
        msgs.append(['uid' + str(i + 1), lines, 0])               

    global POP3_ACCOUNTS
    POP3_ACCOUNTS[server, username] += msgs


class MockSocket:
    def settimeout(self, timeout):
        pass

class MockPOP3:
    def __init__(self, host, port=110):
        self.host = host
        self.port = port
        self.sock = MockSocket()
        if not (self.host and self.port):
            raise socket.error('Host or port not specified')
        self.username = ''
        self.message_list = []
        
    @staticmethod
    def getwelcome():
        return '+OK POP3 server ready'

    def user(self, username):
        try:
            self.message_list = POP3_ACCOUNTS[(self.host, username)]
            for i in range(len(self.message_list)):
                self.message_list[i][2] = 0
            self.username = username
            return '+OK name is a valid mailbox'
        except:
            self.username = ''
            raise poplib.error_proto("-ERR never heard of mailbox name '%s'" %
                                     username)

    def pass_(self, pswd):
        if self.username:
            return '+OK maildrop locked and ready'
        elif 0:
            raise poplib.error_proto('-ERR invalid password')
        else:
            raise poplib.error_proto('-ERR unable to lock maildrop')

    def valid_message_num(self, which):
        if which.isdigit() and (0 < int(which) <= len(self.message_list)):
            return True
        return False            
            
    def retr(self, which):
        if self.valid_message_num(which):
            _, lines, deleted = self.message_list[int(which) - 1]
            if not deleted:
                return ['+OK message follows', lines, 999]
        raise poplib.error_proto('-ERR no such message')

    def dele(self, which):
        if self.valid_message_num(which):
            message = self.message_list[int(which) - 1]
            deleted = message[2]
            if not deleted:
                message[2] = 1
                return '+OK message deleted'
        raise poplib.error_proto('-ERR no such message')

    def quit(self):
        for i in range(len(self.message_list) - 1, -1, -1):
            if self.message_list[i][2]:
                self.message_list.pop(i)    
        return '+OK'

    def uidl(self, which=None):
        if which:
            raise 'Not implemented'
            
        uid_list = []
        for i in range(len(self.message_list)):
            if not self.message_list[i][2]:
                uid_list.append('%s %s' % (i + 1, self.message_list[i][0]))
        return ['+OK unique-id listing follows', uid_list, 999]
        # else: raise poplib.error_proto('-ERR no such message')


class MockPOP3_SSL(MockPOP3):
    def __init__(self, host, port=995):
        MockPOP3.__init__(self, host, port)
