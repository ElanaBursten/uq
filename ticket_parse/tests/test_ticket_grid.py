# test_ticket_grid.py

import site; site.addsitedir('.')
import unittest
#
import client
import date
import locate
import ticket as ticketmod
import ticket_db
import ticket_grid
import ticketloader
import ticketparser
import tools
import _testprep

class TestTicketGrid(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_grid_parsing(self):
        tl = ticketloader.TicketLoader('../testdata/NorthCarolina-1.txt')
        idx = tools.findfirst(tl.tickets, lambda s: s.find('Ticket : C0032043') > -1)
        raw = tl.tickets[idx]
        raw = raw.replace("3512A8106B", "3512A8106B-")  # trailing - should go

        t = self.handler.parse(raw, ["NorthCarolina"])
        self.assertEquals(len(t.grids), 9)
        self.assertEquals(t.grids[0], "3512A8106B")
        self.assertEquals(t.grids[-1], "3512C8106D")

        # this is the first format that parses all grids.  Other grids test
        # should be done in test_ticketparser.py.

    def test_insert_grids(self):
        # test inserting, retrieving and updating of grids

        # insert a test ticket
        t = ticketmod.Ticket()
        t.ticket_number = "123456789"
        t.image = "blah"
        ticket_id = self.tdb.insertticket(t)

        grids_before = ["ABC", "DEF", "GHI"]
        ticket_grid.insert_grids(self.tdb, ticket_id, grids_before)
        grids_after = ticket_grid.get_grids(self.tdb, ticket_id)
        self.assertEquals(grids_before, grids_after)

        grids_update = grids_before[1:]
        grids_update.extend(["JKL", "MNO"])
        ticket_grid.update_grids(self.tdb, ticket_id, grids_before, grids_update)
        grids4 = ticket_grid.get_grids(self.tdb, ticket_id)
        self.assertEquals(grids4, grids_before + ["JKL", "MNO"])

    def test_getticket(self):
        t = ticketmod.Ticket()
        t.grids = ["ABC", "DEF", "GHI"]
        t.ticket_number = "123456789"
        t.image = "blah"
        ticket_id = self.tdb.insertticket(t)

        t2 = self.tdb.getticket(ticket_id)
        self.assert_(t2.grids, "t2.grids should not be empty")
        self.assertEquals(t2.grids, t.grids)

        # update... value of t.grids is irrelevant
        self.tdb.updateticket(ticket_id, t, update_grids=["JKL", "MNO"])
        t3 = self.tdb.getticket(ticket_id)
        self.assertEquals(t3.grids, ["ABC", "DEF", "GHI", "JKL", "MNO"])


if __name__ == "__main__":

    unittest.main()

