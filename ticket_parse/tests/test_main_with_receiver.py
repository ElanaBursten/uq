# test_main_with_receiver.py

from __future__ import with_statement
import copy
import os
import poplib
import shutil
import site; site.addsitedir('.')
import sys
import unittest
#
import config
import filetools
import main
import mail2
import receiver
import ticket_db
import _testprep
#
from mockery import *

this_module = sys.modules[__name__]

POP3_ACCOUNTS = {}

def add_pop3_account(server, username, msg_filenames, msg_file_path_root=''):
    msgs = []
    for i in range(len(msg_filenames)):
        lines = []
        msg_file = open(os.path.join(msg_file_path_root, msg_filenames[i]), 'r')
        try:
            for line in msg_file:
                if line[-1:] == '\n':
                    line = line[:-1]
                lines.append(line)
        finally:
            msg_file.close()
        msgs.append(['uid' + str(i + 1), lines, 0])               
            
    global POP3_ACCOUNTS
    POP3_ACCOUNTS[server, username] = msgs

class Mock_socket:
    def settimeout(self, timeout):
        pass

class MockPOP3:
    def __init__(self, host, port=110):
        self.host = host
        self.port = port
        self.sock = Mock_socket()
        if not (self.host and self.port):
            raise socket.error('Host or port not specified')
        self.username = ''
        self.message_list = []
        
    def getwelcome(self):
        return '+OK POP3 server ready'

    def user(self, username):
        try:
            self.message_list = POP3_ACCOUNTS[(self.host, username)]
            for i in range(len(self.message_list)):
                self.message_list[i][2] = 0
            self.username = username
            return '+OK name is a valid mailbox'
        except:
            self.username = ''
            raise poplib.error_proto('-ERR never heard of mailbox name')

    def pass_(self, pswd):
        if self.username:
            return '+OK maildrop locked and ready'
        elif 0:
            raise poplib.error_proto('-ERR invalid password')
        else:
            raise poplib.error_proto('-ERR unable to lock maildrop')

    def valid_message_num(self, which):
        if which.isdigit() and (0 < int(which) <= len(self.message_list)):
            return True
        return False            
            
    def retr(self, which):
        if self.valid_message_num(which):
            _, lines, deleted = self.message_list[int(which) - 1]
            if not deleted:
                return ['+OK message follows', lines, 999]
        raise poplib.error_proto('-ERR no such message')

    def dele(self, which):
        if self.valid_message_num(which):
            message = self.message_list[int(which) - 1]
            deleted = message[2]
            if not deleted:
                message[2] = 1
                return '+OK message deleted'
        raise poplib.error_proto('-ERR no such message')

    def quit(self):
        for i in range(len(self.message_list) - 1, -1, -1):
            if self.message_list[i][2]:
                self.message_list.pop(i)    
        return '+OK'

    def uidl(self, which=None):
        if which:
            raise 'Not implemented'
            
        uid_list = []
        for i in range(len(self.message_list)):
            if not self.message_list[i][2]:
                uid_list.append('%s %s' % (i + 1, self.message_list[i][0]))
        return ['+OK unique-id listing follows', uid_list, 999]
        # else: raise poplib.error_proto('-ERR no such message')

class MockPOP3_SSL(MockPOP3):
    def __init__(self, host, port=995):
        MockPOP3.__init__(self, host, port)

class TestMainWithReceiver(unittest.TestCase):

    DIRS = ["incoming", "processed", "error", "attachments", "attachments_proc"]

    def setUp(self):
        # make test directories
        for dir in self.DIRS:
            try:
                os.mkdir("test-" + dir)
            except:
                pass
        try:
            os.mkdir('test-incoming/receiver-temp')
        except:
            pass

        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

        self._setup_FAU5()
        self._setup_LAM01()
        self._setup_3003()
        import call_centers; call_centers.get_call_centers(self.tdb).reload()

        self.conf_copy = copy.deepcopy(config.getConfiguration())

        # set up configuration for FAU5, with separate directories
        self.conf_copy.processes = [
            {'incoming': 'test-incoming',
             'format': 'FAU5',
             'processed': 'test-processed',
             'error': 'test-error',
             'attachments': '',
             'accounts': [{'server': 'this',
                           'username': 'that',
                           'destination': 'test-incoming',
                           'call_center': 'FAU5',
                           'attachment_dir': 'attdir',
                           'use_ssl': 0,
                           'password': 'so-n-so'}
                         ],
            },
            {'incoming': 'test-incoming',
             'format': 'LAM01',
             'processed': 'test-processed',
             'error': 'test-error',
             'attachments': 0,
             'attachment_dir': 'test-attachments',
             'attachment_proc_dir': 'test-attachments_proc',
             'upload_location': '',              
             'attachment_user': '',  # Leaving blank should disable attachment
                                     # processing in the parser           
             'accounts': [{'destination': 'test-incoming',
                           'call_center': 'LAM01',
                           'attachment_dir': 'test-attachments',
                           'server': 'this',
                           'username': 'that',
                           'password': 'so-n-so',
                           'full': True,
                           'store_attachments': True,
                           'ignore_attachments': False,
                           'file_ext': '.xml',
                           'aggregate_file': '',
                           'use_ssl': False,
                           'port': 110}
                         ]
            },
            {'incoming': 'test-incoming',
             'format': '3003',
             'processed': 'test-processed',
             'error': 'test-error',
             'attachments': 0,
             'attachment_dir': 'test-attachments',
             'attachment_proc_dir': 'test-attachments_proc',
             'upload_location': '',              
             'attachment_user': '',  # Leaving blank should disable attachment
                                     # processing in the parser           
             'accounts': [],
             'db_accounts': [{'type': 'db',
                              'destination': 'test-incoming',
                              'call_center': '3003',
                              'attachment_dir': 'test-attachments'}]
            }
        ]

        _testprep.delete_utility_ini()

    # FIXME
    def _setup_FAU5(self):
        # make sure we have a call center FAU5
        _testprep.add_test_call_center(self.tdb, 'FAU5')
        self.tdb.runsql("""
          update call_center
          set active=1 where cc_code = 'FAU5'
        """)
        # make sure it has one client called ...
        #_testprep.add_test_client(self.tdb, 'LAMBERT01', 'LAM01')

    def _setup_LAM01(self):
        # make sure we have a call center LAM01
        _testprep.add_test_call_center(self.tdb, 'LAM01')
        self.tdb.runsql("""
          update call_center
          set active=1 where cc_code = 'LAM01'
        """)

    def _setup_3003(self):
        # make sure we have a call center 3003
        _testprep.add_test_call_center(self.tdb, '3003')
        self.tdb.runsql("""
          update call_center
          set active=1 where cc_code = '3003'
        """)
        # todo(dan) Make sure clients exist?

    def tearDown(self):
        # remove test directories
        for dir in self.DIRS:
            try:
                shutil.rmtree("test-" + dir)
            except:
                pass
        _testprep.delete_utility_ini()

    def test_001(self):

        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "AGL-2010-11-02.txt")
        raw_email_data = filetools.read_file_lines(path)
        raw_email_data = [s.rstrip('\n') for s in raw_email_data]

        # mock POP3 server
        # feed it raw email file
        class MockEmailReceiver(receiver.EmailReceiver):
            count = 0 # keep track so we only return this email once
            def connect(self): return True
            def disconnect(self): pass
            def delete(self, n): pass
            def get_next_email(self): 
                self.__class__.count += 1
                if self.count > 1:
                    return '', '', []
                else:
                    return '1', 'uid-1', raw_email_data

        # have main.py (via receiver) fetch the email/tickets
        # process ticket as usual
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            with Mockery(receiver, 'EmailReceiver', MockEmailReceiver):
                m = main.Main(verbose=0, call_center='FAU5')
                m.log.lock = 1
                m.run(runonce=1)

        # verify
        tix = self.tdb.getrecords('ticket', ticket_format='FAU5')
        self.assertEquals(len(tix), 1)

        locs = self.tdb.getrecords('locate', ticket_id=tix[0]['ticket_id'])
        self.assertEquals(len(locs), 8)

    def test_002(self):
        """ Test with 0 and 1 messages waiting """ 
    
        # Verify there are no tickets
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 0)
        
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy),\
         Mockery(poplib, 'POP3', MockPOP3),\
         Mockery(poplib, 'POP3_SSL', MockPOP3_SSL),\
         Mockery(mail2, 'TESTING', 1):
            path = os.path.join(_testprep.TICKET_PATH, "receiver")
            mail2._test_mail_sent = []
            
            # Add an empty mailbox
            add_pop3_account('this', 'that', [], path)
            m = main.Main(verbose=0, call_center='FAU5')
            m.run(runonce=1)

            # Verify no messages left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 0)
            
            # Verify no tickets added
            tix = self.tdb.getrecords('ticket')
            self.assertEquals(len(tix), 0)

            # Add 1 message
            add_pop3_account('this', 'that', ['AGL-2010-11-02.txt'], path)
            m = main.Main(verbose=0, call_center='FAU5')
            m.run(runonce=1)

            # Verify no messages left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 0)

            # Verify 1 ticket added
            tix = self.tdb.getrecords('ticket', ticket_format='FAU5')
            self.assertEquals(len(tix), 1)

    def test_003(self):
        """ Test with 3 messages waiting """ 
    
        # Verify there are no tickets
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 0)
        
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy),\
         Mockery(poplib, 'POP3', MockPOP3),\
         Mockery(poplib, 'POP3_SSL', MockPOP3_SSL),\
         Mockery(mail2, 'TESTING', 1):
            path = os.path.join(_testprep.TICKET_PATH, "receiver")
            mail2._test_mail_sent = []
            
            # Add 3 messages
            add_pop3_account('this', 'that', ['AGL-2010-11-02.txt',
             'AGL-2010-11-02-modified-1.txt', 'AGL-2010-11-02-modified-2.txt'],
             path)
            m = main.Main(verbose=0, call_center='FAU5')
            m.run(runonce=1)

            # Verify no messages left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 0)
            
            # Verify 3 tickets added
            tix = self.tdb.getrecords('ticket', ticket_format='FAU5')
            self.assertEquals(len(tix), 3)
            
    def test_004(self):
        """ Test with 3 messages waiting, and #2 having no header separator """

        # Verify there are no tickets
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 0)
        
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy),\
         Mockery(poplib, 'POP3', MockPOP3),\
         Mockery(poplib, 'POP3_SSL', MockPOP3_SSL),\
         Mockery(mail2, 'TESTING', 1):
            path = os.path.join(_testprep.TICKET_PATH, "receiver")
            mail2._test_mail_sent = []

            # Add 3 messages. #2 has no header separator.
            add_pop3_account('this', 'that', ['AGL-2010-11-02.txt',
             'AGL-2010-11-02-modified-3.txt', 'AGL-2010-11-02-modified-1.txt'],
             path)
            m = main.Main(verbose=0, call_center='FAU5')
            with self.assertRaises(receiver.HeaderDelimeterError):
                m.run(runonce=1)

            # Verify messages with uid = 'uid2' and 'uid3' left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 2)
            self.assertEquals(POP3_ACCOUNTS[('this', 'that')][0][0], 'uid2')
            self.assertEquals(POP3_ACCOUNTS[('this', 'that')][1][0], 'uid3')

            # Verify 1 ticket added
            tix = self.tdb.getrecords('ticket', ticket_format='FAU5')
            self.assertEquals(len(tix), 1)

            # Try again
            with self.assertRaises(receiver.HeaderDelimeterError):
                m.run(runonce=1)

            # Verify message with uid = 'uid2' and 'uid3' still left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 2)
            self.assertEquals(POP3_ACCOUNTS[('this', 'that')][0][0], 'uid2')
            self.assertEquals(POP3_ACCOUNTS[('this', 'that')][1][0], 'uid3')

    def test_005(self):
        """ Test with 3 messages waiting, and #2 having no parsable part """ 
        
        # Verify there are no work orders
        tix = self.tdb.getrecords('work_order')
        self.assertEquals(len(tix), 0)
        
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy),\
         Mockery(poplib, 'POP3', MockPOP3),\
         Mockery(poplib, 'POP3_SSL', MockPOP3_SSL),\
         Mockery(mail2, 'TESTING', 1):
            path = os.path.join(_testprep.TICKET_PATH, "receiver")
            mail2._test_mail_sent = []

            # Add 3 messages. #2 has no parsable part
            add_pop3_account('this', 'that', ['LAM01-attachment-1.txt',
             'LAM01-attachment-1-modified.txt', 'LAM01-attachments-2.txt'],
             path)
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

            # Verify message with uid = 'uid2' left
            self.assertEquals(len(POP3_ACCOUNTS[('this', 'that')]), 1)
            self.assertEquals(POP3_ACCOUNTS[('this', 'that')][0][0], 'uid2')

            # Verify 2 work orders added
            work_orders = self.tdb.getrecords('work_order', wo_source='LAM01')
            self.assertEquals(len(work_orders), 2)

    def _add_incoming_item(self, call_center, item_type, item_text):
        self.tdb.runsql("""
         insert incoming_item (cc_code, item_type, item_text)
         values ('%s', %d, '%s')
         """ % (call_center, item_type, item_text))

    def test_006(self):
        """
        Test receiving from the incoming_items table
        * 1 ticket, 2 messages waiting
        * 3003 configured for db account only
        """

        self._add_incoming_item('3003', 3, filetools.read_file(
         os.path.join(_testprep.TICKET_PATH, 'messages',
         'GeorgiaXML-2013-11', 'sample_message.xml')))
        self._add_incoming_item('3003', 1, filetools.read_file(
         os.path.join(_testprep.TICKET_PATH, 'tickets',
         'GeorgiaXML-2013-11', 'Emergency Notice.xml.txt')))
        self._add_incoming_item('3003', 3, 'test message 2')
        self._add_incoming_item('3003', 1, filetools.read_file(
         os.path.join(_testprep.TICKET_PATH, 'tickets',
         'GeorgiaXML-2013-11', 'Normal Notice.xml.txt')))
        self._add_incoming_item('3003', 2, filetools.read_file(
         os.path.join(_testprep.TICKET_PATH, 'audits',
         'GeorgiaXML-2013-11', 'Audit.xml.txt')))
        self.assertEquals(len(self.tdb.getrecords('incoming_item')), 5)

        # Have main.py (via receiver) fetch the incoming_item, and process as
        # usual
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy), \
         Mockery(mail2, 'TESTING', 1):
            m = main.Main(verbose=0, call_center='3003')
            m.log.lock = 1
            m.run(runonce=1)

        # Verify incoming_item's deleted
        self.assertEquals(len(self.tdb.getrecords('incoming_item')), 0)

        # Verify 2 tickets in db
        tix = self.tdb.getrecords('ticket', ticket_format='3003')
        self.assertEquals(len(tix), 2)

        # Verify 5 locates in db for first ticket
        self.assertEquals(len(self.tdb.getrecords('locate',
         ticket_id=tix[0]['ticket_id'])), 5)

        # Verify 10 locates in db for 2nd ticket
        self.assertEquals(len(self.tdb.getrecords('locate',
         ticket_id=tix[1]['ticket_id'])), 10)

        # Verify 1 summary in db
        self.assertEquals(len(self.tdb.getrecords('summary_header',
         call_center='3003')), 1)

if __name__ == "__main__":
    unittest.main()

