# test_fix_due_dates.py
# Created: 2002.10.30

import unittest
import site; site.addsitedir('.')
#
import config
import fix_due_dates
import ticketloader
import ticketparser
import ticket_db
import _testprep

class TestFixDueDates(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def test_000_prepare(self):
        #cfg = config.getConfiguration()
        #cfg.cc_routing_file = "rules/test_cc_routing_list.tsv"
        _testprep.set_cc_routing_file()


    def test_001(self):
        """ Test if invalid due dates get fixed """
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)
        # note that no due date is set (main.py normally does this)
        # therefore the fixer should flag it as incorrect, and update it

        fixer = fix_due_dates.TicketFixer("2001-01-01", "2002-12-31",
                call_center="Atlanta", verbose=0)
        fixer.run()

        # what did the fixer update?
        self.assertEquals(len(fixer.updates), 1)
        self.assertEquals(fixer.updates[0][0], id)

        # has the ticket been updated?
        results = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(len(results), 1)
        self.assert_(results[0]["due_date"])



if __name__ == "__main__":

    unittest.main()

