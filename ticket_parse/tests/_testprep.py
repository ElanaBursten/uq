# _testprep.py

import ticketloader
import ticketparser
#
import os
import shutil
import uuid

# determine location of test data
whereami = os.path.dirname(os.path.abspath(__file__))
TICKET_PATH = os.path.abspath(os.path.join(whereami, "..", "testdata"))


# these directories need to be present for some tests (esp. test_main.py)
TEST_DIRECTORIES = [
     "test_incoming", "test_processed", "test_error",
     "test_incoming_FCL2", "test_processed_FCL2", "test_error_FCL2",
     "test_incoming_FAM1", "test_processed_FAM1", "test_error_FAM1",
     "test_incoming_LAK1", "test_processed_LAK1", "test_error_LAK1",
     "test_attachments_LAK1", "test_attachment_proc_dir_LAK1",
     "test_incoming_FWP2", "test_processed_FWP2", "test_error_FWP2",
]

# tables that will be cleared before each test (by test cases that use the
# database, at least).  order does matter.
TABLES_TO_BE_CLEARED = [
    "employee_history",
    "summary_fragment",
    "jobsite_arrival",
    "response_ack_context",
    "responder_queue",
    "responder_multi_queue",
    "response_log",
    "response_ack_waiting",
    "ticket_snap",
    "ticket_ack",
    "ticket_hp",
    "ticket_version",
    "ticket_grid",
    "ticket_grid_log",
    "ticket_activity_ack",
    "work_order_ticket",
    "notes",
    "assignment",
    "locate_snap",
    "locate_plat",
    "locate_status",
    "locate",
    "ticket",
    "summary_detail",
    "summary_header",
    "response_ack_waiting",
    "alert_ticket_keyword",
    "alert_ticket_type",
    "work_order_version",
    "wo_response_log",
    "wo_responder_queue",
    "wo_status_history",
    "wo_audit_detail",
    "wo_audit_header",
    "wo_assignment",
    "work_order",
    "incoming_item",
    "event_log"
]

def create_test_dirs():
    for dir in TEST_DIRECTORIES:
        if not os.path.exists(dir):
            os.mkdir(dir)
        clear(dir)

def clear(dir, strict=0):
    """ Empty a directory.  (For now, this assumes that there are no
        subdirectories. """
    if os.path.exists(dir) or strict:
        try:
            filenames = os.listdir(dir)
        except WindowsError:
            import time; time.sleep(0.5)
            filenames = os.listdir(dir) # try again
        for filename in filenames:
            fullname = os.path.join(dir, filename)
            os.remove(fullname)

def delete_test_dirs():
    for dir in TEST_DIRECTORIES:
        shutil.rmtree(dir)

def set_cc_routing_file():
    import config
    cfg = config.getConfiguration()
    cfg.cc_routing_file = os.path.abspath(os.path.join(TICKET_PATH, "rules",
                          "test_cc_routing_list.tsv"))
    # using an absolute path will stop BusinessLogic from looking for this file
    # in the data dir, which is undesirable when testing

###
### create test records

def add_test_employee(tdb, testname, first_name='', last_name=''):
    rows = tdb.getrecords('employee', short_name=testname)
    if rows:
        assert len(rows) == 1, "Too many test employees"
        new_emp_id = rows[0]['emp_id']
    else:
        # add a test locator
        max_emp_id = tdb.dbado.get_max_id('employee', 'emp_id')
        rows = tdb.dbado.getrecords('profit_center')
        sql = """
         insert employee (type_id, emp_number, short_name, active,
           can_receive_tickets, repr_pc_code, payroll_pc_code,
           first_name, last_name)
         values (1015, '666-666', '%s', 1, 1,'%s','%s', '%s', '%s')
        """ % (testname, rows[0]['pc_code'], rows[0]['pc_code'], first_name,
               last_name)
        tdb.dbado.runsql(sql)
        new_emp_id = tdb.dbado.get_ids_after('employee', 'emp_id', max_emp_id)[0]
    return new_emp_id

def delete_test_employee(tdb, testname):
    sql = """
     delete from employee where short_name = '%s'
    """ % (testname,)
    try:
        tdb.runsql(sql)
    except:
        pass

def set_employee_active(db, testname):
    sql = """
     update employee
     set active=1, can_receive_tickets=1
     where short_name = '%s'
    """ % (testname,)
    db.runsql(sql)

def set_employee_inactive(tdb, testname):
    sql = """
     update employee
     set active=0, can_receive_tickets=0
     where short_name = '%s'
    """ % (testname,)
    tdb.runsql(sql)

def is_employee_active_and_rcvng(tdb, testname):
    rows = tdb.dbado.getrecords('employee', short_name=testname)
    if rows:
        if int(rows[0]['active']) and int(rows[0]['can_receive_tickets']):
            return True
        else:
            return False
    return None

def add_test_area(tdb, testname, emp_id, map_id=600):
    # add a test area associated with this locator
    rows = tdb.dbado.getrecords('area', area_name=testname, locator_id=emp_id)
    if rows:
        new_area_id = rows[0]['area_id']
    else:
        max_area_id = tdb.dbado.get_max_id('area', 'area_id')
        sql = """
         insert area (area_name, map_id, locator_id)
         values ('%s', %s, %s)
        """ % (testname, map_id, emp_id,)
        tdb.dbado.runsql(sql)
        new_area_id = tdb.dbado.get_ids_after('area', 'area_id', max_area_id)[0]

    return new_area_id

def delete_test_area(tdb, testname, emp_id, map_id=600):
    sql = """
     delete from area where area_name = '%s' and emp_id = %s and map_id = %s
    """ % (testname, emp_id, map_id)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_county_area(tdb, testname, area_id, state='XX', call_center='',
                         munic='BAR', map_id=600):
    # get max county_area_id from county_area
    rows = tdb.getrecords('county_area', state=state, county=testname,
           area_id=area_id, call_center=call_center, munic = munic)
    if rows:
        new_county_area_id = rows[0]['county_area_id']
    else:
        max_county_area_id = tdb.dbado.get_max_id('county_area', 'county_area_id')
        sql = """
         insert county_area (state, county, munic, area_id, call_center, map_id)
         values ('%s', '%s', '%s', %s, '%s', %s)
        """ % (state, testname, munic, area_id, call_center, int(map_id))
        tdb.runsql(sql)
        new_county_area_id = tdb.dbado.get_ids_after('county_area',
                             'county_area_id', max_county_area_id)[0]

    return new_county_area_id

def delete_test_county_area(tdb, testname, area_id, state='XX',
                            call_center='', munic='BAR'):
    sql = """
     delete from county_area
     where state = '%s' and county = '%s' and munic = '%s' and area_id = %s
       and call_center = '%s'
    """ % (state, testname, munic, area_id, call_center)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_map(tdb, testname, state, county, code):
    rows = tdb.dbado.getrecords('map', map_name=testname, state=state,
           county=county, ticket_code=code)
    if rows:
        map_id = rows[0]['map_id']
    else:
        max_map_id = tdb.dbado.get_max_id('map', 'map_id')
        sql = """
         insert map (map_name, state, county, ticket_code)
         values ('%s', '%s', '%s', '%s')
        """ % (testname, state, county, code)
        tdb.dbado.runsql(sql)
        map_id = tdb.dbado.get_ids_after('map', 'map_id', max_map_id)[0]

    return map_id

def delete_test_map(tdb, testname, state, county, code):
    sql = """
     delete from map where map_name = '%s' and state = '%s' and county = '%s' and ticket_code = '%s'
    """ % (testname, state, county, code)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_map_page(tdb, map_id, map_page_num):
    rows = tdb.dbado.getrecords('map_page', map_id=map_id, map_page_num=map_page_num)
    if rows:
        return 0    # there is no map_page_id
    else:
        sql = """
         insert map_page (map_id, map_page_num, x_min, x_max, y_min, y_max)
         values (%s, '%s', 'A', 'Z', 1, 99)
        """ % (map_id, map_page_num)
        tdb.dbado.runsql(sql)
        return 1 # success

def delete_test_map_page(tdb, map_id, map_page_num):
    sql = """
     delete from map_page where map_id = '%s' and
                                map_page_num = '%s'
    """ % (map_id, map_page_num)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_map_cell(tdb, map_id, map_page_num, x, y, area_id):
    rows = tdb.dbado.getrecords('map_cell', map_id=map_id,
           map_page_num=map_page_num, x_part=x, y_part=y, area_id=area_id)
    if rows:
        pass    # already a record
    else:
        sql = """
         insert map_cell (map_id, map_page_num, x_part, y_part, area_id)
         values (%s, '%s', '%s', '%s', %s)
        """ % (map_id, map_page_num, x, y, area_id)
        tdb.dbado.runsql(sql)

    # doesn't return anything

def delete_test_map_cell(tdb, map_id, map_page_num, x, y, area_id):
    sql = """
     delete from map_cell where map_id = '%s' and
                                map_page_num = '%s' and
                                x_part = '%s' and
                                y_part = '%s' and
                                area_id = %s
    """ % (map_id, map_page_num, x, y, area_id)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_map_geo(tdb, map_id, map_page_num, x_part, y_part, min_lat,
                     max_lat, min_long, max_long):
    rows = tdb.dbado.getrecords('map_geo', map_id=map_id, min_long=min_long,
           min_lat=min_lat)
    if rows:
        pass
    else:
        sql = """
         insert map_geo (map_id, map_page_num, x_part, y_part, min_lat,
                         min_long, max_lat, max_long)
         values (%s, %s, '%s', '%s', %s, %s, %s, %s)
        """ % (map_id, map_page_num, x_part, y_part, min_lat, min_long,
               max_lat, max_long)
        tdb.dbado.runsql(sql)

def add_test_township(tdb, township, range, section, area_id, basecode,
                      call_center='*'):
    rows = tdb.dbado.getrecords('map_trs', township=township, range=range,
           section=section, area_id=area_id, basecode=basecode,
           call_center=call_center)
    if rows:
        pass    # there is no map_trs id
    else:
        sql = """
         insert map_trs (township, range, section, area_id, basecode, call_center)
         values ('%s', '%s', '%s', %s, '%s', '%s')
        """ % (township, range, section, area_id, basecode, call_center)
        tdb.dbado.runsql(sql)

    # does not return anything

def delete_test_township(tdb, township, range, section, area_id, basecode,
                              call_center='*'):
    sql = """
     delete from map_trs where area_name = '%s' and
                               range = '%s' and
                               section = '%s' and
                               area_id = %s and
                               basecode = '%s' and
                               call_center = '%s'
    """ % (township, range, section, area_id, basecode, call_center)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_qtrmin(tdb, lat, long, area_id, call_center=""):
    rows = tdb.dbado.getrecords('map_qtrmin', qtrmin_lat=lat, qtrmin_long=long,
           call_center=call_center)
    if rows:
        # make sure we have the right id
        sql = """
         update map_qtrmin set area_id = %s
         where qtrmin_lat = '%s' and qtrmin_long = '%s'
           and call_center = '%s'
        """ % (area_id, lat, long, call_center)
        tdb.runsql(sql)
    else:
        sql = """
         insert map_qtrmin (qtrmin_lat, qtrmin_long, area_id, long_length,
                            call_center)
         values ('%s', '%s', %s, 1, '%s')
        """ % (lat, long, area_id, call_center)
        tdb.dbado.runsql(sql)

def delete_test_qtrmin(tdb, lat, long, area_id, call_center=""):
    sql = """
     delete from map_qtrmin where qtrmin_lat = '%s' and
                                  qtrmin_long = '%s' and
                                  area_id = %s and
                                  long_length = 1 and
                                  call_center = '%s'
    """ % (lat, long, area_id, call_center)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_hp_grid(tdb, call_center, client_code, grid):
    rows = tdb.dbado.getrecords('hp_grid', call_center=call_center,
           client_code=client_code, grid=grid)
    if rows:
        return rows[0]['hp_grid_id']
    else:
        max_hp_grid_id = tdb.dbado.get_max_id('hp_grid', 'hp_grid_id')
        sql = """
         insert hp_grid (call_center, client_code, grid)
         values ('%s', '%s', '%s')
        """ % (call_center, client_code, grid)
        tdb.dbado.runsql(sql)
        id = tdb.dbado.get_ids_after('hp_grid', 'hp_grid_id', max_hp_grid_id)[0]
        return id

def delete_test_hp_grid(tdb,call_center):
    sql = """
     delete from hp_grid where call_center = '%s'
    """ % (call_center)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_hp_address(tdb, call_center, client_code, street, city,
                        county, state):
    rows = tdb.dbado.getrecords('hp_address',
                                call_center=call_center,
                                client_code=client_code,
                                street=street,
                                city=city,
                                county=county,
                                state=state)

    if rows:
        return rows[0]['hp_address_id']
    else:
        max_hp_address_id = tdb.dbado.get_max_id('hp_address', 'hp_address_id')
        sql = """
         insert hp_address (call_center, client_code, street, city, county, state)
         values ('%s', '%s', '%s', '%s', '%s', '%s')
        """ % (call_center, client_code, street, city, county, state)
        tdb.dbado.runsql(sql)
        id = tdb.dbado.get_ids_after('hp_address', 'hp_address_id', max_hp_address_id)[0]
        return id

def delete_test_hp_address(tdb,call_center):
    sql = """
     delete from hp_address where call_center = '%s'
    """ % (call_center)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_client(tdb, client_code, call_center, active=1,
                    update_call_center=None, alert=0):
    rows = tdb.dbado.getrecords('client', oc_code=client_code,
           call_center=call_center, active=1)
    if rows:
        new_client_id = rows[0]['client_id']
    else:
        max_client_id = tdb.dbado.get_max_id('client', 'client_id')
        if update_call_center:
            spam1 = ', update_call_center'
            spam2 = ", '%s'" % (update_call_center,)
        else:
            spam1 = spam2 = ''
        sql = """
         insert client (oc_code, call_center, office_id, client_name, alert,
                        active %s)
         values ('%s', '%s', 100, '%s', %d, %d %s)
        """ % (spam1, client_code, call_center, client_code, alert, active,
               spam2)
        tdb.dbado.runsql(sql)

        new_client_id = tdb.dbado.get_ids_after('client', 'client_id', max_client_id)[0]

    return new_client_id

def delete_test_client(tdb, client_id):
    sql = """
     delete client
     where client_id = %s
    """ % (client_id,)
    try:
        tdb.runsql(sql)
    except:
        pass

def make_client(tdb, client_code, call_center):
    """ Make sure that <client code> for <call center> is a client.  If the
        record doesn't exist, add it.  If it does exist, but it's not active,
        make it active. """
    rows = tdb.dbado.getrecords('client', oc_code=client_code,
           call_center=call_center)
    if rows:
        if not rows[0]['active'] == '1':
            sql = """
             update client
             set active = 1
             where client_id = %s
            """ % (rows[0]['client_id'],)
            tdb.dbado.runsql(sql)
    else:
        add_test_client(tdb, client_code, call_center)

def add_test_call_center(tdb, name, use_prerouting=1):
    sql = """
     insert call_center
      (cc_code, cc_name, active, uses_wp, use_prerouting)
      values ('%s', '%s', 1, 0, %s)
    """ % (name, name, use_prerouting)
    try:
        tdb.runsql(sql)
    except:
        pass

def delete_test_call_center(tdb, cc_code):
    sql = """
     delete client
     where call_center = '%s'
     or update_call_center = '%s'
    """ % (cc_code,cc_code)
    try:
        tdb.runsql(sql)
    except:
        pass
    sql = """
     delete from call_center where cc_code = '%s'
    """ % (cc_code)
    try:
        tdb.runsql(sql)
    except:
        pass

def add_test_user(tdb, emp_id):
    rows = tdb.dbado.getrecords('users', emp_id=emp_id)
    if rows:
        return rows[0]['uid']
    else:
        # Make sure the login_id is not taken
        login_id = str(emp_id)
        rows = tdb.dbado.getrecords('users', login_id=login_id)
        if rows:
            login_id = str(uuid.uuid4())[:20]
        max_uid = tdb.dbado.get_max_id('users', 'uid')
        tdb.dbado.insert('users', emp_id=emp_id, first_name="test",
         last_name='test', login_id=login_id, password='1234', chg_pwd=0,
         active_ind=1, can_view_notes=0, can_view_history=0)
        return tdb.dbado.get_ids_after('users', 'uid', max_uid)[0]

def add_test_ticket_ack_match(tdb, call_center, match_string, work_priority=None):
    # doesn't return anything, just adds a record if necessary
    rows = tdb.dbado.getrecords('ticket_ack_match', call_center=call_center,
           match_string=match_string)
    if not rows:
        if work_priority is not None:
            tdb.dbado.insert('ticket_ack_match', call_center=call_center,
                             match_string=match_string, work_priority=work_priority)
        else:
            tdb.dbado.insert('ticket_ack_match', call_center=call_center,
                             match_string=match_string)

def add_test_routing_list(tdb, call_center, fieldname, value, area_id):
    rows = tdb.dbado.getrecords('routing_list', call_center=call_center,
           field_name=fieldname, field_value=value, area_id=area_id)
    if rows:
        return rows[0]['routing_list_id']
    else:
        maxid = tdb.dbado.get_max_id('routing_list', 'routing_list_id')
        tdb.dbado.insert('routing_list', call_center=call_center,
         field_name=fieldname, field_value=value, area_id=area_id)
        return tdb.dbado.get_ids_after('routing_list', 'routing_list_id',
               maxid)[0]

def add_test_notification_email(tdb, notif_type, client_id, email):
    maxid = tdb.dbado.get_max_id('notification_email_by_client',
     'notification_email_id')
    tdb.dbado.insert('notification_email_by_client',
     client_id=client_id, notification_type=notif_type, email_address=email)
    return tdb.dbado.get_ids_after('notification_email_by_client',
           'notification_email_id', maxid)[0]

def add_damage_due_date_rule(tdb, pc_code, utility_co, calc_method, days,
                             active=1):
    tdb.dbado.insert('damage_due_date_rule', pc_code=pc_code,
                     utility_co_damaged=utility_co, calc_method=calc_method,
                     days=days, active=active)
             
def add_test_status(tdb, status, complete=0, status_name='test status', billable=0):
    sql = """
     insert statuslist (status, complete, status_name, billable)
     values ('%s', %d, '%s', %d)
    """ % (status, complete, status_name, billable)
    try:
        tdb.runsql(sql)
    except:
        pass                     

def add_incoming_item(tdb, call_center, item_type, item_text, datetime):
    # note: datetime is expected to be a string in the usual format 
    # 'YYYY-MM-DD HH:mm:SS'.
    tdb.runsql("""
     insert incoming_item (cc_code, item_type, item_text, insert_date)
     values ('%s', %d, '%s', '%s')
     """ % (call_center, item_type, item_text, datetime))

def set_client_active(tdb, call_center, client_code):
    sql = """
     update client
     set active=1
     where call_center = '%s' and oc_code = '%s'
    """ % (call_center, client_code)
    tdb.runsql(sql)

def set_client_inactive(tdb, call_center, client_code):
    '''Make client inactive'''
    sql = """
     update client
     set active=0
     where call_center = '%s' and oc_code = '%s'
    """ % (call_center, client_code)
    tdb.runsql(sql)

def set_call_center_active(tdb, call_center, active=1):
    sql = """
     update call_center
     set active=%d
     where cc_code = '%s'
    """ % (int(active), call_center)
    tdb.runsql(sql)

def insert_fax_queue(tdb, call_center, client_code, status, state):
    # remove duplicate records
    tdb.deleterecords("fax_queue_rules", call_center=call_center,
     client_code=client_code, status=status, state=state)

    sql = """
     insert fax_queue_rules
     (call_center, client_code, status, state)
     values ('%s', '%s', '%s', '%s')
    """ % (call_center, client_code, status, state)
    tdb.runsql(sql)

#
# get test tickets

def get_test_ticket(filename, number, formats):
    tl = ticketloader.TicketLoader(filename)
    raw = tl.tickets[number]
    handler = ticketparser.TicketHandler()
    t = handler.parse(raw, formats)
    return t

#
# delete test records

def deletetickets(tdb):
    """ Delete all tickets from ticket and locate table. """
    tdb.dbado.runsql("delete assignment")
    tdb.dbado.runsql("delete locate")
    tdb.dbado.runsql("delete ticket_snap")
    tdb.dbado.runsql("delete ticket")

def deletesummaries(tdb):
    """ Delete all summaries from summary_header and summary_detail
        table. """
    tdb.dbado.runsql("delete summary_detail")
    tdb.dbado.runsql("delete summary_header")

def clear_database(tdb):
    """ Clear some important database tables. For testing purposes only. """
    for tablename in TABLES_TO_BE_CLEARED:
        tdb.dbado.deleterecords(tablename)

def drop(filename, data, dir="test_incoming"):
    """ Write a file (containing one or more images) to the
        test_incoming directory. """
    fullname = os.path.join(dir, filename)
    f = open(fullname, "wb")
    f.write(data)
    f.close()

#
# misc

def delete_utility_ini():
    """ Delete the custom utility.ini in the data dir. Will be rewritten by
        main.py when necessary. """
    import datadir, filetools
    path = datadir.datadir.get_filename("utility.ini")
    filetools.remove_file(path)

