# ftp_server.py

import sys
PYFTPDLIB_EGG = "pyftpdlib-0.6.0-py%d.%d.egg" % sys.version_info[:2]

import os
whereami = os.path.dirname(os.path.abspath(__file__))
lib_path = os.path.join(whereami, "..", PYFTPDLIB_EGG)
sys.path.append(lib_path)

from pyftpdlib import ftpserver

def run_ftp_server(server="127.0.0.1", port=21, directory="test_ftp"):
    authorizer = ftpserver.DummyAuthorizer()
    authorizer.add_user("testuser", "123456789", directory, perm="elradfmw")
    handler = ftpserver.FTPHandler
    handler.authorizer = authorizer
    address = (server, port)
    ftpd = ftpserver.FTPServer(address, handler)
    ftpd.serve_forever()

if __name__ == "__main__":

    port = 21
    directory = "test_ftp"
    if sys.argv[1:]:
        port = int(sys.argv[1])
    if sys.argv[2:]:
        directory = str(sys.argv[2])
    run_ftp_server(port=port, directory=directory)

