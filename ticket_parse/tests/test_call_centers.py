# test_call_centers.py
# Sanity checks for call centers.
# Created: 2002.10.17, HN
#
# TODO: test work order centers

import os
import site; site.addsitedir('.')
import sys
import types
import unittest
#
import businesslogic
import call_centers
import config
import parsers
import recognize
import summaryparsers
import ticket_db
import ticketloader
import ticketparser
import tools
import update_call_centers
import _testprep

# add call centers that aren't done yet
NOT_DONE = []

class TestCallCenters(unittest.TestCase):
    """ Sanity checks for call centers. The moral is... every call center added
        to call_centers.py must be complete, i.e. have parsers, businesslogic,
        etc, or these tests will fail.
    """
    will_fail = False

    def setUp(self):
        _testprep.set_cc_routing_file()
        self.tdb = ticket_db.TicketDB()

    def test_001_parsers(self):
        """ Call centers parsers exist? """
        # For every call center, there should be parsers and summaryparsers,
        # and recognizer regexen.
        names = []
        for format in call_centers.format2cc.keys():
            if format in NOT_DONE:
                continue # skip this format
            try:
                parser = parsers.get_parser(format)
                assert parser.__name__.endswith("Parser")
            except:
                names.append(parser)
        self.assert_(not names, "Parsers not found or not working for: %s" %
          names)

    def test_002_summaryparsers(self):
        """ Call center summary parsers exist? """
        names = []
        for format in call_centers.format2cc.keys():
            try:
                parser = summaryparsers.get_summaryparser(format)
                assert parser.__name__.endswith("SummaryParser")
            except:
                names.append(format)
        self.assert_(not names, "Summaryparsers not found or not working for: %s" % (names,))

    def test_003_recognizers(self):
        """ Call center recognizers exist? """
        for key in call_centers.format2cc.keys():
            self.assert_(recognize.regexen.has_key(key),
             key + " not found in recognize.regexen")
            self.assert_(recognize.s_regexen.has_key(key),
             key + " not found in recognize.s_regexen")
            # we don't test gm_regexen... not every call center has messages

    def test_004_businesslogic(self):
        """ Call center businesslogic exist and is sane? """
        cc_names = call_centers.cc2format.keys()
        cc_names.sort()
        for key in cc_names:
            if key in NOT_DONE:
                continue
            logic = businesslogic.getbusinesslogic(key, self.tdb, [])
            self.assert_(logic.DEFAULT_LOCATOR,
                 key + " does not have a default locator")

            # test if locator_methods() works and returns a list
            # (we can't test the actual results of the methods here)
            blah = logic.locator_methods({}) # use dummy row
            self.assert_(isinstance(blah, list))

    def test_006_businesslogic(self):
        """ Test validity of router methods. """

        t = self._get_ticket()

        for name, obj in businesslogic.__dict__.items():
            # look for all descendants of BusinessLogic
            if type(obj) == types.ClassType \
            and issubclass(obj, businesslogic.BusinessLogic) \
            and obj != businesslogic.BusinessLogic:
                logic = obj(self.tdb, [])
                # a routing method should always return an instance of
                # tools.RoutingResult:
                for method in logic.locator_methods(t):
                    r = method(t)
                    self.assert_(isinstance(r, tools.RoutingResult),
                     "Method %s returns %s, type: %s" % (method, r, type(r)))

    # also tested in test_update_call_centers, but with a different focus.
    def test_007_update_call_centers(self):
        cc = call_centers.get_call_centers(self.tdb)
        for center in cc.ucc.keys():
            self.assert_(update_call_centers.get_updatedata(center))

    def test_008_registry(self):
        VALID_SYMBOLS = ['ftp', 'email', 'xmlhttp', 'workorder', 'soap']
        import callcenters as ccpkg
        cc = call_centers.get_call_centers(self.tdb)
        cc_names = cc.get_all_call_centers()
        for cc_name in cc_names:
            if cc_name.startswith('TEST'):
                continue
            orig_cc_name = cc_name
            if cc_name[0] in '0123456789': cc_name = "c" + cc_name
            try:
                reg = ccpkg.get_registry(cc_name)
            except ImportError:
                pass
            except AttributeError:
                self.fail("No registry found for: %s" % cc_name)
            else:
                for k, v in reg.items():
                    self.assertEquals(len(k), 3)
                    self.assertEquals(k[0], orig_cc_name)
                    self.assertTrue(k[1] in VALID_SYMBOLS,
                     "Invalid symbol: %s" % (repr(k)))

    def test_get_call_centers(self):
        c1 = call_centers.get_call_centers(self.tdb)
        c2 = call_centers.get_call_centers(self.tdb)
        self.assert_(c1 is c2) # must be the same object
        d = c1.get("Atlanta")
        self.assert_(d is not None)
        self.assertEquals(d['cc_code'], 'Atlanta')
        self.assertEquals(d['use_prerouting'], '1')
        self.assertTrue(d.has_key('timezone_code')) # time zone
        all_cc = c1.get_all_call_centers()
        self.assertTrue("Atlanta" in all_cc)

        # update call centers...
        # note: this may change if the reference data changes, e.g. if there
        # are no clients defined for a call center
        self.assertTrue("NCA2" in c1.ucc.keys())
        self.assertEquals(c1.update_call_center("NCA2"), "NCA1")
        self.assertEquals(c1.update_call_center("Atlanta"), None)

    def _get_ticket(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "Atlanta-1.txt"))
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Atlanta"])
        t.client_code = "ATL01" # whatever
        return t # return real ticket, not a dict

    def test_ucc_dict(self):
        cc = call_centers.get_call_centers(self.tdb)
        _testprep.add_test_call_center(self.tdb, 'CCTMP')
        client_id = _testprep.add_test_client(self.tdb, 'CLIENT1', 'CCTMP',
                    update_call_center='CCTMP')
        try:
            # Update call center cannot update itself
            self.assertEquals(cc.update_call_center('CCTMP'), None)
        finally:
            _testprep.delete_test_call_center(self.tdb, 'CCTMP')

    def test_call_centers_for_update(self):
        '''
        Since we now give a call center UpdateData if requested,
        all call centers should have it if called
        '''
        for center in call_centers.cc2format.keys():
            try:
                x = update_call_centers.get_updatedata(center)
            except:
                self.fail("No update data for " + center)

    def test_importing(self):
        """ When we import call_centers for the first time, _cc should be None;
            IOW, no CallCenters object should have been created yet. """

        reload(call_centers)
        self.assertEquals(call_centers._cc, None)



if __name__ == "__main__":
    unittest.main()

