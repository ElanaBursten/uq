# test_wo_router.py

import os
import site; site.addsitedir('.')
import unittest
#
from formats import Lambert
import _testprep
import test_main_work_orders
import ticket_db
import ticketloader
import ticketparser
import wo_router
import work_order

class TestWorkOrderRouter(unittest.TestCase):

    _setup_LAM01 = test_main_work_orders.TestMainWorkOrders._setup_LAM01.im_func

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

        self._setup_LAM01()
        self._setup_LAM01_routing()

    def _setup_LAM01_routing(self):
        from callcenters import LAM01
        # make sure that LAM01's default locator exists
        default_locator = LAM01.WorkOrderBusinessLogic.DEFAULT_LOCATOR
        rows = self.tdb.getrecords('employee', emp_id=default_locator)
        if not rows:
            # if the default locator doesn't exist in our database, then just
            # pick one that does...
            rows = self.tdb.runsql_result("""
             select min(emp_id) as c from employee
            """)
            LAM01.WorkOrderBusinessLogic.DEFAULT_LOCATOR = rows[0]['c']
        self.LAM01_DEF_LOC = LAM01.WorkOrderBusinessLogic.DEFAULT_LOCATOR

        # remove any preexisting county_area rules that might interfere
        self.tdb.runsql("""
          delete county_area
          where call_center = 'LAM01'
          and state = 'MD' and county = 'BALTIMORE'
        """)

        # add custom locator for LAM01
        self.emp_id = _testprep.add_test_employee(self.tdb, 'LAM01-routing')
        self.map_id = _testprep.add_test_map(self.tdb, 'MD.WO.LAMBERT',
                      'MD', 'EXAMPLE', 'LAM01')
        self.area_id = _testprep.add_test_area(self.tdb, 'MD.LAM.BALTIMORE',
                       self.emp_id, self.map_id)
        self.county_area_id = _testprep.add_test_county_area(self.tdb,
                              'BALTIMORE', self.area_id, 'MD', 'LAM01', '*')

        self.emp_id_2 = _testprep.add_test_employee(self.tdb, 'LAM01-two')
        self.area_id_2 = _testprep.add_test_area(self.tdb, 'MD.LAM.BALTIMORE2',
                         self.emp_id_2, self.map_id)
        self.county_area_id_2 = _testprep.add_test_county_area(self.tdb,
                                'BALTIMORE', self.area_id_2, 'MD', 'LAM01',
                                'ESSEX')
        self.assertNotEqual(self.area_id, self.area_id_2)

    def insert_work_order(self):
        handler = ticketparser.TicketHandler()
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00020-A01.txt"))
        raw = tl.tickets[0]
        wo = Lambert.WorkOrderParser().parse(raw)
        wo.work_county = 'FOOBAR' # insert dummy county
        wo.insert(self.tdb)
        return wo

    def test_01_default_routing(self):
        # 2011-12-26 hn New ref data introduce catchall rule in county_area
        # which breaks our default routing. Let's remove that rule.
        self.tdb.runsql("""
          delete county_area
          where call_center = 'LAM01'
          and county = '*'
        """)

        wo = self.insert_work_order()
        self.assertTrue(wo.wo_id)

        # make sure there's only one work_order record right now
        rows = self.tdb.getrecords("work_order")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['status'], '-P')

        # make sure it has NO wo_assignment
        rows = self.tdb.getrecords('wo_assignment')
        self.assertEquals(len(rows), 0)

        options = wo_router.WorkOrderRouterOptions(verbose=0)
        wor = wo_router.WorkOrderRouter(options)
        wor.run()

        # check if our work order record has an assignment, pointing to
        # LAM01's default locator
        rows = self.tdb.getrecords('wo_assignment', wo_id=wo.wo_id)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['added_by'], 'ROUTER')
        self.assertEquals(rows[0]['assigned_to_id'], self.LAM01_DEF_LOC)
        # FAIL: is now routing through munic instead

        # check status of work order!
        wo2 = work_order.WorkOrder.load(self.tdb, wo.wo_id)
        self.assertEquals(wo2.status, '-R')

    # reused for another test.
    def test_02_county_routing(self, munic=None):
        """ Test state/county/area routing.
            Using MD/BALTIMORE/*.
        """
        wo = self.insert_work_order()

        # make sure state/county/city info is set correctly
        wo.work_state = 'MD'
        wo.work_county = 'BALTIMORE'
        if munic:
            wo.work_city = munic
        wo.update(self.tdb)

        # check that find_munic_area_locator_2 works
        rows = self.tdb.find_munic_area_locator_2('MD', 'BALTIMORE',
               'UPPER MARLBORO', 'LAM01')
        self.assertTrue(rows)

        # run the router
        options = wo_router.WorkOrderRouterOptions(verbose=0)
        wor = wo_router.WorkOrderRouter(options)
        wor.run()

        # check that it was routed to self.emp_id
        wo2 = work_order.WorkOrder.load(self.tdb, wo.wo_id)
        self.assertEquals(wo2.status, '-R')

        # get wo_assignment record for this work order
        woa = self.tdb.getrecords('wo_assignment', wo_id=wo2.wo_id)[0]
        emp_id = self.emp_id_2 if munic else self.emp_id
        self.assertEquals(int(woa['assigned_to_id']), int(emp_id))
        # FAIL: routes to different locators now

    def test_03_county_routing(self):
        """ Test state/county/area routing.
            Using MD/BALTIMORE/ESSEX (with different area than test_02).
        """
        self.test_02_county_routing(munic='ESSEX')

    #
    # WGL routing

    def test_10_wgl_routing(self):
        # make sure we have a call center WGL
        _testprep.add_test_call_center(self.tdb, 'WGL')

        # make sure locator is there
        from callcenters import WGL
        default_locator = WGL.WorkOrderBusinessLogic.DEFAULT_LOCATOR
        rows = self.tdb.getrecords('employee', emp_id=default_locator)
        if not rows:
            # if the default locator doesn't exist in our database, then just
            # pick one that does...
            rows = self.tdb.runsql_result("""
             select max(emp_id) as c from employee
            """)
            WGL.WorkOrderBusinessLogic.DEFAULT_LOCATOR = rows[0]['c']
        self.WGL_DEF_LOC = WGL.WorkOrderBusinessLogic.DEFAULT_LOCATOR

        # parse a work order (i.e. put some data in the system as WGL)
        wo = self.insert_work_order()
        wo.wo_source = 'WGL' # we may need to change more fields?
        wo.update(self.tdb)

        # verify that WGL work order is in table
        rows = self.tdb.getrecords('work_order')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['wo_source'], 'WGL')

        # run the router
        options = wo_router.WorkOrderRouterOptions(verbose=0)
        wor = wo_router.WorkOrderRouter(options)
        wor.run()

        # check that it was routed to self.emp_id
        wo2 = work_order.WorkOrder.load(self.tdb, wo.wo_id)
        self.assertEquals(wo2.status, '-R')
        # check if the work order routed to the right locator
        was = self.tdb.getrecords('wo_assignment')
        self.assertEquals(len(was), 1)
        self.assertEquals(was[0]['assigned_to_id'], self.WGL_DEF_LOC)
        self.assertEquals(was[0]['wo_id'], wo.wo_id)


if __name__ == "__main__":
    unittest.main()

