# test_noclienthandler.py

import site; site.addsitedir('.')
import unittest
#
import businesslogic
import locate
import noclienthandler
import ticket_db
import ticketloader
import ticketparser
import _testprep

class TestLog:
    def log_event(self, *args, **kwdargs):
        pass

class TestNoClientHandler(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.set_cc_routing_file()

    def test_has_uq_client(self):
        nchandler = noclienthandler.NoClientHandler(self.tdb, {}, TestLog())
        huq = nchandler.has_uq_client
        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])
        locates = ["A", "B", "C", "D"]
        clients = ["A", "E", "Q"]
        self.assert_(huq("Atlanta", locates, logic, clients))

        locates = ["P", "R", "S"]
        self.assertEquals(huq("Atlanta", locates, logic, clients), 0)

        locates = []
        self.assertEquals(huq("Atlanta", locates, logic, clients), 0)

        # now test a different flavor of logic
        logic = businesslogic.getbusinesslogic("NewJersey", self.tdb, [])
        locates = ["XXX", "YYY", "ZZZ"]
        # no clients as-is, but "XXX" is a special case
        self.assertEquals(huq("NewJersey", locates, logic, clients), 11)

    def test_has_no_uq_clients(self):
        # grab any old Atlanta ticket and see what happens
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])
        nchandler = noclienthandler.NoClientHandler(self.tdb, {}, TestLog())
        clients = ["USW01", "ATL01", "ABS01"]
        self.assertEquals(
         nchandler.has_no_uq_clients(t, 0, logic, clients, 0),
         0) # USW01 is in there, so it does have clients

        clients.remove("USW01")
        self.assertEquals(
         nchandler.has_no_uq_clients(t, 0, logic, clients, 0),
         1) # USW01 is not in there anymore, so it does not have clients

        id = self.tdb.insertticket(t)
        t.locates = [locate.Locate("BLURB77")]  # fake locate
        clients.append("USW01")
        self.assertEquals(
         nchandler.has_no_uq_clients(t, id, logic, clients, 1),
         0) # USW01 is not on ticket, but is in database

        clients.remove("USW01")
        self.assertEquals(
         nchandler.has_no_uq_clients(t, 0, logic, clients, 0),
         1) # USW01 is neither in database nor on ticket




if __name__ == "__main__":

    unittest.main()

