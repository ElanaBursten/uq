# test_check_summary_stored_procs.py
#
# TODO: Clean up tests for LCC1, OCC3

import os
import re
import site; site.addsitedir('.')
import string
import unittest
#
from search_log_file import SearchLogFile
import _testprep
import call_centers
import config
import date
import formats
import locate
import mail2
import main
import recognize
import summaryparsers
import ticket_db
import ticketloader
import ticketparser
import tools

class TestCheckSummarySPs(unittest.TestCase):

    def setUp(self):
        self.handler = ticketparser.TicketHandler()
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def getticket(self, city, separator="~~~~~"):
        filename = "../testdata/sum-%s.txt" % (city,)
        tl = ticketloader.TicketLoader(filename)
        raw = tl.getTicket()
        return raw

    def test_LCC1_check_summary_sp(self):
        """
        test the LCC1 check summary stored procedure
        """
        tdb = ticket_db.TicketDB()
        # Clean the db
        _testprep.clear_database(tdb) # XXX redundant?

        # Delete and then insert the LCC1 call center with the specified audit_method
        _testprep.delete_test_call_center(tdb,'LCC1')
        sql = """
         insert call_center
          (cc_code, cc_name, active, uses_wp, audit_method,use_prerouting)
          values ('LCC1', 'LCC1', 1, 0, 'ticket_LCC1',1)
        """
        try:
            tdb.runsql(sql)
        except:
            pass

        # Modify the configuration

        self.config = config.getConfiguration()
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]
        mail2.TESTING = 1
        mail2._test_mail_sent = []
        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

        self.config.processes[0]['format'] = 'LCC1'

        # Parse a summary ticket so that we can get the expected ticket numbers
        # Summary ticket
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LCC1_audit.txt"))
        raw_summary = tl.tickets[0]
        summ = self.handler.parse(raw_summary, ['Oregon3'])

        # Drop some tickets
        formats = call_centers.cc2format["LCC1"]
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","LCC1-2007-12-11-00045.txt"))

        p = re.compile("(^Ticket No: +)(\d+)( *|$)", re.MULTILINE)
        td_p = re.compile("(^Transmit +Date: +)([0-9/]+)( +Time: +)([0-9:]+)(.*)", re.MULTILINE)

        def td_replace(match):
            groups = match.groups()
            return groups[0]+'12/18/07'+groups[2]+'2:40'+groups[4]

        for index, raw in enumerate(tl.tickets):
            # Replace with first ticket number in summary
            raw = p.sub(r'Ticket No: %s ' % (summ.data[0].ticket_number,),raw)
            # Change transmit date
            raw = td_p.sub(td_replace,raw)
            t = self.handler.parse(raw, formats)
            _testprep.drop("LCC1-WA-%d.txt" % (index,), raw)
        # Assume an OR ticket will do
        tl = ticketloader.TicketLoader(os.path.join("../testdata","Oregon2-1.txt"))
        for index in range(len(summ.data)-1): # Use the ticket numbers from the rest of the summary
            raw = tl.tickets[index]
            # Replace with ticket number in summary
            raw = p.sub(r'Ticket No: %s ' % (summ.data[index+1].ticket_number,),raw)
            # Change transmit date
            raw = td_p.sub(td_replace,raw)
            t = self.handler.parse(raw, formats)
            _testprep.drop("LCC1-OR-%d.txt" % (index,), raw)

        m = main.Main(verbose=0, call_center='LCC1')
        # Truncate the log file
        m.log.logger.logfile.truncate(0)
        m.log.lock = 1
        m.run(runonce=1)

        tickets = tdb.getrecords("ticket")
        def get_number(call_center):
            return len([t for t in tickets if t['ticket_format'] == call_center])
        self.assertEquals(get_number("LOR1"), 23)
        self.assertEquals(get_number("LWA1"), 1)
        # Count how many times ticket adapted
        num_found = 0
        # rewind the log file
        m.log.logger.logfile.seek(0)
        for line in m.log.logger.logfile.readlines():
            if "Ticket adapted: LCC1 ->" in line:
                num_found += 1
        self.assertEquals(num_found, 24)

        # Drop a summary ticket
        _testprep.drop("test1.txt", raw_summary)

        m = main.Main(verbose=0, call_center='LCC1')
        m.log.logger.logfile.truncate(0)
        m.run(runonce=1)
        id = m.ids[0]   # id of ticket that was just posted
        # Verify that summary ticket was parsed
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Processing summary (LCC1, LCC1) (new)")
        self.assertNotEqual(match,None)

        # Run the check_summary
        rows = tdb.runsql_result("exec check_summary_by_ticket_LCC1 '%s','LCC1'" % (date.Date("2007-12-18 00:00:00").isodate(),))
        for row in rows:
            self.assert_(row['found'])

        _testprep.delete_test_call_center(tdb, 'LCC1')
        _testprep.clear_database(tdb)
        _testprep.delete_test_dirs()

    def test_OCC3_summary_report(self):
        '''
        Test the output from the check_summary* sp's
        '''
        import ticketrouter
        tdb = ticket_db.TicketDB()
        _testprep.clear_database(tdb)

        self.config = config.getConfiguration()
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]
        _testprep.create_test_dirs()
        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

        self.config.processes[0]['format'] = 'OCC3'


        # Drop a ticket
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-02-26-00661.txt"))
        raw = tl.getTicket()

        _testprep.drop("occ3.txt", raw)

        m = main.Main(verbose=0, call_center='OCC3')
        m.log.lock = 1
        m.run(runonce=1)

        # Should have one ticket
        rows = tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        # Drop the summaryticket
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","test_summary.txt"))
        raw = tl.getTicket()

        _testprep.drop("occ3_summ.txt", raw)

        m = main.Main(verbose=0, call_center='OCC3')
        m.log.lock = 1
        m.run(runonce=1)

        cc = call_centers.get_call_centers(self.tdb)
        occ3 = cc.get('OCC3')

        # What do we have for the summaries?
        row = tdb.getrecords("summary_header")[0]
        self.assertEquals(row['call_center'], 'OCC3')
        self.assertEquals(row['client_code'], 'WGL904')
        self.assertEquals(row['expected_tickets'], '1')
        row = tdb.runsql_result("exec check_summary_by_%s '%s','OCC3'" % (
              occ3['audit_method'], "2009-02-26 00:00:00"))[0]
        self.assertEquals(row['client_code'],'WGL904')
        self.assertEquals(row['found'],'1')
        row = tdb.getrecords('call_center',cc_code='OCC3')[0]
        original_audit_method = row['audit_method']
        try:
            tdb.updaterecord('call_center', 'cc_code', 'OCC3', audit_method='locate_ucc')
            row = tdb.runsql_result("exec check_summary_by_%s '%s','OCC3'" % (
                  occ3['audit_method'], "2009-02-26 00:00:00"))[0]
            self.assertEquals(row['client_code'],'WGL904')
            self.assertEquals(row['found'],'1')
        finally:
            tdb.updaterecord('call_center', 'cc_code', 'OCC3', audit_method=original_audit_method)

    def test_NewJersey2(self):
        # The check_summary_by_locate_NewJersey2 is a special case, so the
        # testing gets a bit complex.

        # grab an audit and insert it into the database
        audit_path = os.path.join(_testprep.TICKET_PATH, 'audits',
          'NewJersey2-2010-06-18', 'GSU2-2010-06-17-00002.txt')
        tl = ticketloader.TicketLoader(audit_path)
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey2010B'])
        del summ.data[4] # remove duplicates
        del summ.data[2] # ditto
        del summ.data[10:] # only keep first 10 tickets on audit
        summ.expected_tickets = 10
        self.tdb.storesummary(summ)

        # make sure summary is really posted
        self.assertTrue(self.tdb.runsql_result(
          "select * from summary_header where call_center = 'NewJersey2'"))

        # get a list of NewJersey2 clients
        nj2_clients = self.tdb.runsql_result("""
          select oc_code from client
          where call_center = 'NewJersey'
          and update_call_center = 'NewJersey2'
        """)
        self.assertTrue(len(nj2_clients) > 0)
        nj2_clients = sorted([row['oc_code'] for row in nj2_clients])
        the_client = nj2_clients[-1] # pick a client, any client

        # grab a NewJersey2 ticket
        ticket_path = os.path.join(_testprep.TICKET_PATH, 'tickets',
          'NewJersey2-2010-05-28', 'GSU2-2010-05-27-00775.txt')
        tl = ticketloader.TicketLoader(ticket_path)
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NewJersey2010B'])

        # modify ticket dates so they would show up on an audit report
        t.transmit_date = summ.summary_date[:10] + " 11:00:00"
        t.call_date = t.transmit_date

        # make sure we have a valid UQ client on the ticket
        t.locates[0] = locate.Locate(the_client)

        def clear_ticket(t):
            """ Remove ticket_id and locate_ids from a ticket. """
            t.ticket_id = None
            for loc in t.locates:
                loc.locate_id = None

        # first ticket: as-is, ticket_format is NewJersey2, valid client
        t.ticket_number = summ.data[0].ticket_number
        self.tdb.insertticket(t)

        # second ticket: same, but ticket_format is NewJersey
        clear_ticket(t)
        t.ticket_number = summ.data[1].ticket_number
        t.ticket_format = 'NewJersey'
        t.source = 'NewJersey2'
        self.tdb.insertticket(t)

        # third ticket: same, but ticket number is not on audit
        clear_ticket(t)
        t.ticket_number = '123456' # just use a bogus number
        self.tdb.insertticket(t)

        # fourth ticket: like the second, but with a non=NewJersey2 client
        clear_ticket(t)
        t.ticket_number = summ.data[3].ticket_number
        t.locates[0] = locate.Locate('B0GUS')
        self.tdb.insertticket(t)

        tickets_in_db = self.tdb.runsql_result("select * from ticket")
        self.assertEquals(len(tickets_in_db), 4)

        audit_report = self.tdb.runsql_result("""
          exec check_summary_by_locate_GSU2 '%s', 'NewJersey2'
        """ % summ.summary_date)
        #import pprint; pprint.pprint(audit_report)

        # first ticket should be marked as found
        [r1] = [row for row in audit_report
                if row['ticket_number'] == summ.data[0].ticket_number]
        self.assertEquals(r1['found'], '1')

        # second ticket should be marked as found
        [r2] = [row for row in audit_report
                if row['ticket_number'] == summ.data[1].ticket_number]
        self.assertEquals(r2['found'], '1')

        # third ticket should not appear on report at all
        self.assertEquals([],
          [row for row in audit_report if row['ticket_number'] == '123456'])

        # fourth ticket be on the report but NOT be marked as found, because it
        # has a non-NewJersey2 client
        [r4] = [row for row in audit_report
                if row['ticket_number'] == summ.data[3].ticket_number]
        self.assertEquals(r4['found'], '0')


if __name__ == "__main__":
    unittest.main()

