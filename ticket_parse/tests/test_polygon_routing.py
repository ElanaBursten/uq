# test_polygon_routing.py

import os
import site; site.addsitedir('.')
import unittest
#
import businesslogic
import polygon_routing
import ticket_db
import ticketloader
import ticketparser
import _testprep

TESTDIR = os.path.abspath("../testdata")

def intestdir(dir):
    return os.path.join(TESTDIR, dir)

class TestPolygonRouting(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def test_read_mif(self):
        polygons = polygon_routing.read_mif(intestdir('polygons.mif'))
        self.assertEquals(len(polygons), 5) # there are 5 polygons
        self.assertEquals(len(polygons[0]), 6)  # first one has 6 points

    def test_read_table(self):
        data = polygon_routing.read_table(intestdir('polygons.txt'))
        self.assertEquals(len(data), 5)
        self.assertEquals(data[0][0], '1097')
        self.assertEquals(data[1][1], 'DC10')

    def test_make_routing_list(self):
        polygons = polygon_routing.read_mif(intestdir('polygons.mif'))
        area_ids = polygon_routing.read_table(intestdir('polygons.txt'))
        routinglist = polygon_routing.make_routing_list(polygons, area_ids)

        self.assertEquals(str(routinglist[0].coords[0][0]), '-77.051429')
        self.assertEquals(str(routinglist[0].coords[0][1]), '38.915196')
        self.assertEquals(routinglist[0].area_id, '1097')
        self.assertEquals(routinglist[0].name, 'DC01')

    def test_PolygonRoutingList(self):
        routinglist = polygon_routing.PolygonRoutingList(
                      os.path.abspath('../testdata/polygons.mif'),
                      os.path.abspath('../testdata/polygons.txt'))
        # we use absolute paths here so PolygonRoutingList doesn't look in the
        # data dir

        data = [
            ((-77.052209, 38.909123), '1097'),
            ((-77.049673, 38.917473), '1098'),
            ((-77.037123, 38.914943), '1101'),
            ((-77.026978, 38.921812), '1102'),
            ((-77.015096, 38.925045), '1104'),
        ]
        for point, expected_area in data:
            area = routinglist.find_area(point)
            self.assertEquals(area, expected_area)

    def test_PolygonRoutingCache_2(self):
        import config
        cfg = config.getConfiguration()
        cfg.polygon_dir = intestdir("maps")

        cache = polygon_routing.PolygonRoutingCache()
        cache.load('FMW1')
        try:
            cache.load('XYZZY')
        except IOError:
            pass
        else:
            self.Fail("IOError expected")

        # Try getting the routinglists for some call centers.  Those that
        # don't exist will return None.
        cache = polygon_routing.PolygonRoutingCache()
        a = cache['FMW1']
        self.assert_(isinstance(a, polygon_routing.PolygonRoutingList))
        b = cache['FMB1']
        self.assert_(isinstance(b, polygon_routing.PolygonRoutingList))
        c = cache['XYZZY']
        self.assertEquals(c, None)

        q = cache.find_area('FMW1', (-77.052209, 38.909123))
        self.assertEquals(q, '11862')
        r = cache.find_area('FMB1', (-77.052209, 38.909123))
        self.assertEquals(r, '11862')
        s = cache.find_area('XYZZY', (-77.052209, 38.909123))
        self.assertEquals(s, None)

    def test_lwa1(self):
        import config
        cfg = config.getConfiguration()
        cfg.polygon_dir = intestdir("maps")

        routinglist = polygon_routing.PolygonRoutingList(
                      os.path.abspath('../testdata/maps/lwa1.mif'),
                      os.path.abspath('../testdata/maps/lwa1.txt'))
        #for rp in routinglist.routinglist:
        #    print len(rp.coords), rp.area_id, rp.name

        self.assertEquals(routinglist.find_area((-117.978322, 47.410785)), '506')
        self.assertEquals(routinglist.find_area((-118.178322, 47.410785)), '506')

    def test_lwa1_routing(self):

        # add a locator
        emp_id = _testprep.add_test_employee(self.tdb, "LWA1poly", "John",
                 "Polygon")
        # make sure area 513 (a number which is hardcoded in the mapinfo file)
        # has this locator
        self.tdb.runsql("""
          update area
          set locator_id = %d
          where area_id = 513
        """ % int(emp_id))

        import config
        cfg = config.getConfiguration()
        cfg.polygon_dir = intestdir("maps")
        tdb = ticket_db.TicketDB()
        logic = businesslogic.getbusinesslogic("LWA1", tdb, [])
        row = {'ticket_format': 'LWA1', 'work_long': -117.22, 'work_lat': 47.41,
               'ticket_type': 'NORMAL', 'kind': 'NORMAL',
               'work_state': 'ZZ', 'work_county': 'BLAH', 'work_city': 'FOO'}
        rr = logic.locator(row)
        # these may change:
        self.assertEquals(rr.locator, emp_id)
        self.assertEquals(int(rr.area_id), 513)
        # NOTE: this is run after test_lwa1, which sets config.polygon_dir
        # to testdata/maps; that's why it finds polygon files.

        # now use a call center that certainly doesn't have polygon routing.
        # the point of this exercise is that this type of routing should
        # have no errors if polygon files don't exist.
        from callcenters import Atlanta
        class FooBusinessLogic(Atlanta.BusinessLogic):
            def locator_methods(self, row):
                return [
                    self.lcatalog.polygon,
                ]
        foobl = FooBusinessLogic(tdb, [])
        row.update({'ticket_format': 'Atlanta'})
        rr = foobl.locator(row)
        self.assertEquals(rr.locator, FooBusinessLogic.DEFAULT_LOCATOR)

    def test_ticket_1(self):
        handler = ticketparser.TicketHandler()
        tl = ticketloader.TicketLoader("../testdata/lwa1-poly.txt")
        raw = tl.tickets[0]
        t = handler.parse(raw, ["WashingtonState"])

        self.assertAlmostEqual(t.work_long, -122.1459475, 5)
        self.assertAlmostEqual(t.work_lat, 47.449546, 5)

        # we're using the LWA1 polygon test data (testdata/maps/lwa1.txt)
        import config
        cfg = config.getConfiguration()
        cfg.polygon_dir = intestdir("maps")
        prc = polygon_routing.PolygonRoutingCache()
        area = prc.find_area("LWA1", (t.work_long, t.work_lat))
        self.assertEquals(len(prc.data), 1) # the LWA1 object


if __name__ == "__main__":

    unittest.main()
