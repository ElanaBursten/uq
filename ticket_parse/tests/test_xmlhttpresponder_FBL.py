# test_xmlhttpresponder_FBL.py
# 2010-08-19: Moved here to replace a whole bunch of copy & paste programming.

import os
import string
import unittest
#
import _testprep
import config
import date
import locate
import ticket_db
import ticketloader
import ticketparser
import xmlhttpresponder_data as xhrd
from test_xmlhttpresponder import create_mock_responder, find_soap_node

class TestXMLHTTPResponderFBL(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "TestEmployee")
        self.cfg = config.getConfiguration()

    def _test_responder(self, call_center, format, client_code, source_file,
     dec_time=False, namespace=None, nodename="WorkTime"):

        # create test client
        _testprep.add_test_client(self.tdb, client_code, call_center)

        # update configuration
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        z['clients'] = [client_code]

        # get a ticket and parse it
        tl = ticketloader.TicketLoader(source_file)
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, [format])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'

        # replace the locate
        t.locates = [locate.Locate(client=client_code)]
        self.assertEquals(t.ticket_format, call_center)
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id

        # change the status of the locate so we get an entry in the
        # locate_status table (done by locate trigger)
        locate_close_date = date.Date()
        if dec_time:
            locate_close_date.dec_time(hours=1)
        locate_close_date = locate_close_date.isodate()
        sql = """
         update locate
         set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
             closed_date = '%s'
         where locate_id = %s
        """ % (self.test_emp_id, locate_close_date, locate_id,)
        self.tdb.runsql(sql)

        rows = []

        xr = create_mock_responder(rows, verbose=0)
        xr.xmlhttp.responseXML.Text = "success"
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.only_call_center = call_center
        xr.respond_all()

        # Verify WorkTime is as set above
        data = find_soap_node(xr.xml_sent[0], nodename,
               namespace=namespace or "").text
        data = data.replace("T", " ")
        data = data.replace("/", "-")
        self.assertEquals(str(data)[:16], locate_close_date[:16])

    def test_FBL1_work_time_1(self):
        self._test_responder('FBL1', 'BatonRouge1', 'ABC01',
          '../testdata/BatonRouge1-1.txt', dec_time=True)

    def test_FBL1_work_time_2(self):
        self._test_responder('FBL1', 'BatonRouge1', 'ABC01',
          '../testdata/BatonRouge1-1.txt', dec_time=False)

    def test_FEF1_work_time_1(self):
        self._test_responder('FEF1', 'Jacksonville', 'KANELEC01',
          '../testdata/Jacksonville-1.txt', dec_time=True)

    def test_FEF1_work_time_2(self):
        self._test_responder('FEF1', 'Jacksonville', 'KANELEC01',
          '../testdata/Jacksonville-1.txt', dec_time=False)

    def test_NewJersey_work_time_1(self):
        self._test_responder('NewJersey', 'NewJersey', 'KANELEC01',
          '../testdata/NewJersey-1.txt', dec_time=True)

    def test_NewJersey_work_time_2(self):
        self._test_responder('NewJersey', 'NewJersey', 'KANELEC01',
          '../testdata/NewJersey-1.txt', dec_time=False)

    def test_SCA1_work_time_1(self):
        self._test_responder('SCA1', 'SouthCalifornia', 'COX01',
          '../testdata/SouthCalifornia-1.txt', dec_time=True,
          namespace="{http://tempuri.org/ContractLocatorResponseDataSet.xsd}",
          nodename="Locate_DateTime")

    def test_SCA1_work_time_2(self):
        self._test_responder('SCA1', 'SouthCalifornia', 'COX01',
          '../testdata/SouthCalifornia-1.txt', dec_time=False,
          namespace="{http://tempuri.org/ContractLocatorResponseDataSet.xsd}",
          nodename="Locate_DateTime")

    def test_9001_work_time_1(self):
        self._test_responder('9001', 'FL9001', 'SBF20',
          '../testdata/Jacksonville-2.txt', dec_time=True)

    def test_9001_work_time_2(self):
        self._test_responder('9001', 'FL9001', 'SBF20',
          '../testdata/Jacksonville-2.txt', dec_time=False)

    def test_9003_work_time_1(self):
        self._test_responder('9003', 'FL9003', 'ABC01',
          '../testdata/sample_tickets/9003-2007-12-11-00054.txt',
          namespace="{http://tempuri.org/ContractLocatorResponseDataSet.xsd}",
          nodename='Locate_DateTime', dec_time=True)

    def test_9003_work_time_2(self):
        self._test_responder('9003', 'FL9003', 'ABC01',
          '../testdata/sample_tickets/9003-2007-12-11-00054.txt',
          namespace="{http://tempuri.org/ContractLocatorResponseDataSet.xsd}",
          nodename='Locate_DateTime', dec_time=False)

