# test_mail2.py

from __future__ import with_statement
import unittest
#
import config
import emailtools
import mail2
from mock_objects import MockSMTP
from mockery import Mockery

class TestMail2(unittest.TestCase):

    def setUp(self):
        mail2.TESTING = 0

    def test_sendmail(self):
        cfg = config.getConfiguration()
        smtpinfo = cfg.smtp_accs[0]
        self.assertEquals(smtpinfo.host, 'mail.bellsouth.net')

        with Mockery(emailtools, 'get_SMTP_class', lambda x: MockSMTP):
            mail2.sendmail(smtpinfo,
                           toaddrs=["this@that.com"],
                           subject="Just a test",
                           body="blah\nblah\n")

        args, kwargs = MockSMTP._sent[0]
        self.assertEquals(args[0], 'logger@bellsouth.net')
        self.assertTrue('blah' in args[2])
        self.assertTrue('Subject: Just a test' in args[2])

    def test_send_timeout_warning(self):
        cfg = config.getConfiguration()
        if len(cfg.smtp_accs) == 1:
            # add backup SMTP account if it doesn't exist
            smtpinfo = emailtools.SMTPInfo(host='bogus.com', port=225,
                       from_address='foo@bogus.com', use_ssl=1)
            cfg.smtp_accs.insert(1, smtpinfo)

        with Mockery(emailtools, 'get_SMTP_class', lambda x: MockSMTP):
            mail2.send_timeout_warning()
            del cfg.smtp_accs[1]

        args, kwargs = MockSMTP._sent[0]
        self.assertEquals(args[0], 'foo@bogus.com')
        self.assertTrue('timed out, possible problem' in args[2])
        self.assertTrue('Subject: SMTP server warning' in args[2])

    def test_sendmail_ex(self):
        cfg = config.getConfiguration()
        smtpinfo = cfg.smtp_accs[0]

        with Mockery(emailtools, 'get_SMTP_class', lambda x: MockSMTP):
            result = mail2.sendmail_ex(smtpinfo,
                                       toaddrs=["this@that.com"],
                                       subject="Just a test",
                                       body="foo\nfoo\n")

        self.assertTrue(result)
        args, kwargs = MockSMTP._sent[0]
        self.assertEquals(len(MockSMTP._sent), 1)
        self.assertEquals(args[0], 'logger@bellsouth.net')

    def test_sendmail_multipart(self):
        cfg = config.getConfiguration()
        smtpinfo = cfg.smtp_accs[0]

        with open('blah.html', 'w') as f:
            print >> f, "<p>Hi!</p>"
        parts = [('blah.html', 'obsolete', 'text/html', 'base64')]
        with Mockery(emailtools, 'get_SMTP_class', lambda x: MockSMTP):
            mail2.sendmail_multipart(smtpinfo,
                                     toaddrs=['bar@bogus.com'],
                                     subject="Multipart message",
                                     body="Another test",
                                     multiparts=parts)

        args, kwargs = MockSMTP._sent[0]
        self.assertTrue('Content-Type: multipart/mixed' in args[2])
        self.assertTrue('filename="blah.html"' in args[2])

    def test_mailbag(self):
        cfg = config.getConfiguration()
        smtpinfo = cfg.smtp_accs[0]

        emails = ['blah@blah.com', 'john@doe.com', 'blah@blah.com']
        mailserverinfo = mail2.MailServerInfo('host', 'x@y.com', 'Subject')
        with Mockery(emailtools, 'get_SMTP_class', lambda x: MockSMTP):
            mailbag = mail2.MailBag(emails, mailserverinfo)
            mailbag.data.append('1')
            mailbag.data.append('2')
            mailbag.force_send()
            mailbag.clear_queue()

        self.assertEquals(mailbag.num_emails_sent, 1)
        self.assertEquals(mailbag.num_parts_sent, 2)
        # 2 rather than 3, because blah@blah.com appears twice in email list


if __name__ == "__main__":

    unittest.main()

