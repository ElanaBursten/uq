# test_xmlconfigreader.py
# XXX Should be deprecated once we have completely switched over to
# ElementTree. Since xmlconfigreader.py relies on (objects returned by)
# xml.dom.minidom, we cannot convert that module nor these tests.

import site; site.addsitedir('.')
import traceback
import unittest
#
import xml.dom.minidom # TO BE DEPRECATED
#
import xmlconfigreader
from test_config import CONFIG1

class TestXMLConfigReader(unittest.TestCase):

    def parse_xml(self, data):
        try:
            return xml.dom.minidom.parseString(data)
        except:
            if verbose:
                traceback.print_exc()
            raise ValueError, "Error when parsing XML file"
            # how can we pass more useful information in here?

    def test_find_node(self):
        xml = self.parse_xml(CONFIG1)
        node = xmlconfigreader.find_node(xml, ['root'])
        self.assertEquals(node.nodeName, 'root')
        node = xmlconfigreader.find_node(xml, 
               ['root', 'ticketparser', 'ado_database'])
        self.assertEquals(node.nodeName, 'ado_database')
        self.assertEquals(node.getAttribute('host'), 'localhost')


if __name__ == "__main__":

    unittest.main()

