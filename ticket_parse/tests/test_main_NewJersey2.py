# test_main_NewJersey2.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import config
import main
import ticket_db
import ticketloader
import ticketparser
import tools

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs
from mockery_tools import *

class TestNewJersey2(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_NewJersey2',
          'format': 'NewJersey2',
          'processed': 'test_processed_NewJersey2',
          'error': 'test_error_NewJersey2',
          'attachments': ''},]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    def test_1(self):
        '''
        Test the check_summary_by_ticket_serial.sql sp
        Mantis 2281: new jersey 2 audit shows everything missing.
        '''
        self.prepare()
        all_tickets = tools.safeglob(
                      os.path.join("..", "testdata", "sample_tickets",
                      "NewJersey2", "GSU2-2009-04-20-*.txt"))

        # Drop all of the test tickets
        for i,ticket_file in enumerate(all_tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("NJ2_%d.txt" % i, raw, dir='test_incoming_NewJersey2')

        # parse the lot
        m = main.Main(verbose=0, call_center='NewJersey2')
        from StringIO import StringIO
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        rows = self.tdb.runsql_result(
         "exec check_summary_by_ticket_serial '2009/04/20', 'NewJersey2'")
        self.assertEquals(rows[-1]['found'],'1')

    @with_config_copy
    def test_2(self):
        '''
        Test the check_summary_by_ticket_serial.sql sp
        Mantis 2281: new jersey 2 audit shows everything missing.
        '''
        self.prepare()
        all_tickets = tools.safeglob(
          os.path.join("..", "testdata", "sample_tickets", "NewJersey2",
                       "GSU1-2009-06-19-00013.txt"))

        # Drop all of the test tickets
        for i, ticket_file in enumerate(all_tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("NJ2_%d.txt" % i, raw, dir='test_incoming_NewJersey2')

        # parse the lot
        m = main.Main(verbose=0, call_center='NewJersey2')
        from StringIO import StringIO
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()
        self.assertTrue('85 tickets found on summary' in log)

if __name__ == "__main__":
    unittest.main()

