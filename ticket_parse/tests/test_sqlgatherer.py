# test_sqlmap.py

import unittest
import site; site.addsitedir('.')
#
import sqlgatherer
import ticket_db
import _testprep

class TestSQLGatherer(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.sg = sqlgatherer.SQLGatherer(self.tdb)

    def test_declare_var(self):
        self.sg.declare_var('A')
        self.sg.declare_var('B', 'varchar(20)')

        keys = self.sg.vars.keys(); keys.sort()
        self.assertEquals(keys, ['@A', '@B'])

        self.assertEquals(self.sg.collected[0], 'declare @A int')
        self.assertEquals(self.sg.collected[1], 'declare @B varchar(20)')

        self.sg.return_vars()
        self.assertEquals(self.sg.collected[-1], "select @A as A, @B as B")



if __name__ == "__main__":

    unittest.main()
