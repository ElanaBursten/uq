# test_main_FTL5.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import call_centers
import config
import main
import ticket_db
import ticketloader
import ticketparser
import tools

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs
from mockery_tools import *

class TestFTL5(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def prepare(self):

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_FTL5',
          'format': 'FTL5',
          'processed': 'test_processed_FTL5',
          'error': 'test_error_FTL5',
          'attachments': ''},]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

        # make sure the FTL5 call center exists
        rows = self.tdb.getrecords('call_center',cc_code = 'FTL5')
        if not rows:
            _testprep.add_test_call_center(self.tdb, 'FTL5')
            self.delete_call_center = True
        else:
            self.delete_call_center = False

        # make sure the FTL* centers are active
        self.tdb.runsql("""
         update call_center
         set active = 1
         where cc_code in ('FTL1', 'FTL5');
        """)

        ucc = call_centers.get_call_centers(self.tdb)
        ucc.reload()

        # set FTL5 as an update call center for FTL1
        rows = self.tdb.getrecords('client', call_center='FTL1')
        self.client_orig = {}
        for row in rows:
            self.client_orig[row['client_id']] = row['update_call_center']
        # Set the update_call_center to FTL5
        for row in rows:
            self.tdb.updaterecord('client','client_id', row['client_id'],
             update_call_center='FTL5')

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)

        # Restore the update_call_center to what it was before
        for client_id, update_call_center in self.client_orig.iteritems():
            self.tdb.updaterecord('client', 'client_id', int(client_id),
             update_call_center=update_call_center)
        if self.delete_call_center:
            _testprep.delete_test_call_center(self.tdb, 'FTL5')

    @with_config_copy
    @with_call_center_copy
    def test_FTL5_1(self):
        self.prepare()

        f = open(os.path.join("..", "testdata", "sample_tickets", "FTL5",
            "ftl5-2008-10-22-00001.txt"))
        raw = string.join(f.readlines(), '')
        f.close()

        drop("FTL5.txt", raw, dir = 'test_incoming_FTL5')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FTL5')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates),5)
        self.assertEquals(t2.locates[0].client_code,'ALW')
        self.assertEquals(t2.locates[0].added_by,'PARSER')
        self.assertEquals(t2.ticket_format, 'FTL1')
        self.assertEquals(t2.source, 'FTL5')

    @with_config_copy
    @with_call_center_copy
    def test_FTL5_2(self):
        self.prepare()
        Oct_22_tickets = tools.safeglob(
          os.path.join("..", "testdata", "sample_tickets", "FTL5",
                       "ftl5-2008-10-22-*.txt"))

        # Drop all of the Oct 22 tickets
        for i,ticket_file in enumerate(Oct_22_tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("FTL5_%d.txt" % i, raw, dir='test_incoming_FTL5')

        # Parse the lot
        m = main.Main(verbose=0, call_center='FTL5')
        m.log.lock = 1
        m.run(runonce=1)
        self.assertEquals(len(m.ids),5)

        # Drop the audit
        f = open(os.path.join("..", "testdata", "sample_tickets", "FTL5",
            "ftl5-2008-10-23-00001.txt"))
        raw = string.join(f.readlines(), '')
        f.close()

        drop("FTL5_summary.txt", raw, dir='test_incoming_FTL5')
        # Parse the summary
        m = main.Main(verbose=0, call_center='FTL5')
        m.log.lock = 1
        m.run(runonce=1)

        # Was summary complete?
        row = self.tdb.getrecords('summary_header', call_center='FTL5')[0]
        self.assertEquals(row['complete'], '1')



if __name__ == "__main__":

    unittest.main()
