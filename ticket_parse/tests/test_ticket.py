# test_ticket.py

import os
import site; site.addsitedir('.')
import unittest
#
import assignment
import locate
import sqlgatherer
import ticket
import ticketloader
import ticketparser
import ticket_db
import tools
import _testprep

class TestTicket(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_001(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        # test __getattr__, __getitem__
        self.assertEquals(t.ticket_format, 'Atlanta')
        self.assertEquals(t['ticket_format'], 'Atlanta')

        ticket_id, locate_ids = t.insert(self.tdb)

        # check the effects on the ticket/locate objects...
        self.assertEquals(t.ticket_id, ticket_id)
        for loc in t.locates:
            self.assertEquals(loc.ticket_id, ticket_id)
            self.assert_(loc.locate_id)

        # check the results...
        tickets = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        # ^ a list of dicts
        # TODO: test some values, like image, dates, etc
        self.assertEquals(tickets[0]['transmit_date'], t.transmit_date)
        self.assertEquals(tickets[0]['image'].strip(), t.image.strip())

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), len(t.locates))
        # TODO: test values here too
        for loc in locates:
            self.assert_(loc['locate_id'])
            self.assertEquals(loc['status'], '-P')
            self.assertEquals(loc['closed'], '0')

    def test_load_from_data(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        results = self.tdb.get_ticket_data(ticket_id)
        tickets, locates, assignments = results
        self.assertEquals(len(tickets), 1)
        t2 = ticket.Ticket.load_from_ticket_data(tickets, locates, assignments)

        for fieldname, _ in ticket.Ticket.__fields__:
            if fieldname in ['ticket_id', 'image', 'modified_date', 'active',
                             'watch_and_protect']:
                continue
            value1 = getattr(t, fieldname)
            value2 = getattr(t2, fieldname)
            if value1 == value2 or str(value1) == value2:
                pass
            else:
                self.fail("Fields not equal: %r vs %r [%s]" % (
                 value1, value2, fieldname))

        self.assertEquals(len(t.locates), len(t2.locates))
        self.assertEquals(t.locates[0].client_code, t2.locates[0].client_code)
        self.assertEquals(t2.locates[0].ticket_id, ticket_id)

    def test_load(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        t2 = ticket.Ticket.load(self.tdb, ticket_id)

        for fieldname, _ in ticket.Ticket.__fields__:
            if fieldname in ['ticket_id', 'image', 'modified_date', 'active',
                             'watch_and_protect']:
                continue
            value1 = getattr(t, fieldname)
            value2 = getattr(t2, fieldname)
            if value1 == value2 or str(value1) == value2:
                pass
            else:
                self.fail("Fields not equal: %r vs %r [%s]" % (
                 value1, value2, fieldname))

        self.assertEquals(len(t.locates), len(t2.locates))
        self.assertEquals(t.locates[0].client_code, t2.locates[0].client_code)
        self.assertEquals(t2.locates[0].ticket_id, ticket_id)

    def test_check_field_lengths(self):
        t = ticket.Ticket()
        t.kind = "xyzzy"*10 # too long
        toolong = t.check_field_lengths()
        self.assertEquals(len(toolong), 1)
        self.assertEquals(toolong[0], ("kind", 20, len(t.kind), t.kind))

    def test_update_directly(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ticket_id = self.tdb.insertticket(t)

        # update via Ticket.update_directly:
        ticket.Ticket.update_directly(self.tdb, ticket_id,
         ticket_format="frobozz", timeout=30, kind='ABNORMAL')

        # check if updates were applied:
        t2 = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(t2.ticket_format, 'frobozz')
        self.assertEquals(t2.kind, 'ABNORMAL')

    def test_insert_with_assignments(self):
        # try inserting tickets w/locates that have assignments.
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        i = tools.findfirst(t.locates, lambda x: x.client_code == 'USW01')
        t.locates[i]._assignment = assignment.Assignment(300)
        ticket_id, loc_ids = t.insert(self.tdb)

        t2 = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(t.locates), len(t2.locates))
        j = tools.findfirst(t2.locates, lambda x: x.client_code == 'USW01')
        locj = t2.locates[j]
        self.assert_(locj._assignment)
        self.assertEquals(locj._assignment.locator_id, '300')
        self.assertEquals(locj._assignment.locate_id, locj.locate_id)
        self.assertEquals(locj.ticket_id, ticket_id)

    def test_update(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        ticket_id, loc_ids = t.insert(self.tdb)

        # let's add a locate
        t.locates.append(locate.Locate('ABC42'))
        # let's reset the status of another one
        i = tools.findfirst(t.locates, lambda x: x.client_code == 'USW01')
        t.locates[i].status = '-N'

        # do the update
        id2, lids2 = t.update(self.tdb)
        self.assertEquals(id2, ticket_id)
        self.assertEquals(len(loc_ids)+1, len(lids2))

        t2 = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(loc_ids)+1, len(t2.locates))
        j = tools.findfirst(t2.locates, lambda x: x.client_code == 'USW01')
        self.assertEquals(t2.locates[j].status, '-N')
        k = tools.findfirst(t2.locates, lambda x: x.client_code == 'ABC42')
        self.assert_(k >= 0)

    def test_insert_sg(self):
        # try inserting tickets w/locates that have assignments.
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        i = tools.findfirst(t.locates, lambda x: x.client_code == 'USW01')
        t.locates[i]._assignment = assignment.Assignment(300)

        sg = sqlgatherer.SQLGatherer(self.tdb)
        ticket_id, locate_ids = t.insert_sg(sg)

        t2 = ticket.Ticket.load(self.tdb, t.ticket_id)
        self.assertEquals(t.ticket_id, t2.ticket_id)
        self.assertEquals(len(t.locates), len(t2.locates))
        self.assertEquals(len(t.locates), len(locate_ids))
        for loc in t2.locates:
            self.assertEquals(loc.ticket_id, t2.ticket_id)
            self.assertEquals(loc.ticket_id, t.ticket_id)
            self.assert_(loc.locate_id)

    def test_update_sg(self):
        # first, insert a ticket... same as test_insert_sg
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        i = tools.findfirst(t.locates, lambda x: x.client_code == 'USW01')
        t.locates[i]._assignment = assignment.Assignment(300)

        sg = sqlgatherer.SQLGatherer(self.tdb)
        ticket_id, locate_ids = t.insert_sg(sg)

        #
        # then, retrieve that ticket and update it

        u = ticket.Ticket.load(self.tdb, ticket_id)
        for loc in u.locates:
            self.assertEquals(loc.changed(), False)

        # there should be one insert and one update
        u.locates[i].status = 'C' # change status of existing locate
        self.assertEquals(u.locates[i].changed(), True)
        newloc = locate.Locate('NIFF2003')
        u.locates.append(newloc)

        #sg.debug = 1
        ticket_id_2, locate_ids_2 = u.update_sg(sg)

        self.assertEquals(ticket_id_2, ticket_id)
        self.assertEquals(len(locate_ids_2), len(locate_ids)+1)
        self.assertEquals(len(locate_ids_2), len(u.locates))
        for loc in u.locates:
            self.assert_(loc.ticket_id)
            self.assert_(loc.locate_id)
            if loc.client_code == 'USW01':
                self.assertEquals(loc.status, 'C')
                self.assert_(loc._assignment)

        v = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(v.locates), len(locate_ids_2))
        for loc in v.locates:
            self.assert_(loc.ticket_id)
            self.assert_(loc.locate_id)
            if loc.client_code == 'USW01':
                self.assertEquals(loc.status, 'C')
                self.assert_(loc._assignment)

    def test_get_wo_number(self):
        """ Test extraction of the work order number, as inserted into the
            remarks section of a ticket by Lambert. """
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "fmw1-1.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Washington'])
        t.work_description += '\nBSW WO#V0002865\nSOME MORE STUFF HERE'
        wo_number = t.get_wo_number()
        self.assertEquals(wo_number, 'V0002865')

    def test_get_wo_number_2(self):
        """ Test extraction of the work order number from various tickets. """
        path = os.path.join(_testprep.TICKET_PATH, 'work-order-prelim',
               'wo-tickets')
        data = [
            ('FMB1', 'FMB1-2011-04-12-01805.txt', 'Baltimore', 'V0009148'),
            ('FMW4', 'FMW1-2011-04-12-01364.txt', 'Washington', 'V0009112'),
            ('FMW4', 'fmw4-2011-04-12-00742.txt', 'Washington4', 'V0009132'),
        ]
        for cc, fn, format, wo_number in data:
            full_path = os.path.join(path, cc, fn)
            tl = ticketloader.TicketLoader(full_path)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, [format])
            self.assertEquals(t.get_wo_number(), wo_number)


if __name__ == "__main__":

    unittest.main()

