# test_database.py
# Old tests, new module. :-)

import unittest
import site; site.addsitedir('.')
import string
import os
#
import date
import dbinterface_old as dbinterface
import locate
import parsers
import ticketparser
import ticket_db
import ticket as ticketmod
import ticketloader
import _testprep

class TestDatabase(unittest.TestCase):

    datadir = "../testdata/"
    tdb = ticket_db.TicketDB()  # i want to create this only once

    def setUp(self):
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def getTicket(self, filename, number):
        tl = ticketloader.TicketLoader(self.datadir + filename)
        for x in range(number):
            rawticket = tl.getTicket()
        return self.handler.parse(rawticket)

    def testASaveSampleTicket(self):
        # need this to go first.
        ticket = ticketmod.Ticket()
        ticket.ticket_number = 12345
        ticket.image = 'this is a test ticket'

        _testprep.deletetickets(self.tdb)
        self.assertEquals(0, self.tdb.getticketcount())
        self.tdb.insertticket(ticket)
        self.assertEquals(1, self.tdb.getticketcount())

        # this was a separate test, but I've wrapped it into another
        ticket = self.getTicket('atlanta-1.txt', 1)
        self.assertEquals('12311-109-056', ticket.ticket_number)
        self.assertNotEquals('', ticket.image)
        self.assertEquals(1, self.tdb.getticketcount())
        self.tdb.insertticket(ticket)
        self.assertEquals(2, self.tdb.getticketcount())

    def testSpecialChars(self):
        """ Test saving of tickets with special characters """
        ticket = ticketmod.Ticket()
        ticket.ticket_number = "12345"
        ticket.image = 'this is a test ticket'
        ticket.status = 'this < less than'
        ticket.work_address_street = 'with amper & sand&'
        ticket.work_city = 'bracket [me] in'
        ticket.work_state = '<>'
        ticket.con_name = 'RTEWD &&AND DOLA$ POUN# AT@AT STAR*'
        ticket.work_description = 'Sometimes I use a lot +_(*&**^%^#$!6 %%^&(* special characters'
        self.tdb.insertticket(ticket)

    def parseMany(self, file, separator, n):
        tl = ticketloader.TicketLoader(self.datadir + file, separator)
        rawTicket = tl.getTicket()
        while rawTicket <> None:
            ticket = self.handler.parse(rawTicket)
            try:
                self.tdb.insertticket(ticket)
            except dbinterface.DBException:
                #print ticket.tostring()
                raise

            rawTicket = tl.getTicket()
        tl.close()

    def testSaveManyBR(self):
        """ Test parsing and saving of many Baton Rouge tickets """
        self.parseMany('batonrouge1-1.txt', chr(12), 100)

    def testLongFields(self):
        """ Test saving of tickets with long fields """
        ticket = ticketmod.Ticket()
        ticket.ticket_number = '123-55-332'
        ticket.image = 'x' * 1200
        id = self.tdb.insertticket(ticket)
        loaded = self.tdb.getticket(id)
        self.assertEquals(ticket.ticket_number, loaded.ticket_number)

    def testMultiLine(self):
        """ Test loading of multiline tickets """
        ticket = ticketmod.Ticket()
        ticket.ticket_number = '555-444-333'
        ticket.image = "A line of text\n" * 5
        id = self.tdb.insertticket(ticket)
        self.assert_(id > 100)
        loaded = self.tdb.getticket(id)
        self.assertEquals(ticket.ticket_number, loaded.ticket_number)

        # This is broken, but I'm letting it slide for now.  It can save to the
        # database properly, but not load.  Multi line fields
        # get combined.
        #self.assertEquals(ticket.image, loaded.image)

    def testLocates(self):
        """ Test locates on tickets """
        ticket = ticketmod.Ticket()
        ticket.ticket_number = '555-444-333'
        ticket.image = "A line of text\n" * 5

        loc1 = locate.Locate('ABC1')
        loc2 = locate.Locate('DEF2')
        ticket.locates =[loc1, loc2]

        id = self.tdb.insertticket(ticket)   # save

        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 2)

        loaded = self.tdb.getticket(id)      # and reload
        self.assertEquals(ticket.ticket_number, loaded.ticket_number)

        self.assertEquals(len(ticket.locates), len(loaded.locates))
        self.assertEquals('ABC1', loaded.locates[0].client_code)

    def testError(self):
        """ Check if an invalid query really yields an error. """
        error = "Invalid object name 'bladebla'."
        sql = "select * from bladebla"  # table doesn't exist, of course
        try:
            data = self.tdb.runsql(sql)
        except dbinterface.DBException, e:
            # this is what we expect... an exception
            errmsg = e.args[0]
            self.assertEquals(str(errmsg), error)
        else:
            # no exception?! oops...
            self.fail("No DBException was raised")

    def testTicketWithoutTicketNumber(self):
        """ Can we post a Ticket w/o ticket_number? """
        t = ticketmod.Ticket()
        t.image = "nonsense"
        id = self.tdb.insertticket(t)

    def testNullValues(self):
        """ Test null values in the ticket table """
        tl = ticketloader.TicketLoader(self.datadir + "atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        t.transmit_date = None  # make sure it's None
        # post this ticket, which has at least one None value
        id = self.tdb.insertticket(t)
        # get this ticket back from the database
        t2 = self.tdb.getticket(id)
        # check if transmit_date is still None
        self.assertEquals(t2.transmit_date, None)

    def testUpdateTicket(self):
        """ Test updating of tickets """
        tl = ticketloader.TicketLoader(self.datadir + "atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        t.ticket_number = "123-456"
        del t.locates[4:]   # keep only 4 locates
        # post this ticket, so we have at least one such record
        id = self.tdb.insertticket(t)

        # now change the ticket...
        t.caller = "FRED KRUEGER"
        t.operator = "chucky"
        self.tdb.updateticket(id, t, newlocates=[locate.Locate("CUJO01")])

        # retrieve the ticket...
        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.caller, "FRED KRUEGER")
        self.assertEquals(t2.operator, "chucky")
        self.assertEquals(len(t2.locates), 5)
        self.assertEquals(t2.ticket_number, "123-456")

    def testErrorWithDollarSign(self):
        """ Test handling of dollar sign ($) in ticket fields """
        # Field values should not start with a $, because this has special
        # meaning to SQL Server. (variable?)
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","NorthCarolina-1.txt"))
        raw = tl.getTicket()
        raw = string.replace(raw, "CHURCHFIELD LN", "$CHURCHFIELD LN")
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)
        self.assertEquals(t.work_address_street, " $CHURCHFIELD LN")
        # try to post it -- this should *NOT* cause an error, because the
        # leading $ should have been prefixed by a space
        id = self.tdb.insertticket(t)

    def test_dbinterface_update(self):
        # add a test record
        import _testprep
        emp_id = _testprep.add_test_employee(self.tdb, "dookie")

        # update it
        self.tdb.update('employee', 'emp_id', emp_id, short_name='cookie')

        rows = self.tdb.getrecords('employee', emp_id=emp_id)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['short_name'], "cookie")

        # again, but now treat emp_id like an integer
        self.tdb.update('employee', 'emp_id', int(emp_id), short_name='bookie')

        rows = self.tdb.getrecords('employee', emp_id=emp_id)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['short_name'], "bookie")

    def test_insert_responder_queue(self):
        self.tdb.delete("responder_queue")
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Atlanta-1.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        t.do_not_respond_before = '2010-01-01 08:00:00'
        id = self.tdb.insertticket(t)

        locates = self.tdb.getrecords("locate")
        for row in locates:
            self.tdb.insert_responder_queue(row['locate_id'])

        rows = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(rows), len(locates))
        lid1 = locates[0]['locate_id']
        self.assertEquals(rows[0]['locate_id'], lid1)
        self.assertEquals(rows[0]['ticket_format'], 'Atlanta')
        self.assertEquals(rows[0]['client_code'], locates[0]['client_code'])
        self.assertEquals(rows[0]['do_not_respond_before'], '2010-01-01 08:00:00')

    def testZDeletion(self):
        """ Test mass deletion of tickets """
        _testprep.deletetickets(self.tdb)


# When this module is executed from the command-line, run all its tests
if __name__ == '__main__':

    unittest.main()

