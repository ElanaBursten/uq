# test_termtranslator.py

import os
import site; site.addsitedir('.')
import unittest
#
import locate
import termtranslator
import ticketloader
import ticketparser

DATA = """\
Atlanta  ATL01  ATL01;ATL02;ATL03
Atlanta  ATL20  FOOBAR
FCO1     THIS   THAT
"""

class TestTermTranslator(unittest.TestCase):

    def setUp(self):
        with open("test-translations.txt", "w") as f:
            f.write(DATA)

    def test_001(self):
        tt = termtranslator.TermTranslator("test-translations.txt")
        self.assert_(tt.data.has_key('Atlanta'))
        self.assert_(tt.data.has_key('FCO1'))
        self.assertEquals(tt.get_translations('FCV1'), {})
        self.assertEquals(tt.get_translations('FCO1'), {'THIS': ['THAT']})

    def test_002(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Atlanta"])
        t.locates = [locate.Locate(x) for x in ('ATL01', 'USW01', 'ATL20')]

        tt = termtranslator.TermTranslator("test-translations.txt")
        tt.translate(t)

        # ATL01 -> ATL01, ATL02, ATL03
        # ATL20 -> FOOBAR
        # USW01 -> left alone
        # therefore;
        self.assertEquals(len(t.locates), 5)
        self.assertEquals([c.client_code for c in t.locates],
                          ['ATL01', 'ATL02', 'ATL03', 'USW01', 'FOOBAR'])

    def test_003(self):
        # this uses the real translations.py file and may be subject to change
        tl = ticketloader.TicketLoader("../testdata/Houston1-1.txt")
        raw = tl.tickets[0].rstrip()
        raw = raw + "\nSend To: RELIAN01     Seq No: 0029   Map Ref:"
        raw = raw + "\nSend To: SUGAR01      Seq No: 0029   Map Ref:"

        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Houston1"])
        self.assertEquals(t.ticket_format, 'FHL1')

        #self.assertEquals(len(t.locates), 6)
        #self.assertEquals([c.client_code for c in t.locates],
        #                  ['TWC30', 'CP-P', 'CP-G', 'SUGAR-AT1', 'SUGAR-AT2', 'SUGAR-AT3'])
        # XXX we no longer replace RELIAN01 with CP-P and CP-G here

    def test_004(self):
        # this uses the real translations.py file and may be subject to change
        tl = ticketloader.TicketLoader("../testdata/qwest-ooc-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("USW10", "USA01")

        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['Oregon2'])
        self.assertEquals(t.ticket_format, 'LOR1')
        self.assertEquals([x.client_code for x in t.locates], ["CWS01"])

    def tearDown(self):
        try:
            os.remove("test-translations.txt")
        except:
            pass


if __name__ == "__main__":

    unittest.main()

