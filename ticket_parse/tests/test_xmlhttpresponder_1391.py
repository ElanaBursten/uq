# test_xmlhttpresponder_1391.py
# Tests for XMLHTTP responders like the one for 1391.

import datetime
import os
import string
import time
import unittest
#
import _testprep
import config
import date
import locate
import ticket_db
import ticketloader
import ticketparser
import xmlhttpresponder_data as xhrd

import test_xmlhttpresponder as testresp

class TestXMLHTTPResponder1391(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "Test Employee")
        self.cfg = config.getConfiguration()

    def add_config(self, call_center, clients=[]):
        # add default parameters when necessary...
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('1', ''), 'N': ('2', '')},
             'clients': clients[:],
             'all_versions': 0,
             'id': 'spam',
             'initials': 'UQ',
             'name': call_center,
             'post_url': 'http://some.post.url/',
             'resp_url':' http://tempuri.org/SomeResponseURL',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': [],
             'status_code_skip': [],
             'translations': {}}

        try:
            del self.cfg.xmlhttpresponders[call_center]
        except:
            pass
        self.cfg.xmlhttpresponders[call_center] = z
        self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z, unique=True)

    def _test_1391_type_responder(self, call_center, format, client_code,
                                  fac_type, ticket_src):

        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml" % call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1

        # get a ticket for this call center
        tl = ticketloader.TicketLoader(ticket_src)
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, [format])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=2)
        sql = """
         update locate
         set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
             closed_date = '%s'
         where locate_id = %s
        """ % (self.test_emp_id, locate_close_date.isodate(), locate_id)
        self.tdb.runsql(sql)

        rows = []

        xr = testresp.create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1

        # Verify that the responder can interpret success
        self.assertEqual(True, xr.responderdata.response_is_success(None, ('1', '1', 'Success')))
        self.assertEqual(True, xr.responderdata.response_is_success(None, ('1', '1', '250 OK')))
        self.assertNotEqual(True, xr.responderdata.response_is_success(None, ('1', '1', 'invalid response code')))
        self.assertNotEqual(True, xr.responderdata.response_is_success(None, ('1', '1', 'unknown facility type')))
        self.assertNotEqual(True, xr.responderdata.response_is_success(None, ('1', '1', 'Fake')))

        # Verify the FacilityType
        # Get the utility type from the client
        sql = """
         select utility_type
         from client
         where oc_code = '%s'
        """ % client_code
        results = self.tdb.runsql_result(sql)
        if results:
            if xr.responderdata.fac_types.has_key(results[0]["utility_type"]):
                fac = xr.responderdata.fac_types[results[0]["utility_type"]]
            else:
                fac = "O"
        else:
            fac = "O"
        self.assertEquals(fac, fac_type)

        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)

        # Verify the TicketNum
        data = testresp.find_soap_node(xr.xml_sent[0], "TicketNum",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data), t.ticket_number)

        # Verify the DispatchCode
        data = testresp.find_soap_node(xr.xml_sent[0], "DispatchCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data), client_code)

        # Verify the ResponseCode
        data = testresp.find_soap_node(xr.xml_sent[0], "ResponseCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data), xr.responderdata.mappings[status][0])

        data = testresp.find_soap_node(xr.xml_sent[0], "FacilityType",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data), fac_type)

        # Verify the LocateDate / LocateTime
        dat1 = testresp.find_soap_node(xr.xml_sent[0], "LocateDate",
               namespace="{http://tnresponse.korterraweb.com/}").text
        dat2 = testresp.find_soap_node(xr.xml_sent[0], "LocateTime",
               namespace="{http://tnresponse.korterraweb.com/}").text
        year1, month1, day1, hour1, min1, sec1, wday1, yday1, idst1 = \
            time.strptime(string.join([str(dat1),str(dat2)]),
            "%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime(year1,month1,day1,hour1,min1,sec1)
        d2 = datetime.datetime(locate_close_date.year,
                               locate_close_date.month,
                               locate_close_date.day,
                               locate_close_date.hours,
                               locate_close_date.minutes,
                               locate_close_date.seconds)
        # Account for the call center in a later time zone
        d2 = d2 - datetime.timedelta(hours=1)
        td = d2 - d1
        self.assert_(abs(td.seconds) < 60.0)

    #
    # actual tests

    def test_1391(self):
        self._test_1391_type_responder('1391', 'TN1391', 'B11', 'P',
         os.path.join(_testprep.TICKET_PATH, "1391-1.txt"))

    def test_3005(self):
        # Mantis #2719: same as 1391
        self.add_config('3005', clients=['MS4021', 'MS4022']) # dummy data
        self._test_1391_type_responder('3005', 'TN1391', 'B11', 'P',
         os.path.join(_testprep.TICKET_PATH, "1391-1.txt"))

    def test_FJL1(self):
        # MS4021 has utility_type 'elec', therefore 'E'
        self.add_config('FJL1', clients=['MS4021', 'MS4022'])
        self._test_1391_type_responder('FJL1', 'Mississippi2', 'MS4021', 'E',
         os.path.join(_testprep.TICKET_PATH, 'Mississippi2-1.txt'))

