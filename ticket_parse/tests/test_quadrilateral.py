# test_quadrilateral.py

import unittest
import site; site.addsitedir('.')
#
import quadrilateral

class TestQuadrilateral(unittest.TestCase):

    def test01(self):
        """
        A simple rectangle
        """
        points = [(0,0), (1,0), (1,1), (0,1)]
        q = quadrilateral.Quadrilateral(points)
        # The points should be re-arranged such that the vertices are
        # oriented clockwise
        self.assertEquals(q.x[0], 0)
        self.assertEquals(q.x[1], 0)
        self.assertEquals(q.x[2], 1)
        self.assertEquals(q.x[3], 1)
        self.assertEquals(q.y[0], 0)
        self.assertEquals(q.y[1], 1)
        self.assertEquals(q.y[2], 1)
        self.assertEquals(q.y[3], 0)

    def test02(self):
        """
        The centroid of a simple rectangle
        """
        points = [(0,0), (1,0), (1,1), (0,1)]
        q = quadrilateral.Quadrilateral(points)
        x_c,y_c = q.centroid()
        self.assertEquals(x_c,0.5)
        self.assertEquals(y_c,0.5)

    def test03(self):
        """
        A rectangle with negative normal
        """
        points = [(0,0), (1,1), (0,1), (1,0)]
        q = quadrilateral.Quadrilateral(points)
        # The points should be re-arranged such that the vertices are
        # oriented clockwise
        self.assertEquals(q.x[0], 0)
        self.assertEquals(q.x[1], 0)
        self.assertEquals(q.x[2], 1)
        self.assertEquals(q.x[3], 1)
        self.assertEquals(q.y[0], 0)
        self.assertEquals(q.y[1], 1)
        self.assertEquals(q.y[2], 1)
        self.assertEquals(q.y[3], 0)

    def test04(self):
        """
        A rectangle with degenerate edge
        """
        points = [(0,0), (1,0), (0,1), (1,0)]
        q = quadrilateral.Quadrilateral(points)
        # Oneof the points should have been deleted
        self.assertEquals(len(q.x),3)
        self.assertEquals(q.x[0], 0)
        self.assertEquals(q.x[1], 0)
        self.assertEquals(q.x[2], 1)
        self.assertEquals(q.y[0], 0)
        self.assertEquals(q.y[1], 1)
        self.assertEquals(q.y[2], 0)


if __name__ == "__main__":

    unittest.main()
