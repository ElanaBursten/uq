# test_all.py
# DEPRECATED; use nosetests instead.

# test suite that runs everything.
# Last update: 03 Mar 2002, Hans Nowak

import os
import unittest
import sys

def safe_issubclass(subklass, klass):
    try:
        return issubclass(subklass, klass)
    except:
        return False


if __name__ == "__main__":

    print "Running with Python", sys.version

    testmodules = [f[:-3] for f in os.listdir(".")
                   if f.startswith("test_") and f.endswith(".py")]
    testmodules.remove("test_all")

    suite1 = unittest.TestSuite()

    for modulename in testmodules:
        module = __import__(modulename)
        cases = [v for k, v in module.__dict__.items()
                 if safe_issubclass(v, unittest.TestCase)]
        for testcase in cases:
            s = unittest.TestLoader().loadTestsFromTestCase(testcase)
            suite1.addTest(s)
        print "adding:", modulename

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite1)

    # drawback: stuff in __init__ doesn't work as expected.

    # HACK
    try:
        os.mkdir('incoming')
    except:
        pass

