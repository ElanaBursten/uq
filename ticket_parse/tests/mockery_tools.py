# mockery_tools.py

from __future__ import with_statement
import copy
from mockery import Mockery, mock_returns

def with_config_copy(f):
    """ Decorator to run a test with a custom configuration (which is created
        and put in self.conf_copy). """
    def w(self):
        import config
        self.conf_copy = copy.deepcopy(config.getConfiguration())
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            f(self)
    w.__name__ = f.__name__
    w.__doc__ = f.__doc__
    return w

def with_call_center_copy(f):
    def w(self):
        import call_centers
        # apparently deepcopying the whole object doesn't work well, so we make
        # a shallow copy, then make sure the important data is copied
        self.cc_copy = copy.copy(call_centers.get_call_centers(self.tdb))
        self.cc_copy.cc = copy.deepcopy(self.cc_copy.cc)
        self.cc_copy.ucc = copy.deepcopy(self.cc_copy.ucc)
        with Mockery(call_centers, 'get_call_centers', lambda t: self.cc_copy):
            f(self)
    w.__name__ = f.__name__
    w.__doc__ = f.__doc__
    return w

# XXX We could log the calls to sendmail... or write a context manager/function
# that does this for us... this seems to come up every now and then.
def suppress_mail(f):
    """ Decorator to suppress the mail.sendmail function (i.e. no actual email
        is sent).
    """
    def w(self):
        import mail2
        with Mockery(mail2, 'sendmail', mock_returns(True)):
            f(self)
    w.__name__ = f.__name__
    w.__doc__ = f.__doc__
    return w

