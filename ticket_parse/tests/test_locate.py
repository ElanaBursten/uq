# test_locate.py

from __future__ import with_statement
import site; site.addsitedir('.')
import sys
import unittest
#
import assignment
import locate
import ticket_db
import ticketloader
import ticketparser
import _testprep
from mockery import Mockery

class TestLocate(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_load(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        ticket_id = self.tdb.insertticket(t)

        # get ids of locates that were added
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        locates.sort()  # this requires Locate.__cmp__
        lid = locates[0]['locate_id']

        # load id for first locate
        loc1 = locate.Locate.load(self.tdb, lid)
        self.assertEquals(loc1.client_code, 'AGL18')
        self.assertEquals(loc1._assignment, None)

        # assign a locator to that locate
        emp_id = _testprep.add_test_employee(self.tdb, 'LocateTest')
        sql = "exec assign_locate %s, %s" % (lid, emp_id)
        self.tdb.runsql(sql)

        # load id for first locate, again -- it should be assigned now
        loc1 = locate.Locate.load(self.tdb, lid)
        self.assertEquals(loc1.client_code, 'AGL18')
        a = loc1._assignment
        self.assertNotEqual(a, None)
        self.assert_(isinstance(a, assignment.Assignment))
        self.assertEquals(a.locate_id, lid)
        self.assertEquals(a.locator_id, emp_id)
        self.assertEquals(a.active, '1')

    def test_sort(self):
        # test if we can sort a list of Locates.
        locates = [locate.Locate(n) for n in ['ABC', 'ZZZ', 'DEF', 'GHI', 'AEA']]
        locates.sort()
        client_codes = [n.client_code for n in locates]
        self.assertEquals(client_codes, ['ABC', 'AEA', 'DEF', 'GHI', 'ZZZ'])

    def test_insert(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        ticket_id = self.tdb.insertticket(t)

        loc1 = locate.Locate('XYZ01')

        # try to insert it without a ticket id
        try:
            loc1.insert(self.tdb)
        except AssertionError:
            pass
        else:
            self.fail("AssertionError expected (ticket_id not set)")

        # set ticket_id and insert
        loc1.ticket_id = ticket_id
        lid1 = loc1.insert(self.tdb)
        self.assertEquals(loc1.ticket_id, ticket_id)
        self.assertEquals(loc1.locate_id, lid1)

        # load Locate and inspect it
        loc2 = locate.Locate.load(self.tdb, lid1)
        self.assertEquals(loc2.locate_id, lid1)
        self.assertEquals(loc2.client_code, loc1.client_code)

        # add locator and assignment, then update locate
        emp_id = _testprep.add_test_employee(self.tdb, 'LocateTest')
        a = assignment.Assignment(emp_id)
        loc2._assignment = a
        loc2.update(self.tdb)

        # load Locate and inspect it
        loc3 = locate.Locate.load(self.tdb, lid1)
        a = loc3._assignment
        self.assert_(isinstance(a, assignment.Assignment))
        self.assert_(a.assignment_id is not None)
        self.assertEquals(a.locate_id, lid1)

    def test_choose_and_assign(self):
        assignments = [{"active": '1', "insert_date":'2007-12-20'},
                       {"active": '1', "insert_date":'2007-12-19'}]
        loc = locate.Locate()
        locate.log.verbose = 0 # don't write to screen

        my_load_from_row = classmethod(lambda cls, stuff: stuff)
        with Mockery(assignment.Assignment, 'load_from_row', my_load_from_row):
            st = loc.choose_and_assign(assignments)

        self.assertEquals(st, 2) # multiple assignments were provided
        self.assertEquals(loc._assignment, assignments[0])

        # valid reason to check the log file: there should be a message
        # complaining about multiple assignments
        locate.log.logger.logfile.seek(0) # bleh
        lines = locate.log.logger.logfile.readlines()
        self.assert_('data.assignment 1:\n' in lines)
        self.assert_("{'active': '1', 'insert_date': '2007-12-19'}\n" in lines)
        self.assert_('data.assignment 0:\n' in lines)
        self.assert_("{'active': '1', 'insert_date': '2007-12-20'}\n" in lines)



if __name__ == "__main__":

    unittest.main()

