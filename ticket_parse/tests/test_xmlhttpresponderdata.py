# test_xmlhttpresponder.py
# Test retrieving of the XMLHTTPResponderData classes, and the methods of
# these classes.

import site; site.addsitedir('.')
import unittest
import xml.etree.ElementTree as ET
#
import callcenters
import date
import et_tools
import responder_config_data as rcd
import xmlhttpresponder_data as xhrd

class log:
    def __init__(self):
        self.buff = []
    def log_event(self, msg):
        self.buff.append(msg)

class TestXMLHTTPResponderData(unittest.TestCase):

    RESPONDERS = ['7501', 'SCA1', 'FBL1', 'FEF1', '9001', 'NewJersey5']

    # dummy config values to keep getresponderdata happy
    CONFIG = {
        'clients': [],
        'mappings': [],
        'name': "",
        'id': "",
        'initials': "",
        'post_url': "",
        'resp_url': "",
        'translations': [],
        'skip': [],
        'all_versions': 0,
        'send_emergencies': 0,
        'send_3hour': 0,
        'ack_dir': "",
        'ack_proc_dir': "",
    }

    def get_responderdata(self, name):
        class DummyConfig: pass
        config = DummyConfig()
        config.xmlhttpresponders = {}
        config.xmlhttpresponders[name] = self.CONFIG
        config.responderconfigdata = rcd.ResponderConfigData()
        # we don't use add() here, so ResponderConfigData is essentially empty
        config.responderconfigdata.gather_skip_data()
        # the following should not raise an error
        obj = xhrd.getresponderdata(config, name, self.CONFIG)
        return obj

    def test_get_xmlhttpresponderdata(self):
        for resp in self.RESPONDERS:
            obj = self.get_responderdata(resp)
            self.assert_(obj.__class__.__name__.endswith("ResponderData"))
            self.assert_(obj)

    DATA_SUCCESS = [
        # callcenter, row, response, result
        ('c7501', {}, ('', '', 'response posted'), True),
        ('c7501', {}, ('', '', 'bogus'), False),
        ('FBL1', {}, ('', '', '250 ok'), True),
        ('FBL1', {}, ('', '', '404 not ok'), False),
        ('SCA1', {}, ('foo', 'bar', 'baz'), True), # always True
    ]

    def test_response_is_success(self):
        for line in self.DATA_SUCCESS:
            call_center, row, response, result = line
            obj = self.get_responderdata(call_center)
            self.assertEquals(obj.response_is_success(row, response),
             result, "Unexpected result for: %r" % (line,))

    def test_1391_get_xml(self):
        rd = callcenters.get_xmlhttpresponderdata('c1391')()
        class tdb:
            def runsql_result(self,*args, **kwargs):
                return [{'utility_type': 'watr'}]

        rd.tdb = tdb()
        rd.log = log()
        rd.row = {'client_code': 'ABC',
                  'ticket_number': '1234'}
        rd.id = 'Id_&<>'
        rd.initials = 'Initials_&<>'
        names = {'membercode': '1234',
                 'responsecode': '1'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        rc = et_tools.find(doc, 'ResponseCollection')
        self.assertEquals(rc.get('LoginId'), 'Initials_&<>')
        self.assertEquals(rc.get('Password'), 'Id_&<>')
        data = et_tools.find(doc, 'TicketNum').text
        self.assertEquals(str(data), '1234')

    def test_1391_get_xml_2(self):
        """ Test that 1391's XMLHTTPResponderData.process_response() works. """
        rd = callcenters.get_xmlhttpresponderdata('c1391')()

        # create a mock reply and feed it to the responderdata object
        xml = """\
         <AckCollection>
         <Ack>
            <KeyInfo>012345678; ABC01</KeyInfo>
            <TicketNum>012345678</TicketNum>
            <DispatchCode>ABC01</DispatchCode>
            <FacilityType>W</FacilityType>
            <AckCode>0</AckCode>
            <AckCodeDescription>Success</AckCodeDescription>
         </Ack>
         </AckCollection>
        """
        class Reply: pass
        reply = Reply()
        reply.xml = xml
        reply.Text = 'ERROR'
        z = rd.process_response(reply)
        self.assertEquals(z, 'success') # ack code 0 == success

        xml = xml.replace('>0<', '>19<')
        reply.xml = xml
        z = rd.process_response(reply)
        self.assertEquals(z, 'ERROR') # bogus ack code == error

        xml = xml.replace('>19<', '>BOGUS<')
        reply.xml = xml
        z = rd.process_response(reply)
        self.assertEquals(z, 'ERROR') # bogus ack code == error

    def test_7501_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('c7501')()
        rd.row = {'client_code':   'ABC',
                  'ticket_number': 'TN_&><'}
        rd.id = 'Id_&<>'
        rd.initials = 'Initials_&<>'
        names = {'membercode':   '1234',
                 'ticket':       '5678',
                 'responsecode': '1'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'strID').text
        self.assertEquals(str(data), 'Id_&<>')
        data = et_tools.find(doc, 'strInitials').text
        self.assertEquals(str(data), 'Initials_&<>')

    def test_9001_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('c9001')()
        rd.log = log()
        def get_ticket_notes():
            return 'Notes &<>'
        rd.get_ticket_notes = get_ticket_notes
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        names = {'ticket':'1234',
                 'membercode':'1234',
                 'responsecode':'1',
                 'explanation':'Explanation'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'AuthID').text
        self.assertEquals(str(data),'Id_&<>')
        data = et_tools.find(doc, 'Notes').text
        self.assertEquals(str(data),'Notes &<>')

    def test_9003_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('c9003')()
        rd.log = log()
        def get_ticket_notes():
            return 'Notes &<>'
        rd.get_ticket_notes = get_ticket_notes
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        rd.ONE_CALL_CENTER = "OCC &<>"
        names = {'ticket':'1234',
                 'membercode':'1234',
                 'responsecode':'1',
                 'revision':'1',
                 'explanation':'Explanation'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'Authentication_Code').text
        self.assertEquals(str(data),'Id_&<>')
        data = et_tools.find(doc, 'One_Call_Center').text
        self.assertEquals(str(data),'OCC &<>')

    def test_FBL1_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('FBL1')()
        rd.log = log()
        def get_ticket_notes():
            return 'Notes &<>'
        rd.get_ticket_notes = get_ticket_notes
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        names = {'ticket':'1234',
                 'membercode':'1234',
                 'responsecode':'1'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'AuthID').text
        self.assertEquals(str(data),'Id_&<>')

    def test_FDX1_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('FDX1')()
        rd.log = log()
        def get_ticket_notes():
            return 'Notes &<>'
        rd.get_ticket_notes = get_ticket_notes
        rd.row = {'closed_date':None,
                  'first_arrival_date':date.Date().isodate()}
        rd.id = 'Id_&<>'
        rd.ONE_CALL_CENTER = "OCC &<>"
        names = {'ticket':'1234',
                 'responsecode':'1',
                 'membercode':'1234',
                 'revision':'1',
                 'explanation':'Explanation'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'Authentication_Code').text
        self.assertEquals(str(data),'Id_&<>')
        data = et_tools.find(doc, 'One_Call_Center').text
        self.assertEquals(str(data),'OCC &<>')
        data = et_tools.find(doc, 'Note').text
        self.assertEquals(str(data),'Notes &<>')

    def test_FEF1_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('FEF1')()
        rd.log = log()
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        names = {'ticket':'1234',
                 'membercode':'1234',
                 'responsecode':'1'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'AuthID').text
        self.assertEquals(str(data),'Id_&<>')

    def test_SCA1_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('SCA1')()
        rd.log = log()
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        rd.ONE_CALL_CENTER = "OCC &<>"
        names = {'ticket':'1234',
                 'responsecode':'1',
                 'membercode':'1234',
                 'revision':'1',
                 'explanation':'Explanation'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'Authentication_Code').text
        self.assertEquals(str(data),'Id_&<>')
        data = et_tools.find(doc, 'One_Call_Center').text
        self.assertEquals(str(data),'OCC &<>')

    def test_NewJersey_get_xml(self):

        rd = callcenters.get_xmlhttpresponderdata('NewJersey')()
        rd.log = log()
        rd.row = {'closed_date':None}
        rd.id = 'Id_&<>'
        names = {'ticket':'1234',
                 'membercode':'1234',
                 'responsecode':'1',
                 'explanation':'Explanation'}
        xml_blurb = rd.get_xml(names)
        doc = ET.fromstring(xml_blurb)
        data = et_tools.find(doc, 'AuthID').text
        self.assertEquals(str(data),'Id_&<>')

if __name__ == "__main__":

    unittest.main()

