# test_config_etree.py

from __future__ import with_statement
import os
import re
import site; site.addsitedir('.')
import unittest
import xml.etree.ElementTree as ET
#
import config_etree as config
import datadir
import et_tools
import _testprep

# NOTE: this test XML will also be used by text_xmlconfigreader.py!

CONFIG1 = \
r"""<?xml version="1.0" ?>
<root>
    <ticketparser>

        <ado_database
            host="localhost"
            database="TestDB"
            login="sa"
            password="password"
        />

        <server>
          <timezone>EST/EDT</timezone>
        </server>

        <!-- note: user and password will be needed later. -->

        <schema name="uq_schema.sql" />

        <!-- incoming and processed directories for main.py -->
        <process incoming="incoming" format=""
            processed="processed" error="error" group="1" />

        <process incoming="incoming_FWP2" format="FWP2"
            processed="processed" error="error" group="2" attachments="1" />

        <process incoming="dest1" format="FCL2"
            processed="c:/work/uq/ticket_parse/processed" error="error"
            group="3" attachments="1" attachment_dir="att1"
            attachment_proc_dir="att2" upload_location="fred"
            attachment_user="200" />

        <process incoming="incoming2" error="error"
            processed="processed2" format="" group="1" />

        <!-- multiple of these can be specified -->

        <!-- administrator info. multiple admins are possible. -->
        <admin name="Hans Nowak" email="vaporeon@wanadoo.nl" />
        <!-- <admin name="Hans Nowak" email="pikachu@hvision.nl" /> -->
        <smtp host="smtp.earthlink.net" from="logger@earthlink.net" />
        <!-- smtp server name, and "from" address -->

        <!-- in this directory we log errors -->
        <logdir name="logs" />

    </ticketparser>

    <ticketrouter>
        <cc_routing_list filename="blah" />
    </ticketrouter>

    <responder>
        <nodelete>
            <callcenter code="FCV1" />
            <callcenter code="FCV2" />
        </nodelete>

        <irth>
            <callcenter name="Atlanta" server="207.86.92.200" port="7377"
             clientbased="1">
                <logon login="USW01" password="1461" />
                <logon login="YIP01" password="1705" />
                <logon login="CTC04" password="1722" />
                <logon login="ATL01" password="10083" />
                <code_mappings>
                    <mapping uq_code="M" response="1" />
                    <mapping uq_code="NC" response="4" />
                    <mapping uq_code="XH" response="3" explanation="H" />
                    <mapping uq_code="O" response="3" explanation="G" />
                    <mapping uq_code="SC" response="Z" />
                    <mapping uq_code="NP" response="3" explanation="K" />
                    <mapping uq_code="OA" response="Z" />
                    <mapping uq_code="XD" response="6" />
                    <mapping uq_code="SV" response="Z" />
                    <mapping uq_code="C" response="4" />
                    <mapping uq_code="N" response="5" />
                    <mapping uq_code="MHP" response="2" explanation="A" />
                    <mapping uq_code="MPV" response="2" explanation="B" />
                    <mapping uq_code="XA" response="3" explanation="A" />
                    <mapping uq_code="XB" response="3" explanation="B" />
                    <mapping uq_code="XC" response="3" explanation="C" />
                    <mapping uq_code="XE" response="3" explanation="E" />
                    <mapping uq_code="XG" response="3" explanation="F" />
                    <mapping uq_code="XM" response="3" explanation="H" />
                    <mapping uq_code="NL" response="6" />
                </code_mappings>
                <translate_term from="A" to="B" />
            </callcenter>
            <callcenter name="FCV3" server="resp.vups.org" port="7377"
             clientbased="0">
                <logon login="UTIL11" password="wr!w3t$i" />
                <code_mappings>
                    <mapping uq_code="M" response="10" />
                    <mapping uq_code="ZM" response="11" />
                    <mapping uq_code="PM" response="12" />
                    <mapping uq_code="ZPM" response="13" />
                    <mapping uq_code="C" response="20" />
                    <mapping uq_code="ZC" response="21" />
                    <mapping uq_code="PC" response="22" />
                    <mapping uq_code="ZPC" response="23" />
                    <mapping uq_code="N" response="30" />
                    <mapping uq_code="ZN" response="31" />
                    <mapping uq_code="XC" response="40" />
                    <mapping uq_code="XN" response="42" />
                    <mapping uq_code="XQ" response="50" />
                    <mapping uq_code="XL" response="60" />
                    <mapping uq_code="O" response="60" />
                    <mapping uq_code="ZL" response="61" />
                    <mapping uq_code="XD" response="70" />
                    <mapping uq_code="XA" response="90" />
                    <mapping uq_code="XB" response="91" />
                    <mapping uq_code="XE" response="92" />
                    <mapping uq_code="XF" response="93" />
                    <mapping uq_code="XI" response="94" />
                    <mapping uq_code="XG" response="95" />
                    <mapping uq_code="XK" response="96" />
                    <mapping uq_code="NC" response="96" />
                    <mapping uq_code="XJ" response="97" />
                </code_mappings>
                <translate_term from="PEP07" to="PEP04" />
                <skip>
                    <term name="FOO01" />
                    <term name="FOO02" />
                </skip>
                <status_code_skip>
                    <code value="ZZZ" />
                </status_code_skip>
            </callcenter>
            <callcenter name="FTL2" server="207.86.92.200" port="7377">
                <logon login="CHAT50" password="592" />
                <logon login="SCB01" password="273" />
            </callcenter>
        </irth>

        <ftp>
            <output_dir name="ftp_out" />
            <callcenter name="FMW1" server="192.168.59.1"
                        login="XYZ" password="jan99X"
                        parsed_locates_only="0"
                        outgoing_dir="processed" temp_dir="">
                <code_mappings>
                    <mapping uq_code="NP" response="1" />
                    <mapping uq_code="S" response="1" />
                    <mapping uq_code="ST" response="1" />
                    <mapping uq_code="NC" response="1" />
                    <mapping uq_code="NL" response="1" />
                    <mapping uq_code="C" response="1" />
                    <mapping uq_code="M" response="2" />
                    <mapping uq_code="N" response="1" />
                    <mapping uq_code="O" response="5" />
                    <mapping uq_code="PM" response="9" />
                    <mapping uq_code="XG" response="3" />
                    <mapping uq_code="XR" response="4" />
                    <mapping uq_code="XB" response="A" />
                 </code_mappings>

                <translate_term from="PEP07" to="PEP04" />

                <exclude_state name="DC" />
             </callcenter>
        </ftp>
    </responder>

    <archiver root="c:/work/uq/ticket_parse/temp/"
              archive_dir="\temp"
              bzip2_path="\foo\bar\baz">
        <additional_dir path="x/y/z" />
        <additional_dir path="c:/qm/utl/data/ftp_out" />
    </archiver>
    <!-- archive_dir must always use backslashes! -->

    <integrity_check emails="hans.nowak@oasisdigital.com" />

    <placeholder />

</root>"""

FILENAME = "_test_config.xml"

class TestConfig(unittest.TestCase):

    def setUp(self):
        """ Write the _test_config.xml file. """
        f = datadir.datadir.open(FILENAME, "w")
        f.write(CONFIG1)
        f.close()

    def test_001_parsing(self):
        """ Test if this XML parses at all. If it does, check the values. """
        cfg = config.Configuration(FILENAME)

        self.assertEquals(cfg.ado_database['host'], 'localhost')
        self.assertEquals(cfg.ado_database['database'], 'TestDB')
        self.assertEquals(cfg.ado_database['login'], 'sa')
        self.assertEquals(cfg.ado_database['password'], 'password')

        self.assertEquals(cfg.processes, [
         {'incoming': 'incoming',
           'format': '',
           'processed': 'processed',
           'error': 'error',
           'group': '1',
           'attachment_dir': 'attachments',
           'attachment_proc_dir': '',
           'upload_location': '',
           'attachment_user': '',
           'attachments': 0,
           'accounts': [],
           'db_accounts': [],
           'geocode_latlong': 0},
         {'incoming': 'incoming_FWP2',
           'format': 'FWP2',
           'processed': 'processed',
           'error': 'error',
           'group': '2',
           'attachment_dir': 'attachments',
           'attachment_proc_dir': '',
           'upload_location': '',
           'attachment_user': '',
           'attachments': 1,
           'accounts': [],
           'db_accounts': [],
           'geocode_latlong': 0},
         {'incoming': 'dest1',
           'format': 'FCL2',
           'processed': 'c:/work/uq/ticket_parse/processed',
           'error': 'error',
           'group': '3',
           'attachment_dir': 'att1',
           'attachment_proc_dir': 'att2',
           'upload_location': 'fred',
           'attachment_user': '200',
           'attachments': 1,
           'accounts': [],
           'db_accounts': [],
           'geocode_latlong': 0},
         {'incoming': 'incoming2',
           'format': '',
           'processed': 'processed2',
           'error': 'error',
           'group': '1',
           'attachment_dir': 'attachments',
           'attachment_proc_dir': '',
           'upload_location': '',
           'attachment_user': '',
           'attachments': 0,
           'accounts': [],
           'db_accounts': [],
           'geocode_latlong': 0},
        ])

        self.assertEquals(cfg.responders["Atlanta"]["port"], "7377")
        self.assertEquals(cfg.responders["Atlanta"]["mappings"]["O"],
         ("3", "G"))
        self.assertEquals(cfg.responders["Atlanta"]["clientbased"], 1)

        for cc in ["FCV1", "FCV2", "Atlanta", "FCV3", "FTL2", "FMW1"]:
            self.assert_(cc in cfg.nodelete)
        self.assertEquals(len(cfg.nodelete), 6)

        self.assertEquals(cfg.responders["Atlanta"]["translations"]["A"], "B")
        self.assertEquals(cfg.responders["FCV3"]["translations"], {'PEP07': 'PEP04'})

        # as of Mantis #3022, this option is no longer recognized, but the
        # attribute is still supported
        self.assertEquals(cfg.cc_routing_file, "rules/cc_routing_list.tsv")

        self.assertEquals(cfg.responders["FCV3"]['status_code_skip'][0],'ZZZ')

        self.assertEquals(cfg.msmq, {}) # Mantis #2827

    def test_002_extraction(self):
        """ Test extraction of data through regular expressions. """
        admin_emails = config.extract_error_emails(CONFIG1)
        self.assertEquals(admin_emails,
         [{'name': 'Hans Nowak', 'email': 'vaporeon@wanadoo.nl'}, {'name':
          'Hans Nowak', 'email': 'pikachu@hvision.nl'}])

        smtp_data = config.extract_smtp_data(CONFIG1)
        self.assertEquals(smtp_data,
         ('smtp.earthlink.net', 'logger@earthlink.net'))

    def test_003_msmq(self):
        # Mantis #2827: parse msmq block
        CONFIG2 = CONFIG1.replace("<placeholder />",
          """<msmq>
               <method>OS</method>
               <server>MYSERVER</server>
               <queue>private$\\Q1</queue>
             </msmq>
          """)
        with datadir.datadir.open(FILENAME, "w") as f:
            f.write(CONFIG2)

        cfg = config.Configuration(FILENAME)
        self.assertEquals(cfg.msmq, {'method': 'OS', 'server': 'MYSERVER',
                                     'queue': r'private$\Q1'})

    def test_004_responders(self):
        cfg = config.Configuration(FILENAME)
        resp = cfg.responderconfigdata.get_responders()
        self.assertEquals(len(resp), 4)

        # parsed_locates_only is 1 by default:

        r = cfg.responderconfigdata.get_responders('FMW1')[0]
        self.assertEquals(r[2]['parsed_locates_only'], 0)

        r = cfg.responderconfigdata.get_responders('Atlanta')[0]
        self.assertEquals(r[2]['parsed_locates_only'], 1)

    def test_new_smtp(self):
        # check if new .smtp_accs attr looks OK. (#3026/#3027)
        cfg = config.Configuration(FILENAME)
        accs = cfg.smtp_accs
        self.assertEquals(len(accs), 1)
        self.assertEquals(accs[0].host, 'smtp.earthlink.net')

    def test_100_invalid_xml(self):
        """ What happens if the XML is invalid? """
        CONFIG2 = CONFIG1 + "<invalid\xbb"
        f = datadir.datadir.open(FILENAME, "w")
        f.write(CONFIG2)
        f.close()

        try:
            cfg = config.Configuration(FILENAME, verbose=0)
        except config.ConfigurationError:
            pass    # this is as expected
        except Exception, e:
            self.fail("ConfigurationError should have been raised; "\
                      "instead got: %r" % e.args)

    def test_prod_config(self):
        import config_etree as c2
        filename = os.path.join(_testprep.TICKET_PATH, 'config',
                   'test_etree.xml')
        cfge = c2.Configuration(filename)

        self.assertTrue(cfge.ftpresponders)

        # XXX test more attributes here

        # test if we have 'utilities' (Mantis #2743, #3023)
        self.assertEquals(len(cfge.utilities), 2)
        for name, path in cfge.utilities.items():
            self.assertTrue('bin' in path)
            self.assertTrue(os.path.exists(path),
             "Command line utility not found: %r" % path)

    def test_database_info(self):
        # specify both database info and connection string (=> error)
        BOGUS_DBINFO = """
          <ado_database host="foo"
                        database="bar"
                        login="this"
                        password="that"
                        conn_string="ho hum" />
        """
        CONFIG2 = re.sub("<ado_database.*?/>", BOGUS_DBINFO, CONFIG1,
                  flags=re.DOTALL|re.MULTILINE)
        with datadir.datadir.open(FILENAME, "w") as f:
            f.write(CONFIG2)

        try:
            cfg = config.Configuration(FILENAME, verbose=0)
        except config.ConfigurationError, e:
            # we expect an exception
            self.assertTrue("connection string" in e.args[0])
        except Exception, e:
            self.fail("ConfigurationError should have been raised; "\
                      "instead got: %r" % e.args)
        else:
            self.fail("ConfigurationError expected")


    def test_999(self):
        """ Clean up. """
        try:
            os.remove(FILENAME)
        except:
            pass

    def test_actual_config(self):
        # Mantis #2779
        # If you specify multiple blocks for the same call center in the same
        # responder section (e.g. <ftp>), then only the last one is kept.
        # This test verifies this; yet somehow UQ seems to be able to use
        # multiple blocks.
        path = os.path.join(_testprep.TICKET_PATH, "config",
               "test_etree2.xml")
        cfg = config.Configuration(path)
        d = cfg.ftpresponders['FMB1']
        self.assertEquals(d['login'], 'md-lambertcbl2')

    def test_invalid_xml(self):
        # invalid XML; should not be parsed at all
        XML = """\
<?xml version="1.0" ?>
<root>
  text one
  <account this="1" that="2" />
     bogus="3" />
  <account this="4" that="5" />
  text two
</root>
        """
        xml = ET.fromstring(XML)
        try:
            et_tools.validate(xml)
        except et_tools.XMLValidateError, e:
            pass # ok
        else:
            self.fail("XMLValidateError expected")

        xml = ET.parse(os.path.join(_testprep.TICKET_PATH, "config",
              "test_etree2.xml"))
        et_tools.validate(xml)

        xml = ET.parse(os.path.join(_testprep.TICKET_PATH, "config",
              "test_etree.xml"))
        et_tools.validate(xml)

    def test_invalid_XML_2(self):
        XML = """\
<?xml version="1.0" ?>
<root>
  text one
  <error <account this="1" that="2" />
     bogus="3" />
  <account this="4" that="5" />
  text two
</root>
        """

        try:
            xml = ET.fromstring(XML)
        except Exception, e:
            self.assertTrue('not well-formed' in repr(e))
            # using repr() here to make things work for both Python 2.5 and
            # 2.7 (temporarily)
        else:
            self.fail("Invalid XML: exception expected")


if __name__ == "__main__":

    unittest.main()
