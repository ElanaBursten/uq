# test_sqlmap.py

import unittest
import site; site.addsitedir('.')
#
import sqlmap
import sqlmaptypes as smt
import ticket_db
import _testprep

#
# Test class for SQLMap

class Client(sqlmap.SQLMap):
    __table__ = "client"
    __key__ = "client_id"

    __fields__ = [
        ("client_id", smt.sqlint),
        ("client_name", smt.sqlstring(40)),
        ("oc_code", smt.sqlstring(10)),
        ("office_id", smt.sqlint),
        ("modified_date", smt.sqldatetime),
        ("active", smt.sqlbool),
        ("call_center", smt.sqlstring(20)),
        ("update_call_center", smt.sqlstring(20)),
        ("grid_email", smt.sqlstring(255)),
        ("allow_edit_locates", smt.sqlbool),
    ]

#
# Unit tests

class TestSQLMap(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center "\
         "from client where oc_code = 'USW01'"
        self.result_USW01 = self.tdb.runsql_result(sql)[0]
        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'],
         "USW01")

    def tearDown(self):
        # Set it back inactive
        _testprep.set_client_inactive(self.tdb,
         self.result_USW01['call_center'], "USW01")

    def test_types(self):
        def getfielddef(fieldname):
            for name, sqltype in Client.__fields__:
                if name == fieldname:
                    return sqltype
            raise KeyError, "Field not found: %s" % (name,)

        a = getfielddef('oc_code')
        self.assert_(isinstance(a, smt.SQLString))
        self.assertEquals(a.width, 10)

    def test_001_load(self):
        c = Client.load(self.tdb, 300)
        self.assertEquals(c.client_id, '300')
        self.assertEquals(c.oc_code, 'USW01')
        self.assertEquals(c.active, '1')
        self.assertEquals(len(c._data), len(c.__fields__))

    def test_002_insert(self):
        # delete old test data
        self.tdb.deleterecords("client", oc_code="HAK01*")

        # create a Client instance and insert it
        c = Client()
        c.set(client_name="Harkebak", oc_code="HAK01*", office_id=100,
              active=1, call_center="Atlanta",
              modified_date="2004-01-01 12:13:14")
        self.assertEquals(c.oc_code, "HAK01*")

        sql, values = c._sql_insert()
        # TODO: inspect these values...?

        id = c.insert(self.tdb)

        # check if it was actually posted
        rows = self.tdb.getrecords("client", oc_code="HAK01*")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_name'], 'Harkebak')
        self.assertEquals(rows[0]['office_id'], '100')
        self.assertEquals(rows[0]['active'], '1')
        self.assertEquals(rows[0]['modified_date'], '2004-01-01 12:13:14')
        self.assertEquals(rows[0]['client_id'], id)

        # test if client_id has been set
        self.assertEquals(c.client_id, id)

    def test_003_update(self):
        # delete old test data
        self.tdb.deleterecords("client", oc_code="HAK01*")

        # create a Client instance and insert it
        c = Client()
        c.set(client_name="Harkebak", oc_code="HAK01*", office_id=100,
              active=1, call_center="Atlanta",
              modified_date="2004-01-01 12:13:14")
        self.assertEquals(c.oc_code, "HAK01*")

        sql, values = c._sql_insert()
        # TODO: inspect these values...?

        id = c.insert(self.tdb)

        # now get this Client again, modify it, and update it
        d = Client.load(self.tdb, id)
        d.office_id = 109
        d.modified_date = '2004-02-02 08:09:10'
        d.active = 0
        d.grid_email = "hans@hans.com"

        sql, values = d._sql_update()
        id = d.update(self.tdb)

        e = Client.load(self.tdb, id)
        self.assertEquals(e.office_id, '109')
        self.assertEquals(e.grid_email, 'hans@hans.com')

    def test_004_save(self):
        clients = []
        c = Client.new(active=1, office_id=100, client_name='FOO1',
            oc_code='FOO1', call_center='FMW1')
        clients.append(c)
        c = Client.load(self.tdb, 300)
        clients.append(c)

        # use save(), so the system figures out itself whether to use an
        # insert or an update
        ids = []
        for client in clients:
            id = client.save(self.tdb)
            ids.append(id)

        self.assertEquals(clients[0].client_id, ids[0])
        self.assertEquals(clients[1].client_id, ids[1])

        # try saving again; this should cause no errors
        for client in clients:
            client.save(self.tdb)

    def test_005_changes(self):
        # make sure precondition is met
        sql = """
         update client
         set office_id = 100
         where client_id = 300
        """
        self.tdb.runsql(sql)

        c = Client.new(active=1, office_id=100, client_name='FOO1',
            oc_code='FOO1', call_center='FMW1')
        self.assertEquals(len(c._changed), 5)
        c.reset_changes()
        self.assertEquals(c.changed(), False)
        c.oc_code = c.client_name = 'XYZZY'
        self.assertEquals(c.changed(), True)
        self.assertEquals(len(c._changed), 2)
        old = {'oc_code': 'FOO1', 'client_name': 'FOO1'}
        self.assertEquals(c._changed, old)
        c.oc_code = 'ABCDE' # this doesn't change the _changed dict
        self.assertEquals(c._changed, old)

        # try another client
        c = Client.load(self.tdb, 300)
        sql, values = c._sql_update(force=0)
        self.assertEquals(len(values), 1)
        sql, values = c._sql_update(force=1)
        self.assertEquals(len(values), 8) # 8 fields

        c.office_id = 101
        self.assertEquals(c._changed, {'office_id': '100'})
        sql, values = c._sql_update(force=0)
        self.assertEquals(len(values), 2)
        c.save(self.tdb)
        # should have been updated
        self.assertEquals(c.stats['updated'], ['300'])
        self.assertEquals(c.stats['skipped'], [])

        # save again, without changing anything
        c.save(self.tdb)
        # should have been skipped
        self.assertEquals(c.stats['skipped'], ['300'])
        self.assertEquals(c.stats['updated'], [])



if __name__ == "__main__":

    unittest.main()
