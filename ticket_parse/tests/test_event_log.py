# test_event_log.py

import site; site.addsitedir('.')
import unittest
#
import config
import errorhandling2
import event_log
import ticket_db

class TestEventLog(unittest.TestCase):

    def setUp(self):
        self.config = config.getConfiguration(reload=True)
        self.tdb = ticket_db.TicketDB()
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
         me="EventLogTest")
        self.log.lock = 1
        self.event_log = event_log.EventLog(self.tdb, self.log, store_events=1)

    def test_log_event(self):
        def get_events():
            return self.tdb.runsql_result(
             "select * from event_log order by version_num desc")

        self.tdb.deleterecords('event_log')
        rows = get_events()
        self.assertEquals(len(rows), 0)

        # Test various combinations of default_version and max_version

        # Version 1 doesn't exist yet, so the event should be added
        logged, version = self.event_log.log_event(1, 1000, 'event 1',
         default_version=1, max_version=1)
        self.assertTrue(logged)
        self.assertEquals(version, 1)
        rows = get_events()
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['aggregate_type'], '1')
        self.assertEquals(rows[0]['aggregate_id'], '1000')
        self.assertEquals(rows[0]['version_num'], '1')
        self.assertEquals(rows[0]['event_data'], 'event 1')
  
        # This should fail, since version 1 already exists
        logged, version = self.event_log.log_event(1, 1000, 'event 2',
         default_version=1, max_version=1)
        self.assertFalse(logged)
        self.assertTrue(version > 1)
        rows = get_events()
        self.assertEquals(len(rows), 1)

        # Only version 1 exists, so version 2 should be added
        logged, version = self.event_log.log_event(1, 1000, 'event 3',
         default_version=1, max_version=2)
        self.assertTrue(logged)
        self.assertEquals(version, 2)
        rows = get_events()
        self.assertEquals(len(rows), 2)
        self.assertEquals(rows[0]['aggregate_type'], '1')
        self.assertEquals(rows[0]['aggregate_id'], '1000')
        self.assertEquals(rows[0]['version_num'], '2')
        self.assertEquals(rows[0]['event_data'], 'event 3')

        # This should fail, since versions 1 and 2 already exist
        logged, version = self.event_log.log_event(1, 1000, 'event 4',
         default_version=1, max_version=2)
        self.assertFalse(logged)
        self.assertTrue(version > 2)
        rows = get_events()
        self.assertEquals(len(rows), 2)

        # Event should be added, since there is no max_version
        logged, version = self.event_log.log_event(1, 1000, 'event 5',
         default_version=1)
        self.assertTrue(logged)
        self.assertEquals(version, 3)
        rows = get_events()
        self.assertEquals(len(rows), 3)
        self.assertEquals(rows[0]['aggregate_type'], '1')
        self.assertEquals(rows[0]['aggregate_id'], '1000')
        self.assertEquals(rows[0]['version_num'], '3')
        self.assertEquals(rows[0]['event_data'], 'event 5')

        # Event should be added, since there is no max_version
        logged, version = self.event_log.log_event(1, 1000, 'event 6')
        self.assertTrue(logged)
        self.assertEquals(version, 4)
        rows = get_events()
        self.assertEquals(len(rows), 4)
        self.assertEquals(rows[0]['aggregate_type'], '1')
        self.assertEquals(rows[0]['aggregate_id'], '1000')
        self.assertEquals(rows[0]['version_num'], '4')
        self.assertEquals(rows[0]['event_data'], 'event 6')


if __name__ == "__main__":

    unittest.main()
