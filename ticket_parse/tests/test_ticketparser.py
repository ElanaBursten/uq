# Utiliquest Ticket Parser
# Test individual tickets and their attributes.
# (For bulk parsing, see test_ticketparser_bulk.py.)

# XXX Module is very long; we could probably move much of the data to be tested
# to separate files in text format

import os
import site; site.addsitedir('.')
import string
import unittest
#
import call_centers
import datadir
import date
import dbinterface_old as dbinterface
import parsers
import test_ticketloader
import ticket as ticketmod
import ticket_db
import ticketloader
import ticketparser
import tools
import _testprep
from re_shortcut import R

glob = tools.safeglob

class TestTicketParser(unittest.TestCase):

    datadir = "../testdata/"

    tdb = ticket_db.TicketDB()  # i want to create this only once

    def setUp(self):
        self.handler = ticketparser.TicketHandler()

    def getTicket(self, filename, number):
        tl = ticketloader.TicketLoader(self.datadir + filename)
        for x in range(number):
            rawticket = tl.getTicket()
        return self.handler.parse(rawticket)

    def test_get_parser(self):
        # this class must be available in parsers.py
        # moving all parsers into formats
        # this class must be available in the 'formats' package
        klass = parsers.get_parser("HoustonKorterra")
        self.assert_(str(klass).endswith("HoustonKorterra.Parser"))
        klass = parsers.get_parser("BatonRouge1")
        self.assert_(str(klass).endswith("BatonRouge1.Parser"))

    def testAtlantaParser1(self):
        """ Test attributes of AtlantaParser against first ticket in
            atlanta-1.txt. """
        t = self.getTicket("atlanta-1.txt", 1)
        ae = self.assertEquals  # shortcut
        ae(t.ticket_number, "12311-109-056")
        ae(t.revision, "000")
        ae(t.call_date, "2001-12-31 10:11:00")
        ae(t.transmit_date, "2001-12-31 10:14:39")
        ae(t.work_state, "GA")
        ae(t.work_county, "FULTON")
        ae(t.work_city, "ATLANTA")
        ae(t.work_address_number, "")
        self.assert_(t.work_address_street.startswith("SPRING GARDEN"))
        self.assert_(t.work_address_street.endswith("SW"))
        ae(t.work_cross, "")
        self.assert_(t.work_description.startswith("LOCATE BOTH"))
        self.assert_(t.work_description.endswith("BETWEEN"))
        #self.assert_(len(t.grids) == 4)
        #ae(t.grids[0], "3341C8424B")
        ae(t.work_type, "NEW GAS RENEWAL REPLACING GAS")
        ae(t.work_date, "2001-11-02 07:00:00")
        ae(t.work_notc, "000")
        ae(t.priority, "3")
        ae(t.legal_date, "2002-01-04 07:00:00")
        ae(t.legal_good_thru, "2002-01-21 00:00:00")
        ae(t.legal_restake, "2002-01-16 00:00:00")
        ae(t.respond_date, "2002-01-03 23:59:00")
        ae(t.duration, "2 WKS")
        ae(t.company, "AGL")
        self.assert_(t.work_remarks.startswith("RESTAKE"))
        self.assert_(t.work_remarks.endswith("ROAD"))
        ae(t.con_name, "GENERAL PIPELINE COMPANY")
        ae(t.con_address, "4090 BONSAL ROAD")
        ae(t.con_city, "CONLEY")
        ae(t.con_state, "GA")
        ae(t.con_zip, "30027")
        ae(t.caller, "TONYA MASON")
        ae(t.caller_phone, "404-361-0005")
        ae(t.caller_fax, "404-361-1509")
        ae(t.caller_altphone, "")
        ae(t.operator, "109")
        ae(t.channel, "999")
        self.assertEquals(t._dup_fields, [])

        # The first ticket of atlanta-1.txt has 4 grids.
        # 3341C8424B   3341C8424A   3341D8424B   3341D8424A
        self.assertAlmostEqual(t.work_lat, 33.6875,4)
        self.assertAlmostEqual(t.work_long, -84.4125,4)

    def testAtlantaParser2(self):
        """ Test attributes of AtlantaParser against first ticket in
            atlanta-2.txt. """
        t = self.getTicket("atlanta-2.txt", 1)
        ae = self.assertEquals  # shortcut
        ae(t.ticket_number, "12171-039-011")
        ae(t.revision, "000")
        ae(t.work_state, "GA")
        ae(t.work_county, "FULTON")
        ae(t.work_city, "ROSWELL")
        ae(t.work_address_number, "8930")
        ae(t.work_address_street, "MARTIN RD")
        ae(t.work_cross, "")
        ae(t.work_description, "LOC PERIMETER OF HOME")
        ae(t.work_subdivision, "MARTINS LANDING")
        ae(t.work_type, "INSTL SENTRICON")
        ae(t.work_date, "2001-12-20 07:00:00")
        ae(t.work_notc, "070")
        ae(t.priority, "3")
        ae(t.legal_date, "2001-12-20 07:00:00")
        ae(t.legal_good_thru, "2002-01-07 00:00:00")
        ae(t.legal_restake, "2002-01-02 00:00:00")
        ae(t.respond_date, "2001-12-19 23:59:00")
        ae(t.duration, "1 HOUR")
        ae(t.company, "ERIC LAIDHOLD")
        ae(t.con_name, "COOKS PEST CONTROL")
        ae(t.con_state, "GA")
        ae(t.con_zip, "30043")
        ae(t.con_address, "1830 ATKINSON RD")
        ae(t.con_city, "LAWRENCEVILLE")
        ae(t.caller, "CHRIS ESERY")
        ae(t.caller_phone, "770-277-5470")
        ae(t.caller_fax, "")
        ae(t.caller_altphone, t.caller_phone)
        self.assert_(len(t.locates) == 13)
        ae(t.locates[0].client_code, "ACS02")
        self.assertEquals(t._dup_fields, [])
        self.assertAlmostEqual(t.work_lat, 33.9979166667,5)
        self.assertAlmostEqual(t.work_long, -84.3270833333)


    def testAtlantaParser3(self):
        t = self.getTicket("atlanta-1.txt", 3)
        # Verifies that only the name of the contact is stored and not the
        # (optional) "Phone :" part.
        self.assertEquals(t.caller_contact, "DICK STEPHENS")

        t = self.getTicket("atlanta-1.txt", 5)
        self.assertEquals(t.caller_contact, "BOB MOORE")

        tl = ticketloader.TicketLoader("../testdata/Atlanta-6.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Atlanta"])
        self.assertEquals(t.caller_contact, "RANDY LEDFORD")
        #self.assertEquals(t.caller_fax, "678-986-4663")
        # there's two fax fields on the ticket; parser finds first one

    def testWhiteSpace(self):
        """ Breaks if any string attributes have leading or trailing
            whitespace. """
        for i in range(1, 4+1):
            t = self.getTicket("atlanta-1.txt", i)
            for key, value in t.__dict__.items():
                if not key.startswith("_") and key != "image":
                    if type(value) == type(""):
                        culprit = "[ticket %d] %s: %s" % (i, key, `value`)
                        self.assert_(not value.startswith(" "), culprit)
                        self.assert_(not value.endswith(" "), culprit)


    def checkField(self, value):
        svalue = str(value)
        self.assertNotEquals('', value)
        self.assertEquals(svalue.strip(), svalue)

    def parseMany(self, file, separator, n, minTickets):
        tl = ticketloader.TicketLoader(self.datadir + file, separator)
        i = 0
        rawTicket = tl.getTicket()
        while rawTicket <> None and i<n:
            i = i + 1
            ticket = self.handler.parse(rawTicket)
            # print i
            self.checkField(ticket.ticket_number)
            self.checkField(ticket.kind)
            self.checkField(ticket.work_address_street)
            self.checkField(ticket.work_city)
            self.checkField(ticket.work_state)
            self.checkField(ticket.company)
            self.checkField(ticket.con_name)
            self.checkField(ticket.con_address)
            self.checkField(ticket.con_state)
            self.checkField(ticket.con_state)
            self.checkField(ticket.con_city)
            self.checkField(ticket.con_zip)

            # this one can have white space.
            self.assertNotEquals('', ticket.image)

            if ticket.ticket_format == "Atlanta":
                self.assertNotEquals(ticket.work_long, 0.0)
                self.assertNotEquals(ticket.work_lat, 0.0)

            rawTicket = tl.getTicket()
        if i < minTickets:
            self.fail( 'too few tickets' )


    def testParseAtlanta3(self):
        tl = ticketloader.TicketLoader(self.datadir + "atlanta-3.txt")
        t = tl.getTicket()
        while t:
            # How to test this so that we can give a meaningful error
            # message? I want to know in which ticket the error occurred...
            t = self.handler.parse(t)

            t = tl.getTicket()

    def test_isodate(self):
        """ Test tools.isodate function. """
        #bp = parsers.AtlantaParser("dummy") # just any old parser should do
        self.assertEquals(tools.isodate("02/09/02"), "2002-02-09 00:00:00")
        self.assertEquals(tools.isodate("02/09/02 16:05"),
         "2002-02-09 16:05:00")
        # does it handle AM/PM?
        self.assertEquals(tools.isodate("1/03/02 10:30 PM"),
         "2002-01-03 22:30:00")

    def test_longlat(self):
        """ Test if longitude and latitude are filled correctly. """
        tl = ticketloader.TicketLoader("../testdata/atlanta-5.txt")
        raw = tl.getTicket()
        while raw:
            t = self.handler.parse(raw, ['Atlanta'])
            self.assertNotEquals(t.work_long, "0.0")
            self.assertNotEquals(t.work_lat, "0.0")
            #print t.work_long, t.work_lat
            raw = tl.getTicket()

    def testOptionalFieldsAtlanta(self):
        """ Test caller_email, caller_contact, and caller_cellular. These
            fields don't always appear in the ticket, so we must test certain
            ticket numbers for their presence.
        """
        tl = ticketloader.TicketLoader("../testdata/atlanta-3.txt")
        raw = tl.getTicket()
        while raw:
            t = self.handler.parse(raw, ['Atlanta'])
            if t.ticket_number == "10291-028-013":
                self.assertEquals(t.caller_email, "BDBRASWE@SOUTHERNCO.COM")
            elif t.ticket_number == "10291-017-033":
                self.assertEquals(t.caller_contact, "DOUG")
                self.assertEquals(t.caller_cellular, "770-560-2930")
                # members look slightly different in atlanta-3.txt:
                self.assert_(len(t.locates) == 11)
                self.assert_(t.locates[0].client_code == "AGL10")

            raw = tl.getTicket()

    def test_line_noise(self):
        tl = ticketloader.TicketLoader("../testdata/linenoise-1.txt")
        for raw in tl.tickets:
            self.assert_(tools.is_linenoise(raw),
             "Not recognized as line noise: " + `raw`)

    def test_BatonRouge1_1(self):
        tl = ticketloader.TicketLoader("../testdata/batonrouge1-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['BatonRouge1'])

        self.assertEquals(t.ticket_number, "2050")
        self.assertEquals(t.ticket_type, "48HR-48 HOURS NOTICE")
        self.assertEquals(t.kind, "NORMAL")
        self.assertEquals(t.call_date, "2002-01-03 10:18:00")
        self.assertEquals(t.operator, "casandra")
        self.assertEquals(t.con_name, "RELIANT ENERGY/ENTEX")
        self.assertEquals(t.caller_contact, "JEFF")
        self.assertEquals(t.caller_phone, "(337) 477-0070")
        self.assertEquals(t.caller_altcontact, "OFFICE #")
        self.assertEquals(t.caller_altphone, "(337) 475-6368")
        self.assertEquals(t.work_type, "INSTALLING GAS SERVICE")
        self.assertEquals(t.company, "DOUSETT SHEET METAL SHOP")
        self.assertEquals(t.work_state, "LA")
        self.assertEquals(t.work_county, "CALCASIEU")
        self.assertEquals(t.work_city, "LAKE CHARLES CITY")
        self.assertEquals(t.work_address_number, "")
        self.assertEquals(t.work_address_street, "1ST ST")
        self.assertEquals(t.work_cross, "2ND AVE")
        self.assertEquals(t.work_date, "2002-01-07 10:30:00")
        self.assertEquals(t.legal_due_date, "2002-01-07 10:30:00")
        self.assertEquals(len(t.locates), 5)
        self.assertEquals(t._dup_fields, [])
        self.assertEquals(t.image, t.image.lstrip())

        # longitude and latitude
        lat = 30 + 13/60.0 + 33/(60.0*60.0)
        long1 = -(93 + 12/60.0 + 65/3600.0)
        long2 = -(93 + 11/60.0 + 54/3600.0)
        long = (long1 + long2) / 2
        self.assertAlmostEqual(t.work_lat, lat,5)
        self.assertAlmostEqual(t.work_long, long,5)

        raw = raw.replace('Ex. Coord: NW Lat: 30 13 34  Lon: -93 12 65  SE Lat: 30 13 32  Lon:  -93 11 54',
                          '')
        t = self.handler.parse(raw, ['BatonRouge1'])
        self.assertEquals(t.work_lat, 0)
        self.assertEquals(t.work_long, 0)

        raw = raw.replace('Ex. Coord: NW Lat: 30 13 34  Lon: -93 12 65  SE Lat: 30 13 32  Lon:  -93 11 54',
                          'Ex. Coord: NW Lat: abcdefgh  Lon: abcdefghi  SE Lat: abcdefgh  Lon:  abcdefghi')
        t = self.handler.parse(raw, ['BatonRouge1'])
        self.assertEquals(t.work_lat, 0)
        self.assertEquals(t.work_long, 0)


    def test_BatonRouge1_2(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge1-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("CLC01", "CLW02")
        t = self.handler.parse(raw, ['BatonRouge1'])

        # there should be *4* locates now; CLW02 should have been replaced by
        # three others
        self.assertEquals(len(t.locates), 7)

        raw = raw.replace("CLW02", "ETL01") # duplicate!
        t = self.handler.parse(raw, ['BatonRouge1'])
        self.assertEquals(len(t.locates), 4)

    def test_BatonRouge1_3(self):
        # test tickets without "additional members" part
        tl = ticketloader.TicketLoader("../testdata/BatonRouge1-1.txt")
        raw = tl.tickets[16]
        t = self.handler.parse(raw, ['BatonRouge1'])

        self.assertEquals(t.ticket_number, "178632")
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, "GSU03")

        tl = ticketloader.TicketLoader("../testdata/batonrouge-nomark.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['BatonRouge1'])

        self.assertEquals(t.ticket_number, "70192639")
        self.assertEquals(t.legal_due_date, "") # not provided on ticket
        self.assertEquals(t.transmit_date, "2007-05-10 08:44:00")

    def test_BatonRouge1_4(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge1-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['BatonRouge1'])

        # longitude and latitude
        lat1 = 30 + 59/60.0 + 50/(60.0*60.0)
        lat2 = 30 + 50/60.0 + 50/(60.0*60.0)
        lat = (lat1 + lat2) / 2
        long1 = -(89 + 19/60.0 + 31/3600.0)
        long2 = -(89 + 10/60.0 + 6/3600.0)
        long = (long1 + long2) / 2
        self.assertAlmostEqual(t.work_lat, lat,5)
        self.assertAlmostEqual(t.work_long, long,5)

    def test_BatonRouge1_4(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge1-2.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Ex. Coord: NW Lat: 30. 59 50 Lon: -89. 19 31 SE Lat: 30. 50 50 Lon:  -89. 10 6',
                          'Ex. Coord: NW Lat: abcdefghi Lon: abcdefghij SE Lat: abcdefghi Lon:  abcdefghi')
        t = self.handler.parse(raw, ['BatonRouge1'])

        self.assertEquals(t.work_lat, 0)
        self.assertAlmostEqual(t.work_long, 0)

    def testLanham_as_Richmond1(self):
        # there is no Lanham call center (anymore), but we can test it for a
        # similar format
        tl = ticketloader.TicketLoader("../testdata/lanham-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Richmond1"])  # !

        ae = self.assertEquals  # shortcut
        ae(t.ticket_format, "FCV1")
        ae(t.ticket_number, "00139336")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2002-03-07 01:01:00")
        ae(t.operator, "sylvia")
        ae(t.call_date, "2002-02-28 08:35:00")
        ae(t.work_date, "2002-03-04 08:00:00")
        ae(t.work_city, "UPPER MARLBORO")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "N MARWOOD BLVD")
        ae(t.work_cross, "HAVENWOOD CT")
        ae(t.work_type, "INSTALLING UG ELEC SVC")
        self.assert_(t.work_description.startswith("LOC ENTIRE"))
        self.assert_(t.work_description.endswith("MARWOOD BLVD"))
        self.assert_(t.work_remarks.startswith("CALLER REQUESTS"))
        self.assert_(t.work_remarks.endswith("SLM 2/21"))
        ae(t.work_state, "MD")
        ae(t.work_county, "PG")
        ae(t.caller_fax, "(301)736-5937")
        ae(t.con_name, "C W  WRIGHT")
        ae(t.caller, "HEATHER SHAUT")
        ae(t.caller_phone, "(301)736-2396")
        ae(t.caller_altcontact, "NEAL ALDERMAN")
        ae(t.caller_altphone, "")
        ae(t.company, "PEPCO")
        ae(t.con_state, "MD")
        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "MTV01")
        ae(t.locates[4].client_code, "PEP05")
        self.assertEquals(t._dup_fields, [])

    def testNewJerseyParser1(self):
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['NewJersey'])

        ae = self.assertEquals  # shortcut
        ae(t.ticket_number, "020730315")
        ae(t.transmit_date, "2002-03-14 09:59:00")
        ae(t.call_date, t.transmit_date)
        ae(t.work_county, "BERGEN")
        ae(t.work_city, "RIVER VALE")
        ae(t.work_subdivision, "")
        ae(t.work_address_street, "JOHN SHINE CT")
        ae(t.work_address_number, "30")
        ae(t.work_cross, "RIVERVALE RD")
        ae(t.work_type, "FOUNDATION")
        ae(t.work_description, "ENTIRE OF REAR PROP")
        ae(t.work_date, "2002-03-20 07:00:00")
        ae(t.company, "MR AD MRS GUTIERREZ")
        ae(t.caller, "JERRY MONTONE")
        ae(t.caller_phone, "973-523-6992")
        ae(t.con_name, "MONTONE CONSTRUCTION")
        ae(t.con_address, "382 6TH AVE")
        ae(t.con_city, "PATERSON")
        ae(t.con_state, "NJ")
        ae(t.con_zip, "07514")
        ae(t.caller_fax, "973-977-9296")
        ae(t.caller_cellular, "973-418-8418 MOBILE")
        ae(t.work_remarks, "EXPIRATION DATE 04/26/02 ")
        self.assertEquals(t._dup_fields, [])

        lines = raw.split("\n")
        for i,line in enumerate(lines):
            if line.strip().startswith("Remarks:"):
                del lines[i+1:i+3]
                break
        raw = string.join(lines,'\n')
        t = self.handler.parse(raw, ['NewJersey'])

        raw = tl.getTicket()
        raw = raw.replace('CDC = NJN','')
        t = self.handler.parse(raw, ['NewJersey'])
        ae(len(t.locates),0)

    def test_NewJerseyParser_2(self):
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt")
        raw = tl.getTicket()
        lines = raw.split('\n')
        del lines[23:30]
        raw = string.join(lines,'\n')
        t = self.handler.parse(raw, ['NewJersey'])

        ae = self.assertEquals  # shortcut
        ae(t.work_remarks, "EXPIRATION DATE 04/26/02 ")

    def testNewJersey_XXX(self):
        tl = ticketloader.TicketLoader("../testdata/NewJersey-2.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['NewJersey'])

        self.assertEquals(len(t.locates), 7)    # "XXX" is also added now
        self.assertEquals(t.locates[1].client_code, "UNI")

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['NewJersey'])

        self.assertEquals(len(t.locates), 8+1)
        self.assertEquals(t.locates[1].client_code, "PVW")

    def test_NewJersey_New(self):
        tl = ticketloader.TicketLoader("../testdata/NewJerseyNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey"])

        ae = self.assertEquals
        ae(t.transmit_date, "2004-10-27 10:11:00")
        ae(t.ticket_type, "ROUTINE")
        ae(t.ticket_number, "042910009")
        ae(len(t.locates), 1) # we don't parse the "operators notified" block
        ae(t.locates[0].client_code, "TST")
        ae(t.work_date, "2004-10-22 00:01:00")
        ae(t.work_county, "OCEAN")
        ae(t.work_city, "LACEY")
        ae(t.work_address_number, "16")
        ae(t.work_address_street, "HOLLYWOOD BLVD S")
        ae(t.work_cross, "ROUTE 9")
        ae(t.work_type, "ADDITION")
        ae(t.work_description, "M/O BEGINS AT 30FT BEHIND CURB,...") # XXX should be multiple lines now!
        ae(t.work_remarks, "REMARKS LINE 1 REMARKS LINE 2 REMARKS LINE 3" +
                           " REMARKS LINE 4 REMARKS LINE 5 ")
        ae(t.company, "JACK HOMEOWNER")
        ae(t.caller, "JILL HOMEOWNER")
        ae(t.caller_phone, "607-000-0000")
        ae(t.con_name, "JOHN HOMEOWNER")
        ae(t.con_address, "16 HOLLYWOOD BLVD S")
        ae(t.con_city, "FORKED RIVER")
        ae(t.con_state, "NJ")
        ae(t.con_zip, "08731")
        ae(t.caller_fax, "")
        ae(t.caller_cellular, "609-000-2900")
        ae(t.caller_email, "EMAIL-ADDRESS@HOTMAIL.COM")
        ae(t.update_of, None)

        # TODO:
        # Lat/Long

        # set update_of
        tl = ticketloader.TicketLoader("../testdata/NewJerseyNew-2.txt")
        raw = tl.tickets[4]
        t = self.handler.parse(raw, ['NewJersey'])
        self.assertEquals(t.ticket_number, "042910011")
        self.assertEquals(t.update_of, "042781385")

    def test_NewJersey_New_2(self):
        tl = ticketloader.TicketLoader("../testdata/NewJerseyNew-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey"])

        ae = self.assertEquals
        ae(t.transmit_date, "2004-11-30 16:21:00")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "042910010")
        ae(len(t.locates), 1) # we don't parse the "operators notified" block
        ae(t.locates[0].client_code, "TST")
        ae(t.work_date, "2004-10-22 00:01:00")
        ae(t.work_county, "OCEAN")
        ae(t.work_city, "LACEY")
        ae(t.work_address_number, "16")
        ae(t.work_address_street, "HOLLYWOOD BLVD S")
        ae(t.work_cross, "ROUTE 9")
        ae(t.work_type, "REPLACING POLE")
        ae(t.work_description, "15FT RADIUS OF POLE # 50154/41525...")
        ae(t.work_remarks, "REMARKS LINE 1 REMARKS LINE 2 REMARKS LINE 3" +
                           " REMARKS LINE 4 REMARKS LINE 5 ")
        ae(t.company, "JOE HOMEOWNER")
        ae(t.caller, "JACOB HOMEOWNER")
        ae(t.caller_phone, "609-000-0000")
        ae(t.con_name, "JESSI HOMEOWNER")
        ae(t.con_address, "16 HOLLYWOOD BLVD S")
        ae(t.con_city, "FORKED RIVER")
        ae(t.con_state, "NJ")
        ae(t.con_zip, "08731")
        ae(t.caller_fax, "")
        ae(t.caller_cellular, "609-000-2900")
        ae(t.caller_email, "")

        # TODO:
        # Lat/Long

    def test_NewJersey3(self):
        tl = ticketloader.TicketLoader("../testdata/NewJersey3-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey3"])

        ae = self.assertEquals
        ae(t.transmit_date, "2007-02-01 14:58:00")
        ae(t.ticket_type, "ROUTINE")
        ae(t.ticket_number, "070321108")
        ae(len(t.locates), 1) # we don't parse the "operators notified" block
        ae(t.locates[0].client_code, "VIN")
        ae(t.work_date, "2007-02-07 08:00:00")
        ae(t.work_county, "CUMBERLAND")
        ae(t.work_city, "VINELAND")
        ae(t.work_address_number, "110")
        ae(t.work_address_street, "E WALNUT RD")
        ae(t.work_cross, "S SEVENTH ST")
        ae(t.work_type, "RENEWING WTR SVC")
        ae(t.work_description, "CURB TO CURB,CURB TO 20FT BEHIND CURB")
        ae(t.work_remarks, "BLOCK 732 LOT#12.04 ")
        ae(t.company, "VINELAND WATER UTIL")
        ae(t.caller, "VIVIAN SIMONE")
        ae(t.caller_phone, "856-794-4056 X4057")
        ae(t.con_name, "VINELAND WATER UTIL")
        ae(t.con_address, "330 WALNUT RD")
        ae(t.con_city, "VINELAND")
        ae(t.con_state, "NJ")
        ae(t.con_zip, "08360")
        ae(t.caller_fax, "856-794-6181")
        ae(t.caller_cellular, "")
        ae(t.caller_email, "")

        # test actual tickets that have trailing spaces
        tl = ticketloader.TicketLoader("../testdata/NewJersey3-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey3"])

        ae([x.client_code for x in t.locates], ['VIN'])
        ae(t.ticket_number, "070710062")
        ae(t.work_city, "VINELAND")

    def test_NewJersey_dup_fields(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
           "tickets", "NewJersey-2012-02-24-dups", "GSU1-2012-02-22-00094.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey2010"])
        self.assertEquals(t.ticket_number, "120530059")
        self.assertEquals(t.update_of, "120320145")
        self.assertEquals(t._dup_fields, [])

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
          "tickets", "NewJersey2-2012-02-24-dups", "GSU2-2012-02-22-00117.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey2010B"])
        self.assertEquals(t.ticket_number, "120530332")
        self.assertEquals(t.update_of, "120411369")
        self.assertEquals(t._dup_fields, [])

    def testRichmond1Parser1(self):
        tl = ticketloader.TicketLoader("../testdata/richmond1-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Richmond1"])

        ae = self.assertEquals
        ae(t.ticket_format, "FCV1")
        ae(t.ticket_number, "00169658")
        ae(t.ticket_type, "NO SHOW")
        ae(t.kind, "NORMAL")
        ae(t.ticket_type, "NO SHOW")
        ae(t.transmit_date, "2002-03-14 01:03:00")
        ae(t.call_date, "2002-03-11 15:56:00")
        ae(t.operator, "france")
        ae(t.work_date, "2002-03-13 16:00:00")
        ae(t.ticket_format, "FCV1")    # and not "Lanham" or "ONISI"

        ae(t.work_city, "STAFFORD")
        ae(t.work_address_number, "5")
        ae(t.work_address_street, "PIN OAK CT")
        ae(t.work_cross, "KRISMATT CT")
        ae(t.work_type, "INST A FENCE")
        ae(t.work_description, "LOC ENTIRE PROP AT ABOVE ADDRESS")
        ae(t.work_remarks, "NO T/TECH")
        ae(t.work_state, "VA")
        ae(t.work_county, "STAFFORD")

        ae(t.caller_fax, "(540)775-2681")
        ae(t.con_name, "TRIPLE K FENCE CORP")
        ae(t.caller, "KATHY LAKE")
        ae(t.caller_phone, "(540)775-3154")
        ae(t.caller_altcontact, "ANSWERING SERVICE")
        ae(t.caller_altphone, "(540)374-9283")
        ae(t.company, "MR FLYNN")
        ae(t.con_state, "VA")
        #ae(t.con_county, "STAFFORD")
        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "CMG01")
        ae(t.locates[2].client_code, "CTL03")

        ae(t.map_page, "STAF 011 K01")
        self.assertEquals(t._dup_fields, [])

        # go to fifth ticket. this ticket doesn't have a ticket_type.
        raw = tl.tickets[4]
        t = self.handler.parse(raw, ['Richmond1'])
        self.assertEquals(t.ticket_type, "-")
        # test the grid cells as well... at this point, we need only one
        # grid cell parsed
        self.assertEquals(t.map_page, "WARR 001 B06")

    def test_Richmond2Parser_1(self):
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt")
        raw = tl.getTicket()
        raw = raw.replace('ONLY, CALLER','ONLY,\tCALLER') # test the tab removal of post_process
        t = self.handler.parse(raw, ['Richmond2'])

        ae = self.assertEquals
        ae(t.ticket_format, "FCV2")
        ae(t.ticket_number, "0128193")
        ae(t.ticket_type, "EMERGENCY GRID")
        ae(t.call_date, "2002-03-14 04:30:00")
        ae(t.transmit_date, "2002-03-14 06:28:45")
        ae(t.operator, "BAB")
        ae(t.work_state, "VA")
        ae(t.work_county, "PETERSBURG")
        ae(t.work_city, "OAKHURST")
        ae(t.work_address_number, "2545")
        ae(t.work_address_street, "CENTURY DRIVE")
        ae(t.work_cross, "KENMORE DRIVE")

        self.assert_(t.work_description.startswith("TICKET FOR"))
        self.assertTrue('ONLY, CALLER' in t.work_description)
        self.assert_(t.work_description.endswith("INSTRCUCTIONS"))  # sic!
        ae(t.work_type, "EMERGENCY-REPAIRING UG SECONDARY ELECTRIC")
        ae(t.work_date, "2002-03-14 04:30:00")
        ae(t.work_notc, "000")
        ae(t.priority, "0")
        ae(t.ticket_type, "EMERGENCY GRID")
        ae(t.kind, "EMERGENCY")
        ae(t.legal_date, None)

        ae(t.company, "SAME")
        ae(t.con_name, "DOMINION/VPCO - RICHMOND")
        ae(t.con_address, "7500 WEST BROAD STREET")
        ae(t.con_city, "RICHMOND")
        ae(t.con_state, "VA")
        ae(t.con_zip, "23220")
        ae(t.caller_fax, "804 755-5267")
        ae(t.caller, "DOUG GOODMAN")
        ae(t.caller_phone, "804 755-5380")
        self.assert_(t.work_remarks.startswith("MISS UTILITY"))
        self.assert_(t.work_remarks.endswith("4:44 AM"))
        ae(len(t.locates), 6)
        ae(t.locates[0].client_code, "BAR283")
        ae(t.map_page, "0464J01")
        self.assertEquals(t._dup_fields, [])

        # get third ticket
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['Richmond2'])
        ae(t.work_date, "2002-03-18 09:30:00")
        ae(t.respond_date, "2002-03-18 09:16:00")
        ae(t.map_page, "0407E11")

    def test_Richmond2Parser_2(self):
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt")
        raw = tl.getTicket()
        raw = raw.replace('Mbrs : BAR283 CGCH10 CIPT12 TMC218 UTIL11 VPP360',
                          'Mbrs : BAR283 CGCH10 CIPT12 TMC218 UTIL11 VPP360\n: ABC')
        t = self.handler.parse(raw, ['Richmond2'])

        ae = self.assertEquals
        ae(len(t.locates), 7)
        ae(t.locates[6].client_code, "ABC")

    def testPennsylvaniaParser1(self):
        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Pennsylvania'])

        ae = self.assertEquals
        ae(t.ticket_format, "FWP2")
        ae(t.transmit_date, "2002-03-14 08:52:23")
        ae(t.ticket_number, "0730295")
        ae(t.ticket_type, "ROUTINE PLACE")  # not: ROUTINE
        ae(t.kind, "NORMAL")
        ae(t.work_county, "BUTLER")
        ae(t.work_city, "FRANKLIN TWP")
        #ae(t.work_address_street, " ** **SR 0422** ** ")
        ae(t.work_address_street, "SR 0422")
        ae(t.work_cross, "UNIONVILLE RD")
        self.assert_(t.work_description.startswith("CALLER IS WORKING"))
        self.assert_(t.work_description.endswith("OFFSET 977."))
        ae(t.work_type, "SIGN INSTALLATION")
        ae(t.company, "PENNDOT")
        ae(t.work_date, "2002-03-19 08:45:00")
        ae(t.con_name, "PENNDOT")
        ae(t.con_address, "351 NEW CASTLE RD")
        ae(t.con_city, "BUTLER")
        ae(t.con_state, "PA")
        ae(t.con_zip, "16001")
        ae(t.caller, "DAVE WIDENHOFER")
        ae(t.caller_phone, "724-284-8800")
        ae(t.caller_fax, "724-284-1504")
        ae(t.caller_email, "NONE")
        ae(t.caller_contact, "DAVE WIDENHOFER")
        ae(t.caller_altphone, "724-355-1078")
        ae(t.call_date, "2002-03-14 08:43:00")
        ae(t.legal_due_date, "2002-03-18 08:45:00")   # 19 Mar was a Tuesday
        ae(t.operator, "TARA KOSKO MCNICKLE")
        ae(t.work_state, "PA")
        ae(t.work_remarks, "")
        ae(len(t.locates), 10)
        ae(t.locates[0].client_code, "AUB")
        self.assertEquals(t._dup_fields, [])

    def testWestVirginiaParser1(self):
        tl = ticketloader.TicketLoader("../testdata/WestVirginia-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['WestVirginia'])

        ae = self.assertEquals
        ae(t.ticket_format, "FWP1")
        ae(t.ticket_number, "730059")
        ae(t.ticket_type, "ROUTINE")
        ae(t.kind, "NORMAL")
        ae(t.work_county, "HARRISON")
        ae(t.work_city, "WILSONBURG")
        ae(t.work_address_street, "0  ROUTE 9")
        ae(t.work_state, "WV")
        ae(t.work_cross, "COUNTY RT 11")
        ae(t.company, "DOMINION TRANSMISSION")
        ae(t.work_type, "REPAIR GAS LEAK/PIPELINE")
        ae(t.work_date, "2002-03-18 09:45:00")
        self.assert_(t.work_remarks.startswith("FROM ABOVE"))
        self.assert_(t.work_remarks.endswith("MEETING:"))
        ae(t.con_name, "CNG TRANSMISSION CORPORATION")
        ae(t.caller_phone, "304-782-1200")
        ae(t.con_address, "BOX 144-A")
        ae(t.con_city, "SALEM")
        ae(t.con_state, "WV")
        ae(t.con_zip, "26426")
        ae(t.caller_contact, "RANDY SWIGER")
        ae(t.caller_fax, "304-627-3874")
        ae(t.caller_altphone, "304-627-3897")
        ae(t.caller, "RANDY SWIGER")
        ae(t.call_date, "2002-03-14 09:37:00")
        ae(t.work_date, "2002-03-18 09:45:00")
        self.assertEquals(t._dup_fields, [])

        # test some more...
        tl = ticketloader.TicketLoader("../testdata/WestVirginia-3.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['WestVirginia'])
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_type, "EMERGENCY")
        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "AC")

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['WestVirginia'])
        self.assert_(t.ticket_type.startswith("Cancellation"))

        # work date for design tickets...
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['WestVirginia'])
        ae(t.ticket_type, "DESIGN STAGE")
        ae(t.call_date, "2003-02-26 15:59:00")
        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "AC")

    def testWestVirginiaParserAEP(self):
        tl = ticketloader.TicketLoader("../testdata/fwp4.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['WestVirginiaAEP'])
        ae = self.assertEquals

        ae(t.ticket_number, "3570001")
        ae(t.ticket_type, 'EMERGENCY')
        ae(t.kind, 'EMERGENCY')
        ae(t.work_state, 'WV') # implied
        ae(t.work_county, 'OHIO')
        ae(t.work_city, "BETHLEHEM")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "93  CHAPEL RD")
        ae(t.work_cross, "PINE LN")
        ae(t.company, "VILLAGE OF BETHLEHEM")
        ae(t.work_type, "REPAIRING WATER MAIN")
        ae(t.work_date, "2005-12-23 01:15:00")
        ae(t.con_name, "BETHLEHEM, VILLAGE OF")
        ae(t.caller_phone, "304-233-9527")
        ae(t.con_address, "1 COMMUNITY PARK RD")
        ae(t.con_city, "BETHLEHEM")
        ae(t.con_state, "WV")
        ae(t.con_zip, "26003")
        ae(t.call_date, "2005-12-23 01:16:00")
        ae(t.transmit_date, t.call_date)
        ae([x.client_code for x in t.locates],
           ['AW', 'CW', 'MH', 'MPW', 'VOB', 'TCB'])

    def testOhioParser1(self):
        tl = ticketloader.TicketLoader("../testdata/Ohio-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Ohio'])

        ae = self.assertEquals
        ae(t.ticket_format, "FWP3")
        ae(t.transmit_date, "2002-03-14 15:59:00")
        ae(t.ticket_number, "0314-055-071-00")
        ae(t.call_date, "2002-03-14 15:55:00")
        ae(t.work_state, "OH")
        ae(t.work_county, "ATHENS")
        ae(t.work_city, "TROY TWP")
        ae(t.work_date, "2002-03-18 17:00:00")
        ae(t.work_type, "REPLACE POLE #2186524453240")
        ae(t.caller_contact, "ED MAERKER")
        ae(t.company, "AMERICAN ELECTRIC POWER")
        ae(t.caller_phone, "740-594-1977")
        ae(t.caller_fax, "740-594-1955")
        ae(t.work_remarks, "")
        ae(t.work_description, "LOCATE: FRONT OF PROPERTY 70FT N O/O RD")
        ae(t.work_address_street, "BRIMSTONE RD")
        ae(t.work_address_number, "23850")
        self.assert_(t.work_cross.startswith("at ~1400"))
        self.assert_(t.work_cross.endswith("BELLE RD\""))
        self.assertEquals(t._dup_fields, [])

        # try next ticket
        t1 = self.handler.parse(tl.tickets[1])
        ae(t1.work_address_number, "")
        ae(t1.work_address_street, "CTY RD 4")
        self.assert_(t1.work_cross.startswith("between ~300"))
        self.assert_(t1.work_cross.endswith("RD 60\""))

        # try notes in another ticket
        t2 = self.handler.parse(tl.tickets[2])
        self.assert_(t2.work_remarks.startswith("CUSTOMER IS"))
        self.assert_(t2.work_remarks.endswith("IN AREA"))
        self.assert_(t2.work_description.startswith("LOCATE:"))
        self.assert_(t2.work_description.endswith("RD 15"))

        # try Ohio-2
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "Ohio-2.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Ohio'])
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "AGP")
        ae(t.ticket_type, "ON THE JOB")
        ae(t.kind, "NORMAL")

        # simulate an emergency ticket
        raw = raw.replace("ON THE JOB", "SHORT NOTICE")
        t = self.handler.parse(raw, ['Ohio'])
        ae(t.kind, "EMERGENCY")

        # this ticket has no work_date, so we must emulate one
        raw = tl.tickets[5]
        t = self.handler.parse(raw, ['Ohio'])
        self.assertEquals(t.work_date, "2003-01-10 09:35:00")
        ae(t.work_state, "OH")
        ae(t.work_county, "NOBLE")

        # sometimes there are multiple sections with county/place/locn
        tl = ticketloader.TicketLoader("../testdata/Ohio-1.txt")
        raw = tl.findfirst("0315-021-030-00")
        t = self.handler.parse(raw, ['Ohio'])
        self.assertEquals(t.ticket_number, '0315-021-030-00')
        self.assertEquals(t.work_county, 'WASHINGTON')
        self.assertEquals(t.work_city, 'MARIETTA TWP')
        self.assertEquals(t.work_cross, 'between "S R 550" and')

    def testTicketHandler1(self):
        tl = ticketloader.TicketLoader("../testdata/atlanta-2.txt")
        raw = tl.tickets[0]
        z = self.handler.parse(raw, ["Atlanta"])
        self.assert_(isinstance(z, ticketmod.Ticket)
         and z.ticket_format == "Atlanta")

    def testTicketHandler2(self):
        # test what the TicketHandler does with a bogus ticket.
        bogus = "foo!\n"*20
        try:
            z = self.handler.parse(bogus)
        except ticketparser.TicketHandlerError:
            pass    # this is the desired behavior
        else:
            self.fail("No exception raised for unrecognized ticket")

    def testTicketHandlerWithPA(self):
        # Pennsylvania summaries should not be mistaken for tickets
        tl = ticketloader.TicketLoader("../testdata/sum-Pennsylvania.txt")
        raw = tl.getTicket()
        z = self.handler.parse(raw, ['Pennsylvania'])
        self.assertEquals(z.__class__.__name__, "TicketSummary")

    def testSanityCheck(self):
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "Request", "Bogus")
        try:
            z = self.handler.parse(raw, ['NewJersey'])
        except ticketparser.TicketError:
            pass    # this is what we expect
        else:
            self.fail("sanity check failed")

    def test_ParseALot(self):
        filenames = ["sum-Richmond1.txt", "mixed-Richmond1.txt"]
        for filename in filenames:
            fullname = "../testdata/" + filename
            tl = ticketloader.TicketLoader(fullname)
            count = 1
            raw = tl.getTicket()
            while raw:
                z = self.handler.parse(raw, ["Richmond1"])
                raw = tl.getTicket()
                count = count + 1

    def testPennsylvaniaLocateParsing(self):
        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt")
        raw = tl.tickets[7] # rudely grasp the 8th ticket
        ticket = self.handler.parse(raw, ['Pennsylvania'])

        # is this the right ticket anyway?
        self.assertEquals(ticket.ticket_number, "0930747")
        self.assertEquals(len(ticket.locates), 12)
        for loc in ticket.locates:
            self.assert_("*" not in loc.client_code)

        raw = raw.replace('03-APR-02','03-xyz-02')
        from parsers import ParserError
        self.failUnlessRaises(ParserError,self.handler.parse,raw,["Pennsylvania"])

    def testPennsylvaniaAddressHandling(self):
        """ Check if PennsylvaniaParser's post_process does the right thing.
        """
        addresses = [
            ["627** **KRISTI**LN**", "627", "", "KRISTI LN"],
            [" ** **SAXONBURG**RD** ", "", "", "SAXONBURG RD"],
            [" 292** **CRESTVIEW**RD** ", "292", "", "CRESTVIEW RD"],
            [" 831**W**OLD RT 422** ** ", "831 W", "", "OLD RT 422"],
            [" **E**RIVERSIDE**RD** ", "E", "", "RIVERSIDE RD"],
            [" 1211-1213** **GRANT**AVE** ", "1211", "1213", "GRANT AVE"],
            # ^ 1211-1213 is split by BaseParser.post_process!
            [" 309-311-313** **COUNTRYVIEW**DR** ", "309", "311-313",
             "COUNTRYVIEW DR"],
            # this is split too, but the second number would be too long, so
            # it's prepended to the street name:
            [" 210-212-214-216** **BLAHDEBLAH**DR** ", "210", "",
             "212-214-216 BLAHDEBLAH DR"],
            # ^ this one too
        ]
        from formats import Pennsylvania
        for raw, number, number2, street in addresses:
            pp = Pennsylvania.Parser("blah", "Pennsylvania") # start all over
            pp.ticket.work_address_street = raw
            pp.post_process()
            self.assertEquals(pp.ticket.work_address_number, number)
            self.assertEquals(pp.ticket.work_address_street, street)
            self.assertEquals(pp.ticket.work_address_number_2, number2)

    def test_Richmond2_clients(self):
        # Do we parse and find all clients (in "members" section)?
        tl = ticketloader.TicketLoader("../testdata/richmond2-2.txt", chr(12))
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['Richmond2'])
        self.assertEquals(len(t.locates), 17)
        self.assertEquals(t.locates[9].client_code, "DTECH1")
        self.assertEquals(t.locates[-1].client_code, "VPW291")

    def test_date_check(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        # replace a valid time with an invalid one
        raw = string.replace(raw, "10:11", "77:11")

        try:
            t = handler.parse(raw)
        except date.DateError:
            pass    # this is what we expect
        else:
            self.fail("Failed to raise a DateError for invalid date")

    def testNorthCarolina_1(self):
        tl = ticketloader.TicketLoader("../testdata/northcarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])

        ae = self.assertEquals
        ae(t.ticket_format, "FCL1")
        ae(t.ticket_number, "A0062728")
        ae(t.ticket_type, "NORM NEW PLCE")
        ae(t.kind, "NORMAL")
        ae(t.call_date, "2002-02-06 17:47:00")
        ae(t.operator, "PLH")
        ae(t.channel, "999")
        ae(t.revision, "00A")

        ae(t.work_state, "NC")
        ae(t.work_county, "MECKLENBURG")
        ae(t.work_city, "CHARLOTTE OUT")
        ae(t.work_subdivision, "CHURCHFIELD")

        ae(t.work_address_number, "")
        ae(t.work_address_street, "CHURCHFIELD LN")
        ae(t.work_cross, "CAROLINA ACADEMY RD")
        ae(t.work_description.startswith("LOCATE BOTH SIDES"), 1)
        ae(t.work_description.endswith("TIME ON TICKET"), 1)

        ae(t.work_type, "POWER INSTALLATION")
        ae(t.work_date, "2002-02-08 17:47:00")
        ae(t.work_notc, "48/48")
        ae(t.priority, "NORM")
        ae(t.duration, "3 MONTHS")
        ae(t.company, "DUKE POWER")

        ae(t.con_name, "MASTEC")
        ae(t.con_address, "5550-A WILKINSON BLVD")
        ae(t.con_city, "CHARLOTTE")
        ae(t.con_state, "NC")
        ae(t.con_zip, "28208")
        ae(t.caller, "RANDY NORTON")
        ae(t.caller_phone, "704-393-2250")

        ae(t.work_remarks, "--SEE ORIGINAL TICKET#: A0025484-00A")
        ae(t.transmit_date, "2002-02-06 17:48:34")
        ae(len(t.locates), 16)
        ae(t.locates[0].client_code, "ABS01")
        ae(t.locates[-1].client_code, "VCN01")
        self.assertEquals(t._dup_fields, [])

        # there are no grids for this ticket:
        ae(t.work_lat, 0.0)
        ae(t.work_long, 0.0)

    def testNorthCarolina_2(self):
        tl = ticketloader.TicketLoader("../testdata/northcarolina-1.txt")
        raw = tl.tickets[1] # get the *second* ticket
        t = self.handler.parse(raw, ["NorthCarolina"])

        # There are 4 grids:
        # Grids   : 3517C8113B   3517C8113C   3517D8113B   3517D8113C
        lat = 35+((17.375+17.125)/2)/60
        long = -(81+((13.375+13.625)/2)/60)
        self.assertAlmostEqual(t.work_long, long,5)
        self.assertAlmostEqual(t.work_lat, lat,5)

        raw.replace('3517C8113B  ','3517C8113B--')
        t = self.handler.parse(raw, ["NorthCarolina"])
        self.assertEquals(t.grids[0],'3517C8113B')

        # ticket_type 'EMER' should yield a kind 'EMERGENCY'
        while string.find(raw, "C0032043") == -1:
            raw = tl.getTicket()
        t = self.handler.parse(raw)
        self.assertEquals(t.ticket_number, "C0032043")
        self.assertEquals(t.ticket_type, "EMER NEW STRT")
        self.assertEquals(t.kind, "EMERGENCY")

        # test caller_contact field (if filled)
        raw = tl.tickets[3]
        t = self.handler.parse(raw)
        self.assertEquals(t.caller_contact, "KENNY")

    def test_NorthCarolina_3(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-2.txt")
        raw = tl.tickets[15]
        t = self.handler.parse(raw, ["NorthCarolina"])

        self.assertEquals(t.ticket_number, "C0121796")
        self.assertEquals(t.caller, "SHERRY OSBOURNE")
        self.assertEquals(t.caller_contact, t.caller)

    def test_NorthCarolina_4(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.tickets[522]   # !
        t = self.handler.parse(raw, ["NorthCarolina"])

        # all we check for is that it _doesn't_ raise an error because of
        # invalid grids

    def testSouthCarolina_1(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCarolina"])

        ae = self.assertEquals
        #ae(t.ticket_format, "FCL2") # hmmm
        ae(t.ticket_number, "2138035")
        ae(t.call_date, "2002-04-26 14:09:00")
        ae(t.transmit_date, t.call_date)
        ae(t.work_date, "2002-05-01 14:15:00")
        ae(t.ticket_type, "Normal")
        ae(t.kind, "NORMAL")
        ae(t.work_address_street, "SUNSET CIR")
        ae(t.work_address_number, "2270")
        ae(t.work_cross, "DOBYS BRIDGE RD")
        ae(t.work_city, "FT MILL")
        ae(t.work_county, "York")
        ae(t.work_state, "SC")
        ae(t.work_description, "MARK ENTIRE PROPERTY")
        ae(t.work_type, "INGROUND POOL")
        ae(t.duration, "3Weeks")
        ae(t.caller_contact, "")

        ae(t.caller, "ROBERT FERGIONE")
        ae(t.con_name, "POOL & SPA TECH OF CHARLOTTE")
        ae(t.caller_phone, "(704) 544-0424")
        ae(t.caller_altphone, "(704) 544-0424")
        ae(t.con_address, "13917 BALLANTYNE MEADOWS DR")
        ae(t.con_city, "CHARLOTTE")
        ae(t.con_state, "NC")
        ae(t.con_zip, "28277")

        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "FMT20")

        self.assert_(t.work_remarks.startswith("Normal"))
        self.assert_(t.work_remarks.endswith("Walker"))

        # the 5th ticket contains an interesting CALLER INFORMATION section:
        raw = tl.tickets[4] # get the 5th ticket
        t = self.handler.parse(raw)

        self.assertEquals(t.caller, "Michelle Moore")
        self.assertEquals(t.con_name, "Comporium Telecom, Inc.")
        self.assertEquals(t.caller_phone, "(803) 324-3167")
        self.assertEquals(t.con_address, "1523 South Anderson Road")
        self.assertEquals(t.con_city, "Rock Hill")
        self.assertEquals(t.con_state, "SC")
        self.assertEquals(t.con_zip, "29730")
        self.assertEquals(t.duration, "3Weeks")
        self.assertEquals(t._dup_fields, [])

        raw = raw = tl.tickets[0]
        raw = raw.replace('Notice Type: Normal','Notice Type: Emer')
        t = self.handler.parse(raw, ["SouthCarolina"])
        ae(t.kind, "EMERGENCY")

        raw = raw = tl.tickets[0]
        raw = raw.replace('Notice Type: Normal','Notice Type: Rush')
        t = self.handler.parse(raw, ["SouthCarolina"])
        ae(t.kind, "EMERGENCY")

        raw = raw = tl.tickets[0]
        lines = raw.split("\n")
        start = end = 0
        for i,line in enumerate(lines):
            if line.startswith("CALLER INFORMATION"):
                # Make the caller info section 4 lines
                del lines[i+4]
                break
        raw = string.join(lines,'\n')
        t = self.handler.parse(raw, ["SouthCarolina"])

    def testSouthCarolina_2(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCarolina-1.txt")
        raw = tl.tickets[5] # 6th ticket
        t = self.handler.parse(raw, ["SouthCarolina"])

        self.assertEquals(t.ticket_number, "2165093")

        self.assertEquals(t.caller, "KEVIN ALEXANDER")
        self.assertEquals(t.con_address, "1033 KILPATRICK LN")

    def testSouthCarolina_3(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCarolina-1.txt")
        raw = tl.getTicket()
        raw = raw.replace('MARK ENTIRE PROPERTY',
                          'MARK ENTIRE PROPERTY' + 'A'*3500)
        t = self.handler.parse(raw, ["SouthCarolina"])

        self.assertEquals(len(t.work_description), 3499)
        self.assertEquals(t.work_description[-10:], '(More ...)')

    def testOrlando(self):
        tl = ticketloader.TicketLoader("../testdata/Orlando-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Orlando"])

        ae = self.assertEquals
        ae(t.ticket_number, "11621923")
        ae(t.revision, "000")
        ae(t.call_date, "2002-04-26 11:19:00")
        ae(t.transmit_date, "2002-04-26 11:35:11")
        ae(t.work_state, "FL")
        ae(t.work_county, "ORANGE")
        ae(t.work_city, "ORLANDO")
        ae(t.work_subdivision, "MALLARD COVE")
        ae(t.work_address_number, "2714")
        ae(t.work_address_street, "TALOVA DR")
        ae(t.work_cross, "MALLARD COVE BLVD")
        ae(t.work_description.startswith("LOCATE BACK LEFT"), 1)
        ae(t.work_description.endswith("STAKED**"), 1)
        ae(t.work_remarks.startswith("*** GEOCODED PLACE"), 1)
        ae(t.work_remarks.endswith("ADDRESS ***"), 1)
        ae(t.work_date, "2002-05-01 15:00:00")
        ae(t.work_notc, "075")
        ae(t.duration, "02 HRS")
        ae(t.due_date, "2002-04-30 11:19:00")
        ae(t.work_type, "PLANTING TREES")
        ae(t.company, "SELF BY CONTRACTOR")
        ae(t.con_name, "CONRAD MILLER")
        ae(t.con_address, "2714 TALOVA DR")
        ae(t.con_city, "ORLANDO")
        ae(t.con_state, "FL")
        ae(t.con_zip, "32837")
        ae(t.caller, "CONRAD MILLER")
        ae(t.caller_phone, "407-438-9181")
        ae(t.caller_contact, "CONRAD MILLER")
        ae(t.caller_email, "FLORIDACAJUN@MSN.COM")
        ae(t.operator, "MIC")
        ae(t.channel, "04")
        ae(t.map_page, "2821A8124A")
        self.assertEquals(t._dup_fields, [])

        ae(len(t.locates), 6)
        ae(t.locates[0].client_code, "BYERS3")

        ae(t.ticket_type, "STREET") # ?
        ae(t.kind, "NORMAL")
        self.assertAlmostEqual(t.work_lat,28.3666666667,5)
        self.assertAlmostEqual(t.work_long,-81.4166666667,5)

        # twiddle the map_page and see what happens
        raw = string.replace(raw, "2821A8124A", "2821*8124A-")
        t = self.handler.parse(raw)
        self.assertEquals(t.map_page, "2821B8124A")

        # try the locates of another ticket
        raw = tl.tickets[1]
        t = self.handler.parse(raw)
        ae(len(t.locates), 11)
        ae(t.locates[0].client_code, "BYERS3")
        ae(t.locates[-1].client_code, "UTI296")

        # try the address range
        raw = tl.tickets[2]
        t = self.handler.parse(raw)
        ae(t.work_address_number, "7011")
        ae(t.work_address_number_2, "7011")

    def test_Orlando_2005_09_09(self):
        # test OrlandoParser after changes made around 2005-09-09
        # changes:
        # - length of ticket number (was 8, now 9)
        # - Place field renamed to CallerPlace and moved down
        # - New field GeoPlace where old field Place was

        tl = ticketloader.TicketLoader("../testdata/FloridaNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Orlando'])

        self.assertEquals(t.ticket_number, "236500001")
        self.assertEquals(t.transmit_date, "2005-09-08 09:33:41")
        self.assertEquals(t.call_date, "2005-03-24 08:32:00")
        self.assertEquals(t.work_county, "PALM BEACH")
        self.assertEquals(t.work_city, "LAKE WORTHY")
        self.assertEquals(t.work_state, "FL")

        self.assertEquals(t.work_address_number, "")
        self.assertEquals(t.work_address_street, "I 95")
        self.assertEquals(t.work_cross, "10TH AVE N")
        self.assertEquals(t.work_date, "2005-08-24 23:59:00")
        self.assertEquals(t.con_name, "SUNSHINE STATE ONE-CALL OF FL, INC.")
        self.assertEquals(t.company, "TEST TICKET DO NOT RESPOND")
        self.assertEquals(t.map_page, "2637B8003A")
        self.assertEquals(t.work_type, "TEST TICKET DO NOT RESPOND")
        self.assertEquals(t.con_address, "11 PLANTATION ROAD")
        self.assertEquals(t.con_city, "DEBARY")
        self.assertEquals(t.con_state, "FL")
        self.assertEquals(t.con_zip, "32713")
        self.assertEquals(t.caller, "MICHELLE WHITE")
        self.assertEquals(t.caller_phone, "386-575-2000")
        self.assertEquals(t.caller_fax, "386-575-2035")
        self.assertAlmostEqual(t.work_lat,26.6270833333,5)
        self.assertAlmostEqual(t.work_long,-80.0666666667,5)
        self.assertEquals([x.client_code for x in t.locates],
                          ['AC1105', 'CLW792', 'FPLPAL', 'FPUC01', 'LCS495',
                           'LW1136', 'MCIU01', 'PBC750', 'PBT865', 'SBF34',
                           'SSOC02'])

    def testTennessee1(self):
        tl = ticketloader.TicketLoader("../testdata/Tennessee-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tennessee"])
        ae = self.assertEquals

        ae(t.ticket_number, "020870244")
        ae(t.ticket_type, "PRIORITY")
        ae(t.call_date, "2002-03-28 08:18:00")
        ae(t.operator, "AMANDA")
        ae(t.transmit_date, "2002-03-28 08:18:00")

        ae(t.con_name, "SOUTHWEST ELECTRIC")
        ae(t.caller, "SALES JONES")
        ae(t.con_address, "PO BOX 847")
        ae(t.con_city, "COVINGTON")
        ae(t.con_state, "TN")
        ae(t.con_zip, "38019")
        ae(t.caller_phone, "(901) 476-9839")
        ae(t.caller_contact, "SALES JONES")
        ae(t.caller_altphone, "(901) 476-9839")
        ae(t.caller_fax, "")

        ae(t.work_date, "2002-04-01 08:00:00")
        ae(t.work_state, "TN")
        ae(t.work_county, "TIPTON")
        ae(t.work_city, "COVINGTON")

        ae(t.work_address_number, "453")
        ae(t.work_address_street, "LIBERTY CHURCH RD")
        ae(t.work_cross, "HOLLY GROVE")
        ae(t.work_description.startswith("THIS IS A SHORT"), 1)
        ae(t.work_description.endswith("RIBBON..."), 1)
        ae(t.work_type, "POLE(S) & ANCHOR(S), INSTL")
        ae(t.company, "SOUTHWEST ELECTRIC")
        ae(t.map_page, "68A")
        self.assertAlmostEqual(t.work_lat,35.5212516785,5)
        self.assertAlmostEqual(t.work_long,-89.7115859985,5)

        ae(len(t.locates), 4)
        ae(t.locates[0].client_code, "B05")
        ae(t.locates[1].client_code, "FUT")
        ae(t.locates[2].client_code, "MCI")
        ae(t.locates[3].client_code, "SWEMCO")
        self.assertEquals(t._dup_fields, [])

        # does the KUB replacement work?
        raw = raw.replace("B05", "KUB")
        t = self.handler.parse(raw)
        # KUB is removed, but 4 others are inserted -> 7
        self.assertEquals(len(t.locates), 7)

        raw = tl.getTicket()
        raw = raw.replace('LONGITUDE--[-89.7177200317383]','LONGITUDE--[abcdef]')
        t = self.handler.parse(raw, ["Tennessee"])
        ae(t.work_long, 0)

    def test_Tennessee_2(self):
        '''
        Mantis 2180: New ticket format for TN
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","TNOCS","TEST-2008-12-16-00001.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tennessee"])
        ae = self.assertEquals

        ae(t.call_date, '2008-12-08 06:10:00')
        ae(t.caller, 'MARSHA MINON')
        ae(t.caller_altphone, '(931) 358-0435')
        ae(t.caller_contact, 'MARSHA MINON')
        ae(t.caller_phone, '(931) 358-0435')
        ae(t.company, 'COMAST')
        ae(t.con_address, '862 DURHAM RD')
        ae(t.con_city, 'ADAMS')
        ae(t.con_name, 'MINON CONSTRUCTION')
        ae(t.con_state, 'TN')
        ae(t.con_zip, '37010')
        ae(t.explosives, 'N')
        ae(t.kind, 'NORMAL')
        ae(t.map_page, '89A')
        ae(t.operator, 'Jennie')
        ae(t.ticket_number, '083430008')
        ae(t.ticket_type, 'Normal')
        ae(t.transmit_date, '2008-12-08 06:10:00')
        ae(t.work_address_number, '1005')
        ae(t.work_address_street, 'CULPEPPER LN')
        ae(t.work_city, 'FRANKLIN')
        ae(t.work_county, 'WILLIAMSON')
        ae(t.work_cross, 'FORREST CROSSINGS CIR')
        ae(t.work_date, '2008-12-11 06:15:00')
        self.assertAlmostEquals(t.work_lat, 35.8998339388, 5)
        self.assertAlmostEquals(t.work_long, -86.8402199857, 5)
        ae(t.work_state, 'TN')
        ae(t.work_type, 'CATV LINE(S) AND/OR DROP(S), INSTL')
        ae(len(t.locates), 4)
        ae(t.locates[0].client_code, "B01")
        ae(t.locates[1].client_code, 'INTMCWH')
        ae(t.locates[2].client_code, 'MTEMFR')
        ae(t.locates[3].client_code, 'U09')

    def test_Tennessee_3(self):
        '''
        Mantis 2197: extra lines in con name field
        The TN call centerb has been recently adding a carriage return in
        the middle of the data we parse into the "company" field of the
        ticket record. The extra line throws off the detail view on the
        client and all of the reports that we try to export with that field.
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FJT","fjt1-2009-01-07-00064.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tennessee"])
        ae = self.assertEquals

        ae(t.company, 'CHARTER COMMUNICATIONS')

    def test_Tennessee_4(self):
        '''
        Mantis 2197: extra lines in con name field
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FJT","fjt1-2009-01-09-00060.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tennessee"])
        ae = self.assertEquals

        ae(t.company, 'CITY OF MIDDLETON')

    def test_Tennessee_5(self):
        '''
        Mantis 2197: extra lines in con name field
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FJT","fjt1-2009-01-12-00038.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tennessee"])
        ae = self.assertEquals

        ae(t.company, 'CAMDEN CITY HALL/ WATER DEPT/ ST DEPT')

    def test_TN1391(self):
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/1391-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['TN1391'])

        ae = self.assertEquals
        ae(t.transmit_date, "2004-10-12 14:39:00")
        ae(t.ticket_type, "NORMAL")
        ae(t.ticket_number, "042861573")
        ae(t.call_date, "2004-10-12 14:39:00")
        ae(t.con_name, "GARNEY CONSTRUCTION")
        ae(t.caller_contact, "BRIAN WHITNEL")
        ae(t.con_address, "1333 NW VIVIAN RD")
        ae(t.con_city, "KANSAS CITY")
        ae(t.con_state, "MO")
        ae(t.con_zip, "64118")
        ae(t.work_date, "2004-10-15 14:45:00")
        ae(t.work_state, "TN")
        ae(t.work_county, "DAVIDSON")
        ae(t.work_city, "NASHVILLE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "N 9TH ST")
        ae(t.work_cross, "W EASTLAND AVE")
        ae(t.work_type, "WATER LINE MAINT.")
        ae(t.company, "METRO WATER")
        ae(t.map_page, "82D")
        self.assertAlmostEqual(t.work_lat,36.184476895,5)
        self.assertAlmostEqual(t.work_long,-86.758288989,5)
        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "B01")

    def test_Baltimore_1(self):
        tl = ticketloader.TicketLoader("../testdata/Baltimore-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Baltimore"])
        ae = self.assertEquals

        ae(t.ticket_number, "00356468")
        ae(t.ticket_type, "SHORT NOTICE")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2002-05-14 09:01:00")
        ae(t.call_date, "2002-05-14 08:55:00")
        ae(t.work_date, "2002-05-15 09:00:00")
        ae(t.operator, "janice")

        ae(t.work_city, "ANNAPOLIS")
        ae(t.work_address_number, "115")
        ae(t.work_address_street, "WEST ST")
        ae(t.work_cross, "LAYFAYETTE AV")   # sic!
        ae(t.work_type, "INSTALL STORM DRAINS")
        ae(t.work_description.startswith("LOC 200FT"), 1)
        ae(t.work_description.endswith("WEST ST"), 1)
        ae(t.caller_fax, "(410)224-2177")
        ae(t.work_remarks.startswith("CREW IS ON SITE"), 1)
        ae(t.work_remarks.endswith("SHORT NOTICE"), 1)
        ae(t.work_state, "MD")
        ae(t.work_county, "AA")
        # the fax number should not be in here

        ae(t.con_name, "JONES OF ANNAPOLIS INC")
        ae(t.caller_contact, "WENDY SPENCER")
        ae(t.caller_phone, "(410)224-2095")
        ae(t.caller_altcontact, "WANDA OR DAVID")
        ae(t.caller_altphone, "")
        ae(t.company, "BROWN CONTRACTING")
        ae(t.con_state, "MD")
        # no such thing as con_county
        self.assertEquals(t._dup_fields, [])

        ae(t.map_page, "AA 020 J10")

        #self.assertAlmostEquals(t.work_lat,-76.490626,5)
        #self.assertAlmostEquals(t.work_long,39.015622,5)
        # in addition to BGE09, there should be a BGE09G; this is a special
        # case for Baltimore
        ae(len(t.locates), 2)
        ae(t.locates[0].client_code, "BGE09")
        ae(t.locates[0].seq_number, "0121")
        ae(t.locates[1].client_code, "BGE09G")
        ae(t.locates[1].seq_number, None)

        # test another ticket with multiple seq_numbers
        raw = tl.tickets[3]
        t = self.handler.parse(raw, ["Baltimore"])
        self.assertEquals(len(t.locates), 3)    # BGExx rule
        self.assertEquals(t.locates[0].client_code, "BGE63")
        self.assertEquals(t.locates[0].seq_number, "0032")
        self.assertEquals(t.locates[1].client_code, "TBE05")
        self.assertEquals(t.locates[1].seq_number, "0031")
        self.assertEquals(t.locates[2].client_code, "BGE63G")
        self.assertEquals(t.locates[2].seq_number, None)

        # check that BGExxG is not added if it exists already on the ticket
        raw = tl.tickets[0]
        raw = raw.replace("TAN01", "BGE09G  TAN01")
        t = self.handler.parse(raw, ["Baltimore"])
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[1].client_code, "BGE09G")
        self.assertEquals(t.locates[0].client_code, "BGE09")

        # test one with HCU02
        raw = tl.tickets[0]
        raw = raw.replace("BGE   09", "HCU   02")
        t = self.handler.parse(raw, ["Baltimore"])
        locate_names = [x.client_code for x in t.locates]
        self.assert_('HCU02' in locate_names)
        self.assert_('HCU02S' in locate_names)

        # test one with COHWS01
        raw = tl.tickets[0]
        raw = raw.replace("BGE   09", "COHWS 01")
        t = self.handler.parse(raw, ["Baltimore"])
        locate_names = [x.client_code for x in t.locates]
        self.assert_('COHWS01' in locate_names)
        self.assert_('COHWS01S' in locate_names)

        # test update_of
        raw = tl.tickets[4]
        t = self.handler.parse(raw, ['Baltimore'])
        self.assertEquals(t.ticket_number, "00356488")
        self.assertEquals(t.update_of, "00318715")

    def test_Baltimore_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FMB1","FMB1-2009-04-03-01400.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Baltimore"])
        ae = self.assertEquals

        ae(t.ticket_number, "9148914")
        ae(t.ticket_type, "FIOS")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, '2009-04-03 13:55:00')
        ae(t.call_date, '2009-04-03 13:37:00')
        ae(t.work_date, '2009-04-07 13:45:00')
        ae(t.operator, 'webusr')

        ae(t.work_city, 'COLUMBIA ')
        ae(t.work_address_number, '7512')
        ae(t.work_address_street, 'SEA CHANGE ')
        ae(t.work_cross, 'BROKEN LAND ')
        ae(t.work_type, 'FIOS/E369645')
        ae(t.work_description.startswith('LOCATE ENTIRE PROPERTY OF 7512 SEA CHANGE AND ALL EASEMENTS'), 1)
        ae(t.work_description.endswith("AND RD CROSSINGS"), 1)
        ae(t.caller_fax, '(410)721-3613 ')
        ae(len(t.work_remarks), 0)
        ae(t.work_state, "MD")
        ae(t.work_county, "HWD")

        ae(t.con_name, 'RAY SEARS & SON INC ')
        ae(t.caller_contact, "DIANE BALSLEY")
        ae(t.caller_phone, '(410)721-0818')
        ae(t.caller_fax, '(410)721-3613 ')
        ae(t.caller_altcontact, "BOBBY QUEEN")
        ae(t.caller_altphone, "(443)392-0209")
        ae(t.company, 'VRZN')
        ae(t.con_state, "MD")
        self.assertEquals(t._dup_fields, [])

        ae(t.map_page, 'HWD 5053 E1')
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "HTV02")
        ae(t.locates[0].seq_number, "0137")
        #self.assertAlmostEquals(t.work_lat, -76.840626, 5)
        #self.assertAlmostEquals(t.work_long, 39.184372, 5)

    def test_Baltimore_3(self):
        latslongs = {
         'FMB1-2009-05-04-02243.txt': (-76.609375999999997, 39.540624000000001),
         'FMB1-2009-05-04-02244.txt': (-76.609375999999997, 39.540624000000001),
         'FMB1-2009-05-06-01945.txt': (-76.821876000000003, 39.346871999999998),
         'FMB1-2009-05-06-01946.txt': (-76.821876000000003, 39.346871999999998),
         'FMB1-2009-05-06-01947.txt': (-76.821876000000003, 39.346871999999998),
         'FMB1-2009-05-06-01951.txt': (-76.865626000000006, 39.346871999999998),
         'FMB1-2009-05-06-01952.txt': (-76.865626000000006, 39.346871999999998),
         'FMB1-2009-05-06-01960.txt': (-76.740626000000006, 39.465621999999996),
         'FMB1-2009-05-06-01961.txt': (-76.740626000000006, 39.465621999999996),
         'FMB1-2009-05-06-01964.txt': (-76.809376, 39.453122),
         'FMB1-2009-05-06-01965.txt': (-76.809376, 39.453122),
         'FMB1-2009-05-06-01966.txt': (-76.809376, 39.453122)
        }
        tickets = glob(os.path.join("..","testdata","sample_tickets","FMB1","FMB1-2009-05-*.txt"))
        tmp = {}
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["Baltimore"])

            tmp[os.path.basename(ticket_file)] = (t.work_lat, t.work_long)
            for key,val in t._data.iteritems():
                if val is not None and key != 'image' and hasattr(val,'find'):
                    self.assertEquals(val.find('\t'),-1)

    def test_Baltimore_4(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
         "sample_tickets", "FMB1",
         "FMB1-2013-03-18-06-38-56-371-IUVRT_modified.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Baltimore"])
        ae = self.assertEquals

        ae(t.ticket_number, "130770002")
        ae(t.map_page, '3942A7537B')
        self.assertAlmostEquals(t.work_lat,  39.7130783, 5)
        self.assertAlmostEquals(t.work_long, -75.62733885, 5)

    def test_WashingtonParser_2(self):
        tl = ticketloader.TicketLoader("../testdata/Washington2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Washington2"])
        ae = self.assertEquals

        ae(t.service_area_code, "PG NON PIPELINE")
        ae(t.serial_number, "MISUTIL2003010700661")
        ae(t.ticket_type, "-")   # there is no ticket type?
        ae(t.ticket_number, "01008848")
        ae(t.transmit_date, "2003-01-07 16:45:00")
        ae(t.map_page, 'PG 026 A04')
        self.assertEquals(t._dup_fields, [])
        # I'm assuming the test works the same as Washington...

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FMW2", "fmw2-2008-05-07-00059.txt"))
        raw = tl.getTicket()
        fmts = ['Washington2', 'WashingtonVA', 'VUPSNewFMW2']
        t = self.handler.parse(raw, fmts)
        self.assertTrue('PEP904' in [x.client_code for x in t.locates])

    def test_WashingtonParser_3(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
         "sample_tickets", "FMW1",
         "FMW1-2013-03-25-09-42-58-167-INXD2_modified.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Washington"])
        ae = self.assertEquals

        ae(t.ticket_number, "13148511")
        ae(t.map_page, '3856C7701B')
        self.assertAlmostEquals(t.work_lat,  38.9406265, 5)
        self.assertAlmostEquals(t.work_long, -77.0281476, 5)


    def testJacksonvilleParser(self):
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Jacksonville"])
        ae = self.assertEquals

        ae(t.ticket_number, "11620615")
        ae(t.ticket_type, "GRID")
        ae(t.kind, "NORMAL")
        ae(t.revision, "000")
        ae(t.call_date, "2002-04-26 08:46:00")
        ae(t.transmit_date, "2002-04-26 08:55:06")
        ae(t.work_state, "FL")
        ae(t.work_county, "ST LUCIE")
        ae(t.work_city, "PORT ST LUCIE")
        ae(t.work_subdivision, "LAKE CHARLES")
        ae(t.work_address_number, "748")
        ae(t.work_address_street, "SW MUNJACK CIR")
        ae(t.work_cross, "SW LAKE CHARLES CIR")
        ae(t.work_description, "LOCATE ENTIRE PROPERTY")
        ae(t.work_remarks.startswith("CUSTOMER STATES"), 1)
        ae(t.work_remarks.endswith("MANUAL ***"), 1)
        ae(t.work_date, "2002-04-30 08:46:00")
        ae(t.work_notc, "048")
        ae(t.duration, "01 DAY")
        ae(t.due_date, "2002-04-30 08:46:00")
        ae(t.work_type, "SEWER LOCATES")
        ae(t.company, "HANOVER HOMES")

        ae(t.con_name, "AQUA DIMENSIONS PLUMBING")
        ae(t.con_address, "1651 SW MACEDO BLVD.")
        ae(t.con_city, "PORT ST LUCIE")
        ae(t.con_state, "FL")
        ae(t.con_zip, "34984")
        ae(t.caller, "RHONDA LAFFERTY")
        ae(t.caller_phone, "772-344-8433")
        ae(t.caller_contact, "BOB LUDLUM/JEFF WATKINS/RHONDA")
        ae(t.caller_altphone, "")
        ae(t.caller_fax, "772-343-7418")
        self.assertAlmostEqual(t.work_lat,27.2954166667,5)
        self.assertAlmostEqual(t.work_long,-80.4054166667,5)
        self.assertEquals(t._dup_fields, [])

        self.assert_(t.work_long < 0)
        self.assert_(t.work_lat > 0)

        ae(t.operator, "ANA")
        ae(t.channel, "31")

        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "AC1107")

    def testFairfaxParser(self):
        tl = ticketloader.TicketLoader("../testdata/Fairfax-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Fairfax"])
        ae = self.assertEquals

        ae(t.ticket_number, "00180387")
        ae(t.transmit_date, "2002-03-14 12:50:00")
        ae(t.call_date, "2002-03-14 12:44:00")
        ae(t.work_date, "2002-03-18 12:00:00")
        ae(t.operator, "keisha")
        ae(t.ticket_type, "-")
        ae(t.kind, "NORMAL")

        ae(t.work_city, "ASHBURN")
        ae(t.work_address_number, "20949")
        ae(t.work_address_street, "COHASSET TER")
        ae(t.work_cross, "ROWLEY TER")
        ae(t.work_type, "INST ST LIGHTS")
        ae(t.work_description.startswith("LOC ENTIRE PROP"), 1)
        ae(t.work_description.endswith("SIDE OF LOT"), 1)
        ae(t.work_remarks.startswith("CALLER REQUESTS"), 1)
        ae(t.work_remarks.endswith("T/TECH"), 1)
        ae(t.caller_fax, "(703)443-2104")

        ae(t.con_name, "LEO CONSTRUCTION")
        ae(t.caller_contact, "EMMA MOSS")
        ae(t.caller_phone, "(703)450-4500")
        ae(t.caller_altcontact, "ALT#")
        ae(t.caller_altphone, "(703)443-6608")
        ae(t.company, "ASHBURN VILLAGE DEV. CORP")
        ae(t.con_state, "VA")
        ae(t.work_state, "VA")
        ae(t.work_county, "LOUDOUN")
        ae(t.map_page, "LOUD 031 A02")
        ae(t.explosives, "N")
        self.assertEquals(t._dup_fields, [])

        ae(len(t.locates), 8)
        ae(t.locates[0].client_code, "TVA02")
        ae(t.locates[3].client_code, "ATT02")

        raw = raw.replace('Send To: TVA   02  Seq No: 0104   Map Ref:','Send To: TVA   02  Map Ref:')
        t = self.handler.parse(raw, ["Fairfax"])
        ae(t.locates[0].seq_number, None)


    def testRichmond3Parser(self):
        tl = ticketloader.TicketLoader("../testdata/richmond3-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Richmond3'])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-06-19 16:42:29")
        ae(t.ticket_type, "NORM NEW GRID RSND")
        ae(t.ticket_number, "A216500339")
        ae(t.revision, "00A")
        ae(t.call_date, "2002-06-14 10:16:00")
        ae(t.work_state, "VA")
        ae(t.work_county, "BEDFORD")
        ae(t.work_city, "")
        ae(t.work_address_number, "2820")
        ae(t.work_address_street, "DICKERSON MILL RD")
        ae(t.work_cross, "DICKERSON MILL RD")
        ae(t.kind, "NORMAL")
        ae(t.work_type, "LANDSCAPING")
        ae(t.company, "HO/DANBY/540-587-3713")
        ae(t.explosives, "N")

        self.assertAlmostEqual(t.work_lat, 37+(17.875/60),5)
        self.assertAlmostEqual(t.work_long, -(79+34.125/60),5)

        ae(t.work_date, "2002-06-19 07:00:00")
        ae(t.due_date, "2002-06-19 07:00:00")

        ae(t.con_name, "RSG LANDSCAPING & DESIGN")
        ae(t.con_address, "424 LUCK AVE SW")
        #ae(t.con_phone, "540-857-4170")
        ae(t.con_city, "ROANOKE")
        ae(t.con_state, "VA")
        ae(t.con_zip, "24012")
        ae(t.caller, "HANDY MANDY")
        ae(t.caller_phone, "540-857-4171")
        ae(t.caller_email, "hanman@aol.com")
        ae(t.caller_contact, "MARK SPOT")
        ae(t.caller_altphone, "540-857-2323")
        self.assertAlmostEqual(t.work_lat,37.2979166667,5)
        self.assertAlmostEqual(t.work_long,-79.56875,5)
        self.assertEquals(t._dup_fields, [])

        ae(t.work_description, "***TEST***DO NOT LOCATE")
        ae(t.work_remarks, "")

        ae(len(t.locates), 2)
        ae(t.locates[0].client_code, "INTCP0")
        ae(t.locates[1].client_code, "ZZAP12")
        ae(len(t.grids), 14)

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['Richmond3'])
        self.assertEquals(t.ticket_number, "A216500341")
        self.assertEquals(t.work_description.startswith("HOUSE #240"), 1)
        self.assertEquals(t.work_description.endswith("DO NOT LOCATE"), 1)
        self.assertEquals(t.work_remarks,
         "LOCATE THE ENTIRE YARD OF 240 QUILLENS POINT LN")

    def testRichmond3Parser_2(self):
        tl = ticketloader.TicketLoader("../testdata/richmond3-1.txt")
        raw = tl.tickets[6]
        t = self.handler.parse(raw, ['Richmond3'])
        # BTW, this has a grid like 1234A5678A-22. if this parses w/ error, it
        # means the parser can handle this correctly.
        ae = self.assertEquals

        self.assertEquals(len(t.locates), 7)
        self.assertEquals(t.locates[-1].client_code, "UTIL11")

        # insert a nasty, too-short grid
        raw = raw.replace("Grids: 3647C7622B-03", "Grids: 47051")
        t = self.handler.parse(raw, ['Richmond3']) # should not fail
        self.assertEquals(t.work_lat, 0)
        self.assertEquals(t.work_long, 0)
        self.assertEquals(t.map_page, "47051")

    def testRichmond3_sanity(self):
        tl = ticketloader.TicketLoader("../testdata/richmond3-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "LOCATE", "VUPSa")
        try:
            t = self.handler.parse(raw)
        except parsers.ParserError:
            pass    # OK, this is what we expect
        else:
            self.fail("ParserError expected (multiple VUPSa instances)")

    def test_Fairfax2(self):
        tl = ticketloader.TicketLoader("../testdata/Fairfax2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Fairfax2"])

        self.assertEquals(t.transmit_date, "2003-06-11 16:23:38")
        self.assertEquals(t.ticket_number, "A316202180")
        self.assertEquals(t.ticket_type, "NORM NEW GRID RSND")
        self.assertEquals(t.call_date, "2003-06-11 15:07:00")
        self.assertEquals(t.work_state, "VA")
        self.assertEquals(t.work_county, "BEDFORD")
        self.assertEquals(t.work_city, "")
        self.assertEquals(t.work_address_number, "112")
        self.assertEquals(t.work_address_street, "TEST STREET")
        self.assertEquals(t.work_cross, "THIS IS A TEST DR")
        self.assertEquals(t.work_type, "BROKEN ELEC POLE")
        self.assertEquals(t.company, "TEST TICKET")
        self.assertEquals(t.map_page, "3718A7949D-21")
        self.assertEquals(t.work_date, "2003-06-16 07:00:00")
        self.assertEquals(t.con_name, "VUPS")
        self.assertEquals(t.con_address, "1829 BLUE HILLS CIRCLE")
        self.assertEquals(t.caller_phone, "540-985-9355 Ext: 2024")
        self.assertEquals(t.caller_email, "mhutchinson@vups.org")
        self.assertEquals(t.caller_fax, "540-342-8250")
        self.assertEquals(t.explosives, "N")
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, "ADL902")
        self.assertAlmostEqual(t.work_lat,37.3145833333,5)
        self.assertAlmostEqual(t.work_long,-79.81875,5)
        self.assertEquals(t.grids, [])

    def testLongRemarks(self):
        reallylongremark = "foo!\n" * 400   # 400*5 = 2000 characters
        # grab a parser... any one will do
        from formats import Pennsylvania
        pp = Pennsylvania.Parser("blah", "Pennsylvania")
        pp.ticket.work_remarks = reallylongremark
        pp.post_process()
        self.assert_(len(pp.ticket.work_remarks) < 1020)
        self.assert_(pp.ticket.work_remarks.endswith("(MORE...)"))

    def testInvalidLocates(self):
        # test if invalid locates are flagged.
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "TCG01", "ZOINKS! U9#2 noise")
        try:
            t = self.handler.parse(raw)
        except ticketparser.TicketError:
            pass    # yes, we expect an error
        else:
            self.fail("Invalid locates should have been flagged")

    def test_schema_usage(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        # create a field that's too long
        raw = string.replace(raw, "ATLANTA",
              "ATLANTA IS A NICE PLACE TO LIVE IF YOU HAPPEN TO BE AN ORANGE")
        h = ticketparser.TicketHandler()
        t = h.parse(raw)
        self.assertEquals(t.work_city, 'ATLANTA IS A NICE PLACE TO LIVE IF YO...')

    def test_many_locates(self):
        # if there are more than 20 locates, that should be an error
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        # add fake locates so we pass the limit (MAX_LOCATES)
        fake_locates = ["BLA%02d" % (x)
                        for x in range(parsers.BaseParser.MAX_LOCATES - 10)]
        s = string.join(fake_locates, " ")
        raw = string.replace(raw, ": OMN01  TCG01  USW01",
              ": OMN01 TCG01 USW01 " + s)

        try:
            t = self.handler.parse(raw)
        except ticketparser.TicketError:
            self.fail("Locate limit shouldn't work for Atlanta")
        else:
            pass

    def test_max_locates(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sca1-locate-limit-40.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SouthCalifornia"])
        # will raise an error if locate limit is exceeded
        self.assertEquals(len(t.locates), 49)

    def test_Pennsylvania_work_date(self):
        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt")
        raw = tl.getTicket()

        # no work_date is required for tickets with DESIGN in the ticket_type:
        raw1 = string.replace(raw, "ROUTINE PLACE", "DESIGN PLACE")
        raw1 = string.replace(raw1, "19-MAR-02", "         ")
        raw1 = string.replace(raw1, "Dig Time--[0845]", "")
        t = self.handler.parse(raw1)
        # this should not raise an exception
        self.assertEquals(t.work_date, None)    # because it wasn't found

        raw2 = string.replace(raw, "19-MAR-02", "         ")
        raw2 = string.replace(raw2, "Dig Time--[0845]", "")
        try:
            t = self.handler.parse(raw2)
        except AttributeError:
            pass    # this was expected
        else:
            self.fail("AttributeError for work_date was not raised")

        # make sure that other fields are still required
        raw3 = string.replace(raw, "[0730295]", "[]") # no ticket_number
        try:
            t = self.handler.parse(raw3)
        except AttributeError:
            pass    # OK
        else:
            self.fail("ticket_number is empty and should have raised an error")

    def test_emergencies(self):
        # test some emergencies.
        tl = ticketloader.TicketLoader("../testdata/Richmond1-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("NO SHOW", "RUSH RUSH")
        t = self.handler.parse(raw, ["Richmond1"])
        self.assertEquals(t.kind, "EMERGENCY")

        tl = ticketloader.TicketLoader("../testdata/Richmond3-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("NORM NEW GRID RSND", "RUSH RUSH")
        t = self.handler.parse(raw, ["Richmond3"])
        self.assertEquals(t.kind, "EMERGENCY")

    def __test_Illinois_1(self):
        tl = ticketloader.TicketLoader("../testdata/Illinois-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Illinois"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-07-30 02:49:56")
        ae(t.ticket_type, "EMERGENCY STREET")
        ae(t.ticket_number, "2110010")
        ae(t.priority, "0")
        ae(t.work_date, "2002-07-30 02:48:00")
        ae(t.work_state, "IL")
        ae(t.call_date, "2002-07-30 02:47:00")
        ae(t.operator, "AKH")
        ae(t.revision, "00")
        ae(t.work_county, "COOK")
        ae(t.work_city, "CALUMET CITY CIT")
        ae(t.work_address_number, "1429")
        ae(t.work_address_street, "STANLEY BLVD")
        ae(t.work_cross, "163RD PL")
        ae(t.work_subdivision, "")
        ae(t.map_page, "T36NR15E20*W")

        ae(t.work_type, "REPAIR WATER MAIN BREAK.")
        #ae(t.work_description, "LOCATE IN THE PARKWAY IN FRONT OF ADDRESS.")
        self.assert_(t.work_description.startswith("IN THE CITY OF CALUMET"), 1)
        self.assert_(t.work_description.endswith("IN FRONT OF ADDRESS."), 1)
        ae(t.company, "")
        ae(t.con_name, "CALUMET CITY PLUMBING")
        ae(t.caller, "JEFF V.")
        ae(t.con_address, "645 STATE STREET")
        ae(t.con_city, "CALUMET CITY")
        ae(t.con_state, "IL")
        ae(t.con_zip, "60409")
        ae(t.caller_phone, "708-868-0074")
        ae(t.caller_fax, "708-868-0102")
        ae(t.work_remarks, "URGENT FOR NOW ** CREW ON THE WAY.")
        ae(len(t.locates), 11)
        ae(t.locates[0].client_code, "AMNM0A")
        ae(t.locates[-1].client_code, "WCG0A")
        self.assertEquals(t._dup_fields, [])

    def __test_Illinois_2(self):
        tl = ticketloader.TicketLoader("../testdata/Illinois-1.txt")
        raw = tl.tickets[2]
        t = self.handler.parse(raw)

        self.assert_(t.work_remarks.startswith("FROM THE INTERSECTION"), 1)
        self.assert_(t.work_remarks.endswith("560-1247"), 1)

    def test_Illinois_3(self):
        # How many tickets are found, now that ^C is a separator also?
        tl = ticketloader.TicketLoader("../testdata/Illinois-1.txt")
        self.assertEquals(len(tl.tickets), 4)
        # 4, as usual... I guess the "null-length" strings are simply filtered
        # out

    def test_Colorado_1(self):
        tl = ticketloader.TicketLoader("../testdata/Colorado-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ['Colorado'])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-07-30 05:48:00")
        ae(t.ticket_type, "EMER NEW STRT LREQ")
        ae(t.kind, "EMERGENCY")
        ae(t.operator, "PPP")
        ae(t.ticket_number, "0458815")
        ae(t.call_date, "2002-07-30 05:45:00")
        ae(t.work_date, "2002-07-30 06:45:00")
        ae(t.work_state, "CO")
        ae(t.work_county, "JEFFERSON")
        ae(t.work_city, "LAKEWOOD")
        ae(t.work_address_number, "80")
        ae(t.work_address_street, "CHASE ST S")
        ae(t.work_cross, "BAYAUD AVE W")
        ae(t.work_type, "REPR H2O MN")
        self.assert_(t.work_description.startswith("EMERGENCY LOC-"), 1)
        self.assert_(t.work_description.endswith("ACCESS OPEN"), 1)
        ae(t.con_name, "CONSOLIDATED MUTUAL H20")
        ae(t.caller, "CRAIG PADGETT")
        ae(t.caller_phone, "(303)238-0451")
        ae(t.caller_fax, "(303)237-5560")
        ae(t.caller_email, "")
        ae(t.company, "CONSOLIDATED MUTUAL H20")
        ae(t.map_page, "04S069W12SE")
        ae(t.work_remarks, "")
        #ae(len(t.locates), 8)
        #ae(t.locates[0].client_code, "ATDW00")
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "ATDW00")
        self.assertEquals(t._dup_fields, [])

    def test_Colorado_2(self):
        # check if "MEET" tickets have the correct ticket_type
        tl = ticketloader.TicketLoader("../testdata/Colorado-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("Meet: N", "Meet: Y")
        t = self.handler.parse(raw, ['Colorado'])

        self.assertEquals(t.ticket_type, "EMER NEW STRT LREQ MEET")

        # 2004.03.29.  Test "G" locates:
        raw = raw.replace("ATDW00", "PCND00")
        t = self.handler.parse(raw, ["Colorado"])
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[1].client_code, 'PCND00G')

    def test_ColoradoNE(self):
        # 2004-07-20: test 'DHON' format
        tl = ticketloader.TicketLoader("../testdata/ColoradoNE-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Colorado', 'ColoradoNE'])

        self.assertEquals(t.ticket_number, "1088588")
        self.assertEquals(t.ticket_type, "NORMAL NOTICE")
        self.assertEquals(t.transmit_date, "2004-07-16 11:25:00")
        self.assertEquals(t.call_date, "2004-07-16 11:13:00")
        self.assertEquals(t.work_state, "NE")
        self.assertEquals(t.map_page, "12N43W07SE")
        self.assertEquals(t.con_name, "COLORADO BURIED SERVICES INC")
        self.assertEquals(len(t.locates), 2)

    def test_Colorado2_1(self):
        # same as Colorado, so we don't test every single field
        tl = ticketloader.TicketLoader("../testdata/Colorado2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Colorado2'])

        self.assertEquals(t.ticket_number, '0654878')
        self.assertEquals(t.ticket_type, 'NORM UPDT GRID LREQ')

    def test_SanDiego_1(self):
        # XXX The SanDiego test stays, because this is essentially a
        # SouthCalifornia test, and might be useful
        tl = ticketloader.TicketLoader("../testdata/SanDiego-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SanDiego"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-07-29 10:42:22")
        ae(t.ticket_format, "SCA2")
        ae(t.ticket_type, "GRID")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "A0009156")
        ae(t.call_date, "2002-07-29 10:38:00")
        ae(t.operator, "TMC")
        ae(t.revision, "00A")
        ae(t.work_state, "CA")
        ae(t.priority, "2")
        ae(t.work_county, "SAN DIEGO")
        ae(t.work_city, "ENCINITAS")
        ae(t.work_description, "MARK BY")
        ae(t.work_remarks, "FROM CATV PED FRONT OF 14536 ORPHEUS AV CONT"\
         " W/FOR APPROX 80FT TO S/SIDE OF ADDRESS X/ST GLAUCUS ST")
        ae(t.work_type, "BORING TRENCH INSTALL CATV CONDUIT")
        ae(t.work_address_street, "ORPHEUS AVE")
        ae(t.work_cross, "GLAUCUS ST")
        ae(t.work_date, "2002-07-31 10:45:00")
        ae(t.company, "COX COMMUNICATIONS")
        ae(t.con_name, "SUNSHINE COMMUNICATIONS INC")
        ae(t.caller, "PETER ZAMORA")
        ae(t.con_address, "4145 AVENIDA DE LA PLATA")
        ae(t.con_city, "OCEANSIDE")
        ae(t.con_state, "CA")
        ae(t.con_zip, "92056")
        ae(t.caller_fax, "760-726-6415")
        ae(t.map_page, "1147B0324")
        ae(t.caller_phone, "760-726-8415")
        ae(len(t.locates), 10)
        ae(t.locates[0].client_code, "COX04")
        self.assertEquals(t._dup_fields, [])

        raw = raw.replace('Priority: 2','Priority: 0')
        t = self.handler.parse(raw, ["SanDiego"])
        ae(t.priority, "0")
        ae(t.kind, "EMERGENCY")

        raw = tl.getTicket()
        raw = raw.replace('Priority: 2','Priority: 1')
        raw = "EMER\n" + raw
        t = self.handler.parse(raw, ["SanDiego"])
        ae(t.priority, "1")
        ae(t.kind, "EMERGENCY")

    def test_SanDiego_2(self):
        tl = ticketloader.TicketLoader("../testdata/SanDiego-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['SanDiego'])

        # test if address is extracted correctly
        self.assertEquals(t.work_address_number, "523")
        self.assertEquals(t.work_address_street, "N VULCAN AVE")

    def test_empty_ticket_type(self):
        # the "Lanham" family of formats may have an empty ticket_type
        tl = ticketloader.TicketLoader("../testdata/Richmond1-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "NO SHOW", "")
        try:
            t = self.handler.parse(raw)
        except:
            self.fail("Empty ticket should not raise an error for this format")

    def test_date_2400(self):
        # check if 24:00 times are dealt with correctly
        tl = ticketloader.TicketLoader("../testdata/NewJersey-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "At 0700", "At 2400")
        t = self.handler.parse(raw)
        self.assertEquals(t.work_date, "2002-03-20 00:00:00")

    def test_FCO1_locates(self):
        tl = ticketloader.TicketLoader("../testdata/Colorado-1.txt")
        raw = tl.getTicket()
        raw = raw.replace(":USND06", ":USND06 :UNPR04 :COOK13")
        t = self.handler.parse(raw)

        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, "ATDW00")
        self.assertEquals(t.locates[1].client_code, "UNPR04")

    def test_FCO1_locate_finder(self):
        # this annoying bug was caused by a regex that didn't match multiple
        # spaces...
        tl = ticketloader.TicketLoader("../testdata/Colorado-bug-1.txt")
        raw = tl.getTicket()
        self.assert_(raw.find("DIACP") > -1)
        t = self.handler.parse(raw)

        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, "DIACP")

    def test_NorthCalifornia_1(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCalifornia-1.txt")
        raw = tl.tickets[5]
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ae = self.assertEquals

        self.assertEquals(t.ticket_number, "0419540")
        self.assertEquals(len(t.locates), 7)
        self.assertEquals(t.locates[5].client_code, "SANI#1")
        # the point is that we don't choke on the SANI#1 locate

        # test emergency ("priority") tickets
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["NorthCalifornia"])
        self.assertEquals(t.priority, "0")
        self.assertEquals(t.kind, "EMERGENCY")
        self.assertEquals(t.ticket_type, 'PRIORITY NOTICE-EMER')

        # 2003.12.12: According to the new rules, we no longer count the
        # SCETUL in the Send To section.  So:
        self.assertEquals(len(t.locates), 8)

        # now, let's pretend the term id in the top left is SCERID
        raw = raw.replace('PBTHA5', 'SCERID')
        t = self.handler.parse(raw, ["NorthCalifornia"])
        self.assertEquals(len(t.locates), 9)
        self.assertEquals(t.locates[-1].client_code, 'SCERID')

    def test_NorthCalifornia_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NCA1","nca1-2009-04-03-00471.txt"))
        raw = tl.getTicket()
        raw = 'fiber\n' + raw
        raw = raw.replace('CITY OF STOCKTON','VERIZON WO# OC 0829995')
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ae = self.assertEquals
        ae(t.ticket_type, 'NORMAL NOTICE EXTENSION-FTTP')

    def test_NorthCalifornia_3(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NCA1","nca1-2009-04-03-00471.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ae = self.assertEquals

        ae(t.call_date, '2009-04-03 09:17:00')
        ae(t.caller, 'TINA MAYHEW')
        ae(t.caller_altcontact, 'DREW BORIOLO')
        ae(t.caller_cellular, '209-649-7871')
        ae(t.caller_email, 'TINA.MAYHEW@KNIFERIVER.COM')
        ae(t.caller_fax, '209-948-1597')
        ae(t.caller_phone, '209-948-0302')
        ae(t.company, 'CITY OF STOCKTON')
        ae(t.con_address, '655 W. CLAY ST')
        ae(t.con_city, 'STOCKTON')
        ae(t.con_name, 'KNIFE RIVER CONSTRUCTION')
        ae(t.con_state, "CA")
        ae(t.con_zip, '95206')
        ae(t.explosives, 'N')
        ae(t.kind, "NORMAL")
        ae(t.priority, "2")
        ae(t.ticket_number, '0064840')
        ae(t.ticket_type, "NORMAL NOTICE EXTENSION")
        ae(t.transmit_date, '2009-04-03 09:17:30')
        ae(t.work_city, 'STOCKTON')
        ae(t.work_county, 'SAN JOAQUIN')
        ae(t.work_date, '2009-03-12 12:00:00')
        self.assertTrue(t.work_description.startswith("ALL/O W.EUCLID AVE FR W/SI/O N.PERSHING AVE GO 2500' W TO E/SI/O"))
        self.assertAlmostEqual(t.work_long, -121.321946, 5)
        self.assertAlmostEqual(t.work_lat, 37.9753795, 5)
        ae(t.work_state, "CA")
        self.assertEquals(t.work_type,
          'BKHO TO REM & REPL HANDICAP RAMPS& AC OV')

        self.assert_(t.work_remarks.startswith("#1 EXTEND TO 05/05/2009"))
        self.assert_(t.work_remarks.endswith("04/03/2009"))

        self.assertEquals(len(t.locates), 8)
        self.assertEquals(t.locates[0].client_code, "COMSTK")
        self.assertEquals(t.locates[1].client_code, "COSJOA")
        self.assertEquals(t.locates[7].client_code, "UNIPAC")

    def test_NorthCaliforniaATT(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NorthCaliforniaATT'])
        ae = self.assertEquals

        ae(t.ticket_number, "0037524")
        ae(t.ticket_format, 'NCA2')
        ae(t.ticket_type, "NORMAL NOTICE")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2007-01-31 07:42:39")
        ae(t.call_date, "2007-01-31 07:40:00")
        ae(t.work_date, "2007-02-02 08:00:00")
        ae(t.priority, "2")
        ae(t.caller, "LANCE DOWN")
        ae(t.con_name, "THE GAS COMPANY")
        ae(t.con_address, "1700 INYO ST")
        ae(t.con_city, "MOJAVE")
        ae(t.con_state, "CA")
        ae(t.con_zip, "93501")
        ae(t.caller_phone, "661-824-0819")
        ae(t.caller_fax, "")
        ae(t.work_type, "DIG & TR TO REPL GAS SVC")
        ae(t.company, "SAME")
        ae(t.work_city, "TEHACHAPI")
        ae(t.work_state, "CA")
        ae(t.work_county, "KERN")
        ae(t.work_address_number, "303")
        ae(t.work_address_street, "E H ST")
        ae(t.work_cross, "N DAVIS ST")
        self.assertAlmostEqual(t.work_lat,35.132517,5)
        self.assertAlmostEqual(t.work_long,-118.4459915,5)
        ae([x.client_code for x in t.locates], ["PBTHAN"])
        ae(t.serial_number, "102")
        ae(t.locates[0]._plat, "THCHCA01")
        ae(t.image[:6], 'PBTHAN')

        # test another ticket
        #tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
        #     "tickets", "NCA2-2010-04-30", "NCA2-2010-04-29-01250.txt"))
        #raw = tl.tickets[0]
        #t = self.handler.parse(raw, ['NorthCaliforniaATT'])

    def test_SouthCalifornia_1(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCalifornia-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCalifornia"])
        ae = self.assertEquals

        ae(t.ticket_type, "INSUFFICIENT NOTICE GRID")
        ae(t.ticket_format, "SCA1")
        ae(t.transmit_date, "2002-09-13 05:05:35")
        ae(t.ticket_number, "A0057805")
        ae(t.call_date, "2002-09-12 20:37:00")
        ae(t.work_state, "CA")
        ae(t.work_county, "VENTURA")
        ae(t.work_city, "NEWBURY PARK")
        ae(t.work_address_number, "331")
        ae(t.work_address_street, "HUNTERS PT")
        ae(t.work_cross, "ROLLING OAKS")
        ae(t.work_type, "SEWER REPAIR")
        ae(t.work_date, "2002-09-16 08:30:00")
        ae(t.work_description, "LOCATE ALL LINES TO CURB")
        ae(t.work_lat,0.0)
        ae(t.work_long,0.0)
        ae(t.company, "HOME OWNER")
        ae(t.con_name, "WILLIAM BROTHERS PLUMBING")
        ae(t.caller, "BOBBY")
        ae(t.con_address, "3541 OLD CONEJO RD")
        ae(t.con_city, "NEWBURY PARK")
        ae(t.con_state, "CA")
        ae(t.con_zip, "91320")
        ae(t.caller_phone, "805-499-9335")
        ae(len(t.locates), 12)
        self.assert_(t.work_remarks, "LOCATE ALL")
        self.assertEquals(t._dup_fields, [])
        self.assertEquals(t.source, 'SCA1')

        # what happens to the source if we pretend it's from UQSTS0?
        #raw = raw.replace('UTI50', 'UQSTS0')
        #t = self.handler.parse(raw, ['SouthCalifornia'])
        #self.assertEquals(t.source, 'SCA1')

        # REMARKS block (appears in some tickets)
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['SouthCalifornia'])
        self.assert_(t.work_remarks.startswith("600 BLOCK"))
        self.assert_(t.work_remarks.endswith("06:07]"))

        # test if incomplete map pages are fixed (A01 is assumed)
        # e.g. '014' -> '014A01'
        raw = tl.tickets[0]
        raw = string.replace(raw, "0556F02", "0556")
        t = self.handler.parse(raw, ["SouthCalifornia"])
        self.assertEquals(t.map_page, "0556A01")
        '''
        Mantis 2131:
        The SCA7 tickets will update SCA1 tickets. For each SDG0x term id received,
        we need to add a corresponding SDG0xG term id. For example, if we receive
        SDG04 on the ticket, the parser should add locates SDG04 and SDG04G to
        the ticket for marking. The current SDG rule that add a term id only
        if SDG09 is on the ticket will not be used.
        '''

        # test SDG09 rules
        raw = tl.tickets[0]
        raw = string.replace(raw, "CAW03", "SDG09")
        raw = string.replace(raw, "CMW52", "SDG08")
        raw = string.replace(raw, "VCF01", "VCF01G")
        t = self.handler.parse(raw, ["SouthCalifornia"])
        # SDG09 should appear, but not SDG08G
        self.assertEquals([x.client_code for x in t.locates],
                     ["SDG09", "CHRCMAL", "SDG08", "SCG4UO", "UFTV02", "USCE16",
                      "UTI50", "UVZCAM", "UVZMV2", "VCF01G", "VZCAM",
                      "VZMDV2"])

    def test_SouthCalifornia_2(self):
        '''
        Test that -MEET is added to the ticket_type if MEET is found in ticket
        Mantis 2236: parser change for CA tickets
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00632.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCalifornia"])
        self.assertEquals(t.ticket_type, 'NORM NEW GRID-MEET')

    def test_SouthCalifornia_3(self):
        '''
        Test that "-EMER" is added to the ticket_type if we determine the ticket to be an emergency.
        Test that "-FTTP" is added to the ticket_type if the work_type field contains "fiber" and the company is "verizon
        Mantis 2236: parser change for CA tickets
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00881.txt"))
        raw = tl.getTicket()
        raw = raw.replace('Priority: 2','Priority: 1')
        raw = "EMER\n" + raw
        t = self.handler.parse(raw, ["SouthCalifornia"])
        self.assertEquals(t.ticket_type, 'NORM NEW GRID-EMER-FTTP')
        self.assertEquals(t.kind, 'EMERGENCY')

    def test_SouthCalifornia_4(self):
        '''
        Test that "-FTTP" is added to the ticket_type if the work_type field contains "fiber" and the company is "verizon
        Mantis 2236: parser change for CA tickets
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00881.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCalifornia"])
        self.assertEquals(t.ticket_type, 'NORM NEW GRID-FTTP')

    def test_SouthCalifornia_5(self):
        '''
        Test that the caller_email is parsed
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00632.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCalifornia"])
        self.assertEquals(t.caller_email, 'EWORKMAN@AZTEC.US')

    def test_SCA_remarks(self, format="SouthCalifornia"):
        data = [
            ("01209", "BOUNDED AREA", "ON SITE )"),
            ("01207", "UTILIQUEST", "MARK IN ST"),
            ("01181", "BOUNDED AREA-", "NO CONFLICT**"),
            ("01175", "N/W COR OF", "CONFLICT**"),
        ]

        for suffix, start, end in data:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "tickets", "SCA-2010-07-01-remarks",
                 "sca1-2010-06-29-" + suffix + ".txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, [format])
            self.assertTrue(t.work_remarks.startswith(start))
            self.assertTrue(t.work_remarks.endswith(end))

    def test_SCA6_remarks(self):
        data = [
            ("00751", "N/W COR OF", "CONFLICT**"),
            ("00764", "BOUNDED AREA-", "CONFLICT**"),
        ]

        for suffix, start, end in data:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "tickets", "SCA6-2010-07-01-remarks",
                 "sca6-2010-06-29-" + suffix + ".txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ['SouthCaliforniaATT'])
            self.assertTrue(t.work_remarks.startswith(start))
            self.assertTrue(t.work_remarks.endswith(end))

    def test_NCA2_remarks(self):
        # Mantis #2609

        # (ticket_suffix, locations, comments)
        data = [
            ("01100", ("B/SI/O DRAKELEY", "FOR ENT DIST"),
                      ("#1 EXTEND TO", "-ADM 07/01/2010")),
            ("01102", ("ALL/O CHARDONNAY", "GO 300'E"),
                      ("#1 EXTEND TO 08", "-PER DESIREE")),
            ("01105", ("ALL/O BERKSHIRE", "GO 200'N"),
                      ("#1 EXTEND TO 08", "-PER DESIREE")),
            ("01106", ("AT A POINT 300FT", "FOR ENT DIST"),
                      ("RENEWAL OF TICKET", "-PER DESIREE")),
        ]

        # NOTE: Unlike SCA1/6, the "Location:" section is stored in
        # work_description, and the "Comments:" section in work_remarks!
        for suffix, locations, comments in data:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "tickets", "NCA2-2010-07-01-remarks",
                 "nca2-2010-07-01-" + suffix + ".txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ['NorthCaliforniaATT'])
            self.assertTrue(t.work_description.startswith(locations[0]))
            self.assertTrue(t.work_description.endswith(locations[1]))
            self.assertTrue(t.work_remarks.startswith(comments[0]))
            self.assertTrue(t.work_remarks.endswith(comments[1]))

    # XXX move to "DSL" once stable
    def test_SouthCaliforniaATT(self):
        tl = ticketloader.TicketLoader("../testdata/SouthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SouthCaliforniaATT"])
        ae = self.assertEquals

        ae(t.ticket_type, "NORM NEW GRID")
        ae(t.ticket_format, "SCA6")
        ae(t.transmit_date, "2007-01-31 07:43:36")
        ae(t.ticket_number, "A70310080")
        ae(t.call_date, "2007-01-31 07:38:00")
        ae(t.work_state, "CA")
        ae(t.work_county, "LOS ANGELES")
        ae(t.work_city, "ALHAMBRA")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "W MISSION RD")
        ae(t.work_cross, "S MARGUERITA AVE")
        ae(t.work_type, "REPLACE DAMAGED POWER POLE")
        ae(t.work_date, "2007-02-05 09:00:00")
        ae(t.work_description, "MARK BY")
        ae(t.company, "SC EDISON")
        ae(t.con_name, "SC EDISON")
        ae(t.caller, "HARRY SINGH")
        ae(t.con_address, "1000 POTRERO GRANDE")
        ae(t.con_city, "MONTEREY PARK")
        ae(t.con_state, "CA")
        ae(t.con_zip, "")
        ae(t.caller_phone, "323-720-5205")
        self.assertAlmostEqual(t.work_lat,34.083412,5)
        self.assertAlmostEqual(t.work_long,-118.138161,5)
        ae([x.client_code for x in t.locates], ['PTT19'])
        ae(t.serial_number, "98")
        ae(t.locates[0]._plat, "ALHBCA01")
        ae(t.image[:5], 'PTT19')
        self.assertAlmostEqual(t.work_lat,34.083412,6)
        self.assertAlmostEqual(t.work_long,-118.138161,6)

    def test_Alabama_1(self):
        tl = ticketloader.TicketLoader("../testdata/Alabama-0.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        ae = self.assertEquals

        ae(t.transmit_date, "2002-09-23 11:19:33")
        ae(t.ticket_type, "RUSH UPDT STRT")
        ae(t.ticket_number, "0264988")
        ae(t.revision, "00")
        ae(t.call_date, "2002-09-23 11:17:00")
        ae(t.work_state, "AL")
        ae(t.work_county, "BALDWIN")
        ae(t.work_city, "DAPHNE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "CIPRIANO CT")
        ae(t.work_cross, "US HIGHWAY 98")
        ae(t.work_description[:10], "EASTERN SHORE"[:10])
        ae(t.work_description[-10:], "UP TO THE BLDG"[-10:])
        ae(t.work_remarks[:10], "<<<<< UPDATE"[:10])
        ae(t.work_remarks[-10:], "CIPRIANO CT ---"[-10:])
        ae(t.map_page, "T05SR02E06*E")
        ae(t.work_date, "2002-09-23 12:00:00")
        ae(t.work_type, "INSTALL CABLES")
        ae(t.company, "BELL SOUTH")
        ae(t.con_name, "MASTEC")
        ae(t.con_address, "3036 DIAL ST")
        ae(t.con_city, "PRICHARD")
        ae(t.con_state, "AL")
        ae(t.con_zip, "36612")
        ae(t.caller, "SHIRLEY")
        ae(t.caller_phone, "251-456-4800")
        ae(len(t.locates), 9)
        ae(t.locates[0].client_code, "BCSS01")
        self.assertEquals(t._dup_fields, [])

    def test_Alabama_2(self):
        tl = ticketloader.TicketLoader("../testdata/Alabama-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw)
        self.assertEquals(t.work_address_number, "27591")
        self.assertEquals(len(t.locates), 12)

        raw = tl.tickets[1]
        t = self.handler.parse(raw)
        self.assertEquals(t.caller_email, "tnfarmer@southernco.com")
        self.assertEquals(t.caller_fax, "251-434-5432")
        self.assertEquals(t.work_remarks, "")
        self.assertEquals(t._dup_fields, [])

    def test_Kentucky_1(self):
        tl = ticketloader.TicketLoader("../testdata/Kentucky-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        ae = self.assertEquals

        ae(t.ticket_type, "RENEWAL ***** ROUTINE *****")
        ae(t.transmit_date, "2002-11-19 15:34:54")
        ae(t.call_date, "2002-11-19 15:34:00")
        ae(t.ticket_number, "20024701714")
        ae(t.work_county, "CHRISTIAN")
        ae(t.work_city, "OAK GROVE")
        ae(t.work_address_street, "PEMBROKE RD")
        ae(t.work_type, "WATER & SEWER INSTALL")
        ae(t.work_date, "2002-11-21 15:35:00")
        ae(t.caller, "NEAL FIELDS")
        ae(t.caller_phone, "931-920-4720")
        ae(t.con_name, "NEW SOUTH CORPORATION")
        ae(t.con_address, "131 DEAN DR")
        ae(t.con_city, "CLARKSVILLE")
        ae(t.con_state, "TN")
        ae(t.con_zip, "37040")
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "BELLTN")
        ae(t.kind, "NORMAL")

        # test kind == EMERGENCY
        raw = tl.tickets[11]
        t = self.handler.parse(raw, ["Kentucky"])
        ae(t.kind, "EMERGENCY")

    def test_Kentucky_2(self):
        tl = ticketloader.TicketLoader("../testdata/Kentucky-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Kentucky"])
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, "BELLTN")

        # test another ticket
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Kentucky"])
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, "BELLTN")
        self.assertEquals(t._dup_fields, [])

        tl = ticketloader.TicketLoader("../testdata/Kentucky-3.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Kentucky"])
        self.assertEquals(t.ticket_type, "***** ROUTINE *****")

    def test_Dallas1_1(self):
        tl = ticketloader.TicketLoader("../testdata/Dallas1-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Dallas1"])
        ae = self.assertEquals

        ae(t.ticket_number, "022940663")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.call_date, "2002-10-21 08:00:00")
        ae(t.map_page, "MAPSCO 70,N")
        ae(t.work_county, "DALLAS")
        ae(t.work_city, "DALLAS")
        ae(t.work_address_number, "14200")
        ae(t.work_address_street, "C F HAWN FWY")
        ae(t.work_date, "2002-10-21 08:00:00")
        ae(t.work_type, "EMER- SEWER MAIN REPAIR")
        ae(t.caller, "RUDY CARRILLO")
        ae(t.con_name, "CITY OF DALLAS SEWER")
        ae(t.company, "CITY OF DALLAS SEWER")
        ae(t.caller_contact, "RUDY CARRILLO")
        ae(t.caller_phone, "( 214 )670-7553")
        ae(t.caller_fax, "( 214 )670-7480")
        ae(t.caller_email, "CALLER COULD NOT PROVIDE E-MAIL")
        ae(t.work_remarks, "MARK ALL UNDERGROUND FACILITIES AS NECESSARY")
        ae(t.work_cross, "WOODY RD")
        ae(t.transmit_date, "2002-10-21 08:02:57")
        ae(len(t.locates), 2)
        ae(t.locates[0].client_code, "DL1")
        ae(t.locates[1].client_code, "DL1G")
        ae(t.work_description.startswith("EMER- SEWER MAIN REPAIR"), 1)
        ae(t.work_description.endswith("EASMENT."), 1)
        self.assertEquals(t._dup_fields, [])

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Dallas1"])
        ae(t.map_page, "MAPSCO 24,Y")

        raw = raw.replace("MAPSCO 24,Y", "MAPSCO 24,Y,DALLAS")
        t = self.handler.parse(raw, ["Dallas1"])
        ae(t.map_page, "MAPSCO 24,Y")

        raw = raw.replace('DL1', 'GP7')
        t = self.handler.parse(raw, ["Dallas1"])
        self.assertEquals(len(t.locates), 2)
        #self.assertEquals(t.locates[0].client_code, 'GP7E')
        self.assertEquals(t.locates[0].client_code, 'GP7')
        self.assertEquals(t.locates[1].client_code, 'GP7G')

    def test_Dallas1_2(self):
        tl = ticketloader.TicketLoader("../testdata/Dallas1-2.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Dallas1"])

        self.assertEquals(t.call_date, "2003-01-01 08:33:00")
        self.assertEquals(t.work_date, "2003-01-01 08:45:00")

    def test_Dallas1_anomalies(self):
        # test map page anomalies
        tl = ticketloader.TicketLoader("../testdata/Dallas1-1.txt")
        original = tl.tickets[0]

        raw = original.replace("MAPSCO 70,N", "MAPSCO 1388,7K--MARKED")
        t = self.handler.parse(raw, ['Dallas1'])
        self.assertEquals(t.map_page, "MAPSCO 1388,7K")

        raw = original.replace("MAPSCO 70,N", "MAPSCO 11,R THRU 11A-S")
        t = self.handler.parse(raw, ['Dallas1'])
        self.assertEquals(t.map_page, "MAPSCO 11,R")


    # Dallas2 isn't in use anymore -- 2005-12-29
    def __test_Dallas2_1(self):
        tl = ticketloader.TicketLoader("../testdata/Dallas2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Dallas2"])
        ae = self.assertEquals

        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "DFD")
        ae(t.ticket_type, "ROUTINE")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "022945290")
        ae(t.call_date, "2002-10-21 09:21:00")
        ae(t.map_page, "NONE")
        ae(t.work_county, "LAMAR")
        ae(t.work_city, "DEPORT")
        ae(t.work_address_number, "178")
        ae(t.work_address_street, "1ST SW")
        ae(t.work_date, "2002-10-23 09:15:00")
        ae(t.work_type, "TREE REMOVAL AND BURY STUMPS")
        ae(t.caller, "PAULINE STANSEL")
        ae(t.con_name, "NOT PROVIDED")
        ae(t.company, "PAULINE STANSEL")
        ae(t.caller_contact, "PAULINE STANSEL")
        ae(t.caller_phone, "( 903 )737-0634")
        ae(t.caller_fax, "( 000 )000-0000")
        ae(t.caller_email, "CALLER COULD NOT PROVIDE EMAIL")
        ae(t.work_remarks, "MARK ALL UNDERGROUND FACILITIES AS NECESSARY")
        ae(t.work_cross, "HWY 271")
        ae(t.transmit_date, "2002-10-21 09:24:34")
        self.assertEquals(t._dup_fields, [])

    def test_Arkansas_1(self):
        tl = ticketloader.TicketLoader("../testdata/Arkansas-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Arkansas"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-10-23 12:40:00")
        ae(t.ticket_type, "NORMAL") # maybe 'NORMAL NOGRID'
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "021023-0678")
        ae(t.call_date, "2002-10-23 12:40:00")
        ae(t.con_name, "SOUTHWEST ARKANSAS ELECTRIC COOPERATIVE")
        ae(t.caller, "JUDY RUTLEDGE")
        ae(t.con_address, "PO BOX 1807")
        ae(t.con_city, "TEXARKANA")
        ae(t.con_state, "AR")
        ae(t.con_zip, "75504")
        ae(t.work_date, "2002-10-25 12:45:00")
        ae(t.work_state, "AR")
        ae(t.work_county, "MILLER")
        ae(t.work_city, "TEXARKANA")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "SUMMIT DR")
        ae(t.work_cross, "DUDES DR")
        ae(t.work_type, "ELECTRIC, INSTALL SERVICE")
        ae(t.company, "CHARLES SANTIFER")
        ae(t.map_page, "")
        self.assertAlmostEqual(t.work_lat,33.4173611111,5)
        self.assertAlmostEqual(t.work_long,-93.9572777778,5)
        ae(len(t.locates), 3)
        ae(t.locates[0].client_code, "REAFC091")
        self.assertEquals(t._dup_fields, [])

    def test_Arkansas_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FNL2","FNL2-2008-12-30-00001.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Arkansas"])
        ae = self.assertEquals
        ae(t.call_date, '2008-12-30 08:44:00')
        ae(t.caller, 'DANIEL CAUDLE')
        ae(t.caller_altphone, '(870) 743-2323')
        ae(t.caller_contact, 'DANIEL CAUDLE')
        ae(t.caller_fax, '(870) 743-9491')
        ae(t.caller_phone, '(870) 743-2323')
        ae(t.company, 'HDI CONTRACTORS')
        ae(t.con_address, '710 HWY 62/65 S')
        ae(t.con_city, 'HARRISON')
        ae(t.con_name, 'HDI CONTRACTORS')
        ae(t.con_state, 'AR')
        ae(t.con_zip, '72601')
        ae(t.explosives, 'N')
        ae(t.kind, 'NORMAL')
        ae(t.operator, 'MJOHNSON')
        ae(t.ticket_number, '081230-0164')
        ae(t.ticket_type, 'NORMAL')
        ae(t.transmit_date, '2008-12-30 08:44:00')
        ae(t.work_address_street, 'CLARK ST')
        ae(t.work_city, 'JASPER')
        ae(t.work_county, 'NEWTON')
        ae(t.work_cross, 'SYCAMORE ST')
        ae(t.work_date, '2009-01-02 08:45:00')
        ae(t.work_description, 'WORKING APPX .3 MI W OF INT - LOCATE FRONT OF PPTY  ')
        self.assertAlmostEqual(t.work_lat, 36.0049211445, 5)
        self.assertAlmostEqual(t.work_long, -93.1923644159)
        ae(t.work_state, 'AR')
        ae(t.work_type, 'WATER, INSTALL SERVICE')
        ae(len(t.locates), 3)

    def test_AR1393(self):
        tl = ticketloader.TicketLoader("../testdata/AR1393-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['AR1393'])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-08-16 04:57:00")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "050816-0002")
        ae(t.call_date, "2005-08-16 04:57:00")
        ae(t.con_name, "VAN BUREN WATER AND SEWER DEPT")
        ae(t.con_city, "VAN BUREN")
        ae(t.con_state, "AR")
        ae(t.con_zip, "72956")
        ae(t.work_date, "2005-08-16 06:00:00")
        ae(t.work_state, "AR")
        ae(t.work_county, "CRAWFORD")
        ae(t.work_city, "VAN BUREN")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "S 12TH ST")
        ae(t.work_cross, "OZIER ST")
        self.assertAlmostEqual(t.work_lat,35.4331176868,5)
        self.assertAlmostEqual(t.work_long,-94.3470883135,5)
        ae([x.client_code for x in t.locates],
           ['AOG', 'CT90RLVL', 'OGE01', 'SWBF05'])

    def test_Tulsa_1(self):
        tl = ticketloader.TicketLoader("../testdata/Tulsa-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Tulsa"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-10-21 07:48:00")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.ticket_number, "02102107480140")
        ae(t.call_date, "2002-10-21 07:45:00")
        ae(t.caller, "LARRY")
        ae(t.con_name, "ONG-BROKEN ARROW")
        ae(t.con_city, "BROKEN ARROW")
        ae(t.con_state, "OK")
        ae(t.con_zip, "74012")
        ae(t.caller_contact, "LARRY")
        ae(t.caller_phone, "(918) 455-8171")
        ae(t.caller_fax, "(918) 451-0899")
        ae(t.work_date, "2002-10-22 08:00:00")
        ae(t.work_state, "OK")
        ae(t.work_county, "TULSA")
        ae(t.work_city, "BROKEN ARROW")
        ae(t.work_address_number, "214")
        ae(t.work_address_street, "W PITTSBURG ST")
        ae(t.work_cross, "")
        ae(t.work_type, "GAS LINE")
        ae(t.company, "ONG-BROKEN ARROW")
        self.assertAlmostEqual(t.work_lat,36.0355167389,5)
        self.assertAlmostEqual(t.work_long,-95.7927818298,5)
        ae(len(t.locates), 4)
        ae(t.locates[0].client_code, "T01565")
        self.assertEquals(t._dup_fields, [])

    def test_SanAntonio_1(self):
        tl = ticketloader.TicketLoader("../testdata/SanAntonio-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SanAntonio"])
        ae = self.assertEquals

        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "CWD")
        ae(t.ticket_type, "ROUTINE")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "023085079")
        ae(t.call_date, "2002-11-04 07:58:00")
        ae(t.work_county, "LA SALLE")
        ae(t.work_city, "COTULLA")
        ae(t.work_address_number, "1401")
        ae(t.work_address_street, "TILDEN ST")
        ae(t.work_date, "2002-11-06 08:15:00")
        ae(t.work_type, "BURY SVC LINE")
        ae(t.caller, "MIKE VARA")
        ae(t.con_name, "VARA'S TRENCHING")
        ae(t.company, "SWBT")
        ae(t.caller_contact, "MIKE VARA")
        ae(t.caller_phone, "( 830 )278-7408")
        ae(t.caller_fax, "( 830 )278-6996")
        ae(t.caller_email, "VARATREN@HILLCONNECT.COM")
        ae(t.work_remarks, "MARK ALL UNDERGROUND FACILITIES AS NECESSARY")
        ae(t.work_cross, "CHEROKEE")
        ae(t.map_page, "NONE")
        ae(t.transmit_date, "2002-11-04 08:31:43")
        self.assertEquals(t._dup_fields, [])

    def test_Harlingen_1(self):
        tl = ticketloader.TicketLoader("../testdata/Harlingen-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Harlingen"])
        ae = self.assertEquals

        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "CSD")
        ae(t.ticket_type, "NON-COMPLIANT")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "022945044")
        ae(t.call_date, "2002-10-21 07:37:00")
        ae(t.work_county, "CAMERON")
        ae(t.work_city, "HARLINGEN")
        ae(t.work_address_number, "0")
        ae(t.work_address_street, "BASS BLVD")
        ae(t.work_date, "2002-10-22 08:00:00")
        ae(t.work_type, "INSTL DRIVEWAYS")
        ae(t.caller, "CHARLIE GARZA")
        ae(t.con_name, "GRANDE CONSTRUCTION")
        ae(t.company, "STATE OF TEXAS")
        ae(t.caller_contact, "CHARLIE GARZA")
        ae(t.caller_email, "RNJNS@CS.COM")
        ae(t.work_remarks, "MARK ALL UNDERGROUND FACILITIES AS NECESSARY")
        ae(t.work_cross, "BUSINESS 83")
        ae(t.map_page, "NONE")
        ae(t.transmit_date, "2002-10-21 07:49:54")
        self.assertEquals(t._dup_fields, [])

        raw = raw.replace('022945044 to 919564230353 at 07:49:54 on MON, 10/21/02 for CSD #0006','')
        t = self.handler.parse(raw, ["Harlingen"])
        ae(t.transmit_date,t.call_date)

        raw = tl.tickets[1]
        raw = raw.replace('CSD', 'SSR') # or we won't parse map_ref
        t = self.handler.parse(raw, ["Harlingen"])
        self.assertEquals(t.map_ref, "HIDALGO")

        raw = tl.tickets[1]
        raw = raw.replace('CSD', 'PUB')
        t = self.handler.parse(raw, ['Harlingen'])
        self.assertEquals(len(t.locates), 4)
        self.assertEquals(t.locates[-1].client_code, 'PUBE')

    def test_Houston1_1(self):
        tl = ticketloader.TicketLoader("../testdata/Houston1-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Houston1"])
        ae = self.assertEquals

        ae(t.ticket_type, "NOTI-2D REQUEST")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "29400736")
        ae(t.transmit_date, "2002-10-21 09:57:00")
        ae(t.operator, "retahe")
        ae(t.call_date, "2002-10-21 08:55:00")
        ae(t.work_date, "2002-10-21 09:00:00")
        ae(t.work_city, "KATY")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "BARTON MEADOW LN")
        ae(t.work_cross, "LAUREL CHASE LN")
        ae(t.work_type, "INSTALLING UNDERGROUND GAS AND-INSTALLING UNDERG")
        ae(t.work_description.startswith("LSTK=29104239"), 1)
        ae(t.work_description.endswith("FAXBACKREQ: N"), 1)
        ae(t.work_remarks.startswith("WK TO BEGIN DT"), 1)
        ae(t.work_remarks.endswith("10/21/02 08:55:49"), 1)
        ae(t.con_name, "DID NOT PROVIDE")
        ae(t.caller_contact, "PHILLIP TAMBORELLO")
        ae(t.caller_phone, "(713)691-3616")
        ae(t.company, "RELIANT ENERGY HLP")
        ae(t.con_state, "TX")
        ae(t.map_page, "TX 485 N")
        ae(t.work_state, "TX")
        ae(len(t.locates), 1)   # was: 3
        ae(t.locates[0].client_code, "TWC30")
        #ae(t.locates[1].client_code, "RELIAN01")
        self.assertEquals(t._dup_fields, [])

    def test_TX6003(self):
        tl = ticketloader.TicketLoader("../testdata/6003.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['TX6003'])
        ae = self.assertEquals

        ae(t.ticket_number, "36251172")
        ae(t.ticket_type, "NOTI-ROUTINE DUPLICATION")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2005-12-29 10:09:00")
        ae(t.call_date, "2005-12-28 09:08:00")
        ae(t.work_date, "2005-12-30 09:15:00")
        ae(t.work_city, "COVINGTON")
        ae(t.work_address_number, "10371")
        ae(t.work_address_street, "HWY 171")
        ae(t.work_cross, "FM 2719")
        ae(t.work_type, "INSTL PHONE LINE-TELEPHONE")
        ae(t.con_name, "FORTUNE COMMUNICATIONS")
        ae(t.caller_contact, "TREY FORTUNE")
        ae(t.caller_phone, "(325)665-5725")
        ae(t.company, "ALLTEL")
        ae(t.con_state, "TX")
        ae(t.map_page, "")
        ae([x.client_code for x in t.locates],
           ['TEST06'])

    def test_Dallas2New(self):
        tl = ticketloader.TicketLoader("../testdata/fdx2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Dallas2New'])
        ae = self.assertEquals

        ae(t.ticket_number, "36252079")
        ae(t.ticket_type, "INSU-SHORT NOT DUPLICATION")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2005-12-29 10:14:00")
        ae(t.call_date, "2005-12-28 10:23:00")
        ae(t.work_date, "2005-12-30 09:45:00")
        ae(t.work_city, "NOCONA")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "NOBLE RD")
        ae(t.work_cross, "HWY 59")
        ae(t.work_type, "SETTING ONE POLE AND ANCHOR-POLE/SIGN INSTALLATION")
        ae(t.con_name, "WISE ELECTRIC COOPERATIVE")
        ae(t.caller_contact, "BRYAN SHRIVER")
        ae(t.caller_phone, "(940)627-2167")
        ae(t.company, "LEE ANN SMITH")
        ae(t.con_state, "TX")
        ae(t.map_page, "")
        ae([x.client_code for x in t.locates],
           ['TEST06'])

    def OLD_test_Houston2_1(self):
        tl = ticketloader.TicketLoader("../testdata/Houston2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Houston2"])
        ae = self.assertEquals

        ae(t.ticket_type, "EMER-EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "29405312")
        ae(t.transmit_date, "2002-10-21 21:06:00")
        ae(t.call_date, "2002-10-21 20:58:00")
        ae(t.work_date, "2002-10-21 21:00:00")
        ae(t.operator, "gwen")
        ae(t.work_city, "HUMBLE CITY")
        ae(t.work_address_number, "527")
        ae(t.work_address_street, "CHARLESTON SQ")
        ae(t.work_cross, "INTERCONTINENTAL VLG")
        ae(t.work_type, "EMERGENCY REPAIR OF WATER MAIN")
        ae(t.work_description, "MARK ENTIRE FRONT EASEMENT.")
        ae(t.work_remarks, "CALLER STATES CREW IS ON SITE NOW")
        ae(t.con_name, "CITY OF HUMBLE")
        ae(t.caller_contact, "GORDON M")
        ae(t.caller_phone, "(281)446-2327")
        ae(t.company, "CITY OF HUMBLE")
        ae(t.con_state, "TX")
        ae(t.map_page, "TX 336 W")
        #ae(len(t.locates), 1)  # was: 4
        self.assertEquals(t._dup_fields, [])

    # For nose
    OLD_test_Houston2_1.will_fail = True

    def test_AlabamaMobile_1(self):
        tl = ticketloader.TicketLoader("../testdata/AlabamaMobile-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["AlabamaMobile"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-10-07 07:30:22")
        ae(t.ticket_type, "EMER NEW STRT")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "0276634")
        ae(t.call_date, "2002-10-07 07:24:00")
        ae(t.work_state, "AL")
        ae(t.work_county, "MOBILE")
        ae(t.work_city, "THEODORE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "US HIGHWAY 90")
        ae(t.work_cross, "PLANTATION RD")
        ae(t.work_description.startswith("THIS SITE IS NEAR"), 1)
        ae(t.map_page, "T05SR02W27SW")
        ae(t.work_date, "2002-10-07 08:27:00")
        ae(t.work_type, "CLEANING UP SULPHER SPILL")
        ae(t.company, "US ENVIRONMENTAL")
        ae(t.con_name, "US ENVIRONMENTAL")
        ae(t.con_address, "4230-A HALLS MILL RD")
        ae(t.con_state, "AL")
        ae(t.con_city, "MOBILE")
        ae(t.con_zip, "36693")
        ae(t.caller, "JEFF")
        ae(t.caller_phone, "251-662-3500")
        ae(len(t.locates), 21)
        ae(t.locates[0].client_code, "APC55B")
        self.assertEquals(t._dup_fields, [])

        raw = raw.replace('Locat:','')
        t = self.handler.parse(raw, ["AlabamaMobile"])
        ae(t.work_description,'')

    def test_AlabamaNew_1(self):
        # fake a call center code, to prevent errors
        import call_centers
        if not call_centers.format2cc.has_key("AlabamaNew"):
            call_centers.format2cc["AlabamaNew"] = "FAM0"

        tl = ticketloader.TicketLoader("../testdata/AlabamaNew-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["AlabamaNew"])
        ae = self.assertEquals

        # we don't test everything, since this is a pro forma ticket
        ae(t.transmit_date, "2003-01-02 10:10:00")
        ae(t.ticket_number, "03002-001")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        self.assertAlmostEqual(t.work_lat,36.1699812,5)
        self.assertAlmostEqual(t.work_long,-86.7720964667,5)
        ae(len(t.locates), 6)
        ae(t.locates[0].client_code, "ATTT01")
        self.assertEquals(t._dup_fields, [])

    def test_Mississippi_1(self):
        tl = ticketloader.TicketLoader("../testdata/Mississippi-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Mississippi"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-12-02 07:29:00")
        ae(t.ticket_type, "NORMAL")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "02120207290012")
        ae(t.call_date, "2002-12-02 07:29:00")
        ae(t.operator, "JOANNA")
        ae(t.con_name, "MASTEC")
        ae(t.caller, "DENNIS AVERA")
        ae(t.con_address, "1980 HIGHWAY 11 & 80 E")
        ae(t.con_city, "MERIDIAN")
        ae(t.con_state, "MS")
        ae(t.con_zip, "39301")
        ae(t.caller_phone, "(601) 485-4171")
        ae(t.caller_contact, "DENNIS AVERA")
        ae(t.work_date, "2002-12-04 07:30:00")
        ae(t.work_state, "MS")
        ae(t.work_county, "LAUDERDALE")
        ae(t.work_city, "MEEHAN")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "OLD HIGHWAY 80")
        ae(t.work_cross, "MEEHAN SUQUALENA RD")
        ae(t.work_description.startswith("**UPDATE**"), 1)
        ae(t.work_description.endswith("IN WHITE."), 1)
        ae(t.work_type, "TELEPHONE, BURY CABLE")
        ae(t.company, "BELLSOUTH")
        ae(t.map_page, "14E06N22SW")
        self.assertAlmostEqual(t.work_lat,32.3354665188,5)
        self.assertAlmostEqual(t.work_long,-88.8616511963,5)
        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "MS2098")
        self.assertEquals(t._dup_fields, [])

    def test_Mississippi2_1(self):
        tl = ticketloader.TicketLoader("../testdata/Mississippi2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Mississippi2"])
        ae = self.assertEquals

        ae(t.transmit_date, "2002-11-01 16:01:00")
        ae(t.ticket_type, "NORMAL")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "02110116040630")
        ae(t.call_date, "2002-11-01 16:01:00")
        ae(t.operator, "AHICKS")
        ae(t.con_name, "CENTERPOINT ENERGY ENTEX")
        ae(t.caller, "AMY HICK")
        ae(t.con_address, "104 CROSSPARK DR")
        ae(t.con_city, "PEARL")
        ae(t.con_state, "MS")
        ae(t.con_zip, "39208")
        ae(t.caller_phone, "(601) 936-0222")
        ae(t.caller_contact, "MARK DENNAN")
        ae(t.work_date, "2002-11-05 16:15:00")
        ae(t.work_state, "MS")
        ae(t.work_county, "RANKIN")
        ae(t.work_city, "BRANDON")
        ae(t.work_address_number, "312")
        ae(t.work_address_street, "EASTRIDGE  DR")
        ae(t.work_cross, "PARKSIDE DR")
        ae(t.work_description,
         "LOCATE ENTIRE LOT14   SHORT SIDE EASTGATE OF CROSSGATES")
        ae(t.work_type, "GAS, INSTALL LINES (S)")
        ae(t.company, "CENTERPOINT ENERGY ENTEX")
        ae(t.map_page, "03E05N08NE")
        self.assertAlmostEqual(t.work_lat,32.2945671082,5)
        self.assertAlmostEqual(t.work_long,-90.0097579956,5)
        ae(len(t.locates), 6)
        ae(t.locates[0].client_code, "MS1058")
        self.assertEquals(t._dup_fields, [])

    def test_exclude(self):
        """ Test the exclude mechanism of parsers. """
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"], exclude=["AGL18", "FUL02"])

        # two term ids are excluded
        self.assertEquals(len(t.locates), 11)

    def testRichmondNC_1(self):
        # format is the same as NorthCarolina
        tl = ticketloader.TicketLoader("../testdata/northcarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["RichmondNC"])

        ae = self.assertEquals
        ae(t.ticket_format, "FCV4")
        ae(t.ticket_number, "A0062728")
        ae(t.ticket_type, "NORM NEW PLCE")
        ae(t.kind, "NORMAL")
        self.assertEquals(t._dup_fields, [])
        # I don't test everything; if this works, so should the rest

    def test_AlabamaFL(self):
        # looks the same as Orlando/Pensacola
        tl = ticketloader.TicketLoader("../testdata/Orlando-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["AlabamaFL"])

        ae = self.assertEquals
        ae(t.ticket_format, "FAM2")
        ae(t.ticket_number, "11621923")
        ae(t.revision, "000")
        self.assertAlmostEqual(t.work_lat,28.3666666667,5)
        self.assertAlmostEqual(t.work_long,-81.4166666667,5)
        self.assertEquals(t._dup_fields, [])

    def test_Delaware1(self):
        # XXX Most of this has been moved to Delaware1.txt testing DSL, but
        # some parts still have to be done here.

        tl = ticketloader.TicketLoader("../testdata/Delaware1-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Delaware1"])

        raw = tl.tickets[42]    # !
        t = self.handler.parse(raw, ["Delaware1"])
        self.assertEquals(t.caller_phone, "(410)712-0056")

        # test changed Delaware format
        tl = ticketloader.TicketLoader("../testdata/fde1-new.txt")

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['Delaware1'])
        self.assertEquals(t.caller_fax, "(610)274-3435")

        # test parsing with double-alpha grid value
        raw = tl.tickets[2]
        raw = raw.replace('C06', 'DD06')
        t = self.handler.parse(raw, ['Delaware1'])
        self.assertEquals(t.map_page, 'NEWC 6 DD06')

    def test_Albuquerque(self):
        tl = ticketloader.TicketLoader("../testdata/Albuquerque-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Albuquerque"])

        self.assertEquals(t.ticket_number, "2003174098")
        self.assertEquals(t.serial_number, "NMOC2003042500454")
        self.assertEquals(t.ticket_type, "RELOCATE")
        self.assertEquals(t.transmit_date, "2003-04-25 15:17:08")
        self.assertEquals(t.call_date, "2003-04-25 15:17:04")
        self.assertEquals(t.work_date, "2003-04-29 15:17:00")
        self.assertEquals(t.con_name, "K R SWERDFEGER (SWERD)")
        self.assertEquals(t.caller_contact, "SLY JOHNSON")
        self.assertEquals(t.caller_phone, "(505)469-5392")
        self.assertEquals(t.caller_altcontact, "")
        self.assertEquals(t.caller_fax, "")
        self.assertEquals(t.work_state, "NM")
        self.assertEquals(t.work_county, "SANTA FE")
        self.assertEquals(t.work_city, "RURAL SANTA FE")
        self.assertEquals(t.work_address_number, "20")
        self.assertEquals(t.work_address_street, "LA BARBARIA")
        self.assertEquals(t.work_cross, "")
        self.assert_(t.work_description.startswith("CABINET WORK"))
        self.assert_(t.work_description.endswith("SITE 35.4"))
        self.assertEquals(t.work_remarks, "")
        self.assertEquals(t.work_type, "SEE ADDITIONAL INFO")
        self.assertEquals(t.company, "QWEST")
        self.assertEquals(t.map_page, "16N10E")

        # locates...
        self.assertEquals(len(t.locates), 8)
        self.assertEquals(t.locates[0].client_code, "USWN")
        self.assertEquals(t.locates[-1].client_code, "QEST")

        # test some more map_pages
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(t.map_page, "16N10E")
        raw = tl.tickets[3]
        t = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(t.map_page, "10N03E16NW")
        raw = tl.tickets[6]
        t = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(t.map_page, "")

        # test strange locates with "=" in the name
        raw = tl.tickets[9]
        t = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(
         [x.client_code for x in t.locates],
         ["QLNA", "COBL", "KICP", "BWUA", "BURL",
          "CONR", "CROS", "EP02", "EP03", "USWA",
          "FCA", "CAR", "EUN", "LIN", "APC",
          "NEO"])

    def test_Albuquerque2(self):
        # much like Albuquerque, so we don't test everything
        tl = ticketloader.TicketLoader("../testdata/Albuquerque2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Albuquerque2"])

        self.assertEquals(t.ticket_number, "2003262867")
        self.assertEquals(t.ticket_type, "STANDARD LOCATE")
        self.assertEquals(t.call_date, "2003-06-26 13:37:15")
        self.assertEquals(t.con_name, "HOME OWNER (HOMEO)")
        self.assertEquals(t.work_state, "NM")
        self.assertEquals(t.work_county, "UNION")
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.serial_number, None)
        # ...required in Albuquerque, but not here

    def test_AlabamaMobileNew(self):
        """ Test parsing of latitude and longitude in AlabamaMobileNew. """
        tl = ticketloader.TicketLoader("../testdata/AlabamaMobileNew-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["AlabamaMobileNew"])

        self.assertAlmostEqual(t.work_lat, 32.3579334981641, 5)
        self.assertAlmostEqual(t.work_long, -86.2383409575744, 5)

        # insert a bogus latitude and see what happens
        raw = raw.replace("32.3579334981641", "BOGUS")
        t = self.handler.parse(raw, ["AlabamaMobileNew"])
        self.assertEquals(t.work_lat, 0)

        self.assertEquals(t._dup_fields, [])

    def test_WashingtonVA(self):
        tl = ticketloader.TicketLoader("../testdata/WashingtonVA-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonVA"])

        self.assertEquals(t.ticket_format, "FMW2")
        self.assertEquals(t.service_area_code, "VA NON PIPELINE")
        self.assertEquals(t.serial_number, "VUPS2003071400002")
        self.assertAlmostEqual(t.work_lat,38.8520833333,5)
        self.assertAlmostEqual(t.work_long,-77.0479166667,5)

    def test_siamese_tickets(self):
        tl = ticketloader.TicketLoader("../testdata/FMW2-siamese.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Washington2"])

        #self.assertEquals(len(t._dup_fields), 5)
        # may change if we add more fields to BaseParser.only_one
        fieldnames = [t[0] for t in t._dup_fields]
        self.assert_("ticket_number" in fieldnames)
        self.assert_("transmit_date" in fieldnames)

    def test_FMW2_more(self):
        # As of 2004-02-24, FMW2 supports *three* formats.  Let's check them...
        tl = ticketloader.TicketLoader("../testdata/fmw2-more.txt")

        t = self.handler.parse(tl.tickets[0], ['VUPSNewFMW2'])
        t = self.handler.parse(tl.tickets[1], ['VUPSNewFMW2'])
        self.assertEquals(t.ticket_number, "A405401232")
        self.assertEquals(t.transmit_date, "2004-02-23 10:59:00")
        t = self.handler.parse(tl.tickets[2], ['Washington'])

        formats = call_centers.cc2format['FMW2']
        for index, raw in enumerate(tl.tickets):
            try:
                t = self.handler.parse(raw, formats)
            except ticketparser.TicketHandlerError:
                self.fail("Ticket #%s not in formats %s" % (index+1, formats))
            self.assertEquals(t.ticket_format, 'FMW2')
        pass

    def test_PARSE_GRIDS(self):
        p=parsers.get_parser('VUPSNewFMW2')
        p.PARSE_GRIDS = False
        inst = p('','VUPSNewFMW2')
        lst = inst.find_grids()
        self.assertEquals(len(lst),0)

    def test_Oregon(self):
        tl = ticketloader.TicketLoader("../testdata/Oregon-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon"])

        ae = self.assertEquals

        ae(t.ticket_number, "123403")
        ae(t.ticket_type, "CREW ON SITE")
        ae(t.transmit_date, "2003-08-05 12:02:00")
        ae(t.call_date, "2003-07-08 15:52:00")
        ae(t.work_date, "2003-07-08 16:00:00")
        ae(t.work_state, "OR")
        ae(t.work_county, "LANE")
        ae(t.work_city, "EUGENE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "E 43RD")
        ae(t.work_cross, "N SHASTA LOOP")
        ae(t.map_page, "18S3W17NE")
        ae(t.work_type, "INSTALLATION OF U/G POWER MAIN CABLE")
        ae(t.work_remarks, "CREW IS ON SITE NOW!!")
        ae(t.con_name, "C2 UTILITY CONTRACTORS")
        ae(t.caller_contact, "CLARENCE COOPER")
        ae(t.caller_phone, "(541)741-2211")
        ae(t.caller_altcontact, "BRIAN")
        ae(t.caller_altphone, "(541)729-8810")
        ae(t.company, "LANE ELECTRIC")

        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "TEST04")
        ae(t.locates[1].client_code, "EUGENE01")

        # test other tickets...
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Oregon"])
        ae(t.kind, "EMERGENCY")
        ae(t.work_address_number, "7752")

    def test_Greenville(self):
        tl = ticketloader.TicketLoader("../testdata/Greenville-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Greenville"])

        ae = self.assertEquals

        # just a few tests to check if it's really the same as SouthCarolina:
        ae(t.ticket_number, "3289766")
        ae(t.kind, "NORMAL")
        ae(t.ticket_type, "Normal")
        ae(t.transmit_date, "2003-09-08 09:14:00")
        ae(t.call_date, "2003-09-08 09:14:00")
        ae(t.work_date, "2003-09-11 09:15:00")
        ae(t.work_address_number, "613")
        ae(t.work_address_street, "LIGHTHOUSE CT")
        ae(len(t.locates), 5)
        ae(t.locates[0].client_code, "BSZT29")

    def test_VUPSNew(self):
        tl = ticketloader.TicketLoader("../testdata/vupsnew-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["VUPSNewFCV3"])

        ae = self.assertEquals
        self.assertAlmostEquals(t.work_lat,38.9534722222,5)
        self.assertAlmostEquals(t.work_long,-77.8131944444,5)
        ae(t.ticket_number, "A324604085")
        ae(t.ticket_type, "UPDT GRID NORM LREQ RSND")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2003-09-08 15:22:00")
        ae(t.call_date, "2003-09-03 16:36:00")
        ae(t.work_county, "FAUQUIER")
        ae(t.work_city, "MARSHALL")
        ae(t.work_state, "VA")
        ae(t.work_address_number, "7265")
        ae(t.work_address_street, "GOOSE CREEK RD")
        ae(t.work_cross, "ATOKA RD")
        ae(t.work_type, "INSTALL UNDERGROUND ELECTRIC SERVICE")
        ae(t.company, "VA POWER")
        ae(t.work_remarks, "PAINT AND FLAG ENTIRE PROPERTY AND COMMON AREA")
        ae(t.work_description, "CALLER MAP REF 7H2\n                "\
           "<<<<< UPDATE OF TICKET #A322703705 >>>>>\n")
        ae(t.con_name, "LEO CONSTRUCTION COMPANY")
        ae(t.con_address, "PO BOX 966")
        ae(t.con_city, "STERLING")
        ae(t.con_state, "VA")
        ae(t.con_zip, "20167")
        ae(t.caller_phone, "703-443-6608")
        ae(t.caller_contact, "EMMA MOSS #")
        ae(t.caller_altphone, "703-443-6608")
        ae(t.caller_fax, "703-443-2104")
        ae(t.map_page, "3857C7748A")
        ae(t.explosives, "N")
        ae(len(t.locates), 3)
        ae(t.locates[0].client_code, "DMN400")
        ae(t.grids, ['3857C7748A', '3857D7748A', '3857D7748B'])

    def test_VUPSNew_transition(self):
        tl = ticketloader.TicketLoader("../testdata/VUPSNew-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Delaware2", "VUPSNewFDE2"])
        self.assert_(t.ticket_number)
        self.assertAlmostEqual(t.work_lat,37.2625,4)
        self.assertAlmostEqual(t.work_long,-75.9979166667,5)

        # test number of tickets
        raw = tl.findfirst("A407504236-00A")
        t = self.handler.parse(raw, ["Fairfax2", "VUPSNewOCC2"])
        self.assertEquals(len(t.locates), 55)

        tl = ticketloader.TicketLoader("../testdata/Richmond3-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Delaware2", "VUPSNewFDE2"])
        self.assert_(t.ticket_number)
        self.assertAlmostEqual(t.work_lat,37.2979166667,5)
        self.assertAlmostEqual(t.work_long,-79.56875,5)

    def test_VUPSNew_empty_fields(self):
        tl = ticketloader.TicketLoader("../testdata/vupsnew-empty-fields.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewFDE2'])
        self.assertEquals(t.grids, ['3715A7559A', '3715B7559A'])
        self.assertAlmostEqual(t.work_lat,37.2625,4)
        self.assertAlmostEqual(t.work_long,-75.9979166667,5)
        # for other parsers this will raise an error... but for subclasses
        # of VUPSNewParser, work_date is not required

    def test_VUPSNewOCC3(self):
        tl = ticketloader.TicketLoader("../testdata/vupsnew-3.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertEquals(len(t.locates), 10)
        self.assertAlmostEquals(t.work_lat,38.81875,5)
        self.assertAlmostEquals(t.work_long,-77.4125,4)

        # test grids
        tl = ticketloader.TicketLoader("../testdata/vupsnew-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertAlmostEquals(t.work_lat,37.2625,4)
        self.assertAlmostEquals(t.work_long,-75.9979166667,5)
        self.assertEquals(t.grids, ['3715A7559A', '3715B7559A'])

        raw = tl.tickets[5]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertEquals(len(t.grids), 9)
        self.assertEquals(t.grids[0], '3731A7647B')
        self.assertAlmostEquals(t.work_lat,37.5326388889,5)
        self.assertAlmostEquals(t.work_long,-76.79375,5)

        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","occ3","OCC3-2009-02-26-00661.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertTrue('LOCATE REAR PROPERTIES OF ABOVE ADDRESS RANGE' in t.work_description)
        self.assertTrue('E3449290' in t.work_description)
        self.assertTrue('NO EXTENSIONS WILL BE GRANTED' in t.work_description)
        self.assertTrue('PAINT AND FLAG REQUESTED' in t.work_description)
        self.assertTrue('CALLER MAP REF: NONE' in t.work_remarks)
        self.assertTrue('VZN: FIOS WORK ORDER NUMBER E3449290' in t.work_remarks)

        # test tickets send through web interface (Mantis #2570)
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "OCC3-2010-05-18", "OCC3-2010-05-13-00232.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertEquals(t.ticket_number, "B013101833")
        self.assertEquals(len(t.grids), 4)

    def test_VUPSNewOCC2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","OCC2-2009-02-26-00494.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC2'])
        self.assertTrue('FACING THE HOUSE, ENTIRE LEFT SIDE.' in t.work_description)
        self.assertTrue('CALLER MAP REF: NONE' in t.work_remarks)

    def test_VUPSNewOCC3(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata", "tickets",
         "OCC3-2010-05-18", "OCC3-2010-05-13-00519.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC3'])
        self.assertEquals('VA', t.work_state)
        self.assertTrue('CHAPEL RD TO THE SIDE' in t.work_description)
        self.assertTrue("PFA0CW/8759/2423.40/550'" in t.work_remarks)

    def test_VUPSNew_200707(self):
        # VUPS format change, active 2007-07-12.
        tl = ticketloader.TicketLoader("../testdata/vupsnew-200707.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['VUPSNewOCC2'])
        ae = self.assertEquals

        ae(t.ticket_number, "A717700006")
        self.assertAlmostEquals(t.work_lat,38.7080357143,5)
        self.assertAlmostEquals(t.work_long,-77.1014880952,5)
        ae(t.transmit_date, "2007-06-26 15:22:00")
        ae(t.call_date, "2007-06-26 06:19:00")
        ae(t.ticket_type, "NEW GRID EMER LREQ RSND")
        ae(t.work_county, "FAIRFAX")
        ae(t.work_city, "")
        ae(t.work_state, "VA")
        ae(t.work_address_number, "9204")
        ae(t.work_address_street, "CHERRYTREE DR")
        ae(t.work_cross, "PRESIDENTIAL DR")
        ae(t.work_description, 'MARK ENTIRE PROPERTY')
        self.assertTrue('CALLER MAP REF: 29K9' in t.work_remarks)
        self.assertTrue('CREW ON SITE SEE FOR ADDITIONAL INFO,' in t.work_remarks)
        ae(t.work_type, "WATER SERVICE - REPAIR OR REPLACE")
        ae(t.company, "HO/WEIGAND 703-780-8280")
        ae(t.con_name, "ABLE PLUMBING, INC")
        ae(t.con_address, "PO BOX 1206")
        ae(t.con_city, "LORTON")
        ae(t.con_state, "VA")
        ae(t.con_zip, "22199")
        ae([x.client_code for x in t.locates],
           ['COX906', 'DOM400', 'FCU901', 'FCW902', 'VZN906', 'WGL904'])

    def test_VUPSNew_Meet(self):
        """
        Test VUPS Meet ticket
        """
        for format in [['VUPSNewFCV3'],['VUPSNewFDE2'],['VUPSNewFPK1'],['VUPSNewOCC2']]:
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","VUPSNew","FCV3-2008-04-04-00039.txt"))
            raw = tl.tickets[0]
            t = self.handler.parse(raw, format)
            # The work date is set to the meet date for meet tickets
            self.assertEquals(t.work_date,'2008-04-08 10:00:00')
            self.assertAlmostEquals(t.work_lat,38.425,3)
            self.assertAlmostEquals(t.work_long,-77.4041666667,5)


    def test_Houston_centerpoint(self):
        tl = ticketloader.TicketLoader("../testdata/Houston1-centerpoint.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Houston1'])

        self.assertEquals(len(t.locates), 2)
        # RELIAN01, CP-G
        self.assertEquals(t.locates[0].client_code, "RELIAN01")
        self.assertEquals(t.locates[1].client_code, "CP-G")

    def test_Dallas1_high_profile(self):
        # when MapRef is HP, set ticket_type to 'HP - ' + ticket type
        tl = ticketloader.TicketLoader("../testdata/Dallas1-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('SEAGOVILLE', 'HP')
        raw = raw.replace('EMERGENCY', 'NORMAL')
        t = self.handler.parse(raw, ['Dallas1'])
        self.assertEquals(t.ticket_type, 'HP - NORMAL')

        # emergency tickets stay emergencies
        raw = raw.replace('NORMAL', 'EMERGENCY')
        t = self.handler.parse(raw, ['Dallas1'])
        self.assertEquals(t.ticket_type, 'EMERGENCY')

        # does it also work if there is no MapRef?
        raw = raw.replace('EMERGENCY', 'NORMAL')
        raw = raw.replace('HP', '')
        t = self.handler.parse(raw, ['Dallas1'])
        self.assertEquals(t.ticket_type, 'NORMAL')

        # test TX6001 high profile
        tl = ticketloader.TicketLoader("../testdata/TX6001-1.txt")
        for raw in tl.tickets:
            if raw.find('041942954') >= 0:
                break
        t = self.handler.parse(raw, ['TX6001'])
        self.assertEquals(t.ticket_type, 'HP - ROUTINE')

    def test_Arizona(self):
        tl = ticketloader.TicketLoader("../testdata/Arizona-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Arizona'])

        ae = self.assertEquals
        ae(t.ticket_number, '2003110302071')
        ae(t.ticket_type, 'DAMAGED UTILITY')
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, 'CONTE06')
        ae(t.transmit_date, '2003-11-06 14:50:00')
        ae(t.call_date, t.transmit_date)
        ae(t.work_date, "2003-11-04 08:00:00")
        ae(t.due_date, "2003-11-05 17:00:00")
        ae(t.legal_due_date, t.due_date)
        ae(t.work_description, "STREET ADDRESS: 912 NAVAJO AVE LOC ENTIRE ALLEY BEHIND ADDRESS")
        ae(t.work_type, "REPAIR WATER LINE LEAK")
        ae(t.work_remarks.startswith('***Boring'), 1)
        ae(t.con_name, "TOWN OF PARKER")
        ae(t.caller_contact, "ROBERT WARD")
        ae(t.caller_phone, "(928)669-9265")
        ae(t.work_state, "AZ")
        ae(t.work_county, "LA PAZ")
        ae(t.work_city, "PARKER")
        ae(t.work_address_number, "912")
        ae(t.work_address_street, "NAVAJO AVE")
        ae(t.map_page, "9N20W01")
        self.assertAlmostEqual(t.work_lat, 34.1482435,5)
        self.assertAlmostEqual(t.work_long, -114.2957945,5)

        # test an Arizona message
        tl = ticketloader.TicketLoader("../testdata/goodmorning.txt")
        raw = [r for r in tl.tickets if r.find("Message From: Arizona") > -1][0]
        t = self.handler.parse(raw, ['Arizona'])
        self.assert_(isinstance(t, ticketparser.GMMessage),
         "Raw message should be parsed as GMMessage")

    def test_Wisconsin1(self):
        tl = ticketloader.TicketLoader("../testdata/Wisconsin1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Wisconsin1"])

        ae = self.assertEquals
        ae(t.ticket_number, "20034610058")
        ae(t.work_state, "WI")  # not on ticket
        ae(t.ticket_type, "STANDARD")  # we parse both Header and Type now
        ae(t.kind, "NORMAL")
        ae(t.work_date, "2003-11-19 08:57:00")
        ae(t.call_date, "2003-11-14 08:54:26")
        ae(t.transmit_date, "2003-11-14 09:00:22")
        ae(t.caller_contact, "MIKE WESTFALL")
        ae(t.caller_phone, "(414)641-0625")
        ae(t.con_name, "THOMPSON CORPORATION")
        ae(t.con_address, "12760 W NORTH AVE")
        ae(t.con_city, "STILLWATER")
        ae(t.con_state, "WI")
        ae(t.con_zip, "55082")
        ae(t.work_county, "WAUKESHA")
        ae(t.work_city, "NEW BERLIN CITY")
        ae(t.work_address_street, "TWILIGHT WAY")
        ae(t.work_cross, "")
        ae(t.work_type, "INSTALL DRAINAGE PIPE")
        ae(t.company, "THOMPSON CORPORATION")
        self.assert_(len(t.work_description) > 20, `t.work_description`)
        self.assert_(len(t.work_remarks) > 20, `t.work_remarks`)
        ae(len(t.locates), 10)
        ae(t.locates[0].client_code, "NBU01")
        ae(t.locates[6].client_code, "TEST")
        # The ticket has lat/long data swapped
        ae(t.work_lat,0.0)
        ae(t.work_long,0.0)

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Wisconsin1"])

        ae(t.ticket_type, "PRINTS CORRECTION")
        ae(t.work_cross, "INDUSTRIAL BLVD")
        # The ticket has lat/long data swapped
        ae(t.work_lat,0.0)
        ae(t.work_long,0.0)

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ["Wisconsin1"])

        ae(t.caller_contact, "MARIO BISSON")
        ae(t.con_name, "TELDIG SYSTEMS")
        ae(t.con_address, "575 E ST-JOSEPH ST")
        ae(t.con_city, "QUEBEC")
        ae(t.con_state, "QC")
        ae(t.con_zip, "G1K3B7")
        # The ticket has lat/long data swapped
        ae(t.work_lat,0.0)
        ae(t.work_long,0.0)
        # this still works, but we don't parse the optional email

    def test_Wisconsin2_1(self):
        tl = ticketloader.TicketLoader("../testdata/Wisconsin2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Wisconsin2"])

        ae = self.assertEquals
        ae(t.ticket_number, "9104656")
        ae(t.ticket_type, "LORQA Standard Locate")
        ae(t.transmit_date, "2003-12-03 16:11:00")
        ae(t.call_date, t.transmit_date)
        ae(t.work_type, "PLACING A POLE AND ANCHOR/POSS BORING")
        ae(t.company, "SBC")
        ae(t.work_date, "2003-12-08 16:15:00")
        ae(t.caller_contact, "KEVIN MACGLASHIN")
        ae(t.caller_phone, "(920) 337-9980")
        ae(t.con_name, "HOLTGER BROTHERS")
        ae(t.con_address, "950 W MAIN AVE")
        ae(t.con_state, "WI")
        ae(t.con_zip, "54115")
        self.assert_(t.work_description.startswith("MARK THE E HALF"))
        self.assert_(t.work_description.endswith("WIDTH OF THE LOT"))

        ae(t.work_county, "RACINE")
        ae(t.work_city, "RACINE")   # and not C/RACINE
        ae(t.work_address_number,  "1417")
        ae(t.work_address_street, "MARQUETTE ST")
        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "AMT02")

        self.assertAlmostEqual(t.work_lat,42.7384425,5)
        self.assertAlmostEqual(t.work_long,-87.792678,5)

        # try other tickets, especially the addresses
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(t.work_address_number, "3014")
        ae(t.work_address_street, "COUNTY RD W")
        self.assertAlmostEqual(t.work_lat,43.0285325,5)
        self.assertAlmostEqual(t.work_long,-89.1065005,5)

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(t.work_address_number, "1428")
        ae(t.work_address_street, "LA SALLE ST")
        self.assertAlmostEqual(t.work_lat,42.738435,5)
        self.assertAlmostEqual(t.work_long,-87.7920035,5)

        raw = tl.tickets[3]
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(t.work_address_number, "")
        ae(t.work_address_street, "COUNTY HWY G")

        raw = tl.tickets[4]
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(t.work_address_number, "9211")
        ae(t.work_address_street, "BROWN ST")
        ae(t.work_remarks, "EMER DUE TO SERVICE OUTAGE")

        raw = tl.tickets[5]
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(t.con_state, 'ON')
        ae(t.con_zip, '')

        raw = tl.tickets[6] # has long locates section
        t = self.handler.parse(raw, ["Wisconsin2"])
        ae(len(t.locates), 17)
        ae(t.locates[16].client_code, 'WNG60')
        ae(t.work_city, 'MILWAUKEE')
        ae(t.work_county, 'MILWAUKEE')
        ae(t.work_type, 'SOIL BORINGS')
        ae(t.caller_phone, '(414) 378-3688')
        ae(t.con_name, 'HNTB CORP')
        ae(t.con_address, '11414 W PARK PL  SUITE 300')
        ae(t.con_zip, '53224')
        ae(t.con_state, 'WI')

        raw = tl.tickets[7] # also long
        t = self.handler.parse(raw, ['Wisconsin2'])
        ae(t.con_name, 'WISCONSIN AIR NATIONAL GUARD')
        ae(t.work_address_number, '1919   E')
        ae(t.work_address_street, 'GRANGE AVE')
        ae(t.con_city, 'MILWAUKEE')

    def test_Wisconsin2_2(self):
        tl = ticketloader.TicketLoader("../testdata/Wisconsin2.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Lats:  42.739190,42.737695  Lons: -87.793007,-87.792349',
                          'Lats:                       Lons:  ')
        t = self.handler.parse(raw, ["Wisconsin2"])


        self.assertEquals(t.work_lat,0)
        self.assertEquals(t.work_long,0)

    def test_GreenvilleNew(self):
        tl = ticketloader.TicketLoader("../testdata/GreenvilleNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GreenvilleNew"])

        ae = self.assertEquals
        ae(t.transmit_date, "2004-01-21 15:35:01")
        ae(t.ticket_type, "Emergency")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "0401210012")
        ae(t.call_date, "2004-01-21 08:04:11")
        ae(t.work_date, "2004-01-22 09:00:00")
        ae(t.work_state, "SC")
        ae(t.work_county, "LAURENS")
        ae(t.work_city, "LAURENS")
        ae(t.work_address_number, "1")
        ae(t.work_address_street, "TECHNOLOGY PL")
        ae(t.work_cross, "HWY 14")
        ae(t.work_type, "SEE REMARKS")
        self.assert_(t.work_remarks.startswith("BURYING NEW"))
        self.assert_(t.work_remarks.endswith("CALLER//"), `t.work_remarks[-20:]`)
        self.assert_(t.work_description.startswith("BURYING NEW"))
        self.assert_(t.work_description.endswith("CALLER//"), `t.work_remarks[-20:]`)
        ae(t.company, "PIEDMONT RURAL TELEPHONE")
        ae(t.caller, "GENE WHITE")
        ae(t.con_name, "PIEDMONT RURAL TELEPHONE")
        ae(t.con_address, "UNKNOWN")
        ae(t.caller_phone, "(864) 682-3101")
        ae(t.caller_fax, "")
        ae(t.caller_email, "")
        ae(t.map_page[:2], "34")
        ae(t.map_page[5:7], "82")
        ae(len(t.locates), 2)
        self.assertAlmostEqual(t.work_lat,34.5222446246,5)
        self.assertAlmostEqual(t.work_long,-82.0487500315,5)
        ae(t.locates[0].client_code, "DPCZ07")
        ae(t.locates[1].client_code, "LAU27")

        # test ticket with 0, 0 for lat, long
        for raw in tl.tickets:
            if string.find(raw, "Lat/Long: 0, 0") > -1:
                break
        else:
            self.fail("Ticket not found")

        t = self.handler.parse(raw, ["GreenvilleNew"])
        self.assertEquals(t.ticket_number, "0402050945")
        self.assertEquals(t.map_page, "")

        for raw in tl.tickets:
            if string.find(raw, "U.S. Mail") > -1:
                break
        else:
            self.fail("Ticket not found")

        t = self.handler.parse(raw, ["GreenvilleNew"])
        self.assertEquals(t.ticket_number, "0403032355")

        # try another ticket with a known problem
        tl = ticketloader.TicketLoader("../testdata/GreenvilleNew-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GreenvilleNew"])
        self.assertEquals(t.work_address_number, "10")
        self.assertEquals(t.work_address_number_2, "101")
        self.assertEquals(t.work_address_street, "DEER TRACK RD")

    def test_GreenvilleNew_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata", 
             "sample_tickets", "1421", "1421-2008-12-17-01353.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GreenvilleNew"])

        ae = self.assertEquals

    def test_duplicate_locates(self):
        tl = ticketloader.TicketLoader("../testdata/Colorado-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('ATDW00', 'UNPR01')
        t = self.handler.parse(raw, ["Colorado"])
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'UNPR01')
        # only UNPR01 qualifies, and we don't want duplicates

    def test_AlbuquerqueAZ(self):
        tl = ticketloader.TicketLoader("../testdata/AlbuquerqueAZ-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["AlbuquerqueAZ"])

        # same as Arizona, so I'm not testing everything
        self.assertEquals(t.ticket_number, "2003123001505")
        self.assertEquals(t.ticket_type, "UPDATE")
        self.assertAlmostEqual(t.work_lat,31.64769,5)
        self.assertAlmostEqual(t.work_long,-109.686943,5)

    def test_AlbuquerqueTX(self):
        tl = ticketloader.TicketLoader("../testdata/AlbuquerqueTX-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["AlbuquerqueTX"])

        # same as Arizona, so I'm not testing everything
        self.assertEquals(t.ticket_number, "040448342")
        self.assertEquals(t.ticket_type, "ROUTINE")
        self.assertEquals(t.call_date, "2004-02-13 16:21:00")
        self.assertEquals(t.transmit_date, t.call_date)
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'PUB')

        raw = raw.replace('On 13-FEB-04','On 13-XYZ-04')
        from parsers import ParserError
        self.failUnlessRaises(ParserError,self.handler.parse,raw, ["AlbuquerqueTX"])

        raw = tl.tickets[9]
        t = self.handler.parse(raw, ["AlbuquerqueTX"])

    def test_Wisconsin2a(self):
        tl = ticketloader.TicketLoader("../testdata/Wisconsin2a-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Wisconsin2a"])

        self.assertEquals(t.ticket_type, "STANDARD")
        self.assertEquals(t.kind, "NORMAL")
        self.assertEquals(t.ticket_number, "20041000448")

        # what if it was an emergency ticket?
        raw = raw.replace("STANDARD", "EMERGENCY")
        t = self.handler.parse(raw, ["Wisconsin2a"])
        self.assertEquals(t.ticket_type, "EMERGENCY")
        self.assertEquals(t.kind, "EMERGENCY")

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Wisconsin2a"])
        self.assertEquals(t.con_address, "12517 W GRAHAM ST")
        self.assertAlmostEqual(t.work_lat,43.008547,5)
        self.assertAlmostEqual(t.work_long,-88.0442585,5)

        raw = tl.tickets[3]
        t = self.handler.parse(raw, ["Wisconsin2a"])
        self.assertEquals(t.con_city, "GREENFIELD")

    def test_WashingtonState(self):
        tl = ticketloader.TicketLoader("../testdata/WashingtonState-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState"])

        self.assertEquals(t.ticket_number, "4020012")
        self.assertEquals(t.transmit_date, "2004-02-13 15:26:00")
        self.assertEquals(t.work_state, "MT")
        self.assertEquals(len(t.locates), 1) # don't parse member area
        self.assertEquals(t.locates[0].client_code, "11111111")
        #self.assertEquals(t.locates[1].client_code, "PROTEL01")
        self.assertAlmostEqual(t.work_lat,45.9789865,5)
        self.assertAlmostEqual(t.work_long,-108.163664,5)

    def test_WashingtonState_2(self):
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/LWA1-2007-11-05-00222.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState"])

        self.assertEquals(t.ticket_number, "7405270")
        self.assertEquals(t.kind,"NORMAL")

    def test_WashingtonState4(self):
        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LWA4/LWA4-2013-03-06-03-12-43-073-YA6SH.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState4"], exclude=['QLNWA16'])
        self.assertEquals(t.ticket_number, "13049101")

        # QLNWA16 should get translated to POTELWA02, and not excluded
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'POTELWA02')

        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LWA4/LWA4-2013-03-06-03-12-43-073-YA6SH.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState4"], exclude=['POTELWA02'])
        self.assertEquals(t.ticket_number, "13049101")

        # QLNWA16 should get translated to POTELWA02, then excluded. It will
        # probably never be configured like this in production.
        self.assertEquals(len(t.locates), 0)
        
        # Use a modified version of the same ticket, with a term that shouldn't
        # get translated (QLNWA99)
        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LWA4/LWA4-2013-03-06-03-12-43-073-YA6SH_modified.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState4"], exclude=['QLNWA16', 'POTELWA01'])
        self.assertEquals(t.ticket_number, "13049101")

        # QLNWA99 should not get translated or excluded
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'QLNWA99')

        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LWA4/LWA4-2013-03-06-03-12-43-073-YA6SH_modified.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WashingtonState4"], exclude=['QLNWA99'])
        self.assertEquals(t.ticket_number, "13049101")

        # QLNWA99 should not get translated, and should get excluded
        self.assertEquals(len(t.locates), 0)
        
    def test_Oregon6(self):
        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LOR5/LOR5-2013-03-06-09-33-25-611-TNJXG.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon6"], exclude=['QLNOR17'])
        self.assertEquals(t.ticket_number, "13041059")

        # QLNOR17 should get translated to POTELOR01, and not excluded
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'POTELOR01')

        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LOR5/LOR5-2013-03-06-09-33-25-611-TNJXG.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon6"], exclude=['POTELOR01'])
        self.assertEquals(t.ticket_number, "13041059")

        # QLNOR17 should get translated to POTELOR01, then excluded. It will
        # probably never be configured like this in production.
        self.assertEquals(len(t.locates), 0)
        
        # Use a modified version of the same ticket, with a term that shouldn't
        # get translated (QLNOR99)
        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LOR5/LOR5-2013-03-06-09-33-25-611-TNJXG_modified.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon6"], exclude=['QLNOR17', 'POTELOR01'])
        self.assertEquals(t.ticket_number, "13041059")

        # QLNOR99 should not get translated or excluded
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'QLNOR99')

        tl = ticketloader.TicketLoader(
         "../testdata/sample_tickets/LOR5/LOR5-2013-03-06-09-33-25-611-TNJXG_modified.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon6"], exclude=['QLNOR99'])
        self.assertEquals(t.ticket_number, "13041059")

        # QLNOR99 should not get translated, and should get excluded
        self.assertEquals(len(t.locates), 0)

    def test_QWEST_IDL(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-idl-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_IDL"])

        self.assertEquals(t.serial_number, "IDL2004100311")
        self.assertEquals(t.ticket_type, "2ND REQUEST")
        self.assertEquals(t.ticket_number, "2004100311")
        self.assertEquals(t.call_date, "2004-03-01 15:55:38")
        self.assertEquals(t.transmit_date, "2004-03-01 16:00:59")
        self.assertEquals(t.work_date, "2004-03-02 14:57:00")
        self.assertEquals(t.con_name, "IRMINGER CONSTRUCTION")
        self.assertEquals(t.caller_contact, "DAN WILLIAMS")
        self.assertEquals(t.caller_altcontact, "DAN WILLIAMS")
        self.assertEquals(t.caller_phone, "(208)939-1031")
        self.assertEquals(t.caller_altphone, "(208)869-3472")
        self.assertEquals(t.caller_fax, "(208)938-4698")
        self.assertEquals(t.work_state, "ID")
        self.assertEquals(t.con_state, "ID")
        self.assertEquals(t.work_county, "ADA")
        self.assertEquals(t.work_city, "MERIDIAN")
        self.assertEquals(t.work_address_number, "")
        self.assertEquals(t.work_address_street, "E VICTORY RD")
        self.assertEquals(t.work_cross, "S `BRANDY``S JEWEL` WAY")
        self.assert_(t.work_description.startswith("THIS IS A THE RIDENBAUGH"))
        self.assert_(t.work_description.endswith("MARKED IN WHITE"))
        self.assert_(t.work_remarks.startswith("2ND REQUEST DUE"))
        self.assert_(t.work_remarks.endswith("WEST OF RIDENBAUGH CANAL"))
        self.assertEquals(t.work_type, "INSTALL WATER LINE")
        self.assertEquals(t.company, "CITY OF MERIDIAN")
        self.assertEquals(len(t.locates), 6)

        self.assertEquals(t.locates[0].client_code, "QLNIDOCL1")
        self.assertEquals(t.locates[5].client_code, "IMGBOI01")

        # test map page
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["QWEST_IDL"])
        self.assertEquals(t.map_page, "03S06E14SW")

        # test serial number
        tl = ticketloader.TicketLoader("../testdata/qwest-idl-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_IDL"])

        self.assertEquals(t.serial_number, "IDL2004050300368")
        #self.assertEquals(t.map_page, "30N03E18SE") # take the second one
        self.assertEquals(t.map_page, "30N03E19NE")

        # test format change
        tl = ticketloader.TicketLoader("../testdata/qwest-idl-3.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_IDL"])

        self.assertEquals(t.work_date, "2004-12-28 10:08:00")

    def test_QWEST_IEUCC(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-ieucc-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_IEUCC"])

        self.assertEquals(t.serial_number, "IEUCC4069963")
        self.assertEquals(t.ticket_number, "4069963")
        self.assertEquals(t.work_state, "WA")
        self.assertEquals(t.con_state, "WA")
        self.assertEquals(len(t.locates), 1) # don't parse members area
        self.assertEquals(t.locates[0].client_code, "USWEST34")
        self.assertEquals(t.map_page, "25N44E35SE")
        self.assertEquals(t.work_address_number, "4216")
        self.assertEquals(t.work_address_street, "S SULLIVAN ROAD")
        self.assertEquals(t.transmit_date, "2004-03-22 07:41:00")
        self.assertEquals(t.con_name, "VERA WATER AND POWER")
        self.assertEquals(t.caller_contact, "TODD HENRY")
        self.assertEquals(t.company, "VERA WATER AND POWER")

    def test_QWEST_OOC(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-ooc-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_OOC"])

        self.assertEquals(t.serial_number, "OOC4048157")
        self.assertEquals(t.ticket_number, "4048157")
        self.assertEquals(t.work_state, "OR")
        self.assertEquals(t.con_state, "OR")
        self.assertEquals(len(t.locates), 1) # don't parse members area
        self.assertEquals(t.locates[0].client_code, "USW10")
        self.assertEquals(t.map_page, "10S3W16SE")
        self.assertEquals(t.work_address_number, "")
        self.assertEquals(t.work_address_street, "TOWERY WAY")
        self.assertEquals(t.transmit_date, "2004-03-24 08:27:00")
        self.assertEquals(t.con_name, "WADSWORTH ENTERPRISES")
        self.assertEquals(t.caller_contact, "DAMIAN FLOWERS")
        self.assertEquals(t.company, "REIM SCHNEIDER INVESTMENT")

    def test_QWEST_OOC_2(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-ooc-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_OOC"])

        # is this serial number recognized?
        self.assertEquals(t.serial_number, "OOC2005081100259")
        self.assertEquals(t.ticket_format, "LQW1")
        # will be adapted to LOR1 by main.py

    def test_QWEST_UULC(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-uulc-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_UULC"])

        self.assertEquals(t.serial_number, "UULC4078901")
        self.assertEquals(t.ticket_number, "4078901")
        self.assertEquals(t.work_state, "MT")
        self.assertEquals(t.con_state, "MT")
        self.assertEquals(len(t.locates), 1) # don't parse member area
        self.assertEquals(t.locates[0].client_code, "USWEST66")
        self.assertEquals(t.map_page, "1N25E26SW")
        self.assertEquals(t.work_address_number, "3305")
        self.assertEquals(t.work_address_street, "HARLOU")
        self.assertEquals(t.transmit_date, "2004-03-28 21:27:00")
        self.assertEquals(t.con_name, "MOUNTAIN SPRINGS")
        self.assertEquals(t.caller_contact, "DAVID DOSS")
        self.assertEquals(t.company, "JOE GERBASE")

    def test_QWEST_WOC(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-woc-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["QWEST_WOC"])

        self.assertEquals(t.serial_number, "WOC4078880")
        self.assertEquals(t.ticket_number, "4078880")
        self.assertEquals(t.work_state, "WA")
        self.assertEquals(t.con_state, "WA")
        self.assertEquals(len(t.locates), 1) # don't parse member area
        self.assertEquals(t.locates[0].client_code, "USW33")
        self.assertEquals(t.map_page, "13N19E30NWNE")
        self.assertEquals(t.work_address_number, "401")
        self.assertEquals(t.work_address_street, "E ARLINGTON STREET")
        self.assertEquals(t.transmit_date, "2004-03-28 17:18:00")
        self.assertEquals(t.con_name, "BULLPEN FENCE")
        self.assertEquals(t.caller_contact, "TODD MEDRY")
        self.assertEquals(t.company, "TRIANGLE AUTO SUPPLY")

    def test_QWEST_serial_number(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-all.txt")
        data = [
            (0, "QWEST_OOC", "OOC2004050300258"),
            (1, "QWEST_UULC", "UULC2004050300039"),
            (2, "QWEST_IEUCC", "IEUCC2004050300012"),
            (3, "QWEST_WOC", "WOC2004050300013"),
        ]
        for (index, format, serial) in data:
            raw = tl.tickets[index]
            t = self.handler.parse(raw, [format])
            self.assertEquals(t.serial_number, serial)

    def test_Oregon2(self):
        tl = ticketloader.TicketLoader("../testdata/Oregon2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Oregon2"])

        self.assertEquals(t.ticket_number, "4065047")
        self.assertEquals(t.work_state, "OR")
        self.assertEquals(t.con_state, "OR")
        self.assertEquals(len(t.locates), 1) # don't parse members area
        self.assertEquals(t.locates[0].client_code, "TEST04")
        self.assertEquals(t.map_page, "18S3W19NE")
        self.assertEquals(t.work_address_number, "310")
        self.assertEquals(t.work_address_street, "FOXTAIL DR")
        self.assertEquals(t.transmit_date, "2004-04-14 15:04:00")
        self.assertEquals(t.con_name, "CAMPBELL ELECTRIC")
        self.assertEquals(t.caller_contact, "GLEN CAMPBELL")
        self.assertEquals(t.company, "FOREST VILLAGE APT COMPLEX")
        self.assertAlmostEqual(t.work_lat,43.994487,5)
        self.assertAlmostEqual(t.work_long,-123.090609,5)

    def test_OregonFalcon(self):
        tl = ticketloader.TicketLoader("../testdata/OregonFalcon-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["OregonFalcon"])
        ae = self.assertEquals

        ae(t.ticket_number, "6093150")
        ae(t.ticket_type, "48 HOUR NOTICE DUPLICATION")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2006-05-10 16:11:00")
        ae(t.call_date, "2006-05-10 12:30:00")
        ae(t.work_date, "2006-05-12 12:45:00")
        ae(t.work_state, "OR")
        ae(t.work_county, "BAKER")
        ae(t.work_city, "AUBURN")
        ae(t.con_name, "OUNC")
        ae(t.company, "TEST")
        ae([x.client_code for x in t.locates], ['TEST03'])

    def test_GreenvilleNewNew(self):
        tl = ticketloader.TicketLoader("../testdata/fgv1-new.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Greenville", "GreenvilleNew"])

        self.assertEquals(t.ticket_number, "0004200007")
        self.assertEquals(t.transmit_date, "2004-04-21 13:44:35")
        self.assertEquals(t.call_date, "2004-04-20 13:33:45")
        self.assertEquals(t.work_date, "2004-04-20 13:45:41")
        self.assertEquals(t.ticket_type, "Resend")
        self.assertEquals(t.work_state, "SC")
        self.assertEquals(t.work_county, "ABBEVILLE")
        self.assertEquals(t.work_city, "ABBEVILLE")
        self.assertEquals(t.work_address_number, "123")
        self.assertEquals(t.work_address_street, "TEST ST")
        self.assertEquals(t.work_cross, "S TEST ST")
        self.assertEquals(t.work_type, "OTHER")


    def test_TX6001(self):
        tl = ticketloader.TicketLoader("../testdata/TX6001-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["TX6001"])

        self.assertEquals(t.ticket_type, 'ROUTINE')
        self.assertEquals(t.ticket_number, "041941930")
        self.assertEquals(t.transmit_date, "2004-07-12 11:11:10")
        self.assertEquals(t.call_date, "2004-07-12 11:07:00")
        #self.assertEquals(t.map_page, "325430097173A")
        self.assertEquals(t.map_page, "MAPSCO 21,V")
        self.assertEquals(t.work_date, "2004-07-14 11:15:00")
        self.assertEquals(t.work_county, "TARRANT")
        self.assertEquals(t.work_city, "FORT WORTH")
        self.assertEquals(t.work_address_number, "4136")
        self.assertEquals(t.work_address_street, "BOLEN")

        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, "FN3")
        self.assertEquals(t.locates[1].client_code, "FN3G")

    def test_TX6001_2(self):
        """
        Test Mapsco pages with a letter like MAPSCO 456A,U -> MAPSCO/456/U/A
        """
        tl = ticketloader.TicketLoader("../testdata/TX6001-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("MAPSCO:21,V","MAPSCO:456A,U")
        raw = raw.replace("MAPSCO 21,V","MAPSCO 456A,U")
        t = self.handler.parse(raw, ["TX6001"])

        self.assertEquals(t.map_page, "MAPSCO 456A,U")

    def test_TX6002(self):
        tl = ticketloader.TicketLoader("../testdata/TX6002-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['TX6002'])

        self.assertEquals(t.ticket_number, "2004290480")
        self.assertEquals(t.ticket_type, "STANDARD LOCATE")
        self.assertEquals(t.call_date, "2004-07-12 11:26:13")
        self.assertEquals(t.transmit_date, "2004-07-12 11:27:37")
        self.assertEquals(t.work_date, "2004-07-14 11:26:00")
        # this sample doesn't have a map_page

    def test_Pensacola(self):
        # test 'W' locates
        tl = ticketloader.TicketLoader("../testdata/Orlando-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('BYERS3', 'GCE560')
        t = self.handler.parse(raw, ['Pensacola'])
        # check if 'W' locate was added
        self.assertAlmostEqual(t.work_lat,28.3666666667,5)
        self.assertAlmostEqual(t.work_long,-81.4166666667,5)
        self.assertEquals(t.locates[0].client_code, 'GCE560')
        self.assertEquals(t.locates[1].client_code, 'GCE560W')

    def __test_OhioNew_stub(self):
        tl = ticketloader.TicketLoader("../testdata/OhioNew-stub.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["OhioNew"])

        self.assertEquals(t.transmit_date, "2004-08-05 02:15:00")
        self.assertEquals(t.ticket_type, "{NoticeType}")
        self.assertEquals(t.ticket_number, "1234-123-123")
        self.assertEquals(t.work_state, "12")
        self.assertEquals(t.work_county, "1234567890123456")
        self.assertEquals(t.work_city, "123456789012345678901234567890")
        self.assertEquals(t.work_address_number, "123456")
        self.assertEquals(t.work_address_number_2, "123456")
        self.assertEquals(t.work_address_street, "N 123456789012345678901234567890 123456789012 N")
        self.assertEquals(t.work_cross, "N 123456789012345678901234567890 123456789012 N")
        self.assertEquals(t.work_date, "2004-08-06 20:25:00")
        self.assertEquals(t.caller_contact, "12345678901234567890123456789012345678901234567890")
        self.assertEquals(t.con_name, "12345678901234567890123456789012345678901234567890")
        self.assertEquals(t.con_address, "1234567890123456789012345678901234567890123456789012345678901234567890")
        self.assertEquals(t.con_city, "1234567890123456789012")
        self.assertEquals(t.con_state, "12")
        self.assertEquals(t.con_zip, "1234567890")
        self.assertEquals(t.caller_phone, "123-123-1234")
        self.assertEquals(t.caller_fax, "123-123-1234")
        self.assertEquals(t.caller_email, "thisisareallylongstringwithoutanywhitesp")
        # is chopped off for some reason; schema?
        self.assertEquals(t.company, "1234567890123456789012345678901234567890")
        self.assertEquals(len(t.locates), 6)
        self.assertEquals(t.locates[0].client_code, "MBRCO1")

    def test_OhioNew_1(self):
        tl = ticketloader.TicketLoader("../testdata/OhioNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['OhioNew'])

        self.assertEquals(t.transmit_date, "2004-08-16 15:30:00")
        self.assertEquals(t.call_date, t.transmit_date)
        self.assertEquals(t.ticket_number, "0730-500-670") # -01 is revision no
        self.assertEquals(t.ticket_type, "On Job")
        self.assertEquals(t.work_state, "OH")
        self.assertEquals(t.work_county, "WASHINGTON")
        self.assertEquals(t.work_city, "MARIETTA")
        self.assertEquals(t.work_address_number, "")
        self.assertEquals(t.work_address_street, "WESTVIEW AV")
        self.assertEquals(t.work_date, "2004-08-03 14:15:00")
        self.assertEquals(t.work_type, "GAS LINE INSTALLATION")
        self.assertEquals(t.caller_contact, "JOHN MADY")
        self.assertEquals(t.con_name, "BILL HAWK INC")
        self.assertEquals(t.con_address, "2421 N WOOSTER AVE")
        self.assertEquals(t.con_city, "DOVER")
        self.assertEquals(t.con_state, "OH")
        self.assertEquals(t.con_zip, "44622")
        self.assertEquals(t.caller_phone, "330-602-2922")
        self.assertEquals(t.caller_fax, "330-343-3240")
        self.assertEquals(t.company, "DOMINION EAST OHIO")
        self.assertEquals(len(t.locates), 4)
        self.assertEquals(t.locates[0].client_code, "AGP")
        self.assertEquals(t.locates[1].client_code, "CMR")

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['OhioNew'])
        self.assertEquals(len(t.locates), 10)

        raw = tl.tickets[-1]
        t = self.handler.parse(raw, ['OhioNew'])
        self.assertEquals(t.ticket_type, 'Regular')
        self.assertEquals(t.work_address_number, '35855')
        self.assertEquals(t.work_address_street, 'S R 7')

        # cut out member section
        raw = tl.tickets[0]
        index1 = raw.find("Members:")
        index2 = raw.find("eom")
        raw = raw[:index1] + raw[index2:]
        t = self.handler.parse(raw, ['OhioNew'])
        self.assertEquals(len(t.locates), 1)
        self.assertEquals(t.locates[0].client_code, 'AGP')

        raw = tl.tickets[0]
        raw = raw.replace('Type:  On Job','Type:  SHORT NOTICE')
        t = self.handler.parse(raw, ['OhioNew'])
        self.assertEquals(t.kind, "EMERGENCY")

    def test_OhioNew_2(self):
        tl = ticketloader.TicketLoader("../testdata/OhioNew-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Lbps:     RAD   =RENO AREA WT/SW TCC   =CHARTER COMMUNI ',
                          '          RAD   =RENO AREA WT/SW TCC   =CHARTER COMMUNI ')
        t = self.handler.parse(raw, ['OhioNew'])

        self.assertEquals(len(t.locates), 6)
        self.assertEquals(t.locates[0].client_code, "AGP")
        self.assertEquals(t.locates[1].client_code, "CMR")
        self.assertEquals(t.locates[2].client_code, "EOK")
        self.assertEquals(t.locates[3].client_code, "OBT")
        self.assertEquals(t.locates[4].client_code, "RAD")
        self.assertEquals(t.locates[5].client_code, "TCC")

    def test_PA7501(self):
        tl = ticketloader.TicketLoader("../testdata/PA7501-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals

        # most tests have been moved to tests/tickets/PA7501.txt
        # however, not all tests here can be moved like this.

        # test BLAST field
        raw = raw.replace("GOLF", "BLAST")
        t = self.handler.parse(raw, ["PA7501"])
        ae(t.explosives, "Y")
        ae(t.ward, None)

        # test WARD field
        tl = ticketloader.TicketLoader("../testdata/PA7501-ward.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae(t.ticket_number, "3285921")
        ae(t.ward, "40")

        # store that ticket, and check if the ward field is really there
        id = self.tdb.insertticket(t)
        rows = self.tdb.getrecords("ticket", ticket_id=id)
        ae(rows[0]['ward'], '40')

        # test call date after format change
        tl = ticketloader.TicketLoader("../testdata/PA7501-calldate.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae(t.call_date, "2007-07-19 11:35:00")

    def test_PA7501_new(self):
        tl = ticketloader.TicketLoader("../testdata/PA7501-new.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals

        # some tests moved to tests/tickets/PA7501.txt

        t = self.handler.parse(tl.tickets[3], ['PA7501'])

        ae(t.ticket_number, "0665951")
        ae(t.ticket_type, "NEW XCAV DSGN RSND FTTP") # FTTP is added by parser
        ae(t.work_county, "DELAWARE")
        ae(t.work_city, "MIDDLETOWN TWP")
        ae(t.work_address_street, "") # FIXME
        ae(t.work_cross, "") # FIXME
        ae(t.transmit_date, "2007-03-20 11:49:09")
        ae(t.call_date, "2007-03-07 13:30:00")
        ae(t.work_date, None) # FIXME
        ae(t.work_state, "PA")
        ae(t.work_type, "CONDUIT VERIZON FTTP")
        ae(t.company, "VERIZON FTTP")
        ae(t.con_name, "UTILITY CONSULTANTS INC")
        ae(t.con_address, "1418 BOSTON-PROVIDENCE HWY")
        ae(t.con_city, "NORWOOD")
        ae(t.con_state, "MA")
        ae(t.con_zip, "02062")
        ae(t.caller, "PAUL GEORGE")
        ae(t.caller_phone, "781-769-0493")
        ae(t.caller_fax, "781-769-7120")
        ae(t.caller_contact, "PAUL GEORGE")
        ae([x.client_code for x in t.locates],
          ['ACW', 'CR', 'HT', 'IB', 'JY', 'KE', 'MD1', 'SP', 'TX', 'YI', 'KEG'])
        ae(t.explosives, "N")
        ae(t.ward, None)

        t = self.handler.parse(tl.tickets[7], ["PA7501"])
        ae(t.ticket_number, "1567521")
        ae(t.work_date, "2007-06-08 07:00:00")
        ae(t.ticket_type, "UPDT XCAV RTN")
        ae(t.work_county, "CHESTER")
        ae(t.work_city, "NEW LONDON TWP")
        ae(t.work_cross, "THUNDER HILL")
        ae(t.work_type, "WILKINSON") # ?!
        ae(t.con_name, "TECHNIVATE INC")
        ae(t.call_date, "2007-06-05 16:09:00")
        ae(t.transmit_date, "2007-06-05 16:09:46")
        ae([x.client_code for x in t.locates],
           ['ACW', 'CR', 'JZ', 'KF', 'YJ', 'KFG'])

    def test_PA7501_new_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "7501", "TEST-2008-11-17-00001.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals
        ae(len(t.work_remarks), 1012)
        ae(t.company, "JEFFERSON HILLS BORO LOREM IPSUM DOLOR SIT AMET CONSECETU")

        ae([x.client_code for x in t.locates],
           ['AG4', 'BD', 'DC', 'DS', 'EBS', 'EQT', 'GIC', 'JEF', 'JUL', 'QQQ',
            'WE', 'WK', 'XJ', 'XXX', 'YYY'])

    def test_PA7501_new_3(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "7501", "TEST-2008-11-17-00002.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals
        ae(len(t.work_remarks),1012)
        ae(t.respond_date, '2008-11-19 00:00:00')
        ae(t.work_date, '2008-11-20 08:00:00')
        ae(t.company, "JEFFERSON HILLS BORO LOREM IPSUM DOLOR SIT AMET CONSECETU")
        ae([x.client_code for x in t.locates],
           ['BD', 'DC', 'DS', 'EBS', 'EQT', 'GIC', 'JEF', 'JUL', 'QQQ', 'WE',
            'WK', 'XJ', 'XXX', 'YYY'])

    def test_PA7501_work_description(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "7501-2010-01-20", "7501-2010-01-20-00447.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals
        ae(t.ticket_number, "20100201066")
        ae(t.work_description, "RT 263 - TOTAL ROADWAY FROM OLD YORK ROAD" +
           " IN WARMINSTER TWP THRU WARWICK TWP TO APPROX 1000 FT INTO" +
           " BUCKINGHAM TWP")

    def test_FL9001(self):
        tl = ticketloader.TicketLoader("../testdata/FL9001-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["FL9001"])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-01-21 11:24:30")
        ae(t.ticket_type, 'STREET')
        ae(t.ticket_number, "02152648")
        ae(t.call_date, "2005-01-21 11:24:00")
        ae(t.due_date, "2005-01-25 23:59:00")
        ae(t.work_state, "FL")
        ae(t.work_county, "COLLIER")
        ae(t.work_city, "NAPLES")
        ae(t.work_address_number, "490")
        ae(t.work_address_street, "CENTRAL AVE")
        ae(t.work_cross, "5TH ST S")
        ae(t.map_page, "2608A8148D")
        ae(t.work_date, "2005-01-25 23:59:00")
        ae(t.work_type, "HOUSE REMOVAL")
        ae(t.company, "CHRISTOPHER STRUNK")
        ae(t.con_name, "EMERALD LANDSCAPING")
        ae(t.caller_fax, "239-793-4460")
        ae(len(t.locates), 7)
        ae(t.locates[0].client_code, "CON762")
        self.assertAlmostEqual(t.work_lat,26.1479166667,5)
        self.assertAlmostEqual(t.work_long,-81.8020833333,5)

        # for emergencies, we don't take the due date from the ticket
        raw = raw.replace('STREET', 'EMERGENCY')
        t = self.handler.parse(raw, ['FL9001'])
        ae(t.ticket_type, 'EMERGENCY')
        ae(t.kind, 'EMERGENCY')
        self.assertEquals(t.due_date, None)

    def test_NC9101(self):
        tl = ticketloader.TicketLoader("../testdata/NC9101-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NC9101"])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-01-28 09:49:55")
        ae(t.ticket_type, "NORM NEW STRT")
        ae(t.ticket_number, "C0027445")
        ae(t.call_date, "2005-01-28 09:42:00")
        ae(t.work_state, "NC")
        ae(t.work_county, "DARE")
        ae(t.work_city, "NAGS HEAD")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "W WOODHILL DR")
        ae(t.work_cross, "S CROATAN HWY")
        ae(t.map_page, "3558B7538A")
        ae(t.work_type, "INSTALL SEPTIC SYSTEM")
        ae(t.work_date, "2005-02-01 09:42:00")
        ae(t.company, "NORTHBANK PROPERTIES")
        ae(t.con_name, "ROBERTSON REPAIR")
        ae(t.con_address, "PO BOX 1783")
        ae(t.con_city, "MANTIO")
        ae(t.con_state, "NC")
        ae(t.con_zip, "27954")
        self.assertAlmostEqual(t.work_lat,35.9754166667,5)
        self.assertAlmostEqual(t.work_long,-75.6445833333,5)
        ae(len(t.locates), 6)
        ae(t.locates[0].client_code, "CSR01")
        ae(t.locates[1].client_code, "CTT13")

    def test_VA9301(self):
        tl = ticketloader.TicketLoader("../testdata/VA9301-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["VA9301"])
        ae = self.assertEquals
        self.assertAlmostEquals(t.work_lat,36.66875,5)
        self.assertAlmostEquals(t.work_long,-79.9245833333,5)
        ae(t.transmit_date, "2005-01-28 19:10:00")
        ae(t.ticket_number, "A502802391")
        ae(t.ticket_type, "NEW GRID NORM LREQ")
        ae(t.call_date, "2005-01-28 19:08:00")
        ae(t.work_date, "2005-02-16 00:00:00")
        ae(t.work_county, "HENRY")
        ae(t.work_city, "")
        ae(t.work_state, "VA")
        ae(t.work_address_number, "87")
        ae(t.work_address_street, "CEDARWOOD CT")
        ae(t.work_type, "INSTALLING UNDERGROUND ELECTRIC SERVICE")
        ae(t.company, "AEP")
        ae(t.con_name, "C W CAULEY AND SONS INC")
        ae(t.con_address, "PO BOX 123")
        ae(t.con_city, "PATRICK SPRINGS")
        ae(t.con_state, "VA")
        ae(t.con_zip, "24133")
        ae(t.caller_phone, "276-694-3814")
        ae(t.caller_fax, "276-694-2861")
        ae(t.map_page, "3640D7955B-33")
        ae(len(t.locates), 5)
        ae([x.client_code for x in t.locates],
           ["ACH144", "AEP111", "FAXCFM", "HCA073", "STD056"])

    def test_TN9401(self):
        tl = ticketloader.TicketLoader("../testdata/TN9401-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["TN9401"])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-01-31 08:07:00")
        ae(t.ticket_type, "NORMAL")
        ae(t.ticket_number, "050310204")
        ae(t.call_date, "2005-01-31 08:07:00")
        ae(t.con_name, "CITY OF KINGSPORT")
        ae(t.caller_contact, "CHARLES DEVAULT")
        ae(t.con_address, "1213 KONNAROCK RD")
        ae(t.con_city, "KINGSPORT")
        ae(t.con_state, "TN")
        ae(t.con_zip, "37664")
        ae(t.work_date, "2005-02-03 08:15:00")
        ae(t.work_state, "TN")
        ae(t.work_county, "SULLIVAN")
        ae(t.work_city, "KINGSPORT")
        ae(t.work_address_number, "534")
        ae(t.work_address_street, "FAIRVIEW AVE")
        ae(t.work_cross, "RIVERSIDE AVE")
        self.assertAlmostEqual(t.work_lat,36.5548029491,5)
        self.assertAlmostEqual(t.work_long,-82.5758737463,5)
        ae(t.company, "CITY OF KINGSPORT")
        ae(len(t.locates), 5)
        ae([x.client_code for x in t.locates],
           ["COK", "KP002", "U11", "UTAC", "WCC"])

    def test_consolidated(self):
        tl = ticketloader.TicketLoader("../testdata/fcl1-consolidated.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NorthCarolina'])
        self.assertEquals(t.ticket_number, "A0362816")
        self.assertEquals(t.ticket_type, "NORM RXMT GRID")
        self.assertAlmostEqual(t.work_lat,35.1708333333,5)
        self.assertAlmostEqual(t.work_long,-81.46875,5)

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['NorthCarolina'])
        self.assertEquals(t.ticket_number, "A0362816")
        self.assertEquals(t.ticket_type, "NORM NEW GRID")

        tl = ticketloader.TicketLoader("../testdata/fcl1-consolidated-2.txt")
        self.assertEquals(len(tl.tickets), 3)
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['NorthCarolina'])
        self.assertEquals(t.ticket_number, "A0062728")
        self.assertEquals(t.ticket_type, "NORM NEW PLCE")

    def test_Atlanta_noworkstate(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-noworkstate.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

    def test_GreenvilleNC(self):
        # assume format is exactly the same as NorthCarolina
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['GreenvilleNC'])

    def test_ColoradoNew(self):
        tl = ticketloader.TicketLoader("../testdata/ColoradoNew-1.txt")
        # ColoradoNew doesn't really need a new parser
        raw = tl.tickets[4]
        t = self.handler.parse(raw, ["Colorado"])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-01-27 12:04:00")
        ae(t.ticket_type, "EMER UPDT STRT DAMG RSND")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "A0710681")
        ae(t.call_date, "2004-12-03 11:08:00")
        ae(t.work_state, "CO")
        ae(t.work_county, "ADAMS")
        ae(t.work_city, "THORNTON")
        ae(t.work_address_number, "0")
        ae(t.work_address_street, "LAFAYETTE ST")
        ae(t.map_page, "01S068W26S*")
        ae(t.work_type, "TRAFFIC SIGNAL")
        ae(t.con_name, "COLORADO SIGNAL")
        ae(t.company, "CITY OF THORNTON")
        ae(t.caller, "JOYCE ASKEW")
        ae(t.caller_phone, "(303)289-3361")
        ae(t.caller_fax, "(303)288-5669")
        ae([x.client_code for x in t.locates], ["TEST4"])

    def test_NM1101(self):
        tl = ticketloader.TicketLoader("../testdata/NM1101-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NM1101'])

        ae = self.assertEquals
        ae(t.ticket_number, "2005080246")
        ae(t.ticket_type, "STANDARD LOCATE")
        ae(t.transmit_date, "2005-02-14 10:10:06")
        ae(t.call_date, "2005-02-14 10:07:12")
        ae(t.con_name, "SANTA FE COUNTY PUBLIC WORKS")
        ae(t.caller_contact, "ART TRUJILLO")
        ae(t.caller_phone, "(505)490-2134")
        ae(t.caller_fax, "(505)832-9195")
        ae(t.work_state, "NM")
        ae(t.work_county, "SANTA FE")
        ae(t.work_city, "RURAL SANTA FE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "MAPLE RD")
        ae(t.work_cross, "")
        ae(t.map_page, "10N07E23SW")
        ae(t.work_type, "ROAD GRADING")
        ae([x.client_code for x in t.locates],
           ['EMTN', 'EMW', 'USWN', 'NMAW', 'CNME'])

    def test_AZ1102(self):
        tl = ticketloader.TicketLoader("../testdata/AZ1102-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['AZ1102'])
        ae = self.assertEquals

        ae(t.transmit_date, "2005-02-22 14:58:00")
        ae(t.ticket_number, "2005021700058")
        ae(t.ticket_type, "DAMAGED UTILITY")
        ae(t.due_date, "2005-02-22 17:00:00")
        ae(t.call_date, t.transmit_date)
        ae(t.work_state, "AZ")
        ae(t.work_address_number, "413")
        ae(t.work_address_street, "S 2ND AVE")
        ae(t.map_page, "7S26E08-SW")
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "CPVLTE12")
        self.assertAlmostEqual(t.work_lat,32.834753,5)
        self.assertAlmostEqual(t.work_long,-109.7089235,5)

        raw = raw.replace('Send To: CPVLTE12','Send To:')
        t = self.handler.parse(raw, ['AZ1102'])
        ae(len(t.locates), 0)

    def test_Alaska(self):
        tl = ticketloader.TicketLoader("../testdata/Alaska-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Alaska'])

        ae = self.assertEquals
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_number, "2005120076")
        ae(t.call_date, "2005-03-15 12:53:47")
        ae(t.transmit_date, "2005-03-15 12:56:48")
        ae(t.work_date, "2005-03-15 12:55:00")
        ae(t.con_name, "ALASKA DIGLINE, INC.")
        ae(t.company, t.con_name)
        ae(t.caller_contact, "LOUISE FROST")
        ae(t.caller_phone, "(907)279-1122")
        ae(t.caller_fax, "(907)278-0696")
        ae(t.work_city, "EAGLE RIVER")
        ae(t.work_address_number, "9876")
        ae(t.work_address_street, "EAGLE RIVER STREET")
        ae(t.work_cross, "1 INTERSECTING STREET")
        ae(t.map_page, "NW0052")
        ae(t.work_type, "SEE DIG INFO")
        self.assert_(t.work_description.startswith("TEST TICKET 2"))
        self.assert_(t.work_description.endswith("DIG INFO FIELD 5"))
        self.assert_(t.work_remarks.startswith("REMARKS FIELD 1"))
        self.assert_(t.work_remarks.endswith("REMARKS FIELD 3"))
        ae([x.client_code for x in t.locates],
           ['LOC_INC_WA', 'MEA ER', 'GCI ER', 'GCI A/E'])

        # invalid characters
        raw = raw.replace('LOC_INC_WA','LOC_I.|_WA')
        self.failUnlessRaises(ticketparser.TicketError,self.handler.parse,raw, ['Alaska'])

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['Alaska'])
        ae(t.work_address_number, "")

        raw = tl.tickets[4]
        t = self.handler.parse(raw, ["Alaska"])
        ae(t.map_page, "NE3913")

        tl = ticketloader.TicketLoader("../testdata/Alaska-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Alaska"])
        ae(len(t.locates), 10)

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Alaska"])
        ae(len(t.locates), 8)
        ae([x.client_code for x in t.locates],
           ["MEA/GCI", "AWWU", "ENS A/E", "PW SD/ER", "MTA",
            "ER/MV", "MEA ER", "GCI ER"])

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ["Alaska"])
        ae([x.client_code for x in t.locates],
           ['MEA/GCI', 'ENS MV', 'MTA ER/MV', 'GCI MV', 'MEA MV',
            'ARRC', 'GCI-FIBER'])

    def test_Delaware3(self):
        tl = ticketloader.TicketLoader("../testdata/Delaware3-1.txt")
        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['Delaware3'])
        ae = self.assertEquals

        ae(t.ticket_number, "51890187")
        ae(t.ticket_type, "FTTP")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2005-07-08 10:35:00")
        ae(t.call_date, "2005-07-08 10:34:00")
        ae(t.work_date, "2005-07-13 07:00:00")
        ae(t.work_city, "BARBS FARM RD")
        ae(t.work_address_number, "343")
        ae(t.work_address_street, "WHEATSHEAF DR.")
        ae(t.work_type, "DIRECTIONAL BORE")
        ae(t.work_state, "DE")
        ae(t.con_state, "DE")
        ae(t.work_county, "NEW CASTLE")
        ae(len(t.locates), 9)

    def test_TX1103(self):
        tl = ticketloader.TicketLoader("../testdata/TX1103-1.txt")
        self.assertEquals(len(tl.tickets), 5)
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['TX1103'])
        ae = self.assertEquals

        ae(t.ticket_number, "052100991")
        ae(t.ticket_type, "ROUTINE")
        ae(t.kind, "NORMAL")
        ae(t.call_date, "2005-07-29 09:40:00")
        ae(t.transmit_date, "2005-07-29 10:14:06")
        ae(t.work_date, "2005-08-02 09:45:00")
        ae(t.work_county, "EL PASO")
        ae(t.work_state, "TX")
        ae(t.work_city, "EL PASO")
        ae(t.work_address_number, "3333")
        ae(t.work_address_street, "SUN BOWL DR")
        ae(t.work_type, "INSTL ELECT LINE")
        ae(t.con_name, "AC ELECTRIC")
        ae(t.company, "HIGH DESERT COMMUNICATIONS")
        ae([x.client_code for x in t.locates], ['VTG'])

    def test_Utah(self):
        """
        test Utah format
        Modified based on format change 2008/04/03
        (Mantis 2027: tickets are not parsing for LUT1)
        """
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LUT1","LUT1-2008-04-03-00033.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Utah'])
        ae = self.assertEquals

        ae(t.ticket_number, "C80940321")
        ae(t.transmit_date, '2008-04-03 11:28:20')
        ae(t.call_date, '2008-04-03 11:27:00')
        ae(t.work_date, '2008-04-15 11:27:00')
        ae(t.work_state, "UT")
        ae(t.work_county, "WASHINGTON")
        ae(t.work_city, 'HURRICANE')
        ae(t.work_address_number, "")
        ae(t.work_address_street, 'W 100 S')
        ae(t.work_cross, 'S 1150 W')
        ae(t.map_page, '3710B11318A')
        ae(t.work_type, 'INSTL GAS MAIN')
        ae(t.con_name, 'NIELS FUGAL SONS COMPANY')
        ae(t.con_address, '16 W WASHINGTON  CITY INDUSTRIAL DR')
        ae(t.con_city, 'WASHINGTON')
        ae(t.con_state, "UT")
        ae(t.con_zip, '84780')
        ae(t.caller, 'GAYLE HANSEN')
        ae(t.caller_phone, '435-634-1817')
        ae(t.caller_fax, '435-634-9677')
        self.assertAlmostEqual(t.work_lat,37.175,3)
        self.assertAlmostEqual(t.work_long,-11.5166666667,5)
        ae([x.client_code for x in t.locates],
           "ASHCRK BAJA HURCNL HURCTY OCLQS5 QLNUT3 RMPCED UDOT4C".split())

    def test_Nevada(self):
        tl = ticketloader.TicketLoader("../testdata/Nevada-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Nevada'])
        ae = self.assertEquals

        ae(t.transmit_date, "2006-01-16 09:03:29")
        ae(t.ticket_type, "NORMAL NOTICE RENEWAL")
        ae(t.kind, "NORMAL")
        ae(t.ticket_number, "0014832")
        ae(t.call_date, "2006-01-13 11:11:00")
        ae(t.work_date, "2006-01-18 11:30:00")
        ae(t.caller, "CHARLIE PROA")
        ae(t.con_name, "FREHNER CONSTRUCTION")
        ae(t.con_address, "4040 FREHNER RD, NORTH LAS VEGAS")
        ae(t.con_city, "NORTH LAS VEGAS")
        ae(t.con_state, "NV")
        ae(t.con_zip, "89030")
        ae(t.caller_phone, "702-346-1537")
        ae(t.caller_fax, "702-649-8834")
        ae(t.work_type, "EXC FOR ST OVERLAY")
        ae(t.company, "CITY OF MESQUITE")
        ae(t.work_city, "MESQUITE")
        ae(t.work_county, "CLARK")
        ae(t.work_state, "NV")
        self.assertAlmostEqual(t.work_lat,36.803915,5)
        self.assertAlmostEqual(t.work_long,-114.07296,5)
        ae([x.client_code for x in t.locates],
           ['ATTNEV', 'CHACOM', 'MOAPA', 'OVRPWR', 'RVITEL', 'VIRWTR'])

        raw = raw.replace('Priority: 2','Priority: 1')
        raw = raw.replace('0014832 NORMAL','0014832 EMER')
        t = self.handler.parse(raw, ['Nevada'])
        ae(t.kind, "EMERGENCY")
        ae(t.ticket_type, 'EMER NOTICE RENEWAL-EMER')

        raw = tl.tickets[0]
        lines = raw.split("\n")
        for i,line in enumerate(lines):
            if line.strip().startswith("Location:"):
                del lines[i:i+3]
                break
        raw = string.join(lines,'\n')
        t = self.handler.parse(raw, ['Nevada'])
        ae(t.work_description,'')

        raw = tl.tickets[0]
        raw = raw.replace('Long/Lat Long: -114.081787 Lat:  36.802204 Long: -114.064133 Lat:  36.805626','')
        t = self.handler.parse(raw, ['Nevada'])
        ae(t.work_long,0.0)
        ae(t.work_lat,0.0)

        raw = tl.tickets[0]
        raw = raw.replace('Long/Lat Long: -114.081787 Lat:  36.802204 Long: -114.064133 Lat:  36.805626',
                          'Long/Lat Long: abc Lat:  def Long: ghi Lat:  jkl')
        t = self.handler.parse(raw, ['Nevada'])
        ae(t.work_long,0.0)
        ae(t.work_lat,0.0)



    def test_Nevada_work_address(self):
        tl = ticketloader.TicketLoader("../testdata/Nevada-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Nevada'])
        ae = self.assertEquals

        ae(t.ticket_number, "0166872")
        ae(t.con_address, "POB 547")
        ae(t.con_city, "LOGANDALE")
        ae(t.work_address_street, "LISTON AVE")
        ae(t.work_address_number, "3065")
        ae(t.work_cross, "SAINT JOSEPH ST")
        self.assertAlmostEqual(t.work_lat,36.597355,5)
        self.assertAlmostEqual(t.work_long,-114.4612845,5)

    def test_MississippiTN(self):
        tl = ticketloader.TicketLoader("../testdata/MississippiTN-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['MississippiTN'])

        self.assertEquals(t.ticket_number, "060610285")
        self.assertEquals(t.ticket_type, "NORMAL")
        self.assertEquals(t.work_date, "2006-03-07 08:15:00")
        self.assertAlmostEqual(t.work_lat,35.0291262,5)
        self.assertAlmostEqual(t.work_long,-88.5567397023,5)
        self.assertEquals([x.client_code for x in t.locates],
                          ['B05', 'B10', 'SUD'])

    def __test_QWEST_IRTH(self):
        tl = ticketloader.TicketLoader("../testdata/lqw1-new.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['QWEST_IRTH'])

    def test_7501_emergency(self):
        tl = ticketloader.TicketLoader("../testdata/7501-emer-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])

        self.assertEquals(t.ticket_type, "NEW XCAV EMER")
        self.assertEquals(t.kind, "EMERGENCY") # needs fixed

    def test_Washington_format_change_20050831(self):
        # affects FMW1, FDE1, FMB1
        tl = ticketloader.TicketLoader("../testdata/Washington-etc-new.txt")

        map_pages = [
            "NEWC 16 K10",
            "NEWC 16 K10",
            "AA 50 A1",
        ]

        for idx, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, ['Delaware1'])
            self.assertEquals(t.map_page, map_pages[idx])

    def test_Washington_format_change_20071110(self):
        # affects FMW1, FDE1, FMB1
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/FMB1-2007-10-29-02391.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        self.assertEquals(t.map_page, "AA 1234 A1")
        self.assertEquals(t.call_date,'2007-10-29 14:41:00')
        self.assertEquals(t.transmit_date,'2007-10-29 14:52:00')
        self.assertEquals(t.work_date,'2007-10-31 14:45:00')

        tl = ticketloader.TicketLoader("../testdata/sample_tickets/TEST-2007-11-07-00001.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        self.assertEquals(t.map_page, "AA 55 A1")
        self.assertEquals(t.kind,"EMERGENCY")
        self.assertEquals(t.call_date,'2007-11-07 13:48:00')
        self.assertEquals(t.transmit_date,'2007-11-07 14:37:00')
        self.assertEquals(t.work_date,'2007-11-07 14:00:00')

        tl = ticketloader.TicketLoader("../testdata/sample_tickets/TEST-2007-11-07-00002.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        self.assertEquals(t.map_page, "AA 55 A1")
        self.assertEquals(t.kind,"EMERGENCY")
        self.assertEquals(t.call_date,'2007-11-07 15:24:00')
        self.assertEquals(t.transmit_date,'2007-11-07 15:30:00')
        self.assertEquals(t.work_date,'2007-11-07 15:30:00')

    def test_LOR1_serial(self):
        tl = ticketloader.TicketLoader("../testdata/lor1-serial.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Oregon2'])
        self.assertEquals(t.serial_number, None)
        self.assertAlmostEqual(t.work_lat,42.4327105,5)
        self.assertAlmostEqual(t.work_long,-123.049812,5)
        id = self.tdb.insertticket(t)

        t1 = self.tdb.getticket(id)
        self.assertEquals(t1.serial_number, None)

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['QWEST_OOC'])
        t.ticket_format = "LOR1" # adapt
        self.assertEquals(t.serial_number, "OOC2005030900065")
        self.tdb.updateticket(id, t)

        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.serial_number, "OOC2005030900065")

    def test_LocInc_cancellation(self):
        tl = ticketloader.TicketLoader("../testdata/locinc-574119.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Oregon2'])
        self.assertEquals(t.ticket_type, "48 HOUR NOTICE CANCELLATION")
        self.assertEquals(t.kind, "NORMAL") # was: emergency

    def test_FTTP(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") < 0)

        r = raw.replace("NEW GAS RENEWAL REPLACING GAS",
                        "NEW GAS RENEWEL REPLACING FTTP")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") >= 0)

        r = raw.replace("Done for : AGL",
                        "Done for : AGL FTTP BLAH")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") >= 0)

        r = raw.replace("RESTAKE OF 11141-300-020",
                        "RESTAKE OF FTTP AND HISH")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") >= 0)
        self.assertEquals(t.ticket_type, "NORMAL RESTAKE FTTP")

        # make sure we don't get multiple FTTPs in the ticket type
        r = raw.replace("NORMAL RESTAKE", "NORMAL FTTP RESTAKE")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") >= 0)
        self.assertEquals(t.ticket_type, "NORMAL FTTP RESTAKE")

        # add FIOS
        r = raw.replace("GENERAL PIPELINE COMPANY", "GENERAL FTTP FIOS COMPANY")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") < 0)
        self.assertEquals(t.ticket_type, "NORMAL RESTAKE")

        r = raw.replace("NORMAL RESTAKE", "NORMAL RESTAKE FTTP")
        r = r.replace("GENERAL PIPELINE COMPANY", "GENERAL FIOS COMPANY")
        t = self.handler.parse(r, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") < 0)
        self.assertEquals(t.ticket_type, "NORMAL RESTAKE FIOS")

        t = self.handler.parse(raw, ["Atlanta"])
        self.assert_(t.ticket_type.upper().find("FTTP") < 0)

        tl = ticketloader.TicketLoader("../testdata/Dallas1-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Dallas1"])
        self.assert_(t.ticket_type.upper().find("FTTP") < 0)

        raw = raw.replace("CITY OF DALLAS", "VERIZON OSP")
        t = self.handler.parse(raw, ["Dallas1"])
        self.assert_(t.ticket_type.upper().find("FTTP") >= 0)

    def test_Idaho(self):
        # Idaho is no longer active, but testing this format is still useful
        # because other formats (like IdahoNew) derive from it.  So we pretend
        # that LID1 still uses it:
        call_centers.cc2format['LID1'].append('Idaho')
        call_centers.format2cc['Idaho'] = 'LID1'

        tl = ticketloader.TicketLoader("../testdata/Idaho-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Idaho'])
        ae = self.assertEquals

        ae(t.ticket_number, '2006050101')
        ae(t.ticket_type, 'STANDARD LOCATE')
        ae(t.kind, 'NORMAL')
        ae(t.call_date, "2006-01-25 05:52:13")
        ae(t.transmit_date, "2006-01-25 05:55:52")
        ae(t.work_date, "2006-01-27 05:52:00")
        ae(t.con_name, "AVISTA UTILITIES")
        ae(t.caller, "VICKI VINSON")
        ae(t.work_state, "ID")
        ae(t.work_county, "KOOTENAI")
        ae(t.work_city, "RATHDRUM")
        ae(t.work_address_number, "19322")
        ae(t.work_address_street, "N LONE PINE LN")
        ae(t.work_cross, "OHIO MATCH")
        ae(t.work_type, "SEE ADDITIONAL INFO")
        ae([x.client_code for x in t.locates],
           ['LOCATE2', 'AVISTA', "BARCSW", 'VERIZON', 'OHMWTR', 'ADELPHIA',
            'KEC'])

        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['Idaho'])
        self.assertAlmostEqual(t.work_lat, 47.55061000,5)
        self.assertAlmostEqual(t.work_long, -116.32284000,5)

    def test_Greenville3(self):
        tl = ticketloader.TicketLoader("../testdata/Greenville3-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Greenville3'])
        ae = self.assertEquals

        ae(t.ticket_number, "0604120024")
        ae(t.transmit_date, "2006-04-12 07:05:40")
        ae(t.call_date, "2006-04-12 07:05:37")
        ae(t.work_date, "2006-04-12 07:15:21")
        ae(t.ticket_type, "Resend")
        ae(t.work_state, "SC")
        ae(t.work_county, "GREENVILLE")
        ae(t.work_city, "GREENVILLE")
        ae(t.work_address_number, "185")
        ae(t.work_address_street, "WOODRUFF RD")
        ae(t.work_cross, "OLD WOODRUFF RD")
        ae(t.work_type, "SEWER, REPLACE MAIN")
        self.assert_(t.work_description.startswith("NEED TO MARK"))
        self.assert_(t.work_description.endswith("ON SITE //"))
        ae(t.con_address, "360 SOUTH HUDSON STREET")
        ae(t.con_city, "GREENVILLE")
        ae(t.con_state, "SC")
        ae(t.con_zip, "29601")
        ae(t.map_page,'3449A8220A')
        self.assertAlmostEqual(t.work_lat,34.8311065787,5)
        self.assertAlmostEqual(t.work_long,-82.3470882939,5)
        ae([x.client_code for x in t.locates],
           ['BSZT29', 'CCMZ41', 'CGR12', 'DPCZ02', 'PNGZ81'])

        raw = raw.replace('Lat/Long: 34.8311065786813, -82.347088293917',
                          'Lat/Long: abc, def')
        raw = raw.replace('Members Involved: BSZT29 CCMZ41 CGR12 DPCZ02 PNGZ81',
                          '')
        t = self.handler.parse(raw, ['Greenville3'])
        ae(len(t.locates),0)
        ae(t.map_page,'')

    def test_SouthCaliforniaCOX(self):
        tl = ticketloader.TicketLoader("../testdata/sca1-cox.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['SouthCaliforniaCOX'])

        # same as SouthCalifornia, except for locate handling
        self.assertEquals(t.ticket_number, "A0270427")
        self.assertEquals(t.call_date, "2006-02-02 07:27:00")
        self.assertAlmostEqual(t.work_lat,32.5703145,5)
        self.assertAlmostEqual(t.work_long,-117.058056,5)
        self.assertEquals([x.client_code for x in t.locates], ['COX01'])

        # Mantis #2917: more COX tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "SCA5-2011-12-20-cox", "sca1-2011-12-20-00095.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['SouthCaliforniaCOX'])
        self.assertEquals(t.ticket_number, "A13430648")
        self.assertEquals([x.client_code for x in t.locates], ["COXRSM"])
        self.assertEquals(t.ticket_type, "NORM RXMT GRID")

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "SCA5-2011-12-20-cox", "sca5-2011-12-15-00127.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['SouthCaliforniaCOX'])
        self.assertEquals(t.ticket_number, "A13490766")
        self.assertEquals([x.client_code for x in t.locates], ["COX01"])
        self.assertEquals(t.ticket_type, "NORM NEW GRID")

    def test_IdahoNew(self):
        tl = ticketloader.TicketLoader("../testdata/IdahoNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['IdahoNew'])
        ae = self.assertEquals

        ae(t.ticket_number, "2004051120")
        ae(t.ticket_type, "STANDARD LOCATE")
        ae(t.call_date, "2006-04-03 09:15:36")
        ae(t.transmit_date, "2006-04-03 09:22:28")
        ae(t.work_date, "2006-04-05 09:15:00")

        ae(t.con_name, "QUALITY ELECTRIC")
        ae(t.caller_contact, "BRIAN LANGE")
        ae(t.caller_phone, "(208)375-1300")
        ae(t.work_state, "ID")
        ae(t.work_county, "ADA")
        ae(t.work_city, "BOISE CITY")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "S PEPPERTREE AVE")
        ae(t.work_cross, "E GRAND FOREST DR")

        self.assertAlmostEqual(t.work_lat, 43.54497400,5)
        self.assertAlmostEqual(t.work_long, -116.13977350,5)
        ae(t.work_type, "INSTALL TRAFFIC LIGHT(S)")
        ae([x.client_code for x in t.locates],
           ['IDPOCL00', 'IPC01', 'QLNID51', 'IMG01', 'LVLCOM01', 'LVLCOM00'])

    def test_WashingtonState2(self):
        tl = ticketloader.TicketLoader("../testdata/WashingtonState2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['WashingtonState2'])
        ae = self.assertEquals

        ae(t.ticket_number, "6115464")
        ae(t.ticket_type, "2 FULL BUSINESS DAYS")
        ae(t.transmit_date, "2006-04-19 12:27:00")
        ae(t.call_date, "2006-04-19 12:21:00")
        ae(t.work_date, "2006-04-22 00:00:00") # 12:00 AM
        ae(t.work_state, "WA")
        ae(t.work_county, "CHELAN")
        ae(t.work_city, "DRYDEN")
        ae(t.work_address_number, "1")
        ae(t.work_address_street, "FRONTAGE ROAD")
        ae(t.work_cross, "DEADMAN HILL")
        ae(t.map_page, "24N18E27NW")
        ae(t.work_type, "INSTALL FLAG POLE")
        ae([x.client_code for x in t.locates],
           ['CHARTR02'])

    def test_Jacksonville2(self):
        tl = ticketloader.TicketLoader("../testdata/Jacksonville2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Jacksonville2'])
        ae = self.assertEquals

        ae(t.ticket_number, "152600999")
        ae(t.ticket_type, "STREET") # ...?
        ae(t.call_date, "2006-06-01 07:50:00")
        ae(t.transmit_date, "2006-06-01 14:11:11")
        ae(t.work_state, "FL")
        ae(t.work_county, "ST LUCIE")
        ae(t.work_city, "PORT ST LUCIE")
        ae(t.work_address_number, "238")
        ae(t.work_address_street, "SW KESTOR DR")
        ae(t.work_cross, "ATHENA DR")
        ae(t.map_page, "2712C8020B")
        ae(t.work_date, "2006-06-05 23:59:00")
        ae(t.work_type, "SITE PREP")
        ae(t.company, "HOLIDAY HUGGINS")
        ae(t.con_name, "PORT ST LUCIE TRACTOR SERVICE, INC.")
        self.assertAlmostEqual(t.work_lat,27.20625,5)
        self.assertAlmostEqual(t.work_long,-80.34375,5)
        ae([x.client_code for x in t.locates],
           ["AC1107", "AC1110", "FPLSTL", "SBF34", "SLUC01", "UQ1098", "FPLDBR",
            "LS1104"])

    def test_KentuckyNew(self):
        tl = ticketloader.TicketLoader("../testdata/KentuckyNew-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['KentuckyNew'])
        ae = self.assertEquals

        ae(t.transmit_date, "2006-07-11 12:07:54")
        ae(t.call_date, "2006-07-11 11:43:00")
        ae(t.work_date, "2006-07-13 11:15:00")
        ae(t.ticket_type, "NORM RXMT GRID")
        ae(t.work_state, "KY")
        ae(t.work_county, "HART")
        ae(t.work_city, "HORSE CAVE")
        ae(t.work_address_number, "3399")
        ae(t.work_address_street, "UNO HORSE CAVE RD")
        ae(t.work_cross, "LONOKE RD")
        ae(t.work_type, "INSTALL POLES")
        ae(t.company, "TAMMIE ARVIN")
        ae(t.con_name, "FISHEL CO")
        ae(t.con_address, "4508 BISHOP LN")
        ae(t.con_city, "LOUISVILLE")
        ae(t.con_state, "KY")
        ae(t.con_zip, "40218")
        ae(t.caller, "BRENDA RUNYAN")
        ae(t.caller_phone, "(502)456-2900")
        ae(t.caller_contact, "N/A")
        ae(t.caller_fax, "(502)266-5743")
        ae(t.caller_email, "BJRUNYAN@FISHELCO.COM")
        ae([x.client_code for x in t.locates], ["BELLTN"])
        self.assert_(len(t.work_remarks) > 10)

        raw = tl.tickets[7]
        t = self.handler.parse(raw, ['KentuckyNew'])
        self.assertEquals(t.ticket_type, "NORM RXMT STRT")
        self.assertEquals(t.transmit_date, "2006-07-17 11:41:01")

    def test_NorthCalifornia2(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCalifornia2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NorthCalifornia2'])
        ae = self.assertEquals

        ae(t.transmit_date, "2006-07-17 12:21:01")
        ae(t.ticket_type, "NORMAL NOTICE")
        ae(t.ticket_number, "0252769")
        ae(t.call_date, "2006-07-17 12:08:00")
        ae(t.work_date, "2006-07-19 12:30:00")
        ae(t.caller, "LOREN STONEBRINK")
        ae(t.con_name, "WARREN STONEBRINK CONSTRUCTION")
        ae(t.con_address, "516 HASSETT ST")
        ae(t.con_city, "BROOKINGS")
        ae(t.con_state, "OR")
        ae(t.con_zip, "97415")
        ae(t.caller_phone, "541-469-6341")
        ae(t.caller_email, "STONEBRINK@CHARTER.NET")
        ae(t.work_type, "TR FOR GAS L")
        ae(t.company, "P/O ZELLMER")
        ae(t.work_address_number, "205")
        ae(t.work_address_street, "BOW LN")
        ae(t.work_cross, "ARROWHEAD RD")
        ae(t.work_city, "CRESCENT CITY, CO AREA")
        ae(t.work_county, "DEL NORTE")
        ae(t.work_state, "CA")
        self.assertAlmostEqual(t.work_lat,41.824525,5)
        self.assertAlmostEqual(t.work_long,-124.142775,5)
        ae([x.client_code for x in t.locates],
           ['CHACRE', 'CTYCRE', 'PPLCRE', 'VERWCO'])

    def test_FL9002(self):
        tl = ticketloader.TicketLoader("../testdata/FL9002-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["FL9002"])
        ae = self.assertEquals

        ae(t.transmit_date, "2006-08-01 00:30:46")
        ae(t.ticket_number, "208611349")
        ae(t.work_address_number, "3429")
        ae(t.work_address_street, "HARBORSIDE CT")
        ae(t.work_date, "2006-07-31 23:59:00")
        self.assertAlmostEqual(t.work_lat,28.1916666667,5)
        self.assertAlmostEqual(t.work_long,-81.41875,5)
        ae([x.client_code for x in t.locates],
           ['KUA', 'OS1570', 'UTI297', 'LS1104', 'SYKES'])

    def test_Northwest(self):
        tl = ticketloader.TicketLoader("../testdata/Northwest-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Northwest"])
        ae = self.assertEquals

        ae(t.ticket_number, "6159663")
        ae(t.ticket_type, "NORMAL")
        ae(t.map_page, "02S01W12SW")
        ae(t.call_date, "2006-08-10 06:31:00")
        ae(t.transmit_date, "2006-08-10 06:45:50")
        ae([x.client_code for x in t.locates], ["NNG01"])
        ae(t.work_date, "2006-08-02 17:00:00")
        ae(t.work_state, "OR")
        ae(t.work_county, "MU")
        ae(t.work_city, "PORTLAND")
        ae(t.work_address_number, "3127")
        ae(t.work_address_street, "SE TACOMA ST")
        ae(t.work_cross, "JOHNSON CREEK BLVD")
        ae(t.work_type, "SOIL SAMPLING")
        ae(t.con_name, "ECO TECH LLC")
        ae(t.caller_contact, "MONICA HENDERSON")
        ae(t.caller_phone, "(503)493-1040")
        ae(t.company, "HANSON")

        raw = raw.replace('Send To: NNG01','')
        t = self.handler.parse(raw, ["Northwest"])
        ae(len(t.locates),0)

    def test_SC1421(self, format='SC1421'):
        tl = ticketloader.TicketLoader("../testdata/SC1421-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, [format])
        ae = self.assertEquals

        ae(t.ticket_number, "0609211920")
        ae(t.transmit_date, "2006-09-21 13:17:22")
        ae(t.call_date, "2006-09-21 13:17:15")
        ae(t.work_date, "2006-09-26 13:30:24")
        ae(t.work_city, "COLUMBIA")
        ae(t.work_state, "SC")
        ae(t.work_county, "RICHLAND")
        ae(t.work_address_number, "21")
        ae(t.work_address_street, "COLONY HOUSE CT")
        ae(t.work_cross, "CHINQUAPIN RD")
        ae(t.work_type, "CATV, INSTALL DROP(S)")
        self.assert_(t.work_description.startswith("PLEASE LOCATE"))
        self.assert_(t.work_description.endswith("IN FRONT"))
        ae(t.con_name, "TRI-STATE UG")
        ae(t.con_address, "2044 INDUSTRIAL BLVD")
        ae(t.company, "TRI-STATE UG")
        self.assertAlmostEqual(t.work_lat,34.0788729088,5)
        self.assertAlmostEqual(t.work_long,-81.1348976877,5)
        ae([x.client_code for x in t.locates],
           ["BSZB45", "COC82", "SCEKZ42", "SCP63", "TWCZ40"])
        ae(t.map_page, "3404B8108D")

    def test_SC1425(self):
        self.test_SC1421(format='SC1425')

    def test_AlaskaNew_1(self):
        tl = ticketloader.TicketLoader("../testdata/AlaskaNew-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['AlaskaNew'])
        ae = self.assertEquals

        ae(t.ticket_number, "2006241581")
        ae(t.ticket_type, "STANDARD")
        ae(t.transmit_date, "2006-08-14 15:54:35")
        ae(t.call_date, "2006-08-14 15:53:50")
        ae(t.work_date, "2006-08-16 15:53:00")
        ae(t.con_name, "ALASKA HOMES INC")
        ae(t.caller_contact, "GEORGE COWAN")
        ae(t.caller_phone, "(907)227-0741")
        ae(t.caller_fax, "(907)338-2610")
        ae(t.caller_email, "evanrowland@gci.net")
        ae(t.map_page, "AN1538")
        ae(t.work_city, "ANCHORAGE")
        ae(t.work_address_number, "124567")
        ae(t.work_address_street, "GLENKERRY DR")
        ae(t.work_cross, "CAMROSE DR and DONCASTER DR")
        ae(t.work_type, "PILINGS")
        ae([x.client_code for x in t.locates],
           ["AWWU", "CEA", "GCI A/E", "MOA SDTW", "ENS A/E", "ACS"])

        raw = raw.replace('AWWU       ',
                          'GCI - FIBER')
        t = self.handler.parse(raw, ['AlaskaNew'])
        ae([x.client_code for x in t.locates],
           ["GCI-FIBER", "CEA", "GCI A/E", "MOA SDTW", "ENS A/E", "ACS"])
        # invalid characters
        raw = tl.tickets[1]
        raw = raw.replace('AWWU','AW.|')
        self.failUnlessRaises(ticketparser.TicketError,self.handler.parse,raw, ['AlaskaNew'])

    def test_AlaskaNew_2(self):
        tl = ticketloader.TicketLoader("../testdata/AlaskaNew-1.txt")
        raw = tl.tickets[1]
        raw = raw + '\n \n'
        t = self.handler.parse(raw, ['AlaskaNew'])
        ae = self.assertEquals

        ae([x.client_code for x in t.locates],
           ["AWWU", "CEA", "GCI A/E", "MOA SDTW", "ENS A/E", "ACS"])

    def test_BatonRouge3_1(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge3-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["BatonRouge3"])
        ae = self.assertEquals

        ae(t.ticket_number, "60519142")
        ae(t.ticket_type, "48HR-48 HOURS NOTICE")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2006-10-19 06:44:00")
        ae(t.call_date, "2006-10-19 06:36:00")
        ae(t.work_date, "2006-10-23 07:00:00")
        ae(t.company, "ENTERGY")
        ae(t.work_state, "LA")
        ae(t.work_county, "EAST BATON ROUGE")
        self.assertAlmostEqual(t.work_lat,30.413640,4)
        self.assertAlmostEqual(t.work_long,-91.09922,2)
        ae([x.client_code for x in t.locates],
           ['ADELPH01', 'BRW01', 'BTR01', 'GSU04', 'GSUBR01', 'LAGR02',
            'TCIBTR01', 'KMCTEL01'])

    def test_BatonRouge3_2(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge3-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Ex. Coord: NW Lat: 30.4156320Lon: -91.1004820SE Lat: 30.4116490Lon:  -91.0979640',
                          'Ex. Coord: NW Lat: 30.4156320Lon:            SE Lat: 30.4116490Lon:             ')
        t = self.handler.parse(raw, ["BatonRouge3"])

        self.assertAlmostEqual(t.work_lat,30.413640,4)
        self.assertEquals(t.work_long,0.0)

    def test_BatonRouge3_3(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge3-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Ex. Coord: NW Lat: 30.4156320Lon: -91.1004820SE Lat: 30.4116490Lon:  -91.0979640',
                          'Ex. Coord: NW Lat: 30.4156320Lon: -91.1004820SE Lat: 30.4116490Lon:             ')
        t = self.handler.parse(raw, ["BatonRouge3"])

        self.assertAlmostEqual(t.work_lat,30.413640,4)
        self.assertEquals(t.work_long,-91.10048)

    def test_BatonRouge3_4(self):
        tl = ticketloader.TicketLoader("../testdata/BatonRouge3-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Ex. Coord: NW Lat: 30.4156320Lon: -91.1004820SE Lat: 30.4116490Lon:  -91.0979640',
                          'Ex. Coord: NW Lat: 30.4156320Lon:            SE Lat: 30.4116490Lon:             ')
        t = self.handler.parse(raw, ["BatonRouge3"])

        self.assertAlmostEqual(t.work_lat,30.413640,4)
        self.assertEquals(t.work_long,0.0)

    def test_Atlanta2(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta2'])
        ae = self.assertEquals

        ae(t.ticket_number, "11096-075-043")
        ae(t.transmit_date, "2006-11-09 17:13:14")
        ae(t.call_date, "2006-11-09 17:06:00")
        ae(t.ticket_type, "NORMAL")
        ae(t.kind, "NORMAL")
        ae(t.work_state, "GA")
        ae(t.work_county, "BIBB")
        ae(t.work_city, "MACON")
        ae(t.work_address_number, "224")
        ae(t.work_address_street, "ORANGE ST")
        ae(t.work_cross, "")
        ae(t.map_page, "3250B8338D")
        ae(t.work_type, "REPAIR SEWER SERVICE")
        ae(t.work_date, "2006-11-15 07:00:00")
        ae(t.company, "UPTON SINCLAIR")
        ae(t.con_name, "WATSON PLUMBING AND ASSOCIATES")
        ae(t.con_address, "2996 GRAY HWY")
        self.assertAlmostEqual(t.work_lat,32.84375,5)
        self.assertAlmostEqual(t.work_long,-83.6354166667,5)
        ae([x.client_code for x in t.locates],
           ['AGL118', 'ATT02', 'BGAWM', 'CCM01', 'GP500', 'MAC01', 'MAC02',
            'MWA01', 'MWA02'])

    def test_LA1411_1(self):
        # until we have test tickets, this will have to do.  the formats are
        # said to be exactly the same.
        tl = ticketloader.TicketLoader("../testdata/BatonRouge1-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['LA1411'])

        self.assertEquals(t.ticket_number, '2053')
        self.assertAlmostEqual(t.work_lat,30.5056944444,5)
        self.assertAlmostEqual(t.work_long,-91.2147222222,5)

    def test_NewJersey2(self):
        tl = ticketloader.TicketLoader("../testdata/NewJersey2-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NewJersey2'])
        ae = self.assertEquals

        ae(t.ticket_number, "063540056")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.transmit_date, "2006-12-20 07:28:00")
        ae(t.call_date, t.transmit_date)
        ae(t.work_date, "2006-12-20 07:23:00")
        ae(t.work_county, "SUSSEX")
        ae(t.work_city, "VERNON")
        ae(t.work_address_number, "11")
        ae(t.work_address_street, "BALDWIN DR")
        ae(t.work_cross, "MOTT DR")
        ae(t.work_type, "REPAIR WATER MAIN")
        ae(t.company, "UNITED WATER NEW JERSEY")
        ae(t.con_name, "HANK SANDERS INC.")
        ae(t.con_address, "PO BOX 1571")
        ae(t.con_city, "MCAFEE")
        ae(t.con_state, "NJ")
        ae(t.con_zip, "07428")
        ae(t.caller, "CHRIS SANDERS")
        ae(t.caller_phone, "973-827-6661")
        ae(t.serial_number, "GSUPLS063540056")
        ae(t.service_area_code, "JC")

        tl = ticketloader.TicketLoader("../testdata/NewJersey2-2.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['NewJersey2'])
        self.assertEquals(t.image.count("The information contained"), 0)
        self.assertEquals(t.image.count("End Request"), 1)
        self.assert_(t.image.rstrip().endswith("End Request"))

    def test_HoustonKorterra(self):
        tl = ticketloader.TicketLoader("../testdata/HoustonKorterra-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        ae = self.assertEquals

        ae(t.ticket_number, "070330024")
        ae(t.call_date, "2007-02-02 06:19:00")
        ae(t.transmit_date, "2007-02-02 06:21:59")
        ae(t.ticket_type, "EMERGENCY")
        ae(t.kind, "EMERGENCY")
        ae(t.map_page, "MAPSCO 677,X")
        ae(t.work_county, "TRAVIS")
        ae(t.work_city, "AUSTIN")
        ae(t.work_address_number, "11617")
        ae(t.work_address_street, "RAILTON")
        ae(t.work_date, "2007-02-02 06:30:00")
        ae(t.work_type, "EMER- SEWER LINE REPAIR")
        ae(t.company, "CITY OF AUSTIN WATER/WASTEWATE")

        ae(t.con_name, "CITY OF AUSTIN WATER/WASTEWATER")
        ae(t.work_cross, "MAYBACH")
        ae([x.client_code for x in t.locates], ['COP'])

    def test_Atlanta_7(self):
        # 2007-01-12: possible format change Atlanta, check if everything
        # still works as expected
        tl = ticketloader.TicketLoader("../testdata/Atlanta-7.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        ae = self.assertEquals

        ae(t.ticket_number, "11276-032-051")
        ae(t.ticket_type, "NORMAL 2NDREQ")
        ae(t.kind, "NORMAL")
        ae(t.transmit_date, "2007-01-12 06:58:46")
        ae(t.call_date, "2006-12-01 09:24:00")
        ae(t.work_state, "GA")
        ae(t.work_city, "GRIFFIN")
        ae(t.work_county, "SPALDING")
        ae(t.work_address_street, "DAWNVIEW DR")
        ae(t.work_type, "PLANTING TREES")
        ae(t.company, "BOB ADAMS HOMES")
        ae(t.con_name, "THOMS TREES AND PLANTS")
        ae(t.con_address, "625 MCBRIDE RD")
        ae(t.con_city, "FAYETTEVILLE")
        ae([x.client_code for x in t.locates], ['CCAST2', 'HCW02'])

    def test_OregonEM(self):
        tl = ticketloader.TicketLoader("../testdata/OregonEM-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['OregonEM'])
        ae = self.assertEquals

        ae(t.ticket_number, "7033524")
        ae(t.ticket_format, 'LEM1')
        ae(t.ticket_type, "48 HOUR NOTICE UPDATE")
        ae(t.transmit_date, "2007-02-26 05:52:00")
        ae(t.call_date, "2007-02-24 16:16:00")
        ae(t.work_date, "2007-02-28 08:00:00")
        ae(t.work_state, "OR")
        ae([x.client_code for x in t.locates], ['TEST03'])

        raw = tl.tickets[2]
        t = self.handler.parse(raw, ['OregonEM'])
        ae(t.ticket_number, "7053416")
        ae(t.work_state, "WA")

    def test_GA3003(self, format='GA3003'):
        tl = ticketloader.TicketLoader("../testdata/GA3003-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, [format])
        ae = self.assertEquals

        ae(t.ticket_number, "06217-400-001")
        ae(t.ticket_type, "LPMEET")
        ae(t.call_date, "2007-06-21 03:32:00")
        ae(t.work_state, "GA")
        ae(t.work_county, "FULTON")
        ae(t.work_city, "HAPEVILLE")
        ae(t.work_address_number, "")
        ae(t.work_address_street, "Sunset Ave")
        ae(t.work_cross, "N Central Ave")
        ae(t.work_type, "replace main and service sewer")
        ae(t.con_name, "RONNIE D JONES ENTERPRISES")
        ae(t.con_address, "315 MILLER FARMER IND BLVD")
        ae(t.con_city, "NEWNAN")
        ae(t.con_state, "GA")
        ae(t.con_zip, "30263")
        ae(t.transmit_date, "2007-06-25 02:20:50")
        ae(t.work_date, "2007-06-26 10:00:00")
        ae(t.caller, "WILLIS STALLINGS")
        ae(t.caller_phone, "770-251-2667")
        ae(t.caller_fax, "")
        ae(t.caller_altphone, "")
        ae(t.caller_email, "willis@ronniedjones.com")
        ae(t.respond_date, '2007-06-28 11:59:00')
        ae(t.company, 'city of hapeville')
        ae(len(t.locates), 16)
        ae(t.locates[0].client_code, "BSCA")

        # test DIG ticket
        raw = tl.tickets[1]
        t = self.handler.parse(raw, [format])
        ae(t.work_date, "2000-01-01 12:00:00")
        ae(t.ticket_number, "06217-400-001")
        ae(t.ticket_type, "LPEXCA")
        ae(t.work_state, "GA")
        ae(t.work_county, "FULTON")
        ae(t.work_city, "HAPEVILLE")
        ae(t.transmit_date, "2007-06-29 01:45:00")
        ae(t.call_date, "2007-06-29 01:44:00")
        ae(t.con_name, "RONNIE D JONES ENTERPRISES")
        ae(len(t.locates), 14)

    def test_Atlanta4(self):
        self.test_GA3003(format='Atlanta4')

    def test_GA3003_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCT4","TEST-2008-09-23-00065_LPMEET.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['GA3003'])
        ae = self.assertEquals

        ae(t.ticket_number, '08198-400-001')
        ae(t.ticket_type, "LPMEET")
        ae(t.call_date, '2008-08-19 16:37:00')
        ae(t.work_state, "GA")
        ae(t.work_county, 'GWINNETT')
        ae(t.work_city, 'DULUTH')
        ae(t.work_address_number, "3400")
        ae(t.work_address_street, 'SUMMIT RIDGE PKWY')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')
        ae(t.con_name, 'UPC')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_state, "GA")
        ae(t.con_zip, '30096')
        ae(t.transmit_date, '2008-09-23 12:46:51')
        ae(t.work_date, '2008-08-25 12:00:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_phone, '404-444-3606')
        ae(t.caller_fax, "")
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'MHOYT@GAUPC.COM')
        ae(len(t.locates), 8)
        ae(t.locates[0].client_code, "AGLN01")
        ae(t.respond_date,'2008-08-27 23:59:00')

    def test_FCT4_1(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FCT4", "TEST-2008-09-23-00006_NORMAL.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])
        ae = self.assertEquals

        ae(t.ticket_number, '08198-205-001')
        ae(t.call_date, '2008-08-19 16:16:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'IWEATHERS@GAUPC.COM')
        ae(t.caller_phone, '404-444-3606')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'NORMAL')
        ae(t.legal_good_thru,'2008-09-09 00:00:00')
        ae(t.legal_restake,'2008-09-04 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.ticket_number,'08198-205-001')
        ae(t.ticket_type,'NORMAL')
        ae(t.transmit_date,'2008-09-23 12:46:47')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-22 07:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE TS *** WILL BORE Road')
        ae(t.work_state, 'GA')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')
        ae(t.respond_date,'2008-08-21 23:59:00')

    def test_FCT4_2(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FCT4", "TEST-2008-09-23-00016_DESIGN.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])
        ae = self.assertEquals

        ae(t.ticket_number, '08198-205-002')
        ae(t.call_date, '2008-08-19 16:22:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'IWEATHERS@GAUPC.COM')
        ae(t.caller_phone, '404-444-3606')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'NORMAL')
        ae(t.legal_good_thru,'2008-09-09 00:00:00')
        ae(t.legal_restake,'2008-09-04 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.ticket_number,'08198-205-002')
        ae(t.ticket_type,'DESIGN')
        ae(t.transmit_date,'2008-09-23 12:46:48')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-22 07:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_type, 'DESIGN')
        ae(t.work_state, 'GA')
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE '+\
                        'TS *** WILL BORE Road THIS INFORMATION HAS NOT BEEN VERIFIED BY THE UTILITIES '+\
                        'PRO TECTION CENTER AND IS NOT WARRANTED FOR ANY PURPOSE. THIS INFORMATION '+\
                        'IS FURNISHED SOLELY AS AN ACCOMMODATION TO THE REQUESTING PARTY WHO W '+\
                        'ARRANTS THAT IT SHALL NOT BE USED IN CONNECTION WITH ANY EXCAVATION O R '+\
                        'OTHER WORK COVERED BY TITLE 25, CHAPTER 9 OF THE OFFICIAL CODE OF G EORGIA ANNOTATED.')
        ae(t.respond_date,'2008-08-21 23:59:00')

    def test_FCT4_3(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FCT4", "TEST-2008-09-23-00028_EMERG.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])
        ae = self.assertEquals

        ae(t.ticket_number, '08198-205-003')
        ae(t.call_date, '2008-08-19 16:27:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '')
        ae(t.caller_email, '')
        ae(t.caller_phone, '404-444-3606')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'EMERGENCY')
        ae(t.legal_good_thru,'2008-08-22 00:00:00')
        ae(t.legal_restake,'2008-08-21 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.ticket_number,'08198-205-003')
        ae(t.ticket_type,'EMERG')
        ae(t.transmit_date,'2008-09-23 12:46:49')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-19 00:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE TS EMERGENCY: *** WILL BORE Road')
        ae(t.work_state, 'GA')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')
        ae(t.respond_date,'2008-08-19 00:00:00')

    def test_FCT4_4(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FCT4", "TEST-2008-09-23-00041_INSUF.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])
        ae = self.assertEquals

        ae(t.call_date, '2008-08-19 16:28:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'IWEATHERS@GAUPC.COM')
        ae(t.caller_phone, '404-444-3606')
        ae(t.company,'UTILITIES PROTECTION CENTER, INC')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'NORMAL')
        ae(t.legal_good_thru,'2008-09-09 00:00:00')
        ae(t.legal_restake,'2008-09-04 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.priority,'2')
        ae(t.respond_date,'2008-08-21 23:59:00')
        ae(t.ticket_number, '08198-205-004')
        ae(t.ticket_type,'INSUF')
        ae(t.transmit_date,'2008-09-23 12:46:49')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-22 07:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE TS *** WILL BORE Road')
        ae(t.work_state, 'GA')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')

    def test_FCT4_5(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FCT4", "TEST-2008-09-23-00051_DAMAGE.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])
        ae = self.assertEquals

        ae(t.call_date, '2008-08-19 16:34:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'IWEATHERS@GAUPC.COM')
        ae(t.caller_phone, '404-444-3606')
        ae(t.company,'UTILITIES PROTECTION CENTER, INC')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'NORMAL')
        ae(t.legal_good_thru,'2008-08-20 00:00:00')
        ae(t.legal_restake,'2008-08-19 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.priority,'6')
        ae(t.respond_date,'2008-08-19 00:00:00')
        ae(t.ticket_number, '08198-205-005')
        ae(t.ticket_type,'DAMAGE')
        ae(t.transmit_date,'2008-09-23 12:46:50')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-19 00:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE TS DAMAGE TO SERVICE/DROP  EXTENT: CUT **SERVICE OUT *** WILL BORE Road')
        ae(t.work_state, 'GA')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')

    def test_FAU4_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FAU4","FAU4-2009-04-16-00029.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw)
        ae = self.assertEquals
        ae(t.work_address_number, '63')
        ae(t.work_address_number_2, '')

    def test_FAU4_meet_1(self):
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/FAU4-2007-12-04-00001.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw)
        ae = self.assertEquals
        ae(t.ticket_number, '12047-400-001')
        ae(t.ticket_type, "LPMEET")
        ae(t.call_date, '2007-12-04 12:39:00')
        ae(t.work_date, '2007-12-11 11:00:00')
        ae(t.work_state, "GA")
        ae(t.work_county, 'DOOLY')
        ae(t.work_city, 'UNADILLA')
        ae(t.work_address_number, "")
        ae(t.work_address_street, 'CRUMPLER AVE')
        ae(t.work_type, 'INSTALLING WATER MAIN AND SERVICE LINES')
        ae(t.con_name, 'SOUTHERN TRENCHING INC')
        ae(t.con_address, '249 HURRICANE SHOALS RD')
        ae(t.con_city, 'LAWRENCEVILLE')
        ae(t.con_state, "GA")
        ae(t.con_zip, '30045')
        ae(t.con_type, 'CONT')
        ae(t.transmit_date, '2007-12-04 12:43:12')
        ae(t.work_date, '2007-12-11 11:00:00')
        ae(t.caller, 'TRACI MATTHEWS')
        ae(t.caller_phone, '770-513-3755')
        ae(t.caller_fax, "")
        ae(t.caller_altphone, '678-409-4330')
        ae(t.caller_email, 'SOUTHRNTRENCHING@BELLSOUTH.NET')
        ae(len(t.locates), 15)

    def test_FAU4_meet_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCT4","TEST-2008-09-23-00065_LPMEET.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta4'])
        ae = self.assertEquals

        ae(t.ticket_format,'FAU4')
        ae(t.source,'FAU4')

        ae(t.ticket_number, '08198-400-001')
        ae(t.ticket_type, "LPMEET")
        ae(t.call_date, '2008-08-19 16:37:00')
        ae(t.work_state, "GA")
        ae(t.work_county, 'GWINNETT')
        ae(t.work_city, 'DULUTH')
        ae(t.work_address_number, "3400")
        ae(t.work_address_street, 'SUMMIT RIDGE PKWY')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')
        ae(t.con_name, 'UPC')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_state, "GA")
        ae(t.con_zip, '30096')
        ae(t.transmit_date, '2008-09-23 12:46:51')
        ae(t.work_date, '2008-08-25 12:00:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_phone, '404-444-3606')
        ae(t.caller_fax, "")
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'MHOYT@GAUPC.COM')
        ae(len(t.locates), 8)
        ae(t.locates[0].client_code, "AGLN01")
        ae(t.respond_date,'2008-08-27 23:59:00')

    def test_FAU5_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCT4","TEST-2008-09-23-00006_NORMAL.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta5"])
        ae = self.assertEquals

        ae(t.ticket_format,'FAU5')
        ae(t.source,'FAU5')
        ae(t.ticket_number, '08198-205-001')
        ae(t.call_date, '2008-08-19 16:16:00')
        ae(t.caller, 'CC EXCAVATOR')
        ae(t.caller_altphone, '770-476-6042')
        ae(t.caller_email, 'IWEATHERS@GAUPC.COM')
        ae(t.caller_phone, '404-444-3606')
        ae(t.con_address, '3400 SUMMIT RIDGE PKWY')
        ae(t.con_city, 'DULUTH')
        ae(t.con_name, 'UPC')
        ae(t.con_state, 'GA')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '30096')
        ae(t.duration, '1 YEAR')
        ae(t.kind,'NORMAL')
        ae(t.legal_good_thru,'2008-09-09 00:00:00')
        ae(t.legal_restake,'2008-09-04 00:00:00')
        ae(t.map_page,'3359D8409A')
        ae(t.ticket_type,'NORMAL')
        ae(t.transmit_date,'2008-09-23 12:46:47')
        ae(t.work_address_street,'SUMMIT RIDGE PKWY')
        ae(t.work_city,'DULUTH')
        ae(t.work_county,'GWINNETT')
        ae(t.work_date, '2008-08-22 07:00:00')
        self.assertAlmostEqual(t.work_lat, 33.9875,4)
        self.assertAlmostEqual(t.work_long, -84.1666666667)
        ae(t.work_remarks, 'TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE TS *** WILL BORE Road')
        ae(t.work_state, 'GA')
        ae(t.work_type, 'INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION')
        ae(t.respond_date,'2008-08-21 23:59:00')


    def test_Dallas4(self, format='Dallas4'):
        tl = ticketloader.TicketLoader("../testdata/Dallas4-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, [format])
        ae = self.assertEquals

        ae(t.ticket_number, "072414585")
        ae(t.ticket_type, "ROUTINE")
        ae(t.kind, "NORMAL")
        ae(t.call_date, "2007-08-29 15:22:00")
        ae(t.map_page, "GTMAPS 375,R")
        ae(t.work_county, "BRAZOS")
        ae(t.work_city, "COLLEGE STATION")
        ae(t.work_address_number, "3403")
        ae(t.work_address_street, "F & B RD")
        ae(t.work_date, "2007-08-31 15:30:00")
        ae(t.work_type, "DEMOLITION")
        ae(t.caller, "JERRY FALER")
        ae(t.con_name, "CST ENVIRONMENTAL")
        ae(t.company, "TEXAS A&M")
        ae(t.caller_contact, "SEVERO BALLESTEROS")
        ae(t.caller_phone, "( 281 )541-8629")
        ae(t.caller_fax, "( 281 )443-3469")
        ae(t.caller_email, "JFALER@CSTENV.COM")
        ae(t.work_remarks, "MARK ALL UNDERGROUND FACILITIES AS NECESSARY")
        ae(t.work_cross, "WELLBORN RD")
        ae(t.transmit_date, "2007-08-29 15:31:29")
        ae(len(t.locates), 1)
        ae(t.locates[0].client_code, "BY1")

    def test_TX6004(self):
        self.test_Dallas4(format='TX6004')

    def test_PA7501_dig_date_1(self):
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/7501-2007-10-15-00986.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['PA7501'])
        ae = self.assertEquals

        ae(t.work_date, "2007-10-15 00:00:00")
        self.assertAlmostEqual(t.work_lat,39.8802572765,6)
        self.assertAlmostEqual(t.work_long,-75.5989463559,6)

    def test_FL9003_1(self):
        """
        Test a specific FL9003 ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","9003-2007-12-11-00054.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["FL9003"])
        ae = self.assertEquals

        ae(t.call_date, '2007-12-11 15:29:00')
        ae(t.caller,'AMANDA GWYNN')
        ae(t.caller_email,'MYDREAMPOOL@AOL.COM')
        ae(t.caller_fax,'407-895-0588')
        ae(t.caller_phone,'407-354-2549')
        ae(t.channel,'WEB')
        ae(t.company,'HOMEOWNER')
        ae(t.con_address,'PO BOX 2754')
        ae(t.con_city,'WINDERMERE')
        ae(t.con_name,'DREAM POOLS')
        ae(t.con_state,'FL')
        ae(t.con_zip,'34786')
        ae(t.due_date,'2007-12-13 23:59:00')
        ae(t.duration,'03 DAYS')
        ae(t.kind,'NORMAL')
        ae(t.legal_due_date,'2007-12-13 23:59:00')
        ae(t.map_page,'2833C8121B')
        ae(t.operator,'AMG')
        ae(t.source,'9003')
        ae(t.ticket_format,'9003')
        ae(t.ticket_number,'345709157')
        ae(t.ticket_type,'STREET')
        ae(t.transmit_date,'2007-12-11 15:30:05')
        ae(t.work_address_number,'1505')
        ae(t.work_address_street,'PINECREST PL')
        ae(t.work_city,'ORLANDO')
        ae(t.work_county,'ORANGE')
        ae(t.work_cross,'MILLS AVE')
        ae(t.work_date,'2007-12-13 23:59:00')
        ae(t.work_description,'ENTIRE REAR OF THE PROPERTY')
        self.assertAlmostEqual(t.work_lat,28.55625,5)
        self.assertAlmostEqual(t.work_long,-81.3625,5)
        ae(t.work_notc,'056')
        ae(t.work_state,'FL')
        ae(t.work_subdivision,'COLONIALTOWN')

    def test_PA7501_lat_long(self):
        tl = ticketloader.TicketLoader("../testdata/PA7501-new.txt")
        lat_longs = [(40.539698,-79.7744015),
                     (40.541916,-79.775088),
                     (40.542155,-79.774888),
                     (39.9255673574,-75.4276802459),
                     (40.5418475,-79.774305),
                     (40.5409245,-79.7799965),
                     (40.3263803292,-80.0407613245),
                     (39.7607384322,-75.8820893213)]
        for i,raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, ['PA7501'])
            self.assertAlmostEqual(lat_longs[i][0],t.work_lat)
            self.assertAlmostEqual(lat_longs[i][1],t.work_long)

    def test_Dallas1_2(self):
        """
        Test a FDX1 & FDX4 tickets with too long MAPSCO references
        """
        tickets = ['FDX1-2008-07-30-01044.txt',
                   'FDX4-2008-07-30-01198.txt',
                   'FDX4-2008-07-30-01348.txt',
                   'FDX4-2008-07-30-01653.txt',
                   'FDX4-2008-07-30-01654.txt',
                   'FDX4-2008-07-30-01658.txt',
                   'FDX4-2008-07-30-01660.txt',
                   'FDX4-2008-07-30-01661.txt',
                   'FDX4-2008-07-30-01662.txt',
                   'FDX4-2008-07-30-01666.txt',
                   'FDX4-2008-07-30-01667.txt',
                   'FDX4-2008-07-30-01672.txt',
                   'FDX4-2008-07-30-01673.txt',
                   'FDX4-2008-07-30-01869.txt',
                   'FDX4-2008-07-30-01871.txt']
        for ticket in tickets:
            tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","FDX1",ticket))
            raw = tl.tickets[0]
            if ticket.startswith('FDX1'):
                t = self.handler.parse(raw, ["Dallas1"])
            else:
                t = self.handler.parse(raw, ["Dallas4"])
            self.assert_(len(t.map_page) <= 20)

    def test_DIGGTESS_1(self):
        """
        Test a DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","98380-5979242-081130023.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.ticket_number,'081130023')
        ae(t.operator,'Ashley O')
        ae(t.channel,'Voice')
        ae(t.work_notc,'0')
        ae(t.ticket_type,'Emergency')
        ae(t.call_date,'2008-04-22 06:06:49')
        ae(t.transmit_date,'2008-04-22 06:06:49')
        ae(t.map_ref,'')
        ae(t.con_name,'AEP')
        ae(t.con_address,'PO BOX 832')
        ae(t.con_city,'KINGSVILLE')
        ae(t.con_state,'TX')
        ae(t.con_zip,'78363')
        ae(t.caller,'LARRY RUIZ')
        ae(t.caller_contact,'LARRY RUIZ')
        ae(t.caller_phone,'(361) 779-1271')
        ae(t.caller_fax,'(361) 221-0463')
        ae(t.caller_email,'')
        ae(t.caller_altcontact,'')
        ae(t.work_state,'TX')
        ae(t.work_county,'KLEBERG')
        ae(t.work_city,'KINGSVILLE')
        ae(t.work_address_number,'0')
        ae(t.work_address_street,'HWY 77')
        ae(t.work_date,'2008-04-22 06:15:00')
        ae(t.work_type,'EMER - BROKEN POLE REPLACEMENT')
        ae(t.company,'AEP')
        ae(t.work_remarks,'EMER - BROKEN POLE REPLACEMENT - CREW EN ROUTE - CUST W\\ SERVICE - POLE IS\n'\
                          'LEANING - FROM THE INTER GO S ON HWY 77 APPX 0.1MI TO LOCATION ON E SIDE OF\n'\
                          'RD - AREA MARKED WITH WOODEN STAKE, RED PAINT & RED RIBBON.')
        ae(t.work_cross,'CAESAR AVE')
        ae(t.explosives,'No')
        ae(t.map_page,'')
        self.assertAlmostEqual(t.work_lat,27.504081138032301,5)
        self.assertAlmostEqual(t.work_long,-97.84051582378855,5)
        ae(t.locates[0].client_code,'TEST')

    def test_DIGGTESS_2(self):
        """
        Test another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDX4-2008-12-02-01631.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('Aaron','AARON')
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.map_page,'MAPSCO 50,Y')
        ae(t.work_type,'EMER-WATER MAIN REPAIR')
        ae(t.work_address_number, '5151')
        ae(t.work_address_street, 'BROADWAY AVE')

    def test_DIGGTESS_3(self):
        """
        Test another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDX4-2008-12-02-01645.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('Aaron','AARON')
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.map_page,'MAPSCO 31A,B')
        ae(t.work_type,'FIBER DROP')
        ae(t.work_address_number, '0')
        ae(t.work_address_street, 'RIDGEVIEW LN')

    def test_DIGGTESS_4(self):
        """
        Test another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDX4-2008-12-05-01116.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.ticket_type,'No Response')
        ae(t.transmit_date,'2008-12-05 09:38:49')
        ae(t.work_type,'EMER ELEC CABLE REPAIR')
        ae(t.work_address_number, '0')
        ae(t.work_address_street, 'WINDCREST LN')

    def test_DIGGTESS_5(self):
        """
        Test another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDXTEST-2008-09-17-00061.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('Aaron','AARON')
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.map_page, 'KEYMAP 650,M')
        ae(t.work_type,'EXCAVATE AND REPAIR BLOW-OFF VALVE')
        ae(t.work_address_number, '3506')
        ae(t.work_address_street, 'CORNWALL CT')

    def test_DIGGTESS_6(self):
        """
        Test yet another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDX4-2008-12-15-02829.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.call_date,'2008-12-15 12:58:09')
        ae(t.work_type,'WATER LINE REPLACEMENT')
        ae(t.work_address_number, '0')
        ae(t.work_address_street, 'CHERRY HILL LN')

    def test_DIGGTESS_7(self):
        """
        Test yet another DIGGTESS ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","DIGGTESS","FDX4-2009-01-04-00369.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["DIGGTESS"])
        ae = self.assertEquals

        ae(t.con_name,'KLAASMEYER')
        ae(t.work_address_number, '0')
        ae(t.work_address_street, 'FRY LN')

    def test_DIGGTESS_Meet_1(self):
        """
        Test DIGGTESS meet ticket
        """
        meet_tickets = glob(os.path.join(_testprep.TICKET_PATH,
          "sample_tickets", "DIGGTESS", "Meets-FDX4","FDX4-2009-01-05-*.txt"))
        for ticket_file in meet_tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["DIGGTESS"])
            self.assertNotEquals(t.ticket_type.find('-MEET'),-1)

    def test_WV1181(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1181-2011-03-23", "1181-2011-03-23-00001.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WV1181"])

        ae = self.assertEquals

        ae(t.ticket_number, "103280001")
        ae(t.ticket_type, "NORMAL")
        ae(t.transmit_date, "2010-11-24 05:47:13")
        ae(t.call_date, t.transmit_date)
        ae(t.operator, 'RO_CHRIS P')
        ae(t.channel, 'FAX')
        ae(t.con_name, 'ALLEGHENY POWER')
        ae(t.con_address, '237 HARTMAN RD')
        ae(t.con_city, 'MORGANTOWN')
        ae(t.con_state, 'WV')
        ae(t.con_zip, '26505')
        ae(t.caller, 'MARK WILSON')
        ae(t.caller_contact, 'MARK WILSON/JUDY G')
        ae(t.caller_phone, '(301) 616-1431')
        ae(t.caller_email, 'MWILS11@ALLGHENYPOWER.COM')
        ae(t.caller_fax, '(304) 368-3798')
        ae(t.company, 'ALLEGHENY POWER')
        ae(t.work_date, '2010-11-29 06:45:46')
        ae(t.work_type, 'ELEC POLE LINE')
        ae(t.work_state, 'WV')
        ae(t.work_county, 'MONONGALIA')
        ae(t.work_city, 'MORGANTOWN')
        ae(t.work_address_number, '0')
        ae(t.work_address_street, 'RIVER RD')
        ae(t.work_cross, 'DUPONT RD')
        ae(t.work_remarks[:13], 'INSTLLING NEW')
        ae(t.explosives, 'No')

        self.assertAlmostEqual(t.work_lat, 39.6157964, 5)
        self.assertAlmostEqual(t.work_long, -79.979497, 5)

        ae(len(t.locates), 8)
        ae(t.locates[0].client_code, 'AC')
        ae(t.locates[-1].client_code, 'RRP')

    def test_TennesseeKortera_1(self):
        """
        Test a TennesseeKortera ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","FTL5","ftl5-2008-10-22-00001.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["TennesseeKorterra"])
        ae = self.assertEquals
        ae(t.call_date,'2008-09-25 14:38:00')
        ae(t.caller,'JIM MAJORS')
        ae(t.caller_altphone,'(865) 856-3639')
        ae(t.caller_contact,'JIM MAJORS')
        ae(t.caller_phone,'(865) 856-3639')
        ae(t.company,'MILLERS FUNERAL HOME')
        ae(t.con_address,'PO BOX 6346')
        ae(t.con_city,'MARYVILLE')
        ae(t.con_name,'MIKE DAVIS PLUMBING')
        ae(t.con_state,'TN')
        ae(t.con_zip,'37802')
        ae(t.explosives,'N')
        ae(t.kind,'EMERGENCY')
        ae(t.map_page,'57F')
        ae(t.operator,'CHANCE')
        ae(t.source,'FTL5')
        ae(t.ticket_number,'082691473')
        ae(t.ticket_type,'EMERGENCY')
        ae(t.transmit_date,'2008-09-25 14:38:00')
        ae(t.work_address_number,'910')
        ae(t.work_address_street,'YOUNG AVE')
        ae(t.work_city,'MARYVILLE')
        ae(t.work_county,'BLOUNT')
        ae(t.work_cross,'N MAPLE ST')
        ae(t.work_date,'2008-09-25 14:45:00')
        self.assertAlmostEquals(t.work_lat,35.7466298194,5)
        self.assertAlmostEquals(t.work_long,-83.9834681137,5)
        ae(t.work_remarks,'EMERGENCY - UTIL IS REQUIRED BY LAW TO MARK U/G '+\
                          'FACILITIES WITHIN 2 HOUR OF NOTIFICATION...CREW IS '+\
                          'ON SITE...FROM THE INTER GO N APPX 300-400 YARD TO THE '+\
                          'PROP...MARK FRONT OF PROP...AREA MARKED IN WHITE PAINT..')
        ae(t.work_type,'SEWER LINE, INSTL AND/OR REPAIR')
        ae(len(t.locates),5)

    def test_Oregon_WorkDescription(self):
        """
        Test that the oregon parser picks up the work description
        """
        tickets = glob(os.path.join("../testdata","sample_tickets","LCC1","LCC1*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["Oregon3"])
            self.assert_(len(t.work_description))
        tickets = glob(os.path.join("../testdata","sample_tickets","LOR1","LOR1*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["Oregon2"])
            self.assert_(len(t.work_description))
        tickets = glob(os.path.join("../testdata","sample_tickets","LOR2","LOR2*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["OregonFalcon"])
            self.assert_(len(t.work_description))
        tickets = glob(os.path.join("../testdata","sample_tickets","LQW1","LQW1*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["QWEST_IDL", "QWEST_IEUCC", "QWEST_OOC", "QWEST_UULC", "QWEST_WOC"])
            self.assert_(len(t.work_description))
        tickets = glob(os.path.join("../testdata","sample_tickets","LWA1","LWA1*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ["WashingtonState"])
            self.assert_(len(t.work_description))

    def test_MississippiKorterra_1(self):
        """
        Test a MississippiKorterra ticket
        """
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FJL4","TEST-2009-03-31-00030.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["MississippiKorterra"])
        ae = self.assertEquals
        ae(t.call_date,'2009-03-31 06:39:00')
        ae(t.caller, 'BOB GOUDELOCK')
        ae(t.caller_altphone,'')
        ae(t.caller_contact,'BOB GOUDELOCK')
        ae(t.caller_email,'ROBERT.GOUDELOCK@ATMOSENERGY.COM')
        ae(t.caller_fax, '(601) 355-9702')
        ae(t.caller_phone,'(601) 961-6782')
        ae(t.company,'ATMOS ENERGY')
        ae(t.con_address,'4155 INDUSTRIAL DR')
        ae(t.con_city,'JACKSON')
        ae(t.con_name,'ATMOS ENERGY')
        ae(t.con_state,'MS')
        ae(t.con_zip,'39209')
        ae(t.explosives,'N')
        ae(t.kind,'NORMAL')
        ae(t.map_page,'02E06N12NE')
        ae(t.operator,'BGOUDELOCK')
        ae(t.source,'FJL4')
        ae(t.ticket_number,'09033106420004')
        ae(t.ticket_type,'NORMAL')
        ae(t.transmit_date,'2009-03-31 06:39:00')
        ae(t.work_address_number,'1139')
        ae(t.work_address_street,'OLD FANNIN RD')
        ae(t.work_city,'FLOWOOD')
        ae(t.work_county,'RANKIN')
        ae(t.work_cross,'SPANN ')
        ae(t.work_date,'2009-04-02 07:00:00')
        self.assertAlmostEquals(t.work_lat,32.3808761202,5)
        self.assertAlmostEquals(t.work_long,-90.0426500682,5)
        self.assertTrue(t.work_description.startswith('FROM THE INTERSECTION GO SOUTH'))
        self.assertTrue(t.work_description.endswith('IN FRONT OF POTTERY BUSINES'))
        ae(t.work_state, 'MS')
        ae(t.work_type,'GAS, REPAIR LEAK')
        ae(len(t.locates),7)

    def test_SouthCaliforniaSDGKorterra_1(self):
        '''
        2275: Korterra-SDG ticket feed for CA
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-14-00240.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCaliforniaSDGKorterra"])
        self.assertEquals(t.transmit_date, '2009-04-14 13:51:41')
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, 'NCU01')
        self.assertEquals(t.locates[1].client_code, 'NCU01G')

    def test_SouthCaliforniaSDGKorterra_2(self):
        '''
        2275: Korterra-SDG ticket feed for CA
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-14-00240.txt"))
        raw = tl.getTicket()
        raw = raw.replace('NCU01',"NEU01")
        t = self.handler.parse(raw, ["SouthCaliforniaSDGKorterra"])
        self.assertEquals(len(t.locates),2)
        self.assertEquals(t.locates[0].client_code, 'NEU01')
        self.assertEquals(t.locates[1].client_code, 'NEU01G')

    def test_SouthCaliforniaSDGKorterra_3(self):
        '''
        2275: Korterra-SDG ticket feed for CA
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-14-00240.txt"))
        raw = tl.getTicket()
        raw = raw.replace('NCU01',"BCU01")
        t = self.handler.parse(raw, ["SouthCaliforniaSDGKorterra"])
        self.assertEquals(len(t.locates),2)
        self.assertEquals(t.locates[0].client_code, 'BCU01')
        self.assertEquals(t.locates[1].client_code, 'BCU01G')

    def test_SouthCaliforniaSDGKorterra_4(self):
        '''
        2275: Korterra-SDG ticket feed for CA
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-14-00240.txt"))
        raw = tl.getTicket()
        raw = raw.replace('NCU01',"CMU01")
        t = self.handler.parse(raw, ["SouthCaliforniaSDGKorterra"])
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, 'CMU01')
        self.assertEquals(t.locates[1].client_code, 'CMU01G')

        # Mantis #2975
        raw = raw.replace('CMU01', 'EAU01')
        t = self.handler.parse(raw, ["SouthCaliforniaSDGKorterra"])
        self.assertEquals(len(t.locates), 2)
        self.assertEquals(t.locates[0].client_code, 'EAU01')
        self.assertEquals(t.locates[1].client_code, 'EAU01G')

    def test_SouthCaliforniaSCA2Korterra_1(self):
        '''
        2275: Korterra-SDG ticket feed for CA
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-14-00240.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["SouthCaliforniaSCA2Korterra"])
        self.assertEquals(t.source, 'SCA2')
        self.assertEquals(t.ticket_format, 'SCA2')

    def test_NashvilleKorterra_1(self):
        '''
        2282: new ticket feed for FNV3
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FNV3","FNV3-2009-04-01-00034.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NashvilleKorterra"])
        self.assertEquals(t.transmit_date, '2009-04-01 09:24:04')
        self.assertEquals(len(t.locates),6)
        self.assertEquals(t.locates[5].client_code, 'U01')

    def test_NorthCarolina2_1(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00007.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.call_date,'2009-05-07 10:08:00')
        ae(t.caller, 'SAM POTTER-NO EMAIL')
        ae(t.caller_altphone,'')
        ae(t.caller_cellular, '919-218-9654')
        ae(t.caller_contact, 'SAME')
        ae(t.caller_email,'')
        ae(t.caller_fax, '')
        ae(t.caller_phone, '919-639-6216')
        ae(t.company, 'CLASSIC ELECTRIC')
        ae(t.con_address, '220 HILLCREST DR')
        ae(t.con_city, 'WILLOW SPRING')
        ae(t.con_name, 'SAM POTTER DIRECTIONAL BORING')
        ae(t.con_state, 'NC')
        ae(t.con_type, 'CONT')
        ae(t.con_zip, '27592')
        ae(t.duration, '2 DAYS')
        ae(t.explosives,'N')
        ae(t.kind,'NORMAL')
        ae(t.map_page, '3551A7838B')
        ae(t.operator, 'TAN')
        ae(t.priority, 'SHRT')
        ae(t.revision, '0')
        ae(t.source,'FCL3')
        ae(t.ticket_format,'FCL3')
        ae(t.ticket_number, 'A091270725')
        ae(t.ticket_type, 'SHRT NEW GRID LR')
        ae(t.transmit_date, '2009-05-07 10:10:43')
        ae(t.work_address_number,'')
        ae(t.work_address_street, 'WHITTIER DR')
        ae(t.work_city, 'RALEIGH')
        ae(t.work_county, 'WAKE')
        ae(t.work_cross, 'NORTHCLIFT DR')
        ae(t.work_date, '2009-05-11 09:00:00')
        self.assertTrue(t.work_description.startswith('LOCATE BY 05/12/09 12:01 AM\nDISTANCE FROM CROSS STREET IS:'))
        self.assertAlmostEquals(t.work_lat,35.8625,4)
        self.assertAlmostEquals(t.work_long,-78.64375,4)
        self.assertTrue(t.work_remarks.startswith('CALL SAM TO MEET YOU ON SITE.\nPER CALLER: THIS WILL BE GOING TOWA'))
        ae(t.work_state, 'NC')
        ae(t.work_subdivision, 'OPTIMIST PARK COMMUNITY CENTER')
        ae(t.work_type, 'INSTALLING PVC CONDUIT')
        ae(len(t.locates),7)
        ae(t.locates[0].client_code, 'ATT322')

    def test_NorthCarolina2_2(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00008.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.ticket_type, 'RUSH 2NDR GRID LR')
        ae(t.kind, 'EMERGENCY')
        ae(t.revision, '1')

    def test_NorthCarolina2_3(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00009.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.ticket_type, 'RUSH 2NDR PLCE LR')
        ae(t.kind, 'EMERGENCY')
        ae(t.revision, '1')

    def test_NorthCarolina2_4(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00010.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.ticket_type, 'SHRT NEW GRID LR')
        ae(t.kind, 'NORMAL')
        ae(t.revision, '0')

    def test_NorthCarolina2_5(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00011.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.ticket_type, 'RUSH 2NDR GRID LR')
        ae(t.kind, 'EMERGENCY')
        ae(t.revision, '2')

    def test_NorthCarolina2_6(self):
        '''
        Mantis 2298: new ticket feed for customer
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-19-00012.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina2"])
        ae = self.assertEquals
        ae(t.ticket_type, 'NORM RXMT GRID LR')
        ae(t.kind, 'NORMAL')
        ae(t.revision, '1')

    def test_SC1423_map_pages(self):
        '''
        Mantis 2391: 2391: Too many unassigned tickets for SC
        Is the map_page calculated correctly?
        '''

        tickets = glob(os.path.join("..", "testdata", "sample_tickets", "1422",
                  "1422-*.txt"))
        lats = []
        longs = []
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["SC1422"])
            if isinstance(t, ticketmod.Ticket):
                self.assertTrue(t.map_page != '')
                self.assertTrue(t.work_long < 0.0)
                lats.append(float(t.work_lat))
                longs.append(float(t.work_long))
        tickets = glob(os.path.join("..", "testdata", "sample_tickets", "1423",
                  "1423-*.txt"))
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["SC1423"])
            if isinstance(t, ticketmod.Ticket):
                self.assertTrue(t.map_page != '')
                self.assertTrue(t.work_long < 0.0)
                lats.append(float(t.work_lat))
                longs.append(float(t.work_long))
        self.assertTrue(min(lats) > 32.0 and max(lats) < 35.0)
        self.assertTrue(min(longs) > -82.0 and max(longs) < -79.0)

    def test_FMW1_1(self):
        '''
        Mantis 2393: Parser using first value for lat/long and not the centroid
        '''

        tickets = glob(os.path.join("..", "testdata", "sample_tickets", "FMW1",
                  "fmw1-2009-10-14*.txt"))
        lats = []
        longs = []
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["Washington"])
            if isinstance(t, ticketmod.Ticket):
                self.assertTrue(t.map_page != '')
                self.assertTrue(t.work_long < 0.0)
                lats.append(float(t.work_lat))
                longs.append(float(t.work_long))
        self.assertTrue(min(lats) > 38.0 and max(lats) < 40.0)
        self.assertTrue(min(longs) > -78.0 and max(longs) < -76.0)

    # XXX move to bulk parsing/testing
    def test_FMW4(self):
        tickets = glob(os.path.join(_testprep.TICKET_PATH, 'sample_tickets',
                                    'FMW4', '*.txt'))
        for ticket_file in tickets:
            # just make sure it parses, should be the same as FMW1/Washington
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["Washington4"])
            assert isinstance(t, ticketmod.Ticket)

    def test_NewJersey2010(self):
        # most of this test has been moved to tickets/NewJersey2010.txt
        # (testing DSL); only a corner case is left
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'tickets', 'NewJersey-2010-02-15', 'TEST-2010-01-08-00003.txt'))
        raw = tl.tickets[0]
        ae = self.assertEquals

        # test with CDC = XXX
        raw = raw.replace('CDC = ADC', 'CDC = XXX')
        t = self.handler.parse(raw, ["NewJersey2010"])
        ae(len(t.locates), 30)
        ae(t.locates[0].client_code, 'XXX')
        ae(t.locates[1].client_code, 'ADC')

    def test_GA3003_work_description(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'tickets', '3003-2010-01-26', '3003-2010-01-26-01511.txt'))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3003"])

        self.assertEquals(t.work_description, "LOC ENTIRE FRONT OF PROPERTY"
          " *** PLEASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN"
          " SYSTEM. UTILITIES, PLEASE RESPOND TO POSITIVE RESPONSEVIA"
          " HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE"
          " CHECK THE STATUS OF YOUR TICKET VIA THE SAME METHODS. ***")

    def test_GA3004_work_description(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'tickets', '3004-2010-01-26', '3004-2010-01-26-01447.txt'))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["GA3004"])

        self.assertEquals(t.work_description, "LOCATE STARTING IN THE NW"\
          " CORNER OR THE SOUTHBOUND RAMP OF I85 AND BOGGS RD. CONTINUE"\
          " DOWN TO I85 - CALL ROSS FOR DETAIL INSTRUCTIONS @ (404)"\
          "557-2268")

    def test_FAU4_work_description(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'tickets', 'FAU4-2010-01-26', 'FAU4-2010-01-26-00610.txt'))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta4"])

        self.assertEquals(t.work_description, "LOC AROUND AND INSIDE OF THE"\
          " WHITE PAINTED CIRCLES ON BOTH SIDES OF THEPROPERTY *** "\
          "PLEASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM."\
          " UTILITIES, PLEASE RESPOND TO POSITIVE RESPONSE VIA HTTP://"\
          "EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE CHECK"\
          " THE STATUS OF YOUR TICKET VIA THE SAME METHODS. ***")

    def test_FAU5_work_description(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'tickets', 'FAU5-2010-01-26', 'FAU5-2010-01-26-00307.txt'))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta5"])

        self.assertEquals(t.work_description, "LOC AROUND AND INSIDE OF THE"\
          " WHITE PAINTED CIRCLES ON BOTH SIDES OF THEPROPERTY *** PLEASE"\
          " NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. "\
          "UTILITIES, PLEASE RESPOND TO POSITIVE RESPONSE VIA HTTP://"\
          "EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE CHECK THE"\
          " STATUS OF YOUR TICKET VIA THE SAME METHODS. ***")

    def test_NC811(self, format='NC811', call_center='FCL1'):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NC811-2010-05-04", "NCOC811-2010-05-03-00011.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, [format])
        ae = self.assertEquals

        # XXX need better sample tickets

        # ticket has bogus data, so these assertions are kind of strange:
        ae(t.ticket_format, call_center)
        ae(t.ticket_number, "A123456789")
        ae(t.ticket_type, "PPPP TTTT LLLL CCCC RSND")
        ae(t.kind, "NORMAL")
        ae(t.call_date, "2010-05-03 12:02:00")
        ae(t.operator, "12345678901234567890")
        ae(t.channel, "123")
        ae(t.revision, "12A")

        ae(t.work_state, "12")
        ae(t.work_county, "123456789012")
        ae(t.work_city, "1234567890123456789012345678901234567890")
        ae(t.work_subdivision, "1234567890123456789012345678901234567890")

        ae(t.work_address_number, "")
        ae(t.work_address_street, "12345678901 12 123456789012345678901234567890 1234 12")
        ae(t.work_cross, "12 123456789012345678901234567890 1234 12")
        ae(t.work_description.startswith("THIS FIELD DOES NOT"), 1)
        ae(t.work_description.endswith("THE CHICKEN COOP."), 1)

        ae(t.work_type, "123456789012345678901234567890123456789012345"\
           + "67890123456789012345678901234567890")
        ae(t.work_date, "2010-05-05 09:40:00")
        ae(t.work_notc, "123456/123456")
        ae(t.priority, "1234")
        ae(t.duration, "1234567890")
        ae(t.company, "1234567890123456789012345678901234567890")

        ae(t.con_name, "1234567890123456789012345678901234567890")
        ae(t.con_address, "1234567890123456789012345678901234567890")
        ae(t.con_city, "123456789012345678901234567890")
        ae(t.con_state, "12")
        ae(t.con_zip, "12345-6789")
        ae(t.caller, "123456789012345678901234567890")
        ae(t.caller_phone, "123-456-7890 Ext: 123456789012345")

        #ae(t.work_remarks, "--SEE ORIGINAL TICKET#: A0025484-00A")
        ae(t.transmit_date, "2010-05-03 16:28:40") # top of ticket
        ae(len(t.locates), 25)
        ae(t.locates[0].client_code, "MBR001")
        ae(t.locates[-1].client_code, "MBR457")
        self.assertEquals(t._dup_fields, [])

        #ae(t.work_lat, 0.0)
        #ae(t.work_long, 0.0)

        # test a real ticket

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FCT3-2010-05-19", "FCT3-2010-05-19-00023.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, [format])

        ae(t.ticket_number, "A101390407")
        ae(t.ticket_format, call_center)
        ae(t.ticket_type, "NORM NEW GRID MOWR")
        ae(t.kind, "NORMAL")
        ae(t.call_date, "2010-05-19 08:58:00")
        ae(t.operator, "SSB")
        ae(t.channel, "777")
        ae(t.revision, "00A")

        ae(t.work_state, "NC")
        ae(t.work_county, "CHEROKEE")
        ae(t.work_city, "MURPHY")
        ae(t.work_subdivision, "")

        ae(t.work_address_number, "")
        ae(t.work_address_street, "SR1157")
        ae(t.work_cross, "HWY294")
        ae(t.work_description.startswith("ST: GUY ELLER RD"), 1)
        ae(t.work_description.endswith("TO CROSS STREET"), 1)

        ae(t.work_type, "MOWING")
        ae(t.work_date, "2010-05-24 00:01:00")
        ae(t.work_notc, "48/48")
        ae(t.priority, "NORM")
        ae(t.duration, "UNK")
        ae(t.company, "SAME")

        ae(t.con_name, "NCDOT")
        ae(t.con_address, "5426 HWY141")
        ae(t.con_city, "MARBLE")
        ae(t.con_state, "NC")
        ae(t.con_zip, "28905")
        ae(t.caller, "NANCY WOODARD-NO EMAIL")
        ae(t.caller_phone, "828-837-2742")

        #ae(t.work_remarks, "--SEE ORIGINAL TICKET#: A0025484-00A")
        ae(t.transmit_date, "2010-05-19 08:58:19") # top of ticket
        ae(len(t.locates), 2)
        ae(t.locates[0].client_code, "BEC03")
        ae(t.locates[1].client_code, "SCB01")
        self.assertEquals(t._dup_fields, [])

    def test_TN811(self):
        self.test_NC811(format="TN811", call_center="FCT3")

    def test_GA3004_alert(self):
        # Mantis #2845.
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3004-2011-08-agl", "TEST-2011-08-03-00004.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["GA3004"])

        # there are 10 locates on this ticket, but only the AGL one should
        # have an alert set
        self.assertEquals(len(t.locates), 10)
        for loc in t.locates:
            if loc.client_code.startswith('AGL'):
                self.assertEquals(loc.alert, 'A')
            else:
                self.assertEquals(loc.alert, None)
        self.assertEquals(t.alert, 'A')

    def test_GA3004_hp_info(self):
        # Mantis #2869
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3004-2011-08-agl", "TEST-2011-08-03-00005.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["GA3004"])

        self.assertEquals(len(t._hp_info), 29)
        self.assertEquals(t._hp_info[0], 'AGL108-HP : Bare Steel    24-300-S')

    def test_NCA2_hp_info(self):
        # Mantis #3134
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NCA2-2012-10-17-hp",
             "nca2-2012-10-04-22-26-03-145-SXIW8.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NorthCaliforniaATT"])
        self.assertEquals(len(t._hp_info), 1)
        self.assertEquals(t._hp_info[0], "PACBEL: CONDUIT")

    def test_SCA6_hp_info(self):
        # Mantis #3134
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "SCA6-2012-10-17-hp",
             "sca6-2012-10-05-21-06-08-240-GJTD2.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SouthCaliforniaATT"])
        self.assertEquals(len(t._hp_info), 1)
        self.assertEquals(t._hp_info[0], "ATTDSOUTH: CONDUIT,FIBER,FIBER LANDMARK")

    def test_WV1181Korterra(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1181-2011-11-28", "1182-2011-11-29-00275.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WV1181Korterra"])

        ae = self.assertEquals

        ae(t.ticket_number, "1133300460")
        ae(t.ticket_type, "Normal")
        ae(t.serial_number, "1133300460") # used to have WV prefix (#2927)
        ae(t.transmit_date, "2011-11-29 11:03:00")
        ae(t.call_date, "2011-11-29 08:32:00")
        ae(t.operator, '')
        ae(t.channel, 'Email')
        ae(t.con_name, 'ALL-LINK COMMUNICATIONS LLC')
        ae(t.con_address, 'P.O BOX 209')
        ae(t.con_city, 'RUCKERSVILLE')
        ae(t.con_state, 'VA')
        ae(t.con_zip, '22968')
        ae(t.caller, 'SANDRA KINDER')
        ae(t.caller_contact, 'CORINNA REILLY')
        ae(t.caller_phone, '(434) 985-4933')
        ae(t.caller_email, 'ZONE2LOCATES@ALLLINKCOMM.COM')
        ae(t.caller_fax, '(434) 465-6850')
        ae(t.company, 'COMCAST')
        ae(t.work_date, '2011-12-01 08:45:00')
        ae(t.work_type, '')
        ae(t.work_state, 'WV')
        ae(t.work_county, 'Berkeley')
        ae(t.work_city, 'MARTINSBURG')
        ae(t.work_address_number, '')
        ae(t.work_address_street, 'SOPWITH WAY')
        ae(t.work_cross, 'SHASTA LN')
        ae(t.work_remarks[:17], 'Lat/Long: 3944223')
        ae(t.explosives, 'No')

        self.assertAlmostEqual(t.work_lat, 39.442413, 5)
        self.assertAlmostEqual(t.work_long, -77.966642, 5)

        ae(len(t.locates), 6) # actually 7, but BKC appears twice
        ae(t.locates[0].client_code, 'BKC')
        ae(t.locates[-1].client_code, 'WCM')

        # let's try another ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1181-2011-11-28", "1182-2011-11-29-00278.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WV1181Korterra"])

        ae(t.work_remarks[:10], "WORKING ON")
        ae(t.work_remarks[-9:], "OF HOUSE.")
        ae(t.serial_number, "1133300454")

        # Mantis #2893: add serial number
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1181-2011-11-28", "1182-2011-11-29-00286-serial.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WV1181Korterra"])

        ae(t.ticket_number, "1133300459")
        ae(t.serial_number, "1133300459A")

        # another ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1181-2011-11-28", "TEST-2011-12-14-00102.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["WV1181Korterra", "WV1181", "WV1181Alt"])

        ae(t.ticket_format, '1181')
        ae(t.ticket_number, "113220052")
        ae(t.serial_number, "113220052A")

    def test_Baltimore2_serial(self):
        # Mantis #2893: add serial number
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FMB2-2011-04-22", "FMB2-2011-04-21-00224.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Baltimore2"])
        ae = self.assertEquals

        ae(t.ticket_number, "11200349")
        ae(t.serial_number, "11200349") # used to have MD prefix (#2927)

    def test_NewJersey_bogus_date(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NewJersey-2013-07-16-bogus-date",
             "NewJersey-2013-07-16-10-11-13-480-F0BVH.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["NewJersey2010"])

        self.assertEquals(t.legal_good_thru, "2069-12-31 00:00:00")

        # make sure the ticket can be stored and retrieved in spite of the
        # bogus date
        id = self.tdb.insertticket(t)
        rows = self.tdb.getrecords("ticket", ticket_id=id)

    def test_Atlanta_caller_fax(self):
        # Mantis #3379: fax number must be valid
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "Atlanta-2013-08-08-fax", 
             "Atlanta-2013-08-08-0001.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta4"])

        self.assertEquals(t.caller_fax, None)
        self.assertEquals(t.ticket_number, "07313-241-012")

        raw = raw.replace("Fax     :  --", "Fax    : 123-456")
        t = self.handler.parse(raw, ["Atlanta4"])
        self.assertEquals(t.caller_fax, None)

        raw = raw.replace("123-456", "123-456-7890")
        t = self.handler.parse(raw, ["Atlanta4"])
        self.assertEquals(t.caller_fax, "123-456-7890")

    def test_1421_emergency(self):
        # QMAN-3652
        # Test Emergency Tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1421-2014-08-emergency", 
             "1421-2014-08-04-09-15-29-740-LEBSN.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SC1421A"])

        self.assertEquals(t.ticket_type, 'Emergency')
        self.assertEquals(t.kind, 'EMERGENCY')

        # Test Normal Tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1421-2014-02-pups",
             "1421-2014-01-14-08-23-14-490-7N2P3.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SC1421A"])

        self.assertEquals(t.ticket_type, 'Normal')
        self.assertEqual(t.kind, 'NORMAL')

        # Test Cancel Tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1421-2014-02-pups",
             "1421-2014-01-14-08-56-14-605-L4QR5.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["SC1421A"])

        self.assertEquals(t.ticket_type, 'Cancel')
        self.assertEqual(t.kind, 'NORMAL')



if __name__ == "__main__":
    unittest.main()


