# test_ticketloader.py

import os
import site; site.addsitedir('.')
import sys
import unittest
#
import filetools
import ticketparser
import ticketloader
import _testprep

class TicketLoaderTest(unittest.TestCase):
    fileName = 'test.txt'

    def tearDown(self):
        filetools.remove_file("foobar.txt")
        filetools.remove_file("test.txt")

    def writeFile(self, text) :
        file = open(self.fileName, 'w')
        file.write(text);
        file.close()

    def testEmptyTicket(self):
        self.writeFile(chr(12) + "\n\n" )
        tl = ticketloader.TicketLoader(self.fileName)
        self.assertEquals(None, tl.getTicket())

    def testSimpleFile(self):
        self.writeFile(
         "a\nb\nc\nd\n\x0c"\
         "1\n2\n3\n4\n\x0c"\
         "A\nB\nC\nD\n")
        self.checkSimpleFile(chr(12))

    def testSimpleFileControlL(self):
        self.writeFile(
         "a\nb\nc\nd\n\f"\
         "1\n2\n3\n4\n\f"\
         "A\nB\nC\nD\n")
        self.checkSimpleFile(ticketparser.controlL)

    def testSimpleFile2(self):
        # Separator should not necessarily be on a new line.
        self.writeFile(
         "a\nb\nc\nd\x0c"\
         "1\n2\n3\n4\x0c"\
         "A\nB\nC\nD\n")
        self.checkSimpleFile(chr(12))

    def checkSimpleFile(self, separator):
        tl = ticketloader.TicketLoader(self.fileName, separator)
        self.assertEquals("a\nb\nc\nd", tl.getTicket())
        self.assertEquals("1\n2\n3\n4", tl.getTicket())
        self.assertEquals("A\nB\nC\nD", tl.getTicket())
        self.assertEquals(None, tl.getTicket())

    def testFileWithOneTicket(self):
        """ Files without a separator should be treated as one ticket. """
        text = "This is a ticket. It is all alone\nOn a few lines of text"\
         "without any separators\nNice, eh?\nbla\nbla"  # > 3 lines
        self.writeFile(text)
        tl = ticketloader.TicketLoader(self.fileName)
        self.assertEquals(text, tl.getTicket())
        self.assertEquals(None, tl.getTicket())

    def testCrudRemoval(self):
        """ Test if funny characters are actually removed from the raw
            ticket by the TicketLoader. """
        # get any old ticket
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        # insert strange characters
        raw = "\x03" + raw
        raw = raw[:20] + "\x08" + raw[20:]
        # write to file
        f = open("foobar.txt", "w")
        f.write(raw)
        f.close()
        # now get the ticket from that file with TicketLoader
        tl = ticketloader.TicketLoader("foobar.txt")
        raw2 = tl.getTicket()
        # funny characters should be removed
        self.assert_(len(raw) > len(raw2))
        self.assert_("\x03" not in raw2)
        self.assert_("\x08" not in raw2)

    def testLineNoiseRemoval(self):
        bogus = ("foo!\n" * 20   # long enough for a ticket
         + chr(12)
         + "bar!\n" * 20
         + chr(12)
         + "too\nshort\n"
         + chr(12)
         + "baz!\n" * 20
         + chr(12)
        )
        f = open("foobar.txt", "w")
        f.write(bogus)
        f.close()

        tl = ticketloader.TicketLoader("foobar.txt", chr(12))
        raw1 = tl.getTicket()
        self.assert_(raw1.startswith("foo"))

    def testLineNoiseRemoval2(self):
        # should also work for one-line noisy tickets
        raw = "some bogus text here"
        f = open("foobar.txt", "w")
        f.write(raw)
        f.close()
        tl = ticketloader.TicketLoader("foobar.txt", chr(12))
        raw1 = tl.getTicket()
        self.assertEquals(raw1, None)

    def test_ticketloader_email(self):
        tl = ticketloader.TicketLoader("../testdata/email-1.txt")
        self.assertEquals(len(tl.tickets), 2)
        ticket1 = tl.tickets[0]
        self.assertEquals(ticket1.startswith("ticket one"), 1)

    def test_ctrl_c(self):
        """ Test Ctrl-C as separator. """
        tl = ticketloader.TicketLoader("../testdata/fmw2-nosep.txt")
        self.assertEquals(len(tl.tickets), 15)

    def test_ctrl_e(self):
        """ Test Ctrl-E as separator. """
        tl = ticketloader.TicketLoader("../testdata/Oregon2-2.txt")
        self.assertEquals(len(tl.tickets), 3)

    def test_findfirst(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        try:
            raw = tl.findfirst("GAUPC") # the very first ticket should match
        except ValueError:
            self.fail("findfirst() should have matched")

        try:
            raw = tl.findfirst("02244-061-060")
        except ValueError:
            self.fail("findfirst() should have matched")

    def test_remove_garbage(self):
        tl = ticketloader.TicketLoader("../testdata/qwest-woc-ringring.txt")
        # this is a ticket containing a long section with "NO CARRIER" and
        # "RING RING"
        raw = tl.tickets[0]
        self.assertEquals(raw.count("RING"), 0)
        self.assertEquals(raw.count("NO CARRIER"), 0)

    def test_consolidated(self):
        tl = ticketloader.TicketLoader("../testdata/fcl1-consolidated.txt")
        self.assertEquals(len(tl.tickets), 2)

        tl = ticketloader.TicketLoader("../testdata/fcl1-consolidated-2.txt")
        self.assertEquals(len(tl.tickets), 3)

    def test_multiple_tildes(self):
        tl = ticketloader.TicketLoader("../testdata/embedded-tildes.txt")
        raw = tl.tickets[0]
        self.assertEquals(raw.count("PLEASE MARK THE"), 1)
        self.assertEquals(raw.count("~~~~~"), 2)
        self.assertEquals(raw.count("Seq No:   2080 A"), 1)

    def test_whitespace_conversion(self):
        """ Mantis #2570: Some tickets contain the character \xA0, which should
            not be removed, but rather converted to whitespace. """
        path = os.path.join(_testprep.TICKET_PATH,
               "tickets", "OCC3-2010-05-18", "OCC3-2010-05-13-00232.txt")
        tl = ticketloader.TicketLoader(path)
        raw = tl.tickets[0]
        self.assertTrue("\xa0" not in raw)


if __name__ == "__main__":
    unittest.main()
