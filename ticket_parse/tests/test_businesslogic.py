# test_businesslogic.py
#
# 2010-10-01  hn  Due date tests moved to test_businesslogic_due_dates.py.

from __future__ import with_statement
import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import businesslogic
import call_centers
import config
import locatorcatalog
import ticket_db
import ticketloader
import ticketparser
import tools
#
from mockery import *

class TestBusinessLogic(unittest.TestCase):

    will_fail = False

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

    def test_000_prepare(self):
        _testprep.set_cc_routing_file()

    def test_FCO1(self):
        """ Test FCO business logic """
        logic = businesslogic.getbusinesslogic("FCO1", self.tdb, [])

        # normal ticket, transmit_date + midnight 48 hrs is later
        t = {"ticket_type": "BLA", "transmit_date": "2002-09-09 14:15:16",
             "ticket_format": "FCO1",
             "work_date": "2002-09-11 13:00:00",
             "call_date": "2002-09-09 14:15:16", "kind": "NORMAL",
             "company": "DOOKEY"}
        self.assertEquals(logic.legal_due_date(t), "2002-09-11 23:59:59")

        # normal ticket, work_date is later
        t["work_date"] = "2002-09-12 13:00:00"
        self.assertEquals(logic.legal_due_date(t), t["work_date"])

        # meet ticket
        t["ticket_type"] = "LET'S MEET"
        self.assertEquals(logic.is_meet_ticket(t), 1)
        self.assertEquals(logic.legal_due_date(t), "2002-09-12 23:59:59")

        # emergency ticket
        t["kind"] = "EMERGENCY"
        self.assertEquals(logic.legal_due_date(t), "2002-09-09 16:15:16")

    def test_FCO1_locator(self):
        """ Test retrieval of FCO1 locator """

        # this is a tricky one to make independent from database data...
        # if we change the township, use_routing_table_NS doesn't work
        # anymore... apparently it uses the township to determine whether
        # the area is north or south.

        # HACKS to make this test work again, for now
        self.tdb.dbado.runsql('update employee set active=1, can_receive_tickets=1 where emp_id=1366')
        self.tdb.dbado.runsql('update employee set active=1, can_receive_tickets=1 where emp_id=1504')
        # This really needs to be replaced with something more robust, but
        # it's not that easy, since the areas are hardcoded.

        #area_row = self.tdb.getrecords("area", area_id=1420)[0]
        #emp_id = area_row['locator_id']
        #self.tdb.dbado.runsql('update employee set active=1, can_receive_tickets=1 where emp_id=%s' % (emp_id,))

        # add some test data
        self.tdb.delete("map_trs", township='02S', range='068W', section='19',
         basecode='CO')
        emp_id = _testprep.add_test_employee(self.tdb, 'FCO1-001')
        map_id = _testprep.add_test_map(self.tdb, 'FCO1-TESTMAP', 'CO',
                 'NONE', 'CO')
        area_id = _testprep.add_test_area(self.tdb, 'ND-FCO1-TEST', emp_id, map_id)
        _testprep.add_test_township(self.tdb, '02S', '068W', '19', area_id,
         'CO')

        logic = businesslogic.getbusinesslogic("FCO1", self.tdb, [])
        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "02S068W19SW", "company": "AZAZEL",
             "ticket_format": "FCO1", "work_state": "CO"}
        routingresult = logic.locator(t)
        self.assertEquals(routingresult.area_id, logic.REBUILD_ND)
        self.assertEquals(routingresult.locator,
         self.tdb.get_locator_by_area(logic.REBUILD_ND))

        t["company"] = "SPECIAL PROJECTS"
        routingresult = logic.locator(t)
        self.assertEquals(routingresult.locator,
         self.tdb.get_locator_by_area(1420))

        #t["work_type"] = t["company"] = "DRIZZT"
        #routingresult = logic.locator(t)
        #self.assertEquals(routingresult.locator,
        # self.tdb.get_locator_by_area(1345))
        # this kind of routing depends on the FCO1BusinessLogic.routing_data
        # list, and is now obsolete.

        # route by township
        t["con_name"] = t['company'] = "ELMINSTER"
        rows = self.tdb.runsql_result("exec find_township '02S', '068W', '19'")
        self.assert_(rows)
        #area_id = rows[0]['area_id']
        # area name starts with ND, therefore:
        routingresult = logic.locator(t)
        from callcenters import FCO1
        self.assertEquals(routingresult.area_id,
                          FCO1.BusinessLogic.REBUILD_ND)

    test_FCO1_locator.will_fail = True # svn 9187 update to db test data breaks this

    def test_routinglist_1(self):
        """ Test routing list (1) """
        # test routinglist on a higher level than test_routinglist.py.
        self.prepare_test_routinglist_1()
        logic = businesslogic.getbusinesslogic("FCL1", self.tdb, [])
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","NorthCarolina-1.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])
        t.work_address_street = "WATER MOSS"

        rr = logic.locator(t)
        self.assertEquals(rr.area_id, "685")
        self.assertEquals(rr.routing_method, "BusinessLogic.route_by_routinglist")
        self.assert_(rr.locator)

    def prepare_test_routinglist_1(self):
        # insert the record(s) we need to make the routinglist work
        rows = self.tdb.getrecords("routing_list", call_center="FCL1")
        if not rows:
            sql = """
             insert routing_list (call_center, field_name, field_value, area_id)
             values ('FCL1', 'work_address_street', 'WATER MOSS', 102)
            """
            self.tdb.dbado.runsql(sql)

    def test_Atlanta_special_routing(self):
        """ Test special routing Atlanta """
        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])
        row = {"ticket_type": "NORMAL", "kind": "NORMAL", "work_type": "BSW",
               "company": "BELLSOUTH", "client_code": "BGAWA",
               "ticket_format": "Atlanta",
               "work_city": "VALDOSTA"}
        #rr = logic.locator(row)
        #self.assertEquals(rr.area_id, logic.AREA_BELLSOUTH)
        # 2011-12-26 hn this area no longer exists in the ref data.

        row["client_code"] = "ATL01"
        rr = logic.locator(row)
        self.assertEquals(rr.area_id, logic.AREA_SCREENS)

    # FIXME: testing logging output is brittle
    def test_FDX1_raise_exception(self):
        """
        Test a locator method that raises an exception
        """
        with Mockery(businesslogic.traceback, 'print_exc', lambda: None):

            import StringIO, logging, new

            log_str = StringIO.StringIO()
            log_debug = logging.getLogger("ticketrouter")
            log_debug.setLevel(logging.DEBUG)
            handler = logging.StreamHandler(log_str)
            form = logging.Formatter("%(levelname)s %(name)s %(asctime)s %(message)s")
            handler.setFormatter(form)
            log_debug.addHandler(handler)
            log_debug.debug("Debug logging enabled")

            logic = businesslogic.getbusinesslogic("FDX1", self.tdb, [])
            def test_raise(self, row):
                raise Exception
            def new_locator_methods(self,row):
                return [self.test_raise,]
            logic.test_raise = new.instancemethod(test_raise, logic,
             type(logic))
            logic.locator_methods = new.instancemethod(new_locator_methods,
             logic, type(logic))

            row = {"ticket_type": "NORMAL", "kind": "NORMAL",
                   "work_type": "BSW",
                   "company": "BELLSOUTH", "client_code": "D20",
                   "ticket_format": "FDX1",
                   "work_city": "VALDOSTA", 'map_page': 'blah',
                   "work_county": "some_county"}
            rr = logic.locator(row)
            log_out = log_str.getvalue()
            self.assertNotEquals(
             log_out.find("ERROR ticketrouter.businesslogic"), -1)
            self.assertNotEquals(log_out.find("routingresult = f(row)"), -1)
            self.assertNotEquals(log_out.find("Exception"), -1)

    def test_cc_routing_list(self):
        """ Test if cc_routing_list.tsv is read. """
        logic = businesslogic.getbusinesslogic("NCA1", self.tdb, [])
        self.assert_(logic.cc_routing_list)

    def test_cc_routing(self):
        # make sure the locator is active before testing
        try:
            self.tdb.runsql("update area set locator_id=2108 where area_id=3637")
            self.tdb.runsql("update employee set active=1, can_receive_tickets=1 where emp_id = 2108")
        except:
            pass

        businesslogic.BusinessLogic.cc_routing_list = None
        config.getConfiguration().cc_routing_file = os.path.join("..","rules","test_cc_routing_list.tsv")

        logic = businesslogic.getbusinesslogic("NCA1", self.tdb, [])
        row = {
         "ticket_number": "123", "ticket_type": "BOZO",
         "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
         "kind": "NORMAL", "map_page": "MAPSCO 1,A", "company": "AZAZEL",
         "work_state": "TX", "work_county": "SAN JOAQUIN",
         "work_city": "TRACY", "client_code": "PBTHAN",
         "ticket_format": "NCA1",
        }

        rr = logic.locator(row)
        self.assertEquals(rr.area_id, "3637")
        self.assertEquals(rr.locator, "2108")

        row["work_county"] = "AMADOR"
        row["work_city"] = "POOPSIEDOODLE"
        rr = logic.locator(row)
        self.assertEquals(rr.area_id, "3637")
        self.assertEquals(rr.locator, "2108")

        rr = logic.locator({'ticket_format': 'XYZZY'})
        self.assert_(isinstance(rr, tools.RoutingResult))
        self.assert_(rr.area_id is None)
        self.assert_(rr.locator == logic.DEFAULT_LOCATOR)

    def before_Delaware_routing(self):
        # add the necessary test data...
        emp_id = _testprep.add_test_employee(self.tdb, 'test_Delaware')
        area_id = _testprep.add_test_area(self.tdb, 'test_Delaware', emp_id)
        map_id = _testprep.add_test_map(self.tdb, 'test_Delaware', 'DE', 'KENT',
                 'FDE1-ZZYXX')
        _testprep.add_test_map_page(self.tdb, map_id, 1)
        _testprep.add_test_map_cell(self.tdb, map_id, 1, 'A', 1, area_id)

        # add stuff for "double-alpha grid"
        mapid2 = _testprep.add_test_map(self.tdb, 'test_Delaware_2', 'DE',
                 'NEWC', 'FDE1-ABCDE')
        _testprep.add_test_map_page(self.tdb, mapid2, 6)
        _testprep.add_test_map_cell(self.tdb, mapid2, 6, 'DD', 6, area_id)

        return (emp_id, area_id, map_id)

    def test_Delaware_routing(self):
        emp_id, area_id, map_id = self.before_Delaware_routing()

        # this calls locatorcatalog.map_area_with_code with "FDE1-" prepended
        # to the code...
        logic = businesslogic.getbusinesslogic("FDE1", self.tdb, [])

        # get a Delaware ticket
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Delaware1-1.txt"))
        raw = tl.getTicket()
        # XXX replace map info with our own
        raw = raw.replace("Map: KENTD Page: 006   Grid Cells: J12",
                          "Map: ZZYXX Page: 001   Grid Cells: A01")

        t = self.handler.parse(raw, ["Delaware1"])

        rr = logic.route_map_page(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # or even...
        rr = logic.locator(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # test another, with "double-alpha grids" (like DD06)
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","fde1-new.txt"))
        raw = tl.tickets[2]
        # XXX replace map info with our own
        raw = raw.replace("Map: NEWC  Page: 6     Grid Cells: C06",
                          "Map: ABCDE Page: 6     Grid Cells: DD06")
        t = self.handler.parse(raw, ['Delaware1'])

        rr = logic.route_map_page(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # or even...
        rr = logic.locator(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

    def test_FHL2_routing(self):
        # add the necessary test data...
        emp_id = _testprep.add_test_employee(self.tdb, 'test_FHL2')
        area_id = _testprep.add_test_area(self.tdb, 'test_FHL2', emp_id)
        map_id = _testprep.add_test_map(self.tdb, 'test_FHL2', 'TX', 'DALLAS','FHL2')
        _testprep.add_test_map_page(self.tdb, map_id, 577)
        _testprep.add_test_map_cell(self.tdb, map_id, 577, 'K', 1, area_id)

        # check if find_map_area_locator finds this locator/area
        rows = self.tdb.find_map_area_locator('FHL2', 577, 'K', 1)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['area_id'], area_id)
        self.assertEquals(rows[0]['emp_id'], emp_id)

        logic = businesslogic.getbusinesslogic("FHL2", self.tdb, [])
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Dallas1-1.txt"))
        raw = tl.tickets[0]
        raw = raw.replace("MAPSCO 70,N", "MAPSCO 577,K")
        t = self.handler.parse(raw, ['Houston3'])
        self.assertEquals(t.map_page, 'MAPSCO 577,K')

        rr = logic.locator(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 577, 'K', 1, area_id)
        _testprep.delete_test_map_page(self.tdb, map_id, 577)
        _testprep.delete_test_map(self.tdb, 'test_FHL2', 'TX', 'FHL2', 'FHL2')
        _testprep.delete_test_area(self.tdb, 'test_FHL2', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_FHL2')

    def test_LOR1_routing(self):
        emp_id = _testprep.add_test_employee(self.tdb, 'test_LOR1')
        area_id = _testprep.add_test_area(self.tdb, 'test_LOR1', emp_id)
        _testprep.add_test_township(self.tdb, '18S', '03W', '19', area_id, 'ZX')

        logic = businesslogic.getbusinesslogic("LOR1", self.tdb, [])
        t = {
            "ticket_number": "12345",
            "ticket_format": "LOR1",
            "work_state": "ZX",
            "map_page": "18S3W19NE",
            "ticket_type": "NORMAL",
            "kind": "NORMAL",
        }
        rr = logic.locator(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # test another...
        _testprep.add_test_township(self.tdb, "05S", "02E", "05", area_id, "ZX")
        t['map_page'] = "5S2E5" # note the lack of "direction" in the section
        rr = logic.locator(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        _testprep.add_test_employee(self.tdb, 'test_LOR1')

    def test_blasting_is_ticket_ack(self):
        for callcenter in ['OCC2', 'FCV3', 'FPK1', 'FDE2']:
            logic = businesslogic.getbusinesslogic(callcenter, self.tdb, [])
            row = {
             'ticket_format': callcenter,
             'ticket_type': 'NORMAL',
             'kind': 'WHOKNOWS',
             'explosives': 'Y',
            }
            a = logic.is_ticket_ack(row)
            self.assertEquals(a, True)

        row['explosives'] = 'N'
        a = logic.is_ticket_ack(row)
        self.assertEquals(a, False)

    def test_VUPS_Meet(self):
        #for callcenter in ['FCV3', 'OCC2', 'FPK1', 'FDE2']:
        for callcenter in ['FCV3', 'FPK1']:
            logic = businesslogic.getbusinesslogic(callcenter, self.tdb, [])
            format = ['VUPSNew'+callcenter]
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","VUPSNew","FCV3-2008-04-04-00039.txt"))
            raw = tl.tickets[0]
            t = self.handler.parse(raw, format)
            a = logic.legal_due_date(t)
            self.assertEquals(a,'2008-04-08 10:00:00')

    def test_occ2_meet(self):
        '''
        Test that occ2 meet tickets have the (meet date) legal due date set to the work date
        '''
        callcenter = 'OCC2'
        logic = businesslogic.getbusinesslogic(callcenter, self.tdb, [])
        format = ['VUPSNew'+callcenter]
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","occ2-2008-05-23-00295.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, format)
        a = logic.legal_due_date(t)
        #self.assertEquals(a,'2008-05-27 14:00:00')
        self.assertEquals(a, '2008-05-29 07:00:00')
        # due date rule changed as of Mantis #2739

    # generic tester for is_ticket_ack
    def test_ticket_ack(self):
        # 2004.01.15: Since this is not in code anymore, tests may break
        # when UQ changes the content of the ticket_ack_match table
        data = [
            # (call_center, ticket_type, is_ticket_ack)
            ("FDE1", "NONSENSE", 0),
            ("FDE1", "BLAH EMERGENCY BLAH", 1),
            ("FDE1", "BLAH CANCELLATION BLAH", 1),
            ("FWP1", "Cancellation of --> 12345", 1),
            ("FWP1", "DIG UP", 1),
            ("FWP1", "SOME NONSENSE", 0),
            ("FWP1", "EMERGENCY", 1),
            ("FWP1", "BLAH PRIORITY", 1),
            ("FWP1", "SHORT NOTICE", 1),
            ("FCL1", "BLAH 2NDR BLAH", 1),
            ("FTS1", "NON-COMPLIANT", 1),
            ("FTS1", "NO RESPONSE", 1),
            ("FTS1", "BLAH", 0),
            ("FPL1", "EMERGENCY AND STUFF", 1),
            ("FPL1", "BLA REXMIT GOO", 1),
            ("FDX1", "NO RESPONSE", 1),
            ("FDX1", "DOERAK", 0),
            #("FBL1", "2NDQ-2ND REQUEST", 1),
            ('FGV1', 'NO SHOW', 1),
            ('FGV1', 'BLAH NO SHOW BLAH', 1),
        ]

        for (call_center, ticket_type, is_ticket_ack) in data:
            logic = businesslogic.getbusinesslogic(call_center, self.tdb, [])
            row = {"ticket_type": ticket_type, "kind": "NORMAL",
                   "ticket_format": call_center}
            z = logic.is_ticket_ack(row)
            self.assertEquals(z, is_ticket_ack,
             "'%s' should %sbe ticket_ack (%s)" % (ticket_type,
             ["not ", ""][is_ticket_ack], call_center))

    def test_use_prerouting(self):
        self.tdb.delete("call_center", cc_code='HNS1')
        self.tdb.delete("call_center", cc_code='HNS2')

        _testprep.add_test_call_center(self.tdb, 'HNS1', use_prerouting=1)
        _testprep.add_test_call_center(self.tdb, 'HNS2', use_prerouting=0)

        # refresh call_centers.CallCenters, so we know about HNS1/HNS2
        cc = call_centers.get_call_centers(self.tdb)
        cc.reload()

        # get business logic object... doesn't matter which one
        class HNS1BusinessLogic(businesslogic.BusinessLogic):
            call_center = 'HNS1'

        hns1 = HNS1BusinessLogic(self.tdb, [])
        self.assertEquals(hns1.use_prerouting(), True)

        class HNS2BusinessLogic(businesslogic.BusinessLogic):
            call_center = 'HNS2'

        hns2 = HNS2BusinessLogic(self.tdb, [])
        self.assertEquals(hns2.use_prerouting(), False)

if __name__ == "__main__":
    unittest.main()
