# test_ticketextractor.py

import os
import shutil
import site; site.addsitedir('.')
import string
import unittest
import xml.etree.ElementTree as ET
#
import date
import filetools
import ticketextractor
import ticketextractor2
import ticketparser
import ticketloader
import tools
import _testprep

BEGIN = chr(2)
END = chr(3)

class TestTicketExtractor(unittest.TestCase):

    # do NOT rename to setUp(), it doesn't work. apparently the tests rely on
    # each other, which is bad design, but there you go.
    def test_000(self):
        # preparation of everything
        T = ticketextractor.TicketExtractor
        T.positions_file = "test_positions.txt"
        T.ids_file = "test_ids.txt"
        # make sure positions and ids files don't exist (yet)
        filetools.remove_file("test_positions.txt")
        filetools.remove_file("test_ids.txt")
        TestTicketExtractor.te = ticketextractor.TicketExtractor(chr(2), chr(3),
         verbose=0)

        self.te.target_dir = "test_target"
        #self.te.source_file_template = "../testdata/PortData_%s/Tics-01.txt"
        self.te.file_templates = [
         {"name": "../testdata/PortData_%s/Tics-04.txt", "position": "pos01"},
        ]

        TestTicketExtractor.date = date.Date().isodate()[:10]

        # make PortData directory
        fullname = self.te.get_filename(self.te.file_templates[0]["name"],
         self.date)
        path, filename = os.path.split(fullname)
        try:
            os.makedirs(path)
        except OSError:
            pass

    def write_to_tics(self, date, data):
        filename = self.te.get_filename(self.te.file_templates[0]["name"], date)
        f = open(filename, "a+")
        f.write(data)
        f.close()

    def write_base_tics(self, date):
        # write three tickets.
        self.write_to_tics(date, "\x02fubu\x03")
        self.write_to_tics(date, "\x02karakoram\x03")
        self.write_to_tics(date, "some nonsense between tickets")
        self.write_to_tics(date, "\x02tinisen\x03")

    def test_001(self):
        # write base Tics file and see if we indeed have 3 tickets.
        self.write_base_tics(self.date)
        tickets = self.te.get_tickets(self.date)
        self.assertEquals(len(tickets), 3)

    def test_002(self):
        # append a correct ticket and see if it's found.
        self.write_to_tics(self.date, "\x02four\x03")
        tickets = self.te.get_tickets(self.date)
        self.assertEquals(len(tickets), 1)
        self.assertEquals(tickets[0], "\x02four\x03")

    def test_003(self):
        filename = self.te.get_filename(self.te.file_templates[0]["name"],
                   self.date)
        # append a bogus, incomplete ticket and check that it isn't found
        pos1 = self.te.get_position(filename, self.date)
        self.write_to_tics(self.date, "\x02snafu")
        tickets = self.te.get_tickets(self.date)
        self.assertEquals(len(tickets), 0)
        pos2 = self.te.get_position(filename, self.date)
        self.assertEquals(pos1, pos2)

        # now complete the ticket... it should be found
        self.write_to_tics(self.date, " blah\x03")
        tickets = self.te.get_tickets(self.date)
        self.assertEquals(len(tickets), 1)
        pos2 = self.te.get_position(filename, self.date)
        self.assert_(pos2 > pos1)

    def test_004(self):
        # test the recognizer stuff
        formats = ("Colorado", "Illinois", "SouthCalifornia")
        for format in formats:
            tl = ticketloader.TicketLoader("../testdata/%s-1.txt" % (format,))
            raw = tl.getTicket()
            expected_format = ticketextractor.get_ticket_format(raw)
            self.assertEquals(expected_format, format)

        #for format in formats:
        #    tl = ticketloader.TicketLoader("../testdata/sum-%s.txt" % (format,))
        #    raw = tl.getTicket()
        #    expected_format = ticketextractor.get_ticket_format(raw)
        #    self.assertEquals(expected_format, format)

        # summaries have changed, so I'm uncommenting this test block for now

    def test_005(self):
        # test _distribute method
        lst = range(25)
        # we should get 3 lists: 2 of length 10 and 1 of length 5
        chunks = self.te._distribute(lst, 10)
        self.assertEquals(len(chunks), 3)
        self.assertEquals(len(chunks[0]), 10)
        self.assertEquals(len(chunks[1]), 10)
        self.assertEquals(len(chunks[2]), 5)

        chunks = self.te._distribute(range(10), 10)
        self.assertEquals(len(chunks), 1)

        chunks = self.te._distribute(range(11), 10)
        self.assertEquals(len(chunks), 2)

    def test_999(self):
        # clean up
        filetools.remove_file("test_positions.txt")
        filetools.remove_file("test_ids.txt")

        tics_file = self.te.get_filename(self.te.file_templates[0]["name"],
         self.date)
        os.remove(tics_file)

        del self.__class__.te   # don't keep instance around


class TestTicketExtractorHarder(unittest.TestCase):

    def test_100(self):
        # get tickets from Tics-04
        te = ticketextractor.TicketExtractor(BEGIN, END, upload=0, testmode=1,
             verbose=0)
        _, tickets = te._get_tickets("../testdata/Tics-04.txt", 0)

        # now try a text search on that file, to see how many tickets there
        # _should_ be
        raw = open("../testdata/Tics-04.txt", "rb").read()
        num_tickets = string.count(raw, "Ticket Nbr:")

        self.assertEquals(len(tickets), num_tickets)

    def test_read_configuration(self):
        class MockTicketExtractor(ticketextractor.TicketExtractor):
            def _start(self):
                pass
        tx = MockTicketExtractor(BEGIN, END)
        tx.read_configuration(os.path.join(_testprep.TICKET_PATH, "datadir",
          "te_testing.xml"))

        self.assertEquals(tx.host, "ftp-www.earthlink.net")
        self.assertEquals(tx.login, "wurmy")
        self.assertEquals(tx.password, "procyon")
        self.assertEquals(tx.start_dir, "test")
        self.assertEquals(tx.update_dir, "temp\\%s")
        self.assertEquals(tx.file_templates,
          [{'name': u'temp\\PortData_%s\\Tics-23.txt', 'position': u'pos23'},
           {'name': u'temp\\PortData_%s\\Tics-04.txt', 'position': u'pos04'}])
        self.assertEquals(tx.target_dir, "temp\\target")
        self.assertEquals(tx.prefix, "FCO1a")
        self.assertEquals(tx.logdir, "logs")
        self.assertEquals(tx.smtp_host, "smtp.earthlink.net")
        self.assertEquals(tx.from_address, "logger@earthlink.net")
        self.assertEquals(tx.admins,
          [{'email': 'vaporeon@wanadoo.nl', 'ln': '1'}])

    def test_999(self):
        shutil.rmtree('temp')


class TestTicketExtractor2(unittest.TestCase):

    def setUp(self):
        filetools.remove_file("positions2.txt")
        filetools.remove_file("ids2.txt")

    def test_001(self):
        # test get_filenames
        te2 = ticketextractor2.TicketExtractor2(BEGIN, END, verbose=0)
        te2.update_dir = "../testdata/ICU"
        filenames = te2.get_filenames("2002-10-03")
        self.assertEquals(len(filenames), 2)
        filenames = te2.get_filenames("2002-10-02")
        self.assertEquals(len(filenames), 0)
        filenames = te2.get_filenames("2002-09-03")
        self.assertEquals(len(filenames), 0)

    def test_002(self):
        # test _get_tickets
        te2 = ticketextractor2.TicketExtractor2(chr(12), None, verbose=0)
        te2.update_dir = "../testdata/ICU"
        FILENAME = "../testdata/ICU/ATTLIV/10-XX-02/10-03-02.txt"
        data = open(FILENAME, "rb").read()
        expected_tickets = string.count(data, chr(12))
        pos, tickets = te2._get_tickets(FILENAME, 0)
        self.assertEquals(len(tickets), expected_tickets)

        # tickets should not have the separator in them
        ticket = tickets[0]
        self.assert_(ticket.startswith("ATTLIV"))
        self.assert_(ticket.endswith("  \r\n"))
        self.assertEquals(string.find(ticket, chr(12)), -1)

        ticket = tickets[1]
        self.assert_(ticket.startswith("ATTLIV"))
        self.assert_(ticket.endswith("   \r\n"))
        self.assertEquals(string.find(ticket, chr(12)), -1)

    def tearDown(self):
        shutil.rmtree('temp')
        d = date.Date().isodate()[:10]
        s = d[:4] + d[5:7] + d[8:10]
        dirname = "../testdata/PortData_%s" % (s,)
        try:
            shutil.rmtree(dirname)
            os.rmdir(dirname)
        except:
            pass



if __name__ == "__main__":

    unittest.main()

