# New parser for SCA11 (based on SCA1)
# Mantis #3532

source: tickets/SCA11-2014-04/SCA11-2014-04-05-10-50-28-319-ARNTM.txt
formats: SouthCalifornia11

- ticket_number: A40940515
- update_of: A40940515
- ticket_type: SHRT NEW GRID
- kind: NORMAL
- ticket_format: SCA11
- transmit_date: 2014-04-04 11:20:15
- revision: 00A
- operator:  GRITO
- call_date: 2014-04-04 11:18:00
- work_state: CA
- work_county: SAN BERNARDINO
- work_city: UPLAND
- work_address_number: 154
- work_address_street: E 16TH ST
- work_cross: 2ND AVE
- work_description: MARK BY
- work_date: 2014-04-08 11:11:00
- work_type: REPAIR MAIN GAS LEAK
- company: SCG SAP #52-704254
- con_name: SOUTHERN CALIFORNIA GAS COMPANY (L)
- caller: CYNDI
- con_address: 1981 W. LUGONIA AVE
- con_city: REDLANDS
- con_state: CA
- con_zip: 92373
- caller_phone: 800-423-1391
- caller_fax: 909-335-7990
- caller_email: MDOMINGUEZ@SEMPRAUTILITIES.COM
- map_page: 0572B072

~ 5 work_lat: 34.12151
~ 5 work_long: -117.64949

? [x.client_code for x in t.locates]
! ['NEXTGLAVEN', 'SANANTDIST', 'SCG1OD', 'SGV01TRNS', 'SUNESYSLLC', 'UPW01', 'USCE34', 'USCE83RIV', 'UTWCONT', 'UVZPOM']

# Locat:
- work_remarks:

