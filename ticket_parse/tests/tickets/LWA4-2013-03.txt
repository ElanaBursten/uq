# LWA4-2013-03
# Mantis #3271

formats: WashingtonState4
source: tickets/LWA4-2013-03-14/LWA4-2013-03-13-17-04-04-588-71VMQ.txt

- map_page: 2N2E1SWSE
- call_date: 2013-03-13 11:40:00
- caller:
- caller_contact: JOHN HALL
- caller_email: JOHN@DROPBURY.COM
- caller_phone: (360)772-3815
- company: COMCAST
- con_address:
- con_city:
- con_name: FISK COMMUNICATIONS
- con_state: WA
- con_zip:
- kind: NORMAL
- operator: ormary
- ticket_number: 13056222
- update_of:
- ticket_type: 2 FULL BUSINESS
- transmit_date: 2013-03-13 12:23:00
- work_address_number: 16404
- work_address_street: NE 76TH WAY
- work_city: VANCOUVER
- work_county: CLARK
- work_cross: NE 164TH PL
- work_date: 2013-03-16 00:00:00
-start- work_description: ADDRESS IS ON THE NORTH SIDE
-end- work_description: AT THE ABOVE LOCATION.
- work_remarks: CALLER GAVE THOMAS GUIDE PAGE 508 D5
- work_state: WA
- work_type: INSTALL CATV SERVICE

? len(t.locates)
! 1

? t.locates[0].client_code
! 'POTELWA02'

~ 5 work_lat: 45.67745
~ 5 work_long: -122.50381


