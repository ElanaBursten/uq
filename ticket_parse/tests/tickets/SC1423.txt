# SC1423 format
# Mantis #2298

formats: SC1423
source: sample_tickets/1423/1423-2009-05-19-00001.txt

- call_date: 2009-05-12 11:53:20
- caller: LUCY NANCE
- caller_altphone:
- caller_cellular:
- caller_email:
- caller_fax: (843) 676-3675
- caller_phone: (843) 676-3613
- company: SCE&G
- con_address: 1812 N IRBY ST
- con_city: FLORENCE
- con_name: SCE&G
- con_state: SC
- con_type:
- con_zip: 29501
- duration:
- explosives: N
- map_page: 3411D7946C
- operator:
- priority:
- revision:
- source: 1423
- ticket_format: 1423
- ticket_number: 0905121374
- ticket_type: Emergency
- transmit_date: 2009-05-18 10:08:44
- work_address_number:
- work_address_street: WARLEY ST
- work_city: FLORENCE
- work_county: FLORENCE
- work_cross: SPRUCE ST & EVANS ST
- work_date: 2009-05-12 12:00:00
- work_state: SC
- work_subdivision:
- work_type: GAS, REPAIR SERVICE

-start- work_description: STARTING @ THE CORNER OF WARLEY ST & SPRUCE
-start- work_remarks: STARTING @ THE CORNER OF WARLEY ST & SPRUCE
~ 6 work_lat: 34.1870523718936
~ 6 work_long: -79.774100672120596

? len(t.locates)
! 5
? t.locates[0].client_code
! 'BSZU45'

#
# a few other tickets

source: sample_tickets/1423/1423-2009-05-19-00002.txt
- ticket_type: Cancel
- kind: NORMAL

source: sample_tickets/1423/1423-2009-05-19-00003.txt
- ticket_type: Remark
- kind: NORMAL

source: sample_tickets/1423/1423-2009-05-19-00004.txt
- ticket_type: Emergency
- kind: EMERGENCY

source: sample_tickets/1423/1423-2009-05-19-00005.txt
- ticket_type: Resend
- kind: NORMAL

source: sample_tickets/1423/1423-2009-05-19-00006.txt
- ticket_type: No Show
- kind: NORMAL

