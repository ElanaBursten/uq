# QMAN-3417: City of Corona clients

source: tickets/SCA1-2013-10-17-corona/SCA1-2013-10-07-08-06-19-924-M431Y.txt
formats: SouthCalifornia

- ticket_number: B32800020
- ticket_type: NORM NEW GRID
- ticket_format: SCA1

? [x.client_code for x in t.locates]
! ['ATTDSOUTH', 'UCOR19', 'SCG1CO', 'USCE34', 'USCE83RIV', 'UTWCWRIV', 'UVZMENIF', 'UVZPERS', 'UCOR19-S', 'UCOR19-RW', 'UCOR19-E']

