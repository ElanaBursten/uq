# NewJersey9 format

formats: NewJersey9
source: tickets/NewJersey9-2014-08/NewJersey92014-08-13-13-01-04-362-GHAP1.txt
        
- transmit_date: 2014-08-13 12:59:00
- ticket_type: ROUTINE
- ticket_number: 142251303
- work_date: 2014-08-19 00:15:00
- work_county: CAMDEN
- work_city: CAMDEN
- work_address_number: 2881
- work_address_street: MT EPHRAIM AVE
- work_cross: OLYMPIA RD
- work_type: REMOVE POLE(S)
- work_description: 10FT RADIUS OF POLE 15820.  LOCATED 650FT BEHIND CURB.
- work_remarks: Working For Contact:  BRIAN FINOCCHIARO 
- company: VERIZON
- caller: BRIAN FINOCCHIARO
- caller_phone: 609-261-9912
- con_name: VERIZON
- con_address: 625 LUMBERTON RD
- con_city: HAINESPORT
- con_state: NJ
- con_zip: 08036
- caller_fax: 609-267-8635
- caller_cellular: 609-261-9912
- caller_email: bfino1972@gmail.com

? len(t.locates)
! 1
? t.locates[0].client_code
! 'SCNJ5'

