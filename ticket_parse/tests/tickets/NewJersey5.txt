# NewJersey5
# Adapted from test_NewJersey5()
# For our purposes, the NewJersey5 format is the same as PA7501.

formats: NewJersey5, PA7501
source: tickets/NewJersey5-2010-03-15/7501-2010-01-20-00447.txt

- ticket_number: 20100201066
- ticket_type: NEW XCAV DSGN
- work_county: BUCKS
- work_city: BUCKINGHAM TWP
- work_address_street: ROUTE 263
- work_cross: SUGAR BOTTOM ROAD
- work_type: ROADWAY-DRAINAGE-SIGNAL RECONSTRUCTION
- call_date: 2010-01-20 12:59:00
- transmit_date: 2010-01-20 12:59:24
- respond_date: 2010-02-03 00:00:00
- caller: KEVIN NEHF
- caller_phone: 717-691-1340
- con_name: KCI TECHNOLOGIES INC
- con_address: 5001 LOUISE DR STE 201
- con_city: MECHANICSBURG
- con_state: PA
- con_zip: 17055
- work_state: PA
- caller_fax: 717-691-3470
- caller_email: kevin.nehf@kci.com
- company: PENN DOT
- caller_contact: SCOTT RABOCI

? t.work_date
! None

? [x.client_code for x in t.locates]
! ['BTB', 'FP', 'KD', 'SF', 'YI', 'KDG']
