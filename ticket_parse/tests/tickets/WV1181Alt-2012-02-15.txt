# 1181: Korterra tickets _without_ the Korterra header
# Has no serial number, and the transmit date differs because it's not taken
# from the header.

formats: WV1181Alt
source: tickets/1181-2012-02-15/1181-2012-02-11-00005.txt

- ticket_number: 1204215306
- serial_number:
- transmit_date: 2012-02-11 12:39:00
- call_date: 2012-02-11 12:39:00
- ticket_type: Emergency
- kind: EMERGENCY
- con_name: BERKELEY COUNTY WATER
- con_address: PO BOX 737
- con_city: MARTINSBURG
- con_state: WV
- con_zip: 25404
- caller: Wayne Earhart
- caller_email:
- caller_phone: (304) 267-4600
- caller_fax: (304) 264-4590
- work_state: WV
- work_date: 2012-02-11 12:45:00
- work_county: BERKELEY
- company: BERKELEY COUNTY WA
- work_city: MARTINSBURG
- work_address_street: 0 Meadow Ln
- work_cross: Clover
- work_type: Emer water main repair

? [x.client_code for x in t.locates]
! ['PEB', 'AX', 'US', 'CEM', 'WCM', 'BKC']

source: tickets/1181-2012-02-15/1181-2012-02-13-00005.txt

- ticket_number: 1204415338
- caller: DARREN PUFFINDURGER
- caller_email: DPUFFIN@ALLEGHENYPOWER.COM
- caller_phone: (304) 359-0593

