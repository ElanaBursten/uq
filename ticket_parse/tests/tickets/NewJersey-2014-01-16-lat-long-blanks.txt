# Mantis #3482
# Test blank and non-blank work_lat, work_long, work_county, work_city,
# work_subdivision, work_address_street, and work_cross, for New Jersey tickets

formats: NewJersey2010

source: tickets/NewJersery-2014-01-16/NewJersey-2014-01-16-16-42-23-102-5GQPZ.txt

- ticket_number: 140161646
- work_county: MORRIS
- work_city: ROXBURY
- work_subdivision: KENVILLE
- work_address_street: BERKSHIRE VALLEY RD
- work_cross: RAILROAD AVE
~ 5 work_lat: 0
~ 5 work_long: 0

source: tickets/NewJersery-2014-01-16/NewJersey-2014-01-16-16-54-27-117-168Z3.txt
- ticket_number: 140160310
- work_county: MONMOUTH
- work_city: NEPTUNE CITY
- work_subdivision:
- work_address_street: STATE RTE 35
- work_cross: MORRIS AVE
~ 5 work_lat: 40.19759
~ 5 work_long: -74.028701

source: tickets/NewJersery-2014-01-16/NewJersey-2014-01-16-16-50-52-753-5AK3X-modified-for-testing.txt
- ticket_number: 140161663
- work_county:
- work_city:
- work_subdivision:
- work_address_street:
- work_cross:
~ 5 work_lat: 0
~ 5 work_long: 0
