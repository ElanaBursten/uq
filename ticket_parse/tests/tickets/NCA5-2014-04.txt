# QMAN-3532

formats: NorthCalifornia5
source: tickets/NCA5-2014-04/NCA5-sample-1.txt

- call_date: 2014-04-04 09:38:00
- transmit_date: 2014-04-04 09:42:28
- caller: DANIEL LEBRON
- caller_altcontact: MIKE
- caller_cellular: 510-536-4800
- caller_email: DLEBRON@CAMPANELLACORPORATION.COM
- caller_fax: 510-536-6132
- caller_phone: 510-536-4800
- company: SOUTH  BAY CONSTRUCTION
- con_name: CAMPANELLA CORPORATION
- con_address: 2216 DUNN RD, OAKLAND
- con_city: HAYWARD
- con_state: CA
- con_zip: 94545
- explosives: N
- kind: NORMAL
  # emergency if priority = 1
- priority: 2
- ticket_number: 0128250
- ticket_type: NORMAL NOTICE
- work_city: CUPERTINO
- work_county: SANTA CLARA
- work_date: 2014-04-08 10:00:00
- work_state: CA
- work_type: EXC TO REM CONCRETE/ASPHAL/LANDSC
- work_remarks:

~ 5 work_long: -122.03224
~ 5 work_lat: 37.321671

-start- work_description: AT A POINT 280FT
-end- work_description: FOR THE ENT DIST

? [x.client_code for x in t.locates]
! ['CWSLAL', 'CTYCUP', 'CTYSJO', 'CTYSUN', 'COMSJO', 'CUPSAN', 'MCIWSA', 'PACBEL', 'PGECUP', 'SPTTEL', 'SJOWT2', 'SCLVLY', 'SUNSYS']
