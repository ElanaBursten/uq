# Mantis 2706
# Due date changes for 3003, 3004
# Also see: test_businesslogic_due_dates.py

formats: GA3003, GA3004

source: tickets/3003-2011-01-12-due-dates/3003-2011-01-11-00017.txt
- ticket_type: LPEXCA
- revision: 001
- update_of:
- legal_good_thru: 2011-04-11 00:00:00
- work_date: 2011-01-06 12:00:00
  # wrong? is 12 AM

source: tickets/3003-2011-01-12-due-dates/3003-2011-01-11-01138.txt
- ticket_number: 01111-400-091
- update_of: 10180-400-042 
- ticket_type: LPEXCA RESTAK
