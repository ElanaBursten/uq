# Mantis 2823: Some tickets have a term id wity a trailing '3' (e.g. 'GPU3'
# rather than 'GPU'); this needs to be removed

formats: NewJersey2010B
source: tickets/NewJersey2-2011-07-05-emer/TEST-example-1.txt

- ticket_number: 111850008

? [x.client_code for x in t.locates]
! ['GPU']
# NOT GPU3; the trailing 3 should be removed (Mantis 2823)

