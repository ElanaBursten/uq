# Mantis #2564: OCC2 and FDE2 switching to XML format

formats: FairfaxXML, DelawareXML, WashingtonXML

-x- import formats.FairfaxXML; formats.FairfaxXML.Parser.PARSE_GRIDS = True

source: tickets/FairfaxXML-2010-05-06/TEST-2010-03-23-00076.txt

- transmit_date: 2010-03-23 12:44:35
- ticket_number: A008200746
- work_state: VA
- call_date: 2010-03-23 11:39:50
- ticket_type: NORM NEW GRID LREQ
- kind: NORMAL
- work_date: 2010-04-09 23:59:59
- work_county: ALEXANDRIA CITY
- work_city: 
- work_address_number: 611
- work_address_street: FORT WILLIAMS PY
- work_cross: CHELSEA CT
- work_type: DRAIN PIPE - INSTALL
- company: HO/ RAGAN
- explosives: N
- con_name: NEWBORG DRAINAGE & LANDSCAPE
- con_address: 7920 WOODRUFF CT
- con_city: SPRINGFIELD
- con_state: VA
- con_zip: 22151
- caller_phone: 7033213145
- caller_fax: 7033218588
- caller_contact: KRISTINE KAY
- work_remarks: CALLER MAP REF: NONE
- map_page: 3849D7705B-10
  # same as first grid

~ 6 work_lat: 38.819195
~ 6 work_long: -77.094627

? len(t.grids)
! 6

? t.grids[0]
! '3849D7705B'

? len(t.locates)
! 7

? t.locates[0].client_code
! 'ALX901'

? t.locates[-1].client_code
! 'WGL904'

-start- work_description: WORK TYPE INCLUDES GRADING
-end- work_description: EXCAVATING ON THE ENTIRE PROPERTY

#
# test reference field on Verizon tickets

source: tickets/FairfaxXML-2010-05-06/TEST-VZN-1.txt

- ticket_number: A012300907
- map_ref: VZN:E5690230\53005\5845C
- caller_email: thomas_tregembo@yahoo.com

#
# test recent ticket with city etc.

source: tickets/FairfaxXML-2010-05-06/TEST-2010-05-11-00011.txt

- ticket_number: A013000321
- work_county: FAIRFAX
- work_city: ANNANDALE

#
# make sure that tickets are not flagged as line noise

source: tickets/FairfaxXML-2010-05-06/OCC2-2010-06-04-00328.txt

# we can refer to the testing object using obj
? tools.is_linenoise(obj.ticket_image)
! False

- ticket_number: B015500482

? t.locates[0].client_code
! 'ATT392'

#
# some tickets don't have a work date

source: tickets/FairfaxXML-2010-05-06/OCC2-2010-06-04-00334.txt

? tools.is_linenoise(obj.ticket_image)
! False

- ticket_number: A015500610
- ticket_type: EMER NEW GRID LREQ
- kind: EMERGENCY
- work_date: 2010-06-04 13:21:38
  # response_due field
