# Mantis 3095, new feed for 3006

formats: GA3006
source: tickets/3006-2012-11-01/3003-2012-10-30-11-28-08-506-D9V55.txt

- ticket_number: 10262-320-014
- update_of: 
- ticket_type: NORMAL
- call_date: 2012-10-26 10:29:00
- revision: 000
- caller: JESSICA DANIELS
- caller_altphone:
- caller_phone: 478-934-1094
- con_address: 246 N 2ND ST
- con_city: COCHRAN
- con_state: GA
- con_type: CONT
- con_zip: 31014
- duration: 2 WEEKS
  # should NOT include 'Priority:' of course
- con_name: MIDDLE GEORGIA SIGNS
- company: SCRUGGS
- kind: NORMAL
- legal_good_thru: 2012-11-16 00:00:00
- legal_restake: 2012-11-13 00:00:00
- map_page: 3111C8347C
- transmit_date: 2012-10-26 10:29:28
- work_address_street: 8TH AVE NE
- work_cross: N MAIN ST
- work_city: MOULTRIE
- work_county: COLQUITT
- work_date: 2012-10-31 07:00:00
- work_state: GA
- work_type: INSTALLING SIGNS
- respond_date: 2012-10-30 23:59:00

? len(t.locates)
! 10

? t.locates[0].client_code
! 'BGAWS'

# approximate values for floats
~ 4 work_lat: 31.1875
~ 4 work_long: -83.7895


