# Mantis #2977

formats: Nashville2013
source: tickets/FNV1-2013-01-08/FNV1-2013-01-08-05-50-45-098-OIVQX.txt

- ticket_number: 130080004
- transmit_date: 2013-01-08 03:13:00
- ticket_type: Emergency
- kind: EMERGENCY
- call_date: 2013-01-08 03:13:00
- work_date: 2013-01-08 03:15:00
- con_name: METRO WATER SERVICES
- con_address: 1616 3RD AVE N
- con_city: NASHVILLE
- con_state: TN
- con_zip: 37207
- caller_fax: (615) 862-4837
- caller: KEITH HYMAN
- caller_email: 
- caller_phone: (615) 862-4770
- work_state: TN
- work_county: DAVIDSON
- work_city: NASHVILLE
- work_address_number: 537
- work_address_street: AUGUSTA DR
- work_cross: ATLANTA DR
- explosives: N
- work_type: WATER MAIN, REPAIR AND/OR REPL
- company: METRO WATER SERVICES
- map_page: 75D

#- work_description: PLEASE EMAIL ME IF ANY QUESTIONS
-start- work_remarks: EMERGENCY - EACH UTIL IS REQUIRED
-end- work_remarks: INTER APPX. 900FT..

~ 5 work_lat: 36.20975
~ 5 work_long: -86.59488

? len(t.locates)
! 5
? [x.client_code for x in t.locates]
! ['NES', 'B01', 'NYG', 'MWS', 'INTMC']

source: tickets/FNV1-2013-01-08/FNV1-2013-01-08-06-15-59-424-S0RAC.txt
- ticket_number: 130085001
- update_of: 123550713

# format change Sep 2014 (QMAN-3661)
source: tickets/FNV1-2014-09-notime/FNV1-2013-01-08-06-15-59-424-S0RAC-v2.txt
- ticket_number: 130085001
- work_date: 2013-01-11 05:15:00
- work_state: TN
- work_county: RUTHERFORD
- work_city: MURFREESBORO
