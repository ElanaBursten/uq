# Mantis #2805: format changes for 1422

formats: SC1422, SC1422_ATT
source: tickets/1422-2011-06-01/TEST-2011-05-09-00001.txt

- ticket_number: 1105091823
- ticket_type: Normal
- kind: NORMAL
- transmit_date: 2011-05-09 12:21:00
- call_date: 2011-05-09 12:21:08
- work_date: 2011-05-12 12:00:51
- work_state: SC
- work_county: CHARLESTON
- work_city: MOUNT PLEASANT
- work_address_number: 1
- work_address_street: SEAFOOD DR
- work_cross: SHRIMP BOAT LN
- work_type: SEE REMARKS
- con_name: SOUTHEAST UTILITIES
- caller: RONALD BASTIAN
- con_address: 4760 FRANCHISE ST
- con_state: SC
- con_city: NORTH CHARLESTON
- con_zip: 29418

# there's only one term id, near the top
? [x.client_code for x in t.locates]
! ['SCA88']

