# Mantis #3125: ANSCO ticket format, 1424

formats: SC1424
source: tickets/1424-2012-10-05-ansco/1421-2012-10-04-14-32-08-378-R3YAJ.txt

- ticket_number: 1210041011
- ticket_type: Normal
- kind: NORMAL
- transmit_date: 2012-10-04 11:32:00
- call_date: 2012-10-04 11:32:41
- work_date: 2012-10-09 23:59:59
- work_state: SC
- work_county: PICKENS
- work_city: SIX MILE
- work_address_number: 
- work_address_street: OLD SENECA ROAD
- work_cross: MOUNT OLIVET ROAD
- work_type: TELEPHONE, INSTALL DROP(S)
- con_name: ANSCO & ASSOCIATES
- caller: REBECCA WAMPLER
- con_address: 43 SENTELL RD
- con_state: SC
- con_city: GREENVILLE
- con_zip: 29611

# there's only one term id, near the top
? [x.client_code for x in t.locates]
! ['BSZT29', 'CCMZ41', 'DPCZ25', 'FTHZ63']


