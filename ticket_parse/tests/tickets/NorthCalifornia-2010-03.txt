#

formats: NorthCalifornia
source: tickets/NCA1-2010-03-26/TEST-2010-02-01-00007.txt

- call_date: 2010-01-27 14:37:00
- transmit_date: 2010-02-01 16:09:24
- caller: JASON ALURCON
- caller_altcontact: GREG JUELS
- caller_cellular: 916-416-3460
- caller_email: JALURCON@SNC.BIZ
- caller_fax: 916-753-1798
- caller_phone: 916-753-1700
- company: CITY OF COLUSA
- con_address: 9701 DINO DR
- con_city: ELK GROVE
- con_name: SIERRA NEVADA CONSTRUCTION
- con_state: CA
- con_zip: 95624
- explosives: N
- kind: NORMAL
- priority: 2
- ticket_number: 0000038
- ticket_type: NORMAL NOTICE RENEWAL
- work_city: COLUSA
- work_county: COLUSA
- work_date: 2010-01-29 14:45:00
- work_description: ALL/O E CARSON ST FR 3RD ST TO 1ST ST ( R/O/W TO R/O/W )
- work_state: CA
- work_type: DIG FOR CONCRETE REMOVAL

~ 5 work_long: -122.006699
~ 5 work_lat: 39.20595

-start- work_remarks: RENEWAL OF TICKET RN#000010 ORIG DATE
-end- work_remarks: ALL MEMBERS

? [x.client_code for x in t.locates]
! ['CTYCOL', 'COMORO', 'FRNCOL', 'LEVCAL', 'PGEMSV']

# QMAN-3576: additional locates for Citrus Heights

source: tickets/NCA1-2010-03-26/TEST-2010-02-01-00007-citrus.txt
? [x.client_code for x in t.locates]
! ['CTYGSD', 'COMORO', 'FRNCOL', 'LEVCAL', 'PGEMSV', 'CTYGSD-SL', 'CTYGSD-TS']


