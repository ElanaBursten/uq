# Test new due date rule for OCC2, FDE2, FMW3
# Mantis #2739

# IN PROGRESS

formats: FairfaxXML
source: tickets/OCC2-2011-02-25-due-date/OCC2-2011-02-24-00900.txt

- ticket_number: A105501413
- due_date: 2011-03-01 07:00:00

source: tickets/OCC2-2011-02-25-due-date/OCC2-2011-02-24-00901.txt

- ticket_number: A105501417
- due_date: 2011-03-01 07:00:00

#
# what happens if there is no response_due field?

source: tickets/OCC2-2011-02-25-due-date/OCC2-2011-02-24-00905-A.txt

- ticket_number: B105501400
- due_date: 

