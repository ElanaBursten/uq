# Mantis #3444

formats: GeorgiaXML

# -x- import formats.FairfaxXML; formats.FairfaxXML.Parser.PARSE_GRIDS = True

source: tickets/GeorgiaXML-2013-11/Normal Notice.xml.txt

- transmit_date: 2011-07-02 23:09:38
- ticket_number: 07021-300-032
- work_state: GA
- call_date: 2011-07-02 23:09:38
- ticket_type: Normal Notice
- kind: NORMAL
- work_date: 2011-07-08 07:00:00
- work_county: FULTON
- work_city: ATLANTA
- work_address_number: 
- work_address_street: SW Trinity Ave
- work_cross: 
- work_type: rpr man hole tops
- company: gpc
- explosives: No
- con_name: DILLARD SMITH CONSTRUCTION
- con_address: 548 LAKE MIRROR RD
- con_city: COLLEGE PARK
- con_state: GA
- con_zip: 30349
- caller_phone: 4047617924
- caller_fax: 
- caller_contact: Johnny Southern

-start- work_remarks: Previous Ticket Number
-end- work_remarks: 300-030

- map_page: 3344A8423C
# same as first grid

~ 6 work_lat: 33.748554
~ 6 work_long: -84.392159

? len(t.locates)
! 11

? t.locates[0].client_code
! 'AGL103'

? t.locates[-1].client_code
! 'ST005'

-start- work_description: locate along the r/o/w
-end- work_description: from inter to inter

