# test_decorator.py

import time
import unittest
import site; site.addsitedir('.')
#
import decorator

class MyDecorator(decorator.Decorator):
    def __init__(self, *args, **kwargs):
        decorator.Decorator.__init__(self, *args, **kwargs)
        self.times = []

class MyMethodDecorator(decorator.MethodDecorator):

    def before(self, *args, **kwargs):
        self.decorator.times.append(time.localtime(time.time()))

    def after(self, *args, **kwargs):
        self.decorator.times.append(time.localtime(time.time()))

class MyClass:
    magic = 42
    def __init__(self, x):
        self.num = x
    def foo(self):
        pass

()

class YourDecorator(MyDecorator):
    def __init__(self, obj, method_decorator, attr_decorator):
        self.obj = obj
        self.method_decorator = method_decorator
        self.attr_decorator = attr_decorator
        self.times = []
    def __getattr__(self, name):
        attr = getattr(self.obj, name)
        if callable(attr):
            method_decorator = self.method_decorator
            return method_decorator(self, attr)
        else:
            attr_decorator = self.attr_decorator
            return attr_decorator(self, attr)

class YourMethodDecorator(MyMethodDecorator):
    pass

class YourAttributeDecorator:
    def __init__(self, decorator, attr):
        self.decorator = decorator
        self.attr = attr
    def __getattr__(self, name):
        attr = getattr(self.attr, name)
        if callable(attr):
            method_decorator = self.decorator.method_decorator
            return method_decorator(self, attr)
        else:
            attr_decorator = self.decorator.attr_decorator
            return attr_decorator(self, attr)

()

class TestDecorator(unittest.TestCase):

    def test001(self):
        myinstance = MyClass(31)
        dec = MyDecorator(myinstance, MyMethodDecorator)

        self.assertEquals(len(dec.times), 0)

        dec.foo()
        self.assertEquals(len(dec.times), 2)

        # getting an attribute has no effect!
        a = dec.num
        self.assertEquals(len(dec.times), 2)

    def __test002(self):
        myinstance = MyClass(31)
        dec = YourDecorator(myinstance, YourMethodDecorator, YourAttributeDecorator)

        self.assertEquals(len(dec.times), 0)

        dec.foo()
        self.assertEquals(len(dec.times), 2)

        # getting an attribute has no effect!
        a = dec.num
        self.assertEquals(len(dec.times), 2)

        # however, when an attribute is a *function*, then it should work:
        def f(): pass
        myinstance.f = f
        dec.f()
        self.assertEquals(len(dec.times), 4)

        class Dummy: pass
        myinstance.dummy = Dummy()
        myinstance.dummy.f = f
        dec.dummy.f()



if __name__ == "__main__":

    unittest.main()

