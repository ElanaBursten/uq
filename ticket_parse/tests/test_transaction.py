# test_transaction.py

from __future__ import with_statement
import unittest
#
import dbinterface # new
import dbinterface_ado # old
import ticket_db
from transaction import Transaction

class TestTransaction(unittest.TestCase):

    #
    # test rollbacks

    def test_new_dbinterface_rollback(self):
        tdb = ticket_db.TicketDB()
        self._test_transaction_rollback(tdb.dbado)

    def test_old_dbinterface_ado_rollback(self):
        db = dbinterface.detect_interface()
        self._test_transaction_rollback(db)

    def _test_transaction_rollback(self, db):
        # make sure we don't have a call center named 'TRANS123'
        db.runsql("""delete call_center where cc_code = 'TRANS123' """)

        with Transaction(db, suppress=True):
            # valid INSERT statement
            db.runsql("""
                insert call_center
                (cc_code, cc_name)
                values ('TRANS123', 'TRANS123')
            """)

            # now try an invalid INSERT statement (cc_name is a required field,
            # but not specified here)
            db.runsql("""
                insert call_center (cc_code)
                values ('TRANS124')
            """)

        # there should be NO call center named 'TRANS123' in the database
        rows = db.getrecords('call_center', cc_code='TRANS123')
        self.assertEquals(rows, [])

    #
    # test commits

    def test_new_dbinterface_commit(self):
        tdb = ticket_db.TicketDB()
        self._test_transaction_rollback(tdb.dbado)

    def test_old_dbinterface_ado_commit(self):
        db = dbinterface.detect_interface()
        self._test_transaction_rollback(db)

    def _test_transaction_commit(self, db):
        # make sure we don't have a call center named 'TRANS125'
        db.runsql("""delete call_center where cc_code = 'TRANS125' """)

        with Transaction(db, suppress=True):
            # valid INSERT statement
            db.runsql("""
                insert call_center
                (cc_code, cc_name)
                values ('TRANS125', 'TRANS125')
            """)

        # there should be a call center named 'TRANS125' in the database
        rows = db.getrecords('call_center', cc_code='TRANS125')
        self.assertEquals(len(rows), 1)


