# test_responder_config_data.py

import unittest
import site; site.addsitedir('.')
#
import config
import responder_config_data as rcd

class TestResponderConfigData(unittest.TestCase):

    will_fail = True # I don't see the point of this test

    def test_get_responders(self):
        cfg = config.getConfiguration()
        spam = cfg.responderconfigdata

        # get all responders
        eggs = spam.get_responders()
        self.assertEquals(len(eggs), 40) # fix when responders are added

        eggs = spam.get_responders(call_center='Atlanta')
        self.assertEquals(len(eggs), 1) # fix when responders are added

        eggs = spam.get_responders(call_center='FEF1')
        self.assertEquals(len(eggs), 2) # fix when responders are added

        eggs = spam.get_responders(type='irth')
        self.assertEquals(len(eggs), 12) # fix when responders are added

        eggs = spam.get_responders(type='email')
        self.assertEquals(len(eggs), 4) # fix when responders are added



if __name__ == "__main__":

    unittest.main()

