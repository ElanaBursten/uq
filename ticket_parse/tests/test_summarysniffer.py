# test_summarysniffer.py

from __future__ import with_statement
import os
import shutil
import site; site.addsitedir('.')
import unittest
#
import _testprep
import summarysniffer
import ticket_db
import ticketloader
import ticketparser

class __TestSummarySniffer(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def drop(self, filename, raw):
        fullname = os.path.join("test_ss", filename)
        with open(fullname, "wb") as f:
            f.write(raw)

    def test_000(self):
        _testprep.set_cc_routing_file()
        try:
            os.mkdir("test_ss")
        except:
            pass

    def test_001(self):
        # just test any old summary... is it processed correctly?
        ss = summarysniffer.SummarySniffer("test_ss", "2000-01-01",
             "2999-31-12", format="NorthCalifornia", verbose=0)
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCalifornia.txt")
        raw = tl.getTicket()
        self.drop("summ-1.txt", raw)

        ss.run()

        spam = self.tdb.getrecords("summary_header", call_center="NCA1")
        self.assertEquals(len(spam), 1)

    # TODO:
    # test multiple summaries... are they updated?

    def test_999(self):
        try:
            shutil.rmtree("test_ss")
        except:
            pass


if __name__ == "__main__":

    unittest.main()

