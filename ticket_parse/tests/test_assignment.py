# test_assignment.py

import unittest
import os
import site; site.addsitedir('.')
#
import assignment
import ticket_db
import ticketloader
import _testprep

class TestAssignment(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def test_001(self):
        t1 = _testprep.get_test_ticket(os.path.join("..","testdata",
             "Atlanta-1.txt"), 0, ["Atlanta"])
        id = self.tdb.insertticket(t1) # will store ticket and locates

        # single out a locate
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 13)
        lid1 = locates[0]['locate_id']

        # the number of assignments on this locate should be 0
        asgmts = self.tdb.getrecords("assignment", locate_id=lid1)
        self.assertEquals(len(asgmts), 0)

        a = assignment.Assignment(200)
        # in this case, we must set the locate_id by hand
        a.set(locate_id=lid1, added_by="Fred")
        aid = a.insert(self.tdb)
        self.assert_(aid, "Assignment id should be returned")
        self.assertEquals(a.assignment_id, aid)

        rows = self.tdb.getrecords("assignment", locate_id=lid1)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['added_by'], 'Fred')
        self.assertEquals(rows[0]['locate_id'], lid1)
        self.assertEquals(rows[0]['locator_id'], '200')
        self.assertEquals(rows[0]['assignment_id'], aid)

        # loading
        b = assignment.Assignment.load(self.tdb, aid)
        self.assertEquals(b.assignment_id, aid)
        self.assertEquals(b.locator_id, '200')
        self.assertEquals(b.locate_id, lid1)

if __name__ == "__main__":
    unittest.main()

