# test_email.py

import unittest
import site; site.addsitedir('.')
#
import emailtools
import ticket_db

class TestEmail(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def test_001(self):
        sql = """
         update call_center
         set admin_email = 'hans.nowak@oasisdigital.com',
          emergency_email = 'hans.nowak@oasisdigital.com',
          summary_email = '',
          message_email = 'hans.nowak@oasisdigital.com  ',
          warning_email = 'hans.nowak@oasisdigital.com; fred@something.com'
         where cc_code = 'Atlanta'
        """
        self.tdb.runsql(sql)
        emails = emailtools.Emails(self.tdb)

        # does the Email class hold the values we put in the database?
        atl_emails = emails["Atlanta"]
        ijk = ["hans.nowak@oasisdigital.com"]
        self.assertEquals(atl_emails.admin, ijk)
        self.assertEquals(atl_emails.emergency, ijk)
        self.assertEquals(atl_emails.summary, [])
        self.assertEquals(atl_emails.message, ijk)
        self.assertEquals(atl_emails.warning, ijk + ["fred@something.com"])

    def test_notification_email(self):
        """ Test retrieving emails from notification_email table. """
        sqldata = [
            ('XYZ', 'admin', 'hans@hans.com'),
            ('XYZ', 'warning', 'fred@hans.com'),
            ('XYZ', 'admin', 'christine@hans.com'),
            ('PQR', 'admin', 'john@hans.com'),
            ('PQR', 'emergency', 'hans@hans.com'),
            ('PQR', 'emergency', 'jessi@hans.com'),
        ]

        # delete previous test records
        self.tdb.deleterecords("notification_email", call_center="XYZ")
        self.tdb.deleterecords("notification_email", call_center="PQR")

        sqltemplate = """
         insert notification_email
         (call_center, notification_type, email_address)
         values ('%s', '%s', '%s')
        """

        for data in sqldata:
            sql = sqltemplate % data
            self.tdb.runsql(sql)

        emailtools.Emails.read_emails = emailtools.Emails.read_emails_2
        try:
            e = emailtools.Emails(self.tdb)

            xyz = e['XYZ']
            self.assertEquals(xyz.admin, ['hans@hans.com', 'christine@hans.com'])
            self.assertEquals(xyz.warning, ['fred@hans.com'])
            pqr = e['PQR']
            self.assertEquals(pqr.admin, ['john@hans.com'])
            self.assertEquals(pqr.warning, [])
            self.assertEquals(pqr.emergency, ['hans@hans.com', 'jessi@hans.com'])
        finally:
            emailtools.Emails.read_emails = emailtools.Emails.read_emails_1

if __name__ == "__main__":

    unittest.main()
