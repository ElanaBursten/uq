# test_test.py
# Test any derived classes from unittest.TestCase.
# Created: 10 Mar 2002, Hans Nowak

import site; site.addsitedir('.')
import unittest
#
import tptestcase

class TestTPTestCase(tptestcase.TPTestCase):

    def test_close_enough(self):
        self.assert_close_enough(1.11000, 1.11234, eps=0.01)

        # XXX There certainly must be a way to tets this with real float
        # computations... at this moment, I don't know how to get this,
        # though...



if __name__ == "__main__":

    unittest.main()

