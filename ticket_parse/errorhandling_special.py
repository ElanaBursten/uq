# errorhandling_special.py
# Created: 2003.01.09 HN

import sys
#
import config
import datadir
import errorhandling2

def toplevel_logger(configfile, name="Watchdog", subject="Watchdog Warning"):
    """ Create a logger for catching toplevel errors. """
    cfg = config.Configuration(configfile or "config.xml")
    if configfile:
        cfg = config.Configuration(configfile)
    else:
        cfg = config.getConfiguration()
    log = errorhandling2.ErrorHandler(logdir=cfg.logdir,
          smtpinfo=cfg.smtp_accs[0], me=name,
          cc_emails={}, admin_emails=cfg.admins, subject=subject)
    log.defer_mail = 1

    # XXX maybe we need to set call center emails, by copying the admin emails
    # for each call center?

    return log

def log_config_error(configfile, name="Watchdog"):
    """
    In case of a configuration error (e.g. because of invalid XML), try to
    extract crucial info (emails, SMTP server) from the config file, and
    use that to send an error email.
    after this, it's probably a good idea to exit the program... but that is
    left up to the caller.
    """
    data = datadir.datadir.open(configfile or "config.xml", "r").read()
    emails = config.extract_error_emails(data)
    smtp, from_address = config.extract_smtp_data(data)
    smtpinfo = emailtools.SMTPInfo(host=smtp, from_address=from_address)
    log = errorhandling2.ErrorHandler(logdir="logs", smtpinfo=smtpinfo,
          me=name, from_address=from_address, admin_emails=emails)
    ep = errorhandling2.errorpacket()
    ep.add(xml=data)
    exc = sys.exc_info()[1]
    if exc is not None and hasattr(exc, 'errormsg'):
        ep.add(errormsg=exc.errormsg)
    log.log(ep, send=1, write=1, dump=1)
    log.force_send()


