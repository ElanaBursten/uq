# ticketrouterdata.py

import tools

class TicketRouterData:

    def __init__(self):
        self._tickets = {}              # key: ticket_id
        self._locates = {}              # key: locate_id
        self._locate_names = {}         # key: locate_id
        self._assignments = {}          # key: locate_id; value: locator
        self._locate_client = {}        # key: locate_id, value: client_id
        self._locate_client_code = {}   # key: locate_id, value: client_code

        # First, we collect all the info we need in these
        # dictionaries. 'tickets' contains a dict for every ticket with
        # mapinfo, locates, call_date, ticket_format and ticket_number.
        # locates contains the (new) status for every locate.
        # assignments contains the locator (if we assign the locate).

        # 2004.07.07: This is not a very nice way to handle it.  It should be
        # refactored into something more decent.  Maybe there should be a data
        # object for each ticket.

    def add_row(self, row):
        # XXX this really isn't all that great... maybe we should add all
        # fields by default?

        ticket_id = row["ticket_id"]
        if not self._tickets.has_key(ticket_id):
            self._tickets[ticket_id] = {
             "mapinfo": "",
             "locates": [],
             "call_date": row["call_date"],
             "transmit_date": row["transmit_date"],
             "due_date": row["due_date"],
             "respond_date": row.get("respond_date", None),
             "work_date": row.get("work_date", None),
             "ticket_number": row["ticket_number"],
             "ticket_format": row["ticket_format"],
             "company": row.get("company", ""),
             "con_name": row.get("con_name", ""),
             "kind": row["kind"],
             "ticket_type": row["ticket_type"],
             "work_state": row["work_state"],
             "work_type": row.get("work_type", ""),
             "work_address_street": row.get("work_address_street", ""),
             "locator": None,
             "area_id": None,   # for new route_area_id field
             "do_not_mark_before": row.get("do_not_mark_before", None),
             "explosives": row.get("explosives", ""),
             "ward": row.get("ward", ""),
             "installer_number": row.get("installer_number", ""),
             "locate_names": [], # stores names for internal purposes
                                 # yes, this sucks

             # XXX do NOT set client_code here -- it may be set elsewhere
            }

        locate_id = row["locate_id"]

        # _tickets[ticket_id]['locates'] is a list of locate_ids for that
        # ticket:
        self._tickets[ticket_id]["locates"].append(locate_id)

        self._locates[locate_id] = None   # status is not known yet
        self._locate_names[locate_id] = row['client_code']
        self._tickets[ticket_id]['locate_names'].append(row['client_code'])

        return (ticket_id, locate_id)

    def delete(self, ticket_id):
        for locate_id in self._tickets[ticket_id]['locates']:
            tools.safedelete(self._locates, locate_id)
            tools.safedelete(self._locate_client, locate_id)
            tools.safedelete(self._locate_client_code, locate_id)
            tools.safedelete(self._assignments, locate_id)
        tools.safedelete(self._tickets, ticket_id)

