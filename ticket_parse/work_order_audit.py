# work_order_audit.py

import sqlmap
import static_tables

class WorkOrderAuditHeader(sqlmap.SQLMap):
    __table__ = "wo_audit_header"
    __key__ = "wo_audit_header_id"
    __fields__ = static_tables.wo_audit_header

    def __init__(self):
        sqlmap.SQLMap.__init__(self)
        self.entries = []

    def insert(self, tdb):
        woh_id = sqlmap.SQLMap.insert(self, tdb)
        wod_ids = [] # ids for entries
        for entry in self.entries:
            entry.wo_audit_header_id = woh_id
            wod_id = entry.insert(tdb)
            wod_ids.append(wod_id)
        return (woh_id, wod_ids)

    @classmethod
    def load(cls, tdb, woh_id):
        # NB: 'super' is actually useful/necessary here... :-/
        header = super(WorkOrderAuditHeader, cls).load(tdb, woh_id)
        details = WorkOrderAuditDetail.load_multiple(tdb, woh_id, cls.__key__)
        header.entries = details
        return header

class WorkOrderAuditDetail(sqlmap.SQLMap):
    __table__ = "wo_audit_detail"
    __key__ = "wo_audit_detail_id"
    __fields__ = static_tables.wo_audit_detail

