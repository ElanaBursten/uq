# test_dbinterface.py
# XXX Needs more testing, with different kinds of fields and everything

import unittest
#
import dbengine
import dbinterface

class TestDBInterface(unittest.TestCase):

    def setUp(self):
        self.dbi = dbinterface.detect_interface()

    def test_getrecords(self):
        rows = self.dbi.getrecords('client', call_center="Atlanta",
               active=1)
        self.assertTrue(len(rows))
        self.assertTrue(all(row['active'] == '1' for row in rows))

    def test_getrecords_ordered(self):
        rows = self.dbi.getrecords_ordered('client', 'oc_code',
               call_center="Atlanta", active=1)
        self.assertTrue(len(rows))
        self.assertTrue(all(row['active'] == '1' for row in rows))

        # check if they are really ordered
        names = [row['oc_code'] for row in rows]
        self.assertEquals(names, sorted(names))

    def test_delete_and_insert(self):
        self.dbi.delete("call_center", cc_code="TEST1")
        self.dbi.insert("call_center", cc_code="TEST1", cc_name="Test1")

    def test_update(self):
        import date
        self.dbi.update('call_center', 'cc_code', 'Atlanta',
          notes="blah blah", modified_date=date.Date().isodate())

    def test_count(self):
        count = self.dbi.count('client', call_center="Atlanta", active=1)
        self.assertTrue(count > 0)

    def test_call(self):
        rows = self.dbi.call('get_pending_responses', 'Atlanta')

    def test_get_max_id(self):
        pass

    def test_get_ids_after(self):
        pass

    def test_dbexception(self):
        bad_sql = "SELECT * FROM BOGUS_TABLE"
        try:
            self.dbi.runsql_result(bad_sql)
        except dbengine.DBException, e:
            self.assertEquals(len(e.args), 2)
            self.assertEquals(e.data['sql'], bad_sql)
        else:
            self.fail("DBException expected")


