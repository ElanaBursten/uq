# sqltools.py
# Auxiliary functions for generic SQL.

def sql_with_conditions(base_sql, **criteria):
    """ Create a complete SQL statement from a base (the first part of the
        statement) and a dict of parameters. Returns that SQL statement with
        '?' as parameter placeholders, plus a list of parameter values.

        Example:
        sql_with_params("SELECT * FROM TABLE", foo=1, bar="BLAH")
        => ("SELECT * FROM TABLE WHERE 1=1 AND foo = ? AND bar = ?",
            [1, "BLAH"])
    """

    params = []
    sql = base_sql
    if 'where' not in sql.lower():
        sql += ' where 1=1' # add dummy WHERE clause

    for name, value in criteria.items():
        if value == 'IS NOT NULL':
            sql += ' AND %s IS NOT NULL' % name
        elif value == 'IS NULL' or value is None:
            sql += ' and %s IS NULL' % name
        else:
            sql += ' and %s = ?' % name
            params.append(value)

    return sql, params

def sql_with_set(base_sql, **params):
    """ Like sql_with_conditions, but for SQL statements that set fields, like
        INSERT and UPDATE. """
    params = []
    sql = base_sql
    if 'where' not in sql.lower():
        sql += ' where 1=1' # add dummy WHERE clause

    for name, value in criteria.items():
        sql += ' AND %s = ?' % name
        params.append(value)

    return sql, params

def fields_and_values(dict):
    return zip(*dict.items())
    #fieldnames = dict.keys()
    #values = [dict[fn] for fn in fieldnames]
    #return fieldnames, values


