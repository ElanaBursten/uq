# test_dbengine_ado_win32.py

# TODO: once we have .NET modules, maybe we can reuse this test by switching
# the engine?

import sys; sys.path.append("..")
import unittest
#
import dbengine_ado_win32 as dbengine
import datadir

class TestDBEngineADOWin32(unittest.TestCase):

    def test_connection(self):
        """ Test basic connectivity. Assumes we have a data dir with a valid
            database configuration. """
        dbe = dbengine.DBEngineADOWin32()
        try:
            dbe.check_connection() # raises error if not connected
        except:
            self.fail("connection expected")

    def test_runsql(self):
        dbe = dbengine.DBEngineADOWin32()
        sql = "delete from call_center where cc_code = 'TEST'"
        dbe.runsql(sql)

    def test_runsql_result(self):
        dbe = dbengine.DBEngineADOWin32()
        sql = "select top 10 * from call_center"
        rows = dbe.runsql_result(sql)
        self.assertEquals(len(rows), 10)

        # note: depends on presence of this record
        sql = "select * from call_center where cc_code = 'Atlanta'"
        rows = dbe.runsql_result(sql)
        self.assertEquals(len(rows), 1)
        d = rows[0]
        self.assertTrue(isinstance(d, dict))
        self.assertEquals(d['cc_code'], 'Atlanta')
        self.assertEquals(d['cc_name'], "Utilities Protection Center (Irth)")

    def test_runsql_result_multiple(self):
        dbe = dbengine.DBEngineADOWin32()
        sql = """
            select top 5 * from call_center;
            select top 5 * from client
        """
        sets = dbe.runsql_result_multiple(sql)
        self.assertEquals(len(sets), 2)
        self.assertEquals(len(sets[0]), 5)
        self.assertEquals(len(sets[1]), 5)

    def test_runsql_with_parameters(self):
        dbe = dbengine.DBEngineADOWin32()
        sql = "select * from call_center where cc_code = ?"
        rows = dbe.runsql_with_parameters(sql, ["Atlanta"])
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['cc_code'], 'Atlanta')

