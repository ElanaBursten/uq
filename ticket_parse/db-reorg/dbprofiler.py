# dbprofiler.py
# Simple database profiler.

# TODO:
# - More descriptive names for methods

import time

class Profiler(object):
    def __init__(self):
        self.data = []
    def register(self, name, start_time, stop_time):
        self.data.append((name, start_time, stop_time))
        #print ">>", name, stop_time - start_time

def get_name(f):
    if hasattr(f, 'func_name'):
        name = f.func_name
    elif hasattr(f, 'im_func'):
        name = "%s.%s" % (f.im_class, f.im_func.func_name)
    else:
        name = "?"
    return name

def profile(profiler):
    """ Very simple decorator that hooks a function or method up to a
        Profiler instance, so its performance can be measured. """
    def deco(f):
        def _(*args, **kwargs):
            name = get_name(f)
            t1 = time.time()
            result = f(*args, **kwargs)
            t2 = time.time()
            profiler.register(name, t1, t2)
            return result
        return _
    return deco

if __name__ == "__main__":

    prof = Profiler()

    @profile(prof)
    def idle(n):
        for i in range(n): pass

    idle(1000000)

    class Foo:
        @profile(prof)
        def bar(self, x, y):
            for i in range(x*y): pass

    foo = Foo()
    foo.bar(1000, 2000)

    for name, t1, t2 in prof.data:
        print ":: %s (%0.3fs)" % (name, t2-t1)


