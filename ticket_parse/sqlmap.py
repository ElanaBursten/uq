# sqlmap.py
# Object-relational mapping class.

import string
import sqlmaptypes as smt

class SQLMap(object):
    __table__ = ""
    __key__ = ""
    __fields = [
        # in subclasses, fill with tuples
        # (fieldname, sqltype)
    ]

    def __init__(self):
        self._data = {}
        for name, sqltype in self.__fields__:
            self._data[name] = None
        self._changed = {}
        self.debug = 0

    def __getattr__(self, name):
        return self._data[name]

    def __setattr__(self, name, value):
        if self.__dict__.has_key('_data') and self._data.has_key(name):
            if not self._changed.has_key(name):
                self._changed[name] = self._data[name]
            self._data[name] = value
        else:
            self.__dict__[name] = value

    def __repr__(self):
        return "<%s(%r) at 0x%x>" % (self.__class__.__name__,
               self._data[self.__key__], id(self))

    def set(self, **kwargs):
        """ Set a number of fields. """
        for key, value in kwargs.items():
            if key in [f[0] for f in self.__fields__]:
                setattr(self, key, value)
            else:
                raise KeyError, "Name '%s' not in fields" % (key,)

    #
    # creating/loading objects

    @classmethod
    def load(cls, tdb, id):
        d = {cls.__key__: id}
        rows = tdb.getrecords(cls.__table__, **d)
        if not rows:
            raise KeyError, "Table %s: %s %r not found" % (cls.__table__,
                  cls.__key__, id)

        obj = cls() # create "empty" object
        for key, value in rows[0].items():
            if key in [f[0] for f in cls.__fields__]:
                obj._data[key] = value
                # don't use setattr() here

        obj.reset_changes() # treat as new, so no changes

        return obj

    # NOTE:
    # For more complex objects, like Locate and Ticket, there should be a way
    # to do this differently.  We could override the method, and/or provide
    # a way to pass in lists of tickets, locates and assignments.

    @classmethod
    def load_from_row(cls, row, **fields):
        """ Create an instance from a "row" (dictionary).  <fields> can be used
            to set any special fields, like parent keys, etc. """
        obj = cls() # create "empty" object
        field_names = map(lambda first: first[0],cls.__fields__)
        for key, value in row.items():
            if key in field_names:
                obj._data[key] = value

        # set special fields, if any
        for key, value in fields.items():
            if key in field_names:
                obj._data[key] = value

        obj.reset_changes()

        return obj

    @classmethod
    def load_from_rows(cls, rows, **fields):
        """ Like load_from_row, but return a list of rows. """
        return [cls.load_from_row(row, **fields) for row in rows]

    @classmethod
    def load_multiple(cls, tdb, id, link_key):
        d = {link_key: id}
        rows = tdb.getrecords(cls.__table__, **d)
        return cls.load_from_rows(rows, **d)

    @classmethod
    def new(cls, **fields):
        obj = cls()
        obj.set(**fields) # uses setattr()
        return obj

    #
    # insert

    def _sql_insert(self, get_id=1, vars={}):
        """ Generate an SQL string for a parametrized query.  Return the SQL
            string and a list of 2-tuples (value, sqltype).
        """
        # get all fields that have a value that we want to store
        fields = [(field, sqltype) for (field, sqltype) in self.__fields__
                 if self._data[field] is not None
                 and field not in vars.keys()]

        # get the field names and their values for the SQL statement
        fieldnames = [t[0] for t in fields]
        fieldvalues = ["?" for _ in fields]

        # if we have special variables, add them
        for key, value in vars.items():
            fieldnames.append(key)
            fieldvalues.append(value)

        # create the SQL statement
        fieldstr = string.join(fieldnames, ", ")
        valuestr = string.join(fieldvalues, ", ")
        sql = "insert %s (%s) values (%s)" % (self.__table__, fieldstr, valuestr)
        # use scope_identity in the same scope, so we can get the id back
        # (it won't work if we do it in a separate statement!)
        if get_id:
            sql += "\nselect scope_identity() as id"
        values = [(self._data[field], sqltype, field)
                  for field, sqltype in fields]
        return sql, values

    def insert(self, tdb):
        sql, values = self._sql_insert()

        if self.debug:
            import pprint
            print "[debug/insert]", sql
            pprint.pprint(values)

        # pass SQL to ADO object... using tdb
        rows = tdb.runsql_with_parameters(sql, values)
        # get id back
        id = rows[0]['id'] # must always be present
        # insert the new id, so SQLMap knows that it should update the
        # next time
        setattr(self, self.__key__, id)
        self.reset_changes()

        return id

    #
    # update

    def _sql_update(self, force=0):
        fields = [(field, sqltype) for (field, sqltype) in self.__fields__
                  if self._data[field] is not None
                  and (force or self._changed.has_key(field))
                  and field != self.__key__]

        set_clause_list = []
        values = []
        for field, sqltype in fields:
            set_clause_list.append(field)
            value = self._data[field]
            values.append((value, sqltype, field))
        set_clause = string.join(["%s = ?" % (f,) for f in set_clause_list],
                     ", ")
        t = (self.get_id(), self.find_sqltype(self.__key__), self.__key__)
        values.append(t)
        sql = "update %s set %s where %s = ?" % (self.__table__, set_clause,
              self.__key__)
        return sql, values

    def update(self, tdb, force=0):
        self.reset_stats()
        if self.changed() or force:
            sql, values = self._sql_update(force=force)
            tdb.runsql_with_parameters(sql, values)
            self.stats['updated'].append(self.get_id())
        else:
            self.stats['skipped'].append(self.get_id())
        self.reset_changes()
        return self.get_id()

    #
    # auxiliary methods

    def isnew(self):
        return self._data[self.__key__] is None

    def changed(self):
        """ Returns true if the record has changed. """
        return bool(self._changed)

    def reset_changes(self):
        self._changed = {}

    def rollback_changes(self):
        for name, value in self._changed.items():
            setattr(self, name, value)
        self.reset_changes()

    def save(self, tdb, force=0):
        # if the key field is defined, it's considered an update, otherwise
        # it's an insert
        if self.isnew():
            return self.insert(tdb)
        else:
            return self.update(tdb, force=force)

    def get_id(self):
        return self._data[self.__key__]

    def find_sqltype(self, fieldname):
        for field, sqltype in self.__fields__:
            if field == fieldname:
                return sqltype
        raise KeyError, "Field not found: %s" % (fieldname,)

    def reset_stats(self):
        self.stats = {'inserted': [], 'updated': [], 'skipped': []}

    def clear_id(self):
        self._data[self.__key__] = None

    #
    # ensure correctness

    def check_field_lengths(self):
        """ Check field lengths by comparing lengths of actual (string) values
            to allowed lengths.  Returns a list of tuples (field name, max
            length, actual length, actual value) tuples. """
        toolong = []
        stringfields = [(name, fielddef.width)
                        for (name, fielddef) in self.__class__.__fields__
                        if isinstance(fielddef, smt.SQLString)]
        for fieldname, fieldlength in stringfields:
            value = self._data.get(fieldname, None)
            try:
                reallength = len(value)
            except TypeError:
                reallength = 0
            if reallength > fieldlength:
                # field name, max length, actual length, actual value
                toolong.append((fieldname, fieldlength, reallength, value))

        return toolong

    @classmethod
    def update_directly(cls, tdb, id, timeout=None, **kwargs):
        return tdb.update(cls.__table__, cls.__key__, id, timeout=timeout,
               **kwargs)

    #
    # representation

    def long_repr(self):
        """ Return a long string representation of the object, showing all
            attributes. """
        z = []
        items = self.__dict__.items()
        items.sort()
        for key, value in items:
            if not key.startswith("_"):
                s = "%s = %s" % (key, repr(value))
                z.append(s)
        return string.join(z, "\n")


