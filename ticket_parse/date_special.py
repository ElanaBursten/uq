# date_special.py
# Special date routines. ("Special" meaning that they are usually dependent
# on a type of ticket.)
#
# When an argument is called "date", it's usually an instance of Date.

from date import Date
import sys
import string

def read_holidays(tdb):
    holidays = []
    records = tdb.read_holiday_records()
    for rec in records:
        call_center = rec["cc_code"].encode("utf-8")
        holiday_date = rec["holiday_date"].encode("utf-8")[:10]
        holidays.append([holiday_date, call_center])

    return holidays

def isholiday(date, callcenter, holiday_list):
    """ Return true if the given date (Date instance) is a holiday for the
        given call center. """
    ds = date.isodate().split()[0]
    for holiday, hcc in holiday_list:
        if holiday == ds:
            if hcc == "*":
                return 1    # this applies to all call centers
            elif callcenter == hcc:
                return 1
    # apparently we didn't find it
    return 0


if __name__ == "__main__":

    import ticket_db

    tdb = ticket_db.TicketDB()
    holidays = read_holidays(tdb)
    for holiday in holidays:
        print holiday

    # move this to a test
    print isholiday(Date("2002-07-04"), "BLA", holidays)    # should be 1
    print isholiday(Date("2002-07-05"), "FCV3", holidays)   # ...1
    print isholiday(Date("2002-07-05"), "FOL1", holidays)   # ...0

