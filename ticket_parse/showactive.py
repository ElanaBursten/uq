# showactive.py
# Show the Python processes that are currently running (and have a mutex lock).
# Typically run as a script; is also used as a module by watchdog.py.

import dbinterface_ado
import program_status
import mutex

def get_running_processes(dbado):
    """ Return a list of tuples, each tuple representing a process that is
        currently running, of the form (name, pid, start_date). """
    processes = []
    for name in mutex.locked_mutexes(dbado):
        status = program_status.read_status(name)
        if status:
            pid, startdate = status
        else:
            pid, startdate = "?", "?"
        s = (name, pid, startdate)
        processes.append(s)

    return processes


if __name__ == "__main__":
    print "The following processes with mutex-lock are currently active:"
    print
    print "Name                            PID     Active since"
    print "------------------------------  ------  -------------------"

    for proc in get_running_processes(dbinterface_ado.DBInterfaceADO()):
        name, pid, startdate = proc
        print "%-30s  %-6s  %s" % (name, pid, startdate)

