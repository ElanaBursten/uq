# increase_version.py

import os
import string
import xml.etree.ElementTree as ET
#
import et_tools
from version import __version__

try:
    version = int(__version__)
except:
    version = 0

version_file = None
if os.path.exists(os.path.join(os.getcwd(), "staging")):
    path = os.path.join(os.getcwd(), "staging")
else:
    # Likely to be in the tests directory
    path = os.path.join(os.getcwd(), "..")

if __name__ == "__main__":

    for root, dirs, files in os.walk(path):
        for file in files:
            if file == "version.py":
                version_file = os.path.join(root, file)
                break
        if version_file:
            break

    res = os.popen('svn status --xml -v %s' % version_file)
    svn_xml = res.readlines()
    doc = ET.fromstring(string.join(svn_xml))
    svn_version = et_tools.safe_find(doc, 'commit').get('revision')

    g = open('version.dat', "w")
    g.write(svn_version)
    g.close()


