# ticket.py

# Note: When posting a ticket, don't set datetime fields to "", this will
# cause an error.  Other combinations will not cause an error, like putting
# an integer in ticket_number, or a string "0.0" in work_lat, etc.

import re
#
import locate
import string
import sqlmap
import static_tables
import tools

LAT_LONG_DEFAULT = '0.0'

class Ticket(sqlmap.SQLMap):
    # Note: Attribute names starting with an underscore will not be stored
    # automatically.

    # XXX Since we store all the lengths here anyway, we might as well use
    # it to determine if strings are too long.  So in the future we won't
    # need to load the schema file anymore.  Eventually we can get these data
    # from the database instead.

    __table__ = "ticket"
    __key__ = "ticket_id"
    __fields__ = static_tables.ticket

    SPECIAL = ["locates", "image", "grids", "debug"]
    # soon, we won't need these anymore

    def __init__(self):
        sqlmap.SQLMap.__init__(self)

        # setting a lot of default values... may or may not be necessary

        self.image = ""
        self.ticket_number = ""
        #self.call_date = None
        self.revision = ""
        self.work_state = ""
        self.work_county = ""
        self.work_city = ""
        self.work_address_number = ""   # primary number, if any
        self.work_address_number_2 = "" # secondary number ("to"), if any
        self.work_address_street = ""
        self.work_cross = ""
        self.work_subdivision = ""
        self.work_description = ""  # was: location_type
        #self.grids = [] # For now, only in Atlanta parser
        self.work_type = ""
        #self.work_date = None
        self.work_notc = ""
        self.priority = ""
        #self.legal_date = None
        #self.legal_good_thru = ""
        #self.legal_restake = ""
        #self.respond_date = None
        self.duration = ""

        # contractor data
        self.company = ""       # "shortname" of contractor
        self.con_type = ""
        self.work_remarks = ""
        self.con_name = ""      # "full name" of contractor
        self.con_address = ""   # address of contractor
        self.con_city = ""      # city of contractor
        self.con_state = ""     # state of contractor
        self.con_zip = ""

        # caller data
        self.caller = ""
        self.caller_phone = ""
        self.caller_fax = ""
        self.caller_altphone = ""
        self.caller_email = ""
        self.caller_cellular = ""
        self.caller_contact = ""

        self.operator = ""  # operator who took the call
        self.channel = ""   # channel through which the ticket was received
        self.locates = []
        self.kind = ""

        self.work_long = LAT_LONG_DEFAULT  # these are numeric fields; I prefer
        self.work_lat = LAT_LONG_DEFAULT   # everything to be a string though,
                                           # because no computation is done

        self.parsed_ok = 1      # default
        self.ticket_format = "unknown" # e.g. "Atlanta"
        self.parse_errors = None

        self.caller_altcontact = ""
        self.due_date = None    # was: mark_by_date
        #self.legal_due_date = None
        #self.transmit_date = None
        self.ticket_type = ""
        #self.do_not_mark_before = None
        #self.do_not_respond_before = None

        self.map_page = ""   # we don't always fill this from the parser
        self.service_area_code = None

        self._filedate = None   # date/time of file that ticket came in

        self.route_area_id = None
        # will be set by router to the area_id of the locator it found

        self._update_call_center = None
        # is set when we update a call center, e.g. FMW2 -> FMW1. ticket_format
        # is set to FMW1, and _update_call_center contains the original value
        # FMW2.
        self._dup_fields = []
        # fields that are found multiple times; this causes the main program to
        # issue a warning

        self.explosives = ""
        #self.serial_number = None # some kind of alternative ticket number
                                  # so far, only for FMW2
        self.map_ref = ""       # map reference used for billing
                                # so far, only for FTS1

        self.grids = []         # list of all grids on the ticket

        self._hp_info = []

    def get_fields_for_posting(self):
        """ Return a list of tuples (name, value) for all fields that can be
            posted to the database.
        """
        return [(name, value) for name, value in self.__dict__.items()
                if not name.startswith("_")
                and name not in self.SPECIAL]

    def get_fields_for_posting_with_value(self):
        """ Return a list of tuples (name, value) for all fields that can be
            posted and actually have a value (!= None).
        """
        return [(name, value) for (name, value) in self.get_fields_for_posting()
                if value is not None]

    def __eq__(self, other):
        """ Compare two tickets. Only the == operator works in this way;
            comparisons with > and < don't seem to make much sense.
            # XXX Some fields cannot really be compared, like
            # modified_date...
        """
        for key, value in self.__dict__.items():
            if not key.startswith("_"):
                try:
                    yours = getattr(other, key)
                except AttributeError:
                    # if the other object lacks this attribute, it's
                    # apparently not going to compare equal
                    return 0
                if value != yours:
                    return 0    # uh-oh, these attributes are not equal
        # apparently we could not find any differences
        return 1

    def equal(self, other):
        """ As __eq__, but ignores certain fields. modified_date for example
            is not known here, but is set in the tickets retrieved from the
            database.
        """
        IGNORE = ["image", "modified_date"]
        for key, value in self.__dict__.items():
            if not key.startswith("_") and not key in IGNORE:
                try:
                    yours = getattr(other, key)
                except AttributeError:
                    return 0
                if value != yours:
                    return 0
        return 1

    def _cmp_attr(self, other, attrname):
        """ Compare attribute of 'self' and 'other', using some sleight of
            mind to make 'None' compare equal to an empty string, etc.
        """
        # XXX To be implemented...

    def tostring(self):
        print "Ticket:"
        items = self.__dict__.items()
        items.sort()
        for key, value in items:
            print "%s = (length %d) '%s'" % (key, len(str(value)), str(value))

    # XXX replace with SQLMap.long_repr
    def repr(self): # note: not __repr__
        """ Return a long string representation of the ticket, showing all
            attributes. """
        z = []
        items = self.__dict__.items()
        items.sort()
        for key, value in items:
            if not key.startswith("_"):
                s = "%s = %s" % (key, repr(value))
                z.append(s)
        return string.join(z, "\n")

    #
    # Ticket likes to pretend to be a dict:

    def __getitem__(self, name):
        if self._data.has_key(name):
            return self._data[name]
        else:
            return getattr(self, name)

    def __setitem__(self, name, value):
        if self._data.has_key(name):
            self._data[name] = value
        else:
            setattr(self, name, value)

    def get(self, attr, default=None):
        if self._data.has_key(attr):
            return self._data[attr]
        elif hasattr(self, attr):
            return getattr(self, attr)
        else:
            return default

    #
    # needs better implementations of insert, update, load

    # NOTE: insert(), update() etc aren't wrapped in transactions.  The reason
    # is that they are usually called as part of a bigger set of actions, which
    # *is* wrapped in a transaction.  Since transactions cannot be nested, I
    # have to omit it here. :-(

    def insert(self, tdb):
        # insert ticket first (& get id back)
        ticket_id = sqlmap.SQLMap.insert(self, tdb)
        # then insert locates
        loc_ids = []
        for loc in self.locates:
            loc.ticket_id = ticket_id
            loc_id = loc.save(tdb)
            loc_ids.append(loc_id)

        return (ticket_id, loc_ids)

    def update(self, tdb):
        assert self.ticket_id, "Ticket.update: ticket_id not set"
        # save ticket
        ticket_id = sqlmap.SQLMap.update(self, tdb)
        # loop over locates and save them
        for loc in self.locates:
            loc.ticket_id = ticket_id
            loc.save(tdb) # takes care of assignments as well

        loc_ids = [loc.locate_id for loc in self.locates]
        return ticket_id, loc_ids

    @classmethod
    def load(cls, tdb, _id):
        results = tdb.get_ticket_data(_id)
        tickets, locates, assignments = results
        t = cls.load_from_ticket_data(tickets, locates, assignments)
        return t

    @classmethod
    def load_from_ticket_data(cls, tickets, locates, assignments):
        ticket = super(Ticket, cls).load_from_row(tickets[0])
        ticket.locates = [locate.Locate.load_from_data(locrow, assignments)
                          for locrow in locates]
        # note that ticket_id should be already set in the locate row
        return ticket

    def clear_id(self):
        """ Call if a ticket cannot be posted and the transaction is rolled
            back... this will reset the ids of the ticket, locates, and
            assignments.  Useful if the ticket still needs to be used after
            that. """
        sqlmap.SQLMap.clear_id(self)
        for loc in self.locates:
            loc.ticket_id = None
            loc.clear_id()

    #
    # SQLGatherer stuff

    # for now
    def insert_sg(self, sg):
        """ Insert ticket, locates, any assignments, etc, using batch SQL. """

        # remove any IDs we might have... this is an insert, after all
        self.ticket_id = None
        for loc in self.locates:
            loc.locate_id = loc.ticket_id = None
            if loc._assignment:
                loc._assignment.locate_id = loc._assignment.assignment_id = None

        ticvar = '@T1'
        self._var = ticvar[1:]
        sg.declare_var(ticvar)
        sql, values = self._sql_insert(get_id=0)
        sg.add_sql(sql, values)
        sg.select_scope_identity(ticvar)

        # insert each locate on this ticket
        for idx, loc in enumerate(self.locates):
            locvar = '@L' + str(idx+1)
            loc._var = locvar[1:]
            sg.declare_var(locvar)
            sql, values = loc._sql_insert(get_id=0, vars={'ticket_id': ticvar})
            sg.add_sql(sql, values)
            sg.select_scope_identity(locvar)

            # does it have an assignment? if so, insert that as well
            if loc._assignment:
                asgvar = '@A' + str(idx+1)
                loc._assignment._var = asgvar[1:]
                sg.declare_var(asgvar)
                sql, values = loc._assignment._sql_insert(get_id=0,
                              vars={'locate_id': locvar})
                sg.add_sql(sql, values)
                sg.select_scope_identity(asgvar)

        sg.return_vars()

        # we have to call sg.execute() here, because we need to set ticket_id
        # and friends
        rows = sg.execute()
        ids = rows[0]

        # set all the IDs
        self.ticket_id = ids[self._var]
        for loc in self.locates:
            loc.ticket_id = self.ticket_id
            loc.locate_id = ids[loc._var]
            if loc._assignment:
                loc._assignment.locate_id = loc.locate_id
                loc._assignment.assignment_id = ids[loc._assignment._var]

        # prepare a similar return value as insert()
        locids = [loc.locate_id for loc in self.locates]
        return (self.ticket_id, locids)

    def update_sg(self, sg):
        # 1. ticket must exist and have an id already, so called update()
        sql, values = self._sql_update()
        if len(values) > 1:
            # only do this if there are actual values to update
            sg.add_sql(sql, values)

        # 2. for locates, call either insert or update depending on whether
        #    they're new or not... and whether they need updating or not.
        for idx, loc in enumerate(self.locates):
            if loc.ticket_id:
                if loc.changed():
                    # update
                    sql, values = loc._sql_update()
                    if len(values) > 1:
                        locvar = '@L' + str(idx+1)
                        loc._var = locvar[1:]
                        sg.declare_var(locvar)
                        sg.add_sql(sql, values)
                        sg.select_scope_identity(locvar)
                        # we still use a var so we can return it...
            else:
                # no ticket_id: insert; code is same as in insert_sg
                locvar = '@L' + str(idx+1)
                loc._var = locvar[1:]
                sg.declare_var(locvar)
                sql, values = loc._sql_insert(get_id=0, vars={'ticket_id': self.ticket_id})
                sg.add_sql(sql, values)
                sg.select_scope_identity(locvar)

            # handle assignments as well... these could be new (not so likely,
            # but possible)
            asg = loc._assignment
            if asg:
                if asg.locate_id:
                    if asg.changed():
                        sql, values = asg._sql_update()
                        if len(values) > 1:
                            asgvar = '@A' + str(idx+1)
                            loc._assignment._var = asgvar[1:]
                            sg.declare_var(asgvar)
                            sg.add_sql(sql, values)
                            sg.select_scope_identity(asgvar)
                else:
                    asgvar = '@A' + str(idx+1)
                    asg._var = asgvar[1:]
                    sg.declare_var(asgvar)
                    sql, values = asg._sql_insert(get_id=0,
                                  vars={'locate_id': locvar})
                    sg.add_sql(sql, values)
                    sg.select_scope_identity(asgvar)

        sg.return_vars()

        # we have to call sg.execute() here, because we need to set ticket_id
        # and friends
        rows = sg.execute()
        if rows:
            ids = rows[0]
        else:
            ids = []

        # set all the IDs (somewhat different from insert_sg)
        for loc in self.locates:
            if not loc.ticket_id and len(ids):
                loc.ticket_id = self.ticket_id
                loc.locate_id = ids[loc._var]
            if loc._assignment and not loc._assignment.locate_id and len(ids):
                loc._assignment.locate_id = loc.locate_id
                loc._assignment.assignment_id = ids[loc._assignment._var]

        # prepare a similar return value as insert()
        locids = [loc.locate_id for loc in self.locates]
        return (self.ticket_id, locids)


    re_wo_number = re.compile(r"BSW WO#(L?A?M?V?[MD|VA]\d+)")
    
    def get_wo_number(self):
        """ If the remarks field of this ticket contains a work order number,
            parse it and return it. In most cases, will return None.
            N.B. This is NOT stored in a field in Ticket (although it could be
            if necessary).
        """
        for fn in ['work_description', 'work_remarks']:
            value = getattr(self, fn, "")
            if value:
                s = tools.re_get(self.re_wo_number, value)
                if s:
                    return s
        return None


def ticket_diff(original, update):
    """ Return a list of fields of ticket <original> that have changed in
        <update>. """
    IGNORE = ["ticket_id"]
    changed = []

    for key, value in original.__dict__.items():
        if key.startswith("_"):
            continue
        v1 = getattr(original, key) # value in original ticket
        v2 = getattr(update, key)   # value in update

        # if v2 is None, leave this field alone
        if v2 != None and v1 != v2:
            changed.append(key)

    # XXX what about locates??
    # XXX untested! write a test for this...

    return changed

