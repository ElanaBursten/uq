# archiver.py
# Archives processed tickets in a controlled manner.
# Created: 2002.07.03 HN

import getopt
import os
import posixpath
import shutil
import string
import sys
import time
import traceback
import tarfile
#
import call_centers
import config
import datadir
import date
import errorhandling2
import filetools
import tools

__usage__ = """\
archiver.py [options] command [call_centers...]

Options:
    -d date      Process like <date> is the current date.  Must be of the
                 form YYYY-MM-DD.

Commands (exactly one of these must be specified):
    m   Make archives, don't delete source files.
    d   Make archives, then delete source files.
    t   Do everything except making archives and deleting source files.

If no call centers are specified, all are assumed.

"""

"""
Archive format selection:
  -j, --bzip2                        filter the archive through bzip2

Local file selection:
  -T, --files-from=NAME        get names to extract or create from file NAME
      --null                   -T reads null-terminated names, disable -C
      --exclude=PATTERN        exclude files, given as a globbing PATTERN

"""

def convert_unc_path(path):
    if path.startswith("\\\\"): # Windows UNC path
        pass
    return path

class Archiver:

    def __init__(self, make=0, delete=0, current_date=None, verbose=1):
        self.config = config.getConfiguration()
        self.make = make
        self.delete = delete
        self.current_date = current_date
        self.verbose = verbose
        self.to_be_archived = []

        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="TicketArchiver",
                   admin_emails=self.config.admins,
                   subject="Ticket Archiver Error Notification")
        self.log.defer_mail = 1

    def log_event(self, msg):
        self.log.log_event(msg, dump=self.verbose)

    def prepare(self):
        self.archive_date = self.find_archive_date()
        self.log_event("Archive date is %s" % (self.archive_date[:10]))

    def archive_all(self, call_centers_to_archive=None):
        self.log_event("Start of archiver (archive all)")
        self.prepare()
        for process in self.config.processes:
            call_center = process['format'] #process[1]
            processed_dir = process['processed'] #process[2]
            if call_centers_to_archive is None or call_center in call_centers_to_archive:
                if call_center and processed_dir:
                    self.archive(call_center, processed_dir)
            else:
                print "Skipping:", call_center

        if not call_centers_to_archive:
            for additional_dir in self.config.archiver['additional_dirs']:
                name = os.path.split(additional_dir)[1]
                self.archive(name, additional_dir)

    def archive(self, call_center, processed_dir):
        self.log_event("Archiving: %s" % (call_center,))
        self.to_be_archived = []
        if not os.path.exists(processed_dir):
            self.log.log_event("Error: directory %r does not exist or is unreachable" % (processed_dir,))
            return

        self.find_files_to_be_archived(processed_dir)

        if self.make:
            self.make_archive(call_center)
        else:
            # test mode
            if self.to_be_archived:
                print "These files would be put in the archive:"
                for file in self.to_be_archived:
                    print "-", file
            else:
                print "No files would be put in the archive."

    def make_archive(self, name):
        if not self.to_be_archived:
            print "No files found for archive."
            return
        self.run_archiver(name)
        ok = self.verify_archive(name)
        if ok:
            self.move_archive(name)
            filetools.remove_file("files")   # just in case
            if self.delete:
                self.delete_files()

    def find_files_to_be_archived(self, dir):
        files = os.listdir(dir)
        self.log_event("%d files found" % (len(files),))

        # this can be sped up by checking if the filename contains certain
        # dates... e.g. "20050530" or "2005-05-30", etc.

        for file in files:
            fullname = os.path.join(dir, file)
            atime, mtime, ctime = os.stat(fullname)[-3:]
            t9 = time.localtime(mtime)
            ds = "%04d-%02d-%02d %02d:%02d:%02d" % t9[:6]
            if ds <= self.archive_date:
                self.to_be_archived.append(fullname)
                self.log_event("File marked for archiving: %s" % (fullname,))

    def delete_files(self):
        self.log_event("Deleting files...")
        for file in self.to_be_archived:
            try:
                os.remove(file)
            except:
                self.log_event("File could not be removed: %s" % (file,))
        self.log_event("OK (deletion complete)")

    def verify_archive(self, name):
        """ Verify if all files are really in the archive. """
        # strip any paths so we only have the filename
        self.log_event("Verifying archive...")
        filenames = [os.path.split(fn)[1] for fn in self.to_be_archived]
        archive = self._archive_name(name)
        tf = tarfile.open(archive,'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        if len(files_in_archive) != len(self.to_be_archived):
            self.log_event("Archive not correct! %d files not found in archive"
              % len(filenames))
            for fname in self.to_be_archived:
                if not fname in files_in_archive:
                    self.log_event("%s file not found in archive" % (fname,))
                    return 0
        self.log_event("OK")
        tf.close()
        return 1

    def move_archive(self, name):
        filename = self._archive_name(name)
        targetdir = self.config.archiver['archive_dir']
        self.log_event("Moving archive: %s -> %s" % (filename, targetdir))
        # move it, overwriting existing files with the same name...
        try:
            shutil.move(filename, targetdir)
        except:
            self.log_event("Could not move file to %s" % (targetdir,))
            traceback.print_exc()
        else:
            self.log_event("OK")

    def run_archiver(self, name):
        archive = self._archive_name(name)
        tf = tarfile.open(archive,'w|bz2')
        for fname in self.to_be_archived:
            tf.add(fname)
        tf.close()
        self.log_event("OK")

    def write_file_list(self, files):
        """ Write a file list to the current directory. """
        self.log_event("Writing file list...")
        f = open("files", "w")
        for file in files:
            print >> f, convert_unc_path(file)
            # convert UNC paths to something like //10.1.1.25/c$/foo/bar
            # so that cygwin's tar can handle them
        f.close()
        self.log_event("OK")

    def find_archive_date(self):
        """ Determine the archive date, using the current date as a starting
            point (or self.current_date, if it's defined).
            From the current date, it goes back at least one day, but as
            many days as necessary until a Sunday is found.  (IOW, the
            resulting archive date should always be a Sunday.) """
        if self.current_date:
            d = date.Date(self.current_date)
            self.log_event("Specified archive date is %s" % (d))
        else:
            d = date.Date() # today
            self.log_event("Specified archive date is today's date %s" % (d))
        d.dec() # go back at least a day
        # go back until it finds a Sunday
        while d.weekday() != 6:
            d.dec()
        d.hours, d.minutes, d.seconds = 23, 59, 59
        return d.isodate()

    def _archive_name(self, name):
        """ Create and return archive name (with full path).
            Currently looks like e.g. 'FCO1-2004-04-26.tar.bz2'.
        """
        archive = name + "-" + self.archive_date[:10] + ".tar.bz2"
        return archive


if __name__ == "__main__":

    COMMANDS = ("m", "d", "t")
    delete = 0  # delete files after processing
    make = 0    # make files
    current_date = None

    opts, args = getopt.getopt(sys.argv[1:], "d:")

    if not args or ("-?", "") in opts:
        print >> sys.stderr, __usage__
        sys.exit(0)
    for o, a in opts:
        if o == "-d":
            current_date = a

    command, call_centers = args[0].lower(), args[1:]
    if not command in COMMANDS:
        print >> sys.stderr, "Error: must specify command m, d or t"
        print >> sys.stderr, __usage__
        sys.exit(0)

    if command == "m":
        make = 1
    elif command == "d":
        make = delete = 1
    elif command == "t":
        make = delete = 0   # stay as they are

    if not call_centers:
        call_centers = None

    arch = Archiver(make=make, delete=delete, current_date=current_date)
    arch.archive_all(call_centers_to_archive=call_centers)
