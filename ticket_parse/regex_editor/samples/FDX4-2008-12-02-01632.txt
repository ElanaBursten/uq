

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    083371791         Old Ticket:       083370068         
Priority:         Emergency         By:               Angie C           
Source:           Voice             Hours Notice:     0                 
Type:             Emergency         Date:             12/2/2008 10:32:17 AM
Sequence:         3                                                     
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
CITY OF IRVING WATER DEPT.          Type:             Contractor        
333 VALLEY VIEW                     Contact:          WENDY COLLIER     
IRVING, TX 75061                    Caller:           DWAYNE MELANCON   
Phone:            (972) 721-2664    Caller Phone:     (972) 721-2664    
Fax:              (972) 721-2318    Callback:         0800 - 1700       
Alt Contact:                                                            
Caller Email:     WCOLLIER@CITYOFIRVING.ORG                             

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        12/02/08 at 1045  
County:           DALLAS            Type:             WATER             
Place:            IRVING            Done For:         CITY OF IRVING WAT
Street:           2909 WARREN CIR                                       
Intersection:     WELLESLEY DR                                          
Nature of Work:   EMER: WATER MAIN REPAIR                               
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      Yes               Duration:         06 HOURS          
Mapsco:           MAPSCO 31B,G                                          

Remarks
----------------------------------------------------------------------------
RCL-083370068 NEED GAS COMP TO RESP ASAP THIS IS AN EMER: WATER MAIN REPAIR.
CREW ENROUTE.  CUST W/ SVC.  WATER IS VISIBLE.  WORK WILL BE DONE IN THE    
PARKWAY.  AREA IS MARKED W/ WHITE PAINT AND BLUE FLAGS.                     


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
EL5   EXPLORER PIPELINE COMPANY                         No                  
EMF   EXXONMOBIL PIPELINE COMPANY                       No                  
GTL   VERIZON - TX - KELLER GRAPEVINE DISTRICT (METRO...No                  
IR4   ONCOR ELECTRIC DISTRIBUTION-SMP (IRVING)          No                  


Location
----------------------------------------------------------------------------
Latitude:         32.8461183920956  Longitude:        -96.9443921412614 
Second Latitude:  32.8428883451454  Second Longitude: -96.942692610744

