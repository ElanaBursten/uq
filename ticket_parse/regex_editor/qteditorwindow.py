# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interface.ui'
#
# Created: Sat Jan 24 18:06:03 2009
#      by: PyQt4 UI code generator 4.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_editorwindow(object):
    '''
    Defines widgets and layouts
    '''
    def setupUi(self, editorwindow):
        editorwindow.setObjectName("editorwindow")
        editorwindow.resize(785,650)
        editorwindow.setMinimumSize(QtCore.QSize(785,650))
        editorwindow.setSizeIncrement(QtCore.QSize(1,1))
        editorwindow.setBaseSize(QtCore.QSize(785,650))

        self.centralwidget = QtGui.QWidget(editorwindow)
        self.centralwidget.setObjectName("centralwidget")

        self.dblistgroup = QtGui.QGroupBox(self.centralwidget)
        self.dblistgroup.setMaximumWidth(191)
        self.dblistgroup.setAutoFillBackground(False)
        self.dblistgroup.setObjectName("dblistgroup")

        self.dbfieldlist = QtGui.QListWidget()
        self.dbfieldlist.setObjectName("dbfieldlist")

        dblistgroup_VBoxLayout = QtGui.QVBoxLayout(self.dblistgroup)
        dblistgroup_VBoxLayout.setSpacing(1)
        dblistgroup_VBoxLayout.addWidget(self.dbfieldlist)

        self.reeditbox = QtGui.QGroupBox(self.centralwidget)
        self.reeditbox.setMinimumSize(381,181)
        self.reeditbox.setSizeIncrement(QtCore.QSize(1,1))
        self.reeditbox.setObjectName("reeditbox")

        self.regexlabel = QtGui.QLabel()
        self.regexlabel.setGeometry(QtCore.QRect(10,20,431,18))
        self.regexlabel.setObjectName("regexlabel")

        # Hbox for label
        regexlabel_HBoxLayout = QtGui.QHBoxLayout()
        regexlabel_HBoxLayout.addWidget(self.regexlabel)

        self.userregex = QtGui.QLineEdit()
        self.userregex.setGeometry(QtCore.QRect(9,40,431,26))
        self.userregex.setObjectName("userregex")

        self.saveregex = QtGui.QPushButton()
        self.saveregex.setEnabled(False)
        self.saveregex.setFixedWidth(120)
        self.saveregex.setObjectName("saveregex")

        # Hbox for predefined or user regex and save button
        userregex_HBoxLayout = QtGui.QHBoxLayout()
        userregex_HBoxLayout.addWidget(self.userregex)
        userregex_HBoxLayout.addWidget(self.saveregex)

        self.autoregex = QtGui.QLineEdit()
        self.autoregex.setGeometry(QtCore.QRect(10,70,431,28))
        self.autoregex.setObjectName("autoregex")

        self.copyautoregex = QtGui.QPushButton()
        self.copyautoregex.setFixedWidth(120)
        self.copyautoregex.setObjectName("copyautoregex")

        # Hbox for auto regex line edit and copy button
        autoregex_HBoxLayout = QtGui.QHBoxLayout()
        autoregex_HBoxLayout.addWidget(self.autoregex)
        autoregex_HBoxLayout.addWidget(self.copyautoregex)

        self.ignorecase = QtGui.QCheckBox()
        self.ignorecase.setGeometry(QtCore.QRect(10,100,120,21))
        self.ignorecase.setObjectName("ignorecase")
        self.ignorecase.setChecked(True)

        self.dotall = QtGui.QCheckBox()
        self.dotall.setGeometry(QtCore.QRect(140,100,90,21))
        self.dotall.setObjectName("dotall")

        self.highlightautoregex = QtGui.QCheckBox()
        self.highlightautoregex.setEnabled(True)
        self.highlightautoregex.setGeometry(QtCore.QRect(290,100,151,24))
        self.highlightautoregex.setObjectName("highlightautoregex")

        self.restoreauto = QtGui.QPushButton()
        self.restoreauto.setFixedWidth(120)
        self.restoreauto.setObjectName("restoreauto")

        # Hbox for check boxes and restore button
        options_HBoxLayout = QtGui.QHBoxLayout()
        options_HBoxLayout.addWidget(self.ignorecase)
        options_HBoxLayout.addWidget(self.dotall)
        options_HBoxLayout.addWidget(self.highlightautoregex)
        options_HBoxLayout.addStretch()
        options_HBoxLayout.addWidget(self.restoreauto)

        self.line = QtGui.QFrame()
        self.line.setGeometry(QtCore.QRect(10,130,561,16))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")

        # Hbox for Hline
        line_HBoxLayout = QtGui.QHBoxLayout()
        line_HBoxLayout.addWidget(self.line)

        self.erroroutput = QtGui.QLabel()
        self.erroroutput.setGeometry(QtCore.QRect(10,140,561,31))
        self.erroroutput.setObjectName("erroroutput")

        # Hbox for errors
        erroroutput_HBoxLayout = QtGui.QHBoxLayout()
        erroroutput_HBoxLayout.addWidget(self.erroroutput)

        # Put all of the hboxes in a vbox
        reeditbox_VBoxLayout = QtGui.QVBoxLayout(self.reeditbox)
        reeditbox_VBoxLayout.setSpacing(1)
        reeditbox_VBoxLayout.addLayout(regexlabel_HBoxLayout)
        reeditbox_VBoxLayout.addLayout(userregex_HBoxLayout)
        reeditbox_VBoxLayout.addLayout(autoregex_HBoxLayout)
        reeditbox_VBoxLayout.addLayout(options_HBoxLayout)
        reeditbox_VBoxLayout.addLayout(line_HBoxLayout)
        reeditbox_VBoxLayout.addLayout(erroroutput_HBoxLayout)

        self.boxeditorgroup = QtGui.QGroupBox(self.centralwidget)
        self.boxeditorgroup.setMinimumSize(381,261)
        self.boxeditorgroup.setObjectName("boxeditorgroup")

        self.filecombobox = QtGui.QComboBox()
        self.filecombobox.setGeometry(QtCore.QRect(10,20,561,27))
        self.filecombobox.setObjectName("filecombobox")

        self.tickettext = QtGui.QTextEdit()
        self.tickettext.setEnabled(True)
        self.tickettext.setGeometry(QtCore.QRect(10,50,561,381))
        font = QtGui.QFont()
        font.setFamily("Monospace")
        self.tickettext.setFont(font)
        self.tickettext.setAcceptDrops(False)
        self.tickettext.setReadOnly(True)
        self.tickettext.setAcceptRichText(False)
        self.tickettext.setObjectName("tickettext")

        # vbox for combobox and textedit
        boxeditorgroup_VBoxLayout = QtGui.QVBoxLayout(self.boxeditorgroup)
        boxeditorgroup_VBoxLayout.setSpacing(1)
        boxeditorgroup_VBoxLayout.addWidget(self.filecombobox)
        boxeditorgroup_VBoxLayout.addWidget(self.tickettext)

        # Put all of the right side widgets in a vbox
        right_side_VBoxLayout = QtGui.QVBoxLayout()
        right_side_VBoxLayout.setSpacing(1)
        right_side_VBoxLayout.addWidget(self.reeditbox)
        right_side_VBoxLayout.addWidget(self.boxeditorgroup)

        # define the whole layout
        Layout = QtGui.QHBoxLayout()
        Layout.addWidget(self.dblistgroup)
        Layout.addLayout(right_side_VBoxLayout)

        self.centralwidget.setLayout(Layout)
        editorwindow.setCentralWidget(self.centralwidget)

        #self.menubar = QtGui.QMenuBar(editorwindow)
        #self.menubar.setObjectName("menubar")

        #self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile = editorwindow.menuBar().addMenu("File")
        self.menuFile.setObjectName("menuFile")

        self.menuHelp = editorwindow.menuBar().addMenu("Help")
        self.menuHelp.setObjectName("menuHelp")

        #editorwindow.setMenuBar(self.menubar)

        # Actions for menu
        self.actionOpen_Regex_File = QtGui.QAction(editorwindow)
        self.actionOpen_Regex_File.setObjectName("actionOpen_Regex_File")

        self.actionSave_Regex = QtGui.QAction(editorwindow)
        self.actionSave_Regex.setObjectName("actionSave_Regex")

        self.actionExit = QtGui.QAction(editorwindow)
        self.actionExit.setObjectName("actionExit")

        self.actionOpen_Ticket = QtGui.QAction(editorwindow)
        self.actionOpen_Ticket.setObjectName("actionOpen_Ticket")

        self.actionNew_Regex_File = QtGui.QAction(editorwindow)
        self.actionNew_Regex_File.setObjectName("actionNew_Regex_File")

        self.menuFile.addAction(self.actionOpen_Ticket)
        self.menuFile.addAction(self.actionNew_Regex_File)
        self.menuFile.addAction(self.actionOpen_Regex_File)
        self.menuFile.addAction(self.actionSave_Regex)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)

        #self.menubar.addAction(self.menuFile.menuAction())

        self.actionHelpAbout = QtGui.QAction(editorwindow)
        self.actionHelpAbout.setObjectName("actionHelpAbout")

        self.actionHelp = QtGui.QAction(editorwindow)
        self.actionHelp.setObjectName("actionHelp")

        self.menuHelp.addAction(self.actionHelpAbout)
        self.menuHelp.addAction(self.actionHelp)

        #self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(editorwindow)
        QtCore.QMetaObject.connectSlotsByName(editorwindow)

    def retranslateUi(self, editorwindow):
        '''
        All text goes here
        '''
        editorwindow.setWindowTitle(QtGui.QApplication.translate("editorwindow", "RegEx Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.dblistgroup.setTitle(QtGui.QApplication.translate("editorwindow", "Database Fields", None, QtGui.QApplication.UnicodeUTF8))
        self.reeditbox.setTitle(QtGui.QApplication.translate("editorwindow", "Edit", None, QtGui.QApplication.UnicodeUTF8))
        self.regexlabel.setText(QtGui.QApplication.translate("editorwindow", "re_", None, QtGui.QApplication.UnicodeUTF8))
        self.ignorecase.setText(QtGui.QApplication.translate("editorwindow", "IGNORECASE", None, QtGui.QApplication.UnicodeUTF8))
        self.dotall.setText(QtGui.QApplication.translate("editorwindow", "DOTALL", None, QtGui.QApplication.UnicodeUTF8))
        self.erroroutput.setText(QtGui.QApplication.translate("editorwindow", "No errors.", None, QtGui.QApplication.UnicodeUTF8))

        self.saveregex.setText(QtGui.QApplication.translate("editorwindow", "Save Regex", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Save the regex in this line."
        self.copyautoregex.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.copyautoregex.setText(QtGui.QApplication.translate("editorwindow", "Copy Auto-Regex", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Copy the automatically generated regex to the line above"
        self.copyautoregex.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.highlightautoregex.setText(QtGui.QApplication.translate("editorwindow", "Use Auto-Regex", None, QtGui.QApplication.UnicodeUTF8))
        tip = "If checked, highlight the text matching the automatically generated regex. Otherwise, highlight the text matching the regex from the top line"
        self.highlightautoregex.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.restoreauto.setText(QtGui.QApplication.translate("editorwindow", "Restore", None, QtGui.QApplication.UnicodeUTF8))
        self.boxeditorgroup.setTitle(QtGui.QApplication.translate("editorwindow", "Support Ticket", None, QtGui.QApplication.UnicodeUTF8))
        self.menuFile.setTitle(QtGui.QApplication.translate("editorwindow", "File", None, QtGui.QApplication.UnicodeUTF8))

        self.actionOpen_Regex_File.setText(QtGui.QApplication.translate("editorwindow", "Open Ticket Format", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Open an existing ticket format, typically found in the formats directory"
        self.actionOpen_Regex_File.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.actionSave_Regex.setText(QtGui.QApplication.translate("editorwindow", "Save Ticket Format", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Save the new ticket format, typically in the formats directory"
        self.actionSave_Regex.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.actionExit.setText(QtGui.QApplication.translate("editorwindow", "Exit", None, QtGui.QApplication.UnicodeUTF8))

        self.actionOpen_Ticket.setText(QtGui.QApplication.translate("editorwindow", "Open Ticket", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Open ticket(s). If more than one ticket, they should all be of the same format"
        self.actionOpen_Ticket.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.actionNew_Regex_File.setText(QtGui.QApplication.translate("editorwindow", "New Ticket Format", None, QtGui.QApplication.UnicodeUTF8))
        tip = "Create a new ticket format"
        self.actionNew_Regex_File.setToolTip(QtGui.QApplication.translate("editorwindow", tip, None, QtGui.QApplication.UnicodeUTF8))

        self.actionHelpAbout.setText(QtGui.QApplication.translate("editorwindow", "About regex editor", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHelp.setText(QtGui.QApplication.translate("editorwindow", "Help", None, QtGui.QApplication.UnicodeUTF8))
