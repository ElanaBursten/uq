# mapgrid.py
# Created: 12 May 2002, Hans Nowak

class MapGridError(Exception): pass

counties = {
    # county: max. latitude, min. latitude, lat_slices, max. longitude, min.
    # longitude, long_slices, page_turn, lat_columns, long_rows
    "ALEX":
     (36.048740, 35.776720, 9*7+4, -81.346160, -80.997000, 8*10+6, 9, 7, 10),
    "BURK":
     (36.003968, 35.557368, 15*7+5, -81.990784, -81.361484, 15*10+5, 16, 7, 10),
    "CABA":
     (35.508351, 35.183552, 11*7+3, -80.790041, -80.291276, 12*10+3, 13, 7, 10),
    "CALD":
     (36.125880, 35.764540, 12*7+5, -81.813060, -81.321800, 12*10+1, 13, 7, 10),
    "CATA":
     (35.831900, 35.543640, 10*7+1, -81.536100, -80.918980, 15*10+2, 16, 7, 10),
    "CLEV":
     (35.586052, 35.159786, 15*7, -81.766492, -81.318611, 11*10+1, 12, 7, 10),
    "DVIS":
     (36.028440, 35.500640, 18*7+4, -80.481380, -80.038840, 10*10+9, 11, 7, 10),
    "DVIE":
     (36.052800, 35.740180, 11*7, -80.708740, -80.367700, 8*10+4, 9, 7, 10),
    "GAST":
     (35.421782, 35.145672, 9*7+5, -81.455847, -80.914457, 13*10+3, 14, 7, 10),
    "IRED":
     (36.061334, 35.501032, 19*7+5, -81.114240, -80.690477, 10*10+4, 11, 7, 10),
    "LINC":
     (35.573720, 35.399140, 6*7+1, -81.541040, -80.936100, 14*10+9, 15, 7, 10),
    "MECK":
     (35.518724, 34.996900, 18*7+3, -81.060962, -80.545372, 12*10+7, 13, 7, 10),
    "ROWA":
     (35.862848, 35.497209, 12*7+6, -80.771800, -80.175014, 14*10+7, 15, 7, 10),
    "STAN":
     (35.503531, 35.136941, 12*7+6, -80.509467, -80.036088, 11*10+5, 12, 7, 10),
    "UNIO":
     (35.209256, 34.811464, 13*7+7, -80.843904, -80.268039, 14*10+1, 14, 7, 10),

    "Atlanta":
     (34.572202, 33.121568, 20*10, -85.112534, -83.434236, 24*8, 24, 10, 8),

    "BNRG":
     (30.721469, 30.295165, 15*7, -91.481750, -90.832200, 16*10, 16, 7, 10),
    "LAFA":
     (30.380482, 30.011027, 13*7, -92.279858, -91.873818, 10*10, 10, 7, 10),
}

def lookup(latitude, longitude):
    """ Return a list with map cells that cover the given latitude and
        longitude. """
    results = []
    for key in counties.keys():
        try:
            mapcell = coords2map(key, latitude, longitude)
        except MapGridError:
            pass
        else:
            results.append(mapcell)
    return results

def coords2map(county, latitude, longitude):
    (max_lat, min_lat, lat_slices, max_long, min_long, long_slices, page_turn,
     lat_columns, long_rows) = counties[county]
    if not (max_lat >= latitude >= min_lat
     and max_long <= longitude <= min_long):
        raise MapGridError, "Coordinates (%s,%s) out of bounds for %s" % (
                            latitude, longitude, county)
    lat_slice_size = (max_lat - min_lat) / lat_slices
    long_slice_size = abs(max_long - min_long) / long_slices

    lat_slice = _lat_slice(latitude, max_lat, min_lat, lat_slices)
    long_slice = _long_slice(longitude, max_long, min_long, long_slices)

    page, lat_c, long_c = _slices2page(lat_slice, long_slice, lat_columns,
                          long_rows, page_turn)
    long_spec = long_slice - (long_c * long_rows) + 1
    lat_spec = lat_slice - (lat_c * lat_columns) + 1

    return county, page, lat_spec, long_spec

###
### helper functions

def _lat_slice(latitude, max_lat, min_lat, lat_slices):
    z = ((latitude - min_lat) / (max_lat - min_lat)) * lat_slices
    return int(round(lat_slices - z, 8))

def _long_slice(longitude, max_long, min_long, long_slices):
    z = ((longitude - min_long) / (max_long - min_long)) * long_slices
    return int(round(long_slices - z, 8))

def _slices2page(lat_slice, long_slice, lat_columns, long_rows, page_turn):
    long_c = int(long_slice / long_rows)
    lat_c = int(lat_slice / lat_columns)
    page = lat_c * page_turn + long_c + 1
    return page, lat_c, long_c

###

def county_stats():
    items = counties.items()
    items.sort()
    for key, value in items:
        lat_slice_size = (value[0] - value[1]) / value[2]
        print key, "lat_slice_size:", lat_slice_size,
        long_slice_size = abs(value[3] - value[4]) / value[5]
        print "long_slice_size:", long_slice_size

###
### Jacksonville

def alphabetize(num):
    """ Convert a number to a notation A, B, .. Z, AA, AB, .. AZ, BA, etc...
        "Naive" approach.
    """
    def inc(alpha):
        if alpha == "Z":
            return "AA"
        elif len(alpha) == 1:
            return chr(ord(alpha) + 1)
        elif alpha.endswith("Z"):
            return inc(alpha[:-1]) + "A"
        else:
            return alpha[:-1] + inc(alpha[-1])
    alpha = "A"
    for i in range(num-1):
        alpha = inc(alpha)
    return alpha

maps_JAX = {
    # name: max_lat, min_lat, lat_slices, max_long, min_long, long_slices
    "Jacksonville": (30.5878, 30.0809, 45, -82.0584, -81.3318, 56),
}

def coords2map_JAX(latitude, longitude):
    county = "Jacksonville"
    max_lat, min_lat, lat_slices, max_long, min_long, long_slices = \
     maps_JAX[county]
    if not (max_lat >= latitude >= min_lat
     and max_long <= longitude <= min_long):
        raise MapGridError, "Coordinates (%s,%s) out of bounds for %s" % (
         latitude, longitude, county)
    lat_slice_size = (max_lat - min_lat) / lat_slices
    long_slice_size = abs(max_long - min_long) / long_slices

    lat_slice = _lat_slice(latitude, max_lat, min_lat, lat_slices)
    long_slice = _long_slice(longitude, max_long, min_long, long_slices)

    return lat_slice + 1, alphabetize(long_slice + 1)



if __name__ == "__main__":

    #county_stats()
    #
    #print lookup(35.5, -80.5)

    #print coords2map_JAX(34.91, -80.91)
    #print coords2map_JAX(34.89, -80.89)
    print coords2map_JAX(30 + 14.25/60, -(81 + 37.25/60))
    print coords2map_JAX(30.237500, -81.620833) # not out of bounds

