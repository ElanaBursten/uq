# runbilling.py
# Runs a batch file for billing, with a date as the first parameter.

import datetime
import os
import sys
import time

BILLING_LOG = "billing_log.txt"

def timestamp():
    """ Generate a timestamp. """
    t = time.localtime(time.time())
    return "[%04d-%02d-%02d %02d:%02d:%02d]" % tuple(t[:6])

def get_last_sunday():
    t = time.localtime(time.time())
    d = datetime.datetime(*t[:3])
    i = d.isoweekday()
    d = d - datetime.timedelta(i)
    return d

def call_batchfile(filename):
    d = get_last_sunday()
    d = d - datetime.timedelta(1) # get Saturday
    datestr = d.isoformat()[:10]
    cmd = 'call "%s" %s' % (filename, datestr)
    #print cmd

    f = open(BILLING_LOG, "w")
    print >> f, timestamp(), "Starting billing batchfile..."
    f.flush()

    os.system(cmd)

    print >> f, timestamp(), "Batchfile completed"
    f.close()


if __name__ == "__main__":

    call_batchfile(sys.argv[1])

