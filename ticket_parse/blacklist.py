# blacklist.py

import datadir

BUGFILE = "routing_error_skip.txt"

class BlackList:

    def __init__(self, log):
        self.log = log

    def read(self):
        try:
            f = datadir.datadir.open(BUGFILE, "r")
        except:
            self.log.log_event("Could not read: %r" % (BUGFILE,))
            return []

        lines = f.readlines()
        f.close()
        ids = [int(line) for line in lines if line.strip()]
        return ids

    def write(self, ticket_id):
        try:
            f = datadir.datadir.open(BUGFILE, "a+")
        except:
            self.log.log_event("Could not append to: %r" % (BUGFILE,))
            return
        print >> f, ticket_id
        f.close()
        self.log.log_event("Blacklisted ticket id: %s" % (ticket_id,))
