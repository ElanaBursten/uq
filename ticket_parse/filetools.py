# filetools.py

# TODO: move functions from tools.py here

from __future__ import with_statement
import os
import shutil
import sys
import time

def read_file(filename, mode='r'):
    f = open(filename, mode)
    data = f.read()
    f.close()
    return data

def read_file_lines(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    return lines

def get_filenames(directory):
    """ Return a list of fully qualified filenames in the given directory.
        Ignores subdirectories. """
    files = os.listdir(directory)
    files = [os.path.join(directory, fn) for fn in files]
    files = filter(os.path.isfile, files)
    return files

def remove_file(filename):
    """ Remove a file, but don't raise an exception if it doesn't exist.
        Mainly used for cleaning up when test suites are done. """
    try:
        os.remove(filename)
    except OSError:
        pass

def remove_dir(path):
    try:
        shutil.rmtree(path)
    except OSError:
        pass

def move_file(filename, dest):
    shutil.move(filename, dest)
    # XXX what happens if file already exists?

def move_with_benefits(filename, dest, random=0):
    """ Move file <filename> to directory <dest>.  If a file with that name
        already exists, a random suffix is attached. """
    path, file = os.path.split(filename)
    dest_full = os.path.join(dest, file)
    if random:
        dest_full = _gen_random_filename(dest_full)

    try:
        os.rename(filename, dest_full)
    except OSError, e:
        if e.errno == 17:
            move_with_benefits(filename, dest, random=1)
        else:
            raise

def copy_files(source_path, destination_path, filenames):
    for filename in filenames:
        shutil.copyfile(os.path.join(source_path, filename),
                        os.path.join(destination_path, filename)) 

def filesize(filename):
    """ Get the size of a file in bytes. If the file doesn't exist, assume a
        length of 0. """
    try:
        return os.stat(filename)[6]
    except:
        return 0

def _gen_random_filename(filename):
    """ Add a "random" suffix to a given filename.  Used when a filename
        already exists and cannot be overwritten.  The "random" suffix
        is actually based on the current time.
    """
    t9 = time.localtime(time.time())
    suffix = "_%02d%02d%02d" % (t9[3:6])
    # generate a few more numbers just to be sure
    a = time.time()
    z = int((a - int(a)) * 1000)
    suffix = suffix + "%03d" % (z,)
    return filename + suffix

class Stdout(object):
    """ Context manager that redirects stdout. """
    def __init__(self, out):
        self.out = out
    def __enter__(self):
        self.old_stdout = sys.stdout
        sys.stdout = self.out
        return self.out
    def __exit__(self, type, value, traceback):
        sys.stdout = self.old_stdout
        return type is None

MAX_AGG_FILE_SIZE = 2*1024*1024

class AggregateFile:
    def __init__(self, filename, max_size=MAX_AGG_FILE_SIZE):
        self.filename = filename
        self.max_size = max_size
    def write(self, data, sep=''):
        """ Append data to the aggregate file, but only if that file's size
            has not exceeded the maximum. If data was appended, return True,
            otherwise False.
        """
        if filesize(self.filename) < self.max_size:
            with open(self.filename, 'a+b') as f:
                f.write(data)
                if sep: f.write(sep)
            return True
        else:
            return False

