# ftpresponder.py
# Differs from IRTHresponder and XMLHTTPResponder in that there are two
# "passes": send responses (i.e. upload a file), and retrieve them (i.e.
# download a file).  This is different from the approach used by the other
# responders, which send one response, get a result, send another response, etc.
# This is why FTPResponder doesn't derive from AbstractResponder.

# XXX EmailResponder behaves much like FTPResponder, yet derives from
# AbstractResponder.  Someday, some refactoring can be done here to make
# all responders derive from AbstractResponder.

# TODO:
# - extra_data() and extra_names() seem to serve the same purpose; one of them
#   is redundant and should be removed.

from __future__ import with_statement
import ftplib
import getopt
import os
import re
import socket
import string
import sys
import time
import xml.etree.ElementTree as ET
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import baseresponder
import config
import datadir
import date
import dbinterface_ado
import emailtools
import errorhandling2
import errorhandling_special
import ftpresponder_data as frd
import listener
import mutex
import responder_data
import response_ack_context
import statuslist
import tabledefs
import ticket_db
import tools
import version
from magicdate import magicdate
from check_python_version import check_python_version

TIMEOUT = 40  # in seconds
PROCESS_NAME = 'FTPResponder'
MAX_RETRIES = 2
RETRY_WAIT_SECONDS = 5

socket.setdefaulttimeout(TIMEOUT)


class ResponderError(Exception):
    pass


class UnknownStatusError(ResponderError):
    pass


class FTPServerError(Exception):
    pass


class RetryableFTPConnectError(Exception):
    pass


class BadFTPConnectionError(Exception):
    pass

class CCFTPClient(object):
    """ Manages a connection to an FTP server. Can be overridden or replaced for
        testing purposes. """

    def __init__(self, config, serverdict, log, verbose=1):
        self.config = config    # general Configuration instance
        self.serverdict = serverdict
        self.verbose = verbose
        self.ftp = None
        self.log = log

    def do_connect(self):
        for attempt in range(1, 2 + MAX_RETRIES):
            if attempt > 1:
                time.sleep(RETRY_WAIT_SECONDS)
            try:
                self.connect()
                return
            except RetryableFTPConnectError as e:
                if attempt <= MAX_RETRIES:
                    continue
                else:
                    raise

    def connect(self):
        try:
            self.ftp = ftplib.FTP(self.serverdict["server"])
            self.ftp.sock.settimeout(TIMEOUT)
            self.ftp.login(self.serverdict["login"], self.serverdict["password"])
            if self.verbose:
                print self.ftp.getwelcome()
        except Exception as e:
            self.ftp = None
            raise RetryableFTPConnectError("Problem connecting to server: " + e.message)

    def disconnect(self):
        try:
            self.ftp.quit()
        finally:
            self.ftp = None

    def operation_with_retry(self, operation, filename=None, data=None):
        for attempt in range(1, 2 + MAX_RETRIES):
            if attempt > 1:
                time.sleep(RETRY_WAIT_SECONDS)

            if self.ftp is None:
                self.do_connect()

            try:
                if filename is None:
                    return operation()
                elif data is None:
                    return operation(filename)
                else:
                    return operation(filename, data)
            except BadFTPConnectionError:
                self.disconnect()

                if attempt <= MAX_RETRIES:
                    continue
                else:
                    ep = errorhandling2.errorpacket()
                    self.log.log(ep, send=1, write=1, dump=self.verbose)
                    raise

    def put(self, filename, data):
        self.operation_with_retry(self.put_file, filename, data)

    def get(self, filename):
        return self.operation_with_retry(self.get_file, filename)

    def delete(self, filename):
        self.operation_with_retry(self.delete_file, filename)

    def _getlist(self):
        return self.operation_with_retry(self._get_file_list)

    def put_file(self, filename, data):
        """ Write a file to the server. """
        f = StringIO(data)
        try:
            self.ftp.storbinary("STOR " + filename, f)
        except ftplib.all_errors + (AttributeError, ) as e:
            raise BadFTPConnectionError("Unable to perform put: " + e.message)
        finally:
            f.close()

    def get_file(self, filename):
        """ Get data from a file from the server, as a long string. """
        io = StringIO()
        try:
            self.ftp.retrbinary("RETR " + filename, io.write)
        except ftplib.all_errors + (AttributeError, ) as e:
            raise BadFTPConnectionError("Unable to perform get: " + e.message)

        data = io.getvalue()
        return data

    def delete_file(self, filename):
        """ Delete a file from the server. """
        try:
            self.ftp.delete(filename)
        except ftplib.all_errors + (AttributeError, ) as e:
            raise BadFTPConnectionError("Unable to delete file: " + e.message)

    @staticmethod
    def is_traditional_ftp_line(line):
        re_line = re.compile("^[drwx-]{9}")
        m = re_line.match(line.lstrip())
        return bool(m)

    def getfilelist(self):
        """ Return a list of files that we can get. """
        #if self.verbose:
        #    print "Moving to directory", self.serverdict["outgoing_dir"]
        #self.ftp.cwd(self.serverdict["outgoing_dir"])
        # we're not moving anywhere
        lines = self._getlist()

        # We're interested in the lines that look like
        # -rw-r-----    1 byermd00 ftpuser       11 Dec 11 16:30 byermd00_20031211_163359.rsp
        filenames = []
        for line in lines:
            parts = line.split()
            if parts:
                filename = parts[-1]
                filenames.append(filename)

        return filenames

    def get_file_size(self, filename):
        """ Get the (current) size, in bytes, of the given file. """
        lines = self._getlist()

        # We're interested in the lines that look like
        # -rw-r-----    1 byermd00 ftpuser       11 Dec 11 16:30 byermd00_20031211_163359.rsp

        for line in lines:
            parts = line.split()
            if parts:
                ftp_filename = parts[-1]
                if ftp_filename == filename:
                    try:
                        if self.is_traditional_ftp_line(line):
                            size = parts[4]
                        else:
                            # lines of the type:
                            # 04-10-07  01:58PM                 3190 byermd00_070410_135529.out
                            size = parts[-2]
                    except IndexError:
                        self.log.log_event("Could not read file info (IndexError)")
                        self.log.log_event("- Filename: " + filename)
                        self.log.log_event("- Line: " + line)
                        self.log.log_event("- Parts: " + `parts`)
                        return 0
                    try:
                        return int(size)
                    except ValueError:
                        return 0

        raise FTPServerError("File not found: %s" % filename)

    def _get_file_list(self):
        self.list = []
        try:
            self.ftp.retrlines("LIST", self._process_list_line)
        except ftplib.all_errors + (AttributeError, ) as e:
            raise BadFTPConnectionError("Unable to get file list: " + e.message)

        return self.list

    def _process_list_line(self, line):
        """ Helper method for _getlist. """
        self.list.append(line)

class ResponderAckStorage:
    """ Abstract class for response ack storage. """
    def __init__(self, parent):
        self.parent = parent # FIXME?
    def store(self, locates, call_center, filename):
        raise NotImplementedError
    def load(self, call_center, filename):
        raise NotImplementedError
    def delete(self, call_center, filename):
        raise NotImplementedError

class ResponderDBStorage(ResponderAckStorage):

    def __init__(self, log, db):
        self.log = log
        self.db = db

    # note: filenames from/to server are usually of the form 'foo.xml' and
    # 'foo.rxml' for the acks. we store the filename without the extension.

    def store(self, locates, call_center, filename):
        shortname = os.path.splitext(filename)[0]
        for loc in locates:
            ctx = response_ack_context.ResponseAckContext()
            ctx.set(filename=shortname,
                    call_center=call_center,
                    response_id=loc['resp_id'],
                    locate_id=loc['locate_id'],
                    ticket_id=loc['ticket_id'],
                    ticket_number=loc['ticket_number'],
                    client_code=loc['client_code'],
                    resp_type='ftp')
            ctx.insert(self.db)

    def load(self, call_center, filename):
        shortname = os.path.splitext(filename)[0]
        rows = self.db.getrecords('response_ack_context',
               call_center=call_center, filename=shortname)
        return response_ack_context.ResponseAckContext.load_from_rows(rows)

    def delete(self, call_center, filename):
        shortname = os.path.splitext(filename)[0]
        self.db.deleterecords('response_ack_context',
         call_center=call_center, filename=shortname)


class FTPResponder(baseresponder.BaseResponder):
    """ Responder using an FTP connection. """
    name = 'FTPResponder'
    responder_type = 'ftp' # as used by ResponderConfigData

    def __init__(self, configfile="", only_one=0, dont_retrieve=0, dont_send=0,
                 call_center=None, verbose=1, log=None):

        super(FTPResponder, self).__init__()

        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        self.only_one = only_one    # send just one response
        self.dont_retrieve = dont_retrieve
        self.dont_send = dont_send
        self.verbose = verbose
        self.call_center = call_center # if set, only run for this center
        self.max_number = 500

        self.list = []

        self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado) # for looking up email addresses
        self.statuslist = statuslist.StatusList(self.tdb)
        self.emails = emailtools.Emails(self.tdb)
        # necessary for responder_email addresses

        # create and configure the logger
        if log:
            self.log = log
        else:
            self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                       smtpinfo=self.config.smtp_accs[0], me="FTPresponder",
                       admin_emails=self.config.admins,
                       subject="FTPresponder Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = verbose

        self._debug_info = {}

        self.tabledefs = tabledefs.TableDefs(self.tdb)

        # create output directory if it doesn't exist already
        #if not os.path.exists(self.config.ftp_output_dir):
        #    os.makedirs(self.config.ftp_output_dir)

    def create_server(self, serverdict):
        # separate method, so it can be overloaded for testing purposes
        self.server = CCFTPClient(self.config, serverdict, self.log)

    def create_dbstorage(self):
        self.dbstorage = ResponderDBStorage(self.log, self.tdb)

    def run_manual(self, callcenter, ticket_number, status, company):
        # XXX this instance of ftpresponders can be replaced as well, has
        # lower priority though
        # todo(dan) This is just for testing, and currently doesn't work
        resps = self.config.responderconfigdata(callcenter, 'ftp')
        for _, _, serverdict in resps:
            manual_data = (ticket_number, status, company)
            self.run2(serverdict, manual_data=manual_data)

    def run(self):
        if self.call_center:
            responders = self.config.responderconfigdata.get_responders(
             call_center=self.call_center, type=self.responder_type)[:1]
        else:
            # get all responders of this type
            # NOTE: this allows multiple FTP responders for the same call center
            responders = self.config.responderconfigdata.get_responders(
                         type=self.responder_type)
        # XXX reconcile with registry!
        # XXX if there multiple 'eligible' responders, check the
        # raw_responderdata for the name   #/2882
        # e.g. 'name' => 'one'   would get class 1
        #      'name' => 'two'   would get class 2 etc
        for _, _, raw_responderdata in responders:
            if raw_responderdata['name']:
                mutex_name = PROCESS_NAME + ':' + datadir.datadir.get_id() + \
                 ':' + raw_responderdata['name']
                try:
                    with mutex.MutexLock(self.dbado, mutex_name):
                        # todo(dan) Test failed mutex
                        self.run2(raw_responderdata)
                except mutex.SingleInstanceError:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Locked mutex: %s" % mutex_name)
                    self.log.log(ep, send=1, dump=1 and self.log.verbose, write=1)

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

    def run2(self, raw_responderdata, manual_data=None):
        """ Main function. """
        try:
            self.create_server(raw_responderdata)  # create self.server
            call_center = raw_responderdata['name']
            self.responderdata = frd.getresponderdata(self.config, call_center,
                                 raw_responderdata)
            self.responderdata.tdb = self.tdb
            self.responderdata.dbado = self.dbado

            self.create_dbstorage()
            # creation of dbstorage is delayed until here, because we need the
            # serverdict in some cases

            # set emails: admin + respond_emails for this call center
            allemails = [a['email'] for a in self.config.admins]
            allemails.extend(self.emails[call_center].responder)
            self.log.errorbag.emails = allemails

            if not self.dont_send:
                if manual_data:
                    ticket_number, status, company = manual_data
                    # test data
                    locates = [
                        {'locate_id': 0, 'status': status,
                         'ticket_format': call_center, 'ticket_id': 0,
                         'ticket_number': ticket_number,
                         'client_code': company,
                         'ticket_type': 'NORMAL', 'work_state': 'ZZ'}]
                else:
                    locates = self.get_pending_responses(call_center)

                if self.only_one:
                    locates = locates[:1]

                if locates:
                    self.send_responses(locates)
                else:
                    self.log.log_event("Nothing to send")

            if not self.dont_retrieve:
                self.receive_responses()

        except:
            ep = errorhandling2.errorpacket()
            ep.add(responderdata=raw_responderdata)
            self.log.log(ep, send=1, write=1, dump=self.verbose)
        self.log.force_send()   # send any queued error messages

    def connect(self):
        """ Connect to FTP site as specified in config.xml. """
        self.log.log_event("Connecting to: " + self.server.serverdict["server"])
        self.server.do_connect()
        self.log.log_event("Login: " + self.server.serverdict["login"])

    def disconnect(self):
        """ Disconnect from FTP site. """
        self.log.log_event("Disconnecting...")
        self.server.disconnect()
        self.log.log_event("OK (disconnected)")

    #
    # sending responses

    def send_responses(self, locates):
        """ Write response files to the FTP directory. """
        if not locates:
            self.log.log_event("Nothing to send.")
            return

        callcenter = self.server.serverdict['name']
        translations = self.responderdata.translations
        mappings = self.responderdata.mappings
        exclude_states = self.responderdata.exclude_states
        sections = []
        # Keep a list of the responses skipped so that we can
        # send one email with all of the skipped responses
        # instead of one per locate
        skipped_status_codes = []

        for row in locates:
            locate_id = row['locate_id']
            client_code = row['client_code']
            status = row.get('status', '')
            # do we skip this client code?
            if client_code in self.responderdata.skip:
                self.log.log_event("Skipping: %s" % client_code)
                self.log_directly(row, 'skipped', '', '00')
                self.delete_from_queue(locate_id)
                continue

            elif status in self.responderdata.status_code_skip:
                # skipping this status code means:
                # 1. do not respond
                # 2. delete from queue
                # 3. log and add to skipped_status_codes
                self.check_skipped_status_codes(status)
                self.log.log_event("Skipping: %s" % status)
                skipped_status_codes.append(row)
                self.delete_from_queue(locate_id)
                continue

            elif row.get('work_state') in exclude_states:
                self.log.log_event("Skipping: %s (state: %s)" % (
                 row['locate_id'], row['work_state']),)
                self.log_directly(row, 'skipped', '', '00')
                self.delete_from_queue(locate_id)
                continue

            elif client_code not in self.responderdata.clients \
             and self.responderdata.clients != []:
                # if the client code is neither in skip nor in clients, then
                # ignore it; this responder won't handle it
                self.log.log_event("Ignoring: %s" % client_code)
                continue

            # get ticket number and adapt it if necessary
            ticket_number = self.responderdata.adapt_ticket_number(
                            row['ticket_number'])
            if ticket_number is None:
                self.log.log_event("Could not adapt ticket number: %s" %
                 row['ticket_number'])
                continue

            # translate status (stored in t_status)
            t_status, t_explanation = self.translate_status(row, mappings)
            if t_status is None:
                continue
                # cannot send correct status; continue (error has been
                # logged already by translate_status)

            # translate company (stored in t_company)
            t_company = self.translate_company(row, translations)

            # create "section" and keep it around
            resp_id = self.write_initial_response_log(row, t_status)
            section = self.make_response_section(ticket_number, t_status,
                      t_company, locate_id, resp_id, t_explanation, row)
            sections.append(section)

            row['resp_id'] = resp_id
            # add field so temp info (context) will be stored

        # send an email that details the responses skipped due to status codes
        if skipped_status_codes:
            msg = []
            for skipped in skipped_status_codes:
                msg.append(string.join([
                  'ticket number: ', skipped['ticket_number'],
                  'client_code: '  , skipped['client_code'],
                  'work_date: '    , skipped['work_date'],
                  'status: '       , skipped['status']], ' '))
            ep = errorhandling2.errorpacket()
            ep.add(info=string.join(msg,'\n'))
            self.log.log_warning(ep, call_center=callcenter, send=1, write=1,
             dump=1, subject_hint='Skipped status codes')

        # it's possible that we skipped all records; in such case, we should
        # not send an "empty" file
        if not sections:
            self.log.log_event("All records skipped -- nothing to send")
            return

        # create block of text we're going to send, based on sections
        blurb = string.join(sections, '')
        data = self.responderdata.TEMPLATE1.replace("###BODY###", blurb)
        #if '\r' not in data:
        #    data = data.replace('\n', '\r\n')

        if hasattr(self.responderdata, 'is_xml') and self.responderdata.is_xml:
            # validate xml
            try:
                ET.fromstring(data)
            except:
                raise ResponderError("Invalid XML")

        # write file to FTP server
        outfilename = self.write_response_file(data)

        # only keep locates that have a resp_id
        if self.responderdata.store_resp_context:
            locates = [d for d in locates if d.has_key('resp_id')]
            self.dbstorage.store(locates, callcenter, outfilename)

    def check_skipped_status_codes(self, status):
        # Check that skipped status codes are valid
        if not self.statuslist.data.has_key(status):
            raise ValueError, "Status code to skip not in status list"

    def translate_status(self, row, mappings):
        """ Return the translation of a status (as specified in config.xml for
            the call center.  If not found, log an error and return None
            instead. """
        status = row['status']
        try:
            if not mappings.has_key(status):
                raise UnknownStatusError, 'Unknown status: %s' % (status,)
            return mappings[status]
        except:
            ep = errorhandling2.errorpacket(row=row)
            self.log.log(ep, send=1, write=1, dump=1 and self.verbose)
            # delete the record, it's erroneous
            self.delete_from_queue(row['locate_id'])
            return None, "No Mapping"

    def translate_company(self, row, translations):
        company = row['client_code']
        t_company = translations.get(company, company)
        if t_company != company:
            self.log.log_event('Translation: ' + company + ' -> ' + t_company)
        return t_company

    def make_response_section(self, ticket_number, status, company, locate_id,
                              resp_id, explanation, raw):
        """ Return a string representing a "section" of the total text that
            will be sent, containing the data for one response.  This could
            be a single line, a block of XML, etc. """
        namespace = locals().copy()
        del namespace['self']
        namespace['raw'] = raw # original row as returned by stored proc
        extra_data = self.responderdata.extra_data(namespace)
        namespace.update(extra_data)
        extra_names = self.responderdata.extra_names(namespace)
        namespace.update(extra_names)
        section = self.responderdata.make_line(namespace)
        self.log.log_event("Writing section: " + section.rstrip())
        return section

    def write_response_file(self, data):
        """ Write response file to server (with logging). """
        outfilename = self.responderdata.gen_filename()
        self.log.log_event("Storing file: " + outfilename + "...")
        try:
            self.server.put(outfilename, data)
            self.log.log_event("OK (file saved)")
        except BadFTPConnectionError as e:
            self.log.log_event("Unable to save the file: " + e.message)

        return outfilename

    #
    # retrieving responses

    def receive_responses(self):
        """ Read response files from the FTP directory. """
        # get acknowledgement files from the server
        self.connect()
        all_filenames = self.server.getfilelist()
        filenames = [name for name in all_filenames
                     if self.responderdata.is_response_file(name)]

        if not filenames:
            self.log.log_event("Nothing to receive")
            return

        for filename in filenames:

            # check if file can be processed
            if not self.file_can_be_read(filename, 5):
                self.log.log_event("File %s is still being processed; ignore"\
                  " for now" % (filename,))
                continue

            try:
                self.log.log_event("Reading %s..." % (filename,))
                data = self.server.get(filename)
                self.log.log_event("%d bytes read" % (len(data),))

                # write file to disk
                outputfilename = os.path.join(self.config.ftp_output_dir, filename)
                with datadir.datadir.open(outputfilename, 'w') as f:
                    f.write(data)

                self.process_ack_file(data, filename) # will store to database
                # ^ an exception may occur here...

                # only delete if everything went well:
                if self.responderdata.delete_ack:
                    self.log.log_event("Deleting " + filename + " from server...")
                    self.server.delete(filename)
                    self.log.log_event("OK (file deleted)")

                # if that went well, delete the pickle file as well
                if self.responderdata.store_resp_context:
                    call_center = self.server.serverdict['name']
                    self.dbstorage.delete(call_center, filename)
            except:
                ep = errorhandling2.errorpacket(filename=filename)
                self.log.log(ep, send=1, write=1, dump=1 and self.verbose)
        self.disconnect()

    def file_can_be_read(self, filename, num_tries):
        """ Check if the given file can be processed.  This is done by checking
            its size, waiting a few seconds, and checking the size again; if the
            values differ, then apparently the file is still being written.
            If the sizes are the same, return True.
        """
        self.log.log_event("Trying to determine if file can be read: '%s'"
         % filename)
        # get initial file size
        filesize1 = self.server.get_file_size(filename)
        for i in range(num_tries):
            # get new file size
            filesize2 = self.server.get_file_size(filename)
            # if sizes are the same, then file appears fit for processing
            if filesize1 == filesize2 and filesize2 != 0:
                self.log.log_event('...yes')
                return True
            # otherwise, continue
            filesize1 = filesize2
            # wait 5 seconds
            time.sleep(5)

        # if we are here, then we tried <num_tries> without returning True;
        # return False for now, leaving the file alone until next session
        self.log.log_event('...no')
        return False

    def _process_list_line(self, line):
        self.list.append(line)

    def process_ack_file(self, data, filename):
        """ Process the results returned by the server. <filename> is the file
            as found on the server.  <data> are its contents (as a string).
        """
        if self.responderdata.store_resp_context:
            try:
                call_center = self.server.serverdict['name']
                locates = self.dbstorage.load(call_center, filename)
            except IOError:
                # if we cannot read the pickle file, we cannot process the .out
                # file, so delete from server
                self.server.delete(filename)
                raise
        else:
            locates = [] # we won't use this value anyway

        ok, errors = self.responderdata.get_acknowledgements(data, locates)

        # update response log for acknowledged responses
        for ack in ok:
            try:
                # Mantis 2486: check that the status we sent is the current
                # status on the locate
                same_status = self.check_ack_status(ack.resp_id, ack.locate_id)
                if same_status:
                    self.log.log_event("Acknowledged: " + ack.raw)
                    self.update_response_log(ack.resp_id, ack.locate_id,
                      ack.date, ack.time, ack.result, ack.raw)
                    self.delete_from_queue(ack.locate_id)
                else:
                    # if the statuses are not the same, we don't remove the
                    # response from the queue
                    self.log.log_event("Storing results of acknowledgement...")
                    self.update_response_log(ack.resp_id, ack.locate_id,
                      ack.date, ack.time, ack.result, ack.raw)
            except:
                ep = errorhandling2.errorpacket()
                self.log.log(ep, send=1, write=1, dump=1 and self.verbose)
                continue

        # log errors
        for err in errors:
            if isinstance(err, tuple):
                self.log.log(err[1], send=1, write=1, dump=1 and self.verbose)
                if err[0] == 'resend response':
                    self.log.log_event(
                     "Updating response_log with '(ftp retry)'...")
                    self.tdb.update_response_log(err[2].resp_id, '(ftp retry)',
                     0)
                elif err[0] == 'acknowledged, not successful':
                    self.log.log_event("Updating response_log for response " +
                     "that was acknowledged, but not successful...")
                    self.update_response_log(err[2].resp_id, err[2].locate_id,
                     err[2].date, err[2].time, err[2].result, err[2].raw)
                    if self.check_ack_status(err[2].resp_id, err[2].locate_id):
                        self.delete_from_queue(err[2].locate_id)
            else:
                self.log.log(err, send=1, write=1, dump=1 and self.verbose)

    def check_ack_status(self, resp_id, locate_id):
        """ Return True if the status code we sent is the same as the current
            status on the locate. """

        # 1. check status on resp_id
        rows = self.dbado.getrecords('response_log', response_id=resp_id)
        if not rows:
            return False
        resp_status = rows[0]['status']

        # 2. check current status on locate_id
        rows = self.dbado.getrecords('locate', locate_id=locate_id)
        loc_status = rows[0]['status']

        if resp_status != loc_status:
            self.log.log_event(tools.fmt("Statuses are not the same: ",
              resp_status, " in response_log, ", loc_status, " on locate"))
            #self.log.log_event("Statuses are not the same: %s in "\
            #+ "response_log, %s on locate" % (resp_status, loc_status))
        return resp_status == loc_status

    #
    # storing and retrieving locates

    #
    # database access

    def get_pending_responses(self, call_center):
        self.log.log_event("Getting pending responses for: %s..." % call_center)

        try:
            sql = responder_data.special_queries_ftp[call_center]
        except KeyError:
            sql = "exec get_pending_responses '%s', @RetryPeriod = 120" % call_center

        if self.responderdata.parsed_locates_only in (False, 0):
            sql += ", @ParsedLocatesOnly = 0"
            # note: if the value is None, we assume the default (1)

        self._debug_info['sql'] = sql

        rows = self.dbado.runsql_result(sql)
        if self.max_number:
            rows = rows[:self.max_number]
        self.filter_responses(rows)
        self.log.log_event("OK (%d records found)" % len(rows))
        self.log.log_event("%d records found for %s" % (
         len(rows), call_center), dump=0)
        return rows

    #
    # logging

    def write_initial_response_log(self, row, translated_status):
        # get max id of response log
        max_id = int(self.dbado.get_max_id('response_log', 'response_id'))
        # add initial response record
        self.dbado.insertrecord('response_log',
         status=row['status'],
         response_date=date.Date().isodate(),
         locate_id=row['locate_id'],
         call_center=row['ticket_format'],
         success=0, # will be 1 when result is received
         response_sent=translated_status[:15],
         reply="(ftp waiting)",
         )

        # what's the id of the record that was just inserted?
        # (this assumes that there will be no other FTP responders inserting)
        # todo(dan) Is it possible to run multiple FTP responders now?
        sql = """
         select response_id from response_log where response_id > %s
         and reply = '(ftp waiting)'
         """ % (max_id,)
        rows = self.dbado.runsql_result(sql)
        assert rows, "No record was inserted into response_log"

        # return id
        return rows[0]['response_id']

    def update_response_log(self, resp_id, row, rdate, rtime, result, line):
        success = self.responderdata.is_success(result)
        if rdate is None:
            rdate = date.Date().isodate()[:10]
        else:
            # Try to convert it to iso
            try:
                rdate = magicdate(rdate).isoformat()
            except:
                # No joy, leave it as is
                pass
        if rtime is None:
            rtime = date.Date().isodate()[11:]
        response_log_str = string.join([rdate, rtime, result], ' ')
        width = self.tabledefs.lookup('response_log', 'reply').width
        self.tdb.update_response_log(resp_id, response_log_str[:width], success)

    def log_directly(self, row, rdate, rtime, result):
        resp_id = self.write_initial_response_log(row, '-')
        self.update_response_log(resp_id, row, rdate, rtime, result, '-')


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "1rsC:", ["data="])

    only_one = 0
    dont_retrieve = 0
    dont_send = 0
    configfile = ""
    manual = None
    call_center = None

    if args:
        assert len(args) == 4
        manual = args

    for o, a in opts:
        if o == "-1":
            only_one = 1
        elif o == "-r":
            dont_retrieve = 1
        elif o == "-s":
            dont_send = 1
        elif o == "-C":
            call_center = a
        elif o == "--data":
            print "Data dir:", a

    try:
        log = errorhandling_special.toplevel_logger(configfile, "FTPResponder",
              "FTPResponder Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "FTPResponder")
        sys.exit(1)

    check_python_version(log)

    try:
        r2r = FTPResponder(configfile=configfile, only_one=only_one,
              dont_retrieve=dont_retrieve, dont_send=dont_send,
              call_center=call_center)
        r2r.log.log_event("ftpresponder.py (version %s) started (pid %s)"
          % (version.__version__, os.getpid()))
        if manual:
            r2r.run_manual(*manual)
        else:
            r2r.run()
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

