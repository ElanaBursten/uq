from backwards_reader import BackwardsReader
def Search(f,pattern):
    """
    Get the line containg the pattern and the next line after the pattern, searching from the end of the file
    """
    next_line = None
    for line in BackwardsReader(f):
        if line.find(pattern) != -1:
            if next_line is None:
                # last line in file is the one we are searching for
                return line, None
            else:
                return line, next_line
        next_line = line
    # Not found
    return None, None

def SearchLogFile(fname,pattern):
    f = open(fname,"r")
    line, next_line = Search(f,pattern)
    f.close()
    return (line, next_line)
