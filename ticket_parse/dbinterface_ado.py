# dbinterface_ado.py

import copy
import pythoncom
import string
import time
import win32com.client
#
import adoconstants
import config
import dbinterface_old as dbinterface
import errorhandling2
import tools

ADO_READ_COMMITTED = 0x1000
ADO_TIMEOUT_ERROR = -2147217871
SQL_SERVER_PROVIDER_NAMES = ['Microsoft OLE DB Provider for SQL Server',
 'Microsoft SQL Server Native Client 11.0']

def catch_errors(f):
    """ Decorator to catch errors, wrap them in DBExceptions, TimeoutErrors, or
        RunSqlExceptions. And add attributes for more debugging information.
        IMPORTANT: The function param of the method we wrap MUST be an SQL
        string. """
    # Note: at the point of decoration, <f> is not an instance method...
    # it's a function defined in the class definition!
    def sqlfunc(self, sql, *args, **kwargs):
        try:
            return f(self, sql, *args, **kwargs)
        except dbinterface.DBException:
            raise
        except Exception as e:
            self._reraise_com_db_error(e, sql=sql)
            raise dbinterface.RunSQLException(repr(e.args),
                  original_exception_class=e.__class__, original_exception=e,
                  sql=sql)
    sqlfunc.__name__ = f.__name__
    return sqlfunc


class DBInterfaceADO(dbinterface.DBInterface):

    def __init__(self, dbdata={}):
        if not dbdata:
            self.config = config.getConfiguration()
            dbdata = self.config.ado_database

        self.config = None
        if dbdata.get('conn_string', ''):
            self.conn = self._connect(dbdata['conn_string'])
        else:
            self.conn = self._make_connection(
              dbdata['host'], dbdata['database'],
              dbdata['login'], dbdata['password'])

        self.timeout = dbdata.get('timeout_seconds',
                       config.DEFAULT_DB_TIMEOUT_SECONDS)
        self._dbdata = copy.deepcopy(dbdata)

    # some methods are obsolete and should not be called
    def _dontcall(self, *args, **kwargs):
        raise AttributeError, "Don't call this method"
    gethttp = _dontcall
    http_get = _dontcall
    http_post = _dontcall
    send_updategram = _dontcall

    def _checksql(self, sql):
        return sql  # no need to append XML stuff here

    def _reraise_com_db_error(self, e, sql=None, timeout_seconds=None,
                              elapsed_seconds=None):
        # Check if it's a SQL Server exception
        if isinstance(e, pythoncom.com_error) and len(e.args) >= 3 and \
         len(e.args[2]) >= 2 and e.args[2][1] in SQL_SERVER_PROVIDER_NAMES:
            if len(e.args[2]) >= 3:
                msg = e.args[2][2]
            else:
                msg = "Unknown SQL Server error"

            kwargs = {'original_exception_class': e.__class__,
                      'original_exception': e}
            if sql is not None:
                kwargs['sql'] = sql

            if len(e.args[2]) >= 6 and e.args[2][5] == ADO_TIMEOUT_ERROR:
                # It's a timeout
                if timeout_seconds is not None:
                    kwargs['timeout_seconds'] = timeout_seconds
                if elapsed_seconds is not None:
                    kwargs['elapsed_seconds'] = elapsed_seconds
                raise dbinterface.TimeoutError(msg, **kwargs)
            else:
                raise dbinterface.DBException(msg, **kwargs)

    def _make_connection_string(self, host, database, login, password):
        s = [ "Provider=sqloledb;",
              "Data Source=%s;" % (host),
              "Initial Catalog=%s;" % (database),
              "User Id=%s;" % (login),
              "Password=%s;" % (password)]
        connstr = string.join(s)
        return connstr

    def _make_connection(self, host, database, login, password):
        """ Make a connection string, then use it to connect. """
        s = self._make_connection_string(host, database, login, password)
        return self._connect(s)

    def _connect(self, conn_string):
        """ Connect using a connection string. """
        try:
            conn = win32com.client.Dispatch("ADODB.Connection")
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e)
            raise

        try:
            conn.IsolationLevel = ADO_READ_COMMITTED
            conn.Open(conn_string)
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e, timeout_seconds=conn.ConnectionTimeout)
            raise

        return conn

    #
    # "public" versions of runsql* methods

    @catch_errors
    def runsql(self, sql, timeout=None):
        return self._runsql(sql, timeout=timeout)

    @catch_errors
    def runsql_result(self, sql, timeout=None, as_strings=True):
        return self._runsql_result(sql, timeout=timeout,
         as_strings=as_strings)

    @catch_errors
    def runsql_result_multiple(self, sql, timeout=None):
        return self._runsql_result_multiple(sql, timeout=timeout)

    @catch_errors
    def runsql_with_parameters(self, sql, params, timeout=None):
        return self._runsql_with_parameters(sql, params, timeout=timeout)

    def _run_command(self, sql, params=None, timeout=None):
        cmd = win32com.client.Dispatch("ADODB.Command")
        cmd.ActiveConnection = self.conn
        cmd.CommandText = sql
        cmd.CommandTimeOut = timeout or self.timeout

        # add parameters
        if params:
            if params and isinstance(params[0], tuple):
                for (value, sqltype, name) in params:
                    # errors may occur here that produce unhelpful messages,
                    # so I'm catching them and reraising the exception with
                    # a more informative string:
                    try:
                        p = sqltype.make_ado_parameter(value, cmd)
                        p.Value = value
                        cmd.Parameters.Append(p)
                    except AttributeError, e:
                        raise Exception, "Cannot set value for field %r (%r)" % (name, value)
            else:
                # assume a list of values (probably strings)
                for param in params:
                    p = cmd.CreateParameter(param, adoconstants.adVariant,
                        adoconstants.adParamInput)
                    p.Value = param
                    cmd.Parameters.Append(p)
            cmd.Prepared = True

        start_time = time.time()
        try:
            rs, code = cmd.Execute()
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e, sql=cmd.CommandText,
             timeout_seconds=cmd.CommandTimeOut,
             elapsed_seconds=time.time() - start_time)
            raise

        return rs

    #
    # "private" versions of runsql* methods

    def _runsql(self, sql, timeout=None):
        return self._run_command(sql, timeout=timeout)

    def _runsql_result(self, sql, timeout=None, as_strings=True):
        """ More robust version of runsql, with timeout set.
            Returns result set (converted to a list of dicts).
        """
        rs = self._run_command(sql, timeout=timeout)

        # fixed this to work with ADO 2.8 and/or MSSQL2K SP3
        for i in range(100):
            if not self.is_result_set(rs):
                rs, code = rs.NextRecordset()
            else:
                break   # continue as usual

        if rs.Eof:
            return []

        fieldnames = self._get_fieldnames(rs)
        rows = rs.GetRows()
        d = self._make_dict(rows, fieldnames)
        if as_strings:
            self.ado_convert(d)
        return d

    def _runsql_result_multiple(self, sql, timeout=None):
        resultsets = []
        rs = self._run_command(sql, timeout=timeout)

        for i in range(100): # limited, so it won't hang forever
            if self.is_result_set(rs):
                if rs.Eof:
                    resultsets.append([])
                else:
                    fieldnames = self._get_fieldnames(rs)
                    rows = rs.GetRows()
                    d = self._make_dict(rows, fieldnames)
                    self.ado_convert(d)
                    resultsets.append(d)

            rs, code = rs.NextRecordset()
            if not rs:
                break

        return resultsets

    def _runsql_with_parameters(self, sql, params, timeout=None):
        """ Run a parametrized SQL command, e.g.
            "select * from client where client_id = ?"
            <params> is a list of 3-tuples (value, sqltype, fieldname).  (The
            fieldname is not strictly necessary, but useful for debugging.)
            In certain cases, it may also be a list of strings.
        """
        rs = self._run_command(sql, params=params, timeout=timeout)
        for i in range(100):
            if self.is_result_set(rs):
                break # continue as usual, with this recordset
            else:
                try:
                    rs, code = rs.NextRecordset()
                except AttributeError:
                    # if rs does not have an attribute NextRecordset,
                    # then apparently we don't have a valid recordset,
                    # and should return []
                    return []

        if rs.Eof:
            return []

        fieldnames = self._get_fieldnames(rs)
        rows = rs.GetRows()
        d = self._make_dict(rows, fieldnames)
        self.ado_convert(d)
        return d

    #
    # auxiliary methods

    def is_result_set(self, recordset):
        """ Sometimes stored procs return record sets for things that aren't
            actually result sets.  This function determines whether an
            ADO.RecordSet is valid or not. """
        try:
            recordset.Eof
        except:
            return 0
        else:
            return 1

    def ado_convert(self, resultset):
        """ Convert the values in a result set to strings. """
        for row in resultset:
            for key, value in row.items():
                row[key] = tools.ado_to_string(value)

    def _make_dict(self, rows, fieldnames):
        """ Make a list of dictionaries from a list of ADO-returned rows. """
        transposed_rows = zip(*rows)
        qrows = []
        for row in transposed_rows:
            d = {}
            for j in range(len(fieldnames)):
                d[fieldnames[j]] = row[j]
            qrows.append(d)
        return qrows

    def _get_fieldnames(self, rs):
        """ Get a list of fieldnames, in order, from an ADO recordset. """
        return [field.Name for field in rs.Fields]

    def call_template(self, name, **params):
        pass

    def call_template_result(self, name, **params):
        pass

    def call(self, name, *args):
        """ Call a stored procedure. Return no result. """
        def quote(s):
            return "'%s'" % (s,)
        sql = "exec %s %s"
        paramlist = string.join(map(quote, map(str, args)), ", ")
        sql = sql % (name, paramlist)
        self.runsql(sql)

    def call_result(self, name, *args):
        """ Call a stored procedure and return the result set. """
        def quote(s):
            return "'%s'" % (s,)
        sql = "exec %s %s"
        paramlist = string.join(map(quote, map(str, args)), ", ")
        sql = sql % (name, paramlist)
        rows = self.runsql_result(sql)
        return rows

    def get_max_id(self, tablename, id_fieldname):
        """ Get the maximum value of the id field for the given table. """
        rows = self.runsql_result("select isnull(max(%s),0) as m from %s" % (
               id_fieldname, tablename))
               # uses isnull() in case there are no records in the table
        return rows[0]['m']

    def get_ids_after(self, tablename, id_fieldname, id):
        if type(id) == type(""):
            repr_id = "'%s'" % (id,)    # force single quotes
        else:
            repr_id = repr(id)

        rows = self.runsql_result("select %s from %s where %s > %s" % (
               id_fieldname, tablename, id_fieldname, repr_id))
        return [row[id_fieldname] for row in rows]

    def count(self, tablename, **criteria):
        """ Count the number of records in a given table that meet a certain
            set of criteria.
            Example: count("ticket", ticket_id=2080) (check if there is a
             ticket with ID 2080)
        """
        sql = "select count(*) as cnt from %s where 1=1" % (tablename,)
        for key, value in criteria.items():
            sql = sql + "and %s = '%s' " % (key, value)
        rows = self.runsql_result(sql)
        return int(rows[0]['cnt'])

    def getrecords(self, tablename, **criteria):
        """ Get all records from <tablename> that match the given criteria.
            These records are returned as a list of dicts.
        """
        sql = "select * from %s where 1=1 " % (tablename)
        for key, value in criteria.items():
            if value == 'IS NOT NULL':
                sql = sql + "and %s IS NOT NULL " % (key)
            elif value == 'IS NULL':
                sql = sql + "and %s IS NULL " % (key)
            else:
                sql = sql + "and %s = '%s' " % (key, value)
        results = self.runsql_result(sql)
        return results

    def getrecords_ordered(self, tablename, orderfield, **criteria):
        """ Like getrecords(), but orders the result set by <orderfield>. """
        sql = "select * from %s where 1=1 " % (tablename)
        for key, value in criteria.items():
            sql = sql + "and %s = '%s' " % (key, value)
        sql = sql + " order by %s" % (orderfield,)
        results = self.runsql_result(sql)
        return results

    def deleterecords(self, tablename, **criteria):
        """ Delete records from <tablename>, based on the given criteria.
        """
        sql = "delete %s where 1=1 " % (tablename)
        for key, value in criteria.items():
            sql = sql + "and %s = '%s' " % (key, value)
        self.runsql(sql)
        # I don't think we need to return anything...?
        # Is it possible the return the IDs of the deleted records? They will
        # hardly be of any use (because they don't exist anymore :-), but maybe
        # a list of IDs can function as some kind of information... e.g. "3
        # records deleted".
    delete = deleterecords

    def insertrecord(self, tablename, **criteria):
        sql = "insert %s (%s) values (%s)"
        fields = []
        values = []
        for field, value in criteria.items():
            fields.append(field)
            values.append(value)
        s_fields = string.join(fields, ", ")
        values = map(tools.sqlify, values)
        s_values = string.join(values, ", ")

        sql = sql % (tablename, s_fields, s_values)

        self.runsql(sql)
    insert = insertrecord

    def updaterecord(self, tablename, id_fieldname, id, timeout=None, **kwargs):
        """ Update a record in <tablename>, using <id_fieldname> = <id>.
            Update the fields passed as keyword arguments.

            Example:
            update('employee', 'emp_id', 4708, short_name='John')
        """
        if not kwargs:
            return  # nothing to update

        fieldlist = ["%s=%s" % (name, tools.sqlify(value))
                     for (name, value) in kwargs.items()]
        fields = string.join(fieldlist, ", ")

        sql = """
         update %s
         set %s
         where %s = %s
        """ % (tablename, fields, id_fieldname, tools.sqlify(id))

        self.runsql(sql)
    update = updaterecord

    #
    # transactions

    def begin_transaction(self):
        try:
            return self.conn.BeginTrans()
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e,
             timeout_seconds=self.conn.ConnectionTimeout)
            raise

    def rollback_transaction(self):
        try:
            return self.conn.RollbackTrans()
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e,
             timeout_seconds=self.conn.ConnectionTimeout)
            raise

    def commit_transaction(self):
        try:
            return self.conn.CommitTrans()
        except pythoncom.com_error, e:
            self._reraise_com_db_error(e,
             timeout_seconds=self.conn.ConnectionTimeout)
            raise



if __name__ == "__main__":

    # very simple test to see if connection works
    db = DBInterfaceADO()
    print "Number of records in the USERS table:"
    print db.count('users')

