# handle_manual_tickets.py

from __future__ import with_statement
import getopt
import sys
#
import businesslogic
import config
import datadir
import date_special
import dbinterface_ado
import emailtools
import errorhandling2
import errorhandling_special
import mutex
import program_status
import ticket_db
import ticketdb_decorator
import tools
import version

PROCESS_NAME = 'ManualTicketHandler'


class ManualTicketHandler:

    def __init__(self, configfile="", verbose=1, dbado=None):
        self.verbose = verbose

        tools.check_python_version()

        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        self.tdb = ticket_db.TicketDB(dbado=dbado)
        self.tdb = ticketdb_decorator.TicketDBDecorator(self.tdb,
                   ticketdb_decorator.TicketDBMethodDecorator)
        self.holiday_list = date_special.read_holidays(self.tdb)
        self.email = emailtools.Emails(self.tdb)

        # install logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="ManualTicketHandler",
                   cc_emails=self.email,
                   admin_emails=self.config.admins,
                   subject="ManualTicketHandler Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.verbose

        self.biz_id = datadir.datadir.get_id() # still necessary

        self.log.log_event("ManualTicketHandler started (version %s)" %
         version.__version__)

        # write program status
        mutex_name = PROCESS_NAME + ":" + self.biz_id
        program_status.write_status(mutex_name)

        self.logic = {}
        # cache of BusinessLogic instances

    def get_manual_tickets(self):
        sql = """
         select top 500 * from ticket
         where status like 'MANUAL%'
         and due_date is null
        """
        rows = self.tdb.runsql_result(sql)

        # add additional dummy fields to be sure due date functions work
        for row in rows:
            row['client_code'] = 'DUMMY'

        return rows

    def set_fields(self, row):
        call_center = row['ticket_format']

        # for LocInc, set the work_date as well, if it's empty
        # (the due date computations of some of the LocInc centers depends
        # on the work_date)
        work_date_clause = ""
        if self.biz_id.lower().startswith('loc'):
            if not row['work_date']:
                row['work_date'] = row['transmit_date']
                work_date_clause = ", work_date = '%s'" % (row['work_date'],)

        logic = self.get_businesslogic(call_center)
        due_date = logic.legal_due_date(row)

        if not due_date:
            self.log.log_event("Could not set due date for ticket_id %s" % (row['ticket_id'],))
            return

        sql = """
            update ticket
            set due_date = '%s', legal_due_date = '%s'
            %s
            where ticket_id = %s
            """ % (due_date, due_date, work_date_clause, row['ticket_id'])

        try:
            self.tdb.runsql(sql)
            self.log.log_event("Due date for ticket_id %s (%s, %s) set to %r" %
                               (row['ticket_id'], call_center, row['transmit_date'], due_date))
            self.set_workload_dates(row)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Could not set due date")
            self.log.log(ep, send=1, write=1, dump=self.verbose)

    def get_businesslogic(self, call_center):
        try:
            return self.logic[call_center]
        except KeyError:
            logic = businesslogic.getbusinesslogic(call_center, self.tdb,
                    self.holiday_list)
            self.logic[call_center] = logic
            return logic

    def set_workload_dates(self, row):
        assignments = self.tdb.runsql_result("""
            select t.due_date, t.transmit_date, t.ticket_format, a.locate_id, a.locator_id, a.added_by
            from ticket t
            join locate l on l.ticket_id = t.ticket_id
            join assignment a on a.locate_id = l.locate_id
            where
                t.ticket_id = %s
                and l.closed = 0
                and l.active = 1
                and a.active = 1
                and a.workload_date is null
            """ % (row['ticket_id']))
                
        if not assignments:
            self.log.log_event("No open locates with active assignments found")
            return
        else:
            for assignment in assignments:

                if assignment['added_by'].isdigit():
                    added_by = assignment['added_by']
                else:
                    added_by = 'null'

                self.tdb.runsql("""
                    exec assign_locate %s, '%s', %s, '%s'""" % (
                    assignment['locate_id'], assignment['locator_id'], added_by, assignment['due_date']))

                self.log.log_event("Workload date for locate_id %s (%s, %s) set to %s" % (
                    assignment['locate_id'], assignment['ticket_format'],
                    assignment['transmit_date'], assignment['due_date']))

    def run(self):
        try:
            self.log.log_event("Getting pending manual tickets...")
            rows = self.get_manual_tickets()
            self.log.log_event("%d rows found" % (len(rows),))
            for row in rows:
                self.set_fields(row)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Error when trying to process manual tickets")
            self.log.log(ep, send=1, dump=1, write=1)
            self.log.force_send()

        self.log.log_event("End of manual ticket handler")

()

if __name__ == "__main__":

    configfile = ""

    opts, args = getopt.getopt(sys.argv[1:], "c:", ["data="])

    for o, a in opts:
        if o == "-c":
            configfile = a
        elif o == "--data":
            print "Data dir:", a

    try:
        log = errorhandling_special.toplevel_logger(configfile,
              "ManualTicketHandler", "ManualTicketHandler Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "ManualTicketHandler")
        sys.exit(1)

    mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        with mutex.MutexLock(dbado, mutex_name):
            mth = ManualTicketHandler(configfile=configfile, dbado=dbado)
            mth.run()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)
