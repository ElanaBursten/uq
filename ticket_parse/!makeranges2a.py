# !makeranges2a.py

import sys
import tools

call_center = sys.argv[2]

letters = {
    "A": 75,
    "B": 50,
    "C": 25,
    "D": 0,
}

srettel = {
    75: "A",
    50: "B",
    25: "C",
    0: "D",
}

def inc_grid((number1, number2, frac)):
    if frac < 75:
        frac = frac + 25
        return (number1, number2, frac)
    else:
        frac = 0
        number2 = number2 + 1
        if number2 >= 60:
            number2 = number2 - 60
            number1 = number1 + 1
        return (number1, number2, frac)

def split_grid(grid):
    return int(grid[:2]), int(grid[2:4]), letters[grid[4:]]

def join_grid((number1, number2, letter)):
    return "%02d%02d%s" % (number1, number2, srettel[letter])

def float_to_split(f):
    number1 = int(f) // 100
    number2 = int(f) - number1 * 100
    frac = int((f - int(f)) * 100)
    return number1, number2, frac

def all_between(grid1, grid2):
    a = split_grid(grid1)
    b = split_grid(grid2)
    if a > b:
        a, b = b, a
    stuff = []
    c = a
    while c <= b:
        stuff.append(c)
        c = inc_grid(c)
    return [join_grid(x) for x in stuff]

def all_in_square(lat1, long1, lat2, long2):
    lats = all_between(lat1, lat2)
    longs = all_between(long1, long2)
    return [z for z in tools.cartesian(lats, longs)]


if __name__ == "__main__":

    def test1():
        print split_grid('3337A')
        a = split_grid('3359D')
        for i in range(10):
            print a
            a = inc_grid(a)
        print join_grid(a)

    def test2():
        a = all_between('3337A', '3341A')
        print a, len(a)
        a = all_between('8403A', '8359A')
        print a, len(a)

    def test3():
        all = all_in_square('3337A', '8403A', '3341A', '8359A')
        print all
        print len(all)

    def test4():
        print float_to_split(8445.5)
        print float_to_split(8450)
        print float_to_split(8355.75)

    def main():
        filename = sys.argv[1]
        lines = open(filename, "r").readlines()
        del lines[0]
        numgrids = 0
        duplicates = 0
        collected = {}
        f = open("out.tsv", "w")
        for line in lines:
            parts = line.split()
            #minx, maxx, miny, maxy = map(join_grid, map(float_to_split, map(float, parts[4:8])))
            a, b = parts[1], parts[3]
            a1, a2 = a[:5], a[5:]
            b1, b2 = b[:5], b[5:]
            minx, maxx, miny, maxy = b2, a2, a1, b1
            #grids = [b2, a2, a1, a2]
            #minx, maxx, miny, maxy = map(join_grid, map(float_to_split, map(float, grids)))

            area_id = parts[0]
            print minx, maxx, miny, maxy, "=>", area_id

            all = all_in_square(miny, minx, maxy, maxx)
            print len(all), "grids in square"
            numgrids += len(all)

            for (lat, long) in all:
                if collected.has_key((lat, long)):
                    duplicates += 1    # skip duplicate
                else:
                    s = "%s\t%s\t%s\t1\t%s" % (lat, long, area_id, call_center)
                    print >> f, s
                    collected[(lat, long)] = 1

        print "A total of", numgrids, "grids would be generated."
        print "minus", duplicates, "duplicates"

    #test4()

    main()
