# work_order_audit_parser.py
#
# XXX has much in common with work_order_parser.py; refactor later

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

import et_tools
from work_order_audit import WorkOrderAuditHeader, WorkOrderAuditDetail
import tools

class WorkOrderAuditParser(object):
    pass

# XXX format is not 100% certain yet
class WorkOrderAuditXMLParser(WorkOrderAuditParser):

    def parse(self, data):
        self._root = ET.fromstring(data)
        self._data = data

        # prepare WorkOrder record
        wo = WorkOrderAuditHeader()
        fields = {}

        # attributes starting with 'x_' map to WorkOrder field names (without
        # the leading 'x_'). post-processing and more complex extractions can
        # be handled in post_process().

        fieldnames = [name[2:] for name in dir(self) if name.startswith("x_")]
        for fn in fieldnames:
            xml_fieldname = getattr(self, 'x_' + fn, None)
            if xml_fieldname is None:
                continue
            fields[fn] = et_tools.find(self._root, xml_fieldname).text

        wo.set(**fields)

        self.post_process(wo)

        return wo

    def post_process(self, wo_audit):
        pass  # override in subclasses


