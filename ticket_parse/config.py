# config.py

from config_etree import *

import status_translation as st
import dbinterface as dbi


def update_config_from_db(cfg):

    if cfg.db is not None:
        db = cfg.db
    else:
        db = dbi.detect_interface()

    # read responder status translations
    translation_rows = st.StatusTranslation.read_all(db)
    cfg.read_status_translations_db(translation_rows)

    # ...future database interaction goes here...

    return cfg

__usage__ = """\
config.py [options]

Options:
    -C, --check   Check if the XML file is well-formed.
    -v            Display contents of configuration object.
"""

if __name__ == "__main__":

    import getopt, pprint, sys

    cfg = Configuration()

    opts, args = getopt.getopt(sys.argv[1:], "Cv", ["check"])
    for o, a in opts:
        if o == '-v':
            pprint.pprint(cfg.__dict__)
        elif o in ('-C', '--check'):
            cfg.validate()
            print "Configuration OK"

    if not opts:
        print __usage__

