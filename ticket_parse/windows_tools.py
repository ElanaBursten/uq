# windows_tools.py
# Windows-related tools.
# Created: 28 Mar 2002, Hans Nowak
#
# TODO: Must be usable by both CPython and IronPython, using the same
# interface.

import detect
import datadir

#
# implementation-specific code.

if detect.is_ironpython():
    raise NotImplementedError("IronPython not supported yet")

else: # CPython
    import win32api

    def setconsoletitle(title):
        win32api.SetConsoleTitle(title)
