# find_processed.py
# Find tickets received by the SW server on a given date.

import sys
import string
import os
import glob

__usage__ = """\
find_processed.py directory template

Find tickets received in <directory>. Use file mask <template>. Ticket headers
are written to a new file out.txt.

Example:
    find_processed.py fco1_processed FCO1a-2002-09-16*.txt
"""

SEPARATOR = "~~~~~"

def find_processed(directory, template):
    mask = os.path.join(directory, template)
    filenames = glob.glob(mask)

    g = open("out.txt", "w")

    for filename in filenames:
        f = open(filename, "rb")
        data = f.read()
        f.close()

        raw_tickets = string.split(data, SEPARATOR)
        for raw_ticket in raw_tickets:
            # get the first line
            lines = string.split(raw_ticket, "\n")
            print >> g, lines[0]
            print lines[0]

    g.close()



if __name__ == "__main__":

    try:
        directory, template = sys.argv[1:3]
    except:
        print __usage__
        sys.exit(1)

    z = find_processed(directory, template)

