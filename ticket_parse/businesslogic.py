# businesslogic.py

import string
import traceback
import logging
# logging for debugging purposes
log_debug = logging.getLogger("ticketrouter.businesslogic")

import callcenters  # package
import callcenters.duedates
import call_centers # module
import config
import datadir
import date_special
import duedatecatalog
import geocoder
import locatorcatalog
import routinglist
import ticket as ticketmod
import ticketackmatcher
import tools

def _read_cc_routing_list(filename="rules/cc_routing_list.tsv"):
    """ Read the cc_routing_list file. This is always looked for in the data
        dir, unless an absolute path is given (useful for e.g. testing). """
    #if not filename:
    #    #filename = config.getConfiguration().cc_routing_file
    #    filename = 'rules/cc_routing_list.tsv'
    try:
        f = datadir.datadir.open(filename, "r")
    except:
        print 'Warning: routing list', filename, 'not found.'
        return []
    lines = f.readlines()
    f.close()

    data = []
    for line in lines:
        fields = line.split("\t")
        if len(fields) != 5:
            continue    # ignore this line
        fields[4] = fields[4].strip()   # remove possible newline
        data.append(fields[:5])

    return data

class BusinessLogicError(Exception):
    pass

class BusinessLogic:
    """ Abstract class for implementing business logic. Call centers subclass
        this and override the empty methods with meaningful ones.
    """

    DEFAULT_LOCATOR = None
    call_center = None

    # shared between instances:
    cc_routing_list = []
    consolidated_response_groups = []

    update_due_dates = False
    # by default, update tickets don't cause a new due date to be calculated
    # or set; set this to True to change that behavior (affects main.py)
    # NOTE: if True, due_date() and legal_due_date() methods are expected to
    # have a signature like legal_due_date(self, row, is_update).

    def __init__(self, tdb, holiday_list, geocoder=None):
        self.tdb = tdb  # reference to TicketDB instance
        self.lcatalog = locatorcatalog.LocatorCatalog(tdb)
        self.dcatalog = duedatecatalog.DueDateCatalog(self.call_center,
                        holiday_list)
        self.holiday_list = holiday_list
        self.geocoder = geocoder
        self.routinglist = routinglist.RoutingList(self.tdb)
        self.ticketackmatcher = ticketackmatcher.TicketAckMatcher(self.tdb)
        self.call_centers = call_centers.get_call_centers(self.tdb)

        # cc_routing_list is a class attribute so we only have to read it once,
        # then share it among other BusinessLogic classes
        if not BusinessLogic.cc_routing_list:
            BusinessLogic.cc_routing_list = _read_cc_routing_list()

    def cc_routing(self, row):
        # defined here rather than in locatorcatalog; it's temporary anyway
        area_id_found = None
        our_client_code = row.get("client_code", "")
        our_county = row.get("work_county", "")
        our_city = row.get("work_city", "")

        for (call_center, client_code, county, city, area_id) \
        in self.__class__.cc_routing_list:
            if call_center == self.call_center \
            and our_client_code == client_code \
            and our_county == county \
            and (our_city == city or city == "*"):
                area_id_found = area_id
                break

        if area_id_found:
            # this will only find active locators
            locator = self.tdb.get_locator_by_area(area_id_found)
        else:
            locator = None

        return tools.RoutingResult(area_id=area_id_found, locator=locator)

    def legal_due_date(self, ticketrow):
        # the ticketrow passed in comes from the ticketrouter and contains
        # values from the tickets_to_route template.
        raise NotImplementedError("No due date calculation implemented for "\
          "call center: %s" % self.call_center)

    def due_date(self, ticketrow):
        return self.legal_due_date(ticketrow)

    def base_date(self, d):
        """ If a date falls in a weekend or on a holiday (according to the
            holiday table), then return a new "base date" for 7 AM on the next
            business day.
            <d> is a string of the form 'YYYY-MM-DD HH:MM:SS'.
            Should only be used for non-emergency tickets.
        """
        return callcenters.duedates.correct_base_date(self.dcatalog, d)

    def base_date_rev(self, d):
        return callcenters.duedates.correct_base_date_rev(self.dcatalog, d)

    @staticmethod
    def isrenotification(ticket):
        return 0    # may accept Ticket or ticket row (dict)

    @staticmethod
    def do_no_client_check(clientcode=""):
        """ Return true if we do the "no client check". """
        return 1    # by default, always do client check

    def is_ticket_ack(self, ticketrow):
        """ Return true if this ticket should be added to ticket_ack. """
        return self.isemergency(ticketrow) \
            or self.ticketackmatcher.is_ticket_ack(self.call_center,
                                                   ticketrow['ticket_type'])
        # by default, emergencies go to ticket_ack, and matches found by
        # TicketAckMatcher

    def get_work_priority(self, ticketrow):
        return self.ticketackmatcher.get_work_priority(self.call_center,
                                                       ticketrow['ticket_type'])

    @staticmethod
    def isemergency(ticket):
        """ Return true if this ticket is considered an emergency. Can be
            called with both Ticket instances as ticket rows (dicts). """
        return ticket["kind"] == "EMERGENCY" \
         or ticket["ticket_type"].find("EMER") > -1 \
         or ticket["ticket_type"].find("RUSH") > -1
        # may need to be more sophisticated and based on ticket_type...?

    @staticmethod
    def is3hr(ticket):
        """ Return true if this ticket is considered a 3hr response. """
        return ticket["ticket_type"].find("3HRS") > -1

    def respond_to_original(self, ticket, locate_id):
        """ Determine whether a response is necessary for a 3 hr ticket
            that updates another ticket. """

        # Did this ticket update a ticket?
        versions = self.tdb.get_ticket_versions(ticket.ticket_id)
        if len(versions) > 1:
            # Did ticket update a non-3 hour/emer ticket (i.e. normal, no show,
            # etc.)?
            # At least 1 update, get the kind of the previous updates
            versions.reverse() # Will examine from latest to earliest
            for version in versions[1:]:
                version.faux_ticket = {
                  'kind': '',
                  'ticket_type': version.ticket_type
                }
                if not self.isemergency(version.faux_ticket) \
                and not self.is3hr(version.faux_ticket):
                    # Ticket is updating a non-emergency/non-3hr ticket(s)
                    # Was locate marked and response successfully sent for this other ticket?
                    responses = self.tdb.getrecords("response_log", locate_id=locate_id)
                    # If no response sent, respond
                    if not responses:
                        return True # No response was sent, respond
                    # Examine the last response
                    responses.reverse()
                    # If locate not marked or the previous response was not successful, respond
                    if responses[0]['status'] != 'M' \
                    or not int(responses[0]['success']):
                        return True # Respond
        return False # Do not respond

    @staticmethod
    def is_no_show(ticket):
        """ Return true if the ticket is NO SHOW. """
        return ticket["ticket_type"].strip() == "NO SHOW"

    def isbusinessday(self, date):
        """ Check if Date instance is a business day, i.e. it's not a weekend
            day or holiday. """
        return not (date.isweekend() or
         date_special.isholiday(date, self.call_center, self.holiday_list))

    @staticmethod
    def allow_summary_updates():
        """ Return true if summaries can be updated. """
        return 0
        # by default, we disallow this

    def use_prerouting(self):
        """ Return true if we look for an existing locator on the ticket and
            use that, instead of (re)computing a new locator for every locate.
        """
        d = self.call_centers.get(self.call_center)
        if d:
            z = bool(int(d.get('use_prerouting', 1)))
            return z
        else:
            return True # by default, do use pre-routing

    def locator(self, row):
        """ Generic method for checking various locator methods to find a
            locator. Must _always_ return a RoutingResult object. """
        for f in self.locator_methods(row):
            try:
                if callable(f):
                    routingresult = f(row)
                else:
                    raise ValueError, "Invalid locator method spec: %s" % (f,)
                if not isinstance(routingresult, tools.RoutingResult):
                    raise ValueError, "Not a RoutingResult instance: %s" % (
                     routingresult,)
            except:
                log_debug.exception("Exception in locator method %s" %
                  tools.func_name(f))
                traceback.print_exc()
                # XXX this really needs to be logged or something
                # but in order to do that, BusinessLogic needs a log object
            else:
                if routingresult.locator:
                    routingresult.routing_method = tools.func_name(f)
                    return routingresult

        # none of the locator methods yielded a locator
        return tools.RoutingResult(locator=self.DEFAULT_LOCATOR,
               routing_method="default")

    def locator_methods(self, row):
        # all classes should have state/county/city routing by default
        return [self.lcatalog.munic,]

    # If we want to use the routinglist table for a given call center, then
    # just put this method in locator_methods.
    def route_by_routinglist(self, row):
        result = self.routinglist.route(row)
        if result:
            rule, area_id = result
            # find (active) locator for this area
            locator_id = self.tdb.get_locator_by_area(area_id)
            if locator_id:
                data = "matched: [%s] %r" % (rule.field_name, rule.field_value)
                return tools.RoutingResult(locator=locator_id,
                       area_id=area_id, data=data)

        return tools.RoutingResult()

    def do_not_respond_before_date(self, ticket):
        return None

    def consolidate_response_status(self, rows):
        raise NotImplementedError

    def screen(self, ticket, ticket_id, image):
        """ Determine if locate should be screened, i.e. route locate, mark
            locate as "S", and then close locate. """
        return False

    def geocode_with_cross_streets(self, ticket):
        return not ticket.work_address_number and ticket.work_cross

    def valid_coordinates(self, latitude, longitude):
        """ Override to set narrower ranges """
        return 90 > latitude > 0 and -180 < longitude < 0

    def geocode_new_ticket(self, ticket, min_precision):
        if self.geocode_with_cross_streets(ticket):
            geocoded, matches = self.geocoder.lat_long_from_cross_streets(
             ticket.work_address_street, ticket.work_cross, ticket.work_city,
             ticket.work_county, ticket.work_state)
        else:
            geocoded, matches = self.geocoder.lat_long_from_street_address(
             ticket.work_address_number, ticket.work_address_street,
             ticket.work_city, ticket.work_county, ticket.work_state)

        best_match = {}     
        for match in matches:
            if (not best_match or match['precision'] > best_match['precision']) \
             and match['precision'] >= min_precision \
             and self.valid_coordinates(match['latitude'], match['longitude']) \
             and (not ticket.map_page or ticket.map_page == \
             tools.decimal2grid(match['latitude'], match['longitude'])):
                best_match = match
                
        """
        geocode_precision values: 
        * null: Geocoding results never recorded
        * -10: Geocoding results previously recorded, but current lat/long
          parsed. Can't just set to null, because nulls don't sync down
        * 0: Geocoding successful, but no matches found
        * >0: Geocoding successful. Best match recorded. Code indicates type
          of match (Google) or precision range.              
        """

        if geocoded:
            if best_match:
                ticket.work_lat = best_match['latitude']
                ticket.work_long = best_match['longitude']
                ticket.geocode_precision = best_match['precision']
                if not ticket.map_page:
                    # todo(dan) do this for all geocoded centers?
                    ticket.map_page = tools.decimal2grid(ticket.work_lat,
                     ticket.work_long)
            else:
                ticket.work_lat = ticketmod.LAT_LONG_DEFAULT
                ticket.work_long = ticketmod.LAT_LONG_DEFAULT
                # todo(dan) use a constant?
                ticket.geocode_precision = 0
    
def get_businesslogic_class(call_center):
    """ Find the (uninstantiated) BusinessLogic class for the given call
        center. """
    if call_center == "fhl1":
        call_center = call_center.upper()
    if call_center[0] not in string.letters:
        call_center = 'c' + call_center
        # call center '300' has class c300BusinessLogic, etc, because
        # '300BusinessLogic' is not a valid identifier
    name = call_center + "BusinessLogic"

    # try to find *BusinessLogic in this module
    try:
        blclass = globals()[name]
    except:
        # otherwise, look in callcenters package
        try:
            blclass = callcenters.get_businesslogic(call_center)
        except:
            traceback.print_exc()
            raise BusinessLogicError, \
                  "No business logic class available for %s" % (call_center)

    return blclass

def getbusinesslogic(call_center, tdb, holiday_list, geocoder=None):
    """ Get a business logic instance for the given call center. """
    blclass = get_businesslogic_class(call_center)
    return blclass(tdb, holiday_list, geocoder)

def show_hierarchy():
    """ Show the hierarchy of business logic classes.  Important when
        determining what inherits from what. """
    def get_call_center_parent(cc_name):
        """ Return the name of the parent BusinessLogic class, or None if there
            is no parent. """
        klass = get_businesslogic_class(cc_name)
        parent = klass.__bases__[0]
        cutoff = len("BusinessLogic")

        if parent is BusinessLogic:
            parent_name = None
        elif parent.__module__ in ('businesslogic', '__main__'):
            parent_name = klass.__name__[:-cutoff] or None
        else:
            parent_name = parent.__module__.split('.')[-1]

        # remove leading lowercase 'c' from certain names
        if parent_name is not None and parent_name.startswith('c') \
        and parent_name[1:2] in "0123456789":
            parent_name = parent_name[1:]

        return parent_name

    cc_names = call_centers.cc2format.keys()
    cc_pairs = [(cc_name, get_call_center_parent(cc_name))
                for cc_name in cc_names]

    import pprint
    pprint.pprint(sorted(cc_pairs))



if __name__ == "__main__":

    show_hierarchy()

