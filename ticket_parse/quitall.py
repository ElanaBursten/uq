# quitall.py
# Stop cron-like services, and quit all running Python processes.
# Assumes availability of programs 'tlist' and 'crons' (or pycron).

import getopt
import os
import sys
import time
#
import killall
import listener
import sendquitsignal

__usage__ = """\
quitall.py [options]
Quit Python processes.  This only works for those programs that react to the
'quit' signal.

Options:
    -n N        Leave a maximum of N Python processes open.  Default is 1.
                Useful if there are other Python programs running that don't
                need closed.
    -t SEC      Spend up to SEC seconds waiting for processes to close down.
    -v          (verbose) Show output of tlist.
    -k          Kill processes that cannot be terminated normally.
"""

class QuitAll:

    def __init__(self, maxopen=1, monitor_time=30, verbose=0, kill=0):
        self.maxopen = maxopen # maximum number of Python processes open
        self.monitor_time = monitor_time
        self.verbose = verbose
        self.kill = kill

    def stop_crons(self):
        #self.stop_crons_1()
        self.stop_crons_2()

    def stop_crons_1(self):
        print "Stopping crons..."
        pin, pout, perr = os.popen3("crons /stop")
        #os.system("crons /stop")
        for line in pout:
            sys.stdout.write("> " + line)
        print "OK"
        print "Restart service later with: crons /start"
        # TODO: we should look at the output to determine if it's really OK

    def stop_crons_2(self):
        print "Stopping pycron..."
        pin, pout, perr = os.popen3("net stop pycron")
        for line in pout:
            sys.stdout.write("> " + line)
        print "OK"
        print "Restart service later with: net start pycron"
        # TODO: we should look at the output to determine if it's really OK

    def send_quit_signal(self):
        print "Sending 'quit' signal to processes..."
        sendquitsignal.send_quit_signal()
        print "OK"

    def is_python_process(self, line):
        return line.find("python.exe") > -1
        # may be extended with pythonw.exe, etc.

    def monitor_processes(self):
        """ Monitor Python processes to see if they are closed.  Return 0 if
            all of them are closed; return 1 otherwise. """
        start = time.time()
        while (time.time() - start) < self.monitor_time:
            print "Python processes currently active:",
            pin, pout, perr = os.popen3("tlist")
            lines = [line for line in pout if self.is_python_process(line)]
            print len(lines)
            if self.verbose:
                for line in lines:
                    sys.stdout.write("> " + line)
                print

            if len(lines) <= self.maxopen:
                print "All relevant Python processes are closed."
                break

            time.sleep(5)

        else:
            print "Not all processes were closed."
            return 1

        return 0

    def release_quit_signal(self):
        print "Releasing 'quit' signal..."
        sendquitsignal.release_quit_signal()
        print "OK"

    def run(self):
        self.stop_crons()
        self.send_quit_signal()
        stuck = 0
        try:
            stuck = self.monitor_processes()
        finally:
            self.release_quit_signal()

        # if there are still processes open, and self.kill is true, then
        # terminate the process forcefully
        if stuck and self.kill:
            killall.killall()


if __name__ == "__main__":

    maxopen = 1
    monitor_time = 30
    verbose = kill = 0

    opts, args = getopt.getopt(sys.argv[1:], "kn:t:v?")
    for o, a in opts:
        if o == "-n":
            maxopen = int(a)
        elif o == "-t":
            monitor_time = int(a)
        elif o == "-v":
            verbose = 1
        elif o == '-k':
            kill = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)

    quitter = QuitAll(maxopen=maxopen, monitor_time=monitor_time,
              verbose=verbose, kill=kill)
    quitter.run()

