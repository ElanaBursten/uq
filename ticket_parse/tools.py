# tools.py
# Some tools for processing strings, and such.

# TODO: split off: stringtools.py, filetools.py

from __future__ import generators
import glob
import operator
import os
import pywintypes
import re
import string
import sys
import time
import types
import optimize
import version
import warnings

urlfinders = [
    re.compile("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}|"+\
               "(((news|telnet|nttp|file|ftp)://)|(ftp)"+\
               "[-A-Za-z0-9]*\\.)[-A-Za-z0-9\\.]+)(:[0-9]*)?/[-A-Za-z0-9"+\
               "_\\$\\.\\+\\!\\*\\(\\),;:@&=\\?/~\\#\\%]*[^]'\\.}>\\),\\\"]"),
    re.compile("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}|(((news|"+\
               "telnet|nttp|file|ftp)://)|(ftp)[-A-Za-z0-9]*"+\
               "\\.)[-A-Za-z0-9\\.]+)(:[0-9]*)?")]

# determine whether this Python version knows booleans
has_booleans = sys.version_info[:2] >= (2, 3)

def R(regex, flags=0):
    # Shortcut function, provided for convenience. We're going to write
    # re.compile("something", re.MULTILINE) a *lot*.
    return re.compile(regex, re.MULTILINE | flags)

def singlespaced(s):
    """ Turn a string with arbitrary whitespace into a single-spaced string.
    """
    parts = s.split()
    return string.join(parts, " ")

def udecode(x):
    if isinstance(x, unicode):
        return x.encode("utf-8")
    else:
        return str(x)

def sql2iso(s):
    """ Convert an SQL Server date string into an ISO date string, like we
        use. The differences aren't great, but tickets won't compare equal
        if this isn't fixed.
    """
    if s.startswith("1900"):
        return ""   # assume empty/unknown date
    s = s.replace("T", " ")
    if s[-4] == ".":
        s = s[:-4]  # remove trailing milliseconds
    return s

def converttokens(s):
    """ Convert &amp; to &, etc. Since xml.dom.minidom only seems to convert
        these 4, these are the only ones i'm converting back right now.
    """
    s = s.replace("&amp;", "&")
    s = s.replace("&lt;", "<")
    s = s.replace("&quot;", '"')
    s = s.replace("&gt;", ">")
    return s

def caller_info():
    """ Returns a list of calling functions.
        E.g. if foo calls bar, and bar calls baz, and baz calls popo,
        we get: ['popo', 'baz', 'bar', 'foo']
        Use for debugging purposes.
    """
    try:
        raise SyntaxError
    except:
        names = []
        frame = sys.exc_traceback.tb_frame
        while frame:
            name = frame.f_code.co_name
            names.append(name)
            frame = frame.f_back
        return names[1:-1]
        # first one is 'caller_info', last one is '?'

###
### SimpleSet

class SimpleSet:
    """ Rudimentary implementation of a set. Not complete, but complete
        enough to be used by ticketstats.
    """

    def __init__(self, seq=None):
        self.data = {}
        if seq:
            for item in seq:
                self.data[item] = 1

    def add(self, item):
        self.data[item] = 1

    def __contains__(self, item):
        # if x in mySet: ...
        return self.data.has_key(item)

    def __len__(self):
        return len(self.data)

    def __repr__(self):
        keys = self.data.keys()
        keys.sort()
        return repr(keys)

# Only hashable objects can be added to SimpleSet, so I add a function that
# checks this:
def hashable(x):
    try:
        hash(x)
        return 1
    except:
        return 0

def grid2decimal(s):
    """ Convert a grid value, like in the Atlanta ticket, to a float value
        representing longitude and latitude.
    """
    # remove unwanted characters, like the "-"
    s = string.join([c for c in list(s) if c in "0123456789ABCD*"], "")

    assert len(s) in (10, 11), "grid2decimal: Invalid length"
    letters = {"D": 0.125, "C": 0.375, "B": 0.625, "A": 0.875, "*": 0.5}
    islong = (len(s) == 11)
    lats, longs = s[:5], s[5:10+islong]

    lat_degrees = int(lats[:2])
    assert 0 <= lat_degrees <= 90, \
           "Invalid value for lat_degrees: %d" % (lat_degrees,)
    lat_minutes = int(lats[2:4]) + letters.get(lats[4], 0.0)
    assert 0 <= lat_minutes < 60, \
           "Invalid value for lat_minutes: %.2f" % (lat_minutes,)
    latitude = lat_degrees + (lat_minutes / 60.0)

    long_degrees = int(longs[:2+islong])
    assert 0 <= long_degrees <= 180, \
           "Invalid value for long_degrees: %d" % (long_degrees,)
           # how large do these degrees get anyway?
    long_minutes = int(longs[2+islong:4+islong]) + \
                   letters.get(longs[4+islong], 0.0)
    assert 0 <= long_minutes < 60, \
           "Invalid value for long_minutes: %.2f" % (long_minutes,)
    longitude = long_degrees + (long_minutes / 60.0)
    return latitude, longitude

def convertcoord(x):
    degrees = int(x)
    # we're doing some tricky stuff here so the rounding to ints works
    # correctly... 24.99999999 should be rounded to 25, not 24.
    # no, round() doesn't suffice.
    minutes = int(float(str((x - degrees) * 60)))
    frac = max(0, (x*60) - degrees*60 - minutes)
    letter = "X"    # so we know if there is a gap in the logic below
    if frac >= 0.00 and frac <= 0.25:
        letter = "D"
    elif frac > 0.25 and frac <= 0.50:
        letter = "C"
    elif frac > 0.50 and frac <= 0.75:
        letter = "B"
    elif frac > 0.75 and frac <= 1.00:
        letter = "A"
    assert letter != "X", "Gap in logic in tools.convertcoord"
    if len(str(abs(degrees))) <= 2:
        return "%02d%02d%s" % (degrees, minutes, letter)
    else:
        return "%d%02d%s" % (degrees, minutes, letter)

def decimal2grid(lat, longitude):
    """ Convert lat/long decimal values to qtr-min grid form,
        for use in routing via qtrmin
    """
    assert lat > 0, "must have North latitude"
    assert longitude < 0, "must have West longitude"
    return convertcoord(lat) + convertcoord(-longitude)

# XXX modernize
def average(lst):
    """ Return the average value of a list of elements. """
    if not lst:
        return 0
    _sum = reduce(operator.add, lst)
    return _sum / (1.0 * len(lst))

### ----------------------------------------------------------------------
### A Timer class.

class Timer:
    """ A simple timer. """
    def __init__(self):
        self.t1 = self.t2 = 0.0
    def start(self):
        self.t1 = time.time()
        self.t2 = 0.0
    def stop(self):
        assert self.t1, "Cannot stop timer before starting it"
        self.t2 = time.time()
    def elapsed(self):
        if self.t2:
            return self.t2 - self.t1
        else:
            return time.time() - self.t1
    def tell(self):
        """ Tells how much time has passed. Has two modi: after stopping, it
            tells how much time passed between starting and stopped. If the
            timer has been started, but not stopped yet, it tells how much time
            has passed so far.
        """
        if self.t2:
            print "%.2f seconds" % (self.t2 - self.t1)
        else:
            print "%.2f seconds (so far)" % (time.time() - self.t1)


def can_open_excl(filename):
    """ Check if the given file can be opened exclusively. """
    fd = os.open(filename, os.O_RDONLY | os.O_EXCL)
    try:
        os.read(fd, 1)
    except OSError, e:
        # is it OSError 13, "Permission denied"?
        if e.args and e.args[0] == 13:
            result = 0
        else:
            raise   # not the error we wanted
    else:
        result = 1  # no error
    os.close(fd)
    return result

def can_rename(filename):
    """ An alternative for can_open_excl. If the file is in use, then we
        certainly cannot rename it, eh? """
    newfilename = filename + "_"
    try:
        os.rename(filename, newfilename)
        os.rename(newfilename, filename)
    except OSError:
        return 0
    else:
        return 1

def attributes(obj):
    """ Return a dict with the attributes of a given object. Uses dir(), so
        attributes of parent objects are included as well.  (This is why
        simply using __dict__ wouldn't suffice.)
    """
    d = {}
    for name in dir(obj):
        d[name] = getattr(obj, name)
    return d

# These characters do not belong in a ticket image:
_UNWANTED = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x0b\x0c\x0e\x0f\x10"\
            "\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d"\
            "\x1e\x1f"

def remove_crud(data):
    """ Remove unwanted control characters from a string, e.g. a ticket
        image. Can be replaced by something else (e.g. a space) or not. """
    if data is None:
        return None
    chars = list(data)  # string -> list of characters
    # only keep characters in the normal ASCII 32..127 range, and line feeds
    # like \n, and Ctrl-L because it's used as a separator:
    chars = [c for c in chars
             if (0x20 <= ord(c) <= 127) or (c in "\n\r\t\f\x0c")]
    s = string.join(chars, "")
    return s

def remove_crud_2(data, unwanted=""):
    """ Remove line noise characters from a string.  If <unwanted> is
        not set, then all characters < ASCII 0x20 or >= 0x80 are filtered
        out. """
    # Note: does not exactly the same as remove_crud().
    if unwanted:
        chars = [c for c in data if c not in unwanted]
    else:
        chars = [c for c in data if 0x20 <= ord(c) < 0x80]
    return string.join(chars, '')

l = string.join(map(chr,range(0x20,128)), '') + "\n\r\t\f\x0c"
crud = re.compile(r"[^%s]"% (l,), re.MULTILINE)

def remove_crud_3(data):
    """ Remove unwanted control characters from a string, e.g. a ticket
        image. Can be replaced by something else (e.g. a space) or not. """
    if data is None:
        return None
    m = crud.search(data)
    if m:
        chars = list(data)  # string -> list of characters
        # only keep characters in the normal ASCII 32..127 range, and line feeds
        # like \n, and Ctrl-L because it's used as a separator:
        chars = [c for c in chars
                 if (0x20 <= ord(c) <= 127) or (c in "\n\r\t\f\x0c")]
        s = string.join(chars, "")
        return s
    else:
        return data

remove_crud = remove_crud_3

def isodate(date):
    """ Convert a date(time) string into an ISO date/time string.
        The date is assumed to be in the format MM/DD/YY, possibly with
        a time HH:MM. This should be converted to YYYY-MM-DD HH:MM:SS.

        Be prepared that this code can raise all kinds of exceptions. :-)
    """
    if " " in date:
        parts = date.split()
        date, time = parts[:2]
        am = pm = 0  # default: no PM time
        if parts[2:]:   # is there a third part? (AM/PM indicator)
            if parts[2].lower() in ("pm", "p"):
                pm = 1
                am = 0
            else:
                am = 1
                pm = 0
        elif len(parts[1]) == 4 and parts[1][2:] in ("AM", "PM"):
            # we also accept times like "08AM" (Lanham!)
            if parts[1][2:] == "PM":
                pm = 1
                am = 0
            else:
                am = 1
                pm = 0
            time = parts[1][:2] + ":00"
    else:
        time = ""
    m, d, y = map(int, date.split("/"))
    if len(str(y)) <= 2:
        y = 2000 + y
    isodate = "%04d-%02d-%02d" % (y, m, d)  # assuming dates past 1999
    if time:
        parts = map(int, time.split(":")) + [0, 0, 0]
        # computing the correct military hours is tricky:
        hours = parts[0]
        if hours == 12 and am and not pm:  # 12 AM
            hours = 0
        elif hours == 12 and pm:    # 12 PM
            hours = 12
        else:
            hours = hours + (pm * 12)
        t = "%02d:%02d:%02d" % (hours, parts[1], parts[2])
        isodate = isodate + " " + t
    else:
        # always append time, even if there is none known
        isodate = isodate + " 00:00:00"
    return isodate


def check_python_version():
    """ Raise an exception if we don't have the correct Python version.
        Currently, version 2.2 final or higher is required.
    """
    assert sys.version_info >= (2, 2, 0, "final"), \
           "Python 2.2 final or higher required"
    if sys.version_info[3] != "final":
        warnings.warn("Usage of non-final Python versions is not recommended",
                      UserWarning, 1)

def valid_locate_name(name, valid_chars=None):
    """ Return true if 'name' is an valid name for a locate. """
    if valid_chars is None:
        import parsers
        valid_chars = parsers.BaseParser.VALID_LOCATE_CHARS
    if not name:
        return 0    # empty names are not allowed
    for c in name:
        if c not in valid_chars:
            return 0
    return 1

def file_age(filename):
    """ Return the age of a file, in days. Result is a floating point number.
    """
    stats = os.stat(filename)
    cdate = stats[-2]   # mdate, really
    return (time.time() - cdate) / (60.0 * 60.0 * 24.0)

def remove_duplicates(lst):
    """ Remove duplicate elements from a list. """
    kept = []
    for elem in lst:
        if elem not in kept:
            kept.append(elem)
    return kept

def remove_duplicate_locates(locates):
    """ Remove duplicates from a list of locates. """
    kept = []
    for loc in locates:
        if loc.client_code not in [l.client_code for l in kept]:
            kept.append(loc)
    return kept

def findfirst(lst, f):
    """ Return the index of the first element of lst where f(elem) is true.
    """
    for i in range(len(lst)):
        if f(lst[i]):
            return i
    return -1   # nothing found

def find_block(lst, f1, f2):
    """ Find a block of items, starting at the first item x for which f1(x)
        is true, and ending at (and not including) the first item after that
        for which f2(x) is true. """
    collecting = 0
    collected = []
    for x in lst:
        if collecting == 0 and f1(x):
            collecting = 1
            collected.append(x)
        elif collecting == 1:
            if f2(x):
                return collected
            else:
                collected.append(x)


class Tee:
    """ Copy text to both standard output and a file.
        The to_stdout argument is optional; if not set, we don't write to
        stdout. This is useful for making output conditional. """
    def __init__(self, f, to_stdout=1):
        self.file = f
        self.to_stdout = to_stdout
    def write(self, s):
        self.file.write(s)
        if self.to_stdout:
            sys.stdout.write(s)
    def close(self):
        self.file.close()
        # don't close sys.stdout

class BetterTee:
    """ Like Tee, but can take any number of files. """
    def __init__(self, *files):
        self.files = files
    def write(self, s):
        for f in self.files:
            f.write(s)

#
# ADO stuff
@optimize.make_constants(verbose=False)
def ado_to_string(obj):
    """ Convert an object returned by an ADO query to a string. """
    if has_booleans and isinstance(obj, bool):
        return str(int(obj)) # '0' or '1'
    elif isinstance(obj, int):
        return str(obj) # '123'
    elif isinstance(obj, float):
        return str(obj) # '3.1415'
    elif isinstance(obj, unicode):
        return obj.encode("utf-8")
    elif isinstance(obj, types.NoneType):
        return None # None remains None
    elif isinstance(obj, pywintypes.TimeType):
        try:
            i = int(obj)    # get the integer representing the time
            t9 = time.localtime(i)  # get the 9-tuple
        except ValueError:
            t9 = (1900, 1, 1, 0, 0, 0, 0, 0, 0)
        # return an ISO date string
        return "%04d-%02d-%02d %02d:%02d:%02d" % tuple(t9[:6])
    else:
        # unrecognized... try default
        return str(obj)

def quote_apostrophes(s):
    """ Replace apostrophes in a string with double apostrophes, generating
        a valid SQL literal. """
    return s.replace("'", "''")

def func_name(f):
    """ Get the name of a function or method.  If it's a function, just
        return the name.  If it's a method, return a string of the format
        classname.methodname. """
    try:
        name = f.im_func.func_name
    except:
        try:
            name = f.func_name
        except:
            return "unknown"
    try:
        classname = f.im_class.__name__
    except:
        return name
    else:
        return classname + "." + name

class NullWriter:
    """ Behaves like a file, but doesn't actually read or write. """
    def write(self, data):
        pass
    def read(self, *args):
        pass
    def close(self):
        pass
    def readlines(self):
        pass

def get_fields(obj):
    """ Get attribute names from an object. """
    return [name for name in dir(obj) if not name.startswith("_")]

class Bunch(object):
    """ The Bunch idiom, for easy structs. Useful for e.g. grouping options. """
    def __init__(self, **kwargs):
        for name, value in kwargs.items():
            setattr(self, name, value)

class Struct:
    """ A simple struct class. Only attributes in __field__ are allowed. If not
        set, a default value is returned. """
    __fields__ = []
    default = None
    def __init__(self, **kwargs):
        for name in self.__fields__:
            setattr(self, name, self.default)
        for name, value in kwargs.items():
            setattr(self, name, value)
    def __setattr__(self, name, value):
        if name in self.__fields__:
            self.__dict__[name] = value
        else:
            raise AttributeError, name
    def __getattr__(self, name):
        try:
            return self.__dict__[name]
        except KeyError:
            if name in self.__fields__:
                return self.default
            else:
                raise AttributeError, name
    def __repr__(self):
        items = []
        for name in self.__fields__:
            value = getattr(self, name)
            items.append((name, value))
        body_items = ["%s=%s" % (name, repr(value)) for (name, value) in items]
        body = string.join(body_items, ", ")
        return "%s(%s)" % (self.__class__.__name__, body)

def autorepr(obj, fields, sort=False):
    data = ["%s=%r" % (name, getattr(obj, name)) for name in fields]
    if sort: data.sort()
    return string.join(data, ", ")

class RoutingResult(Struct):
    __fields__ = ["routing_method", "locator", "area_id", "mapinfo", "data"]

def sqlify(obj):
    """ Prepare an object for inclusion in an SQL statement.
        string, unicode -> 'foo'
        anything else -> foo
    """
    if isinstance(obj, str):
        return "'%s'" % (obj,)
    elif isinstance(obj, types.NoneType):
        return 'NULL'
    elif isinstance(obj, unicode):
        obj = obj.encode("utf-8")
        return "'%s'" % (obj,)
    else:
        return str(obj)

class PseudoMatch:
    """ A fake Match object for regular expressions. """
    def __init__(self, x):
        if not isinstance(x, tuple):
            x = (x,)
        self.data = x
    def groups(self):
        return self.data
    def group(self, n):
        return self.data[n-1]
    def __repr__(self):
        #return "<PseudoMatch%s>" % (repr(self.data),)
        return repr(self.data)

def split_emails(emailstring):
    """ Split a semicolon-delimited string containing emails.  Empty strings
        are discarded. """
    parts = emailstring.split(";")
    return [part for part in parts if part.strip()]

def cartesian(seq1, seq2):
    """ Generator returning the cartesian product of seq1 and seq2. """
    for x in seq1:
        for y in seq2:
            yield (x, y)

NOT_LINENOISE = string.letters + string.digits + " \t\n_<>/\"'"
# added <, > and / for XML tickets

def linenoiseratio(data, acceptable=NOT_LINENOISE):
    good = bad = 0
    for c in data:
        if c in acceptable:
            good += 1
        else:
            bad += 1

    return good, bad

def is_linenoise(data, cutoff=0.25):
    """ Determine if the given data can be considered line noise.  If the
        line noise ratio > the cutoff value, then this is true; otherwise,
        there are some other rules that may mark the data as noise. """
    good, bad = linenoiseratio(data)
    try:
        ratio = 1.0 * bad / good
    except:
        ratio = 0
    if ratio > cutoff:
        return True

    if len(data) < 60:
        if data.upper().find('NO CARRIER') > -1:
            return True
        # 2 or more lines with only a few characters? line noise
        if len(filter(string.strip, data.split())) >= 2:
            return True
        # repetition of characters
        if data.find("wwwww") > -1:
            return True

    # regular tickets, even messages and audits, have lots of words.  line
    # noise often does not.
    if '?xml' in data: return False
    words = data.split()
    lines = data.split("\n")
    word_to_line_ratio = 1.0 * len(words) / len(lines)
    if word_to_line_ratio <= 2.0:
        return True

    return False

def is_spam(data):
    """
    Typically a spammy email will have <HTML> tags or embedded html whilst
    a ticket will not. This is used to decide whether to email warnings about
    tickets that don't parse.
    """
    if data.upper().find('<HTML>') > -1:
        return True
    for urlregex in urlfinders:
        m = urlregex.search(data)
        if m:
            return True
    return False

def find_any(data, strings):
    """ Return true if any of the strings is found in <data>. """
    for s in strings:
        if data.find(s) > -1:
            return 1
    return 0

class SayMixin:
    # requires a .verbose attribute
    def say(self, *args):
        if self.verbose:
            for arg in args:
                print arg,
    def sayln(self, *args):
        if self.verbose:
            self.say(*args)
            print

def fmt(*args, **kwargs):
    """ Return a string consisting of the arguments, separated by spaces (by
        default.  A different separator can be specified using the 'sep'
        keyword argument.

        name = "Fred"
        fmt("hello", name) # "hello Fred"
        fmt("hello", "hello", sep="-") # "hello-hello"
    """
    sep = " "
    if kwargs.has_key('sep'):
        sep = kwargs['sep']
    return string.join(map(str, args), sep)

def unquote(s):
    """ Remove quotes from a string. """
    if s.startswith("'") and s.endswith("'") and len(s) >= 2:
        return s[1:-1]
    if s.startswith('"') and s.endswith('"') and len(s) >= 2:
        return s[1:-1]
    return s

def make_sql_list(lst):
    quoted = ["'%s'" % (x,) for x in lst]
    return "(" + string.join(quoted, ", ") + ")"

def safeint(x):
    """ Like int(), but doesn't choke on "30.0" or "30.0".  It takes digits
        from the string and stops at the first non-digit character. """
    try:
        return int(x)
    except:
        if isinstance(x, basestring):
            t = ""
            if x[:1] == "-":
                t = t + "-"
                x = x[1:]
            while x and x[0] in "0123456789":
                t = t + x[0]
                x = x[1:]
            return int(t or 0)
        else:
            raise

def safedelete(obj, key):
    try:
        del obj[key]
    except:
        pass

#
# "long" qtrmin conversion

def qtrminlong2decimal(qtrmin):
    """ Converts a "long" qtrmin value (e.g. "3225000964900" to a set
        (long, lat) in decimal format. """
    qtrmin = qtrmin.strip() # get rid of any whitespace
    lats, longs = qtrmin[:6], qtrmin[6:13]
    latcoords = map(int, (lats[:2], lats[2:4], lats[4:6]))
    longcoords = map(int, (longs[:3], longs[3:5], longs[5:6] + "0"))

    last = qtrmin[-1]
    if last == 'A':
        latcoords[2] += 15
        longcoords[2] += 15
    elif last == 'B':
        latcoords[2] += 15
    elif last == 'C':
        longcoords[2] += 15
    elif last == 'D':
        pass    # can effectively be ignored

    #print ">>", latcoords, longcoords, last
    lat = latcoords[0] + latcoords[1] / 60.0 + latcoords[2] / 3600.0
    longitude = longcoords[0] + longcoords[1] / 60.0 + longcoords[2] / 3600.0

    return (longitude, lat)

def time_string():
    t9 = time.localtime(time.time())
    return "%04d-%02d-%02d %02d:%02d:%02d" % t9[:6]

#
# interim function that will eventually be replaced when the object-relational
# mapping is complete.  for now, I will put it here.

def datasets_to_tickets(ticketrows, locaterows, assignmentrows):
    tix = []
    for ticketrow in ticketrows:
        mylocaterows = [r for r in locaterows
                        if r['ticket_id'] == ticketrow['ticket_id']]
        locate_ids = [r['locate_id'] for r in mylocaterows]
        myassignmentrows = [r for r in assignmentrows
                            if r['locate_id'] in locate_ids]
        z = datasets_to_ticket(ticketrow, mylocaterows, myassignmentrows)
        tix.append(z)

    return tix

def datasets_to_ticket(ticketrow, locaterows, assignmentrows):
    """ Return a dict {'ticket_id': ticket_id, 'ticket': ticket, 'locates':
        dict of locate rows keyed on locate id. """
    result = {}

    # create Ticket
    import ticket as ticketmod
    import sqlmap, assignment, locate
    t = ticketmod.Ticket()
    for name, value in ticketrow.items():
        setattr(t, name, value)
    t.image = (t.image or "").strip()

    # create locate dict
    locdict = {}
    for locrow in locaterows:
        locate_id = locrow['locate_id']
        loc = locate.Locate()
        if isinstance(loc, sqlmap.SQLMap):
            fields = [f[0] for f in loc.__fields__]
        else:
            fields = get_fields(loc)
        for attr in fields:
            # this includes the locate_id
            if locrow.has_key(attr):
                setattr(loc, attr, locrow[attr])

        # add assignments, if appropriate
        asgrows = [r for r in assignmentrows if r['locate_id'] == locate_id]
        # XXX there really should be some kind of warning if there are
        # multiple (active) assignments found here!
        for asgrow in asgrows[:1]:
            asg = assignment.Assignment()
            if isinstance(asg, sqlmap.SQLMap):
                fieldnames = [field for (field, sqltype) in asg.__fields__]
            else:
                fieldnames = get_fields(asg)
            for attr in fieldnames:
                if asgrow.has_key(attr):
                    setattr(asg, attr, asgrow[attr])
            if asg.locate_id:
                loc._assignment = asg

        locdict[locate_id] = locrow
        t.locates.append(loc)

    # prepare result
    result['ticket_id'] = ticketrow['ticket_id']
    result['ticket'] = t
    result['locates'] = locdict

    return result

class Transparent:
    """ Class whose __repr__ is the (string representation of) the object
        passed to it.  For example, repr(Transparent("foo")) == "foo", not
        '"foo"'. """
    def __init__(self, x):
        self.data = x
    def __repr__(self):
        return str(self.data)
    def __str__(self):
        return str(self.data)

def is_numeric(s):
    """ Return True if string s is non-empty and entirely numeric (i.e. only
        contains digits 0-9).  Return False otherwise. """
    try:
        int(s)
    except ValueError:
        return False
    else:
        return True

def contains_substrings(s, substrings):
    """ Return True if string <s> contains all the given substrings.
        (If the list of substrings is empty, True is returned as well.) """
    for substr in substrings:
        if s.find(substr) < 0:
            return False
    return True

def separate_by(data, s):
    """ Split <data> in lines, and separate them in groups starting with string
        <s>.  Return a list of parts.  <s> is still included in the result,
        unlike string.split. """
    lines = data.split("\n")
    collected = []
    current = []

    for line in lines:
        if isinstance(s, list) or isinstance(s, tuple):
            found = contains_substrings(line, s)
        else:
            found = line.find(s) >= 0

        if found:
            if current:
                collected.append(current)
            current = [line]
        else:
            current.append(line)

    if current:
        collected.append(current)

    collected = [string.join(lst, "\n") for lst in collected]

    return collected

def safeglob(pattern, minsize=1):
    files = glob.glob(pattern)
    if len(files) < minsize:
        raise ValueError("safeglob: Expected at least %d files, got %d" % (
            minsize, len(files)))
    else:
        return files


def with_attrs(**args):
    """ Decorator for easily adding attributes to functions and methods.
        Usage:

        @with_attrs(will_fail=True, bogus=1)
        def methodname(self, ...):
            ...

    """
    def wrapper(f):
        for name, attr in args.items():
            setattr(f, name, attr)
        return f
    return wrapper

def unique(lst):
    return list(set(lst))

def transpose(lst):
    return zip(*lst)

def re_get(regex, data):
    """ Simple way to get a single group from a regex. """
    m = re.search(regex, data)
    if m:
        return m.group(1)
    else:
        return ""

def fix_ampersands_in_xml(data):
    start = 0
    while True:
        idx = data.find('&', start)
        if idx > -1:
            s = data[idx:idx+10]
            if s.startswith('&amp;') or s.startswith('&gt;') \
            or s.startswith('&lt;') or s.startswith('&apos;') \
            or s.startswith('&quot;'):
                start = idx + 2
            else:
                data = data[:idx] + '&amp;' + data[idx+1:]
                start = idx + 2
        else:
            return data

def is_xml(data):
    """ Simple test to check if a piece of data is (likely) XML. """
    return data.lstrip().startswith("<?xml")

def could_be_base64(s):
    base_64_chars = string.lowercase + string.uppercase + string.digits \
                  + "+/=" + string.whitespace
    def is_base64_char(c):
        return c in base_64_chars
    return all(is_base64_char(c) for c in s)

# necessary because email.message_from_file() and friends don't return all the
# available headers, for some reason.
def parse_headers(data):
    """ Parse headers from a raw email message. Return a dictionary containing
        the headers.
        CAUTION: Overwrites duplicate headers.
    """
    headers = {}
    last_key = None
    for line in data:
        if not line:
            break
        # indented lines are a continuation of the previous one
        if line.startswith(" ") or line.startswith("\t"):
            value = headers.get(last_key, None)
            headers[last_key] = (value or '') + line
        else:
            key, _, value = line.partition(':')
            headers[key.strip()] = value.strip()
            last_key = key.strip()

    return headers

def safe_convert_base64(data):
    """ "Safe" base64 data converter, in the sense that it doesn't bring the
        whole system down if it fails. Rather, it returns None on
        failure and the converted data on success. """
    try:
        return data.decode('base64')
    except:
        return None

# IronPython does not have a datetime.strptime, so we provide a portable
# alternative.
def strptime(datestr, format):
    import datetime, time
    f = getattr(datetime.datetime, 'strptime', None)
    if f is not None:
        return f(datestr, format)
    return datetime(*(time.strptime(datestr, format)[0:6]))

re_filename_date = R("-(\d\d\d\d-\d\d-\d\d)-")
def extract_date_from_filename(filename):
    """ Given a filename like FOO-YYYY-MM-DD-XXXXX.txt (or similar), extract
        the date from it. If no date can be extracted, return None.
        NOTE: filename should probably not contain a path; if any directories
        happen to contain a date, those will be matched instead.
    """
    ds = re_get(re_filename_date, filename)
    return ds or None

re_att_fn = R('name="(.*?)"', re.DOTALL|re.MULTILINE)
re_att_fn_2 = R('name=(\S+)', re.DOTALL|re.MULTILINE)
def get_attachment_filename(blurb):
    """ Sometimes email.Message.get_filename() does not seem to work properly,
        at least not in Python 2.5. If it returns None even though a filename
        is specified, this function "tries harder" to extract it.
    """
    name = re_get(re_att_fn, blurb)
    if not name:
        name = re_get(re_att_fn_2, blurb)
    name = name.lstrip('"').rstrip('"') # we don't want quotes
    return name

def log_info(log, name):
    """ Log version info when starting a program. """
    py_version = sys.version.split()[0]
    log.log_event("%s started; version: %s; Python version %s; pid: %s" %
      (name, version.__version__, py_version, os.getpid()))

def attempt(log, *fns):
    """ Try all the actions given. Return the result of the first action that
        succeeds (i.e., does not raise an exception). Actions are represented
        as functions/lambdas with zero arguments.
        If a log object is specified, it will be used; None can be specified
        as well in case we can't, or don't want to log errors.
    """
    if not fns: return None
    for f in fns:
        try:
            result = f()
        except:
            if log:
                ep = errorhandling.errorpacket()
                log.log(ep, send=0, write=1, dump=1)
        else:
            return result
    raise #Exception("None of the attempted actions succeeded")

def contains_lowercase(s):
    """ Return True if the string contains lower-case letters. """
    for c in s:
        if c in string.lowercase:
            return True
    return False

def svnid_version(svnid):
    parts = svnid.split()
    if len(parts) > 2:
        return parts[2]
    else:
        return "N/A"

def get_hp_reasons(note):
    """ Extract a list of HP reasons from a HPR note. Returns a list of
        integers. """
    if not note.startswith("HPR-"): return []
    note = note[4:] # strip leading HPR-
    parts = note.split()
    refs = [int(s.strip()) for s in parts[0].split(",")]
    return refs

