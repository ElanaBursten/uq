# decorator.py
# Implementation of the Decorator pattern in Python.
# Basically, the Decorator class functions as a wrapper around an object, able
# to handle method calls and attribute lookups for that object, but
# manipulating these for its own purposes.
# Created: 2002.07.09 HN

class Decorator:
    def __init__(self, obj, method_decorator):
        self.obj = obj
        self.method_decorator = method_decorator
    def __getattr__(self, name):
        attr = getattr(self.obj, name)
        if callable(attr):
            method_decorator = self.method_decorator
            return method_decorator(self, attr)
        else:
            return attr

class MethodDecorator:
    """ Method wrapper, meant for subclassing. """
    def __init__(self, decorator, f):
        self.decorator = decorator
        self.f = f
    def __call__(self, *args, **kwargs):
        self.before(*args, **kwargs)
        result = self.f(*args, **kwargs)
        self.after(*args, **kwargs)
        return result
    def before(self, *args, **kwargs):
        pass
    def after(self, *args, **kwargs):
        pass


if __name__ == "__main__":

    class MyMethodDecorator(MethodDecorator):
        def before(self, *args, **kwargs):
            print "Ni!", args, kwargs
        def after(self, *args, **kwargs):
            print "Bye!"

    class Foo:
        x = 42
        def bar(self):
            print "bar!"
        def baz(self, x):
            print "baz", x

    foo = Foo()

    deco = Decorator(foo, MyMethodDecorator)
    deco.bar()
    deco.baz(3)
    print deco.x

