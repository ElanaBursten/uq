# program_status.py

import os
#
import datadir
import date

def _convert_mutex_name(mutexname):
    return mutexname.replace(":", "-")

# any exceptions in both functions are caught, so the caller doesn't halt if
# the status cannot be read or written for some reason.  upon success, the
# date/time is returned, otherwise None.

def write_status(mutexname):
    try:
        mutexname = _convert_mutex_name(mutexname)
        filename = os.path.join("status", "ps-" + mutexname + ".txt")
        f = datadir.datadir.open(filename, "w")
        d = date.Date()
        pid = os.getpid()
        print >> f, pid
        print >> f, d
        f.close()
    except:
        return None
    else:
        return pid, d.isodate()

def read_status(mutexname):
    try:
        mutexname = _convert_mutex_name(mutexname)
        filename = os.path.join("status", "ps-" + mutexname + ".txt")
        f = datadir.datadir.open(filename, "r")
        pid = int(f.readline().strip())
        thedate = f.readline().strip()
        f.close()
        return pid, thedate
    except:
        return None

