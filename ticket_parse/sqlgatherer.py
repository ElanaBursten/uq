# sqlgatherer.py

import string

class SQLGathererError(Exception):
    pass

class SQLGatherer:

    def __init__(self, tdb, debug=0):
        self.tdb = tdb
        self.debug = debug
        self.reset()

    def reset(self):
        self.collected = []
        self.vars = {}
        self.values = []
        # a list of (value, sqltype) tuples that must match the parameters
        # present in the SQL.

    def add_sql(self, sql, values=()):
        """ Add SQL to the list.  If it contains parameters, then the values
            of those must be specified as well. """
        sql = sql.strip()
        if not sql.endswith(";"):
            sql = sql + ";"
        self.collected.append(sql)
        self.values.extend(values)
        # XXX is there a way to check if the number of values matches the
        # number of parameters?

    def declare_var(self, name, type='int'):
        if not name.startswith('@'):
            name = '@' + name
        if self.vars.has_key(name):
            raise SQLGathererError, "Variable already declared: %s" % (name,)
        self.vars[name] = type
        sql = "declare %s %s" % (name, type)
        self.collected.append(sql)

    def return_vars(self, names=()):
        """ Produce a SELECT statement that returns all variables in <names>.
            If <names> is empty, use all variables that have been declared. """
        if not names:
            names = self.vars.keys()
            names.sort()
        foo = ["%s as %s" % (name, name[1:]) for name in names]
        if foo:
            bar = string.join(foo, ", ")
            sql = "select %s" % (bar,)
            self.collected.append(sql)

    def select_scope_identity(self, name):
        if not name.startswith('@'):
            name = '@' + name
        if not self.vars.has_key(name):
            raise SQLGathererError, "Undeclared variable: %s" % (name,)
        sql = "select %s = scope_identity()" % (name,)
        self.collected.append(sql)

    # XXX use transaction!
    # right now, insertticket + friends use a transaction already...
    def execute(self, reset=1):
        # glue all SQL statements together and execute them.
        sql = self.gen_sql()
        if not sql:
            raise SQLGathererError, "No SQL to be executed"
        if self.debug:
            print sql, self.values
        try:
            rows = self.tdb.runsql_with_parameters(sql, self.values)
        finally:
            if reset:
                self.reset() # don't reuse this SQL
        return rows

    def gen_sql(self):
        """ Generate one big SQL statement from all the SQL we've collected
            so far. """
        return string.join(self.collected, "\n")

