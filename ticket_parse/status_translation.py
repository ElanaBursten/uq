# status_translation.py

import sqlmap
import static_tables

class StatusTranslation(sqlmap.SQLMap):
    __table__ = "status_translation"
    __key__ = "status_translation_id"
    __fields__ = static_tables.status_translation

    @classmethod
    def read_all(cls, db):
        return db.getrecords(cls.__table__, active=1)

