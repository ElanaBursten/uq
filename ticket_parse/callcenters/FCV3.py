# FCV3.py

import businesslogic
import FCV1
import date
import tools

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "339" # same as FCV2
    call_center = "FCV3"

    # steal methods from FCV1
    #
    FCV1_legal_due_date = FCV1.BusinessLogic.legal_due_date.im_func
    _legal_due_date = FCV1.BusinessLogic._legal_due_date.im_func

    def legal_due_date(self, ticketrow):
        if ticketrow['ticket_type'].find('MEET') != -1:
            # Meeting ticket. Set the legal due date to the meeting time.
            # For meet tickets, the meeting time has been set to the work date
            # for these particular call centers
            meet_date = ticketrow['work_date']
            d = date.Date(meet_date)
            return d.isodate()
        else:
            return self.FCV1_legal_due_date(ticketrow)

    def locator_methods(self, row):
        return [
            #self.route_special,
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic_with_dummy_city,
        ]

    def isrenotification(self, ticket):
        return 1    # always!

    def is_ticket_ack(self, row):
        return businesslogic.BusinessLogic.is_ticket_ack(self, row) \
            or row['explosives'].strip().upper() == 'Y'

    def allow_summary_updates(self):
        return 1

registry = {}

