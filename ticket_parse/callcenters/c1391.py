# c1391.py

import string
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape
#
import businesslogic
import date
import duedates
import et_tools
import xmlhttpresponder_data as xhrd

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = "1391"
    DEFAULT_LOCATOR = "6847"

    '''
    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = self.dcatalog.inc_n_days(cd, 3) # 72 hours
        return cd.isodate()
    '''
    legal_due_date = duedates.due_date_TN

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_grid_county_state_1391,
            self.lcatalog.munic,
        ]

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):
    """
    XmlNode:
    RespondXml() returns acks of the responses as an XmlNode.
    The acks indicate the status of each of the responses passed to it.
    The XmlNode will have a root node <AckCollection> which will contain <Ack>
    elements for each response.
    Each <Ack> will consist of: <TicketNum>, <DispatchCode>, <FacilityType>,
    <AckCode>, and <AckCodeDescription>.
    Below is are a couple of samples that correspond to the sample responses
    listed above.

    XML Node with 1 Ack:
    <AckCollection>
    <Ack>
       <TicketNum>012345678</TicketNum>
       <DispatchCode>ABC01</DispatchCode>
       <FacilityType>W</FacilityType>
       <AckCode>0</AckCode>
       <AckDescription>Success</ AckDescription>
    </Ack>
    </AckCollection>
    """
    acceptable_codes = ["success",
                        "ticket does not exist",
                        "dispatch code does not exist",
                        "invalid response code",
                        "dispatch code does not appear on ticket",
                        "dispatch code not authenticated",
                        "unknown facility type",
                        "system error - please resend",
                        "250 ok"]
    codes_ok = ["success", "250 ok"]
    codes_reprocess = ["system error - please resend"]
    codes_error = ["ticket does not exist",
                   "dispatch code does not exist",
                   "invalid response code",
                   "dispatch code does not appear on ticket",
                   "dispatch code not authenticated",
                   "unknown facility type"]

    ONE_CALL_CENTER = 'TNOCS'
    XMLNS = "http://tnresponse.korterraweb.com/"

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
    <RespondXml xmlns="%(xmlns)s">
    <xmlDoc>
        <ResponseCollection LoginId='%(member)s' Password='%(id)s'>
            <Response>
                <KeyInfo>012345678</KeyInfo>
                <TicketNum>%(ticket_number)s</TicketNum>
                <DispatchCode>%(membercode)s</DispatchCode>
                <ResponseCode>%(responsecode)s</ResponseCode>
                <FacilityType>%(fac_type)s</FacilityType>
                <LocateDate>%(completion_date)s</LocateDate>
                <LocateTime>%(completion_time)s</LocateTime>
            </Response>
        </ResponseCollection>
    </xmlDoc>
    </RespondXml>
    </soap:Body>
</soap:Envelope>"""

    fac_types = {"watr": "W",
                 "elec": "E",
                 "catv": "C",
                 "sgnl": "T", # What is this? Traffic signal?
                 "fuel": "G",
                 "phon": "P",
                 "ugas": "G",
                 "pipl": "O", # Would gas be better?
                 "sewr": "S"}

    def get_xml(self, names):
        """ Build the xml for this responder """
        # Get the utility type from the client
        sql = """
         select utility_type
         from client
         where oc_code = '%s'
        """ % self.row["client_code"]
        results = self.tdb.runsql_result(sql)
        if results:
            if self.fac_types.has_key(results[0]["utility_type"]):
                fac_type = self.fac_types[results[0]["utility_type"]]
            else:
                fac_type = "O"
        else:
            fac_type = "O"

        # Since the call center is in a later time zone, decrement the hour by 1
        # Use the closed_date, if available (should be)
        date_time = date.Date(self.row.get('closed_date',date.Date().isodate()))
        date_time.dec_time(hours=1)
        date_time = date_time.isodate()
        names.update({
            'member': escape(self.initials),
            'id': escape(self.id),
            'completion_date': date_time[:10],
            'completion_time': date_time[11:],
            'ticket_number': self.row["ticket_number"],
            'fac_type': fac_type,
            'xmlns': self.XMLNS,
        })
        self.log.log_event("Current date/time injected into xml (adjusted for time zone): %s" % date_time)

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

    def process_response(self, reply):
        """
        Should be of the form:
        <AckCollection>
        <Ack>
           <KeyInfo>012345678; ABC01</KeyInfo>
           <TicketNum>012345678</TicketNum>
           <DispatchCode>ABC01</DispatchCode>
           <FacilityType>W</FacilityType>
           <AckCode>0</AckCode>
           <AckCodeDescription>Success</AckCodeDescription>
        </Ack>
        </AckCollection>
        """
        try:
            doc = ET.fromstring(reply.xml)
            reply_code = int(et_tools.find(doc, 'AckCode').text)
            reply_text = et_tools.find(doc, 'AckCodeDescription').text
            return XMLHTTPResponderData.acceptable_codes[reply_code]
        except:
            #import traceback; traceback.print_exc()
            return reply.Text

registry = {
    ('1391', 'xmlhttp', ''): XMLHTTPResponderData,
}

