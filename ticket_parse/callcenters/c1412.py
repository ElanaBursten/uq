# c1412.py
# (used to be: FNL2)

import businesslogic
import FBL1
import tools

class BusinessLogic(FBL1.BusinessLogic):

    DEFAULT_LOCATOR = "2396"
    call_center = "1412"

class FTPResponderData(FBL1.FTPResponderData):
    pass

registry = {
    ('1412', 'ftp', ''): FTPResponderData,
}

