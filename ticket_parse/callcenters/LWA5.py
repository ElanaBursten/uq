# LWA5.py

import LWA1
import _LocIncResponder as lir

class BusinessLogic(LWA1.BusinessLogic):
    call_center = 'LWA5'

class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'UULC'

registry = {
    ('LWA5', 'xmlhttp', ''): XMLHTTPResponderData,
}



