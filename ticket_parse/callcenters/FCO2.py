# FCO2.py

import update_call_centers as ucc
import FCO1

class BusinessLogic(FCO1.BusinessLogic):
    call_center = 'FCO2'

class UpdateData(ucc.UpdateData):
    fields = ["ticket_type"]

registry = {}

