# FDX1.py

import businesslogic
import date
import locate
import tools
import ticket_adapter
import SCA1
import string
from xml.sax.saxutils import escape

class BusinessLogic(businesslogic.BusinessLogic):
    DEFAULT_LOCATOR = "2411"
    call_center = "FDX1"

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_special,
            self.route_by_routinglist,
            self.route_map_area_with_code,
            self.lcatalog.munic,
        ]

    def route_special(self, row):
        return tools.RoutingResult()

    def route_map_area_with_code(self, row):
        # map name (e.g. MAPSCO) is ignored; instead, we use map 'FHL2'
        return self.lcatalog.map_grid_county_state_TX(row, code='FDX1')

class TicketAdapter(ticket_adapter.TicketAdapter):

    # if these locates are present, then we also add <locate_name> + "G"
    G_LOCATES = ["CP3", "CS3", "CR1", "DL1", "IR1", "LGD", "MQ3", "PL1", "GP7",
                 "WX3"]

    def adapt(self, ticket):
        new_locates = []
        loc_names = [x.client_code for x in ticket.locates]
        for loc in ticket.locates:
            if loc.client_code in self.G_LOCATES \
            and (loc.client_code + "G") not in loc_names:
                new_locate = locate.Locate(loc.client_code + "G")
                new_locates.append(new_locate)

        ticket.locates.extend(new_locates)

class XMLHTTPResponderData(SCA1.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'TESS'

    def get_xml(self, names):
        notes = self.get_ticket_notes()
        date_time = self.row['first_arrival_date']
        if date_time is None:
            date_time = self.row['closed_date']
            self.log.log_event("closed date injected into xml: %s"%(date_time))
        else:
            self.log.log_event("first_arrival_date injected into xml: %s"%(date_time))
        names.update({
            'id': escape(self.id),
            'locdatetime': date_time.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes':self.check_notes(notes)
        })
        revision = names['revision']
        names['revision'] = escape(string.join([c for c in revision if c in string.digits], ''))

        return self.XML_TEMPLATE % names

registry = {
    ('FDX1', 'xmlhttp', ''): XMLHTTPResponderData,
}

