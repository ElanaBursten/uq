# NewJersey.py

from xml.sax.saxutils import escape
import xml.etree.ElementTree as ET
import string
import StringIO
import time
#
import xmlhttpresponder_data as xhrd
import businesslogic
import date
import errorhandling2
import tools
import emailresponder_data as erd
import ftpresponder_data as frd
import FJL1

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "265"
    AUTO_ROUTING = "285"
    call_center = "NewJersey"

    def do_no_client_check(self, client_code=""):
        return (client_code != "XXX")   # no check for this client

    def legal_due_date(self, ticketrow):
        # Mantis #2688: fix due date computation; must handle emergencies and
        # use call date (rather than transmit date)
        td = date.Date(ticketrow["call_date"])

        # emergency tickets: +2 hrs
        if self.isemergency(ticketrow):
            dd = self.dcatalog.inc_emergency(td, hours=2)
            return dd.isodate()

        td = date.Date(self.base_date(td.isodate()))
        after_11_50_pm = (td.hours >= 23) and (td.minutes >= 50)
        # if this isn't a business day, move to the earliest next one; only
        # do this for tickets that came in before 11:50PM
        if after_11_50_pm:
            d1 = td
        else:
            d1 = self.dcatalog.inc_to_business_day(td)

        # +3 business days
        # if ticket came in after 11:50 PM, add another day
        d2 = self.dcatalog.inc_n_days(d1, 3 + after_11_50_pm)
        # set time to 23:59:59
        d2.hours, d2.minutes, d2.seconds = 23, 59, 59
        return d2.isodate()

    def locator_methods(self, row):
        return [
            # todo(dan) review the order
            self.lcatalog.map_qtrmin,
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1
        
    def geocode_with_cross_streets(self, ticket):
        return (ticket.work_address_number == '0' or \
         not ticket.work_address_number) and ticket.work_cross

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    # note: we send the "response code" and the "explanation code" as
    # one string.

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
   <TicketResponseStatus xmlns="http://translore.com/ResponseService/">
     <AuthID xmlns="">%(id)s</AuthID>
     <CallCenter xmlns="">NJ1C</CallCenter>
     <TicketNumber xmlns="">%(ticket)s</TicketNumber>
     <TicketRevision xmlns="">0</TicketRevision>
     <UtilityCode xmlns="">%(membercode)s</UtilityCode>
     <Response xmlns="">%(responsecode)s%(explanation)s</Response>
     <Who xmlns="">UTI</Who>
     <Notes xmlns="">Response by Utiliquest</Notes>
     <WorkTime xmlns="">%(locdatetime)s</WorkTime>
   </TicketResponseStatus>
 </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T")
        })
        self.log.log_event("Location date/time injected into xml: %s"%(closed_date,))
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

class EmailResponderData(erd.EmailResponderData):

    SEPARATOR = ','
    LINE_FIELDS = [
        'ticket_number',
        'client_code',
        'disp_code',
        'due_date',
        'closed_date',
    ]

    def response_is_success(self, row, resp):
        # we don't get an acknowledgement, so we consider this to be always true
        return True

    def make_email_line(self, names, tdb, version):
        self.log.log_event("Closed date/time sent: %s" % names.get('closed_date', 'N/A'))
        parts = [names.get(fieldname, "N/A") for fieldname in self.LINE_FIELDS]
        # add extra newline to separate blocks of data
        return string.join(parts, self.SEPARATOR) + "\n"


class FTPResponderData(FJL1.FTPResponderData):
    """ First Energy Korterra FTP responder for NewJersey2. See QMAN-2536. """

    # Notes:
    # Currently assumes that the <occid> is NJOC.
    # <reasonid> is left empty.
    # According to the Mantis item, we use the same rules as for the 1421
    # responder, so I assume we use the same mappings as well.
    # XXX Not sure where isclearexcavate1=2 comes into play...

    # based on the status, we set <ismarked>, <isexcavated> and possibly
    # <actcode>.
    MARK_SETTINGS = {
        'CLEAR':  (0, 1, ''),
        'MARKED': (1, 0, ''),
    }

    TEMPLATE2 = """\
<LocateCollection>
    <Locate type="CUSTOM2" membercode="%(company)s"
            completionid="%(locate_id)s;%(resp_id)s">
        <customerid>SYSTEM</customerid>
        <occid>NJOC</occid>
        <Jobid>%(ticket_number)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <reasonid>%(status)s</reasonid>
        <faccode1>ELECTRIC</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <Remarks>%(remarks)s</Remarks>
        %(completion)s
    </Locate>

</LocateCollection>
    """

    def extra_names(self, namespace):
        extra = FJL1.FTPResponderData.extra_names(self, namespace)

        extra['completion'] = ''
        if namespace['raw']['ticket_format'] == 'NewJersey':
            locate_id = namespace['locate_id']
            sql = """
              select s.complete
              from locate l
              join statuslist s on l.status = s.status
              where l.locate_id = %d
            """ % (int(locate_id))
            rows = self.tdb.runsql_result(sql)

            if rows and int(rows[0]['complete'] or 0) == 0:
                extra['completion'] = '<completionlevel>T</completionlevel>'

        return extra

# Mantis #2882.
# Runs side by side with NewJersey.FTPResponderData.
class NewFTPResponderData(frd.FTPResponderData):

    # Note: Responder only accepts status codes '1' (clear) and '2' (marked),
    # so the appropriate mappings must be added in config.xml.

    store_resp_context = True
    # stores resp. context, because the response format does not allow us to
    # pass a locate_id or resp_id.

    LEN_TICKET_NUMBER = 9

    TEMPLATE1 = """\
<?xml version="1.0" encoding="utf-8"?>
<Responses>
###BODY###
</Responses>"""

    TEMPLATE2 = """\
  <Response>
    <StateCode>NJ</StateCode>
    <TicketNumber>%(ticket_number)s</TicketNumber>
    <DistrictCode>%(company)s</DistrictCode>
    <StatusCode>%(status)s</StatusCode>
  </Response>
    """

    def gen_filename(self):
        """ Generate a unique filename to be posted on the server. """
        t = time.time()
        t9 = time.localtime(t)
        year, month, day, hours, minutes, seconds = t9[:6]
        milliseconds = int((t - int(t)) * 1000)
        name = "%04d%02d%02d%02d%02d%02d%03d.xml" % (year, month, day, hours,
               minutes, seconds, milliseconds)
        return name

    def is_response_file(self, filename):
        """ Ack filenames must end in .rxml. """
        return filename.endswith(".rxml")

    def get_acknowledgements(self, data, locates):
        ok, errors = [], []
        xml = ET.fromstring(data)
        for r in xml.findall('Result'):
            try:
                a = frd.FTPResponderAck()
                a.result = r.find('ResultCode').text
                a.raw = r.find('ResultMessage').text
                ticket_number = r.find('TicketNumber').text
                term_id = r.find('DistrictCode').text
                z = [loc for loc in locates
                     if loc['ticket_number'] == ticket_number
                     and loc['client_code'] == term_id][0]
                a.locate_id = z['locate_id']
                a.resp_id = z['resp_id']
                ok.append(a)
            except IndexError:
                ep = errorhandling2.errorpacket()
                ep.add(info="Term id not found in response_ack_context: %s/%s" %
                 (ticket_number, term_id))
                errors.append(ep)
            except:
                ep = errorhandling2.errorpacket()
                errors.append(ep)

        return ok, errors

    @staticmethod
    def is_success(result_code):
        return result_code == '0'

# QMAN-3421
class RocklandFTPResponderData(frd.FTPResponderData):
    # todo(dan) All of our response lines end with LF, not CRLF. This includes
    # LF's embedded in notes. Is this ok? Could it cause a problem with max
    # length of comments, if LF's get changed somehow to CRLF's?
    TEMPLATE2 = """\
Start::
Ticket:: %(ticket_number)s
State:: %(state)s
Member:: %(company)s
Code:: %(status)s
Method:: %(method)s
Date:: %(completion_datetime)s
User:: %(user)s
Entire Street:: 0
Transmission:: 0
Cast Iron:: 0
Public Inspection:: %(public_improvement_status)s
Comments:: %(notes)s
End::
"""

    LEN_TICKET_NUMBER = 0 # don't adapt ticket_number
    MARKED_STATUSES = ['MRC', 'MRE', 'MRG']
    USER = 'ORUUNJ'
    CALL_CENTER = 'NewJersey'
    
    def extra_data(self, namespace):
        def formatted_raw_notes(raw_notes):
            COMMENT_TERMINATOR = 'End::'
            COMMENT_TERMINATOR_REPLACEMENT = 'End_::'
            MAX_COMMENT_LENGTH = 512
            
            notes = string.join(raw_notes, '\n')
            # todo(dan) need to case-desensitize?
            notes = notes.replace(COMMENT_TERMINATOR,
             COMMENT_TERMINATOR_REPLACEMENT) 
            if len(notes) > MAX_COMMENT_LENGTH:
                notes = notes[:MAX_COMMENT_LENGTH - 9] + '(More...)'            
            return notes

        extra = frd.FTPResponderData.extra_data(self, namespace)
        raw = namespace['raw']
        extra['state'] = raw['work_state']

        if namespace['status'] in self.MARKED_STATUSES:
          extra['method'] = '3'
        else:
          extra['method'] = '0'
        
        if raw['closed_date']:
            extra['completion_datetime'] = raw['closed_date']
        else:
            extra['completion_datetime'] = date.Date().isodate()
        extra['completion_datetime'] = \
         extra['completion_datetime'].replace('-', '/')
             
        extra['user'] = self.USER

        # todo(dan) Review exception handling
        extra['notes'] = formatted_raw_notes(self.get_raw_locate_notes(
         raw['ticket_id'], raw['locate_id']))
        
        extra['public_improvement_status'] = '0'

        return extra

    def gen_filename(self):
        """ Generate a unique filename to be posted on the server. """
        t = time.time()
        t9 = time.localtime(t)
        year, month, day, hours, minutes, seconds = t9[:6]
        milliseconds = int((t - int(t)) * 1000)
        
        prefix = self.CALL_CENTER + '-'
        # todo(dan) Is login necessary?
        if self.login:
            prefix += (self.login + "-")
            
        name = "%s%04d%02d%02d%02d%02d%02d%03d.txt" % (prefix, year, month,
               day, hours, minutes, seconds, milliseconds)
        return name

    # Result files have the same name as the files we originally sent, except
    # the extension is .rct.
    def is_response_file(self, filename):
        """ Ack filenames must end in .rct """
        return filename.lower().startswith(self.CALL_CENTER.lower() + '-') and filename.lower().endswith('.rct')        
        
    def get_acknowledgements(self, data, locates):
        def parse_acknowledgement_data():
            # todo(dan) need to case-desensitize?
            lines = StringIO.StringIO(data)
            ack = None
            while True:
                line = lines.readline().lstrip()
                if not line:
                    break
                elif line.startswith('Start::'):
                    ack = frd.FTPResponderAck()
                    ack.ticket_number = ''
                    ack.term_id = ''
                elif line.startswith('Ticket::'):
                    ack.ticket_number = line[8:].strip()
                elif line.startswith('Member::'):
                    ack.term_id = line[8:].strip()
                elif line.startswith('Result::'):
                    ack.result = line[8:].strip()
                    ack.raw = ack.result
                elif line.startswith('End::'):
                    if ack:
                        acks.append(ack)
                    ack = None
                
        ok, errors, acks = [], [], []
        parse_acknowledgement_data() 
        for ack in acks:
            try:
                # todo(dan) Include individual ack, or complete ack file text, 
                # in error messages?
                if not (ack.ticket_number and ack.term_id):
                    msg = "Acknowledgment missing ticket number and/or term" +\
                     " id. ticket number: '%s', term id: '%s'"
                    raise ValueError(msg % (ack.ticket_number, ack.term_id))
            
                locate_matches = [loc for loc in locates
                     if loc['ticket_number'] == ack.ticket_number
                     and self.translations.get(loc['client_code'],
                      loc['client_code']) == ack.term_id]
                try:
                    locate = locate_matches[0]
                except IndexError:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Term id not found in response_ack_context: %s/%s" %
                     (ack.ticket_number, ack.term_id))
                    errors.append(ep)
                else:                    
                    ack.locate_id = locate['locate_id']
                    ack.resp_id = locate['resp_id']
                    
                    if self.is_success(ack.result):
                        ok.append(ack)
                    else:
                        ep = errorhandling2.ErrorPacket()
                        msg = "Response acknowledged, but result indicates " + \
                         "it failed: %s/%s"
                        ep.add(info=msg % (ack.ticket_number, ack.term_id))
                        errors.append(('acknowledged, not successful', ep, ack))
            except:
                ep = errorhandling2.errorpacket()
                errors.append(ep)

        return ok, errors
        
    @staticmethod
    def is_success(result_code):
        return result_code == 'Success'
        
registry = {
    ('NewJersey', 'xmlhttp', ''): XMLHTTPResponderData,
    ('NewJersey', 'email', ''): EmailResponderData,
    ('NewJersey', 'ftp', ''): FTPResponderData,
    ('NewJersey', 'ftp', 'njoc'): NewFTPResponderData, # Mantis #2882
    ('NewJersey', 'ftp', 'rock'): RocklandFTPResponderData, # QMAN-3421
}

