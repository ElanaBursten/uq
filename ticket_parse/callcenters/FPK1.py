# FPK1.py

import FCV3

class BusinessLogic(FCV3.BusinessLogic):
    call_center = "FPK1"
    DEFAULT_LOCATOR = "2658"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        return 1

registry = {}

