# FNV2.py

import FNV2
import date

class BusinessLogic(FNV2.BusinessLogic):

    DEFAULT_LOCATOR = FNV2.BusinessLogic.DEFAULT_LOCATOR
    call_center = "1394"

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

registry = {}

