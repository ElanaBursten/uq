# FMW2.py

import update_call_centers as ucc
import FMW1

class BusinessLogic(FMW1.BusinessLogic):
    call_center = "FMW2"

class UpdateData(ucc.UpdateData):
    # when updating an FMW2 ticket, only store these fields:
    fields = [
        "service_area_code", "serial_number", "alert",
    ]

registry = {}

