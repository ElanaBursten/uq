# FGV2.py

import businesslogic
import date
import FGV1

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'FGV2'
    DEFAULT_LOCATOR = FGV1.BusinessLogic.DEFAULT_LOCATOR

    #
    # due date methods are the same as FCL1

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        d = date.Date(ticketrow["transmit_date"])
        due_date = self._due_date(d, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 2 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)
        return d

    #
    # locator methods are the same as FGV

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        return 1

registry = {}

