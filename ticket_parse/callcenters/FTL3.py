# FTL3.py

import businesslogic
import FCL1
import c1391
import date

class BusinessLogic(FCL1.BusinessLogic):
    call_center = "FTL3"
    DEFAULT_LOCATOR = "2303"    # assume the same as FTL

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        d = date.Date(ticketrow["transmit_date"])
        due_date = self._due_date(d, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 2 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('FTL3', 'xmlhttp', ''): XMLHTTPResponderData,
}

