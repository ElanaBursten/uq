# FNV1.py

import businesslogic
import FTL1
import c1391

class BusinessLogic(FTL1.BusinessLogic):
    DEFAULT_LOCATOR = "2234"
    call_center = "FNV1"

    # locator_methods are the same as FTL1

    def allow_summary_updates(self):
        return 1

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

class FTPResponderData(FTL1.FTPResponderData):
    pass

registry = {
    ('FNV1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('FNV1', 'ftp', ''): FTPResponderData,
}

