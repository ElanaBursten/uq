# LOR6.py

import LOR1
import _LocIncResponder as lir

class BusinessLogic(LOR1.BusinessLogic):
    call_center = 'LOR6'

class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    pass
    #ONE_CALL_CENTER = 'WOC'
    # NOTE: LOR6 updates LOR1, so LOR1's ONE_CALL_CENTER is used if the term id
    # is not found in the _LocIncResponder dictionary

registry = {
    ('LOR6', 'xmlhttp', ''): XMLHTTPResponderData,
}


