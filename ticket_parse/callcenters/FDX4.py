# FDX4.py

import FDX1
import SCA1

class BusinessLogic(FDX1.BusinessLogic):
    call_center = 'FDX4'

class XMLHTTPResponderData(SCA1.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'TESS'

registry = {
    ('FDX4', 'xmlhttp', ''): XMLHTTPResponderData,
}

