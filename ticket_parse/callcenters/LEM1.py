# LEM1.py

import LOR1

class BusinessLogic(LOR1.BusinessLogic):
    """
    we assume that the rules are the same as for LOR1... note that this will
    only affect tickets that are actually stored as LEM1.
    """
    call_center = 'LEM1'

    def legal_due_date(self, row):
        return row['work_date']

registry = {}

