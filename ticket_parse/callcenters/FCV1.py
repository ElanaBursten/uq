import businesslogic
import date
import tools

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "339"
    call_center = "FCV1"

    # new due_date routines, taking effect 2002.07.01
    # same as FCV3 (refactoring possibility here)

    def legal_due_date(self, ticketrow):
        emergency = ticketrow['kind'] == 'EMERGENCY'
        transmit_date = ticketrow["call_date"]
        return self._legal_due_date(transmit_date, emergency=emergency).isodate()

    def _legal_due_date(self, call_date, emergency=0):
        d = date.Date(call_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # set to 7:00 of the next morning
            d.inc()
            d.hours, d.minutes, d.seconds = 7, 0, 0
            # make sure this doesn't fall in a weekend
            while not self.isbusinessday(d):
                d.inc()
            # fast-forward 48 hours
            d = self.dcatalog.inc_n_days(d, 2)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_area_with_code,
            self.lcatalog.munic,
        ]

    def route_map_area_with_code(self, row):
        code = None
        if row.get("work_county") == "PORTSMOUTH":
            code = "CHES"
        # this returns a RoutingResult, should be OK
        return self.lcatalog.map_area_with_code(row, code)

    def isrenotification(self, ticket):
        return 1    # always!

    def is_no_show(self, ticket):
        return businesslogic.BusinessLogic.is_no_show(ticket) \
         or ticket["ticket_type"].find("LATE") > -1

registry = {}

