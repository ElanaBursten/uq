# FDE2.py

import FDE1
import date

class BusinessLogic(FDE1.BusinessLogic):
    call_center = 'FDE2'

    # steal methods from FDE1
    #
    FDE1_legal_due_date = FDE1.BusinessLogic.legal_due_date.im_func
    _legal_due_date = FDE1.BusinessLogic._legal_due_date.im_func

    def legal_due_date(self, ticketrow):
        dd = ticketrow.get('due_date', None)
        if dd:
            return dd
        if ticketrow['ticket_type'].find('MEET') != -1:
            # Meeting ticket. Set the legal due date to the meeting time.
            # For meet tickets, the meeting time has been set to the work date
            # for these particular call centers
            meet_date = ticketrow['work_date']
            d = date.Date(meet_date)
            return d.isodate()
        else:
            return self.FDE1_legal_due_date(ticketrow)

    def is_ticket_ack(self, row):
        return FDE1.BusinessLogic.is_ticket_ack(self, row) \
            or row['explosives'].strip().upper() == 'Y'

    def allow_summary_updates(self):
        return 1

registry = {}

