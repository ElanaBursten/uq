# FOR1.py

import businesslogic

class BusinessLogic(businesslogic.BusinessLogic):
    DEFAULT_LOCATOR = "4834"
    call_center = "FOR1"

    def legal_due_date(self, row):
        return row["work_date"]

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

registry = {}

