# FCL1.py

from xml.sax.saxutils import escape
import string
import businesslogic
import date
import _c1421

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "630"
    call_center = "FCL1"

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        td = date.Date(ticketrow["transmit_date"])
        cd = date.Date(ticketrow["call_date"])
        due_date = self._due_date(td, cd, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, call_date, emergency=0):
        if emergency:
            dd = date.Date(call_date)
            dd = self.dcatalog.inc_n_hours(dd, 2)
        else:
            # +2 days, set to midnight
            if 0: # OLD WAY, changed -- Mantis #2770
                # add 2 days, but ignore weekends
                dd = date.Date(call_date)
                dd = date.Date(self.base_date(dd.isodate()))
                dd = self.dcatalog.inc_n_days_to_time(dd, 2, 0, minutes=1)
                '''
                After the recent change for the due dates (mantis 2071),
                the users no longer have the visual que of the "yellow" tickets.
                We need to adjust the due date so that it is due 2 minutes
                earlier at 11:59 PM preceeding the actual due date time. For
                example, instead of the due date/time = 12:01 on Tues, it
                should be 11:59 on Monday.
                That way the techs know which tickets will become late the next
                morning using the visual colors already in the system.
                '''
                dd.dec_time(hours=0, minutes=2, seconds=0)
            else:
                # Mantis #2770: unlike most other call centers, if a ticket
                # comes in over the weekend or on a holiday, we don't consider
                # it to have come in on the next business day; rather, we use
                # the PREVIOUS business day as the base date. In other words,
                # a ticket that comes in on Saturday, will be treated like it
                # came in on Friday.
                dd = date.Date(self.base_date_rev(call_date))
                dd = self.dcatalog.inc_n_days(dd, 2)
                dd = self.dcatalog.set_midnight(dd)

        return dd

    def locator_methods(self, row):
        return [
            self.lcatalog.map_qtrmin,
            #self.lcatalog.grid_area,   # now obsolete
            self.route_by_routinglist, # includes street routing
            self.lcatalog.munic,
        ]

    def isrenotification(self, ticket):
        return ticket["ticket_type"].find("RXMT") > -1

    def is_ticket_ack(self, ticket):
        """ For FCL1, we also add renotifications to ticket_ack. """
        #return self.isemergency(ticket) \
        #       or self.isrenotification(ticket) \
        #       or ticket["ticket_type"].find("2NDR") > -1
        return businesslogic.BusinessLogic.is_ticket_ack(self, ticket) \
            or self.isrenotification(ticket)

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1

class XMLHTTPResponderData(_c1421.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'NCOC'
    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
        <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>UTI</Locator_Name>
        <Note>%(notes)s</Note>
        </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        rec = self.tdb.getrecords('client', oc_code = self.row['client_code'])[0]
        fac_type = rec['utility_type']
        notes = self.get_ticket_notes()
        date_time = self.row.get('first_arrival_date',None)
        if date_time is None:
            date_time = self.row['closed_date']
            self.log.log_event("closed date injected into xml: %s"%(date_time))
        else:
            self.log.log_event("first_arrival_date injected into xml: %s"%(date_time))
        names.update({
            'id': escape(self.id),
            'locdatetime': date_time.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes':self.check_notes(notes),
        })
        revision = names['revision']
        names['revision'] = escape(string.join([c for c in revision if c in string.digits], ''))

        return self.XML_TEMPLATE % names

registry = {
    ('FCL1', 'xmlhttp', ''): XMLHTTPResponderData,
}

