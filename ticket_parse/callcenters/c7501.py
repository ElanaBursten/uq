# c7501.py

import businesslogic
import date
import xmlhttpresponder_data as xhrd
from xml.sax.saxutils import escape

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = "7501"
    DEFAULT_LOCATOR = '7014'

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        if emergency:
            due_date = date.Date(date.Date(ticketrow["call_date"]))
            due_date = self.dcatalog.inc_n_hours(due_date, 2).isodate()
        else:
            if ticketrow.get("respond_date"):
                d = date.Date(ticketrow["respond_date"])
                due_date = self._due_date(d).isodate()
            else:   # no respond date
                cd = date.Date(self.base_date(ticketrow["call_date"]))
                due_date = self._due_date(cd).isodate()
        return due_date
    
    def _due_date(self, respond_date):
        # take respond date and set it to 11:59 PM
        due_date = date.Date(respond_date)
        due_date.hours = 23
        due_date.minutes = 59
        return due_date


    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):
    """the XML structure has been expanded to support PA need for a new scheduled ticket date.  This date is returned from 
        the [ticket_info] table."""
    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SubmitTicketResponse xmlns="http://www.pa1call.org/RespondService">
      <strID>%(id)s</strID>
      <strCDC>%(membercode)s</strCDC>
      <strSerialNumber>%(ticket)s</strSerialNumber>
      <strResponse>%(responsecode)s</strResponse>
      <strInitials>%(initials)s</strInitials>
      <strScheduledMarkDate>%(markDate)s</strScheduledMarkDate>      
    </SubmitTicketResponse>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        # names contains ticket, membercode and responsecode
        names['id'] = escape(self.id)
        names['initials'] = escape(self.initials)
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() == 'response posted'

registry = {
    ('7501', 'xmlhttp', ''): XMLHTTPResponderData,
}

