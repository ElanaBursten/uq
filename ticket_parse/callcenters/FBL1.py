# FBL1.py

import string
from xml.sax.saxutils import escape
#
import businesslogic
import date
import errorhandling2
import tools
import xmlhttpresponder_data as xhrd
import ftpresponder_data as frd

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "837"
    call_center = "FBL1"

    COUNTIES = [
     "ST. TAMMANY", "ORLEANS", "ST. BERNARD", "JEFFERSON",
     "PLAQUEMINES", "LAFOURCHE", "TERREBONNE", "ST. MARY",
     "IBERIA", "VERMILION", "CAMERON", "CALCASIEU"
    ]

    def _use_96_hour_rule(self, ticketrow):
        county = ticketrow.get('work_county', '')
        ticket_type = ticketrow.get('ticket_type', '')

        return (county in self.COUNTIES) and ("96 HOURS NOTICE" in ticket_type)

    def legal_due_date(self, ticketrow):
        td = ticketrow["transmit_date"]
        if self.isemergency(ticketrow) \
        or ticketrow["ticket_type"] == "2NDQ-2ND REQUEST":
            return self._legal_due_date_emergency(td)
        #elif self._use_96_hour_rule(ticketrow):
        #    return self._legal_due_date(td, 4)
        else:
            return self._legal_due_date(td, 2)

    def _legal_due_date_emergency(self, transmit_date):
        # emergency: add 4 hours
        d = date.Date(transmit_date)
        d = self.dcatalog.inc_n_hours(d, 4)
        return d.isodate()

    def _legal_due_date(self, transmit_date, days):
        d = date.Date(transmit_date)
        d = date.Date(self.base_date(d.isodate()))
        d = self.dcatalog.inc_n_days(d, days)
        return d.isodate()

    def locator_methods(self, row):
        return [
            #self.route_lat_long,
            self.route_by_routinglist,
            self.lcatalog.map_latlong_qtrmin,
            self.route_munic,
        ]

    def route_munic(self, row):
        locator = area_id = None
        locator_info = self.tdb.find_munic_area_locator_2(
                       row.get("work_state", ""), "*",
                       row.get("work_city", ""), self.call_center)
        if locator_info:
            locator = locator_info[0].get("emp_id", None)
            area_id = locator_info[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

    # [2004.02.27] now replaced by locatorcatalog.map_latlong_qtrmin.
    def route_lat_long(self, row):
        locator = area_id = None
        lat = float(row.get("work_lat", "0"))
        long = float(row.get("work_long", "0"))
        # try to find coordinates in BNRG map, or LAFA map... they are
        # non-overlapping
        if lat and long:
            mapcells = mapgrid.lookup(lat, long)
            if mapcells:
                map, page, x, y = mapcells[0]
                x = chr(ord("A") + x - 1)
                # do something with this...
                result = self.tdb.find_map_area_locator(map, page, x, y)
                if result:
                    locator = result[0].get("emp_id", None)
                    area_id = result[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

    def allow_summary_updates(self):
        return 1

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soapenv:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <soapenv:Body>
   <AddTicketResponse>
     <AuthID>%(id)s</AuthID>
     <CallCenter>LOCC</CallCenter>
     <TicketNumber>%(ticket)s</TicketNumber>
     <TicketRevision>0</TicketRevision>
     <UtilityCode>%(membercode)s</UtilityCode>
     <Response>%(responsecode)s</Response>
     <Who>UTI</Who>
    <Notes>Response by Utiliquest</Notes>
    <WorkTime>%(locdatetime)s</WorkTime>
   </AddTicketResponse>
 </soapenv:Body>
</soapenv:Envelope>
"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T")
        })
        self.log.log_event("Location date/time injected into xml: %s"%(closed_date,))
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

class FTPResponderData(frd.FTPResponderData):

    # The response and ack formats change per 2010-01-01, so we need to
    # provide both the old and the new ways during the transition. default
    # is the NEW version.

    LEN_TICKET_NUMBER = 9

    TEMPLATE2a = "%(ticket_number)s%(status)s%(company)s\n%(ticket_number)s*%(company)s|%(remarks)s|\n"
    TEMPLATE2b = "%(ticket_number)s|%(company)s|%(status)s|\n"

    def template(self):
        if self.method in ('', '1'):
            return self.TEMPLATE2b
        else:
            return self.TEMPLATE2a

    def extra_data(self, namespace):
        # in the old situation we passed the locate id; in the new one this
        # is no longer the case.
        if self.method in ('', '1'):
            return {}
        else:
            self.row = self.tdb.getrecords("locate",
                       locate_id=namespace['locate_id'])[0]
            remarks = self.get_ticket_notes()
            return {'remarks': self.check_notes(remarks)}

    def get_acknowledgements(self, data, locates):
        """ Get a list of locates that were acknowledged by the server. <data>
            is the raw data of the acknowledgement file found on the server.
            <locates> is an optional list of locate dicts (only present when
            we need to store the response context; otherwise empty).

            Returns a tuple (ok, errors) where <ok> is a list of FTPResponderAck
            objects, containing the acknowledgements, and <errors> is a list
            of ErrorPackets containing caught exceptions, that can be logged
            by the caller.
        """

        # also see FDE1.py

        if self.method in ('', '1'):
            lines = [line for line in data.split('\n') if line.strip()]
            ok, errors = [], []
            for i, line in enumerate(lines):
                parts = line.split('|')
                try:
                    locate = locates[i]
                    (ticket_number, client_code, resp, date, time, result,
                     message) = parts[:7]
                    raw = tools.remove_crud_2(line.replace('|', ''))
                    ack = frd.FTPResponderAck(locate_id=locate['locate_id'],
                                              resp_id=locate['resp_id'],
                                              result=result,
                                              raw=raw,
                                              date=date,
                                              time=time)
                    ok.append(ack)
                except:
                    ep = errorhandling2.errorpacket(parts=parts,
                         lines=repr(lines))
                    errors.append(ep)
                    continue

        else:
            # the old way
            lines = data.split('\n')
            # Not interested in the acknowledgement of the comment(s)
            lines = [l for l in lines if l.strip() and l.find('\t*\t') == -1]
            data = string.join(lines,'\n')
            ok, errors = super(FTPResponderData,self).get_acknowledgements(data,locates)

        return ok, errors

registry = {
    ('FBL1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('FBL1', 'ftp', ''): FTPResponderData,
}

