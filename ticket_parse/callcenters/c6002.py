# c6002.py

import c6001

class BusinessLogic(c6001.BusinessLogic):
    call_center = "6002"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

registry = {}

