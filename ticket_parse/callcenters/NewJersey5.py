# NewJersey5.py

import NewJersey
import c7501
import date

class BusinessLogic(c7501.BusinessLogic):
    call_center = "NewJersey5"
    DEFAULT_LOCATOR = "265" # same as NewJersey

    # due date is the same as 7501
    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.set_midnight(cd)
            cd = self.dcatalog.inc_n_days(cd, 3) # 72 hours
        return cd.isodate()

class XMLHTTPResponderData(c7501.XMLHTTPResponderData):
    pass

registry = {
    ('NewJersey5', 'xmlhttp', ''): XMLHTTPResponderData,
}

