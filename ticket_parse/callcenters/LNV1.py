# LNV1.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'LNV1'
    DEFAULT_LOCATOR = "454"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        d = date.Date(ticketrow["transmit_date"])
        due_date = self._due_date(d, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 2 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)
        return d

registry = {}

