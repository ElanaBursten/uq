# OCC2.py

import OCC1
import date

class BusinessLogic(OCC1.BusinessLogic):
    call_center = 'OCC2'

    # steal methods from OCC1
    #
    OCC1_legal_due_date = OCC1.BusinessLogic.legal_due_date.im_func
    _legal_due_date = OCC1.BusinessLogic._legal_due_date.im_func

    def legal_due_date(self, ticketrow):
        dd = ticketrow.get('due_date', None)
        if dd:
            return dd
        if ticketrow['ticket_type'].find('MEET') != -1:
            # Meeting ticket. Set the legal due date to the meeting time.
            # For meet tickets, the meeting time has been set to the work date
            # for these particular call centers
            meet_date = ticketrow['work_date']
            d = date.Date(meet_date)
            return d.isodate()
        else:
            return self.OCC1_legal_due_date(ticketrow)

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            #self.route_map_page,
            self.lcatalog.munic,
        ]

    def is_ticket_ack(self, ticketrow):
        return OCC1.BusinessLogic.is_ticket_ack(self, ticketrow) \
            or ticketrow['explosives'].strip().upper() == 'Y'

    def allow_summary_updates(self):
        return 1

registry = {}

