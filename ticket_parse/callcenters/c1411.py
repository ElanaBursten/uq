# c1411.py

import FBL1

class BusinessLogic(FBL1.BusinessLogic):
    DEFAULT_LOCATOR = "10841"
    call_center = '1411'

class FTPResponderData(FBL1.FTPResponderData):
    pass

registry = {
    ('1411', 'ftp', ''): FTPResponderData,
}

