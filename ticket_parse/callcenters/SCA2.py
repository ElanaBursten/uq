# SCA2.py

import SCA1

class BusinessLogic(SCA1.BusinessLogic):
    DEFAULT_LOCATOR = "2148"
    call_center = "SCA2"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.SCA_special_routing,
            self.route_map_area,
            self.lcatalog.munic,
        ]

    def route_map_area(self, row):
        return self.lcatalog.map_area(row, "SDUQ")
        # is a RoutingResult, should be OK

    def locator_methods(self, row):
        return [
            self.route_map_area,
            self.lcatalog.munic,
        ]

registry = {}

