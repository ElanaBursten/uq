# NewJersey4.py

import NewJersey
import date
import xmlhttpresponder_data as xhrd
from xml.sax.saxutils import escape
import string

class BusinessLogic(NewJersey.BusinessLogic):
    call_center = "NewJersey4"
    # default locator is the same as NewJersey

    # legal due date changed in Mantis #2838
    def legal_due_date(self, ticketrow):
        cd = date.Date(ticketrow['call_date'])

        if self.isemergency(ticketrow):
            dd = self.dcatalog.inc_emergency(cd, hours=2)
            return dd.isodate()

        dd = date.Date(self.base_date(cd.isodate())) # use the 'base date'
        dd = self.dcatalog.inc_n_days(dd, 2) # +48 hours
        dd.hours, dd.minutes, dd.seconds = 23, 59, 59
        return dd.isodate()


class EmailResponderData(NewJersey.EmailResponderData):
    pass

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok', '1']
    codes_error = ["invalid id/cdc combination", "invalid serial number",
                   "member not on ticket", "invalid response code", "-1"]

    # hardcoded for now; should be in config later
    passwords = {
        'CBLVSN WARWICK': '44251',
        'CBLVSN W NYACK': '50761',
    }

    # note: we send the "response code" and the "explanation code" as
    # one string.

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsd="http://www.w3.org/2001/XMLSchema"
               xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <Respond xmlns="http://Irth.com/OneCall/PositiveResponse">
      <occCode>DSNY</occCode>
      <userID>%(membercode)s</userID>
      <password>%(password)s</password>
      <serviceAreaCode>%(membercode)s</serviceAreaCode>
      <occTicketID>%(ticket_number)s</occTicketID>
      <responseCode>%(responsecode)s</responseCode>
      <responseCategory></responseCategory>
      <comment>%(notes)s</comment>
      <utilityType></utilityType>
    </Respond>
  </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        names['ticket'] = strip_ticket_number(names['ticket'])
        names['ticket_number'] = names['ticket']
        names['notes'] = ''
        names['password'] = self.passwords.get(names['membercode'], '')
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

def strip_ticket_number(ticket_number):
    parts = ticket_number.split('-')
    if len(parts) >= 4:
        parts = parts[:-1] # strip last number, it's a version of some kind
    return string.join(parts, "-")

# QMAN-3421
class FTPResponderData(NewJersey.RocklandFTPResponderData):
    USER = 'ORUUNY'
    CALL_CENTER = 'NewJersey4'
    # todo(dan) This should be configured in the db, in a new JIRA item
    PUBLIC_IMPROVEMENT_CLIENTS = {'ORNG': 'ORNG-P', 'RCK-ORNG': 'RCK-ORNG-P',
     'SUL-ORNG': 'SUL-ORNG-P'}
    PUBLIC_IMPROVEMENT_STATUSES = ['0', '1', '2', '3']

    def extra_data(self, namespace):
        extra = NewJersey.RocklandFTPResponderData.extra_data(self, namespace)
        raw = namespace['raw']

        if raw['client_code'] and raw['client_code'].upper() in \
         self.PUBLIC_IMPROVEMENT_CLIENTS:
            locates = self.dbado.runsql_result("""
             select status from locate
             where ticket_id = %s and client_code = '%s' and active = 1
             """ % (raw['ticket_id'],
             self.PUBLIC_IMPROVEMENT_CLIENTS[raw['client_code']]))

            if len(locates) > 1:
                error_msg = \
                 'Error: %d public improvement locates found for ticket # %s.'
                raise Exception(error_msg % (len(locates), raw['ticket_number']))

            if locates:
                try:
                    status = self.mappings[locates[0]['status']][0]
                except KeyError:
                    status = locates[0]['status']

                if status in self.PUBLIC_IMPROVEMENT_STATUSES:
                    extra['public_improvement_status'] = status

        return extra

registry = {
    ('NewJersey4', 'email', ''): EmailResponderData,
    ('NewJersey4', 'xmlhttp', ''): XMLHTTPResponderData,
    ('NewJersey4', 'ftp', ''): FTPResponderData
}

