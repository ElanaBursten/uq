# FGV1.py

import FCL2
import date

class BusinessLogic(FCL2.BusinessLogic):
    call_center = 'FGV1'
    DEFAULT_LOCATOR = '4873'

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 3 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 3)
            d.set_time(23, 59, 59)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

registry = {}

