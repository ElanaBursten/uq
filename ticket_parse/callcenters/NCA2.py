# NCA2.py

import businesslogic
import NCA1
import duedates
import update_call_centers as ucc

class BusinessLogic(NCA1.BusinessLogic):
    call_center = 'NCA2'

    update_due_dates = True
    # Mantis 2828: due dates on NCA2 tickets override existing ones

    legal_due_date = duedates.due_date_CA_ATT

registry = {}

