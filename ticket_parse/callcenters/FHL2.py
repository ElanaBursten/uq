# FHL2.py

import businesslogic
import FHL1
import FHL3

class BusinessLogic(FHL1.BusinessLogic):
    call_center = "FHL2"

    def allow_summary_updates(self):
        return 1

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_area_with_code,
            self.lcatalog.munic,
        ]

    def route_map_area_with_code(self, row):
        # map name (e.g. MAPSCO) is ignored; instead, we use map 'FHL2'
        return self.lcatalog.map_grid_county_state_TX(row, code='FHL2')

    # different feed as of 2006-08-14, still derives from FHL1 though

class FTPResponderData(FHL3.FTPResponderData):
    pass

registry = {
    ('FHL2', 'ftp', ''): FTPResponderData,
}

