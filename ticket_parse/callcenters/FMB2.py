# FMB2.py

import FMB1, NewJersey

class BusinessLogic(FMB1.BusinessLogic):
    call_center = 'FMB2'

class FTPResponderData(NewJersey.FTPResponderData):

    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s"
            completionid="%(locate_id)s;%(resp_id)s">
        <customerid>SYSTEM</customerid>
        <occid>MDOC</occid>
        <Jobid>%(jobid)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <reasonid>%(status)s</reasonid>
        <faccode1>ELECTRIC</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <Remarks>%(remarks)s</Remarks>
    </Locate>
    """

    def extra_data(self, namespace):
        extra = NewJersey.FTPResponderData.extra_data(self, namespace)
        raw = namespace['raw']
        extra['jobid'] = raw['serial_number'] or raw['ticket_number']
        return extra

    # status mappings to determine 'ismarked' etc are different from
    # FJL1/NewJersey
    MARK_SETTINGS = {
        'CLEAR': (0, 1, '<actcode1>NO GAS LINES</actcode1>'),
        'MARKED': (1, 0, '<actcode1>MARKED</actcode1>'),
        'PRIVATE': (1, 0, '<actcode1>MARKED</actcode1>'),
    }

registry = {
    ('FMB2', 'ftp', ''): FTPResponderData,
}

