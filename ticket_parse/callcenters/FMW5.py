# FMW5.py

import update_call_centers as ucc
import businesslogic
import FMW1

class BusinessLogic(FMW1.BusinessLogic):
    call_center = 'FMW5'

class UpdateData(ucc.UpdateData):
    fields = ["ticket_type"]

registry = {}


