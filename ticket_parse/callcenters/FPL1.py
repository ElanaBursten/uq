# FPL1.py

import businesslogic
import FOL1

class BusinessLogic(FOL1.BusinessLogic):
    # TODO: locator uses:
    # 1. lat/long
    # 2. city/county routing
    # if we don't override locator(), it will be the same as Orlando's.

    # is_ticket_ack is inherited from FOL1 as well

    DEFAULT_LOCATOR = "2193"
    call_center = "FPL1"

registry = {}

