# OCC2.py

import update_call_centers as ucc
import OCC1
import date

class BusinessLogic(OCC1.BusinessLogic):
    call_center = 'OCC5'

    def allow_summary_updates(self):
        return 1
    
class UpdateData(ucc.UpdateData):
    fields = ["ticket_type"]

registry = {}

