# FTL2.py

import Atlanta
import c1391

class BusinessLogic(Atlanta.BusinessLogic):
    """
    according to Kyle, FTL2 doesn't have any clients, so we won't need any
    locator methods
    """
    call_center = "FTL2"
    DEFAULT_LOCATOR = "2303"    # assume the same as FTL1

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('FTL2', 'xmlhttp', ''): XMLHTTPResponderData,
}

