# FJL3.py

import businesslogic
import FJL1
import c1391

class BusinessLogic(FJL1.BusinessLogic):
    """
    due date:
    emergencies: +2 hours
    other: +48 hours
    => same as FMS1 (inherited via FJL1)
    """

    call_center = "FJL3"

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('FJL3', 'xmlhttp', ''): XMLHTTPResponderData,
}

