# FCV2.py

import businesslogic
import date
import tools

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "339"
    call_center = "FCV2"

    def legal_due_date(self, ticketrow):
        if ticketrow.get("respond_date", None):
            due_date = ticketrow["respond_date"]
        else:
            d = date.Date(ticketrow["call_date"])
            due_date = self._due_date(d).isodate()
        return due_date

    def _due_date(self, call_date):
        due_date = date.Date(call_date)
        due_date = date.Date(self.base_date(due_date.isodate()))
        return self.dcatalog.inc_n_days(due_date, 2)

    def locator_methods(self, row):
        return [
            self.route_augusta,
            self.route_by_routinglist,
            self.lcatalog.map_grid_county_state,
            self.route_map_area_RVA2,
            self.lcatalog.munic,
        ]

    def route_map_area_RVA2(self, row):
        return self.lcatalog.map_area(row, "RVA2")
        # this returns a RoutingResult, should be OK

    def route_augusta(self, row):
        locator = area_id = None
        if row["work_county"] == "AUGUSTA" and row["map_page"]:
            aug_info = self.tdb.find_augusta_va(row["map_page"])
            if aug_info:
                locator = aug_info[0].get("emp_id", None)
                area_id = aug_info[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

registry = {}

