# FCO1.py

import businesslogic
import tools
import date

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "1625"
    call_center = 'FCO1'

    # for now, this can stay here
    routing_data = (
        (1, "US WEST BROADBAND", 1420),
        (1, "US WEST PROJECTS", 1420),
        (2, "C NO PET FENCE", 1345),
        (2, "THE HOLE BUSINESS", 1345),
        (2, "HOME IMPROVEMENT", 1345),
        (2, "AERATION INC.", 1345),
        (2, "DOG PATCH", 1345),
        (2, "PLUGGIN-A-LAWN", 1345),
        (2, "WEST SUBURBAN RADIO FENCE CO.", 1345),
        (1, "TREX", 1418),
        (3, "TREX", 1418),
        (1, "T-REX", 1418),
        (3, "T-REX", 1418),
        (1, "TREX ENG", 1419),
        (1, "LAWN AERATION", 1345),
        (1, "CORE AERATION", 1345),
        (1, "AERATE LAWN", 1345),
        (1, "BURY NEW SERVICE FOR CATV", 1345),
        (1, "BURY INVISIBLE FENCE", 1308),
        (1, "BURY INV FENCE", 1308),
        (1, "BURY INV FENCE CABLE", 1308),
        (1, "BURYING INV FENCE CABLE", 1308),
        (1, "AERATION", 1309),
        (1, "LAWN AERATION", 1309),
        (2, "INVISIBLE FENCE", 1308),
        (1, "INVISIBLE FENCE", 1308),
        (1, "PED TO HOUSE BURY", 1417),
        (2, "TCS COMMUNICATIONS", 1380),
        (2, "TCS", 1380),
        (2, "DEA", 1380),
        (2, "DEA CONSTRUCTION", 1380),
        (2, "J & R FERNANDEZ", 1380),
        (2, "J R FERNANDEZ", 1380),
        (2, "LEAD COMMUNICATIONS", 1380),
        (2, "LEAD COMMUNICATIONS INC", 1380),
        (2, "UTILICON. INC", 1384),
        (2, "SOUTHWESTERN COMMUNICATIONS INC", 1384),
        (2, "METRO WEST", 1384),
        (2, "BROADBAND SERVICES", 1384),
        (2, "US DIRTWORKS", 1384),
    )

    routing_data_NS = (
        "AT&T BROADBAND DROP BURY",
        "AT& BURY DROP",
        "AT&T DROP BURY",
        "AT&T BURY DROP",
        "AT& T DROP BURY",
        "AT&T BROADBAND DROP BURY",
        "ATT BROADBAND DROP BURY",
        "BURY ATT DROP",
        "BURY AT&T BROADBAND DROP",
        "BURY AT&T DRO",
        "BURY AT&T DROP",
        "BURY CABLE DROP",
        "BURY CABLEING DROP",
        "BURY CABLING DROP",
        "BURY CATV DROP",
        "BURY CATV SVC DROP",
        "BURY DROP",
        "BURY /DROP",
        "BURY ATT CABL",
        "BURYING AT&T DROP",
        "CABLE DROP BURY",
        "CABTV DROP BURY",
        "CATV DROP BURY",
        "CATV UPGRADE REBUILD",
        "DROP BURY",
        "AT&T CATV UPGRADE",
        "AT&T UPGRADE",
        "CATV UPGRADE",
        "CATV MN UPGRADE",
        "CATVUPGRADE",
        "MN CATV UPGRADE",
        "NEW CATV UPGRADE",
        "UPGRADE CATV",
        "UPGRADE CATV MAIN",
        "GRPGRADE CATV MAIN",
        "UPGRADE CATV MN",
        "UPGRADE MN CATV",
        "AT&T BROADBAND REBUILD",
        "REBUILD",
        "REBUILD CATV",
        "AT&T DROP BURY, PED TO HOUSE BURY",
        "REPL CATV MIN",
        "BURY CATV SERV",
        "COMCAST DROP BURY",
    )

    routing_data_usw = (
        "I H C/D I A",
        "I H C/SPECIAL PROJECT",
        "I H C/SPECIAL PROJECTS",
        "MILL ELEC/D C I/S/P",
        "MILLS ELEC/DCI-S/P",
        "MILLS ELECT/DCI-S/P",
        "MILLS ELECTICAL/DCI/SPECIAL PROJECT",
        "MILLS ELECTRICAL/D.C.I.",
        "MILLS ELECTRICAL/DC I",
        "MILLS ELECTRICAL/DCI",
        "MILLS ELECTRICAL/DCI/S/P",
        "MILLS ELECTRICAL/DCI/SPECIAL PROJECT",
        "SP / MILLS ELECT/ DCI",
        "SP/MILLS ELECTRICAL/ DCI",
        "SPEARS EXCAVTING",
        "SPECIAL PROJECT",
        "SPECIAL PROJECTS",
        "s/p",
        "S/P",
    )

    # area codes for "Rebuild ND" and "Rebuild SD"
    REBUILD_ND = "1383"
    REBUILD_SD = "1384"
    USW_PROJECTS = "1420"

    def locator_methods(self, row):
        return [
            #self.use_routing_table_usw,
            #self.use_routing_table_NS,
            self.use_routing_table,
            self.route_by_routinglist,
            self.lcatalog.township,
            self.lcatalog.munic,
        ]

    def use_routing_table(self, row):
        fieldnames = {1: "work_type", 2: "con_name", 3: "company"}
        for type, name, area_id in self.routing_data:
            fieldname = fieldnames[type]
            if row[fieldname] == name:
                # look area_id up in table to get locator
                locator = self.tdb.get_locator_by_area(area_id)
                if locator:
                    return tools.RoutingResult(locator=locator,
                           area_id=str(area_id))
                # if it's None, just continue, I guess

        return tools.RoutingResult() # nothing found, eh?

    def use_routing_table_NS(self, row):
        locator = ""
        for name in self.routing_data_NS:
            if row["work_type"] == name:
                # get area name, using township_extended
                d = self.lcatalog.township_extended(row)
                if not d:
                    return tools.RoutingResult()
                    # useless if we cannot find township data
                area_name = d.get("area_name", "")
                if area_name.startswith("ND"):
                    locator = self.tdb.get_locator_by_area(self.REBUILD_ND)
                    return tools.RoutingResult(locator=locator,
                           area_id=str(self.REBUILD_ND))
                elif area_name.startswith("SD"):
                    locator = self.tdb.get_locator_by_area(self.REBUILD_SD)
                    return tools.RoutingResult(locator=locator,
                           area_id=str(self.REBUILD_SD))
                else:
                    locator = None # useless if area isn't North/South
                return tools.RoutingResult(locator=locator)

        return tools.RoutingResult()

    def use_routing_table_usw(self, row):
        for name in self.routing_data_usw:
            if row["company"] == name:
                locator = self.tdb.get_locator_by_area(self.USW_PROJECTS)
                return tools.RoutingResult(locator=locator,
                       area_id=str(self.USW_PROJECTS))
        return tools.RoutingResult()

    def legal_due_date(self, row):
        # emergency: +2 hours
        # "meet" tickets: use work_date
        # others: use Atlanta-like, go to midnight, then move 2 days
        # if self.isemergency(row):
        #    d = date.Date(row["transmit_date"])
        #    d = self.dcatalog.inc_n_hours(d, 2)
        #    return d.isodate()
        #if self.is_meet_ticket(row):
        #    d = date.Date(row["work_date"])
        #    d = self.dcatalog.set_midnight(d)
        #    return d.isodate()
        #else:
            # choice 1: midnight after 48 hours
         #   d1 = date.Date(self.base_date(row["transmit_date"]))
         #   d1.hours, d1.minutes, d1.seconds = 23, 59, 59
         #   d1 = self.dcatalog.inc_48_hours(d1)
            # choice 2: work_date
        d2 = date.Date(row["work_date"])
            # return the later of these
         #   return max(d1.isodate(), d2.isodate())
        return (d2.isodate())
    
    def is_meet_ticket(self, row):
        return row["ticket_type"].endswith("MEET")

    def do_not_mark_before_date(self, row):
        td = date.Date(row["transmit_date"])
        td = self.dcatalog.inc_n_days(td, 1)
        td.hours, td.minutes, td.seconds = 13, 0, 0
        return td.isodate()

    def allow_summary_updates(self):
        # 2004-06-24: new summary format requires this
        return 1

registry = {}

