# Atlanta.py

import businesslogic
import date
import tools

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "209"
    call_center = "Atlanta"

    AREA_BELLSOUTH = "3128"
    AREA_SCREENS = "3129"

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        if emergency:
            due_date = date.Date(date.Date(ticketrow["call_date"]))
            due_date = self.dcatalog.inc_n_hours(due_date, 2).isodate()
        else:
            if ticketrow.get("respond_date"):
                d = date.Date(ticketrow["respond_date"])
                due_date = self._due_date(d).isodate()
            else:   # no respond date
                cd = date.Date(self.base_date(ticketrow["call_date"]))
                due_date = self._old_due_date(cd).isodate()
        return due_date

    # This was the original due_date computation; it was then replaced by the
    # other (simpler) one, but is now back in use again if a ticket lacks a
    # respond date.
    def _old_due_date(self, date):
        # after 4:30 PM we get an extra day
        extra = (date.hours * 100 + date.minutes) >= 1630
        # we get 2 days plus a possible extra one
        due_date = date + 2 + extra
        # if the resulting day falls in a weekend, move it to the next week
        while not self.isbusinessday(due_date):
            due_date.inc()
        due_date.resettime()    # we want a *date*, not the time
        return due_date

    def _due_date(self, respond_date):
        # take respond date and set it to 11:59 PM
        due_date = date.Date(respond_date)
        due_date.hours = 23
        due_date.minutes = 59
        return due_date

    def locator_methods(self, row):
        return [
            self.route_special,
            self.route_by_routinglist,
            self.lcatalog.grid_area,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic_with_dummy_city,
        ]

    def route_special(self, row):
        BGA_CODES = ("BGAWC", "BGAWA", "BGAWM", "BGAWR", "BGAWS")
        if row.get("work_type", "") in ("BSW", "BURY SERVICE WIRE") \
        and row.get("company", "") == "BELLSOUTH" \
        and row.get("client_code", "") in BGA_CODES:
            locator_id = self.tdb.get_locator_by_area(self.AREA_BELLSOUTH)
            if locator_id:
                return tools.RoutingResult(locator=locator_id,
                       area_id=self.AREA_BELLSOUTH)

        if row.get("work_type", "") in ("BSW", "BURY SERVICE WIRE") \
        and row.get("client_code", "") in ("ATL01", "ATL02"):   # was: USW01
            locator_id = self.tdb.get_locator_by_area(self.AREA_SCREENS)
            if locator_id:
                return tools.RoutingResult(locator=locator_id,
                       area_id=self.AREA_SCREENS)

        return tools.RoutingResult()

    def isrenotification(self, ticket):
        return ticket["ticket_type"].find("UPDATE") > -1 \
         or ticket["ticket_type"].find("RESTAKE") > -1 \
         or ticket["ticket_type"] == "INSUFFICIENT NOTICE"

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1

registry = {
}

