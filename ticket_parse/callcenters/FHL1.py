# FHL1.py

import businesslogic
import FMB1, FDE1
import date
import string
import ftpresponder_data as frd

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = "FHL1"
    DEFAULT_LOCATOR = "4636"

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        #elif row['ticket_type'].lower().find('short not') > -1:
        #    cd = self.dcatalog.inc_emergency(cd, 10)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_area_with_code,
            self.lcatalog.munic,
        ]

    def route_map_area_with_code(self, row):
        # FHL mapinfo looks like this: TX 416 J
        # we append a '1' so we can use map_area_with_code
        row['map_page'] = string.strip(row.get('map_page', '')) + "1"
        return self.lcatalog.map_area_with_code(row, self.__class__.call_center)

    def allow_summary_updates(self):
        return 1

    do_not_respond_before_date = FMB1.BusinessLogic.do_not_respond_before_date.im_func


class FTPResponderData(FDE1.FTPResponderData):
    pass

registry = {
    ('FHL1', 'ftp', ''): FTPResponderData,
}

