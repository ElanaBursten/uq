# NewJersey2.py

import NewJersey
import update_call_centers as ucc
import FJL1
import ticket_adapter

class BusinessLogic(NewJersey.BusinessLogic):
    call_center = 'NewJersey2'

    def allow_summary_updates(self):
        return True

class TicketAdapter(ticket_adapter.TicketAdapter):
    def adapt(self, ticket):
        # Mantis 2823: Customer incorrectly appends a '3' to the term id;
        # needs to be removed
        # We could narrow this further down to e.g. emergencies only?
        for idx, loc in enumerate(ticket.locates):
            if len(loc.client_code) > 3 and loc.client_code.endswith('3'):
                loc.client_code = loc.client_code[:-1]

class UpdateData(ucc.UpdateData):
    __fields__ = ["service_area_code"] # wrong?

class FTPResponderData(NewJersey.FTPResponderData):
    pass

registry = {
    ('NewJersey2', 'ftp', ''): FTPResponderData,
}

