# SCA3.py

import SCA1

class BusinessLogic(SCA1.BusinessLogic):
    call_center = 'SCA3'

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def legal_due_date(self, row):
        # it's supposed to be already extracted from the ticket; however, this
        # does not work for manual tickets, in which case we *do* need to
        # compute a due date
        z = row.get('due_date', None)
        if z and 'SHRT' not in row['ticket_type']:
            return z
        else:
            return SCA1.BusinessLogic.legal_due_date(self, row)

registry = {}

