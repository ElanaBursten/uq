# c9001.py

import xmlhttpresponder_data as xhrd
import date
import FEF1
from xml.sax.saxutils import escape

class BusinessLogic(FEF1.BusinessLogic):
    DEFAULT_LOCATOR = '7294'
    call_center = '9001'

    # due date computation is same as FEF1 (which derives from FOL1).
    # note: for 9001, emergencies are computed here, but for non-emergencies
    # the due date is taken from the ticket.  (Mantis #1409)

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]


class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    # note: we send the "response code" and the "explanation code" as
    # one string.

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
   <TicketResponseStatus xmlns="http://translore.com/ResponseService/">
     <AuthID xmlns="">%(id)s</AuthID>
     <CallCenter xmlns="">SSOCF</CallCenter>
     <TicketNumber xmlns="">%(ticket)s</TicketNumber>
     <TicketRevision xmlns="">0</TicketRevision>
     <UtilityCode xmlns="">%(membercode)s</UtilityCode>
     <Response xmlns="">%(responsecode)s%(explanation)s</Response>
     <Who xmlns="">UTI</Who>
     <Notes xmlns="">%(notes)s</Notes>
     <WorkTime xmlns="">%(locdatetime)s</WorkTime>
   </TicketResponseStatus>
 </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        notes = self.get_ticket_notes()
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T"),
            'notes': self.check_notes(notes)
        })
        self.log.log_event("Location date/time injected into xml: %s"%(closed_date))
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

registry = {
    ('9001', 'xmlhttp', ''): XMLHTTPResponderData,
}

