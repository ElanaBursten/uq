# FWP1.py

import date
import string
import emailresponder_data as erd
import businesslogic

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "296"
    call_center = "FWP1"

    def legal_due_date(self, ticketrow):
        d = date.Date(ticketrow["transmit_date"])
        if self.isemergency(ticketrow):
            d = self.dcatalog.inc_emergency(d, 3)   # +3 hours
        else:
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)  # +2 business days

        return d.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

class EmailResponderData(erd.EmailResponderData):

    # for this responder, these fields all need to have the exact number of
    # characters, padded to the right with spaces if necessary.
    SEPARATOR = ''
    LINE_FIELDS = [
        ('client_code', 5),
        ('ticket_number', 15),
        ('work_date', 10),
        ('work_county', 20),
        ('work_city', 30),
    ]

    def response_is_success(self, row, resp):
        # we don't get an acknowledgement, so we consider this to be always true
        return True

    def make_email_line(self, names, tdb, version):
        #names.update({
        #    'dig_date': names['work_date'],
        #    'county': '...',
        #    'town': '...',
        #})

        # create line
        parts = []
        for fieldname, length in self.LINE_FIELDS:
            part = names[fieldname]
            while len(part) < length:
                part += " "
            if len(part) > length:
                part = part[:length]
            parts.append(part)

        return string.join(parts, self.SEPARATOR)

    def do_skip(self, row):
        # only respond to locates with status N, NP, and C.
        # TEMPORARY -- this should be moved to config.
        status = row.get('status', '')
        return status not in ('N', 'NP', 'C')

registry = {
    ('FWP1', 'email', ''): EmailResponderData,
}

