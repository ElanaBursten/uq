# __init__.py
# a few auxiliary functions to get specific classes

import update_call_centers as ucc

def get_businesslogic(name):
    """ Retrieve a BusinessLogic class for <name>.
        (<name> is the name of a call center, possibly with a 'c' in front of
        it if it starts with a number. """
    mod = __import__(name, globals(), locals())
    # NOTE: globals() and locals() are necessary here, otherwise it won't
    # find modules in this package!  E.g. __import__('LID1') would not work.
    return mod.BusinessLogic

def get_updatedata(name):
    mod = __import__(name, globals(), locals())
    if not hasattr(mod, 'UpdateData'):
        # Inject the UpdateData
        mod.UpdateData = ucc.UpdateData
    # todo(dan) Check if either all of geocode address fields, and lat/long are
    # included, or none are included, to prevent inconsistencies?
    return mod.UpdateData

def get_xmlhttpresponderdata(name):
    mod = __import__(name, globals(), locals())
    return mod.XMLHTTPResponderData

def get_emailresponderdata(name):
    mod = __import__(name, globals(), locals())
    return mod.EmailResponderData

def get_ftpresponderdata(name):
    mod = __import__(name, globals(), locals())
    return mod.FTPResponderData

def get_soapresponderdata(name):
    mod = __import__(name, globals(), locals())
    return mod.SOAPResponderData

# NOTE: possible confusion with call_center_adapter which is used as well
def get_adapter(name):
    mod = __import__(name, globals(), locals())
    return mod.TicketAdapter

def get_wo_businesslogic(name):
    mod = __import__(name, globals(), locals())
    return mod.WorkOrderBusinessLogic

def get_wo_responderdata(name):
    mod = __import__(name, globals(), locals())
    return mod.WorkOrderResponderData

def get_registry(name):
    mod = __import__(name, globals(), locals())
    return mod.registry

