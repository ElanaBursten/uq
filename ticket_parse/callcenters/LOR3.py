# LOR3.py

import LOR1
import _LocIncResponder as lir

class BusinessLogic(LOR1.BusinessLogic):
    call_center = 'LOR3'

class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'OUNC'

registry = {
    ('LOR3', 'xmlhttp', ''): XMLHTTPResponderData,
}

