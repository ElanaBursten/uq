# FOK1.py

import businesslogic
import FNV2

class BusinessLogic(businesslogic.BusinessLogic):
    DEFAULT_LOCATOR = "2488"
    call_center = "FOK1"

    legal_due_date = FNV2.BusinessLogic.legal_due_date.im_func

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

registry = {}

