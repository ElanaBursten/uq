# c300.py

import Atlanta

class BusinessLogic(Atlanta.BusinessLogic):

    DEFAULT_LOCATOR = "6005"
    call_center = "300"

    def locator_methods(self, row):
        return [
            #self.lcatalog.grid_area,
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
            # state/county/* is supposed to route locates to three "default"
            # locators.
        ]

registry = {}

