# FMW1.py

import ftpresponder_data as frd
import businesslogic
import call_centers
import date
import tools
import FMB1
import FDE1

class BusinessLogic(FMB1.BusinessLogic):

    call_center = "FMW1"
    DEFAULT_LOCATOR = "1006"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_page,
            # todo(dan) Ok to add map_qtrmin for formats that didn't get changed?
            self.lcatalog.map_qtrmin,
            self.route_munic,
        ]

    def route_map_page(self, row):
        locator = code = None
        map_page = row.get("map_page", "")
        if map_page:
            try:
                code, page, coords = map_page.split()
            except ValueError:
                pass
                # invalid map_page, locator cannot be determined
            else:
                # **special**
                if code in ("NW", "NE", "SW", "SE"):
                    code = "DC"
                # prepend call center code and dash
                code = self.call_center + "-" + code
                routingresult = self.lcatalog.map_area_with_code(row, code)
                return routingresult
        return tools.RoutingResult()

    def route_munic(self, row):
        locator = area_id = None
        locator_info = self.tdb.find_munic_area_locator_2(
         row.get("work_state", ""), row.get("work_county", ""),
         row.get("work_city", ""), self.call_center)
        if locator_info:
            locator = locator_info[0].get("emp_id", None)
            area_id = locator_info[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

    def legal_due_date(self, ticketrow):
        call_date = ticketrow["call_date"]
        emergency = self.isemergency(ticketrow)
        state = ticketrow.get("work_state", "")
        return self._legal_due_date(date.Date(call_date),
               emergency=emergency, state=state).isodate()

    def _legal_due_date(self, call_date, emergency=0, state=""):
        # emergency tickets: +2 hours
        # DC: +48 hours to 5PM
        # others: +48 hours to midnight
        if emergency:
            return self.dcatalog.inc_emergency(call_date, hours=2)
        elif state == "DC":
            call_date = date.Date(self.base_date(call_date.isodate()))
            dd = self.dcatalog.inc_n_days(call_date, 2)
            dd.hours, dd.minutes, dd.seconds = 17, 0, 0
            return dd
        else:
            call_date = date.Date(self.base_date(call_date.isodate()))
            dd = self.dcatalog.inc_n_days(call_date, 2)
            return self.dcatalog.set_midnight(dd)

    def allow_summary_updates(self):
        return 0
        # FMB1 allows this, but there's no reason to allow it for FMW1

    # do_not_respond_before: derived from FMB1


class FTPResponderData(FDE1.FTPResponderData):
    pass

registry = {
    ('FMW1', 'ftp', ''): FTPResponderData,
}

