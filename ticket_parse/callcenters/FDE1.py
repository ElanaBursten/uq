# FDE1.py

import string
import time
import xml.etree.ElementTree as ET
#
import businesslogic
import date
import errorhandling2
import ftpresponder_data as frd
import tools

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = "FDE1"
    DEFAULT_LOCATOR = "2730"

    # same as FMB1
    def legal_due_date(self, ticketrow):
        call_date = ticketrow["call_date"]
        emergency = self.isemergency(ticketrow)
        return self._legal_due_date(date.Date(call_date),
               emergency=emergency).isodate()

    def _legal_due_date(self, call_date, emergency=0):
        # emergency tickets: +2 hours
        # others: +48 hours to midnight
        if emergency:
            return self.dcatalog.inc_emergency(call_date, hours=2)
        else:
            call_date = date.Date(self.base_date(call_date.isodate()))
            dd = self.dcatalog.inc_n_days(call_date, 2)
            return self.dcatalog.set_midnight(dd)

    # same as OCC1
    def route_map_page(self, row):
        locator = None
        map_page = row.get("map_page", "")
        if map_page:
            try:
                code, page, coords = string.split(map_page)
            except ValueError:
                return self.lcatalog.empty()
                # invalid map_page, locator cannot be determined
            code = self.call_center + "-" + code
            routingresult = self.lcatalog.map_area_with_code(row, code)
            return routingresult

        return tools.RoutingResult()

    def locator_methods(self, row):
        return [
            #self.lcatalog.map_area_with_code,
            self.route_by_routinglist,
            self.route_map_page,
            self.lcatalog.map_latlong_qtrmin,
            self.lcatalog.munic,
        ]


# OBSOLETE
class OldFTPResponderData(frd.FTPResponderData):
    LEN_TICKET_NUMBER = 9
    TEMPLATE2 = "%(ticket_number)s|%(company)s|%(status)s||\n"

    def retry(self, result_code):
        '''
        If result_code indicates that the acknowledgement failed
        and to re-try, return True
        '''
        if str(result_code) == '19':
            return True
        elif str(result_code) == '14':
            return True
        else:
            return False

    def get_acknowledgements(self, data, locates):
        lines = data.split('\n')
        lines = [l for l in lines if l.strip()]

        ok, errors = [], []

        for i in range(len(lines)):
            try:
                line = lines[i].strip()
                locate = locates[i]
                parts_before = line.split('|')
                parts = map(tools.remove_crud_2, parts_before)

                '''The documentation says:

                Ticket Number: The call center ticket number that was sent to
                be statused.
                District Code: The associated district code that is to be
                statused for the corresponding ticket number.
                Status: The proposed status code for the given ticket number.
                Comment: The status comment to be added. The limit on the size
                of the note is 200 characters. This is NOT a required field.
                Date: The date the ticket status was received and processed by
                Ticket Check.
                Time: The time the ticket status was received and processed by
                Ticket Check.
                Result Code: Displays the results of the update to the tickets
                given ticket, district code and response.
                Message: English description of the provided result code

                The replies do not have the Comment field
                '''

                if len(parts) <= 8:
                    # some responses don't include the comment field...
                    ticket_number, client_code, resp, date, time, result, message = parts[:7]
                elif len(parts) == 9:
                    # ...while others do
                    (ticket_number, client_code, resp, comment, date, time,
                     result, message) = parts[:8]

                if self.retry(result):
                    raise ValueError('Result indicates a retry is needed')

                line2 = tools.remove_crud_2(line.replace('|', '')) # ?
                ack = frd.FTPResponderAck(locate_id=locate['locate_id'],
                                          resp_id=locate['resp_id'],
                                          result=result,
                                          raw=line2,
                                          date=date,
                                          time=time)
                ok.append(ack)
            except:
                ep = errorhandling2.errorpacket(parts=parts, locate=locate,
                     lines=repr(lines))
                errors.append(ep)
                continue

        return ok, errors

# XXX we will only be using this responder (#2961)
class FTPResponderData(frd.FTPResponderData):
    # XXX fix state code (work_state or file_ext?)
    # XXX check length of ticket number
    # XXX check valid status codes

    # ----- copied from NewJersey; refactor into base class? -----

    # Note: Responder only accepts status codes '1' (clear) and '2' (marked),
    # so the appropriate mappings must be added in config.xml.

    store_resp_context = True
    # stores resp. context, because the response format does not allow us to
    # pass a locate_id or resp_id.

    LEN_TICKET_NUMBER = 0

    TEMPLATE1 = """\
<?xml version="1.0" encoding="utf-8"?>
<Responses>
###BODY###
</Responses>"""

    TEMPLATE2 = """\
  <Response>
    <StateCode>%(state)s</StateCode>
    <TicketNumber>%(ticket_number)s</TicketNumber>
    <DistrictCode>%(company)s</DistrictCode>
    <StatusCode>%(status)s</StatusCode>
    <StatusComments>%(notes)s</StatusComments>
  </Response>
    """

    def resend_response(self, result_code):
        '''
        If result_code indicates that the acknowledgement failed, and the
        response needs to be resent, return True.
        '''

        if str(result_code) == '19':
            return True
        elif str(result_code) == '14':
            return True
        else:
            return False

    def gen_filename(self):
        """ Generate a unique filename to be posted on the server.
            If specified, use the file_ext in the filename so it's clear which
            center a file belongs to. """
        t = time.time()
        t9 = time.localtime(t)
        year, month, day, hours, minutes, seconds = t9[:6]
        milliseconds = int((t - int(t)) * 1000)
        prefix = self.file_ext + "-" if self.file_ext else ""
        if self.login:
            prefix += (self.login + "-")
        name = "%s%04d%02d%02d%02d%02d%02d%03d.xml" % (prefix, year, month,
               day, hours, minutes, seconds, milliseconds)
        return name

    # Result files have the same name as the files we originally sent, with an
    # .rxml extension instead of .xml.

    def is_response_file(self, filename):
        """ Ack filenames must end in .rxml. """
        return filename.endswith(".rxml")

    def get_acknowledgements(self, data, locates):
        ok, errors = [], []
        xml = ET.fromstring(data)
        for r in xml.findall('Result'):
            try:
                a = frd.FTPResponderAck()
                a.result = r.find('ResultCode').text
                a.raw = r.find('ResultMessage').text
                ticket_number = r.find('TicketNumber').text
                term_id = r.find('DistrictCode').text
                z = [loc for loc in locates
                     if loc['ticket_number'] == ticket_number
                     and self.translations.get(loc['client_code'],
                      loc['client_code']) == term_id][0]
                a.locate_id = z['locate_id']
                a.resp_id = z['resp_id']
                if self.resend_response(a.result):
                    ep = errorhandling2.ErrorPacket()
                    ep.add(
                     info="Result indicates response must be resent: %s/%s" %
                     (ticket_number, term_id))
                    errors.append(('resend response', ep, a))
                else:
                    ok.append(a)
            except IndexError:
                ep = errorhandling2.errorpacket()
                ep.add(info="Term id not found in response_ack_context: %s/%s" %
                 (ticket_number, term_id))
                errors.append(ep)
            except:
                ep = errorhandling2.errorpacket()
                errors.append(ep)

        return ok, errors

    def extra_data(self, namespace):
        extra = frd.FTPResponderData.extra_data(self, namespace)
        self.row = namespace['raw']
        extra['state'] = self.row['work_state']
        extra['notes'] = (self.get_locate_notes() or '')[:200]
        return extra

    @staticmethod
    def is_success(result_code):
        return result_code == '0'

registry = {
    ('FDE1', 'ftp', ''): FTPResponderData,
}

