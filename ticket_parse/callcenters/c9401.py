# c9401.py

import businesslogic
import date
import FNV1
import c1391

class BusinessLogic(FNV1.BusinessLogic):
    DEFAULT_LOCATOR = '7434'
    call_center = '9401'

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, hours=2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 3)
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('9401', 'xmlhttp', ''): XMLHTTPResponderData,
}

