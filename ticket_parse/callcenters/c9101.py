# c9101.py

import FCL1
import date

class BusinessLogic(FCL1.BusinessLogic):
    DEFAULT_LOCATOR = '7428'
    call_center = '9101'

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        d = date.Date(ticketrow["transmit_date"])
        due_date = self._due_date(d, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            d = date.Date(self.base_date(d.isodate()))
            # add 2 days, but ignore weekends
            d = self.dcatalog.inc_n_days(d, 2)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

    def isrenotification(self, ticket):
        """
        overrides FCL1's isrenotification, which flags RXMT tickets;
        this is not desirable for 9101
        """
        return False

registry = {}

