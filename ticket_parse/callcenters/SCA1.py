# SCA1.py

import re
import string
from xml.sax.saxutils import escape
#
import businesslogic
import date
import xmlhttpresponder_data as xhrd
import NCA1

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "2087"
    call_center = "SCA1"

    def locator_methods(self, row):
        return [
            self.cc_routing,
            self.route_by_routinglist,
            self.lcatalog.map_grid_county_state,
            self.lcatalog.munic,
        ]

    def legal_due_date(self, row):
        cds, wds = row['call_date'], row.get('work_date', None)
        d = date.Date(cds)
        # note: short notice rule has priority over emergency (#3320)
        if 'SHRT' in row['ticket_type']:
            d = date.Date(row['work_date'])
        elif self.isemergency(row):
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            d = date.Date(self.base_date(d.isodate()))
            d1 = self.dcatalog.inc_n_days(d, 2) # non-intuitive, fix :(
            wd = row.get('work_date', None)
            if wd:
                d2 = date.Date(wd)
                cds2 = self.dcatalog.inc_n_hours(date.Date(cds), 2).isodate()
                if 'MEET' in row['ticket_type'] and wds >= cds2:
                    # work date should not be in the past
                    # also, work date must be at least 2 hours later than the
                    # call date (Mantis #2865) (also in NCA1)
                    d = d2
                else:
                    d = max(d1, d2)
            else:
                d = d1
        return d.isodate()

    def allow_summary_updates(self):
        return True

    _re_re_mark = re.compile("^\s*Re-Mark:\s*(\S+)", re.MULTILINE)

    def screen(self, row, ticket_id, image):
        if 'UPDT' in row['ticket_type']:
            m = self._re_re_mark.search(image)
            return m and m.group(1) == 'N'
        return False


class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    # this call center doesn't actually check the responses
    acceptable_codes = ["250 ok"]
    codes_ok = ['successful', '250 ok', 'response posted']
    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = 'USAS'

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
        <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>UTI</Locator_Name>
        <Note>%(notes)s</Note>
        </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        date_time = self.row['closed_date']
        if date_time is None:
            # No closed date, set to now
            date_time = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based"\
                               " on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based"\
                               " on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': date_time.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes': 'Response by Utiliquest'
        })
        self.log.log_event("Location date/time injected into xml: %s" %
         date_time)
        revision = names['revision']
        names['revision'] = escape(string.join([c for c in revision
                                                if c in string.digits], ''))

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        return True


class EmailResponderData(NCA1.EmailResponderData):
    # same as NCA1
    update_call_center = ['SCA6', 'SCA8', 'SCA9']

registry = {
    ('SCA1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('SCA1', 'email', ''): EmailResponderData,
}

