# c3002.py

import c300

class BusinessLogic(c300.BusinessLogic):
    call_center = '3002'
    # 3002 is an update call center, so this class is mostly a dummy

    def allow_summary_updates(self):
        return True

    # As of Oct 2005, these summaries have no headers.  As a fix, we're going
    # to pretend they do have headers (otherwise they would be considered
    # "incomplete"), and allow summary updates.

registry = {}

