# SCA6.py

import SCA1
import duedates

class BusinessLogic(SCA1.BusinessLogic):
    call_center = 'SCA6'

    update_due_dates = True
    # Mantis 2828: due dates on SCA6 tickets override existing ones

    legal_due_date = duedates.due_date_CA_ATT

registry = {}

