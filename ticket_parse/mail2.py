# mail2.py
# Updated version of mail.py.
# (mail.py will be phased out, then this should probably be renamed to mail.py
# again).

import base64
import getopt
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import emailtools
import os
import smtplib
import string
import socket
import sys
import time
import tools
import traceback
import version
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

SMTP_TIMEOUT = 7

TESTING = 0  # XXX DEPRECATED in mail2?
# set to 1 if we're testing, in which case no actual mail will be sent

_test_mail_sent = [] # XXX DEPRECATED in mail2?

def _time():
    return time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())

last_timeout_error = ""

# This should be the only place in code that creates an SMTP instance and
# sends mail.  All other mail-sending code should use this (probably
# indirectly via sendmail() or sendmail_multipart()).
#
def _sendmail(smtpinfo, toaddrs, body):
    toaddrs = tools.remove_duplicates(toaddrs)
    if TESTING:
        # append to list for possible inspection
        _test_mail_sent.append([smtpinfo, toaddrs, body])
    else:
        timeout = socket.getdefaulttimeout()
        socket.setdefaulttimeout(SMTP_TIMEOUT)
        try:
            smtp_class = emailtools.get_SMTP_class(smtpinfo.use_ssl)
            server = smtp_class(smtpinfo.host, smtpinfo.port)
            try:
                if smtpinfo.password:
                    server.login(smtpinfo.user, smtpinfo.password)
                server.sock.settimeout(SMTP_TIMEOUT)
                server.sendmail(smtpinfo.from_address, toaddrs, body)
            finally:
                server.quit()
        finally:
            socket.setdefaulttimeout(timeout)

def _sendmail_with_timeout_warning(smtpinfo, toaddrs, body):
    try:
        _sendmail(smtpinfo, toaddrs, body)
    except (socket.timeout, socket.gaierror):
        # todo(dan) Is the original email lost, and is it a problem? What about
        # email responders?
        send_timeout_warning()

def sendmail(smtpinfo, toaddrs, subject, body, add_date_header=1, alternate=0,
 send_failure_warning=1):
    """ Send email.
        <smtpinfo> contains the SMTP account info (incl. sender address).
        <toaddrs> is a list of email addresses that the mail will be sent to.
        <subject> is the email's subject, of course.
        <body> is the email's body in plain text.
        <alternate> will be 1 if sendmail is called by send_timeout_warning().
    """
    toaddrs = tools.remove_duplicates(toaddrs)
    date_header = ""
    if add_date_header:
        date_header = "Date: %s\r\n" % (_time(),) # use GMT

    msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\n%s\r\n%s" % (
          smtpinfo.from_address,
          string.join(toaddrs, ","),
          subject,
          date_header,
          body)

    if alternate or not send_failure_warning:
        _sendmail(smtpinfo, toaddrs, msg)
    else:
        _sendmail_with_timeout_warning(smtpinfo, toaddrs, msg)

def send_timeout_warning():
    """ Attempt to send an email server timeout warning, using an alternate
        SMTP server, if any. """
    import config
    cfg = config.getConfiguration()
    smtpinfo = cfg.smtp_accs[0]
    if len(cfg.smtp_accs) >= 2:
        print >> sys.stderr, "** Sending email server timeout notification **"
        # don't actually switch SMTP servers, for now
        smtpinfo = cfg.smtp_accs[1]
        toaddrs = [d['email'] for d in cfg.admins]
        warning_body = "Warning: SMTP server %r timed out, possible problem" % (
                       smtpinfo,)
        sendmail(smtpinfo, toaddrs, "SMTP server warning",
                 warning_body, add_date_header=1, alternate=1)
    else:
        print "** No alternate SMTP server available **"

    # store last timeout error
    global last_timeout_error
    c = StringIO()
    traceback.print_exc(file=c)
    last_timeout_error = c.getvalue()
    print "Setting last timeout error"

def sendmail_ex(smtpinfo, toaddrs, subject, body, *args, **kwargs):
    try:
        sendmail(smtpinfo, toaddrs, subject, body, *args, **kwargs)
    except:
        return 0
    return 1

def sendmail_multipart(smtpinfo, toaddrs, subject, body, multiparts=[],
                       add_date_header=1, alternate=0):
    msg = _create_multipart_message(smtpinfo.from_address, toaddrs, subject,
           body, multiparts, add_date_header)

    if alternate:
        _sendmail(smtpinfo, toaddrs, msg)
    else:
        _sendmail_with_timeout_warning(smtpinfo, toaddrs, msg)

def _create_multipart_message(fromaddr, toaddrs, subject, bodytext,
                              multiparts=[], add_date_header=1):
    """ Create a multipart message.  Return message as an instance of StringIO.
        <multiparts> is a list of tuples (filename, body, content_type,
        encoding).
        For example:
         ('blah.jpg', '<...image here...>', 'image/jpeg', 'base64')
    """
    toaddrs = tools.remove_duplicates(toaddrs)

    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = COMMASPACE.join(toaddrs)
    if add_date_header:
        msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    # start off with a text/plan part
    msg.attach(MIMEText(bodytext))

    for (filename, bodybinary, content_type, encoding) in multiparts:
        # NOTE: apparently bodybinary is no longer used; the file mentioned
        # must exist, and its contents will be fetched

        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(filename,"rb").read() )
        if encoding == 'base64':
            Encoders.encode_base64(part)
        elif not encoding:
            Encoders.encode_noop(part)
        else:
            raise ValueError, "Encoding '%s' not supported" % (encoding,)

        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                       % os.path.basename(filename))
        msg.attach(part)

    return msg.as_string()

#
# MailServerInfo class

# XXX REDUNDANT/OBSOLETE? covers much the same as SMTPInfo...
class MailServerInfo:
    # XXX does this need use_ssl?

    def __init__(self, host, from_addr, subject):
        self.host = host
        self.from_addr = from_addr
        self.subject = subject

    @classmethod
    def from_smtpinfo(cls, smtpinfo, subject):
        return cls(smtpinfo.host, smtpinfo.from_address, subject)

    def reconfigure(self, config):
        """ Set new values for SMTP server, using a Configuration instance. """
        self.host = config.smtp_accs[0].host
        self.from_addr = config.smtp_accs[0].from_address

#
# MailBag class

# The idea is, to move code/methods from errorhandling.ErrorHandler to MailBag.
# MailBag basically queues mail, but I suppose it can also send mail
# directly.  (After all, direct send is the same as "queue one + send".)

class MailBag:

    def __init__(self, emails, mailserverinfo):
        self.emails = tools.remove_duplicates(emails)
        self.mailserverinfo = mailserverinfo
        self.data = []
        self.num_emails_sent = 0
        self.num_parts_sent = 0

    def add(self, data):
        self.data.append(data)

    def force_send(self):
        if not self.data:
            return  # nothing to send

        self.emails = tools.remove_duplicates(self.emails)
        body = self.make_body()
        msinfo = self.mailserverinfo
        smtpinfo = emailtools.SMTPInfo(host=msinfo.host,
                                       from_address=msinfo.from_addr)
        sendmail(smtpinfo, self.emails, msinfo.subject, body)
        self.num_emails_sent += 1
        self.num_parts_sent += len(self.data)

        return body # mostly for debugging/testing

    def clear_queue(self):
        self.data = []

    def _version_line(self):
        line = "(Version: %s)" % (version.__version__,)
        return line

    def make_body(self):
        """ Join all data together to one large string, suitable for sending
            by email. """
        body = string.join(self.data, "!\n")
        body = self._version_line() + "\n\n" + body
        return body

    def __len__(self):
        return len(self.data)


if __name__ == "__main__":

    ___usage__ = """\
mail2.py options

Options:
    -h host
    -f from_address
    -t addr1;addr2;...
    -a filename;content_type;encoding
    -b body
    -s subject
    """
    # TODO: add -p port, -S use_ssl?

    opts, args = getopt.getopt(sys.argv[1:], "a:b:f:h:p:s:t:")

    subject = "mail.py multipart test"
    fromaddr = "test@zephyrfalcon.org"
    body = "This is a test..."
    port = 25
    host = 'smtp.example.com'

    for o, a in opts:
        if o == "-h":
            host = a
        elif o == "-f":
            fromaddr = a
        elif o == "-t":
            toaddrs = a.split(";")
        elif o == "-a":
            filename, content_type, encoding = a.split(";")
        elif o == "-b":
            body = a
        elif o == "-s":
            subject = a
        elif o == "-p":
            port = int(a)

    data = open(filename, 'rb').read()
    smtpinfo = emailtools.SMTPInfo(host, port, fromaddr)
    # FIXME: add use_ssl
    sendmail_multipart(smtpinfo, toaddrs, subject, body,
     multiparts=[(filename, data, content_type, encoding)])

    print "Email sent to", toaddrs

