# transaction.py
"""
A generic transaction object ("context manager") that works with both new
DBInterface objects and old DBInterfaceADO objects.

Usage:

    with Transaction(db):
        # do stuff...

Further reading: http://www.python.org/doc/2.5/whatsnew/pep-343.html
"""

from __future__ import with_statement
import dbinterface # new
import dbinterface_ado

class Transaction(object):

    def __init__(self, db, suppress=False):
        """ Start a new transaction context with the given db object. If
            suppress is true, any exceptions will be suppressed; this is
            mainly for testing purposes!
        """
        self.db = db
        self.suppress = suppress # suppress exception

    def __enter__(self):
        self.db.begin_transaction()

    def __exit__(self, type, value, tb):
        if tb is None:
            # everything ok so far
            try:
                self.db.commit_transaction()
            except:
                self.db.rollback_transaction()
                raise
        else:
            self.db.rollback_transaction()
            # note: exception will be re-raised
        return self.suppress


