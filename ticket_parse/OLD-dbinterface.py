# dbinterface.py
# My second attempt at writing an interface module for SQLXML servers. Based
# on Kyle's dbinterface.py and my own dbinterface2.py (both of which are now
# obsolete).
#
# This module provides a class (DBInterface) and various useful helper
# functions for manipulating data in SQL Server databases, using SQLXML and
# updategrams. Knowledge of the XML DOM is always useful but not required to
# use these functions.
#
# Created: 10 Feb 2002, Hans Nowak
# Last update: 11 Mar 2002, Hans Nowak

import inspect
import os
import string
import sys
import time
import types
from xml.dom.minidom import Element
#
import config
import tools

'''
import exceptions
class DBException(exceptions.Exception):
    def __init__(self, value, **kwargs):
        self.value = value
        try:
            self.data = kwargs['data']
        except:
            self.data = ""
        try:
            self.sql = kwargs['sql']
        except:
            pass
        try:
            self.timeout = kwargs['timeout']
        except:
            pass

        self.args = (value,)    # don't display 'data' in the traceback
'''

class DBException(Exception):
    def __init__(self, msg, **kwargs):
        self.msg = msg
        self.data = kwargs # dictionary of additional info
        self.args = [msg]

class DBFatalException(DBException):
    """ A fatal database exception, e.g. a network error. """

class TimeoutError(DBException):
    """ DBException caused by a timeout. """

###
### DBInterface class

class DBInterface:
    """ Generic interface class to talk to a database supporting SQLXML. """

    def __init__(self):
        self._lastresult = ""
        # Last unparsed XML retrieved by runsql.
        self.config = config.getConfiguration()

        # timer stuff
        self._start = 0
        self._stop = 0
        self.timed_events = []


    ###
    ### built-in timer

    def starttimer(self):
        self._start = time.time()
        self._stop = 0

    def stoptimer(self):
        assert self._start, "Timer stopped but not started"
        self._stop = time.time()

    def register_timer(self, method, info):
        frames = inspect.stack()[1:]
        frame_info = [f[1:-1] for f in frames]
        t = (self._start, self._stop, method, info, frame_info)
        self.timed_events.append(t)

    ###
    ### a simple "report" function

    # NOTE: This code may better be moved to a separate module or class.

    def simple_profiler(self, f=sys.stdout):
        for i in range(len(self.timed_events)):
            event = self.timed_events[i]
            start, stop, method, info, frame_info = event
            s = "Event %d: started %s, stopped %s, duration %.2f sec" % (
             i+1,
             "%02d:%02d:%02d" % (time.localtime(start)[3:6]),
             "%02d:%02d:%02d" % (time.localtime(stop)[3:6]),
             stop - start,
            )
            print >> f, s
            print >> f, "Type:", method
            print >> f, "Called with:", info[:100],
            print >> f, len(info) > 100 and "..." or ""
            print >> f, "Called by:"
            for frame in frame_info:
                path, fname = os.path.split(frame[0])
                print >> f, " - %s, line %s: %s" % (fname, frame[1], frame[2])
            print >> f
        print >> f, "Total time spent in database: %.2f seconds" % (
         self.time_elapsed())

    def simple_profiler_csv(self, filename):
        """ Write a tab-delimited file with profiler results. """
        f = open(filename, "wb")
        for i in range(len(self.timed_events)):
            event = self.timed_events[i]
            start, stop, method, info, frame_info = event
            print >> f, "%02d:%02d:%02d" % (time.localtime(start)[3:6]), "\t",
            print >> f, "%02d:%02d:%02d" % (time.localtime(stop)[3:6]), "\t",
            print >> f, stop - start, "\t",
            print >> f, info, "\t",
            callers = [frame[2] for frame in frame_info]
            print >> f, string.join(callers, ", ")
        f.close()
        print "CSV-file", filename, "written."

    def time_elapsed(self):
        """ Compute the time elapsed for all the events that are currently in
            the timed_events list. """
        elapsed = reduce(lambda a, b: a+b,
         [e[1] - e[0] for e in self.timed_events], 0)
        return elapsed



### ------------------------------------------------------------------------
### Helper functions -- very important!!

def createTextNode(text):
    """ Creates a Text node and sets its data (text).  Works in both
        Python 2.2 (where Text takes one argument), and 2.3 (where Text
        takes no arguments). """
    from xml.dom.minidom import Text
    try:
        t = Text(text)
    except TypeError:
        t = Text()
        t.data = text
    return t

def issequence(x):
    """ Checks if object x is a sequence-- for our purposes, not for general
        purposes. That is, in the Pythonic sense of the word, there are
        other sequences, and if necessary they can be added here, but for now
        we will only support lists and tuples. (Because it's not general,
        it's here and not in the tools module.)
    """
    return type(x) in (types.ListType, types.TupleType)

def xmlrecord(name, **kwargs):
    """ Create an XML tag.
        Note: None values are ignored (see xmlobject).
    """
    e = Element(name)
    for key, value in kwargs.items():
        if value is not None:
            #e.setAttribute(key, str(value))
            e.setAttribute(key, tools.ado_to_string(value))
    return e

def elem2dict(element):
    """ Construct a dict from a DOM Element's attributes. This allows us
        to easily create custom objects/instances from XML. """
    # Caveat: It's possible that such a method already exists.
    d = {}
    for key, value in element.attributes.items():
        d[key] = value
    return d

