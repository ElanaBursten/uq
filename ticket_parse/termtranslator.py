# termtranslator.py
# Created: 2003.07.11

import string
#
import locate
import ticketparser

class TermTranslator:

    def __init__(self, filename=""):
        self.data = {}
        if filename:
            self._read(filename)

    def _read(self, filename):
        """ Read the term-translations file.  This is a whitespace-delimited
            file with the following format:

                call_center   term_id   new_term_ids

            where new_term_ids is a semicolon-separated list of term ids that
            replace the original term.  For example:
            Atlanta  FOO  FOO;BAR      (adds BAR if FOO is found)
            Atlanta  A    B            (replaces A with B)
            FCO1     C    D;E          (replaces C with D and E)

            The translation data are stored in a nested dictionary: first a
            dict of call centers, containing dicts per term id.
        """
        for line in file(filename, "r"):
            line = line.strip()
            if not line:
                continue # skip
            elif line.startswith('#'):
                continue    # skip
            try:
                call_center, before, after = string.split(line)
            except ValueError:
                pass
            else:
                try:
                    d = self.data[call_center]
                except KeyError:
                    d = self.data[call_center] = {}
                after_ids = string.split(after, ";")
                d[before] = after_ids

    def get_translations(self, call_center):
        """ Return a translation dict for the given call center. """
        return self.data.get(call_center, {})

    def translate(self, ticket):
        """ Translate the locates of the given ticket. """
        call_center = ticket.ticket_format
        translations = self.get_translations(call_center)
        newlocates = []
        for loc in ticket.locates:
            try:
                spam = translations[loc.client_code]
            except KeyError:
                newlocates.append(loc)
                # no translation for this term
            else:
                newlocates.extend([locate.Locate(n) for n in spam])

        ticket.locates = newlocates

    def translate_summary(self, summary):
        call_center = summary.call_center
        translations = self.get_translations(call_center)
        try:
            spam = translations[summary.client_code]
        except KeyError:
            pass # not found
        else:
            if spam:
                summary.client_code = spam[0]

