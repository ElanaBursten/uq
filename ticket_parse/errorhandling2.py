# errorhandling2.py
# Updated version of errorhandling.py, meant to replace the old version
# stepwise without breaking all existing code.
#
# A generic error logging system. Neither the ErrorPacket or the ErrorHandler
# class know anything about tickets, the database, or the configuration.

import traceback
import StringIO
import socket
import os
import sys
import string
import time
#
import config
import datadir
import mail2
import tools
import version

def is_database_glitch(errormsg):
    ERRORS = ('Timeout expired',
              'was deadlocked on lock resources')
    return tools.find_any(errormsg, ERRORS)

def get_exception_message():
    """ Return the message of the current exception. """
    e =  sys.exc_info()[1]
    if e and hasattr(e, 'args'):
        return str(e.args[0])
    else:
        return ""


class ErrorPacket:
    """ Simple container class for error info, and other data that could be
        relevant for solving the cause of the error.
    """
    def __init__(self, traceback="", **kwargs):
        self.traceback = traceback
        self.time = time.asctime(time.localtime(time.time()))
        self.data = {}
        self.add(**kwargs)
        self.exception = None
    def __repr__(self):
        s = ("traceback", "time") + tuple(self.data.keys())
        return "ErrorPacket%s" % (str(s))
    def add(self, **kwargs):
        for key, value in kwargs.items():
            self.data[key] = value
    def set_traceback(self):
        """ If there is a current traceback, set self.traceback to the
            (complete) output of this traceback. Only guaranteed to work if an
            exception occurred.
        """
        s = StringIO.StringIO()
        traceback.print_exc(file=s)
        self.traceback = s.getvalue()
        s.close()
        self.exception = sys.exc_info()[1]
        # what happens if no exception was raised?

def errorpacket(**kwargs):
    """ Create an errorpacket with traceback and the given args, and return
        it. Mostly a shorthand for a common idiom. """
    ep = ErrorPacket()
    ep.set_traceback()
    ep.add(**kwargs)
    return ep

class Logger:
    def __init__(self, logdir, name, keep=False):
        self.logdir = logdir
        self.name = name
        # logging directory is relative to data dir.  DataDir object will also
        # make sure the logging dir exists.
        self.logfile = datadir.datadir.open(self._logfilename(), "a+")

        # if keep is True, then logged lines will be added to self.logged. for
        # debugging purposes. normally keep will be False.
        self.logged = []
        self.keep = keep

    def _logfilename(self):
        """ Generate a log file name. """
        t9 = time.localtime(time.time())
        s = "%04d-%02d-%02d.txt" % t9[:3]
        s = self.name + "-" + s
        s = os.path.join(self.logdir, s) # prepend logdir path
        return s

    def write_line(self, line, dump=0):
        logline = "[%s] %s" % (tools.time_string(), line)
        print >> self.logfile, logline
        if self.keep:
            self.logged.append(logline)
        self.logfile.flush()
        if dump:
            print line


class ErrorHandler:
    """ System to store and report errors. Reporting can be done in various
        ways, e.g. by notifying an administrator by email, writing the error
        information to a file, dumping it to sys.stderr, etc.

        If defer_mail is set, mail will be queued until force_send is
        called.
    """

    def __init__(self, logdir="logs", smtpinfo=None, me="logger",
                 admin_emails=(), cc_emails={}, subject="Error Notification"):

        self.admins = admin_emails
        # This is a list of simple dicts with 'name' and 'email' of admins
        # that need to be notified.

        self.cc_emails = cc_emails # Emails instance

        # make sure we always have SMTP info
        if smtpinfo is None:
            import config
            cfg = config.getConfiguration()
            smtpinfo = cfg.smtp_accs[0]

        self.smtpinfo = smtpinfo # used for sending notification emails
        self.subject = subject
        self.mailserverinfo = mail2.MailServerInfo(smtpinfo.host,
                              smtpinfo.from_address, self.subject)

        self.me = me # will be prepended to filename
        self.logger = Logger(logdir, me)

        self.lock = 0
        # Set this if you don't want the log to work, e.g. during certain
        # tests. This stops the email notification.

        self.errorbag = mail2.MailBag([a["email"] for a in self.admins],
                        self.mailserverinfo)
        self.warning_mail_queue = []    # may not be used
        self.cc_bags = {}
        self.defer_mail = 0
        # Set this if you want notification mail to be put in a queue,
        # rather than sending it immediately. You must then call force_send
        # to send the contents of the queue and empty it.

        # create "line noise" MailBag
        ln_admins = [a['email'] for a in self.admins if a.get('ln') == '1']
        self.linenoisebag = mail2.MailBag(ln_admins, self.mailserverinfo)

        self.verbose = 1       # can we print to the screen?
        self.switched_smtp = 0 # indicates whether we switched to an
                               # alternate SMTP server, if any

        # HACK to keep the last timeout error around
        self.last_timeout_error = ""

    ###
    ### logging errors, events and messages

    def log(self, packet, call_center=None, dump=0, send=0, write=0):
        """ Log an error. """
        if dump:
            self._dumptofile(packet, sys.stderr)
        if write:
            self._dumptofile(packet, self.logger.logfile)
        if send and not self.lock:
            try:
                self.handle_email(packet, call_center=call_center)
            except socket.timeout:
                self.log_event("** Could not send email (timeout) **")

    log_error = log

    def log_event(self, s, dump=1):
        """ Write an "event" (a string) to the log file. This does not count
            as an error.
        """
        self.logger.write_line(s, dump=dump and self.verbose)

    def log_warning(self, obj, call_center=None, subject_hint="", forward=[],
                    dump=0, write=0, send=1):
        """ Send an email warning to the administrator(s). This does not count
            as an error. """
        if isinstance(obj, ErrorPacket):
            ep = obj
        else:
            ep = errorpacket(warning=obj)
        if not ep.traceback:
            del ep.traceback

        # a warning is important enough to write and/or dump:
        if write:
            self._dumptofile(ep, self.logger.logfile, error = 0)
        if dump and self.verbose:
            self._dumptofile(ep, sys.stderr, error = 0)

        try:
            self._sendemail(ep, call_center=call_center,
                            subject_hint=subject_hint, error=0, forward=forward)
        except socket.timeout:
            self.log_event("** Could not send email (timeout) **")
        # send as a warning
        # currently, we do not queue warnings. that would require a separate
        # mail queue, by the way, unless we want errors and warnings to be
        # mingled...

    ###
    ### helper functions

    def _dumptofile(self, packet, f, error=1):
        """ Write an error packet to a file(-like) object. """
        self._writeerrorheader(f, error=error)
        for attr in ("time", "traceback"):
            if hasattr(packet, attr):
                value = getattr(packet, attr)
                self._writeheader(attr, f)
                print >> f, value

        # if the exception has a 'data' attribute, assumed to be a dict, then
        # we include the contents of that dict:
        if packet.exception:
            d = getattr(packet.exception, 'data', {})
            for key in sorted(d.keys()):
                value = d[key]
                self._writeheader(key, f)
                print >> f, value

        # include 'data' attribute as well:
        for key, value in packet.data.items():
            self._writeheader("data." + key, f)
            print >> f, value
        print >> f, "-"*75
        print >> f
        f.flush()

    def packet_as_string(self, packet, error=1):
        s = StringIO.StringIO()
        self._dumptofile(packet, s, error)
        body = s.getvalue()
        s.close()
        return body

    def _writeheader(self, name, f):
        print >> f, "-"*75
        print >> f, name + ":"
        print >> f, "-" * (len(name)+1)

    def _writeerrorheader(self, f, error=1):
        t9 = time.localtime(time.time())    # a 9-tuple
        s = " %s " % ["Warning", "Error"][error]
        s = s + "at %04d-%02d-%02d %02d:%02d:%02d " % t9[:6]
        s = "-"*22 + s + "-"*23
        print >> f, s

    def handle_email(self, packet, call_center=None):
        if self.defer_mail:
            self._queuemail(packet, call_center=call_center)
        else:
            self._sendemail(packet, call_center=call_center)

    def _sendemail(self, packet, call_center=None, subject_hint="", error=1,
                   forward=[]):
        """ Send the error packet as email to the registered administrators.
            If 'error' is false, it's considered a warning. """
        if (not self.smtpinfo) or (not self.admins) or (self.lock):
            return
        # create message with headers and body
        if call_center:
            toaddrs = None
            try:
                if error:
                    toaddrs = self.cc_emails[call_center].admin
                else:
                    toaddrs = self.cc_emails[call_center].warning
            except:
                pass
            # if nothing found, fall back to global admin emails
            if not toaddrs:
                toaddrs = [a["email"] for a in self.admins]
        else:
            toaddrs = [a["email"] for a in self.admins]

        # add 'forward' email addresses as well
        toaddrs.extend(forward)

        if not toaddrs:
            print "_sendemail: No destination emails found"
            return

        if error:
            #subject = self.me + "Error notification"
            subject = self.subject
            pass
        else:
            subject = self.me + " warning"
        if subject_hint:
            subject = subject + " (" + subject_hint + ")"

        body = self.packet_as_string(packet, error=0)
        if not error:   # it's a warning
            body = self._version_line() + "\n\n" + body

        # send message
        try:
            mail2.sendmail(self.smtpinfo, toaddrs, subject, body)
        except:
            print "Could not send mail."
            # This could probably be logged too... meta-logging... but not
            # through mail, obviously.
            ep = errorpacket()
            ep.add(smtpinfo=self.smtpinfo, toaddrs=toaddrs, msg=body)
            self.log(ep, write=1, dump=1 and self.verbose)   # but not send=1!
        else:
            self.log_event("Mail notification sent to %s" % (repr(toaddrs)[1:-1],))
            self.last_timeout_error = ""

    def __try_smtp_switch(self, ep):
        if not self.switched:
            import config
            cfg = config.getConfiguration()
            if cfg.switch_smtp_servers():
                self.switched = 1
                self.smtpinfo = cfg.smtp_accs[0]
                # send error packet again, with new server, as a warning
                self.log_warning(ep, subject_hint="email server problem",
                 send=1, dump=1, write=1)

    def _queuemail(self, packet, call_center=None):
        """ Queue a mail packet for the given call center, or for the general
            queue. """
        if self.lock:
            return
        body = self.packet_as_string(packet)
        is_linenoise = packet.data.get('linenoise_info', 0) and not call_center
        # there is only one 'line noise' MailBag, we don't have one per call
        # center

        # make sure it's queued in the right place
        if call_center:
            if not self.cc_bags.has_key(call_center):
                self.cc_bags[call_center] = \
                 mail2.MailBag(self.cc_emails[call_center].admin,
                              self.mailserverinfo)
            self.cc_bags[call_center].add(body)
        elif is_linenoise:
            self.linenoisebag.add(body)
        else:
            self.errorbag.add(body)

    def _force_send(self, mailbag, call_center=None):
        """ Force sending of mail in queue. """
        if not self.smtpinfo or not mailbag.emails:
            return
        if self.lock:
            return
        if not mailbag.data:
            return  # nothing to send

        # create message with headers and body
        toaddrs = mailbag.emails
        if not toaddrs:
            print "_force_send: No destination emails found"
            return

        # send message
        try:
            mailbag.force_send()
        except:
            print "Could not send mail."
            # This could probably be logged too... meta-logging... but not
            # through mail, obviously.
            ep = errorpacket(smtpinfo=self.smtpinfo,
                 toaddrs=toaddrs, body=mailbag.make_body())
            self.log(ep, write=1, dump=1)   # but not send=1
        else:
            if call_center: print call_center,
            #print "Mail notification sent to", repr(toaddrs)[1:-1],
            #print "(%d packets)" % (len(mailbag),)
            self.log_event("Mail notification sent to %s (%d packets)" % (
             repr(toaddrs)[1:-1], len(mailbag)))

        mailbag.clear_queue()

    def force_send(self):
        """ Force a send of everything in the various queues.
            Can now be called regardless whether there's something in any
            queue. The _force_send helper method deals with this.
        """
        # force send for all call centers
        for call_center, bag in self.cc_bags.items():
            self._force_send(bag, call_center)
        # force send for general queue
        self._force_send(self.errorbag, None)
        self._force_send(self.linenoisebag, None)

    def _version_line(self):
        line = "(Version: %s)" % (version.__version__,)
        return line

    # __del__ is generally a bad idea
    #def __del__(self):
        ## this doesn't always seem to work, e.g. with the responder...
        #self.force_send()


