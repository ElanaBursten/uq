# geocoder.py

import os
import sys
import time

# todo(dan) What if another version of requests is installed? Should lib be
# inserted at the beginning?
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib'))

import requests

import errorhandling2

"""
The geocoder uses the Google geocoder API. Google's documentation is here:
https://developers.google.com/maps/documentation/geocoding/#GeocodingRequests

Google statuses:
successful:
* "OK" - indicates that no errors occurred; the address was successfully parsed
  and at least one geocode was returned.
* "ZERO_RESULTS" - indicates that the geocode was successful but returned no
  results. This may occur if the geocode was passed a non-existent address or a
  latlng in a remote location.

unsuccessful, and shouldn't be immediately retried:
* "OVER_QUERY_LIMIT" - indicates that you are over your quota.
* "REQUEST_DENIED" - indicates that your request was denied, generally because
  of lack of a sensor parameter.
* "INVALID_REQUEST" - generally indicates that the query (address or latlng) is
  missing.

unsuccessful, and can be immediately retried:
* UNKNOWN_ERROR - indicates that the request could not be processed due to a
  server error. The request may succeed if you try again.

Google location_type codes (translated to precision):
* "ROOFTOP" - indicates that the returned result is a precise geocode for which
   we have location information accurate down to street address precision.
* "RANGE_INTERPOLATED" - indicates that the returned result reflects an
   approximation (usually on a road) interpolated between two precise points
   (such as intersections). Interpolated results are generally returned when
   rooftop geocodes are unavailable for a street address.
* "GEOMETRIC_CENTER" - indicates that the returned result is the geometric
  center of a result such as a polyline (for example, a street) or polygon
  (region).
* "APPROXIMATE" - indicates that the returned result is approximate
"""

geocode_url = 'https://maps.googleapis.com/maps/api/geocode/json'
no_retry_statuses = [400, 401, 403, 405, 406, 410, 411, 413, 414, 422, 426, 429,
 501] # todo(dan) Review this. Add more statuses?

precision_translations = {'ROOFTOP': 50, 'RANGE_INTERPOLATED': 40,
 'GEOMETRIC_CENTER': 30,'APPROXIMATE': 20, '': 10}

class Geocoder:
    def __init__(self, geocoder_config, log):
        self.geocoder_config = geocoder_config
        self.log = log

    def log_msg(self, message, message_args=(), google_error_msg=''):
        msg = message % message_args
        if google_error_msg:
            msg += '\nGoogle error message: ' + google_error_msg
        self.log.log_event(msg)

    def log_warning(self, message, message_args=(), google_error_msg='',
     subject_hint='', send=1):
        msg = message % message_args
        if google_error_msg:
            msg += '\nGoogle error message: ' + google_error_msg
        ep = errorhandling2.errorpacket()
        ep.add(info=msg)
        self.log.log_warning(ep, send=send, write=1, dump=1,
         subject_hint=subject_hint)

    def lat_long(self, address):
        """
        Returns:
        * geocoded - True if geocoding was successful, even if 0 matches
        * matches - List of locations, if geocoded is True
        """

        def add_match(raw_match):
            try:
                location_type = raw_match['geometry']['location_type']
            except KeyError:
                precision = precision_translations['']
                self.log_msg('Location type not found in geocode match. ' +
                 'Precision code set to %d (unknown).', (precision))
            else:
                try:
                    precision = precision_translations[location_type]
                except KeyError:
                    precision = precision_translations['']
                    self.log_msg('Unknown location type: "%s" in geocode ' +
                     'match. Precision code set to %d (unknown).',
                     (location_type, precision))

            try:
                lat = raw_match['geometry']['location']['lat']
                lng = raw_match['geometry']['location']['lng']
            except KeyError:
                self.log_msg('Latitude and/or longitude not found in geocode ' +
                 'match. Match skipped.')
                return

            try:
                float(lat)
                float(lng)
            except ValueError:
                self.log_msg('Latitude and/or longitude in geocode match, are' +
                 ' not valid floating point numbers. Match skipped.')
                return

            matches.append({'latitude': lat, 'longitude': lng,
             'precision': precision})

        geocoded = False         
        matches = []
        
        for attempt in range(1, 2 + self.geocoder_config['max_retries']):
            if attempt > 1:
                # todo(dan) replace with a loop including the listener?            
                time.sleep(self.geocoder_config['retry_wait_seconds'])
            self.log_msg('Attempt %d to geocode: %s', (attempt, address))

            response = requests.get(geocode_url, params={'address': address,
             'sensor': 'false'},
             timeout=self.geocoder_config['timeout_seconds'],
             allow_redirects=True, verify=True, stream=False)

            if response.status_code in no_retry_statuses:
                self.log_msg('Geocoding failed with HTTP status: %d. Not ' +
                 'retrying.', (response.status_code))
                break
                
            if 200 <= response.status_code < 300:
                try:
                    results = response.json()
                except ValueError:
                    self.log_msg('Geocoding HTTP status: %d, but no JSON ' +
                     'result returned', (response.status_code))
                    continue

                try:
                    status = results['status']
                except KeyError:
                    self.log_msg('Geocoding HTTP status: %d, but status not ' +
                     'found in JSON result', (response.status_code))
                    continue

                error_msg = results.get('error_message', '')

                try:
                    raw_matches = results['results']
                except KeyError:
                    self.log_msg('Geocoding HTTP status: %d, result status: ' +
                     '"%s", but matches not found in JSON result',
                     (response.status_code, status), error_msg)
                    continue

                if status == 'ZERO_RESULTS':
                    if len(raw_matches) == 0:
                        geocoded = True
                        self.log_msg('Geocoding successful, but no matches ' +
                         'returned.', (), error_msg)
                        break
                elif status == 'OK':
                    for raw_match in raw_matches:
                        add_match(raw_match)
                    geocoded = True
                    self.log_msg('Geocoding successful. %d matches returned.',
                     (len(matches)))
                    break
                elif status == 'OVER_QUERY_LIMIT':
                    self.log_warning('Geocode result status: "%s". Will not ' +
                     'retry.', (status), error_msg,
                     'Google geocode service over query limit')
                    break
                elif status in ['REQUEST_DENIED', 'INVALID_REQUEST']:
                    self.log_msg('Geocode result status: "%s". Will not retry.',
                     (status), error_msg)
                    break
                else:
                    self.log_msg('Unknown geocode result status: "%s"', (status),
                     error_msg)

        return geocoded, matches
        
    def lat_long_from_street_address(self, street_number, street_name, city,
     county, state):
        # todo(dan) is this correct? refactor?
        if not (street_name and state):
            self.log_msg('Cannot geocode street address, without a street ' +
             'name and state (street name: "%s", state: "%s")',
             (street_name, state))
            return False, []
        
        address = street_name
        if street_number:
            address = street_number + ' ' + street_name    
        if city:
            address += ', ' + city
        if county:
            # todo(dan) can county cause problems if city is blank? should
            # 'county' be added (e.g. 'Washtenaw county')?
            address += ', ' + county
        address += ', ' + state
            
        return self.lat_long(address)
        
    def lat_long_from_cross_streets(self, street_name_1, street_name_2, city,
     county, state):
        # todo(dan) is this correct? refactor?
        if not (street_name_1 and street_name_2 and state):
            self.log_msg('Cannot geocode cross streets, without street name 1, ' +
             'street name 2, and state (street name 1: "%s", street name 2: ' +
             '"%s", state: "%s")', (street_name_1, street_name_2, state))
            return False, []
        
        address = street_name_1 + ' and ' + street_name_2
        if city:
            address += ', ' + city
        if county:
            # todo(dan) can county cause problems if city is blank? should
            # 'county' be added (e.g. 'Washtenaw county')?
            address += ', ' + county
        address += ', ' + state
            
        return self.lat_long(address)
