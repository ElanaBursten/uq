unit Hashes;
{Hashes is a freeware utility originally written by Domingo Seoane which uses
 the Cryptography API. This is an adapted version. http://delphi.jmrds.com/?q=node/36}

interface

uses Windows, SysUtils, Classes, OdMiscUtils;

type
  THashAlgorithm = (haMD5, haSHA1);

function CalcHash(Stream: TStream; Algorithm: THashAlgorithm): string; overload;
function CalcHash(Archivo: string; Algorithm: THashAlgorithm): string; overload;
function CalcHash2(Str: string; Algorithm: THashAlgorithm): string;

implementation

type
  HCRYPTPROV = ULONG;
  PHCRYPTPROV = ^HCRYPTPROV;
  HCRYPTKEY = ULONG;
  PHCRYPTKEY = ^HCRYPTKEY;
  HCRYPTHASH = ULONG;
  PHCRYPTHASH = ^HCRYPTHASH;
  LPAWSTR = PAnsiChar;
  ALG_ID = ULONG;

const
  CRYPT_NEWKEYSET = $00000008;
  CRYPT_MACHINE_KEYSET =  $00000020;
  CRYPT_VERIFYCONTEXT = $F0000000;
  PROV_RSA_FULL = 1;
  CALG_MD5 = $00008003;
  CALG_SHA1  = $00008004;
  HP_HASHVAL = $0002;

function CryptAcquireContext(phProv: PHCRYPTPROV;
  pszContainer: LPAWSTR;
  pszProvider: LPAWSTR;
  dwProvType: DWORD;
  dwFlags: DWORD): BOOL; stdcall;
  external ADVAPI32 name 'CryptAcquireContextA';//TODO: This may need to change with unicode upgrade

function CryptCreateHash(hProv: HCRYPTPROV;
  Algid: ALG_ID;
  hKey: HCRYPTKEY;
  dwFlags: DWORD;
  phHash: PHCRYPTHASH): BOOL; stdcall;
  external ADVAPI32 name 'CryptCreateHash';

function CryptHashData(hHash: HCRYPTHASH;
  const pbData: PBYTE;
  dwDataLen: DWORD;
  dwFlags: DWORD): BOOL; stdcall;
  external ADVAPI32 name 'CryptHashData';

function CryptGetHashParam(hHash: HCRYPTHASH;
  dwParam: DWORD;
  pbData: PBYTE;
  pdwDataLen: PDWORD;
  dwFlags: DWORD): BOOL; stdcall;
  external ADVAPI32 name 'CryptGetHashParam';

function CryptDestroyHash(hHash: HCRYPTHASH): BOOL; stdcall;
  external ADVAPI32 name 'CryptDestroyHash';

function CryptReleaseContext(hProv: HCRYPTPROV; dwFlags: DWORD): BOOL; stdcall;
  external ADVAPI32 name 'CryptReleaseContext';

function GetWindowsSystemError(ErrorCode: HRESULT): string;
var
  MsgBuffer: pChar;
begin
  MsgBuffer := nil;
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
    nil, ErrorCode, 0, @MsgBuffer, 0, nil);
  Result := MsgBuffer;
  LocalFree(Cardinal(MsgBuffer));
end;

function StrContainsText(const SubStr, S: string): Boolean;
begin
  Result := Pos(AnsiUpperCase(SubStr), AnsiUpperCase(S)) > 0;
end;

function CalcHash(Stream: TStream; Algorithm: THashAlgorithm): string; overload;
var
  hProv: HCRYPTPROV;
  hHash: HCRYPTHASH;
  Buffer: PByte;
  BytesRead: DWORD;
  Algid: ALG_ID;
  Data: array[1..20] of Byte;
  DataLen: DWORD;
  Success: BOOL;
  i: integer;
  APIError: HResult; 
begin
  Result:= EmptyStr;
  //http://msdn.microsoft.com/en-us/library/windows/desktop/aa379886(v=vs.85).aspx
  //http://msdn.microsoft.com/en-us/library/windows/desktop/aa379886(v=vs.85).aspx
  Success := CryptAcquireContext(@hProv, nil, nil, PROV_RSA_FULL,
    CRYPT_NEWKEYSET or CRYPT_VERIFYCONTEXT Or CRYPT_MACHINE_KEYSET);//CRYPT_MACHINE_KEYSET required for use by services; CRYPT_VERIFYCONTEXT required for hashing; CRYPT_NEWKEYSET needed to make a new keyset (which we are doing)
  if Success then
  begin
    try
      if Algorithm = haMD5 then
      begin
        Algid:= CALG_MD5;
        Datalen:= 16
      end else
      begin
        Algid:= CALG_SHA1;
        Datalen:= 20;
      end;
      if CryptCreateHash(hProv, Algid, 0, 0, @hHash) then
      begin
        GetMem(Buffer,10*1024);
        try
          while  TRUE do
          begin
            BytesRead:= Stream.Read(Buffer^, 10*1024);
            if (BytesRead = 0) then
            begin
              if (CryptGetHashParam(hHash, HP_HASHVAL, @Data, @DataLen, 0)) then
                for i := 1 to DataLen do
                  Result := Result + LowerCase(IntToHex(Integer(Data[i]), 2));
              break;
            end;
            if (not CryptHashData(hHash, Buffer, BytesRead, 0)) then
              break;
          end;
        finally
          FreeMem(Buffer);
        end;
        CryptDestroyHash(hHash);
      end;
    finally
      CryptReleaseContext(hProv, 0);
    end;
  end;
  if (not Success) then begin
    APIError := GetLastError();
    Raise Exception.Create('Hash calculation failed. The Windows error is: ' + IntToStr(APIError) + ': ' + GetWindowsSystemError(APIError));
  end;
end;

function CalcHash(Archivo: string; Algorithm: THashAlgorithm): string; overload;
var
  Stream: TFileStream;
begin
  Result:= EmptyStr;
  if FileExists(Archivo) then
  try
    Stream:= TFileStream.Create(Archivo,fmOpenRead or fmShareDenyWrite);
    try
      Result:= CalcHash(Stream,Algorithm);
    finally
      Stream.Free;
    end;
  except
  end;
end;

function CalcHash2(Str: string; Algorithm: THashAlgorithm): string;
var
  Stream: TStringStream;
begin
  Result:= EmptyStr;
  //TODO: (To be addressed when codebase is moved to XE2/3.)
  //There is a minor security issue using passwords with unicode in D2007.  The
  //characters are translated to ANSI ????.  It is not a critical issue at this
  //time because we do not currently support unicode in D2007.  The default encoding
  // in the constructor TEncoding.Unicode can be changed to unicode. The TEncoding
  //class was introduced in D2009 and will be available to us in XE2/3.
  //http://stackoverflow.com/questions/7497423/undeclared-identifier-tencoding-delphi7
  Stream:= TStringStream.Create(Str);
  try
    Result:= CalcHash(Stream,Algorithm);
  finally
    Stream.Free;
  end;
end;

end.

