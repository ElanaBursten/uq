unit cxTreeListStorage;

interface

uses
  SysUtils, Classes, Contnrs, Variants, XMLDoc, XMLIntf, cxCustomStatusKeeper, cxComponentStorageFactory,
  cxTL, cxDBTL;

type
  TcxTreeNodeInfo = class
  private
    FKeyValues: Variant;
  protected
    IsSelected: Boolean;
    IsFocused: Boolean;
    IsExpanded: Boolean;
  public
    constructor Create; virtual;
    property KeyValues: Variant read FKeyValues write FKeyValues;
  end;

  TcxTreeInfo = class
  private
    FFocusedItemIndex: Integer;
    FTopNodeIndex: Integer;
    FNodeInfos: TObjectList;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    property FocusedItemIndex: Integer read FFocusedItemIndex write FFocusedItemIndex;
    property NodeInfos: TObjectList read FNodeInfos;
    property TopNodeIndex: Integer read FTopNodeIndex write FTopNodeIndex;
  end;

  TcxTreeListStateStorage = class (TcxCustomComponentStateStorage)
  private
    FViewInfo: TcxTreeInfo;
    FLoadExpand: Boolean;
    FLoadSelection: Boolean;
    FLoadFocus: Boolean;
    FLoadFocusedItem: Boolean;
    FLoadTopNode: Boolean;
  protected
    procedure CollapseTreeList;
  public
    constructor Create(AOwner: TcxStatusKeeperStorage); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LoadState; override;
    procedure SaveState; override;
    procedure SaveStateToFile(AParentNode: IXMLNode); override;
    procedure LoadStateFromFile(AParentNode: IXMLNode); override;
  published
    property LoadExpanding: Boolean read FLoadExpand write FLoadExpand default True;
    property LoadSelection: Boolean read FLoadSelection write FLoadSelection default True;
    property LoadFocus: Boolean read FLoadFocus write FLoadFocus default True;
    property LoadFocusedItem: Boolean read FLoadFocusedItem write FLoadFocusedItem default True;
    property LoadTopNode: Boolean read FLoadTopNode write FLoadTopNode default True;
  end;

implementation

{ TcxTreeNodeInfo }

constructor TcxTreeNodeInfo.Create;
begin
  inherited;
  IsSelected := false;
  IsFocused := false;
  IsExpanded := false;
end;

{ TcxTreeInfo }

constructor TcxTreeInfo.Create;
begin
  FNodeInfos := TObjectList.Create(true);
  FFocusedItemIndex := -1;
  FTopNodeIndex := -1;
end;

destructor TcxTreeInfo.Destroy;
begin
  FreeAndNil(FNodeInfos);
  inherited;
end;

{ TcxTreeListStateStorage }

constructor TcxTreeListStateStorage.Create(AOwner: TcxStatusKeeperStorage);
begin
  inherited Create(AOwner);
  FViewInfo := TcxTreeInfo.Create;
  FLoadExpand := true;
  FLoadSelection := true;
  FLoadFocus := true;
  FLoadFocusedItem := true;
  FLoadTopNode := true;
end;

destructor TcxTreeListStateStorage.Destroy;
begin
  FreeAndNil(FViewInfo);
  inherited;
end;

procedure TcxTreeListStateStorage.Assign(Source: TPersistent);
begin
  if Source is TcxTreeListStateStorage then
  begin
    LoadExpanding := TcxTreeListStateStorage(Source).LoadExpanding;
    LoadSelection := TcxTreeListStateStorage(Source).LoadSelection;
    LoadFocus := TcxTreeListStateStorage(Source).LoadFocus;
    LoadFocusedItem := TcxTreeListStateStorage(Source).LoadFocusedItem;
    LoadTopNode := TcxTreeListStateStorage(Source).LoadTopNode;
  end
  else
    inherited Assign(Source);
end;

procedure TcxTreeListStateStorage.CollapseTreeList;
var
  ATreeList: TcxCustomTreeList;
begin
  ATreeList := Owner.Component as TcxCustomTreeList;
  ATreeList.FullCollapse;
end;

procedure TcxTreeListStateStorage.LoadState;
var
  ATreeList: TcxCustomTreeList;
  ANode: TcxTreeListNode;
  ANodeInfo: TcxTreeNodeInfo;
  I: Integer;
begin
  ATreeList := Owner.Component as TcxCustomTreeList;

  ATreeList.BeginUpdate;
  try
    for I := 0 to FViewInfo.NodeInfos.Count - 1 do
    begin
      ANodeInfo := TcxTreeNodeInfo(FViewInfo.NodeInfos[I]);
      if ATreeList is TcxDBTreeList then
        ANode := TcxDBTreeList(ATreeList).FindNodeByKeyValue(ANodeInfo.KeyValues)
      else
        ANode := ATreeList.AbsoluteItems[ANodeInfo.KeyValues];

      if LoadExpanding and ANodeInfo.IsExpanded then
        ANode.Expand(false);
      ANode.Selected := LoadSelection and ANodeInfo.IsSelected;
      ANode.Focused := LoadFocus and ANodeInfo.IsFocused;
    end;

    if LoadFocusedItem then
      if FViewInfo.FocusedItemIndex >= 0 then
        ATreeList.FocusedColumn := ATreeList.Columns[FViewInfo.FFocusedItemIndex]
      else
        ATreeList.FocusedColumn := nil;

    if LoadTopNode then
      if FViewInfo.TopNodeIndex >= 0 then
        ATreeList.TopVisibleNode := ATreeList.AbsoluteItems[FViewInfo.TopNodeIndex]
      else
        ATreeList.TopVisibleNode := nil;
  finally
    ATreeList.EndUpdate;
  end;
end;

procedure TcxTreeListStateStorage.LoadStateFromFile(AParentNode: IXMLNode);
var
  ARootNode, ANode: IXMLNode;
  I: Integer;
  ATreeList: TcxCustomTreeList;
  AVarType: Word;
  ANodeInfo: TcxTreeNodeInfo;
begin
  FreeAndNil(FViewInfo);
  FViewInfo := TcxTreeInfo.Create;
  ATreeList := Owner.Component as TcxCustomTreeList;

  ANode := nil;
  for I := 0 to AParentNode.ChildNodes.Count - 1 do
    if AParentNode.ChildNodes[I].Attributes['ComponentName'] = ATreeList.Name then
    begin
      ARootNode := AParentNode.ChildNodes[I];
      FViewInfo.FocusedItemIndex := StrToInt(ARootNode.Attributes['FocusedItemIndex']);
      FViewInfo.TopNodeIndex := StrToInt(ARootNode.Attributes['TopNodeIndex']);
      break;
    end;

  if ARootNode = nil then
    exit;


  for I := 0 to ARootNode.ChildNodes.Count - 1 do
  begin
    ANodeInfo := TcxTreeNodeInfo.Create;
    FViewInfo.NodeInfos.Add(ANodeInfo);
    ANode := ARootNode.ChildNodes[I];

    AVarType := StrToInt(ANode.Attributes['KeyValueType']);
    ANodeInfo.KeyValues := VarAsType(ANode.Attributes['KeyValue'], AVarType);
    ANodeInfo.IsSelected := StrToBool(ANode.Attributes['IsSelected']);
    ANodeInfo.IsFocused := StrToBool(ANode.Attributes['IsFocused']);
    ANodeInfo.IsExpanded := StrToBool(ANode.Attributes['IsExpanded']);
  end;

  LoadState;
end;

procedure TcxTreeListStateStorage.SaveState;
var
  ATreeList: TcxCustomTreeList;
  ANode: TcxTreeListNode;
  ANodeInfo: TcxTreeNodeInfo;
  I: Integer;
begin
  ATreeList := Owner.Component as TcxCustomTreeList;
  FreeAndNil(FViewInfo);
  FViewInfo := TcxTreeInfo.Create;

  if ATreeList.FocusedColumn <> nil then
    FViewInfo.FocusedItemIndex := ATreeList.FocusedColumn.ItemIndex
  else
    FViewInfo.FocusedItemIndex := -1;

  if ATreeList.TopVisibleNode <> nil then
    FViewInfo.TopNodeIndex := ATreeList.TopVisibleNode.AbsoluteIndex
  else
    FViewInfo.TopNodeIndex := -1;


  for I := 0 to ATreeList.AbsoluteCount - 1 do
  begin
    ANode := ATreeList.AbsoluteItems[I];

    if not (ANode.Selected or ANode.Focused or ANode.Expanded) then
      continue;

    ANodeInfo := TcxTreeNodeInfo.Create;
    if ANode is TcxDBTreeListNode then
      ANodeInfo.KeyValues := TcxDBTreeListNode(ANode).KeyValue
    else
      ANodeInfo.KeyValues := I;

    ANodeInfo.IsSelected := ANode.Selected;
    ANodeInfo.IsFocused := ANode.Focused;
    ANodeInfo.IsExpanded := ANode.Expanded;

    FViewInfo.NodeInfos.Add(ANodeInfo);
  end;
end;

procedure TcxTreeListStateStorage.SaveStateToFile(AParentNode: IXMLNode);
var
  ARootNode, ANode: IXMLNode;
  I: Integer;
  ATreeList: TcxCustomTreeList;
  AViewInfo: TcxTreeNodeInfo;
begin
  SaveState;
  ATreeList := Owner.Component as TcxCustomTreeList;
  ARootNode := AParentNode.AddChild('Component');
  ARootNode.Attributes['ComponentName'] := ATreeList.Name;
  ARootNode.Attributes['FocusedItemIndex'] := FViewInfo.FocusedItemIndex;
  ARootNode.Attributes['TopNodeIndex'] := FViewInfo.TopNodeIndex;

  for I := 0 to FViewInfo.NodeInfos.Count - 1 do
  begin
    AViewInfo := TcxTreeNodeInfo(FViewInfo.NodeInfos[I]);
    ANode := ARootNode.AddChild('Node');

    ANode.Attributes['KeyValue'] := AViewInfo.KeyValues;
    ANode.Attributes['KeyValueType'] := VarType(AViewInfo.KeyValues);
    ANode.Attributes['IsSelected'] := AViewInfo.IsSelected;
    ANode.Attributes['IsFocused'] := AViewInfo.IsFocused;
    ANode.Attributes['IsExpanded'] := AViewInfo.IsExpanded;
  end;
end;

initialization
  cxComponentStateStorageFactory.RegisterClass(TcxTreeList, TcxTreeListStateStorage);
  cxComponentStateStorageFactory.RegisterClass(TcxDBTreeList, TcxTreeListStateStorage);

end.
