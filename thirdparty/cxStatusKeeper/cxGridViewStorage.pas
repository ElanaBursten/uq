unit cxGridViewStorage;

interface

uses
  Classes, Contnrs, SysUtils, Variants, XMLDoc, XMLIntf, cxCustomStatusKeeper, cxComponentStorageFactory,
  cxGridCustomView, cxGridCustomTableView, cxGridRows, cxGridTableView, cxDBData, cxGridLevel,
  cxGridDBTableView, cxGridDBBandedTableView, cxGridBandedTableView, cxGrid;

type
  TcxCustomGridTableViewAccess = class (TcxCustomGridTableView);

  TcxViewRecordInfo = class
  private
    FKeyValues: Variant;
    FViewInfos: TObjectList;
  public
    IsData: Boolean;
    IsSelected: Boolean;
    IsFocused: Boolean;
    IsExpanded: Boolean;
    Level: Integer;
    constructor Create; virtual;
    destructor Destroy; override;
    property KeyValues: Variant read FKeyValues write FKeyValues;
    property ViewInfos: TObjectList read FViewInfos;
  end;

  TcxViewInfo = class
  private
    FFocusedItemIndex: Integer;
    FIsFocused: Boolean;
    FTopRecordIndex: Integer;
    FRecordInfos: TObjectList;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    property FocusedItemIndex: Integer read FFocusedItemIndex write FFocusedItemIndex;
    property IsFocused: Boolean read FIsFocused write FIsFocused;
    property RecordInfos: TObjectList read FRecordInfos;
    property TopRecordIndex: Integer read FTopRecordIndex write FTopRecordIndex;
  end;

  TcxCustomTableGridViewStateStorage = class (TcxCustomComponentStateStorage)
  private
    FViewInfo: TcxViewInfo;
  public
    constructor Create(AOwner: TcxStatusKeeperStorage); override;
    destructor Destroy; override;
  end;

  TcxTableGridViewStateStorage = class (TcxCustomTableGridViewStateStorage)
  private
    FLoadExpand: Boolean;
    FLoadSelection: Boolean;
    FLoadFocus: Boolean;
    FLoadFocusedView: Boolean;
    FLoadFocusedItem: Boolean;
    FLoadTopRecord: Boolean;
    FLoadWithDetails: Boolean;
  protected
    procedure CollapseGrid(AView: TcxCustomGridTableView);
    procedure StoreView(AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
    procedure RestoreView(AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
    function RestoreFocus(AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo): Boolean;
    procedure RestoreTopRecord(AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
    function GetGroupRowKeyValue(AGroup: TcxGridGroupRow): Variant;
    function FindViewRecord(AView: TcxCustomGridTableView; ARecordInfo: TcxViewRecordInfo): TcxCustomGridRecord;

    procedure SaveRecordViewInfo(AViewInfo: TcxViewRecordInfo; AParentNode: IXMLNode);
    procedure SaveViewInfo(AViewInfo: TcxViewInfo; AParentNode: IXMLNode);
    procedure LoadRecordViewInfo(AViewInfo: TcxViewRecordInfo; ANode: IXMLNode);
    procedure LoadViewInfo(AViewInfo: TcxViewInfo; ANode: IXMLNode);
  public
    constructor Create(AOwner: TcxStatusKeeperStorage); override;
    procedure Assign(Source: TPersistent); override;
    procedure SaveState; override;
    procedure LoadState; override;
    procedure SaveStateToFile(AParentNode: IXMLNode); override;
    procedure LoadStateFromFile(AParentNode: IXMLNode); override;
  published
    property LoadExpanding: Boolean read FLoadExpand write FLoadExpand default True;
    property LoadSelection: Boolean read FLoadSelection write FLoadSelection default True;
    property LoadFocus: Boolean read FLoadFocus write FLoadFocus default True;
    property LoadFocusedItem: Boolean read FLoadFocusedItem write FLoadFocusedItem default True;
    property LoadFocusedView: Boolean read FLoadFocusedView write FLoadFocusedView default True;
    property LoadTopRecord: Boolean read FLoadTopRecord write FLoadTopRecord default True;
    property LoadWithDetails: Boolean read FLoadWithDetails write FLoadWithDetails default True;
  end;

implementation

{ TcxViewRecordInfo }

constructor TcxViewRecordInfo.Create;
begin
  inherited Create;
  FViewInfos := TObjectList.Create;
  IsData := false;
  IsSelected := false;
  IsFocused := false;
  IsExpanded := false;
end;

destructor TcxViewRecordInfo.Destroy;
begin
  FreeAndNil(FViewInfos);
  inherited;
end;

{ TcxViewInfo }

constructor TcxViewInfo.Create;
begin
  FRecordInfos := TObjectList.Create;
end;

destructor TcxViewInfo.Destroy;
begin
  FreeAndNil(FRecordInfos);
  inherited;
end;

{ TcxCustomDBTableGridViewStateStorage }

constructor TcxCustomTableGridViewStateStorage.Create(AOwner: TcxStatusKeeperStorage);
begin
  inherited Create(AOwner);
  FViewInfo := TcxViewInfo.Create;
end;

destructor TcxCustomTableGridViewStateStorage.Destroy;
begin
  FreeAndNil(FViewInfo);
  inherited;
end;

{ TcxDBTableGridViewStateStorage }

procedure TcxTableGridViewStateStorage.Assign(Source: TPersistent);
begin
  if Source is TcxTableGridViewStateStorage then
  begin
    LoadExpanding := TcxTableGridViewStateStorage(Source).LoadExpanding;
    LoadSelection := TcxTableGridViewStateStorage(Source).LoadSelection;
    LoadFocus := TcxTableGridViewStateStorage(Source).LoadFocus;
    LoadFocusedItem := TcxTableGridViewStateStorage(Source).LoadFocusedItem;
    LoadFocusedView := TcxTableGridViewStateStorage(Source).LoadFocusedView;
    LoadTopRecord := TcxTableGridViewStateStorage(Source).LoadTopRecord;
    LoadWithDetails := TcxTableGridViewStateStorage(Source).LoadWithDetails;
  end
  else
    inherited Assign(Source);
end;

procedure TcxTableGridViewStateStorage.CollapseGrid(
  AView: TcxCustomGridTableView);
begin
  AView.ViewData.Collapse(true);
end;

constructor TcxTableGridViewStateStorage.Create(AOwner: TcxStatusKeeperStorage);
begin
  inherited Create(AOwner);
  FLoadExpand := true;
  FLoadSelection := true;
  FLoadFocus := true;
  FLoadFocusedView := true;
  FLoadFocusedItem := true;
  FLoadTopRecord := true;
  FLoadWithDetails := true;
end;

function TcxTableGridViewStateStorage.FindViewRecord(
  AView: TcxCustomGridTableView;
  ARecordInfo: TcxViewRecordInfo): TcxCustomGridRecord;
var
  AKeyValue: Variant;
  ARecordIndex: Integer;
  I: Integer;
begin
  Result := nil;;
  AKeyValue := ARecordInfo.KeyValues;
  if ARecordInfo.IsData then
  begin
    if AView.DataController is TcxDBDataController then
      ARecordIndex := TcxDBDataController(AView.DataController).FindRecordIndexByKey(AKeyValue)
    else
      ARecordIndex := AKeyValue;
    Result := AView.ViewData.GetRecordByRecordIndex(ARecordIndex);
  end
  else
    for I := 0 to AView.ViewData.RecordCount - 1 do
      if (AView.ViewData.Records[I] is TcxGridGroupRow) and
        (TcxGridGroupRow(AView.ViewData.Records[I]).Level = ARecordInfo.Level) and
          (GetGroupRowKeyValue(TcxGridGroupRow(AView.ViewData.Records[I])) = ARecordInfo.KeyValues) then
      begin
        Result := AView.ViewData.Records[I];
        break;
      end;
end;

function TcxTableGridViewStateStorage.GetGroupRowKeyValue(
  AGroup: TcxGridGroupRow): Variant;
var
  AValue: Variant;
  I: Integer;
  AParentGroup: TcxGridGroupRow;
begin
  AValue := '';
  AParentGroup := AGroup;
  for I := 0 to AGroup.Level do
  begin
    AValue := VarToStr(AValue) + ';' + VarToStr(AParentGroup.Value);
    if AParentGroup.ParentRecord is TcxGridGroupRow then
      AParentGroup := AParentGroup.ParentRecord as TcxGridGroupRow
    else
      break;
  end;
  Result := AValue;
end;

procedure TcxTableGridViewStateStorage.LoadState;
var
  AGrid: TcxGrid;
  AView: TcxCustomGridTableView;
begin
  AGrid := nil;
  AView := Self.Owner.Component as TcxCustomGridTableView;
  if AView.Control is TcxGrid then
  begin
    AGrid := AView.Control as TcxGrid;
    AGrid.BeginUpdate();
  end;
  RestoreView(AView, FViewInfo);

  if AGrid <> nil then
    AGrid.EndUpdate;

  if LoadFocusedView then
    RestoreFocus(AView, FViewInfo);

  if LoadTopRecord then
    RestoreTopRecord(AView, FViewInfo);
end;

function TcxTableGridViewStateStorage.RestoreFocus(
  AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo): Boolean;
var
  I, J: Integer;
  ARecord: TcxCustomGridRecord;
  ARecordInfo: TcxViewRecordInfo;
  ADetailView: TcxCustomGridTableView;
begin
  Result := false;
  if AViewInfo.IsFocused then
  begin
    if not TcxCustomGridTableViewAccess(AView).CanFocus then
      TcxGridLevel(AView.Level).MakeVisible;
    AView.Focused := true;
    Result := true;
  end;
  for I := 0 to AViewInfo.FRecordInfos.Count - 1 do
  begin
    ARecordInfo := TcxViewRecordInfo(AViewInfo.RecordInfos[I]);
    ARecord := FindViewRecord(AView, ARecordInfo);
    if ARecord <> nil then
      if not AViewInfo.IsFocused then
        for J := 0 to ARecordInfo.ViewInfos.Count - 1 do
          if TcxGridMasterDataRow(ARecord).DetailGridViews[J] is TcxCustomGridTableView then
          begin
            ADetailView := TcxGridMasterDataRow(ARecord).DetailGridViews[J] as TcxCustomGridTableView;
            Result := RestoreFocus(ADetailView, TcxViewInfo(ARecordInfo.ViewInfos[J]));
            if Result then
              break;
          end;
  end;
end;

procedure TcxTableGridViewStateStorage.RestoreView(
  AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
var
  I, J: Integer;
  ARecord: TcxCustomGridRecord;
  ARecordInfo: TcxViewRecordInfo;
  ADetailView: TcxCustomGridTableView;
begin
  CollapseGrid(AView);

  for I := 0 to AViewInfo.FRecordInfos.Count - 1 do
  begin
    ARecordInfo := TcxViewRecordInfo(AViewInfo.RecordInfos[I]);
    ARecord := FindViewRecord(AView, ARecordInfo);
    if ARecord <> nil then
    begin
      if LoadExpanding and ARecordInfo.IsExpanded then
        ARecord.Expand(false)
      else
        ARecord.Collapse(true);
      ARecord := FindViewRecord(AView, ARecordInfo);
      if LoadWithDetails then
        for J := 0 to ARecordInfo.ViewInfos.Count - 1 do
          if TcxGridMasterDataRow(ARecord).DetailGridViews[J] is TcxCustomGridTableView then
          begin
            ADetailView := TcxGridMasterDataRow(ARecord).DetailGridViews[J] as TcxCustomGridTableView;
            RestoreView(ADetailView, TcxViewInfo(ARecordInfo.ViewInfos[J]));
          end;
          
      ARecord.Focused := LoadFocus and ARecordInfo.IsFocused;
      ARecord.Selected := LoadSelection and ARecordInfo.IsSelected;
    end;
  end;

  if LoadFocusedItem then
    AView.Controller.FocusedItemIndex := AViewInfo.FocusedItemIndex;
end;

procedure TcxTableGridViewStateStorage.RestoreTopRecord(
  AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
var
  I, J: Integer;
  ARecord: TcxCustomGridRecord;
  ARecordInfo: TcxViewRecordInfo;
  ADetailView: TcxCustomGridTableView;
begin
  if LoadWithDetails then
    for I := 0 to AViewInfo.FRecordInfos.Count - 1 do
    begin
      ARecordInfo := TcxViewRecordInfo(AViewInfo.RecordInfos[I]);
      if ARecordInfo.ViewInfos.Count = 0 then
        continue;

      ARecord := FindViewRecord(AView, ARecordInfo);
      if (ARecord <> nil) then
        for J := 0 to ARecordInfo.ViewInfos.Count - 1 do
          if TcxGridMasterDataRow(ARecord).DetailGridViews[J] is TcxCustomGridTableView then
          begin
            ADetailView := TcxGridMasterDataRow(ARecord).DetailGridViews[J] as TcxCustomGridTableView;
            RestoreTopRecord(ADetailView, TcxViewInfo(ARecordInfo.ViewInfos[J]));
          end;
    end;

  AView.Controller.TopRecordIndex := AViewInfo.TopRecordIndex;
end;

procedure TcxTableGridViewStateStorage.SaveState;
var
  AView: TcxCustomGridTableView;
begin
  AView := Self.Owner.Component as TcxCustomGridTableView;
  FreeAndNil(FViewInfo);
  FViewInfo := TcxViewInfo.Create;
  StoreView(AView, FViewInfo);
end;

procedure TcxTableGridViewStateStorage.SaveRecordViewInfo(AViewInfo: TcxViewRecordInfo; AParentNode: IXMLNode);
var
  ANode: IXMLNode;
  I: Integer;
begin
  ANode := AParentNode.AddChild('Record');

  ANode.Attributes['KeyValue'] := AViewInfo.KeyValues;
  ANode.Attributes['KeyValueType'] := VarType(AViewInfo.KeyValues);
  ANode.Attributes['IsData'] := AViewInfo.IsData;
  ANode.Attributes['IsSelected'] := AViewInfo.IsSelected;
  ANode.Attributes['IsFocused'] := AViewInfo.IsFocused;
  ANode.Attributes['IsExpanded'] := AViewInfo.IsExpanded;
  ANode.Attributes['Level'] := AViewInfo.Level;

  for I := 0 to AViewInfo.ViewInfos.Count - 1 do
    SaveViewInfo(TcxViewInfo(AViewInfo.ViewInfos[I]), ANode);
end;

procedure TcxTableGridViewStateStorage.SaveViewInfo(AViewInfo: TcxViewInfo; AParentNode: IXMLNode);
var
  ANode: IXMLNode;
  I: Integer;
begin
  ANode := AParentNode.AddChild('View');
  ANode.Attributes['FocusedItemIndex'] := AViewInfo.FocusedItemIndex;
  ANode.Attributes['IsFocused'] := AViewInfo.IsFocused;
  ANode.Attributes['TopRecordIndex'] := AViewInfo.TopRecordIndex;

  for I := 0 to AViewInfo.RecordInfos.Count - 1 do
    SaveRecordViewInfo(TcxViewRecordInfo(AViewInfo.RecordInfos[I]), ANode);
end;

procedure TcxTableGridViewStateStorage.SaveStateToFile(AParentNode: IXMLNode);
var
  ANode: IXMLNode;
  AView: TcxCustomGridTableView;
begin
  SaveState;
  AView := Owner.Component as TcxCustomGridTableView;
  ANode := AParentNode.AddChild('Component');
  ANode.Attributes['ComponentName'] := AView.Name;

  SaveViewInfo(FViewInfo, ANode);
end;

procedure TcxTableGridViewStateStorage.LoadViewInfo(AViewInfo: TcxViewInfo;
  ANode: IXMLNode);
var
  I: Integer;
  ARecordInfo: TcxViewRecordInfo;
begin
  AViewInfo.FocusedItemIndex := StrToInt(ANode.Attributes['FocusedItemIndex']);
  AViewInfo.IsFocused := StrToBool(ANode.Attributes['IsFocused']);
  AViewInfo.TopRecordIndex := StrToInt(ANode.Attributes['TopRecordIndex']);

  for I := 0 to ANode.ChildNodes.Count - 1 do
  begin
    ARecordInfo := TcxViewRecordInfo.Create;
    AViewInfo.RecordInfos.Add(ARecordInfo);
    LoadRecordViewInfo(ARecordInfo, ANode.ChildNodes[I]);
  end;
end;

procedure TcxTableGridViewStateStorage.LoadRecordViewInfo(
  AViewInfo: TcxViewRecordInfo; ANode: IXMLNode);
var
  I: Integer;
  AInfo: TcxViewInfo;
  AVarType: Word;
begin
  AVarType := StrToInt(ANode.Attributes['KeyValueType']);
  AViewInfo.KeyValues := VarAsType(ANode.Attributes['KeyValue'], AVarType);
  AViewInfo.IsData := StrToBool(ANode.Attributes['IsData']);
  AViewInfo.IsSelected := StrToBool(ANode.Attributes['IsSelected']);
  AViewInfo.IsFocused := StrToBool(ANode.Attributes['IsFocused']);
  AViewInfo.IsExpanded := StrToBool(ANode.Attributes['IsExpanded']);
  AViewInfo.Level := StrToInt(ANode.Attributes['Level']);

  for I := 0 to ANode.ChildNodes.Count - 1 do
  begin
    AInfo := TcxViewInfo.Create;
    AViewInfo.ViewInfos.Add(AInfo);
    LoadViewInfo(AInfo, ANode.ChildNodes[I]);
  end;
end;

procedure TcxTableGridViewStateStorage.LoadStateFromFile(
  AParentNode: IXMLNode);
var
  ANode: IXMLNode;
  I: Integer;
  AView: TcxCustomGridTableView;
begin
  FreeAndNil(FViewInfo);
  FViewInfo := TcxViewInfo.Create;
  AView := Owner.Component as TcxCustomGridTableView;

  ANode := nil;
  for I := 0 to AParentNode.ChildNodes.Count - 1 do
    if AParentNode.ChildNodes[I].Attributes['ComponentName'] = AView.Name then
    begin
      ANode := AParentNode.ChildNodes[I];
      break;
    end;

  if ANode = nil then
    exit;

  LoadViewInfo(FViewInfo, ANode.ChildNodes.First);

  LoadState;
end;

procedure TcxTableGridViewStateStorage.StoreView(
  AView: TcxCustomGridTableView; AViewInfo: TcxViewInfo);
var
  I, J: Integer;
  ARecordInfo: TcxViewRecordInfo;
  ADetailViewInfo: TcxViewInfo;
  ADetailView: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord;
begin
  AViewInfo.FTopRecordIndex := AView.Controller.TopRecordIndex;
  AViewInfo.IsFocused := AView.Focused;
  AViewInfo.FocusedItemIndex := AView.Controller.FocusedItemIndex;
  with AView.ViewData do
    for I := 0 to RecordCount - 1 do
    begin
      ARecord := Records[I];
      if not (ARecord.Selected or ARecord.Expanded or ARecord.Focused or ((ARecord is TcxGridMasterDataRow) and
          TcxGridMasterDataRow(ARecord).ActiveDetailGridViewExists)) then
        continue;

      ARecordInfo := TcxViewRecordInfo.Create;
      ARecordInfo.Level := ARecord.Level;
      if ARecord.IsData then
      begin
        if AView.DataController is TcxDBDataController then
          ARecordInfo.KeyValues := AView.DataController.GetRecordId(ARecord.RecordIndex)
        else
          ARecordInfo.KeyValues := ARecord.RecordIndex;
        ARecordInfo.IsData := true;
        if (ARecord is TcxGridMasterDataRow) and
          TcxGridMasterDataRow(ARecord).ActiveDetailGridViewExists then
        begin
          for J := 0 to TcxGridMasterDataRow(ARecord).DetailGridViewCount - 1 do
          if TcxGridMasterDataRow(ARecord).DetailGridViews[J] is TcxCustomGridTableView then
          begin
            ADetailView := TcxGridMasterDataRow(ARecord).DetailGridViews[J] as TcxCustomGridTableView;
            ADetailViewInfo := TcxViewInfo.Create;
            StoreView(ADetailView, ADetailViewInfo);
            ARecordInfo.ViewInfos.Add(ADetailViewInfo);
          end
          else
            continue;
        end;
      end
      else
        begin
          ARecordInfo.KeyValues := GetGroupRowKeyValue(TcxGridGroupRow(Records[I]));
          ARecordInfo.IsData := false;
        end;

      ARecordInfo.IsSelected := ARecord.Selected;
      ARecordInfo.IsFocused := ARecord.Focused;
      ARecordInfo.IsExpanded := ARecord.Expanded;

      AViewInfo.RecordInfos.Add(ARecordInfo);
  end;
end;

initialization
  cxComponentStateStorageFactory.RegisterClass(TcxGridDBTableView, TcxTableGridViewStateStorage);
  cxComponentStateStorageFactory.RegisterClass(TcxGridDBBandedTableView, TcxTableGridViewStateStorage);
  cxComponentStateStorageFactory.RegisterClass(TcxGridTableView, TcxTableGridViewStateStorage);
  cxComponentStateStorageFactory.RegisterClass(TcxGridBandedTableView, TcxTableGridViewStateStorage);


end.
