unit cxCustomStatusKeeper;

interface

uses
  Classes,
  {$IF CompilerVersion >= 23.0}
    Vcl.Controls, Vcl.Dialogs,
  {$ELSE}
    Controls, Dialogs,
  {$IFEND}
  Contnrs, SysUtils, XMLDoc, XMLIntf, cxComponentStorageFactory;

type
  TcxCustomStatusKeeper = class;
  TcxStatusKeeperStorage = class;

  TcxCustomComponentStateStorage = class (TPersistent)
  private
    FOwner: TcxStatusKeeperStorage;
  protected
    procedure LoadState; virtual; abstract;
    procedure SaveState; virtual; abstract;
    procedure SaveStateToFile(AParentNode: IXMLNode); virtual; abstract;
    procedure LoadStateFromFile(AParentNode: IXMLNode); virtual; abstract;
    property Owner: TcxStatusKeeperStorage read FOwner;
  public
    constructor Create(AOwner: TcxStatusKeeperStorage); virtual;
  end;

  TcxCustomComponentStateStorageClass = class of TcxCustomComponentStateStorage;

  TcxStatusKeeperStorage = class (TCollectionItem)
  private
    FComponent: TComponent;
    FComponentStorage: TcxCustomComponentStateStorage;
    FKeeper: TcxCustomStatusKeeper;
    FComponentClassName: string;
    procedure SetComponent(AValue: TComponent);
    function GetComponent: TComponent;
    procedure SetStorage(AValue: TcxCustomComponentStateStorage);
    procedure SetComponentClassName(AValue: string);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Component: TComponent read GetComponent write SetComponent;
    property ComponentClassName: string read FComponentClassName write SetComponentClassName;
    property ComponentStorage: TcxCustomComponentStateStorage read FComponentStorage write SetStorage;
  end;

  TcxStatusKeeperStorages = class(TCollection)
  private
    FStatusKeeper: TcxCustomStatusKeeper;
    procedure SetStorage(Index: Integer; Value: TcxStatusKeeperStorage);
    function GetStorage(Index: Integer): TcxStatusKeeperStorage;
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AKeeper: TcxCustomStatusKeeper; AStorageClass: TCollectionItemClass);
    function  Add: TcxStatusKeeperStorage;
    property StatusKeeper: TcxCustomStatusKeeper read FStatusKeeper;
    property Items[Index: Integer]: TcxStatusKeeperStorage read GetStorage write SetStorage; default;
  end;

  TcxComponentStateStorageClass = class of TcxCustomComponentStateStorage;

  TcxCustomStatusKeeper = class (TComponent)
  private
    FStorages: TcxStatusKeeperStorages;
    procedure SetStorages(AValue: TcxStatusKeeperStorages);
  protected
    function GetComponentStateStorageClass(AComponent: TComponent): TcxComponentStateStorageClass;

    function PrepareXMLDocumentForAllStates: TXMLDocument;
    function PrepareXMLDocumentForComponent(AComponent: TComponent): TXMLDocument;

    procedure GetAllStatesFromXMLDocument(ADocument: TXMLDocument);
    procedure GetComponentStateFromDocument(ADocument: TXMLDocument; AComponent: TComponent);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetComponentStateStorage(AComponent: TComponent): TcxCustomComponentStateStorage;
    procedure AddComponent(AComponent: TComponent);
    procedure RemoveComponent(AComponent: TComponent);
    //Stream
    procedure StoreStatesToStream(AStream: TStream); virtual;
    procedure RestoreStatesFromStream(AStream: TStream); virtual;
    procedure StoreComponentStateToStream(AComponent: TComponent; AStream: TStream); virtual;
    procedure RestoreComponentStateFromStream(AComponent: TComponent; AStream: TStream); virtual;
    //File
    procedure StoreStatesToFile(AFileName: string); virtual;
    procedure RestoreStatesFromFile(AFileName: string); virtual;
    procedure StoreComponentStateToFile(AComponent: TComponent; AFileName: string); virtual;
    procedure RestoreComponentStateFromFile(AComponent: TComponent; AFileName: string); virtual;
    //In-memory
    procedure SaveState; virtual;
    procedure LoadState; virtual;
    procedure SaveComponentState(AComponent: TComponent); virtual;
    procedure LoadComponentState(AComponent: TComponent); virtual;

    property Storages: TcxStatusKeeperStorages read FStorages write SetStorages;
  end;

implementation


{ TcxCustomStatusKeeper }

procedure TcxCustomStatusKeeper.AddComponent(AComponent: TComponent);
var
  AComponentStorageClass: TClass;
  AStorage: TcxStatusKeeperStorage;
begin
  if GetComponentStateStorage(AComponent) = nil then
  begin
    AComponentStorageClass := cxComponentStateStorageFactory.GetStorageClass(AComponent.ClassType);
    if (AComponentStorageClass <> nil) then
    begin
      AStorage := Storages.Add;
      AStorage.Component := AComponent;
    end
    else
      raise Exception.Create('This component is not supported');
  end;
end;

constructor TcxCustomStatusKeeper.Create(AOwner: TComponent);
begin
  inherited;
  FStorages := TcxStatusKeeperStorages.Create(Self, TcxStatusKeeperStorage);
end;

destructor TcxCustomStatusKeeper.Destroy;
begin
  FreeAndNil(FStorages);
  inherited;
end;

function TcxCustomStatusKeeper.GetComponentStateStorage(
  AComponent: TComponent): TcxCustomComponentStateStorage;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FStorages.Count - 1 do
    if FStorages[I].Component = AComponent then
    begin
      Result := TcxCustomComponentStateStorage(FStorages[I].ComponentStorage);
      break;
    end;
end;

function TcxCustomStatusKeeper.GetComponentStateStorageClass(
  AComponent: TComponent): TcxComponentStateStorageClass;
begin
  if AComponent <> nil then
    Result := TcxComponentStateStorageClass(cxComponentStateStorageFactory.GetStorageClass(AComponent.ClassType))
  else
    Result := nil;
end;

procedure TcxCustomStatusKeeper.LoadComponentState(AComponent: TComponent);
var
  AStorage: TcxCustomComponentStateStorage;
begin
  AStorage := GetComponentStateStorage(AComponent);
  if AStorage <> nil then
    AStorage.LoadState
  else
    raise Exception.Create('The corresponding storage does not exist');
end;

procedure TcxCustomStatusKeeper.LoadState;
var
  I: Integer;
begin
  for I := 0 to FStorages.Count - 1 do
    FStorages[I].ComponentStorage.LoadState;
end;

function TcxCustomStatusKeeper.PrepareXMLDocumentForAllStates: TXMLDocument;
var
  ADocument: TXMLDocument;
  AParentNode: IXMLNode;
  I: Integer;
begin
  ADocument := TXMLDocument.Create(Self);
  try
    ADocument.Active := true;
    AParentNode := ADocument.AddChild('Components');
    for I := 0 to Storages.Count - 1 do
      Storages[I].ComponentStorage.SaveStateToFile(AParentNode);
  finally
    Result := ADocument;
  end;
end;

function TcxCustomStatusKeeper.PrepareXMLDocumentForComponent(
  AComponent: TComponent): TXMLDocument;
var
  ADocument: TXMLDocument;
  AStorage: TcxCustomComponentStateStorage;
begin
  Result := nil;
  AStorage := GetComponentStateStorage(AComponent);
  if AStorage <> nil then
  begin
    ADocument := TXMLDocument.Create(Self);
    try
      ADocument.Active := true;
      AStorage.SaveStateToFile(ADocument.AddChild('Components'));
    finally
      Result := ADocument;
    end;
  end;
end;

procedure TcxCustomStatusKeeper.GetAllStatesFromXMLDocument(
  ADocument: TXMLDocument);
var
  I: Integer;
begin
  if ADocument = nil then
    exit;
  for I := 0 to Storages.Count - 1 do
    Storages[I].ComponentStorage.LoadStateFromFile(ADocument.ChildNodes.FindNode('Components'));
end;

procedure TcxCustomStatusKeeper.GetComponentStateFromDocument(
  ADocument: TXMLDocument; AComponent: TComponent);
var
  AStorage: TcxCustomComponentStateStorage;
begin
  AStorage := GetComponentStateStorage(AComponent);
  if AStorage <> nil then
    AStorage.LoadStateFromFile(ADocument.ChildNodes.FindNode('Components'))
  else
    raise Exception.Create('The corresponding storage does not exist');
end;

procedure TcxCustomStatusKeeper.RemoveComponent(AComponent: TComponent);
var
  I: Integer;
begin
  for I := 0 to FStorages.Count - 1 do
    if FStorages[I].Component = AComponent then
    begin
      FStorages.Delete(I);
      break;
    end;
end;

procedure TcxCustomStatusKeeper.SaveComponentState(AComponent: TComponent);
var
  AStorage: TcxCustomComponentStateStorage;
begin
  AStorage := GetComponentStateStorage(AComponent);
  if AStorage <> nil then
    AStorage.SaveState
  else
    raise Exception.Create('The corresponding storage does not exist');
end;

procedure TcxCustomStatusKeeper.SaveState;
var
  I: Integer;
begin
  for I := 0 to FStorages.Count - 1 do
    FStorages[I].ComponentStorage.SaveState;
end;

procedure TcxCustomStatusKeeper.SetStorages(AValue: TcxStatusKeeperStorages);
begin
  Storages.Assign(AValue);
  Storages.Changed;
end;

procedure TcxCustomStatusKeeper.StoreComponentStateToFile(
  AComponent: TComponent; AFileName: string);
var
  ADocument: TXMLDocument;
begin
  ADocument := PrepareXMLDocumentForComponent(AComponent);
  if ADocument <> nil then
  begin
    ADocument.SaveToFile(AFileName);
    ADocument.CleanupInstance;
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.StoreComponentStateToStream(
  AComponent: TComponent; AStream: TStream);
var
  ADocument: TXMLDocument;
begin
  ADocument := PrepareXMLDocumentForComponent(AComponent);
  if ADocument <> nil then
  begin
    ADocument.SaveToStream(AStream);
    ADocument.CleanupInstance;
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.StoreStatesToStream(AStream: TStream);
var
  ADocument: TXMLDocument;
begin
  ADocument := PrepareXMLDocumentForAllStates;
  if ADocument <> nil then
  begin
    ADocument.SaveToStream(AStream);
    ADocument.CleanupInstance;
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.StoreStatesToFile(AFileName: string);
var
  ADocument: TXMLDocument;
begin
  ADocument := PrepareXMLDocumentForAllStates;
  if ADocument <> nil then
  begin
    ADocument.SaveToFile(AFileName);
    ADocument.CleanupInstance;
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.RestoreComponentStateFromFile(
  AComponent: TComponent; AFileName: string);
var
  ADocument: TXMLDocument;
begin
  ADocument := TXMLDocument.Create(Self);
  try
    ADocument.Active := true;
    ADocument.LoadFromFile(AFileName);
    GetComponentStateFromDocument(ADocument, AComponent);
  finally
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.RestoreComponentStateFromStream(
  AComponent: TComponent; AStream: TStream);
var
  ADocument: TXMLDocument;
begin
  ADocument := TXMLDocument.Create(Self);
  try
    ADocument.LoadFromStream(AStream);
    GetAllStatesFromXMLDocument(ADocument);
  finally
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.RestoreStatesFromFile(AFileName: string);
var
  ADocument: TXMLDocument;
begin
  ADocument := TXMLDocument.Create(Self);
  try
    ADocument.LoadFromFile(AFileName);
    GetAllStatesFromXMLDocument(ADocument);
  finally
    FreeAndNil(ADocument);
  end;
end;

procedure TcxCustomStatusKeeper.RestoreStatesFromStream(AStream: TStream);
var
  ADocument: TXMLDocument;
begin
  ADocument := TXMLDocument.Create(Self);
  try
    ADocument.LoadFromStream(AStream);
    GetAllStatesFromXMLDocument(ADocument);
  finally
    FreeAndNil(ADocument);
  end;  
end;

{ TcxCustomComponentStateStorage }

constructor TcxCustomComponentStateStorage.Create(AOwner: TcxStatusKeeperStorage);
begin
  FOwner := AOwner;
end;

{ TcxStatusKeeperStorages }

function TcxStatusKeeperStorages.Add: TcxStatusKeeperStorage;
begin
  Result := TcxStatusKeeperStorage(inherited Add);
end;

constructor TcxStatusKeeperStorages.Create(AKeeper: TcxCustomStatusKeeper;
  AStorageClass: TCollectionItemClass);
begin
  inherited Create(TcxStatusKeeperStorage);
  FStatusKeeper := AKeeper;
end;

function TcxStatusKeeperStorages.GetOwner: TPersistent;
begin
  Result := FStatusKeeper;
end;

function TcxStatusKeeperStorages.GetStorage(
  Index: Integer): TcxStatusKeeperStorage;
begin
  Result := TcxStatusKeeperStorage(inherited Items[Index]);
end;

procedure TcxStatusKeeperStorages.SetStorage(Index: Integer;
  Value: TcxStatusKeeperStorage);
begin
  Items[Index].Assign(Value);
end;

{ TcxStatusKeeperStorage }

procedure TcxStatusKeeperStorage.Assign(Source: TPersistent);
begin
  if Source is TcxStatusKeeperStorage then
  begin
    Component := TcxStatusKeeperStorage(Source).Component;
    ComponentStorage.Assign(TcxStatusKeeperStorage(Source).ComponentStorage);
  end
  else
    inherited Assign(Source);
end;

constructor TcxStatusKeeperStorage.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FKeeper := TcxStatusKeeperStorages(Collection).StatusKeeper;
end;

destructor TcxStatusKeeperStorage.Destroy;
begin
  FreeAndNil(FComponentStorage);
  inherited Destroy;
end;

function TcxStatusKeeperStorage.GetComponent: TComponent;
begin
  Result := FComponent;
end;

procedure TcxStatusKeeperStorage.SetComponent(AValue: TComponent);
var
  AComponentStorageClass: TcxComponentStateStorageClass;
begin
  if FComponent <> AValue then
  begin
    if AValue = nil then
    begin
      FComponent := nil;
      ComponentClassName := '';
      exit;
    end;

    AComponentStorageClass := FKeeper.GetComponentStateStorageClass(AValue);
    if AComponentStorageClass <> nil then
    begin
      FComponent := AValue;
      if not (FComponentStorage is AComponentStorageClass) then
        FreeAndNil(FComponentStorage);
      ComponentClassName := AValue.ClassName;
    end;
  end;
end;

procedure TcxStatusKeeperStorage.SetComponentClassName(AValue: string);
var
  AClass: TClass;
begin
  if FComponentClassName <> AValue then
  begin
    FComponentClassName := AValue;
    if FComponentClassName = '' then
    begin
      FreeAndNil(FComponentStorage);
      exit;
    end;

    AClass := cxComponentStateStorageFactory.FindStorageClassByClassName(AValue);
    if AClass <> nil then
      ComponentStorage := TcxCustomComponentStateStorageClass(AClass).Create(Self);
  end;
end;

procedure TcxStatusKeeperStorage.SetStorage(
  AValue: TcxCustomComponentStateStorage);
begin
  if FComponentStorage = nil then
    FComponentStorage := AValue
  else
  if AValue is ComponentStorage.ClassType then
    ComponentStorage.Assign(AValue);
end;

end.
