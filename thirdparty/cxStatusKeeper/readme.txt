Use these units to provide support for saving and restoring
extended properties for Developer Express grids and tree lists.

It is a subset (just the units, no packages or examples) of 
the cxStatusKeeperPackage.zip file provided by Dev Ex support.

Follow the link below for the knowledge base article which includes 
links to the complete package and some example projects:

http://www.devexpress.com/Support/Center/Question/Details/KA18654

Basic Usage:

// Create a StatusKeeper: 
var
  AStatusKeeper: TcxStatusKeeper;
  AStorage: TcxTableGridViewStateStorage;
begin
  AStatusKeeper := TcxStatusKeeper.Create(Self);
  AStatusKeeper.AddComponent(cxGrid1DBTableView1);

  AStorage := AStatusKeeper.Storages[0].ComponentStorage as TcxTableGridViewStateStorage;
  // Customize Storage options here as needed (they all default to True)
  with AStorage do begin
    LoadExpanding := True;
    LoadSelection := True;
    LoadFocus := True;
    LoadFocusedItem := True;
    LoadFocusedView := True;
    LoadTopRecord := True;
    LoadWithDetails := True;
  end;
end;

// Save state to memory (can also save to stream or file)
AStatusKeeper.SaveState;

// Restore state from memory (can also load from stream or file)
AStatusKeeper.LoadState;