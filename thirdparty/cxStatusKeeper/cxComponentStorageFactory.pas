unit cxComponentStorageFactory;

{. $I cxVer.inc} // dependency removed by Oasis Digital

interface

uses
  Classes, SysUtils, Contnrs;

type
  TcxRegisteredItemData = class
    ComponentClass: TClass;
    RegisteredStorageClass: TClass;
  end;

  TcxComponentStorageFactory = class (TObject)
  private
    FRegisteredItems: TObjectList;
  protected
    function Find(AComponentClass: TClass): TcxRegisteredItemData;
  public
    constructor Create;
    destructor Destroy; override;
    function GetStorageClass(AComponentClass: TClass): TClass;
    procedure RegisterClass(AComponentClass: TClass; AStorageClass: TClass);
    function FindStorageClassByClassName(AName: string): TClass;
  end;

function cxComponentStateStorageFactory: TcxComponentStorageFactory;

implementation

var
  FComponentStateStorageFactory: TcxComponentStorageFactory;

function cxComponentStateStorageFactory: TcxComponentStorageFactory;
begin
  if FComponentStateStorageFactory = nil then
    FComponentStateStorageFactory := TcxComponentStorageFactory.Create;
  Result := FComponentStateStorageFactory;
end;

{ TcxControlStorageFactory }

constructor TcxComponentStorageFactory.Create;
begin
  FRegisteredItems := TObjectList.Create(true);
end;

destructor TcxComponentStorageFactory.Destroy;
begin
  FreeAndNil(FRegisteredItems);
  inherited;
end;

function TcxComponentStorageFactory.Find(AComponentClass: TClass): TcxRegisteredItemData;
var
  I: Integer;
  AData: TcxRegisteredItemData;
begin
  Result := nil;
  for I := 0 to FRegisteredItems.Count - 1 do
  begin
    AData := TcxRegisteredItemData(FRegisteredItems[I]);
    if AData.ComponentClass = AComponentClass then
    begin
      Result := AData;
      break;
    end;
  end;
end;

function TcxComponentStorageFactory.FindStorageClassByClassName(AName: string): TClass;
var
  I: Integer;
  AData: TcxRegisteredItemData;
begin
  Result := nil;
  for I := 0 to FRegisteredItems.Count - 1 do
  begin
    AData := TcxRegisteredItemData(FRegisteredItems[I]);
    if AData.ComponentClass.ClassName = AName then
    begin
      Result := AData.RegisteredStorageClass;
      break;
    end;
  end;
end;

function TcxComponentStorageFactory.GetStorageClass(
  AComponentClass: TClass): TClass;
var
  AData: TcxRegisteredItemData;
begin
  Result := nil;
  AData := Find(AComponentClass);
  if AData <> nil then
    Result := AData.RegisteredStorageClass;
end;

procedure TcxComponentStorageFactory.RegisterClass(AComponentClass,
  AStorageClass: TClass);
var
  AData: TcxRegisteredItemData;
begin
  AData := Find(AComponentClass);
  if AData = nil then
  begin
    AData := TcxRegisteredItemData.Create;
    AData.ComponentClass := AComponentClass;
    AData.RegisteredStorageClass := AStorageClass;
    FRegisteredItems.Add(AData);
  end
  else
    AData.RegisteredStorageClass := AStorageClass;
end;

initialization

finalization
  FreeAndNil(FComponentStateStorageFactory);

end.
 