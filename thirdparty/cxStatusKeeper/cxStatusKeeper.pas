unit cxStatusKeeper;

interface

uses
  cxGridViewStorage,  //Remove this line if you do not have ExpressQuantumGrid
  cxTreeListStorage,  //Remove this line if you do not have ExpressQuantumTreeList
  cxCustomStatusKeeper;

type
  TcxStatusKeeper = class (TcxCustomStatusKeeper)
  published
    property Storages;
  end;

implementation

{ TcxStatusKeeper }


end.
