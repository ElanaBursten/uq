// Translate to Delphi : Eugene Proshkin
// E-mail evgeny@energobank.ru
// Source code on Visual Basic 4 : Data Solutions Pty Ltd (ACN 010 951 498)
// Email for info: clatta@ozemail.com.au

unit PushKeystrokes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShellAPI;

procedure PushKeys(Source: string);
procedure PushFnKey(KeyCode: string);

const
  KEYEVENTF_EXTENDEDKEY = 1;
  KEYEVENTF_KEYUP = 2;
  // Constants for Virtual Keys
  VK_BREAK = 3; // Break = ^C
  VK_BELL = 7; // Bell = ^G
  VK_BACK = 8; // Backspace = ^H
  VK_TAB = 9; // Tab = ^I
  VK_LINEFEED = $A; // Line feed = ^J
  VK_CLEAR = $C; // Form feed = ^L
  VK_RETURN = $D; // Enter key = ^M

  VK_SHIFT = $10; // Shift Key
  VK_CONTROL = $11; // Control key
  VK_MENU = $12; // Alt key
  VK_PAUSE = $13; // Pause??
  VK_CAPITAL = $14; // Capslock
  VK_NUMLOCK = $90; // Numeric lock
  VK_SCROLL = $91; // Scroll lock

  VK_ESCAPE = $1B; // Escape

  VK_SPACE = $20; // Space
  VK_PRIOR = $21; // Page Up
  VK_NEXT = $22; // Page Down
  VK_END = $23; // End
  VK_HOME = $24; // Home
  VK_LEFT = $25; // Left arrow
  VK_UP = $26; // Up arrow
  VK_RIGHT = $27; // Right arrow
  VK_DOWN = $28; // Down arrow
  VK_SELECT = $29; // Select??
  VK_PRINT = $2A; // Print Screen
  VK_EXECUTE = $2B; // Execute??
  VK_SNAPSHOT = $2C; // Snapshot??
  VK_INSERT = $2D; // Insert
  VK_DELETE = $2E; // Delete
  VK_HELP = $2F; // Help

  // Numeric Keypad
  VK_NUMPAD0 = $60;
  VK_NUMPAD1 = $61;
  VK_NUMPAD2 = $62;
  VK_NUMPAD3 = $63;
  VK_NUMPAD4 = $64;
  VK_NUMPAD5 = $65;
  VK_NUMPAD6 = $66;
  VK_NUMPAD7 = $67;
  VK_NUMPAD8 = $68;
  VK_NUMPAD9 = $69;
  VK_MULTIPLY = $6A;
  VK_ADD = $6B;
  VK_SEPARATOR = $6C;
  VK_SUBTRACT = $6D;
  VK_DECIMAL = $6E;
  VK_DIVIDE = $6F;

// Function Keys
  VK_F1 = $70;
  VK_F2 = $71;
  VK_F3 = $72;
  VK_F4 = $73;
  VK_F5 = $74;
  VK_F6 = $75;
  VK_F7 = $76;
  VK_F8 = $77;
  VK_F9 = $78;
  VK_F10 = $79;
  VK_F11 = $7A;
  VK_F12 = $7B;
  VK_F13 = $7C;
  VK_F14 = $7D;
  VK_F15 = $7E;
  VK_F16 = $7F;
  VK_F17 = $80;
  VK_F18 = $81;
  VK_F19 = $82;
  VK_F20 = $83;
  VK_F21 = $84;
  VK_F22 = $85;
  VK_F23 = $86;
  VK_F24 = $87;

var
  ShiftOn: boolean;
  ControlOn: boolean;
  AltOn: boolean;

implementation

////////////////////////////////////////
// Presses the appropriate key specified

procedure PressKey(Vk: Byte; Scan: Byte);
begin
  keybd_event(Vk, Scan, 0, 0);
  keybd_event(Vk, Scan, KEYEVENTF_KEYUP, 0);
end;

////////////////////////////////////////
// Pushes a normal (non-function) key

procedure PushAKey(ch: string);
var Vk: Integer;
  Scan: Integer;
  charz, oemchar: array[0..1] of Char;
begin
  // Get the virtual key code for this character
  Vk := VkKeyScan(ch[1]);

  // Get the scan code for this key
  StrPCopy(charz, ch);
  CharToOem(charz, oemchar);
  Scan := OemKeyScan(Ord(oemchar[0]));

  // Press the key
  PressKey(Vk, Scan);
end;

//////////////////////////////////////////////////////////////////
// Pushes a key with the shift key down
// If ShiftOn is true, it does not press and release the shift key

procedure PushShiftKey(ch: string);
begin
  // Turn shift on
  if not ShiftOn then keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), 0, 0);

  // Push key
  PushAKey(ch);

  // Turn shift off
  if not ShiftOn then keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), KEYEVENTF_KEYUP, 0);
end;

//////////////////////////////////////////////
// This is the routine that does all the work

procedure PushKeys(Source: string);
var Ctr: Longint;
  SubStr: string;
  BrCnt: Integer; // Bracket count
  BrOpen: string; // Open bracket
  BrClose: string; // Close bracket
  Ch: string; // Temporary character
begin
  ShiftOn := False;
  ControlOn := False;
  AltOn := False;

  // Add a space to solve Mid$ problems. We won't process it.
  //Source:= Source + ' ';
  Ctr := 1;
  while Ctr <= Length(Source) do
  begin
    case Source[Ctr] of
      '+': // Shift key pressed
        begin
          BrOpen := Source[Ctr + 1];
          if (BrOpen = '(') or (BrOpen = '{') then
          begin
            if BrOpen = '('
              then BrClose := ')'
            else BrClose := '}';
            inc(Ctr);
            SubStr := '';
            BrCnt := 1;
            while (Ctr <= Length(Source)) and (BrCnt <> 0) do
            begin
              inc(Ctr);
              Ch := Source[Ctr];
              if Ch <> BrClose
                then SubStr := SubStr + Ch
              else dec(BrCnt);
              if Ch = BrOpen then inc(BrCnt);
            end;

              // Turn shift on
            if not ShiftOn then
            begin
              keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), 0, 0);
              ShiftOn := True;
            end;

              // Push the keys
            if BrOpen = '('
              then PushKeys(SubStr)
            else PushFnKey(SubStr);

              // Turn shift off
            keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), KEYEVENTF_KEYUP, 0);
            ShiftOn := False;
          end
           // The next key is shifted
          else
          begin
            inc(Ctr);
            PushShiftKey(Source[Ctr]);
          end;
        end;
      '^': // Control key pressed
        begin
          BrOpen := Source[Ctr + 1];
          if (BrOpen = '(') or (BrOpen = '{') then
          begin
            if BrOpen = ')'
              then BrClose := ')'
            else BrClose := '}';
            inc(Ctr);
            SubStr := '';
            BrCnt := 1;
            while (Ctr <= Length(Source)) and (BrCnt <> 0) do
            begin
              inc(Ctr);
              Ch := Source[Ctr];
              if Ch <> BrClose
                then SubStr := SubStr + Source[Ctr]
              else dec(BrCnt);
              if Ch = BrOpen then inc(BrCnt);
            end;

              // Turn control on
            if not ControlOn then
            begin
              keybd_event(VK_CONTROL, MapVirtualKey(VK_CONTROL, 0), 0, 0);
              ControlOn := True;
            end;

              // Push the keys
            if BrOpen = '('
              then PushKeys(SubStr)
            else PushFnKey(SubStr);

              // Turn control off
            keybd_event(VK_CONTROL, MapVirtualKey(VK_CONTROL, 0), KEYEVENTF_KEYUP, 0);
            ControlOn := False;
          end
          else
          begin
              // The next key is controlled
            inc(Ctr);
            if not ControlOn then keybd_event(VK_CONTROL, MapVirtualKey(VK_CONTROL, 0), 0, 0);
            PushAKey(Source[Ctr]);
            keybd_event(VK_CONTROL, MapVirtualKey(VK_CONTROL, 0), KEYEVENTF_KEYUP, 0);
          end;
        end;
      '%': // Push Alt key
        begin
          BrOpen := Source[Ctr + 1];
          if (BrOpen = '(') or (BrOpen = '{') then
          begin
            if BrOpen = '('
              then BrClose := ')'
            else BrClose := '}';

            inc(Ctr);
            SubStr := '';
            BrCnt := 1;
            while (Ctr <= Length(Source)) and (BrCnt <> 0) do
            begin
              inc(Ctr);
              Ch := Source[Ctr];
              if Ch <> BrClose
                then SubStr := SubStr + Source[Ctr]
              else dec(BrCnt);
              if Ch = BrOpen then inc(BrCnt);
            end;

              // Turn Alt on
            if not AltOn then
            begin
              keybd_event(VK_MENU, MapVirtualKey(VK_MENU, 0), 0, 0);
              AltOn := True;
            end;

              // Push the keys
            if BrOpen = '('
              then PushKeys(SubStr)
            else PushFnKey(SubStr);

              // Turn alt off
            keybd_event(VK_MENU, MapVirtualKey(VK_MENU, 0), KEYEVENTF_KEYUP, 0);
            AltOn := False;
          end
          else
          begin
              // The next key is Alted
            inc(Ctr);
            if not AltOn then keybd_event(VK_MENU, MapVirtualKey(VK_MENU, 0), 0, 0);
            PushAKey(Source[Ctr]);
            keybd_event(VK_MENU, MapVirtualKey(VK_MENU, 0), KEYEVENTF_KEYUP, 0);
          end;
        end;
      '{': // Function keys
        begin
          inc(Ctr);
          SubStr := '';
          while (Ctr < Length(Source)) and (Source[Ctr] <> '}') do
          begin
            SubStr := SubStr + Source[Ctr];
            inc(Ctr);
          end;
           // Right brace
          if (Source[Ctr] = '}') and (Source[Ctr + 1] = '}') then
          begin
            PushShiftKey('}');
            inc(Ctr);
          end
          else PushFnKey(SubStr);
        end;
      'A'..'Z',
        '!', '@',
        '#', '$',
        '&', '*',
        '(', ')',
        '_', '|',
        '''', ':',
        '?', '>',
        '<': // Shifted keys
        PushShiftKey(Source[Ctr]);

      '~': // The enter key
        PushFnKey('ENTER');

    else PushAKey(Source[Ctr]);
    end;

    inc(Ctr);
  end;

  // Turn shift off if need be
  if ShiftOn then keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), KEYEVENTF_KEYUP, 0);
  // Turn control off if need be
  if ControlOn then keybd_event(VK_CONTROL, MapVirtualKey(VK_CONTROL, 0), KEYEVENTF_KEYUP, 0);
  // Turn alt off if need be
  if AltOn then keybd_event(VK_MENU, MapVirtualKey(VK_MENU, 0), KEYEVENTF_KEYUP, 0);
end;

////////////////////////////////////////////////////////////////////////
// Outputs function key. KeyCode may have a number of times to be output

procedure PushFnKey(KeyCode: string);
var
  NumPushes: Longint;
  Ctr: Integer;
  FnKey: string;
  OrigCode: string;
begin
  // Work out which function key to push and how many times
  if Pos(' ', KeyCode) > 0 then
  begin
     // We're doing this multpile times
     // Find the rightmost space. I do this because I want to allow people to
     // output multiple words with spaces in them. eg '{Seven times 7}'
    Ctr := Length(KeyCode);
    while (KeyCode[Ctr] <> ' ') do dec(Ctr);
    OrigCode := Copy(KeyCode, 1, Ctr - 1);
    FnKey := UpperCase(OrigCode);
     // This may fail if not a number, in which case NumPushes will be 1
    try
      NumPushes := StrToInt(Copy(KeyCode, Ctr + 1, Length(KeyCode)));
    except NumPushes := 1;
    end;
  end
  else
  begin
    // Once only
    NumPushes := 1;
    FnKey := UpperCase(KeyCode);
    OrigCode := KeyCode;
  end;

  // Function Keys
  // BTW, if you're reading my code, I do the for-next loop in each case rather
  // than outside the select because I figured it might be faster to do one Select
  // and the for-next loop n-times than do both the for-next and the Select n-times.
  if (FnKey = 'BACKSPACE') or (FnKey = 'BS') or (FnKey = 'BKSP') // Backspace
    then for Ctr := 1 to NumPushes do PressKey(VK_BACK, MapVirtualKey(VK_BACK, 0))

  else if FnKey = 'BELL' // Bell = ^G
    then for Ctr := 1 to NumPushes do PressKey(VK_BELL, MapVirtualKey(VK_BELL, 0))

  else if FnKey = 'BREAK' // Break = ^C
    then for Ctr := 1 to NumPushes do PressKey(VK_BREAK, MapVirtualKey(VK_BREAK, 0))

  else if (FnKey = 'CAPSLOCK') or (FnKey = 'CAPS') // Capslock
    then for Ctr := 1 to NumPushes do PressKey(VK_CAPITAL, MapVirtualKey(VK_CAPITAL, 0))

  else if (FnKey = 'DELETE') or (FnKey = 'DEL') // Delete
    then for Ctr := 1 to NumPushes do PressKey(VK_DELETE, MapVirtualKey(VK_DELETE, 0))

  else if FnKey = 'DOWN' // Down Arrow
    then for Ctr := 1 to NumPushes do PressKey(VK_DOWN, MapVirtualKey(VK_DOWN, 0))

  else if FnKey = 'END' // End
    then for Ctr := 1 to NumPushes do PressKey(VK_END, MapVirtualKey(VK_END, 0))

  else if (FnKey = 'ENTER') or (FnKey = 'RETURN') // Enter = ^M
    then for Ctr := 1 to NumPushes do PressKey(VK_RETURN, MapVirtualKey(VK_RETURN, 0))

  else if (FnKey = 'ESCAPE') or (FnKey = 'ESC') // Escape = ^[
    then for Ctr := 1 to NumPushes do PressKey(VK_ESCAPE, MapVirtualKey(VK_ESCAPE, 0))

  else if (FnKey = 'FORMFEED') or (FnKey = 'FF') // Form feed = ^L
    then for Ctr := 1 to NumPushes do PressKey(VK_CLEAR, MapVirtualKey(VK_CLEAR, 0))

  else if FnKey = 'HELP' // Help
    then for Ctr := 1 to NumPushes do PressKey(VK_HELP, MapVirtualKey(VK_HELP, 0))

  else if FnKey = 'HOME' // Home
    then for Ctr := 1 to NumPushes do PressKey(VK_HOME, MapVirtualKey(VK_HOME, 0))

  else if (FnKey = 'INSERT') or (FnKey = 'INS') // Insert
    then for Ctr := 1 to NumPushes do PressKey(VK_INSERT, MapVirtualKey(VK_INSERT, 0))

  else if FnKey = 'LEFT' // Left Arrow
    then for Ctr := 1 to NumPushes do PressKey(VK_LEFT, MapVirtualKey(VK_LEFT, 0))

  else if (FnKey = 'LINEFEED') or (FnKey = 'LF') // Linefeed = ^J
    then for Ctr := 1 to NumPushes do PressKey(VK_LINEFEED, MapVirtualKey(VK_LINEFEED, 0))

  else if (FnKey = 'NEWLINE') or (FnKey = 'NL') // New line = Carraige return & Line Feed = ^M^J
    then for Ctr := 1 to NumPushes do
    begin
      PressKey(VK_RETURN, MapVirtualKey(VK_RETURN, 0));
      PressKey(VK_LINEFEED, MapVirtualKey(VK_LINEFEED, 0));
    end

  else if FnKey = 'NUMLOCK' // Numeric Lock
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMLOCK, MapVirtualKey(VK_NUMLOCK, 0))

  else if (FnKey = 'PGDN') or (FnKey = 'PAGEDOWN') or (FnKey = 'NEXT') // Page Down
    then for Ctr := 1 to NumPushes do PressKey(VK_NEXT, MapVirtualKey(VK_NEXT, 0))

  else if (FnKey = 'PGUP') or (FnKey = 'PAGEUP') or (FnKey = 'PRIOR') // Page Up
    then for Ctr := 1 to NumPushes do PressKey(VK_PRIOR, MapVirtualKey(VK_PRIOR, 0))

  else if (FnKey = 'PRINTSCREEN') or (FnKey = 'PRTSC') // Print Screen
    then for Ctr := 1 to NumPushes do PressKey(VK_PRINT, MapVirtualKey(VK_PRINT, 0))

  else if FnKey = 'RIGHT' // Right Arrow
    then for Ctr := 1 to NumPushes do PressKey(VK_RIGHT, MapVirtualKey(VK_RIGHT, 0))

  else if (FnKey = 'SCROLLLOCK') or (FnKey = 'SCRLK') // Scroll lock
    then for Ctr := 1 to NumPushes do PressKey(VK_SCROLL, MapVirtualKey(VK_SCROLL, 0))

  else if (FnKey = 'TAB') // Tab = ^I
    then for Ctr := 1 to NumPushes do PressKey(VK_TAB, MapVirtualKey(VK_TAB, 0))

  else if (FnKey = 'UP') // Up Arrow
    then for Ctr := 1 to NumPushes do PressKey(VK_UP, MapVirtualKey(VK_UP, 0))

  else if (FnKey = 'SLEEP') // We sleep for a few seconds
    then Sleep(NumPushes * 1000)

  else if (FnKey = 'F1') // F1
    then for Ctr := 1 to NumPushes do PressKey(VK_F1, MapVirtualKey(VK_F1, 0))

  else if (FnKey = 'F2') // F2
    then for Ctr := 1 to NumPushes do PressKey(VK_F2, MapVirtualKey(VK_F2, 0))

  else if (FnKey = 'F3') // F3
    then for Ctr := 1 to NumPushes do PressKey(VK_F3, MapVirtualKey(VK_F3, 0))

  else if (FnKey = 'F4') // F4
    then for Ctr := 1 to NumPushes do PressKey(VK_F4, MapVirtualKey(VK_F4, 0))

  else if (FnKey = 'F5') // F5
    then for Ctr := 1 to NumPushes do PressKey(VK_F5, MapVirtualKey(VK_F5, 0))

  else if (FnKey = 'F6') // F6
    then for Ctr := 1 to NumPushes do PressKey(VK_F6, MapVirtualKey(VK_F6, 0))

  else if (FnKey = 'F7') // F7
    then for Ctr := 1 to NumPushes do PressKey(VK_F7, MapVirtualKey(VK_F7, 0))

  else if (FnKey = 'F8') // F8
    then for Ctr := 1 to NumPushes do PressKey(VK_F8, MapVirtualKey(VK_F8, 0))

  else if (FnKey = 'F9') // F9
    then for Ctr := 1 to NumPushes do PressKey(VK_F9, MapVirtualKey(VK_F9, 0))

  else if (FnKey = 'F10') // F10
    then for Ctr := 1 to NumPushes do PressKey(VK_F10, MapVirtualKey(VK_F10, 0))

  else if (FnKey = 'F11') // F11
    then for Ctr := 1 to NumPushes do PressKey(VK_F11, MapVirtualKey(VK_F11, 0))

  else if (FnKey = 'F12') // F12
    then for Ctr := 1 to NumPushes do PressKey(VK_F12, MapVirtualKey(VK_F12, 0))

  else if (FnKey = 'F13') // F13
    then for Ctr := 1 to NumPushes do PressKey(VK_F13, MapVirtualKey(VK_F13, 0))

  else if (FnKey = 'F14') // F14
    then for Ctr := 1 to NumPushes do PressKey(VK_F14, MapVirtualKey(VK_F14, 0))

  else if (FnKey = 'F15') // F15
    then for Ctr := 1 to NumPushes do PressKey(VK_F15, MapVirtualKey(VK_F15, 0))

  else if (FnKey = 'F16') // F16
    then for Ctr := 1 to NumPushes do PressKey(VK_F16, MapVirtualKey(VK_F16, 0))

  else if (FnKey = 'F17') // F17
    then for Ctr := 1 to NumPushes do PressKey(VK_F17, MapVirtualKey(VK_F17, 0))

  else if (FnKey = 'F18') // F18
    then for Ctr := 1 to NumPushes do PressKey(VK_F18, MapVirtualKey(VK_F18, 0))

  else if (FnKey = 'F19') // F19
    then for Ctr := 1 to NumPushes do PressKey(VK_F19, MapVirtualKey(VK_F19, 0))

  else if (FnKey = 'F20') // F20
    then for Ctr := 1 to NumPushes do PressKey(VK_F20, MapVirtualKey(VK_F20, 0))

  else if (FnKey = 'F21') // F21
    then for Ctr := 1 to NumPushes do PressKey(VK_F21, MapVirtualKey(VK_F21, 0))

  else if (FnKey = 'F22') // F22
    then for Ctr := 1 to NumPushes do PressKey(VK_F22, MapVirtualKey(VK_F22, 0))

  else if (FnKey = 'F23') // F23
    then for Ctr := 1 to NumPushes do PressKey(VK_F23, MapVirtualKey(VK_F23, 0))

  else if (FnKey = 'F24') // F24
    then for Ctr := 1 to NumPushes do PressKey(VK_F24, MapVirtualKey(VK_F24, 0))

  else if (FnKey = 'NUMPAD0') // Numeric Keypad 0
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD0, MapVirtualKey(VK_NUMPAD0, 0))

  else if (FnKey = 'NUMPAD1') // Numeric Keypad 1
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD1, MapVirtualKey(VK_NUMPAD1, 0))

  else if (FnKey = 'NUMPAD2') // Numeric Keypad 2
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD2, MapVirtualKey(VK_NUMPAD2, 0))

  else if (FnKey = 'NUMPAD3') // Numeric Keypad 3
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD3, MapVirtualKey(VK_NUMPAD3, 0))

  else if (FnKey = 'NUMPAD4') // Numeric Keypad 4
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD4, MapVirtualKey(VK_NUMPAD4, 0))

  else if (FnKey = 'NUMPAD5') // Numeric Keypad 5
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD5, MapVirtualKey(VK_NUMPAD5, 0))

  else if (FnKey = 'NUMPAD6') // Numeric Keypad 6
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD6, MapVirtualKey(VK_NUMPAD6, 0))

  else if (FnKey = 'NUMPAD7') // Numeric Keypad 7
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD7, MapVirtualKey(VK_NUMPAD7, 0))

  else if (FnKey = 'NUMPAD8') // Numeric Keypad 8
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD8, MapVirtualKey(VK_NUMPAD8, 0))

  else if (FnKey = 'NUMPAD9') // Numeric Keypad 9
    then for Ctr := 1 to NumPushes do PressKey(VK_NUMPAD9, MapVirtualKey(VK_NUMPAD9, 0))

  else if (FnKey = 'NUMPADMULTIPLY') or (FnKey = 'NUMPAD*') // Numeric Keypad Multiply
    then for Ctr := 1 to NumPushes do PressKey(VK_MULTIPLY, MapVirtualKey(VK_MULTIPLY, 0))

  else if (FnKey = 'NUMPADADD') or (FnKey = 'NUMPAD+') // Numeric Keypad +
    then for Ctr := 1 to NumPushes do PressKey(VK_ADD, MapVirtualKey(VK_ADD, 0))

  else if (FnKey = 'NUMPADSUBTRACT') or (FnKey = 'NUMPAD-') // Numeric Keypad -
    then for Ctr := 1 to NumPushes do PressKey(VK_SUBTRACT, MapVirtualKey(VK_SUBTRACT, 0))

  else if (FnKey = 'NUMPADDECIMAL') or (FnKey = 'NUMPAD.') // Numeric Keypad .
    then for Ctr := 1 to NumPushes do PressKey(VK_DECIMAL, MapVirtualKey(VK_DECIMAL, 0))

  else if (FnKey = 'NUMPADDIVIDE') or (FnKey = 'NUMPAD/') // Numeric Keypad /
    then for Ctr := 1 to NumPushes do PressKey(VK_DIVIDE, MapVirtualKey(VK_DIVIDE, 0))

  else if (FnKey = '~') or (FnKey = '!') or (FnKey = '@') or (FnKey = '#') or (FnKey = '$')
    or (FnKey = '%') or (FnKey = '^') or (FnKey = '&') or (FnKey = '*') or (FnKey = '(')
    or (FnKey = ')') or (FnKey = '_') or (FnKey = '+') or (FnKey = '|') or (FnKey = '{')
    or (FnKey = '}') or (FnKey = '''') or (FnKey = ':') or (FnKey = '?') or (FnKey = '>')
    or (FnKey = '<') // Shifted keys
    then for Ctr := 1 to NumPushes do PushShiftKey(OrigCode)

  else // Ordinary keys
    for Ctr := 1 to NumPushes do PushKeys(OrigCode);
end;

end.

