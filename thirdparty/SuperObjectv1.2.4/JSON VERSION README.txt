
VERSION:  
  Downloaded March 28, 2013 (last code update: Oct 16, 2012)
  The version is beyond the 1.2.4 to SUPPORT UNICODE

SOURCE REPOSITORY:
  https://code.google.com/p/superobject

NOTES:
  Minor changes were added to support XE3.
  More signification changes to RTTI support so .ToJson and .FromJson class helpers are more flexible.
  (see: https://code.google.com/p/superobject/issues/detail?id=16)

