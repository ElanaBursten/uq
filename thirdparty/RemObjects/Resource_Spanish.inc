ResourceString

       _WSAEINTR            = 'Procedura de sistema interrumpida'; // 10004
       _WSAEBADF            = 'N�mero de archivo incorrecto'; // 10009
       _WSAEACCES           = 'Permiso negado'; // 10013
       _WSAEFAULT           = 'Direcci�n incorrecta'; // 10014
       _WSAEINVAL           = 'Argumento inv�lido'; // 10022
       _WSAEMFILE           = 'Demasiados archivos abiertos'; // 10024
       _WSAEWOULDBLOCK      = 'Operaci�n se bloquear�a'; // 10035
       _WSAEINPROGRESS      = 'Operaci�n ahora en marcha'; // 10036
       _WSAEALREADY         = 'Operaci�n ya en marcha'; // 10037
       _WSAENOTSOCK         = 'Operaci�n de socket en un non-socket'; // 10038
       _WSAEDESTADDRREQ     = 'Direcci�n del destinatario requerido'; // 10039
       _WSAEMSGSIZE         = 'Mensaje demasiado largo'; // 10040
       _WSAEPROTOTYPE       = 'Tipo de protocolo incorrecto para el socket'; // 10041
       _WSAENOPROTOOPT      = 'Protocolo no disponible'; // 10042
       _WSAEPROTONOSUPPORT  = 'Protocolo no soportado'; // 10043
       _WSAESOCKTNOSUPPORT  = 'Tipo de socket no soportado'; // 10044
       _WSAEOPNOTSUPP       = 'Operaci�n no soportada en socket'; // 10045
       _WSAEPFNOSUPPORT     = 'Familia de protocolos no soportada'; // 10046
       _WSAEAFNOSUPPORT     = 'Familia de direcciones no soportada por la familia de protocolos'; // 10047
       _WSAEADDRINUSE       = 'Direcci�n ya en uso'; // 10048
       _WSAEADDRNOTAVAIL    = 'No se puede asignar la direcci�n solicitada'; // 10049
       _WSAENETDOWN         = 'La red est� parada'; // 10050
       _WSAENETUNREACH      = 'La red est� inutilizable'; // 10051
       _WSAENETRESET        = 'La red interrumpi� la conexi�n'; // 10052
       _WSAECONNABORTED     = 'El sistema caus� la interrupci�n de la conexi�n'; // 10053
       _WSAECONNRESET       = 'Conexi�n interrumpida'; // 10054
       _WSAENOBUFS          = 'No hay espacio disponible en el buffer'; // 10055
       _WSAEISCONN          = 'Socket ya est� conectado'; // 10056
       _WSAENOTCONN         = 'Socket no est� conectado'; // 10057
       _WSAESHUTDOWN        = 'Imposible enviar cuando el socket esta cerrado'; // 10058
       _WSAETOOMANYREFS     = 'Demasiadas referencias'; // 10059
       _WSAETIMEDOUT        = 'Conexi�n tuvo un timeout'; // 10060
       _WSAECONNREFUSED     = 'Conexi�n rechazada'; // 10061
       _WSAELOOP            = 'Demasiados niveles de links simb�licos'; // 10062
       _WSAENAMETOOLONG     = 'El nombre del archivo es muy grande'; // 10063
       _WSAEHOSTDOWN        = 'Host est� parado'; // 10064
       _WSAEHOSTUNREACH     = 'No hay ruta para el host'; // 10065
       _WSAENOTEMPTY        = 'El directorio no est� vac�o'; // 10066
       _WSAEPROCLIM         = 'Demasiados procesos'; // 10067
       _WSAEUSERS           = 'Demasiados usuarios'; // 10068
       _WSAEDQUOT           = 'Contingente del disco excedido'; // 10069
       _WSAESTALE           = 'NFS handle archivo vencido'; // 10070
       _WSAEREMOTE          = 'Demasiados niveles remotos en el path'; // 10071
       _WSASYSNOTREADY      = 'Sub-sistema de la red inutilizable'; // 10091
       _WSAVERNOTSUPPORTED  = 'WSOCK32.DLL DLL no puede soportar esta aplicaci�n'; // 10092
       _WSANOTINITIALISED   = 'WSOCK32.DLL no fue inicializado'; // 10093
       _WSAHOST_NOT_FOUND   = 'Host no fue encontrado'; // 11001
       _WSATRY_AGAIN        = 'Host no-autoritativo no fue encontrado'; // 11002
       _WSANO_RECOVERY      = 'No recuperable error'; // 11003
       _WSANO_DATA          = 'No hay datos'; // 11004
       _WSAUNKNOWN          = 'Error de socket desconocido';


