unit uROMSXMLImpl;

interface

uses Classes, ActiveX, uROMSXML2_TLB, uROXMLIntf;

type { TROMSXMLNode }
     TROMSXMLNode = class(TInterfacedObject, IXMLNode)
     private
       fNode : IXMLDOMNode;

     protected
       function GetName : widestring;
       function GetLocalName : widestring;
       function GetRef : pointer;

       function GetParent : IXMLNode;

       function GetValue : Variant;
       procedure SetValue(const Value : Variant);

       function GetXML : widestring;

       function GetAttributes(Index : integer) : IXMLNode;
       function GetAttributeCount : integer;

       function GetChildren(Index : integer) : IXMLNode;
       function GetChildrenCount : integer;

       function Add(const aNodeName : widestring) : IXMLNode;
       function AddAttribute(const anAttributeName : widestring; const anAttributeValue : Variant) : IXMLNode;

       procedure Delete(Index : integer);
       procedure Remove(const aNode : IXMLNode);

       function GetNodeByName(const aNodeName : widestring) : IXMLNode; // Returns NIL if none is found or exception. Up to you.

       function GetAttributeByName(const anAttributeName : widestring) : IXMLNode;

       function GetNodesByName(const aNodeName : widestring) : IXMLNodeList; // Returns NIL if none are found or exception. Up to you.

       function GetNodeByAttribute(const anAttributeName, anAttributeValue : widestring) : IXMLNode;

     public
       constructor Create(const aNode : IXMLDOMNode);
       destructor Destroy; override;
     end;

     { TROMSXMLNodeList }
     TROMSXMLNodeList = class(TInterfacedObject, IXMLNodeLIst)
     private
       fNodeList : IXMLDOMNodeList;
     protected
       function GetNodes(Index : integer) : IXMLNode;
       function GetCount : integer;

     public
       constructor Create(const aNodeList : IXMLDOMNodeList);
       destructor Destroy; override;
     end;

     { TROMSXMLDocument }
     TROMSXMLDocument = class(TInterfacedObject, IXMLDocument)
     private
       fDocument : IXMLDOMDocument2;

     protected
       function GetDocumentNode : IXMLNode;

       function GetEncoding : TXMLEncoding;

       procedure New(aDocumentName : widestring = ''; anEncoding : TXMLEncoding = xeUTF8);

       procedure SaveToStream(aStream : TStream);
       procedure SaveToFile(const aFileName : string);
       procedure LoadFromStream(aStream : TStream);
       procedure LoadFromFile(const aFileName : string);

     public
       constructor Create;
       destructor Destroy; override;

     end;

implementation

uses uROXMLRes, SysUtils;

{ TROMSXMLNode }

constructor TROMSXMLNode.Create(const aNode : IXMLDOMNode);
begin
  if not Assigned(aNode)
    then RaiseError(err_DOMElementIsNIL, []);

  inherited Create;

  fNode := aNode;
end;

destructor TROMSXMLNode.Destroy;
begin
  //fNode := NIL;
  inherited;
end;

function TROMSXMLNode.Add(const aNodeName: widestring): IXMLNode;
var node : IXMLDOMNode;
begin
  node := fNode.ownerDocument.createElement(aNodeName);
  fNode.appendChild(node);

  result := TROMSXMLNode.Create(node);
end;

procedure TROMSXMLNode.Delete(Index: integer);
begin
  fNode.removeChild(fNode.childNodes.item[Index])
end;

function TROMSXMLNode.GetAttributes(Index : integer): IXMLNode;
begin
  result := TROMSXMLNode.Create(fNode.attributes.item[Index]);
end;

function TROMSXMLNode.GetChildren(Index : integer): IXMLNode;
begin
  result := TROMSXMLNode.Create(fNode.childNodes.item[Index])
end;

function TROMSXMLNode.GetName: widestring;
begin
  result := fNode.nodeName
end;

function TROMSXMLNode.GetXML: widestring;
begin
  result := fNode.XML;
end;

procedure TROMSXMLNode.Remove(const aNode: IXMLNode);
begin
  fNode.removeChild(IXMLDOMNode(aNode.Ref))
end;

function TROMSXMLNode.GetValue: Variant;
begin
  result := fNode.text
end;

procedure TROMSXMLNode.SetValue(const Value: Variant);
begin
  fNode.text := Value
end;

function TROMSXMLNode.GetNodeByName(const aNodeName: widestring): IXMLNode;
var //list : IXMLDOMNodeList;
    node : IXMLDOMNode;
begin
  result := NIL;

  node := fNode.selectSingleNode(aNodeName);

  if (node<>NIL)
    then result := TROMSXMLNode.Create(node);
end;

function TROMSXMLNode.GetNodeByAttribute(const anAttributeName,
  anAttributeValue: widestring): IXMLNode;
var i : integer;
begin
  result := NIL;

  for i := 0 to (fNode.attributes.length-1) do
    with fNode.attributes.item[i] do
      if (nodeName=anAttributeName) and (nodeValue=anAttributeValue) then begin
        result := TROMSXMLNode.Create(fNode.attributes.item[i]);
        Exit;
      end;
end;

function TROMSXMLNode.GetNodesByName(const aNodeName: widestring): IXMLNodeList;
var list : IXMLDOMNodeList;
begin
  result := NIL;

  list := fNode.selectNodes(aNodeName);

  if (list<>NIL) or (list.length>0)
    then result := TROMSXMLNodeList.Create(list);
end;

function TROMSXMLNode.GetAttributeByName(
  const anAttributeName: widestring): IXMLNode;
var node : IXMLDOMNode;
begin
  result := NIL;
  node := fNode.attributes.getNamedItem(anAttributeName);

  if (node<>NIL)
    then result := TROMSXMLNode.Create(node);
end;

function TROMSXMLNode.GetRef: pointer;
begin
  result := pointer(fNode);
end;

function TROMSXMLNode.GetLocalName: widestring;
begin
  result := fNode.baseName
end;

function TROMSXMLNode.AddAttribute(const anAttributeName : widestring;
  const anAttributeValue : Variant) : IXMLNode;
var attr : IXMLDOMAttribute;
begin
  attr := fNode.ownerDocument.createAttribute(anAttributeName);
  attr.value := anAttributeValue;
  fNode.attributes.setNamedItem(attr);

  result := TROMSXMLNode.Create(attr);
end;

function TROMSXMLNode.GetParent: IXMLNode;
begin
  result := NIL;
  if (fNode.parentNode<>NIL)
    then result := TROMSXMLNode.Create(fNode.parentNode);
end;

function TROMSXMLNode.GetAttributeCount: integer;
begin
  result := fNode.attributes.length
end;

function TROMSXMLNode.GetChildrenCount: integer;
begin
  result := fNode.childNodes.length
end;

{ TROMSXMLNodeList }

constructor TROMSXMLNodeList.Create(const aNodeList: IXMLDOMNodeList);
begin
  if not Assigned(aNodeList)
    then RaiseError(err_DOMElementIsNIL, []);

  inherited Create;
  
  fNodeList := aNodeList;
end;

destructor TROMSXMLNodeList.Destroy;
begin
//  fNodeList := NIL;
  inherited;
end;

function TROMSXMLNodeList.GetCount: integer;
begin
  result := fNodeList.length
end;

function TROMSXMLNodeList.GetNodes(Index: integer): IXMLNode;
begin
  result := TROMSXMLNode.Create(fNodeList.item[Index])
end;

{ TROMSXMLDocument }

constructor TROMSXMLDocument.Create;
begin
  inherited Create;
  fDocument := NIL;
end;

destructor TROMSXMLDocument.Destroy;
begin
 // fDocument := NIL;
  inherited;
end;

function TROMSXMLDocument.GetDocumentNode: IXMLNode;
begin
  result := TROMSXMLNode.Create(fDocument.documentElement)
end;

function TROMSXMLDocument.GetEncoding: TXMLEncoding;
begin
  result := xeUTF8; // TODO: Complete the UTF8/16 differentiation.
end;

procedure TROMSXMLDocument.LoadFromFile(const aFileName: string);
begin
  if not fDocument.load(aFileName)
    then with fDocument.parseError do
      RaiseError(err_CannotLoadXMLDocument, [Reason, Line, linePos]);
end;

procedure TROMSXMLDocument.LoadFromStream(aStream: TStream);
var adapter : IStream;
begin
  aStream.Position := 0;
  adapter := TStreamAdapter.Create(aStream, soReference);
  if not fDocument.load(adapter) then
    with fDocument.parseError do
      RaiseError(err_CannotLoadXMLDocument, [Reason, Line, linePos]);
end;

procedure TROMSXMLDocument.New(aDocumentName : widestring = ''; anEncoding : TXMLEncoding = xeUTF8);
begin
  fDocument := CoDOMDocument30.Create;
  if (aDocumentName<>'')
    then fDocument.documentElement := fDocument.createElement(aDocumentName);
end;

procedure TROMSXMLDocument.SaveToFile(const aFileName: string);
begin
  fDocument.save(aFileName);
end;

procedure TROMSXMLDocument.SaveToStream(aStream: TStream);
var adapter : IStream;
begin
  adapter := TStreamAdapter.Create(aStream, soReference);
  fDocument.save(adapter);
end;

initialization
  //CoInitializeEx(nil, COINIT_MULTITHREADED or COINIT_SPEED_OVER_MEMORY);

finalization
  //CoUninitialize;

end.
