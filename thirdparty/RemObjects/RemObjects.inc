{$I eDefines.inc}

{ DXSock }

    { To use RemObjects with the standalone version of DXSock, simply remove the
      define below. Note that this has only been tested with DXSock 3.0, and that
      doing so requires a separate license for DXSock from Brain PatchWorks
      see http://www.bpdx.com for more information }

    {$DEFINE RemObjects_USE_RODX}

{ Indy }

    {$IFDEF DELPHI7UP}
      {$DEFINE RemObjects_INDY9}
    {$ELSE}
      {$DEFINE RemObjects_INDY8}
    {$ENDIF}

    {$IFDEF KYLIX3UP}
      {$DEFINE RemObjects_INDY9}
    {$ELSE}
      {$DEFINE RemObjects_INDY8}
    {$ENDIF}

    { If you are using Indy 9 in Delphi 6 just uncomment the Indy9 DEFINE below,
      and remove the Indy package references from the Requires section of
      RemObjects_Indy_D6.dpk before compiling your RemObjects Indy package. }

    {$DEFINE RemObjects_INDY9}

    {$IFDEF RemObjects_INDY9}
      {$UNDEF RemObjects_INDY8}
    {$ENDIF}

{ XML }

    {$IFDEF LINUX}
      {$DEFINE RemObjects_OpenXML}
    {$ELSE}
      {$DEFINE RemObjects_MSXML}
    {$ENDIF}

{ Encryption }

    {$IFDEF MSWINDOWS}
      {$DEFINE RemObjects_UseEncryption }
    {$ENDIF MSWINDOWS}

