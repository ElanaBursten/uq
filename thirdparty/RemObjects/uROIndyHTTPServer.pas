{ Summary: Contains the TROIndyHTTPServer class }
unit uROIndyHTTPServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Indy Components
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

{TODO: Do like in the webmodule. Save a pointer to the user's OnCOmmandGet and fire it }

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROServer, uROIndyTCPServer, IdTCPServer, IdThreadMgr,
  IdHTTPServer, uROClientIntf
  {$IFDEF RemObjects_INDY9}, IdCustomHTTPServer{$ENDIF};

type {--------------- TIndyHTTPTransport ---------------}

     { Summary: Implementation of the IROTransport, IROTCPTransport and IROHTTPTransport interfaces for TROIndyHTTPServer servers
       Description:
         Implementation of the IROTransport, IROTCPTransport and IROHTTPTransport interfaces for TROIndyHTTPServer servers.
         This class allows you to access the Indy response and request classes invloved in the HTTP dispatch of the message.
         See TROServer.DispatchMessage for more information. }

     TIndyHTTPTransport = class(TInterfacedObject, IROTransport, IROTCPTransport, IROHTTPTransport)
     private
       fRequestInfo: TIdHTTPRequestInfo;
       fResponseInfo : TIdHTTPResponseInfo;

     protected
       {--------------- IROHTTPTransport ---------------}
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;
       function GetContentType : string;
       procedure SetContentType(const aValue : string);
       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);
       function GetTargetURL : string;
       procedure SetTargetURL(const aValue : string);

       function GetPathInfo : string;
       function GetLocation : string;

       {--------------- IROTransport ---------------}
       function GetTransportObject : TObject;
       function GetClientAddress : string;

     public
       constructor Create(aRequestInfo: TIdHTTPRequestInfo; aResponseInfo : TIdHTTPResponseInfo);

       { Summary: Provides access to the HTTP request associated to the HTTP call.
         Description: Provides access to the HTTP request associated to the HTTP call. }
       property RequestInfo: TIdHTTPRequestInfo read fRequestInfo;

       { Summary: Provides access to the HTTP response associated to the HTTP call.
         Description: Provides access to the HTTP response associated to the HTTP call. }
       property ResponseInfo : TIdHTTPResponseInfo read fResponseInfo;
     end;

     {--------------- TROIdHTTPServer ---------------}

     { Summary: Internal. TIdHTTPServer descendant that hides the Active property.
       Description:
         Internal. TIdHTTPServer descendant that hides the Active property.
         This has been done to prevent user from accessing the Active property directly in
         the Object Inspector. This should be done using TROIndyHTTPServer.Active. }
     TROIdHTTPServer = class(TIdHTTPServer)
     private
       function GetActive: boolean;

     public
       procedure IndySetActive(Value : boolean);

     published
       { Summary: Used to verify if the Indy HTTP server is listening on the specified port for HTTP requests.
         Description: Used to verify if the Indy HTTP server is listening on the specified port for HTTP requests. }
       property Active : boolean read GetActive;
     end;

     {--------------- TROIndyHTTPServer ---------------}

     { Summary: RemObjects HTTP server based on Nevrona's Indy.
       Description: RemObjects HTTP server based on Nevrona's Indy.
     }
     TROIndyHTTPServer = class(TROIndyTCPServer)
     private
       function GetIndyServer: TROIdHTTPServer;

     protected
       procedure InternalServerCommandGet(AThread: TIdPeerThread;
         RequestInfo: TIdHTTPRequestInfo; ResponseInfo: TIdHTTPResponseInfo); virtual;

       function CreateIndyServer : TIdTCPServer; override;
       procedure IntSetActive(const Value: boolean); override;
       function IntGetActive : boolean; override;

       function GetDispatchersClass : TROMessageDispatchersClass; override;

     public
       constructor Create(aComponent: TComponent); override;

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property IndyServer : TROIdHTTPServer read GetIndyServer;
     end;

implementation

uses SysUtils, IdGlobal, uRORes, uROHTTPTools, uROHTTPDispatch;

{ TROIndyHTTPServer }
constructor TROIndyHTTPServer.Create(aComponent: TComponent);
begin
  inherited;
end;

function TROIndyHTTPServer.CreateIndyServer: TIdTCPServer;
begin
  result := TROIdHTTPServer.Create(Self);//TIdHTTPServer.Create(Self);
  TROIdHTTPServer(result).OnCommandGet := InternalServerCommandGet;
  TROIdHTTPServer(result).DefaultPort := 8099;
end;

function TROIndyHTTPServer.IntGetActive: boolean;
begin
  result := GetIndyServer.Active
end;

procedure TROIndyHTTPServer.IntSetActive(const Value: boolean);
begin
  GetIndyServer.IndySetActive(Value);
end;

function TROIndyHTTPServer.GetIndyServer: TROIdHTTPServer;
begin
  result := TROIdHTTPServer(inherited IndyServer);
  //result := (inherited IndyServer) as TROIdHTTPServer;
end;

procedure TROIndyHTTPServer.InternalServerCommandGet(AThread: TIdPeerThread;
  RequestInfo: TIdHTTPRequestInfo; ResponseInfo: TIdHTTPResponseInfo);
var req, resp : TStringStream;
    transport : IROHTTPTransport;
    ok : boolean;
begin
  req := NIL;
  transport := TIndyHTTPTransport.Create(RequestInfo, ResponseInfo);

  resp := TStringStream.Create('');

  try
    req := TStringStream.Create(RequestInfo.UnparsedParams);
    ok := DispatchMessage(transport, req, resp);

    if ok then ResponseInfo.ResponseNo := HTTP_OK
          else ResponseInfo.ResponseNo := HTTP_FAILED;

    ResponseInfo.ContentText := resp.DataString;
  finally
    req.Free;
    resp.Free;
  end;
end;

function TROIndyHTTPServer.GetDispatchersClass : TROMessageDispatchersClass; 
begin
  result := TROHTTPMessageDispatchers
end;

{ TIndyHTTPTransport }

constructor TIndyHTTPTransport.Create(
  aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
begin
  inherited Create;
  fRequestInfo := aRequestInfo;
  fResponseInfo := aResponseInfo;
end;

function TIndyHTTPTransport.GetClientAddress: string;
begin
  result := fRequestInfo.Host;//RemoteIP
end;

function TIndyHTTPTransport.GetContentType: string;
begin
  result := fResponseInfo.ContentType;
end;

function TIndyHTTPTransport.GetHeaders(
  const aName: string): string;
begin
{$IFDEF RemObjects_INDY8}
  result := GetHeaderValue(fResponseInfo.Headers, aName);
{$ENDIF}
{$IFDEF RemObjects_INDY9}
  result := GetHeaderValue(fResponseInfo.RawHeaders, aName);
{$ENDIF}
end;

function TIndyHTTPTransport.GetPathInfo: string;
begin
  result := RequestInfo.Document
end;

function TIndyHTTPTransport.GetLocation: string;
begin
  result := 'http://'+fRequestInfo.Host
end;

function TIndyHTTPTransport.GetTargetURL: string;
begin
  result := ''
end;

function TIndyHTTPTransport.GetTransportObject: TObject;
begin
  result := Self;
end;

function TIndyHTTPTransport.GetUserAgent: string;
begin
  result := str_ProductName
end;

procedure TIndyHTTPTransport.SetContentType(const aValue: string);
begin
  fResponseInfo.ContentType := aValue
end;

procedure TIndyHTTPTransport.SetHeaders(const aName,
  aValue: string);
begin
{$IFDEF RemObjects_INDY8}
  SetHeaderValue(fResponseInfo.Headers, aName, aValue);
{$ENDIF}
{$IFDEF RemObjects_INDY9}
  SetHeaderValue(fResponseInfo.RawHeaders, aName, aValue);
{$ENDIF}
end;

procedure TIndyHTTPTransport.SetTargetURL(const aValue: string);
begin

end;

procedure TIndyHTTPTransport.SetUserAgent(const aValue: string);
begin
  fResponseInfo.ServerSoftware := str_ProductName
end;

{ TROIdHTTPServer }

function TROIdHTTPServer.GetActive: boolean;
begin
  result := inherited Active
end;

procedure TROIdHTTPServer.IndySetActive(Value: boolean);
begin
  inherited Active := Value
end;

initialization
  RegisterServerClass(TROIndyHTTPServer);

end.
