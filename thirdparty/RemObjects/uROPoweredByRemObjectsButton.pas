unit uROPoweredByRemObjectsButton;  { TRoPoweredByRemObjectsButton component. }

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF LINUX}
  Classes, QControls, QGraphics;
  {$ELSE}
  //ToDo: sort those out!
  Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  Menus,  StdCtrls, ExtCtrls;
  {$ENDIF}


{$IFDEF LINUX}
//ToDo: add image resource
{$ENDIF LINUX}
{$IFDEF MSWINDOWS}
{$R uROPoweredByRemObjectsButton.res uROPoweredByRemObjectsButton.rc}
{$ENDIF MSWINDOWS}

type
  TRoPoweredByRemObjectsButton = class(TGraphicControl)
  private
    fBitmap: TBitmap;
    FDummyByte: byte;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure Click; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override; 
  published
    { Published properties and events }
    { Hidden properties (FDummyByte is declared as a byte): }
    property Height: byte read FDummyByte;
    property Width: byte read FDummyByte;
    property Anchors;
  end;  { TRoPoweredByRemObjectsButton }

implementation

{$IFDEF MSWINDOWS}
uses ShellAPI;
{$ENDIF MSWINDOWS}

constructor TRoPoweredByRemObjectsButton.Create(AOwner: TComponent);
const STR_POWERED_BY_REMOBJECTS = 'POWERED_BY_REMOBJECTS';
begin
  inherited Create(AOwner);
  fBitmap := TBitmap.Create();
  fBitmap.LoadFromResourceName(hInstance,STR_POWERED_BY_REMOBJECTS);
  inherited Height := fBitmap.Height;
  inherited Width := fBitmap.Width;
  {$IFDEF MSWINDOWS}
  Cursor := crHandPoint;
  {$ENDIF MSWINDOWS}
end;  { Create }

destructor TRoPoweredByRemObjectsButton.Destroy;
begin
  { ToDo -cCDK: Free allocated memory and created objects here. }
  inherited Destroy;
end;  { Destroy }

procedure TRoPoweredByRemObjectsButton.Paint;
begin
  inherited;
  Canvas.Draw(0,0,fBitmap);
end;

procedure TRoPoweredByRemObjectsButton.Click;
begin
  {$IFDEF MSWINDOWS}
  ShellExecute(0,'open','http://www.remobjects.com',nil,nil,SW_SHOWNORMAL);
  {$ENDIF MSWINDOWS}
end;


procedure TRoPoweredByRemObjectsButton.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  inherited SetBounds(ALeft,ATop,fBitmap.Width,fBitmap.Height);

end;

initialization
finalization
end.
