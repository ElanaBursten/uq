unit uROStreamHelpers;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
     {$IFDEF DEBUG_REMOBJECTS}eDebugServer,{$ENDIF DEBUG_REMOBJECTS}
     {$IFDEF DOTNET}
     Borland.Delphi.Classes;
     {$ELSE}
     Classes;
     {$ENDIF}

function Stream_ReadStringWithLength(iStream:TStream; iMaxLength:integer=-1): string;
procedure Stream_WriteStringWithLength(iStream:TStream; const iString: string);

implementation

uses uRORes;

function Stream_ReadStringWithLength(iStream:TStream; iMaxLength:integer=-1): string;
var lLength:integer;
    {$IFDEF DOTNET}
    lBuffer:array of byte;
    i:integer;
    {$ENDIF DOTNET}
begin
  iStream.Read(lLength, sizeof(lLength));

  if (iMaxLength > -1) and (lLength > iMaxLength) then
    RaiseError(err_InvalidStringLength,[lLength]);

  SetLength(result, lLength);
  if (lLength > 0) then begin
    {$IFDEF DOTNET}
    SetLength(lBuffer, lLength);
    iStream.Read(lBuffer, lLength);
    for i := 0 to lLength-1 do result[i+1] := chr(lBuffer[i]);
    {$ELSE}
    iStream.Read(result[1], lLength);
    {$ENDIF}
  end;
  //ToDo: find a save and FAST way to do this in .NET
end;

procedure Stream_WriteStringWithLength(iStream:TStream; const iString: string);
var lLength:integer;
    {$IFDEF DOTNET}
    lBuffer:array of byte;
    i:integer;
    {$ENDIF DOTNET}
begin
  lLength := Length(iString);
  iStream.Write(lLength, SizeOf(lLength));
  if (lLength > 0) then begin
    {$IFDEF DOTNET}
    SetLength(lBuffer, lLength);
    for i := 0 to lLength-1 do lBuffer[i] := ord(iString[i+1]);
    iStream.Write(lBuffer, lLength);
    {$ELSE}
    iStream.Write(iString[1], lLength);
    {$ENDIF}
  end;
  //ToDo: find a save and FAST way to do this in .NET
end;

end.
