unit uROEncKeyPropEditor;

{$I RemObjects.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$IFDEF DELPHI5} DsgnIntf, {$ELSE}  DesignIntf, DesignEditors, {$ENDIF}
  StdCtrls, Buttons, uROHCMngr;

{.$DEFINE TestComp}

type
  TROCryptoKeyProperty = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

  {$IFDEF TestComp}
  TBrazilCryptoTest = class(TComponent)
  private
    fkey: TBrazilCryptoKey;
    procedure SetKey(const Value: TBrazilCryptoKey);
  published
    property Key: TBrazilCryptoKey read fkey write SetKey;
  end;
  {$ENDIF}

  TCryptoKeyGenForm = class(TForm)
    eHashKey: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    LHAlgorithm: TLabel;
    CBHash: TComboBox;
    LCKey: TLabel;
    EKey: TEdit;
    LHashKey: TLabel;
    HashManager: TROHashManager;
    LHashInfo: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure EKeyChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBHashClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CryptoKeyGenForm  : TCryptoKeyGenForm;

procedure Register;

implementation

{$R *.dfm}

uses
  uROEncryption, uroHash, urodecutil;

procedure Register;
begin
  RegisterPropertyEditor(TypeInfo(TROCryptoKey), nil, '',
    TROCryptoKeyProperty);

  {$IFDEF TestComp}
  RegisterComponents('Brazil', [TBrazilCryptoTest]);
  {$ENDIF}
end;

{ TROCryptoKeyProperty }

procedure TROCryptoKeyProperty.Edit;
begin
  inherited;

  with TCryptoKeyGenForm.Create(nil) do
  try
    // Initialize form...
    ehashkey.Text := GetStrValue;
    // Show form...
    if ShowModal = mrOK then            // Transfer form properties back
    begin
      SetStrValue(ehashkey.Text);
    end;                                // if
  finally                               // wrap up
    Free;
  end;                                  // try/finally
end;

function TROCryptoKeyProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog];
end;

procedure TCryptoKeyGenForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  S                 : string;
begin
  try
    S := formattostr(PChar(ehashkey.Text), Length(ehashkey.Text), fmtHex);
  except
    on e: Exception do
    begin
      ShowMessage('Invalid key!');
      CanClose := False;
    end;
  end;                                  // try/except
end;

{$IFDEF TestComp}
{ TBrazilCryptoTest }

procedure TBrazilCryptoTest.SetKey(const Value: TBrazilCryptoKey);
begin
  fkey := Value;
end;
{$ENDIF}

procedure TCryptoKeyGenForm.EKeyChange(Sender: TObject);
begin
  EHashKey.Text := HashManager.HashClass.CalcString(EKey.Text, nil, fmtHEX);
end;

procedure TCryptoKeyGenForm.FormCreate(Sender: TObject);
begin
  HashNames(CBHash.Items);
end;

procedure TCryptoKeyGenForm.CBHashClick(Sender: TObject);
begin
  CBHash.ItemIndex := CBHash.ItemIndex;

{1. Variant to select the Hash}
  HashManager.Algorithm := CBHash.Text;
  LHashInfo.Caption := HashManager.Description (*+ ', ' +
                       HashManager.HashClass.ClassName*);

  EKeyChange(nil);
end;

end.

