unit uRODataSnapPublishedProvidersCollection;

interface

uses Classes, Provider;

type TPublishedProvider = class(TCollectionItem)
     private
       fProvider:TCustomProvider;
       fName: string;
       procedure SetProvider(const iValue:TCustomProvider);
       procedure SetName(const iValue:string);
     protected
       function GetDisplayName: string; override;
     published
       property Provider:TCustomProvider read fProvider write SetProvider;
       property Name:string read fName write SetName;
     end;

     TPublishedProviders = class(TCollection)
     private
       fOwner:TComponent;
       function GetItems(iIndex: integer): TPublishedProvider;
     protected
       function GetOwner: TPersistent; override;
     public
       constructor Create(iOwner:TComponent);
       property Items[iIndex:integer]:TPublishedProvider read GetItems; default;
       function GetProviderByName(iName:string):TPublishedProvider;
       procedure UnlinkProvider(iProvider:TCustomProvider);
     end;

implementation

{ TPublishedProviders }

constructor TPublishedProviders.Create(iOwner: TComponent);
begin
  inherited Create(TPublishedProvider);
  fOwner := iOwner;
end;

function TPublishedProviders.GetItems(iIndex: integer): TPublishedProvider;
begin
  result := (inherited Items[iIndex]) as TPublishedProvider;
end;

function TPublishedProviders.GetOwner: TPersistent;
begin
  result := fOwner;
end;

function TPublishedProviders.GetProviderByName(iName:string):TPublishedProvider;
var i:Integer;
begin
  result := nil;
  i := 0;
  while (i < Count) and (result = nil) do begin
    if Items[i].Name = iName then result := Items[i];
    Inc(i);
  end;    { while }
end;

procedure TPublishedProviders.UnlinkProvider(iProvider: TCustomProvider);
var i:Integer;
begin
  i := 0;
  for i := 0 to Count-1 do 
    if Items[i].Provider = iProvider then begin
      Items[i].Provider := nil;
      break;
    end;
end;

{ TPublishedProvider }

function TPublishedProvider.GetDisplayName: string;
begin
  if Name <> '' then result := Name
  else if Assigned(fProvider) then result := fProvider.Name
  else result := ClassName;
end;

procedure TPublishedProvider.SetName(const iValue: string);
begin
  fName := iValue;
end;

procedure TPublishedProvider.SetProvider(const iValue: TCustomProvider);
begin
  fProvider := iValue;
  if (fName = '') and Assigned(iValue) then fName := iValue.Name;

  if Assigned(iValue) and (Collection.Owner is TComponent) then
    iValue.FreeNotification(Collection.Owner as TComponent);
end;


end.
 