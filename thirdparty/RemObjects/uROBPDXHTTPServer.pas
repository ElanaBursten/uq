{ Summary: Contains the TROBPDXHTTPServer class }
unit uROBPDXHTTPServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Classes, uROServer, uROClientIntf, uROBPDXTCPServer,
     {$IFDEF RemObjects_USE_RODX}
     uRODXString, uRODXServerCore, uRODXSock, uRODXHTTPServerCore, uRODXHTTPHeaderTools;
     {$ELSE}
     DXString, DXServerCore, DXSock, DXHTTPServerCore, DXHTTPHeaderTools;
     {$ENDIF}

type {-------------- TROBPDXHTTPTransport --------------}

     { Summary: Implementation of the IROTransport, IROTCPTransport and IROHTTPTransport interfaces for TROBPDXHTTPServer servers
       Description:
         Implementation of the IROTransport, IROTCPTransport and IROHTTPTransport interfaces for TROBPDXHTTPServer servers.
         This class allows you to access the DXSock client thread invloved in the HTTP dispatch of the message.
         See TROServer.DispatchMessage for more information. }

     TROBPDXHTTPTransport = class(TInterfacedObject, IROTransport, IROTCPTransport, IROHTTPTransport)
     private
       fResponseHeaders : TStringList;

       fClientThread: TDXClientThread;
       fHeaderInfo: PHeaderInfo;
     protected
       {-------------- IROHTTPTransport --------------}
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;
       function GetContentType : string;
       procedure SetContentType(const aValue : string);
       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);
       function GetTargetURL : string;
       procedure SetTargetURL(const aValue : string);
       function GetPathInfo : string;
       function GetLocation : string;

       {-------------- IROTransport --------------}
       function GetTransportObject : TObject;
       function GetClientAddress : string;

     public
       constructor Create(aClientThread: TDXClientThread; aHeaderInfo: PHeaderInfo);
       destructor Destroy; override;

       { Summary: DXSock connection thread which originated the server-side call
         Description: DXSock connection thread which originated the server-side call. }
       property ClientThread: TDXClientThread read fClientThread;

       { Summary: Accessor to the HTTP header information.
         Description:
           Accessor to the HTTP header information. This is meant as read-only information. Do not
           manipulate its contents unless you perfectly understand how the values are used in the
           method TROBPDXHTTPServer.InternalHandleSession. }
       property HeaderInfo: PHeaderInfo read fHeaderInfo;
     end;

     {-------------- TROBPDXHTTPServer --------------}

     { Summary: RemObjects HTTP server based on Brain Patchwork's TDXHTTPServerCore.
       Description: RemObjects HTTP server based on Brain Patchwork's TDXHTTPServerCore. }

     TROBPDXHTTPServer = class(TROBPDXTCPServer)
     private
       function GetBPDXServer: TDXHTTPServerCore;

     protected
       function CreateBPDXServer : TDXServerCore; override;

       function BuildResponseHeader(StatusCode:Integer;var EnableKeepAlive : Boolean) : string;
       procedure CleanupNetscapeAndProxyRequests(HeaderInfo:PHeaderInfo);

       procedure InternalOnNewConnect(ClientThread:TDXClientThread);
       procedure InternalHandleSession(ClientThread: TDXClientThread; HeaderInfo: PHeaderInfo; var EnableKeepAlive: boolean);

       function GetDispatchersClass : TROMessageDispatchersClass; override;
     public

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property BPDXServer: TDXHTTPServerCore read GetBPDXServer;
     end;

implementation

uses SysUtils, uRORes, uROHTTPDispatch, uROHTTPTools, uROHelpers;

constructor TROBPDXHTTPTransport.Create(aClientThread: TDXClientThread; aHeaderInfo: PHeaderInfo);
begin
  inherited Create;
  fResponseHeaders := TStringList.Create;
  fClientThread := aClientThread;
  fHeaderInfo := aHeaderInfo;
end;

destructor TROBPDXHTTPTransport.Destroy;
begin
  fResponseHeaders.Free;
  
  inherited;
end;

procedure TROBPDXHTTPTransport.SetHeaders(const aName, aValue : string);
begin
  SetHeaderValue(fResponseHeaders, aName, aValue);
end;

function TROBPDXHTTPTransport.GetHeaders(const aName : string) : string;
begin
  result := GetHeaderValue(fResponseHeaders, aName)
end;

function TROBPDXHTTPTransport.GetContentType : string;
begin
  result := fHeaderInfo.ContentType
end;

procedure TROBPDXHTTPTransport.SetContentType(const aValue : string);
begin
  SetHeaders(id_ContentType, aValue);
end;

function TROBPDXHTTPTransport.GetUserAgent : string;
begin
  result := fHeaderInfo.UserAgent
end;

procedure TROBPDXHTTPTransport.SetUserAgent(const aValue : string);
begin
  SetHeaders(id_UserAgent, aValue);
end;

function TROBPDXHTTPTransport.GetTargetURL : string;
begin
  result := ''
end;

procedure TROBPDXHTTPTransport.SetTargetURL(const aValue : string);
begin

end;

function TROBPDXHTTPTransport.GetTransportObject : TObject;
begin
  result  :=  Self;
end;

function TROBPDXHTTPTransport.GetClientAddress : string;
begin
  result  :=  fClientThread.Socket.PeerIPAddress;
end;

function TROBPDXHTTPTransport.GetPathInfo : string;
begin
  result := StringReplace(fHeaderInfo^.URI, 'http://', '', []);
  result := Copy(result, Pos('/', result), MaxInt);
end;

function TROBPDXHTTPTransport.GetLocation : string;
begin
  result := 'http://'+fHeaderInfo^.Host
end;

{ TROBPDXHTTPServer }
function TROBPDXHTTPServer.GetBPDXServer: TDXHTTPServerCore;
begin
  result := TDXHTTPServerCore(inherited BPDXServer);
end;

function TROBPDXHTTPServer.CreateBPDXServer: TDXServerCore;
begin
  //DXSock.TDXXferTimeout := 100;
  result  :=  TDXHTTPServerCore.Create(Self);

  with TDXHTTPServerCore(result) do begin
    ServerPort  :=  8099;
    BindTo  :=  ''; // blank = ALL IP's!
    Timeout  :=  50000; // 50 seconds for initial header
    ThreadCacheSize  :=  1000;
    SocketOutputBufferSize  :=  bsfHuge;

    // Optimized settings
    {$IFNDEF LINUX}
    ListenerThreadPriority := tpIdle;
    SpawnedThreadPriority := tpIdle;
    {$ENDIF LINUX}
    SocketOutputBufferSize := bsfNormal;
    UseThreadPool := TRUE;
    SupportKeepAlive  :=  FALSE;

    OnNewConnect  :=  InternalOnNewConnect;
    OnCommandGET  :=  InternalHandleSession;
    OnCommandPOST  :=  InternalHandleSession;
    OnCommandHEAD  :=  InternalHandleSession;
  end;
end;

function TROBPDXHTTPServer.BuildResponseHeader(StatusCode:Integer;var EnableKeepAlive : Boolean) : string;
begin
   Result := 'HTTP/1.1 '+BPDXServer.HeaderText(StatusCode)+#13#10+
      'Server: RemoteObjects DXSock Web Server v1.0'+#13#10+
      'Date: '+{DXString.}DateTimeToGMTRFC822(Now)+#13#10+
      'MIME-Version: 1.0'+#13#10+
      'Public: GET,POST,HEAD,TRACE'+#13#10+
      'Accept-Ranges: none'+#13#10;
   if StatusCode<>200 then begin
      Result := Result+
         'Pragma: no-cache'+#13#10+
         'Cache-Control: no-cache'+#13#10;
      EnableKeepAlive := False;
   end;
   if EnableKeepAlive then begin
      Result := Result+'Connection: Keep-Alive'+#13#10+
         'Keep-Alive: timeout='+IntToStr((BPDXServer.Timeout div 1000))+#13#10;
   End
   else begin
      Result := Result+'Connection: close'+#13#10;
   end;
end;

Procedure TROBPDXHTTPServer.CleanupNetscapeAndProxyRequests(HeaderInfo:PHeaderInfo);
Var
   Ws:String;
begin
   if (QuickPos('://',HeaderInfo^.Raw)>0) and (QuickPos('://',HeaderInfo^.Raw)<10) then begin
      Ws := Copy(HeaderInfo^.RAW,1,CharPos(#32,HeaderInfo^.RAW));
      Delete(HeaderInfo^.RAW,1,QuickPos('://',HeaderInfo^.RAW)+2);
      if CharPos('/',HeaderInfo^.RAW)=0 then HeaderInfo^.RAW := '/ HTTP/1.1'
      else Delete(HeaderInfo^.RAW,1,CharPos('/',HeaderInfo^.RAW)-1);
      HeaderInfo^.RAW := Ws+HeaderInfo^.RAW;
   end;
end;

procedure TROBPDXHTTPServer.InternalOnNewConnect(ClientThread:TDXClientThread);
begin
//  ClientThread.Socket.SetNagle(False);
  BPDXServer.ProcessSession(ClientThread); // tell server to handle connection
end;

procedure TROBPDXHTTPServer.InternalHandleSession(ClientThread: TDXClientThread; HeaderInfo: PHeaderInfo; var EnableKeepAlive: boolean);
Var
   StatusCode:Integer; // you set this!
   req : TStringStream;
   resp:TMemoryStream; // you set this!
   transport : IROHTTPTransport;
   HeaderStr:String;
begin
  req  := NIL;
  resp := NIL;

  // if POST then collect the post data!
  if HeaderInfo^.Method='POST' then begin // collect post data!
    if HeaderInfo^.ContentLength=0 then begin // HACKER!!
      ClientThread.Socket.Writeln(BuildResponseHeader(411,EnableKeepAlive));
      Exit;
    end;

    while Length(HeaderInfo^.PostData)<HeaderInfo^.ContentLength do begin
      HeaderInfo^.PostData  :=  HeaderInfo^.PostData+
                                ClientThread.Socket.ReadStr(HeaderInfo^.ContentLength-Length(HeaderInfo^.PostData));
      if ClientThread.Socket.DroppedConnection then begin // connection aborted!
        EnableKeepAlive := False;
        Exit;
      end;
    end;
  end;

  // now we handle the request!
  CleanupNetscapeAndProxyRequests(HeaderInfo);
  if HeaderInfo^.URI='' then HeaderInfo^.URI := '/'; // fix old Netscape!

  try
    req  :=  TStringStream.Create(HeaderInfo^.PostData);
    resp := TMemoryStream.Create;
    transport  :=  TROBPDXHTTPTransport.Create(ClientThread, HeaderInfo);

    { make sure you set the:
        StatusCode to the result you want to send!
        resp - is the page body, xml, html, etc.
        MimeType - is the Content-Type: <value_only> e.g. text/html }

    if DispatchMessage(transport,req,resp)
      then StatusCode:= HTTP_OK
      else StatusCode := HTTP_FAILED;

    HeaderStr := BuildResponseHeader(StatusCode,EnableKeepAlive)+
       id_UserAgent+': '+str_ProductName+#13#10+
       id_ContentLength+': '+IntToStr(resp.Size)+#13#10+
       id_ContentType+': '+transport.Headers[id_ContentType]+#13#10;

    ClientThread.Socket.Writeln(HeaderStr);

    if (HeaderInfo^.Method<>'HEAD') and (resp.Size>0) then begin
      {$IFDEF VER100}
      ClientThread.Socket.BlockWrite(resp.Memory,resp.Size);
      {$ELSE}
      ClientThread.Socket.Write(resp.Memory,resp.Size);
      {$ENDIF}
    end;
  finally
    req.Free;
    resp.Free;
  end;
end;

function TROBPDXHTTPServer.GetDispatchersClass : TROMessageDispatchersClass; 
begin
  result := TROHTTPMessageDispatchers
end;


initialization
  RegisterServerClass(TROBPDXHTTPServer);

end.
