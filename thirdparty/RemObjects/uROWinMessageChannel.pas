{ Summary: Contains the class TROWinMessageChannel }
unit uROWinMessageChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROProxy, Forms, Messages, Windows;

type {-------------- TROWinMessageChannel --------------}

     { Summary: Channel for local, process to process communication.
       Description:
        It dispaches messages to the server by using the standard WM_COPYDATA Windows message.
        The server is specified by setting the ServerID property.
        TROWinMessageChannel internally uses a hidden window to intercept the WM_COPYDATA message sent
        back by the server. }
     TROWinMessageChannel = class(TROTransportChannel)
     private
       fWindowHandle : HWND;
       fResponseRef : TStream;
       fStartServer: boolean;
       fServerID: string;
       fFileName: string;
       fParameters: string;
       fDefaultDirectory: string;
       fDelay: word;

       procedure WndProc(var Msg: TMessage);
       procedure SetFileName(const Value: string);
       procedure SetServerID(const Value: string);

     protected
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the server using WM_COPYDATA messages. }

       procedure IntDispatch(aRequest, aResponse : TStream); override;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

     published
       { Summary: Launches the server if it was not started already
         Description: Launches the server if it was not started already. }
       property StartServer : boolean read fStartServer write fStartServer;
       { Summary: Full path of the server executable file
         Description: Full path of the server executable file. Used only if StartServer is set to TRUE. }
       property FileName : string read fFileName write SetFileName;
       { Summary: Start-up Parameters sent to the server if it needs to be started
         Description: Start-up Parameters sent to the server if it needs to be started. }
       property Parameters : string read fParameters write fParameters;
       { Summary: Default directory to point the server to if it is launched automatically
         Description: Default directory to point the server to if it is launched automatically and StartServer is set to TRUE. }
       property DefaultDirectory : string read fDefaultDirectory write fDefaultDirectory;
       { Summary: Unique server identifier
         Description: Unique server identifier. It has to match the value in TROWinMessageServer.ServerID. }
       property ServerID : string read fServerID write SetServerID;
       { Summary: Milliseconds to wait after starting a server
         Description:
           Milliseconds to wait after starting a server and before dispatching the message.
           Some servers might need some time to launch properly. This property lets you to decide how long should pass
           before the first message broadcast is executed. }
       property Delay : word read fDelay write fDelay;
     end;

implementation

uses Dialogs, SysUtils, ShellAPI, uRORes;

{ TROWinMessageChannel }

constructor TROWinMessageChannel.Create(aOwner: TComponent);
begin
  inherited;

  if not (csDesigning in ComponentState)
    then fWindowHandle := AllocateHWnd(WndProc)
    else fWindowHandle := 0;
end;

destructor TROWinMessageChannel.Destroy;
begin
  if (fWindowHandle>0)
    then DeallocateHWnd(fWindowHandle);
  inherited;
end;

procedure TROWinMessageChannel.IntDispatch(aRequest, aResponse: TStream);
var memstream : TMemoryStream;
    CDS : TCopyDataStruct;
    freememstream : boolean;
    handle : cardinal;
begin
  freememstream := not (aRequest is TMemoryStream);
  fResponseRef := aResponse;

  // Locates the server window
  handle := FindWindow(NIL, PChar(ServerID));
  if (handle=0) then begin
    // Starts it
    if StartServer then begin
      ShellExecute(0, 'open', PChar(FileName), PChar(Parameters), PChar(DefaultDirectory), SW_NORMAL);
      Sleep(Delay);

      handle := FindWindow(NIL, PChar(ServerID));
    end;
  end;

  if (handle=0) then RaiseError(err_CannotFindServer, [ServerID]);

  if not freememstream then memstream := TMemoryStream(aRequest)
  else begin
    memstream := TMemoryStream.Create;
    memstream.LoadFromStream(aRequest);
  end;

  memstream.Position := 0;
  try
    CDS.dwData := 0;
    CDS.cbData := memstream.Size;
    CDS.lpData := memstream.Memory;

    SendMessage(handle, WM_COPYDATA, fWindowHandle, Integer(@CDS));
  finally
    if freememstream then memstream.Free;
  end;
end;

procedure TROWinMessageChannel.SetFileName(const Value: string);
begin
  fFileName := Value;
end;

procedure TROWinMessageChannel.SetServerID(const Value: string);
begin
  fServerID := Value;
end;

procedure TROWinMessageChannel.WndProc(var Msg: TMessage);
var WMMsg : TWMCopyData absolute Msg;
begin
  with Msg do
    case Msg of
      WM_COPYDATA  : begin
        fResponseRef.Write(WMMsg.CopyDataStruct.lpData^, WMMsg.CopyDataStruct.cbData);
      end;
      else result := DefWindowProc(FWindowHandle, Msg, wParam, lParam);
    end;
end;

end.
