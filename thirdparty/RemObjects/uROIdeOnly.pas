unit uROIdeOnly;

interface

implementation

{$IFDEF WINDOWS}
uses Forms, Dialogs;

initialization
  if (not Assigned(Application)) or
     (not Assigned(Application.MainForm)) or
     (Application.MainForm.ClassName <> 'TAppBuilder') then begin
    ShowMessage('The RemObjects SDK Packages may only be used at designtime.');
    Application.Terminate;
  end;
{$ENDIF WINDOWS}
end.
