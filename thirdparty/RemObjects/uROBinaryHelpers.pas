unit uROBinaryHelpers;

interface

uses Classes, Variants,
     uROClientIntf;

function BinaryFromVariant(const iVariant:OleVariant):Binary;
function VariantFromBinary(const iBinary:Binary):OleVariant;

function BinaryFromVariantBinary(const iVariant:OleVariant):Binary;

implementation

uses {$IFDEF DEBUG_REMOBJECTS_DATASNAP}eDebugServer,{$ENDIF}
     SysUtils;

function VariantBinaryFromBinary(const iBinary:Binary):OleVariant; forward;

function BinaryFromVariant(const iVariant:OleVariant):Binary;
var lType:integer;
    lIntegerValue:integer;
begin
  lType := VarType(iVariant);

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.Write('BinaryFromVariant type=%d',[lType]);
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  case lType of

    varEmpty,varNull:begin
        result := Binary.Create();
        result.Write(lType,sizeof(integer));
      end;

    varInteger:begin
        result := Binary.Create();
        result.Write(lType,sizeof(integer));
        lIntegerValue := iVariant;
        result.Write(lIntegerValue,sizeof(integer))
      end;

    varBoolean:begin
        result := Binary.Create();
        result.Write(lType,sizeof(integer));
        lIntegerValue := iVariant;
        result.Write(lIntegerValue,sizeof(integer))
      end;

    //varArray,
    8209:result := BinaryFromVariantBinary(iVariant);

    else raise Exception.CreateFmt('Unsupported variant type "%d"',[VarType(iVariant)]);
  end;    { case }

  if Assigned(result) then
    result.Seek(0,soBeginning)
end;

function VariantFromBinary(const iBinary:Binary):OleVariant;
var lType:integer;
begin
  if iBinary.Size < 4 then raise Exception.Create('Invalid binary format for a variant.');
  iBinary.Read(lType,sizeof(integer));

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.Write('VariantFromBinary type=%d',[lType]);
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  case lType of
    varEmpty:result := EmptyParam;
    varNull:result := Null;
    8209:result := VariantBinaryFromBinary(iBinary);
    else raise Exception.CreateFmt('Unsupported variant type in binary "%d"',[lType]);
  end;    { case }

end;


function BinaryFromVariantBinary(const iVariant:OleVariant):Binary;
var l:longword;
    lType:integer;
    p:pointer;
begin
  if not VarIsArray(iVariant) then raise Exception.CreateFmt('Variant must be Array, but is %d',[VarType(iVariant)]);
  if VarArrayDimCount(iVariant) <> 1 then raise Exception.Create('Variant Array DimCount must be 1');

  l := VarArrayHighBound(iVariant,1)-VarArrayLowBound(iVariant,1)+1;

  p := VarArrayLock(iVariant);
  try
    result := Binary.Create();
    lType := VarType(iVariant);
    {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
    DebugServer.Write('BinaryFromVariantBinary type=%d',[lType]);
    {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
    result.Write(lType,sizeof(integer));
    result.Write(p^,l);
    result.Seek(0,soBeginning)
  finally
    VarArrayUnlock(iVariant);
  end;                                        

end;

function VariantBinaryFromBinary(const iBinary:Binary):OleVariant;
var p:pointer;
begin
  result := VarArrayCreate([0,iBinary.Size-1],varByte);
  p := VarArrayLock(result);
  try
    iBinary.Read(p^,iBinary.Size);
  finally
    VarArrayUnlock(result);
  end;
end;

end.
