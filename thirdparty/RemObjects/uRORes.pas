{ Summary: }
unit uRORes;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.SysUtils;
  {$ELSE}
  SysUtils;
  {$ENDIF DOTNET}

{$IFDEF DOTNET}
type TSessionID = string;
{$ELSE}
type TSessionID = TGUID;
{$ENDIF DOTNET}

const
  str_Author      = 'RemObjects Software, Inc.';
  str_ProductName = 'RemObjects SDK';

  res_RODLFile = 'RODLFILE';

  CRLF = #13#10;

  EMPTY_GUID : TSessionID = '{00000000-0000-0000-0000-000000000000}';
  EMPTY_SESSION_ID : TSessionID = '{00000000-0000-0000-0000-000000000000}';

  HTTPModuleInfoCommand = '/Info';

resourcestring
  // RODL
  err_InvalidIndex        = 'Invalid index %d'; // Raised by the RODL classes to indicate an invalid entity index
  err_InvalidType         = 'Invalid type "%s. Expected "%s""'; // Raised by TRODLComplexEntity.Add when trying to add an invalid class to the internal list
  err_InvalidLibrary      = 'Invalid library'; // Raised by TRODLConverter.Convert when the library is NIL
  err_InvalidStream       = 'Invalid stream'; // Raised by TRODLReader.Read when a NIL stream is passed
  err_InvalidTargetEntity = 'Invalid TargetEntity "%s"'; // Raised by TRODLConverter.Convert when a call to ValidateTargetEntity fails
  err_InvalidParamFlag    = 'Invalid Parameter Flag "%s"'; // Raised by XMLFlagNameToFlag (unit uRODLToXML) when the specified string is not in XMLFlagNames

  err_InvalidStringLength = 'Stream read error: Invalid string length "%d"'; //
  str_InvalidClassTypeInStream = 'Stream read error: Invalid class type encountered: "%d"';
  err_ObjectExpectedInStream = 'Stream read error: Object expected, but nil found.';

  err_MessageClassMustImplementAssign = 'Please implement Assign() for your custom message class.';

  err_InvalidInfo   = 'Invalid or incomplete info'; // Raised by TRODLEntity.Validate when the internal Info property is not valid
  err_DuplicateName = 'Duplicate name "%s"'; // Raised by TRODLComplexEntity.Validate when two items have the same name

  err_UnspecifiedRODLLocation = 'Unspecified RODL location';

  // Message handling
  err_InvalidRequestStream             = 'Invalid request stream (%d bytes)';
  err_NILMessage                       = 'Message is NIL'; // Raised server side when a message passed to ProcessMessage is NIL
  err_UnspecifiedInterface             = 'The message does not have an interface name'; // Raised server side when a message passed to ProcessMessage does not have a valid InterfaceName
  err_UnspecifiedMessage               = 'The message does not have a name'; // Raised server side when a message passed to ProcessMessage does not have a valid MessageName
  err_UnknownMethod                    = 'Unknown method %s for interface %s'; // Raised server side when a server interface does not have a method matching the message name   
  err_ClassFactoryDidNotReturnInstance = 'Class factory did not return an instance of "%s"'; // Raised server side when a class factory returns a NIL reference. It should never happen
  err_ParameterNotFound                = 'Parameter "%s" was not found';

  // Misc
  err_NotImplemented                 = 'Not implemented'; // Not implemented error. Should never be happen
  err_TypeNotSupported               = 'Type "%s" not supported'; // Raised by message classes when the typeinfo of the parameter being read/written is not supported
  err_ClassFactoryNotFound           = 'Class factory for interface %s not found';
  err_UnspecifiedTargetURL           = 'Unspecified TargetURL';
  err_IROMessageNotSupported         = 'Class "%s" does not support IROMessage';
  err_ClassAlreadyRegistered         = 'Class "%s" is already registered';
  err_UnknownClass                   = 'Unknown class "%s"';

  err_DispatcherAlreadyAssigned       = 'Dispatcher for %s already assigned';
  err_CannotFindMessageDispatcher     = 'Cannot find message dispatcher. Maybe there is no message component configured for  for the requested path?';
  err_ServerOnlySupportsOneDispatcher = '%s servers only support one dispatcher';
  err_UnhandledException              = 'Unhandled exception';

  err_ChannelBusy                     = 'Channel is busy. Try again later.';

  // SOAP
  err_ExpectedParameterNotFound = 'Expected parameter "%s" was not found'; // Raised by TROSOAPMessage when an expected parameter was not found
  err_AmbigousResponse          = 'Ambigous response. Expected one "%s" but %d were received'; // Raised by TROSOAPMessage when two parameters in a a SOAP Envelope have the same name
  err_InvalidEnvelope           = 'Invalid SOAP Envelope. %s'; // Invalid SOAP envelope. The message is completed with additional information
  err_InvalidData               = 'Cannot convert data for parameter "%s"'#13'Exception was "%s"'#13'Data was "%s"'; // Error converting the text of the parameter to a Delphi type

  inf_InvalidEnvelopeNode   = 'SOAP Envelope node missing or not root'; // The SOAP envelope does not match the SOAP specs by not providing an Envelope node
  inf_AbsentBody            = 'SOAP envelope does not contain a Body'; // The SOAP envelope does not contain a body
  inf_AbsentMessage         = 'SOAP Body does not contain a message nor a fault'; // The body does not contain any sub node

  // BIN
  err_InvalidHeader = 'Invalid binary header. Either incompatible or not a binary message';

  // WinMessageChannel
  err_CannotFindServer = 'Cannot find server "%s"';
  err_MessagePropertyMustBeSet = 'Message property must be assigned for the server to be active';

  // WInINet channel
  err_UnexpectedWinINetProblem = 'Unexpected error in WinInet HTTP Channel (%d)';

  // BPDX Channels
  err_CannotConnectToServer = 'Cannot connect to server "%s"';
  err_ConnectionError = 'Connection error';

  // Misc
  msg_NoMultipleDispatchers = 'Multiple message dispatchers not supported';
  err_InvalidStorage = 'Invalid storage';
  err_ErrorConvertingFloat = 'Error converting float "%s" at byte %d';  

  {$IFDEF DOTNET}
  err_NotImplementedForDotNetYet = 'This function is not yet implemented for .NET yet.';
  {$ENDIF DOTNET}

{ Global function used to raise exception in the RemObjects SDK }
procedure RaiseError(const anErrorMessage : string; const someParams : array of const);

implementation

procedure RaiseError(const anErrorMessage : string; const someParams : array of const);
begin
  raise Exception.CreateFmt(anErrorMessage, someParams);
end;

end.                                 
