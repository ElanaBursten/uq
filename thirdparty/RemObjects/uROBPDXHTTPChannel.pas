{ Summary: Contains the class TROBPDXHTTPChannel }
unit uROBPDXHTTPChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Classes, uROClient, uROProxy, uROClientIntf, uROBPDXTCPChannel,
     {$IFDEF RemObjects_USE_RODX}
     uRODXSocket,
     uRODXSockClient;
     {$ELSE}
     DXSocket,
     DXSockClient;
     {$ENDIF}

type {------------- TDXSockHTTPClient -------------}

     TDXSockHTTPClient = class(TDXSockClient)
     private
       fHeaders : TStringList;
       NewConnect:PNewConnect;

       function GetHeaders(const aName: string): string;
       procedure SetHeaders(const aName, Value: string);
       function GetContentType: string;
       function GetUserAgent: string;
       procedure SetContentType(const Value: string);
       procedure SetUserAgent(const Value: string);
       function GetUnparsedHeaders: TStrings;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

       procedure Post(const aTargetURL : string; aRequest, aResponse : TStream);

       property Headers[const aName : string] : string read GetHeaders write SetHeaders;
       property UnparsedHeaders : TStrings read GetUnparsedHeaders;

     published
       property ContentType : string read GetContentType write SetContentType;
       property UserAgent : string read GetUserAgent write SetUserAgent;
     end;

     {------------- TROBPDXHTTPChannel -------------}

     { Summary: Channel for remote HTTP communication based on the TDXSockHTTPClient component
       Description:
        It dispaches messages to the server by using the TDXSockHTTPClient component.
        The internal DxSock client is accessible using the BPDXClient client property which is also visible in
        the Object inspector in Delphi 6 and up. }

     TROBPDXHTTPChannel = class(TROBPDXTCPChannel, IROTransport, IROTCPTransport, IROHTTPTransport)
     private
       fTargetURL : string;
       function GetBPDXClient: TDXSockHTTPClient;

     public
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the HTTP server using the internal HTTP client's Post method.
           See TDXSockHTTPClient.Post. }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       { Summary: Creates the internal TDXSockClient client
         Description:
           Creates the internal TDXSockClient client. It returns a TDXSockHTTPClient client. }
       function CreateBPDXClient : TDXSockClient; override;

       {------------- IROHTTPTransport -------------}
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;
       function GetContentType : string;
       procedure SetContentType(const aValue : string);
       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);

       procedure SetTargetURL(const Value: string);
       function GetTargetURL: string;

       function GetPathInfo : string;
       function GetLocation : string;

     public

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property BPDXClient : TDXSockHTTPClient read GetBPDXClient;

       { Summary: Specifies the target URI for the message dispatch
         Description:
           Specifies the target URI for the message dispatch.

           If you importeed a SOAP web service using the Service Builder, this property will be automatically
           assigned a value for you using the URI specified in the WSDL document.

           Refer to the property TROServer.Dispatchers for more info about setting your services URIs. }
       property TargetURL : string read GetTargetURL write SetTargetURL;

     end;

implementation

uses SysUtils, uROHTTPTools, uRORes,
     {$IFDEF RemObjects_USE_RODX}
     uRODXSock,
     uRODXString;
     {$ELSE}
     DXSock,
     DXString;
     {$ENDIF}

{ TDXSockHTTPClient }

constructor TDXSockHTTPClient.Create(aOwner: TComponent);
begin
  inherited;

  fHeaders := TStringList.Create;
  New(NewConnect);
end;

destructor TDXSockHTTPClient.Destroy;
begin
  fHeaders.Free;
  Dispose(NewConnect);

  inherited;
end;

function TDXSockHTTPClient.GetContentType: string;
begin
  result := Headers[id_ContentType]
end;

function TDXSockHTTPClient.GetHeaders(const aName: string): string;
begin
  result := GetHeaderValue(fHeaders, aName);
end;

function TDXSockHTTPClient.GetUnparsedHeaders: TStrings;
begin
  result := fHeaders;
end;

function TDXSockHTTPClient.GetUserAgent: string;
begin
  result := Headers[id_UserAgent];
end;

procedure TDXSockHTTPClient.Post(const aTargetURL: string; aRequest, aResponse: TStream);
var lReceivedHeader : string;
    s:string;
    len : integer;
    tOut:Integer;
    S2:String;

begin
  S:=aTargetURL;
  len:=QuickPos('HTTP://',UpperCase(S));
  If Len>0 then Delete(S,1,Len+6);
  Len:=CharPos('/',S);
  If Len>0 then Delete(S,Len,MaxInt);
  Len:=CharPos(':',S);
  With NewConnect^ do Begin
     If Len>0 then Begin
        Port:=StrToInt(Copy(S,Len+1,MaxInt));
        Delete(S,Len,MaxInt);
     end
     else Port:=80;
     ipAddress:=S;
     UseNAGLE:=EnabledNagle;
     UseUDP:=EnabledUDP;
     UseBlocking:=EnabledBlocking;
  End;
  If Not Connected then Connect(NewConnect);

  { if we have a session ID, then pass it to the server }
  {if ioSessionID <> '' then begin
    UnparsedHeaders.Add(HTTP_SESSION_ID_HEADER+': '+ioSessionID);
  end;}

  If Connected then Begin
    try
      aRequest.Position := 0;
      Headers[id_ContentLength] := IntToStr(aRequest.Size); // It is mandatory for a client to send Content-Length!
      With Socket do Begin
         Writeln('POST '+aTargetURL+' HTTP/1.0'+CRLF+
            'Host: '+NewConnect^.ipAddress+CRLF+
            'Accept: */*'+CRLF+UnparsedHeaders.Text);
         SendFrom(aRequest);
         lReceivedHeader:='';
         While (QuickPos(#13#10#13#10,lReceivedHeader)=0) and (Length(lReceivedHeader)<8192) do Begin
            lReceivedHeader:=lReceivedHeader+ReadStr(-1);
            if ((LastCommandStatus<>0) and (LastCommandStatus<>10060)) then Begin
              RaiseError(err_ConnectionError, []);
              //Exit; // raise exception then exit
            End;
         End;
      End;

      (*S2:={$IFDEF RemObjects_USE_RODX}uRODXString.{$ELSE}DXString.{$ENDIF}Uppercase(lReceivedHeader);
      Len:=QuickPos('CONTENT-L',S2);
      if Len>0 then Begin
         Delete(lReceivedHeader,1,Len+Length(id_ContentLength)+1);
         Len:=StrToInt(Copy(lReceivedHeader,1,CharPos(#13,lReceivedHeader)-1));
      End;
      S2:=lReceivedHeader;
      p :=QuickPos(HTTP_SESSION_ID_HEADER+':',S2);
      if p > 0 then Begin
        Delete(lReceivedHeader,1,p+Length(HTTP_SESSION_ID_HEADER)+1);
        ioSessionID := Copy(lReceivedHeader,1,CharPos(#13,lReceivedHeader)-1);
      End;*)

      Len := StrToInt(GetHeaderValue(lReceivedHeader, id_ContentLength));
      //ioSessionID := GetHeaderValue(lReceivedHeader, HTTP_SESSION_ID_HEADER);

      If Len<1 then Exit; // ToDo: raise exception??
      Delete(lReceivedHeader,1,QuickPos(#13#10#13#10,lReceivedHeader)+3);
      If Length(lReceivedHeader)>0 then aResponse.Write(lReceivedHeader[1],Length(lReceivedHeader));
      If aResponse.Size<len then Begin
        SetLength(S2,Len);
        while (aResponse.Size<len) do Begin
           tOut:=Socket.Read(@S2[1],Len-aResponse.Size);
           If tOut>0 then Begin
              aResponse.Write(S2[1],tOut);
           End
           Else begin
              If Socket.Readable then
                 if Socket.CharactersToRead=0 then Begin
                   RaiseError(err_ConnectionError, []);
                   //Exit; // raise exception - we are disconnected!
                 End;
           End;
        End;
      End;
    finally
     Socket.CloseNow;
    end;
  end
  else RaiseError(err_CannotConnectToServer, [aTargetURL]);

  aResponse.Seek(0,0);
end;

procedure TDXSockHTTPClient.SetContentType(const Value: string);
begin
  Headers[id_ContentType] := Value;
end;

procedure TDXSockHTTPClient.SetHeaders(const aName, Value: string);
begin
  SetHeaderValue(fHeaders, aName, Value);
end;

procedure TDXSockHTTPClient.SetUserAgent(const Value: string);
begin
  Headers[id_UserAgent] := Value;
end;

{ TROBPDXHTTPChannel }
function TROBPDXHTTPChannel.CreateBPDXClient: TDXSockClient;
begin
  result := TDXSockHTTPClient.Create(Self);
  result.Port := 8099;
  result.Host := '127.0.0.1';
  TargetURL := '';
end;

function TROBPDXHTTPChannel.GetBPDXClient: TDXSockHTTPClient;
begin
  result := TDXSockHTTPClient(inherited BPDXClient)
end;

function TROBPDXHTTPChannel.GetContentType: string;
begin
  result := BPDXClient.ContentType
end;

function TROBPDXHTTPChannel.GetHeaders(const aName: string): string;
begin
  result := BPDXClient.Headers[aName]
end;

function TROBPDXHTTPChannel.GetLocation: string;
begin
  result := ''
end;

function TROBPDXHTTPChannel.GetPathInfo: string;
begin
  result := '' // TODO: FIX THIS!!!
end;

function TROBPDXHTTPChannel.GetTargetURL: string;
begin
  result := fTargetURL
end;

function TROBPDXHTTPChannel.GetUserAgent: string;
begin
  result := BPDXClient.UserAgent
end;

procedure TROBPDXHTTPChannel.IntDispatch(aRequest, aResponse: TStream);
begin
  if (fTargetURL='')
    then RaiseError(err_UnspecifiedTargetURL, []);

  BPDXClient.Post(fTargetURL, aRequest, aResponse);
end;

procedure TROBPDXHTTPChannel.SetContentType(const aValue: string);
begin
  BPDXClient.ContentType := aValue
end;

procedure TROBPDXHTTPChannel.SetHeaders(const aName, aValue: string);
begin
  BPDXClient.Headers[aName] := aValue
end;

procedure TROBPDXHTTPChannel.SetTargetURL(const Value: string);
begin
  fTargetURL := Value
end;

procedure TROBPDXHTTPChannel.SetUserAgent(const aValue: string);
begin
  BPDXClient.UserAgent := aValue;
end;

end.
