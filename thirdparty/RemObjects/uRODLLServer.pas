unit uRODLLServer;

interface

uses Classes, uROClientIntf, uROClient, uROServerIntf, uROServer;

type { TRODLLServer }
     TRODLLServer = class(TROServer, IROTransport)
     private
     protected
       { IROTransport }
       function GetTransportObject : TObject;

       procedure IntSetActive(const Value: boolean); override;
       function IntGetActive : boolean; override;

     public
     end;

{ Call this to add a dispatcher to the DLLServer }
procedure RegisterMessage(aMessage : TROMessage);

{ Use this singleton if you need to hook to the server instance }
function DLLServer : TRODLLServer;

implementation

uses uRORes, uRODLLChannel;

var fDLLServer: TRODLLServer;

procedure RegisterMessage(aMessage : TROMessage);
begin
  with TROMessageDispatcher(fDLLServer.Dispatchers.Add) do begin
    Message := aMessage;
  end;
end;

function DLLServer : TRODLLServer;
begin
  result := fDLLServer;
end;

{ EXPORTED (for external user only): Main DLL entry point for message processing }
procedure DLLProcessMessage(aRequestStream,
                            aResponseStream : TStream);
var transport : IROTransport;
begin
  fDLLServer.QueryInterface(IROTransport, transport);
  fDLLServer.DispatchMessage(transport, aRequestStream, aResponseStream);
  //fDLLServer.DispatchMessage((fDLLServer as IROTransport), aRequestStream, aResponseStream);
end;

exports
  DLLProcessMessage name DLLProcessMessageName;

{ TRODLLServer }

function TRODLLServer.GetTransportObject: TObject;
begin
  result := Self;
end;

function TRODLLServer.IntGetActive: boolean;
begin
  result := TRUE;
end;

procedure TRODLLServer.IntSetActive(const Value: boolean);
begin
  // not required in this case. Always active
end;

initialization
  fDLLServer := TRODLLServer.Create(NIL);

finalization
  fDLLServer.Free;

end.
