{ Summary: Contains the class TROBPDXTCPChannel }
unit uROBPDXTCPChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Classes, uROClient, uROProxy, uROClientIntf,
     {$IFDEF RemObjects_USE_RODX}
     uRODXSockClient;
     {$ELSE}
     DXSockClient;
     {$ENDIF}

type {------------- TROBPDXTCPChannel -------------}

     { Summary: Channel for remote TCP/IP communication based on DxSock's TDXSockClient component
       Description:
        It dispaches messages to the server by using the DxSock's TDXSockClient component.
        The internal DxSock client is accessible using the BPDXClient client property which is also visible in
        the Object inspector in Delphi 6 and up. }

     TROBPDXTCPChannel = class(TROTransportChannel, IROTransport, IROTCPTransport)
     private
       fBPDXClient : TDXSockClient;

     public
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the server using the internal TCP client's SendFromStreamWithSize
           and SaveToStreamWithSize methods. }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       { Summary: Creates the internal TDXSockClient client
         Description:
           Creates the internal TDXSockClient client. It returns a TDXSockClient client. }
       function CreateBPDXClient : TDXSockClient; virtual;

       {------------- IROTransport -------------}
       function GetTransportObject : TObject; override;

       {------------- IROTCPTransport -------------}
       function GetClientAddress : string;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property BPDXClient : TDXSockClient read fBPDXClient;

     end;

implementation

uses uRORes, SysUtils;

{ TROBPDXTCPChannel }

constructor TROBPDXTCPChannel.Create(aOwner: TComponent);
begin
  inherited;
  //fKeepConnected := FALSE;
  fBPDXClient := CreateBPDXClient;
  fBPDXClient.Name := 'InternalBPDXClient';
  {$IFDEF DELPHI6UP}
  fBPDXClient.SetSubComponent(TRUE);
  {$ENDIF}
end;

function TROBPDXTCPChannel.CreateBPDXClient: TDXSockClient;
begin
  result := TDXSockClient.Create(Self);
  result.Port := 8090;
  result.Host := '127.0.0.1';
end;

destructor TROBPDXTCPChannel.Destroy;
begin
  if fBPDXClient.Connected
    then fBPDXClient.Disconnect;
    
  inherited;
end;

function TROBPDXTCPChannel.GetClientAddress: string;
begin
  result := '';
end;

function TROBPDXTCPChannel.GetTransportObject: TObject;
begin
  result := Self;
end;

procedure TROBPDXTCPChannel.IntDispatch(aRequest, aResponse: TStream);
begin
  {if not fBPDXClient.Connected
    then }

  if fBPDXClient.DoConnect then try
    aRequest.Position := 0;
    fBPDXClient.Socket.SendFromStreamWithSize(aRequest);
    fBPDXClient.Socket.SaveToStreamWithSize(aResponse,12000);
  finally
    {if not fKeepConnected
      then }fBPDXClient.Disconnect;
  end

  else RaiseError(err_CannotConnectToServer, [fBPDXClient.Host+':'+IntToStr(fBPDXClient.Port)]);
end;

end.
