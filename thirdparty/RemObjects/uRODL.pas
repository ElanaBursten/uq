{ Summary:
   Contains the classes required to create, read, manage and convert RODL files.
   This unit introduces several classes to work with RODL files. Each item in a RODL file is considered
   a TRODLEntity. Each TRODLEntity exposes an Info property of type TRODLNameInfo or one of its descendants.
   This unit also introduces the two base classes TRODLConverter and TRODLReader used to convert a RODL
   file into/from another type of format.

   More information about these classes is available upon request. The demo under Sampes\RODL should help you
   understanding how the RODL classes work. }
   
unit uRODL;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.Classes, RemObjects.SDK.VclReplacements;
  {$ELSE}
  Classes, Contnrs;
  {$ENDIF DOTNET}

const
  EmptyGUID: TGUID  = (D1:0; D2:0; D3:0; D4:(0, 0, 0, 0, 0, 0, 0, 0));

  DefaultIntfName = 'Default';

type {--------------  Misc types -------------- }
     TRODLParamFlag = (fIn, fOut, fInOut, fResult);

     {-------------- TRODLNameInfo -------------- }

     { Summary:
       Description: }

     {:Base anchestor for <See Class="TRODLEntity"> information. }
     TRODLNameInfo = class
     private
       fName: string;
       fAttributes : TStringList;

       procedure SetName(const Value: string);
       function GetAttributes: TStrings;

     protected
       { Summary: Validation method
       Description: Validation method. Override this method to perform additional validation on properties you introduce.}
       function GetIsValid: boolean; virtual;

     public
       constructor Create(const aName :string);
       destructor Destroy; override;

       { Summary: Name of the entity.
         Description: Name of the entity. Cannot be empty}
       property Name : string read fName write SetName;

       { Summary: Validates the information.
         Description: Validates the information. Decendants override this method to perform additional checks. }
       property IsValid : boolean read GetIsValid;

       { Summary:
         Description: }
       property Attributes : TStrings read GetAttributes;
     end;

     TRODLNameInfoClass = class of TRODLNameInfo;

     {--------------  TRODLEntityInfo -------------- }
     TRODLEntityInfo = class(TRODLNameInfo)
     private
       fDocumentation: string;
       fUID: TGUID;

       procedure SetDocumentation(const Value: string);

     protected
       function GetIsValid: boolean; override;

     public
       constructor Create(const aName :string;
                          const aUID: TGUID;
                          const someDocumentation : string); reintroduce; virtual;

       property UID : TGUID read fUID write fUID;
       property Documentation : string read fDocumentation write SetDocumentation;
     end;

     {--------------  TRODLTypedElementInfo -------------- }
     TRODLTypedElementInfo = class(TRODLNameInfo)
     private
       fDataType: string;

       procedure SetDataType(const Value: string);

     protected
       function GetIsValid: boolean; override;

     public
       constructor Create(const aName, aTypeName : string); reintroduce;

       property DataType : string read fDataType write SetDataType;
     end;

     {--------------  TRODLOperationParamInfo -------------- }
     TRODLOperationParamInfo = class(TRODLTypedElementInfo)
     private
       fFlag: TRODLParamFlag;

     public
       constructor Create(const aName, aTypeName : string; aFlag : TRODLParamFlag);

       property Flag : TRODLParamFlag read fFlag write fFlag;
     end;

     {--------------  TRODLEntity -------------- }
     TRODLEntity = class
     private
       fInfo: TRODLNameInfo;

       function GetInfo: TRODLEntityInfo;

     protected
       function RODLTypeInfoClass : TRODLNameInfoClass; virtual;

     public
       constructor Create; virtual;

       function Validate(out FaultyEntity : TRODLEntity; out ErrorMessage : string): boolean; virtual;

       property Info : TRODLEntityInfo read GetInfo;
     end;

     TRODLEntityClass = class of TRODLEntity;

     {--------------  TRODLComplexEntity -------------- }
     TRODLComplexEntity = class(TRODLEntity)
     private
       fItems : TObjectList;

       function GetCount: integer;
       function GetItems(Index: integer): TRODLEntity;

     protected
       function RODLTypeInfoClass : TRODLNameInfoClass; override;
       function GetItemClass : TRODLEntityClass; virtual; abstract;

     public
       constructor Create; override;
       destructor Destroy; override;

       function Validate(out FaultyEntity : TRODLEntity; out ErrorMessage : string): boolean; override;

       function ItemByName(const aName : string) : TRODLEntity;
       function ItemByUID(const aUID : TGUID) : TRODLEntity;

       function Add(anEntity : TRODLEntity) : integer; overload; virtual;
       function Add : TRODLEntity; overload; virtual;

       procedure Exchange(Index1, Index2 : integer); 

       procedure Remove(anEntity : TRODLEntity); virtual;
       procedure Delete(Index : integer); virtual;
       procedure Clear;

       property ItemClass : TRODLEntityClass read GetItemClass;
       property Items[Index : integer] : TRODLEntity read GetItems;
       property Count : integer read GetCount;
     end;

     {--------------  TRODLTypedEntity -------------- }
     TRODLTypedEntity = class(TRODLEntity)
     private

       function GetInfo : TRODLTypedElementInfo;

     protected
       function RODLTypeInfoClass : TRODLNameInfoClass; override;

     public
       property Info : TRODLTypedElementInfo read GetInfo;
     end;

     {--------------  TRODLStruct -------------- }
     TRODLStruct = class(TRODLComplexEntity)
     private
       function GetItems(Index : integer) : TRODLTypedEntity;

     protected
       function GetItemClass : TRODLEntityClass; override;

     public
       function Add(aStructElement : TRODLTypedEntity) : integer; reintroduce; overload;
       function Add : TRODLTypedEntity; reintroduce; overload;

       property Items[Index : integer] : TRODLTypedEntity read GetItems;
     end;

     {--------------  TRODLArray -------------- }
     TRODLArray = class(TRODLEntity)
     private
       fElementType: string;

       procedure SetElementType(const Value: string);

     protected
       function RODLTypeInfoClass: TRODLNameInfoClass; override;

     public
       function Validate(out FaultyEntity : TRODLEntity; out ErrorMessage : string): boolean; override;

       property ElementType : string read fElementType write SetElementType;
     end;

     {--------------  TRODLEnumValue -------------- }
     TRODLEnumValue = class(TRODLEntity)
     private
       function GetInfo : TRODLNameInfo;

     protected
       function RODLTypeInfoClass : TRODLNameInfoClass; override;

     public
       property Info : TRODLNameInfo read GetInfo;
     end;

     {--------------  TRODLEnum -------------- }
     TRODLEnum = class(TRODLComplexEntity)
     private
       function GetItems(Index : integer) : TRODLEnumValue;

     protected
       function GetItemClass : TRODLEntityClass; override;

     public
       function Add(aStructElement : TRODLEnumValue) : integer; reintroduce; overload;
       function Add: TRODLEnumValue; reintroduce; overload;

       property Items[Index : integer] : TRODLEnumValue read GetItems;
     end;

     {--------------  TRODLOperationParam -------------- }
     TRODLOperationParam = class(TRODLEntity)
     private
       function GetInfo : TRODLOperationParamInfo;

     protected
       function RODLTypeInfoClass : TRODLNameInfoClass; override;

     public
       property Info : TRODLOperationParamInfo read GetInfo;
     end;

     {--------------  TRODLOperation -------------- }
     TRODLOperation = class(TRODLComplexEntity)
     private
       function GetItems(Index : integer) : TRODLOperationParam;
       function GetResult: TRODLOperationParam;

     protected
       function GetItemClass : TRODLEntityClass; override;

     public
       function Add(aParam : TRODLOperationParam) : integer; reintroduce; overload;
       function Add : TRODLOperationParam; reintroduce; overload;

       property Items[Index : integer] : TRODLOperationParam read GetItems;
       property Result : TRODLOperationParam read GetResult;
     end;

     {--------------  TRODLServiceInterface -------------- }
     TRODLServiceInterface = class(TRODLComplexEntity)
     private
       function GetItems(Index : integer) : TRODLOperation;

     protected
       function GetItemClass : TRODLEntityClass; override;

     public

       function Add(aParam : TRODLOperation) : integer; reintroduce; overload;
       function Add : TRODLOperation; reintroduce; overload;

       property Items[Index : integer] : TRODLOperation read GetItems;
     end;

     {--------------  TRODLService -------------- }
     TRODLService = class(TRODLComplexEntity)
     private
       function GetItems(Index : integer) : TRODLServiceInterface;
       function GetDefault: TRODLServiceInterface;
       function GetInfo : TRODLNameInfo;

     protected
       function GetItemClass : TRODLEntityClass; override;
       function RODLTypeInfoClass : TRODLNameInfoClass; override;
     public
       constructor Create; override;

       function Add(aServiceInterface : TRODLServiceInterface) : integer; reintroduce; overload;
       function Add : TRODLServiceInterface; reintroduce; overload;

       property Items[Index : integer] : TRODLServiceInterface read GetItems;
       property Default : TRODLServiceInterface read GetDefault;
       property Info : TRODLNameInfo read GetInfo;
     end;

     {--------------  TRODLLibrary --------------}
     TRODLLibrary = class(TRODLComplexEntity)
     private
       fArrayCount: integer;
       fServiceCount: integer;
       fStructCount: integer;
       fEnumCount: integer;

       function GetArray(Index: integer): TRODLArray;
       function GetEnums(Index: integer): TRODLEnum;
       function GetServices(Index: integer): TRODLService;
       function GetStructs(Index: integer): TRODLStruct;

     protected
       function GetItemClass : TRODLEntityClass; override;

     public
       constructor Create; override;

       function Add(anEntity : TRODLEntity) : integer; override;
       
       procedure Delete(Index : integer); override;
       procedure Remove(anEntity : TRODLEntity); override;

       property Structs[Index : integer] : TRODLStruct read GetStructs;
       property StructCount : integer read fStructCount;

       property Arrays[Index : integer] : TRODLArray read GetArray;
       property ArrayCount : integer read fArrayCount;

       property Enums[Index : integer] : TRODLEnum read GetEnums;
       property EnumCount : integer read fEnumCount;

       property Services[Index : integer] : TRODLService read GetServices;
       property ServiceCount : integer read fServiceCount;
     end;

     {--------------  TRODLConverter -------------- }
     TRODLConverter = class
     private
       fBuffer : TStringList;
       fTargetEntity : string;

       function GetBuffer: TStrings;

     protected
       procedure IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); virtual; abstract;
       function ValidateTargetEntity(const aLibrary : TRODLLibrary; const aTargetEntity : string) : boolean; virtual;

     public
       constructor Create(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); virtual;
       destructor Destroy; override;

       procedure Write(const someText : string; Indentation : integer = 0); overload;
       procedure WriteEmptyLine;

       procedure Convert(const aLibrary: TRODLLibrary; const aTargetEntity : string = '');
       class function GetTargetFileName(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''): string; virtual;

       property Buffer : TStrings read GetBuffer;
       property TargetEntity : string read fTargetEntity;
     end;

     TRODLConverterClass = class of TRODLConverter;

     {--------------  TRODLReader -------------- }
     TRODLReader = class
     private
     protected
       function IntReadFromStream(aStream : TStream) : TRODLLibrary; virtual; abstract;

     public
       constructor Create; virtual;

       function Read(aStream : TStream) : TRODLLibrary;
       function ReadFromFile(const aFileName : string) : TRODLLibrary;
     end;

     TRODLReaderClass = class of TRODLReader;

{ Summary: GUID creator function wrapper
  Description: GUID creator function wrapper. }
function NewUID : TGUID;

{ Summary: Reads a file and returns a TRODLLibrary
  Description:
    Reads a file and returns a TRODLLibrary. By specifying the
    reader class of your choice you can import a variety of
    documents and not worry about the conversion details. The
    simplest example is to read the actual RODL file contained in
    your project's directory.

    In order to do that use the following code:
    
    <CODE>
    uses uXMLToRODL, uRODL;
    var library : TRODLLibrary;
    begin
      [..]
      library := ReadRODLFromFile(TXMLToRODL, 'c:myprojectdirmyrodlfile.rodl');
      [..]
    end.
    </CODE>}
function ReadRODLFromFile(aReaderClass : TRODLReaderClass; const aFileName : string) : TRODLLibrary;

implementation

uses {$IFDEF DELPHI5}
     ActiveX,
     {$ENDIF}
     {$IFDEF DOTNET}
     Borland.Delphi.Sysutils,
     {$ELSE}
     SysUtils, TypInfo,
     {$ENDIF DOTNET}
     uRORes;

{ Support routines }
function CleanupText(const someText : string) : string;
var i : integer;
begin
  result := Trim(someText);
  for i := 1 to Length(result) do
    case result[i] of
      '0'..'9' : begin
        if (i=1) then result[i] := '_';
      end;
      'a'..'z',
      'A'..'Z',
      '_' : Continue;
      else result[i] := '_';
    end;
end;

function NewUID : TGUID;
begin
  {$IFDEF DELPHI5}
  CoCreateGuid(result);
  {$ELSE}
  CreateGUID(result)
  {$ENDIF}
end;

function ReadRODLFromFile(aReaderClass : TRODLReaderClass; const aFileName : string) : TRODLLibrary;
begin
  with aReaderClass.Create do try
    result := ReadFromFile(aFileName);
  finally
    Free;
  end;
end;

{ TRODLNameInfo }

constructor TRODLNameInfo.Create(const aName: string);
begin
  Name := aName;
end;

destructor TRODLNameInfo.Destroy;
begin
  if Assigned(fAttributes) then fAttributes.Free;
  inherited;
end;

function TRODLNameInfo.GetAttributes: TStrings;
begin
  if (fAttributes=NIL) then fAttributes := TStringList.Create;
  result := fAttributes;
end;

function TRODLNameInfo.GetIsValid: boolean;
begin
  result := (Trim(Name)<>'');
end;

procedure TRODLNameInfo.SetName(const Value: string);
begin
  fName := CleanupText(Value);
end;

{ TRODLEntityInfo }

constructor TRODLEntityInfo.Create(const aName :string; const aUID: TGUID; const someDocumentation : string);
begin
  inherited Create(aName);

  UID := aUID;
  Documentation := someDocumentation;
end;

function TRODLEntityInfo.GetIsValid: boolean;
begin
  result := inherited GetIsValid and not IsEqualGUID(UID, EmptyGUID);
end;

procedure TRODLEntityInfo.SetDocumentation(const Value: string);
begin
  fDocumentation := CleanupText(Value);
end;

{ TRODLEntity }

constructor TRODLEntity.Create;
begin
  fInfo := RODLTypeInfoClass.Create('');
end;

function TRODLEntity.GetInfo: TRODLEntityInfo;
begin
  result := TRODLEntityInfo(fInfo)
end;

function TRODLEntity.Validate(out FaultyEntity : TRODLEntity; out ErrorMessage : string): boolean;
begin
  result := Info.IsValid;
  if not result then begin
    FaultyEntity := Self;
    ErrorMessage := err_InvalidInfo;
  end;
end;

function TRODLEntity.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLNameInfo
end;

{ TRODLComplexEntity }

function TRODLComplexEntity.Add(anEntity: TRODLEntity): integer;
begin
  if not (anEntity is ItemClass) then RaiseError(err_InvalidType, [anEntity.ClassName, ItemClass.ClassName]);
  result := fItems.Add(anEntity);
end;

function TRODLComplexEntity.Add: TRODLEntity;
begin
  result := ItemClass.Create;
  Add(result);
end;

procedure TRODLComplexEntity.Clear;
begin
  fItems.Clear;
end;

constructor TRODLComplexEntity.Create; 
begin
  inherited Create;

  fItems := TObjectList.Create;
end;

procedure TRODLComplexEntity.Delete(Index: integer);
begin
  fItems.Delete(Index);
end;

destructor TRODLComplexEntity.Destroy;
begin
  fItems.Free;
  inherited;
end;

function TRODLComplexEntity.GetCount: integer;
begin
  result := fItems.Count
end;

function TRODLComplexEntity.Validate(out FaultyEntity : TRODLEntity; out ErrorMessage : string): boolean;
var i, n : integer;
begin
  result := inherited Validate(FaultyEntity, ErrorMessage);
  if not result then Exit;

  result := FALSE;

  // Check type names are unique
  for i := 0 to (Count-2) do
    for n := i+1 to (Count-1) do
      if (CompareText(Items[i].Info.Name, Items[n].Info.Name)=0) then begin
        FaultyEntity := Items[n];
        ErrorMessage := Format(err_DuplicateName, [Items[n].Info.Name]);
        Exit;
      end;

  // Check they are all valid on a basic level
  for i := 0 to (Count-1) do
    if not Items[i].Validate(FaultyEntity, ErrorMessage) then Exit;

  result := TRUE;
end;

function TRODLComplexEntity.GetItems(Index: integer): TRODLEntity;
begin
  result := TRODLEntity(fItems[Index]);
end;

function TRODLComplexEntity.ItemByName(const aName: string): TRODLEntity;
var i : integer;
begin
  result := NIL;

  for i := 0 to (Count-1) do
    if (CompareText(Items[i].Info.Name, aName)=0) then begin
      result := Items[i];
      Exit;
    end;
end;

function TRODLComplexEntity.ItemByUID(const aUID: TGUID): TRODLEntity;
var i : integer;
begin
  result := NIL;

  for i := 0 to (Count-1) do
    if IsEqualGUID(Items[i].Info.UID, aUID) then begin
      result := Items[i];
      Exit;
    end;
end;

function TRODLComplexEntity.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  // All the descendants except TRODLService will use this
  result := TRODLEntityInfo
end;

procedure TRODLComplexEntity.Remove(anEntity: TRODLEntity);
begin
  fItems.Remove(anEntity)
end;

procedure TRODLComplexEntity.Exchange(Index1, Index2: integer);
begin
  fItems.Exchange(Index1, Index2);
end;

{ TRODLTypedElementInfo }

constructor TRODLTypedElementInfo.Create(const aName, aTypeName: string);
begin
  inherited Create(aName);

  DataType := aTypeName;
end;

function TRODLTypedElementInfo.GetIsValid: boolean;
begin
  result := inherited GetIsValid and (Trim(DataType)<>'');
end;

procedure TRODLTypedElementInfo.SetDataType(const Value: string);
begin
  fDataType := CleanupText(Value);
end;

{ TRODLTypedEntity }

function TRODLTypedEntity.GetInfo: TRODLTypedElementInfo;
begin
  result := TRODLTypedElementInfo(inherited Info)
end;

function TRODLTypedEntity.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLTypedElementInfo
end;

{ TRODLStruct }

function TRODLStruct.Add(aStructElement: TRODLTypedEntity): integer;
begin
  result := inherited Add(aStructElement)
end;

function TRODLStruct.Add: TRODLTypedEntity;
begin
  result := TRODLTypedEntity(inherited Add);
  result.Info.Name := 'Field'+IntToStr(Count);
end;

function TRODLStruct.GetItemClass;
begin
  result := TRODLTypedEntity;
end;

function TRODLStruct.GetItems(Index : integer): TRODLTypedEntity;
begin
  result := TRODLTypedEntity(inherited Items[Index]);
end;

{ TRODLEnum }

function TRODLEnum.Add(aStructElement: TRODLEnumValue): integer;
begin
  result := inherited Add(aStructElement)
end;

function TRODLEnum.Add: TRODLEnumValue;
begin
  result := TRODLEnumValue(inherited Add);
  result.Info.Name := 'Value'+IntToSTr(Count);
end;

function TRODLEnum.GetItemClass : TRODLEntityClass;
begin
  result := TRODLEnumValue;
end;

function TRODLEnum.GetItems(Index: integer): TRODLEnumValue;
begin
  result := TRODLEnumValue(inherited Items[Index]);
end;

{ TRODLService }

function TRODLService.Add(aServiceInterface: TRODLServiceInterface): integer;
begin
  result := inherited Add(aServiceInterface);
end;

function TRODLService.Add: TRODLServiceInterface;
begin
  result := TRODLServiceInterface(inherited Add);
end;

constructor TRODLService.Create;
var defintf : TRODLServiceInterface;
begin
  inherited;

  defintf := Add;
  defintf.Info.Name := DefaultIntfName;
  defintf.Info.UID := NewUID;
end;

function TRODLService.GetDefault: TRODLServiceInterface;
var i : integer;
begin
  result := NIL;
  if (Count=1) then result := Items[0]
  else for i := 0 to (Count-1) do
    if (CompareText(Items[i].Info.name, DefaultIntfName)=0) then begin
      result := Items[i];
      Exit;
    end;
end;

function TRODLService.GetInfo: TRODLNameInfo;
begin
  result := TRODLNameInfo(inherited GetInfo);
end;

function TRODLService.GetItemClass : TRODLEntityClass;
begin
  result := TRODLServiceInterface;
end;

function TRODLService.GetItems(Index: integer): TRODLServiceInterface;
begin
  result := TRODLServiceInterface(inherited Items[Index]);
end;

function TRODLService.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLNameInfo;
end;

{ TRODLOperationParamInfo }

constructor TRODLOperationParamInfo.Create(const aName, aTypeName: string;
  aFlag: TRODLParamFlag);
begin
  inherited Create(aName, aTypeName);

  Flag := aFlag;
end;

{ TRODLOperationParam }

function TRODLOperationParam.GetInfo: TRODLOperationParamInfo;
begin
  result := TRODLOperationParamInfo(inherited Info)
end;

function TRODLOperationParam.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLOperationParamInfo
end;

{ TRODLOperation }

function TRODLOperation.Add(aParam: TRODLOperationParam): integer;
begin
  result := inherited Add(aParam)
end;

function TRODLOperation.Add: TRODLOperationParam;
begin
  result := TRODLOperationParam(inherited Add);
  result.Info.Name := 'Param'+IntToStr(Count);  
end;

function TRODLOperation.GetItemClass : TRODLEntityClass;
begin
  result := TRODLOperationParam;
end;

function TRODLOperation.GetItems(Index: integer): TRODLOperationParam;
begin
  result := TRODLOperationParam(inherited Items[Index]);
end;

function TRODLOperation.GetResult: TRODLOperationParam;
var i : integer;
begin
  result := NIL;
  for i := 0 to (Count-1) do
    if (Items[i].Info.Flag=fResult) then begin
      result := Items[i];
      Exit;
    end;
end;

{ TRODLServiceInterface }

function TRODLServiceInterface.Add(aParam: TRODLOperation): integer;
begin
  result := inherited Add(aParam)
end;

function TRODLServiceInterface.Add: TRODLOperation;
begin
  result := TRODLOperation(inherited Add);
  result.Info.Name := 'Operation'+IntToStr(Count);
  result.Info.UID := NewUID;  
end;

function TRODLServiceInterface.GetItemClass : TRODLEntityClass;
begin
  result := TRODLOperation;
end;

function TRODLServiceInterface.GetItems(Index: integer): TRODLOperation;
begin
  result := TRODLOperation(inherited Items[Index]);
end;

{ TRODLLibrary }

function TRODLLibrary.Add(anEntity: TRODLEntity): integer;
begin
  if (anEntity is TRODLArray) then Inc(fArrayCount)
  else if (anEntity is TRODLEnum) then Inc(fEnumCount)
  else if (anEntity is TRODLStruct) then Inc(fStructCount)
  else if (anEntity is TRODLService) then Inc(fServiceCount);

  result := inherited Add(anEntity);
end;

function TRODLLibrary.GetItemClass : TRODLEntityClass;
begin
  result := TRODLEntity;
end;

procedure TRODLLibrary.Remove(anEntity: TRODLEntity);
begin
  if (anEntity is TRODLArray) then Dec(fArrayCount)
  else if (anEntity is TRODLEnum) then Dec(fEnumCount)
  else if (anEntity is TRODLStruct) then Dec(fStructCount)
  else if (anEntity is TRODLService) then Dec(fServiceCount);

  inherited;
end;

procedure TRODLLibrary.Delete(Index: integer);
var item : TRODLEntity;
begin
  item := Items[Index];

  if (item is TRODLArray) then Dec(fArrayCount)
  else if (item is TRODLEnum) then Dec(fEnumCount)
  else if (item is TRODLStruct) then Dec(fStructCount)
  else if (item is TRODLService) then Dec(fServiceCount);

  inherited Delete(Index);
end;

function TRODLLibrary.GetArray(Index: integer): TRODLArray;
var i, c : integer;
begin
  result := NIL;
  if (Index>ArrayCount-1) then RaiseError(err_InvalidIndex, [Index]);

  c := -1;
  for i := 0 to (Count-1) do begin
    if (Items[i] is TRODLArray) then Inc(c);
    if (c=Index) then begin
      result := TRODLArray(Items[i]);
      Exit;
    end;
  end;
end;

function TRODLLibrary.GetEnums(Index: integer): TRODLEnum;
var i, c : integer;
begin
  result := NIL;
  if (Index>EnumCount-1) then RaiseError(err_InvalidIndex, [Index]);

  c := -1;
  for i := 0 to (Count-1) do begin
    if (Items[i] is TRODLEnum) then Inc(c);
    if (c=Index) then begin
      result := TRODLEnum(Items[i]);
      Exit;
    end;
  end;
end;

function TRODLLibrary.GetServices(Index: integer): TRODLService;
var i, c : integer;
begin
  result := NIL;
  if (Index>ServiceCount-1) then RaiseError(err_InvalidIndex, [Index]);

  c := -1;
  for i := 0 to (Count-1) do begin
    if (Items[i] is TRODLService) then Inc(c);
    if (c=Index) then begin
      result := TRODLService(Items[i]);
      Exit;
    end;
  end;
end;

function TRODLLibrary.GetStructs(Index: integer): TRODLStruct;
var i, c : integer;
begin
  result := NIL;
  if (Index>StructCount-1) then RaiseError(err_InvalidIndex, [Index]);

  c := -1;
  for i := 0 to (Count-1) do begin
    if (Items[i] is TRODLStruct) then Inc(c);
    if (c=Index) then begin
      result := TRODLStruct(Items[i]);
      Exit;
    end;
  end;
end;


constructor TRODLLibrary.Create;
begin
  inherited;
  Info.Name := 'NewLibrary';
  Info.UID := NewUID;
end;

{ TRODLEnumValue }

function TRODLEnumValue.GetInfo: TRODLNameInfo;
begin
  result := TRODLNameInfo(inherited Info)
end;

function TRODLEnumValue.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLNameInfo;
end;

{ TRODLArray }

function TRODLArray.RODLTypeInfoClass: TRODLNameInfoClass;
begin
  result := TRODLEntityInfo
end;

procedure TRODLArray.SetElementType(const Value: string);
begin
  fElementType := Trim(Value);
end;

function TRODLArray.Validate(out FaultyEntity: TRODLEntity;
  out ErrorMessage: string): boolean;
begin
  result := inherited Validate(FaultyEntity, ErrorMessage) and (ElementType<>'');
  if not Result then begin
    ErrorMessage := err_InvalidInfo;
    FaultyEntity := Self;
  end;
end;

{ TRODLConverter }
constructor TRODLConverter.Create(const aLibrary : TRODLLibrary; const aTargetEntity : string = '');
begin
  inherited Create;
  
  fBuffer := TStringList.Create;
  if (aLibrary<>NIL)
    then Convert(aLibrary, aTargetEntity);
end;

destructor TRODLConverter.Destroy;
begin
  fBuffer.Free;
  inherited;
end;

procedure TRODLConverter.Convert(const aLibrary: TRODLLibrary; const aTargetEntity : string = '');
begin
  if (aLibrary=NIL) then RaiseError(err_InvalidLibrary, [])
  else if not ValidateTargetEntity(aLibrary, aTargetEntity) then RaiseError(err_InvalidTargetEntity, [aTargetEntity]);

  fTargetEntity := aTargetEntity;

  fBuffer.Clear;
  IntConvert(aLibrary, aTargetEntity);
end;

function TRODLConverter.GetBuffer: TStrings;
begin
  result := fBuffer as TStrings;
end;

function TRODLConverter.ValidateTargetEntity(
  const aLibrary: TRODLLibrary; const aTargetEntity: string): boolean;
begin
  result := TRUE;
end;

procedure TRODLConverter.Write(const someText: string; Indentation : integer = 0);
var i : integer;
    s : string;
begin
  s := '';

  for i := 1 to Indentation do s := s+' ';
  s := s+someText;

  fBuffer.Add(s)
end;

procedure TRODLConverter.WriteEmptyLine;
begin
  fBuffer.Add('');
end;

class function TRODLConverter.GetTargetFileName(const aLibrary: TRODLLibrary;
  const aTargetEntity: string): string;
begin
  result := '';
end;


{ TRODLReader }

constructor TRODLReader.Create;
begin
end;

function TRODLReader.Read(aStream: TStream): TRODLLibrary;
begin
  if (aStream=NIL) then RaiseError(err_InvalidStream, []);
  result := IntReadFromStream(aStream)
end;

function TRODLReader.ReadFromFile(const aFileName: string): TRODLLibrary;
var fs : TFileStream;
begin
  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    result := Read(fs)
  finally
    fs.Free;
  end;
end;

end.
