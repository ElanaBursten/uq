unit uROIDEMenu;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

{ Service Builder command line parameters:

    /rodlfile:filename         : File to load
    /projectname:dpr-file-name : Just for display; implies /autosave
    /autosave                  : Auto-save change when SB loses focus or is closed
    /register                  : Quite, register filetypes (.rodl to lauch with SB)
    /import-wsdl:file-or-url   : Auto-import the WSDL into the specified (or empty) RODL file
    /import                    : Show a selection box to determine what type of file to import.

}

uses Forms,
     Classes, Windows, SysUtils,{$IFDEF DELPHI6UP}DesignEditors, DesignIntf, {$ELSE} DsgnIntf,{$ENDIF}
     ToolsApi, Menus, Contnrs, ComObj, Graphics;

type
  { TMenuItemInfo }
  TMenuItemInfo = record
    Caption,
    ShortCut : string;
    Menu:TMenuItem;
    ImageIndex:integer;
  end;

resourcestring sMakeServerCaption = '&Turn %s into a RemObjects Server';

const MAX_MENU_ITEM = 6;

var                                               
  MenuItems : array[0..MAX_MENU_ITEM] of TMenuItemInfo = (
    (Caption: '&Service Builder';            ShortCut: 'CTRL+ALT+U'; Menu:nil; ImageIndex: 0),
    (Caption: '&Import Service';             ShortCut: 'CTRL+ALT+I'; Menu:nil; ImageIndex: 1),
    (Caption: '-';                           ShortCut: '';           Menu:nil; ImageIndex: -1),
    (Caption: '&Regenerate Units from RODL'; ShortCut: 'CTRL+ALT+E'; Menu:nil; ImageIndex: 2),
    (Caption: sMakeServerCaption;            ShortCut: '';           Menu:nil; ImageIndex: 3),
    (Caption: '-';                           ShortCut: '';           Menu:nil; ImageIndex: -1),
    (Caption: '&About RemObjects SDK';       ShortCut: '';           Menu:nil; ImageIndex: -1)
    //(Caption: '&Import Services'; ShortCut: 'CTRL+ALT+S')
  );


const  
  mi_RODLEditor      = 0;
  mi_ImportServices  = 1;
  mi_Regenerate      = 3;
  mi_ConvertToServer = 4;
  mi_About           = 6;

type
  { TROMenuWizard }
  TROMenuWizard = class(TInterfacedObject, IOTAWizard, IOTANotifier)
  private
    fROMenu : TMenuItem;

    function GetServiceBuilderPath: string;
    function LaunchServiceBuilder(const aProjectName,
      someParams: string): boolean;
    function PromptFileName(const aDefFileName, aFilter : string; out oFilename:string) : boolean;
    function CurrentProjectIsRodlProject: boolean;

  protected
    { Misc }
    procedure CreateMenuItems;
    procedure ShowServiceBuilder(Sender : TObject);
    procedure ImportServices(Sender: TObject);
    procedure OnRegenerateUnits(Sender: TObject);
    procedure OnMenuPopup(Sender: TObject);
    procedure OnConvertToServer(Sender: TObject);
    procedure OnAbout(Sender: TObject);

    { IOTANotifier}
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Execute;
    procedure Modified;

    { IOTAWizard }
    function GetState: TWizardState;
    function GetIDString: string;
    function GetName: string;

  public

    constructor Create;
    destructor Destroy; override;
  end;

procedure Register;

implementation

uses ShellAPI, Controls, uRODL, uROIDETools, uRODLToXML, uRODLToIntf, uRODLGenTools, uRORODLNotifier, Dialogs,
  fRoPleaseWaitForm, uROIDEData, fROAbout;

procedure Register;
begin
  RegisterPackageWizard(TROMenuWizard.Create);
end;

{----------------------------------------------------------------------------}
// Taken from eFiles.pas from the eLibrary 6.0
function ExecuteAndWait(App,CmdLine:string):dword;
var StartupInfo:TStartupInfo;
    ProcessInfo:TProcessInformation;
    //Exitcode:dword;
    s:array[0..MAX_PATH] of char;
begin
  GetStartupInfo(Startupinfo);

  ExpandEnvironmentStrings(pChar(App),@s,MAX_PATH);
  App := string(pChar(@s));

  //writeln('execute: '+App+' '+CmdLine);
  if CreateProcess(pChar(App),
                   pChar('"'+App+'" '+CmdLine),
                   nil,nil,false,0,nil,
                   pChar(GetCurrentDir),
                   StartupInfo,
                   ProcessInfo) then begin
    CloseHandle(ProcessInfo.hThread);

    { Wait till app terminates, but don't block main thread. }
    while WaitForSingleObject(ProcessInfo.hProcess,100) = WAIT_TIMEOUT do
      Application.ProcessMessages();

    GetExitCodeProcess(ProcessInfo.hProcess,result);
    CloseHandle(ProcessInfo.hProcess);
  end
  else raise Exception.CreateFmt('could not create process %s %s: %d',[App,CmdLine,GetLastError]);
end;



{ TROMenuWizard }

constructor TROMenuWizard.Create;
begin
  inherited Create;

  CreateMenuItems;
end;

destructor TROMenuWizard.Destroy;
begin
  fROMenu.Free;
end;

function TROMenuWizard.GetIDString: string;
begin
  Result := '{137E9A72-CBF8-485F-9421-212866E99084}';
end;

function TROMenuWizard.GetName: string;
begin
  Result := 'ROMenuWizard';
end;

// The following are stubs that Delphi never calls.
procedure TROMenuWizard.AfterSave;
begin
end;

procedure TROMenuWizard.BeforeSave;
begin
end;

procedure TROMenuWizard.Destroyed;
begin
end;

procedure TROMenuWizard.Execute;
begin
end;

function TROMenuWizard.GetState: TWizardState;
begin
  Result := [];
end;

procedure TROMenuWizard.Modified;
begin
end;

procedure TROMenuWizard.CreateMenuItems;
var mainmenu : TMainMenu;
    item     : TMenuItem;
    i        : Integer;
    lBitmap  : TBitmap;
begin
  fROMenu := TMenuItem.Create(nil);
  fROMenu.Caption := 'Rem&Objects';
  fROMenu.OnClick := OnMenuPopup;

  with TIdeData.Create(nil) do try

    lBitmap := TBitmap.Create();
    try

      for i := 0 to High(MenuItems) do begin
        item := TMenuItem.Create(fROMenu);
        item.Caption := MenuItems[i].Caption;
        item.ShortCut := TextToShortCut(MenuItems[i].ShortCut);
        item.Tag := i;

        if MenuItems[i].ImageIndex > -1 then begin
          iml_Actions.GetBitmap(MenuItems[i].ImageIndex,lBitmap);
          item.ImageIndex := (BorlandIDEServices as INTAServices).AddMasked(lBitmap,clFuchsia);
        end;

        MenuItems[i].Menu := item;

        case i of
          mi_RODLEditor     : item.OnClick := ShowServiceBuilder;
          mi_ImportServices : item.OnClick := ImportServices;
          mi_Regenerate     : item.OnClick := OnRegenerateUnits;
          mi_ConvertToServer: item.OnClick := OnConvertToServer;
          mi_About          : item.OnClick := OnAbout;
          else item.OnClick := NIL;
        end;

        fROMenu.Add(item);
      end;

    finally
      lBitmap.Free();
    end;

  finally
    Free();
  end;

  mainmenu := (BorlandIDEServices as INTAServices).MainMenu;
  mainmenu.Items.Insert(mainmenu.Items.Count-2, fROMenu);
end;

procedure TROMenuWizard.OnMenuPopup(Sender: TObject);
var lCurrentProjectIsRodlProject:boolean;
    lProject:IOTAProject;
begin
  lCurrentProjectIsRodlProject := CurrentProjectIsRodlProject;
  MenuItems[mi_Regenerate].Menu.Visible := lCurrentProjectIsRodlProject;

  lProject := CurrentProject;
  if Assigned(lProject) then begin
    MenuItems[mi_ConvertToServer].Menu.Visible := not lCurrentProjectIsRodlProject;
    MenuItems[mi_ConvertToServer].Menu.Caption := Format(sMakeServerCaption,[ChangeFileExt(ExtractFileName(lProject.GetFileName),'')]);
  end
  else begin
    MenuItems[mi_ConvertToServer].Menu.Visible := false;
  end;


end;

procedure TROMenuWizard.OnConvertToServer(Sender: TObject);
var lProject:IOTAProject;
    lSource:string;
begin

  lProject := CurrentProject;
  if Assigned(lProject) then begin

    lSource := ReadModuleSource(lProject);
    lSource := StringReplace(lSource,'{$R *.res}',
      '{#ROGEN:'+ExtractFileName(ChangeFileExt(lProject.FileName,'.rodl'))+'} // RemObjects: Careful, do not remove!'#13#10+
      '{$R RODLFile.res}'#13#10+
      '{$R *.res}',
      [rfIgnoreCase]);

    WriteModuleSource(lProject,lSource);
    ShowServiceBuilder(Sender);
  end;
end;

procedure TROMenuWizard.OnRegenerateUnits(Sender: TObject);
var lIgnore:boolean;
begin
  with TRORODLNotifier.Create do try
    BeforeCompile(CurrentProject,false,lIgnore);
  finally
    Free();
  end;
end;

function TROMenuWizard.LaunchServiceBuilder(const aProjectName, someParams : string) : boolean;
var exename : string;
begin
  result := FALSE;
  exename := GetServiceBuilderPath();

  if not FileExists(exename)
    then MessageDlg(Format('Cannot find "%s"', [exename]), mtError, [mbOK], 0)
    else result := (ShellExecute(0, 'open', PChar(exename), PChar(someParams), PChar(ExtractFilePath(aProjectName)), SW_NORMAL)>32)
end;

function TROMenuWizard.CurrentProjectIsRodlProject:boolean;
var prj : IOTAProject;
    lSource:string;
begin
  prj := CurrentProject;
  if (prj <> nil) then begin
    lSource := ReadModuleSource(prj);
    result := ExtractRODLFileName(lSource) <> '';
  end
  else begin
    result := false;
  end;
end;

procedure TROMenuWizard.ShowServiceBuilder(Sender: TObject);
var prj : IOTAProject;
    fname,
    params,
    src : string;
begin
  // TODO: Clean up these hardcoded things
  prj := CurrentProject;
  if (prj=NIL) then LaunchServiceBuilder('', '')
  else begin
    src := ReadModuleSource(prj);
    fname := ModuleDir(prj)+ExtractRODLFileName(src);


    if (ExtractFileName(fname)<>'')
      then params := Format('/rodlfile:"%s" /projectname:"%s" /autosave', [fname, ExtractFileName(prj.FileName)])
      else params := '';

    LaunchServiceBuilder(prj.FileName, params);
  end;
end;

function TROMenuWizard.PromptFileName(const aDefFileName, aFilter : string; out oFilename:string) : boolean;
begin

  with TSaveDialog.Create(NIL) do try
    FileName := aDefFileName;
    Filter   := aFilter;
    DefaultExt := 'pas';
    result := Execute();
    if result then oFileName := FileName;
  finally
    Free;
  end;                                           
end;

procedure TROMenuWizard.ImportServices(Sender: TObject);
var params, fname{, loc} : string;
    tempdir : array[0..1000] of char;
    lib : TRODLLibrary;
    prj : IOTAProject;                            
begin
  //if not GetImportInputs(loc) then Exit;

  GetTempPath(SizeOf(tempdir), tempdir);
  fname := IncludeTrailingBackslash(tempdir)+GUIDToString(NewUID)+'.rodl';


  prj := CurrentProject;
  if (prj <> NIL) then begin
    params := Format('/import /rodlfile:"%s" /autosave /projectname:"(Import Service for %s)"', [fname,ExtractFileName(prj.FileName)]);
  end
  else begin
    params := Format('/import /rodlfile:"%s" /autosave', [fname]);
  end;

  //params := Format('/import-wsdl:%s /rodlfile:%s /autosave', [loc, fname]);
  //params := Format('/import /rodlfile:%s /autosave', [fname]);
  //ShowMessage(params);

  with TPleaseWaitForm.Create(Application,'Importing via the ServiceBuilder...') do try
    Show();
    ExecuteAndWait(GetServiceBuilderPath, params);
    Hide();
  finally
    Free();
  end;
  
  {if not LaunchServiceBuilder('', params) then Exit
  else} begin
    //MessageDlg('Service Builder has been launched. You can now analyze the result of the import', mtInformation, [mbOK], 0);

    if not FileExists(fname) then begin
      MessageDlg(Format('Cannot find "%s"', [fname]), mtError, [mbOK], 0);
      Exit;
    end;

    // Loads the RODL file that was just generated
    with TXMLToRODL.Create do try
      lib := ReadFromFile(fname);
    finally
      Free;
    end;

    with TRODLToIntf.Create(NIL) do try
      Convert(lib, '');
      if not PromptFileName('ImportedService.pas', '', fName) then exit;
      if (fname<>'') then begin
        Buffer[0] := Format('unit %s;', [ChangeFileExt(ExtractFileName(fname), '')]); 
        Buffer.SaveToFile(fname);
      end;

      prj := CurrentProject;
      if (prj<>NIL) then begin
        if (MessageDlg(Format('Do you want to add "%s" to the current project?', [fname]),
            mtInformation, [mbYes, mbNo], 0)=mrYes) then begin
          prj.AddFile(fname, TRUE);
        end;
      end;

    finally
      Free;
      lib.Free;
    end;
  end;
end;

function TROMenuWizard.GetServiceBuilderPath: string;
begin
  result := GetBinDir+'RoServiceBuilder.exe';
end;

procedure TROMenuWizard.OnAbout(Sender: TObject);
begin
  with TAboutForm.Create(Application) do try
    Position := poScreenCenter;
    ShowModal();
  finally
    Free();
  end
end;

end.
