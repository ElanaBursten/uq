{ Summary: Contains the class TROWinInetHTTPChannel }
unit uROWinInetHttpChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, WinInet,
  uRORes, uROProxy, uROClientIntf;


type {--------------- TROWinINetHttpChannel ---------------}

     TStatusEvent = procedure(Sender:TObject; Transferred, Total : integer) of object;

     TLogin = class(TPersistent)
     private
        fUsername,
        fPassword: string;
     published
       property Username:string read fUsername write fUsername;
       property Password:string read fPassword write fPassword;
     end;

     { Summary: Channel for HTTP communication using the standard Windows WinINet library.
       Description:
        It dispaches messages to the server using the standard Windows WinINet library.
        This channel automatically reads your Internet Explorer settings and is capable of working
        through proxyes without the need for any extra configuration to be made. }
     TROWinInetHTTPChannel = class(TROTransportChannel, IROTransport, IROTCPTransport, IROHTTPTransport)
     private
       fUserAgent:string;
       //fContentType: string;
       fTargetUrl:string;
       fStoreConnected:boolean;
       fKeepConnection:boolean;
       fOnStatus:TStatusEvent;

       fHeaders:string;

       fInetConnect: HINTERNET;
       fInetRoot: HINTERNET;
       fUrlScheme: Integer;
       fUrlHost: string;
       fUrlSite: string;
       fUrlPort: Integer;
       fLogin: TLogin;

       procedure SetConnected(iValue:boolean);
       function GetConnected:boolean;
       procedure SetLogin(const Value: TLogin);

       function SendData(iData:TStream):hInternet;

       procedure Check(Error: Boolean);
       procedure ReceiveData(iRequest:hInternet; ioData:TStream);
     protected
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the server using WinINet functions. }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       {--------------- IROTCPTransport ---------------}
       function GetClientAddress : string;

       {--------------- IROHTTPTransport ---------------}
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;
       function GetContentType : string;
       procedure SetContentType(const aValue : string);
       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);

       procedure SetTargetURL(const Value: string);
       function GetTargetURL:string;

       function GetPathInfo : string;
       function GetLocation : string;

       procedure TriggerStatus(iTransfered,iTotal:integer);

     public
       constructor Create(AOwner: TComponent); override;
       destructor Destroy; override;

     published
       { Summary: Specifies the agent originating the request
         Description:
           Specifies the agent originating the request. See also TWebRequest.UserAgent in your Delphi help }
       property UserAgent:string read GetUserAgent write SetUserAgent;

       { Summary: Specifies the target URI for the message dispatch
         Description:
           Specifies the target URI for the message dispatch.

           If you importeed a SOAP web service using the Service Builder, this property will be automatically
           assigned a value for you using the URI specified in the WSDL document.

           Refer to the property TROServer.Dispatchers for more info about setting your services URIs. }
       property TargetURL : string read fTargetURL write SetTargetURL;

       { Summary: Login info for password-restricted web sites
         Description: Login info for password-restricted web sites }
       property Login:TLogin read fLogin write SetLogin;
       { Summary: Internal.
         Description: Internal. }
       property StoreConnected:boolean read fStoreConnected write fStoreConnected default false;
       { Summary: Opens the HTTP connections
         Description:
           Opens the HTTP connection with the host specified using TargetURL.
           It's not necessary for you to set Connected to true unless you are accessing a server
           that handles persistent HTTP connections. }
       property Connected:boolean read GetConnected write SetConnected stored fStoreConnected default false;
       { Summary: Keeps the HTTP connection open after the first request
         Description:
           Keeps the HTTP connection open after the first request. The server has to support this.}
       property KeepConnection:boolean read fKeepConnection write fKeepConnection default false;
       { Summary: Progress notification event
         Description:
           Progress notification event used when receiving responses }
       property OnStatus:TStatusEvent read fOnStatus write fOnStatus;
     end;

resourcestring sxInvalidURLformat = 'Invalid URL format';
               //sRemObjectsSDK = 'RemObjects SDK';


implementation

uses SysUtils, Windows, uROHTTPTools;

{ TROWinINetHTTPChannel }

procedure TROWinINetHTTPChannel.Check(Error: Boolean);
var ErrCode:integer;
    S:string;
begin
  ErrCode := getlasterror;
  if Error and (ErrCode <> 0) then begin
    SetLength(S, 256);
    if FormatMessage(FORMAT_MESSAGE_FROM_HMODULE, Pointer(GetModuleHandle('wininet.dll')), ErrCode, 0, PChar(S), Length(S), nil) > 0 then begin;
      SetLength(S, StrLen(PChar(S)));
      while (Length(S) > 0) and (S[Length(S)] in [#10, #13]) do SetLength(S, Length(S) - 1);
      raise Exception.Create(S);
    end
    else begin
      raise Exception.CreateFmt(err_UnexpectedWinINetProblem,[ErrCode]);
    end;

  end;
end;

constructor TROWinInetHTTPChannel.Create(AOwner: TComponent);
begin
  inherited;
  UserAgent := str_ProductName;
  fLogin := TLogin.Create();
end;

destructor TROWinInetHTTPChannel.Destroy;
begin
  FreeAndNil(fLogin);
  inherited;
end;

function TROWinInetHTTPChannel.GetConnected: boolean;
begin
  result := Assigned(fInetConnect);
end;

function TROWinINetHTTPChannel.GetContentType: string;
begin
  result := GetHeaders(id_ContentType);
end;

procedure TROWinINetHTTPChannel.SetContentType(const aValue: string);
begin
  SetHeaders(id_ContentType,aValue);
end;

function TROWinINetHTTPChannel.GetHeaders(const aName: string): string;
begin
end;

procedure TROWinINetHTTPChannel.SetHeaders(const aName, aValue: string);
begin
  if fHeaders <> '' then fHeaders := fHeaders+#13#10;
  fHeaders := fHeaders+aName+': '+aValue;
end;

function TROWinINetHTTPChannel.GetClientAddress: string;
begin
  result := '';
end;

function TROWinINetHTTPChannel.GetLocation: string;
begin
  result := '';
end;

function TROWinINetHTTPChannel.GetPathInfo: string;
begin
  result := '';
end;

function TROWinINetHTTPChannel.GetTargetURL: string;
begin
  result := fTargetUrl;
end;

function TROWinINetHTTPChannel.GetUserAgent: string;
begin
  result := fUserAgent;
end;

procedure TROWinINetHTTPChannel.SetUserAgent(const aValue: string);
begin
  if UserAgent <> aValue then
    fUserAgent := aValue;
end;

procedure TROWinInetHTTPChannel.SetLogin(const Value: TLogin);
begin
  fLogin.Assign(Value);
end;

procedure TROWinINetHTTPChannel.IntDispatch(aRequest, aResponse: TStream);
var lRequest:hInternet;
begin
  if (fTargetURL='')
    then RaiseError(err_UnspecifiedTargetURL, []);

  Connected := true;
  try
    lRequest := SendData(aRequest);
    try
      ReceiveData(lRequest,aResponse);
    finally
      InternetCloseHandle(lRequest);
    end;
  finally
    if not KeepConnection then Connected := false;
  end;
end;

function TROWinINetHTTPChannel.SendData(iData: TStream):hInternet;
var
  lHeaders: string;
  RetVal, Flags     : DWord;
  p                 : Pointer;
  AcceptTypes       : array of PChar;
  lDataStream        : TMemoryStream;
begin
  SetLength(AcceptTypes, 2);

  AcceptTypes[0] := PChar('application/octet-stream');
  AcceptTypes[1] := nil;

  Flags := INTERNET_FLAG_KEEP_CONNECTION or INTERNET_FLAG_NO_CACHE_WRITE;
  if fURLScheme = INTERNET_SCHEME_HTTPS then Flags := Flags or INTERNET_FLAG_SECURE;

  result := HttpOpenRequest(FInetConnect, 'POST', PChar(fURLSite), nil,  nil, Pointer(AcceptTypes), Flags, Integer(Self));
  Check(not Assigned(result));

  if iData is TMemoryStream then begin
    lDataStream := iData as TMemoryStream
  end
  else begin
    lDataStream := TMemoryStream.Create();
    lDataStream.LoadFromStream(iData);
  end;
  try

    while true do begin
      lHeaders := fHeaders;
      fHeaders := '';
      //Check(HttpAddRequestHeaders(result, pChar(lHeaders), Length(lHeaders), 0));
      Check(not HttpSendRequest(result, pChar(lHeaders), Length(lHeaders), lDataStream.Memory, lDataStream.Size));
      fHeaders := '';
      RetVal := InternetErrorDlg(GetDesktopWindow(), result, getlasterror,
        FLAGS_ERROR_UI_FILTER_FOR_ERRORS or FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS or
        FLAGS_ERROR_UI_FLAGS_GENERATE_DATA, p);
      case RetVal of
        ERROR_SUCCESS: Break;
        ERROR_CANCELLED: SysUtils.Abort;
        ERROR_INTERNET_FORCE_RETRY: {Retry the operation};
      end;
    end;

  finally
    if lDataStream <> iData then lDataStream.Free;
  end;

end;

procedure TROWinInetHTTPChannel.ReceiveData(iRequest:hInternet; ioData:TStream);
const MaxStatusText     : Integer = 4096;
var
  Size, Downloaded, Status, Len, Index: DWord;
  S:string;

  lTotalSize,lReceivedSize:dword;
  //lReceiveStream:TMemoryStream;
begin
  Len := SizeOf(Status);
  Index := 0;

  { Get Status Code }
  if not HttpQueryInfo(iRequest, HTTP_QUERY_STATUS_CODE or HTTP_QUERY_FLAG_NUMBER,
                       @Status, Len, Index) then RaiseLastWin32Error();

  { Throw Exception is StatusCode >= 300 BUT not 500. SOAP faulty envelopes comes with that set }
  if (Status >= 300) and (Status <> 500) then begin
    Index := 0;
    Size := MaxStatusText;
    SetLength(S, Size);
    if HttpQueryInfo(iRequest, HTTP_QUERY_STATUS_TEXT, @S[1], Size, Index) then begin
      SetLength(S, Size);
      raise Exception.CreateFmt('%s (%d)', [S, Status]);
    end;
  end;

  { get total size }
  if not HttpQueryInfo(iRequest, HTTP_QUERY_CONTENT_LENGTH or HTTP_QUERY_FLAG_NUMBER,
                       @lTotalSize, Len, Index) then RaiseLastWin32Error(); //TotalSize = 0;//

  lReceivedSize := 0;
  //lReceiveStream := TMemoryStream.Create();
  //try
    repeat
      Check(not InternetQueryDataAvailable(iRequest, Size, 0, 0));
      if Size > 0 then
      begin
        SetLength(S, Size);
        Check(not InternetReadFile(iRequest, @S[1], Size, Downloaded));
        ioData.Write(S[1], Downloaded);
        Inc(lReceivedSize, Downloaded);
        TriggerStatus(lReceivedSize, lTotalSize);
      end;
    until Size = 0;
    ioData.seek(0, soFromBeginning);

  //finally
    //ReceiveStream.Free();
  //end;

  // change mh: fire event handler to propagate status to application layer
  TriggerStatus(0,0);
  //if Assigned(OnStatus) then OnStatus(self,0,0);
  // change mh end
end;

procedure TROWinInetHTTPChannel.TriggerStatus(iTransfered,iTotal:integer);
begin
  if Assigned(OnStatus) then
    OnStatus(self, iTransfered,iTotal);
end;



procedure TROWinInetHTTPChannel.SetConnected(iValue: boolean);
var lAccessType:Integer;
begin
  if iValue and not GetConnected then begin

    {if Length(Proxy) > 0 then
      AccessType := INTERNET_OPEN_TYPE_PROXY
    else}
      lAccessType := INTERNET_OPEN_TYPE_PRECONFIG;

    fInetRoot := InternetOpen(PChar(UserAgent), lAccessType, nil, nil, 0);//PChar(Proxy)}, PChar(ProxyByPass), 0);

    if InternetAttemptConnect(0) <> ERROR_SUCCESS then SysUtils.Abort;

    Check(not Assigned(FInetRoot));
    try
      FInetConnect := InternetConnect(FInetRoot, PChar(FURLHost), FURLPort, PChar(Login.UserName), PChar(Login.Password), INTERNET_SERVICE_HTTP, 0, Cardinal(Self));
      Check(not Assigned(FInetConnect));
    except
      InternetCloseHandle(FInetRoot);
    end;
  end
  else if not iValue then begin
    if Assigned(FInetConnect) then InternetCloseHandle(FInetConnect);
    FInetConnect := nil;
    if Assigned(FInetRoot) then InternetCloseHandle(FInetRoot);
    FInetRoot := nil;
  end;
end;

procedure TROWinINetHTTPChannel.SetTargetURL(const Value: string);
var URLComp:TURLComponents;
    p:PChar;
begin
  if Targeturl <> Value then begin
    fTargetUrl := Value;

    if fTargetUrl <> '' then begin
      FillChar(URLComp, SizeOf(URLComp), 0);
      URLComp.dwStructSize := SizeOf(URLComp);
      URLComp.dwSchemeLength := 1;
      URLComp.dwHostNameLength := 1;
      URLComp.dwURLPathLength := 1;
      InternetCrackUrl(PChar(fTargetUrl), 0, 0, URLComp);
      if not (URLComp.nScheme in [INTERNET_SCHEME_HTTP, INTERNET_SCHEME_HTTPS]) then
        raise Exception.Create(sxInvalidURLformat);
      FURLScheme := URLComp.nScheme;
      FURLPort := URLComp.nPort;
      p := PChar(fTargetUrl);
      FURLHost := Copy(fTargetUrl, URLComp.lpszHostName - p + 1, URLComp.dwHostNameLength);
      FURLSite := Copy(fTargetUrl, URLComp.lpszUrlPath - p + 1, URLComp.dwURLPathLength);
    end
    else begin
      fURLPort := 0;
      fURLHost := '';
      fURLSite := '';
      fURLScheme := 0;
    end;

  end;
end;

end.
