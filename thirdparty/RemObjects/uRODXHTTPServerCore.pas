unit uRODXHTTPServerCore;

interface

///////////////////////////////////////////////////////////////////////////////
//    Component: TDXHTTPServerCore
//       Author: G.E. Ozz Nixon Jr. (onixon@dxsock.com)
// ========================================================================
// Source Owner: DX, Inc. 1995-2002
//    Copyright: All code is the property of DX, Inc. Licensed for
//               resell by Brain Patchwork DX (tm) and part of the
//               DX (r) product lines, which are (c) 1999-2002
//               DX, Inc. Source may not be distributed without
//               written permission from both Brain Patchwork DX,
//               and DX, Inc.
//      License: (Reminder), None of this code can be added to other
//               developer products without permission. This includes
//               but not limited to DCU's, DCP's, DLL's, OCX's, or
//               any other form of merging our technologies. All of
//               your products released to a public consumer be it
//               shareware, freeware, commercial, etc. must contain a
//               license notification somewhere visible in the
//               application.
//               Example is Internet Explorer - Help->About screen
//               shows the licensed code contained in the application.
// Code Version: (3rd Generation Code)
// ========================================================================
//  Description:
// ========================================================================
///////////////////////////////////////////////////////////////////////////////

{$I uRODXSock.def}

uses
{$IFDEF CODE_TRACER}
  DXCodeTracer,
{$ENDIF}
  uRODXSocket, // 3.0
  Classes,
  uRODXServerCore,
  uRODXISAPI,
  uRODXHTTPHeaderTools;

// RFC2068, Response Codes
Const
   MaxStatusCodes=40;
   StatusCodes:array[0..MaxStatusCodes] of record
      Code:Integer;
      Msg:string
   end =
  ((Code:100; Msg:'Continue'),
   (Code:101; Msg:'Switching Protocols'),
   (Code:200; Msg:'OK'),
   (Code:201; Msg:'Created'),
   (Code:202; Msg:'Accepted'),
   (Code:203; Msg:'Non-Authoritative Information'),
   (Code:204; Msg:'No Content'),
   (Code:205; Msg:'Reset Content'),
   (Code:206; Msg:'Partial Content'),
   (Code:300; Msg:'Multiple Choices'),
   (Code:301; Msg:'Moved Permanently'),
   (Code:302; Msg:'Moved Temporarily'),
   (Code:303; Msg:'See Other'),
   (Code:304; Msg:'Not Modified'),
   (Code:305; Msg:'Use Proxy'),
   (Code:306; Msg:'Unused'),
   (Code:307; Msg:'Temporary Redirect'),
   (Code:400; Msg:'Bad Request'),
   (Code:401; Msg:'Unauthorized'),
   (Code:402; Msg:'Payment Required'),
   (Code:403; Msg:'Forbidden'),
   (Code:404; Msg:'Not Found'),
   (Code:405; Msg:'Method Not Allowed'),
   (Code:406; Msg:'Not Acceptable'),
   (Code:407; Msg:'Proxy Authentication Required'),
   (Code:408; Msg:'Request Time-out'),
   (Code:409; Msg:'Conflict'),
   (Code:410; Msg:'Gone'),
   (Code:411; Msg:'Length Required'),
   (Code:412; Msg:'Precondition Failed'),
   (Code:413; Msg:'Request Entity Too Large'),
   (Code:414; Msg:'Request-URI Too Large'),
   (Code:415; Msg:'Unsupported Media Type'),
   (Code:416; Msg:'Requested Range Not Satisfiable'),
   (Code:417; Msg:'Expectation Failed'),
   (Code:500; Msg:'Internal Server Error'),
   (Code:501; Msg:'Not Implemented'),
   (Code:502; Msg:'Bad Gateway'),
   (Code:503; Msg:'Service Unavailable'),
   (Code:504; Msg:'Gateway Time-out'),
   (Code:505; Msg:'HTTP Version Not Supported'));

Type
  HTTPTBasicEvent = procedure(ClientThread:TDXClientThread;HeaderInfo:PHeaderInfo;Var EnableKeepAlive:Boolean) of object;
  HTTPTOtherEvent = procedure(ClientThread:TDXClientThread;HeaderInfo:PHeaderInfo;var Handled:Boolean) of object;

  TDXHTTPServerCore = class(TDXServerCore)
  private
    fOnCommandGET:HTTPTBasicEvent;         // GET <file> HTTP/1.0
    fOnCommandPOST:HTTPTBasicEvent;        // POST <file> HTTP/1.0
    fOnCommandHEAD:HTTPTBasicEvent;        // HEAD <file> HTTP/1.0
    fOnCommandCHECKOUT:HTTPTBasicEvent;
    fOnCommandSHOWMETHOD:HTTPTBasicEvent;
    fOnCommandPUT:HTTPTBasicEvent;
    fOnCommandDELETE:HTTPTBasicEvent;
    fOnCommandLINK:HTTPTBasicEvent;
    fOnCommandUNLINK:HTTPTBasicEvent;
    fOnCommandCHECKIN:HTTPTBasicEvent;
    fOnCommandTEXTSEARCH:HTTPTBasicEvent;
    fOnCommandSPACEJUMP:HTTPTBasicEvent;
    fOnCommandSEARCH:HTTPTBasicEvent;
    fOnCommandOPTIONS:HTTPTBasicEvent;
    fOnCommandTRACE:HTTPTBasicEvent;
    fOnCommandCONNECT:HTTPTBasicEvent;
    fOnCommandPATCH:HTTPTBasicEvent;
    fOnCommandOther:HTTPTOtherEvent;
    fEventArray:TList;
    fiTimeout:Cardinal;
    fSupportKeepAlive:Boolean;
    fDXISAPI:TDXISAPI;
  protected
    Procedure SetOnCommandGET(value:HTTPTBasicEvent);
    Procedure SetOnCommandPOST(value:HTTPTBasicEvent);
    Procedure SetOnCommandHEAD(value:HTTPTBasicEvent);
    Procedure SetOnCommandCHECKOUT(value:HTTPTBasicEvent);
    Procedure SetOnCommandSHOWMETHOD(value:HTTPTBasicEvent);
    Procedure SetOnCommandPUT(value:HTTPTBasicEvent);
    Procedure SetOnCommandDELETE(value:HTTPTBasicEvent);
    Procedure SetOnCommandLINK(value:HTTPTBasicEvent);
    Procedure SetOnCommandUNLINK(value:HTTPTBasicEvent);
    Procedure SetOnCommandCHECKIN(value:HTTPTBasicEvent);
    Procedure SetOnCommandTEXTSEARCH(value:HTTPTBasicEvent);
    Procedure SetOnCommandSPACEJUMP(value:HTTPTBasicEvent);
    Procedure SetOnCommandSEARCH(value:HTTPTBasicEvent);
    Procedure SetOnCommandOPTIONS(value:HTTPTBasicEvent);
    Procedure SetOnCommandTRACE(value:HTTPTBasicEvent);
    Procedure SetOnCommandCONNECT(value:HTTPTBasicEvent);
    Procedure SetOnCommandPATCH(value:HTTPTBasicEvent);
  public
    constructor Create(AOwner:TComponent); {$IFNDEF OBJECTS_ONLY} override; {$ENDIF}
    destructor Destroy; override;
    procedure ProcessSession(ClientThread:TDXClientThread);
    Procedure AddBasicEvent(Command:String;EventProc:HTTPTBasicEvent);
    Function HeaderText(StatusCode:Integer):String; //2.0.12
    procedure Start; override;
    procedure Stop; override;
  published
    property Timeout:Cardinal read fiTimeout
                           write fiTimeout;
    property SupportKeepAlive:Boolean read fSupportKeepAlive
                                      write fSupportKeepAlive;
    property OnCommandGET: HTTPTBasicEvent read fOnCommandGET
                                           write SetOnCommandGET;
    property OnCommandPOST: HTTPTBasicEvent read fOnCommandPOST
                                            write SetOnCommandPOST;
    property OnCommandHEAD: HTTPTBasicEvent read fOnCommandHEAD
                                            write SetOnCommandHEAD;
    property OnCommandCHECKOUT: HTTPTBasicEvent read fOnCommandCHECKOUT
                                                write SetOnCommandCHECKOUT;
    property OnCommandSHOWMETHOD: HTTPTBasicEvent read fOnCommandSHOWMETHOD
                                                  write SetOnCommandSHOWMETHOD;
    property OnCommandPUT: HTTPTBasicEvent read fOnCommandPUT
                                           write SetOnCommandPUT;
    property OnCommandDELETE: HTTPTBasicEvent read fOnCommandDELETE
                                              write SetOnCommandDELETE;
    property OnCommandLINK: HTTPTBasicEvent read fOnCommandLINK
                                            write SetOnCommandLINK;
    property OnCommandUNLINK: HTTPTBasicEvent read fOnCommandUNLINK
                                              write SetOnCommandUNLINK;
    property OnCommandCHECKIN: HTTPTBasicEvent read fOnCommandCHECKIN
                                               write SetOnCommandCHECKIN;
    property OnCommandTEXTSEARCH: HTTPTBasicEvent read fOnCommandTEXTSEARCH
                                                  write SetOnCommandTEXTSEARCH;
    property OnCommandSPACEJUMP: HTTPTBasicEvent read fOnCommandSPACEJUMP
                                                 write SetOnCommandSPACEJUMP;
    property OnCommandSEARCH: HTTPTBasicEvent read fOnCommandSEARCH
                                              write SetOnCommandSEARCH;
    property OnCommandOPTIONS: HTTPTBasicEvent read fOnCommandOPTIONS
                                               write SetOnCommandOPTIONS;
    property OnCommandTRACE: HTTPTBasicEvent read fOnCommandTRACE
                                             write SetOnCommandTRACE;
    property OnCommandCONNECT: HTTPTBasicEvent read fOnCommandCONNECT
                                             write SetOnCommandCONNECT;
    property OnCommandPATCH: HTTPTBasicEvent read fOnCommandPATCH
                                             write SetOnCommandPATCH;
    property OnCommandOther: HTTPTOtherEvent read fOnCommandOther
                                             write fOnCommandOther;
    property ISAPIServer: TDXISAPI read fDXISAPI
                                   write fDXISAPI;                                
  end;

implementation

Uses
   uRODXSock,
   SysUtils,
   uRODXString;

Type
  PHTTPBasicEvent=^THTTPBasicEvent;
  THTTPBasicEvent=record
     Tag:Integer;
     Command:String;
     EventProcedure:HTTPTBasicEvent;
  End;

///////////////////////////////////////////////////////////////////////////////
//CREATE:
//       Define the Default Port number to Listen On.
///////////////////////////////////////////////////////////////////////////////
constructor TDXHTTPServerCore.Create(AOwner:TComponent);
begin
   inherited Create(AOwner);
   ServerPort:=80;
   fEventArray:=TList.Create;
   fiTimeout:=30000;
end;

///////////////////////////////////////////////////////////////////////////////
//DESTROY:
//        Destory this object.
///////////////////////////////////////////////////////////////////////////////
destructor TDXHTTPServerCore.Destroy;
Var
   PBasicEvent:PHTTPBasicEvent;

begin
   If Assigned(fEventArray) then Begin
      While fEventArray.Count>0 do Begin
         Case PHTTPBasicEvent(fEventArray[0]).Tag of
            1:Begin
              PBasicEvent:=fEventArray[0];
              Dispose(PBasicEvent);
            End;
         End;
         fEventArray.Delete(0);
      End;
      fEventArray.Free;
      fEventArray:=Nil;
   End;
   inherited Destroy;
end;

///////////////////////////////////////////////////////////////////////////////
//ADDBASICEVENT:
//              Allows you to dynamically assign a new command to the internal
//              parser. This allows the servercore to support the 'pre-defined'
//              OnCommand* events, plus you can add other commands dynamically
//              at run-time in your application without requiring a source code
//              modification to our components!
//
//              To make support easier for us, we ask that you use the Add*Event
//              procedures to expand our code, reducing code changes when an
//              upgrade is released!
//
//              See documentation for complete information on how this works.
//
//              Example Usage: AddBasicEvent('CDROM',MySpecialEvent);
///////////////////////////////////////////////////////////////////////////////
Procedure TDXHTTPServerCore.AddBasicEvent(Command:String;EventProc:HTTPTBasicEvent);
Var
   PBasicEvent:PHTTPBasicEvent;
   Loop:Integer;

Begin
   Command:=Uppercase(Command);
   Loop:=0;
   While Loop<fEventArray.Count do Begin
      If PHTTPBasicEvent(fEventArray[Loop]).Command=Command then Begin
         PHTTPBasicEvent(fEventArray[Loop]).EventProcedure:=EventProc;
         Exit;
      End
      Else Inc(Loop);
   End;
   New(PBasicEvent);
   PBasicEvent.Tag:=1;      // Denotes Event in fEventArray is a TBasicEvent!
   PBasicEvent.Command:=Command;
   PBasicEvent.EventProcedure:=EventProc;
   fEventArray.Add(PBasicEvent);
End;

Procedure TDXHTTPServerCore.SetOnCommandGET(value:HTTPTBasicEvent);
Begin
   fOnCommandGET:=Value;
   AddBasicEvent('GET',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandPOST(value:HTTPTBasicEvent);
Begin
   fOnCommandPOST:=Value;
   AddBasicEvent('POST',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandHEAD(value:HTTPTBasicEvent);
Begin
   fOnCommandHEAD:=Value;
   AddBasicEvent('HEAD',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandCHECKOUT(value:HTTPTBasicEvent);
Begin
   fOnCommandCHECKOUT:=Value;
   AddBasicEvent('CHECKOUT',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandSHOWMETHOD(value:HTTPTBasicEvent);
Begin
   fOnCommandSHOWMETHOD:=Value;
   AddBasicEvent('SHOWMETHOD',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandPUT(value:HTTPTBasicEvent);
Begin
   fOnCommandPUT:=Value;
   AddBasicEvent('PUT',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandDELETE(value:HTTPTBasicEvent);
Begin
   fOnCommandDELETE:=Value;
   AddBasicEvent('DELETE',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandLINK(value:HTTPTBasicEvent);
Begin
   fOnCommandLINK:=Value;
   AddBasicEvent('LINK',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandUNLINK(value:HTTPTBasicEvent);
Begin
   fOnCommandUNLINK:=Value;
   AddBasicEvent('UNLINK',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandCHECKIN(value:HTTPTBasicEvent);
Begin
   fOnCommandCHECKIN:=Value;
   AddBasicEvent('CHECKIN',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandTEXTSEARCH(value:HTTPTBasicEvent);
Begin
   fOnCommandTEXTSEARCH:=Value;
   AddBasicEvent('TEXTSEARCH',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandSPACEJUMP(value:HTTPTBasicEvent);
Begin
   fOnCommandSPACEJUMP:=Value;
   AddBasicEvent('SPACEJUMP',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandSEARCH(value:HTTPTBasicEvent);
Begin
   fOnCommandSEARCH:=Value;
   AddBasicEvent('SEARCH',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandOPTIONS(value:HTTPTBasicEvent);
Begin
   fOnCommandOPTIONS:=Value;
   AddBasicEvent('OPTIONS',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandTRACE(value:HTTPTBasicEvent);
Begin
   fOnCommandTRACE:=Value;
   AddBasicEvent('TRACE',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandCONNECT(value:HTTPTBasicEvent);
Begin
   fOnCommandCONNECT:=Value;
   AddBasicEvent('CONNECT',Value);
End;

Procedure TDXHTTPServerCore.SetOnCommandPATCH(value:HTTPTBasicEvent);
Begin
   fOnCommandPATCH:=Value;
   AddBasicEvent('PATCH',Value);
End;

///////////////////////////////////////////////////////////////////////////////
//PROCESSSESSION:
//               If you want this CORE to process the parsing, you should call
//               this from your "OnNewConnect" implementation. This should be
//               right after your call to SayHello (optional).
///////////////////////////////////////////////////////////////////////////////
procedure TDXHTTPServerCore.ProcessSession(ClientThread:TDXClientThread);
var
  S,WS:string;
  HeaderInfo:PHeaderInfo;
  Loop:Integer;
  WasHandled:Boolean;
  OutData:Pointer;
  WantKeepAlive:Boolean;
  //SmartTimeout:Integer; // 3.0a
  ManualTimeout:Cardinal;

  procedure NotHandled;
  begin
     ClientThread.Socket.Write(
        'HTTP/1.0 '+HeaderText(400)+#13#10+#13#10+
        '<html><head><title>Error</title>'+
        '<body><h1>Error</h1><hr><h3>Unsupported Method <B>"'+
        HeaderInfo.Method+'"</B></h3></body></head></html>');
  end;

  Procedure BuildHeader(ClientHeader:String);
  var
     line,token1,token2:String;
     Error:Integer;
     Ch:Char;

   Procedure AddToUnknown; // 7-27
   Begin
      If HeaderInfo^.Unknown='' then HeaderInfo^.Unknown:=token1+': '+line
      Else HeaderInfo^.Unknown:=HeaderInfo^.Unknown+#13#10+token1+': '+line;
   End;

  Begin
     line:=ClientHeader;
     With HeaderInfo^ do Begin
        If RAW='' then Begin
           RAW:=Line;
           Method:=Uppercase(FetchByChar(line,#32,False));
           URI:=EscapeDecode(FetchByChar(line,#32,False));
           If CharPos('?',URI)>0 then Begin
              QueryString:=Copy(URI,CharPos('?',URI)+1,Length(URI));
              Delete(URI,CharPos('?',URI),Length(URI));
           End;
           Protocol:=FetchByChar(line,#32,False);
        End
        Else Begin
           ALL_RAW:=ALL_RAW+Line+#13#10;
           token1:=Uppercase(FetchByChar(line,#32,False));
           token2:=StringReplace(token1,'-','_',[rfReplaceAll]);
           ALL_HTTP:=ALL_HTTP+'HTTP_'+Token2+Line+#13#10;
           Ch:=Token1[1];
           Case Ch of // 2.0
              'A':if token1='ACCEPT:' then Accept:=Accept+Line
                else if token1='ACCEPT-CHARSET:' then AcceptCharset:=Line
                else if token1='ACCEPT-ENCODING:' then AcceptEncoding:=Line
                else if token1='ACCEPT-LANGUAGE:' then AcceptLanguage:=Line
                else if token1='ALLOW:' then Allow:=Line
                else if token1='AUTHORIZATION:' then begin
                   AuthType:=FetchByChar(line,#32,False); // Usually is "Basic"
                   token2:=FetchByChar(line,#32,False);
                   token2:=Base64ToString(token2); // Decode the "Basic" encoded string
                   AuthName:=FetchByChar(token2,':',False);
                   AuthPass:=FetchByChar(token2,';',False); // should actually be leftovers
                end
                Else AddToUnknown;
              'C':if token1='CACHE-CONTROL:' then CacheControl:=Line
                else if token1='CONNECTION:' then Connection:=Line
                else if token1='CACHE-INFO:' then CacheInfo:=Line
                else if token1='CONTENT-LENGTH:' then Begin
                   Val(FetchByChar(line,#32,False),ContentLength,Error);
                end
                else if token1='CONTENT-TYPE:' then ContentType:=Line
                else if token1='COOKIE:' then Cookie:=Line
                Else AddToUnknown;
              'D':if token1='DATE:' then Date:=Line
                Else AddToUnknown;
              'F':if token1='FROM:' then From:=Line
                Else if token1='FORWARDED:' then Forwarded:=Line
                Else if token1='FORWARDED-FOR:' then ForwardedFor:=Line
                Else AddToUnknown;
              'H':if token1='HOST:' then Host:=Line
                Else AddToUnknown;
              'I':if token1='IF-MODIFIED-SINCE:' then IfModSince:=Line
                else if token1='IF-MATCH:' then IfMatch:=Line
                else if token1='IF-NONE-MATCH:' then IfNoneMatch:=Line
                else if token1='IF-RANGE:' then IfRange:=Line
                else if token1='IF-UNMODIFIED-SINCE:' then IfUnModSince:=Line
                Else AddToUnknown;
              'K':if token1='KEEP-ALIVE:' then KeepAlive:=Line
                Else AddToUnknown;
              'M':if token1='MAX-FORWARDS:' then MaxForwards:=Line
                Else AddToUnknown;
              'P':if token1='PUBLIC:' then PublicCache:=Line
                else if token1='PRAGMA:' then Pragma:=Line
                else if token1='PROXY-CONNECTION:' then ProxyConnection:=Line
                else if token1='PROXY-AUTHORIZATION:' then ProxyAuthorization:=Line
                Else AddToUnknown;
              'R':if token1='REFERER:' then Referer:=Line
                else if token1='RANGE:' then Range:=Line
                Else AddToUnknown;
              'T':if token1='TRANSFER-ENCODING:' then TransferEncoding:=Line
                Else AddToUnknown;
              'U':if token1='UPGRADE:' then Upgrade:=Line
                else if token1='USER-AGENT:' then
                UserAgent:=Trim(Line)
                Else AddToUnknown;
              'V':if token1='VIA:' then Via:=Line
                Else AddToUnknown;
              'W':if token1='WEFERER:' then Weferer:=Line
                Else if token1='WSER-AGENT:' then WserAgent:=Line
                Else AddToUnknown;
              Else AddToUnknown;
           End;
        End;
     end;
  End;

  Procedure ManuallyGetHost;
  Begin
     With HeaderInfo^ do Begin
        If Quickpos('//',URI)>0 then Begin
           Host:=Copy(URI,1,QuickPos('//',URI)+1);
           Delete(URI,1,Length(Host));
        End;
        If URI<>'/' then Begin
           Host:=Host+Copy(URI,1,CharPos('/',URI));
           Delete(URI,1,CharPos('/',URI));
        End;
     End;
  End;

begin
   New(HeaderInfo); // 2.4 moved before repeat
   repeat
{      FillChar(HeaderInfo^,Sizeof(HeaderInfo^),#0);
      HeaderInfo^.ClientAddr:=ClientThread.Socket.PeerIPAddress;
      HeaderInfo^.ClientHost:=HeaderInfo^.ClientAddr;
      HeaderInfo^.ContentLength:=0;}
// this is faster than above code
   With HeaderInfo^ do Begin
      ClientAddr:=ClientThread.Socket.PeerIPAddress;
      ClientHost:=HeaderInfo^.ClientAddr;
      ContentLength:=0;
      Raw:='';
      Protocol:='';
      Method:='';
      URI:='';
      PhysPath:='';
      Allow:='';
      AuthType:='';
      AuthName:='';
      AuthPass:='';
      Date:='';
      Pragma:='';
      CacheControl:='';
      Connection:='';
      TransferEncoding:='';
      Upgrade:='';
      Via:='';
      Host:='';
      From:='';
      IfMatch:='';
      IfNoneMatch:='';
      IfRange:='';
      IfRange:='';
      IfUnModSince:='';
      MaxForwards:='';
      ProxyAuthorization:='';
      KeepAlive:='';
      PublicCache:='';
      Range:='';
      Referer:='';
      UserAgent:='';
      ContentType:='';
      Accept:='';
      AcceptCharset:='';
      AcceptEncoding:='';
      AcceptLanguage:='';
      Cookie:='';
      ClientName:='';
      QueryString:='';
      HostRootPath:='';
      Weferer:='';
      WserAgent:='';
      CacheInfo:='';
      ProxyConnection:='';
      Forwarded:='';
      ForwardedFor:='';
      All_HTTP:='';
      All_RAW:='';
      PostData:='';
      Unknown:='';
   End;
   with ClientThread.Socket do begin
{$IFDEF DXSOCK2}
      S:='*';
      SmartTimeout:=fiTimeout;
      While (S<>'') do Begin
         S:=Readln(SmartTimeout);
         SmartTimeout:=500;
         If LastReadTimeout or Not Connected then Begin
{$IFDEF CODE_TRACER}
            If Assigned(ClientThread.DXCodeTracer) then Begin
               ClientThread.DXCodeTracer.SendMessage(dxctDebug,'TDXServerCore.ProcessSession - Header timeout - aborting.');
            End;
{$ENDIF}
            Dispose(Headerinfo);
            Exit;
         End;
         If S<>'' then Begin
            If Assigned(OnFilter) then Begin
               Loop:=FilterRead(@S[1],OutData,Length(S),ClientThread);
               SetLength(S,Loop);
               If Assigned(Outdata) then Begin
                  Move(TDXBSArray(OutData^),S[1],Loop);
                  OnFilter(ddFreePointer,Nil,OutData,Loop,Loop,WasHandled,ClientThread);
               End;
            End;
            //SF_NOTIFY_READ_RAW_DATA
            if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
               fDXISAPI.ServerRawRead(S,Length(S));
            BuildHeader(S);
         End
         Else If (S='') and (HeaderInfo^.Method='') then S:='*'; //2.3 Keep-Alive
      End;
{$ELSE}
//      ShowMessageWindow('','Starting Read!');
      If Not fSupportKeepAlive then Begin
         S:='';
         While (QuickPos(#13#10#13#10,S)=0) and
            (Length(S)<8192) do Begin
            S:=S+ReadStr(-1);
            if ((LastCommandStatus<>0) and (LastCommandStatus<>WSAETIMEDOUT))
               Or (Not Connected)
               then Begin
{$IFDEF CODE_TRACER}
               If Assigned(ClientThread.DXCodeTracer) then Begin
                  ClientThread.DXCodeTracer.SendMessage(dxctDebug,'TDXServerCore.ProcessSession - Header timeout - aborting.');
               End;
{$ENDIF}
               Dispose(Headerinfo);
               Exit;
            End;
         End;
      End
      Else Begin
         ManualTimeout:=TimeCounter+fiTimeout;
         S:='';
         While (QuickPos(#13#10#13#10,S)=0) and
            (Length(S)<8192) and
            (ManualTimeout>TimeCounter) do Begin
            S:=S+ReadStr(-1);
            if ((LastCommandStatus<>0) and (LastCommandStatus<>WSAETIMEDOUT))
               Or (Not Connected)
               then Begin
{$IFDEF CODE_TRACER}
               If Assigned(ClientThread.DXCodeTracer) then Begin
                  ClientThread.DXCodeTracer.SendMessage(dxctDebug,'TDXServerCore.ProcessSession - Header timeout - aborting.');
               End;
{$ENDIF}
               Dispose(Headerinfo);
               Exit;
            End;
         End;
      End;
      If LastReadTimeout or Not Connected then Begin
{$IFDEF CODE_TRACER}
         If Assigned(ClientThread.DXCodeTracer) then Begin
            ClientThread.DXCodeTracer.SendMessage(dxctDebug,'TDXServerCore.ProcessSession - Header timeout - aborting.');
         End;
{$ENDIF}
         Dispose(Headerinfo);
         Exit;
      End;
      While Length(S)>0 do Begin
         Ws:=Copy(S,1,QuickPos(#13#10,S)-1);
         If Assigned(OnFilter) then Begin
            Loop:=FilterRead(@WS[1],OutData,Length(WS),ClientThread);
            SetLength(WS,Loop);
            If Assigned(Outdata) then Begin
               Move(TDXBSArray(OutData^),WS[1],Loop);
               OnFilter(ddFreePointer,Nil,OutData,Loop,Loop,WasHandled,ClientThread);
            End;
         End;
         //SF_NOTIFY_READ_RAW_DATA
         if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
            fDXISAPI.ServerRawRead(WS,Length(WS));
         BuildHeader(Ws);
         Delete(S,1,Length(Ws)+2);
         If Copy(S,1,2)=#13#10 then Begin //end of header
            HeaderInfo^.PostData:=Copy(S,3,Length(S));
            S:='';
         End;
      End;
{$ENDIF}
//      ShowMessageWindow('','Finished Read!'+#13#10+HeaderInfo^.All_RAW);
      If HeaderInfo^.Host='' then ManuallyGetHost;
      Delete(HeaderInfo^.ALL_HTTP,Length(HeaderInfo^.ALL_HTTP)-1,2);
      HeaderInfo^.ALL_HTTP:=HeaderInfo^.ALL_HTTP+#0;
      //SF_NOTIFY_PREPROC_HEADERS
      if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
         fDXISAPI.ServerPreprocHeaderEvent(HeaderInfo);
      Loop:=0;
      WasHandled:=False;
      WantKeepAlive:=fSupportKeepAlive;
      While (Loop<fEventArray.Count) and (Not WasHandled) do Begin
         If PHTTPBasicEvent(fEventArray[Loop]).Command=HeaderInfo^.Method then Begin
            Case PHTTPBasicEvent(fEventArray[Loop]).Tag of
               1:if Assigned(PHTTPBasicEvent(fEventArray[Loop]).EventProcedure) then
                    HTTPTBasicEvent(PHTTPBasicEvent(fEventArray[Loop]).EventProcedure)(ClientThread,HeaderInfo,WantKeepAlive);
            End;
            WasHandled:=True;
         End
         Else Inc(Loop);
      End;
      If Not WasHandled then Begin
         if assigned(OnCommandOther) then
            OnCommandOther(ClientThread,HeaderInfo,WasHandled);
         if not WasHandled then NotHandled;
         WantKeepAlive:=False;
      end;
   end;
   //SF_NOTIFY_END_OF_REQUEST
   if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
      fDXISAPI.ServerEndOfRequest;
   If Not fSupportKeepAlive then WantKeepAlive:=False;
   If WantKeepAlive then ProcessWindowsMessageQueue; // 9/11/2002
{$IFDEF CODE_TRACER}
   If Assigned(ClientThread.DXCodeTracer) then Begin
      If WantKeepAlive then
         ClientThread.DXCodeTracer.SendMessage(dxctDebug,'TDXServerCore.ProcessSession - looping for keepalive.');
   End;
{$ENDIF}
   Until (Not WantKeepAlive) or (Not ClientThread.Socket.Connected);
   Dispose(HeaderInfo); //2.4
   //SF_NOTIFY_END_OF_NET_SESSION
   if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
     fDXISAPI.ServerEndSession;
end;

//2.0.12
Function TDXHTTPServerCore.HeaderText(StatusCode:Integer):String;
Var
   Loop:Integer;

Begin
   If StatusCode>404 then Begin // divide and conquor
      Loop:=MaxStatusCodes;
      While StatusCodes[Loop].Code>StatusCode do Dec(Loop);
   End
   Else begin
      Loop:=0;
      While StatusCodes[Loop].Code<StatusCode do Inc(Loop);
   End;
   If StatusCodes[Loop].Code=StatusCode then
      Result:=IntToStr(StatusCode)+#32+StatusCodes[Loop].Msg
   Else
      Result:='500 Internal Server Error';
End;

Procedure TDXHTTPServerCore.Start;
Begin
   if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then
     fDXISAPI.ServerStartEvent;
   inherited Start;
End;

Procedure TDXHTTPServerCore.Stop;
Begin
   if assigned(fDXISAPI) and (fDXISAPI.FilterCount>0) then Begin
     fDXISAPI.ServerStopEvent;
   End;
   inherited Stop;
End;

end.

