{ Summary: Contains the class TROIndyHTTPChannel }
unit uROIndyHTTPChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Indy Components
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  uRORes, Classes, uROProxy, uROClientIntf, uROIndyTCPChannel, IdTCPCLient, IdHTTP;

type {--------------- TROIndyHTTPChannel ---------------}

     { Summary: Channel for remote HTTP communication based on the Indy TIdHTTP component
       Description:
        It dispaches messages to the server by using the Indy TIdHTTP component.
        The internal Indy client is accessible using the Indy client property which is also visible in
        the Object inspector in Delphi 6 and up. }

     TROIndyHTTPChannel = class(TROIndyTCPChannel, IROTransport, IROTCPTransport, IROHTTPTransport)
     private
       fTargetURL: string;

       function GetIndyClient: TIdHTTP;

     protected
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the server using the internal HTTP Indy client's post method. }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       { Summary: Creates the internal Indy client
         Description:
           Creates the internal Indy client. It returns a TIdHTTP client. }
       function CreateIndyClient : TIdTCPClient; override;

       {--------------- IROTCPTransport ---------------}
       function GetClientAddress : string;

       {--------------- IROHTTPTransport ---------------}
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;
       function GetContentType : string;
       procedure SetContentType(const aValue : string);
       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);

       procedure SetTargetURL(const Value: string);
       function GetTargetURL: string;

       function GetPathInfo : string;
       function GetLocation : string;

     public
     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property IndyClient : TIdHTTP read GetIndyClient;

       { Summary: Specifies the target URI for the message dispatch
         Description:
           Specifies the target URI for the message dispatch.

           If you importeed a SOAP web service using the Service Builder, this property will be automatically
           assigned a value for you using the URI specified in the WSDL document.

           Refer to the property TROServer.Dispatchers for more info about setting your services URIs. }
       property TargetURL : string read GetTargetURL write SetTargetURL;
     end;

implementation

uses SysUtils, IdException, uROHTTPTools;

{ TROIndyHTTPChannel }
function TROIndyHTTPChannel.CreateIndyClient: TIdTCPClient;
begin
  result := TIdHTTP.Create(Self);
  TIdHTTP(result).Request.UserAgent := str_ProductName;
  result.Port := 8099;
  result.Host := '127.0.0.1';
  TargetURL := '';
end;

function TROIndyHTTPChannel.GetTargetURL: string;
begin
  result := fTargetURL;
end;

procedure TROIndyHTTPChannel.SetTargetURL(const Value: string);
begin
  fTargetURL := Trim(Value)
end;

function TROIndyHTTPChannel.GetIndyClient: TIdHTTP;
begin
  result := TIdHTTP(inherited IndyClient);
end;

procedure TROIndyHTTPChannel.IntDispatch(aRequest, aResponse : TStream);
begin
  if (TargetURL='')
    then RaiseError(err_UnspecifiedTargetURL, []);

  try
    IndyClient.Post(TargetURL, aRequest, aResponse);
  except
    //ToDo: what about 500 error nessages that don't have a SOAP error envelope?
    on E:EIdProtocolReplyError do begin
      if E.ReplyErrorCode <> 500 then raise;
      // Indy raises exceptions when HTTP headers contain HTTP/1.0 500 Internal Server Error
      // We want to continue processing instead
    end
    else raise;
  end;
end;

function TROIndyHTTPChannel.GetHeaders(const aName: string): string;
begin
{$IFDEF RemObjects_INDY8}
  result := GetHeaderValue(IndyClient.Request.ExtraHeaders, aName);
{$ENDIF}
{$IFDEF RemObjects_INDY9}
  result := GetHeaderValue(IndyClient.Request.RawHeaders, aName);
{$ENDIF}
end;

procedure TROIndyHTTPChannel.SetHeaders(const aName, aValue: string);
begin
{$IFDEF RemObjects_INDY8}
  SetHeaderValue(IndyClient.Request.ExtraHeaders, aName, aValue);
{$ENDIF}
{$IFDEF RemObjects_INDY9}
  SetHeaderValue(IndyClient.Request.RawHeaders, aName, aValue);
{$ENDIF}
end;

function TROIndyHTTPChannel.GetContentType: string;
begin
  result := IndyClient.Request.ContentType;
end;

procedure TROIndyHTTPChannel.SetContentType(const aValue: string);
begin
  IndyClient.Request.ContentType := aValue;
end;

function TROIndyHTTPChannel.GetUserAgent: string;
begin
  result := IndyClient.Request.UserAgent;
end;

procedure TROIndyHTTPChannel.SetUserAgent(const aValue: string);
begin
  IndyClient.Request.UserAgent := aValue;
end;

function TROIndyHTTPChannel.GetClientAddress: string;
begin
  result := '';
end;

function TROIndyHTTPChannel.GetPathInfo: string;
begin
  result := '';
end;

function TROIndyHTTPChannel.GetLocation: string;
begin
  result := ''
end;

end.
