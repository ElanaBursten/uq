unit uRODXServerCore;

///////////////////////////////////////////////////////////////////////////////
//    Component: TDXServerCore
//       Author: G.E. Ozz Nixon Jr. (onixon@dxsock.com)
// ========================================================================
// Source Owner: DX, Inc. 1995-2002
//    Copyright: All code is the property of DX, Inc. Licensed for
//               resell by Brain Patchwork DX (tm) and part of the
//               DX (r) product lines, which are (c) 1999-2002
//               DX, Inc. Source may not be distributed without
//               written permission from both Brain Patchwork DX,
//               and DX, Inc.
//      License: (Reminder), None of this code can be added to other
//               developer products without permission. This includes
//               but not limited to DCU's, DCP's, DLL's, OCX's, or
//               any other form of merging our technologies. All of
//               your products released to a public consumer be it
//               shareware, freeware, commercial, etc. must contain a
//               license notification somewhere visible in the
//               application.
//               Example is Internet Explorer - Help->About screen
//               shows the licensed code contained in the application.
// Code Version: (3rd Generation Code)
// ========================================================================
//  Description: Ancestor to all our Protocol Implementations.
// ========================================================================
// Parent Thread Object and "Defacto" Server Object.
// The Server Object (DXServerCore) is the heart of all protocols. It is used
// to initiate winsock listening, and start the Parent Thread
// (DXServerCoreThread). The Parent Thread constantly checks if the listener
// has received a connection request, accepts the connection and spawns a
// (DXServerThread) Session. And continues checking for more connections.
///////////////////////////////////////////////////////////////////////////////

interface

{$I uRODXSock.def}

uses
{$IFDEF CODE_TRACER}
  DXCodeTracer,
{$ENDIF}
  uRODXSessionTracker,
  uRODXSock,
  uRODXString,
  Classes;

type
  TWhichProtocol =
     (wpUDPOnly, wpTCPOnly);
  TServerType =
     (stNonBlocking, stThreadBlocking); // so you can port Borland code to DXSOCK!

//*****************************************************************************
// This thread is the spawned "Session" thread. Every connection detected by
// the "Listener" thread is created as a TDXServerThread, and then that new
// thread takes over communications with the new session.
//*****************************************************************************
  TDXClientThread = class;
  TDX_NewConnect = procedure(ClientThread: TDXClientThread) of object;
  TDX_DestroySessionData = procedure(ClientThread: TDXClientThread; SessionData: Pointer) of object;
  TDXClientThread = class(TThread)
  private
{$IFDEF CODE_TRACER}
    CodeTracer:TDXCodeTracer;
{$ENDIF}
    Client: TDXSock;
    feNewConnect: TDX_NewConnect;
    feDestroySessionData: TDX_DestroySessionData;
    fBlockSizeFlags: TDXBlockSizeFlags;
    ListenerThreadObject:TThread;
  protected
    procedure Execute; override;
    function GetSessionID:Integer; // making move from INDY easier!
  public
    fpSessionData: Pointer;
    constructor Create(CreateSuspended: Boolean);
    destructor Destroy; override;
    procedure SetSocketLater(Socket: TDXSock);
  published
    property SessionID:Integer read GetSessionID;
    property Socket: TDXSock read Client
      write Client;
    property OnNewConnect: TDX_Newconnect read feNewConnect
      write feNewConnect;
    property OnDestroySessionData: TDX_DestroySessionData read feDestroySessionData
      write feDestroySessionData;
{$IFDEF CODE_TRACER}
    property DXCodeTracer:TDXCodeTracer read CodeTracer
                                        write CodeTracer;
{$ENDIF}
     Property Terminated;
  end;

//*****************************************************************************
// All protocols use this thread to listen for incoming connections. This is
// the "Listener" thread.
//*****************************************************************************
type
  TDX_ListenerFailed = procedure(ErrorCode: Integer) of object;
  TDXServerCoreThread = class;
  TDX_MaxConnects = procedure(ServerCoreThread: TDXServerCoreThread) of object;
  TDX_Idle = procedure(ServerCoreThread: TDXServerCoreThread) of object;
  TDX_Sleep = procedure(ServerCoreThread: TDXServerCoreThread) of object;
  TDX_WakeUp = procedure(ServerCoreThread: TDXServerCoreThread) of object;
// 3.0c
  TDX_UDPData = procedure(Data:Pointer;PeerIP:String;PeerPort,DataLen:Integer) of object;

  TDXServerCoreThread = class(TThread)
  private
{$IFDEF CODE_TRACER}
    CodeTracer:TDXCodeTracer;
{$ENDIF}
    fbSuspend: Boolean;
    fbBufferCreates: Boolean;
    ListenerSocket: TDXSock;
    fSessionTracker:TDXSessionTracker;
    fsBindTo: string;
    fiServerPort: Integer;
    fiMaxConn: Integer;
    fbAnnouncedIdle: Boolean;
    feNewConnect: TDX_NewConnect;
    feMaxConnects: TDX_MaxConnects;
    feListenerFailed: TDX_ListenerFailed;
    feIdle: TDX_Idle;
    feSleep: TDX_Sleep;
    feWakeUp: TDX_WakeUp;
    feUDPData: TDX_UDPData;
{$IFNDEF LINUX}
    fstPriority: TThreadPriority;
{$ENDIF}
    fThreadPool: TList;
    fWhichprotocol: TWhichProtocol;
    FActiveConnections: Integer;
    fBlockSizeFlags: TDXBlockSizeFlags;
  protected
    MyCriticalSection:TDXCritical;
    procedure Execute; override;
    procedure SetBufferCreates(value: Boolean);
    procedure SetSuspend(value: Boolean);
    function GetSocket: TDXSock;
  public
    constructor Create(CreateSuspended: Boolean);
    destructor Destroy; override;
    function ActiveNumberOfConnections: Integer;
  published
    property MainSocket: TDXSock read GetSocket;
{$IFNDEF LINUX}
    property SpawnedThreadPriority: TThreadPriority read fstPriority
      write fstPriority;
{$ENDIF}
    property BufferCreates: Boolean read fbBufferCreates
      write SetBufferCreates;
    property SuspendListener: Boolean read fbSuspend
      write SetSuspend;
    property BindTo: string read fsBindTo
      write fsBindTo;
    property ServerPort: Integer read fiServerPort
      write fiServerPort;
    property ThreadCacheSize: Integer read fiMaxConn
      write fiMaxConn;
    property OnNewConnect: TDX_NewConnect read feNewConnect
      write feNewConnect;
    property OnMaxConnects: TDX_MaxConnects read feMaxConnects
      write feMaxConnects;
    property OnGoingIdle: TDX_Idle read feIdle
      write feIdle;
    property OnAsleep: TDX_Sleep read feSleep
      write feSleep;
    property OnWakeUp: TDX_WakeUp read feWakeUp
      write feWakeUp;
    property OnListenerFailed: TDX_ListenerFailed read feListenerFailed
      write feListenerFailed;
    property OnUDPDataNoPool: TDX_UDPData read feUDPData
      write feUDPData;
    property ProtocolToBind: TWhichProtocol read fWhichprotocol
      write fWhichprotocol;
{$IFDEF CODE_TRACER}
    property DXCodeTracer:TDXCodeTracer read CodeTracer
                                        write CodeTracer;
{$ENDIF}
  end;

//*****************************************************************************
// All protocols are descendants of this object/component. When making changes
// that need to flow to the actual protocol, like supporting a change to the
// spawned thread. You would make the "property" in this piece of code, and
// then when this piece of code creates the listener thread you can pass the
// information to the listener. At that point, you could pass it down to the
// protocol thread TDXServerThread.
//*****************************************************************************
type
  TDXServerCore = class(TDXComponent)
  private
{$IFDEF CODE_TRACER}
    CodeTracer:TDXCodeTracer;
{$ENDIF}
    fbSSL:Boolean;
    fbActive: Boolean;
    fbSuspend: Boolean;
    fbBufferCreates: Boolean;
    fsBindTo: string;
    fiServerPort: Integer;
    fiMaxConn: Integer;
    feNewConnect: TDX_NewConnect;
    feMaxConnects: TDX_MaxConnects;
    feListenerFailed: TDX_ListenerFailed;
    feIdle: TDX_Idle;
    feSleep: TDX_Sleep;
    feWakeUp: TDX_WakeUp;
    feUDPData: TDX_UDPData;
    ListenerThread: TDXServerCoreThread;
{$IFNDEF LINUX}
    fltPriority: TThreadPriority;
    fstPriority: TThreadPriority;
{$ENDIF}
    fWhichprotocol: TWhichProtocol;
    fBlockSizeFlags: TDXBlockSizeFlags;
    fServerType: TServerType;
    fDummy: string;
  protected
    procedure SetActive(value: boolean);
    procedure SetSuspend(value: boolean);
    function GetThreadCacheSize: Integer;
    procedure SetThreadCacheSize(value: Integer);
    function GetSocket: TDXSock;
    procedure SetfiMaxConn(Value: Integer);
  public
    constructor Create(AOwner:TComponent); {$IFNDEF OBJECTS_ONLY} override; {$ENDIF}
    destructor Destroy; override;
    function ActiveNumberOfConnections: Integer;
    procedure Start; virtual;
    procedure Stop; virtual;
    procedure Open;
    procedure Close;
    procedure Pause;
    procedure Resume;
    property Socket: TDXSock read GetSocket;
    function InternalSessionTracker:TDXSessionTracker;
//    property :TDXSessionTracker read GetSessionTracker;
  published
{$IFNDEF LINUX}
    property ListenerThreadPriority: TThreadPriority read fltPriority
      write fltPriority;
    property SpawnedThreadPriority: TThreadPriority read fstPriority
      write fstPriority;
{$ENDIF}
    property IsActive: Boolean read fbActive;
    property Suspend: Boolean read fbSuspend
      write SetSuspend;
    property UseSSL: Boolean read fbSSL
      write fbSSL;
    property UseThreadPool: Boolean read fbBufferCreates
      write fbBufferCreates;
    property BindTo: string read fsBindTo
      write fsBindTo;
    property ServerPort: Integer read fiServerPort
      write fiServerPort;
    property OnNewConnect: TDX_NewConnect read feNewConnect
      write feNewConnect;
    property OnMaxConnects: TDX_MaxConnects read feMaxConnects
      write feMaxConnects;
    property OnGoingIdle: TDX_Idle read feIdle
      write feIdle;
    property OnAsleep: TDX_Sleep read feSleep
      write feSleep;
    property OnWakeUp: TDX_WakeUp read feWakeUp
      write feWakeUp;
    property OnListenerFailed: TDX_ListenerFailed read feListenerFailed
      write feListenerFailed;
    property ProtocolToBind: TWhichProtocol read fWhichprotocol
      write fWhichprotocol;
    property SocketOutputBufferSize: TDXBlockSizeFlags read fBlockSizeFlags
      write fBlockSizeFlags;
    property ServerType: TServerType read fServerType
      write fServerType;
    property Service: string read fDummy write fDummy;
    property ThreadCacheSize: Integer read GetThreadCacheSize
      write SetThreadCacheSize;
    property OnUDPDataNoPool: TDX_UDPData read feUDPData
      write feUDPData;
{$IFDEF CODE_TRACER}
    property DXCodeTracer:TDXCodeTracer read CodeTracer
                                        write CodeTracer;
{$ENDIF}
  end;

implementation

uses
{$IFNDEF LINUX}
   Windows, // InterlockedXX thanks to EYAL!
{$ENDIF}
   SysUtils,
   uRODXSocket;

///////////////////////////////////////////////////////////////////////////////
//CREATE:
//       This section is created when the DXServerCore has accepted a valid
//       socket connection from a client.
///////////////////////////////////////////////////////////////////////////////

constructor TDXClientThread.Create(CreateSuspended: Boolean);
begin
   inherited Create(CreateSuspended);
   FreeOnTerminate := True;
   Client := nil;
   fpSessionData := nil;
end;

///////////////////////////////////////////////////////////////////////////////
//DESTROY:
//        Cleans-up this instance of the socket connection. Does not effect the
//        "Listener" thread socket.
///////////////////////////////////////////////////////////////////////////////

destructor TDXClientThread.Destroy;
begin
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'ClientThread.Terminating');
   End;
{$ENDIF}
try
   If Assigned(ListenerThreadObject) then
      If Assigned(TDXServerCoreThread(ListenerThreadObject).fSessionTracker) then
         TDXServerCoreThread(ListenerThreadObject).fSessionTracker.UnregisterSession(Self);
except
   on E: Exception do begin
//         E.Message;
   end;
end;
try
   if Assigned(fpSessionData) then
      if assigned(feDestroySessionData) then
         feDestroySessionData(Self, fpSessionData);
except
   on E: Exception do begin
//         E.Message;
   end;
end;
try
   if Assigned(Client) then begin
      Client.Free;
      Client := nil;
   end;
except
   on E: Exception do begin
//         E.Message;
   end;
end;
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'ClientThread.Terminated!');
   End;
{$ENDIF}
   inherited Destroy;
end;

function TDXClientThread.GetSessionID:Integer; // making move from INDY easier!
Begin
   Result:=ThreadID;
End;

///////////////////////////////////////////////////////////////////////////////
//EXECUTE:
//        Once this thread has "Resume"d, execute is called by TThread. This will
//        fire the OnExecute (where the server protocol processes the connection)
//        and once that is finished it fires the OnDisconnet event.
///////////////////////////////////////////////////////////////////////////////

procedure TDXClientThread.Execute;
begin
try
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXClientThread.Execute Starting '+
         IntToStr(TDXServerCoreThread(ListenerThreadObject).FActiveConnections));
   End;
{$ENDIF}
   while not Terminated do begin
      if Assigned(Client) then begin
         Client.OutputBufferSize := fBlockSizeFlags;
         if Client.ValidSocket then begin
{$IFDEF CODE_TRACER}
            If Assigned(CodeTracer) then Begin
               CodeTracer.SendMessage(dxctInfo,'TDXClientThread.Execute ONNEWCONNECT '+
                  IntToStr(TDXServerCoreThread(ListenerThreadObject).FActiveConnections));
            End;
{$ENDIF}
try
            if Assigned(feNewConnect) then feNewConnect(Self);
finally
{$IFDEF LINUX}
            TDXServerCoreThread(ListenerThreadObject).MyCriticalSection.StartingWrite;
            Dec(TDXServerCoreThread(ListenerThreadObject).FActiveConnections);
            TDXServerCoreThread(ListenerThreadObject).MyCriticalSection.FinishedWrite;
{$ELSE}
            InterlockedDecrement(TDXServerCoreThread(ListenerThreadObject).FActiveConnections);
{$ENDIF}
end;
{$IFDEF CODE_TRACER}
            If Assigned(CodeTracer) then Begin
               CodeTracer.SendMessage(dxctInfo,'TDXClientThread.Execute ONNEWCONNECT-DONE '+
                  IntToStr(TDXServerCoreThread(ListenerThreadObject).FActiveConnections));
            End;
{$ENDIF}
         end;
      end
      Else Begin
         FreeOnTerminate:=True;
         Break;
      End;
      // incase user destoryed my DXSock client object, test client.
      if Assigned(Client) then begin
try
         if Client.IsUDPMode then Client.Sock := INVALID_SOCKET
         else If Client.Sock<>INVALID_SOCKET then Client.CloseGracefully;
except
try
         Client.Free;
finally
         Client:=Nil;
end;
end;
      end;
      If FreeOnTerminate then Break;
      if not Terminated then Suspend;
      if Terminated {and Assigned(Client)} then FreeOnTerminate:=True;
   end;
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXClientThread.Execute Exiting.'+
         IntToStr(TDXServerCoreThread(ListenerThreadObject).FActiveConnections));
   End;
{$ENDIF}
finally
   Terminate;
end;
end;

///////////////////////////////////////////////////////////////////////////////
//SETSOCKETLATER:
//               Simple implementation of this "Thread" is to pass the Create the
//               recently "Accept"ed Socket during the create. But, if you are
//               producing speed by pre-creating the threads, this procedure will
//               allow you to pre-create threads, and pass the "Socket" when the
//               new accept is valid.
///////////////////////////////////////////////////////////////////////////////

procedure TDXClientThread.SetSocketLater(Socket: TDXSock);
begin
  if Assigned(Client) then begin
    Client.Free;
    Client := nil;
  end;
  Client := Socket;
  Client.PeerIPAddress := Socket.PeerIPAddress;
  Client.PeerPort := Socket.PeerPort;
end;

///////////////////////////////////////////////////////////////////////////////
// 'LISTENER' THREAD
// =================
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//CREATE:
//       This is the "Outter" Thread of a server. It does the listening loop, and
//       creates the thread that interacts with the client. Think of this as a
//       while active listen for a connection, and on connection spawn a thread
//       for the client connection (DXServerThread).
///////////////////////////////////////////////////////////////////////////////

constructor TDXServerCoreThread.Create(CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);
  FreeOnTerminate := False;
  ListenerSocket := TDXSock.Create;
  fsBindTo := '';
  fiServerPort := 0;
  fiMaxConn := 200;
  FActiveConnections := 0;
  fbAnnouncedIdle := False;
  fbBufferCreates := True;
  fThreadPool := TList.Create;
  MyCriticalSection:=TDXCritical.Create;
  fSessionTracker:=TDXSessionTracker.Create(Nil);
end;

///////////////////////////////////////////////////////////////////////////////
//DESTROY:
//        Kills the Listener Socket.
///////////////////////////////////////////////////////////////////////////////

destructor TDXServerCoreThread.Destroy;
begin
  if Assigned(fThreadPool) then begin
    MyCriticalSection.StartingWrite;
    while fThreadPool.Count > 0 do begin
      if Assigned(fThreadPool[0]) then Begin
try
        TDXClientThread(fThreadPool[0]).ListenerThreadObject:=Nil;
        fSessionTracker.UnRegisterSession(TDXClientThread(fThreadPool[0]));
Finally
try
// this will raise an exception if it is already set "FreeOnTerminate"!
        TDXClientThread(fThreadPool[0]).FreeOnTerminate:=True;
        TDXClientThread(fThreadPool[0]).Terminate;
except
end;
end;
      End;
      fThreadPool.Delete(0);
    end;
    fThreadPool.Free;
    fThreadPool := nil;
    MyCriticalSection.FinishedWrite;
  end;
  fSessionTracker.Free;
  fSessionTracker:=Nil;
try
  if Assigned(ListenerSocket) then ListenerSocket.Free;
finally
  ListenerSocket:=Nil;
end;
  MyCriticalSection.Free;
  inherited Destroy;
end;

function TDXServerCoreThread.ActiveNumberOfConnections: Integer;
begin
// 7-27   MyCriticalSection.StartingRead;
   Result := fActiveConnections;
// 7-27   MyCriticalSection.FinishedRead;
end;

///////////////////////////////////////////////////////////////////////////////
//SETBUFFERCREATES:
//                 To be safe, you should not change the Buffer Creates if you
//                 have already started listening. I try to make sure nothing gets
//                 messed up, but potentially you could crash yourself!
///////////////////////////////////////////////////////////////////////////////

procedure TDXServerCoreThread.SetBufferCreates(value: Boolean);
begin
  if fActiveConnections > 0 then Exit;
  fbBufferCreates := Value;
  if not Value then
    if fiMaxConn < 1 then fiMaxConn := 100;
end;

procedure TDXServerCoreThread.SetSuspend(value: Boolean);
begin
  fbSuspend := Value;
end;

function TDXServerCoreThread.GetSocket: TDXSock;
begin
  Result := ListenerSocket;
end;

///////////////////////////////////////////////////////////////////////////////
//EXECUTE:
//        This is the "Loop" for the server. It listens on the specified port and
//        IP address(es). As a connection comes in, it creates a DXServerThread
//        and gives it a new instance of the client socket. At that point the new
//        DXServerThread runs independant of this thread. If you wish to have a
//        pool of DXServerThreads suspended this is the section to change! When
//        a new DXServerThread is created, all of the "Events" are passed to it,
//        which means you "Server" events better be threadsafe!
//
//        Events: OnIdle is fired one the server has stopped receiving connection
//        requests. It is fired everytime the server goes idle from connection(s)
//        OnSleep is fired every two seconds after this loop has gone Idle.
///////////////////////////////////////////////////////////////////////////////

procedure TDXServerCoreThread.Execute;
var
  toggleSleep: Integer;
  DXClientThread: TDXClientThread;
  newClient: TDXSock;

  function InitializeThreading(UDP: Boolean): Boolean;
  var
    NewListen: PNewListen;
    Ws:String;

  begin
    toggleSleep := 0;
    Result := False;
    if (Length(fsBindTo) > 7) then ListenerSocket.BindTo := fsBindTo;
    New(NewListen);
    with NewListen^ do begin
      Port := fiServerPort;
      UseNAGLE := True;
      UseBlocking := True;
      UseUDP := UDP;
      WinsockQueue := 100;
      ConnectionLess := UDP; // 2.0.12
    end;
    if not ListenerSocket.Listen(NewListen) then begin
{$IFDEF CODE_TRACER}
       If Assigned(CodeTracer) then Begin
          CodeTracer.SendMessage(dxctInfo,'Listener Initialization Error: '+
             ListenerSocket.GetErrorDesc(ListenerSocket.LastCommandStatus));
       End;
{$ENDIF}
      if Assigned(feListenerFailed) then begin
        feListenerFailed(ListenerSocket.LastCommandStatus);
      end
      else begin
        Str(fiServerPort,Ws);
{$IFNDEF LINUX}
        If IsConsole then Begin
           Beep;
           Beep;
           Beep;
           Writeln('Listener on port ' +Ws+', '+
              ListenerSocket.GetErrorDesc(ListenerSocket.LastCommandStatus));
           DoSleepEx(1000);
        End
        Else
{$ENDIF}
           ShowMessageWindow('Listener on port ' +Ws,
             ListenerSocket.GetErrorDesc(ListenerSocket.LastCommandStatus));
      end;
      Dispose(NewListen);
      Exit;
    end;
    Dispose(NewListen);
    Result := True;
  end;

  procedure EventsOrSleep;
  begin
// 3.0b    DoSleepEx(1);
    if toggleSleep < 2000 then begin
      Inc(toggleSleep);
      if (toggleSleep>1000) and not fbAnnouncedIdle then begin
{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.GoingIdle');
         End;
{$ENDIF}
        fbAnnouncedIdle := True;
        if assigned(feIdle) then feIdle(Self);
        ProcessWindowsMessageQueue; // have to do incase event was GUI based.
      end;
    end
    else begin
      if fbAnnouncedIdle then begin
{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.GoingAsleep');
         End;
{$ENDIF}
        if assigned(feSleep) then feSleep(Self);
        ProcessWindowsMessageQueue; // have to do - incase event was GUI based.
        fbAnnouncedIdle := False;
      end;
      if toggleSleep < 5000 {3.0 - was 50} then Inc(toggleSleep)
      else begin
        DoSleepEx(1); // 7-27
        ProcessWindowsMessageQueue;
      end;
    end;
  end;

procedure ThreadAtATime;
begin
   DXClientThread := TDXClientThread.Create(True);
   DXClientThread.Client:=TDXSock.Create; // 3.0d
{$IFNDEF LINUX}
   DXClientThread.Priority := fstPriority;
{$ENDIF}
   DXClientThread.OnNewConnect := feNewConnect;
   DXClientThread.fBlockSizeFlags := fBlockSizeFlags;
   DXClientThread.ListenerThreadObject:=Self;
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.Execute.ThreadAtATime Ready');
   End;
{$ENDIF}
   While not Terminated do Begin
      if (FActiveConnections < fiMaxConn) then begin
         if fbSuspend then DoSleepEx(20)
         else
         if ListenerSocket.Accept(DXClientThread.Client) then begin
{$IFDEF CODE_TRACER}
            If Assigned(CodeTracer) then Begin
               DXClientThread.CodeTracer:=CodeTracer;
            End;
{$ENDIF}
            fSessionTracker.RegisterSession(DXClientThread);
            DXClientThread.Resume;
{$IFDEF LINUX}
            MyCriticalSection.StartingWrite;
            Inc(FActiveConnections);
            MyCriticalSection.FinishedWrite;
{$ELSE}
            InterlockedIncrement(FActiveConnections);
{$ENDIF}
            if (fbAnnouncedIdle) then Begin
{$IFDEF CODE_TRACER}
               If Assigned(CodeTracer) then Begin
                  CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.WakeUp');
               End;
{$ENDIF}
               If Assigned(feWakeUp) then feWakeUp(Self);
            End;
            DXClientThread := TDXClientThread.Create(True);
            DXClientThread.Client:=TDXSock.Create; // 3.0d
{$IFNDEF LINUX}
            DXClientThread.Priority := fstPriority;
{$ENDIF}
            DXClientThread.OnNewConnect := feNewConnect;
            DXClientThread.fBlockSizeFlags := fBlockSizeFlags;
            DXClientThread.ListenerThreadObject:=Self;
            fbAnnouncedIdle := False;
            toggleSleep := 0;
         end
         else EventsOrSleep;
      end
      else
      begin
         if assigned(feMaxConnects) then Begin
            feMaxConnects(Self);
            ProcessWindowsMessageQueue; // have to do incase event was GUI based.
         End
         else
         DoSleepEx(1); //3.0
      end;

   End;
   fSessionTracker.UnRegisterSession(DXClientThread);
   DXClientThread.FreeOnTerminate:=True;
   DXClientThread.Terminate;
end;

procedure ThreadPool;
var
   NextThread: Integer;
   LoopCount: Integer;
   Done:Boolean;

begin
   while fThreadPool.Count < fiMaxConn - 1 do begin
      DXClientThread := TDXClientThread.Create(True);
      DXClientThread.FreeOnTerminate:=False; // flags "Is ThreadPool".
{$IFNDEF LINUX}
      DXClientThread.Priority:=fstPriority;
{$ENDIF}
      DXClientThread.OnNewConnect:=feNewConnect;
      DXClientThread.fBlockSizeFlags:=fBlockSizeFlags;
      DXClientThread.ListenerThreadObject:=Self;
      DXClientThread.Client:=Nil;
      fThreadPool.Add(DXClientThread);
      fSessionTracker.RegisterSession(DXClientThread);
   end;
   LoopCount := fThreadPool.Count;
   NextThread := 0;
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.Execute.ThreadPool Ready');
   End;
{$ENDIF}
   While not Terminated do Begin
      if fbSuspend then DoSleepEx(20)
      else if ListenerSocket.Accept(TDXClientThread(fThreadPool[NextThread]).Client) then begin
{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            DXClientThread.CodeTracer:=CodeTracer;
         End;
{$ENDIF}
         TDXClientThread(fThreadPool[NextThread]).Resume;
{$IFDEF LINUX}
         MyCriticalSection.StartingWrite;
         Inc(FActiveConnections);
         MyCriticalSection.FinishedWrite;
{$ELSE}
         InterlockedIncrement(FActiveConnections);
{$ENDIF}
         Inc(NextThread);
         if NextThread >= LoopCount then NextThread := 0;
         Done:=False;
         while (not Terminated) and (not fbSuspend) and (not done) do begin
            If TDXClientThread(fThreadPool[NextThread]).Client=Nil then Done:=True
            Else If Not TDXClientThread(fThreadPool[NextThread]).Client.ValidSocket then Done:=True
            Else Begin
               Inc(NextThread);
               if NextThread >= LoopCount then NextThread := 0;
            End;
         end;
         toggleSleep := 0;
         if (fbAnnouncedIdle) then Begin
{$IFDEF CODE_TRACER}
            If Assigned(CodeTracer) then Begin
               CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.WakeUp');
            End;
{$ENDIF}
            If Assigned(feWakeUp) then feWakeUp(Self);
         End;
         fbAnnouncedIdle := False;
      end
      else EventsOrSleep;
   End;
end;

{$WARNINGS OFF}
procedure UDPSingleThreaded;
Var
   Data:Pointer;
   DataSize:Integer;

begin
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.Execute.UDPSingleThread Ready');
   End;
{$ENDIF}
   if Assigned(feUDPData) then Data:=SysGetMem(TDXMaxSocketBuffer);
   While not Terminated do Begin
      if (FActiveConnections < fiMaxConn) then begin
         if fbSuspend then DoSleepEx(20)
         else if ListenerSocket.Readable then begin
            if Assigned(feUDPData) then Begin
{$IFDEF VER100}
               DataSize:=ListenerSocket.BlockRead(Data,TDXMaxSocketBuffer);
{$ELSE}
               DataSize:=ListenerSocket.Read(Data,TDXMaxSocketBuffer);
{$ENDIF}
               feUDPData(Data,inet_ntoa(ListenerSocket.SockAddr.sin_addr),
                  ntohs(ListenerSocket.SockAddr.sin_port),DataSize);
            End
            Else Begin
               newClient := TDXSock.Create;
               newClient.Sock := ListenerSocket.Sock;
               newClient.IsUDPMode := True;
               DXClientThread := TDXClientThread.Create(True);
               DXClientThread.SetSocketLater(newClient);
{$IFNDEF LINUX}
               DXClientThread.Priority := fstPriority;
{$ENDIF}
               DXClientThread.OnNewConnect := feNewConnect;
               DXClientThread.fBlockSizeFlags := fBlockSizeFlags;
               DXClientThread.ListenerThreadObject:=Self;
               fSessionTracker.RegisterSession(DXClientThread);
{$IFDEF CODE_TRACER}
               If Assigned(CodeTracer) then Begin
                  DXClientThread.CodeTracer:=CodeTracer;
               End;
{$ENDIF}
               DXClientThread.Resume;
            End;
 //           DoSleepEx(1); { a must - tests show this cleans up the reads tremendously!}
            if (fbAnnouncedIdle) then Begin
{$IFDEF CODE_TRACER}
               If Assigned(CodeTracer) then Begin
                  CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.WakeUp');
               End;
{$ENDIF}
               If Assigned(feWakeUp) then feWakeUp(Self);
            End;
            toggleSleep:=0;
            fbAnnouncedIdle:=False;
         end
         else
            EventsOrSleep;
      end
      else begin
         if assigned(feMaxConnects) then Begin
            feMaxConnects(Self);
            ProcessWindowsMessageQueue; // have to do incase event was GUI based.
         end
         else
         begin
            DoSleepEx(1);
         end;
      end;
   End;
   if Assigned(feUDPData) then SysFreeMem(Data);
end;
{$WARNINGS ON}

begin
   If terminated then exit;
   if not Assigned(ListenerSocket) then begin
      ShowMessageWindow(
         'Developer Trap #2',
         'You must pass the listerner socket. You have coded around ' +
         'our fail-safe routines!');
      Exit;
   end;
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXServerCoreThread.Execute');
   End;
{$ENDIF}
try
   if not InitializeThreading({False}fWhichprotocol=wpUDPOnly) then Exit;
   if fWhichprotocol=wpUDPOnly then UDPSinglethreaded
   else begin
      case fbBufferCreates of
         False: begin
            ThreadAtATime;
         end;
         else begin
            ThreadPool;
         end;
      end;
   end;
finally
{$IFDEF CODE_TRACER}
   If Assigned(CodeTracer) then Begin
      CodeTracer.SendMessage(dxctInfo,'TDXClientThread.Execute Terminating');
   End;
{$ENDIF}
   FreeOnTerminate:=False;
   Terminate;
end;
end;

///////////////////////////////////////////////////////////////////////////////
// ANCESTOR TO ALL PROTOCOLS
// =========================
// DXSERVERCORE - Mainly Allows for "Properties" to be defined. And to Start,
// Stop, and Suspend the 'LISTNER' Thread.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//CREATE:
//       Protocols use this object to interact with the DXSock environment. This
//       Section allows the protocol to specify the status of the server.
///////////////////////////////////////////////////////////////////////////////
constructor TDXServerCore.Create(AOwner: TComponent);
begin
   inherited create(aowner);
   fbActive:=False;
   fiMaxConn:=200;
   fbBufferCreates:=True;
{$IFNDEF LINUX}
   fstPriority:=tpNormal;
   fltPriority:=tpNormal;
{$ENDIF}
   fWhichProtocol:=wpTCPOnly;
   fBlockSizeFlags:=bsfHuge;
   fServerType:=stThreadBlocking;
end;

///////////////////////////////////////////////////////////////////////////////
//DESTROY:
//        If the cleans up any pending threads.
///////////////////////////////////////////////////////////////////////////////

destructor TDXServerCore.Destroy;
begin
   if fbActive then SetActive(False);
   inherited destroy;
end;

function TDXServerCore.GetSocket: TDXSock;
begin
   if Assigned(ListenerThread) then
      Result := ListenerThread.ListenerSocket
   else Result := nil;
end;

function TDXServerCore.ActiveNumberOfConnections: Integer;
begin
   if Assigned(ListenerThread) then  Result := ListenerThread.ActiveNumberOfConnections
   else Result := 0;
end;

procedure TDXServerCore.SetActive(value: boolean);
begin
{$IFNDEF OBJECTS_ONLY}
   if (csDesigning in ComponentState) then exit;
{$ENDIF}
   fbSuspend := False;

   if Value <> fbActive then begin
      if Value then begin
         if not Assigned(feNewConnect) then begin
            if Not Assigned(feUDPData) then Begin
               ShowMessageWindow(
                  'Developer Trap #1!',
                  'You must specify a OnNewConnect Event!');
               Exit;
            End;
         end;
         fbActive:=Value;
{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            CodeTracer.StartTransaction;
            CodeTracer.SendMessage(dxctDebug,'TDXSServerCore.Active=True');
            CodeTracer.SendObject(dxctDebug,Self);
         End;
{$ENDIF}
         fbSuspend := False;
         ListenerThread := TDXServerCoreThread.Create(True);
         ListenerThread.fsBindTo := fsBindTo;
         ListenerThread.fiServerPort := fiServerPort;
         ListenerThread.fiMaxConn := fiMaxConn;
         ListenerThread.feNewConnect := feNewConnect;
         ListenerThread.feMaxConnects := feMaxConnects;
         ListenerThread.feListenerFailed := feListenerFailed;
         ListenerThread.feIdle := feIdle;
         ListenerThread.feSleep := feSleep;
         ListenerThread.feWakeUp := feWakeUp;
{$IFNDEF LINUX}
         ListenerThread.Priority := fltPriority;
         ListenerThread.SpawnedThreadPriority := fstPriority;
{$ENDIF}
         ListenerThread.fBlockSizeFlags := fBlockSizeFlags;
         ListenerThread.fbSuspend := False;
         ListenerThread.feUDPData:=feUDPData; // 3.0c

         If DebugHOOK<>0 then ListenerThread.fbBufferCreates:=False
         Else ListenerThread.fbBufferCreates := fbBufferCreates;

{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            ListenerThread.CodeTracer:=CodeTracer;
         End;
{$ENDIF}

         ListenerThread.fWhichProtocol := fWhichProtocol;
         ListenerThread.Resume;
      end
      else begin
         fbActive:=Value;
         if Assigned(ListenerThread) then begin
{$IFDEF CODE_TRACER}
            If Assigned(CodeTracer) then Begin
               CodeTracer.SendMessage(dxctInfo,'DXServerCore.ListenerThread still existed - terminating');
            End;
{$ENDIF}
            If ListenerThread.Suspended then ListenerThread.Resume;
            ListenerThread.FreeOnTerminate:=True;
            ListenerThread.Terminate;
// 9/11/2002
//            ListenerThread.WaitFor;
//            ListenerThread.Free;
         end;
// 9/11/2002
//         ListenerThread:=Nil;
{$IFDEF CODE_TRACER}
         If Assigned(CodeTracer) then Begin
            CodeTracer.SendObject(dxctDebug,Self);
            CodeTracer.SendMessage(dxctDebug,'TDXSServerCore.Active=False');
            CodeTracer.EndTransaction;
         End;
{$ENDIF}
      end;
   end;
end;

procedure TDXServerCore.Start;
begin
   SetActive(True);
end;

procedure TDXServerCore.Stop;
begin
   SetActive(False);
end;

procedure TDXServerCore.Open;
begin
   Start;
end;

procedure TDXServerCore.Close;
begin
   Stop;
end;

procedure TDXServerCore.Pause;
begin
   SetSuspend(True);
End;

procedure TDXServerCore.Resume;
begin
   SetSuspend(False);
End;

procedure TDXServerCore.SetSuspend(value: boolean);
begin
   if fbActive then begin
      fbSuspend := Value;
      ListenerThread.SuspendListener := Value;
   end;
end;

procedure TDXServerCore.SetfiMaxConn(Value: Integer);
begin
   if Value < 1 then begin
      if DebugHOOK <> 0 then Exit; {cant enable this from within DELPHI live!}
      fiMaxConn := -1;
      fbBufferCreates := True;
   end
   else fiMaxConn := Value;
end;

function TDXServerCore.GetThreadCacheSize: Integer;
begin
   Result := fiMaxConn;
end;

procedure TDXServerCore.SetThreadCacheSize(value: Integer);
begin
   SetfiMaxConn(Value);
end;

function TDXServerCore.InternalSessionTracker:TDXSessionTracker;
Begin
   if Assigned(ListenerThread) then  Result:=ListenerThread.fSessionTracker
   Else Result:=Nil;
End;

end.

