unit uROResWriter;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface
uses
  SysUtils, Classes;


procedure WriteRES(InData, OutStream: TStream; const Name: WideString);

implementation

const
  FirstEmptyResource: array[0..31] of char =
  (#$00,#$00,#$00,#$00,#$20,#$00,#$00,#$00,#$FF,#$FF,#$00,#$00,#$FF,#$FF,#$00,
  #$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,#$00,
  #$00,#$00);
procedure WriteRES(InData, OutStream: TStream; const Name: WideString);
  procedure WriteStr(const s: string);
  begin
    OutStream.Write(s[1], length(s));
  end;
  procedure WriteLong(l: Cardinal);
  begin
    OutStream.Write(l, 4);
  end;
begin
  Outstream.Write(FirstEmptyResource, 32);
  WriteLong(Indata.Size - Indata.Position); // Resource Size
  WriteLong(32 + Length(Name) * 2); // Header Size
  WriteLong($000AFFFF); // RT_RCDATA
  OutStream.Write(Name[1], Length(Name) * 2);
  WriteStr(#$00#$00); // Null terminater
  if (Length(Name) * 2 + 2) mod 4 <> 0 then
    WriteStr(#$00#$00); // extra 0 for dword alignment
  WriteLong(0); // Data Version
  WriteLong(0); // Flags + Language
  WriteLong(0); // Resource Version
  WriteLong(0); // Characteristics
  OutStream.CopyFrom(Indata, Indata.Size - Indata.Position);
end;

end.
