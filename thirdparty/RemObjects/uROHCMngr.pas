{Copyright:      Hagen Reddmann  mailto:HaReddmann@AOL.COM
 Author:         Hagen Reddmann
 Remarks:        freeware, but this Copyright must be included
 known Problems: none
 Version:        3.0,  Part I from Delphi Encryption Compendium
                 Delphi 2-4, designed and testet under D3
 Description:    included the Designtime Components
                 TROHashManager for the TROHash-Administration
                 TROCipherManager for the TROCipher-Administration
                 for Programmers with "Drop and Design any Component Fealing"
                 the normaly way is without any Manager's

 Version 3.0
     Versionsmanagement, no chnages on TManager Classes

 Version 2.2
                 added Progress Event to fill a gauge

      bug fixes: TROHashManager memory leak, .Destroy frees now FDigest

 Version 2.1
                 TROCipherManager: Methods added
                   EncodeBuffer(), DecodeBuffer()
                   EncodeString(), DecodeString()

}
unit uROHCMngr;

interface

{$I uROVer.inc}

uses Classes, uRODECUtil, uROHash, uROCipher;

type
  TProgressEvent = procedure(Sender: TObject; Current, Maximal: Integer) of Object;

  TROHashManager = class(TComponent)
  private
    FHashClass: TROHashClass;
    FHash: TROHash;
    FDigest: String;
    FOnProgress: TProgressEvent;
    function GetInfo(Index: Integer): String;
    procedure SetInfo(Index: Integer; Value: String);
    function GetClass: TROHashClass;
    procedure SetClass(Value: TROHashClass);
    function GetHash: TROHash;
    function GetAlgorithm: String;
    procedure SetAlgorithm(Value: String);
    function GetDigestStr(Index: Integer): String;
    function GetDigestSize: Integer;
    procedure ReadHash(Reader: TReader);
    procedure WriteHash(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
  public
    destructor Destroy; override;
    procedure CalcBuffer(const Buffer; BufferSize: Integer);
    procedure CalcStream(const Stream: TStream; StreamSize: Integer);
    procedure CalcString(const Data: String);
    procedure CalcFile(const FileName: String);

    property Digest: String read FDigest;
    property DigestSize: Integer read GetDigestSize;
    property DigestString[Format: Integer]: String read GetDigestStr;
    property HashClass: TROHashClass read GetClass write SetClass;
    property Hash: TROHash read GetHash;
  published
    property Algorithm: String read GetAlgorithm write SetAlgorithm stored False;
    property Description: String index 0 read GetInfo write SetInfo stored False;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
  end;

  TROCipherManager = class(TComponent)
  private
    FHashManager: TROHashManager;
    FCipherClass: TROCipherClass;
    FCipher: TROCipher;
    FMode: TCipherMode;
    FOnProgress: TProgressEvent;
    procedure SetHashManager(Value: TROHashManager);
    procedure SetMode(Value: TCipherMode);
    function GetCipher: TROCipher;
    function GetClass: TROCipherClass;
    procedure SetClass(Value: TROCipherClass);
    function GetAlgorithm: String;
    procedure SetAlgorithm(Value: String);
    function GetInfo(Index: Integer): String;
    procedure SetInfo(Index: Integer; Value: String);
    procedure ReadCipher(Reader: TReader);
    procedure WriteCipher(Writer: TWriter);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DefineProperties(Filer: TFiler); override;
  public
    destructor Destroy; override;
    procedure InitKey(const Key: String; IVector: Pointer);

    procedure EncodeFile(const Source, Dest: String);
    procedure DecodeFile(const Source, Dest: String);
    procedure EncodeStream(Source, Dest: TStream; DataSize: Integer);
    procedure DecodeStream(Source, Dest: TStream; DataSize: Integer);
    procedure EncodeBuffer(const Source; var Dest; DataSize: Integer);
    procedure DecodeBuffer(const Source; var Dest; DataSize: Integer);
    function EncodeString(const Source: String): String;
    function DecodeString(const Source: String): String;

    property Cipher: TROCipher read GetCipher;
    property CipherClass: TROCipherClass read GetClass write SetClass;
  published
    property Algorithm: String read GetAlgorithm write SetAlgorithm stored False;
    property Description: String index 0 read GetInfo write SetInfo stored False;
    property Mode: TCipherMode read FMode write SetMode default cmCBC;
    property HashManager: TROHashManager read FHashManager write SetHashManager;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
  end;

implementation

uses SysUtils, Windows;

procedure TROHashManager.ReadHash(Reader: TReader);
var
  CN: String;
  I: Integer;
begin
  CN := Reader.ReadString;
  for I := 0 to HashList.Count-1 do
    if CN = TROHashClass(HashList.Objects[I]).ClassName then
    begin
      SetClass(TROHashClass(HashList.Objects[I]));
      Exit;
    end;
end;

procedure TROHashManager.WriteHash(Writer: TWriter);
begin
  Writer.WriteString(HashClass.ClassName);
end;

procedure TROHashManager.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineProperty('Hash', ReadHash, WriteHash, FHashClass <> nil);
end;

function TROHashManager.GetInfo(Index: Integer): String;
begin
  Result := '';
  case Index of
    0: begin
         Index := HashList.IndexOfObject(Pointer(HashClass));
         if Index >= 0 then
         begin
           Result := Trim(HashList[Index]);
           Delete(Result, 1, Pos('=', Result));
         end;
         if Length(Result) > 0 then Result := Result + ', ';
         Result := Result + IntToStr(DigestSize * 8) + 'bit Digestsize';
       end;
  end;
end;

procedure TROHashManager.SetInfo(Index: Integer; Value: String);
begin
end;

function TROHashManager.GetDigestStr(Index: Integer): String;
begin
  Result := StrToFormat(PChar(FDigest), Length(FDigest), Index);
end;

function TROHashManager.GetDigestSize: Integer;
begin
  Result := HashClass.DigestKeySize;
end;

function TROHashManager.GetClass: TROHashClass;
begin
  if FHashClass = nil then SetClass(DefaultHashClass);
  Result := FHashClass;
end;

function TROHashManager.GetHash: TROHash;
begin
  if FHash = nil then FHash := HashClass.Create(nil);
  Result := FHash;
end;

procedure TROHashManager.SetClass(Value: TROHashClass);
begin
  if Value <> FHashClass then
  begin
    if Value = nil then Value := DefaultHashClass;
    FHashClass := Value;
    FHash.Free;
    FHash := nil;
    FDigest := '';
  end;
end;

function TROHashManager.GetAlgorithm: String;
begin
  Result := GetHashName(HashClass);
end;

procedure TROHashManager.SetAlgorithm(Value: String);
begin
  SetClass(GetHashClass(Value));
end;

destructor TROHashManager.Destroy;
begin
  FHash.Free;
  FHash := nil;
  inherited Destroy;
end;

procedure TROHashManager.CalcBuffer(const Buffer; BufferSize: Integer);
begin
  FDigest := HashClass.CalcBuffer(Buffer, BufferSize, nil, fmtCOPY);
end;

procedure TROHashManager.CalcStream(const Stream: TStream; StreamSize: Integer);
begin
  Progress := FOnProgress;
  try
    FDigest := HashClass.CalcStream(Stream, StreamSize, nil, fmtCOPY);
  finally
    Progress := nil;
  end;
end;

procedure TROHashManager.CalcString(const Data: String);
begin
  FDigest := HashClass.CalcString(Data, nil, fmtCOPY);
end;

procedure TROHashManager.CalcFile(const FileName: String);
begin
  Progress := FOnProgress;
  try
    FDigest := HashClass.CalcFile(FileName, nil, fmtCOPY);
  finally
    Progress := nil;
  end;
end;

procedure TROCipherManager.SetHashManager(Value: TROHashManager);
begin
  if Value <> FHashManager then
  begin
    FHashManager := Value;
    if FHashManager <> nil then FHashManager.FreeNotification(Self);
  end;
end;

procedure TROCipherManager.SetMode(Value: TCipherMode);
begin
  FMode := Value;
  if FCipher <> nil then FCipher.Mode := FMode;
end;

function TROCipherManager.GetCipher: TROCipher;
begin
  if FCipher = nil then
  begin
    FCipher := CipherClass.Create('', nil);
    FCipher.Mode := FMode;
  end;  
  Result := FCipher;
end;

function TROCipherManager.GetClass: TROCipherClass;
begin
  if FCipherClass = nil then FCipherClass := DefaultCipherClass;
  Result := FCipherClass;
end;

procedure TROCipherManager.SetClass(Value: TROCipherClass);
begin
  if Value <> CipherClass then
  begin
    FCipher.Free;
    FCipher := nil;
    FCipherClass := Value;
  end;
end;

function TROCipherManager.GetAlgorithm: String;
begin
  Result := GetCipherName(CipherClass);
end;

procedure TROCipherManager.SetAlgorithm(Value: String);
begin
  SetClass(GetCipherClass(Value));
end;

function TROCipherManager.GetInfo(Index: Integer): String;
begin
  Result := '';
  case Index of
    0: begin
         Index := CipherList.IndexOfObject(Pointer(CipherClass));
         if Index >= 0 then
         begin
           Result := Trim(CipherList[Index]);
           Delete(Result, 1, Pos('=', Result));
         end;
         if Length(Result) > 0 then Result := Result + ', ';
         Result := Result + IntToStr(CipherClass.MaxKeySize * 8) + 'bit Key';
       end;
  end;
end;

procedure TROCipherManager.SetInfo(Index: Integer; Value: String);
begin
end;

procedure TROCipherManager.ReadCipher(Reader: TReader);
var
  CN: String;
  I: Integer;
begin
  CN := Reader.ReadString;
  for I := 0 to CipherList.Count-1 do
    if CN = TROCipherClass(CipherList.Objects[I]).ClassName then
    begin
      SetClass(TROCipherClass(CipherList.Objects[I]));
      Exit;
    end;
end;

procedure TROCipherManager.WriteCipher(Writer: TWriter);
begin
  Writer.WriteString(CipherClass.ClassName);
end;

procedure TROCipherManager.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = FHashManager) then
    HashManager := nil;
end;

procedure TROCipherManager.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineProperty('Cipher', ReadCipher, WriteCipher, FCipherClass <> nil);
end;

destructor TROCipherManager.Destroy;
begin
  FCipher.Free;
  FCipher := nil;
  inherited Destroy;
end;

procedure TROCipherManager.InitKey(const Key: String; IVector: Pointer);
begin
  if FHashManager <> nil then Cipher.HashClass := FHashManager.HashClass;
  Cipher.InitKey(Key, IVector);
end;

procedure TROCipherManager.EncodeFile(const Source, Dest: String);
begin
  Progress := FOnProgress;
  try
    Cipher.CodeFile(Source, Dest, paEncode);
  finally
    Progress := nil;
  end;
end;

procedure TROCipherManager.DecodeFile(const Source, Dest: String);
begin
  Progress := FOnProgress;
  try
    Cipher.CodeFile(Source, Dest, paDecode);
  finally
    Progress := nil;
  end;
end;

procedure TROCipherManager.EncodeStream(Source, Dest: TStream; DataSize: Integer);
begin
  Progress := FOnProgress;
  try
    Cipher.CodeStream(Source, Dest, DataSize, paEncode);
  finally
    Progress := nil;
  end;
end;

procedure TROCipherManager.DecodeStream(Source, Dest: TStream; DataSize: Integer);
begin
  Progress := FOnProgress;
  try
    Cipher.CodeStream(Source, Dest, DataSize, paDecode);
  finally
    Progress := nil;
  end;
end;

procedure TROCipherManager.EncodeBuffer(const Source; var Dest; DataSize: Integer);
begin
  Cipher.EncodeBuffer(Source, Dest, DataSize);
end;

procedure TROCipherManager.DecodeBuffer(const Source; var Dest; DataSize: Integer);
begin
  Cipher.DecodeBuffer(Source, Dest, DataSize);
end;

function TROCipherManager.EncodeString(const Source: String): String;
begin
  Result := Cipher.CodeString(Source, paEncode, fmtDEFAULT);
end;

function TROCipherManager.DecodeString(const Source: String): String;
begin
  Result := Cipher.CodeString(Source, paDecode, fmtDEFAULT);
end;


end.
