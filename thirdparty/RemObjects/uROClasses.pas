unit uROClasses;

{$I RemObjects.inc}

interface

uses {$IFDEF DOTNET}
     Borland.Delphi.Classes;
     {$ELSE}
     Classes;
     {$ENDIF}

{$IFDEF DOTNET}
type TClassList = class(TList)
     private
     public
       procedure Add(iItem:TClass); reintroduce;
       procedure Remove(iItem: TClass); reintroduce;
     end;
{$ELSE}
type TClassList = TList;
{$ENDIF DOTNET}

implementation

{ TClassList }

{$IFDEF DOTNET}
procedure TClassList.Add(iItem: TClass);
begin
  inherited Add(pointer(iItem));
end;

procedure TClassList.Remove(iItem: TClass);
begin
  inherited Remove(pointer(iItem));
end;
{$ENDIF DOTNET}

end.
