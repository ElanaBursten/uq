{ Summary: }
unit uROTypes;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  {$MESSAGE error 'This unit will not be used in .NET, use RemObjects.SDK.Types intead' }  
  {$ELSE}
  Classes, TypInfo, Contnrs,
  {$ENDIF DOTNET}
  uRODL, uROClientIntf;

type TRODataType = (rtInteger, rtDateTime, rtDouble, rtCurrency, rtWidestring, rtString,
                    rtInt64, rtBoolean, rtVariant, rtBinary, rtUserDefined);

const DataTypeNames : array[TRODataType] of string = (
        'Integer',
        'DateTime',
        'Double',
        'Currency',
        'Widestring',
        'String',
        'Int64',
        'Boolean',
        'Variant',
        'Binary',
        '???');

      PasFlagNames : array[TRODLParamFlag] of string =
        ('const', 'out', 'var', 'result');

      BoolStr : array[boolean] of string = ('false','true');

type { TROSerializable }
     TROComplexType = class(TPersistent)
     private
       fFieldCount : integer;
       {$IFDEF DOTNET}
       {$ELSE}
       fFieldList : PPropList;
       {$ENDIF DOTNET}

       function GetFieldCount: integer;
       {$IFDEF DOTNET}
       {$ELSE}
       function GetFieldInfo(Index: integer): PTypeInfo;
       {$ENDIF DOTNET}
       function GetFieldName(Index: integer): string;

     protected
       procedure FreeInternalProperties; virtual;

     public
       constructor Create; virtual;
       destructor Destroy; override;

       procedure SetFieldValue(const aFieldName : string; const aValue : Variant);
       function GetFieldValue(const aFieldName : string) : Variant;

       property FieldName[Index : integer] : string read GetFieldName;
       {$IFDEF DOTNET}
       {$ELSE}
       property FieldInfo[Index : integer] : PTypeInfo read GetFieldInfo;
       {$ENDIF DOTNET}
       property FieldCount : integer read GetFieldCount;
     end;
     TROComplexTypeClass = class of TROComplexType;

     { TROArray }
     TROArray = class(TROComplexType)
     private

     protected
       function GetCount : integer; virtual; abstract;

     public
       destructor Destroy; override;

       {$IFDEF DOTNET}
       class function GetItemType : System.Type; virtual;
       {$ELSE}
       class function GetItemType : PTypeInfo; virtual; {$IFNDEF BCB}abstract;{$ENDIF BCB}
       {$ENDIF DOTNET}
       class function GetItemClass : TROComplexTypeClass; virtual; {$IFNDEF BCB}abstract;{$ENDIF BCB}
       class function GetItemSize : integer; virtual; {$IFNDEF BCB}abstract;{$ENDIF BCB}
       function GetItemRef(Index : integer) : pointer; virtual; abstract;
       procedure SetItemRef(Index: integer; Ref : pointer); virtual; abstract; // Only need for arrays of complex types
       procedure Resize(ElementCount : integer); virtual; abstract;

       procedure Delete(Index : integer); virtual; abstract;
       procedure Clear; virtual; abstract;

       property Count : integer read GetCount;
     end;
     TROArrayClass = class of TROArray;

function StrToDataType(const aString : string) : TRODataType;

function IsSimpleType(const aTypeName : string) : boolean;

function IsStruct(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
function IsArray(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
function IsEnum(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;

function IsImplementedAsClass(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;

function IsInputFlag(Flag : TRODLParamFlag) : boolean;
function IsOutputFlag(Flag : TRODLParamFlag) : boolean;

{$IFDEF DOTNET}
{$ELSE}
procedure RegisterROClass(aComplexTypeClass : TROComplexTypeClass);
{$ENDIF DOTNET}
function FindROClass(const aClassName: string):  TROComplexTypeClass;
function FindROArray(const anItemClassName: string):  TROArrayClass;

implementation

uses
  {$IFDEF REMOBJECTS_TRIAL}
  Forms, Windows, Dialogs,
  {$ENDIF REMOBJECTS_TRIAL}
  {$IFDEF DOTNET}
  {$ELSE}
  SysUtils,
  {$ENDIF DOTNET}
  uRORes;

var _ComplexTypes : TList;

{$IFDEF DOTNET}
{$ELSE}
procedure RegisterROClass(aComplexTypeClass : TROComplexTypeClass);
var clsname: string;
begin
  with _ComplexTypes do
    while (IndexOf(aComplexTypeClass)=-1) do begin
      clsname := aComplexTypeClass.ClassName;

      if GetClass(clsname) <> nil then
        RaiseError(err_ClassAlreadyRegistered, [clsname]);

      Add(aComplexTypeClass);
    end;
end;
{$ENDIF DOTNET}

function FindROClass(const aClassName: string): TROComplexTypeClass;
var i: Integer;
begin
  result := NIL;

  with _ComplexTypes do
    for i := 0 to (Count-1) do begin
      {$IFDEF DOTNET}
      if (TROComplexTypeClass(Items[i]).ClassName = aClassName) then begin
      {$ELSE}
      if (CompareText(TROComplexTypeClass(Items[i]).ClassName, aClassName)=0) then begin
      {$ENDIF DOTNET}
        result := TROComplexTypeClass(Items[I]);
        Exit;
      end;
    end;
end;

function FindROArray(const anItemClassName: string):  TROArrayClass;
var i: Integer;
    itmn, cn : string;
begin
  result := NIL;

  with _ComplexTypes do
    for i := 0 to (Count-1) do begin
      cn := TROComplexTypeClass(Items[i]).ClassParent.ClassName;
      if (cn='TROArray') then begin
        itmn := TROArrayClass(Items[i]).GetItemType^.Name;
        if (CompareText(itmn, anItemClassName)=0) then begin
          result := Items[I];
          Exit;
        end;
      end;
  end;
end;

function StrToDataType(const aString : string) : TRODataType;
var dt : TRODataType;
begin
  result := rtUserDefined;

  for dt := Low(TRODataType) to High(TRODataType) do
    if (CompareText(aString, DataTypeNames[dt])=0) then begin
      result := dt;
      Exit;
    end;
end;

function IsInputFlag(Flag : TRODLParamFlag) : boolean;
begin
  result := Flag in [fIn, fInOut]
end;

function IsOutputFlag(Flag : TRODLParamFlag) : boolean;
begin
  result := Flag in [fOut, fInOut, fResult]
end;

function IsSimpleType(const aTypeName : string) : boolean;
var dt : TRODataType;
begin
  for dt := Low(DataTypeNames) to High(DataTypeNames) do begin
    result := (CompareText(aTypeName, DataTypeNames[dt])=0);
    if result then Break;
  end;
end;

function IsStruct(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
var i : integer;
begin
  result := FALSE;
  for i := 0 to (aLibrary.StructCount-1) do
    if (CompareText(aLibrary.Structs[i].Info.Name, aTypeName)=0) then begin
      result := TRUE;
      Exit;
    end
end;

function IsEnum(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
var i : integer;
begin
  result := FALSE;
  for i := 0 to (aLibrary.EnumCount-1) do
    if (CompareText(aLibrary.Enums[i].Info.Name, aTypeName)=0) then begin
      result := TRUE;
      Exit;
    end
end;

function IsArray(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
var i : integer;
begin
  result := FALSE;
  for i := 0 to (aLibrary.ArrayCount-1) do
    if (CompareText(aLibrary.Arrays[i].Info.Name, aTypeName)=0) then begin
      result := TRUE;
      Exit;
    end
end;

function IsImplementedAsClass(const aTypeName : string; aLibrary : TRODLLibrary) : boolean;
begin
  result := IsStruct(aTypeName, aLibrary) or
            IsArray(aTypeName, aLibrary) or
            (CompareText(aTypeName, 'binary')=0);
end;

{ TROComplexType }
constructor TROComplexType.Create;
begin
  inherited;
  fFieldCount := -1;
  fFieldList := NIL;
end;

destructor TROComplexType.Destroy;
begin
  FreeInternalProperties;

  if (fFieldCount<>-1) and (fFieldList<>NIL)
    then FreeMem(fFieldList, fFieldCount*SizeOf(PPropInfo));

  inherited;
end;

procedure TROComplexType.FreeInternalProperties;
var props : PPropList;
    cnt, i : integer;
    sub : TObject;
begin
  cnt := GetTypeData(ClassInfo).PropCount;
  if (cnt=0) then Exit;

  GetMem(props, cnt*SizeOf(PPropInfo));
  try
    cnt := GetPropList(ClassInfo, [tkClass], props);

    for i := 0 to (cnt-1) do begin
      with props^[i]^ do
        if (PropType^.Kind=tkClass) then begin
          sub := GetObjectProp(Self, Name);
          sub.Free;
        end;
    end;
  finally
    FreeMem(props, cnt*SizeOf(PPropInfo));
  end;
end;

function TROComplexType.GetFieldCount: integer;
begin
  if (fFieldCount=-1) then begin
    fFieldCount := GetTypeData(ClassInfo).PropCount;
    if (fFieldCount>0) then begin
      GetMem(fFieldList, fFieldCount*SizeOf(PPropInfo));
      GetPropList(PTypeInfo(ClassInfo), tkProperties, fFieldList);
    end;
  end;

  result := fFieldCount;
end;

function TROComplexType.GetFieldInfo(Index: integer): PTypeInfo;
begin
  result := NIL;
  if (Index<FieldCount)
    then result := fFieldList[Index]^.PropType^;
end;

function TROComplexType.GetFieldName(Index: integer): string;
begin
  if (Index<FieldCount)
    then result := fFieldList[Index]^.Name
end;

function TROComplexType.GetFieldValue(const aFieldName: string): Variant;
begin
  if (aFieldName<>'') then result := GetPropValue(Self, aFieldName)
                      else result := '';
end;

procedure TROComplexType.SetFieldValue(const aFieldName: string;
  const aValue: Variant);
begin
  if (aFieldName<>'')
    then SetPropValue(Self, aFieldName, aValue);
end;

{ TROArray }
destructor TROArray.Destroy;
begin
  Clear;

  inherited;
end;

{$IFDEF BCB}
class function TROArray.GetItemType : PTypeInfo;
begin
  raise Exception.Create('Abstract method called: TROArray.GetItemType.');
end;

class function TROArray.GetItemClass : TROComplexTypeClass;
begin
  raise Exception.Create('Abstract method called: TROArray.GetItemClass.');
end;

class function TROArray.GetItemSize : integer;
begin
  raise Exception.Create('Abstract method called: TROArray.GetItemSize.');
end;
{$ENDIF BCB}

{$IFDEF REMOBJECTS_TRIAL}
procedure Check;
const
  A1:string = 'TApplication';// array[0..12] of char = 'TApplication'#0;
  A2:string = 'TAlignPalette';
  //A3:string = 'TPropertyInspector';
  A4:string = 'TAppBuilder';
  {$IFDEF DELPHI5}
  T1:string = 'Delphi 5';
  {$ENDIF}
  {$IFDEF DELPHI6}
  T1:string = 'Delphi 6';// array[0..10] of char = 'Delphi 6.0'#0;
  {$ENDIF}
  {$IFDEF DELPHI7}
  T1:string = 'Delphi 7';
  {$ENDIF}
begin
  if not ((FindWindow(pChar(A1), pChar(T1)) <> 0) and (FindWindow(pChar(A2), nil) <> 0) and
          //(FindWindow(pChar(A3), nil) <> 0) and
          (FindWindow(pChar(A4), nil) <> 0)) then begin
    ShowMessage('This application was built with a trial version for RemObjects SDK.'#13#13'It may only be launched from within the '+T1+' IDE.');
    Halt;
  end;
end;
{$ENDIF}

initialization
  {$IFDEF REMOBJECTS_TRIAL}
  Check();
  {$ENDIF}
  _ComplexTypes := TList.Create;

finalization
  _ComplexTypes.Free;

end.
