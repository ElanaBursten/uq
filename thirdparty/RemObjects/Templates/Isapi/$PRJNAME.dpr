library $PRJNAME;

{#ROGEN:$SVCLIBNAME.rodl} // RemObjects: Careful, do not remove!

uses
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIApp,
  Unit1 in 'Unit1.pas' {WebModule1: TWebModule};

{$R *.RES}
{$R RODLFile.RES} // RemObjects: Careful, do not remove!

{
  Important note: if you have Delphi 6 you should add the unit ISAPIThreadPool to the above list
}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TWebModule1, WebModule1);
  Application.Run;
end.