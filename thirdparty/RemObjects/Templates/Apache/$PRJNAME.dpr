library $PRJNAME;

{#ROGEN:$SVCLIBNAME.rodl} // RemObjects: Careful, do not remove!

uses
  WebBroker,
  ApacheApp,
  Unit1 in 'Unit1.pas' {WebModule1: TWebModule};

{$R *.RES}
{$R RODLFile.RES} // RemObjects: Careful, do not remove!

exports
  apache_module name '$PRJNAME_module';

begin
  Application.Initialize;
  Application.CreateForm(TWebModule1, WebModule1);
  Application.Run;
end.