program  $PRJNAME;

{$APPTYPE CONSOLE}
{#ROGEN:$SVCLIBNAME.rodl} // RemObjects: Careful, do not remove!

uses
  WebBroker,
  CGIApp,
  Unit1 in 'Unit1.pas' {WebModule1: TWebModule};

{$R *.RES}
{$R RODLFile.RES} // RemObjects: Careful, do not remove!

begin
  Application.Initialize;
  Application.CreateForm(TWebModule1, WebModule1);
  Application.Run;
end.
