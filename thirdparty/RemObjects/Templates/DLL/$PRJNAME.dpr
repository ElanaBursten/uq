library $PRJNAME;

{#ROGEN:$SVCLIBNAME.rodl} // RemObjects: Careful, do not remove!

uses
  Windows,
  uRODLLServer, // Do not remove! Creates the instance of the DLL server
  uRO$MSGCLSNAME;

{$R *.RES}
{$R RODLFile.RES} // RemObjects: Careful, do not remove!


var $MSGCLSNAME : TRO$MSGCLSNAME;

procedure ROProc(Reason:integer);
begin
  case Reason of
    DLL_PROCESS_ATTACH: begin
      $MSGCLSNAME := TRO$MSGCLSNAME.Create(NIL);
      RegisterMessage($MSGCLSNAME);
    end;

    DLL_PROCESS_DETACH: begin
      $MSGCLSNAME.Free;
    end;
  end
end;

begin
  DLLProc:=@ROProc;
  ROProc(DLL_PROCESS_ATTACH)
end.