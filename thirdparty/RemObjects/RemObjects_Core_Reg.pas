unit RemObjects_Core_Reg;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

procedure Register;

implementation

uses
  Classes,
  uRORes,
  uROSOAPMessage,
  {$IFDEF MSWINDOWS}
  uROWinMessageChannel,
  uROWinMessageServer,
  uROWinInetHttpChannel,
  uRODLLChannel,
  {$ENDIF MSWINDOWS}
  uROBINMessage,
  uROWebBrokerServer,
  uROPoweredByRemObjectsButton;

procedure Register;
begin
  RegisterComponents(str_ProductName,
                     [TROBINMessage,
                     TROSOAPMessage,
                     {$IFDEF MSWINDOWS}
                     TROWinInetHttpChannel,
                     TROWinMessageChannel,
                     TROWinMessageServer,
                     TRODLLChannel,
                     {$ENDIF MSWINDOWS}
                     TROWebBrokerServer,
                     TRoPoweredByRemObjectsButton]);
end;

end.
