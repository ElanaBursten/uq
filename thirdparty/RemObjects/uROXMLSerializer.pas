{ Summary: Contains the class TROXMLSerializer }
unit uROXMLSerializer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROSerializer, TypInfo, uROXMLIntf, uROTypes;

const
  // SOAP specifics
  tag_Envelope = 'Envelope';
  tag_Body     = 'Body';
  tag_Header   = 'Header';
  tag_Fault    = 'Fault';

  ns_Envelope = 'SOAP-ENV';
  ns_Standard = 'xs';
  ns_Custom   = 'ro';

  tag_HRef = 'href';
  tag_Id   = 'id';

  // Misc
  SOAP_DecimalSeperator  = '.';
  SOAP_BoolValues: array [Boolean] of  string = ('false', 'true');

  SOAP_DateFormat       = 'yyyy-mm-dd';
  SOAP_DateFormatLength = 10;

  SOAP_DateTimeFormat       = 'yyyy-mm-dd"T"hh":"nn":"ss';
  SOAP_DateTimeFormatLength = 19;

  SOAP_TimeFormat       = 'hh":"nn":"ss';
  SOAP_TimeFormatLength = 8;

  // Data types signatures
  dts_Array        = 'SOAP-ENC:Array';
  dts_base64Binary = 'base64Binary';


type {----------------- Misc -----------------}

     { Summary: XML serialization options
       Description:
         XML serialization options.
     }
     TXMLSerializationOption = (
       xsoWriteMultiRefArray,  // Not implemented yet.
       xsoWriteMultiRefObject, // Writes the elements of the array as tagged nodes outside the SOAP Envelope
       xsoSendUntyped);        // Doesn't include type information attributes in the envelope

     TXMLSerializationOptions = set of TXMLSerializationOption;

     {----------------- TROXMLSerializer -----------------}

     { Summary: XML serializer class
       Description:
         XML serializer class. Provides the implementation for the methods introduced by TROSerliazer.
         See the encoding standards defined at http://www.w3.org/TR/SOAP
     }
     TROXMLSerializer = class(TROSerializer)
     private
       fNode: IXMLNode;
       fSerializationOptions: TXMLSerializationOptions;

       fBodyNode : IXMLNode;
       fMaxRef : integer;

       function BodyNode : IXMLNode;
     protected
       { Writers }
       procedure WriteInteger(const aName : string; anOrdType : TOrdType; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteInt64(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteString(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteWideString(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteDateTime(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteFloat(const aName : string; aFloatType : TFloatType; const Ref; ArrayElementId : integer = -1); override;

       procedure BeginWriteObject(const aName: string; aClass : TClass; anObject: TObject;
                                  var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1); override;
       procedure EndWriteObject(const aName: string; aClass : TClass; anObject: TObject; const LevelRef : IUnknown); override;

       procedure CustomWriteObject(const aName: string; aClass : TClass; const Ref; ArrayElementId : integer = -1); override;

       { Readers }
       procedure ReadInteger(const aName : string; anOrdType : TOrdType; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadInt64(const aName : string; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); override;
       procedure ReadWideString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); override;
       procedure ReadDateTime(const aName : string; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadFloat(const aName : string; aFloatType : TFloatType; var Ref; ArrayElementId : integer = -1); override;

       procedure BeginReadObject(const aName : string; aClass : TClass; var anObject : TObject;
                                 var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1); override;
       procedure EndReadObject(const aName : string; aClass : TClass; var anObject : TObject; const LevelRef : IUnknown); override;

       procedure CustomReadObject(const aName: string; aClass : TClass; var Ref; ArrayElementId: integer); override;

     public
       constructor Create(aStorageRef : pointer); override;

       function SetStorageRef(aStorageRef: pointer) : boolean; override;

       { Summary: XML serialization options
         Description:
           XML serialization options. See type TXMLSerializationOptions. }
       property SerializationOptions : TXMLSerializationOptions read fSerializationOptions write fSerializationOptions;
     end;


{ Summary:
  Description:
}
function XMLToObject(const someXML : string) : TROComplexType;

{ Summary:
  Description:
}
function ObjectToXML(anObject : TROComplexType;
                     const anObjectName : string = '') : string;


procedure SplitNodeName(const aNode : IXMLNode; out aNameSpace, aLocalName : string);

function SOAPDateTimeToDateTime(const aSOAPDate  : string) : TDateTime;
function DateTimeToSOAPDateTime(aDateTime : TDateTime) : string;

function ExtractServerURL(const aFullURL : string) : string;

function SOAPStrToFloat(const aString: string): Extended;

function AddXMLChildNode(aParent : IXMLNode; const aName : string) : IXMLNode;
procedure AddXMLAttribute(aNode : IXMLNode; aName, aValue : string);
procedure AddXMLTextValue(aNode : IXMLNode; aValue : string);

function FindChildNode(aParent : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
function FindChildNodeByAttribute(aParent : IXMLNode; const anAttributeName, anAttributeValue : string) : IXMLNode;
function FindParentNode(aNode : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
function FindAttribute(aNode : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
function GetXMLTextValue(aNode : IXMLNode) : string;

implementation

uses uRORes, SysUtils, uROCompression;

function ObjectToXML(anObject : TROComplexType;
                     const anObjectName : string = '') : string;
var objname : string;
    doc : IXMLDocument;
begin
  result := '';
  
  if (anObjectName='') then objname := 'Object'
                       else objname := anObjectName;

  doc := CreateXMLDoument;
  doc.New(anObject.ClassName);

  with TROXMLSerializer.Create(pointer(doc.DocumentNode)) do try
    SerializationOptions := [xsoSendUntyped];
    Write(anObject.ClassName, anObject.ClassInfo, anObject);

    result := doc.DocumentNode.XML;
  finally
    Free;
  end;
end;

function XMLToObject(const someXML : string) : TROComplexType;
var clsname : string;
    cls : TROComplexTypeClass;
    doc : IXMLDocument;
    ss : TStringStream;
begin
  ss := TStringStream.Create(someXML);
  ss.Position := 0;
  
  doc := CreateXMLDoument;
  doc.New;
  doc.LoadFromStream(ss);

  clsname := doc.DocumentNode.Name;
  cls := FindROClass(clsname);

  if (cls=NIL) then RaiseError(err_UnknownClass, [clsname]);

  with TROXMLSerializer.Create(pointer(doc.DocumentNode)) do try
    Read(clsname, cls.ClassInfo, result);
  finally
    Free;
  end;
end;

procedure SplitName(const aName : string; out aNameSpace, aLocalName : string);
var idx : integer;
begin
  aNameSpace := '';
  if (aName='') then begin
    aLocalName := '';
    Exit;
  end
  else aLocalName := aName;

  idx := Pos(':', aName);
  if (idx>0) then begin
    aNameSpace := Copy(aName, 1, idx-1);
    aLocalName := Copy(aName, idx+1, Length(aName)-idx);
  end;
end;

// Required for a bug in OpenXML which never assigns correctly LocalName
procedure SplitNodeName(const aNode : IXMLNode; out aNameSpace, aLocalName : string);
begin
//  SplitName(aNode.nodeName, aNameSpace, aLocalName)
  SplitName(aNode.Name, aNameSpace, aLocalName)
end;

function SOAPStrToFloat(const aString: string): Extended;
var e: integer;
begin
  Val(aString, Result, e);
  if (e<>0) then RaiseError(err_ErrorConvertingFloat, [aString, e]);
end;

function SOAPDateTimeToDateTime(const aSOAPDate : string) : TDateTime;
var year, month, day, hour, min, sec : word;
begin
  // This probabily will all change. See W3C specs for date/time
  case Length(aSOAPDate) of
    SOAP_DateFormatLength : begin {yyyy-mm-dd}
      year  := StrToInt(Copy(aSOAPDate,1,4));
      month := StrToInt(Copy(aSOAPDate,6,2));
      day   := StrToInt(Copy(aSOAPDate,9,2));
      result := EncodeDate(year, month, day);
    end;
    SOAP_TimeFormatLength : begin {hh:nn:ss}
      hour  := StrToInt(Copy(aSOAPDate,1,2));
      min   := StrToInt(Copy(aSOAPDate,4,2));
      sec   := StrToInt(Copy(aSOAPDate,7,2));
      result := EncodeTime(hour, min, sec, 0);
    end;
    else {SOAP_DateTimeFormatLength : } begin {yyyy-mm-ddThh:nn:ss}
      year  := StrToInt(Copy(aSOAPDate,1,4));
      month := StrToInt(Copy(aSOAPDate,6,2));
      day   := StrToInt(Copy(aSOAPDate,9,2));
      hour  := StrToInt(Copy(aSOAPDate,12,2));
      min   := StrToInt(Copy(aSOAPDate,15,2));
      sec   := StrToInt(Copy(aSOAPDate,18,2));
      result := EncodeDate(year, month, day)+EncodeTime(hour, min, sec, 0);
    end;
  end;
end;

function DateTimeToSOAPDateTime(aDateTime : TDateTime) : string;
begin
  result := FormatDateTime(SOAP_DateTimeFormat, aDateTime);
end;

function ExtractServerURL(const aFullURL : string) : string;
const ProtocolID = 'http://';
var p : integer;
begin
  result := Trim(StringReplace(aFullURL, ProtocolID, '', [rfReplaceAll, rfIgnoreCase]));
  p := Pos('/', result);  
  if (p>0)
    then result := ProtocolID+Copy(result, 1, p)
    else result := ProtocolID+result;
end;

function AddXMLChildNode(aParent : IXMLNode; const aName : string) : IXMLNode;
begin
  {result := aParent.ownerDocument.createElement(aName);
  aParent.appendChild(result);}
  result := aParent.Add(aName)
end;

procedure AddXMLAttribute(aNode : IXMLNode; aName, aValue : string);
begin
  {result := aNode.ownerDocument.createAttribute(aName);
  result.nodeValue := aValue;
  aNode.attributes.setNamedItem(result);}
  aNode.AddAttribute(aName, aValue);
end;

procedure AddXMLTextValue(aNode : IXMLNode; aValue : string);
begin
  //aNode.appendChild(aNode.ownerDocument.createTextNode(aValue));
  aNode.Value := aValue
end;

function FindParentNode(aNode : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
var ns, locname : string;
    parent : IXMLNode;
begin
  result := NIL;
  while (aNode.Parent<>NIL) do begin
    parent := aNode.Parent;

    if IsOnlyLocalName then begin
      SplitNodeName(parent, ns, locname);
      if (CompareText(locname, aName)=0) then begin
        result := parent;
        Exit;
      end;
    end
    else if (parent.Name=aName) then begin
      result := parent;
      Exit;
    end;

    aNode := parent;
  end;
end;

function FindChildNode(aParent : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
var i : integer;
    locname, ns : string;
    item : IXMLNode;
begin
  result := NIL;

  with aParent do
    for i := 0 to (childrenCount-1) do begin
      item := Children[i];

      if IsOnlyLocalName then begin
        SplitNodeName(item, ns, locname);
        if (locname=aName) then begin
          result := item;
          Exit;
        end;
      end
      else begin
        if (item.Name=aName) then begin
          result := item;
          Exit;
        end;
      end
    end
end;

function FindChildNodeByAttribute(aParent : IXMLNode; const anAttributeName, anAttributeValue : string) : IXMLNode;
var i : integer;
    //attr : IXMLNode;
    item, item2 : IXMLNode;
    //list : IXMLNodeList;
begin
  result := NIL;

  with aParent do
    for i := 0 to (ChildrenCount-1) do begin
      item := Children[i];
      item2 := item.GetNodeByAttribute(anAttributeName, anAttributeValue);

      if (item2<>NIL) then begin
        result := item;
        Exit;
      end;
    end
end;

function FindAttribute(aNode : IXMLNode; const aName : string; IsOnlyLocalName : boolean = FALSE) : IXMLNode;
//var i : integer;
    //locname, ns : string;
begin
  result := NIL;
  result := aNode.GetAttributeByName(aName);

  {with aNode do
    for i := 0 to (AttributeCount-1) do begin
      if IsOnlyLocalName then begin
        SplitNodeName(attributes.item(i), ns, locname);
        if (locname=aName) then begin
          result := attributes.item(i);
          Exit;
        end;
      end
      else begin
        if (attributes.item(i).nodeName=aName) then begin
          result := attributes.item(i);
          Exit;
        end;
      end
    end}
end;

function GetXMLTextValue(aNode : IXMLNode) : string;
//var textnode : IXMLNode;
begin
  //textnode := aNode.childNodes.item(0);
  {if Assigned(textnode) and (textnode.hasChildNodes) and (textnode.nodeName='#text')
    then result := textnode.textContent
    else result := '';}
  result := aNode.Value
end;

{ TROXMLSerializer }
constructor TROXMLSerializer.Create(aStorageRef: pointer);
begin
  inherited;

  fSerializationOptions := [xsoWriteMultiRefArray, xsoWriteMultiRefObject];
end;

{$IFDEF DELPHI5}
function Supports(const Instance: IUnknown; const Intf: TGUID): Boolean; overload;
var lDummyInst:IUnknown;
begin
  result := Supports(Instance, Intf, lDummyInst);
end;
{$ENDIF DELPHI5}

function TROXMLSerializer.SetStorageRef(aStorageRef : pointer) : boolean;
begin
  result := Supports(IUnknown(aStorageRef), IXMLNode);
  fBodyNode := NIL;
  fMaxRef := 0;
  if result
    then fNode := IXMLNode(aStorageRef);
end;

procedure TROXMLSerializer.EndReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; const LevelRef : IUnknown);
begin
  fNode := IXMLNode(LevelRef);
end;

procedure TROXMLSerializer.EndWriteObject(const aName: string;
  aClass : TClass; anObject: TObject; const LevelRef : IUnknown);
begin
  fNode := IXMLNode(LevelRef);
end;

procedure TROXMLSerializer.ReadDateTime(const aName: string; var Ref; ArrayElementId : integer = -1);
var subnode : IXMLNode;
begin
  subnode := FindChildNode(fNode, aName, TRUE);
  if (subnode<>NIL)
    then TDateTime(Ref) := SOAPDateTimeToDateTime(GetXMLTextValue(subnode))
    //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadEnumerated(const aName: string;
  anEnumTypeInfo: PTypeInfo; var Ref; ArrayElementId : integer = -1);
var subnode : IXMLNode;
    x : byte;
    s : string;
begin
  subnode := FindChildNode(fNode, aName, TRUE);
  if (subnode<>NIL)
    then begin
      s := GetXMLTextValue(subnode);
      x := GetEnumValue(anEnumTypeInfo, s);
      byte(Ref) := x;
    end;
    //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadFloat(const aName: string;
  aFloatType: TFloatType; var Ref; ArrayElementId : integer = -1);
var subnode : IXMLNode;
begin
  subnode := FindChildNode(fNode, aName, TRUE);
  if (subnode<>NIL) then
    case aFloatType of
      ftSingle : single(Ref) := SOAPStrToFloat(GetXMLTextValue(subnode));
      ftDouble : double(Ref) := SOAPStrToFloat(GetXMLTextValue(subnode));
      ftExtended : extended(Ref) := SOAPStrToFloat(GetXMLTextValue(subnode));
      ftComp : comp(Ref) := SOAPStrToFloat(GetXMLTextValue(subnode));
      ftCurr : currency(Ref) := SOAPStrToFloat(GetXMLTextValue(subnode));
    end
  //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadInt64(const aName: string; var Ref; ArrayElementId : integer = -1);
var subnode : IXMLNode;
begin
  subnode := FindChildNode(fNode, aName, TRUE);
  if (subnode<>NIL)
    then int64(Ref) := StrToInt(GetXMLTextValue(subnode))
    //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadInteger(const aName: string;
  anOrdType: TOrdType; var Ref; ArrayElementId : integer = -1);
var subnode : IXMLNode;
begin
  if (ArrayElementId>=0)
    then subnode := fNode.Children[ArrayElementId]
    else subnode := FindChildNode(fNode, aName, TRUE);

  if (subnode<>NIL) then
    case anOrdType of
      otSByte,
      otUByte : byte(Ref) := StrToInt(GetXMLTextValue(subnode));
      otSWord,
      otUWord : word(Ref) := StrToInt(GetXMLTextValue(subnode));
      otSLong,
      otULong : integer(Ref) := StrToInt(GetXMLTextValue(subnode));
    end
  //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadString(const aName: string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1);
var subnode : IXMLNode;
begin
  //ToDo: honor iMaxLength

  if (ArrayElementId>=0)
    then subnode := fNode.Children[ArrayElementId]
    else subnode := FindChildNode(fNode, aName, TRUE);

  if (subnode<>NIL)
    then string(Ref) := GetXMLTextValue(subnode)
    //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.ReadWideString(const aName: string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1);
var subnode : IXMLNode;
begin
  //ToDo: honor iMaxLength

  if (ArrayElementId>=0)
    then subnode := fNode.Children[ArrayElementId]
    else subnode := FindChildNode(fNode, aName, TRUE);

  if (subnode<>NIL)
    then widestring(Ref) := GetXMLTextValue(subnode)
    //else RaiseError(err_ParameterNotFound, [aName]);
end;

procedure TROXMLSerializer.WriteDateTime(const aName: string; const Ref; ArrayElementId : integer = -1);
var newnode : IXMLNode;
begin
  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:dateTime');
  AddXMLTextValue(newnode, DateTimeToSOAPDateTime(TDateTime(Ref)));
end;

procedure TROXMLSerializer.WriteEnumerated(const aName: string;
  anEnumTypeInfo: PTypeInfo; const Ref; ArrayElementId : integer = -1);
var newnode : IXMLNode;
    isbool : boolean;
    val : string;
begin
  newnode := AddXMLChildNode(fNode, aName);
  isbool := (anEnumTypeInfo.Name='Boolean');

  if not (xsoSendUntyped in SerializationOptions) then begin
    if isbool
      then AddXMLAttribute(newnode, 'xsi:type', 'xsd:boolean')
      else AddXMLAttribute(newnode, 'xsi:type', ns_Custom+':'+anEnumTypeInfo^.Name);
  end;

  val := GetEnumName(anEnumTypeInfo, Ord(byte(Ref)));
  if isbool then val := LowerCase(val);

  AddXMLTextValue(newnode, val); // TODO: check enums bigger than a byte!
end;

procedure TROXMLSerializer.WriteFloat(const aName: string;
  aFloatType: TFloatType; const Ref; ArrayElementId : integer = -1);
var src : pointer;
    text, dtype : string;
    newnode : IXMLNode;
begin
  src := @Ref;
  case aFloatType of
    ftSingle   : begin
      text := FloatToStr(single(src^));
      dtype := 'float';
    end;
    ftDouble   : begin
      text := FloatToStr(double(src^));
      dtype := 'double';
    end;
    ftExtended : begin
      text := FloatToStr(extended(src^));
      dtype := 'double';
    end;
    ftComp     : begin
      text := FloatToStr(comp(src^));
      dtype := 'double';
    end;
    ftCurr     : begin
      text := FloatToStr(currency(src^));
      dtype := 'double';
    end;
  end;

  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:'+dtype);
  AddXMLTextValue(newnode, text);
end;

procedure TROXMLSerializer.WriteInt64(const aName: string; const Ref; ArrayElementId : integer = -1);
var src : pointer;
    text : string;
    newnode : IXMLNode;
begin
  src := @Ref;
  text := IntToStr(int64(src));

  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:long');
  AddXMLTextValue(newnode, text);
end;

procedure TROXMLSerializer.WriteInteger(const aName: string;
  anOrdType: TOrdType; const Ref; ArrayElementId : integer = -1);
var src : pointer;
    text, dtype : string;
    newnode : IXMLNode;
begin
  src := @Ref;
  case anOrdType of
    otSByte : begin
      text := IntToStr(shortint(src^));
      dtype := 'byte';
    end;
    otUByte : begin
      text := IntToStr(byte(src^));
      dtype := 'unsignedByte';
    end;
    otSWord : begin
      text := IntToStr(smallint(src^));
      dtype := 'short';
    end;
    otUWord : begin
      text := IntToStr(word(src^));
      dtype := 'unsignedShort';
    end;
    otSLong : begin
      text := IntToStr(integer(src^));
      dtype := 'int';
    end;
    otULong : begin
      text := IntToStr(integer(src^));
      dtype := 'unsignedInt';
    end;
  end;

  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:'+dtype);
  AddXMLTextValue(newnode, text);
end;

procedure TROXMLSerializer.WriteString(const aName: string; const Ref; ArrayElementId : integer = -1);
var newnode : IXMLNode;
begin
  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:string');
  AddXMLTextValue(newnode, string(Ref));
end;

procedure TROXMLSerializer.WriteWideString(const aName: string; const Ref; ArrayElementId : integer = -1);
var newnode : IXMLNode;
begin
  newnode := AddXMLChildNode(fNode, aName);
  if not (xsoSendUntyped in SerializationOptions) then
    AddXMLAttribute(newnode, 'xsi:type', 'xsd:string');
  AddXMLTextValue(newnode, widestring(Ref));
end;

procedure TROXMLSerializer.BeginReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
var cnt : integer;
    hrefattr : IXMLNode;
    id : string;
begin
  inherited;

  LevelRef := fNode;

  if (ArrayElementId>=0)
    then fNode := fNode.Children[ArrayElementId] // We don't know the name of the array item. Each SOAP vendor might have his own
    else fNode := FindChildNode(fNode, aName, TRUE);

  hrefattr := fNode.GetAttributeByName(tag_HRef);
  if (hrefattr<>NIL) then begin
    id := Copy(hrefattr.Value, 2, Length(hrefattr.Value)); // Removes the '#'
    fNode := FindChildNodeByAttribute(BodyNode, tag_Id, id);
  end;

  if aClass.InheritsFrom(TStream) then begin
    anObject := TMemoryStream.Create;
    IsValidType := TRUE;
  end
  else if IsValidType then begin
    anObject := TROComplexTypeClass(aClass).Create;

    if (anObject is TROArray) then begin
      cnt := fNode.ChildrenCount;
      TROArray(anObject).Resize(cnt);
    end;
  end;
end;

procedure TROXMLSerializer.BeginWriteObject(const aName: string;
  aClass : TClass; anObject: TObject; var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
var id : string;
    refnode : IXMLNode;
begin
  inherited;

  if aClass.InheritsFrom(TStream) then begin
    LevelRef := fNode;
    fNode := AddXMLChildNode(fNode, aName);
    if not (xsoSendUntyped in SerializationOptions) then
      AddXMLAttribute(fNode, 'xsi:type', 'xsi:'+dts_base64Binary);
    IsValidType := TRUE;
  end
  else if IsValidType then begin // It is then a TROComplexType

    if (xsoWriteMultiRefObject in SerializationOptions) then begin
      LevelRef := fNode;

      id := IntToStr(fMaxRef);
      Inc(fMaxRef);

      refnode := AddXMLChildNode(fNode, aName);
      AddXMLAttribute(refnode, tag_href, '#'+id);

      fNode := AddXMLChildNode(BodyNode, ns_Custom+':'+anObject.ClassName);
      AddXMLAttribute(fNode, tag_Id, id);
      if not (xsoSendUntyped in SerializationOptions) then
        AddXMLAttribute(fNode, 'xsi:type', ns_Custom+':'+anObject.ClassName);
    end
    
    else begin
      LevelRef := fNode;
      fNode := AddXMLChildNode(fNode, aName);
      if not (xsoSendUntyped in SerializationOptions) then
        AddXMLAttribute(fNode, 'xsi:type', ns_Custom+':'+anObject.ClassName);
    end
  end;
end;

procedure TROXMLSerializer.CustomReadObject(const aName: string;
  aClass : TClass; var Ref; ArrayElementId: integer);
var obj : TObject absolute Ref;
    ss : TStringStream;
begin
  inherited;

  ss := NIL;
  if (obj is TStream) then try // Created as TMemoryStream in BeginReadObject
    ss := TStringStream.Create(fNode.Value);
    DecodeStream(ss, TMemoryStream(obj));
    TMemoryStream(obj).Position := 0;
  finally
    ss.Free;
  end;
end;

procedure TROXMLSerializer.CustomWriteObject(const aName: string;
  aClass : TClass; const Ref; ArrayElementId : integer = -1);
var obj : TObject absolute Ref;
    ss : TStringStream;
begin
  inherited;

  ss := NIL;
  if (obj is TStream) then try
    ss := TStringStream.Create('');
    TStream(obj).Position := 0;
    EncodeStream(TStream(obj), ss);
    AddXMLTextValue(fNode, ss.DataString);
  finally
    ss.Free;
  end;
end;

function TROXMLSerializer.BodyNode: IXMLNode;
begin
  if (fBodyNode=NIL) then fBodyNode := FindParentNode(fNode, tag_Body, TRUE);
  result := fBodyNode;
end;

end.
