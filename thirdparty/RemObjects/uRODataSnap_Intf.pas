unit uRODataSnap_Intf;

interface

uses Classes, TypInfo, uROProxy, uROTypes, uROClientIntf;

const
  LibraryUID = '{9EC7C50C-DAC2-48A9-9A0F-CBAA29A11EF7}';


type
   { Forward declarations }
   IAppServer = interface;
   TProviderNames = class;

   { Enumerateds }
   { TProviderNames }
   TProviderNames = class(TROArray)
   private
     fItems : array of string;
   protected
     function GetItems(Index : integer) : string;
     procedure SetItems(Index : integer; const Value : string);

   public
     class function GetItemType : PTypeInfo; override;
     class function GetItemClass : TROComplexTypeClass; override;
     class function GetItemSize : integer; override;
     function GetItemRef(Index : integer) : pointer; override;
     procedure Clear; override;
     procedure Resize(ElementCount : integer); override;
     function GetCount : integer; override;

     procedure Add(Value : string);

     property Items[Index : integer] : string read GetItems write SetItems; default;

   end;


   { IAppServer }
   IAppServer = interface
      ['{1AEFCC20-7A24-11D2-98B0-C69BEB4B5B6D}']
      function AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary;
      function AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary;
      function AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary;
      function AS_GetProviderNames: TProviderNames;
      function AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary;
      function AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary;
      procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String);
   end;

   { CoIAppServer }
   CoIAppServer = class
      class function Create(const aMessage : IROMessage; aTransportChannel : IROTransportChannel) : IAppServer;
   end;


implementation

uses SysUtils;

class function TProviderNames.GetItemType : PTypeInfo;
begin
  result := TypeInfo(string);
end;

class function TProviderNames.GetItemClass : TROComplexTypeClass;
begin
  result := NIL
end;

class function TProviderNames.GetItemSize : integer;
begin
  result := SizeOf(string);
end;

function TProviderNames.GetItems(Index : integer) : string;
begin
  result := fItems[Index];
end;

function TProviderNames.GetItemRef(Index : integer) : pointer;
begin
  result := @fItems[Index];
end;

procedure TProviderNames.Clear;
begin
  SetLength(fItems, 0);
end;

procedure TProviderNames.SetItems(Index : integer; const Value : string);
begin
  fItems[Index] := Value;
end;

procedure TProviderNames.Resize(ElementCount : integer);
begin
  SetLength(fItems, ElementCount);
end;

function TProviderNames.GetCount: integer;
begin
  result := Length(fItems);
end;

procedure TProviderNames.Add(Value : string);
begin
  SetLength(fItems, Length(fItems)+1);
  fItems[Length(fItems)-1] := Value;
end;


type
   TIAppServer_Proxy = class(TROProxy, IAppServer)
   private
   protected
      function AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary;
      function AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary;
      function AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary;
      function AS_GetProviderNames: TProviderNames;
      function AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary;
      function AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary;
      procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String);
   end;

class function CoIAppServer.Create(const aMessage : IROMessage; aTransportChannel : IROTransportChannel) : IAppServer;
begin
  result := TIAppServer_Proxy.Create(aMessage, aTransportChannel);
end;

function TIAppServer_Proxy.AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_ApplyUpdates');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('Delta', TypeInfo(Binary), Delta, []);
     __Message.Write('MaxErrors', TypeInfo(Integer), MaxErrors, []);
     __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('ErrorCount', TypeInfo(Integer), ErrorCount, []);
     __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Read('Result', TypeInfo(binary), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

function TIAppServer_Proxy.AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_GetRecords');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('Count', TypeInfo(Integer), Count, []);
     __Message.Write('Options', TypeInfo(Integer), Options, []);
     __Message.Write('CommandText', TypeInfo(WideString), CommandText, []);
     __Message.Write('Params', TypeInfo(Binary), Params, []);
     __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('RecsOut', TypeInfo(Integer), RecsOut, []);
     __Message.Read('Params', TypeInfo(Binary), Params, []);
     __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Read('Result', TypeInfo(binary), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

function TIAppServer_Proxy.AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_DataRequest');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('Data', TypeInfo(Binary), Data, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('Result', TypeInfo(binary), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

function TIAppServer_Proxy.AS_GetProviderNames: TProviderNames;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_GetProviderNames');
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('Result', TypeInfo(TProviderNames), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

function TIAppServer_Proxy.AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_GetParams');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Read('Result', TypeInfo(binary), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

function TIAppServer_Proxy.AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary;
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_RowRequest');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('Row', TypeInfo(Binary), Row, []);
     __Message.Write('RequestType', TypeInfo(Integer), RequestType, []);
     __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Read('Result', TypeInfo(binary), result, []);
  finally
    __request.Free;
    __response.Free;
  end
end;

procedure TIAppServer_Proxy.AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String);
var __request, __response : TMemoryStream;
begin
   __request := TMemoryStream.Create;
   __response := TMemoryStream.Create;

   try
     __Message.Initialize(__TransportChannel, 'IAppServer', 'AS_Execute');
     __Message.Write('ProviderName', TypeInfo(WideString), ProviderName, []);
     __Message.Write('CommandText', TypeInfo(WideString), CommandText, []);
     __Message.Write('Params', TypeInfo(Binary), Params, []);
     __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
     __Message.Finalize;

     __Message.WriteToStream(__request);
     __TransportChannel.Dispatch(__request, __response);
     __Message.ReadFromStream(__response);

     __Message.Read('Params', TypeInfo(Binary), Params, []);
     __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);
  finally
    __request.Free;
    __response.Free;
  end
end;


initialization
  RegisterROClass(TProviderNames);

end.
