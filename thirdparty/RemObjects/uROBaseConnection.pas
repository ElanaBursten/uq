unit uROBaseConnection;                 { TROBaseConnection component. }

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF RemObjects_UseEncryption}uROEncryption,{$ENDIF}
  SysUtils, Classes;

type
  TROBaseConnection = class(TComponent)
  private
    { Private declarations }
    //fInternalEncryption: boolean;
    FEncryption: TROEncryption;
    procedure SetEncryption(NewValue: TROEncryption);
  protected
    { Protected declarations }
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoEncryption(iPlainText, iCipherText: TStream); virtual;
    procedure DoDecryption(iCipherText, iPlainText: TStream); virtual;
  published
    { Published properties and events }
    property Encryption: TROEncryption read FEncryption write SetEncryption;
  end;                                  { TROBaseConnection }

implementation

procedure TROBaseConnection.DoEncryption(iPlainText, iCipherText: TStream); { public }
begin
  iPlainText.Seek(0,soFromBeginning);
  if Assigned(Encryption) then
    Encryption.Encrypt(iPlainText, iCipherText)
  else
    if iPlainText.Size > 0 then
      iCipherText.CopyFrom(iPlainText, iPlainText.Size);
end;                                    { DoEncryption }

procedure TROBaseConnection.DoDecryption(iCipherText, iPlainText: TStream); { public }
begin
  {ToDo: we should find a solution where if no encryption is used, we don't need to
         copy the stream AT ALL. }
  iCipherText.Seek(0,soFromBeginning);
  if Assigned(Encryption) then
    Encryption.Decrypt(iCipherText, iPlainText)
  else
    if iCipherText.Size > 0 then
      iPlainText.CopyFrom(iCipherText, iCipherText.Size);
end;                                    { DoDecryption }

procedure TROBaseConnection.SetEncryption(NewValue: TROEncryption);
begin
  fEncryption.Assign(NewValue);
  {if FEncryption <> NewValue then begin

    if fInternalEncryption then FreeAndNil(fEncryption);
    fInternalEncryption := false;

    FEncryption := NewValue;
    if Assigned(NewValue) then begin
      NewValue.FreeNotification(Self);
    end
    else begin
      fEncryption := TROEncryption.Create(self);
      fEncryption.Name := 'Encryption';
      fEncryption.SetSubComponent(true);
      fInternalEncryption := true;
    end;
  end;}
end;                                    { SetEncryption }

procedure TROBaseConnection.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  {if Operation = opRemove then begin
    if aComponent = FEncryption then FEncryption := nil;
  end;}
end;                                    { Notification }

destructor TROBaseConnection.Destroy;
begin
  { ToDo -cCDK: Free allocated memory and created objects here. }
  inherited Destroy;
  FreeAndNil(fEncryption);
end;                                    { Destroy }

constructor TROBaseConnection.Create(aOwner: TComponent);
{ Creates an object of type TROBaseConnection, and initializes properties. }
begin
  inherited Create(aOwner);
  fEncryption := TROEncryption.Create();//(nil);
  //fEncryption.SetSubComponent(true);
  //fInternalEncryption := true;
  { ToDo -cCDK: Add your initialization code here. }
end;                                    { Create }

initialization
finalization
end.

