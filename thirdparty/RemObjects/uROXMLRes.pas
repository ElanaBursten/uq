unit uROXMLRes;

interface

resourcestring
  err_NotImplemented   = 'Not implemented';
  err_DOMElementIsNIL  = 'DOMElement is NIL';
  err_IndexOutOfBounds = 'Index out of bounds';
  err_CannotLoadXMLDocument = 'Cannot load XML document.'#13'Reason: %s'#13'Line: %d'#13'Position: %d';

procedure RaiseError(const anErrorMessage : string; const someParams : array of const);

implementation

uses SysUtils;

procedure RaiseError(const anErrorMessage : string; const someParams : array of const);
begin
  raise Exception.CreateFmt(anErrorMessage, someParams);
end;

end.
