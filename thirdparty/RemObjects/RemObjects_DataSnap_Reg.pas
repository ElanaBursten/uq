unit RemObjects_DataSnap_Reg;

{$I RemObjects.inc}

interface

procedure Register;

implementation

uses Classes,
     {$IFDEF DELPHI6UP}DesignIntf, DesignEditors, DMForm, {$ELSE} DsgnIntf, DmDesigner, {$ENDIF}
     uRORes,
     uRODataSnapModule,
     uRODataSnapConnection, uRODataSnapProviderPublisher;

procedure Register;
begin
  RegisterComponents(str_ProductName,[TRODataSnapConnection,
                                      TRODataSnapProviderPublisher]);

  {$IFDEF DELPHI6UP}
  RegisterCustomModule(TRODataSnapModule,TCustomModule);
  {$ELSE}
  RegisterCustomModule(TRODataSnapModule,TDataModuleDesignerCustomModule);
  {$ENDIF}

end;

end.
