unit uRODataSnapBaseAppServer;

interface

uses Classes, Provider,
     Windows, { for InterlocIncrement/Decrement }
     uROClientIntf, uROServer, uROServerIntf, uRODataSnap_Intf;

type TRODataSnapBaseAppServer = class(TDataModule, IUnknown, uRODataSnap_Intf.IAppServer)
     private
       fRefCount: Integer;
       function AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary;
       function AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary;
       function AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary;
       function AS_GetProviderNames: TProviderNames;
       function AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary;
       function AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary;
       procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String);

       { we need to reimplement AdRef/Release to provide reference counting }
       function _AddRef: Integer; stdcall;
       function _Release: Integer; stdcall;
       function IUnknown._AddRef = _AddRef;
       function IUnknown._Release = _Release;

     protected
       function GetProviderByName(const iProviderName:string):TCustomProvider; virtual; abstract;
       function GetProviderNames:TProviderNames; virtual; abstract;
     end;

implementation

uses SysUtils,
     {$IFDEF DEBUG_REMOBJECTS_DATASNAP}eDebugServer,{$ENDIF}
     uRODataSnap_Invk,
     uROBinaryHelpers;

function TRODataSnapBaseAppServer.AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary;
var lProvider:TCustomProvider;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_ApplyUpdates(ProviderName=%s,MaxErrors=%d)',[ProviderName,MaxErrors]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  lProvider := GetProviderByName(ProviderName);
  result := BinaryFromVariant(lProvider.ApplyUpdates(VariantFromBinary(Delta),MaxErrors,ErrorCount));

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_ApplyUpdates(ErrorCount=%s)',[ErrorCount]); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer.AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary;
var lProvider:TCustomProvider;
    lOwnerData,lParams:OleVariant;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_GetRecords(ProviderName=%s,Count=%d,Options=%d,CommandText=%s)',[string(ProviderName),Count,Options,CommandText]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  lProvider := GetProviderByName(ProviderName);
  //result := BinaryFromVariant(lProvider.GetRecords(Count, RecsOut, Options));
  lOwnerData := OwnerData;
  lParams := VariantFromBinary(Params);
  result := BinaryFromVariant(lProvider.GetRecords(Count, RecsOut, Options, CommandText, lParams, lOwnerData));
  FreeAndNil(Params);
  Params := BinaryFromVariant(lParams);
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_GetRecords(RecsOut=%d)',[RecsOut]); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer.AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary;
var lProvider:TCustomProvider;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_DataRequest(ProviderName=%s)',[ProviderName]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  lProvider := GetProviderByName(ProviderName);
  result := BinaryFromVariant(lProvider.DataRequest(VariantFromBinary(Data)));

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_DataRequest()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer.AS_GetProviderNames: TProviderNames;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_GetProviderNames()'); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  result := GetProviderNames();

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_GetProviderNames()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer.AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary;
var lProvider:TCustomProvider;
    lOwnerData:OleVariant;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_GetParams(ProviderName=%s)',[ProviderName]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  lProvider := GetProviderByName(ProviderName);
  lOwnerData := OwnerData;
  result := BinaryFromVariant(lProvider.GetParams(lOwnerData));
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_GetParams()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer.AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary;
var lProvider:TCustomProvider;
    lOwnerData:OleVariant;
begin
  lProvider := GetProviderByName(ProviderName);
  lOwnerData := OwnerData;
  result := BinaryFromVariant(lProvider.RowRequest(VariantFromBinary(Row),RequestType,lOwnerData));
  OwnerData := lOwnerData;
end;

procedure TRODataSnapBaseAppServer.AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String);
var lProvider:TCustomProvider;
    lParams,lOwnerData:OleVariant;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'IAppServer.AS_Execute(ProviderName=%s,CommandText=%s)',[string(ProviderName),CommandText]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  lProvider := GetProviderByName(ProviderName);
  lOwnerData := OwnerData;
  lParams := VariantFromBinary(Params);
  lProvider.Execute(Commandtext,lParams,lOwnerData);
  FreeAndNil(Params);
  Params := BinaryFromVariant(lParams);
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'IAppServer.AS_Execute()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TRODataSnapBaseAppServer._AddRef: Integer;
begin
  if not (csDesigning in ComponentState) then begin
    result := InterlockedIncrement(fRefCount);
  end;
end;

function TRODataSnapBaseAppServer._Release: Integer;
begin
  if not (csDesigning in ComponentState) then begin
    result := InterlockedDecrement(fRefCount);
    if result = 0 then Destroy;
  end;
end;

end.
