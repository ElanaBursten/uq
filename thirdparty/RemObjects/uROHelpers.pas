unit uROHelpers;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

function NewGuid:TGUID;
function NewGuidAsString:string;

implementation

{$IFDEF MSWINDOWS}
uses ActiveX, ComObj;
{$ENDIF MSWINDOWS}
{$IFDEF LINUX}
uses SysUtils;
{$ENDIF}

function NewGuid:TGUID;
begin
  {$IFDEF MSWINDOWS}
  CoCreateGuid(result);
  {$ENDIF MSWINDOWS}
  {$IFDEF LINUX}
  CreateGuid(result);
  {$ENDIF}
end;

function NewGuidAsString:string;
begin
  result := GuidToString(NewGuid());
end;

end.
