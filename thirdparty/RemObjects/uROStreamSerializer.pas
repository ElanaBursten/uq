{ Summary: Contains the class TROStreamSerializer }
unit uROStreamSerializer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

{$IFDEF DOTNET}
{$MESSAGE error 'This unit will not be used in .NET, use RemObjects.SDK.StreamSerializer instead' }
{$ENDIF}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DEBUG_REMOBJECTS}eDebugServer,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.Classes,
  Borland.Delphi.SysUtils,
  RemObjects.SDK.Types,
  RemObjects.SDK.Serializer;
  {$ELSE}
  Classes, SysUtils, TypInfo,
  uROTypes, uROSerializer;
  {$ENDIF}


type {------------ TROStreamSerializer ------------}

     { Summary: Stream serializer class
       Description:
         Stream serializer class. Provides the implementation for the methods introduced by TROSerliazer
         and formats types in the proprietary binary RemObjects format.
         Most types are serialized as pure data, so for instance an integer would take 4 bytes.
         Strings are serialized as an integer indicating the string size and then the actual text.
         See the implementation of the methods for further details.  
     }

     TROStreamSerializer = class(TROSerializer)
     private
       fStream : TStream;

     protected
       { Writers }

       procedure WriteInteger(const aName : string; anOrdType : TOrdType; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteInt64(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteString(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteWideString(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteDateTime(const aName : string; const Ref; ArrayElementId : integer = -1); override;
       procedure WriteFloat(const aName : string; aFloatType : TFloatType; const Ref; ArrayElementId : integer = -1); override;

       procedure BeginWriteObject(const aName: string; aClass : TClass; anObject: TObject;
                                  var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1); override;
       procedure EndWriteObject(const aName: string; aClass : TClass; anObject: TObject; const LevelRef : IUnknown); override;

       procedure CustomWriteObject(const aName : string; aClass : TClass; const Ref; ArrayElementId : integer = -1); override;

       { Readers }
       {$IFDEF DEBUG_REMOBJECTS}
       procedure Read(const aName : string; aTypeInfo : PTypeInfo; var Ptr; ArrayElementId : integer = -1); override;
       {$ENDIF DEBUG_REMOBJECTS}

       procedure ReadInteger(const aName : string; anOrdType : TOrdType; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadInt64(const aName : string; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); override;
       procedure ReadWideString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); override;
       procedure ReadDateTime(const aName : string; var Ref; ArrayElementId : integer = -1); override;
       procedure ReadFloat(const aName : string; aFloatType : TFloatType; var Ref; ArrayElementId : integer = -1); override;

       procedure BeginReadObject(const aName : string; aClass : TClass; var anObject : TObject;
                                 var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1); override;
       procedure EndReadObject(const aName : string; aClass : TClass; var anObject : TObject; const LevelRef : IUnknown); override;

       procedure CustomReadObject(const aName : string; aClass : TClass; var Ref; ArrayElementId : integer = -1); override;

     public
       function SetStorageRef(aStorageRef: pointer) : boolean; override;

     end;

procedure ObjectToStream(anObject : TROComplexType; aStream : TStream);
function StreamToObject(aStream : TStream) : TROComplexType;

function StreamToVariant(Stream: TStream): OleVariant;
procedure VariantToStream(const Data: OleVariant; Stream: TStream);

implementation

uses {$IFDEF VER140UP} Variants,{$ENDIF}
     uRORes, uROClientIntf, uROStreamHelpers;


function StreamToVariant(Stream: TStream): OleVariant;
var p: Pointer;
begin
  Result := VarArrayCreate([0, Stream.Size - 1], varByte);
  p := VarArrayLock(Result);
  try
    Stream.Position := 0; //start from beginning of stream
    Stream.Read(p^, Stream.Size);
  finally
    VarArrayUnlock(Result);
  end;
end;

procedure VariantToStream(const Data: OleVariant; Stream: TStream);
var p: Pointer;
begin
  p := VarArrayLock(Data);
  try
    Stream.Write(p^, VarArrayHighBound(Data, 1) + 1); //assuming low bound = 0
  finally
    VarArrayUnlock(Data);
  end;
end;

procedure ObjectToStream(anObject : TROComplexType; aStream : TStream);
var clsname : string;
begin
  with TROStreamSerializer.Create(aStream) do try
    clsname := anObject.ClassName;

    Write('', TypeInfo(string), clsname);
    Write('', anObject.ClassInfo, anObject);
  finally
    Free;
  end;
end;

function StreamToObject(aStream : TStream) : TROComplexType;
var clsname : string;
    cls : TROComplexTypeClass;
begin
  with TROStreamSerializer.Create(aStream) do try
    Read('', TypeInfo(string), clsname);
    cls := FindROClass(clsname);
    if (cls=NIL) then RaiseError(err_UnknownClass, [clsname]);
    Read('', cls.ClassInfo, result);
  finally
    Free;
  end;
end;

{ TROStreamSerializer }

function TROStreamSerializer.SetStorageRef(aStorageRef: pointer) : boolean;
begin
  result := TObject(aStorageRef) is TStream;
  if result then fStream := TStream(aStorageRef);
end;

procedure TROStreamSerializer.EndReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; const LevelRef : IUnknown);
begin
  inherited;
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.EndReadObject(name=%s, position before =$%x',[aName, fStream.Position]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

end;

procedure TROStreamSerializer.EndWriteObject(const aName: string;
  aClass : TClass; anObject: TObject; const LevelRef : IUnknown);
begin
  inherited;
end;

procedure TROStreamSerializer.ReadDateTime(const aName: string; var Ref; ArrayElementId : integer = -1);
begin
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadDateTime(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,SizeOf(TDateTime)]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(Ref, SizeOf(TDateTime));
end;

procedure TROStreamSerializer.ReadEnumerated(const aName: string;
  anEnumTypeInfo: PTypeInfo; var Ref; ArrayElementId : integer = -1);
begin
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadEnumerated(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,SizeOf(byte)]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(Ref, SizeOf(byte));
end;

procedure TROStreamSerializer.ReadFloat(const aName: string;
  aFloatType: TFloatType; var Ref; ArrayElementId : integer = -1);
var sze : byte;
    src : pointer;
begin
  src := @Ref;
  sze := 0;
  case aFloatType of
    ftSingle : sze := SizeOf(single);
    ftDouble : sze := SizeOf(double);
    ftExtended : sze := SizeOf(extended);
    ftComp : sze := SizeOf(comp);
    ftCurr :sze := SizeOf(currency);
  end;

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadFloat(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,sze]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(src^, sze);
end;

procedure TROStreamSerializer.ReadInt64(const aName: string; var Ref; ArrayElementId : integer = -1);
begin
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadInt64(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,SizeOf(Int64)]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(Ref, SizeOf(Int64));
end;

procedure TROStreamSerializer.ReadInteger(const aName: string;
  anOrdType: TOrdType; var Ref; ArrayElementId : integer = -1);
var sze : byte;
    src : pointer;
begin
  src := @Ref;
  sze := 0;
  case anOrdType of
    otSByte,
    otUByte : sze := SizeOf(byte);
    otSWord,
    otUWord : sze := SizeOf(word);
    otSLong,
    otULong : sze := SizeOf(integer);
  end;
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadInteger(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,sze]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}
  fStream.Read(src^, sze);
end;

procedure TROStreamSerializer.ReadString(const aName: string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1);
var sze : integer;
begin

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadString 1(name=%s, position before =$%x',[aName, fStream.Position]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(sze, SizeOf(sze));
  //ToDo: we need some code here to avoid hacker attachs with too long strings

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadString 2(name=%s, position before =$%x, size=$%x',[aName, fStream.Position,sze]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  if ((iMaxLength > -1) and (sze > iMaxLength)) or
     (sze > fStream.Size) then
    RaiseError(err_InvalidStringLength,[sze]);

  if (sze>0) then begin
    SetLength(string(Ref), sze);
    fStream.Read(string(Ref)[1], sze);
  end
  else string(Ref) := '';
end;

procedure TROStreamSerializer.ReadWideString(const aName: string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1);
var sze : integer;
begin
  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadWideString 1(name=%s, position before =$%x',[aName, fStream.Position]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  fStream.Read(sze, SizeOf(sze));

  if ((iMaxLength > -1) and (sze > iMaxLength)) or
     (sze*2 > fStream.Size) then
    RaiseError(err_InvalidStringLength,[sze]);

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.Write('TROStreamSerializer.ReadWideString 2(name=%s, position before =$%x, size=$%x, length=$%x',[aName, fStream.Position,sze,sze*2]);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  if (sze>0) then begin
    SetLength(widestring(Ref), sze);
    fStream.Read(widestring(Ref)[1], sze*2);
  end
  else widestring(Ref) := '';
end;

procedure TROStreamSerializer.WriteDateTime(const aName: string;
  const Ref; ArrayElementId : integer = -1); 
begin
  fStream.Write(Ref, SizeOf(TDateTime));
end;

procedure TROStreamSerializer.WriteEnumerated(const aName: string;
  anEnumTypeInfo: PTypeInfo; const Ref; ArrayElementId : integer = -1);
begin

  fStream.Write(Ref, SizeOf(byte));  // TODO: check enums bigger than a byte!
end;

procedure TROStreamSerializer.WriteFloat(const aName: string;
  aFloatType: TFloatType; const Ref; ArrayElementId : integer = -1);
var sze : byte;
    src : pointer;
begin
  src := @Ref;
  sze := 0;
  case aFloatType of
    ftSingle   : sze := SizeOf(single);
    ftDouble   : sze := SizeOf(double);
    ftExtended : sze := SizeOf(extended);
    ftComp     : sze := SizeOf(comp);
    ftCurr     : sze := SizeOf(currency);
  end;

  fStream.Write(src^, sze);
end;

procedure TROStreamSerializer.WriteInt64(const aName: string; const Ref; ArrayElementId : integer = -1);
begin
  fStream.Write(Ref, SizeOf(Int64));
end;

procedure TROStreamSerializer.WriteInteger(const aName: string;
  anOrdType: TOrdType; const Ref; ArrayElementId : integer = -1);
var sze : byte;
    src : pointer;
begin
  src := @Ref;
  sze := 0;  
  case anOrdType of
    otSByte,
    otUByte : sze := SizeOf(byte);
    otSWord,
    otUWord : sze := SizeOf(word);
    otSLong,
    otULong : sze := SizeOf(integer);
  end;

  fStream.Write(src^, sze);
end;

procedure TROStreamSerializer.WriteString(const aName: string; const Ref; ArrayElementId : integer = -1);
var sze : integer;
begin
  sze := Length(string(Ref));
  fStream.Write(sze, SizeOf(sze));
  if (sze>0) then fStream.Write(string(Ref)[1], sze);
end;

procedure TROStreamSerializer.WriteWideString(const aName: string;
  const Ref; ArrayElementId : integer = -1);
var sze : integer;
begin
  sze := Length(widestring(Ref));
  fStream.Write(sze, SizeOf(sze));
  if (sze>0) then fStream.Write(widestring(Ref)[1], sze*2);
end;

procedure TROStreamSerializer.BeginReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
var IsNIL : ByteBool;
    ctcls : TROComplexTypeClass;
    clsnme : string;
    cnt : integer;
begin
  inherited;

  fStream.Read(IsNIL, SizeOf(IsNIL));

  if not IsNIL then begin

    {$IFDEF DEBUG_REMOBJECTS}
    DebugServer.Write('fStream.Position before reading classname: $%x',[fStream.Position]);
    {$ENDIF DEBUG_REMOBJECTS}

    ReadString('', clsnme,-1,MAX_ITEM_NAME);

    if (clsnme=StreamClsName) then begin
      anObject := TMemoryStream.Create;
      IsValidType := TRUE;
    end
    else if IsValidType then begin
      ctcls := TROComplexTypeClass(aClass);
      anObject := ctcls.Create;

      if (anObject is TROArray) then begin
        ReadInteger('', otULong, cnt);
        TROArray(anObject).Resize(cnt);
      end;
    end
    else begin
      RaiseError(str_InvalidClassTypeInStream,[clsnme]);
    end;
  end
  else anObject := NIL;

  {$IFDEF DEBUG_REMOBJECTS}
  DebugServer.Write('fStream.Position: %d',[fStream.Position]);
  if anObject = nil then DebugServer.Write('anObject is nil') else DebugServer.Write('anObject is assigned');
  {$ENDIF DEBUG_REMOBJECTS}

end;

procedure TROStreamSerializer.BeginWriteObject(const aName: string; aClass : TClass; anObject: TObject;
   var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
var IsNIL : ByteBool;
    clsnme : string;
    cnt : integer;
begin
  inherited;

  IsNIL := (anObject=NIL);
  fStream.Write(IsNIL, SizeOf(IsNIL));
  if not IsNIL then begin
    if aClass.InheritsFrom(TStream) then begin
      clsnme := StreamClsName;
      WriteString('', clsnme);

      IsValidType := TRUE; // Adds TStream as supported type
    end

    else if IsValidType then begin // Is already a TROComplexType
      clsnme := anObject.ClassName;
      WriteString('', clsnme);

      if (anObject is TROArray) then begin
        cnt := TROArray(anObject).Count;
        WriteInteger('', otULong, cnt);
      end;
    end
  end;
end;

procedure TROStreamSerializer.CustomReadObject(const aName: string;
  aClass : TClass; var Ref; ArrayElementId: integer);
var obj : TObject absolute Ref;
    sze : integer;
begin
  inherited;

  if (obj is TMemoryStream) then with TMemoryStream(obj) do begin // Created as TMemoryStream in BeginReadObject
    ReadInteger('', otULong, sze);
    SetSize(sze);
    CopyFrom(fStream, sze);
    Position := 0;
  end;
end;

procedure TROStreamSerializer.CustomWriteObject(const aName: string;
  aClass : TClass; const Ref; ArrayElementId : integer = -1);
var obj : TObject absolute Ref;
    sze : integer;
begin
  inherited;

  if (obj is TStream) then with TStream(obj) do begin
    //sze := Size-Position; // Writes from the current position to the end

    sze := Size;
    Position := 0;
    fStream.Write(sze, SizeOf(sze));
    fStream.CopyFrom(TStream(obj), sze);
  end;
end;

{$IFDEF DEBUG_REMOBJECTS}
procedure TROStreamSerializer.Read(const aName: string;aTypeInfo: PTypeInfo; var Ptr; ArrayElementId: integer);
begin
  DebugServer.EnterMethod('TROStreamSerializer.Read("%s")',[aName]);
  try try
    DebugServer.WriteHexDump('Reading "'+aName+'" from message',fStream);
    inherited;
  except DebugServer.WriteException(); raise; end;
  finally
    DebugServer.ExitMethod('TROStreamSerializer.Read()');
  end;
end;
{$ENDIF DEBUG_REMOBJECTS}

end.
