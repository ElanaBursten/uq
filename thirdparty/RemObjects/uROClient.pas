{ Summary: Contains the class TROMessage from which all message classes inherit from and the functions to register message classes }
unit uROClient;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.Classes, Borland.Delphi.SysUtils,
  RemObjects.SDK.Serializer,
  {$ELSE}
  Classes, SysUtils, TypInfo, uROSerializer,
  {$ENDIF}
  uRORes, uROClientIntf;

type {---------------------- TROMessage ----------------------}

     { Summary: Event type declaration
       Description: Event type declaration. Properties TROMessage.OnReadStream and TROMessage.OnWriteStream are of this type. }
     TStreamOperation = procedure(aStream : TStream) of object;

     TExceptionEvent = procedure(anException : Exception) of object;

     { Summary: Base class for all message classes such as TROBINMessage and TROSOAPMessage
       Description
         Base message class all RemObjects message encoders inherit from. A message class contains two main properties
         (InterfaceName and MessageName), stores the values of all the parameters required by the message and is capable
         of streaming itself on a TStream. It implements the interfaces IROMessage and IROModuleInfo. Most of the methods
         of this class are abstract since they are meant to be overridden by the descendants. }
     TROMessage = class(TComponent, IUnknown, IROMessage, IROMessageCloneable, IROModuleInfo)
     private
       fSerializer : TROSerializer;
       fMessageName,
       fInterfaceName : string;
       fOnReadFromStream: TStreamOperation;
       fOnWriteToStream: TStreamOperation;
       fOnServerException: TExceptionEvent;

       fRefCount:integer;
       fReferenceCounted:boolean;
       fSupportSessions: boolean;
       fSessionID:TSessionID;
       procedure SetSessionID(const Value: TSessionID);

     protected
       constructor CreateRefCountedClone(iMessage:TROMessage); virtual;

       procedure ProcessException;
       function ReadException : Exception; virtual; abstract;

       {---------------------- Internals ----------------------}
       { Summary: Provides a way to customize the construction
         Description:
           Provides a way to customize the construction without forcing you to override the 3 constructors.
           Each constructor calls InitObject.
           !! Always !! put a call to the inherited InitObject from the descendants since this method calls CreateSerializer
           and initializes the serializer class }
       procedure InitObject; virtual;

       { Summary: Creates a TROSerializer instance used to read and write parameters
         Description:
           Creates a TROSerializer instance used to read and write parameters. Examples of TROSerializers are TROXMLSerializer
           and TROStreamSerializer. Different message classes can use the same serializers. For instance, if you created a
           TXMLMessage, you might want to reuse the class TROXMLSerializer and just replace the SOAP Envelope encoding.
           If you wanted to create a new binary message format you might want to reuse TROStreamSerializer and just add a bigger
           header to the message. }
       function CreateSerializer : TROSerializer; virtual; abstract;

       {---------------------- IROUnknown ----------------------}

       function _AddRef:integer; stdcall;
       function _Release:integer; stdcall;

       {---------------------- IROMessage ----------------------}

       procedure Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName : string); virtual;
       procedure Finalize; virtual;

       {$IFDEF DOTNET}
       {$ELSE}
       procedure Write(const aName : string; aTypeInfo : PTypeInfo; const Ptr; Attributes : TParamAttributes); virtual;
       procedure Read(const aName : string; aTypeInfo : PTypeInfo; var Ptr; Attributes : TParamAttributes); virtual;
       {$ENDIF}

       function GetMessageName : string;
       function GetInterfaceName : string;
       function GetSessionID: TGUID;


       procedure SetInterfaceName(const aValue : string);
       procedure SetMessageName(const aValue : string);

       procedure WriteToStream(aStream : TStream); virtual;
       procedure ReadFromStream(aStream : TStream); virtual;

       procedure WriteException(aStream : TStream; anException : Exception); virtual; abstract;

       {---------------------- IROModuleInfo ----------------------}

       procedure GetModuleInfo(aStream : TStream; const aTransport : IROTransport; var aFormat : TDataFormat); virtual;

       {---------------------- IROMessageClonable ----------------------}
       function Clone: IROMessage; 

     public
       { Summary: Constructs a TROMessage object
         Description: Constructs a TROMessage object and calls InitObject }
       constructor Create; reintroduce; overload; virtual;
       { Summary: Constructs a TROMessage object
         Description: Constructs a TROMessage object and calls InitObject }
       constructor Create(aOwner : TComponent); overload; override;
       { Summary: Constructs a TROMessage object
         Description: Constructs a TROMessage object, calls InitObject and immediately after that calls ReadFromStream. }
       constructor Create(aStream : TStream); reintroduce; overload; virtual;
       { Summary: Destroyes the object
         Description: Destroyes the object and the internal serializer}
       destructor Destroy; override;

       procedure Assign(iSource:TPersistent); override;

       { Summary: Returns a reference to the internal serializer
         Description:
           Returns a reference to the internal serializer. You have to type cast this to the type of serializer
           used in CreateSerializer. }
       property Serializer : TROSerializer read fSerializer;

       property MessageName : string read GetMessageName write SetMessageName;
       property InterfaceName : string read GetInterfaceName write SetInterfaceName;
       property SessionID:TSessionID read GetSessionID write SetSessionID;

       property SupportSessions:boolean read fSupportSessions write fSupportSessions default false;

     published
       { Summary: This event fired when the method WriteToStream is about to be called
         Description: This event fired when the method WriteToStream is about to be called }
       property OnWriteToStream : TStreamOperation read fOnWriteToStream Write fOnWriteToStream;
       { Summary: This event fired when the method ReadFromStream is about to be called
         Description: This event fired when the method ReadFromStream is about to be called }
       property OnReadFromStream : TStreamOperation read fOnReadFromStream Write fOnReadFromStream;

       property OnServerException : TExceptionEvent read fOnServerException write fOnServerException;
     end;

     TROMessageClass = class of TROMessage;
     ExceptionClass = class of Exception;

{ Summary: Registers the message class
  Description:
    Registers the message class. See the initialization part of the units uROBINMessage and uROSOAPMessage.
    By registering your message class you can make it available to dialogs like the "New RemObjects Server" dialog
    inside the IDE or write dynamic code by using the functions GetMessageClass and GetMessageClassCount. }
procedure RegisterMessageClass(aROMessageClass : TROMessageClass);
{ Summary: Registers the message class
  Description:
    Unregisters the message class. You are not required to call this in the finalization part of your units
    since the collection of registered classes already does this for you. }
procedure UnregisterMessageClass(aROMessageClass : TROMessageClass);
{ Summary: Returns a pointer to a message class type
  Description:
    Returns a pointer to a message class type. Should be used with GetMessageClassCount.  }
function GetMessageClass(Index : integer) : TROMessageClass;
{ Summary: Returns the number of registered message classes
  Description:
    Returns the number of registered message classes. Should be used with GetMessageClass. }
function GetMessageClassCount : integer;

function GetExceptionClass(const anExceptionClassName : string) : ExceptionClass;
procedure RegisterExceptionClass(anExceptionClass : ExceptionClass);
procedure UnregisterExceptionClass(anExceptionClass : ExceptionClass);

implementation

uses {$IFDEF MSWINDOWS}Windows, {$ENDIF}
     {$IFDEF KYLIX}Types, Libc, {$ENDIF}
     {$IFDEF DEBUG_REMOBJECTS}eDebugServer,{$ENDIF}
     {$IFDEF DOTNET}
     RemObjects.SDK.Types,
     {$ELSE}
     uROTypes,
     {$ENDIF}
     uROClasses, uROHTTPTools;

var _MessageClasses : TClassList;
    _ExceptionClasses : TClassList;

procedure RegisterMessageClass(aROMessageClass : TROMessageClass);
begin
  _MessageClasses.Add(aROMessageClass);
end;

procedure UnregisterMessageClass(aROMessageClass : TROMessageClass);
begin
  _MessageClasses.Remove(aROMessageClass);
end;

function GetMessageClass(Index : integer) : TROMessageClass;
begin
  result := TROMessageClass(_MessageClasses[Index]);
end;

function GetMessageClassCount : integer;
begin
  result := _MessageClasses.Count
end;

function GetExceptionClass(const anExceptionClassName : string) : ExceptionClass;
var i : integer;
begin
  result := EROServerException;

  for i := 0 to (_ExceptionClasses.Count-1) do
    {$IFDEF DOTNET}
    if (ExceptionClass(_ExceptionClasses[i]).ClassName = anExceptionClassName) then begin
    {$ELSE}
    if (CompareText(ExceptionClass(_ExceptionClasses[i]).ClassName, anExceptionClassName)=0) then begin
    {$ENDIF DOTNET}
      result := ExceptionClass(_ExceptionClasses[i]);
      Exit;
    end;
end;

procedure RegisterExceptionClass(anExceptionClass : ExceptionClass);
begin
  _ExceptionClasses.Add(anExceptionClass)
end;

procedure UnregisterExceptionClass(anExceptionClass : ExceptionClass);
begin
  _ExceptionClasses.Remove(anExceptionClass)
end;

{ TROMessage }
constructor TROMessage.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  InitObject;
end;

constructor TROMessage.Create;
begin
  inherited Create(NIL);
  InitObject;
end;

constructor TROMessage.Create(aStream: TStream);
begin
  inherited Create(NIL);
  InitObject;
  ReadFromStream(aStream);
end;

destructor TROMessage.Destroy;
begin
  if Assigned(fSerializer)
    then FreeAndNIL(fSerializer);
    
  inherited;
end;

procedure TROMessage.InitObject;
begin
  fSerializer := CreateSerializer;
end;

procedure TROMessage.Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName: string);
begin
  fInterfaceName := anInterfaceName;
  fMessageName := aMessageName;
end;

procedure TROMessage.Finalize;
begin
  //FreeAndNIL(fSerializer);
end;

function TROMessage.GetInterfaceName: string;
begin
  result := fInterfaceName
end;

function TROMessage.GetMessageName: string;
begin
  result := fMessageName
end;

procedure TROMessage.SetInterfaceName(const aValue: string);
begin
  fInterfaceName := aValue
end;

procedure TROMessage.SetMessageName(const aValue: string);
begin
  fMessageName := aValue
end;

procedure TROMessage.GetModuleInfo(aStream : TStream; const aTransport : IROTransport; var aFormat : TDataFormat);
{$IFDEF DOTNET}
begin
  raise Exception.Create(err_NotImplementedForDotNetYet)
end;
{$ELSE}
var rs : TResourceStream;
begin
  rs := TResourceStream.Create(HINstance, res_RODLFile, RT_RCDATA);
  try
    rs.SaveToStream(aStream);

    SetHTTPInfo(aTransport, dfXML);

    aFormat := dfXML;
  finally
    rs.Free;
  end;
end;
{$ENDIF DOTNET}

procedure TROMessage.ReadFromStream(aStream: TStream);
begin
  if Assigned(fOnReadFromStream) then fOnReadFromStream(aStream);
  aStream.Position := 0; // Just in case
end;

procedure TROMessage.WriteToStream(aStream: TStream);
begin
  if Assigned(fOnWriteToStream) then begin
    aStream.Position := 0; // Just in case
    fOnWriteToStream(aStream);
  end;
  aStream.Position := 0; // Just in case
end;

{$IFDEF DOTNET}
{$ELSE}
procedure TROMessage.Read(const aName: string; aTypeInfo: PTypeInfo;
  var Ptr; Attributes: TParamAttributes);
begin
  Serializer.Read(aName, aTypeInfo, Ptr);
end;

procedure TROMessage.Write(const aName: string; aTypeInfo: PTypeInfo;
  const Ptr; Attributes: TParamAttributes);
begin
  Serializer.Write(aName, aTypeInfo, Ptr);
end;
{$ENDIF DOTNET}

procedure TROMessage.ProcessException;
var E: Exception;
begin
  E := ReadException;
  if Assigned(E) then begin
    if Assigned(fOnServerException) then fOnServerException(E);
    raise E;
  end;
end;

function TROMessage.Clone: IROMessage;
begin
  result := TROMessageClass(ClassType).CreateRefCountedClone(self) as IROMessage;
end;

constructor TROMessage.CreateRefCountedClone(iMessage: TROMessage);
begin
  Create();
  Assign(iMessage);
  fReferenceCounted := true;

  //Todo: implement reference counting;
end;

procedure TROMessage.Assign(iSource: TPersistent);
begin
  //inherited;
  RaiseError(err_MessageClassMustImplementAssign,[]);
end;

function TROMessage._AddRef: integer;
begin
  inc(fRefCount);
  result := fRefCount;
end;

function TROMessage._Release: integer;
begin
  dec(fRefCount);
  result := fRefCount;
  if (fRefCount = 0) and fReferenceCounted then Free();
end;

function TROMessage.GetSessionID: TGUID;
begin
  result := fSessionID;
end;

procedure TROMessage.SetSessionID(const Value: TSessionID);
begin
  fSessionID := Value;
end;

initialization
  _MessageClasses := TClassList.Create;
  _ExceptionClasses := TClassList.Create;

finalization
  _MessageClasses.Free;
  _ExceptionClasses.Free;
  
end.
