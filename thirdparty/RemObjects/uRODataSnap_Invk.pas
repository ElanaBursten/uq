unit uRODataSnap_Invk;

interface

uses Classes, uROServer, uROServerIntf, uROClientIntf, uRODataSnap_Intf;

type
   TIAppServer_Invoker = class(TROInvoker)
   private
   protected
   published
      procedure Invoke_AS_ApplyUpdates(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_GetRecords(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_DataRequest(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_GetProviderNames(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_GetParams(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_RowRequest(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
      procedure Invoke_AS_Execute(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
   end;


implementation

uses {$IFDEF DEBUG_REMOBJECTS_DATASNAP}eDebugServer, uROBINMessage,{$ENDIF}
     uRORes;

procedure TIAppServer_Invoker.Invoke_AS_ApplyUpdates(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_ApplyUpdates(const ProviderName: WideString; const Delta: Binary; const MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: String): binary; }
var
    ProviderName: WideString;
    Delta: Binary;
    MaxErrors: Integer;
    ErrorCount: Integer;
    OwnerData: String;
    Result: binary;
begin
  Delta := NIL;
  Result := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('Delta', TypeInfo(Binary), Delta, []);
    __Message.Read('MaxErrors', TypeInfo(Integer), MaxErrors, []);
    __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);

    Result := (__Instance as IAppServer).AS_ApplyUpdates(ProviderName, Delta, MaxErrors, ErrorCount, OwnerData);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_ApplyUpdatesResponse');
    __Message.Write('ErrorCount', TypeInfo(Integer), ErrorCount, []);
    __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
    __Message.Write('Result', TypeInfo(binary), Result, []);
    __Message.Finalize;

  finally
    Delta.Free;
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_GetRecords(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_GetRecords(const ProviderName: WideString; const Count: Integer; out RecsOut: Integer; const Options: Integer; const CommandText: WideString; var Params: Binary; var OwnerData: String): binary; }
var
    ProviderName: WideString;
    Count: Integer;
    RecsOut: Integer;
    Options: Integer;
    CommandText: WideString;
    Params: Binary;
    OwnerData: String;
    Result: binary;
begin
  Params := NIL;
  Result := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('Count', TypeInfo(Integer), Count, []);
    __Message.Read('Options', TypeInfo(Integer), Options, []);
    __Message.Read('CommandText', TypeInfo(WideString), CommandText, []);
    __Message.Read('Params', TypeInfo(Binary), Params, []);
    __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);

    Result := (__Instance as IAppServer).AS_GetRecords(ProviderName, Count, RecsOut, Options, CommandText, Params, OwnerData);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_GetRecordsResponse');
    __Message.Write('RecsOut', TypeInfo(Integer), RecsOut, []);
    __Message.Write('Params', TypeInfo(Binary), Params, []);
    __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
    __Message.Write('Result', TypeInfo(binary), Result, []);
    __Message.Finalize;

  finally
    Params.Free;
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_DataRequest(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_DataRequest(const ProviderName: WideString; const Data: Binary): binary; }
var
    ProviderName: WideString;
    Data: Binary;
    Result: binary;
begin
  Data := NIL;
  Result := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('Data', TypeInfo(Binary), Data, []);

    Result := (__Instance as IAppServer).AS_DataRequest(ProviderName, Data);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_DataRequestResponse');
    __Message.Write('Result', TypeInfo(binary), Result, []);
    __Message.Finalize;

  finally
    Data.Free;
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_GetProviderNames(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_GetProviderNames: TProviderNames; }
var
    Result: TProviderNames;
begin
  Result := NIL;
  try

    Result := (__Instance as IAppServer).AS_GetProviderNames;

    __Message.Initialize(__Transport, 'IAppServer', 'AS_GetProviderNamesResponse');
    __Message.Write('Result', TypeInfo(TProviderNames), Result, []);
    __Message.Finalize;

  finally
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_GetParams(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_GetParams(const ProviderName: WideString; var OwnerData: String): binary; }
var
    ProviderName: WideString;
    OwnerData: String;
    Result: binary;
begin
  Result := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);

    Result := (__Instance as IAppServer).AS_GetParams(ProviderName, OwnerData);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_GetParamsResponse');
    __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
    __Message.Write('Result', TypeInfo(binary), Result, []);
    __Message.Finalize;

  finally
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_RowRequest(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ function AS_RowRequest(const ProviderName: WideString; const Row: Binary; const RequestType: Integer; var OwnerData: String): binary; }
var
    ProviderName: WideString;
    Row: Binary;
    RequestType: Integer;
    OwnerData: String;
    Result: binary;
begin
  Row := NIL;
  Result := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('Row', TypeInfo(Binary), Row, []);
    __Message.Read('RequestType', TypeInfo(Integer), RequestType, []);
    __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);

    Result := (__Instance as IAppServer).AS_RowRequest(ProviderName, Row, RequestType, OwnerData);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_RowRequestResponse');
    __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
    __Message.Write('Result', TypeInfo(binary), Result, []);
    __Message.Finalize;

  finally
    Row.Free;
    Result.Free;
  end;
end;
procedure TIAppServer_Invoker.Invoke_AS_Execute(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);
{ procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; var Params: Binary; var OwnerData: String); }
var
    ProviderName: WideString;
    CommandText: WideString;
    Params: Binary;
    OwnerData: String;
begin
  Params := NIL;
  try
    __Message.Read('ProviderName', TypeInfo(WideString), ProviderName, []);
    __Message.Read('CommandText', TypeInfo(WideString), CommandText, []);
    __Message.Read('Params', TypeInfo(Binary), Params, []);
    __Message.Read('OwnerData', TypeInfo(String), OwnerData, []);

    (__Instance as IAppServer).AS_Execute(ProviderName, CommandText, Params, OwnerData);

    __Message.Initialize(__Transport, 'IAppServer', 'AS_ExecuteResponse');
    __Message.Write('Params', TypeInfo(Binary), Params, []);
    __Message.Write('OwnerData', TypeInfo(String), OwnerData, []);
    __Message.Finalize;

  finally
    Params.Free;
  end;
end;

end.
