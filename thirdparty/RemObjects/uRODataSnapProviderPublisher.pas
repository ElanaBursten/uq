unit uRODataSnapProviderPublisher;

interface

uses Classes, Provider,
     uRODataSnap_Intf,
     uRODataSnapPublishedProvidersCollection;

type TROCustomDataSnapProviderPublisher = class(TComponent)
     private
       fProviders: TPublishedProviders;
       procedure SetProviders(const Value: TPublishedProviders);
     protected
       procedure Notification(AComponent: TComponent; Operation: TOperation); override;
     public
       constructor Create(AOwner: TComponent); override;
       destructor Destroy; override;
       procedure AddNamesToTProviderNames(iProviderNames:TProviderNames);
       function GetProviderByName(const iProviderName:string):TCustomProvider;
       property Providers:TPublishedProviders read fProviders write SetProviders;
     end;

     TRODataSnapProviderPublisher = class(TROCustomDataSnapProviderPublisher)
     published
       property Providers;
     end;

implementation

uses SysUtils,
     uROServer, uRODataSnapProviderManager, uRODataSnapBaseAppServer,
       uRODataSnap_Invk;

{ TROCustomDataSnapProviderPublisher }

procedure TROCustomDataSnapProviderPublisher.AddNamesToTProviderNames(iProviderNames: TProviderNames);
var i:integer;
begin
  //ToDo: protect by CriticalSection;

  for I := 0 to Providers.Count-1 do begin
    if (Providers[i].Name <> '') and Assigned(Providers[i].Provider) then
      iProviderNames.Add(Providers[i].Name);
  end;    { for }
end;

constructor TROCustomDataSnapProviderPublisher.Create(AOwner: TComponent);
begin
  inherited;
  fProviders := TPublishedProviders.Create(self);
  ProviderManager.RegisterPublisher(self);
end;

destructor TROCustomDataSnapProviderPublisher.Destroy;
begin
  ProviderManager.UnregisterPublisher(self);
  FreeAndNil(fProviders);
  inherited;
end;

function TROCustomDataSnapProviderPublisher.GetProviderByName(const iProviderName:string): TCustomProvider;
var lPublishedProvider:TPublishedProvider;
begin
  //ToDo: protect by CriticalSection;
  lPublishedProvider := fProviders.GetProviderByName(iProviderName);
  if Assigned(lPublishedProvider) then result := lPublishedProvider.Provider
  else result := nil;
end;

procedure TROCustomDataSnapProviderPublisher.Notification(
  AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then begin
    if AComponent is TCustomProvider then Providers.UnlinkProvider(AComponent as TCustomProvider);
  end;
end;

procedure TROCustomDataSnapProviderPublisher.SetProviders(
  const Value: TPublishedProviders);
begin
  fProviders.Assign(Value);
end;


type TRODataSnapSimpleAppServer = class(TRODataSnapBaseAppServer)
       function GetProviderByName(const iProviderName:string):TCustomProvider; override;
       function GetProviderNames:TProviderNames; override;
     end;

function TRODataSnapSimpleAppServer.GetProviderByName(const iProviderName:string):TCustomProvider;
begin
  result := ProviderManager.GetProviderByName(iProviderName);
end;

function TRODataSnapSimpleAppServer.GetProviderNames: TProviderNames;
begin
  result := ProviderManager.GetProviderNames();
end;

procedure Create_IAppServer(out anInstance : IUnknown);
begin
  anInstance := TRODataSnapSimpleAppServer.Create(nil);
end;

{$R *.dfm}

initialization
  TROClassFactory.Create('IAppServer', Create_IAppServer, TIAppServer_Invoker);
end.
