unit uRODataSnapNewServerModuleWizard;

interface

uses DesignEditors, ToolsAPI, Windows, ActnList, Menus, Classes;

const CRLF = #13#10;		// Carriage-return line-feed.

resourcestring
  sBasicDfmSource =
    'object %0:s: T%0:s'                                       + crlf +
    '  Left = 200'                                             + crlf +
    '  Top = 200'                                              + crlf +
    '  Height = 300'                                           + crlf +
    '  Width = 300'                                            + crlf +
    'end';


  sBasicFormSource =
    'unit %0:s;' 					       + crlf +
                                                                 crlf +
    'interface'  					       + crlf +
                                                                 crlf +
    'uses {vcl:} SysUtils, Classes, DB, DBClient, '            + crlf +
    '     {RemObjects:} %3:s;'                                 + crlf +
                                                                 crlf +
    'type'                                                     + crlf +
    '  T%1:s = class(%2:s)'  				       + crlf +
    '  private' 	 				       + crlf +
    '    { Private declarations }'                             + crlf +
    '  public'  			                       + crlf +
    '    { Public declarations }' 		               + crlf +
    '  end;'	 					       + crlf +
                                                                 crlf +
    'var'  						       + crlf +
    '  %1:s: T%1:s;'                                           + crlf +
                                                                 crlf +
    'implementation'                                           + crlf +
                                                                 crlf +
    'uses {RemObjects:} uROServer, uRODataSnap_Invk;'          + crlf +
                                                                 crlf +
    '{$R *.DFM}'                                               + crlf +
                                                                 crlf +
    'procedure Create_DataSnapModule(out oInstance:IUnknown);' + crlf +
    'begin'                                                    + crlf +
    '  oInstance := T%1:s.Create(nil);'                         + crlf +
    'end;'                                                     + crlf +
                                                                 crlf +
    'initialization'                                           + crlf +
    '  { To adjust scalability of the DataSnapModule and avoid recreation of'   + crlf +
    '    instances for each roundtrip, you might want to choose a  different'   + crlf +
    '    ClassFactory type.'                                           + crlf +
    crlf +
    '    Make sure to check the following url for more information:'+ crlf+
    '    http://www.remobjects.com/page.asp?id=C771266D-6D99-4301-B77D-D7B92D3BCD4D }'+ crlf +
    crlf +
    '  //TROPooledClassFactory.Create(''IAppServer'', Create_DataSnapModule, TIAppServer_Invoker, 25);' + crlf +
    '  TROClassFactory.Create(''IAppServer'', Create_DataSnapModule, TIAppServer_Invoker);' + crlf +
    'end.' 																																			+ crlf;

type TSourceFile = class(TInterfacedObject, IOTAFile)
     private
       fAge: TDateTime;
       fSource:string;
     public
       function GetSource: string;
       function GetAge: TDateTime;
       constructor Create(const iSource:string);
     end;

     TBaseFormCreator = class(TInterfacedObject, IOTAModuleCreator)
     public
       function GetCreatorType: string;
       function GetExisting: Boolean;
       function GetFileSystem: string;
       function GetOwner: IOTAModule;
       function GetUnnamed: Boolean;

       function GetAncestorName: string;
       function GetImplFileName: string;
       function GetIntfFileName: string;
       function GetFormName: string;
       function GetMainForm: Boolean;
       function GetShowForm: Boolean;
       function GetShowSource: Boolean;
       function NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile;
       function NewImplSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile;
       function NewIntfSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile;
       procedure FormCreated(const FormEditor: IOTAFormEditor);
     end;		{ TBaseFormCreator }

     TNewModuleExpert = class(TInterfacedObject, IOTAWizard, IOTARepositoryWizard, IOTAFormWizard)
     public

       constructor Create();
       destructor Destroy(); override;

       procedure AfterSave;
       procedure BeforeSave;
       procedure Destroyed;
       procedure Modified;

       procedure Execute;
       function GetAuthor: String;
       function GetComment: String;
       function GetGlyph: Cardinal;
       function GetIDString: String;
       function GetName: String;
       function GetPage: String;
       function GetState: TWizardState;
     end;

procedure Register;

implementation

uses SysUtils, Graphics,
     uRORes;

{ TBaseFormCreator }

procedure TBaseFormCreator.FormCreated(const FormEditor: IOTAFormEditor);
begin

end;

function TBaseFormCreator.GetAncestorName: string;
begin
  result := 'TRODataSnapModule';
end;

function TBaseFormCreator.GetCreatorType: string;
begin
  result := sForm;
end;

function TBaseFormCreator.GetExisting: Boolean;
begin
  result := false;
end;

function TBaseFormCreator.GetFileSystem: string;
begin
  result := '';
end;

function TBaseFormCreator.GetFormName: string;
begin
  result := '';
end;

function TBaseFormCreator.GetImplFileName: string;
begin
  result := '';
end;

function TBaseFormCreator.GetIntfFileName: string;
begin
  result := '';
end;

function TBaseFormCreator.GetMainForm: Boolean;
begin
  result := false;
end;

function TBaseFormCreator.GetOwner: IOTAModule;
var
  ModuleServices: IOTAModuleServices;
  Module: IOTAModule;
  NewModule: IOTAModule;
begin
  Result := nil;
  if BorlandIDEServices.QueryInterface(IOTAModuleServices, ModuleServices) =
S_OK then
  begin
    Module := ModuleServices.CurrentModule;
    if Module <> nil then
      if Module.GetOwnerCount > 0 then
      begin
         NewModule := Module.GetOwner(0);
         if NewModule <> nil then
            if NewModule.QueryInterface(IOTAProject, Result) <> S_OK then
               Result := nil;
      end;
  end;
end;

function TBaseFormCreator.GetShowForm: Boolean;
begin
  result := true;
end;

function TBaseFormCreator.GetShowSource: Boolean;
begin
  result := true;
end;

function TBaseFormCreator.GetUnnamed: Boolean;
begin
  result := true;
end;

function RemoveInitialT(const iString:string):string;
begin
  result := iString;
  if (result <> '') and (result[1] = 'T') then Delete(result,1,1);
  //ShowMessage(iString+' '+result);
end;

function TBaseFormCreator.NewFormFile(const FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  result := TSourceFile.Create(Format(sBasicDfmSource,[RemoveInitialT(FormIdent)]));
end;

function TBaseFormCreator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  result := TSourceFile.Create(Format(sBasicFormSource,[ModuleIdent,RemoveInitialT(FormIdent),AncestorIdent,'uRODataSnapModule']));
end;

function TBaseFormCreator.NewIntfSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin

end;

{ TSourceFile }

constructor TSourceFile.Create(const iSource: string);
begin
  inherited Create();
  fSource := iSource;
  fAge := Now;
end;

function TSourceFile.GetAge: TDateTime;
begin
  result := fAge;
end;

function TSourceFile.GetSource: string;
begin
  result := fSource;
end;

{ TNewModuleExpert }

constructor TNewModuleExpert.Create;
begin
  inherited;
end;

destructor TNewModuleExpert.Destroy;
begin
  inherited;
end;

procedure TNewModuleExpert.Execute;
var
  ModuleServices: IOTAModuleServices;
  ModuleCreator: IOTAModuleCreator;
begin
  if BorlandIDEServices.QueryInterface(IOTAModuleServices, ModuleServices) = S_OK then
  begin
    ModuleCreator := TBaseFormCreator.Create;
    ModuleServices.CreateModule(ModuleCreator);
  end;
end;

function TNewModuleExpert.GetAuthor: String;
begin
  result := 'RemObjects Software';
end;

function TNewModuleExpert.GetComment: String;
begin
  result := 'RemObjects DataSnap Server Module';
end;

function TNewModuleExpert.GetGlyph: Cardinal;
begin
  result := LoadIcon(hInstance,'NewDataSnapModule');
end;

function TNewModuleExpert.GetIDString: String;
begin
  result := 'RemObjectsDataSnapModule';
end;

function TNewModuleExpert.GetName: String;
begin
  result := 'RemObjects DataSnap Server Module';
end;

function TNewModuleExpert.GetPage: String;
begin
  result := str_ProductName;
end;

function TNewModuleExpert.GetState: TWizardState;
begin
  result := [wsEnabled];
end;

procedure TNewModuleExpert.AfterSave;
begin

end;

procedure TNewModuleExpert.BeforeSave;
begin

end;

procedure TNewModuleExpert.Destroyed;
begin

end;

procedure TNewModuleExpert.Modified;
begin

end;

procedure Register;
begin
  //RegisterCustomModule(TSxView, TCustomModule);
  RegisterPackageWizard(TNewModuleExpert.Create as IOTAFormWizard);
end;

{$R 'uRODataSnapNewServerModuleWizard.res' 'uRODataSnapNewServerModuleWizard.rc'}

initialization
finalization
end.
