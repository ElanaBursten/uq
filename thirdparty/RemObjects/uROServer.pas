{ Summary: }
unit uROServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$ifdef REMOBJECTS_UseEncryption}uROBaseConnection, uROEncryption,{$ENDIF}

  {$IFDEF DOTNET}
  Borland.Delphi.Classes,
  Borland.Delphi.SysUtils,
  RemObjects.SDK.VclReplacements,
  {$ELSE}
  Classes, SysUtils, SyncObjs,
  {$ENDIF}

  uRORes, uROServerIntf, uROClient, uROClientIntf;

type { Misc }
     TMessageInvokeMethod = procedure(const anInstance : IInterface;
                                      const aMessage : IROMessage;
                                      const aTransport : IROTransport) of object;


     { Description
       This is the description (the long one right after the class
       \declaration)

       Summary
       This is the summary (the one that goes on top of the page)  }
     TRORemotable = class(TInterfacedObject)
     public
       constructor Create; virtual;
       destructor Destroy; override;
     end;

     TRORemotableClass = class of TRORemotable;

     { TROInvoker }
     {  Description
          This is the description (the long one right after the class declaration)
        Summary
           This is the summary (the one that goes on top of the page)
     }

     TROInvoker = class(TInterfacedObject, IROInvoker)
     private

     protected
       function CustomHandleMessage(const aFactory : IROClassFactory;
                                    const aMessage: IROMessage;
                                    const aTransport : IROTransport) : boolean; virtual;

       procedure BeforeInvoke(aMethodPtr : TMessageInvokeMethod;
                              const anInstance : IInterface;
                              const aFactory : IROClassFactory;
                              const aMessage: IROMessage;
                              const aTransport : IROTransport); virtual;

       procedure AfterInvoke(aMethodPtr: TMessageInvokeMethod;
                              const anInstance: IInterface;
                              const aFactory: IROClassFactory;
                              const aMessage: IROMessage;
                              const aTransport: IROTransport;
                              anException: Exception); virtual;

     public
       constructor Create; virtual;
       destructor Destroy; override;

       function HandleMessage(const aFactory : IROClassFactory;
                              const aMessage : IROMessage;
                              const aTransport : IROTransport) : boolean;
     end;

     TROInvokerClass = class of TROInvoker;

     { TROClassFactory }
     TRORemotableCreatorFunc = procedure(out anInstance : IInterface);
     { Summary
       This is the summary (the one that goes on top of the page)
       
       Description
       This is the description (the long one right after the class
       \declaration)                                               }

     {----------------- TROMessageDispatcher -----------------}
     TROMessageDispatchers = class;
     TROMessageDispatcher = class(TCollectionItem)
     private
       fDispatchers : TROMessageDispatchers;
       fMessage: TROMessage;
       fEnabled: boolean;
       fDummyName: string;

       {$IFDEF DOTNET}
       fMessageIntf : IROMessage;
       fModuleInfoIntf : IROModuleInfo;
       {$ELSE}
       fMessageIntf : pointer;
       fModuleInfoIntf : pointer;
       {$ENDIF DOTNET}

       function GetMessageIntf: IROMessage;
       function GetModuleInfoIntf: IROModuleInfo;

     protected
       procedure SetMessage(const Value: TROMessage); virtual;
       function GetDisplayName: string; override;

     public
       constructor Create(aCollection : TCollection); override;
       destructor Destroy; override;

       procedure Assign(Source: TPersistent); override;

       function ProcessMessage(const aTransport : IROTransport; aRequeststream, aResponsestream : TStream) : boolean;
       function CanHandleMessage(const aTransport: IROTransport; aRequeststream : TStream): boolean; virtual;

       property MessageIntf : IROMessage read GetMessageIntf;
       property ModuleIntf : IROModuleInfo read GetModuleInfoIntf;
     published
       property Name : string read GetDisplayName write fDummyName;
       property Message : TROMessage read fMessage write SetMessage;
       property Enabled : boolean read fEnabled write fEnabled;
     end;

     TROMessageDispatcherClass = class of TROMessageDispatcher;

     {----------------- TROMessageDispatchers -----------------}

     TROServer = class;

     TROMessageDispatchers = class(TCollection)
     private
       fServer : TROServer;

       function GetDispatcher(Index: integer): TROMessageDispatcher;

     protected
       function GetSupportsMultipleDispatchers: boolean; virtual;
       function GetDispatcherClass : TROMessageDispatcherClass; virtual;
       {$IFDEF DELPHI6UP}
       procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
       {$ENDIF DELPHI6UP}

     public
       constructor Create(aServer : TROServer); virtual;
       destructor Destroy; override;

       procedure CleanReferences(aMessage : TROMessage);
       function FindDuplicate(aMessage : TROMessage) : TROMessageDispatcher;
       function FindDispatcher(const aTransport : IROTransport; aRequestStream : TStream): TROMessageDispatcher;

       function DispatcherByName(const aName : string) : TROMessageDispatcher;

       property Dispatchers[Index : integer] : TROMessageDispatcher read GetDispatcher; default;
       property SupportsMultipleDispatchers : boolean read GetSupportsMultipleDispatchers;
     end;

     TROMessageDispatchersClass = class of TROMessageDispatchers;

     {------------------- TROServer -------------------}
     {$ifdef REMOBJECTS_UseEncryption}
     TROServer = class(TROBaseConnection)
     {$else}
     TROServer = class(TComponent)
     {$endif}
     private
       fDoneAfterLoad,
       fLoadActive : boolean;

       fOnAfterServerDeactivate: TNotifyEvent;
       fOnAfterServerActivate: TNotifyEvent;
       fOnBeforeServerDeactivate: TNotifyEvent;
       fOnBeforeServerActivate: TNotifyEvent;
       fDispatchers: TROMessageDispatchers;
       fOnReadFromStream: TStreamOperation;
       fOnWriteToStream: TStreamOperation;

       function GetActive: boolean;
       procedure SetActive(const Value: boolean);
       procedure SetDispatchers(const Value: TROMessageDispatchers);

     private
       procedure TriggerReadFromStream(iStream: TStream);
       procedure TriggerWriteToStream(iStream: TStream);

     protected
       {-------- Internals --------}
       procedure Notification(AComponent: TComponent; Operation: TOperation); override;

       procedure IntSetActive(const Value: boolean); virtual; abstract;
       function IntGetActive : boolean; virtual; abstract;

       procedure Loaded; override;

       function GetDispatchersClass : TROMessageDispatchersClass; virtual;

       function DispatchMessage(const aTransport : IROTransport; aRequeststream, aResponsestream : TStream) : boolean;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

     published
       property Active : boolean read GetActive write SetActive;

       property Dispatchers : TROMessageDispatchers read fDispatchers write SetDispatchers;

       property OnBeforeServerActivate : TNotifyEvent read fOnBeforeServerActivate write fOnBeforeServerActivate;
       property OnAfterServerActivate : TNotifyEvent read fOnAfterServerActivate write fOnAfterServerActivate;
       property OnBeforeServerDeactivate : TNotifyEvent read fOnBeforeServerDeactivate write fOnBeforeServerDeactivate;
       property OnAfterServerDeactivate : TNotifyEvent read fOnAfterServerDeactivate write fOnAfterServerDeactivate;
       property OnWriteToStream : TStreamOperation read fOnWriteToStream Write fOnWriteToStream;
       property OnReadFromStream : TStreamOperation read fOnReadFromStream Write fOnReadFromStream;
     end;

     TROServerClass = class of TROServer;

     TROClassFactory = class(TInterfacedObject, IROClassFactory)
     private
       fCreatorFunc : TRORemotableCreatorFunc;
       fInterfaceName : string;
       fInvoker : IROInvoker;
       fInvokerClass : TROInvokerClass;

     protected
       { IROClassFactory }
       procedure CreateInstance(out anInstance : IInterface); virtual; 
       procedure ReleaseInstance(var anInstance : IInterface); virtual;
       function GetInterfaceName: string;
       function GetInvoker : IROInvoker;

     public
       constructor Create(const anInterfaceName : string;
                          aCreatorFunc : TRORemotableCreatorFunc;
                          anInvokerClass : TROInvokerClass);
       destructor Destroy; override;
     end;


function MainProcessMessage(const aMessage : IROMessage;
                            const aTransport : IROTransport;
                            aRequestStream,
                            aResponseStream : TStream) : boolean;

procedure RegisterClassFactory(const aClassFactory : IROClassFactory);
function FindClassFactory(const anInterfaceName : string) : IROClassFactory;

procedure RegisterServerClass(aROMessageClass : TROServerClass);
procedure UnregisterServerClass(aROMessageClass : TROServerClass);
function GetServerClass(Index : integer) : TROServerClass;
function GetServerClassCount : integer;

implementation

uses {$IFDEF DOTNET}
     {$ELSE}
     Contnrs,
     {$ENDIF}
     uROClasses;

var _ServerClasses : TClassList;

procedure RegisterServerClass(aROMessageClass : TROServerClass);
begin
  _ServerClasses.Add(aROMessageClass);
end;

procedure UnregisterServerClass(aROMessageClass : TROServerClass);
begin
  _ServerClasses.Remove(aROMessageClass);
end;

function GetServerClass(Index : integer) : TROServerClass;
begin
  result := TROServerClass(_ServerClasses[Index]);
end;

function GetServerClassCount : integer;
begin
  result := _ServerClasses.Count
end;

type
     { Description
  This is the description (the long one right after the class
  \declaration)

  Summary
  This is the summary (the one that goes on top of the page)  }
     TROClassFactoryList = class(TInterfaceList)
     private
       function GetItems(Index: integer): IROClassFactory;

     protected
     public
       function Add(aClassFactory : IROClassFactory) : integer;
       function ClassFactoryByInterfaceName(const anInterfaceName : string) : IROClassFactory;

       property Items[Index : integer] : IROClassFactory read GetItems; default;
     end;

var _ClassFactoryList : TROClassFactoryList;

function ClassFactoryList : TROClassFactoryList;
begin
  if (_ClassFactoryList=NIL)
    then _ClassFactoryList := TROClassFactoryList.Create;
  result := _ClassFactoryList;
end;

function MainProcessMessage(const aMessage : IROMessage; const aTransport : IROTransport; aRequestStream, aResponseStream : TStream) : boolean;
var factory : IROClassFactory;
    moduleinfo : IROModuleInfo;
    dataformat : TDataFormat;
//    http       : IROHTTPTransport;
begin
  //result := FALSE;

  try
    if (aMessage=NIL) then RaiseError(err_NILMessage, []);

    if (aRequestStream.Size=0) then begin

      {$IFDEF DOTNET}
      raise Exception.Create(err_NotImplementedForDotNetYet)
      {$ELSE}
      if Supports(aMessage, IROModuleInfo, moduleinfo)
        then moduleinfo.GetModuleInfo(aResponseStream, aTransport, dataformat)
        else RaiseError(err_InvalidRequestStream, [aRequestStream.Size]);
      result := TRUE;
      {$ENDIF DOTNET}

    end
    else begin
      aMessage.ReadFromStream(aRequestStream);

      if (aMessage.InterfaceName='') then RaiseError(err_UnspecifiedInterface, [])
      else if (aMessage.MessageName='') then RaiseError(err_UnspecifiedMessage, []);

      factory := FindClassFactory(aMessage.InterfaceName);
      result := factory.Invoker.HandleMessage(factory, aMessage, aTransport);

      aMessage.WriteToStream(aResponseStream);
    end;
  except
    on E:Exception do begin
      result := FALSE;
      aMessage.WriteException(aResponseStream, E);
    end;
  end;
end;

function FindClassFactory(const anInterfaceName : string) : IROClassFactory;
begin
  result := ClassFactoryList.ClassFactoryByInterfaceName(anInterfaceName)
end;

procedure RegisterClassFactory(const aClassFactory : IROClassFactory);
begin
  ClassFactoryList.Add(aClassFactory);
end;

{ TROClassFactoryList }

function TROClassFactoryList.Add(aClassFactory: IROClassFactory): integer;
begin
  {$IFDEF DOTNET}
  {$ELSE}
  result := inherited Add(aClassFactory);
  {$ENDIF DOTNET}
end;

function TROClassFactoryList.ClassFactoryByInterfaceName(
  const anInterfaceName: string): IROClassFactory;
var i : integer;
begin
  result := NIL;
  for i := 0 to (Count-1) do
    if (CompareText(anInterfaceName, Items[i].GetInterfaceName)=0) then begin
      result := Items[i];
      Break;
    end;

  if (result=NIL)
    then RaiseError(err_ClassFactoryNotFound, [anInterfaceName]);
end;

function TROClassFactoryList.GetItems(Index: integer): IROClassFactory;
begin
  result := inherited Items[Index] as IROClassFactory
end;

{ TROInvoker }

constructor TROInvoker.Create;
begin
  inherited Create;
end;

procedure TROInvoker.BeforeInvoke(aMethodPtr : TMessageInvokeMethod;
                                  const anInstance : IInterface;
                                  const aFactory : IROClassFactory;
                                  const aMessage: IROMessage;
                                  const aTransport : IROTransport);
var dispnotifier : IRODispatchNotifier;
begin
  // This can be enanched by descendants
  {$IFDEF DOTNET}
  {$ELSE}
  if Supports(anInstance, IRODispatchNotifier, dispnotifier)
    then dispnotifier.GetDispatchInfo(aTransport, aMessage);
  {$ENDIF DOTNET}
end;

procedure TROInvoker.AfterInvoke(aMethodPtr : TMessageInvokeMethod;
                                  const anInstance : IInterface;
                                  const aFactory : IROClassFactory;
                                  const aMessage: IROMessage;
                                  const aTransport : IROTransport;
                                  anException : Exception);
begin
  // This can be enanched by descendants
end;


function TROInvoker.CustomHandleMessage(const aFactory : IROClassFactory;
                                         const aMessage: IROMessage;
                                         const aTransport : IROTransport) : boolean;
var mtd : TMessageInvokeMethod;
    lSessionClassFactory:IROSessionClassFactory;
    lInstance : IInterface;
begin
  result := FALSE;

  mtd := NIL;
  lInstance := NIL;

  {$IFDEF DOTNET}
  {$ELSE}
  // The message is guaranteed not to be NIL and to have a name and an InterfaceName at this point
  @mtd := MethodAddress('Invoke_'+aMessage.MessageName);
  if (@mtd<>NIL) then try
    try

      if Supports(aFactory,IROSessionClassFactory,lSessionClassFactory) then
        lSessionClassFactory.CreateSessionInstance(lInstance,aMessage.GetSessionID)
      else
        aFactory.CreateInstance(lInstance);

      if (lInstance = NIL)
        then RaiseError(err_ClassFactoryDidNotReturnInstance, [aMessage.InterfaceName]);

      BeforeInvoke(mtd, lInstance, aFactory, aMessage, aTransport);
      mtd(lInstance, aMessage, aTransport);
      AfterInvoke(mtd, lInstance, aFactory, aMessage, aTransport, NIL);

      result := TRUE;       
    except
      on E:Exception do begin
        AfterInvoke(mtd, lInstance, aFactory, aMessage, aTransport, E);
        raise;
      end;
    end;
  finally
    if (lInstance <> nil) then aFactory.ReleaseInstance(lInstance);
  end
  else RaiseError(err_UnknownMethod, [aMessage.MessageName]);
  {$ENDIF}
end;

destructor TROInvoker.Destroy;
begin
  inherited;
end;

function TROInvoker.HandleMessage(const aFactory : IROClassFactory;
                                   const aMessage : IROMessage;
                                   const aTransport : IROTransport) : boolean;
begin
  result := CustomHandleMessage(aFactory, aMessage, aTransport);
end;

{ TRORemotable }

constructor TRORemotable.Create;
begin
  inherited
end;

destructor TRORemotable.Destroy;
begin
  inherited;
end;

{ TROMessageDispatcher }

constructor TROMessageDispatcher.Create(aCollection : TCollection);
begin
  inherited Create(aCollection);

  fEnabled := TRUE;
  fDispatchers := TROMessageDispatchers(aCollection);
  fModuleInfoIntf := NIL;
  fMessageIntf := NIL;
end;

destructor TROMessageDispatcher.Destroy;
begin
  fModuleInfoIntf := NIL;
  fMessageIntf := NIL;

  inherited;
end;

function TROMessageDispatcher.ProcessMessage(const aTransport: IROTransport; aRequeststream, aResponsestream: TStream) : boolean;
var lMessage:IROMessage;
begin
  lMessage := (MessageIntf as IROMessageCloneable).Clone();
  result := MainProcessMessage(lMessage, aTransport, aRequeststream, aResponsestream);
end;

function TROMessageDispatcher.GetDisplayName: string;
begin
  if (fMessage=NIL)
    then result := '[Unassigned]'
    else result := fMessage.Name;
end;

function TROMessageDispatcher.GetMessageIntf: IROMessage;
begin
  {$IFDEF DOTNET}
  result := fMessageIntf;
  {$ELSE}
  result := IROMessage(fMessageIntf);
  {$ENDIF DOTNET}
end;

function TROMessageDispatcher.GetModuleInfoIntf: IROModuleInfo;
begin
  {$IFDEF DOTNET}
  result := fModuleInfoIntf;
  {$ELSE}
  result := IROModuleInfo(fModuleInfoIntf)
  {$ENDIF DOTNET}
end;

procedure TROMessageDispatcher.SetMessage(const Value: TROMessage);
var ref : IROMessage;
begin
  if (Value=fMessage) then Exit;

  if (Value<>NIL) then begin
    if (fDispatchers.FindDuplicate(Value)<>NIL)
      then RaiseError(err_DispatcherAlreadyAssigned, [Value.Name]);

    {$IFDEF DOTNET}
    fMessageIntf := ref;
    //TODo: check whether "as" really returnd null instead of raisig an exception, as it does in C#.
    fModuleInfoIntf := Value as IROModuleInfo;
    {$ELSE}
    if not Value.GetInterface(IROMessage, ref)
      then RaiseError(err_IROMessageNotSupported, [Value.ClassName]);

    fMessageIntf := pointer(ref);
    Value.GetInterface(IROModuleInfo, fModuleInfoIntf);
    {$ENDIF DOTNET}

  end
  else begin
    fMessageIntf := NIL;
    fModuleInfoIntf := NIL;
  end;

  fMessage := Value;
end;

function TROMessageDispatcher.CanHandleMessage(const aTransport: IROTransport; aRequeststream : TStream): boolean;
begin
  result := fEnabled;
end;

procedure TROMessageDispatcher.Assign(Source: TPersistent);
var SourceDispatcher: TROMessageDispatcher;
begin
  if Source is TROMessageDispatcher then begin
    SourceDispatcher := TROMessageDispatcher(Source);
    Name := SourceDispatcher.Name;
    Message := SourceDispatcher.Message;
    Enabled := SourceDispatcher.Enabled;
  end
  else inherited Assign(Source);
end;

{ TROMessageDispatchers }

procedure TROMessageDispatchers.CleanReferences(
  aMessage : TROMessage);
var i : integer;
begin
  for i := 0 to (Count-1) do
    if (Dispatchers[i].Message=aMessage)
      then Dispatchers[i].Message := NIL;
end;

constructor TROMessageDispatchers.Create(aServer : TROServer);
begin
  inherited Create(GetDispatcherClass);
  fServer := aServer;
end;

destructor TROMessageDispatchers.Destroy;
begin
  inherited;
end;

function TROMessageDispatchers.FindDispatcher(const aTransport : IROTransport;
                                              aRequestStream : TStream): TROMessageDispatcher;
var i : integer;
begin
  result := NIL;

  for i := 0 to (Count-1) do
    if Dispatchers[i].CanHandleMessage(aTransport, aRequestStream) then begin
      result := Dispatchers[i];
      Break;
    end;
end;

function TROMessageDispatchers.DispatcherByName(
  const aName: string): TROMessageDispatcher;
var i : integer;
begin
  result := NIL;
  for i := 0 to (Count-1) do
    if (CompareText(Dispatchers[i].Name, aName)=0) then begin
      result := Dispatchers[i];
      Exit;
    end;
end;

function TROMessageDispatchers.FindDuplicate(
  aMessage: TROMessage): TROMessageDispatcher;
var i : integer;
begin
  result := NIL;
  
  for i := 0 to (Count-1) do
    if (Dispatchers[i].Message=aMessage) then begin
      result := Dispatchers[i];
      Exit;
    end;
end;

function TROMessageDispatchers.GetDispatcher(
  Index: integer): TROMessageDispatcher;
begin
  result := TROMessageDispatcher(inherited Items[Index]);
end;

function TROMessageDispatchers.GetDispatcherClass: TROMessageDispatcherClass;
begin
  result := TROMessageDispatcher;
end;

function TROMessageDispatchers.GetSupportsMultipleDispatchers: boolean;
begin
  result := FALSE;
end;

{$IFDEF DELPHI6UP}
procedure TROMessageDispatchers.Notify(Item: TCollectionItem;
  Action: TCollectionNotification);
begin
  inherited;
  if (Action=cnAdded) then begin
    if not SupportsMultipleDispatchers and (Count>1) then begin

      (*if (csDesigning in fServer.ComponentState) then begin
        { TODO: This is really bad. I should probabily just make a component editor and control it in there.
                The problem is that I cannot find a way to stop a TCollection.Add method gracefully.
                After this method is called TCollection.InsertItem continues and invokes NotifyDesigner
                passing Item. That will obviously fail since Item is being freed here... }
        MessageDlg(Format(err_ServerOnlySupportsOneDispatcher+'.'+#13#10+
                          'You can ignore the error that Delphi will raise in the IDE after you close this dialog.', [fServer.ClassName]), mtError, [mbOK], 0);
      end;*)

      FreeAndNIL(Item);
      RaiseError(err_ServerOnlySupportsOneDispatcher, [])
    end;
  end;
end;
{$ENDIF DELPHI6UP}

{ TROServer }
constructor TROServer.Create(aOwner: TComponent);
begin
  inherited;
  fDispatchers := GetDispatchersClass.Create(Self);
end;

destructor TROServer.Destroy;
begin
  fDispatchers.Free;
  inherited;
end;

procedure TROServer.TriggerReadFromStream(iStream:TStream);
begin
  if Assigned(OnReadFromStream) then begin
    iStream.Position := 0;
    OnReadFromStream(iStream);
    iStream.Position := 0;
  end;
end;

procedure TROServer.TriggerWriteToStream(iStream:TStream);
begin
  if Assigned(OnWriteToStream) then begin
    iStream.Position := 0;
    OnWriteToStream(iStream);
    iStream.Position := 0;
  end;
end;

function TROServer.DispatchMessage(const aTransport: IROTransport; aRequeststream, aResponsestream: TStream) : boolean;
var dispatcher : TROMessageDispatcher;
    excstr : string;
    {$ifdef REMOBJECTS_UseEncryption}
    DEncRequest,DUnEncResponse : TMemoryStream;
    {$endif}
begin
  result := FALSE;

  try
    {aRequeststream.Position := 0;
    if Assigned(OnReadFromStream) then OnReadFromStream(aRequeststream);
    aRequeststream.Position := 0;}

    dispatcher := Dispatchers.FindDispatcher(aTransport, aRequeststream);

    if (dispatcher=NIL) then RaiseError(err_CannotFindMessageDispatcher, []); // Should never happen! If this happens there's no way to format the exception correctly

    {$IFDEF REMOBJECTS_UseEncryption}
    if Encryption.EncryptionMethod <> tetNone then begin
      dEncRequest := TMemoryStream.Create;
      dUnEncResponse := TMemoryStream.Create;
      try
        DoDecryption(aRequeststream,dEncRequest);
        TriggerReadFromStream(dEncRequest);
        result := dispatcher.ProcessMessage(aTransport, dEncRequest, dUnEncResponse); // This traps exception and cannot fail
        TriggerWriteToStream(dUnEncResponse);
        DoEncryption(dUnEncResponse,aResponsestream);
      finally
        dEncRequest.Free;
        dUnEncResponse.free;
      end;
    end
    else
    {$ENDIF}
    begin
      TriggerReadFromStream(aRequeststream);
      result := dispatcher.ProcessMessage(aTransport, aRequeststream, aResponsestream); // This traps exception and cannot fail
      TriggerWriteToStream(aResponsestream);
    end;

  except
    on E:Exception do begin
      if (E.Message<>'') then excstr := E.Message
                         else excstr := err_UnhandledException;
      {$IFDEF DOTNET}
      aResponseStream.CopyFrom(TStringStream.Create(excstr), Length(excstr));
      {$ELSE}
      aResponseStream.Write(excstr[1], Length(excstr));
      {$ENDIF DOTNET}
      TriggerWriteToStream(aResponsestream);
    end;
  end;

  {aResponsestream.Position := 0;
  if Assigned(OnWriteToStream) then OnWriteToStream(aResponsestream);
  aResponsestream.Position := 0;}
end;

function TROServer.GetActive: boolean;
begin
  result := IntGetActive;
end;

procedure TROServer.Loaded;
begin
  inherited;

  IntSetActive(FALSE);
  Active := fLoadActive;
  fDoneAfterLoad := TRUE;
end;

procedure TROServer.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;

  if (Operation=opRemove) and (aComponent is TROMessage)
    then fDispatchers.CleanReferences(TROMessage(aComponent));
end;

procedure TROServer.SetActive(const Value: boolean);
begin
  if (csLoading in ComponentState) then begin
    fDoneAfterLoad := FALSE;
    fLoadActive := Value
  end

  else begin
    if (Value=Active) then Exit;

    case Value of
      TRUE: if Assigned(fOnBeforeServerActivate) then fOnBeforeServerActivate(Self);
      FALSE : if Assigned(fOnBeforeServerDeactivate) then fOnBeforeServerDeactivate(Self);
    end;

    IntSetActive(Value);

    if (Value=Active) then begin
      case Value of
        TRUE: if Assigned(fOnAfterServerActivate) then fOnAfterServerActivate(Self);
        FALSE : if Assigned(fOnAfterServerDeactivate) then fOnAfterServerDeactivate(Self);
      end;
    end;
  end;
end;

procedure TROServer.SetDispatchers(const Value: TROMessageDispatchers);
begin
  fDispatchers := Value;
end;

function TROServer.GetDispatchersClass : TROMessageDispatchersClass;
begin
  result := TROMessageDispatchers; // Default
end;


{ TROClassFactory }

constructor TROClassFactory.Create(const anInterfaceName: string;
  aCreatorFunc: TRORemotableCreatorFunc;
  anInvokerClass : TROInvokerClass);
begin
  inherited Create;

  fInvoker := NIL;
  fCreatorFunc := aCreatorFunc;
  fInterfaceName := anInterfaceName;
  fInvokerClass := anInvokerClass;

  RegisterClassFactory(Self);
end;

procedure TROClassFactory.CreateInstance(out anInstance : IInterface);
begin
  fCreatorFunc(anInstance);
end;

destructor TROClassFactory.Destroy;
begin
  inherited;
end;

function TROClassFactory.GetInterfaceName: string;
begin
  result := fInterfaceName
end;

function TROClassFactory.GetInvoker: IROInvoker;
begin
  if (fInvoker=NIL)
    then fInvoker := fInvokerClass.Create; // Only creates it when really needed

  result := fInvoker;
end;

procedure TROClassFactory.ReleaseInstance(var anInstance: IInterface);
begin
  anInstance := NIL;
end;

initialization
  _ServerClasses := TClassList.Create;
  _ClassFactoryList := NIL;

finalization
  _ClassFactoryList.Free;
  _ServerClasses.Free;
end.
