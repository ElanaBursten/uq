{ Summary: Contains the classes TRODLToXML and TXMLToRODL }
unit uRODLToXML;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - CodeGen
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uRODL;

const XMLFlagNames : array[TRODLParamFlag] of string = (
        'In', 'Out', 'InOut', 'Result');


type {---------------- TRODLToXML ----------------}

     { Summary: RODL converter that transforms a TRODLLibrary in the standard RemObjects RODL XML format
       Description:
         RODL converter that transforms a TRODLLibrary in the standard RemObjects RODL XML format. }
     TRODLToXML = class(TRODLConverter)
     private
     protected
       procedure IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); override;
     public
     end;

     {---------------- TXMLToRODL ----------------}

     { Summary: RODL reader that transforms a standard RemObjects RODL XML file into a TRODLLibrary
       Description:
         RODL reader that transforms a standard RemObjects RODL XML file into a TRODLLibrary. }
     TXMLToRODL = class(TRODLReader)
     private
     protected
       function IntReadFromStream(aStream : TStream) : TRODLLibrary; override;

     public
       { Summary: Quick access function
         Description:
           Quick access function that doesn't force you to pass through a TStream
           when you already have the RODL XML string. }
       function ReadFromString(const aString : string) : TRODLLibrary;
     end;

function XMLFlagNameToFlag(const aName : string) : TRODLParamFlag;

implementation

uses SysUtils,
     {$IFDEF DELPHI5}
     ComObj,
     {$ENDIF}
     uRORes, TypInfo, uROXDOM_2_3;

function XMLFlagNameToFlag(const aName : string) : TRODLParamFlag;
var f : TRODLParamFlag;
begin
  result := fIn;
  
  for f := Low(TRODLParamFlag) to High(TRODLParamFlag) do
    if (CompareText(XMLFlagNames[f], aName)=0) then begin
      result := f;
      Exit;
    end;

  RaiseError(err_InvalidParamFlag, [aName]);
end;

procedure TRODLToXML.IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = '');

  function Indent(SpaceCount : byte) : string;
  var i : integer;
  begin
    result := '';
    for i := 1 to SpaceCount do result := result+' ';
  end;

  procedure WriteAttributes(var XML : string; Info : TRODLNameInfo);
  var attr : integer;
  begin
    if (Info.Attributes.Count>0) then begin
      with Info.Attributes do begin
        xml := xml+Indent(6)+'<CustomAttributes>'+CRLF;

        for attr := 0 to (Info.Attributes.Count-1) do
          xml := xml+Indent(9)+Format('<%s Value="%s" />',
                 [Names[attr], Values[Names[attr]]])+CRLF;

        xml := xml+Indent(6)+'</CustomAttributes>'+CRLF;
      end;
    end;
  end;

var i, k, m, p : integer;
    s, xml : string;
begin
  with aLibrary do try
    xml := Format('<Library Name="%s" UID="%s" Documentation="%s">', [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;
    WriteAttributes(xml, Info); // Attributes
    // Services
    xml := xml+Indent(3)+'<Services>'+CRLF;
    for i := 0 to (ServiceCount-1) do begin
      with Services[i] do begin
        xml := xml+Indent(6)+Format('<Service Name="%s">', [Info.Name])+CRLF;

        WriteAttributes(xml, Info); // Attributes

        xml := xml+Indent(6)+'<Interfaces>'+CRLF;

        for k := 0 to (Count-1) do begin
          with Items[k] do begin
            xml := xml+Indent(9)+
                      Format('<Interface Name="%s" UID="%s" Documentation="%s">', [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;

            WriteAttributes(xml, Info); // Attributes

            xml := xml+Indent(6)+'<Operations>'+CRLF;
            for m := 0 to (Count-1) do begin
              with Items[m] do begin
                xml := xml+Indent(9)+
                   Format('<Operation Name="%s" UID="%s" Documentation="%s">', [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;

                WriteAttributes(xml, Info); // Attributes

                 xml := xml+Indent(6)+'<Parameters>'+CRLF;

                    for p := 0 to (Count-1) do begin
                       with Items[p] do begin
                          if (info.Attributes.Count>0)
                            then begin
                              s := Format('<Parameter Name="%s" DataType="%s" Flag="%s">', [Info.Name, Info.DataType, XMLFlagNames[Info.Flag]])+CRLF;
                              WriteAttributes(s, Info); // Attributes
                              s := s+'</Parameter>'+CRLF;
                            end
                            else s := Format('<Parameter Name="%s" DataType="%s" Flag="%s" />', [Info.Name, Info.DataType, XMLFlagNames[Info.Flag]])+CRLF;

                          xml := xml+Indent(9)+s;
                       end;
                    end;
                 xml := xml+Indent(6)+'</Parameters>'+CRLF;
                 xml := xml+Indent(9)+'</Operation>'+CRLF;
              end;
            end;
            xml := xml+Indent(6)+'</Operations>'+CRLF;

            xml := xml+Indent(9)+'</Interface>'+CRLF;
          end;
        end;

        xml := xml+Indent(6)+'</Interfaces>'+CRLF;
        xml := xml+Indent(6)+'</Service>'+CRLF;
      end;
    end;
    xml := xml+Indent(3)+'</Services>'+CRLF;

    // Structs
    xml := xml+Indent(3)+'<Structs>'+CRLF;
    for i := 0 to (StructCount-1) do begin

       with Structs[i] do begin
        xml := xml+Indent(6)+Format('<Struct Name="%s" UID="%s" Documentation="%s">',
                  [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;
        WriteAttributes(xml, Info); // Attributes
        xml := xml+Indent(3)+'<Elements>'+CRLF;
        for k := 0 to (Count-1) do begin
          with Items[k] do
            xml := xml+Indent(9)+
              Format('<Element Name="%s" DataType="%s" />', [Info.Name, Info.DataType])+CRLF;
        end;
        xml := xml+Indent(3)+'</Elements>'+CRLF;
        xml := xml+Indent(6)+'</Struct>'+CRLF;
      end;

    end;
    xml := xml+Indent(3)+'</Structs>'+CRLF;

    // Enums
    xml := xml+Indent(3)+'<Enums>'+CRLF;
    for i := 0 to (EnumCount-1) do begin

       with Enums[i] do begin
        xml := xml+Indent(6)+Format('<Enum Name="%s" UID="%s" Documentation="%s">', [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;
        WriteAttributes(xml, Info); // Attributes
        xml := xml+Indent(3)+'<EnumValues>'+CRLF;
        for k := 0 to (Count-1) do begin
          with Items[k] do
            xml := xml+Indent(9)+
              Format('<EnumValue Name="%s" />', [Info.Name])+CRLF;
        end;
        xml := xml+Indent(3)+'</EnumValues>'+CRLF;
        xml := xml+Indent(6)+'</Enum>'+CRLF;
      end;

    end;
    xml := xml+Indent(3)+'</Enums>'+CRLF;

    // Arrays
    xml := xml+Indent(3)+'<Arrays>'+CRLF;
    for i := 0 to (ArrayCount-1) do begin
       with Arrays[i] do begin
        xml := xml+Indent(6)+Format('<Array Name="%s" UID="%s" Documentation="%s">',
           [Info.Name, GUIDToString(Info.UID), Info.Documentation])+CRLF;
        WriteAttributes(xml, Info); // Attributes
        xml := xml+Indent(3)+Format('<ElementType DataType="%s" />', [ElementType])+CRLF;

        xml := xml+Indent(6)+'</Array>'+CRLF;
      end;

    end;
    xml := xml+Indent(3)+'</Arrays>'+CRLF;

    xml := xml+'</Library>'+CRLF;
  finally
    Buffer.Text := xml;
  end;
end;

{ TXMLToRODL }
function GetNodeAttribute(aNode : TdomNode; const anAttributeName : string) : string;
begin
  if Assigned(aNode) and Assigned(aNode.attributes.getNamedItem(anATtributeName))
    then result := aNode.attributes.getNamedItem(anAttributeName).textContent
    else begin
      if (UpperCase(anAttributeName)='UID')
        then result := GUIDToString(EmptyGUID)
        else result := '';
    end;
end;

function TXMLToRODL.IntReadFromStream(aStream: TStream): TRODLLibrary;

  procedure ReadAttributes(anXMLNode : TdomNode; Info : TRODLNameInfo);
  var i, k : integer;
      name, value : string;
  begin
    if (anXMLNode=NIL) then Exit;

    value := '';
    for i := 0 to (anXMLNode.childNodes.length-1) do begin
      if (anXMLNode.childNodes.item(i).nodeName='CustomAttributes') then begin
        with anXMLNode.childNodes.item(i).childNodes do begin
          for k := 0 to (length-1) do begin
            name := anXMLNode.childNodes.item(i).childNodes.item(k).nodeName;

            if (name='#text') then Continue;
            if (anXMLNode.childNodes.item(i).childNodes.item(k).attributes.getNamedItem('Value')<>NIL)
              then value := anXMLNode.childNodes.item(i).childNodes.item(k).attributes.getNamedItem('Value').nodeValue
              else value := '';

            Info.Attributes.Values[item(k).nodeName] := value;
          end;
        end;

        Exit;
      end;
    end;
  end;

var domimpl : TDomImplementation;
    parser : TXmlToDomParser;
    xmldoc: TDOMDocument;
    list,
    sublist,
    subsublist,
    lastlist : TDomNodeList;
    i, k, m, p: Integer;

    struct : TRODLStruct;
    stelem : TRODLTypedEntity;
    arr : TRODLArray;
    svc : TRODLService;
    enum : TRODLEnum;
    eval : TRODLEnumValue;
    intf : TRODLServiceInterface;
    op : TRODLOperation;
    par : TRODLOperationParam;

begin
  domimpl := TDomImplementation.Create(NIL);
  parser := TXmlToDomParser.Create(NIL);
  parser.DOMImpl := domimpl;

  result := TRODLLibrary.Create;

  try
    aStream.Position := 0;
    xmldoc := parser.streamToDom(aStream);

    // Library
    if Assigned(xmldoc.documentElement) then begin
      result.Info.Name := GetNodeAttribute(xmldoc.documentElement, 'Name');
      result.Info.UID := StringToGUID(GetNodeAttribute(xmldoc.documentElement, 'UID'));
      result.Info.Documentation := GetNodeAttribute(xmldoc.documentElement, 'Documentation');

      ReadAttributes(xmldoc.documentElement, result.Info);
    end;

    // Services
    list := xmldoc.documentElement.getElementsByTagName('Service');
    if Assigned(list) then begin
      for i := 0 to (list.length-1) do begin
        svc := TRODLService.Create;
        svc.Info.Name := GetNodeAttribute(list.item(i), 'Name');

        ReadAttributes(list.item(i), svc.Info);

        // Default interface
        // TODO: Implement multiple interfaces in the future. Not needed now
        sublist := TDomElement(list.item(i)).getElementsByTagName('Interface');
        if Assigned(sublist) and Assigned(sublist.item(0)) then begin
          //for k := 0 to (sublist.Length-1) do begin
            k := 0; // Fixed thanks to a compiler warning! Was using m as index
            
            intf := svc.Default;
            intf.Info.Name := GetNodeAttribute(sublist.item(k), 'Name');
            intf.Info.UID := StringToGUID(GetNodeAttribute(sublist.item(k), 'UID')); // Fixed thanks to a compiler warning! Was using m as index
            intf.Info.Documentation := GetNodeAttribute(sublist.item(k), 'Documentation');

            ReadAttributes(sublist.item(k), intf.Info);

            // Operations
            subsublist := TDOMElement(sublist.item(k)).getElementsByTagName('Operation');
            if Assigned(subsublist) then begin
              for m := 0 to (subsublist.length-1) do begin
                op := intf.Add;
                op.Info.Name := GetNodeAttribute(subsublist.item(m), 'Name');
                op.Info.Documentation := GetNodeAttribute(subsublist.item(m), 'Documentation');
                op.Info.UID := StringToGUID(GetNodeAttribute(subsublist.item(m), 'UID'));

                ReadAttributes(subsublist.item(m), op.Info);

                // Parameters
                lastlist := TDomElement(subsublist.item(m)).getElementsByTagName('Parameter');
                if Assigned(lastlist) then begin
                  for p := 0 to (lastlist.length-1) do begin
                    par := op.Add;
                    par.Info.Name := GetNodeAttribute(lastlist.item(p), 'Name');
                    par.Info.Flag := XMLFlagNameToFlag(GetNodeAttribute(lastlist.item(p), 'Flag'));
                    par.Info.DataType :=  GetNodeAttribute(lastlist.item(p), 'DataType');
                  end;
                end;
              end;
            end;
          //end;
        end;

        result.Add(svc);
      end;
    end;

    // Structs
    list := xmldoc.documentElement.getElementsByTagName('Struct');
    if Assigned(list) then begin
      for i := 0 to (list.length-1) do begin
        struct := TRODLStruct.Create;
        struct.Info.Name := GetNodeAttribute(list.item(i), 'Name');
        struct.Info.Documentation := GetNodeAttribute(list.item(i), 'Documentation');
        struct.Info.UID := StringToGUID(GetNodeAttribute(list.item(i), 'UID'));

        ReadAttributes(list.item(i), struct.Info);

        sublist := TDOMElement(list.item(i)).getElementsByTagName('Element');
        if Assigned(sublist) then begin
          for p := 0 to (sublist.length-1) do begin
            stelem := struct.Add;
            stelem.Info.Name := GetNodeAttribute(sublist.item(p), 'Name');
            stelem.Info.DataType := GetNodeAttribute(sublist.item(p), 'DataType');
          end;
        end;

        result.Add(struct);
      end;
    end;

    // Enums
    list := xmldoc.documentElement.getElementsByTagName('Enum');
    if Assigned(list) then begin
      for i := 0 to (list.length-1) do begin
        enum := TRODLEnum.Create;
        enum.Info.Name := GetNodeAttribute(list.item(i), 'Name');
        enum.Info.Documentation := GetNodeAttribute(list.item(i), 'Documentation');
        enum.Info.UID := StringToGUID(GetNodeAttribute(list.item(i), 'UID'));

        ReadAttributes(list.item(i), enum.Info);

        sublist := TDOMElement(list.item(i)).getElementsByTagName('EnumValue');
        if Assigned(sublist) then begin
          for p := 0 to (sublist.length-1) do begin
            eval := enum.Add;
            eval.Info.Name := GetNodeAttribute(sublist.item(p), 'Name');
          end;
        end;

        result.Add(enum);
      end;
    end;

    // Arrays
    list := xmldoc.documentElement.getElementsByTagName('Array');
    if Assigned(list) and Assigned(list.item(0)) then begin
      for i := 0 to (list.length-1) do begin
        arr := TRODLArray.Create;
        arr.Info.Name := GetNodeAttribute(list.item(i), 'Name');
        arr.Info.Documentation := GetNodeAttribute(list.item(i), 'Documentation');
        arr.Info.UID := StringToGUID(GetNodeAttribute(list.item(i), 'UID'));

        ReadAttributes(list.item(i), arr.Info);

        sublist := TDOMElement(list.item(i)).getElementsByTagName('ElementType');
        if Assigned(sublist) and (sublist.length>0) then
          arr.ElementType := GetNodeAttribute(sublist.item(0), 'DataType');

        result.Add(arr)
      end;
    end;

  finally
    parser.Free;
    domimpl.Free;
  end;
end;

function TXMLToRODL.ReadFromString(const aString: string): TRODLLibrary;
var ss : TStringStream;
begin
  ss := TStringStream.Create(aString);
  try
    result := Read(ss);
  finally
    ss.Free;
  end;
end;

end.
