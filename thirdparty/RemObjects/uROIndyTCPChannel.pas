{ Summary: Contains the class TROIndyTCPChannel }
unit uROIndyTCPChannel;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Indy Components
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROProxy, IdBaseComponent, uROClientIntf, IdTCPClient;

type {--------------- TROIndyTCPChannel ---------------}

     { Summary: Channel for remote TCP/IP communication based on the Indy TIdTCPClient component
       Description:
        It dispaches messages to the server by using the Indy TIdTCPClient component.
        The internal Indy client is accessible using the Indy client property which is also visible in
        the Object inspector in Delphi 6 and up. }

     TROIndyTCPChannel = class(TROTransportChannel, IROTransport, IROTCPTransport)
     private
       fIndyClient : TIdTCPClient;

     protected
       { Summary: Overridden IntDispatch function.
         Description:
           Overridden IntDispatch function.
           Handles the messaging with the server using the internal TCP Indy client's WriteStream and ReadStream methods. }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       { Summary: Creates the internal Indy client
         Description:
           Creates the internal Indy client. It returns a TIdTCPClient client. }
       function CreateIndyClient : TIdTCPClient; virtual;

       {--------------- IROTransport ---------------}
       function GetTransportObject : TObject; override;

       {--------------- IROTCPTransport ---------------}
       function GetClientAddress : string;

     public
       constructor Create(aOwner : TComponent); override;

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property IndyClient : TIdTCPClient read fIndyClient;
     end;

implementation

uses SysUtils, uRORes;

{ TROIndyTCPChannel }

constructor TROIndyTCPChannel.Create(aOwner: TComponent);
begin
  inherited;
  fIndyClient := CreateIndyClient;
  fIndyClient.Name := 'InternalIndyClient';
  {$IFDEF DELPHI6UP}
  fIndyClient.SetSubComponent(TRUE);
  {$ENDIF}
end;

function TROIndyTCPChannel.CreateIndyClient: TIdTCPClient;
begin
  result := TIdTCPClient.Create(Self);
  result.Port := 8090;
  result.Host := '127.0.0.1';
end;

function TROIndyTCPChannel.GetClientAddress: string;
begin
  result := '';
end;

function TROIndyTCPChannel.GetTransportObject: TObject;
begin
  result := Self;
end;

procedure TROIndyTCPChannel.IntDispatch(aRequest, aResponse: TStream);
begin
  try
    IndyClient.Connect;
    IndyClient.WriteStream(aRequest, TRUE, TRUE);
    IndyClient.ReadStream(aResponse);
  finally
    IndyClient.Disconnect;
  end;
end;

end.
