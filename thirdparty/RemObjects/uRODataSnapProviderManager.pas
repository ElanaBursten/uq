unit uRODataSnapProviderManager;

interface

uses Contnrs, Provider,
     uRODataSnapProviderPublisher, uRODataSnap_Intf;

type TProviderManager = class(TObjectList)
     public
       constructor Create(); 
       procedure RegisterPublisher(iPublisher:TROCustomDataSnapProviderPublisher);
       procedure UnregisterPublisher(iPublisher:TROCustomDataSnapProviderPublisher);
       function GetProviderNames:TProviderNames;
       function GetProviderByName(const iProviderName:string):TCustomProvider;
     end;

var ProviderManager:TProviderManager;

implementation

uses SysUtils, Classes,
     uRODataSnapRes;

{ TProviderManager }

constructor TProviderManager.Create;
begin
  inherited Create(false);
end;

function TProviderManager.GetProviderByName(const iProviderName:string): TCustomProvider;
var i:integer;
begin
  //ToDo: protect by CriticalSection;

  result := nil;
  for i := 0 to Count-1 do begin
    result := (Items[i] as TRODataSnapProviderPublisher).GetProviderByName(iProviderName);
    if Assigned(result) then break;
  end; { for }
  if not Assigned(result) then raise Exception.CreateFmt(sProviderNotExported,[iProviderName]);
end;

function TProviderManager.GetProviderNames: TProviderNames;
var i:integer;
begin
  //ToDo: protect by CriticalSection;
  result := TProviderNames.Create();
  for i := 0 to Count-1 do begin
    (Items[i] as TRODataSnapProviderPublisher).AddNamesToTProviderNames(result);
  end; { for }
end;

procedure TProviderManager.RegisterPublisher(iPublisher: TROCustomDataSnapProviderPublisher);
begin
  //ToDo: protect by CriticalSection;
  if IndexOf(iPublisher) = -1 then Add(iPublisher);
end;

procedure TProviderManager.UnregisterPublisher(iPublisher: TROCustomDataSnapProviderPublisher);
var lIndex:integer;
begin
  lIndex := IndexOf(iPublisher);
  if IndexOf(iPublisher) <> -1 then Delete(lIndex);
end;

initialization
  ProviderManager := TProviderManager.Create();
finalization
  FreeAndNil(ProviderManager);

end.
