unit RemObjects_IDE_Reg;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

procedure Register;

implementation

uses Classes, uROServer, 
     {$IFDEF DELPHI5}
     DsgnIntf
     {$ELSE}
     DesignIntf, DesignEditors
     {$ENDIF},
     fDispatchersEditorForm;

type
  TDispatchersProperty = class(TClassProperty)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

{ TDispatchersProperty }

procedure TDispatchersProperty.Edit;
begin
  with TDispatchersEditorForm.Create(TROServer(GetComponent(0))) do try
    ShowModal;
  finally
    Free;
  end;
end;

function TDispatchersProperty.GetAttributes: TPropertyAttributes;
begin
  result := [paDialog]
end;

{ Register }

procedure Register;
begin
  RegisterPropertyEditor(TypeInfo(TROMessageDispatchers), TROServer, 'Dispatchers', TDispatchersProperty);
end;

end.
