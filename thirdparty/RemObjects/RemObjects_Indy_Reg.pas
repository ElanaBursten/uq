unit RemObjects_Indy_Reg;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Indy Components
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

procedure Register;

implementation

uses Classes,
     uRORes,
     uROIndyTCPChannel, uROIndyHTTPChannel,
     uROIndyHTTPServer, uROIndyTCPServer;

procedure Register;
begin
  RegisterComponents(str_ProductName,
    [TROIndyTCPChannel, TROIndyHTTPChannel, TROIndyHTTPServer, TROIndyTCPServer]);

end;

end.
