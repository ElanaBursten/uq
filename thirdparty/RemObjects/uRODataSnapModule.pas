unit uRODataSnapModule;

interface

uses Classes, Provider,
     uRODataSnap_Intf, uRODataSnapPublishedProvidersCollection,
     uRODataSnapBaseAppServer;


type TRODataSnapModule = class(TRODataSnapBaseAppServer)
     private
       fProviders: TPublishedProviders;
       fUseProviders: boolean;
       procedure SetProviders(const Value: TPublishedProviders);
     protected
       function GetProviderByName(const iProviderName:string):TCustomProvider; override;
       function GetProviderNames:TProviderNames; override;
     protected
       procedure Notification(AComponent: TComponent; Operation: TOperation); override;
     public
       constructor Create(AOwner: TComponent); override;
       destructor Destroy; override;
     published
       property Providers:TPublishedProviders read fProviders write SetProviders;
       property UseProviders:boolean read fUseProviders write fUseProviders default false;
     end;

implementation

uses SysUtils,
     uRODataSnapRes;

{ TRODataSnapModule }

constructor TRODataSnapModule.Create(AOwner: TComponent);
begin
  fProviders := TPublishedProviders.Create(self);
  inherited;
end;

destructor TRODataSnapModule.Destroy;
begin
  FreeAndNil(fProviders);
  inherited;
end;

function TRODataSnapModule.GetProviderByName(const iProviderName: string): TCustomProvider;
var lPublishedProvider: TPublishedProvider;
    lProvider:TCustomProvider;
    i:integer;
begin
  if UseProviders then begin
    lPublishedProvider := Providers.GetProviderByName(iProviderName);
    if not Assigned(lPublishedProvider) then raise Exception.CreateFmt(sProviderNotExported,[iProviderName]);
    result := lPublishedProvider.Provider;
    if not Assigned(result) then raise Exception.CreateFmt(sProviderNotExported,[iProviderName]);
  end
  else begin
    result := nil;
    for i := 0 to ComponentCount-1 do begin
      if Components[i] is TCustomProvider then begin
        lProvider := TCustomProvider(Components[i]);
        if lProvider.Exported and (lProvider.Name = iProviderName) then begin
          result := lProvider;
          break;
        end;
      end;
    end; { for }
    if not Assigned(result) then raise Exception.CreateFmt(sProviderNotExported,[iProviderName]);
  end;
end;

function TRODataSnapModule.GetProviderNames: TProviderNames;
var i:integer;
begin
  result := TProviderNames.Create();

  if UseProviders then begin
    for i := 0 to Providers.Count-1 do begin
      if (Providers[i].Name <> '') and Assigned(Providers[i].Provider) then result.Add(Providers[i].Name);
    end;
  end
  else begin
    for i := 0 to ComponentCount-1 do begin
      if Components[i] is TCustomProvider then with TCustomProvider(Components[i]) do begin
        if Exported then result.Add(Name);
      end;
    end;    { for }
  end;
end;

procedure TRODataSnapModule.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then begin
    if (AComponent is TCustomProvider) and Assigned(Providers) then
      Providers.UnlinkProvider(AComponent as TCustomProvider);
  end;
end;

procedure TRODataSnapModule.SetProviders(const Value: TPublishedProviders);
begin
  fProviders.Assign(Value);
end;

end.
