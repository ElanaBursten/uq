unit uRORODLNotifier;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Classes, ToolsApi, uROIDETools;

type
  { TRORODLNotifier }
  TRORODLNotifier = class(TNotifierObject, IOTAIDENotifier, IOTAIDENotifier50)
  private
    fMessages : TIDEMessageList;

  protected
    procedure FileNotification(NotifyCode: TOTAFileNotification;
      const FileName: string; var Cancel: Boolean);

    procedure AfterCompile(Succeeded: Boolean); overload;
    procedure BeforeCompile(const Project: IOTAProject; var Cancel: Boolean); overload;
    procedure AfterCompile(Succeeded: Boolean; IsCodeInsight: Boolean); overload;

  public
    procedure BeforeCompile(const Project: IOTAProject; IsCodeInsight: Boolean;
      var Cancel: Boolean); overload;

    constructor Create;
    destructor Destroy; override;

  end;

procedure Register;

implementation

uses uRODLGenTools, uRODL, Controls, uRORes, SysUtils, Dialogs,
     uRODLToXML, uRODLToInvk, uRODLToIntf, uRODLToImpl;

var notifieridx : integer = -1;

procedure Register;
begin
  notifieridx := Services.AddNotifier(TRORODLNotifier.Create);
end;

procedure RemoveNotifier;
begin
  if (notifieridx<>-1)
    then Services.RemoveNotifier(notifieridx);
end;

function ProcessProject(const Project: IOTAProject; aMessageList : TIDEMessageList) : boolean;

  function GenUnit(aConverterClass : TRODLConverterClass; aLibrary : TRODLLibrary; aServiceName : string = ''; SkipIfPresent : boolean = FALSE) : boolean;
  var conv : TRODLConverter;
      unitname : string;
      module : IOTAModule;
  begin
    result := FALSE;

    conv := NIL;
    try
      conv := aConverterClass.Create(NIL);
      unitname := ExtractFilePath(Project.FileName)+conv.GetTargetFileName(aLibrary, aServiceName);

      // Checks the module is already in the project
      module := FindModule(Project, unitname);

      // If it is and the file is not supposed to be regenerated just exists
      if (module<>NIL) and SkipIfPresent then begin
        aMessageList.Add(mWarning, 'Skipping regeneration of unit '+unitname);
        result := TRUE;
        Exit;
      end;

      // Converts the RODL file
      conv.Convert(aLibrary, aServiceName);

      if (module=NIL) then begin
        // If not present adds it double checking for file existance if SkipIfPresent. It's for implementation
        // units that might be there and have code already in but not be part of the DPR (for any weird reason)

        if SkipIfPresent and FileExists(unitname) then
          case MessageDlg(
            Format('Unit "%s" has been prepared but a file with the same name already exist in "%s".'#13+
                   'Do you want to overwrite file %s?',
                   [ExtractFileName(unitname), ExtractFilePath(unitname), ExtractFileName(unitname)]),
                   mtWarning, [mbYes, mbNo, mbCancel], 0) of

            mrCancel : Exit;
            mrYes : conv.Buffer.SaveToFile(unitname);
          end

        else conv.Buffer.SaveToFile(unitname);

        Project.AddFile(unitname, FALSE); // Adds it to the project regardless

        if (aConverterClass<>TRODLToInvk)
          then ActionServices.OpenFile(unitname);

      end
      else begin
        // Otherwise updates the source
        WriteModuleSource(module, Trim(conv.Buffer.Text));
      end;

      result := TRUE;
    finally
      conv.Free;
    end;
  end;

var prjname,
    source,
    rodlname : string;
    lib : TRODLLibrary;
    i : integer;
begin
  result := TRUE;

  // Only processes DPRs
  prjname := Project.FileName;
  if (CompareText(ExtractFileExt(prjname), '.dpr')<>0) then Exit;

  // Extract useful data and exists if the file does not reference a RODL file
  source := ReadModuleSource(Project);
  rodlname := ExtractRODLFileName(source);

  if (rodlname='') then Exit
  else if (ExtractFilePath(rodlname)='')
    then rodlname := ExtractFilePath(prjname)+rodlname; // If there's no path specified assumes the file is in the DPR folder

  // Processes RODL
  lib := ReadRODLFromFile(TXMLToRODL, rodlname);

  aMessageList.Add(mInfo, rodlname+' has been loaded');

  // Generates the units
  if not GenUnit(TRODLToIntf, lib) then begin
    result := FALSE;
    Exit;
  end
  else aMessageList.Add(mInfo, 'Interface unit generated');

  if not GenUnit(TRODLToInvk, lib) then begin
    result := FALSE;
    Exit;
  end
  else aMessageList.Add(mInfo, 'Invokation unit generated');

  for i := 0 to (lib.ServiceCount-1) do
    if not GenUnit(TRODLToImpl, lib, lib.Services[i].Info.Name, TRUE) then begin
      result := FALSE;
      Exit;
    end
    else aMessageList.Add(mInfo,
             Format('Implementation unit for service %s generated',
             [lib.Services[i].Info.Name]));

  // Finally generates the RES file
  if GenerateRESFromRODL(rodlname, aMessageList)
    then aMessageList.Add(mInfo, 'RODL resource generated.')
    else begin
      aMessageList.Add(mError, 'RODL resource could not be generated.');
      result := FALSE
    end;
end;

{ TRORODLNotifier }

procedure TRORODLNotifier.AfterCompile(Succeeded, IsCodeInsight: Boolean);
begin
end;

procedure TRORODLNotifier.AfterCompile(Succeeded: Boolean);
begin

end;

procedure TRORODLNotifier.BeforeCompile(const Project: IOTAProject;
  var Cancel: Boolean);
begin

end;

procedure TRORODLNotifier.BeforeCompile(const Project: IOTAProject;
  IsCodeInsight: Boolean; var Cancel: Boolean);
begin
  if IsCodeInsight then Exit;

  fMessages.ClearMessages();

  try
    try
      Cancel := not ProcessProject(Project, fMessages);
    except
      on E:Exception do begin
        fMessages.Add(mError, E.Message);
        Cancel := TRUE;
      end;
    end;

    if Cancel then begin
      fMessages.Add(mError, 'Compile aborted');
      Beep;
    end;
    if (fMessages.COunt>0)
      then fMessages.FlushMessages;
  except
    on E:Exception do begin
      MessageDlg('The RemObject preprocessor raised an unhandled excaption.'#13+
                 E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

constructor TRORODLNotifier.Create;
begin
  fMessages := TIDEMessageList.Create(str_ProductName);
end;

destructor TRORODLNotifier.Destroy;
begin
  fMessages.Free;
  inherited;
end;

procedure TRORODLNotifier.FileNotification(
  NotifyCode: TOTAFileNotification; const FileName: string;
  var Cancel: Boolean);
begin

end;

initialization

finalization
  RemoveNotifier;

end.
