This directory contains the ancient RemObjects version used in Q Manager, modified:

* To build and run in D2006

* To build and run all in this directory, rather than in the various directories it uses out-of-the-box

When RO is finally upgraded, stop using the contents of this directory entirely - simply use the RO files as installed by the current RO installer.


------------------

How to Install:


Open the project group BuildPackages_D10.

Build and install these packages, in order:

Core
IDE

Skip the RODX, BPDX and DataSnap projects, we do not use those in QM.

Add this (where this README is) to your PATH.  On my machines it is:

C:\projects\Utiliquest\qmanager\thirdparty\RemObjects


Copy your RO ".lic" file to this directory.

