{ Summary: Contains all the interfaces implemented by classes used client side. }
unit uROClientIntf;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.Classes,
  {$ELSE}
  Classes, SysUtils, TypInfo,
  {$ENDIF}
  uRORes;


     { Summary: Message types contained in the Header of binary messages
       Description:
         Message types contained in the Header of binary messages.
         Either regular message or server side exception. }

const MESSAGE_TYPE_MESSAGE = 0;
      MESSAGE_TYPE_EXCEPTION = 1;
      MESSAGE_TYPE_QUERY_SERICE_INFO = 2;

      MAX_ITEM_NAME = $100; { max name length for interface, function etc. }
      MAX_EXCEPTION_TEXT = $10000; { max exception text }


type TMessageType = (typMessage          {$IFDEF VER140UP}= MESSAGE_TYPE_MESSAGE{$ENDIF},
                     typException        {$IFDEF VER140UP}= MESSAGE_TYPE_EXCEPTION{$ENDIF},
                     typQueryServiceInfo {$IFDEF VER140UP}= MESSAGE_TYPE_QUERY_SERICE_INFO{$ENDIF});

                     { for Delphi 5 know we can reply on the correct value-assignment 0,1,2 }

     { Old definition before .NET conversion}
     {type TMessageType = (typMessage, typException, typQueryServiceInfo);

type {---------------------- Forward declarations ----------------------}
     IROMessage = interface;

     {---------------------- Exceptions ----------------------}
     EROServerException = class(Exception);

     {---------------------- Misc ----------------------}

     DateTime = TDateTime; { Alias for the type TDateTime }

     Binary = TMemoryStream; { Alias for the TMemoryStream type }

     {---------------------- IROMessage ----------------------}

     { Summary: Flags used during the serialization of message parameters
       Description: Flags used during the serialization of message parameters. }

     TParamAttribute = (paIsDateTime, paIsArrayElement);
     TParamAttributes = set of TParamAttribute; { Set of TParamAttribute }

     IROTransport = interface;

     { Summary: Interface implemented by all message classes.
       Description:
         IROMessage defines the interface all message classes have to implement to serialize parameters
         and prepare a stream which will be bradcasted from the client to the server and vice versa. }
     IROMessage = interface
     ['{092FB8BE-5FC4-48CB-BB50-623465168B98}']
       { Summary: Initializes the message
         Description:
           Initializes the message by setting the internal InterfaceName and MessageName properties.
           Feel free to override this method but always remember to call the inherited version. }
       procedure Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName : string);
       { Summary: Finalizes the message
         Description:
           Finalizes the message. The implementation in TROMessage doesn't do anything. }
       procedure Finalize;

       { Summary: Writes a message parameter
         Description:
           Writes a message parameter by calling the method Write of the internal serializer.
           There shouldn't be any reason to override this method. Parameter serialization should be implemented at the
           TROSerializer level. See CreateSerializer. }
       {$IFDEF DOTNET}
       {$ELSE}
       procedure Write(const aName : string; aTypeInfo : PTypeInfo; const Ptr; ExtraAttributes : TParamAttributes);
       {$ENDIF}

       { Summary: Reads a message parameter
         Description:
           Reades a message parameter by calling the method Read of the internal serializer.
           There shouldn't be any reason to override this method. Parameter serialization should be implemented at the
           TROSerializer level. See CreateSerializer. }
       {$IFDEF DOTNET}
       {$ELSE}
       procedure Read(const aName : string; aTypeInfo : PTypeInfo; var Ptr; ExtraAttributes : TParamAttributes);
       {$ENDIF}

       { Summary: Returns the interface name
         Description: Returns the interface name. }
       function GetInterfaceName : string;
       { Summary: Returns the message name
         Description: Returns the message name. (Note: think of message names as method names). }
       function GetMessageName : string;

       function GetSessionID:TSessionID;

       { Summary: Sets the interface name
         Description: Sets the interface name. }
       procedure SetInterfaceName(const aValue : string);
       { Summary: Sets the message name
         Description: Sets the message name. (Note: think of message names as method names). }
       procedure SetMessageName(const aValue : string);

       { Summary: Writes the message to a TStream
         Description:
           Writes the message to a TStream. Each descendant of TROMessage provide an actual implementation to do
           this. TROMessage.WriteToStream only fires the event OnWriteToStream. After this methos is executed
           the stream position is set to 0. }
       procedure WriteToStream(aStream : TStream);
       { Summary: Reads the message from a stream
         Description:
           Reads the message from a TStream. Each descendant of TROMessage provide an actual implementation to do
           this. TROMessage.ReadFromStream only fires the event OnReadFromStream. After this methos is executed
           the stream position is set to 0. }
       procedure ReadFromStream(aStream : TStream);

       { Summary: Writes an exception to the stream
         Description:
           Writes an exception to the stream. Different types of messages handles this functionality differently.
           For instance, TROSOAPMessage writes a SOAP Fault while TROBINMessage sets its header's MessageType field
           to typException. You must override this method in every descendant. }

       procedure WriteException(aStream : TStream; anException : Exception);

(*       { Summary: Fires the exception specified in the message
         Description:
           If the message contains an exception (i.e. SOAP Fault) this method is called and it's supposed to raise an
           Delphi exception. It's your responsibility to fire it appropriately. }

       procedure RaiseException(anException : Exception);*)

       { Summary: MessageName Accessor
         Description: MessageName Accessor. In OO terms, imagine MessageName to be the name of the method you want to invoke. }
       property MessageName : string read GetMessageName write SetMessageName;
       { Summary: InterfaceName Accessor
         Description: InterfaceName Accessor. }
       property InterfaceName : string read GetInterfaceName write SetInterfaceName;

       property SessionID:TSessionID read GetSessionID;
     end;

     IROMessageCloneable = interface
     ['{4AA33660-25B7-4794-A978-CB791640475D}']
       function Clone:IROMessage; 
     end;

     {---------------------- IROTransport ----------------------}

     { Summary: Transport interface implemented by client transport channels and servers
       Description:
         Transport interface implemented by client transport channels and servers.
         This interface is expecially useful server side when intercepting method calls.
         See TROInvoker.BeforeInvoke and TROInvoker.AfterInvoke for more information on method interception.
     }
     IROTransport = interface
     ['{56FA09B9-FFC8-4432-80E3-BF78E5E7DF33}']
       { Summary: Provides direct access to the transport object
         Description:
           Provides direct access to the transport object. The result has to be type-casted to the appropriate class type
           to get additional information. Each transport channel and server class return a pointer to different type
           of object. See the DispatchNotifier demo for a practical use of this property. }
       function GetTransportObject : TObject;
     end;

     {---------------------- IROTransportChannel ----------------------}

     { Summary: Interface required to be implemented by all client side transport channels
       Description:
         Interface required to be implemented by all client side transport channels. Through the use
         of its Dispatch method, the channels are able to broadcast messages to servers and receive back a response. }

     IROTransportChannel = interface(IROTransport)
     ['{EDFA0CF3-3265-46C9-AC5C-14C3CACF2721}']
       { Summary: Used by transport channels to broadcast messages to servers
         Description:
           Used by transport channels to broadcast messages to servers.
           Different channels implement this method according to the protocol or media they use to broadcast (i.e. TCP/IP,
           Windows messages, etc). }
       procedure Dispatch(aRequest, aResponse : TStream);
     end;

     {---------------------- IROTCPTransport ----------------------}

     { Summary: TCP/IP Transport interface implemented by TCP/IP client transport channels and servers
       Description:
         TCP/IP Transport interface implemented by TCP/IP client transport channels and servers.
         It extents IROTransport by providing the client's IP address through the property ClientAddress.
         It's is only implemented by TCP channels and servers. }

     IROTCPTransport = interface(IROTransport)
     ['{C80E773D-B8A8-4E0D-9B49-2668EFC1E27F}']
       function GetClientAddress : string;

       { Summary: ClientAddress accessor
         Description:
           Client IP address accessor. Returns the IP address of the client sending the request.
           Only meaningful when used server side. }
       property ClientAddress : string read GetClientAddress;
     end;

     {---------------------- IROHTTPTransport ----------------------}

     { Summary: HTTP Transport interface implemented by HTTP client transport channels and servers
       Description:
         TCP/IP Transport interface implemented by TCP/IP client transport channels and servers.
         It extents IROTCPTransport by providing the access to the HTTP headers.
         It's is only implemented by HTTP channels and servers. }

     IROHTTPTransport = interface(IROTCPTransport)
     ['{AD51DB69-993F-49C1-9F3B-4C678A95007D}']
       procedure SetHeaders(const aName, aValue : string);
       function GetHeaders(const aName : string) : string;

       function GetContentType : string;
       procedure SetContentType(const aValue : string);

       function GetUserAgent : string;
       procedure SetUserAgent(const aValue : string);

       function GetTargetURL : string;
       procedure SetTargetURL(const aValue : string);

       function GetPathInfo : string;
       function GetLocation : string;

       { Summary: HTTP Content-Type header accessor
         Description: HTTP Content-Type header accessor. You could also set it using the property Headers }
       property ContentType : string read GetContentType write SetContentType;
       { Summary: HTTP UserAgent header accessor
         Description: HTTP UserAgent header accessor. You could also set it using the property Headers }
       property UserAgent : string read GetUserAgent write SetUserAgent;
       { Summary: Specifies the target URL for the message dispatch
         Description: Specifies the target URL for the message dispatch. }
       property TargetURL : string read GetTargetURL write SetTargetURL;
       { Summary: HTTP headers accessor
         Description: HTTP headers accessor. }
       property Headers[const aName : string] : string read GetHeaders write SetHeaders;

       property PathInfo : string read GetPathInfo;

       property Location : string read GetLocation;
     end;

     {---------------------- IROModuleInfo ----------------------}

     { Summary: Datatype to indicate what type of metadata format IROModuleInfo returns
       Description: Datatype to indicate what type of metadata format IROModuleInfo returns. }
     TDataFormat = (dfXML, dfBinary);

     { Summary: Interface required to be implemented by message classes to extract RODL metadata
       Description:
         Interface required to be implemented by message classes to extract RODL metadata and convert it to a
         format specific to the message type. }
     IROModuleInfo = interface
     ['{83ED3A44-DA4B-4F4E-884A-DDF5E726A2C1}']

       { Summary: Reads the server metadata and converts it to the standard the message implements
         Description:
           Reads the server metadata and converts it to the standard the message implements.
           TROMessage extracts the RODL resource linked by the RemObjects preprocessor to the file.
           Descendants such as TROSOAPMessage convert it in other formats (i.e. WSDL) using classes
           such as TRODLToWSDL. }

       procedure GetModuleInfo(aStream : TStream; const aTransport : IROTransport; var aFormat : TDataFormat);
     end;

implementation

end.
