{ Summary: Contains the class TROIndyTCPServer }
unit uROIndyTCPServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Indy Components
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

{TODO: Do like in the webmodule. Save a pointer to the user's OnExecute and fire it }

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROServer, uROClientIntf, IdTCPServer, IdComponent, IdThreadMgr, IdSocketHandle, IdIntercept;

type {--------------- TIndyTCPConnectionTransport ---------------}

     { Summary: Implementation of the IROTransport and IROTCPTransport interfaces for TROIndyTCPServer servers
       Description:
         Implementation of the IROTransport and IROTCPTransport interfaces for TROIndyTCPServer servers.
         This class allows you to access the Indy connection thread which originated the server-side call.
         See TROServer.DispatchMessage for more information. }
     TIndyTCPConnectionTransport = class(TInterfacedObject, IROTransport, IROTCPTransport)
     private
       fThread: TIdPeerThread;

     protected
       {--------------- IROTransport ---------------}
       function GetTransportObject : TObject;
       function GetClientAddress : string;

     public
       constructor Create(aThread: TIdPeerThread);

       { Summary: Indy connection thread which originated the server-side call
         Description: Indy connection thread which originated the server-side call. }
       property Thread : TIdPeerThread read fThread;
     end;

     {--------------- TROTIdTCPServer ---------------}

     { Summary: Internal. TIdTCPServer descendant that hides the Active property.
       Description:
         Internal. TIdTCPServer descendant that hides the Active property.
         This has been done to prevent user from accessing the Active property directly in
         the Object Inspector. This should be done using TROIndyTCPServer.Active. }
     TROTIdTCPServer = class(TIdTCPServer)
     private
       function GetActive: boolean;

     public
       procedure IndySetActive(Value : boolean);

     published
       { Summary: Used to verify if the Indy TCP server is listening on the specified port for TCP/IP requests.
         Description: Used to verify if the Indy TCP server is listening on the specified port for TCP/IP requests. }
       property Active : boolean read GetActive;
     end;

     {--------------- TROIndyTCPServer ---------------}

     { Summary: RemObjects TCP/IP server based on Nevrona's Indy.
       Description: RemObjects TCP/IP server based on Nevrona's Indy.
     }
     TROIndyTCPServer = class(TROServer, IROTransport)
     private
       fIndyServer : TIdTCPServer;

       function GetIndyServer : TROTIdTCPServer;

     protected

       function CreateIndyServer : TIdTCPServer; virtual;
       procedure IntSetActive(const Value: boolean); override;
       function IntGetActive : boolean; override;

       procedure InternalOnExecute(AThread: TIdPeerThread);

       {--------------- IROTransport ---------------}
       function GetTransportObject : TObject;

     public
       constructor Create(aComponent : TComponent); override;

     published
       { Summary: Provides access to the internal (vendor-specific) class used to send and receive data
         Description:
           Provides access to the internal (vendor-specific) class used to send and receive data.
           Refer to the vendor's documentation for additional help on the properties.

           DELPHI 5: Do NOT try to access or change this property from Delphi 5's Object Inspector.
                     Due to limitations of Delphi 5, subcomponents are not properly accessible via the old Object Inspector. }

       property IndyServer: TROTIdTCPServer read GetIndyServer;
     end;

implementation

uses SysUtils, uRORes, IdTCPConnection;

{ TROIndyTCPServer }

constructor TROIndyTCPServer.Create(aComponent: TComponent);
begin
  inherited;

  fIndyServer := CreateIndyServer;
  fIndyServer.Name := 'InternalIndyServer';
  {$IFDEF DELPHI6UP}
  fIndyServer.SetSubComponent(True);
  {$ENDIF}
end;

function TROIndyTCPServer.CreateIndyServer: TIdTCPServer;
begin
  result := TROTIdTCPServer.Create(Self);
  result.OnExecute := InternalOnExecute;
  result.DefaultPort := 8090;
end;

function TROIndyTCPServer.GetIndyServer: TROTIdTCPServer;
begin
  result := TROTIdTCPServer(fIndyServer)
end;

procedure TROIndyTCPServer.IntSetActive(const Value: boolean);
begin
  IndyServer.IndySetActive(Value);
end;

function TROIndyTCPServer.IntGetActive : boolean;
begin
  result := IndyServer.Active
end;

procedure TROIndyTCPServer.InternalOnExecute(AThread: TIdPeerThread);
var req, resp : TStringStream;
    tcptransport : IROTCPTransport;
begin
  req := TStringStream.Create('');
  resp := TStringStream.Create('');

  tcptransport := TIndyTCPConnectionTransport.Create(aThread);

  try
    with AThread do begin
      Connection.ReadStream(req);

      DispatchMessage(tcptransport, req, resp);
      
      Connection.WriteStream(resp, TRUE, TRUE);

      Connection.Disconnect;
    end;
  finally
    req.Free;
    resp.Free;
  end;
end;

function TROIndyTCPServer.GetTransportObject: TObject;
begin
  result := Self;
end;

{ TROTIdTCPServer }

function TROTIdTCPServer.GetActive: boolean;
begin
  result := inherited Active;
end;

procedure TROTIdTCPServer.IndySetActive(Value: boolean);
begin
  inherited Active := Value
end;

{ TIndyTCPConnectionTransport }

constructor TIndyTCPConnectionTransport.Create(aThread: TIdPeerThread);
begin
  inherited Create;
  fThread := aThread;
end;

function TIndyTCPConnectionTransport.GetClientAddress: string;
begin
{$IFDEF RemObjects_INDY8}
  result := fThread.Connection.Binding.PeerIP
{$ENDIF}
{$IFDEF RemObjects_INDY9}
  result := fThread.Connection.Socket.Binding.PeerIP
{$ENDIF}
end;

function TIndyTCPConnectionTransport.GetTransportObject: TObject;
begin
  result := Self;
end;

initialization
  RegisterServerClass(TROIndyTCPServer);

end.
