unit uRODataSnapConnection;

interface

uses DbClient, Classes, Midas,
     ActiveX, ComObj,{ for ISupportErrorInfo }
     uROClient, uROProxy,
     uRODataSnap_Intf;

type TROCustomDataSnapConnection = class(TCustomRemoteServer, Midas.IAppServer, ISupportErrorInfo)
     private
       fMessage: TROMessage;
       fChannel: TRoTransportChannel;
       fStoreConnected: boolean;
       fProxy:uRODataSnap_Intf.IAppServer;
       procedure SetMessage(const Value: TROMessage);
       procedure SetChannel(const Value: TRoTransportChannel);

     private

       { IAppServer }
       function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; safecall;
       function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; safecall;
       procedure AS_Execute(const ProviderName, CommandText: WideString; var Params, OwnerData: OleVariant); safecall;
       function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; safecall;
       function AS_GetProviderNames: OleVariant; safecall;
       function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; Options: Integer; const CommandText: WideString; var Params, OwnerData: OleVariant): OleVariant; safecall;
       function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; var OwnerData: OleVariant): OleVariant; safecall;

       { ISupportErrorInfo }
       function InterfaceSupportsErrorInfo(const iid: TIID): HResult; stdcall;

     protected
       function GetConnected: boolean; override;
       procedure SetConnected(Value: boolean); override;
       procedure GetProviderNames(Proc: TGetStrProc); override;
       procedure Notification(AComponent: TComponent; Operation: TOperation); override;
     public
       function GetServer: MIdas.IAppServer; override;

       property StoreConnected:boolean read fStoreConnected write fStoreConnected default false;
       property Connected:boolean read GetConnected write SetConnected stored fStoreConnected default false;
       property Message:TROMessage read fMessage write SetMessage;
       property Channel:TRoTransportChannel read fChannel write SetChannel;


       {$IFDEF MSWINDOWS}
       function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
       {$ENDIF}

     end;

     TRODataSnapConnection = class(TROCustomDataSnapConnection)
     published
       property Message;
       property Channel;
       property StoreConnected;
       property Connected;
     end;

implementation

uses Dialogs,
     SysUtils, Variants,
     {$IFDEF DEBUG_REMOBJECTS_DATASNAP}eDebugServer,{$ENDIF}
     uROClientIntf,uROBinaryHelpers;

{ TROCustomDataSnapConnection }

function TROCustomDataSnapConnection.AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer;  out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant;
var lDelta,lResult:Binary;
    lOwnerData:string;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_ApplyUpdates(ProviderName=%s,MaxErrors=%d)',[ProviderName,MaxErrors]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lOwnerData := OwnerData;
  lDelta := BinaryFromVariant(Delta);
  try
    lResult := fProxy.AS_ApplyUpdates(ProviderName,lDelta,MaxErrors,ErrorCount,lOwnerData);
    try
      result := VariantFromBinary(lResult);
    finally
      FreeAndNil(lResult);
    end;
  finally
    FreeAndNil(lDelta);
  end;
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_ApplyUpdates(ErrorCount=%d)',[ErrorCount]); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant;
var lResult:Binary;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_DataRequest(ProviderName=%s)',[ProviderName]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lResult := fProxy.AS_DataRequest(ProviderName,BinaryFromVariant(Data));
  try
    result := VariantFromBinary(lResult);
  finally
    FreeAndNil(lResult);
  end;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_DataRequest()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

procedure TROCustomDataSnapConnection.AS_Execute(const ProviderName,CommandText: WideString; var Params, OwnerData: OleVariant);
var lParams:Binary;
    lOwnerData:string;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_Execute(ProviderName=%s,CommandText=%s)',[ProviderName,CommandText]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lOwnerData := OwnerData;
  lParams := BinaryFromVariant(Params);
  try
    fProxy.AS_Execute(ProviderName,CommandText,lParams,lOwnerData);
  finally
    freeAndNil(lParams);
  end;
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_Execute()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant;
var lResult:Binary;
    lOwnerData:string;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_GetParams(ProviderName=%s)',[ProviderName]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lOwnerData := OwnerData;
  lResult := fProxy.AS_GetParams(ProviderName,lOwnerData);
  try
    result := VariantFromBinary(lResult);
  finally
    FreeAndNil(lResult);
  end;
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_GetParams()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.AS_GetProviderNames: OleVariant;
var lProviderNames:TProviderNames;
    i:integer;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_GetProviderNames()'); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lProviderNames := fProxy.AS_GetProviderNames();
  try
    result := VarArrayCreate([0,lProviderNames.Count-1],varString);
    for i := 0 to lProviderNames.Count-1 do begin
      result[i] := lProviderNames[i];
    end;    { for }
  finally
    lProviderNames.Free();
  end;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_GetProviderNames()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; Options: Integer; const CommandText: WideString; var Params, OwnerData: OleVariant): OleVariant;
var lParams,lResult:Binary;
    lOwnerData:string;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_GetRecords(ProviderName=%s,Count=%d,Options=%d)',[ProviderName,Count,Options]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lOwnerData := OwnerData;
  lParams := BinaryFromVariant(Params);
  try
    lResult := fProxy.AS_GetRecords(ProviderName,Count,RecsOut,Options,CommandText,lParams,lOwnerData);
    try
      result := VariantFromBinary(lResult);
    finally
      FreeAndNil(lResult);
    end;
  finally
    FreeAndNil(lParams);
  end;
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_GetRecords(RecsOut=%d)',[RecsOut]); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; var OwnerData: OleVariant): OleVariant;
var lRow,lResult:Binary;
    lOwnerData:string;
begin
  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  DebugServer.EnterMethodEx(self,'TROCustomDataSnapConnection.AS_RowRequest(ProviderName=%s,RequestType=%d)',[ProviderName,RequestType]); try
  try
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}

  Connected := true;
  lOwnerData := OwnerData;
  lRow := BinaryFromVariant(Row);
  try
    lResult := fProxy.AS_RowRequest(ProviderName,lRow,RequestType,lOwnerData);
    try
      result := VariantFromBinary(lResult);
    finally
      FreeAndNil(lResult);
    end;
  finally
    FreeAndNil(lRow);
  end;
  OwnerData := lOwnerData;

  {$IFDEF DEBUG_REMOBJECTS_DATASNAP}
  except DebugServer.WriteException(); raise; end;
  finally DebugServer.ExitMethodEx(self,'TROCustomDataSnapConnection.AS_RowRequest()'); end;
  {$ENDIF DEBUG_REMOBJECTS_DATASNAP}
end;

function TROCustomDataSnapConnection.GetConnected: boolean;
begin
  result := Assigned(fProxy);
end;

function TROCustomDataSnapConnection.GetServer: Midas.IAppServer;
begin
  result := self;
end;

procedure TROCustomDataSnapConnection.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then begin
    if AComponent = fMessage then fMessage := nil
    else if AComponent = Channel then Channel := nil;
  end;
end;

procedure TROCustomDataSnapConnection.SetChannel(const Value:TRoTransportChannel);
begin
  fChannel := Value;
  if Assigned(fChannel) then fChannel.FreeNotification(self);
end;

procedure TROCustomDataSnapConnection.SetConnected(Value: boolean);
begin
  if Value = Connected then exit;

  if Value then begin

    if not Assigned(fMessage) then raise Exception.Create('Cannot connect: No Message assigned.');
    if not Assigned(fChannel) then raise Exception.Create('Cannot connect: No TransportChannel assigned.');
    fProxy := CoIAppServer.Create(fMessage,fChannel);

  end
  else begin

    fProxy := nil;

  end;
end;

procedure TROCustomDataSnapConnection.SetMessage(const Value:TROMessage);
begin
  fMessage := Value;
  if Assigned(fMessage) then fMessage.FreeNotification(self);
end;


procedure TROCustomDataSnapConnection.GetProviderNames(Proc: TGetStrProc);
var lProviderNames:TProviderNames;
    i:integer;
begin
  { This version is a bit optimized over the default one, because we can
    skip converting to a variant array first. }

  Connected := True;
  lProviderNames := fProxy.AS_GetProviderNames();
  try
    for i := 0 to lProviderNames.Count-1 do begin
      Proc(lProviderNames[i]);
    end;    { for }
  finally
    lProviderNames.Free();
  end;

end;

function TROCustomDataSnapConnection.InterfaceSupportsErrorInfo(
  const iid: TIID): HResult;
begin
  if GetInterfaceEntry(iid) <> nil then result := S_OK else result := S_FALSE;
end;

function TROCustomDataSnapConnection.SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult;
const lErrorGuid : TGUID = '{00000000-0000-0000-0000-000000000000}';
begin
  Result := HandleSafeCallException(ExceptObject, ExceptAddr, lErrorGuid, '', '');
end;

end.
