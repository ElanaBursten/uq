unit uRODLToInvk;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - CodeGen
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses uRODL;

type { TRODLToInvk }
     TRODLToInvk = class(TRODLConverter)
     private
       procedure WriteInvokerDeclaration(const aService: TRODLService; aLibrary : TRODLLibrary);
       procedure WriteOperationImplementation(const aService : TRODLService;
            const anOperation : TRODLOperation;
            aLibrary : TRODLLibrary);

     protected
       procedure IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); override;

     public
       class function GetTargetFileName(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''): string; override;

     end;


implementation

uses SysUtils, uRODLGenTools, uROTypes, uRODLToImpl, Dialogs, uRODLToPascal;

{ TRODLToInvk }

procedure TRODLToInvk.IntConvert(const aLibrary: TRODLLIbrary; const aTargetEntity : string = '');
var i, k : integer;
    s : string;
begin                          
  Write(Format('unit %s;', [ChangeFileExt(GetTargetFileName(aLibrary), '')]));
  WriteEmptyLine;

  Write(IntfInvkNotice);
  WriteEmptyLine;

  Write('interface');
  WriteEmptyLine;

  Write('uses');
  Write('{vcl:} Classes,' ,PASCAL_INDENTATION_LEVEL_1);
  Write('{RemObjects:} uROServer, uROServerIntf, uROClientIntf,' ,PASCAL_INDENTATION_LEVEL_1);
  Write(Format('{Generated:} %s_Intf;', [aLibrary.Info.Name]),PASCAL_INDENTATION_LEVEL_1);
  WriteEmptyLine;

  if aLibrary.Count > 0 then Write('type');

  for i := 0 to (aLibrary.ServiceCount-1) do begin
    WriteInvokerDeclaration(aLibrary.Services[i], aLibrary);
    WriteEmptyLine;
  end;
  WriteEmptyLine;

  Write('implementation');
  WriteEmptyLine;

  s := '';
  for i := 0 to (aLibrary.ServiceCount-1) do begin
    //s := s+ChangeFileExt(TRODLToImpl.GetTargetFileName(aLibrary, aLibrary.Services[i].Info.Name),'');
    s := s+aLibrary.Services[i].Info.Name+'_Impl';

    if (i<aLibrary.ServiceCount-1) then s := s+', ';
  end;

  Write('uses');
  if (Trim(s)<>'')
    then Write(Format('{RemObjects:} uRORes, %s;', [s]),PASCAL_INDENTATION_LEVEL_1)
    else Write(Format('{RemObjects:} uRORes;', []),PASCAL_INDENTATION_LEVEL_1);
  WriteEmptyLine;

  for i := 0 to (aLibrary.ServiceCount-1) do begin
    //WriteInvokerHandleMessage(aLibrary.Services[i]);

    if not Assigned(aLibrary.Services[i].Default) then Continue;

    Write('{ T'+aLibrary.Services[i].Info.Name+'_Invoker }');
    WriteEmptyLine;

    for k := 0 to (aLibrary.Services[i].Default.Count-1) do
      {s := Format('aaa %s ', ['mmm']);
      Buffer.Add(s);
      //}   WriteOperationImplementation(aLibrary.Services[i], aLibrary.Services[i].Default.Items[k], aLibrary);

  end;

  for i := 0 to (aLibrary.ServiceCount-1) do ;


  WriteEmptyLine;
  Write('end.');
end;

procedure TRODLToInvk.WriteInvokerDeclaration(const aService : TRODLService; aLibrary : TRODLLibrary);
var i : integer;
begin
  Write(Format('T%s_Invoker = class(TROInvoker)', [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
  Write('private', PASCAL_INDENTATION_LEVEL_1);
  Write('protected', PASCAL_INDENTATION_LEVEL_1);
  Write('published', PASCAL_INDENTATION_LEVEL_1);

  for i := 0 to (aService.Default.Count-1) do begin
    Write(Format('procedure Invoke_%s(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);', [aService.Default.Items[i].Info.Name]), PASCAL_INDENTATION_LEVEL_2);
  end;

  Write('end;', PASCAL_INDENTATION_LEVEL_1);
end;

procedure TRODLToInvk.WriteOperationImplementation(
  const aService : TRODLService;
  const anOperation: TRODLOperation; aLibrary : TRODLLibrary);
const resultname = 'Result';
var i : integer;
    pars, s, sa : string;
begin
  Write(Format('procedure T%s_Invoker.Invoke_%s(const __Instance : IInterface; const __Message : IROMessage; const __Transport : IROTransport);',
               [aService.Info.Name, anOperation.Info.Name]));

  Write(Format('{ %s }', [GetOperationDefinition(anOperation)]));

  with anOperation do begin
    if (Count>0) then Write('var');

    for i := 0 to (Count-1) do
      Write(Format('%s: %s;', [Items[i].Info.Name, Items[i].Info.DataType]),PASCAL_INDENTATION_LEVEL_1);

{    if Assigned(Result)
      then Write('    %s: %s;', [resultname, Result.Info.DataType]);}

    Write('begin');
    if (Count>0) then begin
      for i := 0 to (Count-1) do
        with Items[i] do
          if IsImplementedAsClass(Info.DataType, aLibrary) then
            Write(Format('%s := NIL;', [Items[i].Info.Name]),PASCAL_INDENTATION_LEVEL_1);
    end;

    Write('  try');

    for i := 0 to (Count-1) do
      if IsInputFlag(Items[i].Info.Flag) then begin

        if (StrToDataType(Items[i].Info.DataType)=rtDateTime)
          then sa := '[paIsDateTime]'
          else sa := '[]';

        Write(Format('__Message.Read(''%s'', TypeInfo(%s), %s, %s);', [Items[i].Info.Name, Items[i].Info.DataType, Items[i].Info.Name, sa]),PASCAL_INDENTATION_LEVEL_2)
      end;

    if (Count>0) then WriteEmptyLine;
    s := '    ';
    if Assigned(Result) then s := s+Result.Info.Name+' := ';
    s := s+Format('(__Instance as %s).%s', [aService.Info.Name, anOperation.Info.Name]);

    if (Count>0) then begin
      pars := '';
      for i := 0 to (Count-1) do begin
        if (Items[i].Info.Flag=fResult) then Continue;
        pars := pars+Items[i].Info.Name+', ';
      end;

      if pars<>'' then s := s+'('+Copy(pars, 1, Length(pars)-2)+')';
    end;

    s := s+';';
    Write(s);

    WriteEmptyLine;
    Write(Format('    __Message.Initialize(__Transport, ''%s'', ''%sResponse'');',[aService.Info.Name, Info.Name]));
    for i := 0 to (Count-1) do
      if IsOutputFlag(Items[i].Info.Flag) then begin

        if (StrToDataType(Items[i].Info.DataType)=rtDateTime)
          then sa := '[paIsDateTime]'
          else sa := '[]';

        Write(Format('    __Message.Write(''%s'', TypeInfo(%s), %s, %s);', [Items[i].Info.Name, Items[i].Info.DataType, Items[i].Info.Name, sa]));
      end;

    {if Assigned(Result) then begin
      Write(Format('  __Message.Write(''%s'', TypeInfo(%s), @%s);', [resultname, Result.Info.DataType, resultname]))
    end;}
    Write('    __Message.Finalize;');
    WriteEmptyLine;

    Write('  finally');
    for i := 0 to (Count-1) do
      with Items[i] do
        if IsImplementedAsClass(Info.DataType, aLibrary) then
          Write(Format('    %s.Free;', [Items[i].Info.Name]));
    Write('  end;');
    Write('end;');
  end;
  WriteEmptyLine;
end;

class function TRODLToInvk.GetTargetFileName(
  const aLibrary: TRODLLIbrary; const aTargetEntity: string): string;
begin
  result := aLibrary.Info.Name+'_Invk.pas';
end;

end.
