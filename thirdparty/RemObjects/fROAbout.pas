unit fROAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TAboutForm = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Button1: TButton;
    lbl_Version: TLabel;
    procedure OnCloseClick(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutForm: TAboutForm;

implementation

uses ShellAPI;

{$R *.dfm}

type TVersion = record
       Major,Minor,Release,Build:word;
     end;

function GetFileVersion(iFileName:string):TVersion;
var whocares:dword;
    Size:dword;
    Data:pointer;
    FixedData:pVSFixedFileInfo;
begin
  Size := GetFileVersionInfoSize(pChar(iFileName), whocares);
  if Size > 0 then begin
    GetMem(Data,Size);
    try
      if GetFileVersionInfo(pChar(iFileName),0,Size,Data) then begin
        Size := sizeof(TVSFixedFileInfo);
        if VerQueryValue(Data,'\',pointer(FixedData),Size) then begin
          result.Major := HiWord(FixedData^.dwFileVersionMS);
          result.Minor := LoWord(FixedData^.dwFileVersionMS);
          result.Release := HiWord(FixedData^.dwFileVersionLS);
          result.Build := LoWord(FixedData^.dwFileVersionLS);
        end;
      end;
    finally
      FreeMem(Data);
    end;
  end;
end;

function GetModuleName:string;
var Buffer: array[0..MAX_PATH] of Char;
begin
  SetString(result, Buffer, GetModuleFileName(HInstance, Buffer, MAX_PATH));
end;

function VersionStringLong:string;
var Version:TVersion;
begin
  Version := GetFileVersion(GetModuleName());
  result := IntToStr(Version.Major)+'.'+IntToStr(Version.Minor)+'.'+
            IntToStr(Version.Release)+'.'+IntToStr(Version.Build){+'.'+
            FormatDateTime('yymmdd', CompileTime);}
end;

procedure TAboutForm.OnCloseClick(Sender: TObject);
begin
  Close();
end;

procedure TAboutForm.Label4Click(Sender: TObject);
begin
  ShellExecute(Handle,'open','mailto:alef@remobjects.com',nil,nil,SW_SHOWNORMAL);
end;

procedure TAboutForm.Label5Click(Sender: TObject);
begin
  ShellExecute(Handle,'open','mailto:mh@remobjects.com',nil,nil,SW_SHOWNORMAL);
end;

procedure TAboutForm.Label3Click(Sender: TObject);
begin
  ShellExecute(Handle,'open','http://www.remobjects.com',nil,nil,SW_SHOWNORMAL);
end;

procedure TAboutForm.FormCreate(Sender: TObject);
begin
  lbl_Version.Caption := VersionStringLong();
end;

end.
