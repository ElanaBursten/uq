unit uRODLLChannel;

interface

uses Classes, uROClient, uROClientIntf, uROProxy;

type TDLLProcessMessage = procedure(aRequestStream, aResponseStream : TStream);

const DLLProcessMessageName = 'DLLProcessMessage';

type { TRODLLChannel }
     TRODLLChannel = class(TROTransportChannel)
     private
       fDLLName: string;
       fDLLHandle : Cardinal;
       fKeepDLLLoaded: boolean;
       fDLLProcessMessage : TDLLProcessMessage;

     protected
       { IROTransport }
       function GetTransportObject : TObject; override;

       { TROTransportChannel }
       procedure IntDispatch(aRequest, aResponse : TStream); override;

       procedure LoadDLL;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

       procedure UnloadDLL;

     published
       property DLLName : string read fDLLName write fDLLName;
       property KeepDLLLoaded : boolean read fKeepDLLLoaded write fKeepDLLLoaded;
     end;

implementation

uses Windows, SysUtils, uRORes;

{ TRODLLChannel }

constructor TRODLLChannel.Create(aOwner: TComponent);
begin
  inherited;
  fDLLHandle := 0;
  fDLLProcessMessage := NIL;
  fKeepDLLLoaded := TRUE;
end;

destructor TRODLLChannel.Destroy;
begin
  UnloadDLL;
  inherited;
end;

function TRODLLChannel.GetTransportObject: TObject;
begin
  result := Self;
end;

procedure TRODLLChannel.IntDispatch(aRequest, aResponse: TStream);
begin
  try
    LoadDLL;
    fDLLProcessMessage(aRequest, aResponse);
  finally
    if not KeepDLLLoaded then UnloadDLL;
  end;
end;

procedure TRODLLChannel.LoadDLL;
begin
  if (fDLLHandle>0) then Exit // Already loaded
  else if not FileExists(DLLName) then RaiseError('Cannot locate %s', [DLLName]); // TODO: Move in uRORes

  try
    fDLLHandle := LoadLibrary(PChar(DLLName));
    @fDLLProcessMessage := GetProcAddress(fDLLHandle, DLLProcessMessageName);

    if (@fDLLProcessMessage=NIL) then RaiseError('Not a RemObjects DLL', []); // TODO: Move in uRORes
  except
    if (fDLLHandle>0) then begin
      FreeLibrary(fDLLHandle);
      fDLLHandle := 0;
    end;
    @fDLLProcessMessage := NIL;
  end
end;

procedure TRODLLChannel.UnloadDLL;
begin
  if (fDLLHandle<>0) then begin
    FreeLibrary(fDLLHandle);
    fDLLHandle := 0;
  end;
end;

end.
