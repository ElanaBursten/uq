unit uROCiphers;

interface

uses
  Windows, SysUtils, Classes, uroCipher, uroCipher1, uroHash, uroDECUtil;

type
  TROBaseCipher = class(TObject)
  public
    procedure EncryptStream(const Plaintext, Ciphertext: TStream); virtual; abstract;
    procedure DecryptStream(const Ciphertext, Plaintext: TStream); virtual; abstract;
  end;

  TRODECCipher = class(TROBaseCipher)
  protected
    fCipher: TROCipher;
  public
    constructor Create(aCipherClass: TROCipherClass; aKey: string);
    destructor Destroy; override;
    procedure EncryptStream(const Plaintext, Ciphertext: TStream); override;
    procedure DecryptStream(const Ciphertext, Plaintext: TStream); override;
  end;

implementation

{ TRODECCipher }

constructor TRODECCipher.Create(aCipherClass: TROCipherClass; aKey: string);
var
  i                 : Integer;
  keyval            : string;
begin
  fCipher := aCipherClass.Create('', nil);
  fCipher.Mode := cmCTS;
  fCipher.HashClass := TROHash_RipeMD256;

  i := Length(aKey) div 2;
  if i > fCipher.KeySize then
    i := fCipher.KeySize;

  //convert key to binary
  keyval := formattostr(PChar(aKey), Length(aKey), fmthex);

  fCipher.Init(keyval[1], i, nil);
end;

procedure TRODECCipher.DecryptStream(const Ciphertext, Plaintext: TStream);
begin
  inherited;

  fCipher.CodeStream(Ciphertext, Plaintext, Ciphertext.Size - Ciphertext.Position, paDecode);
end;

destructor TRODECCipher.Destroy;
begin
  fCipher.Free;

  inherited;
end;

procedure TRODECCipher.EncryptStream(const Plaintext, Ciphertext: TStream);
begin
  inherited;

  //Plaintext.seek(0, soFromBeginning);
  fCipher.CodeStream(Plaintext, Ciphertext, Plaintext.Size, paEncode);
end;

end.

