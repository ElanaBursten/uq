unit uRODLToIntf;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - CodeGen
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses uRODL;

type { TRODLToIntf }
     TRODLToIntf = class(TRODLConverter)
     private
       procedure WriteTypeDeclaration(aLibrary: TRODLLibrary; aType: TRODLEntity);
       procedure WriteServiceDeclaration(aService: TRODLService);
       procedure WriteCoClass(aService: TRODLService);
       procedure WriteServiceConsts(aService : TRODLService);
       procedure WriteArraySerializer(anArray : TRODLArray);
       procedure WriteStructPropMethods(aLibrary : TRODLLibrary; aStruct: TRODLStruct);

       function IsSOAPService(aService : TRODLService) : boolean;
     protected
       procedure IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); override;

     public
       class function GetTargetFileName(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''): string; override;

     end;

implementation

uses SysUtils, Classes, Dialogs, uROTypes,
     {$IFDEF DELPHI5}
     ComObj,
     {$ENDIF}
     uRODLGenTools, uRODLToPascal;

{ TRODLToIntf }

procedure TRODLToIntf.IntConvert(const aLibrary: TRODLLibrary; const aTargetEntity: string);
var x, i : integer;
    s : string;
begin
  try
    Write(Format('unit %s;', [ChangeFileExt(GetTargetFileName(aLibrary), '')]));
    WriteEmptyLine;

    Write(IntfInvkNotice);
    WriteEmptyLine;

    Write('interface');
    WriteEmptyLine;

    Write('uses');
    Write('{vcl:} Classes, TypInfo,',PASCAL_INDENTATION_LEVEL_1);
    Write('{RemObjects:} uROProxy, uROTypes, uROClientIntf;',PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;

    Write('const');
    Write(Format('LibraryUID = ''%s'';', [GUIDToString(aLibrary.Info.UID)]),PASCAL_INDENTATION_LEVEL_1);
    if (aLibrary.Info.Attributes.Values['Wsdl']<>'')
      then Write(Format('WSDLLocation = ''%s'';', [aLibrary.Info.Attributes.Values['Wsdl']]),PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;

    if aLibrary.ServiceCount > 0 then begin
      for i := 0 to (aLibrary.ServiceCount-1) do WriteServiceConsts(aLibrary.Services[i]);
      WriteEmptyLine;
    end;

    if aLibrary.Count > 0 then Write('type');

    with aLibrary do begin
      // Forward declarations
      Write('{ Forward declarations }',PASCAL_INDENTATION_LEVEL_1);
      for i := 0 to (aLibrary.ServiceCount-1) do
        Write(Format('%s = interface;', [aLibrary.Services[i].Info.Name]), PASCAL_INDENTATION_LEVEL_1);

      for i := 0 to (aLibrary.ArrayCount-1) do
        Write(Format('%s = class;', [aLibrary.Arrays[i].Info.Name]), PASCAL_INDENTATION_LEVEL_1);

      for i := 0 to (aLibrary.StructCount-1) do
        Write(Format('%s = class;', [aLibrary.Structs[i].Info.Name]), PASCAL_INDENTATION_LEVEL_1);

      // Enums
      WriteEmptyLine;
      Write('{ Enumerateds }', PASCAL_INDENTATION_LEVEL_1);
      for i := 0 to (EnumCount-1) do
        with Enums[i] do begin
          s := Info.Name+' = (';
          for x := 0 to (Count-1) do
            s := s+Items[x].Info.Name+',';
          System.Delete(s,Length(s),1);
          s := s+');';

          Write(s, PASCAL_INDENTATION_LEVEL_1);
          WriteEmptyLine;
        end;
    end;

    for i := 0 to (aLibrary.StructCount-1) do
      WriteTypeDeclaration(aLibrary, aLibrary.Structs[i]);

    if aLibrary.ArrayCount>0 then begin
      for i := 0 to (aLibrary.ArrayCount-1) do
        WriteTypeDeclaration(aLibrary, aLibrary.Arrays[i]);
      //WriteEmptyLine;
    end;

    for i := 0 to (aLibrary.ServiceCount-1) do
      WriteServiceDeclaration(aLibrary.Services[i]);

    //WriteEmptyLine;

    Write('implementation');
    WriteEmptyLine;

    Write('uses');
    Write('{vcl:} SysUtils;',PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;

    if aLibrary.ArrayCount>0 then begin
      for i := 0 to (aLibrary.ArrayCount-1) do
        WriteArraySerializer(aLibrary.Arrays[i]);
      //WriteEmptyLine;
    end;

    if aLibrary.StructCount>0 then begin
      for i := 0 to (aLibrary.StructCount-1) do
        WriteStructPropMethods(aLibrary, aLibrary.Structs[i]);
      WriteEmptyLine;
    end;

    for i := 0 to (aLibrary.ServiceCount-1) do
      WriteCoClass(aLibrary.Services[i]);

    Write('initialization');
    for i := 0 to (aLibrary.Count-1) do
      if (aLibrary.Items[i] is TRODLArray) or (aLibrary.Items[i] is TRODLStruct)
        then Write('  RegisterROClass('+aLibrary.Items[i].Info.Name+');');

    //WriteEmptyLine;

    {Write('finalization');
    for i := 0 to (aLibrary.Count-1) do
      if (aLibrary.Items[i] is TRODLArray) or (aLibrary.Items[i] is TRODLStruct)
        then Write('  UnRegisterClass('+aLibrary.Items[i].Info.Name+');');

    WriteEmptyLine;}

    Write('end.');

  finally
  end;
end;

procedure TRODLToIntf.WriteServiceConsts(aService : TRODLService);
var urn : string;
begin
  with aService.Info do
    // Writes additional SOAP information
    if IsSOAPService(aService) then begin
      Write(Format('  %s_EndPointURI = ''%s'';', [Name, Attributes.Values['Location']]));

      if (aService.Default.Count>0) then begin
        urn := aService.Default.Items[0].Info.Attributes.Values['Action'];
        if (urn='') then urn := aService.Default.Items[0].Info.Attributes.Values['InputNamespace']; // Apache ones...
        
        if (Pos('#', urn)>0) then Delete(urn, Pos('#', urn), Length(urn));
        Write(Format('  %s_DefaultURN = ''%s'';', [Name, urn]));
      end;
    end;
end;

procedure TRODLToIntf.WriteServiceDeclaration(aService : TRODLService);
var i : integer;
begin
  with aService.Default do begin
    Write(Format('{ %s }', [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
    Write(Format('%s = interface', [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
    Write(Format('[''%s'']', [GUIDToString(Info.UID)]), PASCAL_INDENTATION_LEVEL_2);

    for i := 0 to (Count-1) do
      Write(GetOperationDefinition(Items[i]), PASCAL_INDENTATION_LEVEL_2);

    Write('end;', PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;

    Write(Format('{ Co%s }', [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
    Write(Format('Co%s = class', [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
    Write(Format('class function Create(const aMessage : IROMessage; aTransportChannel : IROTransportChannel) : %s;',
                 [aService.Info.Name]), PASCAL_INDENTATION_LEVEL_2);
    Write('end;', PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;
  end;
end;

class function TRODLToIntf.GetTargetFileName(const aLibrary: TRODLLibrary;
  const aTargetEntity: string): string;
begin
  try
    result := aLibrary.Info.Name+'_Intf.pas'
  except
    result := 'Unknown.pas';
  end;
end;

procedure TRODLToIntf.WriteCoClass(aService: TRODLService);
var i, p : integer;
    sa, s : string;
    soapsvc : boolean;
begin
  with aService.Default do begin
    soapsvc := IsSOAPService(aService);

    Write('type');
    Write(Format('T%s_Proxy = class(TROProxy, %s)', [aService.Info.Name, aService.Info.Name]),PASCAL_INDENTATION_LEVEL_1);
    Write('private',PASCAL_INDENTATION_LEVEL_1);
    Write('protected',PASCAL_INDENTATION_LEVEL_1);

    for i := 0 to (Count-1) do
      Write(GetOperationDefinition(Items[i]), PASCAL_INDENTATION_LEVEL_2);
    Write('end;',PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;

    Write('{ Co'+aService.Info.Name+' }');
    WriteEmptyLine;

    Write(Format('class function Co%s.Create(const aMessage : IROMessage; aTransportChannel : IROTransportChannel) : %s;',
          [aService.Info.Name, aService.Info.Name]));
    Write('begin');
    Write(Format('  result := T%s_Proxy.Create(aMessage, aTransportChannel);', [aService.Info.Name]));
    Write('end;');
    WriteEmptyLine;

    if Count > 0 then begin
      Write(Format('{ T%s_Proxy }', [aService.Info.Name]));
      WriteEmptyLine;
    end;

    for i := 0 to (Count-1) do begin
      Write(GetOperationDefinition(Items[i], Format('T%s_Proxy', [aService.Info.Name])));
      Write('var __request, __response : TMemoryStream;');
      if soapsvc then Write('__http : IROHTTPTransport;');
      Write('begin');
      Write('__request := TMemoryStream.Create;',PASCAL_INDENTATION_LEVEL_1);
      Write('__response := TMemoryStream.Create;',PASCAL_INDENTATION_LEVEL_1);
      WriteEmptyLine;
      Write('try',PASCAL_INDENTATION_LEVEL_1);

      if soapsvc then begin
        Write(Format('__Message.Initialize(__TransportChannel, %s_DefaultURN, ''%s'');', [aService.Info.Name, Items[i].Info.Name]), 5);

        WriteEmptyLine;
        Write('if Supports(__TransportChannel, IROHTTPTransport, __http) then begin', 5);
        Write(Format('  __http.Headers[''SOAPAction''] := ''"%s"'';', [Items[i].Info.Attributes.Values['Action']]), 5);
        Write(Format('  if (__http.TargetURL='''') then __http.TargetURL := %s_EndPointURI;', [aService.Info.Name]), 5);
        Write('end;', 5);
        WriteEmptyLine;
      end
      else Write(Format('__Message.Initialize(__TransportChannel, ''%s'', ''%s'');', [aService.Info.Name, Items[i].Info.Name]), 5);


      with Items[i] do begin
        for p := 0 to (Count-1) do
          if IsInputFlag(Items[p].Info.Flag) then begin
            if (StrToDataType(Items[p].Info.DataType)=rtDateTime)
              then sa := '[paIsDateTime]'
              else sa := '[]';

            Write(Format('__Message.Write(''%s'', TypeInfo(%s), %s, %s);',
                            [Items[p].Info.Name, Items[p].Info.DataType, Items[p].Info.Name, sa]), 5);
          end;
      end;
      Write('__Message.Finalize;',5);
      WriteEmptyLine;

      Write('__Message.WriteToStream(__request);',5);
      Write('__TransportChannel.Dispatch(__request, __response);',5);
      Write('__Message.ReadFromStream(__response);',5);
      WriteEmptyLine;

      //Write(Format('Message.Initialize(''I%s'', ''%s'');', [aService.Info.Name, Items[i].Info.Name]), 5);
      with Items[i] do begin
        for p := 0 to (Count-1) do
          if IsOutputFlag(Items[p].Info.Flag) then begin

            if (StrToDataType(Items[p].Info.DataType)=rtDateTime)
              then sa := '[paIsDateTime]'
              else sa := '[]';

            if (Items[p].Info.Flag=fResult)
              then s := Format('__Message.Read(''%s'', TypeInfo(%s), result, %s);',
                            [Items[p].Info.Name, Items[p].Info.DataType, sa])

              else s := Format('__Message.Read(''%s'', TypeInfo(%s), %s, %s);',
                            [Items[p].Info.Name, Items[p].Info.DataType, Items[p].Info.Name, sa]);

            Write(s,5);
          end;
      end;
      //Write('Message.Finalize;',5);

      Write('  finally');
      Write('    __request.Free;');
      Write('    __response.Free;');
      Write('  end');
      Write('end;');
      WriteEmptyLine;
    end;

    WriteEmptyLine;
  end;
end;

function TRODLToIntf.IsSOAPService(aService: TRODLService): boolean;
begin
  result := aService.Info.Attributes.Values['Type'] = 'SOAP';
end;

procedure TRODLToIntf.WriteTypeDeclaration(aLibrary: TRODLLibrary; aType: TRODLEntity);
var i : integer;
begin
  Write(Format('{ %s }', [aType.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
  // Structs
  if (aType is TRODLStruct) then with TRODLStruct(aType) do begin
    Write(Format('%s = class(TROComplexType)', [Info.Name]),PASCAL_INDENTATION_LEVEL_1);
    Write('private',PASCAL_INDENTATION_LEVEL_1);
    for i := 0 to (Count-1) do begin
      Write(Format('f%s : %s;', [Items[i].Info.Name, Items[i].Info.DataType]), PASCAL_INDENTATION_LEVEL_2);
      {if IsStruct(Items[i].Info.DataType, aLibrary) or IsArray(Items[i].Info.DataType, aLibrary)
        then Write(Format('fDestroy_%s : boolean;', [Items[i].Info.Name]), PASCAL_INDENTATION_LEVEL_2);}
    end;
    WriteEmptyLine;
    for i := 0 to (Count-1) do begin
      if IsStruct(Items[i].Info.DataType, aLibrary) or IsArray(Items[i].Info.DataType, aLibrary) then begin
        //Write(Format('procedure Set%s(Value : %s);', [Items[i].Info.Name, Items[i].Info.DataType]), PASCAL_INDENTATION_LEVEL_2);
        Write(Format('function Get%s : %s;', [Items[i].Info.Name, Items[i].Info.DataType]), PASCAL_INDENTATION_LEVEL_2);
      end;
    end;

    Write('public',PASCAL_INDENTATION_LEVEL_1);
    {Write('  constructor Create; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  destructor Destroy; override;',PASCAL_INDENTATION_LEVEL_1);}
    WriteEmptyLine;
    Write('published',PASCAL_INDENTATION_LEVEL_1);
    for i := 0 to (Count-1) do begin
      if IsStruct(Items[i].Info.DataType, aLibrary) or IsArray(Items[i].Info.DataType, aLibrary)
        then Write(Format('property %s : %s read Get%s write f%s;',
             [Items[i].Info.Name, Items[i].Info.DataType, Items[i].Info.Name, Items[i].Info.Name]), PASCAL_INDENTATION_LEVEL_2)

        else Write(Format('property %s : %s read f%s write f%s;',
             [Items[i].Info.Name, Items[i].Info.DataType, Items[i].Info.Name, Items[i].Info.Name]), PASCAL_INDENTATION_LEVEL_2);
    end;
    WriteEmptyLine;
    Write('end;', PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;
  end

  // Arrays
  else if (aType is TRODLArray) then with TRODLArray(aType) do begin
    Write(Format('%s = class(TROArray)', [Info.Name, ElementType]),PASCAL_INDENTATION_LEVEL_1);
    Write('private',PASCAL_INDENTATION_LEVEL_1);
    Write(Format('  fItems : array of %s;', [ElementType]), PASCAL_INDENTATION_LEVEL_1);
    Write('protected',PASCAL_INDENTATION_LEVEL_1);
    Write(Format('  function GetItems(Index : integer) : %s;', [ElementType]),PASCAL_INDENTATION_LEVEL_1);
    Write(Format('  procedure SetItems(Index : integer; const Value : %s);', [ElementType]),PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;
    Write('public',PASCAL_INDENTATION_LEVEL_1);
    Write('  class function GetItemType : PTypeInfo; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  class function GetItemClass : TROComplexTypeClass; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  class function GetItemSize : integer; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  function GetItemRef(Index : integer) : pointer; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  procedure SetItemRef(Index: integer; Ref : pointer); override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  procedure Clear; override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  procedure Delete(Index : integer); override;',PASCAL_INDENTATION_LEVEL_1);
    
    Write('  procedure Resize(ElementCount : integer); override;',PASCAL_INDENTATION_LEVEL_1);
    Write('  function GetCount : integer; override;',PASCAL_INDENTATION_LEVEL_1);

    WriteEmptyLine;

    if IsSimpleType(ElementType)
      then Write(Format('  procedure Add(Value : %s);', [ElementType]), PASCAL_INDENTATION_LEVEL_1)
      else Write(Format('  function Add : %s;', [ElementType]), PASCAL_INDENTATION_LEVEL_1);

    WriteEmptyLine;
    Write(Format('  property Items[Index : integer] : %s read GetItems write SetItems; default;', [ElementType]), PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;
    Write('end;',PASCAL_INDENTATION_LEVEL_1);
    WriteEmptyLine;
  end;
end;


procedure TRODLToIntf.WriteArraySerializer(anArray : TRODLArray);
begin
  with anArray do begin
    Write('{ '+Info.Name+' }');
    WriteEmptyLine;

    // GetItemType
    Write(Format('class function %s.GetItemType : PTypeInfo;', [Info.Name]));
    Write('begin');
    Write(Format('  result := TypeInfo(%s);', [ElementType]));
    Write('end;');
    WriteEmptyLine;

    // GetItemClass
    Write(Format('class function %s.GetItemClass : TROComplexTypeClass;', [Info.Name]));
    Write('begin');
    if IsSimpleType(ElementType) then Write('  result := NIL')
    else Write(Format('  result := %s;', [ElementType]));
    Write('end;');
    WriteEmptyLine;

    // GetItemSize
    Write(Format('class function %s.GetItemSize : integer;', [Info.Name]));
    Write('begin');
    Write(Format('  result := SizeOf(%s);', [ElementType]));
    Write('end;');
    WriteEmptyLine;

    // GetItems
    Write(Format('function %s.GetItems(Index : integer) : %s;', [Info.Name, ElementType]));
    Write('begin');
    Write('  result := fItems[Index];');
    Write('end;');
    WriteEmptyLine;

    // GetItemRef
    Write(Format('function %s.GetItemRef(Index : integer) : pointer;', [Info.Name]));
    Write('begin');
    if IsSimpleType(ElementType)
      then Write('  result := @fItems[Index];')
      else Write('  result := fItems[Index];');
    Write('end;');
    WriteEmptyLine;

    // SetItemRef
    Write(Format('procedure %s.SetItemRef(Index: integer; Ref : pointer);', [Info.Name]));
    Write('begin');
    if not IsSimpleType(ElementType) then
      Write('  fItems[Index] := ref;')
    else
      Write('  raise Exception.Create(''SetItemRef not implemented for simple types'');');
    Write('end;');
    WriteEmptyLine;

    // Clear
    Write(Format('procedure %s.Clear;', [Info.Name]));
    if not IsSimpleType(ElementType) then Write('var i : integer;');
    Write('begin');
    if not IsSimpleType(ElementType) then Write('  for i := 0 to (Count-1) do fItems[i].Free;');
    Write('  SetLength(fItems, 0);');
    Write('end;');
    WriteEmptyLine;


    // SetItems
    Write(Format('procedure %s.SetItems(Index : integer; const Value : %s);', [Info.Name, ElementType]));
    Write('begin');
    Write('  fItems[Index] := Value;');
    Write('end;');
    WriteEmptyLine;

    // Resize
    Write(Format('procedure %s.Resize(ElementCount : integer);', [Info.Name]));
    Write('begin');
    Write('  SetLength(fItems, ElementCount);');
    Write('end;');
    WriteEmptyLine;

    // GetCount
    Write(Format('function %s.GetCount: integer;', [info.Name]));
    Write('begin');
    Write('  result := Length(fItems);');
    Write('end;');
    WriteEmptyLine;

    // Delete
    Write(Format('procedure %s.Delete(Index : integer);', [info.Name]));
    Write('begin');
    Write('  raise Exception.Create(''Delete not implemented'');');
    Write('end;');
    WriteEmptyLine;
    
    // Add
    if IsSimpleType(ElementType)
      then Write(Format('procedure %s.Add(Value : %s);', [Info.Name, ElementType]))
      else Write(Format('function %s.Add : %s;', [Info.Name, ElementType]));
    Write('begin');
    if IsSimpleType(ElementType) then begin

      Write('  SetLength(fItems, Length(fItems)+1);');
      Write('  fItems[Length(fItems)-1] := Value;');
    end
    else begin
      Write(Format('  result := %s.Create;', [ElementType]));
      Write('  SetLength(fItems, Length(fItems)+1);');
      Write('  fItems[Length(fItems)-1] := result;');
    end;
    Write('end;');
    WriteEmptyLine;
  end;
end;

procedure TRODLToIntf.WriteStructPropMethods(aLibrary : TRODLLibrary; aStruct: TRODLStruct);
var i : integer;
    lWroteComment: boolean;
begin
  with aStruct do begin
{    Write(Format('constructor %s.Create;', [Info.Name]));
    Write('begin');
    Write('  inherited;');
    for i := 0 to (Count-1) do with Items[i] do begin
      if not (IsStruct(Info.DataType, aLibrary) or IsArray(Info.DataType, aLibrary)) then Continue;
      Write(Format('  fDestroy_%s := FALSE;', [Info.Name]));
    end;
    Write('end;');
    WriteEmptyLine;

    Write(Format('destructor %s.Destroy;', [Info.Name]));
    Write('begin');
    for i := 0 to (Count-1) do with Items[i] do begin
      if not (IsStruct(Info.DataType, aLibrary) or IsArray(Info.DataType, aLibrary)) then Continue;
      Write(Format('  if fDestroy_%s then FreeAndNIL(f%s);', [Info.Name, Info.Name]));
    end;
    Write('  inherited;');
    Write('end;');
    WriteEmptyLine;}

    lWroteComment := false;
    for i := 0 to (Count-1) do with Items[i] do begin
      if not (IsStruct(Info.DataType, aLibrary) or IsArray(Info.DataType, aLibrary)) then Continue;

      {Write(Format('procedure %s.Set%s(Value : %s);', [aStruct.Info.Name, Info.Name, Info.DataType]));
      Write('begin');
      Write(Format('  if (f%s<>NIL) and (fDestroy_%s) then FreeAndNIL(f%s);', [Info.Name, Info.Name, Info.Name]));
      Write(Format('  f%s := Value;', [Info.Name]));
      Write(Format('  fDestroy_%s := FALSE;', [Info.Name]));
      Write('end;');
      WriteEmptyLine;}

      if not lWroteComment then begin
        Write('{ '+Info.Name+' }');
        WriteEmptyLine;
        lWroteComment := true;
      end;

      Write(Format('function %s.Get%s : %s;', [aStruct.Info.Name, Info.Name, Info.DataType]));
      Write('begin');
      Write(Format('  if (f%s=NIL) then begin', [Info.Name]));
      Write(Format('    f%s := %s.Create;',
            [Info.Name, Info.DataType]));
      //Write(Format('    fDestroy_%s := TRUE;', [Info.Name]));
      Write('  end;');
      Write(Format('  result := f%s;', [Info.Name]));
      Write('end;');
      WriteEmptyLine;
    end;
  end;
end;

end.
