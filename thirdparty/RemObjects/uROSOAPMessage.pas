{ Summary: Contains the class TROSOAPMessage used to package RemObjects SOAP messages }
unit uROSOAPMessage;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uROClient, uROClientIntf, uROTypes, TypInfo, SysUtils, uROXMLIntf,
  uROSerializer, uROXMLSerializer;

type TAttribute = record
       Name, Value : string;
     end;

const
  EncodingStyle = 'http://schemas.xmlsoap.org/soap/encoding/';
  TempURI = 'http://tempuri.org/';

  id_FaultCode   = 'faultcode';
  id_FaultString = 'faultstring';
  id_FaultActor  = 'faultactor';
  id_FaultDetail = 'detail';

  EnvelopeAttributes : array[0..3] of TAttribute = (
    (Name: 'xmlns:SOAP-ENV'; Value: 'http://schemas.xmlsoap.org/soap/envelope/'),
    (Name: 'xmlns:xsd';      Value: 'http://www.w3.org/2001/XMLSchema'),
    (Name: 'xmlns:xsi';      Value: 'http://www.w3.org/2001/XMLSchema-instance'),
    (Name: 'xmlns:SOAP-ENC'; Value: EncodingStyle));

type {---------------------- Misc ----------------------}

     { Summary: Event type declaration
       Description: Event type declaration. Properties TROSOAPMessage.OnSOAPFault is of this type. }
     TSOAPFaultEvent = procedure(const aFaultNode : IXMLNode;
                                 const aFaultCode, aFaultString, aFaultActor, aFaultDetail : string) of object;

     {---------------------- TROSOAPMessage ----------------------}

     { Summary: RemObjects SOAP message encoding class
       Description:
         RemObjects SOAP message encoding class. It handles the encoding and decoding of SOAP messages
         according to the W3C specs. Use this type of message when you need to interoperate
         with external, non-RemObjects based clients or servers such as Java or .Net ones. }
     TROSOAPMessage = class(TROMessage, IROModuleInfo)
     private
       fXMLMessage : IXMLDocument;

       fEnvNode,
       fBodyNode,
       fHeaderNode,
       fMessageNode,
       fFaultNode : IXMLNode;
       fCustomLocation: string;

       fOnSOAPFault: TSOAPFaultEvent;

       function ParseEnvelope : boolean;
       function GetHeader: IXMLNode;
       function GetSerializationOptions: TXMLSerializationOptions;
       procedure SetSerializationOptions(const Value: TXMLSerializationOptions);

     protected
       {---------------------- Internals ----------------------}
       procedure InitObject; override;
       function CreateSerializer : TROSerializer; override;
       function ReadException : Exception; override;

       {---------------------- IROMessage ----------------------}
       procedure Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName : string); override;
       procedure Finalize; override;

       procedure WriteToStream(aStream : TStream); override;
       procedure ReadFromStream(aStream : TStream); override;
       procedure WriteException(aStream : TStream; anException : Exception); override;

       {---------------------- IROModuleInfo ----------------------}
       procedure GetModuleInfo(aStream : TStream; const aTransport : IROTransport; var aFormat : TDataFormat); override;

     public
       { Summary: Provides access to the SOAP Envelope node
         Description: Provides access to the SOAP Envelope node }
       property EnvNode : IXMLNode read fEnvNode;
       { Summary: Provides access to the SOAP Body node
         Description: Provides access to the SOAP Body node }
       property BodyNode: IXMLNode read fBodyNode;
       { Summary: Provides access to the SOAP message node
         Description:
           Provides access to the SOAP message node. You have to check to see if this property is NIL
           before trying to access it. SOAP envelopes with a Fault node have this property set to NIL. }
       property MessageNode: IXMLNode read fMessageNode;
       { Summary: Provides access to the SOAP Fault node
         Description:
           Provides access to the SOAP Fault node. You have to check to see if this property is NIL
           before trying to access it. Not every SOAP envelopes contain Fault nodes.  }
       property FaultNode : IXMLNode read fFaultNode;
       { Summary: Provides access to the SOAP headers
         Description: Provides access to the SOAP headers }
       property Header : IXMLNode read GetHeader;

       procedure Assign(iSource:TPersistent); override;

     published
       { Summary: Defines the URI clients need to post SOAP requests to
         Description:
           Defines the URI clients need to post SOAP requests to. It is mandatory you set this property
           correctly for the WSDL to be generated properly.
           If you set this property to "http://127.0.0.1:8099" like in the MegaDemo sample, your WSDL will contain
           the following service address:
           
           <service name="NewServiceService">
             <port name="NewServicePort" binding="tns:NewServiceBinding">
                <soap:address location="http://127.0.0.1:8099" />
             </port>
           </service>
          }
       property CustomLocation : string read fCustomLocation write fCustomLocation;
       { Summary: SOAP Serialization options accessor
         Description: SOAP Serialization options accessor }
       property SerializationOptions : TXMLSerializationOptions read GetSerializationOptions write SetSerializationOptions;
       { Summary: Fired whenever then incoming SOAP envelope contains a SOAP fault
         Description: Fired whenever then incoming SOAP envelope contains a SOAP fault. }
       property OnSOAPFault : TSOAPFaultEvent read fOnSOAPFault write fOnSOAPFault;
     end;


implementation

uses uRORes, uRODL, uRODLToXML, uRODLToWSDL, uROHTTPTools;

{ TROSOAPMessage }
procedure TROSOAPMessage.InitObject;
begin
  inherited;

  fXMLMessage := CreateXMLDoument;
  fXMLMessage.New(tag_Envelope);
end;

procedure TROSOAPMessage.Finalize;
begin
  inherited;
end;

procedure TROSOAPMessage.Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName: string);
var i : integer;
    //node : IXMLNode;
begin
  inherited;
  fXMLMessage.New(ns_Envelope+':'+tag_Envelope);

  SetHTTPInfo(aTransport, dfXML);

  fEnvNode := fXMLMessage.DocumentNode;

  for i := 0 to High(EnvelopeAttributes) do
    with EnvelopeAttributes[i] do
      fEnvNode.AddAttribute(Name, Value);

  { Sets variables for easier access in the other methods }
  fBodyNode := fEnvNode.Add(ns_Envelope+':'+tag_Body);
  fBodyNode.AddAttribute('SOAP-ENV:encodingStyle', EncodingStyle);
  fBodyNode.AddAttribute('xmlns:'+ns_Custom, 'urn:'+TempURI); // TODO: I am missing the library name here.. Might need to be fixed

  if not (xsoSendUntyped in SerializationOptions) then begin
    fMessageNode := fBodyNode.Add('NS1:'+aMessageName);
    fMessageNode.AddAttribute('xmlns:NS1', anInterfaceName);
  end
  else begin
    fMessageNode := fBodyNode.Add(aMessageName);

    // TODO: I am not totally sure this is the best way but seems to work for .Net webservices...
    fMessageNode.AddAttribute('xmlns', ExtractServerURL(anInterfaceName));
  end;

  Serializer.SetStorageRef(pointer(fMessageNode));
end;

function TROSOAPMessage.ReadException : Exception;
var faultcode, faultstring, faultactor, faultdetail : string;
    node : IXMLNode;
    s, exnme : string;
begin
  result := NIL;
  if Assigned(fFaultNode) then begin
    node := fFaultNode.GetNodeByName(id_FaultCode);
    if Assigned(node) then begin
      faultcode := node.Value;
      SplitNodeName(node, s, exnme);
    end
    else begin
      faultcode := '';
      exnme := '';
    end;
    
    node := fFaultNode.GetNodeByName(id_FaultString);
    if Assigned(node) then faultstring := node.Value else faultstring := '';

    node := fFaultNode.GetNodeByName(id_FaultActor);
    if Assigned(node) then faultactor := node.Value else faultactor := '';

    node := fFaultNode.GetNodeByName(id_FaultDetail);
    if Assigned(node) then faultdetail := node.Value else faultdetail := '';

    if Assigned(fOnSOAPFault)
      then fOnSOAPFault(fFaultNode, faultcode, faultstring, faultactor, faultdetail);

    result := GetExceptionClass(exnme).Create(faultstring); // TODO: Fix this. It should call RaiseException like TROBinMessage does.
  end;
end;

procedure TROSOAPMessage.ReadFromStream(aStream: TStream);
begin
  inherited;
  aStream.Position := 0;

  try
    fXMLMessage.New;

    try
      fXMLMessage.LoadFromStream(aStream);
    except
      RaiseError(err_InvalidEnvelope, [inf_InvalidEnvelopeNode]);
    end;

    if not ParseEnvelope
      then ProcessException; // In case there's one
  except
    // If something went wrong we recreate if because fXMLMessage is expected to be instantiated.
    if (fXMLMessage=NIL)
      then fXMLMessage.New;
    raise;
  end;
end;

function TROSOAPMessage.ParseEnvelope : boolean;
var localname,
    namespace  : string;
    i, k : integer;
    item, item2 : IXMLNode;
begin
  //result := FALSE;

  fBodyNode := NIL;
  fHeaderNode := NIL;
  fMessageNode := NIL;
  fFaultNode := NIL;
  fEnvNode := fXMLMessage.DocumentNode;

  { Checks the document is non empty and that the main tag is a tag_Envelope (required by SOAP spec) }
  SplitNodeName(fEnvNode, namespace, localname);
  if (localname<>tag_Envelope)
    then RaiseError(err_InvalidEnvelope, [inf_InvalidEnvelopeNode]);

  { Loads the nodes in the Body.
    I am intentionally not picky in the position of BODY and HEADER tags. Specs say HEADER should be first if present and
    BODY second. I just ignore this because requirement. It seems to be a totally useless detail. }

  for i := 0 to (fEnvNode.ChildrenCount-1) do begin
    item := fEnvNode.Children[i];

    SplitNodeName(item, namespace, localname);
    // Body
    if (localname=tag_Body) then begin
      fBodyNode := item;

      { Loads the FAULT or BODY.
        Althought the spec say you might have multiple BODY blocks I only consider one for now.
        I might change this in the future if an issue arises with this decision.
        If there ar multiple FAULTs or multiple BODYs the last one is the considered one }

      for k := 0 to (fBodyNode.ChildrenCount-1) do begin
        item2 := fBodyNode.Children[k];

        SplitNodeName(item2, namespace, localname);
        if (localname='#text') then Continue;

        if (localname=tag_Fault) then begin
          fFaultNode := item2;
          fMessageNode := fFaultNode;
          Break;
        end
        else begin
          fMessageNode := item2;
          Break;
        end;
      end;
    end

    // Header
    else if (localname=tag_Header)
      then fHeaderNode := item;
  end;

  if not Assigned(fBodyNode) then RaiseError(err_InvalidEnvelope, [inf_AbsentBody]);
  if not Assigned(fMessageNode) then RaiseError(err_InvalidEnvelope, [inf_AbsentMessage]);

  // Determines the interface and message name

  result := not Assigned(fFaultNode);
  if result then begin
    //xmlns:NS1="ITestService"
    if (fMessageNode.AttributeCount>0) then begin
      localname := fMessageNode.Attributes[0].Value;
      Delete(localname, 1, Pos('-', localname)); // urn:SOAPLibrary-SOAPService

      interfacename := localname;
    end;

    SplitNodeName(fMessageNode, namespace, localname);
    MessageName := localname;

    Serializer.SetStorageRef(pointer(fMessageNode));
  end;
end;

procedure TROSOAPMessage.WriteException(aStream : TStream; anException : Exception);
const CRLF = #13#10;
var faultenv : string;
begin
  faultenv := Format(
    '<?xml version="1.0" ?>'+CRLF+
    ' <env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">'+CRLF+
    '  <env:Body>'+CRLF+
    '   <env:Fault>'+CRLF+
    '    <'+id_FaultCode+'>SOAP:ENV:'+anException.ClassName+'</'+id_FaultCode+'>'+CRLF+
    '    <'+id_FaultString+'>%s</'+id_FaultString+'>'+CRLF+
    '   </env:Fault>'+CRLF+
    ' </env:Body>'+CRLF+
    '</env:Envelope>', [anException.Message]);

  aStream.Write(faultenv[1], Length(faultenv));
end;

procedure TROSOAPMessage.WriteToStream(aStream: TStream);
begin
  {s := '<?xml version="1.0"?>';
  aStream.Write(s[1], Length(s));}

{  parser := TDomToXmlParser.Create(NIL);
  parser.DOMImpl := fDOMImpl;
  try
    parser.writeToStream(fXMLMessage.documentElement, 'UTF-8', aStream);
  finally
    parser.Free;
  end;}
  fXMLMessage.SaveToStream(aStream);

  // TODO: Fix this. It shouln't be called like this
  inherited;
end;

procedure TROSOAPMessage.GetModuleInfo(aStream : TStream; const aTransport : IROTransport; var aFormat : TDataFormat);
var lib : TRODLLIbrary;
    wsdl : TRODLToWSDL;
    http : IROHTTPTransport;
begin
  inherited GetModuleInfo(aStream, aTransport, aFormat); // Sets ContentType for HTTP transports already

  aFormat := dfXML;

  with TXMLToRODL.Create do try
    lib := Read(aStream);
  finally
    Free;
  end;

  wsdl := TRODLToWSDL.Create(NIL);
  try
    if (Trim(CustomLocation)='') then begin
      Supports(aTransport, IROHTTPTransport, http);
      wsdl.Location := http.Location+http.PathInfo;
    end
    else wsdl.Location := CustomLocation;

    wsdl.Convert(lib);
    aStream.Position := 0;
    wsdl.Buffer.SaveToStream(aStream);
  finally
    wsdl.Free;
  end;
end;

function TROSOAPMessage.GetHeader: IXMLNode;
begin
  // Services like the Amazon one just don't accept empty headers (or don't know how to handle them).
  // Because of this I just create it on demand.

  if (fHeaderNode=NIL) then begin
    {fHeaderNode := fXMLMessage.createElement(tag_Header);
    fEnvNode.insertBefore(fHeaderNode, fBodyNode);}
    fHeaderNode := fEnvNode.Add(tag_Header);
  end;

  result := fEnvNode;
end;

function TROSOAPMessage.CreateSerializer : TROSerializer;
begin
  result := TROXMLSerializer.Create(pointer(fMessageNode));
end;

function TROSOAPMessage.GetSerializationOptions: TXMLSerializationOptions;
begin
  result := TROXMLSerializer(Serializer).SerializationOptions
end;

procedure TROSOAPMessage.SetSerializationOptions(
  const Value: TXMLSerializationOptions);
begin
  TROXMLSerializer(Serializer).SerializationOptions := Value
end;

procedure TROSOAPMessage.Assign(iSource: TPersistent);
var lSource:TROSOApMessage;
begin
  if Assigned(iSource) then begin

    if not (iSource is TROSOAPMessage) then RaiseError('Cannot Assign a %s t a %s',[ClassName,iSource.ClassName]);

    lSource := (iSource as TROSOAPMessage);
    self.CustomLocation := lSource.CustomLocation;
    self.SerializationOptions := lSource.SerializationOptions;
    self.OnSOAPFault := lSource.OnSOAPFault;
  end;
end;

initialization
  RegisterMessageClass(TROSOAPMessage);

end.
