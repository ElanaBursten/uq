unit fRoPleaseWaitForm;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TPleaseWaitForm = class(TForm)
    lbl_Caption: TLabel;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    fWindowList:pointer;
  public
    constructor Create(iOwner: TComponent; const iCaption:string=''; const iHeader:string=''); reintroduce;
    procedure Show(const iCaption:string=''); reintroduce;
    procedure Hide; reintroduce;
  end;

var
  PleaseWaitForm: TPleaseWaitForm;

implementation

{$R *.dfm}

{ TPleaseWaitForm }

constructor TPleaseWaitForm.Create(iOwner: TComponent; const iCaption:string=''; const iHeader:string='');
begin
  inherited Create(iOwner);
  if iHeader <> '' then Caption := iHeader;
  lbl_Caption.Caption := iCaption;
  ClientWidth := lbl_Caption.Left+lbl_Caption.Width+8;
end;

procedure TPleaseWaitForm.Show(const iCaption:string='');
begin
  if iCaption <> '' then begin
    lbl_Caption.Caption := iCaption;
    ClientWidth := lbl_Caption.Left+lbl_Caption.Width+8;
  end;
  inherited Show();
  Application.ProcessMessages();
end;

procedure TPleaseWaitForm.Hide;
begin
  inherited Hide();
  Application.ProcessMessages();
end;

procedure TPleaseWaitForm.FormShow(Sender: TObject);
begin
  SendMessage(Handle, CM_ACTIVATE, 0, 0);
  fWindowList := DisableTaskWindows(0);
end;

procedure TPleaseWaitForm.FormHide(Sender: TObject);
begin
  SendMessage(Handle, CM_DEACTIVATE, 0, 0);
  EnableTaskWindows(fWindowList);
end;

end.
