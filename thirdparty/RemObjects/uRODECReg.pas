{Copyright:      Hagen Reddmann  mailto:HaReddmann@AOL.COM
 Author:         Hagen Reddmann
 Remarks:        freeware, but this Copyright must be included
 known Problems: none
 Version:        3.0,  Delphi Encryption Compendium
                 Delphi 2-4, designed and testet under D3 & D4
 Remarks:        remove RO_Cipher1 from the Uses Clausel to
                 use only a Selection from Ciphers (RO_Cipher.pas)
}
unit uRODECReg;

{$INCLUDE RemObjects.inc}

{$DEFINE Part_II}
{$DEFINE Part_III}

interface

uses
  uRODECConst, uRODECUtil, uROHash, uROCipher, uROHCMngr, SysUtils, Classes,
  {$IFDEF DELPHI5} DsgnIntf, {$ELSE}  DesignIntf, DesignEditors, {$ENDIF}
  uROCipher1, uRORFC2289
{$IFDEF Part_II}
{$ENDIF}
{$IFDEF Part_III}
{$ENDIF};


type
// List all registered RO_Hash Names
  TROAlgorithmHashProperty = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
// List only Checksums
  TROAlgorithmChecksumProperty = class(TROAlgorithmHashProperty)
    procedure GetValues(Proc: TGetStrProc); override;
  end;
// List only Secure Hashs (excludes Checksum)
  TROAlgorithmSecureHashProperty = class(TROAlgorithmHashProperty)
    procedure GetValues(Proc: TGetStrProc); override;
  end;
// List all registered Ciphers
  TROAlgorithmCipherProperty = class(TROAlgorithmHashProperty)
    procedure GetValues(Proc: TGetStrProc); override;
  end;
// List all valid Idents ("otp-", "s/key")
  TROOTPIdentProperty = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
// List all registered Stringformats
  TROStringFormatProperty = class(TIntegerProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    function GetValue: String; override;
    procedure SetValue(const Value: String); override;
  end;

procedure Register;

implementation

{$R *.RES}

function TROAlgorithmHashProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paReadOnly, paValueList, paMultiSelect];
end;

procedure TROAlgorithmHashProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
begin
  with HashList do
    for I := 0 to Count-1 do Proc(Names[I]);
end;

procedure TROAlgorithmChecksumProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
begin
  with HashList do
    for I := 0 to Count-1 do
      if TROHashClass(Objects[I]).InheritsFrom(TROChecksum) then Proc(Names[I]);
end;

procedure TROAlgorithmSecureHashProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
begin
  with HashList do
    for I := 0 to Count-1 do
      if not TROHashClass(Objects[I]).InheritsFrom(TROChecksum) then Proc(Names[I]);
end;

procedure TROAlgorithmCipherProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
begin
  with CipherList do
    for I := 0 to Count-1 do Proc(Names[I]);
end;

function TROOTPIdentProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paMultiSelect];
end;

procedure TROOTPIdentProperty.GetValues(Proc: TGetStrProc);
begin
  Proc(sOTPIdent);
  Proc(sSKeyIdent);
end;

function TROStringFormatProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paMultiSelect];
end;

procedure TROStringFormatProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  S: TStringList;
begin
  S := TStringList.Create;
  try
    GetStringFormats(S);
    for I := 0 to S.Count-1 do Proc(S[I]);
  finally
    S.Free;
  end;
end;

function TROStringFormatProperty.GetValue: String;
var
  Fmt: TROStringFormatClass;
begin
  Fmt := StringFormat(GetOrdValue);   
  if Fmt <> nil then Result := Fmt.Name else Result := '(unknown)';
end;

procedure TROStringFormatProperty.SetValue(const Value: String);
var
  I: Integer;
  S: TStringList;
begin
  S := TStringList.Create;
  try
    GetStringFormats(S);
    I := S.IndexOf(Value);
    if I < 0 then SetOrdValue(StrToIntDef(Value, DefaultStringFormat))
      else SetOrdValue(TROStringFormatClass(S.Objects[I]).Format);
  finally
    S.Free;
  end;
end;

procedure Register;
begin
  {$IFDEF REGISTER_RODEC}
  RegisterComponents('RemObjects DEC', [TROHashManager, TROCipherManager, TROOneTimePassword]);
  RegisterPropertyEditor(TypeInfo(String), TROOneTimePassword, 'Algorithm', TROAlgorithmSecureHashProperty);
  RegisterPropertyEditor(TypeInfo(String), TROOneTimePassword, 'Ident', TROOTPIdentProperty);
  RegisterPropertyEditor(TypeInfo(Integer), TROOneTimePassword, 'Format', TROStringFormatProperty);
  RegisterPropertyEditor(TypeInfo(String), TROHashManager, 'Algorithm', TROAlgorithmHashProperty);
  RegisterPropertyEditor(TypeInfo(String), TROCipherManager, 'Algorithm', TROAlgorithmCipherProperty);
  {$ENDIF REGISTER_RODEC}
{$IFDEF Part_II}
{$ENDIF}
{$IFDEF Part_III}
{$ENDIF}
end;

end.
