{ Summary: }
unit uROWinMessageServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, Windows, Messages, uROClient, uROClientIntf, uROServer;

type { TROWinMessageServer }
     TROWinMessageServer = class(TROServer, IROTransport)
     private
       fWindowHandle : HWND;
       fServerID     : string;
       fTransport    : IROTransport;

       procedure WndProc(var Msg: TMessage);

     protected
       {-------- Internals --------}
       procedure IntSetActive(const Value: boolean); override;
       function IntGetActive : boolean; override;

       { IROTransport }
       function GetTransportObject : TObject;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

     published
       property ServerID : string read fServerID write fServerID;
     end;

implementation

uses Forms, Controls, SysUtils, uRORes;

var
  ROWindowClass: TWndClass = (
    style: 0;
    lpfnWndProc: @DefWindowProc;
    cbClsExtra: 0;
    cbWndExtra: 0;
    hInstance: 0;
    hIcon: 0;
    hCursor: 0;
    hbrBackground: 0;
    lpszMenuName: nil;
    lpszClassName: 'ROUtilWindow');

function AllocateHWnd(const aWindowName : string; Method: TWndMethod): HWND;
var tmpclass: TWndClass;
    isclsregistered: Boolean;
begin
  ROWindowClass.hInstance := HInstance;
  isclsregistered := GetClassInfo(HInstance, ROWindowClass.lpszClassName, tmpclass);

  if not isclsregistered or (tmpclass.lpfnWndProc <> @DefWindowProc) then begin
    if isclsregistered then Windows.UnregisterClass(ROWindowClass.lpszClassName, HInstance);
    Windows.RegisterClass(ROWindowClass);
  end;

  Result := CreateWindowEx(WS_EX_TOOLWINDOW, ROWindowClass.lpszClassName,
    PChar(aWindowName), WS_POPUP {!0}, 0, 0, 0, 0, 0, 0, HInstance, nil);

  if Assigned(Method)
    then SetWindowLong(Result, GWL_WNDPROC, Longint(MakeObjectInstance(Method)));
end;

{ TROWinMessageServer }

constructor TROWinMessageServer.Create(aOwner: TComponent);
begin
  inherited;
  Supports(Self, IROTransport, fTransport);
  fWindowHandle := 0;
end;

destructor TROWinMessageServer.Destroy;
begin
  if (fWindowHandle>1) then DeallocateHWnd(fWindowHandle);
  inherited;
end;

procedure TROWinMessageServer.WndProc(var Msg: TMessage);
var WMMsg : TWMCopyData absolute Msg;
    requeststream,
    responsestream : TMemoryStream;
    CDS: TCopyDataStruct;
begin
  requeststream := NIL;
  responsestream := NIL;

  with Msg do
    case Msg of
      WM_COPYDATA  : try
        requeststream := TMemoryStream.Create;
        responsestream := TMemoryStream.Create;

        requeststream.Write(WMMsg.CopyDataStruct.lpData^, WMMsg.CopyDataStruct.cbData);

        //ProcessMessage(MessageIntf, fTransport, requeststream, responsestream);
        DispatchMessage(fTransport, requeststream, responsestream);

        CDS.dwData := 0;
        CDS.cbData := responsestream.Size;
        CDS.lpData := responsestream.Memory;

        SendMessage(WMMsg.From, WM_COPYDATA, 0, Integer(@CDS));
      finally
        responsestream.Free;
        requeststream.Free;
      end;
      else result := DefWindowProc(FWindowHandle, Msg, wParam, lParam);
    end;
end;

function TROWinMessageServer.IntGetActive: boolean;
begin
  result := (fWindowHandle>0);
end;

procedure TROWinMessageServer.IntSetActive(const Value: boolean);
begin
  if Value then begin
    if not (csDesigning in ComponentState)
      then fWindowHandle := AllocateHWnd(ServerID, WndProc)
      else fWindowHandle := 1; // To save the property value
  end
  else begin
    if (fWindowHandle>0) then begin
      if (fWindowHandle>1) then DeallocateHWnd(fWindowHandle);
      fWindowHandle := 0;
    end;
  end;
end;

function TROWinMessageServer.GetTransportObject: TObject;
begin
  result := Self;
end;

initialization
  RegisterServerClass(TROWinMessageServer);

end.
