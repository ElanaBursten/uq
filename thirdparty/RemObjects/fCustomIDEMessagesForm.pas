unit fCustomIDEMessagesForm;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF KYLIX}
  SysUtils, Classes, QGraphics, QMenus, QTypes, QImgList,
  QControls, QComCtrls, QForms, QDialogs, uROIDETools, Menus, ImgList,
  Controls, ComCtrls
  {$ELSE}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ImgList, uROIDETools, Menus
  {$ENDIF};

type
  TCustomIDEMessagesForm = class(TForm)
    ListView: TListView;
    ImageList: TImageList;
    PopupMenu1: TPopupMenu;
    CopytoClipboard1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CopytoClipboard1Click(Sender: TObject);
  private

  public
    destructor Destroy; override;

    procedure FillList(aMessageList : TIDEMessageList);
  end;

function IDEMessageForm : TCustomIDEMessagesForm;

implementation

uses ClipBrd;

{$R *.DFM}

var
  _CustomIDEMessagesForm: TCustomIDEMessagesForm;

function IDEMessageForm : TCustomIDEMessagesForm;
begin
  if (_CustomIDEMessagesForm=NIL)
    then _CustomIDEMessagesForm := TCustomIDEMessagesForm.Create(Application);
  _CustomIDEMessagesForm.Show;
  
  result := _CustomIDEMessagesForm;
end;

destructor TCustomIDEMessagesForm.Destroy;
begin
  _CustomIDEMessagesForm := NIL;
  inherited;
end;

procedure TCustomIDEMessagesForm.FillList(aMessageList: TIDEMessageList);
var i    : integer;
    item : TListItem;
    s : string;
begin
  Caption := 'RemObjects Preprocessor';
  ListView.Items.Clear;
  if (aMessageLIst=NIL) then Exit;

  with aMessageList do begin
    for i := 0 to (Count-1) do begin
      item := ListView.Items.Add;
      with Items[i] do begin
        if (PrefixStr<>'') then s := PrefixStr+': ' else s := '';
        if (FileName<>'') then s := s+'('+Filename+')';
        s := s+MessageStr;

        item.Caption := s;
        item.ImageIndex := Ord(MessageType);
        {$IFNDEF KYLIX}
        item.StateIndex := Ord(MessageType);
        {$ENDIF}
      end;
    end;

    if (Count>0) then ListView.Items[Count-1].Selected := TRUE;
    Caption := Caption+' ('+ IntToStr(Count)+' messages)';
  end;
end;

procedure TCustomIDEMessagesForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TCustomIDEMessagesForm.FormShow(Sender: TObject);
begin
  Height := 150;
  Width := 400;
  Left := Screen.Width-Width;
  Top := Screen.Height-Height;
end;

procedure TCustomIDEMessagesForm.CopytoClipboard1Click(Sender: TObject);
var i : integer;
    s : string;
begin
  s := '';
  for i := 0 to (ListView.Items.Count-1) do
    s := s+ListView.Items[i].Caption+#13#10;

  Clipboard.AsText := s;
end;

initialization
  _CustomIDEMessagesForm := NIL;

finalization
  _CustomIDEMessagesForm.Free;

end.
