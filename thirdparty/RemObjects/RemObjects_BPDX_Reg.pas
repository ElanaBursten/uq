unit RemObjects_BPDX_Reg;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

procedure Register;

implementation

uses uRORes, Classes, uROBPDXTCPChannel, uROBPDXHTTPChannel, uROBPDXHTTPServer, uROBPDXTCPServer;

procedure Register;
begin
  RegisterComponents(str_ProductName,
    [TROBPDXTCPChannel, TROBPDXHTTPChannel, TROBPDXTCPServer, TROBPDXHTTPServer]);
end;

end.
