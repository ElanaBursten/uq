unit uROClassFactories;

interface

uses {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
     uROServer, uROServerIntf,
     {$IFDEF DOTNET}
     Borland.Delphi.Classes,
     Borland.Delphi.SysUtils,
     RemObjects.SDK.VclReplacements;
     {$ELSE}
     Classes, SysUtils, SyncObjs;
     {$ENDIF}

type TROSingletonClassFactory = class(TROClassFactory)
     private
       fInstance : IInterface;

     protected
       procedure CreateInstance(out anInstance : IInterface); override;
       procedure ReleaseInstance(var anInstance : IInterface); override;
     public
     end;


     TROSynchronizedSingletonClassFactory = class(TROSingletonClassFactory)
     private
       fCriticalSection:TCriticalSection;
     protected
       procedure CreateInstance(out anInstance : IInterface); override;
       procedure ReleaseInstance(var anInstance : IInterface); override;
     public
       constructor Create(const anInterfaceName : string;
                          aCreatorFunc : TRORemotableCreatorFunc;
                          anInvokerClass : TROInvokerClass);
       destructor Destroy; override;
     end;

     { Description
       TROPooledClassFactory is a class factory that maintains a
       list of instances of server objects. Rather than creating a
       separate instance for each call (see <LINK TROClassFactory, TROClassFactory>)
       \or always using the same instance (see <LINK TROSingletonClassFactory, TROSingletonClassFactory>),
       TROPooledClassFactory creates a predefined number of
       instances and returns the first available (not used in that
       moment) to the caller.

       Summary
       Class factory that maintains a pooled list of objects.                                              }
     TROPooledClassFactory = class(TROClassFactory)
     private
       fInstances : array of IInterface;
       fPoolSize,
       fLastInc : integer;

     protected
       procedure CreateInstance(out anInstance : IInterface); override;
       procedure ReleaseInstance(var anInstance : IInterface); override;

     public
       constructor Create(const anInterfaceName : string;
                          aCreatorFunc : TRORemotableCreatorFunc;
                          anInvokerClass : TROInvokerClass;
                          aPoolSize : integer);
       destructor Destroy; override;
     end;

implementation

{ TROSingletonClassFactory }

procedure TROSingletonClassFactory.CreateInstance(
  out anInstance: IInterface);
begin
  if (fInstance=NIL)
    then inherited CreateInstance(fInstance);

  anInstance := fInstance;
end;

procedure TROSingletonClassFactory.ReleaseInstance(
  var anInstance: IInterface);
begin
  // Does nothing. We want to keep instance alive
end;

{ TROPooledClassFactory }

constructor TROPooledClassFactory.Create(const anInterfaceName: string;
  aCreatorFunc: TRORemotableCreatorFunc; anInvokerClass: TROInvokerClass;
  aPoolSize : integer);
var i : integer;
begin
  inherited Create(anInterfaceName, aCreatorFunc, anInvokerClass);

  SetLength(fInstances, aPoolSize);
  fPoolSize := aPoolSize;
  fLastInc := -1;

  for i := 0 to (aPoolSize-1) do
    inherited CreateInstance(fInstances[i]);
end;

destructor TROPooledClassFactory.Destroy;
var i : integer;
begin
  for i := 0 to High(fInstances) do
    fInstances[i] := NIL;

  inherited;
end;

procedure TROPooledClassFactory.CreateInstance(out anInstance: IInterface);
var i, refcnt : integer;
begin
  anInstance := NIL;
  i := fLastInc;

  while (anInstance=NIL) do try
    if (i>=fPoolSize-1) then begin
      i := 0;
      Sleep(100);
    end else Inc(i);

    {$IFDEF DOTNET}
    raise Exception.Create('TROPooledClassFactory is not properly implemented for .NET, yet.');
    {$ELSE}
    refcnt := fInstances[i]._AddRef;
    if (refcnt=2) then begin
      anInstance := fInstances[i];
      fLastInc := i;
    end;

    {$ENDIF}
  finally
    {$IFDEF DOTNET}
    {$ELSE}
    fInstances[i]._Release;
    {$ENDIF}
  end;
end;

procedure TROPooledClassFactory.ReleaseInstance(var anInstance: IInterface);
begin
  // Does nothing. We want to keep instances alive
end;


{ TROSynchronizedSingletonClassFactory }

constructor TROSynchronizedSingletonClassFactory.Create(const anInterfaceName: string; aCreatorFunc: TRORemotableCreatorFunc; anInvokerClass: TROInvokerClass);
begin
  inherited;
  fCriticalSection := TCriticalSection.Create();
end;

destructor TROSynchronizedSingletonClassFactory.Destroy;
begin
  FreeAndNil(fCriticalSection);
  inherited;
end;

procedure TROSynchronizedSingletonClassFactory.CreateInstance(out anInstance: IInterface);
begin
  fCriticalSection.Enter();
  inherited;
end;

procedure TROSynchronizedSingletonClassFactory.ReleaseInstance(var anInstance: IInterface);
begin
  inherited;
  fCriticalSection.Leave();
end;

end.
