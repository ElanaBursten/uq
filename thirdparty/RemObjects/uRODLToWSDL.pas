{ Summary: Contains the class TRODLToWSDL converter which transforms a RODL library into a WSDL document }
unit uRODLToWSDL;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - CodeGen
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, uRODL, uROTypes, uROXMLSerializer;

const

  SOAPDataTypes : array[TRODataType] of string = (
        'int',
        'dateTime',
        'double',
        'double',
        'string',
        'string',
        'int',
        'boolean',
        'anyType',
        dts_base64Binary,
        '???');


type {------------------- TRODLToWSDL -------------------}

     { Summary: RODL converter which transforms a RODL library into a WSDL document
       Description:
         RODL converter which transforms a RODL library into a WSDL document. This class
         is used internally by the TROSOAPMessage class to produce WSDLs.
         See how it is used in the implementation of the method TROSOAPMessage.GetModuleInfo.
       }

     TRODLToWSDL = class(TRODLConverter)
     private
       fLocation: string;
       procedure SetLocation(const Value: string);

       procedure WriteEnum(anEnum : TRODLEnum);
       procedure WriteStruct(aStruct : TRODLStruct);
       procedure WriteArray(anArray : TRODLArray);

     protected
       procedure IntConvert(const aLibrary : TRODLLibrary; const aTargetEntity : string = ''); override;

     public
      { Summary: Identifies the URI where SOAP envelopes have to be posted to
        Description:
          Identifies the URI where SOAP envelopes have to be posted to.
          In the WSDL this URI is located in the soap:address node under the service nodes.

          For instance, in the WSDL at http://www.xmethods.net/sd/2001/FedExTrackerService.wsdl
          the location is "http://services.xmethods.net:80/perl/soaplite.cgi"

          <service name="FedExTrackerService">
              <port name="FedExTrackerPort" binding="tns:FedExTrackerBinding">
                <soap:address location="http://services.xmethods.net:80/perl/soaplite.cgi" />
              </port>
          </service>
        }
       property Location : string read fLocation write SetLocation;
     end;

function SOAPDataType(const aDataTypeName : string; IncludeNamespace : boolean = TRUE) : string;

implementation

uses SysUtils;

function SOAPDataType(const aDataTypeName : string; IncludeNamespace : boolean = TRUE) : string;
var dt : TRODataType;
begin
  dt := StrToDataType(aDataTypeName);
  if (dt=rtUserDefined)
    then result := aDataTypeName
    else result := SOAPDataTypes[dt];

  if IncludeNamespace then begin
    if (dt=rtUserDefined) then result := ns_Custom+':'+result
                          else result := ns_Standard+':'+result;
  end;
end;

{ TRODLToWSDL }

procedure TRODLToWSDL.IntConvert(const aLibrary: TRODLLibrary;
  const aTargetEntity: string);
const definitions_ns : array[0..8] of string = (
           'xmlns="http://schemas.xmlsoap.org/wsdl/" ',
           'xmlns:xs="http://www.w3.org/2001/XMLSchema" ',
           'name="%s" ',
           'targetNamespace="http://tempuri.org/" ',
           'xmlns:tns="http://tempuri.org/" ',
           'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" ',
           'xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" ',
           'xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" ',
           'xmlns:'+ns_Custom+'="urn:%s"');


var s, i, o, p : integer;
begin
  with aLibrary do begin
    Write('<definitions ');

    for i := 0 to High(definitions_ns) do
      Write(Format(definitions_ns[i], [Info.Name]), 3);
    Write('>');


    // Write types

    Write('<types>', 3);
    Write(Format('<xs:schema targetNamespace="urn:%s" xmlns="urn:%s">', [Info.Name, Info.Name]), 6);

    for i := 0 to (EnumCount-1) do WriteEnum(Enums[i]);
    for i := 0 to (StructCount-1) do WriteStruct(Structs[i]);
    for i := 0 to (ArrayCount-1) do WriteArray(Arrays[i]);

    Write('</xs:schema>', 6);
    Write('</types>', 3);

    // Writes messages
    for s := 0 to (ServiceCount-1) do begin
      with Services[s].Default do begin
        for o := 0 to (Count-1) do begin
          with Items[o] do begin
            // Request
            Write(Format('<message name="%sRequest">', [Info.Name]), 3);
            for p := 0 to (Count-1) do
              if IsInputFlag(Items[p].Info.Flag) then
                Write(Format('<part name="%s" type="%s" />', [Items[p].Info.Name, SOAPDataType(Items[p].Info.DataType)]), 6);
            Write('</message>', 3);

            // Response
            Write(Format('<message name="%sResponse">', [Info.Name]), 3);
            for p := 0 to (Count-1) do
              if IsOutputFlag(Items[p].Info.Flag) then
                Write(Format('<part name="%s" type="%s" />', [Items[p].Info.Name, SOAPDataType(Items[p].Info.DataType)]), 6);
            Write('</message>', 3);
          end;
        end;
      end;
    end;

    // Writes port and operations
    for s := 0 to (ServiceCount-1) do begin
      with Services[s].Default do begin
        Write(Format('<portType name="%s">', [Services[s].Info.Name]), 3);
        for o := 0 to (Count-1) do begin
          with Items[o] do begin
            Write(Format('<operation name="%s">', [Info.Name]), 6);
            Write(Format('<input message="tns:%sRequest" />', [Info.Name]), 9);
            Write(Format('<output message="tns:%sResponse" />', [Info.Name]), 9);
            Write(Format('</operation>', [Info.Name]), 6)
          end;
        end;
        Write('</portType>', 3);
      end;
    end;

    // Write bindings
    for s := 0 to (ServiceCount-1) do begin
      with Services[s].Default do begin
        Write(Format('<binding name="%sBinding" type="tns:%s">',
          [Services[s].Info.Name, Services[s].Info.Name]), 3);
        Write('<soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http" />', 6);

        for o := 0 to (Count-1) do begin
          with Items[o] do begin
            Write(Format('<operation name="%s">', [Info.Name]), 9);
            Write(Format('<soap:operation soapAction="urn:%s-%s#%s" style="rpc" />', [aLibrary.Info.Name, Services[s].info.Name, Info.Name]),12);

            Write(Format('<input message="%sRequest">', [Info.Name]), 12);
            Write(Format('<soap:body use="encoded" '+
                         'encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" '+
                         'namespace="urn:%s-%s" />',
                  [aLibrary.Info.Name, Services[s].Info.Name]), 15);
            Write('</input>', 12);

            Write(Format('<output message="%sResponse">', [Info.Name]), 12);
            Write(Format('<soap:body use="encoded" '+
                         'encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" '+
                         'namespace="urn:%s-%s" />',
                  [aLibrary.Info.Name, Services[s].Info.Name]), 15);
            Write('</output>', 12);

            Write('</operation>', 9);
          end;
        end;

        Write('</binding>', 3);
      end;
    end;

    // Writes services
    for s := 0 to (ServiceCount-1) do begin
      with Services[s].Default do begin
        Write(Format('<service name="%sService">', [Services[s].Info.Name]), 3);
        Write(Format('<documentation>%s</documentation>', [Info.Documentation]), 6);
        Write(Format('<port name="%sPort" binding="tns:%sBinding">',
                     [Services[s].Info.Name, Services[s].Info.Name]), 6);
        Write(Format('<soap:address location="%s" />', [fLocation]), 9);
        Write('</port>', 6);

        Write('</service>',3);
      end;
    end;

    Write('</definitions>');
  end;
end;

procedure TRODLToWSDL.SetLocation(const Value: string);
begin
  fLocation := Value;
end;

procedure TRODLToWSDL.WriteArray(anArray: TRODLArray);
begin
  Write(Format('<xs:complexType name="%s">', [anArray.Info.Name]), 9);
  Write('<xs:complexContent>', 12);

  Write('<xs:restriction base="soapenc:Array">', 15);
  Write('<xs:sequence />', 18);
  Write(Format('<xs:attribute ref="soapenc:arrayType" n1:arrayType="%s[]" xmlns:n1="http://schemas.xmlsoap.org/wsdl/" />',
          [SOAPDataType(anArray.ElementType)]), 18);
  Write('</xs:restriction>', 15);
  
  Write('</xs:complexContent>', 12);
  Write('</xs:complexType>', 9);
end;

procedure TRODLToWSDL.WriteEnum(anEnum: TRODLEnum);
var i : integer;
begin
  Write(Format('<xs:simpleType name="%s">', [anEnum.Info.Name]), 9);
  Write('<xs:restriction base="xs:string">', 12);
  for i := 0 to anEnum.Count-1 do
    Write(Format('<xs:enumeration value="%s" />', [anEnum.Items[i].Info.Name]), 15);
  Write('</xs:restriction>', 12);
  Write('</xs:simpleType>', 9);
end;

procedure TRODLToWSDL.WriteStruct(aStruct: TRODLStruct);
var i : integer;
begin
  Write(Format('<xs:complexType name="%s">', [aStruct.Info.Name]), 9);
  Write('<xs:sequence>', 12);
  for i := 0 to aStruct.Count-1 do
    with aStruct.Items[i] do
      Write(Format('<xs:element name="%s" type="%s"/>', [Info.Name, SOAPDataType(Info.DataType)]), 15);
  Write('</xs:sequence>', 12);
  Write('</xs:complexType>', 9);
end;

end.
