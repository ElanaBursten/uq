{ Summary: Contains the TROWebBrokerServer class}
unit uROWebBrokerServer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, HTTPApp, uROClientIntf, uROClient, uROServer, uROServerIntf;

type
  {----------- TROWebBrokerServer -----------}

  { Summary: RemObjects server for Borland's WebBroker and Developer Express' ExpressWeb Framework applications.
    Description:
      RemObjects server for Borland's WebBroker and Developer Express' ExpressWeb Framework applications.
      By putting this server in an existing WebBroker or EWF module (ISAPI, Apache, CGI, etc) you can turn it
      into a full fledged RemObjects server while maintaining the web client functionality intact.

      This server implements IROTransport, IROTCPTransport, IROHTTPTransport.
      See TROServer.DispatchMessage for more information about these interfaces. 
  }

  TROWebBrokerServer = class(TROServer, IROTransport, IROTCPTransport, IROHTTPTransport)
  private
    fUserBeforeDispatch : THTTPMethodEvent;
    fActions : TWebActionItems;

    fActive : boolean;
    fTransportIntf : IROHTTPTransport;

    fRequest : TWebRequest;
    fResponse : TWebResponse;

    function ReadRequestStream(aRequest : TWebRequest): TStream;
    function GetRequest: TWebRequest;
    function GetResponse: TWebResponse;
    function IsWebModuleAction(const aPathInfo: string) : boolean;

  protected
    procedure ReplaceBeforeDispatch(aOwner : TComponent; aNewHandler : THTTPMethodEvent); virtual;

    function GetDispatchersClass : TROMessageDispatchersClass; override;

    procedure Loaded; override;
    procedure IntSetActive(const Value: boolean); override;
    function IntGetActive : boolean; override;

    {----------- IROTransport -----------}
    function GetTransportObject : TObject;

    {----------- IROTCPTransport -----------}
    function GetClientAddress : string;

    {----------- IROHTTPTransport -----------}
    procedure SetHeaders(const aName, aValue : string);
    function GetHeaders(const aName : string) : string;
    function GetContentType : string;
    procedure SetContentType(const aValue : string);
    function GetUserAgent : string;
    procedure SetUserAgent(const aValue : string);
    function GetTargetURL : string;
    procedure SetTargetURL(const aValue : string);
    function GetPathInfo : string;
    function GetLocation : string;

  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;

    { Summary: Provides access to the HTTP request object
      Description: Provides access to the HTTP request object. }
    property Request : TWebRequest read GetRequest;
    { Summary: Provides access to the HTTP response object.
      Description: Provides access to the HTTP response object. }
    property Response : TWebResponse read GetResponse;

  published
    { Summary: Internal. Manages the processing of HTTP requests.
      Description:
        Internal. Manages the processing of HTTP requests. This method is hooked to your WebBroker or EWF
        module at runtime and pre-processes HTTP requests. By checking the PathInfo of the incoming request
        it understands if it is meant to be handled by the owner or it is (for instance) as a SOAP or Binary request. }

    procedure IntOnBeforeDispatch(Sender: TObject;
                                  Request: TWebRequest;
                                  Response: TWebResponse; var Handled: Boolean); virtual;

  end;

implementation

uses TypInfo, uRORes, uROHTTPTools, SysUtils, uROHTTPDispatch;

type
  // See TROWebBrokerServer.Loaded This is for EWF support.
  IInterfaceComponentReference = interface
    ['{E28B1858-EC86-4559-8FCD-6B4F824151ED}']
    function GetComponent: TComponent;
  end;

{ TROWebBrokerServer }

constructor TROWebBrokerServer.Create(aOwner: TComponent);
begin
  inherited;

  fActive := TRUE;
  fActions := NIL;
  Supports(Self, IROTransport, fTransportIntf);
end;

destructor TROWebBrokerServer.Destroy;
begin
  fTransportIntf := NIL;

  inherited;
end;

procedure TROWebBrokerServer.Loaded;
var dispatchactions : IUnknown;
    icref : IInterfaceComponentReference;
begin
  inherited;

  if not (csDesigning in ComponentState) then begin
    ReplaceBeforeDispatch(Owner, IntOnBeforeDispatch);

    if Owner is TWebModule
      then fActions := TWebActionItems(GetObjectProp(Owner, 'Actions'))
      else try
        { This is for EWF. I don't want to set a dependency to the EWF units.
          This component has to stay WebBroker and EWF neutral since, in the end, we are
          dealing with web broker actions. This solves the problem. }

        {$IFDEF DELPHI6UP}
        if IsPublishedProp(Owner, 'DispatchActions')
          then dispatchactions := GetInterfaceProp(Owner, 'DispatchActions')
          else Exit;
        {$ENDIF DELPHI6UP}

        if (dispatchactions<>NIL) then begin
          if Supports(dispatchactions, IInterfaceComponentReference, icref) then begin
            fActions := TWebActionItems(GetObjectProp(icref.GetComponent, 'Actions'));
          end;
        end;
      except
        //ToDo: do we really want to catch this exception and display a DIAOG??
        on E:Exception do begin
          //showmessage(e.message);
          fActions := NIL;
        end;
      end;
  end;
end;

function TROWebBrokerServer.IntGetActive: boolean;
begin
  result := fActive;
end;

procedure TROWebBrokerServer.IntSetActive(const Value: boolean);
begin
  fActive := Value;
end;

function TROWebBrokerServer.ReadRequestStream(aRequest : TWebRequest): TStream;
var s, t : string;
    i : integer;
begin
  s := aRequest.Content;
  i := aRequest.ContentLength-Length(Request.Content);

  while (i>0) do begin
    t := aRequest.ReadString(I);
    s := S+T;
    i := i-Length(t);
  end;

  result := TStringStream.Create(s);
end;

function TROWebBrokerServer.IsWebModuleAction(const aPathInfo: string) : boolean;
var i : integer;
begin
  result := FALSE;
  if (fActions=NIL) then Exit;

  for i := 0 to (fActions.Count-1) do
    if (CompareText(fActions[i].PathInfo, aPathInfo)=0) then begin
      result := TRUE;
      Exit;
    end;
end;

procedure TROWebBrokerServer.IntOnBeforeDispatch(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var requeststream : TStream;
    responsestream : TStringStream;
    ok : boolean;
begin
  requeststream := NIL;
  responsestream := NIL;

  fRequest := Request;
  fResponse := Response;

  // Executes the user's event handler
  if Assigned(fUserBeforeDispatch)
    then fUserBeforeDispatch(Sender, Request, Response, Handled);

  // Processes the message if not handled already
  if not Handled and not IsWebModuleAction(Request.PathInfo) then

    if (Request.PathInfo<>'') then try
      responsestream := TStringStream.Create('');
      requeststream := ReadRequestStream(Request);

      ok := DispatchMessage(fTransportIntf, requeststream, responsestream);

      if not ok then Response.StatusCode := HTTP_FAILED
                else Response.StatusCode := HTTP_OK;

    finally
      requeststream.Free;
      Handled := TRUE;

      responsestream.Position := 0;
      Response.ContentStream := responsestream;
    end;
end;

function TROWebBrokerServer.GetRequest: TWebRequest;
begin
  result := fRequest;
end;

function TROWebBrokerServer.GetResponse: TWebResponse;
begin
  result := fResponse;
end;

function TROWebBrokerServer.GetClientAddress: string;
begin
  result := fRequest.RemoteAddr;
end;

function TROWebBrokerServer.GetContentType: string;
begin
  result := Response.ContentType
end;

function TROWebBrokerServer.GetHeaders(const aName: string): string;
begin
  result := GetHeaderValue(Response.CustomHeaders, aName);
end;

function TROWebBrokerServer.GetTargetURL: string;
begin
    result := ''
end;

function TROWebBrokerServer.GetTransportObject: TObject;
begin
  result := Self;
end;

function TROWebBrokerServer.GetUserAgent: string;
begin
  result := str_ProductName
end;

procedure TROWebBrokerServer.SetContentType(const aValue: string);
begin
  Response.ContentType := aValue
end;

procedure TROWebBrokerServer.SetHeaders(const aName, aValue: string);
begin
  SetHeaderValue(Response.CustomHeaders, aName, aValue);
end;

procedure TROWebBrokerServer.SetTargetURL(const aValue: string);
begin

end;

procedure TROWebBrokerServer.SetUserAgent(const aValue: string);
begin
  
end;

procedure TROWebBrokerServer.ReplaceBeforeDispatch(aOwner: TComponent;
  aNewHandler: THTTPMethodEvent);
// Do not change the sequance of these names. It's important for EWF.
const EventHandlerNames : array[0..1] of string = ('OnBeforeDispatch', 'BeforeDispatch');
var pinfo : PPropInfo;
    i     : integer;
    myevent : THTTPMethodEvent;
    mtd,
    mtd2  : TMethod;
    s     : string;
begin
  for i := 0 to High(EventHandlerNames) do begin
    s := EventHandlerNames[i];
    pinfo := GetPropInfo(Owner, s);

    if (pinfo<>NIL) then begin
      myevent := IntOnBeforeDispatch;
      mtd := TMethod(myevent);  // <--- I have to pass through a local variable. Cannot type caet directly... Go figure!

      mtd2 := GetMethodProp(Owner, pinfo);
      SetMethodProp(Owner, pinfo, mtd);

      TMethod(fUserBeforeDispatch) := mtd2;
      Exit;
    end;
  end;
  //TWebModule(aOwner).BeforeDispatch := aNewHandler;
end;

function TROWebBrokerServer.GetPathInfo: string;
begin
  result := Request.PathInfo
end;

function TROWebBrokerServer.GetDispatchersClass: TROMessageDispatchersClass;
begin
  result := TROHTTPMessageDispatchers;
end;

function TROWebBrokerServer.GetLocation: string;
begin
  result := 'http://'+fRequest.Host+fRequest.URL
end;

initialization
  RegisterServerClass(TROWebBrokerServer);
  
end.
