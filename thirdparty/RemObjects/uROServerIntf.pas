{ Summary: }
unit uROServerIntf;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  uRORes, uROClientIntf;

type {$IFDEF DELPHI5}
     { IInterface }
     IInterface = IUnknown;
     {$ENDIF}

     { IROInvoker }
     IROClassFactory = interface;
     IROInvoker = interface
     ['{6BF28A4D-1511-4FD0-83C0-0F0A23322ACC}']
       function HandleMessage(const aFactory : IROClassFactory;
                               const aMessage : IROMessage;
                               const aTransport : IROTransport) : boolean;
     end;

     { IROClassFactory }
     IROClassFactory = interface
     ['{87580688-9994-4EB3-BDB9-E621ED52C3E9}']
       procedure CreateInstance(out anInstance : IInterface);
       procedure ReleaseInstance(var anInstance : IInterface);

       function GetInterfaceName : string;
       function GetInvoker : IROInvoker;

       property InterfaceName : string read GetInterfaceName;
       property Invoker : IROInvoker read GetInvoker;
     end;

     { IROSessionClassFactory }
     IROSessionClassFactory = interface(IROClassFactory)
     ['{AF6FAB28-5647-441E-B8BF-346C7E80B1C7}']
       procedure CreateSessionInstance(out anInstance : IInterface; iSessionID:TSessionID);
       procedure ExpireSessions;
     end;

     {---------------------- IRODispatchNotifier ----------------------}

     { Summary: Interface implemented by server side classes for interception purposes
       Description:
         Interface implemented by server side classes for interception purposes. }

     IRODispatchNotifier = interface
     ['{C3359706-83B8-4285-866C-D4DD5960B183}']
      { Summary:
        Description: }
       procedure GetDispatchInfo(const aTransport : IROTransport; const aMessage : IROMessage);
     end;

implementation

end.
