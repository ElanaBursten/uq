{ Summary: Contains the class TROBinMessage used to package RemObjects binary messages }
unit uROBINMessage;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  {$IFDEF DOTNET}
  Borland.Delphi.Classes,
  Borland.Delphi.SysUtils,
  RemObjects.SDK.Serializer,
  RemObjects.SDK.VclReplacements,
  {$ELSE}
  Classes, SysUtils, TypInfo,
  uROSerializer,
  {$ENDIF}
  uROClient, uROClientIntf;

type TBINMessageSignature = array[0..4] of char;
const BINSignature : TBINMessageSignature = 'RO103';

     {---------------------- Misc ----------------------}

     { Summary: Binary message header
       Description:
         Binary message header. Contains important information required by the receiver to understand
         how to decode the binary stream. }

     { Header BINAYR LAYOUT: 12 bytes

         52 4f 31 30  = "RO10" basic RO signature for RO 1.0
         XX YY ZZ --  = XX: subversion (currenly 03)
                        YY: option flags: 01 = compressed
                        ZZ: message type as defined in uROClientIntf
         -- -- -- --  = --: reserved for future use }

     {.$IFDEF DOTNET}
type TBINHeaderArray = array[0..11] of byte;

type TBINHeader = class
     private
       fHeader:TBINHeaderArray;
       function GetCompressed: boolean;
       procedure SetCompressed(const Value: boolean);
       function GetSignatureValid: boolean;
       function GetMessageType: TMessageType;
       procedure SetMessageType(const Value: TMessageType);
     public
       constructor CreateFromStream(iStream:TStream);
       constructor Create;
       procedure WriteToStream(iStream:TStream);
       property Header:TBINHeaderArray read fHeader;
       property Compressed:boolean read GetCompressed write SetCompressed;
       property SignatureValid:boolean read GetSignatureValid;
       property MessageType:TMessageType read GetMessageType write SetMessageType;
     end;

     {.$ELSE}
(*type TBINHeader = record
       Signature   : TBINMessageSignature; { 5 bytes long string containing version information such as 'RO103' }
       Compressed  : boolean;              { Boolean flag that indicates if the data in the body of the message is compressed or not }
       MessageType : TMessageType;         { Type of message being sent }
       ExtraBytes  : integer;              { For future use. Indicates the size of the buffer between the end of the header and the beginning of the data section }
     end; // sizeof(TBINHeader) = 5+1+4+4 = 13
     {.$ENDIF}*)

     { Summary: Event type declaration
       Description: Event type declaration. Properties TROBINMessage.OnCompress and TROBINMessage.OnDecompress are of this type. }
     TCompressionEvent = procedure(OriginalSize, CompressedSize, CompressionTime : integer) of object;

     {---------------------- TROBINMessage ----------------------}

     { Summary: RemObjects binary message encoding class
       Description:
         RemObjects binary message encoding class. It handles the encoding and decoding of binary messages
         in the RemObject proprietary format. Use this type of message when you don't need to interoperate
         with external, non-RemObjects based clients or servers. TROBINMessage provides additional features
         for encryption and compression. This message format is the most optimized.

         Binary messages have header of type TBINHeader. Immediately after the header there's the actual data. }
     TROBINMessage = class(TROMessage)
     private
       fStream : TStream;
       fUseCompression: boolean;
       fDestroyStream : boolean;
       fOnCompress: TCompressionEvent;
       fOnDecompress: TCompressionEvent;

       procedure WriteStream(aMessageType: TMessageType; Source, Destination : TStream);

     protected
       {---------------------- Internals ----------------------}
       function ReadException : Exception; override;
       function CreateSerializer : TROSerializer; override;

       {---------------------- IROMessage ----------------------}
       procedure Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName : string); override;

       procedure WriteToStream(aStream : TStream); override;
       procedure ReadFromStream(aStream : TStream); override;

       procedure WriteException(aStream : TStream; anException : Exception); override;

     public
       constructor Create(aOwner : TComponent); override;
       destructor Destroy; override;

       procedure Assign(iSource:TPersistent); override;

       {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
       property Stream:TStream read FStream;
       {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

     published
       { Summary: Specifies if the message should be compressed
         Description:
           Specifies if the message should be compressed. Compression adds enormous benefits when
           streaming a message over a network and adds very little overhead. If you send a compressed message to
           the server you are not guaranteed to receive it compressed on its way back. UseCompression has to be set
           on both servers and clients in order to do complete compressed messaging. }
       property UseCompression : boolean read fUseCompression write fUseCompression default true;
       { Summary: Fired when the message is about to be compressed
         Description: Fired when the outgoing message is about to be compressed }
       property OnCompress : TCompressionEvent read fOnCompress write fOnCompress;
       { Summary: Fired when the message is about to be decompressed
         Description: Fired when the incoming message is about to be decompressed }
       property OnDecompress : TCompressionEvent read fOnDecompress write fOnDecompress;
     end;

implementation

uses {$IFDEF MSWINDOWS} Windows, {$ENDIF MSWINDOWS}
     {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}eDebugServer,{$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}
     {$IFDEF DOTNET}
     RemObjects.SDK.Types,
     RemObjects.SDK.StreamSerializer,
     {$ELSE}
     uROTypes,
     uROStreamSerializer, 
     {$ENDIF}
     uRORes, uRoCompression, uROHTTPTools, uROStreamHelpers;

{$IFNDEF MSWINDOWS}
function GetTickCount: Longint; // TODO: Find an equivalent for Kylix!
begin
  Result := 0;
end;
{$ENDIF}

{ TROBINMessage }

destructor TROBINMessage.Destroy;
begin
  if fDestroyStream
    then FreeAndNIL(fStream);
  inherited;
end;

function TROBINMessage.CreateSerializer : TROSerializer;
begin
  result := TROStreamSerializer.Create(NIL);
end;

type
  THackMemoryStream = class(TMemoryStream);

procedure TROBINMessage.Initialize(const aTransport : IROTransport; const anInterfaceName, aMessageName : string);
{var sze : integer;}
begin
  inherited;

  SetHTTPInfo(aTransport, dfBinary);

  if fDestroyStream then FreeAndNIL(fStream);
  fStream := TMemoryStream.Create;
  THackMemoryStream(fStream).Capacity := 1024 * 1024 * 2; // Support up to 2 MB without any reallocation
  fDestroyStream := TRUE;

  Serializer.SetStorageRef(fStream); // Very important!

  MessageName := aMessageName;
  InterfaceName := anInterfaceName;

  Stream_WriteStringWithLength(fStream,InterfaceName);
  Stream_WriteStringWithLength(fStream,MessageName);

  {sze := Length(InterfaceName);
  fStream.Write(sze, SizeOf(Integer));
  if (InterfaceName<>'')
    then fStream.Write(InterfaceName[1], sze);

  sze := Length(MessageName);
  fStream.Write(sze, SizeOf(Integer));
  if (MessageName<>'')
    then fStream.Write(MessageName[1], sze);}

end;

procedure TROBINMessage.ReadFromStream(aStream: TStream);
var //sze : integer;
    //s : string;
    start : cardinal;
    lHeader : TBINHeader;
begin
  inherited;

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  DebugServer.EnterMethod('TROBINMessage.ReadFromStream(stream=%x; position:$%x)',[integer(pointer(aStream)), aStream.Position]); try
  DebugServer.WriteHexDump('Incoming BIN, start of ReadFromStream: ',aStream);
  //aStream.Seek(0,soBeginning);
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}

  if fDestroyStream then FreeAndNIL(fStream);

  lHeader := TBINHeader.CreateFromStream(aStream);
  try

    if lHeader.Compressed then begin
      start := GetTickCount;

      fDestroyStream := TRUE;
      fStream := TMemoryStream.Create;
      THackMemoryStream(fStream).Capacity := 1024 * 1024 * 2; // Support up to 2 MB without any reallocation

      DecompressStream(aStream, fStream);
      if Assigned(OnDeCompress)
        then OnDecompress(fStream.Size, aStream.Size, GetTickCount-start);

      fStream.Position := 0;

    end
    else begin
      fStream := aStream;
      fDestroyStream := FALSE;
    end;

    Serializer.SetStorageRef(fStream); // Very important!

    case lHeader.MessageType of
      typMessage:begin
          InterfaceName := Stream_ReadStringWithLength(fStream,MAX_ITEM_NAME);
          MessageName := Stream_ReadStringWithLength(fStream,MAX_ITEM_NAME);
        end;
      typException:begin
          ProcessException;
         end;
      else // Do what?
    end;

  finally
    FreeAndNil(lHeader);
  end;

  {$IFDEF DEBUG_REMOBJECTS_BINMESSAGE}
  finally DebugServer.ExitMethod('TROBINMessage.ReadFromStream(stream=%x; position:$%x)',[integer(pointer(aStream)), aStream.Position]); end;
  {$ENDIF DEBUG_REMOBJECTS_BINMESSAGE}
  (* { old code before .NET adustment }

  aStream.Read(lHeader, SizeOf(lHeader));
  if (lHeader.Signature<>BINSignature)
    then RaiseError(err_InvalidHeader, []);

  if lHeader.Compressed then begin
    start := GetTickCount;

    fDestroyStream := TRUE;
    fStream := TMemoryStream.Create;

    DecompressStream(aStream, fStream);
    if Assigned(OnDeCompress)
      then OnDecompress(fStream.Size, aStream.Size, GetTickCount-start);

    fStream.Position := 0;
  end
  else begin
    fStream := aStream;
    fDestroyStream := FALSE;
  end;

  Serializer.SetStorageRef(fStream); // Very important!

  if (lHeader.MessageType=typMessage) then begin

    fStream.Read(sze, SizeOf(Integer));
    SetLength(s, sze);
    if (sze>0)
      then fStream.Read(s[1], sze);
    InterfaceName := s;

    fStream.Read(sze, SizeOf(Integer));
    SetLength(s, sze);
    if (sze>0)
      then fStream.Read(s[1], sze);
    MessageName := s;
  end
  else ProcessException; *)
end;

procedure TROBINMessage.WriteToStream(aStream: TStream);
begin
  WriteStream(typMessage, fStream, aStream);

  inherited;
end;

function TROBINMessage.ReadException : Exception; 
var //sze : integer;
    lExceptionName, lMessage : string;
    lExceptionClass:ExceptionClass;
begin
  // At this point the stream is already at position 1

  lExceptionName := Stream_ReadStringWithLength(fStream,MAX_ITEM_NAME);
  lMessage := Stream_ReadStringWithLength(fStream,MAX_EXCEPTION_TEXT);

  {fStream.Read(sze, SizeOf(sze));
  SetLength(lExceptionName, sze);
  if (sze>0) then fStream.Read(lExceptionName[1], sze);

  fStream.Read(sze, SizeOf(sze));
  SetLength(lMessage, sze);
  if (sze>0) then fStream.Read(lMessage[1], sze);}

  lExceptionClass := GetExceptionClass(lExceptionName);
  if Assigned(lExceptionClass) then result := lExceptionClass.Create(lMessage)
  else result := Exception.Create(lExceptionName+': '+lMessage)
end;

procedure TROBINMessage.WriteException(aStream : TStream; anException: Exception);
{var sze : integer;
    str : string;}
begin
  if fDestroyStream then FreeAndNIL(fStream);
  fStream := TMemoryStream.Create;
  fDestroyStream := TRUE;

  Stream_WriteStringWithLength(fStream,anException.ClassName);
  Stream_WriteStringWithLength(fStream,anException.Message);

  {str := anException.ClassName;

  sze := Length(str);
  fStream.Write(sze, SizeOf(sze));
  fStream.Write(str[1], sze);

  sze := Length(anException.Message);
  fStream.Write(sze, SizeOf(sze));
  fStream.Write(anException.Message[1], sze);}

  WriteStream(typException, fStream, aStream);
end;

procedure TROBINMessage.WriteStream(aMessageType: TMessageType; Source, Destination : TStream);
var start : cardinal;
    lHeader : TBINHeader;
begin
  // Writes the header

  lHeader := TBINHeader.Create();
  try
    lHeader.Compressed := UseCompression;
    lHeader.MessageType := aMessageType;

    lHeader.WriteToStream(Destination);
  finally
    FreeAndNil(lHeader);
  end;

  // Writes the data
  Source.Position := 0;
  if UseCompression then begin
    start := GetTickCount;
    CompressStream(Source, Destination);
    if Assigned(OnCompress)
      then OnCompress(Source.Size, Destination.Size, GetTickCount-start);
  end
  else TMemoryStream(Source).SaveToStream(Destination);

 (* { old code before .NET adustment }
  FilLChar(lHeader,sizeof(lHeader),$23);

  lHeader.Signature := BINSignature;
  lHeader.Compressed := UseCompression;
  lHeader.MessageType := aMessageType;
  lHeader.ExtraBytes := 0; // For future use

  Destination.Write(lHeader, SizeOf(lHeader));

  // Writes the data
  Source.Position := 0;
  if UseCompression then begin
    start := GetTickCount;
    CompressStream(Source, Destination);
    if Assigned(OnCompress)
      then OnCompress(Source.Size, Destination.Size, GetTickCount-start);
  end
  else TMemoryStream(Source).SaveToStream(Destination); *)
end;

constructor TROBINMessage.Create(aOwner: TComponent);
begin
  inherited;
  UseCompression := TRUE;
end;

procedure TROBINMessage.Assign(iSource: TPersistent);
var lSource:TROBINMessage;
begin
  if Assigned(iSource) then begin

    if not (iSource is TROBINMessage) then RaiseError('Cannot Assign a %s t a %s',[ClassName,iSource.ClassName]);

    lSource := (iSource as TROBINMessage);
    self.UseCompression := lSource.UseCompression;
    self.OnCompress := lSource.OnCompress;
    self.OnDecompress := lSource.OnDecompress;
  end;
end;

{ TBINHeader }

const OFFSET_MESSAGE_FLAGS = $05; { offset of Flags field in header }
      OFFSET_MESSAGE_TYPE  = $06; { offset of Type field in header }

      BINMESSAGE_FLAG_COMPRESSED = $01;

constructor TBINHeader.Create;
begin
  inherited Create();
  fHeader[0] := ord(BINSignature[0]);
  fHeader[1] := ord(BINSignature[1]);
  fHeader[2] := ord(BINSignature[2]);
  fHeader[3] := ord(BINSignature[3]);
  fHeader[4] := ord(BINSignature[4]);
end;

{$IFDEF DEBUG_RO}
constructor TBINHeader.CreateFromStream(iStream: TStream);
var
  Data: string;
  HTML: TStringList;
begin
  inherited Create();
  iStream.Read(fHeader, SizeOf(fHeader));
  if not SignatureValid then begin
    Data := '';
    SetLength(Data, iStream.Size); // Not UNICODE safe
    iStream.Position := 0;
    iStream.Read(Data[1], iStream.Size);
    HTML := TStringList.Create;
    try
      HTML.Text := Data;
      HTML.SaveToFile('ROError.html');
    finally
      FreeAndNil(HTML);
    end;
    RaiseError(err_InvalidHeader + ' (see ROError.html for full details)', []);
  end;
end;
{$ELSE}
constructor TBINHeader.CreateFromStream(iStream: TStream);
begin
  inherited Create();
  iStream.Read(fHeader, SizeOf(fHeader));
  if not SignatureValid then RaiseError(err_InvalidHeader, []);
end;
{$ENDIF}

function TBINHeader.GetMessageType: TMessageType;
begin
  result := TMessageType(fHeader[OFFSET_MESSAGE_TYPE]);
end;

procedure TBINHeader.SetMessageType(const Value: TMessageType);
begin
  fHeader[OFFSET_MESSAGE_TYPE] := byte(Value);
end;

function TBINHeader.GetSignatureValid: boolean;
begin
  result := (fHeader[0] = ord(BINSignature[0])) and
            (fHeader[1] = ord(BINSignature[1])) and
            (fHeader[2] = ord(BINSignature[2])) and
            (fHeader[3] = ord(BINSignature[3])) and
            (fHeader[4] = ord(BINSignature[4]));
end;

function TBINHeader.GetCompressed: boolean;
begin
  result := fHeader[OFFSET_MESSAGE_FLAGS] and BINMESSAGE_FLAG_COMPRESSED = BINMESSAGE_FLAG_COMPRESSED;
end;

procedure TBINHeader.SetCompressed(const Value: boolean);
begin
  if Value then
    fHeader[OFFSET_MESSAGE_FLAGS] := fHeader[OFFSET_MESSAGE_FLAGS] or BINMESSAGE_FLAG_COMPRESSED
  else
    fHeader[OFFSET_MESSAGE_FLAGS] := fHeader[OFFSET_MESSAGE_FLAGS] and not BINMESSAGE_FLAG_COMPRESSED;
end;

procedure TBINHeader.WriteToStream(iStream: TStream);
begin
  iStream.Write(fHeader, SizeOf(fHeader));
end;

initialization
  RegisterMessageClass(TROBINMessage);

end.
