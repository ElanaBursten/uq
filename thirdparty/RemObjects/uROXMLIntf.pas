unit uROXMLIntf;

{$I RemObjects.inc}

interface

uses Classes;

type
  { Misc. }
  TXMLEncoding = (xeUTF8, xeUTF16); // Some are missing...

const
  XMLEncodingStr : array[TXMLEncoding] of string = ('UTF8', 'UTF16');

type
  { Forward declarations }
  IXMLNodeList = interface;

  { IXMLNode }
  IXMLNode = interface
  ['{8E0508D5-EFC0-4E1F-9BC5-2CBE3536D008}']
    function GetName : widestring;
    function GetLocalName : widestring;

    function GetRef : pointer;

    function GetParent : IXMLNode;

    function GetValue : Variant;
    procedure SetValue(const Value : Variant);

    function GetXML : widestring;

    function GetAttributes(Index : integer) : IXMLNode;
    function GetAttributeCount : integer;

    function GetChildren(Index : integer) : IXMLNode;
    function GetChildrenCount : integer;

    function Add(const aNodeName : widestring) : IXMLNode;
    function AddAttribute(const anAttributeName : widestring; const anAttributeValue : Variant) : IXMLNode; 

    procedure Delete(Index : integer);
    procedure Remove(const aNode : IXMLNode);

    function GetNodeByName(const aNodeName : widestring) : IXMLNode; // Returns NIL if none is found or exception. Up to you.

    function GetAttributeByName(const anAttributeName : widestring) : IXMLNode;

    function GetNodesByName(const aNodeName : widestring) : IXMLNodeList; // Returns NIL if none are found or exception. Up to you.

    function GetNodeByAttribute(const anAttributeName, anAttributeValue : widestring) : IXMLNode; // Returns NIL if none are found or exception. Up to you.

    property Name : widestring read GetName; // When one is creates it MUST have a name
    property LocalName : widestring read GetLocalName;
    property Value : Variant read GetValue write SetValue; // Only possible if there are no children.

    property Attributes[Index : integer] : IXMLNode read GetAttributes; // NIL if it has no attributes. I am open to other solutions
    property AttributeCount : integer read GetAttributeCount;

    property Children[Index : integer] : IXMLNode read GetChildren; // NIL if it has no children. I am open to other solutions
    property ChildrenCount : integer read GetChildrenCount;

    property XML : widestring read GetXML;
    property Ref : pointer read GetRef;
    property Parent : IXMLNode read GetParent;
  end;

  IXMLNodeList = interface
  ['{C5393D52-8CB1-4E36-9A8D-25396C3C6716}']
    function GetNodes(Index : integer) : IXMLNode;
    function GetCount : integer;

    property Nodes[Index : integer] : IXMLNode read GetNodes; default;
    property Count : integer read GetCount;
  end;

  { IXMLDocument }
  IXMLDocument = interface
  ['{61715E40-F8D5-4A7E-A01B-80114EF35428}']
    function GetDocumentNode : IXMLNode;

    function GetEncoding : TXMLEncoding;

    procedure New(aDocumentName : widestring = ''; anEncoding : TXMLEncoding = xeUTF8); // Once the encoding is set cannot be changed

    procedure SaveToStream(aStream : TStream);
    procedure SaveToFile(const aFileName : string);
    procedure LoadFromStream(aStream : TStream);
    procedure LoadFromFile(const aFileName : string);

    property Encoding : TXMLEncoding read GetEncoding;
    property DocumentNode : IXMLNode read GetDocumentNode; // NIL until New is called. New frees everything each time is called
  end;

function CreateXMLDoument : IXMLDocument;

implementation

uses
  {$IFDEF RemObjects_OpenXML}
  uROOpenXMLImpl
  {$ENDIF}
  {$IFDEF RemObjects_MSXML}
  uROMSXMLImpl
  {$ENDIF}
  ;

function CreateXMLDoument : IXMLDocument;
begin
  {$IFDEF RemObjects_OpenXML}
  result := TROOpenXMLDocument.Create;
  {$ENDIF}
  {$IFDEF RemObjects_MSXML}
  result := TROMSXMLDocument.Create;
  {$ENDIF}
end;

end.
