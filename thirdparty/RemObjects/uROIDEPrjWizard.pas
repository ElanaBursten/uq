unit uROIDEPrjWizard;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Windows, SysUtils,
     {$IFDEF DELPHI6UP} DesignEditors, DesignIntf, {$ELSE} DsgnIntf, {$ENDIF}
     ToolsApi, Graphics;

const
  IconName = 'Icon.ico';
  InfoName = 'Info.ini';

  sect_Information = 'Information';
  sect_ClassFactories = 'Class Factories';
  sect_ProjectGeneration = 'Project Generation';

  id_Name        = 'Name';
  id_Description = 'Description';
  id_MinVersion  = 'MinimumVersion';
  id_InvalidServers = 'InvalidServers';
  id_DefaultServer = 'DefaultServer';

  id_SvcLibName       = 'SvcLibName';
  id_SvcName          = 'SvcName';
  id_PrjName          = 'PrjName';
  id_PrjDir           = 'PrjDir';
  id_cbSvcInstantMode = 'cbSvcInstantMode';

  DelphiModules : array[0..4] of string = ('.dpr', '.pas', '.rodl', '.xml', '.dfm');

type { TROIDEProjectOptions }
     TROIDEProjectOptions = record
       TemplateDir,
       ServiceLibraryName,
       ServiceName,
       ProjectDir,
       ProjectName,
       ServerClassName,
       MessageClassName : string;
     end;

  { TROProjectWizard }
  TROProjectWizard = class(TNotifierObject,
    IOTAWizard,
    IOTARepositoryWizard,
    IOTAProjectWizard)
  private
    fTemplateDir,
    fComment,
    fName : string;
    procedure CreateProject(const someProjectOptions: TROIDEProjectOptions;
      const ScanExtensions: array of string);
    function ReplaceCtrlStrings(const someText: string;
      const someProjectOptions: TROIDEProjectOptions): string;

  protected
    { support for IOTAWizard }
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;
    { support for IOTARepositoryWizard }
    function GetAuthor: string;
    function GetComment: string;
    function GetPage: string;
    {$IFDEF DELPHI6UP}
    function GetGlyph: Cardinal;
    {$ELSE}
    function GetGlyph: HIcon;
    {$ENDIF}

  public
    constructor Create(const aTemplateDir, aName, aComment : string);
    destructor Destroy; override;
  end;

procedure Register;

implementation

uses ComObj, Forms, Classes, Controls, uRORes, uRODL, uRODLGenTools, INIFiles, Dialogs, fServerProjectOptions;

procedure Register;
var dirinfo : TSearchRec;
    s,
    templdirname,
    templname,
    templcomment : string;
    ini     : TIniFile;
    tmpcnt : integer;
    thisver, minver : integer;
begin
  // Trick to get the Delphi version. Not sure how to do it otherwise
  try
    s := Trim(Application.MainForm.Caption);
    s := Copy(s, Pos(' ', s)+1,1);

    thisver := StrToInt(s);
  except
    thisver := 5;
  end;

  tmpcnt := 0;
  if (FindFirst(GetTemplateDir+'*.*', faDirectory, dirinfo)=0) then repeat
    ini := NIL;

    try
      if (dirinfo.Attr and faDirectory=0) or
         (Pos('.', dirinfo.Name)=1) or
         (Pos('_', dirinfo.Name)=1)
      then Continue;

      templdirname := IncludeTrailingBackslash(GetTemplateDir+dirinfo.Name);

      ini := TIniFile.Create(templdirname+InfoName);
      minver := ini.ReadInteger(sect_Information, id_MinVersion, 5);
      templname := ini.ReadString(sect_Information, id_Name, '???');
      templcomment := ini.ReadString(sect_Information, id_Description, '???');

      if thisver<minver then Continue;

      // Register the template wizard
      RegisterPackageWizard(
          TROProjectWizard.Create(templdirname, templname, templcomment)
        as IOTAProjectWizard);

      Inc(tmpcnt);

    finally
      ini.Free;
    end;

  until (FindNext(dirinfo)<>0);

  FindClose(dirinfo);

  // Status message just in case
  if (tmpcnt=0)
    then MessageDlg('No RemObject server templates were found under '+GetTemplateDir, mtWarning, [mbOK], 0)
end;

{ TROProjectWizard }

constructor TROProjectWizard.Create(const aTemplateDir, aName,
  aComment : string);
begin
  inherited Create;

  fName := aName;
  fTemplateDir := aTemplateDir;
  fComment := aComment;
end;

destructor TROProjectWizard.Destroy;
begin
  inherited;
end;

procedure TROProjectWizard.Execute;
var prjopt : TROIDEProjectOptions;
begin
  if PromptProjectOptions(fTemplateDir, prjopt) then begin
    CreateProject(prjopt, DelphiModules);

    with prjopt do begin
      (BorlandIDEServices as IOTAActionServices).OpenProject(ProjectDir+ProjectName+'.dpr', FALSE);
    end;
  end;
end;

procedure TROProjectWizard.CreateProject(const someProjectOptions : TROIDEProjectOptions; const ScanExtensions : array of string);

  function CanWriteFile(const aFilename : string) : boolean;
  begin
    result := not FileExists(aFileName);
    if not result then begin
      Beep;
      result := MessageDlg(Format('File %s already exists. Overwrite?', [aFileName]), mtWarning, [mbYes, mbNo], 0)=mrYes;
    end;
  end;

  procedure CopyTemplateFiles(const SourceDir : string);
  var s, d, templatedir : string;
      dirinfo : TSearchRec;
      sl : TStringList;
      i : integer;
      docopy : boolean;
  begin
    sl := TStringList.Create;
    templatedir := IncludeTrailingBackslash(SourceDir);
    try
      if (FindFirst(templatedir+'*.*', faArchive, dirinfo)=0) then repeat
        if (CompareText(dirinfo.Name, IconName)=0) or
           (CompareText(dirinfo.Name, InfoName)=0) then Continue;

        s := templatedir+dirinfo.Name;
        d := someProjectOptions.ProjectDir+ReplaceCtrlStrings(dirinfo.Name, someProjectOptions);

        docopy := TRUE;
        for i := 0 to High(ScanExtensions) do
          if (CompareText(ExtractFileExt(dirinfo.Name), ScanExtensions[i])=0) then begin

            if not CanWriteFile(d) then Continue;

            sl.LoadFromFile(s);
            sl.Text := ReplaceCtrlStrings(sl.Text, someProjectOptions);
            sl.SaveToFile(d);

            docopy := FALSE;
            Break;
          end;

        if docopy then begin
          if CanWriteFile(d)
            then CopyFile(PChar(s), PChar(d), FALSE);
        end;

      until (FindNext(dirinfo)<>0);

      FindClose(dirinfo);
    finally
      sl.Free;
    end;
  end;

begin
  CopyTemplateFiles(someProjectOptions.TemplateDir);
  CopyTemplateFiles(GetTemplateDir);
end;

function TROProjectWizard.ReplaceCtrlStrings(const someText : string; const someProjectOptions : TROIDEProjectOptions) : string;
var cs : string;
    guid : TGUID;
begin
  cs := UpperCase(someText);
  guid := NewUID;

  result := StringReplace(someText, '$PRJNAME', someProjectOptions.ProjectName, [rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '$SVCNAME', someProjectOptions.ServiceName, [rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '$SVCLIBNAME', someProjectOptions.ServiceLibraryName, [rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '$NEWID', GUIDToString(guid), [rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '$MSGCLSNAME', someProjectOptions.MessageClassName, [rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '$SRVCLSNAME', someProjectOptions.ServerClassName, [rfReplaceAll, rfIgnoreCase]);
end;

function TROProjectWizard.GetAuthor: string;
begin
  Result := str_Author;
end;

function TROProjectWizard.GetComment: string;
begin
  Result := fComment
end;

{$IFDEF DELPHI6UP}
function TROProjectWizard.GetGlyph: Cardinal;
var icon : TIcon;
begin
  if FileExists(fTemplateDir+IconName) then begin
    icon := TIcon.Create;
    icon.LoadFromFile(fTemplateDir+IconName);
    result := icon.Handle;
  end
  else result := 0;
end;
{$ELSE}
function TROProjectWizard.GetGlyph: HICON;
var icon : TIcon;
begin
  if FileExists(fTemplateDir+IconName) then begin
    icon := TIcon.Create;
    icon.LoadFromFile(fTemplateDir+IconName);
    result := icon.Handle;
  end
  else result := 0;
end;
{$ENDIF}

function TROProjectWizard.GetIDString: string;
begin
  Result := GUIDToString(NewUID)
end;

function TROProjectWizard.GetName: string;
begin
  Result := fName
end;

function TROProjectWizard.GetPage: string;
begin
  Result := str_ProductName;
end;

function TROProjectWizard.GetState: TWizardState;
begin
  Result := [];
end;

end.
