{ Summary: Contains the class TROSerializer }
unit uROSerializer;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

{$IFDEF DOTNET}
{$MESSAGE error 'This unit will not be used in .NET, use RemObjects.SDK.Serializer instead' }
{$ENDIF}

interface

uses
  {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
  Classes, TypInfo;

const ArrayItemName = 'item';
      StreamClsName = 'TStream';

type {---------------- TROSerializer ----------------}

     { Summary: Provides the foundation for serialization classes.
       Description:
         Provides the foundation for serialization classes. Each message class (i.e. TROSOAPMessage or TROBINMessage) requires
         data types to be serialized according to the protocol they conform to.
         Since the serialization rules could be the same even in different message types (i.e. XML-RPC and SOAP), a layer
         of separation was necessary.
         TROSerializer introduces all the methods required to serialize simple and complex types, including lists and streams.
         See TROXMLSerializer and TROStreamSerializer for complete, protocol-specific implementations.
     }

     TROSerializer = class
     private

     protected
       {------------- Writers -------------}
       procedure WriteInteger(const aName : string; anOrdType : TOrdType; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteInt64(const aName : string; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteString(const aName : string; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteWideString(const aName : string; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteDateTime(const aName : string; const Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure WriteFloat(const aName : string; aFloatType : TFloatType; const Ref; ArrayElementId : integer = -1); virtual; abstract;

       procedure WriteObject(const aName : string; aClass : TClass; const Ref; ArrayElementId : integer = -1);
       procedure BeginWriteObject(const aName: string; aClass : TClass; anObject: TObject; var LevelRef : IUnknown;
                                  var IsValidType : boolean; ArrayElementId : integer = -1); virtual;
       procedure EndWriteObject(const aName: string; aClass : TClass; anObject: TObject; const LevelRef : IUnknown); virtual;

       procedure CustomWriteObject(const aName : string; aClass : TClass; const Ref; ArrayElementId : integer = -1); virtual;

       {------------- Readers -------------}
       procedure ReadInteger(const aName : string; anOrdType : TOrdType; var Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure ReadInt64(const aName : string; var Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure ReadEnumerated(const aName : string; anEnumTypeInfo : PTypeInfo; var Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure ReadString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); virtual; abstract;
       procedure ReadWideString(const aName : string; var Ref; ArrayElementId : integer = -1; iMaxLength:integer=-1); virtual; abstract;
       procedure ReadDateTime(const aName : string; var Ref; ArrayElementId : integer = -1); virtual; abstract;
       procedure ReadFloat(const aName : string; aFloatType : TFloatType; var Ref; ArrayElementId : integer = -1); virtual; abstract;

       procedure ReadObject(const aName : string; aClass : TClass; var Ref; ArrayElementId : integer = -1);
       procedure BeginReadObject(const aName : string; aClass : TClass; var anObject : TObject; var LevelRef : IUnknown;
                                 var IsValidType : boolean; ArrayElementId : integer = -1); virtual;
       procedure EndReadObject(const aName : string; aClass : TClass; var anObject : TObject; const LevelRef : IUnknown); virtual;

       procedure CustomReadObject(const aName : string; aClass : TClass; var Ref; ArrayElementId : integer = -1); virtual;

     public
       constructor Create(aStorageRef: pointer); virtual;
       destructor Destroy; override;

       { Summary: Main Write method.
         Description:
           Main Write method. By checking the parameter aTypeInfo it will the invoke the appropriate
           protected WriteXXXX method and serialize the parameter pointed by Ref. }
       procedure Write(const aName : string; aTypeInfo : PTypeInfo; const Ref; ArrayElementId : integer = -1);
       { Summary: Main Read method.
         Description:
           Main Read method. By checking the parameter aTypeInfo it will the invoke the appropriate
           protected ReadXXXX method, deserialize the parameter at write the result at the address pointed by Ptr. }
       procedure Read(const aName : string; aTypeInfo : PTypeInfo; var Ptr; ArrayElementId : integer = -1); {$IFDEF DEBUG_REMOBJECTS}virtual;{$ENDIF}

       { Summary: Sets the internal reference to the data-storage media
         Description:
           Sets the internal reference to the data-storage media.
           The storage is specific to the serializer. A TROStreamSerializer would expect a TStream when a TROXMLSerializer
           expects a DOM document. }
       function SetStorageRef(aStorageRef: pointer) : boolean; virtual; abstract;
     end;

     TROSerializerClass = class of TROSerializer;

implementation

uses uRORes, uROTypes, SysUtils;

{ TROSerializer }

procedure TROSerializer.BeginReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
begin
  //IsValidType := Assigned(anObject) or (anObject is TROComplexType);
  IsValidType := aClass.InheritsFrom(TROComplexType)
end;

procedure TROSerializer.BeginWriteObject(const aName: string;
  aClass : TClass; anObject: TObject; var LevelRef : IUnknown; var IsValidType : boolean; ArrayElementId : integer = -1);
begin
  //IsValidType := Assigned(anObject) or (anObject is TROComplexType);
  IsValidType := aClass.InheritsFrom(TROComplexType)
end;

constructor TROSerializer.Create(aStorageRef: pointer);
begin
  inherited Create;

  if (pointer(aStorageRef)<>NIL) and not SetStorageRef(aStorageRef)
    then RaiseError(err_InvalidStorage, []);
end;

procedure TROSerializer.CustomReadObject(const aName: string; aClass : TClass; var Ref;
  ArrayElementId: integer);
var obj : TObject absolute Ref;
    i, cnt : integer;
    itemref : pointer;
begin
  if (obj is TROArray) then with TROArray(obj) do begin
    cnt := TROArray(obj).Count;

    if (GetItemClass<>NIL) then begin
      for i := 0 to (cnt-1) do begin
        itemref := NIL;
        Read(ArrayItemName, GetItemType, itemref, i);
        SetItemRef(i, itemref);
      end;
    end
    else begin
      for i := 0 to (cnt-1) do begin
        itemref := GetItemRef(i);
        Read(ArrayItemName, GetItemType, itemref^, i);
      end;
    end;
  end
end;

procedure TROSerializer.CustomWriteObject(const aName: string; aClass : TClass; const Ref; ArrayElementId : integer = -1);
var obj : TObject absolute Ref;
    i : integer;
    itemref : pointer;
begin
  if (obj is TROArray) then with TROArray(obj) do begin
    if (GetItemClass<>NIL) then begin
      for i := 0 to (Count-1) do begin
        itemref := GetItemRef(i);
        Write(ArrayItemName, GetItemType, itemref, i);
      end;
    end
    else begin
      for i := 0 to (Count-1) do begin
        itemref := GetItemRef(i);
        Write(ArrayItemName, GetItemType, itemref^, i);
      end;
    end;
  end;
end;

destructor TROSerializer.Destroy;
begin
  inherited;
end;

procedure TROSerializer.EndReadObject(const aName: string;
  aClass : TClass; var anObject: TObject; const LevelRef : IUnknown);
begin

end;

procedure TROSerializer.EndWriteObject(const aName: string;
  aClass : TClass; anObject: TObject; const LevelRef : IUnknown);
begin

end;

procedure TROSerializer.Read(const aName: string; aTypeInfo: PTypeInfo;
  var Ptr; ArrayElementId : integer = -1);
begin
  case aTypeInfo^.Kind of
    tkEnumeration : ReadEnumerated(aName, aTypeInfo, byte(Ptr), ArrayElementId);
    tkInteger  : ReadInteger(aName, GetTypeData(aTypeInfo)^.OrdType, Ptr, ArrayElementId);
    tkInt64    : ReadInt64(aName, Ptr, ArrayElementId);

    tkFloat    : if (aTypeInfo=TypeInfo(TDateTime))
                   then ReadDateTime(aName, Ptr, ArrayElementId)
                   else ReadFloat(aName, GetTypeData(aTypeInfo)^.FloatType, Ptr, ArrayElementId);

    tkWString  : ReadWideString(aName, Ptr, ArrayElementId);
    tkLString,
    tkString   : ReadString(aName, Ptr, ArrayElementId);

    tkClass    : ReadObject(aName, GetTypeData(aTypeInfo).ClassType, Ptr, ArrayElementId);

    else RaiseError(err_TypeNotSupported, [GetEnumName(TypeInfo(TTypeKind), Ord(aTypeInfo^.Kind))]);
  end;
end;

procedure TROSerializer.ReadObject(const aName: string; aClass : TClass; var Ref; ArrayElementId : integer = -1);
var obj : TObject absolute Ref;
    props : PPropList;
    cnt, i : integer;
    LevelRef : IUnknown;
    validtype : boolean;

    // Temporary variables
    int64val : int64;
    intval : integer;
    enuval : byte;
    dblval : double;
    //extval : extended;
    strval : string;
    {$IFNDEF DELPHI5}wstrval : widestring;{$ENDIF}
    objval : TObject;
begin
  BeginReadObject(aName, aClass, obj, levelref, validtype, ArrayElementId);

  //Todo -cimportant: obj might be nil here, which would cause an AV when validtype is also false
  if not Assigned(obj) then RaiseError(err_ObjectExpectedInStream, [obj.ClassName]);
  if not validtype then RaiseError(err_TypeNotSupported, [obj.ClassName]);

  if (obj<>NIL) and (obj.ClassInfo<>NIL) then begin
    cnt := GetTypeData(obj.ClassInfo).PropCount;
    if (cnt>0) then begin
      GetMem(props, cnt*SizeOf(PPropInfo));
      try
        cnt := GetPropList(PTypeInfo(obj.ClassInfo), tkProperties, props);

        for i := 0 to (cnt-1) do begin
          with props^[i]^ do

            case PropType^.Kind of
              tkEnumeration : begin
                ReadEnumerated(Name, PropType^, enuval);
                SetOrdProp(obj, Name, enuval);
              end;

              tkInteger : begin
                ReadInteger(Name, GetTypeData(PropType^).OrdType, intval);
                SetOrdProp(obj, Name, intval);
              end;

              tkFloat : begin
                ReadFloat(Name, GetTypeData(PropType^).FloatType, dblval);
                SetFloatProp(obj, Name, dblval);
              end;

              tkLString,
              tkString : begin
                ReadString(Name, strval);
                SetStrProp(obj, Name, strval);
              end;

              tkInt64 : begin
                ReadInt64(Name, int64val);
                SetInt64Prop(obj, Name, int64val);
              end;

              tkWString : begin
                {$IFDEF DELPHI5}
                //RaiseError(err_TypeNotSupported, ['tkWString']);
                ReadString(Name, strval);
                SetStrProp(obj, Name, strval);
                {$ELSE}
                ReadWideString(Name, wstrval);
                SetWideStrProp(obj, Name, wstrval);
                {$ENDIF}
              end;

              tkClass : begin
                ReadObject(Name, GetTypeData(PropType^).ClassType, objval);
                SetObjectProp(obj, Name, objval);
              end;

              else RaiseError(err_TypeNotSupported, [GetEnumName(TypeInfo(TTypeKind), Ord(props^[i].PropType^.Kind))])
            end;
        end;
      finally
        FreeMem(props, cnt*SizeOf(PPropInfo));
      end;
    end;
  end;

  CustomReadObject(aName, aClass, obj, ArrayElementId);

  EndReadObject(aName, aClass, obj, levelref);
end;

procedure TROSerializer.Write(const aName: string; aTypeInfo: PTypeInfo;
  const Ref; ArrayElementId : integer = -1);
begin
  case aTypeInfo^.Kind of
    tkEnumeration : WriteEnumerated(aName, aTypeInfo, Ref);
    tkInteger     : WriteInteger(aName, GetTypeData(aTypeInfo)^.OrdType, Ref);

    tkFloat       : if (aTypeInfo=TypeInfo(TDateTime))
                      then WriteDateTime(aName, Ref)
                      else WriteFloat(aName, GetTypeData(aTypeInfo)^.FloatType, Ref);

    tkWString     : WriteWideString(aName, Ref);
    tkLString,
    tkString      : WriteString(aName, Ref);
    tkInt64       : WriteInt64(aName, Ref);

    tkClass       : WriteObject(aName, GetTypeData(aTypeInfo).ClassType, Ref, ArrayElementId);

    else RaiseError(err_TypeNotSupported, [GetEnumName(TypeInfo(TTypeKind), Ord(aTypeInfo^.Kind))]);
  end;
end;

procedure TROSerializer.WriteObject(const aName: string; aClass : TClass; const Ref; ArrayElementId : integer = -1);
var obj : TObject absolute Ref;
    props : PPropList;
    cnt, i : integer;
    validtype : boolean;
    LevelRef : IUnknown;

    // Temporary variables
    int64val : int64;
    intval : integer;
    enuval : byte;
    dblval : double;
    strval : string;
    {$IFNDEF DELPHI5}wstrval : widestring;{$ENDIF}
    objval : TObject;
    pdata : PTypeData;
begin
  BeginWriteObject(aName, aClass, obj, levelref, validtype, ArrayElementId);
  if not validtype then RaiseError(err_TypeNotSupported, [obj.ClassName]);

  if (obj<>NIL) and (obj.ClassInfo<>NIL) then begin
    pdata := GetTypeData(obj.ClassInfo);
    if (pdata<>NIL) then begin
      cnt := pdata .PropCount;
      if (cnt>0) then begin
        GetMem(props, cnt*SizeOf(PPropInfo));
        try
          cnt := GetPropList(PTypeInfo(obj.ClassInfo), tkProperties, props);

          for i := 0 to (cnt-1) do begin
            with props^[i]^ do

              case PropType^.Kind of
                tkEnumeration : begin
                  enuval := GetOrdProp(obj, Name);
                  WriteEnumerated(props^[i]^.Name, PropType^, enuval);
                end;

                tkInteger : begin
                  intval := GetOrdProp(obj, Name);
                  WriteInteger(props^[i]^.Name, GetTypeData(PropType^)^.OrdType, intval);
                end;

                tkFloat : begin
                  dblval := GetFloatProp(obj, Name);
                  WriteFloat(props^[i]^.Name, GetTypeData(PropType^)^.FloatType, dblval);
                end;

                tkLString,
                tkString : begin
                  strval := GetStrProp(obj, Name);
                  WriteString(Name, strval);
                end;

                tkInt64 : begin
                  int64val := GetInt64Prop(obj, Name);
                  WriteInt64(Name, int64val);
                end;

                tkWString : begin
                  {$IFDEF DELPHI5}
                  //RaiseError(err_TypeNotSupported, ['tkWString']);
                  strval := GetStrProp(obj, Name);
                  WriteString(Name, strval);
                  {$ELSE}
                  wstrval := GetWideStrProp(obj, Name);
                  WriteWideString(Name, wstrval);
                  {$ENDIF}
                end;

                tkClass : begin
                  objval := GetObjectProp(obj, Name);
                  WriteObject(Name, GetTypeData(PropType^).ClassType, objval);
                end;

                else RaiseError(err_TypeNotSupported, [GetEnumName(TypeInfo(TTypeKind), Ord(props^[i].PropType^.Kind))])
              end;
          end;
        finally
          FreeMem(props, cnt*SizeOf(PPropInfo));
        end;
      end;
    end;
  end;

  CustomWriteObject(aName, aClass, obj);

  EndWriteObject(aName, aClass, obj, levelref);
end;

end.
