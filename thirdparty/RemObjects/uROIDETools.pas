unit uROIDETools;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Delphi IDE Integration
{
{ compiler: Delphi 5 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses Classes, ToolsAPI, Contnrs;

const MaxSourceSize = 10000;

      kw_RODLGeneration = '#ROGEN';

type { TIDEMessage }
     TIDEMessageType = (mInfo, mError, mWarning);

     TIDEMessage = class
     private
       fMessageType : TIDEMessageType;
       fFileName,
       fMessageStr,
       fPrefixStr: string;

     public
       constructor Create(aMessageType : TIDEMessageType;
                          const aMessageStr : string;
                          const aFileName : string = '';
                          const aPrefixStr: string = '');

       property MessageType : TIDEMessageType read fMessageType;
       property FileName : string read fFileName;
       property MessageStr : string read fMessageStr;
       property PrefixStr : string read fPrefixStr;
     end;

     { TIDEMessageList }
     TIDEMessageList = class(TObjectList)
     private
       fPrefixStr : string;

       function GetItems(Index: integer): TIDEMessage;

     public
       constructor Create(const aPrefixStr : string);
       destructor Destroy; override;

       procedure ClearMessages;
       procedure Add(aMessageType : TIDEMessageType;
                     const aMessageStr : string;
                     const aFileName : string = '';
                     const aPrefixStr: string = '');

       procedure FlushMessages;

       property Items[Index : integer] : TIDEMessage read GetItems; default;
     end;

// Service access helpers
function ModuleServices : IOTAModuleServices;
function CurrentProject : IOTAProject;
function MessageServices: IOTAMessageServices;
function Services : IOTAServices;
function ActionServices : IOTAActionServices;

//  Module helpers
function ModuleFileName(const aModule: IOTAModule) : string;
function ModuleDir(const aModule: IOTAModule) : string;
function ModuleSourceSize(const aModule: IOTAModule) : integer;

function ReadModuleSource(const aModule: IOTAModule) : string;
procedure WriteModuleSource(const aModule: IOTAModule; const Source  : string);

// Project helpers
function FindModule(const aProject : IOTAProject; const aModuleName : string) : IOTAModule;
function GenerateRESFromRODL(const RODLFileName : string; aMessageList : TIDEMessageList) : boolean;
function ExtractRODLFileName(const Source : string) : string; // Only returns the file name specified in the dpr file

implementation

uses Windows, SysUtils, Dialogs, ShellAPI, Controls, uRORes, {$IFDEF LINUX}
fCustomIDEMessagesFormKylix, {$ELSE}
fCustomIDEMessagesForm, {$ENDIF}uROResWriter;

// Service access helpers
function ModuleServices : IOTAModuleServices;
begin
  result := (BorlandIDEServices as IOTAModuleServices);
end;

function CurrentProject : IOTAProject;
var services: IOTAModuleServices;
    module: IOTAModule;
    project: IOTAProject;
    projectgroup: IOTAProjectGroup;
    multipleprojects: Boolean;
    i: Integer;
begin
  result := NIL;

  multipleprojects := False;
  services := ModuleServices;

  if (services=NIL) then Exit;

  for I := 0 to (services.ModuleCount-1) do begin
    module := services.Modules[I];
    if (module.QueryInterface(IOTAProjectGroup, ProjectGroup) = S_OK) then begin
      result := ProjectGroup.ActiveProject;
      Exit;
    end

    else if module.QueryInterface(IOTAProject, Project) = S_OK then begin
      if (result=NIL)
        then result := Project // Found the first project, so save it
        else multipleprojects := True; // It doesn't look good, but keep searching for a project group
    end;
  end;

  if multipleprojects then result := nil;
end;

function MessageServices: IOTAMessageServices;
begin
  result := (BorlandIDEServices as IOTAMessageServices);
end;

function Services : IOTAServices;
begin
  result := (BorlandIDEServices as IOTAServices);
end;

function ActionServices : IOTAActionServices;
begin
  result := (BorlandIDEServices as IOTAActionServices)
end;

//  Module helpers
function ModuleFileName(const aModule: IOTAModule) : string;
begin
  result := aModule.FileName;
  { AleF: removed for Delphi5
  result := '';

  with aModule do
    for i := 0 to ModuleFileCount-1 do
      if Supports(ModuleFileEditors[i], IOTASourceEditor, editor) then begin
        result := editor.FileName;
        Exit;
      end;}
end;

function ModuleDir(const aModule: IOTAModule) : string;
begin
  result := IncludeTrailingBackslash(ExtractFilePath(ModuleFileName(aModule)))
end;

function ModuleSourceSize(const aModule : IOTAModule) : integer;
begin
  result := Length(ReadModuleSource(aModule));
end;

function ReadModuleSource(const aModule: IOTAModule) : string;
var l, i : integer;
    editor : IOTASourceEditor;
    reader : IOTAEditReader;
begin
  result := '';
  with aModule do
    for i := 0 to GetModuleFileCount-1 do begin
      if Supports(GetModuleFileEditor(i), IOTASourceEditor, editor) then begin
        // TODO: find a way not to depend on files smaller than 10k... I only use this for DPRs so it's fine for now
        SetLength(result, MaxSourceSize);
        //l := 0; to remove warning

        reader := editor.CreateReader;
        l := reader.GetText(0, @result[1], MaxSourceSize);
        reader := NIL;

        SetLength(result, l);
        Exit;
      end;
    end;
end;

procedure WriteModuleSource(const aModule: IOTAModule; const Source  : string);
var i : integer;
    editor : IOTASourceEditor;
    writer : IOTAEditWriter;
begin
  with aModule do
    for i := 0 to GetModuleFileCount-1 do begin
      if Supports(GetModuleFileEditor(i), IOTASourceEditor, editor) then begin
        writer := editor.CreateWriter;
        writer.DeleteTo(MaxInt);
        writer.Insert(PChar(Source));
        writer := NIL;
      end;
    end;
end;

{ TIDEMessageList }

{$IFDEF DELPHI7UP}
procedure TIDEMessageList.Add(aMessageType : TIDEMessageType;
                     const aMessageStr : string;
                     const aFileName : string = '';
                     const aPrefixStr: string = '');
var lGroup:IOTAMessageGroup;
    lDummyLineRef:pointer;
    lPrefix:string;
begin
  with BorlandIDEServices as IOTAMessageServices60 do begin
    lGroup := GetGroup(str_ProductName);
    if not Assigned(lGroup) then lGroup := AddMessageGroup(str_ProductName);

    lPrefix := aPrefixStr;
    if lPrefix = '' then lPrefix := 'Note';
    AddToolMessage(aFilename,aMessageStr,lPrefix,-1,-1,nil,lDummyLineRef,lGroup);
  end;    { with }
end;
{$ELSE}
procedure TIDEMessageList.Add(aMessageType : TIDEMessageType;
                     const aMessageStr : string;
                     const aFileName : string = '';
                     const aPrefixStr: string = '');
var msg : TIDEMessage;
begin
  msg := TIDEMessage.Create(aMessageType, aMessageStr, aFileName, aPrefixStr);
  inherited Add(msg);
end;
{$ENDIF}

procedure TIDEMessageList.ClearMessages;
{$IFDEF DELPHI7UP}
var lGroup:IOTAMessageGroup;
begin
  with BorlandIDEServices as IOTAMessageServices60 do begin
    lGroup := GetGroup(str_ProductName);
    if not Assigned(lGroup) then lGroup := AddMessageGroup(str_ProductName);
    RemoveMessageGroup(lGroup);
  end;
end;
{$ELSE}
begin end;
{$ENDIF}

constructor TIDEMessageList.Create(const aPrefixStr: string);
begin
  inherited Create;
  fPrefixStr := aPrefixStr;
end;

destructor TIDEMessageList.Destroy;
begin
  inherited;
end;

procedure TIDEMessageList.FlushMessages;
begin
  {$IFNDEF DELPHI7UP}
  IDEMessageForm.FillList(Self);
  Clear;
  {$ENDIF DELPHI7UP}
end;

// Project helpers
function FindModule(const aProject : IOTAProject; const aModuleName : string) : IOTAModule;
var i : integer;
begin
  result := NIL;
  for i := 0 to aProject.GetModuleCount-1 do
    if (CompareText(aModuleName, aProject.GetModule(i).FileName)=0) then begin
      result := aProject.GetModule(i).OpenModule;
      Exit;
    end;
end;

function TIDEMessageList.GetItems(Index: integer): TIDEMessage;
begin
  result := TIDEMessage(inherited Items[Index]);
end;

{ TIDEMessage }

constructor TIDEMessage.Create(aMessageType: TIDEMessageType;
  const aMessageStr, aFileName, aPrefixStr: string);
begin
  fMessageType := aMessageType;
  fMessageStr := aMessageStr;
  fFileName := aFileName;
  fPrefixStr := aPrefixStr;
end;

// File names helpers
function ExtractRODLFileName(const Source : string) : string;
var src : string;
    i, fnameidx, idx : integer;
begin
  src := UpperCase(Source);
  idx := Pos(kw_RODLGeneration, src);
  fnameidx := 0;

  if (idx=0) then result := '' // Does not link to a RODL file
  else begin
    for i := idx to Length(src) do begin
      case src[i] of
        '}' : begin
          if not (fnameidx>0) then raise Exception.Create('Unspecified RODL filename')
          else begin
            result := Trim(Copy(Source, fnameidx, i-fnameidx));
            Exit;
          end;
        end;

        ':' : if (fnameidx=0) then fnameidx := i+1;
      end;
    end;
  end;
end;

function GenerateRESFromRODL(const RODLFileName : string; aMessageList : TIDEMessageList) : boolean;
var {outname, }resname{, pars} : string;
    //res : integer;
    resdata, rodldata : TFileStream;
begin                                             
  rodldata := NIL;
  resdata := NIL;

  //result := FALSE;
  try
    try
      resname := IncludeTrailingBackslash(ExtractFilePath(RODLFileName))+res_RODLFile+'.res';

      resdata := TFileStream.Create(resname, fmCreate);
      rodldata := TFileStream.Create(RODLFileName, fmOpenRead+fmShareDenyNone);
      rodldata.Position := 0;

      WriteRES(rodldata, resdata, res_RODLFile);

      result := FileExists(resname);
    except
      on E:Exception do begin
        MessageDlg('The following error occourred while trying to generate the resource file.'#13+
                   E.Message, mtError, [mbOK], 0);
        result := FALSE;
      end;
    end;
  finally
    rodldata.Free;
    resdata.Free;
  end;
{  try
    // Creates an rc file called RODLFile.rc and saves it where the RODL file is located
    outname := IncludeTrailingBackslash(ExtractFilePath(RODLFileName))+res_RODLFile+'.rc';

    with TStringList.Create do try
      Add(Format('%s RCDATA "%s"', [res_RODLFile, RODLFileName]));
      SaveToFile(outname);
    finally
      Free;
    end;

    resname := ChangeFileExt(outname, '.RES');
    DeleteFile(resname);

    if FileExists(resname) then begin
      aMessageList.Add(mError,'The file '+resname+' could not be deleted');
    end;

    pars := '"'+outname+'" "'+resname+'"';
    res := ShellExecute(0, 'open', 'BRCC32', PChar(pars), NIL, SW_HIDE);
    aMessageList.Add(mInfo, 'BRCC32 '+pars);

    if (res<=32) then begin
      aMessageList.Add(mError,Format('ShellExecute returned %s', [res]));
      result := FALSE;
    end
    else result := TRUE;
  except
    on E:Exception do begin
      MessageDlg('The following error occourred while trying to generate the resource file.'#13+
                 E.Message, mtError, [mbOK], 0);
      result := FALSE;
    end;
  end;}
end;

end.
