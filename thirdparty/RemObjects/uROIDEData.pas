unit uROIDEData;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - Core Library
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses
  {$IFDEF DELPHI5}Forms,{$ENDIF}
  SysUtils, Classes, ImgList, Controls;

type
  TIdeData = class(TDataModule)
    iml_Actions: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  ICON_REGENERATE = 1;
  ICON_IMPORT     = 2;
  ICON_MAKESERVER = 3;

var
  IdeData: TIdeData;

implementation

{$R *.dfm}

end.
