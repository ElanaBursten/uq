unit uRODLToImpl;

{----------------------------------------------------------------------------}
{ RemObjects SDK Library - CodeGen
{
{ compiler: Delphi 5 and up, Kylix 2 and up
{ platform: Win32, Linux
{
{ (c)opyright RemObjects Software. all rights reserved.
{
{ Using this code requires a valid license of the RemObjects SDK
{ which can be obtained at http://www.remobjects.com.
{----------------------------------------------------------------------------}

{$I RemObjects.inc}

interface

uses uRODL;

type { TRODLToImpl }
     TRODLToImpl = class(TRODLConverter)
     private
       fService : TRODLService;

       procedure WriteServiceDeclaration(const aService: TRODLService);
       procedure WriteOperationImplementation(const anOperation : TRODLOperation);

     protected
       procedure IntConvert(const aLibrary : TRODLLIbrary; const aTargetEntity : string = ''); override;
       function ValidateTargetEntity(const aLibrary : TRODLLIbrary; const aTargetEntity : string) : boolean; override;

     public
       class function GetTargetFileName(const aLibrary : TRODLLIbrary; const aTargetEntity : string = ''): string; override;

     end;

implementation

uses SysUtils, Dialogs, uRODLGenTools, uRODLToInvk, uRODLToPascal;

{ TRODLToImpl }

procedure TRODLToImpl.IntConvert(const aLibrary: TRODLLIbrary; const aTargetEntity : string = '');
var i : integer;
    s : string;
begin
  if not Assigned(fService) or not Assigned(fService.Default) then exit;

  Write(Format('unit %s;', [ChangeFileExt(GetTargetFileName(aLibrary, TargetEntity), '')]));
  WriteEmptyLine;

  Write(ImplNotice);
  WriteEmptyLine;

  Write('interface');
  WriteEmptyLine;

  Write('uses');
  Write('{vcl:} Classes,' ,PASCAL_INDENTATION_LEVEL_1);
  Write('{RemObjects:} uROClientIntf, uROServer, uROServerIntf,' ,PASCAL_INDENTATION_LEVEL_1);
  Write(Format('{Generated:} %s_Intf;', [aLibrary.Info.Name]),PASCAL_INDENTATION_LEVEL_1);
  WriteEmptyLine;

  Write('type');

  WriteServiceDeclaration(fService);

  WriteEmptyLine;

  Write('implementation');
  WriteEmptyLine;

  s := ChangeFileExt(TRODLToInvk.GetTargetFileName(aLibrary),'');
  Write('uses');
  Write(Format('{Generated:} %s;', [s]),PASCAL_INDENTATION_LEVEL_1);

  WriteEmptyLine;

  Write(Format('procedure Create_%s(out anInstance : IUnknown);', [fService.Info.Name]));
  Write('begin');
  Write(Format('  anInstance := T%s.Create;', [fService.Info.Name]));
  Write('end;');
  WriteEmptyLine;

  if Assigned(fService.Default) then begin
    for i := 0 to fService.Default.Count-1 do
      WriteOperationImplementation(fService.Default.Items[i]);
  end;

  Write('initialization');
  s := fService.Info.Name;
  s := Format('  TROClassFactory.Create(''%s'', Create_%s, T%s_Invoker);', [s,s,s]);
  Write(s);

  WriteEmptyLine;

  Write('finalization');
  WriteEmptyLine;

  Write('end.');
  s := buffer.Text;
  //showmessage(INtToSTr(Length(s)));
end;

procedure TRODLToImpl.WriteServiceDeclaration(const aService : TRODLService);
var i : integer;
begin
  Write(Format('T%s = class(TRORemotable, %s)', [aService.Info.Name, aService.Info.Name]), PASCAL_INDENTATION_LEVEL_1);
  Write('private',PASCAL_INDENTATION_LEVEL_1);
  Write('protected',PASCAL_INDENTATION_LEVEL_1);

  for i := 0 to (aService.Default.Count-1) do
    Write(Format(GetOperationDefinition(aService.Default.Items[i]), []), PASCAL_INDENTATION_LEVEL_2);

  Write('end;', PASCAL_INDENTATION_LEVEL_1);
end;

function TRODLToImpl.ValidateTargetEntity(
  const aLibrary: TRODLLIbrary; const aTargetEntity: string): boolean;
var i : integer;
begin
  result := FALSE;

  for i := 0 to (aLibrary.ServiceCount-1) do
    if (CompareText(aLibrary.Services[i].Info.Name, aTargetEntity)=0) then begin
      fService := aLibrary.Services[i]; // Will be used later
      result := TRUE;
      Exit;
    end;
end;

procedure TRODLToImpl.WriteOperationImplementation(
  const anOperation: TRODLOperation);
var i : integer;
begin
  Write(GetOperationDefinition(anOperation, 'T'+fService.Info.Name));
  Write('begin');
  Write('end;');

  for i := 0 to (anOperation.Count-1) do begin
  end;

  WriteEmptyLine;
end;

class function TRODLToImpl.GetTargetFileName(
  const aLibrary: TRODLLIbrary; const aTargetEntity: string): string;
begin
  result := aTargetEntity+'_Impl.pas';
end;

end.

