Apache Thrift compiler: thrift*.exe 

Example command line:
cd "C:\Oasis Digital\QManager\uq-qm\bin"
thrift-0.9.1.exe -verbose -o "c:\FolderWhereGeneratedDelphiSourceShouldGo\MustBeValidExistingDir\" --gen delphi:register_types "C:\Oasis Digital\QManager\uq-qm\server\QMServerLibrary.thrift"


Use this command to see all the Thrift compiler options:
thrift-0.9.1.exe -help


Thrift links:
http://thrift.apache.org/tutorial/delphi/ (Apache Thrift Delphi tutorial)
http://thrift.apache.org/static/files/thrift-20070401.pdf (The Facebook Developers' white paper)
http://www.manning.com/abernethy/tPGtApacheThrift_MEAP_ch1.pdf (Programmer's Guide)

Regarding Thrift string type and Delphi compiler interpretation:
http://stackoverflow.com/questions/17580870/utf8-version-of-widestring


N.B.:
1) One of the Thrift source files (\thirdparty\Thrift\src\Thrift.Transport.pas) has been customized by Oasis Digital to optionally support usage of Web.Win.Sockets via 
the USE_THRIFT_SOCKETS compiler directive. The reason for this is so that Oasis Digital apps that do not need Thrift services/sockets will not be forced to include the 
inet170.bpl runtime package.
