{******************************************************************************}
{*                                                                            *}
{* (C) Copyright 1999-2002, Lasse V�gs�ther Karlsen                           *}
{*                                                                            *}
{******************************************************************************}

{ Description:
    This unit contains easy to use functions for compression and decompression
    through the zLib compression engine.

    Note: This is not a ZIP compression package, it will not handle .zip files,
      although the compression used is compatible with the one used in .zip
      files, the framework around the compressed data is missing.
}
unit lvkZLibUtils;

interface

uses
  lvkZLib, lvkZLibConsts, SysUtils, Classes, lvkSafeMem;

const
  DEFAULT_BUFFER_SIZE       = 32*1024;
  DEFAULT_WRITE_HEADER      = True;
  DEFAULT_READ_HEADER       = DEFAULT_WRITE_HEADER;
  DEFAULT_COMPRESSION_LEVEL = Z_BEST_COMPRESSION;

{ Description:
    This function compresses a file and stores the resulting data into another
    file.
  Parameters:
    SourceFileName - Full path and name of the file containing data to compress.
    SourceStream - The stream to read the uncompressed data from.
    Input - The string or memory block to read the uncompressed data to.
    DestinationFileName - Full path and name of the file to store the
      compressed data into.
    DestinationStream - The stream to write the compressed data to.
    CompressionLevel - How much work to put into compressing the data. More
      work results in smaller files, but will take slightly longer to process.
      The value range from 0 (no compression) to 9 (best compression). Default
      is to use the best compression level (ie. 9).
    BufferSize - How large input and output buffers to process the data through.
      Using larger buffers means more work can be done before having to transfer
      more data to/from the files, but will of course mean a higher memory
      total on your application.
    WriteHeader - Leave it to True to be compatible with the .ZIP file format
      compression, set it to False to be compatible with the Deflate algorithm
      used with web browsers, and with the gzip file format.
  Returns:
    For the string and memory variants, the function returns the compressed
    stream or memory block.
  See also:
    zLibDecompress
}
procedure zLibCompress(const SourceFileName, DestinationFileName: string;
  const CompressionLevel: Integer=DEFAULT_COMPRESSION_LEVEL;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const WriteHeader: Boolean=DEFAULT_WRITE_HEADER); overload;

{ Description:
    This function decompresses a compressed file and stores the resulting data
    into another file.
  Parameters:
    SourceFileName - Full path and name of the file containing data to
      decompress.
    SourceStream - The stream to read the compressed data from.
    Input - The input memory or string to read the compressed data from.
    DestinationFileName - Full path and name of the file to store the
      decompressed data into.
    DestinationStream - The stream to write the decompressed data to.
    BufferSize - How large input and output buffers to process the data through.
      Using larger buffers means more work can be done before having to transfer
      more data to/from the files, but will of course mean a higher memory
      total on your application.
    ReadHeader - Leave it to True to be compatible with the .ZIP file format
      compression, set it to False to be compatible with the Deflate algorithm
      used with web browsers, and with the gzip file format. Note that this
      parameter must have the same value as the WriteHeader parameter you used
      with zLibCompressXYZ.
  Returns:
    For the string and memory variants, the function returns the decompressed
    stream or memory block.
  See also:
    zLibCompress
}
procedure zLibDecompress(const SourceFileName, DestinationFileName: string;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const ReadHeader: Boolean=DEFAULT_READ_HEADER); overload;

// <COMBINE zLibCompress@string@string@Integer@Cardinal@Boolean>
procedure zLibCompress(const SourceStream, DestinationStream: TStream;
  const CompressionLevel: Integer=DEFAULT_COMPRESSION_LEVEL;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const WriteHeader: Boolean=DEFAULT_WRITE_HEADER); overload;

// <COMBINE zLibDecompress@string@string@Cardinal@Boolean>
procedure zLibDecompress(const SourceStream, DestinationStream: TStream;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const ReadHeader: Boolean=DEFAULT_READ_HEADER); overload;

// <COMBINE zLibCompress@string@string@Integer@Cardinal@Boolean>
function zLibCompress(const Input: string;
  const CompressionLevel: Integer=DEFAULT_COMPRESSION_LEVEL;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const WriteHeader: Boolean=DEFAULT_WRITE_HEADER): string; overload;

// <COMBINE zLibDecompress@string@string@Cardinal@Boolean>
function zLibDecompress(const Input: string;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const ReadHeader: Boolean=DEFAULT_READ_HEADER): string; overload;

// <COMBINE zLibCompress@string@string@Integer@Cardinal@Boolean>
function zLibCompress(const Input: ISafeMem;
  const CompressionLevel: Integer=DEFAULT_COMPRESSION_LEVEL;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const WriteHeader: Boolean=DEFAULT_WRITE_HEADER): ISafeMem; overload;

// <COMBINE zLibDecompress@string@string@Cardinal@Boolean>
function zLibDecompress(const Input: ISafeMem;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const ReadHeader: Boolean=DEFAULT_READ_HEADER): ISafeMem; overload;

implementation

uses
  lvkZLibTypes, Windows;

procedure zLibCompress(const SourceFileName, DestinationFileName: string;
  const CompressionLevel: Integer; const BufferSize: Cardinal;
  const WriteHeader: Boolean);
var
  SourceStream      : TStream;
  DestinationStream : TStream;
begin
  SourceStream := nil;
  DestinationStream := nil;
  try
    SourceStream := TFileStream.Create(SourceFileName, fmOpenRead or
      fmShareDenyWrite);
    DestinationStream := TFileStream.Create(DestinationFileName, fmCreate);

    zLibCompress(SourceStream, DestinationStream, CompressionLevel, BufferSize,
      WriteHeader);
  finally
    DestinationStream.Free;
    SourceStream.Free;
  end;
end;

procedure zLibDecompress(const SourceFileName, DestinationFileName: string;
  const BufferSize: Cardinal; const ReadHeader: Boolean);
var
  SourceStream      : TStream;
  DestinationStream : TStream;
begin
  SourceStream := nil;
  DestinationStream := nil;
  try
    SourceStream := TFileStream.Create(SourceFileName, fmOpenRead or
      fmShareDenyWrite);
    DestinationStream := TFileStream.Create(DestinationFileName, fmCreate);

    zLibDecompress(SourceStream, DestinationStream, BufferSize, ReadHeader);
  finally
    DestinationStream.Free;
    SourceStream.Free;
  end;
end;

procedure zLibCompress(const SourceStream, DestinationStream: TStream;
  const CompressionLevel: Integer; const BufferSize: Cardinal;
  const WriteHeader: Boolean);
var
  zs            : z_stream;
  InputBuffer   : ISafeMem;
  OutputBuffer  : ISafeMem;
  errorCode     : Integer;
  AtEnd         : Boolean;

  procedure Initialize;
  begin
    ZeroMemory(@zs, SizeOf(zs));

    zs.next_in := InputBuffer.Pointer;
    zs.next_out := OutputBuffer.Pointer;
    zs.avail_out := OutputBuffer.Size;

    if WriteHeader then
      zLibCheckCompress(deflateInit(zs, CompressionLevel))
    else
      zLibCheckCompress(deflateInit2(zs, CompressionLevel, Z_DEFLATED, -15, 9,
        Z_DEFAULT_STRATEGY));
  end;

  procedure Cleanup;
  begin
    zLibCheckCompress(deflateEnd(zs));
  end;

begin
  Assert((CompressionLevel >= -1) and (CompressionLevel <= 9));
  Assert(BufferSize >= 2048);

  InputBuffer := AllocateSafeMem(BufferSize);
  OutputBuffer := InputBuffer.Clone(False);

  Initialize;
  try
    SourceStream.Position := 0;
    AtEnd := False;

    repeat
      if zs.avail_in = 0 then
      begin
        zs.next_in := InputBuffer.Pointer;
        zs.avail_in := SourceStream.Read(zs.next_in^, InputBuffer.Size);
        if zs.avail_in < BufferSize then
          AtEnd := True;
      end;

      if AtEnd then
        errorCode := deflate(zs, Z_FINISH)
      else
        errorCode := deflate(zs, Z_NO_FLUSH);

      if (errorCode <> Z_STREAM_END) and (errorCode <> Z_OK) then
        zLibCheckCompress(errorCode);

      if (zs.avail_out = 0) or (errorCode = Z_STREAM_END) then
      begin
        DestinationStream.WriteBuffer(OutputBuffer.Pointer^, zs.next_out -
          OutputBuffer.Pointer);
        zs.next_out := OutputBuffer.Pointer;
        zs.avail_out := OutputBuffer.Size;
      end;
    until errorCode = Z_STREAM_END;
  finally
    Cleanup;
  end;
end;

procedure zLibDecompress(const SourceStream, DestinationStream: TStream;
  const BufferSize: Cardinal; const ReadHeader: Boolean);
var
  zs            : z_stream;
  InputBuffer   : ISafeMem;
  OutputBuffer  : ISafeMem;
  errorCode     : Integer;

  procedure Initialize;
  begin
    ZeroMemory(@zs, SizeOf(zs));

    zs.next_in := InputBuffer.Pointer;
    zs.next_out := OutputBuffer.Pointer;
    zs.avail_out := OutputBuffer.Size;

    if ReadHeader then
      zLibCheckDecompress(inflateInit(zs))
    else
      zLibCheckDecompress(inflateInit2(zs, -15));
  end;

  procedure Cleanup;
  begin
    zLibCheckDecompress(inflateEnd(zs));
  end;

begin
  Assert(BufferSize >= 2048);

  InputBuffer := AllocateSafeMem(BufferSize);
  OutputBuffer := InputBuffer.Clone(False);

  Initialize;
  try
    SourceStream.Position := 0;

    repeat
      if zs.avail_in = 0 then
      begin
        zs.next_in := InputBuffer.Pointer;
        zs.avail_in := SourceStream.Read(InputBuffer.Pointer^,
          InputBuffer.Size);
      end;

      errorCode := inflate(zs, Z_SYNC_FLUSH);
      if (errorCode <> Z_STREAM_END) and (errorCode <> Z_OK) then
        zLibCheckDecompress(errorCode);

      if (zs.avail_out = 0) or (errorCode = Z_STREAM_END) then
      begin
        DestinationStream.WriteBuffer(OutputBuffer.Pointer^, zs.next_out -
          OutputBuffer.Pointer);
        zs.next_out := OutputBuffer.Pointer;
        zs.avail_out := OutputBuffer.Size;
      end;
    until errorCode = Z_STREAM_END;
  finally
    Cleanup;
  end;
end;

function zLibCompress(const Input: string;
  const CompressionLevel: Integer; const BufferSize: Cardinal;
  const WriteHeader: Boolean): string;
var
  SourceStream      : TStringStream;
  DestinationStream : TStringStream;
begin
  SourceStream := nil;
  DestinationStream := nil;
  try
    SourceStream := TStringStream.Create(Input);
    DestinationStream := TStringStream.Create('');

    zLibCompress(SourceStream, DestinationStream, CompressionLevel, BufferSize,
      WriteHeader);

    Result := DestinationStream.DataString;
  finally
    DestinationStream.Free;
    SourceStream.Free;
  end;
end;

function zLibDecompress(const Input: string;
  const BufferSize: Cardinal; const ReadHeader: Boolean): string;
var
  SourceStream      : TStringStream;
  DestinationStream : TStringStream;
begin
  SourceStream := nil;
  DestinationStream := nil;
  try
    SourceStream := TStringStream.Create(Input);
    DestinationStream := TStringStream.Create('');

    zLibDecompress(SourceStream, DestinationStream, BufferSize, ReadHeader);

    Result := DestinationStream.DataString;
  finally
    DestinationStream.Free;
    SourceStream.Free;
  end;
end;

function zLibCompress(const Input: ISafeMem;
  const CompressionLevel: Integer=DEFAULT_COMPRESSION_LEVEL;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const WriteHeader: Boolean=DEFAULT_WRITE_HEADER): ISafeMem;
begin
  Assert(Assigned(Input));

  Result := AllocateSafeMem(0);
  zLibCompress(Input.Stream, Result.Stream, CompressionLevel, BufferSize,
    WriteHeader);
  Result.Stream.Position := 0;
end;

function zLibDecompress(const Input: ISafeMem;
  const BufferSize: Cardinal=DEFAULT_BUFFER_SIZE;
  const ReadHeader: Boolean=DEFAULT_READ_HEADER): ISafeMem;
begin
  Assert(Assigned(Input));

  Result := AllocateSafeMem(0);
  zLibDecompress(Input.Stream, Result.Stream, BufferSize, ReadHeader);
  Result.Stream.Position := 0;
end;

end.
