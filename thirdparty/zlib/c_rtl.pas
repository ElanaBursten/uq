{******************************************************************************}
{*                                                                            *}
{* (C) Copyright 1999-2002, Lasse V�gs�ther Karlsen                           *}
{*                                                                            *}
{******************************************************************************}

unit c_rtl;

interface

function malloc(size: Cardinal): Pointer; cdecl;
procedure free(p: Pointer); cdecl;
function memset(src: Pointer; c: Integer; n: Cardinal): Pointer; cdecl;
function memMove(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl;
function memcpy(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl;
function strncmp(const s1, s2: PChar; const MaxLen: Integer): Integer; cdecl;

implementation

uses
  SysUtils;

function malloc(size: Cardinal): Pointer; cdecl;
begin
  Result := AllocMem(size);
end;

procedure free(p: Pointer); cdecl;
begin
  FreeMem(p);
end;

function memset(src: Pointer; c: Integer; n: Cardinal): Pointer; cdecl;
begin
  fillchar(src^,n,c);
  Result := src;
end;

function memMove(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl;
begin
  Move(src^,dest^,n);
  Result := dest;
end;

function memcpy(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl;
begin
  Move(src^,dest^,n);
  Result := dest;
end;

function strncmp(const s1, s2: PChar; const MaxLen: Integer): Integer; cdecl;
begin
  Result := StrLComp(s1, s2, MaxLen);
end;

end.
 