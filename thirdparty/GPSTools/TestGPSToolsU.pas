unit TestGPSToolsU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComObj, UQGPS, GPSThread;

type
  TMainTestForm = class(TForm)
    GPSMemo: TMemo;
    GpsTimer: TTimer;
    Panel1: TPanel;
    StartButton: TButton;
    StopButton: TButton;
    Panel2: TPanel;
    StatusLabel: TLabel;
    RunThreadedCheckbox: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GpsTimerTimer(Sender: TObject);
  private
    GPS: TGPSInformation;
    GPSThd: TBackgroundGPSThread;
    LastLat, LastLon: Double;
    CurrentGPSLatitude: Double;
    CurrentGPSLongitude: Double;
    CurrentGPSHDOP: Integer;
    CurrentGPSFixTime: TDateTime;
    procedure ShowMyPosition;
    procedure StartMoving(Moving: Boolean);
    procedure CreateGPSThread;
    function RunThreaded: Boolean;
    procedure CreateGPSInfo;
    function GetMyGPSPosition(var Lat, Lon: Double; var HDOP: Integer): Boolean;
    procedure ShowGPSStatus(var Msg: TMessage); message UM_GPS_STATUS;
  end;

var
  MainTestForm: TMainTestForm;

implementation

uses StdConvs, ConvUtils, DateUtils, OdMiscUtils;

{$R *.dfm}

procedure TMainTestForm.FormCreate(Sender: TObject);
begin
  GPSMemo.Clear;
  LastLat := 0;
  LastLon := 0;
end;

procedure TMainTestForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(GPS);
  FreeAndNil(GPSThd);
end;

function TMainTestForm.RunThreaded: Boolean;
begin
  Result := RunThreadedCheckBox.Checked;
end;

procedure TMainTestForm.StartButtonClick(Sender: TObject);
begin
  if RunThreaded then
    CreateGPSThread
  else
    CreateGPSInfo;

  GpsMemo.Lines.Add('Started ' + DateTimeToStr(Now) + ' linear measurements in ' +
    ConvTypeToDescription(duFeet));
  StartMoving(True);
end;

procedure TMainTestForm.StopButtonClick(Sender: TObject);
begin
  StartMoving(False);
  FreeAndNil(GPSThd);
  FreeAndNil(GPS);
  StatusLabel.Caption := 'No active GPS';
  GpsMemo.Lines.Add('Stopped ' + DateTimeToStr(Now));
  GpsMemo.Lines.Add('-------');
end;

procedure TMainTestForm.StartMoving(Moving: Boolean);
begin
  if RunThreaded then begin
    Assert(Assigned(GPSThd));
    if Moving then
      GPSThd.StartNow
    else
      GPSThd.StopNow;
  end else begin
    Assert(Assigned(GPS));
    GpsTimer.Enabled := Moving;
    GPS.Enabled := Moving;
  end;

  StartButton.Enabled := not Moving;
  StopButton.Enabled := Moving;
  RunThreadedCheckbox.Enabled := not Moving;
end;


// Non-threaded mode methods start here
procedure TMainTestForm.CreateGPSInfo;
begin
  try
    GPS := TGPSInformation.Create(duFeet);
  except
    on E: EOleSysError do begin
      ShowMessage('Unable to create the GPS object. ' +
        'You may need to register GPSToolsXP230.dll first using regsvr32.' +
        #13#10#13#10 + E.Message);
      Abort;
    end;
    on E: Exception do begin
      ShowMessage('Unable to create GPS object. ' + #13#10#13#10 + E.Message);
      Abort;
    end;
  end;
end;

procedure TMainTestForm.GpsTimerTimer(Sender: TObject);
begin
  GpsTimer.Enabled := False;
  try
    ShowMyPosition;
  finally
    GpsTimer.Enabled := True;
  end;
end;

procedure TMainTestForm.ShowMyPosition;
var
  LocationInfo: TGPSPositionInfo;
  FeetMoved: Double;
  MetersMoved: Double;
begin
  Assert(Assigned(GPS));
  LocationInfo := GPS.GetMyPosition;
  if not LocationInfo.HaveFix then begin
    GpsMemo.Lines.Add(Format('%s Waiting for GPS to fix position.',
      [DateTimeToStr(Now)]));
    Exit;
  end;

  if not LocationInfo.HavePosition then begin
    GpsMemo.Lines.Add(Format('%s No GPS position acquired.',
      [DateTimeToStr(Now)]));
    Exit;
  end;

  if GPS.Enabled then
    StatusLabel.Caption := 'Connected to GPS on ' + GPS.ComPort
  else
    StatusLabel.Caption := 'No active GPS';

  if LastLat = 0 then
    MetersMoved := 0
  else
    MetersMoved := LatLongDistanceMeters(LocationInfo.Latitude,
      LocationInfo.Longitude, LastLat, LastLon);

  FeetMoved := Convert(MetersMoved, duMeters, duFeet);
  LastLat := LocationInfo.Latitude;
  LastLon := LocationInfo.Longitude;

  with LocationInfo do
    GpsMemo.Lines.Add(Format('%s  Pos: %.6f, %.6f  %sMoved: %f %s[Sats: %d %sHDOP: %f]',
      [DateTimeToStr(Now), Latitude, Longitude, #09, FeetMoved, #09, SatellitesUsed, #09, HDOP]));
end;


// Threaded mode methods start here
procedure TMainTestForm.CreateGPSThread;
begin
  GPSThd := TBackgroundGPSThread.Create(Application.MainForm.Handle, True,
    ExtractFilePath(Application.ExeName), 5);
end;

function TMainTestForm.GetMyGPSPosition(var Lat, Lon: Double; var HDOP: Integer): Boolean;
const
  MaxGPSAgeInSeconds = 60;
begin
  Result := WithinPastSeconds(Now, CurrentGPSFixTime, MaxGPSAgeInSeconds) and
    (CurrentGPSLatitude <> 0) and (CurrentGPSLongitude <> 0);
  if Result then begin
    Lat := CurrentGPSLatitude;
    Lon := CurrentGPSLongitude;
    HDOP := CurrentGPSHDOP;
  end;
end;

procedure TMainTestForm.ShowGPSStatus(var Msg: TMessage);
const
  DegreesSymbol = #176;
var
  Status: TGPSStatus;
begin
  Assert(TObject(Msg.LParam) is TGPSStatus, 'ShowGPSStatus needs a TGPSStatus object parameter.');
  Status := TGPSStatus(Msg.LParam);
  try
    if NotEmpty(Status.MessageText) then
      GpsMemo.Lines.Add(Status.MessageText);

    if Status.HavePosition then begin
      CurrentGPSFixTime := Now;
      CurrentGPSLatitude := Status.Latitude;
      CurrentGPSLongitude := Status.Longitude;
      CurrentGPSHDOP := Status.HDOP;
    end
    else begin
      CurrentGPSFixTime := 0;
      CurrentGPSLatitude := 0;
      CurrentGPSLongitude := 0;
      CurrentGPSHDOP := -1;
    end;

    if Status.HavePosition then
      GpsMemo.Lines.Add(Format('%s  %.3f%s,%.3f%s HDOP: %d',
        [FormatDateTime('ddd hh:nn:ss', Now),
        CurrentGPSLatitude, DegreesSymbol, CurrentGPSLongitude, DegreesSymbol,
        CurrentGPSHDOP]))
    else
      GpsMemo.Lines.Add(Format('%s  position unavailable',
        [FormatDateTime('ddd hh:nn:ss', Now)]))
  finally
    FreeAndNil(Status);
  end;
end;

end.
