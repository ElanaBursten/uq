unit GpsToolsXP_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 8291 $
// File generated on 6/25/2010 12:14:11 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\WINDOWS\system32\GpsToolsXP230.dll (1)
// LIBID: {3CEBAD2A-B8BC-44FD-87DB-C639266BFF4B}
// LCID: 0
// Helpfile: 
// HelpString: Franson GpsToolsXP 2.3
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// Errors:
//   Error creating palette bitmap of (TLicense) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TNmeaParser) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TNmeaSentence) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TPosition) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TCustomDatum) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TCustomGrid) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
//   Error creating palette bitmap of (TGpsToolsVersion) : Server C:\WINDOWS\system32\GpsToolsXP230.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GpsToolsXPMajorVersion = 1;
  GpsToolsXPMinorVersion = 0;

  LIBID_GpsToolsXP: TGUID = '{3CEBAD2A-B8BC-44FD-87DB-C639266BFF4B}';

  IID_ILicense: TGUID = '{772AA3F6-A49A-4472-964D-308DA64CA0F2}';
  IID_INmeaParser: TGUID = '{F05800D3-256C-486D-8E24-3483A207C330}';
  IID_IPosition: TGUID = '{C762FD70-DAED-4A04-8A7F-F3A820CD7513}';
  CLASS_License: TGUID = '{DB3C0ABE-C362-42C7-8FF8-52FD5EDE02E7}';
  DIID__INmeaParserEvents: TGUID = '{87D6E08D-4022-4CC2-8903-C9EB12D8DE1B}';
  IID_INmeaSentence: TGUID = '{AFD70F6F-36C7-4319-B160-E0B9FFC10912}';
  IID_INmeaGGA: TGUID = '{3F835160-B6D0-487B-A828-CB0DDD73D340}';
  IID_INmeaGLL: TGUID = '{CF4E432C-CBF3-4A33-9EA0-C3440DF0F594}';
  IID_INmeaRMC: TGUID = '{53284151-BC91-4A50-AC52-C0B490A11EA2}';
  IID_ISatellites: TGUID = '{517E5ACC-95FB-4BCC-B049-A65AD13C660C}';
  IID_ISatellite: TGUID = '{BDD097E1-9573-4DA2-95E1-1000090B4591}';
  IID_IMovement: TGUID = '{A03C2B35-8400-466B-B2C4-C34954C1C57D}';
  IID_IQuality: TGUID = '{9D098B93-3799-489B-8E55-40689BCBE07C}';
  IID_IGpsFix: TGUID = '{6B293970-1DDF-41EB-8CCF-CA2BF0A20728}';
  IID_IComStatus: TGUID = '{5F9E467B-EB06-4DE9-9631-EE4C5B2389FB}';
  IID_ICustomDatum: TGUID = '{E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}';
  IID_ICustomGrid: TGUID = '{F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}';
  IID_IGpsToolsVersion: TGUID = '{7E0EF922-5D91-4A67-9846-77C1D455AE53}';
  CLASS_NmeaParser: TGUID = '{BC32D60A-D170-4282-A0CA-FAAE7EF5A53E}';
  CLASS_NmeaSentence: TGUID = '{D1B811D9-9A7F-439C-876F-D20AA8BDE7DB}';
  CLASS_NmeaGGA: TGUID = '{C4D983F6-7C95-415C-B029-F7C8E3F0194F}';
  CLASS_NmeaGLL: TGUID = '{B57AB7B0-96F0-4B4C-80F8-3AB909F242C6}';
  CLASS_NmeaRMC: TGUID = '{2FD8CC93-84CA-4942-8841-25F5B6FD0AB3}';
  CLASS_Position: TGUID = '{161514DA-DBDF-46D1-A0D4-E272AFBFB22B}';
  CLASS_Satellites: TGUID = '{1FE7D23D-EAC4-43C2-A3AD-C696EC466D6C}';
  CLASS_Satellite: TGUID = '{6AE35788-527E-490A-9B96-F3B8894787CF}';
  CLASS_Movement: TGUID = '{B4E2BA45-2270-4E8C-9FA9-C48D53CA60FB}';
  CLASS_Quality: TGUID = '{8BED3172-A1C2-45DD-B511-1C1D4AB99D93}';
  CLASS_GpsFix: TGUID = '{E857E518-4EFA-49DA-B0CB-3CA414402FD7}';
  CLASS_ComStatus: TGUID = '{1D981C0A-21F1-4A57-AE11-6221B7DCE8DE}';
  CLASS_CustomDatum: TGUID = '{E6110D33-AD89-4384-996D-5F98EB199EAB}';
  CLASS_CustomGrid: TGUID = '{F293E541-4865-499E-B2E0-98E9AF79AC31}';
  CLASS_GpsToolsVersion: TGUID = '{7E5DF1C0-0008-420F-A7AD-B655C1059E31}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ILicense = interface;
  ILicenseDisp = dispinterface;
  INmeaParser = interface;
  INmeaParserDisp = dispinterface;
  IPosition = interface;
  IPositionDisp = dispinterface;
  _INmeaParserEvents = dispinterface;
  INmeaSentence = interface;
  INmeaSentenceDisp = dispinterface;
  INmeaGGA = interface;
  INmeaGGADisp = dispinterface;
  INmeaGLL = interface;
  INmeaGLLDisp = dispinterface;
  INmeaRMC = interface;
  INmeaRMCDisp = dispinterface;
  ISatellites = interface;
  ISatellitesDisp = dispinterface;
  ISatellite = interface;
  ISatelliteDisp = dispinterface;
  IMovement = interface;
  IMovementDisp = dispinterface;
  IQuality = interface;
  IQualityDisp = dispinterface;
  IGpsFix = interface;
  IGpsFixDisp = dispinterface;
  IComStatus = interface;
  IComStatusDisp = dispinterface;
  ICustomDatum = interface;
  ICustomDatumDisp = dispinterface;
  ICustomGrid = interface;
  ICustomGridDisp = dispinterface;
  IGpsToolsVersion = interface;
  IGpsToolsVersionDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  License = ILicense;
  NmeaParser = INmeaParser;
  NmeaSentence = INmeaSentence;
  NmeaGGA = INmeaGGA;
  NmeaGLL = INmeaGLL;
  NmeaRMC = INmeaRMC;
  Position = IPosition;
  Satellites = ISatellites;
  Satellite = ISatellite;
  Movement = IMovement;
  Quality = IQuality;
  GpsFix = IGpsFix;
  ComStatus = IComStatus;
  CustomDatum = ICustomDatum;
  CustomGrid = ICustomGrid;
  GpsToolsVersion = IGpsToolsVersion;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}


// *********************************************************************//
// Interface: ILicense
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {772AA3F6-A49A-4472-964D-308DA64CA0F2}
// *********************************************************************//
  ILicense = interface(IDispatch)
    ['{772AA3F6-A49A-4472-964D-308DA64CA0F2}']
    procedure Set_LicenseKey(Param1: OleVariant); safecall;
    procedure GateLicense(Email: OleVariant; Key: OleVariant); safecall;
    property LicenseKey: OleVariant write Set_LicenseKey;
  end;

// *********************************************************************//
// DispIntf:  ILicenseDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {772AA3F6-A49A-4472-964D-308DA64CA0F2}
// *********************************************************************//
  ILicenseDisp = dispinterface
    ['{772AA3F6-A49A-4472-964D-308DA64CA0F2}']
    property LicenseKey: OleVariant writeonly dispid 1;
    procedure GateLicense(Email: OleVariant; Key: OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: INmeaParser
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F05800D3-256C-486D-8E24-3483A207C330}
// *********************************************************************//
  INmeaParser = interface(IDispatch)
    ['{F05800D3-256C-486D-8E24-3483A207C330}']
    procedure Parse(Sentence: OleVariant); safecall;
    procedure Set_AllEventsGeneric(Param1: OleVariant); safecall;
    procedure Set_GpsDatum(Param1: OleVariant); safecall;
    procedure Set_ComPort(Param1: OleVariant); safecall;
    procedure Set_BaudRate(Param1: OleVariant); safecall;
    procedure Set_PortEnabled(Param1: OleVariant); safecall;
    procedure Set_NoEvents(Param1: OleVariant); safecall;
    function GetGpsFix(TimeOut: OleVariant; FixType: OleVariant): OleVariant; safecall;
    function GetSatellites(TimeOut: OleVariant): OleVariant; safecall;
    function GetMovement(TimeOut: OleVariant): OleVariant; safecall;
    function GetQuality(TimeOut: OleVariant): OleVariant; safecall;
    function GetComStatus: OleVariant; safecall;
    procedure WriteData(Data: OleVariant); safecall;
    procedure Set_ChecksumMandatory(Param1: OleVariant); safecall;
    procedure EventInterval(Interval: OleVariant; EventType: OleVariant); safecall;
    property AllEventsGeneric: OleVariant write Set_AllEventsGeneric;
    property GpsDatum: OleVariant write Set_GpsDatum;
    property ComPort: OleVariant write Set_ComPort;
    property BaudRate: OleVariant write Set_BaudRate;
    property PortEnabled: OleVariant write Set_PortEnabled;
    property NoEvents: OleVariant write Set_NoEvents;
    property ChecksumMandatory: OleVariant write Set_ChecksumMandatory;
  end;

// *********************************************************************//
// DispIntf:  INmeaParserDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F05800D3-256C-486D-8E24-3483A207C330}
// *********************************************************************//
  INmeaParserDisp = dispinterface
    ['{F05800D3-256C-486D-8E24-3483A207C330}']
    procedure Parse(Sentence: OleVariant); dispid 1;
    property AllEventsGeneric: OleVariant writeonly dispid 2;
    property GpsDatum: OleVariant writeonly dispid 3;
    property ComPort: OleVariant writeonly dispid 4;
    property BaudRate: OleVariant writeonly dispid 5;
    property PortEnabled: OleVariant writeonly dispid 6;
    property NoEvents: OleVariant writeonly dispid 7;
    function GetGpsFix(TimeOut: OleVariant; FixType: OleVariant): OleVariant; dispid 8;
    function GetSatellites(TimeOut: OleVariant): OleVariant; dispid 9;
    function GetMovement(TimeOut: OleVariant): OleVariant; dispid 10;
    function GetQuality(TimeOut: OleVariant): OleVariant; dispid 11;
    function GetComStatus: OleVariant; dispid 12;
    procedure WriteData(Data: OleVariant); dispid 13;
    property ChecksumMandatory: OleVariant writeonly dispid 14;
    procedure EventInterval(Interval: OleVariant; EventType: OleVariant); dispid 15;
  end;

// *********************************************************************//
// Interface: IPosition
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C762FD70-DAED-4A04-8A7F-F3A820CD7513}
// *********************************************************************//
  IPosition = interface(IDispatch)
    ['{C762FD70-DAED-4A04-8A7F-F3A820CD7513}']
    procedure LatitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                          out Seconds: OleVariant; out Hemisphere: OleVariant); safecall;
    procedure LongitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                           out Seconds: OleVariant; out Hemisphere: OleVariant); safecall;
    function LatitudeString(var frm: OleVariant): OleVariant; safecall;
    function LongitudeString(var frm: OleVariant): OleVariant; safecall;
    function Altitude(var frm: OleVariant): OleVariant; safecall;
    function Get_LatitudeRads: OleVariant; safecall;
    function Get_LongitudeRads: OleVariant; safecall;
    procedure Set_LatitudeRads(pVal: OleVariant); safecall;
    procedure Set_LongitudeRads(pVal: OleVariant); safecall;
    procedure SetLatitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                             Hemisphere: OleVariant); safecall;
    procedure SetLongitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                              Hemisphere: OleVariant); safecall;
    procedure SetAltitude(Altitude: OleVariant; frm: OleVariant); safecall;
    function Get_Datum: OleVariant; safecall;
    procedure Set_Datum(pVal: OleVariant); safecall;
    function Get_Grid: OleVariant; safecall;
    procedure Set_Grid(pVal: OleVariant); safecall;
    function Get_Easting: OleVariant; safecall;
    procedure Set_Easting(pVal: OleVariant); safecall;
    function Get_Northing: OleVariant; safecall;
    procedure Set_Northing(pVal: OleVariant); safecall;
    function Get_Zone: OleVariant; safecall;
    procedure Set_Zone(pVal: OleVariant); safecall;
    function Get_Latitude: OleVariant; safecall;
    procedure Set_Latitude(pVal: OleVariant); safecall;
    function Get_Longitude: OleVariant; safecall;
    procedure Set_Longitude(pVal: OleVariant); safecall;
    function Distance(Position2: OleVariant; Method: OleVariant): OleVariant; safecall;
    function Bearing(Position2: OleVariant; Method: OleVariant): OleVariant; safecall;
    procedure Move(Distance: OleVariant; Bearing: OleVariant; Method: OleVariant); safecall;
    function Copy: OleVariant; safecall;
    function Get_ModifiedCount: OleVariant; safecall;
    function IsSameDatum(Position2: OleVariant): OleVariant; safecall;
    function IsSameGrid(Position2: OleVariant): OleVariant; safecall;
    procedure MakeSameDatum(Position2: OleVariant); safecall;
    procedure MakeSameGrid(Position2: OleVariant); safecall;
    procedure _Set_CustomDatum(pVal: OleVariant); safecall;
    function Get_CustomDatum: OleVariant; safecall;
    procedure _Set_CustomGrid(pVal: OleVariant); safecall;
    function Get_CustomGrid: OleVariant; safecall;
    property LatitudeRads: OleVariant read Get_LatitudeRads write Set_LatitudeRads;
    property LongitudeRads: OleVariant read Get_LongitudeRads write Set_LongitudeRads;
    property Datum: OleVariant read Get_Datum write Set_Datum;
    property Grid: OleVariant read Get_Grid write Set_Grid;
    property Easting: OleVariant read Get_Easting write Set_Easting;
    property Northing: OleVariant read Get_Northing write Set_Northing;
    property Zone: OleVariant read Get_Zone write Set_Zone;
    property Latitude: OleVariant read Get_Latitude write Set_Latitude;
    property Longitude: OleVariant read Get_Longitude write Set_Longitude;
    property ModifiedCount: OleVariant read Get_ModifiedCount;
    property CustomDatum: OleVariant read Get_CustomDatum write _Set_CustomDatum;
    property CustomGrid: OleVariant read Get_CustomGrid write _Set_CustomGrid;
  end;

// *********************************************************************//
// DispIntf:  IPositionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C762FD70-DAED-4A04-8A7F-F3A820CD7513}
// *********************************************************************//
  IPositionDisp = dispinterface
    ['{C762FD70-DAED-4A04-8A7F-F3A820CD7513}']
    procedure LatitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                          out Seconds: OleVariant; out Hemisphere: OleVariant); dispid 1;
    procedure LongitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                           out Seconds: OleVariant; out Hemisphere: OleVariant); dispid 2;
    function LatitudeString(var frm: OleVariant): OleVariant; dispid 3;
    function LongitudeString(var frm: OleVariant): OleVariant; dispid 4;
    function Altitude(var frm: OleVariant): OleVariant; dispid 5;
    property LatitudeRads: OleVariant dispid 6;
    property LongitudeRads: OleVariant dispid 7;
    procedure SetLatitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                             Hemisphere: OleVariant); dispid 8;
    procedure SetLongitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                              Hemisphere: OleVariant); dispid 9;
    procedure SetAltitude(Altitude: OleVariant; frm: OleVariant); dispid 10;
    property Datum: OleVariant dispid 11;
    property Grid: OleVariant dispid 12;
    property Easting: OleVariant dispid 13;
    property Northing: OleVariant dispid 14;
    property Zone: OleVariant dispid 15;
    property Latitude: OleVariant dispid 16;
    property Longitude: OleVariant dispid 17;
    function Distance(Position2: OleVariant; Method: OleVariant): OleVariant; dispid 18;
    function Bearing(Position2: OleVariant; Method: OleVariant): OleVariant; dispid 19;
    procedure Move(Distance: OleVariant; Bearing: OleVariant; Method: OleVariant); dispid 20;
    function Copy: OleVariant; dispid 21;
    property ModifiedCount: OleVariant readonly dispid 22;
    function IsSameDatum(Position2: OleVariant): OleVariant; dispid 23;
    function IsSameGrid(Position2: OleVariant): OleVariant; dispid 24;
    procedure MakeSameDatum(Position2: OleVariant); dispid 25;
    procedure MakeSameGrid(Position2: OleVariant); dispid 26;
    property CustomDatum: OleVariant dispid 27;
    property CustomGrid: OleVariant dispid 28;
  end;

// *********************************************************************//
// DispIntf:  _INmeaParserEvents
// Flags:     (4096) Dispatchable
// GUID:      {87D6E08D-4022-4CC2-8903-C9EB12D8DE1B}
// *********************************************************************//
  _INmeaParserEvents = dispinterface
    ['{87D6E08D-4022-4CC2-8903-C9EB12D8DE1B}']
    procedure Generic(var objSentence: OleVariant); dispid 1;
    procedure GGA(var objGGA: OleVariant; var objSentence: OleVariant); dispid 2;
    procedure GLL(var objGLL: OleVariant; var objSentence: OleVariant); dispid 3;
    procedure RMC(var objRMC: OleVariant; var objSentence: OleVariant); dispid 4;
    procedure OnSatellites(var objSatellites: OleVariant); dispid 5;
    procedure OnGpsFix(var objGpsFix: OleVariant); dispid 6;
    procedure OnMovement(var objMovement: OleVariant); dispid 7;
    procedure OnQuality(var objQuality: OleVariant); dispid 8;
    procedure OnComStatus(var objComStatus: OleVariant); dispid 9;
  end;

// *********************************************************************//
// Interface: INmeaSentence
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AFD70F6F-36C7-4319-B160-E0B9FFC10912}
// *********************************************************************//
  INmeaSentence = interface(IDispatch)
    ['{AFD70F6F-36C7-4319-B160-E0B9FFC10912}']
    function Fields(var Index: OleVariant): OleVariant; safecall;
    function Get_FieldsCount: OleVariant; safecall;
    function Get_Command: OleVariant; safecall;
    function Get_Sentence: OleVariant; safecall;
    procedure Set_Command(pVal: OleVariant); safecall;
    procedure Set_Sentence(pVal: OleVariant); safecall;
    procedure SetField(Index: OleVariant; Value: OleVariant); safecall;
    function Get_FullCommand: OleVariant; safecall;
    procedure Set_FullCommand(pVal: OleVariant); safecall;
    property FieldsCount: OleVariant read Get_FieldsCount;
    property Command: OleVariant read Get_Command write Set_Command;
    property Sentence: OleVariant read Get_Sentence write Set_Sentence;
    property FullCommand: OleVariant read Get_FullCommand write Set_FullCommand;
  end;

// *********************************************************************//
// DispIntf:  INmeaSentenceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AFD70F6F-36C7-4319-B160-E0B9FFC10912}
// *********************************************************************//
  INmeaSentenceDisp = dispinterface
    ['{AFD70F6F-36C7-4319-B160-E0B9FFC10912}']
    function Fields(var Index: OleVariant): OleVariant; dispid 1;
    property FieldsCount: OleVariant readonly dispid 2;
    property Command: OleVariant dispid 3;
    property Sentence: OleVariant dispid 4;
    procedure SetField(Index: OleVariant; Value: OleVariant); dispid 5;
    property FullCommand: OleVariant dispid 6;
  end;

// *********************************************************************//
// Interface: INmeaGGA
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3F835160-B6D0-487B-A828-CB0DDD73D340}
// *********************************************************************//
  INmeaGGA = interface(IDispatch)
    ['{3F835160-B6D0-487B-A828-CB0DDD73D340}']
    function Get_Position: OleVariant; safecall;
    function Get_Valid: OleVariant; safecall;
    function Get_UTC: OleVariant; safecall;
    function Get_SatellitesInView: OleVariant; safecall;
    function Get_HDOP: OleVariant; safecall;
    property Position: OleVariant read Get_Position;
    property Valid: OleVariant read Get_Valid;
    property UTC: OleVariant read Get_UTC;
    property SatellitesInView: OleVariant read Get_SatellitesInView;
    property HDOP: OleVariant read Get_HDOP;
  end;

// *********************************************************************//
// DispIntf:  INmeaGGADisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3F835160-B6D0-487B-A828-CB0DDD73D340}
// *********************************************************************//
  INmeaGGADisp = dispinterface
    ['{3F835160-B6D0-487B-A828-CB0DDD73D340}']
    property Position: OleVariant readonly dispid 1;
    property Valid: OleVariant readonly dispid 2;
    property UTC: OleVariant readonly dispid 3;
    property SatellitesInView: OleVariant readonly dispid 4;
    property HDOP: OleVariant readonly dispid 5;
  end;

// *********************************************************************//
// Interface: INmeaGLL
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CF4E432C-CBF3-4A33-9EA0-C3440DF0F594}
// *********************************************************************//
  INmeaGLL = interface(IDispatch)
    ['{CF4E432C-CBF3-4A33-9EA0-C3440DF0F594}']
    function Get_Position: OleVariant; safecall;
    function Get_Valid: OleVariant; safecall;
    function Get_UTC: OleVariant; safecall;
    property Position: OleVariant read Get_Position;
    property Valid: OleVariant read Get_Valid;
    property UTC: OleVariant read Get_UTC;
  end;

// *********************************************************************//
// DispIntf:  INmeaGLLDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CF4E432C-CBF3-4A33-9EA0-C3440DF0F594}
// *********************************************************************//
  INmeaGLLDisp = dispinterface
    ['{CF4E432C-CBF3-4A33-9EA0-C3440DF0F594}']
    property Position: OleVariant readonly dispid 1;
    property Valid: OleVariant readonly dispid 2;
    property UTC: OleVariant readonly dispid 3;
  end;

// *********************************************************************//
// Interface: INmeaRMC
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {53284151-BC91-4A50-AC52-C0B490A11EA2}
// *********************************************************************//
  INmeaRMC = interface(IDispatch)
    ['{53284151-BC91-4A50-AC52-C0B490A11EA2}']
    function GroundSpeed(var frm: OleVariant): OleVariant; safecall;
    function Get_Position: OleVariant; safecall;
    function Get_Valid: OleVariant; safecall;
    function Get_UTC: OleVariant; safecall;
    function Get_Heading: OleVariant; safecall;
    function Get_MagneticVariation: OleVariant; safecall;
    property Position: OleVariant read Get_Position;
    property Valid: OleVariant read Get_Valid;
    property UTC: OleVariant read Get_UTC;
    property Heading: OleVariant read Get_Heading;
    property MagneticVariation: OleVariant read Get_MagneticVariation;
  end;

// *********************************************************************//
// DispIntf:  INmeaRMCDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {53284151-BC91-4A50-AC52-C0B490A11EA2}
// *********************************************************************//
  INmeaRMCDisp = dispinterface
    ['{53284151-BC91-4A50-AC52-C0B490A11EA2}']
    function GroundSpeed(var frm: OleVariant): OleVariant; dispid 1;
    property Position: OleVariant readonly dispid 2;
    property Valid: OleVariant readonly dispid 3;
    property UTC: OleVariant readonly dispid 4;
    property Heading: OleVariant readonly dispid 5;
    property MagneticVariation: OleVariant readonly dispid 6;
  end;

// *********************************************************************//
// Interface: ISatellites
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {517E5ACC-95FB-4BCC-B049-A65AD13C660C}
// *********************************************************************//
  ISatellites = interface(IDispatch)
    ['{517E5ACC-95FB-4BCC-B049-A65AD13C660C}']
    function Get_Count: OleVariant; safecall;
    function Item(var Index: OleVariant): OleVariant; safecall;
    property Count: OleVariant read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  ISatellitesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {517E5ACC-95FB-4BCC-B049-A65AD13C660C}
// *********************************************************************//
  ISatellitesDisp = dispinterface
    ['{517E5ACC-95FB-4BCC-B049-A65AD13C660C}']
    property Count: OleVariant readonly dispid 1;
    function Item(var Index: OleVariant): OleVariant; dispid 0;
  end;

// *********************************************************************//
// Interface: ISatellite
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BDD097E1-9573-4DA2-95E1-1000090B4591}
// *********************************************************************//
  ISatellite = interface(IDispatch)
    ['{BDD097E1-9573-4DA2-95E1-1000090B4591}']
    function Get_ID: OleVariant; safecall;
    function Get_Elevation: OleVariant; safecall;
    function Get_Azimuth: OleVariant; safecall;
    function Get_SNR: OleVariant; safecall;
    function Get_UsedForFix: OleVariant; safecall;
    property ID: OleVariant read Get_ID;
    property Elevation: OleVariant read Get_Elevation;
    property Azimuth: OleVariant read Get_Azimuth;
    property SNR: OleVariant read Get_SNR;
    property UsedForFix: OleVariant read Get_UsedForFix;
  end;

// *********************************************************************//
// DispIntf:  ISatelliteDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BDD097E1-9573-4DA2-95E1-1000090B4591}
// *********************************************************************//
  ISatelliteDisp = dispinterface
    ['{BDD097E1-9573-4DA2-95E1-1000090B4591}']
    property ID: OleVariant readonly dispid 1;
    property Elevation: OleVariant readonly dispid 2;
    property Azimuth: OleVariant readonly dispid 3;
    property SNR: OleVariant readonly dispid 4;
    property UsedForFix: OleVariant readonly dispid 5;
  end;

// *********************************************************************//
// Interface: IMovement
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A03C2B35-8400-466B-B2C4-C34954C1C57D}
// *********************************************************************//
  IMovement = interface(IDispatch)
    ['{A03C2B35-8400-466B-B2C4-C34954C1C57D}']
    function Get_Heading: OleVariant; safecall;
    function Speed(var frm: OleVariant): OleVariant; safecall;
    function Get_MagneticVariation: OleVariant; safecall;
    property Heading: OleVariant read Get_Heading;
    property MagneticVariation: OleVariant read Get_MagneticVariation;
  end;

// *********************************************************************//
// DispIntf:  IMovementDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A03C2B35-8400-466B-B2C4-C34954C1C57D}
// *********************************************************************//
  IMovementDisp = dispinterface
    ['{A03C2B35-8400-466B-B2C4-C34954C1C57D}']
    property Heading: OleVariant readonly dispid 1;
    function Speed(var frm: OleVariant): OleVariant; dispid 2;
    property MagneticVariation: OleVariant readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IQuality
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9D098B93-3799-489B-8E55-40689BCBE07C}
// *********************************************************************//
  IQuality = interface(IDispatch)
    ['{9D098B93-3799-489B-8E55-40689BCBE07C}']
    function Get_PDOP: OleVariant; safecall;
    function Get_HDOP: OleVariant; safecall;
    function Get_VDOP: OleVariant; safecall;
    property PDOP: OleVariant read Get_PDOP;
    property HDOP: OleVariant read Get_HDOP;
    property VDOP: OleVariant read Get_VDOP;
  end;

// *********************************************************************//
// DispIntf:  IQualityDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9D098B93-3799-489B-8E55-40689BCBE07C}
// *********************************************************************//
  IQualityDisp = dispinterface
    ['{9D098B93-3799-489B-8E55-40689BCBE07C}']
    property PDOP: OleVariant readonly dispid 1;
    property HDOP: OleVariant readonly dispid 2;
    property VDOP: OleVariant readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IGpsFix
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6B293970-1DDF-41EB-8CCF-CA2BF0A20728}
// *********************************************************************//
  IGpsFix = interface(IDispatch)
    ['{6B293970-1DDF-41EB-8CCF-CA2BF0A20728}']
    function Get_UTC: OleVariant; safecall;
    function Get_Position: OleVariant; safecall;
    function Get_FixType: OleVariant; safecall;
    property UTC: OleVariant read Get_UTC;
    property Position: OleVariant read Get_Position;
    property FixType: OleVariant read Get_FixType;
  end;

// *********************************************************************//
// DispIntf:  IGpsFixDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6B293970-1DDF-41EB-8CCF-CA2BF0A20728}
// *********************************************************************//
  IGpsFixDisp = dispinterface
    ['{6B293970-1DDF-41EB-8CCF-CA2BF0A20728}']
    property UTC: OleVariant readonly dispid 1;
    property Position: OleVariant readonly dispid 2;
    property FixType: OleVariant readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IComStatus
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F9E467B-EB06-4DE9-9631-EE4C5B2389FB}
// *********************************************************************//
  IComStatus = interface(IDispatch)
    ['{5F9E467B-EB06-4DE9-9631-EE4C5B2389FB}']
    function Get_ValidNmea: OleVariant; safecall;
    function Get_ComPort: OleVariant; safecall;
    function Get_BaudRate: OleVariant; safecall;
    function Get_ValidFix: OleVariant; safecall;
    property ValidNmea: OleVariant read Get_ValidNmea;
    property ComPort: OleVariant read Get_ComPort;
    property BaudRate: OleVariant read Get_BaudRate;
    property ValidFix: OleVariant read Get_ValidFix;
  end;

// *********************************************************************//
// DispIntf:  IComStatusDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F9E467B-EB06-4DE9-9631-EE4C5B2389FB}
// *********************************************************************//
  IComStatusDisp = dispinterface
    ['{5F9E467B-EB06-4DE9-9631-EE4C5B2389FB}']
    property ValidNmea: OleVariant readonly dispid 1;
    property ComPort: OleVariant readonly dispid 2;
    property BaudRate: OleVariant readonly dispid 3;
    property ValidFix: OleVariant readonly dispid 4;
  end;

// *********************************************************************//
// Interface: ICustomDatum
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}
// *********************************************************************//
  ICustomDatum = interface(IDispatch)
    ['{E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}']
    function Get_SemiMajorAxis: OleVariant; safecall;
    procedure Set_SemiMajorAxis(pVal: OleVariant); safecall;
    function Get_E2: OleVariant; safecall;
    procedure Set_E2(pVal: OleVariant); safecall;
    function Get_DeltaX: OleVariant; safecall;
    procedure Set_DeltaX(pVal: OleVariant); safecall;
    function Get_DeltaY: OleVariant; safecall;
    procedure Set_DeltaY(pVal: OleVariant); safecall;
    function Get_DeltaZ: OleVariant; safecall;
    procedure Set_DeltaZ(pVal: OleVariant); safecall;
    function Get_RotX: OleVariant; safecall;
    procedure Set_RotX(pVal: OleVariant); safecall;
    function Get_RotY: OleVariant; safecall;
    procedure Set_RotY(pVal: OleVariant); safecall;
    function Get_RotZ: OleVariant; safecall;
    procedure Set_RotZ(pVal: OleVariant); safecall;
    function Get_ScaleFactor: OleVariant; safecall;
    procedure Set_ScaleFactor(pVal: OleVariant); safecall;
    function Get_InverseFlattering: OleVariant; safecall;
    procedure Set_InverseFlattering(pVal: OleVariant); safecall;
    function Get_Transformable: OleVariant; safecall;
    procedure Set_Transformable(pVal: OleVariant); safecall;
    function Get_Name: OleVariant; safecall;
    procedure Set_Name(pVal: OleVariant); safecall;
    function Compare(CustomDatum: OleVariant): OleVariant; safecall;
    property SemiMajorAxis: OleVariant read Get_SemiMajorAxis write Set_SemiMajorAxis;
    property E2: OleVariant read Get_E2 write Set_E2;
    property DeltaX: OleVariant read Get_DeltaX write Set_DeltaX;
    property DeltaY: OleVariant read Get_DeltaY write Set_DeltaY;
    property DeltaZ: OleVariant read Get_DeltaZ write Set_DeltaZ;
    property RotX: OleVariant read Get_RotX write Set_RotX;
    property RotY: OleVariant read Get_RotY write Set_RotY;
    property RotZ: OleVariant read Get_RotZ write Set_RotZ;
    property ScaleFactor: OleVariant read Get_ScaleFactor write Set_ScaleFactor;
    property InverseFlattering: OleVariant read Get_InverseFlattering write Set_InverseFlattering;
    property Transformable: OleVariant read Get_Transformable write Set_Transformable;
    property Name: OleVariant read Get_Name write Set_Name;
  end;

// *********************************************************************//
// DispIntf:  ICustomDatumDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}
// *********************************************************************//
  ICustomDatumDisp = dispinterface
    ['{E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}']
    property SemiMajorAxis: OleVariant dispid 1;
    property E2: OleVariant dispid 2;
    property DeltaX: OleVariant dispid 3;
    property DeltaY: OleVariant dispid 4;
    property DeltaZ: OleVariant dispid 5;
    property RotX: OleVariant dispid 6;
    property RotY: OleVariant dispid 7;
    property RotZ: OleVariant dispid 8;
    property ScaleFactor: OleVariant dispid 9;
    property InverseFlattering: OleVariant dispid 10;
    property Transformable: OleVariant dispid 14;
    property Name: OleVariant dispid 15;
    function Compare(CustomDatum: OleVariant): OleVariant; dispid 16;
  end;

// *********************************************************************//
// Interface: ICustomGrid
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}
// *********************************************************************//
  ICustomGrid = interface(IDispatch)
    ['{F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}']
    function Get_Datum: OleVariant; safecall;
    procedure Set_Datum(pVal: OleVariant); safecall;
    function Get_CustomDatum: OleVariant; safecall;
    procedure _Set_CustomDatum(pVal: OleVariant); safecall;
    function Get_Algorithm: OleVariant; safecall;
    procedure Set_Algorithm(pVal: OleVariant); safecall;
    function Get_FalseEasting: OleVariant; safecall;
    procedure Set_FalseEasting(pVal: OleVariant); safecall;
    function Get_FalseNorthing: OleVariant; safecall;
    procedure Set_FalseNorthing(pVal: OleVariant); safecall;
    function Get_LongitudeOfOrigin: OleVariant; safecall;
    procedure Set_LongitudeOfOrigin(pVal: OleVariant); safecall;
    function Get_LatitudeOfOrigin: OleVariant; safecall;
    procedure Set_LatitudeOfOrigin(pVal: OleVariant); safecall;
    function Get_ScaleFactor: OleVariant; safecall;
    procedure Set_ScaleFactor(pVal: OleVariant); safecall;
    function Get_LatitudeSP1: OleVariant; safecall;
    procedure Set_LatitudeSP1(pVal: OleVariant); safecall;
    function Get_LatitudeSP2: OleVariant; safecall;
    procedure Set_LatitudeSP2(pVal: OleVariant); safecall;
    function Compare(CustomGrid: OleVariant): OleVariant; safecall;
    property Datum: OleVariant read Get_Datum write Set_Datum;
    property CustomDatum: OleVariant read Get_CustomDatum write _Set_CustomDatum;
    property Algorithm: OleVariant read Get_Algorithm write Set_Algorithm;
    property FalseEasting: OleVariant read Get_FalseEasting write Set_FalseEasting;
    property FalseNorthing: OleVariant read Get_FalseNorthing write Set_FalseNorthing;
    property LongitudeOfOrigin: OleVariant read Get_LongitudeOfOrigin write Set_LongitudeOfOrigin;
    property LatitudeOfOrigin: OleVariant read Get_LatitudeOfOrigin write Set_LatitudeOfOrigin;
    property ScaleFactor: OleVariant read Get_ScaleFactor write Set_ScaleFactor;
    property LatitudeSP1: OleVariant read Get_LatitudeSP1 write Set_LatitudeSP1;
    property LatitudeSP2: OleVariant read Get_LatitudeSP2 write Set_LatitudeSP2;
  end;

// *********************************************************************//
// DispIntf:  ICustomGridDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}
// *********************************************************************//
  ICustomGridDisp = dispinterface
    ['{F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}']
    property Datum: OleVariant dispid 1;
    property CustomDatum: OleVariant dispid 2;
    property Algorithm: OleVariant dispid 3;
    property FalseEasting: OleVariant dispid 4;
    property FalseNorthing: OleVariant dispid 5;
    property LongitudeOfOrigin: OleVariant dispid 6;
    property LatitudeOfOrigin: OleVariant dispid 7;
    property ScaleFactor: OleVariant dispid 8;
    property LatitudeSP1: OleVariant dispid 9;
    property LatitudeSP2: OleVariant dispid 10;
    function Compare(CustomGrid: OleVariant): OleVariant; dispid 11;
  end;

// *********************************************************************//
// Interface: IGpsToolsVersion
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E0EF922-5D91-4A67-9846-77C1D455AE53}
// *********************************************************************//
  IGpsToolsVersion = interface(IDispatch)
    ['{7E0EF922-5D91-4A67-9846-77C1D455AE53}']
    function Get_Major: OleVariant; safecall;
    function Get_Minor: OleVariant; safecall;
    function Get_Build: OleVariant; safecall;
    function Get_Revision: OleVariant; safecall;
    property Major: OleVariant read Get_Major;
    property Minor: OleVariant read Get_Minor;
    property Build: OleVariant read Get_Build;
    property Revision: OleVariant read Get_Revision;
  end;

// *********************************************************************//
// DispIntf:  IGpsToolsVersionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E0EF922-5D91-4A67-9846-77C1D455AE53}
// *********************************************************************//
  IGpsToolsVersionDisp = dispinterface
    ['{7E0EF922-5D91-4A67-9846-77C1D455AE53}']
    property Major: OleVariant readonly dispid 1;
    property Minor: OleVariant readonly dispid 2;
    property Build: OleVariant readonly dispid 3;
    property Revision: OleVariant readonly dispid 4;
  end;

// *********************************************************************//
// The Class CoLicense provides a Create and CreateRemote method to          
// create instances of the default interface ILicense exposed by              
// the CoClass License. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLicense = class
    class function Create: ILicense;
    class function CreateRemote(const MachineName: string): ILicense;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TLicense
// Help String      : License Class
// Default Interface: ILicense
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TLicenseProperties= class;
{$ENDIF}
  TLicense = class(TOleServer)
  private
    FIntf: ILicense;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TLicenseProperties;
    function GetServerProperties: TLicenseProperties;
{$ENDIF}
    function GetDefaultInterface: ILicense;
  protected
    procedure InitServerData; override;
    procedure Set_LicenseKey(Param1: OleVariant);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ILicense);
    procedure Disconnect; override;
    procedure GateLicense(Email: OleVariant; Key: OleVariant);
    property DefaultInterface: ILicense read GetDefaultInterface;
    property LicenseKey: OleVariant write Set_LicenseKey;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TLicenseProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TLicense
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TLicenseProperties = class(TPersistent)
  private
    FServer:    TLicense;
    function    GetDefaultInterface: ILicense;
    constructor Create(AServer: TLicense);
  protected
    procedure Set_LicenseKey(Param1: OleVariant);
  public
    property DefaultInterface: ILicense read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoNmeaParser provides a Create and CreateRemote method to          
// create instances of the default interface INmeaParser exposed by              
// the CoClass NmeaParser. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoNmeaParser = class
    class function Create: INmeaParser;
    class function CreateRemote(const MachineName: string): INmeaParser;
  end;

  TNmeaParserGeneric = procedure(ASender: TObject; var objSentence: OleVariant) of object;
  TNmeaParserGGA = procedure(ASender: TObject; var objGGA: OleVariant; var objSentence: OleVariant) of object;
  TNmeaParserGLL = procedure(ASender: TObject; var objGLL: OleVariant; var objSentence: OleVariant) of object;
  TNmeaParserRMC = procedure(ASender: TObject; var objRMC: OleVariant; var objSentence: OleVariant) of object;
  TNmeaParserOnSatellites = procedure(ASender: TObject; var objSatellites: OleVariant) of object;
  TNmeaParserOnGpsFix = procedure(ASender: TObject; var objGpsFix: OleVariant) of object;
  TNmeaParserOnMovement = procedure(ASender: TObject; var objMovement: OleVariant) of object;
  TNmeaParserOnQuality = procedure(ASender: TObject; var objQuality: OleVariant) of object;
  TNmeaParserOnComStatus = procedure(ASender: TObject; var objComStatus: OleVariant) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TNmeaParser
// Help String      : NmeaParser Class
// Default Interface: INmeaParser
// Def. Intf. DISP? : No
// Event   Interface: _INmeaParserEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TNmeaParserProperties= class;
{$ENDIF}
  TNmeaParser = class(TOleServer)
  private
    FOnGeneric: TNmeaParserGeneric;
    FOnGGA: TNmeaParserGGA;
    FOnGLL: TNmeaParserGLL;
    FOnRMC: TNmeaParserRMC;
    FOnSatellites: TNmeaParserOnSatellites;
    FOnGpsFix: TNmeaParserOnGpsFix;
    FOnMovement: TNmeaParserOnMovement;
    FOnQuality: TNmeaParserOnQuality;
    FOnComStatus: TNmeaParserOnComStatus;
    FIntf: INmeaParser;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TNmeaParserProperties;
    function GetServerProperties: TNmeaParserProperties;
{$ENDIF}
    function GetDefaultInterface: INmeaParser;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    procedure Set_AllEventsGeneric(Param1: OleVariant);
    procedure Set_GpsDatum(Param1: OleVariant);
    procedure Set_ComPort(Param1: OleVariant);
    procedure Set_BaudRate(Param1: OleVariant);
    procedure Set_PortEnabled(Param1: OleVariant);
    procedure Set_NoEvents(Param1: OleVariant);
    procedure Set_ChecksumMandatory(Param1: OleVariant);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: INmeaParser);
    procedure Disconnect; override;
    procedure Parse(Sentence: OleVariant);
    function GetGpsFix(TimeOut: OleVariant; FixType: OleVariant): OleVariant;
    function GetSatellites(TimeOut: OleVariant): OleVariant;
    function GetMovement(TimeOut: OleVariant): OleVariant;
    function GetQuality(TimeOut: OleVariant): OleVariant;
    function GetComStatus: OleVariant;
    procedure WriteData(Data: OleVariant);
    procedure EventInterval(Interval: OleVariant; EventType: OleVariant);
    property DefaultInterface: INmeaParser read GetDefaultInterface;
    property AllEventsGeneric: OleVariant write Set_AllEventsGeneric;
    property GpsDatum: OleVariant write Set_GpsDatum;
    property ComPort: OleVariant write Set_ComPort;
    property BaudRate: OleVariant write Set_BaudRate;
    property PortEnabled: OleVariant write Set_PortEnabled;
    property NoEvents: OleVariant write Set_NoEvents;
    property ChecksumMandatory: OleVariant write Set_ChecksumMandatory;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TNmeaParserProperties read GetServerProperties;
{$ENDIF}
    property OnGeneric: TNmeaParserGeneric read FOnGeneric write FOnGeneric;
    property OnGGA: TNmeaParserGGA read FOnGGA write FOnGGA;
    property OnGLL: TNmeaParserGLL read FOnGLL write FOnGLL;
    property OnRMC: TNmeaParserRMC read FOnRMC write FOnRMC;
    property OnSatellites: TNmeaParserOnSatellites read FOnSatellites write FOnSatellites;
    property OnGpsFix: TNmeaParserOnGpsFix read FOnGpsFix write FOnGpsFix;
    property OnMovement: TNmeaParserOnMovement read FOnMovement write FOnMovement;
    property OnQuality: TNmeaParserOnQuality read FOnQuality write FOnQuality;
    property OnComStatus: TNmeaParserOnComStatus read FOnComStatus write FOnComStatus;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TNmeaParser
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TNmeaParserProperties = class(TPersistent)
  private
    FServer:    TNmeaParser;
    function    GetDefaultInterface: INmeaParser;
    constructor Create(AServer: TNmeaParser);
  protected
    procedure Set_AllEventsGeneric(Param1: OleVariant);
    procedure Set_GpsDatum(Param1: OleVariant);
    procedure Set_ComPort(Param1: OleVariant);
    procedure Set_BaudRate(Param1: OleVariant);
    procedure Set_PortEnabled(Param1: OleVariant);
    procedure Set_NoEvents(Param1: OleVariant);
    procedure Set_ChecksumMandatory(Param1: OleVariant);
  public
    property DefaultInterface: INmeaParser read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoNmeaSentence provides a Create and CreateRemote method to          
// create instances of the default interface INmeaSentence exposed by              
// the CoClass NmeaSentence. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoNmeaSentence = class
    class function Create: INmeaSentence;
    class function CreateRemote(const MachineName: string): INmeaSentence;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TNmeaSentence
// Help String      : NmeaSentence Class
// Default Interface: INmeaSentence
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TNmeaSentenceProperties= class;
{$ENDIF}
  TNmeaSentence = class(TOleServer)
  private
    FIntf: INmeaSentence;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TNmeaSentenceProperties;
    function GetServerProperties: TNmeaSentenceProperties;
{$ENDIF}
    function GetDefaultInterface: INmeaSentence;
  protected
    procedure InitServerData; override;
    function Get_FieldsCount: OleVariant;
    function Get_Command: OleVariant;
    function Get_Sentence: OleVariant;
    procedure Set_Command(pVal: OleVariant);
    procedure Set_Sentence(pVal: OleVariant);
    function Get_FullCommand: OleVariant;
    procedure Set_FullCommand(pVal: OleVariant);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: INmeaSentence);
    procedure Disconnect; override;
    function Fields(var Index: OleVariant): OleVariant;
    procedure SetField(Index: OleVariant; Value: OleVariant);
    property DefaultInterface: INmeaSentence read GetDefaultInterface;
    property FieldsCount: OleVariant read Get_FieldsCount;
    property Command: OleVariant read Get_Command write Set_Command;
    property Sentence: OleVariant read Get_Sentence write Set_Sentence;
    property FullCommand: OleVariant read Get_FullCommand write Set_FullCommand;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TNmeaSentenceProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TNmeaSentence
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TNmeaSentenceProperties = class(TPersistent)
  private
    FServer:    TNmeaSentence;
    function    GetDefaultInterface: INmeaSentence;
    constructor Create(AServer: TNmeaSentence);
  protected
    function Get_FieldsCount: OleVariant;
    function Get_Command: OleVariant;
    function Get_Sentence: OleVariant;
    procedure Set_Command(pVal: OleVariant);
    procedure Set_Sentence(pVal: OleVariant);
    function Get_FullCommand: OleVariant;
    procedure Set_FullCommand(pVal: OleVariant);
  public
    property DefaultInterface: INmeaSentence read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoNmeaGGA provides a Create and CreateRemote method to          
// create instances of the default interface INmeaGGA exposed by              
// the CoClass NmeaGGA. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoNmeaGGA = class
    class function Create: INmeaGGA;
    class function CreateRemote(const MachineName: string): INmeaGGA;
  end;

// *********************************************************************//
// The Class CoNmeaGLL provides a Create and CreateRemote method to          
// create instances of the default interface INmeaGLL exposed by              
// the CoClass NmeaGLL. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoNmeaGLL = class
    class function Create: INmeaGLL;
    class function CreateRemote(const MachineName: string): INmeaGLL;
  end;

// *********************************************************************//
// The Class CoNmeaRMC provides a Create and CreateRemote method to          
// create instances of the default interface INmeaRMC exposed by              
// the CoClass NmeaRMC. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoNmeaRMC = class
    class function Create: INmeaRMC;
    class function CreateRemote(const MachineName: string): INmeaRMC;
  end;

// *********************************************************************//
// The Class CoPosition provides a Create and CreateRemote method to          
// create instances of the default interface IPosition exposed by              
// the CoClass Position. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPosition = class
    class function Create: IPosition;
    class function CreateRemote(const MachineName: string): IPosition;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TPosition
// Help String      : Position Class
// Default Interface: IPosition
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TPositionProperties= class;
{$ENDIF}
  TPosition = class(TOleServer)
  private
    FIntf: IPosition;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TPositionProperties;
    function GetServerProperties: TPositionProperties;
{$ENDIF}
    function GetDefaultInterface: IPosition;
  protected
    procedure InitServerData; override;
    function Get_LatitudeRads: OleVariant;
    function Get_LongitudeRads: OleVariant;
    procedure Set_LatitudeRads(pVal: OleVariant);
    procedure Set_LongitudeRads(pVal: OleVariant);
    function Get_Datum: OleVariant;
    procedure Set_Datum(pVal: OleVariant);
    function Get_Grid: OleVariant;
    procedure Set_Grid(pVal: OleVariant);
    function Get_Easting: OleVariant;
    procedure Set_Easting(pVal: OleVariant);
    function Get_Northing: OleVariant;
    procedure Set_Northing(pVal: OleVariant);
    function Get_Zone: OleVariant;
    procedure Set_Zone(pVal: OleVariant);
    function Get_Latitude: OleVariant;
    procedure Set_Latitude(pVal: OleVariant);
    function Get_Longitude: OleVariant;
    procedure Set_Longitude(pVal: OleVariant);
    function Get_ModifiedCount: OleVariant;
    procedure _Set_CustomDatum(pVal: OleVariant);
    function Get_CustomDatum: OleVariant;
    procedure _Set_CustomGrid(pVal: OleVariant);
    function Get_CustomGrid: OleVariant;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IPosition);
    procedure Disconnect; override;
    procedure LatitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                          out Seconds: OleVariant; out Hemisphere: OleVariant);
    procedure LongitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                           out Seconds: OleVariant; out Hemisphere: OleVariant);
    function LatitudeString(var frm: OleVariant): OleVariant;
    function LongitudeString(var frm: OleVariant): OleVariant;
    function Altitude(var frm: OleVariant): OleVariant;
    procedure SetLatitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                             Hemisphere: OleVariant);
    procedure SetLongitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                              Hemisphere: OleVariant);
    procedure SetAltitude(Altitude: OleVariant; frm: OleVariant);
    function Distance(Position2: OleVariant; Method: OleVariant): OleVariant;
    function Bearing(Position2: OleVariant; Method: OleVariant): OleVariant;
    procedure Move(Distance: OleVariant; Bearing: OleVariant; Method: OleVariant);
    function Copy: OleVariant;
    function IsSameDatum(Position2: OleVariant): OleVariant;
    function IsSameGrid(Position2: OleVariant): OleVariant;
    procedure MakeSameDatum(Position2: OleVariant);
    procedure MakeSameGrid(Position2: OleVariant);
    property DefaultInterface: IPosition read GetDefaultInterface;
    property LatitudeRads: OleVariant read Get_LatitudeRads write Set_LatitudeRads;
    property LongitudeRads: OleVariant read Get_LongitudeRads write Set_LongitudeRads;
    property Datum: OleVariant read Get_Datum write Set_Datum;
    property Grid: OleVariant read Get_Grid write Set_Grid;
    property Easting: OleVariant read Get_Easting write Set_Easting;
    property Northing: OleVariant read Get_Northing write Set_Northing;
    property Zone: OleVariant read Get_Zone write Set_Zone;
    property Latitude: OleVariant read Get_Latitude write Set_Latitude;
    property Longitude: OleVariant read Get_Longitude write Set_Longitude;
    property ModifiedCount: OleVariant read Get_ModifiedCount;
    property CustomDatum: OleVariant read Get_CustomDatum write _Set_CustomDatum;
    property CustomGrid: OleVariant read Get_CustomGrid write _Set_CustomGrid;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TPositionProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TPosition
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TPositionProperties = class(TPersistent)
  private
    FServer:    TPosition;
    function    GetDefaultInterface: IPosition;
    constructor Create(AServer: TPosition);
  protected
    function Get_LatitudeRads: OleVariant;
    function Get_LongitudeRads: OleVariant;
    procedure Set_LatitudeRads(pVal: OleVariant);
    procedure Set_LongitudeRads(pVal: OleVariant);
    function Get_Datum: OleVariant;
    procedure Set_Datum(pVal: OleVariant);
    function Get_Grid: OleVariant;
    procedure Set_Grid(pVal: OleVariant);
    function Get_Easting: OleVariant;
    procedure Set_Easting(pVal: OleVariant);
    function Get_Northing: OleVariant;
    procedure Set_Northing(pVal: OleVariant);
    function Get_Zone: OleVariant;
    procedure Set_Zone(pVal: OleVariant);
    function Get_Latitude: OleVariant;
    procedure Set_Latitude(pVal: OleVariant);
    function Get_Longitude: OleVariant;
    procedure Set_Longitude(pVal: OleVariant);
    function Get_ModifiedCount: OleVariant;
    procedure _Set_CustomDatum(pVal: OleVariant);
    function Get_CustomDatum: OleVariant;
    procedure _Set_CustomGrid(pVal: OleVariant);
    function Get_CustomGrid: OleVariant;
  public
    property DefaultInterface: IPosition read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSatellites provides a Create and CreateRemote method to          
// create instances of the default interface ISatellites exposed by              
// the CoClass Satellites. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSatellites = class
    class function Create: ISatellites;
    class function CreateRemote(const MachineName: string): ISatellites;
  end;

// *********************************************************************//
// The Class CoSatellite provides a Create and CreateRemote method to          
// create instances of the default interface ISatellite exposed by              
// the CoClass Satellite. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSatellite = class
    class function Create: ISatellite;
    class function CreateRemote(const MachineName: string): ISatellite;
  end;

// *********************************************************************//
// The Class CoMovement provides a Create and CreateRemote method to          
// create instances of the default interface IMovement exposed by              
// the CoClass Movement. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMovement = class
    class function Create: IMovement;
    class function CreateRemote(const MachineName: string): IMovement;
  end;

// *********************************************************************//
// The Class CoQuality provides a Create and CreateRemote method to          
// create instances of the default interface IQuality exposed by              
// the CoClass Quality. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoQuality = class
    class function Create: IQuality;
    class function CreateRemote(const MachineName: string): IQuality;
  end;

// *********************************************************************//
// The Class CoGpsFix provides a Create and CreateRemote method to          
// create instances of the default interface IGpsFix exposed by              
// the CoClass GpsFix. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGpsFix = class
    class function Create: IGpsFix;
    class function CreateRemote(const MachineName: string): IGpsFix;
  end;

// *********************************************************************//
// The Class CoComStatus provides a Create and CreateRemote method to          
// create instances of the default interface IComStatus exposed by              
// the CoClass ComStatus. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoComStatus = class
    class function Create: IComStatus;
    class function CreateRemote(const MachineName: string): IComStatus;
  end;

// *********************************************************************//
// The Class CoCustomDatum provides a Create and CreateRemote method to          
// create instances of the default interface ICustomDatum exposed by              
// the CoClass CustomDatum. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCustomDatum = class
    class function Create: ICustomDatum;
    class function CreateRemote(const MachineName: string): ICustomDatum;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCustomDatum
// Help String      : CustomDatum Class
// Default Interface: ICustomDatum
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TCustomDatumProperties= class;
{$ENDIF}
  TCustomDatum = class(TOleServer)
  private
    FIntf: ICustomDatum;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TCustomDatumProperties;
    function GetServerProperties: TCustomDatumProperties;
{$ENDIF}
    function GetDefaultInterface: ICustomDatum;
  protected
    procedure InitServerData; override;
    function Get_SemiMajorAxis: OleVariant;
    procedure Set_SemiMajorAxis(pVal: OleVariant);
    function Get_E2: OleVariant;
    procedure Set_E2(pVal: OleVariant);
    function Get_DeltaX: OleVariant;
    procedure Set_DeltaX(pVal: OleVariant);
    function Get_DeltaY: OleVariant;
    procedure Set_DeltaY(pVal: OleVariant);
    function Get_DeltaZ: OleVariant;
    procedure Set_DeltaZ(pVal: OleVariant);
    function Get_RotX: OleVariant;
    procedure Set_RotX(pVal: OleVariant);
    function Get_RotY: OleVariant;
    procedure Set_RotY(pVal: OleVariant);
    function Get_RotZ: OleVariant;
    procedure Set_RotZ(pVal: OleVariant);
    function Get_ScaleFactor: OleVariant;
    procedure Set_ScaleFactor(pVal: OleVariant);
    function Get_InverseFlattering: OleVariant;
    procedure Set_InverseFlattering(pVal: OleVariant);
    function Get_Transformable: OleVariant;
    procedure Set_Transformable(pVal: OleVariant);
    function Get_Name: OleVariant;
    procedure Set_Name(pVal: OleVariant);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ICustomDatum);
    procedure Disconnect; override;
    function Compare(CustomDatum: OleVariant): OleVariant;
    property DefaultInterface: ICustomDatum read GetDefaultInterface;
    property SemiMajorAxis: OleVariant read Get_SemiMajorAxis write Set_SemiMajorAxis;
    property E2: OleVariant read Get_E2 write Set_E2;
    property DeltaX: OleVariant read Get_DeltaX write Set_DeltaX;
    property DeltaY: OleVariant read Get_DeltaY write Set_DeltaY;
    property DeltaZ: OleVariant read Get_DeltaZ write Set_DeltaZ;
    property RotX: OleVariant read Get_RotX write Set_RotX;
    property RotY: OleVariant read Get_RotY write Set_RotY;
    property RotZ: OleVariant read Get_RotZ write Set_RotZ;
    property ScaleFactor: OleVariant read Get_ScaleFactor write Set_ScaleFactor;
    property InverseFlattering: OleVariant read Get_InverseFlattering write Set_InverseFlattering;
    property Transformable: OleVariant read Get_Transformable write Set_Transformable;
    property Name: OleVariant read Get_Name write Set_Name;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TCustomDatumProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TCustomDatum
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TCustomDatumProperties = class(TPersistent)
  private
    FServer:    TCustomDatum;
    function    GetDefaultInterface: ICustomDatum;
    constructor Create(AServer: TCustomDatum);
  protected
    function Get_SemiMajorAxis: OleVariant;
    procedure Set_SemiMajorAxis(pVal: OleVariant);
    function Get_E2: OleVariant;
    procedure Set_E2(pVal: OleVariant);
    function Get_DeltaX: OleVariant;
    procedure Set_DeltaX(pVal: OleVariant);
    function Get_DeltaY: OleVariant;
    procedure Set_DeltaY(pVal: OleVariant);
    function Get_DeltaZ: OleVariant;
    procedure Set_DeltaZ(pVal: OleVariant);
    function Get_RotX: OleVariant;
    procedure Set_RotX(pVal: OleVariant);
    function Get_RotY: OleVariant;
    procedure Set_RotY(pVal: OleVariant);
    function Get_RotZ: OleVariant;
    procedure Set_RotZ(pVal: OleVariant);
    function Get_ScaleFactor: OleVariant;
    procedure Set_ScaleFactor(pVal: OleVariant);
    function Get_InverseFlattering: OleVariant;
    procedure Set_InverseFlattering(pVal: OleVariant);
    function Get_Transformable: OleVariant;
    procedure Set_Transformable(pVal: OleVariant);
    function Get_Name: OleVariant;
    procedure Set_Name(pVal: OleVariant);
  public
    property DefaultInterface: ICustomDatum read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoCustomGrid provides a Create and CreateRemote method to          
// create instances of the default interface ICustomGrid exposed by              
// the CoClass CustomGrid. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCustomGrid = class
    class function Create: ICustomGrid;
    class function CreateRemote(const MachineName: string): ICustomGrid;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCustomGrid
// Help String      : CustomGrid Class
// Default Interface: ICustomGrid
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TCustomGridProperties= class;
{$ENDIF}
  TCustomGrid = class(TOleServer)
  private
    FIntf: ICustomGrid;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TCustomGridProperties;
    function GetServerProperties: TCustomGridProperties;
{$ENDIF}
    function GetDefaultInterface: ICustomGrid;
  protected
    procedure InitServerData; override;
    function Get_Datum: OleVariant;
    procedure Set_Datum(pVal: OleVariant);
    function Get_CustomDatum: OleVariant;
    procedure _Set_CustomDatum(pVal: OleVariant);
    function Get_Algorithm: OleVariant;
    procedure Set_Algorithm(pVal: OleVariant);
    function Get_FalseEasting: OleVariant;
    procedure Set_FalseEasting(pVal: OleVariant);
    function Get_FalseNorthing: OleVariant;
    procedure Set_FalseNorthing(pVal: OleVariant);
    function Get_LongitudeOfOrigin: OleVariant;
    procedure Set_LongitudeOfOrigin(pVal: OleVariant);
    function Get_LatitudeOfOrigin: OleVariant;
    procedure Set_LatitudeOfOrigin(pVal: OleVariant);
    function Get_ScaleFactor: OleVariant;
    procedure Set_ScaleFactor(pVal: OleVariant);
    function Get_LatitudeSP1: OleVariant;
    procedure Set_LatitudeSP1(pVal: OleVariant);
    function Get_LatitudeSP2: OleVariant;
    procedure Set_LatitudeSP2(pVal: OleVariant);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ICustomGrid);
    procedure Disconnect; override;
    function Compare(CustomGrid: OleVariant): OleVariant;
    property DefaultInterface: ICustomGrid read GetDefaultInterface;
    property Datum: OleVariant read Get_Datum write Set_Datum;
    property CustomDatum: OleVariant read Get_CustomDatum write _Set_CustomDatum;
    property Algorithm: OleVariant read Get_Algorithm write Set_Algorithm;
    property FalseEasting: OleVariant read Get_FalseEasting write Set_FalseEasting;
    property FalseNorthing: OleVariant read Get_FalseNorthing write Set_FalseNorthing;
    property LongitudeOfOrigin: OleVariant read Get_LongitudeOfOrigin write Set_LongitudeOfOrigin;
    property LatitudeOfOrigin: OleVariant read Get_LatitudeOfOrigin write Set_LatitudeOfOrigin;
    property ScaleFactor: OleVariant read Get_ScaleFactor write Set_ScaleFactor;
    property LatitudeSP1: OleVariant read Get_LatitudeSP1 write Set_LatitudeSP1;
    property LatitudeSP2: OleVariant read Get_LatitudeSP2 write Set_LatitudeSP2;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TCustomGridProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TCustomGrid
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TCustomGridProperties = class(TPersistent)
  private
    FServer:    TCustomGrid;
    function    GetDefaultInterface: ICustomGrid;
    constructor Create(AServer: TCustomGrid);
  protected
    function Get_Datum: OleVariant;
    procedure Set_Datum(pVal: OleVariant);
    function Get_CustomDatum: OleVariant;
    procedure _Set_CustomDatum(pVal: OleVariant);
    function Get_Algorithm: OleVariant;
    procedure Set_Algorithm(pVal: OleVariant);
    function Get_FalseEasting: OleVariant;
    procedure Set_FalseEasting(pVal: OleVariant);
    function Get_FalseNorthing: OleVariant;
    procedure Set_FalseNorthing(pVal: OleVariant);
    function Get_LongitudeOfOrigin: OleVariant;
    procedure Set_LongitudeOfOrigin(pVal: OleVariant);
    function Get_LatitudeOfOrigin: OleVariant;
    procedure Set_LatitudeOfOrigin(pVal: OleVariant);
    function Get_ScaleFactor: OleVariant;
    procedure Set_ScaleFactor(pVal: OleVariant);
    function Get_LatitudeSP1: OleVariant;
    procedure Set_LatitudeSP1(pVal: OleVariant);
    function Get_LatitudeSP2: OleVariant;
    procedure Set_LatitudeSP2(pVal: OleVariant);
  public
    property DefaultInterface: ICustomGrid read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoGpsToolsVersion provides a Create and CreateRemote method to          
// create instances of the default interface IGpsToolsVersion exposed by              
// the CoClass GpsToolsVersion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGpsToolsVersion = class
    class function Create: IGpsToolsVersion;
    class function CreateRemote(const MachineName: string): IGpsToolsVersion;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TGpsToolsVersion
// Help String      : GpsToolsVersion Class
// Default Interface: IGpsToolsVersion
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TGpsToolsVersionProperties= class;
{$ENDIF}
  TGpsToolsVersion = class(TOleServer)
  private
    FIntf: IGpsToolsVersion;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TGpsToolsVersionProperties;
    function GetServerProperties: TGpsToolsVersionProperties;
{$ENDIF}
    function GetDefaultInterface: IGpsToolsVersion;
  protected
    procedure InitServerData; override;
    function Get_Major: OleVariant;
    function Get_Minor: OleVariant;
    function Get_Build: OleVariant;
    function Get_Revision: OleVariant;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IGpsToolsVersion);
    procedure Disconnect; override;
    property DefaultInterface: IGpsToolsVersion read GetDefaultInterface;
    property Major: OleVariant read Get_Major;
    property Minor: OleVariant read Get_Minor;
    property Build: OleVariant read Get_Build;
    property Revision: OleVariant read Get_Revision;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TGpsToolsVersionProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TGpsToolsVersion
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TGpsToolsVersionProperties = class(TPersistent)
  private
    FServer:    TGpsToolsVersion;
    function    GetDefaultInterface: IGpsToolsVersion;
    constructor Create(AServer: TGpsToolsVersion);
  protected
    function Get_Major: OleVariant;
    function Get_Minor: OleVariant;
    function Get_Build: OleVariant;
    function Get_Revision: OleVariant;
  public
    property DefaultInterface: IGpsToolsVersion read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = '(none)';

  dtlOcxPage = '(none)';

implementation

uses ComObj;

class function CoLicense.Create: ILicense;
begin
  Result := CreateComObject(CLASS_License) as ILicense;
end;

class function CoLicense.CreateRemote(const MachineName: string): ILicense;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_License) as ILicense;
end;

procedure TLicense.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{DB3C0ABE-C362-42C7-8FF8-52FD5EDE02E7}';
    IntfIID:   '{772AA3F6-A49A-4472-964D-308DA64CA0F2}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TLicense.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    // If you get an EOleSysError here, run "regsvr32 GpsToolsXP230.dll" in the lib\ dir as an administrator
    punk := GetServer;
    Fintf:= punk as ILicense;
  end;
end;

procedure TLicense.ConnectTo(svrIntf: ILicense);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TLicense.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TLicense.GetDefaultInterface: ILicense;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TLicense.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TLicenseProperties.Create(Self);
{$ENDIF}
end;

destructor TLicense.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TLicense.GetServerProperties: TLicenseProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TLicense.Set_LicenseKey(Param1: OleVariant);
begin
  DefaultInterface.Set_LicenseKey(Param1);
end;

procedure TLicense.GateLicense(Email: OleVariant; Key: OleVariant);
begin
  DefaultInterface.GateLicense(Email, Key);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TLicenseProperties.Create(AServer: TLicense);
begin
  inherited Create;
  FServer := AServer;
end;

function TLicenseProperties.GetDefaultInterface: ILicense;
begin
  Result := FServer.DefaultInterface;
end;

procedure TLicenseProperties.Set_LicenseKey(Param1: OleVariant);
begin
  DefaultInterface.Set_LicenseKey(Param1);
end;

{$ENDIF}

class function CoNmeaParser.Create: INmeaParser;
begin
  Result := CreateComObject(CLASS_NmeaParser) as INmeaParser;
end;

class function CoNmeaParser.CreateRemote(const MachineName: string): INmeaParser;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_NmeaParser) as INmeaParser;
end;

procedure TNmeaParser.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{BC32D60A-D170-4282-A0CA-FAAE7EF5A53E}';
    IntfIID:   '{F05800D3-256C-486D-8E24-3483A207C330}';
    EventIID:  '{87D6E08D-4022-4CC2-8903-C9EB12D8DE1B}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TNmeaParser.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as INmeaParser;
  end;
end;

procedure TNmeaParser.ConnectTo(svrIntf: INmeaParser);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TNmeaParser.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TNmeaParser.GetDefaultInterface: INmeaParser;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TNmeaParser.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TNmeaParserProperties.Create(Self);
{$ENDIF}
end;

destructor TNmeaParser.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TNmeaParser.GetServerProperties: TNmeaParserProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TNmeaParser.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    1: if Assigned(FOnGeneric) then
         FOnGeneric(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
    2: if Assigned(FOnGGA) then
         FOnGGA(Self,
                OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant},
                OleVariant((TVarData(Params[1]).VPointer)^) {var OleVariant});
    3: if Assigned(FOnGLL) then
         FOnGLL(Self,
                OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant},
                OleVariant((TVarData(Params[1]).VPointer)^) {var OleVariant});
    4: if Assigned(FOnRMC) then
         FOnRMC(Self,
                OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant},
                OleVariant((TVarData(Params[1]).VPointer)^) {var OleVariant});
    5: if Assigned(FOnSatellites) then
         FOnSatellites(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
    6: if Assigned(FOnGpsFix) then
         FOnGpsFix(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
    7: if Assigned(FOnMovement) then
         FOnMovement(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
    8: if Assigned(FOnQuality) then
         FOnQuality(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
    9: if Assigned(FOnComStatus) then
         FOnComStatus(Self, OleVariant((TVarData(Params[0]).VPointer)^) {var OleVariant});
  end; {case DispID}
end;

procedure TNmeaParser.Set_AllEventsGeneric(Param1: OleVariant);
begin
  DefaultInterface.Set_AllEventsGeneric(Param1);
end;

procedure TNmeaParser.Set_GpsDatum(Param1: OleVariant);
begin
  DefaultInterface.Set_GpsDatum(Param1);
end;

procedure TNmeaParser.Set_ComPort(Param1: OleVariant);
begin
  DefaultInterface.Set_ComPort(Param1);
end;

procedure TNmeaParser.Set_BaudRate(Param1: OleVariant);
begin
  DefaultInterface.Set_BaudRate(Param1);
end;

procedure TNmeaParser.Set_PortEnabled(Param1: OleVariant);
begin
  DefaultInterface.Set_PortEnabled(Param1);
end;

procedure TNmeaParser.Set_NoEvents(Param1: OleVariant);
begin
  DefaultInterface.Set_NoEvents(Param1);
end;

procedure TNmeaParser.Set_ChecksumMandatory(Param1: OleVariant);
begin
  DefaultInterface.Set_ChecksumMandatory(Param1);
end;

procedure TNmeaParser.Parse(Sentence: OleVariant);
begin
  DefaultInterface.Parse(Sentence);
end;

function TNmeaParser.GetGpsFix(TimeOut: OleVariant; FixType: OleVariant): OleVariant;
begin
  Result := DefaultInterface.GetGpsFix(TimeOut, FixType);
end;

function TNmeaParser.GetSatellites(TimeOut: OleVariant): OleVariant;
begin
  Result := DefaultInterface.GetSatellites(TimeOut);
end;

function TNmeaParser.GetMovement(TimeOut: OleVariant): OleVariant;
begin
  Result := DefaultInterface.GetMovement(TimeOut);
end;

function TNmeaParser.GetQuality(TimeOut: OleVariant): OleVariant;
begin
  Result := DefaultInterface.GetQuality(TimeOut);
end;

function TNmeaParser.GetComStatus: OleVariant;
begin
  Result := DefaultInterface.GetComStatus;
end;

procedure TNmeaParser.WriteData(Data: OleVariant);
begin
  DefaultInterface.WriteData(Data);
end;

procedure TNmeaParser.EventInterval(Interval: OleVariant; EventType: OleVariant);
begin
  DefaultInterface.EventInterval(Interval, EventType);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TNmeaParserProperties.Create(AServer: TNmeaParser);
begin
  inherited Create;
  FServer := AServer;
end;

function TNmeaParserProperties.GetDefaultInterface: INmeaParser;
begin
  Result := FServer.DefaultInterface;
end;

procedure TNmeaParserProperties.Set_AllEventsGeneric(Param1: OleVariant);
begin
  DefaultInterface.Set_AllEventsGeneric(Param1);
end;

procedure TNmeaParserProperties.Set_GpsDatum(Param1: OleVariant);
begin
  DefaultInterface.Set_GpsDatum(Param1);
end;

procedure TNmeaParserProperties.Set_ComPort(Param1: OleVariant);
begin
  DefaultInterface.Set_ComPort(Param1);
end;

procedure TNmeaParserProperties.Set_BaudRate(Param1: OleVariant);
begin
  DefaultInterface.Set_BaudRate(Param1);
end;

procedure TNmeaParserProperties.Set_PortEnabled(Param1: OleVariant);
begin
  DefaultInterface.Set_PortEnabled(Param1);
end;

procedure TNmeaParserProperties.Set_NoEvents(Param1: OleVariant);
begin
  DefaultInterface.Set_NoEvents(Param1);
end;

procedure TNmeaParserProperties.Set_ChecksumMandatory(Param1: OleVariant);
begin
  DefaultInterface.Set_ChecksumMandatory(Param1);
end;

{$ENDIF}

class function CoNmeaSentence.Create: INmeaSentence;
begin
  Result := CreateComObject(CLASS_NmeaSentence) as INmeaSentence;
end;

class function CoNmeaSentence.CreateRemote(const MachineName: string): INmeaSentence;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_NmeaSentence) as INmeaSentence;
end;

procedure TNmeaSentence.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{D1B811D9-9A7F-439C-876F-D20AA8BDE7DB}';
    IntfIID:   '{AFD70F6F-36C7-4319-B160-E0B9FFC10912}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TNmeaSentence.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as INmeaSentence;
  end;
end;

procedure TNmeaSentence.ConnectTo(svrIntf: INmeaSentence);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TNmeaSentence.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TNmeaSentence.GetDefaultInterface: INmeaSentence;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TNmeaSentence.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TNmeaSentenceProperties.Create(Self);
{$ENDIF}
end;

destructor TNmeaSentence.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TNmeaSentence.GetServerProperties: TNmeaSentenceProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TNmeaSentence.Get_FieldsCount: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FieldsCount;
end;

function TNmeaSentence.Get_Command: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Command;
end;

function TNmeaSentence.Get_Sentence: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Sentence;
end;

procedure TNmeaSentence.Set_Command(pVal: OleVariant);
begin
  DefaultInterface.Set_Command(pVal);
end;

procedure TNmeaSentence.Set_Sentence(pVal: OleVariant);
begin
  DefaultInterface.Set_Sentence(pVal);
end;

function TNmeaSentence.Get_FullCommand: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FullCommand;
end;

procedure TNmeaSentence.Set_FullCommand(pVal: OleVariant);
begin
  DefaultInterface.Set_FullCommand(pVal);
end;

function TNmeaSentence.Fields(var Index: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Fields(Index);
end;

procedure TNmeaSentence.SetField(Index: OleVariant; Value: OleVariant);
begin
  DefaultInterface.SetField(Index, Value);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TNmeaSentenceProperties.Create(AServer: TNmeaSentence);
begin
  inherited Create;
  FServer := AServer;
end;

function TNmeaSentenceProperties.GetDefaultInterface: INmeaSentence;
begin
  Result := FServer.DefaultInterface;
end;

function TNmeaSentenceProperties.Get_FieldsCount: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FieldsCount;
end;

function TNmeaSentenceProperties.Get_Command: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Command;
end;

function TNmeaSentenceProperties.Get_Sentence: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Sentence;
end;

procedure TNmeaSentenceProperties.Set_Command(pVal: OleVariant);
begin
  DefaultInterface.Set_Command(pVal);
end;

procedure TNmeaSentenceProperties.Set_Sentence(pVal: OleVariant);
begin
  DefaultInterface.Set_Sentence(pVal);
end;

function TNmeaSentenceProperties.Get_FullCommand: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FullCommand;
end;

procedure TNmeaSentenceProperties.Set_FullCommand(pVal: OleVariant);
begin
  DefaultInterface.Set_FullCommand(pVal);
end;

{$ENDIF}

class function CoNmeaGGA.Create: INmeaGGA;
begin
  Result := CreateComObject(CLASS_NmeaGGA) as INmeaGGA;
end;

class function CoNmeaGGA.CreateRemote(const MachineName: string): INmeaGGA;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_NmeaGGA) as INmeaGGA;
end;

class function CoNmeaGLL.Create: INmeaGLL;
begin
  Result := CreateComObject(CLASS_NmeaGLL) as INmeaGLL;
end;

class function CoNmeaGLL.CreateRemote(const MachineName: string): INmeaGLL;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_NmeaGLL) as INmeaGLL;
end;

class function CoNmeaRMC.Create: INmeaRMC;
begin
  Result := CreateComObject(CLASS_NmeaRMC) as INmeaRMC;
end;

class function CoNmeaRMC.CreateRemote(const MachineName: string): INmeaRMC;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_NmeaRMC) as INmeaRMC;
end;

class function CoPosition.Create: IPosition;
begin
  Result := CreateComObject(CLASS_Position) as IPosition;
end;

class function CoPosition.CreateRemote(const MachineName: string): IPosition;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Position) as IPosition;
end;

procedure TPosition.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{161514DA-DBDF-46D1-A0D4-E272AFBFB22B}';
    IntfIID:   '{C762FD70-DAED-4A04-8A7F-F3A820CD7513}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TPosition.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IPosition;
  end;
end;

procedure TPosition.ConnectTo(svrIntf: IPosition);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TPosition.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TPosition.GetDefaultInterface: IPosition;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TPosition.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TPositionProperties.Create(Self);
{$ENDIF}
end;

destructor TPosition.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TPosition.GetServerProperties: TPositionProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TPosition.Get_LatitudeRads: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeRads;
end;

function TPosition.Get_LongitudeRads: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LongitudeRads;
end;

procedure TPosition.Set_LatitudeRads(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeRads(pVal);
end;

procedure TPosition.Set_LongitudeRads(pVal: OleVariant);
begin
  DefaultInterface.Set_LongitudeRads(pVal);
end;

function TPosition.Get_Datum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Datum;
end;

procedure TPosition.Set_Datum(pVal: OleVariant);
begin
  DefaultInterface.Set_Datum(pVal);
end;

function TPosition.Get_Grid: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Grid;
end;

procedure TPosition.Set_Grid(pVal: OleVariant);
begin
  DefaultInterface.Set_Grid(pVal);
end;

function TPosition.Get_Easting: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Easting;
end;

procedure TPosition.Set_Easting(pVal: OleVariant);
begin
  DefaultInterface.Set_Easting(pVal);
end;

function TPosition.Get_Northing: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Northing;
end;

procedure TPosition.Set_Northing(pVal: OleVariant);
begin
  DefaultInterface.Set_Northing(pVal);
end;

function TPosition.Get_Zone: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Zone;
end;

procedure TPosition.Set_Zone(pVal: OleVariant);
begin
  DefaultInterface.Set_Zone(pVal);
end;

function TPosition.Get_Latitude: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Latitude;
end;

procedure TPosition.Set_Latitude(pVal: OleVariant);
begin
  DefaultInterface.Set_Latitude(pVal);
end;

function TPosition.Get_Longitude: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Longitude;
end;

procedure TPosition.Set_Longitude(pVal: OleVariant);
begin
  DefaultInterface.Set_Longitude(pVal);
end;

function TPosition.Get_ModifiedCount: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ModifiedCount;
end;

procedure TPosition._Set_CustomDatum(pVal: OleVariant);
  { Warning: The property CustomDatum has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomDatum := pVal;
end;

function TPosition.Get_CustomDatum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomDatum;
end;

procedure TPosition._Set_CustomGrid(pVal: OleVariant);
  { Warning: The property CustomGrid has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomGrid := pVal;
end;

function TPosition.Get_CustomGrid: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomGrid;
end;

procedure TPosition.LatitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                                out Seconds: OleVariant; out Hemisphere: OleVariant);
begin
  DefaultInterface.LatitudeDMS(Degrees, Minutes, Seconds, Hemisphere);
end;

procedure TPosition.LongitudeDMS(out Degrees: OleVariant; out Minutes: OleVariant; 
                                 out Seconds: OleVariant; out Hemisphere: OleVariant);
begin
  DefaultInterface.LongitudeDMS(Degrees, Minutes, Seconds, Hemisphere);
end;

function TPosition.LatitudeString(var frm: OleVariant): OleVariant;
begin
  Result := DefaultInterface.LatitudeString(frm);
end;

function TPosition.LongitudeString(var frm: OleVariant): OleVariant;
begin
  Result := DefaultInterface.LongitudeString(frm);
end;

function TPosition.Altitude(var frm: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Altitude(frm);
end;

procedure TPosition.SetLatitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                                   Hemisphere: OleVariant);
begin
  DefaultInterface.SetLatitudeDMS(Degrees, Minutes, Seconds, Hemisphere);
end;

procedure TPosition.SetLongitudeDMS(Degrees: OleVariant; Minutes: OleVariant; Seconds: OleVariant; 
                                    Hemisphere: OleVariant);
begin
  DefaultInterface.SetLongitudeDMS(Degrees, Minutes, Seconds, Hemisphere);
end;

procedure TPosition.SetAltitude(Altitude: OleVariant; frm: OleVariant);
begin
  DefaultInterface.SetAltitude(Altitude, frm);
end;

function TPosition.Distance(Position2: OleVariant; Method: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Distance(Position2, Method);
end;

function TPosition.Bearing(Position2: OleVariant; Method: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Bearing(Position2, Method);
end;

procedure TPosition.Move(Distance: OleVariant; Bearing: OleVariant; Method: OleVariant);
begin
  DefaultInterface.Move(Distance, Bearing, Method);
end;

function TPosition.Copy: OleVariant;
begin
  Result := DefaultInterface.Copy;
end;

function TPosition.IsSameDatum(Position2: OleVariant): OleVariant;
begin
  Result := DefaultInterface.IsSameDatum(Position2);
end;

function TPosition.IsSameGrid(Position2: OleVariant): OleVariant;
begin
  Result := DefaultInterface.IsSameGrid(Position2);
end;

procedure TPosition.MakeSameDatum(Position2: OleVariant);
begin
  DefaultInterface.MakeSameDatum(Position2);
end;

procedure TPosition.MakeSameGrid(Position2: OleVariant);
begin
  DefaultInterface.MakeSameGrid(Position2);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TPositionProperties.Create(AServer: TPosition);
begin
  inherited Create;
  FServer := AServer;
end;

function TPositionProperties.GetDefaultInterface: IPosition;
begin
  Result := FServer.DefaultInterface;
end;

function TPositionProperties.Get_LatitudeRads: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeRads;
end;

function TPositionProperties.Get_LongitudeRads: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LongitudeRads;
end;

procedure TPositionProperties.Set_LatitudeRads(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeRads(pVal);
end;

procedure TPositionProperties.Set_LongitudeRads(pVal: OleVariant);
begin
  DefaultInterface.Set_LongitudeRads(pVal);
end;

function TPositionProperties.Get_Datum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Datum;
end;

procedure TPositionProperties.Set_Datum(pVal: OleVariant);
begin
  DefaultInterface.Set_Datum(pVal);
end;

function TPositionProperties.Get_Grid: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Grid;
end;

procedure TPositionProperties.Set_Grid(pVal: OleVariant);
begin
  DefaultInterface.Set_Grid(pVal);
end;

function TPositionProperties.Get_Easting: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Easting;
end;

procedure TPositionProperties.Set_Easting(pVal: OleVariant);
begin
  DefaultInterface.Set_Easting(pVal);
end;

function TPositionProperties.Get_Northing: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Northing;
end;

procedure TPositionProperties.Set_Northing(pVal: OleVariant);
begin
  DefaultInterface.Set_Northing(pVal);
end;

function TPositionProperties.Get_Zone: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Zone;
end;

procedure TPositionProperties.Set_Zone(pVal: OleVariant);
begin
  DefaultInterface.Set_Zone(pVal);
end;

function TPositionProperties.Get_Latitude: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Latitude;
end;

procedure TPositionProperties.Set_Latitude(pVal: OleVariant);
begin
  DefaultInterface.Set_Latitude(pVal);
end;

function TPositionProperties.Get_Longitude: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Longitude;
end;

procedure TPositionProperties.Set_Longitude(pVal: OleVariant);
begin
  DefaultInterface.Set_Longitude(pVal);
end;

function TPositionProperties.Get_ModifiedCount: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ModifiedCount;
end;

procedure TPositionProperties._Set_CustomDatum(pVal: OleVariant);
  { Warning: The property CustomDatum has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomDatum := pVal;
end;

function TPositionProperties.Get_CustomDatum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomDatum;
end;

procedure TPositionProperties._Set_CustomGrid(pVal: OleVariant);
  { Warning: The property CustomGrid has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomGrid := pVal;
end;

function TPositionProperties.Get_CustomGrid: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomGrid;
end;

{$ENDIF}

class function CoSatellites.Create: ISatellites;
begin
  Result := CreateComObject(CLASS_Satellites) as ISatellites;
end;

class function CoSatellites.CreateRemote(const MachineName: string): ISatellites;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Satellites) as ISatellites;
end;

class function CoSatellite.Create: ISatellite;
begin
  Result := CreateComObject(CLASS_Satellite) as ISatellite;
end;

class function CoSatellite.CreateRemote(const MachineName: string): ISatellite;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Satellite) as ISatellite;
end;

class function CoMovement.Create: IMovement;
begin
  Result := CreateComObject(CLASS_Movement) as IMovement;
end;

class function CoMovement.CreateRemote(const MachineName: string): IMovement;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Movement) as IMovement;
end;

class function CoQuality.Create: IQuality;
begin
  Result := CreateComObject(CLASS_Quality) as IQuality;
end;

class function CoQuality.CreateRemote(const MachineName: string): IQuality;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Quality) as IQuality;
end;

class function CoGpsFix.Create: IGpsFix;
begin
  Result := CreateComObject(CLASS_GpsFix) as IGpsFix;
end;

class function CoGpsFix.CreateRemote(const MachineName: string): IGpsFix;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_GpsFix) as IGpsFix;
end;

class function CoComStatus.Create: IComStatus;
begin
  Result := CreateComObject(CLASS_ComStatus) as IComStatus;
end;

class function CoComStatus.CreateRemote(const MachineName: string): IComStatus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ComStatus) as IComStatus;
end;

class function CoCustomDatum.Create: ICustomDatum;
begin
  Result := CreateComObject(CLASS_CustomDatum) as ICustomDatum;
end;

class function CoCustomDatum.CreateRemote(const MachineName: string): ICustomDatum;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CustomDatum) as ICustomDatum;
end;

procedure TCustomDatum.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E6110D33-AD89-4384-996D-5F98EB199EAB}';
    IntfIID:   '{E598C416-D3BB-4320-AD6D-B0BF63E2AE4F}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCustomDatum.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ICustomDatum;
  end;
end;

procedure TCustomDatum.ConnectTo(svrIntf: ICustomDatum);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCustomDatum.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCustomDatum.GetDefaultInterface: ICustomDatum;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TCustomDatum.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TCustomDatumProperties.Create(Self);
{$ENDIF}
end;

destructor TCustomDatum.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TCustomDatum.GetServerProperties: TCustomDatumProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TCustomDatum.Get_SemiMajorAxis: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.SemiMajorAxis;
end;

procedure TCustomDatum.Set_SemiMajorAxis(pVal: OleVariant);
begin
  DefaultInterface.Set_SemiMajorAxis(pVal);
end;

function TCustomDatum.Get_E2: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.E2;
end;

procedure TCustomDatum.Set_E2(pVal: OleVariant);
begin
  DefaultInterface.Set_E2(pVal);
end;

function TCustomDatum.Get_DeltaX: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaX;
end;

procedure TCustomDatum.Set_DeltaX(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaX(pVal);
end;

function TCustomDatum.Get_DeltaY: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaY;
end;

procedure TCustomDatum.Set_DeltaY(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaY(pVal);
end;

function TCustomDatum.Get_DeltaZ: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaZ;
end;

procedure TCustomDatum.Set_DeltaZ(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaZ(pVal);
end;

function TCustomDatum.Get_RotX: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotX;
end;

procedure TCustomDatum.Set_RotX(pVal: OleVariant);
begin
  DefaultInterface.Set_RotX(pVal);
end;

function TCustomDatum.Get_RotY: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotY;
end;

procedure TCustomDatum.Set_RotY(pVal: OleVariant);
begin
  DefaultInterface.Set_RotY(pVal);
end;

function TCustomDatum.Get_RotZ: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotZ;
end;

procedure TCustomDatum.Set_RotZ(pVal: OleVariant);
begin
  DefaultInterface.Set_RotZ(pVal);
end;

function TCustomDatum.Get_ScaleFactor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ScaleFactor;
end;

procedure TCustomDatum.Set_ScaleFactor(pVal: OleVariant);
begin
  DefaultInterface.Set_ScaleFactor(pVal);
end;

function TCustomDatum.Get_InverseFlattering: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.InverseFlattering;
end;

procedure TCustomDatum.Set_InverseFlattering(pVal: OleVariant);
begin
  DefaultInterface.Set_InverseFlattering(pVal);
end;

function TCustomDatum.Get_Transformable: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Transformable;
end;

procedure TCustomDatum.Set_Transformable(pVal: OleVariant);
begin
  DefaultInterface.Set_Transformable(pVal);
end;

function TCustomDatum.Get_Name: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Name;
end;

procedure TCustomDatum.Set_Name(pVal: OleVariant);
begin
  DefaultInterface.Set_Name(pVal);
end;

function TCustomDatum.Compare(CustomDatum: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Compare(CustomDatum);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TCustomDatumProperties.Create(AServer: TCustomDatum);
begin
  inherited Create;
  FServer := AServer;
end;

function TCustomDatumProperties.GetDefaultInterface: ICustomDatum;
begin
  Result := FServer.DefaultInterface;
end;

function TCustomDatumProperties.Get_SemiMajorAxis: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.SemiMajorAxis;
end;

procedure TCustomDatumProperties.Set_SemiMajorAxis(pVal: OleVariant);
begin
  DefaultInterface.Set_SemiMajorAxis(pVal);
end;

function TCustomDatumProperties.Get_E2: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.E2;
end;

procedure TCustomDatumProperties.Set_E2(pVal: OleVariant);
begin
  DefaultInterface.Set_E2(pVal);
end;

function TCustomDatumProperties.Get_DeltaX: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaX;
end;

procedure TCustomDatumProperties.Set_DeltaX(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaX(pVal);
end;

function TCustomDatumProperties.Get_DeltaY: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaY;
end;

procedure TCustomDatumProperties.Set_DeltaY(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaY(pVal);
end;

function TCustomDatumProperties.Get_DeltaZ: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DeltaZ;
end;

procedure TCustomDatumProperties.Set_DeltaZ(pVal: OleVariant);
begin
  DefaultInterface.Set_DeltaZ(pVal);
end;

function TCustomDatumProperties.Get_RotX: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotX;
end;

procedure TCustomDatumProperties.Set_RotX(pVal: OleVariant);
begin
  DefaultInterface.Set_RotX(pVal);
end;

function TCustomDatumProperties.Get_RotY: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotY;
end;

procedure TCustomDatumProperties.Set_RotY(pVal: OleVariant);
begin
  DefaultInterface.Set_RotY(pVal);
end;

function TCustomDatumProperties.Get_RotZ: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RotZ;
end;

procedure TCustomDatumProperties.Set_RotZ(pVal: OleVariant);
begin
  DefaultInterface.Set_RotZ(pVal);
end;

function TCustomDatumProperties.Get_ScaleFactor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ScaleFactor;
end;

procedure TCustomDatumProperties.Set_ScaleFactor(pVal: OleVariant);
begin
  DefaultInterface.Set_ScaleFactor(pVal);
end;

function TCustomDatumProperties.Get_InverseFlattering: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.InverseFlattering;
end;

procedure TCustomDatumProperties.Set_InverseFlattering(pVal: OleVariant);
begin
  DefaultInterface.Set_InverseFlattering(pVal);
end;

function TCustomDatumProperties.Get_Transformable: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Transformable;
end;

procedure TCustomDatumProperties.Set_Transformable(pVal: OleVariant);
begin
  DefaultInterface.Set_Transformable(pVal);
end;

function TCustomDatumProperties.Get_Name: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Name;
end;

procedure TCustomDatumProperties.Set_Name(pVal: OleVariant);
begin
  DefaultInterface.Set_Name(pVal);
end;

{$ENDIF}

class function CoCustomGrid.Create: ICustomGrid;
begin
  Result := CreateComObject(CLASS_CustomGrid) as ICustomGrid;
end;

class function CoCustomGrid.CreateRemote(const MachineName: string): ICustomGrid;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CustomGrid) as ICustomGrid;
end;

procedure TCustomGrid.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{F293E541-4865-499E-B2E0-98E9AF79AC31}';
    IntfIID:   '{F367D809-6B57-45EC-ABC3-F2CEA90D8AF1}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCustomGrid.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ICustomGrid;
  end;
end;

procedure TCustomGrid.ConnectTo(svrIntf: ICustomGrid);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCustomGrid.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCustomGrid.GetDefaultInterface: ICustomGrid;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TCustomGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TCustomGridProperties.Create(Self);
{$ENDIF}
end;

destructor TCustomGrid.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TCustomGrid.GetServerProperties: TCustomGridProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TCustomGrid.Get_Datum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Datum;
end;

procedure TCustomGrid.Set_Datum(pVal: OleVariant);
begin
  DefaultInterface.Set_Datum(pVal);
end;

function TCustomGrid.Get_CustomDatum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomDatum;
end;

procedure TCustomGrid._Set_CustomDatum(pVal: OleVariant);
  { Warning: The property CustomDatum has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomDatum := pVal;
end;

function TCustomGrid.Get_Algorithm: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Algorithm;
end;

procedure TCustomGrid.Set_Algorithm(pVal: OleVariant);
begin
  DefaultInterface.Set_Algorithm(pVal);
end;

function TCustomGrid.Get_FalseEasting: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FalseEasting;
end;

procedure TCustomGrid.Set_FalseEasting(pVal: OleVariant);
begin
  DefaultInterface.Set_FalseEasting(pVal);
end;

function TCustomGrid.Get_FalseNorthing: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FalseNorthing;
end;

procedure TCustomGrid.Set_FalseNorthing(pVal: OleVariant);
begin
  DefaultInterface.Set_FalseNorthing(pVal);
end;

function TCustomGrid.Get_LongitudeOfOrigin: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LongitudeOfOrigin;
end;

procedure TCustomGrid.Set_LongitudeOfOrigin(pVal: OleVariant);
begin
  DefaultInterface.Set_LongitudeOfOrigin(pVal);
end;

function TCustomGrid.Get_LatitudeOfOrigin: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeOfOrigin;
end;

procedure TCustomGrid.Set_LatitudeOfOrigin(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeOfOrigin(pVal);
end;

function TCustomGrid.Get_ScaleFactor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ScaleFactor;
end;

procedure TCustomGrid.Set_ScaleFactor(pVal: OleVariant);
begin
  DefaultInterface.Set_ScaleFactor(pVal);
end;

function TCustomGrid.Get_LatitudeSP1: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeSP1;
end;

procedure TCustomGrid.Set_LatitudeSP1(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeSP1(pVal);
end;

function TCustomGrid.Get_LatitudeSP2: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeSP2;
end;

procedure TCustomGrid.Set_LatitudeSP2(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeSP2(pVal);
end;

function TCustomGrid.Compare(CustomGrid: OleVariant): OleVariant;
begin
  Result := DefaultInterface.Compare(CustomGrid);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TCustomGridProperties.Create(AServer: TCustomGrid);
begin
  inherited Create;
  FServer := AServer;
end;

function TCustomGridProperties.GetDefaultInterface: ICustomGrid;
begin
  Result := FServer.DefaultInterface;
end;

function TCustomGridProperties.Get_Datum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Datum;
end;

procedure TCustomGridProperties.Set_Datum(pVal: OleVariant);
begin
  DefaultInterface.Set_Datum(pVal);
end;

function TCustomGridProperties.Get_CustomDatum: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CustomDatum;
end;

procedure TCustomGridProperties._Set_CustomDatum(pVal: OleVariant);
  { Warning: The property CustomDatum has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CustomDatum := pVal;
end;

function TCustomGridProperties.Get_Algorithm: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Algorithm;
end;

procedure TCustomGridProperties.Set_Algorithm(pVal: OleVariant);
begin
  DefaultInterface.Set_Algorithm(pVal);
end;

function TCustomGridProperties.Get_FalseEasting: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FalseEasting;
end;

procedure TCustomGridProperties.Set_FalseEasting(pVal: OleVariant);
begin
  DefaultInterface.Set_FalseEasting(pVal);
end;

function TCustomGridProperties.Get_FalseNorthing: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FalseNorthing;
end;

procedure TCustomGridProperties.Set_FalseNorthing(pVal: OleVariant);
begin
  DefaultInterface.Set_FalseNorthing(pVal);
end;

function TCustomGridProperties.Get_LongitudeOfOrigin: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LongitudeOfOrigin;
end;

procedure TCustomGridProperties.Set_LongitudeOfOrigin(pVal: OleVariant);
begin
  DefaultInterface.Set_LongitudeOfOrigin(pVal);
end;

function TCustomGridProperties.Get_LatitudeOfOrigin: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeOfOrigin;
end;

procedure TCustomGridProperties.Set_LatitudeOfOrigin(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeOfOrigin(pVal);
end;

function TCustomGridProperties.Get_ScaleFactor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ScaleFactor;
end;

procedure TCustomGridProperties.Set_ScaleFactor(pVal: OleVariant);
begin
  DefaultInterface.Set_ScaleFactor(pVal);
end;

function TCustomGridProperties.Get_LatitudeSP1: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeSP1;
end;

procedure TCustomGridProperties.Set_LatitudeSP1(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeSP1(pVal);
end;

function TCustomGridProperties.Get_LatitudeSP2: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.LatitudeSP2;
end;

procedure TCustomGridProperties.Set_LatitudeSP2(pVal: OleVariant);
begin
  DefaultInterface.Set_LatitudeSP2(pVal);
end;

{$ENDIF}

class function CoGpsToolsVersion.Create: IGpsToolsVersion;
begin
  Result := CreateComObject(CLASS_GpsToolsVersion) as IGpsToolsVersion;
end;

class function CoGpsToolsVersion.CreateRemote(const MachineName: string): IGpsToolsVersion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_GpsToolsVersion) as IGpsToolsVersion;
end;

procedure TGpsToolsVersion.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{7E5DF1C0-0008-420F-A7AD-B655C1059E31}';
    IntfIID:   '{7E0EF922-5D91-4A67-9846-77C1D455AE53}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TGpsToolsVersion.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IGpsToolsVersion;
  end;
end;

procedure TGpsToolsVersion.ConnectTo(svrIntf: IGpsToolsVersion);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TGpsToolsVersion.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TGpsToolsVersion.GetDefaultInterface: IGpsToolsVersion;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TGpsToolsVersion.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TGpsToolsVersionProperties.Create(Self);
{$ENDIF}
end;

destructor TGpsToolsVersion.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TGpsToolsVersion.GetServerProperties: TGpsToolsVersionProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TGpsToolsVersion.Get_Major: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Major;
end;

function TGpsToolsVersion.Get_Minor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Minor;
end;

function TGpsToolsVersion.Get_Build: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Build;
end;

function TGpsToolsVersion.Get_Revision: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Revision;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TGpsToolsVersionProperties.Create(AServer: TGpsToolsVersion);
begin
  inherited Create;
  FServer := AServer;
end;

function TGpsToolsVersionProperties.GetDefaultInterface: IGpsToolsVersion;
begin
  Result := FServer.DefaultInterface;
end;

function TGpsToolsVersionProperties.Get_Major: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Major;
end;

function TGpsToolsVersionProperties.Get_Minor: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Minor;
end;

function TGpsToolsVersionProperties.Get_Build: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Build;
end;

function TGpsToolsVersionProperties.Get_Revision: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Revision;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TLicense, TNmeaParser, TNmeaSentence, TPosition, 
    TCustomDatum, TCustomGrid, TGpsToolsVersion]);
end;

end.
