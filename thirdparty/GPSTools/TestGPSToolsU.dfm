object MainTestForm: TMainTestForm
  Left = 192
  Top = 114
  Caption = 'Test GPSTools'
  ClientHeight = 508
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GPSMemo: TMemo
    Left = 0
    Top = 55
    Width = 713
    Height = 453
    Align = alClient
    Lines.Strings = (
      'GPSMemo')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 28
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object StartButton: TButton
      Left = 1
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = StartButtonClick
    end
    object StopButton: TButton
      Left = 82
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Stop'
      Enabled = False
      TabOrder = 1
      OnClick = StopButtonClick
    end
    object RunThreadedCheckbox: TCheckBox
      Left = 208
      Top = 5
      Width = 97
      Height = 17
      Caption = 'Run Threaded'
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 28
    Width = 713
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    Color = clMedGray
    TabOrder = 2
    object StatusLabel: TLabel
      Left = 8
      Top = 8
      Width = 71
      Height = 13
      Caption = 'No active GPS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object GpsTimer: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = GpsTimerTimer
    Left = 20
    Top = 88
  end
end
