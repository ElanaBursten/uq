program TestGPSTools;

// Use this project search path: ..\..\common

uses
  Forms,
  TestGPSToolsU in 'TestGPSToolsU.pas' {MainTestForm},
  GpsToolsXP_TLB in 'GpsToolsXP_TLB.pas',
  OdMiscUtils in '..\..\common\OdMiscUtils.pas',
  GPSThread in '..\..\client\GPSThread.pas',
  QMConst in '..\..\client\QMConst.pas',
  BackgroundWorker in '..\..\client\BackgroundWorker.pas',
  UQGPS in '..\..\common\UQGPS.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
    ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  Application.Initialize;
  Application.CreateForm(TMainTestForm, MainTestForm);
  Application.Run;
end.
