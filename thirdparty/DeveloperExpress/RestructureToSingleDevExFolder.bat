@REM This script copies DevEx component files from the DevEx installation folder to
@REM a single folder. They can then be installed using a custom project we have made 
@REM for the runtime and design time packages.

@REM -----------Set these paths, then run this script------------------------
@REM This is the folder where you told the DevEx installer to place the files:
SET DEVEXINSTALLFOLDER=C:\Delphi Tools\DevEx1224

@REM This will be the single folder you want to use to install DevEx into the IDE from.
SET DESTFOLDER=C:\Delphi Tools\DevEx1224Pkg\
@REM (note to remember build box location)
@REM SET DESTFOLDER = C:\QMBuildServer\QManager\thirdparty\DevEx1224

@REM This is your path to the DeveloperExpress folder in source control.
SET DEVEXPROJSOURCE=C:\Trunk1\thirdparty\DeveloperExpress
@REM ------------------------------------------------------------------------

@REM Create/clean new folder
IF exist %DESTFOLDER%\nul ( echo %DESTFOLDER% exists ) ELSE ( mkdir %DESTFOLDER% && echo %DESTFOLDER% created)
del "%DESTFOLDER%*.groupproj"

pause
del %DESTFOLDER%*.dproj
del %DESTFOLDER%*.dpk
del %DESTFOLDER%*.~*
del %DESTFOLDER%*.dsk
del %DESTFOLDER%*.ddp
del %DESTFOLDER%*.cfg
del %DESTFOLDER%*.map
del %DESTFOLDER%*.local
del %DESTFOLDER%*.dcu
del %DESTFOLDER%*.pas
del %DESTFOLDER%*.dfm
del %DESTFOLDER%*.inc
del %DESTFOLDER%*.res
del %DESTFOLDER%*.dcr
del %DESTFOLDER%*.identcache
del %DESTFOLDER%*.used
del %DESTFOLDER%*.bpl
del %DESTFOLDER%*.dcp
rmdir %DESTFOLDER%__history /Q /S

pause

@REM Now copy the component files to the new folder
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.pas" %DESTFOLDER%
pause


copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.pas" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.dfm" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.inc" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.res" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.dcr" %DESTFOLDER%

copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.pas" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.dfm" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.inc" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.res" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.dcr" %DESTFOLDER%

@REM copy the custom group and pkg projects to the single new folder.
copy "%DEVEXPROJSOURCE%\*.groupproj" %DESTFOLDER%
copy "%DEVEXPROJSOURCE%\*.dproj" %DESTFOLDER%
copy "%DEVEXPROJSOURCE%\*.dpk" %DESTFOLDER%

pause

@REM Now open the .groupproj in the IDE. Build the runtime package first, then build and install the design-time package.
