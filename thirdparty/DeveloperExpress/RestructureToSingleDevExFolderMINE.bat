@REM This script copies DevEx component files from the DevEx installation folder to
@REM a single folder. They can then be installed using a custom project we have made 
@REM for the runtime and design time packages.

@REM -----------Set these paths, then run this script------------------------
@REM This is the folder where you told the DevEx installer to place the files:
@REM SET DEVEXINSTALLFOLDER=C:\Program Files (x86)\DevExpress VCL\
SET DEVEXINSTALLFOLDER=C:\Delphi Tools\DevExpressFull1224XE3\DevExpress VCL\

@REM This will be the single folder you want to use to install DevEx into the IDE from.
@REM SET DESTFOLDER=C:\Components\DevEx1224\
SET DESTFOLDER=C:\Delphi Tools\DevEx1224_XE3
@REM (note to remember build box location)
@REM SET DESTFOLDER = C:\QMBuildServer\QManager\thirdparty\DevEx1224\

@REM This is your path to the DeveloperExpress folder in source control.
@REM SET DEVEXPROJSOURCE=C:\Oasis Digital\QManager\trunk\thirdparty\DeveloperExpress\
SET DEVEXPROJSOURCE=C:\DATA\DEVELOPMENT\WORKING\QManager\TRUNK1\thirdparty\DeveloperExpress
@REM ------------------------------------------------------------------------

@REM Create/clean new folder
IF exist %DESTFOLDER%\nul ( echo %DESTFOLDER% exists ) ELSE ( mkdir %DESTFOLDER% && echo %DESTFOLDER% created)
del %DESTFOLDER%*.groupproj
del %DESTFOLDER%*.dproj
del %DESTFOLDER%*.dpk
del %DESTFOLDER%*.~*
del %DESTFOLDER%*.dsk
del %DESTFOLDER%*.ddp
del %DESTFOLDER%*.cfg
del %DESTFOLDER%*.map
del %DESTFOLDER%*.local
del %DESTFOLDER%*.dcu
del %DESTFOLDER%*.pas
del %DESTFOLDER%*.dfm
del %DESTFOLDER%*.inc
del %DESTFOLDER%*.res
del %DESTFOLDER%*.dcr
del %DESTFOLDER%*.identcache
del %DESTFOLDER%*.used
del %DESTFOLDER%*.bpl
del %DESTFOLDER%*.dcp
rmdir %DESTFOLDER%__history /Q /S

pause

@REM Now copy the component files to the new folder
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.inc" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.res" %DESTFOLDER%
copy "%DEVEXINSTALLFOLDER%ExpressCommon Library\Sources\*.dcr" %DESTFOLDER%

copy "%DEVEXINSTALLFOLDER%ExpressCore Library\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCore Library\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCore Library\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCore Library\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressCore Library\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressDataController\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressDataController\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressDataController\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressDataController\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressDataController\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressEditors Library\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressEditors Library\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressEditors Library\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressEditors Library\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressEditors Library\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressExport Library\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressExport Library\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressExport Library\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressExport Library\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressExport Library\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressGDI+ Library\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressGDI+ Library\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressGDI+ Library\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressGDI+ Library\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressGDI+ Library\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressLayout Control\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLayout Control\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLayout Control\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLayout Control\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLayout Control\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressLibrary\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLibrary\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLibrary\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLibrary\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressLibrary\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressMemData\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressMemData\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressMemData\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressMemData\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressMemData\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressPageControl\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPageControl\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPageControl\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPageControl\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPageControl\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressPivotGrid\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPivotGrid\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPivotGrid\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPivotGrid\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressPivotGrid\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressQuantumGrid\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumGrid\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumGrid\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumGrid\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumGrid\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%ExpressQuantumTreeList\Sources\*.dcr" "%DESTFOLDER%"

copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.pas" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.dfm" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.inc" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.res" "%DESTFOLDER%"
copy "%DEVEXINSTALLFOLDER%XP Theme Manager\Sources\*.dcr" "%DESTFOLDER%"

@REM copy the custom group and pkg projects to the single new folder.
copy "%DEVEXPROJSOURCE%\*.groupproj" "%DESTFOLDER%"
copy "%DEVEXPROJSOURCE%\*.dproj" "%DESTFOLDER%"
copy "%DEVEXPROJSOURCE%\*.dpk" "%DESTFOLDER%"

pause

@REM Now open the .groupproj in the IDE. Build the runtime package first, then build and install the design-time package.
