Developer Express Grid Pack 12.2.4 (DevExpressVCLProducts-12.2.4.exe)
 (Powerful grid, data entry controls, etc.) (Must be purchased)
  http://www.devexpress.com/
Note: A custom combined package was created that combines DevEx 12.2.4 into 
  a single runtime and single designtime package. The Express Grid Pack must
  be purchased and the source installed, but the custom package project is 
  located under \trunk\thirdparty\DeveloperExpress.


Developer Express Installation Notes
--------------------------------------------------------------------------------
1) Uninstall old DevEx 
  a) Remove DevEx pkgs in Delphi
  b) Run DevEx uninstaller
  c) Search for and delete bpls: cx*.bpl and dx*.bpl).
2) Run the DevEx installer (Express Grid Pack 12.2.4 DevExpressVCLProducts-12.2.3.exe).
   UNcheck all options except: ExpressPivotGrid, ExpressQuantumGrid and 
   ExpressQuantumTreeList (ExpressLayoutControl cannot be unchecked). Be sure to 
   uncheck the ExpressSkins library or it will create a bunch of extra files you 
   don't need. Note your installation folder, you'll need it later.
3) Edit trunk\thirdparty\DeveloperExpress\RestructureToSingleDevExFolder.bat. 
   Set the folder name variables for your environment and save. They default to these values,
   which might work for your environment if you installed DevEx to the default folder:
      DEVEXINSTALLFOLDER=C:\Program Files (x86)\DevExpress VCL\
      DESTFOLDER=C:\Components\DevEx1224\
4) Run trunk\thirdparty\DeveloperExpress\RestructureToSingleDevExFolder.bat to 
   consolidate the component files into a single destination directory. This will first clean
   out the destination folder and then copy the source and package project files into this 
   single directory.
5) Open Delphi, if there are any DevEx packages installed then remove them and their paths also. 
   At this point you could uninstall DevEx if you want to.
   To build and install the custom packages, repeat the next two steps in Delphi 2007 and XE3.
6) In Delphi, add your destination folder to the environment path.
7) Go to the destination folder you set in the .bat file. Open the custom package
   group in Delphi. Compile the runtime package, then install the designtime package.
