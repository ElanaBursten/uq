This library was added to wrap Powershell calls in Delphi (QM-147)
It is generally used for wrapping the console of any console application (input/output). However, we are using it to capture output for certain Powershell commands without a lot of overhead.

Note: Powershell version is dependent on .NET package installed (most of the time).  There seemed to be an issue with using this application with any Powershell version under 5.  But not sure if that is the build interfering.

