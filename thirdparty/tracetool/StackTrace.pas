{
  Include that unit in your applicatin just after the TraceTool unit to enable Stack tracing.

  You MUST have JEDI Code Library (JCL) installed to use that unit.
  See http://www.delphi-jedi.org then go to "JCL Code Library" section
  Or go directly to http://sourceforge.net/projects/jcl for download
  The actual code is tested with the JVCL 1.9 beta version.
  The JVCL 1.22 and the 1.9 are quite similar (just one flag is added)

  Stack tracing is not include by default in tracetool for some reasons :
  - Not everyone want to install JEDI.
  - Target size.
  - stack information is not always usefull.
  - Some other solutions exist for stack tracing.
    This unit is just a wrapper for the JCL way.
    You can write another wrapper (see the initialization section in this file)

  Important note : To display correctly the stack (and related), don't forget to set 
                   'Detailed' Map in the Linker project options.

  Author : Thierry Parent
}

unit StackTrace;

interface

uses Classes , windows , SysUtils, TraceTool , JclDebug  ;

   procedure JCLDebug_SendStack  (NodeEx : ITraceNodeEx ; Level: Integer = 0) ;
   procedure JCLDebug_SendCaller (NodeEx : ITraceNodeEx ; Level: Integer = 0) ;
   function  JCLDebug_SendMethod (NodeEx : ITraceNodeEx ; const Addr: Pointer) : string ;
   function  IsMapFileDetailled  (const Addr: Pointer): boolean ;

implementation

//------------------------------------------------------------------------------

procedure JCLDebug_SendStack (NodeEx : ITraceNodeEx; Level: integer = 0) ;
var
  i : integer ;
  group : TMemberNode ;
  isOneDebug : boolean ;
begin

  isOneDebug := false ;
  group := TMemberNode.create ('Stack information') ;
  NodeEx.Members.Add (group) ;

  with TJclStackInfoList.Create(false, 0, nil) do
  try
    for I := 1+Level to Count - 1 do begin
       if IsMapFileDetailled (Items[I].CallerAddr) then
          isOneDebug := true ;
       group.Add (GetLocationInfoStr(Items[I].CallerAddr,true)) ;
          // all other GetLocationInfoStr options are set to default value : false
          // IncludeModuleName , IncludeAddressOffset ,
          // IncludeStartProcLineOffset and IncludeVAdress (starting from 1.90)
    end ;
       if isOneDebug = false then begin
          group.Add ('->No detailed map file') ;
          group.Add ('->See Project/Options/Linker') ;
       end ;

  finally
    Free;
  end;
end ;

//------------------------------------------------------------------------------

procedure JCLDebug_SendCaller (NodeEx : ITraceNodeEx ; Level: Integer = 0) ;
var
  group : TMemberNode ;
  ptr : Pointer ;
begin
  group := TMemberNode.create ('Caller information') ;
  NodeEx.Members.Add (group) ;
  ptr := Caller(Level+1) ;
  group.Add (GetLocationInfoStr (ptr)); // all other options have default value (false)
  if IsMapFileDetailled (ptr) = false then begin
     group.Add ('->No detailed map file') ;
     group.Add ('->See Project/Options/Linker') ;
  end ;
end ;

//------------------------------------------------------------------------------

function JCLDebug_SendMethod (NodeEx : ITraceNodeEx ; const Addr: Pointer) : string;
var
  group : TMemberNode ;
begin
   result := GetLocationInfoStr (Addr) ;
   if NodeEx = nil then
      exit ;
   group := TMemberNode.create ('Method information') ;
   NodeEx.Members.Add (group) ;
   group.Add (result) ;  // all other options have default value (false)
   if IsMapFileDetailled (Addr) = false then begin
      group.Add ('->No detailed map file') ;
      group.Add ('->See Project/Options/Linker') ;
   end ;
end ;

//------------------------------------------------------------------------------

function IsMapFileDetailled  (const Addr: Pointer): boolean ;
var
  Info : TJclLocationInfo;
begin
   result := false ;
   if GetLocationInfo(Addr, Info) = false then
      exit ;
   if Info.LineNumber <= 0 then
      exit  ;

   result := true ;
end ;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

initialization
   SendStackProc  := JCLDebug_SendStack  ;
   SendCallerProc := JCLDebug_SendCaller ;
   SendMethodProc := JCLDebug_SendMethod ;

end.
