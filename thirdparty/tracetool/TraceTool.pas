{
  This the Trace tool client part that send message to the Viewer.

  Author : Thierry Parent

  Version : 4.1.0

  Optional stack traces can be done using the provided StackTrace unit that use jedi code source,
  See http://www.delphi-jedi.org then go to "JCL Code Library" section
  Or  http://sourceforge.net/projects/jcl/
}

unit TraceTool;

interface

uses Classes , windows, ActiveX , sysutils, Registry , Messages,
     menus, comobj , SyncObjs , Contnrs,
     Variants,
     TypInfo;

{$Include TraceTool.Inc}

type

   // forward declaration

   ITraceNode     = interface ;
   ITraceNodeBase = interface ;
   ITraceNodeEx   = interface ;
   IWinTrace      = interface ;

   TTrace         = class ;
   TTraceOptions  = class ;
   TSocketTrace   = class ;


   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   // the main entry point
   TTrace = class
   private
      class procedure SendToClient (CommandList : TStringList ; winTrace : IWinTrace) ;
   public
      class function  Warning : ITraceNode ;         // use main trace win
      class function  Error : ITraceNode ;           // use main trace win
      class function  Debug : ITraceNode ;           // use main trace win
      class function  Options : TTraceOptions ;
      class function  WinTrace : IWinTrace ;

      class procedure ClearAll ()  ;                // clear the main trace win
      class procedure Show ( IsVisible : boolean) ;
      class procedure Flush (FlushTimeOut : integer = 5000);
      class function  CreateTraceID: string;
      class function  createWinTrace (WinTraceID : string ; WinTraceText : string = '') : IWinTrace ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   IWinTrace = interface
      function Warning : ITraceNode ;
      function Error : ITraceNode ;
      function Debug : ITraceNode ;

      // Id
      function  getId : string ;
      procedure setId (v : string) ;
      property  Id : string read getId write setId ;

      procedure SaveToTextfile (Filename : string) ;
      procedure SaveToXml (Filename : string) ;
      procedure LoadXml (Filename : string) ;
      procedure DisplayWin () ;
      procedure setMultiColumn() ;
      procedure setColumnsTitle(Titles:string) ;
      procedure ClearAll () ;
      procedure setLogFile (Filename : string; mode : integer) ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   TSendMode = (tmWinMsg, tmAlternate);

   // some flags are not usefull in delphi 7
   TraceDisplayFlag =
   (
      ShowModifiers        ,      // Not implemented (dot net or java)
      ShowCustomAttributes ,      // Not implemented (dot net or Java5)
      ShowNonPublic        ,      // Not implemented (dot net or java)
      ShowInheritedMembers ,      // Not implemented (dot net or java)
      ShowClassInfo        ,
      ShowFields           ,
      ShowEvents           ,
      ShowMethods
   ) ;
   TraceDisplayFlags = set of TraceDisplayFlag ;


   /// <summary>
   /// Options for the traces.
   /// </summary>
   TTraceOptions = class
   private
      fProcessFileName : string ;
      fSendMode : TSendMode;
      fSendFunctions : boolean ;
      fSendInherited : boolean ;
      fSendEvents : boolean ;
      fSocketPort: integer;
      fSocketHost: string;

      constructor Create;
      procedure SetProcessName (const Value : boolean) ;
      procedure setSocketHost(const Value: string);
      procedure setSocketPort(const Value: integer);
   protected
      function GetDefault() : TraceDisplayFlags ;

   public
      /// Change SendMode to Mode.Socket to use it under ASP
      property SendMode : TSendMode read fSendMode write fSendMode ;  //  = SendMode.WinMsg ;
      /// specify if the process name should be send
      property SendProcessName : boolean                      write SetProcessName ;
      /// indicate if the reflection should display functions
      property SendFunctions   : boolean read fSendFunctions  write fSendFunctions ;
      /// indicate if the reflections should display inherited members
      property SendInherited   : boolean read fSendInherited  write fSendInherited ;
      /// indicate if the reflections should display the events
      property SendEvents      : boolean read fSendEvents     write fSendEvents ;

      property SocketHost : string  read fSocketHost write setSocketHost ;
      property SocketPort : integer read fSocketPort write setSocketPort ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   /// <summary>
   /// TMemberNode represent a node inside the right object tree
   /// </summary>

   TMemberNode = class
   public
      /// The 3 columns to display
      Col1, Col2, Col3 : string ;
      /// an array of sub members (TMemberNode).
      Members : array of TMemberNode  ;
      Tag : Integer ;  // User defined tag, NOT SEND to the viewer

      /// Create a TMemberNode with text for the 3 columns
      constructor create () ; overload ;
      constructor create (col1 : string) ; overload ;
      constructor create (col1 , col2 : string) ; overload ;
      constructor create (col1 , col2 , col3 : string) ; overload ;

      /// Add a member to the members list
      function Add (member : TMemberNode ) : TMemberNode ; overload ;
      /// Create a sub TMemberNode with text for the 3 columns and add it to the Members array
      function Add (col1  : string) : TMemberNode ; overload ;
      function Add (col1 , col2 : string) : TMemberNode ;  overload ;
      function Add (col1 , col2 , col3 : string) : TMemberNode ; overload ;

      /// recursively convert members to a string list
      procedure AddToStringList (CommandList : TStringList ) ;
      procedure BeforeDestruction ; override ;
   end ;


   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   /// the base node interface : give access to ID, enabled and IconIndex properties
   ITraceNodeBase = interface
      // ID
      function getID : string ;
      procedure setID (v : string) ;
      property id : string read getId write setID ;

      // Enabled
      function  getEnabled : boolean ;
      procedure setEnabled (v : boolean) ;
      property  Enabled : boolean read getEnabled write setEnabled ;

      // IconIndex
      function  getIconIndex: integer;
      procedure setIconIndex (v : integer) ;
      property  IconIndex: integer read getIconIndex write setIconIndex ;

      // WinTrace
      function  getWinTrace: IWinTrace;
      property  WinTrace: IWinTrace read getWinTrace ;

      // User variable, provided for the convenience of developers
      function  getTag : integer ;
      procedure setTag (v : integer) ;
      property Tag : integer read getTag write setTag ;

      // create a new TraceNodeEx node from the current node
      function CreateNodeEx : ITraceNodeEx ;

   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   /// first way to send traces : call send or Resend. You must give all parameters
   // Since TTrace.warning, debug and error are ITraceNode, you can create new ITraceNodeEx objects from here
   ITraceNode = interface (ITraceNodeBase)
      // Send functions
      function  Send        (leftMsg : string) : ITraceNode ; Overload ;
      function  Send        (leftMsg,RightMsg: string): ITraceNode; Overload ;
      function  Send        (leftMsg : variant ): ITraceNode; Overload ;
      function  Send        (leftMsg : variant; RightMsg: variant): ITraceNode; Overload ;

      function  SendObject  (leftMsg : string ; Obj : TObject) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; Obj : TObject; flags : TraceDisplayFlags) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; disp : IDispatch) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; disp : IDispatch; flags : TraceDisplayFlags) : ITraceNode; overload ;

      // Ambigious overloaded call under delphi 5 :-(
      {$IFDEF Delphi7}
      function  SendObject  (leftMsg : string ; v : variant) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; v : variant; flags : TraceDisplayFlags) : ITraceNode; overload ;
      {$ENDIF}
      {$IFNDEF Delphi7}
      function  SendObjectV  (leftMsg : string ; v : variant) : ITraceNode; overload ;
      function  SendObjectV  (leftMsg : string ; v : variant; flags : TraceDisplayFlags) : ITraceNode; overload ;
      {$ENDIF}

      function  SendDump    (leftMsg, Title: string; memory: pointer; ByteCount: integer): ITraceNode ;
      function  SendStrings (leftMsg: string; strings: TStrings): ITraceNode ;
      function  SendStack   (leftMsg: string; level: integer = 0): ITraceNode ;
      function  SendCaller  (leftMsg: string; level: integer = 0): ITraceNode ;
      function  SendMethod  (leftMsg: string; Meth: Pointer) : ITraceNode ;
      function  SendValue   (leftMsg: string; v: Variant): ITraceNode;

      procedure Indent (leftMsg : string) ;
      procedure UnIndent (leftMsg : string = '') ;

      // Resend functions
      function  Resend      (leftMsg,RightMsg: string) : ITraceNode;overload ;
      function  ResendLeft  (leftMsg: string) : ITraceNode;
      function  ResendRight (RightMsg: string) : ITraceNode;
      function  Append      (leftMsg,RightMsg: string): ITraceNode;
      function  AppendLeft  (leftMsg : string) : ITraceNode ;
      function  AppendRight (RightMsg : string) : ITraceNode ;
      function  Show        () : ITraceNode ;
      function  SetSelected () : ITraceNode ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   /// second way to send traces : create an object and fill the fields then send it
   /// the send function return a ITraceNode interface
   ITraceNodeEx = interface (ITraceNodeBase)
      // LeftMsg
      function  getLeftMsg : string ;
      procedure setLeftMsg (v : string) ;
      property  LeftMsg : string read getLeftMsg write setLeftMsg ;

      // RightMsg
      function  getRightMsg : string ;
      procedure setRightMsg (v : string) ;
      property  RightMsg : string read getRightMsg write setRightMsg ;

      // ParentNodeId : The Id of the parent node
      function  getParentNodeId : string ;
      procedure setParentNodeId (v : string) ;
      property ParentNodeId : string read getParentNodeId write setParentNodeId ;

      function getMembers : TMemberNode ;
      property Members : TMemberNode read getMembers ;

      // Add procedure
      procedure AddDump (Title: string; memory: pointer; ByteCount: integer) ;
      procedure AddObject (Obj : TObject ; flags : TraceDisplayFlags) ; overload ;
      procedure AddObject (Obj : TObject) ; overload ;
      procedure AddObject (Disp : IDispatch; flags : TraceDisplayFlags); overload ;
      procedure AddObject (Disp : IDispatch); overload ;

      // Ambigious overloaded call under delphi 5 :-(
      {$IFDEF Delphi7}
      procedure AddObject (v : variant; flags : TraceDisplayFlags); overload ;
      procedure AddObject (v : variant); overload ;
      {$ENDIF}
      {$IFNDEF Delphi7}
      procedure AddObjectV (v : variant; flags : TraceDisplayFlags); overload ;
      procedure AddObjectV (v : variant); overload ;
      {$ENDIF}

      procedure AddStrings (strings: TStrings);
      procedure AddStackTrace (Level : integer = 0);
      procedure AddCaller (Level : integer = 0);
      procedure AddMethod (Meth : Pointer) ;

      // send left,right and members
      function Send : ItraceNode ;

      // Resend functions
      function  Resend : ItraceNode ; overload ;  // resend currrent Left and right msg

   end ;

   TSocketTrace = class
      procedure SetHost(const Value: string); virtual ; abstract ;
      procedure SetPort(const Value: integer); virtual ; abstract ;
      procedure Send (str : string; nbchar : integer) ; virtual ; abstract ;
   end ;


   // The followings procedures type are used to extend TraceTool
   //TSendProc        = procedure (str : string; nbchar : integer) ;
   TSendStackProc   = procedure (NodeEx : ITraceNodeEx ; Level: Integer = 0) ;
   TSendCallerProc  = procedure (NodeEx : ITraceNodeEx ; Level: Integer = 0) ;
   TSendMethodProc  = function (NodeEx : ITraceNodeEx ; const Addr: Pointer) : string ;

   function StartDebugWin: hWnd;
   function getVariantType (TypeInfo : ITypeInfo ; ObjTypeDesc: PTypeDesc) : string ; overload ;
   function GetVariantType(VarType: integer): string; overload ;
   function GetVarValue(v: Variant): string; overload ;
   function GetVarValue(v: Variant; var strType: string): string; overload ;
   function GetDispatchName (Disp : IDispatch) : string ;
   procedure GetDispathDescriptions (TypeAttr : PTypeAttr; var strGuid,strProg, strTypeLibGuid, strTypeLib : string) ; overload ;
   procedure GetDispathDescriptions (Disp : IDispatch; var strGuid,strProg, strTypeLibGuid, strTypeLib : string) ; overload ;

var
   AlternateSend : TSocketTrace ;    // the alternate procedure (if for example SocketTrace unit is specified)

   // The followings procedures are used to extend TraceTool. By default theses procedures are nil
   // the developer need to include SocketTrace or stacktrace to automatically set theses procedures.
   SendStackProc     : TSendStackProc  ;
   SendCallerProc    : TSendCallerProc ;
   SendMethodProc    : TSendMethodProc ;

   // stat traces
   // swap : string ;

implementation

type


   // from Delphi 7 (don't exist in delphi 5)

   PPointer = ^Pointer ;
   PMethodInfoHeader = ^TMethodInfoHeader;
   TMethodInfoHeader = packed record
     Len: Word;
     Addr: Pointer;
     Name: ShortString;
   end;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   // the thread that send to the server
   TMsgThread = class(TThread)
   private
      hCloseEvent : THandle ;     // Handle to close the thread.
      hMessageReady : THandle ;   // signal that messages are ready to be send
   protected
      // master thread routine
      procedure Execute; override;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   TWinTrace = class (TInterfacedObject, IWinTrace)
   private
      fDebug    : ITraceNode ;
      fError    : ITraceNode ;
      fWarning  : ITraceNode ;
      fWinTraceID : string ;
   public
      constructor create (WinTraceID : string ; WinTraceText : string = '') ; overload ;
      function Warning : ITraceNode ;
      function Error : ITraceNode ;
      function Debug : ITraceNode ;

      // Id
      function  getId : string ;
      procedure setId (v : string) ;
      property  Id : string read getId write setId ;

      procedure SaveToTextfile (Filename : string) ;
      procedure SaveToXml (Filename : string) ;
      procedure DisplayWin () ;
      procedure setMultiColumn() ;
      procedure setColumnsTitle(Titles:string) ;


      procedure ClearAll () ;
      procedure LoadXml (Filename : string) ;
      procedure setLogFile (Filename : string; mode : integer) ;

   private
      constructor create () ; overload ;
   end ;


   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   TNodeContext = class
   public
     ThreadId : DWORD ;
     Level    : integer ;
     NodeId : string ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------

   TTraceNode = class (TInterfacedObject, ITraceNode , ITracenodeEx)
   private
      fId : string ;
      fEnabled : boolean ;
      fIconIndex: integer ;
      fMembers : TMemberNode ;  // Members object is create only when needed (AddDump, AddObject,...)
      fLeftMsg , fRightMsg : string ; // used in ITraceNodeEx mode
      fParentNodeId : string ;
      fWinTrace : TWinTrace ;
      fTag : integer ;           // User variable, provided for the convenience of developers
      fContextList : TObjectList ; // own objects

      // TOject reflection functions
      function  GetPropertyTypeString (TypeKind: TTypeKind): string;
      procedure DisplayFields (AObject: TObject; flags : TraceDisplayFlags);
      procedure DisplayMethods (AObject: TObject; flags : TraceDisplayFlags);
      procedure DisplayBases (AObject: TObject; flags : TraceDisplayFlags);

      // create new node and context related fcts.
      function  prepareNewNode (leftMsg, newId: string): TStringList;
      function  getLastContext  : TNodeContext ;
      function  getLastContextId : string;
      procedure PushContext (NewContext: TNodeContext);
      procedure deleteLastContext ;
   public

      constructor create (ParentNode : TTraceNode ; generateUniqueId : boolean ) ;
      procedure BeforeDestruction ; override ;

      // ITraceNodeBase : ID, Enabled , IconIndex
      //--------------------------------------------

      function  getId : string ;
      procedure setID (v : string) ;
      property id : string read getId write setID ;

      function  getEnabled : boolean ;
      procedure setEnabled (v : boolean) ;
      property  Enabled : boolean read getEnabled write setEnabled ;

      function  getIconIndex: integer;
      procedure setIconIndex (v : integer) ;
      property  IconIndex: integer read getIconIndex write setIconIndex ;

      function  getWinTrace: IWinTrace;
      property  WinTrace: IWinTrace read getWinTrace  ;

      function  getTag : integer ;
      procedure setTag (v : integer) ;
      property Tag : integer read getTag write setTag ;


      // ITraceNode
      //--------------------------------------------

      // Send functions
      function  Send        (leftMsg : string) : ITraceNode ; Overload ;
      function  Send        (leftMsg,RightMsg: string) : ITraceNode; Overload ;
      function  Send        (leftMsg : variant): ITraceNode; Overload ;
      function  Send        (leftMsg : variant; RightMsg: variant): ITraceNode; Overload ;

      function  SendObject  (leftMsg : string ; Obj : TObject) : ITraceNode ; overload ;
      function  SendObject  (leftMsg : string ; Obj : TObject; flags : TraceDisplayFlags) : ITraceNode ; overload ;
      function  SendObject  (leftMsg : string ; disp : IDispatch) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; disp : IDispatch; flags : TraceDisplayFlags) : ITraceNode; overload ;

      // Ambigious overloaded call under delphi 5 :-(
      {$IFDEF Delphi7}
      function  SendObject  (leftMsg : string ; v : variant) : ITraceNode; overload ;
      function  SendObject  (leftMsg : string ; v : variant; flags : TraceDisplayFlags) : ITraceNode; overload ;
      {$ENDIF}
      {$IFNDEF Delphi7}
      function  SendObjectV (leftMsg : string ; v : variant) : ITraceNode; overload ;
      function  SendObjectV (leftMsg : string ; v : variant; flags : TraceDisplayFlags) : ITraceNode; overload ;
      {$ENDIF}

      function  SendDump    (leftMsg, Title: string; memory: pointer; ByteCount: integer) : ITraceNode ;
      function  SendStrings (leftMsg: string; strings: TStrings) : ITraceNode ;
      function  SendStack   (leftMsg: string; level: integer = 0) : ITraceNode ;
      function  SendCaller  (leftMsg: string; level: integer = 0) : ITraceNode ;
      function  SendMethod  (leftMsg: string; Meth: Pointer) : ITraceNode;
      function  SendValue (leftMsg: string; v: Variant): ITraceNode;

      procedure Indent (leftMsg : string) ;
      procedure UnIndent (leftMsg : string = '') ;

      // Resend functions
      function  Resend : ItraceNode ;  overload ;  // resend the node (don't recreate a node and the target)
      function  Resend      (leftMsg, RightMsg: string) : ITraceNode ; overload ;
      function  ResendLeft  (leftMsg: string) : ITraceNode;
      function  ResendRight (RightMsg: string) : ITraceNode;
      function  Append      (leftMsg, RightMsg: string) : ITraceNode; Overload ;
      function  AppendLeft  (leftMsg : string) : ITraceNode ;
      function  AppendRight (RightMsg : string) : ITraceNode ;

      function  Show        () : ITraceNode ;
      function  SetSelected () : ITraceNode ;

      // create a new TraceNodeEx node with the current instance as parent.
      function CreateNodeEx : ITraceNodeEx ;

      // ITraceNodeEx
      //--------------------------------------------

      // LeftMsg
      function  getLeftMsg : string ;
      procedure setLeftMsg (v : string) ;
      property  LeftMsg : string read getLeftMsg write setLeftMsg ;

      // RightMsg
      function  getRightMsg : string ;
      procedure setRightMsg (v : string) ;
      property  RightMsg : string read getRightMsg write setRightMsg ;

      // ParentNodeId : The Id of the parent node
      function  getParentNodeId : string ;
      procedure setParentNodeId (v : string) ;
      property ParentNodeId : string  read FParentNodeId write SetParentNodeId;

      // Members
      function getMembers : TMemberNode ;
      property Members : TMemberNode read getMembers ;

      // Add procedure
      procedure AddDump (Title: string; memory: pointer; ByteCount: integer) ;
      procedure AddObject (Obj : TObject; flags : TraceDisplayFlags) ;  overload ;
      procedure AddObject (Obj : TObject) ; overload ;
      procedure AddObject (disp: IDispatch; flags : TraceDisplayFlags); overload ;
      procedure AddObject (disp: IDispatch); overload ;


      {$IFDEF Delphi7}
      procedure AddObject (v : variant; flags : TraceDisplayFlags); overload ;
      procedure AddObject (v : variant); overload ;
     {$ENDIF}
     {$IFNDEF Delphi7}
      procedure AddObjectV (v : variant; flags : TraceDisplayFlags); overload ;
      procedure AddObjectV (v : variant); overload ;
      {$ENDIF}

      procedure AddScannedObject (Obj : TObject) ;
      procedure AddStrings (strings: TStrings);

      procedure AddValue (v: Variant) ;
      procedure AddStackTrace (Level : integer = 0);
      procedure AddCaller (Level : integer = 0);
      procedure AddMethod (Meth : Pointer) ;


      // send left,right and members
      function Send : ItraceNode ; Overload ;
   end ;

   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------
   //--------------------------------------------------------------------------------------------------


var
   AnsiCharTypes: array [AnsiChar] of Word;
   MsgThread : TMsgThread ;
   _WinTrace : IWinTrace ;
   _opt      : TTraceOptions = nil ;

   // Messages list
   criticalSection : TCriticalSection ;
   setMessageList : TObjectList ;
   getMessageList : TObjectList;

threadvar
   SendObjectRecursiveStatus : short ;

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

// general purpose procedure and functions

// code only
procedure addCommand(CommandList: TStringList; code: integer); overload ;
begin
   CommandList.add (Format('%5d', [code])) ;
end;

//------------------------------------------------------------------------------

// code + int
procedure addCommand(CommandList: TStringList; code, intvalue: integer); overload ;
begin
   CommandList.add (Format('%5d%11d', [code,intvalue]));
end;

//------------------------------------------------------------------------------

// code + string
procedure addCommand(CommandList: TStringList; code: integer; lib: string); overload ;
begin
   CommandList.add (Format('%5d', [code]) + lib);
end;

//------------------------------------------------------------------------------

// code + int + string
procedure addCommand(CommandList: TStringList; code, intvalue: integer; lib: string); overload ;
begin
   CommandList.add (Format('%5d%11d', [code,intvalue]) + lib);
end;

//------------------------------------------------------------------------------

// code + bool
procedure addCommand(CommandList: TStringList; code: integer; boolvalue: boolean); overload ;
begin
   if boolvalue = true then
      addCommand (CommandList, code, '1')
   else
      addCommand (CommandList, code,'0') ;

end;

//------------------------------------------------------------------------------

function CharIsAlphaNum(const C: AnsiChar): Boolean;
begin
  Result := ((AnsiCharTypes[C] and C1_ALPHA) <> 0) or
    ((AnsiCharTypes[C] and C1_DIGIT) <> 0);
end;

//--------------------------------------------------------------------------------------------------

function CharIsAlpha(const C: AnsiChar): Boolean;
begin
  Result := (AnsiCharTypes[C] and C1_ALPHA) <> 0;
end;

//------------------------------------------------------------------------------

procedure LoadCharTypes;
var
  CurrChar: AnsiChar;
  CurrType: Word;
begin
  for CurrChar := Low(AnsiChar) to High(AnsiChar) do
  begin
    GetStringTypeExA(LOCALE_USER_DEFAULT, CT_CTYPE1, @CurrChar, SizeOf(AnsiChar), CurrType);
    AnsiCharTypes[CurrChar] := CurrType;
  end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

{ TWinTrace }

constructor TWinTrace.create(WinTraceID : string ; WinTraceText : string = '');
var
   CommandList : TStringList;
begin
   if WinTraceID = '' then
      WinTraceID :=  TTrace.CreateTraceID() ;

   fWinTraceID := WinTraceID ;
   create () ;
   CommandList := TStringList.create ;  // will be freed by the thread

   if WinTraceText = '' then
      WinTraceText := 'Trace ' + WinTraceID ;
   addCommand (CommandList, CST_TREE_NAME, WinTraceText);
   TTrace.SendToClient (CommandList, self);
end;


//------------------------------------------------------------------------------

constructor TWinTrace.create;
var
   TraceNode : TTraceNode ;
begin
   TraceNode := TTraceNode.create (nil,false);
   TraceNode.fWinTrace := self ;
   TraceNode.fIconIndex :=  CST_ICO_INFO ;
   fDebug := TraceNode ;

   TraceNode := TTraceNode.create (nil,false);
   TraceNode.fWinTrace := self ;
   TraceNode.fIconIndex := CST_ICO_WARNING ;
   fWarning := TraceNode ;

   TraceNode := TTraceNode.create (nil,false);
   TraceNode.fWinTrace := self ;
   TraceNode.fIconIndex := CST_ICO_ERROR ;
   fError := TraceNode ;
end;

//------------------------------------------------------------------------------

function TWinTrace.Debug: ITraceNode;
begin
   result := fDebug ;
end;

//------------------------------------------------------------------------------

function TWinTrace.Error: ITraceNode;
begin
   result := fError ;
end;

//------------------------------------------------------------------------------

function TWinTrace.Warning: ITraceNode;
begin
   result := fWarning ;
end;

//------------------------------------------------------------------------------

function TWinTrace.getId: string;
begin
   result := fWinTraceID ;
end;

//------------------------------------------------------------------------------

procedure TWinTrace.setId (v: string);
begin
   fWinTraceID := v ;
end;

//------------------------------------------------------------------------------

procedure TWinTrace.ClearAll;
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_CLEAR_ALL);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.DisplayWin;
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_DISPLAY_TREE);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.setMultiColumn() ;
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_TREE_MULTI_COLUMN);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.setColumnsTitle(Titles:string) ;
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_TREE_COLUMNTITLE,Titles);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.SaveToTextfile(Filename: string);
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_SAVETOTEXT, Filename);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.SaveToXml(Filename: string);
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_SAVETOXML, Filename);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------

procedure TWinTrace.LoadXml(Filename: string);
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_LOADXML, Filename);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------
//
// Set the log file.<br>(Path is relative to the viewer)
// fileName : target filename.
// mode : When 0, Log is disabled.
//        When 1, no limit.
//        When 2, a new file is create each day (CCYYMMDD is appended to the filename)

procedure TWinTrace.setLogFile(Filename: string; mode: integer);
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_LOGFILE, mode, Filename);
   TTrace.SendToClient (CommandList,self);
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



{ TTrace }

class function TTrace.Debug: ITraceNode;
begin
   result := WinTrace.Debug ;
end;

//------------------------------------------------------------------------------

class function TTrace.Error: ITraceNode;
begin
   result := WinTrace.Error ;
end;

//------------------------------------------------------------------------------

class function TTrace.Warning: ITraceNode;
begin
   result := WinTrace.Warning ;
end;

//------------------------------------------------------------------------------

class function TTrace.WinTrace: IWinTrace;
begin
   if _WinTrace = nil then
      _WinTrace := TWinTrace.create ;
   result := _WinTrace ;
end;

//------------------------------------------------------------------------------

class function TTrace.Options: TTraceOptions;
begin
   if _opt = nil then
      _opt := TTraceOptions.Create ;
   result := _opt ;
end;

//------------------------------------------------------------------------------

class function TTrace.CreateTraceID: string;
var
  P: PWideChar;
  TID: TGUID ;
begin
  CoCreateGuid(TID);
  StringFromCLSID(TID, P);
  Result := P;
  CoTaskMemFree(P);
end;

//------------------------------------------------------------------------------


class function TTrace.createWinTrace(WinTraceID, WinTraceText: string): IWinTrace;
begin
   result := TWinTrace.create (WinTraceID, WinTraceText) ;
end;

//------------------------------------------------------------------------------

class procedure TTrace.ClearAll () ;
begin
   WinTrace.ClearAll() ;
end;

//--------------------------------------------------------------------------------------------------

class procedure TTrace.Show(IsVisible: boolean);
var
   CommandList : TStringList;
begin
   CommandList := TStringList.create ;  // will be freed by the thread
   if IsVisible = true then
      addCommand (CommandList, CST_SHOW, 1)
   else
      addCommand (CommandList, CST_SHOW, 0);

   SendToClient (CommandList, nil);
end;

//------------------------------------------------------------------------------

class procedure TTrace.SendToClient (CommandList: TStringList ; winTrace : IWinTrace );
begin

   // CST_PROCESS_NAME,CST_THREAD_ID and CST_MESSAGE_TIME must be processsed before other command


   // insert process name
   if Options.fProcessFileName <> '' then
      CommandList.Insert(0,Format('%5d%s', [CST_PROCESS_NAME,Options.fProcessFileName]));

   // insert thread id
   CommandList.Insert(0,Format('%5d%11d', [CST_THREAD_ID,getCurrentThreadID()]));

   // add current time
   CommandList.Insert(0,Format('%5d%s', [CST_MESSAGE_TIME,FormatDateTime('hh:mm:ss:zzz',now)])) ;

   // CST_USE_TREE must be inserted at th FIRST position
   if winTrace <> nil then
      if winTrace.ID <> '' then
         CommandList.Insert(0, Format('%5d%s', [CST_USE_TREE,winTrace.ID])) ;

   criticalSection.Acquire ;
   setMessageList.Add(CommandList);
   setevent(MsgThread.hMessageReady) ;
   criticalSection.Leave ;


   // the CommandList is freed by the thread.
   //PostThreadMessage(MsgThread.ThreadID, WM_SEND_TO_DEBUG, WParam(0) , LParam(Pointer(CommandList)) ) ;
end;

//------------------------------------------------------------------------------

class procedure TTrace.Flush (FlushTimeOut : integer = 5000);
var
   flushEvent : THandle ;
   CommandList: TStringList ;
begin
   // create the event
   flushEvent := CreateEvent( nil, True, False, nil );  // Create the close event. Manualreset = true, initial = false

   // create the flush event message
   CommandList := TStringList.Create ;
   CommandList.Insert(0,Format('%5d%d', [CST_FLUSH ,flushEvent]));

   // add it to queue end tell the sender thread to read the queue
   criticalSection.Acquire ;
   setMessageList.Add(CommandList);
   setevent(MsgThread.hMessageReady) ;
   criticalSection.Leave ;

   // wait for the sender read events. (max 5 sec)
   WaitForSingleObject(flushEvent, FlushTimeOut) ;

   // release event
   CloseHandle (flushEvent) ;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

{ TMemberNode }

/// <summary>
/// Create a TMemberNode with no text in the 3 columns
/// </summary>
constructor TMemberNode.create;
begin
   create ('','','') ;
end;


//------------------------------------------------------------------------------

/// <summary>
/// Create a TMemberNode with a text for the first column
/// </summary>
/// <param name="col1">text of col1</param>
constructor TMemberNode.create (col1: string);
begin
   create (col1 , '' , '') ;
end;

//------------------------------------------------------------------------------

/// <summary>
/// Create a TMemberNode with text for the first 2 columns
/// </summary>
/// <param name="col1">text of col1</param>
/// <param name="col2">text of col2</param>
constructor TMemberNode.create (col1, col2: string);
begin
   create (col1 , col2 , '') ;
end;

//------------------------------------------------------------------------------

/// <summary>
/// Create a TMemberNode with text for the 3 columns
/// </summary>
/// <param name="col1">text of col1</param>
/// <param name="col2">text of col2</param>
/// <param name="col3">text of col3</param>
constructor TMemberNode.create (col1, col2, col3: string);
begin
   self.Col1 := col1 ;
   self.Col2 := col2 ;
   self.Col3 := col3 ;
end;

//------------------------------------------------------------------------------

/// clear the sub members
procedure TMemberNode.BeforeDestruction;
var
   c : integer ;
begin
   inherited;
   for c := 0 to length (Members)-1 do
      Members[c].free ;
end;

//------------------------------------------------------------------------------

/// <summary>
/// Add a member to the members list
/// </summary>
/// <param name="member"></param>
/// <returns>the TMember node to add</returns>
function TMemberNode.Add(member: TMemberNode): TMemberNode;
var
   le : integer ;
begin 
   le := length (Members) ;
   SetLength (Members, le+1);
   Members[le] := member ;
   result := member ;
end;

//------------------------------------------------------------------------------

function TMemberNode.Add(col1: string): TMemberNode;
begin
   result := Add (col1 , '' , '') ;
end;

//------------------------------------------------------------------------------

function TMemberNode.Add(col1, col2: string): TMemberNode;
begin
   result := Add (col1 , col2 , '') ;
end;
//------------------------------------------------------------------------------

function TMemberNode.Add(col1, col2, col3: string): TMemberNode;
var
   member : TMemberNode ;
begin

   member := TMemberNode.create (col1,col2,col3) ;
   Add (member) ;
   result := member ;
end;

//------------------------------------------------------------------------------

procedure TMemberNode.AddToStringList(CommandList: TStringList);
var
   node : TMemberNode ;
   c : integer ;

   procedure _AddToStringList(subnode : TMemberNode ; CommandList: TStringList);
   var
      SSnode : TMemberNode ;
      d : integer ;
   begin

      // create the member and set col1
      addCommand (CommandList, CST_CREATE_MEMBER, subnode.Col1);
      // add command if col2 and/or col3 exist
      if (subnode.Col2 <> '') then
         addCommand (CommandList, CST_MEMBER_COL2, subnode.Col2) ;

      if (subnode.Col3 <> '') then
         addCommand (CommandList, CST_MEMBER_COL3, subnode.Col3);

      // recursive add sub nodes, if any
      for d := 0 to length (subnode.Members)-1 do begin
         SSnode := subnode.Members[d] ;
         _AddToStringList (SSnode, CommandList);
         SSnode.Free ;   // once copied to Commandlist, free the node
         subnode.Members[d] := nil ;
      end ;
      setlength (subnode.Members,0) ;  // once copied to Commandlist, clear the array

      // close the member group
      addCommand (CommandList, CST_ADD_MEMBER) ;

  end;
begin

   // the root node node itself is not send for now.
   // Later we can send the 3 columns text to specify the header, if specfied.
   // the text should be like that : "Myfirstcol:150" where 150 is the column with
   for c := 0 to length (Members)-1 do begin
      node := Members[c] ;
      _AddToStringList (node,CommandList);
      node.Free ;   // once copied to Commandlist, free the node
      Members[c] := nil ;
   end ;
   setlength (Members,0) ;  // once copied to Commandlist, clear the array
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

{ TTraceNode }


constructor TTraceNode.create (ParentNode : TTraceNode ; generateUniqueId: boolean);
begin
   if (generateUniqueId) then
      Id := TTrace.CreateTraceID
   else
      Id := '' ;

   if (ParentNode = nil) then begin
      self.fIconIndex := CST_ICO_DEFAULT ;
      self.fEnabled := true ;
      self.fParentNodeId := '' ;
      self.fWinTrace := nil ;
   end else begin
      self.fIconIndex := ParentNode.IconIndex ;
      self.fEnabled := ParentNode.Enabled ;
      self.fParentNodeId := ParentNode.getlastcontextId() ;  // get parent node id (or node context id)
      self.fWinTrace := ParentNode.fWinTrace ;
   end ;
   fMembers := nil ; // created only when needed ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.BeforeDestruction;
begin
   if fContextList <> nil then
      fContextList.free ;  // own contexts

   if fMembers <> nil then
      fMembers.free ;
end;

//------------------------------------------------------------------------------

function TTraceNode.getWinTrace: IWinTrace;
begin
   result := fWinTrace
end;

//------------------------------------------------------------------------------

function TTraceNode.getId: string;
begin
   result := fid ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.setID (v: string);
begin
   fId := v ;
end;

//------------------------------------------------------------------------------

function TTraceNode.getEnabled: boolean;
begin
   result := fEnabled ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.setEnabled(v: boolean);
begin
   fEnabled := v ;
end;

//------------------------------------------------------------------------------

function TTraceNode.getIconIndex: integer;
begin
   result := fIconIndex ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.setIconIndex(v: integer);
begin
   fIconIndex := v ;
end;

//------------------------------------------------------------------------------

function TTraceNode.getTag: integer;
begin
   result := fTag ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.setTag(v: integer);
begin
   fTag := v ;
end;

//------------------------------------------------------------------------------

function TTraceNode.prepareNewNode (leftMsg : string ; newId : string) : TStringList ;
begin
   result := TStringList.create ;     // will be freed by the thread

   //addCommand (result, CST_NEW_NODE, Id) ;         // param : parent Node id (self)
   addCommand (result, CST_NEW_NODE, getlastcontextId()) ;  // param : parent Node id (self or context)

   addCommand (result, CST_TRACE_ID, newId) ;      // param : Node Id
   addCommand (result, CST_LEFT_MSG, leftMsg);     // param : left string
   addCommand (result, CST_ICO_INDEX, IconIndex) ; // param : icon index
end ;

//------------------------------------------------------------------------------

function TTraceNode.Send(leftMsg: variant): ITraceNode;
begin
   Send (GetVarValue (leftMsg)) ;
end;

//------------------------------------------------------------------------------

function TTraceNode.Send(leftMsg, RightMsg: variant): ITraceNode;
begin
   Send (GetVarValue (leftMsg),GetVarValue (RightMsg)) ;
end;

//------------------------------------------------------------------------------

function TTraceNode.Send (leftMsg: string): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
   pc : pchar ;
begin
   if (fEnabled = false) then begin
      result := self ;
      exit ;
   end ;

   // Ajust leftMsg in case of bstr
   pc := Pchar (leftMsg) ;
   leftMsg := pc ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (LeftMsg , node.Id) ;  // will be freed by the thread

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.Send (leftMsg, RightMsg: string): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
   pc : pchar ;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // Ajust leftMsg and rightMsg in case of bstr
   pc := Pchar (leftMsg) ;  leftMsg := pc ;
   pc := Pchar (rightMsg) ; rightMsg := pc ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread
   addCommand (CommandList, CST_RIGHT_MSG,rightMsg);    // param : right string

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

// send IDispatch
function TTraceNode.SendObject(leftMsg: string;  disp: IDispatch): ITraceNode;
begin
   result := SendObject (leftMsg , disp , TTrace.options.GetDefault) ;
end;

//------------------------------------------------------------------------------

function TTraceNode.SendObject(leftMsg: string; disp: IDispatch; flags: TraceDisplayFlags): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddObject (disp,flags);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

// send variant

{$IFDEF Delphi7}
function TTraceNode.SendObject (leftMsg: string; v: variant): ITraceNode;
begin
   result := SendObject (leftMsg , v , TTrace.options.GetDefault) ;
end;
{$ENDIF}

{$IFNDEF Delphi7}
function TTraceNode.SendObjectV (leftMsg: string; v: variant): ITraceNode;
begin
   result := SendObjectV (leftMsg , v , TTrace.options.GetDefault) ;
end;
{$ENDIF}

//------------------------------------------------------------------------------

{$IFDEF Delphi7}
function TTraceNode.SendObject (leftMsg: string; v: variant; flags: TraceDisplayFlags): ITraceNode;
{$ENDIF}
{$IFNDEF Delphi7}
function TTraceNode.SendObjectV (leftMsg: string; v: variant; flags: TraceDisplayFlags): ITraceNode;
{$ENDIF}

var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   {$IFDEF Delphi7}
   node.AddObject (v,flags);
   {$ENDIF}
   {$IFNDEF Delphi7}
   node.AddObjectV (v,flags);
   {$ENDIF}

   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

// send TObject
function TTraceNode.SendObject (leftMsg: string; Obj: TObject): ITraceNode;
begin
   result := SendObject (leftMsg , Obj , TTrace.options.GetDefault) ;
end ;

//------------------------------------------------------------------------------

function TTraceNode.SendObject (leftMsg: string; Obj: TObject; flags : TraceDisplayFlags): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddObject (Obj, flags);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendDump (leftMsg: string; Title: string; memory: pointer; ByteCount: integer): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddDump(Title, memory, ByteCount );
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendStrings (leftMsg: string; strings: TStrings): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddStrings (strings);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendStack (leftMsg: string; level : integer): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddStackTrace(level+1);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendCaller (leftMsg: string; level : integer): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddCaller(level+1);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendMethod (leftMsg: string; Meth : Pointer): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddMethod(Meth);
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SendValue (leftMsg: string; v: Variant): ITraceNode;
var
   node : TTraceNode ;
   CommandList : TStringList;
begin
   if (Enabled = false) then begin
      result := self ;
      exit ;
   end ;

   // create a node with same properties as "self" with new ID
   node := TTraceNode.create (self, true) ;
   result := node ;
   CommandList := prepareNewNode (leftMsg , node.Id) ;  // will be freed by the thread

   node.AddValue (v) ;
   node.fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);

end;

//------------------------------------------------------------------------------


// ITraceNode
function TTraceNode.CreateNodeEx: ITraceNodeEx;
var
   node : TTraceNode ;
begin
   node := TTraceNode.create (self,true);
   result := node ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
function TTraceNode.getLeftMsg: string;
begin
   result := fLeftMsg ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.setLeftMsg (v: string);
begin
   fLeftMsg := v ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
function TTraceNode.getRightMsg: string;
begin
   result := fRightMsg ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.setRightMsg (v: string);
begin
   fRightMsg := v ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
function TTraceNode.getParentNodeId: string;
begin
   result := fParentNodeId ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.setParentNodeId (v: string);
begin
   fParentNodeId := v ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.AddStrings (strings : TStrings);
var
   StringsGroup : TMemberNode ;
   i : integer ;
begin
   if (Enabled = false) then
      exit ;

   getMembers () ;  // ensure members array is create

   if (strings = nil) then begin
      fMembers.Add ('Null Object') ;
      exit ;
   end ;

   StringsGroup := TMemberNode.create ('Strings') ;
   fMembers.Add (StringsGroup) ;

   for I := 0 to Strings.Count -1 do
      StringsGroup.add (intTostr (i) , Strings.strings[i]) ;  // col3 empty

end ;

//------------------------------------------------------------------------------

procedure inner_AddValue (vElem : variant; UpperNode: TMemberNode; PreTitle : string) ;
var
   strType, strValue, IndexStr : string ;
   DimCount, LowBound, HighBound,c,tempint : integer ;
   ArrayNode : TMemberNode ;
   index : array [0..49] of LongInt ;   // 50 dimensions maximum
   cd : integer ;  // current dimension
   ARR : psafearray ;
   ArrElement : OleVariant ;
begin
   // arrays is a special case, we need to display all fields
   if (VarType(vElem) and VT_ARRAY) = VT_ARRAY then begin
      // get the safearray
      ARR := tagVARIANT(vElem).parray ;

      DimCount  := VarArrayDimCount(vElem);
      IndexStr := '' ;
      for c := 0 to DimCount-1 do begin
         LowBound  := VarArrayLowBound (vElem,c+1) ;
         HighBound := VarArrayHighBound(vElem,c+1) ;
         index [c] := LowBound ;
         IndexStr := IndexStr + '[' + inttostr (LowBound) + '..' + inttostr (HighBound) + ']' ;
      end ;
      tempint := VarType(vElem) ;
      tempint := tempint xor VT_ARRAY ;
      ArrayNode := UpperNode.Add (PreTitle + 'Array of ' + TraceTool.GetVariantType (tempint), IndexStr ) ;

      cd := DimCount-1 ;
      while cd >= 0 do begin       // loop 1
         IndexStr := '' ;
         for c := 0 to DimCount-1 do begin
            IndexStr := IndexStr + '[' + inttostr (index[c]) + ']' ;
         end ;

         if tempint = varVariant then begin
            OleCheck(SafeArrayGetElement(ARR, index, ArrElement));
         end else begin
            SafeArrayGetElement(ARR, index, TVarData(ArrElement).VPointer) ;
            TVarData(ArrElement).VType := tempint;
         end ;

         if (VarType(ArrElement) and VT_ARRAY) = VT_ARRAY then begin
            // the element is an array : recursive call
            inner_AddValue (ArrElement , ArrayNode, IndexStr + ' : ') ;
         end else begin
            strValue := GetVarValue (ArrElement, strType)  ;
            // print the indices , type , value
            ArrayNode.Add (IndexStr,strType,strValue) ;
         end ;

         cd := DimCount-1 ;
         while cd >= 0 do begin    // loop 2
            inc (index [cd]) ;
            if index [cd] > VarArrayHighBound (vElem,cd+1) then begin
               // out of bound. Reset current dimension and try to increment upper dimension
               index [cd] := VarArrayLowBound (vElem,cd+1) ;
               dec (cd) ;
            end else begin      // current dimension is not out of bound
               break ;          // loop 2
            end ;
         end ;
      end ;
      exit ;  // if array : stop
   end ;

   strValue := GetVarValue (vElem, strType)  ;
   UpperNode.Add (PreTitle+strType,strValue);

end ;

//------------------------------------------------------------------------------

procedure TTraceNode.AddValue(v: Variant);
begin
   if (Enabled = false) then
      exit ;

   getMembers () ;  // ensure members array is create
   try
      inner_AddValue (v, fMembers,'') ;
   except
   end ;
end;

//------------------------------------------------------------------------------
// add IDispatch
procedure TTraceNode.AddObject(disp: IDispatch);
begin
   AddObject(disp,TTrace.Options.GetDefault);
end;

//------------------------------------------------------------------------------

procedure TTraceNode.AddObject(disp: IDispatch; flags: TraceDisplayFlags);
var
   TypeInfo: ITypeInfo;
   intfName : WideString ;

   MyITypeInfo2 : ITypeInfo2 ;
   TypeAttr: PTypeAttr;
   strTemp : string ;
   Result: HResult ;

   MemberTypeDesc: PTypeDesc ;    // member type description
   FuncDesc: PFuncDesc;
   VarDesc: PVarDesc;

   GeneralInfo, Fields, Properties, Methods  : TMemberNode ;
   MemberValue : OleVariant ;
   MemberName: WideString;
   MemberTypeReal,MemberTypeDeclared : string ;
   MemberDoc, MemberDocCR : string ;
   MemberFlags : string ;
   strMembervalue : string ;
   cNames   : integer;
   DocString, helpcontext,helpFile, HelpString, HelpStringContext, HelpStringDll : widestring ;
   pc : Pchar ;

   //----------------------------------------------

   procedure GetProperty(Index: Integer; var Value: TVarData);
   var
      //Status: HResult;
      ExcepInfo: TExcepInfo;
      NullParams: TDISPPARAMS;
   begin
      with NullParams do begin
         rgvarg := nil;
         rgdispidNamedArgs := nil;
         cArgs := 0;
         cNamedArgs := 0;
      end;
      Value.VType := varEmpty;
      {Status :=} disp.Invoke(Index, GUID_NULL, 0,
               DISPATCH_PROPERTYGET, NullParams, @Value, @ExcepInfo, nil);
   end;

   //----------------------------------------------

   procedure GetMemberDoc (MemId : integer) ;
   begin
      // get Help String for the field (from ITypeInfo2)
      MemberDoc := '' ;
      MemberDocCR := '' ;
      If succeeded(TypeInfo.QueryInterface(ITypeInfo2,MyITypeInfo2)) then
      begin
         MyITypeInfo2.GetDocumentation2(MemId ,0, @HelpString, nil,  nil);
         MemberDoc := HelpString ;
         pc := Pchar (MemberDoc) ;
         MemberDoc := pc ;
         if MemberDoc <> '' then
            MemberDocCR := #10 + MemberDoc ;
      end;
   end ;
 
   //----------------------------------------------

   procedure AddGeneralInfo ;
   var
      i : integer ;
      TypeInfoInterface : ITypeInfo ;
    begin
      if not (ShowClassInfo in flags) then
         exit ;

      // get documenation for the interface (-1)
      OleCheck(TypeInfo.GetDocumentation(-1, @intfName, @DocString, @helpcontext, @helpFile));
      If succeeded(TypeInfo.QueryInterface(ITypeInfo2,MyITypeInfo2)) then
         MyITypeInfo2.GetDocumentation2(-1 ,0, @HelpString, @HelpStringContext,  @HelpStringDll);

      GeneralInfo := TMemberNode.create ('General info');
      Members.Add (GeneralInfo) ;

      // widestring to string, with possible BSTR problem
      strTemp := intfName ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      GeneralInfo.Add ('Interface name',strTemp ) ;

      for i := 0 to TypeAttr.cImplTypes -1 do begin
         OleCheck(TypeInfo.GetRefTypeInfo(i, TypeInfoInterface));
         OleCheck(TypeInfoInterface.GetDocumentation(MEMBERID_NIL, @intfName, nil,nil,nil));
         // widestring to string, with possible BSTR problem
         strTemp := intfName ;
         pc := Pchar (strTemp) ;
         strTemp := pc ;
         GeneralInfo.Add ('Implemented interface', strTemp ) ;
      end ;

      strTemp := DocString ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      if strTemp <> '' then
         GeneralInfo.Add ('DocString',strTemp ) ;

      if HelpString <> DocString then begin
         strTemp := HelpString ;
         pc := Pchar (strTemp) ;
         strTemp := pc ;
         if strTemp <> '' then
            GeneralInfo.Add ('HelpString',strTemp ) ;
      end ;

      strTemp := helpcontext ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      if strTemp <> '' then
         GeneralInfo.Add ('helpcontext',strTemp ) ;

      strTemp := helpFile ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      if strTemp <> '' then
         GeneralInfo.Add ('helpFile',strTemp ) ;

      strTemp := HelpStringContext ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      if strTemp <> '' then
         GeneralInfo.Add ('HelpStringContext',strTemp ) ;

      strTemp := HelpStringDll ;
      pc := Pchar (strTemp) ;
      strTemp := pc ;
      if strTemp <> '' then
         GeneralInfo.Add ('HelpStringDll',strTemp ) ;

   end ;

   //----------------------------------------------

   procedure AddFields ;
   var
      i : integer ;
   begin
      fields := TMemberNode.create ('Fields');
      for I := 0 to TypeAttr.cVars - 1 do
      begin
         OleCheck(TypeInfo.GetVarDesc(I, VarDesc));
         try
            // get documentation for the field. Only member name is needed
            OleCheck(TypeInfo.GetDocumentation(VarDesc.memid, @MemberName, nil, nil, nil));

            // get Help String for the field (from ITypeInfo2)
            GetMemberDoc (VarDesc.memid) ;

            try
               // get the field value and his real type
               VarClear(MemberValue);
               GetProperty(VarDesc.memid, TVarData(MemberValue));
               strMembervalue := TraceTool.GetVarValue (MemberValue,MemberTypeReal) ;
            except
            end ;

            MemberTypeDesc := @VarDesc.elemdescVar.tdesc ;
            MemberTypeDeclared := TraceTool.getVariantType (Typeinfo,MemberTypeDesc) ;

            if MemberTypeReal = 'IDispatch' then MemberTypeReal := ''
            else if MemberTypeReal = 'Null' then MemberTypeReal := ''
            else if MemberTypeReal = MemberTypeDeclared then MemberTypeReal := ''
            else MemberTypeReal := ' {' + MemberTypeReal + '}' ;

            MemberFlags := '' ;
            if (VarDesc.wVarFlags and VARFLAG_FREADONLY        <> 0 ) then MemberFlags := MemberFlags + '{Read Only} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FSOURCE          <> 0 ) then MemberFlags := MemberFlags + '{Source} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FBINDABLE        <> 0 ) then MemberFlags := MemberFlags + '{Bindable} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FREQUESTEDIT     <> 0 ) then MemberFlags := MemberFlags + '{Request Edit} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FDISPLAYBIND     <> 0 ) then MemberFlags := MemberFlags + '{Display Bind} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FDEFAULTBIND     <> 0 ) then MemberFlags := MemberFlags + '{Default Bind} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FHIDDEN          <> 0 ) then MemberFlags := MemberFlags + '{Hidden} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FRESTRICTED      <> 0 ) then MemberFlags := MemberFlags + '{Restricted} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FDEFAULTCOLLELEM <> 0 ) then MemberFlags := MemberFlags + '{Default Coll Elem} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FUIDEFAULT       <> 0 ) then MemberFlags := MemberFlags + '{UI Default} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FNONBROWSABLE    <> 0 ) then MemberFlags := MemberFlags + '{Non Browsable} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FREPLACEABLE     <> 0 ) then MemberFlags := MemberFlags + '{Replaceable} ' ;
            if (VarDesc.wVarFlags and VARFLAG_FIMMEDIATEBIND   <> 0 ) then MemberFlags := MemberFlags + '{Immediate Bind} ' ;

            fields.Add('[' + inttostr (VarDesc.memid) + '] ' + MemberName + ' ' + MemberFlags,
                       strMemberValue + MemberTypeReal,
                       MemberTypeDeclared + MemberDoc) ;

         finally
            TypeInfo.ReleaseVarDesc(VarDesc);
         end;
      end;
   end ;

   //----------------------------------------------

   procedure AddProperty ;
   var
      memberNode : TMemberNode ;
      memberfound : boolean ;
      j : integer ;
      PosCR : integer ;
      PreviousCol3, PreviousMemberType : string ;  // Property type and documentation
   begin
      MemberFlags := '' ;
      strMembervalue := '' ;
      MemberTypeReal := '' ;
      if (FuncDesc.invkind and INVOKE_PROPERTYGET <> 0 ) then begin
         try
            // get the property value and his real type
            VarClear(MemberValue);
            GetProperty(FuncDesc.memid, TVarData(MemberValue));
            strMembervalue := TraceTool.GetVarValue (MemberValue,MemberTypeReal) ;

            if MemberTypeReal = 'IDispatch' then MemberTypeReal := ''
            else if MemberTypeReal = 'Null' then MemberTypeReal := ''
            else if MemberTypeReal = MemberTypeDeclared then MemberTypeReal := ''
            else MemberTypeReal := ' {' + MemberTypeReal + '}' ;

         except
         end ;
         MemberFlags := MemberFlags + ' {get}' ;
      end ;

      if (FuncDesc.invkind and INVOKE_PROPERTYPUT    <> 0 ) then
         MemberFlags := MemberFlags + ' {put}' ;
      if (FuncDesc.invkind and INVOKE_PROPERTYPUTREF <> 0 ) then
         MemberFlags := MemberFlags + ' {put ref}' ;

      // first function parameter is the member type
      if FuncDesc.cParams > 0 then begin
         MemberTypeDeclared := TraceTool.GetVariantType(Typeinfo,@funcdesc.lprgelemdescParam[0].tdesc) ; 
      end ;

      // search for previous node with the same Id AND same declared value type
      memberfound := false ;
      For j := 0 to length(properties.Members) -1 do begin
         memberNode := properties.Members [j] ;
         if memberNode.Tag = FuncDesc.memid then begin
            PreviousCol3 := memberNode.Col3;  // Property type and documentation
            PosCR := pos (#10, PreviousCol3) ;
            if PosCR = 0 then // no doc
               PreviousMemberType := PreviousCol3
            else
               PreviousMemberType := copy (PreviousCol3,1 , posCR-1) ;

            if PreviousMemberType = MemberTypeDeclared then begin
               memberfound := true ;
               memberNode.Col1 := memberNode.Col1 + MemberFlags ;  // append new member flags
               memberNode.Col2 := memberNode.Col2 + strMembervalue + MemberTypeReal ; // add value if get is after the set
               break ;
            end ;
         end ;
      end ;

      if memberfound = false then begin
         memberNode := TMemberNode.create ('[' + inttostr (FuncDesc.memid) + '] ' + MemberName + MemberFlags ,
                        strMembervalue + MemberTypeReal, MemberTypeDeclared + MemberDocCR) ;
         memberNode.tag := FuncDesc.memid ;    // save member ID for later retreival
         properties.Add (memberNode) ;
      end ;
   end ;

   //----------------------------------------------

   procedure AddMethod ;
   var
      j : integer ;
      ParamName : WideString ;
      FuncParametersNames : array[0..30] of WideString;    // function parameters names
      ParamTypeDesc : PTypeDesc;    // param type description
      ParamTypeDeclared : string ;
      ParamElemDesc : TElemDesc ;
      lParamFlags: SmallInt;
      strParamflags : string;
      lParamDefaultValue: OLEVariant;
   begin
      // retrieve function parameters names
      TypeInfo.GetNames(funcdesc.memid,@FuncParametersNames,30,cNames);

      MemberName := MemberName + ' (' ;
      for j := 0 to FuncDesc.cParams-1 do
      begin
         ParamElemDesc := funcdesc.lprgelemdescParam[j] ;
         ParamTypeDesc := @ParamElemDesc.tdesc ;    // get TTypeDesc
         // get param name (the more easy task)
         ParamName := FuncParametersNames[j+1];
         // get param type
         ParamTypeDeclared := TraceTool.GetVariantType(Typeinfo,ParamTypeDesc) ;

         // lParamFlags tells whether a parameter is input/output/default values/optional, etc
         lParamFlags := ParamElemDesc.paramdesc.wParamFlags;
         strParamflags := '' ;
         // Check if Param has a default value
         if (lParamFlags and PARAMFLAG_FHASDEFAULT ) <> 0 then begin
            lParamDefaultValue := OLEVariant( ParamElemDesc.paramdesc.pparamdescex.varDefaultValue);
            strParamflags := strParamflags + '[Default = ' + TraceTool.GetVarValue (lParamDefaultValue) + ']' ;
         end;

         if (lParamFlags and PARAMFLAG_FIN    ) <> 0 then strParamflags := '[IN] ' + strParamflags ;
         if (lParamFlags and PARAMFLAG_FOUT   ) <> 0 then strParamflags := '[OUT] ' + strParamflags ;
         if (lParamFlags and PARAMFLAG_FLCID  ) <> 0 then strParamflags := '[LCID] ' + strParamflags ;
         if (lParamFlags and PARAMFLAG_FRETVAL) <> 0 then strParamflags := '[RETVAL] ' + strParamflags ;
         if (lParamFlags and PARAMFLAG_FOPT   ) <> 0 then strParamflags := '[OPT] ' + strParamflags ;

         if j = 0 then
            MemberName := MemberName + strParamflags + ParamTypeDeclared + ' ' + ParamName
         else
            MemberName := MemberName + ', ' + strParamflags + ParamTypeDeclared + ' ' + ParamName ;

      end;
      MemberName := MemberName + ')' ;
      methods.Add('[' + inttostr (FuncDesc.memid) + '] ' + MemberTypeDeclared , MemberName , Memberdoc) ;
   end ;

   //----------------------------------------------

   //function GetInterfaceForFunction (const inTypeinfo: ITypeInfo;
   //                                  inMemid:TDispID;
   //                                  href: cardinal;
   //                                  var IsGenericBaseInterface: Boolean): string;
   //var
   //LTypeInfoRef  : ITypeInfo;
   //InterfaceName : WideString;
   //ARefType      : HRefType;
   //begin
   //  InterfaceName := '' ;
   //  IsGenericBaseInterface := TRUE;
   //  Case (inMemId) of
   //    // M�thode pour trouver Interface des fonctions h�rit�es IUnknown,IDispatch (qui sont r�p�t�es dans tlb)
   //    1610612736,1610612737,1610612738 :
   //       begin
   //          InterfaceName:='IUnknown'; // IUnkn. QI, Addref, release,
   //       end ;
   //    1610678272,1610678273,1610678274,1610678275 :
   //       begin
   //          InterfaceName:='IDispatch'; // ...
   //       end ;
   //    else
   //    begin
   //      if succeeded(inTypeinfo.GetRefTypeInfo(href,LTypeInfoRef)) then begin
   //         LTypeInfoRef.GetDocumentation(-1,@InterfaceName,nil,nil,nil);
   //      end ;
   //      IsGenericBaseInterface:=False;
   //    end;
   //  end;
   //  Result:=InterfaceName;
   //end;


   procedure AddMethodsAndProperties ;
   var
      i : integer ;
      //InheritStr : string ;
      //isGenericInterface: Boolean ;
   begin
      Properties  := TMemberNode.create ('Properties');
      methods     := TMemberNode.create ('Methods');
      for I := 0 to TypeAttr.cFuncs - 1 do
      begin
         OleCheck(TypeInfo.GetFuncDesc(I, FuncDesc));
         try
            // bypass function from IDispatch and IUnknown
            if FuncDesc.memid >= $60000000 then
               continue ;

            // get documentation for the method. Only member name is needed
            OleCheck(TypeInfo.GetDocumentation(FuncDesc.memid, @MemberName, nil, nil, nil)); // @DocString, @helpcontext, @helpFile

            //if (FuncDesc.elemdescFunc.tdesc.vt=VT_PTR) then
            //   InheritStr:=GetInterfaceForFunction(TypeInfo,FuncDesc.memid,FuncDesc.elemdescFunc.tdesc.ptdesc.hreftype,isGenericInterface)
            //else
            //   InheritStr:=GetInterfaceForFunction(TypeInfo,FuncDesc.memid,FuncDesc.elemdescFunc.tdesc.hreftype,isGenericInterface);
            //MemberName := InheritStr + ':' + MemberName ;

            // get Help String for the method (from ITypeInfo2)
            GetMemberDoc (FuncDesc.memid) ;

            MemberTypeDesc := @FuncDesc.elemdescFunc.tdesc ;
            MemberTypeDeclared := TraceTool.GetVariantType (Typeinfo,MemberTypeDesc) ;

            // check if the method is a property
            if ((FuncDesc.invkind and INVOKE_PROPERTYGET    <> 0 ) or
                (FuncDesc.invkind and INVOKE_PROPERTYPUT    <> 0 ) or
                (FuncDesc.invkind and INVOKE_PROPERTYPUTREF <> 0 )) then begin
               AddProperty () ;
            end else if ShowMethods in flags then begin  // METHOD
               AddMethod ();
            end;
         finally
            TypeInfo.ReleaseFuncDesc(FuncDesc);
         end;
      end;
   end ;

   //----------------------------------------------

begin   // procedure AddObject

   getMembers ;
   try
      Result := disp.GetTypeInfo(0,0,TypeInfo) ;
      if not Succeeded(Result) then
         TypeInfo := nil ;  // ensure TypeInfo don't contain bad result
   except
   end ;

   if TypeInfo = nil then begin
      members.Add ('No infos') ;
      Exit;
   end ;

   OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
   try
      // add general info (interface name, help strings, ...)
      AddGeneralInfo ();

      // add fields
      AddFields () ;
      if length (Fields.Members) <> 0 then
         Members.Add (Fields)
      else
         Fields.free ;

      // add Methods and properties
      AddMethodsAndProperties() ;
      if length (Properties.Members) <> 0 then
         Members.Add (Properties)
      else
         Properties.free ;

      if length (methods.Members) <> 0 then
         Members.Add (methods)
      else
         methods.free ;

   finally
      TypeInfo.ReleaseTypeAttr(TypeAttr);
   end;
end;

//------------------------------------------------------------------------------
// add variant

{$IFDEF Delphi7}
procedure TTraceNode.AddObject (v: variant);
begin
   AddObject(v,TTrace.Options.GetDefault);
end;
{$ENDIF}

{$IFNDEF Delphi7}
procedure TTraceNode.AddObjectV (v: variant);
begin
   AddObjectV (v,TTrace.Options.GetDefault);
end;
{$ENDIF}

//------------------------------------------------------------------------------

{$IFDEF Delphi7}
procedure TTraceNode.AddObject (v: variant; flags: TraceDisplayFlags);
{$ENDIF}
{$IFNDEF Delphi7}
procedure TTraceNode.AddObjectV (v: variant; flags: TraceDisplayFlags);
{$ENDIF}
var
   strType : string ;
   strValue : string ;
begin
   if VarType(V) = VT_DISPATCH then begin
      {$IFDEF Delphi7}
      AddObject (IDispatch(TVarData(v).VDispatch)) // add idispatch
      {$ENDIF}
      {$IFNDEF Delphi7}
      AddObjectV (IDispatch(TVarData(v).VDispatch)) // add idispatch
      {$ENDIF}
   end else begin
      getMembers ;
      strValue := GetVarValue (v , strType) ;
      Members.Add (strType,strValue) ;             // add type and value
   end ;
end;

//------------------------------------------------------------------------------

// add TObject
procedure TTraceNode.AddObject (Obj: TObject);
begin
   AddObject(Obj,TTrace.Options.GetDefault);
end ;

//------------------------------------------------------------------------------

procedure TTraceNode.AddObject (Obj: TObject; flags : TraceDisplayFlags);
var
   Collection : TCollection ;
   CollectionItemClass : TCollectionItemClass ;
   ObjectGroup : TMemberNode ;
begin
   if (Enabled = false) then
      exit ;

   getMembers () ;  // ensure members array is create

   if (Obj = nil) then begin
      fMembers.Add ('Null Object') ;
      exit ;
   end ;

   // detect recursive call to SendObject when DisplayFields try to get fields values
   if SendObjectRecursiveStatus > 0 then begin
      fMembers.add ('Object adress' , '$' + inttohex (integer(obj),2) ) ;
      fMembers.add ('Class Name' , obj.className ) ;
      fMembers.add ('Error : A Field Read method call AddObject().') ;
      fMembers.add ('->Possible recursive call is stopped now') ;
      SendObjectRecursiveStatus := 2 ; // tell the calling SendObject that a recursive call was stopped
      exit ;
   end ;

   SendObjectRecursiveStatus := 1 ;

   try

      // no quick info to display (Object.ToString)
      //-------------------------------------------

      // AddValue(obj);
      
      // Class information
      // ------------------

      if ShowClassInfo in flags then begin
         ObjectGroup := TMemberNode.create ('Class information') ;
         fMembers.Add(ObjectGroup) ;

         ObjectGroup.add ('Object adress' , '$' + inttohex (integer(obj),2) ) ;
         ObjectGroup.add ('Class Name' , obj.className ) ;

         if obj is TComponent then
            ObjectGroup.add ('TComponent name' , TComponent(obj).name ) ;

         if obj is TMenu then
            ObjectGroup.add ('Is TMenu' , 'True' ) ;

         if obj is TMenuItem then
            ObjectGroup.add ('Is TMenu Item' , 'True' ) ;

         if OBJ is TCollection then begin
            Collection := TCollection (obj) ;
            CollectionItemClass := Collection.ItemClass ;
            ObjectGroup.add ('Is TCollection' , 'ItemClass' , CollectionItemClass.ClassName) ;
         end ;
      end ;

      // no type information for that object.
      if PTypeInfo(obj.ClassInfo) = nil then begin
         fMembers.Add('No Type information for that object') ;
         fMembers.Add('Derive your object from TPersistent or') ;
         fMembers.Add('use {$M+} and {$M-} directives (see TPersistent)') ;
         exit ;
      end ;

      // fields and events
      // -----------------
      DisplayFields (obj, flags);

      // functions
      //-----------
      if ShowMethods in flags then
         DisplayMethods (obj, flags) ;

      // inherit classes and interfaces
      //-------------------------------
      if ShowClassInfo in flags then
         DisplayBases (obj, flags) ;

      if SendObjectRecursiveStatus = 2 then begin
         fMembers.add ('Warning : One or more Field Read method called AddObject.') ;
         fMembers.add ('->Possible recursive call was stopped') ;
      end ;

   finally
      SendObjectRecursiveStatus := 0 ;
   end ;
end;

//------------------------------------------------------------------------------
// from delphi 6
function GetPropList(TypeInfo: PTypeInfo; out PropList: PPropList): Integer;
begin
  Result := GetTypeData(TypeInfo)^.PropCount;
  if Result > 0 then
  begin
    GetMem(PropList, Result * SizeOf(Pointer));
    GetPropInfos(TypeInfo, PropList);
  end;
end;

// for delphi 5
//function GetPropList(TypeInfo: PTypeInfo; PropList: PPropList): Integer;
//var
//  I, Count: Integer;
//  PropInfo: PPropInfo;
//  TempList: PPropList;
//begin
//  Result := 0;
//  Count := GetTypeData(TypeInfo)^.PropCount;
//  if Count > 0 then
//  begin
//    GetMem(TempList, Count * SizeOf(Pointer));
//    try
//      GetPropInfos(TypeInfo, TempList);
//      for I := 0 to Count - 1 do
//      begin
//        PropInfo := TempList^[I];
//        if PropList <> nil then PropList^[Result] := PropInfo;
//        Inc(Result);
//      end;
//    finally
//      FreeMem(TempList, Count * SizeOf(Pointer));
//    end;
//  end;
//end;

//------------------------------------------------------------------------------

procedure TTraceNode.DisplayBases (AObject: TObject; flags : TraceDisplayFlags);
var
   Group : TMemberNode ;
   ClassPtr: TClass;
   IntfTable: PInterfaceTable;
   Entry : PInterfaceEntry ;
   I: Integer;
   interfacesNames : string ;
begin
   Group := nil ;
   ClassPtr := AObject.ClassType;
   while ClassPtr <> nil do begin
      if classPtr = TObject then
         break ;

      interfacesNames := '' ;
      IntfTable := ClassPtr.GetInterfaceTable;

      if IntfTable <> nil then begin
          for I := 0 to IntfTable.EntryCount-1 do begin
            Entry := @IntfTable.Entries[I];
             if (interfacesNames = '') then
               interfacesNames := GUIDToString(Entry.IID)
            else
               interfacesNames := interfacesNames + ',' + GUIDToString(Entry.IID) ;
          end;
      end ;

      if Group = nil then begin
          Group := TMemberNode.create ('Classes and interfaces') ;
          fMembers.Add(Group) ;
      end ;
      Group.add (ClassPtr.ClassName, interfacesNames) ;

      ClassPtr := ClassPtr.ClassParent;
   end;
end ;

//------------------------------------------------------------------------------

procedure TTraceNode.DisplayMethods (AObject: TObject; flags : TraceDisplayFlags);
var
   MethodGroup : TMemberNode ;
   VMT: Pointer;
   MethodInfo: Pointer;
   Count: Integer;
   r : PMethodInfoHeader ;
   strMethodInfo : string ;
begin
   MethodGroup := nil ;
   VMT := PPointer(AObject)^;
   repeat
      MethodInfo := PPointer(Integer(VMT) + vmtMethodTable)^;
      if MethodInfo <> nil then
      begin
         // Scan method table for the method
         Count := PWord(MethodInfo)^;
         Inc(Integer(MethodInfo), 2);
         while Count > 0 do
         begin
            r := MethodInfo;
            // add the method
            if MethodGroup = nil then begin
               MethodGroup := TMemberNode.create ('Methods') ;
               fMembers.Add(MethodGroup) ;
            end ;
            if assigned (SendMethodProc) then
               strMethodInfo := SendMethodProc (nil,r^.Addr) ;
            MethodGroup.add (r^.Name , strMethodInfo) ;

            Inc(Integer(MethodInfo), PMethodInfoHeader(MethodInfo)^.Len);
            Dec(Count);
         end;
      end;
      // Find the parent VMT
      VMT := PPointer(Integer(VMT) + vmtParent)^;
      if VMT = nil then
         break ;
      VMT := PPointer(VMT)^;
   until False;

end ;

//------------------------------------------------------------------------------

function ParamsToString(P:Pointer;Count:Integer):String;
type
  PByte=^Byte;
  PShortString=^ShortString;
var
  i:Integer;
  S:String;
  Flags:TParamFlags;
begin
  Result:='(';
  for i:=1 to Count do
    begin
      if i>1 then S:='; ' else S:='';
      Flags:=TParamFlags(P^);
      if pfVar in Flags then S:=S + ' var ';
      if pfConst in Flags then S:=S + ' const ';
      P:=Pointer(Integer(P)+1);    // P is the param name
      S:=S+PShortString(P)^;
      P:=Pointer(Integer(P)+Length(PShortString(P)^)+1);
      S:=S+': '+PShortString(P)^;
      P:=Pointer(Integer(P)+Length(PShortString(P)^)+1);
      Result:=Result+S;
    end;
  Result:=Result+')';
end;

//------------------------------------------------------------------------------

function MethodSyntax(P:PPropInfo):String;
var
  PList:Pointer;
  PCount:Integer;
  TypeData:TTypeData;
begin
  Result:='';
  if not Assigned(P) or (P^.PropType^.Kind<>tkMethod) then exit;
  TypeData:=GetTypeData(P^.PropType^)^;
  PList:=@(TypeData.ParamList);
  PCount:=TypeData.ParamCount;
  Result:=ParamsToString(PList,PCount)+' of object;';
  case TypeData.MethodKind of
    mkProcedure:Result:='procedure '+Result;
    mkFunction:Result:='function '+Result;
    mkSafeProcedure:Result:='procedure '+Result+' safecall;';
    mkSafeFunction:Result:='function '+Result+' safecall;';
  end;
  Result:=P^.PropType^.Name+': '+Result;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.DisplayFields (AObject: TObject; flags : TraceDisplayFlags);
var
   FieldGroup : TMemberNode ;
   EventGroup : TMemberNode ;
   obj, subObj : TObject ;
   I, Count: Integer;
   TypeInfo : PTypeInfo ;
   PropInfo: PPropInfo;
   TempList: PPropList;
   VariantPropValue : variant ;
   intPropValue : LongInt ;

   Prop_Name      : string ;
   Prop_Type      : string ;
   Prop_ClassType : string ;
   Prop_Value     : string ;

   //------------------------------------------------------
   // add to the property group
   procedure AddToFieldGroup () ;
   begin
      if ShowFields in flags then begin
         if FieldGroup = nil then begin
            FieldGroup := TMemberNode.create ('Properties') ;
            fMembers.Add(FieldGroup) ;
         end ;
         FieldGroup.add (prop_name , prop_type, prop_value) ;
      end ;
   end ;

   //------------------------------------------------------
   // add to the event group
   procedure AddToEventGroup () ;
   begin
      if ShowEvents in flags then begin
         if EventGroup = nil then begin
            EventGroup := TMemberNode.create ('Events') ;
            fMembers.Add(EventGroup) ;
         end ;
         EventGroup.add (prop_name , prop_type, prop_value) ;
      end ;
   end ;
   //------------------------------------------------------

begin
   TypeInfo := PTypeInfo(AObject.ClassInfo) ;
   if TypeInfo = nil then
      exit ;
   obj := AObject ;
   Count := GetPropList(typeInfo, TempList);   // if Count is zero , FreeMem(TempList) don't have to be called
   if Count > 0 then
   try
      FieldGroup := nil ;
      EventGroup := nil ;

      for I := 0 to Count - 1 do begin
         PropInfo := TempList^[I];

         Prop_Name := PropInfo.Name ;
         Prop_Type := GetPropertyTypeString (PropInfo^.PropType^.Kind);
         Prop_ClassType := '' ;
         Prop_Value := '' ;

         case PropInfo^.PropType^.Kind of
            tkInteger,tkString,tkWChar,tkLString,tkWString,tkInt64 :
               begin
                  if obj <> nil then begin
                     VariantPropValue := GetPropValue(AObject, PropInfo.Name,true) ;
                     Prop_Value := VariantPropValue ;
                  end ;
                  AddToFieldGroup() ;
               end ;
            tkClass :
               begin
                  intPropValue := GetOrdProp(obj, PropInfo) ;
                  if intPropValue = 0 then  // the property point to a nil TObject
                     Prop_Value := 'nil'
                  else begin
                     subObj := TObject (intPropValue);
                     // get the className of the object property
                     //Prop_ClassType := subObj.ClassName ;
                     Prop_Type := subObj.ClassName ;
                     Prop_Value := intToStr (intPropValue) ;  // value is the adresse reference
                  end;
                  AddToFieldGroup() ;
               end ;
            tkMethod :   // events 
               begin
                  prop_type := MethodSyntax (PropInfo) ;
                  if obj <> nil then begin
                     intPropValue := GetOrdProp(AObject, PropInfo) ;  // how to handle that value ? negative or small
                     if intPropValue = 0 then
                        Prop_Value := 'nil'
                     else
                        Prop_Value := 'Pointer' ;
                  end ;
                  AddToEventGroup() ;
               end ;
            else
               begin  // enumeration, ...
                  if obj <> nil then begin
                     VariantPropValue := GetPropValue(AObject, PropInfo.Name,true) ;
                     Prop_Value := VariantPropValue ;
                  end ;
                  AddToFieldGroup() ;
               end ;
         end ;
      end;  // for each prop
   finally
      FreeMem(TempList);
   end;
end ;

//------------------------------------------------------------------------------

function TTraceNode.GetPropertyTypeString (TypeKind : TypInfo.TTypeKind) : string ;
begin
  result := '' ;
  case TypeKind of
     tkUnknown      : result := 'Unknown' ;
     tkInteger      : result := 'Integer' ;
     tkChar         : result := 'Char' ;
     tkEnumeration  : result := 'Enumeration' ;
     tkFloat        : result := 'Float' ;
     tkString       : result := 'String' ;
     tkSet          : result := 'Set' ;
     tkClass        : result := 'Class' ;
     tkMethod       : result := 'Method' ;
     tkWChar        : result := 'WChar' ;
     tkLString      : result := 'LString' ;
     tkWString      : result := 'WString' ;
     tkVariant      : result := 'Variant' ;
     tkArray        : result := 'Array' ;
     tkRecord       : result := 'Record' ;
     tkInterface    : result := 'Interface' ;
     tkInt64        : result := 'Int64' ;
     tkDynArray     : result := 'DynArray' ;
  end ;
end ;



//------------------------------------------------------------------------------

// sample use :
//   // send Screen object separatly
//   SentObj := TObjectList.create (false);  // object list don't own objects
//   for c := 0 to Screen.FormCount -1 do
//      AddScannedObject (SentObj,'', CST_ICO_FORM, Screen.Forms[c]) ;
//   SentObj.free ;


procedure TTraceNode.AddScannedObject(Obj: TObject);
begin
   // to do ...
end;


//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.AddDump(Title: string; memory: pointer; ByteCount: integer);
var
   c,d, beginLine : integer ;
   hexa_representation : string ;
   Str_representation : string ;
   Ptr : Pchar ;
   OneIntChar : integer ;
   OneChar : char ;
   DumpGroup : TMemberNode ;
begin
   if (Enabled = false) then
      exit ;

   getMembers () ;  // ensure members array is create

   DumpGroup := TMemberNode.create (Title) ;
   fMembers.Add (DumpGroup) ;

   Ptr := memory ;
   c := 0 ;
   while c <= ByteCount-1 do begin
      d := 0 ;

      beginLine := c ;
      hexa_representation := '' ;
      Str_representation := '' ;
      while (c <= ByteCount-1) and (d < 16) do begin
         OneIntChar := integer(Ptr^) ;
         hexa_representation := hexa_representation + intTohex (OneIntChar,2) + ' ' ;

         if OneIntChar = 255 then
            OneIntChar := 0 ;
         OneChar := chr(OneIntChar) ;

         if AnsiCharTypes[OneChar] and (C1_ALPHA or C1_PUNCT or C1_BLANK or C1_XDIGIT or C1_DIGIT) <> 0 then
            Str_representation := Str_representation + OneChar
         else
            Str_representation := Str_representation + '.' ;

         inc (d) ;
         inc (c) ;
         Inc (Ptr) ;
      end ;
      if hexa_representation <> '' then
         DumpGroup.add (inttohex (beginLine,6) , hexa_representation , Str_representation) ;
   end ;

end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.AddCaller (Level: integer);
begin
   if assigned (SendCallerProc) then
      SendCallerProc (self, Level+1) ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
procedure TTraceNode.AddMethod (Meth: Pointer);
begin
   if assigned (SendMethodProc) then
      SendMethodProc (self,Meth) ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
// In order to use stack info
// add the StackTrace unit after Tracetool in the use list
// and set "Debug information" (project compiler options)
// and set "Detailed map file" (project linker options)

procedure TTraceNode.AddStackTrace (Level: integer);
begin
   // the stack trace is performed in the supplied StackTrace unit (use jedi code lib)
   if assigned (SendStackProc) then
      SendStackProc (self,level+1) ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
// return the member array.
// That let user code to add infos in the member pane
function TTraceNode.getMembers: TMemberNode;
begin
   if (fMembers = nil) then
      fMembers := TMemberNode.create ;
   result := fMembers ;
end;

//------------------------------------------------------------------------------

// ITraceNodeEx
function TTraceNode.Send: ItraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_NEW_NODE  , fParentNodeId) ; // param : parent Node id
   addCommand (CommandList, CST_TRACE_ID  , Id) ;            // param : guid
   if LeftMsg <> '' then
      addCommand (CommandList, CST_LEFT_MSG  , LeftMsg);     // param : left string
   if RightMsg <> '' then
      addCommand (CommandList, CST_RIGHT_MSG , RightMsg);    // param : right string
   addCommand (CommandList, CST_ICO_INDEX , IconIndex) ;     // param : the icon index

   // check if some "addxxx" functions has create the Members.
   if fMembers <> nil then
      fMembers.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);

end;

//------------------------------------------------------------------------------
// ITraceNodeEx

function TTraceNode.Resend: ItraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   CommandList := TStringList.create ;  // will be freed by the thread
   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_RIGHT_MSG,RightMsg);      // param : right string
   addCommand (CommandList, CST_LEFT_MSG,leftMsg);        // param : left string

   // resend members or not ?
   // if yes, does the new members must override already sent members or must be appened ?
   // For now, the resend just do nothing with members....
   // Members.AddToStringList (CommandList) ;   // convert all groups and nested items/group to strings

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------
// ITraceNode

function TTraceNode.Resend(leftMsg, RightMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_RIGHT_MSG,RightMsg);      // param : right string
   addCommand (CommandList, CST_LEFT_MSG,leftMsg);        // param : left string
   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

// ITraceNode
function TTraceNode.ResendLeft(leftMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_LEFT_MSG,leftMsg);        // param : left string
   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

// ITraceNode
function TTraceNode.ResendRight(RightMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_RIGHT_MSG,RightMsg);      // param : right string
   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.AppendLeft(LeftMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_APPEND_LEFT_MSG,leftMsg);        // param : left string

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.AppendRight(RightMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_APPEND_RIGHT_MSG,RightMsg);        // param : left string

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.Append(LeftMsg, RightMsg: string): ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be modified (for now)') ;
   CommandList := TStringList.create ;  // will be freed by the thread

   addCommand (CommandList, CST_USE_NODE,id);             // param : the node that receive the string
   addCommand (CommandList, CST_APPEND_RIGHT_MSG,RightMsg);      // param : right string
   addCommand (CommandList, CST_APPEND_LEFT_MSG,LeftMsg);        // param : left string

   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.Show: ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be used') ;
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_FOCUS_NODE,id);              // param : the node that receive focus
   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.SetSelected: ITraceNode;
var
   CommandList : TStringList;
begin
   result := self ;
   if Enabled = false then
      exit ;

   if id = '' then
      raise exception.create ('Node Id is null, root node cannot be used') ;
   CommandList := TStringList.create ;  // will be freed by the thread
   addCommand (CommandList, CST_SELECT_NODE,id);              // param : the node that receive focus
   TTrace.SendToClient (CommandList, fWinTrace);
end;

//------------------------------------------------------------------------------

function TTraceNode.getLastContext : TNodeContext;
var
   c : integer ;
   thId: DWORD ;
begin
   result := nil ;
   if fContextList = nil then
      exit ;

   thId := GetCurrentThreadId() ;
   for c := 0 to fContextList.Count-1 do begin
      result := TNodeContext (fContextList.Items[c]) ;
      if result.ThreadId = thId then
         exit ;
   end ;
   result := nil ;
end;

//------------------------------------------------------------------------------

function TTraceNode.getLastContextId : string;
var
   context : TNodeContext ;
begin
   context := getLastContext () ;
   if context = nil then
      result := self.id
   else
      result := context.NodeId ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.PushContext (NewContext : TNodeContext) ;
begin
   if fContextList = nil then
      fContextList := TObjectList.create (true) ;  // own context

   fContextList.Insert (0, NewContext) ;
end ;

//------------------------------------------------------------------------------

procedure TTraceNode.deleteLastContext ;
var
   c : integer ;
   context : TNodeContext ;
   thId: DWORD ;
begin
   if fContextList = nil then   // should not happens
      exit ;

   thId := GetCurrentThreadId() ;
   for c := 0 to fContextList.Count-1 do begin
      context := TNodeContext (fContextList.Items[c]) ;
      if context.ThreadId = thId then begin
         fContextList.Delete (c) ;
         exit ;
      end ;
   end ;
end ;

//------------------------------------------------------------------------------

procedure TTraceNode.Indent(leftMsg: string);
var
   currentThreadId : integer ;
   LastContext , NewContext : TNodeContext ;
   CommandList : TStringList ;
begin
   currentThreadId := GetCurrentThreadId() ;
   NewContext := TNodeContext.create ;
   NewContext.ThreadId := currentThreadId ;

   LastContext := getLastContext () ;
   NewContext.NodeId := TTrace.CreateTraceID ;
   CommandList := TStringList.create ;     // will be freed by the sender thread

   if LastContext = nil then begin
      NewContext.level := 1 ;
      addCommand (CommandList, CST_NEW_NODE, self.Id) ;              // param : parent Node id
   end else begin  // context already exist
      NewContext.level := LastContext.level + 1 ;
      addCommand (CommandList, CST_NEW_NODE, LastContext.NodeId) ;   // param : parent Node id 
   end ;

   addCommand (CommandList, CST_TRACE_ID, NewContext.NodeId) ;   // param : Node Id
   addCommand (CommandList, CST_LEFT_MSG, leftMsg);              // param : left string
   addCommand (CommandList, CST_ICO_INDEX, IconIndex) ;          // param : icon index
   TTrace.SendToClient (CommandList, fWinTrace);

   PushContext (NewContext) ;
end;

//------------------------------------------------------------------------------

procedure TTraceNode.UnIndent(leftMsg: string);
begin
   deleteLastContext () ;
   if leftMsg <> '' then begin
      Send (leftMsg) ;
   end ;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

{ TTraceOptions }

procedure TTraceOptions.SetProcessName(const Value: boolean);
var
   buf : array [0..MAX_PATH] of char ;
begin
   if Value = true then begin
      buf [GetModuleFileName(HInstance, buf, sizeof (buf))] := #0 ;
      fProcessFileName := buf ;
   end else begin
      fProcessFileName := '' ;
   end ;
end;

//------------------------------------------------------------------------------

constructor TTraceOptions.Create;
begin
   fSendMode := tmWinMsg ;
   fSendFunctions := false ;
   fSendInherited := false ;
   fSendEvents    := false ;
   fProcessFileName := '' ;
   fSocketPort := 8090 ;
   fSocketHost := '127.0.0.1' ;
end;

//------------------------------------------------------------------------------

function TTraceOptions.GetDefault: TraceDisplayFlags;
begin
   // display at least public (inherited) fields and properties
   // Privat members are discarded
   result := [ShowFields] ; // ShowModifiers , ShowInheritedMembers

   if fSendEvents then
      result := result + [ShowEvents] ;

   if fSendFunctions then
      result := result + [ShowMethods] ;

   //if fSendInherited then
   //   result := result + [ShowInheritedMembers] ;
end;

//------------------------------------------------------------------------------

procedure TTraceOptions.setSocketHost(const Value: string);
begin
   if assigned (AlternateSend) then
      AlternateSend.SetHost(value);
end;

//------------------------------------------------------------------------------

procedure TTraceOptions.setSocketPort(const Value: integer);
begin
   if assigned (AlternateSend) then
      AlternateSend.SetPort(value);
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

{ TMsgThread }

procedure TMsgThread.Execute;
var
   dwHandleSignaled:   DWORD;
   flushEvent : THandle ;
   CDS: TCopyDataStruct;
   DebugWin: hWnd;
   MessageString: string;
   c,i : integer ;
   tot : integer ;

   tempList : TObjectList ; // used for swap of list
   CommandList : TStringList ;
   HandlesToWaitFor: array[0..1] of THandle;

begin
   // We will be waiting on these objects.
   HandlesToWaitFor[0] := hCloseEvent;
   HandlesToWaitFor[1] := hMessageReady ;

    // This is the main loop.  Loop until we break out.
   while True do
   begin
       // wait for a message or the CloseEvent. The Update will wait max 60 sec
       dwHandleSignaled := WaitForMultipleObjects({count} 2, {handelList} @HandlesToWaitFor, {bwaitall} False, {millisec} 60000);

       case dwHandleSignaled of
          WAIT_OBJECT_0:     // CloseEvent signaled
             begin
                 //senddebug ('time to exit') ;
                 break ;  // ResetEvent
             end;

           WAIT_OBJECT_0 + 1: // hMessageReady signaled : New message was received.
              begin
                 try  // ensure that the trace will not generate error

                    criticalSection.Acquire ;

                    //stat traces
                    //swap := swap + 'Begin at ' + FormatDateTime('hh:mm:ss:zzz',now) + ' : ' + inttostr(setMessageList.Count) ;

                    // swap the 2 list to release the lock as fast as possible
                    tempList := getMessageList ;          // getMessageList is the empty list
                    getMessageList := setMessageList ;    // setMessageList is the list of message to send
                    setMessageList := tempList ;
                    // let other thread setting the ready flag
                    ResetEvent(hMessageReady) ;
                    criticalSection.Leave ;

                    // loop over the getMessageList

                    for c := 0 to getMessageList.Count-1 do begin
                       if not (getMessageList.Items[c] is TStringList) then
                          continue ;
                       CommandList := TStringList (getMessageList.Items[c]) ;

                       tot := 0 ;
                       MessageString := '' ;
                       for i := 0 to CommandList.Count -1 do begin
                          tot := tot + Length(CommandList.Strings[i]) + 1 ;
                          MessageString := MessageString + CommandList.Strings[i] + #0;
                       end ;
                       MessageString := MessageString + #0 ;
                       inc (tot);
                       CommandList.free ;

                       // special case : the CST_FLUSH is handled by the sender thread and is not send to server
                       if strtoint(Copy (MessageString,0,5)) = CST_FLUSH then begin
                          flushEvent := strtoint(Copy (MessageString,5,20)) ;
                          SetEvent(flushEvent);
                          continue ;
                       end ;

                       if TTrace.Options.fSendMode = tmAlternate then begin
                          try
                             if assigned (AlternateSend) then
                                AlternateSend.Send (MessageString , tot) ;
                          finally
                          end ;

                       end else begin  // windows messages

                          DebugWin := StartDebugWin;
                          if DebugWin <> 0 then begin
                            //SetLength(MessageString, 256);  // force 256, because result are put back at same place
                            CDS.cbData := tot ;
                            CDS.dwData := WMD ;   // identification code 'WebModuleDebug'
                            CDS.lpData := pchar (MessageString); // no need to add #0, because String are null terminated

                            SendMessage(DebugWin, WM_COPYDATA, 0, LParam(@CDS));  //WParam(Application.Handle)

                          end;
                       end ;
                    end ;  // next message
                    getMessageList.Clear ;
                    // stat trace
                    // swap := swap + 'End at ' + FormatDateTime('hh:mm:ss:zzz',now) ;
                 except
                 end ;

              end;

           WAIT_FAILED:       // Wait failed.  Shouldn't happen.
              begin
                 //sendDebugEX ('WAIT_FAILED: ' + IntToStr(GetLastError),mtError);
                 break;
              end;

           WAIT_TIMEOUT:
              begin
                 // time out, perform some jobs...
                 // ...
                 continue ;
              end ;

           else                // This case should never occur.
              begin
                 //sendDebugEX( 'Unexpected Wait return value ' +IntToStr(dwHandleSignaled),mtError );
                 break;
              end;
       end;



    end; {main loop}

end;

//------------------------------------------------------------------------------

{
SocketHost=LocalHost
SocketPort=8090
DebugEnabled=true
WarningEnabled=true
ErrorEnabled=true
SendMode=socket
SendProcessName=false
SendFunctions=false
SendInherited=false
SendEvents=false
}

procedure LoadConfig ;
var
   configString : TStringList ;
   c,d : integer ;
   line, prop, value : string ;
   
   function getBool () : boolean ;
   begin
      result := false ;
      if StrIComp(pchar(value) , 'true') = 0 then
         result := true
      else if value = '1' then
         result := true ;
   end ;
begin
   configString := TStringlist.create ;
   try try
      if FileExists('TraceTool.config') then
         configString.LoadFromFile('TraceTool.config');
         for c := 0 to configString.Count-1 do begin
            line := configString.Strings[c] ;
            d := pos ('=',line) ;
            if d <> 0 then begin
               prop := copy (line, 1 , d-1) ;
               value := copy (line, d+1, 1000) ;

               if StrIComp(pchar(prop),          'SocketHost'     ) = 0 then begin  // string
                  ttrace.Options.SocketHost := value  ;
               end else if StrIComp(pchar(prop), 'SocketPort'     ) = 0 then begin  // integer
                  ttrace.Options.SocketPort := strToIntDef (value,8090) ;
               end else if StrIComp(pchar(prop), 'DebugEnabled'   ) = 0 then begin  // boolean
                  ttrace.Debug.Enabled := getBool () ;
               end else if StrIComp(pchar(prop), 'WarningEnabled' ) = 0 then begin  // boolean
                  ttrace.warning.Enabled := getBool () ;
               end else if StrIComp(pchar(prop), 'ErrorEnabled'   ) = 0 then begin  // boolean
                  ttrace.error.Enabled := getBool () ;
               end else if StrIComp(pchar(prop), 'SendMode'       ) = 0 then begin  // TSendMode
                  if StrIComp(pchar(value) , 'WinMsg') = 0 then
                     ttrace.Options.SendMode := tmWinMsg
                  else if StrIComp(pchar(value) , 'Alternate') = 0 then
                     ttrace.Options.SendMode := tmAlternate
                  else if StrIComp(pchar(value) , 'socket') = 0 then
                     ttrace.Options.SendMode := tmAlternate ;
               end else if StrIComp(pchar(prop), 'SendProcessName') = 0 then begin  // boolean
                  ttrace.Options.SendProcessName := getBool () ;
               end else if StrIComp(pchar(prop), 'SendFunctions'  ) = 0 then begin  // boolean
                  ttrace.Options.SendFunctions := getBool () ;
               end else if StrIComp(pchar(prop), 'SendInherited'  ) = 0 then begin  // boolean
                  ttrace.Options.SendInherited := getBool () ;
               end else if StrIComp(pchar(prop), 'SendEvents'     ) = 0 then begin  // boolean
                  ttrace.Options.SendEvents := getBool () ;
               end;
            end ;
         end ;
   except
      on e : exception do begin end ;
   end finally
      configString.Clear ;
      configString.free ;
   end ;
end;

//------------------------------------------------------------------------------

function StartDebugWin: hWnd;
var
  Reg: TRegistry;
  DebugFilename: string;
  Buf: array[0..MAX_PATH + 1] of Char;
  si: TStartupInfo;
  pi: TProcessInformation;
  c : integer ;
begin
   result := FindWindow('TFormReceiver', 'FormReceiver');

   if result <> 0 then
      exit ;

   // retry in case of another thread already launch the process
   result := FindWindow('TFormReceiver', 'FormReceiver');
   if result <> 0 then
      exit ;

   Result := 0 ;

   Reg := TRegistry.Create;
   try
      Reg.RootKey := HKEY_LOCAL_MACHINE;
      Reg.OpenKey('Software\TraceTool',true);
      DebugFilename := Reg.ReadString('FilePath');
      Reg.CloseKey ;
   finally
      Reg.Free;
   end ;

   if (Trim(DebugFilename) = '') or not FileExists(DebugFilename) then begin
      GetModuleFileName(HINSTANCE, Buf, SizeOf(Buf)-1);
      DebugFileName := ExtractFilePath(StrPas(Buf))+'TraceTool.exe';
   end ;

   if (Trim(DebugFilename) = '') then
      exit ;

   if not FileExists(DebugFilename) then
      exit ;

   FillChar(si, SizeOf(si), #0);
   si.cb := SizeOf(si);
   si.dwFlags := STARTF_USESHOWWINDOW;    // If this value is not specified, the wShowWindow member is ignored.
   si.wShowWindow := SW_NORMAL;  // Displays the window as an icon. The window that is currently active remains active
   //si.wShowWindow := SW_SHOWMINNOACTIVE;  // Displays the window as an icon. The window that is currently active remains active

   if not CreateProcess(PChar(DebugFilename), nil, nil, nil, False, 0, nil, nil, si, pi) then
   begin
     Result := 0;
     Exit;
   end;

   try
     WaitForInputIdle(pi.hProcess, 3 * 1000); // wait for 3 seconds to get idle
   finally
     CloseHandle(pi.hThread);
     CloseHandle(pi.hProcess);
   end;

   // wait max 5 secondes
   for c := 1 to 50 do begin
      Result := FindWindow('TFormReceiver', 'FormReceiver');
      if result <> 0 then
         exit ;
      WaitForSingleObject (MsgThread.Handle,100) ;   // 50*100 = 5 sec
   end ;
end;

//------------------------------------------------------------------------------

// get the member type (field or method) or parameter type
// don't work with member from IDispatch and IUnknown
function getVariantType (TypeInfo : ITypeInfo ; ObjTypeDesc: PTypeDesc) : string ;
var
   ARefType : Cardinal;
   TypeInfoBis : ITypeInfo;
   intfName : WideString ;
begin
   if (ObjTypeDesc.vt=VT_PTR) then
   begin
      // associated typeInfo to this type
      if (ObjTypeDesc.ptDesc.vt = VT_USERDEFINED) then begin
         ARefType := ObjTypeDesc.ptDesc^.hreftype ; // object href
         OleCheck(TypeInfo.GetRefTypeInfo(ARefType,TypeInfoBis))
      end else
         TypeInfoBis := TypeInfo;

      OleCheck(TypeInfoBis.GetDocumentation (-1 ,@intfName, nil,nil,  nil));
      result := intfName ;   // widestring to string

   end else begin
      result := TraceTool.GetVariantType (ObjTypeDesc.vt) ;
   end ;
end ;

//------------------------------------------------------------------------------
// convert varType (integer) to string representation
// if VT_PTR (dispatch), the real type is not returned
function GetVariantType (VarType : integer) : string ;
begin
   // [V] - may appear in a VARIANT
   // [T] - may appear in a TYPEDESC
   // [P] - may appear in an OLE property set
   // [S] - may appear in a Safe Array }

   result := '' ;
   if (VarType) = VT_ARRAY then begin
      result := 'Array ' ;
      exit ;
   end ;

   case VarType of
      vt_empty           : result := 'Empty' ;                       // 0        [V]   [P]  nothing
      vt_null            : result := 'Null' ;                        // 1        [V]        SQL style Null
      VT_I2              : result := 'Small int' ;                   // 2        [V][T][P]  2 byte signed int
      VT_I4              : result := 'Integer' ;                     // 3        [V][T][P]  4 byte signed int
      VT_R4              : result := 'Single' ;                      // 4        [V][T][P]  4 byte real
      VT_R8              : result := 'Double' ;                      // 5        [V][T][P]  8 byte real
      VT_CY              : result := 'Currency' ;                    // 6        [V][T][P]  currency
      VT_DATE            : result := 'Date' ;                        // 7        [V][T][P]  date
      VT_BSTR            : result := 'String' ;                      // 8        [V][T][P]  binary string
      varString          : result := 'String' ;                      // $0100               256 : Pascal string; not OLE compatible
      VT_DISPATCH        : result := 'IDispatch';                    // 9        [V][T]     IDispatch FAR*
      VT_ERROR           : result := 'Error';                        // 10       [V][T]     SCODE
      VT_BOOL            : result := 'Boolean' ;                     // 11       [V][T][P]  True=-1, False=0
      VT_VARIANT         : result := 'Variant';                      // 12       [V][T][P]  VARIANT FAR*
      VT_UNKNOWN         : result := 'Unknown';                      // 13       [V][T]     IUnknown FAR*
      VT_DECIMAL         : result := 'Decimal';                      // 14       [V][T][S]  16 byte fixed point
      15                 : result := 'Undefined';                    // 15                  undefined
      VT_I1              : result := 'signed char';                  // 16        [T]       signed char
      VT_UI1             : result := 'unsigned char';                // 17        [T]       unsigned char
      VT_UI2             : result := 'unsigned short';               // 18        [T]       unsigned short
      VT_UI4             : result := 'unsigned short';               // 19        [T]       unsigned short
      VT_I8              : result := 'signed 64-bit int';            // 20        [T][P]    signed 64-bit int
      VT_UI8             : result := 'unsigned 64-bit int';          // 21        [T]       unsigned 64-bit int
      VT_INT             : result := 'signed machine int';           // 22        [T]       signed machine int
      VT_UINT            : result := 'unsigned machine int';         // 23        [T]       unsigned machine int
      VT_VOID            : result := 'C style void';                 // 24        [T]       C style void
      VT_HRESULT         : result := 'HRESULT';                      // 25        [T]       HRESULT
      VT_PTR             : result := 'pointer type';                 // 26        [T]       pointer type
      VT_SAFEARRAY       : result := 'VT_ARRAY in VARIANT';          // 27        [T]       (use VT_ARRAY in VARIANT)
      VT_CARRAY          : result := 'C style array';                // 28        [T]       C style array
      VT_USERDEFINED     : result := 'user defined type';            // 29        [T]       user defined type
      VT_LPSTR           : result := 'null terminated string';       // 30        [T][P]    null terminated string
      VT_LPWSTR          : result := 'wide null terminated string';  // 31        [T][P]    wide null terminated string
      VT_FILETIME        : result := 'FILETIME';                     // 64           [P]    FILETIME
      VT_BLOB            : result := 'Length prefixed bytes';        // 65           [P]    Length prefixed bytes
      VT_STREAM          : result := 'Name of the stream follows';   // 66           [P]    Name of the stream follows
      VT_STORAGE         : result := 'Name of the storage follows';  // 67           [P]    Name of the storage follows
      VT_STREAMED_OBJECT : result := 'Stream contains an object';    // 68           [P]    Stream contains an object
      VT_STORED_OBJECT   : result := 'Storage contains an object';   // 69           [P]    Storage contains an object
      VT_BLOB_OBJECT     : result := 'Blob contains an object';      // 70           [P]    Blob contains an object
      VT_CF              : result := 'Clipboard format';             // 71           [P]    Clipboard format
      VT_CLSID           : result := 'A Class ID';                   // 72           [P]    A Class ID
      VT_VECTOR          : result := 'simple counted array';         // $1000        [P]    simple counted array
      VT_ARRAY           : result := 'SAFEARRAY*';                   // $2000  [V]          SAFEARRAY*
      VT_BYREF           : result := 'Byref';                        // $4000  [V]          Byref
      VT_RESERVED        : result := 'Reserved';                     // $8000               Reserved
      VT_ILLEGAL         : result := 'Illegal';                      // $ffff               Illegal
      VT_ILLEGALMASKED   : result := 'IllegalMask';                  // $0fff               IllegalMask
      else                 result := 'unknow ' + inttostr(VarType);
   end;
end ;

//------------------------------------------------------------------------------

function GetDispatchName (Disp : IDispatch) : string ;
var
   p : PWideChar ;
   TypeInfo: ITypeInfo;
   TypeAttr: PTypeAttr;
   Res: HResult ;
   strGuid : string ;
begin
   try
      res := disp.GetTypeInfo(0,0,TypeInfo) ;
      if not Succeeded(Res) then
         TypeInfo := nil ;  // ensure TypeInfo don't contain bad result
   except
   end ;

   if TypeInfo = nil then
      Exit;

   OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
   try
      StringFromCLSID(TypeAttr.guid, P);
      strGuid := P;
      CoTaskMemFree(P);

      result := '' ;
      with TRegistry.Create do
      try
         RootKey := HKEY_CLASSES_ROOT;
         OpenKey (Format ('Interface\%s', [strGuid]), False);
         result := ReadString ('');
         CloseKey ;
      finally
         Free ;
      end ;
   finally
      TypeInfo.ReleaseTypeAttr(TypeAttr);
   end ;
end ;

//------------------------------------------------------------------------------

procedure GetDispathDescriptions (Disp : IDispatch; var strGuid,strProg, strTypeLibGuid, strTypeLib : string) ;
var
   TypeInfo: ITypeInfo;
   TypeAttr: PTypeAttr;
   Result: HResult ;
begin
   strGuid := '' ;
   strProg := '' ;
   strTypeLibGuid := '' ;
   strTypeLib := '' ;

   try
      result := disp.GetTypeInfo(0,0,TypeInfo) ;
      if not Succeeded(Result) then
         TypeInfo := nil ;  // ensure TypeInfo don't contain bad result
   except
   end ;

   if TypeInfo = nil then
      Exit;

   OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
   try
      GetDispathDescriptions (TypeAttr , strGuid,strProg, strTypeLibGuid, strTypeLib) ;
   finally
      TypeInfo.ReleaseTypeAttr(TypeAttr);
   end ;
end ;

//------------------------------------------------------------------------------

procedure GetDispathDescriptions (TypeAttr : PTypeAttr; var strGuid,strProg, strTypeLibGuid, strTypeLib : string) ;
var
   p : PWideChar ;
   strTypeLibVersion : string ;
begin
   StringFromCLSID(TypeAttr.guid, P);
   strGuid := P;
   CoTaskMemFree(P);

   strTypeLibGuid := '' ;
   strTypeLib     := '' ;
   strProg        := '' ;
   with TRegistry.Create do
   try
      RootKey := HKEY_CLASSES_ROOT;
      OpenKey (Format ('Interface\%s', [strGuid]), False);
      strProg := ReadString ('');
      CloseKey ;

      OpenKey (Format ('Interface\%s\TypeLib', [strGuid]), False);
      strTypeLibGuid := ReadString ('') ;
      strTypeLibVersion := ReadString ('Version') ;
      CloseKey ;

      OpenKey (Format ('TypeLib\%s\%s', [strTypeLibGuid,strTypeLibVersion]), False);
      strTypeLib := ReadString ('') ;
      CloseKey ;
   finally
      Free ;
   end ;
end ;

//------------------------------------------------------------------------------

function GetVarValue(v: Variant): string; overload ;
var
   strType : string ;
begin
   result := GetVarValue (v,strType) ;
end ;

//------------------------------------------------------------------------------

// return variant value and his type by reference
function GetVarValue (v: Variant ; var strType : string ) : string ;
var
   pc : pchar ;
   disp : IDispatch ;
   ptr : integer ;
begin
   result := '' ;
   if (VarType(v) and VT_ARRAY) = VT_ARRAY then begin
      strType := 'Array ' ;
      exit ;
   end ;
   strType := TraceTool.GetVariantType (VarType(v));

   case VarType(v) of
      VT_DISPATCH :
      begin
         disp := IDispatch (v);
         ptr := integer (disp) ;
         result := '$' + inttohex (ptr,8) ;
         // get the real type
         if disp <> nil then
            strType := TraceTool.GetDispatchName (disp ) ;
      end ;

      VT_I2..VT_BSTR ,      // 2..8    : standard type
      varString ,           // $0100   : pascal string
      VT_BOOL   ,           // 11      : bool
      VT_I1..VT_UINT  :     // 16..23  : chars , shorts , int64
      begin
         result := v ;
         // Ajust string in case of bstr
         pc := Pchar (result) ;
         result := pc ;
      end ;
   end;
end ;

//------------------------------------------------------------------------------

initialization
   LoadCharTypes;          // this table first
   MsgThread := TMsgThread.Create (true) ;   // create suspended
   MsgThread.hCloseEvent   := CreateEvent( nil, True, False, nil );  // Create the close event. Manualreset = true, initial = false
   MsgThread.hMessageReady := CreateEvent( nil, True, False, nil );  // Create the ready event. Manualreset = true, initial = false
   MsgThread.FreeOnTerminate := true ;
   MsgThread.Resume ;
   criticalSection := TCriticalSection.Create ;
   setMessageList := TObjectList.Create (false) ;  // don't own object
   getMessageList := TObjectList.Create (false) ;  // don't own object
   LoadConfig() ;


finalization
   setevent(MsgThread.hCloseEvent) ;


end.
