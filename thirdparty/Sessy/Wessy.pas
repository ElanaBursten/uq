// -------------------------------------------------------------------
// Wessy - a web service framework for Delphi Web Broker
// Version 0.1
// Copyright 2014, Oasis Digital Solutions Inc. All Rights Reserved.
//
// License: MIT License: http://opensource.org/licenses/MIT
// Alternatively, you may have obtained this file directly from
// Oasis Digital, under a commercial license as part of a
// development project.
//
// Key features:
// * Login and ping in the box
// * Works with Sessy
// * CORS
// -------------------------------------------------------------------

unit Wessy;

interface

uses
  Classes, SysUtils, DB, Web.HTTPApp, System.Generics.Collections,
  Sessy;

type

  IWebDataModule = interface
    procedure UseContext(ARequest: TWebRequest; AResponse: TWebResponse;
      AParams: TStrings; ASessionVars: TStrings);
    function CheckLogin(const Username, Password: string): boolean;
    function CheckAuthorization(const Username, APIKey: string): boolean;
    procedure ReturnUserInformation;
    procedure ReturnPingInformation;
  end;

  TWebDataModuleMaker = reference to function: IWebDataModule;

  THandlerMethod = procedure of object;

  ENotLoggedIn = class(Exception);

  THandler = class
    HttpMethod: TMethodType;
    Route: AnsiString;
    MethodName: string;
    RequireLogin: Boolean;
  end;

  TWebRouter = class(TComponent)
  private
    WFD: TWebFileDispatcher;
    FAppRoutes: TList<AnsiString>;
    FSessionManager: TSessy;
    FWebDataModuleMaker: TWebDataModuleMaker;
    FHandlers: TObjectList<THandler>;
    FLoginRoute: AnsiString;
    FLoginFormRoute: AnsiString;
    FLoginFormFailedRoute: AnsiString;
    FLogoutRoute: AnsiString;
    FPingRoute: AnsiString;
    FDefaultLoggedInRoute: AnsiString;

    procedure WFDAfterDispatch(Sender: TObject; const AFileName: string;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  public
    // Setup
    procedure AddAppRoute(const Route: AnsiString);
    procedure ServeFiles(const DevFileRelativePath: string);
    procedure UseSessionManager(ASessionManager: TSessy);
    procedure UseDataModuleMaker(AWebDataModuleMaker: TWebDataModuleMaker);
    procedure UseCoreServiceRoutes(const ALoginRoute, ALogoutRoute, APingRoute: AnsiString);
    procedure UseDefaultLoggedInRoute(const Route: AnsiString);
    procedure AddHandler(AHttpMethod: TMethodType; ARoute: AnsiString;
      AMethodName: string; ARequireLogin: Boolean = True);

    // Entry points
    procedure Route(Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    function GetMimeType(FileName: String): String;
    function GetWWWDir: string;
    procedure RespondWithFile(Response: TWebResponse; const Filename: string);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

function ParseRegEx(const PathInfo, Expression: string; Strings: TStrings): Boolean;

implementation

uses
  Forms, ActiveX, XSuperObject, RegularExpressionsCore, Web.WebFileDispatcher, IdCoderMIME;

function ParseRegEx(const PathInfo, Expression: string; Strings: TStrings): Boolean;
var
  RegEx: TPerlRegEx;
  I: Integer;
begin
  if Assigned(Strings) then
    Strings.Clear;
  RegEx := TPerlRegEx.Create;
  try
    RegEx.RegEx := UTF8String('^' + Expression + '$');
    RegEx.Subject := UTF8String(PathInfo);
    if RegEx.Match then begin
      if Assigned(Strings) then
        for I := 1 to RegEx.GroupCount do
          Strings.Add(string(RegEx.Groups[I]));
      Result := True;
    end else
      Result := False;
  finally
    RegEx.Free;
  end;
end;

{ TWebRouter }

procedure TWebRouter.AddAppRoute(const Route: AnsiString);
begin
  FAppRoutes.Add(Route);
end;

procedure TWebRouter.AddHandler(AHttpMethod: TMethodType; ARoute: AnsiString;
  AMethodName: string; ARequireLogin: Boolean);
var
  H: THandler;
begin
  // need to keep a list

  H := THandler.Create;
  H.HttpMethod := AHttpMethod;
  H.Route := ARoute;
  H.MethodName := AMethodName;
  H.RequireLogin := ARequireLogin;
  FHandlers.Add(H);
end;

constructor TWebRouter.Create(AOwner: TComponent);
begin
  inherited;
  FAppRoutes := TList<AnsiString>.Create;

  FHandlers := TObjectList<THandler>.Create(True);

  // Passing AOwner lets it link in to the webmodule routing
  WFD := TWebFileDispatcher.Create(AOwner);

  WFD.WebFileExtensions.Clear;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'text/css';
    Extensions := 'css';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'text/html';
    Extensions := 'html;htm';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'text/javascript';
    Extensions := 'js';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'image/jpeg';
    Extensions := 'jpeg;jpg';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'image/png';
    Extensions := 'png';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'font/x-woff';
    Extensions := 'woff';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'application/vnd.ms-fontobject';
    Extensions := 'eot';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'application/octet-stream';
    Extensions := 'ttf';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'image/gif';
    Extensions := 'gif';
  end;
  with WFD.WebFileExtensions.Add do begin
    MimeType := 'image/svg+xml';
    Extensions := 'svgz;svg';
  end;
  WFD.AfterDispatch := WFDAfterDispatch;
  with WFD.WebDirectories.Add do begin
    DirectoryAction := dirInclude;
    DirectoryMask := '*';
  end;
  WFD.RootDirectory := 'www';

  FLoginRoute := '/api/v1/login';
  FLoginFormRoute := '/login';
  FLoginFormFailedRoute := '/login?failed=1';
  FLogoutRoute := '/api/v1/logout';
  FPingRoute := '/api/v1/ping';
  FDefaultLoggedInRoute := '/app';
end;

procedure TWebRouter.WFDAfterDispatch(Sender: TObject; const AFileName: string;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  // Don't cache the static assets for now, it makes development a pain.
  // In production we need a better solution, though it might not matter
  // if the application is deployed with PhoneGap.
  Response.SetCustomHeader('Cache-Control', 'no-cache');
end;

destructor TWebRouter.Destroy;
begin
  FreeAndNil(FHandlers);
  FreeAndNil(FAppRoutes);
  inherited;
end;

procedure TWebRouter.Route(Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
var
  IDM: IWebDataModule;
  SessionVars: TStrings;
  Params: TStrings;
  ApiMonLoginRegex: AnsiString;

  function MatchesRoute(HttpMethod: TMethodType; Route: AnsiString): Boolean;
  begin
    if Handled then begin
      Result := False;
      Exit;
    end;
    Result := ParseRegEx(string(Request.PathInfo), string(Route), Params) and
      (HttpMethod = Request.MethodType);
  end;

  procedure ServeFilesAtAppRoutes;
  begin
    if FAppRoutes.IndexOf(Request.PathInfo) >= 0 then begin
      Handled := True;
      Response.SetCustomHeader('Cache-Control', 'no-cache'); // dynamic data

      // Serve page, or redirect to login
      if SessionVars.Values['LoggedIn'] <> '' then begin
        RespondWithFile(Response, WFD.RootDirectory + '/main.html');
      end else begin
        Response.StatusCode := 302;
        Response.SendRedirect(FLoginFormRoute);
      end;
    end;
  end;

  function CheckRequestAuth: Boolean;
  const
    BasicHeader = 'Basic ';
  var
    AuthDecoded: string;
    AuthParams: TStrings;
  begin
    Result := False;
    AuthParams := TStringList.Create;
    try
      AuthDecoded := TIdDecoderMIME.DecodeString(Copy(string(Request.Authorization), Length(BasicHeader) + 1,
        Length(string(Request.Authorization))));
      if ParseRegEx(AuthDecoded, '(\w+):(\w+)', AuthParams) then
        Result := IDM.CheckAuthorization(AuthParams[0], AuthParams[1]);
    finally
      FreeAndNil(AuthParams);
    end;
  end;

  procedure RunHandler(RequireLogin: Boolean; Handler: TProc);
  begin
    Handled := True;
    IDM.UseContext(Request, Response, Params, SessionVars);
    if RequireLogin then
      if SessionVars.Values['LoggedIn'] = '' then
        if not CheckRequestAuth then
          raise ENotLoggedIn.Create('This resource can only be used by a logged in user');

    Handler;
    Response.SetCustomHeader('Cache-Control', 'no-cache'); // dynamic data
    FSessionManager.SaveSession(SessionVars, Response, False);
  end;

  procedure HandleCoreRoutes;
  begin
    if MatchesRoute(mtGet, FLoginFormRoute) then
      RunHandler(False, procedure
        begin
          if SessionVars.Values['LoggedIn'] <> '' then begin
            Response.StatusCode := 302;
            Response.SendRedirect(AnsiString(FDefaultLoggedInRoute));
          end else begin
            RespondWithFile(Response, WFD.RootDirectory + '/login.html');
          end;
        end);

    if MatchesRoute(mtPost, FLoginFormRoute) then
      RunHandler(False, procedure
        var
          Username, Password: string;
        begin
          Username := Request.ContentFields.Values['username'];
          Password := Request.ContentFields.Values['password'];

          // Request, Response, and Params are available to IDM.CheckLogin.
          if IDM.CheckLogin(Username, Password) then begin
            SessionVars.Values['LoggedIn'] := Username;

            // Ugh, it doesn't give me a chance to add cookies?
            //Response.SendRedirect(FDefaultLoggedInRoute);
            Response.StatusCode := 302;
            Response.CustomHeaders.Values['Location'] := string(FDefaultLoggedInRoute);

            //		redirect := hr.FormValue("redirectTo")
            //		http.Redirect(w, hr, "/"+redirect, 302)
          end else begin
            SessionVars.Values['LoggedIn'] := '';
            Response.StatusCode := 302;
            Response.SendRedirect(AnsiString(FLoginFormFailedRoute));
          end;
        end);

    if MatchesRoute(mtPost, FLoginRoute) then
      RunHandler(False, procedure
        var
          RequestObj: ISuperObject;
          Username, Password: string;
        begin
          RequestObj := SO(Request.Content);
          Username := RequestObj.AsObject.S['username'];
          Password := RequestObj.AsObject.S['password'];

          // Request, Response, and Params are available to IDM.CheckLogin.
          if IDM.CheckLogin(Username, Password) then begin
            SessionVars.Values['LoggedIn'] := Username;
            IDM.ReturnUserInformation;
          end else begin
            SessionVars.Values['LoggedIn'] := '';
            Response.Content := '{"result": "failure"}';
            Response.ContentType := 'application/json';
          end;
        end);

    if MatchesRoute(mtPost, FLogoutRoute) then
      RunHandler(False, procedure begin
        SessionVars.Values['LoggedIn'] := '';
        Response.Content := '{"result": "ok"}';  // can be ignored by client
        Response.ContentType := 'application/json';
      end);

    if MatchesRoute(mtGet, FPingRoute) then
      RunHandler(True, procedure begin
        // Ping service - client can use this to check whether it has
        // connectivity, can log in, etc.
        // Default, if it fails:
        IDM.ReturnPingInformation;
      end);
  end;

  procedure HandleAddedRoutes;
  var
    I: Integer;
    H: THandler;
    DM: TDataModule;
  begin
    // Get a TDataModule ref so its methods are accessible to RunHandler.
    // Never free DM! That happens when IDM gets set to nil below.
    DM := IDM as TDataModule;
    for I := 0 to FHandlers.Count-1 do begin
      H := FHandlers[I];
      if MatchesRoute(H.HttpMethod, H.Route) then begin
        RunHandler(H.RequireLogin, procedure
        var m: TMethod;
        begin
          Assert(Assigned(DM), 'DM must be assigned');
          m.Code := DM.MethodAddress(H.MethodName);
          Assert(m.Code <> nil, 'Method not found: ' + H.MethodName);
          m.Data := pointer(DM);
          THandlerMethod(m)();
        end);
      end;
    end;
  end;

  procedure AddCORSAll;
  var
    RequestOrigin: string;
  begin
    RequestOrigin := string(Request.GetFieldByName('Origin'));
    if RequestOrigin = '' then
      RequestOrigin := '*';
    Response.SetCustomHeader('Access-Control-Allow-Origin', RequestOrigin);
    Response.SetCustomHeader('Access-Control-Allow-Credentials', 'true');
  end;

  procedure AddCORSOptions;
  begin
    Response.SetCustomHeader('Access-Control-Max-Age', '120');
    Response.SetCustomHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    Response.SetCustomHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  end;

  procedure HandleWithParamsAndDM;
  begin
    CoInitializeEx(nil, COINIT_MULTITHREADED);
    try
      IDM := FWebDataModuleMaker();
      Params := TStringList.Create;
      try
        HandleCoreRoutes;
        HandleAddedRoutes;
      finally
        Params.Free;
        // TODO: Setting IDM := nil does not free the associated TDataModule object as expected.
        // ref: http://wiert.me/2009/08/10/delphi-using-fastmm4-part-2-tdatamodule-descendants-exposing-interfaces-or-the-introduction-of-a-tinterfaceddatamodule/
        IDM := nil; 
      end;
    finally
      CoUninitialize;
    end;
  end;

begin
  Handled := False;
  try
    SessionVars := FSessionManager.LoadSession(Request);
    try
      ServeFilesAtAppRoutes;
      if Handled then
        Exit;

      ApiMonLoginRegex := '(/api/.*)|(/monitor/.*)|(' + FLoginFormRoute + ')';
      if ParseRegEx(string(Request.PathInfo), string(ApiMonLoginRegex), nil) then begin
        if Request.Method = 'OPTIONS' then begin
          Handled := True;
          AddCORSOptions;
        end else
          HandleWithParamsAndDM;

        AddCORSAll;
      end;
    finally
      if Assigned(SessionVars) then
        FreeAndNil(SessionVars);
    end;
    if not Handled then
      Response.StatusCode := 404;
  except
    on E: Exception do begin
      // TODO log
      Response.Content := E.Message;
      if E is ENotLoggedIn then
        Response.StatusCode := 403
      else
        Response.StatusCode := 500;
      Handled := True;
    end;
  end;
end;

procedure TWebRouter.ServeFiles(const DevFileRelativePath: string);
var
  WwwPath: string;
begin
  WwwPath := ExtractFilePath(Application.ExeName) + 'www';
  if not DirectoryExists(WwwPath) then begin
    WwwPath := ExtractFilePath(Application.ExeName) + DevFileRelativePath;
  end;
  WFD.RootDirectory := WwwPath;
end;

procedure TWebRouter.RespondWithFile(Response: TWebResponse; const Filename: string);
begin
  Response.ContentType := AnsiString(GetMimeType(Filename));
  Response.ContentStream := TFileStream.Create(Filename, fmOpenRead, fmShareDenyNone);
  Response.SetCustomHeader('Cache-Control', 'no-cache');   // easy development
end;

procedure TWebRouter.UseDefaultLoggedInRoute(const Route: AnsiString);
begin
  FDefaultLoggedInRoute := Route;
end;

procedure TWebRouter.UseCoreServiceRoutes(const ALoginRoute, ALogoutRoute,
  APingRoute: AnsiString);
begin
  FLoginRoute := ALoginRoute;
  FLogoutRoute := ALogoutRoute;
  FPingRoute := APingRoute;
end;

procedure TWebRouter.UseDataModuleMaker(AWebDataModuleMaker: TWebDataModuleMaker);
begin
  FWebDataModuleMaker := AWebDataModuleMaker;
end;

procedure TWebRouter.UseSessionManager(ASessionManager: TSessy);
begin
  FSessionManager := ASessionManager;
end;

function TWebRouter.GetMimeType(FileName: string): string;
var
  i: Integer;
  Ext: string;
  ExtList: TStringList;
begin
  Result := 'text/html; charset=utf-8';
  ExtList := TStringList.Create;
  try
    ExtList.Delimiter := ';';
    Ext := StringReplace(ExtractFileExt(FileName), '.', '', [rfReplaceAll]);
    for i := 0 to Pred(WFD.WebFileExtensions.Count) do begin
      ExtList.DelimitedText := WFD.WebFileExtensions.Items[i].Extensions;
      if ExtList.IndexOf(string(Ext)) > -1 then begin
        Result := WFD.WebFileExtensions.Items[i].MimeType;
        Exit;
      end;
    end;
  finally
    FreeAndNil(ExtList);
  end;
end;

function TWebRouter.GetWWWDir: string;
begin
  Result := WFD.RootDirectory;
end;

end.

