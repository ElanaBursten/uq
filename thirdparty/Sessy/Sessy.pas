// -------------------------------------------------------------------
// Sessy - a session manager for Delphi Web Broker
// Version 0.6
// Copyright 2014, Oasis Digital Solutions Inc. All Rights Reserved.
//
// License: MIT License: http://opensource.org/licenses/MIT
// Alternatively, you may have obtained this file directly from
// Oasis Digital, under a commercial license as part of a
// development project.
//
// Key features:
// * Fits in one source file
// * Support session storage only in client cookies - no DB or files
// * No servers state, no scalability issues
// * Do NOT use large sessions data
// * Encrypts the cookie for tamper resistance
// * Internal expiration, for replay attack resistance
// * Uses DCPCrypt for AES and Base64
// -------------------------------------------------------------------

unit Sessy;

interface

uses
  Sysutils, Classes, Web.HTTPApp, DCPrijndael, DCPcrypt2, DCPsha1;

type
  TSessy = class(TComponent)
  private
    Encryptor: TDCP_rijndael;
    FDomain: string;
    FPath: string;
    FCookieName: string;
    FSessionTimeoutSeconds: integer;
  public
    function LoadSession(Request: TWebRequest): TStrings;
    procedure SaveSession(const SessionVars: TStrings; Response: TWebResponse; const Secure: Boolean);
    constructor Create(const ADomain, APath, ACookineName, AKey: string;
      ASessionTimeoutSeconds: integer; AOwner: TComponent); reintroduce;
  end;

implementation

const
  JUNK_PAD = 16;

// Utilites to convert Base64 between cookie-sage and normal form.

function Base64ToCookieSafe(const Raw: string): string;
var
  I: integer;
begin
  Result := Raw;
  for I := 1 to Length(Result) do begin
    if Result[I] = '=' then
      Result[I] := '.';
    if Result[I] = '+' then
      Result[I] := '-';
    if Result[I] = '/' then
      Result[I] := '_';
  end;
end;

function CookieSafeToBase64(const Raw: string): string;
var
  I: integer;
begin
  Result := Raw;
  for I := 1 to Length(Result) do begin
    if Result[I] = '.' then
      Result[I] := '=';
    if Result[I] = '-' then
      Result[I] := '+';
    if Result[I] = '_' then
      Result[I] := '/';
  end;
end;

{ TSessy }

constructor TSessy.Create(const ADomain, APath, ACookineName, AKey: string;
  ASessionTimeoutSeconds: integer; AOwner: TComponent);
begin
  inherited Create(AOwner);
  Encryptor := TDCP_rijndael.Create(Self);
  Encryptor.InitStr(AKey, TDCP_sha1);

  FDomain := ADomain;
  FPath := APath;
  FCookieName := ACookineName;
  FSessionTimeoutSeconds := ASessionTimeoutSeconds;
end;

function TSessy.LoadSession(Request: TWebRequest): TStrings;
var
  CookieVal: string;
  Decrypted: string;
  InternalExpires: TDateTime;
begin
  Result := TStringList.Create;
  Result.Delimiter := '|';
  InternalExpires := 0;
  try
    CookieVal := Request.CookieFields.Values[FCookieName];
    // strip the random junk
    Encryptor.Reset;
    Decrypted := Encryptor.DecryptString(CookieSafeToBase64(CookieVal));
    Delete(Decrypted, 1, JUNK_PAD);
    Result.DelimitedText := Decrypted;
    InternalExpires := StrToFloatDef(Result.Values['Expires'], 0);
  except
    // OK, means it was corrupt, we will make a new session.
  end;

  if (Result.IndexOfName('ID')<0) or (InternalExpires < Now) then begin
    // Corrupt, missing, or expired session.
    Result.Clear;
    Result.Add('ID=' + IntToStr(Random(10000000)));
  end;
end;

function Randomstring: string;
var
  I: integer;
begin
  SetLength(Result, JUNK_PAD);
  for I := 1 to JUNK_PAD do
    Result[I] := System.Char(1 + Random(253));
end;

procedure TSessy.SaveSession(const SessionVars: TStrings; Response: TWebResponse;
  const Secure: Boolean);
var
  Cook: string;
  Encrypted: string;
  InternalExpires: TDateTime;
  CookieExpires: TDateTime;
begin
  InternalExpires := Now + 1.0 / SecsPerDay * FSessionTimeoutSeconds;
  SessionVars.Values['Expires'] := FloatToStr(InternalExpires);

  // Make the cook last a couple extra days; this way we don't care if the
  // client's clock is a little off, and don't have to worry about converting
  // to UTC below.
  CookieExpires := InternalExpires + 3.0;  // days

  Encryptor.Reset;
  // Junk at the beginning so the encrypted result is not stable from one
  // version to the next.
  Encrypted := Encryptor.EncryptString(Randomstring +
    SessionVars.DelimitedText);

  // Delphi HTTPEncodes any cookie value set using TCookie, so
  // we build the header manually instead:
  // CookieSafeBase64 protects the + from HTTPDecode making it a space
  Cook := Format('%s=%s; ', [FCookieName, Base64ToCookieSafe(Encrypted)]);
  if FDomain <> '' then
    Cook := Cook + Format('domain=%s; ', [FDomain]);
  if FPath <> '' then
    Cook := Cook + Format('path=%s; ', [FPath]);
  Cook := Cook + Format(
    FormatDateTime('"expires="' + sDateFormat + ' "GMT; "', CookieExpires),
        [DayOfWeekStr(CookieExpires), MonthStr(CookieExpires)]);
  if Secure then Cook := Cook + 'secure';
  if Copy(Cook, Length(Cook) - 1, MaxInt) = '; ' then
    SetLength(Cook, Length(Cook) - 2);

  // Example:
  // Set-Cookie:SAMPLE_APP=RofbMmx1c7CHy_KoYrna90F5kUo.; Path=/; Expires=Sat, 06-Apr-2013 04:50:02 GMT
  Response.SetCustomHeader('Set-Cookie', Cook);
end;

initialization
  Randomize;

end.

