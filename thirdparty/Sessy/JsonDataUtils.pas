unit JsonDataUtils;

interface

{ Use this conditional compiler DEF if you are using ADO; otherwise FireDAC is needed. }
{.$DEFINE ADO_SUPPORT}

uses
  Classes, SysUtils, DB, Web.HTTPApp,
  {$IFDEF ADO_SUPPORT} // Use ADO for DB access
  Data.Win.ADODB,
  {$ELSE}              // Use FireDAC for DB access
  AdUtils,
  {$ENDIF}
  SuperObject;

procedure RespondWithJsonFromDataSet(const Response: TWebResponse;
  const Data: TDataSet; const PathLabel: boolean = False);
function DateTimeToIsoString(AValue: TDateTime): string;
function CreateJsonFromDataSet(const Data: TDataSet): ISuperObject;
function CreatePathedJsonFromDataSet(const Data: TDataSet): ISuperObject;

{$IFDEF ADO_SUPPORT}
procedure LoadNextRecordset(const SP: TADOStoredProc; const RS: TCustomADODataSet);
{$ENDIF}

implementation

const
  IsoDelphiDateTimeFormat = 'YYYY"-"MM"-"DD"T"hh":"nn":"ss"."zzz';

function DateTimeToIsoString(AValue: TDateTime): string;
begin
  Result := FormatDateTime(IsoDelphiDateTimeFormat, AValue);
end;

// TODO: use SuperObject DelphiDateTimeToISO8601Date instead.

{$IFDEF ADO_SUPPORT}
procedure LoadNextRecordset(const SP: TADOStoredProc; const RS: TCustomADODataSet);
var
  RecsAffected: Integer;
  AdoRecordSet: _Recordset;
begin
  AdoRecordSet := SP.NextRecordset(RecsAffected);
  Assert(Assigned(AdoRecordSet), 'SP.NextRecordset must return not nil: ' + RS.Name);
  RS.Recordset := AdoRecordSet;
  Assert(RS.Active, 'Recordset should be active: ' + RS.Name);
end;
{$ENDIF}

function CreateJsonFromDataSet(const Data: TDataSet): ISuperObject;
var
  i: Integer;
  MainObj, DetailObj: ISuperObject;
  F: TField;
begin
  Data.Open;
  try
    MainObj := SA([]);
    while not Data.Eof do begin
      DetailObj := SO;
      for i := 0 to Data.FieldCount - 1 do begin
        F := Data.Fields[i];
        if F.DataType = ftDateTime then
          // DetailObj.S[F.FieldName] := DelphiDateTimeToISO8601Date(F.AsDateTime)
          DetailObj.S[F.FieldName] := DateTimeToIsoString(F.AsDateTime)
        else if F.DataType = ftDate then
          DetailObj.S[F.FieldName] := FormatDateTime('YYYY-MM-DD', F.AsDateTime)
        else if F.DataType = ftLargeint then
          DetailObj.I[F.FieldName] := F.AsLargeInt
        else if F.DataType = ftInteger then
          DetailObj.I[F.FieldName] := F.AsInteger
        else if F.DataType = ftCurrency then
          DetailObj.C[F.FieldName] := F.AsCurrency
        else if F.DataType = ftFloat then
          DetailObj.D[F.FieldName] := F.AsFloat
        else
          DetailObj.S[F.FieldName] := StringReplace(StringReplace(F.AsString, #13, '' ,[rfReplaceAll]), #10, '\n' ,[rfReplaceAll]);
      end;
      MainObj.AsArray.Add(DetailObj);
      Data.Next;
    end;
    Result := MainObj;
  finally
    Data.Close;
  end;
end;

function CreatePathedJsonFromDataSet(const Data: TDataSet): ISuperObject;
var
  I: Integer;
  F: TField;
  Path: string;
begin
  Data.Open;
  try
    Result := SO;
    while not Data.Eof do begin
      for I := 1 to Data.FieldCount - 1 do begin
        F := Data.Fields[I];
        if not F.IsNull then begin
          Path := Data.Fields[0].AsString + '.' + F.FieldName;
          if F.DataType = ftDateTime then
            Result.S[Path] := DateTimeToIsoString(F.AsDateTime)
          else if F.DataType = ftLargeint then
            Result.I[Path] := F.AsLargeInt
          else if F.DataType = ftInteger then
            Result.I[Path] := F.AsInteger
          else if F.DataType = ftCurrency then
            Result.C[Path] := F.AsCurrency
          else if F.DataType = ftFloat then
            Result.D[Path] := F.AsFloat
          else
            // Escape CRLF, and nothing else, because 'minimal' escape mode does
            // does the rest already.
            Result.S[Path] := StringReplace(
              StringReplace(F.AsString, #13, '' ,[rfReplaceAll]),
              #10, '\n' ,[rfReplaceAll]);
        end;
      end;
      Data.Next;
    end;
  finally
    Data.Close;
  end;
end;

procedure RespondWithJsonFromDataSet(const Response: TWebResponse;
  const Data: TDataSet; const PathLabel: boolean);
begin
  Response.ContentType := 'application/json';
  // Use minimal-escape mode, for tidier JSON
  if PathLabel then
    Response.Content := CreatePathedJsonFromDataSet(Data).AsJSon(True, False)
  else
    Response.Content := CreateJsonFromDataSet(Data).AsJSon(True, False);
end;

end.

