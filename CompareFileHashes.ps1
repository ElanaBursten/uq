<# 
  This Powershell script uses the SHA1 Crypto Provider to compare hashes of 2 files
  It returns an error if they don't match.
#> 
[CmdletBinding(
  SupportsShouldProcess=$False,
  SupportsTransactions=$False,
  ConfirmImpact="Low",
  DefaultParameterSetName="")]

param(
  [Parameter(Position=0,Mandatory=$true)]
  [string]$file1,

  [Parameter(Position=1,Mandatory=$true)]
  [string]$file2,

  [Parameter(Position=2,Mandatory=$false)]
  [ValidateSet("MD5","SHA1")]
  [string]$algorithm="SHA1"
)

trap 
{ 
  Write-Error "ERROR: $_"
  Exit 1 
} 

# Decide which algorithm to use
switch($Algorithm) {
  "MD5"  {$crypt = new-object System.Security.Cryptography.MD5CryptoServiceProvider}
  "SHA1" {$crypt = new-object System.Security.Cryptography.SHA1CryptoServiceProvider}
}

# Create Input Data 
$filestr1   = Get-Content $file1
$filestr2   = Get-Content $file2
$filebytes1 = [System.Text.UnicodeEncoding]::Unicode.getbytes($filestr1) 
$filebytes2 = [System.Text.UnicodeEncoding]::Unicode.getbytes($filestr2) 
 
# Create a New SHA1 Crypto Provider 
 
# Hash the files and compare the results
$result1 = $crypt.ComputeHash($filebytes1) 
$result2 = $crypt.ComputeHash($filebytes2) 
$hash1 = [system.BitConverter]::ToUint64($result1,0).tostring("x")
$hash2 = [system.BitConverter]::ToUint64($result2,0).tostring("x")
if ($hash1 -ne $hash2) {
  Write-Output "$file1 $Algorithm $hash1"
  Write-Output "$file2 $Algorithm $hash2"
  throw "$file1 and $file2 differ."
}

Write-Output "OK: $file1 and $file2 match."
