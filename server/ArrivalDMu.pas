unit ArrivalDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, DB, ADODB, QMServerLibrary_Intf, RemObjectsUtils;

type
  TAfterArrivalSaveEvent = procedure (Sender: TObject; const ArrivalID, TicketID, DamageID: Integer; const ArrivalDate: TDateTime; const EntryType: string) of object;

  TArrivalDM = class(TLogicFeatureDM)
    JobsiteArrival: TADODataSet;
  private
    FAfterArrivalSave: TAfterArrivalSaveEvent;
    procedure DoAfterArrivalSave(const ArrivalID, TicketID, DamageID: Integer;
      const ArrivalDate: TDateTime; const EntryMethod: string);
  public
    procedure ProcessJobsiteArrivalChanges(ChangeList: JobsiteArrivalChangeList;
      var NewKeys: GeneratedKeyList; OnlyForTicket: Integer = -1);
    property AfterArrivalSave: TAfterArrivalSaveEvent read FAfterArrivalSave write FAfterArrivalSave;
  end;

implementation

uses QMConst, OdIsoDates, OdExceptions;

{$R *.dfm}

procedure TArrivalDM.ProcessJobsiteArrivalChanges(ChangeList: JobsiteArrivalChangeList; var NewKeys: GeneratedKeyList; OnlyForTicket: Integer);
const
  Existing = 'select * from jobsite_arrival where emp_id = :emp_id ' +
   'and (ticket_id = :ticket_id or damage_id = :damage_id) and entry_method = :entry_method ' +
   'and arrival_date = %s and added_by = :added_by';
var
  i: Integer;
  Arrival: JobsiteArrivalChange;
  ArrivalID: Integer;
  TicketID: Integer;
  DamageID: Integer;
  EmpID: Integer;
  EntryMethod: string;
  ArrivalDate: TDateTime;
  NewArrivalID: Integer;
  DeletedDate: TDateTime;
  DeletedBy: Integer;
  Active: Boolean;
  LocationStatus: String;
  D: TADODataSet;
  Limitation: string;
  HasPermission: Boolean;

  function ArrivalNeedsTicketAct: Boolean;
  begin
    Result := (TicketID > 0) and (SameText(EntryMethod, 'clock'));
  end;

begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessJobsiteArrivalChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessJobsiteArrivalChanges');

  D := JobsiteArrival;
  for i := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Jobsite Arrival %d of %d', [i + 1, ChangeList.Count]));
    Arrival := ChangeList[i];
    Assert(Assigned(Arrival));

    ArrivalID := Arrival.ArrivalID;
    EmpID := Arrival.EmpID;
    TicketID := Arrival.TicketID;
    DamageID := Arrival.DamageID;
    ArrivalDate := Arrival.ArrivalDate;
    EntryMethod := Arrival.EntryMethod;
    Active := Arrival.Active;
    LocationStatus := Arrival.LocationStatus;

    // Older clients resent unrelated arrivals with each ticket change, so skip those if we ever get here
    if (OnlyForTicket > 0) and (TicketID <> OnlyForTicket) then
      Continue;

    Op.Detail := 'Check for existing arrival';
    D.Close;
    D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(ArrivalDate)]);
    AssignParamsFromROComplex(D.Parameters, Arrival); // 5 Params
    D.Parameters.ParamValues['added_by'] := LogicDM.LoggedInEmpID; // Not being set/sent in the params
    D.Open;
    if D.RecordCount < 1 then begin // This is a new/insert record
      Op.Detail := 'New Arrival - Checking permission';
      if DamageID > 0 then
        HasPermission := (LogicDM.LoggedInEmpID = EmpID) or LogicDM.CanI(RightDamagesEditArrivals, Limitation)
      else
        HasPermission := (LogicDM.LoggedInEmpID = EmpID) or LogicDM.CanI(RightTicketsEditArrivals, Limitation);
      if HasPermission and (LogicDM.LoggedInEmpID <> EmpID) then
        HasPermission := LogicDM.CanIAccessEmpID(LogicDM.LoggedInEmpID, EmpID, Limitation);
      if not HasPermission then
        raise EOdNotAllowed.Create('You are not allowed to add an arrival for employee ID ' + IntToStr(EmpID));

      Op.Detail := 'Adding new arrival';
      D.Insert;
      if DamageID > 0 then
        D.FieldByName('damage_id').AsInteger := DamageID
      else if TicketID > 0 then //do not insert 0
        D.FieldByName('ticket_id').AsInteger := TicketID
      else
        raise EOdException.Create('Cannot insert an arrival without a ticket_id or damage_id. Arrival ID = ' + IntToStr(ArrivalID));
      D.FieldByName('emp_id').AsInteger := EmpID;
      D.FieldByName('arrival_date').AsDateTime := ArrivalDate;
      D.FieldByName('entry_method').AsString := EntryMethod;
      D.FieldByName('added_by').AsInteger := LogicDM.LoggedInEmpID;
      if LocationStatus <> '' then
        D.FieldByName('location_status').AsString := LocationStatus;
      D.Post;

      NewArrivalID := D.FieldByName('arrival_id').AsInteger;
      Assert(NewArrivalID > 0, 'Negative new arrival ID');
      LogicDM.UpdateDateTimeToIncludeMilliseconds('jobsite_arrival', 'arrival_id', NewArrivalID, 'arrival_date', ArrivalDate);
      AddIdentity(NewKeys, 'jobsite_arrival', ArrivalID, NewArrivalID);
      if ArrivalNeedsTicketAct then
        LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypeArrive, ArrivalDate);

      DoAfterArrivalSave(NewArrivalID, TicketID, 0, ArrivalDate, EntryMethod);
    end
    else if ArrivalID < 0 then
    begin
      // Handle returning the real/server ID for for failed sync re-saves
      NewArrivalID := D.FieldByName('arrival_id').AsInteger;
      AddIdentity(NewKeys, 'jobsite_arrival', ArrivalID, NewArrivalID);
    end;

    // This also handles when an arrival is added and deleted in the same session
    // The client prevents an arrival from being "deleted more than once"
    if (not Active) then begin // This is a delete (we don't support other types of edits)
      Op.Detail := 'Deleting arrival - Checking permission';
      if TicketID > 0 then
        HasPermission := LogicDM.CanI(RightTicketsEditArrivals, Limitation)
      else
        HasPermission := LogicDM.CanI(RightDamagesEditArrivals, Limitation);
      if HasPermission then begin
        HasPermission := (LogicDM.LoggedInEmpID = EmpID) or
          LogicDM.CanIAccessEmpID(LogicDM.LoggedInEmpID, EmpID, Limitation);
        if not HasPermission then
          raise EOdException.Create('You are not allowed to modify arrivals for employee ID ' + IntToStr(EmpID));

        Op.Detail := 'Deleting arrival';

        DeletedDate := Arrival.DeletedDate;
        DeletedBy := Arrival.DeletedBy;

        if LogicDM.LoggedInEmpID <> DeletedBy then
          raise EOdNotAllowed.Create('You are not allowed to save an arrival not modified by you.');

        D.Refresh;
        D.Edit;
        D.FieldByName('deleted_date').AsDateTime := DeletedDate;
        D.FieldByName('deleted_by').AsInteger := DeletedBy;
        D.FieldByName('active').AsBoolean := False;
        D.Post;
        LogicDM.UpdateDateTimeToIncludeMilliseconds('jobsite_arrival', 'arrival_id', D.FieldByName('arrival_id').AsInteger, 'deleted_date', DeletedDate);
      end else
        raise EOdException.Create('You do not have permission to edit arrivals.');
    end;
  end;
end;

procedure TArrivalDM.DoAfterArrivalSave(const ArrivalID, TicketID, DamageID: Integer;
  const ArrivalDate: TDateTime; const EntryMethod: string);
begin
  if Assigned(FAfterArrivalSave) then
    FAfterArrivalSave(Self, ArrivalID, TicketID, DamageID, ArrivalDate, EntryMethod);
end;

end.
