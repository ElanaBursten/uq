unit EventSourceDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, EventSource2007, MSSqlEventSource2007,
  QMEventsNoThrift;

type
  TEventsDM = class(TDataModule)
  private
    Store: IEventStore;
  public
    constructor Create(Owner: TComponent; Conn: TADOConnection); reintroduce;

    procedure LogCompletionChangedEvent(const TicketID, LocateID,
      ChangedByEmpID: Integer; const ClientCode, CallCenter, StatusCode: string;
      const ChangedDate: TDateTime; const IsClosed: Boolean;
      const CompletionChanged: Boolean; const TicketType: string;
      const WorkloadDate: TDateTime);
  end;

implementation

uses
  OdAdoUtils, OdIsoDates;

{$R *.dfm}

{ TEventsDM }

constructor TEventsDM.Create(Owner: TComponent; Conn: TADOConnection);
begin
  inherited Create(Owner);

  Assert(Assigned(Conn));
  SetConnections(Conn, Self);
  // A TMSSQLEventStore doesn't need a Publisher.
  Store := TMSSQLEventStore.Create(nil, Conn);
end;

procedure TEventsDM.LogCompletionChangedEvent(const TicketID, LocateID,
  ChangedByEmpID: Integer; const ClientCode, CallCenter, StatusCode: string;
  const ChangedDate: TDateTime; const IsClosed: Boolean;
  const CompletionChanged: Boolean; const TicketType: string;
  const WorkloadDate: TDateTime);
var
  EventList: IEventList;
  Key: IAggrKey;
  E: TEventImpl;
begin
  EventList := TEventListImpl.Create;
  Key := TAggrKeyImpl.Create;
  Key.AggrType := atTicket;
  Key.AggrID := TicketID;
  E := TEventImpl.Create;
  E.Union := TEventUnionImpl.Create;
  if IsClosed then begin
    E.Union.LocateClosed := TLocateClosedImpl.Create;
    E.AggregateKey := Key;
    E.Union.LocateClosed.LocateID := LocateID;
    E.Union.LocateClosed.ClosedByEmpID := ChangedByEmpID;
    E.Union.LocateClosed.ClientCode := ClientCode;
    E.Union.LocateClosed.CallCenter := CallCenter;
    E.Union.LocateClosed.ClosedDate := IsoDateTimeToStr(ChangedDate);
    E.Union.LocateClosed.StatusCode := StatusCode;
    E.Union.LocateClosed.CompletedTicket := CompletionChanged;
    E.Union.LocateClosed.TicketType := TicketType;
    E.Union.LocateClosed.WorkloadDate := IsoDateTimeToStr(WorkloadDate);
  end else begin
    E.Union.LocateReopened := TLocateReopenedImpl.Create;
    E.AggregateKey := Key;
    E.Union.LocateReopened.LocateID := LocateID;
    E.Union.LocateReopened.ReopenedByEmpID := ChangedByEmpID;
    E.Union.LocateReopened.ClientCode := ClientCode;
    E.Union.LocateReopened.CallCenter := CallCenter;
    E.Union.LocateReopened.ReopenedDate := IsoDateTimeToStr(ChangedDate);
    E.Union.LocateReopened.StatusCode := StatusCode;
    E.Union.LocateReopened.ReopenedTicket := CompletionChanged;
    E.Union.LocateReopened.TicketType := TicketType;
    E.Union.LocateReopened.WorkloadDate := IsoDateTimeToStr(WorkloadDate);
  end;
  EventList.Items.Add(E);
  Store.SaveEvents(Key, EventList, -1);
end;

end.
