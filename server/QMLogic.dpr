library QMLogic;

{$IFOPT B+}
  QMLogic should never be compiled with "Complete Boolean Evaluation" on
  Please turn it off in the Project Options dialog
{$ENDIF}


// Packages: rtl;vcl;vclx;dbrtl;vcldb;adortl
// Relative Directories: ..\thirdparty\RemObjects;..\common

{#ROGEN:QMServerLibrary.rodl} // RemObjects: Careful, do not remove!


uses
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIApp,
  ISAPIThreadPool,
  SysUtils,
  ServerWebModule in 'ServerWebModule.pas' {WebModule1: TWebModule},
  QMServerLibrary_Intf in 'QMServerLibrary_Intf.pas',
  QMServerLibrary_Invk in 'QMServerLibrary_Invk.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  LogicDMu in 'LogicDMu.pas' {LogicDM: TDataModule},
  OdDataSetToXml in '..\common\OdDataSetToXml.pas',
  OdSQLBuilder in '..\common\OdSQLBuilder.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  QMService_Impl in 'QMService_Impl.pas',
  ThreadSafeLoggerU in '..\common\ThreadSafeLoggerU.pas',
  OperationTracking in 'OperationTracking.pas',
//  OdVclUtils in '..\common\OdVclUtils.pas',   //QM-127
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  ReportConfig in '..\reportengine\ReportConfig.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdAdoSqlXml in '..\common\OdAdoSqlXml.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdThreadTimer in '..\common\OdThreadTimer.pas',
  OdSystemStatus in '..\common\OdSystemStatus.pas',
  OdIntPacking in '..\common\OdIntPacking.pas',
  OdSqlXmlOutput in '..\common\OdSqlXmlOutput.pas',
  FixedClassFactory in 'FixedClassFactory.pas',
  ADODB_TLB in '..\common\ADODB_TLB.pas',
  TimeSheetExport in 'TimeSheetExport.pas',
  OdUqInternet in '..\common\OdUqInternet.pas',
  PasswordRules in '..\common\PasswordRules.pas',
//  BetterADODataSet in '..\thirdparty\BetterADO\BetterADODataSet.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  ServerAttachmentDMu in 'ServerAttachmentDMu.pas' {ServerAttachment: TDataModule},
  TicketImage in '..\common\TicketImage.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  OdFtp in '..\common\OdFtp.pas',
  HoursCalc in '..\common\HoursCalc.pas',
  HoursDataset in '..\common\HoursDataset.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  QMServerProcessU in 'QMServerProcessU.pas',
  OdUtcDates in '..\common\OdUtcDates.pas',
  PriorityRulesBase in '..\common\PriorityRulesBase.pas',
  WeeklyTimesheet in 'WeeklyTimesheet.pas',
  FaxDispositionDMu in 'FaxDispositionDMu.pas' {FaxDispositionDM: TDataModule},
  QMTempFolder in '..\common\QMTempFolder.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  BaseLogicFeatureDMu in 'BaseLogicFeatureDMu.pas' {LogicFeatureDM: TDataModule},
  TicketDMu in 'TicketDMu.pas' {TicketDM: TDataModule},
  TimesheetDMu in 'TimesheetDMu.pas' {TimesheetDM: TDataModule},
  ServerBillingDMu in 'ServerBillingDMu.pas' {ServerBillingDM: TDataModule},
  WorkOrderDMu in 'WorkOrderDMu.pas' {WorkOrderDM: TDataModule},
  DamageDMu in 'DamageDMu.pas' {DamageDM: TDataModule},
  ArrivalDMu in 'ArrivalDMu.pas' {ArrivalDM: TDataModule},
  AssetDMu in 'AssetDMu.pas' {AssetDM: TDataModule},
  ManageWorkDMu in 'ManageWorkDMu.pas' {ManageWorkDM: TDataModule},
  RemObjectsUtils in 'RemObjectsUtils.pas',
  AddinInfoDMu in 'AddinInfoDMu.pas' {AddinInfoDM: TDataModule},
  NotesDMu in 'NotesDMu.pas' {NotesDM: TDataModule},
  UQDbConfig in '..\common\UQDbConfig.pas',
  EventSourceDMu in 'EventSourceDMu.pas' {EventsDM: TDataModule},
  uADSI in 'uADSI.pas',
  QMConst in '..\client\QMConst.pas';

{$R RODLFile.RES} // RemObjects: Careful, do not remove!
{$R '..\QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

function GetServerIniName: string;
begin
  Result := ExtractFilePath(GetThisModulePath) + 'QMServer.ini';
end;

begin
  LogicDmIniName := GetServerIniName;
  FaxDMIniName := LogicDmIniName;
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TWebModule1, WebModule1);
  Application.Run;
end.

