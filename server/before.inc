  if FBeforeExecuting then
    raise Exception.Create('The code in before.inc should never be nested.  Only call it once per operation.');
  FBeforeExecuting := True;
  GetDM;
  DM.Op := GetOperationTracker.AddOperation;
  try
    DM.Op.Phase := 'Connecting to DB';
    try
      DM.Connect;
    except
      on E1: Exception do begin
        try
          // Log the real error
          DM.Op.Log(E1.Message);
        except
          // There is nothing we can do, if we get an error while
          // trying to log the error.
        end;
        // Then raise a friendlier one
        raise Exception.Create('The Q Manager system encountered an error connecting to the database.  Please try again in a few minutes.');
      end;
    end;
    DM.Op.Phase := 'Setting up connection';
    try  // except handle error by logging it
      try
        DM.Conn.IsolationLevel := DM.DesiredIsolation;
        DM.Conn.BeginTrans;
        try // commit or roll back
          DM.Op.Phase := 'Checking security';
          CheckSecurity(SecSession);
          DM.Op.Phase := 'Processing';

