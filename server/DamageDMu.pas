unit DamageDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, ADODB, DB, NotesDMu, 
  QMServerLibrary_Intf, RemObjectsUtils;

const
  FEATURE_NAME = 'Damages';
  DBLDash = '--';     //EB QMANTWO-615

type
  TDamageDM = class(TLogicFeatureDM)
    DamageBill: TADODataSet;
    NextUQDamageID: TADOStoredProc;
    InsertDamage: TADOCommand;
    DamageThirdParty: TADODataSet;
    DamageEstimate: TADODataSet;
    DamageInvoice: TADODataSet;
    DamageLitigation: TADODataSet;
    UpdateDamageApproval: TADOCommand;
    DocumentCommand: TADOCommand;
    UpdateDamage3: TADOCommand;
    updateFacilities: TADOCommand;
    Damage: TADODataSet;
  private
    NotesDM: TNotesDM;
    function GetNotesDM: TNotesDM;
    procedure SendDamagePendingApprovalNotification(const DamageId: Integer; const ProfitCenter: string);
    function GetDamageApprovingManager(const  ProfitCenterCode: string): Integer;
    function GetDamageApprovalEmailAddr(ApprovingMgrId: Integer): string;
    function GetDefaultEstimateForFacility(var Amount: Double; const FacType, FacSize, FacMaterial, ProfitCenter, UtilityCo: string): Boolean;
  public
    destructor Destroy; override;
    function GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
    function GetUQDamageIDForDamageID(const DamageID: Integer): Integer;
    procedure SetDamagePriority(const DamageID, PriorityID: Integer);
    function GetPriorityNameFromDamage(const DamageID: Integer): string;
    function DamageSearch9(const TicketNumber: string;
      const UQDamageID: Integer; const DamageDateFrom, DamageDateTo,
      DueDateFrom, DueDateTo: TDateTime; const Investigator, Excavator, Location,
      City, State, ProfitCenter, DamageType, UtilityCoDamaged, ClientClaimID,
      WorkToBeDone: string; const IncludeUQResp, IncludeExcvResp,
      IncludeUtilityResp: Boolean; const InvestigatorID, Attachments,
      Invoices: Integer; const ClaimStatus: string;
      const IncludeEstimates: Boolean; const InvoiceCode, ExcavationType,
      Locator: string): string;
    function DamageInvoiceSearch3(const InvoiceNumber, Company, City,
      State: string; const UQDamageID: Integer; const Amount: Currency;
      const Comment: string; const RecvDateFrom, RecvDateTo, InvoiceDateFrom,
      InvoiceDateTo: TDateTime; const InvoiceCode: string): string;
    function DamageLitigationSearch3(const UQDamageID, LitigationID: Integer;
      const FileNumber, Plaintiff: string; const CarrierID: Integer;
      const Attorney, Party, Demand, Accrual, Settlement: string;
      const ClosedDateFrom, ClosedDateTo: TDateTime): string;
    function DamageThirdPartySearch(const UQDamageID, ThirdPartyID,
      CarrierID: Integer; const Claimant, Address, City, State, ClaimStatus,
      ProfitCenter, UtilityCoDamaged: string; const NotifiedDateFrom,
      NotifiedDateTo: TDateTime): string;
    function GetDamage2(const DamageID: Integer): string;
    function GetDamageInvoice(const InvoiceID: Integer): string;
    function GetDamageLitigation(const LitigationID: Integer): string;
    function GetDamageThirdParty(const ThirdPartyID: Integer): string;
    function ApproveDamage(const DamageID: Integer; const ApprovedDateTime: TDateTime): string;
    function SaveDamage3(const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
    function SaveDamageBill(const Params: NVPairListList): GeneratedKeyList;
    function SaveDamageThirdParty(const Params: NVPairListList): GeneratedKeyList;
    function SaveDamageEstimate2(const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
    function SaveDamageInvoice(const Params: NVPairListList): GeneratedKeyList;
    function SaveDamageLitigation(const Params: NVPairListList): GeneratedKeyList;
    procedure SaveDamagePriority(const DamageID: Integer; const EntryDate: TDateTime; const Priority: Integer);
    function SaveRequiredDocuments(const RequiredDocs: RequiredDocumentList): GeneratedKeyList;
    procedure MoveDamages(const Damages: IntegerList; const FromInvestigatorID, ToInvestigatorID: Integer);
    function ClearDashes(var AValue: string): string; overload;
    function ClearDashes(AVariant: Variant): string; overload;
  end;

implementation

uses QMConst, LogicDMu, OdSqlBuilder, OdDataSetToXML, OdMiscUtils, OdExceptions,
  OdIsoDates, OdDbUtils, JclStrings;

{$R *.dfm}

destructor TDamageDM.Destroy;
begin
  FreeAndNil(NotesDM);
  inherited;
end;


function TDamageDM.GetNotesDM: TNotesDM;
begin
  if NotesDM = nil then
    NotesDM := TNotesDM.Create(nil, LogicDM);
  Result := NotesDM;
end;

function TDamageDM.GetDamageApprovalEmailAddr(ApprovingMgrId: Integer): string;
const
  GetEmailSelect = 'select email_destination from notification_email_user where emp_id=:emp_id and email_type = :email_type';
  DamageApprovalEmailType = 'damage_approval';
var
  Data: TADODataSet;
begin
  Result := '';
  Data := TADODataSet.Create(Self);
  try
    Data.Connection := LogicDM.Conn;
    Data.CommandText := GetEmailSelect;
    Data.Parameters.ParamValues['emp_id'] := ApprovingMgrId;
    Data.Parameters.ParamValues['email_type'] := DamageApprovalEmailType;
    Data.Open;
    Result := Data.FieldByName('email_destination').AsString;
    Data.Close;
  finally
    FreeAndNil(Data);
  end;
end;

function TDamageDM.GetDamageApprovingManager(const ProfitCenterCode: string): Integer;
const
  ManagerSelect = 'select manager_emp_id from profit_center where pc_code = :pc_code';
var
  Data: TADODataSet;
begin
  Data := TADODataSet.Create(Self);
  try
    try
      Data.Connection := LogicDM.Conn;
      Data.CommandText := ManagerSelect;
      Data.Parameters.ParamValues['pc_code'] := ProfitCenterCode;
      Data.Open;
      Result := Data.FieldByName('manager_emp_id').AsInteger;
      Data.Close;
    except on Exception do
      result := -1;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDamageDM.SendDamagePendingApprovalNotification(const DamageId: Integer; const ProfitCenter: string);
var
  DestEmail: string;
  ApprovingMgrId: Integer;
  Subject, Body, FromEMail: string;
begin
  ApprovingMgrId := GetDamageApprovingManager(ProfitCenter);
  if (ApprovingMgrId = -1) then
    Op.Log('No Approving Manager Found. Data has been saved, but no notification has been sent.'#13#10 + 'Please contact your administrator.');
  FromEMail:=''; //QM-9  gets address from INI
  DestEmail := GetDamageApprovalEmailAddr(ApprovingMgrId);
  if (DestEmail = '') then
    Op.Log('E-mail address for approving manager not defined. Data has been saved, but no notification has been sent.'#13#10 + 'Please contact your administrator.')
  else begin
    Subject := 'Damage Pending Approval Notification. Damage #' + IntToStr(DamageId);
    Body    := 'Damage Investigation Completed. Ready for review and approval.';
    LogicDM.SendEmailWithIniSettingsExp('DamageEmail', FromEMail, DestEmail, Subject, Body);
  end;
end;

function TDamageDM.GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
const
  Select = 'SELECT damage_id FROM damage where uq_damage_id=%d';
var
  Data: TDataset;
begin
  Result := -1;
  Data := LogicDM.GetDataSetforSQL(Format(Select, [UQDamageID]));
  if not Data.IsEmpty then
    Result := Data.Fields[0].Value;
  Data.Close;
end;

function TDamageDM.GetUQDamageIDForDamageID(const DamageID: Integer): Integer;
const
  Select = 'SELECT uq_damage_id FROM damage where damage_id=%d';
var
  Data: TDataset;
begin
  Result := -1;
  Data := LogicDM.GetDataSetforSQL(Format(Select, [DamageID]));
  if not Data.IsEmpty then
    Result := Data.Fields[0].Value;
  Data.Close;
end;

procedure TDamageDM.SetDamagePriority(const DamageID, PriorityID: Integer);
const
  SQL = 'update damage set work_priority_id = %d where damage_id = %d';
var
  RecsAffected: Integer;
begin
  RecsAffected := LogicDM.ExecuteSQL(Format(SQL, [PriorityID, DamageID]));
  Assert(RecsAffected = 1, 'The damage''s priority could not be changed in SetDamagePriority.');
end;

function TDamageDM.GetPriorityNameFromDamage(const DamageID: Integer): string;
const
  SelectPriority = 'select r.description from damage ' +
    'inner join reference r on damage.work_priority_id = r.ref_id ' +
    'where damage_id = %d';
var
  D: TDataSet;
begin
  Result := '';
  D := LogicDM.GetDataSetForSQL(Format(SelectPriority, [DamageID]));
  if not D.Eof then
    Result := D.FieldByName('Description').AsString;
  D.Close;
end;

function TDamageDM.GetDefaultEstimateForFacility(var Amount: Double; const FacType,
  FacSize, FacMaterial, ProfitCenter, UtilityCo: string): Boolean;
const
  LocateFields = 'facility_type;facility_size;facility_material;profit_center;utility_co_name';
  Select = 'SELECT dde.*, COALESCE(ref.description, ''*'') AS utility_co_name ' +
    'FROM damage_default_est dde LEFT JOIN reference ref ON ref.ref_id = dde.utility_co_id';
var
  Data: TDataSet;

  function GetMatch(var Amount: Double; const FacType, FacSize, FacMaterial, ProfitCenter, UtilityCo: string): Boolean;
  begin
    Result := False;
    if Data.Locate(LocateFields, VarArrayOf([FacType, FacSize, FacMaterial, ProfitCenter, UtilityCo]), []) then begin
      Amount := Data.FieldByName('default_amount').AsFloat;
      Result := True;
    end;
  end;

begin
  Amount := -1;
  Data := LogicDM.GetDataSetforSQL(Select);
  if not GetMatch(Amount, FacType, FacSize, FacMaterial, ProfitCenter, UtilityCo) then
  if not GetMatch(Amount, FacType, FacSize, FacMaterial, ProfitCenter, '*') then
  if not GetMatch(Amount, FacType, FacSize, FacMaterial, '*', UtilityCo) then
  if not GetMatch(Amount, FacType, FacSize, FacMaterial, '*', '*') then
  if not GetMatch(Amount, FacType, FacSize, '*', ProfitCenter, UtilityCo) then
  if not GetMatch(Amount, FacType, FacSize, '*', ProfitCenter, '*') then
  if not GetMatch(Amount, FacType, FacSize, '*', '*', UtilityCo) then
  if not GetMatch(Amount, FacType, FacSize, '*', '*', '*') then
  if not GetMatch(Amount, FacType, '*', FacMaterial, ProfitCenter, UtilityCo) then
  if not GetMatch(Amount, FacType, '*', FacMaterial, ProfitCenter, '*') then
  if not GetMatch(Amount, FacType, '*', FacMaterial, '*', UtilityCo) then
  if not GetMatch(Amount, FacType, '*', FacMaterial, '*', '*') then
  if not GetMatch(Amount, FacType, '*', '*', ProfitCenter, UtilityCo) then
  if not GetMatch(Amount, FacType, '*', '*', ProfitCenter, '*') then
  if not GetMatch(Amount, FacType, '*', '*', '*', UtilityCo) then
  if not GetMatch(Amount, FacType, '*', '*', '*', '*') then
  else GetMatch(Amount, '*', '*', '*', '*', '*');
  Data.Close;
  Result := Amount > 0;
end;

//DamageSearch9 introduced in build 6994
function TDamageDM.DamageSearch9(const TicketNumber: string;
  const UQDamageID: Integer; const DamageDateFrom, DamageDateTo,
  DueDateFrom, DueDateTo: TDateTime; const Investigator, Excavator,
  Location, City, State, ProfitCenter, DamageType, UtilityCoDamaged,
  ClientClaimID, WorkToBeDone: string; const IncludeUQResp,
  IncludeExcvResp, IncludeUtilityResp: Boolean; const InvestigatorID,
  Attachments, Invoices: Integer; const ClaimStatus: string;
  const IncludeEstimates: Boolean; const InvoiceCode,
  ExcavationType: string; const Locator: string): string;

const
  AttachmentCountClause = '(select count(*) from attachment a where a.foreign_id=d.damage_id and a.foreign_type=2 and a.active=1)';
  InvoiceCountClause = '(select count(*) from damage_invoice i where i.damage_id=d.damage_id)';
  EstimateClause =
    ', (select top 1 amount from damage_estimate where modified_date = '+
    '(select max(modified_date) from damage_estimate where damage_estimate.damage_id = d.damage_id) '+
    'and damage_estimate.damage_id = d.damage_id) as amount ';
  LocatorNameClause =
     '   case ' +
     '     when d.locator_id is not null then ' +
     '       (select e.short_name from employee e where e.emp_id = d.locator_id) ' +
     '     when d.ticket_id is null then ''-'' ' +
     '     when (d.client_id is not null) and (d.ticket_id is not null) then ' +
     '       (select top 1 e.short_name from employee e ' +
     '       inner join locate l on e.emp_id = l.assigned_to_id ' +
     '           where l.ticket_id = d.ticket_id ' +
     '         and l.client_id = d.client_id ' +
     '         and l.active = 1) ' +
     '     when (d.client_id is null) then ' +
     '       (select top 1 e.short_name from employee e ' +
     '           inner join locate l on e.emp_id = l.assigned_to_id ' +
     '           where l.ticket_id = d.ticket_id ' +
     '         and l.active = 1) ' +
     '     else ''-'' end as locator_name ';
  Select =
    'select top %d damage_id, uq_damage_id, damage_type, d.ticket_id, damage_date, invoice_code, '+
    'excavator_company, city, location, county, client_claim_id, t.work_type, '+    //QM-117 SR
    'investigator_id, t.ticket_number, e.short_name, claim_status, '+
    'profit_center, state, utility_co_damaged, uq_notified_date, d.due_date, '+
    AttachmentCountClause + ' as attachments, '+
    InvoiceCountClause + ' as invoices, '+
    'case '+
    '  when (exc_resp_code is not null) and (uq_resp_code is not null) then ''Both'' '+
    '  when (exc_resp_code is not null) and (uq_resp_code is null) then ''Excavator'' '+
    '  when (exc_resp_code is null) and (uq_resp_code is not null) then ''Our'' '+
    '  else ''None'' '+
    'end as [r], '+
    'uq_resp_code, exc_resp_code, spc_resp_code, t.work_description, '+
    'case when ((d.size_type is null) or (d.size_type ='''')) then d.facility_type + '' '' + d.facility_size '+
    '  else d.size_type end as size_type, ' +
    ' e1.short_name as added_by_name, ' +
    LocatorNameClause +
    ' %s ' + // EstimateClause placeholder
    'from damage d %s'+
    'left join ticket t on d.ticket_id=t.ticket_id '+
    'left join employee e on d.investigator_id=e.emp_id '+
    'left join employee e1 on d.added_by=e1.emp_id ';
  LocatorSQL =
     ' (select '+
     '    case '+
     '      when (d.locator_id is not null) then ' +
     '        (select top 1 1 from employee e ' +
     '        where e.emp_id = d.locator_id ' +
     '          and e.short_name like ''%s'') '+
     '      when (d.ticket_id is null) then 0 '+
     '      when ((d.client_id is not null) and (d.ticket_id is not null)) then '+
     '      (select top 1 1 '+
     '          from employee e '+
     '            inner join locate l on e.emp_id = l.assigned_to_id '+
     '          where '+
     '            l.ticket_id = d.ticket_id and '+
     '            l.client_id = d.client_id and '+
     '            l.active = 1 and '+
     '            e.short_name  like ''%s'') '+
     '       when (d.client_id is null) then '+
     '         (select top 1 1 '+
     '          from employee e '+
     '          inner join locate l on e.emp_id = l.assigned_to_id '+
     '          where l.ticket_id = d.ticket_id and '+
     '                l.active = 1 and '+
     '                e.short_name  like ''%s'') '+
     '     end) = 1 ';
  MaxDamages = 1200;

var
  SQL: TOdSqlBuilder;
  EstimateStr: string;
  PivotDate: TDateTime;
  OldDataSet, NewDataSet: TDataSet;
  PCenter, LimitationString: string;
  CanSearchAll: Boolean;
  LimitInvestigatorID: Integer;
  FullTextFields: string;
begin
  // We may want to enforce a date range later
  {
  if (DamageDateFrom + DamageDateTo + DueDateFrom + DueDateTo) < 1 then
    raise Exception.Create('You must specify a date range when searching damages');
  if MonthsBetween(DueDateFrom, DueDateTo) > 12 then
    raise Exception.Create('The maximum due date search range is 1 year');
  if MonthsBetween(DamageDateFrom, DamageDateTo) > 12 then
    raise Exception.Create('The maximum damage date search range is 1 year');
  }

  LimitationString := '';
  CanSearchAll := LogicDM.CanI(RightDamagesViewAll, LimitationString);

  LimitInvestigatorID := InvestigatorID;
  // Enforce "Me Only" limitation:
  if not CanSearchAll then
    LimitInvestigatorID := LogicDM.LoggedInEmpID;

  // Enforce profit center limitation if present:  no longer used.  QMANTWO-412
  // limited on client side   QMANTWO-412
//  PCenter := ProfitCenter;
//  if not StringInArray(Trim(LimitationString), ['', '-']) then
//    PCenter := LimitationString;

  FullTextFields := LogicDM.GetStringIniSetting('FullTextSearch', 'damage', '');

  SQL := TOdSqlBuilder.Create;
  try
    StrToStrings(FullTextFields, ',', SQL.FullTextSearchColumns, False);
    SQL.TableAlias := 'd';
    SQL.AddExactStringFilter(' ticket_number ', TicketNumber);
    SQL.AddIntFilterUnlessZero(' uq_damage_id ', UQDamageID);
    SQL.AddDateTimeRangeFilter(' damage_date ', DamageDateFrom, DamageDateTo);
    SQL.AddDateTimeRangeFilter(' d.due_date ', DueDateFrom, DueDateTo);
    SQL.AddKeywordSearch(' e.short_name ', Investigator);
    SQL.AddKeywordSearch(' d.excavator_company ', Excavator);
    SQL.AddKeywordSearch(' d.location ', Location);
    SQL.AddKeywordSearch(' d.city', City);
    SQL.AddExactStringFilter(' profit_center ', ProfitCenter); //QMANTWO-412
    SQL.AddExactStringFilter(' state', State);
    // PENDING APPROVAL has replaced COMPLETED, but both status will be out there for awhile..
    //   so we need to return both....
    if (DamageType = DamageTypePendingApproval) then
      SQL.AddWhereCondition('((damage_type = ''' + DamageTypePendingApproval + ''') or (damage_type = ''' + DamageTypeCompleted  + '''))')
    else
      SQL.AddExactStringFilter(' damage_type ', UpperCase(DamageType));
    SQL.AddKeywordSearch(' d.utility_co_damaged ', UtilityCoDamaged);
    SQL.AddPrefixSearch(' d.client_claim_id ', ClientClaimID);
    SQL.AddKeywordSearch(' t.work_description ', WorkToBeDone);
    SQL.AddKeywordSearch(' d.claim_status ', ClaimStatus);
    SQL.AddKeywordSearch(' d.invoice_code ', InvoiceCode);
    SQL.AddExactStringFilter(' d.excavation_type ', ExcavationType);

    if IncludeUQResp then
      SQL.AddWhereCondition(' d.uq_resp_code is not null');
    if IncludeExcvResp then
      SQL.AddWhereCondition(' d.exc_resp_code is not null');
    if IncludeUtilityResp then
      SQL.AddWhereCondition(' d.spc_resp_code is not null');

    if Trim(Locator) <> '' then
      SQL.AddWhereConditionFmt(LocatorSQL,['%'+Locator+'%','%'+Locator+'%','%'+Locator+'%']);

    if Attachments = Ord(acHas) then
      SQL.AddNumericFilter(AttachmentCountClause, '> 0')
    else if Attachments = Ord(acDoesNotHave) then
      SQL.AddNumericFilter(AttachmentCountClause, '= 0');

    if Invoices = Ord(icHas) then
      SQL.AddNumericFilter(InvoiceCountClause, '> 0')
    else if Invoices = Ord(icDoesNotHave) then
      SQL.AddNumericFilter(InvoiceCountClause, '= 0');

    SQL.CheckFilterCount;

    SQL.AddWhereCondition('d.active=1');
    SQL.AddIntFilterUnlessZero('d.investigator_id', LimitInvestigatorID);

    if IncludeEstimates then
      EstimateStr :=  EstimateClause
    else
      EstimateStr := '';

    OldDataSet := nil;
    if (UQDamageID = 0) and (not ForceMainDBSearch(FEATURE_NAME)) then begin
      ConnectRepDM;
      PivotDate := ReportingDatabasePivot;

      SetPhase('Historical query in ' + RepDM.DBName);
      RepDM.SearchQuery.CommandText := Format(Select, [MaxDamages, EstimateStr, '']) + SQL.WhereClause;
      Op.Detail := SQL.WhereClause;
      OldDataSet := RepDM.SearchQuery;
      OldDataSet.Open;

      SetPhase('Current data query in Main DB');
      SQL.AddDateTimeGreaterFilter('d.modified_date', PivotDate);
      LogicDM.SearchQuery.CommandText := Format(Select, [MaxDamages, EstimateStr, ' with (index(damage_modified_date))']) + SQL.WhereClause;
      Op.Detail := SQL.WhereClause;
      NewDataSet := LogicDM.SearchQuery;
      NewDataSet.Open;
    end else begin
      // Fast damage ID and forced main DB searches
      LogicDM.SearchQuery.CommandText := Format(Select, [MaxDamages, EstimateStr, '']) + SQL.WhereClause;
      SetPhase('Running query');
      Op.Detail := SQL.WhereClause;
      NewDataSet := LogicDM.SearchQuery;
      NewDataSet.Open;
    end;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([NewDataSet, OldDataSet], MaxDamages, 'damage_id');
  finally
    FreeAndNil(SQL);
  end;
end;

function TDamageDM.ClearDashes(var AValue: string): string;
begin
  {EB - If a value is coming over as '--', this will clear it}
  if AValue = DblDash then
    AValue := '';
  Result := AValue;
end;


function TDamageDM.ClearDashes(AVariant: Variant): string;
var
  InputStr: string;
begin
  InputStr := VartoStr(AVariant);
  Result := ClearDashes(InputStr);
end;

function TDamageDM.DamageInvoiceSearch3(const InvoiceNumber, Company, City,
  State: string; const UQDamageID: Integer; const Amount: Currency;
  const Comment: string; const RecvDateFrom, RecvDateTo, InvoiceDateFrom,
  InvoiceDateTo: TDateTime; const InvoiceCode: string): string;
const
  Select =
    'select di.invoice_id, di.invoice_num, di.company, di.received_date, ' +
    '  di.amount, di.damage_city, di.damage_state, d.damage_id, d.uq_damage_id, ' +
    '  di.comment, di.cust_invoice_date, di.invoice_code ' +
    'from damage_invoice di (NOLOCK) ' +
    'inner join damage d (NOLOCK) on d.damage_id=di.damage_id ';
var
  SQL: TOdSqlBuilder;
begin
  SQL := TOdSqlBuilder.Create;
  try
    SQL.AddPrefixSearch('di.invoice_num', InvoiceNumber);
    SQL.AddKeywordSearch('di.company', Company);
    SQL.AddKeywordSearch('di.damage_city', City);
    SQL.AddExactStringFilter('di.damage_state', State);
    SQL.AddIntFilterUnlessZero('d.uq_damage_id', UQDamageID);
    if Amount > 0 then
      SQL.AddNumericFilter('di.amount', FloatToStr(Amount));
    SQL.AddKeywordSearch('di.comment', Comment);
    SQL.AddDateTimeRangeFilter('di.received_date', RecvDateFrom, RecvDateTo);
    SQL.AddDateTimeRangeFilter('di.cust_invoice_date', InvoiceDateFrom, InvoiceDateTo);
    SQL.AddExactStringFilter('di.invoice_code', InvoiceCode);

    SQL.CheckFilterCount;

    LogicDM.SearchQuery.CommandText := Select + SQL.WhereClause;
    SetPhase('Running query');
    Op.Detail := SQL.WhereClause;
    LogicDM.SearchQuery.Open;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([LogicDM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    LogicDM.SearchQuery.Close;
  end;
end;

function TDamageDM.DamageLitigationSearch3(const UQDamageID: Integer;
  const LitigationID: Integer; const FileNumber, Plaintiff: string;
  const CarrierID: Integer; const Attorney, Party, Demand, Accrual,
  Settlement: string; const ClosedDateFrom,
  ClosedDateTo: TDateTime): string;
const
  Select =
    'select lit.litigation_id, d.uq_damage_id, lit.file_number, lit.closed_date, lit.plaintiff, lit.accrual, lit.demand, '+
    'lit.settlement, car.name as carrier, att.description as attorney, pty.description as party '+
    'from damage_litigation lit (NOLOCK) '+
    ' inner join damage d (NOLOCK) on d.damage_id = lit.damage_id ' +
    ' left join carrier car (NOLOCK) on lit.carrier_id = car.carrier_id '+
    ' left join reference att on att.code = lit.attorney '+
    ' left join reference pty on pty.code = lit.party ';
var
  SQL: TOdSqlBuilder;
begin

  SQL := TOdSqlBuilder.Create;
  try
    SQL.AddExactStringFilter('lit.file_number', FileNumber);
    SQL.AddIntFilterUnlessZero('d.uq_damage_id', UQDamageID);
    SQL.AddIntFilterUnlessZero('lit.litigation_id', LitigationID);
    SQL.AddDateTimeRangeFilter('lit.closed_date', ClosedDateFrom, ClosedDateTo);
    SQL.AddKeywordSearch('lit.plaintiff', Plaintiff);
    SQL.AddIntFilterUnlessZero('lit.carrier_id', CarrierID);
    SQL.AddExactStringFilter('lit.attorney', Attorney);
    SQL.AddExactStringFilter('lit.party', Party);
    SQL.AddNumericFilter('lit.accrual', Accrual);
    SQL.AddNumericFilter('lit.demand', Demand);
    SQL.AddNumericFilter('lit.settlement', Settlement);
    SQL.CheckFilterCount;

    LogicDM.SearchQuery.CommandText := Select + SQL.WhereClause;
    SetPhase('Running query');
    Op.Detail := SQL.WhereClause;
    LogicDM.SearchQuery.Open;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([LogicDM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    LogicDM.SearchQuery.Close;
  end;
end;

function TDamageDM.DamageThirdPartySearch(const UQDamageID, ThirdPartyID,
  CarrierID: Integer; const Claimant, Address, City, State, ClaimStatus,
  ProfitCenter, UtilityCoDamaged: string; const NotifiedDateFrom,
  NotifiedDateTo: TDateTime): string;
const
  Select =
    'select par.third_party_id, d.uq_damage_id, par.claimant, par.uq_notified_date, '+
    'par.address1, par.city, par.state, car.name as carrier, d.profit_center, d.utility_co_damaged '+
    'from damage_third_party par (NOLOCK) '+
    ' inner join damage d (NOLOCK) on d.damage_id = par.damage_id ' +
    ' left join carrier car (NOLOCK) on par.carrier_id = car.carrier_id ';
var
  SQL: TOdSqlBuilder;
begin

  SQL := TOdSqlBuilder.Create;
  try
    SQL.AddIntFilterUnlessZero('d.uq_damage_id', UQDamageID);
    SQL.AddIntFilterUnlessZero('par.third_party_id', ThirdPartyID);
    SQL.AddDateTimeRangeFilter('par.uq_notified_date', NotifiedDateFrom, NotifiedDateTo);
    SQL.AddKeywordSearch('par.claimant', Claimant);
    SQL.AddKeywordSearch('par.address1', Address);
    SQL.AddKeywordSearch('par.city', City);
    SQL.AddExactStringFilter('par.state', State);
    SQL.AddIntFilterUnlessZero('par.carrier_id', CarrierID);
    SQL.AddKeywordSearch('d.utility_co_damaged', UtilityCoDamaged);
    SQL.AddExactStringFilter('d.profit_center', ProfitCenter);
    SQL.AddKeywordSearch('par.claim_status', ClaimStatus);
    SQL.CheckFilterCount;

    LogicDM.SearchQuery.CommandText := Select + SQL.WhereClause;
    SetPhase('Running query');
    Op.Detail := SQL.WhereClause;
    LogicDM.SearchQuery.Open;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([LogicDM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    LogicDM.SearchQuery.Close;
  end;
end;

function TDamageDM.GetDamage2(const DamageID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_damage5', '@DamageID', DamageID);
end;

function TDamageDM.GetDamageInvoice(const InvoiceID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_invoice2', '@InvoiceID', InvoiceID);
end;

function TDamageDM.GetDamageLitigation(const LitigationID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_litigation2', '@LitigationID', LitigationID);
end;

function TDamageDM.GetDamageThirdParty(const ThirdPartyID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_third_party2', '@ThirdPartyID', ThirdPartyID);
end;

function TDamageDM.ApproveDamage(const DamageID: Integer; const ApprovedDateTime: TDateTime): string;
var
  D: TADODataset;
  RecordsAffected: Integer;
begin
  if not (LogicDM.CanI(RightDamagesV2) or LogicDM.CanI(RightDamages)) then begin
    Result := 'You do not have permission to update damage investigations.';
    Exit;
  end;
  if not LogicDM.CanI(RightDamagesApprove) then begin
    Result := 'You do not have permission to approve damage investigations.';
    Exit;
  end;

  D := Damage;
  D.Close;
  D.CommandText := 'select damage_type, approved_by_id, approved_datetime ' +
    'from damage where damage_id = :damage_id';
  D.Parameters.ParamValues['damage_id'] := DamageID;
  D.ExecuteOptions := [];
  D.Open;

  Result := '';

  if D.RecordCount = 0 then
    Result := 'Damage record not found.'
  else if (D.FieldByName('damage_type').AsString = DamageTypeApproved) and
    not D.FieldByName('approved_by_id').IsNull and
    not D.FieldByName('approved_datetime').IsNull then
    Result := 'Damage investigation already approved.'
  else if (D.FieldByName('damage_type').AsString = DamageTypeApproved) or
    not D.FieldByName('approved_by_id').IsNull or
    not D.FieldByName('approved_datetime').IsNull then
    Result := 'Damage investigation is not approved, but has a damage type of ' +
      DamageTypeApproved + ', an approval date, and/or an approved by id. ' +
      'This is not valid, and the investigation cannot be approved.'
  else if (D.FieldByName('damage_type').AsString <> DamageTypeCompleted) and
    (D.FieldByName('damage_type').AsString <> DamageTypePendingApproval) then
    Result := 'Damage investigation must be ' + DamageTypeCompleted + ' or ' +
      DamageTypePendingApproval + ' to approve, but it currently is ''' +
      D.FieldByName('damage_type').AsString + '''.'
  else begin
    UpdateDamageApproval.Parameters.ParamValues['approved'] := DamageTypeApproved;
    UpdateDamageApproval.Parameters.ParamValues['completed'] := DamageTypeCompleted;
    UpdateDamageApproval.Parameters.ParamValues['pending_approval'] := DamageTypePendingApproval;
    UpdateDamageApproval.Parameters.ParamValues['damage_id'] := DamageID;
    UpdateDamageApproval.Parameters.ParamValues['approved_by_id'] := LogicDM.LoggedInEmpID;
    UpdateDamageApproval.Parameters.ParamValues['approved_datetime'] := ApprovedDateTime;
    UpdateDamageApproval.Parameters.ParamValues['modified_by'] := LogicDM.LoggedInEmpID;
    UpdateDamageApproval.Execute(RecordsAffected, EmptyParam);
    if RecordsAffected <> 1 then
      Result := 'Damage investigation was not approved.';
  end;

  D.Close;
end;

function TDamageDM.SaveDamageBill(const Params: NVPairListList): GeneratedKeyList;
const
  Existing = 'select * from damage_bill where damage_id = :damage_id'; // There should be at most 1 per damage
  Updating = 'update damage_bill set billable=%d, billing_period_date=''%s'', modified_by=%d, ' +
    'modified_date=getdate() where damage_bill_id=%d';
var
  DamageBillPairList: NVPairList;
  Index: Integer;
  DamageBillID: Integer;
  NewDamageBillID: Integer;
  DamageID: Integer;
  Billable: Boolean;
  BillingPeriodDate: TDateTime;
  D: TADODataset;
  InsertMode: Boolean;

  procedure UpdateDamageBill;
  begin
    SetPhase('Editing existing damage bill');
    LogicDM.ExecuteSQL(Format(Updating, [Ord(Billable), DateToStr(BillingPeriodDate), LogicDM.LoggedInEmpID, DamageBillID]));
  end;

begin
  if not LogicDM.CanI(RightDamagesBillDamage) then
    raise EOdNotAllowed.Create('You do not have permission to bill damages.');

  NewDamageBillID := -1;
  Result := GeneratedKeyList.Create;
  D := DamageBill;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage Bill %d of %d', [Index + 1, Params.Count]));
    DamageBillPairList := Params[Index];
    DamageBillID := NVPairInteger(DamageBillPairList, 'damage_bill_id', bvError);
    DamageID := NVPairInteger(DamageBillPairList, 'damage_id', bvError);
    Billable := NVPairBoolean(DamageBillPairList, 'billable', bvError);
    BillingPeriodDate := NVPairDateTime(DamageBillPairList, 'billing_period_date', bvError);

    InsertMode := (DamageBillID < 0);
    D.Close;
    if InsertMode then begin
      D.CommandText := Existing;
      D.Parameters.ParamValues['damage_id'] := DamageID;
      D.Open;
      if D.RecordCount = 0 then begin
        SetPhase('Editing damage bill');
        D.Insert;
        D.FieldByName('damage_id').Value := DamageID;
        D.FieldByName('billable').Value := Billable;
        D.FieldByName('billing_period_date').Value := BillingPeriodDate;
        D.FieldByName('modified_by').Value := LogicDM.LoggedInEmpId;
        D.Post;
      end
      else if D.RecordCount > 1 then
        raise Exception.Create('Multiple existing damage bills match new damage bill save data');
      NewDamageBillID := D.FieldByName('damage_bill_id').AsInteger;
    end
    else
      UpdateDamageBill;

    if DamageBillID < 0 then begin
      Assert(NewDamageBillID > 0, 'Negative new damage bill ID');
      AddIdentity(Result, 'damage_bill', DamageBillID, NewDamageBillID);
    end;
  end;
end;

function TDamageDM.SaveDamage3(const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
const
  Existing = 'select * from damage where added_by=:added_by and active=1 and profit_center=:profit_center and uq_notified_date=%s';
  ExistingByID = 'select * from damage where damage_id=:damage_id';
  InsertDamageEstimate = 'insert into damage_estimate (damage_id, third_party, emp_id, amount, comment, added_date) ' +  //QMANTWO-615 req
    'values (%d, 0, %d, %f, ''%s'', ''%s'')';  //QMANTWO-615 req
var
  DamagePairList: NVPairList;
  Index: Integer;
  DamageID: Integer;
  NewDamageType : string;
  NotifiedDate: TDateTime;
  ModifiedDate: TDateTime;
  AddedBy: Integer;
  FacilityType: string;
  FacilitySize: string;
  FacilityMaterial: string;

  FacilityType_2: string; //QMANTWO-615
  FacilitySize_2: string; //QMANTWO-615
  FacilityMaterial_2: string; //QMANTWO-615

  FacilityType_3: string; //QMANTWO-615
  FacilitySize_3: string; //QMANTWO-615
  FacilityMaterial_3: string; //QMANTWO-615

  ProfitCenter: string;
  UtilityCo: string;
  InvoiceCode: string;
  NewDamageID: Integer;
  UQDamageID: Integer;
  D: TADODataset;
  EstimateLocked: Boolean;
  UserChangedProfitCenter: Boolean;
  State : String;
  County: String;
  City: String;
  InsertEstimate: Boolean;
  DamageUpdated: Boolean;

  function GetNextUQDamageID: Integer;
  begin
    NextUQDamageID.Parameters.ParamValues['@NAME'] := 'NextDamageID';
    NextUQDamageID.ExecProc;
    try
      Result := NextUQDamageID.Parameters.ParamValues['@RETURN_VALUE'];
    finally
      NextUQDamageID.Close;
    end;
  end;

  procedure AddDamage;
  begin
    SetPhase('Inserting damage');
    {EB - The NVPairList is more dynamic than the structure lists and it will match it by the name of the field}
    AssignParamsFromNVPairList(InsertDamage.Parameters, DamagePairList);
    UQDamageID := GetNextUQDamageID;
    SetPhase('Inserting damage: Getting next UQ Damage ID');
    InsertDamage.Parameters.ParamByName('uq_damage_id').Value := UQDamageID;
    InsertDamage.Parameters.ParamByName('user_changed_pc').Value := UserChangedProfitCenter;
    InsertDamage.Parameters.ParamByName('profit_center').Value := ProfitCenter;
    {EB - no need to specify the Intelex num parameter value, it will be handled in the NVPairList}
    SetPhase('Inserting damage: Executing Insert');
    InsertDamage.Execute;
    SetPhase('Inserting damage: Assigning Damage ID');
    NewDamageID := GetDamageIDForUQDamageID(UQDamageID);
    SetPhase('Inserting damage: Updating DateTime');
    LogicDM.UpdateDateTimeToIncludeMilliseconds('damage', 'damage_id', NewDamageID, 'uq_notified_date', NotifiedDate);
  end;

  procedure AddDamageDefaultEstimate; //QMANTWO-615 req
  var
    Amount1: Double; //QMANTWO-615
    Amount2: Double; //QMANTWO-615
    Amount3: Double; //QMANTWO-615
    AmountTot: Double; //QMANTWO-615
  begin
    Amount1:=0.00;Amount2:=0.00;Amount3:=0.00;AmountTot:=0.00;  
    if (NewDamageID > 0) then
    begin
      GetDefaultEstimateForFacility(Amount1, FacilityType,   FacilitySize,   FacilityMaterial, ProfitCenter, UtilityCo);  //QMANTWO-615
      GetDefaultEstimateForFacility(Amount2, FacilityType_2, FacilitySize_2, FacilityMaterial_2, ProfitCenter, UtilityCo); //QMANTWO-615
      GetDefaultEstimateForFacility(Amount3, FacilityType_3, FacilitySize_3, FacilityMaterial_3, ProfitCenter, UtilityCo); //QMANTWO-615
      if (Amount1=-1) then Amount1:=0.00;
      if (Amount2=-1) then Amount2:=0.00;
      if (Amount3=-1) then Amount3:=0.00;
      AmountTot := Amount1+Amount2+Amount3;  //QMANTWO-615
      if AmountTot > 0.00 then
      begin
        SetPhase('Inserting damage estimate');
        LogicDM.ExecuteSQL(Format(InsertDamageEstimate, [NewDamageID, LogicDM.LoggedInEmpID, AmountTot, 'Estimate update from facility change', IsoDateTimeToStr(ModifiedDate)]));//QMANTWO-615
      end;
    end;
  end;

  procedure AddDamageUpdateEstimate; //QMANTWO-615 req
  var
    Amount1: Double;   //QMANTWO-615
    Amount2: Double;   //QMANTWO-615
    Amount3: Double;   //QMANTWO-615
    AmountTot: Double; //QMANTWO-615
  begin
    SetPhase('Checking damage estimate');
    Amount1:=0.00;Amount2:=0.00;Amount3:=0.00;AmountTot:=0.00;
    if (DamageID > 0) then
    begin
      GetDefaultEstimateForFacility(Amount1, FacilityType,   FacilitySize,   FacilityMaterial, ProfitCenter, UtilityCo); //QMANTWO-615
      GetDefaultEstimateForFacility(Amount2, FacilityType_2, FacilitySize_2, FacilityMaterial_2, ProfitCenter, UtilityCo);  //QMANTWO-615
      GetDefaultEstimateForFacility(Amount3, FacilityType_3, FacilitySize_3, FacilityMaterial_3, ProfitCenter, UtilityCo); //QMANTWO-615
      if (Amount1=-1) then Amount1:=0.00;    //QMANTWO-615
      if (Amount2=-1) then Amount2:=0.00;    //QMANTWO-615
      if (Amount3=-1) then Amount3:=0.00;    //QMANTWO-615
      AmountTot := Amount1+Amount2+Amount3;  //QMANTWO-615

      if AmountTot > 0.00 then
      begin
        SetPhase('Updating damage estimate');
        LogicDM.ExecuteSQL(Format(InsertDamageEstimate, [DamageID, LogicDM.LoggedInEmpID, AmountTot, 'Estimate update from facility change', IsoDateTimeToStr(ModifiedDate)]));//QMANTWO-615
      end;
    end;
  end;

  function FacilityChanged: Boolean;  //QMANTWO-615 req
  begin
    Result :=
      (ClearDashes(D.FieldByName('facility_type').Value) <> ClearDashes(FacilityType)) or
      (ClearDashes(D.FieldByName('facility_size').Value) <> ClearDashes(FacilitySize)) or
      (ClearDashes(D.FieldByName('facility_material').Value) <> ClearDashes(FacilityMaterial)) or

      (ClearDashes(D.FieldByName('facility_type_2').Value) <> ClearDashes(FacilityType_2)) or    //QMANTWO-615 req
      (ClearDashes(D.FieldByName('facility_size_2').Value) <> ClearDashes(FacilitySize_2)) or    //QMANTWO-615 req
      (ClearDashes(D.FieldByName('facility_material_2').Value) <> ClearDashes(FacilityMaterial_2)) or  //QMANTWO-615 req

      (ClearDashes(D.FieldByName('facility_type_3').Value) <> ClearDashes(FacilityType_3)) or    //QMANTWO-615 req
      (ClearDashes(D.FieldByName('facility_size_3').Value) <> ClearDashes(FacilitySize_3)) or    //QMANTWO-615 req
      (ClearDashes(D.FieldByName('facility_material_3').Value) <> ClearDashes(FacilityMaterial_3)) or    //QMANTWO-615 req

      (D.FieldByName('utility_co_damaged').Value <> UtilityCo);
  end;

  function DamageTabFieldsChanged: Boolean;
  begin
    Result := (D.FieldByName('damage_date').Value <> NVPairDateTime(DamagePairList, 'damage_date', bvZero)) or
      (D.FieldByName('notified_by_person').Value <> NVPairString(DamagePairList, 'notified_by_person', bvNull)) or
      (D.FieldByName('notified_by_company').Value <> NVPairString(DamagePairList, 'notified_by_company', bvNull)) or
      (D.FieldByName('notified_by_phone').Value <> NVPairString(DamagePairList, 'notified_by_phone', bvNull)) or
      (D.FieldByName('profit_center').Value <> ProfitCenter) or
      FacilityChanged or
      (D.FieldByName('size_type').Value <> NVPairString(DamagePairList, 'size_type', bvNull)) or
      (D.FieldByName('state').Value <> State) or
      (D.FieldByName('county').Value <> County) or
      (D.FieldByName('city').Value <> City) or
      (D.FieldByName('page').Value <> NVPairString(DamagePairList, 'page', bvNull)) or
      (D.FieldByName('grid').Value <> NVPairString(DamagePairList, 'grid', bvNull)) or
      (D.FieldByName('location').Value <> NVPairString(DamagePairList, 'location', bvNull)) or
      (D.FieldByName('excavator_company').Value <> NVPairString(DamagePairList, 'excavator_company', bvNull)) or
      (D.FieldByName('locate_requested').Value <> NVPairString(DamagePairList, 'locate_requested', bvNull)) or
      (D.FieldByName('investigator_id').Value <> NVPairInteger(DamagePairList, 'investigator_id', bvNull)) or
      (D.FieldByName('ticket_id').Value <> NVPairInteger(DamagePairList, 'ticket_id', bvNull)) or
      (D.FieldByName('locator_id').Value <> NVPairInteger(DamagePairList, 'locator_id', bvNull)) or
      (D.FieldByName('locate_marks_present').Value <> NVPairString(DamagePairList, 'locate_marks_present', bvNull)) or
      (D.FieldByName('client_claim_id').Value <> NVPairString(DamagePairList, 'client_claim_id', bvNull)) or
      (D.FieldByName('remarks').Value <> NVPairString(DamagePairList, 'remarks', bvDefault));
  end;

  function ResponsibilityChanged: Boolean;
  begin
    Result := (D.FieldByName('exc_resp_code').Value <> NVPairString(DamagePairList, 'exc_resp_code', bvNull)) or
      (D.FieldByName('exc_resp_other_desc').Value <> NVPairString(DamagePairList, 'exc_resp_other_desc', bvNull)) or
      (D.FieldByName('exc_resp_details').Value <> NVPairString(DamagePairList, 'exc_resp_details', bvDefault)) or
      (D.FieldByName('exc_resp_response').Value <> NVPairString(DamagePairList, 'exc_resp_response', bvDefault)) or
      (D.FieldByName('uq_resp_code').Value <> NVPairString(DamagePairList, 'uq_resp_code', bvNull)) or
      (D.FieldByName('uq_resp_other_desc').Value <> NVPairString(DamagePairList, 'uq_resp_other_desc', bvNull)) or
      (D.FieldByName('uq_resp_ess_step').Value <> NVPairString(DamagePairList, 'uq_resp_ess_step', bvNull)) or
      (D.FieldByName('uq_resp_details').Value <> NVPairString(DamagePairList, 'uq_resp_details', bvDefault)) or
      (D.FieldByName('spc_resp_code').Value <> NVPairString(DamagePairList, 'spc_resp_code', bvNull)) or
      (D.FieldByName('spc_resp_details').Value <> NVPairString(DamagePairList, 'spc_resp_details', bvDefault)) or
      not VarIsNull(NVPairInteger(DamagePairList, 'reason_changed', bvNull));
  end;

  function TryUpdateDamage: Boolean;
  var
    RecordsAffected: Integer;
    UpdateCommand: TADOCommand;
    Error: SyncError;
  begin
    Result := False;
    SetPhase('Editing existing damage');
    UpdateCommand := UpdateDamage3;

    if (D.FieldByName('locator_id').AsInteger <> NVPairInteger(DamagePairList, 'locator_id', bvZero))
      and (not LogicDM.CanI(RightDamagesAssignLocator)) then
      raise EOdNotAllowed.CreateFmt('Unable to update damage %d. You do not have ' +
        'permission to change a damage''s locator.',
        [D.FieldByName('uq_damage_id').AsInteger]);

    if (D.FieldByName('damage_type').AsString = DamageTypeApproved) and
      not LogicDM.CanI(RightDamagesEditWhenApproved) and (DamageTabFieldsChanged or
      ResponsibilityChanged) then begin
      Op.Log('Damage record update canceled. The damage is approved on ' +
        'the server, and the user does not have permission to change the ' +
        'Damage tab, Responsibility or Estimates for an approved damage. ' +
        'This error may be due to the user working with an outdated copy of ' +
        'the damage record in their local cache.' + CRLF +
        NVPairListToText(DamagePairList));
      Error := Errors.Add;
      Error.Message := Format('Unable to update damage %d. This damage is ' +
        'approved on the server, and you do not have permission to change ' +
        'the Damage tab, Responsibility or Estimates for an approved ' +
        'damage. This error may be due to working with an outdated copy of ' +
        'the damage record in your local cache.',
        [D.FieldByName('uq_damage_id').AsInteger]);
      Error.TableName := 'damage';
      Error.PrimaryKeyID := DamageID; 

      Result := False;
      Exit;
    end;

    AssignParamsFromNVPairList(UpdateCommand.Parameters, DamagePairList);
    UpdateCommand.Parameters.ParamByName('user_changed_pc').Value := UserChangedProfitCenter;
    UpdateCommand.Parameters.ParamByName('profit_center').Value := ProfitCenter;

    UpdateCommand.Execute(RecordsAffected, EmptyParam); //QMANTWO-615
try
      updateFacilities.Parameters.ParamByName('Facility_Type').value := FacilityType; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Size').value := FacilitySize;  //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Material').value := FacilityMaterial; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Type_2').value := FacilityType_2; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Size_2').value := FacilitySize_2; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Material_2').value := FacilityMaterial_2;//QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Type_3').value := FacilityType_3;  //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Facility_Size_3').value := FacilitySize_3; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('facility_material_3').value := FacilityMaterial_3; //QMANTWO-615
      updateFacilities.Parameters.ParamByName('Damage_ID').value := DamageID;  //QMANTWO-615
      updateFacilities.Execute(RecordsAffected, EmptyParam);  //QMANTWO-615

except on E: Exception do
  Op.Log(E.message);
end;

    if RecordsAffected = 0 then begin
      Op.Log('Damage record update failed. ' +CRLF +
        NVPairListToText(DamagePairList));
      Error := Errors.Add;
      Error.Message := 'Error: Unable to update the damage ' +
        'record with damage ID = ' + D.FieldByName('uq_damage_id').AsString;  //QMANTWO-615
      Error.TableName := 'damage';
      Error.PrimaryKeyID := DamageID;

    end
    else begin
      UQDamageID := D.FieldByName('uq_damage_id').AsInteger;
      // if type (status) goes from anything to "Pending Approval" send out the e-mail...
      if ((D.FieldByName('damage_type').AsString <> DamageTypePendingApproval) and
        (NewDamageType = DamageTypePendingApproval)) then
        SendDamagePendingApprovalNotification(UQDamageID, ProfitCenter);

      Result := True;
    end;
  end;

begin
  if not (LogicDM.CanI(RightDamagesV2) or LogicDM.CanI(RightDamages)) then
    raise EOdNotAllowed.Create('You do not have permission to update damages.');  

  {EB - Mark the beginning of the Save call....ShowMessage('LINE 942:Beginning of Save'); }
  NewDamageID := -1;
  Result := GeneratedKeyList.Create;
  Errors := SyncErrorList.Create;
  D := Damage;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage %d of %d', [Index + 1, Params.Count]));
    DamagePairList := Params[Index];
    DamageID := NVPairInteger(DamagePairList, 'damage_id', bvError);
    AddedBy := NVPairInteger(DamagePairList, 'added_by', bvNegativeOne);
    NotifiedDate := NVPairDateTime(DamagePairList, 'uq_notified_date', bvError);

    FacilityType := NVPairString(DamagePairList, 'facility_type', bvError);
    FacilitySize := NVPairString(DamagePairList, 'facility_size', bvError);
    FacilityMaterial := NVPairString(DamagePairList, 'facility_material', bvError);

    FacilityType_2 := NVPairString(DamagePairList, 'facility_type_2', bvDefault);  //QMANTWO-615
    FacilitySize_2 := NVPairString(DamagePairList, 'facility_size_2', bvDefault);  //QMANTWO-615
    FacilityMaterial_2 := NVPairString(DamagePairList, 'facility_material_2', bvDefault); //QMANTWO-615

    FacilityType_3 := NVPairString(DamagePairList, 'facility_type_3', bvDefault);  //QMANTWO-615
    FacilitySize_3 := NVPairString(DamagePairList, 'facility_size_3', bvDefault);  //QMANTWO-615
    FacilityMaterial_3 := NVPairString(DamagePairList, 'facility_material_3', bvDefault); //QMANTWO-615

    UtilityCo := NVPairString(DamagePairList, 'utility_co_damaged', bvError);
    InvoiceCode := NVPairString(DamagePairList, 'invoice_code', bvDefault);
    EstimateLocked := NVPairBoolean(DamagePairList, 'estimate_locked', bvZero);
    NewDamageType := NVPairString(DamagePairList, 'damage_type', bvError);
    ModifiedDate := NVPairDateTime(DamagePairList, 'modified_date', bvZero);
    if ModifiedDate < 1 then
      ModifiedDate := RoundSQLServerTimeMS(Now);

    //Auto generate PC when user is not permitted to change profit center; or
    //user is allowed to change profit center but has not manually changed profit center.
    UserChangedProfitCenter := NVPairBoolean(DamagePairList, 'user_changed_pc', bvNegativeOne);
    State := NVPairString(DamagePairList, 'state', bvError);
    County := NVPairString(DamagePairList, 'county', bvError);
    City := NVPairString(DamagePairList, 'city', bvError);
    ProfitCenter := NVPairString(DamagePairList, 'profit_center', bvError);
    D.Close;
    D.ExecuteOptions := [];

    if DamageID < 0 then begin     // Inserting a new damage
      NewDamageID := -1;           // To ensure it gets set below
      D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(NotifiedDate)]);
      D.Parameters.ParamValues['added_by'] := AddedBy;
      D.Parameters.ParamValues['profit_center'] := ProfitCenter;
      D.Open;
      if D.RecordCount = 0 then begin
        AddDamage;
        AddDamageDefaultEstimate;
      end else if D.RecordCount = 1 then begin //editing an existing damage
        NewDamageID := D.FieldByName('damage_id').AsInteger;
      end
      else if D.RecordCount > 1 then begin
        Op.Log('Multiple existing damages match new damage: ' + IntToStr(NewDamageID) + ': ' + D.CommandText + ': ' +
          'added_by = ' + D.Parameters.ParamValues['added_by'] + ': ' +
          'profit_center = ' + D.Parameters.ParamValues['profit_center'] + ': ' +
          'uq_notified_date = ' + IsoDateTimeToStrQuoted(NotifiedDate));
        raise Exception.Create('Multiple existing damages match new damage save data');
      end;

      Assert(NewDamageID > 0, 'Negative new damage ID');
      AddIdentity(Result, 'damage', DamageID, NewDamageID);
    end else begin
      D.CommandText := ExistingByID;
      D.Parameters.ParamValues['damage_id'] := DamageID;
      D.Open;
      InsertEstimate := (not EstimateLocked) and FacilityChanged;
      // TODO: Permission checks to be sure user can choose selected invoice code, claim status, etc (still in client)
      DamageUpdated := TryUpdateDamage;
      if InsertEstimate and DamageUpdated then
        AddDamageUpdateEstimate;
    end;
  end;
end;

function TDamageDM.SaveDamageThirdParty(const Params: NVPairListList): GeneratedKeyList;
const
  Existing = 'select * from damage_third_party where damage_id = :damage_id and added_by = :added_by ' +
    'and uq_notified_date = %s';
  Updating = 'update damage_third_party set claimant=''%s'', address1=''%s'', address2=''%s'', ' +
    'city=''%s'', state=''%s'', zipcode=''%s'', phone=''%s'', claim_desc=''%s'', carrier_id=%s, ' +
    'carrier_notified=%d, demand=%f, claim_status=''%s'', ' +
    'modified_by=%d, modified_date=getdate() where third_party_id=%d';
var
  ThirdPartyPairList: NVPairList;
  Index: Integer;
  ThirdPartyID: Integer;
  NewThirdPartyID: Integer;
  DamageID: Integer;
  CarrierNotified: Boolean;
  UQNotifiedDate: TDateTime;
  Claimant: string;
  Address1: string;
  Address2: string;
  City: string;
  State: string;
  ZipCode: string;
  Phone: string;
  ClaimDesc: string;
  CarrierID: string;
  Demand: Currency;
  ClaimStatus: string;
  D: TADODataset;
  InsertMode: Boolean;

  procedure UpdateDamageThirdParty;
  begin
    SetPhase('Editing existing damage 3rd party');
    LogicDM.ExecuteSQL(Format(Updating, [Claimant, Address1, Address2, City, State,
      ZipCode, Phone, ClaimDesc, CarrierID, Ord(CarrierNotified), Demand, ClaimStatus,
      LogicDM.LoggedInEmpID, ThirdPartyID]));
  end;

begin
  if not LogicDM.CanI(RightLitigationEdit) then
    raise EOdNotAllowed.Create('You do not have permission to save third party damages.');

  NewThirdPartyID := -1;
  Result := GeneratedKeyList.Create;
  D := DamageThirdParty;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage Third Party %d of %d', [Index + 1, Params.Count]));
    ThirdPartyPairList := Params[Index];
    ThirdPartyID := NVPairInteger(ThirdPartyPairList, 'third_party_id', bvError);
    DamageID := NVPairInteger(ThirdPartyPairList, 'damage_id', bvError);
    UQNotifiedDate := NVPairDateTime(ThirdPartyPairList, 'uq_notified_date', bvError);
    CarrierNotified := NVPairBoolean(ThirdPartyPairList, 'carrier_notified', bvError);
    Claimant := NVPairString(ThirdPartyPairList, 'claimant', bvError);
    Address1 := NVPairString(ThirdPartyPairList, 'address1', bvError);
    City := NVPairString(ThirdPartyPairList, 'city', bvError);
    State := NVPairString(ThirdPartyPairList, 'state', bvError);
    Phone := NVPairString(ThirdPartyPairList, 'phone', bvError);
    ClaimDesc := NVPairString(ThirdPartyPairList, 'claim_desc', bvError);
    Demand := NVPairFloat(ThirdPartyPairList, 'demand', bvError);
    ClaimStatus := NVPairString(ThirdPartyPairList, 'claim_status', bvError);
    Address2 := NVPairString(ThirdPartyPairList, 'address2', bvDefault);
    ZipCode := NVPairString(ThirdPartyPairList, 'zipcode', bvDefault);
    CarrierID := NVPairString(ThirdPartyPairList, 'carrier_id', bvDefault, 'null');

    InsertMode := (ThirdPartyID < 0);
    D.Close;
    if InsertMode then begin
      D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(UQNotifiedDate)]);
      D.Parameters.ParamValues['damage_id'] := DamageID;
      D.Parameters.ParamValues['added_by'] := LogicDM.LoggedInEmpID;
      D.Open;
      if D.RecordCount = 0 then begin
        SetPhase('Editing damage third party');
        D.Insert;
        D.FieldByName('damage_id').Value := DamageID;
        D.FieldByName('claimant').Value := Claimant;
        D.FieldByName('address1').Value := Address1;
        D.FieldByName('address2').Value := Address2;
        D.FieldByName('city').Value := City;
        D.FieldByName('state').Value := State;
        D.FieldByName('zipcode').Value := ZipCode;
        D.FieldByName('phone').Value := Phone;
        D.FieldByName('claim_desc').Value := ClaimDesc;
        D.FieldByName('demand').Value := Demand;
        D.FieldByName('claim_status').Value := ClaimStatus;
        D.FieldByName('carrier_notified').Value := CarrierNotified;
        D.FieldByName('uq_notified_date').Value := UQNotifiedDate;
        D.FieldByName('modified_by').Value := LogicDM.LoggedInEmpId;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpId;
        if CarrierID <> 'null' then
          D.FieldByName('carrier_id').Value := CarrierID
        else
          D.FieldByName('carrier_id').Clear;
        D.Post;
        NewThirdPartyID := D.FieldByName('third_party_id').AsInteger;
        LogicDM.UpdateDateTimeToIncludeMilliseconds('damage_third_party', 'third_party_id', NewThirdPartyID, 'uq_notified_date', UQNotifiedDate);
      end
      else if D.RecordCount > 1 then
        raise Exception.Create('Multiple existing damage third party records match new damage third party save data');
      NewThirdPartyID := D.FieldByName('third_party_id').AsInteger;
    end
    else
      UpdateDamageThirdParty;

    if ThirdPartyID < 0 then begin
      Assert(NewThirdPartyID > 0, 'Negative new damage third party ID');
      AddIdentity(Result, 'damage_third_party', ThirdPartyID, NewThirdPartyID);
    end;
  end;
end;

function TDamageDM.SaveDamageEstimate2(const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
const
  Existing = 'select * from damage_estimate ';
  ExistingDamageApproved = 'select uq_damage_id from damage where damage_type = %s and damage_id = %d';
var
  EstimatePairList: NVPairList;
  Index: Integer;
  EstimateID: Integer;
  NewEstimateID: Integer;
  DamageID: Integer;
  ThirdParty: Boolean;
  Amount: Currency;
  Comment: string;
  EstimateType: Integer;
  Company: Variant;
  LitigationID: Variant;
  ThirdPartyID: Variant;
  AddedDate: Variant;
  D: TADODataset;
  InsertMode: Boolean;
  SQL: TOdSqlBuilder;

  function CanAddEstimateForThisDamage: Boolean;
  var
    Error: SyncError;
  begin
    Result := True;
    //If the user is NOT granted the right to edit estimates on Approved damages-
    //determine if the damage is approved. If approved, user cannot add an estimate.
    if not LogicDM.CanI(RightDamagesEditWhenApproved) then begin
      D.Close;
      D.CommandText := Format(ExistingDamageApproved, [QuotedStr(DamageTypeApproved), DamageID]);
      D.Open;
      Result := (D.RecordCount = 0); //if no record is returned then the damage is not approved and estimate can be added

      if not Result then begin//cannot save the estimate, raise exception(todo?) and add to sync errors
        Op.Log(Format('Damage estimate save failed on approved damage: %d (estimate_id = %d). The '+
          'user is revoked the %s right on the server. Please sync to get the updated right.',
          [D.FieldByName('uq_damage_id').AsInteger, EstimateID, RightDamagesEditWhenApproved]) +
          CRLF + NVPairListToText(EstimatePairList));
        Error := Errors.Add;
        Error.Message := Format('Error: Unable to save the damage estimate ' +
          'on damage = %d (estimate_id = %d). The user is revoked the %s right ' +
          'on the server. Please sync to get the updated right.',
          [D.FieldByName('uq_damage_id').AsInteger, EstimateID, RightDamagesEditWhenApproved]);
        Error.TableName := 'damage_estimate';
        Error.PrimaryKeyID := EstimateID;
      end;
    end;
  end;

begin
  SetName('SaveDamageEstimate2');
  if not LogicDM.CanI(RightDamagesAddEstimate) then
    raise EOdNotAllowed.Create('You do not have permission to add damage estimates.');

  NewEstimateID := -1;
  Result := GeneratedKeyList.Create;
  Errors := SyncErrorList.Create;
  D := DamageEstimate;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage Estimate %d of %d', [Index + 1, Params.Count]));
    EstimatePairList := Params[Index];
    EstimateID := NVPairInteger(EstimatePairList, 'estimate_id', bvError);
    DamageID := NVPairInteger(EstimatePairList, 'damage_id', bvError);
    if CanAddEstimateForThisDamage then begin
      ThirdParty := NVPairBoolean(EstimatePairList, 'third_party', bvZero);
      Amount := NVPairFloat(EstimatePairList, 'amount', bvError);
      Comment := NVPairString(EstimatePairList, 'comment', bvError);
      Company := NVPairString(EstimatePairList, 'company', bvNull);
      EstimateType := NVPairInteger(EstimatePairList, 'estimate_type', bvNegativeOne);
      LitigationID := NVPairInteger(EstimatePairList, 'litigation_id', bvNull);
      ThirdPartyID := NVPairInteger(EstimatePairList, 'third_party_id', bvNull);
      AddedDate := NVPairDateTime(EstimatePairList, 'added_date', bvNull);
      if EstimateType = -1 then
        if ThirdParty then
          EstimateType := 1
        else
          EstimateType := 0;

      InsertMode := (EstimateID < 0);
      D.Close;
      if InsertMode then begin
        SQL := TOdSqlBuilder.Create;
        try
          SQL.AddIntFilterRequirePositive('damage_id', DamageID);
          SQL.AddIntFilterRequirePositive('emp_id', LogicDM.LoggedInEmpID);
          SQL.AddExactStringFilterNonEmpty('comment', Comment);
          SQL.AddCurrencyFilter('amount', Amount);
          SQL.AddISODateFilterAllowNull('added_date', AddedDate);
          D.CommandText := Existing + SQL.WhereClause;
        finally
          FreeAndNil(SQL);
        end;
        D.Open;
        if D.RecordCount = 0 then begin
          SetPhase('Adding damage estimate');
          D.Insert;
          D.FieldByName('damage_id').Value := DamageID;
          D.FieldByName('third_party').Value := ThirdParty;
          D.FieldByName('emp_id').Value := LogicDM.LoggedInEmpId;
          D.FieldByName('amount').Value := Amount;
          D.FieldByName('comment').Value := Comment;
          D.FieldByName('estimate_type').Value := EstimateType;
          D.FieldByName('company').Value := Company;
          D.FieldByName('litigation_id').Value := LitigationID;
          D.FieldByName('third_party_id').Value := ThirdPartyID;
          D.FieldByName('modified_date').Value := Now;
          D.FieldByName('added_date').Value := AddedDate;
          D.Post;
          NewEstimateID := D.FieldByName('estimate_id').AsInteger;
          if not VarIsNull(AddedDate) then
            LogicDM.UpdateDateTimeToIncludeMilliseconds('damage_estimate', 'estimate_id', NewEstimateID, 'added_date', VarToDateTime(AddedDate));
        end
        else if D.RecordCount > 1 then
          raise Exception.Create('Multiple existing estimates match new damage estimate save data');
        NewEstimateID := D.FieldByName('estimate_id').AsInteger;
      end;
      if EstimateID < 0 then begin
        Assert(NewEstimateID > 0, 'Negative new damage estimate ID');
        AddIdentity(Result, 'damage_estimate', EstimateID, NewEstimateID);
      end;
    end; //CanAddEstimateForThisDamage
  end;
end;

function TDamageDM.SaveDamageInvoice(const Params: NVPairListList): GeneratedKeyList;
var
  InvoicePairList: NVPairList;
  Index: Integer;
  InvoiceID: Integer;
  NewInvoiceID: Integer;
  DamageID: Integer;
  InvoiceNum: string;
  Amount: Currency;
  Company: Variant;
  InvoiceDate: TDateTime;
  ReceivedDate: TDateTime;
  InvoiceApproved: Boolean;
  Comment: Variant;
  DamageCity: Variant;
  DamageState: Variant;
  DamageDate: Variant;
  Satisfied: Boolean;
  SatisfiedByEmp: Variant;
  SatisfiedDate: Variant;
  ApprovedDate: Variant;
  ApprovedAmount: Variant;
  PaidDate: Variant;
  PaidAmount: Variant;
  InvoiceCode: Variant;
  PacketRequestDate: Variant;
  PacketRecvDate: Variant;
  LitigationID: Variant;
  ThirdPartyID: Variant;
  PaymentCode: Variant;
  D: TADODataset;
  InsertMode: Boolean;
const
  Existing = 'select * from damage_invoice where damage_id = :damage_id and ' +
    'invoice_num = :invoice_num and company = :company and amount = :amount and ' +
    'received_date = :received_date and added_by = :added_by';
  Updating = 'update damage_invoice set damage_id=:damage_id, ' +
    'invoice_num=:invoice_num, amount=:amount, company=:company, ' +
    'received_date=:received_date, damage_city=:damage_city, damage_state=:damage_state, ' +
    'damage_date=:damage_date, satisfied=:satisfied, satisfied_by_emp_id=:satisfied_by_emp_id, ' +
    'satisfied_date=:satisfied_date, approved_date=:approved_date, ' +
    'approved_amount=:approved_amount, paid_date=:paid_date, paid_amount=:paid_amount, ' +
    'comment=:comment, modified_date=GetDate(), invoice_code=:invoice_code, ' +
    'cust_invoice_date=:cust_invoice_date, packet_request_date=:packet_request_date, ' +
    'packet_recv_date=:packet_recv_date, invoice_approved=:invoice_approved, ' +
    'litigation_id=:litigation_id, third_party_id=:third_party_id, payment_code=:payment_code ' +
    'where invoice_id=:invoice_id';

  procedure UpdateDamageInvoice;
  begin
    SetPhase('Editing existing damage invoice');
    ExecuteSQLWithParams(LogicDM.Conn, Updating, InvoicePairList);
  end;

begin
  NewInvoiceID := -1;
  Result := GeneratedKeyList.Create;
  D := DamageInvoice;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage Invoice %d of %d', [Index + 1, Params.Count]));
    InvoicePairList := Params[Index];
    InvoiceID := NVPairInteger(InvoicePairList, 'invoice_id', bvError);
    DamageID := NVPairInteger(InvoicePairList, 'damage_id', bvError);
    InvoiceNum := NVPairString(InvoicePairList, 'invoice_num', bvError);
    Amount := NVPairFloat(InvoicePairList, 'amount', bvError);
    Company := NVPairString(InvoicePairList, 'company', bvDefault);
    InvoiceDate := NVPairDateTime(InvoicePairList, 'cust_invoice_date', bvError);
    ReceivedDate := NVPairDateTime(InvoicePairList, 'received_date', bvError);
    Comment := NVPairString(InvoicePairList, 'comment', bvDefault);
    DamageCity := NVPairString(InvoicePairList, 'damage_city', bvNull);
    DamageState := NVPairString(InvoicePairList, 'damage_state', bvNull);
    DamageDate := NVPairDateTime(InvoicePairList, 'damage_date', bvNull);
    Satisfied := NVPairBoolean(InvoicePairList, 'satisfied', bvZero);
    SatisfiedByEmp := NVPairInteger(InvoicePairList, 'satisfied_by_emp_id', bvNull);
    SatisfiedDate := NVPairDateTime(InvoicePairList, 'satisfied_date', bvNull);
    ApprovedDate := NVPairDateTime(InvoicePairList, 'approved_date', bvNull);
    ApprovedAmount := NVPairFloat(InvoicePairList, 'approved_amount', bvNull);
    PaidDate := NVPairDateTime(InvoicePairList, 'paid_date', bvNull);
    PaidAmount := NVPairFloat(InvoicePairList, 'paid_amount', bvNull);
    InvoiceCode := NvPairString(InvoicePairList, 'invoice_code', bvNull);
    PacketRequestDate := NVPairDateTime(InvoicePairList, 'packet_request_date', bvNull);
    PacketRecvDate := NVPairDateTime(InvoicePairList, 'packet_recv_date', bvNull);
    InvoiceApproved := NVPairBoolean(InvoicePairList, 'invoice_approved', bvZero);
    LitigationID := NVPairInteger(InvoicePairList, 'litigation_id', bvNull);
    ThirdPartyID := NVPairInteger(InvoicePairList, 'third_party_id', bvNull);
    PaymentCode := NVPairString(InvoicePairList, 'payment_code', bvNull);
    InsertMode := (InvoiceID < 0);

    Op.Detail := 'Params: ' + NVPairListToText(InvoicePairList);

    D.Close;
    if InsertMode then begin
      D.CommandText := Existing;
      D.Parameters.ParamValues['damage_id'] := DamageID;
      D.Parameters.ParamValues['company'] := Company;
      D.Parameters.ParamValues['invoice_num'] := InvoiceNum;
      D.Parameters.ParamValues['received_date'] := ReceivedDate; // This is a truncated date (without a time)
      D.Parameters.ParamValues['amount'] := Amount;
      D.Parameters.ParamValues['added_by'] := LogicDM.LoggedInEmpID;
      SetPhase(Format('Opening: Damage Invoice %d of %d', [Index + 1, Params.Count]));
      D.Open;
      if D.RecordCount = 0 then begin
        SetPhase('Editing damage invoice');
        D.Insert;
        D.FieldByName('damage_id').Value := DamageID;
        D.FieldByName('invoice_num').Value := InvoiceNum;
        D.FieldByName('amount').Value := Amount;
        D.FieldByName('company').Value := Company;
        D.FieldByName('cust_invoice_date').Value := InvoiceDate;
        D.FieldByName('received_date').Value := ReceivedDate;
        D.FieldByName('comment').Value := Comment;
        D.FieldByName('damage_city').Value := DamageCity;
        D.FieldByName('damage_state').Value := DamageState;
        D.FieldByName('damage_date').Value := DamageDate;
        D.FieldByName('satisfied').Value := Satisfied;
        if Satisfied then begin
          D.FieldByName('satisfied_by_emp_id').Value := LogicDM.LoggedInEmpId;
          D.FieldByName('satisfied_date').Value := SatisfiedDate;
        end;
        D.FieldByName('invoice_approved').Value := InvoiceApproved;
        D.FieldByName('approved_date').Value := ApprovedDate;
        D.FieldByName('approved_amount').Value := ApprovedAmount;
        D.FieldByName('paid_date').Value := PaidDate;
        D.FieldByName('paid_amount').Value := PaidAmount;
        D.FieldByName('invoice_code').Value := InvoiceCode;
        D.FieldByName('packet_request_date').Value := PacketRequestDate;
        D.FieldByName('packet_recv_date').Value := PacketRecvDate;
        D.FieldByName('litigation_id').Value := LitigationID;
        D.FieldByName('third_party_id').Value := ThirdPartyID;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpID;
        D.FieldByName('modified_date').Value := Now;
        D.FieldByName('payment_code').Value := PaymentCode;
        SetPhase(Format('Posting: Damage Invoice %d of %d', [Index + 1, Params.Count]));
        D.Post;
      end
      else if D.RecordCount > 1 then
        raise Exception.Create('Multiple existing invoices match new damage invoice save data');
      NewInvoiceID := D.FieldByName('invoice_id').AsInteger;
    end
    else begin
      // TODO: Need to check that user has ApproveInvoice permission to do edits
      {
      if not CanI(RightDamagesApproveInvoice) then
        raise EOdNotAllowed.Create('You are not permitted to change invoice approval.');
      }
      SetPhase(Format('UpdateDamageInvoice: Damage Invoice %d of %d', [Index + 1, Params.Count]));
      UpdateDamageInvoice;
    end;
    if InvoiceID < 0 then begin
      Assert(NewInvoiceID > 0, 'Negative new damage invoice ID');
      AddIdentity(Result, 'damage_invoice', InvoiceID, NewInvoiceID);
    end;
  end;
end;

function TDamageDM.SaveDamageLitigation(const Params: NVPairListList): GeneratedKeyList;
var
  LitigationPairList: NVPairList;
  Index: Integer;
  LitigationID: Integer;
  NewLitigationID: Integer;
  DamageID: Integer;
  FileNumber: string;
  Plaintiff: string;
  Defendant: string;
  SummonsDate: TDateTime;
  CarrierNotified: Boolean;
  ClaimStatus: string;
  CarrierID: Variant;
  Attorney: Variant;
  Party: Variant;
  Demand: Variant;
  Accrual: Variant;
  Settlement: Variant;
  ClosedDate: Variant;
  Comment: Variant;
  Venue: Variant;
  VenueState: Variant;
  ServiceDate: Variant;
  D: TADODataset;
  InsertMode: Boolean;
const
  Existing = 'select * from damage_litigation where damage_id=:damage_id and ' +
    'file_number=:file_number and plaintiff=:plaintiff and ' +
    'defendant=:defendant and summons_date=:summons_date';
  Updating = 'update damage_litigation set damage_id=:damage_id, ' +
    'file_number=:file_number, plaintiff=:plaintiff, ' +
    'carrier_id=:carrier_id, attorney=:attorney, party=:party, ' +
    'demand=:demand, accrual=:accrual, settlement=:settlement, ' +
    'closed_date=:closed_date, comment=:comment, defendant=:defendant, ' +
    'venue=:venue, venue_state=:venue_state, service_date=:service_date, ' +
    'summons_date=:summons_date, carrier_notified=:carrier_notified, ' +
    'claim_status=:claim_status, modified_by=:modified_by where litigation_id=:litigation_id';

  procedure UpdateDamageLitigation;
  begin
    SetPhase('Editing existing damage litigation');
    ExecuteSQLWithParams(LogicDM.Conn, Updating, LitigationPairList);
  end;

begin
  if not LogicDM.CanI(RightLitigationEdit) then
    raise EOdNotAllowed.Create('You do not have permission to save litigation changes');

  NewLitigationID := -1;
  Result := GeneratedKeyList.Create;
  D := DamageLitigation;
  for Index := 0 to Params.Count - 1 do begin
    SetPhase(Format('Damage Litigation %d of %d', [Index + 1, Params.Count]));
    LitigationPairList := Params[Index];
    LitigationID := NVPairInteger(LitigationPairList, 'litigation_id', bvError);
    DamageID := NVPairInteger(LitigationPairList, 'damage_id', bvError);
    FileNumber := NVPairString(LitigationPairList, 'file_number', bvError);
    Plaintiff := NVPairString(LitigationPairList, 'plaintiff', bvError);
    Defendant := NVPairString(LitigationPairList, 'defendant', bvError); // This causes the old litigation screen to be unable to add new records
    SummonsDate := NVPairDateTime(LitigationPairList, 'summons_date', bvError);
    CarrierNotified := NVPairBoolean(LitigationPairList, 'carrier_notified', bvDefault);
    ClaimStatus := NVPairString(LitigationPairList, 'claim_status', bvError);
    CarrierID := NVPairInteger(LitigationPairList, 'carrier_id', bvNull);
    Attorney := NVPairString(LitigationPairList, 'attorney', bvNull);
    Party := NVPairString(LitigationPairList, 'party', bvNull);
    Demand := NVPairFloat(LitigationPairList, 'demand', bvNull);
    Accrual := NVPairFloat(LitigationPairList, 'accrual', bvNull);
    Settlement := NVPairFloat(LitigationPairList, 'settlement', bvNull);
    ClosedDate := NVPairDateTime(LitigationPairList, 'closed_date', bvNull);
    Comment := NVPairString(LitigationPairList, 'comment', bvNull);
    Venue := NVPairString(LitigationPairList, 'venue', bvNull);
    VenueState := NVPairString(LitigationPairList, 'venue_state', bvNull);
    ServiceDate := NVPairDateTime(LitigationPairList, 'service_date', bvNull);

    InsertMode := (LitigationID < 0);
    D.Close;
    if InsertMode then begin
      D.CommandText := Existing;
      D.Parameters.ParamValues['damage_id'] := DamageID;
      D.Parameters.ParamValues['file_number'] := FileNumber;
      D.Parameters.ParamValues['plaintiff'] := Plaintiff;
      D.Parameters.ParamValues['defendant'] := Defendant;
      D.Parameters.ParamValues['summons_date'] := SummonsDate;
      D.Open;
      if D.RecordCount = 0 then begin
        SetPhase('Editing damage litigation');
        D.Insert;
        D.FieldByName('damage_id').Value := DamageID;
        D.FieldByName('file_number').Value := FileNumber;
        D.FieldByName('plaintiff').Value := Plaintiff;
        D.FieldByName('defendant').Value := Defendant;
        D.FieldByName('summons_date').Value := SummonsDate;
        D.FieldByName('carrier_notified').Value := CarrierNotified;
        D.FieldByName('claim_status').Value := ClaimStatus;
        D.FieldByName('carrier_id').Value := CarrierID;
        D.FieldByName('attorney').Value := Attorney;
        D.FieldByName('party').Value := Party;
        D.FieldByName('demand').Value := Demand;
        D.FieldByName('accrual').Value := Accrual;
        D.FieldByName('settlement').Value := Settlement;
        D.FieldByName('closed_date').Value := ClosedDate;
        D.FieldByName('comment').Value := Comment;
        D.FieldByName('venue').Value := Venue;
        D.FieldByName('venue_state').Value := VenueState;
        D.FieldByName('service_date').Value := ServiceDate;
        D.FieldByName('modified_by').Value := LogicDM.LoggedInEmpID;
        D.Post;
      end
      else if D.RecordCount > 1 then
        raise Exception.Create('Multiple existing litigations match new litigation save data');
      NewLitigationID := D.FieldByName('litigation_id').AsInteger;
    end
    else
      UpdateDamageLitigation;
    if LitigationID < 0 then begin
      Assert(NewLitigationID > 0, 'Negative new damage litigation ID');
      AddIdentity(Result, 'damage_litigation', LitigationID, NewLitigationID);
    end;
  end;
end;

procedure TDamageDM.MoveDamages(const Damages: IntegerList; const FromInvestigatorID, ToInvestigatorID: Integer);
const
  FindDamageSQL = 'select * from damage where damage_id = :id and investigator_id = :investigator';
var
  I: Integer;
begin
  Assert(Assigned(Damages), 'MoveDamages requires damage list');
  Damage.CommandText := FindDamageSQL;
  for I := 0 to Damages.Count-1 do begin
    Damage.Close;
    Damage.Parameters.ParamValues['id'] := Damages[I];
    Damage.Parameters.ParamValues['investigator'] := FromInvestigatorID;
    Damage.Open;
    if not Damage.Eof then begin
      Damage.Edit;
      Damage.FieldByName('investigator_id').Value := ToInvestigatorID;
      Damage.Post;
    end;
  end;
  CloseDataSet(Damage);
end;

procedure TDamageDM.SaveDamagePriority(const DamageID: Integer; const EntryDate: TDateTime; const Priority: Integer);
const
  SelectExisting = 'select coalesce(work_priority_id, -1) work_priority_id from damage where damage_id = ';
var
  OldPriority: Integer;
  D: TDataSet;
  PriorityName: string;
begin
  D := LogicDM.CreateDataSetForSQL(SelectExisting + IntToStr(DamageID));
  try
    OldPriority := D.FieldByName('work_priority_id').AsInteger;
    if OldPriority <> Priority then begin
      SetPhase('Editing damage priority');
      SetDamagePriority(DamageID, Priority);
      PriorityName := GetPriorityNameFromDamage(DamageID);
      GetNotesDM.AddNote(qmftDamage, DamageID, EntryDate, LogicDM.LoggedInUID,
        Format('Work priority set to %s by %s (Employee ID %d)',
        [PriorityName, LogicDM.LoggedInEmpShortName, LogicDM.LoggedInEmpID]), PrivateDamageSubType);  //EB 720 Added explicit subtype
    end;
  finally
    FreeAndNil(D);
  end;
end;

function TDamageDM.SaveRequiredDocuments(const RequiredDocs: RequiredDocumentList): GeneratedKeyList;
const
  SqlInsertDocument = 'insert into document (foreign_type, foreign_id, required_doc_id, comment, added_by_id, added_date, active) ' +
    'values (:foreign_type, :foreign_id, :required_doc_id, :comment, :added_by_id, :added_date, :active)';
  SqlUpdateDocument = 'update document set comment = :comment, active = :active where doc_id = :doc_id';
  SqlReqDocExists = 'select doc_id from document where foreign_type = :foreign_type ' +
    'and foreign_id = :foreign_id and required_doc_id = :required_doc_id ' +
    'and comment = :comment and added_by_id = :added_by_id and ' +
    'added_date = :added_date and active = :active';
  SqlInactivateDocument = 'update document set active = 0 ' +
    'where foreign_type = %d and foreign_id = %d ' +
    'and required_doc_id = %d and active = 1';
var
  ForeignType: Integer;
  ForeignID: Integer;    // damage_id
  RequiredDocID: Integer;
  Comment: Variant;
  AddedByID: Integer;
  AddedDate: TDateTime;
  Active: Boolean;

  procedure InactivateExistingDocs;
  begin
    SetPhase('Inactivate existing required docs');
    LogicDM.ExecuteSQL(Format(SqlInactivateDocument, [ForeignType, ForeignID, RequiredDocID]));
  end;

  function NewRequiredDocExists: Integer;
  begin
    SetPhase('Check for required doc');
    Result := -1;
    LogicDM.GenericQuery.CommandText := SqlReqDocExists;
    LogicDM.GenericQuery.Parameters.ParamValues['required_doc_id'] := RequiredDocID;
    LogicDM.GenericQuery.Parameters.ParamValues['foreign_id'] := ForeignID;
    LogicDM.GenericQuery.Parameters.ParamValues['foreign_type'] := ForeignType;
    LogicDM.GenericQuery.Parameters.ParamValues['comment'] := Comment;
    LogicDM.GenericQuery.Parameters.ParamValues['added_by_id'] := AddedByID;
    LogicDM.GenericQuery.Parameters.ParamValues['added_date'] := AddedDate;
    LogicDM.GenericQuery.Parameters.ParamValues['active'] := Active;
    LogicDM.GenericQuery.Open;
    if (not LogicDM.GenericQuery.EOF) then
      Result := LogicDM.GenericQuery.FieldByName('doc_id').AsInteger;
    LogicDM.GenericQuery.Close;
  end;

var
  I: Integer;
  ReqDoc: RequiredDocument;
  DocID: Integer;
  NewDocID: Integer;
  CheckDocID: Integer;
begin
  Result := GeneratedKeyList.Create;
  for I := 0 to RequiredDocs.Count - 1 do begin
    ReqDoc := RequiredDocs.Items[I];
    DocId := ReqDoc.DocID;
    ForeignType := ReqDoc.ForeignType;
    ForeignID := ReqDoc.ForeignID;
    RequiredDocID := ReqDoc.RequiredDocID;
    if IsEmpty(ReqDoc.Comment) then
      Comment := Null
    else
      Comment := ReqDoc.Comment;
    Active := ReqDoc.Active;
    AddedDate := ReqDoc.AddedDate;
    AddedByID := ReqDoc.AddedByID;

    if (DocId < 0) then begin
      CheckDocId := NewRequiredDocExists;
      if (CheckDocId = -1) then begin
        InactivateExistingDocs;
        DocumentCommand.CommandText := SqlInsertDocument;
      end else begin
        DocumentCommand.CommandText := SqlUpdateDocument;
        DocId := CheckDocId;
      end;
    end else begin
      DocumentCommand.CommandText := SqlUpdateDocument;
    end;

    SetPhase(Format('Saving required document %d of %d', [I+1, RequiredDocs.Count]));
    DocumentCommand.Parameters.ParamValues['comment'] := Comment;
    DocumentCommand.Parameters.ParamValues['active'] := Active;
    if (DocId > 0) then
      DocumentCommand.Parameters.ParamValues['doc_id'] := DocID
    else begin
      DocumentCommand.Parameters.ParamValues['foreign_type'] := ForeignType;
      DocumentCommand.Parameters.ParamValues['foreign_id'] := ForeignID;
      DocumentCommand.Parameters.ParamValues['required_doc_id'] := RequiredDocID;
      DocumentCommand.Parameters.ParamValues['added_by_id'] := AddedByID;
      DocumentCommand.Parameters.ParamValues['added_date'] := AddedDate;
    end;
    DocumentCommand.Execute;
    if (DocId < 0) then begin
      NewDocId := LogicDM.GetAnyIdentity;
      AddIdentity(Result, 'document', DocId, NewDocId);
    end;
  end;
end;

end.
