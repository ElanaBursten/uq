inherited NotesDM: TNotesDM
  OldCreateOrder = True
  Height = 181
  Width = 287
  object InsertNote: TADOCommand
    CommandText = 
      'INSERT INTO notes ('#13#10' foreign_type,'#13#10' foreign_id,'#13#10' entry_date,'#13 +
      #10' uid,'#13#10' note,'#13#10' private'#13#10') VALUES ('#13#10' 1,'#13#10' :TicketID,'#13#10' :EntryD' +
      'ate,'#13#10' :UID,'#13#10' :Note,'#13#10' :SubType'#13#10')'#13#10
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'EntryDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'UID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Note'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'SubType'
        DataType = ftInteger
        Value = Null
      end>
    Left = 32
    Top = 72
  end
  object Notes: TADODataSet
    CommandText = 
      'select * from notes'#13#10'where uid = :UID'#13#10'  and foreign_id = :forei' +
      'gn_id'#13#10'  and foreign_type = :foreign_type'#13#10'  and entry_date = :e' +
      'ntry_date'
    Parameters = <
      item
        Name = 'uid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'foreign_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'foreign_type'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'entry_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 32
    Top = 14
  end
  object UpdateNote: TADOCommand
    CommandText = 
      'Update notes '#13#10'     Set note =:NoteText,'#13#10'           sub_type=:S' +
      'ubType'#13#10'WHERE notes_id =:NoteID'#13#10
    Parameters = <
      item
        Name = 'NoteID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'NoteText'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'SubType'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end>
    Left = 205
    Top = 21
  end
end
