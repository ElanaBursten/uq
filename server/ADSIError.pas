(* ----------------------------------------------------------------------------
 Module:  ADSI error messages
 Author:  Marc Scheuner
 Date:    16-May-2002

 Changes:

---------------------------------------------------------------------------- *)

unit ADSIError;

interface

//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//      Sev - is the severity code
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//

// Define the facility codes
const
  FACILITY_CONTROL  = 10;
  FACILITY_DISPATCH =  2;
  FACILITY_ITF      =  4;
  FACILITY_NULL     =  0;
  FACILITY_RPC      =  1;
  FACILITY_SSPI     =  9;
  FACILITY_STORAGE  =  3;
  FACILITY_WINDOWS  =  8;
  FACILITY_WIN32    =  7;

  //  An invalid Active Directory pathname was passed
  E_ADS_BAD_PATHNAME = HRESULT($80005000);

  //  An unknown Active Directory domain object was requested
  E_ADS_INVALID_DOMAIN_OBJECT    = HRESULT($80005001);

  //  An unknown Active Directory user object was requested
  E_ADS_INVALID_USER_OBJECT      = HRESULT($80005002);

  //  An unknown Active Directory computer object was requested
  E_ADS_INVALID_COMPUTER_OBJECT  = HRESULT($80005003);

  //  An unknown Active Directory object was requested
  E_ADS_UNKNOWN_OBJECT           = HRESULT($80005004);

  //  The specified Active Directory property was not set
  E_ADS_PROPERTY_NOT_SET         = HRESULT($80005005);

  //  The specified Active Directory property is not supported
  E_ADS_PROPERTY_NOT_SUPPORTED   = HRESULT($80005006);

  //  The specified Active Directory property is invalid
  E_ADS_PROPERTY_INVALID         = HRESULT($80005007);

  //  One or more input parameters are invalid
  E_ADS_BAD_PARAMETER            = HRESULT($80005008);

  //  The specified Active Directory object is not bound to a remote resource
  E_ADS_OBJECT_UNBOUND           = HRESULT($80005009);

  //  The specified Active Directory object has not been modified
  E_ADS_PROPERTY_NOT_MODIFIED    = HRESULT($8000500A);

  //  The specified Active Directory object has not been modified
  E_ADS_PROPERTY_MODIFIED        = HRESULT($8000500B);

  //  The Active Directory datatype cannot be converted to/from a native DS datatype
  E_ADS_CANT_CONVERT_DATATYPE    = HRESULT($8000500C);

  //  The Active Directory property cannot be found in the cache.
  E_ADS_PROPERTY_NOT_FOUND       = HRESULT($8000500D);

  //  The Active Directory object exists.
  E_ADS_OBJECT_EXISTS            = HRESULT($8000500E);

  //  The attempted action violates the DS schema rules.
  E_ADS_SCHEMA_VIOLATION         = HRESULT($8000500F);

  //  The specified column in the Active Directory was not set.
  E_ADS_COLUMN_NOT_SET           = HRESULT($80005010);


  //  One or more errors occurred
  S_ADS_ERRORSOCCURRED           = HRESULT($00005011);

  //  No more rows to be obatained by the search result.
  S_ADS_NOMORE_ROWS              = HRESULT($00005012);

  //  No more columns to be obatained for the current row.
  S_ADS_NOMORE_COLUMNS           = HRESULT($00005013);

  //  The search filter specified is invalid
  E_ADS_INVALID_FILTER           = HRESULT($80005014);


implementation

end.
