unit WorkOrderDMu;

interface

uses
  SysUtils, Classes, DB, BaseLogicFeatureDMu, ADODB,
  QMServerLibrary_Intf, AddinInfoDMu, NotesDMu, RemObjectsUtils, uROTypes;

type
  TWorkOrderDM = class(TLogicFeatureDM)
    UpdateWorkOrderStatus: TADOStoredProc;
    UpdateWorkOrder: TADOCommand;
    UpdateWOAssignments: TADOStoredProc;
    UpdateInspection: TADOCommand;
    InsertRemedy: TADOCommand;
    UpdateWorkOrderOHMDetails: TADOQuery;
    WorkOrder: TADODataSet;
    Inspection: TADODataSet;
    Remedy: TADODataSet;
  private
    NotesDM: TNotesDM;
    AddinInfoDM: TAddinInfoDM;
    function GetNotesDM: TNotesDM;
    function GetAddinInfoDM: TAddinInfoDM;
    procedure SetWorkOrderStatus(Change: TROComplexType);
  public
    destructor Destroy; override;
    function GetWorkOrder(const WorkOrderID: Integer): string;
    function GetWorkOrderOHMDetails(const WorkOrderID: Integer): string; //QMANTWO-391
    function GetWorkOrderHistory(const WorkOrderID: Integer): string;
    procedure MoveWorkOrders(const WorkOrders: IntegerList; const FromWorkerID, ToWorkerID: Integer);
    procedure StatusWorkOrders2(const WorkOrderChanges: WorkOrderChangeList2; out GeneratedKeys: GeneratedKeyList);
    procedure StatusWorkOrders5(const WorkOrderChanges: WorkOrderChangeList5; out GeneratedKeys: GeneratedKeyList);
    procedure StatusWorkOrderInspections(const WorkOrderChanges: WOInspectionChangeList; out GeneratedKeys: GeneratedKeyList);
    procedure SaveWorkOrdersOHM(const WorkOrderOHMChanges: WorkOrderOHMChangeList);  //QMANTWO-391
    function WorkOrderSearch(const TicketNumber, WorkOrderNumber, Street, City,
      State, County, WOSource, ClientName, Company: string; const DueDateFrom,
      DueDateTo, TransmitDateFrom, TransmitDateTo: TDateTime; const Kind: string;
      const Tickets: Integer): string;
  end;

implementation

{$R *.dfm}

uses QMConst, LogicDMu, OdDatasetToXML, OdSQLBuilder, OdDbUtils;

const
  FEATURE_NAME = 'WorkOrder';

destructor TWorkOrderDM.Destroy;
begin
  FreeAndNil(NotesDM);
  FreeAndNil(AddinInfoDM);
  inherited;
end;

function TWorkOrderDM.GetAddinInfoDM: TAddinInfoDM;
begin
  if AddinInfoDM = nil then
    AddinInfoDM := TAddinInfoDM.Create(nil, LogicDM);
  Result := AddinInfoDM;
end;

function TWorkOrderDM.GetNotesDM: TNotesDM;
begin
  if NotesDM = nil then
    NotesDM := TNotesDM.Create(nil, LogicDM);
  Result := NotesDM;
end;

function TWorkOrderDM.GetWorkOrder(const WorkOrderID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_work_order2', '@WorkOrderID', WorkOrderID);
end;

function TWorkOrderDM.GetWorkOrderHistory(const WorkOrderID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_work_order_history2', '@WorkOrderID', WorkOrderID);
end;

function TWorkOrderDM.GetWorkOrderOHMDetails(const WorkOrderID: Integer): string;  //QMANTWO-391
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_work_order_OHM_details', '@WorkOrderID', WorkOrderID);
end;

procedure TWorkOrderDM.MoveWorkOrders(const WorkOrders: IntegerList; const FromWorkerID, ToWorkerID: Integer);
const
  FindWOSQL = 'select * from work_order where wo_id = :wo_id and assigned_to_id = :assigned_to_id';
var
  I: Integer;
begin
  Assert(Assigned(WorkOrders), 'MoveWorkOrders requires work order list');

  WorkOrder.CommandText := FindWOSQL;
  for I := 0 to WorkOrders.Count-1 do begin
    WorkOrder.Close;
    WorkOrder.Parameters.ParamValues['wo_id'] := WorkOrders[I];
    WorkOrder.Parameters.ParamValues['assigned_to_id'] := FromWorkerID;
    WorkOrder.Open;
    if not WorkOrder.FieldByName('closed').AsBoolean then begin
      UpdateWOAssignments.Parameters.ParamValues['@WorkOrderID'] := WorkOrders[I];
      UpdateWOAssignments.Parameters.ParamValues['@WorkerID'] := ToWorkerID;
      UpdateWOAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
      UpdateWOAssignments.ExecProc;
    end;
    WorkOrder.Close;
  end;
end;
{-------------------------------------------------------------------------------------}
procedure TWorkOrderDM.StatusWorkOrders2(const WorkOrderChanges: WorkOrderChangeList2;
  out GeneratedKeys: GeneratedKeyList);
const
  ExistingByID = 'select assigned_to_id, status, closed from work_order where wo_id=:wo_id';
var
  i: Integer;
  WO: WorkOrderChange2;
  WorkOrderID: Integer;
  AssignedToID: Integer;
  NewStatus: String;
  Closed: Boolean;
  StatusDate: TDateTime;
  StatusedHow: String;
  StatusedByID: Integer;

  procedure UpdateWO;
  begin
    SetPhase('Editing existing work order');

    UpdateWorkOrder.Parameters.ParamByName('wo_id').Value := WorkOrderID;
    UpdateWorkOrder.Parameters.ParamByName('work_description').Value := WO.WorkDescription;
    UpdateWorkOrder.Parameters.ParamByName('work_cross').Value := WO.WorkCross;
    UpdateWorkOrder.Parameters.ParamByName('state_hwy_row').Value := WO.StateHwyROW;
    UpdateWorkOrder.Parameters.ParamByName('road_bore_count').Value := WO.RoadBoreCount;
    UpdateWorkOrder.Parameters.ParamByName('driveway_bore_count').Value := WO.DrivewayBoreCount;
    UpdateWorkOrder.Execute;

    // TODO-3339: Refactor this to call SetWorkOrderStatus instead
    SetPhase('Calling sp to insert wo_status_history and the wo_responder_queue');
    { TODO: This will be needed if we ever add a way to change the assigned worker from WO Detail.
    if (WODS.FieldByName('assigned_to_id').AsInteger <> WO.AssignedToID)
      and (not CanI(RightWorkOrdersAssignNewWorkOrders)) then
      raise EOdNotAllowed.Create('You do not have permission to change a work order''s worker.');
    }
    if (WorkOrder.FieldByName('status').AsString <> NewStatus) then begin
      UpdateWorkOrderStatus.Parameters.ParamByName('@WorkOrderID').Value := WorkOrderID;
      UpdateWorkOrderStatus.Parameters.ParamByName('@NewStatus').Value := NewStatus;
      UpdateWorkOrderStatus.Parameters.ParamByName('@Closed').Value := Closed;
      UpdateWorkOrderStatus.Parameters.ParamByName('@StatusDate').Value := StatusDate;
      if StatusedByID <> 0 then
        UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := StatusedByID
      else
        UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := AssignedToID;
      UpdateWorkOrderStatus.Parameters.ParamByName('@StatusedHow').Value := StatusedHow;
      UpdateWorkOrderStatus.ExecProc;
    end;
  end;

begin
  GeneratedKeys := GeneratedKeyList.Create;

  for i := 0 to WorkOrderChanges.Count - 1 do begin
    SetPhase(Format('Work Order %d of %d', [i + 1, WorkOrderChanges.Count]));
    Op.Detail := 'Loading work orders';

    WO := WorkOrderChanges[i];
    WorkOrderID := WO.WorkOrderID;
    AssignedToID := WO.AssignedToID;
    NewStatus := WO.Status;
    Closed := WO.Closed;
    StatusDate := WO.StatusDate;
    StatusedHow := WO.StatusedHow;
    StatusedByID := WO.StatusedByID;

    Op.Detail := 'Finding Work Order ' + IntToStr(WorkOrderID);
    WorkOrder.Close;
    WorkOrder.CommandText := ExistingByID;
    WorkOrder.Parameters.ParamValues['wo_id'] := WorkOrderID;
    WorkOrder.ExecuteOptions := [];
    WorkOrder.Open;
    Assert((WorkOrder.RecordCount=1), 'More than one work order was returned for the wo_id in the StatusWorkOrders2 operation.');

    if WorkOrder.FieldByName('closed').AsBoolean then
      Op.Detail := Format('Editing closed work order. Current status=%s; new status=%s.',
        [WorkOrder.FieldByName('status').AsString, NewStatus]);

    UpdateWO;

    SetPhase('Processing WO Notes changes');
    Op.Detail := 'NewNotes';
    GetNotesDM.ProcessNotesChanges(WO.NotesChanges, GeneratedKeys);

    SetPhase('Processing WO Addin info changes');
    Op.Detail := 'Addin info';
    GetAddinInfoDM.ProcessAddinInfoChanges(WO.AddinInfoChanges, GeneratedKeys);
  end;

  Op.Detail := 'Done - Status Work Orders';
  Op.Log('Done - Status Work Orders');
end;

{------------------------------------------------------------------------------}
{--StatusWorkOrders5:
   This is almost an exact copy of StatusWorkOrders2, with the exception of
   notes subtype which was added for QMANTWO-77. Once all clients have updated
   to the associated build, the StatusWorkOrders2 can be totally removed.
}
procedure TWorkOrderDM.StatusWorkOrders5(
  const WorkOrderChanges: WorkOrderChangeList5;
  out GeneratedKeys: GeneratedKeyList);
const
  ExistingByID = 'select assigned_to_id, status, closed from work_order where wo_id=:wo_id';
var
  i: Integer;
  WO: WorkOrderChange5;
  WorkOrderID: Integer;
  AssignedToID: Integer;
  NewStatus: String;
  Closed: Boolean;
  StatusDate: TDateTime;
  StatusedHow: String;
  StatusedByID: Integer;

  procedure UpdateWO;
  begin
    SetPhase('Editing existing work order');

    UpdateWorkOrder.Parameters.ParamByName('wo_id').Value := WorkOrderID;
    UpdateWorkOrder.Parameters.ParamByName('work_description').Value := WO.WorkDescription;
    UpdateWorkOrder.Parameters.ParamByName('work_cross').Value := WO.WorkCross;
    UpdateWorkOrder.Parameters.ParamByName('state_hwy_row').Value := WO.StateHwyROW;
    UpdateWorkOrder.Parameters.ParamByName('road_bore_count').Value := WO.RoadBoreCount;
    UpdateWorkOrder.Parameters.ParamByName('driveway_bore_count').Value := WO.DrivewayBoreCount;
    UpdateWorkOrder.Execute;

    // TODO-3339: Refactor this to call SetWorkOrderStatus instead
    SetPhase('Calling sp to insert wo_status_history and the wo_responder_queue');
    { TODO: This will be needed if we ever add a way to change the assigned worker from WO Detail.
    if (WODS.FieldByName('assigned_to_id').AsInteger <> WO.AssignedToID)
      and (not CanI(RightWorkOrdersAssignNewWorkOrders)) then
      raise EOdNotAllowed.Create('You do not have permission to change a work order''s worker.');
    }
    if (WorkOrder.FieldByName('status').AsString <> NewStatus) then begin
      UpdateWorkOrderStatus.Parameters.ParamByName('@WorkOrderID').Value := WorkOrderID;
      UpdateWorkOrderStatus.Parameters.ParamByName('@NewStatus').Value := NewStatus;
      UpdateWorkOrderStatus.Parameters.ParamByName('@Closed').Value := Closed;
      UpdateWorkOrderStatus.Parameters.ParamByName('@StatusDate').Value := StatusDate;
      if StatusedByID <> 0 then
        UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := StatusedByID
      else
        UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := AssignedToID;
      UpdateWorkOrderStatus.Parameters.ParamByName('@StatusedHow').Value := StatusedHow;
      UpdateWorkOrderStatus.ExecProc;
    end;
  end;

begin
  GeneratedKeys := GeneratedKeyList.Create;

  for i := 0 to WorkOrderChanges.Count - 1 do begin
    SetPhase(Format('Work Order %d of %d', [i + 1, WorkOrderChanges.Count]));
    Op.Detail := 'Loading work orders';

    WO := WorkOrderChanges[i];
    WorkOrderID := WO.WorkOrderID;
    AssignedToID := WO.AssignedToID;
    NewStatus := WO.Status;
    Closed := WO.Closed;
    StatusDate := WO.StatusDate;
    StatusedHow := WO.StatusedHow;
    StatusedByID := WO.StatusedByID;

    Op.Detail := 'Finding Work Order ' + IntToStr(WorkOrderID);
    WorkOrder.Close;
    WorkOrder.CommandText := ExistingByID;
    WorkOrder.Parameters.ParamValues['wo_id'] := WorkOrderID;
    WorkOrder.ExecuteOptions := [];
    WorkOrder.Open;
    Assert((WorkOrder.RecordCount=1), 'More than one work order was returned for the wo_id in the StatusWorkOrders5 operation.');

    if WorkOrder.FieldByName('closed').AsBoolean then
      Op.Detail := Format('Editing closed work order. Current status=%s; new status=%s.',
        [WorkOrder.FieldByName('status').AsString, NewStatus]);

    UpdateWO;

    SetPhase('Processing WO Notes changes');
    Op.Detail := 'NewNotes';
    GetNotesDM.ProcessNotesChanges5(WO.NotesChanges, GeneratedKeys);

    SetPhase('Processing WO Addin info changes');
    Op.Detail := 'Addin info';
    GetAddinInfoDM.ProcessAddinInfoChanges(WO.AddinInfoChanges, GeneratedKeys);
  end;

  Op.Detail := 'Done - Status Work Orders';
  Op.Log('Done - Status Work Orders');

end;

//QMANTWO-391 EB - OHM Details are readonly except for the curbvaluefound
procedure TWorkOrderDM.SaveWorkOrdersOHM(const WorkOrderOHMChanges: WorkOrderOHMChangeList);
var
  i : integer;
  OHM: WorkOrderOHMChange;
  CVF: Boolean;
begin

  for i := 0 to WorkOrderOHMChanges.Count - 1 do begin
    SetPhase(Format('Work Order OHM %d of %d', [i + 1, WorkOrderOHMChanges.Count]));
    Op.Detail := 'Saving Work Order OHM Detail Changes';

    OHM := WorkOrderOHMChanges[i];
    UpdateWorkOrderOHMDetails.Parameters.ParamByName('wo_id').Value := OHM.WorkOrderID;
    CVF := NVPairBoolean(OHM.OHMDetailsData, 'curbvaluefound', bvError);
    UpdateWorkOrderOHMDetails.Parameters.ParamByName('curbvaluefound').Value := CVF;
  // Use AssignParamsFromNVPairList() to assign parameters going forward....
    UpdateWorkOrderOHMDetails.ExecSQL;
  end;

  Op.Detail := 'Done - Saving Work Order OHM Details';
  Op.Log('Done - Saving Work Order OHM Details');
end;

{
 Change is declared as TROComplexType so it can be used by both
 StatusWorkOrderInspections and StatusWorkOrders2 which have 2 different RO
 structures.
}
procedure TWorkOrderDM.SetWorkOrderStatus(Change: TROComplexType);
var
  NewStatus: string;
  NewCGAReason: string;
  AssignedToID: Integer;
  StatusedByID: Integer;
begin
  SetPhase('Calling sp to insert wo_status_history and the wo_responder_queue');
  Assert(Change.HasProperty('Status'), 'Cannot find a status property.');
  Assert(Change.HasProperty('AssignedToID'), 'Cannot find a AssignedToID property.');
  Assert(Change.HasProperty('StatusedByID'), 'Cannot find a StatusedByID property.');

  NewStatus := Change.GetFieldValue('Status');
  Assert(Change.GetFieldValue('WorkOrderID') = WorkOrder.FieldByName('wo_id').AsInteger,
    'Expected WO # ' + Change.GetFieldValue('WorkOrderID') + ' but have ' +
    WorkOrder.FieldByName('wo_id').AsString);

  if Change.HasProperty('CGAReason') then begin //WGL Work order
    NewCGAReason := Change.GetFieldValue('CGAReason');
    if (WorkOrder.FieldByName('status').AsString = NewStatus) and (Inspection.FieldByName('cga_reason').AsString = NewCGAReason) then
      Exit; // status and CGA reason unchanged, so nothing to do
    UpdateWorkOrderStatus.Parameters.ParamByName('@CGAReason').Value := Change.GetFieldValue('CGAReason');
  end
  else //LAM01 work order
    if (WorkOrder.FieldByName('status').AsString = NewStatus) then
      Exit; // status unchanged, so nothing to do

  AssignedToID := Change.GetFieldValue('AssignedToID');
  StatusedByID := Change.GetFieldValue('StatusedByID');
  { TODO: This will be needed if we ever add a way to change the assigned worker from WO Detail.
  if (WorkOrder.FieldByName('assigned_to_id').AsInteger <> AssignedToID)
    and (not CanI(RightWorkOrdersAssignNewWorkOrders)) then
    raise EOdNotAllowed.Create('You do not have permission to change a work order''s worker.');
  }
  UpdateWorkOrderStatus.Parameters.ParamByName('@WorkOrderID').Value := Change.GetFieldValue('WorkOrderID');
  UpdateWorkOrderStatus.Parameters.ParamByName('@NewStatus').Value := NewStatus;
  UpdateWorkOrderStatus.Parameters.ParamByName('@Closed').Value := Change.GetFieldValue('Closed');
  UpdateWorkOrderStatus.Parameters.ParamByName('@StatusDate').Value := Change.GetFieldValue('StatusDate');
  if StatusedByID <> 0 then
    UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := StatusedByID
  else
    UpdateWorkOrderStatus.Parameters.ParamByName('@StatusBy').Value := AssignedToID;
  UpdateWorkOrderStatus.Parameters.ParamByName('@StatusedHow').Value := Change.GetFieldValue('StatusedHow');
  UpdateWorkOrderStatus.ExecProc;
end;

procedure TWorkOrderDM.StatusWorkOrderInspections(const WorkOrderChanges: WOInspectionChangeList; out GeneratedKeys: GeneratedKeyList);
const
  ExistingByID = 'select wo_id, assigned_to_id, status, closed from work_order where wo_id=:wo_id';
  ExistingInspectionByID = 'select wo_id, cga_reason from work_order_inspection where wo_id = :wo_id';
var
  i: Integer;
  WO: WOInspectionChange;
  WorkOrderID: Integer;
  NewStatus: string;

  procedure UpdateWO;
  begin
    SetWorkOrderStatus(WO);
  end;

  procedure UpdateWOInspection;
  begin
    AssignParamsFromROComplex(UpdateInspection.Parameters, WO, True);
    UpdateInspection.Parameters.ParamByName('InspectionChangeDate').Value := RoundSQLServerTimeMS(Now);
    UpdateInspection.Execute;
  end;

  function RemedyExists(const WORemedy: WorkRemedyChange; out RemedyID: Integer): Boolean;
  begin
    // check for an existing one by wo_id, work_type_id, active
    RemedyID := 0;
    Remedy.Close;
    Remedy.Parameters.ParamByName('WorkOrderID').Value := WO.WorkOrderID;
    Remedy.Parameters.ParamByName('WorkTypeID').Value := WORemedy.WorkTypeID;
    Remedy.Open;
    Assert(Remedy.RecordCount < 2, 'Found more than one matching work order remedy row.');
    Result := (Remedy.RecordCount = 1);
    if Result then
      RemedyID := Remedy.FieldByName('wo_remedy_id').AsInteger;
  end;

  procedure UpdateWORemedy;
  var
    I: Integer;
    RemedyID: Integer;
    WORemedy: WorkRemedyChange;
  begin
    for i := 0 to WO.RemedyChanges.Count - 1 do begin
      Op.Detail := 'Saving Work Remedy ' + IntToStr(i+1) + ' of ' +
        IntToStr(WO.RemedyChanges.Count);
      WORemedy := WO.RemedyChanges[i];
      if not RemedyExists(WORemedy, RemedyID) then begin
        AssignParamsFromROComplex(InsertRemedy.Parameters, WORemedy);
        InsertRemedy.Parameters.ParamByName('WorkOrderID').Value := WorkOrderID;
        InsertRemedy.Execute;
        RemedyID := LogicDM.GetAnyIdentity;
      end else begin
        Remedy.Edit;
        Remedy.FieldByName('active').AsBoolean := WORemedy.Active;
        Remedy.Post;
      end;
      if WORemedy.WORemedyID < 0 then
        AddIdentity(GeneratedKeys, 'work_order_remedy', WORemedy.WORemedyID, RemedyID);
    end;
  end;

begin
  GeneratedKeys := GeneratedKeyList.Create;

  for i := 0 to WorkOrderChanges.Count - 1 do begin
    SetPhase(Format('Work Order %d of %d', [i + 1, WorkOrderChanges.Count]));
    Op.Detail := 'Loading work orders';

    WO := WorkOrderChanges[i];
    WorkOrderID := WO.WorkOrderID;
    NewStatus := WO.Status;

    Op.Detail := 'Finding Work Order ' + IntToStr(WorkOrderID);
    WorkOrder.Close;
    WorkOrder.CommandText := ExistingByID;
    WorkOrder.Parameters.ParamValues['wo_id'] := WorkOrderID;
    WorkOrder.ExecuteOptions := [];
    WorkOrder.Open;
    Assert((WorkOrder.RecordCount=1), 'More than one work_order found for wo_id.');

    Op.Detail := 'Finding Inspection for Work Order ' + IntToStr(WorkOrderID);
    Inspection.Close;
    Inspection.CommandText := ExistingInspectionByID;
    Inspection.Parameters.ParamValues['wo_id'] := WorkOrderID;
    Inspection.ExecuteOptions := [];
    Inspection.Open;
    Assert((Inspection.RecordCount=1), 'More than one work_order_inspection found for wo_id.');

    if WorkOrder.FieldByName('closed').AsBoolean then
      Op.Detail := Format('Editing closed inspection. Current status=%s; new status=%s.',
        [WorkOrder.FieldByName('status').AsString, NewStatus]);

    Op.Detail := 'Saving Work Order ' + IntToStr(WorkOrderID);
    UpdateWO;
    Op.Detail := 'Saving Work Order Inspection ' + IntToStr(WorkOrderID);
    UpdateWOInspection;
    UpdateWORemedy;

    SetPhase('Processing Inspection Notes changes');
    Op.Detail := 'NewNotes';
    GetNotesDM.ProcessNotesChanges(WO.NotesChanges, GeneratedKeys);

    SetPhase('Processing Inspection Addin info changes');
    Op.Detail := 'Addin info';
    GetAddinInfoDM.ProcessAddinInfoChanges(WO.AddinInfoChanges, GeneratedKeys);
  end;

  Op.Detail := 'Done - Status Work Orders';
  Op.Log('Done - Status Work Orders');
end;

function TWorkOrderDM.WorkOrderSearch(const TicketNumber, WorkOrderNumber,
  Street, City, State, County, WOSource, ClientName,
  Company: string; const DueDateFrom, DueDateTo, TransmitDateFrom,
  TransmitDateTo: TDateTime; const Kind: string; const Tickets: Integer): string;
const
  TicketsClause = '(select count(*) from work_order_ticket wot where wot.wo_id = work_order.wo_id and active=1) ';
  Select =
  'select work_order.wo_id, work_order.wo_number, ' +
  TicketsClause + ' as tickets, ' +
  'dticket.ticket_number,' +
  ' work_order.work_address_number, work_order.work_address_number, ' +
  ' work_order.work_address_street, work_order.work_city, work_order.work_state, ' +
  ' work_order.work_county, work_order.work_type, work_order.wo_source, ' +
  ' work_order.client_id, work_order.caller_name, work_order.due_date, ' +
  ' work_order.transmit_date, work_order.kind, work_order.status, client.client_name ' +
  ' from work_order ';
  JoinClause = ' left join client on client.client_id = work_order.client_id ' +
    'left join (select t.ticket_number, wot.wo_id from ticket t join work_order_ticket wot on wot.ticket_id = t.ticket_id) dticket on dticket.wo_id = work_order.wo_id ' ;

  NolockClause = '(NOLOCK) ';
  NolockIndexClause = 'with (NOLOCK, INDEX(work_order_due_date)) ';
  WorkOrderNumberIndexClause = 'with (index(work_order_wo_number)) ';
  ModifiedDateClause = 'with (NOLOCK, INDEX(work_order_modified_date)) ';
var
  SQL: TOdSqlBuilder;
  FullSQL: string;
  PivotDate: TDateTime;
  OldDataSet, NewDataSet: TDataSet;

  function RunSearchInternal(DMToUse: TLogicDM; ModifiedSince: TDateTime; Clause: string): TDataSet;
  begin
    SQL := TOdSQLBuilder.Create;
    try
      SQL.TableAlias := 'work_order';
      Assert(Assigned(DMToUse), 'RunSearchForDueDateRange requires not-nil data module');

      SQL.AddExactStringFilter('work_order.wo_number',WorkOrderNumber);
      SQL.AddExactStringFilter('ticket_number', TicketNumber);
      SQL.AddKeywordSearch('work_order.work_address_street', Street);
      SQL.AddKeywordSearch('work_order.work_city', City);
      SQL.AddExactStringFilter('work_order.work_state', State);
      SQL.AddKeywordSearch('work_order.work_county', County);
      SQL.AddKeywordSearch('work_order.wo_source', WOSource);
      SQL.AddKeywordSearch('client.client_name', ClientName);
      SQL.AddKeywordSearch('work_order.caller_name', Company);
      SQL.AddExactStringFilter('work_order.kind', Kind);
      SQL.CheckFilterCount;

      SQL.AddDateTimeRangeFilter('work_order.transmit_date', TransmitDateFrom, TransmitDateTo);
      SQL.AddDateTimeRangeFilter('work_order.due_date', DueDateFrom, DueDateTo);

      SQL.AddDateTimeGreaterFilter('work_order.modified_date', ModifiedSince);

      if Tickets = Ord(wcHas) then
        SQL.AddNumericFilter(TicketsClause, '> 0')
      else if Tickets = Ord(wcDoesNotHave) then
        SQL.AddNumericFilter(TicketsClause, '= 0');

      FullSQL := Select + Clause + JoinClause + SQL.WhereClause;
      DMToUse.SearchQuery.CommandText := FullSQL;
      Op.Detail := Clause + JoinClause + SQL.WhereClause;

      DMToUse.SearchQuery.Open;

      Result := DMToUse.SearchQuery;
      Assert(Assigned(Result), 'RunSearchForDueDateRange should return non-nil query');
    finally
      FreeAndNil(SQL);
    end;
  end;

begin
  if SearchIsDisabled(FEATURE_NAME) then
    raise Exception.Create('Work Order Search is currently disabled due to a system problem. It will be enabled as soon as possible.');

  if ((TicketNumber = '') and (WorkOrderNumber = '')) and (DueDateFrom = 0) and (TransmitDateFrom = 0) then
    raise Exception.Create('You must specify a transmit-date range or a due-date range unless searching by ticket or work order number.');

  OldDataSet := nil;
  RepDM := nil;
  try
    if WorkOrderNumber <> '' then begin
      SetPhase('Work Order Number query, all Main DB');
      NewDataSet := RunSearchInternal(LogicDM, 0, WorkOrderNumberIndexClause);
    end else if ForceMainDBSearch(FEATURE_NAME) then begin
      SetPhase('Ini/keyword forced main DB search');
      NewDataSet := RunSearchInternal(LogicDM, 0, NolockClause);
    end else begin
      ConnectRepDM;
      PivotDate := ReportingDatabasePivot;
      SetPhase('Historical query in ' + RepDM.DBName);
      OldDataSet := RunSearchInternal(RepDM, 0, NolockIndexClause);

      SetPhase('Current data query in Main DB');
      NewDataSet := RunSearchInternal(LogicDM, PivotDate, ModifiedDateClause);
    end;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([NewDataSet, OldDataSet], 2000, 'wo_id');
  finally
    FreeAndNil(RepDM);
  end;
end;

end.
