unit LogicDMu;

interface

uses
  SysUtils, Classes, ADODB, DB, OperationTracking, Variants,
  OdMiscUtils, EventSourceDMu, RemObjectsUtils, QMServerLibrary_Intf;

type
  TParamRec = record
    Name: WideString;
    ParamType: TFieldType;
    Dir: TParameterDirection;
    Size: Integer; //QM-486 Virtual Bucket EB
  end;

type
  TLogicDM = class(TDataModule)
    Conn: TADOConnection;
    CheckLogin: TADOStoredProc;
    ChangePassword: TADOStoredProc;
    GetScopeIdentityCommand: TADOCommand;
    GetAnyIdentityCommand: TADOCommand;
    EmpHier: TADODataSet;
    CheckRight: TADODataSet;
    LogReportResult: TADOCommand;
    RecordComputerInfo: TADOStoredProc;
    InsertIntoSyncLog: TADOCommand;
    SyncSP: TADOStoredProc;
    FindUserNotifEmail: TADOStoredProc;
    StartReportCommand: TADOCommand;
    CheckReportDataset: TADODataSet;
    Messages: TADODataSet;
    MessageDest: TADODataSet;                                       
    EmployeeActivity: TADODataSet;
    Area: TADODataSet;
    UploadQueue: TADODataSet;
    CompletionReportSP: TADOStoredProc;
    InsertTicketActivityAck: TADOStoredProc;
    ResetAPIKey: TADOStoredProc;
    Employee: TADODataSet;
    updUserLastLogin: TADOCommand;
    CheckADLogin: TADOStoredProc;
    qryTextMailAddress: TADOQuery;
    EmployeeActivityToday: TADOQuery;
    GetFirstTaskTime: TADOQuery;
    InsertcustomAnswer: TADOCommand;
    UpdateCustomAnswer: TADOCommand;
    CustomFormAnswer: TADOQuery;
    TrivialOperation: TADODataSet;
    AttachmentData: TADODataSet;
    SearchQuery: TADODataSet;
    GenericQuery: TADODataSet;
    GetTableData: TADODataSet;
    ErrorReport: TADODataSet;   //QM-100 EB Pt 2
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  public
    DesiredIsolation: TIsolationLevel;
    DefaultIsolation: TIsolationLevel;
    Op: TOpInProgress;
    CacheConnectionString, CacheIniName: string;
    LoggedInEmpID: Integer;
    LoggedInEmpShortName: string;
    LoggedInUID: Integer;
    LoggedInClientVersion: string;
    ClientVersion: TVersionNumber;
    DBName: string;
    LoggedInEmpAPIKey: string;
    EventsDM: TEventsDM;

    procedure CloseDataSets;
    procedure Connect;
    procedure Disconnect;
    function GetScopeIdentity: Integer;
    function GetAnyIdentity: Integer;
    function GetBreadCrumb(EmpID:integer):string;
    function GetBreadCrumbList(MgrID: integer): string;  //QMANTWO-302
    function CanI(const RightName: string; var LimitationString: string; EmpID: Integer): Boolean; overload;
    function CanI(const RightName: string; EmpID: Integer): Boolean; overload;
    function CanI(const RightName: string): Boolean; overload;
    function CanI(const RightName: string; var LimitationString: string): Boolean; overload;
    function GetDataSetForSQL(const SelectSQL: string): TDataSet;
    function CreateDataSetForSQL(const SelectSQL: string): TDataSet;
    function GetServerSetting(const Section, Setting, Default: string): string;
    function HaveFields(const TableName: string; FieldNames: array of string): Boolean;
    function SQLServerTableExists(const TableName: string): Boolean;
    function GetRightID(RightName: string): Integer;
    procedure UpdateDateTimeToIncludeMilliseconds(const TableName, PKField: string;
      PKValue: Integer; const DateField: string; DateValue: TDateTime);
    function GetCodeByRefID(const RefID : Integer): string;
    function CanIAccessEmpID(const LoggedInEmpID, EmpID: Integer; const Limitation: string): Boolean;
    function GetRefID(RefType, RefCode: String): Integer;
    function GetConfigurationDataInt(const SettingName: string; const Default: Integer): Integer;
    function GetConfigurationDataValue(const SettingName, Default: string): string;
    function GetEmployeeManager(const EmpID: Integer; var ManagerShortName: string): Integer;
    function ExecuteSQL(const SQL: string): Integer;
    function ExecuteSQLWithParams(const SQL: string; Params: array of const): Integer;
    procedure SendMessageToEmployee(const MessageNumber: string;
      const SendTo: Integer; const Destination, Subject, Body: string;
      const RuleID: Integer = -1; const EntryID: Integer=-1);
    procedure SetOpName(const OpName: string);
    function GetMultiParamSPAsXML(const StoredProcName: string;
      Parameters: array of TParamRec; Values: array of Variant): string;

    function GetXMultiparamSPAsXML(const StoredProcName: string;
      Parameters: array of TParamRec; Values: array of Variant): string; //QM-486 Virtual Bucket EB

    function GetSingleParamSPAsXML(const StoredProcName,
      ParamName: string; const Value: Variant): string;
    function GetIniBoolSetting(const Section, Ident: string; Default: Boolean): Boolean;
    function GetStringIniSetting(const Section, Ident, Default: string): string;
    procedure SendEmailWithIniSettings(IniSection, EmailTo, Subject, Body: string; FileList: TStrings = nil); //QM-9  Orign
    procedure SendEmailWithIniSettingsExp(IniSection, EmailFrom, EmailTo, Subject, Body: string; FileList: TStrings = nil);//QM-1076 HP Restriction - being explicit with call
    procedure DebugShowEmailinTextFile(IniSection, EmailFrom, EmailTo, Subject, Body: string; DebugFilePath: string); //QM-308 EB for debug
    procedure CheckApplicationVersion(Login: Boolean; var VersionMessage: string);
    function CanIManageEmployees(LimitationString: string; const EmpIDs: TIntegerArray;
      const IncludeLoggedInEmpID: Boolean; var BadEmpID: Integer; var BadEmpName:string): Boolean;

    procedure AddTicketActivityAck(TicketID: Integer; ActivityType: string; ActivityDate: TDateTime);
    function HasActivityToday(ActivityType: string; Emp_ID: integer): Boolean;

    function StoreEventsEnabled: Boolean;
    procedure UpdateEmployeeLocalUTCBias(const EmpID, LocalUTCBias: Integer);
    procedure UpdateEmployeePlatsUpdated(const EmpID: Integer; UpdateDate: TDateTime);
    function GetHomeDomain: string;
    procedure UpdatePhoneNo(EmpID:integer;NewPhoneNo:string); //qm-388  sr

    {First Task}
    procedure UpdateFirstTaskReminder(EmpID: integer; TimeStr: string); //QM-494 First Task EB
    function GetFirstTaskReminder(EmpID: integer): string; //QM-494 First Task EB

    {Custom Form Data}
    function SaveNewCustomFormData(NewRecList: CustomFormAnswerList): GeneratedKeyList;  //TRY9
    procedure SaveModCustomFormData(ModRecList: CustomFormAnswerList);

    {TSE}   //QM-833 TimeSheet TSE EB
    function EmpIsTSE(pEmpID: integer): boolean;  //QM-833 Timesheet TSE
  end;

function QuoteDate(AValue: TDateTime): string;


const
  DEFAULT_DOMAIN = 'DYNUTIL';
var
  LogicDmIniName: string;
  TicketSearchDisabledFlag: Boolean;
  WorkOrderSearchDisabledFlag: Boolean;

implementation

uses QMConst, OdAdoUtils, OdDbUtils, IniFiles, OdIsoDates, OdSqlXmlOutput,
  JclStrings, OdInternetUtil, dialogs;

{$R *.dfm}

function QuoteDate(AValue: TDateTime): string;
begin
  Result := IsoDateTimeToStrQuoted(AValue);
end;

procedure TLogicDM.CloseDataSets;
begin
  TrivialOperation.Close;
end;

procedure TLogicDM.Connect;
begin
  if CacheConnectionString = '' then begin
    CacheConnectionString := ConnectADOConnectionWithIni(Conn, LogicDmIniName);
  end else begin
    Assert(not Conn.Connected, 'The TLogicDM.Conn database is connected');
    Conn.ConnectionString := CacheConnectionString;
    Conn.Open;
  end;
end;

procedure TLogicDM.DataModuleCreate(Sender: TObject);
begin
  DefaultIsolation := ilReadCommitted;
  DesiredIsolation := DefaultIsolation;
  // For selects like the one below to work, SearchQuery.ParamCheck must be False.
  // select * from employee where short_name like '%"Pee Wee%' and modified_date > '1999-01-01 00:00:00.000'
  // The combination of an unmatched " and the :'s in the timestamp confuse the
  // TParameters collection gatherer.
  SearchQuery.ParamCheck := False;

  //Conn.Properties['SQLXML Version'].Value := 'SQLXML.3.0';
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
  DBName := '';

  EventsDM := TEventsDM.Create(Self, Conn);
end;

procedure TLogicDM.Disconnect;
begin
  Conn.Connected := False;
end;

function TLogicDM.GetAnyIdentity: Integer;
var
  RS: _RecordSet;
begin
  RS := GetAnyIdentityCommand.Execute;
  if (RS.EOF or VarIsNull(RS.Fields[0].Value)) then
    raise Exception.Create('No Identity to fetch');
  Result := RS.Fields[0].Value;
end;

function TLogicDM.GetBreadCrumb(EmpID: integer): string;
var
  Param: TParamRec;   //QMANTWO-175
begin
  Param.Name := '@empID';
  Param.ParamType := ftInteger;
  Param.Dir := pdInput;

  Result := GetSingleParamSPAsXML('GetBreadCrumbByEmpID', '@empID', empID);
end;

function TLogicDM.GetBreadCrumbList(MgrID: integer): string; //QMANTWO-302
var
  Param: TParamRec;   
begin
  Param.Name := '@MgrID';
  Param.ParamType := ftInteger;
  Param.Dir := pdInput;

  Result := GetSingleParamSPAsXML('GetBreadCrumbListByMgrID', '@MgrID', MgrID);
end;

function TLogicDM.GetScopeIdentity: Integer;
var
  RS: _RecordSet;
begin
  RS := GetScopeIdentityCommand.Execute;
  if (RS.EOF or VarIsNull(RS.Fields[0].Value)) then
    raise Exception.Create('No Scope Identity to fetch');
  Result := RS.Fields[0].Value;
end;

procedure TLogicDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;



function TLogicDM.CanI(const RightName: string; var LimitationString: string; EmpID: Integer): Boolean;
var
  Rights: TADODataSet;
  RightID: Integer;
begin
  RightID := GetRightID(RightName);
  Result := False;
  Rights := CheckRight;
  Rights.CommandText := Format('select allowed, limitation from dbo.get_effective_rights(%d) where right_id = %d', [EmpID, RightID]);
  Rights.Open;
  LimitationString := ''; //qm-126  sr
  try
    if (not Rights.Eof) and (Rights.FieldByName('allowed').AsString = 'Y') then begin
      Result := True;
      if Rights.FieldByName('limitation').AsString <> '-' then  //qm-126  sr
      LimitationString := Rights.FieldByName('limitation').AsString;  //qm-126  sr
    end;
  finally
    Rights.Close;
  end;
end;

function TLogicDM.CanI(const RightName: string; EmpID: Integer): Boolean;
var
  Limitation: string;
begin
  Result := CanI(RightName, Limitation, EmpID);
end;

function TLogicDM.CanI(const RightName: string): Boolean;
begin
  Result := CanI(RightName, LoggedInEmpID);
end;

function TLogicDM.CanI(const RightName: string;
  var LimitationString: string): Boolean;
begin
  Result := CanI(RightName, LimitationString, LoggedInEmpID);
end;

function TLogicDM.GetDataSetForSQL(const SelectSQL: string): TDataSet;
begin
  // Don't free the dataset returned here, it is a shared one
  // Also, don't try to create two of these at once, since it is shared
  Result := GenericQuery;
  Result.Close;
  (Result as TADODataSet).CommandText := SelectSQL;
  Result.Open;
end;

function TLogicDM.CreateDataSetForSQL(const SelectSQL: string): TDataSet;
begin
  // You must free this dataset when you are done with it
  Result := TADODataSet.Create(nil);
  try
    (Result as TADODataSet).Connection := Conn;
    (Result as TADODataSet).CommandText := SelectSQL;
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TLogicDM.GetServerSetting(const Section, Setting,
  Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(LogicDmIniName);
  try
    Result := Ini.ReadString(Section, Setting, Default);
  finally
    FreeAndNil(Ini);
  end;
end;



function TLogicDM.HaveFields(const TableName: string; FieldNames: array of string): Boolean;
var
  DataSet: TDataSet;
  i: Integer;
begin
  DataSet := CreateDataSetForSQL(Format('select * from %s where 0=1', [TableName]));
  try
    Result := True;
    for i := Low(FieldNames) to High(FieldNames) do begin
      if not Assigned(DataSet.FindField(FieldNames[i])) then begin
        Result := False;
        Break;
      end;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TLogicDM.SQLServerTableExists(const TableName: string): Boolean;
const
  Select = 'select count(*) TableCount from information_schema.tables where table_name=''%s''';
var
  DataSet: TDataset;
begin
  DataSet := CreateDataSetForSQL(Format(Select, [TableName]));
  try
    Result := DataSet.Fields[0].Value = 1;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TLogicDM.StoreEventsEnabled: Boolean;
begin
  Result := GetIniBoolSetting('Features', 'StoreEvents', False);
end;

function TLogicDM.GetRightID(RightName: string): Integer;
const
  SQL = 'Select right_id from right_definition where entity_data = ''%s''';
var
  DataSet: TDataSet;
begin
  Result := 0;
  DataSet := CreateDataSetForSQL(Format(SQL, [RightName]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('right_id').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TLogicDM.UpdateDateTimeToIncludeMilliseconds(const TableName,
  PKField: string; PKValue: Integer; const DateField: string;
  DateValue: TDateTime);
var
  SQL: string;
  DateString: string;
  Records: Integer;
begin
  Assert(NotEmpty(TableName) and NotEmpty(PKField) and NotEmpty(DateField), 'TableName, PKField, and DateField are required in UpdateDateTimeToIncludeMilliseconds');
  Assert(PKValue > 0, 'PKValue is required in UpdateDateTimeToIncludeMilliseconds');
  if FloatEquals(DateValue, 0.0) then
    DateString := 'null'
  else
    DateString := IsoDateTimeToStrQuoted(DateValue);
  // Verify the value is already rounded to the nearest valid SQL Server time (3.333 ms).  Only a valid check for new cients.
  //Assert(DateValue = RoundSQLServerTimeMS(DateValue), 'Non-rounded SQL Server timestamp detected: ' + ISODateTimeToStr(DateValue));
  SQL := Format('update %s set %s = %s where %s = %s', [TableName, DateField, DateString, PKField, IntToStr(PKValue)]);
  Conn.Execute(SQL, Records);
  // Assertions here might mean you need a "set nocount off" in the TableName trigger
  Assert(Records = 1, 'Wrong number of records changed in UpdateDateTimeToIncludeMilliseconds: ' + IntToStr(Records));
end;

function TLogicDM.GetCodeByRefID(const RefID : Integer): string;
const
  SelectCode = 'select code from reference where ref_id = %d';
var
  D: TDataSet;
begin
  Result := '';
  D := GetDataSetForSQL(Format(SelectCode, [RefID]));
  if not D.Eof then
    Result := D.FieldByName('Code').AsString;
  D.Close;
end;

function TLogicDM.CanIAccessEmpID(const LoggedInEmpID, EmpID: Integer; const Limitation: string): Boolean;
const
  SQL = 'select h_emp_id from get_hier(%d, 0) where h_emp_id = %d ';
var
  Limitations: TStringList;
  i: Integer;
  ManagerID: Integer;
begin
  Limitations := TStringList.Create;
  try
    if (Limitation <> '') and (Limitation <> '-') then
      Limitations.CommaText := Limitation;
    EmpHier.Close;
    EmpHier.CommandText := Format(SQL, [LoggedInEmpID, EmpID]);
    for i := 0 to Limitations.Count - 1 do begin
      ManagerId := StrToIntDef(Limitations[i], -1);
      EmpHier.CommandText := EmpHier.CommandText +
        ' union ' + Format(SQL, [ManagerId, EmpID]);       //qm-126
    end;
    EmpHier.Open;
    Result := HasRecords(EmpHier);
  finally
    EmpHier.Close;
    FreeAndNil(Limitations);
  end;
end;

function TLogicDM.GetRefID(RefType, RefCode: String): Integer;
const
  SQL = 'Select ref_id from reference where type = ''%s'' and code = ''%s''';
var
  DataSet: TDataSet;
begin
  Result := 0;
  DataSet := CreateDataSetForSQL(Format(SQL, [RefType, RefCode]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('ref_id').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TLogicDM.GetConfigurationDataValue(const SettingName, Default: string): string;
const
  Select = 'select * from configuration_data where name = %s';
var
  Data: TDataSet;
begin
  Result := Default;
  Data := GetDataSetforSQL(Format(Select, [QuotedStr(SettingName)]));
  try
    if Data.RecordCount = 1 then
      Result := Data.FieldByName('value').AsString;
  finally
    Data.Close;
  end;
end;

function TLogicDM.GetConfigurationDataInt(const SettingName: string; const Default: Integer): Integer;
var
  Value: string;
begin
  Value := GetConfigurationDataValue(SettingName, IntToStr(Default));
  if not TryStrToInt(Value, Result) then
    Result := Default;
end;

function TLogicDM.GetEmployeeManager(const EmpID: Integer; var ManagerShortName: string): Integer;
const
  Select = 'select * from dbo.get_emp_superiors(%d) where active = 1 and distance < 0';
begin
  ManagerShortName := '-';
  Result := -1;
  EmpHier.CommandText := Format(Select, [EmpID]);
  EmpHier.Open;
  try
    if not EmpHier.IsEmpty then begin
      Result := EmpHier.FieldByName('emp_id').AsInteger;
      ManagerShortName := EmpHier.FieldByName('short_name').AsString;
    end;
  finally
    EmpHier.Close;
  end;
end;





function TLogicDM.GetHomeDomain: string;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(LogicDmIniName);
  try
    Result := Ini.ReadString('Domain_Settings', 'HomeDomain', DEFAULT_DOMAIN);
  finally
    FreeAndNil(Ini);
  end;
end;

function TLogicDM.EmpIsTSE(pEmpID: integer): boolean; //QM-833 Timesheet TSE EB
var
  pLimitation: string;
begin
  Result := False;
  if CanI(RightTimesheetsEntryTSE, pLimitation, pEmpiD) then begin
    Result := (pLimitation = TimesheetTSESource);
  end;
end;

function TLogicDM.ExecuteSQL(const SQL: string): Integer;
var
  Command: TADOCommand;
  RecordsAffected: Integer;
begin
  Command := TADOCommand.Create(nil);
  try
    Command.Connection := Conn;
    Command.CommandText := SQL;
    Command.ExecuteOptions := [eoExecuteNoRecords];
    Command.Execute(RecordsAffected, EmptyParam);
    Result := RecordsAffected;
  finally
    FreeAndNil(Command);
  end;
end;

function TLogicDM.ExecuteSQLWithParams(const SQL: string; Params: array of const): Integer;
var
  Command: TADOCommand;
  RecordsAffected: Integer;
begin
  Command := TADOCommand.Create(nil);
  try
    Command.Connection := Conn;
    Command.CommandText := SQL;
    SetParams(Command, Params);
    Command.ExecuteOptions := [eoExecuteNoRecords];
    Command.Execute(RecordsAffected, EmptyParam);
    Result := RecordsAffected;
  finally
    FreeAndNil(Command);
  end;
end;

procedure TLogicDM.SendEmailWithIniSettings(IniSection, EmailTo, Subject,     //original
  Body: string; FileList: TStrings);   //QM-9 retained for backward compatibility
begin
  Assert(Assigned(Op), 'Cannot send e-mail with subject "' + Subject + '" without an active Operation');
  try
    OdInternetUtil.SendEmailWithIniSettings(LogicDmIniName, IniSection, EmailTo, '',
      Subject, Body, FileList, True);                                               //QM-137 placeholder for BCC  sr
  except
    on E: Exception do begin
      Op.Log('Error Sending Email: ' + CompressWhiteSpace(E.Message));
    end;
  end;
end;

procedure TLogicDM.SendMessageToEmployee(const MessageNumber: string; const SendTo: Integer; const Destination, Subject, Body: string; const RuleID: Integer = -1; const EntryID: Integer=-1);
var
  MessageID: Integer;
  MessageRuleID: string;
  MessageTSEEntryID: string;
const
  MessageSelect = 'select * from message where message_number = ''%s'' and from_emp_id = %d ' +
    'and destination = %s and subject = ''%s'' and substring(body, 1, 255) = ''%s''';
  MessageDestSelect = 'select * from message_dest where message_id = %d and emp_id = %d and rule_id = %s';
  MessageDestInsert = 'insert into message_dest(message_id, emp_id, rule_id, tse_entry_id) values (%d, %d, %s, %s)';
begin
  Op.Phase := 'Checking for existing message';
  Messages.Close;
  Messages.CommandText := Format(MessageSelect, [MessageNumber, LoggedInEmpID, QuotedStr(Destination), Subject,
    Copy(Body, 1, 255)]);
  Messages.Open;
  if Messages.IsEmpty then begin
    Op.Phase := 'Inserting new message';
    Messages.Insert;
    Messages.FieldByName('message_number').AsString := MessageNumber;
    Messages.FieldByName('from_emp_id').AsInteger := LoggedInEmpID;
    Messages.FieldByName('sent_date').AsDateTime := Now;
    Messages.FieldByName('show_date').AsDateTime := Date;
    Messages.FieldByName('destination').AsString := Destination;
    Messages.FieldByName('subject').AsString := Subject;
    Messages.FieldByName('body').AsString := Body;
    Messages.FieldByName('expiration_date').AsDateTime := Now + 30;
    Messages.Post;
  end;
  Op.Phase := 'Checking for message destination';
  MessageID := Messages.FieldByName('message_id').AsInteger;
  if RuleID > 0 then
    MessageRuleID := IntToStr(RuleID)
  else
    MessageRuleID := 'null';
  if EntryID > 0 then
    MessageTSEEntryID := IntToStr(EntryID)
  else
    MessageTSEEntryID := 'null';
  Messages.Close;
  Messages.CommandText := Format(MessageDestSelect, [MessageID, SendTo, MessageRuleID]);
  Messages.Open;
  try
    if Messages.IsEmpty then begin
      Op.Phase := 'Inserting message destination';
      ExecuteSQL(Format(MessageDestInsert, [MessageID, SendTo, MessageRuleID, MessageTSEEntryID]));
    end;
  finally
    Messages.Close;
  end;
end;

procedure TLogicDM.AddTicketActivityAck(TicketID: Integer; ActivityType: string; ActivityDate: TDateTime);
begin
  Op.Detail := Format('Insert Ticket Activity %s for Ticket %d', [ActivityType, TicketID]);
  Assert(TicketID > 0);
  InsertTicketActivityAck.Parameters.ParamByName('@TicketID').Value := TicketID;
  InsertTicketActivityAck.Parameters.ParamByName('@ActivityType').Value := ActivityType;
  InsertTicketActivityAck.Parameters.ParamByName('@ActivityDate').Value := ActivityDate;
  InsertTicketActivityAck.Parameters.ParamByName('@ActivityEmpID').Value := LoggedInEmpID;
  InsertTicketActivityAck.ExecProc;
end;

function TLogicDM.HasActivityToday(ActivityType: string; Emp_ID: integer): Boolean;   //QM-100 EB UtiliSafe (also as utility)
const
  SQL = 'Select * from employee_activity ' +
        'where activity_type = ''%s'' ' +
        'and emp_id = %d ' +
        'and (activity_date >= (convert(datetime, convert(varchar, GetDate(), 101)))) ';
begin
  Result := False;

  Assert(ActivityType <> '');
  EmployeeActivityToday.Parameters.ParamByName('ActivityType').Value := ActivityType;
  EmployeeActivityToday.Parameters.ParamByName('emp_id').Value := Emp_ID;
  try
    EmployeeActivityToday.Open;
    if EmployeeActivityToday.EOF then
      Result := False
    else
      Result := True;
  finally
    EmployeeActivityToday.Close;
  end;
end;

procedure TLogicDM.SetOpName(const OpName: string);
begin
  Assert(Assigned(Op), 'No active Operation defined');
  Op.OpName := OpName;
end;

function TLogicDM.GetSingleParamSPAsXML(const StoredProcName, ParamName: string;
  const Value: Variant): string;
var
  Param: TParamRec;
begin
  Param.Name := ParamName;
  Param.ParamType := ftInteger;
  Param.Dir := pdInput;
  Result := GetMultiParamSPAsXML(StoredProcName, [Param], [Value]);
end;

function TLogicDM.GetMultiParamSPAsXML(const StoredProcName: string;
  Parameters: array of TParamRec; Values: array of Variant): string;
var
  I: Integer;
  SP: TADOStoredProc;
begin
  Assert(Length(Values) = Length(Parameters));

  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := StoredProcName;
    for I := Low(Parameters) to High(Parameters) do
      SP.Parameters.CreateParameter(Parameters[I].Name, Parameters[I].ParamType, Parameters[I].Dir, 0, Values[I]);

    SP.Open;
    Result := FormatSqlXml(SP);
  finally
    SP.Close;
    FreeAndNil(SP);
  end;
end;

function TLogicDM.GetXMultiparamSPAsXML(const StoredProcName: string;
      Parameters: array of TParamRec; Values: array of Variant): string; //QM-486 Virtual Bucket EB
var
  I: Integer;
  SP: TADOStoredProc;
begin
  Assert(Length(Values) = Length(Parameters));

  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := StoredProcName;
    for I := Low(Parameters) to High(Parameters) do
      SP.Parameters.CreateParameter(Parameters[I].Name, Parameters[I].ParamType,
                                    Parameters[I].Dir, Parameters[i].size, Values[I]);

    SP.Open;
    Result := FormatSqlXml(SP);
  finally
    SP.Close;
    FreeAndNil(SP);
  end;
end;


function TLogicDM.GetIniBoolSetting(const Section, Ident: string; Default: Boolean): Boolean;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(LogicDmIniName);
  try
    Result := Ini.ReadBool(Section, Ident, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

function TLogicDM.GetStringIniSetting(const Section, Ident, Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(LogicDmIniName);
  try
    Result := Ini.ReadString(Section, Ident, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

function TLogicDM.CanIManageEmployees(LimitationString: string; const EmpIDs: TIntegerArray;
  const IncludeLoggedInEmpID: Boolean; var BadEmpID: Integer; var BadEmpName:string): Boolean;
var
  i, ManagerId: Integer;
  Limitations: TStrings;
begin
  if IsEmpty(LimitationString) then // Default to me for blank limitations
    LimitationString := IntToStr(LoggedInEmpID);

  Limitations := TStringList.Create;
  try
    StrToStrings(LimitationString, ',', Limitations, False);
    EmpHier.Close;
//    EmpHier.CommandText := '';
//    EmpHier.CommandText := Format('select short_name from employee where emp_id = %d ',[BadEmpID]);
//    EmpHier.Open;
    for i := 0 to Limitations.Count - 1 do
    begin
      ManagerId := StrToIntDef(Limitations[i], LoggedInEmpID);
      if EmpHier.CommandText = '' then
      begin
        EmpHier.CommandText := Format('select * from get_hier(%d, 0)',  [ManagerId]);
        if not IncludeLoggedInEmpID then
          EmpHier.CommandText := EmpHier.CommandText + Format(' where h_emp_id <> %d', [LoggedInEmpID]);
      end else  //if EmpHier.CommandText = ''
      begin
        EmpHier.CommandText := EmpHier.CommandText + ' union ' + Format('select * from get_hier(%d, 0)', [ManagerId]);    //qm-126  sr
        if not IncludeLoggedInEmpID then
          EmpHier.CommandText := EmpHier.CommandText + Format(' where h_emp_id <> %d', [LoggedInEmpID]);
      end;  //else
    end;  //for i := 0 to Limitations.Count - 1 do
//    showmessage('CanIManageEmployees: ' +EmpHier.CommandText);   //qm-126  sr
    EmpHier.Open;
    for i := Low(EmpIDs) to High(EmpIDs) do begin
      if not EmpHier.Locate('h_emp_id', EmpIDs[i], []) then begin
        Result := False;
        BadEmpID := EmpIDs[i];
        Employee.Close;
        Employee.CommandText := '';
        Employee.CommandText := Format('select short_name from employee where emp_id = %d ',[BadEmpID]);
        Employee.Open;
        BadEmpName := Employee.FieldByName('short_name').AsString;
        Employee.close;
        Exit;
      end;
    end;
    Result := True;
  finally
    EmpHier.Close;
    FreeAndNil(Limitations);
  end;
end;


procedure TLogicDM.SaveModCustomFormData(ModRecList: CustomFormAnswerList);
var
  i: integer;

  {Fields: custom_form_answer}
  AnswerID: integer; //should be positive (existing record)
  FormFieldID: integer;
  AnswerStr: string;
  AnsweredBy: integer;
  AnswerDate: TDateTime;
  CFAnswer: NVPairList;
begin
  for i := 0 to ModRecList.Count - 1 do begin
    CFAnswer := ModRecList[i].AnswerData;

    AnswerID := NVPairInteger(CFAnswer, 'answer_id', bvError);
    FormFieldID := NVPairInteger(CFAnswer, 'form_field_id', bvError);
    AnswerStr := NVPairString(CFAnswer, 'answer', bvNull);
    AnsweredBy := NVPairInteger(CFAnswer, 'answered_by', bvNull);
    AnswerDate := NVPairDateTime(CFAnswer, 'answer_date', bvError);


    With UpdateCustomAnswer.Parameters do begin
      ParamByName('answer_id').Value := AnswerID;
      ParamByName('answer').Value := AnswerStr;
      ParamByName('answered_by').Value := AnsweredBy;
      ParamByName('answer_date').Value := AnswerDate;
      UpdateCustomAnswer.Execute;

    end;
  end;

end;

function TLogicDM.SaveNewCustomFormData(NewRecList: CustomFormAnswerList): GeneratedKeyList;
const
  dsSQL = 'select * from custom_form_answer where answer_date=GetDate()+1';
  {note: this is just a straight insert, date is not an issue.}
var
  i: integer;

  {Fields: custom_form_answer}
  AnswerID: integer; //should be negative coming in
  FormFieldID: integer;
  AnswerStr: string;
  AnsweredBy: integer;
  AnswerDate: TDateTime;
  CFAnswer: NVPairList;
  ForeignID: integer;
  NewAnswerID: integer;

begin
  CustomFormAnswer.Open;
  Result := GeneratedKeyList.Create;
  try
    for i := 0 to NewRecList.Count - 1 do begin
      CFAnswer := NewRecList[i].AnswerData;
      AnswerID := NVPairInteger(CFAnswer, 'answer_id', bvError);
      FormFieldID := NVPairInteger(CFAnswer, 'form_field_id', bvError);
      AnswerStr := NVPairString(CFAnswer, 'answer', bvNull);
      AnsweredBy := NVPairInteger(CFAnswer, 'answered_by', bvNull);
      AnswerDate := NVPairDateTime(CFAnswer, 'answer_date', bvError);
      ForeignID := NVPairInteger(CFAnswer, 'foreign_id', bvNull);  {only required for forms that can be opened multiple times for different tickets, damages, etc}

      With CustomFormAnswer do begin
        Insert;
        FieldByName('form_field_id').AsInteger := FormFieldID;
        FieldByName('answer').AsString := AnswerStr;
        FieldByName('answered_by').AsInteger := AnsweredBy;
        FieldByName('answer_date').AsDateTime := AnswerDate;
        FieldByName('foreign_id').AsInteger := ForeignID;
        Post;
        NewAnswerID := FieldByName('answer_id').AsInteger;
        AddIdentity(Result, 'custom_form_answer', AnswerID, NewAnswerID);  //TRY9
      end;
    end;
  finally
    CustomFormAnswer.Close;
  end;


   // With InsertCustomAnswer.Parameters do begin
//      ParamByName('form_field_id').Value := FormFieldID;
//      ParamByName('answer').Value := AnswerStr;
//      ParamByName('answered_by').Value := AnsweredBy;
//      ParamByName('answer_date').Value := AnswerDate;
//      ParamByName('foreign_id').Value := ForeignID;
//      InsertCustomAnswer.Execute;
//      NewAnswerID := GetAnyIdentity;
//      if (AnswerID < 0) and (NewAnswerID > 0) then
//        AddIdentity(NewKeys, 'custom_form_answer',AnswerID, NewAnswerID);
//    end;

end;

procedure TLogicDM.SendEmailWithIniSettingsExp(IniSection, EmailFrom, EmailTo, Subject, Body: string;FileList: TStrings); //QM-9  New for SMS
begin
  Assert(Assigned(Op), 'Cannot send e-mail with subject "' + Subject + '" without an active Operation');
  try
    OdInternetUtil.SendEmailWithIniSettings(LogicDmIniName, IniSection, EmailFrom, EmailTo,'',           //qm-137 BCC placeholder  SR
      Subject, Body, FileList, True);
  except
    on E: Exception do begin
      Op.Log('Error Sending Email: ' + CompressWhiteSpace(E.Message));
    end;
  end;
end;

procedure TLogicDM.DebugShowEmailinTextFile(IniSection, EmailFrom, EmailTo,
  Subject, Body: string; DebugFilePath: string);    //QM-308 EB For non email testing
var
  ServerTxtFile: TStringList;
  DateStr: string;
begin
  ServerTxtFile := TStringList.Create;
  DateStr :=  formatdatetime('YYMMDD-hhnnssz', Now);
  try
    ServerTxtFile.Add(IniSection);
    ServerTxtFile.Add(' ');
    ServerTxtFile.Add('Email From: ' + EmailFrom);
    ServerTxtFile.Add('Email To: ' + EmailTo);
    ServerTxtFile.Add('Subject: ' + Subject);
    ServerTxtFile.Add('----------------------------');
    ServerTxtFile.Add(Body);

    ServerTxtFile.SaveToFile(DebugFilePath + '\' + 'HPEmailText' + DateStr + '.txt' );
  finally
    FreeAndNil(ServerTxtFile);
  end;
end;

procedure TLogicDM.CheckApplicationVersion(Login: Boolean; var VersionMessage: string);
const
  Msg = 'Your application version %s is %s.  Please use the auto-upgrade feature.';
  ClientVersionsSection = 'ClientVersions';
  BlockedVersions: array [0..1] of string = ('1.0.3.6', '1.0.3.8');
var
  WarnForOlderThan: string;
  NoOperationsForOlderThan: string;
  NoLoginForOlderThan: string;
begin
  if (not Login) and (StringInArray(LoggedInClientVersion, BlockedVersions)) then
    raise Exception.Create('Please upgrade to a newer version of Q Manager');

  WarnForOlderThan := GetServerSetting(ClientVersionsSection, 'WarnForOlderThan', '1.0.4.0');
  NoOperationsForOlderThan := GetServerSetting(ClientVersionsSection, 'NoOperationsForOlderThan', '1.0.3.11');
  NoLoginForOlderThan := GetServerSetting(ClientVersionsSection, 'NoLoginForOlderThan', '0.0.0.0');

  if CompareVersionNumber(ClientVersion, ParseVersionString(WarnForOlderThan)) < 0 then
    VersionMessage := Format(Msg, [LoggedInClientVersion, 'outdated']);

  if (not Login) and (CompareVersionNumber(ClientVersion, ParseVersionString(NoOperationsForOlderThan)) < 0) then
    raise Exception.CreateFmt(Msg, [LoggedInClientVersion, 'too old to communicate with the server']);

  if CompareVersionNumber(ClientVersion, ParseVersionString(NoLoginForOlderThan)) < 0 then
    raise Exception.CreateFmt('Your application version %s is too old to login or communicate with this server.  Please install a newer version of Q Manager.', [LoggedInClientVersion]);
end;

procedure TLogicDM.UpdateEmployeeLocalUTCBias(const EmpID, LocalUTCBias: Integer);
const
  UpdateSQL = 'update employee set local_utc_bias = :local_utc_bias1 ' +
    'where emp_id = :emp_id and (local_utc_bias is null or local_utc_bias <> :local_utc_bias2)';
begin
  ExecuteSQLWithParams(UpdateSQL, [LocalUTCBias, EmpID, LocalUTCBias]);
end;

procedure TLogicDM.UpdateEmployeePlatsUpdated(const EmpID: Integer; UpdateDate: TDateTime);
const
  UpdateSQL = 'update employee set plat_update_date =:platsdate where emp_id = :emp_id';
var
  StrThisDate: string;
begin
  StrThisDate := DateTimeToStr(UpdateDate);
  ExecuteSQLWithParams(UpdateSQL, [StrThisDate, EmpID]);
end;

procedure TLogicDM.UpdateFirstTaskReminder(EmpID: integer; TimeStr: string);  //QM-494 First Task EB
const
 UpdateSQL =  'update employee '+
              'set first_task_reminder = :reminder  '+
              'where emp_id = :empID ';
begin
  ExecuteSQLWithParams(UpdateSQL, [TimeStr, EmpID]);
end;

function TLogicDM.GetFirstTaskReminder(EmpID: integer): string; //QM-494 First Task EB
begin
  Result := BlankTime;
  try
    GetFirstTaskTime.Parameters.ParamByName('empId').Value := EmpID;
    GetFirstTaskTime.Open;
    GetFirstTaskTime.First;
    if not GetFirstTaskTime.EOF then begin
      Result := GetFirstTaskTime.FieldByName('first_task_reminder').AsString;
      if Result = '' then
        Result := BlankTime;
    end;
  finally
    GetFirstTaskTime.Close;
  end;
end;

procedure TLogicDM.UpdatePhoneNo(EmpID: integer; NewPhoneNo: string);  //qm-388 sr
const
 UpdateSQL =  'update employee '+
              'set contact_phone = :phoneNo  '+
              'where emp_id = :empID ';

begin
  ExecuteSQLWithParams(UpdateSQL, [NewPhoneNo, EmpID]);
end;

end.
