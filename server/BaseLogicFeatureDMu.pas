unit BaseLogicFeatureDMu;

interface

uses
  SysUtils, Classes, LogicDMu, OperationTracking;

type
  TLogicFeatureDM = class(TDataModule)
  private
    procedure ReportConnectStatusProc(NewStatus: string);
  protected
    LogicDM: TLogicDM;
    RepDM: TLogicDM;
    function Op: TOpInProgress;
    procedure SetPhase(const Phase: string);
    procedure ConnectRepDM;
    function ReportingDatabasePivot: TDateTime; virtual;
    function ForceMainDBSearch(const FeatureName: string): Boolean; virtual;
    function SearchIsDisabled(const FeatureName: string): Boolean; virtual;
  public
    constructor Create(Owner: TComponent; DM: TLogicDM); overload;
  end;

implementation

{$R *.dfm}

uses OdAdoUtils, ReportConfig;

constructor TLogicFeatureDM.Create(Owner: TComponent; DM: TLogicDM);
begin
  inherited Create(Owner);

  Assert(Assigned(DM), 'No LogicDM assigned');
  Assert(Assigned(DM.Op), 'SetName not called before creating op DataModule');
  LogicDM := DM;
  RepDM := nil;
  SetConnections(LogicDM.Conn, Self);
end;

function TLogicFeatureDM.ForceMainDBSearch(const FeatureName: string): Boolean;
begin
  Assert(FeatureName <>  '', 'Missing FeatureName');
  Result := LogicDM.GetIniBoolSetting('Features', FeatureName+'SearchMainDB', False);
end;

function TLogicFeatureDM.SearchIsDisabled(const FeatureName: string): Boolean;
begin
  Assert(FeatureName <>  '', 'Missing FeatureName');
  Result := LogicDM.GetIniBoolSetting('Features', 'Disable'+FeatureName+'Search', False);
end;

function TLogicFeatureDM.Op: TOpInProgress;
begin
  Result := LogicDM.Op;
end;

procedure TLogicFeatureDM.SetPhase(const Phase: string);
begin
  Assert(Assigned(LogicDM.Op), 'No operation assigned');
  Op.Phase := Phase;
end;

procedure TLogicFeatureDM.ConnectRepDM;
var
  ReportingConfig: TReportConfig;
begin
  Assert(Assigned(LogicDM.Op), 'No active Operation defined');
  RepDM := TLogicDM.Create(nil);
  ReportingConfig := LoadConfig(LogicDmIniName);
  try
    ReportingConfig.ConnectReportingDB(RepDM.Conn, ReportConnectStatusProc);
    Assert(RepDM.Conn.Connected, 'ConnectRepDM must return with a connected DM');
    RepDM.DBName := Op.Phase;  // the final status from the above, is which DB it chose.
    Op.Log('Connected to reporting DB ' + RepDM.DBName);
  finally
    FreeAndNil(ReportingConfig);
  end;
end;

procedure TLogicFeatureDM.ReportConnectStatusProc(NewStatus: string);
begin
  SetPhase(NewStatus);
end;

function TLogicFeatureDM.ReportingDatabasePivot: TDateTime;
begin
  // 15 minutes ago:
  Result := Now - (15.0 / 60.0 / 24.0);
end;

end.
