inherited ServerBillingDM: TServerBillingDM
  OldCreateOrder = True
  Height = 406
  Width = 438
  object BillingCommitSP: TADOStoredProc
    ProcedureName = 'commit_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CommittedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 6
  end
  object BillingUncommitSP: TADOStoredProc
    ProcedureName = 'uncommit_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 54
  end
  object BillingDeleteSp: TADOStoredProc
    ProcedureName = 'delete_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 102
  end
  object BillingReexportSP: TADOStoredProc
    ProcedureName = 'queue_billing_reexport;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RequestBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 158
  end
  object BillingCallCenterGroup: TADODataSet
    CommandText = 'select center_group_id from billing_run where run_id = :run_id'
    Parameters = <
      item
        Name = 'run_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 138
    Top = 158
  end
  object CenterGroup: TADODataSet
    CommandText = 'select * from center_group_detail'
    Parameters = <>
    Left = 138
    Top = 104
  end
  object BillableCenterGroup: TADODataSet
    CommandText = 'select * from center_group'
    Parameters = <>
    Left = 138
    Top = 210
  end
  object BillingAdjustment: TADODataSet
    CommandText = 'select * from billing_adjustment where 0=1'
    Parameters = <>
    Left = 33
    Top = 210
  end
  object InsertBillingRun: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from billing_run where 1=0'
    EnableBCD = False
    Parameters = <>
    Left = 138
    Top = 4
  end
  object BillingQueueData: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select cg.group_code, br.period_end_date, br.span, e.short_name,' +
      #13#10' br.request_date, br.run_date, br.completed_date,'#13#10' br.run_id,' +
      ' br.center_group_id,'#13#10' case'#13#10'   when completed_date is null and ' +
      'run_date is null then '#39'Pending'#39#13#10'   when completed_date is null ' +
      'and run_date is not null then '#39'Running'#39#13#10'   when completed_date ' +
      'is not null and cancelled=1 then '#39'Cancelled'#39#13#10'   when completed_' +
      'date is not null and bill_id is null then '#39'Failed'#39#13#10'   when comp' +
      'leted_date is not null and bill_id is not null then '#39'Completed'#39#13 +
      #10'   else '#39'Unknown'#39#13#10' end as status'#13#10' from billing_run br'#13#10'  left' +
      ' join center_group cg on br.center_group_id=cg.center_group_id'#13#10 +
      '  left join employee e on br.request_by=e.emp_id'#13#10'where br.compl' +
      'eted_date is null'#13#10' or br.completed_date>dateadd(hh, -18, getdat' +
      'e())'#13#10
    EnableBCD = False
    Parameters = <>
    Left = 138
    Top = 54
  end
end
