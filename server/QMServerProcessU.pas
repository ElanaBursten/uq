unit QMServerProcessU;

interface

uses
  SysUtils, Classes, ThreadSafeLoggerU, FaxDispositionDMu;

type
  TQMServerProcess = class
  private
    Logger: TThreadSafeLogger;
    LogFilePath: string;
    LogEnabled: Boolean;
    StartupFailureMessage: string;
    InstanceId: Integer;
    FaxDM: TFaxDispositionDM;
  public
    constructor Create;
    destructor Destroy; override;
    function GetMonitorHtml: string;
    function GetStartupMessage: string;
    function GetFaxDM: TFaxDispositionDM;
  private
    procedure LoadConfig;
  end;

implementation

uses
  IniFiles, OperationTracking, LogicDMu;

{ TQMServerProcess }

constructor TQMServerProcess.Create;
begin
  try
    Randomize;
    InstanceId := Random(8999) + 1000;

    LoadConfig;
    Logger := TThreadSafeLogger.Create(LogFilePath, 'Logic.log', InstanceId, 0, LogEnabled);

    OperationTracking.RawLogger := Logger;
    GetOperationTracker.Log('Starting');

    if TicketSearchDisabledFlag then
      GetOperationTracker.Log('Ticket Search disabled.');

    if WorkOrderSearchDisabledFlag then
      GetOperationTracker.Log('Work Order Search disabled.');
  except
    on E: Exception do begin
      GetOperationTracker.Log('Startup Failure: ' + E.Message);
      StartupFailureMessage := E.Message;
    end;
  end;
end;

destructor TQMServerProcess.Destroy;
begin
  Logger.Log('Stopping');
  OperationTracking.RawLogger := nil;
  FreeAndNil(Logger);
  FreeAndNil(FaxDM);
  inherited;
end;

function TQMServerProcess.GetMonitorHtml: string;
begin
  Result := GetOperationTracker.GetHtml(StartupFailureMessage, InstanceId);;
end;

function TQMServerProcess.GetStartupMessage: string;
begin
  Result := 'Startup Status: ' + StartupFailureMessage;
end;

procedure TQMServerProcess.LoadConfig;
const
  IniError = 'Could not load LogFile Path setting in INI file: %s.';
var
  Ini: TIniFile;
begin
  if LogicDmIniName = '' then
    raise Exception.Create('LogicDmIniName not set');

  Ini := TIniFile.Create(LogicDmIniName);
  try
    LogFilePath := Ini.ReadString('LogFile', 'Path', '');
    TicketSearchDisabledFlag := Ini.ReadString('Features', 'DisableTicketSearch', '0') = '1';
    WorkOrderSearchDisabledFlag := Ini.ReadString('Features', 'DisableWorkOrderSearch', '0') = '1';
    LogEnabled := Ini.ReadString('LogFile', 'LogicLogEnabled', '1') = '1';
  finally
    FreeAndNil(Ini);
  end;

  if LogFilePath = '' then
    raise Exception.CreateFmt(IniError, [LogicDmIniName] );

end;

function TQMServerProcess.GetFaxDM: TFaxDispositionDM;
begin
  if FaxDM = nil then
    FaxDM := TFaxDispositionDM.Create(nil);
  Result := FaxDM;
end;

end.
