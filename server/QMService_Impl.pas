unit QMService_Impl;

{$I RemObjects.inc}

interface

uses
  {vcl:} Classes, SysUtils, Windows, Types,
  {RemObjects:} uROClientIntf, uROServer, 
  {Generated:} QMServerLibrary_Intf,
{$IFDEF ROSDK5UP}
  uROClassFactories, uROClasses,
{$ELSE}
  FixedClassFactory,
{$ENDIF}
  OdIntPacking, OdMiscUtils, LogicDMu, DB, ADODB_TLB, ADODB, uADSI,
  ServerBillingDMu, DamageDMu, TicketDMu, TimesheetDMu, WorkOrderDMu,
  ArrivalDMu, AssetDMu, ManageWorkDMu, AddinInfoDMu, NotesDMu, RemObjectsUtils;

type
  TXMLVars = record
    Name: WideString;
    ParamType: DataTypeEnum;
    Dir: ParameterDirectionEnum;
  end;

{$IFDEF ROSDK5UP}
  MemStream = TROBinaryMemoryStream;

  TQMService = class(TRORemotable, IQMService)
{$ELSE}
  MemStream = Binary;

  TQMService = class(TRORemotable, QMService)
{$ENDIF}
  private                                                            
    DM: TLogicDM;
    RepDM: TLogicDM;  // this is created temporarily, not held.
    BillingDM: TServerBillingDM;
    TimesheetDM: TTimesheetDM;
    WorkOrderDM: TWorkOrderDM;
    DamageDM: TDamageDM;
    TicketDM: TTicketDM;
    ArrivalDM: TArrivalDM;
    AssetDM: TAssetDM;
    ManageWorkDM: TManageWorkDM;
    AddinInfoDM: TAddinInfoDM;
    NotesDM: TNotesDM;
    LoggedInEmpNumber: string;
    LoggedInUserType: string;
    LoggedInChgPwd: Boolean;
    LoggedInChgPwdDate: TDateTime;
    LoggedInReportTo: Integer;
    FExecuteRecsAffected: Integer;
    FAllowExpiredLogin: Boolean;
    FBeforeExecuting: Boolean;
    function LoggedInEmpID: Integer;
    function LoggedInEmpShortName: string;
    function LoggedInUID: Integer;
    function LoggedInEmpAPIKey: string;
    procedure GetDM;
    procedure GetTimesheetDM;
    procedure GetBillingDM;
    procedure GetWorkOrderDM;
    procedure GetDamageDM;
    procedure GetTicketDM;
    procedure GetArrivalDM;
    procedure GetAssetDM;
    procedure GetManageWorkDM;
    procedure GetAddinInfoDM;
    procedure GetNotesDM;
    procedure CheckSecurity(const SecSession: string);
    procedure CheckUserSecurity(const UserName, Password, Version: string; SkipExpiredCheck: Boolean);
    procedure SetName(OpName: string);
    procedure SetPhase(const PhaseMessage: string);
    procedure CheckTimeWindow(SyncTime: TDateTime;const SyncTimeBias: Integer);
    procedure ReportErrorsInternal(const Errors: ErrorReportList);
    //procedure MarkRowModified(const TableName, PKName: string; PKValue: Integer);
    function SyncInternal(const SecSession: string; const UpdatesSince: string;
      const SyncRequestID: string; const ClientVersion: string; const LocalTime: DateTime;
      const UTCBias: Integer; const TicketInCacheCSV: string;
      const ComputerInfo: ComputerInformation; SentBytes: Integer = -1;
      AdditionalInfo: NVPairList = nil; const DamageInCacheCSV: string='';
      const WorkOrderInCacheCSV: string=''; const NumTicketsInClientCache: Integer=0): string;
    function GetSyncDataWithoutSQLXML(const UpdatesSince: string; const TicketInCacheCSV: string; const DamageInCacheCSV: string=''; const WorkOrderInCacheCSV: string=''): string;
    function GetIDList(const ItemArray: IntArray; var Added, InCache: Integer): string;
    function CanIDef(Right: string; var LimitationString: string; const DefaultLimitation: string): Boolean;
    function CanI(Right: string; var LimitationString: string): Boolean; overload;
    procedure LogSQLXMLProgress(var TableName: string; var Count: Integer);
    procedure SaveNewAttachmentsInternal(const SecSession: string; const NewAttachments: NewAttachmentList; out GeneratedKeys: GeneratedKeyList);
    procedure SaveAttachmentUpdatesInternal(const SecSession: string; const AttachmentUpdates: AttachmentUpdateList);
    procedure SaveAttachmentUploadsInternal(const SecSession: string; const AttachmentUploads: AttachmentUploadList);
    procedure LogAttachmentProgress(Sender: TObject; const LogMessage: string);
    procedure NotifyManagerOfEmployeeActivity(const ActivityType: string; EmpID: Integer; ActivityDate: TDateTime;Details,ExtraDetails:String);
    function SaveEmployeeActivityInternal(Activities: EmployeeActivityChangeList): GeneratedKeyList;
    function SaveEmployeeActivityInternal3(Activities: EmployeeActivityChangeList3): GeneratedKeyList;
    function InsertTicketNoteDirect(const SecSession: string; Const TicketID: Integer; Const EntryDate: TDateTime;    //qm-747  sr
     Const NoteText: string; Const SubType: Integer;  Const ClientCode: string; Const EnteredByID: Integer): Integer;  //qm-747  sr
  protected
    // General
    function Ping(const SecSession: string; const AnyText: string): string;
    function Login(const SecSession: string; const UserName: string; const Password: string; const AppVersion: string): loginData;
    function Login2(const SecSession: string; const UserName: string; const Password: string; const AppVersion: string): LoginData2;
    function LoginAD(const SecSession: string; const UserName: string): boolean;
    procedure ChangePassword(const SecSession: string; const UserID: Integer; const NewPassword: string);
    function GetTableData(const SecSession: string; const TableName: string; const UpdatesSince: DateTime): string;
    function GetColumnData(const SecSession, TableName, KeyField, Columns: string; const UpdatesSince: DateTime; const NonNullOnly: Boolean): string;
    procedure ReportErrors(const SecSession: string; const Errors: ErrorReportList);
    procedure ReportStatus(const SecSession: string; const RequestID: string; const Success: Boolean; const ElapsedTime: DateTime; const Details: string);
    procedure StartReport(const SecSession: string; const RequestID: string; const ReportParams: string; out WaitSeconds: Integer);
    procedure CheckReport(const SecSession: string; const RequestID: string; out Status: string; out Done: Boolean; out Url: string);
    function GetRights(const SecSession: string; const EmpID: Integer): string;
    procedure SendTextViaEmail(const SecSession: string; const EmpID: integer; const
      TxtMessage: String);    //QM-9

    function SyncDown6(const SecSession: string; const UpdatesSince: string; const SyncRequestID: string; const ClientVersion: string; const LocalTime: DateTime; const UTCBias: Integer; const TicketInCachePacked: string; const ComputerInfo: ComputerInformation; const SentBytes: Integer; const AdditionalInfo: NVPairList; const DamageInCachePacked: string; const WorkOrderInCachePacked: string): string;

    procedure SaveNewAttachments(const SecSession: string; const NewAttachments: NewAttachmentList; out GeneratedKeys: GeneratedKeyList);
    procedure SaveAttachmentUploads(const SecSession: string; const AttachmentUploads: AttachmentUploadList);
    procedure SaveAttachmentUpdates(const SecSession: string; const AttachmentUpdates: AttachmentUpdateList);
    function GetAttachmentList(const SecSession: string; const ForeignType: Integer; const ForeignID: Integer; const ImagesOnly: Boolean): string;
    function GetPrintableAttachmentList(const SecSession: string; const ForeignType: Integer; const ForeignID: Integer): string;
    function SaveUploadQueueStats(const SecSession: string; const Statistics: PendingUploadsStats): Boolean;
    procedure AckMessage2(const SecSession: string; const MessageAcks: NVPairListList; out ReturnValue: string);
    function SaveMessage(const SecSession: string; const MessageData: MessageChange): string;
    function MessageSearch(const SecSession: string; const Params: NVPairList): string;
    function GetMessageRecip(const SecSession: string; const MessageID: Integer): string;
    procedure SetMessageActive(const SecSession: string; const MessageID: Integer; const IsActive: Boolean);
    procedure UpdateRestrictedUseExemption(const SecSession: string; const Exemptions: NVPairListList);
    function SaveAddinInfo2(const SecSession: string; const AddinInfoChanges: AddinInfoChangeList): GeneratedKeyList;
    procedure SaveAreas2(const SecSession: string; const AreaChanges: AreaChangeList);
    function SaveNewNotes2(const SecSession: string; const NotesChanges: NotesChangeList): GeneratedKeyList;
    function SaveNewNotes5(const SecSession: string; const NotesChanges: NotesChangeList5): GeneratedKeyList;
    function SaveEmployeeActivity2(const SecSession: string; const EmpActivityChanges: EmployeeActivityChangeList): GeneratedKeyList;
    function SaveEmployeeActivity3(const SecSession: string; const EmpAcitivtyChanges3: EmployeeActivityChangeList3): GeneratedKeyList;
    function SaveJobsiteArrivals2(const SecSession: string; const JobsiteArrivalChanges: JobsiteArrivalChangeList): GeneratedKeyList;

    // First Task
    procedure UpdateFirstTaskReminder(const SecSession: string; const EmpID: integer; const ReminderTime: string); //QM-494 First Task Reminder EB
    function GetFirstTaskReminder(const SecSession: string; const EmpID: integer): string;  //QM-494 First Task Reminder EB

    // Work Management
    function GetBreadCrumb(const SecSession: string; const empID : integer):string;
    function GetBreadCrumbList(const SecSession: string; const MgrID: integer): string;
    function GetHierarchy2(const SecSession: string; const IncludePeers: Boolean; const IncludeInactive: Boolean): string;
    function MultiOpenTotals6(const SecSession: string; const Managers: string): string;
    function MultiOpenTotals6PD(const SecSession: string; const Managers: string): string;  //QM-133 Past Due EB
    function MultiOpenTotals6OP(const SecSession: string; const Managers: string): string;  //QM-428 Open EB
    function MultiOpenTotals6TD(const SecSession: string; const Managers: string): string;  //QM-318 Today EB
    function MultiOpenTotals6V(const SecSession: string; const Managers: string; const VirtualBucketMgrID: integer): string; //QM-486 Virtual Bucket EB
    procedure SaveEmployeeTicketViewLimit(const SecSession: string; const EmployeeID, TicketViewLimit: Integer);
    procedure SaveEmployeeShowFutureWork(const SecSession: string; const EmpID: Integer; const ShowFutureTickets: Boolean);
    function GetTicketActivities(const SecSession: string; const EmpID: Integer; const NumDays: Integer): string;
    function GetPastDueList(const SecSession: string; const ManagerID: Integer;  const MaxDays: Integer): string;
    function GetTodayList(const SecSession: string; const ManagerID: Integer; const MaxDays: Integer): string;  //QM-318 EB
    function GetOpenTicketExpList(const SecSession: string; const ManagerID: Integer; const RowLimit: integer; const MaxDays: Integer): string; //QM-695 EB
    function GetOpenTicketSumList(const SecSession: string; const ManagerID: Integer; const MaxDays: Integer): string;
    function GetVirtualBucket(const SecSession: string; const ManagerID: Integer; const BucketName: string): string; //QM-486 Virtual Bucket EB


    {MARS - Bulk Status Change QM-671 EB}
    function GetMARSEmpList(const SecSession:string; const MgrID: integer): string;
    function AddMARSRequest(const SecSession:string; const LocatorID: integer; const NewStatus: string; const InsertedBy: integer): boolean;
    function RemoveMARSRequest(const SecSession: string; const MARSID: integer): boolean;

    {Route Order Optimization QM-702 EB}
    function AddROOptimizationRequest(const SecSession: string; const LocatorID: integer; Const InsertedBy: integer; const NextTicketID: integer): boolean;  //QM-702 RO Optimization EB

    {TODO: Note from Kyle regarding AckTicketActivities:
 I don't think we need to pass a list, we can simply issue a command for each one that we wish to ack. This is that notion of a single
 compound command mechanism replacing the need for each individual command to implement its own way of affecting many entities.}

    //Activites
    {TODO: Note from Kyle regarding AckTicketActivities:
     I don't think we need to pass a list, we can simply issue a command for each one that we wish to ack. This is that notion of a single
     compound command mechanism replacing the need for each individual command to implement its own way of affecting many entities.}
    function AckTicketActivities(const SecSession: string; const ActivityAckIDArray: IntegerList): Integer;
    function AckAllTicketActivitiesForManager(const SecSession: string; const Params: NVPairListList): Integer;
    function GetUnackedTicketActivityCount(const SecSession: string; const Params: NVPairList): Integer;
    procedure AckTicket(const SecSession: string; const TicketID: TicketKey; const AckedByEmp: Integer);
    procedure AckDamageTicket(const SecSession: string; const TicketID: TicketKey; const AckedByEmp: Integer; const InvestigatorID: Integer);
    function HasActivityToday(const SecSession: string; const Activity: string; const EmpId: Integer): boolean;
    function GetTicketsForLocator(const SecSession: string; const LocatorID: Integer; const NumDays: Integer; const OpenOnly: Boolean; const Unacknowledged: Boolean): string;
    function GetTicketsForLocatorRO(const SecSession: string; const LocatorID: Integer; const NumDays: Integer; const OpenOnly: Boolean; const Unacknowledged: Boolean): string;  //QMANTWO-775 Route Order Override
    function GetWorkOrdersForEmployee(const SecSession: string; const EmployeeID: Integer; const NumDays: Integer; const OpenOnly: Boolean; const Unacknowledged: Boolean): string;
    function GetDamagesForLocator(const SecSession: string; const LocatorID: Integer; const NumDays: Integer; const OpenOnly: Boolean): string;
    function GetEmpPermission(const SecSession: string; const EmpID: Integer; const PermissionStr: string): string; //QM-10 Returns limitation if Emp has rights or 'Revoked' if not

    // Assets
    function GetAssets(const SecSession: string; const EmpID: Integer): string;
    procedure SaveEmployeeAssets2(const SecSession: string; const EmpID: Integer; const Assets: NVPairListList);
    procedure ReassignAssets(const SecSession: string; const Assets: NVPairListList);
    procedure UpdatePhoneNo(const SecSession: string; const EmpID:integer;const NewPhoneNo:string);

    // Tickets
    function TicketSearch6(const SecSession: string; const TicketNumber: string;
            const Street: string; const City: string; const State: string;
            const County: string; const WorkType: string; const TicketType: string;
            const DoneFor: string; const Company: string; const DueDateFrom: DateTime;
            const DueDateTo: DateTime; const RecvDateFrom: DateTime; const RecvDateTo: DateTime;
            const CallCenter: string; const TermCode: string; const Attachments: Integer;
            const ManualTickets: Integer; const Priority: string; const UtilityCo: Integer): string;//QMANTWO-810 sr
    function DebugTicketIDSearch(const SecSession: string; const TicketID: integer): string;  //QM-305 EB
    function DebugLocateIDSearch(const SecSession: string; const LocateID: integer): string;  //QM-305 EB
    function CompletionReport(const SecSession: string; const DateFrom: DateTime; const DateTo: DateTime; const Office: Integer; const Manager: Integer; const SortBy: Integer): string;
{TODO: Kyle regarding TicketInfoChangeList:   EB - This is going to take a little more work than
expected. But it needs to be done...
A bunch of these list types should go away, they are replaced by our new compound command mechanism.}
    procedure StatusTicketsLocates14(const SecSession: string; const TicketChanges: TicketChangeList14; out GeneratedKeys: GeneratedKeyList);
    procedure StatusTicketsLocates15(const SecSession: string; const TicketChanges: TicketChangeList15; out GeneratedKeys: GeneratedKeyList);
    function SaveNewTickets(const SecSession: string; const Tickets: NewTicketList): GeneratedKeyList;
    function GetTicketID(const SecSession: string; const TicketNumber: string; const CallCenter: string; const TransmitDateFrom, TransmitDateTo: DateTime; const DamageId: Integer): Integer;
    function GetTicket2(const SecSession: string; const TicketID: Integer; const UpdatesSince: TDateTime): string;
    function GetTicketHistory(const SecSession: string; const TicketID: Integer): string;
    procedure CreateFollowupTicket(const SecSession: String; const TicketID: Integer; const FollowupTypeID: Integer; const FollowupDueDate: DateTime; const FollowupTransmitDate: DateTime);
    procedure SaveTicketPriority(const SecSession: string; const TicketID: Integer; const EntryDate: TDateTime; const Priority: Integer);
    procedure MoveLocates(const SecSession: string; const Locates: LocateList; const NewLocatorID: Integer);
    procedure AssignProjLocate(const SecSession: string; const LocateID: integer; const AssignedToID: Integer); //QMANTWO-616
    procedure MoveTickets(const SecSession: string; const Tickets: TicketList; const FromLocatorID: Integer; const ToLocatorID: Integer);
    function SaveTaskSchedule2(const SecSession: string; const TaskScheduleChanges: TaskScheduleChangeList): GeneratedKeyList;
    function GetEPRHistory(const SecSession: string; const TicketID: Integer): string;
    function GetTicketLocateList(const SecSession: string; const TicketID : integer):string; //QMANTWO-422 EB
    procedure SaveOtherTicketInfoChanges(const SecSession: string; const TicketInfoChanges: TicketInfoChangeList; out GeneratedKeys: GeneratedKeyList); //QMANTWO-775
    
    procedure SaveTicketDueDate(const SecSession: string; const TicketID: Integer; const NewDueDate: TDateTime); //QM-216 EB Set Ticket Due Date
    function  BulkChangeTicketDueDates(const SecSession: string; const TicketIDs: string; const NewDueDate: TDateTime): string; //QM-959 EB Bulk Change
    procedure SaveTicketTypeHP(const SecSession: String; const TicketID: Integer; const NewTicketType: String); //qm-373 SR
    function GetRiskHistory(const SecSession: string; const TicketID: Integer): string;  //QM-387 EB
    procedure SaveTicketType(const SecSession: string; const TicketID: Integer; const TicketType: string);  //QM-604 EB Nipsco

    //Ticket Qualifications / Employee Plus
    function GetOQDataForEmployee(const SecSession: string; const EmpID: Integer): string;  //QM-444 OQ
    function GetOQDataForManager(const SecSession: string; const MgrID: Integer; const IncludeRecentExp: Integer): string;  //QM-444 OQ Part 2 EB
    function GetPlusForManager(const SecSession: string; const MgrID: Integer): string;  //QM-585 EB PLUS Whitelist
    procedure AddEmpToPlusWhiteList(const SecSession: string; const LocatorID: Integer; const PlusID: Integer; const ModifiedByID: Integer; const Util: string); //QM-585 EB PLUS Whitelist
    procedure RemoveEmpFromPlusWhitelist(const SecSession: string; const LocatorID: Integer; const PlusID: Integer; const ModifiedByID: Integer); //QM-585 EB PLUS Whitelist
    function GetEmployeePlusListByEmp(const SecSession: string; const EmpID: Integer): string; //QM-585 EB PLUS Whitelist
    function GetWMEmpPlusUtilByEmp(const SecSession: string; const EmpID: Integer): string;  //QM-585 EB PLUS Whitelist
    function GetEmpPlusMgrList(const SecSession: string; const MgrID: Integer): string; //QM-585 EB PLUS Whitelist

    // Work Orders
    function WorkOrderSearch(const SecSession: string; const TicketNumber: string; const WorkOrderNumber: string; const Street: string; const City: string; const State: string; const County: string; const WOSource: string; const ClientName: string; const Company: string; const DueDateFrom: DateTime; const DueDateTo: DateTime; const TransmitDateFrom: DateTime; const TransmitDateTo: DateTime; const Kind: string; const Tickets: Integer):string;
    function GetWorkOrder(const SecSession: string; const WorkOrderID: Integer): string;
    function GetWorkOrderHistory(const SecSession: string; const WorkOrderID: Integer): string;
    procedure StatusWorkOrders2(const SecSession: string; const WorkOrderChanges: WorkOrderChangeList2; out GeneratedKeys: GeneratedKeyList);
    procedure StatusWorkOrders5(const SecSession: string; const WorkOrderChanges: WorkOrderChangeList5; out GeneratedKeys: GeneratedKeyList);
    procedure StatusWorkOrderInspections(const SecSession: string; const WOInspectionChanges: WOInspectionChangeList; out GeneratedKeys: GeneratedKeyList);
    procedure MoveWorkOrders(const SecSession: string; const WorkOrders: IntegerList; const FromWorkerID: Integer; const ToWorkerID: Integer);
    function GetWorkOrderOHM(const SecSession: string; const WorkOrderID: Integer): string;
    procedure SaveWorkOrdersOHM(const SecSession: string; const WorkOrderOHMChanges: WorkOrderOHMChangeList);

    // Damages
    function DamageSearch9(const SecSession: string; const TicketNumber: string; const UQDamageID: Integer; const DamageDateFrom: DateTime; const DamageDateTo: DateTime; const DueDateFrom: DateTime; const DueDateTo: DateTime; const Investigator: string; const Excavator: string; const Location: string; const City: string; const State: string; const ProfitCenter: string; const DamageType: string; const UtilityCoDamaged: string; const ClientClaimID: string; const WorkToBeDone: string; const IncludeUQResp: Boolean; const IncludeExcvResp: Boolean; const IncludeUtilityResp: Boolean; const InvestigatorID: Integer; const Attachments: Integer; const Invoices: Integer; const ClaimStatus: string; const IncludeEstimates: Boolean; const InvoiceCode: string; const ExcavationType: string; const Locator: string): string;
    function DamageInvoiceSearch3(const SecSession: string; const InvoiceNumber: string; const Company: string; const City: string; const State: string; const UQDamageID: Integer; const Amount: Currency; const Comment: string; const RecvDateFrom: DateTime; const RecvDateTo: DateTime; const InvoiceDateFrom: DateTime; const InvoiceDateTo: DateTime; const InvoiceCode: string): string;
    function DamageLitigationSearch3(const SecSession: string; const UQDamageID, LitigationID: Integer; const FileNumber, Plaintiff: string; const CarrierID: Integer; const Attorney, Party, Demand, Accrual, Settlement: string; const ClosedDateFrom, ClosedDateTo: DateTime): string;
    function DamageThirdPartySearch(const SecSession: string; const UQDamageID, ThirdPartyID, CarrierID: Integer; const Claimant, Address, City, State, ClaimStatus, ProfitCenter, UtilityCoDamaged: string; const NotifiedDateFrom, NotifiedDateTo: DateTime): string;
    function GetDamage2(const SecSession: string; const DamageID: Integer): string;
    function GetDamageInvoice(const SecSession: string; const InvoiceID: Integer): string;
    function GetDamageLitigation(const SecSession: string; const LitigationID: Integer): string;
    function GetDamageThirdParty(const SecSession: string; const ThirdPartyID: Integer): string;
    function SaveDamageBill(const SecSession: string; const Params: NVPairListList): GeneratedKeyList;
    function SaveDamage3(const SecSession: string; const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
    function ApproveDamage(const SecSession: String; const DamageID: Integer; const ApprovedDateTime: DateTime): String;
    function SaveDamageThirdParty(const SecSession: string; const Params: NVPairListList): GeneratedKeyList;
    function SaveDamageEstimate2(const SecSession: string; const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
    function SaveDamageInvoice(const SecSession: string; const Params: NVPairListList): GeneratedKeyList;
    function SaveDamageLitigation(const SecSession: string; const Params: NVPairListList): GeneratedKeyList;
    function SaveRequiredDocuments(const SecSession: AnsiString; const RequiredDocs: RequiredDocumentList): GeneratedKeyList;
    procedure SaveDamagePriority(const SecSession: string; const DamageID: Integer; const EntryDate: TDateTime; const Priority: Integer);
    function GetDamageIDForUQDamageID(const SecSession: string; const UQDamageID: Integer): Integer;
    procedure MoveDamages(const SecSession: string; const Damages: IntegerList; const FromInvestigatorID: Integer; const ToInvestigatorID: Integer);

    // Timesheets
    function SearchTimesheetsForProfitCenter(const SecSession: string; const SearchType: Integer; const DateStart: DateTime; const DateStop: DateTime; const StartEmpId: Integer; const LevelLimit: Integer; const ProfitCenter: string): string;
    function ApproveTimesheets(const SecSession: string; const Final: Boolean; const LocalTime: DateTime; const Entries: TimesheetRecordList): string;
    function PostTimesheets(const SecSession: string; const Timesheets: TimesheetEntryList): GeneratedKeyList;
    function ExportTimesheets(const SecSession: string; const ProfitCenter: string; const WeekEnding: TDateTime): string;
    function SaveBreakRuleResponses2(const SecSession: string; const BreakRuleResponses: BreakRuleResponseList): GeneratedKeyList;
    function GetEmployeeFloatingHolidays(const SecSession: String; const EmployeeID: Integer; const Year: Integer; const WeekStart: DateTime; const WeekEnd: DateTime): Integer;
    function GetSickleavePTOBalances(const SecSession: String; const Emp_ID: Integer): string;  //QM-579 EB

    // Billing
    function BillingAdjustmentSearch(const SecSession: string; const Params: NVPairList): string;
    function GetBillingAdjustment(const SecSession: string; const AdjustmentID: Integer): string;
    function SaveBillingAdjustments(const SecSession: string; const BillingAdjustments: NVPairListList): GeneratedKeyList;
    procedure StartBilling(const SecSession: string; const Params: NVPairList);
    function BillingRunSearch(const SecSession: string; const Params: NVPairList): string;
    function BillingHeaderSearch(const SecSession: string; const Params: NVPairList): string;
    function BillingAction(const SecSession: string; const Params: NVPairList): string;
    function GetBillingRunLog(const SecSession: string; const Params: NVPairList): string;
    procedure CancelBillingRun(const SecSession: string; const Params: NVPairList);

    //Plats
    procedure UpdateEmployeePlatsUpdated(const SecSession: string; const EmpID: Integer);
    procedure UpdateEmployeePlatsFromHistory(const SecSession: string; const EmpID: Integer; const UpdateDate: TDateTime);

    //Custom Form Data
    function SaveNewCustomFormData(const SecSession: string; const CustomFormAnswers: CustomFormAnswerList): GeneratedKeyList;  //QM-593 Custom Forms (AT&T Damage Form)
    procedure SaveModCustomFormData(const SecSession: string; const CustomFormAnswers: CustomFormAnswerList);

    //Ticket Alerts
    function GetTicketAlerts(const SecSession: string; const TicketID: integer): string;
  public
    destructor Destroy; override;
  end;

implementation

uses
  {Generated:} QMServerLibrary_Invk, uROTypes, OdSqlBuilder, OdDataSetToXml,
  Variants, OperationTracking, OdIsoDates, QMConst, DateUtils,
  OdSqlXmlOutput, StrUtils, JclStrings, PasswordRules, 
  OdExceptions, ServerAttachmentDMu, OdUtcDates;

type
  ELoginException = class(Exception);

procedure Create_QMServices(out anInstance : IUnknown);
begin
  anInstance := TQMService.Create;
end;

procedure TQMService.AckDamageTicket(const SecSession: string;
  const TicketID: TicketKey; const AckedByEmp, InvestigatorID: Integer);
begin
  {$I before.inc}
  SetName('AckDamageTicket');
  GetManageWorkDM;
  ManageWorkDM.AckDamageTicket(TicketID.TicketID, AckedByEmp, InvestigatorID);
  {$I after.inc}
end;

procedure TQMService.AckTicket(const SecSession: string;
  const TicketID: TicketKey; const AckedByEmp: Integer);
begin
  {$I before.inc}
  SetName('AckTicket');
  GetManageWorkDM;
  ManageWorkDM.AckTicket(TicketID, AckedByEmp);
  {$I after.inc}
end;

function TQMService.ApproveTimesheets(const SecSession: string; const Final: Boolean;
  const LocalTime: DateTime; const Entries: TimesheetRecordList): string;
begin
  {$I before.inc}
  SetName('ApproveTimesheets');
  GetTimesheetDM;
  Result := TimesheetDM.ApproveTimesheets(Final, LocalTime, Entries);
  {$I after.inc}
end;

procedure TQMService.ChangePassword(const SecSession: string;
  const UserID: Integer; const NewPassword: string);
var
  SQL: string;
  NewHash, NewAPIHash: string;
  Problem: string;
  DataSet: TDataSet;
begin
  FAllowExpiredLogin := True;
  {$I before.inc}
  SetName('ChangePassword');

  try
    if UserID=0 then
      raise Exception.Create('User ID must be supplied');

    // Verify the new password meets the minimum requirements
    DataSet := DM.CreateDataSetForSQL('select first_name, last_name, login_id from users where (uid = ' + IntToStr(UserID) + ')');
    try
      if (DataSet.Eof) then
        raise Exception.Create('User ID not found');
      if (DataSet.RecordCount > 1) then
        raise Exception.Create('Internal Error - User ID returned multiple records');
      // TODO -oJBH -cPasswordRules : Uncomment below to use enhanced password complexity rules (if approved by UQ)
      //if not PasswordMeetsRequirements(DataSet.FieldByName('login_id').AsString, '', NewPassword, Problem, DataSet.FieldByName('first_name').AsString, DataSet.FieldByName('last_name').AsString) then
      if not PasswordMeetsRequirements(DataSet.FieldByName('login_id').AsString,'', NewPassword, Problem) then
        raise Exception.Create(Problem);

      NewHash := GeneratePasswordHash(DataSet.FieldByName('login_id').AsString, NewPassword);
      NewAPIHash := GeneratePasswordHash(DataSet.FieldByName('login_id').AsString, NewHash);
    finally
      FreeAndNil(DataSet);
    end;

    // Verify the new password is not in password history (prevent reuse)
    DataSet := DM.CreateDataSetForSQL('select when_changed from user_password_history where (uid = ' + IntToStr(UserID) + ') and (password_hash = ' + QuotedStr(NewHash) + ')');
    try
      if (not DataSet.Eof) then
        raise Exception.Create('Passwords can not be reused, please select a new password');
    finally
      FreeAndNil(DataSet);
    end;

    // Change the user's password
    with DM.ChangePassword do begin
      Parameters.ParamValues['@UserID'] := UserID;
      Parameters.ParamValues['@NewPassword'] := NewHash;
      ExecProc;
    end;
    // Reset user's API key as well
    DM.ResetAPIKey.Parameters.ParamValues['@UserID'] := UserId;
    DM.ResetAPIKey.Parameters.ParamValues['@NewKey'] := NewAPIHash;
    DM.ResetAPIKey.ExecProc;
  except
    on E: Exception do
      raise Exception.Create(E.Message + '  The password was not changed.');
  end;

  // Record password change in the user_password_history table
  SQL := 'insert user_password_history (uid, password_hash, changed_by_uid) values (' + IntToStr(UserID) + ',' + QuotedStr(NewHash) + ',' + IntToStr(LoggedInUID) + ')';
  DM.ExecuteSQL(SQL);

  {$I after.inc}
end;

procedure TQMService.CheckSecurity(const SecSession: string);
var
  p: Integer;
  UserName, Remaining, PW, Version: string;
begin
  if SecSession = '' then
    raise ELoginException.Create('Security information missing');

  p := Pos('/', SecSession);
  if p=0 then
    raise Exception.Create('Invalid security information');

  UserName := Copy(SecSession, 1, p-1);
  Remaining := Copy(SecSession, p+1, 100);

  p := Pos('/', Remaining);
  if p = 0 then begin
    PW := Remaining;
    Version := '';  // not specified
  end else begin
    PW := Copy(Remaining, 1, p-1);
    Version := Copy(Remaining, p+1, 100);
  end;

  if (UserName = '') or (PW = '') then
    raise Exception.Create('Invalid security information');

  CheckUserSecurity(UserName, PW, Version, FAllowExpiredLogin);
end;

procedure TQMService.CheckUserSecurity(const UserName, Password,
  Version: string; SkipExpiredCheck: Boolean);
  {This procedure is called every time Before.Inc is called!!!}
const
  Expired = 'The password for login %s has expired.  You must change your ' +
    'password in the Options screen before you can continue.';
var
  LoginFailed: Boolean;
//  RecordsAffected: integer;
begin
  with DM do begin
    LoggedInClientVersion := Version;
    Assert(not CheckLogin.Active);
    // check for client sending hash to encrypted db:
    CheckLogin.Parameters.ParamValues['@UserName'] := UserName;
    CheckLogin.Parameters.ParamValues['@PW'] := Password;
    SetPhase('Checking security - Loading Login Data');
    CheckLogin.Open;
    try

      LoginFailed := CheckLogin.EOF;
      // TODO -cAllowOldPasswords : Remove the block below once passwords encrypted in the database
      // check for client sending hash to unencrypted db:
      if LoginFailed and PasswordIsHashed(Password) then begin
        CheckLogin.Close;
        CheckLogin.Parameters.ParamValues['@UserName'] := UserName;
        CheckLogin.Parameters.ParamValues['@PW'] := Null;
        SetPhase('Checking security - Loading Login Data (interim compatibility)');
        CheckLogin.Open;
        LoginFailed := CheckLogin.EOF;
        if not LoginFailed then
          LoginFailed := (GeneratePasswordHash(UserName, CheckLogin.FieldByName('password').AsString) <> Password);
      end;

      // TODO -oJBH -cAllowOldPasswords : Remove the below to disable receiving clear text passwords from older QManager versions
      // check for clear text in encrypted db:
      if LoginFailed and (not PasswordIsHashed(Password)) then begin
        CheckLogin.Close;
        CheckLogin.Parameters.ParamValues['@UserName'] := UserName;
        CheckLogin.Parameters.ParamValues['@PW'] := GeneratePasswordHash(UserName, Password);
        SetPhase('Checking security - Loading Login Data (backwards compatibility)');
        CheckLogin.Open;
        LoginFailed := CheckLogin.EOF;
      end;

      // Verify user name & password were valid - CheckLogin should contain 1 row if valid
      if LoginFailed then
        raise ELoginException.CreateFmt('Login failure, invalid login/password for %s', [UserName]);
      if CheckLogin.FieldByName('emp_id').AsInteger < 1 then
        raise ELoginException.CreateFmt('Login failure, login %s has no employee record', [UserName]);

      // The login/pw matched a valid user, load their data
      DM.LoggedInEmpID := CheckLogin.FieldByName('emp_id').AsInteger;
      DM.LoggedInEmpShortName := CheckLogin.FieldByName('short_name').AsString;
      LoggedInEmpNumber := CheckLogin.FieldByName('emp_number').AsString;
      Op.EmpID := LoggedInEmpId;
      DM.LoggedInUID := CheckLogin.FieldByName('uid').AsInteger;
      LoggedInUserType := CheckLogin.FieldByName('typecode').AsString;
      LoggedInChgPwd := CheckLogin.FieldByName('chg_pwd').AsBoolean;
      LoggedInChgPwdDate := CheckLogin.FieldByName('chg_pwd_date').AsDateTime;
      LoggedInReportTo := CheckLogin.FieldByName('report_to').AsInteger;
      DM.ClientVersion.dwFileVersionMS := 0;
      DM.ClientVersion.dwFileVersionLS := 0;
      try
        DM.ClientVersion := ParseVersionString(DM.LoggedInClientVersion);
      except
        // Treat unparseable version string as 0.0.0.0
      end;

      // The expiration check gets skipped when this call is used to validate
      // a user who is in the process of changing their expired password, so
      // that they don't have to call support in that situation.
      if not SkipExpiredCheck then begin
        if LoggedInChgPwd or ((LoggedInChgPwdDate > 1) and (Now > LoggedInChgPwdDate)) then begin
          raise Exception.CreateFmt(Expired, [UserName]);
        end;
      end;

      // Check for API key - create one if it doesn't exist
      if IsEmpty(CheckLogin.FieldByName('api_key').AsString) then begin
        DM.LoggedInEmpAPIKey := GeneratePasswordHash(UserName, Password);
        DM.ResetAPIKey.Parameters.ParamValues['@UserID'] := DM.LoggedInUID;
        DM.ResetAPIKey.Parameters.ParamValues['@NewKey'] := DM.LoggedInEmpAPIKey;
        DM.ResetAPIKey.ExecProc;
      end else
        DM.LoggedInEmpAPIKey := CheckLogin.FieldByName('api_key').AsString;

      // If there is more than one match, it is an error
      SetPhase('Checking security - verifying only one login');
      CheckLogin.Next;
      if not CheckLogin.Eof then
        raise ELoginException.CreateFmt('Login failure, ambiguous login/password for %s', [UserName])
        {EB - Do not uncomment this code - it causes a deadlock with the user trigger, leaving comments in for reference}
 //     else
//      begin
//        DM.updUserLastLogin.Parameters.ParamByName('empid').Value :=   LoggedInEmpID;
//        DM.updUserLastLogin.Parameters.ParamByName('lastLogin').Value :=  Now;
//        DM.updUserLastLogin.Execute(RecordsAffected, EmptyParam);
//        if RecordsAffected <> 1 then raise ELoginException.CreateFmt('Cannot record Last login for %s', [UserName]);
//      end;
    finally
      CheckLogin.Close;
    end;
  end;
end;

function TQMService.CompletionReport(const SecSession: string;
  const DateFrom: DateTime; const DateTo: DateTime; const Office: Integer;
  const Manager: Integer; const SortBy: Integer): string;
begin
  {$I before.inc}
  SetName('CompletionReport');
  GetTicketDM;
  Result := TicketDM.CompletionReport(DateFrom, DateTo, Office, Manager, SortBy);
  {$I after.inc}
end;

//DamageInvoiceSearch3 introduced in build 6461
function TQMService.DamageInvoiceSearch3(const SecSession, InvoiceNumber,
  Company, City, State: string; const UQDamageID: Integer;
  const Amount: Currency; const Comment: string; const RecvDateFrom,
  RecvDateTo, InvoiceDateFrom, InvoiceDateTo: DateTime;
  const InvoiceCode: string): string;
begin
  {$I before.inc}
  SetName('DamageInvoiceSearch3');
  GetDamageDM;
  Result := DamageDM.DamageInvoiceSearch3(InvoiceNumber, Company, City, State,
    UQDamageID, Amount, Comment, RecvDateFrom, RecvDateTo, InvoiceDateFrom,
    InvoiceDateTo, InvoiceCode);
  {$I after.inc}
end;

//DamageLitigationSearch3 introduced in build 6461
function TQMService.DamageLitigationSearch3(const SecSession: string;
  const UQDamageID: Integer; const LitigationID: Integer; const FileNumber, Plaintiff: string;
  const CarrierID: Integer; const Attorney, Party, Demand, Accrual,
  Settlement: string; const ClosedDateFrom,
  ClosedDateTo: DateTime): string;
begin
  {$I before.inc}
  SetName('DamageLitigationSearch3');
  GetDamageDM;
  Result := DamageDM.DamageLitigationSearch3(UQDamageID, LitigationID, FileNumber,
    Plaintiff, CarrierID, Attorney, Party, Demand, Accrual, Settlement,
    ClosedDateFrom, ClosedDateTo);
  {$I after.inc}
end;

//DamageSearch9 introduced in build 6994
function TQMService.DamageSearch9(const SecSession, TicketNumber: string;
  const UQDamageID: Integer; const DamageDateFrom, DamageDateTo,
  DueDateFrom, DueDateTo: DateTime; const Investigator, Excavator,
  Location, City, State, ProfitCenter, DamageType, UtilityCoDamaged,
  ClientClaimID, WorkToBeDone: string; const IncludeUQResp,
  IncludeExcvResp, IncludeUtilityResp: Boolean; const InvestigatorID,
  Attachments, Invoices: Integer; const ClaimStatus: string;
  const IncludeEstimates: Boolean; const InvoiceCode,
  ExcavationType: string; const Locator: string): string;
begin
  {$I before.inc}
  SetName('DamageSearch9');
  GetDamageDM;
  Result := DamageDM.DamageSearch9(TicketNumber, UQDamageID, DamageDateFrom, DamageDateTo,
    DueDateFrom, DueDateTo, Investigator, Excavator, Location, City, State,
    ProfitCenter, DamageType, UtilityCoDamaged, ClientClaimID, WorkToBeDone,
    IncludeUQResp, IncludeExcvResp, IncludeUtilityResp, InvestigatorID,
    Attachments, Invoices, ClaimStatus, IncludeEstimates, InvoiceCode,
    ExcavationType, Locator);
  {$I after.inc}
end;



destructor TQMService.Destroy;
begin
  FreeAndNil(BillingDM);
  FreeAndNil(TimesheetDM);
  FreeAndNil(WorkOrderDM);
  FreeAndNil(TicketDM);
  FreeAndNil(DamageDM);
  FreeAndNil(ArrivalDM);
  FreeAndNil(AssetDM);
  FreeAndNil(ManageWorkDM);
  FreeAndNil(AddinInfoDM);
  FreeAndNil(NotesDM);
  FreeAndNil(DM);

  inherited;
end;

procedure TQMService.GetDM;
begin
  if DM = nil then
    DM := TLogicDM.Create(nil);
end;

procedure TQMService.GetBillingDM;
begin
  if BillingDM = nil then
    BillingDM := TServerBillingDM.Create(nil, DM);
end;

procedure TQMService.GetDamageDM;
begin
  if DamageDM = nil then
    DamageDM := TDamageDM.Create(nil, DM);
end;

procedure TQMService.GetTicketDM;
begin
  if TicketDM = nil then
    TicketDM := TTicketDM.Create(nil, DM);
end;

procedure TQMService.GetArrivalDM;
begin
  if ArrivalDM = nil then
    ArrivalDM := TArrivalDM.Create(nil, DM);
end;

procedure TQMService.GetTimesheetDM;
begin
  if TimesheetDM = nil then
    TimesheetDM := TTimesheetDM.Create(nil, DM);
end;

procedure TQMService.GetWorkOrderDM;
begin
  if WorkOrderDM = nil then
    WorkOrderDM := TWorkOrderDM.Create(nil, DM);
end;

procedure TQMService.GetAssetDM;
begin
  if AssetDM = nil then
    AssetDM := TAssetDM.Create(nil, DM);
end;

procedure TQMService.GetManageWorkDM;
begin
  if ManageWorkDM = nil then
    ManageWorkDM := TManageWorkDM.Create(nil, DM);
end;



procedure TQMService.GetAddinInfoDM;
begin
  if AddinInfoDM = nil then
    AddinInfoDM := TAddinInfoDM.Create(nil, DM);
end;

procedure TQMService.GetNotesDM;
begin
  if NotesDM = nil then
    NotesDM := TNotesDM.Create(nil, DM);
end;



function TQMService.GetHierarchy2(const SecSession: string;
  const IncludePeers: Boolean; const IncludeInactive: Boolean): string;
begin
  {$I before.inc}
  SetName('GetHierarchy2');
  GetManageWorkDM;
  Result := ManageWorkDM.GetHierarchy2(IncludePeers, IncludeInactive);
  {$I after.inc}
end;

function TQMService.GetTableData(const SecSession, TableName: string;
  const UpdatesSince: DateTime): string;
const
  AreaSQL = 'select m.map_id, m.map_name, m.state, m.county, m.ticket_code, a.area_id, a.area_name, a.locator_id ' +
    'from map m inner join area a on m.map_id = a.map_id ' +
    'inner join get_hier(%d, 0) on a.locator_id = h_emp_id';
var
  Limitation: string;
  RootEmp: Integer;
begin
  {$I before.inc}
  SetName('GetTableData');

  DM.Op.Detail := Format('Table %s, Date %s',
    [TableName, IsoDateTimeToStr(UpdatesSince)]);

  if SameText(TableName, TableEditableAreas) then begin
    if not CanIDef(RightRoutingEditAreas, Limitation, IntToStr(LoggedInEmpID)) then
      raise EOdNotAllowed.Create('You are not allowed to edit map areas');
    RootEmp := StrToIntDef(Limitation, LoggedInEmpID);
    DM.GetTableData.CommandText := Format(AreaSQL, [RootEmp]);
  end
  else if TableName='area' then
    DM.GetTableData.CommandText := 'select * from area where map_id = (select value from configuration_data where name = ''AssignableAreaMapID'')'
  else begin
    DM.GetTableData.CommandText := Format('select * from %s where modified_date >= ''%s''',
      [TableName, IsoDateTimeToStr(UpdatesSince)]);
  end;

  DM.GetTableData.Open;
  try
    Assert(Assigned(DM.GetTableData), 'DM.GetTableData must be assigned');
    Result := ConvertDataSetsToSQLXML([DM.GetTableData], [TableName]);
  finally
    DM.GetTableData.Close;
  end;
  {$I after.inc}
end;

procedure TQMService.MoveLocates(const SecSession: string;
    const Locates: LocateList; const NewLocatorID: Integer);
begin
  {$I before.inc}
  SetName('MoveLocates');
  GetTicketDM;
  TicketDM.MoveLocates(Locates, NewLocatorID);
  {$I after.inc}
end;

procedure TQMService.AssignProjLocate(const SecSession: string; const LocateID: integer; const AssignedToID: Integer);
  //QMANTWO-616 EB Project Ticket: Locate Assignment
begin
    {$I before.inc}
  SetName('AssignProjLocates');
  GetTicketDM;
  TicketDM.AssignProjLocate(LocateID, AssignedToID);
  {$I after.inc}
end;


// Originally Deprecated 08/19/13 (replaced with new QMAPI REST op)
//QMANTWO-573  EB - Reinstated!!!!
procedure TQMService.MoveTickets(const SecSession: string;
  const Tickets: TicketList; const FromLocatorID, ToLocatorID: Integer);
begin
  {$I before.inc}
  SetName('MoveTickets');
  Assert(Assigned(Tickets), 'MoveTickets requires ticket list');
  GetTicketDM;
  TicketDM.MoveTickets(Tickets, FromLocatorID, ToLocatorID);
  {$I after.inc}
end;

function TQMService.MultiOpenTotals6(const SecSession: string; const Managers: string): string;
begin
  Result := '';
  {$I before.inc}
  SetName('MultiOpenTotals6');
  GetManageWorkDM;
  Result := ManageWorkDM.MultiOpenTotals6(Managers);
  {$I after.inc}
end;

function TQMService.MultiOpenTotals6OP(const SecSession,
  Managers: string): string;
begin
  Result := '';
  {$I before.inc}
  SetName('MultiOpenTotals6OP');   //QM-428 Open
  GetManageWorkDM;
  Result := ManageWorkDM.MultiOpenTotals6OP(Managers);
  {$I after.inc}
end;

function TQMService.MultiOpenTotals6PD(const SecSession,
  Managers: string): string;
begin
  Result := '';
  {$I before.inc}
  SetName('MultiOpenTotals6PD');   //EB QM-248 & QM-133 Past Due
  GetManageWorkDM;
  Result := ManageWorkDM.MultiOpenTotals6PD(Managers);
  {$I after.inc}
end;

function TQMService.MultiOpenTotals6TD(const SecSession,
  Managers: string): string;
begin
  Result := '';
  {$I before.inc}
  SetName('MultiOpenTotals6TD');   //QM-318 Today EB
  GetManageWorkDM;
  Result := ManageWorkDM.MultiOpenTotals6TD(Managers);
  {$I after.inc}
end;

function TQMService.MultiOpenTotals6V(const SecSession: string; const Managers: string;
  const VirtualBucketMgrID: integer): string;
begin
  Result := '';
  {$I before.inc}
  SetName('MultiOpenTotals6V');
  GetManageWorkDM;
  Result := ManageWorkDM.MultiOpenTotals6V(Managers, VirtualBucketMgrID);
  {$I after.inc}
end;

function TQMService.Ping(const SecSession: string; const AnyText: string): string;
var
  Ticks1, Ticks2: Int64;
  HowLong: Integer;
  I: Integer;
begin
  {$I before.inc}
  SetName('Ping');

  // Do nothing interesting; this allows tests such as the "does security work"
  // test on the prod server without doing any business operations.

  Ticks1 := GetTickCount;
  DM.TrivialOperation.Open;   // hit the DB, so we know it's up.
  DM.TrivialOperation.Close;
  Ticks2 := GetTickCount;

  HowLong := StrToIntDef(AnyText, 0);
  for I := 0 to HowLong do
    Sleep(1000);

  Result := 'DB Ping = ' + IntToStr(Ticks2 - Ticks1) + ' ms';
  {$I after.inc}
end;

function TQMService.PostTimesheets(const SecSession: string;
  const Timesheets: TimesheetEntryList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('PostTimesheets');
  GetTimesheetDM;
  Result := TimesheetDM.PostTimesheets(Timesheets);
  {$I after.inc}
end;

procedure TQMService.ReportErrors(const SecSession: string; const Errors: ErrorReportList);
begin
  {$I before.inc}
  SetName('ReportErrors');
  ReportErrorsInternal(Errors);
  {$I after.inc}
end;

procedure TQMService.SendTextViaEmail(const SecSession: string; const EmpID:integer; const TxtMessage: String);
var
  MailAddress, aRecipName, aSubject, aBody, FromEMail:string;
  PAD_CHAR:AnsiChar;
begin   //QM-9  SR
  {$I before.inc}
  PAD_CHAR :=#32;
  SetName('SendTextViaEmail');  
  MailAddress:='';
  aRecipName:='';
  aSubject :='';
  aBody :='';
  FromEMail:='';
  try
    DM.qryTextMailAddress.Parameters.ParamByName('EmpID').Value:= EmpID;
    DM.qryTextMailAddress.Parameters.ParamByName('LoginEmpID').Value:= LoggedInEmpID;
    DM.qryTextMailAddress.Open;
    MailAddress:= DM.qryTextMailAddress.FieldByName('TextMailAddress').AsString;
    aRecipName:=  DM.qryTextMailAddress.FieldByName('short_name').AsString;
    FromEMail :=  DM.qryTextMailAddress.FieldByName('FromAddress').AsString;
    aBody := TxtMessage;
    while Length(aBody)<=130 do  //QM-9 forces Disclaimer off text
    aBody:=aBody+ PAD_CHAR;

    DM.SendEmailWithIniSettingsExp('SendTextEmail', FromEMail, MailAddress, aSubject, aBody);

  finally
    DM.qryTextMailAddress.close;
  end;
  {$I after.inc}
End;

procedure TQMService.ReportErrorsInternal(const Errors: ErrorReportList);
var
  I: Integer;
  ER: ErrorReport;
  Data: TDataSet;
  Subject, Body, ToEmail,  FromEMail: string;
  EmailType: string;
  ReportedDate: TDateTime;
const
  BodyTemplate = 'Error: %s~Employee ID: %d~Employee #: %s~Employee Name: %s~Occurred: %s~Reported: %s~Severity: %d~Details:~%s~';
begin
  FromEMail:=''; //QM-9 gets this from INI
  Data := DM.ErrorReport;
  ReportedDate := Now;
  DM.Op.Detail := 'Adding errors to Error Report';
  for i := 0 to Errors.Count - 1 do begin
    ER := Errors[i];
    Data.Open;
    Data.Append;
    Data.FieldByName('emp_id').AsInteger := LoggedInEmpID;
    Data.FieldByName('occurred_date').AsDateTime := ER.OccurredWhen;
    Data.FieldByName('report_date').AsDateTime := ReportedDate;
    Data.FieldByName('severity').AsInteger := ER.Severity;
    Data.FieldByName('error_text').AsString := Copy(ER.ErrorText, 1, 200);
    if ER.Details <> '' then
      Data.FieldByName('details').AsString := ER.Details;
    Data.Post;

    Subject := Format('Q Manager Client Error Report: %s (Locator %d )', [ER.ErrorText, LoggedInEmpID]);
    Body := StringReplace(BodyTemplate, '~', #13#10, [rfReplaceAll]);
    Body := Format(Body,
             [ER.ErrorText,
              LoggedInEmpID,
              LoggedInEmpNumber,
              LoggedInEmpShortname,
              DateTimeToStr(ER.OccurredWhen),
              DateTimeToStr(ReportedDate),
              ER.Severity,
              ER.Details]);

    EmailType := '';
    if AnsiContainsText(ER.ErrorText, 'Missing Locates') then
      EmailType := 'client_error'
    else if AnsiContainsText(ER.ErrorText, 'Time Mismatch') then
      EmailType := 'time_warning';

    ToEmail := '';
    try
      if EmailType <> '' then begin
        // at this point use DM.FindUserNotifEmail to find the TO address
        DM.FindUserNotifEmail.Parameters.ParamValues['@EmpId'] := LoggedInEmpId;
        DM.FindUserNotifEmail.Parameters.ParamValues['@EmailType'] := EmailType;
        DM.FindUserNotifEmail.Open;
        if not DM.FindUserNotifEmail.Eof then begin
          ToEmail := DM.FindUserNotifEmail.FieldByName('email_destination').AsString;
        end;
      end;
    except
      on E: Exception do begin
        DM.Op.Log('ERROR getting email notif dest: ' + E.Message);
      end;
    end;

    // always send email to this address
    DM.Op.Detail := Format('E-mailing %s problem report to %s',[EmailType, ToEmail]);
    DM.SendEmailWithIniSettingsExp('ErrorReportEmail', FromEMail, ToEmail, Subject, Body);
  end;
  Data.Close;
end;

procedure TQMService.SaveNewAttachments(const SecSession: string;
  const NewAttachments: NewAttachmentList; out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('SaveNewAttachments');
  SaveNewAttachmentsInternal(SecSession, NewAttachments, GeneratedKeys);
  {$I after.inc}
end;

procedure TQMService.SaveNewAttachmentsInternal(const SecSession: string;
  const NewAttachments: NewAttachmentList; out GeneratedKeys: GeneratedKeyList);

  procedure GetAttachmentAsNewAttachmentObject(Orig: NewAttachment; New: TNewAttachment);
  begin
    Assert(Assigned(New));
    Assert(Assigned(Orig));
    New.AttachmentID := Orig.AttachmentID;
    New.ForeignType := Orig.ForeignType;
    New.ForeignID := Orig.ForeignID;
    New.Size := Orig.Size;
    New.AttachedBy := Orig.AttachedBy;
    New.FileName := Orig.FileName;
    New.OrigFileName := Orig.OrigFileName;
    New.OrigFileModDate := Orig.OrigFileModDate;
    New.AttachDate := Orig.AttachDate;
    New.Source := Orig.Source;
    New.FileHash := Orig.FileHash;
    New.UploadMachineName := Orig.UploadMachineName;
  end;
var
  i: Integer;
  OldID: Integer;
  NewID: Integer;
  Attacher: TServerAttachment;
  NewAttachmentObject: TNewAttachment;
begin
  NewAttachmentObject := nil;

  GeneratedKeys := GeneratedKeyList.Create;
  Attacher := TServerAttachment.Create(DM.Conn, LoggedInEmpID);
  try
    NewAttachmentObject := TNewAttachment.Create;
    Attacher.OnLogMessage := LogAttachmentProgress;
    Attacher.ClientVersion := DM.ClientVersion;
    for i := 0 to NewAttachments.Count - 1 do begin
      DM.Op.Detail := 'SaveNewAttachment';
      GetAttachmentAsNewAttachmentObject(NewAttachments[i], NewAttachmentObject);
      Attacher.InsertNew(NewAttachmentObject, OldId, NewId);
      if OldID < 0 then
        AddIdentity(GeneratedKeys, 'attachment', OldID, NewID);
    end;
  finally
    FreeAndNil(NewAttachmentObject);
    FreeAndNil(Attacher);
  end;
end;


function TQMService.SaveNewCustomFormData(const SecSession: string; const CustomFormAnswers: CustomFormAnswerList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveNewCustomFormData');    //QM-593 Custom Form (AT&T Damage form)
  GetDM;
  Result := DM.SaveNewCustomFormData(CustomFormAnswers);
  {$I after.inc}
end;

procedure TQMService.SaveModCustomFormData(const SecSession: string;
  const CustomFormAnswers: CustomFormAnswerList);
begin
    {$I before.inc}
  SetName('SaveModCustomFormData');    //QM-593 Custom Form (AT&T Damage form)
  GetDM;
  DM.SaveModCustomFormData(CustomFormAnswers);
  {$I after.inc}
end;

procedure TQMService.SaveAttachmentUploads(const SecSession: string;
  const AttachmentUploads: AttachmentUploadList);
begin
  {$I before.inc}
  SetName('SaveAttachmentUploads');
  SaveAttachmentUploadsInternal(SecSession, AttachmentUploads);
  {$I after.inc}
end;

procedure TQMService.SaveAttachmentUploadsInternal(
  const SecSession: string; const AttachmentUploads: AttachmentUploadList);

  procedure GetAttachmentAsAttachmentUploadObject(Orig: AttachmentUpload; New: TAttachmentUpload);
  begin
    Assert(Assigned(New));
    Assert(Assigned(Orig));
    New.AttachmentID := Orig.AttachmentID;
    New.UploadDate := Orig.UploadDate;
    New.BackgroundUpload := Orig.BackgroundUpload;
  end;
var
  i: Integer;
  Attacher: TServerAttachment;
  AttachmentUploadObject: TAttachmentUpload;
begin
  AttachmentUploadObject := nil;

  Attacher := TServerAttachment.Create(DM.Conn, LoggedInEmpID);
  try
    AttachmentUploadObject := TAttachmentUpload.Create;
    Attacher.OnLogMessage := LogAttachmentProgress;
    Attacher.ClientVersion := DM.ClientVersion;
    for i := 0 to AttachmentUploads.Count - 1 do begin
      DM.Op.Detail := 'SaveAttachmentUpload';
      GetAttachmentAsAttachmentUploadObject(AttachmentUploads[i], AttachmentUploadObject);
      Attacher.MarkUploaded(AttachmentUploadObject);
    end;
  finally
    FreeAndNil(AttachmentUploadObject);
    FreeAndNil(Attacher);
  end;
end;

procedure TQMService.SaveAttachmentUpdates(const SecSession: string;
  const AttachmentUpdates: AttachmentUpdateList);
begin
  {$I before.inc}
  SetName('SaveAttachmentUpdates');
  SaveAttachmentUpdatesInternal(SecSession, AttachmentUpdates);
  {$I after.inc}
end;

procedure TQMService.SaveAttachmentUpdatesInternal(
  const SecSession: string; const AttachmentUpdates: AttachmentUpdateList);

  procedure GetAttachmentAsAttachmentUpdateObject(Orig: AttachmentUpdate; New: TAttachmentUpdate);
  begin
    Assert(Assigned(New));
    Assert(Assigned(Orig));
    New.AttachmentID := Orig.AttachmentID;
    New.Active := Orig.Active;
    New.Comment := Orig.Comment;
    New.DocumentType := Orig.DocumentType;
  end;
var
  i: Integer;
  Attacher: TServerAttachment;
  AttachmentUpdateObject: TAttachmentUpdate;
begin
  AttachmentUpdateObject := nil;

  Attacher := TServerAttachment.Create(DM.Conn, LoggedInEmpID);
  try
    AttachmentUpdateObject := TAttachmentUpdate.Create;
    Attacher.OnLogMessage := LogAttachmentProgress;
    Attacher.ClientVersion := DM.ClientVersion;
    for i := 0 to AttachmentUpdates.Count - 1 do begin
      DM.Op.Detail := 'SaveAttachmentUpdate';
      GetAttachmentAsAttachmentUpdateObject(AttachmentUpdates[i], AttachmentUpdateObject);
      Attacher.UpdateInfo(AttachmentUpdateObject);
    end;
  finally
    FreeAndNil(AttachmentUpdateObject);
    FreeAndNil(Attacher);
  end;
end;

function TQMService.GetAssets(const SecSession: string; const EmpID: Integer): string;
begin
  {$I before.inc}
  SetName('GetAssets');
  GetAssetDM;
  Result := AssetDM.GetAssets(EmpID);
  {$I after.inc}
end;

procedure TQMService.SaveEmployeeAssets2(const SecSession: string;
  const EmpID: Integer; const Assets: NVPairListList);
begin
  {$I before.inc}
  SetName('SaveEmployeeAssets2');
  GetAssetDM;
  AssetDM.SaveEmployeeAssets2(EmpID, Assets);
  {$I after.inc}
end;

procedure TQMService.ReassignAssets(const SecSession: string; const Assets: NVPairListList);
begin
  {$I before.inc}
  SetName('ReassignAssets');
  GetAssetDM;
  AssetDM.ReassignAssets(Assets);
  {$I after.inc}
end;



function TQMService.SearchTimesheetsForProfitCenter(const SecSession: string;
  const SearchType: Integer; const DateStart, DateStop: DateTime;
  const StartEmpId, LevelLimit: Integer; const ProfitCenter: string): string;
begin
  {$I before.inc}
  SetName('SearchTimesheets');
  GetTimesheetDM;
  Result := TimesheetDM.SearchTimesheets(SearchType, DateStart, DateStop, StartEmpId, LevelLimit, ProfitCenter);
  {$I after.inc}
end;

procedure TQMService.SetName(OpName: string);
begin
  Assert(Assigned(DM.Op), 'No active Operation defined');
  DM.Op.OpName := OpName;
end;

procedure TQMService.StatusTicketsLocates14(const SecSession: string;
  const TicketChanges: TicketChangeList14;
  out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('StatusTicketsLocates14');
  GetTicketDM;
  TicketDM.StatusTicketsLocates14Internal(TicketChanges, GeneratedKeys);
  {$I after.inc}
end;

procedure TQMService.StatusTicketsLocates15(const SecSession: string;
  const TicketChanges: TicketChangeList15; out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('StatusTicketsLocates15');
  GetTicketDM;
  TicketDM.StatusTicketsLocates15Internal(TicketChanges, GeneratedKeys);
  {$I after.inc}
end;

//TicketSearch6 introduced in build 8674
function TQMService.TicketSearch6(const SecSession: string; const TicketNumber: string;
            const Street: string; const City: string; const State: string;
            const County: string; const WorkType: string; const TicketType: string;
            const DoneFor: string; const Company: string; const DueDateFrom: DateTime;
            const DueDateTo: DateTime; const RecvDateFrom: DateTime; const RecvDateTo: DateTime;
            const CallCenter: string; const TermCode: string; const Attachments: Integer;
            const ManualTickets: Integer; const Priority: string; const UtilityCo: Integer): string; //QMANTWO-810
begin
  {$I before.inc}
  SetName('TicketSearch6');
  GetTicketDM;
  Result := TicketDM.TicketSearch(TicketNumber, Street, City, State, County,
    WorkType, TicketType, DoneFor, Company, DueDateFrom, DueDateTo, RecvDateFrom,
    RecvDateTo, CallCenter, TermCode, Attachments, ManualTickets, Priority, UtilityCo);   //QMANTWO-810
  {$I after.inc}
end;

function TQMService.DebugLocateIDSearch(const SecSession: string;
  const LocateID: integer): string;
begin
 {$I before.inc}
  SetName('DebugLocateIDSearch');
  GetTicketDM;
  Result := TicketDM.LocateIDSearch(LocateID);
  {$I after.inc}
end;

function TQMService.DebugTicketIDSearch(const SecSession: string;
  const TicketID: integer): string;
  //QM-305 EB Add Debug Ticket ID Search
begin
  {$I before.inc}
  SetName('DebugTicketIDSearch');
  GetTicketDM;
  Result := TicketDM.TicketIDSearch(TicketID);
  {$I after.inc}
end;

function TQMService.WorkOrderSearch(const SecSession, TicketNumber, WorkOrderNumber,
  Street, City, State, County, WOSource, ClientName,
  Company: string; const DueDateFrom, DueDateTo, TransmitDateFrom,
  TransmitDateTo: DateTime; const Kind: string; const Tickets: Integer): string;
begin
  {$I before.inc}
  SetName('WorkOrderSearch');
  GetWorkOrderDM;
  Result := WorkOrderDM.WorkOrderSearch(TicketNumber, WorkOrderNumber, Street,
    City, State, County, WOSource, ClientName, Company, DueDateFrom, DueDateTo,
    TransmitDateFrom, TransmitDateTo, Kind, Tickets);
  {$I after.inc}
end;

function TQMService.Login(const SecSession, UserName, Password, AppVersion: string): LoginData;
var
  VersionMessage: string;
begin
  FAllowExpiredLogin := True;
  Result := nil;
  {$I before.inc}

  // If we make it here, CheckSecurity has validated the login already
  SetName('Login');
  Assert((LoggedInEmpID > 0) and (LoggedInUID > 0));

  Result := LoginData.Create;
  Result.SecSession := '';
  Result.EmpID := LoggedInEmpID;
  Result.UserID := LoggedInUID;
  Result.ShortName := LoggedInEmpShortName;
  Result.ReportToEmpID := LoggedInReportTo;
  Result.UserRole := LoggedInUserType;
  Result.DisplayMessage := '';
  Result.EmpTypeID := 0;
  Result.PasswordExpirationDate := LoggedInChgPwdDate;
  Result.NeedPasswordChange := PasswordChangeNone;
  if LoggedInChgPwd or ((LoggedInChgPwdDate > 1) and (LoggedInChgPwdDate < Now)) then
    Result.NeedPasswordChange := PasswordChangeForce;

  DM.CheckApplicationVersion(True, VersionMessage);
  Result.DisplayMessage := VersionMessage;
  {$I after.inc}
end;      

function TQMService.Login2(const SecSession, UserName, Password, AppVersion: string): LoginData2;
var
  VersionMessage, adUserName: string;
  ADSI : TADSI;
   sDomain : string;
begin
  FAllowExpiredLogin := True;
  Result := nil;
  {$I before.inc}
  try
    SetPhase('Getting ready to check AD User Rights');
    ADSI := TADSI.Create(nil);
    sDomain := DM.GetHomeDomain;
    DM.CheckLogin.Close;
    DM.CheckLogin.Parameters.ParamValues['@UserName'] := UserName;
    if length(Password) < 35 then
      DM.CheckLogin.Parameters.ParamValues['@PW'] := GeneratePasswordHash(UserName, Password)
    else
      DM.CheckLogin.Parameters.ParamValues['@PW'] := Password;
    DM.CheckLogin.Open;
        if ((DM.CheckLogin.Eof) or (DM.CheckLogin.FieldByName('ad_username').IsNull) or (DM.CheckLogin.FieldByName('ad_username').asstring = '')) then
        begin
          SetPhase('');
          raise ELoginException.CreateFmt('Login failure, No AD user name for %s', [UserName]);
        end
        else
        adUserName := DM.CheckLogin.FieldByName('ad_username').AsString;

      SetPhase('Checking security - checking Active Directory status');
      if (ADSI.IsUserActiveAD(sDomain, adUserName)) = false then
      begin
      SysErrorMessage(GetLastError);
        SetPhase('Checking security - checking Active Directory status has failed');
        raise ELoginException.CreateFmt(SysErrorMessage(GetLastError)+' AD account inactive for %s', [UserName]);
      end;
  finally
    DM.CheckLogin.close;
    FreeAndNil(ADSI);
  end;

  // If we make it here, CheckSecurity has validated the login already
  SetName('Login');
  Assert((LoggedInEmpID > 0) and (LoggedInUID > 0));

  Result := LoginData2.Create;
  Result.SecSession := '';
  Result.EmpID := LoggedInEmpID;
  Result.UserID := LoggedInUID;
  Result.ShortName := LoggedInEmpShortName;
  Result.ReportToEmpID := LoggedInReportTo;
  Result.UserRole := LoggedInUserType;
  Result.DisplayMessage := '';
  Result.EmpTypeID := 0;
  Result.PasswordExpirationDate := LoggedInChgPwdDate;
  Result.NeedPasswordChange := PasswordChangeNone;
  Result.APIKey := LoggedInEmpAPIKey;
  if LoggedInChgPwd or ((LoggedInChgPwdDate > 1) and (LoggedInChgPwdDate < Now)) then
    Result.NeedPasswordChange := PasswordChangeForce;

  DM.CheckApplicationVersion(True, VersionMessage);
  Result.DisplayMessage := VersionMessage;
  {$I after.inc}
end;

function TQMService.LoginAD(const SecSession, UserName: string): boolean;
var
  adUserName: string;
  ADSI : TADSI;
  sDomain : string;
begin
  FAllowExpiredLogin := True;
  Result := False;

  {$I before.inc}
  try
    ADSI := TADSI.Create(nil);
    sDomain := DM.GetHomeDomain;
    DM.CheckADLogin.Close;
    DM.CheckADLogin.Parameters.ParamValues['@UserName'] := UserName;
    DM.CheckADLogin.Open;
        if ((DM.CheckADLogin.IsEmpty) or (DM.CheckADLogin.FieldByName('ad_username').IsNull) or (DM.CheckADLogin.FieldByName('ad_username').asstring = '')) then
        begin
          Result := False;
          SetPhase('Missing AD User Name or Employee not set to Active');
          raise ELoginException.CreateFmt('Login failure, No AD user name for %s', [UserName]);
          exit;
        end
        else
          adUserName := DM.CheckADLogin.FieldByName('ad_username').AsString;

      SetPhase('Checking security - checking Active Directory status');


      if (ADSI.IsUserActiveAD(sDomain, adUserName)) = false then
      begin
        SysErrorMessage(GetLastError);
        SetPhase('Checking security - checking Active Directory status has failed');
        Result := False;
   //     raise ELoginException.CreateFmt(SysErrorMessage(GetLastError)+' AD account inactive for %s', [UserName]);
      end
      else
        Result := True;
  finally
    DM.CheckADLogin.close;
    FreeAndNil(ADSI);
  end;

  // If we make it here, CheckSecurity has validated the login already
  SetName('LoginAD');
  Assert((LoggedInEmpID > 0) and (LoggedInUID > 0));

  {$I after.inc}
end;

(*
procedure TQMService.MarkRowModified(const TableName, PKName: string; PKValue: Integer);
const
  SQL = 'update %s set modified_date = getdate() where %s = %d';
begin
  ExecuteSQL(Format(SQL, [TableName, PKName, PKValue]));
  if FExecuteRecsAffected <> 1 then
    raise Exception.CreateFmt('Error setting %s/%s/%d modified', [TableName, PKName, PKValue]);
end;
*)

procedure TQMService.ReportStatus(const SecSession, RequestID: string;
  const Success: Boolean; const ElapsedTime: DateTime;
  const Details: string);
var
  Seconds: Integer;
begin
  {$I before.inc}
  SetName('ReportStatus');
  if Success then
    SetPhase('Report Success, ID=' + RequestID)
  else
    SetPhase('Report Failure, ID=' + RequestID + ' Details=' + Details);

  Seconds := Round(ElapsedTime * 24.0 * 60.0 * 60.0);

  DM.LogReportResult.Parameters.ParamValues['client_success'] := Success;
  DM.LogReportResult.Parameters.ParamValues['client_elapsed'] := Seconds;
  DM.LogReportResult.Parameters.ParamValues['client_details'] := Copy(Details, 1, 150);
  DM.LogReportResult.Parameters.ParamValues['request_id'] := RequestID;
  DM.LogReportResult.Execute;
  {$I after.inc}
end;

function TQMService.GetRights(const SecSession: string; const EmpID: Integer): string;
const
  EmpSQL = 'select * from employee_right where emp_id = %d';
  GrpSQL = 'select * from dbo.get_emp_group_rights(%d)';
var
  EmpRights: TDataSet;
  GrpRights: TDataSet;
begin
  {$I before.inc}
  SetName('GetRights');

  DM.Op.Detail := Format('Employee Rights for EmpID: %d', [EmpID]);

  GrpRights := nil;
  EmpRights := DM.CreateDataSetForSQL(Format(EmpSQL, [EmpID]));
  try
    Assert(Assigned(EmpRights));
    GrpRights := DM.CreateDataSetForSQL(Format(GrpSQL, [EmpID]));
    Result := ConvertDataSetsToSQLXML([EmpRights, GrpRights], ['employee_right', 'emp_group_right']);
  finally
    FreeAndNil(EmpRights);
    FreeAndNil(GrpRights);
  end;
  {$I after.inc}
end;

//SyncDown6 introduced in build 10509
function TQMService.SyncDown6(const SecSession, UpdatesSince,
  SyncRequestID, ClientVersion: string; const LocalTime: DateTime;
  const UTCBias: Integer; const TicketInCachePacked: string;
  const ComputerInfo: ComputerInformation;
  const SentBytes: Integer;
  const AdditionalInfo: NVPairList;
  const DamageInCachePacked: string;
  const WorkOrderInCachePacked: string): string;
var
  TicketArray: IntArray;
  TicketList: string;
  DamageArray: IntArray;
  DamageList: string;
  WorkOrderArray: IntArray;
  WorkOrderList: string;
  NAdded: Integer;
  NumTicketsInCache: Integer;
  NumDamagesInCache: Integer;
  NumWOsInCache: Integer;
begin
  // Some code here is also present above, to minimize critical path code changes
  // All of this and similar duplication will be removed when moving to BDS2006
  // and not requiring backward compatibility with older clients
  SetLength(TicketArray, 0);  // To prevent compiler warning
  SetLength(DamageArray, 0);  // To prevent compiler warning
  SetLength(WorkOrderArray, 0);// To prevent compiler warning
  {$I before.inc}
  SetName('SyncDown6');

  //Build existing Tickets List:
  SetPhase('Building client ticket list');
  TicketArray := UnPackIntArray(TicketInCachePacked);
  TicketList := GetIDList(TicketArray, NAdded, NumTicketsInCache);
  if NAdded <> NumTicketsInCache then
    raise Exception.Create('Internal error reading TicketArray, expected '
       + IntToStr(NumTicketsInCache) + ' but got ' + IntToStr(NAdded));

  //Build Existing Damages List:
  SetPhase('Building client damage list');
  DamageArray := UnPackIntArray(DamageInCachePacked);
  DamageList := GetIDList(DamageArray, NAdded, NumDamagesInCache);
  if NAdded <> NumDamagesInCache then
    raise Exception.Create('Internal error reading DamageArray, expected '
       + IntToStr(NumDamagesInCache) + ' but got ' + IntToStr(NAdded));

  //Build Existing Work Orders List:
  SetPhase('Building client work order list');
  WorkOrderArray := UnPackIntArray(WorkOrderInCachePacked);
  WorkOrderList := GetIDList(WorkOrderArray, NAdded, NumWOsInCache);
  if NAdded <> NumWOsInCache then
    raise Exception.Create('Internal error reading WorkOrderArray, expected '
       + IntToStr(NumWOsInCache) + ' but got ' + IntToStr(NAdded));

  DM.Op.Detail := Format('Since: %s, #TicInClient: %d (%d), #DmgInClient: %d (%d), #WOInClient: %d (%d)',
    [UpdatesSince, NumTicketsInCache, Length(TicketList), NumDamagesInCache,
    Length(DamageList), NumWOsInCache, Length(WorkOrderList)]);

  Result := SyncInternal(SecSession, UpdatesSince, SyncRequestID, ClientVersion,
    LocalTime, UTCBias, TicketList, ComputerInfo, SentBytes, AdditionalInfo,
    DamageList, WorkOrderList, NumTicketsInCache);
  {$I after.inc}
end;

function TQMService.GetIDList(const ItemArray: IntArray; var Added, InCache: Integer): string;
var
  I: Integer;
begin
  Result := '';

  Added := 0;
  for I := Low(ItemArray) to High(ItemArray) do begin
    if Result = '' then
      Result := IntToStr(ItemArray[i])
    else
      Result := Result + ',' + IntToStr(ItemArray[i]);
    Inc(Added);
  end;
  InCache := High(ItemArray)-Low(ItemArray)+1;
end;

function TQMService.SyncInternal(const SecSession, UpdatesSince,
  SyncRequestID, ClientVersion: string; const LocalTime: DateTime;
  const UTCBias: Integer; const TicketInCacheCSV: string;
  const ComputerInfo: ComputerInformation;
  SentBytes: Integer; AdditionalInfo: NVPairList; const DamageInCacheCSV: string;
  const WorkOrderInCacheCSV: string; const NumTicketsInClientCache: integer): string;
var
  VersionMessage: string;
  StartTime, EndTime: TDateTime;
  ElapSeconds: Integer;
  AddSerial, AddDomain: Variant;
  ComputerModel: Variant;
  AddinVersion: Variant;
  PersequorVersion: Variant;
  MemeVersion: Variant;  //qm-972 sr
  SyncSource: Variant;
  AssetTag : Variant;
  AirCardPhoneNo : Variant;
  AirCardESN  : Variant;
CONST
  PLAT_BASE_DATE : TDatetime = 1/1/2015;  //QM-23 sr minimum plat date
begin
  // VersionMessage is currently ignored.  In the next SyncDown revision, return/display it.
  DM.CheckApplicationVersion(False, VersionMessage);

  StartTime := Now;
  Result := GetSyncDataWithoutSQLXML(UpdatesSince, TicketInCacheCSV, DamageInCacheCSV, WorkOrderInCacheCSV);
  EndTime := Now;
  ElapSeconds := Round((EndTime - StartTime) * 24.0 * 60.0 * 60.0);

  try
    SetPhase('Logging sync');
    // Record in sync log
    if Assigned(AdditionalInfo) then begin
      AddSerial := NVPairString(AdditionalInfo, 'ComputerSerial', bvNull);
      AddDomain := NVPairString(AdditionalInfo, 'DomainLogin', bvNull);
      ComputerModel := NVPairString(AdditionalInfo, 'ComputerModel', bvNull);
      AddinVersion := NVPairString(AdditionalInfo, 'AddinVersion', bvNull);
      PersequorVersion := NVPairString(AdditionalInfo, 'PersequorVersion', bvNull);
      MemeVersion := NVPairString(AdditionalInfo, 'MemeVersion', bvNull);   //qm-972 sr
      SyncSource := NVPairString(AdditionalInfo, 'SyncSource', bvNull);
      AssetTag := NVPairString(AdditionalInfo, 'AssetTag', bvNull);
      AirCardPhoneNo := NVPairString(AdditionalInfo, 'AirCardPhoneNo', bvNull);
      AirCardESN  := NVPairString(AdditionalInfo, 'AirCardESN', bvNull);
    end else begin
      AddSerial := Null;
      AddDomain := Null;
      ComputerModel := Null;
      AddinVersion := Null;
      PersequorVersion := Null;
      SyncSource := Null;
      AssetTag   := Null;
      AirCardPhoneNo := Null;
      AirCardESN := Null;
    end;
    DM.InsertIntoSyncLog.Parameters.ParamValues['EmployeeID'] := LoggedInEmpID;
    DM.InsertIntoSyncLog.Parameters.ParamValues['ClientVersion'] := ClientVersion;
    DM.InsertIntoSyncLog.Parameters.ParamValues['LocalDate'] := LocalTime;
    DM.InsertIntoSyncLog.Parameters.ParamValues['UTCBias'] := UTCBias;

    if UpdatesSince <> '' then
      DM.InsertIntoSyncLog.Parameters.ParamValues['UpdatesSince'] := IsoStrToDateTime(UpdatesSince)
    else
      DM.InsertIntoSyncLog.Parameters.ParamValues['UpdatesSince'] := Null;

    DM.InsertIntoSyncLog.Parameters.ParamValues['bytes_down'] := Length(Result);
    DM.InsertIntoSyncLog.Parameters.ParamValues['total_time'] := ElapSeconds;
    if SentBytes >= 0 then
      DM.InsertIntoSyncLog.Parameters.ParamValues['bytes_up'] := SentBytes
    else
      DM.InsertIntoSyncLog.Parameters.ParamValues['bytes_up'] := Null;
    DM.InsertIntoSyncLog.Parameters.ParamValues['addin_version'] := AddinVersion;
    DM.InsertIntoSyncLog.Parameters.ParamValues['persequor_version'] := PersequorVersion;
    DM.InsertIntoSyncLog.Parameters.ParamValues['Meme_version'] := MemeVersion;    //qm-972 sr
    DM.InsertIntoSyncLog.Parameters.ParamValues['sync_source'] := SyncSource;
    DM.InsertIntoSyncLog.Parameters.ParamValues['NumTicketsInClientCache'] :=  NumTicketsInClientCache;
    DM.InsertIntoSyncLog.Execute;
  except
    on E: Exception do
      DM.Op.Log('Error logging sync: ' + E.Message);
  end;

  SetPhase('Saving employee local utc changes');
  DM.UpdateEmployeeLocalUTCBias(LoggedInEmpID, UTCBias);

  {Checks the server clock to the local computer clock}
  CheckTimeWindow(LocalTime, UTCBias);

//  SetPhase('Checking Computer Information');
//  if AddSerial <> 'XYXYX' then begin
//    DM.Op.Log(Format('WRONG SERIAL NUMBER (%s / %s)', [AddSerial, ComputerModel]));
//    SetPhase('ERROR: Computer Serial Number does not match.');
//    raise Exception.Create(Format('User has not been assigned this serial number (%s / %s)', [AddSerial, ComputerModel]));
//  end;

  if DM.GetIniBoolSetting('Features', 'SaveComputerInfo', True) then begin
    try
      SetPhase('Saving computer info');
      DM.RecordComputerInfo.Parameters.ParamValues['@EmployeeID'] := LoggedInEmpID;
      DM.RecordComputerInfo.Parameters.ParamValues['@WindowsUser'] := ComputerInfo.WindowsUser;
      DM.RecordComputerInfo.Parameters.ParamValues['@OsPlatform'] := ComputerInfo.OsPlatform;
      DM.RecordComputerInfo.Parameters.ParamValues['@OsMajorVersion'] := ComputerInfo.OsMajorVersion;
      DM.RecordComputerInfo.Parameters.ParamValues['@OsMinorVersion'] := ComputerInfo.OsMinorVersion;
      DM.RecordComputerInfo.Parameters.ParamValues['@OsServicePackVersion'] := ComputerInfo.OsServicePackVersion;
      DM.RecordComputerInfo.Parameters.ParamValues['@ComputerName'] := ComputerInfo.ComputerName;
      DM.RecordComputerInfo.Parameters.ParamValues['@DellServiceTag'] := ComputerInfo.DellServiceTag;
      DM.RecordComputerInfo.Parameters.ParamValues['@HotFixList'] := ComputerInfo.HotFixList;
      {
      The following was removed until all users clients are updated past 20674  -- SR  1/30/20
      The service ComputerInfo.PlatDate has been removed and will have to be added resored
//      if ComputerInfo.PlatDate > PLAT_BASE_DATE then  //QM-23  SR
//      DM.RecordComputerInfo.Parameters.ParamValues['@Platdate'] := ComputerInfo.PlatDate else //QM-23
//      DM.RecordComputerInfo.Parameters.ParamValues['@Platdate'] := NULL; //QM-23  SR
      }
      DM.RecordComputerInfo.Parameters.ParamValues['@ComputerSerial'] := AddSerial;
      DM.RecordComputerInfo.Parameters.ParamValues['@DomainLogin'] := AddDomain;
      DM.RecordComputerInfo.Parameters.ParamValues['@ComputerModel'] := ComputerModel;
      DM.RecordComputerInfo.Parameters.ParamValues['@AssetTag'] := AssetTag;
      DM.RecordComputerInfo.Parameters.ParamValues['@AirCardPhoneNo'] := AirCardPhoneNo;
      DM.RecordComputerInfo.Parameters.ParamValues['@AirCardESN'] := AirCardESN;
      DM.RecordComputerInfo.ExecProc;
    except
      on E: Exception do
        DM.Op.Log(Format('Error saving computer info (%s/%s/%s/%s): ', [ComputerInfo.WindowsUser, ComputerInfo.ComputerName, ComputerInfo.DellServiceTag, ComputerInfo.HotFixList]) + E.Message);
    end;
  end;
end;

procedure TQMService.CheckTimeWindow(SyncTime: TDateTime; const SyncTimeBias: Integer);
var
  CurrentTime, CurrentTimeAdj: TDateTime;
  SyncTimeAdj: TDateTime;
  TimeDiffToleranceMins: integer;
  MismatchErrors: ErrorReportList;
  MismatchER: ErrorReport;
  Gap: integer;
begin
  CurrentTime := Now;
  // Convert both times to UTC (Coordinated Universal Time) to compare them
  CurrentTimeAdj := DateTimeToUTCDateTime(CurrentTime, GetUTCBias);
  SyncTimeAdj := DateTimeToUTCDateTime(SyncTime, SyncTimeBias);

  TimeDiffToleranceMins := DM.GetConfigurationDataInt('TimeDiffToleranceMinutes', 5); //QMANTWO-762 Default: 5 mins if not set in Config

  Gap := DateUtils.MinutesBetween(CurrentTimeAdj, SyncTimeAdj);
  if Gap >= TimeDiffToleranceMins then begin
    MismatchErrors := ErrorReportList.Create;
    try
      MismatchER := MismatchErrors.Add;
      MismatchER.OccurredWhen := Now;
      MismatchER.Severity := 5;
      MismatchER.ErrorText := 'Time Mismatch';
      MismatchER.Details := 'Sync Time: ' + DateTimeToStr(SyncTime) + #13#10 +
                            'Server Clock: ' + DateTimeToStr(CurrentTime)+ #13#10 +
                            Format('Time mismatch of > %d minutes', [TimeDiffToleranceMins]);

      ReportErrorsInternal(MismatchErrors);
    finally
      FreeAndNil(MismatchErrors);
    end;
  end;
end;

function TQMService.GetSickleavePTOBalances(const SecSession: String;     //QM-549 EB Sick/PTO balances
  const Emp_ID: Integer): string;
begin
  {$I before.inc}
  SetName('GetSickleavePTOBalances');
  GetTimesheetDM;
  Result := TimesheetDM.GetSickLeavePTOBalances(Emp_ID);
  {$I after.inc}
end;

function TQMService.GetSyncDataWithoutSQLXML(const UpdatesSince: string;
  const TicketInCacheCSV: string; const DamageInCacheCSV: string;
  const WorkOrderInCacheCSV: string): string;
var
  SSP: TADOStoredProc;
begin
  SetPhase('Configuring Sync SP');
  SSP := DM.SyncSP;
  SSP.Parameters.ParamValues['@EmployeeID'] := LoggedInEmpID;
  SSP.Parameters.ParamValues['@UpdatesSinceS'] := UpdatesSince;
  SSP.Parameters.ParamValues['@ClientTicketListS'] := TicketInCacheCSV;
  SSP.Parameters.ParamValues['@ClientDamageListS'] := DamageInCacheCSV;
  SSP.Parameters.ParamValues['@ClientWorkOrderListS'] := WorkOrderInCacheCSV;
  SetPhase('Running Sync SP');
  SSP.Open;
  SetPhase('Packing Results as XML');
  try
    if IsoStrToDateTimeDef(UpdatesSince) < EncodeDate(1990, 1, 1) then
      Result := FormatSqlXml(SSP, 3, LogSQLXMLProgress)
    else
      Result := FormatSqlXml(SSP, 1, LogSQLXMLProgress);
  finally
    SSP.Close;
  end;
end;

procedure TQMService.LogSQLXMLProgress(var TableName: string; var Count: Integer);
begin
  SetPhase(Format('Packing Results as XML: %s (%d %s)',
    [TableName, Count, AddSIfNot1(Count, 'row')]));
end;



function TQMService.GetWorkOrder(const SecSession: string; const WorkOrderID: Integer): string;
begin
  {$I before.inc}
  SetName('GetWorkOrder');
  GetWorkOrderDM;
  Result := WorkOrderDM.GetWorkOrder(WorkOrderID);
  {$I after.inc}
end;

//OHM
function TQMService.GetWorkOrderOHM(const SecSession: string;
  const WorkOrderID: Integer): string;
begin
  {$I before.inc}
  SetName('GetWorkOrderOHM');
  GetWorkOrderDM;
  Result := WorkOrderDM.GetWorkOrderOHMDetails(WorkOrderID);
  {$I after.inc}
end;

function TQMService.GetTicket2(const SecSession: string; const TicketID: Integer; const UpdatesSince: TDateTime): string;
begin
  {$I before.inc}
  SetName('GetTicket2');
  GetTicketDM;
  Result := TicketDM.GetTicket2(TicketID, UpdatesSince);
  {$I after.inc}
end;

function TQMService.GetTicketHistory(const SecSession: string; const TicketID: Integer): string;
begin
  {$I before.inc}
  SetName('GetTicketHistory');
  GetTicketDM;
  Result := TicketDM.GetTicketHistory(TicketID);
  {$I after.inc}
end;

function TQMService.GetWorkOrderHistory(const SecSession: string; const WorkOrderID: Integer): string;
begin
  {$I before.inc}
  SetName('GetWorkOrderHistory');
  GetWorkOrderDM;
  Result := WorkOrderDM.GetWorkOrderHistory(WorkOrderID);
  {$I after.inc}
end;



function TQMService.GetDamage2(const SecSession: string; const DamageID: Integer): string;
begin
  {$I before.inc}
  SetName('GetDamage2');
  GetDamageDM;
  Result := DamageDM.GetDamage2(DamageID);
  {$I after.inc}
end;

function TQMService.GetDamageInvoice(const SecSession: string; const InvoiceID: Integer): string;
begin
  {$I before.inc}
  SetName('GetDamageInvoice');
  GetDamageDM;
  Result := DamageDM.GetDamageInvoice(InvoiceID);
  {$I after.inc}
end;

function TQMService.GetDamageLitigation(const SecSession: string; const LitigationID: Integer): string;
begin
  {$I before.inc}
  SetName('GetDamageLitigation');
  GetDamageDM;
  Result := DamageDM.GetDamageLitigation(LitigationID);
  {$I after.inc}
end;

function TQMService.GetDamageThirdParty(const SecSession: string;
  const ThirdPartyID: Integer): string;
begin
  {$I before.inc}
  SetName('GetDamageThirdParty');
  GetDamageDM;
  Result := DamageDM.GetDamageThirdParty(ThirdPartyID);
  {$I after.inc}
end;

function TQMService.GetTicketsForLocator(const SecSession: string;
  const LocatorID, NumDays: Integer; const OpenOnly, Unacknowledged: Boolean): string;
begin
  {$I before.inc}
  SetName('GetTicketsForLocator');
  GetManageWorkDM;
  Result := ManageWorkDM.GetTicketsForLocator(LocatorID, NumDays, OpenOnly, Unacknowledged);
  {$I after.inc}
end;

function TQMService.GetTicketsForLocatorRO(const SecSession: string; const LocatorID: Integer; const NumDays: Integer; const OpenOnly: Boolean; const Unacknowledged: Boolean): string;
begin
  {$I before.inc}
  SetName('GetTicketsForLocatorRO');
  GetManageWorkDM;
  Result := ManageWorkDM.GetTicketsForLocatorRO(LocatorID, NumDays, OpenOnly, Unacknowledged);
  {$I after.inc}
end;

function TQMService.GetWorkOrdersForEmployee(const SecSession: string;
  const EmployeeID, NumDays: Integer; const OpenOnly, Unacknowledged: Boolean): string;
{Note that the unacknowledged parameter is included but not used at this time.  It is
being included now in order to avoid a server change in future in case this feature is added.}
begin
{$I before.inc}
  SetName('GetWorkOrdersForEmployee');
  GetManageWorkDM;
  Result := ManageWorkDM.GetWorkOrdersForEmployee(EmployeeID, NumDays, OpenOnly, Unacknowledged);
{$I after.inc}
end;

function TQMService.HasActivityToday(const SecSession,
  Activity: string; const EmpId: Integer): boolean;
begin
  {$I before.inc}
  Result := False;
  GetDM;
  Result := DM.HasActivityToday(Activity, EmpId);  //QM-100 EB
  {$I after.inc}
end;

procedure TQMService.SetPhase(const PhaseMessage: string);
begin
  Assert(Assigned(DM.Op), 'No active Operation defined');
  DM.Op.Phase := PhaseMessage;
end;

procedure TQMService.StartReport(const SecSession, RequestID, ReportParams: string; out WaitSeconds: Integer);
var
  BareParams: TStringList;
  ParamString, RepName: string;
  UserID: Integer;
begin
  {$I before.inc}
  SetName('StartReport');

  BareParams := TStringList.Create;
  try
    BareParams.Delimiter := '|';
    BareParams.DelimitedText := ReportParams;
    UserID := StrToIntDef(BareParams.Values['User'], 0);
    RepName := BareParams.Values['Report'];
    BareParams.Values['RequestID'] := '';
    BareParams.Values['Report'] := '';
    BareParams.Values['User'] := '';
    BareParams.Delimiter := '|';
    ParamString := BareParams.DelimitedText;
  finally
    FreeAndNil(BareParams);
  end;

  with DM.StartReportCommand do begin
    Parameters.ParamValues['request_id'] := RequestID;
    Parameters.ParamValues['report_name'] := RepName;
    Parameters.ParamValues['user_id'] := UserID;
    Parameters.ParamValues['params'] := ParamString;
    Parameters.ParamValues['priority'] := 100;
    Execute;
  end;
  {$I after.inc}
end;

procedure TQMService.CheckReport(const SecSession, RequestID: string;
  out Status: string; out Done: Boolean; out Url: string);
begin
  {$I before.inc}
  SetName('CheckReport');
  DM.CheckReportDataset.Parameters.ParamValues['request_id'] := RequestID;
  DM.CheckReportDataset.Open;
  Done := DM.CheckReportDataset.FieldByName('status').AsString = 'done';
  Url := 'http://localhost/pdf/' + RequestID + '.pdf';
  DM.CheckReportDataset.Close;
  {$I after.inc}
end;

function TQMService.GetEmployeeFloatingHolidays(const SecSession: String;
  const EmployeeID, Year: Integer; const WeekStart, WeekEnd: DateTime): Integer;
begin
  {$I before.inc}
  SetName('EmployeeFloatingHolidays');
  GetTimesheetDM;
  Result := TimesheetDM.GetFloatingHolidayCount(EmployeeID, Year, WeekStart, WeekEnd);
  {$I after.inc}
end;



function TQMService.GetEmpPermission(const SecSession: string;
  const EmpID: Integer; const PermissionStr: string): string;
var
  LimitationStr: string;
  HasRight: Boolean;
begin
  //QM-10 EB
    {$I before.inc}
  SetName('GetEmpPermission');
  LimitationStr := '';
  GetDM;
  HasRight := DM.CanI(PermissionStr, LimitationStr, EmpID);
  if HasRight then
    Result := LimitationStr
  else
    Result := REVOKED;
  {$I after.inc}
end;

function TQMService.GetEmpPlusMgrList(const SecSession: string;
  const MgrID: Integer): string;
begin
{$I before.inc}
  SetName('GetEmpPlusMgrList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetEmpPlusMgrList(MgrID);
  {$I after.inc}
end;

function TQMService.GetEPRHistory(const SecSession: string;
  const TicketID: Integer): string;
begin
  //EB - QMANTWO-338
  {$I before.inc}
  SetName('EPRHistory');
  GetTicketDM;
  Result := TicketDM.GetEPRHistory(TicketID);
  {$I after.inc}
end;




function TQMService.GetRiskHistory(const SecSession: string; const TicketID: Integer): string;
begin
  //QM-387 EB
  {$I before.inc}
  SetName('GetRiskHistory');
  GetTicketDM;
  Result := TicketDM.GetRiskHistory(TicketID);
  {$I after.inc}
end;

procedure TQMService.SaveTicketType(const SecSession: string; const TicketID: Integer; const TicketType: string);  //QM-604 EB Nipsco
begin
  //QM-604 EB Nipsco
  {$I before.inc}
  SetName('SaveTicketType');
  GetTicketDM;
  TicketDM.SaveTicketType(TicketID, TicketType);
  {$I after.inc}
end;

function TQMService.ExportTimesheets(const SecSession, ProfitCenter: string; const WeekEnding: TDateTime): string;
begin
  {$I before.inc}
  SetName('ExportTimesheets');
  GetTimesheetDM;
  Result := TimesheetDM.ExportTimesheets(ProfitCenter, WeekEnding);
  {$I after.inc}
end;

function TQMService.GetColumnData(const SecSession, TableName, KeyField, Columns: string;
  const UpdatesSince: DateTime; const NonNullOnly: Boolean): string;
const
  SelectSQL = 'select top 10000 ''%s'' as tablename, %s, %s from %s where 1=1 ';
  ModifiedSQL = 'and modified_date > %s ';
  NullSQL = ' %s is not null ';
  MaxRows = 9900;
var
  SQL: string;
  Fields: TStringList;
  FieldStr: string;
  i: Integer;
  Dataset: TDataSet;
begin
  {$I before.inc}
  SetName('GetColumnData');
  Fields := TStringList.Create;
  try
    SetPhase('Generating SQL');
    FieldStr := StringReplace(Columns, ',', ';', [rfReplaceAll]);
    StrToStrings(FieldStr, ';', Fields, False);
    Assert(Fields.Count > 0);
    for i := 0 to Fields.Count - 1 do
      Assert(IsValidIdent(Fields[i]));
    Assert(IsValidIdent(TableName));
    Assert(IsValidIdent(KeyField));
    SQL := Format(SelectSQL, [TableName, KeyField, StringReplace(FieldStr, ';', ',', [rfReplaceAll]), TableName]);
    if UpdatesSince > 1 then
      SQL := SQL + Format(ModifiedSQL, [IsoDateToStrQuoted(UpdatesSince)]);
    if NonNullOnly then begin
      SQL := SQL + 'and (';
      for i := 0 to Fields.Count - 1 do begin
        if i = 0 then
          SQL := SQL + Format(NullSQL, [Fields[i]])
        else
          SQL := SQL + ' or ' + Format(NullSQL, [Fields[i]]);
      end;
      SQL := SQL + ')';
    end;
    SetPhase('Executing SQL');
    DataSet := DM.GetDataSetForSQL(SQL);
    SetPhase('Generating XML result');
    Result := FormatSqlXml(DataSet as TCustomADODataSet, 1, nil, MaxRows);
  finally
    FreeAndNil(Fields);
  end;
  {$I after.inc}
end;

function TQMService.SaveNewTickets(const SecSession: string; const Tickets: NewTicketList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveNewTickets');
  GetTicketDM;
  Result := TicketDM.SaveNewTickets(Tickets);
  {$I after.inc}
end;

procedure TQMService.SaveOtherTicketInfoChanges(const SecSession: string;
  const TicketInfoChanges: TicketInfoChangeList; out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('SaveOtherTicketInfoChanges');
  GetTicketDM;
  TicketDM.SaveOtherTicketInfoChanges(TicketInfoChanges, GeneratedKeys );
  {$I after.inc}
end;

function TQMService.BillingAdjustmentSearch(const SecSession: string; const Params: NVPairList): string;
begin
  {$I before.inc}
  SetName('BillingAdjustmentSearch');
  GetBillingDM;
  Result := BillingDM.BillingAdjustmentSearch(Params);
  {$I after.inc}
end;

function TQMService.GetBillingAdjustment(const SecSession: string;
  const AdjustmentID: Integer): string;
begin
  {$I before.inc}
  SetName('GetBillingAdjustment');
  GetBillingDM;
  Result := BillingDM.GetBillingAdjustment(AdjustmentID);
  {$I after.inc}
end;

function TQMService.SaveBillingAdjustments(const SecSession: string;
  const BillingAdjustments: NVPairListList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveBillingAdjustments');
  GetBillingDM;
  Result := BillingDM.SaveBillingAdjustments(BillingAdjustments);
  {$I after.inc}
end;

procedure TQMService.StartBilling(const SecSession: string; const Params: NVPairList);
begin
  {$I before.inc}
  SetName('StartBilling');
  GetBillingDM;
  BillingDM.StartBilling(Params);
  {$I after.inc}
end;

function TQMService.BillingRunSearch(const SecSession: string; const Params: NVPairList): string;
begin
  {$I before.inc}
  SetName('BillingRunSearch');
  GetBillingDM;
  Result := BillingDM.BillingRunSearch(Params);
  {$I after.inc}
end;



function TQMService.BillingHeaderSearch(const SecSession: string; const Params: NVPairList): string;
begin
  {$I before.inc}
  SetName('BillingHeaderSearch');
  GetBillingDM;
  Result := BillingDM.BillingHeaderSearch(Params);
  {$I after.inc}
end;

function TQMService.BillingAction(const SecSession: string; const Params: NVPairList): string;
begin
  Result := '';
  {$I before.inc}
  SetName('BillingAction');
  GetBillingDM;
  BillingDM.BillingAction(Params);
  {$I after.inc}
end;

function TQMService.GetBillingRunLog(const SecSession: string; const Params: NVPairList): string;
begin
  Result := '';
  {$I before.inc}
  SetName('GetBillingRunLog');
  GetBillingDM;
  Result := BillingDM.GetBillingRunLog(Params);
  {$I after.inc}
end;

function TQMService.GetBreadCrumb(const SecSession: string;
  const empID: integer): string;
begin
  {$I before.inc}
  SetName('GetBreadCrumb');
  GetDM;
  result := DM.GetBreadCrumb(empID);
  {$I after.inc}
end;

function TQMService.GetBreadCrumbList(const SecSession: string; const MgrID: integer): string;     //QMANTWO-302
begin
  {$I before.inc}
  SetName('GetBreadCrumbList');
  GetDM;
  result := DM.GetBreadCrumbList(MgrID);
  {$I after.inc}
end;

procedure TQMService.CancelBillingRun(const SecSession: string; const Params: NVPairList);
begin
  {$I before.inc}
  SetName('CancelBillingRun');
  GetBillingDM;
  BillingDM.CancelBillingRun(Params);
  {$I after.inc}
end;

function TQMService.CanI(Right: string; var LimitationString: string): Boolean;
begin
  Result := DM.CanI(Right, LimitationString, LoggedInEmpID);
end;

function TQMService.SaveDamageBill(const SecSession: string; const Params: NVPairListList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveDamageBill');
  GetDamageDM;
  Result := DamageDM.SaveDamageBill(Params);
  {$I after.inc}
end;

function TQMService.SaveDamage3(const SecSession: string;
  const Params: NVPairListList;
  out Errors: SyncErrorList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveDamage3');
  GetDamageDM;
  Result := DamageDM.SaveDamage3(Params, Errors);
  {$I after.inc}
end;

function TQMService.ApproveDamage(const SecSession: String; const DamageID: Integer; const ApprovedDateTime: DateTime): String;
begin
  Result := '';
  {$I before.inc}
  SetName('ApproveDamage');
  GetDamageDM;
  Result := DamageDM.ApproveDamage(DamageID, ApprovedDateTime);
  {$I after.inc}
end;

procedure TQMService.StatusWorkOrders2(const SecSession: string;
  const WorkOrderChanges: WorkOrderChangeList2;
  out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('StatusWorkOrders2');
  GetWorkOrderDM;
  WorkOrderDM.StatusWorkOrders2(WorkOrderChanges, GeneratedKeys);
  {$I after.inc}
end;

procedure TQMService.StatusWorkOrders5(const SecSession: string;
  const WorkOrderChanges: WorkOrderChangeList5;
  out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('StatusWorkOrders5');
  GetWorkOrderDM;
  WorkOrderDM.StatusWorkOrders5(WorkOrderChanges, GeneratedKeys);
  {$I after.inc}
end;

procedure TQMService.SaveWorkOrdersOHM(const SecSession: string;
  const WorkOrderOHMChanges: WorkOrderOHMChangeList);
begin
    {$I before.inc}
  SetName('SaveWorkOrdersOHM');
  GetWorkOrderDM;
  WorkOrderDM.SaveWorkOrdersOHM(WorkOrderOHMChanges);
  {$I after.inc}
end;

procedure TQMService.StatusWorkOrderInspections(const SecSession: string;
  const WOInspectionChanges: WOInspectionChangeList;
  out GeneratedKeys: GeneratedKeyList);
begin
  {$I before.inc}
  SetName('StatusWorkOrderInspections');
  GetWorkOrderDM;
  WorkOrderDM.StatusWorkOrderInspections(WOInspectionChanges, GeneratedKeys);
  {$I after.inc}
end;

function TQMService.DamageThirdPartySearch(const SecSession: string;
  const UQDamageID, ThirdPartyID, CarrierID: Integer; const Claimant, Address,
  City, State, ClaimStatus, ProfitCenter, UtilityCoDamaged: string;
  const NotifiedDateFrom, NotifiedDateTo: DateTime): string;
begin
  {$I before.inc}
  SetName('DamageThirdPartySearch');
  GetDamageDM;
  Result := DamageDM.DamageThirdPartySearch(UQDamageID, ThirdPartyID, CarrierID,
    Claimant, Address, City, State, ClaimStatus, ProfitCenter, UtilityCoDamaged,
    NotifiedDateFrom, NotifiedDateTo);
  {$I after.inc}
end;

function TQMService.SaveDamageThirdParty(const SecSession: string;
  const Params: NVPairListList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveDamageThirdParty');
  GetDamageDM;
  Result := DamageDM.SaveDamageThirdParty(Params);
  {$I after.inc}
end;

function TQMService.SaveDamageEstimate2(const SecSession: string;
  const Params: NVPairListList; out Errors: SyncErrorList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveDamageEstimate2');
  GetDamageDM;
  Result := DamageDM.SaveDamageEstimate2(Params, Errors);
  {$I after.inc}
end;

function TQMService.SaveDamageInvoice(const SecSession: string;
  const Params: NVPairListList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveDamageInvoice');
  GetDamageDM;
  Result := DamageDM.SaveDamageInvoice(Params);
  {$I after.inc}
end;

function TQMService.SaveDamageLitigation(const SecSession: string;
  const Params: NVPairListList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveDamageLitigation');
  GetDamageDM;
  Result := DamageDM.SaveDamageLitigation(Params);
  {$I after.inc}
end;

//AckMessage2 introduced in build 7614
procedure TQMService.AckMessage2(const SecSession: string;
  const MessageAcks: NVPairListList; out ReturnValue: string);
const
  AckMessageSQL = 'update message_dest set ack_date=%s ' +
    'where message_dest_id=%d and ack_date is null';
var
  i: Integer;
  MessageAck: NVPairList;
  MessageID: Integer;
  AckDate: TDateTime;
begin
  {$I before.inc}
  SetName('AckMessage2');
  Assert(Assigned(MessageAcks));
  ReturnValue := '';

  //TODO: Add additional sanity check to be sure message_dest is for the current user
  for i := 0 to MessageAcks.Count - 1 do begin
    SetPhase(Format('Setting ack date on message %d of %d', [i+1, MessageAcks.Count]));
    MessageAck := MessageAcks[i];
    Assert(Assigned(MessageAck));
    MessageID := NVPairInteger(MessageAck, 'message_dest_id', bvError);
    AckDate := NVPairDateTime(MessageAck, 'ack_date', bvError);
    DM.ExecuteSQL(Format(AckMessageSQL, [QuoteDate(AckDate), MessageID]));
  end;
  {$I after.inc}
end;

function TQMService.GetMessageRecip(const SecSession: string; const MessageID: Integer): string;
begin
  {$I before.inc}
  SetName('GetMessageRecip');
  Result := DM.GetSingleParamSPAsXML('dbo.get_message_recip2',
    '@MessageID', MessageID);
  {$I after.inc}
end;

function TQMService.MessageSearch(const SecSession: string; const Params: NVPairList): string;
const
  SelectByMessage ='select m.message_id, m.message_number, m.from_emp_id, e.short_name as SenderName, '+ //QM-22
  'm.destination, m.subject, m.sent_date, m.show_date, m.expiration_date, m.active, count(*) as sent_to_count, '+
  'count(ack_date) as ack_count  '+
  'from message m  '+
  'inner join message_dest md on m.message_id=md.message_id  '+
  'inner join employee e on e.emp_id = m.from_emp_id ';  //QM-22
  SelectByEmployee = 'select md.*, m.message_number, m.from_emp_id, m.destination, m.subject, ' +
    'm.sent_date, m.show_date, m.expiration_date, m.active, e.short_name as recipient_name ' +
    'from message_dest md inner join message m on m.message_id=md.message_id ' +
    'inner join employee e on e.emp_id=md.emp_id ';
  GroupBy = ' group by m.message_id, m.message_number, m.from_emp_id, e.short_name, m.destination, ' + //QM-22
    'm.subject, m.sent_date, m.show_date, m.expiration_date, m.active';
var
  SQL: TOdSqlBuilder;
  MessageNumber: string;
  Destination: string;
  Subject: string;
  SentDateStart: TDateTime;
  SentDateEnd: TDateTime;
  ShowDateStart: TDateTime;
  ShowDateEnd: TDateTime;
  RecipientName: string;
  SenderName: string;   //QM-22
  ShowInactive: Boolean;
begin
  {$I before.inc}
  SetName('MessageSearch');

  SQL := TOdSqlBuilder.Create;
  try
    SentDateStart := NVPairDateTime(Params, 'sent_date_start', bvZero);
    SentDateEnd := NVPairDateTime(Params, 'sent_date_end', bvZero);
    ShowDateStart := NVPairDateTime(Params, 'show_date_start', bvZero);
    ShowDateEnd := NVPairDateTime(Params, 'show_date_end', bvZero);
    MessageNumber := NVPairString(Params, 'message_number', bvDefault);
    Destination := NVPairString(Params, 'destination', bvDefault);
    Subject := NVPairString(Params, 'subject', bvDefault);
    RecipientName := NVPairString(Params, 'recipient_name', bvDefault);
    SenderName := NVPairString(Params, 'sender_name', bvDefault);  //QM-22
    ShowInactive := NVPairBoolean(Params, 'show_inactive', bvZero);

    SQL.AddDateTimeRangeFilter('m.sent_date', SentDateStart, SentDateEnd);
    SQL.AddDateTimeRangeFilter('m.show_date', ShowDateStart, ShowDateEnd);
    SQL.AddKeywordSearch('m.message_number', MessageNumber);
    SQL.AddKeywordSearch('m.subject', Subject);
    SQL.AddExactStringFilter('m.destination', Destination);
    SQL.AddKeywordSearch('e.short_name', RecipientName);
    if not ShowInactive then
      SQL.AddBooleanSearch('m.active', True);
    SQL.CheckFilterCount;
    if Length(RecipientName) = 0 then
      DM.SearchQuery.CommandText := SelectByMessage + SQL.WhereClause + GroupBy
    else
      DM.SearchQuery.CommandText := SelectByEmployee + SQL.WhereClause;
    SetPhase('Running query');
    DM.Op.Detail := SQL.WhereClause;
    DM.SearchQuery.Open;

    SetPhase('Encoding results');
    Result := ConvertDataSetToXML([DM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    DM.SearchQuery.Close;
  end;
  {$I after.inc}
end;

function TQMService.SaveMessage(const SecSession: string; const MessageData: MessageChange): string;
const
  ExistingMsg = 'select * from message where message_number=''%s''';
  InsertMsgDest = 'insert into message_dest (message_id, emp_id) select %d, emp_id ' +
    'from employee where emp_id in (%s)';
var
  M: TADODataSet;
  LimitationString: string;
  StartingIndex, EmpIndex, EmpsToInsert: Integer;
  MessageNum, BadEmpName: string;
  ParamPair: NVPairList;
  NewMessageID, BadEmpID: Integer;
  SendToEmps: string;
  StartingTime, EndingTime: Cardinal;
  EmpArray: TIntegerArray;
const
  INSERT_AT_ONCE = 50;
begin
  {$I before.inc}
  SetName('SaveMessage');
  StartingTime := GetTickCount;
  if not CanI(RightMessagesSend, LimitationString) then
    raise EOdNotAllowed.Create('You do not have permission to send messages.');

  Assert(MessageData.MessageDestEmpIds.Count > 0, 'No employees in the message recipient list.');
  EmpArray := IntegerListToArray(MessageData.MessageDestEmpIds);
  if not DM.CanIManageEmployees(LimitationString, EmpArray, True, BadEmpID, BadEmpName) then begin
    raise EOdNotAllowed.CreateFmt('You do not have permission to send a message to employee %s ', [BadEmpName]);
  end;

  Result := '';
  M := DM.Messages;
  ParamPair := MessageData.MessageParams;
  MessageNum := NVPairString(ParamPair, 'message_number', bvError);
  M.Close;
  M.CommandText := Format(ExistingMsg, [MessageNum]);
  SetPhase('Checking for existing message');
  M.Open;
  if M.IsEmpty then begin
    SetPhase('Inserting new message');
    M.Insert;
    M.FieldByName('message_number').AsString := MessageNum;
    M.FieldByName('from_emp_id').AsInteger := LoggedInEmpID;
    M.FieldByName('sent_date').AsDateTime := Now;
    M.FieldByName('show_date').AsDateTime := NVPairDateTime(ParamPair, 'show_date', bvError);
    M.FieldByName('destination').AsString := NVPairString(ParamPair, 'destination', bvError);
    M.FieldByName('subject').AsString := NVPairString(ParamPair, 'subject', bvError);
    M.FieldByName('body').AsString := NVPairString(ParamPair, 'body', bvError);
    M.FieldByName('expiration_date').AsDateTime := NVPairDateTime(ParamPair, 'expiration_date', bvError);
    M.Post;
  end;

  SetPhase('Getting message id');
  NewMessageID := M.FieldByName('message_id').AsInteger;
  Assert(NewMessageID > 0, 'Negative new message ID');

  Assert(MessageData.MessageDestEmpIds.Count > 0, 'No employees in the message recipient list.');
  StartingIndex := 0;
  SetPhase('Inserting message destinations');

  while StartingIndex < MessageData.MessageDestEmpIds.Count do begin
    EmpsToInsert := MessageData.MessageDestEmpIds.Count - StartingIndex;
    if EmpsToInsert > INSERT_AT_ONCE then
      EmpsToInsert := INSERT_AT_ONCE;

    SendToEmps := '';
    for EmpIndex := 0 to EmpsToInsert - 1 do
      SendToEmps := SendToEmps + ',' +
        IntToStr(MessageData.MessageDestEmpIds.Items[StartingIndex + EmpIndex]);
    Delete(SendToEmps, 1, 1);  // trim initial comma
    FExecuteRecsAffected := DM.ExecuteSQL(Format(InsertMsgDest, [NewMessageID, SendToEmps]));
    if FExecuteRecsAffected <> EmpsToInsert then
      DM.Op.Log(Format('Warning, expected to insert %d destinations, inserted %d.  (%s)',
      [EmpsToInsert, FExecuteRecsAffected, SendToEmps] ));

    StartingIndex := StartingIndex + INSERT_AT_ONCE;
  end;

  EndingTime := GetTickCount;
  Result := Format('Message sent to %d %s in %5.1f seconds', [MessageData.MessageDestEmpIds.Count,
    AddSIfNot1(MessageData.MessageDestEmpIds.Count, 'user'),
    1.0 * (EndingTime - StartingTime) / 1000.0]);
  {$I after.inc}
end;



function TQMService.SaveNewNotes2(const SecSession: string; const NotesChanges: NotesChangeList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveNewNotes2');
  Assert(Assigned(NotesChanges));
  GetNotesDM;
  Result := NotesDM.SaveNewNotesInternal(NotesChanges);
  {$I after.inc}
end;

function TQMService.SaveNewNotes5(const SecSession: string;
  const NotesChanges: NotesChangeList5): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveNewNotes5');
  Assert(Assigned(NotesChanges));
  GetNotesDM;
  Result := NotesDM.SaveNewNotesInternal5(NotesChanges);
  {$I after.inc}
end;

function TQMService.SaveEmployeeActivity2(const SecSession: string; const EmpActivityChanges: EmployeeActivityChangeList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveEmployeeActivity2');
  Assert(Assigned(EmpActivityChanges));
  Result := SaveEmployeeActivityInternal(EmpActivityChanges);
  {$I after.inc}
end;

function TQMService.SaveEmployeeActivity3(const SecSession: string;
  const EmpAcitivtyChanges3: EmployeeActivityChangeList3): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveEmployeeActivity3');
  Assert(Assigned(EmpAcitivtyChanges3));
  Result := SaveEmployeeActivityInternal3(EmpAcitivtyChanges3);
  {$I after.inc}
end;

function TQMService.SaveEmployeeActivityInternal(Activities: EmployeeActivityChangeList): GeneratedKeyList;
const
  Existing = 'select * from employee_activity ';
var
  i: Integer;
  Activity: EmployeeActivityChange;
  EmpID: Integer;
  OldActID: Integer;
  NewActID: Integer;
  ActType: string;
  ActDetails: Variant;
  ActDate: TDateTime;
  ExtraDetails: Variant;

  D: TADODataSet;
  SQL: TOdSqlBuilder;
begin
  Assert(Assigned(Activities));
  Result := GeneratedKeyList.Create;

  D := DM.EmployeeActivity;
  for i := 0 to Activities.Count - 1 do begin
    Activity := Activities[i];
    Assert(Assigned(Activity));

    OldActID := Activity.EmpActivityID;
    EmpID := Activity.EmpID;
    ActDate := Activity.ActivityDate;
    ActType := Activity.ActivityType;


    if IsEmpty(Activity.Details) then
      ActDetails := Null
    else
      ActDetails := Activity.Details;
    if IsEmpty(Activity.ExtraDetails) then
      ExtraDetails := Null
    else
      ExtraDetails := Activity.ExtraDetails;

    SQL := TOdSqlBuilder.Create;
    try
      D.Close;
      SQL.AddIntFilterRequirePositive('emp_id', EmpID);
      SQL.AddExactStringFilterNonEmpty('activity_type', ActType);
      SQL.AddExactStringFilterAllowNull('details', ActDetails);
      SQL.AddExactStringFilterAllowNull('extra_details', ExtraDetails);
      SQL.AddISODateFilterNonNull('activity_date', ActDate);
      D.CommandText := Existing + SQL.WhereClause;
      D.Open;
    finally
      FreeAndNil(SQL);
    end;

    if D.RecordCount < 1 then begin
      D.Insert;
      D.FieldByName('emp_id').AsInteger := EmpID;
      D.FieldByName('activity_date').AsDateTime := ActDate;
      D.FieldByName('activity_type').AsString := ActType;
      D.FieldByName('details').Value := ActDetails;
      D.FieldByName('extra_details').Value := ExtraDetails;
      D.Post;
      NewActID := D.FieldByName('emp_activity_id').AsInteger;
      DM.UpdateDateTimeToIncludeMilliseconds('employee_activity', 'emp_activity_id', NewActID, 'activity_date', ActDate);

      // TODO: better way to get the activity_type codes that require manager notification
      if (ActType = ActivityTypeStartTimeOverride) or (ActType = ActivityTypeLunchBreakException) then begin  // QMANTWO-235
        NotifyManagerOfEmployeeActivity(ActType, EmpID, ActDate, Activity.Details, Activity.ExtraDetails);   // QMANTWO-235
      end;
    end;

    NewActID := D.FieldByName('emp_activity_id').AsInteger;
    Assert(NewActID > 0, 'Negative new activity ID');
    AddIdentity(Result, 'employee_activity', OldActID, NewActID);
  end;
end;

function TQMService.SaveEmployeeActivityInternal3(
  Activities: EmployeeActivityChangeList3): GeneratedKeyList;
const
  Existing = 'select * from employee_activity ';
var
  i: Integer;
  Activity: EmployeeActivityChange3;
  EmpID: Integer;
  OldActID: Integer;
  NewActID: Integer;
  ActType: string;
  ActDetails: Variant;
  ActDate: TDateTime;
  ExtraDetails: Variant;
  Lat: Double;
  Lng: Double;
  D: TADODataSet;
  SQL: TOdSqlBuilder;
begin
  Assert(Assigned(Activities));
  Result := GeneratedKeyList.Create;

  D := DM.EmployeeActivity;
  for i := 0 to Activities.Count - 1 do begin
    Activity := Activities[i];
    Assert(Assigned(Activity));

    OldActID := Activity.EmpActivityID;
    EmpID := Activity.EmpID;
    ActDate := Activity.ActivityDate;
    ActType := Activity.ActivityType;
    Lat := Activity.Lat;
    Lng := Activity.Lng;

    if IsEmpty(Activity.Details) then
      ActDetails := Null
    else
      ActDetails := Activity.Details;
    if IsEmpty(Activity.ExtraDetails) then
      ExtraDetails := Null
    else
      ExtraDetails := Activity.ExtraDetails;

    SQL := TOdSqlBuilder.Create;
    try
      D.Close;
      SQL.AddIntFilterRequirePositive('emp_id', EmpID);
      SQL.AddExactStringFilterNonEmpty('activity_type', ActType);
      SQL.AddExactStringFilterAllowNull('details', ActDetails);
      SQL.AddExactStringFilterAllowNull('extra_details', ExtraDetails);
      SQL.AddISODateFilterNonNull('activity_date', ActDate);
      D.CommandText := Existing + SQL.WhereClause;
      D.Open;
    finally
      FreeAndNil(SQL);
    end;

    if D.RecordCount < 1 then begin
      D.Insert;
      D.FieldByName('emp_id').AsInteger := EmpID;
      D.FieldByName('activity_date').AsDateTime := ActDate;
      D.FieldByName('activity_type').AsString := ActType;
      D.FieldByName('details').Value := ActDetails;
      D.FieldByName('extra_details').Value := ExtraDetails;
      D.FieldByName('lat').Value := Lat;
      D.FieldByName('lng').Value := Lng;
      D.Post;
      NewActID := D.FieldByName('emp_activity_id').AsInteger;
      DM.UpdateDateTimeToIncludeMilliseconds('employee_activity', 'emp_activity_id', NewActID, 'activity_date', ActDate);

      // TODO: better way to get the activity_type codes that require manager notification

      if (ActType = ActivityTypeStartTimeOverride) or (ActType = ActivityTypeLunchBreakException) then begin  // QMANTWO-235
        NotifyManagerOfEmployeeActivity(ActType, EmpID, ActDate, Activity.Details, Activity.ExtraDetails);    // QMANTWO-235

      end;
    end;

    NewActID := D.FieldByName('emp_activity_id').AsInteger;
    Assert(NewActID > 0, 'Negative new activity ID');
    AddIdentity(Result, 'employee_activity', OldActID, NewActID);
  end;
end;

procedure TQMService.UpdateEmployeePlatsFromHistory(const SecSession: string;
  const EmpID: Integer; const UpdateDate: TDateTime);
begin
 {$I before.inc}
  SetName('UpdateEmployeePlatsFromHistory');
  GetTicketDM;
  DM.UpdateEmployeePlatsUpdated(EmpID, UpdateDate);
  {$I after.inc}
end;

procedure TQMService.UpdateEmployeePlatsUpdated(const SecSession: string;
  const EmpID: Integer);
begin
 {$I before.inc}
  SetName('UpdateEmployeePlatsUpdated');
  GetTicketDM;
  DM.UpdateEmployeePlatsUpdated(EmpID, NOW);
  {$I after.inc}
end;

procedure TQMService.UpdateFirstTaskReminder(const SecSession: string;  //QM-494 First Task Reminder EB
  const EmpID: integer; const ReminderTime: string);
begin
 {$I before.inc}
 SetName('UpdateFirstTaskReminder');
 GetDM;
 DM.UpdateFirstTaskReminder(EmpID, ReminderTime);
 {$I after.inc}
end;

function TQMService.GetFirstTaskReminder(const SecSession: string; //QM-494 First Task Reminder EB
  const EmpID: integer): string;
begin
 {$I before.inc}
 SetName('GetFirstTaskReminder');
 GetDM;
 Result := DM.GetFirstTaskReminder(EmpID);
 {$I after.inc}
end;

procedure TQMService.UpdatePhoneNo(const SecSession: string;     //qm-388 sr
  const EmpID: integer; const NewPhoneNo: string);
begin
 {$I before.inc}
//qm-388 sr      procedure TTicketDM.UpdatePhoneNo(EmpID: integer; NewPhoneNo: string);
  SetName('UpdatePhoneNo');
  GetTicketDM;
  DM.UpdatePhoneNo(EmpID, NewPhoneNo);
  {$I after.inc}
end;

procedure TQMService.UpdateRestrictedUseExemption(const SecSession: string; const Exemptions: NVPairListList);
const
  GrantSQL = 'execute dbo.grant_restricted_use_exemption %d, %d, %s, %d';
var
  i: Integer;
  Exemption: NVPairList;
  EmpID: Integer;
  Revoke: Boolean;
  EffectiveDate: TDateTime;
begin
  {$I before.inc}
  SetName('UpdateRestrictedUseExemption');
  Assert(Assigned(Exemptions));
  for i := 0 to Exemptions.Count - 1 do begin
    Exemption := Exemptions[i];
    Assert(Assigned(Exemption));

    EmpID := NVPairInteger(Exemption, 'emp_id', bvError);
    EffectiveDate := NVPairDateTime(Exemption, 'effective_date', bvError);
    Revoke := NVPairBoolean(Exemption, 'revoke', bvError);
    if Revoke then begin
      SetPhase('Revoke exemption for emp # ' + IntToStr(EmpID));
      DM.ExecuteSQL(Format(GrantSQL, [EmpID, LoggedInEmpID, QuoteDate(EffectiveDate), 0]));
    end else begin
      SetPhase('Grant exemption for emp # ' + IntToStr(EmpID));
      DM.ExecuteSQL(Format(GrantSQL, [EmpID, LoggedInEmpID, QuoteDate(EffectiveDate), 1]));
    end;
  end;
  {$I after.inc}
end;


function TQMService.SaveJobsiteArrivals2(const SecSession: string; const JobsiteArrivalChanges: JobsiteArrivalChangeList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveJobsiteArrivals2');
  Assert(Assigned(JobsiteArrivalChanges));
  Result := GeneratedKeyList.Create;
  GetArrivalDM;
  ArrivalDM.ProcessJobsiteArrivalChanges(JobsiteArrivalChanges, Result);
  {$I after.inc}
end;

function TQMService.GetAttachmentList(const SecSession: string; const ForeignType: Integer; const ForeignID: Integer; const ImagesOnly: Boolean): string;
const
  SQL = 'select * from attachment where upload_date is not null and foreign_type = %d and foreign_id = %d and active = 1 %s';
var
  Dataset: TDataset;
begin
  {$I before.inc}
  SetName('GetAttachmentList');
  if ImagesOnly then
    Dataset := DM.GetDataSetForSQL(Format(SQL, [ForeignType, ForeignID, ' and extension in (''.bmp'',''.jpg'',''.jpeg'',''.gif'')']))
  else
    Dataset := DM.GetDataSetForSQL(Format(SQL, [ForeignType, ForeignID, '']));
  Result := ConvertDataSetToXML([Dataset], 0);
  {$I after.inc}
end;

function TQMService.GetTicketID(const SecSession: string; const TicketNumber: string; const CallCenter: string;
  const TransmitDateFrom, TransmitDateTo: DateTime; const DamageId: Integer): Integer;
const
  SQLTicket = 'select ticket_id from ticket where ticket_number = %s and ticket_format = %s and transmit_date between %s and %s';
  SQLDamage = 'select ticket_id from damage where damage_id = %d';
  SQLUpdateTicket = 'select ticket_id from ticket where ticket_number = %s and ticket_format = %s'; //qm-397 sr
var
  Dataset: TDataset;
  iDate1, iDate2:extended;
begin
  Result := 0;
  iDate1:=  DateOf(TransmitDateFrom);
  iDate2 := DateOf(TransmitDateTo) ;
  {$I before.inc}
  SetName('GetTicketId');
  if ((DamageId = 0) and (iDate1 > 0.0)) then
    Dataset := DM.GetDataSetForSQL(Format(SQLTicket, [QuotedStr(TicketNumber), QuotedStr(CallCenter), QuotedStr(DateToStr(TransmitDateFrom)), QuotedStr(DateToStr(TransmitDateTo))]))
  else
  if ((DamageId = 0) and (iDate1 + iDate2 = 0.0)) then   //qm-397 sr
    Dataset := DM.GetDataSetForSQL(Format(SQLUpdateTicket, [QuotedStr(TicketNumber), QuotedStr(CallCenter)]))   //qm-397 sr
  else
    Dataset := DM.GetDataSetForSQL(Format(SQLDamage, [DamageId]));
  if Dataset.RecordCount > 0 then
    Result := Dataset.FieldByName('ticket_id').AsInteger;
  {$I after.inc}
end;

function TQMService.GetTicketLocateList(const SecSession: string;
  const TicketID: integer): string;
begin
  Result := '';
  {$I before.inc}
  SetName('GetTicketLocateList');
  GetTicketDM;
  result := TicketDM.GetTicketLocateList(TicketID);
  {$I after.inc}
end;

function TQMService.GetDamageIDForUQDamageID(const SecSession: string; const UQDamageID: Integer): Integer;
begin
  Result := 0;
  {$I before.inc}
  SetName('GetDamageIDForUQDamageID');
  GetDamageDM;
  Result := DamageDM.GetDamageIDForUQDamageID(UQDamageID);
  if Result = -1 then
    Result := 0;
  {$I after.inc}
end;

procedure TQMService.SaveTicketDueDate(const SecSession: string;
  const TicketID: Integer; const NewDueDate: TDateTime);  //QM-216 EB Due Date
begin
  {$I before.inc}
  SetName('SaveTicketDueDate');
  GetTicketDM;
  TicketDM.SaveTicketDueDate(TicketID, NewDueDate);
  {$I after.inc}
end;


function TQMService.BulkChangeTicketDueDates(const SecSession: string;
  const TicketIDs: string; const NewDueDate: TDateTime): string;
begin
  {$I before.inc}
  SetName('BulkChangeTicketDueDates');
  GetDM;
  GetTicketDM;
  Result := TicketDM.BulkChangeTicketDueDates(TicketIDs, NewDueDate);
  {$I after.inc}
end;

procedure TQMService.SaveTicketPriority(const SecSession: string; const TicketID: Integer;
  const EntryDate: TDateTime; const Priority: Integer);
begin
  {$I before.inc}
  SetName('SaveTicketPriority');
  GetTicketDM;
  TicketDM.SaveTicketPriority(TicketID, EntryDate, Priority);
  {$I after.inc}
end;

procedure TQMService.SaveTicketTypeHP(const SecSession: String;     //qm-373 SR
  const TicketID: Integer; const NewTicketType: String);
begin
  {$I before.inc}
  SetName('SaveTicketTypeHP');
  GetTicketDM;
  TicketDM.SaveTicketTypeHP(TicketID, NewTicketType);
  {$I after.inc}
end;

procedure TQMService.SaveEmployeeTicketViewLimit(const SecSession: string;
  const EmployeeID, TicketViewLimit: Integer);
begin
  {$I before.inc}
  SetName('SaveEmployeeTicketViewLimit');
  GetManageWorkDM;
  ManageWorkDM.SaveEmployeeTicketViewLimit(EmployeeID, TicketViewLimit);
  {$I after.inc}
end;

function TQMService.CanIDef(Right: string; var LimitationString: string;
  const DefaultLimitation: string): Boolean;
begin
  Result := CanI(Right, LimitationString);
  if Result and IsEmpty(LimitationString) or (Trim(LimitationString) = '-') then
    LimitationString := DefaultLimitation;
end;

procedure TQMService.SaveAreas2(const SecSession: string; const AreaChanges: AreaChangeList);
var
  i: Integer;
  Area: AreaChange;
  AreaID: Integer;
  LocatorID: Integer;
  D: TADODataSet;
begin
  {$I before.inc}
  SetName('SaveAreas2');
  Assert(Assigned(AreaChanges));

  D := DM.Area;
  for i := 0 to AreaChanges.Count - 1 do begin
    Area := AreaChanges[i];
    Assert(Assigned(Area));

    AreaID := Area.AreaID;
    LocatorID := Area.LocatorID;
    Assert(AreaID > 0);
    Assert(LocatorID > 0);

    D.Close;
    D.CommandText := 'select area_id, locator_id from area where area_id = ' + IntToStr(AreaID);
    D.Open;
    if D.IsEmpty then
      raise Exception.CreateFmt('Area ID %d not found to edit', [AreaID]);
    D.Edit;
    D.FieldByName('locator_id').AsInteger := LocatorID;
    D.Post;

    DM.ExecuteSQL(Format('insert into area_change (area_id, changed_by, locator_id) values (%d, %d, %d)', [AreaID, LoggedInEmpID, LocatorID]));
  end;
  {$I after.inc}
end;



function TQMService.GetPrintableAttachmentList(const SecSession: string; const ForeignType: Integer; const ForeignID: Integer): string;
const
  SQL = 'select * ' +
        'from attachment a '+
        '  inner join reference r on r.type = ''%s'' and a.extension = r.code '+
        'where upload_date is not null and foreign_type = %d and foreign_id = %d and a.active = 1 and r.active_ind = 1';
var
  Dataset: TDataset;
begin
  {$I before.inc}
  SetName('GetPrintableAttachmentList');
  Dataset := DM.GetDataSetForSQL(Format(SQL, [ReferenceTypePrintableFile, ForeignType, ForeignID, '']));
  Result := ConvertDataSetToXML([Dataset], 0);
  {$I after.inc}
end;

function TQMService.SaveAddinInfo2(const SecSession: string; const AddinInfoChanges: AddinInfoChangeList): GeneratedKeyList;
begin
  {$I before.inc}
  SetName('SaveAddinInfo2');
  Assert(Assigned(AddinInfoChanges));
  Result := GeneratedKeyList.Create;
  GetAddinInfoDM;
  AddinInfoDM.ProcessAddinInfoChanges(AddinInfoChanges, Result);
  {$I after.inc}
end;

function TQMService.GetPastDueList(const SecSession: string; const ManagerID,
  MaxDays: Integer): string;   //QM-133 EB Past Due
begin
 {$I before.inc}
  SetName('GetPasDueList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetPastDueList(ManagerID, MaxDays);
  {$I after.inc}
end;



function TQMService.GetTodayList(const SecSession: string; const ManagerID: Integer; const MaxDays: Integer): string;  //QM-318 EB;
begin
 {$I before.inc}
  SetName('GetTodayList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetTodayList(ManagerID, MaxDays);
  {$I after.inc}
end;

function TQMService.GetVirtualBucket(const SecSession: string; const ManagerID: integer; const BucketName: string): string; //QM-486 Virtual Bucket EB
begin
  {$I before.inc}
  SetName('GetVirtualBucket');
  GetManageWorkDM;
  Result := ManageWorkDM.GetVirtualBucket(ManagerID, BucketName);
  {$I after.inc}
end;

function TQMService.GetMARSEmpList(const SecSession: string;
  const MgrID: integer): string;  //QM-671 EB MARS Bulk Status
begin
{$I before.inc}
  SetName('GetMARSEmpList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetMARSEmpList(MgrID);
  {$I after.inc}
end;

function TQMService.GetOpenTicketExpList(const SecSession: string;  //QM-695 EB Open Ticket Extended
  const ManagerID, RowLimit, MaxDays: Integer): string;
begin
  {$I before.inc}
  SetName('GetOpenTicketExpList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetOpenTicketExpList(ManagerID, RowLimit, MaxDays);
  {$I after.inc}
end;

function TQMService.GetOpenTicketSumList(const SecSession: string;
  const ManagerID, MaxDays: Integer): string;
begin
 {$I before.inc}
  SetName('GetOpenTicketSumList');
  GetManageWorkDM;
  Result := ManageWorkDM.GetOpenTicketSumList(ManagerID, MaxDays);
  {$I after.inc}
end;

function TQMService.GetOQDataForEmployee(const SecSession: string;
  const EmpID: Integer): string;
begin
 {$I before.inc}
  SetName('GetOQDataForEmployee');
  GetManageWorkDM;
  Result := ManageWorkDM.GetOQDataForEmployee(EmpID);
  {$I after.inc}
end;

function TQMService.GetOQDataForManager(const SecSession: string;
  const MgrID: Integer; const IncludeRecentExp: Integer): string;
begin
{$I before.inc}
  SetName('GetOQDataForManager');
  GetManageWorkDM;
  Result := ManageWorkDM.GetOQDataForManager(MgrID, IncludeRecentExp);
  {$I after.inc}
end;

function TQMService.GetPlusForManager(const SecSession: string;
  const MgrID: Integer): string;  //QM-585 EB Plus Whitelist
begin
  {$I before.inc}
  SetName('GetPlusForManager');
  GetManageWorkDM;
  Result := ManageWorkDM.GetPlusForManager(MgrID);
  {$I after.inc}
end;

function TQMService.GetTicketActivities(const SecSession: String; const EmpID: Integer; const NumDays: Integer): String;
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@NumDays'; ParamType: ftInteger; Dir: pdInput));
begin
  {$I before.inc}
  SetName('GetTicketActivities');
  GetManageWorkDM;
  Result := ManageWorkDM.GetTicketActivities(EmpID, NumDays);
  {$I after.inc}
end;


function TQMService.GetTicketAlerts(const SecSession: string;  //QM-771 Ticket Alerts
  const TicketID: integer): string;
begin
  {$I before.inc}
  SetName('GetTicketAlerts');
  GetTicketDM;
  Result := TicketDM.GetTicketAlertsforTicket(TicketID);
  {$I after.inc}
end;

function TQMService.AckTicketActivities(const SecSession: string; const ActivityAckIDArray: IntegerList): Integer;
begin
  {$I before.inc}
  SetName('AckTicketActivities');
  GetManageWorkDM;
  Result := ManageWorkDM.AckTicketActivities(ActivityAckIDArray);
  {$I after.inc}
end;

procedure TQMService.AddEmpToPlusWhiteList(const SecSession: string;
  const LocatorID, PlusID, ModifiedByID: Integer; const Util: string);       //QM-585 EB Plus Whitelist
begin
   {$I before.inc}
  SetName('AddEmpToPlusWhiteList');
  GetManageWorkDM;
  ManageWorkDM.AddEmpToPlusWhiteList(LocatorID, PlusID, ModifiedByID, Util);
  {$I after.inc}
end;

function TQMService.AddMARSRequest(const SecSession: string;
  const LocatorID: integer; const NewStatus: string;
  const InsertedBy: integer): boolean;
begin
   {$I before.inc}
  SetName('AddMARSRequest');
  GetManageWorkDM;
  Result := ManageWorkDM.AddMARSRequest(LocatorID, NewStatus, InsertedBy);
  {$I after.inc}
end;



function TQMService.RemoveMARSRequest(const SecSession: string;
  const MARSID: integer): boolean;
begin
  {$I before.inc}
  SetName('RemoveMARSRequest');
  GetManageWorkDM;
  Result := ManageWorkDM.StopMARSRequest(MARSID);
  {$I after.inc}
end;

 
function TQMService.AddROOptimizationRequest(const SecSession: string;   //QM-702 Route Optimization EB
  const LocatorID, InsertedBy, NextTicketID: integer): boolean;
begin
  {$I before.inc}
  SetName('AddROOptimizationRequest');
  GetManageWorkDM;
  Result := ManageWorkDM.AddROOptimizationRequest(LocatorID, InsertedBy, NextTicketID);
  {$I after.inc}
end;


procedure TQMService.RemoveEmpFromPlusWhitelist(const SecSession: string;  //QM-585 EB Plus Whitelist
const LocatorID, PlusID, ModifiedByID: Integer);
begin
  {$I before.inc}
  SetName('RemoveEmpFromPlusWhitelist');
  GetManageWorkDM;
  ManageWorkDM.RemoveEmpFromPlusWhitelist(LocatorID, PlusID, ModifiedByID);
  {$I after.inc}
end;



function TQMService.GetEmployeePlusListByEmp(const SecSession: string;   //QM-585 EB Plus Whitelist
  const EmpID: Integer): string;
begin
  {$I before.inc}
  SetName('GetEmployeePlusListByEmp');
  GetManageWorkDM;
  Result := ManageWorkDM.GetEmployeePlusListByEmp(EmpID);
  {$I after.inc}
end;

function TQMService.GetWMEmpPlusUtilByEmp(const SecSession: string;
  const EmpID: Integer): string;
begin
  {$I before.inc}
  SetName('GetWMEmpPlusUtilByEmp');
  GetManageWorkDM;
  Result := ManageWorkDM.GetWMEmpPlusUtilByEmp(EmpID);
  {$I after.inc}
end;

function TQMService.AckAllTicketActivitiesForManager(const SecSession: string;
  const Params: NVPairListList): Integer;
begin
  {$I before.inc}
  SetName('AckAllTicketActivitiesForManager');
  GetManageWorkDM;
  Result := ManageWorkDM.AckAllTicketActivitiesForManager(Params);
  {$I after.inc}
end;

function TQMService.GetUnackedTicketActivityCount(const SecSession: string;
  const Params: NVPairList): Integer;
begin
  {$I before.inc}
  SetName('GetUnackedTicketActivityCount');
  GetManageWorkDM;
  Result := ManageWorkDM.GetUnackedTicketActivityCount(Params);
  {$I after.inc}
end;


function TQMService.InsertTicketNoteDirect(const SecSession: string; Const TicketID: Integer; Const EntryDate: TDateTime;    //qm-747  sr
     Const NoteText: string; Const SubType: Integer;  Const ClientCode: string; Const EnteredByID: Integer): integer;
var
  iEnteredByID: integer;
begin
  {$I before.inc}
  SetName('InsertTicketNoteDirect');
  GetTicketDM;
  iEnteredByID:=DM.LoggedInUID;
  Result := TicketDM.InsertTicketNote(TicketID, EntryDate, iEnteredByID, NoteText, SubType, ClientCode); //qm-747
  {$I after.inc}
end;

procedure TQMService.LogAttachmentProgress(Sender: TObject; const LogMessage: string);
begin
  DM.Op.Detail := LogMessage;
end;

function TQMService.SaveBreakRuleResponses2(const SecSession: string;
  const BreakRuleResponses: BreakRuleResponseList): GeneratedKeyList;
begin
{$I before.inc}
  SetName('SaveBreakRuleResponses2');
  Assert(Assigned(BreakRuleResponses));
  Result := GeneratedKeyList.Create;
  GetTimesheetDM;
  TimesheetDM.ProcessBreakRuleResponses(BreakRuleResponses, Result);
{$I after.inc}
end;

function TQMService.SaveTaskSchedule2(const SecSession: string;
  const TaskScheduleChanges: TaskScheduleChangeList): GeneratedKeyList;
begin
{$I before.inc}
  SetName('SaveTaskSchedule2');
  Assert(Assigned(TaskScheduleChanges));
  Result := GeneratedKeyList.Create;
  GetTicketDM;
  TicketDM.ProcessTaskSchedule(TaskScheduleChanges, Result);
{$I after.inc}
end;

function TQMService.SaveUploadQueueStats(const SecSession: string;
  const Statistics: PendingUploadsStats): Boolean;
begin
  Result := False;
{$I before.inc}
  SetName('SaveUploadQueueStats');
  Assert(Assigned(Statistics));
  DM.UploadQueue.Parameters.ParamByName('emp_id').Value := LoggedInEmpId;
  DM.UploadQueue.Open;
  try
    if DM.UploadQueue.IsEmpty then begin
      DM.Op.Detail := 'Adding new EmployeeUploadQueue';
      DM.UploadQueue.Insert;
      DM.UploadQueue.FieldByName('emp_id').Value := LoggedInEmpID;
    end else begin
      DM.Op.Detail := 'Editing EmployeeUploadQueue';
      DM.UploadQueue.Edit;
    end;
    DM.UploadQueue.FieldByName('file_count').Value := Statistics.FileCount;
    DM.UploadQueue.FieldByName('file_size_total').Value := Statistics.FileTotalKB;
    DM.UploadQueue.FieldByName('oldest_file_minutes').Value := Statistics.LongestWaitMinutes;
    DM.UploadQueue.FieldByName('modified_date').Value := Now;
    DM.UploadQueue.Post;
  finally
    DM.UploadQueue.Close;
  end;
  Result := True;
{$I after.inc}
end;

procedure TQMService.NotifyManagerOfEmployeeActivity(const ActivityType: string; EmpID: Integer; ActivityDate: TDateTime;Details, ExtraDetails:String);
var
  MsgNumber: string;
  MgrID: Integer;
  MgrName: string;
  MsgBody: string;
  MsgSubject: string;
begin
  DM.Op.Detail := 'Notifying manager of ' + ActivityType;
  MsgNumber := 'EACT' + FormatDateTime('yyyymmddhhnnss', ActivityDate);
  MgrID := DM.GetEmployeeManager(EmpID, MgrName);
  Assert(MgrID <> -1, 'Could not determine a manager for EmpID ' + IntToStr(EmpID));
  if ActivityType = ActivityTypeLunchBreakException then begin
    MsgSubject := 'Short meal break by ' + LoggedInEmpShortName;
    MsgBody := LoggedInEmpShortName + ' ended their meal break early to clock in for work.';
  end else   // QMANTWO-235
  if ActivityType = ActivityTypeStartTimeOverride then begin  // QMANTWO-235
    MsgSubject := 'Work Start Time Overriden by '+LoggedInEmpShortName; // QMANTWO-235
    MsgBody := LoggedInEmpShortName +' changed Work Start from '+ExtraDetails+ ' Reason: '+Details;    // QMANTWO-235
  end else begin
    MsgSubject := 'Alertable activity performed by ' + LoggedInEmpShortName;
    MsgBody := MsgSubject;
  end;
  DM.SendMessageToEmployee(MsgNumber, MgrID, MgrName, MsgSubject, MsgBody);
end;

function TQMService.SaveRequiredDocuments(const SecSession: AnsiString; const RequiredDocs: RequiredDocumentList): GeneratedKeyList;
begin
  Result := nil;
  {$I before.inc}
  SetName('SaveRequiredDocuments');
  GetDamageDM;
  Result := DamageDM.SaveRequiredDocuments(RequiredDocs);
  {$I after.inc}
end;

function TQMService.GetDamagesForLocator(const SecSession: string;
  const LocatorID, NumDays: Integer; const OpenOnly: Boolean): string;
begin
  {$I before.inc}
  SetName('GetDamagesForLocator');
  GetManageWorkDM;
  Result := ManageWorkDM.GetDamagesForLocator(LocatorID, NumDays, OpenOnly);
  {$I after.inc}
end;

procedure TQMService.MoveDamages(const SecSession: string; const Damages: IntegerList; const FromInvestigatorID: Integer; const ToInvestigatorID: Integer);
begin
  {$I before.inc}
  SetName('MoveDamages');
  GetDamageDM;
  DamageDM.MoveDamages(Damages, FromInvestigatorID, ToInvestigatorID);
  {$I after.inc}
end;

procedure TQMService.MoveWorkOrders(const SecSession: string; const WorkOrders: IntegerList; const FromWorkerID: Integer; const ToWorkerID: Integer);
begin
  {$I before.inc}
  SetName('MoveWorkOrders');
  GetWorkOrderDM;
  WorkOrderDM.MoveWorkOrders(WorkOrders, FromWorkerID, ToWorkerID);
  {$I after.inc}
end;

procedure TQMService.SaveDamagePriority(const SecSession: string; const DamageID: Integer;
  const EntryDate: TDateTime; const Priority: Integer);
begin
  {$I before.inc}
  SetName('SaveDamagePriority');
  GetDamageDM;
  DamageDM.SaveDamagePriority(DamageID, EntryDate, Priority);
  {$I after.inc}
end;

procedure TQMService.SaveEmployeeShowFutureWork(const SecSession: string; const EmpID: Integer; const ShowFutureTickets: Boolean);
begin
  {$I before.inc}
  SetName('SaveEmployeeShowFutureWork');
  GetManageWorkDM;
  ManageWorkDM.SaveEmployeeShowFutureWork(EmpID, ShowFutureTickets);
  {$I after.inc}
end;

procedure TQMService.CreateFollowupTicket(const SecSession: String;
  const TicketID, FollowupTypeID: Integer; const FollowupDueDate,
  FollowupTransmitDate: DateTime);
begin
  {$I before.inc}
  SetName('CreateFollowupTicket');
  GetTicketDM;
  TicketDM.CreateFollowupTicket(TicketID, FollowupTypeID, FollowupDueDate, FollowupTransmitDate);
  {$I after.inc}
end;

procedure TQMService.SetMessageActive(const SecSession: string; const MessageID: Integer; const IsActive: Boolean);
const
  SQL = 'update message set active = %d where message_id = %d';
var
  RecsAffected: Integer;
begin
  {$I before.inc}
  SetName('SetMessageActive');
  DM.Conn.Execute(Format(SQL, [Ord(IsActive), MessageID]), RecsAffected);
  Assert(RecsAffected = 1, 'The message''s active status was not changed.');
  {$I after.inc}
end;

function TQMService.LoggedInEmpID: Integer;
begin
  Result := DM.LoggedInEmpID;
end;

function TQMService.LoggedInEmpShortName: string;
begin
  Result := DM.LoggedInEmpShortName;
end;

function TQMService.LoggedInUID: Integer;
begin
  Result := DM.LoggedInUID;
end;        

function TQMService.LoggedInEmpAPIKey: string;
begin
  Result := DM.LoggedInEmpAPIKey;
end;
{$IFDEF ROSDK5UP}
var
  fClassFactory: IROClassFactory;

initialization
{$IFDEF FPC}
  {$I NewService_Impl.lrs}
{$ENDIF}
  fClassFactory := TROClassFactory.Create('QMService', Create_QMServices, TQMService_Invoker);
  // RegisterForZeroConf(fClassFactory,'_NewService_rosdk._tcp.');

finalization
  UnRegisterClassFactory(fClassFactory);
  fClassFactory := nil;


{$ELSE}

initialization
  TLessBrokenPooledClassFactory.Create('QMService', Create_QMServices, TQMService_Invoker, 30);

{$ENDIF}

end.

