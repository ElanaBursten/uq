unit AddinInfoDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, ADODB, QMServerLibrary_Intf, DB;

type
  TAddinInfoDM = class(TLogicFeatureDM)
    AddinInfoData: TADODataSet;
  private
    { Private declarations }
  public
    procedure ProcessAddinInfoChanges(ChangeList: AddinInfoChangeList; var NewKeys: GeneratedKeyList);
  end;

implementation

uses LogicDMu, RemObjectsUtils, OdIsoDates;

{$R *.dfm}

procedure TAddinInfoDM.ProcessAddinInfoChanges(ChangeList: AddinInfoChangeList; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from addin_info where ' +
   'foreign_type = :foreign_type and foreign_id = :foreign_id ' +
   'and added_date = %s and added_by_id = :added_by_id';
var
  i: Integer;
  AddinInfo: AddinInfoChange;
  AddinInfoID: Integer;
  ForeignID: Integer;
  ForeignType: Integer;
  AddedDate: TDateTime;
  NewAddinInfoID: Integer;
  Latitude: Double;
  Longitude: Double;
  D: TADODataSet;
begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessAddinInfoChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessAddinInfoChanges');
  Assert(Assigned(LogicDM.Op), 'Operation not started');

  D := AddinInfoData;
  for i := 0 to ChangeList.Count - 1 do begin
    Op.Phase := Format('Addin Info %d of %d', [i + 1, ChangeList.Count]);
    AddinInfo := ChangeList[i];
    Assert(Assigned(AddinInfo));

    AddinInfoID := AddinInfo.AddinInfoID;
    ForeignType := AddinInfo.ForeignType;
    ForeignID := AddinInfo.ForeignID;
    AddedDate := AddinInfo.AddedDate;
    {TODO: could these be null?}
    Latitude := AddinInfo.Latitude;
    Longitude := AddinInfo.Longitude;

    if AddinInfoID > 0 then
      raise Exception.Create('Editing existing addin_info is not supported');

    Op.Detail := 'Check for existing AddinInfo';
    D.Close;
    D.EnableBCD := False;
    D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(AddedDate)]);
    AssignParamsFromROComplex(D.Parameters, AddinInfo);
    D.Parameters.ParamValues['added_by_id'] := LogicDM.LoggedInEmpID;
    D.Open;
    if D.RecordCount < 1 then begin
      Op.Detail := 'Adding new AddinInfo';
      D.Insert;
      D.FieldByName('foreign_type').AsInteger := ForeignType;
      D.FieldByName('foreign_id').AsInteger := ForeignID;
      D.FieldByName('latitude').AsFloat := Latitude;
      D.FieldByName('longitude').AsFloat := Longitude;
      D.FieldByName('added_date').AsDateTime := AddedDate;
      D.FieldByName('added_by_id').AsInteger := LogicDM.LoggedInEmpID;
      D.Post;
    end else if D.RecordCount > 1 then
      raise Exception.Create('More than one addin_info row matches the selection criteria.');

    NewAddinInfoID := D.FieldByName('addin_info_id').AsInteger;
    Assert(NewAddinInfoID > 0, 'Negative new addin_info ID');
    LogicDM.UpdateDateTimeToIncludeMilliseconds('addin_info', 'addin_info_id', NewAddinInfoID, 'added_date', AddedDate);
    AddIdentity(NewKeys, 'addin_info', AddinInfoID, NewAddinInfoID);
  end;
end;

end.
