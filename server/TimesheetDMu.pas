unit TimesheetDMu;

interface

uses
  SysUtils, Classes, ADODB, DB,  Variants, uROClientIntf,
  OdMiscUtils, BaseLogicFeatureDMu, QMServerLibrary_Intf, RemObjectsUtils;

type
  TTimesheetDM = class(TLogicFeatureDM)
    TimesheetEntry: TADODataSet;
    CurrentTimesheet: TADODataSet;
    TimesheetsForEmp: TADODataSet;
    GetTimesheetChanges: TADOStoredProc;
    OriginalTime: TADODataSet;
    BreakRulesResponse: TADODataSet;
    TimesheetExport: TADOStoredProc;
    GetFlHolidays: TADOStoredProc;
    EmpTimeRuleData: TADODataSet;
    GetSickLeavePTOQry: TADODataSet;
  private
    procedure SetPreviousSheetsToOldStatus(WorkEmpId: Integer; WorkDate: TDateTime);
    procedure RecalculateTimesheets(Timesheets: TimesheetEntryList);
    function GetEmpWeekBeginDatesFromTimesheets(Timesheets: TimesheetEntryList): TStringList;
    procedure GenerateTimeAlteredMessages(TimeSheets: TimesheetEntryList);
    function GetBreakRuleMessageID(const PCCode, BreakRuleType: string): Integer;
    function GetEmployeeProfitCenter(const EmpID: Integer; const PayrollPC: Boolean=False): string;
    function GetEmployeeShortName(const EmpID: Integer): string;

  public
    function PostTimesheets(const Timesheets: TimesheetEntryList): GeneratedKeyList;
    function ApproveTimesheets(const Final: Boolean;
      const LocalTime: DateTime; const Entries: TimesheetRecordList): string;
    function SearchTimesheets(const SearchType: Integer;
      const DateStart, DateStop: DateTime; const StartEmpId: Integer;
      const LevelLimit: Integer; const PayrollCenter: string): string;
    procedure ProcessBreakRuleResponses(ChangeList: BreakRuleResponseList; var NewKeys: GeneratedKeyList);
    function ExportTimesheets(const ProfitCenter: string; const WeekEnding: TDateTime): string;
    function GetFloatingHolidayCount(EmployeeID: Integer; Year: Integer; WeekStart: DateTime; WeekEnd: DateTime): integer;
    function GetSickLeavePTOBalances(Emp_id: integer): string;
    function EmpIsTSE(EmpID: integer; EntryID: integer = 0): boolean;
  end;

implementation

uses QMConst, OdAdoUtils, OdIsoDates, HoursCalc, HoursDataset, WeeklyTimesheet,
  TimeSheetExport, OdSqlBuilder, OdDataSetToXml, LogicDMu, DateUtils, JclStrings,
  OdExceptions;

{$R *.dfm}

{ TTimesheetDM }

procedure TTimesheetDM.SetPreviousSheetsToOldStatus(WorkEmpId: Integer;
  WorkDate: TDateTime);
const
  UpdateSQL = 'update timesheet_entry set status = ''%s'' ' +
    'where (status = ''%s'' or status = ''%s'') and work_emp_id = %d and work_date = ''%s'' ';
var
  SQL: string;
begin
    SQL := Format(UpdateSQL, [TimesheetStatusOld, TimesheetStatusActive,
      TimesheetStatusSubmit, WorkEmpId, IsoDateTimeToStr(WorkDate)]);
    LogicDM.ExecuteSQL(SQL);
end;


function TTimesheetDM.PostTimesheets(const Timesheets: TimesheetEntryList): GeneratedKeyList;

  function MaxFloatingHolidays(const EmpID: Integer): Integer;
  const
    Select = 'select lc.floating_holidays_per_year from locating_company lc ' +
      'join employee e on (lc.company_id = e.company_id) where (e.emp_id = %d)';
  var
    Data: TDataSet;
  begin
    Data := LogicDM.CreateDataSetForSQL(Format(Select, [EmpID]));
    try
      if Data.IsEmpty then
        Result := 0
      else
        Result := Data.FieldByName('floating_holidays_per_year').AsInteger;
    finally
      FreeAndNil(Data);
    end;
  end;

var
  TimeSheetIndex: Integer;
  Sheet: NVPairList;
  D: TDataSet;
  IsFinalApproved: Boolean;
  CanUnapprove: Boolean;
  OldEntryId: Integer;
  NewEntryId: Integer;
  WorkEmpId: Integer;
  WorkDate: TDateTime;
  Status: string;
  UseStatus: string;
  EntryDateLocal: TDateTime;
  ApproveDate: Variant;
  ApproveBy: Variant;
  FinalApproveDate: Variant;
  FinalApproveBy: Variant;
  MilesStart1: Variant;
  MilesStop2: Variant;
  VehicleUse: string;
  RegHours: Double;
  OtHours: Double;
  DtHours: Double;
  CalloutHours: Double;
  VacHours: Variant;
  LeaveHours: Variant;
  PTOHours: Variant;
  BrHours: Variant;
  HolHours: Variant;
  JuryHours: Variant;
  FloatingHoliday: Variant;
  RuleAckDate: Variant;
  RuleIdAck: Variant;
  EmpTypeId: Variant;
  ReasonChanged: Variant;
  LunchStart: Variant;
  PerDiem: Variant;
  Source: string;
  WorkStartTimes: array[1..TimesheetNumWorkPeriods] of Variant;
  WorkStopTimes:  array[1..TimesheetNumWorkPeriods] of Variant;
  CalloutStartTimes: array[1..TimesheetNumCalloutPeriods] of Variant;
  CalloutStopTimes:  array[1..TimesheetNumCalloutPeriods] of Variant;
  I: Integer;
begin
  {$IF (CompilerVersion >= 16) and (CompilerVersion < 18)}
  IsFinalApproved := False; // Fix BDS warning only
  {$IFEND}
  D := TimesheetEntry;
  D.Open;
  Result := GeneratedKeyList.Create;
  CanUnapprove := LogicDM.CanI(RightTimesheetsUnApprove);

  for TimeSheetIndex := 0 to Timesheets.Count - 1 do begin
    Sheet := TimeSheets[TimeSheetIndex];

    OldEntryId := NVPairInteger(Sheet, 'entry_id', bvError);
    WorkEmpId := NVPairInteger(Sheet, 'work_emp_id', bvError);
    WorkDate := NVPairDateTime(Sheet, 'work_date', bvError);
    Status := NVPairString(Sheet, 'status', bvError);
    EntryDateLocal := NVPairDateTime(Sheet, 'entry_date_local', bvError);

    ApproveDate := NVPairDateTime(Sheet, 'approve_date', bvNull);
    ApproveBy := NVPairInteger(Sheet, 'approve_by', bvNull);
    FinalApproveDate := NVPairDateTime(Sheet, 'final_approve_date', bvNull);
    FinalApproveBy := NVPairInteger(Sheet, 'final_approve_by', bvNull);

    for I := 1 to TimesheetNumWorkPeriods do begin
      WorkStartTimes[I] := NVPairTime(Sheet, 'work_start' + IntToStr(I), bvNull);
      WorkStopTimes[I] := NVPairTime(Sheet, 'work_stop' + IntToStr(I), bvNull);
    end;
    for I := 1 to TimesheetNumCalloutPeriods do begin
      CalloutStartTimes[I] := NVPairTime(Sheet, 'callout_start' + IntToStr(I), bvNull);
      CalloutStopTimes[I] := NVPairTime(Sheet, 'callout_stop' + IntToStr(I), bvNull);
    end;

    MilesStart1 := NVPairInteger(Sheet, 'miles_start1', bvNull);
    MilesStop2 := NVPairInteger(Sheet, 'miles_stop1', bvNull);
    VehicleUse := NVPairString(Sheet, 'vehicle_use', bvDefault, 'NA');

    RegHours := NVPairFloat(Sheet, 'reg_hours', bvZero);
    OtHours := NVPairFloat(Sheet, 'ot_hours', bvZero);
    DtHours := NVPairFloat(Sheet, 'dt_hours', bvZero);
    CalloutHours := NVPairFloat(Sheet, 'callout_hours', bvZero);

    VacHours := NVPairFloat(Sheet, 'vac_hours', bvZero);
    LeaveHours := NVPairFloat(Sheet, 'leave_hours', bvZero);
    PTOHours := NVPairFloat(Sheet, 'pto_hours', bvZero);

//    if (PTOHours <> 0) and (VacHours + LeaveHours <> 0) then      //qm-528  sr
//      raise Exception.CreateFmt('Detected a timesheet that contains both PTO and VAC or LEAVE hours for %d/%s', [WorkEmpID, DateToStr(WorkDate)]);     //qm-528  sr

    BrHours := NVPairFloat(Sheet, 'br_hours', bvZero);
    HolHours := NVPairFloat(Sheet, 'hol_hours', bvZero);
    JuryHours := NVPairFloat(Sheet, 'jury_hours', bvZero);

    FloatingHoliday := NVPairBoolean(Sheet, 'floating_holiday', bvZero);

    PerDiem := NVPairBoolean(Sheet, 'per_diem', bvZero); //QMANTWO-733 EB

    RuleAckDate := NVPairDateTime(Sheet, 'rule_ack_date', bvNull);
    RuleIdAck := NVPairInteger(Sheet, 'rule_id_ack', bvNull);
    EmpTypeID := NVPairInteger(Sheet, 'emp_type_id', bvNull);
    ReasonChanged := NVPairInteger(Sheet, 'reason_changed', bvNull);
    LunchStart := NVPairTime(Sheet, 'lunch_start', bvNull);
    Source := NVPairString(Sheet, 'source', bvDefault, 'entry');

    if EmpIsTSE(WorkEmpID) then   //QM-833 Timesheet TSE EB
      Source := 'TSE';
    

    CurrentTimesheet.Parameters.ParamValues['WorkEmpID'] := WorkEmpId;
    CurrentTimesheet.Parameters.ParamValues['WorkDate'] := WorkDate;
    CurrentTimesheet.Open;
    try
      Assert(CurrentTimesheet.RecordCount <= 1, 'Found more than one current timesheet for a given employee and work date');
      IsFinalApproved := TimesheetIsFinalApproved(CurrentTimesheet);
    finally
      CurrentTimesheet.Close;
    end;

    if IsFinalApproved and (not CanUnapprove) then begin  // User can not overwrite FA timesheets
      // The current timesheet does not need to be modified
      UseStatus := TimesheetStatusOld;
    end
    else begin
      SetPreviousSheetsToOldStatus(WorkEmpId, WorkDate);
      UseStatus := Status; // ACTIVE or SUBMIT
    end;

    D.Insert;
    D.FieldByName('status').AsString := UseStatus; //!!!
    D.FieldByName('entry_by').AsInteger := LogicDM.LoggedInEmpID; //!!!
    D.FieldByName('work_emp_id').AsInteger := WorkEmpId;
    D.FieldByName('work_date').AsDateTime := WorkDate;
    D.FieldByName('entry_date_local').AsDateTime := EntryDateLocal;
    D.FieldByName('entry_date').AsDateTime := Now;

    for I := 1 to TimesheetNumWorkPeriods do begin
      D.FieldByName('work_start' + IntToStr(I)).Value := WorkStartTimes[I];
      D.FieldByName('work_stop' + IntToStr(I)).Value := WorkStopTimes[I];
    end;
    for I := 1 to TimesheetNumCalloutPeriods do begin
      D.FieldByName('callout_start' + IntToStr(I)).Value := CalloutStartTimes[I];
      D.FieldByName('callout_stop' + IntToStr(I)).Value := CalloutStopTimes[I];
    end;

    D.FieldByName('miles_start1').Value := MilesStart1;
    D.FieldByName('miles_stop1').Value := MilesStop2;
    D.FieldByName('vehicle_use').AsString := VehicleUse;

    D.FieldByName('reg_hours').AsFloat := RegHours;
    D.FieldByName('ot_hours').AsFloat := OtHours;
    D.FieldByName('dt_hours').AsFloat := DtHours;
    D.FieldByName('callout_hours').AsFloat := CalloutHours;

    D.FieldByName('vac_hours').Value := VacHours;
    D.FieldByName('leave_hours').Value := LeaveHours;
    D.FieldByName('pto_hours').Value := PTOHours;
    D.FieldByName('br_hours').Value := BrHours;
    D.FieldByName('hol_hours').Value := HolHours;
    D.FieldByName('jury_hours').Value := JuryHours;

    D.FieldByName('floating_holiday').Value := FloatingHoliday;

    D.FieldByName('per_diem').Value := PerDiem;

    D.FieldByName('rule_ack_date').Value := RuleAckDate;
    D.FieldByName('rule_id_ack').Value := RuleIdAck;
    D.FieldByName('emp_type_id').Value := EmpTypeID;
    D.FieldByName('reason_changed').Value := ReasonChanged;
    D.FieldByName('lunch_start').Value := LunchStart;
    D.FieldByName('source').Value := Source;

    D.Post;
    NewEntryID := D.FieldByName('entry_id').AsInteger;
    if NewEntryID < 1 then
      raise Exception.Create('Invalid timesheet entry ID after posting: ' + IntToStr(NewEntryID));
    if OldEntryID < 0 then
      AddIdentity(Result, 'timesheet_entry', OldEntryID, NewEntryID);

    CurrentTimesheet.Open;
    try
      if CurrentTimesheet.RecordCount <> 1 then
        raise Exception.CreateFmt('Found more than one current timesheet after posting for %d/%s', [WorkEmpID, DateToStr(WorkDate)]);
    finally
      CurrentTimesheet.Close;
    end;
  end;

  RecalculateTimesheets(Timesheets);
  GenerateTimeAlteredMessages(Timesheets);
end;

procedure TTimesheetDM.RecalculateTimesheets(Timesheets: TimesheetEntryList);

  procedure GetExistingTime(HC: TBaseHoursCalculator; WeekBeginDate: TDateTime);
  var
    DayIdx: Integer;
    WorkingHours: Double;
    CallOutHours: Double;
  begin
    HC.ClearResults;
    while not TimesheetsForEmp.Eof do begin
      DayIdx := Round(TimesheetsForEmp.FieldByName('work_date').AsDateTime - WeekBeginDate);
      if not (DayIdx in [0..6]) then
        raise Exception.CreateFmt('Unexpected DayIndex of %d for Work Date %s',
          [DayIdx, TimesheetsForEmp.FieldByName('work_date').AsString]);

      WorkingHours := GetWorkHours(TimesheetsForEmp);
      CallOutHours := GetCalloutHours(TimesheetsForEmp);
      HC.Worked[DayIdx] := WorkingHours;
      HC.Callout[DayIdx] := CallOutHours;
      HC.Regular[DayIdx] := TimesheetsForEmp.FieldByName('reg_hours').AsFloat;
      HC.Overtime[DayIdx] := TimesheetsForEmp.FieldByName('ot_hours').AsFloat;
      HC.DoubleTime[DayIdx] := TimesheetsForEmp.FieldByName('dt_hours').AsFloat;
      HC.CalloutCalc[DayIdx] := TimesheetsForEmp.FieldByName('callout_hours').AsFloat;
      TimesheetsForEmp.Next;
    end;
  end;

  procedure SaveRecalculatedHours(HC: TBaseHoursCalculator; WeekBeginDate: TDateTime; CanUnapprove: Boolean);
  var
    DayIdx: Integer;
    FldIdx: Integer;
    WorkDate: TDateTime;
    WorkEmpID: Integer;
    UseStatus: string;
    D: TDataset;
  const
    SkipFields: array[0..5] of string = ('entry_id', 'modified_date', 'reg_hours', 'ot_hours', 'callout_hours', 'status');
  begin
    TimesheetsForEmp.First;
    D := TimesheetEntry;
    while not TimesheetsForEmp.Eof do begin
      WorkDate := TimesheetsForEmp.FieldByName('work_date').AsDateTime;
      WorkEmpID := TimesheetsForEmp.FieldByName('work_emp_id').AsInteger;
      DayIdx := DaysBetween(WorkDate, WeekBeginDate);
      if not DayIdx in [0..6] then
        raise Exception.CreateFmt('Internal error, unexpected DayIdx of %d for employee id %d ' +
          'and date %s', [DayIdx, WorkEmpID, DateToStr(WorkDate)]);

      if (FloatEquals(TimesheetsForEmp.FieldByName('reg_hours').AsFloat, HC.Regular[DayIdx], 0.01) = False) or
        (FloatEquals(TimesheetsForEmp.FieldByName('ot_hours').AsFloat, HC.Overtime[DayIdx], 0.01) = False) or
        (FloatEquals(TimesheetsForEmp.FieldByName('dt_hours').AsFloat, HC.Doubletime[DayIdx], 0.01) = False) or
        (FloatEquals(TimesheetsForEmp.FieldByName('callout_hours').AsFloat, HC.CalloutCalc[DayIdx], 0.01) = False) then begin
        if TimesheetIsFinalApproved(TimesheetsForEmp) and (CanUnapprove = False) then   // User can not overwrite FA timesheets
          UseStatus := TimesheetStatusOld
        else begin
          SetPreviousSheetsToOldStatus(WorkEmpID, WorkDate);
          UseStatus := TimesheetsForEmp.FieldByName('status').AsString;
        end;

        Op.Detail := Format('Inserting %s timesheet for recalculated hours', [UseStatus]);
        D.Insert;
        for FldIdx := 0 to TimesheetsForEmp.FieldCount - 1 do begin
          if not StringInArray(D.Fields[FldIdx].FieldName, SkipFields) then
            D.Fields[FldIdx].Value := TimesheetsForEmp.Fields[FldIdx].Value;
        end;
        D.FieldByName('reg_hours').AsFloat := HC.Regular[DayIdx];
        D.FieldByName('ot_hours').AsFloat := HC.Overtime[DayIdx];
        D.FieldByName('dt_hours').AsFloat := HC.Doubletime[DayIdx];
        D.FieldByName('callout_hours').AsFloat := HC.CalloutCalc[DayIdx];
        D.FieldByName('status').AsString := UseStatus;
        D.Post;
      end else
        Op.Detail := 'Hours are up to date';

      TimesheetsForEmp.Next;
    end;
  end;

const
  SelectTimeRule = 'select code from reference inner join employee ' +
    'on timerule_id = ref_id where emp_id = ';
var
  i: Integer;
  WorkEmpID: Integer;
  WeekBeginDate: TDateTime;
  HC: TBaseHoursCalculator;
  TimeRule: TDataSet;
  CanUnapprove: Boolean;
  EmployeeWorkWeeks: TStringList;
begin
  Op.Phase := 'Recalculating timesheets hours';
  Assert(Assigned(Timesheets), 'Timesheets undefined in RecalculateTimesheets');
  CanUnapprove := LogicDM.CanI(RightTimesheetsUnApprove);
  EmployeeWorkWeeks := GetEmpWeekBeginDatesFromTimesheets(Timesheets);
  try
    for i := 0 to EmployeeWorkWeeks.Count - 1 do begin
      WeekBeginDate := IsoStrToDate(Copy(EmployeeWorkWeeks[i], 1, 10));
      WorkEmpId := StrToInt(Copy(EmployeeWorkWeeks[i], 12, Length(EmployeeWorkWeeks[i])-11));
      TimeRule := LogicDM.GetDataSetForSQL(SelectTimeRule + IntToStr(WorkEmpID));
      TimesheetsForEmp.Close;
      TimesheetsForEmp.Parameters.ParamValues['WorkEmpID'] := WorkEmpId;
      TimesheetsForEmp.Parameters.ParamValues['StartWorkDate'] := WeekBeginDate;
      TimesheetsForEmp.Parameters.ParamValues['EndWorkDate'] := WeekBeginDate + 6;
      TimesheetsForEmp.Open;
      HC := CreateHoursCalculator(TimeRule.FieldByName('code').AsString);
      try
        Op.Detail := 'Gather existing sheets ' + EmployeeWorkWeeks[i];
        GetExistingTime(HC, WeekBeginDate);
        Op.Detail := 'Calculate hours ' + EmployeeWorkWeeks[i];
        HC.Calculate;
        Op.Detail := 'Save any changed sheets ' + EmployeeWorkWeeks[i];
        SaveRecalculatedHours(HC, WeekBeginDate, CanUnapprove);
      finally
        FreeAndNil(HC);
      end;
    end;
  finally
    FreeAndNil(EmployeeWorkWeeks);
  end;
end;

procedure TTimesheetDM.GenerateTimeAlteredMessages(TimeSheets: TimesheetEntryList);

  function NeedToNotifyEmployeeOfAlteredHours(const WorkEmpID: Integer; var RuleID: Integer): Boolean;
  var
    PCCode: string;
  begin
    RuleID := -1;
    Result := WorkEmpID <> LogicDM.LoggedInEmpID;
    if Result then begin
      PCCode := GetEmployeeProfitCenter(WorkEmpID);
      RuleID :=  GetBreakRuleMessageID(PCCode, BreakRuleTypeManagerAlteredTime);
      Result := RuleID > 0;
    end;
  end;

var
  EmployeeWorkWeeks: TStringList;
  Sheets: TWeeklyTimesheet;
  WorkEmpID: Integer;
  WorkEmpShortName: string;
  WeekBeginDate: TDateTime;
  WeekEndDate: TDateTime;
  HTMLMessage: string;
  i: Integer;
  RuleID: Integer;
  MessageNumber: string;
  ChangeDateTime: TDateTime;
  LastChangeEntryID: Integer;
  SummaryData: TADODataset;
begin
  Op.Phase := 'Generating time altered messages';
  Assert(Assigned(Timesheets), 'Timesheets undefined in GenerateTimeAlteredMessages');
  EmployeeWorkWeeks := GetEmpWeekBeginDatesFromTimesheets(Timesheets);
  try
    for i := 0 to EmployeeWorkWeeks.Count - 1 do begin
      Op.Detail := 'Gather existing sheets ' + EmployeeWorkWeeks[i];

      WorkEmpID := StrToInt(Copy(EmployeeWorkWeeks[i], 12, Length(EmployeeWorkWeeks[i])-11));
      if not NeedToNotifyEmployeeOfAlteredHours(WorkEmpID, RuleID) then
        Continue;

      Assert(RuleID > 0, 'Rule ID should be > 0 to notify for altered time');
      WorkEmpShortName := GetEmployeeShortName(WorkEmpID);
      // For this usage, week begins on Monday, not Sunday.
      WeekBeginDate := IsoStrToDate(Copy(EmployeeWorkWeeks[i], 1, 10)) + 1;
      WeekEndDate := WeekBeginDate + 6;
      GetTimesheetChanges.Close;
      GetTimesheetChanges.Parameters.ParamByName('@SheetsForID').Value := WorkEmpId;
      GetTimesheetChanges.Parameters.ParamByName('@WorkStart').Value := WeekBeginDate;
      GetTimesheetChanges.Parameters.ParamByName('@WorkEnd').Value := WeekEndDate;
      GetTimesheetChanges.Open;
      LoadNextRecordSet(GetTimesheetChanges, OriginalTime);

      Sheets := nil;
      SummaryData := TADODataSet.Create(nil);
      try
        LoadNextRecordSet(GetTimesheetChanges, SummaryData);
        Op.Detail := 'Getting notifications to send ' + EmployeeWorkWeeks[i];
        Sheets := TWeeklyTimesheet.CreateFromDataSets(WeekEndDate, GetTimesheetChanges, OriginalTime, SummaryData);
        HTMLMessage := Sheets.GetTimeChangesAsHTML;
        ChangeDateTime := Sheets.MaximumChangeDateTime;
        LastChangeEntryID := Sheets.LatestChangeEntryID;
      finally
        FreeAndNil(Sheets);
        FreeAndNil(SummaryData);
      end;
      if NotEmpty(HTMLMessage) then begin
        Op.Detail := 'Sending message ' + EmployeeWorkWeeks[i];
        MessageNumber := 'TSE' + FormatDateTime('yyyymmddhhnnss', ChangeDateTime);
        LogicDM.SendMessageToEmployee(MessageNumber, WorkEmpID, WorkEmpShortName,
          'Hours for week ending ' + DateToStr(WeekEndDate) +
          ' changed by ' + LogicDM.LoggedInEmpShortName, HTMLMessage, RuleID,
          LastChangeEntryID);
      end;
    end;
  finally
    FreeAndNil(EmployeeWorkWeeks);
  end;
end;

function TTimesheetDM.GetEmpWeekBeginDatesFromTimesheets(Timesheets: TimesheetEntryList): TStringList;
var
  i: Integer;
  WorkEmpIdStr: string;
  WorkDate: TDateTime;
  WeekBeginDate: TDateTime;
begin
  Result := TStringList.Create;
  for i := 0 to Timesheets.Count - 1 do begin
    WorkEmpIdStr := NVPairString(Timesheets[i], 'work_emp_id', bvError);
    WorkDate := NVPairDateTime(Timesheets[i], 'work_date', bvError);
    WeekBeginDate := BeginningOfTheWeek(WorkDate);
    if Result.IndexOf(IsoDateToStr(WeekBeginDate) + ' ' + WorkEmpIdStr) < 0 then
      Result.Add(IsoDateToStr(WeekBeginDate) + ' ' + WorkEmpIdStr);
  end;
end;

function TTimesheetDM.GetFloatingHolidayCount(EmployeeID: Integer; Year: Integer; WeekStart: DateTime; WeekEnd: DateTime): integer;
begin
  GetFlHolidays.Close;
  GetFlHolidays.Parameters.ParamByName('@EmpID').Value := EmployeeID;
  GetFlHolidays.Parameters.ParamByName('@WeekStart').Value := WeekStart;
  GetFlHolidays.Parameters.ParamByName('@WeekEnd').Value := WeekEnd;
  GetFlHolidays.Parameters.ParamByName('@Year').Value := Year;
  GetFlHolidays.Open;
  Result := GetFlHolidays.FieldByName('CountDays').AsInteger;
end;

function TTimesheetDM.GetSickLeavePTOBalances(Emp_id: integer): string; //QM-549 EB Sick & PTO Balance
begin
  GetSickLeavePTOQry.Close;
//  GetSickLeavePTOQry.ParamByName['Emp_ID'].Value := Emp_ID;
  GetSickLeavePTOQry.Parameters.ParamValues['Emp_ID'] := Emp_ID;    //QM-977 Remove BetterADO EB
  GetSickLeavePTOQry.Open;
  if GetSickLeavePTOQry.RecordCount > 0 then
  Result := ConvertDataSetsToSQLXML([GetSickLeavePTOQry], ['employee_balance']);

end;

function TTimesheetDM.ApproveTimesheets(const Final: Boolean;
  const LocalTime: DateTime; const Entries: TimesheetRecordList): string;
var
  Current: TDateTime;
  TSEList: TimesheetRecordList;  //QM-833 Timesheet TSE EB

  function EntryIDsToCommaString: string;
  var
    i: Integer;
    EntryID: string;
  begin
    Result := '';
    for i := 0 to Entries.Count - 1 do begin
      if Entries[i].EntryID > 0 then begin
        EntryID := IntToStr(Entries[i].EntryID);
        if Result = '' then
          Result := EntryID
        else
          Result := Result + ',' + EntryID;
      end;
    end;
  end;

  function LoadTSEList: boolean;  //QM-833 Timesheet TSE EB
  {keeping this separate so it is easier to maintain if it changes}
  var
    ii: integer;
    TSERec: TimesheetRecord;
  begin
    Result := False;
    Assert(Assigned(TSEList));

    for ii := 0 to Entries.Count - 1 do begin
      if EmpIsTSE(Entries[ii].EmpID, Entries[ii].EntryID) then begin
        TSERec := TSEList.Add;
        TSERec.EntryID := Entries[ii].EntryID;
        TSERec.EmpID := Entries[ii].EmpID;
        TSERec.WorkDate := Entries[ii].WorkDate;
        Result := True;  {Found one}
      end;
    end;
  end;

  function InsertCommands(var Count: Integer): string;
  const
    SQL =
      'if not exists(select entry_id from timesheet_entry ' +
      '  where status <> %s and work_emp_id = %d and work_date = %s) ' + sLineBreak +
      'insert into timesheet_entry (' +
      ' work_emp_id, work_date, status, entry_by, entry_date, entry_date_local, ' +
      ' approve_date, approve_date_local, approve_by) ' + sLineBreak +
      ' values (%d, %s, %s, %d, %s, %s, %s, %s, %d) ' + sLineBreak;
  var
    i: Integer;
    InsertCmd: string;
  begin
    Result := '';
    Count := 0;
    for i := 0 to Entries.Count - 1 do begin
      if Entries[i].EntryID < 1 then begin
        // Don't insert if the data has changed since they downloaded the list
        CurrentTimesheet.Parameters.ParamValues['WorkEmpID'] := Entries[i].EmpID;
        CurrentTimesheet.Parameters.ParamValues['WorkDate'] := Entries[i].WorkDate;
        CurrentTimesheet.Open;
        if CurrentTimesheet.Eof then begin
          InsertCmd := Format(SQL, [QuotedStr(TimesheetStatusOld), Entries[i].EmpID,
            QuoteDate(Trunc(Entries[i].WorkDate)), Entries[i].EmpID, QuoteDate(Trunc(Entries[i].WorkDate)),
            QuotedStr(TimesheetStatusActive), LogicDM.LoggedInEmpID, QuoteDate(Current), QuoteDate(LocalTime),
            QuoteDate(Current), QuoteDate(LocalTime), LogicDM.LoggedInEmpID]);
          Result := Result + InsertCmd;
          Inc(Count);
        end;
      end;
    end;
  end;

  procedure VerifyApprovalPermissions;
  var
    i: Integer;
    BadEmpID: Integer;
    BadEmpName : string;
    LimitationString: string;
    EntryEmpIDs: TIntegerArray;
  begin
    if Final and (not LogicDM.CanI(RightTimesheetsFinalApprove, LimitationString, LogicDM.LoggedInEmpID)) then
      raise EOdNotAllowed.Create('You do not have timesheet final approve permissions');
    if (not Final) and (not LogicDM.CanI(RightTimesheetsApprove, LimitationString, LogicDM.LoggedInEmpID)) then
      raise EOdNotAllowed.Create('You do not have timesheet approve permissions');

    SetLength(EntryEmpIDs, Entries.Count);
    for i := 0 to Entries.Count - 1 do begin
      if Entries[i].EmpID = LogicDM.LoggedInEmpID then
        raise EOdNotAllowed.Create('You can not approve timesheets for yourself');
      if Entries[i].WorkDate > (Today + 1) then
        raise EOdNotAllowed.Create('You can not approve timesheets with future work dates');
      EntryEmpIDs[i] := Entries[i].EmpID;
    end;
    if not LogicDM.CanIManageEmployees(LimitationString, EntryEmpIDs, False, BadEmpID, BadEmpName) then
      raise EOdNotAllowed.CreateFmt('You do not have permissions to approve timesheets for employee %s', [BadEmpName]);
  end;

const
  ExistingSQL = 'update timesheet_entry set %s = %s, %s = %s, %s = %s where %s is null and entry_id in (%s) %s';
var
  RecsAffected: Integer;
  ByFieldName: string;
  DateFieldName: string;
  LocalFieldName: string;
  InString: string;
  ExtraWhere: string;
  InsertRecs: string;
  ApproveMessage: string;
  InsertCount: Integer;
  HasTSE: boolean;  //QM-833 Timesheet TSE EB
begin
  if Entries.Count < 1 then
    Exit;
  Current := Now;
  Result := '';
  ApproveMessage := '';

  VerifyApprovalPermissions;
  TSEList := TimesheetRecordList.Create; //QM-833 TSE Timesheet
  try
    if Final then begin
      ExtraWhere := ' and approve_by is not null ';
      ByFieldName := 'final_approve_by';
      DateFieldName := 'final_approve_date';
      LocalFieldName := 'final_approve_date_local';
      ApproveMessage := 'final ';
      HasTSE := False;   //QM-833 Timesheet TSE EB
    end
    else begin
      ExtraWhere := '';
      ByFieldName := 'approve_by';
      DateFieldName := 'approve_date';
      LocalFieldName := 'approve_date_local';
      HasTSE := LoadTSEList;  //QM-833 Timesheet TSE EB
    end;
    InString := EntryIDsToCommaString;
    if InString <> '' then begin
    // Set the approval date, local date, and approved by fields
      RecsAffected := LogicDM.ExecuteSQL(Format(ExistingSQL, [DateFieldName,  QuoteDate(Current),
        LocalFieldName, QuoteDate(LocalTime),
        ByFieldName, IntToStr(LogicDM.LoggedInEmpID),
        ByFieldName, InString, ExtraWhere]));
      if RecsAffected = 1 then
        Result := Format('1 entered timesheet was %sapproved.', [ApproveMessage])
      else
        Result := Format('%d entered timesheets were %sapproved.', [RecsAffected, ApproveMessage]);
    end;

  InsertRecs := InsertCommands(InsertCount);
  if InsertCount > 0 then begin
    if Final then
      raise Exception.Create('You can not final approve a timesheet until it has been submitted and approved');
    LogicDM.ExecuteSQL(InsertRecs);
    if Result <> '' then
      Result := Result + sLineBreak;
    if InsertCount = 1 then
      Result := Result + '1 non-entered timesheet was approved.'
    else
      Result := Result + Format('%d non-entered timesheets were approved.', [InsertCount]);
  end;

  if Result <> '' then
    Result := Format('Approvals by %s at %s:', [LogicDM.LoggedInEmpShortName, DateTimeToStr(LocalTime)])
      + sLineBreak + Result;

  if HasTSE then ApproveTimesheets(TRUE, LocalTime, TSEList);

  finally
    FreeAndNil(TSEList);  //QM-833 TSE Timesheet
  end;
end;



function TTimesheetDM.SearchTimesheets(const SearchType: Integer;
  const DateStart, DateStop: DateTime; const StartEmpId: Integer;
  const LevelLimit: Integer; const PayrollCenter: string): string;

  function GetTimesheetDownloadXml: string;
  const
    SelectSQL = 'select * from timesheet_entry where status <> ''%s'' and work_emp_id = %d ' +
      ' and work_date >= %s and work_date <= %s';
  var
    DataSet: TDataSet;
    SQL: string;
  begin
    Result := '';
    SQL := Format(SelectSQL, [TimesheetStatusOld, StartEmpID, QuoteDate(DateStart), QuoteDate(DateStop)]);
    Op.Phase := 'Running query';
    Op.Detail := 'Timesheet Approval Fetch: ' + IntToStr(StartEmpId);
    DataSet := LogicDM.GetDataSetForSQL(SQL);

    Op.Phase := 'Getting Time Rule';
    EmpTimeRuleData.Parameters.Items[0].Value := StartEmpId;
    EmpTimeRuleData.Open;

    Op.Phase := 'Encoding results';
    Result := ConvertDataSetsToSQLXML([DataSet, EmpTimeRuleData], ['timesheet_entry', 'employee']);
    DataSet.Close;
    EmpTimeRuleData.Close;
  end;

const
  Select =
    'select ds.d as work_date, e.h_emp_id as work_emp_id, e.h_short_name as short_name, t.* ' + sLineBreak +
    'from get_report_hier2(%d, 0, %d) e '+
    '  inner join dbo.DateSpan(%s, %s) ds on 1=1 '+
    '  left join timesheet_entry t on t.work_emp_id = e.h_emp_id and t.work_date =ds.d '+ sLineBreak +
    'where (t.work_emp_id is not null '+
    '  or (e.h_active=1 and (dbo.emp_has_right(e.h_emp_id, 18) = 1))) '+
    '  and (t.work_date is null or (%s)) ';
var
  SQL: TOdSqlBuilder;
  DateRange: string;
  StartStr: string;
  StopStr: string;
  RangeStopStr: string;
  WhereWithAnd: string;
begin
  Assert(StartEmpID > 0, 'A starting employee must be specified');
  Assert((DateStop > 1) and (DateStart > 1), 'Start/stop dates are required');
  if DateStop - DateStart > 7 then
    raise Exception.Create('You can only request 7 days of timesheets at once');

  if SearchType = TimesheetSearchXMLDownload then
    Result := GetTimesheetDownloadXml
  else begin
    if DateStop > (Today + 1) then
      raise Exception.Create('You can not request timesheets for dates in the future');
    StartStr := QuotedStr(IsoDateTimeToStr(DateStart));
    RangeStopStr := QuotedStr(IsoDateTimeToStr(DateStop - 1)); // DateSpan is inclusive
    StopStr := QuotedStr(IsoDateTimeToStr(DateStop));

    SQL := TOdSqlBuilder.Create;
    try
      SQL.AddWhereCondition(Format('t.status is null or t.status = ''%s'' or t.status = ''%s''', [TimesheetStatusActive, TimesheetStatusSubmit]));
      if SearchType = Ord(tsApproval) then begin
        SQL.AddIsNullSearch('t.approve_by');
        SQL.AddWhereCondition('e.h_emp_id <> ' + IntToStr(LogicDM.LoggedInEmpID));
      end
      else if SearchType = Ord(tsFinalApproval) then begin
        SQL.AddIsNullSearch('t.final_approve_by');
        SQL.AddIsNotNullSearch('t.approve_by');
        SQL.AddWhereCondition('e.h_emp_id <> ' + IntToStr(LogicDM.LoggedInEmpID));
      end;
      if not IsEmpty(PayrollCenter) then
        SQL.AddWhereCondition('dbo.get_employee_pc(e.h_emp_id, 1) = ' + QuotedStr(PayrollCenter));

      DateRange := Format('t.work_date >= %s and t.work_date < %s', [StartStr, StopStr]);
      WhereWithAnd := StringReplace(SQL.WhereClause, 'WHERE ', ' and ', []);
      Op.Phase := 'Running query';
      Op.Detail := SQL.WhereClause;

      LogicDM.SearchQuery.CommandText :=
        Format(Select, [StartEmpID, LevelLimit, StartStr, RangeStopStr, DateRange]) + WhereWithAnd;
      LogicDM.SearchQuery.Open;

      Op.Phase := 'Encoding results';
      Result := ConvertDataSetToXML([LogicDM.SearchQuery], 3000);
    finally
      FreeAndNil(SQL);
      LogicDM.SearchQuery.Close;
    end;
  end;
end;

procedure TTimesheetDM.ProcessBreakRuleResponses(ChangeList: BreakRuleResponseList; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from break_rules_response where ' +
  '(emp_id = :emp_id) and ' +
  '(rule_id = :rule_id) and ' +
  '(response_date = :response_date) and ' +
  '(response = :response) and ' +
  '((contact_phone = :contact_phone) or ((contact_phone is null) and (:contact_phone2 is null))) and ' +
  '((message_dest_id = :message_dest_id) or ((message_dest_id is null) and (:message_dest_id2 is null)))';
var
  i: Integer;
  Change: BreakRuleResponse;
  ResponseID: Integer;
  EmpID: Integer;
  RuleID: Integer;
  ResponseDate: TDateTime;
  ContactPhone: Variant;
  Response: string;
  NewResponseID: Integer;
  D: TADODataSet;
  MessageDestID: Variant;
begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessBreakRuleResponses');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessBreakRuleResponses');

  D := BreakRulesResponse;
  D.CommandText := Existing;
  // These 2 params need their DataType set manually. Delphi can't do it
  // automatically because the query only compares them to Null.
  D.Parameters.ParamByName('message_dest_id2').DataType := ftInteger;
  D.Parameters.ParamByName('contact_phone2').DataType := ftString;

  for i := 0 to ChangeList.Count - 1 do begin
    Op.Phase := Format('Break Rule Response %d of %d', [i + 1, ChangeList.Count]);
    Change := ChangeList[i];
    Assert(Assigned(Change));

    ResponseID := Change.ResponseID;
    EmpID := Change.EmpID;
    RuleID := Change.RuleID;
    ResponseDate := Change.ResponseDate;
    Response := Change.Response;
    if IsEmpty(Change.ContactPhone) then
      ContactPhone := Null
    else
      ContactPhone := Change.ContactPhone;
    if Change.MessageDestID = 0 then
      MessageDestID := Null
    else
      MessageDestID := Change.MessageDestID;

    if ResponseID > 0 then
      raise Exception.Create('Editing existing break_rules_response is not supported');

    Op.Detail := 'Check for existing BreakRulesResponse';
    D.Close;
    AssignParamsFromROComplex(D.Parameters, Change);
    D.Parameters.ParamByName('message_dest_id2').Value := D.Parameters.ParamByName('message_dest_id').Value;
    D.Parameters.ParamByName('contact_phone2').Value := D.Parameters.ParamByName('contact_phone').Value;
    D.Open;
    if D.RecordCount < 1 then begin
      Op.Detail := 'Adding new BreakRulesResponse';
      D.Insert;
      D.FieldByName('emp_id').AsInteger := EmpID;
      D.FieldByName('rule_id').AsInteger := RuleID;
      D.FieldByName('response_date').AsDateTime := ResponseDate;
      D.FieldByName('response').AsString := Response;
      D.FieldByName('contact_phone').Value := ContactPhone;
      D.FieldByName('message_dest_id').Value := MessageDestID;
      D.Post;
    end else if D.RecordCount > 1 then
      raise Exception.Create('More than one break_rules_response row matches the selection criteria.');

    NewResponseID := D.FieldByName('response_id').AsInteger;
    Assert(NewResponseID > 0, 'Negative new break_rules_response ID');
    LogicDM.UpdateDateTimeToIncludeMilliseconds('break_rules_response', 'response_id', NewResponseID, 'response_date', ResponseDate);
    AddIdentity(NewKeys, 'break_rules_response', ResponseID, NewResponseID);
  end;
end;

function TTimesheetDM.GetBreakRuleMessageID(const PCCode, BreakRuleType: string): Integer;
const
  SQL = 'select rule_id from break_rules where pc_code = ''%s'' and rule_type = ''%s''';
var
  DataSet: TDataSet;
begin
  Result := -1;
  DataSet := LogicDM.CreateDataSetForSQL(Format(SQL, [PCCode, BreakRuleType]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('rule_id').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TTimesheetDM.GetEmployeeProfitCenter(const EmpID: Integer; const PayrollPC: Boolean=False): string;
const
  SQL = 'select dbo.get_employee_pc(%d, %s) as pc_code';
var
  DataSet: TDataSet;
begin
  Result := '';
  DataSet := LogicDM.CreateDataSetForSQL(Format(SQL, [EmpID, BooleanToString01(PayrollPC)]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('pc_code').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TTimesheetDM.GetEmployeeShortName(const EmpID: Integer): string;
const
  SQL = 'select short_name from employee where emp_id=';
var
  DataSet: TDataSet;
begin
  Result := '-';
  DataSet := LogicDM.CreateDataSetForSQL(SQL + IntToStr(EmpID));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.Fields[0].AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TTimesheetDM.EmpIsTSE(EmpID: integer; EntryID: integer = 0): boolean;     //QM-833 Timesheet TSE EB
const
  SQLEmp = 'Select empr.emp_id, empr.emp_right_id, empr.limitation from employee_right empr ' +
        'inner join right_definition def ' +
        'on def.right_id = empr.right_id ' +
        'where (def.entity_data = ''TimesheetsEntry'') ' +
        'and empr.emp_id = ';

  SQLEntry = 'Select ts.work_emp_id, ts.source from timesheet_entry ts where entry_id = ';
var
  DataSet: TDataSet;
  Limitation: string;
  lSource: string;
begin
  Result := False;
  Limitation := '';
  try
    if EntryID > 0 then begin
      DataSet := LogicDM.CreateDataSetForSQL(SQLEntry + IntToStr(EntryID));
      if (DataSet.RecordCount > 0) then begin
        lSource := DataSet.FieldByName('source').AsString;
        if (lSource = TimesheetTSESource) then
          Result := True;
      end;
    end
    else begin {Just checking if the employee has TSE rights}
      DataSet := LogicDM.CreateDataSetForSQL(SQLEmp + IntToStr(EmpID));
      if Dataset.RecordCount > 0 then begin
        Limitation := Dataset.FieldByName('limitation').AsString;
        if UpperCase(Limitation) = TimesheetTSESource then
          Result := True;
      end;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TTimesheetDM.ExportTimesheets(const ProfitCenter: string; const WeekEnding: TDateTime): string;
var
  Exporter: TPayrollExporter;

  function DoExport: string;
  var
    WEDate: TDateTime;
  begin
    WEDate := EndOfTheDay(WeekEnding);
    Result := Exporter.ExportData(ProfitCenter, WEDate, LogicDM, TimesheetExport); //QMANTWO-650
  end;

begin
  // Current, in active use as of 2012: Dynamics
  if LogicDM.GetServerSetting('Timesheets', 'ExportEmployees', '1') = '1' then begin
    Op.Phase := 'Export Employees';
    Exporter := TDynamicsEmployeeExporter.Create;
    try
      Result := DoExport;
    finally
      FreeAndNil(Exporter);
    end;
  end;

  if LogicDM.GetServerSetting('Timesheets', 'ExportToDynamics', '1') = '1' then begin
    Op.Phase := 'Dynamics Export';
    Exporter := TDynamicsTimeSheetExporter.Create;
    try
      Result := DoExport;
    finally
      FreeAndNil(Exporter);
    end;
  end;
end;

end.
