object FaxDispositionDM: TFaxDispositionDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 400
  Width = 686
  object Conn: TADOConnection
    CommandTimeout = 20
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=TestDB;Data Source=localhost' +
      ';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=409' +
      '6;Workstation ID=DEV-LAPTOP;Use Encryption for Data=False;Tag wi' +
      'th column collation when possible=False;'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 8
    Top = 16
  end
  object SetSuccess: TADOCommand
    CommandText = 
      'update fax_message set fax_status = '#39'Success'#39', complete_date = :' +
      'date, ext_disposition = :ext_disposition where fm_id = :id'
    Connection = Conn
    Parameters = <
      item
        Name = 'date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'ext_disposition'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 72
    Top = 32
  end
  object SetFailure: TADOCommand
    CommandText = 
      'update fax_message set fax_status = '#39'Failed'#39', complete_date = :d' +
      'ate, error_details = :details, ext_disposition = :ext_dispositio' +
      'n where fm_id = :id'
    Connection = Conn
    Parameters = <
      item
        Name = 'date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'ext_disposition'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 72
    Top = 88
  end
  object LogResp: TADOStoredProc
    Connection = Conn
    ProcedureName = 'log_fax_as_response;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FmId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@StatusDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Success'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 72
    Top = 152
  end
  object GetSystemEmpID: TADODataSet
    Connection = Conn
    CommandText = 'select dbo.GetConfigValue('#39'SystemUserEmpID'#39', null)'
    Parameters = <>
    Left = 160
    Top = 32
  end
end
