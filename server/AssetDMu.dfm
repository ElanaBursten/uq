inherited AssetDM: TAssetDM
  OldCreateOrder = True
  Height = 188
  Width = 210
  object AssetData: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select * from asset'#13#10'where asset_id in'#13#10'  (select asset_id from ' +
      'asset_assignment where emp_id = :EmpID)'
    EnableBCD = False
    Parameters = <
      item
        Name = 'EmpID'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    Left = 49
    Top = 21
  end
  object EmployeeAssets: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select asset.asset_id, asset.asset_number, asset.asset_code'#13#10'fro' +
      'm asset_assignment'#13#10'  inner join asset on asset.asset_id = asset' +
      '_assignment.asset_id'#13#10'where asset_assignment.emp_id = :EmpID'#13#10'  ' +
      'and asset_assignment.active = 1'
    EnableBCD = False
    Parameters = <
      item
        Name = 'EmpID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 49
    Top = 69
  end
  object AssetAssignmentData: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from asset_assignment'#13#10'where emp_id = :EmpID'
    EnableBCD = False
    Parameters = <
      item
        Name = 'EmpID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 53
    Top = 117
  end
end
