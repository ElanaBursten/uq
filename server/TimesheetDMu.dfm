inherited TimesheetDM: TTimesheetDM
  OldCreateOrder = True
  Height = 408
  Width = 627
  object TimesheetEntry: TADODataSet
    CommandText = 'select * from timesheet_entry where 0=1'
    Parameters = <>
    Left = 52
    Top = 24
  end
  object CurrentTimesheet: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select entry_id, final_approve_by from timesheet_entry'#13#10'where st' +
      'atus in ('#39'ACTIVE'#39', '#39'SUBMIT'#39')'#13#10'  and work_emp_id = :WorkEmpID'#13#10'  ' +
      'and work_date = :WorkDate'
    Parameters = <
      item
        Name = 'WorkEmpID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'WorkDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 48
    Top = 80
  end
  object TimesheetsForEmp: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select * from timesheet_entry'#13#10'where status in ('#39'ACTIVE'#39', '#39'SUBMI' +
      'T'#39')'#13#10'  and work_emp_id = :WorkEmpID'#13#10'  and work_date >= :StartWo' +
      'rkDate'#13#10'  and work_date <= :EndWorkDate'
    Parameters = <
      item
        Name = 'WorkEmpID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'StartWorkDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EndWorkDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 48
    Top = 136
  end
  object GetTimesheetChanges: TADOStoredProc
    ProcedureName = 'get_tse_audit;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@WorkEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SheetsForID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 25
  end
  object OriginalTime: TADODataSet
    CommandText = 'select * from timesheet_entry where 0=1'
    Parameters = <>
    Left = 160
    Top = 81
  end
  object BreakRulesResponse: TADODataSet
    CommandText = 
      '/* SQL replaced at runtime */'#13#10'select * from break_rules_respons' +
      'e'#13#10'where 0=1'
    Parameters = <>
    Left = 152
    Top = 202
  end
  object TimesheetExport: TADOStoredProc
    CacheSize = 100
    CursorType = ctOpenForwardOnly
    CommandTimeout = 180
    ProcedureName = 'payroll_export_solomon2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BEGIN_DATE'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@END_DATE'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@REGION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end>
    Left = 288
    Top = 24
  end
  object GetFlHolidays: TADOStoredProc
    ProcedureName = 'get_emp_fl_holidays'
    Parameters = <
      item
        Name = '@EmpID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@WeekStart'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@WeekEnd'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Year'
        DataType = ftInteger
        Value = Null
      end>
    Left = 288
    Top = 80
  end
  object EmpTimeRuleData: TADODataSet
    CursorType = ctStatic
    CommandText = 'select emp_id, timerule_id from employee where emp_id=:emp_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 156
    Top = 140
  end
  object GetSickLeavePTOQry: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from employee_balance'#13#10'where emp_id=:emp_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 284
    Top = 148
  end
end
