unit EventSource2007;
{
  This unit contains interim code so QMLogic (D2007) can save events.
  It will eventually be replaced by service2\EventSource.
}

interface

uses
  SysUtils, Classes, Contnrs, QMEventsNoThrift;

type
  IEventPublisher = interface
    ['{8C474EBE-E896-4E7F-8559-E01A4CDFD6DA}']
    procedure Publish(Event: IEvent);
  end;

  IEventStore = interface
    ['{29941354-7848-4E9B-859F-8228F9BECB69}']
    procedure SaveEvents(Key: IAggrKey; EventList: IEventList; ExpectedVersion: Integer);
    function GetEventsForAggregate(Key: IAggrKey): IEventList;
  end;

  TEventDescriptor = class
  public
    EventData: IEvent;
    AggKey: TAggrKeyImpl;
    Version: Integer;
    constructor Create(AKey: IAggrKey; AEventData: IEvent; AVersion: Integer = -1);
  end;

  TBaseEventStore = class(TInterfacedObject, IEventStore)
  private
    Publisher: IEventPublisher;
  protected
    // Subclasses must implement this to get all events for an aggregate from the event store
    function LoadEventDescriptorsForAggregate(Key: IAggrKey): TObjectList; virtual; abstract;
    // Subclasses must implement this to add events to the event store;
    procedure PersistEventDescriptors(Key: IAggrKey; NewEventDescriptors: TObjectList; ExpectedVersion: Integer); virtual; abstract;
    procedure SaveEvents(Key: IAggrKey; EventList: IEventList; ExpectedVersion: Integer=-1); // -1 means no ExpectedVersion check.
    function GetEventsForAggregate(Key: IAggrKey): IEventList;
  public
    constructor Create(APublisher: IEventPublisher); virtual;
  end;

  // Exceptions:
  EEventStoreError = class(Exception);
  EAggregateNotFound = class(EEventStoreError);
  EConcurrencyException = class(EEventStoreError);

implementation

constructor TEventDescriptor.Create(AKey: IAggrKey; AEventData: IEvent; AVersion: Integer);
begin
  inherited Create();
  AggKey := TAggrKeyImpl(AKey);
  EventData := AEventData;
  Version := AVersion;
end;

constructor TBaseEventStore.Create(APublisher: IEventPublisher);
begin
  Publisher := APublisher;
end;

function TBaseEventStore.GetEventsForAggregate(Key: IAggrKey): IEventList;
var
  EventDescriptors: TObjectList;
  EvDesc: TEventDescriptor;
  I: Integer;
begin
  EventDescriptors := LoadEventDescriptorsForAggregate(TAggrKeyImpl(Key));
  if (EventDescriptors = nil) or (EventDescriptors.Count < 1) then
    raise EAggregateNotFound.CreateFmt('No events for Aggregate %s', [Key.ToString]);

  Result := TEventListImpl.Create;
  for I := 0 to EventDescriptors.Count - 1 do begin
    EvDesc := EventDescriptors.Items[I] as TEventDescriptor;
    Result.Items.Add(TEventImpl(EvDesc.EventData));
  end;
end;

procedure TBaseEventStore.SaveEvents(Key: IAggrKey;
  EventList: IEventList; ExpectedVersion: Integer);
var
  EventDescriptors: TObjectList;
  I: Integer;
  J: Integer;
  Event: TEventImpl;
begin
  EventDescriptors := TObjectList.Create;
  try
    I := ExpectedVersion;
    for J := 0 to EventList.Items.Count - 1 do begin
      Event := TEventImpl(EventList.Items[J]);
      Inc(I);
      EventDescriptors.Add(TEventDescriptor.Create(Key, Event, I));
    end;

    PersistEventDescriptors(Key, EventDescriptors, ExpectedVersion);

    if not Assigned(Publisher) then
      Exit;

    // The Events were saved, so publish their availabilty.
    for J := 0 to EventList.Items.Count - 1 do begin
      Event := EventList.Items[J] as TEventImpl;
      Publisher.Publish(Event);
    end;
  finally
    FreeAndNil(EventDescriptors);
  end;
end;

end.
