unit standaloneServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uROClient, uROServer, uROIndyTCPServer, uROIndyHTTPServer,
  uROSOAPMessage, uROBinMessage, QMServerProcessU, StdCtrls;

type
  TStandaloneServerForm = class(TForm)
    ROSOAPMessage1: TROSOAPMessage;
    ROIndyHTTPServer1: TROIndyHTTPServer;
    ROBinMessage1: TROBinMessage;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    QMS: TQMServerProcess;
  end;

var
  StandaloneServerForm: TStandaloneServerForm;

implementation

{$R *.dfm}

procedure TStandaloneServerForm.FormCreate(Sender: TObject);
begin
  QMS := TQMServerProcess.Create;
  ROIndyHTTPServer1.Active := True;
end;

procedure TStandaloneServerForm.FormDestroy(Sender: TObject);
begin
  ROIndyHTTPServer1.Active := False;
  FreeAndNil(QMS);
end;

end.


