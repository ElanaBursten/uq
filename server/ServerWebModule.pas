unit ServerWebModule;

interface

uses
  SysUtils, Classes, HTTPApp,
  uROClient, uROSOAPMessage, uROServer, uROWebBrokerServer, uROBINMessage,
  uROBaseConnection,
  QMServerProcessU;

type
  TWebModule1 = class(TWebModule)
    SoapMsg: TROSOAPMessage;
    ROServer: TROWebBrokerServer;
    BinMsg: TROBINMessage;
    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleDestroy(Sender: TObject);
    procedure WebModule1MonitorActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1CheckStartupActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure BinMsgCompress(OriginalSize, CompressedSize,
      CompressionTime: Integer);
    procedure BinMsgDecompress(OriginalSize, CompressedSize,
      CompressionTime: Integer);
    procedure WebModule1CpuImageAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1FaxStatusActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    QMS: TQMServerProcess;
  end;

var
  WebModule1: TWebModule1;

implementation

{$R *.DFM}

uses
  OperationTracking, FaxDispositionDMu;

procedure TWebModule1.WebModuleCreate(Sender: TObject);
begin
  QMS := TQMServerProcess.Create;
end;

procedure TWebModule1.WebModuleDestroy(Sender: TObject);
begin
  FreeAndNil(QMS);
end;

procedure TWebModule1.WebModule1MonitorActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.ContentType := 'text/html';
  Response.Content := QMS.GetMonitorHtml;
  Handled := True;
end;

procedure TWebModule1.WebModule1CheckStartupActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.ContentType := 'text/plain';
  Response.Content := QMS.GetStartupMessage;
end;

procedure TWebModule1.BinMsgCompress(OriginalSize, CompressedSize, CompressionTime: Integer);
begin
  OperationTracking.CompressionStats(OriginalSize, CompressedSize, CompressionTime);
end;

procedure TWebModule1.BinMsgDecompress(OriginalSize, CompressedSize, CompressionTime: Integer);
begin
  OperationTracking.DeCompressionStats(OriginalSize, CompressedSize, CompressionTime);
end;

procedure TWebModule1.WebModule1CpuImageAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.ContentType := 'image/bmp';
  Response.ContentStream := OperationTracking.GetOperationTracker.GetBitmapStream;
end;

procedure TWebModule1.WebModule1FaxStatusActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  QMS.GetFaxDM.ProcesseFaxDisposition(Request.ContentFields.Values['xml']);

  //Respond to eFax Developer that the disposition was successfully received.
  Response.ContentType := 'text/html';
  Response.Content := '<html><body>Post Successful</body></html>';
end;

end.

