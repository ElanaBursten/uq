unit RemObjectsUtils;

interface

uses SysUtils, DB, ADODB, Variants, uRoTypes,
{$IFDEF ROSDK5UP}
  uRoClasses,
{$ENDIF}
  OdMiscUtils, OdIsoDates, QMServerLibrary_Intf;

type
  TBlankValueAction = (bvError, bvNull, bvZero, bvDefault, bvNegativeOne);

  TROArrayHelper = class helper for TROArray
    procedure Preallocate(const ArraySize: Integer);
    function Element(const Idx: Integer): TROComplexType;
  end;

  TROComplexTypeHelper = class helper for TROComplexType
    function HasProperty(const PropName: string): Boolean;
  end;

function ExecuteSQLWithParams(Conn: TADOConnection; const SQL: string; NVPairs: NVPairList): Integer; overload;
function ExecuteSQLWithParams(Conn: TADOConnection; const SQL: string; ROData: TROComplexType; const MapZeroToNull: Boolean=True): Integer; overload;
function IntArrayToCommaString(Ints: IntegerList): string;
function IntegerListToArray(IntList: IntegerList): TIntegerArray;
procedure AddIdentity(List: GeneratedKeyList; const Table: string; Old, New: Integer);
function GetGeneratedIdentity(List: GeneratedKeyList; const Table: string; OldKey: Integer; var NewKey: Variant): Boolean;
function ColumnNameToROName(ColName: string): string;
procedure AssignParamsFromROComplex(Parameters: TParameters; ROData: TROComplexType; MapZeroToNull: Boolean=True);
procedure LoadROArrayFromDataSet(ROArray: TROArray; Dataset: TDataSet; IncludeModifiedDate: Boolean = False);

procedure ConvertNVPairListListToROArray(NVData: NVPairListList; ROArray: TROArray);

// TODO: The Name/Value proc usage needs to be reviewed. See JIRA issue QMAN-2411.
function GetNameValue(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction; const Default: string = ''): Variant;
procedure SetNVPairInteger(PairList: NVPairList; const ValueName: string; Value: Integer);
function NVPairInteger(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
function NVPairString(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction; const Default: string = ''): Variant;
function NVPairDateTime(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
function NVPairTime(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
function NVPairFloat(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
function NVPairBoolean(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
function NVPairListToText(PairList: NVPairList): string;
procedure AssignParamsFromNVPairList(Parameters: TParameters; NVPairs: NVPairList);


implementation

uses
  QMConst, TypInfo, JclStrings;

function ExecuteSQLWithParams(Conn: TADOConnection; const SQL: string; NVPairs: NVPairList): Integer;
var
  Command: TADOCommand;
  RecordsAffected: Integer;
begin
  Assert(Assigned(Conn));
  Command := TADOCommand.Create(nil);
  try
    Command.Connection := Conn;
    Command.CommandText := SQL;
    AssignParamsFromNVPairList(Command.Parameters, NVPairs);
    Command.ExecuteOptions := [eoExecuteNoRecords];
    Command.Execute(RecordsAffected, EmptyParam);
    Result := RecordsAffected;
  finally
    FreeAndNil(Command);
  end;
end;

function ExecuteSQLWithParams(Conn: TADOConnection; const SQL: string; ROData: TROComplexType; const MapZeroToNull: Boolean): Integer; overload;
var
  Command: TADOCommand;
  RecordsAffected: Integer;
begin
  Assert(Assigned(Conn));
  Command := TADOCommand.Create(nil);
  try
    Command.Connection := Conn;
    Command.CommandText := SQL;
    AssignParamsFromROComplex(Command.Parameters, ROData, MapZeroToNull);
    Command.ExecuteOptions := [eoExecuteNoRecords];
    Command.Execute(RecordsAffected, EmptyParam);
    Result := RecordsAffected;
  finally
    FreeAndNil(Command);
  end;
end;

function IntArrayToCommaString(Ints: IntegerList): string;
var
  i: Integer;
  Value: string;
begin
  Result := '';
  for i := 0 to Ints.Count - 1 do begin
    Value := IntToStr(Ints[i]);
    if Result = '' then
      Result := Value
    else
      Result := Result + ',' + Value;
  end;
end;

function GetNameValue(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction; const Default: string = ''): Variant;
var
  i: Integer;
begin
  for i := 0 to PairList.Count - 1 do begin
    if SameText(PairList[i].Name, ValueName) then begin
      Result := PairList[i].Value;
      if Result = '' then begin
        case BlankAction of
          bvError: raise Exception.CreateFmt('The parameter named %s requires a value', [ValueName]);
          bvNull: Result := Null;
          bvZero: Result := 0;
          bvNegativeOne: Result := -1;
          bvDefault: Result := Default;
        end;
      end;
      Exit;
    end;
  end;

  // When the pair is not present, treat that as blank:
  case BlankAction of
    bvError: raise Exception.CreateFmt('Unable to find value named %s', [ValueName]);
    bvNull: Result := Null;
    bvZero: Result := 0;
    bvDefault: Result := Default;
    bvNegativeOne: Result := -1;
    else Result := 0;
  end;
end;

procedure SetNVPairInteger(PairList: NVPairList; const ValueName: string; Value: Integer);
var
  i: Integer;
begin
  Assert(Assigned(PairList));
  Assert(Trim(ValueName) <> '');
  for i := 0 to PairList.Count - 1 do begin
    if SameText(PairList[i].Name, ValueName) then begin
      PairList[i].Value := IntToStr(Value);
      Break;
    end;
  end;
end;

function NVPairInteger(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction);
  if VarIsNull(Value) then
    Result := Null
  else try
    Result := StrToInt(Value);
  except
    on E: Exception do begin
      E.Message := 'Invalid value for named pair ' + ValueName + ': ' + Value;
      raise;
    end;
  end;
end;

function NVPairString(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction; const Default: string = ''): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction, Default);
  if VarIsNull(Value) then
    Result := Null
  else
    Result := ReplaceControlChars(Value);
end;

function NVPairDateTime(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction);
  if VarIsNull(Value) then
    Result := Null
  else try
    Result := IsoStrToDateTimeDef(Value);
  except
    on E: Exception do begin
      E.Message := 'Invalid date time value for named pair ' + ValueName + ': ' + Value;
      raise;
    end;
  end;
end;

function NVPairTime(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction);
  if VarIsNull(Value) then
    Result := Null
  else try
    Result := IsoStrToTime(Value);
  except
    on E: Exception do begin
      E.Message := 'Invalid time value for named pair ' + ValueName + ': ' + Value;
      raise;
    end;
  end;
end;

function NVPairFloat(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction);
  if VarIsNull(Value) then
    Result := 0.0   //  QMANTWO-277
  else try
    Result := StrToFloat(Value);
  except on E: Exception do begin
      E.Message := 'Invalid value for named pair ' + ValueName + ': ' + Value;
      raise;
    end;
  end;
end;

function NVPairBoolean(PairList: NVPairList; const ValueName: string; BlankAction: TBlankValueAction): Variant;
var
  Value: Variant;
begin
  Value := GetNameValue(PairList, ValueName, BlankAction);
  if VarIsNull(Value) then
    Result := Null
  else try
    Result := StrToBool(Value);
  except
    on E: Exception do begin
      E.Message := 'Invalid boolean value for named pair ' + ValueName + ': ' + Value;
      raise;
    end;
  end;
end;

function NVPairListToText(PairList: NVPairList): string;
var
  i: Integer;
  Value: string;
  Name: string;
begin
  Result := '';
  for i := 0 to PairList.Count-1 do begin
    Value := PairList.Items[i].Value;
    Name := PairList.Items[i].Name;
    Result := Result + Name + '=' + Value + ',';
  end;
  if Length(Result) > 0 then
    Delete(Result, Length(Result), 1);
end;

procedure AssignParamsFromNVPairList(Parameters: TParameters; NVPairs: NVPairList);
var
  ParamIndex: Integer;
  ParamName: string;
begin
  for ParamIndex := 0 to Parameters.Count - 1 do begin
    ParamName := Parameters[ParamIndex].Name;
    if Parameters[ParamIndex].DataType = ftInteger then
      Parameters[ParamIndex].Value := NVPairInteger(NVPairs, ParamName, bvNull)
    else if Parameters[ParamIndex].DataType = ftFloat then
      Parameters[ParamIndex].Value := NVPairFloat(NVPairs, ParamName, bvNull)
    else if Parameters[ParamIndex].DataType = ftDateTime then
      Parameters[ParamIndex].Value := NVPairDateTime(NVPairs, ParamName, bvNull)
    else if Parameters[ParamIndex].DataType = ftBoolean then
      Parameters[ParamIndex].Value := NVPairBoolean(NVPairs, ParamName, bvZero)
    else
      Parameters[ParamIndex].Value := NVPairString(NVPairs, ParamName, bvNull);
  end;
end;

procedure AddIdentity(List: GeneratedKeyList; const Table: string; Old, New: Integer);
var
  GenKey: GeneratedKey;
begin
  Assert(Assigned(List));
  GenKey := List.Add;
  GenKey.TableName := Table;
  GenKey.TempKeyValue := Old;
  GenKey.KeyValue := New;
end;

function GetGeneratedIdentity(List: GeneratedKeyList; const Table: string; OldKey: Integer; var NewKey: Variant): Boolean;
var
  GenKey: GeneratedKey;
  i: Integer;
begin
  NewKey := Null;
  Result := False;
  Assert(Assigned(List));
  for i := 0 to List.Count - 1 do begin
    GenKey := List.Items[i];
    Result := SameText(Table, GenKey.TableName) and (OldKey = GenKey.TempKeyValue);
    if Result then begin
      NewKey := GenKey.KeyValue;
      Break;
    end;
  end;

  Assert((NewKey = Null) or (NewKey > 0));
end;

function IntegerListToArray(IntList: IntegerList): TIntegerArray;
var
  i: Integer;
begin
  SetLength(Result, IntList.Count);
  for i := 0 to IntList.Count - 1 do
    Result[i] := IntList.Items[i];
end;

procedure AssignParamsFromROComplex(Parameters: TParameters; ROData: TROComplexType; MapZeroToNull: Boolean);
var
  I: Integer;
  Param: TParameter;
  ROName: string;
  V: Variant;
begin
  for I := 0 to Parameters.Count - 1 do begin
    Param := Parameters[I];
    try
      ROName := ColumnNameToROName(Param.Name);
      if ROData.HasProperty(ROName) then
        V := ROData.GetFieldValue(ROName)
      else
        V := Null;
      if VarIsStr(V) and (V='') then
        Param.Value := Null
      else begin
        if MapZeroToNull and VarIsNumeric(V) and (V=0) then
          Param.Value := Null
        else
          Param.Value := V;
      end;
    except
      on E: Exception do begin
        E.Message := E.Message + ' param ' + Param.Name;
        raise;
      end;
    end;
  end;
end;

// convert a db column name to an RO property name (remove _ and make CamelCase)
function ColumnNameToROName(ColName: string): string;
var
  Done: Boolean;
  Start: PChar;
  Name, Word: string;
begin
  Result := '';
  Name := StringReplace(ColName, '_', ' ', [rfReplaceAll]);
  Start := Pointer(Name);
  repeat
    Done := StrWord(Start, Word);
    if Word <> '' then begin
      Word := StrProper(Word);
      Result := Result + Word;
    end;
  until Done;
end;

procedure LoadROArrayFromDataSet(ROArray: TROArray; Dataset: TDataSet; IncludeModifiedDate: Boolean);
var
  ROPropName: string;

  procedure SetValue(Item: TROComplexType; Field: TField);
  var
    V: Variant;
  begin
    Assert(Assigned(Item), 'RO Item not assigned');
    try
      V := Field.Value;
      if (Field.DataType in [ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency,
        ftBCD, ftAutoInc, ftLargeint, ftDateTime]) and VarIsNull(V) then begin
        // numeric, so the RO data probably can't accept a Null
        Item.SetFieldValue(ROPropName, 0);
      end else
        Item.SetFieldValue(ROPropName, V);
    except
      on E: Exception do begin
        E.Message := E.Message + ' in structure ' + Item.ClassName;
        raise;
      end;
    end;
  end;

var
  RowIdx: Integer;
  FldIdx: Integer;
  Field: TField;
  Item: TROComplexType;
begin
  Assert(Assigned(ROArray), 'ROArray not defined');
  Assert(ROArray.GetItemClass.InheritsFrom(TROComplexType),
    'ROArray Items must be TROComplexType children');

  RowIdx := 0;
  if not DataSet.IsEmpty then begin
    ROArray.Preallocate(Dataset.RecordCount);
    while not DataSet.Eof do begin
      Item := ROArray.Element(RowIdx);
      for FldIdx := 0 to DataSet.FieldCount - 1 do begin
        Field := DataSet.Fields[FldIdx];
        ROPropName := ColumnNameToROName(Field.FieldName);
        if Item.HasProperty(ROPropName) then begin
          if IncludeModifiedDate then begin
            if not StringInArray(Field.FieldName, NeverSendFieldNamesNoModifiedDate) then
              SetValue(Item, Field);
          end
          else if not StringInArray(Field.FieldName, NeverSendFieldNames) then
            SetValue(Item, Field);
        end;
      end;
      DataSet.Next;
      Inc(RowIdx);
    end;
  end;
end;

procedure ConvertNVPairListListToROArray(NVData: NVPairListList; ROArray: TROArray);
var
  i: Integer;
  Old: NVPairList;
  New: TROComplexType;
  j: Integer;
  OldName: string;
  NewName: string;
  V: Variant;
begin
  Assert(Assigned(NVData), 'NV is unassigned');
  Assert(Assigned(ROArray), 'RO is unassigned');

  ROArray.Preallocate(NVData.Count);
  for i := 0 to NVData.Count - 1 do begin
    Old := NVData.Items[i];
    Assert(Assigned(Old));

    New := ROArray.Element(i);
    try
      for j := 0 to Old.Count - 1 do begin
        OldName := Old.Items[j].Name;
        NewName := ColumnNameToROName(OldName);
        if New.HasProperty(NewName) then begin
          if (PropIsType(New, NewName, tkFloat) and StrContainsText('_date', OldName)) then
            V := NVPairDateTime(Old, OldName, bvZero)
          else if PropIsType(New, NewName, tkInteger) then
            V := NVPairInteger(Old, OldName, bvZero)
          else
            V := NVPairString(Old, OldName, bvDefault);
          New.SetFieldValue(NewName, V);
        end;
      end;
    except
      on E: Exception do begin
        E.Message := E.Message + ' upgrading NVPairs to struct ' + New.ClassName;
        raise;
      end;
    end;
  end;
end;

// TROArrayHelper class helpers for TROArray.
procedure TROArrayHelper.Preallocate(const ArraySize: Integer);
var
  i: Integer;
begin
  Clear;
  Resize(ArraySize);

  // TODO: This for loop is only needed for the ancient RemObjects used in QM
  for i := 0 to ArraySize - 1 do
    SetItemRef(i, TROComplexType(TROComplexTypeClass(GetItemClass).Create));
end;

function TROArrayHelper.Element(const Idx: Integer): TROComplexType;
begin
  Assert((Idx < Count) and (Idx > -1), 'Idx must be between 0 and ' + IntToStr(Count));
  Assert(GetItemClass.InheritsFrom(TROComplexType),
    'ROArray Items must be TROComplexType children');

  Result := TROComplexType(Self.GetItemRef(Idx));
end;

// TROComplexTypeHelper class helpers for TROComplexType.
function TROComplexTypeHelper.HasProperty(const PropName: string): Boolean;
begin
  Result := (GetPropInfo(Self, PropName) <> nil);
end;

end.
