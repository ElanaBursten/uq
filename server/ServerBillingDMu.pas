unit ServerBillingDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, QMServerLibrary_Intf,
  OdMiscUtils, BaseLogicFeatureDMu, RemObjectsUtils;

type
  TServerBillingDM = class(TLogicFeatureDM)
    BillingCommitSP: TADOStoredProc;
    BillingUncommitSP: TADOStoredProc;
    BillingDeleteSp: TADOStoredProc;
    BillingReexportSP: TADOStoredProc;
    BillingCallCenterGroup: TADODataSet;
    CenterGroup: TADODataSet;
    BillableCenterGroup: TADODataSet;
    BillingAdjustment: TADODataSet;
    InsertBillingRun: TADODataSet;
    BillingQueueData: TADODataSet;
  private
    function VerifyBillingRights(Right: string; CallCenterGroup: Integer): Boolean;
    procedure BillingCommit(BillID, CallCenterGroupID: Integer);
    procedure BillingDelete(BillID, CallCenterGroupID: Integer);
    procedure BillingReexport(BillID, CallCenterGroupID: Integer);
    procedure BillingUncommit(BillID, CallCenterGroupID: Integer);
    function GetCallCenterGroupIDForRunID(const RunID: Integer): Integer;
    function GetBillableCenterGroupIDList(BillingRights: array of string): TStringArray;
    function GetCenterGroup(GroupID: Integer): TStringArray;
  public
    function BillingAction(const Params: NVPairList): string;
    function BillingAdjustmentSearch(const Params: NVPairList): string;
    function BillingHeaderSearch(const Params: NVPairList): string;
    function BillingRunSearch(const Params: NVPairList): string;
    procedure CancelBillingRun(const Params: NVPairList);
    function GetBillingAdjustment(const AdjustmentID: Integer): string;
    function GetBillingRunLog(const Params: NVPairList): string;
    function SaveBillingAdjustments(const BillingAdjustments: NVPairListList): GeneratedKeyList;
    procedure StartBilling(const Params: NVPairList);
  end;

implementation

uses OdAdoUtils, OdDbUtils, OdIsoDates, OdDataSetToXml, OdSqlBuilder,
  QMConst, LogicDMu, JclStrings, Variants;

{$R *.dfm}

{ TBillingDM }

function TServerBillingDM.VerifyBillingRights(Right: string; CallCenterGroup: Integer): Boolean;
const
  Selecting = 'select center_group_id from center_group where group_code in (%s)';
var
  LimitationString: string;
  AllowedCenters: string;
  D: TDataSet;
begin
  Result := LogicDM.CanI(Right, LimitationString, LogicDM.LoggedInEmpID);
  if (Result = False) or (StringInArray(Trim(LimitationString), ['', '-'])) then
    Exit;

  AllowedCenters := StringToDelimitedString(LimitationString, '''', ',');
  D := LogicDM.GetDatasetForSQL(Format(Selecting, [AllowedCenters]));
  if not D.IsEmpty then
    Result := False;

  while not D.EOF do begin
    if CallCenterGroup = D.FieldByName('center_group_id').AsInteger then begin
      Result := True;
      Break;
    end;
    D.Next;
  end;
  D.Close;
end;

function TServerBillingDM.BillingAction(const Params: NVPairList): string;
var
  BillID: Integer;
  BillAction: string;
  CallCenterGroupID: Integer;
begin
  BillID := NVPairInteger(Params, 'bill_id', bvError);
  BillAction := NVPairString(Params, 'bill_action', bvError);
  CallCenterGroupID := NVPairInteger(Params, 'call_center_group_id', bvError);

  if BillID = 0 then
    raise Exception.Create('A bill ID must be specified');

  // BillAction will be one of: COMMIT, UNCOMMIT, DELETE, REEXPORT
  if BillAction = 'COMMIT' then
    BillingCommit(BillID, CallCenterGroupID)
  else if BillAction = 'UNCOMMIT' then
    BillingUncommit(BillID, CallCenterGroupID)
  else if BillAction = 'DELETE' then
    BillingDelete(BillID, CallCenterGroupID)
  else if BillAction = 'REEXPORT' then
    BillingReexport(BillID, CallCenterGroupID)
  else
    raise Exception.Create('Unrecognized billing action specified ('+ BillAction + ')');
end;

procedure TServerBillingDM.BillingCommit(BillID: Integer; CallCenterGroupID: Integer);
begin
  SetName('BillingCommit');
  if not VerifyBillingRights(RightBillingCommit, CallCenterGroupID) then
    raise Exception.Create('You do not have permission to commmit the selected billing');
  BillingCommitSP.Parameters.ParamValues['@BillID'] := BillID;
  BillingCommitSP.Parameters.ParamValues['@CommittedBy'] := LogicDM.LoggedInEmpID;
  BillingCommitSP.ExecProc;
  CheckForAdoError(BillingCommitSP.Connection);
end;

procedure TServerBillingDM.BillingDelete(BillID: Integer; CallCenterGroupID: Integer);
begin
  SetName('BillingDelete');
  if not VerifyBillingRights(RightBillingDelete, CallCenterGroupID) then
    raise Exception.Create('You do not have permission to delete the selected billing');
  BillingDeleteSP.Parameters.ParamValues['@BillID'] := BillID;
  BillingDeleteSP.ExecProc;
  CheckForAdoError(BillingDeleteSP.Connection);
end;

procedure TServerBillingDM.BillingUncommit(BillID: Integer; CallCenterGroupID: Integer);
begin
  SetName('BillingUncommit');
  if not VerifyBillingRights(RightBillingUnCommit, CallCenterGroupID) then
    raise Exception.Create('You do not have permission to uncommmit the selected billing');
  BillingUncommitSP.Parameters.ParamValues['@BillID'] := BillID;
  BillingUncommitSP.ExecProc;
  CheckForAdoError(BillingUnCommitSP.Connection);
end;

procedure TServerBillingDM.BillingReexport(BillID: Integer; CallCenterGroupID: Integer);
begin
  SetName('BillingReExport');
  if (VerifyBillingRights(RightBillingReexport, CallCenterGroupID) = False)
    and (VerifyBillingRights(RightBillingCommit, CallCenterGroupID) = False) then
    raise Exception.Create('You do not have permission to re-export the selected billing');

  BillingReexportSP.Parameters.ParamValues['@BillID'] := BillID;
  BillingReexportSP.Parameters.ParamValues['@RequestBy'] := LogicDM.LoggedInEmpID;
  BillingReexportSP.ExecProc;
  if BillingReexportSP.Parameters.ParamValues['@RETURN_VALUE'] = 3 then
    raise Exception.Create('Selected re-export cannot be queued because it has no Center Group');
  if BillingReexportSP.Parameters.ParamValues['@RETURN_VALUE'] = 2 then
    raise Exception.Create('Multiple re-exports were queued');
  if BillingReexportSP.Parameters.ParamValues['@RETURN_VALUE'] = 1 then
    raise Exception.Create('Unable to re-export the selected billing');
  CheckForAdoError(BillingReexportSP.Connection);
end;

procedure TServerBillingDM.StartBilling(const Params: NVPairList);
const
  CheckQueueSQL = 'select * from billing_run where period_end_date=%s and span=%s ' +
    'and center_group_id in (%s) and completed_date is null';
var
  CallCenterGroupID: Integer;
  CenterGroupList: TStringList;
  Index: Integer;
  Span: string;
  PeriodEndDate: TDateTime;
  BillQ: TDataset;
begin
  CenterGroupList := TStringList.Create;
  try
    StrToStrings(NVPairString(Params, 'CallCenterGroupID', bvError), ',', CenterGroupList, False);
    PeriodEndDate := NVPairDateTime(Params, 'PeriodEndDate', bvError);
    Span := NVPairString(Params, 'Span', bvDefault, 'WEEK');

    Op.Phase := 'Check queue for pending request';
    CenterGroupList.Delimiter := ',';
    BillQ := LogicDM.CreateDataSetForSQL(Format(CheckQueueSQL, [QuotedStr(IsoDateToStr(PeriodEndDate)),
      QuotedStr(Span), CenterGroupList.DelimitedText]));
    try
      if HasRecords(BillQ) then
        raise Exception.Create('Cannot add centers to the queue. ' +
        'There are pending requests for the selected centers and period already queued.');
    finally
      FreeAndNil(BillQ);
    end;
    Op.Phase := 'Queue billing request';
    for Index := 0 to CenterGroupList.Count - 1 do begin
      CallCenterGroupID := StrToIntDef(CenterGroupList.Strings[Index], -1);
      if not VerifyBillingRights(RightBillingRun, CallCenterGroupID) then
        raise Exception.Create('You do not have permission to run the billing');
      InsertBillingRun.Open;
      InsertBillingRun.Append;
      InsertBillingRun.FieldByName('request_by').AsInteger := LogicDM.LoggedInEmpID;
      InsertBillingRun.FieldByName('request_date').AsDateTime := Now;
      InsertBillingRun.FieldByName('period_end_date').AsDateTime := PeriodEndDate;
      InsertBillingRun.FieldByName('center_group_id').AsInteger := CallCenterGroupID;
      InsertBillingRun.FieldByName('span').AsString := Span;
      InsertBillingRun.Post;
    end;
  finally
    FreeAndNil(CenterGroupList);
  end;
end;

function TServerBillingDM.GetCallCenterGroupIDForRunID(const RunID: Integer): Integer;
begin
  BillingCallCenterGroup.Parameters.ParamByName('run_id').Value := RunID;
  BillingCallCenterGroup.Open;
  try
    if not BillingCallCenterGroup.IsEmpty then
      Result := BillingCallCenterGroup.FieldByName('center_group_id').AsInteger
    else
      Result := 0;
  finally
    BillingCallCenterGroup.Close;
  end;
end;

function TServerBillingDM.GetBillingRunLog(const Params: NVPairList): string;
var
  RunID: Integer;
  CallCenterGroupID: Integer;
begin
  RunID := NVPairInteger(Params, 'run_id', bvError);
  CallCenterGroupID := GetCallCenterGroupIDForRunID(RunID);

  if not VerifyBillingRights(RightBillingRun, CallCenterGroupID) then
    raise Exception.Create('You do not have permission to view this billing run log');

  LogicDM.SearchQuery.CommandText := Format('select run_log from billing_run where run_id=%d', [RunID]);
  LogicDM.SearchQuery.Open;
  try
    Op.Phase := 'Encoding results';
    Result := LogicDM.SearchQuery.FieldValues['run_log'];
  finally
    LogicDM.SearchQuery.Close;
  end;
end;

procedure TServerBillingDM.CancelBillingRun(const Params: NVPairList);
const
  UpdateSQL = 'update billing_run set cancelled=1, completed_date=getdate() where run_id=%d';
var
  RunID: Integer;
  CallCenterGroupID: Integer;
begin
  RunID := NVPairInteger(Params, 'run_id', bvError);
  CallCenterGroupID := GetCallCenterGroupIDForRunID(RunID);

  if not VerifyBillingRights(RightBillingRun, CallCenterGroupID) then
    raise Exception.Create('You do not have permission to cancel the billing run');

  LogicDM.ExecuteSQL(Format(UpdateSQL, [RunID]));
end;

function TServerBillingDM.BillingRunSearch(const Params: NVPairList): string;
begin
  Op.Phase := 'Running query';
  BillingQueueData.Open;
  Op.Phase := 'Encoding results';
  Result := ConvertDataSetToXML([BillingQueueData], 3000);
end;

function TServerBillingDM.BillingAdjustmentSearch(const Params: NVPairList): string;
const
  Select =
    'select cl.client_name, cl.call_center, cu.customer_name, ba.* from billing_adjustment ba ' +
    '  left join customer cu on cu.customer_id = ba.customer_id ' +
    '  left join client cl on cl.client_id = ba.client_id ';
  CenterGroupWhere = '(select count(*) from client where customer_id = cu.customer_id and call_center in (%s)) > 0';
var
  SQL: TOdSqlBuilder;
  Group: OdMiscUtils.TStringArray;
  AdjustmentID: Variant;
  AdjustmentDateStart: TDateTime;
  AdjustmentDateEnd: TDateTime;
  CallCenterGroupID: Integer;
  CustomerID: Integer;
  ClientID: Integer;
  Amount: string;
  AdjustmentType: string;
  Description: string;
  AddedDateStart: TDateTime;
  AddedDateEnd: TDateTime;
  WhichInvoice: string;
  GLCode: string;
begin
  Group := nil;
  SQL := TOdSqlBuilder.Create;
  try
    AdjustmentID := NVPairInteger(Params, 'adjustment_id', bvZero);
    AdjustmentDateStart := NVPairDateTime(Params, 'adjustment_date_start', bvZero);
    AdjustmentDateEnd := NVPairDateTime(Params, 'adjustment_date_end', bvZero);
    AddedDateStart := NVPairDateTime(Params, 'added_date_start', bvZero);
    AddedDateEnd := NVPairDateTime(Params, 'added_date_end', bvZero);
    CustomerID := NVPairInteger(Params, 'customer_id', bvZero);
    ClientID := NVPairInteger(Params, 'client_id', bvZero);
    Amount := NVPairString(Params, 'amount', bvDefault, '');
    AdjustmentType := NVPairString(Params, 'type', bvDefault, '');
    Description := NVPairString(Params, 'description', bvDefault, '');
    CallCenterGroupID := NVPairInteger(Params, 'center_group_id', bvZero);
    WhichInvoice := NVPairString(Params, 'which_invoice', bvDefault);
    GLCode := NVPairString(Params, 'gl_code', bvDefault, '');

    SQL.AddWhereCondition('ba.active=1');
    SQL.AddDateTimeRangeFilter('ba.added_date', AddedDateStart, AddedDateEnd);
    SQL.AddDateTimeRangeFilter('ba.adjustment_date', AdjustmentDateStart, AdjustmentDateEnd);
    SQL.AddIntFilterUnlessZero('ba.customer_id', CustomerID);
    SQL.AddIntFilterUnlessZero('ba.client_id', ClientID);
    SQL.AddNumericFilter('ba.amount', Amount);
    SQL.AddExactStringFilter('ba.type', AdjustmentType);
    SQL.AddKeywordSearch('ba.description', Description);
    SQL.AddKeywordSearch('ba.which_invoice', WhichInvoice);
    SQL.AddKeywordSearch('ba.gl_code', GLCode);

    if CallCenterGroupID > 0 then begin
      Group := GetCenterGroup(CallCenterGroupID);
      if Length(Group) > 0 then
        SQL.AddWhereConditionFmt(CenterGroupWhere, [StringArrayToDelimitedString(Group, '''', ',')]);
    end;

    SQL.CheckFilterCount;
    LogicDM.SearchQuery.CommandText := Select + SQL.WhereClause;
    Op.Phase := 'Running query';
    Op.Detail := SQL.WhereClause;
    LogicDM.SearchQuery.Open;

    Op.Phase := 'Encoding results';
    Result := ConvertDataSetToXML([LogicDM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    LogicDM.SearchQuery.Close;
  end;
end;

function TServerBillingDM.GetCenterGroup(GroupID: Integer): TStringArray;
var
  i: Integer;
begin
  Assert(GroupID > 0);
  OpenDataSet(CenterGroup);
  CenterGroup.Filter := 'center_group_id = ' + IntToStr(GroupID);
  CenterGroup.Filtered := True;
  try
    SetLength(Result, CenterGroup.RecordCount);
    i := 0;
    CenterGroup.First;
    while not CenterGroup.Eof do begin
      Result[i] := CenterGroup.FieldByName('call_center').AsString;
      CenterGroup.Next;
      Inc(i);
    end;
  finally
    CenterGroup.Filtered := False;
  end;
end;

function TServerBillingDM.GetBillableCenterGroupIDList(BillingRights: array of string): TStringArray;
const
  Select = 'select distinct center_group_id from center_group where group_code in (%s)';
var
  i: Integer;
  Limitation: string;
  LimitationList: string;
  BillableCenters: string;
begin
  LimitationList := '';
  for i := 0 to Length(BillingRights)-1 do begin
    if LogicDM.CanI(BillingRights[i], Limitation, LogicDM.LoggedInEmpID) then
      if not (StringInArray(Trim(Limitation), ['', '-'])) then
        LimitationList := LimitationList + ',' + Limitation;
  end;
  if LimitationList = '' then
    Exit;

  BillableCenters := StringToDelimitedString(LimitationList, '''', ',');
  BillableCenterGroup.Close;
  BillableCenterGroup.CommandText := Format(Select, [BillableCenters]);
  OpenDataSet(BillableCenterGroup);
  SetLength(Result, BillableCenterGroup.RecordCount);
  i := 0;
  BillableCenterGroup.First;
  while not BillableCenterGroup.Eof do begin
    Result[i] := BillableCenterGroup.FieldByName('center_group_id').AsString;
    BillableCenterGroup.Next;
    Inc(i);
  end;
end;

function TServerBillingDM.BillingHeaderSearch(const Params: NVPairList): string;
const
  Select = 'select bh.*, e.short_name as committed_by_name from billing_header bh ' +
      'left join employee e on bh.committed_by_emp=e.emp_id ';
  CenterGroupWhere = 'bh.center_group_id in (%s)';
var
  SQL: TOdSqlBuilder;
  Group: OdMiscUtils.TStringArray;
  PeriodBeginDateFrom: TDateTime;
  PeriodBeginDateTo: TDateTime;
  PeriodEndDateFrom: TDateTime;
  PeriodEndDateTo: TDateTime;
  BillRunDateFrom: TDateTime;
  BillRunDateTo: TDateTime;
  CommitDateFrom: TDateTime;
  CommitDateTo: TDateTime;
  BillingCommitted: Integer;
  CallCenterGroupID: Integer;
  Description: string;
begin
  Group := nil;
  SQL := TOdSqlBuilder.Create;
  try
    PeriodBeginDateFrom := NVPairDateTime(Params, 'period_start_from', bvZero);
    PeriodBeginDateTo := NVPairDateTime(Params, 'period_start_to', bvZero);
    PeriodEndDateFrom := NVPairDateTime(Params, 'period_end_from', bvZero);
    PeriodEndDateTo := NVPairDateTime(Params, 'period_end_to', bvZero);
    BillRunDateFrom := NVPairDateTime(Params, 'run_date_from', bvZero);
    BillRunDateTo := NVPairDateTime(Params, 'run_date_to', bvZero);
    CommitDateFrom := NVPairDateTime(Params, 'commit_date_from', bvZero);
    CommitDateTo := NVPairDateTime(Params, 'commit_date_to', bvZero);
    Description := NVPairString(Params, 'description', bvDefault, '');
    BillingCommitted := NVPairInteger(Params, 'committed', bvNegativeOne);
    CallCenterGroupID := NVPairInteger(Params, 'center_group_id', bvZero);

    SQL.AddDateTimeRangeFilter('bh.bill_start_date', PeriodBeginDateFrom, PeriodBeginDateTo);
    SQL.AddDateTimeRangeFilter('bh.bill_end_date', PeriodEndDateFrom, PeriodEndDateTo);
    SQL.AddDateTimeRangeFilter('bh.bill_run_date', BillRunDateFrom, BillRunDateTo);
    SQL.AddDateTimeRangeFilter('bh.committed_date', CommitDateFrom, CommitDateTo);
    SQL.AddKeywordSearch('bh.description', Description);
    SQL.AddIntFilterIfGreaterThanZero('bh.center_group_id', CallCenterGroupID);
    if CallCenterGroupID <= 0 then begin
      Group := GetBillableCenterGroupIDList([RightBillingCommit, RightBillingDelete,
        RightBillingUncommit, RightBillingReexport]);
      if Length(Group) > 0 then
        SQL.AddWhereCondition(Format(CenterGroupWhere, [StringArrayToDelimitedString(Group, '''', ',')]));
    end;

    if BillingCommitted > -1 then
      SQL.AddBooleanSearch('bh.committed', BillingCommitted > 0);

    SQL.CheckFilterCount;

    LogicDM.SearchQuery.CommandText := Select + SQL.WhereClause;
    Op.Phase := 'Running query';
    Op.Detail := SQL.WhereClause;
    LogicDM.SearchQuery.Open;

    Op.Phase := 'Encoding results';
    Result := ConvertDataSetToXML([LogicDM.SearchQuery], 500);
  finally
    FreeAndNil(SQL);
    LogicDM.SearchQuery.Close;
  end;
end;

function TServerBillingDM.GetBillingAdjustment(const AdjustmentID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_billing_adjustment2',
    '@AdjustmentID', AdjustmentID);
end;

function TServerBillingDM.SaveBillingAdjustments(const BillingAdjustments: NVPairListList): GeneratedKeyList;
const
  Existing = 'select * from billing_adjustment where customer_id = :customer_id' +
    ' and adjustment_date = :adjustment_date and type = :type and amount = :amount' +
    ' and description = :description and active = :active';
  Updating = 'update billing_adjustment set customer_id=%d,' +
    ' adjustment_date=''%s'', type=''%s'', amount=%f,' +
    ' description=%s, last_modified_by=%d, active=%d, which_invoice=%s, gl_code=%s' +
    ' where adjustment_id=%d';
var
  Index: Integer;
  AdjustmentID: Integer;
  CustomerID: Integer;
  AdjustmentType: string;
  Description: string;
  Amount: Currency;
  Active: Boolean;
  AdjustmentDate: TDateTime;
  AddedDate: TDateTime;
  Adjustment: NVPairList;
  NewAdjustmentID: Integer;
  WhichInvoice: string;
  D: TADODataset;
  InsertMode: Boolean;
  GLCode: Variant;

  function QuotedNonNullStr(const InString: string): string;
  begin
    if SameText(InString, 'null') then
      Result := InString
    else
      Result := QuotedStr(InString);
  end;

  procedure UpdateAdjustment;
  begin
    Op.Phase := 'Editing existing adjustment';
    LogicDM.ExecuteSQL(Format(Updating, [CustomerID, DateToStr(AdjustmentDate), AdjustmentType,
      Amount, QuotedStr(Description), LogicDM.LoggedInEmpID, Ord(Active), QuotedNonNullStr(WhichInvoice),
      VarParamToStr(GLCode), AdjustmentID]));
  end;

begin
  NewAdjustmentID := -1;
  Result := GeneratedKeyList.Create;
  D := BillingAdjustment;
  for Index := 0 to BillingAdjustments.Count - 1 do begin
    Op.Phase := Format('Adjustment %d of %d', [Index + 1, BillingAdjustments.Count]);
    Adjustment := BillingAdjustments[Index];
    AdjustmentID := NVPairInteger(Adjustment, 'adjustment_id', bvError);
    InsertMode := (AdjustmentID < 0);
    AdjustmentDate := NVPairDateTime(Adjustment, 'adjustment_date', bvError);
    AdjustmentType := NVPairString(Adjustment, 'type', bvError);
    CustomerID := NVPairInteger(Adjustment, 'customer_id', bvError);
    Description := NVPairString(Adjustment, 'description', bvError);
    Amount := NVPairFloat(Adjustment, 'amount', bvError);
    AddedDate := NVPairDateTime(Adjustment, 'added_date', bvError);
    if VarIsNull(NVPairBoolean(Adjustment, 'active', bvNull)) then
      Active := True
    else
      Active := NVPairBoolean(Adjustment, 'active', bvError);
    WhichInvoice := NVPairString(Adjustment, 'which_invoice', bvDefault, 'null');
    GLCode := NVPairString(Adjustment, 'gl_code', bvNull);

    D.Close;
    D.ExecuteOptions := [];
    if InsertMode then begin
      D.CommandText := Existing;
      D.Parameters.ParamValues['customer_id'] := CustomerID;
      D.Parameters.ParamValues['adjustment_date'] := AdjustmentDate; // This is a truncated date (with no time)
      D.Parameters.ParamValues['type'] := AdjustmentType;
      D.Parameters.ParamValues['amount'] := Amount;
      D.Parameters.ParamValues['description'] := Description;
      D.Parameters.ParamValues['active'] := True;
      D.Open;
      if D.RecordCount = 1 then
        D.Edit
      else if D.RecordCount = 0 then begin
        D.Insert;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpID;
      end
      else
        raise Exception.Create('Multiple existing adjustments match new ajustment save data');

      Op.Phase := 'Editing adjustment';
      Assert(EditingDataSet(D));

      D.FieldByName('customer_id').Value := CustomerId;
      D.FieldByName('adjustment_date').Value := AdjustmentDate;
      D.FieldByName('type').value := AdjustmentType;
      D.FieldByName('description').Value := Description;
      D.FieldByName('amount').Value := Amount;
      D.FieldByName('added_date').Value := AddedDate;
      D.FieldByName('last_modified_by').Value := LogicDM.LoggedInEmpID;
      D.FieldByName('active').Value := Active;
      if WhichInvoice = 'null' then
        D.FieldByName('which_invoice').Clear
      else
        D.FieldByName('which_invoice').Value := WhichInvoice;
      if VarIsNull(GLCode) then
        D.FieldByName('gl_code').Clear
      else
        D.FieldByName('gl_code').AsString := GLCode;
      D.Post;
      NewAdjustmentID := D.FieldByName('adjustment_id').AsInteger;
    end
    else
      UpdateAdjustment;

    if AdjustmentID < 0 then begin
      Assert(NewAdjustmentID > 0, 'Negative new adjustment ID');
      AddIdentity(Result, 'billing_adjustment', AdjustmentID, NewAdjustmentID);
    end;
  end;
end;

end.
