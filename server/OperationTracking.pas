unit OperationTracking;

interface

uses
  SysUtils, Classes, SyncObjs, Windows, ThreadSafeLoggerU,
  OdSystemStatus, OdThreadTimer, Graphics;

const
{$I ..\version.inc}

type
  TOperationTracker = class;

  TOpInProgress = class(TObject)
    StartTime: TDateTime;
    EndTime: TDateTime;
    OpName: string;

    OpID: Integer;
    EmpID: Integer;
    Succeeded: Boolean;
    FTimer: TOdThreadTimer;
    procedure Log(const Msg: string);
    procedure LogFmt(const Msg: string; Params: array of const);
  private
    FPhase: string;
    FDetail: string;
    Tracker: TOperationTracker;
    procedure SetPhase(NewPhase: string);
    procedure SetDetail(NewDetail: string);
  public
    property Phase: string read FPhase write SetPhase;
    property Detail: string read FDetail write SetDetail;

    destructor Destroy; override;
  end;

  TProjectionMetricsTracker = class(TObject)
    public
    function GetHtml(ProjectorStatus: string; TotalEvents, ProjectedEvents, UnprojectedEvents: Integer): string;
  end;

  TOperationTracker = class(TObject)
  private
    FLastCPULog: TDateTime;
    procedure LogCpu(Sender: TObject);
  public
    InvokationsCompleted: Integer;
    InvokationsFailed: Integer;
    NumDataModules: Integer;
    FStatisticsCheckedCount: Integer;

    OperationsInProgress: TList;
    CS: TCriticalSection;
    NextOperId: Integer;
    SystemStatus: TSystemStatus;

    function GetHtml(StartupFailureMessage: string; StartupMagicNumber: Integer): string;
    function AddOperation: TOpInProgress;
    procedure FinishOperation(Op: TOpInProgress);
    procedure Log(Msg: String);
    function GetBitmapStream: TMemoryStream;

    constructor Create;
    destructor Destroy; override;
  end;

function GetOperationTracker: TOperationTracker;
function GetProjectionTracker: TProjectionMetricsTracker;

procedure CompressionStats(OriginalSize, CompressedSize,
    CompressionTime: Integer);

procedure DeCompressionStats(OriginalSize, CompressedSize,
    CompressionTime: Integer);

var
  RawLogger: TThreadSafeLogger;

implementation

uses
  DateUtils, Types;

var
  _ProjectionTracker: TProjectionMetricsTracker;
  _OperationTracker: TOperationTracker;
  ComputerName: string;

threadvar
  CurrentOperation: TOpInProgress;
  MostRecentOperationId: Integer;

function GetOperationTracker: TOperationTracker;
begin
  if not Assigned(_OperationTracker) then
    _OperationTracker := TOperationTracker.Create;
  Result := _OperationTracker;
end;

function GetProjectionTracker: TProjectionMetricsTracker;
begin
  if not Assigned(_ProjectionTracker) then
    _ProjectionTracker := TProjectionMetricsTracker.Create;
  Result := _ProjectionTracker;
end;

procedure AddRow(var Content: TStringList; s1, s2: string);
begin
  Content.Add('<tr><td valign="top">'+s1+'</td><td valign="top">'+s2+'</td></tr>');
end;

function TProjectionMetricsTracker.GetHTML(ProjectorStatus: string; TotalEvents, ProjectedEvents, UnprojectedEvents: Integer): string;
var
  Content: TStringList;

begin
  Content := TStringList.Create;
  try
    with Content do begin
      Add('<html><head>');
      Add('<STYLE TYPE=text/css>');
      Add('BODY{margin-top:1px;margin-left:1px;margin-right:2px;font-family:arial;font-size:8pt;};');
      Add('TABLE{font-family:arial;font-size:8pt;};');
      Add('</STYLE>');
      Add('<META HTTP-EQUIV=Refresh CONTENT="60"');
      Add('</head>');
      Add('<body><table>');
      AddRow(Content, '<h1>Projection Metrics</h1>','');
      AddRow(Content, 'Status', ProjectorStatus);
      AddRow(Content, 'Total Events' , IntToStr(TotalEvents));
      AddRow(Content, 'Projected Events' , IntToStr(ProjectedEvents));
      AddRow(Content, 'Unprojected Events' , IntToStr(UnprojectedEvents));
      Add('</table>');
      Add('<br><br>Statistics automatically updated every 60 seconds</body></html>');
    end;
  finally
   Result := Content.Text;
   FreeAndNil(Content);
  end;
end;

{ TOperationTracker }

const
  OperIdRange = 1000000;

function TOperationTracker.AddOperation: TOpInProgress;
begin
  CS.Enter;
  try
    Result := TOpInProgress.Create;
    Result.Tracker := Self;
    Result.StartTime := Now;
    Result.OpId := NextOperId;
    MostRecentOperationId := NextOperId;
    Result.OpName := '-';
    Result.Phase := '-';
    Result.FTimer := TOdThreadTimer.Create;
    Result.FTimer.Reset;
    NextOperId := (NextOperId + 1) mod OperIdRange;
    OperationsInProgress.Add(Result);
    CurrentOperation := Result;
  finally
    CS.Leave;
  end;
end;

constructor TOperationTracker.Create;
begin
  inherited;
  OperationsInProgress := TList.Create;
  CS := TCriticalSection.Create;
  NextOperId := Random(OperIdRange);
  SystemStatus := TSystemStatus.Create(500);
  SystemStatus.OnUpdate := LogCPU;
  SystemStatus.StartBackgroundThread;
end;

destructor TOperationTracker.Destroy;
begin
  FreeAndNil(SystemStatus);
  FreeAndNil(CS);
  FreeAndNil(OperationsInProgress);
  inherited;
end;

procedure TOperationTracker.FinishOperation(Op: TOpInProgress);
begin
  Assert(Assigned(Op));
  CurrentOperation := nil;
  CS.Enter;
  try
    Op.EndTime := Now;
    if Op.Succeeded then
      Inc(InvokationsCompleted)
    else
      Inc(InvokationsFailed);

    Assert(OperationsInProgress.IndexOf(Op) >= 0, 'Op must be in the list');
    OperationsInProgress.Remove(Op);
    FreeAndNil(Op);
  finally
    CS.Leave;
  end;
end;

function TOperationTracker.GetBitmapStream: TMemoryStream;
var
  ImageStream: TMemoryStream;
  B: Graphics.TBitmap;
begin
  ImageStream := TMemoryStream.Create;

  B := SystemStatus.RenderAsBitmap(34, 120);
  try
    B.SaveToStream(ImageStream);
    ImageStream.Position := 0;
    Result := ImageStream;
  finally
    FreeAndNil(B);
  end;
end;

function TOperationTracker.GetHtml(StartupFailureMessage: string; StartupMagicNumber: Integer): string;
var
  Content: TStringList;
  I: Integer;
  Op: TOpInProgress;
  RunTime: TDateTime;
  Elapsed: TDateTime;
  Seconds: Int64;
  StatisticsTime: TDateTime;
  CpuString: string;

begin
  Content := TStringList.Create;
  CS.Enter;
  try
    Inc(FStatisticsCheckedCount);
    with Content do begin
      Add('<html><head>');

      Add('<STYLE TYPE=text/css>');
      Add('BODY{margin-top:1px;margin-left:1px;margin-right:2px;font-family:arial;font-size:8pt;};');
      Add('TABLE{font-family:arial;font-size:8pt;};');
      Add('</STYLE>');
      Add('<META HTTP-EQUIV=Refresh CONTENT="5"');

      Add('</head>');
      Add('<body><table>');
    end;
    AddRow(Content, 'Server', '<b>' + ComputerName +
                     '</b> at ' + TimeToStr(Now) +
                     ' (' + FormatFloat('0', SystemStatus.CPUUsage) +
                     '%)  version ' + AppVersion);

    AddRow(Content, 'CPU', '<img src="cpu.bmp" />');

    // display mem free:
    //AddRow('Phys./Free', FormatFloat('#,##0.00', SystemStatus.PhysicalMemory / 1024) + ' / ' +
    //  FormatFloat('#,##0.00', SystemStatus.PhysicalFree / 1024));
    //AddRow('Proc./Delta', FormatFloat('#,##0.00', SystemStatus.ProcessMemory / 1024) + ' / ' +
    //  FormatFloat('#,##0.00', SystemStatus.ProcessDelta / 1024));

    AddRow(Content, 'Run/OK/Fail', Format('%d / %d / %d',
              [OperationsInProgress.Count, InvokationsCompleted, InvokationsFailed]));

    StatisticsTime := Now;

    for I := 0 to OperationsInProgress.Count-1 do begin
      Op := TOpInProgress(OperationsInProgress.Items[I]);
      RunTime := StatisticsTime - Op.StartTime;
      Op.FTimer.Measure;

      Elapsed := Op.FTimer.GetElapsedTime;
      Seconds := Round(Elapsed*60*60*24);
      CpuString := FormatDateTime('nn:ss', Elapsed);

      if Seconds < 3 then
        CpuString := Format('<font color="green">%s</font>', [CpuString])
      else if (Seconds >= 3) and (Seconds < 8) then
        CpuString := Format('<b><font color="#C1C100">%s</font></b>', [CpuString])
      else
        CpuString := Format('<b><font color="red">%s</font></b>', [CpuString]);

      AddRow(Content, Format('Oper %d', [I+1]), Format('<b>%s</b> - %s - CPU: %s - EmpID %d - %s - %s',
         [Op.OpName,
          FormatDateTime('nn:ss', RunTime),
          CpuString,
          Op.EmpID,
          Op.Phase,
          Op.Detail]));
    end;

    AddRow(Content, 'DMs', IntToStr(NumDataModules));
    if StartupFailureMessage <> '' then
      AddRow(Content, 'Startup', StartupFailureMessage);

    Content.Add('</table></body></html>');
  finally
    CS.Leave;
    Result := Content.Text;
    FreeAndNil(Content);
  end;
end;

procedure LoadComputerName;
var
  NameBuf: array[0..100] of char;
  Size: DWord;
begin
  Size := 100;
  if GetComputerName(NameBuf, Size) then
    ComputerName := StrPas(NameBuf);
end;

procedure TOperationTracker.Log(Msg: String);
var
  DateString: String;
begin
  if Assigned(RawLogger) then begin
    DateString := FormatDateTime('yyyy-mm-dd hh:mm:ss - - - ', Now);
    RawLogger.Log(DateString + Msg);
  end;
end;

procedure TOperationTracker.LogCpu(Sender: TObject);
begin
  if SecondsBetween(FLastCPULog, Now) > 30 then begin
    Log(Format('CPU Usage: %4.1f', [SystemStatus.CPUUsage]));
    FLastCPULog := Now;
  end;
end;

{ TOpInProgress }

destructor TOpInProgress.Destroy;
begin
  FreeAndNil(FTimer);
  inherited;
end;

procedure TOpInProgress.Log(const Msg: string);
var
  DateString: string;
  DurString: string;
  CpuString: string;
  LogString: string;
  CheckTime: TDateTime;
  Elapsed  : TDateTime;
begin
  Assert(Assigned(FTimer), 'No FTimer assigned. AddOperation must be called before Log.');
  FTimer.Measure;
  CheckTime := Now;
  DateString := FormatDateTime('yyyy-mm-dd hh:mm:ss', CheckTime);
  DurString := FormatDateTime('nn:ss.zzz', CheckTime - StartTime);
  Elapsed := FTimer.GetElapsedTime;
  CpuString := FormatDateTime('nn:ss.zzz', Elapsed);

  if Assigned(RawLogger) then begin
    LogString := Format('%s %06d %d %s %s %s "%s" "%s"', [DateString, OpID, EmpID, OpName, DurString, CpuString, Phase, Msg] );
    RawLogger.Log(LogString);
  end;
end;

procedure TOpInProgress.LogFmt(const Msg: string; Params: array of const);
begin
  Log(Format(Msg, Params));
end;

procedure TOpInProgress.SetDetail(NewDetail: string);
begin
  Tracker.CS.Enter;
  try
    FDetail := NewDetail;
  finally
    Tracker.CS.Leave;
  end;
end;

procedure TOpInProgress.SetPhase(NewPhase: string);
begin
  Tracker.CS.Enter;
  try
    FPhase := NewPhase;
  finally
    Tracker.CS.Leave;
  end;
end;

// ********** Not yet implemented: use the threadvar to track
// ********** compression overhead from RO

procedure CompressionStats(OriginalSize, CompressedSize,
    CompressionTime: Integer);
begin
  // MostRecentOperationId
end;

procedure DeCompressionStats(OriginalSize, CompressedSize,
    CompressionTime: Integer);
begin

end;

initialization
  LoadComputerName;

finalization
  FreeAndNil(_OperationTracker);
  FreeAndNil(_ProjectionTracker);

end.

