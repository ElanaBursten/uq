object LogicDM: TLogicDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 669
  Width = 647
  object Conn: TADOConnection
    CommandTimeout = 20
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=TestDB;Data Source=localhost' +
      ';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=409' +
      '6;Workstation ID=DEV-LAPTOP;Use Encryption for Data=False;Tag wi' +
      'th column collation when possible=False;'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 8
    Top = 16
  end
  object CheckLogin: TADOStoredProc
    Connection = Conn
    CursorType = ctStatic
    ProcedureName = 'login_user2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UserName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@PW'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 197
    Top = 16
  end
  object ChangePassword: TADOStoredProc
    Connection = Conn
    ProcedureName = 'change_password;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UserID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewPassword'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end>
    Left = 192
    Top = 128
  end
  object GetScopeIdentityCommand: TADOCommand
    CommandText = 'SELECT SCOPE_IDENTITY() AS X'
    Connection = Conn
    Parameters = <>
    Left = 96
    Top = 240
  end
  object GetAnyIdentityCommand: TADOCommand
    CommandText = 'SELECT @@IDENTITY AS X'
    Connection = Conn
    Parameters = <>
    Left = 96
    Top = 288
  end
  object EmpHier: TADODataSet
    Connection = Conn
    Parameters = <>
    Left = 72
    Top = 464
  end
  object CheckRight: TADODataSet
    Connection = Conn
    Parameters = <>
    Left = 192
    Top = 72
  end
  object LogReportResult: TADOCommand
    CommandText = 
      'update report_log set'#13#10' client_success = :client_success,'#13#10' clie' +
      'nt_elapsed = :client_elapsed,'#13#10' client_details = :client_details' +
      #13#10'where'#13#10' request_id = :request_id'#13#10
    Connection = Conn
    Parameters = <
      item
        Name = 'client_success'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'client_elapsed'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_details'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'request_id'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    Left = 312
    Top = 184
  end
  object RecordComputerInfo: TADOStoredProc
    Connection = Conn
    ProcedureName = 'record_computer_info;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmployeeID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WindowsUser'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@OsPlatform'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OsMajorVersion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OsMinorVersion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OsServicePackVersion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ComputerName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@DellServiceTag'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@HotFixList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3000
        Value = Null
      end
      item
        Name = '@ComputerSerial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@DomainLogin'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@ComputerModel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AssetTag'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@AirCardPhoneNo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@AirCardESN'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 432
    Top = 128
  end
  object InsertIntoSyncLog: TADOCommand
    CommandText = 
      'insert into sync_log (   emp_id,   sync_date,   client_version, ' +
      '  local_date,   local_utc_bias,   updates_since,   bytes_down,  ' +
      ' bytes_up,   total_time,   addin_version,   persequor_version,  ' +
      ' MEME_version,   sync_source,   tickets_in_client_cache) values ' +
      '(   :EmployeeID,   getdate(),   :ClientVersion,   :LocalDate,   ' +
      ':UTCBias,   :UpdatesSince,   :bytes_down,   :bytes_up,   :total_' +
      'time,   :addin_version,   :persequor_version,   :MEME_version,  ' +
      ' :sync_source,   :NumTicketsInClientCache)'
    Connection = Conn
    Parameters = <
      item
        Name = 'EmployeeID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ClientVersion'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'LocalDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'UTCBias'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'UpdatesSince'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'bytes_down'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'bytes_up'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'total_time'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'addin_version'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'persequor_version'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'MEME_version'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'sync_source'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'NumTicketsInClientCache'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 432
    Top = 72
  end
  object SyncSP: TADOStoredProc
    CacheSize = 100
    Connection = Conn
    CursorType = ctOpenForwardOnly
    CommandTimeout = 180
    ProcedureName = 'sync_6;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmployeeID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UpdatesSinceS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ClientTicketListS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ClientDamageListS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@ClientWorkOrderListS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 432
    Top = 16
  end
  object FindUserNotifEmail: TADOStoredProc
    CacheSize = 100
    Connection = Conn
    CursorType = ctOpenForwardOnly
    CommandTimeout = 180
    ProcedureName = 'find_user_notification_email;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmpId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmailType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 194
    Top = 188
  end
  object StartReportCommand: TADOCommand
    CommandText = 
      'insert into report_log'#13#10'(request_id, report_name, user_id,'#13#10' par' +
      'ams, status, priority, request_date)'#13#10'values'#13#10'(:request_id, :rep' +
      'ort_name,'#13#10' :user_id, :params, '#39'queued'#39', :priority, getdate())'
    Connection = Conn
    Parameters = <
      item
        Name = 'request_id'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'report_name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'user_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'params'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 500
        Value = Null
      end
      item
        Name = 'priority'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 312
    Top = 128
  end
  object CheckReportDataset: TADODataSet
    Connection = Conn
    CommandText = 'select * from report_log'#13#10'where request_id=:request_id'
    Parameters = <
      item
        Name = 'request_id'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    Left = 312
    Top = 232
  end
  object Messages: TADODataSet
    Connection = Conn
    CommandText = 'select * from message where 0=1'
    ParamCheck = False
    Parameters = <>
    Left = 469
    Top = 311
  end
  object MessageDest: TADODataSet
    Connection = Conn
    CommandText = 'select * from message_dest where 0=1'
    Parameters = <>
    Left = 469
    Top = 359
  end
  object EmployeeActivity: TADODataSet
    Connection = Conn
    CommandText = 
      '/* SQL replaced at runtime */'#13#10'select * from employee_activity'#13#10 +
      'where 0=1'
    Parameters = <>
    Left = 552
    Top = 30
  end
  object Area: TADODataSet
    Connection = Conn
    Parameters = <>
    Left = 186
    Top = 374
  end
  object UploadQueue: TADODataSet
    Connection = Conn
    CommandText = 'select * from employee_upload_queue where emp_id = :emp_id'
    Parameters = <
      item
        Name = 'emp_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 373
    Top = 309
  end
  object CompletionReportSP: TADOStoredProc
    CacheSize = 100
    Connection = Conn
    CursorType = ctOpenForwardOnly
    CommandTimeout = 500
    ProcedureName = 'completion_report_data_4;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@datefrom'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@dateto'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@office'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@manager'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@sortby'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 16
  end
  object InsertTicketActivityAck: TADOStoredProc
    Connection = Conn
    ProcedureName = 'insert_ticket_activity_ack2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ActivityType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ActivityDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ActivityEmpID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 434
    Top = 233
  end
  object ResetAPIKey: TADOStoredProc
    Connection = Conn
    CursorType = ctStatic
    ProcedureName = 'reset_api_key;1'
    Parameters = <
      item
        Name = '@UserID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@NewKey'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 213
    Top = 264
  end
  object Employee: TADODataSet
    Connection = Conn
    CommandText = 'select * from employee where 0 = 1'
    Parameters = <>
    Left = 72
    Top = 376
  end
  object updUserLastLogin: TADOCommand
    CommandText = 
      'update users '#13#10'set last_login = :lastLogin '#13#10'where emp_id = :emp' +
      'ID '
    Connection = Conn
    Parameters = <
      item
        Name = 'lastLogin'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'empID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 272
    Top = 331
  end
  object CheckADLogin: TADOStoredProc
    Connection = Conn
    CursorType = ctStatic
    ProcedureName = 'getADName;1'
    Parameters = <
      item
        Name = '@UserName'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 253
    Top = 32
  end
  object qryTextMailAddress: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'LoginEmpID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'EmpID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'Select [dbo].[extractNumbers](contact_phone)+R.description as Te' +
        'xtMailAddress, E.short_name, [email_address] as FromAddress'
      'from employee E'
      'join users U on U.emp_id = :LoginEmpID'
      'join reference R on r.ref_id = e.phoneCo_ref'
      'where E.emp_id = :EmpID'
      'and r.type = '#39'phoneco'#39)
    Left = 472
    Top = 416
  end
  object EmployeeActivityToday: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'ActivityType'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select * from employee_activity '
      ' where activity_type =:ActivityType'
      ' and emp_id=:emp_id '
      
        ' and (activity_date >= (convert(datetime, convert(varchar, GetDa' +
        'te(), 101)))) ')
    Left = 560
    Top = 96
  end
  object GetFirstTaskTime: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'empID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select first_task_reminder '
      'from employee'
      'where emp_id = :empID')
    Left = 192
    Top = 472
  end
  object InsertcustomAnswer: TADOCommand
    CommandText = 
      'insert into custom_form_answer'#13#10'(form_field_id, answer, answered' +
      '_by, answer_date, foreign_id)'#13#10'values'#13#10'(:form_field_id, :answer,' +
      ' :answered_by, :answer_date, :foreign_id)'
    Connection = Conn
    Parameters = <
      item
        Name = 'form_field_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'answer'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'answered_by'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'answer_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'foreign_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 520
    Top = 505
  end
  object UpdateCustomAnswer: TADOCommand
    CommandText = 
      'update  custom_form_answer'#13#10'  set answer=:answer,'#13#10'        answe' +
      'red_by=:answered_by,'#13#10'        answer_date=:answer_date'#13#10'where an' +
      'swer_id=:answer_id'
    Connection = Conn
    Parameters = <
      item
        Name = 'answer'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'answered_by'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'answer_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'answer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 475
    Top = 560
  end
  object CustomFormAnswer: TADOQuery
    Connection = Conn
    Parameters = <>
    SQL.Strings = (
      'select * from custom_form_answer where answer_date=GetDate()+1')
    Left = 412
    Top = 497
  end
  object TrivialOperation: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select * from holiday where 1=0'
    CommandTimeout = 5
    EnableBCD = False
    Parameters = <>
    Left = 96
    Top = 16
  end
  object AttachmentData: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select * from attachment'
    EnableBCD = False
    Parameters = <>
    Left = 373
    Top = 357
  end
  object SearchQuery: TADODataSet
    CacheSize = 50
    Connection = Conn
    CursorType = ctOpenForwardOnly
    CommandTimeout = 200
    EnableBCD = False
    ParamCheck = False
    Parameters = <>
    Left = 96
    Top = 80
  end
  object GenericQuery: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <>
    Left = 98
    Top = 135
  end
  object GetTableData: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <>
    Left = 100
    Top = 188
  end
  object ErrorReport: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select * from error_report where 1=0'
    EnableBCD = False
    Parameters = <>
    Left = 320
    Top = 80
  end
end
