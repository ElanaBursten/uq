unit QMEventsNoThrift;
{
  This unit contains interim code so QMLogic (D2007) can save events.
  It will eventually be replaced by code generated from service2\QMLogic2ServiceLib.thrift.
}

interface

uses
  Classes, SysUtils, Contnrs;

type
  TAggregateType = (
    atTicket = 1,
    atDamage = 2,
    atWorkOrder = 3
  );
  TDateTimeString = string;

  IAggrKey = interface(IInterface)
    function GetAggrType: TAggregateType;
    procedure SetAggrType( const Value: TAggregateType);
    function GetAggrID: Integer;
    procedure SetAggrID( const Value: Integer);

    function ToString: string;
    function SerializeToJSON: string;

    property AggrType: TAggregateType read GetAggrType write SetAggrType;
    property AggrID: Integer read GetAggrID write SetAggrID;
  end;

  TAggrKeyImpl = class(TInterfacedObject, IAggrKey)
  private
    FAggrType: TAggregateType;
    FAggrID: Integer;
    function GetAggrType: TAggregateType;
    procedure SetAggrType( const Value: TAggregateType);
    function GetAggrID: Integer;
    procedure SetAggrID( const Value: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: string;
    function SerializeToJSON: string;

    // Properties
    property AggrType: TAggregateType read GetAggrType write SetAggrType;
    property AggrID: Integer read GetAggrID write SetAggrID;
  end;

  ILocateAssigned = interface(IInterface)
    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetFromLocatorID: Integer;
    procedure SetFromLocatorID( const Value: Integer);
    function GetToLocatorID: Integer;
    procedure SetToLocatorID( const Value: Integer);
    function GetChangedByID: Integer;
    procedure SetChangedByID( const Value: Integer);
    function GetChangedDate: TDateTimeString;
    procedure SetChangedDate( const Value: TDateTimeString);

    property LocateID: Integer read GetLocateID write SetLocateID;
    property FromLocatorID: Integer read GetFromLocatorID write SetFromLocatorID;
    property ToLocatorID: Integer read GetToLocatorID write SetToLocatorID;
    property ChangedByID: Integer read GetChangedByID write SetChangedByID;
    property ChangedDate: TDateTimeString read GetChangedDate write SetChangedDate;
  end;

  TLocateAssignedImpl = class(TInterfacedObject, ILocateAssigned)
  private
    FLocateID: Integer;
    FFromLocatorID: Integer;
    FToLocatorID: Integer;
    FChangedByID: Integer;
    FChangedDate: TDateTimeString;

    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetFromLocatorID: Integer;
    procedure SetFromLocatorID( const Value: Integer);
    function GetToLocatorID: Integer;
    procedure SetToLocatorID( const Value: Integer);
    function GetChangedByID: Integer;
    procedure SetChangedByID( const Value: Integer);
    function GetChangedDate: TDateTimeString;
    procedure SetChangedDate( const Value: TDateTimeString);
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;

    // Properties
    property LocateID: Integer read GetLocateID write SetLocateID;
    property FromLocatorID: Integer read GetFromLocatorID write SetFromLocatorID;
    property ToLocatorID: Integer read GetToLocatorID write SetToLocatorID;
    property ChangedByID: Integer read GetChangedByID write SetChangedByID;
    property ChangedDate: TDateTimeString read GetChangedDate write SetChangedDate;
  end;

  ITicketScheduled = interface(IInterface)
    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);
    function GetChangedByID: Integer;
    procedure SetChangedByID( const Value: Integer);
    function GetChangedDate: TDateTimeString;
    procedure SetChangedDate( const Value: TDateTimeString);

    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
    property ChangedByID: Integer read GetChangedByID write SetChangedByID;
    property ChangedDate: TDateTimeString read GetChangedDate write SetChangedDate;
  end;

  TTicketScheduledImpl = class(TInterfacedObject, ITicketScheduled)
  private
    FWorkloadDate: TDateTimeString;
    FChangedByID: Integer;
    FChangedDate: TDateTimeString;

    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);
    function GetChangedByID: Integer;
    procedure SetChangedByID( const Value: Integer);
    function GetChangedDate: TDateTimeString;
    procedure SetChangedDate( const Value: TDateTimeString);
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;

    // Properties
    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
    property ChangedByID: Integer read GetChangedByID write SetChangedByID;
    property ChangedDate: TDateTimeString read GetChangedDate write SetChangedDate;
  end;

  ILocateClosed = interface(IInterface)
    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetClosedDate: TDateTimeString;
    procedure SetClosedDate( const Value: TDateTimeString);
    function GetClosedByEmpID: Integer;
    procedure SetClosedByEmpID( const Value: Integer);
    function GetClientCode: string;
    procedure SetClientCode( const Value: string);
    function GetCallCenter: string;
    procedure SetCallCenter( const Value: string);
    function GetStatusCode: string;
    procedure SetStatusCode( const Value: string);
    function GetCompletedTicket: Boolean;
    procedure SetCompletedTicket( const Value: Boolean);
    function GetTicketType: string;
    procedure SetTicketType( const Value: string);
    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);

    function ToString: string;
    function SerializeToJSON: string;

    property LocateID: Integer read GetLocateID write SetLocateID;
    property ClosedDate: TDateTimeString read GetClosedDate write SetClosedDate;
    property ClosedByEmpID: Integer read GetClosedByEmpID write SetClosedByEmpID;
    property ClientCode: string read GetClientCode write SetClientCode;
    property CallCenter: string read GetCallCenter write SetCallCenter;
    property StatusCode: string read GetStatusCode write SetStatusCode;
    property CompletedTicket: Boolean read GetCompletedTicket write SetCompletedTicket;
    property TicketType: string read GetTicketType write SetTicketType;
    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
  end;

  TLocateClosedImpl = class(TInterfacedObject, ILocateClosed)
  private
    FLocateID: Integer;
    FClosedDate: TDateTimeString;
    FClosedByEmpID: Integer;
    FClientCode: string;
    FCallCenter: string;
    FStatusCode: string;
    FCompletedTicket: Boolean;
    FTicketType: string;
    FWorkloadDate: TDateTimeString;

    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetClosedDate: TDateTimeString;
    procedure SetClosedDate( const Value: TDateTimeString);
    function GetClosedByEmpID: Integer;
    procedure SetClosedByEmpID( const Value: Integer);
    function GetClientCode: string;
    procedure SetClientCode( const Value: string);
    function GetCallCenter: string;
    procedure SetCallCenter( const Value: string);
    function GetStatusCode: string;
    procedure SetStatusCode( const Value: string);
    function GetCompletedTicket: Boolean;
    procedure SetCompletedTicket( const Value: Boolean);
    function GetTicketType: string;
    procedure SetTicketType( const Value: string);
    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;
    function SerializeToJSON: string;

    // Properties
    property LocateID: Integer read GetLocateID write SetLocateID;
    property ClosedDate: TDateTimeString read GetClosedDate write SetClosedDate;
    property ClosedByEmpID: Integer read GetClosedByEmpID write SetClosedByEmpID;
    property ClientCode: string read GetClientCode write SetClientCode;
    property CallCenter: string read GetCallCenter write SetCallCenter;
    property StatusCode: string read GetStatusCode write SetStatusCode;
    property CompletedTicket: Boolean read GetCompletedTicket write SetCompletedTicket;
    property TicketType: string read GetTicketType write SetTicketType;
    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
  end;

  ILocateReopened = interface(IInterface)
    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetReopenedDate: TDateTimeString;
    procedure SetReopenedDate( const Value: TDateTimeString);
    function GetReopenedByEmpID: Integer;
    procedure SetReopenedByEmpID( const Value: Integer);
    function GetClientCode: string;
    procedure SetClientCode( const Value: string);
    function GetCallCenter: string;
    procedure SetCallCenter( const Value: string);
    function GetStatusCode: string;
    procedure SetStatusCode( const Value: string);
    function GetReopenedTicket: Boolean;
    procedure SetReopenedTicket( const Value: Boolean);
    function GetTicketType: string;
    procedure SetTicketType( const Value: string);
    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);

    function ToString: string;
    function SerializeToJSON: string;

    property LocateID: Integer read GetLocateID write SetLocateID;
    property ReopenedDate: TDateTimeString read GetReopenedDate write SetReopenedDate;
    property ReopenedByEmpID: Integer read GetReopenedByEmpID write SetReopenedByEmpID;
    property ClientCode: string read GetClientCode write SetClientCode;
    property CallCenter: string read GetCallCenter write SetCallCenter;
    property StatusCode: string read GetStatusCode write SetStatusCode;
    property ReopenedTicket: Boolean read GetReopenedTicket write SetReopenedTicket;
    property TicketType: string read GetTicketType write SetTicketType;
    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
  end;

  TLocateReopenedImpl = class(TInterfacedObject, ILocateReopened)
  private
    FLocateID: Integer;
    FReopenedDate: TDateTimeString;
    FReopenedByEmpID: Integer;
    FClientCode: string;
    FCallCenter: string;
    FStatusCode: string;
    FReopenedTicket: Boolean;
    FTicketType: string;
    FWorkloadDate: TDateTimeString;

    function GetLocateID: Integer;
    procedure SetLocateID( const Value: Integer);
    function GetReopenedDate: TDateTimeString;
    procedure SetReopenedDate( const Value: TDateTimeString);
    function GetReopenedByEmpID: Integer;
    procedure SetReopenedByEmpID( const Value: Integer);
    function GetClientCode: string;
    procedure SetClientCode( const Value: string);
    function GetCallCenter: string;
    procedure SetCallCenter( const Value: string);
    function GetStatusCode: string;
    procedure SetStatusCode( const Value: string);
    function GetReopenedTicket: Boolean;
    procedure SetReopenedTicket( const Value: Boolean);
    function GetTicketType: string;
    procedure SetTicketType( const Value: string);
    function GetWorkloadDate: TDateTimeString;
    procedure SetWorkloadDate( const Value: TDateTimeString);
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;
    function SerializeToJSON: string;

    // Properties
    property LocateID: Integer read GetLocateID write SetLocateID;
    property ReopenedDate: TDateTimeString read GetReopenedDate write SetReopenedDate;
    property ReopenedByEmpID: Integer read GetReopenedByEmpID write SetReopenedByEmpID;
    property ClientCode: string read GetClientCode write SetClientCode;
    property CallCenter: string read GetCallCenter write SetCallCenter;
    property StatusCode: string read GetStatusCode write SetStatusCode;
    property ReopenedTicket: Boolean read GetReopenedTicket write SetReopenedTicket;
    property TicketType: string read GetTicketType write SetTicketType;
    property WorkloadDate: TDateTimeString read GetWorkloadDate write SetWorkloadDate;
  end;

  IEventUnion = interface(IInterface)
    function GetLocateAssigned: ILocateAssigned;
    procedure SetLocateAssigned( const Value: ILocateAssigned);
    function GetTicketScheduled: ITicketScheduled;
    procedure SetTicketScheduled( const Value: ITicketScheduled);
    function GetLocateClosed: ILocateClosed;
    procedure SetLocateClosed( const Value: ILocateClosed);
    function GetLocateReopened: ILocateReopened;
    procedure SetLocateReopened( const Value: ILocateReopened);

    function SerializeToJSON: string;
    function ToString: string;

    property LocateAssigned: ILocateAssigned read GetLocateAssigned write SetLocateAssigned;
    property TicketScheduled: ITicketScheduled read GetTicketScheduled write SetTicketScheduled;
    property LocateClosed: ILocateClosed read GetLocateClosed write SetLocateClosed;
    property LocateReopened: ILocateReopened read GetLocateReopened write SetLocateReopened;
  end;

  TEventUnionImpl = class(TInterfacedObject, IEventUnion)
  private
    FLocateAssigned: ILocateAssigned;
    FTicketScheduled: ITicketScheduled;
    FLocateClosed: ILocateClosed;
    FLocateReopened: ILocateReopened;
    function GetLocateAssigned: ILocateAssigned;
    procedure SetLocateAssigned( const Value: ILocateAssigned);
    function GetTicketScheduled: ITicketScheduled;
    procedure SetTicketScheduled( const Value: ITicketScheduled);
    function GetLocateClosed: ILocateClosed;
    procedure SetLocateClosed( const Value: ILocateClosed);
    function GetLocateReopened: ILocateReopened;
    procedure SetLocateReopened( const Value: ILocateReopened);
    // Clear values(for union's property setter)
    procedure ClearUnionValues;
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;
    function SerializeToJSON: string;

    // Properties
    property LocateAssigned: ILocateAssigned read GetLocateAssigned write SetLocateAssigned;
    property TicketScheduled: ITicketScheduled read GetTicketScheduled write SetTicketScheduled;
    property LocateClosed: ILocateClosed read GetLocateClosed write SetLocateClosed;
    property LocateReopened: ILocateReopened read GetLocateReopened write SetLocateReopened;
  end;

  IEvent = interface(IInterface)
    function GetEventID: string;
    procedure SetEventID( const Value: string);
    function GetAggregateKey: IAggrKey;
    procedure SetAggregateKey( const Value: IAggrKey);
    function GetUnion: IEventUnion;
    procedure SetUnion( const Value: IEventUnion);

    function SerializeToJSON: string;

    property EventID: string read GetEventID write SetEventID;
    property AggregateKey: IAggrKey read GetAggregateKey write SetAggregateKey;
    property Union: IEventUnion read GetUnion write SetUnion;
  end;

  TEventImpl = class(TInterfacedObject, IEvent)
  private
    FEventID: string;
    FAggregateKey: IAggrKey;
    FUnion: IEventUnion;

    function GetEventID: string;
    procedure SetEventID( const Value: string);
    function GetAggregateKey: IAggrKey;
    procedure SetAggregateKey( const Value: IAggrKey);
    function GetUnion: IEventUnion;
    procedure SetUnion( const Value: IEventUnion);
  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;
    function SerializeToJSON: string;

    // Properties
    property EventID: string read GetEventID write SetEventID;
    property AggregateKey: IAggrKey read GetAggregateKey write SetAggregateKey;
    property Union: IEventUnion read GetUnion write SetUnion;
  end;

  IEventList = interface(IInterface)
    function GetItems: TObjectList;
    procedure SetItems( const Value: TObjectList);

    property Items: TObjectList read GetItems write SetItems;
  end;

  TEventListImpl = class(TInterfacedObject, IEventList)
  private
    FItems: TObjectList;

    function GetItems: TObjectList;
    procedure SetItems( const Value: TObjectList);

  public
    constructor Create;
    destructor Destroy; override;

    function ToString: string;

    // Properties
    property Items: TObjectList read GetItems write SetItems;
  end;

implementation

uses
  StrUtils;

function BoolAs01(Value: Boolean): string;
begin
  Result := IfThen(Value, '1', '0');
end;

constructor TAggrKeyImpl.Create;
begin
  inherited;
end;

destructor TAggrKeyImpl.Destroy;
begin
  inherited;
end;

function TAggrKeyImpl.GetAggrType: TAggregateType;
begin
  Result := FAggrType;
end;

procedure TAggrKeyImpl.SetAggrType( const Value: TAggregateType);
begin
  FAggrType := Value;
end;

function TAggrKeyImpl.GetAggrID: Integer;
begin
  Result := FAggrID;
end;

procedure TAggrKeyImpl.SetAggrID( const Value: Integer);
begin
  FAggrID := Value;
end;

function TAggrKeyImpl.SerializeToJSON: string;
const
  AggrKeyJSON = '{"2":{"rec":{"1":{"i32":%d},"2":{"i32":%d}}}';
begin
  Result := Format(AggrKeyJSON, [Integer(AggrType), AggrID]);
end;

function TAggrKeyImpl.ToString: string;
begin
  Result := '(AggrType: ';
  Result := Result + IntToStr(Integer(AggrType));
  Result := Result + ',AggrID: ';
  Result := Result + IntToStr(AggrID);
  Result := Result + ')';
end;

constructor TLocateAssignedImpl.Create;
begin
  inherited;
end;

destructor TLocateAssignedImpl.Destroy;
begin
  inherited;
end;

function TLocateAssignedImpl.GetLocateID: Integer;
begin
  Result := FLocateID;
end;

procedure TLocateAssignedImpl.SetLocateID( const Value: Integer);
begin
  FLocateID := Value;
end;

function TLocateAssignedImpl.GetFromLocatorID: Integer;
begin
  Result := FFromLocatorID;
end;

procedure TLocateAssignedImpl.SetFromLocatorID( const Value: Integer);
begin
  FFromLocatorID := Value;
end;

function TLocateAssignedImpl.GetToLocatorID: Integer;
begin
  Result := FToLocatorID;
end;

procedure TLocateAssignedImpl.SetToLocatorID( const Value: Integer);
begin
  FToLocatorID := Value;
end;

function TLocateAssignedImpl.GetChangedByID: Integer;
begin
  Result := FChangedByID;
end;

procedure TLocateAssignedImpl.SetChangedByID( const Value: Integer);
begin
  FChangedByID := Value;
end;

function TLocateAssignedImpl.GetChangedDate: TDateTimeString;
begin
  Result := FChangedDate;
end;

procedure TLocateAssignedImpl.SetChangedDate( const Value: TDateTimeString);
begin
  FChangedDate := Value;
end;

function TLocateAssignedImpl.ToString: string;
begin
  Result := Result + ',LocateID: ';
  Result := Result + IntToStr(LocateID);
  Result := Result + ',FromLocatorID: ';
  Result := Result + IntToStr(FromLocatorID);
  Result := Result + ',ToLocatorID: ';
  Result := Result + IntToStr(ToLocatorID);
  Result := Result + ',ChangedByID: ';
  Result := Result + IntToStr(ChangedByID);
  Result := Result + ',ChangedDate: ';
  Result := Result + ChangedDate;
  Result := Result + ')';
end;

constructor TTicketScheduledImpl.Create;
begin
  inherited;
end;

destructor TTicketScheduledImpl.Destroy;
begin
  inherited;
end;

function TTicketScheduledImpl.GetWorkloadDate: TDateTimeString;
begin
  Result := FWorkloadDate;
end;

procedure TTicketScheduledImpl.SetWorkloadDate( const Value: TDateTimeString);
begin
  FWorkloadDate := Value;
end;

function TTicketScheduledImpl.GetChangedByID: Integer;
begin
  Result := FChangedByID;
end;

procedure TTicketScheduledImpl.SetChangedByID( const Value: Integer);
begin
  FChangedByID := Value;
end;

function TTicketScheduledImpl.GetChangedDate: TDateTimeString;
begin
  Result := FChangedDate;
end;

procedure TTicketScheduledImpl.SetChangedDate( const Value: TDateTimeString);
begin
  FChangedDate := Value;
end;

function TTicketScheduledImpl.ToString: string;
begin
  Result := 'not implemented';
end;

constructor TLocateClosedImpl.Create;
begin
  inherited;
end;

destructor TLocateClosedImpl.Destroy;
begin
  inherited;
end;

function TLocateClosedImpl.GetLocateID: Integer;
begin
  Result := FLocateID;
end;

procedure TLocateClosedImpl.SetLocateID( const Value: Integer);
begin
  FLocateID := Value;
end;

function TLocateClosedImpl.GetClosedDate: TDateTimeString;
begin
  Result := FClosedDate;
end;

procedure TLocateClosedImpl.SetClosedDate( const Value: TDateTimeString);
begin
  FClosedDate := Value;
end;

function TLocateClosedImpl.GetClosedByEmpID: Integer;
begin
  Result := FClosedByEmpID;
end;

procedure TLocateClosedImpl.SetClosedByEmpID( const Value: Integer);
begin
  FClosedByEmpID := Value;
end;

function TLocateClosedImpl.GetClientCode: string;
begin
  Result := FClientCode;
end;

procedure TLocateClosedImpl.SetClientCode( const Value: string);
begin
  FClientCode := Value;
end;

function TLocateClosedImpl.GetCallCenter: string;
begin
  Result := FCallCenter;
end;

procedure TLocateClosedImpl.SetCallCenter( const Value: string);
begin
  FCallCenter := Value;
end;

function TLocateClosedImpl.GetStatusCode: string;
begin
  Result := FStatusCode;
end;

procedure TLocateClosedImpl.SetStatusCode( const Value: string);
begin
  FStatusCode := Value;
end;

function TLocateClosedImpl.GetCompletedTicket: Boolean;
begin
  Result := FCompletedTicket;
end;

procedure TLocateClosedImpl.SetCompletedTicket(const Value: Boolean);
begin
  FCompletedTicket := Value;
end;

function TLocateClosedImpl.GetTicketType: string;
begin
  Result := FTicketType;
end;

procedure TLocateClosedImpl.SetTicketType(const Value: string);
begin
  FTicketType := Value;
end;

function TLocateClosedImpl.GetWorkloadDate: string;
begin
  Result := FWorkloadDate;
end;

procedure TLocateClosedImpl.SetWorkloadDate(const Value: string);
begin
  FWorkloadDate := Value;
end;

function TLocateClosedImpl.SerializeToJSON: string;
const
  LocateClosedJSON = '"3":{"rec":{"3":{"rec":{' +
    '"1":{"i32":%d},' +   // locate_id
    '"2":{"str":"%s"},' + // closed_date
    '"3":{"i32":%d},' +   // closed_by_emp_id
    '"4":{"str":"%s"},' + // client_code
    '"5":{"str":"%s"},' + // call_center
    '"6":{"str":"%s"},' + // status_code
    '"7":{"tf":%s},' +    // completes ticket
    '"8":{"str":"%s"},' + // ticket type
    '"9":{"str":"%s"}' +  // workload date
    '}}}}}';
begin
  Result := Format(LocateClosedJSON, [LocateID, ClosedDate, ClosedByEmpID,
    ClientCode, CallCenter, StatusCode, BoolAs01(CompletedTicket), TicketType,
    WorkloadDate]);
end;

function TLocateClosedImpl.ToString: string;
begin
  Result := Result + 'LocateID: ';
  Result := Result + IntToStr(LocateID);
  Result := Result + ',ClosedDate: ';
  Result := Result + ClosedDate;
  Result := Result + ',ClosedByEmpID: ';
  Result := Result + IntToStr(ClosedByEmpID);
  Result := Result + ',ClientCode: ';
  Result := Result + ClientCode;
  Result := Result + ',CallCenter: ';
  Result := Result + CallCenter;
  Result := Result + ',StatusCode: ';
  Result := Result + StatusCode;
  Result := Result + ',CompletedTicket: ';
  Result := Result + BoolAs01(CompletedTicket);
  Result := Result + ',TicketType: ';
  Result := Result + TicketType;
  Result := Result + ',WorkloadDate: ';
  Result := Result + WorkloadDate;
  Result := Result + ')';
end;

constructor TLocateReopenedImpl.Create;
begin
  inherited;
end;

destructor TLocateReopenedImpl.Destroy;
begin
  inherited;
end;

function TLocateReopenedImpl.GetLocateID: Integer;
begin
  Result := FLocateID;
end;

procedure TLocateReopenedImpl.SetLocateID( const Value: Integer);
begin
  FLocateID := Value;
end;

function TLocateReopenedImpl.GetReopenedDate: TDateTimeString;
begin
  Result := FReopenedDate;
end;

procedure TLocateReopenedImpl.SetReopenedDate( const Value: TDateTimeString);
begin
  FReopenedDate := Value;
end;

function TLocateReopenedImpl.GetReopenedByEmpID: Integer;
begin
  Result := FReopenedByEmpID;
end;

procedure TLocateReopenedImpl.SetReopenedByEmpID( const Value: Integer);
begin
  FReopenedByEmpID := Value;
end;

function TLocateReopenedImpl.GetClientCode: string;
begin
  Result := FClientCode;
end;

procedure TLocateReopenedImpl.SetClientCode( const Value: string);
begin
  FClientCode := Value;
end;

function TLocateReopenedImpl.GetCallCenter: string;
begin
  Result := FCallCenter;
end;

procedure TLocateReopenedImpl.SetCallCenter( const Value: string);
begin
  FCallCenter := Value;
end;

function TLocateReopenedImpl.GetStatusCode: string;
begin
  Result := FStatusCode;
end;

procedure TLocateReopenedImpl.SetStatusCode( const Value: string);
begin
  FStatusCode := Value;
end;

function TLocateReopenedImpl.GetReopenedTicket: Boolean;
begin
  Result := FReopenedTicket;
end;

procedure TLocateReopenedImpl.SetReopenedTicket(const Value: Boolean);
begin
  FReopenedTicket := Value;
end;

function TLocateReopenedImpl.GetTicketType: string;
begin
  Result := FTicketType;
end;

procedure TLocateReopenedImpl.SetTicketType(const Value: string);
begin
  FTicketType := Value;
end;

function TLocateReopenedImpl.GetWorkloadDate: TDateTimeString;
begin
  Result := FWorkloadDate;
end;

procedure TLocateReopenedImpl.SetWorkloadDate(const Value: TDateTimeString);
begin
  FWorkloadDate := Value;
end;

function TLocateReopenedImpl.SerializeToJSON: string;
const
  LocateReopenedJSON = '"3":{"rec":{"4":{"rec":{' +
    '"1":{"i32":%d},' +   // locate_id
    '"2":{"str":"%s"},' + // reopened_date
    '"3":{"i32":%d},' +   // reopened_by_emp_id
    '"4":{"str":"%s"},' + // client_code
    '"5":{"str":"%s"},' + // call_center
    '"6":{"str":"%s"},' + // status_code
    '"7":{"tf":%s},'   +  // reopens ticket
    '"8":{"str":"%s"},' + // ticket type
    '"9":{"str":"%s"}' +  // workload date
    '}}}}}';
begin
  Result := Format(LocateReopenedJSON, [LocateID, ReopenedDate, ReopenedByEmpID,
    ClientCode, CallCenter, StatusCode, BoolAs01(ReopenedTicket), TicketType,
    WorkloadDate]);
end;

function TLocateReopenedImpl.ToString: string;
begin
  Result := Result + 'LocateID: ';
  Result := Result + IntToStr(LocateID);
  Result := Result + ',ReopenedDate: ';
  Result := Result + ReopenedDate;
  Result := Result + ',ReopenedByEmpID: ';
  Result := Result + IntToStr(ReopenedByEmpID);
  Result := Result + ',ClientCode: ';
  Result := Result + ClientCode;
  Result := Result + ',CallCenter: ';
  Result := Result + CallCenter;
  Result := Result + ',StatusCode: ';
  Result := Result + StatusCode;
  Result := Result + ',ReopenedTicket: ';
  Result := Result + BoolAs01(ReopenedTicket);
  Result := Result + ',TicketType: ';
  Result := Result + TicketType;
  Result := Result + ',WorkloadDate: ';
  Result := Result + WorkloadDate;
  Result := Result + ')';
end;

constructor TEventImpl.Create;
begin
  inherited;
end;

destructor TEventImpl.Destroy;
begin
  inherited;
end;

function TEventImpl.GetAggregateKey: IAggrKey;
begin
  Result := FAggregateKey;
end;

procedure TEventImpl.SetAggregateKey(const Value: IAggrKey);
begin
  FAggregateKey := Value;
end;

function TEventImpl.GetEventID: string;
begin
  Result := FEventID;
end;

procedure TEventImpl.SetEventID(const Value: string);
begin
  FEventID := Value;
end;

function TEventImpl.GetUnion: IEventUnion;
begin
  Result := FUnion;
end;

procedure TEventImpl.SetUnion(const Value: IEventUnion);
begin
  FUnion := Value;
end;

function TEventImpl.SerializeToJSON: string;
begin
  Result := AggregateKey.SerializeToJSON + ',' + FUnion.SerializeToJSON;
end;

function TEventImpl.ToString: string;
begin
  Result := '(AggregateKey: ';
  if (AggregateKey = nil) then
    Result := Result + '<null>'
  else
    Result := Result + AggregateKey.ToString + ',';
  Result := FUnion.ToString;
end;

procedure TEventUnionImpl.ClearUnionValues;
begin
  FLocateAssigned := nil;
  FTicketScheduled := nil;
  FLocateClosed := nil;
  FLocateReopened := nil;
end;

constructor TEventUnionImpl.Create;
begin
  inherited;
end;

destructor TEventUnionImpl.Destroy;
begin
  inherited;
end;

function TEventUnionImpl.GetLocateAssigned: ILocateAssigned;
begin
  Result := FLocateAssigned;
end;

procedure TEventUnionImpl.SetLocateAssigned( const Value: ILocateAssigned);
begin
  ClearUnionValues;
  FLocateAssigned := Value;
end;

function TEventUnionImpl.GetTicketScheduled: ITicketScheduled;
begin
  Result := FTicketScheduled;
end;

procedure TEventUnionImpl.SetTicketScheduled( const Value: ITicketScheduled);
begin
  ClearUnionValues;
  FTicketScheduled := Value;
end;

function TEventUnionImpl.GetLocateClosed: ILocateClosed;
begin
  Result := FLocateClosed;
end;

procedure TEventUnionImpl.SetLocateClosed( const Value: ILocateClosed);
begin
  ClearUnionValues;
  FLocateClosed := Value;
end;

function TEventUnionImpl.GetLocateReopened: ILocateReopened;
begin
  Result := FLocateReopened;
end;

procedure TEventUnionImpl.SetLocateReopened( const Value: ILocateReopened);
begin
  ClearUnionValues;
  FLocateReopened := Value;
end;

function TEventUnionImpl.SerializeToJSON: string;
begin
  if Assigned(LocateClosed) then
    Result := LocateClosed.SerializeToJSON
  else if Assigned(LocateReopened) then
    Result := LocateReopened.SerializeToJSON
  else
    raise Exception.Create('Only LocateClosed and LocateReopened events are supported.');
end;

function TEventUnionImpl.ToString: string;
begin
  Result := '(LocateAssigned: <null>,TicketScheduled: <null>,LocateClosed:';
  if (LocateClosed = nil) then
    Result := Result + '<null>'
  else
    Result := Result + LocateClosed.ToString;
  Result := Result + ',LocateReopened: ';
  if (LocateReopened = nil) then
    Result := Result + '<null>'
  else
    Result := Result + LocateReopened.ToString;
  Result := Result + ')';
end;

constructor TEventListImpl.Create;
begin
  inherited;
  FItems := TObjectList.Create(False);
end;

destructor TEventListImpl.Destroy;
begin
  FreeAndNil(FItems);
  inherited;
end;

function TEventListImpl.GetItems: TObjectList;
begin
  Result := FItems;
end;

procedure TEventListImpl.SetItems( const Value: TObjectList);
begin
  FItems := Value;
end;

function TEventListImpl.ToString: string;
begin
  Result := 'not implemented';
end;


// Type factory methods and registration
function Create_IAggrKey_Impl: IAggrKey;
begin
  Result := TAggrKeyImpl.Create;
end;

function Create_ILocateAssigned_Impl: ILocateAssigned;
begin
  Result := TLocateAssignedImpl.Create;
end;

function Create_ITicketScheduled_Impl: ITicketScheduled;
begin
  Result := TTicketScheduledImpl.Create;
end;

function Create_ILocateClosed_Impl: ILocateClosed;
begin
  Result := TLocateClosedImpl.Create;
end;

function Create_ILocateReopened_Impl: ILocateReopened;
begin
  Result := TLocateReopenedImpl.Create;
end;

function Create_IEvent_Union_Impl: IEventUnion;
begin
  Result := TEventUnionImpl.Create;
end;

function Create_IEvent_Impl: IEvent;
begin
  Result := TEventImpl.Create;
end;

function Create_IEventList_Impl: IEventList;
begin
  Result := TEventListImpl.Create;
end;

end.
