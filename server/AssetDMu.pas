unit AssetDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, DB, ADODB, QMServerLibrary_Intf,
  RemObjectsUtils;

type
  TAssetDM = class(TLogicFeatureDM)
    AssetData: TADODataSet;
    EmployeeAssets: TADODataSet;
    AssetAssignmentData: TADODataSet;
  private
    procedure ActivateOrInsertAssetAssignment(EmpID, AssetID: Integer);
    function ActivateOrInsertNewAsset(const AssetCode, AssetNumber: string): Integer;
    procedure DeactivateExistingAssignmentsExcept(AssetID, EmpID: Integer);
    procedure MarkAssetAssignmentInactive(EmpID, AssetID: Integer);
    procedure SetAssetProperties(AssetID: Integer; const Condition,
      Comment, SerialNum, Description, SolomonProfitCenter, Cost: string);
  public
    function GetAssets(const EmpID: Integer): string;
    procedure SaveEmployeeAssets2(const EmpID: Integer; const Assets: NVPairListList);
    procedure ReassignAssets(const Assets: NVPairListList);
  end;

implementation

uses OdMiscUtils, OdDatasetToXML, OdDbUtils, LogicDMu;

{$R *.dfm}

function TAssetDM.GetAssets(const EmpID: Integer): string;
begin
  try
    AssetData.Parameters.ParamValues['EmpID'] := EmpID;
    AssetData.Open;
    AssetAssignmentData.Parameters.ParamValues['EmpID'] := EmpID;
    AssetAssignmentData.Open;
    Result := ConvertDataSetsToSQLXML([AssetAssignmentData, AssetData],
      ['asset_assignment', 'asset']);
  finally
    AssetData.Close;
    AssetAssignmentData.Close;
  end;
end;

procedure TAssetDM.SaveEmployeeAssets2(const EmpID: Integer; const Assets: NVPairListList);

  function GetNewAssetForID(AssetID: Integer): NVPairList;
  var
    i: Integer;
  begin
    Result := nil;
    for i := 0 to Assets.Count - 1 do begin
      if NVPairInteger(Assets[i], 'asset_id', bvError) = AssetID then begin
        Result := Assets[i];
        Break;
      end;
    end;
  end;

var
  i: Integer;
  NewAssetRow: NVPairList;
  NewAssetID: Integer;
  OldAssetID: Integer;
  NewAssetNumber: string;
  NewAssetCode: string;
  NewSerialNum: string;
  NewDescription: string;
  NewSolomonProfitCenter: string;
  NewCondition: string;
  NewComment: string;
  NewCost: string;
begin
  if EmpID <= 0 then
    raise Exception.Create('Invalid employee ID: ' + IntToStr(EmpID));

  for i := 0 to Assets.Count - 1 do begin
    NewAssetRow := Assets[i];
    // The asset ID may be blank or -1 for new assets, we assign an ID below.
    NewAssetNumber := NVPairString(NewAssetRow, 'asset_number', bvError);
    NewAssetCode := NVPairString(NewAssetRow, 'asset_code', bvError);
    NewSerialNum := NVPairString(NewAssetRow, 'serial_num', bvDefault);
    NewDescription := NVPairString(NewAssetRow, 'description', bvDefault);
    NewSolomonProfitCenter := NVPairString(NewAssetRow, 'solomon_profit_center', bvDefault);
    NewCondition := NVPairString(NewAssetRow, 'condition', bvDefault);
    NewComment := NVPairString(NewAssetRow, 'comment', bvDefault);
    NewCost := NVPairString(NewAssetRow, 'cost', bvDefault);

    // This handles both inserts and changes to the asset number
    NewAssetID := ActivateOrInsertNewAsset(NewAssetCode, NewAssetNumber);
    Assert(NewAssetID > 0);
    SetAssetProperties(NewAssetID, NewCondition, NewComment, NewSerialNum, NewDescription, NewSolomonProfitCenter, NewCost);
    SetNVPairInteger(NewAssetRow, 'asset_id', NewAssetID); // Accurate id needed to get "deletes" right below
    // Blank/unknown asset numbers can be "shared" by multiple employees
    if NewAssetNumber <> '' then
      DeactivateExistingAssignmentsExcept(NewAssetID, EmpID);
    ActivateOrInsertAssetAssignment(EmpID, NewAssetID);
  end;

  EmployeeAssets.Parameters.ParamValues['EmpID'] := EmpID;
  EmployeeAssets.Open;
  try
    // Release any assets from the DB that are not in the passed-in asset list
    while not EmployeeAssets.Eof do begin
      OldAssetID := EmployeeAssets.FieldByName('asset_id').AsInteger;
      Assert(OldAssetID > 0, 'Invalid asset ID: ' + IntToStr(OldAssetID));
      NewAssetRow := GetNewAssetForID(OldAssetID);
      if not Assigned(NewAssetRow) then
        MarkAssetAssignmentInactive(EmpID, OldAssetID);
      EmployeeAssets.Next;
    end;
  finally
    EmployeeAssets.Close;
  end;
end;

procedure TAssetDM.ReassignAssets(const Assets: NVPairListList);
var
  i: Integer;
  AssetID: Integer;
  AssetNumber: string;
  AssetCode: string;
  DestEmpID: Integer;
  Asset: NVPairList;
begin
  for i := 0 to Assets.Count - 1 do begin
    Asset := Assets[i];
    AssetID := NVPairInteger(Asset, 'asset_id', bvError);
    DestEmpID := NVPairInteger(Asset, 'emp_id', bvError);
    AssetNumber := Trim(NVPairString(Asset, 'asset_number', bvError));
    AssetCode := Trim(NVPairString(Asset, 'asset_code', bvError));
    Assert(DestEmpID > 0);
    Assert((AssetID > 0) or (AssetNumber <> ''));
    if AssetID < 1 then
      AssetID := ActivateOrInsertNewAsset(AssetCode, AssetNumber);
    DeactivateExistingAssignmentsExcept(AssetID, DestEmpID);
    ActivateOrInsertAssetAssignment(DestEmpID, AssetID);
  end;
end;

procedure TAssetDM.MarkAssetAssignmentInactive(EmpID, AssetID: Integer);
const
  UpdateSQL = 'update asset_assignment set active = 0 where emp_id = %d and asset_id = %d and active = 1';
begin
  LogicDM.ExecuteSQL(Format(UpdateSQL, [EmpID, AssetID]));
end;

procedure TAssetDM.ActivateOrInsertAssetAssignment(EmpID, AssetID: Integer);
const
  SelectSQL = 'select * from asset_assignment where asset_id = %d and emp_id = %d';
  InsertSQL = 'insert into asset_assignment (asset_id, emp_id) values (%d, %d)';
  UpdateSQL = 'update asset_assignment set active = 1 where asset_id = %d and emp_id = %d and active = 0';
var
  DataSet: TDataSet;
begin
  DataSet := LogicDM.GetDataSetForSQL(Format(SelectSQL, [AssetID, EmpID]));
  if DataSet.Eof then
    LogicDM.ExecuteSQL(Format(InsertSQL, [AssetID, EmpID]))
  else
    LogicDM.ExecuteSQL(Format(UpdateSQL, [AssetID, EmpID]));
  DataSet.Close;
end;

procedure TAssetDM.DeactivateExistingAssignmentsExcept(AssetID, EmpID: Integer);
const
  UpdateSQL = 'update asset_assignment set active = 0 where asset_id = %d and emp_id <> %d and active = 1';
begin
  LogicDM.ExecuteSQL(Format(UpdateSQL, [AssetID, EmpID]));
end;

function TAssetDM.ActivateOrInsertNewAsset(const AssetCode, AssetNumber: string): Integer;
const
  SelectSQL = 'select * from asset where asset_code = ''%s'' and asset_number = ''%s''';
  InsertSQL = 'insert into asset (asset_code, asset_number) values (''%s'', ''%s'')';
var
  DataSet: TDataSet;
begin
  DataSet := LogicDM.GetDataSetForSQL(Format(SelectSQL, [AssetCode, AssetNumber]));
  if DataSet.Eof then begin
    LogicDM.ExecuteSQL(Format(InsertSQL, [AssetCode, AssetNumber]));
    DataSet := LogicDM.GetDataSetForSQL(Format(SelectSQL, [AssetCode, AssetNumber]));
    if DataSet.Eof then
      raise Exception.Create('Insert of asset failed');
    Result := DataSet.FieldByName('asset_id').AsInteger;
  end
  else
    Result := DataSet.FieldByName('asset_id').AsInteger;
  DataSet.Close;
end;

procedure TAssetDM.SetAssetProperties(AssetID: Integer;
  const Condition, Comment, SerialNum, Description, SolomonProfitCenter, Cost: string);

  procedure ChangeFieldIfNecessary(const Field: TField; Value: string);
  begin
    if Field.AsString <> Value then begin
      EditDataSet(Field.DataSet);
      if Value = '' then
        Field.Clear
      else
        Field.AsString := Value;
    end;
  end;

const
  SelectSQL = 'select * from asset where asset_id = %d';
  UpdateSQL = 'update asset_assignment set active = 0 where asset_id = %d and emp_id <> %d and active = 1';
  NewFieldVersion = '2.0.96.19';
var
  DataSet: TDataSet;
  CommentField: TField;
  ConditionField: TField;
  SerialNumField: TField;
  DescriptionField: TField;
  SolomonProfitCenterField: TField;
  CostField: TField;
begin
  DataSet := LogicDM.GetDataSetForSQL(Format(SelectSQL, [AssetID]));
  if DataSet.Eof then
    raise Exception.CreateFmt('Asset ID %d not found in SetAssetProperties', [AssetID]);
  ConditionField := DataSet.FieldByName('condition');
  CommentField := DataSet.FieldByName('comment');
  SerialNumField := DataSet.FieldByName('serial_num');
  DescriptionField := DataSet.FieldByName('description');
  SolomonProfitCenterField := DataSet.FieldByName('solomon_profit_center');
  CostField := DataSet.FieldByName('cost');
  ChangeFieldIfNecessary(CommentField, Comment);
  ChangeFieldIfNecessary(ConditionField, Condition);
  // Don't let old versions clear these out
  if CompareVersionNumber(LogicDM.ClientVersion, ParseVersionString(NewFieldVersion)) > 0 then begin
    ChangeFieldIfNecessary(SerialNumField, SerialNum);
    ChangeFieldIfNecessary(DescriptionField, Description);
    ChangeFieldIfNecessary(SolomonProfitCenterField, SolomonProfitCenter);
    ChangeFieldIfNecessary(CostField, Cost);
  end;
  PostDataSet(DataSet);
  DataSet.Close;
end;

end.
