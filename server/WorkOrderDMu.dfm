inherited WorkOrderDM: TWorkOrderDM
  OldCreateOrder = True
  Height = 241
  Width = 423
  object UpdateWorkOrderStatus: TADOStoredProc
    ProcedureName = 'update_work_order_status;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkOrderID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewStatus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@Closed'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@StatusDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@StatusBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@StatusedHow'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CGAReason'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 146
    Top = 11
  end
  object UpdateWorkOrder: TADOCommand
    CommandText = 
      'UPDATE work_order SET work_description = :work_description,work_' +
      'cross = :work_cross,state_hwy_row = :state_hwy_row,road_bore_cou' +
      'nt = :road_bore_count,driveway_bore_count = :driveway_bore_count' +
      ' WHERE wo_id = :wo_id'
    Parameters = <
      item
        Name = 'work_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3500
        Value = Null
      end
      item
        Name = 'work_cross'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'state_hwy_row'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'road_bore_count'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'driveway_bore_count'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'wo_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 144
    Top = 72
  end
  object UpdateWOAssignments: TADOStoredProc
    ProcedureName = 'assign_work_order;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkOrderID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkerID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 40
    Top = 128
  end
  object UpdateInspection: TADOCommand
    CommandText = 
      'UPDATE work_order_inspection SET '#13#10'  actual_meter_number = :Actu' +
      'alMeterNumber,'#13#10'  actual_meter_location = :ActualMeterLocation,'#13 +
      #10'  building_type = :BuildingType,'#13#10'  cga_visits = :CGAVisits,'#13#10' ' +
      ' map_status = :MapStatus,'#13#10'  mercury_regulator = :MercuryRegulat' +
      'or,'#13#10'  vent_clearance_dist = :VentClearanceDist,'#13#10'  vent_ignitio' +
      'n_dist = :VentIgnitionDist,'#13#10'  alert_first_name = :AlertFirstNam' +
      'e,'#13#10'  alert_last_name = :AlertLastName,'#13#10'  alert_order_number = ' +
      ':AlertOrderNumber,'#13#10'  inspection_change_date = :InspectionChange' +
      'Date,'#13#10'  cga_reason = :CGAReason,'#13#10'  gas_light = :GasLight'#13#10'WHER' +
      'E wo_id = :WorkOrderID'
    Parameters = <
      item
        Name = 'ActualMeterNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'ActualMeterLocation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'BuildingType'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'CGAVisits'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MapStatus'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MercuryRegulator'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'VentClearanceDist'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'VentIgnitionDist'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'AlertFirstName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'AlertLastName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'AlertOrderNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'InspectionChangeDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'CGAReason'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'GasLight'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'WorkOrderID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 250
    Top = 61
  end
  object InsertRemedy: TADOCommand
    CommandText = 
      'insert work_order_remedy (wo_id, work_type_id, active)'#13#10'values (' +
      ':WorkOrderID, :WorkTypeID, :ActiveFlag)'#13#10
    Parameters = <
      item
        Name = 'WorkOrderID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'WorkTypeID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Active'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    Left = 331
    Top = 61
  end
  object UpdateWorkOrderOHMDetails: TADOQuery
    Parameters = <
      item
        Name = 'curbvaluefound'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'wo_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update work_order_OHM_details'
      'set curbvaluefound= :curbvaluefound'
      'where wo_id= :wo_id')
    Left = 152
    Top = 168
  end
  object WorkOrder: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select * from work_order where wo_id = :wo_id and assigned_to_id' +
      ' = :assigned_to_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'wo_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'assigned_to_id'
        Size = -1
        Value = Null
      end>
    Left = 24
    Top = 8
  end
  object Inspection: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from work_order_inspection where wo_id = :wo_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'wo_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'assigned_to_id'
        Size = -1
        Value = Null
      end>
    Left = 250
    Top = 13
  end
  object Remedy: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select * from work_order_remedy '#13#10'where wo_id = :WorkOrderID'#13#10'an' +
      'd work_type_id = :WorkTypeID'
    EnableBCD = False
    Parameters = <
      item
        Name = 'WorkOrderID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'WorkTypeID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 331
    Top = 13
  end
end
