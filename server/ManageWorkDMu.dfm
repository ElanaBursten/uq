inherited ManageWorkDM: TManageWorkDM
  OldCreateOrder = True
  Height = 363
  Width = 409
  object HierDisplaySP: TADOStoredProc
    CacheSize = 50
    CursorType = ctOpenForwardOnly
    ProcedureName = 'hier_display3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ManagerList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@include_neighbors'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 40
    Top = 8
  end
  object AcknowledgeTicket: TADOStoredProc
    ProcedureName = 'acknowledge_ticket;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmpID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 158
    Top = 8
  end
  object AcknowledgeDamageTicket: TADOStoredProc
    ProcedureName = 'acknowledge_damage_ticket;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmpID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@InvestigatorID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 63
  end
  object OQEmpData: TADOStoredProc
    ProcedureName = 'get_oq_data_employee;1'
    Parameters = <
      item
        Name = 'EmpID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 152
    Top = 143
  end
  object GetVirtualBucketTickets: TADOStoredProc
    ProcedureName = 'get_virtual_bucket_tickets;1'
    Parameters = <
      item
        Name = '@ManagerID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@BucketName'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 288
    Top = 136
  end
  object AddEmpPlus: TADOStoredProc
    ProcedureName = 'add_employee_PLUS'
    Parameters = <
      item
        Name = 'LocatorID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'PlusID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'ModifiedByID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'Util'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 272
    Top = 239
  end
  object RemoveEmpPlus: TADOStoredProc
    ProcedureName = 'remove_employee_PLUS'
    Parameters = <
      item
        Name = 'LocatorID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'PlusID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'ModifiedByID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 328
    Top = 255
  end
  object GetEmpPlusUtilbyEmp: TADOStoredProc
    ProcedureName = 'get_WM_empplus_util_by_emp'
    Parameters = <
      item
        Name = 'EmpID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 152
    Top = 231
  end
  object InsertMarsRequest: TADOCommand
    CommandText = 
      'INSERT INTO mars'#13#10'           (tree_emp_id,   new_status,   inser' +
      'ted_by,   insert_date)'#13#10'     VALUES'#13#10'           (:EmpID,        ' +
      '  :NewStatus,    :InsertedBy,    GetDate())'
    Parameters = <
      item
        Name = 'EmpID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'NewStatus'
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = 'InsertedBy'
        DataType = ftInteger
        Value = Null
      end>
    Left = 50
    Top = 285
  end
  object InsertROOptimization: TADOCommand
    CommandText = 
      'INSERT INTO route_optimize_queue'#13#10'           (tree_emp_id,  next' +
      '_ticket_id,  inserted_by,   insert_date, active)'#13#10'     VALUES'#13#10' ' +
      '          (:EmpID,          :NextTicketID,    :InsertedBy,    Ge' +
      'tDate(),  1)'
    Parameters = <
      item
        Name = 'EmpID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'NextTicketID'
        DataType = ftInteger
        Size = 5
        Value = Null
      end
      item
        Name = 'InsertedBy'
        DataType = ftInteger
        Value = Null
      end>
    Left = 165
    Top = 295
  end
  object DamageListDataset: TADODataSet
    CacheSize = 50
    CursorType = ctOpenForwardOnly
    CommandTimeout = 45
    EnableBCD = False
    Parameters = <>
    Left = 289
    Top = 8
  end
  object WorkOrderList: TADODataSet
    CacheSize = 50
    CursorType = ctOpenForwardOnly
    CommandTimeout = 45
    EnableBCD = False
    Parameters = <>
    Left = 288
    Top = 59
  end
  object WorkloadData: TADODataSet
    CacheSize = 50
    CursorType = ctOpenForwardOnly
    CommandTimeout = 45
    EnableBCD = False
    Parameters = <>
    Left = 40
    Top = 64
  end
end
