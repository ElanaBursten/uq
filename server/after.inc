          DM.Conn.CommitTrans;
          DM.Op.Succeeded := True;
          DM.Op.Log(DM.Op.Detail);  // successful
        except
          DM.Op.Log('Rolling back');
          try
            DM.Conn.RollbackTrans;
          except
            on RE: Exception do
              DM.Op.Log('Exception occured while rolling back: ' + RE.Message);
          end;
          raise;
        end;
      finally
        try
          if Assigned(RepDM) then begin
            try
              RepDM.CloseDataSets;
              RepDM.Disconnect;
            finally
              FreeAndNil(RepDM);
            end;
          end;

          DM.CloseDataSets;
        finally
          DM.Disconnect;
        end;
        // put back the default isolation
        DM.DesiredIsolation := DM.DefaultIsolation;
      end;
    except
      on E: Exception do begin
        E.Message := Format('QM Server Error: %s (%s: %s: %s)', [E.Message, DM.Op.OpName, DM.Op.Phase, DM.Op.Detail]);
        try
          DM.Op.Log(E.Message);
        except
          // There is nothing we can do, if we get an error while
          // trying to log the error.
        end;
        raise;
      end;
    end;
  finally
    FAllowExpiredLogin := False;
    FBeforeExecuting := False;
    GetOperationTracker.FinishOperation(DM.Op);
  end;

