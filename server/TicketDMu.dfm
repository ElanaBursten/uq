inherited TicketDM: TTicketDM
  OldCreateOrder = True
  Height = 497
  Width = 545
  object GPSPosition: TADODataSet
    Parameters = <>
    Left = 408
    Top = 8
  end
  object InsertGPS: TADOCommand
    CommandText = 
      'INSERT INTO gps_position ('#13#10'  added_by, '#13#10'  added_date, '#13#10'  lati' +
      'tude, '#13#10'  longitude,'#13#10'  hdop_feet)'#13#10'VALUES ('#13#10'  :added_by, '#13#10'  :' +
      'added_date,'#13#10'  :latitude, '#13#10'  :longitude, '#13#10'  :hdop_feet)'#13#10
    Parameters = <
      item
        Name = 'added_by'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'added_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'latitude'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'longitude'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'hdop_feet'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 410
    Top = 61
  end
  object UpdateAssignments: TADOStoredProc
    ProcedureName = 'assign_locate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 310
    Top = 8
  end
  object BillingQueue: TADOCommand
    CommandText = 
      'insert into billing_queue(event_type, ticket_id, locate_id, even' +
      't_date,   locate_hours_id, locate_status_id) values (:event_type' +
      ', :ticket_id, :locate_id, :event_date,   :locate_hours_id, :loca' +
      'te_status_id)'#13#10
    Parameters = <
      item
        Name = 'event_type'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'locate_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'event_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'locate_hours_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'locate_status_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 310
    Top = 62
  end
  object InsertNotificationQueuesSP: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'insert_notification_queues;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LsID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Status'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@State'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@ClientCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 310
    Top = 172
  end
  object InsertResponderMultiQueue: TADOCommand
    CommandText = 'exec insert_responder_multi_queue :respond_to, :locate_id'
    Parameters = <
      item
        Name = 'respond_to'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'locate_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 310
    Top = 116
  end
  object EditHours: TADOCommand
    CommandText = 
      'update locate_hours'#13#10'set '#13#10'  regular_hours = :regular_hours,'#13#10'  ' +
      'overtime_hours = :overtime_hours,'#13#10'  units_marked = :units_marke' +
      'd,'#13#10'  status = :status'#13#10'where'#13#10'  locate_hours_id = :locate_hours' +
      '_id'
    Parameters = <
      item
        Name = 'regular_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'overtime_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'units_marked'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'locate_hours_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 110
    Top = 172
  end
  object MakeFollowupTicket: TADOStoredProc
    ProcedureName = 'create_followup_ticket_2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FollowupTypeID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TransmitDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DueDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 308
    Top = 228
  end
  object LocateHours: TADODataSet
    CommandText = 'select * from locate_hours'#13#10'where locate_id = :locate_id'
    Parameters = <
      item
        Name = 'locate_id'
        DataType = ftInteger
        Size = 16
        Value = Null
      end>
    Left = 110
    Top = 116
  end
  object LocatePlatData: TADODataSet
    CommandText = 'select * from locate_plat where 0=1'
    Parameters = <>
    Left = 109
    Top = 224
  end
  object TicketInfoData: TADODataSet
    CommandText = 
      'select * from ticket_info'#13#10'where added_by = :AddedBy'#13#10'  and tick' +
      'et_id = :TicketID'#13#10'  and info_type = :InfoType'#13#10'  and info = :In' +
      'fo'#13#10'  and modified_date = :ModifiedDate'
    Parameters = <
      item
        Name = 'AddedBy'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'InfoType'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'Info'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'ModifiedDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 30
    Top = 60
  end
  object LocateFacilityData: TADODataSet
    CommandText = 'select * from locate_facility where 0=1'
    Parameters = <>
    Left = 110
    Top = 276
  end
  object NextTicketID: TADOStoredProc
    ProcedureName = 'NextUniqueID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Name'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = 'NextNewTicketID'
      end>
    Left = 30
    Top = 170
  end
  object TaskSchedule: TADODataSet
    CommandText = 'select * from task_schedule where 0=1'
    Parameters = <>
    Left = 480
    Top = 60
  end
  object NewTicket: TADODataSet
    CommandText = 'select * from ticket where 0=1'
    Parameters = <>
    Left = 29
    Top = 116
  end
  object InsertTicketAck: TADOStoredProc
    ProcedureName = 'insert_ticket_ack;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TicketID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 310
    Top = 284
  end
  object AddClientOld: TADOCommand
    CommandText = 
      'INSERT INTO client'#13#10'           (client_id'#13#10'           ,client_na' +
      'me'#13#10'           ,oc_code'#13#10'           ,office_id'#13#10'           ,acti' +
      've'#13#10'           ,call_center'#13#10'           ,update_call_center'#13#10'   ' +
      '        ,grid_email'#13#10'           ,allow_edit_locates'#13#10'           ' +
      ',customer_id'#13#10'           ,utility_type'#13#10'           ,status_group' +
      '_id'#13#10'           ,unit_conversion_id'#13#10'           ,alert'#13#10'        ' +
      '   ,require_locate_units'#13#10'           ,attachment_url'#13#10'          ' +
      ' ,qh_id)'#13#10'     VALUES'#13#10'           (:client_id'#13#10'           ,:clie' +
      'nt_name'#13#10'           ,:oc_code'#13#10'           ,:office_id'#13#10'         ' +
      '  ,:active'#13#10'           ,:call_center'#13#10'           ,:update_call_c' +
      'enter'#13#10'           ,:grid_email'#13#10'           ,:allow_edit_locates'#13 +
      #10'           ,:customer_id'#13#10'           ,:utility_type'#13#10'          ' +
      ' ,:status_group_id'#13#10'           ,:unit_conversion_id'#13#10'           ' +
      ',:alert'#13#10'           ,:require_locate_units'#13#10'           ,:attachm' +
      'ent_url'#13#10'           ,:qh_id)'#13#10
    Parameters = <
      item
        Name = 'client_id'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'client_name'
        DataType = ftString
        Precision = 40
        Size = -1
        Value = Null
      end
      item
        Name = 'oc_code'
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = 'office_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'active'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = 'update_call_center'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = 'grid_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = 'allow_edit_locates'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'utility_type'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = 'status_group_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'unit_conversion_id'
        Attributes = [paNullable]
        Size = -1
        Value = Null
      end
      item
        Name = 'alert'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'require_locate_units'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'attachment_url'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = '200'
      end
      item
        Name = 'qh_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end>
    Left = 318
    Top = 430
  end
  object ClientData: TADODataSet
    CommandText = 
      'Select client_id'#13#10'           ,client_name'#13#10'           ,oc_code'#13#10 +
      '           ,office_id'#13#10'           ,active'#13#10'           ,call_cent' +
      'er'#13#10'           ,update_call_center'#13#10'           ,grid_email'#13#10'    ' +
      '       ,allow_edit_locates'#13#10'           ,customer_id'#13#10'           ' +
      ',utility_type'#13#10'           ,status_group_id'#13#10'           ,unit_con' +
      'version_id'#13#10'           ,alert'#13#10'           ,require_locate_units'#13 +
      #10'           ,attachment_url'#13#10'           ,qh_id'#13#10'           ,pare' +
      'nt_client_id'#13#10'Where parent_client_id=:parent_client_id'
    Parameters = <
      item
        Name = 'parent_client_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end>
    Left = 320
    Top = 384
  end
  object ADOStoredProc1: TADOStoredProc
    ProcedureName = 'assign_locate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 438
    Top = 168
  end
  object getHPRanking: TADOQuery
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <>
    Left = 200
    Top = 396
  end
  object GetHPMultiCodes: TADOQuery
    CursorType = ctStatic
    EnableBCD = False
    Parameters = <>
    Left = 200
    Top = 444
  end
  object Ticket: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select ticket_number,revision,  ticket_id, followup_type_id,'#13#10'  ' +
      'ticket_format, source, watch_and_protect, status, work_state, '#13#10 +
      '  work_city, transmit_date, due_date,'#13#10' route_area_id, ticket_ty' +
      'pe, kind'#13#10' from ticket'#13#10'where ticket_id=:id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 30
    Top = 12
  end
  object Locate: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select active,added_by,client_code,client_id,closed,closed_by_id' +
      ',closed_date,closed_how,assigned_to,'#13#10'high_profile,high_profile_' +
      'reason,locate_id,modified_date,qty_marked,status,watch_and_prote' +
      'ct, mark_type, entry_date, ticket_id, gps_id,'#13#10'(select max(locat' +
      'or_id) from assignment a where a.locate_id = locate.locate_id an' +
      'd a.active=1) as locator_id, status_changed_by_id, '#13#10'(select max' +
      '(workload_date) from assignment a where a.locate_id = locate.loc' +
      'ate_id and a.active=1) as workload_date '#13#10'from locate where tick' +
      'et_id = :id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    Left = 112
    Top = 8
  end
  object FindLocateQuery: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select locate_id from locate'#13#10' where ticket_id=:ticket_id'#13#10'  and' +
      ' client_code=:client_code'
    EnableBCD = False
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    Left = 112
    Top = 61
  end
  object LocatorOnTicket: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select coalesce(min(l.assigned_to), min(l.assigned_to_id)) as Lo' +
      'cator_id'#13#10' from locate l'#13#10' inner join assignment a on l.locate_i' +
      'd=a.locate_id'#13#10' where l.ticket_id=:ticket_id'#13#10'  and a.active=1'#13#10 +
      '  and l.active=1'#13#10
    EnableBCD = False
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 200
    Top = 8
  end
  object HighProfileEmailList: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select email_address'#13#10'from notification_email_by_client'#13#10'where n' +
      'otification_type = '#39'highprofile'#39#13#10'  and client_id = :client_id '
    EnableBCD = False
    Parameters = <
      item
        Name = 'client_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 216
    Top = 70
  end
  object StatusList: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from StatusList order by status'
    EnableBCD = False
    Parameters = <>
    Left = 200
    Top = 116
  end
  object FindClientCode: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select client_id from client (NOLOCK)'#13#10' where oc_code=:code and ' +
      'call_center=:center and'#13#10'active = 1'#13#10'order by active desc, clien' +
      't_id asc'
    EnableBCD = False
    Parameters = <
      item
        Name = 'code'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'center'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 200
    Top = 172
  end
  object TicketImageData: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select ticket_id, ticket_format, image, xml_ticket_format '#13#10'from' +
      ' ticket left outer join call_center on ticket_format = cc_code'#13#10 +
      'where ticket_id=:id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 29
    Top = 224
  end
  object getUtilityType: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select utility_type, client_id  '#13#10'from client (NOLOCK)'#13#10' where o' +
      'c_code= :ClientCode '#13#10'and call_center= :CallCenter'#13#10' and active ' +
      '= 1'#13#10'order by active desc, client_id asc'
    EnableBCD = False
    Parameters = <
      item
        Name = 'ClientCode'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 200
    Top = 244
  end
  object EPRHistorybyTicket: TADODataSet
    CursorType = ctStatic
    CommandText = 'Select * from epr_response_history'#13#10'where ticket_id= :ticket_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'ticket_id'
        DataType = ftInteger
        Value = Null
      end>
    Left = 32
    Top = 312
  end
  object GetTicketIDFromLocate: TADODataSet
    CursorType = ctStatic
    CommandText = 'select ticket_id '#13#10'from locate where locate_id =:LocateID'
    EnableBCD = False
    Parameters = <
      item
        Name = 'LocateID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 200
    Top = 312
  end
  object LocatesForTicket: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select a.locate_id, a.locator_id, e.short_name as locator, l.tic' +
      'ket_id, t.ticket_number,'#13#10'       l.status, l.client_code, '#13#10#9'   ' +
      'c.client_name as client, l.high_profile,'#13#10#9'   qty_marked,'#13#10#9'   l' +
      '.closed, l.closed_date,'#13#10' c.alert, l.alert as locatealert'#13#10' from' +
      ' locate l'#13#10'  left join ticket t on l.ticket_id=t.ticket_id'#13#10'  le' +
      'ft join client c on l.client_id=c.client_id'#13#10'  left join assignm' +
      'ent a on l.locate_id = a.locate_id'#13#10'  left join employee e on a.' +
      'locator_id=e.emp_id'#13#10' where (a.active=1)'#13#10'  and (l.ticket_id=:ti' +
      'cket_id)'#13#10'  and (a.locate_id >0)'#13#10'  and (l.status <> '#39'-N'#39')'
    EnableBCD = False
    Parameters = <
      item
        Name = 'ticket_id'
        DataType = ftInteger
        Value = Null
      end>
    Left = 24
    Top = 376
  end
  object RiskScoreHistoryByTicket: TADODataSet
    CursorType = ctStatic
    CommandText = 'Select * from ticket_risk where ticket_id=:ticket_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'ticket_id'
        DataType = ftInteger
        Value = Null
      end>
    Left = 88
    Top = 424
  end
end
