unit ServerAttachmentDMu;

// Until the conversion from ADO to FireDAC is not completed, 
// changes in this unit and in ServerAttachmentAdDMu should be synchronized.

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseAttachmentDMu, DB, ADODB, IdFtp, OdMiscUtils, IdIOHandler,
  IdTCPConnection;

type
  TLogReportMessageEvent = procedure(Sender: TObject; const LogMessage: string;
    const IsError: Boolean) of object;
  TCorruptAttachmentEvent = procedure(Sender: TObject;
    const AttachmentID: Integer; const DownloadedFilename: string) of object;

  TAttachmentObject = class(TObject)
  public
    AttachmentID: Integer;
  end;

  TNewAttachment = class(TAttachmentObject)
  public
    ForeignType: Integer;
    ForeignID: Integer;
    Size: Integer;
    AttachedBy: Integer;
    FileName: string;
    OrigFileName: string;
    OrigFileModDate: TDateTime;
    AttachDate: TDateTime;
    Source: string;
    FileHash: string;
    UploadMachineName: string;
  end;

  TAttachmentUpload = class(TAttachmentObject)
  public
    UploadDate: TDateTime;
    BackgroundUpload: Boolean;
  end;

  TAttachmentUpdate = class(TAttachmentObject)
  public
    Active: Boolean;
    Comment: string;
    DocumentType: string;
  end;

  TAttachment = class(TNewAttachment)
  public
    Active: Boolean;
    Comment: string;
    UploadDate: TDateTime;
    BackgroundUpload: Boolean;
    ServerLocatedDate: TDateTime;
    DocumentType: string;
  end;

  TFileCheckModes = set of (fcmCheckFileSize, fcmCheckFileHash);

  TServerAttachment = class(TBaseAttachment)
    AttachmentData: TADODataSet;
    TempQuery: TADODataSet;
  private
    FTP: TIdFTP;
    FFileCheckMode: TFileCheckModes;
    FRootDir: string;
    FClientVersion: TVersionNumber;
    FOnLogReportMessage: TLogReportMessageEvent;
    FOnCorruptAttachment: TCorruptAttachmentEvent;
    procedure FinalizeFTPConnection;
    procedure InitializeFTPConnection;
    function CheckAttachmentFileHash(FTP: TIdFTP; const FileName,
      FileHash: string; const ForeignType, ForeignID: Integer): Boolean;
    function GetExistingAttachmentRecord(Attach: TNewAttachment): Boolean;
    function GetExistingAttachmentRecordID(Attach: TNewAttachment): Integer;
    procedure InsertNewAttachmentRecord;
    procedure UpdateFieldIfNecessary(const FieldName: string;
      Value: Variant);
    procedure UpdateFieldIfNecessaryDefaultToNull(const FieldName: string;
      Value: Variant; DefaultValue: Variant);
    procedure LogProgress(const Msg, Phase: string);
    procedure Startup(const Connection: TADOConnection);
    procedure ValidateFileOnServer(const FileName, OrigFileName, FileHash: string;
      const FileSize, ForeignType, ForeignID: Integer);

  protected
    function GetDownloadLocationSQL: string; override;
    function GetUploadFileTypesSQL: string; override;
    function GetAttachmentParentSQL: string; override;
    function GetDuplicateAttachmentCheckSQL: string; override;
    function OpenQuery(const SQL: string): TDataSet; override;
    procedure LogReportMessage(const Msg: string; const IsError: Boolean=False);
  public
    constructor Create(const Connection: TADOConnection; const EmpID: Integer; const FileCheckMode: TFileCheckModes = [fcmCheckFileHash]); reintroduce; overload;
    constructor Create(const Connection: TADOConnection); reintroduce; overload;
    destructor Destroy; override;
    property ClientVersion: TVersionNumber read FClientVersion write FClientVersion;
    property OnLogReportMessage: TLogReportMessageEvent read FOnLogReportMessage
      write FOnLogReportMessage;
    property OnCorruptAttachment: TCorruptAttachmentEvent
      read FOnCorruptAttachment write FOnCorruptAttachment;
    function AttachUploadedFileToRecordAWS(const ForeignType,
      ForeignID: Integer; const FileName, UploadedFileName, Comment, Source: string;
      const DocumentType: string=''):integer;
    function AttachFileToRecord(const ForeignType, ForeignID: Integer; const FileName, Source: string; const CanAttachAnything: Boolean): string; override;
    procedure AttachUploadedFileToRecord(const ForeignType, ForeignID: Integer;
      const FileName, UploadedFileName, Comment, Source: string;
      const DocumentType: string='');
    procedure Add(const Attachment: TAttachment; var OldId, NewId: Integer);
    procedure InsertNew(const NewAttachment: TNewAttachment; var OldId, NewId: Integer);
    procedure MarkUploaded(const AttachmentUpload: TAttachmentUpload);
    procedure UpdateInfo(const AttachmentUpdate: TAttachmentUpdate);
    procedure LocateAttachment(const AttachmentId: Integer);
    procedure DownloadAttachments(ForeignId, ForeignType: Integer; FileList, ErrorLog: TStrings;
      const AttachmentIDList, Destination: string; UseOrigFilename: Boolean=False);
    procedure UploadAttachment(const Location, ForeignType, ForeignID: Integer; const FileName: string);
    function DownloadAttachment(const AttachmentID: Integer): string; override;
    procedure RemoveAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string); override;
    function GetFileExtensionsForPrintableFileType(const FileType: string): string;
    function AttachmentsIncludeFileType(Dataset: TDataset; const FileTypes: string): Boolean;
    function GetAttachmentDownloadFolder: string; override;
    function GetAttachmentFolder: string; override;
    procedure DownloadAttachmentsForAttachmentIDs(const AttachmentIDList,
      DestinationFolder: string; const UseOrigFilename: Boolean);
  end;

implementation

{$R *.dfm}
uses OdDbUtils, OdAdoUtils, OdUqInternet, OdSecureHash, JclSysInfo,
  JclFileUtils, JclStrings, OdFtp, QMConst, OdExceptions;


function TServerAttachment.AttachUploadedFileToRecordAWS(const ForeignType,
  ForeignID: Integer; const FileName, UploadedFileName, Comment, Source: string;
  const DocumentType: string=''):integer;
var
  Attach: TAttachment;
  OldID, NewID: Integer;
begin
  Attach := TAttachment.Create;
  try
    OldID := -Random(1000000) - 10000;
    Attach.AttachmentID := OldID;
    Attach.ForeignType := ForeignType;
    Attach.ForeignID := ForeignID;
    Attach.AttachedBy := AttachedByEmpID;
    Attach.FileName := ExtractFileName(UploadedFileName);
    Attach.OrigFileName := FileName;
    Attach.OrigFileModDate := GetFileDateTime(UploadedFileName);
    Attach.AttachDate := Now;
    Attach.UploadDate := Attach.AttachDate;
    Attach.UploadMachineName := GetLocalComputerName;
    Attach.Size := GetSizeOfFile(UploadedFileName);
    Attach.Active := True;
    Attach.Source := Source;
    Attach.Comment := Comment;
    Attach.FileHash := HashFile(UploadedFileName);
    Attach.DocumentType := DocumentType;
    Add(Attach, OldID, NewID);
    Assert(NewID > 0);
    Log(Format('Inserted attachment_id %d', [NewID]));
  finally
    result := NewID;
    FreeAndNil(Attach);

  end;
end;




procedure TServerAttachment.AttachUploadedFileToRecord(const ForeignType,
  ForeignID: Integer; const FileName, UploadedFileName, Comment, Source: string;
  const DocumentType: string='');
var
  Attach: TAttachment;
  OldID, NewID: Integer;
begin
  Attach := TAttachment.Create;
  try
    OldID := -Random(1000000) - 10000;
    Attach.AttachmentID := OldID;
    Attach.ForeignType := ForeignType;
    Attach.ForeignID := ForeignID;
    Attach.AttachedBy := AttachedByEmpID;
    Attach.FileName := ExtractFileName(UploadedFileName);
    Attach.OrigFileName := FileName;
    Attach.OrigFileModDate := GetFileDateTime(UploadedFileName);
    Attach.AttachDate := Now;
    Attach.UploadDate := Attach.AttachDate;
    Attach.UploadMachineName := GetLocalComputerName;
    Attach.Size := GetSizeOfFile(UploadedFileName);
    Attach.Active := True;
    Attach.Source := Source;
    Attach.Comment := Comment;
    Attach.FileHash := HashFile(UploadedFileName);
    Attach.DocumentType := DocumentType;
    Add(Attach, OldID, NewID);
    Assert(NewID > 0);
    Log(Format('Inserted attachment_id %d', [NewID]));
  finally
    FreeAndNil(Attach);
  end;
end;

function TServerAttachment.AttachFileToRecord(const ForeignType,
  ForeignID: Integer; const FileName, Source: string; const CanAttachAnything: Boolean): string;
var
  NewFileName: string;
  NewFilePath: string;
  Attach: TAttachment;
  OldID, NewID: Integer;
begin
  CanIAttachAnything := CanAttachAnything;
  NewFileName := PrepareFileForAttachment(ForeignType, ForeignID, FileName, Source);
  Result := NewFileName;
  Attach := TAttachment.Create;
  try
    OldID := -Random(1000000) - 10000;
    Attach.AttachmentID := OldID;
    Attach.ForeignType := ForeignType;
    Attach.ForeignID := ForeignID;
    Attach.AttachedBy := AttachedByEmpID;
    Attach.FileName := NewFileName;
    Attach.OrigFileName := ExtractFileName(FileName);
    Attach.OrigFileModDate := GetFileDateTime(FileName);
    Attach.AttachDate := Now;
    Attach.UploadMachineName := GetLocalComputerName;
    Attach.Size := GetSizeOfFile(NewFilePath);
    Attach.Active := True;
    Attach.Source := Source;
    Attach.FileHash := HashFile(FileName);

    Add(Attach, OldID, NewID);
    Assert(NewID > 0);
  finally
    FreeAndNil(Attach);
  end;
end;

function TServerAttachment.AttachmentsIncludeFileType(Dataset: TDataset; const FileTypes: string): Boolean;
var
  FileExtension: String;
begin
  Assert(Assigned(Dataset));
  Assert(Dataset.Active);
  Assert(Assigned(Dataset.FieldByName('orig_filename')));
  Result := False;
  Dataset.First;
  while not Dataset.EOF do begin
    FileExtension := ExtractFileExt(Dataset.FieldByName('orig_filename').AsString);
    Result := Result or StrContainsText(FileExtension, FileTypes);
    if Result then
      Break;
    Dataset.Next;
  end;
  Dataset.First;
end;

constructor TServerAttachment.Create(const Connection: TADOConnection);
begin
  inherited Create(nil);
  Startup(Connection);
end;

constructor TServerAttachment.Create(const Connection: TADOConnection;
  const EmpID: Integer; const FileCheckMode: TFileCheckModes);
begin
  inherited Create(nil);
  Startup(Connection);
  AttachedByEmpID := EmpID;
  FFileCheckMode := FileCheckMode;
end;

procedure TServerAttachment.Startup(const Connection: TADOConnection);
begin
  ApplicationName := 'Q Manager';
  AttachedByEmpID := -1;
  CanIAttachAnything := False;
  FFileCheckMode := [];
  ClientVersion := ParseVersionString('0.0.0.0');

  SetConnections(Connection, Self);
  Assert(Assigned(Connection), 'Database connection is not assigned');
end;

destructor TServerAttachment.Destroy;
begin
  if Assigned(FTP) then
    FinalizeFTPConnection;

  inherited;
end;

procedure TServerAttachment.UploadAttachment(const Location, ForeignType, ForeignID: Integer; const FileName: string);
var
  FileToUpload: string;
  FtpFileName: string;
  YearDir: string;
  ForeignTypeDir: string;
  DateDir: string;
  IDDir: string;
begin
  Assert(Location > 0, 'Invalid location ID in UploadAttachment: ' + IntToStr(Location));
  Assert(ForeignType > 0, 'Invalid foreign type in UploadAttachment: ' + IntToStr(ForeignType));
  Assert(ForeignID > 0, 'Invalid foreign ID in UploadAttachment: ' + IntToStr(ForeignID));
  Assert(FileName <> '', 'Empty FileName in UploadAttachment');

  Log('Connecting to FTP server directory');
  if not Assigned(FTP) then
    InitializeFTPConnection;
  Assert(Assigned(FTP), 'FTP Connection is not assigned in UploadAttachment');
  PrepareFTPServer(FTP, Location, FRootDir);
  FtpFileName := ExtractFileName(FileName);
  FileToUpload := FileName;
  if FileExists(FileToUpload) then begin
    GetDirectoryNamesForAttachment(ForeignType, ForeignID, ForeignTypeDir, YearDir, DateDir, IDDir);
    ChangeToOrMakeDir(FTP, ForeignTypeDir);
    ChangeToOrMakeDir(FTP, YearDir);
    ChangeToOrMakeDir(FTP, DateDir);
    ChangeToOrMakeDir(FTP, IDDir);
    Log('Uploading file: ' + FileName);
    FTP.Put(FileToUpload, FtpFileName);
    FTP.ChangeDir(FRootDir);
  end else
    raise Exception.Create('ERROR: Attached file no longer exists to upload: ' + FileToUpload);
end;

procedure TServerAttachment.InitializeFTPConnection;
begin
  Assert(not Assigned(FTP));
  FTP := OdUqInternet.CreateFTPDownloader(GetFTPDownloaderData);
  FRootDir := FTP.RetrieveCurrentDir;
end;

procedure TServerAttachment.FinalizeFTPConnection;
begin
  try
    if FTP.Connected then
      FTP.Disconnect;
  finally
    FreeAndNil(FTP);
  end;
end;

function TServerAttachment.GetAttachmentParentSQL: string;
begin
  Result := 'select top 1 * from %s where %s = %d %s';
end;

function TServerAttachment.GetDownloadLocationSQL: string;
begin
  Result := 'select * from upload_location where description = ''Download'' and active=1';
end;

function TServerAttachment.GetDuplicateAttachmentCheckSQL: string;
begin
  Result := 'select count(*) from attachment where foreign_type = %d ' +
    'and foreign_id = %d and orig_filename = %s and active = 1';
end;

function TServerAttachment.GetUploadFileTypesSQL: string;
begin
  Result := 'select * from upload_file_type where active=1';
end;

function TServerAttachment.OpenQuery(const SQL: string): TDataSet;
begin
  TempQuery.Close;
  TempQuery.CommandText := SQL;
  TempQuery.Open;
  Result := TempQuery;
end;

procedure TServerAttachment.LogReportMessage(const Msg: string;
  const IsError: Boolean=False);
begin
  if Assigned(FOnLogReportMessage) then
    FOnLogReportMessage(Self, Msg, IsError);
end;

procedure TServerAttachment.RemoveAttachment(const ForeignType,
  ForeignID: Integer; const FileName, Source: string);
var
  Recs: Integer;
const
  SQL = 'update attachment set active=0 where foreign_type=%d and foreign_id=%d ' +
    'and orig_filename=''%s'' and IsNull(source, '''')=''%s'' and active=1';
begin
  Recs := ExecuteQuery(AttachmentData.Connection,
    Format(SQL, [ForeignType, ForeignID, FileName, Source]));
  if (Recs > 1) and (Source <> AddinAttachmentSource) then
    raise Exception.CreateFmt('More than 1 active attachment found to remove for Type / Id / Orig File / Source: %d / %d / %s / %s',
      [ForeignType, ForeignID, FileName, Source]);
  if Recs < 1 then
    raise Exception.CreateFmt('No attachment found to remove for Type / Id / Orig File / Source: %d / %d / %s / %s',
      [ForeignType, ForeignID, FileName, Source]);
end;

function TServerAttachment.CheckAttachmentFileHash(FTP: TIdFTP; const FileName, FileHash: string; const ForeignType, ForeignID: Integer): Boolean;
var
  AttachmentData: OdUqInternet.TAttachmentData;
  Phase: string;
begin
  Assert(FTP <> nil);
  Assert(FileName <> '');
  Assert(FileHash <> '', 'Blank hash is not allowed');

  try
    Phase := 'AttachmentData.ServerFilename';
    AttachmentData.ServerFilename := FileName;
    Phase := Format('GetDirectoryNamesForAttachment %d %d %s %s %s %s', [ForeignType, ForeignID, AttachmentData.ForeignTypeDir, AttachmentData.YearDir, AttachmentData.DateDir, AttachmentData.IDDir]);
    GetDirectoryNamesForAttachment(ForeignType, ForeignID, AttachmentData.ForeignTypeDir, AttachmentData.YearDir, AttachmentData.DateDir, AttachmentData.IDDir);
    Phase := Format('CheckAttachmentFileHashOnFTPServer.GetFile %s on %s:%d', [FileName, FTP.Host, FTP.Port]);
    AttachmentData.FileHash := FileHash;
    Result := OdUqInternet.CheckFileHashOnFTPServer(FTP, AttachmentData);
  except
    on E: Exception do
      raise Exception.Create(E.Message + ' in ' + Phase);
  end;
end;

procedure TServerAttachment.UpdateFieldIfNecessary(const FieldName: string; Value: Variant);
var
  Field: TField;
begin
  Field := AttachmentData.FieldByName(FieldName);
  if Field.Value <> Value then begin
    if not (AttachmentData.State in dsEditModes) then
      AttachmentData.Edit;
    Field.Value := Value;
  end;
end;

procedure TServerAttachment.UpdateFieldIfNecessaryDefaultToNull(
  const FieldName: string; Value: Variant; DefaultValue: Variant);
begin
  if Value = DefaultValue then
     UpdateFieldIfNecessary(FieldName, Null)
  else
     UpdateFieldIfNecessary(FieldName, Value);
end;

function TServerAttachment.GetExistingAttachmentRecord(Attach: TNewAttachment): Boolean;
const
  GetExisting = 'select * from attachment where filename = :filename '+
    'and orig_filename = :orig_filename and foreign_type = :foreign_type '+
    'and foreign_id = :foreign_id and size = :size and file_hash = :file_hash';
begin
  AttachmentData.Close;
  AttachmentData.CommandText := GetExisting;
  AttachmentData.Parameters.ParamValues['file_hash'] := Attach.FileHash;
  AttachmentData.Parameters.ParamValues['filename'] := Attach.FileName;
  AttachmentData.Parameters.ParamValues['orig_filename'] := Attach.OrigFileName;
  AttachmentData.Parameters.ParamValues['foreign_type'] := Attach.ForeignType;
  AttachmentData.Parameters.ParamValues['foreign_id'] := Attach.ForeignID;
  AttachmentData.Parameters.ParamValues['size'] := Attach.Size;
  AttachmentData.Open;
  Log('GetExistingAttachmentRecord found ' + IntToStr(AttachmentData.RecordCount));
  Result := not AttachmentData.IsEmpty;
end;

function TServerAttachment.GetExistingAttachmentRecordID(Attach: TNewAttachment): Integer;
begin
  if GetExistingAttachmentRecord(Attach) then
    Result := AttachmentData.FieldByName('attachment_id').AsInteger
  else
    Result := -1;
end;

procedure TServerAttachment.InsertNewAttachmentRecord;
begin
  AttachmentData.Close;
  AttachmentData.CommandText := 'select * from attachment where 0=1';
  AttachmentData.Open;
  if not AttachmentData.Eof then
    raise Exception.Create('Attachment error: 0=1?');
  AttachmentData.Insert;
end;

procedure TServerAttachment.Add(const Attachment: TAttachment; var OldId, NewId: Integer);

  procedure CheckForReactivate;
  begin
    if Attachment.Active and (not AttachmentData.FieldByName('active').AsBoolean) and
      (HasDuplicatedAttachmentFileName(Attachment.ForeignType, Attachment.ForeignID, Attachment.OrigFileName)) then begin
      raise Exception.CreateFmt('Cannot make attachment active. ' +
        'An active attachment already exists with that filename (%s)',
        [Attachment.OrigFileName]);
    end;
  end;

var
  OriginalUploadDateNull: Boolean;
begin
  OldID := Attachment.AttachmentID;
  AttachmentData.Close;
  if OldID > 0 then begin
    AttachmentData.CommandText := 'select * from attachment where attachment_id = ' + IntToStr(OldID);
    AttachmentData.Open;
    if AttachmentData.Eof then
      raise Exception.CreateFmt('Attachment %d not found to save changes', [OldID]);
    CheckForReactivate;
  end else begin
    if not GetExistingAttachmentRecord(Attachment) then
      InsertNewAttachmentRecord;
  end;
  AttachmentData.FieldByName('modified_date').Required := False;
  OriginalUploadDateNull := AttachmentData.FieldByName('upload_date').IsNull;

  UpdateFieldIfNecessary('filename', Attachment.FileName);
  UpdateFieldIfNecessary('orig_filename', Attachment.OrigFileName);
  UpdateFieldIfNecessary('orig_file_mod_date', Attachment.OrigFileModDate);
  UpdateFieldIfNecessary('comment', Attachment.Comment);
  UpdateFieldIfNecessary('foreign_type', Attachment.ForeignType);
  UpdateFieldIfNecessary('foreign_id', Attachment.ForeignID);
  UpdateFieldIfNecessary('active', Attachment.Active);
  UpdateFieldIfNecessary('size', Attachment.Size);
  UpdateFieldIfNecessary('extension', ExtractFileExt(Attachment.OrigFileName));
  UpdateFieldIfNecessary('attached_by', Attachment.AttachedBy);
  UpdateFieldIfNecessaryDefaultToNull('file_hash', Attachment.FileHash, '');
  UpdateFieldIfNecessaryDefaultToNull('source', Attachment.Source, '');
  UpdateFieldIfNecessaryDefaultToNull('attach_date', Attachment.AttachDate, 0);
  UpdateFieldIfNecessaryDefaultToNull('upload_date', Attachment.UploadDate, 0);
  UpdateFieldIfNecessaryDefaultToNull('background_upload',
    Attachment.BackgroundUpload, False);
  UpdateFieldIfNecessaryDefaultToNull('upload_machine_name',
    Attachment.UploadMachineName, '');
  UpdateFieldIfNecessaryDefaultToNull('doc_type', Attachment.DocumentType, '');

  case AttachmentData.State of
    dsEdit: Log('Updating attachment # ' + AttachmentData.FieldByName('attachment_id').AsString);
    dsInsert: Log('Inserting new attachment');
    else Log('No attachment update needed');
  end;

  PostDataSet(AttachmentData);
  NewID := AttachmentData.FieldByName('attachment_id').AsInteger;
  if NewID < 1 then
    raise Exception.Create('Invalid attachment ID after posting: ' + IntToStr(NewID));

  if OriginalUploadDateNull and (Attachment.UploadDate <> 0) then
    ValidateFileOnServer(Attachment.FileName, Attachment.OrigFileName, Attachment.FileHash, Attachment.Size, Attachment.ForeignType, Attachment.ForeignID);
end;

procedure TServerAttachment.InsertNew(const NewAttachment: TNewAttachment; var OldId, NewId: Integer);
var
  ExistingId: Integer;
begin
  OldID := NewAttachment.AttachmentID;
  Assert(OldID < 0, 'Attachment_id must be < 0');
  ExistingId := GetExistingAttachmentRecordID(NewAttachment);
  if ExistingId >= 0 then
    NewId := ExistingId  //there could have been a problem during the first insert, and we need to make sure the id is passed back
  else begin
    InsertNewAttachmentRecord;
    AttachmentData.FieldByName('modified_date').Required := False;

    UpdateFieldIfNecessary('filename', NewAttachment.FileName);
    UpdateFieldIfNecessary('orig_filename', NewAttachment.OrigFileName);
    UpdateFieldIfNecessary('orig_file_mod_date', NewAttachment.OrigFileModDate);
    UpdateFieldIfNecessary('foreign_type', NewAttachment.ForeignType);
    UpdateFieldIfNecessary('foreign_id', NewAttachment.ForeignID);
    UpdateFieldIfNecessary('active', True);
    UpdateFieldIfNecessary('size', NewAttachment.Size);
    UpdateFieldIfNecessary('extension', ExtractFileExt(NewAttachment.OrigFileName));
    UpdateFieldIfNecessary('attached_by', NewAttachment.AttachedBy);
    UpdateFieldIfNecessaryDefaultToNull('file_hash', NewAttachment.FileHash, '');
    UpdateFieldIfNecessaryDefaultToNull('source', NewAttachment.Source, '');
    UpdateFieldIfNecessaryDefaultToNull('attach_date', NewAttachment.AttachDate, 0);
    UpdateFieldIfNecessaryDefaultToNull('upload_machine_name', NewAttachment.UploadMachineName, '');
    if AttachmentData.State = dsInsert then
      Log('Inserting new attachment')
    else
      Log('No attachment to add');

    PostDataSet(AttachmentData);
    NewID := AttachmentData.FieldByName('attachment_id').AsInteger;
  end;
  if NewID < 1 then
    raise Exception.Create('Invalid attachment ID after posting: ' + IntToStr(NewID));
end;

procedure TServerAttachment.MarkUploaded(const AttachmentUpload: TAttachmentUpload);
begin
  Assert(AttachmentUpload.UploadDate > 0, 'UploadDate is missing in TServerAttachment.MarkUploaded');
  LocateAttachment(AttachmentUpload.AttachmentID);
  if AttachmentData.FieldByName('upload_date').IsNull then begin
    UpdateFieldIfNecessary('upload_date', AttachmentUpload.UploadDate);
    UpdateFieldIfNecessary('background_upload', AttachmentUpload.BackgroundUpload);
  end;
  if AttachmentData.State = dsEdit then
    Log('Marking attachment # ' + AttachmentData.FieldByName('attachment_id').AsString + ' as uploaded')
  else
    Log('No new upload info for attachment');
  PostDataSet(AttachmentData);
end;

procedure TServerAttachment.UpdateInfo(const AttachmentUpdate: TAttachmentUpdate);

  procedure CheckForReactivate;
  var
    ForeignID, ForeignType: Integer;
    OrigFileName: string;
  begin
    ForeignType := AttachmentData.FieldByName('foreign_type').AsInteger;
    ForeignID := AttachmentData.FieldByName('foreign_id').AsInteger;
    OrigFileName := AttachmentData.FieldByName('orig_filename').AsString;
    if AttachmentUpdate.Active and (not AttachmentData.FieldByName('active').AsBoolean) and
      (HasDuplicatedAttachmentFileName(ForeignType, ForeignID, OrigFileName)) then begin
      raise Exception.CreateFmt('Cannot make attachment active. ' +
        'An active attachment already exists with that filename (%s)', [OrigFileName]);
    end;
  end;

begin
  LocateAttachment(AttachmentUpdate.AttachmentID);
  CheckForReactivate;
  UpdateFieldIfNecessary('active', AttachmentUpdate.Active);
  UpdateFieldIfNecessaryDefaultToNull('comment', AttachmentUpdate.Comment, '');
  UpdateFieldIfNecessaryDefaultToNull('doc_type', AttachmentUpdate.DocumentType, '');
  if AttachmentData.State = dsEdit then
    Log('Updating attachment # ' + AttachmentData.FieldByName('attachment_id').AsString)
  else
    Log('No attachment update needed');
  PostDataSet(AttachmentData);
end;

procedure TServerAttachment.LocateAttachment(const AttachmentId: Integer);
begin
  AttachmentData.Close;
  AttachmentData.CommandText := 'select * from attachment where attachment_id = ' + IntToStr(AttachmentID);
  AttachmentData.Open;
  if AttachmentData.Eof then
    raise Exception.CreateFmt('Attachment %d not found to save changes', [AttachmentID]);
end;

procedure TServerAttachment.ValidateFileOnServer(const FileName, OrigFileName, FileHash: string; const FileSize, ForeignType, ForeignID: Integer);
var
  FileComparison: TFileComparison;
begin
  if FFileCheckMode <> [] then begin
    // Need a connection to get the attached file's size or hash from the FTP server
    if not Assigned(FTP) then
      InitializeFTPConnection;
    Assert(Assigned(FTP), 'No FTP Connection assigned in ValidateFileOnServer');
  end;

  if fcmCheckFileSize in FFileCheckMode then begin
    FTP.ChangeDir(FRootDir);
    FileComparison := CheckAttachmentFileSizeOnFTPServer(FTP, FileName, FileSize, ForeignType, ForeignID);
    if FileComparison in [fcFileOnServerIsBigger, fcFileOnServerIsSmaller, fcNoFileOnServer] then begin
      case FileComparison of
        fcFileOnServerIsBigger:
          LogProgress('Error: File on FTP Server is bigger than the original attachment file (' + OrigFileName + ')', 'SaveAttachment');
        fcFileOnServerIsSmaller:
          LogProgress('Error: File on FTP Server is smaller than the original attachment file (' + OrigFileName + ')', 'SaveAttachment');
        fcNoFileOnServer:
          LogProgress('Error: File on FTP Server does not exist (' + OrigFileName + ')', 'SaveAttachment');
      end;
    end;
  end;

  if fcmCheckFileHash in FFileCheckMode then begin
    if NotEmpty(FileHash) then begin
      FTP.ChangeDir(FRootDir);
      if not CheckAttachmentFileHash(FTP, FileName, FileHash, ForeignType, ForeignID) then
        raise Exception.CreateFmt('The uploaded attachment file %s appears to be corrupt. Please retry your upload.', [OrigFileName]);
    end;
  end;
end;

procedure TServerAttachment.LogProgress(const Msg, Phase: string);
begin
  Log(Msg + ' in ' + Phase);
end;

// DownloadAttachments only returns the error messages in ErrorLog if any
// downloads fail.  The ErrorLog string at Index I contains the error message
// for FileList at index I, if any error exists. If the download is successful
// but the file fails the hash check, an EOdCorruptFile exception is raised.
procedure TServerAttachment.DownloadAttachments(ForeignId: Integer; ForeignType: Integer;
  FileList: TStrings; ErrorLog: TStrings; const AttachmentIDList, Destination: string; UseOrigFilename: Boolean);
const
  SelAttachments = 'select * from attachment where (foreign_id = %d) and (active = 1) and (upload_date is not null) and (foreign_type = %d) %s';
var
  Dataset: TADODataSet;
  FileExt: string;
  FTPFileName: string;
  LocalFileName: string;
  AttachmentID: Integer;
  ErrorMessage: string;
  AttachmentIDCriteria: string;
begin
  Assert(Assigned(FileList));
  Assert(DirectoryExists(Destination));

  Dataset := TADODataSet.Create(nil);
  try
    Dataset.Connection := AttachmentData.Connection;
    Dataset.CommandType := cmdText;

    // list of attachment_id values for a filter, or blank for all
    if not ODMiscUtils.IsEmpty(AttachmentIDList) then begin
      if not ValidateIntegerCommaSeparatedList(AttachmentIDList) then
        raise Exception.Create('The Attachment ID list ' + AttachmentIDList + ' is not valid.');
      AttachmentIDCriteria := 'and attachment_id in (' + AttachmentIDList + ')';
    end;

    Dataset.CommandText := Format(SelAttachments, [ForeignId, ForeignType, AttachmentIDCriteria]);
    Dataset.Open;
    while not Dataset.EOF do begin
      AttachmentID := Dataset.FieldByName('attachment_id').AsInteger;
      FTPFileName := Dataset.FieldByName('filename').AsString;
      FileExt := Dataset.FieldByName('extension').AsString;
      if UseOrigFilename then
        LocalFileName := Dataset.FieldByName('orig_filename').AsString
      else
        LocalFileName := Dataset.FieldByName('filename').AsString;
      LocalFileName := AddSlash(Destination) + ChangeFileExt(LocalFileName, FileExt);
      FileList.Add(LocalFileName);
      ErrorMessage := DownloadAttachmentToFile(AttachmentID, LocalFileName);
      if Assigned(ErrorLog) then
        ErrorLog.Add(ErrorMessage);
      Dataset.Next;
    end;
  finally
    FreeAndNil(Dataset);
  end;
end;

procedure TServerAttachment.DownloadAttachmentsForAttachmentIDs(
  const AttachmentIDList, DestinationFolder: string;
  const UseOrigFilename: Boolean);
const
  SelAttachment = 'select * from attachment where attachment_id in (%s)';
var
  Dataset: TADODataSet;
  UploadDateDir: string;
  DestDir: string;
  AttachmentID: Integer;
  FileName: string;
begin
  Assert(NotEmpty(AttachmentIDList), 'AttachmentIDList cannot be empty');
  if not ValidateIntegerCommaSeparatedList(AttachmentIDList) then
    raise Exception.Create('The Attachment ID list is not valid: ' + AttachmentIDList);
  if not Assigned(FTP) then
    InitializeFTPConnection;
  Assert(Assigned(FTP), 'No FTP Connection assigned in DownloadAttachmentsForAttachmentIDs');

  Dataset := TADODataSet.Create(nil);
  try
    Dataset.Connection := AttachmentData.Connection;
    Dataset.CommandType := cmdText;

    Dataset.CommandText := Format(SelAttachment, [AttachmentIDList]);
    Dataset.Open;

    //Iterate dataset to get each attachment's info like filename
    while not Dataset.EOF do begin
      DestDir := IncludeTrailingPathDelimiter(DestinationFolder);
      UploadDateDir := FormatDateTime('yyyy-mm-dd', DataSet.FieldByName('upload_date').AsDateTime);
      if not IsEmpty(UploadDateDir) then
        DestDir := IncludeTrailingPathDelimiter(DestDir + UploadDateDir);
      if not DirectoryExists(DestDir) then
        OdForceDirectories(DestDir);
      if not (CanWriteToDirectory(DestDir)) then
        raise Exception.CreateFmt('Unable to create downloaded files in directory %s as user %s',
          [DestDir, GetLocalUserName]);

      AttachmentID := Dataset.FieldByName('attachment_id').AsInteger;
      if UseOrigFilename then
        FileName := DestDir + DataSet.FieldByName('orig_filename').AsString
      else
        FileName := DestDir + DataSet.FieldByName('filename').AsString;

      try
        DownloadAttachmentToFile(AttachmentID, FileName, FTP);
      except
        on E: EOdCorruptFile do
          if Assigned(FOnCorruptAttachment) then begin
            FOnCorruptAttachment(Self, AttachmentID, Filename);
            LogReportMessage(E.Message, True);
          end
          else
            raise;
      end;
      Dataset.Next;
    end;

  finally
    FreeAndNil(Dataset);
    FinalizeFTPConnection;
  end;
end;

function TServerAttachment.DownloadAttachment(const AttachmentID: Integer): string;
var
  DestDir: string;
  ForeignType: Integer;
  ForeignID: Integer;
  FileSize: Integer;
  ServerFileName: string;
  OriginalFileName: string;
  FileHash: string;
begin
  GetAttachmentDataFromAttachmentID(AttachmentID, ForeignType, ForeignID,
    FileSize, ServerFileName, OriginalFileName, FileHash);
  DestDir := GetAttachmentDownloadFolder;
  DestDir := DestDir + IncludeTrailingPathDelimiter(GetForeignTypeDirFromForeignType(ForeignType)) + IntToStr(ForeignID);
  if not DirectoryExists(DestDir) then
    OdForceDirectories(DestDir);
  Result := IncludeTrailingPathDelimiter(DestDir) + OriginalFileName;
  Result := DownloadAttachmentToFile(AttachmentID, Result);
end;

// FileType is the reference modifier field defining the subset, or 'Image' in this case.
function TServerAttachment.GetFileExtensionsForPrintableFileType(const FileType: string): string;
const
  SQL = 'select code from reference where type = ''%s'' and modifier = %s and active_ind = 1';
var
  DataSet: TADOdataset;
begin
  DataSet := TADOdataset.Create(nil);
  try
    DataSet.Connection := AttachmentData.Connection;
    DataSet.CommandType := cmdText;
    DataSet.CommandText := Format(SQL, [ReferenceTypePrintableFile, QuotedStr(FileType)]);
    DataSet.Open;
    while not DataSet.EOF do begin
      Result := Result + DataSet.FieldByName('code').AsString + ' ';
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TServerAttachment.GetAttachmentDownloadFolder: string;
begin
  Result := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(GetAppdataFolder) +
    ApplicationName + ' Downloads');
end;

function TServerAttachment.GetAttachmentFolder: string;
begin
  Result := PathAppend(ExtractFileDir(ParamStr(0)), 'Attachments');
  if not DirectoryExists(Result) then
    if not CreateDir(Result) then
      raise Exception.Create('Unable to create directory: ' + Result);
end;

end.
