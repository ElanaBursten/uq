unit FaxDispositionDMu;

interface

uses
  SysUtils, Classes, ComObj, OperationTracking, DB, ADODB;

type
  TeFaxDisposition = class
    UserName: string;
    Password: string;
    TransmissionID: Integer;
    DOCID: string;
    FaxNumber: string;
    CompletionDate: TDateTime;
    FaxStatus: Integer;
    RecipientCSID: string;
    Duration: string;
    PagesSent: Integer;
    NumberOfRetries: Integer;
    DispositionXML: string;
  end;

  TFaxDispositionDM = class(TDataModule)
    Conn: TADOConnection;
    SetSuccess: TADOCommand;
    SetFailure: TADOCommand;
    LogResp: TADOStoredProc;
    GetSystemEmpID: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    procedure ParseeFaxDisposition(DispositionXML: WideString;
      eFaxDisp: TeFaxDisposition);
    procedure SaveDispositionDataToDB;
    procedure LogAsResponse(FmId: Integer; Success: Integer);
    procedure SaveSuccess;
    procedure SaveFailure;   
    function GetSystemUserEmpID: Integer;
  public
    Op: TOpInProgress;
    DefaultIsolation: TIsolationLevel;
    eFaxDisposition : TeFaxDisposition;
    CacheConnectionString: string;
    procedure Connect;
    procedure Disconnect;
    function CreateDataSetForSQL(const SelectSQL: string): TDataSet;
    function GeteFaxErrorDescByCode(FaxStatusCode: Integer): String;
    procedure ProcesseFaxDisposition(DispositionXML: WideString);
  end;

var
  FaxDMIniName: string;

const
  MAX_DISPOSITION_LEN: Integer = 500;

implementation

uses OdAdoUtils;

{$R *.dfm}

procedure TFaxDispositionDM.DataModuleCreate(Sender: TObject);
begin
  DefaultIsolation := ilReadCommitted;

  Op := GetOperationTracker.AddOperation;
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
end;

procedure TFaxDispositionDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.FinishOperation(Op);
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;

procedure TFaxDispositionDM.ProcesseFaxDisposition(DispositionXML: WideString);
begin                 
  Op.OpName := 'ProcessFaxDisposition';
  Connect;                    
  Op.Phase := 'Processing eFax disposition.';
  Op.EmpID := GetSystemUserEmpID;
  eFaxDisposition := TeFaxDisposition.Create;
  try
    ParseeFaxDisposition(DispositionXML, eFaxDisposition);
    SaveDispositionDataToDB;
  finally
    eFaxDisposition.Free;
  end;
end;

procedure TFaxDispositionDM.ParseeFaxDisposition(DispositionXML: WideString; eFaxDisp : TeFaxDisposition);
//Per eFax API spec, the disposition will always be in XML format. eFax Developer
//will retry posting the disposition to us up to 4 times total.
var
  AOutboundClient : Variant;
  AOutboundDisposition : Variant;
begin
  Op.Phase := 'Parsing the eFax disposition.';
  try
    //Must use late binding until we upgrade to D2007: http://www.drbob42.com/examines/examin36.htm
    //Use J2.eFaxDeveloper.Outbound.OutboundClient to parse the xml
    AOutboundClient := CreateOleObject('J2.eFaxDeveloper.Outbound.OutboundClient');

    AOutboundDisposition := AOutboundClient.DeserializeOutboundDisposition_2(DispositionXML);

    eFaxDisp.UserName := AOutboundDisposition.UserName;
    eFaxDisp.Password := AOutboundDisposition.Password;
    eFaxDisp.TransmissionID := StrToInt(AOutboundDisposition.TransmissionId);
    eFaxDisp.DOCID := AOutboundDisposition.docId;
    eFaxDisp.FaxNumber := AOutboundDisposition.FaxNumber;
    eFaxDisp.CompletionDate := AOutboundDisposition.CompletionDate;
    eFaxDisp.FaxStatus := AOutboundDisposition.FaxStatus;
    eFaxDisp.RecipientCSID := AOutboundDisposition.RecipientCSID;
    eFaxDisp.Duration := FloatToStr(AOutboundDisposition.Duration);
    eFaxDisp.PagesSent := AOutboundDisposition.PagesSent;
    eFaxDisp.NumberOfRetries := AOutboundDisposition.NumberOfRetries;
    eFaxDisp.DispositionXML := DispositionXML;

    if AOutboundDisposition.FaxStatus = 0 then //success
      Op.LogFmt('eFax transmission completed. The disposition xml is: %s', [DispositionXML])
    else
      Op.LogFmt('eFax transmission failed. The disposition xml is: %s', [DispositionXML]);

  except
    on E:Exception do begin
      Op.LogFmt('Error parsing eFax dispositions. The exception is: %s '
        +' The disposition xml received is: %s', [E.Message, DispositionXML]);
    end;
  end;
end;

procedure TFaxDispositionDM.Connect;
begin
  Op.Phase := 'Connecting to DB';
  try
    if CacheConnectionString = '' then begin
      ConnectADOConnectionWithIni(Conn, FaxDMIniName);
      CacheConnectionString := Conn.ConnectionString;
    end else begin
      Assert(not Conn.Connected, 'The TFaxDispositionDM.Conn database is connected');
      Conn.ConnectionString := CacheConnectionString;
      Conn.Open;
    end;
  except
    on E: Exception do begin
      try
        //Log the real error
        Op.Log(E.Message);
      except
        // There is nothing we can do, if we get an error while
        // trying to log the error.
      end;
      // Then raise a friendlier one
      raise Exception.Create('The Q Manager system encountered an error connecting to the database.  Please try again in a few minutes.');
    end;
  end;
end;

procedure TFaxDispositionDM.Disconnect;
begin
  Conn.Connected := False;
end;

procedure TFaxDispositionDM.SaveDispositionDataToDB;
begin
  Op.Phase := 'Saving eFax disposition information to database.';
  Assert(Assigned(eFaxDisposition));

  Op.Phase := 'Setting up connection';
  try  // except handle error by logging it
    try
      Conn.IsolationLevel := DefaultIsolation;
      Conn.BeginTrans;
      try // commit or roll back
        Op.Phase := 'Processing';

        if eFaxDisposition.FaxStatus = 0 then
          SaveSuccess
        else
          SaveFailure;

        Conn.CommitTrans;
        Op.Succeeded := True;
        Op.Log(Op.Detail);  // successful
      except
        Op.Log('Rolling back');
        try
          Conn.RollbackTrans;
        except
          on RE: Exception do
            Op.Log('Exception occured while rolling back: ' + RE.Message);
        end;
        raise;
      end;
    finally
      Disconnect;
    end;
  except
    on E: Exception do begin
      E.Message := Format('QM Server Error: %s (%s: %s: %s)', [E.Message, Op.OpName, Op.Phase, Op.Detail]);
      try
        Op.Log(E.Message);
      except
        // There is nothing we can do, if we get an error while
        // trying to log the error.
      end;
      raise;
    end;
  end;

end;

function TFaxDispositionDM.CreateDataSetForSQL(const SelectSQL: string): TDataSet;
begin
  // You must free this dataset when you are done with it
  Result := TADODataSet.Create(nil);
  try
    (Result as TADODataSet).Connection := Conn;
    (Result as TADODataSet).CommandText := SelectSQL;
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TFaxDispositionDM.GeteFaxErrorDescByCode(FaxStatusCode: Integer): String;
const
  SQL = 'Select description from eFax_error_codes where code = %d';
var
  DataSet: TDataSet;
begin
  Result := '';
  DataSet := CreateDataSetForSQL(Format(SQL, [FaxStatusCode]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('description').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TFaxDispositionDM.LogAsResponse(FmId: Integer; Success: Integer);
begin
  LogResp.Parameters.ParamValues['@FmId'] := FmId;
  LogResp.Parameters.ParamValues['@StatusDate'] := eFaxDisposition.CompletionDate;
  LogResp.Parameters.ParamValues['@Success'] := Success;
  LogResp.ExecProc;
end;

procedure TFaxDispositionDM.SaveSuccess;
begin
  SetSuccess.Parameters.ParamValues['id'] := eFaxDisposition.TransmissionID;
  SetSuccess.Parameters.ParamValues['date'] := eFaxDisposition.CompletionDate;
  SetSuccess.Parameters.ParamValues['ext_disposition'] := Copy(eFaxDisposition.DispositionXML, 1, MAX_DISPOSITION_LEN);
  SetSuccess.Execute;
  LogAsResponse(eFaxDisposition.TransmissionID, 1);
end;

procedure TFaxDispositionDM.SaveFailure;
begin
  SetFailure.Parameters.ParamValues['id'] := eFaxDisposition.TransmissionID;
  //details is the error code mapped to corresponding descriptions provided by eFax
  SetFailure.Parameters.ParamValues['details'] := IntToStr(eFaxDisposition.FaxStatus)
    + ': ' + GeteFaxErrorDescByCode(eFaxDisposition.FaxStatus);
  SetFailure.Parameters.ParamValues['date'] := eFaxDisposition.CompletionDate;
  SetFailure.Parameters.ParamValues['ext_disposition'] := Copy(eFaxDisposition.DispositionXML, 1, MAX_DISPOSITION_LEN);
  SetFailure.Execute;
  LogAsResponse(eFaxDisposition.TransmissionID, 0);
end;  

function TFaxDispositionDM.GetSystemUserEmpID: Integer;
begin
  Result := 0;
  GetSystemEmpID.Open;
  try
    if GetSystemEmpID.Fields[0].IsNull then
      raise Exception.Create('Cannot get system user employee id. ' +
        'Missing the SystemUserEmpID configuration_data row.');
    Result := GetSystemEmpID.Fields[0].AsInteger;
  finally
    GetSystemEmpID.Close;
  end;
end;




end.
