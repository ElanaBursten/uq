object StandaloneServerForm: TStandaloneServerForm
  Left = 0
  Top = 0
  Caption = 'Standalone QM Server - SOAP in progress 1'
  ClientHeight = 190
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 133
    Height = 13
    Caption = 'http://localhost:8099/SOAP'
  end
  object ROSOAPMessage1: TROSOAPMessage
    Envelopes = <>
    ServerTargetNamespace = 'http://qmanager.utiliquest.com/qmserver2009'
    SerializationOptions = [xsoSendUntyped, xsoStrictStructureFieldOrder, xsoDocument, xsoSplitServiceWsdls]
    Left = 72
    Top = 32
  end
  object ROIndyHTTPServer1: TROIndyHTTPServer
    Dispatchers = <
      item
        Name = 'ROSOAPMessage1'
        Message = ROSOAPMessage1
        Enabled = True
        PathInfo = 'SOAP'
      end>
    IndyServer.Bindings = <>
    IndyServer.DefaultPort = 8099
    Port = 8099
    Left = 120
    Top = 32
  end
  object ROBinMessage1: TROBinMessage
    Envelopes = <>
    Left = 72
    Top = 72
  end
end
