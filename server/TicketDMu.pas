unit TicketDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Types,
  Dialogs, BaseLogicFeatureDMu, DB, ADODB, QMServerLibrary_Intf,
  ArrivalDMu, AddinInfoDMu, NotesDMu, RemObjectsUtils, ADODB_TLB;

const
  FEATURE_NAME = 'Ticket';

type
//  THPLocRecords = record
//    HPDescriptions: TStringList;
//    HPSubjectLines: TStringList;
//  end;
  TTicketDM = class(TLogicFeatureDM)
    GPSPosition: TADODataSet;
    InsertGPS: TADOCommand;
    UpdateAssignments: TADOStoredProc;
    BillingQueue: TADOCommand;
    InsertNotificationQueuesSP: TADOStoredProc;
    InsertResponderMultiQueue: TADOCommand;
    EditHours: TADOCommand;
    MakeFollowupTicket: TADOStoredProc;
    LocateHours: TADODataSet;
    LocatePlatData: TADODataSet;
    TicketInfoData: TADODataSet;
    LocateFacilityData: TADODataSet;
    NextTicketID: TADOStoredProc;
    TaskSchedule: TADODataSet;
    NewTicket: TADODataSet;
    InsertTicketAck: TADOStoredProc;
    AddClientOld: TADOCommand;
    ClientData: TADODataSet;
    ADOStoredProc1: TADOStoredProc;
    getHPRanking: TADOQuery;
    GetHPMultiCodes: TADOQuery;
    Ticket: TADODataSet;
    Locate: TADODataSet;
    FindLocateQuery: TADODataSet;
    LocatorOnTicket: TADODataSet;
    HighProfileEmailList: TADODataSet;
    StatusList: TADODataSet;
    FindClientCode: TADODataSet;
    TicketImageData: TADODataSet;
    getUtilityType: TADODataSet;
    EPRHistorybyTicket: TADODataSet;
    GetTicketIDFromLocate: TADODataSet;
    LocatesForTicket: TADODataSet;
    RiskScoreHistoryByTicket: TADODataSet;
  private
    ArrivalDM: TArrivalDM;
    AddinInfoDM: TAddinInfoDM;
    NotesDM: TNotesDM;

    function GetArrivalDM: TArrivalDM;
    function GetAddinInfoDM: TAddinInfoDM;
    function GetNotesDM: TNotesDM;
    function AssignNewLocateTo(const TicketID: Integer): Integer;
    function GetMultiHighProfileReasonDesc(const TicketID: Integer; const ClientCode: string): string;
    function GetClientHighProfileEmailList(const ClientID: Integer): string;
    function ValidStatusForClient(const ClientID: Integer; const Status: string): Boolean;
    function ValidStatusForTicket(const TicketID: Integer; Status: string): Boolean;
    function ValidStatusForEmployee(const LoggedInEmpID: Integer; const Status: string): Boolean;
    procedure OpenTicketTo(TicketID: Integer);
    function InsertGPSPostion(const AddedBy: Integer; AddedDate: TDateTime; Latitude, Longitude: Double; HDOP: Integer): Integer;
    function GPSPositionExists(AddedBy: Integer; AddedDate: TDateTime; Lat, Lon: Double; HDOP: Integer; var ExistingGPSID: Integer): Boolean;
    function GetClientIDForCode(const ClientCode, CallCenter: string): Integer;
    function IsMeetTicket(const TicketID: Integer): Boolean;
    function IsTicketAssigned(const TicketID, EmpID: Integer): Boolean;
    function FindLocateID(const TicketID: Integer; const ClientCode: string): Integer;
    function InsertBillingQueue(EventType: string; TicketID: Integer;
      LocateID: Variant; EventDate: TDateTime; LocateHoursID,
      LocateStatusID: Variant): Integer;
    procedure InsertNotificationQueues(LocateID, LocateStatusID: Integer;
      const Status, CallCenter, State, ClientCode: string);
    function IsReleasePriority(const PriorityID: Integer): Boolean;
    function NextNewTicketNumberID: Integer;
    procedure SetTicketPriority(const TicketID, PriorityID: Integer);

    function GetPriorityNameFromTicket(const TicketID: Integer): string;
    function CallCenterCondition(Centers: string): string;
    function GetLocateMultiResponse(const CallCenter, ClientCode: string): Boolean;
    function GetClientUtilType(const ClientCode, CallCenter: string): String;
//    function InsertTicketNoteDirect(TicketID: Integer; EntryDate: TDateTime;  EnteredByUID: Integer; NoteText: string; SubType: Integer;   ClientCode: string): Integer; //QM-300 sr
  public
    destructor Destroy; override;
    function InsertTicketNote(TicketID: Integer; EntryDate: TDateTime; EnteredByUID: Integer; NoteText: string; SubType: Integer=0; ClientCode: string=''): Integer;  //qm-747  sr
    function IsManualTicket(TicketID: Integer): Boolean;
    procedure StatusTicketsLocates14Internal(const TicketChanges: TicketChangeList14; out GeneratedKeys: GeneratedKeyList);
    procedure StatusTicketsLocates15Internal(const TicketChanges: TicketChangeList15; out GeneratedKeys: GeneratedKeyList);
    procedure ProcessTicketAreaChanges(ChangeList: TicketAreaChangeList);
    procedure ProcessLocatePlatChanges(ChangeList: LocatePlatChangeList; var NewKeys: GeneratedKeyList);
    procedure ProcessTicketInfoChanges(ChangeList: TicketInfoChangeList; var NewKeys: GeneratedKeyList);
    procedure SaveOtherTicketInfoChanges(ChangeList: TicketInfoChangeList; var NewKeys: GeneratedKeyList);
    procedure ProcessLocateFacilityChanges(ChangeList: LocateFacilityChangeList; var NewKeys: GeneratedKeyList); overload;
    procedure ProcessLocateFacilityChanges(ChangeList: LocateFacilityChangeList2; var NewKeys: GeneratedKeyList); overload;
    procedure ProcessTaskSchedule(ChangeList: TaskScheduleChangeList; var NewKeys: GeneratedKeyList);
    procedure CreateFollowupTicket(const TicketID, FollowupTypeID: Integer; const FollowupDueDate, FollowupTransmitDate: TDateTime);
    procedure MoveLocates(const Locates: LocateList; const NewlocatorID: Integer);
    procedure AssignProjLocate(LocateID, AssignedToID: integer);
    procedure MoveTickets(const Tickets: TicketList; const FromLocatorID, ToLocatorID: Integer);
    procedure SaveTicketPriority(const TicketID: Integer; const EntryDate: TDateTime; const Priority: Integer);
    function SaveNewTickets(const Tickets: NewTicketList): GeneratedKeyList;
    function GetTicketImage(const TicketID: Integer): string;
    function GetTicket2(const TicketID: Integer; UpdatesSince: TDateTime): string;
    function GetTicketHistory(const TicketID: Integer): string;
    function TicketSearch(const TicketNumber, Street, City, State, County,
      WorkType, TicketType, DoneFor, Company: string; const DueDateFrom,
      DueDateTo, RecvDateFrom, RecvDateTo: TDateTime; const CallCenter,
      TermCode: string; const Attachments, ManualTickets: Integer;
      const Priority: string; const UtilityCo:integer): string;  //QMANTWO-810
    function TicketIDSearch(const TicketID: integer): string;   //QM-305 EB Ticket ID Search
    function LocateIDSearch(const LocateID: integer): string;   //QM-305 EB Ticket ID Search

    function CompletionReport(const DateFrom, DateTo: TDateTime; const Office,
      Manager, SortBy: Integer): string;
    function GetEPRHistory(TicketID: integer): string;
    function GetRiskHistory(TicketID: integer): string;  //QM-387 EB
    function GetTicketLocateList(TicketID: integer): string;
    procedure SaveTicketTypeHP(TicketID: integer; NewTicketType: string);   //qm-373 SR
    function NoteHasChanged2(NoteText: string): boolean;
    procedure SaveTicketType(TicketID: integer; TicketType: string); //QM-604 EB Nipsco
    function GetTicketAlertsForTicket(TicketID: integer): string; //QM-771 EB

    {Update Due Date Calls}
    procedure SaveTicketDueDate(TicketID: integer; NewDueDate: TDateTime); //QM-216 EB Single Due Date
    function BulkChangeTicketDueDates(TicketIDs: string; NewDueDate: TDateTime): string;  //QM-959 EB Bulk Due Date Change

  end;
  function DataSetToXML(DataSet  : TDataSet): string;  //QMANTWO-307



  implementation

uses DateUtils, QMConst, ServerAttachmentDMu, OdMiscUtils, OdDbUtils, OdIsoDates,
  OdExceptions, OdSqlBuilder, OdDatasetToXML, QMTempFolder, TicketImage, LogicDMu,
  JclStrings;

{$R *.dfm}

function TTicketDM.AssignNewLocateTo(const TicketID: Integer): Integer;
begin
  if LogicDM.CanI(RightTicketsAssignNewLocatesToTicketLocator) then begin
    LocatorOnTicket.Parameters.ParamValues['ticket_id'] := TicketID;
    LocatorOnTicket.Open;
    Result := LocatorOnTicket.Fields[0].AsInteger;
    LocatorOnTicket.Close;
  end else
    Result := LogicDM.LoggedInEmpID;
end;

function TTicketDM.GetMultiHighProfileReasonDesc(const TicketID: Integer; const ClientCode: string): string;
const
  MultiHPReasonNotes = 'select note from notes ' +
    'where foreign_id = %d and foreign_type = 1 and note like ''HPR-%%''';
var
  ClientCodePos: Integer;
  Reasons: string;
  Notes: TDataset;
  HPReasons: TDataset;

  function TranslateHPReasons(const ReasonList: string): string;
  var
    Codes: TStringList;
    I: Integer;
  begin
    Result := '';
    Codes := TStringList.Create;
    try
      Codes.Delimiter := ',';
      Codes.DelimitedText := ReasonList;
      for I := 0 to Codes.Count-1 do begin
        if HPReasons.Locate('code', Codes.Strings[I], []) then
          Result := Result + HPReasons.FieldByName('description').AsString + ','
        else
          Result := Result + Codes.Strings[I] + ',';
      end;
      if Length(Result) > 0 then
        Delete(Result, Length(Result), 1);
    finally
      FreeAndNil(Codes);
    end;
  end;

begin
  HPReasons := nil;
  Notes := LogicDM.CreateDataSetForSQL(Format(MultiHPReasonNotes, [TicketID]));
  try
    HPReasons := LogicDM.CreateDataSetForSQL('select code, description from reference where type=''HPMULTI''');
    Result := '';
    Notes.First;
    while not Notes.Eof do begin
      ClientCodePos := Pos(' ' + ClientCode, Notes.FieldByName('note').AsString);
      if ClientCodePos > 0 then begin
        // the part between the HPR- prefix & the client code suffix is the list of reasons
        Reasons := Copy(Notes.FieldByName('note').AsString, 5, ClientCodePos - 4);
        Result := TranslateHPReasons(Reasons);
        Break;
      end;
      Notes.Next;
    end;
  finally
    FreeAndNil(HPReasons);
    FreeAndNil(Notes);
  end;
end;

function TTicketDM.GetArrivalDM: TArrivalDM;
begin
  if ArrivalDM = nil then
    ArrivalDM := TArrivalDM.Create(nil, LogicDM);

  Result := ArrivalDM;
end;

function TTicketDM.GetAddinInfoDM: TAddinInfoDM;
begin
  if AddinInfoDM = nil then
    AddinInfoDM := TAddinInfoDM.Create(nil, LogicDM);

  Result := AddinInfoDM;
end;

function TTicketDM.GetNotesDM: TNotesDM;
begin
  if NotesDM = nil then begin
    NotesDM := TNotesDM.Create(nil, LogicDM);
  end;

  Result := NotesDM;
end;

function TTicketDM.GetClientHighProfileEmailList(const ClientID: Integer): string;
begin
  //HighProfileEmailList.ParamByName['client_id'].Value := ClientID;
  HighProfileEmailList.Parameters.ParamValues['client_id'] := ClientID;    //QM-977 BetterADO  EB
  HighProfileEmailList.Open;
  repeat
    Result := Result + VarToStr(HighProfileEmailList.FieldValues['email_address']) + ';';
    HighProfileEmailList.Next;
  until HighProfileEmailList.Eof;
  Delete(Result, Length(Result), 1);
  HighProfileEmailList.Close;
end;

{For Older clients (pre-subtype)}
procedure TTicketDM.StatusTicketsLocates14Internal(const TicketChanges: TicketChangeList14;
  out GeneratedKeys: GeneratedKeyList);
var
  Attacher: TServerAttachment;

  procedure RefreshLocateDataSet(TicketID: Integer);
  begin
    Locate.Close;
    Locate.Parameters.ParamValues['id'] := TicketID;
    Locate.Open;
  end;

  function TicketToText(Ticket, Locates: TDataset; TermId: string; ClientID: Integer; TicketImage: string): string;
  const
    LocateSQL =
      'select locate.locate_id, statuslist.status_name, employee.short_name, ' +
      '  dbo.get_plat_list(locate.locate_id) as plat_list, ' +
      '  reference.description as hp_reason_desc ' +
      'from locate ' +
      '  left join statuslist on statuslist.status = locate.status ' +
      '  left join assignment on assignment.locate_id = locate.locate_id ' +
      '    and assignment.active = 1 ' +
      '  left join employee on employee.emp_id = assignment.locator_id ' +
      '  left join reference on reference.ref_id = locate.high_profile_reason ' +
      'where ticket_id = %s';
    NotesSQL =
      'select notes.entry_date, notes.modified_date, notes.note ' + //Can't use the notes.sub_type ' +
      'from ticket_notes notes ' +
      'where notes.ticket_id = %s ';
  var
    TicketAsText: TStringList;
    LocatesAdditional: TDataset;
    Notes: TDataset;
    HighProfileReason: string;
  begin
    TicketAsText := TStringList.Create;
    try
      with TicketAsText do begin
        Add(Format('Ticket Number: %s', [Ticket.FieldByName('ticket_number').AsString]));
        Add(Format('Received date: %s', [Ticket.FieldByName('transmit_date').AsString]));
        Add(Format('Due date: %s', [Ticket.FieldByName('due_date').AsString]));
        Add(Format('Client Term: %s', [TermId]));
        Add('');
        Add('Ticket Image:');
        Add('------------');
        Add(TicketImage);
        Add('');
        Locates.First;
        LocatesAdditional := LogicDM.CreateDataSetForSQL(Format(LocateSQL,[Ticket.FieldByName('ticket_id').AsString]));
        while not Locates.EOF do begin
          if (Locates.FieldByName('client_id').AsInteger <> ClientID) then begin
            Locates.Next;
            Continue;
          end;
          LocatesAdditional.Locate('locate_id',Locates.FieldByName('locate_id').AsInteger,[]);
          HighProfileReason := LocatesAdditional.FieldByName('hp_reason_desc').AsString;
          if Locates.FieldByName('high_profile').AsBoolean and (HighProfileReason = '') then
            HighProfileReason := GetMultiHighProfileReasonDesc(Locates.FieldByName('ticket_id').AsInteger,
              Locates.FieldByName('client_code').AsString);

          with Locates do begin
            Add('-------------');
            Add('Client: '+FieldByName('client_code').AsString);
            Add('Status: '+LocatesAdditional.FieldByName('status_name').AsString);
            Add('When Closed: '+FieldByName('closed_date').AsString);
            Add('High Profile: '+FieldByName('high_profile').AsString);
            Add('High Profile Reason: '+HighProfileReason);
            Add('Closed: '+FieldByName('closed').AsString);
            Add('Plat Number(s): '+LocatesAdditional.FieldByName('plat_list').AsString);
          end;
          Locates.Next;
        end;
        Notes := LogicDM.CreateDataSetForSQL(Format(NotesSQL, [Ticket.FieldByName('ticket_id').AsString]));
        Notes.First;
        while not Notes.EOF do begin
          Add('-------------');
          Add('Note Entered: ' + Notes.FieldByName('entry_date').AsString);
          Add(Notes.FieldByName('note').AsString);
    //      Add(Notes.FieldByName('sub_type').AsString);
          Notes.Next;
        end;
        FreeAndNil(LocatesAdditional);
        FreeAndNil(Notes);
        Result := Text;
      end; // with
    finally
      FreeAndNil(TicketAsText);
    end;
  end;

  procedure SendHighProfileEmails(TermId: string; TicketNumber: string; ClientID: Integer; TicketImage: string; TicketId: Integer);
  var
    Body, FromEMail: string;
    EmailList: string;
    FileList: TStringList;
    ErrorList: TStringList;
    TempDir: ITemporaryFolder;
  begin
    if LogicDM.GetIniBoolSetting('HighProfileEmail', 'Ignore', False) then
      Exit;
    FromEMail:='';    //QM-9 gets this from INI
    ErrorList := nil;
    FileList := TStringList.Create;
    try
      ErrorList := TStringList.Create;
      Body := TicketToText(Ticket, Locate, TermId, ClientID, TicketImage);
      EmailList := GetClientHighProfileEmailList(ClientID);
      if LogicDM.GetIniBoolSetting('HighProfileEmail', 'IncludeAttachments', True) then begin //qm-300 sr
        TempDir := NewTemporaryFolder;
        Attacher.DownloadAttachments(TicketId, qmftTicket, FileList, ErrorList,
          '', TempDir.FullPathName);
        if NotEmpty(ErrorList.Text) then
          Body := Body + sLineBreak + 'Attachment Errors:' + sLineBreak + Trim(ErrorList.Text);
      end;
      LogicDM.SendEmailWithIniSettingsExp('HighProfileEmail', FromEMail, EmailList, 'High Profile Locate', Body, FileList);
    finally
      FreeAndNil(FileList);
      FreeAndNil(ErrorList);
    end;
  end;

  procedure ValidateLocateStatus(const Status: string);
  var
    TicketID: Integer;
    LocateID: Integer;
    ClientID: Integer;
  begin
    TicketID := Locate.FieldByName('ticket_id').AsInteger;
    LocateID := Locate.FieldByName('locate_id').AsInteger;
    ClientID := Locate.FieldByName('client_id').AsInteger;
    try
      // For now, these are non-fatal (log-only) errors
      if not ValidStatusForClient(ClientID, Status) then
        raise Exception.CreateFmt('%s is not allowed for client %d', [Status, ClientID]);
      if not ValidStatusForTicket(TicketID, Status) then
        raise Exception.CreateFmt('%s is not allowed for non-meet ticket %d', [Status, TicketID]);
      if not ValidStatusForEmployee(LogicDM.LoggedInEmpID, Status) then
        raise Exception.CreateFmt('%s status is not allowed for employee %d', [Status, LogicDM.LoggedInEmpID]);
    except
      on E: Exception do
        Op.Log(Format('Warning - Invalid status for locate %d: %s', [LocateID, E.Message]));
    end;
  end;

var
  I, J: Integer;
  TicketID, LocateID: Integer;
  Tk: TicketChange14;
  Note: TicketNote2;
  Loc: LocateChange6;
  H: LocateHour3;
  GPS: GPSChange;
  OriginalFollowupID: Integer;
  ClientID: Integer;
  StatusID: Integer;
  IncomingHoursID: Integer;
  OutgoingHoursID: Integer;
  NewlyAddedLocate: Boolean;
  MarkedHighProfile: Boolean;
  NoteID: Integer;
  HighProfileLocates: TStringList;
  Dummy: string;
  OldKind: string;
  HighDate: TDateTime;
  EarliestTicketActivityDate: TDateTime;
  NewGPSID: Integer;
  GPSID: Variant;
  AssignedLocatorID: Integer;
  StatusClosedLocate: Boolean;
  StatusReopenedLocate: Boolean;
  KindBeforeStatusChange: string;
  TicketKindChanged: Boolean;
begin
  // Experimental feature:
  // this info is needed before the DM is set up / transaction is started
  // Repeatable Read has the effect of locking the affected rows, in SQL Server.
  // FDesiredIsolation := ilRepeatableRead;

  LogicDM.CheckApplicationVersion(False, Dummy);
  GeneratedKeys := GeneratedKeyList.Create;

  Attacher := nil;
  StatusList.Open;
  Assert(StatusList.RecordCount > 0, 'Statuses not defined');
  HighProfileLocates := TStringList.Create;
  try
    Attacher := TServerAttachment.Create(LogicDM.Conn, LogicDM.LoggedInEmpID);
    HighDate := EncodeDate(2199, 12, 31); // arbitrary high date
    for I := 0 to TicketChanges.Count-1 do begin
      SetPhase(Format('Ticket %d of %d', [I + 1, TicketChanges.Count]));
      Op.Detail := 'Loading tickets and locates';

      EarliestTicketActivityDate := HighDate;
      HighProfileLocates.Clear;
      Tk := TicketChanges[I];
      TicketID := Tk.TicketID.TicketID;  // less than ideal variable naming...
      OpenTicketTo(TicketID);
      if Ticket.Eof then
        raise Exception.CreateFmt('Ticket ID %d does not exist to edit', [TicketID]);

      OldKind := Ticket.FieldByName('kind').AsString;

      // ********** GPS Changes
      for J := 0 to Tk.GPSChanges.Count - 1 do begin
        SetPhase(Format('Ticket %d of %d, GPS %d of %d',
            [I + 1, TicketChanges.Count, J + 1, Tk.GPSChanges.Count]));
        Op.Detail := 'Finding GPS';
        GPS := TicketChanges[I].GPSChanges[J];
        if GPS.GPSID > 0 then
          raise Exception.Create('Updating of existing GPS positions is not supported, ID ' + IntToStr(GPS.GPSID));

        NewGPSID := InsertGPSPostion(GPS.AddedBy, GPS.AddedDate, GPS.Latitude, GPS.Longitude, GPS.HDOPFeet);
        Assert(NewGPSID > 0);
        AddIdentity(GeneratedKeys, 'gps_position', GPS.GPSID, NewGPSID);
      end;

      RefreshLocateDataSet(TicketID);
      // ********* Locate Changes
      for J := 0 to Tk.LocateChanges.Count-1 do begin
        SetPhase(Format('Ticket %d of %d, Locate %d of %d',
            [I + 1, TicketChanges.Count, J + 1, Tk.LocateChanges.Count]));
        Op.Detail := 'Finding Locate';
        Loc := TicketChanges[I].LocateChanges[J];

        LocateID := -1;
        NewlyAddedLocate := False;
        KindBeforeStatusChange := Ticket.FieldByName('kind').AsString;

        // Try to find the locate by ID, in case there are duplicates of the
        // client code on this ticket for any reason
        if (Loc.LocateID > 0) and Locate.Locate('locate_id', Loc.LocateID, []) then begin
          Locate.Edit;
          LocateID := Loc.LocateID;
          AssignedLocatorID := Locate.FieldByName('locator_id').AsInteger;
        end else begin
          // Get the ClientID here, so a little less happens between the refresh and Append/Post
          ClientID := GetClientIDForCode(Loc.ClientCode, Ticket.FieldByName('ticket_format').AsString);

          // Refresh in case this term was just added by another sync in the meantime
          RefreshLocateDataSet(TicketID);

          // Locate it by the Client Code.  If it's new (ID<0), nonetheless
          // find it by the client code, so we don't create a duplicate
          if Locate.Locate('client_code', Loc.ClientCode, []) then begin
            // Update an existing locate
            Locate.Edit;
            LocateID := Locate.FieldByName('locate_id').AsInteger;
            AssignedLocatorID := Locate.FieldByName('locator_id').AsInteger;
            NewlyAddedLocate := False;
          end else begin
            // Add a new locate
            Op.Detail := 'Adding New Locate';

            Locate.Append;
            Locate.FieldByName('ticket_id').AsInteger := TicketID;
            Locate.FieldByName('client_code').AsString := Loc.ClientCode;
            Locate.FieldByName('client_id').AsInteger := ClientID;
            Locate.FieldByName('active').AsBoolean := True;
            Locate.FieldByName('added_by').AsString := IntToStr(LogicDM.LoggedInEmpID);
            Locate.FieldByName('modified_date').AsDateTime := Now;
            AssignedLocatorID := AssignNewLocateTo(TicketID);
            NewlyAddedLocate := True;
          end;
        end;

        // Common handling for inserts and updates
        if Locate.FieldByName('status').AsString <> Loc.Status then
          ValidateLocateStatus(Loc.Status);

        Locate.FieldByName('status').AsString := Loc.Status;
        if StatusList.Locate('status', Loc.Status, []) then begin
          StatusClosedLocate := StatusList.FieldByName('complete').AsBoolean and (not Locate.FieldByName('closed').AsBoolean);
          StatusReopenedLocate := Locate.FieldByName('closed').AsBoolean and (not StatusList.FieldByName('complete').AsBoolean);
          Locate.FieldByName('closed').AsBoolean := StatusList.FieldByName('complete').AsBoolean;
        end else
          raise Exception.Create('Unknown Status: ' + Loc.Status);

        Locate.FieldByName('mark_type').AsString := Loc.MarkType;
        if Loc.StatusDate > 1 then
          Locate.FieldByName('closed_date').AsDateTime := RoundSQLServerTimeMS(Loc.StatusDate);

        if (Locate.FieldByName('closed').AsBoolean) and (Loc.StatusDate > 1) and
          (Loc.StatusDate < EarliestTicketActivityDate) then
          EarliestTicketActivityDate := Loc.StatusDate;

        if Loc.StatusAsAssignedLocator then begin
          Locate.FieldByName('closed_by_id').AsInteger := AssignedLocatorID;

          InsertTicketNote(TicketID, Loc.StatusDate, LogicDM.LoggedInUID,
             'Locate ' + Loc.ClientCode + NoteStatusedasAssigned +
             LogicDM.LoggedInEmpShortName + ' (Locator ID ' + IntToStr(LogicDM.LoggedInEmpID) + ')', PrivateTicketSubType);  //SubType of 100 is standard ticket note
        end else
          Locate.FieldByName('closed_by_id').AsInteger := LogicDM.LoggedInEmpID;

        //Keep track of who actually statused the locate in status_changed_by_id- currently used for reporting purposes only
        //This is different than closed_by_id, which is affected by the "Status As Assigned User" checkbox.
        Locate.FieldByName('status_changed_by_id').AsInteger := LogicDM.LoggedInEmpID;

        Locate.FieldByName('closed_how').AsString := Loc.StatusedHow;
        Locate.FieldByName('qty_marked').AsFloat := Loc.QtyMarked;

        // TODO: We don't handle a locate being unmarked as HP
        MarkedHighProfile := (Locate.FieldByName('high_profile').AsBoolean <> Loc.HighProfile) and Loc.HighProfile;

        Locate.FieldByName('high_profile').AsBoolean := Loc.HighProfile;

        if Loc.HighProfileReason > -1 then
          Locate.FieldByName('high_profile_reason').AsInteger := Loc.HighProfileReason
        else
          Locate.FieldByName('high_profile_reason').Clear;

        if Loc.EntryDate > 0 then
          Locate.FieldByName('entry_date').AsDateTime := Loc.EntryDate
        else
          Locate.FieldByName('entry_date').Clear;

        if (Loc.GPSID < 0) then begin
          if GetGeneratedIdentity(GeneratedKeys, 'gps_position', Loc.GPSID, GPSID) then
            Locate.FieldByName('gps_id').Value := GPSID
          else
            raise Exception.Create('Cannot find gps_id for newly added ticket GPS data');
        end else if (Loc.GPSID > 0) then
          Locate.FieldByName('gps_id').AsInteger := Loc.GPSID
        else
          Locate.FieldByName('gps_id').Clear;

        // If you have an update/insert trigger, add the code to the beginning:
        // SET NOCOUNT ON

        if StringInArray(Locate.FieldByName('status').AsString, InternalStatuses) then
          raise Exception.Create('Q Manager can not edit locates with a status of -P or -N');

        // This controls the WHERE clause it uses
        Locate.Properties['Update Criteria'].value := adCriteriaKey;
  

        // Only post and do the related changes if a locate field was changed
        if (NewlyAddedLocate or AnyFieldWasEdited(Locate, True)) then begin
          Op.Detail := 'Posting Locate';
          Locate.Post;
          // There is a locate table trigger that does this:
          // * Insert row in to locate_status
          // * insert row in to responder_queue
          // * update the ticket row to reflect DONE or not
          // All of that can be moved here, once every locator is using QMLogic

          // TODO: Find a better way to address getting this value (a query, etc.), since it is very unreliable
          StatusID := LogicDM.GetAnyIdentity;  // Make sure this comes after immediately after locate.post so
                                               // we get the locate_status_id (not the locate_id).

          // We can't fetch the ID until it's actually posted to the DB
          if NewlyAddedLocate then begin
            Op.Detail := 'NewlyAddedLocate UpdateAssignments';

            // We can't get it from here, because this gets populated with
            // the wrong ID, because of a bug with Delphi ADO and Triggers
            //LocateID := Locate.FieldByName('locate_id').AsInteger;

            // And we can't get it from the Scope Identity, because ADO already
            // grabbed @@IDENTITY and destroyed it
            // LocateID := GetScopeIdentity;

            Op.Detail := 'Getting back locate_id';
            LocateID := FindLocateID(TicketID, Loc.ClientCode);
            Assert(LocateID > 1000);  // Make sure the ID got populated back

            // Assign to me
            UpdateAssignments.Parameters.ParamValues['@LocateID'] := LocateID;
            UpdateAssignments.Parameters.ParamValues['@LocatorID'] := AssignedLocatorID;
            UpdateAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
            UpdateAssignments.ExecProc;

            MarkedHighProfile := Loc.HighProfile;
          end;

          Op.Detail := 'InsertBillingQueue';
          InsertBillingQueue(BillingEventLocate, TicketID, LocateID, Loc.StatusDate, Null, StatusID);

          Op.Detail := 'InsertNotificationQueues';
          Op.Log(Format('Trying to insert in notificaton queues: %s, %s, %s, %s, %s, %s',
            [IntToStr(LocateID), IntToStr(StatusID), Loc.Status,
            Ticket.FieldByName('ticket_format').AsString,
            Ticket.FieldByName('work_state').AsString, Loc.ClientCode]));
          InsertNotificationQueues(LocateID, StatusID, Loc.Status,
             Ticket.FieldByName('ticket_format').AsString,
             Ticket.FieldByName('work_state').AsString, Loc.ClientCode);

          Op.Log('Statused locate ' + IntToStr(LocateID) + ' to status ' + Loc.Status);

          // Track high profile locates so we can send e-mails later
          if MarkedHighProfile then
            HighProfileLocates.Add(IntToStr(LocateID));

          if GetLocateMultiResponse(Ticket.FieldByName('ticket_format').AsString,  //QMANTWO-168
            Loc.ClientCode) then begin
            Op.Detail := 'Inserting in Responder Multi Queue';
            with InsertResponderMultiQueue do begin
              Parameters.ParamValues['respond_to'] := 'client';
              Parameters.ParamValues['locate_id'] := LocateID;
              Execute;
            end;
          end;
        end
        else begin // No locate fields were changed, so don't save the locate record
          Op.Detail := 'Canceling unchanged locate';
          Locate.Cancel;
        end;

        // If the incoming ID is negative, then return an ID, even if its an
        // existing locate - that way if two locators insert a locate, it works.
        if Loc.LocateID < 0 then begin
          Op.Detail := 'Returning LocateID';
          if LocateID < 0 then
            raise Exception.Create('Error, LocateID < 0');
          AddIdentity(GeneratedKeys, 'locate', Loc.LocateID, LocateID);
        end;

        RefreshDataSet(Ticket); // get changes made by the locate_iu trigger
        Assert(TicketID = Ticket.FieldByName('ticket_id').Value, 'Unexpected ticket_id');

// TODO: The StoreEventsEnabled option needs to go away once we are reliably creating
// events so it can't accidentally be turned off and leave a hole in the event_log!
        if LogicDM.StoreEventsEnabled then begin
          Op.Detail := 'Updating Event Store';
          if StatusClosedLocate then
            TicketKindChanged := (KindBeforeStatusChange <> TicketKindDone)
              and (Ticket.FieldByName('kind').AsString = TicketKindDone)
          else if StatusReopenedLocate then
            TicketKindChanged := (KindBeforeStatusChange = TicketKindDone)
              and (Ticket.FieldByName('kind').AsString <> TicketKindDone);

          if StatusClosedLocate or StatusReopenedLocate then
            LogicDM.EventsDM.LogCompletionChangedEvent(TicketID,
              Locate.FieldByName('locate_id').AsInteger,
              Locate.FieldByName('closed_by_id').AsInteger,
              Locate.FieldByName('client_code').AsString,
              Ticket.FieldByName('ticket_format').AsString,
              Locate.FieldByName('status').AsString,
              Locate.FieldByName('closed_date').AsDateTime,
              Locate.FieldByName('closed').AsBoolean,
              TicketKindChanged,
              Ticket.FieldByName('ticket_type').AsString,
              Locate.FieldByName('workload_date').AsDateTime);
        end;
      end; // end of LocateChanges loop

      // ********* Notes

      Op.Detail := 'NewNotes';
      for J := 0 to Tk.NewNotes.Count - 1 do begin
        Note := Tk.NewNotes[J];

        if Note.NoteID > 0 then    {14}
           raise Exception.Create('Updating of existing notes is not supported, ID ' + IntToStr(Note.NoteID));

          {EB - OLDER CLIENTS USING THIS VERSION: For the subtype, we are just going to enter 0}
          NoteID := InsertTicketNote(TicketID, Note.EntryDate, Note.EnteredByUID, Note.NoteText, 0, Note.ClientCode );
          Assert(NoteID > 0);
          AddIdentity(GeneratedKeys, 'notes', Note.NoteID, NoteID);

      end;

      // ********* Hours

      Op.Detail := 'Hours';
      RefreshLocateDataSet(TicketID); // In case we added new locates that also have hours changes

      for J := 0 to Tk.HoursChanges.Count-1 do begin
        H := Tk.HoursChanges[J];
        IncomingHoursID := H.LocateHoursID;
        OutgoingHoursID := IncomingHoursID;

        LocateID := FindLocateID(TicketID, H.ClientCode); // The passed in locate_id might be negative
        Assert(LocateID > 0);
        if H.EntryDate < EarliestTicketActivityDate then
          EarliestTicketActivityDate := H.EntryDate;

        if IncomingHoursID > 0 then begin
          with EditHours do begin
            Parameters.ParamValues['locate_hours_id'] := IncomingHoursID;
            Parameters.ParamValues['regular_hours'] := H.RegHours;
            Parameters.ParamValues['overtime_hours'] := H.OTHours;
            Parameters.ParamValues['status'] := H.Status;
            Parameters.ParamValues['units_marked'] := H.UnitsMarked;
            Execute;
          end;
        end
        else begin
          if Locate.Locate('client_code', H.ClientCode, []) then begin
            LocateHours.Close;
            LocateHours.Parameters.ParamValues['locate_id'] := LocateID;
            LocateHours.Open;
            // This may not always be safe due to timestamp rounding, but works in testing due
            // to double-stripping of the milliseconds in the Locate() call and the .Post
            if LocateHours.Locate('emp_id;entry_date', VarArrayOf([H.EmpID, H.EntryDate]), []) then
              LocateHours.Edit
            else begin
              LocateHours.Insert;
              LocateHours.FieldByName('locate_id').AsInteger := LocateID;
              LocateHours.FieldByName('emp_id').AsInteger := H.EmpID;
            end;
            LocateHours.FieldByName('work_date').AsDateTime := H.WorkDate;
            LocateHours.FieldByName('regular_hours').AsFloat := H.RegHours;
            LocateHours.FieldByName('overtime_hours').AsFloat := H.OTHours;
            LocateHours.FieldByName('entry_date').AsDateTime := H.EntryDate;
            LocateHours.FieldByName('status').AsString := H.Status;
            LocateHours.FieldByName('units_marked').AsInteger := H.UnitsMarked;
 //           LocateHours.FieldByName('bigfoot').AsBoolean := False;  //QM-1037 EB Bigfoot/ESketch
            if H.UnitConversionID > 0 then
              LocateHours.FieldByName('unit_conversion_id').AsInteger := H.UnitConversionID
            else
              LocateHours.FieldByName('unit_conversion_id').Clear;
            LocateHours.Post;
            OutgoingHoursID := LocateHours.FieldByName('locate_hours_id').AsInteger;

            AddIdentity(GeneratedKeys, 'locate_hours', H.LocateHoursID, OutgoingHoursID);
          end else
            raise Exception.Create('Can''t add hours to locate ' + H.ClientCode + ', not found in ticket ' + IntToStr(TicketID));
        end;

        Assert(OutgoingHoursID > 0);
        Assert(LocateID > 0);
        Assert(TicketID > 0);
        InsertBillingQueue(BillingEventLabor, TicketID, LocateID, H.WorkDate, OutgoingHoursID, Null);
      end;

      Op.Detail := 'Area reassignments';
      ProcessTicketAreaChanges(Tk.AreaChanges);
      Op.Detail := 'Plats';
      ProcessLocatePlatChanges(Tk.PlatChanges, GeneratedKeys);
      Op.Detail := 'Ticket info';
      ProcessTicketInfoChanges(Tk.InfoChanges, GeneratedKeys);
      Op.Detail := 'Jobsite arrivals';
      GetArrivalDM.ProcessJobsiteArrivalChanges(Tk.ArrivalTimeChanges, GeneratedKeys, TicketID);
      Op.Detail := 'Facilities';
      ProcessLocateFacilityChanges(Tk.FacilityChanges, GeneratedKeys);
      Op.Detail := 'Addin info';

      GetAddinInfoDM.ProcessAddinInfoChanges(Tk.AddinInfoChanges, GeneratedKeys);

      // Send High Profile e-mail notifications for the ticket
      Op.Detail := 'MarkedHighProfile Processing';
      for J := 0 to HighProfileLocates.Count - 1 do begin
        try
          LocateID := StrToIntDef(HighProfileLocates.Strings[J], 0);
          if not Locate.Locate('locate_id', LocateID, []) then
            raise Exception.Create('Can''t find locate_id ' + HighProfileLocates.Strings[J]);
          ClientID := Locate.FieldByName('client_id').AsInteger;

          Op.Log('Sending High profile Email ' + IntToStr(LocateID));
          SendHighProfileEmails(Locate.FieldByName('client_code').AsString,
            Ticket.FieldByName('ticket_number').AsString, ClientID,
            GetTicketImage(TicketID), TicketId);
        except
          on E: Exception do begin
            Op.Log('ERROR sending High Profile Email: ' + E.Message);
          end;
        end;
      end;

      Op.Detail := 'Followup';
      // ********* Followup ticket creation (watch and protect, remark, etc.)
      OriginalFollowupID := Ticket.FieldByName('followup_type_id').AsInteger;
      if (Tk.FollowupTypeID > 0) and (Tk.FollowupTypeID <> OriginalFollowupID) then begin
        if not LogicDM.CanI(RightTicketsCreateFollowUp) then
          raise EOdNotAllowed.Create('You do not have permission to create follow up tickets.');

        EditDataSet(Ticket);
        Ticket.FieldByName('followup_type_id').AsInteger := Tk.FollowupTypeID;
        Ticket.Post;

        if Tk.FollowupTransmitDate <= 0 then
          raise Exception.Create('The followup transmit date cannot be ' +
            'blank when creating a followup ticket.');
        if Tk.FollowupDueDate <= 0 then
          raise Exception.Create('The followup due date cannot be blank ' +
            'when creating a followup ticket.');
        if not (CompareDateTime(Tk.FollowupDueDate, Tk.FollowupTransmitDate) = GreaterThanValue) then
          raise Exception.Create('The followup due date/time must be later ' +
            'than the followup transmit date/time.');

        MakeFollowupTicket.Parameters.ParamValues['@TransmitDate'] :=
          Tk.FollowupTransmitDate;
        MakeFollowupTicket.Parameters.ParamValues['@DueDate'] :=
          Tk.FollowupDueDate;
        MakeFollowupTicket.Parameters.ParamValues['@TicketID'] := TicketID;
        MakeFollowupTicket.Parameters.ParamValues['@FollowupTypeID'] := Tk.FollowupTypeID;
        MakeFollowupTicket.ExecProc;
      end;

      Op.Detail := 'Posting Ticket';
      PostDataSet(Ticket);

      {
      Some ackable activity occured on the ticket, either a status indicating
      closed, or hrs / units added to an ongoing ticket. The earliest date of
      the changed locate activity is used for activity_date rather than create
      ticket_activity_ack rows for every locate on the ticket.
      }
      if EarliestTicketActivityDate <> HighDate then begin
        Ticket.Refresh;
        if (OldKind <> TicketKindDone ) and (Ticket.FieldByName('kind').AsString <> TicketKindOngoing) then
          LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypeComplete, EarliestTicketActivityDate)
        else if (Ticket.FieldByName('kind').AsString = TicketKindOngoing) then
          LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypeOngoing, EarliestTicketActivityDate);
      end;
      LocateHours.Close;
      Locate.Close;
      Ticket.Close;
    end;
  finally
    FreeAndNil(HighProfileLocates);
    FreeAndNil(Attacher);
  end;

  Op.Detail := 'Done';
  Op.Log('Done');   // we'll leave this along and delete all the other
                    // from the log when analyzing it.
end;

{EB - Did not want to duplicate this whole thing, but seemed to be the easiest
 way to keep them separate so that the old client calls to StatusTicketsLocates14Internal
 could simply be removed when everyone upgraded}
procedure TTicketDM.StatusTicketsLocates15Internal(
  const TicketChanges: TicketChangeList15; out GeneratedKeys: GeneratedKeyList);
const
    HighProfileNotePrefix = 'HPR-';  //qm-300 sr
    XHighProfileNotePrefix = 'X' + HighProfileNotePrefix;   //qm-300 sr
    ColonDelim = ':';    //qm-300 sr
var
  I, J ,ii, JJ, NN: Integer;
  TicketID, LocateID: Integer;
  Tk: TicketChange15;  {EB - Updated for subtype}
  Note: TicketNote5;   {EB - Updated for subtype}
  Loc: LocateChange6;
  H: LocateHour3;
  GPS: GPSChange;
  OriginalFollowupID: Integer;
  ClientID: Integer;
  StatusID: Integer;
  IncomingHoursID: Integer;
  OutgoingHoursID: Integer;
  NewlyAddedLocate: Boolean;
  MarkedHighProfile: Boolean;
  NoteID: Integer;
  HighProfileLocates: TStringList;
  Dummy: string;
  OldKind: string;
  HighDate: TDateTime;
  EarliestTicketActivityDate: TDateTime;
  NewGPSID: Integer;
  GPSID: Variant;
  AssignedLocatorID: Integer;
  StatusClosedLocate: Boolean;
  StatusReopenedLocate: Boolean;
  KindBeforeStatusChange: string;
  TicketKindChanged: Boolean;
  HPNoteString : TstringList;  //qm-300 SR
  Reasons: string;
  HPRAdded: boolean;  //qm-300 SR
  ClientCode: string; //qm-300 SR
  ClientCodePos, DelimPos: Integer;  //qm-300 SR
  NewHPNote:string; //qm-300 SR
  Attacher: TServerAttachment;
  HPDescription:Tstrings; //qm-300 SR
  HPSubjectLines:TStringList; //qm-300 SR
  TktState, TktCity, TktNumber :String;
  CallCenter: string;
  procedure RefreshLocateDataSet(TicketID: Integer);
  begin
    Locate.Close;
    Locate.Parameters.ParamValues['id'] := TicketID;
    Locate.Open;
  end;

  function TicketToText(Ticket, Locates: TDataset; TermId: string; ClientID: Integer; TicketImage: string; var OutClientCode: string): string;
  const
    LocateSQL =
      'select locate.locate_id, statuslist.status_name, employee.short_name, ' +
      '  dbo.get_plat_list(locate.locate_id) as plat_list, ' +
      '  reference.description as hp_reason_desc ' +
      'from locate ' +
      '  left join statuslist on statuslist.status = locate.status ' +
      '  left join assignment on assignment.locate_id = locate.locate_id ' +
      '    and assignment.active = 1 ' +
      '  left join employee on employee.emp_id = assignment.locator_id ' +
      '  left join reference on reference.ref_id = locate.high_profile_reason ' +
      'where ticket_id = %s';
    NotesSQL =
      'select notes.entry_date, notes.modified_date, notes.note ' +  //qm-300 sr
      'from ticket_notes notes ' +
      'where notes.ticket_id = %s '+
      'and sub_type in (100,200,500)';  //qm-300 sr
  var
    TicketAsText: TStringList;
    LocatesAdditional: TDataset;
    Notes: TDataset;
    HighProfileReason: string;
    ii:integer;
  begin
    TicketAsText := TStringList.Create;
    OutClientCode := '';
    try
      with TicketAsText do begin
        Add(Format('Ticket Number: %s', [Ticket.FieldByName('ticket_number').AsString]));
        Add(Format('Received date: %s', [Ticket.FieldByName('transmit_date').AsString]));
        Add(Format('Due date: %s', [Ticket.FieldByName('due_date').AsString]));
        Add(Format('Client Term: %s', [TermId]));
        Add('');
        Add('Ticket Image:');
        Add('------------');
        Add(TicketImage);
        Add('');
        Locates.First;
        LocatesAdditional := LogicDM.CreateDataSetForSQL(Format(LocateSQL,[Ticket.FieldByName('ticket_id').AsString]));
        while not Locates.EOF do begin
          if (Locates.FieldByName('client_id').AsInteger <> ClientID) then begin
            Locates.Next;
            Continue;
          end;
          LocatesAdditional.Locate('locate_id',Locates.FieldByName('locate_id').AsInteger,[]);
          HighProfileReason := LocatesAdditional.FieldByName('hp_reason_desc').AsString;
          if Locates.FieldByName('high_profile').AsBoolean and (HighProfileReason = '') then
            HighProfileReason := GetMultiHighProfileReasonDesc(Locates.FieldByName('ticket_id').AsInteger,
              Locates.FieldByName('client_code').AsString);

          with Locates do begin
            Add('-------------');
            Add('Locator: '+LocatesAdditional.FieldByName('short_name').AsString);//qm-300 sr
            Add('Client: '+FieldByName('client_code').AsString);
            OutClientCode := FieldByName('client_code').AsString;
            Add('Status: '+LocatesAdditional.FieldByName('status_name').AsString);
            Add('When Closed: '+FieldByName('closed_date').AsString);
            Add('Closed: '+FieldByName('closed').AsString);
            Add('Plat Number(s): '+LocatesAdditional.FieldByName('plat_list').AsString);
            Add('High Profile Reason: ');

            For ii := 0 to HPDescription.count -1 do
            begin
              if String(HPDescription.Objects[ii]) = FieldByName('client_code').AsString  then
              Add('  '+HPDescription.Strings[ii]);
            end;

          end;
          Locates.Next;
        end;
        Notes := LogicDM.CreateDataSetForSQL(Format(NotesSQL, [Ticket.FieldByName('ticket_id').AsString]));
        Notes.First;
        while not Notes.EOF do begin
          Add('-------------');
          Add('Note Entered: ' + Notes.FieldByName('entry_date').AsString);
          Add(Notes.FieldByName('note').AsString);
//          Add(Notes.FieldByName('sub_type').AsString);
          Notes.Next;
        end;
        FreeAndNil(LocatesAdditional);
        FreeAndNil(Notes);
        Result := Text;
      end; // with
    finally
      FreeAndNil(TicketAsText);
    end;
  end;

  function ProcessHPMailBody(HPNoteString:TStringList; ClientCode:string; EmpID:integer; CallCenter: string):TStrings;  //qm-300 sr
  var
    UtilType: string;
    hiRanked: string;
    s: string;
    ClientID: integer;
    RestrictHPEmails: integer; //QM-1076 HP Email Refinement
    HasOnlyEmail, HasOther: Boolean;  //QM-1076 HP Email Refinement - this determines if we have a mix
  const
    SUBJECT_LINE = '%s - %s - %s - %s';
    DEF_LINE = '%s - %s';
  GET_HP_RANKING =  'Select HPR.utility_code, HPR.rank, R.Code, R.[description], HPR.only_email ' +
                    'from reference r  ' +
                    'left outer join hp_utils_rank HPR on (r.ref_id = HPR.hp_ref_id )  ' +
                    'where (r.type = ''hpmulti'')  ' +
                      'and (HPR.client_id = %d) ' +
                      'and (HPR.utility_code = ''%s'') ' +
                      'and (R.code in (%s)) ' +
                      'and (R.active_ind = 1)  ' +
                      'and (HPR.active = 1)  ' +
                      'and (HPR.rank > 0)   ' +
                    'order by HPR.rank ';

  GET_JUST_CODES =  'select R.code, R.description, R.sortby, r.ref_id from reference r ' +
					          'where (r.type = ''hpmulti'')  ' +
					          'and (r.code in (%s)) '  +
				    	      'and (r.active_ind = 1) ' +
				           	 'order by R.sortby';

  CLIENT_RESTRICT_EMAIL = 'select count(*) restrictedcount from hp_utils_rank ' +       //QM-1076 HP Restrict Email
                          'where (client_id = %d) and (only_email = 1)';

  begin
    if trim(HPNoteString.Text) = '' then     //QM-860 EB If there is no HP Note, then nothing to do here
      exit;
      
    hiRanked := '';
    RestrictHPEmails := 0;  //QM-1076 HP Restrict Email
    HasOnlyEmail := False; //QM-1076 HP Restrict Email
    HasOther := False;     //QM-1076 HP Restrict Email

    UtilType := GetClientUtilType(ClientCode, Ticket.FieldByName('ticket_format').AsString);
    ClientID := GetClientIDForCode(ClientCode, CallCenter);
    try
//      HPNoteString.clear;

     {first use this to see if we need to restict emails}
      getHPRanking.sql.text := format(CLIENT_RESTRICT_EMAIL, [ClientID]);
      getHPRanking.open;
      getHPRanking.First;
      if not getHPRanking.EOF then
        RestrictHPEmails := getHPRanking.FieldByname('restrictedcount').asInteger;
      getHPRanking.Close;

      {New Query - Get ranking}
      getHPRanking.sql.text := format(GET_HP_RANKING,[ClientID, UtilType, HPNoteString.DelimitedText]);
      getHPRanking.open;
      getHPRanking.First;

      {If code was ranked in hp_utils_rank}
      if not getHPRanking.EOF then begin
        hiRanked :=getHPRanking.FieldByName('description').AsString;
        s := format(SUBJECT_LINE,[TktNumber, TktState, TktCity, hiRanked]);
        while not getHPRanking.EOF do
        begin   {Subject line and list of Codes}
          if RestrictHPEmails > 0 then begin
            if (getHPRanking.FieldByName('only_email').AsBoolean = TRUE) then begin
              HasOnlyEmail := True;    //if this record says only email
              HPSubjectLines.AddObject(s, TObject(ClientCode));
              HPDescription.AddObject(getHPRanking.FieldByName('description').AsString, TObject(ClientCode));
            end
            else
              HasOther := True;
          end
          else begin  {No Restrictions on email }
             HPSubjectLines.AddObject(s, TObject(ClientCode));
             HPDescription.AddObject(getHPRanking.FieldByName('description').AsString, TObject(ClientCode));
          end;
          getHPRanking.Next;
        end;
      end
      {Not ranked}
      else begin
        GetHPMultiCodes.sql.text := format(GET_JUST_CODES, [HPNoteString.DelimitedText]);
        GetHPMultiCodes.open;
        GetHPMultiCodes.first;
        if not GetHPMultiCodes.EOF then begin
          hiRanked := '';
          s := format(DEF_LINE,[TktNumber, 'High Profile Locate']);
        end;
        while not GetHPMultiCodes.EOF do
        begin   {Subject line and list of Codes}
          if (RestrictHPEmails < 1) then begin
            HPSubjectLines.AddObject(s, TObject(ClientCode));
            HPDescription.AddObject(GetHPMultiCodes.FieldByName('description').AsString, TObject(ClientCode));
          end;
          GetHPMultiCodes.Next;
        end;
      end;

       {Should not send Email}
      if (RestrictHPEmails > 0) and (HasOnlyEmail = FALSE) then begin
        HPDescription.AddObject(HPEMAILNOSEND, TObject(ClientCode));  //QM-1076 EB HP Email Restriction
     //   HPSubjectLines.AddObject(' ' + HPEMAILNOSEND, TObject(ClientCode));  //QM-1076 EB HP Email Restriction
      end;

   finally
      HPNoteString.clear;
      getHPRanking.close;
      GetHPMultiCodes.Close;
      
      result := HPDescription;
    end;
  end;

  

  procedure SendHighProfileEmails(TermId: string; TicketNumber: string; ClientID: Integer; TicketImage: string; TicketId: Integer);
  var
    Body, FromEMail, ToEmail, Subject: string;
    EmailList: string;
    FileList: TStringList;
    ErrorList: TStringList;
    i: integer;
    RetClientCode: string;
    DebugFileLoc: string;
    DebugUsingFile: boolean;   //QM-1076 HP Email Restriction
    Ignore: boolean;
    HPNoSendFlag: boolean;  //QM-1076 HP Email Restriction
  begin
    HPNOSendFlag := False;
    if LogicDM.GetIniBoolSetting('HighProfileEmail', 'Ignore', False) then
      Exit;
    FromEMail:=''; //QM-9 gets this from INI
    ToEmail:= ''; //Used for debug
    ErrorList := nil;
    FileList := TStringList.Create;
    try
      Subject := '';

      ErrorList := TStringList.Create;
      Body := TicketToText(Ticket, Locate, TermId, ClientID, TicketImage, RetClientCode);

      {Make sure it is matched correctly}
      for i := 0 to HPSubjectLines.Count - 1 do
      begin
        if String(HPSubjectLines.Objects[i]) = RetClientCode then
          Subject := HPSubjectLines.Strings[i];
      end;

      {Get High Profile Settings that we need}   //QM-1076 High Profile Restriction EB
      EmailList := GetClientHighProfileEmailList(ClientID);
      DebugUsingFile := LogicDM.GetIniBoolSetting('HighProfileEmail', 'DebugUsingFile', False);
      DebugFileLoc := LogicDM.GetStringIniSetting('HighProfileEmail', 'DebugFilePath', '');
      FromEmail := LogicDM.GetStringIniSetting('HighProfileEmail', 'From', '');
      ToEmail := LogicDM.GetStringIniSetting('HighProfileEmail', 'To', '');
      Ignore := LogicDM.GetIniBoolSetting('HighProfileEmail', 'Ignore', False);

      if (AnsiPos(HPEMAILNOSEND, Body) > 0) then
        HPNOSendFlag := TRUE;  //QM-1076 EB Don't want to send this

      {Send Email information to a Debug Text file instead (DebugUsingFile=1)}
      if DebugUsingFile then begin
        LogicDM.DebugShowEmailinTextFile('HighProfileEmail', FromEmail, EmailList{ToEmail}, Subject, Body, DebugFileLoc);
      end;

      if (HPNoSendFlag = FALSE) then  //QM-1076 EB Email Restrict (if this is in the body, don't send}
        LogicDM.SendEmailWithIniSettingsExp('HighProfileEmail', FromEMail, EmailList, Subject, Body, FileList);  //qm-300 sr

    finally
      FreeAndNil(FileList);
      FreeAndNil(ErrorList);
    end;
  end;



  procedure ValidateLocateStatus(const Status: string);
  var
    TicketID: Integer;
    LocateID: Integer;
    ClientID: Integer;
  begin
    TicketID := Locate.FieldByName('ticket_id').AsInteger;
    LocateID := Locate.FieldByName('locate_id').AsInteger;
    ClientID := Locate.FieldByName('client_id').AsInteger;
    try
      // For now, these are non-fatal (log-only) errors
      if not ValidStatusForClient(ClientID, Status) then
        raise Exception.CreateFmt('%s is not allowed for client %d', [Status, ClientID]);
      if not ValidStatusForTicket(TicketID, Status) then
        raise Exception.CreateFmt('%s is not allowed for non-meet ticket %d', [Status, TicketID]);
      if not ValidStatusForEmployee(LogicDM.LoggedInEmpID, Status) then
        raise Exception.CreateFmt('%s status is not allowed for employee %d', [Status, LogicDM.LoggedInEmpID]);
    except
      on E: Exception do
        Op.Log(Format('Warning - Invalid status for locate %d: %s', [LocateID, E.Message]));
    end;
  end;

{Start of StatusTicketsLocates15Internal}
begin
  // Experimental feature:
  // this info is needed before the DM is set up / transaction is started
  // Repeatable Read has the effect of locking the affected rows, in SQL Server.
  // FDesiredIsolation := ilRepeatableRead;
  HPSubjectLines:= TStringList.Create;

  LogicDM.CheckApplicationVersion(False, Dummy);
  GeneratedKeys := GeneratedKeyList.Create;

  Attacher := nil;
  StatusList.Open;
  Assert(StatusList.RecordCount > 0, 'Statuses not defined');
  HighProfileLocates := TStringList.Create;
  try
    Attacher := TServerAttachment.Create(LogicDM.Conn, LogicDM.LoggedInEmpID);
    HighDate := EncodeDate(2199, 12, 31); // arbitrary high date
    for I := 0 to TicketChanges.Count-1 do begin
      SetPhase(Format('Ticket %d of %d', [I + 1, TicketChanges.Count]));
      Op.Detail := 'Loading tickets and locates';

      EarliestTicketActivityDate := HighDate;
      HighProfileLocates.Clear;
      Tk := TicketChanges[I];
      TicketID := Tk.TicketID.TicketID;  // less than ideal variable naming...
      OpenTicketTo(TicketID);
      if Ticket.Eof then
        raise Exception.CreateFmt('Ticket ID %d does not exist to edit', [TicketID]);
      TktState  := Ticket.FieldByName('Work_State').AsString; //qm-300 sr
      TktCity   := trim(Ticket.FieldByName('Work_City').AsString); //qm-300 sr
      TktNumber := trim(Ticket.FieldByName('ticket_number').AsString); //qm-300 sr
      OldKind := Ticket.FieldByName('kind').AsString;

       {New GPS Position}      // QM-799 Marking this 
      for J := 0 to Tk.GPSChanges.Count - 1 do begin
        SetPhase(Format('Ticket %d of %d, GPS %d of %d',
            [I + 1, TicketChanges.Count, J + 1, Tk.GPSChanges.Count]));
        Op.Detail := 'Finding GPS';
        GPS := TicketChanges[I].GPSChanges[J];
        if GPS.GPSID > 0 then
          raise Exception.Create('Updating of existing GPS positions is not supported, ID ' + IntToStr(GPS.GPSID));

        NewGPSID := InsertGPSPostion(GPS.AddedBy, GPS.AddedDate, GPS.Latitude, GPS.Longitude, GPS.HDOPFeet);
        Assert(NewGPSID > 0);
        AddIdentity(GeneratedKeys, 'gps_position', GPS.GPSID, NewGPSID);
      end;

      RefreshLocateDataSet(TicketID);
      // ********* Locate Changes
      for J := 0 to Tk.LocateChanges.Count-1 do begin
        SetPhase(Format('Ticket %d of %d, Locate %d of %d',
            [I + 1, TicketChanges.Count, J + 1, Tk.LocateChanges.Count]));
        Op.Detail := 'Finding Locate';
        Loc := TicketChanges[I].LocateChanges[J];

        LocateID := -1;
        NewlyAddedLocate := False;
        KindBeforeStatusChange := Ticket.FieldByName('kind').AsString;

        // Try to find the locate by ID, in case there are duplicates of the
        // client code on this ticket for any reason
        if (Loc.LocateID > 0) and Locate.Locate('locate_id', Loc.LocateID, []) then begin
          Locate.Edit;
          LocateID := Loc.LocateID;
          AssignedLocatorID := Locate.FieldByName('locator_id').AsInteger;
        end else begin
          // Get the ClientID here, so a little less happens between the refresh and Append/Post
          ClientID := GetClientIDForCode(Loc.ClientCode, Ticket.FieldByName('ticket_format').AsString);

          // Refresh in case this term was just added by another sync in the meantime
          RefreshLocateDataSet(TicketID);

          // Locate it by the Client Code.  If it's new (ID<0), nonetheless
          // find it by the client code, so we don't create a duplicate
          if Locate.Locate('client_code', Loc.ClientCode, []) then begin
            // Update an existing locate
            Locate.Edit;
            LocateID := Locate.FieldByName('locate_id').AsInteger;
            AssignedLocatorID := Locate.FieldByName('locator_id').AsInteger;
            NewlyAddedLocate := False;
          end else begin
            // Add a new locate
            Op.Detail := 'Adding New Locate';

            Locate.Append;
            Locate.FieldByName('ticket_id').AsInteger := TicketID;
            Locate.FieldByName('client_code').AsString := Loc.ClientCode;
            Locate.FieldByName('client_id').AsInteger := ClientID;
            Locate.FieldByName('active').AsBoolean := True;
            Locate.FieldByName('added_by').AsString := IntToStr(LogicDM.LoggedInEmpID);
            Locate.FieldByName('modified_date').AsDateTime := Now;
            if Locate.FieldByName('assigned_to').AsInteger > 0 then
              AssignedLocatorID := Locate.FieldByName('assigned_to').AsInteger
            else
              AssignedLocatorID := AssignNewLocateTo(TicketID);
            NewlyAddedLocate := True;
          end;
        end;

        // Common handling for inserts and updates
        if Locate.FieldByName('status').AsString <> Loc.Status then
          ValidateLocateStatus(Loc.Status);

        Locate.FieldByName('status').AsString := Loc.Status;
        if StatusList.Locate('status', Loc.Status, []) then begin
          StatusClosedLocate := StatusList.FieldByName('complete').AsBoolean and (not Locate.FieldByName('closed').AsBoolean);
          StatusReopenedLocate := Locate.FieldByName('closed').AsBoolean and (not StatusList.FieldByName('complete').AsBoolean);
          Locate.FieldByName('closed').AsBoolean := StatusList.FieldByName('complete').AsBoolean;
        end else
          raise Exception.Create('Unknown Status: ' + Loc.Status);

        Locate.FieldByName('mark_type').AsString := Loc.MarkType;
        if Loc.StatusDate > 1 then
          Locate.FieldByName('closed_date').AsDateTime := RoundSQLServerTimeMS(Loc.StatusDate);

        if (Locate.FieldByName('closed').AsBoolean) and (Loc.StatusDate > 1) and
          (Loc.StatusDate < EarliestTicketActivityDate) then
          EarliestTicketActivityDate := Loc.StatusDate;

        if Loc.StatusAsAssignedLocator then begin
          Locate.FieldByName('closed_by_id').AsInteger := AssignedLocatorID;

        InsertTicketNote(TicketID, Loc.StatusDate, LogicDM.LoggedInUID,
             'Locate ' + Loc.ClientCode + NoteStatusedasAssigned +
             LogicDM.LoggedInEmpShortName + ' (Locator ID ' + IntToStr(LogicDM.LoggedInEmpID) + ')', PrivateTicketSubType);  //SubType of 100 is standard ticket note
        end else
          Locate.FieldByName('closed_by_id').AsInteger := LogicDM.LoggedInEmpID;

        //Keep track of who actually statused the locate in status_changed_by_id- currently used for reporting purposes only
        //This is different than closed_by_id, which is affected by the "Status As Assigned User" checkbox.
        Locate.FieldByName('status_changed_by_id').AsInteger := LogicDM.LoggedInEmpID;

        Locate.FieldByName('closed_how').AsString := Loc.StatusedHow;
        Locate.FieldByName('qty_marked').AsFloat := Loc.QtyMarked;

        // TODO: We don't handle a locate being unmarked as HP
        MarkedHighProfile := (Locate.FieldByName('high_profile').AsBoolean <> Loc.HighProfile) and Loc.HighProfile;

        Locate.FieldByName('high_profile').AsBoolean := Loc.HighProfile;

        if Loc.HighProfileReason > -1 then
          Locate.FieldByName('high_profile_reason').AsInteger := Loc.HighProfileReason
        else
          Locate.FieldByName('high_profile_reason').Clear;

        if Loc.EntryDate > 0 then
          Locate.FieldByName('entry_date').AsDateTime := Loc.EntryDate
        else
          Locate.FieldByName('entry_date').Clear;

          //QM-799 Setting the gps_id (Marking this section of code)
        if (Loc.GPSID < 0) then begin
          if GetGeneratedIdentity(GeneratedKeys, 'gps_position', Loc.GPSID, GPSID) then
            Locate.FieldByName('gps_id').Value := GPSID
          else
            raise Exception.Create('Cannot find gps_id for newly added ticket GPS data');
        end else if (Loc.GPSID > 0) then
          Locate.FieldByName('gps_id').AsInteger := Loc.GPSID
        else
          Locate.FieldByName('gps_id').Clear;

        // If you have an update/insert trigger, add the code to the beginning:
        // SET NOCOUNT ON

        if StringInArray(Locate.FieldByName('status').AsString, InternalStatuses) then
          raise Exception.Create('Q Manager can not edit locates with a status of -P or -N');

        // This controls the WHERE clause it uses
        Locate.Properties['Update Criteria'].value := adCriteriaKey;

        // Only post and do the related changes if a locate field was changed
        if (NewlyAddedLocate or AnyFieldWasEdited(Locate, True)) then begin
          Op.Detail := 'Posting Locate';
          Locate.Post;
          // There is a locate table trigger that does this:
          // * Insert row in to locate_status
          // * insert row in to responder_queue
          // * update the ticket row to reflect DONE or not
          // All of that can be moved here, once every locator is using QMLogic

          // TODO: Find a better way to address getting this value (a query, etc.), since it is very unreliable
          StatusID := LogicDM.GetAnyIdentity;  // Make sure this comes after immediately after locate.post so
                                               // we get the locate_status_id (not the locate_id).

          // We can't fetch the ID until it's actually posted to the DB
          if NewlyAddedLocate then begin
            Op.Detail := 'NewlyAddedLocate UpdateAssignments';

            // We can't get it from here, because this gets populated with
            // the wrong ID, because of a bug with Delphi ADO and Triggers
            //LocateID := Locate.FieldByName('locate_id').AsInteger;

            // And we can't get it from the Scope Identity, because ADO already
            // grabbed @@IDENTITY and destroyed it
            // LocateID := GetScopeIdentity;

            Op.Detail := 'Getting back locate_id';
            LocateID := FindLocateID(TicketID, Loc.ClientCode);
            Assert(LocateID > 1000);  // Make sure the ID got populated back

            // Assign to me
            UpdateAssignments.Parameters.ParamValues['@LocateID'] := LocateID;
            UpdateAssignments.Parameters.ParamValues['@LocatorID'] := AssignedLocatorID;
            UpdateAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
            UpdateAssignments.ExecProc;

            MarkedHighProfile := Loc.HighProfile;
          end;

          Op.Detail := 'InsertBillingQueue';
          InsertBillingQueue(BillingEventLocate, TicketID, LocateID, Loc.StatusDate, Null, StatusID);

          Op.Detail := 'InsertNotificationQueues';
          Op.Log(Format('Trying to insert in notificaton queues: %s, %s, %s, %s, %s, %s',
            [IntToStr(LocateID), IntToStr(StatusID), Loc.Status,
            Ticket.FieldByName('ticket_format').AsString,
            Ticket.FieldByName('work_state').AsString, Loc.ClientCode]));
          InsertNotificationQueues(LocateID, StatusID, Loc.Status,
             Ticket.FieldByName('ticket_format').AsString,
             Ticket.FieldByName('work_state').AsString, Loc.ClientCode);

          Op.Log('Statused locate ' + IntToStr(LocateID) + ' to status ' + Loc.Status);

          // Track high profile locates so we can send e-mails later
          if MarkedHighProfile then
            HighProfileLocates.Add(IntToStr(LocateID));

          if GetLocateMultiResponse(Ticket.FieldByName('ticket_format').AsString,    //QMANTWO-168
            Loc.ClientCode) then begin
            Op.Detail := 'Inserting in Responder Multi Queue';
            with InsertResponderMultiQueue do begin
              Parameters.ParamValues['respond_to'] := 'client';
              Parameters.ParamValues['locate_id'] := LocateID;
              Execute;
            end;
          end;
        end
        else begin // No locate fields were changed, so don't save the locate record
          Op.Detail := 'Canceling unchanged locate';
          Locate.Cancel;
        end;

        // If the incoming ID is negative, then return an ID, even if its an
        // existing locate - that way if two locators insert a locate, it works.
        if Loc.LocateID < 0 then begin
          Op.Detail := 'Returning LocateID';
          if LocateID < 0 then
            raise Exception.Create('Error, LocateID < 0');
          AddIdentity(GeneratedKeys, 'locate', Loc.LocateID, LocateID);
        end;

        RefreshDataSet(Ticket); // get changes made by the locate_iu trigger
        Assert(TicketID = Ticket.FieldByName('ticket_id').Value, 'Unexpected ticket_id');

// TODO: The StoreEventsEnabled option needs to go away once we are reliably creating
// events so it can't accidentally be turned off and leave a hole in the event_log!
        if LogicDM.StoreEventsEnabled then begin
          Op.Detail := 'Updating Event Store';
          if StatusClosedLocate then
            TicketKindChanged := (KindBeforeStatusChange <> TicketKindDone)
              and (Ticket.FieldByName('kind').AsString = TicketKindDone)
          else if StatusReopenedLocate then
            TicketKindChanged := (KindBeforeStatusChange = TicketKindDone)
              and (Ticket.FieldByName('kind').AsString <> TicketKindDone);

          if StatusClosedLocate or StatusReopenedLocate then
            LogicDM.EventsDM.LogCompletionChangedEvent(TicketID,
              Locate.FieldByName('locate_id').AsInteger,
              Locate.FieldByName('closed_by_id').AsInteger,
              Locate.FieldByName('client_code').AsString,
              Ticket.FieldByName('ticket_format').AsString,
              Locate.FieldByName('status').AsString,
              Locate.FieldByName('closed_date').AsDateTime,
              Locate.FieldByName('closed').AsBoolean,
              TicketKindChanged,
              Ticket.FieldByName('ticket_type').AsString,
              Locate.FieldByName('workload_date').AsDateTime);
        end;
      end; // end of LocateChanges loop

      { ********* Notes loop ****************}
      Op.Detail := 'NewNotes';
      HPNoteString := TStringList.Create;
      HPNoteString.Clear; // qm-300 sr
      HPDescription:=  TstringList.Create; // qm-300 sr
      for NN := 0 to Tk.NewNotes.Count - 1 do begin
      //  NoteHasChanged := False;
        Note := Tk.NewNotes[NN];
        if (Note.NoteID > 0) and NoteHasChanged2(Note.NoteText) then   {15}
          GetNotesDM.ChangeNote(Note.NoteID, Note.EnteredByUID, Note.NoteText, Note.SubType)  //QM-188 EB
        else begin
          {EB - OLDER CLIENTS USING THIS VERSION: For the subtype, we are just going to enter 0}
          NoteID := InsertTicketNote(TicketID, Note.EntryDate, Note.EnteredByUID, Note.NoteText, Note.SubType, Note.ClientCode );

          HPRAdded := (Pos(HighProfileNotePrefix, Note.NoteText) = 1);

        //qm-300 sr
          If HPRAdded then
          begin
             {We just want to extract the Reason codes, so strip the description at the end}
             DelimPos := Pos(ColonDelim, Note.NoteText); // the colon
             if DelimPos > 0 then
               NewHPNote := Copy(Note.NoteText, 0, DelimPos-1);

             NewHPNote := trim(NewHPNote);
             ClientCodePos:=-1;  //qm-300 sr
             ClientCodePos := Pos(Note.ClientCode, NewHPNote );  //qm-300 sr

             if ClientCodePos = -1 then    {Can't find either format}
               ShowMessage('Line 1340 Invalid format for high profile (HPR-) note: ' + NewHPNote)
             else
             begin
               Reasons := Copy(NewHPNote, Length(HighProfileNotePrefix) + 1, ClientCodePos - 1 - Length(HighProfileNotePrefix));
               HPNoteString.CommaText := Reasons;  // TObject(Note.EnteredByUID));  // qm-300 sr
//               showmessage('HPNoteString '+HPNoteString.text);
//               Sleep(300);
               CallCenter := Ticket.FieldByName('ticket_format').Value;
               HPDescription:=ProcessHPMailBody(HPNoteString, Note.ClientCode, Note.EnteredByUID, CallCenter);  // qm-300 sr
               HPNoteString.clear;
             end;
          end;

          Assert(NoteID > 0);
          AddIdentity(GeneratedKeys, 'notes', Note.NoteID, NoteID);
        end; //else begin
      end; //for NN := 0 to Tk.NewNotes.Count - 1 do begin

      HPNoteString.free; // qm-300 sr
      {---------------------------------------------}

      // ********* Hours

      Op.Detail := 'Hours';
      RefreshLocateDataSet(TicketID); // In case we added new locates that also have hours changes

      for J := 0 to Tk.HoursChanges.Count-1 do begin
        H := Tk.HoursChanges[J];
        IncomingHoursID := H.LocateHoursID;
        OutgoingHoursID := IncomingHoursID;

        LocateID := FindLocateID(TicketID, H.ClientCode); // The passed in locate_id might be negative
        Assert(LocateID > 0);
        if H.EntryDate < EarliestTicketActivityDate then
          EarliestTicketActivityDate := H.EntryDate;

        if IncomingHoursID > 0 then begin
          with EditHours do begin
            Parameters.ParamValues['locate_hours_id'] := IncomingHoursID;
            Parameters.ParamValues['regular_hours'] := H.RegHours;
            Parameters.ParamValues['overtime_hours'] := H.OTHours;
            Parameters.ParamValues['status'] := H.Status;
            Parameters.ParamValues['units_marked'] := H.UnitsMarked;
            Execute;
          end;
        end
        else begin
          if Locate.Locate('client_code', H.ClientCode, []) then begin
            LocateHours.Close;
            LocateHours.Parameters.ParamValues['locate_id'] := LocateID;
            LocateHours.Open;
            // This may not always be safe due to timestamp rounding, but works in testing due
            // to double-stripping of the milliseconds in the Locate() call and the .Post
            if LocateHours.Locate('emp_id;entry_date', VarArrayOf([H.EmpID, H.EntryDate]), []) then
              LocateHours.Edit
            else begin
              LocateHours.Insert;
              LocateHours.FieldByName('locate_id').AsInteger := LocateID;
              LocateHours.FieldByName('emp_id').AsInteger := H.EmpID;
            end;
            LocateHours.FieldByName('work_date').AsDateTime := H.WorkDate;
            LocateHours.FieldByName('regular_hours').AsFloat := H.RegHours;
            LocateHours.FieldByName('overtime_hours').AsFloat := H.OTHours;
            LocateHours.FieldByName('entry_date').AsDateTime := H.EntryDate;
            LocateHours.FieldByName('status').AsString := H.Status;
            LocateHours.FieldByName('units_marked').AsInteger := H.UnitsMarked;
//            LocateHours.FieldByName('bigfoot').AsBoolean := False;
            if H.UnitConversionID > 0 then
              LocateHours.FieldByName('unit_conversion_id').AsInteger := H.UnitConversionID
            else
              LocateHours.FieldByName('unit_conversion_id').Clear;
            LocateHours.Post;
            OutgoingHoursID := LocateHours.FieldByName('locate_hours_id').AsInteger;

            AddIdentity(GeneratedKeys, 'locate_hours', H.LocateHoursID, OutgoingHoursID);
          end else
            raise Exception.Create('Can''t add hours to locate ' + H.ClientCode + ', not found in ticket ' + IntToStr(TicketID));
        end;

        Assert(OutgoingHoursID > 0);
        Assert(LocateID > 0);
        Assert(TicketID > 0);
        InsertBillingQueue(BillingEventLabor, TicketID, LocateID, H.WorkDate, OutgoingHoursID, Null);
      end;

      Op.Detail := 'Area reassignments';
      ProcessTicketAreaChanges(Tk.AreaChanges);
      Op.Detail := 'Plats';
      ProcessLocatePlatChanges(Tk.PlatChanges, GeneratedKeys);
      Op.Detail := 'Ticket info';
      ProcessTicketInfoChanges(Tk.InfoChanges, GeneratedKeys);
      Op.Detail := 'Jobsite arrivals';
      GetArrivalDM.ProcessJobsiteArrivalChanges(Tk.ArrivalTimeChanges, GeneratedKeys, TicketID);
      Op.Detail := 'Facilities';
      ProcessLocateFacilityChanges(Tk.FacilityChanges, GeneratedKeys);
      Op.Detail := 'Addin info';

      GetAddinInfoDM.ProcessAddinInfoChanges(Tk.AddinInfoChanges, GeneratedKeys);

      // Send High Profile e-mail notifications for the ticket
      Op.Detail := 'MarkedHighProfile Processing';
      for JJ := 0 to HighProfileLocates.Count - 1 do begin
        try
          LocateID := StrToIntDef(HighProfileLocates.Strings[JJ], 0);
          if not Locate.Locate('locate_id', LocateID, []) then
            raise Exception.Create('Can''t find locate_id ' + HighProfileLocates.Strings[J]);
          ClientID := Locate.FieldByName('client_id').AsInteger;
          Op.Log('Sending High profile Email ' + IntToStr(LocateID));
          SendHighProfileEmails(Locate.FieldByName('client_code').AsString,
            Ticket.FieldByName('ticket_number').AsString, ClientID,
            GetTicketImage(TicketID), TicketId);

        except
          on E: Exception do begin
            Op.Log('ERROR sending High Profile Email: ' + E.Message);
            showmessage('ERROR sending High Profile Email: ' + E.Message);
          end;
        end;
      end;  //for JJ := 0 to

      Op.Detail := 'Followup';
      // ********* Followup ticket creation (watch and protect, remark, etc.)
      OriginalFollowupID := Ticket.FieldByName('followup_type_id').AsInteger;
      if (Tk.FollowupTypeID > 0) and (Tk.FollowupTypeID <> OriginalFollowupID) then begin
        if not LogicDM.CanI(RightTicketsCreateFollowUp) then
          raise EOdNotAllowed.Create('You do not have permission to create follow up tickets.');

        EditDataSet(Ticket);
        Ticket.FieldByName('followup_type_id').AsInteger := Tk.FollowupTypeID;
        Ticket.Post;

        if Tk.FollowupTransmitDate <= 0 then
          raise Exception.Create('The followup transmit date cannot be ' +
            'blank when creating a followup ticket.');
        if Tk.FollowupDueDate <= 0 then
          raise Exception.Create('The followup due date cannot be blank ' +
            'when creating a followup ticket.');
        if not (CompareDateTime(Tk.FollowupDueDate, Tk.FollowupTransmitDate) = GreaterThanValue) then
          raise Exception.Create('The followup due date/time must be later ' +
            'than the followup transmit date/time.');

        MakeFollowupTicket.Parameters.ParamValues['@TransmitDate'] :=
          Tk.FollowupTransmitDate;
        MakeFollowupTicket.Parameters.ParamValues['@DueDate'] :=
          Tk.FollowupDueDate;
        MakeFollowupTicket.Parameters.ParamValues['@TicketID'] := TicketID;
        MakeFollowupTicket.Parameters.ParamValues['@FollowupTypeID'] := Tk.FollowupTypeID;
        MakeFollowupTicket.ExecProc;
      end;

      Op.Detail := 'Posting Ticket';
      PostDataSet(Ticket);

      {
      Some ackable activity occured on the ticket, either a status indicating
      closed, or hrs / units added to an ongoing ticket. The earliest date of
      the changed locate activity is used for activity_date rather than create
      ticket_activity_ack rows for every locate on the ticket.
      }
      {EB - research the date}
      if EarliestTicketActivityDate <> HighDate then begin
        Ticket.Refresh;
        {retain the Activity if Ongoing}
        if (OldKind <> TicketKindDone ) and (Ticket.FieldByName('kind').AsString <> TicketKindOngoing) then begin
          If Ticket.FieldByName('kind').AsString = TicketKindDone then
            LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypeComplete, EarliestTicketActivityDate)
          else
            LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypePartial, EarliestTicketActivityDate);
        end
        else if (Ticket.FieldByName('kind').AsString = TicketKindOngoing) then
          LogicDM.AddTicketActivityAck(TicketID, TicketActivityTypeOngoing, EarliestTicketActivityDate);
      end;
      LocateHours.Close;
      Locate.Close;
      Ticket.Close;
    end;
  finally
    FreeAndNil(HPDescription); //qm-300 sr
    FreeAndNil(HPSubjectLines);   //qm-300 sr
    FreeAndNil(HighProfileLocates);
    FreeAndNil(Attacher);
  end;

  Op.Detail := 'Done';
  Op.Log('Done');   // we'll leave this along and delete all the other
                    // from the log when analyzing it.
end;

function TTicketDM.ValidStatusForClient(const ClientID: Integer; const Status: string): Boolean;
const
  SelectStatus = 'select distinct sgi.status from client ' +
    'inner join status_group_item sgi on client.status_group_id=sgi.sg_id ' +
    'inner join statuslist sl on sgi.status=sl.status ' +
    'where sgi.active=1 and sgi.status not in (%s) ' +
    'and sl.active=1 and client.client_id=%d';
var
  StatusList: TDataSet;
begin
  Result := True;
  StatusList := LogicDM.CreateDataSetForSQL(Format(SelectStatus,
    [StringArrayToDelimitedString(InternalStatuses, '''', ','), ClientID]));
  try
    if HasRecords(StatusList) then
      Result := StatusList.Locate('status', Status, []);
  finally
    FreeAndNil(StatusList);
  end;
end;

function TTicketDM.ValidStatusForTicket(const TicketID: Integer; Status: string): Boolean;
begin
  Assert(StatusList.Active, 'StatusList must be active in ValidStatusForTicket');
  if not StatusList.Locate('status', Status, []) then
    raise Exception.Create('Unknown Status: ' + Status);

  Result := True;
  if StatusList.FieldByName('meet_only').AsBoolean then
    Result := IsMeetTicket(TicketID);
end;

function TTicketDM.ValidStatusForEmployee(const LoggedInEmpID: Integer; const Status: string): Boolean;
const
  SelectStatus = 'select sgi.status ' +
    'from status_group sg ' +
    '  join status_group_item sgi on sgi.sg_id = sg.sg_id ' +
    'where ' +
    '  %s ' +
    '  sg.active = 1 ' +
    '  and sgi.active = 1';
var
  StatusList: TDataSet;
  RestrictionGroups: String;
begin
  Result := True;

  if LogicDM.CanI(RightTicketsAllowRestrictedStatusCodes, LoggedInEmpID)
    and LogicDM.CanI(RightTicketsAllowStatusCodes, LoggedInEmpID) then
    RestrictionGroups := ' sg.sg_name =''RestrictedStatusList'' or sg.sg_name = ''UnrestrictedStatusList'' and '
  else if LogicDM.CanI(RightTicketsAllowStatusCodes, LoggedInEmpID) then
    RestrictionGroups := ' sg.sg_name = ''UnrestrictedStatusList'' and '
  else if LogicDM.CanI(RightTicketsAllowRestrictedStatusCodes, LoggedInEmpID) then
    RestrictionGroups := ' sg.sg_name = ''RestrictedStatusList'' and '
  else begin//Revoked rights, no need to perform select
    Result := False;
    RestrictionGroups := '';
  end;

  if RestrictionGroups <> '' then begin
    StatusList := LogicDM.CreateDataSetForSQL(Format(SelectStatus, [RestrictionGroups]));
    try
      if HasRecords(StatusList) then
        Result := StatusList.Locate('status', Status, []);
    finally
      FreeAndNil(StatusList);
    end;
  end;
end;

procedure TTicketDM.OpenTicketTo(TicketID: Integer);
begin
  Assert(not Ticket.Active);
  Ticket.Parameters.ParamValues['id'] := TicketID;
  Ticket.Open;
end;




// TODO: This should be data driven instead of using hardcoded checks.
//MultiResponder MultiResponse, MultiQueue
function TTicketDM.GetLocateMultiResponse(const CallCenter, ClientCode: string): Boolean;
begin    //QM-168
  Result := False;

  {OHIO  QM-296 handled by python responder- Can remove the commented code below once
  determined to be fully functional}
//  if (CallCenter = 'OUPS1') or (CallCenter = 'OUPS2') then
//  begin  //QM-163  changed all codes
//        //new codes due to moving to polygons  --sr
//    if     ((Copy(ClientCode, 1, 4)) = 'CGEP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGRO')
//        or ((Copy(ClientCode, 1, 4)) = 'PIFP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGSP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGBP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGVP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGIP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGLP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGPP')
//        or ((Copy(ClientCode, 1, 4)) = 'PILP')
//        or ((Copy(ClientCode, 1, 4)) = 'CGCP')   //EB QMANTWO-468
//        or ((Copy(ClientCode, 1, 4)) = 'CPOP')   // QMANTWO-588
//        or ((Copy(ClientCode, 1, 4)) = 'CLLP')  //QM-75  SR
//        or ((Copy(ClientCode, 1, 4)) = 'CGEO')  //QM-168
//        or ((Copy(ClientCode, 1, 4)) = 'CGWP')  //QM-183  sr
//        or ((Copy(ClientCode, 1, 4)) = 'CGNP')  //QM-183  sr
//        or ((Copy(ClientCode, 1, 4)) = 'CGZP')  //QM-183  sr
//
//        //retained for old tickets    --sr
//        or ((Copy(ClientCode, 1, 3)) = 'CGE')
//        or ((Copy(ClientCode, 1, 3)) = 'PIF')
//        or ((Copy(ClientCode, 1, 3)) = 'CGR')
//        or ((Copy(ClientCode, 1, 3)) = 'CGS')
//        or ((Copy(ClientCode, 1, 3)) = 'CGB')
//        or ((Copy(ClientCode, 1, 3)) = 'CGV')
//        or ((Copy(ClientCode, 1, 3)) = 'CGI')
//        or ((Copy(ClientCode, 1, 3)) = 'CGL')
//        or ((Copy(ClientCode, 1, 3)) = 'CGP')
//        or ((Copy(ClientCode, 1, 3)) = 'PIL')
//        or ((Copy(ClientCode, 1, 3)) = 'CGC')
//        or ((Copy(ClientCode, 1, 3)) = 'CLL')  //QM-75  SR
//    then
//      Result := True;
//  end else
  if  (CallCenter = 'OCC2') then
  begin
// added as secondary request  -- 1/23/2017
     if ((Copy(ClientCode, 1, 6)) = 'CGV082')
        or ((Copy(ClientCode, 1, 6)) = 'CGV111')
        or ((Copy(ClientCode, 1, 6)) = 'CGV131')
        or ((Copy(ClientCode, 1, 6)) = 'CGV222')
        or ((Copy(ClientCode, 1, 6)) = 'CGV249')
        or ((Copy(ClientCode, 1, 6)) = 'CGV333')
        or ((Copy(ClientCode, 1, 6)) = 'CGV347')
        or ((Copy(ClientCode, 1, 6)) = 'CGV391')
        or ((Copy(ClientCode, 1, 6)) = 'CGV466')
        or ((Copy(ClientCode, 1, 6)) = 'CGV920')
        or ((Copy(ClientCode, 1, 6)) = 'CGV930')
    then
      Result := True;
  end
//  else      //QM-359  Transferred to back end multiresponder  SR
//  if  (CallCenter = '1851') then
//  begin
//// added as secondary request  -- 1/23/2017
//     if    ((Copy(ClientCode, 1, 6)) = 'NI0001')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0002')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0003')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0004')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0005')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0006')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0007')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0008')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0009')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0010')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0011')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0012')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0013')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0014')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0015')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0016')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0017')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0018')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0019')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0020')
//        or ((Copy(ClientCode, 1, 6)) = 'NI0021')
//        or ((Copy(ClientCode, 1, 6)) = 'ID8011')
//    then
//      Result := True;
//  end
  else if (CallCenter = 'FMW1') and (Copy(ClientCode, 1, 3) = 'PEP') then
    Result := True;
end;

function TTicketDM.InsertGPSPostion(const AddedBy: Integer; AddedDate: TDateTime; Latitude, Longitude: Double; HDOP: Integer): Integer;
const
  SQL = 'INSERT INTO gps_position (added_by_id, added_date, latitude, longitude, hdop_feet) '+
    'VALUES (:added_by_id, %s, :latitude, :longitude, :hdop_feet)';
begin
  if not GPSPositionExists(AddedBy, AddedDate, Latitude, Longitude, HDOP, Result) then begin
    InsertGPS.CommandText := Format(SQL, [IsoDateTimeToStrQuoted(AddedDate)]);
    InsertGPS.Parameters.ParamValues['added_by_id'] := AddedBy;
    InsertGPS.Parameters.ParamValues['latitude'] := Latitude;
    InsertGPS.Parameters.ParamValues['longitude'] := Longitude;
    InsertGPS.Parameters.ParamValues['hdop_feet'] := HDOP;
    InsertGPS.Execute;
    Result := LogicDM.GetAnyIdentity;
  end;
end;

function TTicketDM.GPSPositionExists(AddedBy: Integer; AddedDate: TDateTime; Lat, Lon: Double; HDOP: Integer; var ExistingGPSID: Integer): Boolean;
const
  Existing = 'select gps_id from gps_position where added_date = %s ' +
    'and added_by_id = :added_by_id and latitude = :latitude and longitude = :longitude ' +
    'and hdop_feet = :hdop_feet';
begin
  ExistingGPSID := -1;
  GPSPosition.Close;
  GPSPosition.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(AddedDate)]);
  GPSPosition.Parameters.ParamValues['added_by_id'] := AddedBy;
  GPSPosition.Parameters.ParamValues['latitude'] := Lat;
  GPSPosition.Parameters.ParamValues['longitude'] := Lon;
  GPSPosition.Parameters.ParamValues['hdop_feet'] := HDOP;
  GPSPosition.Open;

  if GPSPosition.IsEmpty then
    Result := False
  else begin
    Result := True;
    ExistingGPSID := GPSPosition.FieldByName('gps_id').AsInteger;
  end;
end;

function TTicketDM.GetClientIDForCode(const ClientCode, CallCenter: string): Integer;
begin
  FindClientCode.Parameters.ParamValues['code'] := ClientCode;
  FindClientCode.Parameters.ParamValues['center'] := CallCenter;
  FindClientCode.Open;
  if FindClientCode.EOF then
    raise Exception.CreateFmt('Could not find client_id for %s/%s', [CallCenter, ClientCode]);
  Result := FindClientCode.FieldByName('client_id').AsInteger;
  FindClientCode.Close;
end;



function TTicketDM.GetClientUtilType(const ClientCode, CallCenter: string): String;  //qm-300 sr
begin
  getUtilityType.Parameters.ParamValues['ClientCode'] := ClientCode;
  getUtilityType.Parameters.ParamValues['CallCenter'] := CallCenter;
  getUtilityType.Open;
  if getUtilityType.EOF then
    raise Exception.CreateFmt('Could not find utility type for %s/%s', [CallCenter, ClientCode]);
  Result := getUtilityType.FieldByName('utility_type').AsString;
  getUtilityType.Close;
end;

function TTicketDM.GetEPRHistory(TicketID:integer): string;   //QMANTWO-338 EB
begin
  try
//  EPRHistorybyTicket.ParamByName['ticket_id'].Value := TicketID;
  EPRHistorybyTicket.Parameters.ParamValues['ticket_id'] := TicketID;  //QM-977 Remove BetterADO EB
  EPRHistorybyTicket.Open;
  Result := ConvertDataSetsToSQLXML([EPRHistoryByTicket], ['epr_response_history']);
  finally
    if EPRHistoryByTicket.Active then EPRHistoryByTicket.Close;
  end;

end;

function TTicketDM.GetRiskHistory(TicketID:integer): string;   //QM-387 EB
begin
  try
  RiskScoreHistorybyTicket.Parameters.ParamValues['ticket_id']:= TicketID;  //QM-977 Remove BetterADO EB
  RiskScoreHistorybyTicket.Open;
  Result := ConvertDataSetsToSQLXML([RiskScoreHistorybyTicket], ['ticket_risk']);
  finally
    if RiskScoreHistorybyTicket.Active then RiskScoreHistorybyTicket.Close;
  end;

end;


function TTicketDM.NextNewTicketNumberID: Integer;
begin
  NextTicketID.Close;
  try
    NextTicketID.ExecProc;
    Result := NextTicketID.Parameters.ParamValues['@RETURN_VALUE'];
  finally
    NextTicketID.Close;
  end;
end;

function TTicketDM.NoteHasChanged2(NoteText: string): boolean;   //QM-188 EB
var
  i: integer;
begin
  Result := False;
  for i := 1 to 3 do
    if AnsiPos(NoteChangeCodes[i], NoteText) > 0 then
      Result := True;

end;

function TTicketDM.IsManualTicket(TicketID: Integer): Boolean;
begin
  OpenTicketTo(TicketID);
  try
    Assert(not Ticket.Eof);
    Result := StrContains(TicketStatusManual, Ticket.FieldByName('status').AsString);
  finally
    Ticket.Close;
  end;
end;

function TTicketDM.IsMeetTicket(const TicketID: Integer): Boolean;
begin
  Result := False;
  if TicketID < 0 then
    Exit;

  Assert(Ticket.Active, 'Ticket must be active in IsMeetTicket');
  Assert(Ticket.FieldByName('ticket_id').AsInteger = TicketID, 'Expecting ticket id ' + IntToStr(TicketID) +
    ' but got ticket id ' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger));

  Result := StrContains(TicketTypeMeet, Ticket.FieldByName('ticket_type').AsString);
end;

function TTicketDM.IsReleasePriority(const PriorityID: Integer): Boolean;
begin
  Result := (LogicDM.GetCodeByRefID(PriorityID) = 'Release');
end;

function TTicketDM.IsTicketAssigned(const TicketID, EmpID: Integer): Boolean;
begin
  Result := False;
  if TicketID <= 0 then
    Exit;

  LocatorOnTicket.Parameters.ParamValues['ticket_id'] := TicketID;
  LocatorOnTicket.Open;
  try
    Result := (EmpID = LocatorOnTicket.Fields[0].AsInteger);
  finally
    LocatorOnTicket.Close;
  end;
end;

function TTicketDM.LocateIDSearch(const LocateID: integer): string;
var
  TicketID: integer;
begin
  If GetTicketIDFromLocate.Active then
    GetTicketIDFromLocate.Close;
  //GetTicketIDFromLocate.ParamByName['LocateID'].Value := LocateID;
  GetTicketIDFromLocate.Parameters.ParamValues['LocateID'] := LocateID;  //QM-977 EB Remove BetterADO
  GetTicketIDFromLocate.Open;
  TicketID := GetTicketIDFromLocate.FieldByName('ticket_id').AsInteger;
  Result := TicketIDSearch(TicketID);
end;

function TTicketDM.InsertTicketNote(TicketID: Integer; EntryDate: TDateTime;      //qm-747  sr
  EnteredByUID: Integer; NoteText: string; SubType: Integer; ClientCode: string): Integer;
var
  ForeignType: Integer;
  ForeignID: Integer;
begin
  {EB - Default the Note subtype to 0 for older clients}
  if IsEmpty(ClientCode) then begin
    ForeignType := qmftTicket;
    ForeignID := TicketID;
  end else begin
    ForeignType := qmftLocate;
    ForeignID := FindLocateID(TicketID, ClientCode);
    Assert(ForeignID > 0, Format('Cannot find locate %s on Ticket ID %d to add a note.', [ClientCode, TicketID]));
  end;
//  EntryDate := Now;   //QM-865 EB Entry Date not being set (Strangely enough this does not show any changes and it works without it testing on mine}  //qm-920 eb/sr
  Result := GetNotesDM.AddNote(ForeignType, ForeignID, EntryDate, EnteredByUID, NoteText, SubType);
end;

function TTicketDM.FindLocateID(const TicketID: Integer; const ClientCode: string): Integer;
begin
  with FindLocateQuery do begin
    Parameters.ParamValues['ticket_id'] := TicketID;
    Parameters.ParamValues['client_code'] := ClientCode;
    Open;
    try
      Result := Fields[0].AsInteger;
    finally
      Close;
    end;
  end;
end;

function TTicketDM.InsertBillingQueue(EventType: string; TicketID: Integer;
  LocateID: Variant; EventDate: TDateTime; LocateHoursID, LocateStatusID: Variant): Integer;
begin
  if (EventType = BillingEventLocate)  then
    Assert((LocateId <> Null) and (LocateStatusID <> Null))
  else if (EventType = BillingEventTicket) then
    Assert((LocateId = Null) and (LocateStatusID = Null) and (LocateHoursID = Null))
  else if (EventType = BillingEventLabor)  then
    Assert((LocateId <> Null) and (LocateHoursID <> Null));

  BillingQueue.Parameters.ParamByName('event_type').Value := EventType;
  BillingQueue.Parameters.ParamByName('ticket_id').Value := TicketId;
  BillingQueue.Parameters.ParamByName('locate_id').Value := LocateId;
  BillingQueue.Parameters.ParamByName('event_date').Value := EventDate;
  BillingQueue.Parameters.ParamByName('locate_hours_id').Value := LocateHoursId;
  BillingQueue.Parameters.ParamByName('locate_status_id').Value := LocateStatusId;

  BillingQueue.Execute;
  Result := 0; // Use the resulting identity later
end;

procedure TTicketDM.InsertNotificationQueues(LocateID, LocateStatusID: Integer;
  const Status, CallCenter, State, ClientCode: string);
var
  SP: TADOStoredProc;
begin
  SP := InsertNotificationQueuesSP;
  SP.Parameters.ParamByName('@LocateID').Value := LocateID;
  SP.Parameters.ParamByName('@LsID').Value := LocateStatusID;
  SP.Parameters.ParamByName('@Status').Value := Status;
  SP.Parameters.ParamByName('@CallCenter').Value := CallCenter;
  SP.Parameters.ParamByName('@State').Value := State;
  SP.Parameters.ParamByName('@ClientCode').Value := ClientCode;
  SP.ExecProc;
end;


procedure TTicketDM.ProcessTicketAreaChanges(ChangeList: TicketAreaChangeList);
const
  SelectAreaName = 'select area_name from area where area_id=%d';
var
  Index: Integer;
  TicketArea: TicketAreaChange;
  TicketID: Integer;
  RouteAreaID: Integer;
  OldAreaID: Integer;
  RouteAreaName: string;
  Area: TDataSet;
  EntryDate: TDateTime;
  CloseTicketWhenDone: Boolean;
begin
  Assert(Assigned(ChangeList), 'ChangeList is uassigned in ProcessTicketAreaChanges');

  for Index := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Ticket Area %d of %d', [Index + 1, ChangeList.Count]));
    TicketArea := ChangeList[Index];
    TicketID := TicketArea.TicketID;
    RouteAreaID := TicketArea.RouteAreaID;
    EntryDate := TicketArea.AreaChangedDate;
    SetPhase('Comparing existing ticket');

    // When called from SaveTicketArea, Ticket won't be active & ChangeList can have changes for multiple tickets
    CloseTicketWhenDone := not Ticket.Active;
    if not Ticket.Active then
      OpenTicketTo(TicketID);

    try
      if Ticket.IsEmpty then
        raise Exception.CreateFmt('Cannot find requested ticket for area assignment. Ticket ID %d', [TicketID]);

      Assert(TicketID = Ticket.FieldByName('ticket_id').AsInteger,
        Format('Expected Ticket ID %d, but have Ticket ID %d', [TicketID,
        Ticket.FieldByName('ticket_id').AsInteger]));

      OldAreaID := Ticket.FieldByName('route_area_id').AsInteger;
      if (RouteAreaID > 0) and (OldAreaID <> RouteAreaID) then begin
        if not LogicDM.CanI(RightTicketsAssignArea) then
          raise EOdNotAllowed.Create('You do not have ticket area assignment permissions.');

        Area := LogicDM.GetDataSetForSQL(Format(SelectAreaName, [RouteAreaID]));
        try
          if Area.IsEmpty then
            raise Exception.CreateFmt('Cannot find requested area for area assignment. Area ID %d', [RouteAreaID]);
          RouteAreaName := Area.FieldByName('area_name').AsString;
        finally
          Area.Close;
        end;
      end
      else
        RouteAreaName := 'Unassigned';

      if OldAreaID <> RouteAreaID then begin
       SetPhase('Editing existing ticket');
        Ticket.Edit;
        if RouteAreaID > 0 then
          Ticket.FieldByName('route_area_id').AsInteger := RouteAreaID
        else
          Ticket.FieldByName('route_area_id').Clear;
        Ticket.Post;
        InsertTicketNote(TicketID, EntryDate, LogicDM.LoggedInUID, Format(NoteAreaChanged + ' %s by %s ' + ' (Employee ID %d)',
          [RouteAreaName, LogicDM.LoggedInEmpShortName, LogicDM.LoggedInEmpID]), PublicTicketSubType);  //100 is Standard Ticket subtype
      end;
    finally
      if CloseTicketWhenDone then
        Ticket.Close;
    end;
  end;
end;

procedure TTicketDM.ProcessLocatePlatChanges(ChangeList: LocatePlatChangeList; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from locate_plat where locate_id = :locate_id' +
    ' and plat = :plat and active=1';
  Updating = 'update locate_plat set plat=%s, modified_date=getdate(),' +
    ' modified_by=%d, active=%d where locate_plat_id=%d and active=1';
var
  LocatePlat: LocatePlatChange;
  Index: Integer;
  LocatePlatID: Integer;
  LocateID: Integer;
  NewLocatePlatID: Integer;
  ActiveFlag: Boolean;
  Plat: string;
  D: TADODataset;
  InsertMode: Boolean;

  procedure UpdateLocatePlat;
  begin
    SetPhase('Editing existing locate plat');
    LogicDM.ExecuteSQL(Format(Updating, [QuotedStr(Plat), LogicDM.LoggedInEmpID,
      Integer(ActiveFlag), LocatePlatID]));
  end;

begin
  NewLocatePlatID := -1;
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessLocatePlatChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessLocatePlatChanges');

  D := LocatePlatData;
  for Index := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Locate Plat %d of %d', [Index + 1, ChangeList.Count]));
    LocatePlat := ChangeList[Index];
    LocateID := LocatePlat.LocateID;
    Plat := LocatePlat.Plat;
    LocatePlatID := LocatePlat.LocatePlatID;
    ActiveFlag := LocatePlat.Active;

    InsertMode := (LocatePlatID < 0);
    D.Close;
    if InsertMode then begin
      D.CommandText := Existing;
      D.Parameters.ParamValues['locate_id'] := LocateID;
      D.Parameters.ParamValues['plat'] := Plat;
      D.Open;
      if D.RecordCount = 0 then begin
        SetPhase('Editing locate plat');
        D.Insert;
        D.FieldByName('locate_id').Value := LocateID;
        D.FieldByName('plat').Value := Plat;
        D.FieldByName('modified_by').Value := LogicDM.LoggedInEmpID;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpID;
        D.Post;
      end
      else if D.RecordCount > 1 then
        raise Exception.Create('Multiple existing plats match new plat save data');
      NewLocatePlatID := D.FieldByName('locate_plat_id').AsInteger;
    end
    else
      UpdateLocatePlat;

    if LocatePlatID < 0 then begin
      Assert(NewLocatePlatID > 0, 'Negative new locate plat ID');
      AddIdentity(NewKeys, 'locate_plat', LocatePlatID, NewLocatePlatID);
    end;
  end;
end;

procedure TTicketDM.ProcessTicketInfoChanges(ChangeList: TicketInfoChangeList; var NewKeys: GeneratedKeyList);
var
  TicketInfo: TicketInfoChange;
  TicketInfoID: Integer;
  TicketID: Integer;
  InfoType: string;
  Info: string;
  ModifiedDate: TDateTime;
  i: Integer;
  NewTicketInfoID: Integer;
  D: TADODataset;
begin

  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessTicketInfoChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessTicketInfoChanges');

  for i := 0 to ChangeList.Count - 1 do begin
    TicketInfo := ChangeList[i];
    Assert(Assigned(TicketInfo));
    TicketInfoID := TicketInfo.TicketInfoID;
    TicketID := TicketInfo.TicketID;
    InfoType := TicketInfo.InfoType;
    Info := TicketInfo.Info;
    ModifiedDate := TicketInfo.ModifiedDate;

    if TicketInfoID > 0 then
      raise Exception.Create('Editing existing ticket info records is not yet supported');
    if TicketID < 0 then
      raise Exception.Create('Negative ticket ID numbers are not allowed in ProcessTicketInfoChanges');
//    Assert(InfoType = ONGOING);  // More values may come later
    Assert(NotEmpty(Info), 'Ticket_Info is Empty');

    D := TicketInfoData;
    D.Close;
    D.Parameters.ParamValues['AddedBy'] := LogicDM.LoggedInEmpID;
    D.Parameters.ParamValues['TicketID'] := TicketID;
    D.Parameters.ParamValues['InfoType'] := InfoType;
    D.Parameters.ParamValues['Info'] := Info;
    D.Parameters.ParamValues['ModifiedDate'] := ModifiedDate; // This is not duplicate-safe due to timestamp rounding, time zones, etc.
    D.Open;

    // Don't add a duplicate row for a failed sync, etc.
    if D.Eof then begin
      D.Insert;
      D.FieldByName('ticket_id').AsInteger := TicketID;
      D.FieldByName('info_type').AsString := InfoType;
      D.FieldByName('info').AsString := Info;
      D.FieldByName('added_by').AsInteger := LogicDM.LoggedInEmpID;
      D.Post; // modified_date has a default of getdate()
    end;

    // For both existing rows and new inserts:
    NewTicketInfoID := D.FieldByName('ticket_info_id').AsInteger;
    Assert(NewTicketInfoID > 0);
    AddIdentity(NewKeys, 'ticket_info', TicketInfoID, NewTicketInfoID);
  end;
end;

procedure TTicketDM.ProcessLocateFacilityChanges(ChangeList: LocateFacilityChangeList; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from locate_facility ';
  Updating = 'update locate_facility set offset = :offset, active = :active, ' +
    'modified_by = :modified_by, deleted_by = :deleted_by, deleted_date = :deleted_date ' +
    'where locate_facility_id = :locate_facility_id';
  DetailedDateTime = 'mm/dd/yy hh:mm:ss:z';
var
  i: Integer;
  LocateFacility: LocateFacilityChange;
  LocateFacilityExtended: LocateFacilityChange2;
  LocateFacilityID: Integer;
  LocateID: Integer;
  AddedBy: Integer;
  AddedDate: TDateTime;
  Offset: Variant;
  FacType: string;
  FacSize: Variant;
  FacMaterial: Variant;
  FacPressure: Variant;
  ActiveFlag: Boolean;
  InsertMode: Boolean;
  NewID: Integer;
  D: TADODataset;
  SQL: TOdSqlBuilder;
  Inserted: Boolean;
  ChangeDate: TDatetime;
begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessLocateFacilityChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessTicketInfoChanges');

  NewID := -1;
  D := LocateFacilityData;
  for i := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Locate Facility %d of %d', [i + 1, ChangeList.Count]));
    LocateFacility := ChangeList[i];
    Assert(Assigned(LocateFacility));
    LocateFacilityID := LocateFacility.LocateFacilityID;
    LocateID := LocateFacility.LocateID;
    if LocateFacility.Offset = 0 then
      Offset := Null
    else
      Offset := LocateFacility.Offset;
    FacType := LocateFacility.FacilityType;
    if IsEmpty(LocateFacility.FacilitySize) then
      FacSize := Null
    else
      FacSize := LocateFacility.FacilitySize;
    if IsEmpty(LocateFacility.FacilityMaterial) then
      FacMaterial := Null
    else
      FacMaterial := LocateFacility.FacilityMaterial;
    if IsEmpty(LocateFacility.FacilityPressure) then
      FacPressure := Null
    else
      FacPressure := LocateFacility.FacilityPressure;
    AddedDate := LocateFacility.AddedDate;
    AddedBy := LocateFacility.AddedBy;
    ActiveFlag := LocateFacility.Active;
    D.Close;
    InsertMode := (LocateFacilityID < 0);
    Inserted := False;
    if InsertMode then begin
      Op.Detail := 'Adding new locate facility';
      SQL := TOdSqlBuilder.Create;
      try
        SQL.AddIntFilterRequirePositive('locate_id', LocateID);
        SQL.AddExactStringFilterNonEmpty('facility_type', FacType);
        SQL.AddExactStringFilterAllowNull('facility_material', FacMaterial);
        SQL.AddExactStringFilterAllowNull('facility_size', FacSize);
        SQL.AddExactStringFilterAllowNull('facility_pressure', FacPressure);
        SQL.AddIntFilterRequirePositive('added_by', AddedBy);
        SQL.AddISODateFilterNonNull('added_date', AddedDate);
        SQL.AddBooleanSearch('active', ActiveFlag);
        D.CommandText := Existing + SQL.WhereClause;
        D.Open;
      finally
        FreeAndNil(SQL);
      end;
      if D.RecordCount = 1 then begin
        D.Edit;
        // This can be removed once the duplicate row bug is confirmed fixed
        Op.LogFmt('Warning: Attempt to edit or save duplicate locate_facility.  OldID: %d New ID: %d OldAddedDate: %s NewAddedDate: %s LocateID: %d EmpID: %d AddedBy: %d',
          [D.FieldByName('locate_facility_id').AsInteger, LocateFacilityID, FormatDateTime(DetailedDateTime, D.FieldByName('added_date').Value), FormatDateTime(DetailedDateTime, AddedDate),
           LocateID, LogicDM.LoggedInEmpID, AddedBy]);
      end
      else if D.RecordCount = 0 then begin
        D.Insert;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpID;
        D.FieldByName('added_date').Value := AddedDate;
        Inserted := True;
      end else
        raise Exception.Create('Multiple existing locate facilities match new facility save data');

      D.FieldByName('locate_id').AsInteger := LocateID;
      D.FieldByName('facility_type').Value := FacType;
      D.FieldByName('facility_size').Value := FacSize;
      D.FieldByName('facility_material').Value := FacMaterial;
      D.FieldByName('facility_pressure').Value := FacPressure;
      D.FieldByName('offset').Value := Offset;
      D.FieldByName('active').Value := ActiveFlag;
      D.Post;
      NewID := D.FieldByName('locate_facility_id').AsInteger;
      if Inserted then
        LogicDM.UpdateDateTimeToIncludeMilliseconds('locate_facility', 'locate_facility_id', NewID, 'added_date', AddedDate);
    end
    {Elana - Update (mostly deletes) - This is a temporary fix (need to review all
            items in the ticket info being passed and see if we can pass it separately
            with rollback instead of bundling it}
    else begin
      Op.Detail := 'Editing existing locate facility';
      //Determine if this is a valid delete
      if not LocateFacility.Active then begin
        ChangeDate := Now;
        LocateFacilityExtended := LocateFacilityChange2.Create;
        try
        LocateFacilityExtended.LocateFacilityID := LocateFacility.LocateFacilityID;
        LocateFacilityExtended.LocateID := LocateFacility.LocateID;
        LocateFacilityExtended.Offset := LocateFacility.Offset;
        LocateFacilityExtended.FacilityType := LocateFacility.FacilityType;
        LocateFacilityExtended.FacilityMaterial := LocateFacility.FacilityMaterial;
        LocateFacilityExtended.FacilitySize := LocateFacility.FacilitySize;
        LocateFacilityExtended.FacilityPressure := LocateFacility.FacilityPressure;
        LocateFacilityExtended.AddedDate := LocateFacility.AddedDate;
        LocateFacilityExtended.AddedBy := LocateFacility.AddedBy;
        LocateFacilityExtended.Active := LocateFacility.Active;
        {Extended Fields}
        LocateFacilityExtended.ModifiedBy :=  LogicDM.LoggedInEmpID;
        LocateFacilityExtended.ModifiedDate := ChangeDate;
        LocateFacilityExtended.DeletedBy := LogicDM.LoggedInEmpId;
        LocateFacilityExtended.DeletedDate := ChangeDate;
        ExecuteSQLWithParams(LogicDM.Conn, Updating, LocateFacilityExtended);
        finally
          FreeAndNil(LocateFacilityExtended);
        end;
      end
      else
        ExecuteSQLWithParams(LogicDM.Conn, Updating, LocateFacility);
    end;
    if LocateFacilityID < 0 then begin
      Assert(NewID > 0, 'Negative new Locate Facility ID');
      AddIdentity(NewKeys, 'locate_facility', LocateFacilityID, NewID);
    end;
  end;
end;



procedure TTicketDM.ProcessLocateFacilityChanges(
  ChangeList: LocateFacilityChangeList2; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from locate_facility ';
  Updating = 'update locate_facility set offset = :offset, active = :active, ' +
    'modified_by = :modified_by, deleted_by = :deleted_by, deleted_date = :deleted_date ' +
    'where locate_facility_id = :locate_facility_id';
  DetailedDateTime = 'mm/dd/yy hh:mm:ss:z';
var
  i: Integer;
  LocateFacility: LocateFacilityChange2;
  LocateFacilityID: Integer;
  LocateID: Integer;
  AddedBy: Integer;
  AddedDate: TDateTime;
  Offset: Variant;
  FacType: string;
  FacSize: Variant;
  FacMaterial: Variant;
  FacPressure: Variant;
  ActiveFlag: Boolean;
  InsertMode: Boolean;
  NewID: Integer;
  D: TADODataset;
  SQL: TOdSqlBuilder;
  Inserted: Boolean;
begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessLocateFacilityChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessTicketInfoChanges');

  NewID := -1;
  D := LocateFacilityData;
  for i := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Locate Facility %d of %d', [i + 1, ChangeList.Count]));
    LocateFacility := ChangeList[i];
    Assert(Assigned(LocateFacility));
    LocateFacilityID := LocateFacility.LocateFacilityID;
    LocateID := LocateFacility.LocateID;
    if LocateFacility.Offset = 0 then
      Offset := Null
    else
      Offset := LocateFacility.Offset;
    FacType := LocateFacility.FacilityType;
    if IsEmpty(LocateFacility.FacilitySize) then
      FacSize := Null
    else
      FacSize := LocateFacility.FacilitySize;
    if IsEmpty(LocateFacility.FacilityMaterial) then
      FacMaterial := Null
    else
      FacMaterial := LocateFacility.FacilityMaterial;
    if IsEmpty(LocateFacility.FacilityPressure) then
      FacPressure := Null
    else
      FacPressure := LocateFacility.FacilityPressure;
    AddedDate := LocateFacility.AddedDate;
    AddedBy := LocateFacility.AddedBy;
    ActiveFlag := LocateFacility.Active;
    D.Close;
    InsertMode := (LocateFacilityID < 0);
    Inserted := False;
    if InsertMode then begin
      Op.Detail := 'Adding new locate facility';
      SQL := TOdSqlBuilder.Create;
      try
        SQL.AddIntFilterRequirePositive('locate_id', LocateID);
        SQL.AddExactStringFilterNonEmpty('facility_type', FacType);
        SQL.AddExactStringFilterAllowNull('facility_material', FacMaterial);
        SQL.AddExactStringFilterAllowNull('facility_size', FacSize);
        SQL.AddExactStringFilterAllowNull('facility_pressure', FacPressure);
        SQL.AddIntFilterRequirePositive('added_by', AddedBy);
        SQL.AddISODateFilterNonNull('added_date', AddedDate);
        SQL.AddBooleanSearch('active', ActiveFlag);
        D.CommandText := Existing + SQL.WhereClause;
        D.Open;
      finally
        FreeAndNil(SQL);
      end;
      if D.RecordCount = 1 then begin
        D.Edit;
        // This can be removed once the duplicate row bug is confirmed fixed
        Op.LogFmt('Warning: Attempt to edit or save duplicate locate_facility.  OldID: %d New ID: %d OldAddedDate: %s NewAddedDate: %s LocateID: %d EmpID: %d AddedBy: %d',
          [D.FieldByName('locate_facility_id').AsInteger, LocateFacilityID, FormatDateTime(DetailedDateTime, D.FieldByName('added_date').Value), FormatDateTime(DetailedDateTime, AddedDate),
           LocateID, LogicDM.LoggedInEmpID, AddedBy]);
      end
      else if D.RecordCount = 0 then begin
        D.Insert;
        D.FieldByName('added_by').Value := LogicDM.LoggedInEmpID;
        D.FieldByName('added_date').Value := AddedDate;
        Inserted := True;
      end else
        raise Exception.Create('Multiple existing locate facilities match new facility save data');

      D.FieldByName('locate_id').AsInteger := LocateID;
      D.FieldByName('facility_type').Value := FacType;
      D.FieldByName('facility_size').Value := FacSize;
      D.FieldByName('facility_material').Value := FacMaterial;
      D.FieldByName('facility_pressure').Value := FacPressure;
      D.FieldByName('offset').Value := Offset;
      D.FieldByName('active').Value := ActiveFlag;
      D.Post;
      NewID := D.FieldByName('locate_facility_id').AsInteger;
      if Inserted then
        LogicDM.UpdateDateTimeToIncludeMilliseconds('locate_facility', 'locate_facility_id', NewID, 'added_date', AddedDate);
    end else begin
      Op.Detail := 'Editing existing locate facility';
      ExecuteSQLWithParams(LogicDM.Conn, Updating, LocateFacility);
    end;
    if LocateFacilityID < 0 then begin
      Assert(NewID > 0, 'Negative new Locate Facility ID');
      AddIdentity(NewKeys, 'locate_facility', LocateFacilityID, NewID);
    end;
  end;

end;

procedure TTicketDM.CreateFollowupTicket(const TicketID, FollowupTypeID: Integer;
  const FollowupDueDate, FollowupTransmitDate: TDateTime);
begin
  Op.Detail := 'Validating followup ticket permissions and fields';

  if not LogicDM.CanI(RightTicketsCreateFollowUp) then
    raise EOdNotAllowed.Create('You do not have permission to create follow up tickets.');

  if FollowupTransmitDate <= 0 then
    raise Exception.Create('The followup transmit date cannot be ' +
      'blank when creating a followup ticket.');
  if FollowupDueDate <= 0 then
    raise Exception.Create('The followup due date cannot be blank ' +
      'when creating a followup ticket.');
  if not (CompareDateTime(FollowupDueDate, FollowupTransmitDate) = GreaterThanValue) then
    raise Exception.Create('The followup due date/time must be later ' +
      'than the followup transmit date/time.');

  Op.Detail := 'Executing sp to create followup ticket';
  MakeFollowupTicket.Parameters.ParamValues['@TransmitDate'] := FollowupTransmitDate;
  MakeFollowupTicket.Parameters.ParamValues['@DueDate'] := FollowupDueDate;
  MakeFollowupTicket.Parameters.ParamValues['@TicketID'] := TicketID;
  MakeFollowupTicket.Parameters.ParamValues['@FollowupTypeID'] := FollowupTypeID;
  MakeFollowupTicket.ExecProc;
  Op.Detail := 'Done creating followup ticket.';
end;

destructor TTicketDM.Destroy;
begin
  FreeAndNil(AddinInfoDM);
  FreeAndNil(NotesDM);
  FreeAndNil(ArrivalDM);
  inherited;
end;

procedure TTicketDM.MoveLocates(const Locates: LocateList; const NewlocatorID: Integer);
var
  LocateID: Integer;
  LastTicketID: Integer;
  I: Integer;
begin
  LastTicketID := -999;
  for I := 0 to Locates.Count-1 do begin
    if LastTicketID <> Locates[I].TicketID then begin
      LastTicketID := Locates[I].TicketID;

      Ticket.Close;
      Ticket.Parameters.ParamValues['id'] := LastTicketID;
      Ticket.Open;

      Locate.Close;
      Locate.Parameters.ParamValues['id'] := LastTicketID;
      Locate.Open;
    end;

    if Locates[I].ClientCode = '' then
      raise Exception.Create('A client code must be specified');

    if Locate.Locate('client_code', Locates[I].ClientCode, []) then begin
      // Move all of them, if there are dups.  We hate dups.
      while (Locate.FieldByName('client_code').AsString = Locates[I].ClientCode) and not Locate.EOF do begin
        LocateID := Locate.FieldByName('locate_id').AsInteger;
        UpdateAssignments.Parameters.ParamValues['@LocateID'] := LocateID;
        UpdateAssignments.Parameters.ParamValues['@LocatorID'] := NewLocatorID;
        UpdateAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
        UpdateAssignments.ExecProc;
        Locate.Next;
      end;
    end else
      raise Exception.Create('Could not find locate ' + Locates[I].ClientCode + ' on this ticket.');
  end;
end;

Procedure TTicketDM.AssignProjLocate(LocateID, AssignedToID: integer);
//QMANTWO-616 EB Project Tickets Assign locates
begin
    UpdateAssignments.Parameters.ParamValues['@LocateID'] := LocateID;
    UpdateAssignments.Parameters.ParamValues['@LocatorID'] := AssignedToID;
    UpdateAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
    UpdateAssignments.ExecProc;

end;



procedure TTicketDM.MoveTickets(const Tickets: TicketList; const FromLocatorID, ToLocatorID: Integer);
var
  I, LocateID: Integer;
begin
  for I := 0 to Tickets.Count-1 do begin
    Assert(Assigned(Tickets[I]));
    Ticket.Close;
    Ticket.Parameters.ParamValues['id'] := Tickets[I].TicketID;
    Ticket.Open;

    Locate.Parameters.ParamValues['id'] := Tickets[I].TicketID;
    Locate.Open;
    while not Locate.EOF do begin
      // FromLocatorID is -1 when reassigning from the emergency bucket, and translates into "reassign all locates"
      if not Locate.FieldByName('closed').AsBoolean and
         ((Locate.FieldByName('assigned_to').AsInteger = FromLocatorID)
           or (FromLocatorID = -1) or (FromLocatorID = -3) or (FromLocatorID = -4)
           or (FromLocatorID = -5) or (FromLocatorID = -6)) and  //QM-133, QM-318, QM-428, QM-486 EB Past Due, Today, Open, Virtual Bucket
         (Locate.FieldByName('status').AsString <> NotAClientStatus) and
         (Locate.FieldByName('status').AsString <> ParsingStatus) then begin

        LocateID := Locate.FieldByName('locate_id').AsInteger;

        UpdateAssignments.Parameters.ParamValues['@LocateID'] := LocateID;
        UpdateAssignments.Parameters.ParamValues['@LocatorID'] := ToLocatorID;
        UpdateAssignments.Parameters.ParamValues['@AddedBy'] := LogicDM.LoggedInEmpID;
        UpdateAssignments.ExecProc;
      end;
      Locate.Next;
    end;
    Locate.Close;
  end;
end;

procedure TTicketDM.SaveTicketDueDate(TicketID: integer; NewDueDate: TDateTime);  //QM-216 Ticket Due Date
const
  SQL = 'update ticket set ticket_type = ''%s'', due_date = %s where ticket_id = %d';
  RS = '-RS';
var
  RecsAffected: Integer;
  UpdateSQL: string;
  RSTicketType: string;
begin
  OpenTicketTo(TicketID);
  RSTicketType := Ticket.FieldByName('ticket_type').AsString;
  if ansipos(RS, RSTicketType) <= 0 then
    RSTicketType := RSTicketType + RS;
  if TicketID > 0 then
    UpdateSQL := Format(SQL,[RSTicketType, (IsoDateTimeToStrQuoted(NewDueDate)), TicketID]);

  LogicDM.Conn.Execute(UpdateSQL, RecsAffected);
  Assert(RecsAffected = 1, 'The ticket''s due date could not be changed in SaveTicketDueDate.');
end;

function TTicketDM.BulkChangeTicketDueDates(TicketIDs: string; NewDueDate: TDateTime): string;
const
  SQLW = '(ticket_id in (%s)) ';
  {Fix multiple ticket IDs returning because of locate}
  ValidSQL = 'Select distinct t.ticket_id ' +
             'from ticket t ' +
               'inner join [locate] l on (t.ticket_id = l.ticket_id) ' +
               'inner join client cl on (l.client_id = cl.client_id) ' +
               'inner join status_group sg on (sg.sg_name like ''EditTicketDueDate-'' + t.ticket_format) ' +
              // 'inner join status_group_item sgi on (sg.sg_id = sgi.sg_id) ' +
             'where (sg.active = 1) and ' +
                   '(t.ticket_id in (%s)) ' +
             'order by t.ticket_id ';

  SQLLocate = 'update locate set status = ''ORS'' ' +
              'where (closed = 0) and (ticket_id in (%s)) ';
 //             'and status <> ''ORS'' ';
  SQLDD = 'update ticket set due_date = %s where (ticket_id in (%s))';
  SQLTT = 'update ticket set ticket_type = ticket_type + ''-RS'' ' +
          'where (ticket_id in (%s)) ';
  SQLTTcont = ' and (ticket_type not like ''%-RS'') ';

  RS = '-RS';
  sc = ';';
var
  i, RecsAffected: Integer;
  FilterSQL, UpdateSQL: string;
  UptTktList: TStringList;
  QryDS, UpDateDS: TDataSet;
  DateStr: string;
  {For notes}
  lNoteDate: TDateTime;
  lTicketID: integer;
  lNoteText, lEmployeeName, lDisplayDueDateStr: string;
  DebugSQL : string;
begin
  RecsAffected := 0;
  Result := 'Incoming Tickets: ' + TicketIDs + sc;
  Result := 'New Due Date: ' + DateToStr(NewDueDate) + sc;
  UptTktList := TStringList.Create;
  UptTktList.StrictDelimiter := True;
  UptTktList.Delimiter := ',';
  DateStr := IsoDateTimeToStrQuoted(NewDueDate);
  lDisplayDueDateStr := IsoDateToStr(NewDueDate);  {Neater display}
  try
    try
      FilterSQL := Format(ValidSQL, [TicketIDs]);
      QryDS := LogicDM.GetDataSetForSQL(FilterSQL);
      QryDS.First;

      {Validate incoming tickets to make sure that you can update them}
      while not QryDS.EOF do begin
        {Add valid tickets to the UptTktList}
        UptTktList.Add(QryDS.FieldByName('ticket_id').asString);
        QryDS.Next;
      end;

      {Update Locates -  status = ORS}
      RecsAffected := 0;
      UpdateSQL := Format(SQLLocate, [UptTktList.DelimitedText]);
      RecsAffected := LogicDM.ExecuteSQL(UpdateSQL);
         if RecsAffected < 1 then
           Result := Result + 'No Locates were updated' + sc
         else
           Result := IntToStr(RecsAffected) + ' Locates were updated.';


      {Update Tickets - Due Date}
      RecsAffected := 0;
      Result := 'New Due Date: ' + DateTimeToStr(NewDueDate) + sc;
      Result := Result + 'Tickets that can be Updated: ' + intToStr(UptTktList.Count) + sc;
      UpdateSQL := Format(SQLDD, [DateStr, UptTktList.DelimitedText]);
      DebugSQL := UpdateSQL;
      RecsAffected := LogicDM.ExecuteSQL(UpdateSQL);
         if RecsAffected > 0 then begin
           Result := Result + ' ' + 'Tickets Changed: ' + IntToStr(RecsAffected) + sc;
           Result := Result + '  ' + UptTktList.DelimitedText + sc;
           Result := Result + DashLineSep + sc;
           Result := Result + ' DONE: Changes made on: ' + DateToStr(Now) + ' Server time' + sc;
           DebugSQL := Result + sc + UpdateSQL;
          end
          else
            DebugSQL := DebugSQL + 'No Due Dates could be updated.' + sc;

    {Update Tickets - Ticket Type = -RS}
      RecsAffected := 0;
      UpdateSQL := '';
      UpdateSQL := format(SQLTT, [UptTktList.DelimitedText]) + SQLTTCont;
      DebugSQL := UpdateSQL;
      Result := Result + sc;
      DebugSQL := DebugSQL + sc + UpdateSQL;
      RecsAffected := LogicDM.ExecuteSQL(UpdateSQL);

    {Add Note}
      lNoteDate := Now;
      for i := 0 to UptTktList.Count - 1 do begin
        lTicketID := StrToInt(UptTktList.Strings[i]);
        lEmployeeName := LogicDM.LoggedInEmpShortName;
        lNoteText := lEmployeeName + format((BulkModDueDateTxt1 + BulkModDueDateTxt2), [lDisplayDueDateStr]);
        GetNotesDM.AddNote(qmftTicket, lTicketID, lNoteDate, LogicDM.LoggedInUID, lNoteText, PrivateTicketSubType);
      end;
    except
      Result := 'Error: ' + DebugSQL;
    end;
  finally
  //  QryDS.Close; //  FreeAndNil(QryDS);   {Do not close or Free}
    FreeAndNil(UptTktList);
  end;
end;

procedure TTicketDM.SaveTicketType(TicketID: integer; TicketType: string);   {QM-604 EB Nipsco}
{This is a generic Ticket Type Save - takes exactly what is presented}
const
  SQL = 'update ticket set ticket_type = ''%s'' where ticket_id = %d';
var
  RecsAffected: Integer;
  UpdateSQL: string;
begin
  OpenTicketTo(TicketID);
    if (TicketID > 0) then
      UpdateSQL := Format(SQL,[TicketType, TicketID]);

    LogicDM.Conn.Execute(UpdateSQL, RecsAffected);
    Assert(RecsAffected = 1, 'The ticket''s ticket type could not be changed.');
end;



procedure TTicketDM.SaveTicketTypeHP(TicketID: integer; NewTicketType: string);   //qm-373 SR
const
  SQL = 'update ticket set ticket_type = ''%s'' where ticket_id = %d';
  HP = '-HP';
var
  RecsAffected: Integer;
  UpdateSQL: string;
  HPTicketType: string;
begin
   //qm-373 SR
  OpenTicketTo(TicketID);

  {Default UpdateSQL}
  if  (TicketID > 0) then
      UpdateSQL := Format(SQL,[NewTicketType, TicketID]);   //QM-1076 HP Restriction

  {If it is an HP ticket}
  if (ansipos(HP, NewTicketType) > 0) then //Has HP
  begin
    HPTicketType := Ticket.FieldByName('ticket_type').AsString;
    if (ansipos(HP, HPTicketType) <= 0) and (TicketID > 0) then
      UpdateSQL := Format(SQL,[NewTicketType, TicketID]);
  end;

  LogicDM.Conn.Execute(UpdateSQL, RecsAffected);
//  Assert(RecsAffected = 1, 'The ticket''s due date could not be changed in SaveTicketDueDate.');
end;

procedure TTicketDM.SaveTicketPriority(const TicketID: Integer;
  const EntryDate: TDateTime; const Priority: Integer);
const
  SelectExisting = 'select coalesce(work_priority_id, -1) work_priority_id from ticket where ticket_id = ';

var
  OldPriority: Integer;
  Tick: TDataSet;
  PriorityName: string;
begin
  if not LogicDM.CanI(RightTicketsReleaseForWork) then
    if IsReleasePriority(Priority) then
      raise EOdNotAllowed.Create('You do not have permission to release tickets for work.');
  Tick := LogicDM.CreateDataSetForSQL(SelectExisting + IntToStr(TicketID));
  try
    OldPriority := Tick.FieldByName('work_priority_id').AsInteger;
    if OldPriority <> Priority then begin
      SetPhase('Editing ticket priority');
      SetTicketPriority(TicketID, Priority);
      PriorityName := GetPriorityNameFromTicket(TicketID);
      {Note for Work Priority Change}
      InsertTicketNote(TicketID, EntryDate, LogicDM.LoggedInUID, Format('Work priority set to %s by %s (Employee ID %d)',
        [PriorityName, LogicDM.LoggedInEmpShortName, LogicDM.LoggedInEmpID]), PrivateTicketSubType); //EB Make change of note priority private
    end;
  finally
    FreeAndNil(Tick);
  end;
end;

procedure TTicketDM.ProcessTaskSchedule(ChangeList: TaskScheduleChangeList; var NewKeys: GeneratedKeyList);
const
  Existing = 'select * from task_schedule where emp_id = :emp_id ' +
   'and ticket_id = :ticket_id and est_start_date = %s ' +
   'and added_by = :added_by and active = :active and added_date = %s';
var
  i: Integer;
  Change: TaskScheduleChange;
  TaskScheduleID: Integer;
  EmpID: Integer;
  TicketID: Integer;
  EstStartDate: TDateTime;
  PerformedDate: Variant;
  AddedDate: TDateTime;
  AddedBy: Integer;
  Active: Boolean;
  NewTaskScheduleID: Integer;
  D: TADODataSet;
begin
  Assert(Assigned(ChangeList), 'ChangeList is unassigned in ProcessTaskSchedule');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessTaskSchedule');

  D := TaskSchedule;
  for i := 0 to ChangeList.Count - 1 do begin
    SetPhase(Format('Task Schedule %d of %d', [i + 1, ChangeList.Count]));
    Change := ChangeList[i];
    Assert(Assigned(Change));

    TaskScheduleID := Change.TaskScheduleID;
    EmpID := Change.EmpID;
    TicketID := Change.TicketID;
    EstStartDate := Change.EstStartDate;
    AddedBy := Change.AddedBy;
    AddedDate := Change.AddedDate;
    Active := Change.Active;
    if Change.PerformedDate = 0 then
      PerformedDate := Null
    else
      PerformedDate := Change.PerformedDate;

    Op.Detail := 'Check for existing TaskSchedule';
    D.Close;
    D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(EstStartDate), IsoDateTimeToStrQuoted(AddedDate)]);
    AssignParamsFromROComplex(D.Parameters, Change);
    D.Open;

    if D.RecordCount > 1 then
      raise Exception.Create('More than one task_schedule row matches the selection criteria.');

    if D.RecordCount = 1 then begin //allow the performed_date column to be updated
      Op.Detail := 'Updating task_schedule.performed_date';
      if (D.FieldByName('performed_date').IsNull) and (not VarIsNull(PerformedDate)) then begin
        D.Edit;
        D.FieldByName('performed_date').Value := PerformedDate;
        D.Post;
      end;
    end else begin
      Op.Detail := 'Adding new TaskSchedule';
      D.Insert;
      D.FieldByName('emp_id').AsInteger := EmpID;
      D.FieldByName('ticket_id').AsInteger := TicketID;
      D.FieldByName('est_start_date').AsDateTime := EstStartDate;
      D.FieldByName('added_by').AsInteger := AddedBy;
      D.FieldByName('added_date').AsDateTime := AddedDate;
      D.FieldByName('active').AsBoolean := Active and IsTicketAssigned(TicketID, EmpID);
      D.FieldByName('performed_date').Value := PerformedDate;
      D.Post;
    end;

    if TaskScheduleID < 0 then begin
      NewTaskScheduleID := D.FieldByName('task_schedule_id').AsInteger;
      Assert(NewTaskScheduleID > 0, 'Negative new task_schedule ID');
      LogicDM.UpdateDateTimeToIncludeMilliseconds('task_schedule', 'task_schedule_id', NewTaskScheduleID, 'est_start_date', EstStartDate);
      LogicDM.UpdateDateTimeToIncludeMilliseconds('task_schedule', 'task_schedule_id', NewTaskScheduleID, 'added_date', AddedDate);
      AddIdentity(NewKeys, 'task_schedule', TaskScheduleID, NewTaskScheduleID);
    end;
  end;
end;

procedure TTicketDM.SetTicketPriority(const TicketID, PriorityID: Integer);
const
  SQL = 'update ticket set work_priority_id = %d where ticket_id = %d';
var
  RecsAffected: Integer;
begin
  LogicDM.Conn.Execute(Format(SQL, [PriorityID, TicketID]), RecsAffected);
  Assert(RecsAffected = 1, 'The ticket''s priority could not be changed in SetTicketPriority.');
end;



function TTicketDM.GetPriorityNameFromTicket(const TicketID: Integer): string;
const
  SelectPriority = 'select r.description from ticket ' +
    'inner join reference r on ticket.work_priority_id = r.ref_id ' +
    'where ticket_id = %d';
var
  D: TDataSet;
begin
  Result := '';
  D := LogicDM.GetDataSetForSQL(Format(SelectPriority, [TicketID]));
  if not D.Eof then
    Result := D.FieldByName('Description').AsString;
  D.Close;
end;

function TTicketDM.SaveNewTickets(const Tickets: NewTicketList): GeneratedKeyList;
const
  Existing = 'select * from ticket with (index(ticket_transmit_date)) where transmit_date = %s and call_date = %s '+
    'and ticket_format = :ticket_format and ticket_type = :ticket_type and work_type = :work_type and work_city = :work_city '+
    'and work_state = :work_state and work_address_number = :work_address_number and work_address_street = :work_address_street '+
    'and ticket_number like ''MAN%%'' and status like ''MANUAL%%''';
var
  xmlStr : string; //QMANTWO-307
  TicketIndex: Integer;
  TicketID: Integer;
  CallDate: TDateTime;
  TransmitDate: TDateTime;
  TicketFormat: string;
  TicketType: string;
  WorkType: string;
  WorkCity: string;
  WorkState: string;
  WorkAddressStreet: string;
  WorkAddressNumber: string;
  WorkDescription: Variant;
  WorkRemarks: Variant;
  Company: Variant;
  ConName: Variant;
  CallerContact: Variant;
  MapPage: Variant;
  WorkCross: Variant;
  WorkCounty: Variant;
  Kind: Variant;
  Ticket: NVPairList;
  NewTicketID: Integer;
  D: TADODataSet;
  NeedTicketAck: Boolean;
  WorkLat, WorkLong: Double;
  WoNumber : Variant;  //QMANTWO-253,277
begin
  Result := GeneratedKeyList.Create;
  D := NewTicket;
  for TicketIndex := 0 to Tickets.Count - 1 do begin
    SetPhase(Format('Ticket %d of %d', [TicketIndex + 1, Tickets.Count]));
    Ticket := Tickets[TicketIndex].TicketData;
    TicketID := NVPairInteger(Ticket, 'ticket_id', bvError);
    NeedTicketAck := False;
    if TicketID > 0 then
      raise Exception.CreateFmt('Can not create new ticket for existing ticket ID %s', [TicketID]);

    CallDate := NVPairDateTime(Ticket, 'call_date', bvError);
    TransmitDate := RoundSQLServerTimeMS(NVPairDateTime(Ticket, 'transmit_date', bvError));
    TicketFormat := NVPairString(Ticket, 'ticket_format', bvError);
    TicketType := NVPairString(Ticket, 'ticket_type', bvError);
    WorkType := NVPairString(Ticket, 'work_type', bvError);
    WorkCity := NVPairString(Ticket, 'work_city', bvError);
    WorkState := NVPairString(Ticket, 'work_state', bvError);
    WorkAddressStreet := NVPairString(Ticket, 'work_address_street', bvError);
    WorkAddressNumber := NVPairString(Ticket, 'work_address_number', bvError);

    WorkDescription := NVPairString(Ticket, 'work_description', bvNull);
    WorkRemarks := NVPairString(Ticket, 'work_remarks', bvNull);
    Company := NVPairString(Ticket, 'company', bvNull);
    ConName := NVPairString(Ticket, 'con_name', bvNull);
    CallerContact := NVPairString(Ticket, 'caller_contact', bvNull);
    MapPage := NVPairString(Ticket, 'map_page', bvNull);
    WorkCross := NVPairString(Ticket, 'work_cross', bvNull);
    WorkCounty := NVPairString(Ticket, 'work_county', bvNull);
    Kind := NVPairString(Ticket, 'Kind', bvNull);
    WorkLat := NVPairFloat(Ticket, 'work_lat', bvZero);
    WorkLong := NVPairFloat(Ticket, 'work_long', bvZero);
    WoNumber := NVPairString(Ticket, 'wo_number', bvNull);   //QMANTWO-253
    D.Close;
    D.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(TransmitDate), IsoDateTimeToStrQuoted(CallDate)]);
    D.Parameters.ParamValues['ticket_format'] := TicketFormat;
    D.Parameters.ParamValues['ticket_type'] := TicketType;
    D.Parameters.ParamValues['work_type'] := WorkType;
    D.Parameters.ParamValues['work_city'] := WorkCity;
    D.Parameters.ParamValues['work_state'] := WorkState;
    D.Parameters.ParamValues['work_address_street'] := WorkAddressStreet;
    D.Parameters.ParamValues['work_address_number'] := WorkAddressNumber;
    D.Open;
    if D.RecordCount = 1 then
      D.Edit
    else if D.RecordCount = 0 then
      D.Insert
    else
      raise Exception.Create('Multiple existing tickets match the new ticket save data for ticket ID: ' + IntToStr(TicketID));

    if VarIsNull(Kind) then
      Kind := TicketKindNormal;

    SetPhase('Editing ticket');
    Assert(EditingDataSet(D));
    D.FieldByName('call_date').Value := CallDate;
    D.FieldByName('ticket_format').Value := TicketFormat;
    D.FieldByName('ticket_type').Value := TicketType;
    D.FieldByName('work_type').Value := WorkType;
    D.FieldByName('work_city').Value := WorkCity;
    D.FieldByName('work_state').Value := WorkState;
    D.FieldByName('work_address_street').Value := WorkAddressStreet;
    D.FieldByName('work_address_number').Value := WorkAddressNumber;
    D.FieldByName('work_description').Value := WorkDescription;
    D.FieldByName('work_remarks').Value := WorkRemarks;
    D.FieldByName('company').Value := Company;
    D.FieldByName('con_name').Value := ConName;
    D.FieldByName('caller_contact').Value := CallerContact;
    D.FieldByName('map_page').Value := MapPage;
    D.FieldByName('work_cross').Value := WorkCross;
    D.FieldByName('work_county').Value := WorkCounty;
    D.FieldByName('kind').Value := Kind;
    D.FieldByName('parsed_ok').AsBoolean := True;
    D.FieldByName('active').AsBoolean := True;
    D.FieldByName('modified_date').AsDateTime := Now;
    D.FieldByName('image').AsString := 'xml'; //QMANTWO-307
    D.FieldByName('revision').AsString := 'MANUAL';
    D.FieldByName('channel').AsString := IntToStr(LogicDM.LoggedInEmpID);
    D.FieldByName('work_lat').AsFloat := WorkLat;
    D.FieldByName('work_long').AsFloat := WorkLong;
    D.FieldByName('wo_number').Value := WoNumber;     //QMANTWO-253, 277
    //D.FieldByName('due_date').AsDateTime := Now + 2; // This and work_date are set by some Python code
    if LogicDM.CanI(RightTicketsAutoApproveNewTicket) or LogicDM.CanI(RightTicketManagement) then
      D.FieldByName('status').AsString := TicketStatusManualApproved
    else begin
      D.FieldByName('status').AsString := TicketStatusManual;
      NeedTicketAck := True;
    end;

    D.FieldByName('transmit_date').AsDateTime := TransmitDate;
    if D.FieldByName('ticket_number').AsString = '' then
      D.FieldByName('ticket_number').AsString := UpperCase(ManualTicketNumberPrefix + TicketFormat +'-'+ IntToStr(NextNewTicketNumberID));
     //QMANTWO-307
    xmlStr :=  DataSetToXML(D);//QMANTWO-307
    if D.RecordCount = 1 then //QMANTWO-307
      D.Edit                  //QMANTWO-307
    else if D.RecordCount = 0 then //QMANTWO-307
      D.Insert;  //QMANTWO-307
    D.FieldByName('image').AsString := xmlStr; //QMANTWO-307
    D.Post;
    NewTicketID := D.FieldByName('ticket_id').AsInteger;
    LogicDM.UpdateDateTimeToIncludeMilliseconds('ticket', 'ticket_id', NewTicketID, 'call_date', CallDate);
    LogicDM.UpdateDateTimeToIncludeMilliseconds('ticket', 'ticket_id', NewTicketID, 'transmit_date', TransmitDate);

    if NeedTicketAck then begin
      InsertTicketAck.Parameters.ParamByName('@TicketID').Value := NewTicketID;
      InsertTicketAck.ExecProc;
    end;
    if TicketID < 0 then begin
      Assert(NewTicketID > 0, 'Negative new ticket ID');
      AddIdentity(Result, 'ticket', TicketID, NewTicketID);
    end;
  end;
end;

procedure TTicketDM.SaveOtherTicketInfoChanges(ChangeList: TicketInfoChangeList;
  var NewKeys: GeneratedKeyList);
begin
 NewKeys := GeneratedKeyList.Create;  {QMANTWO-775 EB When calling outside of Ticket Changes}
 ProcessTicketInfoChanges(ChangeList,NewKeys);
end;

function TTicketDM.GetTicketImage(const TicketID: Integer): string;
var
  TicketIsXml: Boolean;
begin
 // TicketImageData.ParamByName['ID'].Value := TicketID;
  TicketImageData.Parameters.ParamValues['ID'] := TicketID;   //QM-977 Remove BetterADO  EB
  TicketImageData.Open;
  Result := GetDisplayableTicketImage(TicketImageData.FieldByName('image').AsString,
    TicketImageData.FieldByName('xml_ticket_format').AsString, TicketIsXml);
  TicketImageData.Close;
end;

function TTicketDM.GetTicketLocateList(TicketID: integer): string;
begin

  If LocatesForTicket.Active then
    LocatesForTicket.Close;
 // LocatesForTicket.ParamByName['ticket_id'].Value := TicketID;
  LocatesForTicket.Parameters.ParamValues['ticket_id'] := TicketID;  //QM-977 Remove BetterADO EB
  LocatesForTicket.Open;
  Result := ConvertDataSetToXML([LocatesForTicket], 2000, 'locate_id');
end;

function TTicketDM.GetTicket2(const TicketID: Integer; UpdatesSince: TDateTime): string;
const
  Params: array[0..1] of TParamRec =
      ((Name: '@TicketID';     ParamType: ftInteger; Dir: pdInput),
       (Name: '@UpdatesSince'; ParamType: ftDate;    Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_ticket5', Params, [TicketID, UpdatesSince]);
end;

function TTicketDM.GetTicketAlertsForTicket(TicketID: integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_ticket_alerts', '@TicketID', TicketID);
end;

function TTicketDM.GetTicketHistory(const TicketID: Integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('dbo.get_ticket_history2', '@TicketID', TicketID);
end;

function TTicketDM.TicketIDSearch(const TicketID: integer): string;
//QM-305 EB Add Debug Ticket ID Search
const
AttachmentCountClause =
    '(select count(*) from attachment a ' +
    'where a.foreign_id=ticket.ticket_id and a.foreign_type=1 and a.active=1)';
  Select =
    'select distinct ' +
    AttachmentCountClause + ' as attachments, ticket.status, ticket.ticket_id, ' +
    'ticket.ticket_number, ticket.revision, ticket.due_date, ticket.work_state, ticket.work_city, ' + 
    'case when (ticket.parent_ticket_id is NOT NULL) then ''Child'' else '''' end as followup_type, ' +
    'parent_ticket_id, ' +
    'ticket.work_address_number, ticket.work_address_street, ticket.company, ' +
    'ticket.con_name, ticket.ticket_type, ticket.work_county, ticket.work_type, ' +
    'ticket.kind, Coalesce((select sortby from reference where reference.ref_id = ticket.work_priority_id), 0) as work_priority from ticket ' +
    'where ticket_id = ''%d'' ';
var
  MaxTicketReturn: integer; //QM-305 EB Add Debug Ticket ID Search
  SQL: string;
begin
  SQL := Format(Select, [TicketID]);
  LogicDM.SearchQuery.CommandText :=  SQL;
  LogicDM.SearchQuery.Open;

  try
    MaxTicketReturn := StrToInt(LogicDM.GetConfigurationDataValue('TicketSearchMax', '2000')); //QM-305 EB Add Debug Ticket ID Search
  except
    MaxTicketReturn := 2500;
  end;
  Result := ConvertDataSetToXML([LogicDM.SearchQuery], MaxTicketReturn, 'ticket_id');

end;

function TTicketDM.TicketSearch(const TicketNumber, Street,
  City, State, County, WorkType, TicketType, DoneFor, Company: string;
  const DueDateFrom, DueDateTo, RecvDateFrom, RecvDateTo: TDateTime;
  const CallCenter, TermCode: string; const Attachments: Integer;
  const ManualTickets: Integer; const Priority: string; const UtilityCo:integer): string;  //QMANTWO-810
const
  AttachmentCountClause =
    '(select count(*) from attachment a ' +
    'where a.foreign_id=ticket.ticket_id and a.foreign_type=1 and a.active=1)';
  Select =
    'select distinct ' +   //QMANTWO-810
    AttachmentCountClause + ' as attachments, ticket.status, ticket.ticket_id, ' +
    'ticket.ticket_number, ticket.revision, ticket.due_date, ticket.work_state, ticket.work_city, ' + //QMANTWO-605
    'case when (ticket.parent_ticket_id is NOT NULL) then ''Child'' else '''' end as followup_type, ' +
    'parent_ticket_id, ' +
    'ticket.work_address_number, ticket.work_address_street, ticket.company, ' +
    'ticket.con_name, ticket.ticket_type, ticket.work_county, ticket.work_type, ' +
    'ticket.kind, Coalesce((select sortby from reference where reference.ref_id = ticket.work_priority_id), 0) as work_priority from ticket ';
  TermCodeJoinClause = ' inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'and locate.client_code=''%s'' ';

  UtilityCoJoinClause1 = ' join client  on locate.client_code =client.oc_code ';  //QMANTWO-810
  UtilityCoJoinClause2 = ' join locate on ticket.ticket_id = locate.ticket_id '+
                         ' join client  on locate.client_code =client.oc_code ';  //QMANTWO-810

  UtilityCoWhereClause=   ' AND client.ref_id = ''%d'' ';   //QMANTWO-810

  NolockClause = '(NOLOCK) ';
  NolockIndexClause = 'with (NOLOCK, INDEX(ticket_duedate)) ';
  TicketNumberIndexClause = 'with (index(ticket_ticket_number)) ';
  ModifiedDateClause = 'with (NOLOCK, INDEX(ticket_modifieddate)) ';



var
  SQL: TOdSqlBuilder;
  FullSQL: string;
  DueDateFromCalc, DueDateToCalc: TDateTime;
  PivotDate: TDateTime;
  OldDataSet, NewDataSet: TDataSet;
  JoinClause: string;
  TextKeywordSearch: Boolean;
  FullTextFields: string;
  SearchUtiltyCo:Boolean;
  MaxTicketReturn: Integer;
  function RunSearchInternal(DMToUse: TLogicDM; ModifiedSince: TDateTime; Clause: string): TDataSet;
  begin
    SQL := TOdSqlBuilder.Create;
    try
      StrToStrings(FullTextFields, ',', SQL.FullTextSearchColumns, False);
      SQL.TableAlias := 'ticket';
      Assert(Assigned(DMToUse), 'RunSearchForDueDateRange requires not-nil data module');
      //Ken Funk 2016-01-27
      //Original Code
      //SQL.AddSingleKeywordSearchTailWC('ticket.ticket_number', TicketNumber);
      //New Code Block
      if Trim(TicketNumber) <> '' then
        SQL.AddSingleKeywordSearchTailWC('ticket.ticket_number', TicketNumber);
      //End New Code Block
      if TextKeyWordSearch then
        SQL.AddLookupKeywordSearch('ticket_word', 'ticket_id', 'word', Copy(Street, 2, Length(Street)))
      else
        SQL.AddKeywordSearch('ticket.work_address_street', Street);

      SQL.AddKeywordSearch('ticket.work_city', City);
      SQL.AddExactStringFilter('ticket.work_state', State);
      SQL.AddExactStringFilter('ticket.work_county', County);
      SQL.AddKeywordSearch('ticket.work_type', WorkType);
      SQL.AddKeywordSearch('ticket.ticket_type', TicketType);
      SQL.AddKeywordSearch('ticket.company', DoneFor);
      SQL.AddKeywordSearch('ticket.con_name', Company);

      if ManualTickets = Ord(mtManual) then
        SQL.AddWhereCondition('ticket.status like ''%MANUAL''') 
      else if ManualTickets = Ord(mtNeedsApproval) then
        SQL.AddWhereCondition(Format('ticket.status = ''%s''', [TicketStatusManual]))
      else if ManualTickets = Ord(mtApproved) then
        SQL.AddWhereCondition(Format('ticket.status = ''%s''', [TicketStatusManualApproved]));
      SQL.CheckFilterCount;

      SQL.AddWhereCondition(CallCenterCondition(CallCenter));
      SQL.AddDateTimeRangeFilter('ticket.transmit_date', RecvDateFrom, RecvDateTo);
      SQL.AddDateTimeRangeFilter('ticket.due_date', DueDateFromCalc, DueDateToCalc);

      SQL.AddIntFilterUnlessZero('client.ref_id', UtilityCo); //QMANTWO-810

      if not IsEmpty(Priority) then begin
        // NORMAL priority includes all tickets with work_priority_id = null
        if UpperCase(Priority) = TicketKindNormal then  //QM-52  sr
          SQL.AddWhereConditionFmt('ticket.work_priority_id is null OR ticket.work_priority_id = %d', [LogicDM.GetRefID('tkpriority', Priority)])
        else
          SQL.AddWhereConditionFmt('ticket.work_priority_id = %d', [LogicDM.GetRefID('tkpriority', Priority)])
      end;

      if Attachments = Ord(acHas) then
        SQL.AddNumericFilter(AttachmentCountClause, '> 0')
      else if Attachments = Ord(acDoesNotHave) then
        SQL.AddNumericFilter(AttachmentCountClause, '= 0');

      SQL.AddDateTimeGreaterFilter('ticket.modified_date', ModifiedSince);

      JoinClause := '';
      if not(TermCode = '') and (SearchUtiltyCo=false) then      //QMANTWO-810
        JoinClause := Format(TermCodeJoinClause, [TermCode]);

      if not(TermCode = '') and (SearchUtiltyCo=true) then     //QMANTWO-810
      begin
         JoinClause := '';
         JoinClause := Format(TermCodeJoinClause, [TermCode]);
         JoinClause:= JoinClause+#10#13+UtilityCoJoinClause1;
      end;

      if (TermCode = '') and (SearchUtiltyCo=true) then     //QMANTWO-810
      begin
         JoinClause := '';
         JoinClause:= UtilityCoJoinClause2;
      end;


      FullSQL := Select + Clause + JoinClause + SQL.WhereClause;   //QMANTWO-810
      DMToUse.SearchQuery.CommandText := FullSQL;
      Op.Detail := Clause + JoinClause + SQL.WhereClause;

      DMToUse.SearchQuery.Open;

      Result := DMToUse.SearchQuery;
      Assert(Assigned(Result), 'RunSearchForDueDateRange should return not-nil query');
    finally
      FreeAndNil(SQL);
    end;
  end;

  procedure CalcDueDateRange; //QM-57  This will always fail if the actual ticket Due date is NULL
  begin
    if (DueDateFromCalc = 0) and (RecvDateFrom > 0) and (RecvDateTo > 0) then begin
      DueDateFromCalc := RecvDateFrom - 14;
      DueDateToCalc := RecvDateTo + 14;   // cast a wider net
    end;
  end;

begin
  SearchUtiltyCo:=false;
  TextKeywordSearch := (Street <> '') and (Street[1] = '~');
  if UtilityCo <> 0 then SearchUtiltyCo:=true;  //QMANTWO-810
  

  if SearchIsDisabled(FEATURE_NAME) then
    raise Exception.Create('Ticket Search is currently disabled due to a system problem.  It will be enabled as soon as possible.');

  if (TicketNumber = '') and (DueDateFrom = 0) and (RecvDateFrom = 0) then
    raise Exception.Create('You must specify a received-date range or a due-date range unless searching by ticket number.');

  OldDataSet := nil;
  DueDateFromCalc := DueDateFrom;
  DueDateToCalc := DueDateTo;

  FullTextFields := LogicDM.GetStringIniSetting('FullTextSearch', 'ticket', '');
  //Ken Funk 2016-01-27 - added trim
  if (Trim(TicketNumber) <> '') then begin
    SetPhase('Ticket Number query, all Main DB');
    //Larry - This shouldn't be in here, regardless of the search 2/1/2016
    //NewDataSet := RunSearchInternal(LogicDM, 0, TicketNumberIndexClause);
    NewDataSet := RunSearchInternal(LogicDM, 0, NolockClause);
  end else if ForceMainDBSearch(FEATURE_NAME) or TextKeywordSearch then begin
    SetPhase('Ini/keyword forced main DB search');
    NewDataSet := RunSearchInternal(LogicDM, 0, NolockClause);
  end else begin
    ConnectRepDM;
    PivotDate := ReportingDatabasePivot;

    SetPhase('Historical query in ' + RepDM.DBName);
    // Force clustered index on due date
  //QM-57 Do not set a due date criteria for Manual tickets    sr
  //Manual tkts due dates are null and setting a search due date will always fail.    sr
    if ManualTickets < 1 then  //QM-57
    CalcDueDateRange;

    OldDataSet := RunSearchInternal(RepDM, 0, NolockIndexClause);

    SetPhase('Current data query in Main DB');
    NewDataSet := RunSearchInternal(LogicDM, PivotDate, ModifiedDateClause);
  end;

  SetPhase('Encoding results');
  try
    MaxTicketReturn := StrToInt(LogicDM.GetConfigurationDataValue('TicketSearchMax', '2000'));
  except
    MaxTicketReturn := 2500;
  end;
  Result := ConvertDataSetToXML([NewDataSet, OldDataSet], MaxTicketReturn, 'ticket_id');
end;



function TTicketDM.CallCenterCondition(Centers: string): string;
var
  CenterList: TStringList;
  i: Integer;
  Center: string;
begin
  if Trim(Centers)='' then
    Exit;

  CenterList := TStringList.Create;
  try
    CenterList.CommaText := Centers;
    for i := 0 to CenterList.Count - 1 do begin
      if i <> 0 then
        Result := Result + ' OR ';

      Center := Trim(CenterList[i]);
      // Prevent SQL injection attacks:
      Center := StringReplace(Center, '''', '', [rfReplaceAll] );
      Result := Result + 'ticket_format=''' + Center + '''';
    end;
  finally
    FreeAndNil(CenterList);
  end;
end;

function TTicketDM.CompletionReport(const DateFrom, DateTo: TDateTime; const Office: Integer;
  const Manager: Integer; const SortBy: Integer): string;
begin
  Op.Detail := 'Range: ' + DateTimeToStr(DateFrom) + ' - ' + DateTimeToStr(DateTo);

  if (SortBy < 0) or (SortBy > 4) then
    raise Exception.Create('SortBy must be 0..4');

  ConnectRepDM;
  with RepDM.CompletionReportSP do begin
    Parameters.ParamValues['@datefrom'] := DateFrom;
    Parameters.ParamValues['@dateto'] := DateTo;
    Parameters.ParamValues['@office'] := Office;
    Parameters.ParamValues['@manager'] := Manager;
    Parameters.ParamValues['@sortby'] := SortBy;
    SetPhase('Running query');
    Open;
    try
      SetPhase('Encoding results');
      Result := ConvertDataSetToXML([RepDM.CompletionReportSP], 0);
    finally
      Close;
    end;
  end;
end;

function DataSetToXML(DataSet  : TDataSet): string;
var    //QMANTWO-307
  i: integer;
  function MakeTag(TagName, Value: String): string;
  begin
    result := '<' + TagName + '>' + Value + '</' + TagName + '>';
  end;

begin
  result := '';
  if (not DataSet.Active) or (DataSet.IsEmpty) then
    Exit;
  result := result + '<' + DataSet.Name + '>';
  DataSet.First;
  while not DataSet.EOF do
  begin
    result := result + '<RECORD>';
    for i := 0 to DataSet.Fields.Count - 1 do
      result := result + MakeTag(DataSet.Fields[i].DisplayName,
        DataSet.Fields[i].Text);
    result := result + '</RECORD>';
    DataSet.Next;
  end;
  result := result + '</' + DataSet.Name + '>';
end;


end.
