unit NotesDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, DB, ADODB, QMServerLibrary_Intf, RemObjectsUtils;

type
  TNotesDM = class(TLogicFeatureDM)
    InsertNote: TADOCommand;
    Notes: TADODataSet;
    UpdateNote: TADOCommand;
  public
    function AddNote(ForeignType, ForeignID: Integer; EntryDate: TDateTime;
      EnteredByUID: Integer; const NoteText: string; SubType: Integer=0): Integer;
      procedure ChangeNote(NoteID: Integer; UserID: Integer; ChangedText: string; SubType:Integer);
    function NoteExists(ForeignID, ForeignType, EnteredByUID: Integer;
      EntryDate: TDateTime; const Note: string;
      var ExistingNoteID: Integer): Boolean;
    function SaveNewNotesInternal(NotesList: NotesChangeList): GeneratedKeyList;
    function SaveNewNotesInternal5(NotesList: NotesChangeList5): GeneratedKeyList;
    procedure ProcessNotesChanges(NotesList: NotesChangeList; var NewKeys: GeneratedKeyList);
    procedure ProcessNotesChanges5(NotesList: NotesChangeList5; var NewKeys: GeneratedKeyList);
  end;

var
  NotesDM: TNotesDM;

implementation

uses QMConst, OdMiscUtils, OdIsoDates;

{$R *.dfm}

{This is present to work with the older clients. Once they upgrade, it can be removed}
procedure TNotesDM.ProcessNotesChanges(NotesList: NotesChangeList; var NewKeys: GeneratedKeyList);
var
  NoteParams: NotesChange;
  NewNotesID: Integer;
  NotesID: Integer;
  ForeignType: Integer;
  ForeignID: Integer;
  NoteText: string;
  EnteredDate: TDateTime;
  IsActive: Boolean;
  i: Integer;
begin
  Assert(Assigned(NotesList), 'NotesList is unassigned in ProcessNotesChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessNotesChanges');

  for i := 0 to NotesList.Count - 1 do begin
    NoteParams := NotesList.Items[i];
    Assert(Assigned(NoteParams));

    NotesID := NoteParams.NotesID;
    ForeignID := NoteParams.ForeignID;
    ForeignType := NoteParams.ForeignType;
    NoteText := NoteParams.Note;
    EnteredDate := NoteParams.EntryDate;
    IsActive := NoteParams.Active;

    if NotesID > 0 then
      raise Exception.Create('Editing existing notes is not supported');
    if ForeignID < 0 then
      raise Exception.Create('Negative Notes ForeignID numbers are not allowed in SaveNewNotes2');
    if (ForeignType < qmftTicket) or (ForeignType > qmftWorkOrder) then
      raise Exception.Create('Notes Foreign Type is not within the allowed range');
    Assert(NotEmpty(NoteText));

    // Ticket notes may be duplicates from a failed sync or sent by older clients and
    // already saved via StatusTicketsLocates, so ignore duplicates...
    if not NoteExists(ForeignID, ForeignType, LogicDM.LoggedInUID, EnteredDate, NoteText, NewNotesID) then begin
      Notes.Insert;
      Notes.FieldByName('foreign_id').AsInteger := ForeignID;
      Notes.FieldByName('foreign_type').AsInteger := ForeignType;
      Notes.FieldByName('note').AsString := NoteText;
      Notes.FieldByName('uid').AsInteger := LogicDM.LoggedInUID;
      Notes.FieldByName('entry_date').AsDateTime := EnteredDate;
      Notes.FieldByName('active').AsBoolean := IsActive;
      Notes.FieldByName('sub_type').AsInteger := 0; //default it to 0
      Notes.Post; // modified_date has a default of getdate()
      NewNotesID := Notes.FieldByName('notes_id').AsInteger;
      LogicDM.UpdateDateTimeToIncludeMilliseconds('notes', 'notes_id', NewNotesID, 'entry_date', EnteredDate);
    end;

    // For both existing rows and new inserts:
    Assert(NewNotesID > 0);
    AddIdentity(NewKeys, 'notes', NotesID, NewNotesID);
  end;
end;

procedure TNotesDM.ProcessNotesChanges5(NotesList: NotesChangeList5;
  var NewKeys: GeneratedKeyList);
var
  NoteParams: NotesChange5;
  NewNotesID: Integer;
  NotesID: Integer;
  ForeignType: Integer;
  ForeignID: Integer;
  NoteText: string;
  EnteredDate: TDateTime;
  IsActive: Boolean;
  SubType: Integer;
  i: Integer;
begin
  Assert(Assigned(NotesList), 'NotesList is unassigned in ProcessNotesChanges');
  Assert(Assigned(NewKeys), 'NewKeys is unassigned in ProcessNotesChanges');

  for i := 0 to NotesList.Count - 1 do begin
    NoteParams := NotesList.Items[i];
    Assert(Assigned(NoteParams));

    NotesID := NoteParams.NotesID;
    ForeignID := NoteParams.ForeignID;
    ForeignType := NoteParams.ForeignType;
    NoteText := NoteParams.Note;
    EnteredDate := NoteParams.EntryDate;
    IsActive := NoteParams.Active;

    If NoteParams.SubType >= 0 then
      SubType := NoteParams.SubType
    else
      SubType := 0;

    if NotesID > 0 then
      raise Exception.Create('Editing existing notes is not supported');
    if ForeignID < 0 then
      raise Exception.Create('Negative Notes ForeignID numbers are not allowed in SaveNewNotes2');
    if (ForeignType < qmftTicket) or (ForeignType > qmftWorkOrder) then
      raise Exception.Create('Notes Foreign Type is not within the allowed range');
    Assert(NotEmpty(NoteText));

    // Ticket notes may be duplicates from a failed sync or sent by older clients and
    // already saved via StatusTicketsLocates, so ignore duplicates...
    if not NoteExists(ForeignID, ForeignType, LogicDM.LoggedInUID, EnteredDate, NoteText, NewNotesID) then begin
      Notes.Insert;
      Notes.FieldByName('foreign_id').AsInteger := ForeignID;
      Notes.FieldByName('foreign_type').AsInteger := ForeignType;
      Notes.FieldByName('note').AsString := NoteText;
      Notes.FieldByName('uid').AsInteger := LogicDM.LoggedInUID;
      Notes.FieldByName('entry_date').AsDateTime := EnteredDate;
      Notes.FieldByName('active').AsBoolean := IsActive;
      Notes.FieldByName('sub_type').AsInteger := SubType;
      Notes.Post; // modified_date has a default of getdate()
      NewNotesID := Notes.FieldByName('notes_id').AsInteger;
      LogicDM.UpdateDateTimeToIncludeMilliseconds('notes', 'notes_id', NewNotesID, 'entry_date', EnteredDate);
    end;

    // For both existing rows and new inserts:
    Assert(NewNotesID > 0);
    AddIdentity(NewKeys, 'notes', NotesID, NewNotesID);
  end;
end;

procedure TNotesDM.ChangeNote(NoteID: Integer; UserID: Integer; ChangedText: string; SubType:Integer);  //QM-188   (Note.NoteID, Note.EnteredByUID, Note.NoteText)
const
  SQL = 'update notes set note = ''%s'', sub_type = %d, modified_date = %s where notes_id = %d';
var
  RecsAffected: Integer;
  UpdateSQL: string;
begin
  UpdateSQL := Format(SQL,[ChangedText, SubType, (IsoDateTimeToStrQuoted(Now)), NoteID]);
  LogicDM.Conn.Execute(UpdateSQL, RecsAffected);
  Assert(RecsAffected = 1, 'Note ' + IntToStr(NoteID) + ' could not be updated.');
end;

function TNotesDM.NoteExists(ForeignID, ForeignType,
  EnteredByUID: Integer; EntryDate: TDateTime; const Note: string; var ExistingNoteID: Integer): Boolean;
const
  Existing = 'select * from notes where uid = :uid and foreign_id = :foreign_id and foreign_type = :foreign_type and entry_date = %s';
begin
  ExistingNoteID := -1;
  Notes.Close;
  Notes.CommandText := Format(Existing, [IsoDateTimeToStrQuoted(EntryDate)]);
  Notes.Parameters.ParamValues['foreign_id'] := ForeignID;
  Notes.Parameters.ParamValues['foreign_type'] := ForeignType;
  Notes.Parameters.ParamValues['uid'] := EnteredByUID;
  Notes.Open;

  if Length(Note) > Notes.FieldByName('note').Size then
    raise Exception.CreateFmt('Notes can be at most %d characters long.', [Notes.FieldByName('note').Size]);

  if Notes.IsEmpty then
    Result := False
  else begin
    Result := Notes.Locate('note', Note, []); // Verify we have an exact duplicate note
    if Result then
      ExistingNoteID := Notes.FieldByName('notes_id').AsInteger;
  end;
end;

function TNotesDM.AddNote(ForeignType, ForeignID: Integer; EntryDate: TDateTime;
  EnteredByUID: Integer; const NoteText: string; SubType: Integer): Integer;
const
  SQL = 'INSERT INTO notes (foreign_type, foreign_id, entry_date, uid, note, sub_type) '+
        'VALUES (:ForeignType, :ForeignID, %s, :UID, :Note, :SubType)';
var
  D: TADOCommand;
begin
 {This should be fine for clients with subtypes and without subtypes, because it is defaulted}
  Assert(ForeignType in [qmftTicket..qmftWorkOrder], 'Invalid value for ForeignType in InsertNote');
  if SubType = Null then
    SubType := 0;

  Result := -1;
  if not NoteExists(ForeignID, ForeignType, EnteredByUID, EntryDate, NoteText, Result) then begin
    D := InsertNote;
    D.CommandText := Format(SQL, [IsoDateTimeToStrQuoted(EntryDate)]);
    D.Parameters.ParamValues['ForeignType'] := ForeignType;
    D.Parameters.ParamValues['ForeignID'] := ForeignID;
    D.Parameters.ParamValues['UID'] := EnteredByUID;
    D.Parameters.ParamValues['Note'] := NoteText;
    D.Parameters.ParamValues['SubType'] :=  SubType;
    D.Execute;
    Result := LogicDM.GetAnyIdentity;
  end;
end;

{EB - This is present to work with the older clients. Once they upgrade, it can be removed}
function TNotesDM.SaveNewNotesInternal(NotesList: NotesChangeList): GeneratedKeyList;
var
  NoteParams: NotesChange;
  NewNotesID: Integer;
  NotesID: Integer;
  ForeignType: Integer;
  ForeignID: Integer;
  NoteText: string;
  EnteredDate: TDateTime;
  IsActive: Boolean;
  i: Integer;
begin
  Result := GeneratedKeyList.Create;

  for i := 0 to NotesList.Count - 1 do begin
    NoteParams := NotesList.Items[i];
    Assert(Assigned(NoteParams));

    NotesID := NoteParams.NotesID;
    ForeignID := NoteParams.ForeignID;
    ForeignType := NoteParams.ForeignType;
    NoteText := NoteParams.Note;
    EnteredDate := NoteParams.EntryDate;
    IsActive := NoteParams.Active;

    if NotesID > 0 then
      raise Exception.Create('Editing existing notes is not supported');
    if ForeignID < 0 then
      raise Exception.Create('Negative Notes ForeignID numbers are not allowed in SaveNewNotes2');
    if (ForeignType < qmftTicket) or (ForeignType > qmftWorkOrder) then
      raise Exception.Create('Notes Foreign Type is not within the allowed range');
    Assert(NotEmpty(NoteText));

    // Ticket notes may be duplicates from a failed sync or sent by older clients and
    // already saved via StatusTicketsLocates, so ignore duplicates...
    if not NoteExists(ForeignID, ForeignType, LogicDM.LoggedInUID, EnteredDate, NoteText, NewNotesID) then begin
      Notes.Insert;
      Notes.FieldByName('foreign_id').AsInteger := ForeignID;
      Notes.FieldByName('foreign_type').AsInteger := ForeignType;
      Notes.FieldByName('note').AsString := NoteText;
      Notes.FieldByName('uid').AsInteger := LogicDM.LoggedInUID;
      Notes.FieldByName('entry_date').AsDateTime := EnteredDate;
      Notes.FieldByName('active').AsBoolean := IsActive;
      Notes.FieldByName('sub_type').AsInteger := 0; //EB - This will default the sub_type when the client is older
      Notes.Post; // modified_date has a default of getdate()
      NewNotesID := Notes.FieldByName('notes_id').AsInteger;
      LogicDM.UpdateDateTimeToIncludeMilliseconds('notes', 'notes_id', NewNotesID, 'entry_date', EnteredDate);
    end;

    // For both existing rows and new inserts:
    Assert(NewNotesID > 0);
    AddIdentity(Result, 'notes', NotesID, NewNotesID);
  end;
end;

{EB - The only difference with this one is that the structures contain the subtype}
function TNotesDM.SaveNewNotesInternal5(
  NotesList: NotesChangeList5): GeneratedKeyList;
var
  NoteParams: NotesChange5;
  NewNotesID: Integer;
  NotesID: Integer;
  ForeignType: Integer;
  ForeignID: Integer;
  NoteText: string;
  EnteredDate: TDateTime;
  IsActive: Boolean;
  SubType: Integer;
  i: Integer;
begin
  Result := GeneratedKeyList.Create;

  for i := 0 to NotesList.Count - 1 do begin
    NoteParams := NotesList.Items[i];
    Assert(Assigned(NoteParams));

    NotesID := NoteParams.NotesID;
    ForeignID := NoteParams.ForeignID;
    ForeignType := NoteParams.ForeignType;
    NoteText := NoteParams.Note;
    EnteredDate := NoteParams.EntryDate;
    IsActive := NoteParams.Active;

    if NoteParams.SubType >= 0 then
      SubType := NoteParams.SubType
    else
     SubType := 0; 


    if NotesID > 0 then
      raise Exception.Create('Editing existing notes is not supported');
    if ForeignID < 0 then
      raise Exception.Create('Negative Notes ForeignID numbers are not allowed in SaveNewNotes2');
    if (ForeignType < qmftTicket) or (ForeignType > qmftWorkOrder) then
      raise Exception.Create('Notes Foreign Type is not within the allowed range');
    Assert(NotEmpty(NoteText));

    // Ticket notes may be duplicates from a failed sync or sent by older clients and
    // already saved via StatusTicketsLocates, so ignore duplicates...
    if not NoteExists(ForeignID, ForeignType, LogicDM.LoggedInUID, EnteredDate, NoteText, NewNotesID) then begin
      Notes.Insert;
      Notes.FieldByName('foreign_id').AsInteger := ForeignID;
      Notes.FieldByName('foreign_type').AsInteger := ForeignType;
      Notes.FieldByName('note').AsString := NoteText;
      Notes.FieldByName('uid').AsInteger := LogicDM.LoggedInUID;
      Notes.FieldByName('entry_date').AsDateTime := EnteredDate;
      Notes.FieldByName('active').AsBoolean := IsActive;
      Notes.FieldByName('sub_type').AsInteger := SubType;
      Notes.Post; // modified_date has a default of getdate()
      NewNotesID := Notes.FieldByName('notes_id').AsInteger;
      LogicDM.UpdateDateTimeToIncludeMilliseconds('notes', 'notes_id', NewNotesID, 'entry_date', EnteredDate);
    end;

    // For both existing rows and new inserts:
    Assert(NewNotesID > 0);
    AddIdentity(Result, 'notes', NotesID, NewNotesID);
  end;
end;

end.
