unit ManageWorkDMu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseLogicFeatureDMu, QMServerLibrary_Intf, ADODB,
  DB, TicketDMu, RemObjectsUtils;

type
  TManageWorkDM = class(TLogicFeatureDM)
    HierDisplaySP: TADOStoredProc;
    AcknowledgeTicket: TADOStoredProc;
    AcknowledgeDamageTicket: TADOStoredProc;
    OQEmpData: TADOStoredProc;
    GetVirtualBucketTickets: TADOStoredProc;
    AddEmpPlus: TADOStoredProc;
    RemoveEmpPlus: TADOStoredProc;
    GetEmpPlusUtilbyEmp: TADOStoredProc;
    InsertMarsRequest: TADOCommand;
    InsertROOptimization: TADOCommand;
    DamageListDataset: TADODataSet;
    WorkOrderList: TADODataSet;
    WorkloadData: TADODataSet;
  private
    FTicketDM: TTicketDM;
    function GetTicketDM: TTicketDM;
    procedure SendDamageNotificationEmail(const TicketID, DamageID: Integer; const InvestigatorName: string);
  public
    destructor Destroy; override;
    function GetHierarchy2(const IncludePeers: Boolean; const IncludeInactive: Boolean): string;
    function MultiOpenTotals6(const Managers: string): string;
    function MultiOpenTotals6PD(const Managers: string): string; //QM-248 & QM-133 Past Due EB
    function MultiOpenTotals6OP(const Managers: string): string; //QM-428 Open  EB
	  function MultiOpenTotals6TD(const Managers: string): string; //QM 318 Today EB
    function MultiOpenTotals6V(const Managers: string; const VirtualBucketMgrID: integer): string; //QM-486 Virtual Bucket EB
    procedure SaveEmployeeTicketViewLimit(const EmployeeID, TicketViewLimit: Integer);
    procedure SaveEmployeeShowFutureWork(const EmpID: Integer; const ShowFutureTickets: Boolean);
    function GetPastDueList(const ManagerID: Integer; const MaxDays: Integer): string; //QM-133 Past Due EB
    function GetVirtualBucket(const ManagerID: Integer; const BucketName: string): string; //QM-486 Virtual Bucket EB
    function GetTodayList(const ManagerID: Integer; const MaxDays: Integer): string; //QM-318 Today EB
	  function GetOpenTicketSumList(const ManagerID: Integer; const MaxDays: Integer): string;  //QM-428 Open EB
    function GetOpenTicketExpList(const ManagerID: Integer; const RowLimit: Integer; const MaxDays: Integer): string;  //QM-695 Open Expanded EB
    function GetTicketActivities(const EmpID: Integer; const NumDays: Integer): String;
    function AckTicketActivities(const ActivityAckIDArray: IntegerList): Integer;
    function AckAllTicketActivitiesForManager(const Params: NVPairListList): Integer;
    function GetUnackedTicketActivityCount(const Params: NVPairList): Integer;
    procedure AckTicket(const TicketID: TicketKey; const AckedByEmp: Integer);
    procedure AckDamageTicket(const TicketID: Integer; const AckedByEmp: Integer; const InvestigatorID: Integer);
    function GetTicketsForLocator(const LocatorID: Integer; const NumDays: Integer; const OpenOnly: Boolean; const Unacknowledged: Boolean): string;
    function GetTicketsForLocatorRO(const LocatorID, NumDays: Integer; const OpenOnly, Unacknowledged: Boolean): string;     //QMANTWO-775 EB Ticket Route Override
    function GetDamagesForLocator(const LocatorID, NumDays: Integer; const OpenOnly: Boolean): string;
    function GetWorkOrdersForEmployee(const EmployeeID, NumDays: Integer; const OpenOnly, Unacknowledged: Boolean): string;

    {OQ Qualifications and PLUS Whitelist}
    function GetOQDataForEmployee(EmpID: integer): string;  //QM-444 EB
    function GetOQDataForManager(MgrID: integer; IncludeRecentExp: Integer): string;  //QM-444 Part 2 EB
    function GetPLUSForManager(MgrID: integer): string;  //QM-585 EB PLUS Whitelist
    procedure AddEmpToPlusWhiteList(LocatorID: integer; PlusID: Integer; ModifiedByID: Integer; Util: string); //QM-585 EB Plus Whitelist
    procedure RemoveEmpFromPlusWhitelist(LocatorID: integer; PlusID: Integer; ModifiedByID: Integer); //QM-585 EB Plus Whitelist
    function GetEmployeePlusListByEmp(EmpID: integer): string; //QM-585 EB PLUS Whitelist
    function GetWMEmpPlusUtilByEmp(EmpID: integer): string; //QM-585 EB Plus Whitelist Part 2
    function GetEmpPlusMgrList(MgrID: integer): string;  //QM-585 EB Plus Whitelist part 3

    {MARS Bulk Status QM-671 EB}
    function GetMARSEmpList(MgrID: integer): string;
    function GetMARSHier(MgrID: integer): string;
    function AddMARSRequest(LocatorID: integer; NewStatus: string; InsertedBy: integer): boolean;
    function StopMARSRequest(pMARSID: integer): boolean;

    {Route Order Optimization QM-702}
    function AddROOptimizationRequest(LocatorID:integer; InsertedBy:integer; NextTicketID: integer): boolean;
    function StopOptimizationRequest(LocatorID: integer): boolean;
  end;

implementation

uses QMConst, OdExceptions, OdSqlXmlOutput, OdMiscUtils, OdDataSetToXML, LogicDMu,
  StrUtils;

{$R *.dfm}

function TManageWorkDM.GetHierarchy2(const IncludePeers: Boolean;
  const IncludeInactive: Boolean): string;
var
  LimitationString: string;
begin
  if not LogicDM.CanI(RightTicketManagement, LimitationString) then
    raise EOdNotAllowed.Create('You do not have ticket management permission');

  if (LimitationString = '') or (LimitationString = '-') then
    LimitationString := IntToStr(LogicDM.LoggedInEmpID);

  HierDisplaySP.ProcedureName := 'hier_display3;1';
  HierDisplaySP.Parameters.Refresh;
  HierDisplaySP.Parameters.ParamValues['@ManagerList'] := LimitationString;

  if IncludePeers then
    HierDisplaySP.Parameters.ParamValues['@include_neighbors'] := 1
  else
    HierDisplaySP.Parameters.ParamValues['@include_neighbors'] := 0;
  HierDisplaySP.Open;
  try
    Result := FormatSQLXML(HierDisplaySP);
  finally
    HierDisplaySP.Close;
  end;
end;





function TManageWorkDM.GetMARSEmpList(MgrID: integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('get_MARS_EmpList', '@MgrID', MgrID);   //QM-671 EB MARS Bulk Status
end;

function TManageWorkDM.GetMARSHier(MgrID: integer): string;  //QM-671 EB MARS Bulk Status
  const
  Params: array[0..1] of TParamRec =
      ((Name: '@ID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@Managers_Only'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_hier', Params, [MgrID, 0]);
end;

function TManageWorkDM.MultiOpenTotals6(const Managers: string): string;
var
  MgrList: string;
  D: TADODataSet;

  procedure AddBitParamBasedOnRight(const RightID: string);
  begin
    D.CommandText := D.CommandText + ', ' + BoolToStr(LogicDM.CanI(RightID, LogicDM.LoggedInEmpID));
  end;

begin
  MgrList := Managers;
  if IsEmpty(MgrList) then begin
    if not LogicDM.CanI(RightTicketManagement, MgrList) then
      raise EOdNotAllowed.Create('You do not have ticket management permission');
    if (MgrList = '') or (MgrList = '-') then
      MgrList := IntToStr(LogicDM.LoggedInEmpID);
  end;
  if not ValidateIntegerCommaSeparatedList(MgrList) then
    raise Exception.Create('The Managers parameter must be a comma separated list of emp_ids');

  SetPhase('Configuring SP params');
  Op.Detail := MgrList;
  D := WorkloadData;
  D.CommandText := 'exec dbo.multi_open_totals5 ''' + MgrList + '''';
  AddBitParamBasedOnRight(RightTicketsAcknowledgeActivity);
  AddBitParamBasedOnRight(RightDamagesManagement);
  AddBitParamBasedOnRight(RightWorkOrdersManagement);

  SetPhase('Opening SP');
  D.Open;
  SetPhase('Building Result List');
  Result := FormatSQLXML(D);
  D.Close;
end;

function TManageWorkDM.MultiOpenTotals6OP(const Managers: string): string;
var
  MgrList: string;           
  D: TADODataSet;

  procedure AddBitParamBasedOnRight(const RightID: string);
  begin
    D.CommandText := D.CommandText + ', ' + BoolToStr(LogicDM.CanI(RightID, LogicDM.LoggedInEmpID));
  end;

begin
  MgrList := Managers;
  if IsEmpty(MgrList) then begin
    if not LogicDM.CanI(RightTicketManagement, MgrList) then
      raise EOdNotAllowed.Create('You do not have ticket management permission');
    if (MgrList = '') or (MgrList = '-') then
      MgrList := IntToStr(LogicDM.LoggedInEmpID);
  end;
  if not ValidateIntegerCommaSeparatedList(MgrList) then
    raise Exception.Create('The Managers parameter must be a comma separated list of emp_ids');

  SetPhase('Configuring SP params');
  Op.Detail := MgrList;
  D := WorkloadData;
  D.CommandText := 'exec dbo.multi_open_totals6OP ''' + MgrList + '''';
  AddBitParamBasedOnRight(RightTicketsAcknowledgeActivity);
  AddBitParamBasedOnRight(RightDamagesManagement);
  AddBitParamBasedOnRight(RightWorkOrdersManagement);

  SetPhase('Opening SP');
  D.Open;
  SetPhase('Building Result List');
  Result := FormatSQLXML(D);
  D.Close;
end;

function TManageWorkDM.MultiOpenTotals6PD(const Managers: string): string;
var
  MgrList: string;
  D: TADODataSet;

  procedure AddBitParamBasedOnRight(const RightID: string);
  begin
    D.CommandText := D.CommandText + ', ' + BoolToStr(LogicDM.CanI(RightID, LogicDM.LoggedInEmpID));
  end;

begin
  MgrList := Managers;
  if IsEmpty(MgrList) then begin
    if not LogicDM.CanI(RightTicketManagement, MgrList) then
      raise EOdNotAllowed.Create('You do not have ticket management permission');
    if (MgrList = '') or (MgrList = '-') then
      MgrList := IntToStr(LogicDM.LoggedInEmpID);
  end;
  if not ValidateIntegerCommaSeparatedList(MgrList) then
    raise Exception.Create('The Managers parameter must be a comma separated list of emp_ids');

  SetPhase('Configuring SP params');
  Op.Detail := MgrList;
  D := WorkloadData;
  D.CommandText := 'exec dbo.multi_open_totals6 ''' + MgrList + '''';
  AddBitParamBasedOnRight(RightTicketsAcknowledgeActivity);
  AddBitParamBasedOnRight(RightDamagesManagement);
  AddBitParamBasedOnRight(RightWorkOrdersManagement);

  SetPhase('Opening SP');
  D.Open;
  SetPhase('Building Result List');
  Result := FormatSQLXML(D);
  D.Close;
end;

function TManageWorkDM.MultiOpenTotals6TD(const Managers: string): string;  //QM-318 Today EB
var
  MgrList: string;
  D: TADODataSet;

  procedure AddBitParamBasedOnRight(const RightID: string);
  begin
    D.CommandText := D.CommandText + ', ' + BoolToStr(LogicDM.CanI(RightID, LogicDM.LoggedInEmpID));
  end;

begin
  MgrList := Managers;
  if IsEmpty(MgrList) then begin
    if not LogicDM.CanI(RightTicketManagement, MgrList) then
      raise EOdNotAllowed.Create('You do not have ticket management permission');
    if (MgrList = '') or (MgrList = '-') then
      MgrList := IntToStr(LogicDM.LoggedInEmpID);
  end;
  if not ValidateIntegerCommaSeparatedList(MgrList) then
    raise Exception.Create('The Managers parameter must be a comma separated list of emp_ids');

  SetPhase('Configuring SP params');
  Op.Detail := MgrList;
  D := WorkloadData;
  D.CommandText := 'exec dbo.multi_open_totals6TD ''' + MgrList + '''';
  AddBitParamBasedOnRight(RightTicketsAcknowledgeActivity);
  AddBitParamBasedOnRight(RightDamagesManagement);
  AddBitParamBasedOnRight(RightWorkOrdersManagement);

  SetPhase('Opening SP');
  D.Open;
  SetPhase('Building Result List');
  Result := FormatSQLXML(D);
  D.Close;
end;


function TManageWorkDM.MultiOpenTotals6V(const Managers: string; const VirtualBucketMgrID: integer): string; //QM-486 Virtual Bucket EB
var
  MgrList: string;
  D: TADODataSet;

  procedure AddBitParamBasedOnRight(const RightID: string);
  begin
    D.CommandText := D.CommandText + ', ' + BoolToStr(LogicDM.CanI(RightID, LogicDM.LoggedInEmpID));
  end;

begin
  MgrList := Managers;
  if IsEmpty(MgrList) then begin
    if not LogicDM.CanI(RightTicketManagement, MgrList) then
      raise EOdNotAllowed.Create('You do not have ticket management permission');
    if (MgrList = '') or (MgrList = '-') then
      MgrList := IntToStr(LogicDM.LoggedInEmpID);
  end;
  if not ValidateIntegerCommaSeparatedList(MgrList) then
    raise Exception.Create('The Managers parameter must be a comma separated list of emp_ids');

  SetPhase('Configuring SP params');
  Op.Detail := MgrList;
  D := WorkloadData;
  D.CommandText := 'exec dbo.multi_open_totals6V ''' + MgrList + '''';
  AddBitParamBasedOnRight(RightTicketsAcknowledgeActivity);
  AddBitParamBasedOnRight(RightDamagesManagement);
  AddBitParamBasedOnRight(RightWorkOrdersManagement);
  D.CommandText := D.CommandText + ', ' + IntToStr(VirtualBucketMgrID); //QM-486 Virtual Bucket EB {We need to add the Bucket Mgr that we are using}

  SetPhase('Opening SP');
  D.Open;
  SetPhase('Building Result List');
  Result := FormatSQLXML(D);
  D.Close;
end;



procedure TManageWorkDM.SaveEmployeeTicketViewLimit(const EmployeeID, TicketViewLimit: Integer);
const
  Update = 'update employee set ticket_view_limit = %d where emp_id = %d';
var
  RecsAffected: Integer;
begin
  if not LogicDM.CanI(RightTicketsChangeViewLimits) then
    raise EOdNotAllowed.Create('You do not have permission to change employee ticket view limits');
  LogicDM.Conn.Execute(Format(Update, [TicketViewLimit, EmployeeID]), RecsAffected);
  Assert(RecsAffected = 1, 'The employee''s ticket view limit was not changed in SaveEmployeeTicketViewLimit.');
end;

procedure TManageWorkDM.SaveEmployeeShowFutureWork(const EmpID: Integer; const ShowFutureTickets: Boolean);
const
  UpdateSQL = 'update employee set show_future_tickets = %d where emp_id = %d';
var
  RecsAffected: Integer;
begin
  if not LogicDM.CanI(RightTicketsManageFutureTickets) then
    raise EOdNotAllowed.Create('You do not have permission to manage future tickets.');
  LogicDM.Conn.Execute(Format(UpdateSQL, [Ord(ShowFutureTickets), EmpID]), RecsAffected);
  Assert(RecsAffected = 1, 'The employee''s show_future_tickets flag was not changed in SaveEmployeeShowFutureWork.');
end;

function TManageWorkDM.GetPastDueList(const ManagerID, MaxDays: Integer): string; //QM-133 EB Past Due
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@MaxDays'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_past_due_tickets', Params, [ManagerID, MaxDays]);
end;



function TManageWorkDM.GetVirtualBucket(const ManagerID: Integer; const BucketName: string): string; //QM-486 Virtual Bucket EB
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput; Size: 0),
       (Name: '@BucketName'; ParamType: ftString; Dir: pdInput; Size: 15));
begin
//SP.Parameters.CreateParameter(Parameters[I].Name, Parameters[I].ParamType, Parameters[I].Dir, 0, Values[I]);
  try
    GetVirtualBucketTickets.Connection := LogicDM.Conn;
    GetVirtualBucketTickets.Parameters.ParamValues['@ManagerID'] := ManagerID;
    GetVIrtualBucketTickets.Parameters.ParamValues['@BucketName'] := BucketName;
    GetVirtualBucketTickets.Open;
    Result := FormatSqlXml(GetVirtualBucketTickets);
  finally
    GetVirtualBucketTickets.Close;
  end;

 // Result := LogicDM.GetXMultiParamSPAsXML('dbo.get_virtual_bucket_tickets', Params, [ManagerID, BucketName]);
end;

function TManageWorkDM.GetTodayList(const ManagerID, MaxDays: Integer): string;  //QM-318 EB Today
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@MaxDays'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_today_tickets', Params, [ManagerID, MaxDays]);
end;

function TManageWorkDM.GetOpenTicketExpList(const ManagerID: Integer; const RowLimit: Integer; const MaxDays: Integer): string;
const
  Params: array[0..2] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@RowLimit'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@MaxDays'; ParamType: ftInteger; Dir: pdInput));
begin

  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_open_ticket_list_ext', Params, [ManagerID, RowLimit, MaxDays]);
  {Note: currently, we are only pulling the counts so that the grid is not disabled}

end;

function TManageWorkDM.GetOpenTicketSumList(const ManagerID,
  MaxDays: Integer): string;   //QM-428 Open Count list
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@MaxDays'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_open_ticket_list', Params, [ManagerID, MaxDays]);
  {Note: currently, we are only pulling the counts so that the grid is not disabled}
end;




function TManageWorkDM.GetOQDataForEmployee(EmpID: integer): string;  //QM-444 EB
var
  firstvalue: boolean;
begin
  OQEmpData.Parameters.ParamValues['EmpID'] := EmpID;
  OQEmpData.Open;
  OQEmpData.First;
  firstvalue := True;
  if OQEmpData.EOF then begin
    Result := '--'
  end
  else begin
    Result := '';
    while not OQEmpData.EOF do begin
      if not firstvalue then
        Result := Result + ', ';
      Result := Result + OQEmpData.FieldbyName('modifier').AsString;
      OQEmpData.Next;
      firstvalue := False;
    end;
  end;
end;


function TManageWorkDM.GetOQDataForManager(MgrID: integer; IncludeRecentExp: Integer): string;
const
  Params: array[0..1] of TParamRec =
      ((Name: '@MgrID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@IncRecentExp'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('get_oq_data_mgr', Params, [MgrID, IncludeRecentExp ]);
end;


function TManageWorkDM.GetPLUSForManager(MgrID: integer): string;   //QM-585 EB PLUS Whitelist
//begin
//  Result := LogicDM.GetSingleParamSPAsXML('get_PLUS_termcodes', '@MgrID', MgrID);

const
  SQL = 'Select dbo.get_PLUS_termcodes(%d) as MgrPlusPermission ';
var
  DataSet: TDataSet;
begin
  Result := '';
  DataSet := LogicDM.CreateDataSetForSQL(Format(SQL, [MgrID]));
  try
    if Dataset.RecordCount > 0 then
      Result := Dataset.FieldByName('MgrPlusPermission').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TManageWorkDM.AddEmpToPlusWhiteList(LocatorID, PlusID,
  ModifiedByID: Integer; Util: string);   //QM-585 EB Plus Whitelist
begin

  with AddEmpPlus do begin
    Parameters.ParamValues['LocatorID'] := LocatorID;
    Parameters.ParamValues['PlusID'] := PlusId;
    Parameters.ParamValues['ModifiedByID'] := ModifiedByID;
    Parameters.ParamValues['Util'] := Util;
    ExecProc;
  end;
end;

function TManageWorkDM.AddMARSRequest(LocatorID: integer; NewStatus: string;   //QM-671 MARS EB Bulk Status
  InsertedBy: integer): boolean;
begin
  Result := False;
  with InsertMarsRequest do begin
    Parameters.ParamValues['EmpID'] := LocatorID;
    Parameters.ParamValues['NewStatus'] := NewStatus;
    Parameters.ParamValues['InsertedBy'] := InsertedBy;
    Execute;
    Result := True;
  end;
end;

function TManageWorkDM.StopOptimizationRequest(LocatorID: integer): boolean;
const
 UpdateSQL =  'update route_optimize_queue '+
              'set active = 0 '+
              'where tree_emp_id = :EmpID ';
begin
  {Note: Use this for update queries. Seems to work better than components}
  Result := False;

  LogicDM.ExecuteSQLWithParams(UpdateSQL, [LocatorID]);
  Result := True;
end;


function TManageWorkDM.AddROOptimizationRequest(LocatorID, InsertedBy, NextTicketID: integer): boolean;   //QM-702 Route Optimization EB
const
//  SQL = 'select * from route_optimize_queue ' +
//        'where (tree_emp_id = %d)';

  TicketRequest =   'INSERT INTO route_optimize_queue' +
                      '(tree_emp_id, next_ticket_id, inserted_by, insert_date, active) ' +
                      'VALUES' +
                      '(:EmpID, :NextTicketID, :InsertedBy, GetDate(), 1)';

  NOTicketRequest = 'INSERT INTO route_optimize_queue' +
                      '(tree_emp_id, inserted_by, insert_date, active) ' +
                      'VALUES' +
                      '(:EmpID, :InsertedBy, GetDate(), 1)';
var
  DS: TDataset;
  IncludeTkt: boolean;
begin
  Result := False;
  StopOptimizationRequest(LocatorID);

////Taking out
////  DS:= LogicDM.CreateDataSetForSQL(Format(SQL, [LocatorID]));
////  if DS.RecordCount > 0 then
////    Result := True
////  else begin
    with InsertROOptimization do begin
      if NextTicketID = 0 then begin
        CommandText := NoTicketRequest;
        IncludeTkt := False;
      end
      else begin
        CommandText := TicketRequest;
        IncludeTkt := True;
      end;

      Parameters.ParamValues['EmpID'] := LocatorID;
      Parameters.ParamValues['InsertedBy'] := InsertedBy;

      if IncludeTkt then
        Parameters.ParamValues['NextTicketID'] := NextTicketID;
        
      Execute;
      Result := True;
    end;
////  end;
end;

function TManageWorkDM.StopMARSRequest(pMARSID: integer): boolean;   //QM-671 MARS EB Bulk Status
const
 UpdateSQL =  'update mars '+
              'set active = 0 '+
              'where mars_id = :mars_id ';

begin
  {Note: Use this for update queries. Seems to work better than components}
  Result := False;

  LogicDM.ExecuteSQLWithParams(UpdateSQL, [pMARSID]);
  Result := True;
end;







procedure TManageWorkDM.RemoveEmpFromPlusWhitelist(LocatorID, PlusID,
  ModifiedByID: Integer);
begin

  with RemoveEmpPlus do begin
    Parameters.ParamValues['LocatorID'] := LocatorID;
    Parameters.ParamValues['PlusID'] := PlusId;
    Parameters.ParamValues['ModifiedByID'] := ModifiedByID;
    ExecProc;
  end;
end;


function TManageWorkDM.GetTicketActivities(const EmpID: Integer; const NumDays: Integer): String;   //QM-133 EB Past Due
const
  Params: array[0..1] of TParamRec =
      ((Name: '@ManagerID'; ParamType: ftInteger; Dir: pdInput),
       (Name: '@NumDays'; ParamType: ftInteger; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_unacked_ticket_activities2', Params,
    [EmpID, NumDays]);
end;

function TManageWorkDM.AckTicketActivities(const ActivityAckIDArray: IntegerList): Integer;
begin
  // Currently setup to only handle one ticket ack id at a time.
  Assert(ActivityAckIDArray.Count = 1);
  Result := LogicDM.ExecuteSQL(Format('exec dbo.acknowledge_ticket_activity %d, %d',
    [ActivityAckIDArray.Items[0], LogicDM.LoggedInEmpID]));
end;



destructor TManageWorkDM.Destroy;
begin
  FreeAndNil(FTicketDM);
  inherited;
end;

function TManageWorkDM.AckAllTicketActivitiesForManager(const Params: NVPairListList): Integer;
var
  i: Integer;
  ManagerID: Integer;
  CutoffDate: TDateTime;
begin
  Result := 0;
  if not LogicDM.CanI(RightTicketsAcknowledgeAllActivities) then
    raise EOdNotAllowed.Create('You do not have permission to acknowledge all ticket activities.');

  for i := 0 to Params.Count-1 do begin
    ManagerID := NVPairInteger(Params.Items[i], 'manager_id', bvError);
    CutoffDate := NVPairDateTime(Params.Items[i], 'cutoff_date', bvError);
    Result := LogicDM.ExecuteSQL(Format('exec dbo.acknowledge_all_ticket_activity %d, %s, %d',
      [ManagerID, QuoteDate(CutoffDate), LogicDM.LoggedInEmpID]));
  end;
end;

function TManageWorkDM.GetUnackedTicketActivityCount(const Params: NVPairList): Integer;
var
  ManagerID: Integer;
  CutoffDate: TDateTime;
const
  Select = 'select count(*) as Cnt from ticket_activity_ack ack ' +
    'inner join dbo.get_hier(%d, 0) hier on ack.locator_emp_id = hier.h_emp_id ' +
    'where ack.activity_date < %s and ack.has_ack = 0';
begin
  if not LogicDM.CanI(RightTicketsAcknowledgeAllActivities) then
    raise EOdNotAllowed.Create('You do not have permission to acknowledge all ticket activities.');
  ManagerID := NVPairInteger(Params, 'manager_id', bvError);
  CutoffDate := NVPairDateTime(Params, 'cutoff_date', bvError);
  LogicDM.SearchQuery.CommandText := Format(Select, [ManagerID, QuoteDate(CutoffDate)]);
  LogicDM.SearchQuery.Open;
  try
    Result := LogicDM.SearchQuery.FieldByName('Cnt').AsInteger;
  finally
    LogicDM.SearchQuery.Close;
  end;
end;



function TManageWorkDM.GetTicketDM: TTicketDM;
begin
  if FTicketDM = nil then
    FTicketDM := TTicketDM.Create(nil, LogicDM);
  Result := FTicketDM;
end;

procedure TManageWorkDM.AckTicket(const TicketID: TicketKey; const AckedByEmp: Integer);
begin
  if GetTicketDM.IsManualTicket(TicketID.TicketID) then
    if not LogicDM.CanI(RightTicketsApproveManual) then
      raise EOdNotAllowed.Create('You do not have permission to approve manual tickets');
  with AcknowledgeTicket do begin
    Parameters.ParamValues['@TicketID'] := TicketID.TicketID;
    Parameters.ParamValues['@EmpID'] := AckedByEmp;
    ExecProc;
  end;
end;

procedure TManageWorkDM.AckDamageTicket(const TicketID: Integer; const AckedByEmp, InvestigatorID: Integer);
var
  UQDamageID: Integer;
  InvestigatorName: string;
  Ack: TADOStoredProc;
begin
  Ack := AcknowledgeDamageTicket;
  Ack.Parameters.ParamValues['@TicketID'] := TicketID;
  Ack.Parameters.ParamValues['@EmpID'] := AckedByEmp;
  Ack.Parameters.ParamValues['@InvestigatorID'] := InvestigatorID;
  Ack.Open;
  UQDamageID := Ack.FieldByName('UQDamageID').AsInteger;
  InvestigatorName := Ack.FieldByName('InvestigatorName').AsString;
  SendDamageNotificationEmail(TicketID, UQDamageID, InvestigatorName);
end;

procedure TManageWorkDM.SendDamageNotificationEmail(const TicketID, DamageID: Integer;
  const InvestigatorName: string);
var
  Subject, Body, FromEMail: string;
begin
  FromEMail:=''; //QM-9  gets address from INI
  Subject := 'Damage Ticket Notification, Damage # ' + IntToStr(DamageID);
  Body := 'Damage Created, Damage #:' + IntToStr(DamageID) + CRLF +
          'Assigned to Investigator: ' + InvestigatorName + CRLF +
          'Ticket:' + CRLF + CRLF +
          GetTicketDM.GetTicketImage(TicketID);
  LogicDM.SendEmailWithIniSettingsExp('DamageEmail', FromEMail, '', Subject, Body);
end;



function TManageWorkDM.GetTicketsForLocator(const LocatorID, NumDays: Integer;
  const OpenOnly, Unacknowledged: Boolean): string;
const
  Params: array[0..3] of TParamRec =
       ((Name: '@ID';       ParamType: ftInteger; Dir: pdInput),
       (Name: '@OpenOnly'; ParamType: ftBoolean; Dir: pdInput),
       (Name: '@NumDays';  ParamType: ftInteger; Dir: pdInput),
       (Name: '@UnAcked';  ParamType: ftBoolean; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_ticket_list2', Params,
    [LocatorID, OpenOnly, NumDays, UnAcknowledged]);
end;

function TManageWorkDM.GetTicketsForLocatorRO(const LocatorID, NumDays: Integer;      //QMANTWO-775 EB Ticket Route Override
  const OpenOnly, Unacknowledged: Boolean): string;
const
  Params: array[0..3] of TParamRec =
       ((Name: '@ID';       ParamType: ftInteger; Dir: pdInput),
       (Name: '@OpenOnly'; ParamType: ftBoolean; Dir: pdInput),
       (Name: '@NumDays';  ParamType: ftInteger; Dir: pdInput),
       (Name: '@UnAcked';  ParamType: ftBoolean; Dir: pdInput));
begin
  Result := LogicDM.GetMultiParamSPAsXML('dbo.get_ticket_list_RO', Params,
    [LocatorID, OpenOnly, NumDays, UnAcknowledged]);
end;

function TManageWorkDM.GetDamagesForLocator(const LocatorID, NumDays: Integer; const OpenOnly: Boolean): string;
begin
  DamageListDataSet.CommandText := Format('exec dbo.get_damage_list %d, %s, %d',
    [LocatorID, IfThen(OpenOnly, '1', '0'), NumDays]);
  DamageListDataSet.Open;
  try
    Result := ConvertDataSetToXML([DamageListDataset], 5000, 'damage_id');
  finally
    DamageListDataSet.Close;
  end;
end;

function TManageWorkDM.GetEmployeePlusListByEmp(EmpID: integer): string;
begin
 // Result := LogicDM.GetSingleParamSPAsXML('dbo.get_employee_plus_by_emp', @EmpID, EmpID);
end;


function TManageWorkDM.GetEmpPlusMgrList(MgrID: integer): string;
begin
  Result := LogicDM.GetSingleParamSPAsXML('get_PLUS_MgrList', '@MgrID', MgrID);
end;


function TManageWorkDM.GetWMEmpPlusUtilByEmp(EmpID: integer): string;
var
  firstvalue: boolean;
begin
{Call this one before you call the OQ Licensing query}
  GetEmpPlusUtilbyEmp.Parameters.ParamValues['EmpID'] := EmpID;
  GetEmpPlusUtilbyEmp.Open;
  GetEmpPlusUtilbyEmp.First;
  firstvalue := True;
  if GetEmpPlusUtilbyEmp.EOF then begin
    Result := '--'
  end
  else begin
    Result := '';
    while not GetEmpPlusUtilbyEmp.EOF do begin
      if firstvalue then
        Result := GetEmpPlusUtilbyEmp.FieldByname('util').AsString + ':';
      if not firstvalue then
        Result := Result + ', ';
      Result := Result +
                GetEmpPlusUtilbyEmp.FieldbyName('short_name').AsString;
              //  GetEmpPlusUtilbyEmp.FieldbyName('group_code').AsString;
      GetEmpPlusUtilbyEmp.Next;
      firstvalue := False;
    end;
  end;
end;

function TManageWorkDM.GetWorkOrdersForEmployee(const EmployeeID, NumDays: Integer;
  const OpenOnly, Unacknowledged: Boolean): string;
begin
  WorkOrderList.CommandText := Format('exec dbo.get_work_order_list %d, %s, %d',
    [EmployeeID, IfThen(OpenOnly, '1', '0'), NumDays]);
  WorkOrderList.Open;
  try
    Result := ConvertDataSetToXML([WorkOrderList], 5000, 'wo_id');
  finally
    WorkOrderList.Close;
  end;
end;

end.
