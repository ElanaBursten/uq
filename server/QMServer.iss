; -- QMServer.iss --
; This script ceates a virtual dir and installs QMLogic.dll, QMLogic2.exe, ReportEngineCgi.exe, and other front end server files
; Use Inno Setup 5.4.2+
#define Version GetEnv("VERSION")
#define MyAppName "Q Manager Server"
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName={#MyAppName} 
AppVerName={#MyAppName} {#Version} 
UninstallDisplayName = {#MyAppName} {#Version} {code:GetVirtualDirNameFromInstallDir|''}
;N.B. AppId must be unique (per Virtual Dir) for the uninstallers to work correctly.
AppID={#MyAppName} {code:GetVirtualDirAlias|''}
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\{#MyAppName}
DefaultGroupName={#MyAppName}
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/
AppMutex=UtiliQuestQManagerServer
AppVersion={#Version}{#GitCommitIDWithDash}
OutputDir=..\build
OutputBaseFilename=QManagerServerSetup-{#Version}{#GitCommitIDWithDash}
DisableProgramGroupPage=yes
PrivilegesRequired=none
OutputManifestFile=QMServerSetup-Manifest.txt
SetupLogging=yes
UsePreviousLanguage=no

[Components]
Name: "core"; Description: "Core Q Manager Server Files"; Types: full custom; Flags: fixed
;Name: "qmlogic2"; Description: "Q Manager Server QMLogic2"; Types: full
;Name: "qmprojmetrics"; Description: "Q Manager Projector - Metrics"; Types: full
; TODO: Add WGL SOAP Server

[Files]
;DLLs needed in the app directory for SSL support
Source: ..\lib\libeay32.dll; DestDir: {app}; Flags: ignoreversion
Source: ..\lib\ssleay32.dll; DestDir: {app}; Flags: ignoreversion

;todo track down: default.aspx reports the machine name when hitting http://qmclustertest.utiliquest.com
;Source: "default.aspx";   DestDir: "{app}"; Flags: onlyifdoesntexist
;todo track down: ReportCheck.asp validates the link to the reporting server is working by querying the db for a row
;Source: "ReportCheck.asp";   DestDir: "{app}"; Flags: onlyifdoesntexist

;Server bpls
; Delphi 2007 Standard
Source: "..\lib\rtl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclx100.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\dbrtl100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcldb100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclsmp100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclie100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcljpg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclimg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\adortl100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\midas.dll";     DestDir: "{sys}"; Flags: sharedfile

;eFax API files 
;There is an intentional typo in the required param StrongAssemblyName (no space before Version) 
;to prevent uninstall from GAC (yet allow install into GAC). This is to support multiple server installs.
Source: ..\lib\eFaxDeveloper.dll; DestDir: {sys}; Flags: sharedfile gacinstall; StrongAssemblyName: "eFaxDeveloper,Version=2.0.0.0, Culture=neutral, PublicKeyToken=b715e7d32ebddfc8WW0, ProcessorArchitecture=MSIL"
Source: ..\lib\eFaxDeveloper.tlb; DestDir: {sys}; Flags: sharedfile

;QM Server applications
Source: QMLogic.dll; DestDir: {app}; BeforeInstall: DoBeforeInstall; Flags: ignoreversion
Source: ..\SharedSecureAttachments\DSSS.dll; DestDir: {app}; BeforeInstall: DoBeforeInstallDSSS; Flags: ignoreversion
Source: SampleQMServer.ini; DestDir: {app}; DestName: QMServer.ini; Flags: onlyifdoesntexist uninsneveruninstall
Source: ..\build\QM_email_log_errors.exe; DestDir: {app}; Flags: ignoreversion
Source: ..\tools\email_log_errors\Sample_QM_email_log_errors.ini; DestDir: {app}; DestName: QM_email_log_errors.ini; Flags: onlyifdoesntexist uninsneveruninstall
Source: ..\reportengine\ReportEngineCGI.exe; DestDir: {app}; Flags: ignoreversion
Source: ..\reportengine\sample_ReportEngineCGI.ini; DestDir: {app}; DestName: ReportEngineCGI.ini; Flags: onlyifdoesntexist uninsneveruninstall

Source: ..\fax_status\FaxStatusUpdate.exe; DestDir: {app}; Flags: ignoreversion
;Source: ..\server2\QMLogic2.exe; DestDir: {app}; DestName: "{code:GetQMLogic2ServiceName|''}.exe"; Flags: ignoreversion; AfterInstall: AfterInstallQMLogic2; Components: qmlogic2
;Source: ..\projections\QMProjectionMetricsService.exe; DestDir: {app}; Flags: ignoreversion; AfterInstall: AfterInstallQMProjMetrics; Components: qmprojmetrics
;Source: ..\projections\QMProjectionAdmin.exe; DestDir: {app}; Flags: ignoreversion;  Components: qmprojmetrics 
Source: ..\tools\gen_events\QMGenTicketEvents.exe; DestDir: {app}; Flags: ignoreversion

;Files used by installer  
Source: ..\tests\install\VersionInfo.vbs; DestDir: {app}; Flags: ignoreversion
Source: ..\tests\install\CheckServerVersion.bat; DestDir: {app}; Flags: ignoreversion

[Tasks]
Name: ShowVersions; Description: "Verify application versions?"; GroupDescription: "Post Install Checks"; Flags: unchecked
Name: ShowTestPage; Description: "Show test web page?"; GroupDescription: "Post Install Checks"; Flags: unchecked

[Run]
Filename: "{app}\CheckServerVersion.bat"; Description: "Verify server application versions?";Flags: unchecked; Check: IsTaskSelected('ShowVersions')
Filename: "https://127.0.0.1/{code:GetVirtualDirAlias}/QMLogic.dll/monitor"; Flags: shellexec runasoriginaluser; Check: IsTaskSelected('ShowTestPage')
;deRegister the old version
Filename: "{dotnet20}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb /u"; Flags: nowait
;Register eFax API
Filename: "{dotnet40}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb"; Description: "Register eFax API"; Flags: nowait

[UninstallRun]
Filename: "iisreset.exe"; Flags: waituntilterminated; Check: IsWinXP
;Do not unregister shared assembly in order to support multiple server installs. 
;http://msdn.microsoft.com/en-us/library/tc0204w0(v=vs.71).aspx
;Filename: "{dotnet20}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb /u"; Flags: nowait

[Registry]
;Registry key is no longer needed, but leaving it in case needed for future use
;Root: HKCU; Subkey: "Software\Utiliquest\Q Manager Server\Settings\{#SetupSetting("AppVersion")}\{code:GetVirtualDirAlias}"; ValueType: string; ValueName: "Alias"; ValueData: "{code:GetVirtualDirAlias}"; Flags: uninsdeletekey

[Code]
const
  IISServerName = 'localhost';
  IISServerNumber = '1';
  MSXMLDOMDocument3GUID = '{f5078f32-c551-11d3-89b9-0000f81fe221}';
  DefaultSite = 'Default Web Site';
  AppVersionToInstall = '{#SetupSetting("AppVersion")}';
  MSG_RUNNING_SERVICES = 'Active Q Manager services could not be stopped. The setup cannot continue.';
  MSG_MUST_BE_ADMIN = 'Q Manager must be installed by a user with Windows Administrator rights.';
  QMLOGIC2_SERVICE_NAME = 'QMLogic2_';
  
var
  VirtualDirAliasPage: TInputQueryWizardPage;
  ServiceCredentialsPage: TInputQueryWizardPage;

procedure ShowError(Msg: string);
begin
  SuppressibleMsgBox(Msg, mbError, MB_OK, MB_OK);
end;  

{--- IIS ---}
{Version differences in IIS:
http://msdn.microsoft.com/en-us/library/ms524539(v=VS.90).aspx}
function GetIISVersion: Integer;
var
  MajorVersion: Cardinal;
begin
  Result := -1; //then no IIS
  if RegQueryDWordValue(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\InetStp', 'MajorVersion', MajorVersion) then
    Result := MajorVersion; 
end;

function IsWinXP: Boolean;
begin
  Result := (GetIISVersion < 6);
end;

function GetWMI: Variant;
begin
  // Connect to WMI
  Result := CreateOleObject('WbemScripting.SWbemLocator');
end;

function GetIISProvider(WMILocatorObj: Variant; ServerName: string): Variant;
begin
  try
    //Connect to the IIS namespace
    Result := WMILocatorObj.ConnectServer(ServerName, 'root/MicrosoftIISv2');
  except
    RaiseException('Please install IIS 6 WMI and scripting compatibility in the control panel under ' + 
      'Programs and Features, Windows Features, IIS 6 Management Compatibility.' #13#13 '(Error ''' + 
      GetExceptionMessage + ''' occurred)');
  end;
end;

procedure RecycleAppPool(ServerName, AppPoolName: string);
var
  ProviderObj, AppPool: Variant;
begin
  WizardForm.StatusLabel.Caption := 'Recycling ' + AppPoolName + ' application pool';
  ProviderObj := GetIISProvider(GetWMI, ServerName);
  try
    //Connect to the application pools node
    AppPool := ProviderObj.Get('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
    AppPool.Recycle;
  except
    ShowError('Application Pool ' + AppPoolName + ' could not be recycled. ' + 
      'Try using IIS Manager to recycle it once the install finishes.' #13#13 '(Error ''' + 
      GetExceptionMessage + ''' occurred)');
  end;
end;

procedure CreateAppPool(ServerName, AppPoolName: string);
var
  ProviderObj, NewAppPool : variant;
begin
  WizardForm.StatusLabel.Caption := 'Creating ' + AppPoolName + ' application pool';
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    // Create the new application pool.
    NewAppPool := ProviderObj.Get('IIsApplicationPoolSetting').SpawnInstance_();
    NewAppPool.Name := 'W3SVC/AppPools/' + AppPoolName;
    NewAppPool.Enable32BitAppOnWin64 := True;
    NewAppPool.Put_();
  except
    RaiseException('Failed to create application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
  
  try
    // Verify creation.
    NewAppPool := ProviderObj.Get('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
  except
    RaiseException('Failed verifying creation of application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure CreateVirDir(ServerName, MetabasePath, VirDirName, PhysicalPath: string);
var
  ProviderObj, NewVirDir : variant;
begin
  WizardForm.StatusLabel.Caption := 'Creating ' + VirDirName + ' virtual directory on ' + ServerName;
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);
    NewVirDir := ProviderObj.Get('IIsWebVirtualDirSetting').SpawnInstance_();
    NewVirDir.Name := MetabasePath + '/' + VirDirName;
    NewVirDir.Path := PhysicalPath;
    NewVirDir.AppFriendlyName := VirDirName;
    NewVirDir.AccessScript := True;
    NewVirDir.AccessExecute := True;
    NewVirDir.AccessSource := True;
    NewVirDir.Put_();
  except
    RaiseException('Failed to create virtual directory.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure AssignVirDirToAppPool(ServerName, MetabasePath, AppPoolName: string);
var
  ProviderObj, VirDir : variant;
begin
  //  serverName is of the form "<servername>", for example "Localhost" 
  //  metabasePath is of the form "W3SVC/<siteID>/Root[/<vDir>]", for example "W3SVC/1/ROOT/MyVDir" 
  //  appPoolName is of the form "<name>", for example, "MyAppPool"
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    VirDir := ProviderObj.Get('IIsWebVirtualDirSetting="' + MetabasePath + '"');                                                                                                       
    VirDir.AppPoolID := AppPoolName;
    VirDir.Put_();
  except
    RaiseException('Failed to assign virtual directory to application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure InstallISAPIandCGIExtensions;
var
  IIS, WebSite: Variant;
begin
  WizardForm.StatusLabel.Caption := 'Configuring IIS for Q Manager Server';
  try
    // Create the main IIS COM Automation object 
    IIS := CreateOleObject('IISNamespace');
    // Connect to the IIS server 
    WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');

    //Install ISAPI and CGI extensions. MSDN reference:
    //http://msdn.microsoft.com/en-us/library/ms524567(v=vs.90).aspx
    WebSite.AddExtensionFile(ExpandConstant('{app}') + '\QMLogic.dll', True, 'QMSRVR', True, 'Q Manager server application');
    WebSite.AddExtensionFile(ExpandConstant('{app}') + '\ReportEngineCGI.exe', True, 'QMRPT', True, 'Q Manager reporting engine');
    WebSite.EnableExtensionFile(ExpandConstant('{app}') + '\QMLogic.dll');
    WebSite.EnableExtensionFile(ExpandConstant('{app}') + '\ReportEngineCGI.exe');
    WebSite.SetInfo();
  except
    RaiseException('Failed to install and enable ISAPI and CGI extensions.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

function ServerAppExists: Boolean;
begin
  Result := FileExists(ExpandConstant('{app}') + '\QMLogic.dll');
end;

procedure CleanUp(ServerName, MetabasePath, AppPoolName: string);
var
  IIS, WebSite, ProviderObj : variant;
begin
  //Uninstall ISAPI and CGI extensions
  try
    // Create the main IIS COM Automation object
    try
      IIS := CreateOleObject('IISNamespace');
    except
      RaiseException('Please install the IIS 6 compatibility scripting feature under Programs and Features, Windows Features.' #13#13 '(Error ''' + GetExceptionMessage + ''' occurred)');
    end;
    WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');
      
    WebSite.DeleteExtensionFileRecord(ExpandConstant('{app}\QMLogic.dll'));
    WebSite.DeleteExtensionFileRecord(ExpandConstant('{app}\ReportEngineCGI.exe'));
    WebSite.SetInfo();
  except
    RaiseException('Failed to uninstall ISAPI and CGI extensions.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;  

  //  serverName is of the form "<servername>", for example "Localhost" 
  //  metabasePath is of the form "W3SVC/<siteID>/Root[/<vDir>]", for example "W3SVC/1/ROOT/MyVDir" 
  //  appPoolName is of the form "<name>", for example, "MyAppPool"
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    //Remove the virtual dir, then the application pool
    ProviderObj.Delete('IIsWebVirtualDir="' + MetabasePath + '"' );
    ProviderObj.Delete('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
  except
    RaiseException('Failed in CleanUp with the following exception: '#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure AddIIS7Extension(const Filename: string);
begin
  //
end;

procedure CreateIIS7Website(const ApplicationPoolName, VirtualDirAliasName: string);
var
  oWebAdmin, oSite, oApp, oAppPool, oSection: Variant;
  LocatorObj: Variant;
begin
  LocatorObj := GetWMI;
  oWebAdmin := LocatorObj.ConnectServer(IISServerName, 'root\WebAdministration');
  // Delete old app and pool?  If we supported enumerators/queries, we could query the existing items
  try
    try
      oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
      oApp.Delete_;
    except
    end;
    try
      oAppPool := oWebAdmin.Get('ApplicationPool.Name=''' + ApplicationPoolName + '''')
      oAppPool.Delete_;
    except
    end;
    //Create App Pool
    oWebAdmin.Get('ApplicationPool').Create(ApplicationPoolName);
    
    //Create the application
    oWebAdmin.Get('Application').Create('/' + VirtualDirAliasName, DefaultSite, ExpandConstant('{app}'));
    //Assign the application to the app pool: http://learn.iis.net/page.aspx/163/managing-applications-and-application-pools-on-iis-70-with-wmi/
    oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
    oApp.ApplicationPool := ApplicationPoolName;
    oApp.Put_;

    oSite := oWebAdmin.Get('Site.Name='''+ DefaultSite + '''');
    //Get the ISAPI-CGI restriction section.
    oSection := oSite.GetSection('IsapiCgiRestrictionSection');
    //oSection.NotListedCgisAllowed := True;
    //oSection.NotListedIsapisAllowed := True;
    //todo
  except
    //todo
  end;
end;

procedure CleanUpIIS7Website(const ApplicationPoolName, VirtualDirAliasName: string);
var
  oWebAdmin, oSite, oApp, oAppPool, oSection: Variant;
  LocatorObj: Variant;
begin
  LocatorObj := GetWMI;
  oWebAdmin := LocatorObj.ConnectServer(IISServerName, 'root\WebAdministration');
  oSite := oWebAdmin.Get('Site.Name=''' + DefaultSite + '''');

  try
    //Get the ISAPI-CGI restriction section.
    oSite.GetSection('IsapiCgiRestrictionSection', oSection);
    //oSection.Remove();
  except //todo
  end;

  //Delete an application
  try
    oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
    oApp.Delete_;
  except //todo
  end;

  //Delete an application pool
  try
    oAppPool := oWebAdmin.Get('ApplicationPool.Name=''' + ApplicationPoolName + '''');
    oAppPool.Delete_;
  except //todo
  end;
end;

procedure CreateWebSite; 
//For >=IIS6, create a virtual dir, app pool, and install ISAPI and CGI extensions
//MSDN info: http://msdn.microsoft.com/en-us/library/ms525832(v=VS.90).aspx
var
  VirtualDirAliasName, ApplicationPoolName: string;
begin
  VirtualDirAliasName := VirtualDirAliasPage.Values[0];
  ApplicationPoolName := VirtualDirAliasName + 'Pool';

  //if GetIISVersion >= 7 then begin
  //  CreateIIS7WebSite(ApplicationPoolName, VirtualDirAliasName);
  //end
  //else
  begin
    CreateAppPool(IISServerName, ApplicationPoolName); 
    CreateVirDir(IISServerName, 'W3SVC/1/Root', VirtualDirAliasName, ExpandConstant('{app}')); 
    AssignVirDirToAppPool(IISServerName, 'W3SVC/1/ROOT/' + VirtualDirAliasName, ApplicationPoolName);
    InstallISAPIandCGIExtensions;
  end;
end;

function RemoveTrailingBackslash(Path: string): string;
begin
  Result := Path;
  if Result[Length(Result)] = '\' then 
    Delete(Result, Length(Result), 1); 
end;

function ExtractPathFromFilename(FileName: string): string;
begin
  Result := FileName;
  while Result[Length(Result)] <> '\' do //remove exe name from string to next backslash
    Delete(Result, Length(Result), 1);
end;

function GetVirtualDirNameFromInstallDir(Param: string): string;
begin
  Result := ExpandConstant('{app}'); 
  Result := ExtractFileName(RemoveTrailingBackslash( Result ));//gets the folder the exe lives in, which is what we want - the name of the virtual dir
end;

procedure DeleteOldQMLogic;
var
  OldQML: string;
  Tries: Integer;
begin
  OldQML := ExpandConstant('{app}') + '\QMLogic.dll.old';
  if not FileExists(OldQML) then
    Exit;
    
  WizardForm.StatusLabel.Caption := 'Deleting temporary QMLogic.dll.old file';
  Tries := 1;
  repeat   
    DelayDeleteFile(OldQML, 40);
    Tries := Tries + 1;
    WizardForm.StatusLabel.Caption := 'Deleting temporary QMLogic.dll.old file - attempt ' + IntToStr(Tries) +  ' of 10';
  until (not FileExists(OldQML)) or (Tries > 10); 
  if FileExists(OldQML) then
    ShowError('Unable to delete ' + OldQML + '. '#13#13'The ' +
      'file may still be in use. Try deleting it again once the installation finishes.');
end;

procedure DeleteOldDSSS;
var
  OldDSSS: string;
  Tries: Integer;
begin
  OldDSSS := ExpandConstant('{app}') + '\DSSS.dll.old';
  if not FileExists(OldDSSS) then
    Exit;
    
  WizardForm.StatusLabel.Caption := 'Deleting temporary OldDSSS.dll.old file';
  Tries := 1;
  repeat   
    DelayDeleteFile(OldDSSS, 40);
    Tries := Tries + 1;
    WizardForm.StatusLabel.Caption := 'Deleting temporary DSSS.dll.old file - attempt ' + IntToStr(Tries) +  ' of 10';
  until (not FileExists(OldDSSS)) or (Tries > 10); 
  if FileExists(OldDSSS) then
    ShowError('Unable to delete ' + OldDSSS + '. '#13#13'The ' +
      'file may still be in use. Try deleting it again once the installation finishes.');
end;

procedure RenameQMLogic;
begin
  DeleteOldQMLogic;
  if not RenameFile(ExpandConstant('{app}') + '\QMLogic.dll', ExpandConstant('{app}') + '\QMLogic.dll.old') then
    RaiseException('Unable to rename ' + ExpandConstant('{app}') + '\QMLogic.dll to QMLogic.dll.old.');
end;

procedure RenameDSSS;
begin
  DeleteOldDSSS;
  if not RenameFile(ExpandConstant('{app}') + '\DSSS.dll', ExpandConstant('{app}') + '\DSSS.dll.old') then
    RaiseException('Unable to rename ' + ExpandConstant('{app}') + '\DSSS.dll to DSSS.dll.old.');
end;

procedure ManageOldIISWebSite(Creating: Boolean); 
//For < IIS6, create or remove virtual directory
var
  VirtualDirAliasName: string;
  IIS,WebSite, WebServer, WebRoot, VDir: variant;
begin
  if Creating then
    VirtualDirAliasName := VirtualDirAliasPage.Values[0]
  else begin
  //Note: the virtual directory name is obtained from the folder name the uninstaller is run from. 
  //(It cannot be obtained at the time the uninstaller is built since the virtual dir is user defined.)
    VirtualDirAliasName := GetVirtualDirNameFromInstallDir('');
  end;
  
  //Create the main IIS COM Automation object 
  try
    IIS := CreateOleObject('IISNamespace');
  except
    RaiseException('Please install Microsoft IIS first.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;

  //Connect to the IIS server 
  WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');
  WebServer := WebSite.GetObject('IIsWebServer', IISServerNumber);
  WebRoot := WebServer.GetObject('IIsWebVirtualDir', 'Root');

  //(Re)create/remove a virtual dir 
  try
    WebRoot.Delete('IIsWebVirtualDir', VirtualDirAliasName);
    WebRoot.SetInfo();
  except
  end;

  if Creating then begin
    VDir := WebRoot.Create('IIsWebVirtualDir', VirtualDirAliasName);
    VDir.AccessRead := True;
    VDir.AccessScript := True;
    VDir.AccessExecute := True;
    VDir.AccessSource := True;
    VDir.AppFriendlyName := VirtualDirAliasName;
    VDir.Path := ExpandConstant('{app}');
    VDir.SetInfo();
    VDir.AppCreate2(1); // Pass in Application Protection (low - IIS process=0 / medium - pooled=2 / high - isolated=1)
    VDir.SetInfo();
  end;
end;

procedure DoBeforeInstall;
begin
  if ServerAppExists then begin //assume that website is installed correctly
    if GetIISVersion >= 6 then //older than IIS 6 not supported; UQ QML servers are running Windows Server 2003 Web Edition 
    begin
      RenameQMLogic;
    end
  end
  else begin
    if GetIISVersion < 6 then
      ManageOldIISWebSite(True)
    else
      CreateWebSite;
  end;
  WizardForm.StatusLabel.Caption := 'Installing files';
end;

procedure DoBeforeInstallDSSS;
begin
  RenameDSSS;
end;

procedure CreateCustomPages;
var
  DefaultVirDir: string;
begin
  VirtualDirAliasPage := CreateInputQueryPage(wpWelcome, 'Virtual Directory Setup',  
    'Virtual Directory Alias', 'Please specify the virtual directory alias, then click Next.');
  // Add items (False means it's not a password edit)
  VirtualDirAliasPage.Add('Alias:', False);
  DefaultVirDir := GetPreviousData('Alias', '');
  if DefaultVirDir = '' then
    DefaultVirDir := 'QMServerApp';
  VirtualDirAliasPage.Values[0] := DefaultVirDir;
  
  // Setup the QMLogic Service Credentials page
  ServiceCredentialsPage := CreateInputQueryPage(wpSelectComponents, 'QMLogic Service Setup',
    'QMLogic Service Credentials', 
    'Specify a Domain user and password used to run the QMLogic Service. ' +
    'These credentials are optional. If left blank, the service uses the Local System account.');
  ServiceCredentialsPage.Add('Username (in Domain\User format):', False);
  ServiceCredentialsPage.Add('Password:', True);
  ServiceCredentialsPage.Add('Reenter Password:', True);
end;

procedure RegisterPreviousData(PreviousDataKey: Integer);
begin
  SetPreviousData(PreviousDataKey, 'Alias', VirtualDirAliasPage.Values[0]);
end;

function GetVirtualDirAlias(Param: string): string;
begin
  if Assigned(VirtualDirAliasPage) then 
    Result := VirtualDirAliasPage.Values[0]
  else //it has not been collected from the custom page yet
    Result := '';
end;

function GetQMLogic2ServiceName(Param: string): string;
var
  Alias: string; 
begin
  Alias := GetPreviousData('Alias', '');
  if Alias = '' then
    Alias := GetVirtualDirAlias('');
  if Alias = '' then
    Alias := GetVirtualDirNameFromInstallDir('');
  Result := QMLOGIC2_SERVICE_NAME + Alias;
end;

function GetServiceRunAsUser(): string;
begin
  if Assigned(ServiceCredentialsPage) then 
    Result := ServiceCredentialsPage.Values[0]
  else // not assigned yet
    Result := '';
end;

function GetServiceRunAsPassword(): string;
begin
  if Assigned(ServiceCredentialsPage) then 
    Result := ServiceCredentialsPage.Values[1]
  else // not assigned yet
    Result := '';
end;

//Hijack the the users directory selection and append the virtual directory before it is set as the app constant
function NextButtonClick(CurPage: Integer): Boolean;
begin
  Result := True;
  if CurPage = wpReady then
    WizardForm.DirEdit.Text := WizardForm.DirEdit.Text + '\' + GetVirtualDirAlias('');

  if CurPage = ServiceCredentialsPage.ID then begin
    Result := (ServiceCredentialsPage.Values[1] = ServiceCredentialsPage.Values[2]);
    if not Result then
      ShowError('Passwords do not match. Please type the same password in both password boxes.');
  end; 

  if CurPage = VirtualDirAliasPage.ID then begin
    if Copy(UpperCase(VirtualDirAliasPage.Values[0]), Length(VirtualDirAliasPage.Values[0])-2, 3) <> 'APP' then begin
        ShowError('Please make the ''Virtual Directory Alias'' value end with ''App''.  For example: QMServerApp');
        Result := False;
      end;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo,
  MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: string): string;
var
  S: string;
begin
  S := MemoTypeInfo + NewLine;
  S := S + MemoComponentsInfo + NewLine;
  S := S + 'Virtual Directory Alias: ' + NewLine;
  S := S + Space + VirtualDirAliasPage.Values[0] + NewLine;
  S := S + 'Virtual Directory Path: ' + NewLine; 
  S := S + Space + ExpandConstant('{app}') + '\' + VirtualDirAliasPage.Values[0] + NewLine;
  if IsComponentSelected('qmlogic2') then begin
    S := S + 'QMLogic2 Service: ' + NewLine;
    S := S + Space + GetQMLogic2ServiceName('') + NewLine;
  end;
  S := S + MemoGroupInfo;
  Result := S;
end;

procedure InitializeWizard();
begin
  CreateCustomPages;
end;

function ShouldSkipPage(PageID: Integer): Boolean;
begin
{ TODO: Credentials is currently disabled until QMLogic2.exe gets /user and 
        /password switches. The credentials can be set using the Win services 
        manager in the meantime.
}
  if PageID = ServiceCredentialsPage.ID then
    Result := True; 
//    Result := not IsComponentSelected('qmlogic2');
end;

function VerifyMSXML3Installed: Boolean;
begin
  Result := RegValueExists(HKCR, 'CLSID\' + MSXMLDOMDocument3GUID, '');
  if not Result then
    ShowError('This program requires the Microsoft XML Parser (MSXML) version 3.' +#13+#10+
              'Please install MSXML 3 and then re-run this setup.  Note that IE 6 installs this component.');
end;

function RemoveIISSettings: Boolean;
var
  VirDirAliasName: string;
begin
  //Note: the virtual directory name is obtained from the folder name the uninstaller is run from. 
  //(It cannot be obtained at the time the uninstaller is built since the virtual dir is user defined.)
  VirDirAliasName := GetVirtualDirNameFromInstallDir('');

  if GetIISVersion >= 6 then begin
    if SuppressibleMsgBox('Do you want to delete the application pool, virtual directory and ' +
      'uninstall ISAPI and CGI extensions? The application pool name is: ' + VirDirAliasName + 
      'Pool. The virtual dir alias name is: ' + VirDirAliasName, mbConfirmation, MB_YESNO, IDYES) = IDYES 
    then begin
      //if GetIISVersion >= 7 then
      //  CleanUpIIS7Website(VirDirAliasName + 'Pool', VirDirAliasName)
      //else
        CleanUp(IISServerName, 'W3SVC/1/ROOT/' + VirDirAliasName, VirDirAliasName + 'Pool'); 
    end;
  end
  else begin
    ManageOldIISWebSite(False);
  end;

  Result := True;
end;

procedure UpdateProgressStatus(Msg: string);
begin
  if IsUninstaller then
    UninstallProgressForm.StatusLabel.Caption := Msg
  else
    WizardForm.StatusLabel.Caption := Msg;
end;

// QM Services
function StopQMService(SvcName: string): Boolean;
var
  Status: Boolean;
  ResultCode: Integer;
begin
  Result := True;
  UpdateProgressStatus('Stopping ' + SvcName +  ' service');
  Status := Exec('>', 'sc stop ' + SvcName, '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
  if Status then
    Result := Exec('>', 'sc delete ' + SvcName, '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
end;

function StopAndDeleteQMServices(): Boolean;
var
  SvcName: string;
begin
  SvcName := GetQMLogic2ServiceName('');
  Result := StopQMService(SvcName);
  Result := Result and StopQMService('QMProjMetricsService');
end;

function InitializeSetup: Boolean;
begin
  Result := True;
  if not IsAdminLoggedOn then begin
    ShowError(MSG_MUST_BE_ADMIN);
    Result := False;
  end;
  if Result then
    Result := VerifyMSXML3Installed;
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
  InstalledDLLVersion : string;
begin
  if CurStep = ssInstall then begin
    if IsComponentSelected('qmlogic2') and (not StopAndDeleteQMServices) then
      RaiseException(MSG_RUNNING_SERVICES);

    GetVersionNumbersString(ExpandConstant('{app}') + '\QMLogic.dll',InstalledDLLVersion);
    if ServerAppExists then begin //assume that website is installed correctly
      if SuppressibleMsgBox('The server version ' + InstalledDLLVersion + 
        ' is already installed in this directory, are you ' + 
        'sure you want to overwrite/update it with version '+ AppVersionToInstall + 
        ' ?', mbConfirmation, MB_YESNO, IDYES) = IDNO then
        WizardForm.Close;
    end;        
  end else if (CurStep = ssPostInstall) and (GetIISVersion >= 6) then begin   
    //Recycle the application pool so the DLL is replaced.
    RecycleAppPool(IISServerName, VirtualDirAliasPage.Values[0] + 'Pool');
    DeleteOldQMLogic;
  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then begin
    if not StopAndDeleteQMServices then
      ShowError('One or more Q Manager Windows services could not be stopped. ' +
        'The uninstall will attempt to continue.'); 
    if not RemoveIISSettings then 
      ShowError('The Q Manager IIS configuration settings could not be removed. ' +
        'The uninstall will attempt to continue.');
  end; 
end;

procedure AfterInstallQMLogic2();
var
  Status: Boolean;
  SvcName: string;
  ResultCode: Integer;
begin
  SvcName := GetQMLogic2ServiceName('');
  WizardForm.StatusLabel.Caption := 'Adding ' + SvcName + ' as a Windows Service.';

//  Using InstallService() doesn't set the sevice name correctly. To get all 
//  the service naming parts right, the service must be installed by running it 
//  with the /install switch.

  Status := Exec(ExpandConstant('{app}\' + SvcName + '.exe'), '/install /silent /name:' + SvcName, 
    '', SW_HIDE, ewWaitUntilTerminated, ResultCode); 

  if not Status then
    RaiseException('The ' + SvcName + ' Windows Service could not ' +
      'be installed. Verify the selected user and password are correct.');

  WizardForm.StatusLabel.Caption := 'Starting ' + SvcName + ' Windows Service.';
  Status := Exec('>', 'sc start ' + SvcName, '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
  if not Status then
    RaiseException('The ' + SvcName + ' Windows Service could not be started.');
end;

procedure AfterInstallQMProjMetrics();
var
  Status: Boolean;
  SvcName: string;
  ResultCode: Integer;
begin
  SvcName := 'QMProjectionMetricsService.exe';
  WizardForm.StatusLabel.Caption := 'Adding ' + SvcName + ' as a Windows Service.';

//  Using InstallService() doesn't set the sevice name correctly. To get all 
//  the service naming parts right, the service must be installed by running it 
//  with the /install switch.

  Status := Exec(ExpandConstant('{app}\' + SvcName), '/install /silent', 
    '', SW_HIDE, ewWaitUntilTerminated, ResultCode); 

  if not Status then
    RaiseException('The ' + SvcName + ' Windows Service could not ' +
      'be installed. Verify the selected user and password are correct.');

  WizardForm.StatusLabel.Caption := 'Starting ' + SvcName + ' Windows Service.';
  Status := Exec('>', 'sc start QMProjMetricsService', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
  if not Status then
    RaiseException('The ' + SvcName + ' Windows Service could not be started.');
end;
