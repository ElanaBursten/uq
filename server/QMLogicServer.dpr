program QMLogicServer;

{#ROGEN:QMServerLibrary2.rodl} // RemObjects: Careful, do not remove!

uses
  uROComInit,
  Forms,
  ActiveX,
  ComObj,
  SysUtils,
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  LogicDMu in 'LogicDMu.pas' {LogicDM: TDataModule},
  OdDataSetToXml in '..\common\OdDataSetToXml.pas',
  OdSQLBuilder in '..\common\OdSQLBuilder.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  ThreadSafeLoggerU in '..\common\ThreadSafeLoggerU.pas',
  OperationTracking in 'OperationTracking.pas',
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  ReportConfig in '..\reportengine\ReportConfig.pas',
  QMConst in '..\client\QMConst.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdAdoSqlXml in '..\common\OdAdoSqlXml.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdThreadTimer in '..\common\OdThreadTimer.pas',
  OdSystemStatus in '..\common\OdSystemStatus.pas',
  OdIntPacking in '..\common\OdIntPacking.pas',
  OdSqlXmlOutput in '..\common\OdSqlXmlOutput.pas',
  ADODB_TLB in '..\common\ADODB_TLB.pas',
  TimeSheetExport in 'TimeSheetExport.pas',
  OdUqInternet in '..\common\OdUqInternet.pas',
  PasswordRules in '..\common\PasswordRules.pas',
  BetterADODataSet in '..\thirdparty\BetterADO\BetterADODataSet.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  ServerAttachmentDMu in 'ServerAttachmentDMu.pas' {ServerAttachment: TDataModule},
  TicketImage in '..\common\TicketImage.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  OdFtp in '..\common\OdFtp.pas',
  HoursCalc in '..\common\HoursCalc.pas',
  HoursDataset in '..\common\HoursDataset.pas',
  standaloneServer in 'standaloneServer.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  QMServerLibrary2_Intf in 'QMServerLibrary2_Intf.pas',
  QMServerLibrary2_Invk in 'QMServerLibrary2_Invk.pas',
  QMService2_Impl in 'QMService2_Impl.pas',
  QMServerProcessU in 'QMServerProcessU.pas',
  Hashes in '..\thirdparty\Hashes.pas';

{$R *.res}
{$R RODLFile.RES} // RemObjects: Careful, do not remove!

function GetServerIniName: string;
begin
  Result := ExtractFilePath(ParamStr(0)) + 'QMServer.ini';
end;

begin
  LogicDmIniName := GetServerIniName;
  Application.Initialize;
  //Application.MainFormOnTaskbar := True;
  Application.CreateForm(TStandaloneServerForm, StandaloneServerForm);
  Application.Run;
end.


