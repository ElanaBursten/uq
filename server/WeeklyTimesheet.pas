unit WeeklyTimesheet;

interface

uses
  DB, SysUtils, Classes, DateUtils, QMConst;

const
  NoDayIdx = -1;
  WorkSpanMax = (TimesheetNumWorkPeriods -1);
  CalloutSpanMax = (TimesheetNumCalloutPeriods - 1);

type
  TTimesheetDay = class;

  TWeeklyTimesheet = class(TObject)
  private
    FWorkerEmpID: Integer;
    FEndDate: TDateTime;
    FBeginDate: TDateTime;
    FCurrentTimeData: TDataSet;
    FOriginalTimeData: TDataSet;
    FCurrentDaysArray: array[DayMonday..DaySunday] of TTimesheetDay;
    FOriginalDaysArray: array[DayMonday..DaySunday] of TTimesheetDay;
    FMinimumChangeDateTime: TDateTime;
    FMaximumChangeDateTime: TDateTime;
    FLatestChangeEntryID: Integer;
    function GetCurrentDays(Index: Integer): TTimesheetDay;
    function GetOriginalDays(Index: Integer): TTimesheetDay;
    procedure ClearWeek;
    procedure SetupWeek;
    function AddOriginalWorkDay(const DayIdx, NewTSEId: Integer): Boolean;
    function GetHourChangesAsTableRows(const DayIdx1, DayIdx2, DayIdx3, DayIdx4: Integer): string;
    function TimesDiffer(const Time1, Time2: TDateTime): Boolean;
    procedure SummarizeChanges(SummaryData: TDataSet);
    function AnyHoursOfType(const StartDayIdx, EndDayIdx: Integer; const HourType: string): Boolean;
  public
    property WorkerEmpID: Integer read FWorkerEmpID write FWorkerEmpID;
    property BeginDate: TDateTime read FBeginDate write FBeginDate;
    property EndDate: TDateTime read FEndDate write FEndDate;
    property CurrentDays[Index: Integer]: TTimesheetDay read GetCurrentDays;
    property OriginalDays[Index: Integer]: TTimesheetDay read GetOriginalDays;
    property MinimumChangeDateTime: TDateTime read FMinimumChangeDateTime;
    property MaximumChangeDateTime: TDateTime read FMaximumChangeDateTime;
    property LatestChangeEntryID: Integer read FLatestChangeEntryID;
    function AddWorkDay(WorkDate: TDateTime): Integer;
    function GetTimeChangesAsHTML: string;
    function AnyCurrentHoursForDay(const DayIdx: Integer): Boolean;
    function AnyOriginalHoursForDay(const DayIdx: Integer): Boolean;

    constructor CreateFromDataSets(WeekEndingDate: TDateTime; CurrentTimeData, OriginalTimeData, ChangeSummaryData: TDataSet);
    destructor Destroy; override;
  end;

  TTimesheetDay = class(TObject)
  private
    FEntryID: Integer;
    FWorkDate: TDateTime;
    FVacationHours: Double;
    FBereavementHours: Double;
    FHolidayHours: Double;
    FJuryHours: Double;
    FLeaveHours: Double;
    FPTOHours: Double;
    FRegularHours: Double;
    FCalloutHours: Double;
    FOTHours: Double;
    FDTHours: Double;
    FEntryBy: Integer;
    FStartTimes: array[0..WorkSpanMax] of TDateTime;
    FStopTimes: array[0..WorkSpanMax] of TDateTime;
    FCalloutStartTimes: array[0..CalloutSpanMax] of TDateTime;
    FCalloutStopTimes: array[0..CalloutSpanMax] of TDateTime;
    FHasPriorEntry: Boolean;
    FEntryDate: TDateTime;
    FReasonChanged: string;
    FApproveDate: TDateTime;
    FFinalApproveDate: TDateTime;
    function GetTotalWorkHours: Double;
    function GetTotalOtherHours: Double;
    procedure SetupDay(const WorkDate: TDateTime; Data: TDataSet);
    procedure InitializeDay;
    function GetCalloutStartTimes(Index: Integer): TDateTime;
    function GetCalloutStopTimes(Index: Integer): TDateTime;
    function GetStartTimes(Index: Integer): TDateTime;
    function GetStopTimes(Index: Integer): TDateTime;
    function GetTotalHours: Double;
    function FormatTime(const Hours: TDateTime; const Encoded: Boolean = True): string;
    function FormatHours(const Hours: Double; const Encoded: Boolean = True): string;
  public
    property EntryID: Integer read FEntryID write FEntryID;
    property WorkDate: TDateTime read FWorkDate write FWorkDate;
    property EntryBy: Integer read FEntryBy write FEntryBy;
    property EntryDate: TDateTime read FEntryDate write FEntryDate;
    property StartTimes[Index: Integer]: TDateTime read GetStartTimes;
    property StopTimes[Index: Integer]: TDateTime read GetStopTimes;
    property CalloutStartTimes[Index: Integer]: TDateTime read GetCalloutStartTimes;
    property CalloutStopTimes[Index: Integer]: TDateTime read GetCalloutStopTimes;
    property VacationHours: Double read FVacationHours write FVacationHours;
    property LeaveHours: Double read FLeaveHours write FLeaveHours;
    property PTOHours: Double read FPTOHours write FPTOHours;
    property BereavementHours: Double read FBereavementHours write FBereavementHours;
    property HolidayHours: Double read FHolidayHours write FHolidayHours;
    property JuryHours: Double read FJuryHours write FJuryHours;
    property RegularHours: Double read FRegularHours write FRegularHours;
    property OTHours: Double read FOTHours write FOTHours;
    property DTHours: Double read FDTHours write FDTHours;
    property CalloutHours: Double read FCalloutHours write FCalloutHours;
    property TotalWorkHours: Double read GetTotalWorkHours;
    property TotalOtherHours: Double read GetTotalOtherHours;
    property TotalHours: Double read GetTotalHours;
    property HasPriorEntry: Boolean read FHasPriorEntry write FHasPriorEntry;
    property ReasonChanged: string read FReasonChanged write FReasonChanged;
    property ApproveDate: TDateTime read FApproveDate write FApproveDate;
    property FinalApproveDate: TDateTime read FFinalApproveDate write FFinalApproveDate;
    function HasAnyWorkHours: Boolean;
    function HasTimeInSlot(const TimeSlot: Integer): Boolean;
    function HasCalloutTimeInSlot(const TimeSlot:Integer): Boolean;
    function IsTimeSlotEmpty(const TimeSlot: Integer): Boolean;
    function IsCalloutTimeSlotEmpty(const TimeSlot: Integer): Boolean;
    function StartTimeAsString(const TimeSlot: Integer): string;
    function StopTimeAsString(const TimeSlot: Integer): string;
    function CalloutStartTimeAsString(const TimeSlot: Integer): string;
    function CalloutStopTimeAsString(const TimeSlot: Integer): string;
    function VacationHoursAsString: string;
    function LeaveHoursAsString: string;
    function PTOHoursAsString: string;
    function BereavementHoursAsString: string;
    function HolidayHoursAsString: string;
    function JuryHoursAsString: string;
    function IsApproved: Boolean;
    function HasAnyHoursOfType(const HourType: string): Boolean;

    constructor Create(WorkDate: TDateTime; TimesheetData: TDataSet);
    destructor Destroy; override;
  end;

implementation

uses
  OdMiscUtils;

const
  HTMLHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> ' +
    '<html xmlns="http://www.w3.org/1999/xhtml"> ' +
    '<head> ' +
    '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> ' +
    '<title>Altered Hours Notice</title> ' +
    '<style type="text/css"> ' +
    '<!-- ' +
    '  .NonWorkHoursType { ' +
    '    border: 1px solid #000; ' +
    '    text-align: left; ' +
    '    font-size: 80%; ' +
    '    padding: 0px; ' +
    '    margin: 1px; ' +
    '    background-color: #FFF; ' +
    '    width: 50px; ' +
    '  } ' +
    '  .HoursData { ' +
    '    border: 1px solid #000; ' +
    '    text-align: right; ' +
    '    padding: 0px; ' +
    '    margin: 1px; ' +
    '    background-color: #FFF; ' +
    '    width: 50px; ' +
    '  } ' +
    '  .HoursDataHighlight { ' +
    '    border: 1px solid #000; ' +
    '    text-align: right; ' +
    '    padding: 0px; ' +
    '    margin: 1px; ' +
    '    background-color: #FF6; ' +
    '    width: 50px; ' +
    '  } ' +
    '  .HoursDataNoHours { ' +
    '    border: 1px solid #000; ' +
    '    text-align: right; ' +
    '    padding: 0px; ' +
    '    margin: 1px; ' +
    '    background-color: #999; ' +
    '    width: 50px; ' +
    '  } ' +
    '  .HoursDataNoDay { ' +
    '    text-align: right; ' +
    '    padding: 0px; ' +
    '    margin: 1px; ' +
    '    background-color: #FFF; ' +
    '    width: 50px; ' +
    '  } ' +
    '  --> ' +
    '  </style></head>';

  TableTop = '<body> ' +
    '  <table width="807" cellpadding="0" cellspacing="0"> ' +
    '    <col width="51" span="4" /> ' +
    '    <col width="4" /> ' +
    '    <col width="51" span="4" /> ' +
    '    <col width="5" /> ' +
    '    <col width="51" span="4" /> ' +
    '    <col width="5" /> ' +
    '    <col width="51" span="4" /> ';

  DateHeaderRow1 = '<tr> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td width="2"><div align="center"></div></td> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td width="1"><div align="center"></div></td> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td width="2"></td></tr>' +
    '<tr> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td></td> <td></td> <td></td> <td></td> <td></td> ' +
    '</tr>';

  DividerRow = '<tr><td colspan="19">&nbsp;</td></tr>';

  DateHeaderRow2 = '<tr> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td><div align="center"></div></td>' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="4"><div align="center">%s</div></td> ' +
    '</tr><tr> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '<td><div align="center"></div></td> ' +
    '<td colspan="2"><div align="center">Original</div></td> ' +
    '<td colspan="2"><div align="center">Altered</div></td> ' +
    '</tr>';

  TableBottom = '</table><p>%s</p></body></html>';

  OtherHourTypes: array[0..5] of string = ('Vac:','Leave:','Bereave:','Hol:','Jury:','PTO:');

{ TWeeklyTimesheet }

function TWeeklyTimesheet.AddOriginalWorkDay(const DayIdx, NewTSEID: Integer): Boolean;
var
  Day: TTimesheetDay;
begin
  Assert(NewTSEID > 0, 'NewTSEID must be greater than 0');
  Day := nil;
  if FOriginalTimeData.Locate('new_entry_id', NewTSEID, []) then begin
    Day := TTimesheetDay.Create(FOriginalTimeData.FieldByName('work_date').AsDateTime,
      FOriginalTimeData);
    FOriginalDaysArray[DayIdx] := Day;
  end;
  Result := Assigned(Day);
end;

function TWeeklyTimesheet.AddWorkDay(WorkDate: TDateTime): Integer;
var
  DayIdx: Integer;
  Day: TTimesheetDay;
begin
  DayIdx := Trunc(WorkDate) - Trunc(FBeginDate) + 1;
  Assert(DayIdx in [DayMonday..DaySunday],
    Format('DayIdx %d must be between %d and %d in AddWorkDay', [DayIdx, DayMonday, DaySunday]));

  Day := TTimesheetDay.Create(WorkDate, FCurrentTimeData);
  if Assigned(FOriginalTimeData) and (Day.EntryID > 0) then
    Day.HasPriorEntry := AddOriginalWorkDay(DayIdx, Day.EntryID);
  FCurrentDaysArray[DayIdx] := Day;
  Result := DayIdx;
end;

function TWeeklyTimesheet.AnyCurrentHoursForDay(const DayIdx: Integer): Boolean;
begin
  Result := Assigned(CurrentDays[DayIdx]) and CurrentDays[DayIdx].HasAnyWorkHours;
end;

function TWeeklyTimesheet.AnyHoursOfType(const StartDayIdx, EndDayIdx: Integer; const HourType: string): Boolean;
var
  Day: TTimesheetDay;
  I: Integer;
begin
  Result := False;
  for I := StartDayIdx to EndDayIdx do begin
    Day := CurrentDays[I];
    if Assigned(Day) and Day.HasAnyHoursOfType(HourType) then
      Result := True
    else begin
      Day := OriginalDays[I];
      if Assigned(Day) and Day.HasAnyHoursOfType(HourType) then
        Result := True;
    end;
    if Result then
      Break;
  end;
end;

function TWeeklyTimesheet.AnyOriginalHoursForDay(const DayIdx: Integer): Boolean;
begin
  Result := CurrentDays[DayIdx].HasPriorEntry and OriginalDays[DayIdx].HasAnyWorkHours;
end;

constructor TWeeklyTimesheet.CreateFromDataSets(WeekEndingDate: TDateTime;
  CurrentTimeData, OriginalTimeData, ChangeSummaryData: TDataSet);
begin
  Assert(Assigned(CurrentTimeData), 'CurrentTimeData is unassigned in TWeeklyTimesheet.CreateFromDataSets');
  Assert(Assigned(OriginalTimeData), 'OriginalTimeData is unassigned in TWeeklyTimesheet.CreateFromDataSets');
  Assert(Assigned(ChangeSummaryData), 'ChangeSummaryData is unassigned in TWeeklyTimesheet.CreateFromDataSets');
  Assert(DayOfTheWeek(WeekEndingDate) = DaySunday, 'WeekEndingDate must be a Sunday');

  FCurrentTimeData := CurrentTimeData;
  FOriginalTimeData := OriginalTimeData;
  EndDate := WeekEndingDate;
  BeginDate := StartOfTheWeek(EndDate);

  if not CurrentTimeData.IsEmpty then
    WorkerEmpID := CurrentTimeData.FieldByName('work_emp_id').AsInteger;

  SetupWeek;
  SummarizeChanges(ChangeSummaryData);
end;

destructor TWeeklyTimesheet.Destroy;
begin
  ClearWeek;
  inherited;
end;

procedure TWeeklyTimesheet.SetupWeek;
var
  I: Integer;
  WorkDate: TDateTime;
begin
  for I := 0 to 6 do begin
    WorkDate := BeginDate + I;
    AddWorkDay(WorkDate);
  end;
end;

procedure TWeeklyTimesheet.SummarizeChanges(SummaryData: TDataSet);
begin
  FMinimumChangeDateTime := EncodeDate(2100, 12, 31);
  FMaximumChangeDateTime := 0;
  FLatestChangeEntryID := 0;
  if not SummaryData.Eof then begin
    FMaximumChangeDateTime := SummaryData.FieldByName('last_change_date').AsDateTime;
    if not SummaryData.FieldByName('first_change_date').IsNull then
      FMinimumChangeDateTime := SummaryData.FieldByName('first_change_date').AsDateTime;
    if not SummaryData.FieldByName('last_change_tse_id').IsNull then
      FLatestChangeEntryID := SummaryData.FieldByName('last_change_tse_id').AsInteger;
  end;
end;

procedure TWeeklyTimesheet.ClearWeek;
var
  I: Integer;
  Day: TTimesheetDay;
begin
  for I := DayMonday to DaySunday do begin
    Day := CurrentDays[I];
    FreeAndNil(Day);
    Day := OriginalDays[I];
    FreeAndNil(Day);
  end;
end;

function TWeeklyTimesheet.GetCurrentDays(Index: Integer): TTimesheetDay;
begin
  Assert((Index >= Low(FCurrentDaysArray)) and (Index <= High(FCurrentDaysArray)), Format('Index %d is out of range in GetCurrentDays', [Index]));
  Result := FCurrentDaysArray[Index];
end;

function TWeeklyTimesheet.GetOriginalDays(Index: Integer): TTimesheetDay;
begin
  Assert((Index >= Low(FOriginalDaysArray)) and (Index <= High(FOriginalDaysArray)), Format('Index %d is out of range in GetOriginalDays', [Index]));
  Result := FOriginalDaysArray[Index];
end;

function TWeeklyTimesheet.TimesDiffer(const Time1, Time2: TDateTime): Boolean;
begin
  Result := MinuteSpan(Time1, Time2) <> 0;
end;

function TWeeklyTimesheet.GetHourChangesAsTableRows(const DayIdx1, DayIdx2, DayIdx3, DayIdx4: Integer): string;
const
  HoursDataCell = '<td class="%s">%s</td>';
  DividerColumnDataCell = '<td></td>';
  BlankDataCell = '&nbsp;';

  function NotApproved(const DayIdx: Integer): Boolean;
  begin
    Result := not CurrentDays[DayIdx].IsApproved;
  end;

  function NotEnteredByWorker(const DayIdx: Integer): Boolean;
  begin
    Result := (CurrentDays[DayIdx].EntryBy <> WorkerEmpID);
  end;

  function StartTimesDiffer(DayIdx, TimeSlot: Integer): Boolean;
  begin
    Result := NotEnteredByWorker(DayIdx) and NotApproved(DayIdx);
    if Result and CurrentDays[DayIdx].HasPriorEntry then
      Result := TimesDiffer(CurrentDays[DayIdx].StartTimes[TimeSlot], OriginalDays[DayIdx].StartTimes[TimeSlot]);
  end;

  function StopTimesDiffer(DayIdx, TimeSlot: Integer): Boolean;
  begin
    Result := NotEnteredByWorker(DayIdx) and NotApproved(DayIdx);
    if Result and CurrentDays[DayIdx].HasPriorEntry then
      Result := TimesDiffer(CurrentDays[DayIdx].StopTimes[TimeSlot], OriginalDays[DayIdx].StopTimes[TimeSlot]);
  end;

  function CalloutStartTimesDiffer(DayIdx, TimeSlot: Integer): Boolean;
  begin
    Result := NotEnteredByWorker(DayIdx) and NotApproved(DayIdx);
    if Result and CurrentDays[DayIdx].HasPriorEntry then
      Result := TimesDiffer(CurrentDays[DayIdx].CalloutStartTimes[TimeSlot], OriginalDays[DayIdx].CalloutStartTimes[TimeSlot]);
  end;

  function CalloutStopTimesDiffer(DayIdx, TimeSlot: Integer): Boolean;
  begin
    Result := NotEnteredByWorker(DayIdx) and NotApproved(DayIdx);
    if Result and CurrentDays[DayIdx].HasPriorEntry then
      Result := TimesDiffer(CurrentDays[DayIdx].CalloutStopTimes[TimeSlot], OriginalDays[DayIdx].CalloutStopTimes[TimeSlot]);
  end;

  function GetTableDataForRegularHours(const DayIdx, TimeSlot: Integer): string;
  var
    StartTimeStyle: string;
    StopTimeStyle: string;
    StartTimeString: string;
    StopTimeString: string;
  begin
    Result := '';
    StartTimeString := BlankDataCell;
    StopTimeString := BlankDataCell;
    StartTimeStyle := 'HoursDataNoDay';
    StopTimeStyle := 'HoursDataNoDay';

    // blank data cells without a border
    if DayIdx = NoDayIdx then begin
      Result := Format(HoursDataCell + HoursDataCell + HoursDataCell +
        HoursDataCell + DividerColumnDataCell, [StartTimeStyle, '', StopTimeStyle, '',
        StartTimeStyle, '', StopTimeStyle, '']);
      Exit;
    end;

    if (CurrentDays[DayIdx].HasAnyWorkHours = False) and (CurrentDays[DayIdx].HasPriorEntry = False) then begin
      StartTimeStyle := 'HoursDataNoHours';
      StopTimeStyle := 'HoursDataNoHours';
    end else begin
      StartTimeStyle := 'HoursData';
      StopTimeStyle := 'HoursData';
    end;

    // no time entered for the selected day & time slot
    if (CurrentDays[DayIdx].IsTimeSlotEmpty(TimeSlot)) and
      ((not CurrentDays[DayIdx].HasPriorEntry) or OriginalDays[DayIdx].IsTimeSlotEmpty(TimeSlot)) then begin
      Result := Format(HoursDataCell + HoursDataCell + HoursDataCell +
        HoursDataCell + DividerColumnDataCell, [StartTimeStyle, BlankDataCell,
        StopTimeStyle, BlankDataCell, StartTimeStyle, BlankDataCell,
        StopTimeStyle, BlankDataCell]);
      Exit;
    end;

    if CurrentDays[DayIdx].HasAnyWorkHours then begin
      // check if the start or stop hours changed for the slot
      if StartTimesDiffer(DayIdx, TimeSlot) then
        StartTimeStyle := 'HoursDataHighlight';
      if StopTimesDiffer(DayIdx, TimeSlot) then
        StopTimeStyle := 'HoursDataHighlight';
    end else begin
      StartTimeStyle := 'HoursDataNoHours';
      StopTimeStyle := 'HoursDataNoHours';
    end;

    if StringInArray('HoursDataHighlight', [StartTimeStyle, StopTimeStyle]) then begin
      // some hours were altered; put the orig start/stop hrs in the first 2 cells & the current hrs in the next 2
      StartTimeString := BlankDataCell;
      StopTimeString := BlankDataCell;
      if CurrentDays[DayIdx].HasPriorEntry then begin
        StartTimeString := OriginalDays[DayIdx].StartTimeAsString(TimeSlot);
        StopTimeString := OriginalDays[DayIdx].StopTimeAsString(TimeSlot);
      end;
      Result := Result + Format(HoursDataCell + HoursDataCell,
        [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);
      StartTimeString := CurrentDays[DayIdx].StartTimeAsString(TimeSlot);
      StopTimeString := CurrentDays[DayIdx].StopTimeAsString(TimeSlot);
    end else begin
      // no hours were altered; put the current start/stop hrs in the first 2 cells & blanks in the next 2
      StartTimeString := CurrentDays[DayIdx].StartTimeAsString(TimeSlot);
      StopTimeString := CurrentDays[DayIdx].StopTimeAsString(TimeSlot);
      Result := Result + Format(HoursDataCell + HoursDataCell,
        [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);

      StartTimeString := BlankDataCell;
      StopTimeString := BlankDataCell;
    end;
    Result := Result + Format(HoursDataCell + HoursDataCell + DividerColumnDataCell,
      [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);
  end;

  function GetTableDataForCalloutHours(const DayIdx, TimeSlot: Integer): string;
  var
    StartTimeStyle: string;
    StopTimeStyle: string;
    StartTimeString: string;
    StopTimeString: string;
  begin
    Result := '';
    StartTimeString := BlankDataCell;
    StopTimeString := BlankDataCell;
    StartTimeStyle := 'HoursDataNoDay';
    StopTimeStyle := 'HoursDataNoDay';
    if DayIdx = NoDayIdx then begin
      Result := Format(HoursDataCell + HoursDataCell + HoursDataCell +
        HoursDataCell + DividerColumnDataCell, [StartTimeStyle, '', StopTimeStyle, '',
        StartTimeStyle, '', StopTimeStyle, '']);
      Exit;
    end;

    if (CurrentDays[DayIdx].HasAnyWorkHours = False) and (CurrentDays[DayIdx].HasPriorEntry = False) then begin
      StartTimeStyle := 'HoursDataNoHours';
      StopTimeStyle := 'HoursDataNoHours';
    end else begin
      StartTimeStyle := 'HoursData';
      StopTimeStyle := 'HoursData';
    end;

    // no callout time entered for selected day & slot
    if (CurrentDays[DayIdx].IsCalloutTimeSlotEmpty(TimeSlot)) and
       ((not CurrentDays[DayIdx].HasPriorEntry) or (OriginalDays[DayIdx].IsCalloutTimeSlotEmpty(TimeSlot))) then begin
        Result := Format(HoursDataCell + HoursDataCell + HoursDataCell +
          HoursDataCell + DividerColumnDataCell, [StartTimeStyle, BlankDataCell,
          StopTimeStyle, BlankDataCell, StartTimeStyle, BlankDataCell,
          StopTimeStyle, BlankDataCell]);
       Exit;
    end;

    if CurrentDays[DayIdx].HasAnyWorkHours then begin
      // check if the callout start or stop hours changed for the slot
      if CalloutStartTimesDiffer(DayIdx, TimeSlot) then
        StartTimeStyle := 'HoursDataHighlight';
      if CalloutStopTimesDiffer(DayIdx, TimeSlot) then
        StopTimeStyle := 'HoursDataHighlight';
    end else begin
      StartTimeStyle := 'HoursDataNoHours';
      StopTimeStyle := 'HoursDataNoHours';
    end;

    if StringInArray('HoursDataHighlight', [StartTimeStyle, StopTimeStyle]) then begin
      StartTimeString := BlankDataCell;
      StopTimeString := BlankDataCell;
      // load the callout hours prior to the change in the first 2 columns (Original Time)
      if CurrentDays[DayIdx].HasPriorEntry then begin
        StartTimeString := 'co ' + OriginalDays[DayIdx].CalloutStartTimeAsString(TimeSlot);
        StopTimeString := 'co ' + OriginalDays[DayIdx].CalloutStopTimeAsString(TimeSlot);
      end;
      Result := Result + Format(HoursDataCell + HoursDataCell,
        [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);

      // load the changed hours entered by manager in the next 2 columns (Altered Time)
      StartTimeString := 'co ' + CurrentDays[DayIdx].CalloutStartTimeAsString(TimeSlot);
      StopTimeString := 'co ' + CurrentDays[DayIdx].CalloutStopTimeAsString(TimeSlot);
    end else begin
      // load the hours entered by the user in the first 2 columns
      StartTimeString := 'co ' + CurrentDays[DayIdx].CalloutStartTimeAsString(TimeSlot);
      StopTimeString := 'co ' + CurrentDays[DayIdx].CalloutStopTimeAsString(TimeSlot);
      Result := Result + Format(HoursDataCell + HoursDataCell,
        [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);

      // load blanks for the Altered Time, because nothing changed
      StartTimeString := BlankDataCell;
      StopTimeString := BlankDataCell;
    end;
    Result := Result + Format(HoursDataCell + HoursDataCell + DividerColumnDataCell,
      [StartTimeStyle, StartTimeString, StopTimeStyle, StopTimeString]);
  end;

  function GetTableDataForNonWorkingHours(const DayIdx: Integer; const HoursType: string): string;
  var
    HoursStyle: string;
    CurrentHours, OrigHours: string;
  begin
    Result := '';
    CurrentHours := BlankDataCell;
    OrigHours := BlankDataCell;

    if HoursType = 'Vac:' then begin
      CurrentHours := CurrentDays[DayIdx].VacationHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].VacationHoursAsString;
    end;
    if HoursType = 'Leave:' then begin
      CurrentHours := CurrentDays[DayIdx].LeaveHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].LeaveHoursAsString;
    end;
    if HoursType = 'PTO:' then begin
      CurrentHours := CurrentDays[DayIdx].PTOHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].PTOHoursAsString;
    end;
    if HoursType = 'Bereave:' then begin
      CurrentHours := CurrentDays[DayIdx].BereavementHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].BereavementHoursAsString;
    end;
    if HoursType = 'Hol:' then begin
      CurrentHours := CurrentDays[DayIdx].HolidayHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].HolidayHoursAsString;
    end;
    if HoursType = 'Jury:' then begin
      CurrentHours := CurrentDays[DayIdx].JuryHoursAsString;
      if CurrentDays[DayIdx].HasPriorEntry then
        OrigHours := OriginalDays[DayIdx].JuryHoursAsString;
    end;

    HoursStyle := 'HoursData';
    if CurrentHours <> OrigHours then
      HoursStyle := 'HoursDataHighlight';

    Result := Format('<td class="NonWorkHoursType">%s</td><td class="%s">%s</td><td class="HoursData">&nbsp;</td><td class="%s">%s</td><td></td>',
      [HoursType, HoursStyle, OrigHours, HoursStyle, CurrentHours]);
  end;

  function GetHourTotalsAsTableRow: string;
  const
    HoursTotalCells = '<td>Hours:</td><td align="right">%s</td><td>&nbsp;</td><td align="right">%s</td><td></td>';
    EmptyDayCells = '<td></td><td></td><td></td><td></td><td></td>';
  var
    TotalRow: string;
    CurrentHourTotals: array[1..4] of string;
    OrigHourTotals: array[1..4] of string;
    I: Integer;
    DayIdx: Integer;
  begin
    for I := 1 to 4 do begin
      CurrentHourTotals[I] := '&nbsp;';
      OrigHourTotals[I] := '&nbsp;';
    end;

    for I := 1 to 4 do begin
      DayIdx := DayIdx1 + I - 1;
      if (I = 4) and (DayIdx4 = NoDayIdx) then
        Break;

      if AnyCurrentHoursForDay(DayIdx) then begin
        if AnyOriginalHoursForDay(DayIdx) then begin
          CurrentHourTotals[I] := FormatFloat('0.0', CurrentDays[DayIdx].TotalHours);
          OrigHourTotals[I] := FormatFloat('0.0', OriginalDays[DayIdx].TotalHours);
        end else if NotEnteredByWorker(DayIdx) then
          CurrentHourTotals[I] := FormatFloat('0.0', CurrentDays[DayIdx].TotalHours)
        else
          OrigHourTotals[I] := FormatFloat('0.0', CurrentDays[DayIdx].TotalHours);
      end;
    end;

    TotalRow := Format(HoursTotalCells + HoursTotalCells + HoursTotalCells,
      [OrigHourTotals[1], CurrentHourTotals[1],
      OrigHourTotals[2], CurrentHourTotals[2],
      OrigHourTotals[3], CurrentHourTotals[3]]);
    if DayIdx4 <> NoDayIdx then
      TotalRow := TotalRow + Format(HoursTotalCells, [OrigHourTotals[4], CurrentHourTotals[4]])
    else
      TotalRow := TotalRow + EmptyDayCells;
    Result := '<tr>' + TotalRow + '</tr>';
  end;

  function CurrentDayHasTimeInSlot(const DayIdx, TimeIdx: Integer; const CalloutTime: Boolean): Boolean;
  begin
    Result := (DayIdx <> NoDayIdx) and Assigned(CurrentDays[DayIdx]);
    if Result then begin
      if CalloutTime then
        Result := CurrentDays[DayIdx].HasCalloutTimeInSlot(TimeIdx)
      else
        Result := CurrentDays[DayIdx].HasTimeInSlot(TimeIdx);
    end;
  end;

  function OriginalDayHasTimeInSlot(const DayIdx, TimeIdx: Integer; const CalloutTime: Boolean): Boolean;
  begin
    Result := (DayIdx <> NoDayIdx) and Assigned(OriginalDays[DayIdx]);
    if Result then begin
      if CalloutTime then
        Result := OriginalDays[DayIdx].HasCalloutTimeInSlot(TimeIdx)
      else
        Result := OriginalDays[DayIdx].HasTimeInSlot(TimeIdx);
    end;
  end;

var
  TableRow: string;
  HourType: string;
  TimeIdx: Integer;
  MaxDayIdx: Integer;
  I: Integer;
  J: Integer;
begin
  if DayIdx4 = NoDayIdx then
    MaxDayIdx := DayIdx3
  else
    MaxDayIdx := DayIdx4;

  for TimeIdx := 0 to WorkSpanMax do begin // regular time changes
    if CurrentDayHasTimeInSlot(DayIdx1, TimeIdx, False) or
      CurrentDayHasTimeInSlot(DayIdx2, TimeIdx, False) or
      CurrentDayHasTimeInSlot(DayIdx3, TimeIdx, False) or
      CurrentDayHasTimeInSlot(DayIdx4, TimeIdx, False) or
      OriginalDayHasTimeInSlot(DayIdx1, TimeIdx, False) or
      OriginalDayHasTimeInSlot(DayIdx2, TimeIdx, False) or
      OriginalDayHasTimeInSlot(DayIdx3, TimeIdx, False) or
      OriginalDayHasTimeInSlot(DayIdx4, TimeIdx, False) then begin
      TableRow := GetTableDataForRegularHours(DayIdx1, TimeIdx);
      TableRow := TableRow + GetTableDataForRegularHours(DayIdx2, TimeIdx);
      TableRow := TableRow + GetTableDataForRegularHours(DayIdx3, TimeIdx);
      TableRow := TableRow + GetTableDataForRegularHours(DayIdx4, TimeIdx);
      Result := Result + '<tr>' + TableRow + '</tr>';
    end;
  end;

  for TimeIdx := 0 to CalloutSpanMax do begin // callout time changes
    if CurrentDayHasTimeInSlot(DayIdx1, TimeIdx, True) or
      CurrentDayHasTimeInSlot(DayIdx2, TimeIdx, True) or
      CurrentDayHasTimeInSlot(DayIdx3, TimeIdx, True) or
      CurrentDayHasTimeInSlot(DayIdx4, TimeIdx, True) or
      OriginalDayHasTimeInSlot(DayIdx1, TimeIdx, True) or
      OriginalDayHasTimeInSlot(DayIdx2, TimeIdx, True) or
      OriginalDayHasTimeInSlot(DayIdx3, TimeIdx, True) or
      OriginalDayHasTimeInSlot(DayIdx4, TimeIdx, True) then begin
      TableRow := GetTableDataForCalloutHours(DayIdx1, TimeIdx);
      TableRow := TableRow + GetTableDataForCalloutHours(DayIdx2, TimeIdx);
      TableRow := TableRow + GetTableDataForCalloutHours(DayIdx3, TimeIdx);
      TableRow := TableRow + GetTableDataForCalloutHours(DayIdx4, TimeIdx);
      Result := Result + '<tr>' + TableRow + '</tr>';
    end;
  end;

  // vacation, leave, bereavement, jury, holiday, pto hours
  for J := 0 to High(OtherHourTypes) do begin
    HourType := OtherHourTypes[J];
    TableRow := '';
    if AnyHoursOfType(DayIdx1, MaxDayIdx, HourType) then begin
      for I := 0 to 3 do begin
        if (I < 3) or (DayIdx4 <> NoDayIdx) then
          TableRow := TableRow + GetTableDataForNonWorkingHours(DayIdx1 + I, HourType);
      end;
    end;
    Result := Result + '<tr>' + TableRow + '</tr>';
  end;

  // total hours
  Result := Result + GetHourTotalsAsTableRow;
end;

function TWeeklyTimesheet.GetTimeChangesAsHTML: string;

  function GetReasonsForChange: string;
  var
    i: Integer;
  begin
    Result := '';
    for i := DayMonday to DaySunday do begin
      if NotEmpty(CurrentDays[i].ReasonChanged) then
        Result := Result + '<br />' +
          FormatDateTime('mm/dd: ', CurrentDays[i].WorkDate) +
          CurrentDays[i].ReasonChanged;
    end;
    if IsEmpty(Result) then
      Result := '&nbsp;'
    else
      Result := '<b>Reasons for Changes:</b><br />' + Result;
  end;

var
  HTMLContent: TStringList;
  Reasons: string;
begin
  Result := '';
  Assert(FCurrentTimeData.Active, 'FCurrentTimeData dataset must be active');
  Assert(FOriginalTimeData.Active, 'FOriginalTimeData dataset must be active');

  HTMLContent := TStringList.Create;
  try
    if FCurrentTimeData.IsEmpty then
      Exit;

    HTMLContent.Add(HTMLHeader);
    HTMLContent.Add(TableTop);
    HTMLContent.Add(Format(DateHeaderRow1, [
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate),
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+1),
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+2)]));
    HTMLContent.Add(GetHourChangesAsTableRows(DayMonday, DayTuesday, DayWednesday, NoDayIdx));

    HTMLContent.Add(DividerRow);
    HTMLContent.Add(Format(DateHeaderRow2, [
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+3),
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+4),
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+5),
      FormatDateTime('dddd, mm/dd/yyyy', BeginDate+6)]));
    HTMLContent.Add(GetHourChangesAsTableRows(DayThursday, DayFriday, DaySaturday, DaySunday));

    Reasons := GetReasonsForChange;
    HTMLContent.Add(Format(TableBottom, [Reasons]));
    Result := HTMLContent.Text;
  finally
    FreeAndNil(HTMLContent);
  end;
end;

{ TTimesheetDay }

constructor TTimesheetDay.Create(WorkDate: TDateTime; TimesheetData: TDataSet);
begin
  Assert(Assigned(TimesheetData) and (TimesheetData.Active), 'TimesheetData must be assigned and active in TTimesheetDay.Create');
  SetupDay(WorkDate, TimesheetData);
end;

destructor TTimesheetDay.Destroy;
begin
  inherited;
end;

procedure TTimesheetDay.InitializeDay;
var
  I: Integer;
begin
  FEntryID := 0;
  FEntryBy := 0;
  FEntryDate := 0;
  FVacationHours := 0;
  FLeaveHours := 0;
  FPTOHours := 0;
  FBereavementHours := 0;
  FHolidayHours := 0;
  FJuryHours := 0;
  FRegularHours := 0;
  FOTHours := 0;
  FDTHours := 0;
  FCalloutHours := 0;
  for I := 0 to WorkSpanMax do begin
    FStartTimes[I] := 0.0;
    FStopTimes[I] := 0.0;
  end;
  for I := 0 to CalloutSpanMax do begin
    FCalloutStartTimes[I] := 0.0;
    FCalloutStopTimes[I] := 0.0;
  end;
  FHasPriorEntry := False;
end;

function TTimesheetDay.HasTimeInSlot(const TimeSlot: Integer): Boolean;
begin
  Assert(TimeSlot in [0..WorkSpanMax], 'TimeSlot out of range in IsTimeSlotEmpty');
  Result := (StartTimes[TimeSlot] > 0) or (StopTimes[TimeSlot] > 0);
end;

function TTimesheetDay.HasCalloutTimeInSlot(const TimeSlot: Integer): Boolean;
begin
  Assert(TimeSlot in [0..CalloutSpanMax], 'TimeSlot out of range in IsCalloutTimeSlotEmpty');
  Result := (CalloutStartTimes[TimeSlot] > 0) and (CalloutStopTimes[TimeSlot] > 0);
end;

function TTimesheetDay.IsTimeSlotEmpty(const TimeSlot: Integer): Boolean;
begin
  Result := not HasTimeInSlot(TimeSlot);
end;

function TTimesheetDay.IsCalloutTimeSlotEmpty(const TimeSlot: Integer): Boolean;
begin
  Result := not HasCalloutTimeInSlot(TimeSlot);
end;

procedure TTimesheetDay.SetupDay(const WorkDate: TDateTime; Data: TDataSet);
var
  I: Integer;
begin
  InitializeDay;
  FWorkDate := WorkDate;
  if not Data.Locate('work_date', FWorkDate, []) then
    Exit;

  FEntryID := Data.FieldByName('entry_id').AsInteger;
  FEntryBy := Data.FieldByName('entry_by').AsInteger;
  FEntryDate := Data.FieldByName('entry_date_local').AsDateTime;
  FVacationHours := Data.FieldByName('vac_hours').AsFloat;
  FLeaveHours := Data.FieldByName('leave_hours').AsFloat;
  FPTOHours := Data.FieldByName('pto_hours').AsFloat;
  FBereavementHours := Data.FieldByName('br_hours').AsFloat;
  FHolidayHours := Data.FieldByName('hol_hours').AsFloat;
  FJuryHours := Data.FieldByName('jury_hours').AsFloat;
  FRegularHours := Data.FieldByName('reg_hours').AsFloat;
  FOTHours := Data.FieldByName('ot_hours').AsFloat;
  FDTHours := Data.FieldByName('dt_hours').AsFloat;
  FCalloutHours := Data.FieldByName('callout_hours').AsFloat;
  for I := 0 to WorkSpanMax do begin
    FStartTimes[I] := Data.FieldByName(Format('work_start%d',[I+1])).AsDateTime;
    FStopTimes[I] := Data.FieldByName(Format('work_stop%d',[I+1])).AsDateTime;
  end;
  for I := 0 to CalloutSpanMax do begin
    FCalloutStartTimes[I] := Data.FieldByName(Format('callout_start%d',[I+1])).AsDateTime;
    FCalloutStopTimes[I] := Data.FieldByName(Format('callout_stop%d',[I+1])).AsDateTime;
  end;
  FReasonChanged := Data.FieldByName('reason_changed_desc').AsString;
  FApproveDate := Data.FieldByName('approve_date').AsDateTime;
  FFinalApproveDate := Data.FieldByName('final_approve_date').AsDateTime;
end;

function TTimesheetDay.FormatHours(const Hours: Double; const Encoded: Boolean): string;
begin
  if FloatEquals(Hours, 0) then begin
    if Encoded then
      Result := '&nbsp;'
    else
      Result := '';
  end else
    Result := FormatFloat('0.0', Hours);
end;

function TTimesheetDay.FormatTime(const Hours: TDateTime; const Encoded: Boolean = True): string;
begin
  if FloatEquals(Hours, 0) then begin
    if Encoded then
      Result := '&nbsp;'
    else
      Result := '';
  end else
    Result := FormatDateTime('hh:nn', Hours);
end;

function TTimesheetDay.StartTimeAsString(const TimeSlot: Integer): string;
begin
  Assert(TimeSlot in [0..WorkSpanMax], 'TimeSlot out of range');
  Result := FormatTime(FStartTimes[TimeSlot]);
end;

function TTimesheetDay.StopTimeAsString(const TimeSlot: Integer): string;
begin
  Assert(TimeSlot in [0..WorkSpanMax], 'TimeSlot out of range');
  Result := FormatTime(FStopTimes[TimeSlot]);
end;

function TTimesheetDay.CalloutStartTimeAsString(const TimeSlot: Integer): string;
begin
  Assert(TimeSlot in [0..CalloutSpanMax], 'TimeSlot out of range');
  Result := FormatTime(FCalloutStartTimes[TimeSlot]);
end;

function TTimesheetDay.CalloutStopTimeAsString(const TimeSlot: Integer): string;
begin
  Assert(TimeSlot in [0..CalloutSpanMax], 'TimeSlot out of range');
  Result := FormatTime(FCalloutStopTimes[TimeSlot]);
end;

function TTimesheetDay.JuryHoursAsString: string;
begin
  Result := FormatHours(JuryHours);
end;

function TTimesheetDay.LeaveHoursAsString: string;
begin
  Result := FormatHours(LeaveHours);
end;

function TTimesheetDay.PTOHoursAsString: string;
begin
  Result := FormatHours(PTOHours);
end;

function TTimesheetDay.BereavementHoursAsString: string;
begin
  Result := FormatHours(BereavementHours);
end;

function TTimesheetDay.VacationHoursAsString: string;
begin
  Result := FormatHours(VacationHours);
end;

function TTimesheetDay.HolidayHoursAsString: string;
begin
  Result := FormatHours(HolidayHours);
end;

function TTimesheetDay.GetCalloutStartTimes(Index: Integer): TDateTime;
begin
  Assert(Index in [0..CalloutSpanMax], 'Index out of range');
  Result := FCalloutStartTimes[Index];
end;

function TTimesheetDay.GetCalloutStopTimes(Index: Integer): TDateTime;
begin
  Assert(Index in [0..CalloutSpanMax], 'Index out of range');
  Result := FCalloutStopTimes[Index];
end;

function TTimesheetDay.GetStartTimes(Index: Integer): TDateTime;
begin
  Assert(Index in [0..WorkSpanMax], 'Index out of range');
  Result := FStartTimes[Index];
end;

function TTimesheetDay.GetStopTimes(Index: Integer): TDateTime;
begin
  Assert(Index in [0..WorkSpanMax], 'Index out of range');
  Result := FStopTimes[Index];
end;

function TTimesheetDay.GetTotalOtherHours: Double;
begin
  Result := VacationHours + LeaveHours + PTOHours + BereavementHours + HolidayHours + JuryHours;
end;

function TTimesheetDay.GetTotalWorkHours: Double;
begin
  Result := RegularHours + OTHours + DTHours + CalloutHours;
end;

function TTimesheetDay.GetTotalHours: Double;
begin
  Result := TotalWorkHours + TotalOtherHours;
end;

function TTimesheetDay.HasAnyWorkHours: Boolean;
begin
  Result := (StartTimes[0] > 0) or (CalloutStartTimes[0] > 0) or (TotalOtherHours > 0.0);
end;

function TTimesheetDay.HasAnyHoursOfType(const HourType: string): Boolean;
begin
  Result := ((HourType = 'Vac:') and (VacationHours > 0)) or
    ((HourType = 'Leave:') and (LeaveHours > 0)) or
    ((HourType = 'PTO:') and (PTOHours > 0)) or
    ((HourType = 'Hol:') and (HolidayHours > 0)) or
    ((HourType = 'Bereave:') and (BereavementHours > 0)) or
    ((HourType = 'Jury:') and (JuryHours > 0));
end;

function TTimesheetDay.IsApproved: Boolean;
begin
  Result := (ApproveDate > 0) or (FinalApproveDate > 0);
end;

end.

