inherited DamageDM: TDamageDM
  OldCreateOrder = True
  Height = 354
  Width = 368
  object DamageBill: TADODataSet
    CommandText = 'select * from damage_bill where 0=1'
    Parameters = <>
    Left = 48
    Top = 55
  end
  object NextUQDamageID: TADOStoredProc
    ProcedureName = 'NextUniqueID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Name'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 261
    Top = 6
  end
  object InsertDamage: TADOCommand
    CommandText = 
      'INSERT INTO damage (uq_damage_id, profit_center, damage_date, uq' +
      '_notified_date,'#13#10' notified_by_person, notified_by_company, notif' +
      'ied_by_phone, location, page, grid, '#13#10'city, county, state, utili' +
      'ty_co_damaged, remarks, investigator_id, excavator_company, '#13#10'lo' +
      'cate_marks_present, locate_requested, ticket_id, modified_by, '#13#10 +
      'facility_type, facility_size, facility_material, '#13#10'facility_type' +
      '_2, facility_size_2, facility_material_2, '#13#10'facility_type_3, fac' +
      'ility_size_3, facility_material_3, '#13#10'size_type, client_claim_id,' +
      ' damage_type, claim_status, locator_id, estimate_agreed,  '#13#10' rea' +
      'son_changed, work_priority_id, user_changed_pc, intelex_num)'#13#10'VA' +
      'LUES (:uq_damage_id, :profit_center, :damage_date, :uq_notified_' +
      'date, '#13#10':notified_by_person, :notified_by_company, :notified_by_' +
      'phone, :location, :page, :grid, '#13#10':city, :county, :state, :utili' +
      'ty_co_damaged, :remarks, :investigator_id, :excavator_company, '#13 +
      #10':locate_marks_present, :locate_requested, :ticket_id, :modified' +
      '_by, '#13#10':facility_type, :facility_size, :facility_material, '#13#10':fa' +
      'cility_type_2, :facility_size_2, :facility_material_2, '#13#10':facili' +
      'ty_type_3, :facility_size_3, :facility_material_3, '#13#10':size_type,' +
      ' :client_claim_id, :damage_type, :claim_status,'#13#10':locator_id, :e' +
      'stimate_agreed,  :reason_changed, :work_priority_id, :user_chang' +
      'ed_pc, :intelex_num)'
    ExecuteOptions = [eoExecuteNoRecords]
    Parameters = <
      item
        Name = 'uq_damage_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'profit_center'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'damage_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'uq_notified_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'notified_by_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_phone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'grid'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'utility_co_damaged'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'remarks'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'investigator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'excavator_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'locate_marks_present'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'locate_requested'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'facility_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_type_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_type_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'size_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'client_claim_id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'damage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'claim_status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'locator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'estimate_agreed'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'reason_changed'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_priority_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'user_changed_pc'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'intelex_num'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 152
    Top = 111
  end
  object DamageThirdParty: TADODataSet
    CommandText = 'select * from damage_third_party where 0=1'
    Parameters = <>
    Left = 48
    Top = 104
  end
  object DamageEstimate: TADODataSet
    CommandText = 
      'select *,  intelex_num as utilistar_id from damage_estimate wher' +
      'e 0=1'
    Parameters = <>
    Left = 48
    Top = 146
  end
  object DamageInvoice: TADODataSet
    CommandText = 'select * from damage_invoice where 0=1'
    Parameters = <>
    Left = 48
    Top = 197
  end
  object DamageLitigation: TADODataSet
    CommandText = 'select * from damage_litigation where 0=1'
    Parameters = <>
    Left = 48
    Top = 247
  end
  object UpdateDamageApproval: TADOCommand
    CommandText = 
      'update damage set'#13#10'  damage_type = :approved,'#13#10'  approved_by_id ' +
      '= :approved_by_id, '#13#10'  approved_datetime = :approved_datetime,'#13#10 +
      '  modified_by = :modified_by'#13#10'where'#13#10'  damage_id = :damage_id an' +
      'd'#13#10'  approved_by_id is null and'#13#10'  approved_datetime is null and' +
      #13#10'  coalesce(damage_type, '#39#39') in (:completed, :pending_approval)'
    Parameters = <
      item
        Name = 'approved'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'approved_by_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'approved_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'damage_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'completed'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'pending_approval'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 152
    Top = 163
  end
  object DocumentCommand: TADOCommand
    Parameters = <>
    Left = 48
    Top = 295
  end
  object DamageFacilityData: TADODataSet
    CommandText = 'select * from damage_facility where 0=1'
    Parameters = <>
    Left = 150
    Top = 228
  end
  object UpdateDamage3: TADOCommand
    CommandText = 
      'update damage set  intelex_num=:intelex_num, office_id=:office_i' +
      'd, profit_center=:profit_center, damage_inv_num=:damage_inv_num,' +
      ' damage_type=:damage_type, damage_date=:damage_date, due_date=:d' +
      'ue_date, notified_by_person=:notified_by_person, notified_by_com' +
      'pany=:notified_by_company, notified_by_phone=:notified_by_phone,' +
      'client_code=:client_code, client_id=:client_id, client_claim_id=' +
      ':client_claim_id, date_mailed=:date_mailed, date_faxed=:date_fax' +
      'ed, sent_to=:sent_to, size_type=:size_type, location=:location, ' +
      'page=:page, grid=:grid, city=:city, county=:county, state=:state' +
      ', utility_co_damaged=:utility_co_damaged, diagram_number=:diagra' +
      'm_number, remarks=:remarks, investigator_id=:investigator_id, in' +
      'vestigator_arrival=:investigator_arrival, investigator_departure' +
      '=:investigator_departure, investigator_est_damage_time=:investig' +
      'ator_est_damage_time, investigator_narrative=:investigator_narra' +
      'tive, excavator_company=:excavator_company, excavator_type=:exca' +
      'vator_type, excavation_type=:excavation_type, locate_marks_prese' +
      'nt=:locate_marks_present, locate_requested=:locate_requested, ti' +
      'cket_id=:ticket_id, site_mark_state=:site_mark_state,site_sewer_' +
      'marked=:site_sewer_marked, site_water_marked=:site_water_marked,' +
      ' site_catv_marked=:site_catv_marked, site_gas_marked=:site_gas_m' +
      'arked, site_power_marked=:site_power_marked, site_tel_marked=:si' +
      'te_tel_marked, site_other_marked=:site_other_marked, site_pictur' +
      'es=:site_pictures, marks_within_tolerance=:marks_within_toleranc' +
      'e, site_marks_measurement=:site_marks_measurement, site_buried_u' +
      'nder=:site_buried_under, site_clarity_of_marks=:site_clarity_of_' +
      'marks, site_hand_dig=:site_hand_dig, site_mechanized_equip=:site' +
      '_mechanized_equip, site_paint_present=:site_paint_present, site_' +
      'flags_present=:site_flags_present, site_exc_boring=:site_exc_bor' +
      'ing, site_exc_grading=:site_exc_grading, site_exc_open_trench=:s' +
      'ite_exc_open_trench, site_img_35mm=:site_img_35mm, site_img_digi' +
      'tal=:site_img_digital, site_img_video=:site_img_video, disc_repa' +
      'ir_techs_were=:disc_repair_techs_were, disc_repairs_were=:disc_r' +
      'epairs_were, disc_repair_person=:disc_repair_person, disc_repair' +
      '_contact=:disc_repair_contact, disc_repair_comment=:disc_repair_' +
      'comment, disc_exc_were=:disc_exc_were, disc_exc_person=:disc_exc' +
      '_person, disc_exc_contact=:disc_exc_contact, disc_exc_comment=:d' +
      'isc_exc_comment, disc_other1_person=:disc_other1_person, disc_ot' +
      'her1_contact=:disc_other1_contact, disc_other1_comment=:disc_oth' +
      'er1_comment, disc_other2_person=:disc_other2_person, disc_other2' +
      '_contact=:disc_other2_contact, disc_other2_comment=:disc_other2_' +
      'comment, exc_resp_code=:exc_resp_code, exc_resp_type=:exc_resp_t' +
      'ype, exc_resp_other_desc=:exc_resp_other_desc, exc_resp_details=' +
      ':exc_resp_details, exc_resp_response=:exc_resp_response, uq_resp' +
      '_code=:uq_resp_code, uq_resp_type=:uq_resp_type, uq_resp_other_d' +
      'esc=:uq_resp_other_desc, uq_resp_details=:uq_resp_details, spc_r' +
      'esp_code=:spc_resp_code, spc_resp_type=:spc_resp_type, spc_resp_' +
      'other_desc=:spc_resp_other_desc, spc_resp_details=:spc_resp_deta' +
      'ils, closed_date=:closed_date, active=:active, modified_by=:modi' +
      'fied_by, uq_resp_ess_step=:uq_resp_ess_step, locator_experience=' +
      ':locator_experience, locate_equipment=:locate_equipment, was_pro' +
      'ject=:was_project, claim_status=:claim_status, claim_status_date' +
      '=:claim_status_date, invoice_code=:invoice_code, estimate_locked' +
      '=:estimate_locked, site_marked_in_white=:site_marked_in_white, s' +
      'ite_tracer_wire_intact=:site_tracer_wire_intact, site_pot_holed=' +
      ':site_pot_holed,site_nearest_fac_measure=:site_nearest_fac_measu' +
      're, excavator_doing_repairs=:excavator_doing_repairs, offset_vis' +
      'ible=:offset_visible, offset_qty=:offset_qty,claim_coordinator_i' +
      'd=:claim_coordinator_id,locator_id=:locator_id, estimate_agreed ' +
      '= :estimate_agreed, reason_changed = :reason_changed, work_prior' +
      'ity_id = :work_priority_id, user_changed_pc = :user_changed_pc'#13#10 +
      'where'#13#10'  damage_id = :damage_id and'#13#10'  (coalesce(damage_type, '#39#39 +
      ') = coalesce(:damage_type, '#39#39') or coalesce(damage_type, '#39#39') <> '#39 +
      'APPROVED'#39' and coalesce(:damage_type, '#39#39') <> '#39'APPROVED'#39')'
    Parameters = <
      item
        Name = 'intelex_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'office_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'profit_center'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'damage_inv_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'damage_type'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'damage_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'notified_by_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'notified_by_phone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_claim_id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'date_mailed'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_faxed'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sent_to'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'size_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'grid'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'utility_co_damaged'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'diagram_number'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'remarks'
        Attributes = [paNullable, paLong]
        DataType = ftMemo
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'investigator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'investigator_arrival'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'investigator_departure'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'investigator_est_damage_time'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'investigator_narrative'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'excavator_company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'excavator_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'excavation_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'locate_marks_present'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'locate_requested'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'site_mark_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'site_sewer_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_water_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_catv_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_gas_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_power_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_tel_marked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_other_marked'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'site_pictures'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'marks_within_tolerance'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'site_marks_measurement'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'site_buried_under'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'site_clarity_of_marks'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'site_hand_dig'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_mechanized_equip'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_paint_present'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_flags_present'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_boring'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_grading'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_exc_open_trench'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_img_35mm'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_img_digital'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_img_video'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'disc_repair_techs_were'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repairs_were'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_repair_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_exc_were'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_exc_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_other1_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other1_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other1_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'disc_other2_person'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other2_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'disc_other2_comment'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'exc_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'exc_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'exc_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'exc_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'exc_resp_response'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'uq_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'uq_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'spc_resp_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'spc_resp_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'spc_resp_other_desc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'spc_resp_details'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'closed_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'active'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'uq_resp_ess_step'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'locator_experience'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'locate_equipment'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'was_project'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'claim_status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'claim_status_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'invoice_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'estimate_locked'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'site_marked_in_white'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'site_tracer_wire_intact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'site_pot_holed'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'site_nearest_fac_measure'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'excavator_doing_repairs'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'offset_visible'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'offset_qty'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'claim_coordinator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'locator_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'estimate_agreed'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'reason_changed'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_priority_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'user_changed_pc'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'damage_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'damage_type'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'damage_type'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 152
    Top = 55
  end
  object updateFacilities: TADOCommand
    CommandText = 
      'update damage set   '#13#10'facility_type=:facility_type, '#13#10'facility_s' +
      'ize=:facility_size, '#13#10'facility_material=:facility_material,  '#13#10'f' +
      'acility_type_2=:facility_type_2,'#13#10'facility_size_2=:facility_size' +
      '_2,'#13#10'facility_material_2=:facility_material_2, '#13#10'facility_type_3' +
      '=:facility_type_3,'#13#10'facility_size_3=:facility_size_3,'#13#10'facility_' +
      'material_3=:facility_material_3'#13#10'where'#13#10'  damage_id = :damage_id'
    Parameters = <
      item
        Name = 'facility_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_type_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_type_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_size_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'facility_material_3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Damage_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 256
    Top = 55
  end
  object Damage: TADODataSet
    CursorType = ctStatic
    CommandText = 'select *, intelex_num as utilistar_id from damage where 0=1'
    EnableBCD = False
    Parameters = <>
    Left = 49
    Top = 6
  end
end
