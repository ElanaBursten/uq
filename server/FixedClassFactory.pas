unit FixedClassFactory;

{
This is a copy of some RO code, modified to not burn up 100% CPU in a busy-wait.

It is intended for use only with the very old 1.x version of RO, not with any
newer RO.
}

interface

uses {$IFDEF REMOBJECTS_TRIAL}uROTrial,{$ENDIF}
     uROServer, 
     {$IFDEF DOTNET}
     Borland.Delphi.Classes,
     Borland.Delphi.SysUtils,
     RemObjects.SDK.VclReplacements;
     {$ELSE}
     Classes, SysUtils, SyncObjs;
     {$ENDIF}

type

     TLessBrokenPooledClassFactory = class(TROClassFactory)
     private
       fInstances: array of IInterface;
       fPoolSize,
       fLastInc: Integer;

     protected
       procedure CreateInstance(out anInstance: IInterface); override;
       procedure ReleaseInstance(var anInstance: IInterface); override;

     public
       constructor Create(const anInterfaceName: string;
                          aCreatorFunc: TRORemotableCreatorFunc;
                          anInvokerClass: TROInvokerClass;
                          aPoolSize: Integer);
       destructor Destroy; override;
     end;

implementation

{ TLessBrokenPooledClassFactory }

constructor TLessBrokenPooledClassFactory.Create(const anInterfaceName: string;
  aCreatorFunc: TRORemotableCreatorFunc; anInvokerClass: TROInvokerClass;
  aPoolSize: Integer);
var i: Integer;
begin
  inherited Create(anInterfaceName, aCreatorFunc, anInvokerClass);

  SetLength(fInstances, aPoolSize);
  fPoolSize := aPoolSize;
  fLastInc := -1;

  for i := 0 to (aPoolSize-1) do
    inherited CreateInstance(fInstances[i]);
end;

destructor TLessBrokenPooledClassFactory.Destroy;
var i: Integer;
begin
  for i := 0 to High(fInstances) do
    fInstances[i] := nil;

  inherited;
end;

procedure TLessBrokenPooledClassFactory.CreateInstance(out anInstance: IInterface);
var i, refcnt: Integer;
begin
  anInstance := nil;
  i := fLastInc;

  while (anInstance = nil) do try

    // KJC: Here is the new logic.  Each time it looks through all the
    // instances and they are all full, it figures we must be pretty busy,
    // so let the CPU go get some work done for a while.

    if (i>=fPoolSize-1) then begin
      i := 0;
      Sleep(500);
    end else Inc(i);

    {$IFDEF DOTNET}
    raise Exception.Create('TLessBrokenPooledClassFactory is not properly implemented for .NET, yet.');
    {$ELSE}
    refcnt := fInstances[i]._AddRef;
    if (refcnt=2) then begin
      anInstance := fInstances[i];
      fLastInc := i;
    end;

    {$ENDIF}
  finally
    {$IFDEF DOTNET}
    {$ELSE}
    fInstances[i]._Release;
    {$ENDIF}
  end;
end;

procedure TLessBrokenPooledClassFactory.ReleaseInstance(var anInstance: IInterface);
begin
  // Does nothing. We want to keep instances alive
end;

end.
