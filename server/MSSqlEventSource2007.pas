unit MSSqlEventSource2007;
{
  This unit contains interim code so QMLogic (D2007) can save events.
  It will eventually be replaced by service2\MSSqlEventSource.
}

interface

uses
  SysUtils, Contnrs, Variants, DB, ADODB, EventSource2007, QMEventsNoThrift;

type
  TMSSQLEventStore = class(TBaseEventStore, IEventStore)
  private
    Conn: TADOConnection;
  protected
    function GetMaxVersion(Key: IAggrKey): Integer;
    function LoadEventDescriptorsForAggregate(Key: IAggrKey): TObjectList; override;
    procedure PersistEventDescriptors(Key: IAggrKey; NewEventDescriptors: TObjectList; ExpectedVersion: Integer); override;
  public
    constructor Create(APublisher: IEventPublisher; AConnection: TADOConnection); reintroduce;
  end;

implementation

uses OdDbUtils;

{ TMSSQLEventStore }

constructor TMSSQLEventStore.Create(APublisher: IEventPublisher;
  AConnection: TADOConnection);
begin
  inherited Create(APublisher);
  Conn := AConnection;
end;

function TMSSQLEventStore.GetMaxVersion(Key: IAggrKey): Integer;
const
  SQL = 'SELECT MAX(version_num) FROM event_log '+
        'WHERE aggregate_type=%d and aggregate_id=%d';
var
  MaxVer: Variant;
  Query: TADOQuery;
begin
  Result := 0;
  Query := TADOQuery.Create(nil);
  try
    Query.Connection := Conn;
    Query.SQL.Text := Format(SQL, [Integer(Key.AggrType), Key.AggrID]);
    Query.Open;
    if Query.HasRecords then
      MaxVer := Query.Fields[0].AsInteger
    else
      MaxVer := Null;
    if MaxVer <> Null then
      Result := MaxVer;
  finally
    FreeAndNil(Query);
  end;
end;

function TMSSQLEventStore.LoadEventDescriptorsForAggregate(Key: IAggrKey): TObjectList;
begin
  raise Exception.Create('Not implmented');
end;

procedure TMSSQLEventStore.PersistEventDescriptors(Key: IAggrKey;
  NewEventDescriptors: TObjectList; ExpectedVersion: Integer);
const
  SQL = 'INSERT INTO event_log (aggregate_type, aggregate_id, version_num, event_data) ' +
        'VALUES (:aggregate_type, :aggregate_id, :version_num, :event_data)';
var
  Event: TEventDescriptor;
  Data: TADOCommand;
  PublishSP: TADOStoredProc;
  Version: Integer;
  I: Integer;
begin
  Assert(Conn.InTransaction, 'PersistEventDescriptors can only be executed in an active transaction.');
  Assert(Assigned(NewEventDescriptors), 'NewEventDescriptors must be assigned.');

  Version := GetMaxVersion(Key);
  if (ExpectedVersion > -1) and (ExpectedVersion <> Version) then
    raise EConcurrencyException.CreateFmt('Event store concurrency ' +
      'error for Aggregate: %s. Expected Ver: %d; Actual Ver: %d.',
      [Key.ToString, ExpectedVersion, Version]);

  Data := TADOCommand.Create(nil);
  try
    PublishSP := TADOStoredProc.Create(nil);
    Data.Connection := Conn;
    Data.CommandText := SQL;
    PublishSP.Connection := Conn;
    PublishSP.ProcedureName := 'add_new_event_notice';
    PublishSP.Parameters.Refresh;
    for I := 0 to NewEventDescriptors.Count - 1 do begin
      Event := NewEventDescriptors.Items[I] as TEventDescriptor;

      Inc(Version);
      Data.Parameters.ParamByName('aggregate_type').Value := Integer(Key.AggrType);
      Data.Parameters.ParamByName('aggregate_id').Value := Key.AggrID;
      Data.Parameters.ParamByName('version_num').Value := Version;
      Data.Parameters.ParamByName('event_data').Value := Event.EventData.SerializeToJSON;
      try
        Data.Execute;
        PublishSP.Parameters.ParamByName('@AggrType').Value := Data.Parameters.ParamByName('aggregate_type').Value;
        PublishSP.Parameters.ParamByName('@AggrID').Value := Data.Parameters.ParamByName('aggregate_id').Value;
        PublishSP.Parameters.ParamByName('@Version').Value := Data.Parameters.ParamByName('version_num').Value;
        PublishSP.ExecProc;
      except
        on E: Exception do begin
        { If we get a duplicate key error here, that means there was
          a concurrency error (an existing row for the same Aggregate & Version).
          Raise EConcurrencyException instead. Handle the exception by trying to
          save the events again.
        }
          if Pos('duplicate key', E.Message) > 0 then
            raise EConcurrencyException.CreateFmt('Event store concurrency ' +
              'error. Aggregate %s, Ver: %d. %s',
              [Key.ToString, Version, E.Message])
          else
            raise;
        end;
      end;
    end;
  finally
    FreeAndNil(Data);
    FreeAndNil(PublishSP);
  end;
end;

end.
