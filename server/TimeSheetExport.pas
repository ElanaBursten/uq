unit TimeSheetExport;

interface

uses
  SysUtils, Classes, LogicDMu, ADODB, DB;

type
  TPayrollExporter = class
  private
    ProfitCenter: string;
    WeekEnding: TDateTime;
    DM: TLogicDM;
    ExportSP: TADOStoredProc;
    BeginDate: TDateTime;
    ResultString: string;
    procedure CheckExportCanProceed;
    procedure DoFileExport;
    procedure RecordExportInDB; virtual;
    procedure SetupExportStoredProcedure; virtual;
    procedure BackupExistingFile(const FileName: string);
    function GetExportFolder: string; virtual;
    procedure SetExportStoredProcedureName; virtual; abstract;
    function GetFileNamePrefix: string; virtual;
    procedure ProcessResults; virtual; abstract;
    function BackupPrefix: string; virtual; abstract;
  public
    function ExportData(AProfitCenter: string;
      AWeekEnding: TDateTime; ADM: TLogicDM;
      AExportSP: TADOStoredProc): string;
  end;

  TDynamicsTimeSheetExporter = class (TPayrollExporter)
  private
      WhichPayroll: string;  //QMANTWO-650
    procedure SetExportStoredProcedureName; override;
    procedure ProcessResults; override;
    procedure RecordExportInDB; override;
    function BackupPrefix: string; override;
  end;

  TDynamicsEmployeeExporter = class(TPayrollExporter)
  private

    procedure ProcessResults; override;
    function BackupPrefix: string; override;
    procedure SetExportStoredProcedureName; override;
    function GetFileNamePrefix: string; override;
  end;


implementation

uses
  OdMiscUtils, QMConst, OdIsoDates, Variants, OdDbUtils, Windows, dialogs;

{ TPayrollExporter }

// TODO: Make this happen only once per set of exports, not for each file
procedure TPayrollExporter.CheckExportCanProceed;
const
  PermissionError =
    'You do not have permission to export timesheets for profit center %s. (EmpID = %d, Approvable = %d)';
  CheckSQL =
    'select t.approve_by, t.final_approve_by, e.emp_id, e.short_name, ds.d as work_date, t.source, ' +
    '  dbo.get_employee_pc(e.emp_id, 1) as payroll_pc ' +
    'from employee e ' +
    '  inner join dbo.DateSpan(%s, %s) ds on 1=1 ' +
    '  left join timesheet_entry t on t.work_emp_id = e.emp_id and t.work_date = ds.d ' +
    'where e.active=1 and (dbo.get_employee_pc(e.emp_id, 1) = ''%s'') ' +
    '  and ((approve_by is null) or (final_approve_by is null))' +
    '  and (t.status is null or t.status = ''ACTIVE'' or t.status = ''SUBMIT'') ' +

    '  and (t.entry_by <> 20)   ' +
	  '  and (upper(t.source) <> upper(''%s'')) ' +  //QM-613 EB Timesheet UTA   //QM-760 Timesheet RTASQ EB   //QM-781 SR added )
    '  and (dbo.emp_has_right(e.emp_id, 18) = 1)';
  ApproveError = 'All of the timesheets for a profit center must be approved and final approved before exporting.'
    + sLineBreak +
    'You have %d %s pending %s for profit center %s, including: ' + sLineBreak +
    '%s for %s';
var
  ProfitCenterRootEmpID: Integer;
  ExportAsManagerID: Integer;
  LimitationString: string;
  NeedApproval: TDataSet;
  NeedName: string;
  NeedWorkDate: string;
  MissingApprovalType: string;
begin
  if Trim(ProfitCenter) = '' then
    raise Exception.Create('No profit center specified');

  ProfitCenterRootEmpID := -1;
  ExportAsManagerID := DM.LoggedInEmpID;
  if DM.CanI(RightTimesheetsFinalApprove, LimitationString, ExportAsManagerID) then
    ExportAsManagerID := StrToIntDef(LimitationString, ExportAsManagerID);

  DM.EmpHier.CommandText := Format('exec dbo.get_repr_pc_under %d', [ExportAsManagerID]); //qm-126  sr
//  showmessage('CheckExportCanProceed: '+dm.EmpHier.CommandText); //qm-126  sr
  DM.EmpHier.Open;
  try
    if not DM.EmpHier.Locate('h_repr_pc_code', ProfitCenter, [loCaseInsensitive]) then
      raise Exception.CreateFmt(PermissionError, [ProfitCenter, ExportAsManagerID, DM.EmpHier.RecordCount]);
    ProfitCenterRootEmpID := DM.EmpHier.FieldByName('h_emp_id').AsInteger;
  finally
    DM.EmpHier.Close;
  end;

  Assert(ProfitCenterRootEmpID > 0, 'Unable to find a managing employee for profit center ' + ProfitCenter);

  if WeekEnding > Now + 3 then
    raise Exception.Create('You can not export timesheets dated in the future: '
      + DateTimeToStr(WeekEnding));

  NeedApproval := DM.GetDataSetForSQL(Format(CheckSQL,
    [IsoDateToStrQuoted(BeginDate), IsoDateToStrQuoted(WeekEnding), ProfitCenter, TimesheetRtasqSource ]));   //QM-760 Timesheet RTASQ EB
//  NeedApproval := DM.GetDataSetForSQL(Format(CheckSQL,
//    [IsoDateToStrQuoted(BeginDate), IsoDateToStrQuoted(WeekEnding), ProfitCenter ]));   //QM-760 Timesheet RTASQ EB


  if not NeedApproval.Eof then begin
    MissingApprovalType := 'final approval';
    if NeedApproval.Locate('approve_by', Null, []) then
      MissingApprovalType := 'approval'
    else if not NeedApproval.Locate('final_approve_by', Null, []) then
      raise Exception.Create('Internal Error: No timesheets needing final approval found');
    NeedName := NeedApproval.FieldByName('short_name').AsString;
    NeedWorkDate := NeedApproval.FieldByName('work_date').AsString;
    raise Exception.CreateFmt(ApproveError, [NeedApproval.RecordCount,
      AddSIfNot1(NeedApproval.RecordCount, 'timesheet'),
        MissingApprovalType, ProfitCenter, NeedName, NeedWorkDate]);
  end;
end;

function TPayrollExporter.GetExportFolder: string;
const
  LocateCoSQL =
    'select l.name as name from locating_company l' +
    '  inner join profit_center p on l.company_id = p.company_id ' +
    'where p.pc_code = ''%s''';

var
  LocateCoData: TDataSet;
  LocateName: string;
begin
  Result := DM.GetServerSetting('Timesheets', 'ExportDir', '');
  if Result = '' then
    raise Exception.Create('[Timesheets] ExportDir=xxx is not defined in ' + LogicDmIniName);
  LocateCoData := DM.GetDataSetForSQL(Format(LocateCoSQL, [ProfitCenter]));
  try
    if not LocateCoData.Eof then
      LocateName := LocateCoData.FieldByName('name').AsString
    else
      raise Exception.Create('Locating company not found for profit center: ' + ProfitCenter);
  finally
    LocateCoData.Close;
  end;

  Result := AddSlash(AddSlash(Result) + AddSlash(LocateName)+ FormatDateTime('yyyy-mm-dd', WeekEnding));
  OdForceDirectories(Result);
end;

procedure TPayrollExporter.DoFileExport;
begin
  SetupExportStoredProcedure;
  try
    ProcessResults;
  finally
    ExportSP.Close;
  end;
end;

function TPayrollExporter.ExportData(AProfitCenter: string;
  AWeekEnding: TDateTime; ADM: TLogicDM; AExportSP: TADOStoredProc): string;
begin
  Assert(Assigned(ADM), 'LogicDM is unassigned');
  Assert(Assigned(AExportSP), 'ExportSP is unassigned');

  ProfitCenter := AProfitCenter;
  WeekEnding := AWeekEnding;
  DM := ADM;
  ExportSP := AExportSP;

  BeginDate := BeginningOfTheWeek(WeekEnding);
  CheckExportCanProceed;
  DoFileExport;
  RecordExportInDB;
  Result := ResultString;
end;

procedure TDynamicsTimeSheetExporter.RecordExportInDB;
const
  SQL = 'select * from timesheet_export where 0=1';
var
  RecordExport: TDataset;
begin
  RecordExport := DM.CreateDatasetForSQL(SQL);
  try
    // TODO: Maybe useful to record which export process completed (ADP, Dynamics, etc.).
    // It is not currently an issue because only the Dynamics exporter class does this.
    RecordExport.Insert;
    RecordExport.FieldByName('profit_center').AsString := ProfitCenter;
    RecordExport.FieldByName('week_ending').AsDateTime := Trunc(WeekEnding);
    RecordExport.FieldByName('export_by').AsInteger := DM.LoggedInEmpID;
    RecordExport.FieldByName('export_date').AsDateTime := Now;
    RecordExport.Post;
  finally
    FreeAndNil(RecordExport);
  end;
end;

procedure TPayrollExporter.SetupExportStoredProcedure;
var
  P: TParameters;
begin
  SetExportStoredProcedureName;

  Assert(ExportSP.ProcedureName <> '', 'no stored procedure name specified for timesheet export');

  P := ExportSP.Parameters;
  P.Refresh;
  // Timesheet export SPs use these:
  if Assigned(P.FindParam('@BEGIN_DATE')) then
    P.ParamValues['@BEGIN_DATE'] := BeginDate;
  // Passing a 23:59:59 date to the SP makes things harder to think about there
  if Assigned(P.FindParam('@END_DATE')) then
    P.ParamValues['@END_DATE'] := WeekEnding; // These SPs strip off the time, and then treat the end date as inclusive
  if Assigned(P.FindParam('@REGION')) then
    P.ParamValues['@REGION'] := ProfitCenter;
  // Employee, Project, Task exports use these:
  if Assigned(P.FindParam('@StartDate')) then
    P.ParamValues['@StartDate'] := BeginDate;
  if Assigned(P.FindParam('@EndDate')) then
    P.ParamValues['@EndDate'] := Trunc(WeekEnding) + 1; // This expects a more normal (non-inclusive) end date
  if Assigned(P.FindParam('@ProfitCenter')) then
    P.ParamValues['@ProfitCenter'] := ProfitCenter;
  if Assigned(P.FindParam('@PayrollPC')) then
    P.ParamValues['@PayrollPC'] := ProfitCenter;
  if Assigned(P.FindParam('@CompanyName')) then
    P.ParamValues['@CompanyName'] := '*';
  ExportSP.Open;
end;

procedure TPayrollExporter.BackupExistingFile(const FileName: string);
var
  BackupName: string;
begin
  if FileExists(FileName) then begin
    BackupName := ExtractFilePath(FileName) + BackupPrefix +
      ChangeFileExt(ExtractFileName(FileName), '') + '-' +
      FormatDateTime('yyyy-mm-dd-hh-mm-ss', Now) + ExtractFileExt(FileName);

    if not RenameFile(FileName, BackupName) then
      raise Exception.Create('Error renaming old export file: ' + FileName + ' to ' + BackupName);
  end;
end;

function TPayrollExporter.GetFileNamePrefix: string;
begin
  Result := 'EPI';
end;

{ TDynamicsTimeSheetExporter }

procedure TDynamicsTimeSheetExporter.SetExportStoredProcedureName;
begin
  WhichPayroll := dm.GetConfigurationDataValue('TimesheetExport', 'Ulti');//QMANTWO-650
  if WhichPayroll = 'ADP'  then ExportSP.ProcedureName := 'payroll_export_adp4'  //QMANTWO-650
  else
  ExportSP.ProcedureName := 'payroll_export_Ulti'; //QMANTWO-650
end;

procedure TDynamicsTimeSheetExporter.ProcessResults;
const
  Extension = '.csv';
var
  CompanyLen: Integer;
  FullName: string;
  Company: string;
  ProfitCenterSuffix: string;
  Skips: TStringList;
  Details: TADODataSet;
  RecAffected: Integer;
  Summary: TDataSet;
begin
  inherited;
  Details := TADODataSet.Create(nil);
  Skips := TStringList.Create;
  try
    Details.RecordSet := ExportSP.NextRecordSet(RecAffected);
    Summary := ExportSP;

    Skips.Add('adp_code');
    if WhichPayroll <> 'ADP' then //QMANTWO-650
    begin
      Skips.Add('co code');   //QMANTWO-650
      Skips.Add('Line');   //QMANTWO-650
    end;
    while not Summary.EOF do begin
      Company := Summary.FieldByName('co_code').AsString;
      if WhichPayroll = 'ADP' then  //QMANTWO-650
      begin
        Details.Filter := '[co code]=' + QuotedStr(Company);
        Details.Filtered := True; 
      end;

      CompanyLen := Length(Company);
      if CompanyLen < 3 then
        Company := Company + StringOfChar('_', 3 - CompanyLen)
      else if CompanyLen > 3 then
        Company := Copy(Company, 1, 3);
      if WhichPayroll <> 'ADP' then Details.Next;    //QMANTWO-650
      ProfitCenterSuffix := Details.FieldByName('adp_code').AsString;
      Details.First;     //QMANTWO-650
      FullName := GetExportFolder + GetFileNamePrefix + Company + ProfitCenterSuffix + Extension;
      BackupExistingFile(FullName);
      if WhichPayroll = 'ADP' then   //QMANTWO-650
      SaveDelimToFile(Details, FullName, ',', True, nil, Skips)   //QMANTWO-650
      else
      SaveDelimToFile(Details, FullName, ',', False, nil, Skips);  //QMANTWO-650
      ResultString := ResultString +
        Format('%s: Exported %d %s for profit center %s for the week ending %s'#13#10,
        [Company, Details.RecordCount, AddSIfNot1(Details.RecordCount, 'timesheet row'),
        ProfitCenter, DateToStr(WeekEnding)]);
      ResultString := ResultString +' '+WhichPayroll;  //QMANTWO-650

      Summary.Next;
    end;
  finally
    FreeAndNil(Skips);
    FreeAndNil(Details);
  end;
end;

function TDynamicsTimeSheetExporter.BackupPrefix: string;
begin
  Result := 'Old-';
end;

{ TDynamicsEmployeeExporter }

procedure TDynamicsEmployeeExporter.SetExportStoredProcedureName;
begin
  ExportSP.ProcedureName := 'export_employees';
end;

function TDynamicsEmployeeExporter.GetFileNamePrefix: string;
begin
  Result := 'Employees';
end;

function TDynamicsEmployeeExporter.BackupPrefix: string;
begin
  Result := '';
end;

procedure TDynamicsEmployeeExporter.ProcessResults;
const
  Extension = '.txt';
var
  FullName: string;
  Data: TDataSet;
begin
  Data := ExportSP;

  if Data.IsEmpty then
    Exit;

  FullName := GetExportFolder + ProfitCenter + '-' + GetFileNamePrefix + Extension;
  BackupExistingFile(FullName);
  SaveDelimToFile(Data, FullName, '|', False, nil, nil);
  ResultString := ResultString +
    Format('Exported %d %s for profit center %s for the week ending %s'#13#10,
    [Data.RecordCount, GetFileNamePrefix,
    ProfitCenter, DateToStr(WeekEnding)]);
end;

procedure TPayrollExporter.RecordExportInDB;
begin
//Only used by Dynamic Exporter
end;

end.

