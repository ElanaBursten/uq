object WebModule1: TWebModule1
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  OnDestroy = WebModuleDestroy
  Actions = <
    item
      Name = 'XmlAction'
      PathInfo = '/XML'
    end
    item
      MethodType = mtGet
      Name = 'MonitorAction'
      PathInfo = '/monitor'
      OnAction = WebModule1MonitorActionAction
    end
    item
      MethodType = mtGet
      Name = 'CheckStartupAction'
      PathInfo = '/check'
      OnAction = WebModule1CheckStartupActionAction
    end
    item
      Name = 'CpuImage'
      PathInfo = '/cpu.bmp'
      OnAction = WebModule1CpuImageAction
    end
    item
      Name = 'FaxStatusAction'
      PathInfo = '/faxstatus'
      OnAction = WebModule1FaxStatusActionAction
    end>
  Height = 308
  Width = 462
  object SoapMsg: TROSOAPMessage
    SerializationOptions = [xsoWriteMultiRefArray, xsoWriteMultiRefObject]
    Left = 104
    Top = 16
  end
  object ROServer: TROWebBrokerServer
    Encryption.EncryptionMethod = tetRijndael
    Encryption.EncryptionSendKey = 'B2E3056082D3D01A158BE07937FC9452'
    Encryption.EncryptionRecvKey = '7A2B4FFB71F59204A247382D98BE0C08'
    Encryption.UseCompression = True
    Active = True
    Dispatchers = <
      item
        Name = 'SoapMsg'
        Message = SoapMsg
        Enabled = True
        PathInfo = 'SOAP'
      end
      item
        Name = 'BinMsg'
        Message = BinMsg
        Enabled = True
        PathInfo = 'BIN'
      end>
    Left = 48
    Top = 16
  end
  object BinMsg: TROBINMessage
    OnCompress = BinMsgCompress
    OnDecompress = BinMsgDecompress
    Left = 168
    Top = 16
  end
end
