object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = '                      ---Encryption Test Tool---'
  ClientHeight = 200
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblMsg: TLabel
    Left = 0
    Top = 0
    Width = 438
    Height = 25
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 64
    Top = 80
    Width = 61
    Height = 13
    Caption = 'From INI File'
  end
  object Label2: TLabel
    Left = 199
    Top = 80
    Width = 49
    Height = 13
    Caption = 'Encrypted'
  end
  object Label3: TLabel
    Left = 328
    Top = 80
    Width = 50
    Height = 13
    Caption = 'Decrypted'
  end
  object edtClearText: TEdit
    Left = 40
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object edtEncrypted: TEdit
    Left = 167
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object edtToggle: TEdit
    Left = 296
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object edtEncrypt: TButton
    Left = 64
    Top = 123
    Width = 75
    Height = 25
    Caption = 'Encrypt'
    TabOrder = 3
    OnClick = edtEncryptClick
  end
  object btnToggle: TButton
    Left = 318
    Top = 123
    Width = 75
    Height = 25
    Caption = 'Decrypt'
    TabOrder = 4
    OnClick = btnToggleClick
  end
  object btnReadINI: TButton
    Left = 184
    Top = 123
    Width = 75
    Height = 25
    Caption = 'Read INI'
    TabOrder = 5
    OnClick = btnReadINIClick
  end
end
