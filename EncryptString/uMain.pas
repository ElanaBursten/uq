unit uMain;
{Though called encryption. this is not that.  It is simply a scrambler that is hard to repeat.
This code will only work on UNICODE UCS-2 LE.  That is the same as UTF-16.  To ensure the INI
is of the right format, open in Windows Notepad and do a save as  and set the encoding to Unicode.

This version of the INI will not work with Delphi 2007 since that Delphi is pre-Unicode. D2007
requires that the INI be ANSI.

This procedure expects the password start with 'DEC_'.  If I don't see that, I will treat the
password as clear text.
}
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmMain = class(TForm)
    edtClearText: TEdit;
    edtEncrypted: TEdit;
    edtToggle: TEdit;
    edtEncrypt: TButton;
    btnToggle: TButton;
    btnReadINI: TButton;
    lblMsg: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    function EnDeCrypt(const Value: String): String;
    procedure edtEncryptClick(Sender: TObject);
    procedure btnToggleClick(Sender: TObject);
    procedure btnReadINIClick(Sender: TObject);
  private
    function readINI: boolean;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses System.IniFiles, System.StrUtils;
{$R *.dfm}
procedure TfrmMain.edtEncryptClick(Sender: TObject);
begin
  edtEncrypted.Text := EnDeCrypt(edtClearText.Text);
end;

procedure TfrmMain.btnReadINIClick(Sender: TObject);
begin
  readINI;
end;

procedure TfrmMain.btnToggleClick(Sender: TObject);
begin
  edtToggle.Text := EnDeCrypt(edtEncrypted.Text);
end;

function TfrmMain.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function TfrmMain.readINI: boolean;
var
  myPath:string;
  apassword: string;
  MyIni: TIniFile;
const
  DECRYPT = 'DEC_';
begin
  Result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try

    MyIni := TIniFile.Create(myPath + 'MyINI.ini');
    apassword := MyIni.ReadString('Database', 'Password', '');
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      edtClearText.text := copy(apassword, 5, maxint);
      edtEncryptClick(nil);
      lblMsg.Caption := 'That Password was encrypted';
    end
    else
    begin
      edtClearText.text := apassword;
      lblMsg.Caption := 'That Password was clear text';
    end;

    Result := True;
  finally
    freeandnil(MyIni);
  end;
end;

end.
