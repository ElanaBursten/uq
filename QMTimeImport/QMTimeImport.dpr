program QMTimeImport;

{$APPTYPE CONSOLE}

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  ActiveX,
  TimeImportData in 'TimeImportData.pas' {TimeImportDM: TDataModule},
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  HoursCalc in '..\common\HoursCalc.pas',
  HoursDataset in '..\common\HoursDataset.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  OdLog in '..\common\OdLog.pas' {OdExceptionDialog},
  QMConst in '..\client\QMConst.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdDBISAMUtils in '..\common\OdDBISAMUtils.pas',
  UQDbConfig in '..\common\UQDbConfig.pas',
  CodeLookupList in '..\common\CodeLookupList.pas';

var
  DM: TTimeImportDM;
begin
  if (ParamCount = 1) and (ParamStr(1) = '/v') then begin
    WriteLn('Version: ' + AppVersionShort);
    Exit;
  end;
  if ParamCount < 1 then begin
    WriteLn('Usage: FileToImport.xml [AlternateConfig.ini]');
    WriteLn('  If the AlternateConfig.ini parameter is used, its');
    WriteLn('  settings are applied instead of QMServer.ini.');
    Exit;
  end;

  try
    CoInitialize(nil);
    DM := TTimeImportDM.Create(nil);
    try
      DM.Execute(ParamStr(1));
    finally
      FreeAndNil(DM);
      CoUninitialize;
    end;
  except
    on E: Exception do begin
      Writeln('ERROR:' + E.Message);
    end;
  end;

end.
