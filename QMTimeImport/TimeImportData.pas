unit TimeImportData;

interface

uses
  SysUtils, Classes, ADODB, DB, HoursCalc, HoursDataset, Contnrs, MSXML2_TLB,
  OdMiscUtils, OdLog, DBISAMTb;

type
  TTimeImportDM = class(TDataModule)
    Conn: TADOConnection;
    ExistingTimeData: TADODataSet;
    EmpData: TADODataSet;
    InsertTimesheet: TADOCommand;
    Timesheets: TDBISAMTable;
    TimesheetEntry: TADODataSet;
    EmpLookup: TADODataSet;
    PTOStartDate: TADODataSet;
    RefPTOHrs: TADODataSet;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FStartDate: TDateTime;
    FSaveChanges: Boolean;
    FEmployeeList: TStringList;
    FLogger: TOdLog;
    FTimeImportDoc: IXMLDomDocument;
    HC: TBaseHoursCalculator;
    FCountAdded, FCountUpdated: Integer;
    procedure ClearWeek;
    procedure AddTimeToHoursCalc;
    procedure GetImportedTime(const EmpNum: string);
    procedure GetExistingTime(const EmpNum: string; const EmpID: Integer);
    procedure RecalcHours(const EmpNum: string);
    procedure SaveToDatabase;
    function ParseTimeImportFile(const FileName: string): IXMLDomDocument;
    procedure Log(const Msg: string);
    procedure LoadEmployeeList(Nodes: IXMLDOMNodeList);
    procedure LogError(const Msg: string);
    function GetExistingSheetForEmpAndDate(const EmpNum: string; const EmpID: Integer; const WorkDate: TDateTime): Integer;
    function GetEmpIDForEmpNum(const EmpNum: string): Integer;
    procedure SetupTimesheetTable;
    function ValidateTimesheet(const EmpNum: string): Boolean;
    procedure ClearFilter(DataSet: TDataSet);
  public
    procedure Execute(const ImportFileName: string);
  end;

implementation

uses
  Variants, OdAdoUtils, OdIsoDates, OdMSXMLUtils, OdDBISAMUtils, OdDbUtils,
  QMConst, DateUtils, Forms;

{$WARN SYMBOL_PLATFORM OFF}

{$R *.dfm}

type
  TTimeSpan = record
    Start: string;
    Stop: string;
  end;

{ TTimeImportDM }

procedure TTimeImportDM.Execute(const ImportFileName: string);
var
  IniFileName, LogFileName, EmpNum, TimeRule: string;
  I: Integer;
begin
  try
    if not FileExists(ImportFileName) then
      raise Exception.CreateFmt('The specified time import file %s does not exist.',
        [ImportFileName]);

    if ParamCount > 1 then
      IniFileName := ParamStr(2)
    else
      IniFileName := 'qmserver.ini';
    if not FileExists(IniFileName) then
      raise Exception.CreateFmt('The configuration file %s does not exist.',
        [IniFileName]);

    LogFileName := 'QMTimeImport-' + FormatDateTime('yyyy-mm-dd-hhnnss', Now) + '.log';
    InitializeHandler(LogFileName, IniFileName);
    FLogger := TOdLog.Create(LogFileName, IniFileName);
    FSaveChanges := True;
    Log('QMTimeImport process starting ' + DateTimeToStr(Now));
    Log('Connecting to database');
    ConnectAdoConnectionWithIni(Conn, IniFileName);

    Log('Parsing time import file ' + ImportFileName);
    FTimeImportDoc := ParseTimeImportFile(ImportFileName);

    Log('Setting up temporary storage');
    SetupTimesheetTable;

    try
      Conn.BeginTrans;
      Log('Processing');

      for I := 0 to FEmployeeList.Count - 1 do begin
        EmpNum := FEmployeeList.Strings[I];
        EmpData.Close;
        EmpData.Parameters[0].Value := EmpNum;
        EmpData.Open;
        if EmpData.IsEmpty then begin
          LogError('WorkEmpNumber ' + EmpNum + ' is not in the QM employee table');
          Continue;
        end;

        TimeRule := EmpData.FieldByName('Code').AsString;
        HC := CreateHoursCalculator(TimeRule);
        try
          Log(CRLF + 'Employee ' + EmpNum + ' - ' + HC.Identity);
          ClearWeek;
          GetExistingTime(EmpNum, EmpData.FieldByName('emp_id').AsInteger);
          GetImportedTime(EmpNum);
          RecalcHours(EmpNum);
          if FSaveChanges then
            SaveToDatabase;
          Log('Processed employee ' + EmpNum);
        finally
          FreeAndNil(HC);
        end;
      end;

      if FSaveChanges then begin
        Conn.CommitTrans;
        Log(CRLF + 'Time file was imported into the QM database.');
        Log('----------------------------');
        Log(Format('Employees Processed: %7d', [FEmployeeList.Count]));
        Log(Format('Timesheets Added:    %7d', [FCountAdded]));
        Log(Format('Timesheets Updated:  %7d', [FCountUpdated]));
        Log('============================');
      end else begin
        Conn.RollbackTrans;
        Log(CRLF + 'Time file was not imported because errors were detected.');
      end;
    finally
      Conn.Close;
    end;
  except on E: Exception do
    LogError(E.Message);
  end;
  Log('QMTimeImport process complete ' + DateTimeToStr(Now));
end;

procedure TTimeImportDM.AddTimeToHoursCalc;
var
  WorkingHours: Double;
  CallOutHours: Double;
  DayIndex: Integer;
  WorkDate: TDateTime;
begin
  WorkingHours := GetWorkHours(Timesheets);
  CallOutHours := GetCalloutHours(Timesheets);

  WorkDate := Timesheets.FieldByName('work_date').AsDateTime;
  DayIndex := Round(WorkDate - FStartDate);

  if (DayIndex < 0) or (DayIndex > 6) then
    raise Exception.Create('Time for multiple week ending periods detected');

  HC.Worked[DayIndex] := WorkingHours;
  HC.Callout[DayIndex] := CallOutHours;
  HC.Regular[DayIndex] := Timesheets.FieldByName('reg_hours').AsFloat;
  HC.Overtime[DayIndex] := Timesheets.FieldByName('ot_hours').AsFloat;
  HC.DoubleTime[DayIndex] := Timesheets.FieldByName('dt_hours').AsFloat;
  HC.CalloutCalc[DayIndex] := Timesheets.FieldByName('callout_hours').AsFloat;
end;

function FmtHrs(Hours: Double): string;
const
  FmtString = '%6.2f';
begin
  if Abs(Hours) < 0.01 then
    Result := '      '
  else
    Result := Format(FmtString, [Hours]);
end;

function FmtMiles(Miles: Integer): string;
const
  FmtString = '%6d';
begin
  if Miles < 1 then
    Result := '      '
  else
    Result := Format(FmtString, [Miles]);
end;

procedure TTimeImportDM.ClearWeek;
begin
  HC.ClearResults;
  Timesheets.Close;
  ClearTable(Timesheets);
  Timesheets.Open;
end;

procedure TTimeImportDM.RecalcHours(const EmpNum: string);
var
  DayIdx: Integer;

  procedure UpdateHours;
  begin
    Timesheets.Edit;
    if not FloatEquals(Timesheets.FieldByName('reg_hours').AsFloat, HC.Regular[DayIdx], 0.01) then
      Timesheets.FieldByName('reg_hours').AsFloat := HC.Regular[DayIdx];
    if not FloatEquals(Timesheets.FieldByName('ot_hours').AsFloat, HC.Overtime[DayIdx], 0.01) then
      Timesheets.FieldByName('ot_hours').AsFloat := HC.Overtime[DayIdx];
    if not FloatEquals(Timesheets.FieldByName('dt_hours').AsFloat, HC.Doubletime[DayIdx], 0.01) then
      Timesheets.FieldByName('dt_hours').AsFloat := HC.Doubletime[DayIdx];
    if not FloatEquals(Timesheets.FieldByName('callout_hours').AsFloat, HC.CalloutCalc[DayIdx], 0.01) then
      Timesheets.FieldByName('callout_hours').AsFloat := HC.CalloutCalc[DayIdx];
    if Timesheets.Modified then begin
      Timesheets.FieldByName('status').Value := 'IMPORT';
      Timesheets.Post;
    end else
      Timesheets.Cancel;
  end;

begin
  try
    HC.Calculate;
  except
    HC.WriteStateToFile('TimeImportErrorState.txt');
    raise;
  end;

  Timesheets.Filter := 'status = ''IMPORT''';
  Timesheets.Filtered := True;
  try
    Timesheets.First;
    while not Timesheets.Eof do begin
      DayIdx := DaysBetween(Timesheets.FieldByName('work_date').AsDateTime, FStartDate);
      if not DayIdx in [0..6] then
        raise Exception.CreateFmt('Internal error, unexpected DayIdx of %d for employee %s ' +
          'and date %s', [DayIdx, EmpNum, Timesheets.FieldByName('work_date').AsString]);

      UpdateHours;
      Timesheets.Next;
    end;
  finally
    ClearFilter(Timesheets);
  end;
end;

procedure TTimeImportDM.DataModuleCreate(Sender: TObject);
begin
  FEmployeeList := TStringList.Create;
  FLogger := nil;
end;

procedure TTimeImportDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(HC);
  FreeAndNil(FEmployeeList);
  FreeAndNil(FLogger);
  UnInitializeHandler;
end;

procedure TTimeImportDM.SaveToDatabase;

  procedure SetPreviousSheetsToOldStatus(WorkEmpId: Integer; WorkDate: TDateTime);
  const
    UpdateSQL = 'update timesheet_entry set status = ''%s'' ' +
      'where (status = ''%s'' or status = ''%s'') ' +
      'and work_emp_id = %d and work_date = ''%s''';
  var
    SQL: string;
    RecsAffected: Integer;
  begin
    SQL := Format(UpdateSQL, [TimesheetStatusOld, TimesheetStatusActive,
      TimesheetStatusSubmit, WorkEmpId, IsoDateTimeToStr(WorkDate)]);
    RecsAffected := ExecuteQuery(Conn, SQL);
    Inc(FCountUpdated, RecsAffected);
  end;

  procedure InsertNewTimesheet;
  var
    FldIdx: Integer;
    FieldName: string;
  begin
    for FldIdx := 0 to Timesheets.FieldCount - 1 do begin
      FieldName := Timesheets.Fields[FldIdx].FieldName;
      SetParams(InsertTimesheet, FieldName, Timesheets.FieldByName(FieldName).Value);
    end;
    InsertTimesheet.Parameters.ParamByName('status').Value := TimesheetStatusSubmit;
    InsertTimesheet.Execute;
    Inc(FCountAdded);
  end;

begin
  Timesheets.Filter := 'status = ''IMPORT''';
  Timesheets.Filtered := True;
  Timesheets.First;
  if Timesheets.RecordCount > 0 then
    Log('  Saving to Database')
  else
    Log('  No new or changed time to save');

  try
    try
      while not Timesheets.Eof do begin
        if Timesheets.FieldByName('entry_id').AsInteger > 0 then
          SetPreviousSheetsToOldStatus(Timesheets.FieldByName('work_emp_id').AsInteger,
            Timesheets.FieldByName('work_date').AsDateTime);
        InsertNewTimesheet;
        Timesheets.Next;
      end;
    except
      on E: Exception do begin
        LogError(E.Message);
        raise;
      end;
    end;
  finally
    ClearFilter(Timesheets);
  end;
end;

function TTimeImportDM.ParseTimeImportFile(const FileName: string): IXMLDomDocument;
var
  TimeEntryElems: IXMLDOMNodeList;
begin
  Result := LoadXmlFile(FileName);
  if not Assigned(Result) then
    raise Exception.CreateFmt('Unable to create a Dom Document using the specified ' +
      'file. Verify %s is a valid xml document.', [FileName]);

  TimeEntryElems := Result.selectNodes('/QManager/TimeEntry');
  if not Assigned(TimeEntryElems) or (TimeEntryElems.length < 1) then
    raise Exception.Create('No /QManager/TimeEntry nodes detected in the import file.');

  LoadEmployeeList(TimeEntryElems);
end;

procedure TTimeImportDM.LoadEmployeeList(Nodes: IXMLDOMNodeList);
var
  I: Integer;
  EmpNumber: string;
begin
  Log('  Gathering employee numbers');
  FEmployeeList.Clear;
  for I := 0 to Nodes.length - 1 do begin
    if (GetAttributeTextFromNode(Nodes.item[I], 'WorkEmpNumber', EmpNumber)) and
      (FEmployeeList.IndexOf(EmpNumber) = -1) then
      FEmployeeList.Add(EmpNumber);
  end;
  Log(Format('  Detected %d %s for %d %s.', [Nodes.length,
    AddSIfNot1(Nodes.length, 'timesheet'), FEmployeeList.Count,
    AddSIfNot1(FEmployeeList.Count, 'employee')]));
end;

function TTimeImportDM.GetExistingSheetForEmpAndDate(const EmpNum: string; const EmpID: Integer; const WorkDate: TDateTime): Integer;
begin
  Result := -1 * Random(MaxInt);
  TimesheetEntry.Close;
  TimesheetEntry.Parameters.ParamByName('work_emp').Value := EmpID;
  TimesheetEntry.Parameters.ParamByName('work_date').Value := WorkDate;
  TimesheetEntry.Open;
  if HasRecords(TimesheetEntry) then
    Result := TimesheetEntry.FieldByName('entry_id').Value;
end;

function TTimeImportDM.GetEmpIDForEmpNum(const EmpNum: string): Integer;
begin
  Result := -1;
  EmpLookup.Close;
  EmpLookup.Parameters[0].Value := EmpNum;
  EmpLookup.Open;
  if HasRecords(EmpLookup) then
    Result := EmpLookup.FieldByName('emp_id').AsInteger;
  EmpLookup.Close;
end;

function TTimeImportDM.ValidateTimesheet(const EmpNum: string): Boolean;
var
  AnyErrors: Boolean;

  procedure AddError(Msg: string);
  begin
    AnyErrors := True;
    LogError(Format(Msg + ' for work date %s', [Timesheets.FieldByName('work_date').AsString]));
  end;

begin
  Assert((Timesheets.FieldByName('entry_id').AsInteger < 0) or
    (Timesheets.FieldByName('entry_id').AsInteger = TimesheetEntry.FieldByName('entry_id').AsInteger),
    'Existing timesheet_entry table not in sync with temporary timesheet_entry table');

  AnyErrors := False;
  try
    CheckWorkPeriods(Timesheets, 'work_', 'Work', TimesheetNumWorkPeriods);
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckWorkPeriods(Timesheets, 'callout_', 'Callout', TimesheetNumCalloutPeriods);
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'leave_hours', 'Leave');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'vac_hours', 'Vacation');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'br_hours', 'Bereavement');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'hol_hours', 'Holiday');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'jury_hours', 'Jury duty');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckHoursEntry(Timesheets, 'pto_hours', 'PTO');
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckPTOHours(TimeSheets, PTOStartDate, RefPTOHrs);
  except
    on E: Exception do AddError(E.Message);
  end;
  try
    CheckOverlap(Timesheets, 'work_', 'callout_',
      TimesheetNumWorkPeriods, TimesheetNumCalloutPeriods);
  except
    on E: Exception do AddError(E.Message);
  end;

  if Timesheets.FieldByName('vehicle_use').Value = 'COV' then begin
    if Timesheets.FieldByName('miles_start1').IsNull then
      AddError('StartMiles must be more than 0 for COV Vehicle Use')
    else if Timesheets.FieldByName('miles_stop1').AsInteger < Timesheets.FieldByName('miles_start1').AsInteger then
      AddError('EndMiles cannot be less than StartMiles')
  end else if (Timesheets.FieldByName('miles_start1').AsInteger > 0) or
    (Timesheets.FieldByName('miles_stop1').AsInteger > 0) then
    AddError('StartMiles and EndMiles only apply to COV Vehicle Use');

  if HaveNonNullField(TimesheetEntry, 'final_approve_by') then
    AddError('Cannot replace final approved timesheet')
  else if HaveNonNullField(TimesheetEntry, 'approve_by') then
    AddError('Cannot replace approved timesheet');

  Result := not AnyErrors;
end;

procedure TTimeImportDM.GetImportedTime(const EmpNum: string);
var
  WorkSpans: array[0..TimesheetNumWorkPeriods-1] of TTimeSpan;
  CalloutSpans: array[0..TimesheetNumCalloutPeriods-1] of TTimeSpan;
  UpdateState: string;

  procedure ClearSpans;
  var
    i: Integer;
  begin
    for i := Low(WorkSpans) to High(WorkSpans) do begin
      WorkSpans[i].Start := '';
      WorkSpans[i].Stop := '';
    end;
    for i := Low(CalloutSpans) to High(CalloutSpans) do begin
      CalloutSpans[i].Start := '';
      CalloutSpans[i].Stop := '';
    end;
  end;

  function GetHoursData(Node: IXMLDOMNode; const HourCategory: WideString): HoursType;
  var
    s: WideString;
  begin
    Result := 0.0;
    if GetNodeText(Node, HourCategory, s) then
      Result := StrToFloat(s);
  end;

  procedure SetSpanTime(const FieldName, SpanTime: string);
  begin
    if IsEmpty(SpanTime) then
      Timesheets.FieldByName(FieldName).Clear
    else begin
      try
        Timesheets.FieldByName(FieldName).AsDateTime := StrToTime(SpanTime);
      except
        on EConvertError do
          LogError(Format('%s is not a valid time in %s for employee %s and date %s',
            [SpanTime, FieldName, EmpNum, Timesheets.FieldByName('work_date').AsString]));
      end;
    end;
  end;

  procedure LoadFieldsToCheck(var FieldNames: array of string);
  var
    I, J: Integer;
  begin
    J := 0;
    for I := 1 to TimesheetNumWorkPeriods do begin
      FieldNames[J] := 'work_start' + IntToStr(I);
      Inc(J);
      FieldNames[J] := 'work_stop' + IntToStr(I);
      Inc(J);
    end;
    J := TimesheetNumWorkPeriods * 2;
    for I := 1 to TimesheetNumCalloutPeriods do begin
      FieldNames[J] := 'callout_start' + IntToStr(I);
      Inc(J);
      FieldNames[J] := 'callout_stop' + IntToStr(I);
      Inc(J);
    end;
    J := (TimesheetNumWorkPeriods * 2) + (TimesheetNumCalloutPeriods * 2);
    FieldNames[J] := 'miles_start1';
    FieldNames[J+1] := 'miles_stop1';
    FieldNames[J+2] := 'vehicle_use';
    FieldNames[J+3] := 'vac_hours';
    FieldNames[J+4] := 'leave_hours';
    FieldNames[J+5] := 'br_hours';
    FieldNames[J+6] := 'hol_hours';
    FieldNames[J+7] := 'jury_hours';
    FieldNames[J+8] := 'entry_date_local';
    FieldNames[J+9] := 'pto_hours';
  end;

const
  TimeEntryRoot = '/QManager/TimeEntry';
  TimesheetsForEmp = TimeEntryRoot + '[@WorkEmpNumber="%s"]';
  WorkSpanForSheet = 'WorkSpan';
  CalloutSpanForSheet = 'CalloutSpan';
var
  SheetsForEmp: IXMLDOMNodeList;
  Time: IXMLDOMNode;
  Spans: IXMLDOMNodeList;
  Vehicle: IXMLDOMNode;
  I: Integer;
  J: Integer;
  TSEID: Integer;
  WorkEmpID: Integer;
  EnteredByEmpID: Integer;
  WorkDateText: string;
  WorkDate: TDateTime;
  EnteredByEmp: string;
  EnteredDateText: string;
  EnteredDate: TDateTime;
  HolidayHrs: HoursType;
  LeaveHrs: HoursType;
  BereaveHrs: HoursType;
  VacHrs: HoursType;
  JuryHrs: HoursType;
  PTOHrs: HoursType;
  VehicleFlag: string;
  MilesStart: Integer;
  MilesStop: Integer;
  s: string;
  NoErrors: Boolean;
  TimesheetFieldsToCheck: array of string;
begin
  SetLength(TimesheetFieldsToCheck,
    (TimesheetNumWorkPeriods * 2) + (TimesheetNumCalloutPeriods * 2) + 10);
  LoadFieldsToCheck(TimesheetFieldsToCheck);
  SheetsForEmp := FTimeImportDoc.selectNodes(Format(TimesheetsForEmp, [EmpNum]));

  FStartDate := EncodeDate(2150, 12, 31);
  for I := 0 to SheetsForEmp.length - 1 do begin
    ClearSpans;
    Time := SheetsForEmp.item[I];
    if not GetAttributeTextFromNode(Time, 'WorkDate', WorkDateText) then begin
      LogError('Missing WorkDate attribute for employee ' + EmpNum);
      WorkDate := 0;
    end else
      WorkDate := IsoStrToDate(WorkDateText);

    if (BeginningOfTheWeek(WorkDate) < FStartDate) then
      FStartDate := BeginningOfTheWeek(WorkDate);

    if not GetAttributeTextFromNode(Time, 'EntryByEmpNumber', EnteredByEmp) then
      EnteredByEmp := EmpNum;
    if not GetAttributeTextFromNode(Time, 'EntryDate', EnteredDateText) then
      EnteredDateText := IsoDateTimeToStr(Now);
    EnteredDate := IsoStrToDateTime(EnteredDateText);

    WorkEmpID := GetEmpIDForEmpNum(EmpNum);
    EnteredByEmpID := GetEmpIDForEmpNum(EnteredByEmp);
    TSEID := GetExistingSheetForEmpAndDate(EmpNum, WorkEmpID, WorkDate);
    if Timesheets.Locate('work_emp_id;work_date;status',
      VarArrayOf([WorkEmpID, WorkDate, 'IMPORT']), []) then
      LogError(Format('Multiple TimeEntry nodes for employee %s and date %s.', [EmpNum, IsoDateToStr(WorkDate)]));

    Spans := Time.selectNodes(WorkSpanForSheet);
    if Spans.length > High(WorkSpans)+1 then
      LogError(Format('WorkSpan limit of %d exceeded for employee %s and date %s.',
        [High(WorkSpans)+1, EmpNum, WorkDate]));
    for J := 0 to Spans.length - 1 do begin
      if not GetAttributeTextFromNode(Spans.item[J], 'Start', WorkSpans[J].Start) then
        LogError('Missing Start attribute on WorkSpan #' + IntToStr(J+1));
      if not GetAttributeTextFromNode(Spans.item[J], 'End', WorkSpans[J].Stop) then
        LogError('Missing Stop attribute on WorkSpan #' + IntToStr(J+1));
    end;

    Spans := Time.selectNodes(CalloutSpanForSheet);
    if Spans.length > High(CalloutSpans)+1 then
      LogError(Format('CalloutSpan limit of %d exceeded for employee %s and date %s.',
        [High(CalloutSpans)+1, EmpNum, WorkDate]));
    for J := 0 to Spans.length - 1 do begin
      if not GetAttributeTextFromNode(Spans.item[J], 'Start', CalloutSpans[J].Start) then
        LogError('Missing Start attribute on CalloutSpan #' + IntToStr(J+1));
      if not GetAttributeTextFromNode(Spans.item[J], 'End', CalloutSpans[J].Stop) then
        LogError('Missing Stop attribute on CalloutSpan #' + IntToStr(J+1));
    end;

    LeaveHrs := GetHoursData(Time, 'LeaveHours');
    BereaveHrs := GetHoursData(Time, 'BereavementHours');
    VacHrs := GetHoursData(Time, 'VacationHours');
    HolidayHrs := GetHoursData(Time, 'HolidayHours');
    JuryHrs := GetHoursData(Time, 'JuryHours');
    PTOHrs := GetHoursData(Time, 'PTOHours');

    VehicleFlag := 'NA';
    MilesStart := -1;
    MilesStop := -1;
    Vehicle := Time.selectSingleNode('Vehicle');
    if Assigned(Vehicle) then begin
      if not GetAttributeTextFromNode(Vehicle, 'Use', VehicleFlag) then
        VehicleFlag := 'NA';
      if GetAttributeTextFromNode(Vehicle, 'StartMiles', s) then
        MilesStart := StrToIntDef(s, -1);
      if GetAttributeTextFromNode(Vehicle, 'EndMiles', s) then
        MilesStop := StrToIntDef(s, -1);
    end;

    // add to temporary memory dataset
    Timesheets.Insert;
    Timesheets.FieldByName('status').AsString := 'IMPORT';
    Timesheets.FieldByName('entry_id').AsInteger := TSEID;
    Timesheets.FieldByName('work_emp_id').AsInteger := WorkEmpID;
    Timesheets.FieldByName('entry_by').AsInteger := EnteredByEmpID;
    Timesheets.FieldByName('entry_date_local').AsDateTime := EnteredDate;
    Timesheets.FieldByName('entry_date').AsDateTime := Now;
    Timesheets.FieldByName('work_date').AsDateTime := WorkDate;
    Timesheets.FieldByName('hol_hours').AsFloat := HolidayHrs;
    Timesheets.FieldByName('leave_hours').AsFloat := LeaveHrs;
    Timesheets.FieldByName('br_hours').AsFloat := BereaveHrs;
    Timesheets.FieldByName('vac_hours').AsFloat := VacHrs;
    Timesheets.FieldByName('jury_hours').AsFloat :=  JuryHrs;
    Timesheets.FieldByName('pto_hours').AsFloat := PTOHrs;
    Timesheets.FieldByName('vehicle_use').AsString := VehicleFlag;
    Timesheets.FieldByName('reg_hours').AsFloat := 0.00;
    Timesheets.FieldByName('ot_hours').AsFloat := 0.00;
    Timesheets.FieldByName('dt_hours').AsFloat := 0.00;
    Timesheets.FieldByName('callout_hours').AsFloat := 0.00;
    Timesheets.FieldByName('floating_holiday').AsBoolean := False;
    Timesheets.FieldByName('per_diem').AsBoolean := False;
    if MilesStart > 0 then
      Timesheets.FieldByName('miles_start1').AsInteger := MilesStart;
    if MilesStop > 0 then
      Timesheets.FieldByName('miles_stop1').AsInteger := MilesStop;
    for J := Low(WorkSpans) to High(WorkSpans) do begin
      SetSpanTime('work_start'+IntToStr(J+1), WorkSpans[J].Start);
      SetSpanTime('work_stop'+IntToStr(J+1), WorkSpans[J].Stop);
    end;
    for J := Low(CalloutSpans) to High(CalloutSpans) do begin
      SetSpanTime('callout_start'+IntToStr(J+1), CalloutSpans[J].Start);
      SetSpanTime('callout_stop'+IntToStr(J+1), CalloutSpans[J].Stop);
    end;
    NoErrors := ValidateTimesheet(EmpNum);

    if TSEID < 0 then
      UpdateState := 'ADDED   '
    else
      UpdateState := 'UPDATED ';
    // If new timesheet for emp & day, or existing sheet changed, import it.
    if NoErrors and ((TSEID < 0) or ((TSEID > 0) and
      AnyFieldsDiffer(Timesheets, TimesheetEntry, TimesheetFieldsToCheck))) then begin
      PostDataSet(Timesheets);
      try
        AddTimeToHoursCalc;
        Log(Format('  %s %s', [UpdateState, WorkDateText]));
      except
        on E: Exception do begin
          LogError(E.Message + ' for employee ' + EmpNum + ' and date ' + WorkDateText);
          CancelDataSet(Timesheets);
        end;
      end;
    end else
      CancelDataSet(Timesheets);
  end;
end;

procedure TTimeImportDM.GetExistingTime(const EmpNum: string; const EmpID: Integer);
var
  I: Integer;
  WorkDate: TDateTime;
begin
  ExistingTimeData.Parameters.ParamByName('EmpNum').Value := EmpNum;
  ExistingTimeData.Parameters.ParamByName('StartDate').Value := FStartDate;
  ExistingTimeData.Parameters.ParamByName('EndDate').Value := FStartDate + 6;
  ExistingTimeData.Open;
  try
    for I := 0 to 6 do begin
      WorkDate := FStartDate + I;
      if ExistingTimeData.Locate('work_date', WorkDate, []) then begin
        try
          CopyRecord(ExistingTimeData, Timesheets);
          AddTimeToHoursCalc;
        except
          on E: Exception do
            LogError(E.Message + ' getting existing time for employee ' +
              EmpNum + ' and date ' + IsoDateToStr(WorkDate));
        end;
      end;
    end;
  finally
    ExistingTimeData.Close;
  end;
end;

procedure TTimeImportDM.Log(const Msg: string);
begin
  FLogger.Write(Msg);
  {$IFNDEF TEST_MODE}
  WriteLn(Msg);
  {$ENDIF}
end;

procedure TTimeImportDM.LogError(const Msg: string);
begin
  FSaveChanges := False;
  FLogger.Write('* Error: ' + Msg);
  {$IFNDEF TEST_MODE}
  WriteLn('* Error: ' + Msg);
  {$ENDIF}
end;

procedure TTimeImportDM.SetupTimesheetTable;
const
  SelectTimesheet = 'select * from timesheet_entry ' +
    'where work_emp_id = :work_emp and work_date = :work_date ' +
    'and status <> ''' + TimesheetStatusOld + '''';
begin
  CloneStructure(TimesheetEntry, Timesheets);
  TimesheetEntry.CommandText := SelectTimesheet;
end;

procedure TTImeImportDM.ClearFilter(DataSet: TDataSet);
begin
  DataSet.Filtered := False;
  DataSet.Filter := '';
end;

initialization
  Randomize;

end.
