object TimeImportDM: TTimeImportDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 209
  Width = 420
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=canucanoe;Persist Security Info=Tru' +
      'e;User ID=sa;Initial Catalog=TestDB;Data Source=localhost'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 64
    Top = 24
  end
  object ExistingTimeData: TADODataSet
    CacheSize = 500
    Connection = Conn
    CommandText = 
      'select tse.*'#13#10'from timesheet_entry tse'#13#10'   left join employee em' +
      'p on tse.work_emp_id = emp.emp_id'#13#10'where (tse.status <> '#39'OLD'#39')'#13#10 +
      ' and tse.work_date between :StartDate and :EndDate'#13#10' and emp.emp' +
      '_number = :EmpNum'#13#10'order by tse.work_date'#13#10
    Parameters = <
      item
        Name = 'StartDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EndDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EmpNum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 144
    Top = 24
  end
  object EmpData: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select e.emp_id, e.active, r.code from employee e'#13#10'inner join re' +
      'ference r on e.timerule_id = r.ref_id'#13#10'where emp_number = :emp_n' +
      'umber'
    Parameters = <
      item
        Name = 'emp_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 144
    Top = 74
  end
  object InsertTimesheet: TADOCommand
    CommandText = 
      'INSERT INTO timesheet_entry (work_emp_id,work_date,status,entry_' +
      'date,entry_date_local,entry_by,work_start1,work_stop1,work_start' +
      '2,work_stop2,work_start3,work_stop3,work_start4,work_stop4,'#13#10'wor' +
      'k_start5,work_stop5,callout_start1,callout_stop1,callout_start2,' +
      'callout_stop2,callout_start3,callout_stop3,callout_start4,callou' +
      't_stop4,callout_start5,callout_stop5,callout_start6,callout_stop' +
      '6,'#13#10'miles_start1,miles_stop1,vehicle_use,reg_hours,ot_hours,dt_h' +
      'ours,callout_hours,vac_hours,leave_hours,br_hours,hol_hours,jury' +
      '_hours,pto_hours,floating_holiday,work_start6,work_stop6,work_st' +
      'art7,work_stop7,'#13#10'work_start8,work_stop8,work_start9,work_stop9,' +
      'work_start10,work_stop10,work_start11,work_stop11,work_start12,w' +
      'ork_stop12,work_start13,work_stop13,work_start14,work_stop14,'#13#10'w' +
      'ork_start15,work_stop15,work_start16,work_stop16,callout_start7,' +
      'callout_stop7,callout_start8,callout_stop8,callout_start9,callou' +
      't_stop9,callout_start10,callout_stop10,callout_start11,callout_s' +
      'top11,'#13#10'callout_start12,callout_stop12,callout_start13,callout_s' +
      'top13,callout_start14,callout_stop14,callout_start15,callout_sto' +
      'p15,callout_start16,callout_stop16) '#13#10'values (:work_emp_id,:work' +
      '_date,:status,:entry_date,:entry_date_local,:entry_by,:work_star' +
      't1,:work_stop1,:work_start2,:work_stop2,:work_start3,:work_stop3' +
      ',:work_start4,:work_stop4,'#13#10':work_start5,:work_stop5,:callout_st' +
      'art1,:callout_stop1,:callout_start2,:callout_stop2,:callout_star' +
      't3,:callout_stop3,:callout_start4,:callout_stop4,:callout_start5' +
      ',:callout_stop5,:callout_start6,:callout_stop6,'#13#10':miles_start1,:' +
      'miles_stop1,:vehicle_use,:reg_hours,:ot_hours,:dt_hours,:callout' +
      '_hours,:vac_hours,:leave_hours,:br_hours,:hol_hours,:jury_hours,' +
      ':pto_hours,:floating_holiday,:work_start6,:work_stop6,:work_star' +
      't7,:work_stop7,'#13#10':work_start8,:work_stop8,:work_start9,:work_sto' +
      'p9,:work_start10,:work_stop10,:work_start11,:work_stop11,:work_s' +
      'tart12,:work_stop12,:work_start13,:work_stop13,:work_start14,:wo' +
      'rk_stop14,'#13#10':work_start15,:work_stop15,:work_start16,:work_stop1' +
      '6,:callout_start7,:callout_stop7,:callout_start8,:callout_stop8,' +
      ':callout_start9,:callout_stop9,:callout_start10,:callout_stop10,' +
      ':callout_start11,:callout_stop11,'#13#10':callout_start12,:callout_sto' +
      'p12,:callout_start13,:callout_stop13,:callout_start14,:callout_s' +
      'top14,:callout_start15,:callout_stop15,:callout_start16,:callout' +
      '_stop16)'
    Connection = Conn
    Parameters = <
      item
        Name = 'work_emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'status'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'entry_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'entry_date_local'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'entry_by'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_start1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop4'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop5'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'miles_start1'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'miles_stop1'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'vehicle_use'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'reg_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'ot_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'dt_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'callout_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'vac_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'leave_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'br_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'hol_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'jury_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'pto_hours'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'floating_holiday'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'work_start6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop6'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start7'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop7'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start8'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop8'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start9'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop9'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start10'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop10'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start11'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop11'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start12'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop12'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start13'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop13'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start14'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop14'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start15'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop15'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_start16'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_stop16'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start7'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop7'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start8'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop8'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start9'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop9'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start10'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop10'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start11'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop11'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start12'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop12'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start13'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop13'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start14'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop14'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start15'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop15'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_start16'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'callout_stop16'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 240
    Top = 27
  end
  object Timesheets: TDBISAMTable
    DatabaseName = 'memory'
    EngineVersion = '4.34 Build 7'
    Exclusive = True
    TableName = 'timesheet_entry'
    Left = 344
    Top = 24
  end
  object TimesheetEntry: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 'select * from timesheet_entry where 0=1'
    Parameters = <>
    Left = 344
    Top = 72
  end
  object EmpLookup: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 'select emp_id from employee'#13#10'where emp_number = :emp_number'
    Parameters = <
      item
        Name = 'emp_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 144
    Top = 124
  end
  object PTOStartDate: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select * from configuration_data where name='#39'PTOHoursEntryStartD' +
      'ate'#39
    Parameters = <>
    Left = 232
    Top = 122
  end
  object RefPTOHrs: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select * from reference where type='#39'ptohrs'#39' and description=:pto' +
      '_hours and active_ind=1'
    Parameters = <
      item
        Name = 'pto_hours'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 312
    Top = 122
  end
end
