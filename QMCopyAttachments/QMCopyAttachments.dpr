program QMCopyAttachments;

{$APPTYPE CONSOLE}
{$WARN SYMBOL_PLATFORM OFF}

{$R '..\QMIcon.res'}
{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  DateUtils,
  StrUtils,
  CopyAttachmentsDMu in 'CopyAttachmentsDMu.pas' {CopyAttachmentsDM: TDataModule},
  QMConst in '..\client\QMConst.pas',
  ServerAttachmentDMu in '..\server\ServerAttachmentDMu.pas',
  ComInit in '..\common\ComInit.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdLog in '..\common\OdLog.pas' {ExceptionDialog},
  OdExceptions in '..\common\OdExceptions.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  OdUqInternet in '..\common\OdUqInternet.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdFtp in '..\common\OdFtp.pas',
  CommandLine in '..\common\CommandLine.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  dcpcrypt2 in '..\thirdparty\dcpcrypt\dcpcrypt2.pas';

const
  LogFilenamePrefix = 'QMCopyAttachments';

type
  TApplicationMode = (amHelp, amCopyAttachments, amInvalidSyntax);

  { TODO : Possible ways to refactor:
    * Move TStatusEvents into it's own unit
    * Rename it to something like TCopyAttachmentsMain
    * Move most of the code from this unit into TCopyAttachmentsMain, leaving
      mainly just the outer exception handler
    * Move the ComInit Initialization and Finalization code into the new unit,
      and remove ComInit
    Otherwise, consider renaming TStatusEvents. }
  TStatusEvents = class(TObject)
  private
    FStartTime: TDateTime;
    FMessageLog: TOdLog;
    FExecuteInitialized: Boolean;
    FAttachmentsToCopy: Integer;
    FAttachmentsCopied: Integer;
    FDetailStarted: Boolean;
    FErrorsLogged: Integer;
  public
    property StartTime: TDateTime read FStartTime write FStartTime;
    procedure OpenLog(LogFilename, IniFilename: string);
    procedure Log(Sender: TObject; Msg: string;
      StatusEventFlags: TStatusEventFlags=[seDetailed]);
    destructor Destroy; override;
  end;

var
  ApplicationMode: TApplicationMode;
  CopyAttachmentsDM: TCopyAttachmentsDM;
  IniFilename: string;
  LogFilename: string;
  WOID: Integer;
  TicketID: Integer;
  ErrorsLogged: Integer;
  StatusEvents: TStatusEvents;
  StartTime: TDateTime;
  EndTime: TDateTime;

  function ValidateParams(out IniFilename: string; out WOID, TicketID: Integer):
    TApplicationMode;
  var
    NextParam, ParamsLeft: Integer;
  begin
    if HelpSwitch then
      Result := amHelp
    else begin
      NextParam := 1;
      INIFilename := ParamStrIfNext('/ini', NextParam);
      ParamsLeft := ParamCount - NextParam + 1;

      if (ParamsLeft = 2) and TryParamStrToIDInt(NextParam, WOID) and
        TryParamStrToIDInt(NextParam + 1, TicketID) then
        Result := amCopyAttachments
      else
        Result := amInvalidSyntax;
    end;
  end;

  procedure ShowHelp;
  begin                       
    Writeln('Version: ' + AppVersionShort);
    Writeln('Copy attachments from a work order to a ticket.');
    Writeln;
    Writeln('QMCopyAttachments [/ini INIFilename] wo_id ticket_id');
    Writeln;
    Writeln('/ini INIFilename  Configuration filename (defaults to QMServer.ini, in the');
    Writeln('                  current directory). The database and log file configurations');
    Writeln('                  come from this file.');
    Writeln('wo_id             Specifies the work order ID (work_order.wo_id)');
    Writeln('ticket_id         Specifies the ticket ID (ticket.ticket_id)');
    Writeln;
    Writeln('ERRORLEVEL will be set as follows, when the utility exits:');
    Writeln('0 - All attachments successfully copied, and no errors encountered.');
    Writeln('1 - All attachments successfully copied, but 1 or more errors encountered. ');
    Writeln('2 - One or more attachments were not successfully copied. Rerunning the command');
    Writeln('    may fix this.');
  end;

  function CheckInvalidSyntaxMode: Boolean;
  begin
    if ApplicationMode = amInvalidSyntax then begin
      StatusEvents.Log(nil, 'Error: Invalid syntax', [seFatalError]);
      Writeln;
      ShowHelp;
      Result := True;
    end else
      Result := False;
  end;

  function GetLogFilename(StartTime: TDateTime; Extension: string): string;
  begin
    Result := LogFilenamePrefix + '-' +
      FormatDateTime('yyyy-mm-dd', StartTime) + '.' + Extension;
  end;

  function SummaryMessage(StatusEvents: TStatusEvents;
    LogFilename: string): string;
  var
    CopyMsg, ErrorsMsg, DetailsMsg: string;
  begin
    { Tell the user how successful the copy operation was. If there were any
      issues. And where to find the details. }

    if StatusEvents.FExecuteInitialized then begin
      if StatusEvents.FAttachmentsToCopy < 0 then
        CopyMsg := 'Copy failed'
      else
        CopyMsg := IntToStr(StatusEvents.FAttachmentsCopied) + ' of ' +
          IntToStr(StatusEvents.FAttachmentsToCopy) +
          AddSIfNot1(StatusEvents.FAttachmentsToCopy, ' file') +
          ' successfully copied'
    end
    else
      CopyMsg := 'Copy failed';

    ErrorsMsg := IntToStr(StatusEvents.FErrorsLogged) + AddSIfNot1(
      StatusEvents.FErrorsLogged, ' error') + ' encountered';

    if StatusEvents.FDetailStarted then
      DetailsMsg := 'Details written to log file:' + sLineBreak + '- ' +
        LogFilename
    else if LogFilename <> '' then
      DetailsMsg := 'No details written to log file:' + sLineBreak + '- ' +
        LogFilename
    else
      DetailsMsg := 'Log filename not determined';

    Result := sLineBreak + CopyMsg + sLineBreak + ErrorsMsg + sLineBreak +
      DetailsMsg;
  end;

{ TStatusEvents }

destructor TStatusEvents.Destroy;
begin
  FreeAndNil(FMessageLog);
  inherited;
end;

procedure TStatusEvents.Log(Sender: TObject; Msg: string;
  StatusEventFlags: TStatusEventFlags);
var
  WriteBrief, WriteDetailed: Boolean;
  DetailedMsg, BriefMsg: string;
begin
  if Sender is TCopyAttachmentsDM then begin
    FAttachmentsToCopy := TCopyAttachmentsDM(Sender).NumberAttachmentsToCopy;
    FAttachmentsCopied := TCopyAttachmentsDM(Sender).NumberAttachmentsCopied;
  end;

  WriteBrief := False;
  WriteDetailed := False;

  if StatusEventFlags * ErrorFlags <> [] then begin
    WriteBrief := True;
    if Assigned(FMessageLog) then
      WriteDetailed := True;
    Inc(FErrorsLogged);
  end;

  if seInitialized in StatusEventFlags then
    FExecuteInitialized := True;

  if FExecuteInitialized and (FAttachmentsToCopy >= 0) and
    (FAttachmentsCopied = FAttachmentsToCopy) then
    ExitCode := 1; // Copying successful, but non-copy errors are possible

  if seBrief in StatusEventFlags then
    WriteBrief := True;

  if seDetailed in StatusEventFlags then
    WriteDetailed := True;

  if WriteBrief then begin
    BriefMsg := Msg;
    if StatusEventFlags * [seStartingFileCopy] <> [] then
      BriefMsg := sLineBreak + BriefMsg;
    Writeln(BriefMsg);
  end;

  if WriteDetailed then begin
    Assert(Assigned(FMessageLog));

    DetailedMsg := Msg;
    if not FDetailStarted then
      DetailedMsg := '----------' + sLineBreak + DateTimeToStr(FStartTime) +
        ': ' + DetailedMsg
    else if (StatusEventFlags * [seStartingFileCopy, seCopySummary] <> []) then
      // Separate each file copy section
      DetailedMsg := sLineBreak + DetailedMsg;

    FMessageLog.Write(DetailedMsg);
    FDetailStarted := True;
  end;

  { TODO : Set rerun flag, and add a new file logger for a "rerun" .bat file }
end;

procedure TStatusEvents.OpenLog(LogFilename, IniFilename: string);
begin
  if not Assigned(FMessageLog) then begin
    FMessageLog := TOdLog.Create(LogFilename, IniFilename);
    FMessageLog.OpenLog;
  end;
end;

begin
  try
    ExitCode := 2; // Requires rerunning
    StatusEvents := TStatusEvents.Create;
    try
      try
        StartTime := Now;
        StatusEvents.StartTime := StartTime;
        ApplicationMode := ValidateParams(IniFilename, WOID, TicketID);

        if ApplicationMode = amHelp then begin
          ShowHelp;
          ExitCode := 0; // Ok
          Exit;
        end;

        if IniFilename = '' then
          IniFilename := 'QMServer.ini';
        if not FileExists(IniFilename) then begin
          StatusEvents.Log(nil, 'Error: Ini file, ' + IniFilename + ', not ' +
            'found', [seFatalError]);
          CheckInvalidSyntaxMode;
          Exit;
        end;

        LogFilename := GetLogFilename(StartTime, 'log');
        InitializeHandler(LogFilename, IniFilename);
        StatusEvents.Log(nil, 'Opening log file for reporting detailed ' +
          'progress:' + sLineBreak + '- ' + LogFilename, [seBrief]);

        StatusEvents.OpenLog(LogFilename, IniFilename);

        if CheckInvalidSyntaxMode then
          Exit;

        StatusEvents.Log(nil, 'Copy from wo_id ' + IntToStr(WOID) +
          ' to ticket_id ' + IntToStr(TicketID) + ' started');

        try
          CopyAttachmentsDM := TCopyAttachmentsDM.Create(nil);
          CopyAttachmentsDM.OnLogStatusEvent := StatusEvents.Log;
          CopyAttachmentsDM.Execute(WOID, TicketID, IniFilename);
        finally
          FreeAndNil(CopyAttachmentsDM);
        end;

        EndTime := Now;
        StatusEvents.Log(nil, 'Completed in ' + FloatToStr(MilliSecondsBetween(
          EndTime, StartTime) / 1000) + ' seconds');

        StatusEvents.Log(nil, SummaryMessage(StatusEvents, LogFilename),
          [seBrief]);
      except
        on E: Exception do
          StatusEvents.Log(nil, 'Error: ' + E.Message, [seFatalError]);
      end;
    finally
      ErrorsLogged := StatusEvents.FErrorsLogged;
      FreeAndNil(StatusEvents);
      UnInitializeHandler;
    end;

    if (ExitCode = 1) and (ErrorsLogged = 0) then
      // All files successfully copied and no errors logged
      ExitCode := 0; // Ok
  except
    on E: Exception do
      Writeln('Error: ' + E.Message);
  end;
end.
