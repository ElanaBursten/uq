object CopyAttachmentsDM: TCopyAttachmentsDM
  OldCreateOrder = False
  Height = 254
  Width = 298
  object GetAttachmentsToCopy: TADOStoredProc
    Connection = DBConnection
    ProcedureName = 'get_attachments_to_copy;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@wo_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ticket_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@destination_linked_to_source'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@download_location_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 152
    Top = 16
  end
  object DBConnection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=ihjidojo3;Persist Security Info=Tru' +
      'e;User ID=uqweb;Initial Catalog=QM;Data Source=DYATL-DQMGDB01'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = DBConnectionAfterConnect
    Left = 40
    Top = 16
  end
  object GetSystemEmpID: TADODataSet
    Connection = DBConnection
    CommandText = 'select dbo.GetConfigValue('#39'SystemUserEmpID'#39', null)'
    Parameters = <>
    Left = 152
    Top = 72
  end
  object qryAPI_storage_credentials: TADOQuery
    Connection = DBConnection
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[storage_type]'
      '      ,[enc_access_key]'
      '      ,[enc_secret_key]'
      '      ,[unc_date_created]'
      '      ,[modified_date]'
      '      ,[unc_date_expired]'
      '      ,[api_storage_constants_id]'
      '      ,[description]'
      '  FROM [dbo].[api_storage_credentials]'
      '  where id = 1')
    Left = 208
    Top = 136
  end
  object qryAPI_storage_constants: TADOQuery
    Connection = DBConnection
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[host]'
      '      ,[rest_url]'
      '      ,[service]'
      '      ,[bucket]'
      '      ,[bucket_folder]'
      '      ,[region]'
      '      ,[algorithm]'
      '      ,[signed_headers]'
      '      ,[content_type]'
      '      ,[accepted_values]'
      '      ,[environment]'
      '      ,[description]'
      '      ,[unc_date_modified]'
      '      ,[modified_date]'
      '  FROM [dbo].[api_storage_constants]'
      '  where id = 1')
    Left = 40
    Top = 144
  end
end
