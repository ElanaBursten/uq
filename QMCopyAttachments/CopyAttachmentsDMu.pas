unit CopyAttachmentsDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, ServerAttachmentDMu, OdUqInternet;

type
  TStatusEventFlag = (seBrief, seDetailed, seError, seDeleteError, seFatalError,
    seInitialized, seStartingFileCopy, seCopySummary);
  TStatusEventFlags = set of TStatusEventFlag;
  TLogStatusEventProcedure = procedure(Sender: TObject; Msg: string;
    StatusEventFlags: TStatusEventFlags) of object;

const
  ErrorFlags: set of TStatusEventFlag = [seError, seDeleteError, seFatalError];

type
  TCopyAttachmentsDM = class(TDataModule)
    GetAttachmentsToCopy: TADOStoredProc;
    DBConnection: TADOConnection;
    GetSystemEmpID: TADODataSet;
    qryAPI_storage_credentials: TADOQuery;
    qryAPI_storage_constants: TADOQuery;
    procedure DBConnectionAfterConnect(Sender: TObject);
  private
    FOnLogStatusEvent: TLogStatusEventProcedure;
    FNumberAttachmentsToCopy: Integer;
    FNumberAttachmentsCopied: Integer;
    FSystemEmpID: Integer;
    function CopyAttachment(ServerAttachment: TServerAttachment;
      var DownloadedFilename: string; var UploadFilename: string;
      TicketID: Integer): Boolean;
    procedure CleanUpFiles(DownloadedFilename, UploadFilename: string);
    function CopySingleAttachment(ServerAttachment: TServerAttachment;
      TicketID: Integer): Boolean;
    procedure LogServerAttachmentMessage(Sender: TObject; const LogMsg: string);
    procedure LogStatusEvent(Msg: string;
      StatusEventFlags: TStatusEventFlags=[seDetailed]);
    function GetSystemUserEmpID: Integer;
    function GetAWSConstants: Boolean;
    function GetAWSCredentials: Boolean;
  public
    property OnLogStatusEvent: TLogStatusEventProcedure read FOnLogStatusEvent
      write FOnLogStatusEvent;
    property NumberAttachmentsToCopy: Integer read FNumberAttachmentsToCopy;
    property NumberAttachmentsCopied: Integer read FNumberAttachmentsCopied;
    procedure Execute(WOID, TicketID: Integer; IniFilename: string);
  end;

implementation

{$R *.dfm}

uses
  OdADOUtils, QMConst, OdMiscUtils, StrUtils, OdExceptions, BaseAttachmentDMu;

{ TCopyAttachmentsDM }


function TCopyAttachmentsDM.GetAWSConstants: Boolean;  //QMANTWO-556 sr
begin
   if qryAPI_storage_constants.Active then
      qryAPI_storage_constants.Close;
   qryAPI_storage_constants.Open;
   if qryAPI_storage_constants.RecordCount > 0 then begin
     myAttachmentData.Constants.Host := qryAPI_storage_constants.FieldByName('host').AsString;
     myAttachmentData.Constants.RestURL :=  qryAPI_storage_constants.FieldByName('rest_url').AsString;
     myAttachmentData.Constants.Service := qryAPI_storage_constants.FieldByName('service').AsString;
     myAttachmentData.Constants.StagingBucket := qryAPI_storage_constants.FieldByName('bucket').AsString;
     myAttachmentData.Constants.StagingFolder := qryAPI_storage_constants.FieldByName('bucket_folder').AsString;
     myAttachmentData.Constants.Region := qryAPI_storage_constants.FieldByName('region').AsString;
     myAttachmentData.Constants.Algorithm := qryAPI_storage_constants.FieldByName('algorithm').AsString;
     myAttachmentData.Constants.SignedHeaders := qryAPI_storage_constants.FieldByName('signed_headers').AsString;
     myAttachmentData.Constants.ContentType := qryAPI_storage_constants.FieldByName('content_type').AsString;
     myAttachmentData.Constants.AcceptedValues := qryAPI_storage_constants.FieldByName('accepted_values').AsString;
     if myAttachmentData.Constants.Host <> '' then
       Result := True
     else
       Result := False
   end
   else
     Result := False;
end;

function TCopyAttachmentsDM.GetAWSCredentials: Boolean;   //QMANTWO-556 sr
begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
  if qryAPI_storage_credentials.Active then
    qryAPI_storage_credentials.Close;

  qryAPI_storage_credentials.Open;
  if qryAPI_storage_credentials.RecordCount > 0 then begin
    myAttachmentData.AWSKey := qryAPI_storage_credentials.FieldByName('enc_access_key').AsWideString;
    myAttachmentData.AWSSecret := qryAPI_storage_credentials.FieldByName('enc_secret_key').AsWideString;
    if (myAttachmentData.AWSKey <> '') and (myAttachmentData.AWSSecret <> '') then
      Result := True
    else
      Result := False;
  end
  else begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
    Result := False;
  end;
end;


function TCopyAttachmentsDM.CopyAttachment(ServerAttachment: TServerAttachment;  //qmatwo-556
  var DownloadedFilename: string; var UploadFilename: string;
  TicketID: Integer): Boolean;
var
  ABaseAttachment:TBaseAttachment;
begin
  Result := False;

  LogStatusEvent('Initiating download for attachment_id ' +
    GetAttachmentsToCopy.FieldByName('attachment_id').AsString,
    [seDetailed, seStartingFileCopy]);
  LogStatusEvent('Copying ' +
    GetAttachmentsToCopy.FieldByName('orig_filename').AsString +
    ' (file ' + IntToStr(GetAttachmentsToCopy.RecNo) + ' of ' +
    IntToStr(FNumberAttachmentsToCopy) + ')', [seBrief, seStartingFileCopy]);

  DownloadedFilename := ServerAttachment.DownloadAttachment(
    GetAttachmentsToCopy.FieldByName('attachment_id').AsInteger);

  { DownloadAttachment can return a filename or an exception message. If the
    file wasn't found on the server, it will probably return a filename. }
  if not FileExists(DownloadedFilename) then begin
    if (DownloadedFilename <> '') and (ExtractFilePath(DownloadedFilename) = '')
      then
      LogStatusEvent(DownloadedFilename, [seError])
    else
      LogStatusEvent('Error: Downloading the attachment file was not ' +
        'successful. The file may not have been found on the FTP server.',
        [seError]);
    Exit;
  end;

  LogStatusEvent('Initiating preparation of downloaded file: ' +
    DownloadedFilename);

  UploadFilename := ServerAttachment.PrepareFileForAttachment(qmftTicket,
    TicketID, DownloadedFilename,
    GetAttachmentsToCopy.FieldByName('source').AsString);


  myAttachmentData.LocalFilename := DownloadedFilename;
  myAttachmentData.ServerFilename := UploadFilename;
  LogStatusEvent('Initiating upload for prepared file: ' + UploadFilename);

  ServerAttachment.AttachedByEmpID := FSystemEmpID;

  { TODO : Since we're now doing each attachment in a separate transaction,
    it's possible download_location_id could change. Should we get the
    location each time inside this transaction? Or should we upload all
    files to the same place for consistency, even if the download location
    gets changed in the middle? }


  LogStatusEvent('Initiating database update for uploaded attachment');

    myAttachmentData.AttachmentID := ServerAttachment.AttachUploadedFileToRecordAWS(qmftTicket, TicketID,
    ExtractFileName(DownloadedFilename), UploadFilename,
    GetAttachmentsToCopy.FieldByName('comment').AsString,
    CopyFromWorkOrderSource,
    GetAttachmentsToCopy.FieldByName('doc_type').AsString);

   OdUqInternet.Upload_Attachment(OdUqInternet.myAttachmentData,ABaseAttachment);
  Result := True;
end;

procedure TCopyAttachmentsDM.CleanUpFiles(DownloadedFilename,
  UploadFilename: string);
begin
  { TODO : If the file is downloaded, but with an error, DownloadedFilename may
    not contain the filename. So may need a different way to find the file and
    dir names for cleaning up. }
  try
    if DownloadedFilename <> '' then
      OdDeleteFile(ExtractFilePath(DownloadedFilename),
        ExtractFileName(DownloadedFilename), -1, False, False);
  except
    on E: Exception do
      LogStatusEvent('Error: ' + E.Message, [seDeleteError]);
  end;
  try
    if UploadFilename <> '' then
      OdDeleteFile(ExtractFilePath(UploadFilename),
        ExtractFileName(UploadFilename), -1, False, False);
  except
    on E: Exception do
      LogStatusEvent('Error: ' + E.Message, [seDeleteError]);
  end;
  if (DownloadedFilename <> '') and (GetAttachmentsToCopy.RecNo =
    GetAttachmentsToCopy.RecordCount) and
    DirectoryExists(ExtractFilePath(DownloadedFilename))then
      if not RemoveDir(ExtractFilePath(DownloadedFilename)) then
        LogStatusEvent('Error: ' + SysErrorMessage(GetLastError),
          [seDeleteError]);
end;

function TCopyAttachmentsDM.CopySingleAttachment(
  ServerAttachment: TServerAttachment; TicketID: Integer): Boolean;
var
  DownloadedFilename, UploadFilename: string;
begin
  Result := False;
  DownloadedFilename := '';
  UploadFilename := '';
  try
    DBConnection.BeginTrans;
    try
      if CopyAttachment(ServerAttachment, DownloadedFilename, UploadFilename,
        TicketID) then begin
          DBConnection.CommitTrans;
          // Return True even if CleanUpFiles fails
          Result := True;
      end
      else
        DBConnection.RollbackTrans;
    except
      // If the exception is serious, reraise it

      on E: Exception do begin
        // Cancel the copy operation for the current attachment
        if DBConnection.InTransaction then
          DBConnection.RollbackTrans;
        LogStatusEvent('Error: ' + E.Message, [seError]);
      end;
    end;
  finally
    CleanUpFiles(DownloadedFilename, UploadFilename);
  end;
end;

procedure TCopyAttachmentsDM.DBConnectionAfterConnect(Sender: TObject);
begin
  GetAWSConstants;
  GetAWSCredentials;
end;

function TCopyAttachmentsDM.GetSystemUserEmpID: Integer;
begin
  Result := 0;
  GetSystemEmpID.Open;
  try
    if GetSystemEmpID.Fields[0].IsNull then
      raise Exception.Create('Cannot get system user employee id. ' +
        'Missing the SystemUserEmpID configuration_data row.');
    Result := GetSystemEmpID.Fields[0].AsInteger;
  finally
    GetSystemEmpID.Close;
  end;
end;

procedure TCopyAttachmentsDM.Execute(WOID, TicketID: Integer;
  IniFilename: string);
var
  ServerAttachment: TServerAttachment;
begin
  Assert(FileExists(IniFilename), 'Ini file ' + IniFilename + ' does not exist');

  try
    FNumberAttachmentsToCopy := -1;
    FNumberAttachmentsCopied := 0;
    LogStatusEvent('', [seInitialized]);

    LogStatusEvent('Connecting to the database');
    ConnectAdoConnectionWithIni(DBConnection, IniFilename);

    DBConnection.BeginTrans;
    LogStatusEvent('Getting System User emp_id');
    FSystemEmpID := GetSystemUserEmpID;

    { Call stored proc to validate input params, get download location, and get
      list of attachments }
    LogStatusEvent('Querying for attachments to copy');
    GetAttachmentsToCopy.Parameters.ParamValues['@wo_id'] := WOID;
    GetAttachmentsToCopy.Parameters.ParamValues['@ticket_id'] := TicketID;
    GetAttachmentsToCopy.Parameters.ParamValues['@destination_linked_to_source'] := 0;
    GetAttachmentsToCopy.Parameters.ParamValues['@download_location_id'] := 0;
    GetAttachmentsToCopy.Open;
    DBConnection.CommitTrans;

    if not GetAttachmentsToCopy.Parameters.ParamValues[
      '@destination_linked_to_source'] then begin
      LogStatusEvent('Error: Wo_id is not associated with ticket_id',
        [seFatalError]);
      Exit;
    end;

    FNumberAttachmentsToCopy := GetAttachmentsToCopy.RecordCount;
    FNumberAttachmentsCopied := 0;

    LogStatusEvent('Found ' + IntToStr(FNumberAttachmentsToCopy) +
      ' attachments to copy');

    { Try to download, upload, then record, each attachment in the list }
    ServerAttachment := TServerAttachment.Create(DBConnection);
    try
      ServerAttachment.OnLogMessage := LogServerAttachmentMessage;
      GetAttachmentsToCopy.First;
      while not GetAttachmentsToCopy.Eof do begin
        if CopySingleAttachment(ServerAttachment, TicketID) then
          Inc(FNumberAttachmentsCopied);
        GetAttachmentsToCopy.Next;
      end;
      if FNumberAttachmentsToCopy > 0 then
        LogStatusEvent(IntToStr(FNumberAttachmentsCopied) + ' of ' +
          IntToStr(FNumberAttachmentsToCopy) +
          ' attachments successfully copied', [seDetailed, seCopySummary]);
    finally
      FreeAndNil(ServerAttachment);
    end;
  finally
    if DBConnection.InTransaction then
      DBConnection.RollbackTrans;
    DBConnection.Close;
  end;
end;

procedure TCopyAttachmentsDM.LogStatusEvent(Msg: string;
  StatusEventFlags: TStatusEventFlags=[seDetailed]);
begin
  if Assigned(FOnLogStatusEvent) then
    FOnLogStatusEvent(Self, Msg, StatusEventFlags);
end;

procedure TCopyAttachmentsDM.LogServerAttachmentMessage(Sender: TObject;
  const LogMsg: string);
begin
  LogStatusEvent(LogMsg);
end;

end.
