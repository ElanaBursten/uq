unit EngImpl;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, SysUtils, ActiveX, QMDBEng_TLB, StdVcl, ADODB;

type
  TQMDBEngine = class(TAutoObject, IQMDBEngine)
  private
    Conn: TAdoConnection;
    procedure HandleStatusMsg(Status: string);
    procedure Disconnect;
  protected
    procedure GetReportingConnection(const IniFileName: WideString;
      out Connection: OleVariant); safecall;
  public
    destructor Destroy; override;
  end;

implementation

uses ComServ, ReportConfig;

destructor TQMDBEngine.Destroy;
begin
  Disconnect;
  inherited;
end;

procedure TQMDBEngine.Disconnect;
begin
  if Assigned(Conn) then
    FreeAndNil(Conn);

end;

procedure TQMDBEngine.GetReportingConnection(const IniFileName: WideString;
  out Connection: OleVariant);
var
  Config: TReportConfig;
begin
  if Assigned(Conn) then
    raise Exception.Create('Already connected');

  Config := LoadConfig(IniFileName);
  try
    Conn := TAdoConnection.Create(nil);
    Config.ConnectReportingDb(Conn, HandleStatusMsg);
    Connection := Conn.ConnectionObject;
  finally
    Config.Free;
  end;
end;

procedure TQMDBEngine.HandleStatusMsg(Status: string);
begin

end;

initialization
  TAutoObjectFactory.Create(ComServer, TQMDBEngine, Class_QMDBEngine,
    ciMultiInstance, tmApartment);
end.
