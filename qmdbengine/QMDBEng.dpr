library QMDBEng;

{$R '..\QMVersion.res' '..\QMVersion.rc'}

uses
  ComServ,
  EngImpl in 'EngImpl.pas' {QMDBEngine: CoClass},
  ReportConfig in '..\reportengine\ReportConfig.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  UQDbConfig in '..\common\UQDbConfig.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin

  // The package list for deployment is:
  // rtl;vcl;adortl
end.
