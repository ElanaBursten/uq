unit QMDBEng_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 9/4/2007 10:31:21 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\projects\Utiliquest\qmanager\qmdbengine\QMDBEng.tlb (1)
// LIBID: {B9AB6B43-1715-455F-B329-D38A006A0DB8}
// LCID: 0
// Helpfile:
// HelpString: QMDBEng Library
// DepndLst:
//   (1) v2.0 stdole, (C:\WINDOWS\system32\STDOLE2.TLB)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  QMDBEngMajorVersion = 1;
  QMDBEngMinorVersion = 0;

  LIBID_QMDBEng: TGUID = '{B9AB6B43-1715-455F-B329-D38A006A0DB8}';

  IID_IQMDBEngine: TGUID = '{E8B82FBB-74E6-46B0-BF90-B292983B9900}';
  CLASS_QMDBEngine: TGUID = '{9A3AAA82-64A0-447D-B1F2-286D0A1954C1}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IQMDBEngine = interface;
  IQMDBEngineDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  QMDBEngine = IQMDBEngine;


// *********************************************************************//
// Interface: IQMDBEngine
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E8B82FBB-74E6-46B0-BF90-B292983B9900}
// *********************************************************************//
  IQMDBEngine = interface(IDispatch)
    ['{E8B82FBB-74E6-46B0-BF90-B292983B9900}']
    procedure GetReportingConnection(const IniFileName: WideString;
                                     out ConnectionWrapper: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  IQMDBEngineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E8B82FBB-74E6-46B0-BF90-B292983B9900}
// *********************************************************************//
  IQMDBEngineDisp = dispinterface
    ['{E8B82FBB-74E6-46B0-BF90-B292983B9900}']
    procedure GetReportingConnection(const IniFileName: WideString;
                                     out ConnectionWrapper: OleVariant); dispid 1;
  end;

// *********************************************************************//
// The Class CoQMDBEngine provides a Create and CreateRemote method to
// create instances of the default interface IQMDBEngine exposed by
// the CoClass QMDBEngine. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoQMDBEngine = class
    class function Create: IQMDBEngine;
    class function CreateRemote(const MachineName: string): IQMDBEngine;
  end;

implementation

uses ComObj;

class function CoQMDBEngine.Create: IQMDBEngine;
begin
  Result := CreateComObject(CLASS_QMDBEngine) as IQMDBEngine;
end;

class function CoQMDBEngine.CreateRemote(const MachineName: string): IQMDBEngine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_QMDBEngine) as IQMDBEngine;
end;

end.
