unit UTQUnit;

interface

uses
  DB;

 type
   TDataReturn = Class
   Private
     _IsOk: Boolean;
     _CallerProcess: String;
     _CalledProcess: String;
     _ReturnMessage: String;
     _ReturnScalar: String;
     _ReturnDS: TDataSet;
     _ReturnRows: Integer;
   public
     property IsOk: Boolean
       read _IsOk write _IsOk;
     property CallerProcess: String
     read _CallerProcess write _CallerProcess;
       property  CalledProcess: String
     read _CalledProcess  write  _CalledProcess;
     property ReturnMessage: String
       read _ReturnMessage  write _ReturnMessage;
     property ReturnScalar: String
       read _ReturnScalar write _ReturnScalar;
     property ReturnDS: TDataSet
       read _ReturnDS  write _ReturnDS;
     property ReturnRows: Integer
       read _ReturnRows write _ReturnRows;
   end;

implementation

{ TDataReturn }

end.
