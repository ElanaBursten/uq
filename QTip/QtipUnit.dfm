object frmQTipMain: TfrmQTipMain
  Left = 0
  Top = 0
  Caption = 'Q-Ticket in Polygon'
  ClientHeight = 547
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnQTip: TButton
    Left = 32
    Top = 488
    Width = 75
    Height = 25
    Caption = 'Run QTip'
    TabOrder = 0
    OnClick = btnQTipClick
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 58
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 128
      Top = 20
      Width = 35
      Height = 16
      Caption = 'Bill ID'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtBillID: TEdit
      Left = 169
      Top = 19
      Width = 168
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 58
    Width = 750
    Height = 383
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 2
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 748
      Height = 381
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      OnDblClick = Memo1DblClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 750
    Height = 19
    Panels = <
      item
        Text = 'Connected'
        Width = 70
      end
      item
        Width = 200
      end
      item
        Text = 'Version'
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object AConn: TADOConnection
    Provider = 'SQLNCLI11'
    Left = 400
    Top = 248
  end
  object GenQuery: TADOQuery
    Connection = AConn
    Parameters = <>
    Left = 416
    Top = 120
  end
  object AQuery: TADOQuery
    Connection = AConn
    Parameters = <>
    Left = 416
    Top = 176
  end
end
