unit QtipUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Inifiles, UTQUnit, ADODB, CentralOhioPolygons, StdCtrls, Data.DB, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TPolyArray = Array of Array of Double;

type
  TfrmQTipMain = class(TForm)
    AConn: TADOConnection;
    GenQuery: TADOQuery;
    AQuery: TADOQuery;
    btnQTip: TButton;
    pnlTop: TPanel;
    edtBillID: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure btnQTipClick(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
    myPath: String;
    ConnectStr: String;
    CurrentIndex: Integer;
    BillCC: Array of String;
    BillID: String;
    AreaUnknown, ColsCentral1325, ColsNE0823, ColsNW1324, ColsSE0822, Springfield0651: Array of Double;
    UnknownIDs, ColsCentral1325IDs, ColsNE0823IDs, ColsNW1324IDs, ColsSE0822IDs, Springfield0651IDs: Array of String;
    TicketTotal: Integer;
    FlatRate: Currency;
    dr: TDataReturn;
    LogFile: String;
    function CreateLogfile: String;
    procedure WriteToLog(aLogMessage: String);
    procedure ExpandArrays;
    function GetBillingDetail: TDataReturn;
    function GetFlatRate(ClientID: String): Double;
    function IsInPolygon(Lat, Long: Double; const Polygon: TPolyArray): boolean;
    function AddID(Poly: Integer; BillID: String): boolean;
    function CreateDynArray(Dim: Integer; OhioPoly: Polygon): TPolyArray;
    procedure CalcPercents;
    function ProcessBillingDetail: boolean;
    function UpdateBillingDetail(var CC, UpdtTxt, UpdtTxt2, TheList: String): boolean;
    function SetDupsAndCNCLsToNoCharge: boolean;
    function connectDB: boolean;
    function processParams: boolean;
    procedure RunQTip;
    function EnDeCrypt(const Value: String): String;
  public
    { Public declarations }
  end;

var
  frmQTipMain: TfrmQTipMain;
  UseGUI: boolean;

implementation
USES System.StrUtils, odMiscUtils;
{$R *.dfm}
{ TForm1 }

function TfrmQTipMain.CreateLogfile: String;
var
  F: TextFile;
  FN: String;
const
  BreakingLine = '//-------------------------------------------//';
begin
  if not directoryexists('C:\QM\LOGS\') then
    CreateDir('C:\QM\LOGS\');
  FN := 'C:\QM\LOGS\QTiP-' + formatdatetime('dd-mm-yy', Now) + '.txt';
  AssignFile(F, FN);
  if not FileExists(FN) then
  begin
    Rewrite(F);
    Append(F);
    WriteLn(F, BreakingLine);
    WriteLn(F, 'This Logfile was created on ' + DateTimeToStr(Now));
    WriteLn(F, BreakingLine);
    WriteLn(F, '');
    CloseFile(F);
  end;
  Result := FN;
end;

procedure TfrmQTipMain.WriteToLog(aLogMessage: String);
var
  F: TextFile;
begin
  AssignFile(F, LogFile);
  Append(F);
  WriteLn(F, DateTimeToStr(Now) + ': ' + aLogMessage);
  CloseFile(F);
end;

Function TfrmQTipMain.CreateDynArray(Dim: Integer; OhioPoly: Polygon): TPolyArray;
var
  x, y: Integer;
  Matrix: TPolyArray;
begin
  // Initialise 'Matrix' from Constant Array:
  SetLength(Matrix, Dim, 2);
  for x := 0 to Pred(Dim) do
  begin
    for y := 0 to 1 do
    begin
      Case Ord(OhioPoly) of
        0:
          Matrix[x, y] := ColsNW1324Polygon[x, y];
        1:
          Matrix[x, y] := Springfield0651Polygon[x, y];
        2:
          Matrix[x, y] := ColsNE0823Polygon[x, y];
        3:
          Matrix[x, y] := ColsCentral1325Polygon[x, y];
        4:
          Matrix[x, y] := ColsSE0822Polygon[x, y];
      end;
    end;
  end;
  Result := Matrix;
end;

procedure TfrmQTipMain.RunQTip;
var
  Index7, Index9: Integer;
  DArrColsCentral1325, DArrColsNE0823, DarrColsNW1324, DArrColsSE0822, DArrSpringfield0651: TPolyArray;
begin
  CurrentIndex := 0;
  try

    dr := TDataReturn.Create;
    dr.ReturnDS := AQuery;
    begin
      WriteToLog('Building Query for Application.');
      If BillID = '' then
        WriteToLog('No Parameters were passed in. Will use the top most Billing ID.')
      else
        WriteToLog('Using Billing ID ' + BillID + ' that was passed in as a Parameter.');
      dr := GetBillingDetail;
      if not dr.IsOk then
        WriteToLog('Query Failed. Error ' + dr.ReturnMessage)
      else
      begin
        TicketTotal := dr.ReturnRows;
        WriteToLog('Query Executed Successful.  Returned ' + InttoStr(dr.ReturnRows) + ' rows for Ohio.');
        Memo1.Lines.Add('Query: ' + InttoStr(dr.ReturnRows) + ' rows for Ohio.');
        If TicketTotal > 0 then
        begin
          ExpandArrays;
          dr.ReturnDS.First;
          BillID := dr.ReturnDS.FieldByName('bill_id').AsString;
          WriteToLog('Bill ID is ' + BillID);
          Memo1.Lines.Add('Bill ID: ' + BillID);
          FlatRate := GetFlatRate(dr.ReturnDS.FieldByName('client_id').AsString);
          WriteToLog('Flat Rate is ' + CurrToStrF(FlatRate, ffCurrency, 2));
          Memo1.Lines.Add('Flat Rate: ' + CurrToStrF(FlatRate, ffCurrency, 2));
          BillCC[CurrentIndex] := dr.ReturnDS.FieldByName('billing_cc').AsString;
          WriteToLog('Current Index = ' + InttoStr(CurrentIndex));
          Memo1.Lines.Add('Current Index: ' + InttoStr(CurrentIndex));
          WriteToLog('Current Billing CC = ' + BillCC[CurrentIndex]);
          Memo1.Lines.Add('Current Billing CC: ' + BillCC[CurrentIndex]);
          WriteToLog('Scanning Records');
          DArrColsCentral1325 := CreateDynArray(idx_ColsCentral1325, pColsCentral1325);
          DArrColsNE0823 := CreateDynArray(idx_ColsNE0823, pColsNE0823);
          DarrColsNW1324 := CreateDynArray(idx_ColsNW1324, pColsNW1234);
          DArrColsSE0822 := CreateDynArray(idx_ColsSE0822, pColsSE0822);
          DArrSpringfield0651 := CreateDynArray(idx_Springfield0651, pSpringfield0651);
          while not dr.ReturnDS.Eof do
          begin
            if BillCC[CurrentIndex] <> dr.ReturnDS.FieldByName('billing_cc').AsString then
            begin
              Inc(CurrentIndex);
              ExpandArrays;
              BillCC[CurrentIndex] := dr.ReturnDS.FieldByName('billing_cc').AsString;
              WriteToLog('Current Index = ' + InttoStr(CurrentIndex));
              Memo1.Lines.Add('Current Index: ' + InttoStr(CurrentIndex));
              WriteToLog('Current Billing CC = ' + BillCC[CurrentIndex]);
              Memo1.Lines.Add('Current Billing CC: ' + BillCC[CurrentIndex]);
            end;
            If IsInPolygon(dr.ReturnDS.FieldByName('work_lat').AsFloat, dr.ReturnDS.FieldByName('work_long').AsFloat,
              DArrColsCentral1325) then
            // Poly=1 CentralOhioPolygons.ColsCentral1325Polygon
            begin
              AddID(1, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              ColsCentral1325[CurrentIndex] := ColsCentral1325[CurrentIndex] + 1;
            end
            else If IsInPolygon(dr.ReturnDS.FieldByName('work_lat').AsFloat, dr.ReturnDS.FieldByName('work_long').AsFloat,
              DArrColsNE0823) then
            // Poly=2 CentralOhioPolygons.ColsNE0823Polygon
            begin
              AddID(2, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              ColsNE0823[CurrentIndex] := ColsNE0823[CurrentIndex] + 1;
            end
            else If IsInPolygon(dr.ReturnDS.FieldByName('work_lat').AsFloat, dr.ReturnDS.FieldByName('work_long').AsFloat,
              DarrColsNW1324) then
            // Poly=3 CentralOhioPolygons.ColsNW1324Polygon
            begin
              AddID(3, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              ColsNW1324[CurrentIndex] := ColsNW1324[CurrentIndex] + 1;
            end
            else If IsInPolygon(dr.ReturnDS.FieldByName('work_lat').AsFloat, dr.ReturnDS.FieldByName('work_long').AsFloat,
              DArrColsSE0822) then
            // Poly=4 CentralOhioPolygons.ColsSE0822Polygon
            begin
              AddID(4, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              ColsSE0822[CurrentIndex] := ColsSE0822[CurrentIndex] + 1;
            end
            else if IsInPolygon(dr.ReturnDS.FieldByName('work_lat').AsFloat, dr.ReturnDS.FieldByName('work_long').AsFloat,
              DArrSpringfield0651) then
            // Poly=5 CentralOhioPolygons.Springfield0651Polygon
            begin
              AddID(5, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              Springfield0651[CurrentIndex] := Springfield0651[CurrentIndex] + 1;
            end
            else
            begin
              // Poly=0 UnKnown Area
              WriteToLog('Unknown found - Billing Detail ID = ' + dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              Memo1.Lines.Add('Unknown found - Billing Detail ID = ' + dr.ReturnDS.FieldByName('billing_detail_id').AsString);
              WriteToLog('Lat=' + dr.ReturnDS.FieldByName('work_lat').AsString + ' Long=' + dr.ReturnDS.FieldByName('work_long')
                .AsString);
              // AddID(0, dr.ReturnDS.fieldbyname('billing_detail_id').AsString));
              Memo1.Lines.Add('Lat=' + dr.ReturnDS.FieldByName('work_lat').AsString + ' Long=' +
                dr.ReturnDS.FieldByName('work_long').AsString);
              // AreaUnknown(CurrentIndex) = AreaUnknown(CurrentIndex] +1
              If BillCC[CurrentIndex] = 'CGR' then
              begin
                WriteToLog('CC = CGR.  Adding Unknown Location to the default for CGR Springfield 0651');
                Memo1.Lines.Add('CC = CGR.  Adding Unknown Location to the default for CGR Springfield 0651');
                AddID(5, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
                Springfield0651[CurrentIndex] := Springfield0651[CurrentIndex] + 1;
              end
              else
              begin
                WriteToLog('CC <> CGR.  Adding Unknown Location to default for non CGR ColsCentral 1325');
                Memo1.Lines.Add('CC <> CGR.  Adding Unknown Location to default for non CGR ColsCentral 1325');
                AddID(1, dr.ReturnDS.FieldByName('billing_detail_id').AsString);
                ColsCentral1325[CurrentIndex] := ColsCentral1325[CurrentIndex] + 1;
              end;
            end;
            dr.ReturnDS.Next;
          end; // While not Eof
          SetLength(DArrColsCentral1325, 0);
          DArrColsCentral1325 := nil;
          SetLength(DArrColsNE0823, 0);
          DArrColsNE0823 := nil;
          SetLength(DarrColsNW1324, 0);
          DarrColsNW1324 := nil;
          SetLength(DArrColsSE0822, 0);
          DArrColsSE0822 := nil;
          SetLength(DArrSpringfield0651, 0);
          DArrSpringfield0651 := nil;

          // Remove for Production
          For Index7 := 0 To Length(BillCC) - 1 do
          begin
            WriteToLog('Total Tickets in Billing Run ' + InttoStr(TicketTotal));
            Memo1.Lines.Add('Total Tickets in Billing Run ' + InttoStr(TicketTotal));
            WriteToLog('BillCC = ' + BillCC[Index7]);
            Memo1.Lines.Add('BillCC: ' + BillCC[Index7]);
            WriteToLog('Number found in ColsCentral1325Polygon = ' + FloattoStr(ColsCentral1325[Index7]));
            Memo1.Lines.Add('ColsCentral1235Polygon Number: ' + FloattoStr(ColsCentral1325[Index7]));
            WriteToLog('Number found in ColsNE0823Polygon = ' + FloattoStr(ColsNE0823[Index7]));
            Memo1.Lines.Add('ColsNE0823Polygon Number: ' + FloattoStr(ColsNE0823[Index7]));
            WriteToLog('Number found in ColsNW1324Polygon = ' + FloattoStr(ColsNW1324[Index7]));
            Memo1.Lines.Add('ColsNW1324Polygon Number: ' + FloattoStr(ColsNW1324[Index7]));
            WriteToLog('Number found in ColsSE0822Polygon = ' + FloattoStr(ColsSE0822[Index7]));
            Memo1.Lines.Add('ColsSE0822Polygon Number: ' + FloattoStr(ColsSE0822[Index7]));
            WriteToLog('Number found in Springfield0651Polygon = ' + FloattoStr(Springfield0651[Index7]));
            Memo1.Lines.Add('Springfield0651Polygon Number: ' + FloattoStr(Springfield0651[Index7]));
            WriteToLog('Number not found = ' + FloattoStr(AreaUnknown[Index7]));
            Memo1.Lines.Add('not found Number: ' + FloattoStr(AreaUnknown[Index7]));
          end;
          CalcPercents;
          For Index9 := 0 To Length(BillCC) - 1 do
          begin
            WriteToLog('Ticket Percentages');
            Memo1.Lines.Add('Ticket Percentages');
            WriteToLog('BillCC = ' + BillCC[Index9]);
            Memo1.Lines.Add('BillCC: ' + BillCC[Index9]);
            WriteToLog('Percent of ColsCentral1325Polygon = ' + formatfloat('0.00%', ColsCentral1325[Index9] * 100));
            Memo1.Lines.Add('ColsCentral1325Polygon Percent: ' + formatfloat('0.00%', ColsCentral1325[Index9] * 100));
            WriteToLog('Percent of ColsNE0823Polygon = ' + formatfloat('0.00%', ColsNE0823[Index9] * 100));
            Memo1.Lines.Add('ColsNE0823Polygon Percent: ' + formatfloat('0.00%', ColsNE0823[Index9] * 100));
            WriteToLog('Percent of ColsNW1324Polygon = ' + formatfloat('0.00%', ColsNW1324[Index9] * 100));
            Memo1.Lines.Add('ColsNW1324Polygon Percent: ' + formatfloat('0.00%', ColsNW1324[Index9] * 100));
            WriteToLog('Percent of ColsSE0822Polygon = ' + formatfloat('0.00%', ColsSE0822[Index9] * 100));
            Memo1.Lines.Add('ColsSE0822Polygon: ' + formatfloat('0.00%', ColsSE0822[Index9] * 100));
            WriteToLog('Percent of Springfield0651Polygon = ' + formatfloat('0.00%', Springfield0651[Index9] * 100));
            Memo1.Lines.Add('Percent of Springfield0651Polygon = ' + formatfloat('0.00%', Springfield0651[Index9] * 100));
            WriteToLog('Percent of not found = ' + formatfloat('0.00%', AreaUnknown[Index9] * 100));
            Memo1.Lines.Add('not found Percent: ' + formatfloat('0.00%', AreaUnknown[Index9] * 100));
          end;
          // End of Block to Remove
          ProcessBillingDetail;
        end; // TicketTotal>0
      end; // dr.IsOk
      WriteToLog('QTiP Execution Complete.');
    end; // ConnectStr<>''
  finally
    freeandnil(dr);
    AConn.Close;
  end;
end;

procedure TfrmQTipMain.FormCreate(Sender: TObject);
var
  VersionString:string;
begin
  if GetFileVersionNumberString(Application.ExeName, VersionString) then
  StatusBar1.Panels[3].Text := VersionString;

  LogFile := CreateLogfile;
  WriteToLog('Starting QTiP...');
  WriteToLog('');
  If processParams and connectDB then
  begin
    if not UseGUI then
    begin
      RunQTip;
      application.Terminate;
    end;
  end
  else application.Terminate;
end;

function TfrmQTipMain.ProcessBillingDetail: boolean;
var
  MyText: String;
  MyText2: String;
  TempList: String;
  CurrentCC: String;
  Index, Index2: Integer;
  bResult: boolean;
  idx : integer;
begin
  try
    idx  := Length(BillCC);
    for Index := 0 To idx -1 do
    begin
      CurrentCC := BillCC[Index];
      If (AreaUnknown[Index] <> 0) and (Length(UnknownIDs) > 0) then
      begin
        MyText := CurrentCC + '-Unknown Area ' + formatfloat('0.00%', AreaUnknown[Index] * 100) + ' {' +
          CurrToStrF(AreaUnknown[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-Unknown Area';
        TempList := UnknownIDs[0];
        for Index2 := 1 to Length(UnknownIDs) - 1 do
          TempList := TempList + ',' + UnknownIDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
      If ColsCentral1325[Index] <> 0 then
      begin
        MyText := CurrentCC + '-1325-Cols Central ' + formatfloat('0.00%', ColsCentral1325[Index] * 100) + ' {' +
          CurrToStrF(ColsCentral1325[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-1325-Cols Central';
        TempList := ColsCentral1325IDs[0];
        for Index2 := 1 to Length(ColsCentral1325IDs) - 1 do
          TempList := TempList + ',' + ColsCentral1325IDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
      If ColsNE0823[Index] <> 0 then
      begin
        MyText := CurrentCC + '-0823-Cols NE ' + formatfloat('0.00%', ColsNE0823[Index] * 100) + ' {' +
          CurrToStrF(ColsNE0823[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-0823-Cols NE';
        TempList := ColsNE0823IDs[0];
        for Index2 := 1 to Length(ColsNE0823IDs) - 1 do
          TempList := TempList + ',' + ColsNE0823IDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
      If ColsNW1324[Index] <> 0 then
      begin
        MyText := CurrentCC + '-1324-Cols NW ' + formatfloat('0.00%', ColsNW1324[Index] * 100) + ' {' +
          CurrToStrF(ColsNW1324[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-1324-Cols NW';
        TempList := ColsNW1324IDs[0];
        for Index2 := 1 to Length(ColsNW1324IDs) - 1 do
          TempList := TempList + ',' + ColsNW1324IDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
      If ColsSE0822[Index] <> 0 then
      begin
        MyText := CurrentCC + '-0822-Cols SE ' + formatfloat('0.00%', ColsSE0822[Index] * 100) + ' {' +
          CurrToStrF(ColsSE0822[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-0822-Cols SE';
        TempList := ColsSE0822IDs[0];
        for Index2 := 1 to Length(ColsSE0822IDs) - 1 do
          TempList := TempList + ',' + ColsSE0822IDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
      If Springfield0651[Index] <> 0 then
      begin
        MyText := CurrentCC + '-0651-Springfield ' + formatfloat('0.00%', Springfield0651[Index] * 100) + ' {' +
          CurrToStrF(Springfield0651[Index] * FlatRate, ffCurrency, 2) + '}';
        MyText2 := CurrentCC + '-0651-Springfield';
        TempList := Springfield0651IDs[0];
        for Index2 := 1 to Length(Springfield0651IDs) - 1 do
          TempList := TempList + ',' + Springfield0651IDs[Index2];
        bResult := UpdateBillingDetail(CurrentCC, MyText, MyText2, TempList);
      end;
    end;
  finally
    Result := bResult;
  end;
end;

function TfrmQTipMain.UpdateBillingDetail(var CC: String; var UpdtTxt: String; var UpdtTxt2: String; var TheList: String): boolean;
var
  qString: String;
  QryResult: Integer;
begin
  qString := 'Update billing_detail ';
  // ''''''''''''''''''''''''''''''''''''''''''''''''''After Hours - CGE-1324-Cols NW
  qString := qString + 'Set line_item_text = CASE WHEN UPPER(SUBSTRING(line_item_text,1,11)) = ' +
    '''AFTER HOURS'' THEN (''After Hours - ' + UpdtTxt2 + ''') ELSE ''' + UpdtTxt + ''' END ';
  qString := qString + 'WHERE bill_id = ' + BillID + ' and billing_cc = ''' + CC + ''' and billing_detail_id in (' + TheList + ')';
  try
    GenQuery.SQL.Text := qString;
    QryResult := GenQuery.ExecSQL;
    Result := true;
    WriteToLog('Updated ' + InttoStr(QryResult) + ' records.');
    Memo1.Lines.Add('Updated ' + InttoStr(QryResult) + ' records.');
  except
    On E: Exception do
    begin
      Result := false;
      WriteToLog('Error encountered trying to update with ' + qString);
      WriteToLog('Error: ' + E.Message);
    end;
  end;
end;

procedure TfrmQTipMain.btnQTipClick(Sender: TObject);
begin
  BillID := edtBillID.text;
  RunQTip;
end;

procedure TfrmQTipMain.CalcPercents;
var
  Index: Integer;
begin
  for Index := 0 To Length(BillCC) - 1 do
  begin
    If AreaUnknown[Index] <> 0 then
      AreaUnknown[Index] := AreaUnknown[Index] / TicketTotal;
    If ColsCentral1325[Index] <> 0 then
      ColsCentral1325[Index] := ColsCentral1325[Index] / TicketTotal;
    If ColsNE0823[Index] <> 0 then
      ColsNE0823[Index] := ColsNE0823[Index] / TicketTotal;
    If ColsNW1324[Index] <> 0 then
      ColsNW1324[Index] := ColsNW1324[Index] / TicketTotal;
    If ColsSE0822[Index] <> 0 then
      ColsSE0822[Index] := ColsSE0822[Index] / TicketTotal;
    If Springfield0651[Index] <> 0 then
      Springfield0651[Index] := Springfield0651[Index] / TicketTotal;
  end;
end;

function TfrmQTipMain.AddID(Poly: Integer; BillID: String): boolean;
var
  Good: boolean;
begin
  Good := true;
//  BillID:='60202';
  case Poly of
    0: // Unknown ID's
      begin
        SetLength(UnknownIDs, Length(UnknownIDs) + 1);
        UnknownIDs[Length(UnknownIDs) - 1] := BillID;
      end;
    1: // ColsCentral1325IDs
      begin
        SetLength(ColsCentral1325IDs, Length(ColsCentral1325IDs) + 1);
        ColsCentral1325IDs[Length(ColsCentral1325IDs) - 1] := BillID;
      end;
    2: // ColsNE0823IDs
      begin
        SetLength(ColsNE0823IDs, Length(ColsNE0823IDs) + 1);
        ColsNE0823IDs[Length(ColsNE0823IDs) - 1] := BillID;
      end;
    3: // ColsNW1324IDs
      begin
        SetLength(ColsNW1324IDs, Length(ColsNW1324IDs) + 1);
        ColsNW1324IDs[Length(ColsNW1324IDs) - 1] := BillID;
      end;
    4: // ColsSE0822IDs
      begin
        SetLength(ColsSE0822IDs, Length(ColsSE0822IDs) + 1);
        ColsSE0822IDs[Length(ColsSE0822IDs) - 1] := BillID;
      end;
    5: // Springfield0651IDs
      begin
        SetLength(Springfield0651IDs, Length(Springfield0651IDs) + 1);
        Springfield0651IDs[Length(Springfield0651IDs) - 1] := BillID;
      end;
  else
    begin
      WriteToLog('Error. Bad Polygon value sent to function.');
      Good := false;
    end;
  end;
  Result := Good;
end;

function TfrmQTipMain.SetDupsAndCNCLsToNoCharge: boolean;
var
  qString: String;
  QryResult: Integer;
begin
  Result := true;
  qString := 'SELECT billing_detail_id ';
  qString := qString + 'INTO #TempT ';
  qString := qString + 'FROM billing_detail ';
  qString := qString + 'WHERE bill_id = ' + BillID + ' and [status] <> ''ZZZ'' and ';
  qString := qString + 'work_state = ''OH'' and ((billing_cc = ''CGE'') or ' +
    '(billing_cc = ''CGR'')) and ticket_type NOT LIKE ''%CNCL%'' ';
  // qstring := qstring + 'Group by ticket_number ';
  qString := qString + '; ';
  qString := qString + 'Update billing_detail ';
  qString := qString + 'Set line_item_text = ''No Charge'' ';
  qString := qString + 'WHERE bill_id = ' + BillID + ' and ' + '((billing_cc = ''CGE'') or (billing_cc = ''CGR'')) and  ' +
    'billing_detail_id not in (SELECT billing_detail_id from #TempT)';
  try
    GenQuery.SQL.Text := qString;
    QryResult := GenQuery.ExecSQL;
    // WriteToLog('Updated ' + InttoStr(QryResult) + ' ZZZ/CNCL records with query: ' + qString);
    WriteToLog('Updated ' + InttoStr(QryResult) + ' ZZZ/CNCL records.');
    Memo1.Lines.Add('Updated ' + InttoStr(QryResult) + ' ZZZ/CNCL records.');
  except
    On E: Exception do
    begin
      WriteToLog('Error encountered trying to update with ' + qString);
      WriteToLog('Error: ' + E.Message);
      Result := false;
    end;
  end;
end;

function TfrmQTipMain.IsInPolygon(Lat: Double; Long: Double; const Polygon: TPolyArray): boolean;
var
  I: Integer;
  J: Integer;
  oddNodes: boolean;
begin
  J := Length(Polygon) - 2;
  oddNodes := false;
{$B-}
  for I := 0 to Length(Polygon) - 2 do
  begin
    If ((Polygon[I, 0] < Lat) and (Polygon[J, 0] >= Lat) or (Polygon[J, 0] < Lat) and (Polygon[I, 0] >= Lat)) and
      ((Polygon[I, 1] <= Long) or (Polygon[J, 1] <= Long)) then
    begin
      If (Polygon[I, 1] + (Lat - Polygon[I, 0]) / (Polygon[J, 0] - Polygon[I, 0]) * (Polygon[J, 1] - Polygon[I, 1]) < Long) then
        oddNodes := not oddNodes
    end;
    J := I;
  end;
  Result := oddNodes;
end;

procedure TfrmQTipMain.Memo1DblClick(Sender: TObject);
begin
  memo1.Clear;
end;

function TfrmQTipMain.GetFlatRate(ClientID: String): Double;
var
  qString: String;
  TheFee: Double;
begin
  qString := 'select flat_fee from billing_output_config where customer_id = ' +
    '(select customer_id from client where client_id = ' + ClientID + ')';
  try
    try
      GenQuery.SQL.Text := qString;
      GenQuery.Open;
      TheFee := GenQuery.FieldByName('flat_fee').AsFloat;
    except
      TheFee := 0;
    end;
  finally
    GenQuery.Close;
    Result := TheFee;
  end;
end;

Procedure TfrmQTipMain.ExpandArrays;
begin
  SetLength(BillCC, CurrentIndex + 1);
  SetLength(AreaUnknown, CurrentIndex + 1);
  SetLength(ColsCentral1325, CurrentIndex + 1);
  SetLength(ColsNE0823, CurrentIndex + 1);
  SetLength(ColsNW1324, CurrentIndex + 1);
  SetLength(ColsSE0822, CurrentIndex + 1);
  SetLength(Springfield0651, CurrentIndex + 1);
  AreaUnknown[CurrentIndex] := 0;
  ColsCentral1325[CurrentIndex] := 0;
  ColsNE0823[CurrentIndex] := 0;
  ColsNW1324[CurrentIndex] := 0;
  ColsSE0822[CurrentIndex] := 0;
  Springfield0651[CurrentIndex] := 0;
end;

function TfrmQTipMain.GetBillingDetail: TDataReturn;
var
  qString: String;
begin
  // qString = 'SELECT max(billing_detail_id) as billing_detail_id, ticket_number '
  qString := 'SELECT billing_detail_id INTO #Temp1 FROM billing_detail ';
  If BillID = '' then
    qString := qString + 'WHERE bill_id = (Select max(bill_id) from billing_header) ' + 'and [status] <> ''ZZZ'' and '
  else
    qString := qString + 'WHERE bill_id = ' + BillID + ' and [status] <> ''ZZZ'' and ';

  qString := qString + 'work_state = ''OH'' and ((billing_cc = ''CGE'') ' +
    'or (billing_cc = ''CGR'')) and ticket_type NOT LIKE ''%CNCL%'';';
  // qString = qstring + 'Group by ticket_number'
  qString := qString + 'SELECT billing_detail_id, bill_id, client_id, billing_cc, ticket_number, ' +
    'work_state, ticket_type, [status], work_lat, work_long, line_item_text ';
  qString := qString + 'INTO #TBilling FROM billing_detail ';
  qString := qString + 'WHERE billing_detail_id in (Select billing_detail_id from #Temp1); ';
  qString := qString + 'SELECT * FROM #TBilling ';
  qString := qString + 'order by billing_cc';

  // Remove Prior to Production
  WriteToLog('Sending Query Searching for Ohio Tickets in the Billing Detail Table.');
  WriteToLog('Query = ' + qString);
  TADOQuery(dr.ReturnDS).SQL.Text := qString;

  // End Block to Remove

  try
    dr.ReturnDS.Open;
    dr.IsOk := true;
    dr.ReturnRows := dr.ReturnDS.RecordCount;
    Result := dr;
  except
    On E: Exception do
    begin
      dr.IsOk := false;
      dr.ReturnMessage := 'Error encountered running Billing Detail Query - ' + E.Message;
    end;
  end;
end;

function TfrmQTipMain.connectDB: boolean;
var
  connStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  BillingIni: TIniFile;
  const
  DECRYPT = 'DEC_';
begin
  Result := false;
  WriteToLog('Searching running directory for QManagerBilling.ini for Database Values');
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');
    aServer := BillingIni.ReadString('Database', 'Server', '');
    aDatabase := BillingIni.ReadString('Database', 'DB', 'QM');
    ausername := BillingIni.ReadString('Database', 'UserName', '');
    apassword := BillingIni.ReadString('Database', 'Password', '');
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
      WriteToLog('You are using a Clear Text Password.  Please contact IT for the correct Password');
      result := false;
      Exit;

    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
    WriteToLog('Built Connection String for Server-' + aServer + ' Database-' + aDatabase);

    AConn.ConnectionString := connStr;
    WriteToLog('Connection string '+AConn.ConnectionString);
    try
      AConn.Open;
    except on E: Exception do
      writeToLog(e.Message);
    end;
    Result := AConn.Connected;
    if result then
      StatusBar1.Panels[1].Text :=  aServer;

  finally
    freeandnil(BillingIni);
  end;
end;

function TfrmQTipMain.processParams: boolean;
var
  I: Integer;
begin
  for I := 0 to ParamCount do
  begin
    case I of
      0:
        myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
      1:begin
        BillID := paramstr(1);
        WriteToLog('BillID '+BillID+' passed in to process');
      end;
    end;
  end;
  Result := true;
end;


function TfrmQTipMain.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

end.
