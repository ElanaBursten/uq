program Qtip;
 {$R '..\QMIcon.res'}
 {$R 'QMVersion.res' '..\QMVersion.rc'}
uses
  Forms,
  QtipUnit in 'QtipUnit.pas' {frmQTipMain},
  CentralOhioPolygons in 'CentralOhioPolygons.pas';


begin
  Application.Initialize;
  QtipUnit.UseGUI := ((ParamStr(2) = 'GUI') or (ParamCount=0));
  Application.ShowMainForm := QtipUnit.UseGUI;
  Application.MainFormOnTaskBar := QtipUnit.UseGUI;
  Application.CreateForm(TfrmQTipMain, frmQTipMain);
  Application.Run;

  ExitCode := 69;
end.
