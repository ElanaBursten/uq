
USE [QM]
GO
CREATE NONCLUSTERED INDEX [IX_locate_ticket_id_status]
ON [dbo].[locate] ([ticket_id],[status])
INCLUDE ([client_id])
GO

