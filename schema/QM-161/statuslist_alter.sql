/*
   Wednesday, April 22, 202012:11:57 PM
   User:QM-161 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.statuslist ADD
	note_required bit NOT NULL CONSTRAINT DF_statuslist_note_required DEFAULT 0
GO
ALTER TABLE dbo.statuslist SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.statuslist', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.statuslist', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.statuslist', 'Object', 'CONTROL') as Contr_Per 