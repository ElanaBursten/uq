------------------------------------------------
-- QM-585 Employee PLUS WhiteList
-- WHITELIST table for employees allowed to work on certain utilities
-- EB Created 6/2022
------------------------------------------------

if not exists (select * from information_schema.tables where table_name = 'employee_plus')
CREATE TABLE employee_plus(
	emp_id int NOT NULL,
	term_group_id int NOT NULL,
	active bit NOT NULL,
	modified_date datetime NOT NULL,
	modified_by_id int NOT NULL,
	util varchar(15) NULL
)
GO

--rollback
--DROP TABLE employee_plus


