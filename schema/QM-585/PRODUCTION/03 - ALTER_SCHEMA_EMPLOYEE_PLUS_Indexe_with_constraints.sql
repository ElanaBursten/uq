------------------------------------------------
-- QM-585 Employee PLUS WhiteList
-- Employee_Plus TABLE for employees allowed to work on certain utilities
-- If an employee is turned off, active flag is reset to 0 (FALSE)
-- EB Created 6/2022
------------------------------------------------
ALTER TABLE dbo.employee_plus
  ADD CONSTRAINT ep_emp_id_term_group_id UNIQUE(emp_id, term_group_id)

  Go

CREATE NONCLUSTERED INDEX [employee_plus_emp_id] ON [dbo].[employee_plus]
(
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
GO

CREATE NONCLUSTERED INDEX [employee_plus_term_group_id] ON [dbo].[employee_plus]
(
	[term_group_id] ASC,
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
GO