-----------------------------------------------
-- split_field function - splits delimited field into TWO columns
-- QM-585 Employee Plus Whitelist
-- Only handles first two columns (vertical addition of columns).
--    Note: to split into separate columns, use split_del_string instead
-----------------------------------------------
if object_id('dbo.split_field') is not null
  drop function dbo.split_field
go

CREATE FUNCTION dbo.split_field
(
    @in_string VARCHAR(MAX),
    @delimeter VARCHAR(1)
)

RETURNS @list TABLE(
                    col1 varchar(500),
					col2 varchar(500),
					pos1 int
                   )
AS
BEGIN
  DECLARE @F1 varchar(500)
  DECLARE @f2 varchar(500)
  DECLARE @aPos integer

        --WHILE LEN(@in_string) > 0
        BEGIN
		  --Default values if no delimeter found
		  SET @in_string = trim(@In_string)
		  SET @f1 = @in_string
		  SET @f2 = ''

		  SET @aPos = CHARINDEX(@delimeter, @in_string)
		  If @aPos > 0 BEGIn		    
		    SET @F1 = trim(left(@in_string, @aPos-1))
			SET @aPos = len(@in_string) - @aPos
		    SET @F2 = trim(right(@in_string,@aPos))
		  END
            INSERT INTO @list(col1, col2, pos1)
              Values(@F1, @f2,@aPos)
        end
    RETURN 
END

--select * from split_del_string('omg|dhghf|thruyght|thrh|', '|')
--select * from split_field('Whitelist for Baltimore |Gas Limited BGE','|')

