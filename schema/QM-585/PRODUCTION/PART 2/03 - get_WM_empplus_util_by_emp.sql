if object_id('dbo.get_WM_empplus_util_by_emp') is not null
  drop procedure dbo.get_WM_empplus_util_by_emp
go

create procedure dbo.get_WM_empplus_util_by_emp(@EmpID Int)
as
Begin
   select @EmpID emp_id, 
          ep.util,
          tg.term_group_id as group_id,
		  tg.group_code, --tg.comment, 
		  col1 as long_name, 
		  col2 as short_name 
	from term_group tg
		inner join employee_PLUS ep  
		  on (ep.term_group_id = tg.term_group_id) 
		  and (ep.emp_id = @EmpID)
   CROSS APPLY split_field(tg.comment,'|')
    where (upper(tg.group_code) like 'PLUS-%') and
          (tg.active=1) and  --term is active
		  (ep.active = 1)
    order by tg.group_code 
	
END

--select * from employee_plus

--get_WM_empplus_util_by_emp 2694



--select * from employee where emp_id in
--(3512,
--11458,
--216,
--254,
--1191,
--1246,
--1191,
--10282,
--2694)


--update term_group 
----  set comment=comment + '|' + 'WGL'
--  set comment='Whitelist for Baltimore Gas|BGE'
--where (upper(group_code) like 'PLUS-%') and term_group_id=9213
--select * from term_group tg where (upper(tg.group_code) like 'PLUS-%')

--Washington: WGL
--Baltimore: BGE
--Scana: SSG
--Columbia: CGV

         