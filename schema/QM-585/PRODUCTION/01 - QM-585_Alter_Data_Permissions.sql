------------------------------------------------
-- QM-585 Employee PLUS WhiteList
-- Permissions to set Employee_Plus white list
-- Modifier: list of term code groups to assign
-- EB Created 6/2022
------------------------------------------------

if not exists(select * from right_definition where entity_data='EmployeePlusWhitelist')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Employee Plus Whitelist', 
						'General', 
						'EmployeePlusWhitelist', 
						'Limitation is comma-searated list of Term_group_ids', 
						GetDate());
GO

--select * from right_definition
--where entity_data = 'EmployeePlusWhitelist'
--order by right_description

--This permission will enable user to whitelist a locator for restricted client codes (terms)