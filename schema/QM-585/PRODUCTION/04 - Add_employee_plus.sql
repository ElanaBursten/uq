------------------------------------------------
-- QM-585 Employee PLUS WhiteList: add_employee_PLUS
-- Adds the employee to a Whitelist.  If the record exists, it simply updates it
-- If an employee is turned off, active flag is reset to 1 (active)
-- EB Created 6/2022
------------------------------------------------
-- 07/16/2023 Only update for inactive records
if object_id('dbo.add_employee_PLUS') is not null
  drop procedure dbo.add_employee_PLUS
GO

CREATE procedure [dbo].[add_employee_PLUS] (
@locator_id int, 
@plus_id int,
@modified_by_id int,
@util varchar(15)='')
as 
 declare @ActiveRecordExists bit;

  Select emp_id, active as isactive
  from employee_plus
  where (emp_id = @locator_id) and (term_group_id = @plus_id)

  If @@ROWCOUNT > 0
    update employee_plus
	set active=1, modified_date=GetDate(), modified_by_id = @modified_by_id
	where (emp_id = @locator_id) and (term_group_id = @plus_id) and (active=0)
  else
    INSERT INTO employee_plus
           (emp_id
           ,term_group_id
           ,active
           ,modified_date
           ,modified_by_id
           ,util)
     VALUES
           (@locator_id
           ,@plus_id
           ,1
           ,GetDate()
           ,@modified_by_id
           ,@util)

GO

--select * from employee where active=1 and emp_id = '11458'
--select * from employee_plus
--   add_employee_PLUS 15897, 9209, 15897, 'GAS1'


