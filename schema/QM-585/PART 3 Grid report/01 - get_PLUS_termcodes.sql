------------------------------------------------
-- QM-585 Employee PLUS WhiteList: get_PLUS_termcodes
-- Returns list of WhiteLists for Manager Permissions
-- Note: it is combined this way for easy retrieval but may be refactored at later date
-- EB Created 6/2022
------------------------------------------------

if object_id('dbo.get_PLUS_termcodes') is not null
  drop function dbo.get_PLUS_termcodes
go


create function dbo.get_PLUS_termcodes(@MgrID integer)
returns varchar(8000)
as  
begin
  declare @empPlus varchar(8000)

  declare @EmpRightsTemp Table(
  record_id integer NOT NULL,
  emp_id integer NOT NULL,
  right_id integer NOT NULL,
  plusterm integer --varchar(150)
  )


 Insert into @EmpRightsTemp
 (record_id,
  emp_id,
  right_id,
  plusterm
 )
 (select er.emp_right_id record_id, 
         er.emp_id, 
		 er.right_id, 
	     CAST(m.val as int) plusterm  
	  from employee_right er
	  cross apply split_del_string(er.limitation, ',') m
	  where 
	  (er.emp_id = @MgrID) and 
	  (er.allowed='Y') and
	  (er.limitation <> '-') and
	  (ISNUMERIC(m.val)=1))



 set @empPlus = (Select STRING_AGG(CAST(tg.term_group_id as varchar(20)) + '=' + tg.comment, ' ^ ') as Plus
                FROM @EmpRightsTemp ert, term_group tg
                WHERE 
                  (ert.plusterm = tg.term_group_id) and (UPPER(tg.group_code) like 'PLUS-%') and 
                  (tg.active = 1))
return (@empPlus)
end


--Select term_group_id, group_code, comment 
--from term_group
--where (upper(group_code) like 'PLUS-%') and
--      (active=1)

--LEN(Select dbo.get_PLUS_termcodes(1191)) as empPLUS
--declare @mgrPlus varchar(max)
--set @mgrPlus = (Select dbo.get_PLUS_termcodes(1191))
--SELECT LEN(@mgrPlus)