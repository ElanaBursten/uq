------------------------------------------------
-- QM-585 Employee PLUS WhiteList: get_PLUS_MgrList
-- Returns list of active Employees under a Manager and if they are whitelisted for utility via employee_plus
-- EB Created 6/2022
------------------------------------------------

if object_id('dbo.get_PLUS_MgrList') is not null
  drop procedure dbo.get_PLUS_MgrList
go


create procedure dbo.get_PLUS_MgrList(@MgrID integer)   
as  

select 'PLUS_MgrList' as tname,
        h.h_emp_id emp_id, h.h_emp_number emp_number, 
        h.h_last_name last_name, h.h_first_name first_name, h.h_short_name short_name,  
        ep.term_group_id tg_id, ep.active tg_active,
        tg.group_code PLUS_code, tg.comment PLUS_desc,
		ep.modified_date, e2.short_name modified_by
from dbo.get_hier2_active(@MgrID,0, 20) h
  left outer join employee_plus ep
    on h.h_emp_id=ep.emp_id
  left outer join term_group tg
    on tg.term_group_id = ep.term_group_id   
  left outer join employee e2
    on ep.modified_by_id = e2.emp_id
where (h_emp_number is not null)
  order by h_last_name, h_first_name, tg.group_code

GO

--  dbo.get_PLUS_MgrList 1191


 
 