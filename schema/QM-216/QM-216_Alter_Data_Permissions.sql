--QM-216  Ability to Edit Ticket Due Date 8/19/2020
--QM-216 - Adds Permissions for using this feature along with the status groups


if not exists(select * from right_definition where entity_data='TicketDetailDueDate')
INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc]
           ,[modified_date])
     VALUES
           ('Tickets - Edit Due Date'
           ,'General'
           ,'TicketDetailDueDate'
           ,'Limitation does not apply to this right.'
           ,GetDate())
GO


