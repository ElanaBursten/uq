-- First, prepare the data

-- Gather the data for the first two, 4:52
select locate_id as tbd_locate_id, sum(price) as tbd_price, max(bucket) as tbd_bucket
 into tbd
 from billing_detail
  group by locate_id 
 
-- building the index, 2:03
alter table tbd add primary key (tbd_locate_id)
 
-- Gather the data for the second two updates, 3:37
select b.locate_id as tfstat_locate_id, CAST(CONVERT(Varchar(20), min(b.status_date), 101) AS DateTime) as fsd
 INTO tfstat
 from locate_status b
 group by  b.locate_id
 
-- building the index, 1:32
alter table tfstat add primary key (tfstat_locate_id)
go
 
-- Get rid of old locate_fact
drop table Locate_Fact
go

-- Make new, empty one.
CREATE TABLE [Locate_Fact] (
	[locate_id] [int] NOT NULL ,
	[ticket_id] [int] NOT NULL ,
	[client_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[client_id] [int] NULL CONSTRAINT [DF_Locate_Fact_client_id] DEFAULT (0),
	[status] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[high_profile] [bit] NULL ,
	[units] [int] NULL CONSTRAINT [DF_Locate_Fact_units] DEFAULT (1),
	[price] [money] NULL CONSTRAINT [DF_Locate_Fact_price] DEFAULT (0.00),
	[closed] [bit] NULL ,
	[closed_how] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[time_id] [datetime] NULL CONSTRAINT [DF_Locate_Fact_time_id] DEFAULT ('1/1/2000'),
	[received_date] [datetime] NULL CONSTRAINT [DF_Locate_Fact_received_date] DEFAULT ('1/1/2000'),
	[closed_date] [datetime] NULL CONSTRAINT [DF_Locate_Fact_closed_date] DEFAULT ('1/1/2000'),
	[due_date] [datetime] NULL ,
	[modified_date] [datetime] NULL ,
	[active] [bit] NULL ,
	[invoiced] [bit] NULL ,
	[assigned_to] [int] NULL ,
	[regular_hours] [numeric](5, 2) NULL ,
	[overtime_hours] [numeric](5, 2) NULL ,
	[added_by] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[watch_and_protect] [bit] NULL ,
	[revenue] [money] NULL CONSTRAINT [DF_Locate_Fact_revenue] DEFAULT (0.00),
	[closed_status] [int] NULL ,
	[initially_statused] [int] NULL CONSTRAINT [DF_Locate_Fact_initially_statused] DEFAULT (0),
	[locate_type_id] [int] NULL ,
	[tier_id] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Locate_Fact_tier_id] DEFAULT (N'Normal'),
	[emp_id] [int] NULL CONSTRAINT [DF_Locate_Fact_emp_id] DEFAULT (0)
)
go

-- Build the locate_fact table, 2:04:32
insert into Locate_Fact (
  locate_id,
  ticket_id,
  client_code,
  client_id,
  status,
  high_profile,
  units,
  price,
  closed,
  emp_id,
  closed_how,
  due_date,
  received_date,
  closed_date,
  modified_date,
  active,
  invoiced,
  assigned_to,
  regular_hours,
  overtime_hours,
  added_by,
  watch_and_protect,
  closed_status,
  locate_type_id,
  initially_statused,
  time_id,
  revenue,
  tier_id
)
SELECT 
  a.locate_id,
  a.ticket_id,
  a.client_code,
  a.client_id,
  a.status,
  a.high_profile,
  a.qty_marked as units,
  a.price,
  a.closed,
  a.closed_by_id as emp_id,
  a.closed_how,
  b.due_date, 
  CAST(CONVERT(Varchar(20), b.modified_date, 101) AS DateTime) as received_date,
  CAST(CONVERT(Varchar(20), a.closed_date, 101) AS DateTime) AS closed_date_notime,
  a.modified_date,
  a.active,
  a.invoiced,
  a.assigned_to,
  a.regular_hours, 
  a.overtime_hours,
  a.added_by,
  a.watch_and_protect, 
  (CASE WHEN b.due_date > a.closed_date THEN 1 ELSE 0 END) AS closed_status, 
  (CASE WHEN a.status ='-N' THEN 0 ELSE 1 END) AS locate_type_id,
  initially_statused = 
    case when fsd < due_date then 1
         else 0 end,
  tfstat.fsd as time_id,
  tbd.tbd_price as revenue,
  tbd.tbd_bucket as tier_id
FROM locate a (index(locate_ticketid))
   inner join ticket b (index(PK_ticket_ticketid)) on a.ticket_id = b.ticket_id
   left join tfstat on a.locate_id = tfstat.tfstat_locate_id
   left join tbd on a.locate_id = tbd.tbd_locate_id
go

-- alter table locate_fact
-- alter column locate_id int NOT NULL
-- go

-- Set a PK, 4:00
alter table locate_fact add primary key nonclustered (locate_id) 
go

/*
select count(*) from locate
select count(*) from locate_fact
select count(*) from tbd
select count(*) from tfstat




*/
