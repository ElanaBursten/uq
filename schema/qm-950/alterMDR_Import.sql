/*
   Friday, May 17, 20249:38:16 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: UQVIEW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MDR_Import ADD
	EmployeeType varchar(15) NULL
GO
ALTER TABLE dbo.MDR_Import SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MDR_Import', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MDR_Import', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MDR_Import', 'Object', 'CONTROL') as Contr_Per 