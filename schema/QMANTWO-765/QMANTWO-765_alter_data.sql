--QMANTWO-765  DAMAGE RECOVERY  (EB create 5/31/2019)
--   * New tables:  damage_recovery  *  damage_recovery_history
--   * Added recovstat type to the reference table

-----------------------------------------------
--RECOVERY STATUS (reference table: recovstat)
-----------------------------------------------
--Recovery_status = 'PENDING', 'NOT RECOVERABLE', 'RECOVERED ALL', 'RECOVERED PARTIAL', 'FAILED'

if not exists(select * from reference where type='recovstat' and code='PENDING')
  INSERT INTO reference(type, code, description) values ('recovstat', 'PENDING', 'PENDING')
if not exists(select * from reference where type='recovstat' and code='NOT RECOVERABLE')
  INSERT INTO reference(type, code, description) values ('recovstat', 'NOT RECOVERABLE', 'NOT RECOVERABLE')
if not exists(select * from reference where type='recovstat' and code='RECOVERED ALL')
  INSERT INTO reference(type, code, description) values ('recovstat', 'RECOVERED ALL', 'RECOVERED ALL')
if not exists(select * from reference where type='recovstat' and code='RECOVERED PART')
  INSERT INTO reference(type, code, description) values ('recovstat', 'RECOVERED PART', 'RECOVERED PARTIAL')
if not exists(select * from reference where type='recovstat' and code='FAILED')
  INSERT INTO reference(type, code, description) values ('recovstat', 'FAILED', 'FAILED')
if not exists(select * from reference where type='recovstat' and code='SUBMITTED')
  INSERT INTO reference(type, code, description) values ('recovstat', 'SUBMITTED', 'SUBMITTED')
GO

--select * from reference where type = 'recovstat'


-----------------------------------------------
--RECOVERY PERMISSIONS ()
-----------------------------------------------

if not Exists(select * from right_definition WHERE entity_data = 'DamageRecoveryView')
  INSERT INTO right_definition
             (right_description,right_type,entity_data,parent_right_id,modifier_desc)
       VALUES
            ('Damages - Recovery View','General','DamageRecoveryView', NULL,
            'Limitation - not defined')
GO

if not Exists(select * from right_definition WHERE entity_data = 'DamageRecoveryEdit')
  INSERT INTO right_definition
             (right_description,right_type,entity_data,parent_right_id,modifier_desc)
       VALUES
             ('Damages - Recovery Edit','General','DamageRecoveryEdit', NULL,
             'Limitation - not defined')
GO

-- This has already been defined in Production.  Adding it here so that it is in repository
--if not Exists(select * from right_definition WHERE entity_data = 'DamagesSetClaimStatus')
--  INSERT INTO right_definition
--             (right_description,right_type,entity_data,parent_right_id,modifier_desc)
--       VALUES
--             ('Damages - Set Claim Status','General','DamagesSetClaimStatus', NULL,
--             'Limitation - not defined')
--GO

----------------------------------------------
--Reversal
----------------------------------------------
--delete from right_definition where entity_data='DamageRecoveryView'
--delete from right_definition where entity_data='DamageRecoveryEdit'