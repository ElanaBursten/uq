--QMANTWO-765  DAMAGE RECOVERY  (EB create 5/31/2019)
--   * table name: damage_recovery_history
--         * Note: history table for damage_recovery
--   

-----------------------------------------------
-- damage_recovery_history TABLE 
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'damage_recovery_history') begin
Create Table damage_recovery_history(
recovery_id int NOT NULL,
archive_date datetime NOT NULL,
damage_id int NOT NULL,
paid_date datetime NULL,
damage_state varchar(2) NULL,
recovery_status varchar(20) NOT NULL,
recovery_amount decimal(12,2) NULL,
recovery_company varchar(50) NULL,
recovery_date datetime NULL, 
comment varchar(8000) NULL, 
active bit NOT NULL,
export_date datetime NULL,
TotalAttachments int NULL,
modified_date datetime NULL,
modified_by int NULL
)
ALTER TABLE damage_recovery_history
ADD CONSTRAINT PK_damagehistory PRIMARY KEY (recovery_id,archive_date);
end
GO





----------------------------------------------
--Reversal
----------------------------------------------
--drop table damage_recovery_history
