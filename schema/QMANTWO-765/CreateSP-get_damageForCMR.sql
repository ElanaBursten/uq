USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[get_damageForCMR]    Script Date: 6/24/2019 1:52:46 PM ******/
DROP PROCEDURE [dbo].[get_damageForCMR]
GO

/****** Object:  StoredProcedure [dbo].[get_damageForCMR]    Script Date: 6/24/2019 1:52:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Declare @DamageID int;
--set @DamageID =  584937

CREATE proc [dbo].[get_damageForCMR](@DamageID int)
as
  select damage_id
		,office_id
		,damage_inv_num
		--,damage_type
		,damage_date
		,uq_notified_date
		,notified_by_person
		,notified_by_company
		,notified_by_phone
		,client_code
		,(select [client_name] from [dbo].[client] where [client].client_id = damage.client_id) as ClientName
		,client_claim_id
		,date_mailed
		,date_faxed
		,sent_to
		,size_type
		,location
		,page
		,grid
		,city
		,county
		,state
		,utility_co_damaged
		,diagram_number
		,remarks
		,investigator_id
		,investigator_arrival
		,investigator_departure
		,investigator_est_damage_time
		,investigator_narrative
		,excavator_company
		,excavator_type
		,excavation_type
		,locate_marks_present
		,locate_requested
		,site_mark_state
		,site_sewer_marked
		,site_water_marked
		,site_catv_marked
		,site_gas_marked
		,site_power_marked
		,site_tel_marked
		,site_other_marked
		,site_pictures
		,site_marks_measurement
		,site_buried_under
		,site_clarity_of_marks
		,site_hand_dig
		,site_mechanized_equip
		,site_paint_present
		,site_flags_present
		,site_exc_boring
		,site_exc_grading
		,site_exc_open_trench
		,site_img_35mm
		,site_img_digital
		,site_img_video
		,disc_repair_techs_were
		,disc_repairs_were
		,disc_repair_person
		,disc_repair_contact
		,disc_repair_comment
		,disc_exc_were
		,disc_exc_person
		,disc_exc_contact
		,disc_exc_comment
		,disc_other1_person
		,disc_other1_contact
		,disc_other1_comment
		,disc_other2_person
		,disc_other2_contact
		,disc_other2_comment
		,exc_resp_code
		,exc_resp_type
		,exc_resp_other_desc
		,exc_resp_details
		,exc_resp_response
		,uq_resp_code
		,uq_resp_type
		,uq_resp_other_desc
		,uq_resp_details
		,spc_resp_code
		,spc_resp_type
		,spc_resp_other_desc
		,spc_resp_details
		,modified_date
		,active
		,profit_center
		,due_date
		,utilistar_id
		,closed_date
		,modified_by
		,uq_resp_ess_step
		,facility_type
		,facility_size
		,facility_material
		,facility_type_2
		,facility_type_3
		,facility_material_2
		,facility_material_3
		,facility_size_2
		,facility_size_3
		,locator_experience
		,locate_equipment
		,was_project
		,claim_status
		,claim_status_date
		,invoice_code
		,added_by
		,accrual_date
		,invoice_code_initial_value
		,uq_damage_id
		,estimate_locked
		,site_marked_in_white
		,site_tracer_wire_intact
		,site_pot_holed
		,site_nearest_fac_measure
		,excavator_doing_repairs
		,offset_visible
		,offset_qty
		,claim_coordinator_id
		,locator_id
		,estimate_agreed
		,approved_by_id
		,approved_datetime
		,reason_changed
		,work_priority_id
		,user_changed_pc
		,marks_within_tolerance
		,intelex_num
 
  
--end of damage  
  ,
		  (select 
			 estimate_id
			,invoice_num
			,amount
			,company
			,received_date
			,damage_city
			,damage_state
			,damage_date
			,satisfied
			,satisfied_by_emp_id
			,satisfied_date
			,comment
			,modified_date
			,approved_date
			,approved_amount
			,paid_date
			,paid_amount
			,invoice_code
			,cust_invoice_date
			,packet_request_date
			,packet_recv_date
			,invoice_approved
			,litigation_id
			,third_party_id
			,added_by
			,payment_code
		 -- ,case
			--when IsNull(litigation_id, 0) > 1 then 2
			--when IsNull(third_party_id, 0) > 1 then 1
			--else 0
		 --  end as invoice_type
		  from damage_invoice
		  where damage_id=@DamageID
		  For XML RAW, TYPE, ROOT('damage_invoice') , ELEMENTS XSINIL),
-- End of Invoice
			  (select damage_bill_id
					 ,billing_period_date
					 ,billable
					 ,modified_date
					 ,invoiced
			  from damage_bill
			  where damage_id=@DamageID
			  For XML RAW, TYPE, ROOT('damage_bill') , ELEMENTS XSINIL),
-- End of Damage_Bill
				(Select ticket_id
				,ticket_number
				,company
				,con_type
				,con_name
				,con_address
				,con_city
				,con_state
				,con_zip
				,call_date
				,caller
				,caller_contact
				,caller_phone
				,caller_cellular
				,caller_fax
				,caller_altcontact
				,caller_altphone
				,caller_email
				,image
				from ticket 
				where ticket.ticket_id = (select ticket_id from damage where damage_id =@DamageID)
                For XML RAW, TYPE, ROOT('ticket') , ELEMENTS XSINIL),
-- End of Ticket
				  (select litigation_id
						,file_number
						,plaintiff
						,carrier_id
						,attorney
						,party
						,demand
						,accrual
						,settlement
						,closed_date
						,comment
						,defendant
						,venue
						,venue_state
						,service_date
						,summons_date
						,carrier_notified
						,claim_status
						,added_by
						,modified_date
				  from damage_litigation
				  where damage_id= @DamageID
				  For XML RAW, TYPE, ROOT('damage_litigation') , ELEMENTS XSINIL),
-- End of damage_litigation
					  (select third_party_id
							,claimant
							,address1
							,address2
							,city
							,state
							,zipcode
							,phone
							,claim_desc
							,uq_notified_date
							,carrier_id
							,carrier_notified
							,demand
							,claim_status
							,added_by
							,modified_date
					  from damage_third_party
					  where damage_id=@DamageID
					  For XML RAW, TYPE, ROOT('damage_third_party') , ELEMENTS XSINIL),
--  End of damage_third_party
						  (select notes_id
								--,foreign_type
								--,foreign_id
								,entry_date
								,modified_date
								,uid
								,active
								,note
								,sub_type
						  ,case foreign_type
							when 2 then 0
							when 3 then 1
							when 4 then 2
							else null end as damage_notes_type,
						   @DamageID as damage_id
						  from notes
						  where (foreign_id = @DamageID and foreign_type = 2)
						   or (foreign_id in (select third_party_id from damage_third_party
						   where damage_id = @DamageID) and foreign_type = 3)
						   or (foreign_id in (select litigation_id from damage_litigation
						   where damage_id = @DamageID) and foreign_type = 4)
						  For XML RAW, TYPE, ROOT('Notes') , ELEMENTS XSINIL)
-- End of Notes

    from damage
  where damage_id=@DamageID
  For XML RAW, TYPE, ROOT('damage') , ELEMENTS XSINIL;
GO

