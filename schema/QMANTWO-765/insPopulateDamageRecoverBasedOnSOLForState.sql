declare @ThisDate Date,
@recoveryStatus varchar(20);

set @recoveryStatus = 'PENDING'; 
set @ThisDate = GetDate();

insert into [dbo].[damage_recovery]([damage_id], [paid_date], [damage_state], [recovery_status])
select damage_id, paid_date, [damage_state], @recoveryStatus
from [damage_invoice]
where  
   ([damage_state] = 'CA' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'CO' and datediff(M, paid_date, @ThisDate)<25)
or ([damage_state] = 'DC' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'DE' and datediff(M, paid_date, @ThisDate)<25)
or ([damage_state] = 'GA' and datediff(M, paid_date, @ThisDate)<49)
or ([damage_state] = 'IN' and datediff(M, paid_date, @ThisDate)<25)
or ([damage_state] = 'MD' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'NJ' and datediff(M, paid_date, @ThisDate)<73)
or ([damage_state] = 'NY' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'OH' and datediff(M, paid_date, @ThisDate)<25)
or ([damage_state] = 'OR' and datediff(M, paid_date, @ThisDate)<73)
or ([damage_state] = 'SC' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'TN' and datediff(M, paid_date, @ThisDate)<37)
or ([damage_state] = 'TX' and datediff(M, paid_date, @ThisDate)<25)
or ([damage_state] = 'VA' and datediff(M, paid_date, @ThisDate)<61)
or ([damage_state] = 'WA' and datediff(M, paid_date, @ThisDate)<37)
order by paid_date  