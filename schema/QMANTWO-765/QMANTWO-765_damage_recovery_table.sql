--QMANTWO-765  DAMAGE RECOVERY  (EB create 5/31/2019)
--   * table name: damage_recovery
--         * Note: see also the related history table: damage_recovery_history
--   

-----------------------------------------------
-- damage_recovery TABLE 
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'damage_recovery')
Create Table damage_recovery(
recovery_id int identity(1,1) Primary Key NOT NULL,
damage_id int NOT NULL,
paid_date datetime NULL,
damage_state varchar(2) NULL,
recovery_status varchar(20) NOT NULL,
recovery_amount decimal(12,2) NULL,
recovery_company varchar(50) NULL,
recovery_date datetime NULL,
comment varchar(8000) NULL, 
export_date datetime NULL,
TotalAttachments int NULL,
modified_date datetime NULL,
modified_by int NULL
)
GO

-----------------------------------------------
-- damage_recovery trigger 
-----------------------------------------------
if exists(select name from sysobjects where name = 'damage_recovery_iu' and type = 'tr')
  drop trigger damage_recovery_iu
GO

create trigger damage_recovery_iu on damage_recovery
for INSERT, UPDATE
not for replication
as
BEGIN
  declare @ChangeDate datetime;
  declare @RecoveryID int;
  declare @Operation char(1);
  declare @EmpID int;

--Fields from damage_recovery that we want to retain as-is
  declare @damageid int;
  declare @paid_date datetime;
  declare @status varchar(20);
  declare @amt decimal(12,2);
  declare @co varchar(50);
  declare @comment varchar(8000);
  declare @expdate datetime;
  declare @moddate datetime;
  declare @modby int;
  declare @recovery_date datetime;
  declare @totalattachments int;
  declare @damage_state varchar(2);


  set @ChangeDate=GetDate();

  --If Exists(select * from DELETED)
  --    select @Operation='U'
	 -- else
	 -- select @Operation='I'

    select @RecoveryID=i.recovery_id, @damageid=i.damage_id, 
	       @paid_date=i.paid_date, @status=i.recovery_status, 
	       @amt=i.recovery_amount, @co=i.recovery_company, @comment=i.comment,
		   @expdate=i.export_date, @moddate=i.modified_date, @modby=i.modified_by, 
		   @EmpID=i.modified_by, @recovery_date=i.recovery_date, @totalattachments=i.TotalAttachments, 
		   @damage_state=i.damage_state from inserted i

  -- "Deactivate" any current history records
      update damage_recovery_history 
	  set active = 0, modified_date = @ChangeDate 
	  where recovery_id = @RecoveryID

  --Insert new record into history table
    insert into damage_recovery_history (recovery_id, archive_date, damage_id, paid_date, recovery_status, recovery_amount, recovery_company, comment,                                                            active, export_date, modified_date, modified_by, recovery_date, TotalAttachments, damage_state)
	                              values(@RecoveryID, @ChangeDate, @damageid, @paid_date, @Status, @amt, @co, @comment, 
								                      1, @expdate, @ChangeDate, @EmpID, @recovery_date, @totalattachments, @damage_state)


     update damage_recovery set modified_date = getdate() where recovery_id in (select recovery_id from inserted)
END
GO

----------------------------------------------
--Reversal
----------------------------------------------
--drop table damage_recovery






