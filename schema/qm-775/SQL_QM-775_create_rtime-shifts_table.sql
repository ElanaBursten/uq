USE [QM]
GO

/****** Object:  Table [dbo].[rabbitmq_staging_rtime_shifts]    Script Date: 7/21/2023 1:05:23 PM ******/
DROP TABLE [dbo].[rabbitmq_staging_rtime_shifts]
GO

/****** Object:  Table [dbo].[rabbitmq_staging_rtime_shifts]    Script Date: 7/21/2023 1:05:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[rabbitmq_staging_rtime_shifts](
	[rabbitmq_staging_shifts_id] [int] IDENTITY(1,1) NOT NULL,
	[source] [varchar](25) NULL,
	[shiftdate] [datetime] NULL,
	[shiftid] [int] NULL,
	[workernumber] [varchar](15) NULL,
	[emp_id] [int] NULL,
	[eventid] [int] NOT NULL,
	[eventtime] [datetime] NULL,
	[eventtype] [varchar](25) NULL,
	[latitude] [decimal](9, 6) NULL,
	[longitude] [decimal](9, 6) NULL,
	[lateentryflag] [varchar](10) NULL,
	[acknowledgement] [varchar](25) NULL,
	[editreasoncode] [varchar](25) NULL,
	[editreasonstatement] [varchar](25) NULL,
	[editreasonshortdescription] [varchar](50) NULL,
	[shift_type_id] [int] NULL,
	[name] [varchar](50) NULL,
	[insert_date] [datetime] NULL,
	[procedded] [bit] NULL,
	[process_status] [varchar](50) NULL
) ON [PRIMARY]
GO

