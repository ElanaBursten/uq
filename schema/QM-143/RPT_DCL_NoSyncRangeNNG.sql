USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_DCL_NoSyncRangeNNG]    Script Date: 3/30/2020 4:21:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER   procedure [dbo].[RPT_DCL_NoSyncRangeNNG]
  (
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer, 
  @ShowStatusHistory bit    -- 0 = False, 1 = True
  )
as  
              --20745-8b386cd
select
  case
  when locate.client_code in ( 'NWN01-S', 'NWN02-S') then
  Replace(ticket.ticket_number,'-'+serial_number,'') 
  else
  ticket.ticket_number End as ticket_number,  --QM-153  sr
  convert(datetime, convert(varchar(12), locate.closed_date , 102) , 102) as closed_date_only,
  locate.closed_date,
  case 
  when locate.client_code in ( 'NWN01-S', 'NWN02-S') then
  locate.client_code+dbo.extractNumbers(ticket.serial_number)
  else
  locate.client_code End as client_code,   --QM-143
  locate.status,
  locate.modified_date,  -- most recent sync date
  locate.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date,
  e.first_name+' '+e.last_name as EmployeeName
from ticket with (index(ticket_transmit_date))
  inner join locate with (index(locate_ticketid)) on ticket.ticket_id = locate.ticket_id  
  inner join employee e on locate.Assigned_to_id = e.emp_id
where locate.status <> '-N'
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
--  and (locate.modified_date in (select max(l.modified_date)
--                                from locate l
--                                where l.ticket_id = ticket.ticket_id)
--      or @ShowStatusHistory=1)
order by locate.closed_date


