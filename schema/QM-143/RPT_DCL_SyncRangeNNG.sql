USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_DCL_SyncRangeNNG]    Script Date: 3/30/2020 5:02:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER   procedure [dbo].[RPT_DCL_SyncRangeNNG]
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as                  --20745-8b386cd

/* Any idea in progress, for later:
declare @EarliestTicketID integer
declare @EarliestLocateID integer
select @EarliestTicketID = (select min(ticket_id) from (select top 1000 ticket_id from ticket where transmit_date>=@XmitDateFrom) t1)
select @EarliestLocateID = (select min(locate_id) from locate where ticket_id = @EarliestTicketID)
*/
select 
  case
  when L.client_code in ( 'NWN01-S', 'NWN02-S') then
  Replace(ticket.ticket_number,'-'+serial_number,'') 
  else
  ticket.ticket_number End as ticket_number,  --QM-153  sr
  L.closed_date_only,
  L.closed_date,
  case 
  when L.client_code in ( 'NWN01-S', 'NWN02-S') then
  L.client_code+dbo.extractNumbers(ticket.serial_number)
  else
  L.client_code End as client_code,   --QM-143
  L.status,
  L.modified_date,  -- sync date
  L.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date,
  L.EmployeeName 
FROM 
 (select
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102) as closed_date_only,
  min(ls.status_date) as closed_date,
  locate.client_code,
  ls.status,
  min(ls.insert_date) as modified_date,  -- sync date
  locate.closed,
  locate.ticket_id,
  locate.locate_id,
  e.first_name+' '+e.last_name as EmployeeName
 from locate_status ls
  inner join locate on ls.locate_id = locate.locate_id
  inner join employee e on locate.Assigned_to_id = e.emp_id
 where ls.insert_date between @SyncDateFrom and @SyncDateTo
  and ls.status <> '-N'
  and locate.modified_date >= @SyncDateFrom  -- this one is a perf opt
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and (ls.status_date in (select max(locate_status.status_date)
                           from locate_status  
                           where locate_status.locate_id = locate.locate_id)
      or @ShowStatusHistory=1)
 group by
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102),
  locate.client_code,
  ls.status,
  locate.closed,
  locate.ticket_id,
  locate.locate_id, 
  e.first_name+' '+e.last_name  --locate.Assigned_to_id   
 ) L
  inner join ticket on ticket.ticket_id = L.ticket_id  
where ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by L.closed_date

