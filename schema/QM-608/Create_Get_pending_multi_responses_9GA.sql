USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[get_pending_multi_responses_9GA]    Script Date: 7/29/2022 2:39:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER Procedure [dbo].[get_pending_multi_responses_9GA](@RespondTo varchar(20), 
 @CallCenter varchar(20), @ParsedLocatesOnly bit = 1, 
 @FTPRetryMinutes int = 240, @ResponseDelayMinutes int = 0, 
 @EmailRetryMinutes int = 120, @LocateAddedBys varchar(8000) = 'PARSER') 
as 

--Declare @RespondTo varchar(20) 
--Declare @CallCenter varchar(20)
--Declare @ParsedLocatesOnly bit = 1
--Declare @FTPRetryMinutes int = 240
--Declare @ResponseDelayMinutes int = 0
--Declare @EmailRetryMinutes int = 120
--Declare @LocateAddedBys varchar(8000) = 'PARSER'




set @RespondTo = 'alcs_ga'
set @CallCenter = '3003'

set nocount on 

Drop table if exists #TempResponseTable
 
select top 800 
 rq.locate_id, locate.status, ticket.ticket_format, 
 ticket.ticket_id, ticket.ticket_number, locate.client_code, 
 ticket.ticket_type, ticket.work_state, rq.insert_date, 
 ticket.serial_number, locate.closed_date, ticket.work_county, 
 ticket.work_city, ticket.work_date, ticket.service_area_code, 
 ticket.due_date, e.short_name as locator_name, 
 rq.related_response_id, rlg.response_date as related_response_date, 
 rlg.response_date_is_dst as related_response_date_is_dst, 
 rlg.status as related_status, rlg.response_sent as related_response_sent, 
 locate.qty_marked, rq.in_progress_response_id, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='ONGOCONT')), '') as contact, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='CONPHNE')), '') as contact_phone, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='RCOMMENT')), '') as response_comment, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='SMDT')), '') as scheduled_meet_datetime, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='ACTOFGOD')), '') as act_of_god, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='CONMETH')), '') as contact_method, 
 (select min(arrival_date) from jobsite_arrival ja 
    where ja.ticket_id=ticket.ticket_id) as first_arrival_date 

 Into #TempResponseTable
 from responder_multi_queue rq 
 inner join locate on rq.locate_id=locate.locate_id 
 inner join ticket on locate.ticket_id=ticket.ticket_id 
 left join employee e 
  on e.emp_id = coalesce(locate.closed_by_id, locate.assigned_to) 
 left join response_log rlg 
  on rlg.response_id = rq.related_response_id 
where 
 len(coalesce(@CallCenter, '')) > 0 
 and rq.ticket_format = @CallCenter 
 and rq.respond_to = @RespondTo 
 
 -- todo(dan) Add do_not_respond_before if/when it's needed 
 -- and (rq.do_not_respond_before is null 
 --      or rq.do_not_respond_before <= getdate()) 
 
 and ticket.ticket_number not like 'MAN%' 
 and isnull(ticket.status, '') not like 'MANUAL%' 
 
 and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser') 
  or @ParsedLocatesOnly = 0) 
 
 and not exists (select * from response_log rl 
    where rl.reply = '(ftp waiting)' 
    and rl.locate_id = rq.locate_id 
    and DateDiff(minute, rl.response_date, GetDate()) < @FTPRetryMinutes) 
 
 and not exists (select * from response_log rl2 
    where rl2.reply = '(email waiting)' 
    and rl2.response_id = rq.in_progress_response_id 
    and rl2.locate_id = rq.locate_id 
    and DateDiff(minute, rl2.response_date, GetDate()) < @EmailRetryMinutes) 
 
 and ((DateDiff(minute, rq.insert_date, GetDate()) > @ResponseDelayMinutes) 
  or @ResponseDelayMinutes = 0) 
 
 -- todo(dan) remove ParsedLocatesOnly option, after all uses replaced by this 
 and ((locate.added_by in (select S from StringListToTable(@LocateAddedBys))) 
  or (@LocateAddedBys = '*')) 
 
order by insert_date 
 
Update #TempResponseTable
set #TempResponseTable.serial_number = ticket_version.serial_number 
 from #TempResponseTable, ticket_version 
where #TempResponseTable.ticket_id = ticket_version.ticket_id
and #TempResponseTable.serial_number like '%'+#TempResponseTable.client_code
and #TempResponseTable.serial_number  like '%|%' 


select * from #TempResponseTable where serial_number is not null

Drop table if exists #TempResponseTable