--QM-118 Timesheet Approval - Select All Button
--QM-118 Note: New permission for Select ALL on Timesheet Approval to limit who can use
if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'TimesheetsApproveSelectAll')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Timesheets - Approval Select All', 'General', 'TimesheetsApproveSelectAll', 'Limitation is not used for this right.')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'TimesheetsApproveSelectAll'

  Select * from right_definition where entity_data = 'TimesheetsApproveSelectAll'
