--QM-147 Permissions for using Powershell Bootup instead of WMI
--Note: this option is also available in the INI Settings

if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'UsePowershellBootup')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Config - Use Powershell Bootup Time', 'General', 'UsePowershellBootup', 'Limitation is not used for this right.')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'UsePowershellBootup'

  Select * from right_definition where entity_data = 'UsePowershellBootup'
