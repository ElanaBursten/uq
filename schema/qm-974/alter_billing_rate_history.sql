/*
   Thursday, July 25, 20249:50:26 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_rate_history
	DROP CONSTRAINT DF__billing_r__parti__46DD686B
GO
ALTER TABLE dbo.billing_rate_history
	DROP CONSTRAINT user_name_constraint
GO
ALTER TABLE dbo.billing_rate_history
	DROP CONSTRAINT host_name_constraint
GO
CREATE TABLE dbo.Tmp_billing_rate_history
	(
	br_id int NULL,
	call_center varchar(20) NOT NULL,
	billing_cc varchar(10) NOT NULL,
	status varchar(20) NOT NULL,
	parties int NOT NULL,
	rate money NULL,
	bill_code varchar(10) NULL,
	bill_type varchar(10) NULL,
	area_name varchar(40) NULL,
	work_county varchar(40) NULL,
	line_item_text varchar(60) NULL,
	work_city varchar(40) NULL,
	modified_date datetime NULL,
	archive_date datetime NOT NULL,
	which_invoice varchar(20) NULL,
	additional_rate money NULL,
	user_name varchar(40) NULL,
	os_user_name varchar(40) NULL,
	computer_name varchar(40) NULL,
	value_group_id int NULL,
	additional_line_item_text varchar(60) NULL,
	CWICode varchar(30) NULL,
	additional_CWICode varchar(30) NULL,
	Rpt_gl nvarchar(25) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_billing_rate_history SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_billing_rate_history ADD CONSTRAINT
	DF__billing_r__parti__46DD686B DEFAULT (1) FOR parties
GO
ALTER TABLE dbo.Tmp_billing_rate_history ADD CONSTRAINT
	user_name_constraint DEFAULT (suser_sname()) FOR user_name
GO
ALTER TABLE dbo.Tmp_billing_rate_history ADD CONSTRAINT
	host_name_constraint DEFAULT (host_name()) FOR computer_name
GO
IF EXISTS(SELECT * FROM dbo.billing_rate_history)
	 EXEC('INSERT INTO dbo.Tmp_billing_rate_history (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name, work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, user_name, os_user_name, computer_name, value_group_id, additional_line_item_text, CWICode, additional_CWICode, Rpt_gl)
		SELECT br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name, work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, user_name, os_user_name, computer_name, value_group_id, additional_line_item_text, CONVERT(varchar(30), CWICode), CONVERT(varchar(30), additional_CWICode), Rpt_gl FROM dbo.billing_rate_history WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.billing_rate_history
GO
EXECUTE sp_rename N'dbo.Tmp_billing_rate_history', N'billing_rate_history', 'OBJECT' 
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_rate_history', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_rate_history', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_rate_history', 'Object', 'CONTROL') as Contr_Per 