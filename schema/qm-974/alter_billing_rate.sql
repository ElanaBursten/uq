/*
   Thursday, July 25, 20249:48:51 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
USE QM;
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_rate
	DROP CONSTRAINT billing_rate_fk_value_group
GO
ALTER TABLE dbo.value_group SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.value_group', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.value_group', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.value_group', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_rate
	DROP CONSTRAINT DF__billing_r__parti__3EA749C6
GO
ALTER TABLE dbo.billing_rate
	DROP CONSTRAINT DF__billing_r__modif__47D18CA4
GO
CREATE TABLE dbo.Tmp_billing_rate
	(
	br_id int NOT NULL IDENTITY (3000, 1),
	call_center varchar(20) NOT NULL,
	billing_cc varchar(10) NOT NULL,
	status varchar(20) NOT NULL,
	parties int NOT NULL,
	rate money NULL,
	bill_code varchar(10) NULL,
	bill_type varchar(10) NOT NULL,
	area_name varchar(40) NULL,
	work_county varchar(40) NULL,
	line_item_text varchar(60) NULL,
	work_city varchar(40) NULL,
	modified_date datetime NOT NULL,
	which_invoice varchar(20) NULL,
	additional_rate money NULL,
	value_group_id int NULL,
	additional_line_item_text varchar(60) NULL,
	CWICode nvarchar(30) NULL,
	Rpt_gl nvarchar(25) NULL,
	Additional_Rpt_gl nvarchar(25) NULL,
	additional_CWICode nvarchar(30) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_billing_rate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_billing_rate ADD CONSTRAINT
	DF__billing_r__parti__3EA749C6 DEFAULT (1) FOR parties
GO
ALTER TABLE dbo.Tmp_billing_rate ADD CONSTRAINT
	DF__billing_r__modif__47D18CA4 DEFAULT (getdate()) FOR modified_date
GO
SET IDENTITY_INSERT dbo.Tmp_billing_rate ON
GO
IF EXISTS(SELECT * FROM dbo.billing_rate)
	 EXEC('INSERT INTO dbo.Tmp_billing_rate (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name, work_county, line_item_text, work_city, modified_date, which_invoice, additional_rate, value_group_id, additional_line_item_text, CWICode, Rpt_gl, additional_CWICode)
		SELECT br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name, work_county, line_item_text, work_city, modified_date, which_invoice, additional_rate, value_group_id, additional_line_item_text, CONVERT(nvarchar(30), CWICode), Rpt_gl, CONVERT(nvarchar(30), additional_CWICode) FROM dbo.billing_rate WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_billing_rate OFF
GO
DROP TABLE dbo.billing_rate
GO
EXECUTE sp_rename N'dbo.Tmp_billing_rate', N'billing_rate', 'OBJECT' 
GO
ALTER TABLE dbo.billing_rate ADD CONSTRAINT
	PK__billing_rate__3DB3258D PRIMARY KEY CLUSTERED 
	(
	br_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.billing_rate ADD CONSTRAINT
	constr_billing_rate_unique UNIQUE NONCLUSTERED 
	(
	call_center,
	billing_cc,
	status,
	parties,
	bill_type,
	work_county,
	area_name,
	work_city,
	value_group_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.billing_rate ADD CONSTRAINT
	billing_rate_fk_value_group FOREIGN KEY
	(
	value_group_id
	) REFERENCES dbo.value_group
	(
	value_group_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE TRIGGER [dbo].[billing_rate_iud] ON dbo.billing_rate
FOR INSERT, UPDATE, DELETE
AS
BEGIN
  set nocount on
  declare @ChangeTime datetime
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
    insert into billing_rate_history
     (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, value_group_id, additional_line_item_text, CWICode, additional_CWICode, Rpt_gl)
    select
      br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, @ChangeTime, which_invoice, additional_rate, value_group_id, additional_line_item_text, CWICode,additional_CWICode,Rpt_gl
    from deleted

  -- Upon insert/update update the modified_date for the changed rows  --qm-850  sr
    --update billing_rate
    --set modified_date = @ChangeTime
    --where br_id in (select br_id from inserted)
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'CONTROL') as Contr_Per 