--QM-142 Add multiple locators to ticket (TicketsAddLocatesWLocator)
--This is a scaled back version of MultiLocatorsForProjTicket 

if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'TicketsAddLocatesWLocator')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Tickets - Add Locates w/Locators', 'General', 'TicketsAddLocatesWLocator', 'Limitation is Employee Hierarchy level as list of available locators')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'MultiLocatorsForTicket'

  Select * from right_definition where entity_data = 'TicketsAddLocatesWLocator'
