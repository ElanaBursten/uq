--QM-10 Export Work Management Ticket List (to GPX file)
--QM-10 Note: Adds GPX Export Permission for Work Management
if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'ManageWorkExport')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Tickets - Management Export', 'General', 'ManageWorkExport', 'Limitation is not used for this right.')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'ManageWorkExport'

  Select * from right_definition where entity_data = 'ManageWorkExport'