--QM-10 Export GPX Work Management
--QMANTWO-10 Note: changes the Work Management open tickets list to include route_order, lat, lon and state column. Related to QMANTWO-775 Route Order 

if object_id('dbo.get_open_tickets_for_locator2') is not null
  DROP procedure dbo.get_open_tickets_for_locator2
GO
 
CREATE procedure get_open_tickets_for_locator2 (@locator_id int) 
as 
  declare @results table ( 
    ticket_id integer not null primary key, 
    ticket_number varchar(20) not null,
    kind varchar(20) not null,
    due_date datetime null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null,
    work_type varchar(90) null,
    legal_good_thru datetime null, 
    map_page varchar(20) null, 
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null, 
    work_address_number varchar(10) null, 
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    points decimal(6,2) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null,
    est_start_time datetime null,
    area_name varchar(40),
    route_order decimal(9,5),  --QMANTWO-775 QM-10
    lat decimal(9,5), --QM-10
    lon decimal(9,5), --QM-10
    work_state varchar(2));  --QM-10

  declare @limit integer
  select @limit = coalesce(ticket_view_limit, 0) from employee where emp_id = @locator_id

  insert into @results 
  select ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(substring(r.description,0,20), 'Normal') as work_priority,   --QMANTWO-565
    case when
      @limit < 1 or ticket.kind = 'EMERGENCY' or ticket.kind = 'ONGOING' or r.code='Release' then 'Y'
      else 'N'
    end as ticket_is_visible,
    (select min(task.est_start_date) from task_schedule task
     where task.ticket_id = ticket.ticket_id
       and task.est_start_date >= GetDate() and task.active=1
       and task.emp_id = @locator_id) as est_start_time
	,a.area_name,
	ticket.route_order, ticket.work_lat, ticket.work_long, ticket.work_state 

  from ticket
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id from locate
      where assigned_to = @locator_id
    )


  update @results set ticket_is_visible = 'Y' where ticket_id in
    (select top(@limit) ticket_id from @results where ticket_is_visible <> 'Y'
       order by work_priority_sort desc, due_date, ticket_number)

/*   For SQL Server 2005 and later, use ROW_COUNT() or the update above.
   Only constant TOP limits are supported in SQL Server 2000, so use the
   slower one at-a-time method below for now. - ugh
*/

/*  declare @idx integer
  set @idx = 1
  while @idx <= @limit
  begin
    update @results set ticket_is_visible = 'Y'
    where ticket_id = (select top 1 ticket_id from @results where ticket_is_visible <> 'Y'
      order by work_priority_sort desc, due_date, ticket_number)
    set @idx = @idx + 1
  end
*/
  select 'ticket' as tname, * from @results ticket order by work_priority_sort desc, due_date, ticket_number

GO
grant execute on get_open_tickets_for_locator2 to uqweb, QManagerRole

