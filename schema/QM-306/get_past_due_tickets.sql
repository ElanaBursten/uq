--QM-306 Utils on WorkManagementTicket Grid 

if object_id('dbo.get_past_due_tickets') is not null
  drop procedure dbo.get_past_due_tickets
go	

create procedure get_past_due_tickets(@ManagerID Int, @MaxDays Int)
as

DECLARE @pastduetickets TABLE (    
    ticket_id integer not null, 
    ticket_number varchar(20) not null,
	emp_id integer not null,
	short_name varchar(30) null,
    kind varchar(20) not null,
    due_date datetime null,
	work_address_number varchar(10) null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null,
    work_type varchar(90) null,
    legal_good_thru datetime null, 
    map_page varchar(20) null, 
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null,     
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    --points decimal(6,2) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null,
    --est_start_time datetime null,
    area_name varchar(40),
    route_order decimal(9,5),  
    lat decimal(9,5),
    lon decimal(9,5),
    work_state varchar(2),
	util varchar(150)); 


insert into @pastduetickets
  select distinct
    ticket.ticket_id, 
	ticket.ticket_number,
	loc.assigned_to,
	emp.short_name, 
    ticket.kind, 
	ticket.due_date,
	ticket.work_address_number,
    ticket.work_address_street, 
	ticket.work_description, 
	ticket.work_type,
    ticket.legal_good_thru, 
	ticket.map_page, 
	ticket.ticket_type, 
	ticket.work_city,
    ticket.ticket_format, 	 
	ticket.work_county,
    ticket.do_not_mark_before, 
	ticket.modified_date, 
	ticket.alert,
    null as scheduled, 
	null as start_date,  
	null as end_date,  --null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(substring(r.description,0,20), 'Normal') as work_priority, 
    case when
     -- @limit < 1 or
	  (ticket.kind = 'EMERGENCY') 
	    or (ticket.Kind = 'NORMAL')
	    or (ticket.kind = 'ONGOING') 
		or (r.code='Release') then 'Y'
      else 'N'
    end as ticket_is_visible, 
	a.area_name,
	ticket.route_order, 
	ticket.work_lat, 
	ticket.work_long, 
	ticket.work_state,
	---------------------------------------
	isnull(STUFF 
  ((
    SELECT distinct ', ' + c.Utility_type
      FROM client c, locate l
      WHERE (c.client_id = l.client_id)
	  And (l.ticket_id = ticket.ticket_Id)
	  And l.status <> '-N'
      FOR XML PATH('')), 1, 1, ' '), '-')
    -------------------------------------- 
  from ticket
  inner join [locate] loc
    on ticket.ticket_id = loc.ticket_id
  inner join (select h_emp_id from dbo.get_hier(@ManagerID,0)) mgremps
    on mgremps.h_emp_id = loc.assigned_to
  left join employee emp
    on loc.assigned_to = emp.emp_id
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
   where (ticket.due_date < GetDate())

   select 'pastdue' as tname, * from @pastduetickets pastdue order by due_date, short_name



 -- get_past_due_tickets 3834, 20




 
