--QM-306 Permissions 


if not exists(select * from right_definition where entity_data='TicketsViewUtilIcons')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - View Utility Icons', 'General', 'TicketsViewUtilIcons', 'Limitation does not apply to this right', GetDate());
GO

select * from right_definition order by right_description

