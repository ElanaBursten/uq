--QM-551 Age 
--QM-387 Risk Score
--QM-368 Update to STRING_AGG:  Only get utils for this locator
--QM-353 Add top level manager utility summary
--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)

if object_id('dbo.get_old_tickets_for_locator2') is not null
  drop procedure dbo.get_old_tickets_for_locator2
go
create procedure dbo.get_old_tickets_for_locator2 (
       @locator_id int, 
	   @num_days int,
	   @toplevel bit = 0)
as
 
  declare @since datetime
  declare @assignments_since datetime

  select @since = dateadd(Day, -@num_days, getdate())

  select 'ticket' as tname, ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    (select min(task.est_start_date) from task_schedule task 
     where task.ticket_id = ticket.ticket_id
       and task.est_start_date >= GetDate()
       and task.emp_id = @locator_id 
       and task.active=1) as est_start_time,
	   a.area_name,
	   ticket.route_order,
	   ticket.work_lat lat,
	   ticket.work_long lon,
	   ticket.work_state,
	   ticket.company, ticket.con_name,  --QM-325

--------------Utility Icons -----------------------
    CASE
      WHEN @toplevel = 0 THEN   --Not top level node
		 (SELECT STRING_AGG(c.Utility_type, ', ') as u
          FROM locate l1 , client c WITH (NOLOCK)
          WHERE (l1.ticket_id = ticket.ticket_Id)
		  AND (l1.assigned_to  = @LOCATOR_ID)  -- Narrow down to just this locator
	      AND (c.client_id = l1.client_id)	
	      And (l1.client_id is not null))            --QM-357 effectively removes -Ns  SR
	 ELSE ' '			 
     END as Util,

-----------Utility Icons (2012 Compatible)-------------------
   --          CASE
   --               WHEN @toplevel = 0 THEN   --Not top level node
			--	    isnull(STUFF 
   --                 ((
   --                   SELECT distinct ', ' + c.Utility_type
   --                   FROM client c, locate l WITH (NOLOCK)
   --                   WHERE (c.client_id = l.client_id)
	  --                And (l.ticket_id = ticket.ticket_Id)
	  --                And l.client_id is not null
   --                   FOR XML PATH('')), 1, 1, ' '), '-') 				    
			--	 ELSE ' '			 
			--END as UTIL,
----------------------------------------------------
(select dbo.get_latest_risk_score(ticket.ticket_id)),
----------------------------------------------------
(select dbo.get_ticket_age_hours(ticket.ticket_id)) as age_hours
--(select datediff(hour, ticket.transmit_date, GetDate())) as age_hours  --QM-551 

  FROM ticket 
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id from locate
     where assigned_to_id = @locator_id
      and (closed_date >= @since or closed_date is null)
      and (status <> '-N')
   )
