--QM-551 Age
--Added to calculate 


if object_id('dbo.[get_ticket_age_hours]') is not null
  drop function dbo.[get_ticket_age_hours]
go


create function [dbo].[get_ticket_age_hours] (
  @TicketID integer
)
returns integer --age hours based on Call Center time offset
as
begin
  declare @AgeInHours integer
  set @AgeInHours = 0
--

  set @AgeInHours = (
  select (
          case 
		    when cc.time_zone = 1198 --PST 
			  then datediff(hour,transmit_date,getdate())-3 
            when cc.time_zone = 1199 --EST
              then datediff(hour,transmit_date,getdate())
            when cc.time_zone = 1200 --CST
              then datediff(hour,transmit_date,getdate())-1
            when cc.time_zone = 1203 --MST
              then datediff(hour,transmit_date,getdate())-2
            when cc.time_zone = null --unknown, default
              then datediff(hour,transmit_date,getdate())
          end)
 from ticket t with (nolock)
   join call_center cc on cc_code = t.ticket_format 
 where ticket_id = @TicketID)

  return @AgeInHours
end

GO

----------------------------------
