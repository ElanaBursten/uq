--QM-551 Age
--Added to easily pull name when needed


if object_id('dbo.[get_locator_name_for_ticket]') is not null
  drop function dbo.[get_locator_name_for_ticket]
go


create function [dbo].[get_locator_name_for_ticket] (
  @TicketID integer
)
returns varchar(30) --employee short name
as
begin
  declare @LocatorName varchar(30)
  set @LocatorName = '--'
--
	       set @LocatorName = (select top 1 e.short_name
           from [locate] l inner join employee e on l.assigned_to_id = e.emp_id
           where (l.ticket_id = @TicketID) and (l.status <> '-N') and (l.status <> '-P')) 

  return @LocatorName
end

GO

--select dbo.get_locator_name_for_ticket(1442)