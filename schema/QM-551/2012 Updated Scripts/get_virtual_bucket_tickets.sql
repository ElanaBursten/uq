--------------------------------------------------------------
-- QM-551 Age and Days Overdue 
-- QM-486 Virtual Buckets - Retrieves the Virtual Bucket by ManagerID and Bucket Name
-- QM-486 get_virtual_bucket_tickets - new procedure for this release
-- EB

--     **PRODUCTION INSTALLATION INSTRUCTIONS:  CHECK Utility Icons line**
--------------------------------------------------------------
if object_id('dbo.get_virtual_bucket_tickets') is not null
  drop procedure dbo.get_virtual_bucket_tickets
go

create procedure [dbo].[get_virtual_bucket_tickets](
  @ManagerID Integer, 
  @BucketName varchar(15),
  @toplevel bit = 0)
as
--  NOTE: going to break some of this down into variables so that it is easier to read ~EB
DECLARE @Today DateTime
DECLARE @Tomorrow Datetime
DECLARE @DaysBack Integer
DECLARE @OnlyPastDueFlag Integer
DECLARE @OnlyTodayFlag Integer
DECLARE @TodayStartAsDec float 
DECLARE @TomorrowEndAsDec float
DECLARE @OnlyPastFlagDate Datetime
DECLARE @TodayStartMod datetime
DECLARE @TomorrowEndMod datetime
DECLARE @FIVEYEARS float   
    Set @FIVEYEARS = 1825

--Get the flags from virtual_bucket settings
SELECT top 1 @DaysBack=daysback, 
             @OnlyPastDueFlag=CAST(only_pastdue AS Int),
			 @OnlyTodayFlag=CAST(only_today AS Int)
  FROM virtual_bucket 
  WHERE (manager_id = @ManagerID) and (bucket_name = @BucketName) and ([active]=1)

--This will set the range midnight today - midnight tomorrow
Set @Tomorrow= DATEADD(day, DATEDIFF(day,0,GETDATE())+1,0)
Set @Today = DATEADD(day, DATEDIFF(day,0,GETDATE()),0) 

Set @TodayStartAsDec=CAST(@Today as FLOAT) - (@FIVEYEARS - (@FIVEYEARS * @OnlyTodayFlag))
Set @TomorrowEndAsDec=CAST(@Tomorrow as FLOAT) + ((@FIVEYEARS)-(@FIVEYEARS * @OnlyTodayFlag))
                 
Set @TodayStartMod= CAST(@TodayStartAsDec as Datetime)  -- adjust dates to whether flag is used
Set @TomorrowEndMod=CAST(@TomorrowEndAsDec as datetime)

Set @OnlyPastFlagDate = GetDate() + CAST((@FIVEYEARS - (@FIVEYEARS * @OnlyPastDueFlag)) as Datetime)

DECLARE @Co TABLE (
      CoID integer not null,
      CompanyStr varchar(100) not null
	  )

INSERT into @Co
  SELECT id, val
  FROM virtual_bucket v22
  CROSS APPLY split_del_string(v22.cofilter, '|')
  WHERE v22.manager_id = @ManagerID and v22.bucket_name = @bucketname
  

DECLARE @C1 varchar(100)
DECLARE @C2 varchar(100)
DECLARE @C3 varchar(100)
DECLARE @C4 varchar(100)
DECLARE @C5 varchar(100)
DECLARE @C6 varchar(100)
DECLARE @C7 varchar(100)
DECLARE @C8 varchar(100)
DECLARE @C9 varchar(100)
DECLARE @C10 varchar(100)

SET @C1 = (select CompanyStr from @Co where COId = 1)
SET @C2 = (select CompanyStr from @Co where COId = 2)
SET @C3 = (select CompanyStr from @Co where COId = 3)
SET @C4 = (select CompanyStr from @Co where COId = 4)
SET @C5 = (select CompanyStr from @Co where COId = 5)
SET @C6 = (select CompanyStr from @Co where COId = 6)
SET @C7 = (select CompanyStr from @Co where COId = 7)
SET @C8 = (select CompanyStr from @Co where COId = 8)
SET @C9 = (select CompanyStr from @Co where COId = 9)
SET @C10 = (select CompanyStr from @Co where COId = 10)

DECLARE @DateUsed datetime
SET @DateUsed = GetDate()

DECLARE @virtualbuckettable TABLE (    
    ticket_id integer not null, 
    ticket_number varchar(20) not null,
	emp_id integer not null,
	short_name varchar(30) null,
    kind varchar(20) not null,
    due_date datetime null,
	work_address_number varchar(10) null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null,
    work_type varchar(90) null,
    legal_good_thru datetime null, 
    map_page varchar(20) null, 
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null,     
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    --points decimal(6,2) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null,
    --est_start_time datetime null,
    area_name varchar(40),
    route_order decimal(9,5),  
    lat decimal(9,5),
    lon decimal(9,5),
    work_state varchar(2),
	company varchar(80),   --QM-325
	con_name varchar(50),  --QM-325
	util varchar(150), --QM-306
	risk varchar(100),  --QM-386  (source + type)
	age decimal(6,2), --QM-551
	age_hours decimal(6,2), --QM-551
	days_overdue integer --QM-551
	); 



insert into @virtualbuckettable
  select distinct
    ticket.ticket_id, 
	ticket.ticket_number,
	loc.assigned_to,
	emp.short_name, 
    ticket.kind, 
	ticket.due_date,
	ticket.work_address_number,
    ticket.work_address_street, 
	ticket.work_description, 
	ticket.work_type,
    ticket.legal_good_thru, 
	ticket.map_page, 
	ticket.ticket_type, 
	ticket.work_city,
    ticket.ticket_format, 	 
	ticket.work_county,
    ticket.do_not_mark_before, 
	ticket.modified_date, 
	ticket.alert,
    null as scheduled, 
	null as start_date,  
	null as end_date,  --null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(substring(r.description,0,20), 'Normal') as work_priority, 
    case when
     -- @limit < 1 or
	  (ticket.kind = 'EMERGENCY') 
	    or (ticket.Kind = 'NORMAL')
	    or (ticket.kind = 'ONGOING') 
		or (r.code='Release') then 'Y'
      else 'N'
    end as ticket_is_visible, 
	a.area_name,
	ticket.route_order, 
	ticket.work_lat, 
	ticket.work_long, 
	ticket.work_state,
	ticket.company, ticket.con_name,  --QM-325

-----------Utility Icons (All Utils for All locators)-------------------
  --  CASE
  --    WHEN @toplevel = 0 THEN   --Not top level node
		-- (SELECT STRING_AGG(c.Utility_type, ', ') as u
  --        FROM locate l1 , client c WITH (NOLOCK)
  --        WHERE (l1.ticket_id = ticket.ticket_Id)
	 --     AND (c.client_id = l1.client_id)	
	 --     And (l1.client_id is not null))              --QM-357 effectively removes -Ns  SR
	 --ELSE ' '				 
  --   END,
  ----------- Do not remove: this is used for local testing for Utility Icons ---------
               CASE
                  WHEN @toplevel = 0 THEN   --Not top level node
				    isnull(STUFF 
                    ((
                      SELECT distinct ', ' + c.Utility_type
                      FROM client c, locate l WITH (NOLOCK)
                      WHERE (c.client_id = l.client_id)
	                  And (l.ticket_id = ticket.ticket_Id)
	                  And l.client_id is not null
                      FOR XML PATH('')), 1, 1, ' '), '-') 				    
				 ELSE ' '				 
			END,
-------------------------------------------
 (select dbo.get_latest_risk_score(ticket.ticket_id)),
 -------------------------------------------
    (select datediff(hour, ticket.transmit_date, GetDate())/60.0), --QM-551 age
    (select datediff(minute, ticket.transmit_date, GetDate())/60.0),  --QM-551 
    (select datediff(day, ticket.due_date, GetDate()))  --QM-551 days overdue


  FROM ticket
  inner join [locate] loc
    on ticket.ticket_id = loc.ticket_id
  inner join (select h_emp_id from dbo.get_hier(@ManagerID,0)) mgremps
    on mgremps.h_emp_id = loc.assigned_to
  left join employee emp
    on loc.assigned_to = emp.emp_id
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  WHERE (ticket.transmit_date > (@DateUsed - @DaysBack))  --Get tickets this far back
   AND
     (((due_date >=@TodayStartMod) AND (due_date < @TomorrowEndMod)) AND
	    (due_date < @OnlyPastFlagDate)
	 )
   AND 
		 (
		     (@C1  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C1 + '[^a-z]%')))
		  OR (@C2  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C2 + '[^a-z]%')))
          OR (@C3  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C3 + '[^a-z]%')))
          OR (@C4  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C4 + '[^a-z]%')))
		  OR (@C5  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C5 + '[^a-z]%')))
		  OR (@C6  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C6 + '[^a-z]%')))
		  OR (@C7  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C7 + '[^a-z]%')))
		  OR (@C8  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C8 + '[^a-z]%')))
		  OR (@C9  IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C9 + '[^a-z]%')))
		  OR (@C10 IS NOT NULL AND ('.' + ticket.company + '.' like ('%[^a-z]' + @C10 + '[^a-z]%')))
		)
  
  -- RETURN as virtualbucket so that we can ID it in QManager client
   select 'virtualbucket' as tname, * from @virtualbuckettable virtualbucket order by company, due_date, short_name

-----------------------------------------------
--  @ManagerID Int,              * Where we are on the tree
--  @DaysBack Int,               * Number of days we are going back (may change this to a range)
--  @BucketName varchar(15),     * Name of the Bucket that we will show on the screen
--  @CriteriaStr varchar(MAX),   * String of criteria (delimeter is a '|')  for the company match
--  @toplevel bit = 0            * Not used yet

   --TEST FILTER VALUES
 --  select (@DateUsed - @DaysBack) as StartDate, @DateUsed as EnteredDate, @DaysBack as NumOfDays,
 --         @C1 as C1, @C2 as C2, @C3 as C3, @C4 as C4, @C5 as C5, 
 --         @C6 as C6, @C7 as C7, @C8 as C8, @C9 as C9, @C10 as C10


--exec get_virtual_bucket_tickets 3834, 25, 'Random Bucket', 'A&N|VERIZON|ESVABA|'
--exec get_virtual_bucket_tickets 3834, 'Random Bucket'
--select * from virtual_bucket





 
