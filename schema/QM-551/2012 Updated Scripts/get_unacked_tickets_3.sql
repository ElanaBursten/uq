--QM-551 Age and Days Overdue 
--QM-387 Risk Score
--QM-368 Update STRING_AGG
--QM-353 Remove utils from top nodes
--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)
if object_id('dbo.get_unacked_tickets_3') is not null
  drop procedure dbo.get_unacked_tickets_3
go	


CREATE procedure [dbo].[get_unacked_tickets_3] 
  (@mgr_id int,
   @toplevel bit = 0)
as
  set nocount on

  select 'ticket' as tname, ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date, null as end_date, null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    null as est_start_time, 
	a.area_name,
	ticket.route_order,
	ticket.work_lat lat,
	ticket.work_long lon,
	ticket.work_state,
	ticket.company, ticket.con_name,  --QM-325
	
-----------STRING_AGG Utility Icons (All Locators)-------------------
--    CASE
--      WHEN @toplevel = 0 THEN   --Not top level node
--		 (SELECT STRING_AGG(c.Utility_type, ', ') as u
--          FROM locate l1 , client c WITH (NOLOCK)
--          WHERE (l1.ticket_id = ticket.ticket_Id)
--	      AND (c.client_id = l1.client_id)	
--	      And (l1.client_id is not null))              --QM-357 effectively removes -Ns  SR
--	 ELSE ' '				 
--     END as Util,
---------------------------------------------
	
----------- Test Utility Icons (do not remove)-------------------
             CASE
                  WHEN @toplevel = 0 THEN   --Not top level node
				    isnull(STUFF 
                    ((
                      SELECT distinct ', ' + c.Utility_type
                      FROM client c, locate l WITH (NOLOCK)
                      WHERE (c.client_id = l.client_id)
	                  And (l.ticket_id = ticket.ticket_Id)
	                  And l.client_id is not null  
                      FOR XML PATH('')), 1, 1, ' '), '-') 				    
				 ELSE ' '			 
			END  as util,
-------------------------------------------
 (select dbo.get_latest_risk_score(ticket.ticket_id)) as risk,
 -------------------------------------------
 (select dbo.get_locator_name_for_ticket(ticket.ticket_id)) as short_name,
 cast((select datediff(hour, ticket.transmit_date, GetDate())/60.0) AS DECIMAL(6, 2)) as age, --QM-551 age
 cast((select datediff(minute, ticket.transmit_date, GetDate())/60.0 )AS DECIMAL(6, 2)) as age_hours,  --QM-551 
 (select datediff(day, ticket.due_date, GetDate())) as days_overdue  --QM-551 days overdue

  from ticket
  left join reference r on r.ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id
    from ticket_ack
    inner join (select h_emp_id from dbo.get_hier(@mgr_id,0)) mgremps
      on mgremps.h_emp_id = ticket_ack.locator_emp_id
    where ticket_ack.has_ack = 0 )



	--get_unacked_tickets_3 1005

	--select * from employee where Last_name = 'Havery' 3834

GO


