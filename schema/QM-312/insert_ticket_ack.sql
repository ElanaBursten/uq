--QM-312 Ticket Ack - Fixes when a ticket has been acknowledged, still open and a new locate comes in.
--Changed 1/8/2020 EB
if object_id('dbo.insert_ticket_ack') is not null
  drop procedure dbo.insert_ticket_ack
go

CREATE PROCEDURE insert_ticket_ack (
  @TicketID int
)
AS

-- check if this ticket_id already exists
declare @Count int
select @Count = count(ticket_id) from ticket_ack where ticket_id = @TicketId

-- If it exists, get the highest version
declare @TicketAckVersionID int
if (@Count > 0)
begin
  SELECT top 1 @TicketAckVersionID = ticket_version_id
  from ticket_ack where ticket_id = @TicketId
end

-- also check if there are record with has_ack=0
declare @CountUnacked int
select @CountUnacked = count(ticket_id) from ticket_ack
  where ticket_id = @TicketId and has_ack=0

-- determine the ticket_version_id
declare @TicketVersionID int
SELECT top 1 @TicketVersionID = ticket_version_id
FROM ticket_version
INNER JOIN ticket
ON ticket.ticket_id = ticket_version.ticket_id and 
ticket_version.transmit_date = ticket.transmit_date and
ticket.ticket_id = @TicketId
order by ticket_version.transmit_date DESC

 -- try to determine the locator associated with this ticket
 -- (moved to caputre Locator)
  declare @LocatorId int
  select @LocatorId = max(locator_id) from locate l
  left outer join assignment a on l.locate_id = a.locate_id
  where l.ticket_id = @TicketId
  and l.status <> '-N'
  and a.active = 1
  and l.active = 1
  group by locator_id

-- if there is no record, we will insert it
-- if there already is a record, but it has has_ack=0, we will update it
-- existing records with has_ack=1 will *NOT* be updated.
if (@Count = 0) or (@CountUnacked > 0)
begin
  -- insert or update:  
  if @Count = 0
  begin
    -- insert new record
    insert ticket_ack
    (ticket_id, ticket_version_id, locator_emp_id)
    values (@TicketId, @TicketVersionID, @LocatorId)
  end else 
  begin
    -- update existing record
    if @CountUnacked > 0
    begin
      update ticket_ack
      set locator_emp_id=@LocatorId, ticket_version_id = @TicketVersionID
      where ticket_id = @TicketId and ticket_version_id = @TicketAckVersionID or
            ticket_id = @TicketId and ticket_version_id IS NULL
    end
  end

end 
else if (@Count > 0) and (@CountUnacked = 0) 
  begin
-- There are already records and they have all been acked   
    insert ticket_ack
    (ticket_id, ticket_version_id, locator_emp_id)
    values (@TicketId, @TicketVersionID, @LocatorId)
  end
GO

grant execute on insert_ticket_ack to uqweb, QManagerRole