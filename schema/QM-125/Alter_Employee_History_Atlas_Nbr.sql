Use QM

/*
   Sunday, March 15, 20206:49:36 PM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.employee_history ADD
	atlas_number varchar(20) NULL
GO
ALTER TABLE dbo.employee_history SET (LOCK_ESCALATION = TABLE)   --QM-125
GO
COMMIT
select Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'CONTROL') as Contr_Per 