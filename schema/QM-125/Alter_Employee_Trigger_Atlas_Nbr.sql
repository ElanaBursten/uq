USE [QM]
GO
/****** Object:  Trigger [dbo].[employee_iu]    Script Date: 3/15/2020 6:58:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER trigger [dbo].[employee_iu] on [dbo].[employee]   --QM-125         
for insert, update  
not for replication  
as  
begin  
  set nocount on  
  
  declare @ChangeDate datetime  
  set @ChangeDate = getdate()  
  
  -- Don't update modified date or add history for rights_midified_date changes alone  
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)  
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)  
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)  
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)  
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)  
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)  
    or update(incentive_pay) or update(contact_phone) or update(local_utc_bias) or update(ad_username) or update(test_com_login) 
	or update(adp_login) or update(home_lat) or update(home_lng) or update(alt_lat) or update(alt_lng) or update(atlas_number)))  --QM-125   
  begin  
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)  
  
    if update(report_to) or update(repr_pc_code)  
    begin -- This change may modify the effective PC of non-updated employees  
      -- Recalculate and store any changes to everyone below these employees  
  
      declare @AllEmps TABLE (  
        emp_id integer NOT NULL PRIMARY KEY,  
        x integer,  
        emp_pc_code varchar(15) NULL  
      )  
  
      insert into @AllEmps select * from employee_pc_data(null)  
  
      declare @ChangedEmps TABLE (  
        emp_id integer NOT NULL PRIMARY KEY  
      )  
  
      insert into @ChangedEmps  
        select emps.emp_id  
        from @AllEmps emps  
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate  
        where ((emps.emp_pc_code <> eh.active_pc)  
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))  
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))  
          or emps.emp_id in (select emp_id from inserted)  
  
      -- "Deactivate" any current history records  
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)  
  
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      )  
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
        home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number   --QM-125   
	  from employee e  
        inner join @AllEmps emps on emps.emp_id = e.emp_id  
      where e.emp_id in (select emp_id from @ChangedEmps)  
    end  
    else -- This change only affects the exact employee(s) being modified  
    begin  
      -- "Deactivate" any current history records  
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)  
  
      -- Record the new/current employee data  
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      )  
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets,  
        incentive_pay, contact_phone, local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      from inserted  
    end  
  end  
end  
