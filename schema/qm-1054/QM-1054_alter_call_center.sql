/*
   Monday, January 06, 202510:35:41 AM
   User: 
   Server: BRIAN-PC
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.call_center ADD
	emergency_requeue_email varchar(250) NULL
GO
ALTER TABLE dbo.call_center SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.call_center', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.call_center', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.call_center', 'Object', 'CONTROL') as Contr_Per 