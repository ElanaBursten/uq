--get_ticket5 is the ON DEMAND pull of a ticket (as opposed the sync_6 which syncs it for the locator to DBISAM)
--QM-1037 BigFoot: Using ESketch Footage. 
--QM-387 Ticket Risk (Add ticket risk when retrieving ticket for Work Management)

if object_id('dbo.get_ticket5') is not null
  drop procedure dbo.get_ticket5
go


create proc [dbo].[get_ticket5](@TicketID int, @UpdatesSince datetime)
as
  set nocount on

  select 'ticket' as tname, ticket.*, coalesce(area.area_name, '') as route_area_name,
  (dbo.get_latest_risk_score(ticket.ticket_id)) as risk_score
  from ticket
  left join area on area.area_id = ticket.route_area_id
    where ticket.ticket_id=@TicketID
	 
  select *,
  cast(dbo.get_ticket_workload_date(ticket_id, locator_id) as datetime) as workload_date
  from (
    select 'locate' as tname, 
	--  locate.*,
	locate.locate_id, locate.ticket_id, locate.client_code, locate.client_id, locate.status, locate.high_profile, 
    
     locate.price, locate.closed, locate.closed_by_id, locate.closed_how, locate.closed_date, locate.modified_date, locate.active,
     locate.invoiced, locate.assigned_to, locate.regular_hours, locate.overtime_hours, locate.added_by, locate.watch_and_protect,
     locate.high_profile_reason, locate.seq_number, locate.assigned_to_id, locate.mark_type, locate.alert,    
     (select max(assignment.locator_id) from assignment
          where locate.locate_id=assignment.locate_id and assignment.active=1
     ) as locator_id,
	  locate.entry_date, locate.gps_id, locate.status_changed_by_id, 
	   (Select dbo.get_qty_marked_total(locate.locate_id, 0)) as qty_marked,
	   (Select dbo.get_length_total(locate.locate_id, 0)) as length_total,
	   (Select dbo.get_length_total(locate.locate_id, 1)) as esketch_length_total,
	   (Select dbo.get_qty_marked_total(locate.locate_id, 1))as esketch_qty_marked
    from locate
    where locate.ticket_id=@TicketID
      and (locate.status <> '-N')
      and (locate.status <> '-P')) l

  select 'response_log' as tname,
    response_log.response_id,
    response_log.locate_id,
    response_log.response_date,
    response_log.call_center,
    response_log.status,
    response_log.response_sent,
    response_log.success,
    -- work around for junk data issue:
    case when (call_center='FMB1' or call_center='FMW1') and reply like '%N' then '(hidden)'
         else response_log.reply end as reply
  from locate
    inner join response_log with (index(response_log_locate_id))
      on response_log.locate_id = locate.locate_id
  where locate.ticket_id = @TicketID

  select 'notes' as tname, notes.*
   from notes
   where notes.foreign_type = 1       -- 1 = ticket notes
     and notes.foreign_id = @TicketID -- foreign_id has the ticket ID
     and notes.active = 1             -- get only active ticket notes
  union
  select 'notes' as tname, notes.*
   from notes inner join locate on (notes.foreign_id = locate.locate_id)
   where notes.foreign_type = 5        -- 5 = locate notes
     and locate.ticket_id = @TicketID
     and notes.active = 1

  select 'locate_hours' as tname, locate_hours.*
  from locate_hours with (index(locate_hours_locate_id))
    where locate_id in
      (select locate_id from locate where locate.ticket_id=@TicketID)

  select 'attachment' as tname, attachment.*
  from attachment
  where attachment.foreign_id = @TicketID
    and attachment.foreign_type = 1
    and attachment.active = 1

  select 'locate_plat' as tname, locate_plat.*
    from locate_plat
   where locate_id in (select locate_id from locate where ticket_id = @TicketID)

  select 'ticket_version' as tname,
     ticket_version_id, ticket_id, ticket_revision, ticket_number, ticket_type, transmit_date,
     processed_date, serial_number, arrival_date, filename, ticket_format, source
  from ticket_version
  where ticket_id = @TicketID

  select 'jobsite_arrival' as tname, jobsite_arrival.*
    from jobsite_arrival
    where ticket_id = @TicketID
      and active = 1

  select 'locate_facility' as tname, locate_facility.*
    from locate_facility
   where locate_id in (select locate_id from locate where ticket_id = @TicketID)

  select 'client' as tname, client.*
    from client
   where modified_date > @UpdatesSince

  select 'call_center' as tname, call_center.*
    from call_center
   where modified_date > @UpdatesSince

  select 'employee' as tname, employee.*
    from employee
   where modified_date > @UpdatesSince

  select 'reference' as tname, reference.*
    from reference
   where modified_date > @UpdatesSince

  select 'statuslist' as tname, statuslist.*
    from statuslist
   where modified_date > @UpdatesSince

  select 'status_group' as tname, status_group.*
    from status_group
   where modified_date > @UpdatesSince

  select 'status_group_item' as tname, status_group_item.*
    from status_group_item
   where modified_date > @UpdatesSince
GO

--exec dbo.get_ticket5 1029, '2024-11-17 00:00:00.000'