--QM-1037 BigFoot: Using ESketch Footage.  
  --Function will sum up the QTY MARKED for either 
  --       ESketch (Bigfoot = 1)
  --       Locator entered (Bigfoot = 0)
  -- The last entered qty_marked value in locate is generated by the last entered lengths (either ESketch or the Locator)
  -- Therefore, we need a way to find the original values to display


if object_id('dbo.get_qty_marked_total') is not null
  drop function dbo.get_qty_marked_total
go

Create function dbo.get_qty_marked_total(@locate_id int, @isbigfoot bit) 
returns int 

as begin
    declare @QtyMarked int
	declare @TotalLength int
	declare @UnitConversionID int
	declare @UnitType varchar(10)
	declare @FirstUnitFactor int
	declare @RestUnitFactor int

--	Get the currency conversion for this locate
	select @UnitConversionID = c.unit_conversion_id, 
	       @UnitType = uc.unit_type, 
           @FirstUnitFactor = uc.first_unit_factor, 
		   @RestUnitFactor = uc.rest_units_factor 
    from client c join locate l on l.client_id = c.client_id
    join billing_unit_conversion uc on uc.unit_conversion_id = c.unit_conversion_id 
    where l.locate_id = @locate_id and Coalesce(uc.first_unit_factor, 0) > 0

-- Get Length for either ESketch or from locator
  if @isbigfoot = 0
    SELECT @TotalLength = SUM(Units_marked)
    FROM locate_hours
    WHERE locate_id = @locate_ID
	and ((bigfoot = @isbigfoot) or (bigfoot is NULL))

  else
     set @TotalLength = (select sum(lh.units_marked) as sum_units_marked
            from locate_hours lh
            where (locate_id = @locate_id) 
            and (bigfoot = @isbigfoot)
           )

IF @UnitConversionID = 38014 or @UnitConversionID is NULL --special rule because of weird entry
    BEGIN
        -- Every foot = 1 unit because @rest_units_factor is NULL
        SET @QtyMarked = 1;--@total_marked;
    END
    ELSE
    BEGIN
        IF @TotalLength <= @FirstUnitFactor
            SET @QtyMarked = 1;
        ELSE
            SET @QtyMarked = 1 + CEILING((@TotalLength - @FirstUnitFactor) / @RestUnitFactor);
    END


 return @QtyMarked
end
Go

  --Select dbo.get_length_total(32565, 0)
  --Select dbo.get_qty_marked_total(32565,0)
  --Select dbo.get_qty_marked_total(20169, 1)
--select * from locate_hours where locate_id = 32565

--Select * from locate where locate_id = 32565


