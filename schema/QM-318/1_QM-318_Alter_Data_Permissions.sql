--QM-318 Permissions for Today, Open & Past Due
--     Used to control the version of multi_open_totals6 begin used.  In order: 
--          * MultiOpenTotals6 - Does not include any of the buckets (Today, Open or Past Due)
--          * MultiOpenTotals6PD - Past Due bucket only
--          * MultiOpenTotals6OP - Open & Past Due  
--          * MultiOpenTotals6TD - Today, Open & Past Due

--Update Old entries that have been changed
--update right_definition 
--set entity_data = 'TicketsTodayOpenPastDueTree', right_description = 'Tickets - Show Today, Open, Past Due' 
--where entity_data = 'TicketsTodayPastDueTree'

if not exists(select * from right_definition where entity_data='TicketsTodayOpenPastDueTree')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - Show Today, Open, Past Due', 'General', 'TicketsTodayOpenPastDueTree', 'Limitation does not apply to this right', GetDate());
GO

--select * from right_definition order by right_description

