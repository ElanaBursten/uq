-- Function: get_latest_risk_score
-- QM-729 Added Nolock and consolidated
-- EB
--------------------------------------


if object_id('dbo.get_latest_risk_score') is not null
  drop function dbo.get_latest_risk_score
go


create function dbo.get_latest_risk_score(@ticket_id int)
returns varchar(100)
as
begin
  return lTrim(coalesce((select top 1 concat( 
       tr.risk_source, '-', tr.risk_score_type, ' : ', tr.risk_score) 
  from ticket_risk tr with (nolock) 
  where (tr.ticket_id = @ticket_id)
  order by tr.ticket_version_id desc), '--'))

end	
GO

--select dbo.get_latest_risk_score(10521) as risk_score_text


