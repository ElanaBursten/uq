USE QM;
Insert into customer
([customer_name],[customer_description],[customer_number],[city],[state],[zip],[street],[phone]
           ,[locating_company],[modified_date],[active],[street_2],[attention],[contact_name]
           ,[contact_email],[period_type],[pc_code],[contract],[contract_review_due_date]
           ,[customer_number_pc],[damage_cap])
values('NORTHWEST NATURAL GAS (PORT)','Northwest Natural Gas|NNG-HP','NNGOR40301','PORTLAND', 'OR', '97208-4709','PO BOX 4709', '503-226-4211',		
7,	GetDate(), 1,	NULL,'PURCHASING',	NULL,NULL,'WEEK',403, 'LOC-13',NULL,NULL,NULL)

GO


update client         --update Oregon to new HP customer
set customer_id = (select customer_id
					from customer
					where [customer_description] = 'Northwest Natural Gas|NNG-HP' and [customer_number]='NNGOR40301')  -- OR
where client_id = 4866

GO

Insert into customer
([customer_name],[customer_description],[customer_number],[city],[state],[zip],[street],[phone]
           ,[locating_company],[modified_date],[active],[street_2],[attention],[contact_name]
           ,[contact_email],[period_type],[pc_code],[contract],[contract_review_due_date]
           ,[customer_number_pc],[damage_cap])
values('NORTHWEST NATURAL GAS (WASH)','Northwest Natural Gas|NNG-HP','NNGWA40301','PORTLAND', 'OR', '97208-4709','PO BOX 4709', '503-226-4211',		
7,	GetDate(), 1,	NULL,'PURCHASING',	NULL,NULL,'WEEK',403, 'LOC-13',NULL,NULL,NULL)

GO

update client         --update Wash to new HP customer
set customer_id = (select customer_id
					from customer
					where [customer_description] = 'Northwest Natural Gas|NNG-HP' and [customer_number]='NNGWA40301')  -- WA
where client_id = 4868

GO

--**** UnComment below to verify*****
--select *
--from customer
--where [customer_number]='NNGWA40301'
--or [customer_number]='NNGOR40301'

--Select *
--from Client
--where client_id in (4866,4868)