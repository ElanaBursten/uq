USE [QM]
GO

/****** Object:  Table [dbo].[rabbitmq_staging_rtime]    Script Date: 6/1/2023 5:00:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rabbitmq_staging_rtime](
	[rabbitmq_staging_id] [int] IDENTITY(1,1) NOT NULL,
	[emitting_context] [varchar](50) NULL,
	[event_type] [varchar](20) NULL,
	[workernumber] [varchar](20) NULL,
	[createdbyworkernumber] [varchar](20) NULL,
	[workerstatus] [varchar](20) NULL,
	[workerstatuscode] [int] NULL,
	[company] [varchar](20) NULL,
	[eventtime] [datetime] NULL,
	[latitude] [varchar](20) NULL,
	[longitude] [varchar](20) NULL,
	[worker_company] [varchar](50) NULL,
	[eventtypecode] [int] NULL,
	[exchange] [varchar](20) NULL,
	[routing_key] [varchar](50) NULL,
	[eventid] [int] NULL,
	[parenteventid] [int] NULL,
	[empid] [int] NULL,
	[source] [nchar](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


