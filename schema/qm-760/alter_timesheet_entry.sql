/*
   Friday, May 19, 20236:44:14 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.timesheet_entry ADD
	Session int NOT NULL CONSTRAINT DF_timesheet_entry_Session DEFAULT 0,
	EventType varchar(10) NULL
GO
ALTER TABLE dbo.timesheet_entry SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.timesheet_entry', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.timesheet_entry', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.timesheet_entry', 'Object', 'CONTROL') as Contr_Per 