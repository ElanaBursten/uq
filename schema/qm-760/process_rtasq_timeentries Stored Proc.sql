USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[process_rtasq_timeentries]    Script Date: 7/17/2023 10:09:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[process_rtasq_timeentries]      --qm-760
AS      
BEGIN 

set Ansi_Warnings Off

-------move rows prior to 90 days to history table

INSERT INTO [dbo].[rabbitmq_staging_rtime_history]
(rabbitmq_staging_id,[emitting_context],[event_type],[workernumber],[createdbyworkernumber],[workerstatus],[workerstatuscode],[company],[eventtime],[latitude],[longitude],[worker_company],[eventtypecode]
,[exchange],[routing_key],[eventid],[parenteventid],[empid],[source],[processed],[process_status])
select rabbitmq_staging_id,[emitting_context],[event_type],[workernumber],[createdbyworkernumber],[workerstatus],[workerstatuscode],[company],[eventtime],[latitude],[longitude],[worker_company],[eventtypecode]
,[exchange],[routing_key],[eventid],[parenteventid],[empid],[source],[processed],[process_status] 
from rabbitmq_staging_rtime 
where convert(varchar,eventtime,112) <= convert(varchar,getdate()-90,112)

Delete from rabbitmq_staging_rtime 
where convert(varchar,eventtime,112) <= convert(varchar,getdate()-90,112)

--------Get all the non processed rows into #rmq_Staging
select * into #rmq_Staging from rabbitmq_staging_rtime 	where processed is null

-------For distinct workers for each day get all the events (processed and non processed) into #rmq_Staging_1
select distinct  r.workernumber,r.createdbyworkernumber,r.eventtime,r.parenteventid into #rmq_Staging_1 
from rabbitmq_staging_rtime r
inner join ( select distinct workernumber,convert(varchar,eventtime,112) workdate,parenteventid from #rmq_Staging
)t on t.workernumber = r.workernumber and (t.workdate = convert(varchar,r.eventtime,112) OR t.parenteventid =r.parenteventid)

--------Get all the parentevent Ids that cross over days into #pid_twodates
select parenteventId  into #pid_twodates
from #rmq_Staging_1
group by parenteventid
having count(distinct convert(varchar,eventtime,112)) > 1

---------Check if the last event is OnClock or first event is On_break or Off_Clock. Add 12AM additional time entry to separate the days
;with newentries as (select workernumber,createdbyworkernumber,parenteventid, case when rnd =1 and workerstatus ='On_Clock' then convert(datetime,convert(varchar(10),eventtime,121)+' 23:59:00.000') 
	when rst=1 and workerstatus in ('On_Break','OFF_CLOCK') then convert(datetime,convert(varchar(10),eventtime,121)+' 00:00:01.000') end eventtime
	from (
	select r.workernumber,r.createdbyworkernumber,eventtime,r.parenteventid,r.eventid,r.event_type,r.workerstatus,ROW_NUMBER() Over(partition by convert(varchar,r.eventtime,112),r.parenteventid order by eventtime desc)rnd
	,ROW_NUMBER() Over(partition by convert(varchar,r.eventtime,112),r.parenteventid order by eventtime) rst
	from #rmq_Staging r
	inner join #pid_twodates p on p.parenteventid = r.parenteventid
	)t
	where (rnd =1 and workerstatus ='On_Clock') OR (rst=1 and workerstatus in ('On_Break','OFF_CLOCK') )
)
insert into #rmq_Staging_1 (workernumber,createdbyworkernumber,parenteventId , eventtime )
select workernumber,createdbyworkernumber,parenteventId , eventtime from newentries


------Get the other timesheet entry data from employee_history table
create table #te_otherdata  (work_emp_id int,work_date datetime,modified_date datetime,[status] varchar(8),entry_date datetime,entry_date_local datetime,entry_by int, emp_type_id int, work_pc_code varchar(15))
insert into #te_otherdata (work_emp_id,work_date ,modified_date ,[status] ,entry_date ,entry_date_local,entry_by, emp_type_id, work_pc_code)
select distinct e.emp_id as work_emp_id,convert(datetime,convert(varchar,r.eventtime,101)) work_date, getdate() as modified_date,'ACTIVE' as [status], getdate() as entry_date,getdate() as entry_date_local
,case when cbwn.emp_id is null then e.emp_id else cbwn.emp_id end as entry_by
,case when eh.type_id is null then e.type_id else eh.type_id end emp_type_id
,case when eh.active_pc is null then dbo.get_employee_pc(eh.emp_id,1) else eh.active_pc end Work_pc_code
from #rmq_Staging_1 r
inner join employee e with(nolock) on right('000000'+isnull(e.emp_number,''),6) = Right('000000'+isnull(r.workernumber,''),6) and right('000000'+isnull(e.adp_login,''),6) = Right('000000'+isnull(r.workernumber,''),6)
	and e.active =1
inner join employee_history eh with(nolock) on e.emp_id =eh.emp_id and convert(datetime,convert(varchar,r.eventtime,101)) >= eh.active_start and convert(datetime,convert(varchar,r.eventtime,101)) < eh.active_end 
	 and eh.active =1
left join employee cbwn with(nolock) on isnull(cbwn.adp_login,'') = r.createdbyworkernumber and cbwn.active =1
order by work_emp_id,work_date


-----get the time entries for the non processed rows from #rmq_Staging_1
create table #results (emp_id int,work_date datetime,[Work_Start1] datetime,[Work_Stop1] datetime,[Work_Start2] datetime,[Work_Stop2] datetime,[Work_Start3] datetime,[Work_Stop3] datetime,[Work_Start4] datetime,[Work_Stop4] datetime,[Work_Start5] datetime,[Work_Stop5] datetime,[Work_Start6] datetime,[Work_Stop6] datetime,[Work_Start7] datetime,[Work_Stop7] datetime,[Work_Start8] datetime,[Work_Stop8] datetime,[Work_Start9]
			 datetime,[Work_Stop9] datetime,[Work_Start10] datetime,[Work_Stop10] datetime,[Work_Start11] datetime,[Work_Stop11] datetime,[Work_Start12] datetime,[Work_Stop12] datetime,[Work_Start13] datetime,[Work_Stop13] datetime,[Work_Start14] datetime,[Work_Stop14] datetime,[Work_Start15] datetime,[Work_Stop15] datetime
			 ,[Work_Start16] datetime,[Work_Stop16] datetime,reg_hours decimal(10,3))

insert into #results (emp_id,work_date,[Work_Start1],[Work_Stop1],[Work_Start2],[Work_Stop2],[Work_Start3],[Work_Stop3],[Work_Start4],[Work_Stop4],[Work_Start5],[Work_Stop5],[Work_Start6],[Work_Stop6],[Work_Start7],[Work_Stop7],[Work_Start8],[Work_Stop8],[Work_Start9]
			,[Work_Stop9],[Work_Start10],[Work_Stop10],[Work_Start11],[Work_Stop11],[Work_Start12],[Work_Stop12],[Work_Start13],[Work_Stop13],[Work_Start14],[Work_Stop14],[Work_Start15],[Work_Stop15],[Work_Start16],[Work_Stop16],reg_hours)
		select empId,workDate,MAX([Work_Start1]),MAX([Work_Stop1]),MAX([Work_Start2]),MAX([Work_Stop2]),MAX([Work_Start3]),MAX([Work_Stop3]),MAX([Work_Start4]),MAX([Work_Stop4]),MAX([Work_Start5]),MAX([Work_Stop5]),MAX([Work_Start6]),MAX([Work_Stop6]),MAX([Work_Start7]),MAX([Work_Stop7]),MAX([Work_Start8]),MAX([Work_Stop8])
			,MAX([Work_Start9]),MAX([Work_Stop9]),MAX([Work_Start10]),MAX([Work_Stop10]),MAX([Work_Start11]),MAX([Work_Stop11]),MAX([Work_Start12]),MAX([Work_Stop12]),MAX([Work_Start13]),MAX([Work_Stop13]),MAX([Work_Start14]),MAX([Work_Stop14]),MAX([Work_Start15]),MAX([Work_Stop15]),MAX([Work_Start16]),MAX([Work_Stop16])
			,cast(ISNULL(datediff(second,max([Work_Start1]),max([Work_Stop1])),0) +ISNULL(datediff(second,max([Work_Start2]),max([Work_Stop2])),0) +ISNULL(datediff(second,max([Work_Start3]),max([Work_Stop3])),0) +ISNULL(datediff(second,max([Work_Start4]),max([Work_Stop4])),0)
				+ISNULL(datediff(second,max([Work_Start5]),max([Work_Stop5])),0) +ISNULL(datediff(second,max([Work_Start6]),max([Work_Stop6])),0) +ISNULL(datediff(second,max([Work_Start7]),max([Work_Stop7])),0) +ISNULL(datediff(second,max([Work_Start8]),max([Work_Stop8])),0)
				+ISNULL(datediff(second,max([Work_Start9]),max([Work_Stop9])),0) +ISNULL(datediff(second,max([Work_Start10]),max([Work_Stop10])),0) +ISNULL(datediff(second,max([Work_Start11]),max([Work_Stop11])),0) +ISNULL(datediff(second,max([Work_Start12]),max([Work_Stop12])),0)
				+ISNULL(datediff(second,max([Work_Start13]),max([Work_Stop13])),0) +ISNULL(datediff(second,max([Work_Start14]),max([Work_Stop14])),0) +ISNULL(datediff(second,max([Work_Start15]),max([Work_Stop15])),0)
			+ISNULL(datediff(second,max([Work_Start16]),max([Work_Stop16])),0) as decimal(10,3)) / 3600 Reg_Hours
			from (
				select  empId, WorkDate, case when rn%2 = 0 then 'Work_Stop'+ cast(rn/2 as varchar) else 'Work_Start'+ cast((rn/2)+1 as varchar) end  Column_name,eventtime,rn
				from (	select eh.short_name,eh.emp_id empID,convert(varchar,r.eventtime,112) WorkDate,r.parenteventid,r.eventtime
						,ROW_NUMBER() over (partition by eh.emp_id,convert(varchar,r.eventtime,112) order by r.eventtime)rn
						from #rmq_Staging_1 r 			
						inner join employee_history eh with(nolock) on right('000000'+isnull(eh.emp_number,''),6) = Right('000000'+isnull(r.workernumber,''),6) and eh.active_start <= convert(datetime,convert(varchar,r.eventtime,101)) 
							and convert(datetime,convert(varchar,r.eventtime,101)) < eh.active_end and eh.active =1 									
				)r
			)sourcetable
			pivot(  max(eventtime) for Column_name in ([Work_Start1],[Work_Stop1],[Work_Start2],[Work_Stop2],[Work_Start3],[Work_Stop3],[Work_Start4],[Work_Stop4],[Work_Start5],[Work_Stop5],[Work_Start6],[Work_Stop6],[Work_Start7],[Work_Stop7],[Work_Start8],[Work_Stop8],[Work_Start9]
			,[Work_Stop9],[Work_Start10],[Work_Stop10],[Work_Start11],[Work_Stop11],[Work_Start12],[Work_Stop12],[Work_Start13],[Work_Stop13],[Work_Start14],[Work_Stop14],[Work_Start15],[Work_Stop15],[Work_Start16],[Work_Stop16])
			)pivottable
			Group by empId,workDate 

------Remove the date part from work start and stops
Update #results
set Work_Start1 = convert(datetime,'1899-12-30 '+convert(varchar,work_start1,114))
,Work_Start2 = convert(datetime,'1899-12-30 '+convert(varchar,work_start2,114))
,Work_Start3 = convert(datetime,'1899-12-30 '+convert(varchar,work_start3,114))
,Work_Start4 = convert(datetime,'1899-12-30 '+convert(varchar,work_start4,114))
,Work_Start5 = convert(datetime,'1899-12-30 '+convert(varchar,work_start5,114))
,Work_Start6 = convert(datetime,'1899-12-30 '+convert(varchar,work_start6,114))
,Work_Start7 = convert(datetime,'1899-12-30 '+convert(varchar,work_start7,114))
,Work_Start8 = convert(datetime,'1899-12-30 '+convert(varchar,work_start8,114))
,Work_Start9 = convert(datetime,'1899-12-30 '+convert(varchar,work_start9,114))
,Work_Start10 = convert(datetime,'1899-12-30 '+convert(varchar,Work_Start10,114))
,Work_Start11 = convert(datetime,'1899-12-30 '+convert(varchar,work_start11,114))
,Work_Start12 = convert(datetime,'1899-12-30 '+convert(varchar,work_start12,114))
,Work_Start13 = convert(datetime,'1899-12-30 '+convert(varchar,work_start13,114))
,Work_Start14 = convert(datetime,'1899-12-30 '+convert(varchar,work_start14,114))
,Work_Start15 = convert(datetime,'1899-12-30 '+convert(varchar,work_start15,114))
,Work_Start16 = convert(datetime,'1899-12-30 '+convert(varchar,work_start16,114))
,work_stop1 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop1,114))
,work_stop2 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop2,114))
,work_stop3 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop3,114))
,work_stop4 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop4,114))
,work_stop5 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop5,114))
,work_stop6 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop6,114))
,work_stop7 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop7,114))
,work_stop8 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop8,114))
,work_stop9 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop9,114))
,work_stop10 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop10,114))
,work_stop11 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop11,114))
,work_stop12 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop12,114))
,work_stop13 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop13,114))
,work_stop14 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop14,114))
,work_stop15 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop15,114))
,work_stop16 = convert(datetime,'1899-12-30 '+convert(varchar,work_stop16,114))


-------Insert the combined results into temp timesheet entry table #te_data
SELECT [work_emp_id],te.[work_date],[modified_date],[status],[entry_date],[entry_date_local],[entry_by],'NA' as [vehicle_use]
,[reg_hours],0 as [ot_hours],0 as [dt_hours],0 as [callout_hours],0 as [vac_hours],0 as [leave_hours],0 as [br_hours],0 as [hol_hours],0 as [jury_hours],0 as [pto_hours]
,NULL [floating_holiday],[emp_type_id],[work_pc_code],'RTasq' [source]
,[work_start1],[work_stop1],[work_start2],[work_stop2],[work_start3],[work_stop3],[work_start4],[work_stop4],[work_start5],[work_stop5]
,[work_start6],[work_stop6],[work_start7],[work_stop7],[work_start8],[work_stop8],[work_start9],[work_stop9],[work_start10],[work_stop10],[work_start11],[work_stop11],[work_start12],[work_stop12]
,[work_start13],[work_stop13],[work_start14],[work_stop14],[work_start15],[work_stop15],[work_start16],[work_stop16] into #te_data
from #te_otherdata te
inner join #results r on r.emp_id = te.work_emp_id and r.work_date = te.work_date
order by work_emp_id,work_date

------Delete rows from #te_data if it already exists in timesheet_entry table and there is no change
delete ted 
from #te_data ted
inner join timesheet_entry te with(nolock) on ted.work_emp_id =te.work_emp_id and ted.work_date =te.work_date and te.status ='ACTIVE' --and te.source = ted.source and te.source ='RTasq'
where cast(ted.reg_hours as decimal(5,2)) = te.reg_hours
and ISNULL(ted.Work_Start1,'') = ISNULL(te.work_start1,'')
and ISNULL(ted.work_stop1,'') = ISNULL(te.work_stop1,'')
and ISNULL(ted.Work_Start2,'') = ISNULL(te.work_start2,'') and ISNULL(ted.work_stop2,'') = ISNULL(te.work_stop2,'')
and ISNULL(ted.Work_Start3,'') = ISNULL(te.work_start3,'') and ISNULL(ted.Work_Stop3,'') = ISNULL(te.work_stop3,'')
and ISNULL(ted.Work_Start4,'') = ISNULL(te.work_start4,'') and ISNULL(ted.work_stop4,'') = ISNULL(te.work_stop4,'')
and ISNULL(ted.Work_Start5,'') = ISNULL(te.work_start5,'') and ISNULL(ted.Work_Stop5,'') = ISNULL(te.work_stop5,'')
and ISNULL(ted.Work_Start6,'') = ISNULL(te.work_start6,'') and ISNULL(ted.Work_Stop6,'') = ISNULL(te.work_stop6,'')
and ISNULL(ted.Work_Start7,'') = ISNULL(te.work_start7,'') and ISNULL(ted.work_stop7,'') = ISNULL(te.work_stop7,'')
and ISNULL(ted.Work_Start8,'') = ISNULL(te.work_start8,'') and ISNULL(ted.work_stop8,'') = ISNULL(te.work_stop8,'')
and ISNULL(ted.Work_Start9,'') = ISNULL(te.work_start9,'') and ISNULL(ted.work_stop9,'') = ISNULL(te.work_stop9,'')
and ISNULL(ted.Work_Start10,'') = ISNULL(te.work_start10,'') and ISNULL(ted.Work_Stop10,'') = ISNULL(te.work_stop10,'')
and ISNULL(ted.Work_Start11,'') = ISNULL(te.work_start11,'') and ISNULL(ted.work_stop11,'') = ISNULL(te.work_stop11,'')
and ISNULL(ted.Work_Start12,'') = ISNULL(te.work_start12,'') and ISNULL(ted.Work_Stop12,'') = ISNULL(te.work_stop12,'')
and ISNULL(ted.Work_Start13,'') = ISNULL(te.work_start13,'') and ISNULL(ted.Work_Stop13,'') = ISNULL(te.work_stop13,'')
and ISNULL(ted.Work_Start14,'') = ISNULL(te.work_start14,'') and ISNULL(ted.work_stop14,'') = ISNULL(te.work_stop14,'')
and ISNULL(ted.Work_Start15,'') = ISNULL(te.work_start15,'') and ISNULL(ted.work_stop15,'') = ISNULL(te.work_stop15,'')
and ISNULL(ted.Work_Start16,'') = ISNULL(te.work_start16,'') and ISNULL(ted.work_stop16,'') = ISNULL(te.work_stop16,'')

------If an entry exists in timesheet_entry table then set it to OLD for all where hours changed
Update te
set te.status ='OLD' 
from timesheet_entry te 
inner join #te_data ted on ted.work_emp_id =te.work_emp_id and ted.work_date =te.work_date and te.status ='ACTIVE' --and te.source = ted.source and te.source ='RTasq'
--where cast(ted.reg_hours as decimal(5,2)) <> cast(te.reg_hours as decimal(5,2))


-------Insert new entries into timesheet_entry table
insert into timesheet_entry ([work_emp_id],[work_date],[modified_date],[status],[entry_date],[entry_date_local],[entry_by],[vehicle_use],[reg_hours],[ot_hours],[dt_hours],[callout_hours],[vac_hours],[leave_hours],[br_hours],[hol_hours]
,[jury_hours],[pto_hours],[floating_holiday],[emp_type_id],[work_pc_code],[source]
,[work_start1],[work_stop1],[work_start2],[work_stop2],[work_start3],[work_stop3],[work_start4],[work_stop4],[work_start5],[work_stop5]
,[work_start6],[work_stop6],[work_start7],[work_stop7],[work_start8],[work_stop8],[work_start9],[work_stop9],[work_start10],[work_stop10],[work_start11],[work_stop11],[work_start12],[work_stop12]
,[work_start13],[work_stop13],[work_start14],[work_stop14],[work_start15],[work_stop15],[work_start16],[work_stop16])
select [work_emp_id],[work_date],[modified_date],[status],[entry_date],[entry_date_local],[entry_by],[vehicle_use],[reg_hours],[ot_hours],[dt_hours],[callout_hours],[vac_hours],[leave_hours],[br_hours],[hol_hours]
,[jury_hours],[pto_hours],[floating_holiday],[emp_type_id],[work_pc_code],[source]
,[work_start1],[work_stop1],[work_start2],[work_stop2],[work_start3],[work_stop3],[work_start4],[work_stop4],[work_start5],[work_stop5]
,[work_start6],[work_stop6],[work_start7],[work_stop7],[work_start8],[work_stop8],[work_start9],[work_stop9],[work_start10],[work_stop10],[work_start11],[work_stop11],[work_start12],[work_stop12]
,[work_start13],[work_stop13],[work_start14],[work_stop14],[work_start15],[work_stop15],[work_start16],[work_stop16]
from #te_data

--------set the processed rows as processed in rabbitmq_staging_rtime table
Update r
set r.Processed =1, r.process_status =case when r.process_status is null then 'Success' else r.process_status end
from rabbitmq_staging_rtime r
inner join #rmq_Staging rs on rs.rabbitmq_staging_id = r.rabbitmq_staging_id

------Drop all temporary tables
Drop table #rmq_Staging
Drop table #rmq_Staging_1
drop table #pid_twodates
drop table #te_data
drop table #te_otherdata
drop table #results

END
GO


