USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_TicketsDue_5]    Script Date: 2/16/2021 10:47:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--qm-340

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

ALTER procedure [dbo].[RPT_TicketsDue_5] (@ManagerId int, @DueDate datetime, @EmployeeStatus int)
as

--declare @ManagerId int, @DueDate datetime, @EmployeeStatus int;
--set		@ManagerId = 1004
--set		@DueDate = '2021-02-04'
--set		@EmployeeStatus = 1

  set nocount on

  declare @emps table (
    emp_id int primary key,
    short_name varchar(50)
  )

  insert into @emps
  select h_emp_id, h_short_name
  from dbo.get_report_hier3(@ManagerId, 0, 99, @EmployeeStatus)

  select
    convert(datetime, convert(varchar(12), ticket.due_date, 102), 102) as due_date_only,
    emps.short_name,
    ticket.ticket_number,
    ticket.ticket_id,
    ticket.due_date,
    ticket.ticket_type,
    ticket.work_type,
    locate.client_code,
    client.client_name,
    emps.emp_id as locator_id,
    locate.status,
    ticket.work_remarks,
    locate.closed,
    (select max(sync_log.sync_date) from sync_log
      where emps.emp_id = sync_log.emp_id) as last_sync_date,
    Coalesce((select description from reference where reference.ref_id = ticket.work_priority_id), 'Normal') priority,
    0 ticket_count,
    locate.locate_id
  into #tickets_due
  from locate  
    inner join ticket on ticket.ticket_id = locate.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join @emps emps on emps.emp_id = locate.assigned_to
  where
    ticket.due_date < @DueDate
	
	--select * from #tickets_due   --test point

  -- mark ticket count as 1 for every unique ticket in a set of locates
  update #tickets_due
    set ticket_count = 1
  where locate_id in (select locate_id 
					  from (
							select locate_id, ticket_number, 
							row_number() over (partition by ticket_number 
											   order by locate_id) rnum 
							from #tickets_due
							) s 
    where rnum = 1)

	--select * from #tickets_due   --test point  

  /* Replace comma,new line,tab values from Ticket_type, Work_type, Work_Remarks and Notes fields */
 SELECT
    due_date_only, short_name, ticket_number, td.ticket_id, due_date,
	case when ticket_type is not null then 
	Replace(Replace(Replace(Replace(ticket_type,',',' '),Char(9),''),Char(10),''),Char(13),'') END ticket_type, 
	case when work_type is not null then 
	Replace(Replace(Replace(Replace(work_type,',',' '),Char(9),''),Char(10),''),Char(13),'') END work_type, 
	client_code,client_name, locator_id, [status], 
	case when work_remarks is not null then 
	Replace(Replace(Replace(Replace(work_remarks,',',' '),Char(9),''),Char(10),''),Char(13),'')END work_remarks, 
	closed, last_sync_date, priority, ticket_count,Max(tn.entry_date) entry_date,max(tn.modified_date) modified_date,

	(STUFF((SELECT ' || ' +convert(varchar,tn.entry_date,101)+' - '+ 
	Replace(Replace(Replace(Replace(tn.note,',',' '),Char(9),''),Char(10),''),Char(13),'') 
	FROM ticket_notes tn 
				inner join reference r on r.code = isnull(tn.sub_type,0) and r.type =  'notes_sub' and r.description like '%public%'
				 WHERE tn.ticket_id = td.ticket_id and tn.foreign_type =1 
 FOR XML PATH(''), TYPE, ROOT).value('root[1]','nvarchar(max)'),1,4,'')) as note
  from #tickets_due td
  left join ticket_notes tn on tn.ticket_id = td.ticket_id and tn.foreign_type=1 and tn.sub_type in (select code from reference where type ='notes_sub' and description like '%public%')
group by due_date_only, short_name, ticket_number, td.ticket_id, due_date, ticket_type, work_type, client_code,
    client_name, locator_id, [status], work_remarks, closed, last_sync_date, priority, ticket_count
  order by convert(varchar, due_date, 102), short_name, ticket_number,td.ticket_id

  ---- Notes data
  --select tn.ticket_id, tn.entry_date, tn.modified_date, tn.notes_id, tn.note
  --from ticket_notes tn
  --where tn.ticket_id in (select distinct tic.ticket_id from @tickets_due tic)
  --order by tn.ticket_id, tn.entry_date, tn.notes_id
