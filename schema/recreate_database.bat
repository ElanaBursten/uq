@echo on
if "%SVR%"=="" set SVR=localhost

osql -S %SVR% -E -n -Q "drop database %DB%" || goto :failed
osql -S %SVR% -E -n -Q "CREATE database %DB%" || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i uq_schema.sql || goto :failed

@REM osql -S %SVR% -E -n -d %DB% -i sp-webapp.sql
osql -S %SVR% -b -E -n -d %DB% -i sp\get_old_tickets_for_locator.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_open_tickets_for_locator.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp-other.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i uq_ref_data.sql || goto :failed

@REM These just remove some SP dependency warnings
osql -S %SVR% -b -E -n -d %DB% -i sp\acknowledge_ticket.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_office_id.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_ticket.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_ticketnumber.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_ticket_serial.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_locate_FCO2.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_locate.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_unacked_tickets_2.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\update_ticket_status.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_locate_FHL1.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_locate_FPL1.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\check_summary_by_ticket_serial_LQW1.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_open_damages_for_locator.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_old_damages_for_locator.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_open_work_orders_for_emp.sql || goto :failed
osql -S %SVR% -b -E -n -d %DB% -i sp\get_old_work_orders_for_emp.sql || goto :failed

FOR %%v IN (sp\*.sql) DO (
  osql -S %SVR% -b -E -n -d %DB% -i %%v || goto :failed
)

exit

:failed
echo ***** FAILED ****
exit 1
