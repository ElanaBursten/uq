USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[close_all_locates_for_locator]    Script Date: 3/30/2023 3:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Need to be able to override more than one code when run.
--AutoClose autoclose.ini 25126 NP 0 BSCA=OS
--design for:
--AutoClose autoclose.ini 25126 NP 0 BSCA=OS|ATT01=OS


ALTER procedure [dbo].[close_all_locates_for_locator] ( --CHG0068821
@LocatorID int,
@Status varchar(20),
@Billed bit,
@Excludes varchar(max)=null,  --QMANTWO-799
@OverRide varchar(20)=null, --QM-262 sr
@TimeOffset int   --QM-733 sr
)
as

set nocount on --CHG0068821
--=================================================================================Begin Test Params
--declare @LocatorID int,            --test
-- @Status    varchar(20),
-- @Billed    bit,
-- @Excludes  varchar(max),
-- @OverRide varchar(max);

--set @LocatorID = 17411 --23910
--set @Status    = 'M'
--set @Billed    = 1
--set @Excludes  = 'ATTDSOUTH, USCE47, UVZHTB'
--set @OverRide = 'USCENC=OS|ATT01=OS'  --to be modified
--set @TimeOffset = -3

--=================================================================================End Test Params
--Declare @ovrdClient varchar(12),
--@ovrdStatus varchar(12)

declare @ovrdListList table (              --QM-715 sr
ovrdListList varchar(32)
,ovrdClient varchar(12)
,ovrdStatus varchar(12)
)


declare @exclsTab table (              --QMANTWO-799
excludeClients varchar(16)
)

--=================================================================================================

  insert into @ovrdListList(ovrdListList) select * from [dbo].StringSplit (@OverRide,'|')  --QM-715 sr

  update @ovrdListList 
  set ovrdClient= (select substring(ovrdListList,0 ,Charindex('=',ovrdListList))),   --Charindex('=',ovrdListList)
  ovrdStatus =  (select substring(ovrdListList,Charindex('=',ovrdListList)+1,len(ovrdListList)))
  --select * from @ovrdListList  --test point QM-715 sr


--If @OverRide is not null
--begin --CHG0068821


--  insert into @ovrdList
--  select * from [dbo].StringSplit (@OverRide,'=')  --QM-262 sr
--  set @ovrdClient = (select top 1 * from @ovrdList)
--  set @ovrdStatus = (select * from @ovrdList where ovrdList not in (Select @ovrdClient))

--  Insert into @exclsTab
--  values(@ovrdClient)
--end
--=================================================================================================
declare @ltoc table (
locate_id int not null primary key
)

insert into @exclsTab
select * from [dbo].StringSplit (@Excludes,',')  --QMANTWO-799

--select excludeClients from @exclsTab    --test works


insert into @ltoc
select distinct locate.locate_id
 from locate --CHG0068821
 --inner join locate on locate.locate_id=assignment.locate_id  QM-365   SR
 where assigned_to_id=@LocatorID
  and locate.closed=0
  and locate.active=1
 

--select count(*) as LocatesToProcess from @ltoc --test

declare @closed_date_now datetime
select @closed_date_now = DateAdd(HH, -@TimeOffset, getdate())   --localize  qm-733

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
	   entry_date=@closed_date_now,
       qty_marked=1,
       invoiced=@Billed
  where locate_id = @loc
  and locate.client_code not in (select excludeClients from @exclsTab)  --QMANTWO-799
 
  If @OverRide is not null --QM-249
  update L   --QM-249
   set status=ovrdStatus,  
       closed=1,  
       closed_by_id=assigned_to,  
       closed_how='Bulk',  
       closed_date=@closed_date_now,  
       entry_date=@closed_date_now,  
       qty_marked=1,
       invoiced=@Billed
  From Locate L
  join @ovrdListList OLL on (L.client_code=OLL.ovrdClient)
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END --CHG0068821
