
CREATE Procedure dbo.sp_CHK_LoginUser
---------------------------------------------------------
--	Desc:  Validate User Login
--
--	Notes: 1/2/2002	sbrooks	Created
---------------------------------------------------------
	@LoginId	varchar(10),
	@LoginPwd	varchar(10),
	@UserId		int output,
	@GrpId		int output,
	@FirstName	varchar(20) output,
	@LastName	varchar(30) output,
	@ChgPwd		bit output,
	@LastLogin	datetime output,
	@LoginUser	varchar(50) output,
	@UserGuid	varchar(50) output
	
As

	Declare @SessionId   int

	-- Check user file for valid login
	SELECT @UserId = uid,
	       @GrpId = grp_id,
	       @FirstName = first_name,
	       @LastName = last_name,   
	       @ChgPwd = chg_pwd,   
	       @LastLogin = last_login  
	  FROM users  
	 WHERE active_ind = 1 AND
		   login_id = @LoginId AND  
	       password = @LoginPwd


	-- Invalid login, return error
	If @@Rowcount = 0 Return -1

	-- If first login, user must change password
	If @LastLogin Is Null
		Select @ChgPwd = 1
	Else
	Begin
		Update users
		   Set last_login = Getdate()
		 Where uid = @UserId
	End		

	-- Get unique indentifier
	Select @UserGuid = NewId()
	
	-- Delete any old user sessions 
	
	
	-- Set up login user string
	Select @LoginUser = 'User: ' + @FirstName + ' ' + @LastName
	
	-- Create a record in user_session for this user
	Insert Into user_session
		( user_guid, create_date )
	Values
		( @UserGuid, Getdate() )
	If @@Rowcount = 0 Return -4
	
	Select @SessionId = @@Identity
	
	Insert Into user_session_vars
		( session_id, var_name, var_value )
	Values
		( @SessionId, 'UserId', @UserId )
	If @@Rowcount = 0 Return -5

	Insert Into user_session_vars
		( session_id, var_name, var_value )
	Values
		( @SessionId, 'ValidUser', 'Yes' )
	If @@Rowcount = 0 Return -6

	Insert Into user_session_vars
		( session_id, var_name, var_value )
	Values
		( @SessionId, 'LoginUser', @LoginUser )
	If @@Rowcount = 0 Return -7
	
	Return 0


GO

Create Procedure dbo.sp_GET_EmployeeId
---------------------------------------------------------
--	Desc:	Retrieve EmployeeId for user login
--
--	Notes:	1/18/2002	sbrooks	Created
---------------------------------------------------------
	(	@UserId	int	)	
AS

	SELECT emp_id AS EmpId
	  FROM users
	 WHERE uid = @UserId
	
	Return @@Error
GO


CREATE Procedure dbo.sp_POST_Employee
---------------------------------------------------------
--	Desc:	Post Employee record to database
--
--	Notes:	1/18/2002	sbrooks	Created
---------------------------------------------------------
	(
		@Action			char(1),
		@EmpId			int,
		@EmpType		int,	
		@StatusId		int,
		@TimesheetId	int,
		@EmpNumber		varchar(15),		
		@NickName		varchar(20),
		@FirstName		varchar(20),
		@LastName		varchar(30),
		@MiddleInit		char(1),
		@MgrId			int,
		@UserId			int,
		@GrpId			int,
		@LoginId		varchar(20),
		@LoginPwd		varchar(20),
		@strOffice		varchar(2000),
		@LoginUser		int
	)
As
	Declare
		@iReturn	int,
		@RecCnt		int,
		@StatCde	char(1),
		@ActiveInd	int,
        @TempStr1	varchar(20),
        @ChrIdx1	int,
        @OfficeId	int,
        @RefCode	varchar(5)

	Begin Transaction		
	If @Action = 'D' Or @Action = 'C' 
	Begin
	
		Select 'delete record or check constraints'
/*	
		If Exists
			(SELECT office_id 
			   FROM employee_office
			  WHERE office_id = @OfficeId)
		Begin
			-- Constraints exist, no deletion allowed
			Select @iReturn = 99
			Goto lblError
		End Else
		Begin
		
			If @Action = 'C'
			-- Just checking contraints, don't delete record
			Begin
				Select @iReturn = 98
				Goto lblError
			End
			
			-- No constraints, delete record
			DELETE office
			 WHERE office_id = @OfficeId
			 
			If @@Error <> 0
			Begin
				Select @iReturn = 10
				Goto lblError
			End
		End
		
*/		
	End Else
	Begin

		-- Check for duplicate employee number
		Select @RecCnt = Count(*)
		  From employee
		 Where emp_number = @EmpNumber And
		       emp_id <> @EmpId
		       
		If @RecCnt > 0 
		Begin
			Select @iReturn = 99
			Goto lblError
		End

		-- Check for duplicate login id's
		Select @RecCnt = Count(*)
		  From users
		 Where login_id = @LoginId And
		       emp_id <> @EmpId
		       
		If @RecCnt > 0 
		Begin
			Select @iReturn = 98
			Goto lblError
		End
		
		-- Check to see if manager is required
		If @MgrId = 0
		Begin
		
			SELECT @RefCode = code  
			  FROM reference  
			 WHERE ref_id = @EmpType

			If @@Rowcount = 0			 
			Begin		 
				Select @iReturn = 20
				Goto lblError
			End 

			If @RefCode <> 'SD'
			Begin		 
				Select @iReturn = 97
				Goto lblError
			End 

		End

		-- Set users active indicator based on employee status
		SELECT @StatCde = code  
		  FROM reference  
		 WHERE ref_id = @StatusId
		 
		If @@Rowcount = 0 Select @StatCde = 'I'
		
		If @StatCde = 'A'
			Select @ActiveInd = 1
		Else
			Select @ActiveInd = 0
		 
		If @EmpId = 0
		Begin
	
			-- Insert new employee record
			INSERT INTO employee  
			       ( type_id,   
			         status_id,   
			         timesheet_id,   
			         emp_number,   
			         short_name,   
			         first_name,   
			         last_name,   
			         middle_init,   
			         report_to,   
			         create_date,   
			         create_uid ) 
			VALUES ( @EmpType,   
			         @StatusId,   
			         @TimesheetId,   
			         @EmpNumber,   
			         @NickName,   
			         @FirstName,   
			         @LastName,   
			         @MiddleInit,   
			         @MgrId,   
			         getdate(),   
			         @LoginUser ) 
			         
			If @@Rowcount = 0
			Begin
				Select @iReturn = 30
				Goto lblError
			End	
			
			Select @EmpId = @@Identity	         

		End Else
		Begin
		
			-- Update existing employee record
			UPDATE employee  
			   SET type_id = @EmpType,   
			       status_id = @StatusId,   
			       timesheet_id = @TimesheetId,   
			       emp_number = @EmpNumber,   
			       short_name = @NickName,   
			       first_name = @FirstName,   
			       last_name = @LastName,   
			       middle_init = @MiddleInit,   
			       report_to = @MgrId,   
			       modified_date = getdate(),   
			      modified_uid = @LoginUser  
			 WHERE emp_id = @EmpId 
   			 			            
			If @@Rowcount = 0
			Begin
				Select @iReturn = 40
				Goto lblError
			End	

		  	
		End	

		If @UserId = 0
		Begin
		
			-- Insert new users record
			INSERT INTO users  
			       ( grp_id,   
			         emp_id,   
			         first_name,   
			         last_name,   
			         login_id,   
			         password,   
			         chg_pwd,   
			         last_login,   
			         active_ind )  
			VALUES ( @GrpId,   
			         @EmpId,   
			         @NickName,   
			         @LastName,   
			         @LoginId,   
			         @LoginPwd,   
			         0,   
			         null,   
			         @ActiveInd )  

			If @@Rowcount = 0
			Begin
				Select @iReturn = 50
				Goto lblError
			End
					         		
		End Else
		Begin
		
			-- Update existing users record
			UPDATE users  
			   SET grp_id = @GrpId,   
			       emp_id = @EmpId,   
			       first_name = @NickName,   
			       last_name = @LastName,   
			       login_id = @LoginId,   
			       password = @LoginPwd,   
			       chg_pwd = 0, 
			       active_ind = @ActiveInd  
			 WHERE uid = @UserId 
 			 			            
			If @@Rowcount = 0
			Begin
				Select @iReturn = 60
				Goto lblError
			End	
		
		End


		
	End

	-- Update employee locations
	Create Table #Office ( OfficeId int )  -- Temp table for values

	-- Parse Key and Value strings and build temp table for offices
	If @strOffice Is Not Null
	Begin

		Select @ChrIdx1 = 1
	   
		While @ChrIdx1 > 0      -- Parse strings and values in temp table
		Begin
      
			Select @ChrIdx1 = CharIndex(',',@strOffice)
	      
			If @ChrIdx1 > 0 
			Begin
				Select @TempStr1 = Substring(@strOffice,1,@ChrIdx1 - 1)
				Select @strOffice = Substring(@strOffice,@ChrIdx1 + 1,2000)
			End Else 
				Select @TempStr1 = @strOffice
	      
			If @TempStr1 Is Not Null And @TempStr1 <> ''
			Begin
				Select @OfficeId = Convert(int,@TempStr1)
				Insert Into #Office Values (@OfficeId)
			End
	   
		End
	End

	-- Insert new records in temp table and not in database
	INSERT INTO employee_office  
         ( emp_id, office_id )
    SELECT @EmpId, a.OfficeId
      FROM #Office a
     WHERE NOT EXISTS
		( SELECT office_id
		    FROM employee_office
		   WHERE emp_id = @EmpId AND
		         office_id = a.OfficeId )  

	If @@Error > 0
	Begin
		Select @iReturn = 70
		Goto lblError
	End                      

	-- Delete any offices in DB and not in temp table
	DELETE employee_office
	 WHERE emp_id = @EmpId AND
	       office_id NOT IN
		      ( SELECT OfficeID FROM #Office )

	If @@Error > 0
	Begin
		Select @iReturn = 80
		Goto lblError
	End                      

	Drop Table #Office

	Commit Transaction         
	Return 0
	
lblError:
	Rollback Transaction
	Return @iReturn



GO

CREATE Procedure dbo.sp_POST_Office
---------------------------------------------------------
--	Desc:	Post Office record to database
--
--	Notes:	1/4/2002	sbrooks	Created
---------------------------------------------------------
	(
		@Action char(1),
        @OfficeId int,   
        @ProfitCenter varchar(15),   
        @OfficeName varchar(40),   
        @HoursRespond int
	)
As
	Declare
		@iReturn	int,
		@RecCnt		int

	Begin Transaction		

				Select @iReturn = 40
				Goto lblError



	If @Action = 'D' Or @Action = 'C' 
	Begin
	
		If Exists
			(SELECT office_id 
			   FROM employee_office
			  WHERE office_id = @OfficeId)
		Begin
			-- Constraints exist, no deletion allowed
			Select @iReturn = 99
			Goto lblError
		End Else
		Begin
		
			If @Action = 'C'
			-- Just checking contraints, don't delete record
			Begin
				Select @iReturn = 98
				Goto lblError
			End
			
			-- No constraints, delete record
			DELETE office
			 WHERE office_id = @OfficeId
			 
			If @@Error <> 0
			Begin
				Select @iReturn = 10
				Goto lblError
			End
		End
	End Else
	Begin

		-- Check for duplicate profit center codes
		Select @RecCnt = Count(*)
		  From office
		 Where profit_center = @ProfitCenter And
		       office_id <> @OfficeId
		       
		If @RecCnt > 0 
		Begin
			Select @iReturn = 97
			Goto lblError
		End

		If @OfficeId = 0
		Begin
	
			-- Insert new office record
			INSERT INTO office  
			       ( office_name,   
			         profit_center,   
			         hours_to_respond )  
			VALUES ( @OfficeName,   
                     @ProfitCenter,
                     @HoursRespond )

			If @@Rowcount = 0
			Begin
				Select @iReturn = 30
				Goto lblError
			End		         
		     
		End Else
		Begin
		
			-- Update existing office record
			UPDATE office  
			   SET office_name = @OfficeName,   
			       profit_center = @ProfitCenter,   
			       hours_to_respond = @HoursRespond  
			 WHERE office.office_id = @OfficeId  
			 			            
			If @@Rowcount = 0
			Begin
				Select @iReturn = 40
				Goto lblError
			End		         
		  	
		End	
	End

	Commit Transaction         
	Return 0
	
lblError:
	Rollback Transaction
	Return @iReturn

GO

CREATE Procedure dbo.sp_SEL_EmpTypeDD
---------------------------------------------------------
--	Desc:  Select Employee Types from reference table
--
--	Notes: 1/16/2002	sbrooks	Created
---------------------------------------------------------
	( @UserId	int )

As

	Declare 
		@AllEmployees	int,
		@EmpType		int
		
	-- Check to see if this user has access to all employees
	SELECT @AllEmployees = a.all_employees,   
	       @EmpType = c.type_id  
	  FROM webgrp a,   
	       users b,   
	       employee c 
	 WHERE b.grp_id = a.grp_id AND 
	       c.emp_id =* b.emp_id AND 
	       b.uid = @UserId 
				
	If @@Rowcount = 0 
		Select @AllEmployees = 0, @EmpType = 0

	If @AllEmployees = 0
	Begin
	
		  SELECT a.ref_id,   
		         a.description,   
		         a.sortby  
		    FROM reference a  
		   WHERE a.type = 'emptype' AND  
		         a.active_ind = 1  AND
		         a.sortby > (SELECT sortby FROM reference WHERE ref_id = @EmpType) 
		ORDER BY a.sortby ASC 

	End Else
	Begin

		  SELECT a.ref_id,   
		         a.description
		    FROM reference a  
		   WHERE a.type = 'emptype' AND  
		         a.active_ind = 1   
		ORDER BY a.sortby ASC
		 
	End
	
	Return @@error

GO

CREATE Procedure dbo.sp_SEL_Employee
---------------------------------------------------------
--	Desc:	Retrieve Employee record for maintenance
--
--	Notes:	1/16/2002	sbrooks	Created
---------------------------------------------------------
	@EmpId			int
	
AS

	SELECT a.type_id AS EmpType,   
	       a.status_id AS StatusId,   
	       a.timesheet_id AS TimesheetId,   
	       a.emp_number AS EmpNumber,   
	       a.short_name AS NickName,   
	       a.first_name AS FirstName,   
	       a.last_name AS LastName,   
	       a.middle_init AS MiddleInit,   
	       a.report_to AS MgrId,   
	       b.uid AS UserId,   
	       b.grp_id AS GrpId,   
	       b.login_id AS LoginId,   
	       b.password AS LoginPwd  
	  FROM employee a,   
	       users b  
	 WHERE b.emp_id =* a.emp_id AND 
	       a.emp_id = @EmpId 
	 
	 
	SELECT a.office_id AS OfficeId,
	       b.profit_center AS ProfitCenter
	  FROM employee_office a,
	       office b
	 WHERE b.office_id = a.office_id AND
	       a.emp_id = @EmpId 	  
	 
	Return @@Error
	
	

GO


CREATE Procedure dbo.sp_SEL_EmployeeList
---------------------------------------------------------
--	Desc:	Retrieve Employee records for lookup
--
--	Parms: Mode			- Type of lookup
--         Idx			- Index to search on
--         Name			- Search string
--		   UserId		- User Id of user doing search
--		   OfficeId		- Office location 
--
--	Notes:	1/4/2002	sbrooks	Created
---------------------------------------------------------
	@Mode			char(1),
	@Idx			char(3),
	@Name			varchar(50),
	@UserId			int
	
AS

	Declare 
		@strSearch		varchar(52),
		@AllEmployees	bit,
		@CurrLevel		int, 
		@NextLevel		int,
		@EmpId			int		

	-- Check to see if this user has access to all employees
	SELECT @AllEmployees = a.all_employees,
	       @EmpId = b.emp_id  
	  FROM webgrp a,   
	       users b  
	 WHERE b.grp_id = a.grp_id AND  
	       b.uid = @UserId 

	If @@Rowcount = 0 
		Select @AllEmployees = 0, @EmpId = 0
	
	Create Table #TempMgr (EmpLevel int, EmpId int)

	-- Build list of report_to ids for employee selection
	If @AllEmployees = 0
	Begin

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		 VALUES (0, @EmpId)

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		SELECT 1, a.emp_id
		  FROM employee a,   
		       reference b 
		 WHERE b.ref_id = a.type_id AND  
		       a.report_to = @EmpId AND
		       b.code <> 'Loc'
       
		If @@Rowcount = 0 Goto lblEndMgr

		Select @CurrLevel = 1
		Select @NextLevel = 2
		lblNextMgr:

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		SELECT @NextLevel, a.emp_id
		  FROM employee a,   
		       reference b 
		 WHERE b.ref_id = a.type_id AND  
		       a.report_to IN 
				  (SELECT DISTINCT EmpId 
				     FROM #TempMgr 
				    WHERE EmpLevel = @CurrLevel) AND
		       b.code <> 'Loc'
       
		If @@Rowcount = 0 Goto lblEndMgr
		Select @CurrLevel = @CurrLevel + 1
		Select @NextLevel = @NextLevel + 1
		Goto lblNextMgr

		lblEndMgr:

	End

	If @Mode = 'I' And @Idx = 'All'
	Begin

		If @AllEmployees = 0
		Begin

			SELECT a.emp_id,   
			       a.emp_number,   
			       a.first_name,   
			       a.last_name,   
			       b.description,  
			       c.description
			  FROM employee a,   
			       reference b,
			       reference c
			 WHERE a.type_id = b.ref_id AND
			       a.status_id = c.ref_id AND
			       a.report_to IN (SELECT DISTINCT EmpId FROM #TempMgr)
		  ORDER BY a.last_name, a.first_name

		End Else
		Begin
		
			SELECT a.emp_id,   
			       a.emp_number,   
			       a.first_name,   
			       a.last_name,   
			       b.description,  
			       c.description
			  FROM employee a,   
			       reference b,
			       reference c
			 WHERE a.type_id = b.ref_id AND
			       a.status_id = c.ref_id 
		  ORDER BY a.last_name, a.first_name
		  
		End 
		
	End Else
	Begin
	
		If @Mode = 'I'
			If @Idx = 'Num'
				Select @strSearch = '[0123456789]%'
			Else
				Select @strSearch = RTrim(@Idx) + '%'
		Else
			Select @strSearch = '%' + @Name + '%'


		If @AllEmployees = 0
		Begin

				SELECT a.emp_id,   
				       a.emp_number,   
				       a.first_name,   
				       a.last_name,   
				       b.description,  
				       c.description
				  FROM employee a,   
				       reference b,
				       reference c
				 WHERE a.type_id = b.ref_id AND
				       a.status_id = c.ref_id AND
					   a.report_to IN (SELECT DISTINCT EmpId FROM #TempMgr) AND
				       a.last_name LIKE @strSearch
			ORDER BY a.last_name, a.first_name

		End Else
		Begin

				SELECT a.emp_id,   
				       a.emp_number,   
				       a.first_name,   
				       a.last_name,   
				       b.description,  
				       c.description
				  FROM employee a,   
				       reference b,
				       reference c
				 WHERE a.type_id = b.ref_id AND
				       a.status_id = c.ref_id AND
				       a.last_name LIKE @strSearch
			ORDER BY a.last_name, a.first_name
			
		End

	End
	
	Drop Table #TempMgr

	Return @@Error
	


GO


CREATE Procedure dbo.sp_SEL_EmployeeOffice
---------------------------------------------------------
--	Desc:	Retrieve Office record for Employee Maintenance
--
--	Notes:	1/16/2002	sbrooks	Created
---------------------------------------------------------
	@UserId	int
		
AS

	Declare 
		@AllOffices	int,
		@EmpId		int
		
	-- Check to see if this user has access to all offices
	SELECT @AllOffices = a.all_offices,
	       @EmpId = b.emp_id  
	  FROM webgrp a,   
	       users b  
	 WHERE b.grp_id = a.grp_id AND  
	       b.uid = @UserId 
				
	If @@Rowcount = 0 
		Select @AllOffices = 0, @EmpId = 0

	If @AllOffices = 0
	Begin

		  SELECT a.office_id,   
		         a.office_name  
		    FROM office a,   
		         employee_office b  
		   WHERE b.office_id = a.office_id AND  
		         b.emp_id = @EmpId  
		ORDER BY a.office_name ASC	
		
	End Else
	Begin

		  SELECT office_id,   
		         office_name  
		    FROM office  
		ORDER BY office_name ASC
		
	End
	
	Return @@Error
GO


CREATE Procedure dbo.sp_SEL_ManagerDD
---------------------------------------------------------
--	Desc:  Select from Employee Table list of Managers
--
--	Notes: 1/4/2002	sbrooks	Created
---------------------------------------------------------
	(	@UserId	int   )
As

	Declare 
		@AllEmployees	bit,
		@CurrLevel		int, 
		@NextLevel		int,
		@EmpId			int		

	-- Check to see if this user has access to all employees
	SELECT @AllEmployees = a.all_employees,
	       @EmpId = b.emp_id  
	  FROM webgrp a,   
	       users b  
	 WHERE b.grp_id = a.grp_id AND  
	       b.uid = @UserId 

	If @@Rowcount = 0 
		Select @AllEmployees = 0, @EmpId = 0
	
	Create Table #TempMgr (EmpLevel int, EmpId int)

	-- Build list of report_to ids for this user
	If @AllEmployees = 0
	Begin

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		 VALUES (0, @EmpId)

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		SELECT 1, a.emp_id
		  FROM employee a,   
		       reference b 
		 WHERE b.ref_id = a.type_id AND  
		       a.report_to = @EmpId AND
		       b.code <> 'Loc'
       
		If @@Rowcount = 0 Goto lblEndMgr

		Select @CurrLevel = 1
		Select @NextLevel = 2
		lblNextMgr:

		INSERT INTO #TempMgr (EmpLevel, EmpId)
		SELECT @NextLevel, a.emp_id
		  FROM employee a,   
		       reference b 
		 WHERE b.ref_id = a.type_id AND  
		       a.report_to IN 
				  (SELECT DISTINCT EmpId 
				     FROM #TempMgr 
				    WHERE EmpLevel = @CurrLevel) AND
		       b.code <> 'Loc'
       
		If @@Rowcount = 0 Goto lblEndMgr
		Select @CurrLevel = @CurrLevel + 1
		Select @NextLevel = @NextLevel + 1
		Goto lblNextMgr

		lblEndMgr:

	End

	If @AllEmployees = 0
	Begin
	
		  SELECT a.emp_id,   
		         a.first_name + ' ' + a.last_name
		    FROM employee a  
		   WHERE a.emp_id IN (SELECT DISTINCT EmpId FROM #TempMgr)
		ORDER BY a.last_name

	End Else
	Begin

		  SELECT a.emp_id,   
		         a.first_name + ' ' + a.last_name
		    FROM employee a,   
		         reference b  
		   WHERE b.ref_id = a.type_id AND
		         b.type = 'emptype' AND
		         b.code <> 'Loc'
		ORDER BY a.last_name
		
	End 
	
	Drop Table #TempMgr

	Return @@error

GO


CREATE Procedure dbo.sp_SEL_MenuItems
---------------------------------------------------------
--	Desc:  Select Menu Items
--
--	Notes: 1/3/2002	sbrooks	Created
---------------------------------------------------------
	(
		@UserId int,
		@MnuId int
	)
As

	SELECT a.app_id,   
	       a.title,   
	       a.description,   
	       a.path,   
	       a.sortby,   
	       a.active_ind  
	  FROM webapp a,   
	       webmnuapp b,
	       webgrpapp c,   
	       users d
	 WHERE c.app_id = a.app_id AND 
	       b.app_id = a.app_id AND  
	       d.grp_id = c.grp_id AND  
	       a.active_ind = 1 AND  
	       b.mnu_id = @MnuId AND  
	       d.uid = @UserId 

	Return 0 

GO

CREATE Procedure dbo.sp_SEL_Office
---------------------------------------------------------
--	Desc:	Retrieve Office record for Maintenance
--
--	Notes:	1/16/2002	sbrooks	Created
---------------------------------------------------------
	@OfficeId	int
		
AS
	  SELECT profit_center AS ProfitCenter,   
	         office_name AS OfficeName,
	         hours_to_respond AS HoursRespond  
	    FROM office  
	   WHERE office_id = @OfficeId

	Return @@Error
GO


Create Procedure dbo.sp_SEL_OfficeDD
---------------------------------------------------------
--	Desc:  Select from Office Table
--
--	Notes: 1/4/2002	sbrooks	Created
---------------------------------------------------------
As
	  SELECT office_id,   
	         office_name  
	    FROM office  
	ORDER BY office_name ASC 
	
	Return @@error

GO


CREATE Procedure dbo.sp_SEL_OfficeList
---------------------------------------------------------
--	Desc:	Retrieve Office records for lookup
--
--	Notes:	1/16/2002	sbrooks	Created
---------------------------------------------------------
	
AS

	  SELECT office_id,   
	         profit_center,   
	         office_name  
	    FROM office  
	ORDER BY office_name ASC 

	Return @@Error
GO


CREATE Procedure dbo.sp_SEL_ReferenceDD
---------------------------------------------------------
--	Desc:  Select from Reference Table
--
--	Notes: 1/4/2002	sbrooks	Created
---------------------------------------------------------
	(
		@RefType varchar(10)
	)
As

	  SELECT ref_id,   
	         description
	    FROM reference  
	   WHERE active_ind = 1 AND
	         type = @RefType   
	ORDER BY sortby ASC 

/*
	  SELECT code,   
	         description
	    FROM reference  
	   WHERE active_ind = 1 AND
	         type = @RefType   
	ORDER BY sortby ASC 
*/	
	Return @@error

GO


CREATE Procedure dbo.sp_SEL_SessionVars
--------------------------------------------------------
--	Desc:  Retrieve Session Variables
--
--	Notes: 1/22/2001	sbrooks	Created
---------------------------------------------------------
	(	@UserGuid	varchar(50)	)
As

	SELECT b.var_name,   
	       b.var_value  
	  FROM user_session a,   
	       user_session_vars b  
	 WHERE b.session_id = a.session_id AND  
	       a.user_guid = @UserGuid 

	Return @@Error
	


GO

CREATE Procedure dbo.sp_SEL_WebGrpDD
---------------------------------------------------------
--	Desc:  Select from WebGrp Table
--
--	Notes: 1/16/2002	sbrooks	Created
---------------------------------------------------------
	( @UserId	int )

As

	Declare 
		@AllEmployees	int,
		@AccessLevel	int
		
	-- Check to see if this user has access to all employees
	SELECT @AllEmployees = a.all_employees,
	       @AccessLevel = a.access_level  
	  FROM webgrp a,   
	       users b  
	 WHERE b.grp_id = a.grp_id AND  
	       b.uid = @UserId 
				
	If @@Rowcount = 0 
		Select @AllEmployees = 0, @AccessLevel = 10

	If @AllEmployees = 0
	Begin
	
		  SELECT grp_id,   
		         description  
		    FROM webgrp 
		   WHERE grp_id > 0 AND
		         access_level >= @AccessLevel 
		ORDER BY access_level ASC

	End Else
	Begin

		  SELECT grp_id,   
		         description  
		    FROM webgrp 
		   WHERE grp_id > 0
		ORDER BY access_level ASC

	End
	
	Return @@error

GO



