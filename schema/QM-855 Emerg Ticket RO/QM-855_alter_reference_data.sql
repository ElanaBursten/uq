--QM-855 Ticket worked out of Route Order
--Decision made to just use a reason in the Skip Dialog

if not exists(select code from reference where code = 'EMERGSKIP')
INSERT INTO [dbo].[reference]
           ([type]
           ,[code]
           ,[description]
           ,[active_ind])
     VALUES
           ('routeord'
           ,'EMERGSKIP'
           ,'Emergency Ticket'
           ,1)
GO

select * from reference where type = 'routeord'

