--QM-896 Route Order Ticket limit setting - this is a new stored procedure
if object_id('dbo.get_ticket_list_RO') is not null
  drop procedure dbo.get_ticket_list_RO
GO

CREATE procedure [dbo].get_ticket_list_RO (@emp_id int, @open_only bit, @num_days int, @unacked bit)
as
  if @open_only = 1
    exec dbo.get_open_tickets_for_locator_RO @emp_id
  else begin
    if @unacked = 1 /*  Unacknowledged Tickets */
      exec dbo.get_unacked_tickets_RO @emp_id
    else  /* Last X Days */
      exec dbo.get_old_tickets_for_locator_RO @emp_id, @num_days
  end

  --exec get_ticket_list_RO 10564, 1, 20000, 0

