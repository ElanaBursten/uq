USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[get_pending_multi_responses_10]    Script Date: 1/7/2022 3:37:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER Procedure [dbo].[get_pending_multi_responses_10](@RespondTo varchar(20), 
 @CallCenter varchar(20), @ParsedLocatesOnly bit = 1, 
 @FTPRetryMinutes int = 240, @ResponseDelayMinutes int = 0, 
 @EmailRetryMinutes int = 120, @LocateAddedBys varchar(8000) = 'PARSER') 
as   -- QM-478 Add RSReason (reschedule Reason) for Boss911 parser
set nocount on 
-- QM-468 Add EPR_Link for SCA and NCA responses
select top 800 
 rq.locate_id, locate.status, ticket.ticket_format, 
 ticket.ticket_id, ticket.ticket_number, locate.client_code, 
 ticket.ticket_type, ticket.work_state, rq.insert_date, 
 ticket.serial_number, locate.closed_date, ticket.work_county, 
 ticket.work_city, ticket.work_date, ticket.service_area_code, 
 ticket.due_date, e.short_name as locator_name, 
 rq.related_response_id, rlg.response_date as related_response_date, 
 rlg.response_date_is_dst as related_response_date_is_dst, 
 rlg.status as related_status, rlg.response_sent as related_response_sent, 
 SGI.outgoing_status, SGI.status_explanation, c.utility_type, --QM-503
 locate.qty_marked, rq.in_progress_response_id, 
 rt.api_key, rt.[include_notes], rt.[include_epr_link],  --QM-503
 rt.[url], rt.[user_name], rt.[password], rt.[all_versions], rt.[real_locator_name],--QM-503
 [real_locator_name], rt.[one_call_center],--QM-503
 case statuslist.complete when NULL then 0 when 0 then 0 else 1 END complete, --qm-513

 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='ONGOCONT')), '') as contact, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='CONPHNE')), '') as contact_phone, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='RCOMMENT')), '') as response_comment, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='RSREASON')), '') as reschedule_reason,  ---- QM-478 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='SMDT')), '') as scheduled_meet_datetime, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='ACTOFGOD')), '') as act_of_god, 
 coalesce((select info from ticket_info ti2 where ti2.ticket_info_id = 
  (select max(ticket_info_id) from ticket_info ti 
    where ti.ticket_id = locate.ticket_id 
    and info_type='CONMETH')), '') as contact_method, 
 (select min(arrival_date) from jobsite_arrival ja 
    where ja.ticket_id=ticket.ticket_id) as first_arrival_date, 
 (select coalesce(EH.link,ER.link)
	from epr_response_history EH 
	left outer join epr_response ER on (ER.ticket_id=EH.ticket_id)
	where EH.ticket_id = ticket.ticket_id)  as EPR_Link,  --QM-468
  (SELECT TOP 1 note
	FROM [QM].[dbo].[notes]
	where foreign_id = locate.locate_id 
	and foreign_type = 5
	and sub_type = 500
	and active =1) as PubLocateNote  --QM-468
 from responder_multi_queue rq 
 inner join locate on rq.locate_id=locate.locate_id 
 inner join ticket on locate.ticket_id=ticket.ticket_id 
 left join employee e on e.emp_id = coalesce(locate.closed_by_id, locate.assigned_to) 
 left join response_log rlg on rlg.response_id = rq.related_response_id 
 join status_group SG on (SG.sg_name = @RespondTo) --QM-503
 left outer join status_group_item SGI on (SGI.sg_id= SG.sg_id) --QM-503
 left outer join respond_to RT on (rt.respond_to=@RespondTo)--QM-503
 left outer join client C on (c.[oc_code] = locate.client_code) 
 left outer join statuslist on (locate.status = statuslist.status) --qm-513
where 

 len(coalesce(@CallCenter, '')) > 0 
 and rq.ticket_format = @CallCenter 
 and rq.respond_to = @RespondTo 
 and RT.active=1
 -- todo(dan) Add do_not_respond_before if/when it's needed 
 -- and (rq.do_not_respond_before is null 
 --      or rq.do_not_respond_before <= getdate()) 
 
 and ticket.ticket_number not like 'MAN%' 
 and isnull(ticket.status, '') not like 'MANUAL%' 
 
 and  SGI.status =locate.status  --QM-503
 and  SGI.active = 1 --QM-503
 and  sg.active=1 --QM-503
 and  c.call_center = @CallCenter

 and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser') 
  or @ParsedLocatesOnly = 0) 
 
 and not exists (select * from response_log rl 
    where rl.reply = '(ftp waiting)' 
    and rl.locate_id = rq.locate_id 
    and DateDiff(minute, rl.response_date, GetDate()) < @FTPRetryMinutes) 
 
 and not exists (select * from response_log rl2 
    where rl2.reply = '(email waiting)' 
    and rl2.response_id = rq.in_progress_response_id 
    and rl2.locate_id = rq.locate_id 
    and DateDiff(minute, rl2.response_date, GetDate()) < @EmailRetryMinutes) 
 
 and ((DateDiff(minute, rq.insert_date, GetDate()) > @ResponseDelayMinutes) 
  or @ResponseDelayMinutes = 0) 
 
 -- todo(dan) remove ParsedLocatesOnly option, after all uses replaced by this 
 and ((locate.added_by in (select S from StringListToTable(@LocateAddedBys))) 
  or (@LocateAddedBys = '*')) 
 
order by insert_date 


 
