USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[[GetNextManTicketNumber]]    Script Date: 10/10/2020 3:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- Author:        Larry Killen 
-- Create date:   10/10/2020
-- Description:   returns an integer value for the next manual ticket number.
-- This is the value of the new Manual ticket number.  DO NOT INCREMENT!!!  sr
-- Note: Using [StringSplit] since 2014 does not support [String_Split()]
-- ============================================= 
Create FUNCTION [dbo].[GetNextManTicketNumber]()
RETURNS  Int
as
BEGIN 
	Declare @LastTicketNumber  varChar(20);
	Declare @Result int;

	set @LastTicketNumber = (select ticket_number 
							from ticket 
							where ticket_id = (	select Max(ticket_id) 
												from ticket 
												where ticket_number like '%MAN%'))

	Set @Result = (select cast(value as int)+1 as NextManTktNo from [dbo].[StringSplit]( @LastTicketNumber,'-') 
	where value not in (
	select top 2 value from [dbo].[StringSplit]( @LastTicketNumber,'-') ))
	RETURN @Result
End
