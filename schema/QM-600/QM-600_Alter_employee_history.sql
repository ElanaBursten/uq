USE [QM]

ALTER TABLE dbo.employee_history ADD
	terminated bit NULL,
	termination_date datetime NULL
GO
