
<?xml version="1.0"?>
<!-- 
Stylesheet : LAM01_WO.xsl
Description: Transformation for Lambert XML Work Orders
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8" />
<xsl:template match="/">
<html>
<body>  
  <h1>Work Order&#xA;</h1>
<xsl:for-each select="/XML">

      <xsl:text>&#xA;Internal Number: </xsl:text> 
      <xsl:value-of select="//INTERNALNUMBER"/> 
      <xsl:text>&#xA;Type: </xsl:text>
      <xsl:value-of select="TYPE"/>
      <xsl:text>&#xA;Job ID: </xsl:text>
      <xsl:value-of select="//JOBID"/>
      <xsl:text>&#xA;Order Number: </xsl:text>
      <xsl:value-of select="//ORDERNUMBER"/>
      <xsl:text>&#xA;Transmit Date: </xsl:text>
      <xsl:value-of select="//TRANSMITDATE"/>
      <xsl:text>&#xA;Date Due: </xsl:text>
      <xsl:value-of select="//DUEDATE"/>
      <xsl:text>&#xA;Master Order Number: </xsl:text>
      <xsl:value-of select="//MON"/>
      <xsl:text>&#xA;Issue Date: </xsl:text>
      <xsl:value-of select="//ISSUEDATE"/>
      <xsl:text>&#xA;</xsl:text>
      <xsl:text>&#xA;Customer Name: </xsl:text>
      <xsl:value-of select="//CUSTOMERNAME"/>
      <xsl:text>&#xA;Customer Address: </xsl:text>
      <xsl:value-of select="//CUSTOMERADDRESS"/>
      <xsl:text>&#xA;County: </xsl:text>
      <xsl:value-of select="//CUSTOMERCOUNTY"/>
      <xsl:text>     City: </xsl:text>
      <xsl:value-of select="//CUSTOMERCITY"/>
      <xsl:text>&#xA;State: </xsl:text>
      <xsl:value-of select="//CUSTOMERSTATE"/>
      <xsl:text>     Zip: </xsl:text>
      <xsl:value-of select="//CUSTOMERZIP"/>
      <xsl:text>&#xA;Phone Number: </xsl:text>
      <xsl:value-of select="//CUSTOMERCONTACTNUMBER"/>
      <xsl:text>&#xA;Best Number:  </xsl:text>
      <xsl:value-of select="//CUSTOMERBTN"/>
      <xsl:text>&#xA;</xsl:text>
      <xsl:text>&#xA;Map Page: </xsl:text>
      <xsl:value-of select="//MAP"/>
      <xsl:text>     Grid: </xsl:text>
      <xsl:value-of select="//GRID"/>
      <xsl:text>&#xA;</xsl:text>
      <xsl:text>&#xA;Work Type: </xsl:text>
      <xsl:value-of select="//DROPTYPE"/>
      <xsl:text>&#xA;Extent of Work: </xsl:text>
      <xsl:value-of select="//DEFAULTEXTENTOFWORK"/>
     <xsl:text>&#xA;Wirecenter: </xsl:text>
      <xsl:value-of select="//WIRECENTER"/>
      <xsl:text>&#xA;Central Office: </xsl:text>
      <xsl:value-of select="//CO"/>
      <xsl:text>&#xA;Serving Terminal: </xsl:text>
      <xsl:value-of select="//SERVINGTERMINAL"/>
      <xsl:text>&#xA;Terminal GPS: </xsl:text>
      <xsl:value-of select="//TERMINALGPS"/>
      <xsl:text>&#xA;Circuit ID: </xsl:text>
      <xsl:value-of select="//CIRCUITID"/>
      <xsl:text>&#xA;F2 Cable: </xsl:text>
      <xsl:value-of select="//F2CABLE"/>
      <xsl:text>&#xA;Terminal Port: </xsl:text>
      <xsl:value-of select="//TERMINALPORT"/>
      <xsl:text>&#xA;F2 Pair: </xsl:text>
      <xsl:value-of select="//F2PAIR"/>
      <xsl:text>&#xA;</xsl:text>
      <xsl:text>&#xA;Verizon Attachment Included?: </xsl:text>
      <xsl:value-of select="//ATTACHMENTINCLUDED"/>
</xsl:for-each> 
</body> 
</html>

</xsl:template>
</xsl:stylesheet>
