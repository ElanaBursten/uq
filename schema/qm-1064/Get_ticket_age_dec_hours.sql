
if object_id('dbo.[get_ticket_age_dec_hours]') is not null
  drop function dbo.[get_ticket_age_dec_hours]
go

--QM-1064 designed for Emergency Alerts Larry Killen, XSI
-- returns decimal hours

create function [dbo].[get_ticket_age_dec_hours] (
  @TicketID integer
)
returns DECIMAL(10, 2) --age hours based on Call Center time offset
as
begin
  declare @AgeInDecHours DECIMAL(10, 2),
  @AgeInMins integer
  set @AgeInMins =0 
  set @AgeInDecHours = 0.0
--

  set @AgeInMins = (
  select (
          case 
		    when cc.time_zone = 1198 --PST 
			  then datediff(mi,transmit_date,getdate())-3 
            when cc.time_zone = 1199 --EST
              then datediff(mi,transmit_date,getdate())
            when cc.time_zone = 1200 --CST
              then datediff(mi,transmit_date,getdate())-1
            when cc.time_zone = 1203 --MST
              then datediff(mi,transmit_date,getdate())-2
            when cc.time_zone = null --unknown, default
              then datediff(mi,transmit_date,getdate())
          end)
 from ticket t with (nolock)
   join call_center cc on cc_code = t.ticket_format 
 where ticket_id = @TicketID)
  SET @AgeInDecHours = CAST(@AgeInMins AS DECIMAL(10, 2)) / 60.0;
  return @AgeInDecHours
end

GO

----------------------------------
