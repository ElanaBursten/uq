USE [QM]
GO

/****** Object:  Table [dbo].[employee_plus_history]    Script Date: 7/1/2023 1:23:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[employee_plus_history](
	[emp_id] [int] NOT NULL,
	[term_group_id] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[modified_by_id] [int] NOT NULL,
	[util] [varchar](15) NULL,
	[operation] [varchar](10) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[employee_plus_history] ADD  CONSTRAINT [DF_employee_plus_history_active]  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[employee_plus_history] ADD  CONSTRAINT [DF_employee_plus_history_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO


