USE [QM]
GO
/****** Object:  Table [dbo].[customer_locator]    Script Date: 6/10/2021 4:50:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer_locator](
	[customer_locator_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_id] [int] NOT NULL,
	[loc_emp_id] [int] NOT NULL,
	[RoBoSource] [nvarchar](256) NULL,
	[RoBoDest] [nvarchar](256) NULL,
	[RoBoOptions] [nvarchar](256) NULL,
	[RoBoFiles] [nvarchar](256) NULL,
	[RoBoLogs] [nvarchar](256) NULL,
	[active] [bit] NULL,
	[modified_date] [datetime] NOT NULL,
	[copyStart] [datetime] NULL,
	[copyFinish] [datetime] NULL,
	[fileCount] [int] NULL,
 CONSTRAINT [PK_customer_locator] PRIMARY KEY CLUSTERED 
(
	[customer_locator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[customer_locator] ON 

INSERT [dbo].[customer_locator] ([customer_locator_id], [customer_id], [loc_emp_id], [RoBoSource], [RoBoDest], [RoBoOptions], [RoBoFiles], [RoBoLogs], [active], [modified_date], [copyStart], [copyFinish], [fileCount]) VALUES (4, 2143, 3512, NULL, NULL, NULL, NULL, NULL, 1, CAST(N'2021-06-10T16:41:06.713' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[customer_locator] OFF
GO
/****** Object:  Trigger [dbo].[cust_locator_iu]    Script Date: 6/10/2021 4:50:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Larry Killen
-- Create date: 6/10/2021
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[cust_locator_iu] 
   ON  [dbo].[customer_locator] 
   AFTER INSERT,UPDATE
AS 
BEGIN
  If SYSTEM_USER = 'no_trigger'
  return
	SET NOCOUNT ON;

   update customer_locator
   set modified_date = GetDate()
   from inserted I
   where [customer_locator].customer_locator_id=I.customer_locator_id

END
GO
ALTER TABLE [dbo].[customer_locator] ENABLE TRIGGER [cust_locator_iu]
GO
