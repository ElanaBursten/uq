# test_export_data.py

from zipfile import ZipFile
import csv
import os
import shutil
import shutil
import tempfile
import unittest
#
import export_data
import datadir
import config

class TestExportData(unittest.TestCase):

    def setUp(self):
        cfg = config.Configuration()

        # if there is a valid data dir, use it
        if datadir.datadir.path:
            adodb = cfg.ado_database
            self.server = adodb['host']
            self.db = adodb['database']
            self.user = adodb['login']
            self.password = adodb['password']
        else:
            self.server = "OVERTON"
            self.db = "TestDB"
            self.user = "sa"
            self.password = "rebecca2"

        if os.path.exists("exportdata.zip"):
            os.remove("exportdata.zip")

    def test_fix_area(self):
        """ Test the fix_area method with space. """
        data = [["403","Highlands","600","2136"],
                ["403"," ","600","2136"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_area, 'area')
        fd = open(fname)
        r = csv.DictReader(fd,['area_id','area_name','map_id','locator_id'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['area_name'],"Highlands")
        self.assertEqual(rows[1]['area_name'],"-")

    def test_fix_area_2(self):
        """ Test the fix_area method with null. """
        data = [["403","Highlands","600","2136"],
                ["403","","600","2136"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_area, 'area')
        fd = open(fname)
        r = csv.DictReader(fd,['area_id','area_name','map_id','locator_id'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['area_name'],"Highlands")
        self.assertEqual(rows[1]['area_name'],"-")

    def __test_fix_area_3(self):
        """ Test the fix_area method with with a known bad file"""
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        if not os.path.exists(de.tempdir):
            os.makedirs(de.tempdir)
        shutil.copy("area_bad.tsv",os.path.join(de.tempdir,"area_bad.tsv"))
        de.process_file("area_bad.tsv",de.fix_area, 'area')
        fd = open(os.path.join(de.tempdir,"area_bad.tsv"))
        r = csv.DictReader(fd,['area_id','area_name','map_id','locator_id'],delimiter="\t")
        for i,a in enumerate(r):
            self.assert_(a['area_name'],"area_name null at line %d" % (i,))
        fd.close()
        shutil.rmtree(de.tempdir)

    def test_strip_email(self):
        """ Test the strip_email method. """
        data = [["Atlanta",
                 "Utilities Protection Center (Atlanta)",
                 "10/4/2007 10:56:10 AM",
                 "True",
                 "joe@biz.com",
                 "hj.mcginness@utiliquest.com; kay.bixby@utiliquest.com",
                 "joe@biz.com",
                 "joe@biz.com",
                 "joe@biz.com",
                 "False",
                 "joe@biz.com",
                 "joe@biz.com",
                 "",
                 "True",
                 "",
                 "False"],]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.strip_emails, "call_center")
        fd = open(fname)
        r = csv.DictReader(fd,['cc_code',
                               'cc_name',
                               'modified_date',
                               'active',
                               'admin_email',
                               'emergency_email',
                               'summary_email',
                               'message_email',
                               'warning_email',
                               'uses_wp',
                               'responder_email',
                               'noclient_email',
                               'audit_method',
                               'use_prerouting',
                               'notes',
                               'track_arrivals'],
                               delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(len(rows[0]['admin_email']),0)
        self.assertEqual(len(rows[0]['summary_email']),0)
        self.assertEqual(len(rows[0]['message_email']),0)
        self.assertEqual(len(rows[0]['warning_email']),0)
        self.assertEqual(len(rows[0]['responder_email']),0)
        self.assertEqual(len(rows[0]['noclient_email']),0)

    def test_fix_map_trs(self):
        """ Test the fix_map_trs method with space. """
        data = [["01N","071W","10","1337","CO","FCO1"],
                [" "," ","          ","1337","CO","FCO1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_map_trs, "map_trs")
        fd = open(fname)
        r = csv.DictReader(fd,['township','range','section','area_id','basecode','call_center'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['township'],"01N")
        self.assertEqual(rows[0]['range'],"071W")
        self.assertEqual(rows[0]['section'],"10")
        self.assertEqual(rows[1]['township'],"-")
        self.assertEqual(rows[1]['range'],"-")
        self.assertEqual(rows[1]['section'],"-")

    def test_fix_map_trs_2(self):
        """ Test the fix_map_trs method with nulls. """
        data = [["01N","071W","10","1337","CO","FCO1"],
                ["","","","1337","CO","FCO1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_map_trs, "map_trs")
        fd = open(fname)
        r = csv.DictReader(fd,['township','range','section','area_id','basecode','call_center'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['township'],"01N")
        self.assertEqual(rows[0]['range'],"071W")
        self.assertEqual(rows[0]['section'],"10")
        self.assertEqual(rows[1]['township'],"-")
        self.assertEqual(rows[1]['range'],"-")
        self.assertEqual(rows[1]['section'],"-")

    def test_fix_county_area(self):
        """ Test the fix_county_area method with space. """
        data = [["901","602","NJ","MONMOUTH","FAIR HAVEN","414","NewJersey"],
                ["901","602","NJ","MONMOUTH"," ","414","NewJersey"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_county_area, "county_area")
        fd = open(fname)
        r = csv.DictReader(fd,['county_area_id','map_id','state','county','munic','area_id','call_center'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['munic'],"FAIR HAVEN")
        self.assertEqual(rows[1]['munic'],"-")

    def test_fix_county_area_2(self):
        """ Test the fix_county_area method with nulls. """
        data = [["901","602","NJ","MONMOUTH","FAIR HAVEN","414","NewJersey"],
                ["901","602","NJ","MONMOUTH","","414","NewJersey"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_county_area, "county_area")
        fd = open(fname)
        r = csv.DictReader(fd,['county_area_id','map_id','state','county','munic','area_id','call_center'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['munic'],"FAIR HAVEN")
        self.assertEqual(rows[1]['munic'],"-")

    def test_fix_status_group(self):
        """ Test the fix_status_group method with space. """
        data = [["94000","SCA-NCA_StatusList"," ","1"],
                ["94000","       "," ","1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_status_group, "status_group")
        fd = open(fname)
        r = csv.DictReader(fd,['sg_id','sg_name','modified_date','active'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['sg_name'],"SCA-NCA_StatusList")
        self.assertEqual(rows[1]['sg_name'],"-")

    def test_fix_status_group_2(self):
        """ Test the fix_status_group method with null. """
        data = [["94000","SCA-NCA_StatusList"," ","1"],
                ["94000","","","1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_status_group, "status_group")
        fd = open(fname)
        r = csv.DictReader(fd,['sg_id','sg_name','modified_date','active'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['sg_name'],"SCA-NCA_StatusList")
        self.assertEqual(rows[1]['sg_name'],"-")

    def test_fix_map(self):
        """ Test the fix_map method with space. """
        data = [["602","NJ.MAIN","NJ","New Jersey","FNJ1"],
                ["602"," ","NJ","New Jersey","FNJ1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_map, "map")
        fd = open(fname)
        r = csv.DictReader(fd,['map_id','map_name','state','county','ticket_code'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['map_name'],"NJ.MAIN")
        self.assertEqual(rows[1]['map_name'],"-")

    def test_fix_map_2(self):
        """ Test the fix_map method with null. """
        data = [["602","NJ.MAIN","NJ","New Jersey","FNJ1"],
                ["602","","NJ","New Jersey","FNJ1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_map, "map")
        fd = open(fname)
        r = csv.DictReader(fd,['map_id','map_name','state','county','ticket_code'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['map_name'],"NJ.MAIN")
        self.assertEqual(rows[1]['map_name'],"-")

    def test_fix_term_group(self):
        """ Test the fix_term_group method with space. """
        data = [["9010"," ","BRNoChargeTermsGulfgate"," "," ","1"],
                ["9010","    ","   ","  "," ","1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_term_group,"term_group")
        fd = open(fname)
        r = csv.DictReader(fd,['term_group_id','center_group_id','group_code','comment','modified_date','active'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['group_code'],"BRNoChargeTermsGulfgate")
        self.assertEqual(rows[1]['group_code'],"-")

    def test_fix_term_group_2(self):
        """ Test the fix_term_group method with null. """
        data = [["9010"," ","BRNoChargeTermsGulfgate"," "," ","1"],
                ["9010","","","","","1"]]
        fname = tempfile.mktemp()
        fd = open(fname,"w")
        w = csv.writer(fd,delimiter="\t",quoting=csv.QUOTE_NONE)
        w.writerows(data)
        fd.close()
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.process_file(fname,de.fix_term_group,"term_group")
        fd = open(fname)
        r = csv.DictReader(fd,['term_group_id','center_group_id','group_code','comment','modified_date','active'],delimiter="\t")
        rows = []
        for a in r:
            rows.append(a)
        self.assertEqual(rows[0]['group_code'],"BRNoChargeTermsGulfgate")
        self.assertEqual(rows[1]['group_code'],"-")

    def test_all(self):
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      os.path.join(os.getcwd(),"export_temp"),
                                      os.path.join(os.getcwd(),"export_target"),
                                      noscrub=True)
        de.run()
        z = ZipFile(os.path.join("export_target","exportdata.zip"),"r")
        names = z.namelist()
        name_dict = {}
        for name in names:
            name_dict[name] = z.getinfo(name).file_size
        for row in export_data.export_data:
            size = os.stat(os.path.join("export_temp",row[1])).st_size
            self.assertEqual(name_dict[row[1]],size)
        cont = z.read('center_group.tsv')
        import cStringIO
        s = cStringIO.StringIO(cont)
        lines = s.readlines()
        last_record = lines[-2].split('\t')
        # the last record is now 'NoResponseCallcenters'?
        self.assertEquals(last_record[0],'10097')
        self.assertEquals(last_record[1],'141')
        self.assertEquals(last_record[2],'LALA (141)')
        self.assertEquals(last_record[3],'1')
        self.assertEquals(last_record[4],'')
        self.assertEquals(last_record[5].strip(),'1')

    def test_limit_large(self):
        temp_dir = os.path.join(os.getcwd(),"export_temp_test_limit_large")
        target_dir = os.path.join(os.getcwd(),"export_target_test_limit_large")
        export_data.export_data[0] = ("map_cell", "map_cell.tsv", "\\t", 1, 500,("map_id","map_page_num","x_part","y_part"))
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      temp_dir,
                                      target_dir, noscrub=True)
        de.run()
        z = ZipFile(os.path.join(target_dir,"exportdata.zip"), "r")

        size = os.stat(os.path.join(temp_dir, "map_cell.tsv")).st_size
        self.assertEqual(z.getinfo("map_cell.tsv").file_size,size)
        f = open(os.path.join(temp_dir, "map_cell.tsv"))
        lines = f.readlines()
        self.assertEqual(len(lines), 500)

    def test_skiplarge(self):
        temp_dir = os.path.join(os.getcwd(),"export_temp_test_limit_large")
        target_dir = os.path.join(os.getcwd(),"export_target_test_limit_large")
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      temp_dir,
                                      target_dir,
                                      skiplarge=True, noscrub=True)
        de.run()
        z = ZipFile(os.path.join(target_dir,"exportdata.zip"),"r")
        num_files = len(z.infolist())
        should_be = 0
        for tablename, filename, separator, large, num_rows, order in export_data.export_data:
            if not large:
                should_be += 1
        self.assertEqual(should_be,num_files)

    def test_read_table_attributes(self):
        # first, create a TSV file with field info
        temp_dir = os.path.join(os.getcwd(), "export_temp_test_limit_large")
        target_dir = os.path.join(os.getcwd(), "export_target_test_limit_large")
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      temp_dir, target_dir, skiplarge=True,
                                      noscrub=True)
        de.table_attributes('users')

        # does file exist?
        self.assert_(os.path.exists(os.path.join(temp_dir, 'fields-users.tsv')))

        fields = de.read_table_attributes('users')
        # actual positions may change if table definition changes, but this is
        # unlikely, and will affect only this test
        self.assertEquals(fields['uid'], 0)
        self.assertEquals(fields['first_name'], 3)
        self.assertEquals(fields['password'], 6)

    def test_data_scrubbing_name_generation(self):
        import scrub_data
        scrubber = scrub_data.Scrubber('.')

        key = 'UQ rules'
        self.assertEquals(scrubber.gen_random_first_name(key), 'Magdalen')
        self.assertEquals(scrubber.gen_random_last_name(key), 'Mclemore')
        self.assertEquals(scrubber.gen_random_id(key), '099398')
        self.assertEquals(scrubber.gen_random_id(key, 7), '3876614')
        self.assertEquals(scrubber.gen_random_phone(key), '(553) 487-3606')
        self.assertEquals(scrubber.gen_random_street(key), '73606 Mclemore Ln')
        self.assertEquals(scrubber.gen_random_email(key), 'magdalen@sample.com')

    def test_data_scrubbing(self):
        temp_dir = os.path.join(os.getcwd(),"export_temp_scrubbing")
        target_dir = os.path.join(os.getcwd(),"export_target_scrubbing")
        de = export_data.DataExporter(self.server, self.db, self.user, self.password,
                                      temp_dir,
                                      target_dir, skiptar=True,
                                      skiplarge=False, noscrub=False)
        de.run()
        #z = ZipFile(os.path.join(target_dir,"exportdata.zip"),"r")

        # XXX add tests...


def suite():
    s = unittest.makeSuite(TestExportData)
    return unittest.TestSuite([s])

if __name__ == "__main__":

    unittest.main()
