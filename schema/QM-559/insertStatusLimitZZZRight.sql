INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc])
     VALUES
           ('Tickets - Super Restricted Status'
           ,'General'
           ,'TicketDetailStatus'
           ,'Add restricted statuses in single quotes, no spaces. i.e. ''NC'',''ZZZ'',''O'' ')
GO



