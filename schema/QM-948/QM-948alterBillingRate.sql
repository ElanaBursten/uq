/*
   Tuesday, August 20, 20241:07:35 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.billing_rate.additional_CWICode', N'Tmp_AddlCWICode', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.billing_rate.Tmp_AddlCWICode', N'AddlCWICode', 'COLUMN' 
GO
ALTER TABLE dbo.billing_rate SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_rate', 'Object', 'CONTROL') as Contr_Per 