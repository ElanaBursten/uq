/*
   Sunday, December 11, 202212:04:56 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.admin_restriction ADD
	active bit NOT NULL CONSTRAINT DF_admin_restriction_active DEFAULT 1
GO
ALTER TABLE dbo.admin_restriction SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'CONTROL') as Contr_Per 