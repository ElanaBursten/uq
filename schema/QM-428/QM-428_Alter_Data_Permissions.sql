--QM-248 Open Tickets (and Past Due) on Tree
if not exists(select * from right_definition where entity_data='TicketsOpenTree')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Show Open Ticket & Past Due on Tree', 
						'General', 
						'TicketsOpenTree', 
						'Limitation does not apply to this right', 
						GetDate());
GO





