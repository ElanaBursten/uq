if object_id('dbo.get_open_ticket_list') is not null
  drop procedure dbo.get_open_ticket_list
go

create procedure dbo.get_open_ticket_list(
  @ManagerID Int, 
  @MaxDays Int,
  @toplevel bit = 0)
as

DECLARE @opentickets TABLE (
ManagerID int not null,
short_name varchar(30) null,
tkt_count int,
loc_count int 
);

insert into @opentickets

select 
  @ManagerID as manager_ID, 
  (select short_name from employee where emp_id = @ManagerID) as short_name,
  ISNULL((select sum(open_tkt_count) from dbo.get_hier_open(@ManagerID, 0, 10)), 0) as tkt_count,
  ISNULL((select sum(open_loc_count) from dbo.get_hier_open(@ManagerID, 0, 10)), 0) as loc_count


select 'opentickets' as tname, * from @opentickets

Go

  get_open_ticket_list 2694, 0

  Select * from employee where last_name = 'Walters'