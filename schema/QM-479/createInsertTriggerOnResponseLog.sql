SET ANSI_NULLS ON
GO                                                                  
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Larry
-- Create date: 10/6/2021
-- Description:	puts a record in the multi-responder queue for BSCA Success responses
-- QM-479 Add a copy to the multi-responder queue if it had a succesful singlular response.
-- =============================================
CREATE TRIGGER [dbo].[insIntoMultiQ] 
   ON  [dbo].[response_log] 
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    Insert into [dbo].[responder_multi_queue]([respond_to],[locate_id],[insert_date],[ticket_format],[client_code])
	Select 'alcs_ga' as [respond_to],I.locate_id, [response_date] as [insert_date], [call_center] as [ticket_format], L.client_code
	from Inserted I
	join Locate L on (L.locate_id = I.locate_id)
	where L.client_code = 'BSCA'
	and reply='Success'

END
GO
