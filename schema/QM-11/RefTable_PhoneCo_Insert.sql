Use [QM]

insert into dbo.reference (type, code, description, sortby, active_ind)
values ('phoneco', 'ATT', '@txt.att.net', 1, 1)

insert into dbo.reference (type, code, description, sortby, active_ind)
values ('phoneco', 'VER', '@vtext.com', 2, 1)

insert into dbo.reference (type, code, description, sortby, active_ind)
values ('phoneco', 'Sprint', '@messaging.sprintpcs.com', 3, 1)

--Select *
--from reference
--where type = 'phoneco'
