
/* Notes about this build: The table may already exist, but the trigger needs to be present (it is in alter script).
/* We are gradually moving the uq_schema into separate files to make it more manageable. This has not been done for this JIRA
/*     These files should be run in this order:
ALTERS:QMANTWO-163_alter_schema.sql
SP:trigger_audit_due_date_rule.sql



/* DIRECTIONS FOR CREATING THIS FILE:
/* This is a list of scripts for the Build that will be pulled into ScriptsForBuild.zip 
/* If you need to include any comments or notes, include /* at the beginning of the line
/* The format is the subdirectory of the Schema file, colon as the delimiter and name of file
/* This file is processed by SchemaHelper application which parses the information to create zip
/* If you need to reference another directory path, include it in the schemahelper.ini
