/*
   Monday, March 21, 20226:29:20 AM
   User: 
   Server: BRIAN-PC
   Database: TestDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.respond_to ADD CONSTRAINT
	DF_respond_to_modified_date DEFAULT getdate() FOR modified_date
GO
ALTER TABLE dbo.respond_to ADD CONSTRAINT
	DF_respond_to_active DEFAULT 1 FOR active
GO
ALTER TABLE dbo.respond_to SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.respond_to', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.respond_to', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.respond_to', 'Object', 'CONTROL') as Contr_Per 