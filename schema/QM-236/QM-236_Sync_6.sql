--QM-236 Also, added RECOMPILE
--QM-164 Plat Mgr Changes: Adding some of the employee fields that were not shown
--  Fields should show for all active employees

if object_id('dbo.sync_6') is not null
  drop procedure dbo.sync_6
go

create proc [dbo].[sync_6] (
  @EmployeeID int,
  @UpdatesSinceS varchar(30),
  @ClientTicketListS text,
  @ClientDamageListS varchar(MAX),
  @ClientWorkOrderListS varchar(MAX)
) WITH RECOMPILE 
as
  set nocount on

  --Work orders temp tables
  DECLARE @workorders TABLE (wo_id integer not null primary key)
  DECLARE @mod_workorders TABLE (wo_id integer not null primary key)
  DECLARE @client_has_workorders TABLE (client_wo_id integer not null primary key)
  DECLARE @should_have_workorders TABLE (should_wo_id integer not null primary key)
  DECLARE @related_tickets TABLE (ticket_id integer not null primary key)

  --Damages temp tables
  DECLARE @damages TABLE (damage_id integer not null primary key)
  DECLARE @mod_damages TABLE (damage_id integer not null primary key)
  DECLARE @client_has_damages TABLE (client_damage_id integer not null primary key)
  DECLARE @should_have_damages TABLE (should_damage_id integer not null primary key)

  DECLARE @tix TABLE (ticket_id integer not null primary key)
  DECLARE @modtix TABLE (ticket_id integer not null primary key)
  DECLARE @closed_since datetime
  DECLARE @oldest_l int
  DECLARE @UpdatesSince datetime

  DECLARE @client_has_tix TABLE (client_ticket_id integer not null primary key)
  DECLARE @should_have_tix TABLE (should_ticket_id integer not null primary key)
  DECLARE @current_pc varchar(15)

  set @UpdatesSince = convert(datetime, @UpdatesSinceS)

  -- Go back 2 minutes, to reduce update overlap errors
  set @UpdatesSince = dateadd(s, -120, @UpdatesSince)

/* Use this in emergency situations to disable initial syncs
  if @UpdatesSince < '2000-01-01'
  begin
    RAISERROR ('Initial syncs temporarily disabled', 50005, 1) with log
    return
  end
*/

  select 'row' as tname, getdate() as SyncDateTime

  select 'office' as tname, * from office
    where modified_date > @UpdatesSince

  select 'call_center' as tname, call_center.cc_code, cc_name, active, track_arrivals,
   xml_ticket_format, coalesce(call_center_contact.phone, '') as alert_phone_no, allowed_work
   from call_center
   left join call_center_contact on call_center.cc_code = call_center_contact.cc_code
    and call_center_contact.name='ALERT'
   where modified_date > @UpdatesSince

  select 'statuslist' as tname, * from statuslist where modified_date > @UpdatesSince

  select 'reference' as tname, * from reference where modified_date > @UpdatesSince

  select 'profit_center' as tname, * from profit_center where modified_date > @UpdatesSince

  select 'customer' as tname, * from customer where modified_date > @UpdatesSince

  select 'center_group' as tname, * from center_group where modified_date > @UpdatesSince

  select 'center_group_detail' as tname, * from center_group_detail where modified_date > @UpdatesSince

  select 'client' as tname, * from client where modified_date > @UpdatesSince

select 'client_info' as tname, * from client_info where modified_date > @UpdatesSince  --QMANTWO-676

--configuration_data : Each required QM value is explicitly defined.  Important Note: DBISAM receives this as CASE-SENSITIVE
 select 'configuration_data' as tname, * from configuration_data
  where (modified_date > @UpdatesSince) and (sync=1)
 -- We no longer specify them individually.  Just mark them sync=1 so that they will be sync'd down
 --   and ((Name in ('ClientURL', 'TicketActRefreshSeconds', 'TicketChangeViewLimitsMax',
 --   'UpdateFileName', 'UpdateFileHash', 'QMClientHttpPort', 'PTOHoursEntryStartDate', 
	--'CenBaseUrl', 'EPR_Pepper', 'RemBaseURL','TEMABaseUrl', 'UtilisafeBaseUrl', -- QM-53, QM-100
	--'override_units', 'SCANA_BaseURL', 'INR_BaseURL', 'JobSafetyURL','JSASafetyTktURL', 'IN_QCard'))  --QMANTWO-434 QMANTWO-591 QMANTWO-753 
 --   or (Name like 'AttachWarning%'))

  --QMANTWO-552
  select 'api_storage_constants' as tname, * from api_storage_constants --where (modified_date > @UpdatesSince)
  select 'api_storage_credentials' as tname, * from api_storage_credentials --where (modified_date > @UpdatesSince)

  select 'highlight_rule' as tname, * from highlight_rule where modified_date > @UpdatesSince

  select 'employee' as tname, emp_id, type_id, status_id, timesheet_id,
    emp_number, short_name, first_name, last_name, report_to, active, company_car, timerule_id,
    repr_pc_code, payroll_pc_code, company_id, ticket_view_limit, show_future_tickets, 
    contact_phone, local_utc_bias, phoneCo_ref,
    plat_update_date, plat_age, changeby   --QM-164
   from employee where modified_date > @UpdatesSince

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, login_id, chg_pwd,
    last_login, active_ind, can_view_notes, can_view_history, chg_pwd_date, api_key
   from users where emp_id = @EmployeeID  -- Myself only

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, active_ind
   from users where emp_id <> @EmployeeID -- Everyone else
   and  modified_date > @UpdatesSince

  select 'locating_company' as tname, * from locating_company
   where modified_date > @UpdatesSince

  select 'carrier' as tname, * from carrier
   where modified_date > @UpdatesSince

  select 'followup_ticket_type' as tname, * from followup_ticket_type
   where modified_date > @UpdatesSince

  set @current_pc = dbo.get_historical_pc(@EmployeeID, GetDate())
  if @current_pc is null set @current_pc = dbo.get_employee_pc(@EmployeeID, 0)

  select 'employee' as tname, emp_id, charge_cov, timerule_id,
    timesheet_id, company_car, effective_pc = @current_pc,
    first_name, last_name
  from employee
  where (emp_id = @EmployeeID)
    and (modified_date > @UpdatesSince or dbo.get_historical_pc(emp_id, @UpdatesSince) <> @current_pc)

  select 'employee_right' as tname, * from employee_right
    where emp_id=@EmployeeID and modified_date > @UpdatesSince

  if ((select rights_modified_date from employee where emp_id = @EmployeeID) > @UpdatesSince)
    select 'emp_group_right' as tname, * from dbo.get_emp_group_rights(@EmployeeID)

  select 'timesheet_entry' as tname, *
  from timesheet_entry
  where
    work_emp_id = @EmployeeID and
    (((modified_date > @UpdatesSince) and (work_date > dateadd(Day, -38, getdate())))
    or
    ((work_date > dateadd(year,datediff(year,0,getdate()),0)) and (floating_holiday = 1)))

  select 'asset_type' as tname, * from asset_type
  where modified_date > @UpdatesSince

  select 'asset_assignment' as tname, * from asset_assignment
  where emp_id = @EmployeeID
    and modified_date > @UpdatesSince

  select 'asset' as asset, * from asset
  where asset_id in (select asset_id from asset_assignment where emp_id = @EmployeeID)
    and modified_date > @UpdatesSince

  select 'upload_location' as tname, * from upload_location
  where modified_date > @UpdatesSince

  select 'upload_file_type' as tname, * from upload_file_type
  where modified_date > @UpdatesSince

  select 'damage_default_est' as tname, * from damage_default_est
  where modified_date > @UpdatesSince

  select 'status_group' as tname, * from status_group
  where modified_date > @UpdatesSince

  select 'status_group_item' as tname, * from status_group_item
  where modified_date > @UpdatesSince

  select 'call_center_hp' as tname, * from call_center_hp
  where modified_date > @UpdatesSince

  select 'billing_unit_conversion' as tname, * from billing_unit_conversion
  where modified_date > @UpdatesSince

  select 'billing_gl' as tname, * from billing_gl
  where modified_date > @UpdatesSince

  select 'area' as tname, * from area
  where map_id = (select value from configuration_data where name = 'AssignableAreaMapID')
    and modified_date > @UpdatesSince

  select 'billing_output_config' as tname, output_config_id, customer_id, which_invoice
  from billing_output_config where modified_date > @UpdatesSince

  select 'required_document' as tname, * from required_document
    where modified_date > @UpdatesSince

  select 'required_document_type' as tname, * from required_document_type
    where modified_date > @UpdatesSince

  -- Ticket-related data
  select @closed_since = dateadd(day, -5, getdate())

  select @oldest_l = (select top 1 oldest_open_locate from oldest_open)

  -- all open locates, so client can detect it has missing locates
  select 'open_locate' as tname, ticket_id, locate_id, status
   from locate open_locate
   where assigned_to=@EmployeeID

  -- all open tickets, so we can be sure to send them all
  insert into @should_have_tix
  select distinct ticket_id
   from locate
   where assigned_to=@EmployeeID

  -- what the client does have
  insert into @client_has_tix
  select ID from IdListToTable(@ClientTicketListS)

  -- DISABLE the sync of "my old tickets":
  if (1=0) and (@@ROWCOUNT<1 or @UpdatesSince < '2000-01-01')  begin    -- support old clients and init syncs
    if @UpdatesSince < '2000-01-01'  begin
      -- Get all tickets assigned to me even if closed
      insert into @tix  (ticket_id)
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date> @closed_since
      union
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date is null
      union
      select should_ticket_id from @should_have_tix
    end else begin
      -- This is a subsequent sync with the client tic list missing,
      -- must use old alg to get consistent results:
      -- Get all tickets ever assigned to me, this is the SLOW part
      insert into @tix  (ticket_id)
      select distinct locate.ticket_id
       from assignment
       inner join locate on locate.locate_id=assignment.locate_id
          and (locate.closed=0 or locate.closed_date> @closed_since)
       where assignment.locator_id = @EmployeeID
        and assignment.locate_id >= @oldest_l
    end
  end else begin  -- newer clients tell us what they have
    -- they need to know about updates to tickets they are
    -- assigned now, and also tickets still in cache
    insert into @tix  (ticket_id)
      select should_ticket_id from @should_have_tix
      union
      select client_ticket_id from @client_has_tix
  end

  -- subtract what we have, from what we should have
  delete from @should_have_tix where should_ticket_id in
    (select client_ticket_id from @client_has_tix)

  -- Find those that have changed, somewhat slow
  insert into @modtix (ticket_id)
  select t.ticket_id
   from @tix t
    inner join ticket on ticket.ticket_id = t.ticket_id
    and ticket.modified_date >@UpdatesSince
  union
  select locate.ticket_id
   from @tix t2
    inner join locate on locate.ticket_id = t2.ticket_id
   where locate.modified_date > @UpdatesSince
    and locate.status<>'-N'
  union
  select locate.ticket_id
   from @tix t3
    inner join locate on locate.ticket_id = t3.ticket_id
    inner join assignment on locate.locate_id=assignment.locate_id
   where assignment.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @tix t4
    inner join notes on notes.foreign_id = t4.ticket_id
   where notes.foreign_type = 1
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @tix t5
    inner join attachment on attachment.foreign_id = t5.ticket_id
   where attachment.foreign_type = 1
    and attachment.modified_date > @UpdatesSince
  union
    select should_ticket_id from @should_have_tix
  union
  select locate.ticket_id
  from @tix t6
    inner join locate on locate.ticket_id = t6.ticket_id 
    inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes
  where 
    notes.modified_date > @UpdatesSince
  union 
  select ts.ticket_id
    from @tix t7
    inner join task_schedule ts on ts.ticket_id = t7.ticket_id
    where ts.modified_date > @UpdatesSince

  -- now get the whole ticket for just the ones that have changed
  declare @tickets_to_send table (
  [ticket_id] [int] NOT NULL ,
  [ticket_number] [varchar] (20) NOT NULL ,
  [parsed_ok] [bit] NOT NULL ,
  [ticket_format] [varchar] (20) NOT NULL ,
  [kind] [varchar] (20) NOT NULL ,
  [status] [varchar] (20) NULL ,
  [map_page] [varchar] (20) NULL ,
  [revision] [varchar] (20) NOT NULL ,
  [transmit_date] [datetime] NULL ,
  [due_date] [datetime] NULL ,
  [work_description] [varchar] (3500) NULL ,
  [work_state] [varchar] (2) NULL ,
  [work_county] [varchar] (40) NULL ,
  [work_city] [varchar] (40) NULL ,
  [work_address_number] [varchar] (10) NULL ,
  [work_address_number_2] [varchar] (10) NULL ,
  [work_address_street] [varchar] (60) NULL ,
  [work_cross] [varchar] (100) NULL ,
  [work_subdivision] [varchar] (70) NULL ,
  [work_type] [varchar] (90) NULL ,
  [work_date] [datetime] NULL ,
  [work_notc] [varchar] (40) NULL ,
  [work_remarks] [varchar] (1200) NULL ,
  [priority] [varchar] (40) NULL ,
  [legal_date] [datetime] NULL ,
  [legal_good_thru] [datetime] NULL ,
  [legal_restake] [varchar] (40) NULL ,
  [respond_date] [datetime] NULL ,
  [duration] [varchar] (40) NULL ,
  [company] [varchar] (80) NULL ,
  [con_type] [varchar] (50) NULL ,
  [con_name] [varchar] (50) NULL ,
  [con_address] [varchar] (50) NULL ,
  [con_city] [varchar] (40) NULL ,
  [con_state] [varchar] (40) NULL ,
  [con_zip] [varchar] (40) NULL ,
  [call_date] [datetime] NULL ,
  [caller] [varchar] (50) NULL ,
  [caller_contact] [varchar] (50) NULL ,
  [caller_phone] [varchar] (40) NULL ,
  [caller_cellular] [varchar] (40) NULL ,
  [caller_fax] [varchar] (40) NULL ,
  [caller_altcontact] [varchar] (40) NULL ,
  [caller_altphone] [varchar] (40) NULL ,
  [caller_email] [varchar] (40) NULL ,
  [operator] [varchar] (40) NULL ,
  [channel] [varchar] (40) NULL ,
  [work_lat] [decimal](9, 6) NULL ,
  [work_long] [decimal](9, 6) NULL ,
  [geocode_precision] [int] NULL ,
  [image] [text] NOT NULL ,
  [parse_errors] [text] NULL ,
  [modified_date] [datetime] NOT NULL,
  [active] [bit] NOT NULL,
  [ticket_type] [varchar] (38) NULL ,
  [legal_due_date] [datetime] NULL ,
  [parent_ticket_id] [int] NULL ,
  [do_not_mark_before] [datetime] NULL ,
  [route_area_id] [int] NULL ,
  [watch_and_protect] [bit] NULL,
  [service_area_code] [varchar] (40) NULL ,
  [explosives] [varchar] (20) NULL ,
  [serial_number] [varchar] (40) NULL ,
  [map_ref] [varchar] (60) NULL ,
  [followup_type_id] [int] NULL ,
  [do_not_respond_before] [datetime] NULL ,
  [recv_manager_id] [int] NULL ,
  [ward] [varchar] (10) NULL ,
  route_area_name varchar(50) NULL,
  alert varchar(1) NULL,
  start_date datetime NULL,
  end_date datetime NULL,
  points decimal(6,2) NULL,
  work_priority_id int NULL,
  wo_number varChar(40) NULL,   -- QMANTWO-253
  route_order decimal(9,5) NULL --QMANTWO-775
  )

  declare @locates_to_send table (
  [locate_id] [int] NOT NULL ,
  [ticket_id] [int] NOT NULL ,
  [client_code] [varchar] (10) NULL ,
  [client_id] [int] NULL ,
  [status] [varchar] (5) NOT NULL ,
  [high_profile] [bit] NOT NULL ,
  [qty_marked] [int] NULL ,
  [price] [money] NULL ,
  [closed] [bit] NOT NULL ,
  [closed_by_id] [int] NULL ,
  [closed_how] [varchar] (10) NULL ,
  [closed_date] [datetime] NULL ,
  [modified_date] [datetime] NOT NULL ,
  [active] [bit] NOT NULL,
  [invoiced] [bit] NULL ,
  [assigned_to] [int] NULL ,
  [regular_hours] [decimal](5, 2) NULL ,
  [overtime_hours] [decimal](5, 2) NULL ,
  [added_by] [varchar] (8) NULL ,
  [watch_and_protect] [bit] NULL ,
  [high_profile_reason] [int] NULL ,
  [seq_number] [varchar] (20) NULL ,
  [assigned_to_id] [int] NULL ,
  [mark_type] [varchar] (10) NULL ,
  [alert] varchar(1) NULL,
  [locator_id] int,
  length_total int,
  entry_date datetime,
  gps_id int,
  status_changed_by_id int,
  workload_date datetime NULL
  )

  -- gather the ticket and locate data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @tickets_to_send
  select
     ticket.ticket_id,
     ticket.ticket_number,
     ticket.parsed_ok,
     ticket.ticket_format,
     ticket.kind,
     ticket.status,
     ticket.map_page,
     ticket.revision,
     ticket.transmit_date,
     ticket.due_date,
     ticket.work_description,
     ticket.work_state,
     ticket.work_county,
     ticket.work_city,
     ticket.work_address_number,
     ticket.work_address_number_2,
     ticket.work_address_street,
     ticket.work_cross,
     ticket.work_subdivision,
     ticket.work_type,
     ticket.work_date,
     ticket.work_notc,
     ticket.work_remarks,
     ticket.priority,
     ticket.legal_date,
     ticket.legal_good_thru,
     ticket.legal_restake,
     ticket.respond_date,
     ticket.duration,
     ticket.company,
     ticket.con_type,
     ticket.con_name,
     ticket.con_address,
     ticket.con_city,
     ticket.con_state,
     ticket.con_zip,
     ticket.call_date,
     ticket.caller,
     ticket.caller_contact,
     ticket.caller_phone,
     ticket.caller_cellular,
     ticket.caller_fax,
     ticket.caller_altcontact,
     ticket.caller_altphone,
     ticket.caller_email,
     ticket.operator,
     ticket.channel,
     ticket.work_lat,
     ticket.work_long,
     ticket.geocode_precision,
     ticket.image,
     ticket.parse_errors,
     ticket.modified_date,
     ticket.active,
     ticket.ticket_type,
     ticket.legal_due_date,
     ticket.parent_ticket_id,
     ticket.do_not_mark_before,
     ticket.route_area_id,
     ticket.watch_and_protect,
     ticket.service_area_code,
     ticket.explosives,
     ticket.serial_number,
     ticket.map_ref,
     ticket.followup_type_id,
     ticket.do_not_respond_before,
     ticket.recv_manager_id,
     ticket.ward,
     coalesce(area.area_name, '') as area_name,
     ticket.alert,
     -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null:
     null, --     start_date,
     null, --     end_date,
     null, --     points
     ticket.work_priority_id,
	 ticket.wo_number,    -- QMANTWO-253
	 ticket.route_order    --QMANTWO-775
    from @modtix mt
      inner join ticket on mt.ticket_id = ticket.ticket_id
      left join area on area.area_id = ticket.route_area_id

  insert into @locates_to_send
    (locate_id, ticket_id, client_code, client_id, status, high_profile, qty_marked,
     price, closed, closed_by_id, closed_how, closed_date, modified_date, active,
     invoiced, assigned_to, regular_hours, overtime_hours, added_by, watch_and_protect,
     high_profile_reason, seq_number, assigned_to_id, mark_type, alert,
     locator_id, entry_date, gps_id, status_changed_by_id, length_total, workload_date)--todo length_total is not defined in the temp table?
  select
     l.locate_id, l.ticket_id, l.client_code, l.client_id, l.status, l.high_profile, l.qty_marked,
     l.price, l.closed, l.closed_by_id, l.closed_how, l.closed_date, l.modified_date, l.active,
     l.invoiced, l.assigned_to, l.regular_hours, l.overtime_hours, l.added_by, l.watch_and_protect,
     l.high_profile_reason, l.seq_number, l.assigned_to_id, l.mark_type, l.alert, assignment.locator_id, 
     l.entry_date, l.gps_id, l.status_changed_by_id,
     (select sum(lh.units_marked) from locate_hours lh where lh.locate_id = l.locate_id),
     cast(dbo.get_ticket_workload_date(l.ticket_id, assignment.locator_id) as datetime)
    from @modtix mt
      inner join locate l
        on mt.ticket_id = l.ticket_id and l.status <> '-N' and l.status <> '-P'
      inner join assignment
        on l.locate_id=assignment.locate_id and assignment.active = 1

  -- Send the ticket and locate data
  select 'ticket' as tname, * from @tickets_to_send ticket

  select 'locate' as tname, * from @locates_to_send locate

  -- All locate_hours for all the locates being sent
  select 'locate_hours' as tname, locate_hours.*
    from @locates_to_send locate 
    inner join locate_hours on (locate_hours.locate_id = locate.locate_id)


 -----Send down Damages to Client-----

  --All damages for this employee
  insert into @should_have_damages
    select damage_id from damage where investigator_id = @EmployeeID
      and damage_type in ('INCOMING', 'PENDING')
      and active = 1

  --Damages the client does have
  insert into @client_has_damages
    select ID from IdListToTable(@ClientDamageListS)

  -- they need to know about updates to damages they are
  -- assigned now, and also damages still in cache
  insert into @damages (damage_id)
    select should_damage_id from @should_have_damages
    union
    select client_damage_id from @client_has_damages

  -- subtract what we have, from what we should have
  delete from @should_have_damages where should_damage_id in
    (select client_damage_id from @client_has_damages)

  -- Find those that have changed, somewhat slow
  insert into @mod_damages (damage_id)
  select d.damage_id
   from @damages d
    inner join damage on damage.damage_id = d.damage_id
    and damage.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @damages d2
    inner join notes on notes.foreign_id = d2.damage_id
   where notes.foreign_type = 2
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @damages d3
    inner join attachment on attachment.foreign_id = d3.damage_id
   where attachment.foreign_type = 2
    and attachment.modified_date > @UpdatesSince
  union
  select should_damage_id from @should_have_damages

  -- now get the whole damage for just the ones that have changed
  declare @damages_to_send table (
  damage_id int NOT NULL,
  office_id int NULL,
  damage_inv_num varchar(12) NULL,
  damage_type varchar(20) NULL,
  damage_date datetime NULL,
  uq_notified_date datetime NULL,
  notified_by_person varchar(40) NULL,
  notified_by_company varchar(40) NULL,
  notified_by_phone varchar(20) NULL,
  client_code varchar(10) NULL,
  client_id int NULL,
  client_claim_id varchar(30) NULL,
  date_mailed datetime NULL,
  date_faxed datetime NULL,
  sent_to varchar(30) NULL,
  size_type varchar(50) NULL,
  location varchar(80) NULL,
  page varchar(20) NULL,
  grid varchar(20) NULL,
  city varchar(40) NULL,
  county varchar(40) NULL,
  state varchar(2) NULL,
  utility_co_damaged varchar(40) NULL,
  diagram_number int NULL,
  remarks text NULL,
  investigator_id int NULL,
  investigator_arrival datetime NULL,
  investigator_departure datetime NULL,
  investigator_est_damage_time datetime NULL,
  investigator_narrative text NULL,
  excavator_company varchar(30) NULL,
  excavator_type varchar(20) NULL,
  excavation_type varchar(20) NULL,
  locate_marks_present varchar(1) NULL,
  locate_requested varchar(1) NULL,
  ticket_id int NULL,
  site_mark_state varchar(20) NULL,
  site_sewer_marked bit NULL,
  site_water_marked bit NULL,
  site_catv_marked bit NULL,
  site_gas_marked bit NULL,
  site_power_marked bit NULL,
  site_tel_marked bit NULL,
  site_other_marked varchar(20) NULL,
  site_pictures varchar(20) NULL,
  site_marks_measurement decimal(9, 3) NULL,
  site_buried_under varchar(20) NULL,
  site_clarity_of_marks varchar(20) NULL,
  site_hand_dig bit NULL,
  site_mechanized_equip bit NULL,
  site_paint_present bit NULL,
  site_flags_present bit NULL,
  site_exc_boring bit NULL,
  site_exc_grading bit NULL,
  site_exc_open_trench bit NULL,
  site_img_35mm bit NULL,
  site_img_digital bit NULL,
  site_img_video bit NULL,
  disc_repair_techs_were varchar(30) NULL,
  disc_repairs_were varchar(30) NULL,
  disc_repair_person varchar(30) NULL,
  disc_repair_contact varchar(30) NULL,
  disc_repair_comment text NULL,
  disc_exc_were varchar(30) NULL,
  disc_exc_person varchar(30) NULL,
  disc_exc_contact varchar(30) NULL,
  disc_exc_comment text NULL,
  disc_other1_person varchar(30) NULL,
  disc_other1_contact varchar(30) NULL,
  disc_other1_comment text NULL,
  disc_other2_person varchar(30) NULL,
  disc_other2_contact varchar(30) NULL,
  disc_other2_comment text NULL,
  exc_resp_code varchar(4) NULL,
  exc_resp_type varchar(4) NULL,
  exc_resp_other_desc varchar(100) NULL,
  exc_resp_details text NULL,
  exc_resp_response text NULL,
  uq_resp_code varchar(4) NULL,
  uq_resp_type varchar(4) NULL,
  uq_resp_other_desc varchar(100) NULL,
  uq_resp_details text NULL,
  spc_resp_code varchar(4) NULL,
  spc_resp_type varchar(4) NULL,
  spc_resp_other_desc varchar(100) NULL,
  spc_resp_details text NULL,
  modified_date datetime NOT NULL,
  active bit NOT NULL,
  profit_center varchar(15) NULL,
  due_date datetime NULL,
  intelex_num varchar(15) NULL,
  closed_date datetime NULL,
  modified_by int NULL,
  uq_resp_ess_step varchar(5) NULL,
  facility_type varchar(10) NULL,
  facility_size varchar(10) NULL,
  locator_experience varchar(10) NULL,
  locate_equipment varchar(10) NULL,
  was_project bit NOT NULL,
  claim_status varchar(10) NULL,
  claim_status_date datetime NULL,
  invoice_code varchar(10) NULL,
  facility_material varchar(10) NULL,
  added_by int NULL,
  accrual_date datetime NULL,
  invoice_code_initial_value varchar(10) NULL,
  uq_damage_id int NULL,
  estimate_locked bit NOT NULL,
  site_marked_in_white varchar(1) NULL,
  site_tracer_wire_intact varchar(1) NULL,
  site_pot_holed varchar(1) NULL,
  site_nearest_fac_measure decimal(9, 3) NULL,
  excavator_doing_repairs varchar(20) NULL,
  offset_visible varchar(1) NULL,
  offset_qty decimal(9, 3) NULL,
  claim_coordinator_id int NULL,
  locator_id int NULL,
  estimate_agreed bit NOT NULL,
  approved_by_id int NULL,
  approved_datetime datetime NULL,
  reason_changed int NULL,
  work_priority_id int NULL,
  marks_within_tolerance varchar(1) NULL
  )

  -- gather the damage data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @damages_to_send
  select
     damage.damage_id,
     damage.office_id,
     damage.damage_inv_num,
     damage.damage_type,
     damage.damage_date,
     damage.uq_notified_date,
     damage.notified_by_person,
     damage.notified_by_company,
     damage.notified_by_phone,
     damage.client_code,
     damage.client_id,
     damage.client_claim_id,
     damage.date_mailed,
     damage.date_faxed,
     damage.sent_to,
     damage.size_type,
     damage.location,
     damage.page,
     damage.grid,
     damage.city,
     damage.county,
     damage.state,
     damage.utility_co_damaged,
     damage.diagram_number,
     damage.remarks,
     damage.investigator_id,
     damage.investigator_arrival,
     damage.investigator_departure,
     damage.investigator_est_damage_time,
     damage.investigator_narrative,
     damage.excavator_company,
     damage.excavator_type,
     damage.excavation_type,
     damage.locate_marks_present,
     damage.locate_requested,
     damage.ticket_id,
     damage.site_mark_state,
     damage.site_sewer_marked,
     damage.site_water_marked,
     damage.site_catv_marked,
     damage.site_gas_marked,
     damage.site_power_marked,
     damage.site_tel_marked,
     damage.site_other_marked,
     damage.site_pictures,
     damage.site_marks_measurement,
     damage.site_buried_under,
     damage.site_clarity_of_marks,
     damage.site_hand_dig,
     damage.site_mechanized_equip,
     damage.site_paint_present,
     damage.site_flags_present,
     damage.site_exc_boring,
     damage.site_exc_grading,
     damage.site_exc_open_trench,
     damage.site_img_35mm,
     damage.site_img_digital,
     damage.site_img_video,
     damage.disc_repair_techs_were,
     damage.disc_repairs_were,
     damage.disc_repair_person,
     damage.disc_repair_contact,
     damage.disc_repair_comment,
     damage.disc_exc_were,
     damage.disc_exc_person,
     damage.disc_exc_contact,
     damage.disc_exc_comment,
     damage.disc_other1_person,
     damage.disc_other1_contact,
     damage.disc_other1_comment,
     damage.disc_other2_person,
     damage.disc_other2_contact,
     damage.disc_other2_comment,
     damage.exc_resp_code,
     damage.exc_resp_type,
     damage.exc_resp_other_desc,
     damage.exc_resp_details,
     damage.exc_resp_response,
     damage.uq_resp_code,
     damage.uq_resp_type,
     damage.uq_resp_other_desc,
     damage.uq_resp_details,
     damage.spc_resp_code,
     damage.spc_resp_type,
     damage.spc_resp_other_desc,
     damage.spc_resp_details,
     damage.modified_date,
     damage.active,
     damage.profit_center,
     damage.due_date,
     damage.intelex_num,
     damage.closed_date,
     damage.modified_by,
     damage.uq_resp_ess_step,
     damage.facility_type,
     damage.facility_size,
     damage.locator_experience,
     damage.locate_equipment,
     damage.was_project,
     damage.claim_status,
     damage.claim_status_date,
     damage.invoice_code,
     damage.facility_material,
     damage.added_by,
     damage.accrual_date,
     damage.invoice_code_initial_value,
     damage.uq_damage_id,
     damage.estimate_locked,
     damage.site_marked_in_white,
     damage.site_tracer_wire_intact,
     damage.site_pot_holed,
     damage.site_nearest_fac_measure,
     damage.excavator_doing_repairs,
     damage.offset_visible,
     damage.offset_qty,
     damage.claim_coordinator_id,
     damage.locator_id,
     damage.estimate_agreed,
     damage.approved_by_id,
     damage.approved_datetime,
     damage.reason_changed,
     damage.work_priority_id,
     damage.marks_within_tolerance
   from @mod_damages md
      inner join damage on md.damage_id = damage.damage_id

  -- Send the damage data
  select 'damage' as tname, * from @damages_to_send damage

-------------Send down Work Orders to client-------------

  --All Work Orders for this employee
  insert into @should_have_workorders
    select wo_id from work_order where assigned_to_id = @EmployeeID
      and active = 1
      and closed = 0
      and status <> '-P'
      and kind <> 'CANCEL'

  --Work Orders the client does have
  insert into @client_has_workorders
    select ID from IdListToTable(@ClientWorkOrderListS)

  -- they need to know about updates to Work Orders they are
  -- assigned now, and also Work Orders still in cache
  insert into @workorders (wo_id)
    select should_wo_id from @should_have_workorders
    union
    select client_wo_id from @client_has_workorders

  -- subtract what we have, from what we should have
  delete from @should_have_workorders where should_wo_id in
    (select client_wo_id from @client_has_workorders)

  -- Find those that have changed, somewhat slow
  insert into @mod_workorders (wo_id)
  select wo.wo_id
   from @workorders wo
    inner join work_order on work_order.wo_id = wo.wo_id
    and work_order.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @workorders wo2
    inner join notes on notes.foreign_id = wo2.wo_id
   where notes.foreign_type = 7
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @workorders wo3
    inner join attachment on attachment.foreign_id = wo3.wo_id
   where attachment.foreign_type = 7
    and attachment.modified_date > @UpdatesSince
  union
  select work_order_ticket.wo_id
    from @workorders wo4
    inner join work_order_ticket on work_order_ticket.wo_id = wo4.wo_id
    where work_order_ticket.modified_date > @UpdatesSince
  union
  select work_order_inspection.wo_id
    from @workorders wo5
    inner join work_order_inspection on work_order_inspection.wo_id = wo5.wo_id
    where work_order_inspection.modified_date > @UpdatesSince
  union
  select work_order_remedy.wo_id
    from @workorders wo6
    inner join work_order_remedy on work_order_remedy.wo_id = wo6.wo_id
    where work_order_remedy.modified_date > @UpdatesSince
  union
  select work_order_OHM_details.wo_id --QMANTWO-391
    from @workorders wo7
    inner join work_order_OHM_details on work_order_OHM_details.wo_id = wo7.wo_id
    where work_order_OHM_details.modified_date > @UpdatesSince
  union
  select should_wo_id from @should_have_workorders

  -- now get the whole work order for just the ones that have changed
  declare @workorders_to_send table (
    wo_id integer not null,
    wo_number varchar(20) not null,
    assigned_to_id integer null,
    client_id integer null,
    parsed_ok bit null,
    wo_source varchar(20) not null,
    kind varchar(40) not null,
    status varchar(5) not null,
    closed bit not null,
    map_page varchar(20) null,
    map_ref varchar(60) null,
    transmit_date datetime not null,
    due_date datetime not null,
    closed_date datetime null,
    status_date datetime null,
    cancelled_date datetime null,
    work_type varchar(90) null,
    work_description varchar(3500) null,
    work_address_number varchar(10) null,
    work_address_number_2 varchar(10) null,
    work_address_street varchar(60) null,
    work_cross varchar(100) null,
    work_county varchar(40) null,
    work_city varchar(40) null,
    work_state varchar(2) null,
    work_zip varchar(10) null,
    work_lat decimal(9,6) null,
    work_long decimal(9,6) null,
    caller_name varchar(50) null,
    caller_contact varchar(50) null,
    caller_phone varchar(40) null,
    caller_cellular varchar(40) null,
    caller_fax varchar(40) null,
    caller_altcontact varchar(40) null,
    caller_altphone varchar(40) null,
    caller_email varchar(40) null,
    client_wo_number varchar(40) not null,
    image text null,
    parse_errors text null,
    update_of_wo_id integer null,
    modified_date datetime not null,
    active bit not null,
    job_number varchar(20) null,
    client_order_num varchar(20) null,
    client_master_order_num varchar(20) null,
    wire_center varchar(20) null,
    work_center varchar(20) null,
    central_office varchar(20) null,
    serving_terminal varchar(20) null,
    circuit_number varchar(20) null,
    f2_cable varchar(20) null,
    terminal_port varchar(20) null,
    f2_pair varchar(20) null,
    state_hwy_row varchar(10) null,
    road_bore_count integer null,
    driveway_bore_count integer null,
    call_date datetime null,
    source_sent_attachment varchar(1) null
  )

  -- gather the Work Order data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @workorders_to_send
  select
     work_order.wo_id
    ,work_order.wo_number
    ,work_order.assigned_to_id
    ,work_order.client_id
    ,work_order.parsed_ok
    ,work_order.wo_source
    ,work_order.kind
    ,work_order.status
    ,work_order.closed
    ,work_order.map_page
    ,work_order.map_ref
    ,work_order.transmit_date
    ,work_order.due_date
    ,work_order.closed_date
    ,work_order.status_date
    ,work_order.cancelled_date
    ,work_order.work_type
    ,work_order.work_description
    ,work_order.work_address_number
    ,work_order.work_address_number_2
    ,work_order.work_address_street
    ,work_order.work_cross
    ,work_order.work_county
    ,work_order.work_city
    ,work_order.work_state
    ,work_order.work_zip
    ,work_order.work_lat
    ,work_order.work_long
    ,work_order.caller_name
    ,work_order.caller_contact
    ,work_order.caller_phone
    ,work_order.caller_cellular
    ,work_order.caller_fax
    ,work_order.caller_altcontact
    ,work_order.caller_altphone
    ,work_order.caller_email
    ,work_order.client_wo_number
    ,work_order.image
    ,work_order.parse_errors
    ,work_order.update_of_wo_id
    ,work_order.modified_date
    ,work_order.active
    ,work_order.job_number
    ,work_order.client_order_num
    ,work_order.client_master_order_num
    ,work_order.wire_center
    ,work_order.work_center
    ,work_order.central_office
    ,work_order.serving_terminal
    ,work_order.circuit_number
    ,work_order.f2_cable
    ,work_order.terminal_port
    ,work_order.f2_pair
    ,work_order.state_hwy_row
    ,work_order.road_bore_count
    ,work_order.driveway_bore_count
    ,work_order.call_date
    ,work_order.source_sent_attachment
  from @mod_workorders mwo
      inner join work_order on mwo.wo_id = work_order.wo_id

  -- Send the Work Order data
  select 'work_order' as tname, * from @workorders_to_send work_order
  
  -- Send the work_order_version data
  select 'work_order_version' as tname, work_order_version.*
    from @mod_workorders mw
      inner join work_order_version on mw.wo_id = work_order_version.wo_id

  -- Send the Work Order <-> Ticket association data
  select 'work_order_ticket' as tname, work_order_ticket.* 
  from work_order_ticket 
  inner join @workorders_to_send wo on work_order_ticket.wo_id = wo.wo_id
  where work_order_ticket.modified_date > @UpdatesSince

  -- Send work order assignment data
  select 'wo_assignment' as tname, * from wo_assignment
  where assigned_to_id = @EmployeeID
    and modified_date > @UpdatesSince

  -- Get ticket_id's for related tickets that need to be sent
  insert into @related_tickets (ticket_id)
    select ticket_id
    from work_order_ticket wt
      -- Only include tickets related to work orders the client has or should have
      inner join @workorders w on w.wo_id = wt.wo_id
    where
      -- Exclude tickets we already sent
      wt.ticket_id not in (select ticket_id from @tickets_to_send) and
      -- Exclude tickets the client already has                
      wt.ticket_id not in (
        select c.client_ticket_id
        from @client_has_tix c
          inner join ticket t on t.ticket_id = c.client_ticket_id
        where t.modified_date <= @UpdatesSince)

  -- Send the related ticket data
  select 'ticket' as tname,
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.parsed_ok,
    ticket.ticket_format,
    ticket.kind,
    ticket.status,
    ticket.map_page,
    ticket.revision,
    ticket.transmit_date,
    ticket.due_date,
    ticket.work_description,
    ticket.work_state,
    ticket.work_county,
    ticket.work_city,
    ticket.work_address_number,
    ticket.work_address_number_2,
    ticket.work_address_street,
    ticket.work_cross,
    ticket.work_subdivision,
    ticket.work_type,
    ticket.work_date,
    ticket.work_notc,
    ticket.work_remarks,
    ticket.priority,
    ticket.legal_date,
    ticket.legal_good_thru,
    ticket.legal_restake,
    ticket.respond_date,
    ticket.duration,
    ticket.company,
    ticket.con_type,
    ticket.con_name,
    ticket.con_address,
    ticket.con_city,
    ticket.con_state,
    ticket.con_zip,
    ticket.call_date,
    ticket.caller,
    ticket.caller_contact,
    ticket.caller_phone,
    ticket.caller_cellular,
    ticket.caller_fax,
    ticket.caller_altcontact,
    ticket.caller_altphone,
    ticket.caller_email,
    ticket.operator,
    ticket.channel,
    ticket.work_lat,
    ticket.work_long,
    ticket.geocode_precision,
    ticket.image,
    ticket.parse_errors,
    ticket.modified_date,
    ticket.active,
    ticket.ticket_type,
    ticket.legal_due_date,
    ticket.parent_ticket_id,
    ticket.do_not_mark_before,
    ticket.route_area_id,
    ticket.watch_and_protect,
    ticket.service_area_code,
    ticket.explosives,
    ticket.serial_number,
    ticket.map_ref,
    ticket.followup_type_id,
    ticket.do_not_respond_before,
    ticket.recv_manager_id,
    ticket.ward,
    coalesce(area.area_name, '') as area_name,
    ticket.alert,
    -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null:
    null, --     start_date,
    null, --     end_date,
    null, --     points 
    ticket.work_priority_id,
	ticket.wo_number,    -- QMANTWO-253
    ticket.route_order  --QMANTWO-775	
  from @related_tickets r
    inner join ticket on ticket.ticket_id = r.ticket_id
    left join area on area.area_id = ticket.route_area_id

  -- Send the Work Order Inspection data
  select 'work_order_inspection' as tname, work_order_inspection.* 
  from @workorders_to_send wo
  inner join work_order_inspection on work_order_inspection.wo_id = wo.wo_id

  -- Send the Work Order Remedy data
  select 'work_order_remedy' as tname, work_order_remedy.* 
  from @workorders_to_send wo
  inner join work_order_remedy on work_order_remedy.wo_id = wo.wo_id

  --Send the Work Order OHM data QMANTWO-391
  select 'work_order_OHM_details' as tname, work_order_OHM_details.*
  from @Workorders_to_send wo
  inner join work_order_OHM_details on work_order_OHM_details.wo_id = wo.wo_id

------------End send down Work Orders to client -----------------

  -- Retrieve all notes for any ticket,damage, or work order that has been modified in any way, even
  -- if the note has not been modified. This is necessary because the ticket, damage, or work order
  -- may be have been reassigned to a different locator.
  select 'notes' as tname, notes.*
   from @modtix mt
   inner join notes on notes.foreign_type = 1 -- 1 = ticket notes
    and mt.ticket_id = notes.foreign_id
  union
  select 'notes' as tname, notes.*
   from @modtix mt
     inner join locate on locate.ticket_id = mt.ticket_id
     inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes
  union
  select 'notes' as tname, notes.*
     from @mod_damages md
     inner join notes on notes.foreign_type = 2 --damage notes
      and md.damage_id = notes.foreign_id
  union
  select 'notes' as tname, notes.*
     from @mod_workorders mwo
     inner join notes on notes.foreign_type = 7 --work order notes
      and mwo.wo_id = notes.foreign_id

  -- All attachments for modified tickets, damages, and work orders:
  select 'attachment' as tname, attachment.*
   from @modtix mt
   inner join attachment on mt.ticket_id = attachment.foreign_id
    and attachment.foreign_type = 1 --ticket attachments
  union
  select 'attachment' as tname, attachment.*
   from @mod_damages md
   inner join attachment on md.damage_id = attachment.foreign_id
    and attachment.foreign_type = 2 -- damage attachments
  union
  select 'attachment' as tname, attachment.*
   from @mod_workorders mwo
   inner join attachment on mwo.wo_id = attachment.foreign_id
    and attachment.foreign_type = 7 -- work order attachments

  --All documents for modified damages
  select 'document' as tname, document.*
    from @mod_damages md
    inner join document on document.foreign_type = 2
     and md.damage_id = document.foreign_id

  --All estimates for modified damages
  select 'damage_estimate' as tname, damage_estimate.*
  from @mod_damages md
  inner join damage_estimate on damage_estimate.active = 1
    and md.damage_id = damage_estimate.damage_id

  --All invoices for modified damages
  select 'damage_invoice' as tname, damage_invoice.*
    from @mod_damages md
    inner join damage_invoice on md.damage_id = damage_invoice.damage_id

  -- All Plats for modified tickets:
  select 'locate_plat' as tname, locate_plat.*
   from @locates_to_send ls
   inner join locate_plat on ls.locate_id = locate_plat.locate_id

  -- All unacked, unexpired messages for employee:
  select 'message_dest' as tname, message.message_id, message.message_number,
   message.from_emp_id, message.destination, message.subject, message.body,
   message.sent_date, message.show_date, message.expiration_date, message.active,
   md.message_dest_id, md.emp_id, md.ack_date, md.read_date, md.rule_id
  from message inner join message_dest md on message.message_id=md.message_id
  where md.ack_date is null
    and md.emp_id = @EmployeeID
    and message.expiration_date > getdate()
    and message.modified_date > @UpdatesSince

  -- Send the ticket_version data
  select 'ticket_version' as tname,
     tv.ticket_version_id, tv.ticket_id, tv.ticket_revision, tv.ticket_number, tv.ticket_type, tv.transmit_date,
     tv.processed_date, tv.serial_number, tv.arrival_date, tv.filename, tv.ticket_format, tv.source 
    from @modtix mt
      inner join ticket_version tv on mt.ticket_id = tv.ticket_id

  -- Send the break_rules data
  select 'break_rules' as tname, break_rules.*
   from break_rules
   where modified_date > @UpdatesSince

  -- Send the right definition data
  select 'right_definition' as tname, * from right_definition where modified_date > @UpdatesSince

  -- Send the right restriction data
  select 'right_restriction' as tname, *
   from right_restriction
   where modified_date > @UpdatesSince

  -- Send the usage restriciton message data
  select 'restricted_use_message' as tname, *
   from restricted_use_message
   where modified_date > @UpdatesSince

  -- Send the usage restriciton exemption data
  select 'restricted_use_exemption' as tname, *
   from restricted_use_exemption
   where emp_id = @EmployeeID
     and modified_date > @UpdatesSince

  -- Send the jobsite_arrival data
  select 'jobsite_arrival' as tname, arrival.*
    from @modtix mt
      inner join jobsite_arrival arrival on mt.ticket_id = arrival.ticket_id

  -- All locate_facility for locates on modified tickets:
  select 'locate_facility' as tname, lf.*
    from @locates_to_send ls
      inner join locate_facility lf on ls.locate_id = lf.locate_id

  -- save some key dates
  declare @Today datetime
  declare @Tomorrow datetime
  declare @DayAfterTomorrow datetime
  set @Today = convert(datetime, convert(varchar, GetDate(), 101))
  set @Tomorrow = DateAdd(Day, 1, @Today)
  set @DayAfterTomorrow = DateAdd(Day, 2, @Today)
    
  declare @PC varchar(15)
  select @PC = Coalesce(dbo.get_employee_pc(@EmployeeID, 1), '?')
  select 'center_ticket_summary' as tname, 
    @PC pc_code, 
    0 as tickets_open,
    0 as tickets_due_today,
    0 as tickets_due_tomorrow

  /* TODO: The real center_ticket_summary counts are temporarily disabled for performance reasons...

  -- Get ticket summary counts for emp's profit center:
  -- List of emps in the same profit center as @EmployeeID
  declare @emps_in_center table (
    emp_id integer not null primary key
  )
  insert into @emps_in_center select emp_id 
    from dbo.employee_pc_data(@pc) where @pc is not null

  -- Get open tickets for the center
  declare @open_tickets table (
    ticket_id integer not null primary key,
    due_date datetime
  )
  insert into @open_tickets
    select distinct ticket.ticket_id, ticket.due_date 
    from @emps_in_center emp
      inner join locate on emp.emp_id = locate.assigned_to 
      inner join ticket on locate.ticket_id = ticket.ticket_id 

  select 'center_ticket_summary' as tname, 
    @PC pc_code, 
    count(*) as tickets_open,
    coalesce(sum(
      case when due_date >= @Today and due_date < @Tomorrow then 1 else 0 
      end), 0) as tickets_due_today,
    coalesce(sum(
      case when due_date >= @Tomorrow and due_date < @DayAfterTomorrow then 1 else 0 
      end), 0) as tickets_due_tomorrow
  from @open_tickets
*/

  -- Send the task_schedule data for emp's tickets
  select 'task_schedule' as tname, ts.*
    from @modtix mt
    inner join task_schedule ts on mt.ticket_id = ts.ticket_id
  -- and send any modified task_schedule data no longer for emp's tickets
  union select 'task_schedule' as tname, ts1.*
    from task_schedule ts1
    left join @modtix mt1 on mt1.ticket_id = ts1.ticket_id
    where (ts1.emp_id = @EmployeeID) and (ts1.est_start_date >= @Today)
      and (ts1.modified_date > @UpdatesSince)
      and (mt1.ticket_id is null)

  select 'damage_profit_center_rule' as tname, * from damage_profit_center_rule
  where modified_date > @UpdatesSince

  select 'work_order_work_type' as tname, * from work_order_work_type where modified_date > @UpdatesSince

/* Added for the questions to ask on Status changes (CO) */
  select 'question_info' as tname, * from question_info where modified_date > @UpdatesSince

  select 'question_group_detail' as tname, * from question_group_detail where modified_date > @UpdatesSince

  select 'question_header' as tname, * from question_header where modified_date > @UpdatesSince

  select 'holiday' as tname, * from holiday where (modified_date > @UpdatesSince) and (holiday_date >= (@Today - 3))

  select 'employee_activity' as tname, * from employee_activity where (activity_date >= @Today) and activity_type = 'UTILISAFE'

GO


