USE [QM]
GO

/****** Object:  Trigger [iu_EmpBalance]    Script Date: 5/24/2022 12:47:17 PM ******/
DROP TRIGGER [dbo].[iu_EmpBalance]
GO

/****** Object:  Trigger [dbo].[iu_EmpBalance]    Script Date: 5/24/2022 12:47:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Larry Killen
-- Create date: 5/24/22
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[iu_EmpBalance] 
   ON  [dbo].[employee_balance] 
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update employee_balance set modified_date = getdate() where emp_id in (select emp_id from inserted)  

END
GO

ALTER TABLE [dbo].[employee_balance] ENABLE TRIGGER [iu_EmpBalance]
GO

