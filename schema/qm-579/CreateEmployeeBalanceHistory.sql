USE [QM]
GO

/****** Object:  Table [dbo].[employee_balance_history]    Script Date: 5/19/2022 3:51:21 PM ******/

if object_id('dbo.employee_balance_history') is not null
  drop Table employee_balance_history
go	
/****** Object:  Table [dbo].[employee_balance_history]    Script Date: 5/19/2022 3:51:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[employee_balance_history](
	[Employee_Balance_History] [int] IDENTITY(1,1) NOT NULL,
	[Company_Code] [varchar](50) NULL,
	[Employee_Number] [varchar](50) NOT NULL,
	[emp_id] [int] NOT NULL,
	[First_Name] [varchar](20) NULL,
	[Last_Name] [varchar](30) NULL,
	[Employment_Status] [varchar](50) NULL,
	[SICK_Option] [varchar](50) NULL,
	[SICK_Balance] [decimal](12, 6) NULL,
	[PTO_Option] [varchar](50) NULL,
	[PTO_Balance] [decimal](12, 6) NULL,
	[PTO_Accrual] [decimal](12, 6) NULL,
	[WeeksRemaining] [decimal](12, 8) NULL,
	[Accrual_to_EOY] [decimal](12, 6) NULL,
	[Est_Bal_at_EOY] [decimal](12, 8) NULL,
	[Supervisor_Name] [varchar](50) NULL,
	[EMail_Sup] [varchar](50) NULL,
	[modified_date] [datetime] NULL,
	[insert_date] [datetime] NULL,
	[Date_Copied] [datetime] NULL,
 CONSTRAINT [PK_employee_balance_history] PRIMARY KEY CLUSTERED 
(
	[Employee_Balance_History] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


