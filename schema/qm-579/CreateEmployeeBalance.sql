USE [QM]
GO

/****** Object:  Table [dbo].[employee_balance]    Script Date: 5/11/2022 11:57:46 AM ******/
if object_id('dbo.[employee_balance]') is not null
  DROP TABLE [dbo].[employee_balance]
go	

/****** Object:  Table [dbo].[employee_balance]    Script Date: 5/11/2022 11:57:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[employee_balance](
	[Company_Code] [varchar](50) NULL,
	[Employee_Number] [varchar](50) NOT NULL,
	[emp_id] [int] NOT NULL,
	[First_Name] [varchar](20) NULL,
	[Last_Name] [varchar](30) NULL,
	[Employment_Status] [varchar](50) NULL,
	[SICK_Option] [varchar](50) NULL,
	[SICK_Balance] [decimal](12, 6) NULL,
	[PTO_Option] [varchar](50) NULL,
	[PTO_Balance] [decimal](12, 6) NULL,
	[PTO_Accrual] [decimal](12, 6) NULL,
	[WeeksRemaining] [decimal](12, 8) NULL,
	[Accrual_to_EOY] [decimal](12, 6) NULL,
	[Est_Bal_at_EOY] [decimal](12, 8) NULL,
	[Supervisor_Name] [varchar](50) NULL,
	[EMail_Sup] [varchar](50) NULL,
	[modified_date] [datetime] NULL,
	[insert_date] [datetime] NULL,
 CONSTRAINT [PK_employee_balance] PRIMARY KEY CLUSTERED 
(
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

