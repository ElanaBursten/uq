USE [QM]
GO   
/****** Object:  Trigger [dbo].[employee_plus_uid]    Script Date: 7/27/2023 2:42:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Trigger [dbo].[employee_plus_u]    Script Date: 6/30/23 ******/
ALTER trigger [dbo].[employee_plus_uid] on [dbo].[employee_plus]
AFTER INSERT, UPDATE, DELETE
as
IF EXISTS ( SELECT 0 FROM Deleted )
BEGIN
   IF EXISTS ( SELECT 0 FROM Inserted )
   BEGIN	   
      INSERT INTO [dbo].[employee_plus_history]
      (emp_id,
       term_group_id, 
	   active,
	   modified_date,
	   modified_by_id,
       util,
	   operation
      )
      (SELECT 
	   u.emp_id,
       u.term_group_id, 
	   u.active,
	   getdate(),
	   u.modified_by_id,
       u.util,
	   'UPDATED'
      FROM deleted as u)
   END
   ELSE
   BEGIN
     INSERT INTO dbo.employee_plus_history
      (emp_id,
       term_group_id, 
	   active,
	   modified_date,
	   modified_by_id,
       util,
	   operation
      )
     (SELECT 
	   d.emp_id,
       d.term_group_id, 
	   d.active,
	   getdate(),
	   d.modified_by_id,
       d.util,
	   'DELETED'
      FROM deleted as d
	  )
    END 
	--UPDATE employee_plus SET modified_date = getdate() FROM   --qm-789
 --      employee_plus ep join deleted del ON ep.emp_id = del.emp_id
END
ELSE
BEGIN
  INSERT INTO dbo.employee_plus_history
   (emp_id,
    term_group_id, 
	active,
	modified_date,
	modified_by_id,
    util,
	operation
    )
   (SELECT 
	i.emp_id,
    i.term_group_id, 
	i.active,
	getdate(),
	i.modified_by_id,
    i.util,
	'INSERTED'
    FROM inserted as i )
	  --UPDATE employee_plus SET modified_date = getdate() FROM  --qm-789
   --    employee_plus ep join inserted ins ON ep.emp_id = ins.emp_id
END



