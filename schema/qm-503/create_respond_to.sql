USE [QM]
GO

/****** Object:  Table [dbo].[respond_to]    Script Date: 12/29/2021 12:08:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[respond_to](
	[respond_to_id] [int] IDENTITY(1,1) NOT NULL,
	[respond_to] [varchar](20) NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[include_notes] [bit] NULL,
	[include_epr_link] [bit] NULL,
	[url] [varchar](max) NULL,
	[user_name] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[all_versions] [bit] NULL,
	[real_locator_name] [bit] NULL,
	[api_key] [varchar](max) NULL,
	[one_call_center] [varchar](10) NULL,
	[respond_to_note] [nvarchar](max) NULL,
 CONSTRAINT [uq_respond_to] UNIQUE NONCLUSTERED 
(
	[respond_to] ASC,
	[active] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

