USE [QM]
GO

/****** Object:  Trigger [dbo].[IU_AddToMultiQ]    Script Date: 12/29/2021 12:07:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Larry Killen
-- Jira:        QM-503
-- Create date: 12/8/2021
-- Description:	moves data into multi_queue based on respond_to field
-- =============================================
CREATE TRIGGER [dbo].[IU_AddToMultiQ] 
   ON  [dbo].[responder_queue] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @ClientID integer, @LocateID integer, @TicketFormat varchar(20), @ClientCode varchar(20), @ClientRespondTo varChar(20);
Select @LocateID=locate_id, @TicketFormat=ticket_format,@ClientCode=client_code  from INSERTED;

set @ClientID = (
  select client_id --client_id 
  from client
  where call_center = @TicketFormat
  and oc_code =@ClientCode) 

--Set  @ClientRespondTo = 
	Declare db_cursor CURSOR for
	SELECT CLIENT_RESPOND_TO
	FROM [DBO].[CLIENT_RESPOND_TO]
	WHERE CLIENT_ID =@CLIENTID

	Open db_cursor
	FETCH NEXT FROM db_cursor into @ClientRespondTo						
	WHILE @@FETCH_STATUS = 0
	Begin					 
	  insert into [QM].[dbo].[responder_multi_queue] ([respond_to], [locate_id],  [ticket_format], [client_code] )
	  values 
	  (@ClientRespondTo, @LocateID,@TicketFormat, @ClientCode)
	  FETCH NEXT FROM db_cursor into @ClientRespondTo
	  delete from [dbo].[responder_queue] where [locate_id]=@LocateID		
	END
	CLOSE db_cursor
	DEALLOCATE db_cursor
END
GO

ALTER TABLE [dbo].[responder_queue] ENABLE TRIGGER [IU_AddToMultiQ]
GO

