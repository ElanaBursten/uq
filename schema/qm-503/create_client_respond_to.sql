USE [QM]
GO

/****** Object:  Table [dbo].[client_respond_to]    Script Date: 12/29/2021 12:06:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[client_respond_to](
	[crt_id] [int] IDENTITY(1,1) NOT NULL,
	[client_id] [int] NOT NULL,
	[client_code] [varchar](10) NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[client_respond_to] [varchar](20) NOT NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_client_respond_to] PRIMARY KEY CLUSTERED 
(
	[crt_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uq_client_respond_to] UNIQUE NONCLUSTERED 
(
	[client_id] ASC,
	[client_respond_to] ASC,
	[active] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[client_respond_to] ADD  CONSTRAINT [DF_ClientRespond_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO

ALTER TABLE [dbo].[client_respond_to] ADD  CONSTRAINT [DF_ClientRespond_active]  DEFAULT ((1)) FOR [active]
GO

