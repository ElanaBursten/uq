--QM-387 Risk Score
--QM-368 Replaced the STUFF command with STRING_AGG and with temporary table
--This version ONLY gathers the utils llst for the incoming locator
--All other utils are missing from the Work Management - Ticket grid

if object_id('dbo.get_open_tickets_for_locator2') is not null
  drop procedure dbo.get_open_tickets_for_locator2
GO

CREATE procedure [dbo].[get_open_tickets_for_locator2] (@locator_id int)
as   --qm-368 added WITH RECOMPILE
  --declare @locator_id int;  test 
  --set @locator_id = 27617   test 

  declare @results table ( 
    ticket_id integer not null primary key, 
    ticket_number varchar(20) not null,
    kind varchar(20) not null,
    due_date datetime null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null,
    work_type varchar(90) null,
    legal_good_thru datetime null, 
    map_page varchar(20) null, 
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null, 
    work_address_number varchar(10) null, 
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    points decimal(6,2) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null,
    est_start_time datetime null,
    area_name varchar(40),
    route_order decimal(9,5),  --QMANTWO-775 QM-10
    lat decimal(9,5), --QM-10
    lon decimal(9,5), --QM-10
    work_state varchar(2),  --QM-10
	company varchar(80),   --QM-325
	con_name varchar(50),  --QM-325
	util varchar(200),  --QM-306
	risk varchar(100)  --QM-386  (source + type)
	);

  declare @limit integer
  select @limit = coalesce(ticket_view_limit, 0) from employee where emp_id = @locator_id

  drop table if exists #Ticket_Utils

  SELECT L.TICKET_ID, STRING_AGG(C.UTILITY_TYPE, ',' ) AS UTILS 
  INTO #TICKET_UTILS
  FROM CLIENT C 
  JOIN LOCATE L   ON (C.CLIENT_ID = L.CLIENT_ID)
  WHERE L.ASSIGNED_TO  = @LOCATOR_ID
  AND (L.CLIENT_ID > 1)
  GROUP BY L.TICKET_ID
  order by L.TICKET_ID 

  insert into @results 
  select ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(substring(r.description,0,20), 'Normal') as work_priority,   --QMANTWO-565
    case when
      @limit < 1 or ticket.kind = 'EMERGENCY' or ticket.kind = 'ONGOING' or r.code='Release' then 'Y'
      else 'N'
    end as ticket_is_visible,
    (select min(task.est_start_date) from task_schedule task 
     where task.ticket_id = ticket.ticket_id 
       and task.est_start_date >= GetDate() and task.active=1 
       and task.emp_id = @locator_id) as est_start_time 
	,a.area_name, 
	ticket.route_order, ticket.work_lat, ticket.work_long, ticket.work_state,
	ticket.company, ticket.con_name, tu.Utils,

	 (select dbo.get_latest_risk_score(ticket.ticket_id))

  From Ticket 
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  left outer join #Ticket_Utils tu on (tu.ticket_id = ticket.ticket_id)	
  where ticket.ticket_id in (
    select ticket_id from locate
      where assigned_to = @locator_id
    )


  update @results set ticket_is_visible = 'Y' where ticket_id in
    (select top(@limit) ticket_id from @results where ticket_is_visible <> 'Y'
       order by work_priority_sort desc, due_date, ticket_number)


  select 'ticket' as tname, * from @results ticket order by work_priority_sort desc, due_date, ticket_number

--GO
--grant execute on get_open_tickets_for_locator2 to uqweb, QManagerRole
GO
