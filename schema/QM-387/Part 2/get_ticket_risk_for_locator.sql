 if object_id('dbo.get_ticket_risk_for_locator') is not null
  drop function dbo.get_ticket_risk_for_locator
go

create function get_ticket_risk_for_locator (@emp_id int, @SinceUpdate datetime)
returns
  @results TABLE (
   ticket_risk_id integer NOT NULL PRIMARY KEY,
   ticket_id integer NOT NULL,
   ticket_version_id integer NULL,
   locate_id integer NOT NULL,
   ticket_revision varchar(20) NULL,
   risk_source varchar(20) NULL,
   risk_score varchar(20) NULL,
   transmit_date datetime  NULL,
   insert_date datetime  NOT NULL,
   risk_score_type varchar(20) NULL
  )
as
begin
  insert into @results
  select tr.* from ticket_risk tr, ticket t, locate l 
  where 
    (tr.ticket_id = t.ticket_id) and
    (t.ticket_id = l.ticket_id) and
    (tr.locate_id = l.locate_id) and
    (l.assigned_to = @emp_id) and
    (tr.insert_date > @SinceUpdate)

	Return
end

GO


--select tr.* from ticket_risk tr, ticket t, locate l 
--where (tr.ticket_id = t.ticket_id) and
--  (t.ticket_id = l.ticket_id) and
--  (tr.locate_id = l.locate_id) and
--  (l.assigned_to = 1191) 

--  select * from employee where last_name = 'Walters'

--  select * from ticket_risk order by ticket_id