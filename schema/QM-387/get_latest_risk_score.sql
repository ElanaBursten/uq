--QM-387 Risk Score
--get_latest_risk_score - returns a single concat value which will go in the WM - Ticket List
--it pulls the latest risk score for the ticket
if object_id('dbo.get_latest_risk_score') is not null
  drop function dbo.get_latest_risk_score
go

create function dbo.get_latest_risk_score(@ticket_id int)
returns varchar(100)
as
begin
  return coalesce((select top 1 concat( 
       tr.risk_source, '-', tr.risk_score_type, ' : ', tr.risk_score) 
  from ticket_risk tr 
  where (tr.ticket_id = @ticket_id)
  order by tr.ticket_version_id desc), '--')

end	
go


