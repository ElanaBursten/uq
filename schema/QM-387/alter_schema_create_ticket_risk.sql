--QM-387 Risk Score Schema (for QM-386)

CREATE TABLE ticket_risk(
	[ticket_risk_id] [int] IDENTITY(1,1) NOT NULL,
	[ticket_id] [int] NOT NULL,
	[ticket_version_id] [int] NULL,
	[locate_id] [int] NOT NULL,
	[ticket_revision] [varchar](20) NULL,
	[risk_source] [varchar](20) NULL,
	[risk_score] [varchar](20) NULL,
	[transmit_date] [datetime] NULL,
	[insert_date] [datetime] NOT NULL,
	[risk_score_type] [varchar](20) NULL
)
GO

ALTER TABLE ticket_risk ADD  CONSTRAINT DF_ticket_risk_Insert_Date  DEFAULT (getdate()) FOR insert_date
GO


--select * from ticket_risk
