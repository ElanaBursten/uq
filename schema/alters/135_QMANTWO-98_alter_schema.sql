--Added as part of QMANTWO-98 Change QM.damages.utilistar_id to QM.damages.intelex_id
--Comment or Uncomment Database and Rollback information
--You will receive a Caution message: "Caution: Changing any part of an object name could break scripts and stored procedures."


IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'damage' and COLUMN_NAME='intelex_num')
  Alter table damage
    Add intelex_num varchar(15)
GO

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'damage_estimate' and COLUMN_NAME='intelex_num')
  Alter table damage_estimate
    Add intelex_num varchar(15)
GO


--ROLLBACK QMANTWO-98 CHANGES

--IF EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
--              TABLE_NAME = 'damage' and COLUMN_NAME='intelex_num')
--  Alter table damage
--    DROP COLUMN intelex_num
--GO

--IF EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
--              TABLE_NAME = 'damage_estimate' and COLUMN_NAME='intelex_num')
--    Alter table damage_estimate
--    DROP COLUMN intelex_num
--GO
