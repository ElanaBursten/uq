
ALTER TABLE billing_output_config 
  ADD CONSTRAINT uk_boc_customer UNIQUE NONCLUSTERED (
    customer_id, center_group_id, billing_period, which_invoice
  )
go

