IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where
TABLE_NAME = 'status_translation' and COLUMN_NAME='explanation_code')
  ALTER TABLE status_translation 
    ADD explanation_code varchar(20) null;
go 
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where
TABLE_NAME = 'status_translation' and COLUMN_NAME='responder_type')
  ALTER TABLE status_translation
    ADD responder_type varchar(20) null;
go 

-- any existing rows are only for work orders, so set responder_type = 'work_order'
IF EXISTS (SELECT * from INFORMATION_SCHEMA.COLUMNS
  where TABLE_NAME = 'status_translation'
  and COLUMN_NAME = 'responder_type' 
  and IS_NULLABLE = 'YES')
    update status_translation 
    set responder_type = 'work_order' 
    where responder_type is null;
go

-- now disallow nulls for the responder_type column
IF EXISTS (SELECT * from INFORMATION_SCHEMA.COLUMNS
  where TABLE_NAME = 'status_translation'
  and COLUMN_NAME = 'responder_type' 
  and IS_NULLABLE = 'YES')
    ALTER TABLE status_translation
      ALTER COLUMN responder_type varchar(20) not null;
go

IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where
TABLE_NAME = 'status_translation' and COLUMN_NAME='responder_kind')
  ALTER TABLE status_translation 
    ADD responder_kind varchar(20) null;
go 

/* outgoing status field needs to be longer than 20 chars */
ALTER TABLE status_translation
  ALTER COLUMN outgoing_status varchar(30) not null;
go

IF EXISTS (SELECT * FROM SYS.INDEXES WHERE NAME = 'status_translation_cc_code_status') 
  drop index status_translation_cc_code_status on status_translation;
/* index needs to include more fields */
create unique nonclustered index status_translation_cc_code_status on
status_translation (cc_code, responder_type, responder_kind, status);
go
