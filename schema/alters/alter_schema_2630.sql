-- Schema changes needed to add Damages to the Ticket Management screen.
-- Also deploy the new multi_open_totals3.sql, get_damage_list.sql, get_open_damages_for_locator.sql, & get_old_damages_for_locator.sql stored procedures

--Add new right permitting a user to see & reassign damages on Ticket Management screen
if not exists (select * from right_definition where entity_data = 'DamagesManagement')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesManagement', 'Damages - Management', 'General', 
    'Limitation does not apply to this right.')
go

--Add new reference table row for the Damage Move employee activity
if not exists (select * from reference where type = 'empact' and code = 'DMGMOV')
  insert into reference (type, code, description)
    values ('empact', 'DMGMOV', 'Move Damages')
go

