
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='billing_transmission_detail')
  create table billing_transmission_detail (
   bill_trans_detail_id int identity(65001,1) not null primary key,
   bill_id integer not null, /* foreign key link to billing_header */
   customer_id int not null,
   which_invoice varchar(20) null,
   billing_cc varchar(10) not null, /* term code */
   client_id integer not null,
   ticket_format varchar(20) NULL,
   ticket_version_id integer not null,
   ticket_id integer not null,
   transmit_date datetime not null,
   ticket_image_fragment varchar(250),
   ticket_source varchar(20) not null,
   locator_id integer null,
   locate_closed bit null,
   plat varchar(20) null,
   plat_group varchar(30) null,
   locate_id int null,
   rate money null,
   line_item_text varchar(60) null,
   bill_city varchar(40) null,
   ticket_number varchar(20) not null,
   work_city varchar(40),
   revision varchar(20) null
  )
GO

IF NOT EXISTS (select * from sys.indexes where object_id = object_id('billing_transmission_detail') and name = 'billing_transmission_detail_bill_id_cust_id')
  create index billing_transmission_detail_bill_id_cust_id on billing_transmission_detail(bill_id, customer_id)
GO

--todo drop because the index was missing one of the fields in QA?
drop index ticket_version.ticket_version_transmit_date
GO

IF NOT EXISTS (select * from sys.indexes where object_id = object_id('ticket_version') and name = 'ticket_version_transmit_date')
  CREATE INDEX ticket_version_transmit_date
   ON ticket_version(transmit_date, ticket_format) 
GO
