--QM-48  {Late add to repository} 
if not exists (select * from right_definition where entity_data = 'EmerViewer')
INSERT INTO right_definition
           (right_description
           ,right_type
           ,entity_data
           ,modifier_desc
           ,modified_date)
     VALUES
           ('Tickets - View Emergencies'
           ,'General'
           ,'EmerViewer'
           , 'Limitation does not apply to this right.'
           ,getDate())
GO