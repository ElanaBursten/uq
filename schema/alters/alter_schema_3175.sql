/* Drop oboslete stored procedures with SQLXML formatting that are no longer used by QM */

/* Rollback Notes:
   To be able to rollback this change, you should first use SSMS to extract copies
   of the nine stored procedures listed below from the QM schema to SQL text files.
   Those extracted scripts can be used to recreate the dropped stored procs if needed.
*/

if object_id('dbo.direct_report_ticket_totals_5') is not null
  drop procedure dbo.direct_report_ticket_totals_5
GO

if object_id('dbo.direct_report_ticket_totals_6') is not null
  drop procedure dbo.direct_report_ticket_totals_6
GO

if object_id('dbo.find_duplicates') is not null
  drop procedure dbo.find_duplicates
GO

if object_id('dbo.find_duplicates_renotif') is not null
  drop procedure dbo.find_duplicates_renotif
GO

if object_id('dbo.get_damage2') is not null
  drop procedure dbo.get_damage2
GO

if object_id('dbo.get_damage3') is not null
  drop procedure dbo.get_damage3
GO

if object_id('dbo.get_damage_all') is not null
  drop procedure dbo.get_damage_all
GO

if object_id('dbo.get_old_acked_tickets') is not null
  drop procedure dbo.get_old_acked_tickets
GO

if object_id('dbo.search_ticket') is not null
  drop procedure dbo.search_ticket
GO
