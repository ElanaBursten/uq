declare @Name varchar(25)
set @Name = 'ExportTimeToSolomon'

if not exists (select value from configuration_data where name = @Name) 
  insert into configuration_data (name, value, editable) values (@Name, '1', 1)

select dbo.GetConfigValue(@Name, @Name + ' did not get added to config') as ExportTimeToSolomon