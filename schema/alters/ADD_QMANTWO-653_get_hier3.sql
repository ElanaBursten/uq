USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[get_hier3]    Script Date: 9/20/2018 9:53:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


-- This SP is fast if you don't need active details, PC details, hierarchy path details, etc.
ALTER function [dbo].[get_hier3](@id int, @managers_only bit)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_report_level integer,
  h_report_to integer,  
  h_ad_username varchar(20) NULL,
  h_adp_login varchar(8) NULL
)
as
begin
  declare @adding_level int
  select @adding_level = 1

  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
     h_type_id, h_report_to, h_ad_username, h_adp_login, h_report_level)
  select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
     e.type_id, e.report_to, e.ad_username, e.adp_login, @adding_level
    from dbo.employee e
    left join dbo.employee man on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user
  WHILE @adding_level < 10
  BEGIN
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_type_id, h_report_to, h_ad_username, h_adp_login, h_report_level)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.type_id, e.report_to, e.ad_username, e.adp_login, r.h_report_level+1
      from dbo.employee e
       left join dbo.reference et on e.type_id = et.ref_id
       inner join @results r on e.report_to = r.h_emp_id
            AND r.h_report_level=@adding_level
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    SELECT @adding_level = @adding_level + 1
  END
  return
end



