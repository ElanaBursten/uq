/* Changes requried for Mantis issue # 2047 */

/* New fields for exported_employees */
alter table exported_employees
  add emp_type varchar(15) null,
    vehicle varchar(15) null
go


/* Recreate the export_employees stored procedure */
if object_id('dbo.export_employees') is not null
  drop procedure dbo.export_employees
GO

CREATE PROCEDURE export_employees (
  @CompanyName  varchar(20), -- locating_company.name or or '*' for all
  @ProfitCenter varchar(20)  -- employee's normal profit center or '*' for all
)
AS

set nocount on

declare @Emp table (
  emp_id int not null primary key,
  emp_number varchar(15) not null,
  short_name varchar(30) null,
  payroll_company_code varchar(20) null,
  emp_pc_code varchar(15) null,
  manager_emp_number varchar(15) not null,
  emp_type varchar(15) null,
  vehicle varchar(15) null
)

insert into @Emp
select
  e.emp_id,
  Coalesce(dbo.left_fill(e.emp_number, '0', 6), '000000') as emp_number,
  e.short_name,
  lc.payroll_company_code,
  epd.emp_pc_code,
  Coalesce(dbo.left_fill(rte.emp_number, '0', 6), '000000') as manager_emp_number,
  case when et.code in ('loc', 'sup', 'trn') then Upper(et.code) else 'OTH' end as emp_type,
  case when e.charge_cov = 1 then 'COV' else '' end as vehicle
from dbo.employee_pc_data(null) epd
  inner join employee e on e.emp_id = epd.emp_id
  left join locating_company lc on lc.company_id = e.company_id
  left join employee rte on rte.emp_id = e.report_to
  left join reference et on e.type_id = et.ref_id
where ((@CompanyName = '*') or (@CompanyName = lc.name))
  and ((@ProfitCenter   = '*') or (@ProfitCenter   = epd.emp_pc_code))

DELETE @Emp
FROM @Emp e
  inner join exported_employees ee
    on ee.emp_id = e.emp_id and
       ((ee.emp_number = e.emp_number) or ((ee.emp_number is null) and (e.emp_number is null))) and
       ((ee.short_name = e.short_name) or ((ee.short_name is null) and (e.short_name is null))) and
       ((ee.payroll_company_code = e.payroll_company_code) or ((ee.payroll_company_code is null) and (e.payroll_company_code is null))) and
       ((ee.emp_pc_code = e.emp_pc_code) or ((ee.emp_pc_code is null) and (e.emp_pc_code is null))) and
       ((ee.manager_emp_number = e.manager_emp_number) or ((ee.manager_emp_number is null) and (e.manager_emp_number is null))) and
       ((ee.emp_type = e.emp_type) or ((ee.emp_type is null) and (e.emp_type is null))) and
       ((ee.vehicle = e.vehicle) or ((ee.vehicle is null) and (e.vehicle is null)))

insert into exported_employees
  (emp_id, emp_number, short_name, payroll_company_code, emp_pc_code, manager_emp_number, emp_type, vehicle)
select
   emp_id, emp_number, short_name, payroll_company_code, emp_pc_code, manager_emp_number, emp_type, vehicle
from @Emp

select emp_number, short_name, payroll_company_code,
  emp_pc_code, manager_emp_number, emp_type, vehicle
from @Emp
order by 1, 2

GO

grant execute on export_employees to uqweb, QManagerRole
GO

/*
exec export_employees '*', '*'     -- 10213, 11080
exec export_employees 'UTI', '*'   -- 619
exec export_employees 'UTI', 'FRI' -- 248
select * from exported_employees
Clear exports:
delete from exported_employees

select * from employee
select * from locating_company
select * from dbo.employee_pc_data(null)
select * from dbo.employee_payroll_data2()
*/
