if not exists (select * from INFORMATION_SCHEMA.SEQUENCES where SEQUENCE_NAME = 'event_log_seq')
  CREATE SEQUENCE event_log_seq AS INT START WITH 21000
GO

/* if the primary key is defined wrong, drop the table and recreate it */
if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'event_log' and COLUMN_NAME = 'event_id' and DATA_TYPE = 'uniqueidentifier') 
  DROP TABLE event_log
GO

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'event_log') begin
  CREATE TABLE event_log (
    event_id integer NOT NULL DEFAULT NEXT VALUE FOR event_log_seq,
    aggregate_type integer NOT NULL,      -- 1 = Ticket, 2 = Damage, 3 = Work Order
    aggregate_id integer NOT NULL, 
    version_num integer NOT NULL,
    event_data varchar(5000) NOT NULL,    -- the event object
    event_added datetime NOT NULL DEFAULT GetDate(),
    constraint pk_event_log primary key nonclustered (event_id)
  )

  CREATE UNIQUE CLUSTERED INDEX event_log_aggregate_idx ON event_log(aggregate_type, aggregate_id, version_num)
end
GO

/* Clear any GUID value that got saved in config data from previous tests */ 
if exists (select * from configuration_data where name = 'LastProjMetricsEventID' and value like '%-%-%-%-%')
  update configuration_data set value = '' where name = 'LastProjMetricsEventID'

 
/* To rollback:
if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'event_log') 
  DROP TABLE event_log
GO
if exists (select * from INFORMATION_SCHEMA.SEQUENCES where SEQUENCE_NAME = 'event_log_seq')
  DROP SEQUENCE event_log_seq
GO
*/
