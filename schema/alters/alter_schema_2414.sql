-- New attachment.background_upload column to mark attachments uploaded by background thread
if not exists (select * from information_schema.columns where table_name = 'attachment' and column_name = 'background_upload')
  alter table attachment 
    add background_upload bit null
go

-- Add ATBUPL employee activity type reference code
if not exists (select ref_id from reference where type='empact' and code='ATBUPL') 
  insert into reference (type, code, description)
    values ('empact', 'ATBUPL', 'Background attachment upload')

-- Also need to deploy the new rpt_employee_activity.sql script.
