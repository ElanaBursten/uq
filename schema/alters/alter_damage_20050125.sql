/*
   Tuesday, January 25, 2005 11:58:50 AM
*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.damage ADD
	added_by int NULL
GO
COMMIT

BEGIN TRANSACTION
create trigger damage_i on damage
for insert
AS
update damage set added_by = modified_by where damage_id in (select damage_id from inserted)
GO
COMMIT
