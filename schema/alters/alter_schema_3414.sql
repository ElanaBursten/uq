-- Obsolete: Replaced by multi_open_totals3 on Sep. 23, 2010
if object_id('dbo.multi_open_totals2') is not null
  drop procedure dbo.multi_open_totals2
GO

if object_id('dbo.multi_open_totals3') is not null
  drop procedure dbo.multi_open_totals3
GO

if object_id('dbo.multi_open_totals4') is not null
  drop procedure dbo.multi_open_totals4
GO
