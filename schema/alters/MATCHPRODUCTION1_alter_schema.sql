--Schema Review:  
--These are changes that have happened in UQ Production that have not been updated in our code.
--DO NOT APPLY TO PRODUCTION (as they have already been made)
--Make sure these changes have been applied to build machine

ALTER TABLE damage
ALTER COLUMN client_claim_id varchar(30)

IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'customer' and COLUMN_NAME='contract_review_due_date')
ALTER TABLE customer
ADD contract_review_due_date date NULL

--In the former build, the contract_review_due_date was under client so it should be removed:
IF EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'client' and COLUMN_NAME='contract_review_due_date')
ALTER TABLE client
DROP COLUMN contract_review_due_date

--These are the production values that were reset for Build machine.
ALTER TABLE employee_activity
ALTER COLUMN lat decimal(9,6) NULL

ALTER TABLE employee_activity
ALTER COLUMN lng decimal(9,6) NULL

--ESRI's ArcInfo Precision (previously defined for employee_activity)
--ALTER TABLE employee_activity
--ALTER COLUMN lat decimal(12,9) NULL

--ALTER TABLE employee_activity
--ALTER COLUMN lng decimal(12,9) NULL

