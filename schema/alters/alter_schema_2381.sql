-- Add new reason codes to the reference table:
insert into reference (type, code, description, sortby) values ('tsereason', 'TERMINATED', 'Employee has been terminated', 1)
insert into reference (type, code, description, sortby) values ('tsereason', 'SUSPENDED', 'Employee on suspension', 2)
insert into reference (type, code, description, sortby) values ('tsereason', 'ABSENT', 'Employee absence - Sick or Vacation ', 3)
insert into reference (type, code, description, sortby) values ('tsereason', 'HOLIDAY', 'Holiday', 4)
insert into reference (type, code, description, sortby) values ('tsereason', 'UNABLE', 'Employee was unable to enter time', 5)
insert into reference (type, code, description, sortby) values ('tsereason', 'CORRECTED', 'Employee entered incorrect time', 6)
insert into reference (type, code, description, sortby) values ('tsereason', 'ACCIDENT', 'Employee left work early due to accident/injury', 7)
insert into reference (type, code, description, sortby) values ('tsereason', 'NOBREAK', 'Employee failed to take the required break', 8)
insert into reference (type, code, description, sortby) values ('tsereason', 'MISCONDUCT', 'Misconduct investigation - Employee was not working - Needs HR e-mail approval', 9)

-- add new reason code field & foreign key to the timesheet_entry table.
alter table timesheet_entry 
  add reason_changed int null
go

alter table timesheet_entry
  add constraint FK_timesheet_entry_reason_changed foreign key (reason_changed) references reference (ref_id)
go
