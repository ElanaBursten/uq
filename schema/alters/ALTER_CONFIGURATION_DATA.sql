delete
from configuration_data
where name = 'SCANA_BaseURL'

INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[modified_date]
           ,[editable]
           ,[description])
     VALUES
           ('SCANA_BaseURL'
           ,'http://qacluster.utiliquest.com/UTQPortal/SCANA/QForms/Home/QForm?'
           ,GetDate()
           ,1
           ,'Form for  EFDC Manual ticket')
GO

SELECT *
  FROM [QM].[dbo].[configuration_data]
  where name = 'SCANA_BaseURL'

GO

