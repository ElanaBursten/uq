alter table damage_invoice
  add invoice_approved bit not null default 0
go

insert right_definition (right_description) values ('Damages - Approve Invoice') -- Id 42
go
