-- 2615: add unique constraint to group_definition.priority
if not exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
  WHERE TABLE_NAME = 'group_definition' and CONSTRAINT_NAME = 'uk_group_definition_priority')
  ALTER TABLE group_definition 
    ADD CONSTRAINT uk_group_definition_priority UNIQUE NONCLUSTERED (priority)

GO

