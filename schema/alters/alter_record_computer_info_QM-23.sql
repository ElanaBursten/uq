USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[record_computer_info]    Script Date: 11/25/2019 12:11:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[record_computer_info]
    (
    @EmployeeID int,
    @WindowsUser varchar(25),
    @OsPlatform int,
    @OsMajorVersion int,
    @OsMinorVersion int,
    @OsServicePackVersion int,
    @ComputerName varchar(25),
    @DellServiceTag varchar(25),
    @HotFixList varchar(3000),
    @ComputerSerial varchar(25) = null,
    @DomainLogin varchar(25) = null,
    @ComputerModel varchar(50) = null,
    @AssetTag varchar(30) = null,
    @AirCardPhoneNo varchar(15) = null,
    @AirCardESN varchar(20) = null,
	@Platdate datetime=null  --QM-23  sr
    )
as

set nocount on

-- Older clients don't send any of this data
if (@WindowsUser = '') or @WindowsUser is null
  return

declare @LastEmpRowDate datetime
declare @MatchingRowID integer

set @LastEmpRowDate =
  (select max(insert_date) from computer_info where emp_id = @EmployeeID)

update employee  --QM-23  sr
set [plat_update_date] =@Platdate
where emp_id =@EmployeeID 

-- Only record the computer info when it has changed
set @MatchingRowID =
 (select top 1 computer_info_id
  from computer_info
  where
    emp_id = @EmployeeID and
    insert_date = @LastEmpRowDate and
    windows_user = @WindowsUser and
    os_platform = @OsPlatform and
    os_major_version = @OsMajorVersion and
    os_minor_version = @OsMinorVersion and
    os_service_pack = @OsServicePackVersion and
    computer_name = @ComputerName and
    ((computer_serial = @ComputerSerial) or (computer_serial is null and @ComputerSerial is null)) and
    ((domain_login = @DomainLogin) or (domain_login is null and @DomainLogin is null)) and
    ((computer_model = @ComputerModel) or (computer_model is null and @ComputerModel is null)) and
    ((asset_tag = @AssetTag) or (asset_tag is null and @AssetTag is null)) and    
    ((aircard_phone_no = @AirCardPhoneNo) or (aircard_phone_no is null and @AirCardPhoneNo is null)) and    
    ((aircard_phone_esn = @AirCardESN) or (aircard_phone_esn is null and @AirCardESN is null)) and    
    service_tag  = @DellServiceTag)

if @MatchingRowID is null
begin
  insert into computer_info
    (emp_id, windows_user, os_platform, os_major_version, os_minor_version,
     os_service_pack, computer_name, computer_serial, domain_login, service_tag,
     computer_model, asset_tag, aircard_phone_no, aircard_phone_esn)
  values
    (@EmployeeID, @WindowsUser, @OsPlatform, @OsMajorVersion, @OsMinorVersion,
     @OsServicePackVersion, @ComputerName, @ComputerSerial, @DomainLogin, @DellServiceTag,
     @ComputerModel, @AssetTag, @AirCardPhoneNo, @AirCardESN)

  select @MatchingRowID = SCOPE_IDENTITY()
end

-- Set active, sync_id for the most recently touched computer_info record 
If (@computerSerial is not null) 
begin
  update computer_info set active = 0 where computer_serial = @ComputerSerial and active = 1 and computer_info_id <> @MatchingRowID
  update computer_info set active = 1, sync_id = (select top 1 sync_id from sync_log where emp_id = @EmployeeID order by sync_date desc)
    from computer_info where computer_info_id = @MatchingRowID
end



declare @HotFixes table (hotfix varchar(99) null)

insert into @HotFixes
select S from dbo.StringListToTable(@HotFixList)

-- Delete uninstalled hotfixes
delete from computer_hotfix
where computer_info_id = @MatchingRowID
  and hotfix_id not in (select left(hotfix, 20) from @HotFixes)

-- Add newly installed hotfixes
insert into computer_hotfix
  (computer_info_id, hotfix_id)
select
  @MatchingRowID, left(hotfix, 20)
  from @HotFixes
where not exists
  (select hotfix_id from computer_hotfix
   where computer_info_id = @MatchingRowID
     and hotfix_id = hotfix)
