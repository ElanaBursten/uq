-- 2383: add new AttachmentsBackgroundUpload right if it doesn't exist
if not exists (select right_id from right_definition where entity_data = 'AttachmentsBackgroundUpload')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Attachments - Background Upload', 'General', 'AttachmentsBackgroundUpload', 'Limitation is the location_id to use for uploads.')
else
  update right_definition set modifier_desc = 'Limitation is the location_id to use for uploads.' 
  where entity_data = 'AttachmentsBackgroundUpload'
