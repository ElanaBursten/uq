if not exists(select * from sys.key_constraints where name = 'pk_ticket_ack_match')
  alter table ticket_ack_match
  add constraint pk_ticket_ack_match primary key nonclustered (ticket_ack_match_id)
go

if not exists(select * from sys.key_constraints where name = 'pk_ticket_grid_log')
  alter table ticket_grid_log
  add constraint pk_ticket_grid_log primary key nonclustered (ticket_version_id, client_code)
go

if not exists(select * from sys.key_constraints where name = 'pk_ticket_hp')
  alter table ticket_hp
  add constraint pk_ticket_hp primary key nonclustered (ticket_hp_id)
go

if exists(select table_name from information_schema.tables where table_name='notification_email_highprofile')
  drop table notification_email_highprofile
go

if not exists(select * from sys.key_constraints where name = 'pk_responder_multi_queue')
  alter table responder_multi_queue
  add constraint pk_responder_multi_queue primary key nonclustered (locate_id, respond_to, insert_date)
go

if not exists(select * from sys.key_constraints where name = 'pk_responder_queue')
  alter table responder_queue
  add constraint pk_responder_queue primary key nonclustered (locate_id, insert_date)
go

if not exists(select * from sys.key_constraints where name = 'pk_response_ack_waiting')
  alter table response_ack_waiting
  add constraint pk_response_ack_waiting primary key nonclustered (response_id)
go

if not exists(select * from sys.key_constraints where name = 'pk_wo_responder_queue')
  alter table wo_responder_queue
  add constraint pk_wo_responder_queue primary key nonclustered (wo_id, insert_date)
go

-- These indexes are no longer needed

if exists (select * from sys.indexes where object_id = object_id('responder_queue') and name = 'responder_queue_locate_id')
  drop index responder_queue_locate_id on responder_queue
go

if exists (select * from sys.indexes where object_id = object_id('responder_multi_queue') and name = 'responder_multi_queue_locate_id')
  drop index responder_multi_queue_locate_id on responder_multi_queue
go

if exists (select * from sys.indexes where object_id = object_id('wo_responder_queue') and name = 'wo_responder_queue_wo_id')
  drop index wo_responder_queue_wo_id on wo_responder_queue
go
