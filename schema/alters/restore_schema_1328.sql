-- Drop field from damage_bill
if (select count(*) from information_schema.columns 
  where table_schema='dbo' and table_name='damage_bill' and column_name='invoiced') = 1 begin
  alter table damage_bill
    drop column invoiced 
end
go

-- drop RPT_billable_damages sp
if object_id('dbo.RPT_billable_damages') is not null
  drop procedure dbo.RPT_billable_damages
go
