	
--QMANTWO-564_Alter_Schema.sql - Multi select Ticket Priorities	
	
if not Exists(select * from right_definition WHERE entity_data = 'MultiSelectPriorityChange')

INSERT INTO right_definition
           (right_description,right_type,entity_data,parent_right_id,modifier_desc)
     VALUES
           ('Tickets - MultiSelect Priorities','General','MultiSelectPriorityChange',NULL,
           'Limitation does not apply to this right.')
GO

--Select * from right_definition WHERE entity_data = 'MultiSelectPriorityChange'  65754
--Select * from right_definition order by right_description

