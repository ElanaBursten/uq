USE [QM]
GO

/****** Object:  Table [dbo].[billing_ohm_weekly]    Script Date: 9/23/2018 3:45:42 PM ******/
DROP TABLE [dbo].[billing_ohm_weekly]
GO

/****** Object:  Table [dbo].[billing_ohm_weekly]    Script Date: 9/23/2018 3:45:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[billing_ohm_weekly](
	[Name] [varchar](50) NULL,
	[bom_id] [int] IDENTITY(1,1) NOT NULL,
	[JO] [int] NULL,
	[Deliverable_Number] [varchar](10) NULL,
	[Deliverable_Name] [varchar](50) NULL,
	[Total_Hours] [int] NULL,
	[Training_Hours] [int] NULL,
	[Collection_Hours] [int] NULL,
	[Total_Footage] [varchar](10) NULL,
	[Main] [varchar](10) NULL,
	[Service] [varchar](10) NULL,
	[Main_CHPO_Footage] [varchar](10) NULL,
	[Service_CHPO_Footage] [varchar](10) NULL,
 CONSTRAINT [PK_billing_ohm_weekly] PRIMARY KEY CLUSTERED 
(
	[bom_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

