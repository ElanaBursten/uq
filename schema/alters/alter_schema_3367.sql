if exists(select name from sysobjects where name = 'work_order_work_type_iu' and type = 'TR')
  drop trigger work_order_work_type_iu
GO
create trigger work_order_work_type_iu on work_order_work_type
for insert, update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
    -- Any changes to their work type or the active flags
  if (select count(*) from deleted d inner join inserted i on d.work_type_id = i.work_type_id
      where (d.work_type = 'VI' or d.work_type = 'VC')
      and (d.work_type <> i.work_type or d.active <> i.active))>0
  begin
    rollback transaction
    raiserror('Error: Changing the VI or VC codes and their Active flags is prohibited.',16,1)
    return
  end
   
  update work_order_work_type
  set modified_date = GetDate()
  from inserted I
  where work_order_work_type.work_type_id = I.work_type_id
end
GO
