-- Initially populate the employee_history table from the employee data...

declare @AllEmps TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  x integer,
  emp_pc_code varchar(15) NULL
)

insert into @AllEmps select * from dbo.employee_pc_data(null)

-- Add a history row for all of the current employee data
insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
  emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
  create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
  timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
  crew_num, autoclose, company_id, installer_number, rights_modified_date,
  active_pc
)
select '1990-01-01', '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
  emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
  create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
  timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
  crew_num, autoclose, company_id, installer_number, rights_modified_date,
  emps.emp_pc_code
from employee e
  inner join @AllEmps emps on emps.emp_id = e.emp_id
go
