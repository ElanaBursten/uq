USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[asset_export]    Script Date: 5/6/2019 11:11:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER proc [dbo].[asset_export] (
  @company varchar(4),
  @charge_cov bit ,
  @AcctCat varchar(16),
  @Amount varChar(6),
  @LossAmt varChar(6),         --QMANTWO-744 (pass thru)      
  @Descrip  varchar(30),
  @TransDate varchar(10)
)
as
-- P3H  = UTQ
-- M1U  = STS
-- M1T  = LOC

Declare
  @Level1 varchar(6),
  @Units varchar(4), 
  @EarningsType varchar(50);
  --set @EarningsType = '(PL1, LOC, MLO)'
  set @Level1 = 'Level1'
  set @Units = '0.00';


DECLARE @emps TABLE (
  --_emp_id int not null primary key,
  --empNo     varchar(6)   null,  
  --empManNo varchar(6)   null,  
  --solomon_pc varchar(4)   null,   
  --current_mgr_id integer not null,
  Level1 varChar(6) not null,
  projectID varchar(10) not null,
  taskID varchar(16) not null,
  acctCat varchar(16) not null,
  transDate varchar(10) not null,
  units varchar(4) not null,
  Amount varChar(6) not null,
  LossAmt varChar(6),
  Descrip varchar(30) not null,
  company varchar(4),
  last_name varchar(30),
  charge_cov bit,
  EarningsType varchar(8)
)


insert into @emps 
  select 
 --   e.emp_id, 
	--dbo.format_emp_number(pd.Emp_Number) as Emp_Number, 
	--dbo.format_emp_number(pd.Man_Emp_Number) as Man_Emp_Number,
	--pc.solomon_pc,
 --   Coalesce(e.report_to, -1) as current_mgr_id, 
	@Level1,
	projectID='0000000000',
	taskID = pc.solomon_pc+dbo.format_emp_number(pd.Man_Emp_Number)+dbo.format_emp_number(pd.Emp_Number),
    @AcctCat, 
	CONVERT(VARCHAR(10), @TransDate, 103),
	@Units, 
	@Amount,
	@LossAmt,
	@Descrip,
    IsNull(lc.payroll_company_code, '---') as Company,
    e.last_name, 
	e.charge_cov,
	pd.EarningsType
  from dbo.employee_payroll_data_Ulti(NULL) pd   
    inner join employee e on (e.emp_id=pd.emp_id)
    inner join reference tr on (e.timerule_id=tr.ref_id)
    inner join profit_center pc on (pc.pc_code=pd.emp_pc_code)
    inner join locating_company lc on (lc.company_id=e.company_id)
  where pd.EarningsType in ('PL1', 'PL2', 'PL3', 'LOC', 'MLO', 'SUP')
  --( case when @EarningsType = 'ALL' then pd.EarningsType 
  --else @EarningsType END)
  and pd.EarningsType is not null
  and e.emp_number is not null
  and e.active=1
  and (e.charge_cov=@charge_cov or @charge_cov is null) 
  and lc.payroll_company_code = @company



  Select *
  from @emps 
GO


