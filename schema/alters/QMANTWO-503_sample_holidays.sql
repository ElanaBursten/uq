/* QMANTWO-503 Sample Holidays */

------------------------------------------
INSERT holiday (cc_code, holiday_date, billing_holiday) VALUES (N'*', CAST(N'2017-11-23 00:00:00.000' AS DateTime), 1) --THANKSGIVING
INSERT holiday (cc_code, holiday_date, billing_holiday) VALUES (N'*', CAST(N'2017-12-25 00:00:00.000' AS DateTime), 1) --CHRISTMAS
INSERT holiday (cc_code, holiday_date, billing_holiday) VALUES (N'*', CAST(N'2018-01-01 00:00:00.000' AS DateTime), 1) --NEW YEARS