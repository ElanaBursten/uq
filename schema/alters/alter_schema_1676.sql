-- Create new employee activity table
create table employee_activity (
   emp_activity_id int identity(22000, 1) not null primary key nonclustered,
   emp_id int not null,
   activity_date datetime not null, /* Employee time zone */
   activity_type varchar(12) not null,
   details varchar(50) null,
   insert_date datetime not null default GetDate(),
)
go

-- Cluster by activity date to keep from clustering on autoinc ID
create clustered index employee_activity_activity_date
   on employee_activity(activity_date)
go

-- Employee / date index so reports work
create index employee_activity_emp_id_activity_date
   on employee_activity(emp_id, activity_date)
go

-- Foreign key link to employee
alter table employee_activity
   add constraint employee_activity_fk_employee
   foreign key (emp_id) references employee (emp_id)
go

-- Create new stored procedure for the reports
if object_id('dbo.RPT_employee_activity') is not null
	drop procedure dbo.RPT_employee_activity
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_employee_activity(
  @ManagerId int,
  @StartDate datetime, 
  @EmployeeStatus int
)
as
  set nocount on

  declare @result table (
    r_emp_id int,
    mgr_id int,
    mgr_short_name varchar(30) NULL,
    report_level integer NOT NULL,
    emp_number varchar(15) NULL,
    short_name varchar(30) NULL,
    last_name varchar(30) NULL,
    h_report_level integer,
    h_namepath varchar(450) NULL,
    h_idpath varchar(160) NULL,
    tse_id int NULL,
    primary key(r_emp_id)
  )

  declare @emp_activity table(
    emp_id int null,
    activity_date datetime null,
    activity_type varchar(12) null
  )

  declare @emp_id int
  declare @EndDate datetime
  declare @AfterEndDate datetime

  select @EndDate = dateadd(d, 6, @StartDate)
  -- used to make the locate_status query more efficient
  select @AfterEndDate = dateadd(d, 4, @EndDate)


  -- get all the emps for this manager in result table
  insert into @result (r_emp_id, mgr_id, mgr_short_name, emp_number,
    short_name, last_name, report_level, h_namepath, h_idpath)
  select h_emp_id, h_report_to, h_report_to_short_name, h_emp_number,
    h_short_name, h_last_name, h_report_level, h_namepath, h_idpath
  from get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)
  where h_emp_id <> @ManagerID

  insert into @emp_activity (activity_date, emp_id, activity_type) 

    -- All new logged activities
    select ea.activity_date, ea.emp_id, ea.activity_type
    from employee_activity ea inner join @result r on ea.emp_id = r.r_emp_id
    where -- ea.emp_id = @emp_id and
          ea.activity_date between @StartDate and @EndDate

    UNION ALL

    -- Uploading an attachment
    Select a.attach_date, a.attached_by, 'UPLAT'
    from attachment a inner join @result r on a.attached_by = r_emp_id
    where -- a.attached_by = @emp_id and
          a.attach_date between @StartDate and @EndDate

    UNION ALL

    -- Entering Time - Timesheet entry
    select tse.entry_date_local, tse.entry_by, 'TSENT'
    from timesheet_entry tse inner join @result r on tse.entry_by = r_emp_id
    where -- tse.entry_by = @emp_id and
          tse.entry_date_local between @StartDate and @EndDate and
          tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- NEED CHANGES TO GET USER LOCAL DATE-TIME
    -- Manual ticket entry � might need to extend logging shortly
    select t.transmit_date, t.channel, 'TKMAN'
    from ticket t inner join @result r on t.channel = r_emp_id
    where -- t.channel = @emp_id and 
          t.status like 'MANUAL%' and 
          t.transmit_date between @StartDate and @EndDate

    UNION ALL

    -- Change status of a locate
    select ls.status_date, ls.statused_by, 'LOCST'
    from locate_status ls inner join @result r on ls.statused_by = r_emp_id
    where -- ls.statused_by = @emp_id and
          ls.status_date between @StartDate and @EndDate and
          ls.insert_date between @StartDate and @AfterEndDate

    UNION ALL
 
    -- Add hours to a locate
    select lh.entry_date, lh.emp_id, 'LOCHR'
    from locate_hours lh inner join @result r on lh.emp_id = r_emp_id
    where --- lh.emp_id = @emp_id and
          lh.entry_date between @StartDate and @EndDate

    UNION ALL

    -- Add a note
    select n.entry_date, n.uid, 'NOTES'
    from notes n inner join @result r on n.uid = r_emp_id
    where -- n.uid = @emp_id and
          n.entry_date between @StartDate and @EndDate

    UNION ALL

    -- Sync
    select sl.local_date, sl.emp_id, 'SYNC'
    from sync_log sl inner join @result r on sl.emp_id = r_emp_id
    where -- sl.emp_id = @emp_id and
          sl.local_date between @StartDate and @EndDate

    UNION ALL

    -- NEED CHANGES TO GET USER LOCAL DATE-TIME
    -- Moving a ticket from one to another
    select a.insert_date, a.added_by, 'TKASG'
    from assignment a inner join @result r on a.added_by = r_emp_id
    where -- a.added_by = @emp_id and
          a.insert_date between @StartDate and @EndDate
    and a.added_by <> 'router'

    UNION ALL

    -- NEED CHANGES TO GET USER LOCAL DATE-TIME
    -- Running Reports
    select rl.request_date, user_id, 'RUNRE'
    from report_log rl inner join @result r on rl.user_id = r_emp_id
    where -- rl.user_id = @emp_id and
          rl.request_date between @StartDate and @EndDate

    UNION ALL

    -- Read a message
    select md.ack_date, md.emp_id, 'RDMSG'
   from message_dest md inner join @result r on md.emp_id = r_emp_id
    where -- md.emp_id = @emp_id and
          md.ack_date between @StartDate and @EndDate

    UNION ALL

    -- Approve time
    select tse.approve_date_local, tse.approve_by, 'TAPRV'
    from timesheet_entry tse inner join @result r on tse.approve_by = r_emp_id
    where -- tse.approve_by = @emp_id and
          tse.approve_date_local between @StartDate and @EndDate
    
    order by 1
--  end insert

  delete from @result where r_emp_id not in (select emp_id from @emp_activity)

  -- employee list
  select * from @result
   order by report_level, mgr_short_name, mgr_id, short_name

  -- for grouping
  select distinct emp_id, convert(datetime, convert(varchar, activity_date, 110)) as activity_date
  from @emp_activity

  -- the activity datas
  select * from @emp_activity
  order by activity_date

Go

/*

exec dbo.RPT_employee_activity 2676, '2006-11-07T00:00:00.000', 1

*/
