--Add new right permitting a user to select particular damages for export; otherwise all damages are exported from MyTickets screen.
if not exists (select * from right_definition where entity_data = 'DamagesSelectiveExport')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesSelectiveExport', 'Damages - Selective Export', 'General', 'Limitation does not apply to this right.')
go
--Add new activity type for damage export
if not exists(select * from reference where type = 'empact' and code = 'DMGRTEXP')
  insert into reference (type, code, description, active_ind) values ('empact','DMGRTEXP', 'Export Damage Route', 1)
go


