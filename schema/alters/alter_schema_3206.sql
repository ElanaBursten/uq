if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS
 where COLUMN_NAME = 'pto_hours' AND TABLE_NAME = 'timesheet_entry') 
begin
  alter table timesheet_entry
    add pto_hours decimal(5,2)
end
go

if not exists (select name from configuration_data where name = 'ExportPTOHours')
begin
  insert into configuration_data(name, value, editable)
  values ('ExportPTOHours', 'N', 1)
end
go

/* To rollback these changes:

Prior to deploying the updated payroll_export_adp.sql script, use SQL Server 
Management Studio (or equivalent tool) to export a copy of the payroll_export_adp4
stored procedure. Deploy that exported script in case you need to rollback the 3206
changes.
 
-- drop the new timesheet_entry.pto_hours column
if exists (select * from information_schema.columns where table_name='timesheet_entry' and column_name='pto_hours')
  alter table timesheet_entry drop column pto_hours
go

-- remove the new configuration data row
delete configuration_data where name='ExportPTOHours'
*/