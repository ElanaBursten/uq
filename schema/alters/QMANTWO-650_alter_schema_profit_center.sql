--adds the solomon_pc column to the profit_center center table.  populated by a separate script  --sr 

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'profit_center') 
         AND name = 'solomon_pc')

    ALTER TABLE profit_center
       ADD solomon_pc varchar(4)

GO

---------------------------------------------

Update [dbo].[profit_center]
set [solomon_pc] = (substring([timesheet_num],(0),(4))+'0')

-------------------------------------------------------------------
--Add Configuration Data-------------------------------------------

/****** Script for SelectTopNRows command from SSMS  ******/
--makes an entry in the configuration_data table to direct how the payroll export works
Delete
  FROM configuration_data
  where name = 'TimesheetExport'
GO


INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[modified_date]
           ,[editable]
           ,[description])
     VALUES
           ( 'TimesheetExport'
           ,'ADP'
           ,getDate()
           ,1
           ,'ADP or Ulti.  Defaults to Ulti.  Not sync''d')
GO

--Reversal
--delete from  [dbo].[configuration_data]
--where [name] = 'TimesheetExport'
--and [description] = 'ADP or Ulti.  Defaults to Ulti.  Not sync''d'


-------------------------------------------------------------------
--Permissions------------------------------------------------------
 update right_definition
  set modifier_desc = 'ADP or Ulti must be selected' 
  where right_description = 'Timesheets - Export'

