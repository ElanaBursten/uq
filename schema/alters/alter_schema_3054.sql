-- Inserts missing notes.uid references into the users table so an FK can be added
if exists(select top 1 * from notes where uid not in (select uid from users)) 
begin
  set identity_insert users on
  insert into users (uid, login_id, first_name, last_name, active_ind, chg_pwd, chg_pwd_date)
    select distinct notes.uid, 
      'user_' + convert(varchar(8), notes.uid) as login_id,
      'unknown' as first_name,
      'recovered user' as last_name, 
      0 as active_ind,
      1 as chg_pwd,
      GetDate() as chg_pwd_date
    from notes 
    where notes.uid not in (select users.uid from users)
  set identity_insert users off
end
go

-- Add uid foreign key to notes table
if not exists(select * from sys.foreign_keys where object_id = OBJECT_ID(N'dbo.notes_fk_users') and parent_object_id = OBJECT_ID(N'dbo.notes'))
  alter table notes with check add constraint notes_fk_users foreign key(uid) references users(uid) 
go
