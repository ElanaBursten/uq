--Add new right that allows user to select Work Orders for Streets and Trips Export
if not exists (select * from right_definition where entity_data = 'WorkOrdersSelectiveExport')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('WorkOrdersSelectiveExport', 'Work Orders - Selective Export', 'General', 'Limitation does not apply to this right.')
go

--New employee activity for Work Order Street and Trips Export
if not exists (select * from reference where type = 'empact' and code = 'WORTEXP')
  insert into reference (type, code, description) values ('empact', 'WORTEXP', 'Export Work Order Route')
go

