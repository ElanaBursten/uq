-- add new column to the employee table that flags a person as an Incentive Pay participant.
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'employee' and COLUMN_NAME = 'incentive_pay')
  alter table employee add incentive_pay bit not null default 0
go

-- add the new column to the employee_history table.
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'employee_history' and COLUMN_NAME = 'incentive_pay')
  alter table employee_history add incentive_pay bit null
go

-- recreate employee_iu trigger to populate the new incentive_pay column in employee_history
if exists(select name from sysobjects where name = 'employee_iu' and type = 'TR')
  drop trigger employee_iu
go
create trigger dbo.employee_iu on employee
for insert, update
not for replication
as
begin
  set nocount on

  declare @ChangeDate datetime
  set @ChangeDate = getdate()

  -- Don't update modified date or add history for rights_midified_date changes alone
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)
    or update(incentive_pay)))
  begin
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)

    if update(report_to) or update(repr_pc_code)
    begin -- This change may modify the effective PC of non-updated employees
      -- Recalculate and store any changes to everyone below these employees

      declare @AllEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY,
        x integer,
        emp_pc_code varchar(15) NULL
      )

      insert into @AllEmps select * from employee_pc_data(null)

      declare @ChangedEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY
      )

      insert into @ChangedEmps
        select emps.emp_id
        from @AllEmps emps
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate
        where ((emps.emp_pc_code <> eh.active_pc)
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))
          or emps.emp_id in (select emp_id from inserted)

      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)

      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay
      )
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets, incentive_pay
      from employee e
        inner join @AllEmps emps on emps.emp_id = e.emp_id
      where e.emp_id in (select emp_id from @ChangedEmps)
    end
    else -- This change only affects the exact employee(s) being modified
    begin
      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)

      -- Record the new/current employee data
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay
      )
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets,
        incentive_pay
      from inserted
    end
  end
end
go


/* To rollback:
-- Restore the previous version of the employee_iu trigger:
if exists(select name from sysobjects where name = 'employee_iu' and type = 'TR')
  drop trigger employee_iu
go
create trigger dbo.employee_iu on employee
for insert, update
not for replication
as
begin
  set nocount on

  declare @ChangeDate datetime
  set @ChangeDate = getdate()

  -- Don't update modified date or add history for rights_midified_date changes alone
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)))
  begin
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)

    if update(report_to) or update(repr_pc_code)
    begin -- This change may modify the effective PC of non-updated employees
      -- Recalculate and store any changes to everyone below these employees

      declare @AllEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY,
        x integer,
        emp_pc_code varchar(15) NULL
      )

      insert into @AllEmps select * from employee_pc_data(null)

      declare @ChangedEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY
      )

      insert into @ChangedEmps
        select emps.emp_id
        from @AllEmps emps
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate
        where ((emps.emp_pc_code <> eh.active_pc)
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))
          or emps.emp_id in (select emp_id from inserted)

      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)

      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets
      from employee e
        inner join @AllEmps emps on emps.emp_id = e.emp_id
      where e.emp_id in (select emp_id from @ChangedEmps)
    end
    else -- This change only affects the exact employee(s) being modified
    begin
      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)

      -- Record the new/current employee data
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets
      from inserted
    end
  end
end
go

-- drop column from employee_history
if exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'employee_history' and COLUMN_NAME = 'incentive_pay')
  alter table employee_history drop column incentive_pay 
go

-- drop column and it's default constraint from employee
declare @Tbl varchar(256), @Col varchar(256), @Sql varchar(1000)
set @Tbl='employee'
set @Col='incentive_pay'

if exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @Tbl and COLUMN_NAME = @Col) 
begin
  -- get the incentive_pay default value constaint name
  declare @Con nvarchar(256)
  select @Con = d.name
  from sys.tables t
  join sys.default_constraints d on d.parent_object_id = t.object_id
  join sys.columns c on c.object_id = t.object_id
    and c.column_id = d.parent_column_id
  where t.name = @Tbl and c.name = @Col

  -- now we have the constaint, drop it and the incentive_pay column
  set @Sql = 'alter table ' + @Tbl + ' drop constraint ' + @Con + ', column ' + @Col
  execute(@Sql)
end
go
*/