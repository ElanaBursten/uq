/* Undo commands: 
drop index profit_center.profit_center_adp_code
alter table profit_center drop column adp_code
go
*/

alter table profit_center add adp_code varchar(2) null
go

-- set default values for the newly added field
declare @prefixes char(26)
declare @idx int
declare @counter int
declare @max int

set @prefixes = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
set @idx = 1
set @max = ((select count(*) from profit_center) / 10) + 1


while @idx <= @max 
begin
  set @counter = 0
  while @counter < 10
  begin
    update profit_center
    set adp_code = SubString(@prefixes, @idx, 1) + cast(@counter as char(1)) 
    where pc_code = (select top 1 pc_code from profit_center where adp_code is null)
    set @counter = @counter + 1
  end
  set @idx = @idx + 1
end
go

alter table profit_center alter column adp_code varchar(2) not null
go
create unique nonclustered index profit_center_adp_code on profit_center (adp_code)
go

