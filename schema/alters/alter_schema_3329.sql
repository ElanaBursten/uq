set nocount on

-- Work Order details specific to inspection work orders
if not exists (select * from information_schema.tables where table_name='work_order_inspection')
begin
  CREATE TABLE work_order_inspection (
    wo_id integer not null, -- 1 to 1 relationship with work_order
    account_number varchar(50) not null,
    premise_id varchar(50) not null,
    meter_number varchar(80) null,
    compliance_due_date datetime not null,
   
    actual_meter_number varchar(80) null,
    actual_meter_location varchar(10) null,  -- reference table code (type='metrloc')
    building_type varchar(10) null,       -- reference table code (type='bldtype')
    cga_visits integer null,
    map_status varchar(10) null,          -- reference table code (type='mapstat')
    mercury_regulator varchar(10) null,   -- reference table code (type='ynub')
    vent_clearance_dist varchar(10) null, -- reference table code (type='ventclr')
    vent_ignition_dist varchar(10) null,  -- reference table code (type='ventign')
    alert_first_name varchar(20) null,
    alert_last_name varchar(30) null,
    alert_order_number varchar(50) null,
    inspection_change_date datetime null, 
    modified_date datetime default GetDate(),
    constraint pk_work_order_inspection primary key nonclustered (wo_id)
  ) 
  alter table work_order_inspection
    add constraint work_order_inspection_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
end
GO

-- List of valid work order remedy work types
if not exists (select * from information_schema.tables where table_name='work_order_work_type')
begin
  CREATE TABLE work_order_work_type (
    work_type_id integer identity not null,
    work_type varchar(10) not null,
    work_description varchar(100) not null,
    flag_color varchar(10) null,
    alert  bit not null default 0,
    active bit not null default 1,
    modified_date datetime default GetDate(),
    constraint pk_work_order_work_type primary key nonclustered (work_type_id)
  )
end
GO

if not exists (select * from sys.indexes where name = 'wo_work_type_work_type')
  create unique nonclustered index wo_work_type_work_type on work_order_work_type (work_type)
GO

-- Remedy work selected for a given work order inspection
if not exists (select * from information_schema.tables where table_name='work_order_remedy')
begin
  CREATE TABLE work_order_remedy (
    wo_remedy_id integer identity (100,1) not null,
    wo_id integer not null,
    work_type_id int not null,
    active bit not null default 1,
    modified_date datetime default GetDate(),
    constraint pk_work_order_remedy primary key nonclustered (wo_remedy_id)
  )
  alter table work_order_remedy
    add constraint work_order_remedy_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
  alter table work_order_remedy
    add constraint work_order_remedy_fk_work_type_id
    foreign key (work_type_id) references work_order_work_type (work_type_id)
end
GO

if exists(select name from sysobjects where name = 'work_order_inspection_iu' and type = 'TR')
  drop trigger work_order_inspection_iu
GO
create trigger work_order_inspection_iu on work_order_inspection
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_inspection
  set modified_date = GetDate()
  from inserted I
  where work_order_inspection.wo_id = I.wo_id
end
GO

if exists(select name from sysobjects where name = 'work_order_remedy_iu' and type = 'TR')
  drop trigger work_order_remedy_iu
GO
create trigger work_order_remedy_iu on work_order_remedy
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_remedy
  set modified_date = GetDate()
  from inserted I
  where work_order_remedy.wo_id = I.wo_id
end
GO

if exists(select name from sysobjects where name = 'work_order_work_type_iu' and type = 'TR')
  drop trigger work_order_work_type_iu
GO
create trigger work_order_work_type_iu on work_order_work_type
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_work_type
  set modified_date = GetDate()
  from inserted I
  where work_order_work_type.work_type_id = I.work_type_id
end
GO

-- Add new WGL Call Center and WGLINSP client code:
if not exists (select * from call_center where cc_code = 'WGL')
begin
  insert into call_center (cc_code, cc_name, active, notes) 
  values ('WGL', 'Washington Gas Inspections', 1, 'Source of work order inspections received from WGL via the QMWGLService SOAP web service.')
end

if not exists (select * from client where oc_code = 'WGLINSP') 
begin
  insert into client (oc_code, client_name, active, call_center) 
  values ('WGLINSP', 'Washington Gas Inspections', 1, 'WGL')
end

-- New Configuration data settings
if not exists (select * from configuration_data where name = 'WGLServiceMaxMBIn') 
  insert into configuration_data (name, value, editable) values ('WGLServiceMaxMBIn', 50, 1)
if not exists (select * from configuration_data where name = 'WGLServiceMaxOrdersIn') 
  insert into configuration_data (name, value, editable) values ('WGLServiceMaxOrdersIn', 20, 1)
	
-- Add the list of selectable work remediation types
if exists (select * from work_order_work_type)
  delete from work_order_work_type
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('VC', 'Inadequate Vent Clearance Obstructions', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('VI', 'Inadequate Vent Clearance Ignition Sources', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('PM', 'Surface Corrosion', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('RM', 'Atmospheric Corrosion', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('SM', 'Improper MBU Alignment', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('MM', 'Improper Ground Clearance', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('MG', 'Inadequate MBU Protection', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('PSOS', 'Obstruction over MBU', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('HS', 'Insufficent Support of MBU', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('EPP', 'Exposed Plastic Pipe', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('RVL5', 'Riser Valve position less than 50% Closed', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('UBSV', 'Unplanned buried Service Valve', NULL, 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('RDM', 'Rapid Dial or Index Movement', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('RVM5', 'Riser Valve position more than 50% Closed', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('ILRVD', 'Improper Location of Regulator Vent Discharge', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('FF', 'Flooded Facilites', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('UBFR', 'Unplanned buried facilities regulator', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('PDB', 'Piping Damaged and/or Bent', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('IMOV', 'Inside Meter Missing/Restricted Outside Vent', 'RED', 1)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('AC', 'Atmospheric Corrosion', 'ORANGE', 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('EPPC', 'Exposed Plastic Pipe', 'ORANGE', 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('DP', 'Damage Piping', 'ORANGE', 0)
INSERT INTO work_order_work_type (work_type, work_description, flag_color, alert) VALUES ('US', 'Unsupported Piping', 'ORANGE', 0)

-- Add the list of allowed Meter Locations
if exists (select * from reference where type = 'metrloc')
  delete from reference where type = 'metrloc'
INSERT INTO reference (type, code, description) values ('metrloc', 'I', 'INSIDE')
INSERT INTO reference (type, code, description) values ('metrloc', 'O', 'OUTSIDE')
INSERT INTO reference (type, code, description) values ('metrloc', 'B', '')

-- Add the list of allowed Vent Clearance distances
if exists (select * from reference where type = 'ventclr')
  delete from reference where type = 'ventclr'
INSERT INTO reference (type, code, description) values ('ventclr', '1', 'Less than a foot')
INSERT INTO reference (type, code, description) values ('ventclr', '2', '> 1 foot and <= 2 feet')
INSERT INTO reference (type, code, description) values ('ventclr', '3', '> 2 feet and < 3 feet')
INSERT INTO reference (type, code, description) values ('ventclr', 'B', '')

-- Add the list of allowed Vent Ignition distances
if exists (select * from reference where type = 'ventign')
  delete from reference where type = 'ventign'
INSERT INTO reference (type, code, description) values ('ventign', '1', 'Less than a foot')
INSERT INTO reference (type, code, description) values ('ventign', '2', '> 1 foot and <= 2 feet')
INSERT INTO reference (type, code, description) values ('ventign', '3', '> 2 feet and < 3 feet')
INSERT INTO reference (type, code, description) values ('ventign', 'B', '')

-- Add the list of allowed Map Statuses
if exists (select * from reference where type = 'mapstat')
  delete from reference where type = 'mapstat'
INSERT INTO reference (type, code, description) values ('mapstat', 'CORRECT', 'Locateable Maps Correct')
INSERT INTO reference (type, code, description) values ('mapstat', 'INCORRECT', 'Locateable Maps Incorrect')
INSERT INTO reference (type, code, description) values ('mapstat', 'UNLOC', 'Unlocateable cannot verfiy maps')
INSERT INTO reference (type, code, description) values ('mapstat', 'B', '')

-- Add the list of allowed Building Types
if exists (select * from reference where type = 'bldtype')
  delete from reference where type = 'bldtype'
INSERT INTO reference (type, code, description) values ('bldtype', 'CHURCH', 'Church')
INSERT INTO reference (type, code, description) values ('bldtype', 'DAYCARE', 'Day Care Center')
INSERT INTO reference (type, code, description) values ('bldtype', 'APTGARDN', 'Garden Type Apartments')
INSERT INTO reference (type, code, description) values ('bldtype', 'GOVT', 'Government')
INSERT INTO reference (type, code, description) values ('bldtype', 'APTHIGH', 'High Rise Apartments')
INSERT INTO reference (type, code, description) values ('bldtype', 'HOSP', 'Hospital')
INSERT INTO reference (type, code, description) values ('bldtype', 'MULTI', 'Multifamily')
INSERT INTO reference (type, code, description) values ('bldtype', 'ELDER', 'Nursing Home/Elder Care')
INSERT INTO reference (type, code, description) values ('bldtype', 'OFFHIGH', 'Office - High Rise')
INSERT INTO reference (type, code, description) values ('bldtype', 'POPA', 'Places of Public Assembly')
INSERT INTO reference (type, code, description) values ('bldtype', 'REST', 'Restaurant')
INSERT INTO reference (type, code, description) values ('bldtype', 'SCHOOL', 'School')
INSERT INTO reference (type, code, description) values ('bldtype', 'SECSTRUC', 'Secondary Structure')
INSERT INTO reference (type, code, description) values ('bldtype', 'SINGFAM', 'Single Family Detached')
INSERT INTO reference (type, code, description) values ('bldtype', 'SMCOMM', 'Small Commercial')
INSERT INTO reference (type, code, description) values ('bldtype', 'TOWNHSE', 'Town Homes')
INSERT INTO reference (type, code, description) values ('bldtype', 'WRHSE', 'Warehousing')
INSERT INTO reference (type, code, description) values ('bldtype', 'B', '')

/* To rollback these changes:
-- drop the triggers
if exists(select name from sysobjects where name = 'work_order_work_type_iu' and type = 'TR')
  drop trigger work_order_work_type_iu
GO
if exists(select name from sysobjects where name = 'work_order_remedy_iu' and type = 'TR')
  drop trigger work_order_remedy_iu
GO
if exists(select name from sysobjects where name = 'work_order_inspection_iu' and type = 'TR')
  drop trigger work_order_inspection_iu
GO

-- drop the foreign keys
IF EXISTS (select * from sys.indexes where name = 'work_order_remedy_fk_wo_id')
 alter table work_order_remedy drop work_order_remedy_fk_wo_id
GO 
IF EXISTS (select * from sys.indexes where name = 'work_order_remedy_fk_work_type_id')
 alter table work_order_remedy drop work_order_remedy_fk_work_type_id
GO 
IF EXISTS (select * from sys.indexes where name = 'work_order_inspection_fk_wo_id')
 alter table work_order_inspection drop work_order_inspection_fk_wo_id
GO 

-- drop the new work_order_remedy table
if exists (select * from information_schema.tables where table_name = 'work_order_remedy')
  drop table work_order_remedy
GO

-- drop the new work_order_inspection table
if exists (select * from information_schema.tables where table_name = 'work_order_inspection')
  drop table work_order_inspection
GO

-- drop the new work_order_work_type table
if exists (select * from information_schema.tables where table_name = 'work_order_work_type')
  drop table work_order_work_type
GO
*/
