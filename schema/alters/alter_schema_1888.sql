/* Undo commands:
 update reference set modifier = null where type = 'utility' and active_ind = 1
*/

 update reference set modifier = case code 
   when 'catv' then 'CATV' 
   when 'elec' then 'ELECTRIC'
   when 'ugas' then 'GAS' 
   when 'pipl' then 'GAS'
   when 'phon' then 'PHONE' 
   when 'sewr' then 'SEWER'
   when 'watr' then 'WATER'
   when 'fuel' then 'OTHER'
   when 'sgnl' then 'OTHER'
   else null
 end
 where type = 'utility' and active_ind = 1
