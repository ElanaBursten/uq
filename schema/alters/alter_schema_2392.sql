/*
  2392: Auto-sync updates; 
        the updated rpt_employee_activity.sql stored proc should also be applied.
*/

-- New sync_log.sync_source column
if not exists (select * from INFORMATION_SCHEMA.columns where table_name = 'sync_log' and column_name = 'sync_source')
  alter table sync_log add sync_source varchar(10) null -- IDLE,TICKSAVE,TIMESAVE
go

-- This right was renamed after the initial version of the script, so rename it if is there:
if exists (select right_id from right_definition where entity_data = 'SyncAutoSyncIdle')
  update right_definition set entity_data = 'SyncAfterIdle' where entity_data = 'SyncAutoSyncIdle'

-- Add new SyncAfterIdle, SyncAfterTicketSave, SyncAfterTimeentrySave rights if they doesn't exist
if not exists (select right_id from right_definition where entity_data = 'SyncAfterIdle')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Sync - Auto-sync on Idle', 'General', 'SyncAfterIdle', 
    'Limitation is a comma separated list of minutes: idle delay, delay minimized, minimum time between syncs.')
if not exists (select right_id from right_definition where entity_data = 'SyncAfterTicketSave')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Sync - Auto-sync on Ticket Save', 'General', 'SyncAfterTicketSave',  'Limitation is a not used for this right.')
if not exists (select right_id from right_definition where entity_data = 'SyncAfterTimesheetSave')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Sync - Auto-sync on Timesheet Save', 'General', 'SyncAfterTimesheetSave',  'Limitation is a not used for this right.')

-- Reference rows for sync_source codes:
if not exists (select * from reference where type = 'syncsrc' and code = 'IDLE')
  insert into reference (type, code, description) values ('syncsrc', 'IDLE', 'Idle time exceeded')
if not exists (select * from reference where type = 'syncsrc' and code = 'TICKSAVE')
  insert into reference (type, code, description) values ('syncsrc', 'TICKSAVE', 'Ticket saved')
if not exists (select * from reference where type = 'syncsrc' and code = 'TIMESAVE')
  insert into reference (type, code, description) values ('syncsrc', 'TIMESAVE', 'Timesheet saved')

-- Auto-sync employee activity type
if not exists (select * from reference where type = 'empact' and code = 'SYNCA')
  insert into reference (type, code, description) values ('empact', 'SYNCA', 'Sync (automatic)')
