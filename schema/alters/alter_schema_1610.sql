alter table damage add 
  claim_coordinator_id int null
go

IF EXISTS (SELECT TABLE_NAME from INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'dycom_damage')
  DROP VIEW dbo.dycom_damage
GO

CREATE VIEW dycom_damage
AS
  select d.*, dp.short_period as fiscal_period,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id asc) as initial_estimate,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id desc) as current_estimate
  from damage d
  left join dycom_period dp on d.accrual_date >= dp.starting and accrual_date < dp.ending
GO

