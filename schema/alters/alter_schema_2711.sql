--remove old, deprecated sync SP's
if object_id('dbo.sync_4') is not null
  drop procedure dbo.sync_4
go
if object_id('dbo.sync_5') is not null
  drop procedure dbo.sync_5
go

--permit no-trigger user to update the damage table in order to prevent undesirable trigger behavior for this data migration
grant select, update on damage to no_trigger
go

--add new marks_within_tolerance varchar column; the site_marks_in_tolerance bit column is being deprecated and will eventually be removed
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME='marks_within_tolerance')
   alter table damage add marks_within_tolerance varchar(1) null DEFAULT NULL
go

--only need to perform if the old column exists
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME='site_marks_in_tolerance')
begin
  --if the old default constraint exists on the old column, remove it
  IF EXISTS(SELECT * FROM sysconstraints WHERE id=OBJECT_ID('damage') AND COL_NAME(id,colid)='site_marks_in_tolerance' AND OBJECTPROPERTY(constid, 'IsDefaultCnst')=1)    
  BEGIN
    declare @Command nvarchar(1000)
    select @Command = 'ALTER TABLE damage drop constraint ' + d.name
     from sys.tables t   
      join    sys.default_constraints d       
       on d.parent_object_id = t.object_id  
      join    sys.columns c      
       on c.object_id = t.object_id      
        and c.column_id = d.parent_column_id
     where t.name = N'Damage'
      and c.name = N'site_marks_in_tolerance'
    Execute (@Command)
  END
  
  --copy old column data into new column
  if exists (select damage_id, site_marks_in_tolerance, marks_within_tolerance from damage where site_marks_in_tolerance is not null and marks_within_tolerance is null)
  begin
    EXECUTE AS USER = 'no_trigger'
    update damage 
    set marks_within_tolerance = site_marks_in_tolerance 
    where site_marks_in_tolerance is not null and marks_within_tolerance is null  
    REVERT
  end
  
end
GO


-- Also apply the updated sync_6.sql
