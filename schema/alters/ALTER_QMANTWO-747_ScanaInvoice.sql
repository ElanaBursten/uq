USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_invoice7]    Script Date: 5/21/2019 2:16:09 PM ******/
 drop procedure [dbo].[RPT_invoice7]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- sp_helptext RPT_invoice7

Create PROCEDURE [dbo].[RPT_invoice7] (
  @OutputConfigID int,
  @BillID int
)
AS

Begin
--  declare               --test
--  @OutputConfigID int,    --test
--  @BillID int                --test  
--set @OutputConfigID = 7531       --test
--set @BillID = 62319               --test

set nocount on

-- The @Debug variable is currently used to debug the transmission billing
declare @Debug bit
set @Debug = 0

declare @StartDate datetime
declare @EndDate datetime
declare @WhichInvoice varchar(20)

-- Variables loaded from the billing output config record, so we only retrieve them once
declare @Template varChar(50)   --QMANTWO-747
declare @CustomerID int
declare @IncludeTrans bit -- Enable CA style transmissions
declare @IncludeBulk bit  -- Enable BULK1K transmissions
declare @TransTotal money
declare @TransPlatMap bit -- Map plat groups to a value group-defined code
declare @TransPlatMapPrefix varchar(20) -- Value group prefix to match plat group code
declare @OmitZeroAmount bit
declare @GroupByPrice bit
declare @SalesTaxIncluded bit
declare @FlatFee money

declare @LineItemTotal money
declare @AdjustmentPercentTotal money
declare @AdjustmentDollarTotal money
declare @TaxTotal money
declare @PeriodToPost varchar(10)
declare @MaxBreakdown varchar(60)

declare @customer_company_id int
declare @customer_pc varchar(15)
declare @customer_active bit
declare @customer_num varchar(20)

-- This is used as part of the Solomon task id for data with no associated locator
declare @no_locator_info varchar(12)
set @no_locator_info = Replicate('0', 12)

IF OBJECT_ID(N'tempdb..#Trans') IS NOT NULL
    DROP TABLE #Trans;

IF OBJECT_ID(N'tempdb..#Trans1') IS NOT NULL
    DROP TABLE #Trans1;


declare @termidlist table (
	client_id int not null,
	termid varchar(20) NOT NULL
)
declare @centerlist table (
	center varchar(20) NOT NULL
)
---- CA Transmission Billing: 'Trans' bill_code rates in the rate table
--declare @trans1 table ( -- The ticket_version (master) data for each transmission, there can be more than one per ticket
--  ticket_version_id int NOT NULL ,
--  ticket_id int NOT NULL ,
--  transmit_date datetime NOT NULL ,
--  ticket_format varchar(20) NULL ,
--  frag varchar(250) NULL ,
--  source varchar(20) NULL ,
--  work_county varchar(40) NULL ,
--  work_city varchar(40) NULL ,
--  revision varchar(20) NULL
--)

 create table #Trans1( -- The ticket_version (master) data for each transmission, there can be more than one per ticket
  ticket_version_id int NOT NULL ,
  ticket_id int NOT NULL ,
  transmit_date datetime NOT NULL ,
  ticket_format varchar(20) NULL ,
  frag varchar(250) NULL ,
  source varchar(20) NULL ,
  work_county varchar(40) NULL ,
  work_city varchar(40) NULL ,
  revision varchar(20) NULL
)

--declare @trans table ( -- The locate (detail) data for CA transmissions (not used by BULK1K billing)
--  tvid int NOT NULL,
--  tid int not null,
--  transmit_date datetime not null,
--  termcode varchar(30) not null,
--  cc varchar(30) not null,
--  fragment varchar(250),
--  source varchar(20) NULL,
--  locator_id int NULL,
--  locate_closed bit NULL,
--  plat varchar(20) NULL,
--  plat_group varchar(30) NULL,
--  locate_id int NULL,
--  rate money null,
--  line_item_text varchar(60) null,
--  bill_city varchar(40) NULL,
--  revision varchar(20) NULL,
--  primary key (tvid, termcode)
--)

create table #Trans ( -- The locate (detail) data for CA transmissions (not used by BULK1K billing)
  tvid int NOT NULL,
  tid int not null,
  transmit_date datetime not null,
  termcode varchar(30) not null,
  cc varchar(30) not null,
  fragment varchar(250),
  source varchar(20) NULL,
  locator_id int NULL,
  locate_closed bit NULL,
  plat varchar(20) NULL,
  plat_group varchar(30) NULL,
  locate_id int NULL,
  rate money null,
  line_item_text varchar(60) null,
  bill_city varchar(40) NULL,
  revision varchar(20) NULL
)

declare @transBulk table (  -- 'BULK1K' in the rate table
	cc varchar(30) not null,
	termcode varchar(30) not null,
	tid int not null,
	transmit_date datetime not null,
	locate_closed bit NULL
	primary key (termcode, tid)
)

  SELECT @Template=output_template    --QMANTWO-747
  FROM billing_output_config          --QMANTWO-747
  where Output_Config_ID = @OutputConfigID    --QMANTWO-747

select @StartDate=bill_start_date, @EndDate=bill_end_date
  from billing_header
  where bill_id = @BillID

select @IncludeTrans=transmission_billing, -- CA style transmissions
       @IncludeBulk = transmission_bulk,   -- BULK1K transmissions
       @TransPlatMapPrefix = plat_map_prefix,
       @WhichInvoice = which_invoice,
       @OmitZeroAmount = omit_zero_amount,
       @CustomerID = customer_id,
       @GroupByPrice = group_by_price,
       @SalesTaxIncluded = sales_tax_included,
       @FlatFee = flat_fee
from billing_output_config
where output_config_id=@OutputConfigID

set @TransPlatMap = (case when (@TransPlatMapPrefix is null) or (@TransPlatMapPrefix in ('', ' ')) then 0 else 1 end)

select @customer_company_id = (
  select c.locating_company
  from customer c
  where c.customer_id = @CustomerID)

select @customer_pc = (
  select pc.timesheet_num
  from customer c
    inner join profit_center pc on c.pc_code = pc.pc_code
  where c.customer_id = @CustomerID)

select @customer_active = c.active
from customer c
where c.customer_id = @CustomerID

select @customer_num = (
  select
    case when boc.customer_number is null or boc.customer_number='' then c.customer_number
    else boc.customer_number end
  from billing_output_config boc
    inner join customer c on boc.customer_id = c.customer_id
  where output_config_id=@OutputConfigID)

insert into @termidlist
select cli.client_id, cli.oc_code
  from client cli
where cli.customer_id = @CustomerID

-- Transmission billing only
if (@IncludeTrans = 1) or (@IncludeBulk = 1) begin
  insert into @centerlist
  select distinct cli.call_center
    from client cli
  where cli.customer_id = @CustomerID
end

declare @CustBillingDetail table (
  locate_id int not null,
  billing_cc varchar(10) not null,
  line_item_text varchar(60) null,
  rate money not null,
  price money not null,
  qty_marked integer null,
  qty_charged int null,
  hours DECIMAL(9,3) null,
  status varchar(5) not null,
  work_county varchar(40) null,
  work_state varchar(2) null,
  work_city varchar(40) null,
  locator_id int not null,
  tax_name varchar(40) null,
  tax_rate decimal(6,4) null,
  tax_amount money null,
  con_name varchar(50) null,
  bill_code varchar(10) null,
  ticket_type varchar(30) null
)

-- Pre-calculated billing_detail records for the specific customer and invoice name
insert into @CustBillingDetail
  (locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
   work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name, bill_code,
   ticket_type)
select
   locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
   work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name, bill_code,
   ticket_type
from billing_detail where bill_id = @BillID
  and client_id in (select client_id from @termidlist)
  and (coalesce(@WhichInvoice, '') = coalesce(which_invoice, ''))

if @IncludeTrans = 1 begin
  -- CA-style trans, used only in CA.
  -- Uses two steps (Trans1 and Trans) to get a feasible query plan
  -- Get the transmitted tickets:
  insert into #Trans1 
  select
   tv.ticket_version_id,
   tv.ticket_id,
   tv.transmit_date,
   tv.ticket_format,
   substring(tv.image, 1, 250) as frag,
   tv.source, t.work_county, t.work_city, tv.ticket_revision
    from ticket_version tv -- Note that we do not count manual tickets as "transmits"
      inner join @centerlist cl on cl.center = tv.ticket_format
      inner join ticket t on t.ticket_id = tv.ticket_id
    where tv.transmit_date between @StartDate and @EndDate

  if @Debug = 1
    select '#Trans1 ' as name, * from #Trans1 

  -- Tack on the locate details for those tickets:
  insert into #Trans (tvid, tid, transmit_date, termcode, cc, fragment, source,
    locator_id, locate_closed, plat, plat_group, locate_id, bill_city, revision)
  select
   ticket_version_id,
   tra.ticket_id,
   transmit_date,
   locate.client_code as termcode,
   max(ticket_format) as cc,
   max(frag) as frag,
   max(source) as source,
   locate.assigned_to_id,
   locate.closed,
   null, -- plat
   null, -- plat_group
   locate.locate_id,
   tra.work_city,
   tra.revision
  from #Trans1 tra
    inner join locate --with (index(locate_ticketid))
      on tra.ticket_id = locate.ticket_id
        and locate.client_code in (select termid from @termidlist)
        and locate.status <> '-N'
        and locate.added_by = 'PARSER'
  group by tra.ticket_version_id,
    tra.ticket_id,
    tra.transmit_date,
    locate.client_code,
    locate.assigned_to_id,
    locate.closed,
    locate.locate_id,
    tra.work_city,
    tra.revision

  update #Trans set plat =  -- Bill the transmit using the first/oldest locate plat
    (select top 1 plat from locate_plat lp where tr.locate_id = lp.locate_id order by insert_date asc) from #Trans tr

  if @TransPlatMap = 1 begin
    update #Trans set plat_group = (
      select group_code
      from value_group vg
        inner join value_group_detail vgd on vg.value_group_id = vgd.value_group_id
      where vgd.match_value = tr.plat
        and vg.group_code like @TransPlatMapPrefix+'%')
    from #Trans tr
  end

  if @Debug = 1
    select '#Trans_before' as name, * from #Trans

  -- GENERAL rule for non-COX, non-AT&T tickets, this does not do the right thing for AT&T nor Cox
  -- Ignore COX transmissions for non-COX clients, but don't delete AT&T (ATTD* and PTT*) and SDG transmissions that come direct from the customer:
  delete from #Trans where cc='SCA1' and (not fragment like '%UQSTSO%') and (termcode not in ('COX01', 'COX04')
    and (termcode not like 'PTT%') and (termcode not like 'ATTD%') and (termcode not like 'SDG%')
    and (termcode not in ('NCU01', 'NEU01', 'BCU01', 'CMU01', 'EAU01')))

  -- Ignore non-COX transmissions for COX clients:
  delete from #Trans where cc='SCA1' and termcode in ('COX01', 'COX04') and not fragment like '%COX0%USAS%'

  -- Specific rule for AT&T: AT&T trans only count if AT&T term in the upper left.
  delete from #Trans where cc='SCA1' and ((termcode like 'ATTD%') or (termcode like 'PTT%')) and not fragment like '%' + termcode + '%USAS%'

  -- Specific rule for SDG: The term code must appear in the the ticket header
  delete from #Trans where cc='SCA1' and (termcode like 'SDG%') and not fragment like '%' + termcode + '%USAS%'

  -- Specific rule for update tickets from SDG: Their term code must appear in the ticket header fragment
  delete from #Trans where cc='SCA1' and termcode in ('NCU01', 'NEU01', 'BCU01', 'CMU01', 'EAU01') and not fragment like '%SDG%USAS%'

  delete from #Trans where cc='SCA1' and termcode like '_____G'
  delete from #Trans where cc='NCA1' and not fragment like '%' + termcode + '%'

  if @Debug = 1
    select '#Trans_after' as name, * from #Trans 

  -- @lineitems and @soloitems are populated for these charges below
end

if @IncludeBulk = 1 begin
  -- Uses two steps (Trans1 and transBulk) to get a feasible query plan
  insert into #Trans1 (ticket_version_id, ticket_id, transmit_date, ticket_format)
  select
   ticket_version_id,
   ticket_id,
   transmit_date,
   ticket_format
  from ticket_version
      inner join @centerlist cl on cl.center = ticket_version.ticket_format
  where ticket_version.transmit_date between @StartDate and @EndDate

  insert into @transBulk
  select
   max(tra.ticket_format) as cc,
   locate.client_code as termcode,
   min(tra.ticket_id) as ticket_id,
   min(tra.transmit_date) as transmit_date,
   locate.closed
  from #Trans1 tra
    inner join ticket
      on ticket.ticket_id=tra.ticket_id
    inner join locate --with (index(locate_ticketid))
      on tra.ticket_id = locate.ticket_id
        and locate.client_code in (select termid from @termidlist)
        and locate.status <> '-N'
        and locate.added_by = 'PARSER'
  group by locate.client_code, ticket.ticket_number, locate.closed

end

-- Detail dataset (invoice lines for detail section)

declare @lineitems table (
	liid int identity,
	subcategory varchar(50),
	billing_cc varchar(35),
	line_item_text varchar(60),
	rate money,
	price money,
	Tickets int,
	Units int,
	UnitsCharged int,
	Hours decimal(38, 3),
	Amount money,
	omit_zero_amount bit,
	pre_tax_amount money,
	tax_name varchar(40),
	tax_rate decimal(6,4),
	tax_amount money,
	source varchar(10) null,
	plat_group varchar(35) null
)

declare @emppc table (
	emp_id int NOT NULL primary key,
	emp_payroll_pc_code varchar(15),
	emp_company_id int NULL
)

-- Solomon line items to export into the accounting system
declare @soloitems table (
	amount money,
	company_id int,
	pc varchar(15),
	source varchar(40), -- The source type or GL source specified by an adjustment record (New source items will need a matching ChargeType* constant)
	is_gl_code bit not null, -- Only used to debug the validity of the source later
	task_code varchar(55)
)

insert into @emppc (emp_id, emp_payroll_pc_code, emp_company_id)
select emp_id, coalesce(pc.timesheet_num, '000'), emp_company_id
 from dbo.employee_payroll_data2() e
 left join profit_center pc on e.emp_payroll_pc_code = pc.pc_code

if (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
begin
  -- IF we are NOT in Included Sales Tax mode, this will insert items:
  if (@FlatFee is null or @FlatFee <= 0)
    insert into @lineitems
      (subcategory, billing_cc, line_item_text, rate, price,
       Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems -- We are NOT in Included Sales Tax mode
  select
    coalesce(round(bd.price, 2),0) as Amount,
    ep.emp_company_id as company_id,
    ep.emp_payroll_pc_code as pc,
    dbo.get_billing_charge_code(bd.bill_code, bd.ticket_type) as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
   from @CustBillingDetail bd
     left join @emppc ep on bd.locator_id = ep.emp_id
end -- Sales tax not included

if (@SalesTaxIncluded = 1)
begin
  -- IF we ARE in Included Sales Tax mode, this will insert items:
  if (@FlatFee is null or @FlatFee <= 0)
    insert into @lineitems
      (subcategory, billing_cc, line_item_text, rate, price,
       Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount,
       pre_tax_amount, tax_name, tax_rate, tax_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount,
     pre_tax_amount, tax_name, tax_rate, tax_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems -- We ARE in Included Sales Tax mode
  select
    coalesce(round(price,2),0) - coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    dbo.get_billing_charge_code(bd.bill_code, bd.ticket_type) as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
   from @CustBillingDetail bd
   left join billing_scc_invoice scci
     on bd.work_state=scci.state and bd.work_county=scci.county
        and bd.work_city=scci.city and scci.active=1
        and (scci.customer_id=@CustomerID or scci.customer_id is null)
   left join @emppc ep on bd.locator_id = ep.emp_id

  insert into @soloitems
  select
    coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    'TAX' as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
  from @CustBillingDetail bd
   left join billing_scc_invoice scci
     on bd.work_state=scci.state and bd.work_county=scci.county
        and bd.work_city=scci.city and scci.active=1
        and (scci.customer_id=@CustomerID or scci.customer_id is null)
   left join @emppc ep on bd.locator_id = ep.emp_id
end -- Sales tax included

if @FlatFee > 0
begin
  -- Some customers are set to be charged flat fees per billing cycle
  -- (See billing_output_config.flat_fee) rather than per locate/transmission
  insert into @lineitems
    (subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems
  select
    @FlatFee as Amount,
    @customer_company_id as company_id,
    @customer_pc as pc,
    'FLAT_FEE' as source,
    0 as is_gl_code,
    @customer_pc + @no_locator_info as task_code
end


if @TransPlatMap = 1
begin
  -- For these customers, the bill is separated into invoices by plat group
  -- The plat group is entered in the city field and the which_invoice field
  -- in the rate table.  This is used by CA transmissions only (AT&T).
  update #Trans set bill_city = plat_group where plat_group is not null
end


if @IncludeTrans = 1
begin
  -- Collect the rates for normal transmission billing.  Only this
  -- transmission type supports city/county rates right now and only
  -- for city/county based rate rules.
  declare @TransRates table (
    br_id int not null,
    call_center varchar(20) NOT NULL,
    billing_cc varchar(10) NOT NULL,
    rate money NULL,
    bill_code varchar(10) NULL,
    work_county varchar(40) NULL,
    work_city varchar(40) NULL,
    which_invoice varchar(20) NULL,
    line_item_text varchar(60) NULL
  )

  -- Grab all potential Trans rates for all invoice names
  insert into @TransRates
  select br_id, call_center, billing_cc, rate, bill_code,
    work_county, work_city, which_invoice, line_item_text
  from billing_rate
    inner join @centerlist cl on cl.center = call_center
  where billing_cc in (select termid from @termidlist)
    and bill_code = 'Trans'  -- Here is where we ignore non-Trans rates/terms

  update @TransRates set work_county = null where work_county in ('', ' ')
  update @TransRates set work_city   = null where work_city   in ('', ' ')

  if @Debug = 1
    select '@TransRates' as name, * from @TransRates

  -- Map each unique center/term/county/city in this billing run to a trans rate
  declare @RateLookup table (
    br_id int null,
    call_center varchar(20) NOT NULL,
    billing_cc varchar(10) NOT NULL,
    work_county varchar(40) NULL,
    work_city varchar(40) NULL,
    line_item_text varchar(60) NULL,
    rate money NULL,
    which_invoice varchar(20)
  )

  insert into @RateLookup (call_center, billing_cc, work_county, work_city)
  select distinct
    tr1.ticket_format, tr.termcode, tr1.work_county, tr.bill_city
  from #Trans tr
    inner join #Trans1 tr1 on tr1.ticket_version_id =  tr.tvid

  if @Debug = 1
    select '@RateLookup_before' as name, * from @RateLookup

  -- Get any exact center + term + county + city matches
  update rl set br_id =
    (select br_id from @TransRates tr where (tr.call_center = rl.call_center) and (tr.billing_cc = rl.billing_cc)
     and (tr.work_county = rl.work_county) and (tr.work_city = rl.work_city))
  from @RateLookup rl
  where br_id is null

  update rl
  set
    rl.rate           = (select tr.rate           from @TransRates tr where tr.br_id = rl.br_id),
    rl.line_item_text = (select tr.line_item_text from @TransRates tr where tr.br_id = rl.br_id),
    rl.which_invoice  = (select tr.which_invoice  from @TransRates tr where tr.br_id = rl.br_id)
  from @RateLookup rl
  where rl.rate is null

  -- Get any center + term + city matches
  update rl set br_id =
    (select br_id from @TransRates tr where (tr.call_center = rl.call_center) and (tr.billing_cc = rl.billing_cc)
     and (tr.work_county is null) and (tr.work_city = rl.work_city))
  from @RateLookup rl
  where br_id is null

  update rl
  set
    rl.rate           = (select tr.rate           from @TransRates tr where tr.br_id = rl.br_id),
    rl.line_item_text = (select tr.line_item_text from @TransRates tr where tr.br_id = rl.br_id),
    rl.which_invoice  = (select tr.which_invoice  from @TransRates tr where tr.br_id = rl.br_id)
  from @RateLookup rl
  where rl.rate is null

  -- Get any center + term + county matches
  update rl set br_id =
    (select br_id from @TransRates tr where (tr.call_center = rl.call_center) and (tr.billing_cc = rl.billing_cc)
     and (tr.work_county = rl.work_county) and (tr.work_city is null))
  from @RateLookup rl
  where br_id is null

  update rl
  set
    rl.rate           = (select tr.rate           from @TransRates tr where tr.br_id = rl.br_id),
    rl.line_item_text = (select tr.line_item_text from @TransRates tr where tr.br_id = rl.br_id),
    rl.which_invoice  = (select tr.which_invoice  from @TransRates tr where tr.br_id = rl.br_id)
  from @RateLookup rl
  where rl.rate is null

  -- Get the fallback basic center + term only match
  update rl set br_id =
    (select br_id from @TransRates tr where (tr.call_center = rl.call_center) and (tr.billing_cc = rl.billing_cc)
     and (tr.work_county is null) and (tr.work_city is null))
  from @RateLookup rl
  where br_id is null

  update rl
  set
    rl.rate           = (select tr.rate           from @TransRates tr where tr.br_id = rl.br_id),
    rl.line_item_text = (select tr.line_item_text from @TransRates tr where tr.br_id = rl.br_id),
    rl.which_invoice  = (select tr.which_invoice  from @TransRates tr where tr.br_id = rl.br_id)
  from @RateLookup rl
  where rl.rate is null

  if @Debug = 1
    select '@RateLookup_before_delete' as name, * from @RateLookup

  -- Delete rows with no matching trans rate, since they shouldn't get charged trans fees
  delete from @RateLookup where rate is null

  -- Delete rows that should get their transmission charges from other named invoices
  delete from @RateLookup where coalesce(which_invoice, '') <> coalesce(@WhichInvoice, '')

  if @Debug = 1
    select '@RateLookup_after' as name, * from @RateLookup

  -- Apply the rate per transmission to #Trans , so we don't have to keep joining to @RateLookup
  update tr
  set rate = (
    select rate
    from @RateLookup br
    where br.call_center = tr.cc
	  and br.billing_cc = tr.termcode
	  and coalesce(br.work_city, '') = coalesce(tr.bill_city, '')
	  and coalesce(br.work_county, '') = coalesce(tr1.work_county,  '')
    ),
  line_item_text = (
    select line_item_text
    from @RateLookup br
    where br.call_center = tr.cc
	  and br.billing_cc = tr.termcode
	  and coalesce(br.work_city, '') = coalesce(tr.bill_city, '')
	  and coalesce(br.work_county, '') = coalesce(tr1.work_county,  '')
    )
  from #Trans tr
    inner join #Trans1 tr1 on tr1.ticket_version_id = tr.tvid

  -- Remove non-transmission rate locates from the list
  delete from #Trans where rate is null

  if @Debug = 1
    select '#Trans_after' as name, * from #Trans 

  -- CA-Style (Trans rates in the Rate table) TRANSMISSION line items
  insert into @lineitems
  (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units,
   UnitsCharged, Hours, Amount, omit_zero_amount, source, plat_group)
  select
    null as subcategory,
    tr.termcode as billing_cc,
    coalesce(max(tr.line_item_text), 'Transmissions') as line_item_text,
    tr.rate as rate,
    tr.rate as price,
    count(tr.tid) as Tickets,
    count(tr.tid) as Units,
    count(tr.tid) as UnitsCharged,
    0 as Hours,
    sum(tr.rate) as Amount,
    0 as omit_zero_amount,
    'trans' as source,
    tr.plat_group
  from #Trans tr
  group by
    tr.termcode,
    tr.plat_group,
    tr.rate

  insert into @soloitems
  select
    coalesce(tr.rate, 0) as Amount,
    @customer_company_id as company_id,
    coalesce(ep.emp_payroll_pc_code, @customer_pc) as pc,
    'TRANSMISSION' as source,
    0 as is_gl_code,
    coalesce(ep.emp_payroll_pc_code, '000') + @no_locator_info as task_code
  from #Trans tr
    left join @emppc ep on tr.locator_id = ep.emp_id
end -- CA #Trans support


if @IncludeBulk = 1
begin
  -- BULK-style (rates as BULK1K in the rate table) TRANSMISSION items
  insert into @lineitems
  (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount, source)
  select
    null as subcategory,
    tr.termcode as billing_cc,
    coalesce(max(br.line_item_text), 'Transmissions') as line_item_text,
    br.rate as rate,
    br.rate as price,
    count(tr.tid) as Tickets,
    round(count(tr.tid), -2) as Units,
    round(count(tr.tid), -2) as UnitsCharged,
    0 as Hours,
    round(br.rate * round(count(tr.tid), -2) / 1000,2) as amount, -- Round to the nearest 100 ticket #
    0 as omit_zero_amount,
    'B1KTrans'
    from @transBulk tr
      left join billing_rate br
        on br.call_center = tr.cc
         and br.billing_cc = tr.termcode
         and br.bill_type='BULK1K'
    group by
      tr.termcode,
      br.rate

  insert into @soloitems
  select
    round(br.rate * round(count(tr.tid), -2) / 1000,2) as amount,
    @customer_company_id as company_id,
    @customer_pc as pc,
    'BULK_TRANSMISSION' as source,
    0 as is_gl_code,
    @customer_pc + @no_locator_info as task_code
    from @transBulk tr
      left join billing_rate br
        on br.call_center = tr.cc
         and br.billing_cc = tr.termcode
         and br.bill_type='BULK1K'
    group by
      tr.termcode,
      br.rate
end

-- Delete 0-amount items, if configured to do so:
if @OmitZeroAmount = 1
begin
  delete from @lineitems
  where amount=0 or amount is null
end

-- Insert an empty line item, if there is nothing there, so the reports will work.
if not exists (select top 1 * from @lineitems)
insert into @lineitems
  (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
select
  null as subcategory,
  '' as billing_cc, '' as line_item_text,
  null as rate,
  null as price,
  null as Tickets, null as Units, null as UnitsCharged, null as Hours,
  0 as Amount,
  0 as omit_zero_amount


-- Output begins here --
if  @Template = 'UQ-ScanaByLineItem.rtm'  --QMANTWO-747
begin -- Summary of each locate category/description by line item text QMANTWO-747
	select * from @lineitems              --QMANTWO-747
	order by 4,2,3,5,6,7,8                --QMANTWO-747
end                                       --QMANTWO-747   
else
begin  -- Summary of each locate category/description by term
	select * from @lineitems
	order by 2,3,4,5,6,7,8
end

-- Get current invoice total for above data

select @LineItemTotal = sum(coalesce(amount,0))
from @lineitems

-- Adjustment Detail dataset
select description, type,
  case
    when type='AMOUNT' then sum(amount)
    when type='PERCENT' then sum(round(@LineItemTotal * (amount) / 100, 2))
    else 0 end as AdjustmentAmount -- COMMENT type adjustments with no amount
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')
 group by description, type

insert into @soloitems
select
  case
    when type='AMOUNT' then sum(amount)
    when type='PERCENT' then sum(round(@LineItemTotal * (amount) / 100, 2))
    else 0 end as AdjustmentAmount,
  @customer_company_id as company_id,
  @customer_pc as pc,
  ba.gl_code as source,
  1 as is_gl_code, -- A blank/null GL code means "charge to the otherwise largest $ amount GL code on this invoice"
  @customer_pc + @no_locator_info as task_code
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.active=1
   and ba.type <> 'COMMENT'
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')
 group by description, type, ba.gl_code

-- Get the total of all AMOUNT adjustments into the variable
-- Perhaps we could declare the detail data above as a cursor
-- and iterate over it, if needed later
select @AdjustmentDollarTotal = sum(amount)
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.type='AMOUNT'
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')

select @AdjustmentPercentTotal = sum(round(@LineItemTotal * (amount / 100), 2))
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.type='PERCENT'
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')

-- Sales Tax
-- Note that this only picks up traditional billing, not Trans or Flat.
select tax_name, tax_rate, sum(price) as amount, sum(tax_amount) as tax_amount
from @CustBillingDetail bd
where (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
group by tax_name, tax_rate
having sum(tax_amount)>0
order by tax_name, tax_rate

if (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
begin
  insert into @soloitems
  select
    coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    'TAX' as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
  from @CustBillingDetail bd
   left join @emppc ep on bd.locator_id = ep.emp_id

  select @TaxTotal = coalesce(sum(tax_amount), 0)
   from @CustBillingDetail bd
end
else begin -- Sales tax is included in rates
  select @TaxTotal = 0
end

-- Get the Solomon Period To Post
select @PeriodToPost = min(dp.period) from dycom_period dp where @EndDate between dp.starting and dp.ending


-- Total Invoice Amount is the amount of all prices and adjustments

declare @total money
set @total = coalesce(@LineItemTotal,0) +
    coalesce(@AdjustmentPercentTotal,0) +
    coalesce(@AdjustmentDollarTotal, 0)  +
    coalesce(@TaxTotal, 0)

-- Invoice header/master dataset (Invoice basics, Customer settings, etc.)
select
  boc.output_config_id,
  boc.customer_id,
  boc.output_template,
  boc.contract,
  boc.payment_terms,
  boc.cust_po_num,
  boc.output_fields,
  boc.billing_period,
  boc.flat_fee,
  boc.comment,
  boc.center_group_id,
  boc.SolomonOffice,
  boc.SolomonPeriodToPost,
  boc.which_invoice,
  boc.sales_tax_rate,
  boc.group_by_price,
  boc.omit_zero_amount,
  case when boc.customer_number is null or boc.customer_number='' then c.customer_number
      else boc.customer_number end as customer_number,
  boc.transmission_billing,
  boc.sales_tax_included,
  boc.transmission_bulk,
  bh.committed,
  bh.bill_id as billing_run_id,
  c.customer_id,
  c.customer_name,
  coalesce(c.customer_description, c.customer_name) as customer_description,
  c.city,
  c.state,
  c.zip,
  c.street,
  c.phone,
  c.locating_company,
  c.modified_date,
  c.active,
  c.street_2,
  c.attention,
  c.contact_name,
  c.contact_email,
  c.period_type,
  c.pc_code,
  lc.logo_filename, lc.billing_footer,
  case when attention is null then null
       else 'Attn: ' + attention end as attention_tagged,

  @LineItemTotal as total_line_items,
  @AdjustmentPercentTotal as total_adjustments_percent,
  @AdjustmentDollarTotal as total_adjustments_dollar,

  coalesce(@AdjustmentPercentTotal, 0) +
    coalesce(@AdjustmentDollarTotal, 0) as total_adjustments,

  coalesce(@LineItemTotal,0) +
    coalesce(@AdjustmentPercentTotal, 0) +
    coalesce(@AdjustmentDollarTotal, 0) as subtotal_with_adjustments,

  @TaxTotal as total_tax,

  -- Total Invoice Amount is the amount of all prices and adjustments
  @total as grand_total,

  pc.gl_code as ProjectIDHeader, pc.timesheet_num as SolOffice,
  @PeriodToPost as SolPeriodToPost,
  (select coalesce(sum(amount),0) from @soloitems) as sol_total,
  lc.name as locating_company_name,
  case
    -- mark header to ignore $0 invoices for inactive customers
    when (@total < 0.001 and @total > -0.001) and @customer_active=0 then 1
    else 0
  end as ignore_invoice,
  coalesce(pc.billing_contact, '') as billing_contact

from billing_output_config boc
  inner join customer c on boc.customer_id=c.customer_id
  inner join locating_company lc on c.locating_company=lc.company_id
  inner join center_group cg on boc.center_group_id=cg.center_group_id
  inner join billing_header bh on bh.bill_id = @BillID
  left outer join profit_center pc on c.pc_code=pc.pc_code
where boc.output_config_id=@OutputConfigID

-- Gray/Kiely contractor special case data for NewJersey (no longer used - just a placeholder for backward compatibility)
select '' as billing_cc, '' as con_name, 0 as Cnt, 0.0 as TotalPrice
where 0=1

-- Totals by Status.  Only used for a few customers.
select
  billing_cc, status,
  count(*) as Tickets,
  sum(qty_marked) as Units,
  sum(qty_charged) as UnitsCharged,
  sum(hours) as Hours,
  sum(price) as Amount
 from @CustBillingDetail bd
group by
  billing_cc, status

-- Percentage breakdowns.  Only used for a few customers.

if exists(select billing_breakdown_id from billing_breakdown where output_config_id = @OutputConfigID)
begin
  set @MaxBreakdown = (select max(explanation) 
    from billing_breakdown
    where output_config_id=@OutputConfigID)

  select br.explanation, br.ratio,
   case when br.explanation<@MaxBreakdown then round(br.ratio * @total, 2)
   else @total -
     (select sum(round(br.ratio * @total, 2))
       from billing_breakdown br
       where br.output_config_id=@OutputConfigID
        and br.explanation<@MaxBreakdown)
   end as amount,
   case when br.explanation<@MaxBreakdown then round(br.ratio * @FlatFee, 2)
   else @FlatFee - 
     (select sum(round(br.ratio * @FlatFee, 2))
       from billing_breakdown br
       where br.output_config_id=@OutputConfigID
        and br.explanation<@MaxBreakdown)
    end as flat_fee_amount
from billing_breakdown br --with (index(billing_breakdown_oc))
where br.output_config_id=@OutputConfigID
 order by br.explanation
end
else
begin -- No breakdowns defined
  select null as explanation, null as ratio, null as amount, null as flat_fee_amount where 0=1
end

-- Billing Detail datasets: Transmission

if @IncludeTrans = 1
begin
  select -- CA Trans
   tr.termcode,
   ticket.ticket_id,
   ticket.ticket_format as call_center,
   ticket.ticket_number,
   ticket.ticket_type,
   ticket.con_name,
   ticket.work_city,
   tr.transmit_date as transmit_date,
   ticket.work_county,
   ticket.work_address_number,
   ticket.work_address_number_2,
   ticket.work_address_street,
   ticket.work_cross,
   ticket.map_page,
   tr.rate,
   tr.locate_closed,
   tr.plat,
   tr.revision
  from #Trans tr
    left join ticket with (index(PK_ticket_ticketid))
      on ticket.ticket_id = tr.tid
end
else if @IncludeBulk = 1
begin
  select -- BULK1K
   tr.termcode,
   ticket.ticket_id,
   ticket.ticket_format as call_center,
   ticket.ticket_number,
   ticket.ticket_type,
   ticket.con_name,
   ticket.work_city,
   tr.transmit_date as transmit_date,
   ticket.work_county,
   ticket.work_address_number,
   ticket.work_address_number_2,
   ticket.work_address_street,
   ticket.work_cross,
   ticket.map_page,
   br.rate,
   tr.locate_closed,
   null as plat , -- plat not used for these
   null as revision -- revision not used here
  from @transBulk tr
    inner join ticket with (index(PK_ticket_ticketid))
      on ticket.ticket_id = tr.tid
    left join billing_rate br
      on br.call_center = tr.cc
       and br.billing_cc = tr.termcode
       and br.bill_type='BULK1K'
  order by 1,2,3,4,5
end
else begin
  select -- No trans billing option enabled
    null as termcode,
    null as ticket_id,
    null as call_center,
    null as ticket_number,
    null as ticket_type,
    null as con_name,
    null as work_city,
    null as transmit_date,
    null as work_county,
    null as work_address_number,
    null as work_address_number_2,
    null as work_address_street,
    null as work_cross,
    null as map_page,
    null as rate,
    null as locate_closed,
    null as plat,
    null as revision
  where 0=1
end

-- Solomon Data

delete from @soloitems where amount = 0 or amount is null
update @soloitems set company_id=@customer_company_id, pc=@customer_pc,
  task_code=@customer_pc + @no_locator_info
  where company_id <> @customer_company_id
    or pc is null

select 'Level4' as LevelMarker,
  @customer_num as Contract,
  task_code as TaskID,
  s.pc as SolomonOffice,
  sum(s.amount) as Amount,
  s.pc,
  s.source,
  s.is_gl_code as IsGLCode
 from @soloitems s
 group by s.pc, task_code, s.source, s.is_gl_code
 having sum(s.amount)>0 or @customer_active=1

-- For debugging only.  This is not directly used by the invoice export code:
if @Debug = 1
  select * from @soloitems

  drop table #Trans
  drop table #Trans1

  End

