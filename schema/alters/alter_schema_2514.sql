-- Add new Ticket Release for work right if it does not exist
if not exists (select right_id from right_definition where entity_data = 'TicketsReleaseForWork')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Tickets - Release for Work', 'General', 'TicketsReleaseForWork', 
    'Limitation does not apply to this right.') 

-- Add reference information
if not exists (select * from reference where type='tkpriority' and code='Release')
  insert into reference (type, code, description, sortby, active_ind)
  values ('tkpriority','Release','Release for work', 8, 1)
  
go
