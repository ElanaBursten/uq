-- Add Contract Review Due Date to Customer Table
-- There is no logic for QManager associated with this change. 
--     At this timeQManager Admin will ONLY show the date (greyed out)

IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'customer' and COLUMN_NAME='contract_review_due_date')
Alter table customer
add contract_review_due_date date null