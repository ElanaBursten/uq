--Add new right that allows user to search Work Orders
if not exists (select * from right_definition where entity_data = 'SearchWorkOrders')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('SearchWorkOrders', 'Work Orders - Search', 'General', 'Limitation is a comma-separated list of call centers the user may search.')
go

if not exists (select * from reference where type = 'empact' and code = 'WOFND')
  insert into reference (type, code, description) values ('empact', 'WOFND', 'Find Work Order')
go