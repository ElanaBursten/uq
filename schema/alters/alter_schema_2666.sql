--If any users have the Damages - Set Responsibility right, delete the reference
if exists (select * from employee_right where right_id = (select right_id from right_definition where entity_data = 'DamagesSetResponsibility'))
  delete from employee_right where right_id = (select right_id from right_definition where entity_data = 'DamagesSetResponsibility')
go

--Remove the Damages - Set Responsibility right
if exists(select * from right_definition where entity_data = 'DamagesSetResponsibility')
  delete from right_definition where entity_data = 'DamagesSetResponsibility'
go
  
