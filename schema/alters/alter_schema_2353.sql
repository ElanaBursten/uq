-- New table to track employee tasks. Currently used to track first
-- ticket to work on for a given date.

/* ============================================================ */
/*   Table: task_schedule - used to mark first work of the day  */
/* ============================================================ */
if not exists (select * from information_schema.tables where table_name = 'task_schedule')
  create table task_schedule (
    task_schedule_id  integer   not null identity(14000, 1),
    emp_id            integer   not null, -- employee with a task
    ticket_id         integer   not null, -- ticket for the task
    est_start_date    datetime  not null, -- estimated start time
    added_date        datetime  not null, -- local time the task was added
    added_by          integer   not null, -- employee adding the task
    active            bit       not null, -- active indicator
    modified_date     datetime  not null default GetDate(),
    constraint pk_task_schedule primary key (task_schedule_id)
  )
go

if not exists (select * from sys.indexes where name = 'task_schedule_ticket_id') 
  create index task_schedule_ticket_id on task_schedule (ticket_id, est_start_date)
if not exists (select * from sys.indexes where name = 'task_schedule_est_start_date') 
  create index task_schedule_est_start_date on task_schedule (emp_id, est_start_date)
if not exists (select * from sys.indexes where name = 'task_schedule_added_date') 
  create index task_schedule_added_date on task_schedule (emp_id, added_date)
go

if not exists (select constraint_name from information_schema.table_constraints 
 where constraint_name = 'task_schedule_fk_ticket' and table_name = 'task_schedule' and constraint_type = 'foreign key') 
  alter table task_schedule add constraint task_schedule_fk_ticket
    foreign key (ticket_id) references ticket (ticket_id)

if not exists (select constraint_name from information_schema.table_constraints 
 where constraint_name = 'task_schedule_fk_employee' and table_name = 'task_schedule' and constraint_type = 'foreign key') 
  alter table task_schedule add constraint task_schedule_fk_employee
    foreign key (emp_id) references employee (emp_id)

if not exists (select constraint_name from information_schema.table_constraints 
 where constraint_name = 'task_schedule_fk_added_by' and table_name = 'task_schedule' and constraint_type = 'foreign key') 
  alter table task_schedule add constraint task_schedule_fk_added_by
    foreign key (added_by) references employee (emp_id)
go

-- Add new TKSCHED employee activity type reference code
if not exists (select ref_id from reference where type='empact' and code='TKSCHED') 
  insert into reference (type, code, description)
    values ('empact', 'TKSCHED', 'Set Est. Start Time')

-- Add new FIRSTTASK employee activity type reference code
if not exists (select ref_id from reference where type='empact' and code='FIRSTTASK') 
  insert into reference (type, code, description)
    values ('empact', 'FIRSTTASK', 'First Task')

-- Also need to deploy the new sync_4.sql & rpt_employee_activity.sql scripts.


