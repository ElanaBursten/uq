if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'client' and COLUMN_NAME = 'attachment_url')
  alter table client add attachment_url varchar(200) null
go

