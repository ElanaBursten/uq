-- Add allowed_work to call_center table
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'call_center' and COLUMN_NAME='allowed_work')
    ALTER TABLE call_center
      ADD allowed_work varchar(10) null;
     
GO

/* 

Run the update queries below to add the data for allowed_work.  It will cause the
data to syncd to client


update call_center set allowed_work = 'ticket' 
where ((cc_code <> 'WGL') and (cc_code <> 'LAM01')) and (allowed_work is null)

update call_center set allowed_work = 'work_order'
where ((cc_code = 'WGL') or (cc_code = 'LAM01')) and (allowed_work is null)

*/
