USE [QM]
GO

/****** Object:  UserDefinedFunction [dbo].[get_report_hier5]    Script Date: 9/18/2018 4:01:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE function [dbo].[get_report_hier5](@id int, @managers_only bit)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_short_name_tagged varchar(45) NULL, -- Not in get_hier
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_adp_login varchar(8) NULL,
  h_ad_username varchar(20) NULL,
  h_type_id integer,
  h_active bit, -- Not in get_hier
  h_report_level integer,
  h_report_to integer,
  h_report_to_short_name varchar(30) NULL,  -- Everything below is not in get_hier
  h_namepath varchar(450) NULL,
  h_idpath varchar(160) NULL,
  h_1 varchar(30) NULL,
  h_2 varchar(30) NULL,
  h_3 varchar(30) NULL,
  h_4 varchar(30) NULL,
  h_5 varchar(30) NULL,
  h_6 varchar(30) NULL,
  h_7 varchar(30) NULL,
  h_8 varchar(30) NULL,
  h_9 varchar(30) NULL
)
as
begin
  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
     h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
     h_namepath, h_idpath, h_1)
  select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
     e.type_id, e.active, e.report_to, 1, man.short_name,
     e.short_name, convert(varchar(20), e.emp_id),
     e.short_name
    from employee e
    left join employee man on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user; hideously ugly duplication,
  -- but I'd need to use code gen to get rid of it.

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=1
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=2
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=3
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4, h_5)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=4
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4, h_5, h_6)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=5
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=6
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=7
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_ad_username, h_adp_login,
          h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number, e.ad_username, e.adp_login, 
        e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id and r.h_report_level=8
       left join reference et on e.type_id = et.ref_id
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

  update @results
   set h_short_name_tagged = 
    case when h_active=1 then h_short_name
                         else h_short_name + ' (inactive)'
    end

  return
end



GO


