--QMANTWO-733 Adding Per_diem to timesheet_entry 3/2019

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'timesheet_entry' and COLUMN_NAME='per_diem')
alter table timesheet_entry
add per_diem bit NULL default 0

-- select * from timesheet_entry