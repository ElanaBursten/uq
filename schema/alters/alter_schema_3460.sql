/*
-- The event_log table needs to be recreated only if it was added prior to deploying QMAN-3460 for the first time.
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'event_log') 
  drop table event_log
GO
*/

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'event_log') begin
  CREATE TABLE event_log (
    event_id uniqueidentifier NOT NULL DEFAULT NewSequentialID(),
    aggregate_type integer NOT NULL,      -- 1 = Ticket, 2 = Damage, 3 = Work Order
    aggregate_id integer NOT NULL, 
    version_num integer NOT NULL,
    event_data varchar(5000) NOT NULL,    -- the event object
    event_added datetime NOT NULL DEFAULT GetDate(),
    constraint pk_event_log primary key nonclustered (event_id)
  )

  CREATE UNIQUE CLUSTERED INDEX event_log_aggregate_idx ON event_log(aggregate_type, aggregate_id, version_num)
end
GO

-- Setup MSSQL Service Broker to handle new event notifications
-- Create Message Type
if not exists (select * from sys.service_message_types where name = 'NewEventMessage') 
  CREATE MESSAGE TYPE NewEventMessage VALIDATION = NONE

-- Create Contract
if not exists (select * from sys.service_contracts where name = 'NewEventContract')
  CREATE CONTRACT NewEventContract (NewEventMessage SENT BY INITIATOR)

-- Create Send Queue
if not exists (select * from sys.service_queues where name = 'NewEventSendQueue')
  CREATE QUEUE NewEventSendQueue

-- Create Receive Queue
if not exists (select * from sys.service_queues where name = 'NewEventReceiveQueue')
  CREATE QUEUE NewEventReceiveQueue WITH STATUS = ON,
    POISON_MESSAGE_HANDLING(STATUS = OFF)

-- Create Send Service on Send Queue
if not exists (select * from sys.services where name = 'NewEventSendService')
  CREATE SERVICE NewEventSendService ON QUEUE NewEventSendQueue (NewEventContract)

-- Create Receive Service on Receive Queue
if not exists (select * from sys.services where name = 'NewEventReceiveService')
  CREATE SERVICE NewEventReceiveService ON QUEUE NewEventReceiveQueue (NewEventContract)

GO

/* To rollback these changes:

-- Drop service broker config 
if exists (select * from sys.services where name = 'NewEventSendService')
  DROP SERVICE NewEventSendService
go
if exists (select * from sys.services where name = 'NewEventReceiveService')
  DROP SERVICE NewEventReceiveService
go
if exists (select * from sys.service_queues where name = 'NewEventSendQueue')
  DROP QUEUE NewEventSendQueue
go
if exists (select * from sys.service_queues where name = 'NewEventReceiveQueue')
  DROP QUEUE NewEventReceiveQueue
go
if exists (select * from sys.service_contracts where name = 'NewEventContract')
  DROP CONTRACT NewEventContract
go
if exists (select * from sys.service_message_types where name = 'NewEventMessage') 
  DROP MESSAGE TYPE NewEventMessage
go

-- Drop the event_log table
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'event_log') 
  drop table event_log
go
*/
