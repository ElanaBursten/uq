if not exists (select column_name from information_schema.columns
  where table_name = 'ticket' and column_name='geocode_precision')
  alter table ticket 
    add geocode_precision int
go
