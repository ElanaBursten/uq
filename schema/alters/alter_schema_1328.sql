-- New field for damage_bill
if (select count(*) from information_schema.columns 
  where table_schema='dbo' and table_name='damage_bill' and column_name='invoiced') = 0 begin
  alter table damage_bill
    add invoiced bit not null default 0
end
go

-- (Re)create RPT_billable_damages sp
if object_id('dbo.RPT_billable_damages') is not null
  drop procedure dbo.RPT_billable_damages
go

create procedure dbo.RPT_billable_damages (
  @BillingStartDate datetime, -- Beginning range of dates the damage was billed
  @BillingEndDate   datetime, -- Ending range of dates the damage was billed
  @ProfitCenter  varchar(10) -- Profit center filter
)
as
  set nocount on

  -- Results table
  select db.*, 
    e.short_name as billed_by,
    d.uq_damage_id,
    d.damage_date,
    d.utility_co_damaged,
    d.ticket_id,
    d.client_id,
    IsNull(t.ticket_number, '-') as ticket_number,
    IsNull(c.call_center, '-') as call_center,
    IsNull(c.oc_code, '-') as oc_code,
    IsNull(br.rate, 0.00) as billing_rate
  from damage_bill db
    inner join damage d on d.damage_id = db.damage_id
    inner join employee e on e.emp_id = db.modified_by
    left join ticket t on t.ticket_id = d.ticket_id
    left join client c on c.client_id = d.client_id
    left join billing_rate br on (br.call_center = c.call_center 
      and br.billing_cc = c.oc_code and br.bill_type = 'DMGCHARGE')
  where (billing_period_date >= @BillingStartDate 
    and billing_period_date < @BillingEndDate)
    and (@ProfitCenter = '' or d.profit_center = @ProfitCenter)
    and (billable = 1)
  order by call_center, db.billing_period_date

go

grant execute on dbo.RPT_billable_damages to uqweb, QManagerRole

/*
exec dbo.RPT_billable_damages '2006-08-01', '2006-08-31', ''
*/
