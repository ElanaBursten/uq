-- alter_schema_1715.sql
alter table right_definition add 
  right_type varchar(10) NULL,
  modified_date datetime not null default getdate()
go

create trigger right_definition_u on right_definition
for update
AS
  update right_definition
   set modified_date = getdate()
   where right_id in (select right_id from inserted)
go

create unique index right_definition_entity_data on right_definition(entity_data)
