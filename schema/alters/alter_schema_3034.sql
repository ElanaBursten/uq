
-- Drop index no longer in use
if exists (select * from dbo.sysindexes where name = N'billing_special_tickets_call_center_term_group' and id = object_id(N'[dbo].[billing_special_tickets]'))
  drop index billing_special_tickets.billing_special_tickets_call_center_term_group
GO

-- Drop tables no longer in use
if exists(select table_name from information_schema.TABLES where TABLE_NAME='tbl_PAYROLL_IMPORT')
  drop table tbl_PAYROLL_IMPORT
GO
if exists(select table_name from information_schema.TABLES where TABLE_NAME='ticket_marker')
  drop table ticket_marker  
GO
if exists(select table_name from information_schema.TABLES where TABLE_NAME='version')
  drop table version  
GO
if exists(select table_name from information_schema.TABLES where TABLE_NAME='billing_special_tickets')
  drop table billing_special_tickets 
GO
    
-- Drop stored procedures no longer in use
if object_id('dbo.PAYROLL_IMPORT2') is not null
	drop procedure dbo.PAYROLL_IMPORT2
GO 
if object_id('dbo.set_recv_manager_id') is not null
	drop procedure dbo.set_recv_manager_id
GO 

/* Add primary key to users_client table */
if not exists(select * from sys.key_constraints where name = 'PK_users_client')
  alter table users_client
    add constraint PK_users_client
    primary key nonclustered (uid, client_id)
go


