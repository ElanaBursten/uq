/* Undo commands:
 
 -- NOTE: Save a backup copy of your current sync_3, get_ticket, & 
 --       get_ticket_history sps in case you need to undo.

 delete from right_definition where entity_data='TicketsEditFacilities'
 alter table locate_facility 
   drop locate_facility_fk_locate
 alter table locate_facility 
   drop locate_facility_added_by_fk_employee
 alter table locate_facility 
   drop locate_facility_modified_by_fk_employee
 alter table locate_facility 
   drop locate_facility_deleted_by_fk_employee
 drop table locate_facility
 go
*/

create table locate_facility (
  locate_facility_id integer identity not null primary key nonclustered,
  locate_id integer not null,
  facility_type varchar(15) null,     -- a reference.code of type='factype'
  facility_material varchar(15) null, -- a reference.code of type='facmtrl'
  facility_size varchar(15) null,     -- a reference.code of type='facsize'
  facility_pressure varchar(15) null, -- a reference.code of type='facpres'
  offset integer null,
  added_by integer not null,
  added_date datetime not null,
  deleted_by integer null,
  deleted_date datetime null,
  modified_by integer null,
  modified_date datetime not null default GetDate(), -- server date
  active bit not null default 1
  )
go

create index locate_facility_locate on locate_facility(locate_id)

alter table locate_facility
  add constraint locate_facility_fk_locate foreign key (locate_id)
  references locate (locate_id)
alter table locate_facility
  add constraint locate_facility_added_by_fk_employee foreign key (added_by)
  references employee (emp_id)
alter table locate_facility
  add constraint locate_facility_deleted_by_fk_employee foreign key (deleted_by)
  references employee (emp_id)
alter table locate_facility
  add constraint locate_facility_modified_by_fk_employee foreign key (modified_by)
  references employee (emp_id)
go

-- new locate_facility update trigger
create trigger locate_facility_u on locate_facility
for update
as
  update locate_facility set modified_date = GetDate() where locate_facility_id in (select locate_facility_id from inserted)
go

insert into right_definition (right_description, right_type, entity_data) 
  values ('Tickets - Edit Facilities', 'General', 'TicketsEditFacilities')
