alter table damage
  add site_marked_in_white varchar(1) null,    -- was area marked in white
  site_tracer_wire_intact varchar(1) null,     -- was tracer wire intact
  site_pot_holed varchar(1) null,              -- was facility pot holed
  site_nearest_fac_measure decimal(9,3) null,  -- feet to nearest facility
  excavator_doing_repairs varchar(20) null,    -- excavator doing the repairs
  offset_visible varchar(1) null,              -- were offsets visible
  offset_qty decimal(9,3) null                 -- offset quantity

go

IF EXISTS (SELECT TABLE_NAME from INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'dycom_damage')
  DROP VIEW dbo.dycom_damage
GO

CREATE VIEW dycom_damage
AS
  select d.*, dp.short_period as fiscal_period,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id asc) as initial_estimate,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id desc) as current_estimate
  from damage d
  left join dycom_period dp on d.accrual_date >= dp.starting and accrual_date < dp.ending
GO
