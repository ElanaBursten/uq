-- add new reference code for jobsite arrival activity
if not exists (select ref_id from reference where type = 'empact' and code = 'ARRIVAL')
  insert into reference (type, code, description) values ('empact', 'ARRIVAL', 'Jobsite Arrival')

-- add new index on emp & arrival date to jobsite_arrival
if not exists (select * from sys.indexes where name = 'jobsite_arrival_arrive_emp_id') 
  create index jobsite_arrival_arrive_emp_id on jobsite_arrival(emp_id, arrival_date)

