-- Reconfigure billing_adjustment indexes for better billing performance:
create index billing_adjustment_customer_id_adjustment_date on 
billing_adjustment(customer_id, adjustment_date)
go

drop index billing_adjustment.billing_adjustment_customer_id
go
