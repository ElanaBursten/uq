if not exists (select * from information_schema.tables where table_name='summary_fragment')
  create table summary_fragment (
    summary_fragment_id int identity not null primary key,
    call_center varchar(20) not null,
    client_code varchar(10) not null,
    parse_date datetime not null,
    parse_error bit default 0 not null,
    image text not null
  )
go
