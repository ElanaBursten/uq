/* Undo commands:
 delete from right_definition where entity_data='TimesheetsEmpTypeEntry'

 alter table timesheet_entry
   drop timesheet_entry_fk_reference
 go
 alter table timesheet_entry
   drop column emp_type_id 
 go
*/

alter table timesheet_entry
  add emp_type_id int null
go

alter table timesheet_entry
  add constraint timesheet_entry_fk_reference foreign key (emp_type_id)
  references reference (ref_id)
go

insert into right_definition (right_description, right_type, entity_data) 
  values ('Timesheets - Employee Type Entry', 'General', 'TimesheetsEmpTypeEntry')
