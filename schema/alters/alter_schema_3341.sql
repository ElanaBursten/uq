if not exists (select code from reference where type = 'empact' and code = 'WOPAST')
  insert into reference(type, code, description, active_ind) values ('empact', 'WOPAST', 'Work Order Plat Addin Start', 1)
  
if not exists (select code from reference where type = 'empact' and code = 'WOPAEN')
  insert into reference(type, code, description, active_ind) values ('empact', 'WOPAEN', 'Work Order Plat Addin End', 1)