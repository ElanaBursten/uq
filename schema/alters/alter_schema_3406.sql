IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='invoiced')
  ALTER TABLE work_order_inspection 
    ADD invoiced bit not null default 0;
go 