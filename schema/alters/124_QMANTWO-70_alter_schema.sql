--Added Call Centers that can use the 'Ack' Status acknowledging from the Emergency bucket
--Check values and Call Centers to make sure that they are correct
-----------------------------------------------------------------------
Select * from center_group cg, center_group_detail cgd 
where cg.group_code = 'CallCentersThatAck'
and cg.center_group_id = cgd.center_group_id

-----------------------------------------------------------------------
IF NOT EXISTS(select * from center_group where group_code = 'CallCentersThatAck')
	 INSERT INTO center_group
           (group_code
           ,comment
           ,show_for_billing
           ,modified_date
           ,active)
     VALUES
           ('CallCentersThatAck'
           ,'Centers to ACK Emergencies on Automatically'
           ,0
           ,GetDate()
           ,1)



--Individual Call Centers that will change status to 'Ack' on Emergency Acknowledge:

DECLARE @AckGroup int;
SET @AckGroup = (select center_group_id from center_group where group_code = 'CallCentersThatAck')

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FMB1'
           ,GetDate()
           ,1)

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FMW1'
           ,GetDate()
           ,1)

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FDE1'
           ,GetDate()
           ,1)

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FMW5'
           ,GetDate()
           ,1)

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FMB3'
           ,GetDate()
           ,1)

INSERT INTO center_group_detail
           (center_group_id
           ,call_center
           ,modified_date
           ,active)
     VALUES
           (@AckGroup
           ,'FCO1'
           ,GetDate()
           ,1)
GO

----------------------------------------------------------------------------------------------
--****  REMOVE the center_group and center_group_detail entries  *****

--DECLARE @AckGroup int;
--SET @AckGroup = (select center_group_id from center_group where group_code = 'CallCentersThatAck')

--DELETE FROM center_group_detail
--WHERE center_group_id = @AckGroup
--GO

--DELETE FROM center_group
--where group_code = 'CallCentersThatAck'