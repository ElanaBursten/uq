IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'damage_due_date_rule')
BEGIN
CREATE TABLE damage_due_date_rule (
  rule_id  INT IDENTITY(1, 1) NOT NULL PRIMARY KEY, 
  pc_code  VARCHAR(15) NOT NULL,
  utility_co_damaged VARCHAR(40) NOT NULL, 
  calc_method varchar(30) NOT NULL, --todo SiZE?  Possible values? i.e. Business Day, etc
  days INTEGER, --number of days until due
  modified_date DATETIME NOT NULL DEFAULT GetDate(),
  active BIT DEFAULT 1
)
END

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE 
WHERE CONSTRAINT_NAME ='damage_due_date_rule_unique_constr')
BEGIN
  ALTER TABLE damage_due_date_rule
   ADD CONSTRAINT damage_due_date_rule_unique_constr
   UNIQUE NONCLUSTERED (pc_code, utility_co_damaged, active)
END

IF NOT EXISTS(SELECT * FROM reference WHERE type = 'dddrule' AND code = 'Default')
BEGIN
  INSERT INTO reference (type, code, description, active_ind) VALUES ('dddrule', 'Default', 'Default Method', 1)
END

IF NOT EXISTS(SELECT * FROM damage_due_date_rule WHERE pc_code = '*' AND utility_co_damaged = '*')
BEGIN
  INSERT INTO damage_due_date_rule (pc_code, calc_method, active, days, utility_co_damaged) VALUES('*', 'Default', 1, 5, '*')
END
GO