/* Cleanup invalid data so foreign keys can be applied. Run this script before running alter_schema_3044.sql */

-- Move orphaned ticket_version rows to a new table, ticket_version_OrphansNeedReview, when the matching ticket.ticket_id is missing; to keep them for any additional research UQ may want to do
if not exists (select * from INFORMATION_SCHEMA.TABLES where table_name = 'ticket_version_OrphansNeedReview') 
begin
  CREATE TABLE ticket_version_OrphansNeedReview (
    ticket_version_id int NOT NULL PRIMARY KEY,
    ticket_id integer NOT NULL,
    ticket_revision varchar(20) NULL,
    ticket_number varchar(20) NOT NULL,
    ticket_type varchar(38) NULL,
    transmit_date datetime NOT NULL,
    processed_date datetime NOT NULL,
    arrival_date datetime NULL,
    serial_number varchar(40) NULL,
    image text NULL,
    filename varchar(100) NULL,
    ticket_format varchar(20) NULL,
    source varchar(12) NULL
  )
end
go

-- find and cache the orphan ticket_version row ids
select ticket_version_id into #Orphan from ticket_version 
where ticket_id not in (select ticket_id from ticket)
-- 25 seconds
-- copy the orphans to ticket_version_OrphansNeedReview
insert ticket_version_OrphansNeedReview 
  select tv.* from ticket_version tv 
  inner join #Orphan o on tv.ticket_version_id = o.ticket_version_id
-- 80 seconds
-- delete the orphans from ticket_version
delete from ticket_version where ticket_version_id in (select ticket_version_id from #Orphan)
-- 45 seconds
-- drop temp cache table
drop table #Orphan
go

-- These commented out queries can be run to review the bad call center references.
/*
select * from client where update_call_center not in (select cc_code from call_center)
select * from client where call_center not in (select cc_code from call_center)
select * from billing_rate where call_center not in (select cc_code from call_center)
*/

-- Some invalid rows were found in the UQ test DB where the update_call_center was set to empty string instead of null
update client set update_call_center = NULL where update_call_center = ''

-- Some invalid call_center's were found for these tables in test data. To allow the FK's to run, set these to the closest named call centers. 
update client set update_call_center = NULL where update_call_center not in (select cc_code from call_center)
update client set call_center = 'FCV1' where call_center = 'FCV5'
update billing_rate set call_center = 'FAQ4' where call_center = 'FAQ2'
