
/* ============================================================ */
/*   Table: damage_profit_center_rule - rules for automated assignment of damage profit centers.*/
/* ============================================================ */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'damage_profit_center_rule')
  create table damage_profit_center_rule (
    rule_id  int identity(1, 1) not null primary key, 
    pc_code  varchar(15) not null,
    state    varchar(2) not null,
    county   varchar(40) not null,
    city     varchar(40) not null,
    modified_date datetime not null default GetDate(),
    active bit default 1
  )
go

-- add unique index to enforce one profit center per location
IF NOT EXISTS (select * from sys.indexes where name = 'damage_profit_center_rule_state_county_city')
  create unique index damage_profit_center_rule_state_county_city on damage_profit_center_rule(state,county,city)
go

-- add constraint to enforce valid profit center
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'damage_profit_center_rule' and CONSTRAINT_NAME = 'damage_profit_center_rule_fk_profit_center')
  alter table damage_profit_center_rule
    add constraint damage_profit_center_rule_fk_profit_center
    foreign key (pc_code) references profit_center (pc_code)
go

/* This insert statement is optional.  It can be used to initially populate the damage_profit_center_rule table with data.*/
/*
insert into damage_profit_center_rule (pc_code, state, county, city)
  select distinct profit_center, state, ISNULL(county, '*') as county, ISNULL(city, '*') as city
  from damage where profit_center is not null and state is not null 
  and modified_date between DATEADD(month, -12, GetDate()) and GETDATE()
*/
 
--Add new right that allows a user to manually assign a profit center (overriding the automatic assignment done by the app)
if not exists (select * from right_definition where entity_data = 'DamagesAssignProfitCenter')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesAssignProfitCenter', 'Damages - Assign Profit Center', 'General', 
    'Limitation does not apply to this right.')
go

-- add new column to the damage table that indicates user changed profit center
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME = 'user_changed_pc')
  alter table damage add user_changed_pc bit NOT NULL default 0
go





  
    

