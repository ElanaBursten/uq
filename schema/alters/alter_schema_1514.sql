if object_id('dbo.get_plat_list') is not null
	drop function dbo.get_plat_list
go

create function get_plat_list(@locate_id int)
returns varchar(400)
as
begin
  declare @locate_plat_list table (
    plat_list varchar(20)
  )

  insert into @locate_plat_list
    select locate_plat.plat
    from locate_plat 
    where locate_plat.locate_id = @locate_id

  declare @plat_list varchar(400)
  declare @locate_plat varchar(20)

  declare locate_plat_cursor cursor for
    select * from @locate_plat_list

  open locate_plat_cursor
  fetch next from locate_plat_cursor into @locate_plat

  set @plat_list = ''

  while @@fetch_status=0 begin
    if @plat_list = '' 
      set @plat_list = @locate_plat
    else
      set @plat_list = @plat_list +', '+ @locate_plat
    fetch next from locate_plat_cursor into @locate_plat
  end

  close locate_plat_cursor
  deallocate locate_plat_cursor

  return (@plat_list)
end
GO

-- ===========================================================

if object_id('dbo.RPT_TicketDetailById') is not null
	drop procedure dbo.RPT_TicketDetailById
go

create proc RPT_TicketDetailById (
	@TicketId int
)
as
set nocount on

-- Ticket data
select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image
from ticket
where ticket.ticket_id = @TicketId

-- Locate data
select
  locate.ticket_id,
  locate.client_code,
  locate.status,
  statuslist.status_name,
  employee.short_name,
  locate.high_profile,
  locate.qty_marked,
  locate.closed,
  locate.closed_date,
  dbo.get_plat_list(locate.locate_id) plat_list
from ticket
  left join locate
    on (locate.ticket_id = ticket.ticket_id)
  left join statuslist
    on statuslist.status = locate.status
  left join assignment
    on assignment.locate_id = locate.locate_id
      and assignment.active = 1
  left join employee
    on employee.emp_id = assignment.locator_id
where ticket.ticket_id = @TicketId
order by locate.ticket_id, locate.client_code

-- Notes data
select notes.entry_date, notes.modified_date, notes.note, ticket.ticket_id
  from ticket
   inner join notes on ticket.ticket_id=notes.foreign_id and notes.foreign_type = 1
where ticket.ticket_id = @TicketId
order by ticket.ticket_id, notes.entry_date

go
grant execute on RPT_TicketDetailById to uqweb, QManagerRole

/*
exec dbo.RPT_TicketDetailById 242342
exec dbo.RPT_TicketDetailById 5854755
exec dbo.RPT_TicketDetailById 9664115
*/


-- ===========================================================

if object_id('dbo.RPT_ClosedTicketDetail2') is not null
	drop procedure dbo.RPT_ClosedTicketDetail2
go

create proc RPT_ClosedTicketDetail2 (
	@TicketNumber varchar(30),
	@CallCenter varchar(10),
	@XmitDateFrom datetime,
	@XmitDateTo datetime
)
as
set nocount on

-- Ticket data
select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image
from ticket
where ticket.ticket_number = @TicketNumber
 and ticket.ticket_format = @CallCenter
 and ticket.transmit_date between @XmitDateFrom and @XmitDateTo

-- Locate data
select
  locate.ticket_id,
  locate.client_code,
  locate.status,
  statuslist.status_name,
  employee.short_name,
  locate.high_profile,
  locate.qty_marked,
  locate.closed,
  locate.closed_date,
  dbo.get_plat_list(locate.locate_id) plat_list
from ticket
  left join locate
    on locate.ticket_id = ticket.ticket_id
    and locate.status<>'-N'
  left join statuslist
    on statuslist.status = locate.status
  left join assignment
    on assignment.locate_id = locate.locate_id
     and assignment.active = 1
  left join employee
    on employee.emp_id = assignment.locator_id
where ticket.ticket_number = @TicketNumber
 and ticket.ticket_format = @CallCenter
 and ticket.transmit_date between @XmitDateFrom and @XmitDateTo

-- Notes data
select notes.entry_date, notes.modified_date, notes.note, ticket.ticket_id
  from ticket
   inner join notes on ticket.ticket_id=notes.foreign_id and notes.foreign_type = 1
where ticket.ticket_number = @TicketNumber
 and ticket.ticket_format = @CallCenter
 and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
order by ticket.ticket_id, notes.entry_date

-- Attachments
select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name
from attachment a
     inner join employee e on e.emp_id = a.attached_by
where
  a.active = 1 and
  a.extension in ('.bmp','.jpg','jpeg','.gif')

go
grant execute on RPT_ClosedTicketDetail2 to uqweb, QManagerRole

/*
exec dbo.RPT_ClosedTicketDetail2 '030922157', 'FDX1', '2002-01-01', '2004-01-01'
exec dbo.RPT_ClosedTicketDetail2 '04093153', 'FMW1', '2004-03-01', '2004-04-01'
*/
