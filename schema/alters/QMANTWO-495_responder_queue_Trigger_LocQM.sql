USE [LOCQM]
GO
/****** Object:  Trigger [dbo].[responder_queue_i]    Script Date: 10/23/2017 5:30:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].responder_queue_i ON [dbo].[responder_queue]
FOR INSERT
AS
  set nocount on
  if SYSTEM_USER = 'no_trigger'
    return
BEGIN
  insert into [dbo].[responder_multi_queue] (respond_to, locate_id, ticket_format, client_code, insert_date) 
    select distinct 'client', i.locate_id, i.ticket_format, i.client_code, i.insert_date
    from inserted i
	where 
	(i.ticket_format = 'SCA1' and i.client_code = 'ATTDSOUTH')
	or
	(i.ticket_format = 'NCA1' and i.client_code = 'PACBEL')
	or
	(i.ticket_format = 'LOR1' and i.client_code in ('PGE01',
												   'PGE03',
												   'PGE04',
												   'PGEF01',
												   'PGEF03',
												   'PGEF04',
												   'PGETUF01'))


END;