update right_definition 
  set modifier_desc = 'Limitation is F,S: F = Threshold in feet to compute on site vs. off site; S = GPS polling delay seconds. Default is 2000,5.'
  where entity_data = 'TrackGPSPosition'
