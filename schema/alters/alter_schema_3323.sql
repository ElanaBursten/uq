if not exists (select * from information_schema.tables where table_name='employee_pay_incentive')
begin
  create table employee_pay_incentive
  (
    incentive_id integer identity (82000, 1) not null,
    emp_id integer not null,             -- A point in the hierarchy
    incentive_type varchar(20) not null, -- HOL=Holiday Pay Incentives
    rate numeric(7,2) not null,          -- Amount to pay
    added_date datetime not null default GetDate(),
    modified_date datetime not null default GetDate(),
    active bit not null default 1,
    constraint pk_employee_pay_incentive primary key (incentive_id)
  )
end
go

IF NOT EXISTS (select * from sys.indexes where name = 'employee_pay_incentive_emp_id')
  create index employee_pay_incentive_emp_id on employee_pay_incentive(emp_id)
GO
if not exists(select * from sys.foreign_keys where object_id = OBJECT_ID(N'dbo.employee_pay_incentive_fk_employee') and parent_object_id = OBJECT_ID(N'dbo.employee_pay_incentive'))
  ALTER TABLE employee_pay_incentive ADD CONSTRAINT employee_pay_incentive_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id)
GO

if exists(select name from sysobjects where name = 'employee_pay_incentive_iu' and type = 'TR')
  drop trigger employee_pay_incentive_iu
GO
create trigger employee_pay_incentive_iu on employee_pay_incentive
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update employee_pay_incentive
  set modified_date = GetDate()
  from inserted I
  where employee_pay_incentive.incentive_id = I.incentive_id
end
GO

delete from reference where type = 'incpaytype'

insert into reference (type, code, description, active_ind)
  values ('incpaytype', 'HOL', 'Holiday', 1)

/* To rollback these changes:
-- drop the trigger
if exists(select name from sysobjects where name = 'employee_pay_incentive_iu' and type = 'TR')
  drop trigger employee_pay_incentive_iu
GO

-- drop the foreign key
IF EXISTS (select * from sys.indexes where name = 'employee_pay_incentive_emp_id')
 alter table employee_pay_incentive drop employee_pay_incentive_fk_employee
GO 

-- drop the new employee_pay_incentive table
if exists (select * from information_schema.tables where table_name = 'employee_pay_incentive')
  drop table employee_pay_incentive
GO

-- remove the new reference rows
delete from reference where type = 'incpaytype'
*/

