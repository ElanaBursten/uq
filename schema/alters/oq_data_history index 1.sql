
/****** Object:  Index [oq_data_empid_qualifieddate]    Script Date: 8/13/2021 11:26:07 AM ******/
CREATE NONCLUSTERED INDEX [oq_data_empid_qualifieddate] ON [dbo].[oq_data_history]
(
	[emp_id] ASC,
	[qualified_date] ASC,
	[history_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO


