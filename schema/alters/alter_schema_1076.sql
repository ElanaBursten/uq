if not exists(select right_id from right_definition where right_description='Damages - Bill Damage')
	insert into right_definition (right_description) values ('Damages - Bill Damage')

create table damage_bill (
	damage_bill_id integer identity(77000, 1) primary key,
	damage_id integer not null,
	billing_period_date datetime not null,
	billable bit not null default 1,
	modified_by integer not null,
	modified_date datetime not null DEFAULT getdate()
	)
go

ALTER TABLE damage_bill
	ADD CONSTRAINT damage_bill_fk_damage
	FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_bill
	ADD CONSTRAINT damage_bill_fk_employee
	FOREIGN KEY (modified_by) REFERENCES employee(emp_id)

GO