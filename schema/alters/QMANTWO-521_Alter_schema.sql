--QMANTWO-521 LDAP
--2/2018
--employee and employee_history table structure changes

--employee: ad_username column
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='ad_username')
BEGIN
	ALTER TABLE dbo.employee ADD
		ad_username varchar(8) NULL

	ALTER TABLE dbo.employee SET (LOCK_ESCALATION = TABLE)

	COMMIT
END
GO
select Has_Perms_By_Name(N'dbo.employee', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'CONTROL') as Contr_Per 
---------------------------------------------------


--employee_history: ad_username column
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_history' and COLUMN_NAME='ad_username')
BEGIN
	ALTER TABLE dbo.employee_history ADD
		ad_username varchar(8) NULL

	ALTER TABLE dbo.employee_history SET (LOCK_ESCALATION = TABLE)

	COMMIT
END
GO
select Has_Perms_By_Name(N'dbo.employee', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'CONTROL') as Contr_Per 