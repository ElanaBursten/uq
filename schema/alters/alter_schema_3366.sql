-- Add map_x_coord and map_y_coord columns to the work_order table

if not exists (select * from information_schema.columns
  where table_name = 'work_order' and column_name = 'map_x_coord')
    alter table work_order add map_x_coord decimal(12,4) null

if not exists (select * from information_schema.columns
  where table_name = 'work_order' and column_name = 'map_y_coord')
    alter table work_order add map_y_coord decimal(12,4) null

/* Undo steps:
-- drop the new work_order_work_type table
if exists (select * from information_schema.columns
  where table_name = 'work_order' and column_name = 'map_x_coord')
    alter table work_order drop column map_x_coord
    
if exists (select * from information_schema.columns
  where table_name = 'work_order' and column_name = 'map_y_coord')
    alter table work_order drop column map_y_coord
GO
*/
