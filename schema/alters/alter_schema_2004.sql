/* Undo commands: 
 delete from reference where type='empact' and code in ('DMADST', 'DMADEN')
*/
insert into reference (type, code, description) values ('empact', 'DMADST', 'Damage Addin Start')
insert into reference (type, code, description) values ('empact', 'DMADEN', 'Damage Addin End')
go
