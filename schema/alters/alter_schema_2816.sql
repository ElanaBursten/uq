--New employee activity for copying attachments from work orders to tickets with the QMCopyAttachments utility
if not exists (select * from reference where type = 'empact' and code = 'ATSYSADD')
  insert into reference (type, code, description) values ('empact', 'ATSYSADD', 'System Attachment Add')
if not exists (select * from reference where type = 'empact' and code = 'ATSYSUPL')
  insert into reference (type, code, description) values ('empact', 'ATSYSUPL', 'System Attachment Upload')
go

