USE [QM]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [client_fk_status_group]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [client_fk_office]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [client_fk_customer]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [DF__client__require___74650750]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [DF__client__alert__69A781BB]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [DF__client__allow_ed__1AFEE62D]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [DF__client__active__1273C1CD]
GO

ALTER TABLE [dbo].[client] DROP CONSTRAINT [DF__client__modified__117F9D94]
GO

/****** Object:  Table [dbo].[client]    Script Date: 12/7/2018 11:49:28 AM ******/
DROP TABLE [dbo].[client]
GO

/****** Object:  Table [dbo].[client]    Script Date: 12/7/2018 11:49:28 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[client](
	[client_id] [int] IDENTITY(300,1) NOT NULL,
	[client_name] [varchar](40) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[client] ADD [oc_code] [varchar](10) NOT NULL
ALTER TABLE [dbo].[client] ADD [office_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [modified_date] [datetime] NOT NULL
ALTER TABLE [dbo].[client] ADD [active] [bit] NOT NULL
ALTER TABLE [dbo].[client] ADD [call_center] [varchar](20) NOT NULL
ALTER TABLE [dbo].[client] ADD [update_call_center] [varchar](20) NULL
ALTER TABLE [dbo].[client] ADD [grid_email] [varchar](255) NULL
ALTER TABLE [dbo].[client] ADD [allow_edit_locates] [bit] NOT NULL
ALTER TABLE [dbo].[client] ADD [customer_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [utility_type] [varchar](15) NULL
ALTER TABLE [dbo].[client] ADD [status_group_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [unit_conversion_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [alert] [bit] NOT NULL
ALTER TABLE [dbo].[client] ADD [require_locate_units] [bit] NOT NULL
ALTER TABLE [dbo].[client] ADD [attachment_url] [varchar](200) NULL
ALTER TABLE [dbo].[client] ADD [qh_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [ref_id] [int] NULL
ALTER TABLE [dbo].[client] ADD [parent_client_id] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[client] ADD  DEFAULT (getdate()) FOR [modified_date]
GO

ALTER TABLE [dbo].[client] ADD  DEFAULT (1) FOR [active]
GO

ALTER TABLE [dbo].[client] ADD  DEFAULT (1) FOR [allow_edit_locates]
GO

ALTER TABLE [dbo].[client] ADD  DEFAULT (0) FOR [alert]
GO

ALTER TABLE [dbo].[client] ADD  DEFAULT ((0)) FOR [require_locate_units]
GO

ALTER TABLE [dbo].[client]  WITH CHECK ADD  CONSTRAINT [client_fk_customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[customer] ([customer_id])
GO

ALTER TABLE [dbo].[client] CHECK CONSTRAINT [client_fk_customer]
GO

ALTER TABLE [dbo].[client]  WITH NOCHECK ADD  CONSTRAINT [client_fk_office] FOREIGN KEY([office_id])
REFERENCES [dbo].[office] ([office_id])
GO

ALTER TABLE [dbo].[client] CHECK CONSTRAINT [client_fk_office]
GO

ALTER TABLE [dbo].[client]  WITH CHECK ADD  CONSTRAINT [client_fk_status_group] FOREIGN KEY([status_group_id])
REFERENCES [dbo].[status_group] ([sg_id])
GO

ALTER TABLE [dbo].[client] CHECK CONSTRAINT [client_fk_status_group]
GO

