/* alter_schema_2042.sql - adds a new field to statuslist & flags the initial statuses */
alter table statuslist add meet_only bit not null default 0
go

update statuslist set meet_only = 1 where status in ('XC', 'XN')
go
