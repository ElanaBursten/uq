
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE
TABLE_NAME = 'billing_trans_detail')

	CREATE TABLE billing_trans_detail(
		trans_billing_id [bigint] IDENTITY(1,1) NOT NULL,
		bill_id [int] NOT NULL,
		termcode [nvarchar](20) NULL,
		ticket_id [int] NULL,
		call_center [nvarchar](20) NULL,
		ticket_number [nvarchar](20) NULL,
		ticket_type [varchar](50) NULL,
		con_name [varchar](50) NULL,
		work_city [varchar](50) NULL,
		transmit_date [datetime] NULL,
		work_county [varchar](50) NULL,
		work_address_number [varchar](20) NULL,
		work_address_number_2 [varchar](20) NULL,
		work_address_street [varchar](60) NULL,
		work_cross [varchar](100) NULL,
		map_page [varchar](60) NULL,
		rate [money] NULL,
		locate_closed [bit] NULL,
		plat [varchar](20) NULL
	) ON [PRIMARY]

GO


CREATE CLUSTERED INDEX ClusteredIndexBillID ON billing_trans_detail
(
	bill_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO


GO

CREATE NONCLUSTERED INDEX index_tranmit_date ON billing_trans_detail
(
	transmit_date ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO



