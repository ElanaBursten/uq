-- New computer_info field to mark computers as retired
alter table computer_info 
  add retire_reason varchar(10) null
go

-- New reference table rows for the Computer Retirement Reasons
insert into reference (type, code, description, sortby) values ('compretr', '00', 'Not retired', 0)
insert into reference (type, code, description, sortby) values ('compretr', '01', 'Stolen', 1)
insert into reference (type, code, description, sortby) values ('compretr', '02', 'Unrepairable', 2)
insert into reference (type, code, description, sortby) values ('compretr', '03', 'Sold', 3)
go
