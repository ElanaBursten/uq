
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'work_order_version')
  CREATE TABLE work_order_version (
  wo_version_id int identity (8000, 1),
  wo_id integer NOT NULL,
  wo_revision varchar(20) NULL, /* The call center's description of this revision, if any */
  wo_number varchar(20) not null,     /* corresponds to work_order.wo_number*/
  work_type varchar(90) null,         /* corresponds to work_order.work_type*/
  transmit_date datetime not null,    /* corresponds to work_order.transmit_date */
  processed_date datetime NOT NULL,   /* D/T processed by parser */
  arrival_date datetime NULL,        /* timestamp of filename */
  image text NULL,                    /* corresponds to work_order.image */
  filename varchar(100) NULL, /* Filename where the work order came from */
  wo_source varchar(20) not null     /* aka call center; used to determine parser, responder, xml translation, etc for this wo*/
  )
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_work_order_version_wo_version_id')
  alter table work_order_version
  add constraint PK_work_order_version_wo_version_id
  primary key nonclustered (wo_version_id)
go
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'work_order_version' and CONSTRAINT_NAME = 'work_order_version_fk_wo_id')
  alter table work_order_version
  add constraint work_order_version_fk_wo_id
  foreign key (wo_id) references work_order (wo_id)
go

IF NOT EXISTS (select * from sys.indexes where name = 'work_order_version_wo_id')
  create index work_order_version_wo_id on work_order_version(wo_id)
go
  
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_version_transmit_date')
  create  INDEX work_order_version_transmit_date on work_order_version(transmit_date, wo_source)
go


/*The following query will generate records for the new work_order_version table from the work_order records.  Notably, the generated arrival_date is actually the 
transmit_date of the work order.  The reason this is done is to accomodate current work order attachments - they were initially stored by transmit_date.  By generating 
work_order_version records- placing the transmit_date in the arrival_date field- then the attachments for those older work orders can be found; follow consistent pathing*/
/*
declare @NumToInsert int
set @NumToInsert = 2000
declare @NotInsertedYet int
set @NotInsertedYet = (select count(*) from work_order where work_order.wo_id not in (select wo_id from work_order_version))

 --todo, perhaps the comment would be better in wo_revision vs image?  
IF @NotInsertedYet <> 0 BEGIN
--  PRINT N'There are currently ' + CAST(@NotInsertedYet as varchar(10)) + N' records to insert.';

  INSERT INTO work_order_version (wo_id,wo_revision, wo_number, work_type, transmit_date, processed_date,arrival_date,image, wo_source)
  select TOP(@NumToInsert) wo.wo_id, 'Generated', wo.wo_number, wo.work_type, wo.transmit_date, GETDATE(),wo.transmit_date,  wo.image, wo.wo_source 
    from work_order wo
    where wo.wo_id not in (select wo_id from work_order_version);
    
  IF (@NotInsertedYet - @NumToInsert > 0)   
    PRINT N'There are currently ' + CAST((@NotInsertedYet-@NumToInsert) as varchar(10)) + N' remaining records to insert.';
  ELSE
    PRINT N'There are no more records to insert.'  
END
ELSE
  PRINT N'There are no more records to insert.'
GO
*/  
