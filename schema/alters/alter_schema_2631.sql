/*The new work_priority_id column being added here is not populated by this feature.  It is 
being added because it is needed as part of the query on My Tickets screen so the damages can be prioritized.*/

-- Add work_priority_id column to Damage table
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'damage' and column_name = 'work_priority_id')
    alter table damage add 
      work_priority_id int null
go

-- New Reference data:
if not exists (select * from reference where type='dmgpriorit' and code='High')
  insert into reference (type, code, description, sortby) values ('dmgpriorit', 'High', 'High', 9)

if not exists (select * from reference where type='dmgpriorit' and code='Normal')
  insert into reference (type, code, description, sortby) values ('dmgpriorit', 'Normal', 'Normal', 0)

