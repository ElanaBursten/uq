--QMANTWO_163 Add AuditDueDateRules to Admin
----------------------------------------------
--Table: AuditDueDateRules
----------------------------------------------

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'audit_due_date_rule')
CREATE TABLE audit_due_date_rule(
	rule_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	audit_source varchar(15) NOT NULL,
	ticket_format varchar(15) NOT NULL,
	hours_due int NULL,
	modified_date datetime default GetDate(),
	active bit default 1,
    CONSTRAINT audit_due_date_rule_unique_constr UNIQUE NONCLUSTERED (audit_source, ticket_format, active)
)
GO


-- Recreate the audit_due_date_rule_u trigger
IF object_id('audit_due_date_rule_u') is not null
  DROP TRIGGER audit_due_date_rule_u

GO

CREATE TRIGGER audit_due_date_rule_u on audit_due_date_rule
FOR insert, update
not for replication
AS
BEGIN
  set NOCOUNT on
  update audit_due_date_rule
    set modified_date = getdate()
	from inserted I 
	where audit_due_date_rule.rule_id = I.rule_id
END
GO