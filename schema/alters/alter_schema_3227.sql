
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS
 where COLUMN_NAME = 'work_extent' AND TABLE_NAME = 'ticket') 
begin
  alter table ticket
    add work_extent varchar(100) null 
end
go

