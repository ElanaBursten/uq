-- Fix ynub and yesnoblank reference types so blank values are sync'ed correctly
update reference set code = 'B'
where (type = 'yesnoblank' or type = 'ynub')
  and description is null
  and code is null
  and active_ind = 1

/*
To rollback this script's changes:

update reference set code = null
where (type = 'yesnoblank' or type = 'ynub')
  and description is null
  and code = 'B'
  and active_ind = 1

*/
