-- One row for each county abbreviation found on incoming tickets
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'map_adc_county_abbr') 
  create table map_adc_county_abbr (
    county_abbr_id integer NOT NULL identity (6000, 1) primary key,
    county_abbr varchar(50),
    county_id integer NOT NULL,
  )
go

-- One row for each county map
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'map_adc_county') 
  create table map_adc_county (
    county_id integer NOT NULL identity (5000, 1) primary key,
    county_name varchar(50) NOT NULL,
    grid_lat_offset decimal(9,6) NOT NULL,  -- how much to adjust page_lat_ctr by for next cell's center
    grid_long_offset decimal(9,6) NOT NULL  -- how much to adjust page_long_ctr by for next cell's center 
  )
go

-- One row for each county map page
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'map_adc_map_page')
  create table map_adc_map_page (
    map_page_id integer NOT NULL identity (8000, 1) primary key,
    map_page varchar(4) NOT NULL,
    county_id integer NOT NULL,
    page_lat_ctr decimal(9,6) NOT NULL,
    page_long_ctr decimal(9,6) NOT NULL,
  )
go

-- One row for every old county map page & grid
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'map_adc_old_map_page') 
  create table map_adc_old_map_grid (
    old_map_grid_id integer NOT NULL identity (9000, 1) primary key,
    county_id integer NOT NULL,
    old_map_page_grid varchar(20) NOT NULL,
    new_map_page_grid varchar(20) NULL -- there may not be matching new page  
  )
go

-- foreign keys
ALTER TABLE map_adc_county_abbr
  ADD CONSTRAINT map_adc_county_abbr_fk_map_adc_county
  FOREIGN KEY (county_id) REFERENCES map_adc_county(county_id)
go
ALTER TABLE map_adc_map_page
  ADD CONSTRAINT map_adc_map_page_fk_map_adc_county
  FOREIGN KEY (county_id) REFERENCES map_adc_county(county_id)
go
ALTER TABLE map_adc_old_map_page
  ADD CONSTRAINT map_adc_old_map_page_fk_map_adc_county
  FOREIGN KEY (county_id) REFERENCES map_adc_county(county_id)
go

-- insert statements go here to populate the new tables...
