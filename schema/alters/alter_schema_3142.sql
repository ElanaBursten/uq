if not exists (select * from reference where type = 'emptype' and code='SYSTEM')
  insert into reference (type,code,description,active_ind) values ('emptype','SYSTEM','System',1)
select * from reference where type = 'emptype' and code = 'SYSTEM'

--Minimum identity value is 200 on employee table so no value < 200 should be in use now or in future; so we will make emp_id = 1 always be the system employee.
Set IDENTITY_INSERT dbo.employee ON
GO
if not exists (select * from employee where emp_id = 1)
  insert into employee (emp_id, type_id, emp_number, short_name, first_name, last_name, report_to, active, can_receive_tickets, charge_cov, company_id)
    values (1, (select ref_id from reference where type = 'emptype' and code = 'SYSTEM'), '1','System User', 'System', 'User',(select emp_id from employee where report_to is null),0,0,0,1)
Set IDENTITY_INSERT dbo.employee OFF
GO 
select * from employee where emp_id = 1

--Minimum identity value is 500 on users table so no value , 500 should be in use now or in future; so we will make uid = 1 for the system user that corresponds to the system employee.
Set IDENTITY_INSERT dbo.users ON
GO
if not exists (select * from users where uid = 1)
  insert into users (uid, emp_id, first_name, last_name, login_id, chg_pwd,active_ind,can_view_notes,can_view_history,etl_access)
    values (1, 1, 'System', 'User','system', 0,0,0,0,0)
Set IDENTITY_INSERT dbo.users OFF
GO 
select * from users where uid = 1

--Set the system user ID in the configuration settings table
if not exists(select * from configuration_data where name = 'SystemUserEmpID')
  insert into configuration_data (name, value, editable) values ('SystemUserEmpID',1,0) --not editable so that it cannot be changed by Admin users
select * from configuration_data where name = 'SystemUserEmpID'



