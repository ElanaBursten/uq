-- Add 'Tickets - Show Future Workload' right
if not exists (select * from right_definition where entity_data = 'TicketsShowFutureWorkload')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('TicketsShowFutureWorkload', 'Tickets - Show Future Workload', 'General', 
    'Limitation does not apply to this right.')
go
