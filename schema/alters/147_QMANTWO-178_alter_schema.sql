--QMANTWO_178 ALter Statement:
----------------------------------------------
--Table: Disclaimer
----------------------------------------------
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'disclaimer')
CREATE TABLE disclaimer(
	disclaimer_id int IDENTITY(1,1) NOT NULL Primary Key,
	disclaimer_text text NOT NULL
	)

GO
SET IDENTITY_INSERT disclaimer ON 

INSERT disclaimer (disclaimer_id, disclaimer_text) VALUES (1, N'Ticket information, facility maps, electronic locate manifests and any associated aerial images ("Materials") provided to the excavator via "Enhanced Positive Response" (EPR) are for internal excavator planning and communication purposes only and are not authorized to be redistributed to any third party. The Materials are distributed and transmitted on an "as is" and "as available" basis, without warranties of any kind, either express or implied. They are not to be used to determine where excavation can occur and do not substitute for the physical markings at the excavation site. They are not to be relied upon to determine whether or where facilities exist at the excavation site. Excavators are required to comply with the applicable Underground Facilities Damage Prevention laws in the area where the work was performed.')
INSERT disclaimer (disclaimer_id, disclaimer_text) VALUES (2, N'Ticket information, facility maps, electronic locate manifests and any associated aerial images ("Materials") provided to the excavator via "Enhanced Positive Response" (EPR) are for internal excavator planning and communication purposes only and are not authorized to be redistributed to any third party. The Materials are distributed and transmitted on an "as is" and "as available" basis, without warranties of any kind, either express or implied. They are not to be used to determine where excavation can occur and do not substitute for the physical markings at the excavation site. They are not to be relied upon to determine whether or where facilities exist at the excavation site. Excavators are required to comply with the applicable Underground Facilities Damage Prevention laws in the area where the work was performed.')
INSERT disclaimer (disclaimer_id, disclaimer_text) VALUES (3, N'Ticket information, facility maps, electronic locate manifests and any associated aerial images ("Materials") provided to the excavator via "Enhanced Positive Response" (EPR) are for internal excavator planning and communication purposes only and are not authorized to be redistributed to any third party. The Materials are distributed and transmitted on an "as is" and "as available" basis, without warranties of any kind, either express or implied. They are not to be used to determine where excavation can occur and do not substitute for the physical markings at the excavation site. They are not to be relied upon to determine whether or where facilities exist at the excavation site. Excavators are required to comply with the applicable Underground Facilities Damage Prevention laws in the area where the work was performed.')
INSERT disclaimer (disclaimer_id, disclaimer_text) VALUES (4, N'Ticket information, facility maps, electronic locate manifests and any associated aerial images ("Materials") provided to the excavator via "Enhanced Positive Response" (EPR) are for internal excavator planning and communication purposes only and are not authorized to be redistributed to any third party. The Materials are distributed and transmitted on an "as is" and "as available" basis, without warranties of any kind, either express or implied. They are not to be used to determine where excavation can occur and do not substitute for the physical markings at the excavation site. They are not to be relied upon to determine whether or where facilities exist at the excavation site. Excavators are required to comply with the applicable Underground Facilities Damage Prevention laws in the area where the work was performed.')
SET IDENTITY_INSERT disclaimer OFF



---------------------------------------------
-- ROLLBACK 
---------------------------------------------
-- Drop table disclaimer
