/*
   Friday, November 11, 20165:19:18 PM
   User: 
   Server: DYATL-DQMGDB01
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.configuration_data ADD
	description ntext NULL
GO
ALTER TABLE dbo.configuration_data
	DROP CONSTRAINT DF__configura__edita__60882BD5
GO
ALTER TABLE dbo.configuration_data ADD CONSTRAINT
	DF__configura__edita__60882BD5 DEFAULT ((1)) FOR editable
GO
ALTER TABLE dbo.configuration_data SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select * from configuration_data