-- 2460: Add pc_city column to the profit_center table
if not exists (select * from INFORMATION_SCHEMA.columns where table_name = 'profit_center' and column_name = 'pc_city')
  alter table profit_center add pc_city varchar(40) null 
go
