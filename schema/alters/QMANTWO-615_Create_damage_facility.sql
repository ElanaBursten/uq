/*
   Monday, July 16, 20182:36:11 PM
   User: 
   Server: DYATL-DQMGDB01
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.damage_facility
	DROP CONSTRAINT DF_damage_facility_modified_date
GO
CREATE TABLE dbo.Tmp_damage_facility
	(
	damage_id int NOT NULL,
	Facility_Type varchar(20) NULL,
	Facility_Size varchar(10) NULL,
	Facility_Material varchar(10) NULL,
	modified_date datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_damage_facility SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_damage_facility ADD CONSTRAINT
	DF_damage_facility_modified_date DEFAULT (getdate()) FOR modified_date
GO
IF EXISTS(SELECT * FROM dbo.damage_facility)
	 EXEC('INSERT INTO dbo.Tmp_damage_facility (damage_id, Facility_Type, Facility_Size, Facility_Material, modified_date)
		SELECT damage_id, Facility_Type, Facility_Size, Facility_Material, modified_date FROM dbo.damage_facility WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.damage_facility
GO
EXECUTE sp_rename N'dbo.Tmp_damage_facility', N'damage_facility', 'OBJECT' 
GO
COMMIT
select Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'CONTROL') as Contr_Per 