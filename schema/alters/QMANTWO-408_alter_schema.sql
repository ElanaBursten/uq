--QMANTWO-408
--The items in facilities type (factype) and utilities (utility) do not match up.
--This is an optional update and will affect values throughout the application



INSERT INTO reference
           (type
           ,code
           ,description
           ,active_ind
           ,modified_date)
     VALUES
           ('factype'
           ,'GASELEC'
           ,'Gas and Electric'
           ,1
           ,GetDATE())
GO

select * from reference where (type = 'utility') or (type = 'factype')

--rollback
--delete from reference
--where (type = 'factype') and (code = 'GASELEC')
