-- Create exported_damages_history table if it doesn't already exist in the db:
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'exported_damages_history') 
	CREATE TABLE exported_damages_history(
		short_period varchar(20) NOT NULL,
		estimate_id int NOT NULL,
		project varchar(20) NOT NULL,
		task varchar(30) NOT NULL,
		claim_number int NOT NULL,
		invoice_code varchar(10) NULL,
		liability_amount decimal(12, 2) NULL,
    change_in_liability_amount decimal(12, 2) NULL, 
    insert_date datetime NOT NULL DEFAULT GetDate()
	) 
GO

/* -- adds just the insert_date column to exported_damages_history:
ALTER TABLE exported_damages_history ADD insert_date datetime NOT NULL DEFAULT GetDate()
GO
*/

-- Create exported_damages_history_claim_number_insert_date if it doesn't already exist:
IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE NAME = 'exported_damages_history_claim_number_insert_date') 
  CREATE INDEX exported_damages_history_claim_number_insert_date ON exported_damages_history (claim_number, insert_date)
GO

-- Add new right for viewing accrual history tab:
IF NOT EXISTS (SELECT * FROM right_definition WHERE entity_data = 'DamagesViewAccrualHistory') 
  INSERT INTO right_definition (right_description, right_type, entity_data, modifier_desc)
    VALUES ('Damages - View Accrual History', 'General', 'DamagesViewAccrualHistory', 'Limitation does not apply to this right.')
GO
