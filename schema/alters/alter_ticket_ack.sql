/*
   Tuesday, December 3, 20198:51:47 AM
   User:SR 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ticket_ack ADD
	claimedBy_Emp_id int NULL
GO
ALTER TABLE dbo.ticket_ack SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ticket_ack', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ticket_ack', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ticket_ack', 'Object', 'CONTROL') as Contr_Per 