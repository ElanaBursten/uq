-- Event Log
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'event_log') begin
  CREATE TABLE event_log (
    event_id uniqueidentifier NOT NULL DEFAULT NewID(),
    aggregate_type integer NOT NULL,      -- 1 = Ticket, 2 = Damage, 3 = Work Order
    aggregate_id integer NOT NULL, 
    version_num integer NOT NULL,
    event_data varchar(5000) NOT NULL,    -- the event object
    event_added datetime NOT NULL DEFAULT GetDate(),
    constraint pk_event_log primary key nonclustered (event_id)
  )

  CREATE UNIQUE CLUSTERED INDEX event_log_aggregate_idx ON event_log(aggregate_type, aggregate_id, version_num)
end
GO


/* To rollback this change:

drop table event_log

*/
