IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'customer' and COLUMN_NAME = 'contract')
  alter table customer add contract varchar(50) null