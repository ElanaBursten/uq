--Add new right that allows user to view Work Order History
if not exists (select * from right_definition where entity_data = 'ViewWorkOrderHistory')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('ViewWorkOrderHistory', 'Work Orders - View History', 'General', 'Limitation does not apply to this right.')
go
