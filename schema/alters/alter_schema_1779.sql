if exists(select name FROM sysobjects where name = 'locate_iu' AND type = 'TR')
  DROP TRIGGER locate_iu
GO

CREATE TRIGGER locate_iu ON locate
FOR INSERT, UPDATE
AS
BEGIN
/* ********************** WARNING!!!! ******************************
   This is a very fragile part of the system, do not do anything with
   this trigger in the production DB without coordinating with the
   team, and having all of the front-end machines shut down to
   prevent any changes from slipping in while changing the trigger.
*/

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- exit if only the invoiced column was changed
  declare @invoiced_col int
  set @invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'invoiced')

  -- NOTE: "@invoiced_col % 8" would not work if the column number is 16, as it is
  -- for assigned_to, so it was changed to "- 8"
  if (@invoiced_col < 9) or (@invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- exit if the assigned_to column was changed
  -- and none of the initial fields changed
  declare @assignedto_col int
  set @assignedto_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to')

  if (@assignedto_col < 9) or (@assignedto_col > 16)
  begin
    rollback transaction
    RAISERROR('assigned_to has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@assignedto_col - 8)-1)))
    return

  -- exit if the assigned_to_id column was changed
  -- and none of the initial fields changed
  declare @assignedtoid_col int
  set @assignedtoid_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to_id')

  if (@assignedtoid_col < 17) or (@assignedto_col > 24)
  begin
    rollback transaction
    RAISERROR('assigned_to_id has moved out of the third column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 3, 1) = power(2, ((@assignedtoid_col - 16)-1)))
    return

  -- if both assigned_to and assigned_to_id changed, either of the
  -- above will return

  -- set the ticket's kind
  DECLARE @ticket_id int
  SELECT @ticket_id = ticket_id FROM inserted

  EXEC dbo.update_ticket_status @ticket_id

  -- Update the modified_date when a locate changes
  update locate set
    modified_date = getdate()
  where locate_id in (select locate_id from inserted)

  -- When a locate is re-opened, refresh the assigned_to field
  update locate
    set assigned_to = (
      select top 1 assignment.locator_id
      from assignment
      where
        (assignment.locate_id = locate.locate_id) and
        (assignment.active = 1)
    )
  from deleted -- contains rows that were just updated, but before the updates were applied
    inner join inserted
      on inserted.locate_id = deleted.locate_id
  where locate.locate_id = deleted.locate_id
    and deleted.closed = 1
    and inserted.closed = 0

  -- Set assigned_to = NULL for all closed/non-active locates
  update locate
    set assigned_to = NULL
    where locate_id in (
      select locate_id
      from inserted
      where closed = 1
       or status = '-P'
       or status = '-N'
       or active = 0
    )

  -- Add response for a changed status not already queued
  insert into responder_queue (locate_id, ticket_format, client_code)
  select distinct i.locate_id, t.ticket_format, i.client_code
    from inserted i
      inner join ticket t on i.ticket_id=t.ticket_id
      left outer join responder_queue rq on i.locate_id=rq.locate_id
    where i.status <> '-N'
     and i.status <> '-R'
     and i.status <> '-P'
     and rq.locate_id is null 
     and (i.status <> (select top 1 ls.status from locate_status ls where ls.locate_id = i.locate_id order by insert_date desc)
      or i.status is not null and not exists (select ls.locate_id from locate_status ls where ls.locate_id = i.locate_id))
  
  -- Do this insert last because StatusTicketsLocatesInternal needs this identity    
  insert into locate_status (
    locate_id, status_date, status, high_profile, high_profile_reason,
    qty_marked, statused_by,  statused_how, regular_hours, overtime_hours, mark_type)
  select
    locate_id, closed_date, status, high_profile, high_profile_reason,
    qty_marked, closed_by_id, closed_how, regular_hours, overtime_hours, mark_type
  from inserted
  where status <> '-N'
   and status <> '-P'
END
go
