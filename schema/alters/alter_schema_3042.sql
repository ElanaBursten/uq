if not exists(select * from sys.key_constraints where name = 'pk_map_trs') begin
  declare @trs table (
    township varchar(5) not null,
    range varchar(6) not null,
    section varchar(6) not null,
    area_id integer null,
    basecode varchar(10) null,
    call_center varchar(20) null,
    points int null
  )
  
  /* We researched these, and concluded the null basecodes are obsolete, and 
     the other 2 are typos */
  delete from map_trs where
    basecode is null or
    basecode in ('`', 'c0')

  -- Our test data didn't have any of these, but I'm leaving it in just in case
  update map_trs set basecode = '-'
  where rtrim(basecode) = '' 

  update map_trs set call_center = '*'
  where call_center is null or rtrim(call_center) = ''

  /* Insert map_trs records into @trs, without duplicates. And assign
     points as follows: no locator = -1; inactive locator = 0; active locator,
     not receiving tickets = 1, active locator, receiving tickets = 2 */
  insert into @trs
  select distinct m.*, coalesce(e.active, -1) + coalesce(e.active, 0) *
    coalesce(e.can_receive_tickets, 0)
  from map_trs m
  left join area a on a.area_id = m.area_id
  left join employee e on e.emp_id = a.locator_id

  begin transaction

  delete from map_trs

  insert into map_trs
  select t.township, t.range, t.section, t.area_id, t.basecode, t.call_center
  from @trs t
  where not exists (
    /* Check for a record with the same key, and more points. Or the same key,
       the same points, and higher area_id. */
    select * from @trs t2
    where
      t2.township = t.township and
      t2.range = t.range and
      t2.section = t.section and
      t2.basecode = t.basecode and
      t2.call_center = t.call_center and (
        t2.points > t.points or (
          t2.points = t.points and
          coalesce(t2.area_id, -1) > coalesce(t.area_id, -1))))

  commit transaction
  
  alter table map_trs
    alter column basecode varchar(10) not null
  alter table map_trs
    alter column call_center varchar(20) not null
end
go

if not exists(select * from sys.key_constraints where name = 'pk_map_trs')
  alter table map_trs
    add constraint pk_map_trs primary key nonclustered (basecode, township,
      range, section, call_center)
go

-- These have been replaced by find_township_baseline_2, and are obsolete
if object_id('dbo.find_township') is not null
  drop procedure dbo.find_township
go
if object_id('dbo.find_township_baseline') is not null
  drop procedure dbo.find_township_baseline
go
