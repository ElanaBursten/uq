-- create new temp table to save text passwords
/* Note: saved_password is no longer needed once db password data is encrypted */ 
create table saved_password (
  user_id int not null primary key,
  text_password varchar(40) null
)
go

insert into saved_password select uid, password from users
go

-- widen users.password
alter table users
  alter column password varchar(40) null
go

-- recreate the change_password sp
if object_id('dbo.change_password') is not null
  drop procedure dbo.change_password
go

create procedure change_password (
  @UserID       integer,
  @NewPassword  varchar(40)
)
as
  set nocount on
  declare @NewDate datetime
  set @NewDate = convert(char(10), dateadd(d, Convert(int, dbo.GetConfigValue('PasswordExpirationDays', '120')), getdate()), 102)
  update users set
    password = @NewPassword,
    chg_pwd  = 0,
    chg_pwd_date = @NewDate
  where users.uid = @UserID

go
grant execute on change_password to uqweb, QManagerRole

/*
dbo.change_password 2227, 'fishfaces'
select convert(char(10), getdate( ), 102)

select * from configuration_data
insert into configuration_data (name, value, editable) values ('PasswordExpirationDays', '120', 1)
*/

-- (re)create login_user2 
if object_id('dbo.login_user2') is not null
  drop procedure dbo.login_user2
go

create procedure login_user2
  (
  @UserName varchar(25),
  @PW  varchar(40)
  )
as

select employee.emp_id, users.chg_pwd, users.uid, users.chg_pwd_date,
  employee.type_id, employee.report_to, employee.short_name,
  reference.code AS 'TypeCode', employee.emp_number,
  /* Note: password is no longer needed once db password data is encrypted (see #1641) */ 
  password 
 from users
  left join employee on users.emp_id=employee.emp_id
  left join reference on employee.type_id=reference.ref_id and reference.type='emptype'
 where login_id = @UserName
  and (users.password = @PW 
    /* Note: Null @PW criteria can be removed once db password data is encrypted */ 
    or @PW is null)
  and users.active_ind = 1
  and users.password is not null
  and users.password <> ''
  and (employee.active=1 or employee.active is null)

go
grant execute on login_user2 to uqweb, QManagerRole
go

/*
exec dbo.login_user2 '993726', '1234'       -- valid
exec dbo.login_user2 '01557', '1234'        -- valid
exec dbo.login_user2 '93644', null          -- valid null password
exec dbo.login_user2 'ruggeri', 'password'  -- user inactive
exec dbo.login_user2 '994370', '123456'     -- employee inactive
exec dbo.login_user2 '02221', '4321'        -- wrong password
exec dbo.login_user2 'blah', null           -- wrong user
*/
