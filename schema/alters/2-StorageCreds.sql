
USE [QM]
GO

USE [QM]
GO

/****** Object:  Table [dbo].[api_storage_credentials]    Script Date: 2/28/2018 5:21:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[api_storage_credentials](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[storage_type] [varchar](50) NULL,
	[enc_access_key] [nvarchar](max) NULL,
	[enc_secret_key] [nvarchar](max) NULL,
	[unc_date_created] [datetime] NOT NULL,
	[modified_date] [datetime] NULL,
	[unc_date_expired] [datetime] NULL,
	[api_storage_constants_id] [int] NOT NULL,
	[description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO





INSERT INTO [dbo].[api_storage_credentials]
           ([storage_type]
           ,[enc_access_key]
           ,[enc_secret_key]
           ,[unc_date_created]
		   ,[modified_date]
           ,[unc_date_expired]
           ,[api_storage_constants_id]
           ,[description])
     VALUES
           ('AWS'
           ,'xxxxxxxxxxxxxxxxxxxxxxxxx'
           ,'xxxxxxxxxxxxxxxxxxxxxxxxx'
           ,CURRENT_TIMESTAMP
		   ,CURRENT_TIMESTAMP
           ,CURRENT_TIMESTAMP
           ,1
           ,'Production QM keys for AWS')
GO

INSERT INTO [dbo].[api_storage_credentials]
           ([storage_type]
           ,[enc_access_key]
           ,[enc_secret_key]
           ,[unc_date_created]
		   ,[modified_date]
           ,[unc_date_expired]
           ,[api_storage_constants_id]
           ,[description])
     VALUES
           ('AWS'
           ,'xxxxxxxxxxxxxxxxxxxxxxxxx'
           ,'xxxxxxxxxxxxxxxxxxxxxxxxx'
           ,CURRENT_TIMESTAMP
		   ,CURRENT_TIMESTAMP
           ,CURRENT_TIMESTAMP
           ,2
           ,'Production LOC keys for AWS')
GO

