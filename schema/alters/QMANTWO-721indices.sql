--I added the indices below and was able to reduce the query time from over 4 minutes to 44 seconds. The index on Ticket Snap took about 8 --minutes to create. I would recommend we wait until after hours to add that one.

USE QM

/****** Object: Index PayRoll_PC_Code Script Date: 3/5/2019 4:58:51 PM ******/
CREATE NONCLUSTERED INDEX PayRoll_PC_Code ON dbo.employee
(
payroll_pc_code ASC
)
INCLUDE ( emp_id) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
GO

/****** Object: Index Repr_PC_Code Script Date: 3/5/2019 4:58:51 PM ******/
CREATE NONCLUSTERED INDEX Repr_PC_Code ON dbo.employee
(
repr_pc_code ASC
)
INCLUDE ( emp_id) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
GO

================================================================================
USE QM
GO

/****** Object: Index First_Close_EmpID_Close_Date Script Date: 3/5/2019 4:58:51 PM ******/
CREATE NONCLUSTERED INDEX First_Close_EmpID_Close_Date ON dbo.ticket_snap
(
first_close_date ASC,
first_close_emp_id ASC
)
INCLUDE ( ticket_id) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
GO