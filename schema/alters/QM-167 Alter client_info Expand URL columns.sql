--QM-167 Note: The field links were changed previously.  However, the model values were not updated in the Build database
--The build will need to be run in order for the DBISAM values to update
ALTER TABLE client_info 
ALTER COLUMN training_link nvarchar(200);
ALTER TABLE client_info
ALTER COLUMN plats_online_link nvarchar(200);
GO 
