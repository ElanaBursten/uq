-- Add new Timesheets - Clock Import right if it does not exist
if not exists (select right_id from right_definition where entity_data = 'TimesheetsClockImport')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Timesheets - Clock Import', 'General', 'TimesheetsClockImport', 
    'Limitation does not apply to this right.') 
go
