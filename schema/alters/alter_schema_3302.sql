if not exists (select * from term_group where group_code = 'AtlantaCanBillCancels')
  insert into term_group (group_code, comment)
  values ('AtlantaCanBillCancels', 'Bill these terms on cancel tickets')

if not exists (select * from term_group where group_code = 'AtlantaCanBillDamages')
  insert into term_group (group_code, comment)
  values ('AtlantaCanBillDamages', 'Bill these terms on damage tickets')
