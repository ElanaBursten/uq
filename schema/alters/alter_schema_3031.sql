-- Store the external system's transmission ID (eFax's transmission identifier, size 8 per spec); this will be needed for subsequent status requests
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'fax_message' and COLUMN_NAME='ext_doc_id')
  alter table fax_message add ext_doc_id varchar(8) NULL 
go

-- Store the disposition XML (that was HTTP posted to us); this reports whether eFax succeeded in transmitting the fax or not
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'fax_message' and COLUMN_NAME='ext_disposition')
  alter table fax_message add ext_disposition varchar(MAX) NULL 
go

-- Dynamic Fax Header String used by eFax
if not exists (select * from configuration_data where name = 'FaxHeader')
  insert into configuration_data (name, value, editable)
    values ('FaxHeader', '@DATE1 @TIME3 Utiliquest @ROUTETO{26} @RCVRFAX Pg%P/@SPAGES', 1)
go  
    
-- Add unique constraints on the fax_queue_rules and email_queue_rules to prevent duplicate rules
if not exists (select constraint_name from information_schema.table_constraints 
  where constraint_name = 'constr_fax_queue_rules_unique' and table_name = 'fax_queue_rules' and constraint_type = 'unique') 
  ALTER TABLE fax_queue_rules
    ADD CONSTRAINT constr_fax_queue_rules_unique
    UNIQUE (call_center, client_code, status, state)
go

if not exists (select constraint_name from information_schema.table_constraints 
  where constraint_name = 'constr_email_queue_rules_unique' and table_name = 'email_queue_rules' and constraint_type = 'unique') 
ALTER TABLE email_queue_rules
  ADD CONSTRAINT constr_email_queue_rules_unique
  UNIQUE (call_center, client_code, status, state)
go 

