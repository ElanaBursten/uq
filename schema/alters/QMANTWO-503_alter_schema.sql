/* QMANTWO-503: Holiday table will be pulled down to DBISAM. Adding modified_date to trigger in the sync. */

IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS 
               where TABLE_NAME = 'holiday' 
			   and COLUMN_NAME = 'modified_date')

ALTER TABLE holiday ADD
	modified_date datetime NULL
GO
