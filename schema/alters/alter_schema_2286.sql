-- 2286: Create new first_close_pc_close_date index 
create index first_close_pc_close_date on locate_snap(first_close_pc, first_close_date)
go

-- Also need to apply the new export_tasks.sql sp
