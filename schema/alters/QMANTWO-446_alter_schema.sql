--QMANTWO-446 New Column to determine the work_order_work_type
--As we add work order types, we need to be able to filter down what can be displayed for each type
--As is common to QM tables, * will represent that it is used for all wo_source types



IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS 
               where TABLE_NAME = 'work_order_work_type' 
			   and COLUMN_NAME = 'wo_source')
ALTER table work_order_work_type
add wo_source varchar(20) NOT NULL Default('*')

--Update Modified_date to ensure that the wo_source replicate
Update work_order_work_type --Forces the modified_date to change on existing records
set modified_date = GetDate()

--Select * from work_order_work_type


------------SET THESE ONLY FOR TESTING-------------------------------
Update work_order_work_type
set wo_source = 'WGL' 
where (work_type_id < 81)

Update work_order_work_type
set wo_source = '*' 
where (work_type in ('NM', 'CBNV', 'PM', 'RM', 'MM'))


Update work_order_work_type
set wo_source = 'OHM,INR' 
where (work_type in ('CVLID', 'NOAOC'))

Update work_order_work_type
set wo_source = 'INR' 
where (work_type in ('AC'))