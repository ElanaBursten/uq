-- New fields for damage_invoice
ALTER TABLE damage_invoice
  ADD litigation_id integer NULL,
  third_party_id integer NULL,
  added_by integer NULL
GO

-- Foreign keys
ALTER TABLE damage_third_party
  ADD CONSTRAINT damage_third_party_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_third_party
  ADD CONSTRAINT damage_third_party_fk_carrier
  FOREIGN KEY (carrier_id) REFERENCES carrier(carrier_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_damage_litigation
  FOREIGN KEY (litigation_id) REFERENCES damage_litigation(litigation_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_damage_third_party
  FOREIGN KEY (third_party_id) REFERENCES damage_third_party(third_party_id)

ALTER TABLE damage_invoice
  ADD CONSTRAINT damage_invoice_fk_damage_litigation
  FOREIGN KEY (litigation_id) REFERENCES damage_litigation(litigation_id)

ALTER TABLE damage_invoice
  ADD CONSTRAINT damage_invoice_fk_damage_third_party
  FOREIGN KEY (third_party_id) REFERENCES damage_third_party(third_party_id)
GO

-- New damage field
ALTER TABLE damage 
  ADD estimate_locked bit NOT NULL DEFAULT 0
GO

-- Need to recreate since damage table changed
IF EXISTS (SELECT TABLE_NAME from INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'dycom_damage')
  DROP VIEW dbo.dycom_damage
GO
CREATE VIEW dycom_damage
AS
  select d.*, dp.short_period as fiscal_period,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id asc) as initial_estimate,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id desc) as current_estimate
  from damage d
  left join dycom_period dp on d.accrual_date >= dp.starting and accrual_date < dp.ending
GO

-- Add new reference data
INSERT INTO REFERENCE (type, code, description, sortby) VALUES ('esttype', '0', 'DAMAGE', 1)
INSERT INTO REFERENCE (type, code, description, sortby) VALUES ('esttype', '1', '3RD PARTY', 2)
INSERT INTO REFERENCE (type, code, description, sortby) VALUES ('esttype', '2', 'LITIGATION', 3)
GO

