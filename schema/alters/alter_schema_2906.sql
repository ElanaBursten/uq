IF EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'right_definition' AND COLUMN_NAME = 'modifier_desc') BEGIN
	ALTER TABLE right_definition ALTER COLUMN modifier_desc VARCHAR(175)
	UPDATE right_definition SET modifier_desc='Limitation is F,S,P: F = Threshold in feet to compute on site vs. off site; S = GPS polling delay seconds; P = Port procedure/number. Default is 2000,5,-1.' WHERE entity_data='TrackGPSPosition'
END
