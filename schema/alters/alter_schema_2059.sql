/* Adds new right identifier to allow viewing Ticket Details without arriving first */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Tickets - Show without Arriving', 'General', 'TicketsShowWithoutArriving', 'Limitation does not apply to this right')
