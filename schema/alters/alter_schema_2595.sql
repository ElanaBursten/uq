
-- Add reference information
if not exists (select * from reference where type='locstat' and code='ONSITE')
  insert into reference (type, code, description, sortby, active_ind)
  values ('locstat','ONSITE','On Site', 1, 1)

if not exists (select * from reference where type='locstat' and code='OFFSITE')
  insert into reference (type, code, description, sortby, active_ind)
  values ('locstat','OFFSITE','Off Site', 2, 1)

if not exists (select * from reference where type='workstat' and code='CLEARED')
  insert into reference (type, code, description, sortby, active_ind)
  values ('workstat','CLEARED','Cleared', 1, 1)

if not exists (select * from reference where type='workstat' and code='MARKED')
  insert into reference (type, code, description, sortby, active_ind)
  values ('workstat','MARKED','Marked', 2, 1)
  
go  

-- Add work_status (for CLEARED/MARKED) column to locate_snap  
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'locate_snap' and column_name = 'work_status')
    alter table locate_snap add 
      work_status varchar(15) null
go
    
-- Add location_status (for ONSITE/OFFSITE) to locate_snap
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'locate_snap' and column_name = 'location_status')
    alter table locate_snap add 
      location_status varchar(15) null 
go

-- Add work_status column to ticket_snap
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'ticket_snap' and column_name = 'work_status')
    alter table ticket_snap add 
      work_status varchar(15) null 
go    
  
  

  

 
