-- New reference table rows for Ticket Plat Addin Start & End employee activity types

if not exists (select * from reference where type = 'empact' and code = 'TIPAST') 
  insert into reference (type, code, description) values ('empact', 'TIPAST', 'Ticket Plat Addin Start')
if not exists (select * from reference where type = 'empact' and code = 'TIPAEN') 
  insert into reference (type, code, description) values ('empact', 'TIPAEN', 'Ticket Plat Addin End')
