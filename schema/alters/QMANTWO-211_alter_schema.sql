
	--QMANTWO-211_Alter_Schema.sql - This adds the new activity type when the login machine does not match the assigned machine
	  if not exists (select * from right_definition where entity_data = 'SyncMyComputerSerialNum')
    insert into right_definition(right_description, right_type, entity_data, parent_right_id, modifier_desc)
	values('Sync - Computer Serial #', 'General', 'SyncMyComputerSerialNum', NULL, 'Limitation: Employee computer serial number');


	IF NOT EXISTS(select * from reference where type = 'empact' and code = 'SERIALMISMATCH')
	INSERT INTO reference(type, code, description, active_ind)
	               VALUES('empact', 'SERIALMISMATCH', 'Device Serial # does match user', 1 )
				   
	--select * from reference 
	--where type = 'empact'
	--and code = 'SERIALMISMATCH'
