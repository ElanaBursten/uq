-- Add all new rights that are not already in the right_definition table:

declare @NewRights table (
  description varchar(50) not null,
  code varchar(50) not null,
  mod_desc varchar(150) null)

insert into @NewRights values ('Tickets - Auto-approve New Tickets', 'TicketsAutoApproveNewTickets', null)
insert into @NewRights values ('Tickets - Assign New Tickets', 'TicketsAssignNewTickets', null)
insert into @NewRights values ('Tickets - Add Ready To Mark Locates', 'TicketsAddReadyToMarkLocates', null)
insert into @NewRights values ('Tickets - Assign New Locates to Ticket Locator', 'TicketsAssignNewLocatesToTicketLocator', null)
insert into @NewRights values ('Tickets - Edit Closed Locates', 'TicketsEditClosedLocates', null)
insert into @NewRights values ('Tickets - View Another Locator''s Locates', 'TicketsViewAnotherLocatorsLocates', null)
insert into @NewRights values ('Tickets - Edit Another Locator''s Locates', 'TicketsEditAnotherLocatorsLocates', null)
insert into @NewRights values ('Tickets - Edit Another Locator''s Hours/Units', 'TicketsEditAnotherLocatorsUnits', null)
insert into @NewRights values ('Tickets - Show Unacked Tickets', 'TicketsShowUnacked', null)
insert into @NewRights values ('Sync - Auto-sync on First Login', 'SyncAutoSyncFirstLogin', null)
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  select description, 'General', code, IsNull(mod_desc, 'Limitation does not apply to this right.')
    from @NewRights
    where not exists (select right_id from right_definition where entity_data = code)

-- Add MGR to the modifier for employee types QM previously treated as Managers
update reference
  set modifier = case when (modifier is null) or (modifier='') then 'MGR' 
    else modifier + ',MGR' end
  where (type = 'emptype') 
    and (ref_id in (5,6,7,8,9,10)) -- Codes: 'CORP', 'SD', 'DIR', 'MGR', 'SS', 'SUP', 'OF'
    and ((modifier not like '%MGR%') or (modifier is null))

-- Add CC to the modifier for employee types QM previously treated as Claims Coordinators
update reference
  set modifier = case when (modifier is null) or (modifier='') then 'CC' 
    else modifier + ',CC' end
  where (type = 'emptype') 
    and (code = 'CC')
    and ((modifier not like '%CC%') or (modifier is null))

-- Add OFF to the modifier for employee types QM previously treated as Office Employees
update reference
  set modifier = case when (modifier is null) or (modifier='') then 'OFF' 
    else modifier + ',OFF' end
  where (type = 'emptype') 
    and (ref_id in (10, 577))
    and ((modifier not like '%OFF%') or (modifier is null))

-- Set modifier = LOC for the locator employee type
update reference set modifier = 'LOC' where (type = 'emptype') and (code = 'LOC')

-- Grant emps that used to have unacked emergency ticket nodes based on their type the new Right to show them.
declare @RightShowUnackedTickets integer
set @RightShowUnackedTickets = (select right_id from right_definition where entity_data = 'TicketsShowUnacked')

insert into employee_right (emp_id, right_id, allowed)
  select emp_id, @RightShowUnackedTickets, 'Y'
    from employee
    where (active = 1) and (type_id in (8, 9, 10)) -- Codes: 'SS','SUP','MGR'
      and not exists (select er.emp_id from employee_right er where er.emp_id = emp_id and er.right_id = @RightShowUnackedTickets)

/* *** Also be sure to apply these updated stored procedures:
  get_hier.sql
  get_report_hier.sql
  get_report_hier2.sql
  get_report_hier3.sql
  hier_display.sql
  multi_open_totals.sql
  multi_open_totals2.sql
  RPT_tse_totals.sql
  RPT_productivity_pc.sql
*** */
