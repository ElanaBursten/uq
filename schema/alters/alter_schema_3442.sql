if not exists (select * from information_schema.tables 
  where table_name = 'incoming_item') begin
  create table incoming_item (
    item_id integer identity primary key,
    cc_code varchar(20) not null,
    item_type integer null, -- 1 = ticket, 2 = audit, 3 = message
    item_text varchar(max) not null,
    insert_date datetime not null default getdate()
  )
  
  alter table incoming_item
    add constraint incoming_item_fk_cc_code
    foreign key (cc_code) references call_center (cc_code) not for replication 
    
  alter table incoming_item
    add constraint incoming_item_check_item_text_length
    check (datalength(item_text) <= 50000)    
end
go

/* to rollback this change:

drop table incoming_item

*/
