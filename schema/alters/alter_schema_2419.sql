-- Schema changes for 2419, 2507, & 2508:

-- add new columns to timesheet_entry (for #2419)
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'timesheet_entry' and column_name = 'lunch_start')
    alter table timesheet_entry add 
      lunch_start datetime null, /* one of the work_stop# times */
      source varchar(10) null    /* entry, clock */
go

-- Add new TimesheetsClockEntry right if it doesn't exist (for #2419)
if not exists (select right_id from right_definition where entity_data = 'TimesheetsClockEntry')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Timesheets - Clock Entry', 'General', 'TimesheetsClockEntry', 
    'Limitation does not apply to this right.')

-- Add new WorkBeforeClockIn right if it doesn't exist (for #2507)
if not exists (select right_id from right_definition where entity_data = 'WorkBeforeClockIn')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Restrict Usage - Use Before Clock In', 'General', 'WorkBeforeClockIn', 
    'Limitation does not apply to this right.')
-- Add new WorkDuringLunchBreak right if it doesn't exist (for #2507)
if not exists (select right_id from right_definition where entity_data = 'WorkDuringLunchBreak')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Restrict Usage - Use During Lunch Break', 'General', 'WorkDuringLunchBreak', 
    'Limitation does not apply to this right.')

-- Add new emloyee activity type for lunch break exceptions (for #2507)
if not exists (select * from reference where type='empact' and code='TELUNCHEX')
   insert into reference (type, code, description) values ('empact', 'TELUNCHEX', 'Lunch Break Exception')

-- Add new WorkAfterWorkClockOut right if it doesn't exist (for #2508)
if not exists (select right_id from right_definition where entity_data = 'WorkAfterWorkClockOut')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Restrict Usage - Use After Work Clock Out', 'General', 'WorkAfterWorkClockOut', 
    'Limitation does not apply to this right.')
