--QMANTWO-616 New Column: parent_client_id
--  For Project Tickets where we want a child client defined to assign to team members



IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS 
               where TABLE_NAME = 'client' 
			   and COLUMN_NAME = 'parent_client_id')
ALTER table client
add parent_client_id int NULL

-------------------------------------------------------------------------------------------
--QMANTWO-616_Alter_Schema Project Multi Locators	
	
if not Exists(select * from right_definition WHERE entity_data = 'MultiLocatorsForProjTicket')

INSERT INTO right_definition
           (right_description,right_type,entity_data,parent_right_id,modifier_desc)
     VALUES
           ('Tickets - Project Multi-locators','General','MultiLocatorsForProjTicket',NULL,
           'Limitation is Employee Hierarchy level as list of available locators')
GO
-------------------------------------------------------------------------------------------
--Select * from right_definition WHERE entity_data = 'MultiLocatorsForProjTicket'  65754
--Select * from right_definition order by right_description

-- Select * from Client



