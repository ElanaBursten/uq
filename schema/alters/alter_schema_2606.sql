--Add new right allowing a user to set the Tickets - Show Future Work permission on the Ticket Mgt screen.
if not exists (select * from right_definition where entity_data = 'TicketsManageFutureTickets')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('TicketsManageFutureTickets', 'Tickets - Manage Future Tickets', 'General', 'Limitation does not apply to this right.')
go

--Add new flag to the employee table; used to permit a user to see future work (tickets due after today)
-- employee table updates:
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'employee' and column_name = 'show_future_tickets')
    alter table employee
      add show_future_tickets bit NOT NULL default 1
go
-- employee_hist table updates:
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'employee_history' and column_name = 'show_future_tickets')
  alter table employee_history
    add show_future_tickets bit NULL
go


/* Update employee_iu trigger definition for the new show_future_tickets column */
drop trigger dbo.employee_iu
go

create trigger dbo.employee_iu on employee
for insert, update
as
begin
  set nocount on
  
  declare @ChangeDate datetime
  set @ChangeDate = getdate()

  -- Don't update modified date or add history for rights_midified_date changes alone
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(ultra_user) or update(dialup_user)
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)))
  begin
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)

    if update(report_to) or update(repr_pc_code) 
    begin -- This change may modify the effective PC of non-updated employees
      -- Recalculate and store any changes to everyone below these employees

      declare @AllEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY,
        x integer,
        emp_pc_code varchar(15) NULL
      )

      insert into @AllEmps select * from employee_pc_data(null)

      declare @ChangedEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY
      )

      insert into @ChangedEmps
        select emps.emp_id
        from @AllEmps emps
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate
        where ((emps.emp_pc_code <> eh.active_pc)
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))
          or emps.emp_id in (select emp_id from inserted)

      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)

      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets
      from employee e
        inner join @AllEmps emps on emps.emp_id = e.emp_id
      where e.emp_id in (select emp_id from @ChangedEmps)
    end
    else -- This change only affects the exact employee(s) being modified
    begin
      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)

      -- Record the new/current employee data
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets
      from inserted
    end
  end
end
go

