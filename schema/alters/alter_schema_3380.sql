-- Add Floating Holiday Incentive Type
if not exists (select * from reference where type = 'incpaytype' and code = 'FHL')
  insert into reference(type, code, description, active_ind) values ('incpaytype', 'FHL', 'Floating Holiday', 1);
go

if not exists(select * from dbo.sysobjects where id = object_id(N'dbo.employee_pay_incentive_unique'))
  alter table employee_pay_incentive add constraint employee_pay_incentive_unique unique(emp_id, incentive_type)
GO