/* Undo commands:
alter table statuslist drop column allow_locate_units
go
alter table client drop column require_locate_units
go
*/

/* New client field: */
alter table client add require_locate_units bit not null default 0
go

/* New statuslist field: */
alter table statuslist add allow_locate_units bit not null default 0
go

/* Add all status codes that should allow entry of some units here: */
update statuslist	set allow_locate_units = 1 where status_code in ('M', 'H', 'MCX', 'MFC', 'MFO', 'O', 'OH', 'PI', 'PM')

/* Also need to apply the updated sync_3.sql & get_ticket.sql scripts */
