create table alert_ticket_type (
  alert_ticket_type_id integer NOT NULL identity (100000, 1) primary key,
  call_center varchar(10) NOT NULL,
  ticket_type varchar(50) NOT NULL,
  active bit NOT NULL DEFAULT 1
)
go

create table alert_ticket_keyword (
  alert_ticket_keyword_id integer NOT NULL identity (11000, 1) primary key,
  alert_ticket_type_id integer NOT NULL,
  keyword varchar(100) NOT NULL
)
go

create index alert_ticket_keyword_ticket_type on alert_ticket_keyword (alert_ticket_type_id)
go

alter table alert_ticket_keyword
  add constraint alert_ticket_keyword_fk_ticket_type
  foreign key (alert_ticket_type_id) references alert_ticket_type(alert_ticket_type_id)
go
