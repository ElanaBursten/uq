-- Adds the message for the Submit Time Disagree box for all profit centers:
insert into break_rules (pc_code, rule_type, rule_message, button_text)
  select pc_code, 
    'SUBMITDIS', 
    'You have indicated there is a discrepancy with your recorded working hours for this day. Please enter a contact phone number below and a representative from the Human Resources department will contact you to discuss the matter. If you selected this option by error, please press Cancel to return to the acknowledgement page. If you do not have a working phone number, please enter 000-000-0000.',
    '"Send Info to HR",Cancel'
  from profit_center
  order by pc_code
go

/* ============================================================ */
/* Table: break_rules_response - emp response to break rules messages */
/* ============================================================ */
create table break_rules_response (
  response_id integer identity,
  emp_id integer not null,
  rule_id integer not null,
  response_date datetime null,
  response varchar(100) null,     /* Caption of button clicked */
  contact_phone varchar(20) null, /* Employee's contact phone # */
  constraint pk_break_rules_response primary key (response_id)  
)
go
create index break_rules_response_emp_id on break_rules_response (emp_id, response_date)
create index break_rules_response_response_date on break_rules_response (response_date)
create index break_rules_response_rule_id on break_rules_response (rule_id, emp_id)
go

alter table break_rules_response
   add constraint break_rules_response_fk_employee
   foreign key (emp_id) references employee (emp_id)
go

alter table break_rules_response
   add constraint break_rules_response_fk_break_rules
   foreign key (rule_id) references break_rules (rule_id)
go


