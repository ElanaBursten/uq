/* Adds new right allowing locator to send messages to managers up the hierarchy */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Messages - Send to Manager', 'General', 'MessagesSendToManager', 'A comma separated list of extra employee ids the grantee can send messages to.')

/* Adds new right allowing locator to send messages to others managed by their direct supervisor */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Messages - Send to Peers', 'General', 'MessagesSendToPeers', 'A comma separated list of extra employee ids the grantee can send messages to.')
