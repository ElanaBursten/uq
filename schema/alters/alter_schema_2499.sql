-- Add new Ticket Status code rights if they don't exist
if not exists (select right_id from right_definition where entity_data = 'TicketsAllowStatusCodes')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Tickets - Allow Status Codes', 'General', 'TicketsAllowStatusCodes', 
    'Limitation does not apply to this right.')
    
if not exists (select right_id from right_definition where entity_data = 'TicketsAllowRestrictedStatusCodes')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Tickets - Allow Restricted Status Codes', 'General', 'TicketsAllowRestrictedStatusCodes', 
    'Limitation does not apply to this right.')    

-- add new column to status_group
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'status_group' and column_name = 'right_id')
    alter table status_group add 
      right_id int null 

if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE where constraint_name = 'status_group_fk_right_definition')
  ALTER TABLE status_group
    ADD CONSTRAINT status_group_fk_right_definition
    FOREIGN KEY (right_id) REFERENCES right_definition(right_id)
go
  
-- Assign rights to the new status groups; status group right assignment will be read-only for now.
if not exists (select * from status_group where sg_name = 'UnrestrictedStatusList')
  insert into status_group (sg_name, right_id)
    values ('UnrestrictedStatusList', (select right_id from right_definition where entity_data = 'TicketsAllowStatusCodes') )
    
if not exists (select * from status_group where sg_name = 'RestrictedStatusList')
  insert into status_group (sg_name, right_id)
    values ('RestrictedStatusList', (select right_id from right_definition where entity_data = 'TicketsAllowRestrictedStatusCodes') )    
  
go
