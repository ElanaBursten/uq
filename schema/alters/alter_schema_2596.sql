-- 2569 - Add reference information used by employee activty report

if not exists (select * from reference where type='locstat' and code='NA')
  insert into reference (type, code, description, sortby) values ('locstat','NA','', 3)

if not exists (select * from reference where type='workstat' and code='NA')
  insert into reference (type, code, description, sortby) values ('workstat','NA','', 3)

go
