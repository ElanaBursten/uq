IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.users_client_fk_users') AND parent_object_id = OBJECT_ID(N'dbo.users_client'))
  ALTER TABLE users_client WITH CHECK ADD CONSTRAINT users_client_fk_users FOREIGN KEY(uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_fk_users_create') AND parent_object_id = OBJECT_ID(N'dbo.employee'))
  ALTER TABLE employee WITH CHECK ADD CONSTRAINT employee_fk_users_create FOREIGN KEY(create_uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_fk_users_modified') AND parent_object_id = OBJECT_ID(N'dbo.employee'))
  ALTER TABLE employee WITH CHECK ADD CONSTRAINT employee_fk_users_modified FOREIGN KEY(modified_uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_history_fk_users_create') AND parent_object_id = OBJECT_ID(N'dbo.employee_history'))
  ALTER TABLE employee_history WITH CHECK ADD CONSTRAINT employee_history_fk_users_create FOREIGN KEY(create_uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_history_fk_users_modified') AND parent_object_id = OBJECT_ID(N'dbo.employee_history'))
  ALTER TABLE employee_history WITH CHECK ADD CONSTRAINT employee_history_fk_users_modified FOREIGN KEY(modified_uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.asset_assignment_history_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.asset_assignment_history'))
  ALTER TABLE asset_assignment_history WITH CHECK ADD CONSTRAINT asset_assignment_history_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.computer_info_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.computer_info'))
  ALTER TABLE computer_info WITH CHECK ADD CONSTRAINT computer_info_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.damage_invoice_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.damage_invoice'))
  ALTER TABLE damage_invoice WITH CHECK ADD CONSTRAINT damage_invoice_fk_employee FOREIGN KEY(satisfied_by_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_group_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.employee_group'))
  ALTER TABLE employee_group WITH CHECK ADD CONSTRAINT employee_group_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_history_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.employee_history'))
  ALTER TABLE employee_history WITH CHECK ADD CONSTRAINT employee_history_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.error_report_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.error_report'))
  ALTER TABLE error_report WITH CHECK ADD CONSTRAINT error_report_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.exported_employees_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.exported_employees'))
  ALTER TABLE exported_employees WITH CHECK ADD CONSTRAINT exported_employees_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.locate_hours_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.locate_hours'))
  ALTER TABLE locate_hours WITH CHECK ADD CONSTRAINT locate_hours_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.notification_email_user_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.notification_email_user'))
  ALTER TABLE notification_email_user WITH CHECK ADD CONSTRAINT notification_email_user_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.profit_center_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.profit_center'))
  ALTER TABLE profit_center WITH CHECK ADD CONSTRAINT profit_center_fk_employee FOREIGN KEY(manager_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.sync_log_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.sync_log'))
  ALTER TABLE sync_log WITH CHECK ADD CONSTRAINT sync_log_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.timesheet_entry_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.timesheet_entry'))
  ALTER TABLE timesheet_entry WITH CHECK ADD CONSTRAINT timesheet_entry_fk_employee FOREIGN KEY(work_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.employee_work_goal_fk_employee') AND parent_object_id = OBJECT_ID(N'dbo.employee_work_goal'))
  ALTER TABLE employee_work_goal WITH CHECK ADD CONSTRAINT employee_work_goal_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO