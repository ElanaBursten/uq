if not exists (select name from configuration_data where name = 'PTOHoursEntryStartDate')
begin
  insert into configuration_data(name, value, editable)
  values ('PTOHoursEntryStartDate', '', 1)
end
go

delete from reference where type = 'ptohrs'

insert into reference (type, description, active_ind)
  values ('ptohrs', '0', 1)
insert into reference (type, description, active_ind)
  values ('ptohrs', '4', 1)
insert into reference (type, description, active_ind)
  values ('ptohrs', '8', 1)
  
if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.CHECK_CONSTRAINTS where CONSTRAINT_NAME='timesheet_entry_check_pto_vac_leave_hours')
  alter table timesheet_entry with check add constraint timesheet_entry_check_pto_vac_leave_hours
    check (
      (IsNull(pto_hours, 0.0) = 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) = 0.0) or
      (IsNull(pto_hours, 0.0) > 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) = 0.0) or
      (IsNull(pto_hours, 0.0) = 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) > 0.0))
go
   
/* To rollback these changes:

Prior to deploying the updated sync_6.sql script, use SQL Server 
Management Studio (or equivalent tool) to export a copy of the sync_6
stored procedure. Deploy that exported script in case you need to rollback the 3207
changes.
 
-- remove the new configuration data row
delete configuration_data where name='PTOHoursEntryStartDate'

-- remove the new reference rows
delete from reference where type = 'ptohrs'

-- remove the new constraint on timesheet_entry
alter table timesheet_entry drop constraint timesheet_entry_check_pto_vac_leave_hours
*/