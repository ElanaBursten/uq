-- Add column to billing_rate table to store line item text for additional feet/units
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_rate' and COLUMN_NAME='additional_line_item_text')
  alter table billing_rate add additional_line_item_text varchar(60) null
go

-- Add additional_line_item_text column to billing_rate_history table.
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_rate_history' and COLUMN_NAME='additional_line_item_text')
  alter table billing_rate_history add additional_line_item_text varchar(60) null
go

-- Add billing_detail.additional_line_item_text
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_detail' and COLUMN_NAME='additional_line_item_text')
  alter table billing_detail add additional_line_item_text varchar(60) null
go
