-- add Asset Tag to computer_info table
if not exists (select * from INFORMATION_SCHEMA.COLUMNS 
  where table_name = 'computer_info' and column_name = 'asset_tag')
    ALTER TABLE computer_info add asset_tag varchar(30) NULL
go
