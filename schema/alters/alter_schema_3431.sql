IF NOT EXISTS (Select * from work_order_work_type where work_type = 'NOAOC')
  INSERT INTO work_order_work_type (work_type, work_description, active) VALUES ('NOAOC', 'No AOC Observed', 1)
GO