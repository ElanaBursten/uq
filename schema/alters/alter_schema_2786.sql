if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order' and COLUMN_NAME = 'call_date')
  alter table work_order add call_date datetime null
go

