declare @due_date datetime,
		@locate int,
		@locator int,
		@added_by int

declare AssignmentCursor cursor local for
    select t.due_date, a.locate_id, a.locator_id, a.added_by
	from ticket t
		join locate l on l.ticket_id = t.ticket_id
		join assignment a on a.locate_id = l.locate_id
	where t.status like 'MANUAL%' and
		t.due_date is not null and
		l.closed = 0 and
		l.active = 1 and
		a.active = 1 and
		a.workload_date is null
		
open AssignmentCursor

fetch next from AssignmentCursor into @due_date,	@locate, @locator, @added_by

while @@FETCH_STATUS = 0 BEGIN

    exec assign_locate @locate, @locator, @added_by, @due_date

    fetch next from AssignmentCursor into @due_date,	@locate, @locator, @added_by
END

close AssignmentCursor
deallocate AssignmentCursor
