/* QMAN-3374 also requires the updated get_wgl_work_order_status stored proc be deployed */

/* Drop obsolete get_wgl_work_order_remedy. The new get_wgl_work_order_status gets 
   remediation work without this extra stored proc. */
if object_id('dbo.get_wgl_work_order_remedy') is not null
  drop procedure dbo.get_wgl_work_order_remedy
go

/* Insert new WGL_INSP_ResponseList status group and its statuses */
declare @RespondableStatusList varchar(40)
set @RespondableStatusList = 'WGL_INSP_ResponseList'
if not exists (select * from status_group where sg_name = @RespondableStatusList) begin
  insert into status_group (sg_name, active) values (@RespondableStatusList, 1)
  insert into status_group_item (sg_id, status, active) 
    select sg_id, 'CGA', 1  from status_group where sg_name = @RespondableStatusList
  insert into status_group_item (sg_id, status, active) 
    select sg_id, 'DONE', 1 from status_group where sg_name = @RespondableStatusList
end
go
