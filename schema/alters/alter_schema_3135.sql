if exists(select * from sys.key_constraints where name = 'pk_response_ack_context')
    alter table response_ack_context
    drop constraint pk_response_ack_context;

alter table response_ack_context
add constraint pk_response_ack_context primary key (call_center,
filename, locate_id);