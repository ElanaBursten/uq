/*
 Script to make all db changes required for Mantis issues #1284 & 1307
*/

-- **** NOTE: Change this to 0 before running in production
declare @UQDamageIDPrefix int
set @UQDamageIDPrefix = 99000000

-- #1284 table changes
alter table statuslist
  add mark_type_required bit not null default 0,
  default_mark_type varchar(10) null
GO
alter table locate
  add mark_type varchar(10) null
GO
alter table locate_status
  add mark_type varchar(10) null
GO
-- #1284 reference data
insert into reference (type, code, description, sortby) values ('marktype', 'NEW MARK', 'New mark', 1)
insert into reference (type, code, description, sortby) values ('marktype', 'NO MARK',  'No mark',  2)
insert into reference (type, code, description, sortby) values ('marktype', 'RE-MARK',  'Re-mark',  3)
GO

-- #1307 table changes
alter table damage
  add uq_damage_id int null
GO

setuser 'no_trigger'
update damage set uq_damage_id = damage_id + @UQDamageIDPrefix where uq_damage_id is null
GO
setuser

create unique index damage_uq_damage_id on damage(uq_damage_id)
GO
-- #1307 data
insert into configuration_data (name, value, editable) select 'NextDamageID', max(uq_damage_id), 0 from damage
GO

-- #1284 Updated procs & triggers

-- New locate trigger
if exists(select name from sysobjects where name = 'locate_iu' and type = 'TR')
  drop trigger locate_iu
GO

create trigger locate_iu on locate
for insert, update
as
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- exit if only the invoiced column was changed
  declare @invoiced_col int
  set @invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'invoiced')

  -- NOTE: "@invoiced_col % 8" would not work if the column number is 16, as it is
  -- for assigned_to, so it was changed to "- 8"
  if (@invoiced_col < 9) or (@invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- exit if the assigned_to column was changed
  -- and none of the initial fields changed
  declare @assignedto_col int
  set @assignedto_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to')

  if (@assignedto_col < 9) or (@assignedto_col > 16)
  begin
    rollback transaction
    RAISERROR('assigned_to has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@assignedto_col - 8)-1)))
    return

  -- exit if the assigned_to_id column was changed
  -- and none of the initail fields changed
  declare @assignedtoid_col int
  set @assignedtoid_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to_id')

  if (@assignedtoid_col < 17) or (@assignedto_col > 24)
  begin
    rollback transaction
    RAISERROR('assigned_to_id has moved out of the third column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 3, 1) = power(2, ((@assignedtoid_col - 16)-1)))
    return

  -- if both assigned_to and assigned_to_id changed, either of the
  -- above will return

  -- set the ticket's kind
  DECLARE @ticket_id int
  SELECT @ticket_id = ticket_id FROM inserted

  EXEC dbo.update_ticket_status @ticket_id

  -- Update the modified_date when a locate changes
  update locate set
    modified_date = getdate()
  where locate_id in (select locate_id from inserted)

  -- When a locate is re-opened, refresh the assigned_to field
  update locate
    set assigned_to = (
      select top 1 assignment.locator_id
      from assignment
      where
        (assignment.locate_id = locate.locate_id) and
        (assignment.active = 1)
    )
  from deleted -- contains rows that were just updated, but before the updates were applied
    inner join inserted
      on inserted.locate_id = deleted.locate_id
  where locate.locate_id = deleted.locate_id
    and deleted.closed = 1
    and inserted.closed = 0

  -- Set assigned_to = NULL for all closed/non-active locates
  update locate
    set assigned_to = NULL
    where locate_id in (
      select locate_id
      from inserted
      where closed = 1
       or status = '-P'
       or status = '-N'
       or active = 0
    )

  insert into locate_status (
    locate_id, status_date, status, high_profile, high_profile_reason,
    qty_marked, statused_by,  statused_how, regular_hours, overtime_hours, mark_type)
  select
    locate_id, closed_date, status, high_profile, high_profile_reason,
    qty_marked, closed_by_id, closed_how, regular_hours, overtime_hours, mark_type
  from inserted
  where closed_date is not null
   and status <> '-N'
   and status <> '-P'

  insert into responder_queue (locate_id, ticket_format, client_code)
  select inserted.locate_id, ticket.ticket_format, inserted.client_code
  from inserted
    inner join ticket on inserted.ticket_id=ticket.ticket_id
  where inserted.status <> '-N'
   and inserted.status <> '-R'
   and inserted.status <> '-P'
end
GO

-- New sync_3 sp
if object_id('dbo.sync_3') is not null
  drop procedure dbo.sync_3
GO

create proc sync_3 (
  @EmployeeID int,
  @UpdatesSinceS varchar(30),
  @ClientTicketListS text
)
as
  set nocount on

  DECLARE @tix TABLE (ticket_id integer not null primary key)
  DECLARE @modtix TABLE (ticket_id integer not null primary key)
  DECLARE @closed_since datetime
  DECLARE @oldest_l int
  DECLARE @UpdatesSince datetime

  DECLARE @client_has_tix TABLE (client_ticket_id integer not null primary key)
  DECLARE @should_have_tix TABLE (should_ticket_id integer not null primary key)

  set @UpdatesSince = convert(datetime, @UpdatesSinceS)

  -- Go back 2 minutes, to reduce update overlap errors
  set @UpdatesSince = dateadd(s, -120, @UpdatesSince)

/* Use this in emergency situations to disable initial syncs
  if @UpdatesSince < '2000-01-01'
  begin
    RAISERROR ('Initial syncs temporarily disabled', 50005, 1) with log
    return
  end
*/

  select 'row' as tname, getdate() as SyncDateTime

  select 'office' as tname, * from office
    where modified_date > @UpdatesSince

  select 'call_center' as tname, cc_code, cc_name, uses_wp, active from call_center
   where modified_date > @UpdatesSince

  select 'statuslist' as tname, * from statuslist where modified_date > @UpdatesSince

  select 'reference' as tname, * from reference where modified_date > @UpdatesSince

  select 'profit_center' as tname, * from profit_center where modified_date > @UpdatesSince

  select 'customer' as tname, * from customer where modified_date > @UpdatesSince

  select 'center_group' as tname, * from center_group where modified_date > @UpdatesSince

  select 'center_group_detail' as tname, * from center_group_detail where modified_date > @UpdatesSince

  select 'client' as tname, * from client where modified_date > @UpdatesSince

  select 'employee' as tname, emp_id, type_id, status_id, timesheet_id,
    emp_number, short_name, report_to, active, company_car, timerule_id,
    repr_pc_code, payroll_pc_code
   from employee where modified_date > @UpdatesSince

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, login_id, chg_pwd,
    last_login, active_ind, can_view_notes, can_view_history, chg_pwd_date
   from users where emp_id = @EmployeeID

  select 'carrier' as tname, * from carrier
   where modified_date > @UpdatesSince

  select 'vehicle_type' as tname, * from vehicle_type
   where modified_date > @UpdatesSince

  select 'followup_ticket_type' as tname, * from followup_ticket_type
   where modified_date > @UpdatesSince

  select 'employee' as tname, emp_id, charge_cov, timerule_id,
    timesheet_id, company_car from employee
   where emp_id = @EmployeeID and modified_date > @UpdatesSince

  select 'employee_right' as tname, * from employee_right
    where emp_id=@EmployeeID and modified_date > @UpdatesSince

  select 'vehicle_use' as tname, * from vehicle_use
    where modified_date > @UpdatesSince and emp_id = @EmployeeID

 -- Non-ticket-related data to grab
  select 'timesheet' as tname, timesheet.*, timesheet_detail.*
  from timesheet
    inner join timesheet_detail
      on timesheet.timesheet_id = timesheet_detail.timesheet_id
  where (timesheet.modified_date > @UpdatesSince) and (timesheet.end_date > dateadd(Day, -38, getdate()))
    and timesheet.emp_id = @EmployeeID

  select 'timesheet_entry' as tname, *
  from timesheet_entry
  where (modified_date > @UpdatesSince) and (work_date > dateadd(Day, -38, getdate()))
    and work_emp_id = @EmployeeID

  select 'asset_type' as tname, * from asset_type
  where modified_date > @UpdatesSince

  select 'asset_assignment' as tname, * from asset_assignment
  where emp_id = @EmployeeID
    and modified_date > @UpdatesSince

  select 'asset' as asset, * from asset
  where asset_id in (select asset_id from asset_assignment where emp_id = @EmployeeID)
    and modified_date > @UpdatesSince

  select 'upload_location' as tname, * from upload_location
  where modified_date > @UpdatesSince

  select 'damage_default_est' as tname, * from damage_default_est
  where modified_date > @UpdatesSince

  select 'status_group' as tname, * from status_group
  where modified_date > @UpdatesSince

  select 'status_group_item' as tname, * from status_group_item
  where modified_date > @UpdatesSince

  select 'call_center_hp' as tname, * from call_center_hp
  where modified_date > @UpdatesSince

  select 'billing_unit_conversion' as tname, * from billing_unit_conversion
  where modified_date > @UpdatesSince

  select 'area' as tname, * from area
  where map_id = (select value from configuration_data where name = 'AssignableAreaMapID')

  select 'billing_output_config' as tname, output_config_id, customer_id, which_invoice
  from billing_output_config

  -- Ticket-related data
  select @closed_since = dateadd(day, -5, getdate())

  select @oldest_l = (select top 1 oldest_open_locate from oldest_open)

  -- all open locates, so client can detect it has missing locates
  select 'open_locate' as tname, ticket_id, locate_id, status
   from locate open_locate
   where assigned_to=@EmployeeID

  -- all open tickets, so we can be sure to send them all
  insert into @should_have_tix
  select distinct ticket_id
   from locate
   where assigned_to=@EmployeeID

  -- what the client does have
  insert into @client_has_tix
  select ID from IdListToTable(@ClientTicketListS)

  -- for some reason, some old clients suspiciously always report
  -- 2 tickets in cache
  if @@ROWCOUNT<1 or @UpdatesSince < '2000-01-01'  begin    -- support old clients and init syncs
    if @UpdatesSince < '2000-01-01'  begin
      -- Get all tickets assigned to me even if closed
      insert into @tix  (ticket_id)
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date> @closed_since
      union
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date is null
      union
      select should_ticket_id from @should_have_tix
    end else begin
      -- This is a subsequent sync with the client tic list missing,
      -- must use old alg to get consistent results:
      -- Get all tickets ever assigned to me, this is the SLOW part
      insert into @tix  (ticket_id)
      select distinct locate.ticket_id
       from assignment
       inner join locate on locate.locate_id=assignment.locate_id
          and (locate.closed=0 or locate.closed_date> @closed_since)
       where assignment.locator_id = @EmployeeID
        and assignment.locate_id >= @oldest_l
    end
  end else begin  -- newer clients tell us what they have
    -- they need to know about updates to tickets they are
    -- assigned now, and also tickets still in cache
    insert into @tix  (ticket_id)
      select should_ticket_id from @should_have_tix
      union
      select client_ticket_id from @client_has_tix
  end

  -- subtract what we have, from what we should have
  delete from @should_have_tix where should_ticket_id in
    (select client_ticket_id from @client_has_tix)

  -- Find those that have changed, somewhat slow
  insert into @modtix (ticket_id)
  select t.ticket_id
   from @tix t
    inner join ticket on ticket.ticket_id = t.ticket_id
    and ticket.modified_date >@UpdatesSince
  union
  select locate.ticket_id
   from @tix t2
    inner join locate on locate.ticket_id = t2.ticket_id
   where locate.modified_date > @UpdatesSince
    and locate.status<>'-N'
  union
  select locate.ticket_id
   from @tix t3
    inner join locate on locate.ticket_id = t3.ticket_id
    inner join assignment on locate.locate_id=assignment.locate_id
   where assignment.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @tix t4
    inner join notes on notes.foreign_id = t4.ticket_id
   where notes.foreign_type = 1
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @tix t5
    inner join attachment on attachment.foreign_id = t5.ticket_id
   where attachment.foreign_type = 1
    and attachment.modified_date > @UpdatesSince
  union
    select should_ticket_id from @should_have_tix

  -- now get the whole ticket for just the ones that have changed
  declare @tickets_to_send table (
  [ticket_id] [int] NOT NULL ,
  [ticket_number] [varchar] (20) NOT NULL ,
  [parsed_ok] [bit] NOT NULL ,
  [ticket_format] [varchar] (20) NOT NULL ,
  [kind] [varchar] (20) NOT NULL ,
  [status] [varchar] (20) NULL ,
  [map_page] [varchar] (20) NULL ,
  [revision] [varchar] (20) NOT NULL ,
  [transmit_date] [datetime] NULL ,
  [due_date] [datetime] NULL ,
  [work_description] [varchar] (3500) NULL ,
  [work_state] [varchar] (2) NULL ,
  [work_county] [varchar] (40) NULL ,
  [work_city] [varchar] (40) NULL ,
  [work_address_number] [varchar] (10) NULL ,
  [work_address_number_2] [varchar] (10) NULL ,
  [work_address_street] [varchar] (60) NULL ,
  [work_cross] [varchar] (100) NULL ,
  [work_subdivision] [varchar] (70) NULL ,
  [work_type] [varchar] (90) NULL ,
  [work_date] [datetime] NULL ,
  [work_notc] [varchar] (40) NULL ,
  [work_remarks] [varchar] (1200) NULL ,
  [priority] [varchar] (40) NULL ,
  [legal_date] [datetime] NULL ,
  [legal_good_thru] [datetime] NULL ,
  [legal_restake] [varchar] (40) NULL ,
  [respond_date] [datetime] NULL ,
  [duration] [varchar] (40) NULL ,
  [company] [varchar] (80) NULL ,
  [con_type] [varchar] (50) NULL ,
  [con_name] [varchar] (50) NULL ,
  [con_address] [varchar] (50) NULL ,
  [con_city] [varchar] (40) NULL ,
  [con_state] [varchar] (40) NULL ,
  [con_zip] [varchar] (40) NULL ,
  [call_date] [datetime] NULL ,
  [caller] [varchar] (50) NULL ,
  [caller_contact] [varchar] (50) NULL ,
  [caller_phone] [varchar] (40) NULL ,
  [caller_cellular] [varchar] (40) NULL ,
  [caller_fax] [varchar] (40) NULL ,
  [caller_altcontact] [varchar] (40) NULL ,
  [caller_altphone] [varchar] (40) NULL ,
  [caller_email] [varchar] (40) NULL ,
  [operator] [varchar] (40) NULL ,
  [channel] [varchar] (40) NULL ,
  [work_lat] [decimal](9, 6) NULL ,
  [work_long] [decimal](9, 6) NULL ,
  [image] [text] NOT NULL ,
  [parse_errors] [text] NULL ,
  [modified_date] [datetime] NOT NULL,
  [active] [bit] NOT NULL,
  [ticket_type] [varchar] (38) NULL ,
  [legal_due_date] [datetime] NULL ,
  [parent_ticket_id] [int] NULL ,
  [do_not_mark_before] [datetime] NULL ,
  [route_area_id] [int] NULL ,
  [watch_and_protect] [bit] NULL,
  [service_area_code] [varchar] (40) NULL ,
  [explosives] [varchar] (20) NULL ,
  [serial_number] [varchar] (40) NULL ,
  [map_ref] [varchar] (60) NULL ,
  [followup_type_id] [int] NULL ,
  [do_not_respond_before] [datetime] NULL ,
  [recv_manager_id] [int] NULL ,
  [ward] [varchar] (10) NULL ,
  route_area_name varchar(50) NULL
  )

  declare @locates_to_send table (
  [locate_id] [int] NOT NULL ,
  [ticket_id] [int] NOT NULL ,
  [client_code] [varchar] (10) NULL ,
  [client_id] [int] NULL ,
  [status] [varchar] (5) NOT NULL ,
  [high_profile] [bit] NOT NULL ,
  [qty_marked] [int] NULL ,
  [price] [money] NULL ,
  [closed] [bit] NOT NULL ,
  [closed_by_id] [int] NULL ,
  [closed_how] [varchar] (10) NULL ,
  [closed_date] [datetime] NULL ,
  [modified_date] [datetime] NOT NULL ,
  [active] [bit] NOT NULL,
  [invoiced] [bit] NULL ,
  [assigned_to] [int] NULL ,
  [regular_hours] [decimal](5, 2) NULL ,
  [overtime_hours] [decimal](5, 2) NULL ,
  [added_by] [varchar] (8) NULL ,
  [watch_and_protect] [bit] NULL ,
  [high_profile_reason] [int] NULL ,
  [seq_number] [varchar] (20) NULL ,
  [assigned_to_id] [int] NULL ,
  [mark_type] [varchar] (10) NULL ,
  locator_id int
  )

  -- gather the ticket and locate data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @tickets_to_send
  select
     ticket.ticket_id,
     ticket.ticket_number,
     ticket.parsed_ok,
     ticket.ticket_format,
     ticket.kind,
     ticket.status,
     ticket.map_page,
     ticket.revision,
     ticket.transmit_date,
     ticket.due_date,
     ticket.work_description,
     ticket.work_state,
     ticket.work_county,
     ticket.work_city,
     ticket.work_address_number,
     ticket.work_address_number_2,
     ticket.work_address_street,
     ticket.work_cross,
     ticket.work_subdivision,
     ticket.work_type,
     ticket.work_date,
     ticket.work_notc,
     ticket.work_remarks,
     ticket.priority,
     ticket.legal_date,
     ticket.legal_good_thru,
     ticket.legal_restake,
     ticket.respond_date,
     ticket.duration,
     ticket.company,
     ticket.con_type,
     ticket.con_name,
     ticket.con_address,
     ticket.con_city,
     ticket.con_state,
     ticket.con_zip,
     ticket.call_date,
     ticket.caller,
     ticket.caller_contact,
     ticket.caller_phone,
     ticket.caller_cellular,
     ticket.caller_fax,
     ticket.caller_altcontact,
     ticket.caller_altphone,
     ticket.caller_email,
     ticket.operator,
     ticket.channel,
     ticket.work_lat,
     ticket.work_long,
     ticket.image,
     ticket.parse_errors,
     ticket.modified_date,
     ticket.active,
     ticket.ticket_type,
     ticket.legal_due_date,
     ticket.parent_ticket_id,
     ticket.do_not_mark_before,
     ticket.route_area_id,
     ticket.watch_and_protect,
     ticket.service_area_code,
     ticket.explosives,
     ticket.serial_number,
     ticket.map_ref,
     ticket.followup_type_id,
     ticket.do_not_respond_before,
     ticket.recv_manager_id,
     ticket.ward,
     (select area_name from area where ticket.route_area_id = area.area_id)
    from @modtix mt
      inner join ticket on mt.ticket_id = ticket.ticket_id

  insert into @locates_to_send
  select locate.*, assignment.locator_id
    from @modtix mt
      inner join locate
        on mt.ticket_id = locate.ticket_id and locate.status <> '-N' and locate.status <> '-P'
      inner join assignment
        on locate.locate_id=assignment.locate_id and assignment.active = 1

  -- Send the ticket and locate data
  select 'ticket' as tname, * from @tickets_to_send ticket

  select 'locate' as tname, * from @locates_to_send locate

  -- Retrieve all notes for any ticket that has been modified in any way, even
  -- if the note has not been modified. This is necessary because the ticket
  -- may be have been reassigned to a different locator.
  select 'notes' as tname, notes.*
   from @modtix mt
   inner join notes on notes.foreign_type = 1 -- 1 = ticket notes
    and mt.ticket_id = notes.foreign_id

  -- All attachments for modified tickets:
  select 'attachment' as tname, attachment.*
   from @modtix mt
   inner join attachment on mt.ticket_id = attachment.foreign_id
    and attachment.foreign_type = 1

  -- All Plats for modified tickets:
  select 'locate_plat' as tname, locate_plat.*
   from @locates_to_send ls
   inner join locate_plat on ls.locate_id = locate_plat.locate_id
GO
grant execute on sync_3 to uqweb, QManagerRole
GO


-- #1307 Updated procs & triggers

-- New damage_i trigger
if object_id('dbo.damage_i') is not null
  drop trigger dbo.damage_i
GO

create trigger dbo.damage_i on damage
for insert
as
  if (select Coalesce(uq_damage_id, 0) from inserted) < 1
  begin
    declare @next_damage_id integer
    execute @next_damage_id = dbo.NextUniqueID 'NextDamageID'
    update damage set uq_damage_id = @next_damage_id, added_by = modified_by
     where (damage_id in (select damage_id from inserted))
  end
  else
    update damage set added_by = modified_by
     where damage_id in (select damage_id from inserted)
GO

-- New acknowledge_damage_ticket
if object_id('dbo.acknowledge_damage_ticket') is not null
  drop procedure dbo.acknowledge_damage_ticket
GO

create procedure acknowledge_damage_ticket(
  @TicketID int,
  @EmpID int,
  @InvestigatorID int)
as
begin transaction
exec acknowledge_ticket @TicketID, @EmpID

declare @OID int
execute get_office_id @TicketID, @OfficeID = @OID output

declare @ProfitCenter varchar(20)
set @ProfitCenter = (select profit_center from office where office_id = @OID)

declare @UQDamageID int
execute @UQDamageID = NextUniqueID 'NextDamageID'

insert into damage (state, city, county, damage_type, excavator_company, ticket_id,
  investigator_id, office_id, profit_center, uq_notified_date, notified_by_person,
  notified_by_company, remarks, page, claim_status, uq_damage_id)
select t.work_state, t.work_city, t.work_county, 'INCOMING', SUBSTRING(t.con_name, 1, 30), t.ticket_id,
  @InvestigatorID, @OID, @ProfitCenter, t.transmit_date, 'Call Center', 'Call Center',
  work_description, map_page, 'OPEN', @UQDamageID
from ticket t
where t.ticket_id = @TicketID

if exists (select damage_id from damage where uq_damage_id = @UQDamageID) begin
  commit transaction
  select @UQDamageID as "DamageID",
    (select short_name from employee where emp_id=@InvestigatorID) as "InvestigatorName"
end
else begin
  rollback transaction
  RAISERROR ('Acknowledged ticket id %d could not create damage id %d.', 16, 1, @TicketID, @UQDamageID)
end
GO
grant execute on acknowledge_damage_ticket to uqweb, QManagerRole
GO

-- New RPT_damageidexception sp
if object_id('dbo.RPT_damageidexception') is not null
  drop procedure dbo.RPT_damageidexception
go

create proc RPT_damageidexception(
  @StartDamageID int,
  @EndDamageID int
)
as

set nocount on

declare @SequenceList table (
  sequence_no int not null
 )

declare @ExceptionDamageIDs table (
  uq_damage_id int not null,
  description varchar(40) not null,
  sort_order int not null
)

declare @current_damage_id int
declare @highest int

set @highest = (select max(uq_damage_id) from damage)

if @EndDamageID=0 or @EndDamageID>@highest
  set @EndDamageID = @highest

set @current_damage_id = @StartDamageID

while (@current_damage_id <= @EndDamageID)
begin
  insert into @SequenceList values (@current_damage_id)
  set @current_damage_id = @current_damage_id + 1
end

insert into @ExceptionDamageIDs values (@StartDamageID, 'Start of Report Range', @StartDamageID-1)
insert into @ExceptionDamageIDs values (@EndDamageID, 'End of Report Range', @EndDamageID+1)

insert into @ExceptionDamageIDs
  select sequence_no, 'Missing', sequence_no
  from @SequenceList left outer join damage on uq_damage_id=sequence_no
  where uq_damage_id is null

select uq_damage_id as damage_id, description from @ExceptionDamageIDs
order by sort_order
GO

grant execute on RPT_damageidexception to uqweb, QManagerRole
GO

-- New RPT_DamagesByLocator sp
if object_id('dbo.RPT_DamagesByLocator') is not null
  drop procedure dbo.RPT_DamagesByLocator
GO

create procedure dbo.RPT_DamagesByLocator (
  @DateFrom datetime,
  @DateTo datetime,
  @ProfitCenter varchar(20),
  @LiabilityType int
)
as

declare @results table (
  damage_idz         integer     not null,
  uq_damage_idz      integer     not null,
  ticket_idz         integer     null,
  ticket_numberz     varchar(25) null,
  damage_date        datetime    null,
  uq_notified_date   datetime    null,
  estimate_date      datetime    null,
  estimate_amount    decimal(12,2) null,
  invoice_amount     datetime    null,
  paid_amount        datetime    null,
  locator_id         integer     null,
  locator_name       varchar(35) null,
  damage_inv_num     varchar(12) null,
  damage_type        varchar(20) null,
  location           varchar(80) null,
  city               varchar(40) null,
  state              varchar(2)  null,
  utility_co_damaged varchar(40) null,
  investigator_id    integer     null,
  investigator_name  varchar(35) null,
  excavator_company  varchar(30) null,
  uq_resp_code       varchar(4)  null,
  uq_resp_type       varchar(4)  null,
  locator_last_name varchar(30) null
)

-- First grab the damages, without any other tables,
-- to prevent SQL Server from deciding to table scan
-- the assignment table.  Duh.

insert into @results (damage_idz, uq_damage_idz, ticket_idz, damage_date,
  uq_notified_date, damage_type, location, city, state,
  utility_co_damaged, investigator_id, excavator_company, uq_resp_code,
  uq_resp_type)
select d.damage_id, d.uq_damage_id, d.ticket_id, d.damage_date,
  d.uq_notified_date, d.damage_type, d.location, d.city, d.state,
  d.utility_co_damaged, d.investigator_id, d.excavator_company, d.uq_resp_code,
  d.uq_resp_type
from damage d
where uq_notified_date >= @DateFrom
  and uq_notified_date <= @DateTo
  and profit_center = @ProfitCenter
  and (
  /* This avoids the need for an IF or for client-side SQL */
  ((uq_resp_code is not null) and @LiabilityType=0) OR
  ((uq_resp_code is null) and @LiabilityType=1) OR
  ((uq_resp_code is not null and exc_resp_code is null) and @LiabilityType=2) OR
  ((uq_resp_code is not null and exc_resp_code is not null) and @LiabilityType=3) OR
  ((uq_resp_code is null and exc_resp_code is null) and @LiabilityType=4) OR
  (@LiabilityType=5)
 )

-- Set the ticket ID and number from the ticket table
update @results
 set ticket_numberz = t.ticket_number
 from ticket t
 where ticket_idz = t.ticket_id

-- Set the locator ID
update @results
 set locator_id=a.locator_id
 from locate l, assignment a
 where l.ticket_id = ticket_idz
   and l.locate_id = a.locate_id
   and a.active = 1
   and l.status <> '-N'

update @results
set investigator_name =
  (select short_name from employee e where e.emp_id = investigator_id)

update @results
set locator_name =
  (select short_name from employee e where e.emp_id = locator_id)

update @results
set locator_last_name =
  (select last_name from employee e where e.emp_id = locator_id)

update @results
set estimate_amount =
  (select amount
   from damage_estimate
   where (damage_estimate.damage_id = damage_idz)
     and (estimate_id =
           (select max(estimate_id)
           from damage_estimate
           where damage_estimate.damage_id = damage_idz)))

select uq_damage_idz as damage_id, ticket_numberz as ticket_number,
  damage_date, uq_notified_date,
  estimate_amount, locator_id, locator_name, location, city, state,
  utility_co_damaged, excavator_company, uq_resp_code, uq_resp_type, damage_type,
  locator_last_name
from @results
order by locator_last_name, damage_id

GO

grant execute on RPT_DamagesByLocator to uqweb, QManagerRole
GO

-- New RPT_DEC sp
if object_id('dbo.RPT_DamageLiabilityChanges3') is not null
  drop procedure dbo.RPT_DamageLiabilityChanges3
go

create procedure dbo.RPT_DamageLiabilityChanges3 (
  @RangeStart    datetime, -- Damage change range [@RangeStart..@RangeEnd)
  @RangeEnd      datetime, -- This is the key range to consider for changes!
  @RecdStart     datetime, -- Damage claim received [@RecdStart..@RecdEnd)
  @RecdEnd       datetime, -- Just a filter, don't use any data as of these dates
  @DmgStart      datetime, -- Damage date
  @DmgEnd        datetime, -- Just a filter, don't use any data as of these dates
  @State         char(2),  -- Just a filter
  @ProfitCenter  varchar(10), -- Just a filter
  @LiabilityTypeStart int,
  @LiabilityTypeEnd int,
  @IncludeZeroUQLiab bit,
  @MinChange integer,
  @ExcludeInitialEst bit
)
as
  set nocount on

  -- Results master table
  declare @results table (
    damage_idz int not null primary key,
    uq_damage_idz int not null,
    -- What section of the report this record belongs in.
    -- Should be one of these for all returned records: NEW, CHANGED, TOUQ, FROMUQ.
    report_category varchar(10) NULL,
    report_category_name varchar(60),     -- to make it easy to show on the client

    -- information about the damage
    damage_date datetime,
    uq_notified_date datetime,
    client_claim_id varchar(25) NULL,
    profit_center varchar(15),
    utility_co_damaged varchar(40),
    facility_type varchar(10) NULL,
    facility_size varchar(10) NULL,
    size_type varchar(50) NULL,
    location varchar(80) NULL,
    city varchar(40) NULL,
    state varchar(2) NULL,
    invoice_code varchar(10) NULL,

    -- These dates are not returned to the caller, just used while getting data:
    start_estimate_date datetime,  -- Might be before the date range
    end_estimate_date datetime, -- Might be before the date range

    -- Responsibility/estimate details at the beginning of the date range
    -- (Liabilities might be different from estimates, depending on who is at fault)
    start_uq_resp_code varchar(10) NOT NULL,
    start_exc_resp_code varchar(10) NOT NULL,
    start_spc_resp_code varchar(10) NOT NULL,

    start_uq_liab decimal(12, 2) default 0,
    start_exc_liab decimal(12, 2) default 0,
    start_spc_liab decimal(12, 2) default 0,

    -- Responsibility/estimate details at the end of the date range
    end_uq_resp_code varchar(10) NOT NULL,
    end_exc_resp_code varchar(10) NOT NULL,
    end_spc_resp_code varchar(10) NOT NULL,

    end_uq_liab decimal(12, 2) default 0,
    end_exc_liab decimal(12, 2) default 0,
    end_spc_liab decimal(12, 2) default 0,

    -- The difference between the start liability and the end liability
    uq_liability_change decimal(12, 2),
    exc_liability_change decimal(12, 2),
    spc_liability_change decimal(12, 2),

    -- The last date the damage was some UQ responsibility value other than the ending value.
    -- Informational output for the "Changed to UQ" and "Changed from UQ" sections.
    last_uq_resp_change_date_in_range datetime,

    -- Development purposes only / not returned to the caller:

    -- These have the estimates at start and end dates, regardless of liability -
    -- this report is about liability, so be careful not to display them anywhere
    start_estimate decimal(12, 2) default 0,
    end_estimate decimal(12, 2) default 0,

    -- The first damage modification date after the date range. If a record exists
    -- for this date, it contains the responsibility values at the end of the range.
    -- If not, the current values are the ending responsibility values.
    first_mod_date_after_range_end datetime,

    -- The first modification of this damage after the start date.  If such a value
    -- exists, it contains the starting responsibility values.  If it does not exist,
    -- The starting responsibility values are null unless the damage was created before
    -- the range began, in which case the current values are also the starting values.
    first_mod_date_after_range_start datetime
  )

  declare @relevant_damages table (
    rd_damage_id int NOT NULL primary key
  )

  insert into @relevant_damages
  select damage_id from damage
    where (uq_notified_date >= @RangeStart and uq_notified_date < @RangeEnd)
     or (damage_date >= @RangeStart and damage_date < @RangeEnd)
  union
  select damage_id from damage_estimate
    where (modified_date >= @RangeStart and modified_date < @RangeEnd)
  union
  select damage_id from damage_history
    where (modified_date >= @RangeStart and modified_date < @RangeEnd)
     and not (modified_date between '2004-03-23T23:00:00' and '2004-03-24T01:00:00')
  union
  select damage_id from damage_invoice
    where damage_id is not null
     and ( (received_date >= @RangeStart and received_date < @RangeEnd)
        or (paid_date >= @RangeStart and paid_date < @RangeEnd) )

  -- Start with all matching damages
  insert into @results (
    damage_idz,  -- These values are never changed by the SQL queries
    uq_damage_idz,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    location,
    city,
    state,
    facility_type,
    facility_size,
    invoice_code,
    start_uq_resp_code,  -- These 4 codes might be overriden later by history values
    start_exc_resp_code,
    start_spc_resp_code,
    end_uq_resp_code,
    end_exc_resp_code,
    end_spc_resp_code)
  select
    damage_id,
    uq_damage_id,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    location,
    city,
    state,
    facility_type,
    facility_size,
    invoice_code,
    coalesce(uq_resp_code, '-'),   -- Transform NULLs in the DB to values
    coalesce(exc_resp_code, '-'),  -- we can deal with here more easily
    coalesce(spc_resp_code, '-'),
    coalesce(uq_resp_code, '-'),
    coalesce(exc_resp_code, '-'),
    coalesce(spc_resp_code, '-')
  from @relevant_damages r
   inner join  damage on r.rd_damage_id = damage.damage_id
  where damage.uq_notified_date >= @RecdStart and damage.uq_notified_date<@RecdEnd
    and damage.damage_date >= @DmgStart and damage.damage_date<@DmgEnd
    and ((@State='') or (damage.state = @State ))
    and ((@ProfitCenter = '') or (damage.profit_center=@ProfitCenter))

  -- All non-liability-related filters have been applied, only liability
  -- filter need to be applied at the end.

  -- Update the added date of the estimate active on the starting date

  update @results
  set start_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = damage_idz and third_party = 0
    and modified_date <= @RangeStart
  )

  -- Update the added date of the estimate active on the ending date
  update @results
  set end_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = damage_idz and third_party = 0
    and modified_date <= @RangeEnd
  )

  -- Update the starting estimate amount
  update @results
  set start_estimate = (
    select max(amount) from damage_estimate
    where damage_id = damage_idz
      and modified_date = start_estimate_date -- modified_date is never null
  )

  -- Update the ending estimate amount
  update @results
  set end_estimate = (
    select max(amount) from damage_estimate
    where damage_id = damage_idz
      and modified_date = end_estimate_date
  )

  -- Update the first modification date after the range start
  update @results
  set first_mod_date_after_range_start = (
    select min(modified_date)
    from damage_history hist
    where (damage_idz = hist.damage_id)
      and (hist.modified_date >= @RangeStart)
  )

  -- Update the first modification date after the range end
  update @results
  set first_mod_date_after_range_end = (
    select min(modified_date)
    from damage_history hist
    where (damage_idz = hist.damage_id)
      and (hist.modified_date >= @RangeEnd)
  )

  -- The responsibility state at the start of the range comes from either the
  -- first history record added after the range start, or the curent values if
  -- no such history record exists.  The curent value is already in place.
  -- TODO: Find a non-code-duplicating way to express the below 4 queries
  update @results
   set start_uq_resp_code = coalesce(uq_resp_code,'-'),
       start_exc_resp_code = coalesce(exc_resp_code,'-'),
       start_spc_resp_code = coalesce(spc_resp_code,'-')
   from damage_history hist
    where damage_idz = hist.damage_id
    and hist.modified_date = first_mod_date_after_range_start

  -- The responsibility state at the end of the range comes from either the
  -- first history record added after the range end, or the curent values if
  -- no such history record exists.  The curent value is already in place.
  update @results
   set end_uq_resp_code = coalesce(uq_resp_code,'-'),
       end_exc_resp_code = coalesce(exc_resp_code,'-'),
       end_spc_resp_code = coalesce(spc_resp_code,'-')
   from damage_history hist
    where damage_idz = hist.damage_id
    and hist.modified_date = first_mod_date_after_range_end

  -- If a damage did not exist at the range start, the starting responsibilities must be none
  update @results
   set start_uq_resp_code = '-',
       start_exc_resp_code = '-',
       start_spc_resp_code = '-'
  where uq_notified_date > @RangeStart

  -- If a damage did not exist at the range end, the ending responsibilities then must be none
  update @results
   set end_uq_resp_code = '-',
       end_exc_resp_code = '-',
       end_spc_resp_code = '-'
  where uq_notified_date > @RangeEnd

  -- The last_uq_resp_change_date_in_range is the last date the damage was some
  -- responsibility value other than the terminal value for the range.
  update @results
  set last_uq_resp_change_date_in_range = (
    select max(hist.modified_date)
    from damage_history hist
    where damage_idz = hist.damage_id
      and hist.modified_date >= @RangeStart and hist.modified_date < @RangeEnd
      and coalesce(hist.uq_resp_code, '-') <> end_uq_resp_code)
    -- getting rid of NULLs up from makes this easier

  -- Calculate the liability amounts.  UQ has no monetary liability unless
  -- they have some fault in the damage (based on the *_uq_resp_code).
  update @results
  set start_uq_liab = coalesce(start_estimate,0)
  where start_uq_resp_code <> '-'

  update @results
  set end_uq_liab = coalesce(end_estimate, 0)
  where end_uq_resp_code <> '-'

  update @results
  set start_exc_liab = coalesce(start_estimate,0)
  where start_exc_resp_code <> '-'
   and start_uq_resp_code = '-'

  update @results
  set end_exc_liab = coalesce(end_estimate, 0)
  where end_exc_resp_code <> '-'
   and end_uq_resp_code = '-'

  update @results
  set start_spc_liab = coalesce(start_estimate,0)
  where start_spc_resp_code <> '-'
   and start_uq_resp_code = '-'
   and start_exc_resp_code = '-'

  update @results
  set end_spc_liab = coalesce(end_estimate, 0)
  where end_spc_resp_code <> '-'
   and end_uq_resp_code = '-'
   and end_exc_resp_code = '-'

  -- Update the difference between the start/end liability (might be negative)
  update @results
  set uq_liability_change = end_uq_liab - start_uq_liab,
      exc_liability_change = end_exc_liab - start_exc_liab,
      spc_liability_change = end_spc_liab - start_spc_liab

  -- note that this will exclude some that have a UQ liability code,
  -- if there are no dollars of liability.
  if @IncludeZeroUQLiab=0
    delete from @results
     where start_uq_liab=0 and end_uq_liab=0

  -- Filter for requested liability types
  -- 0: UQ has liability
  -- 1: UQ has no liability
  -- 2: Only UQ has liability
  -- 3: Both UQ and excavator have liability
  -- 4: Neither UQ nor excavator are liable
  -- 5: No filtering

  delete from @results
  where (
    ((@LiabilityTypeStart = 0) and (start_uq_resp_code='-')) or
    ((@LiabilityTypeStart = 1) and (start_uq_resp_code<>'-')) or
    ((@LiabilityTypeStart = 2) and not (start_uq_resp_code<>'-' and start_exc_resp_code='-')) or
    ((@LiabilityTypeStart = 3) and not (start_uq_resp_code<>'-' and start_exc_resp_code<>'-')) or
    ((@LiabilityTypeStart = 4) and not (start_uq_resp_code='-' and start_exc_resp_code='-' ))
  )

  delete from @results
  where (
    ((@LiabilityTypeEnd = 0) and (end_uq_resp_code='-')) or
    ((@LiabilityTypeEnd = 1) and (end_uq_resp_code<>'-')) or
    ((@LiabilityTypeEnd = 2) and not (end_uq_resp_code<>'-' and end_exc_resp_code='-')) or
    ((@LiabilityTypeEnd = 3) and not (end_uq_resp_code<>'-' and end_exc_resp_code<>'-')) or
    ((@LiabilityTypeEnd = 4) and not (end_uq_resp_code='-' and end_exc_resp_code='-' ))
  )

  -- Apply the minimum change detection

  if @MinChange > 0 begin
    delete from @results
      where abs(uq_liability_change) < @MinChange
        and abs(exc_liability_change) < @MinChange
        and abs(spc_liability_change) < @MinChange
  end

  -- Exlude items that only got an initial estimate
  if @ExcludeInitialEst = 1
    delete from @results
       where start_uq_liab=0
        and damage_idz in  (
          select damage_id
            from @results r
              inner join damage_estimate e
                 on r.damage_idz = e.damage_id
            where (e.modified_date >= @RangeStart and e.modified_date < @RangeEnd)
                and e.active = 1
          group by damage_id having count(damage_id) = 1)

  -- Determine what section of the report this damage goes in.  If none of the 4
  -- classes match, the damage liability has not changed, and can be ignored.

  -- Assume they all go here first; they are only
  -- in @results at all, because they had some activity during the period
  update @results
  set report_category = '7OTHER'

  -- The ones that never were UQ, go on their own page, which is usually
  -- omitted anyway
  update @results
  set report_category = '6NOUQ'
  where start_uq_resp_code = '-'
    and end_uq_resp_code = '-'

  -- Anything that changed, is in this category
  update @results
  set report_category = '2CHANGED'
  where uq_liability_change <> 0

  -- Put them here instead based on the liab change
  update @results
  set report_category = '3TOUQ'
  where uq_notified_date < @RangeStart
    and start_uq_resp_code = '-'
    and end_uq_resp_code <> '-'

  -- Put them here instead based on the liab change
  update @results
  set report_category = '4FROMUQ'
  where uq_notified_date < @RangeStart
    and start_uq_resp_code <> '-'
    and end_uq_resp_code = '-'

  -- Put them here if UQ had liab at both points
  -- and it did not change
  update @results
  set report_category = '5UQBOTH'
  where uq_liability_change = 0
    and start_uq_resp_code <> '-'
    and end_uq_resp_code <> '-'

  -- If they are new in the period, UQ liab, they go here
  update @results
  set report_category = '1NEW'
  where uq_notified_date >= @RangeStart
    and uq_liability_change <> 0

  -- Some sanity checks
  declare @LiabilityChanges int
  declare @ReportRecords int
  declare @NumRecords int

  set @NumRecords = (select count(*) from @results where (end_uq_liab <> 0) and (end_uq_resp_code='-'))
  if (@NumRecords > 0)
    RAISERROR('Liability without responsibility code', 18, 1) with log

  set @NumRecords = (select count(*) from @results where report_category in ('3TOUQ', 'FROMUQ') and (last_uq_resp_change_date_in_range is null))
  if (@NumRecords > 0)
    RAISERROR('TOUQ/FROMUQ without an in-range responsibility change date', 18, 1) with log


  -- set category full names and order field

  update @results set report_category_name='New Damage Claims'
   where report_category = '1NEW'
  update @results set report_category_name='New Estimates for Earlier Damage Claims'
   where report_category = '2CHANGED'
  update @results set report_category_name='Damages Changed To UtiliQuest Responsibility'
   where report_category = '3TOUQ'
  update @results set report_category_name='Damages Changed From UtiliQuest Responsibility'
   where report_category = '4FROMUQ'
  update @results set report_category_name='Damages With Unchanged UtiliQuest Responsibility'
   where report_category = '5UQBOTH'
  update @results set report_category_name='Damages With No UtiliQuest Responsibility'
   where report_category = '6NOUQ'
  update @results set report_category_name='Damages With Other Activity'
   where report_category = '7OTHER'


  -- ***************** Emit the results ********************
  -- Emit the damages
  select  uq_damage_idz as damage_id,
    report_category,

    uq_liability_change,
    exc_liability_change,
    spc_liability_change,
    uq_liability_change as liability_change,  -- support old REs

    start_uq_resp_code, start_exc_resp_code, start_spc_resp_code,
    start_uq_liab, start_exc_liab, start_spc_liab,
    end_uq_resp_code, end_exc_resp_code, end_spc_resp_code,
    end_uq_liab, end_exc_liab, end_spc_liab,

    last_uq_resp_change_date_in_range,

    damage_date, uq_notified_date,
    client_claim_id, profit_center, utility_co_damaged,
    size_type, facility_type, facility_size,
    invoice_code,
    city, state,
    report_category_name,
    case
      when (location <> '') and (city <> '') then
        location + ', ' + city
      when (location <> '') then
        location
      else
        city
    end as location
  from @results
  order by report_category, profit_center, damage_idz
  -- order by damage_idz (useful for debugging)

 -- emit Estimates
  select
    estimate_id,
    uq_damage_idz as damage_id,
    third_party,
    emp_id,
    amount,
    company,
    contract,
    phone,
    comment,
    modified_date,
    active
  from @results r
    inner join damage_estimate e on r.damage_idz = e.damage_id
  where (e.modified_date >= @RangeStart and e.modified_date < @RangeEnd)
     and e.active = 1
  order by e.damage_id, e.modified_date

 -- emit Invoices
  select
    r.uq_damage_idz as damage_id,
    i.invoice_num,
    i.amount,
    i.company,
    i.received_date,
    i.satisfied_date,
    i.comment,
    i.approved_date,
    i.approved_amount,
    i.paid_date,
    i.paid_amount,
    i.invoice_id,
    case when i.received_date >= @RangeStart and i.received_date < @RangeEnd
         then i.amount
         else null end as recieved_amount_in_period,
    case when i.paid_date >= @RangeStart and i.paid_date < @RangeEnd
         then i.paid_amount
         else null end as paid_amount_in_period
  from @results r
    inner join damage_invoice i on r.damage_idz = i.damage_id
  where (i.received_date >= @RangeStart and i.received_date < @RangeEnd)
     or (i.paid_date >= @RangeStart and i.paid_date < @RangeEnd)
  order by i.damage_id, i.received_date, i.paid_date

  -- Include totals for easier debugging and to faciliate
  -- a first or last page summary on the report if needed
  select
    report_category,
    max(report_category_name) as report_category_name,
    count(damage_idz) as N,
    sum(uq_liability_change) as liab_change,
    sum(uq_liability_change) as uq_liab_change,
    sum(exc_liability_change) as exc_liab_change,
    sum(spc_liability_change) as spc_liab_change
   from @results
   group by report_category with rollup
   order by report_category
   -- WITH ROLLUP adds a line with totals

  -- Include totals for easier debugging and to faciliate
  -- a first or last page summary on the report if needed
  select
    profit_center,
    report_category,
    max(report_category_name) as report_category_name,
    count(damage_idz) as N,
    sum(uq_liability_change) as liab_change,
    sum(uq_liability_change) as uq_liab_change,
    sum(exc_liability_change) as exc_liab_change,
    sum(spc_liability_change) as spc_liab_change
   from @results
   group by profit_center, report_category with rollup
   order by profit_center, report_category
   -- WITH ROLLUP adds a line with totals

GO

grant execute on dbo.RPT_DamageLiabilityChanges3 to uqweb, QManagerRole
GO
