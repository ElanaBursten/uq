-- alter_schema_1755.sql
alter table break_rules add button_text varchar(100) null
go
alter table employee_activity add extra_details varchar(20) null
go

/* To undo: 
alter table break_rules drop column button_text
go
alter table employee_activity drop column extra_details
go
*/
