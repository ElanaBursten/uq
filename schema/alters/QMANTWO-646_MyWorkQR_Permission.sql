--QMANTWO-646_Alter_Schema.sql - WhichWaze
if not Exists(select * from right_definition WHERE entity_data = 'WhichWaze')

INSERT INTO right_definition
           (right_description,right_type,entity_data,parent_right_id,modifier_desc)
     VALUES
           ('Tickets - QRScanner','General','WhichWaze',NULL,
           'Requires Q for Qtip or W for Waze Limitation')
GO