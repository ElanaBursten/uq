/* 1769: This script assigns all the new manager rights to the emptypes that used to get them by default */
insert into employee_right (emp_id, right_id, allowed)
select e.emp_id, r.right_id, 'Y' as allowed
from employee e, right_definition r
where type_id in (5,6,7,8,9,10)
 and entity_data in ('TicketsAutoApproveNewTickets',
  'TicketsAssignNewTickets',
  'TicketsAddReadyToMarkLocates',
  'TicketsAssignNewLocatesToTicketLocator',
  'TicketsEditClosedLocates',
  'TicketsViewAnotherLocatorsLocates',
  'TicketsEditAnotherLocatorsLocates',
  'TicketsEditAnotherLocatorsUnits',
  'TicketsShowUnacked',
  'SyncAutoSyncFirstLogin')
 and not exists (select er.emp_id from employee_right er 
   where er.emp_id = e.emp_id and er.right_id = r.right_id)

