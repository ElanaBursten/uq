-- Set reference.modifier to 'SYS' for employee activities that rely on Windows
-- API for their timestamp so they can be flagged on the Emp Activity Report.
update reference set modifier = 'SYS' 
where modifier <> 'SYS' and type = 'empact' 
  and code in ('SLEEP','WAKE','WINON','WINOFF','WINLCK','WINULK','WINCON','WINDIS','PWROFF','PWRON')
