/*
  Add a time zone column to the call center table and populate some time zones in the 
  reference table.
*/
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'call_center' and COLUMN_NAME='time_zone')
  ALTER TABLE call_center ADD time_zone int null
  ALTER TABLE call_center ADD CONSTRAINT call_center_tz_fk_reference FOREIGN KEY (time_zone) REFERENCES reference (ref_id)
go

INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','PST/PDT', 'Pacific Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'PST/PDT' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','EST/EDT', 'Eastern Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'EST/EDT' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','CST/CDT', 'Central Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'CST/CDT' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','AKST/AKDT', 'Alaska Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'AKST/AKDT' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','HST', 'Hawaii Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'HST' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','MST/MDT', 'Mountain Time',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'MST/MDT' AND type = 'timezone');
INSERT INTO reference(type, code, description, active_ind) SELECT 'timezone','MST', 'Mountain Time (Arizona)',1 WHERE NOT EXISTS (SELECT * FROM reference WHERE code = 'MST' AND type = 'timezone');
