-- 2260: Show emp a message when manager changes their time entry hours:
--       After running these commands, be sure to apply the updated 
--       sync_3.sql & rpt_time_submit_msg_response.sql scripts

-- Add new rule_id field & foreign key to message_dest 
alter table message_dest
  add rule_id int null
go
alter table message_dest
    add constraint message_dest_fk_break_rule foreign key  (rule_id)
       references break_rules (rule_id)
go

-- add reference table entries for the types of Break Rules
insert into reference (type, code, description) values ('BRKRULE', 'Submit', 'Time Entry Submitted')
insert into reference (type, code, description) values ('BRKRULE', 'MgrAlter', 'Manager Altered Time')
insert into reference (type, code, description) values ('BRKRULE', 'Every', 'Break Required Every x Minutes')
insert into reference (type, code, description) values ('BRKRULE', 'After', 'Break Required After x Minutes')
insert into reference (type, code, description) values ('BRKRULE', 'SubmitDis', 'Disagree to Sumbit Message')
go

-- use QManagerAdmin to setup a MgrAlter message for each profit center or use this sql insert:
/*
insert into break_rules (pc_code, rule_type, rule_message, button_text)
  select pc_code, 
    'MgrAlter', 
    'Your manager altered your hours.',
    'Accept,Reject'
  from profit_center
  order by pc_code
go
*/

