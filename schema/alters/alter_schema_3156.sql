IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'users' and COLUMN_NAME='api_key')
  alter table users add api_key varchar(40) null
go