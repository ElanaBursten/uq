IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'fax_queue_rules' and COLUMN_NAME = 'send_type')
  alter table fax_queue_rules add send_type varchar(15) not null default 'FAXMAKER'
go

IF EXISTS (select * from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE where TABLE_NAME ='fax_queue_rules' and COLUMN_NAME = 'send_type' and CONSTRAINT_NAME = 'send_type_check')
  ALTER TABLE fax_queue_rules DROP CONSTRAINT send_type_check
GO

ALTER TABLE fax_queue_rules ADD CONSTRAINT send_type_check check (send_type in ('FAXMAKER', 'EFAX'))

IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'fax_queue' and COLUMN_NAME = 'send_type')
  alter table fax_queue add send_type varchar(15)
go
IF NOT EXISTS (select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'fax_message' and COLUMN_NAME = 'send_type')
  alter table fax_message add send_type varchar(15)
go