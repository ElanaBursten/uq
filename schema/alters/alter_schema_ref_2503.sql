if not exists 
  (select * from configuration_data where name='TicketChangeViewLimitsMax') 
  insert into configuration_data(name, value, editable) values ('TicketChangeViewLimitsMax', 100, 1)