-- Employee Activity reference entries
if not exists (select * from reference where type = 'empact' and code = 'DMGNOTE') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGNOTE', 'Add Damage Note',1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGARRV') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGARRV', 'Damage Arrival', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGATTCH') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGATTCH', 'Add Damage Attachment', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGATTUP') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGATTUP', 'Upload Damage Attachment', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGSAVE') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGSAVE', 'Save Damage Changes', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGCOMP') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGCOMP', 'Complete Damage', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGADD') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGADD', 'Add New Damage', 1)
go
if not exists (select * from reference where type = 'empact' and code = 'DMGATTBUP') 
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGATTBUP', 'Background Damage Attachment Upload', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGFND')
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGFND', 'Find Damage', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGVUE')
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGVUE', 'View Damage', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGAPPR')
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGAPPR', 'Approve Damage', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGEST')
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGEST', 'Add Damage Estimate', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGINV')
  insert into reference (type, code, description, active_ind) values ('empact', 'DMGINV', 'Add Damage Invoice', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGTHDPTYADD')
  insert into reference (type, code, description, active_ind) values ('empact','DMGTHDPTYADD', 'Add New Third Party Damage Claim', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGTHDPTYVUE')
  insert into reference (type, code, description, active_ind) values ('empact','DMGTHDPTYVUE', 'View Third Party Damage Claim', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGTHDPTYSAV')
  insert into reference (type, code, description, active_ind) values ('empact','DMGTHDPTYSAV', 'Save Third Party Damage Claim Changes', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGLITGADD')
  insert into reference (type, code, description, active_ind) values ('empact','DMGLITGADD', 'Add New Damage Claim Litigation', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGLITGVUE')
  insert into reference (type, code, description, active_ind) values ('empact','DMGLITGVUE', 'View Damage Claim Litigation', 1)
go
if not exists(select * from reference where type = 'empact' and code = 'DMGLITGSAVE')
  insert into reference (type, code, description, active_ind) values ('empact','DMGLITGSAVE', 'Save Damage Claim Litigation Changes', 1)
go

