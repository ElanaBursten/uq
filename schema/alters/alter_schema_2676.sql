--Add new right permitting a user to access to the new Ticket Management screen
if not exists (select * from right_definition where entity_data = 'TicketManagementV2')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('TicketManagementV2', 'Tickets - Management New', 'General', 
    'Uses the Tickets - Management right limitation.')
go
