if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'DamagesAssignLocator') 
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
    values ('Damages - Assign Locator', 'General', 'DamagesAssignLocator', 
    'Limitation is the employee ID representing the part of the hierarchy the user can assign to a damage.')

