--Add List of Responder Types
if exists (select * from reference where type = 'resptype')
  delete from reference where type = 'resptype'
INSERT INTO reference(type, code, description) values ('resptype', 'xmlhttp', 'XMLHTTP')
INSERT INTO reference(type, code, description) values ('resptype', 'email', 'Email')
INSERT INTO reference(type, code, description) values ('resptype', 'ftp', 'FTP')
INSERT INTO reference(type, code, description) values ('resptype', 'xcel', 'XCEL')
INSERT INTO reference(type, code, description) values ('resptype', 'work_order', 'Work Order')
INSERT INTO reference(type, code, description) values ('resptype', 'soap', 'SOAP')

--Add List of Responder Kinds
if exists (select * from reference where type = 'respkind')
  delete from reference where type = 'respkind'
INSERT INTO reference(type, code, description) values ('respkind', 'njoc', 'njoc')
INSERT INTO reference(type, code, description) values ('respkind', 'pups', 'pups')
INSERT INTO reference(type, code, description) values ('respkind', 'translore', 'translore')
