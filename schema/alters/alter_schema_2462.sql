if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'attachment' and COLUMN_NAME = 'upload_machine_name')
  alter table attachment add upload_machine_name varchar(25) null
go

