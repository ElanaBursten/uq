/* Creates payment code reference table entries & adds damage_invoice.payment_code field */
insert into reference (type, code, description, sortby) values ('paycode', 'LIUD', 'Litigation Indemnification Utility Damage', 1)
insert into reference (type, code, description, sortby) values ('paycode', 'LILOU', 'Litigation Indemnification Loss of Use', 2)
insert into reference (type, code, description, sortby) values ('paycode', 'LIPD', 'Litigation Indemnification 3rd party Property Damage', 3)
insert into reference (type, code, description, sortby) values ('paycode', 'LAF', 'Litigation Attorney Fees', 4)
insert into reference (type, code, description, sortby) values ('paycode', 'LEXP', 'Litigation Legal Expenses (court reporter, filing fees, etc)', 5)
insert into reference (type, code, description, sortby) values ('paycode', 'LWIT', 'Litigation Expert Witness Expense', 6)
go

alter table damage_invoice 
  add payment_code varchar(10)
go

/* there are a very small number of rows in this table currently, so the not null shouldn't be a problem */
alter table damage_litigation
  add defendant   varchar(45) null,
  venue         varchar(100) null,
  venue_state   char(2) null,
  service_date  datetime null,
  summons_date  datetime null,
  carrier_notified bit not null default 0,
  claim_status  varchar(10) null,
  added_by      int null,
  modified_date datetime not null default GetDate(),
  modified_by   int null
go


