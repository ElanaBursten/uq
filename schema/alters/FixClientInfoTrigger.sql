USE [QM]
GO

/****** Object:  Trigger [client_info_u]    Script Date: 9/12/2019 10:04:45 AM ******/
DROP TRIGGER [dbo].[client_info_u]  --drops this trigger completely.
GO

/****** Object:  Trigger [dbo].[client_info_ui]    Script Date: 9/12/2019 9:48:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- creates a NEW trigger properly named that works with updates and inserts
CREATE trigger [dbo].[client_info_iu] on [dbo].[client_info] 
for insert,update
AS
  update client_info set modified_date = getdate() where client_info_id in (select client_info_id from inserted) --CHG0057666

