if not exists (select * from information_schema.tables where table_name = 'status_translation')
begin
  create table status_translation (
    status_translation_id int identity primary key,
    cc_code varchar(20) not null,
    status varchar(5) not null,
    outgoing_status varchar(20) not null,
    active bit not null default 1,
    modified_date datetime not null default getdate(),
  )
end
go


if not exists (select * from sys.indexes where name = 'status_translation_cc_code_status')
  create unique nonclustered index status_translation_cc_code_status on status_translation (cc_code, status)
go


if exists(select name from sysobjects where name = 'status_translation_iu' and type = 'tr')
  drop trigger status_translation_iu
go


create trigger status_translation_iu on status_translation
for insert,update
not for replication
as
begin
  if system_user = 'no_trigger'
    return

  set nocount on
  update status_translation
  set modified_date = getdate()
  from inserted i
  where status_translation.status_translation_id = i.status_translation_id
end
go


if not exists(select * from status_translation where cc_code = 'WGL' and status = 'ILS')
  insert into status_translation (cc_code, status, outgoing_status)
  values ('WGL', 'ILS', 'ILS 300')
if not exists(select * from status_translation where cc_code = 'WGL' and status = 'DONE')
  insert into status_translation (cc_code, status, outgoing_status)
  values ('WGL', 'DONE', 'COMPLETE')
go