--QMANTWO-434 INR ALTER SCHEMA

--You will also need to run sync_6 for retrieving config data

INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[modified_date]
           ,[editable]
           ,[description])
     VALUES
           ('INR_BaseURL'
           ,'http://dyatl-dqmgwb01/utqinspector/INRWorkOrder.aspx?'  -- PLACE HOLDER
           ,GetDate()
           ,1
           ,'Used for INR web site')
GO
--------------------------------------------------------