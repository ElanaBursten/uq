if not exists (
select column_name from information_schema.columns
where column_name = 'wo_number' and table_name = 'ticket') 
  alter table ticket
  add wo_number varchar(40) null 

if not exists (
select column_name from information_schema.columns
where column_name = 'boring' and table_name = 'ticket') 
  alter table ticket
  add boring varchar(20) null 

go

/* To roll back these changes:

if exists (
select column_name from information_schema.columns
where column_name = 'wo_number' and table_name = 'ticket') 
  alter table ticket drop column wo_number

if exists (
select column_name from information_schema.columns
where column_name = 'boring' and table_name = 'ticket') 
  alter table ticket drop column boring

go

*/