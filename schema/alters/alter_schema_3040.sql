/* Execute the alter_schema_3040_clean.sql script before executing this one */

/* Convert exported_tasks columns to non-null in order to use as part of primary key */
if exists (select * from dbo.sysindexes where name = N'exported_tasks_customer_number' and id = object_id(N'[dbo].[exported_tasks]')) --no longer needed with new PK
  drop index exported_tasks.exported_tasks_customer_number
go  
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_tasks', 'U'), 'customer_number', 'AllowsNull') ) = 1)
  alter table exported_tasks alter column customer_number varchar(20) NOT NULL
go
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_tasks', 'U'), 'short_name', 'AllowsNull') ) = 1)
  alter table exported_tasks alter column short_name varchar(30) NOT NULL
go
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_tasks', 'U'), 'manager_emp_number', 'AllowsNull') ) = 1)
  alter table exported_tasks alter column manager_emp_number varchar(15) NOT NULL
go

/*add identity column to use for exported_employee's primary key*/
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='exported_employees' and COLUMN_NAME='exported_employees_id')
  alter table exported_employees add exported_employees_id integer identity primary key nonclustered
go

/*Convert exported_projects columns to non-null in order to use as part of primary key*/
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_projects', 'U'), 'payroll_company_code', 'AllowsNull') ) = 1)
  alter table exported_projects alter column payroll_company_code varchar(20) NOT NULL
go
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_projects', 'U'), 'pc_code', 'AllowsNull') ) = 1)
  alter table exported_projects alter column pc_code varchar(15) NOT NULL
go
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_projects', 'U'), 'manager_emp_number', 'AllowsNull') ) = 1)
  alter table exported_projects alter column manager_emp_number varchar(15) NOT NULL
go
if exists (select * from dbo.sysindexes where name = N'exported_projects_customer_number' and id = object_id(N'[dbo].[exported_projects]')) --no longer needed with PK
  drop index exported_projects.exported_projects_customer_number
go  
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_projects', 'U'), 'customer_number', 'AllowsNull') ) = 1)
  alter table exported_projects alter column customer_number varchar(20) NOT NULL
go

/* Convert exported_damages_history columns to non-null to use them as part of pk */
if ((SELECT COLUMNPROPERTY(OBJECT_ID('exported_damages_history', 'U'), 'invoice_code', 'AllowsNull') ) = 1)
  alter table exported_damages_history alter column invoice_code varchar(10) NOT NULL
go

/* Add primary keys to payroll export tables */
-- The exported_damages table is populated by the export_damages stored proc; that stored proc is not called anywhere in the QM source code. 
if not exists(select * from sys.key_constraints where name = 'PK_exported_damages')
  alter table exported_damages
    add constraint PK_exported_damages
    primary key nonclustered (short_period, estimate_id)
go

--Old Mantis 2262 indicates this table is populated by a query against the exported_damages table
if not exists(select * from sys.key_constraints where name = 'PK_exported_damages_history')
  alter table exported_damages_history
    add constraint PK_exported_damages_history
    primary key nonclustered (short_period, estimate_id, project, task, invoice_code)
go

--The exported_tasks table is populated by the export_tasks3 stored procedure.  This stored procedure is in use (called by the method TSolomonExportDM.SaveAllData).
if not exists(select * from sys.key_constraints where name = 'PK_exported_tasks')
  alter table exported_tasks
    add constraint PK_exported_tasks
    primary key nonclustered (customer_number, task_id, short_name, manager_emp_number)
go

--The exported_projects table population is now deprecated; but keeping the table in case UQ may need.
if not exists(select * from sys.key_constraints where name = 'PK_exported_projects')
  alter table exported_projects
    add constraint PK_exported_projects
    primary key nonclustered (payroll_company_code, pc_code, manager_emp_number, customer_number)
go
