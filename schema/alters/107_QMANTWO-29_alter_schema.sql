--107 (QQMANTWO-29)
-- Adds Question for assigning a new locator 'TK_ASSGN' | 'DROPDN'
-- Adds the code '666' for Colorado - Acknowledge Button click

USE QM

declare @Max_qi_id Int

  SELECT @Max_qi_id =  max([qi_id])+1 FROM [QM].[dbo].[question_info]
  Insert into [QM].[dbo].[question_info](qi_id, qi_description, info_type, form_field, active)values
  (@Max_qi_id, 'Who did you assign to this ticket?', 'TK_ASSGN', 'DROPDN', 1)

declare @Max_qgd_id Int
SELECT @Max_qgd_id =  max([qgd_id])+1 FROM [QM].[dbo].[question_group_detail]
  Insert into [QM].[dbo].[question_group_detail]  ([qgd_id], [qh_id] ,[qi_id] ,[tstatus],[active]) values
	(@Max_qgd_id,1,	@Max_qi_id,	'666',1)

SELECT @Max_qgd_id =  max([qgd_id])+1 FROM [QM].[dbo].[question_group_detail]
  Insert into [QM].[dbo].[question_group_detail]  ([qgd_id], [qh_id] ,[qi_id] ,[tstatus],[active]) values
	(@Max_qgd_id,2,	@Max_qi_id,	'666',1)