USE [QM]
GO

/****** Object:  Table [dbo].[client_info]    Script Date: 10/18/2018 2:31:38 PM ******/
DROP TABLE [dbo].[client_info]
GO

/****** Object:  Table [dbo].[client_info]    Script Date: 10/18/2018 2:31:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[client_info](
	[client_info_id] [int] IDENTITY(1,1) NOT NULL,
	[client_id] [int] NOT NULL,
	[client_rules] [nvarchar](max) NULL,
	[client_marking_concerns] [nvarchar](max) NULL,
	[client_trainer_emp_id] [int] NULL,
	[client_field_engineer_emp_id] [int] NULL,
	[client_plat_info] [nvarchar](max) NULL,
	[client_plat_file] [nvarchar](50) NULL,
	[training_link] [nvarchar](50) NULL,
	[plats_online_link] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL,
 CONSTRAINT [PK_client_info] PRIMARY KEY CLUSTERED 
(
	[client_info_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

