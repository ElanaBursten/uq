/*
   Thursday, August 03, 20171:10:39 PM
   User: 
   Server: DYATL-DQMGDB01
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_unit_conversion
	DROP CONSTRAINT DF__billing_u__first__7E188EBC
GO
ALTER TABLE dbo.billing_unit_conversion
	DROP CONSTRAINT DF__billing_u__modif__7F0CB2F5
GO
ALTER TABLE dbo.billing_unit_conversion
	DROP CONSTRAINT DF__billing_u__activ__0000D72E
GO
ALTER TABLE dbo.billing_unit_conversion
	DROP CONSTRAINT DF__billing_u__inser__00F4FB67
GO
CREATE TABLE dbo.Tmp_billing_unit_conversion
	(
	unit_conversion_id int NOT NULL IDENTITY (38000, 1),
	first_unit_type varchar(10) NULL,
	first_unit_factor int NOT NULL,
	rest_unit_type varchar(20) NULL,
	rest_units_factor int NULL,
	feet_per_unit int NULL,
	modified_date datetime NOT NULL,
	active bit NOT NULL,
	insert_date datetime NOT NULL,
	modified_by int NULL,
	added_by int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_billing_unit_conversion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_billing_unit_conversion ADD CONSTRAINT
	DF__billing_u__first__7E188EBC DEFAULT ((1)) FOR first_unit_factor
GO
ALTER TABLE dbo.Tmp_billing_unit_conversion ADD CONSTRAINT
	DF__billing_u__modif__7F0CB2F5 DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_billing_unit_conversion ADD CONSTRAINT
	DF__billing_u__activ__0000D72E DEFAULT ((1)) FOR active
GO
ALTER TABLE dbo.Tmp_billing_unit_conversion ADD CONSTRAINT
	DF__billing_u__inser__00F4FB67 DEFAULT (getdate()) FOR insert_date
GO
SET IDENTITY_INSERT dbo.Tmp_billing_unit_conversion ON
GO
IF EXISTS(SELECT * FROM dbo.billing_unit_conversion)
	 EXEC('INSERT INTO dbo.Tmp_billing_unit_conversion (unit_conversion_id, first_unit_type, first_unit_factor, rest_unit_type, rest_units_factor, modified_date, active, insert_date, modified_by, added_by)
		SELECT unit_conversion_id, first_unit_type, first_unit_factor, rest_unit_type, rest_units_factor, modified_date, active, insert_date, modified_by, added_by FROM dbo.billing_unit_conversion WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_billing_unit_conversion OFF
GO
DROP TABLE dbo.billing_unit_conversion
GO
EXECUTE sp_rename N'dbo.Tmp_billing_unit_conversion', N'billing_unit_conversion', 'OBJECT' 
GO
ALTER TABLE dbo.billing_unit_conversion ADD CONSTRAINT
	PK__billing_unit_con__7D246A83 PRIMARY KEY CLUSTERED 
	(
	unit_conversion_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_unit_conversion', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_unit_conversion', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_unit_conversion', 'Object', 'CONTROL') as Contr_Per 