update damage set uq_notified_date = dateadd(second, dup_id-orig_id, dd.uq_notified_date)
FROM
(select o.damage_id orig_id,
 d.damage_id dup_id,
 o.uq_notified_date, o.profit_center, o.added_by
 from damage o
 inner join damage d
  on d.damage_id > o.damage_id
  and d.uq_notified_date = o.uq_notified_date
  and d.profit_center = o.profit_center
  and d.added_by = o.added_by 
 where o.damage_id = (
   select MIN(damage_id) from damage m
   where m.uq_notified_date = o.uq_notified_date
     and m.profit_center = o.profit_center
     and m.added_by = o.added_by 
 )) dd
where damage.damage_id=dd.dup_id


if not exists (select constraint_name from information_schema.table_constraints 
  where constraint_name = 'damage_notification_unique_constr' and table_name = 'damage' and constraint_type = 'unique') 
  alter table damage
    add constraint damage_notification_unique_constr
    unique nonclustered (uq_notified_date, profit_center, added_by)
go
