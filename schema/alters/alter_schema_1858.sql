-- new jobsite_arrival table
create table jobsite_arrival (
  arrival_id integer identity(43000, 1) not null primary key nonclustered,
  ticket_id integer not null,
  arrival_date datetime not null,
  emp_id integer not null,
  entry_method varchar(10) not null,
  added_by integer not null,
  deleted_by integer null,
  deleted_date datetime null,
  active bit not null default 1,
  modified_date datetime not null default GetDate()
)
go

create index jobsite_arrival_ticket_id on jobsite_arrival(ticket_id)
go

alter table jobsite_arrival
  add constraint jobsite_arrival_fk_ticket
  foreign key (ticket_id) references ticket(ticket_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_arriver_fk_employee
  foreign key (emp_id) references employee(emp_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_adder_fk_employee
  foreign key (added_by) references employee(emp_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_deleter_fk_employee
  foreign key (deleted_by) references employee(emp_id)
go

-- new jobsite_arrival update trigger
create trigger jobsite_arrival_u on jobsite_arrival
for update
AS
  update jobsite_arrival set modified_date = GetDate() where arrival_id in (select arrival_id from inserted)
go

-- add track_arrivals flag to call_center table
alter table call_center
  add track_arrivals bit null
go

-- add missing call_center update trigger
create trigger call_center_u on call_center
for update
AS
  update call_center set modified_date = getdate() where cc_code in (select cc_code from inserted)
go

-- add arrival_date to billing_detail table
alter table billing_detail
  add initial_arrival_date datetime null
go

if object_id('dbo.get_ticket3') is not null
  drop procedure dbo.get_ticket3
go

create proc get_ticket3(@TicketID int)
as
  set nocount on

  select ticket.*, coalesce(area.area_name, '') as route_area_name
  from ticket
  left join area on area.area_id = ticket.route_area_id
    where ticket.ticket_id=@TicketID
    for xml auto

  select locate.*,
   (select max(assignment.locator_id)
    from assignment
    where locate.locate_id=assignment.locate_id and assignment.active=1
   ) as locator_id
  from locate
  where locate.ticket_id=@TicketID
    and (locate.status <> '-N')
    and (locate.status <> '-P')
  for xml auto

  select
    response_log.response_id,
    response_log.locate_id,
    response_log.response_date,
    response_log.call_center,
    response_log.status,
    response_log.response_sent,
    response_log.success,
    -- work around for junk data issue:
    case when (call_center='FMB1' or call_center='FMW1') and reply like '%N' then '(hidden)'
         else response_log.reply end as reply
  from locate
    inner join response_log with (index(response_log_locate_id))
      on response_log.locate_id = locate.locate_id
  where locate.ticket_id = @TicketID
  for xml auto

  select notes.*
  from notes
   where notes.foreign_type = 1    -- foreign_type of 1 = ticket notes
    and notes.foreign_id=@TicketID -- foreign_id has the ticket ID
    and notes.active = 1           -- get only active notes
  for xml auto

  select locate_hours.*
  from locate_hours with (index(locate_hours_locate_id))
    where locate_id in
      (select locate_id from locate where locate.ticket_id=@TicketID)
  for xml auto

  select attachment.*
  from attachment
  where attachment.foreign_id = @TicketID
    and attachment.foreign_type = 1
    and attachment.active = 1
  for xml auto

  select locate_plat.*
    from locate_plat
   where locate_id in (select locate_id from locate where ticket_id = @TicketID)
    for xml auto

  select
     ticket_version_id, ticket_id, ticket_revision, ticket_number, ticket_type, transmit_date,
     processed_date, serial_number, arrival_date, filename, ticket_format, source
  from ticket_version
  where ticket_id = @TicketID
    for xml auto

  select jobsite_arrival.*
    from jobsite_arrival
    where ticket_id = @TicketID
      and active = 1
    for xml auto
go

grant execute on get_ticket3 to uqweb, QManagerRole
go

/*
exec dbo.get_ticket3 10016630
exec dbo.get_ticket3 13596692
exec dbo.get_ticket3 13572526
exec dbo.get_ticket3 17345511
exec dbo.get_ticket3 38673907
exec dbo.get_ticket3 38754194
*/

if object_id('dbo.get_ticket_history') is not null
  drop procedure dbo.get_ticket_history
GO

CREATE proc get_ticket_history(@TicketID int)
as
  set nocount on

  declare @Locates table (locate_id int not null)

  insert into @Locates select locate_id
  from locate where ticket_id = @TicketID

  select * from ticket_version
    where ticket_id = @TicketID
  for xml auto

  select * from assignment
    where locate_id in (select locate_id from @Locates)
  for xml auto

  select * from locate_status
    where locate_id in (select locate_id from @Locates)
  for xml auto

  select * from response_log
    where locate_id in (select locate_id from @Locates)
  for xml auto

  declare @BillingDetail table (bill_id int not null, billing_detail_id int not null)

  insert into @BillingDetail
  select bill_id, billing_detail_id
  from billing_detail
  where locate_id in (select locate_id from @Locates)

  select * from billing_detail
  where billing_detail_id in (select billing_detail_id from @BillingDetail)
  for xml auto

  select * from billing_header
  where bill_id in (select distinct bill_id from @BillingDetail)
  for xml auto

  select * from ticket_ack
    where ticket_id = @TicketID
  for xml auto

  select billing_invoice.* from billing_invoice
  where billing_invoice.billing_header_id in (select distinct bill_id from @BillingDetail)
  for xml auto

  select ticket_info.* from ticket_info
    where ticket_id = @TicketID
  for xml auto

  select jobsite_arrival.* from jobsite_arrival
    where ticket_id = @TicketID
  for xml auto

GO

grant execute on get_ticket_history to uqweb, QManagerRole
go
/*
exec dbo.get_ticket_history 3965012
exec dbo.get_ticket_history 17207760
*/

if object_id('dbo.sync_3') is not null
  drop procedure dbo.sync_3
go

create proc sync_3 (
  @EmployeeID int,
  @UpdatesSinceS varchar(30),
  @ClientTicketListS text
)
as
  set nocount on

  DECLARE @tix TABLE (ticket_id integer not null primary key)
  DECLARE @modtix TABLE (ticket_id integer not null primary key)
  DECLARE @closed_since datetime
  DECLARE @oldest_l int
  DECLARE @UpdatesSince datetime

  DECLARE @client_has_tix TABLE (client_ticket_id integer not null primary key)
  DECLARE @should_have_tix TABLE (should_ticket_id integer not null primary key)

  set @UpdatesSince = convert(datetime, @UpdatesSinceS)

  -- Go back 2 minutes, to reduce update overlap errors
  set @UpdatesSince = dateadd(s, -120, @UpdatesSince)

/* Use this in emergency situations to disable initial syncs
  if @UpdatesSince < '2000-01-01'
  begin
    RAISERROR ('Initial syncs temporarily disabled', 50005, 1) with log
    return
  end
*/

  select 'row' as tname, getdate() as SyncDateTime

  select 'office' as tname, * from office
    where modified_date > @UpdatesSince

  select 'call_center' as tname, cc_code, cc_name, active, track_arrivals from call_center
   where modified_date > @UpdatesSince

  select 'statuslist' as tname, * from statuslist where modified_date > @UpdatesSince

  select 'reference' as tname, * from reference where modified_date > @UpdatesSince

  select 'profit_center' as tname, * from profit_center where modified_date > @UpdatesSince

  select 'customer' as tname, * from customer where modified_date > @UpdatesSince

  select 'center_group' as tname, * from center_group where modified_date > @UpdatesSince

  select 'center_group_detail' as tname, * from center_group_detail where modified_date > @UpdatesSince

  select 'client' as tname, * from client where modified_date > @UpdatesSince

  select 'configuration_data' as tname, * from configuration_data where (modified_date > @UpdatesSince) and (Name in ('ClientURL'))

  select 'employee' as tname, emp_id, type_id, status_id, timesheet_id,
    emp_number, short_name, report_to, active, company_car, timerule_id,
    repr_pc_code, payroll_pc_code, company_id
   from employee where modified_date > @UpdatesSince

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, login_id, chg_pwd,
    last_login, active_ind, can_view_notes, can_view_history, chg_pwd_date
   from users where emp_id = @EmployeeID

  select 'locating_company' as tname, * from locating_company
   where modified_date > @UpdatesSince

  select 'carrier' as tname, * from carrier
   where modified_date > @UpdatesSince

  select 'vehicle_type' as tname, * from vehicle_type
   where modified_date > @UpdatesSince

  select 'followup_ticket_type' as tname, * from followup_ticket_type
   where modified_date > @UpdatesSince

  select 'employee' as tname, emp_id, charge_cov, timerule_id,
    timesheet_id, company_car, effective_pc = dbo.get_employee_pc(emp_id, 0),
    first_name, last_name
   from employee
   where emp_id = @EmployeeID

  select 'employee_right' as tname, * from employee_right
    where emp_id=@EmployeeID and modified_date > @UpdatesSince

  select 'vehicle_use' as tname, * from vehicle_use
    where modified_date > @UpdatesSince and emp_id = @EmployeeID

  select 'timesheet' as tname, timesheet.*, timesheet_detail.*
  from timesheet
    inner join timesheet_detail
      on timesheet.timesheet_id = timesheet_detail.timesheet_id
  where (timesheet.modified_date > @UpdatesSince) and (timesheet.end_date > dateadd(Day, -38, getdate()))
    and timesheet.emp_id = @EmployeeID

  select 'timesheet_entry' as tname, *
  from timesheet_entry
  where
    work_emp_id = @EmployeeID and
    (modified_date > @UpdatesSince) and (work_date > dateadd(Day, -38, getdate()))
    -- We no longer go back 2 years to get old floating_holiday timesheets, since that feature is not in use

  select 'asset_type' as tname, * from asset_type
  where modified_date > @UpdatesSince

  select 'asset_assignment' as tname, * from asset_assignment
  where emp_id = @EmployeeID
    and modified_date > @UpdatesSince

  select 'asset' as asset, * from asset
  where asset_id in (select asset_id from asset_assignment where emp_id = @EmployeeID)
    and modified_date > @UpdatesSince

  select 'upload_location' as tname, * from upload_location
  where modified_date > @UpdatesSince

  select 'upload_file_type' as tname, * from upload_file_type
  where modified_date > @UpdatesSince

  select 'damage_default_est' as tname, * from damage_default_est
  where modified_date > @UpdatesSince

  select 'status_group' as tname, * from status_group
  where modified_date > @UpdatesSince

  select 'status_group_item' as tname, * from status_group_item
  where modified_date > @UpdatesSince

  select 'call_center_hp' as tname, * from call_center_hp
  where modified_date > @UpdatesSince

  select 'billing_unit_conversion' as tname, * from billing_unit_conversion
  where modified_date > @UpdatesSince

  select 'billing_gl' as tname, * from billing_gl
  where modified_date > @UpdatesSince

  select 'area' as tname, * from area
  where map_id = (select value from configuration_data where name = 'AssignableAreaMapID')
    and modified_date > @UpdatesSince

  select 'billing_output_config' as tname, output_config_id, customer_id, which_invoice
  from billing_output_config where modified_date > @UpdatesSince

  -- Ticket-related data
  select @closed_since = dateadd(day, -5, getdate())

  select @oldest_l = (select top 1 oldest_open_locate from oldest_open)

  -- all open locates, so client can detect it has missing locates
  select 'open_locate' as tname, ticket_id, locate_id, status
   from locate open_locate
   where assigned_to=@EmployeeID

  -- all open tickets, so we can be sure to send them all
  insert into @should_have_tix
  select distinct ticket_id
   from locate
   where assigned_to=@EmployeeID

  -- what the client does have
  insert into @client_has_tix
  select ID from IdListToTable(@ClientTicketListS)

  -- DISABLE the sync of "my old tickets":
  if (1=0) and (@@ROWCOUNT<1 or @UpdatesSince < '2000-01-01')  begin    -- support old clients and init syncs
    if @UpdatesSince < '2000-01-01'  begin
      -- Get all tickets assigned to me even if closed
      insert into @tix  (ticket_id)
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date> @closed_since
      union
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date is null
      union
      select should_ticket_id from @should_have_tix
    end else begin
      -- This is a subsequent sync with the client tic list missing,
      -- must use old alg to get consistent results:
      -- Get all tickets ever assigned to me, this is the SLOW part
      insert into @tix  (ticket_id)
      select distinct locate.ticket_id
       from assignment
       inner join locate on locate.locate_id=assignment.locate_id
          and (locate.closed=0 or locate.closed_date> @closed_since)
       where assignment.locator_id = @EmployeeID
        and assignment.locate_id >= @oldest_l
    end
  end else begin  -- newer clients tell us what they have
    -- they need to know about updates to tickets they are
    -- assigned now, and also tickets still in cache
    insert into @tix  (ticket_id)
      select should_ticket_id from @should_have_tix
      union
      select client_ticket_id from @client_has_tix
  end

  -- subtract what we have, from what we should have
  delete from @should_have_tix where should_ticket_id in
    (select client_ticket_id from @client_has_tix)

  -- Find those that have changed, somewhat slow
  insert into @modtix (ticket_id)
  select t.ticket_id
   from @tix t
    inner join ticket on ticket.ticket_id = t.ticket_id
    and ticket.modified_date >@UpdatesSince
  union
  select locate.ticket_id
   from @tix t2
    inner join locate on locate.ticket_id = t2.ticket_id
   where locate.modified_date > @UpdatesSince
    and locate.status<>'-N'
  union
  select locate.ticket_id
   from @tix t3
    inner join locate on locate.ticket_id = t3.ticket_id
    inner join assignment on locate.locate_id=assignment.locate_id
   where assignment.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @tix t4
    inner join notes on notes.foreign_id = t4.ticket_id
   where notes.foreign_type = 1
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @tix t5
    inner join attachment on attachment.foreign_id = t5.ticket_id
   where attachment.foreign_type = 1
    and attachment.modified_date > @UpdatesSince
  union
    select should_ticket_id from @should_have_tix

  -- now get the whole ticket for just the ones that have changed
  declare @tickets_to_send table (
  [ticket_id] [int] NOT NULL ,
  [ticket_number] [varchar] (20) NOT NULL ,
  [parsed_ok] [bit] NOT NULL ,
  [ticket_format] [varchar] (20) NOT NULL ,
  [kind] [varchar] (20) NOT NULL ,
  [status] [varchar] (20) NULL ,
  [map_page] [varchar] (20) NULL ,
  [revision] [varchar] (20) NOT NULL ,
  [transmit_date] [datetime] NULL ,
  [due_date] [datetime] NULL ,
  [work_description] [varchar] (3500) NULL ,
  [work_state] [varchar] (2) NULL ,
  [work_county] [varchar] (40) NULL ,
  [work_city] [varchar] (40) NULL ,
  [work_address_number] [varchar] (10) NULL ,
  [work_address_number_2] [varchar] (10) NULL ,
  [work_address_street] [varchar] (60) NULL ,
  [work_cross] [varchar] (100) NULL ,
  [work_subdivision] [varchar] (70) NULL ,
  [work_type] [varchar] (90) NULL ,
  [work_date] [datetime] NULL ,
  [work_notc] [varchar] (40) NULL ,
  [work_remarks] [varchar] (1200) NULL ,
  [priority] [varchar] (40) NULL ,
  [legal_date] [datetime] NULL ,
  [legal_good_thru] [datetime] NULL ,
  [legal_restake] [varchar] (40) NULL ,
  [respond_date] [datetime] NULL ,
  [duration] [varchar] (40) NULL ,
  [company] [varchar] (80) NULL ,
  [con_type] [varchar] (50) NULL ,
  [con_name] [varchar] (50) NULL ,
  [con_address] [varchar] (50) NULL ,
  [con_city] [varchar] (40) NULL ,
  [con_state] [varchar] (40) NULL ,
  [con_zip] [varchar] (40) NULL ,
  [call_date] [datetime] NULL ,
  [caller] [varchar] (50) NULL ,
  [caller_contact] [varchar] (50) NULL ,
  [caller_phone] [varchar] (40) NULL ,
  [caller_cellular] [varchar] (40) NULL ,
  [caller_fax] [varchar] (40) NULL ,
  [caller_altcontact] [varchar] (40) NULL ,
  [caller_altphone] [varchar] (40) NULL ,
  [caller_email] [varchar] (40) NULL ,
  [operator] [varchar] (40) NULL ,
  [channel] [varchar] (40) NULL ,
  [work_lat] [decimal](9, 6) NULL ,
  [work_long] [decimal](9, 6) NULL ,
  [image] [text] NOT NULL ,
  [parse_errors] [text] NULL ,
  [modified_date] [datetime] NOT NULL,
  [active] [bit] NOT NULL,
  [ticket_type] [varchar] (38) NULL ,
  [legal_due_date] [datetime] NULL ,
  [parent_ticket_id] [int] NULL ,
  [do_not_mark_before] [datetime] NULL ,
  [route_area_id] [int] NULL ,
  [watch_and_protect] [bit] NULL,
  [service_area_code] [varchar] (40) NULL ,
  [explosives] [varchar] (20) NULL ,
  [serial_number] [varchar] (40) NULL ,
  [map_ref] [varchar] (60) NULL ,
  [followup_type_id] [int] NULL ,
  [do_not_respond_before] [datetime] NULL ,
  [recv_manager_id] [int] NULL ,
  [ward] [varchar] (10) NULL ,
  route_area_name varchar(50) NULL,
  alert varchar(1) NULL,
  start_date datetime NULL,
  end_date datetime NULL,
  points decimal(6,2) NULL
  )

  declare @locates_to_send table (
  [locate_id] [int] NOT NULL ,
  [ticket_id] [int] NOT NULL ,
  [client_code] [varchar] (10) NULL ,
  [client_id] [int] NULL ,
  [status] [varchar] (5) NOT NULL ,
  [high_profile] [bit] NOT NULL ,
  [qty_marked] [int] NULL ,
  [price] [money] NULL ,
  [closed] [bit] NOT NULL ,
  [closed_by_id] [int] NULL ,
  [closed_how] [varchar] (10) NULL ,
  [closed_date] [datetime] NULL ,
  [modified_date] [datetime] NOT NULL ,
  [active] [bit] NOT NULL,
  [invoiced] [bit] NULL ,
  [assigned_to] [int] NULL ,
  [regular_hours] [decimal](5, 2) NULL ,
  [overtime_hours] [decimal](5, 2) NULL ,
  [added_by] [varchar] (8) NULL ,
  [watch_and_protect] [bit] NULL ,
  [high_profile_reason] [int] NULL ,
  [seq_number] [varchar] (20) NULL ,
  [assigned_to_id] [int] NULL ,
  [mark_type] [varchar] (10) NULL ,
  [alert] varchar(1) NULL,
  [locator_id] int
  )

  -- gather the ticket and locate data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @tickets_to_send
  select
     ticket.ticket_id,
     ticket.ticket_number,
     ticket.parsed_ok,
     ticket.ticket_format,
     ticket.kind,
     ticket.status,
     ticket.map_page,
     ticket.revision,
     ticket.transmit_date,
     ticket.due_date,
     ticket.work_description,
     ticket.work_state,
     ticket.work_county,
     ticket.work_city,
     ticket.work_address_number,
     ticket.work_address_number_2,
     ticket.work_address_street,
     ticket.work_cross,
     ticket.work_subdivision,
     ticket.work_type,
     ticket.work_date,
     ticket.work_notc,
     ticket.work_remarks,
     ticket.priority,
     ticket.legal_date,
     ticket.legal_good_thru,
     ticket.legal_restake,
     ticket.respond_date,
     ticket.duration,
     ticket.company,
     ticket.con_type,
     ticket.con_name,
     ticket.con_address,
     ticket.con_city,
     ticket.con_state,
     ticket.con_zip,
     ticket.call_date,
     ticket.caller,
     ticket.caller_contact,
     ticket.caller_phone,
     ticket.caller_cellular,
     ticket.caller_fax,
     ticket.caller_altcontact,
     ticket.caller_altphone,
     ticket.caller_email,
     ticket.operator,
     ticket.channel,
     ticket.work_lat,
     ticket.work_long,
     ticket.image,
     ticket.parse_errors,
     ticket.modified_date,
     ticket.active,
     ticket.ticket_type,
     ticket.legal_due_date,
     ticket.parent_ticket_id,
     ticket.do_not_mark_before,
     ticket.route_area_id,
     ticket.watch_and_protect,
     ticket.service_area_code,
     ticket.explosives,
     ticket.serial_number,
     ticket.map_ref,
     ticket.followup_type_id,
     ticket.do_not_respond_before,
     ticket.recv_manager_id,
     ticket.ward,
     coalesce(area.area_name, '') as area_name,
     ticket.alert,
     -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null:
     null, --     start_date,
     null, --     end_date,
     null  --     points
    from @modtix mt
      inner join ticket on mt.ticket_id = ticket.ticket_id
      left join area on area.area_id = ticket.route_area_id

  insert into @locates_to_send
    (locate_id, ticket_id, client_code, client_id, status, high_profile, qty_marked,
     price, closed, closed_by_id, closed_how, closed_date, modified_date, active,
     invoiced, assigned_to, regular_hours, overtime_hours, added_by, watch_and_protect,
     high_profile_reason, seq_number, assigned_to_id, mark_type, alert, locator_id)
  select
     l.locate_id, l.ticket_id, l.client_code, l.client_id, l.status, l.high_profile, l.qty_marked,
     l.price, l.closed, l.closed_by_id, l.closed_how, l.closed_date, l.modified_date, l.active,
     l.invoiced, l.assigned_to, l.regular_hours, l.overtime_hours, l.added_by, l.watch_and_protect,
     l.high_profile_reason, l.seq_number, l.assigned_to_id, l.mark_type, l.alert, assignment.locator_id
    from @modtix mt
      inner join locate l
        on mt.ticket_id = l.ticket_id and l.status <> '-N' and l.status <> '-P'
      inner join assignment
        on l.locate_id=assignment.locate_id and assignment.active = 1

  -- Send the ticket and locate data
  select 'ticket' as tname, * from @tickets_to_send ticket

  select 'locate' as tname, * from @locates_to_send locate

  -- Retrieve all notes for any ticket that has been modified in any way, even
  -- if the note has not been modified. This is necessary because the ticket
  -- may be have been reassigned to a different locator.
  select 'notes' as tname, notes.*
   from @modtix mt
   inner join notes on notes.foreign_type = 1 -- 1 = ticket notes
    and mt.ticket_id = notes.foreign_id

  -- All attachments for modified tickets:
  select 'attachment' as tname, attachment.*
   from @modtix mt
   inner join attachment on mt.ticket_id = attachment.foreign_id
    and attachment.foreign_type = 1

  -- All Plats for modified tickets:
  select 'locate_plat' as tname, locate_plat.*
   from @locates_to_send ls
   inner join locate_plat on ls.locate_id = locate_plat.locate_id

  -- All unacked, unexpired messages for employee:
  select 'message_dest' as tname, message.message_id, message.message_number,
   message.from_emp_id, message.destination, message.subject, message.body,
   message.sent_date, message.show_date, message.expiration_date,
   md.message_dest_id, md.emp_id, md.ack_date, md.read_date
  from message inner join message_dest md on message.message_id=md.message_id
  where md.ack_date is null
    and md.emp_id = @EmployeeID
    and message.expiration_date > getdate()

  -- Send the ticket_version data
  select 'ticket_version' as tname,
     tv.ticket_version_id, tv.ticket_id, tv.ticket_revision, tv.ticket_number, tv.ticket_type, tv.transmit_date,
     tv.processed_date, tv.serial_number, tv.arrival_date, tv.filename, tv.ticket_format, tv.source
    from @modtix mt
      inner join ticket_version tv on mt.ticket_id = tv.ticket_id

  -- Send the break_rules data
  select 'break_rules' as tname, break_rules.*
   from break_rules
   where modified_date > @UpdatesSince

  -- Send the right definition data
  select 'right_definition' as tname, * from right_definition where modified_date > @UpdatesSince

  -- Send the right restriction data
  select 'right_restriction' as tname, *
   from right_restriction
   where modified_date > @UpdatesSince

  -- Send the usage restriciton message data
  select 'restricted_use_message' as tname, *
   from restricted_use_message
   where modified_date > @UpdatesSince

  -- Send the usage restriciton exemption data
  select 'restricted_use_exemption' as tname, *
   from restricted_use_exemption
   where emp_id = @EmployeeID
     and modified_date > @UpdatesSince

  -- Send the jobsite_arrival data
  select 'jobsite_arrival' as tname, arrival.*
    from @modtix mt
      inner join jobsite_arrival arrival on mt.ticket_id = arrival.ticket_id
go

grant execute on sync_3 to uqweb, QManagerRole

/*
execute dbo.sync_3 869, '', ''
execute dbo.sync_3 1048, '', ''

execute dbo.sync_3 485, '2004-10-05T12:31:27', ''
execute dbo.sync_3 1920, '2004-10-04T12:00:00', '11468545,11468585,11468673'

execute dbo.sync_3 5654, '1980-02-02', ''

execute dbo.sync_3 5654, '2004-10-05T10:00:00', '14032606,14032612,14032830,14032832,14032868,14032872,14032941,14114127,14114367,14114373,14114485,14114577,14114778,14183768,14190209,14198723,14204438,14205207,14216174,14222243,14222757,14227090,14227227,14227355,14235186,14237609,14240012,14240129,14240236,14240248,14240299,14240360,14240362,14241739,14241817,14248166,14249912,14250939,14254604,14255160,14257362,14257395,14259823,14260936,14265976,14268416,14273680,14273681,14274602,14275777,14275784,14277492,14282431,14282435,14283496,14287533,14291996,14297773,14301535,14305644,14306123,14306126,14306359,14306535,14306671,14306815,14306930,14307475,14308076,14308082,14308211,14308213,14308273,14308352,14308356,14308357,14308402,14308404,14308656,14308752,14314837,14314957,14315051,14315052,14315054,14315138,14315142,14315373,14315374,14315604,14315665,14315801,14315807,14315897,14315909,14315918,14316388,14317678,14322998,14323981,14325080,14335429'

execute dbo.sync_3 924, '2005-02-15T10:51:56.017', ''
1293850 reads!!!

execute dbo.sync_3 924, '2005-02-15T10:51:56.017', '11468545,11468585,11468673'

execute dbo.sync_3 7116, '', ''


select * from sync_log
where emp_id=924
and sync_id> (select max(sync_id) from sync_log) - 25000

select * from employee where emp_id=924
select * from users where emp_id=924

select * from users where login_id='001918'
  reaganp

select top 1 * from oldest_open
select * from oldest_open_log

--2 months back:
update oldest_open set oldest_open_locate=60423174

select min(locate_id) from
  ticket  t
  inner join locate l on t.ticket_id=l.ticket_id
  and t.transmit_date between '2004-03-21' and '2004-03-22'

select top 10 * from sync_log where emp_id=1896 order by sync_date desc
select top 10 * from sync_log where emp_id=1620 order by sync_date desc

select * from employee where emp_id=3855
select distinct ticket_id from locate where assigned_to=6383
*/
