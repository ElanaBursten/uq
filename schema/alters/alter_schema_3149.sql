--First, drop default constraint on deprecated site_marks_in_tolerance column if it exists
declare @default_name varchar(256)
select @default_name = [name] from sys.default_constraints 
where parent_object_id = object_id('[damage]')
  and col_name(parent_object_id, parent_column_id) = 'site_marks_in_tolerance'

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(@default_name) AND type = 'D')
  exec('ALTER TABLE damage DROP CONSTRAINT ' + @default_name)
GO

--Drop deprecated site_marks_in_tolerance column
IF EXISTS (select * from information_schema.columns where table_name='damage' and column_name='site_marks_in_tolerance')
  alter table damage drop column site_marks_in_tolerance
GO


/*Rollback script provided for convenience in case rollback is needed.

--Add back the deprecated column
IF NOT EXISTS (select * from information_schema.columns where table_name='damage' and column_name='site_marks_in_tolerance')
  alter table damage add site_marks_in_tolerance bit NULL DEFAULT 1
GO

--Populate the column with data from the column that replaced it, marks_within_tolerance.
IF EXISTS (select * from information_schema.columns where table_name='damage' and column_name='site_marks_in_tolerance')
  update damage
    set site_marks_in_tolerance = marks_within_tolerance
    where marks_within_tolerance in ('0','1')
GO

select marks_within_tolerance,site_marks_in_tolerance,* from damage
*/

