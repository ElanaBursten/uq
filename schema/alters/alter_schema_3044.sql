/* Add some missing foreign keys and drop obosolete tables. Run the alter_schema_3044_clean.sql first to fix some inconsistent data.*/

/* Add these foreign keys that are in uq_schema.sql, but not in the production QM / LocQM databases */
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.ticket_version_fk_ticket') AND parent_object_id = OBJECT_ID(N'dbo.ticket_version'))
  ALTER TABLE ticket_version WITH CHECK ADD CONSTRAINT ticket_version_fk_ticket FOREIGN KEY(ticket_id) REFERENCES ticket(ticket_id) 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.client_fk_call_center') AND parent_object_id = OBJECT_ID(N'dbo.client'))
  ALTER TABLE client WITH CHECK ADD CONSTRAINT client_fk_call_center FOREIGN KEY(call_center) REFERENCES call_center(cc_code) 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.client_fk_update_call_center') AND parent_object_id = OBJECT_ID(N'dbo.client'))
  ALTER TABLE client WITH CHECK ADD CONSTRAINT client_fk_update_call_center FOREIGN KEY(update_call_center) REFERENCES call_center(cc_code) 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.damage_litigation_fk_damage') AND parent_object_id = OBJECT_ID(N'dbo.damage_litigation'))
  ALTER TABLE damage_litigation WITH CHECK ADD CONSTRAINT damage_litigation_fk_damage FOREIGN KEY(damage_id) REFERENCES damage(damage_id) 
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.damage_litigation_fk_carrier') AND parent_object_id = OBJECT_ID(N'dbo.damage_litigation'))
  ALTER TABLE damage_litigation WITH CHECK ADD CONSTRAINT damage_litigation_fk_carrier FOREIGN KEY(carrier_id) REFERENCES carrier(carrier_id)
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.billing_rate_fk_call_center') AND parent_object_id = OBJECT_ID(N'dbo.billing_rate'))
  ALTER TABLE billing_rate WITH CHECK ADD CONSTRAINT billing_rate_fk_call_center FOREIGN KEY(call_center) REFERENCES call_center(cc_code) 
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='nonclient_locate') -- This table is in LocQM, but not QM
  IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.nonclient_locate_fk_ticket') AND parent_object_id = OBJECT_ID(N'dbo.nonclient_locate'))
    ALTER TABLE nonclient_locate WITH CHECK ADD CONSTRAINT nonclient_locate_fk_ticket FOREIGN KEY(ticket_id) REFERENCES ticket(ticket_id)
GO

/* Drop old ADC map tables no longer used */
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='map_adc_county_abbr')
  DROP TABLE map_adc_county_abbr
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='map_adc_old_map_page')
  DROP TABLE map_adc_old_map_page
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='map_adc_map_page')
  DROP TABLE map_adc_map_page
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='map_adc_county')
  DROP TABLE map_adc_county
