/*The error codes and corresponding descriptions as provided in the eFax Developer API SDK.}*/
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'eFax_error_codes')
BEGIN
CREATE TABLE eFax_error_codes (
  code  INT NOT NULL PRIMARY KEY,
  description VARCHAR(100) NOT NULL,
  class VARCHAR(1) NOT NULL
)
END

if not exists (select code from eFax_error_codes) begin
  insert into eFax_error_codes (code, description, class) values (60, 'failure to update control file', 'Z')
  insert into eFax_error_codes (code, description, class) values (61, 'failure to invoke pre-processor', 'Z')
  insert into eFax_error_codes (code, description, class) values (62, 'failure to invoke post processor', 'Z')
  insert into eFax_error_codes (code, description, class) values (63, 'phone number failed to pass phone mask', 'Z')
  insert into eFax_error_codes (code, description, class) values (64, 'unable to find or load user profile', 'Z')
  insert into eFax_error_codes (code, description, class) values (65, 'unable to find cover sheet', 'Z')
  insert into eFax_error_codes (code, description, class) values (66, 'transmission stopped on abnormal termination', 'Z')
  insert into eFax_error_codes (code, description, class) values (67, 'unspecified error in .FS file command', 'Z')
  insert into eFax_error_codes (code, description, class) values (68, 'error in file list no files to send', 'Z')
  insert into eFax_error_codes (code, description, class) values (69, 'receive rejected for mail stop match', 'Z')
  insert into eFax_error_codes (code, description, class) values (70, 'not attempted after earlier failure', 'Z')
  insert into eFax_error_codes (code, description, class) values (71, 'internal error no fax board', 'Z')
  insert into eFax_error_codes (code, description, class) values (72, 'failed by FFMERGE', 'Z')
  insert into eFax_error_codes (code, description, class) values (73, 'unable to create attempt record', 'Z')
  insert into eFax_error_codes (code, description, class) values (74, 'error or hangup during voice OB call', 'Z')
  insert into eFax_error_codes (code, description, class) values (75, 'phone failed on global DNS lookup', 'Z')
  insert into eFax_error_codes (code, description, class) values (76, 'phone failed on user DNS lookup', 'Z')
  insert into eFax_error_codes (code, description, class) values (77, 'missing phone number', 'Z')
  insert into eFax_error_codes (code, description, class) values (78, 'phone number too long', 'Z')
  insert into eFax_error_codes (code, description, class) values (79, 'auto job launch on timeout cancelled', 'Z')
  insert into eFax_error_codes (code, description, class) values (80, 'exception while checking DNS', 'Z')
  insert into eFax_error_codes (code, description, class) values (81, 'no user profile for pre-process', 'Z')
  insert into eFax_error_codes (code, description, class) values (82, 'FXCVRT.DLL not found/loaded for pre-process', 'Z')
  insert into eFax_error_codes (code, description, class) values (83, 'FXCVRT.DLL failed to convert source to target', 'Z')
  insert into eFax_error_codes (code, description, class) values (84, 'Timeout accessing DNS_DOMN.NDX', 'Z')
  insert into eFax_error_codes (code, description, class) values (90, 'e-mail failure', 'Z')
  insert into eFax_error_codes (code, description, class) values (91, 'unspecified receive failure', 'Z')
  insert into eFax_error_codes (code, description, class) values (95, '"already in progress" FS file found for this node', 'Z')
  insert into eFax_error_codes (code, description, class) values (100, 'out of memory to process FS file', 'Z')
  insert into eFax_error_codes (code, description, class) values (101, 'invalid preprocess specification', 'Z')
  insert into eFax_error_codes (code, description, class) values (102, 'invalid postprocess specification', 'Z')
  insert into eFax_error_codes (code, description, class) values (103, 'invalid $fax_origin command', 'Z')
  insert into eFax_error_codes (code, description, class) values (104, 'invalid number of parameters on FS command', 'Z')
  insert into eFax_error_codes (code, description, class) values (105, 'unrecognized command in FS file', 'Z')
  insert into eFax_error_codes (code, description, class) values (108, '$fax_filename file not found', 'Z')
  insert into eFax_error_codes (code, description, class) values (110, 'invalid mailbox for outbound poll', 'Z')
  insert into eFax_error_codes (code, description, class) values (119, 'invalid fax send date', 'Z')
  insert into eFax_error_codes (code, description, class) values (120, 'invalid e-mail parameters', 'Z')
  insert into eFax_error_codes (code, description, class) values (121, 'invalid retry parameters', 'Z')
  insert into eFax_error_codes (code, description, class) values (122, 'invalid document conversion options', 'Z')
  insert into eFax_error_codes (code, description, class) values (123, 'DNS file missing', 'Z')
  insert into eFax_error_codes (code, description, class) values (124, 'FS include file error', 'Z')
  insert into eFax_error_codes (code, description, class) values (130, 'Cannot stop wait for inbound call on selected line', 'Z')
  insert into eFax_error_codes (code, description, class) values (131, 'FS file found for completed job', 'Z')
  insert into eFax_error_codes (code, description, class) values (132, 'IIF sequence not finished with correct state', 'Z')
  insert into eFax_error_codes (code, description, class) values (141, 'no local DNS server found', 'Z')
  insert into eFax_error_codes (code, description, class) values (142, 'no e-mail subject body or attachment', 'Z')
  insert into eFax_error_codes (code, description, class) values (143, 'target domain name invalid', 'B')
  insert into eFax_error_codes (code, description, class) values (144, 'domain name lookup timed out', 'A')
  insert into eFax_error_codes (code, description, class) values (145, 'unspecified DNS lookup error', 'A')
  insert into eFax_error_codes (code, description, class) values (146, 'SMTP send timed out', 'A')
  insert into eFax_error_codes (code, description, class) values (147, 'unspecified SMTP send error', 'A')
  insert into eFax_error_codes (code, description, class) values (148, 'Exception while processing e-mail', 'Z')
  insert into eFax_error_codes (code, description, class) values (149, 'Missing body or alternate body file', 'Z')
  insert into eFax_error_codes (code, description, class) values (150, 'Missing attachment file', 'Z')
  insert into eFax_error_codes (code, description, class) values (152, 'No e-mail sender name given', 'Z')
  insert into eFax_error_codes (code, description, class) values (153, 'No attachmentinsert into eFax_error_codes (code, description, class) values (s) for MHTML', 'Z')
  insert into eFax_error_codes (code, description, class) values (154, 'Remote Host not found', 'Z')
  insert into eFax_error_codes (code, description, class) values (155, 'Connection refused', 'Z')
  insert into eFax_error_codes (code, description, class) values (157, 'Unspecified Failure (No data or no address)', 'Z')
  insert into eFax_error_codes (code, description, class) values (160, 'Socket Error #10060 Connection timed out.', 'F')
  insert into eFax_error_codes (code, description, class) values (163, 'Unexpected disconnect ("Unspecified failure")', 'F')
  insert into eFax_error_codes (code, description, class) values (164, 'SMTP timeout before connect', 'F')
  insert into eFax_error_codes (code, description, class) values (258, 'No dial tone detected', 'D')
  insert into eFax_error_codes (code, description, class) values (259, 'No loop current detected', 'D')
  insert into eFax_error_codes (code, description, class) values (260, 'Local phone in use insert into eFax_error_codes (code, description, class) values (country-specific)', 'D')
  insert into eFax_error_codes (code, description, class) values (261, 'Busy trunk line detected', 'D')
  insert into eFax_error_codes (code, description, class) values (265, 'T1 time slot busy', 'D')
  insert into eFax_error_codes (code, description, class) values (266, 'Ringing detected during dialing', 'C')
  insert into eFax_error_codes (code, description, class) values (267, 'No wink', 'C')
  insert into eFax_error_codes (code, description, class) values (268, 'ISDN invalid dial string', 'C')
  insert into eFax_error_codes (code, description, class) values (269, 'Redial failed insert into eFax_error_codes (code, description, class) values (Japan)', 'C')
  insert into eFax_error_codes (code, description, class) values (301, 'Normal busy: remote end busy (off hook)', 'B')
  insert into eFax_error_codes (code, description, class) values (302, 'Normal busy: remote end busy (off hook)', 'B')
  insert into eFax_error_codes (code, description, class) values (303, 'Fast busy: telephone co trunk lines busy', 'B')
  insert into eFax_error_codes (code, description, class) values (304, 'Calling while already connected', 'C')
  insert into eFax_error_codes (code, description, class) values (305, 'Unexpected confirmation tone', 'C')
  insert into eFax_error_codes (code, description, class) values (308, 'Ringing insert into eFax_error_codes (code, description, class) values (single tone)', 'A')
  insert into eFax_error_codes (code, description, class) values (309, 'Ringing insert into eFax_error_codes (code, description, class) values (double tone)', 'A')
  insert into eFax_error_codes (code, description, class) values (316, 'Answer detected probable human', 'A')
  insert into eFax_error_codes (code, description, class) values (317, 'Remote answered call', 'A')
  insert into eFax_error_codes (code, description, class) values (318, 'Dialtone remains after dial sequence', 'A')
  insert into eFax_error_codes (code, description, class) values (324, 'Silence insert into eFax_error_codes (code, description, class) values (no CNG detected)', 'A')
  insert into eFax_error_codes (code, description, class) values (325, 'Ringing timed out insert into eFax_error_codes (code, description, class) values (no answer)', 'A')
  insert into eFax_error_codes (code, description, class) values (326, 'Group 2 fax detected (cannot transmit)', 'C')
  insert into eFax_error_codes (code, description, class) values (327, 'Special Info. Tone: invalid # or svc', 'C')
  insert into eFax_error_codes (code, description, class) values (328, 'Possible dead line after dial', 'D')
  insert into eFax_error_codes (code, description, class) values (329, 'Special Info. Tone: invalid #', 'C')
  insert into eFax_error_codes (code, description, class) values (330, 'Special Info. Tone: reorder tone', 'C')
  insert into eFax_error_codes (code, description, class) values (331, 'Special Info. Tone: no circuit', 'C')
  insert into eFax_error_codes (code, description, class) values (332, 'CNG fax tone detected', 'C')
  insert into eFax_error_codes (code, description, class) values (333, 'Remote fax went off hook (digital only)', 'C')
  insert into eFax_error_codes (code, description, class) values (334, 'Special call progress result', 'C')
  insert into eFax_error_codes (code, description, class) values (339, 'Fax answer tone detected insert into eFax_error_codes (code, description, class) values (CED)', 'C')
  insert into eFax_error_codes (code, description, class) values (340, 'Unknown call progress result', 'C')
  insert into eFax_error_codes (code, description, class) values (349, 'ISDN call collision', 'C')
  insert into eFax_error_codes (code, description, class) values (421, 'Service not available', 'C')
  insert into eFax_error_codes (code, description, class) values (450, 'Mailbox not available', 'D')
  insert into eFax_error_codes (code, description, class) values (451, 'Server error', 'A')
  insert into eFax_error_codes (code, description, class) values (452, 'Server insufficient storage', 'A')
  insert into eFax_error_codes (code, description, class) values (500, 'SMTP command unrecognized (syntax error)', 'Z')
  insert into eFax_error_codes (code, description, class) values (501, 'SMTP syntax error in command arguments', 'Z')
  insert into eFax_error_codes (code, description, class) values (502, 'SMTP command not implemented', 'Z')
  insert into eFax_error_codes (code, description, class) values (503, 'SMTP commands in bad sequence', 'Z')
  insert into eFax_error_codes (code, description, class) values (504, 'SMTP command parameter not implemented', 'Z')
  insert into eFax_error_codes (code, description, class) values (550, 'Mailbox unavailable (e.g. mailbox not found)', 'E')
  insert into eFax_error_codes (code, description, class) values (551, 'User not local (try <forward address>)', 'F')
  insert into eFax_error_codes (code, description, class) values (552, 'Mailbox has exceeded storage allocation', 'G')
  insert into eFax_error_codes (code, description, class) values (553, 'Mailbox name invalid', 'H')
  insert into eFax_error_codes (code, description, class) values (554, 'SMTP transaction failed', 'Z')
  insert into eFax_error_codes (code, description, class) values (1040, 'The converter failed', 'E')
  insert into eFax_error_codes (code, description, class) values (4007, 'Unknown dial error (line not connected)', 'C')
  insert into eFax_error_codes (code, description, class) values (4009, 'Unknown error', 'E')
  insert into eFax_error_codes (code, description, class) values (4010, 'Error on infopkt file', 'E')
  insert into eFax_error_codes (code, description, class) values (4011, 'No error not all pages sent', 'E')
  insert into eFax_error_codes (code, description, class) values (4012, 'Unable to start fax receive', 'Z')
  insert into eFax_error_codes (code, description, class) values (4013, 'Unable to get remote info', 'F')
  insert into eFax_error_codes (code, description, class) values (4014, 'Unable to train', 'F')
  insert into eFax_error_codes (code, description, class) values (4015, 'Unable to open file for receive', 'Z')
  insert into eFax_error_codes (code, description, class) values (4016, 'Unable to process retry action', 'E')
  insert into eFax_error_codes (code, description, class) values (4017, 'Receive decline on sender CSID', 'Z')
  insert into eFax_error_codes (code, description, class) values (4018, 'Timeout on wait for call complete', 'B')
  insert into eFax_error_codes (code, description, class) values (4020, 'One or more pages was confirmed as RTN', 'F')
  insert into eFax_error_codes (code, description, class) values (4021, 'One or more pages not MCF RTP or RTN', 'F')
  insert into eFax_error_codes (code, description, class) values (4101, 'Ring detect without successful handshake', 'F')
  insert into eFax_error_codes (code, description, class) values (4102, 'Call aborted', 'E')
  insert into eFax_error_codes (code, description, class) values (4103, 'No loop current / A/B bits (hang up in send)', 'C')
  insert into eFax_error_codes (code, description, class) values (4104, 'ISDN disconnect', 'B')
  insert into eFax_error_codes (code, description, class) values (4111, 'No answer T.30 T1 timeout', 'F')
  insert into eFax_error_codes (code, description, class) values (4120, 'Unspecified transmit Phase B error', 'F')
  insert into eFax_error_codes (code, description, class) values (4121, 'Remote cannot receive or send', 'F')
  insert into eFax_error_codes (code, description, class) values (4122, 'COMREC error', 'F')
  insert into eFax_error_codes (code, description, class) values (4123, 'COMREC invalid command receivedd', 'F')
  insert into eFax_error_codes (code, description, class) values (4124, 'RSPREC error', 'F')
  insert into eFax_error_codes (code, description, class) values (4125, 'DCS sent 3 times without response', 'F')
  insert into eFax_error_codes (code, description, class) values (4126, 'DIS/DTC recd 3 times; DCS not recognized', 'F')
  insert into eFax_error_codes (code, description, class) values (4127, 'Failure to train', 'F')
  insert into eFax_error_codes (code, description, class) values (4128, 'RSPREC invalid response received', 'F')
  insert into eFax_error_codes (code, description, class) values (4129, 'DCN received in COMREC', 'F')
  insert into eFax_error_codes (code, description, class) values (4130, 'DCN received in RSPREC', 'F')
  insert into eFax_error_codes (code, description, class) values (4133, 'Incompatible fax formats (eg page width)', 'F')
  insert into eFax_error_codes (code, description, class) values (4134, 'Invalid DMA count specified for transmit', 'F')
  insert into eFax_error_codes (code, description, class) values (4135, 'BFT specified but no ECM enabled on xmit', 'F')
  insert into eFax_error_codes (code, description, class) values (4136, 'BFT specified but not supported by rcvr', 'F')
  insert into eFax_error_codes (code, description, class) values (4140, 'No response to RR after 3 tries', 'F')
  insert into eFax_error_codes (code, description, class) values (4141, 'No response to CTC or response not CTR', 'F')
  insert into eFax_error_codes (code, description, class) values (4142, 'T5 timout since receiving RNR', 'F')
  insert into eFax_error_codes (code, description, class) values (4143, 'Do not continue after receiving ERR', 'F')
  insert into eFax_error_codes (code, description, class) values (4144, 'ERR response to EOR-EOP or EOR-PRI-EOP', 'F')
  insert into eFax_error_codes (code, description, class) values (4145, 'Transmitted DCN after receiving RTN', 'F')
  insert into eFax_error_codes (code, description, class) values (4151, 'RSPREC error', 'F')
  insert into eFax_error_codes (code, description, class) values (4152, 'No response to MPS repeated 3 times', 'F')
  insert into eFax_error_codes (code, description, class) values (4153, 'Invalid response to MPS', 'F')
  insert into eFax_error_codes (code, description, class) values (4154, 'No response to EOP repeated 3 times', 'F')
  insert into eFax_error_codes (code, description, class) values (4155, 'Invalid response to EOP', 'F')
  insert into eFax_error_codes (code, description, class) values (4156, 'No response to EOM repeated 3 times', 'F')
  insert into eFax_error_codes (code, description, class) values (4157, 'Invalid response to EOM', 'F')
  insert into eFax_error_codes (code, description, class) values (4160, 'DCN received in RSPREC', 'F')
  insert into eFax_error_codes (code, description, class) values (4161, 'No response after 3 tries for PPS-NULL', 'F')
  insert into eFax_error_codes (code, description, class) values (4162, 'No response after 3 tries for PPS-MPS', 'F')
  insert into eFax_error_codes (code, description, class) values (4163, 'No response after 3 tries for PPS-EOP', 'F')
  insert into eFax_error_codes (code, description, class) values (4164, 'No response after 3 tries for PPS-EOMM', 'F')
  insert into eFax_error_codes (code, description, class) values (4165, 'No response after 3 tries for EOR-NULL', 'F')
  insert into eFax_error_codes (code, description, class) values (4166, 'No response after 3 tries for EOR-MPS', 'F')
  insert into eFax_error_codes (code, description, class) values (4167, 'No response after 3 tries for EOR-EOP', 'F')
  insert into eFax_error_codes (code, description, class) values (4168, 'No response after 3 tries for EOR-EOM', 'F')
  insert into eFax_error_codes (code, description, class) values (4173, 'T.30 T2 timeout, expected page not received', 'F')
  insert into eFax_error_codes (code, description, class) values (4174, 'T.30 T1 timeout after EOM received', 'F')
  insert into eFax_error_codes (code, description, class) values (4175, 'DCN received in COMREC', 'F')
  insert into eFax_error_codes (code, description, class) values (4202, 'COMREC invalid response received', 'F')
  insert into eFax_error_codes (code, description, class) values (4205, 'DCN received for command received', 'F')
  insert into eFax_error_codes (code, description, class) values (4208, 'Receive page count error in ECM mode', 'F')
  insert into eFax_error_codes (code, description, class) values (4251, 'Bad MMR data received from remote', 'F')
  insert into eFax_error_codes (code, description, class) values (4340, 'No interrupt acknowledge time-out', 'F')
  insert into eFax_error_codes (code, description, class) values (4341, 'Comm fault loop current still present', 'F')
  insert into eFax_error_codes (code, description, class) values (4342, 'T.30 holdup time-out', 'F')
  insert into eFax_error_codes (code, description, class) values (4343, 'DCN received from host in receive holdup', 'F')
  insert into eFax_error_codes (code, description, class) values (4600, 'Error interrupt problem with card', 'Z')
  insert into eFax_error_codes (code, description, class) values (4601, 'Unexpected overrun', 'Z')
  insert into eFax_error_codes (code, description, class) values (4602, 'Unexpected 03 or 7F interrupt', 'E')
  insert into eFax_error_codes (code, description, class) values (4603, 'IOCTL error', 'Z')
  insert into eFax_error_codes (code, description, class) values (4604, 'OVerlay DLOAD error', 'Z')
  insert into eFax_error_codes (code, description, class) values (4605, 'Max Timeout', 'F')
  insert into eFax_error_codes (code, description, class) values (5000, 'Unclassified API error', 'Z')
  insert into eFax_error_codes (code, description, class) values (5001, 'File I/O error', 'Z')
  insert into eFax_error_codes (code, description, class) values (5002, 'Bad file format', 'Z')
  insert into eFax_error_codes (code, description, class) values (5003, 'Board does not have required capability', 'Z')
  insert into eFax_error_codes (code, description, class) values (5004, 'Channel not in correct state', 'F')
  insert into eFax_error_codes (code, description, class) values (5005, 'Bad API parameter value', 'Z')
  insert into eFax_error_codes (code, description, class) values (5006, 'Memory allocation error', 'Z')
  insert into eFax_error_codes (code, description, class) values (5007, 'Channel not in required state', 'Z')
  insert into eFax_error_codes (code, description, class) values (5008, 'Dialling attempted too soon', 'F')
  insert into eFax_error_codes (code, description, class) values (6000, 'insert into eFax_error_codes (code, description, class) values (no cause available)', 'C')
  insert into eFax_error_codes (code, description, class) values (6001, 'UNASSIGNED_NUMBER', 'Z')
  insert into eFax_error_codes (code, description, class) values (6002, 'NO_ROUTE', 'B')
  insert into eFax_error_codes (code, description, class) values (6003, 'insert into eFax_error_codes (code, description, class) values (no cause available)', 'C')
  insert into eFax_error_codes (code, description, class) values (6006, 'CHANNEL_UNACCEPTABLE', 'C')
  insert into eFax_error_codes (code, description, class) values (6016, 'NORMAL_CLEARING', 'B')
  insert into eFax_error_codes (code, description, class) values (6017, 'USER_BUSY', 'B')
  insert into eFax_error_codes (code, description, class) values (6018, 'NO_USER_RESPONDING', 'A')
  insert into eFax_error_codes (code, description, class) values (6021, 'CALL_REJECTED', 'B')
  insert into eFax_error_codes (code, description, class) values (6022, 'NUMBER_CHANGED', 'C')
  insert into eFax_error_codes (code, description, class) values (6027, 'DEST_OUT_OF_ORDER', 'B')
  insert into eFax_error_codes (code, description, class) values (6028, 'INVALID_NUMBER_FORMAT', 'C')
  insert into eFax_error_codes (code, description, class) values (6029, 'FACILITY_REJECTED', 'C')
  insert into eFax_error_codes (code, description, class) values (6030, 'RESP_TO_STAT_ENQ', 'C')
  insert into eFax_error_codes (code, description, class) values (6031, 'UNSPECIFIED_CAUSE', 'C')
  insert into eFax_error_codes (code, description, class) values (6034, 'NO_CIRCUIT_AVAILABLE', 'B')
  insert into eFax_error_codes (code, description, class) values (6038, 'NETWORK_OUT_OF_ORDER', 'B')
  insert into eFax_error_codes (code, description, class) values (6041, 'TEMPORARY_FAILURE', 'B')
  insert into eFax_error_codes (code, description, class) values (6042, 'NETWORK_CONGESTION', 'B')
  insert into eFax_error_codes (code, description, class) values (6043, 'ACCESS_INFO_DISCARDED', 'C')
  insert into eFax_error_codes (code, description, class) values (6044, 'REQ_CHANNEL_NOT_AVAIL', 'E')
  insert into eFax_error_codes (code, description, class) values (6045, 'PRE_EMPTED', 'C')
  insert into eFax_error_codes (code, description, class) values (6050, 'FACILITY_NOT_SUBSCRIBED', 'B')
  insert into eFax_error_codes (code, description, class) values (6052, 'OUTGOING_CALL_BARRED', 'Z')
  insert into eFax_error_codes (code, description, class) values (6054, 'INCOMING_CALL_BARRED', 'Z')
  insert into eFax_error_codes (code, description, class) values (6058, 'BEAR_CAP_NOT_AVAIL', 'B')
  insert into eFax_error_codes (code, description, class) values (6063, 'SERVICE_NOT_AVAIL', 'B')
  insert into eFax_error_codes (code, description, class) values (6065, 'CAP_NOT_IMPLEMENTED', 'B')
  insert into eFax_error_codes (code, description, class) values (6066, 'CHAN_NOT_IMPLEMENTED', 'B')
  insert into eFax_error_codes (code, description, class) values (6069, 'FACILITY_NOT_IMPLEMENT', 'Z')
  insert into eFax_error_codes (code, description, class) values (6081, 'INVALID_CALL_REF', 'E')
  insert into eFax_error_codes (code, description, class) values (6082, 'CHAN_DOES_NOT_EXIST', 'E')
  insert into eFax_error_codes (code, description, class) values (6088, 'INCOMPATIBLE_DEST', 'Z')
  insert into eFax_error_codes (code, description, class) values (6095, 'INVALID_MSG_UNSPEC', 'E')
  insert into eFax_error_codes (code, description, class) values (6096, 'MANDATORY_IE_MISSING', 'E')
  insert into eFax_error_codes (code, description, class) values (6097, 'NONEXISTENT_MSG', 'E')
  insert into eFax_error_codes (code, description, class) values (6098, 'WRONG_MESSAGE', 'E')
  insert into eFax_error_codes (code, description, class) values (6099, 'BAD_INFO_ELEM', 'E')
  insert into eFax_error_codes (code, description, class) values (6100, 'INVALID_ELEM_CONTENTS', 'E')
  insert into eFax_error_codes (code, description, class) values (6101, 'WRONG_MSG_FOR_STATE', 'E')
  insert into eFax_error_codes (code, description, class) values (6102, 'TIMER_EXPIRY', 'E')
  insert into eFax_error_codes (code, description, class) values (6103, 'MANDATORY_IE_LEN_ERR', 'E')
  insert into eFax_error_codes (code, description, class) values (6111, 'PROTOCOL_ERROR', 'E')
  insert into eFax_error_codes (code, description, class) values (6127, 'INTERWORKING_UNSPEC', 'B')
  insert into eFax_error_codes (code, description, class) values (6401, 'Alerting message but timed out before connect', 'A')
  insert into eFax_error_codes (code, description, class) values (6402, 'Setup acknowledged but timed out before connect', 'A')
  insert into eFax_error_codes (code, description, class) values (6403, 'Progress message but timed out before connect', 'A')
  insert into eFax_error_codes (code, description, class) values (6404, 'Layer 2 - D-Channel went down', 'E')
  insert into eFax_error_codes (code, description, class) values (6405, 'Wait for complete was terminated unexpectedly', 'E')
  insert into eFax_error_codes (code, description, class) values (6406, 'Disconnect message occurred after connect message', 'E')
  insert into eFax_error_codes (code, description, class) values (6407, 'Outgoing call attempted but no response from network', 'E')
  insert into eFax_error_codes (code, description, class) values (6408, 'Special code used internally', 'E')
end
go
