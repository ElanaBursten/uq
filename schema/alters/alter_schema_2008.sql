/* New addin_info table to save data returned from the addin */
create table addin_info(
  addin_info_id int identity(50000,1) not null,
  foreign_type tinyint not null,
  foreign_id int not null,
  latitude decimal(9,6) null,
  longitude decimal(9,6) null,
  added_date datetime not null,
  added_by_id int not null,
  active bit not null default 1,
  modified_date datetime not null default (getdate()),
  primary key nonclustered (addin_info_id)
)
go

create index addin_info_foreign_id on addin_info (foreign_id)
go

alter table addin_info 
  add constraint addin_info_fk_employee foreign key (added_by_id) references employee (emp_id)
go
