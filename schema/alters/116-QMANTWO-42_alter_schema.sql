-- QMANTWO-42 Added Foreign SubType to Notes

--Add the subtype and remove the private checkbox
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'notes' and column_name = 'sub_type')
	ALTER TABLE notes add sub_type int NULL

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
          WHERE table_name = 'notes' and column_name = 'private')
    ALTER TABLE notes DROP COLUMN [private]

--NOTE: THIS IS A TEMPLATE AND DOES NOT CONTAIN ALL THE VALUES
--The default SubTypes should match the following (Descriptions can be changed). 
--Several automated processes write notes, therefore, they must be hardcoded. 

--ForeignType + '00':  Default/Public comment 
--ForeignType + '01':  Private
--ForeignType + '02':  High Profile
--All other codes can be assigned

--The last value in the update is the modifier. QManager does not use this field in regard to Note types.  
--However, the value for private is inserted as 1.  Change this back to 0 if it is not going to be used elsewhere.

select * from reference where type = 'notes_sub'

--Clear out previous entries for 'notes_sub'
--If EXISTS(SELECT * FROM Reference where type = 'notes_sub')
--  DELETE FROM Reference where type='notes_sub'


IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'notes' and COLUMN_NAME='sub_type')
    ALTER TABLE notes
      ADD sub_type int null; -- the first character (integer) will match the foregin type (Ticket, Locate, Work Order, etc.)

-- This is a sample of the values that should be added to reference table for notes_sub, change description as needed
--TICKET
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='100')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','100','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='101')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','101','Private Note - Ticket',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='102')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','102','High Priority - Ticket',2,1,GETDATE(),0) 
-----------------------------------------------------------------------------------------
--LOCATE
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='500')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','500','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='501')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','501','Private Note - Locate',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='502')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','502','High Priority - Locate',2,1,GETDATE(),0) 
-----------------------------------------------------------------------------------------
--DAMAGE
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='200')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','200','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='201')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','201','Private Note - Damage',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='202')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','202','High Priority - Damage',2,1,GETDATE(),0) 

-----------------------------------------------------------------------------------------
--3RD PARTY
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='300')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','300','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='301')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','301','Private Note - 3rd Party',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='302')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','302','High Priority - 3rd Party',2,1,GETDATE(),0) 

-----------------------------------------------------------------------------------------
--LITIGATION
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='400')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','400','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='401')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','401','Private Note - Litigation',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='402')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','402','High Priority - Litigation',2,1,GETDATE(),0) 

-----------------------------------------------------------------------------------------
--DAMAGE NARRAIVE
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='600')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','600','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='601')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','601','Private Note - Damage Nar',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='602')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','602','High Priority - Damage Nar',2,1,GETDATE(),0) 

-----------------------------------------------------------------------------------------
--WORK ORDERS
IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='700')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','700','Public (Default)',0,1,GETDATE(),0) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='701')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','701','Private Note - WO',1,1,GETDATE(),1) 

IF NOT EXISTS (SELECT type, code from reference where type='notes_sub' and code='702')
  INSERT INTO reference(type,code,description,sortby,active_ind,modified_date,modifier)
                 VALUES('notes_sub','702','High Priority - WO',2,1,GETDATE(),0) 

Select * from reference where type='notes_sub'
Select * from notes