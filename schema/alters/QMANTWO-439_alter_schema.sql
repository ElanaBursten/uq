--QMANTWO-439 ALTER work_order_work_type - for OHM (new data)
GO

INSERT INTO [dbo].[work_order_work_type]
           ([work_type]
           ,[work_description]
           ,[alert]
           ,[active]
           ,[modified_date])
     VALUES
           ('CVLID', 'Curb Valve lid damaged',0, 1, GetDate());
GO

----TEST for successful use of the new list item (it has been checked, saved and sync'd)
  --SELECT wo.wo_number, wor.work_type_id, wot.work_type, wot.work_description 
  --FROM work_order wo 
  --  JOIN work_order_remedy wor ON wo.wo_id = wor.wo_id
  --JOIN work_order_work_type wot ON wor.work_type_id = wot.work_type_id