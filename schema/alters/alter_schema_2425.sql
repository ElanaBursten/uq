-- Add departure_date column to the jobsite_arrival table if missing:
if not exists (select * from information_schema.columns 
  where table_name = 'jobsite_arrival' and column_name = 'departure_date')
    alter table jobsite_arrival add 
      departure_date datetime null,   /* user's time zone */
      departure_reason varchar(10)    /* DONE, NOWORK */
go

-- Insert new departure reason reference rows if missing:
if not exists (select * from reference where type='departrsn' and code='DONE')
  insert into reference (type, code, description)
    values ('departrsn', 'DONE', 'Ticket Work Done')
if not exists (select * from reference where type='departrsn' and code='NOWORK')
  insert into reference (type, code, description)
    values ('departrsn', 'NOWORK', 'Ticket Not Worked')
go
