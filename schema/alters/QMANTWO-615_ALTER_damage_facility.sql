/*
   Wednesday, July 18, 201812:34:39 PM
   User: 
   Server: DYATL-DQMGDB01
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_damage_facility
	(
	damage_facility_id int NOT NULL IDENTITY (1, 1),
	damage_id int NOT NULL,
	Facility_Type varchar(10) NULL,
	Facility_Size varchar(10) NULL,
	Facility_Material varchar(10) NULL,
	facility_pressure varchar(15) NULL,
	offset int NULL,
	added_by int NULL,
	added_date datetime NULL,
	deleted_by int NULL,
	deleted_date datetime NULL,
	modified_by int NULL,
	modified_date datetime NOT NULL,
	active bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_damage_facility SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_damage_facility ADD CONSTRAINT
	DF_damage_facility_modified_date DEFAULT 0 FOR modified_date
GO
ALTER TABLE dbo.Tmp_damage_facility ADD CONSTRAINT
	DF_damage_facility_active DEFAULT 1 FOR active
GO
SET IDENTITY_INSERT dbo.Tmp_damage_facility OFF
GO
IF EXISTS(SELECT * FROM dbo.damage_facility)
	 EXEC('INSERT INTO dbo.Tmp_damage_facility (damage_id, Facility_Type, Facility_Size, Facility_Material, modified_date)
		SELECT damage_id, Facility_Type, Facility_Size, Facility_Material, modified_date FROM dbo.damage_facility WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.damage_facility
GO
EXECUTE sp_rename N'dbo.Tmp_damage_facility', N'damage_facility', 'OBJECT' 
GO
ALTER TABLE dbo.damage_facility ADD CONSTRAINT
	PK_damage_facility PRIMARY KEY CLUSTERED 
	(
	damage_facility_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.damage_facility', 'Object', 'CONTROL') as Contr_Per 