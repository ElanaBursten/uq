--employee: home_lat
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='home_lat')
ALTER TABLE dbo.employee ADD
	home_lat decimal(9, 6) NULL

--employee: home_lng
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='home_lng')
ALTER TABLE dbo.employee ADD
	home_lng decimal(9, 6) NULL

--employee: alt_lat
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='alt_lat')
ALTER TABLE dbo.employee ADD
	alt_lat decimal(9, 6) NULL

--employee: alt_lng
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='alt_lng')
ALTER TABLE dbo.employee ADD
	alt_lng decimal(9, 6) NULL

ALTER TABLE dbo.employee SET (LOCK_ESCALATION = TABLE)
GO
