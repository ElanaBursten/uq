/**** Be sure to make a backup copy of the assignment_i trigger before 
      running this script in case it needs to be rolled back. To complete
      a rollback, you should also run the commands in this comment after 
      restoring the assignment_i trigger. 

  -- Undo commands:
  if object_id('dbo.timesheet_entry_iu') is not null
    drop trigger dbo.timesheet_entry_iu
  go
  create trigger timesheet_entry_u on timesheet_entry
  for update
  as
  begin
  	set nocount on

    update timesheet_entry set modified_date = GetDate()
    where entry_id in (select entry_id from inserted)
  end
  go
 
  if object_id('dbo.ticket_i') is not null
    drop trigger dbo.ticket_i
  go

 alter table timesheet_entry
    drop column work_pc_code
 go

 drop table locate_snap
 drop table ticket_snap
 go
*/

-- New Timesheet profit center field:
alter table timesheet_entry
  add work_pc_code varchar(15) null -- timesheet_entry update trigger
go

-- Remove the old timesheet update trigger & add new insert/update trigger to set work_pc_code
if object_id('dbo.timesheet_entry_u') is not null 
  drop trigger dbo.timesheet_entry_u 
go
if object_id('dbo.timesheet_entry_iu') is not null
  drop trigger dbo.timesheet_entry_iu 
go

create trigger timesheet_entry_iu on timesheet_entry
for insert, update
as
begin
  set nocount on

  update timesheet_entry set modified_date = GetDate()
    where entry_id in (select entry_id from inserted)

  update timesheet_entry 
    set work_pc_code = dbo.get_historical_pc(work_emp_id, work_date)
  where entry_id in (select entry_id from inserted)
    and work_pc_code is null 
    and (status='ACTIVE' or status='SUBMIT') 
end
go

/*
An insert trigger on ticket inserts a new ticket_snap row, 
populating ticket_id & first_receive_date.

An insert trigger on assignment updates the ticket_snap.first_receive_pc.

The locate_iu trigger executes dbo.update_ticket_status to set the 
ticket.kind field. When ticket.kind = 'DONE", update_ticket_status also sets 
ticket_snap's first_closed_date, first_close_emp_id, first_close_pc. 
*/

-- New ticket_snap table & indexes
create table ticket_snap (
  ticket_id integer not null primary key nonclustered,    -- ticket insert trigger
  first_receive_date datetime not null default GetDate(), -- ticket insert trigger
  first_receive_pc varchar(15) null,                   -- assignment insert trigger
  first_close_date datetime null,                      -- locate update trigger
  first_close_emp_id integer null,                     -- locate update trigger
  first_close_pc varchar(15) null                      -- locate update trigger
  )
go
create index first_receive_date on ticket_snap(first_receive_date)
create index first_close_date on ticket_snap(first_close_date)

alter table ticket_snap
  add constraint ticket_snap_fk_ticket foreign key (ticket_id)
  references ticket(ticket_id)
alter table ticket_snap
  add constraint ticket_snap_fk_employee foreign key (first_close_emp_id)
  references employee(emp_id)
alter table ticket_snap
  add constraint ticket_snap_fk_receive_pc foreign key (first_receive_pc)
  references profit_center(pc_code)
alter table ticket_snap
  add constraint ticket_snap_fk_close_pc foreign key (first_close_pc)
  references profit_center(pc_code)
go

/*
An insert trigger on assignment inserts a new locate_snap row, populating 
locate_id, first_receive_pc, & first_receive_date.

An update trigger on locate sets locate_snap's first_closed_date, 
first_close_emp_id, & first_close_pc. 
*/

-- New locate_snap table & indexes
create table locate_snap (
  locate_id integer not null primary key nonclustered,    -- assignment insert trigger
  first_receive_date datetime not null default GetDate(), -- assignment insert trigger
  first_receive_pc varchar(15) null,                      -- assignment insert trigger
  first_close_date datetime null,                         -- locate update trigger
  first_close_emp_id integer null,                        -- locate update trigger
  first_close_pc varchar(15) null                         -- locate update trigger
  )
go

create index first_receive_date on locate_snap(first_receive_date)
create index first_close_date on locate_snap(first_close_date)

alter table locate_snap
  add constraint locate_snap_fk_locate foreign key (locate_id)
  references locate(locate_id)
alter table locate_snap
  add constraint locate_snap_fk_employee foreign key (first_close_emp_id)
  references employee(emp_id)
alter table locate_snap
  add constraint locate_snap_fk_receive_pc foreign key (first_receive_pc)
  references profit_center(pc_code)
alter table locate_snap
  add constraint locate_snap_fk_close_pc foreign key (first_close_pc)
  references profit_center(pc_code)
go

/*
New ticket insert trigger to create ticket_snap row & set first_recveive_date:
*/
if object_id('dbo.ticket_i') is not null
  drop trigger dbo.ticket_i
go

create trigger ticket_i on ticket
for insert 
as begin
  set nocount on

  insert into ticket_snap (ticket_id, first_receive_date)
    select inserted.ticket_id, GetDate()
    from inserted left join ticket_snap on inserted.ticket_id = ticket_snap.ticket_id
    where ticket_snap.ticket_id is null
end
go

/* 
Updated assignemnt trigger to create locate_snap row & set first_receive* fields.
It also sets the ticket_snap.first_receive_pc
*/
if object_id('dbo.assignment_i') is not null
  drop trigger dbo.assignment_i
GO

CREATE TRIGGER assignment_i ON assignment
FOR INSERT
AS
BEGIN
  SET NOCOUNT ON

  UPDATE locate SET
    assigned_to =
       case when locate.closed = 1 then null
            else inserted.locator_id end,
    assigned_to_id = inserted.locator_id
  FROM inserted
  WHERE
    locate.locate_id = inserted.locate_id

  update ticket
    set recv_manager_id =
      (select max(e.report_to)
         from employee e where e.emp_id=locate.assigned_to)
    from locate
    where locate.locate_id in (select locate_id from inserted)
     and ticket.ticket_id = locate.ticket_id
     and ticket.recv_manager_id is null
  
  -- 1849 change starts here:
  update ticket_snap
    set first_receive_pc = dbo.get_historical_pc(locate.assigned_to_id, first_receive_date)
    from locate 
    where locate.locate_id in (select locate_id from inserted)
      and ticket_snap.ticket_id = locate.ticket_id
      and ticket_snap.first_receive_pc is null
  
  insert into locate_snap (locate_id, first_receive_date, first_receive_pc)
    select inserted.locate_id, GetDate(), dbo.get_historical_pc(inserted.locator_id, GetDate())
    from inserted left join locate_snap on inserted.locate_id = locate_snap.locate_id
    where locate_snap.locate_id is null 
  -- end 1849

END
GO
