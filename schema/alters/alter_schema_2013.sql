/* Add new locating_company field if its not there already: */
if (select count(*) from information_schema.columns 
  where table_schema='dbo' and table_name='locating_company' and column_name='payroll_company_code') = 0 begin
    alter table locating_company add payroll_company_code varchar(20) null
end
go

/* set the initial values used in the ADP payroll export as of 4/2/2008 */
update locating_company  set payroll_company_code = 'P3H' 
  where name = 'UtiliQuest' and payroll_company_code is null
update locating_company  set payroll_company_code = 'M1U' 
  where name = 'STS' and payroll_company_code is null
update locating_company  set payroll_company_code = 'M1V' 
  where name = 'UTI' and payroll_company_code is null
update locating_company  set payroll_company_code = 'M1T' 
  where name = 'Locating Inc.' and payroll_company_code is null


/* Create the new payroll_export_adp4 sp */
if object_id('dbo.payroll_export_adp4') is not null
  drop procedure dbo.payroll_export_adp4
GO

create proc payroll_export_adp4 (
  @begin_date datetime,
  @end_date datetime,
  @region varchar(16))
as

SET NOCOUNT ON

if (@region is not null) and (not exists (select pc_code from profit_center where pc_code = @region))
  RAISERROR ('The payroll profit center %s was not found in the profit_center table. Please add it using the QManager administration tool.', 15, 1, @region)

-- Chop off any time part
set @end_date = convert(datetime, convert(varchar(10), @end_date, 102), 102)

-- Make sure we landed on a Saturday, in case the :59:59:999 rounded to Sunday
while DatePart(DW, @end_date) <> 7
  set @end_date = DateAdd(d, -1, @end_date)

DECLARE @emps TABLE (
  _emp_id int not null primary key,
  emp_number varchar(25) not null,
  company varchar(4),
  last_name varchar(30),
  time_rule varchar(10),
  charge_cov bit,
  adp_code varchar(2)
)

DECLARE @time_data TABLE  (
  e_id int not null,
  m_id int not null,
  p_ctr varchar(16) not null,
  reg_hours decimal(8,2),
  ot_hours decimal(8,2),
  dt_hours decimal(8,2),
  br_hours decimal(8,2),
  callout_hours decimal(8,2),
  vac_hours decimal(8,2),
  leave_hours decimal(8,2),
  hol_hours decimal(8,2),
  jury_hours decimal(8,2),
  pov_hours decimal(8,2),
  cov_hours decimal(8,2),
  mileage decimal(8,2),
  cov_days int
)

DECLARE @emp_hours_summary TABLE (
  emp_id int not null primary key,
  max_hours_mgr int,
  max_hours_pc varchar(16),
  total_reg_hours decimal(8,2),
  total_ot_hours decimal(8,2),
  total_dt_hours decimal(8,2),
  total_pov_hours decimal(8,2),
  total_other_hours decimal(8,2)
)

DECLARE @results TABLE  (
  co_code varchar(20),
  emp_id int not null,   -- internal only, not exported
  mgr_id int not null,   -- internal only, not exported
  pc_code varchar(16) not null,
  line int not null,     -- internal only, not exported
  reg_hours decimal(8,2),
  ot_hours decimal(8,2),
  hours_3_code varchar(5),
  hours_3 decimal(8,2),
  earnings_3_code varchar(5),
  earnings_3 decimal(8,2)
)

-- Collect the emps for the profit center
insert into @emps 
  select e.emp_id, 
    dbo.left_fill(e.emp_number, '0', 6),
    -- IsNull(pd.emp_pc_code, '000'), 
    IsNull(lc.payroll_company_code, '---'),
    e.last_name, tr.code, e.charge_cov, pc.adp_code
    from dbo.employee_payroll_data(@region) pd
    inner join employee e on (e.emp_id=pd.emp_id)
    inner join reference tr on (e.timerule_id=tr.ref_id)
    inner join profit_center pc on (pc.pc_code=pd.emp_pc_code)
    inner join locating_company lc on (lc.company_id=e.company_id)
  where e.emp_number is not null

-- collect the summarized hours data for the period by employee, manager, & profit center
insert into @time_data
  select e._emp_id, 
    emphist.report_to,
    Coalesce(emphist.payroll_pc_code, emphist.active_pc) as pc_code,
    Round((Sum(IsNull(tse.reg_hours, 0)) + Sum(IsNull(tse.ot_hours, 0)) + Sum(IsNull(tse.dt_hours, 0))), 1),
    Round(Sum(IsNull(tse.ot_hours, 0)), 1), 
    Round(Sum(IsNull(tse.dt_hours, 0)), 1),
    Sum(IsNull(tse.br_hours,0)),    
    Round(Sum(IsNull(tse.callout_hours,0)), 1),
    Sum(IsNull(tse.vac_hours,0)),
    Sum(IsNull(tse.leave_hours,0)),
    Sum(IsNull(tse.hol_hours,0)),
    Sum(IsNull(tse.jury_hours,0)),
--    Sum(
--      case
--        when tse.vehicle_use = 'POV' then Round(IsNull(tse.reg_hours,0) + IsNull(tse.ot_hours,0) + IsNull(tse.dt_hours,0) + IsNull(tse.callout_hours,0),1)
--        else 0
--      end),
    0, -- removed POV hours from export -JT 09/27/2007
    Sum(
      case
        when tse.vehicle_use like 'COV%' then Round(IsNull(tse.reg_hours,0) + IsNull(tse.ot_hours,0) + IsNull(tse.dt_hours,0) + IsNull(tse.callout_hours,0),1)
        else 0
      end),
    Sum(
      case
        when tse.vehicle_use = 'POV' then (Coalesce(tse.miles_stop1,0) - Coalesce(tse.miles_start1,0))
        else 0
      end),
    Sum(
      case 
        when (tse.vehicle_use like 'COV%' or (e.charge_cov = 1 and tse.vehicle_use not like 'POV%'))
              and IsNull(tse.reg_hours,0) + IsNull(tse.ot_hours,0) + IsNull(tse.dt_hours,0) + IsNull(tse.callout_hours,0) > 0 then 1
        else 0
      end) -- number of days in the period where emp has some production hours & COV,
  from timesheet_entry tse
  inner join @emps e on tse.work_emp_id = e._emp_id
  inner join employee_history emphist on e._emp_id = emphist.emp_id
  where tse.work_date between @begin_date and @end_date
    and emphist.active_start <= tse.work_date and emphist.active_end > tse.work_date
    and tse.final_approve_by is not null
    and tse.status <> 'OLD'
    and (IsNull(tse.reg_hours,0) +
      IsNull(tse.ot_hours,0) +
      IsNull(tse.dt_hours,0) +
      IsNull(tse.callout_hours,0) +
      IsNull(tse.vac_hours,0) +
      IsNull(tse.leave_hours,0) +
      IsNull(tse.br_hours,0) +
      IsNull(tse.hol_hours,0) +
      IsNull(tse.jury_hours,0)) > 0
   group by e._emp_id, emphist.payroll_pc_code, emphist.active_pc, emphist.report_to

-- Remove 0-hour rows, so that employees with no time will not appear  */
delete from @time_data where 
    (IsNull(reg_hours,0) +
      IsNull(ot_hours,0) +
      IsNull(dt_hours,0) +
      IsNull(callout_hours,0) +
      IsNull(vac_hours,0) +
      IsNull(leave_hours,0) +
      IsNull(br_hours,0) +
      IsNull(hol_hours,0) +
      IsNull(jury_hours,0)) = 0

update @time_data set
  -- Salaried & Salaried Exempt emps don't get DT
  dt_hours = case time_rule 
    when 'SAL' then 0 
    when 'SALNEX' then 0 
    else dt_hours end,
  -- Salaried & Salaried Exempt emps don't get Callouts
  callout_hours = case time_rule 
    when 'SAL' then 0 
    when 'SALNEX' then 0
    else callout_hours end,
  -- Salaried emps don't get OT
  ot_hours = case time_rule when 'SAL' then 0 else ot_hours end
  from @emps  
  where e_id = _emp_id

insert into @emp_hours_summary (emp_id, total_reg_hours, total_ot_hours, total_dt_hours, total_pov_hours, total_other_hours)
  select e_id, sum(reg_hours), sum(ot_hours), sum(dt_hours), sum(pov_hours), sum(vac_hours + leave_hours + br_hours + hol_hours + jury_hours)
    from @time_data 
    group by e_id

-- save the mgr & profit center associated with the highest reg hours for the period
update @emp_hours_summary
  set max_hours_mgr = (select top 1 m_id from @time_data where e_id = emp_id order by reg_hours desc),
  max_hours_pc = (select top 1 p_ctr from @time_data where e_id = emp_id order by reg_hours desc)

update @time_data
  -- all ot goes to the mgr / pc with the most reg hours
  set ot_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc) then total_ot_hours 
    else 0 
  end,
  -- all dt goes to the mgr / pc with the most reg hours
  dt_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc) then total_dt_hours 
    else 0 
  end,
  -- reg hour adjustments go to the mgr / pc with the most reg hours
  reg_hours = case 
    when (((time_rule = 'SAL' or time_rule = 'SALNEX') and total_reg_hours <> 40) 
      and m_id = max_hours_mgr and p_ctr = max_hours_pc) then 40 - (total_reg_hours - reg_hours) - total_other_hours
    else reg_hours
  end,
	-- *** Note that pov is NOT currently used ***
  -- pov hour adjustments go to the mgr / pc with the most reg hours
  pov_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc and total_pov_hours > 42) then 42 - (total_pov_hours - pov_hours)
    else pov_hours
  end
  from @emp_hours_summary inner join @emps on _emp_id = emp_id
  where e_id = emp_id 
  
-- make sure the last update didn't make reg_hours negative
update @time_data set reg_hours = 0 where reg_hours < 0
  
-- *** Populate the result set 
-- Insert row 1
insert into @results (emp_id, mgr_id, pc_code, line, reg_hours, ot_hours)
  select e_id, m_id, p_ctr, 1, reg_hours, ot_hours
  from @time_data 

-- Update row 1 with Double-time if needed
update @results
  set hours_3_code = 'DT', hours_3 = D.dt_hours
  from @time_data D 
  where emp_id = D.e_id and mgr_id = D.m_id and pc_code = D.p_ctr
  and line = 1 and D.dt_hours <> 0

-- Insert "POV/COV" row(s)
-- the line 5 is just to make it sort after the other stuff
insert into @results (emp_id, mgr_id, pc_code, line, earnings_3_code, earnings_3)
  select e_id, m_id, p_ctr, 5, 'CAR', (cov_days * 3.0)
    from @time_data
    where cov_days <> 0

insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 5, 'CR3', pov_hours
    from @time_data 
    where pov_hours <> 0
  
insert into @results (emp_id, mgr_id, pc_code, line, earnings_3_code, earnings_3)
  select e_id, m_id, p_ctr, 5, 'MIL', (mileage * 0.41)
    from @time_data
    where mileage <> 0

-- Insert other details based on hours data:
-- callouts
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'CLT', callout_hours
    from @time_data
    where callout_hours <> 0
-- vacation
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'VAC', vac_hours
    from @time_data
    where vac_hours <> 0
-- bereavement
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'BER', br_hours
    from @time_data
    where br_hours <> 0
-- leave
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'SCK', leave_hours
    from @time_data
    where leave_hours <> 0
-- holiday
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'HOL', hol_hours
    from @time_data
    where hol_hours <> 0
-- jury duty
insert into @results (emp_id, mgr_id, pc_code, line, hours_3_code, hours_3)
  select e_id, m_id, p_ctr, 2, 'JUR', jury_hours
    from @time_data
    where jury_hours <> 0

-- update company
update @results 
  set co_code = e.company
  from @emps e where e._emp_id = emp_id

-- Output summary 
select co_code, count(*) 'row_count' 
  from @results
  group by co_code

-- Output details, label fields per requirements for simple exporting
select co_code AS 'co code',
  e.adp_code,   -- not output, used for naming the csv file
  r.pc_code AS 'batch ID',
  e.emp_number AS 'file #',
  r.pc_code + dbo.left_fill(m.emp_number, '0', 6) + e.emp_number as 'job code',
  reg_hours AS 'reg hours',
  ot_hours AS 'o/t hours',
  hours_3_code AS 'hours 3 code',
  hours_3 AS 'hours 3 amount',
  earnings_3_code AS 'earnings 3 code',
  earnings_3 AS 'earnings 3 amount'
 from @results r
  inner join @emps e on e._emp_id=r.emp_id
  inner join employee m on mgr_id = m.emp_id
 order by co_code, r.pc_code, e.emp_number, r.line, r.earnings_3_code

-- Extra results helpful for debugging:
/*
select 'Extra Debugging Output - NOT for Production!'
select * from @emps
select * from @time_data
select * from @emp_hours_summary
*/
   
GO
grant execute on payroll_export_adp4 to uqweb, QManagerRole

/* Try it out with:
exec dbo.payroll_export_adp4 '2008-03-23', '2008-03-29', null -- everyone at once
exec dbo.payroll_export_adp4 '2007-09-02', '2007-09-08', 'FMW'
exec dbo.payroll_export_adp4 '2008-03-23', '2008-03-29', '007'
exec dbo.payroll_export_adp3 '2008-03-23', '2008-03-29', '007'

-- compare results with the Solomon export
exec dbo.payroll_export_adp4 '2007-09-02', '2007-09-08', 'FXL'
exec dbo.payroll_export_solomon2 '2007-09-02', '2007-09-08', 'FXL'
-- this pc has multiple companies
exec dbo.payroll_export_adp4 '2007-09-02', '2007-09-08', '004'
exec dbo.payroll_export_adp3 '2007-09-02', '2007-09-08', '004'
exec dbo.payroll_export_solomon2 '2007-09-02', '2007-09-08', '004'

-- ***************************************   Notes:   *********************************************

-- This query output gets transformed in to TSV in a generic way, so
-- the fields, sorting, etc. are all specified here, not in the transformation code

-- to get the list of time types:
select ref_id, code, description from reference where type='timetype' or type='timemisc'

-- get list of employees in a payroll profit center (NULL region gets them all):
declare @region varchar(20)
set @region='FNJ'
select E.emp_id, E.emp_number, E.last_name, E.short_name, PD.emp_pc_code, E.company_id
 from dbo.employee_payroll_data(@region) PD
inner join employee E on (E.emp_id=PD.emp_id)
where E.emp_number is not null

*/
