-- add column to attachment table
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'attachment' and COLUMN_NAME='doc_type')
  alter table attachment add doc_type varchar(15) null
go

-- document table holds the data pertinent to required documents
-- currently this is only used for damages, but is left open for
-- use by other pieces later (tickets, locates, etc...)

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'document')
  create table document (
    doc_id int identity not null,
    foreign_type int not null,
    foreign_id int not null,
    required_doc_id int null,
    comment varchar(250) null,
    added_date datetime not null,
    added_by_id int not null,
    active bit null default 1,
    modified_date datetime not null default getdate(),
    constraint pk_document primary key (doc_id)
  )
GO
IF NOT EXISTS (select * from sys.indexes where name = 'document_required_doc_id')
  create index document_required_doc_id on document(required_doc_id)
IF NOT EXISTS (select * from sys.indexes where name = 'document_foreign_id')
  create index document_foreign_id on document(foreign_id)
GO

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'required_document')
  create table required_document (
    required_doc_id int identity not null,
    foreign_type int not null,
    name varchar(100) null,
    require_for_locate bit not null,
    require_for_no_locate bit not null,
    require_for_min_estimate decimal(12,2) not null default 0,
    user_selectable bit not null,
    active bit not null default 1,  
    modified_date datetime not null default getdate(),
    constraint pk_required_document primary key (required_doc_id)
  )
GO

IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'required_document_type')
  create table required_document_type (
    rdt_id int identity not null,
    required_doc_id int not null,
    doc_type varchar(15) not null,
    active bit not null default 1,
    modified_date datetime not null default getdate(),
    constraint pk_required_document_type primary key (rdt_id)
    )
 GO

IF NOT EXISTS (select * from sys.indexes where name = 'required_document_type_doc_type')
  create unique index required_document_type_doc_type on required_document_type (required_doc_id, doc_type)
GO

-- constraints for new tables:
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE 
  where TABLE_NAME = 'document' and CONSTRAINT_NAME = 'document_fk_required_document')
  alter table document 
    add constraint document_fk_required_document 
    foreign key (required_doc_id) references required_document(required_doc_id)
go

if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE 
  where TABLE_NAME = 'document' and CONSTRAINT_NAME = 'document_fk_employee')
  alter table document 
    add constraint document_fk_employee
    foreign key (added_by_id) references employee(emp_id)

if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE 
  where TABLE_NAME = 'required_document_type' and CONSTRAINT_NAME = 'required_document_type_fk_required_document')
  alter table required_document_type
    add constraint required_document_type_fk_required_document
    foreign key (required_doc_id) references required_document(required_doc_id)
go

-- triggers for new tables:
if exists(select name FROM sysobjects where name = 'required_document_u' AND type = 'TR')
  drop trigger required_document_u
GO
create trigger required_document_u on required_document
for update
AS
begin
  update required_document set modified_date = getdate()
   where required_doc_id in (select required_doc_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'document_u' AND type = 'TR')
  drop trigger document_u
GO
create trigger document_u on document
for update
AS
begin
  update document set modified_date = getdate()
   where doc_id in (select doc_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'required_document_type_u' AND type = 'TR')
  drop trigger required_document_type_u
GO
create trigger required_document_type_u on required_document_type
for update
AS
begin
  update required_document_type set modified_date = getdate()
   where rdt_id in (select rdt_id from inserted)
end
go

-- (re)insert intitial doctype reference data
delete from reference where (type = 'doctype')
insert into reference (type, code, description) values ('doctype', 'LOCPHOTO',  'Locate Photo')
insert into reference (type, code, description) values ('doctype', 'DMGPHOTO',  'Damage Photo')
insert into reference (type, code, description) values ('doctype', 'INVSKETCH', 'Investigator Sketch')
insert into reference (type, code, description) values ('doctype', 'MANIFEST',  'Manifest')
insert into reference (type, code, description) values ('doctype', 'FACPRINT',  'Facility Print')
insert into reference (type, code, description) values ('doctype', 'TICSEARCH', 'Ticket Search')
insert into reference (type, code, description) values ('doctype', 'EMAIL',     'E-Mail')
insert into reference (type, code, description) values ('doctype', 'SUMMARY',   'Summary')
insert into reference (type, code, description) values ('doctype', 'INVOICE',   'Invoice')
insert into reference (type, code, description) values ('doctype', 'VIDEO',     'Video')
insert into reference (type, code, description) values ('doctype', 'HPRPT',     'HP Report')
insert into reference (type, code, description) values ('doctype', 'COMPLETE',  'Complete')
insert into reference (type, code, description) values ('doctype', 'LITGRDOC',  'Litigator Document')
insert into reference (type, code, description) values ('doctype', 'HIGHPROF',  'High Profile')
insert into reference (type, code, description) values ('doctype', 'LSARPT',    'LSA Report')
insert into reference (type, code, description) values ('doctype', 'DPSSTMT',   'DPS Statement')
insert into reference (type, code, description) values ('doctype', 'OTHER',     'Other')

--Insert Intial data into required_document table
delete from required_document_type -- so all required_document rows can be deleted
delete from required_document
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Pre-excavation Photos/Videos',1,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Facility Maps/Records',1,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Damage Photos/Videos',1,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Investigative Sketch',0,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Ticket Search/Ticket Image',0,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'E-Sketch/Virtual or Manual Manifest',1,1,0,0)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'High Profile Notification',1,1,5000.00,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Locate Status Activity Report',1,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'DPS Locate Statement',1,1,0,1)
insert into required_document (foreign_type, name, require_for_locate, require_for_no_locate, require_for_min_estimate, user_selectable) 
  values (2,'Utility Damage Investigation Summary',1,1,0,1)

--Insert Intial data into required_document_type table
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'LOCPHOTO' from required_document where name = 'Pre-excavation Photos/Videos'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'FACPRINT' from required_document where name = 'Facility Maps/Records'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'DMGPHOTO' from required_document where name = 'Damage Photos/Videos'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'VIDEO' from required_document where name = 'Damage Photos/Videos'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'INVSKETCH' from required_document where name = 'Investigative Sketch'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'TICSEARCH' from required_document where name = 'Ticket Search/Ticket Image'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'EMAIL' from required_document where name = 'Ticket Search/Ticket Image'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'MANIFEST' from required_document where name = 'E-Sketch/Virtual or Manual Manifest'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'HPRPT' from required_document where name = 'High Profile Notification'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'HIGHPROF' from required_document where name = 'High Profile Notification'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'LSARPT' from required_document where name = 'Locate Status Activity Report'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'DPSSTMT' from required_document where name = 'DPS Locate Statement'
insert into required_document_type (required_doc_id, doc_type) 
  select required_doc_id, 'SUMMARY' from required_document where name = 'Utility Damage Investigation Summary'
go

