--Add list of Gas Light choices
if exists (select * from reference where type = 'gaslight')
  delete from reference where type = 'gaslight'
INSERT INTO reference (type, code, description) values ('gaslight', 'ACTLOC', 'Active Locateable')
INSERT INTO reference (type, code, description) values ('gaslight', 'ACTUNLOC', 'Active Unlocateable')
INSERT INTO reference (type, code, description) values ('gaslight', 'INLOC', 'Inactive Locatable')
INSERT INTO reference (type, code, description) values ('gaslight', 'INUNLOC', 'Inactive Unlocateable')
INSERT INTO reference (type, code, description) values ('gaslight', 'NOGASLAMP', 'No gas lamp')
INSERT INTO reference (type, code, description) values ('gaslight', 'B', '')  

--Add Gas Light column
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='gas_light')
  alter table work_order_inspection add gas_light varchar(15) null
GO  

/*To rollback:
-- drop the new columns
    
if exists (select * from information_schema.columns
  where table_name = 'work_order_inspection' and column_name = 'gas_light')
    alter table work_order_inspection drop column gas_light
GO

*/
