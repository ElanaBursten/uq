-- Remove the CONTROL permission from QManagerRole db role and uqweb db user

if exists (select prin.name, perm.* from sys.database_permissions perm
  inner join sys.database_principals prin on perm.grantee_principal_id = prin.principal_id
  where prin.name in ('QManagerRole') and perm.permission_name = 'CONTROL' and perm.state = 'G')
    REVOKE CONTROL TO QManagerRole AS dbo
go

if exists (select prin.name, perm.* from sys.database_permissions perm
  inner join sys.database_principals prin on perm.grantee_principal_id = prin.principal_id
  where prin.name in ('uqweb') and perm.permission_name = 'CONTROL' and perm.state = 'G')
    REVOKE CONTROL TO uqweb AS dbo
go

-- Add grants to functions that might be missing them
grant execute on get_emp_fl_holidays to uqweb, QManagerRole;
grant execute on CheckDuplicateTimesheetEntry to uqweb, QManagerRole;
grant execute on get_locator_for_damage to uqweb, QManagerRole;
go

-- Grant service broker related permissions to uqweb
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO uqweb, QManagerRole;
GRANT RECEIVE ON NewEventReceiveQueue TO uqweb, QManagerRole;
GRANT SEND ON SERVICE::NewEventSendService TO uqweb, QManagerRole;
GO
