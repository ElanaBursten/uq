USE [QM]
GO

Create table attachment_queue (attachment_id int, insert_date datetime)
Go

----Created index on attachment_id for new table attachment_queue
CREATE NONCLUSTERED INDEX [index_attachment_id_attachment_queue] ON [dbo].[attachment_queue]
(
	[attachment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[tgr_attachment_upload_date_iu] ON [dbo].[attachment] 
FOR INSERT,UPDATE
AS
BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	
	declare @attachment_id int;
	declare @new_upload_date datetime;
	declare @old_upload_date datetime;
	declare @source varchar(8);
	
	SELECT   @new_upload_date= i.upload_date,@attachment_id = i.attachment_id,
			 @old_upload_date = d.upload_date , @source = i.source
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.attachment_id = d.attachment_id
			
IF @source not in ('AUTOAWS','MANAWS') ----Check if source is neither AUTOAWS nor MANAWS
BEGIN
	IF @old_upload_date is NULL and @new_upload_date is not null -----Check if there is any upload date
	BEGIN
		insert into attachment_queue (attachment_id,insert_date)
				values (@attachment_id,getdate())
	END	
END
END

GO
