/* Add workload_date column to assignment table. */
if not exists (select * from information_schema.columns where table_name='assignment' and column_name='workload_date')
  alter table assignment add workload_date datetime null
go


/* To rollback these changes:
-- drop the new assignment.workload_date column
if exists (select * from information_schema.columns where table_name='assignment' and column_name='workload_date')
  alter table assignment drop column workload_date
go
*/

