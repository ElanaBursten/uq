USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[AddChildClient]    Script Date: 12/7/2018 11:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 BP
-- Create date:  12/5/2018
-- Description:	 Clones a child row to Client table
--                Sets parent_client_id and alters oc_code (appending -P01-P99) 
-- =============================================
ALTER PROCEDURE [dbo].[AddChildClient] 
  -- parameter for stored procedure
     @oc_code varchar(10)
AS
BEGIN
  IF LEN(ISNULL(@oc_code, '')) = 0
  BEGIN
    RAISERROR('No value "%s" passed for @oc_code.', 16, -1, @oc_code)
    RETURN
   END

 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @err int
  Declare @MaxClient Integer
  Declare @Temp VarChar(10)
  Declare @AppendVal VarChar(4)

  Select * Into #TempTable From client Where oc_code = @oc_code
  IF @@ROWCOUNT = 0
  BEGIN
    RAISERROR('No match found for @oc_code.', 16, -1, @oc_code)
    RETURN 50000
   END 
  SELECT @err = @@error IF @err <> 0 RETURN @err
   
  SELECT @MaxClient = Max(Substring(right(oc_code, 2), Patindex('%[0-9]%', right(oc_code,2)),len(right(oc_code,2))))
    From client Where oc_code like Substring(@oc_code,1,6) + '-P%'   
	and active = 1
       
  If @MaxClient is NULL
  BEGIN 
    Select @AppendVal = '-P01'
    Select @Temp = Substring(@oc_code,1,6) + @AppendVal
  END
  Else 
  Begin
    If @MaxClient >= 9
	BEGIN
	  Select @AppendVal = '-P' + Cast(@MaxClient + 1 As Varchar(2))
	  Select @Temp = Substring(@oc_code,1,6) + @AppendVal
	END
	Else
	BEGIN 
	  Select @AppendVal = '-P0' + Cast(@MaxClient + 1 As Varchar(1))
	  Select @Temp = Substring(@oc_code,1,6) + @AppendVal
	END
  End

  Update #TempTable set oc_code = @Temp, client_name = client_name + @AppendVal, parent_client_id = (Select client_id from #TempTable)
  SELECT @err = @@error IF @err <> 0 RETURN @err
  Alter Table #TempTable Drop Column [client_id]
  SELECT @err = @@error IF @err <> 0 RETURN @err

  BEGIN TRANSACTION
  Insert Into client Select * From #TempTable;
  SELECT @err = @@error IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END
  COMMIT TRANSACTION
  SELECT @err = @@error IF @err <> 0 RETURN @err
END

