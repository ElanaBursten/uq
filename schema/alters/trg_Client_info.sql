USE [QM]
GO

/****** Object:  Trigger [client_info_u]    Script Date: 10/18/2018 2:33:35 PM ******/
DROP TRIGGER [dbo].[client_info_u]
GO

/****** Object:  Trigger [dbo].[client_info_u]    Script Date: 10/18/2018 2:33:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- add missing client_info update trigger
create trigger [dbo].[client_info_u] on [dbo].[client_info]
for update
AS
  update client_info set modified_date = getdate() where client_info_id in (select client_info_id from inserted)

GO

