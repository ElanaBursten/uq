-- 2446: Add billing_holiday column to holidays table.
if not exists (select * from INFORMATION_SCHEMA.columns where table_name = 'holiday' and column_name = 'billing_holiday')
  alter table holiday add billing_holiday bit not null default 1 -- treat as a holiday (ie. After Hours) when billing
go
