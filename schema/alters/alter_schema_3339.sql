
-- Create new WGL statuses
if not exists (select * from statuslist where status in ('CGA','DONE','ILS','529'))
begin
	insert into statuslist (status,status_name,complete,active)
	values 
	('529','Painted Meter',0,1)
	insert into statuslist (status,status_name,complete,active)
	values 
	('CGA','Cannot Gain Access',0,1)
	insert into statuslist (status,status_name,complete,active)
	values 
	('DONE','Pos No Issues Found',1,1)
	insert into statuslist (status,status_name,complete,active)
	values 
	('ILS','Inside Leak Survey',0,1)
end
GO
--Create the WGL status list
if not exists (select * from status_group where sg_name = 'WGL_INSP_StatusList')
insert into status_group (sg_name,active) values ('WGL_INSP_StatusList',1)
GO
--Populate the WGL status list with statuses
if not exists (select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'))
begin
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'),'CGA',1) 
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'),'DONE',1)
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'),'-R',1)
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'),'529',1)
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList'),'ILS',1)
end	
GO
-- update the existing client record to use the WGL status list
if exists (select * from client where oc_code = 'WGLINSP') 
	update client set status_group_id = (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList') where oc_code = 'WGLINSP'
GO
-- Add the new WGL statuses to the UnrestrictedStatusList so they will show up in the list for the employee
if not exists (select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'UnrestrictedStatusList') and status='CGA')
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'UnrestrictedStatusList'),'CGA',1) 
GO
if not exists (select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'UnrestrictedStatusList') and status='DONE')
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'UnrestrictedStatusList'),'DONE',1)
GO
if not exists (select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'UnrestrictedStatusList') and status='529')
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'UnrestrictedStatusList'),'529',1)
GO
if not exists (select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'UnrestrictedStatusList') and status='ILS')
	insert into status_group_item (sg_id,status,active)
	values ( (select sg_id from status_group where sg_name = 'UnrestrictedStatusList'),'ILS',1)
GO

/*
select * from statuslist where status in ('CGA','DONE','ILS','529')
select * from status_group where sg_name = 'WGL_INSP_StatusList'
select * from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList')
select * from client where oc_code = 'WGLINSP'
*/
/*Rollback section:
update client set status_group_id = NULL where oc_code = 'WGLINSP'
DELETE from status_group_item where sg_id = (select sg_id from status_group where sg_name = 'WGL_INSP_StatusList')
DELETE from status_group where sg_name = 'WGL_INSP_StatusList'
DELETE from statuslist where status in ('CGA','DONE','ILS','529')
*/

--todo server ini map completion codes
/*ILS=ILS 300
CGA=CGA
529=529
DONE=COMPLETE*/


