-- Add new local entry_date field to locate & locate_status for timestamp of last change
alter table locate add entry_date datetime null
go

alter table locate_status add entry_date datetime null
go

-- Description for new Locate Change activity so it appears correctly on the report
if not exists (select ref_id from reference where type = 'empact' and code = 'LOCCH') 
  insert into reference (type, code, description) values ('empact', 'LOCCH', 'Locate Change')
go

-- Be sure to apply the updated trigger_locate.sql & RPT_employee_activity.sql scripts
