USE [QM]
GO

INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc])
     VALUES
           ('Tickets - Ticket Street Map'
           ,'General'
           ,'TicketStreetMap'
           ,'Creates Ticket Street Map for MyWork tickets')
GO

