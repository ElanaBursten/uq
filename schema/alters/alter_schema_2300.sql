-- Create Term Group that controls who gets a Xign csv file
if not exists (select term_group_id from term_group where group_code = 'TermsNeedingXignExport')
  insert into term_group(group_code, comment) values ('TermsNeedingXignExport', 'All term codes that need a Xign export')

/*
-- Clear all existing members of the Xign term group:
delete from term_group_detail 
where term_group_id in (
	select term_group_id from term_group where group_code='TermsNeedingXignExport'
	)
*/

-- Add all active terms for all active Comcast customers to the Xign group
insert into term_group_detail(term_group_id, term)
  select distinct (select top 1 term_group_id from term_group where group_code='TermsNeedingXignExport'), cl.oc_code 
  from client cl
  inner join customer cu on cl.customer_id = cu.customer_id
  where cu.customer_name like '%comcast%' 
    and cu.active = 1 
    and cl.active = 1


