/* If FNVCanBillCancels term group doesn't exist, create it and add all active FNV1 terms as members */

if not exists (select term_group_id from term_group where group_code = 'FNVCanBillCancels')
  insert term_group (group_code, comment, active) values ('FNVCanBillCancels', 'FNV terms that can be billed for cancel tix', 1)

if not exists (select tgd.term_group_id from term_group_detail tgd
  inner join term_group tg on tgd.term_group_id=tg.term_group_id
  where group_code = 'FNVCanBillCancels')
begin
  insert into term_group_detail (term_group_id, term, active)
    select distinct (select term_group_id from term_group where group_code = 'FNVCanBillCancels') term_group_id,
      oc_code term,
      active
    from client
    where call_center = 'FNV1'
      and active = 1
end

/* Rollback notes:

This script only adds data to the term_group and term_group_detail tables.

QMAdmin's Term Groups option can be used to delete the FNVCanBillCancels group
that this script creates.

*/
