alter table area
  add modified_date datetime NOT NULL DEFAULT getdate()
GO

insert into right_definition (right_description) values ('Tickets - Assign Area') -- Id 41
insert into configuration_data values ('AssignableAreaMapID', '694', GetDate(), 1)
GO

-- BE SURE TO RUN trigger_area.sql, TOO.
