-- Added as part of QMANTWO-90 (131)
-- No functionality is added for QManager Client at this time. These are external fields
-- QManager Admin updated to reflect these fields

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='ad_username')
  Alter table employee
    ADD ad_username varchar(8) null


IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='test_com_login')
  Alter table employee
    ADD test_com_login varchar(20) null


IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee' and COLUMN_NAME='adp_login')
  Alter table employee
    ADD adp_login varchar(20) null


-- Also, need to add these to employee_history
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_history' and COLUMN_NAME='ad_username')
  Alter table employee_history
    ADD ad_username varchar(8) null


IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_history' and COLUMN_NAME='test_com_login')
  Alter table employee_history
    ADD test_com_login varchar(20) null


IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_history' and COLUMN_NAME='adp_login')
  Alter table employee_history
    ADD adp_login varchar(20) null


--Add new fields to the update
