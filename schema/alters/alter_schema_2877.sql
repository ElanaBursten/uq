/* Add a column to support saving the number of tickets in a client cache*/
IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'sync_log' and COLUMN_NAME = 'tickets_in_client_cache')
	ALTER TABLE sync_log ADD tickets_in_client_cache int null
GO
