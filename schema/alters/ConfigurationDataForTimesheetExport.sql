/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [QM].[dbo].[configuration_data]
  where name = 'TimesheetExport'

  update  [QM].[dbo].[configuration_data]
  set name = 'TimesheetExport',
  value = 'ADP'
  where name = 'TimesheetExport'
USE [QM]
GO

INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[modified_date]
           ,[editable]
           ,[description])
     VALUES
           ( 'TimesheetExport'
           ,'ADP'
           ,getDate()
           ,1
           ,'ADP or Ulti.  Defaults to Ulti.  Not sync''d')
GO

