/* Use these commands to rollback the schema changes if the need arises:

-- drop the new field 
alter table ticket drop column work_priority_id
go

-- delete the new reference data
delete from reference where type = 'tkpriority'

*/

-- ticket table updates:
alter table ticket
  add work_priority_id int null
go

-- New Reference data:
insert into reference (type, code, description, sortby) values ('tkpriority', 'High', 'High', 9)
insert into reference (type, code, description, sortby) values ('tkpriority', 'Normal', 'Normal', 0)

-- Updated sync_3 sp:
if object_id('dbo.sync_3') is not null
  drop procedure dbo.sync_3
go

create proc sync_3 (
  @EmployeeID int,
  @UpdatesSinceS varchar(30),
  @ClientTicketListS text
)
as
  set nocount on

  DECLARE @tix TABLE (ticket_id integer not null primary key)
  DECLARE @modtix TABLE (ticket_id integer not null primary key)
  DECLARE @closed_since datetime
  DECLARE @oldest_l int
  DECLARE @UpdatesSince datetime

  DECLARE @client_has_tix TABLE (client_ticket_id integer not null primary key)
  DECLARE @should_have_tix TABLE (should_ticket_id integer not null primary key)
  DECLARE @current_pc varchar(15)

  set @UpdatesSince = convert(datetime, @UpdatesSinceS)

  -- Go back 2 minutes, to reduce update overlap errors
  set @UpdatesSince = dateadd(s, -120, @UpdatesSince)

/* Use this in emergency situations to disable initial syncs
  if @UpdatesSince < '2000-01-01'
  begin
    RAISERROR ('Initial syncs temporarily disabled', 50005, 1) with log
    return
  end
*/

  select 'row' as tname, getdate() as SyncDateTime

  select 'office' as tname, * from office
    where modified_date > @UpdatesSince

  select 'call_center' as tname, cc_code, cc_name, active, track_arrivals from call_center
   where modified_date > @UpdatesSince

  select 'statuslist' as tname, * from statuslist where modified_date > @UpdatesSince

  select 'reference' as tname, * from reference where modified_date > @UpdatesSince

  select 'profit_center' as tname, * from profit_center where modified_date > @UpdatesSince

  select 'customer' as tname, * from customer where modified_date > @UpdatesSince

  select 'center_group' as tname, * from center_group where modified_date > @UpdatesSince

  select 'center_group_detail' as tname, * from center_group_detail where modified_date > @UpdatesSince

  select 'client' as tname, * from client where modified_date > @UpdatesSince

  select 'configuration_data' as tname, * from configuration_data where (modified_date > @UpdatesSince) and (Name in ('ClientURL'))

  select 'employee' as tname, emp_id, type_id, status_id, timesheet_id,
    emp_number, short_name, report_to, active, company_car, timerule_id,
    repr_pc_code, payroll_pc_code, company_id
   from employee where modified_date > @UpdatesSince

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, login_id, chg_pwd,
    last_login, active_ind, can_view_notes, can_view_history, chg_pwd_date
   from users where emp_id = @EmployeeID  -- Myself only

  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, active_ind
   from users where emp_id <> @EmployeeID -- Everyone else
   and  modified_date > @UpdatesSince

  select 'locating_company' as tname, * from locating_company
   where modified_date > @UpdatesSince

  select 'carrier' as tname, * from carrier
   where modified_date > @UpdatesSince

  select 'vehicle_type' as tname, * from vehicle_type
   where modified_date > @UpdatesSince

  select 'followup_ticket_type' as tname, * from followup_ticket_type
   where modified_date > @UpdatesSince

  set @current_pc = dbo.get_historical_pc(@EmployeeID, GetDate())
  if @current_pc is null set @current_pc = dbo.get_employee_pc(@EmployeeID, 0)

  select 'employee' as tname, emp_id, charge_cov, timerule_id,
    timesheet_id, company_car, effective_pc = @current_pc,
    first_name, last_name
  from employee
  where (emp_id = @EmployeeID)
    and (modified_date > @UpdatesSince or dbo.get_historical_pc(emp_id, @UpdatesSince) <> @current_pc)

  select 'employee_right' as tname, * from employee_right
    where emp_id=@EmployeeID and modified_date > @UpdatesSince

  if ((select rights_modified_date from employee where emp_id = @EmployeeID) > @UpdatesSince)
    select 'emp_group_right' as tname, * from dbo.get_emp_group_rights(@EmployeeID)

  select 'vehicle_use' as tname, * from vehicle_use
    where modified_date > @UpdatesSince and emp_id = @EmployeeID

  select 'timesheet' as tname, timesheet.*, timesheet_detail.*
  from timesheet
    inner join timesheet_detail
      on timesheet.timesheet_id = timesheet_detail.timesheet_id
  where (timesheet.modified_date > @UpdatesSince) and (timesheet.end_date > dateadd(Day, -38, getdate()))
    and timesheet.emp_id = @EmployeeID

  select 'timesheet_entry' as tname, *
  from timesheet_entry
  where
    work_emp_id = @EmployeeID and
    (modified_date > @UpdatesSince) and (work_date > dateadd(Day, -38, getdate()))
    -- We no longer go back 2 years to get old floating_holiday timesheets, since that feature is not in use

  select 'asset_type' as tname, * from asset_type
  where modified_date > @UpdatesSince

  select 'asset_assignment' as tname, * from asset_assignment
  where emp_id = @EmployeeID
    and modified_date > @UpdatesSince

  select 'asset' as asset, * from asset
  where asset_id in (select asset_id from asset_assignment where emp_id = @EmployeeID)
    and modified_date > @UpdatesSince

  select 'upload_location' as tname, * from upload_location
  where modified_date > @UpdatesSince

  select 'upload_file_type' as tname, * from upload_file_type
  where modified_date > @UpdatesSince

  select 'damage_default_est' as tname, * from damage_default_est
  where modified_date > @UpdatesSince

  select 'status_group' as tname, * from status_group
  where modified_date > @UpdatesSince

  select 'status_group_item' as tname, * from status_group_item
  where modified_date > @UpdatesSince

  select 'call_center_hp' as tname, * from call_center_hp
  where modified_date > @UpdatesSince

  select 'billing_unit_conversion' as tname, * from billing_unit_conversion
  where modified_date > @UpdatesSince

  select 'billing_gl' as tname, * from billing_gl
  where modified_date > @UpdatesSince

  select 'area' as tname, * from area
  where map_id = (select value from configuration_data where name = 'AssignableAreaMapID')
    and modified_date > @UpdatesSince

  select 'billing_output_config' as tname, output_config_id, customer_id, which_invoice
  from billing_output_config where modified_date > @UpdatesSince

  -- Ticket-related data
  select @closed_since = dateadd(day, -5, getdate())

  select @oldest_l = (select top 1 oldest_open_locate from oldest_open)

  -- all open locates, so client can detect it has missing locates
  select 'open_locate' as tname, ticket_id, locate_id, status
   from locate open_locate
   where assigned_to=@EmployeeID

  -- all open tickets, so we can be sure to send them all
  insert into @should_have_tix
  select distinct ticket_id
   from locate
   where assigned_to=@EmployeeID

  -- what the client does have
  insert into @client_has_tix
  select ID from IdListToTable(@ClientTicketListS)

  -- DISABLE the sync of "my old tickets":
  if (1=0) and (@@ROWCOUNT<1 or @UpdatesSince < '2000-01-01')  begin    -- support old clients and init syncs
    if @UpdatesSince < '2000-01-01'  begin
      -- Get all tickets assigned to me even if closed
      insert into @tix  (ticket_id)
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date> @closed_since
      union
      select locate.ticket_id
       from locate  where locate.assigned_to_id=@EmployeeID
            and locate.closed_date is null
      union
      select should_ticket_id from @should_have_tix
    end else begin
      -- This is a subsequent sync with the client tic list missing,
      -- must use old alg to get consistent results:
      -- Get all tickets ever assigned to me, this is the SLOW part
      insert into @tix  (ticket_id)
      select distinct locate.ticket_id
       from assignment
       inner join locate on locate.locate_id=assignment.locate_id
          and (locate.closed=0 or locate.closed_date> @closed_since)
       where assignment.locator_id = @EmployeeID
        and assignment.locate_id >= @oldest_l
    end
  end else begin  -- newer clients tell us what they have
    -- they need to know about updates to tickets they are
    -- assigned now, and also tickets still in cache
    insert into @tix  (ticket_id)
      select should_ticket_id from @should_have_tix
      union
      select client_ticket_id from @client_has_tix
  end

  -- subtract what we have, from what we should have
  delete from @should_have_tix where should_ticket_id in
    (select client_ticket_id from @client_has_tix)

  -- Find those that have changed, somewhat slow
  insert into @modtix (ticket_id)
  select t.ticket_id
   from @tix t
    inner join ticket on ticket.ticket_id = t.ticket_id
    and ticket.modified_date >@UpdatesSince
  union
  select locate.ticket_id
   from @tix t2
    inner join locate on locate.ticket_id = t2.ticket_id
   where locate.modified_date > @UpdatesSince
    and locate.status<>'-N'
  union
  select locate.ticket_id
   from @tix t3
    inner join locate on locate.ticket_id = t3.ticket_id
    inner join assignment on locate.locate_id=assignment.locate_id
   where assignment.modified_date > @UpdatesSince
  union
  select notes.foreign_id
   from @tix t4
    inner join notes on notes.foreign_id = t4.ticket_id
   where notes.foreign_type = 1
    and notes.modified_date > @UpdatesSince
  union
  select attachment.foreign_id
   from @tix t5
    inner join attachment on attachment.foreign_id = t5.ticket_id
   where attachment.foreign_type = 1
    and attachment.modified_date > @UpdatesSince
  union
    select should_ticket_id from @should_have_tix
  union
  select locate.ticket_id
  from @tix t6
    inner join locate on locate.ticket_id = t6.ticket_id 
    inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes
  where 
    notes.modified_date > @UpdatesSince

  -- now get the whole ticket for just the ones that have changed
  declare @tickets_to_send table (
  [ticket_id] [int] NOT NULL ,
  [ticket_number] [varchar] (20) NOT NULL ,
  [parsed_ok] [bit] NOT NULL ,
  [ticket_format] [varchar] (20) NOT NULL ,
  [kind] [varchar] (20) NOT NULL ,
  [status] [varchar] (20) NULL ,
  [map_page] [varchar] (20) NULL ,
  [revision] [varchar] (20) NOT NULL ,
  [transmit_date] [datetime] NULL ,
  [due_date] [datetime] NULL ,
  [work_description] [varchar] (3500) NULL ,
  [work_state] [varchar] (2) NULL ,
  [work_county] [varchar] (40) NULL ,
  [work_city] [varchar] (40) NULL ,
  [work_address_number] [varchar] (10) NULL ,
  [work_address_number_2] [varchar] (10) NULL ,
  [work_address_street] [varchar] (60) NULL ,
  [work_cross] [varchar] (100) NULL ,
  [work_subdivision] [varchar] (70) NULL ,
  [work_type] [varchar] (90) NULL ,
  [work_date] [datetime] NULL ,
  [work_notc] [varchar] (40) NULL ,
  [work_remarks] [varchar] (1200) NULL ,
  [priority] [varchar] (40) NULL ,
  [legal_date] [datetime] NULL ,
  [legal_good_thru] [datetime] NULL ,
  [legal_restake] [varchar] (40) NULL ,
  [respond_date] [datetime] NULL ,
  [duration] [varchar] (40) NULL ,
  [company] [varchar] (80) NULL ,
  [con_type] [varchar] (50) NULL ,
  [con_name] [varchar] (50) NULL ,
  [con_address] [varchar] (50) NULL ,
  [con_city] [varchar] (40) NULL ,
  [con_state] [varchar] (40) NULL ,
  [con_zip] [varchar] (40) NULL ,
  [call_date] [datetime] NULL ,
  [caller] [varchar] (50) NULL ,
  [caller_contact] [varchar] (50) NULL ,
  [caller_phone] [varchar] (40) NULL ,
  [caller_cellular] [varchar] (40) NULL ,
  [caller_fax] [varchar] (40) NULL ,
  [caller_altcontact] [varchar] (40) NULL ,
  [caller_altphone] [varchar] (40) NULL ,
  [caller_email] [varchar] (40) NULL ,
  [operator] [varchar] (40) NULL ,
  [channel] [varchar] (40) NULL ,
  [work_lat] [decimal](9, 6) NULL ,
  [work_long] [decimal](9, 6) NULL ,
  [image] [text] NOT NULL ,
  [parse_errors] [text] NULL ,
  [modified_date] [datetime] NOT NULL,
  [active] [bit] NOT NULL,
  [ticket_type] [varchar] (38) NULL ,
  [legal_due_date] [datetime] NULL ,
  [parent_ticket_id] [int] NULL ,
  [do_not_mark_before] [datetime] NULL ,
  [route_area_id] [int] NULL ,
  [watch_and_protect] [bit] NULL,
  [service_area_code] [varchar] (40) NULL ,
  [explosives] [varchar] (20) NULL ,
  [serial_number] [varchar] (40) NULL ,
  [map_ref] [varchar] (60) NULL ,
  [followup_type_id] [int] NULL ,
  [do_not_respond_before] [datetime] NULL ,
  [recv_manager_id] [int] NULL ,
  [ward] [varchar] (10) NULL ,
  route_area_name varchar(50) NULL,
  alert varchar(1) NULL,
  start_date datetime NULL,
  end_date datetime NULL,
  points decimal(6,2) NULL,
  work_priority_id int NULL
  )

  declare @locates_to_send table (
  [locate_id] [int] NOT NULL ,
  [ticket_id] [int] NOT NULL ,
  [client_code] [varchar] (10) NULL ,
  [client_id] [int] NULL ,
  [status] [varchar] (5) NOT NULL ,
  [high_profile] [bit] NOT NULL ,
  [qty_marked] [int] NULL ,
  [price] [money] NULL ,
  [closed] [bit] NOT NULL ,
  [closed_by_id] [int] NULL ,
  [closed_how] [varchar] (10) NULL ,
  [closed_date] [datetime] NULL ,
  [modified_date] [datetime] NOT NULL ,
  [active] [bit] NOT NULL,
  [invoiced] [bit] NULL ,
  [assigned_to] [int] NULL ,
  [regular_hours] [decimal](5, 2) NULL ,
  [overtime_hours] [decimal](5, 2) NULL ,
  [added_by] [varchar] (8) NULL ,
  [watch_and_protect] [bit] NULL ,
  [high_profile_reason] [int] NULL ,
  [seq_number] [varchar] (20) NULL ,
  [assigned_to_id] [int] NULL ,
  [mark_type] [varchar] (10) NULL ,
  [alert] varchar(1) NULL,
  [locator_id] int
  )

  -- gather the ticket and locate data, to a table var, so these queries complete
  -- right away (without NOLOCK)
  insert into @tickets_to_send
  select
     ticket.ticket_id,
     ticket.ticket_number,
     ticket.parsed_ok,
     ticket.ticket_format,
     ticket.kind,
     ticket.status,
     ticket.map_page,
     ticket.revision,
     ticket.transmit_date,
     ticket.due_date,
     ticket.work_description,
     ticket.work_state,
     ticket.work_county,
     ticket.work_city,
     ticket.work_address_number,
     ticket.work_address_number_2,
     ticket.work_address_street,
     ticket.work_cross,
     ticket.work_subdivision,
     ticket.work_type,
     ticket.work_date,
     ticket.work_notc,
     ticket.work_remarks,
     ticket.priority,
     ticket.legal_date,
     ticket.legal_good_thru,
     ticket.legal_restake,
     ticket.respond_date,
     ticket.duration,
     ticket.company,
     ticket.con_type,
     ticket.con_name,
     ticket.con_address,
     ticket.con_city,
     ticket.con_state,
     ticket.con_zip,
     ticket.call_date,
     ticket.caller,
     ticket.caller_contact,
     ticket.caller_phone,
     ticket.caller_cellular,
     ticket.caller_fax,
     ticket.caller_altcontact,
     ticket.caller_altphone,
     ticket.caller_email,
     ticket.operator,
     ticket.channel,
     ticket.work_lat,
     ticket.work_long,
     ticket.image,
     ticket.parse_errors,
     ticket.modified_date,
     ticket.active,
     ticket.ticket_type,
     ticket.legal_due_date,
     ticket.parent_ticket_id,
     ticket.do_not_mark_before,
     ticket.route_area_id,
     ticket.watch_and_protect,
     ticket.service_area_code,
     ticket.explosives,
     ticket.serial_number,
     ticket.map_ref,
     ticket.followup_type_id,
     ticket.do_not_respond_before,
     ticket.recv_manager_id,
     ticket.ward,
     coalesce(area.area_name, '') as area_name,
     ticket.alert,
     -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null:
     null, --     start_date,
     null, --     end_date,
     null, --     points
     ticket.work_priority_id
    from @modtix mt
      inner join ticket on mt.ticket_id = ticket.ticket_id
      left join area on area.area_id = ticket.route_area_id

  insert into @locates_to_send
    (locate_id, ticket_id, client_code, client_id, status, high_profile, qty_marked,
     price, closed, closed_by_id, closed_how, closed_date, modified_date, active,
     invoiced, assigned_to, regular_hours, overtime_hours, added_by, watch_and_protect,
     high_profile_reason, seq_number, assigned_to_id, mark_type, alert, locator_id)
  select
     l.locate_id, l.ticket_id, l.client_code, l.client_id, l.status, l.high_profile, l.qty_marked,
     l.price, l.closed, l.closed_by_id, l.closed_how, l.closed_date, l.modified_date, l.active,
     l.invoiced, l.assigned_to, l.regular_hours, l.overtime_hours, l.added_by, l.watch_and_protect,
     l.high_profile_reason, l.seq_number, l.assigned_to_id, l.mark_type, l.alert, assignment.locator_id
    from @modtix mt
      inner join locate l
        on mt.ticket_id = l.ticket_id and l.status <> '-N' and l.status <> '-P'
      inner join assignment
        on l.locate_id=assignment.locate_id and assignment.active = 1

  -- Send the ticket and locate data
  select 'ticket' as tname, * from @tickets_to_send ticket

  select 'locate' as tname, * from @locates_to_send locate

  -- Retrieve all notes for any ticket that has been modified in any way, even
  -- if the note has not been modified. This is necessary because the ticket
  -- may be have been reassigned to a different locator.
  select 'notes' as tname, notes.*
   from @modtix mt
   inner join notes on notes.foreign_type = 1 -- 1 = ticket notes
    and mt.ticket_id = notes.foreign_id
  union
  select 'notes' as tname, notes.*
   from @modtix mt
     inner join locate on locate.ticket_id = mt.ticket_id 
     inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes

  -- All attachments for modified tickets:
  select 'attachment' as tname, attachment.*
   from @modtix mt
   inner join attachment on mt.ticket_id = attachment.foreign_id
    and attachment.foreign_type = 1

  -- All Plats for modified tickets:
  select 'locate_plat' as tname, locate_plat.*
   from @locates_to_send ls
   inner join locate_plat on ls.locate_id = locate_plat.locate_id

  -- All unacked, unexpired messages for employee:
  select 'message_dest' as tname, message.message_id, message.message_number,
   message.from_emp_id, message.destination, message.subject, message.body,
   message.sent_date, message.show_date, message.expiration_date,
   md.message_dest_id, md.emp_id, md.ack_date, md.read_date
  from message inner join message_dest md on message.message_id=md.message_id
  where md.ack_date is null
    and md.emp_id = @EmployeeID
    and message.expiration_date > getdate()

  -- Send the ticket_version data
  select 'ticket_version' as tname,
     tv.ticket_version_id, tv.ticket_id, tv.ticket_revision, tv.ticket_number, tv.ticket_type, tv.transmit_date, 
     tv.processed_date, tv.serial_number, tv.arrival_date, tv.filename, tv.ticket_format, tv.source 
    from @modtix mt
      inner join ticket_version tv on mt.ticket_id = tv.ticket_id

  -- Send the break_rules data
  select 'break_rules' as tname, break_rules.*
   from break_rules
   where modified_date > @UpdatesSince

  -- Send the right definition data
  select 'right_definition' as tname, * from right_definition where modified_date > @UpdatesSince

  -- Send the right restriction data
  select 'right_restriction' as tname, *
   from right_restriction
   where modified_date > @UpdatesSince

  -- Send the usage restriciton message data
  select 'restricted_use_message' as tname, *
   from restricted_use_message
   where modified_date > @UpdatesSince

  -- Send the usage restriciton exemption data
  select 'restricted_use_exemption' as tname, *
   from restricted_use_exemption
   where emp_id = @EmployeeID
     and modified_date > @UpdatesSince

  -- Send the jobsite_arrival data
  select 'jobsite_arrival' as tname, arrival.*
    from @modtix mt
      inner join jobsite_arrival arrival on mt.ticket_id = arrival.ticket_id

  -- All locate_facility for locates on modified tickets:
  select 'locate_facility' as tname, lf.*
    from @locates_to_send ls
      inner join locate_facility lf on ls.locate_id = lf.locate_id
go

grant execute on sync_3 to uqweb, QManagerRole


-- Updated get_open_tickets_for_locator sp:
if object_id('dbo.get_open_tickets_for_locator') is not null
  drop procedure dbo.get_open_tickets_for_locator
go

create proc get_open_tickets_for_locator (@locator_id int)
as
  set nocount on

  declare @results table (
    ticket_id integer not null primary key,
    ticket_number varchar(20) not null,
    kind varchar(20) not null,
    due_date datetime null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null, 
    work_type varchar(90) null,
    legal_good_thru datetime null, 
    map_page varchar(20) null, 
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null, 
    work_address_number varchar(10) null, 
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    points decimal(6,2) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null)

  declare @limit integer
  select @limit = coalesce(ticket_view_limit, 0) from employee where emp_id = @locator_id

  insert into @results 
  select ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    /* UQ's database does not have these fields
    convert(varchar(10), ticket.start_date, 101) as scheduled, 
    convert(varchar(5), ticket.start_date, 108) as start_date, 
    convert(varchar(5), ticket.end_date, 108) as end_date, 
    ticket.points,
    */
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    case when @limit < 1 then 'Y' else 'N' end as ticket_is_visible
  from ticket
  left join reference r on ref_id = ticket.work_priority_id
  where ticket.ticket_id in (
    select ticket_id from locate
      where assigned_to = @locator_id
    )

  /* 
  update @results set ticket_is_visible = 'Y' where ticket_id in
    (select top(@limit) ticket_id from @results where ticket_is_visible <> 'Y' 
       order by work_priority_sort desc, due_date, ticket_number)

   For SQL Server 2005 and later, use ROW_COUNT() or the update above.
   Only constant TOP limits are supported in SQL Server 2000, so use the 
   slower one at-a-time method below for now. - ugh
  */

  declare @idx integer
  set @idx = 1
  while @idx <= @limit 
  begin
    update @results set ticket_is_visible = 'Y'
    where ticket_id = (select top 1 ticket_id from @results where ticket_is_visible <> 'Y'
      order by work_priority_sort desc, due_date, ticket_number)
    set @idx = @idx + 1
  end

  select * from @results ticket order by work_priority_sort desc, due_date, ticket_number
  for XML AUTO

go
grant execute on get_open_tickets_for_locator to uqweb, QManagerRole

-- Updated get_unacked_tickets_2 sp:
if object_id('dbo.get_unacked_tickets_2') is not null
  drop procedure dbo.get_unacked_tickets_2
go

create proc get_unacked_tickets_2 (@mgr_id int)
as
  set nocount on

  select ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    /* UQ's database does not have these fields
    convert(varchar(10), ticket.start_date, 101) as scheduled, 
    convert(varchar(5), ticket.start_date,  108) as start_date, 
    convert(varchar(5), ticket.end_date, 108) as end_date, 
    ticket.points,
    */
    null as scheduled, null as start_date, null as end_date, null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible
  from ticket
  left join reference r on r.ref_id = ticket.work_priority_id
  where ticket.ticket_id in (
    select ticket_id
    from ticket_ack
    inner join (select h_emp_id from dbo.get_hier(@mgr_id,0)) mgremps
      on mgremps.h_emp_id = ticket_ack.locator_emp_id
    where ticket_ack.has_ack = 0 )

  for XML AUTO
go
grant execute on get_unacked_tickets_2 to uqweb, QManagerRole

-- Updated get_old_tickets_for_locator sp:
if object_id('dbo.get_old_tickets_for_locator') is not null
  drop procedure dbo.get_old_tickets_for_locator
go

create proc get_old_tickets_for_locator (@locator_id int, @num_days int)
as
  set nocount on

  declare @since datetime
  declare @assignments_since datetime

  select @since = dateadd(Day, -@num_days, getdate())

  select ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    /* UQ's database does not have these fields
    convert(varchar(10), ticket.start_date, 101) as scheduled, 
    convert(varchar(5), ticket.start_date,  108) as start_date, 
    convert(varchar(5), ticket.end_date, 108) as end_date, 
    ticket.points,
    */
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible
  from ticket 
  left join reference r on ref_id = ticket.work_priority_id
  where ticket.ticket_id in (
    select ticket_id from locate
     where assigned_to_id = @locator_id
      and (closed_date >= @since or closed_date is null)
   )
 for XML AUTO
go
grant execute on get_old_tickets_for_locator to uqweb, QManagerRole

