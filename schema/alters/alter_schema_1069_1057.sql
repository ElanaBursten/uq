/* Creates payment code reference table entries & adds damage_invoice.payment_code field */
insert into reference (type, code, description, sortby) values ('paycode', 'LIUD', 'Litigation Indemnification Utility Damage', 1)
insert into reference (type, code, description, sortby) values ('paycode', 'LILOU', 'Litigation Indemnification Loss of Use', 2)
insert into reference (type, code, description, sortby) values ('paycode', 'LIPD', 'Litigation Indemnification 3rd party Property Damage', 3)
insert into reference (type, code, description, sortby) values ('paycode', 'LAF', 'Litigation Attorney Fees', 4)
insert into reference (type, code, description, sortby) values ('paycode', 'LEXP', 'Litigation Legal Expenses (court reporter, filing fees, etc)', 5)
insert into reference (type, code, description, sortby) values ('paycode', 'LWIT', 'Litigation Expert Witness Expense', 6)
go

alter table damage_invoice 
  add payment_code varchar(10)
go

/* there are a very small number of rows in this table currently, so the not null shouldn't be a problem */
alter table damage_litigation
  add defendant   varchar(45) null,
  venue         varchar(100) null,
  venue_state   char(2) null,
  service_date  datetime null,
  summons_date  datetime null,
  carrier_notified bit not null default 0,
  claim_status  varchar(10) null,
  added_by      int null,
  modified_date datetime not null default GetDate(),
  modified_by   int null
go

-- New damage_litigation triggers:
if object_id('dbo.damage_litigation_i') is not null
  drop trigger dbo.damage_litigation_i
go

create trigger dbo.damage_litigation_i on damage_litigation
for insert
as  
  update damage_litigation set added_by = modified_by
    where litigation_id in (select litigation_id from inserted)
go

if object_id('dbo.damage_litigation_iu') is not null
  drop trigger dbo.damage_litigation_iu
GO

create trigger dbo.damage_litigation_iu on damage_litigation
for insert, update
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_litigation set modified_date = getdate()
    where litigation_id in (select litigation_id from inserted)
go

-- New damage_third_party triggers:
if object_id('dbo.damage_third_party_i') is not null
  drop trigger dbo.damage_third_party_i
go

create trigger dbo.damage_third_party_i on damage_third_party
for insert
as  
  update damage_third_party set added_by = modified_by
    where third_party_id in (select third_party_id from inserted)
go

if object_id('dbo.damage_third_party_iu') is not null
  drop trigger dbo.damage_third_party_iu
GO

create trigger dbo.damage_third_party_iu on damage_third_party
for insert, update
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_third_party set modified_date = getdate()
    where third_party_id in (select third_party_id from inserted)
go

-- Updated get_damage3 proc:
if object_id('dbo.get_damage3') is not null
  drop procedure dbo.get_damage3
GO

create proc get_damage3(@DamageID int)
as
  select *
  from damage
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_estimate
  where damage_id=@DamageID
    and active = 1
  for xml auto

  select *
  from damage_history
  where damage_id=@DamageID
  for xml auto

  select *, case
    when IsNull(litigation_id, 0) > 1 then 2
    when IsNull(third_party_id, 0) > 1 then 1
    else 0
   end as invoice_type
  from damage_invoice
  where damage_id=@DamageID
  for xml auto

  select *
  from attachment
  where (foreign_id = @DamageID and foreign_type = 2)
   or (foreign_id = (select top 1 ticket_id from damage
   where damage_id = @DamageID) and foreign_type = 1)
  for xml auto

  select *
  from damage_bill
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_litigation
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_third_party
  where damage_id=@DamageID
  for xml auto

  select *, case foreign_type
    when 2 then 0
    when 3 then 1
    when 4 then 2
    else null end as damage_notes_type,
   @DamageID as damage_id
  from notes
  where (foreign_id = @DamageID and foreign_type = 2)
   or (foreign_id in (select third_party_id from damage_third_party
   where damage_id = @DamageID) and foreign_type = 3)
   or (foreign_id in (select litigation_id from damage_litigation
   where damage_id = @DamageID) and foreign_type = 4)
  for xml auto

GO


grant execute on get_damage3 to uqweb, QManagerRole
/*
exec dbo.get_damage3 105524
*/


-- Updated get_damage_details proc:
if object_id('dbo.get_damage_details') is not null
  drop procedure dbo.get_damage_details
GO

create proc get_damage_details (@damage_id int)
as

declare @TicketResults table (
  ticket_idz         integer     not null,
  ticket_number      varchar(25) null,
  transmit_date      datetime    null,
  due_date           datetime    null,
  locator_id         integer     null,
  locator_name       varchar(35) null,
  completed_date     datetime    null,
  image              text        null)

set nocount on

-- return damage data
select damage.*, ticket.ticket_number,
  Coalesce(employee.short_name, '') as investigator_name,
  Coalesce(pc_name, profit_center) as 'profit_center_name',
  r1.description as 'excavator_type_desc',
  r2.description as 'excavation_type_desc',
  Coalesce(r3.description, damage.size_type, '') as 'facility_size_desc',
  r4.description as 'tracer_wire_intact_desc',
  r5.description as 'marked_in_white_desc',
  r6.description as 'site_pot_holed_desc'
from damage
  left join profit_center on profit_center.pc_code = damage.profit_center
  left join ticket on ticket.ticket_id = damage.ticket_id
  left join employee on employee.emp_id = damage.investigator_id
  left join reference r1 on (r1.type='excrtype' and r1.code=damage.excavator_type)
  left join reference r2 on (r2.type='excntype' and r2.code=damage.excavation_type)
  left join reference r3 on (r3.type='facsize' and r3.code=damage.facility_size and r3.modifier=damage.facility_type)
  left join reference r4 on (r4.type='yesnoblank' and r4.code=damage.site_tracer_wire_intact)
  left join reference r5 on (r5.type='yesnoblank' and r5.code=damage.site_marked_in_white)
  left join reference r6 on (r6.type='ynub' and r6.code=damage.site_pot_holed)
where damage_id = @damage_id

-- return ticket data
insert into @TicketResults (ticket_idz, ticket_number, transmit_date, due_date, image)
  select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image
  from ticket
  where ticket.ticket_id =
        (select ticket_id from damage where damage_id=@damage_id)

-- Set the locator ID
update @TicketResults
  set locator_id=a.locator_id
  from locate l, assignment a
  where l.ticket_id = ticket_idz
  and l.locate_id = a.locate_id
  and a.active = 1
  and l.status <> '-N'

-- Set the locator name
update @TicketResults
  set locator_name=short_name
  from employee where emp_id=locator_id

-- Set the completed date
update @TicketResults
  set completed_date = (select min(l.closed_date)
  from locate l, assignment a
  where l.ticket_id = ticket_idz
  and l.locate_id = a.locate_id
  and a.active = 1
  and l.closed = 1)

select ticket_idz as ticket_id, ticket_number, transmit_date, due_date,
  locator_id, locator_name, completed_date, image
 from @TicketResults

-- Locate data
select
  locate.ticket_id,
  locate.client_code,
  locate.status,
  statuslist.status_name,
  employee.short_name,
  locate.high_profile,
  locate.qty_marked,
  locate.closed,
  locate.closed_date,
  dbo.get_plat_list(locate.locate_id) as plat_list
from ticket
  left join locate
    on (locate.ticket_id = ticket.ticket_id)
  left join statuslist
    on statuslist.status = locate.status
  left join assignment
    on assignment.locate_id = locate.locate_id
  left join employee
    on employee.emp_id = assignment.locator_id
where ticket.ticket_id =
        (select ticket_id from damage where damage_id=@damage_id)
 and assignment.active = 1
order by locate.ticket_id, locate.client_code

-- Ticket notes data
select notes.entry_date, notes.modified_date, notes.note, damage.ticket_id
  from damage
   inner join notes on damage.ticket_id=notes.foreign_id and notes.foreign_type = 1
where damage.damage_id=@damage_id
order by notes.entry_date

-- damage attachments

select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name
from attachment a
     inner join employee e on e.emp_id = a.attached_by
where
  a.active = 1 and
  a.extension in ('.bmp','.jpg','jpeg','.gif') and
  a.foreign_id = @damage_id

-- ticket attachments

select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.foreign_id
from attachment a
     inner join employee e on e.emp_id = a.attached_by
where
  a.active = 1 and
  a.extension in ('.bmp','.jpg','jpeg','.gif') and
  a.foreign_id in (select ticket_idz from @TicketResults)

-- Damage third party data
select damage_third_party.*
  from damage_third_party
  where damage_id = @damage_id

-- Damage litigation data
select damage_litigation.*
  from damage_litigation
  where damage_id = @damage_id

-- Damage notes data
select notes.*, @damage_id as damage_id
  from notes
  where (foreign_id = @damage_id and foreign_type = 2)
  or (foreign_id in (select third_party_id from damage_third_party where damage_id = @damage_id)
  and foreign_type = 3)
  or (foreign_id in (select litigation_id from damage_litigation where damage_id = @damage_id)
  and foreign_type = 4)
order by notes.entry_date

-- Damage history data
select damage_history.*
  from damage_history
  where damage_id = @damage_id
order by modified_date

GO

grant execute on get_damage_details to uqweb, QManagerRole

/*
exec dbo.get_damage_details 50860
exec dbo.get_damage_details 68753
exec dbo.get_damage_details 62129

-- SQL to add a note to force rolling to another page:
insert into notes (foreign_type, foreign_id, active, uid, note)
 values (1, 5442685, 1, 100, '2 this is a test note, it shoudl be long enough to scroll so we can see what happens when we go past one page!')
*/
