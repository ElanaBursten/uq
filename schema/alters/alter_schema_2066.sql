/* Adds new right allowing locator to sync ticket status changes immediately on Done */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Tickets - Immediate Sync', 'General', 'TicketsImmediateSync', 'Limitation is not used for this right.')

