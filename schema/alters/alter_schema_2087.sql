/* create new right */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Tickets - Acknowledge All Activities', 'General', 'TicketsAcknowledgeAllActivities', 'Limitation does not apply to this right.')
go

/* New stored proc to ack activities before a given date for a selected manager */
if object_id('dbo.acknowledge_all_ticket_activity') is not null
  drop procedure dbo.acknowledge_all_ticket_activity
GO

create procedure acknowledge_all_ticket_activity(@ManagerID int, @AckBeforeDate datetime, @AckByEmpID int)
as

update ticket_activity_ack
  set ack_emp_id = @AckByEmpID, ack_date = GetDate(), has_ack = 1
  from (select h_emp_id from dbo.get_hier(@ManagerID, 0)) hier
  where locator_emp_id = hier.h_emp_id
    and activity_date < @AckBeforeDate
    and has_ack = 0
go

grant execute on acknowledge_all_ticket_activity to uqweb, QManagerRole

/*
  exec dbo.acknowledge_all_ticket_activity 9999123, 'Jan 1, 2008', 3510
  exec dbo.acknowledge_all_ticket_activity 2676, 'Jan 1, 2008', 3510
*/

