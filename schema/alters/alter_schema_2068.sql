/* create new right */
insert into right_definition (right_description, right_type, entity_data, modifier_desc)
values ('Tickets - Acknowledge Activity', 'General', 'TicketsAcknowledgeActivity', 'Limitation does not apply to this right.')
go

/* New ticket activity ack table */
CREATE TABLE ticket_activity_ack (
  ticket_activity_ack_id integer identity PRIMARY KEY,
  ticket_id integer NOT NULL,
  activity_type varchar(20) NOT NULL,
  activity_date datetime NOT NULL,
  has_ack bit NOT NULL DEFAULT 0,  /* 0=needs ack, 1=has ack */
  ack_date datetime NULL,
  ack_emp_id integer NULL,  /* the manager that performed the ack */
  locator_emp_id integer NULL,  /* locator associated with the ticket */
)

create index ticket_activity_ack_ack_date on ticket_activity_ack(ack_date)
create index ticket_activity_ack_has_ack on ticket_activity_ack(has_ack)
create index ticket_activity_ack_emp_id_has_ack on ticket_activity_ack(locator_emp_id, has_ack)
create index ticket_activity_ack_ticket_id on ticket_activity_ack(ticket_id)
go
alter table dbo.ticket_activity_ack add constraint 
  FK_ticket_activity_ack_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO
alter table dbo.ticket_activity_ack add constraint 
  FK_ticket_activity_ack_ack_emp_id foreign key (ack_emp_id)
  references employee (emp_id)
GO
alter table dbo.ticket_activity_ack add constraint 
  FK_ticket_activity_ack_locator_emp_id foreign key (locator_emp_id)
  references employee (emp_id)
GO

