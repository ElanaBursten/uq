/* Find / fix invalid update_call_center values currently in the client table:

  update client set update_call_center = null where update_call_center = ''

  select * from client where update_call_center not in (select cc_code from call_center)

*/

ALTER TABLE client
  ADD CONSTRAINT client_fk_update_call_center
  FOREIGN KEY (update_call_center) REFERENCES call_center(cc_code)

GO
