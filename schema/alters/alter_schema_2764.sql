-- Add column to billing_rate table to associate a value group with the rate
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_rate' and COLUMN_NAME='value_group_id')
  alter table billing_rate add value_group_id int null
go

--Modify unique index to include value_group_id
if exists (select * from dbo.sysindexes where name = N'constr_billing_rate_unique' and id = object_id(N'[dbo].[billing_rate]'))
  alter table billing_rate drop constraint constr_billing_rate_unique
go

ALTER TABLE billing_rate
  ADD CONSTRAINT constr_billing_rate_unique
  UNIQUE (call_center, billing_cc, status, parties, bill_type, work_county, area_name, work_city, value_group_id)
go  

--Add foreign key to enforce valid rate-value group relationship
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'billing_rate' and CONSTRAINT_NAME = 'billing_rate_fk_value_group')
  ALTER TABLE billing_rate
    ADD CONSTRAINT billing_rate_fk_value_group
    FOREIGN KEY (value_group_id) REFERENCES value_group(value_group_id)
go
  
-- Add value_group_id column to billing_rate_history table.
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_rate_history' and COLUMN_NAME='value_group_id')
  alter table billing_rate_history add value_group_id int null
go
  
-- Add billing_detail.rate_value_group_id
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'billing_detail' and COLUMN_NAME='rate_value_group_id')
  alter table billing_detail add rate_value_group_id int null
go
  

