alter table damage_default_est
  add utility_co_id integer NULL
GO

-- delete the old unique index that did not include the utility_co_id field
if exists (select * from dbo.sysindexes where name = N'ddest_type_size_material_profit' and id = object_id(N'[dbo].[damage_default_est]'))
  drop index damage_default_est.ddest_type_size_material_profit
GO

if exists (select * from dbo.sysindexes where name = N'ddest_type_size_material_profit_utility' and id = object_id(N'[dbo].[damage_default_est]'))
  drop index damage_default_est.ddest_type_size_material_profit_utility
GO

CREATE UNIQUE INDEX ddest_type_size_material_profit_utility ON damage_default_est(facility_type, facility_size, facility_material, profit_center, utility_co_id) ON [PRIMARY]
GO
