-- Code calling GetNextInvoiceID/dbo.NextUniqueID in TBillingExportDM.RecordInvoice should make this unique, and it is defined non-null
if not exists(select * from sys.key_constraints where name = 'PK_billing_invoice')
  ALTER TABLE billing_invoice
  ADD CONSTRAINT PK_billing_invoice PRIMARY KEY NONCLUSTERED (invoice_id)
GO

-- Code calling Inc() TPhase.AddSubPhase/TOdPerfLog.Start[Phase] sbould make the phase unique per billing run, and it is defined non-null
if not exists(select * from sys.key_constraints where name = 'PK_billing_performance_log')
  ALTER TABLE billing_performance_log
  ADD CONSTRAINT PK_billing_performance_log PRIMARY KEY NONCLUSTERED (billing_run_id, phase_id)
GO

/*billing_rate_history requires some cleanup to apply the PK*/
-- We found 12 rows in UQ test DB that have no history span thus such rows should be removed
delete from billing_rate_history where modified_date = archive_date

-- The PK columns must be non-null and should always be non-null anyway
if ((SELECT COLUMNPROPERTY(OBJECT_ID('billing_rate_history', 'U'), 'br_id', 'AllowsNull') ) = 1)
  alter table billing_rate_history alter column br_id integer NOT NULL
GO
if ((SELECT COLUMNPROPERTY(OBJECT_ID('billing_rate_history', 'U'), 'modified_date', 'AllowsNull') ) = 1)
  alter table billing_rate_history alter column modified_date datetime NOT NULL
GO

if not exists(select * from sys.key_constraints where name = 'PK_billing_rate_history')
  ALTER TABLE billing_rate_history
  ADD CONSTRAINT PK_billing_rate_history PRIMARY KEY NONCLUSTERED (br_id, modified_date)
GO

-- This is already a unique index, so it shouldn't cause any problems
if not exists(select * from sys.key_constraints where name = 'PK_value_group_detail')
  ALTER TABLE value_group_detail
  ADD CONSTRAINT PK_value_group_detail PRIMARY KEY NONCLUSTERED (value_group_id, match_type, match_value)
GO

