/* Undo commands:
alter table attachment drop column file_hash
go
*/

/* New attachment field: */
alter table attachment add file_hash varchar(40) null
go
