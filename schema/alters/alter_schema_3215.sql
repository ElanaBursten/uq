-- New right to allow deleting a message sent from anyone
if not exists (select right_id from right_definition where entity_data = 'MessagesDeleteAny')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
	values ('Messages - Delete Any', 'General', 'MessagesDeleteAny', 'Limitation does not apply to this right.')

-- Set limitation descriptions for the MessagesSend and MessagesReview rights 
update right_definition set modifier_desc = 'Limitation can be a list of manager ids representing additional parts of the hierarchy where a message can be sent.'
where entity_data = 'MessagesSend'

update right_definition set modifier_desc = 'Limitation does not apply to this right.'
where entity_data = 'MessagesReview'

-- Add active and modified_date columns to message table
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS
 where COLUMN_NAME = 'active' AND TABLE_NAME = 'message') 
begin
  alter table message
    add active bit not null default 1
end
go

if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS
 where COLUMN_NAME = 'modified_date' AND TABLE_NAME = 'message') 
begin
  alter table message
    add modified_date datetime not null default GetDate()
end
go

-- (Re)create message_iu trigger on message table
if exists(select name from sysobjects where name = 'message_iu' and type = 'TR')
  drop trigger message_iu
GO

create trigger message_iu on message
for insert, update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update message
  set modified_date = GetDate()
  from inserted I
  where message.message_id = I.message_id
end
go


/* To rollback these changes:
-- drop the new message_iu trigger
if exists(select name from sysobjects where name = 'message_iu' and type = 'TR')
  drop trigger message_iu
*/