-- Remove all vestiges of the legacy timeentry system

-- drop old report stored procedures that are no longer used:
if object_id('dbo.RPT_timesheetexception_2') is not null
  drop procedure dbo.RPT_timesheetexception_2
go
if object_id('dbo.RPT_emp_timesheet') is not null
  drop procedure dbo.RPT_emp_timesheet
go
if object_id('dbo.RPT_mgr_timesheets3') is not null
  drop procedure dbo.RPT_mgr_timesheets3
go
if object_id('dbo.tse_export_adp') is not null
  drop procedure dbo.tse_export_adp
go


-- Safely delete the LegacyHoursEntry Right & its related data
declare @RightID integer
declare @OkToDelete bit

-- get the right_id for the LegacyHoursEntry right
set @RightID = (select right_id from right_definition where entity_data = 'LegacyHoursEntry')

-- remove references to the LegacyHoursEntry right if is not allowed for an employee/group
delete from employee_right where right_id = @RightID and allowed = 'N' 
delete from group_right where right_id = @RightID and allowed = 'N' 

-- remove references to the LegacyHoursEntry right for inactive employees
delete from employee_right 
where right_id = @RightID and emp_id in (select emp_id from employee where active = 0)

-- remove references to the LegacyHoursEntry right for inactive groups
delete from group_right 
where right_id = @RightID and group_id in (select group_id from group_definition where active = 0)

-- check if any emp still has the right
if exists (select * from employee_right where right_id = @RightID)
  set @OkToDelete = 0
else
  set @OkToDelete = 1
  
if (@OkToDelete = 1) 
  -- check if any group still has the right
  if not exists (select * from group_right where right_id = @RightID) 
    -- no employee or group references; its safe to delete the right_definition
    delete from right_definition where right_id = @RightID
  else
    -- found group(s) with the right
    select 'ERROR: Active Group granted LegacyEntryHours right', 
      g.group_id, g.group_name, g.active, e.emp_id
    from group_definition g 
    inner join employee_group e on e.group_id = g.group_id 
    where g.group_id in (select group_id from group_right where right_id = @RightID)
else
  -- found emps with the right
  select 'ERROR: Active Employee granted LegacyEntryHours right', e.emp_id, 
    e.emp_number, e.active, e.short_name 
  from employee e inner join employee_right r on e.emp_id = r.emp_id
  where r.right_id = @RightID

if @OkToDelete = 0 begin
  raiserror ('Active employees or groups are granted the old time entry right (LegacyHoursEntry). The script cannot continue.', 16, 1) with log
  return
end

-- Drop the obsolete timeentry & vehicle use tables from the database. 

/*
-- Once the data in these tables has been archived, run this block of sql to 
-- drop the 4 old tables from the database.

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'timesheet_detail') begin
  drop trigger timesheet_detail_u
  drop table timesheet_detail
end
go

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'timesheet') begin
  drop trigger timesheet_u
  drop table timesheet
end

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'vehicle_use') begin
  drop trigger vehicle_use_u
  drop table vehicle_use
end
go

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'vehicle_type') begin
  drop trigger vehicle_type_u
  drop table vehicle_type
end
go

*/
