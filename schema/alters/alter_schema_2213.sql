if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'AttachmentsRemove') 
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
    values ('Attachments - Remove', 'General', 'AttachmentsRemove', 'Limitation does not apply to this right.')

