--Add new right that permits a user to set a damage's responsibility type
if not exists (select * from right_definition where entity_data = 'DamagesSetResponsibility')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesSetResponsibility', 'Damages - Set Responsibility', 'General', 
    'Limitation does not apply to this right.')
go

--Add new right that allows a user to edit an Approved Damage
if not exists (select * from right_definition where entity_data = 'DamagesEditWhenApproved')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesEditWhenApproved', 'Damages - Edit When Approved', 'General', 
    'Limitation does not apply to this right.')
go

-- alter damage constraint to allow new status,"PENDING APPROVAL"
if exists (select * from sys.check_constraints where object_id = OBJECT_ID(N'[dbo].[damage_type_ok]') and parent_object_id = OBJECT_ID(N'[dbo].[damage]'))
  alter table damage drop constraint damage_type_ok
go

alter table damage 
  add constraint damage_type_ok 
  check ((damage_type = 'APPROVED' or (damage_type = 'COMPLETED' 
    or (damage_type = 'PENDING APPROVAL') or (damage_type = 'PENDING' 
    or damage_type = 'INCOMING'))))
go

-- add new field to the profit_center table
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'profit_center' and COLUMN_NAME = 'manager_emp_id')
  alter table profit_center add manager_emp_id integer null
go

-- add new columns to the damage table
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME = 'estimate_agreed')
  alter table damage add estimate_agreed bit NOT NULL default 0
go
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME = 'approved_by_id')
  alter table damage add approved_by_id integer NULL
go
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME = 'approved_datetime')
  alter table damage add approved_datetime datetime NULL
go
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage' and COLUMN_NAME = 'reason_changed')
  alter table damage add reason_changed int NULL 
go  

-- new columns for damage_history table
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'exc_resp_other_desc')
  alter table damage_history add exc_resp_other_desc varchar(100) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'exc_resp_details')
  alter table damage_history add exc_resp_details varchar(MAX) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'exc_resp_response')
  alter table damage_history add exc_resp_response varchar(MAX) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'uq_resp_other_desc')
  alter table damage_history add uq_resp_other_desc varchar(100) NULL
go    
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'uq_resp_details')
  alter table damage_history add uq_resp_details varchar(MAX) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'uq_resp_ess_step')
  alter table damage_history add uq_resp_ess_step varchar(5) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'spc_resp_details')
  alter table damage_history add spc_resp_details varchar(MAX) NULL
go  
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'damage_history' and COLUMN_NAME = 'reason_changed')
  alter table damage_history add reason_changed int NULL 
go  

-- Add damage reasons to reference table
if not exists (select * from reference where type='dmgreason' and code='ERROR')
  insert into reference (type, code, description, sortby, active_ind)
  values ('dmgreason','ERROR','Data entry error', 1, 1)

if not exists (select * from reference where type='dmgreason' and code='ADDINFO')
  insert into reference (type, code, description, sortby, active_ind)
  values ('dmgreason','ADDINFO','Additional information became available', 2, 1)

if not exists (select * from reference where type='dmgreason' and code='OTHER')
  insert into reference (type, code, description, sortby, active_ind)
  values ('dmgreason','OTHER','Other', 3, 1)
  
go 


