-- 2594 - Add new gps_position table; new locate & locate_status fields to link to a gps_position row
--        Also requires an updated locate_iu trigger to populate locate_status.gps_id from locate data.
--        New GPS tracking right.

-- Data Updates:
if not exists (select * from right_definition where entity_data = 'TrackGPSPosition')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('TrackGPSPosition', 'Location - Track GPS Position', 'General', 
    'Limitation is the threshold in feet when computing if a locate is on or off site.')
go

-- Metadata Updates:
if not exists (select * from INFORMATION_SCHEMA.Columns where table_name = 'locate' and column_name = 'gps_id')
  alter table locate add gps_id int null
go

if not exists (select * from INFORMATION_SCHEMA.Columns where table_name = 'locate_status' and column_name = 'gps_id')
  alter table locate_status add gps_id int null
go

-- GPS fix for an employee at a point in time
if not exists (select * from INFORMATION_SCHEMA.Tables where table_name = 'gps_position')
  create table gps_position (
    gps_id int not null identity(90000, 1),
    added_by_id int not null,
    added_date datetime not null,
    latitude decimal(9,6) not null,
    longitude decimal(9,6) not null,
    hdop_feet integer not null,
    constraint pk_gps_position primary key (gps_id)
  )
go

if not exists (select constraint_name from INFORMATION_SCHEMA.table_constraints 
  where constraint_name = 'locate_fk_gps_position' and table_name = 'locate' and constraint_type = 'foreign key') 
  alter table locate 
    add constraint locate_fk_gps_position
    foreign key (gps_id) references gps_position (gps_id)

if not exists (select constraint_name from INFORMATION_SCHEMA.table_constraints 
  where constraint_name = 'gps_position_fk_employee' and table_name = 'gps_position' and constraint_type = 'foreign key') 
  alter table gps_position
    add constraint gps_position_fk_employee
    foreign key (added_by_id) references employee (emp_id)
go
