/* alter_schema_20.sql - adds a new field to employee & employee_history */
alter table employee add hire_date datetime null
go

alter table employee_history add hire_date datetime null
go
