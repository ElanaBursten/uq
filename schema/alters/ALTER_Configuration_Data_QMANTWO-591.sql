USE [QM]                --QMANTWO-591
GO

Delete from [dbo].[configuration_data]
where [name] = 'JobSafetyURL'
GO

INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[editable]
           ,[description])
     VALUES
           ('JobSafetyURL'
           ,'https://qmcluster.utiliquest.com/UTQPortal/JobSafetyForm/Home/QForm?'
           ,1
           ,'Safety form')
GO

SELECT *
  FROM [QM].[dbo].[configuration_data]
  where name = 'JobSafetyURL'