-- 2367: add new TimesheetsPriorDayEntry right if it doesn't exist
if not exists (select right_id from right_definition where entity_data='TimesheetsPriorDayEntry') 
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
	values ('Timesheets - Prior Day Entry', 'General', 'TimesheetsPriorDayEntry', 'Limitation does not apply to this right')