-- alter_schema_3040_clean.sql -- Run this script prior to running the alter_schema_3040.sql script.

/* Prepare exported_tasks table for PK addition */
/* Remove old exported_tasks rows containing nulls in customer_number or manager_emp_number; current version of export_tasks sp prevents nulls */
delete from exported_tasks where (customer_number is null) or (manager_emp_number is null)

/* Remove duplicate rows from exported_tasks */
select distinct * into #UniqueExportedTasks from exported_tasks
truncate table exported_tasks
insert exported_tasks select * from #UniqueExportedTasks
drop table #UniqueExportedTasks

/* Remove duplicate rows from exported_damages */
select distinct * into #UniqueExportedDamages from exported_damages
truncate table exported_damages
insert exported_damages select * from #UniqueExportedDamages
drop table #UniqueExportedDamages

/* Remove duplicate rows from exported_damages_history */
select distinct * into #UniqueExportedDamagesHistory from exported_damages_history
truncate table exported_damages_history
insert exported_damages_history select * from #UniqueExportedDamagesHistory
drop table #UniqueExportedDamagesHistory

