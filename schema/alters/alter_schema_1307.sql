/*
 This script will add the new uq_damage_id field to the damage table &
 make changes to related triggers & stored procedures.
*/

-- Add field to damage table.
alter table damage
  add uq_damage_id int null
go

/* !!!!!! -- Tested, but not ready to implement
-- Populate new field
update damage
  set uq_damage_id = damage_id
  where uq_damage_id is null
go
create unique index damage_uq_damage_id on damage(uq_damage_id)
go
-- Populate next id in config data
insert into configuration_data (name, value, editable) select 'NextDamageID', max(damage_id), 0 from damage
go
*/



