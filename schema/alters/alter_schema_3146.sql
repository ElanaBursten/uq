-- Fix all reference types so blank values are saved correctly
update reference set code = 'B'
where (type='buried' or type='uqresp' or type='exresp' or type='spresp')
  and description is null
  and code is null

-- Mark unused reference types inactive
update reference set active_ind = 0
where (type = 'uqresp' or type = 'exresp' or type = 'spresp')

/* to undo:
update reference set code = null
where (type='buried' or type='uqresp' or type='exresp' or type='spresp')
  and description is null
  and code = 'B'

update reference set active_ind = 1
where (type = 'uqresp' or type = 'exresp' or type = 'spresp')

*/
