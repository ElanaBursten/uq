/* Adds reference and configuration data to allow the viewing CV files in reports as images */

if not exists (select * from reference where type = 'printable' and code = '.@CV')
  insert into reference (type, code, description, sortby, active_ind, modifier)
    values ('printable', '.@CV', 'Dycom Container File', 0, 1, 'Image')

if not exists (select * from configuration_data where name = 'CVReportRegex')
  insert into configuration_data (name, value, editable)
    values ('CVReportRegex', 'manifest', 1)

if not exists (select * from upload_file_type where extension = '@cv')
  insert into upload_file_type (extension, max_kilobytes, size_error_message, active)
    values ('@cv', 3000, 'The file you are trying to attach has exceeded the acceptable limit. The @CV file is too large to be attached.', 1)