

/****** Object:  Table [dbo].[oq_data_history]    Script Date: 8/13/2021 11:25:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[oq_data_history](
	[oq_data_history_id] [int] IDENTITY(1,1) NOT NULL,
	[oq_data_id] [int] NOT NULL,
	[ref_id] [int] NULL,
	[emp_id] [int] NULL,
	[qualified_date] [datetime] NULL,
	[expired_date] [datetime] NULL,
	[modified_by] [int] NULL,
	[status] [varchar](20) NULL,
	[active] [bit] NULL,
	[modified_date] [datetime] NULL,
	[history_date] [datetime] NULL
) ON [PRIMARY]
GO


