if object_id('employee_activity') is not null
  drop table employee_activity
go

create table employee_activity (
   emp_activity_id int identity(22000, 1) not null primary key nonclustered,
   emp_id int not null,
   activity_date datetime not null, /* Employee time zone */
   activity_type varchar(12) not null,
   details varchar(50) null,
   insert_date datetime not null default GetDate()
)
go

-- Cluster by activity date to keep from clustering on autoinc ID
create clustered index employee_activity_activity_date
   on employee_activity(activity_date)
go

-- Employee / date index so reports work
create index employee_activity_emp_id_activity_date
   on employee_activity(emp_id, activity_date)
go

-- Foreign key link to employee
alter table employee_activity
   add constraint employee_activity_fk_employee
   foreign key (emp_id) references employee (emp_id)
go


-- Deletes & recreates all employee activity type reference rows:
delete from reference where (type = 'empact')
go

insert into reference (type, code, description) values ('empact', 'SLEEP',  'PC Sleep')
insert into reference (type, code, description) values ('empact', 'WAKE',   'PC Wake')
insert into reference (type, code, description) values ('empact', 'WINON',  'Windows Logon')
insert into reference (type, code, description) values ('empact', 'WINOFF', 'Windows Logoff')
insert into reference (type, code, description) values ('empact', 'WINLCK', 'Windows Lock')
insert into reference (type, code, description) values ('empact', 'WINULK', 'Windows Unlock')
insert into reference (type, code, description) values ('empact', 'WINCON', 'Logged on')
insert into reference (type, code, description) values ('empact', 'WINDIS', 'Logged off')
insert into reference (type, code, description) values ('empact', 'START',  'QM Logon')
insert into reference (type, code, description) values ('empact', 'QUIT',   'QM Logoff')
insert into reference (type, code, description) values ('empact', 'TKVUE',  'View Ticket')
insert into reference (type, code, description) values ('empact', 'TKRTE',  'Route Ticket')
insert into reference (type, code, description) values ('empact', 'TKFND',  'Find Ticket')
insert into reference (type, code, description) values ('empact', 'TKMOV',  'Move Ticket')
insert into reference (type, code, description) values ('empact', 'TKNEW',  'New Ticket')
insert into reference (type, code, description) values ('empact', 'MSGV',   'View Message')
insert into reference (type, code, description) values ('empact', 'SYNCF',  'Failed Sync')

insert into reference (type, code, description) values ('empact', 'SYNC',   'Sync')
insert into reference (type, code, description) values ('empact', 'PWROFF', 'PC Shutdown')
insert into reference (type, code, description) values ('empact', 'WSTAR',  'Work Start')
insert into reference (type, code, description) values ('empact', 'WSTOP',  'Work Stop')
insert into reference (type, code, description) values ('empact', 'CSTAR',  'Callout Start')
insert into reference (type, code, description) values ('empact', 'CSTOP',  'Callout Stop')
insert into reference (type, code, description) values ('empact', 'ADDNT',  'Add Note')
insert into reference (type, code, description) values ('empact', 'LOCST',  'Status Locate')
insert into reference (type, code, description) values ('empact', 'ATUPL',  'Attachment Upload')
insert into reference (type, code, description) values ('empact', 'LOCHR',  'Hourly Locate')
insert into reference (type, code, description) values ('empact', 'RUNRE',  'Run Report')
insert into reference (type, code, description) values ('empact', 'TSENT',  'Timesheet Entry')
insert into reference (type, code, description) values ('empact', 'APRTS',  'Timesheet Approval')
insert into reference (type, code, description) values ('empact', 'ATADD',  'Attachment Add')
insert into reference (type, code, description) values ('empact', 'WEBUPD', 'Web Update')


go