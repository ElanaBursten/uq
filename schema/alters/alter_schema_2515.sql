-- Add new Ticket Selective Export right if it does not exist
if not exists (select right_id from right_definition where entity_data = 'TicketsSelectiveExport')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc) 
  values ('Tickets - Selective Export', 'General', 'TicketsSelectiveExport', 
    'Limitation does not apply to this right.') 

go
