/* 
Add source_sent_attachment column to work_order; It is a Y|N flag 
from the work order feed. It doesn't mean an attachment exists. 
*/

IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS 
  where TABLE_NAME = 'work_order' and COLUMN_NAME = 'source_sent_attachment')
    alter table work_order add source_sent_attachment varchar(1) null 
go

