alter table statuslist
  add mark_type_required bit not null default 0,
  default_mark_type varchar(10) null
GO

alter table locate
  add mark_type varchar(10) null
GO

alter table locate_status
  add mark_type varchar(10) null
GO

insert into reference (type, code, description, sortby) values ('marktype', 'NEW MARK', 'New mark', 10)
insert into reference (type, code, description, sortby)	values ('marktype', 'NO MARK', 'No mark', 20)
insert into reference (type, code, description, sortby)	values ('marktype', 'RE-MARK', 'Re-mark', 30)
GO
