/*
-- cleanup from old alter scripts first
if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'billing_unit_conversion')
  drop table dbo.billing_unit_conversion
if exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'locate_hours' and COLUMN_NAME = 'unit_conversion_id')
  alter table locate_hours drop column unit_conversion_id
if exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'client' and COLUMN_NAME = 'unit_conversion_id')
  alter table client drop column unit_conversion_id
go
*/

create table billing_unit_conversion (      -- Rules on how to convert units marked to units billed
  unit_conversion_id integer identity(38000,1) primary key,
  unit_type varchar(10) null,               -- how the marked units are measured (units, feet, moves, ...)
  first_unit_factor integer not null default 1, -- how many units marked count as first unit billed
  rest_units_factor integer null,               -- how many units marked between remaining units billed (if different)
  modified_date datetime not null default getdate(),
  active bit not null default 1,
  insert_date datetime not null default getdate(),
  modified_by integer null,
  added_by integer null
)

alter table locate_hours
  add unit_conversion_id integer null -- The conversion rule applied to the entered units
alter table client
  add unit_conversion_id integer null -- The conversion rule currently used for this client

/*
 To compute the description:
select bu.unit_conversion_id as ID, CASE
  WHEN bu.rest_units_factor IS Null THEN 'Bill 1 every ' + cast(bu.first_unit_factor as varchar(10)) + ' ' + bu.unit_type
  ELSE  'Bill 1 first ' + cast(bu.first_unit_factor as varchar(10)) +
        ' ' + bu.unit_type + '; 1 each additional ' + cast(bu.rest_units_factor as varchar(10)) + ' ' + bu.unit_type
END as Description
from billing_unit_conversion bu order by bu.unit_conversion_id
*/

/* TODO:
 Need a before update trigger on billing_unit_conversion to prevent editing
 of unit_type, first_unit_factor, rest_units_factor on any unit_conversion_id
 row that is being used by a billing_detail record.
 (if exists select top 1 unit_conversion_id from billing_detail where unit_conversion_id = update.unit_conversion_id)
 OR, those 3 fields need to be added to billing_detail instead of just the id for historical tracking)

 Foreign keys on locate_hours, client, & billing_detail?

 Need to alter billing_detail
 alter table billing_detail
	add unit_conversion_id integer
*/

go
