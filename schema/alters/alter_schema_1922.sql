/* Changes to damage table structure: */
alter table damage
  add locator_id integer null
go

/* New get_locator_for_damage function: */
if object_id('dbo.get_locator_for_damage') is not null
  drop function dbo.get_locator_for_damage
go

create function get_locator_for_damage (
  @DamageID integer
)
returns integer
as
begin
  declare @LocatorID integer
  select @LocatorID = null
  
  -- Get the locator ID from the damage table
  select @LocatorID = (select locator_id from damage where damage_id = @DamageID)

  -- Get the locator ID from the locate for the ticket & client associated with the damage
  if @LocatorID is null 
    select @LocatorID = (select l.assigned_to_id
      from damage d
      inner join locate l on l.ticket_id = d.ticket_id and l.client_id = d.client_id
      where d.damage_id = @DamageID and l.status <> '-N')

  -- Get the locator ID from ticket_snap if none found for damaged client
  if @LocatorID is null
    select @LocatorID = (select ts.first_close_emp_id
      from damage d
      inner join ticket_snap ts on ts.ticket_id = d.ticket_id
      where d.damage_id = @DamageID)

  -- Get the locator ID from locates if none found for client or ticket_snap
  if @LocatorID is null
    select @LocatorID = (select top 1 l.assigned_to_id 
    from damage d 
    inner join locate l on l.ticket_id = d.ticket_id
    where d.damage_id = @DamageID and l.status <> '-N')

  return @LocatorID
end
GO

grant execute on get_locator_for_damage to uqweb, QManagerRole

/* Recreate dycom_damage view: */
if exists (select TABLE_NAME from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'dycom_damage')
  drop view dbo.dycom_damage
go

create view dycom_damage
as
  select d.*, dp.short_period as fiscal_period,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id asc) as initial_estimate,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id desc) as current_estimate,
  dbo.get_locator_for_damage(d.damage_id) as emp_id
  from damage d
  left join dycom_period dp on d.accrual_date >= dp.starting and accrual_date < dp.ending
go
