/* Add primary keys to change tracking tables */

if not exists(select * from sys.key_constraints where name = 'PK_area_change')
  alter table area_change 
    add constraint PK_area_change 
    primary key nonclustered (area_id, changed_by, change_date)
go

if not exists(select * from sys.key_constraints where name = 'PK_asset_assignment_history')
  alter table asset_assignment_history 
    add constraint PK_asset_assignment_history 
    primary key nonclustered (assignment_id, archive_date)
go

if not exists(select * from sys.key_constraints where name = 'PK_asset_history')
  alter table asset_history 
    add constraint PK_asset_history 
    primary key nonclustered (asset_id, archive_date)
go

if not exists(select * from sys.key_constraints where name = 'PK_employee_history')
  alter table employee_history 
    add constraint PK_employee_history 
    primary key nonclustered (emp_id, active_start)
go

if not exists(select * from sys.key_constraints where name = 'PK_oldest_open_log')
  alter table oldest_open_log
    add constraint PK_oldest_open_log
    primary key nonclustered (insert_date)
go
