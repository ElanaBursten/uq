--Add new right that allows user to create Follow Up Tickets
if not exists (select * from right_definition where entity_data = 'TicketsCreateFollowUp')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('TicketsCreateFollowUp', 'Tickets - Create Follow Up', 'General', 'Limitation does not apply to this right.')
go