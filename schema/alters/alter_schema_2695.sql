-- Add column to locate table to track user who actually statused the locate for reporting purposes.  The app otherwise uses 
-- closed_by_id (which should have been named statused_by_id) to determine who statused the locate based on the "Status As Assigned User" checkbox.
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'locate' and COLUMN_NAME='status_changed_by_id')
  alter table locate add status_changed_by_id int null
go

--Add the new column to locate_status
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'locate_status' and COLUMN_NAME='status_changed_by_id')
  alter table locate_status add status_changed_by_id int null
go

--Employee Activity reference entry
if not exists (select * from reference where type = 'empact' and code = 'LOCSTA') 
  insert into reference (type, code, description, active_ind) values ('empact', 'LOCSTA', 'Status Locate as Assigned Employee',1)
go
