if not exists(select table_name from information_schema.columns
  where table_name = 'upload_location' and column_name = 'port')
  alter table upload_location add port int not null default 21
go
  
if not exists(select table_name from information_schema.table_constraints
  where table_name = 'upload_location' and constraint_name = 'port_in_range' and
  constraint_type = 'check')
  alter table upload_location
    add constraint port_in_range check(port >= 1 and port <= 65535)
go
