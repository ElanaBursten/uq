-- Safely delete the SyncAutoSyncFirstLogin Right & its related data
declare @RightID integer
declare @OkToDelete bit

-- get the right_id for the SyncAutoSyncFirstLogin right
set @RightID = (select right_id from right_definition where entity_data = 'SyncAutoSyncFirstLogin')

-- remove references to the SyncAutoSyncFirstLogin right for inactive employees
delete from employee_right 
where right_id = @RightID and emp_id in (select emp_id from employee where active = 0)

-- remove references to the SyncAutoSyncFirstLogin right for inactive groups
delete from group_right 
where right_id = @RightID and group_id in (select group_id from group_definition where active = 0)

-- check if any emp still has the right
if exists (select * from employee_right where right_id = @RightID)
  set @OkToDelete = 0
else
  set @OkToDelete = 1
  
if (@OkToDelete = 1) 
  -- check if any group still has the right
  if not exists (select * from group_right where right_id = @RightID) 
    -- no employee or group references; its safe to delete the right_definition
    delete from right_definition where right_id = @RightID
  else
    -- found group(s) with the right
    select 'ERROR: Active Group granted SyncAutoSyncFirstLogin right', 
      g.group_id, g.group_name, g.active, e.emp_id
    from group_definition g 
    inner join employee_group e on e.group_id = g.group_id 
    where g.group_id in (select group_id from group_right where right_id = @RightID)
else
  -- found emps with the right
  select 'ERROR: Active Employee granted SyncAutoSyncFirstLogin right', e.emp_id, 
    e.emp_number, e.active, e.short_name 
  from employee e inner join employee_right r on e.emp_id = r.emp_id
  where r.right_id = @RightID

if @OkToDelete = 0 begin
  raiserror ('Active employees or groups are granted the old auto sync on first login right (SyncAutoSyncFirstLogin). The script cannot continue.', 16, 1) with log
  return
end