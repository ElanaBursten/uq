/*Schema changes for the new Work Orders subsystem*/

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'work_order')
create table work_order (
  wo_id integer identity (110000, 1) not null, /* QM internal identity */
  wo_number varchar(20) not null,     /* incoming InternalNumber prefixed with 'LAM'; UQ's primary reference to a wo */
  assigned_to_id integer null,        /* employee.emp_id assigned to the work order */
  client_id integer null,             /* client.client_id the work is for */
  parsed_ok bit null,                 /* 1 if any parser errors */
  wo_source varchar(20) not null,     /* aka call center; used to determine parser, responder, xml translation, etc for this wo*/
  kind varchar(40) not null,          /* PRIORITY, NORMAL, CANCEL */
  status varchar(5) not null,         /* the current statuslist.status of this work order */
  closed bit not null,                /* Is the work done */
  map_page varchar(20) null,          /* job site�s paper map page reference */
  map_ref varchar(60) null,           /* additional paper map references */
  transmit_date datetime not null,    /* when did QM get the work order */
  due_date datetime not null,         /* when does the work need to be done */
  closed_date datetime null,          /* datetime the work was completed */
  status_date datetime null,          /* datetime the status was last changed */
  cancelled_date datetime null,       /* datetime the work was canceled */
  work_type varchar(90) null,         /* summary of work to do */
  work_description varchar(3500) null, /* detailed explanation of the work to do */
  work_address_number varchar(10) null, /* number part of the work�s street address*/
  work_address_number_2 varchar(10) null, /* ending number part of the work�s address in case the job site is a range of addresses*/
  work_address_street varchar(60) null, /* street name of the job site*/
  work_cross varchar(100) null,       /* nearest cross street to the job site*/
  work_county varchar(40) null,       /* county name of the job site */
  work_city varchar(40) null,         /* city name of the job site*/
  work_state varchar(2) null,         /* state abbreviation of the job site */
  work_zip varchar(10) null,          /* zip code of the job site*/
  work_lat decimal(9,6) null,         /* job site�s latitude*/
  work_long decimal(9,6) null,        /* job site�s longitude*/
  caller_name varchar(50) null,       /* work reqeuester */
  caller_contact varchar(50) null,    /* name of contact person requesting the work*/
  caller_phone varchar(40) null,      /* caller�s primary phone number*/
  caller_cellular varchar(40) null,   /* caller�s cell*/
  caller_fax varchar(40) null,        /* caller�s fax*/
  caller_altcontact varchar(40) null, /* name of alternate contact*/
  caller_altphone varchar(40) null,   /* alternate contact�s phone*/
  caller_email varchar(40) null,      /* caller�s email address*/
  client_wo_number varchar(40) not null, /* client system's primary id for a work order*/
  image text null,                    /* raw XML representation of the work order */
  parse_errors text null,             /* description of any problems detected by the parser*/
  update_of_wo_id integer null,       /* wo_id of the work_order this one updates */
  modified_date datetime not null default GetDate(), /* sync support */
  active bit not null default 1,      /* 0 if this work order is inactive */

  job_number varchar(20) null,
  client_order_num varchar(20) null,
  client_master_order_num varchar(20) null,
  wire_center varchar(20) null, 
  work_center varchar(20) null,
  central_office varchar(20) null,
  serving_terminal varchar(20) null,
  circuit_number varchar(20) null,
  f2_cable varchar(20) null,
  terminal_port varchar(20) null,
  f2_pair varchar(20) null,

  state_hwy_row varchar(10) null,     /* a 'strow' reference.code value */
  road_bore_count integer null,       /* Workers can set this using the Work Order Detail screen.*/
  driveway_bore_count integer null,   /* Workers can set this using the Work Order Detail screen.*/
  )     
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_work_order_wo_id')
  alter table work_order add constraint PK_work_order_wo_id primary key nonclustered (wo_id)
go

IF NOT EXISTS (select * from sys.indexes where name = 'work_order_wo_number')
  create index work_order_wo_number on work_order (wo_number)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_due_date')  
  create index work_order_due_date on work_order (due_date)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_modified_date')  
  create index work_order_modified_date on work_order (modified_date)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_transmit_date')  
  create index work_order_transmit_date on work_order (transmit_date)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_status')  
  create index work_order_status on work_order (status)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_client_wo_number')  
  create index work_order_client_wo_number on work_order (client_wo_number)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_assigned_to')  
  create index work_order_assigned_to on work_order (assigned_to_id)
go

--Assignment must be to a valid employee
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'work_order' and CONSTRAINT_NAME = 'work_order_fk_assigned_to_id')
  alter table work_order
    add constraint work_order_fk_assigned_to_id
    foreign key (assigned_to_id) references employee (emp_id)
go
--Must reference valid client
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'work_order' and CONSTRAINT_NAME = 'work_order_fk_client_id')
  alter table work_order
    add constraint work_order_fk_client_id
    foreign key (client_id) references client (client_id)
go

/* Associates work orders with tickets */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'work_order_ticket')
create table work_order_ticket (
  wo_id integer not null,          /* ref to work_order.wo_id */
  ticket_id integer not null,      /* ref to ticket.ticket_id */
  modified_date datetime not null DEFAULT getdate(), /* datetime this link was last changed */
  active bit not null DEFAULT 1    /* is this link active */
  )
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_work_order_ticket')
  alter table work_order_ticket add constraint PK_work_order_ticket primary key nonclustered (wo_id, ticket_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'work_order_ticket_ticket_id')  
  create index work_order_ticket_ticket_id on work_order_ticket (ticket_id)
go
-- Must be a valid work order
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'work_order_ticket' and CONSTRAINT_NAME = 'work_order_ticket_fk_wo_id')
  alter table work_order_ticket
    add constraint work_order_ticket_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go

-- Must be a valid ticket
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'work_order_ticket' and CONSTRAINT_NAME = 'work_order_ticket_fk_ticket_id')
  alter table work_order_ticket
    add constraint work_order_ticket_fk_ticket_id
    foreign key (ticket_id) references ticket (ticket_id)
go

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_assignment')
create table wo_assignment (
  assignment_id integer identity (210000, 1) not null,
  wo_id integer not null,                            /* work_order.wo_id link */
  assigned_to_id integer not null,                   /* Employee assigned to the work order */
  insert_date datetime not null default GetDate(),   /* Always the server default date */
  added_by varchar(8) not null,                      /* PARSER, or emp_id of user */
  modified_date datetime not null default GetDate(), /* for sync support */
  active bit not null default 1,                     /* Is the work order assignment current or historical */
)
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_wo_assignment_assignment_id')
  alter table wo_assignment add constraint PK_wo_assignment_assignment_id primary key nonclustered (assignment_id)
go

IF NOT EXISTS (select * from sys.indexes where name = 'wo_assignment_wo_assigned_to')  
  create clustered index wo_assignment_wo_assigned_to on wo_assignment (wo_id, assigned_to_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_assignment_assigned_to')  
  create index wo_assignment_assigned_to on wo_assignment (assigned_to_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_assignment_modified_date')  
  create index wo_assignment_modified_date on wo_assignment (modified_date)
go
-- Must be a valid work order
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_assignment' and CONSTRAINT_NAME = 'wo_assignment_fk_wo_id')
  alter table wo_assignment
    add constraint wo_assignment_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Assignment must be to a valid employee
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_assignment' and CONSTRAINT_NAME = 'wo_assignment_fk_assigned_to_id')
  alter table wo_assignment
    add constraint wo_assignment_fk_assigned_to_id
    foreign key (assigned_to_id) references employee (emp_id)
go

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_status_history')
create table wo_status_history (
  wo_status_id integer identity (310000, 1) not null, 
  wo_id integer not null,        /* link to work_order.wo_id */
  status_date datetime not null, /* local time when this status was saved */
  status varchar(5) not null,    /* the status code */
  statused_by_id integer null,   /* an employee.emp_id that entered the status */
  statused_how varchar(20) null, /* how was the status changed 'PARSER', 'DETAIL', 'MANAGE' */
  insert_date datetime not null default GetDate(), /* db server datetime this status change was added to history */
  )
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_wo_status_history_wo_status_id')
  alter table wo_status_history add constraint PK_wo_status_history_wo_status_id primary key nonclustered (wo_status_id)
go

IF NOT EXISTS (select * from sys.indexes where name = 'wo_status_history_wo')  
  create index wo_status_history_wo on wo_status_history (wo_id, status_date)
go

--Must reference valid work order
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_status_history' and CONSTRAINT_NAME = 'wo_status_history_fk_wo_id')
  alter table wo_status_history
    add constraint wo_status_history_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Must reference valid employee
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_status_history' and CONSTRAINT_NAME = 'wo_status_history_fk_statused_by_id')
  alter table wo_status_history
    add constraint wo_status_history_fk_statused_by_id
    foreign key (statused_by_id) references employee (emp_id)
go

/* TODO: 
  need a stored procedure to populate
  need more foreign keys?
*/
/* wo_responder_queue will be populated by QML; a work order response can only be sent after all its attachments have an upload_date */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_responder_queue')
create table wo_responder_queue (
  wo_id int not null,     /* link to the work_order.wo_id */
  client_wo_number varchar(40) null,  /* work_order.client_wo_number - here to minimize responder lookups */
  insert_date datetime not null default GetDate(), /* date the response was queued */
  wo_source varchar(20) null,  /* work_order.source - here to minimize responder lookups */
  client_id integer null,      /* work_order.client_id - here to minimize responder lookups */
  ready_to_respond bit null,   /* false until all attachments are uploaded */
  )
go

IF NOT EXISTS (select * from sys.indexes where name = 'wo_responder_queue_wo_id')  
  create index wo_responder_queue_wo_id on wo_responder_queue(wo_id)
go

--Must reference valid work order
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_responder_queue' and CONSTRAINT_NAME = 'wo_responder_queue_fk_wo_id')
  alter table wo_responder_queue
    add constraint wo_responder_queue_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Must reference valid client
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_responder_queue' and CONSTRAINT_NAME = 'wo_responder_queue_fk_client_id')
  alter table wo_responder_queue
    add constraint wo_responder_queue_fk_client_id
    foreign key (client_id) references client (client_id)
go


/* A log of all work order responses */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_response_log')
create table wo_response_log (
  wo_response_id integer identity (320000, 1),
  wo_id int not null,
  response_date datetime not null,
  wo_source varchar(20) not null,
  status varchar(5) not null,
  response_sent varchar(15) not null, /* whatever we translated the status to for cases where respondee needs an alternate status */
  success bit not null DEFAULT 0,     /* 0 until the respondee's ack is received */
  reply varchar(40)
)
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_wo_response_log_wo_response_id')
  alter table wo_response_log add constraint PK_wo_response_log_wo_response_id primary key nonclustered (wo_response_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_response_log_response_date')  
  create index wo_response_log_response_date on wo_response_log(response_date)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_response_log_wo_id')  
  create index wo_response_log_wo_id on wo_response_log(wo_id)
go

--Must reference valid work order
if not exists (select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_response_log' and CONSTRAINT_NAME = 'wo_response_log_fk_wo_id')
  alter table wo_response_log
    add constraint wo_response_log_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go

/* Audit report headers to reconcile work order receipts */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_audit_header')
create table wo_audit_header (
  wo_audit_header_id integer identity (23001, 1) not null,
  wo_source varchar(20) not null,   /* e.g. LAM01 */
  summary_date datetime not null,   /* transmission date */
  modified_date datetime null
)
go

if NOT EXISTS(select * from sys.key_constraints where name = 'PK_wo_audit_header_wo_audit_header_id')
alter table wo_audit_header add constraint PK_wo_audit_header_wo_audit_header_id primary key nonclustered (wo_audit_header_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_audit_header_summary_date')
create index wo_audit_header_summary_date on wo_audit_header(summary_date)
go

/* Audit report details of work order receipts for a wo_audit_header */
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wo_audit_detail')
create table wo_audit_detail (
   wo_audit_detail_id integer identity (43001, 1) not null,
   wo_audit_header_id integer not null,    /* link to wo_audit_header */
   client_wo_number varchar(40) not null,  /* the sender's work order id */
   modified_date datetime null
)
go
if NOT EXISTS(select * from sys.key_constraints where name = 'PK_wo_audit_detail_wo_audit_detail_id')
alter table wo_audit_detail add constraint PK_wo_audit_detail_wo_audit_detail_id primary key nonclustered (wo_audit_detail_id)
go
IF NOT EXISTS (select * from sys.indexes where name = 'wo_audit_detail_client_wo_number')
create index wo_audit_detail_client_wo_number on wo_audit_detail(client_wo_number)
go

-- must reference a valid wo_audit_header
if NOT EXISTS(select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE where TABLE_NAME = 'wo_audit_detail' and CONSTRAINT_NAME = 'wo_audit_detail_fk_wo_audit_header_id')
  alter table wo_audit_detail 
    add constraint wo_audit_detail_fk_wo_audit_header_id
    foreign key (wo_audit_header_id) references wo_audit_header (wo_audit_header_id)
go

--todo make sure thes are also in relevant .tsv files
--Employee Activity reference entries
if not exists (select * from reference where type = 'empact' and code = 'WOVUE')
  insert into reference (type, code, description) values ('empact', 'WOVUE', 'View Work Order')
if not exists (select * from reference where type = 'empact' and code = 'WOMOV')
  insert into reference (type, code, description) values ('empact', 'WOMOV', 'Move Work Order')
if not exists (select * from reference where type = 'empact' and code = 'WOADST')
  insert into reference (type, code, description) values ('empact', 'WOADST', 'Start WO Addin')
if not exists (select * from reference where type = 'empact' and code = 'WOADEN')
  insert into reference (type, code, description) values ('empact', 'WOADEN', 'End WO Addin')
if not exists (select * from reference where type = 'empact' and code = 'WOSAVE') 
  insert into reference (type, code, description) values ('empact', 'WOSAVE', 'Save Work Order Changes')
if not exists (select * from reference where type = 'empact' and code = 'WOCLOSE') 
  insert into reference (type, code, description) values ('empact', 'WOCLOSE', 'Close Work Order')
go

-- State Hwy ROW Reference entries
if not exists (select * from reference where type = 'strow' and code = 'Y') 
  insert into reference (type, code, description) values ('strow', 'Y', 'Yes')
if not exists (select * from reference where type = 'strow' and code = 'N') 
  insert into reference (type, code, description) values ('strow', 'N', 'No')
if not exists (select * from reference where type = 'strow' and code = 'U') 
  insert into reference (type, code, description, active_ind) values ('strow', 'U', 'Unknown', 0) -- Not used yet
if not exists (select * from reference where type = 'strow' and code = 'NA') 
  insert into reference (type, code, description, active_ind) values ('strow', 'NA', 'N/A', 0)   -- Not used yet
go

--Add new right allowing a user to reassign work orders on the Work Mgt screen
if not exists (select * from right_definition where entity_data = 'WorkOrdersManagement')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('WorkOrdersManagement', 'Work Orders - Management', 'General', 'Limitation does not apply to this right.')
go
--Add new right that immediately syncs work orders for users granted this right
if not exists (select * from right_definition where entity_data = 'WorkOrdersImmediateSync')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('WorkOrdersImmediateSync', 'Work Orders - Immediate Sync', 'General', 'Limitation does not apply to this right.')
go

