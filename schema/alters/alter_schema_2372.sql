-- new columns in sync_log for E-Sketch & Persequor file version numbers
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='sync_log' and COLUMN_NAME='persequor_version') 
  alter table sync_log add 
    addin_version varchar(30) null, 
    persequor_version varchar(30) null
go
