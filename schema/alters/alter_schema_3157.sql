if not exists (select * from information_schema.tables where table_name='employee_work_goal')
begin
  create table employee_work_goal (
    work_goal_id integer identity (81000, 1) not null,
    emp_id int not null, 
    effective_date datetime not null,
    ticket_count int not null default 0,
    locate_count int not null default 0,
    added_date datetime not null default GetDate(),
    modified_date datetime not null default GetDate(),
    active bit not null default 1,
    constraint pk_employee_work_goal primary key (work_goal_id)
  )
end
go

if exists(select name from sysobjects where name = 'employee_work_goal_iu' and type = 'TR')
  drop trigger employee_work_goal_iu
GO

create trigger employee_work_goal_iu on employee_work_goal
for insert, update
not for replication
AS
begin

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update employee_work_goal
  set modified_date = GetDate()
  from inserted I
  where employee_work_goal.work_goal_id = I.work_goal_id
end
GO

/* To rollback these changes:
-- drop the new employee_work_goal_iu trigger
if exists(select name from sysobjects where name = 'employee_work_goal_iu' and type = 'TR')
  drop trigger employee_work_goal_iu

-- drop the new employee_work_goal table
if exists (select * from information_schema.tables where table_name = 'employee_work_goal')
  drop table employee_work_goal
go
*/
