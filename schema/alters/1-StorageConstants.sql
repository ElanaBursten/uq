USE [QM]
GO

/****** Object:  Table [dbo].[api_storage_constants]    Script Date: 2/28/2018 5:24:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[api_storage_constants](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[host] [varchar](max) NOT NULL,
	[rest_url] [varchar](max) NOT NULL,
	[service] [varchar](max) NOT NULL,
	[bucket] [varchar](max) NOT NULL,
	[bucket_folder] [varchar](250) NULL,
	[region] [varchar](150) NOT NULL,
	[algorithm] [varchar](150) NOT NULL,
	[signed_headers] [varchar](max) NOT NULL,
	[content_type] [varchar](max) NOT NULL,
	[accepted_values] [varchar](max) NOT NULL,
	[environment] [varchar](50) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[unc_date_modified] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


USE [QM]
GO

INSERT INTO [dbo].[api_storage_constants]
           ([host]
           ,[rest_url]
		   ,[service]
           ,[bucket]
           ,[bucket_folder]
           ,[region]
           ,[algorithm]
           ,[signed_headers]
           ,[content_type]
           ,[accepted_values]
           ,[environment]
           ,[description]
           ,[unc_date_modified])
     VALUES
           ('o9mwuetv47.execute-api.us-east-1.amazonaws.com'
           ,'/prod/qmattach/v1/attachments/LOC_UTL'
		   ,'execute-api'
           ,'dii-utq-attachments-prod'
           ,'api/LOC_UTL/'
           ,'us-east-1'
           ,'AWS4-HMAC-SHA256'
           ,'accept;content-type;host;x-amz-date'
           ,'application/json'
           ,'application/octet-stream,application/json'
           ,'PRODUCTION'
           ,'PROD values for QM attachments'
           ,CURRENT_TIMESTAMP)
GO

USE [QM]
GO

INSERT INTO [dbo].[api_storage_constants]
           ([host]
           ,[rest_url]
		   ,[service]
           ,[bucket]
           ,[bucket_folder]
           ,[region]
           ,[algorithm]
           ,[signed_headers]
           ,[content_type]
           ,[accepted_values]
           ,[environment]
           ,[description]
           ,[unc_date_modified])
     VALUES
           ('o9mwuetv47.execute-api.us-east-1.amazonaws.com'
           ,'/prod/qmattach/v1/attachments/LOC_LOC'
		   ,'execute-api'
           ,'dii-utq-attachments-prod'
           ,'api/LOC_LOC/'
           ,'us-east-1'
           ,'AWS4-HMAC-SHA256'
           ,'accept;content-type;host;x-amz-date'
           ,'application/json'
           ,'application/octet-stream,application/json'
           ,'PRODUCTION'
           ,'PROD values forLOC attachments'
           ,CURRENT_TIMESTAMP)
GO



