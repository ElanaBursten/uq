alter table call_center add xml_ticket_format text null
go

/* Also need to apply these updated scripts:
 sync_3.sql
 RPT_get_damage_details.sql
 RPT_TicketDetail.sql
 RPT_TicketDetailById.sql
*/
