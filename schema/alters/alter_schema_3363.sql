if not exists (select * from information_schema.constraint_table_usage
  where table_name = 'status_translation' and constraint_name =
  'status_translation_fk_status')
  begin try
    alter table status_translation
    add constraint status_translation_fk_status
    foreign key (status) references statuslist (status) not for replication 
  end try
  begin catch
    select status as invalid_status, * from status_translation s
    where not exists(select * from statuslist l where l.status = s.status)
    raiserror ('Error adding foreign key for status_translation.status. Check the result set for invalid_status values.', 16, 2)
  end catch
go

if not exists (select * from information_schema.constraint_table_usage
  where table_name = 'status_translation' and constraint_name =
  'status_translation_fk_cc_code')
  begin try
    alter table status_translation
    add constraint status_translation_fk_cc_code
    foreign key (cc_code) references call_center (cc_code) not for replication 
  end try
  begin catch
    select cc_code as invalid_cc_code, * from status_translation s
    where not exists(select * from call_center c where c.cc_code = s.cc_code)
    raiserror ('Error adding foreign key for status_translation.cc_code. Check the result set for invalid_cc_code values.', 16, 2)
  end catch
go

if not exists(select * from sys.check_constraints where name = 'status_translation_check_blank_outgoing_status')
  begin try
    alter table status_translation 
    add constraint status_translation_check_blank_outgoing_status
    check not for replication (outgoing_status <> '') 
  end try
  begin catch
    select 'blank outgoing_status' as ERROR, * from status_translation
    where outgoing_status = ''
    raiserror ('Error adding check constraint for status_translation.outgoing_status. Check the result set for ERROR = "blank outgoing_status".', 16, 2)
  end catch
go
