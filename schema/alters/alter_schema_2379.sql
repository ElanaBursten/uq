/* Remove old stored procedures that 2008 Upgrade Advisor complains about */

/* These are not in the source repository, so they are likely unused & can be dropped.
   If they are used by some other non-QM process then they should be fixed instead.

if object_id('dbo.sp_SEL_Employee') is not null
	drop procedure dbo.sp_SEL_Employee
go
if object_id('dbo.sp_SEL_EmpTypeDD') is not null
	drop procedure dbo.sp_SEL_EmpTypeDD
go
if object_id('dbo.RPT_tse_totals_tony') is not null
  drop procedure dbo.RPT_tse_totals_tony
go
if object_id('dbo.RPT_tse_totals_tony_wkly') is not null
  drop procedure dbo.RPT_tse_totals_tony_wkly
go

*/

/* Drop obsolete QM stored procedures */
if object_id('dbo.completion_report_data_3') is not null
  drop procedure dbo.completion_report_data_3
go
if object_id('dbo.completion_report_data_4') is not null
  drop procedure dbo.completion_report_data_4
go
if object_id('dbo.completion_report_data_5') is not null
  drop procedure dbo.completion_report_data_5
go
if object_id('dbo.get_old_tickets_for_locator2') is not null
  drop procedure dbo.get_old_tickets_for_locator2
go
if object_id('dbo.locator_load3') is not null
  drop procedure dbo.locator_load3
go
if object_id('dbo.locator_sync_7') is not null
  drop procedure dbo.locator_sync_7
go
if object_id('dbo.locator_sync_8') is not null
  drop procedure dbo.locator_sync_8
go
if object_id('dbo.queue_faxes') is not null
  drop procedure dbo.queue_faxes
go
if object_id('dbo.RPT_afterhours') is not null
  drop procedure dbo.RPT_afterhours
go
if object_id('dbo.RPT_highprofile2') is not null
  drop procedure dbo.RPT_highprofile2
go
if object_id('dbo.RPT_invoice2') is not null
  drop procedure dbo.RPT_invoice2
go
if object_id('dbo.RPT_invoice3') is not null
  drop procedure dbo.RPT_invoice3
go
if object_id('dbo.RPT_invoice4') is not null
  drop procedure dbo.RPT_invoice4
go
if object_id('dbo.RPT_LateTickets') is not null
  drop procedure dbo.RPT_LateTickets
go
if object_id('dbo.RPT_LateTicketsHier') is not null
  drop procedure dbo.RPT_LateTicketsHier
go
if object_id('dbo.RPT_locator_load') is not null
  drop procedure dbo.RPT_locator_load
go
if object_id('dbo.RPT_no_response') is not null
  drop procedure dbo.RPT_no_response
go
if object_id('dbo.RPT_productivity_5') is not null
  drop procedure dbo.RPT_productivity_5
go
if object_id('dbo.RPT_timesheetexception') is not null
  drop procedure dbo.RPT_timesheetexception
go
if object_id('dbo.RPT_tse_exception') is not null
  drop procedure dbo.RPT_tse_exception
go
if object_id('dbo.transmission_billing') is not null
  drop procedure dbo.transmission_billing
go
if object_id('dbo.transmission_billing_bulk') is not null
  drop procedure dbo.transmission_billing_bulk
go
if object_id('dbo.RPT_DamageLiabilityChanges3') is not null
  drop procedure dbo.RPT_DamageLiabilityChanges3
go
