--add new task_schedule.performed_date column; this column will be used by MyWork screen on the client to determine if a task has 
--been performed today or prior
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'task_schedule' and COLUMN_NAME='performed_date')
   alter table task_schedule add performed_date datetime null
go

