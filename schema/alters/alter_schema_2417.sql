-- Inserts configuration_data to control when to highlight the upload status bar for a backlog condition

-- Also deploy the updated sync_4.sql script associated with Mantis Issue #2417 so the new
-- settings get downloaded to the client.

if not exists (select name from configuration_data where name = 'AttachWarningMaxFiles')
  insert into configuration_data (name, value, editable) values ('AttachWarningMaxFiles', 100, 1)

if not exists (select name from configuration_data where name = 'AttachWarningMaxSizeKB')
  insert into configuration_data (name, value, editable) values ('AttachWarningMaxSizeKB', 10000, 1)

if not exists (select name from configuration_data where name = 'AttachWarningMaxWaitHrs')
  insert into configuration_data (name, value, editable) values ('AttachWarningMaxWaitHrs', 24, 1)
