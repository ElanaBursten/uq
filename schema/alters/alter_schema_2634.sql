--Add new right permitting a user to edit a Damage without Arriving first
if not exists (select * from right_definition where entity_data = 'DamagesEditWithoutArriving')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesEditWithoutArriving', 'Damages - Edit Without Arriving', 'General', 
    'Limitation does not apply to this right.')
go

--Add new right permitting a user to Arrive on a new damage or edit an existing damage (corresponds to the TicketsEditArrivals right)
if not exists (select * from right_definition where entity_data = 'DamagesEditArrivals')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('DamagesEditArrivals', 'Damages - Edit Arrivals', 'General', 
    'Limitation is the employee ID representing the part of the tree hierarchy the user edits arrivals for.')
go

--update the Tickets - Edit Arrivals right to have the correct modifier_desc
update right_definition set modifier_desc = 'Limitation is the employee ID representing the part of the tree hierarchy the user edits arrivals for.' where entity_data = 'TicketsEditArrivals'
go

-- add new column to the jobsite_arrival table that contains damage_id for damage arrivals
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'jobsite_arrival' and COLUMN_NAME = 'damage_id')
  alter table jobsite_arrival add damage_id int default NULL
go

-- add new column to the jobsite_arrival table to hold the location_status (ONSITE, OFFSITE, or NULL if not recorded yet)
if not exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'jobsite_arrival' and COLUMN_NAME = 'location_status')
  alter table jobsite_arrival add location_status varchar(15) default NULL
go

if exists (select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'jobsite_arrival' and COLUMN_NAME = 'ticket_id' and IS_NULLABLE = 'NO')
  ALTER TABLE jobsite_arrival ALTER COLUMN ticket_id int NULL
go

-- add index for damage_id searches
IF NOT EXISTS (select * from sys.indexes where name = 'jobsite_arrival_damage_id')
  create index jobsite_arrival_damage_id on jobsite_arrival(damage_id)
go