--106 (QQMANTWO-31)
--Allow Notes to be stored Private or Public (Internal and External) on the Ticket Detail Notes screen.


IF NOT EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'notes' and COLUMN_NAME='private')
    ALTER TABLE notes
      ADD private bit null; -- Determines if the note is private