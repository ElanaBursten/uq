--This query will return user and employee ids in various tables which are not found in the corresponding users or employee table.
--This should be run and the data cleaned before applying the foreign keys in the alter_schema_3074 query.

--users.uid
select t1.uid as 'users_client.uid' from users_client t1 left join users t2 on t1.uid = t2.uid where t2.uid is null and t1.uid is not null
select t1.emp_id as 'employee.emp_id', t1.create_uid from employee t1 left join users t2 on t1.create_uid = t2.uid where t2.uid is null and t1.create_uid is not null 
select t1.emp_id as 'employee.emp_id', t1.modified_uid from employee t1 left join users t2 on t1.modified_uid = t2.uid where t2.uid is null and t1.modified_uid is not null 
select t1.emp_id as 'employee_history.emp_id', t1.create_uid from employee_history t1 left join users t2 on t1.create_uid = t2.uid where t2.uid is null and t1.create_uid is not null
select t1.emp_id as 'employee_history.emp_id', t1.modified_uid from employee_history t1 left join users t2 on t1.modified_uid = t2.uid where t2.uid is null and t1.modified_uid is not null 

--employee.emp_id
select t1.assignment_id as 'asset_assignment_history.assignment_id', t1.emp_id from asset_assignment_history t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.computer_info_id as 'computer_info.computer_info_id', t1.emp_id from computer_info t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null 
select t1.invoice_id as 'damage_invoice.invoice_id', t1.satisfied_by_emp_id from damage_invoice t1 left join employee t2 on t1.satisfied_by_emp_id = t2.emp_id where t2.emp_id is null and t1.satisfied_by_emp_id is not null
select t1.emp_group_id as 'employeegroup.emp_group_id', t1.emp_id from employee_group t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null 
select t1.emp_id as 'employee_history.emp_id' from employee_history t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.error_report_id as 'error_report.error_report_id', t1.emp_id from error_report t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.emp_id as 'exported_employees.emp_id' from exported_employees t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.locate_hours_id as 'locate_hours.locate_hours_id', t1.emp_id from locate_hours t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.emp_id as 'notification_email_user.emp_id' from notification_email_user t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null 
select t1.pc_code as 'profit_center.pc_code', t1.manager_emp_id from profit_center t1 left join employee t2 on t1.manager_emp_id = t2.emp_id where t2.emp_id is null and t1.manager_emp_id is not null
select t1.sync_id as 'sync_log.sync_id', t1.emp_id from sync_log t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null
select t1.entry_id as 'timesheet_entry.entry_id', t1.work_emp_id from timesheet_entry t1 left join employee t2 on t1.work_emp_id = t2.emp_id where t2.emp_id is null and t1.work_emp_id is not null
select t1.work_goal_id as 'employee_work_goal.work_goal_id', t1.emp_id from employee_work_goal t1 left join employee t2 on t1.emp_id = t2.emp_id where t2.emp_id is null and t1.emp_id is not null