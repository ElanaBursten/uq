USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[RPT_employees_3]    Script Date: 1/30/2019 4:36:43 PM ******/
if object_id('RPT_employees_3') is not null  
drop procedure RPT_employees_3

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

ALTER PROCEDURE [dbo].[RPT_employees_3] (
  @ManagerID int,
  @EmployeeStatus int,
  @EmployeeTypeList Varchar(400)    --QMANTWO-706 
)
AS

if @EmployeeTypeList = '' 
begin
  select hier.*, users.login_id, r.code as h_type_id_code
  from dbo.get_report_hier3(@ManagerID, 0, 9, @EmployeeStatus) hier
       left join reference r on r.ref_id=h_type_id
       left outer join users on (users.emp_id = h_emp_id and users.active_ind = 1)
  select 'All' as code, 'All Employee Types' as description
end
else begin
  select hier.*, users.login_id, r.code as h_type_id_code
  from dbo.get_report_hier3(@ManagerID, 0, 9, @EmployeeStatus) hier
       left join reference r on r.ref_id=h_type_id
       left outer join users on (users.emp_id = h_emp_id and users.active_ind = 1)
       inner join dbo.StringListToTable(@EmployeeTypeList) l on hier.h_type_id = l.S
  select code, description 
  from reference 
  inner join dbo.StringListToTable(@EmployeeTypeList) on ref_id = S
end


GO


