/* New  hp_address table */
create table hp_address (
  hp_address_id int identity (8500,1) not null,
  call_center varchar(20) not null,
  client_code varchar(10) not null,
  street varchar(60) not null,
  city varchar(40) not null,
  county varchar(40) not null,
  state varchar(2) not null, 
  primary key  nonclustered (hp_address_id)
)
go
create index hp_address_index on hp_address(hp_address_id)
go

