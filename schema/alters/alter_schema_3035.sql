-- Drop these triggers on obsolete tables:
if exists(select name FROM sysobjects where name = 'timesheet_detail_u' AND type = 'TR')
  DROP TRIGGER timesheet_detail_u
GO
if exists(select name FROM sysobjects where name = 'timesheet_u' AND type = 'TR')
  DROP TRIGGER timesheet_u
GO
if exists(select name FROM sysobjects where name = 'vehicle_type_u' AND type = 'TR')
  DROP TRIGGER vehicle_type_u
GO
if exists(select name FROM sysobjects where name = 'vehicle_use_u' AND type = 'TR')
  DROP TRIGGER vehicle_use_u
GO


-- Add not for replication to all triggers
if exists(select name FROM sysobjects where name = 'right_definition_u' AND type = 'TR')
  DROP TRIGGER right_definition_u
GO
create trigger right_definition_u on right_definition
for update
not for replication
AS
  update right_definition
   set modified_date = getdate()
   where right_id in (select right_id from inserted)
Go

if exists(select name FROM sysobjects where name = 'employee_right_u' AND type = 'TR')
  DROP TRIGGER employee_right_u
GO
create trigger employee_right_u on employee_right
for update
not for replication
AS
update employee_right
 set modified_date = getdate()
 where emp_right_id in (select emp_right_id from inserted)
go

if exists(select name FROM sysobjects where name = 'employee_right_i' AND type = 'TR')
  DROP TRIGGER employee_right_i
GO
create trigger employee_right_i on employee_right
for insert
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from inserted)
Go

if exists(select name FROM sysobjects where name = 'employee_right_u_2' AND type = 'TR')
  DROP TRIGGER employee_right_u_2
GO
create trigger employee_right_u_2 on employee_right
for update
not for replication
as
  if update(allowed) or update(limitation) begin
    update employee
      set rights_modified_date = GetDate()
    where
      emp_id in (select emp_id from inserted)
  end
Go

if exists(select name FROM sysobjects where name = 'employee_right_d' AND type = 'TR')
  DROP TRIGGER employee_right_d
GO
create trigger employee_right_d on employee_right
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from deleted)
Go

if exists(select name FROM sysobjects where name = 'right_restriction_u' AND type = 'TR')
  DROP TRIGGER right_restriction_u
GO
create trigger right_restriction_u on right_restriction 
for update 
not for replication
as
  update right_restriction
   set modified_date = GetDate()
   where right_restriction_id in (select right_restriction_id from inserted)
go

if exists(select name FROM sysobjects where name = 'restricted_use_message_u' AND type = 'TR')
  DROP TRIGGER restricted_use_message_u
GO
create trigger restricted_use_message_u on restricted_use_message 
for update 
not for replication
as
  update restricted_use_message
   set modified_date = GetDate()
   where rum_id in (select rum_id from inserted)
go

if exists(select name FROM sysobjects where name = 'restricted_use_exemption_u' AND type = 'TR')
  DROP TRIGGER restricted_use_exemption_u
GO
create trigger restricted_use_exemption_u on restricted_use_exemption 
for update 
not for replication
as
  update restricted_use_exemption
   set modified_date = GetDate()
   where rue_id in (select rue_id from inserted)
go

if exists(select name FROM sysobjects where name = 'locate_facility_u' AND type = 'TR')
  DROP TRIGGER locate_facility_u
GO
create trigger locate_facility_u on locate_facility
for update
not for replication
as
  update locate_facility set modified_date = GetDate() where locate_facility_id in (select locate_facility_id from inserted)
go

if exists(select name FROM sysobjects where name = 'group_definition_d' AND type = 'TR')
  DROP TRIGGER group_definition_d 
GO
create trigger group_definition_d on group_definition
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from deleted))
    or 0 < (select count(*) from deleted where group_id in (select group_id from group_definition where is_default = 1))
GO

if exists(select name FROM sysobjects where name = 'group_definition_u' AND type = 'TR')
  DROP TRIGGER group_definition_u
GO
create trigger group_definition_u on group_definition
for update
not for replication
as
  set nocount on
  update group_definition
    set modified_date = getdate()
    where group_id in (select group_id from inserted)

  if update(is_default) begin
    update employee set rights_modified_date = GetDate()
  end
  else begin
    if update(active) or update(priority) begin
      update employee
        set rights_modified_date = GetDate()
      where
        emp_id in (select emp_id from
                   employee_group
                   where employee_group.group_id in (select group_id from inserted))
        or (0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1)))
    end
  end
GO

if exists(select name FROM sysobjects where name = 'employee_group_iu' AND type = 'TR')
  DROP TRIGGER employee_group_iu
GO
create trigger employee_group_iu on employee_group
for insert, update
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from inserted)
Go

if exists(select name FROM sysobjects where name = 'employee_group_d' AND type = 'TR')
  DROP TRIGGER employee_group_d
GO
create trigger employee_group_d on employee_group
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from deleted)
Go

if exists(select name FROM sysobjects where name = 'group_right_i' AND type = 'TR')
  DROP TRIGGER group_right_i
GO
create trigger group_right_i on group_right
for insert
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from inserted))
    or 0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1))
Go

if exists(select name FROM sysobjects where name = 'group_right_u' AND type = 'TR')
  DROP TRIGGER group_right_u
GO
create trigger group_right_u on group_right
for update
not for replication
as
  update group_right
    set modified_date = getdate()
    where group_right_id in (select group_right_id from inserted)

  if update(allowed) or update(limitation) begin
    update employee
      set rights_modified_date = GetDate()
    where
      emp_id in (select emp_id from
                 employee_group
                 where employee_group.group_id in (select group_id from inserted))
      or 0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1))
  end
Go

if exists(select name FROM sysobjects where name = 'group_right_d' AND type = 'TR')
  DROP TRIGGER group_right_d
GO
create trigger group_right_d on group_right
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from deleted))
    or 0 < (select count(*) from deleted where group_id in (select group_id from group_definition where is_default = 1))
Go

if exists(select name FROM sysobjects where name = 'document_u' AND type = 'TR')
  DROP TRIGGER document_u
GO
create trigger document_u on document
for update
not for replication
AS
begin
  update document set modified_date = getdate()
   where doc_id in (select doc_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'required_document_u' AND type = 'TR')
  DROP TRIGGER required_document_u
GO
create trigger required_document_u on required_document
for update
not for replication
as
begin
  update required_document set modified_date = getdate()
   where required_doc_id in (select required_doc_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'required_document_type_u' AND type = 'TR')
  DROP TRIGGER required_document_type_u
GO
create trigger required_document_type_u on required_document_type
for update
not for replication
AS
begin
  update required_document_type set modified_date = getdate()
   where rdt_id in (select rdt_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'locating_company_u' AND type = 'TR')
  DROP TRIGGER locating_company_u
GO
create trigger locating_company_u on locating_company
for update
not for replication
AS
update locating_company set modified_date = getdate() where company_id in (select company_id from inserted)
go

if exists(select name FROM sysobjects where name = 'billing_output_config_u' AND type = 'TR')
  DROP TRIGGER billing_output_config_u
GO
create trigger billing_output_config_u on billing_output_config
for update
not for replication
AS
update billing_output_config set modified_date = getdate() where output_config_id in (select output_config_id from inserted)
go

if exists(select name FROM sysobjects where name = 'billing_gl_u' AND type = 'TR')
  DROP TRIGGER billing_gl_u
GO
create trigger billing_gl_u on billing_gl
for update
not for replication
AS
update billing_gl set modified_date = getdate() where billing_gl_id in (select billing_gl_id from inserted)
go

if exists(select name FROM sysobjects where name = 'configuration_data_u' AND type = 'TR')
  DROP TRIGGER configuration_data_u
GO
create trigger configuration_data_u on configuration_data
for update
not for replication
AS
update configuration_data set modified_date = getdate() where name in (select name from inserted)
go

if exists(select name FROM sysobjects where name = 'billing_adjustment_u' AND type = 'TR')
  DROP TRIGGER billing_adjustment_u
GO
create trigger billing_adjustment_u on billing_adjustment
for update
not for replication
AS
update billing_adjustment set modified_date = getdate() where adjustment_id in (select adjustment_id from inserted)
go

if exists(select name FROM sysobjects where name = 'profit_center_u' AND type = 'TR')
  DROP TRIGGER profit_center_u
GO
create trigger profit_center_u on profit_center
for update
not for replication
AS
update profit_center set modified_date = getdate() where pc_code in (select pc_code from inserted)
go

if exists(select name FROM sysobjects where name = 'office_u' AND type = 'TR')
  DROP TRIGGER office_u
GO
create trigger office_u on office
for update
not for replication
AS
update office set modified_date = getdate() where office_id in (select office_id from inserted)
go

if exists(select name FROM sysobjects where name = 'statuslist_u' AND type = 'TR')
  DROP TRIGGER statuslist_u
GO
create trigger statuslist_u on statuslist
for update
not for replication
AS
update statuslist set modified_date = getdate() where status in (select status from inserted)
go

if exists(select name FROM sysobjects where name = 'client_u' AND type = 'TR')
  DROP TRIGGER client_u
GO
create trigger client_u on client
for update
not for replication
AS
if SYSTEM_USER = 'no_trigger'
  return
update client set modified_date = getdate() where client_id in (select client_id from inserted)
go

if exists(select name FROM sysobjects where name = 'ticket_u' AND type = 'TR')
  DROP TRIGGER ticket_u
GO
create trigger ticket_u on ticket
for update
not for replication
AS
update ticket set modified_date = getdate() where ticket_id in (select ticket_id from inserted)
go

if exists(select name FROM sysobjects where name = 'followup_ticket_type_u' AND type = 'TR')
  DROP TRIGGER followup_ticket_type_u
GO
create trigger followup_ticket_type_u on followup_ticket_type
for update
not for replication
AS
update followup_ticket_type set modified_date = getdate() where type_id in (select type_id from inserted)
go

if exists(select name FROM sysobjects where name = 'assignment_u' AND type = 'TR')
  DROP TRIGGER assignment_u
GO
create trigger assignment_u on assignment
for update
not for replication
AS
update assignment set modified_date = getdate() where assignment_id in (select assignment_id from inserted)
go

if exists(select name FROM sysobjects where name = 'employee_iu' AND type = 'TR')
  DROP TRIGGER employee_iu
GO
create trigger employee_iu on employee
for insert, update
not for replication
as
begin
  set nocount on
  
  declare @ChangeDate datetime
  set @ChangeDate = getdate()

  -- Don't update modified date or add history for rights_midified_date changes alone
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(ultra_user) or update(dialup_user)
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)))
  begin
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)

    if update(report_to) or update(repr_pc_code) 
    begin -- This change may modify the effective PC of non-updated employees
      -- Recalculate and store any changes to everyone below these employees

      declare @AllEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY,
        x integer,
        emp_pc_code varchar(15) NULL
      )

      insert into @AllEmps select * from employee_pc_data(null)

      declare @ChangedEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY
      )

      insert into @ChangedEmps
        select emps.emp_id
        from @AllEmps emps
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate
        where ((emps.emp_pc_code <> eh.active_pc)
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))
          or emps.emp_id in (select emp_id from inserted)

      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)

      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets
      from employee e
        inner join @AllEmps emps on emps.emp_id = e.emp_id
      where e.emp_id in (select emp_id from @ChangedEmps)
    end
    else -- This change only affects the exact employee(s) being modified
    begin
      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)

      -- Record the new/current employee data
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets
      )
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, ultra_user, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets
      from inserted
    end
  end
end
go

if exists(select name FROM sysobjects where name = 'employee_no_delete' AND type = 'TR')
  DROP TRIGGER employee_no_delete
GO
create trigger employee_no_delete on employee
instead of delete
not for replication
as
begin
  raiserror('Deleting employees is not allowed (due to an "instead of delete" trigger)', 16, 1)
end
go

if exists(select name FROM sysobjects where name = 'carrier_u' AND type = 'TR')
  DROP TRIGGER carrier_u
GO
create trigger carrier_u on carrier
for update
not for replication
AS
update carrier set modified_date = getdate() where carrier_id in (select distinct carrier_id from inserted)
go

if exists(select name FROM sysobjects where name = 'reference_u' AND type = 'TR')
  DROP TRIGGER reference_u
GO
create trigger reference_u on reference
for update
not for replication
AS
update reference set modified_date = getdate() where ref_id in (select distinct ref_id from inserted)
go

if exists(select name FROM sysobjects where name = 'asset_type_u' AND type = 'TR')
  DROP TRIGGER asset_type_u
GO
create trigger asset_type_u on asset_type
for update
not for replication
AS
update asset_type set modified_date = getdate() where asset_code in (select asset_code from inserted)
go

if exists(select name FROM sysobjects where name = 'attachment_u' AND type = 'TR')
  DROP TRIGGER attachment_u
GO
create trigger attachment_u on attachment
for update
not for replication
AS
update attachment set modified_date = getdate() where attachment_id in (select attachment_id from inserted)
go

if exists(select name FROM sysobjects where name = 'upload_location_u' AND type = 'TR')
  DROP TRIGGER upload_location_u
GO
create trigger upload_location_u on upload_location
for update
not for replication
AS
update upload_location set modified_date = getdate() where location_id in (select location_id from inserted)
go

if exists(select name FROM sysobjects where name = 'upload_file_type_u' AND type = 'TR')
  DROP TRIGGER upload_file_type_u
GO
create trigger upload_file_type_u on upload_file_type
for update
not for replication
AS
update upload_file_type set modified_date = getdate() where upload_file_type_id in (select upload_file_type_id from inserted)
go

if exists(select name FROM sysobjects where name = 'damage_default_est_u' AND type = 'TR')
  DROP TRIGGER damage_default_est_u
GO
create trigger damage_default_est_u on damage_default_est
for update
not for replication
AS
update damage_default_est set modified_date = getdate() where ddest_id in (select ddest_id from inserted)
go

if exists(select name FROM sysobjects where name = 'users_iu' AND type = 'TR')
  DROP TRIGGER users_iu
GO
create trigger users_iu on users
for insert, update
not for replication
AS
  declare @n int
  select @n =
   (select count(*)
     from users
      inner join inserted on users.login_id = inserted.login_id
     where users.uid <> inserted.uid)

  if @n > 0
  begin
    RAISERROR ('Cannot create duplicate user login_id', 16, 1)
    ROLLBACK TRANSACTION
  end
go

if exists(select name FROM sysobjects where name = 'users_u' AND type = 'TR')
  DROP TRIGGER users_u
GO
create trigger users_u on users
for update
not for replication
AS
update users set modified_date = getdate() where uid in (select uid from inserted)
go

if exists(select name FROM sysobjects where name = 'customer_u' AND type = 'TR')
  DROP TRIGGER customer_u
GO
create trigger customer_u on customer
for update
not for replication
AS
update customer set modified_date = getdate() where customer_id in (select customer_id from inserted)
go

if exists(select name FROM sysobjects where name = 'center_group_u' AND type = 'TR')
  DROP TRIGGER center_group_u
GO
create trigger center_group_u on center_group
for update
not for replication
AS
update center_group set modified_date = getdate() where center_group_id in (select center_group_id from inserted)
go

if exists(select name FROM sysobjects where name = 'center_group_detail_u' AND type = 'TR')
  DROP TRIGGER center_group_detail_u
GO
create trigger center_group_detail_u on center_group_detail
for update
not for replication
AS
update center_group_detail set modified_date = getdate() where center_id in (select center_id from inserted)
go

if exists(select name FROM sysobjects where name = 'term_group_u' AND type = 'TR')
  DROP TRIGGER term_group_u
GO
create trigger term_group_u on term_group
for update
not for replication
AS
update term_group set modified_date = getdate() where term_group_id in (select term_group_id from inserted)
go

if exists(select name FROM sysobjects where name = 'term_group_detail_u' AND type = 'TR')
  DROP TRIGGER term_group_detail_u
GO
create trigger term_group_detail_u on term_group_detail
for update
not for replication
AS
update term_group_detail set modified_date = getdate() where term_id in (select term_id from inserted)
go

if exists(select name FROM sysobjects where name = 'status_group_u' AND type = 'TR')
  DROP TRIGGER status_group_u
GO
create trigger status_group_u on status_group
for update
not for replication
AS
update status_group set modified_date = getdate() where sg_id in (select sg_id from inserted)
go

if exists(select name FROM sysobjects where name = 'status_group_item_u' AND type = 'TR')
  DROP TRIGGER status_group_item_u
GO
create trigger status_group_item_u on status_group_item
for update
not for replication
AS
update status_group_item set modified_date = getdate() where sg_item_id in (select sg_item_id from inserted)
go

if exists(select name FROM sysobjects where name = 'call_center_hp_u' AND type = 'TR')
  DROP TRIGGER call_center_hp_u
GO
create trigger call_center_hp_u on call_center_hp
for update
not for replication
AS
update call_center_hp set modified_date = getdate() where call_center_hp_id in (select call_center_hp_id from inserted)
go

if exists(select name FROM sysobjects where name = 'break_rules_u' AND type = 'TR')
  DROP TRIGGER break_rules_u
GO
create trigger break_rules_u on break_rules
for update
not for replication
as
begin
  update break_rules set modified_date = getdate()
   where rule_id in (select rule_id from inserted)
end
go

if exists(select name FROM sysobjects where name = 'call_center_u' AND type = 'TR')
  DROP TRIGGER call_center_u
GO
create trigger call_center_u on call_center
for update
not for replication
AS
update call_center set modified_date = getdate() where cc_code in (select cc_code from inserted)
go

if exists(select name FROM sysobjects where name = 'jobsite_arrival_u' AND type = 'TR')
  DROP TRIGGER jobsite_arrival_u
GO
create trigger jobsite_arrival_u on jobsite_arrival
for update
not for replication
as
update jobsite_arrival set modified_date = GetDate() where arrival_id in (select arrival_id from inserted)
go

if exists(select name FROM sysobjects where name = 'highlight_rule_u' AND type = 'TR')
  DROP TRIGGER highlight_rule_u
GO
create trigger highlight_rule_u on highlight_rule
for update
not for replication
as
update highlight_rule set modified_date = getdate() where highlight_rule_id in (select highlight_rule_id from inserted)
go

if exists(select name FROM sysobjects where name = 'area_u' AND type = 'TR')
  DROP TRIGGER area_u
GO

create trigger area_u on area
for update
not for replication
AS
begin
  update area set modified_date = getdate()
   where area_id in (select area_id from inserted)
end

GO

-- Log all asset changes to asset_history

if exists(select name FROM sysobjects where name = 'asset_iud' AND type = 'TR')
  DROP TRIGGER asset_iud
GO

CREATE TRIGGER asset_iud ON asset
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @NumInserted int
  declare @NumDeleted int
  declare @ChangeTime datetime
  set @NumInserted = (select count(*) from inserted)
  set @NumDeleted = (select count(*) from deleted)
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
  if (@NumDeleted > 0)
  begin
    insert into asset_history
     (asset_id, asset_code, asset_number, active, modified_date, comment, condition, archive_date, solomon_profit_center, serial_num, description, cost)
    select
      asset_id, asset_code, asset_number, active, modified_date, comment, condition, @ChangeTime,  solomon_profit_center, serial_num, description, cost
    from deleted
  end

  -- Insert/update operation: update modified_date for the changed rows
  if (@NumInserted > 0)
  begin
    update asset
    set modified_date = @ChangeTime
    where asset_id in (select asset_id from inserted)
  end
END
go

-- Log all asset_assignment changes to asset_assignment_history

if exists(select name FROM sysobjects where name = 'asset_assignment_iud' AND type = 'TR')
  DROP TRIGGER asset_assignment_iud
GO

CREATE TRIGGER asset_assignment_iud ON asset_assignment
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @NumInserted int
  declare @NumDeleted int
  declare @ChangeTime datetime
  set @NumInserted = (select count(*) from inserted)
  set @NumDeleted = (select count(*) from deleted)
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
  if (@NumDeleted > 0)
  begin
    insert into asset_assignment_history
     (assignment_id, asset_id, emp_id, active, modified_date, archive_date)
    select
      assignment_id, asset_id, emp_id, active, modified_date, @ChangeTime
    from deleted
  end

  -- Insert/update operation: update modified_date for the changed rows
  if (@NumInserted > 0)
  begin
    update asset_assignment
    set modified_date = @ChangeTime
    where assignment_id in (select assignment_id from inserted)
  end
END
go

if object_id('dbo.assignment_i') is not null
  drop trigger dbo.assignment_i
GO

CREATE TRIGGER assignment_i ON assignment
FOR INSERT
not for replication
AS
BEGIN
  SET NOCOUNT ON

  UPDATE locate SET
    assigned_to =
       case when locate.closed = 1 then null
            else inserted.locator_id end,
    assigned_to_id = inserted.locator_id
  FROM inserted
  WHERE
    locate.locate_id = inserted.locate_id

  update ticket
    set recv_manager_id =
      (select max(e.report_to)
         from employee e where e.emp_id=locate.assigned_to)
    from locate
    where locate.locate_id in (select locate_id from inserted)
     and ticket.ticket_id = locate.ticket_id
     and ticket.recv_manager_id is null
  
  update ticket_snap
    set first_receive_pc = dbo.get_historical_pc(locate.assigned_to_id, first_receive_date)
    from locate 
    where locate.locate_id in (select locate_id from inserted)
      and ticket_snap.ticket_id = locate.ticket_id
      and ticket_snap.first_receive_pc is null
  
  insert into locate_snap (locate_id, first_receive_date, first_receive_pc)
    select inserted.locate_id, GetDate(), dbo.get_historical_pc(inserted.locator_id, GetDate())
    from inserted left join locate_snap on inserted.locate_id = locate_snap.locate_id
    where locate_snap.locate_id is null 
END
GO

-- Log all billing rate changes to the billing_rate_history table

if exists(select name FROM sysobjects where name = 'billing_rate_iud' AND type = 'TR')
  DROP TRIGGER billing_rate_iud
GO

CREATE TRIGGER billing_rate_iud ON billing_rate
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @ChangeTime datetime
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
    insert into billing_rate_history
     (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, value_group_id, additional_line_item_text)
    select
      br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, @ChangeTime, which_invoice, additional_rate, value_group_id, additional_line_item_text
    from deleted

  -- Upon insert/update update the modified_date for the changed rows
    update billing_rate
    set modified_date = @ChangeTime
    where br_id in (select br_id from inserted)
END
go

if object_id('dbo.damage_i') is not null
  drop trigger dbo.damage_i
GO

create trigger dbo.damage_i on damage
for insert
not for replication
as
  set nocount on
  if (select Coalesce(uq_damage_id, 0) from inserted) < 1
  begin
    declare @next_damage_id integer
    execute @next_damage_id = dbo.NextUniqueID 'NextDamageID'
    update damage set uq_damage_id = @next_damage_id, added_by = modified_by
     where (damage_id in (select damage_id from inserted))
  end
  else
    update damage set added_by = modified_by
     where damage_id in (select damage_id from inserted)
go

if object_id('dbo.damage_iu') is not null
  drop trigger dbo.damage_iu
GO

create trigger dbo.damage_iu on damage
for insert, update
not for replication
as
  set nocount on
  if SYSTEM_USER = 'no_trigger'
    return

  declare @damages table (damage_id int not null)

  /* Find damages that have changed in an important way */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.exc_resp_code <> ins.exc_resp_code) or
      (del.exc_resp_code is null and ins.exc_resp_code is not null) or
      (del.exc_resp_code is not null and ins.exc_resp_code is null) or
    (del.exc_resp_type <> ins.exc_resp_type) or
      (del.exc_resp_type is null and ins.exc_resp_type is not null) or
      (del.exc_resp_type is not null and ins.exc_resp_type is null) or
    (del.uq_resp_code <> ins.uq_resp_code) or
      (del.uq_resp_code is null and ins.uq_resp_code is not null) or
      (del.uq_resp_code is not null and ins.uq_resp_code is null) or
    (del.uq_resp_type <> ins.uq_resp_type) or
      (del.uq_resp_type is null and ins.uq_resp_type is not null) or
      (del.uq_resp_type is not null and ins.uq_resp_type is null) or
    (del.spc_resp_code <> ins.spc_resp_code) or
      (del.spc_resp_code is null and ins.spc_resp_code is not null) or
      (del.spc_resp_code is not null and ins.spc_resp_code is null) or
    (del.spc_resp_type <> ins.spc_resp_type) or
      (del.spc_resp_type is null and ins.spc_resp_type is not null) or
      (del.spc_resp_type is not null and ins.spc_resp_type is null) or
    (del.profit_center <> ins.profit_center) or
      (del.profit_center is null and ins.profit_center is not null) or
      (del.profit_center is not null and ins.profit_center is null) or
    (del.reason_changed <> ins.reason_changed) or
      (del.reason_changed is null and ins.reason_changed is not null) or
      (del.reason_changed is not null and ins.reason_changed is null)

  insert into damage_history
   (damage_id,
    modified_date, modified_by,
    exc_resp_code, exc_resp_type,
    uq_resp_code, uq_resp_type,
    spc_resp_code, spc_resp_type,
    profit_center, exc_resp_other_desc,
    exc_resp_details, exc_resp_response,
    uq_resp_other_desc, uq_resp_details,
    uq_resp_ess_step, spc_resp_details, reason_changed
   )
  select d.damage_id,
    getdate(), d.modified_by,
    d.exc_resp_code, d.exc_resp_type,
    d.uq_resp_code, d.uq_resp_type,
    d.spc_resp_code, d.spc_resp_type,
    d.profit_center, d.exc_resp_other_desc,
    damage.exc_resp_details, damage.exc_resp_response,
    d.uq_resp_other_desc, damage.uq_resp_details,
    d.uq_resp_ess_step, damage.spc_resp_details, ins.reason_changed
  from deleted d
       join damage on damage.damage_id = d.damage_id
       join inserted ins on d.damage_id = ins.damage_id
    where d.damage_id in (select damage_id from @damages)

  update damage set modified_date = getdate()
    where damage_id in (select damage_id from inserted)

  update damage set invoice_code_initial_value = invoice_code
    where damage_id in (select damage_id from inserted)
      and invoice_code_initial_value is null
      and invoice_code is not null

  delete from @damages

  /* Find damages with changed damage dates or ticket IDs */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.damage_date <> ins.damage_date) or
      (del.damage_date is null and ins.damage_date is not null) or
      (del.damage_date is not null and ins.damage_date is null) or
    (del.ticket_id <> ins.ticket_id) or
      (del.ticket_id is null and ins.ticket_id is not null) or
      (del.ticket_id is not null and ins.ticket_id is null)

  /* Also include any damages being inserted, since the deleted table is empty in that case */
  insert into @damages select damage_id from inserted where damage_id not in (select damage_id from deleted)

  update damage
  set accrual_date =
  dbo.MinOfFourDates(
    damage_date,
    uq_notified_date,
    coalesce((select t.due_date from ticket t where t.ticket_id = damage.ticket_id), '2100-01-01'),
    coalesce((select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N'), '2100-01-01')
  )
  where damage_id in (select damage_id from @damages)
go

if object_id('dbo.damage_litigation_i') is not null
  drop trigger dbo.damage_litigation_i
go

create trigger dbo.damage_litigation_i on damage_litigation
for insert
not for replication
as  
  update damage_litigation set added_by = modified_by
    where litigation_id in (select litigation_id from inserted)
go

if object_id('dbo.damage_litigation_iu') is not null
  drop trigger dbo.damage_litigation_iu
GO

create trigger dbo.damage_litigation_iu on damage_litigation
for insert, update
not for replication
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_litigation set modified_date = getdate()
    where litigation_id in (select litigation_id from inserted)
go

if object_id('dbo.damage_third_party_i') is not null
  drop trigger dbo.damage_third_party_i
go

create trigger dbo.damage_third_party_i on damage_third_party
for insert
not for replication
as  
  update damage_third_party set added_by = modified_by
    where third_party_id in (select third_party_id from inserted)
go

if object_id('dbo.damage_third_party_iu') is not null
  drop trigger dbo.damage_third_party_iu
GO

create trigger dbo.damage_third_party_iu on damage_third_party
for insert, update
not for replication
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_third_party set modified_date = getdate()
    where third_party_id in (select third_party_id from inserted)
go

if exists(select name FROM sysobjects where name = 'locate_iu' AND type = 'TR')
  DROP TRIGGER locate_iu
GO

CREATE TRIGGER locate_iu ON locate
FOR INSERT, UPDATE
not for replication
AS
BEGIN
/* ********************** WARNING!!!! ******************************
   This is a very fragile part of the system, do not do anything with
   this trigger in the production DB without coordinating with the
   team, and having all of the front-end machines shut down to
   prevent any changes from slipping in while changing the trigger.
*/

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- exit if only the invoiced column was changed
  declare @invoiced_col int
  set @invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'invoiced')

  -- NOTE: "@invoiced_col % 8" would not work if the column number is 16, as it is
  -- for assigned_to, so it was changed to "- 8"
  if (@invoiced_col < 9) or (@invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- exit if the assigned_to column was changed
  -- and none of the initial fields changed
  declare @assignedto_col int
  set @assignedto_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to')

  if (@assignedto_col < 9) or (@assignedto_col > 16)
  begin
    rollback transaction
    RAISERROR('assigned_to has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@assignedto_col - 8)-1)))
    return

  -- exit if the assigned_to_id column was changed
  -- and none of the initial fields changed
  declare @assignedtoid_col int
  set @assignedtoid_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to_id')

  if (@assignedtoid_col < 17) or (@assignedto_col > 24)
  begin
    rollback transaction
    RAISERROR('assigned_to_id has moved out of the third column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 3, 1) = power(2, ((@assignedtoid_col - 16)-1)))
    return

  -- if both assigned_to and assigned_to_id changed, either of the
  -- above will return

  -- set the ticket's kind
  DECLARE @ticket_id int
  SELECT @ticket_id = ticket_id FROM inserted

  EXEC dbo.update_ticket_status @ticket_id

  -- Update the modified_date when a locate changes
  update locate set
    modified_date = getdate()
  where locate_id in (select locate_id from inserted)

  -- When a locate is re-opened, refresh the assigned_to field
  update locate
    set assigned_to = (
      select top 1 assignment.locator_id
      from assignment
      where
        (assignment.locate_id = locate.locate_id) and
        (assignment.active = 1)
    )
  from deleted -- contains rows that were just updated, but before the updates were applied
    inner join inserted
      on inserted.locate_id = deleted.locate_id
  where locate.locate_id = deleted.locate_id
    and deleted.closed = 1
    and inserted.closed = 0

  -- Set assigned_to = NULL for all closed/non-active locates
  update locate
    set assigned_to = NULL
    where locate_id in (
      select locate_id
      from inserted
      where closed = 1
       or status = '-P'
       or status = '-N'
       or active = 0
    )

  -- Add response for a changed status not already queued
  insert into responder_queue (locate_id, ticket_format, client_code)
  select distinct i.locate_id, t.ticket_format, i.client_code
    from inserted i
      inner join ticket t on i.ticket_id=t.ticket_id
      left outer join responder_queue rq on i.locate_id=rq.locate_id
    where i.status <> '-N'
     and i.status <> '-R'
     and i.status <> '-P'
     and rq.locate_id is null 
     and (i.status <> (select top 1 ls.status from locate_status ls where ls.locate_id = i.locate_id order by insert_date desc)
      or i.status is not null and not exists (select ls.locate_id from locate_status ls where ls.locate_id = i.locate_id))

  -- 1849: update locate_snap fields
  -- Locates that are closed when they are added won't have a locate_snap.
  -- Add those here so the following update locate_snap will find them.
  insert into locate_snap (locate_id, first_receive_date, first_receive_pc)
    select i.locate_id, GetDate(), dbo.get_historical_pc(i.closed_by_id, GetDate())
      from inserted i 
        left join assignment a on i.locate_id = a.locate_id
        left join locate_snap ls on i.locate_id = ls.locate_id
      where a.locate_id is null and ls.locate_id is null 
        and i.closed = 1 and i.active = 1

  update locate_snap
    set first_close_date = i.closed_date, 
    first_close_emp_id = i.closed_by_id,  
    first_close_pc = dbo.get_historical_pc(i.closed_by_id, i.closed_date)
  from inserted i 
  where i.closed = 1 and i.active = 1 and i.locate_id = locate_snap.locate_id
    and first_close_date is null
  -- end 1849
  
  update locate_snap --update work_status and location_status every time locate is closed (not just first time closed)
    set work_status = dbo.get_locate_work_status(i.status),
    location_status = dbo.get_locate_location_status(i.locate_id, i.ticket_id, i.closed_by_id)
  from inserted i 
  where i.closed = 1 and i.active = 1 and i.locate_id = locate_snap.locate_id    
  
  -- Do this insert last because StatusTicketsLocatesInternal needs this identity    
  insert into locate_status (
    locate_id, status_date, status, high_profile, high_profile_reason,
    qty_marked, statused_by, statused_how, regular_hours, overtime_hours, 
    mark_type, entry_date, gps_id, status_changed_by_id)
  select
    locate_id, closed_date, status, high_profile, high_profile_reason,
    qty_marked, closed_by_id, closed_how, regular_hours, overtime_hours, 
    mark_type, entry_date, gps_id, status_changed_by_id
  from inserted
  where status <> '-N'
   and status <> '-P'
END
go


if exists(select name FROM sysobjects where name = 'locate_i' AND type = 'TR')
  DROP TRIGGER locate_i
GO

CREATE TRIGGER locate_i ON locate
FOR INSERT
not for replication
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  
  if (exists(select count(*) from locate l
    where l.ticket_id in (select ticket_id from inserted)
    group by l.ticket_id, client_code
    having count(*) > 1))
  begin
    rollback transaction
    RAISERROR('A duplicate client can not be added to a locate.  Please sync again in a few minutes.', 18, 1) with log
    return
  end

END
go

if exists(select name FROM sysobjects where name = 'locate_hours_u' AND type = 'TR')
  DROP TRIGGER locate_hours_u
GO

CREATE TRIGGER locate_hours_u ON locate_hours
FOR UPDATE
not for replication
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- See what columns were in the update statement
  declare @hours_invoiced_col int
  declare @units_invoiced_col int

  set @hours_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'hours_invoiced')
  set @units_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'units_invoiced')

  if (@hours_invoiced_col < 9) or (@hours_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('hours_invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (@units_invoiced_col < 9) or (@units_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('units_invoiced has moved out of the second column byte', 18, 1) with log
  end

  -- Exit if only one of the invoiced columns was in the update statement (see commit_billing)
  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@hours_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@units_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- Update the modified_date when a locate_hours row changes
  update locate_hours set modified_date = getdate()
  where locate_hours_id in (select locate_hours_id from inserted)
END
go

/* Create ticket_snap row & set first_recveive_date */
if object_id('dbo.ticket_i') is not null
  drop trigger dbo.ticket_i
go

create trigger ticket_i on ticket
for insert 
not for replication
as begin
  set nocount on

  insert into ticket_snap (ticket_id, first_receive_date)
    select inserted.ticket_id, GetDate()
    from inserted left join ticket_snap on inserted.ticket_id = ticket_snap.ticket_id
    where ticket_snap.ticket_id is null
end
go

if object_id('dbo.timesheet_entry_u') is not null -- this is intentional to merge this into the new iu trigger
  drop trigger dbo.timesheet_entry_u 
go
if object_id('dbo.timesheet_entry_iu') is not null
  drop trigger dbo.timesheet_entry_iu
go

create trigger timesheet_entry_iu on timesheet_entry
for insert, update
not for replication
as
begin
  set nocount on

  update timesheet_entry set modified_date = GetDate()
    where entry_id in (select entry_id from inserted)

  update timesheet_entry 
    set work_pc_code = dbo.get_historical_pc(work_emp_id, work_date)
  where entry_id in (select entry_id from inserted)
    and work_pc_code is null 
    and (status='ACTIVE' or status='SUBMIT') 
end
go

if exists(select name FROM sysobjects where name = 'work_order_iu' AND type = 'TR')
  DROP TRIGGER work_order_iu
GO

CREATE TRIGGER work_order_iu ON work_order
FOR INSERT, UPDATE
not for replication
AS
BEGIN

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- Update the modified_date when a wo changes
  update work_order set
    modified_date = GetDate()
  where wo_id in (select wo_id from inserted)

END
go

if exists(select name FROM sysobjects where name = 'work_order_ticket_iu' AND type = 'TR')
  DROP TRIGGER work_order_ticket_iu
GO

CREATE TRIGGER work_order_ticket_iu ON work_order_ticket
FOR INSERT, UPDATE
not for replication
AS
BEGIN

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- Update the modified_date when a wo ticket changes
  update work_order_ticket set
    modified_date = GetDate()
  from inserted I 
  where work_order_ticket.wo_id = I.wo_id 
    and work_order_ticket.ticket_id = I.ticket_id

END
go

