--New Table for OHM Work Order. This is a 1 -> 1 relationship with work_order, but is stored separately
--drop table work_order_OHM_details
CREATE TABLE work_order_OHM_details(
	wo_dtl_id int IDENTITY(1,1) NOT NULL,
	wo_id int NOT NULL,
	wo_number varchar(20) NULL,
	OSID varchar(50) NULL,
	due_date datetime NULL,
	curb_b varchar(100) NULL,
	meter_1 varchar(50) NULL,
	slpi_p varchar(50) NULL,
	slp_1 varchar(50) NULL,
	slp_2 varchar(50) NULL,
	slin_s datetime NULL,
	sl_pi varchar(50) NULL,
	slp_3 varchar(50) NULL,
	slpre varchar(50) NULL,
	main_p varchar(50) NULL,
	mai_1 varchar(50) NULL,
	main_r varchar(50) NULL,
	meter_2 varchar(50) NULL,
	has_c bit NULL,
    nominaldia varchar(50) NULL, 
	coatingtyp varchar(50) NULL, 
	installedd  varchar(50) NULL,  
	measuredle varchar(50) NULL, 
	material varchar(50) NULL,
    curbvaluefound bit NULL,
    modified_date datetime NULL
)

GO



ALTER TABLE work_order_OHM_details  WITH CHECK ADD  CONSTRAINT FK_work_order_OHM_details_work_order FOREIGN KEY(wo_id)
REFERENCES work_order (wo_id)
GO

ALTER TABLE work_order_OHM_details CHECK CONSTRAINT FK_work_order_OHM_details_work_order
GO


--------------------------------------------------------------------------------------------
--TRIGGER for work_order_OHM_details
if object_id('dbo.work_order_OHM_details_iu') is not null
  drop trigger dbo.work_order_OHM_details_iu
go

create trigger work_order_OHM_details_iu on work_order_OHM_details
for insert, update 
not for replication
AS
BEGIN
  declare @wo_id int; 
  declare @compliance_due_date datetime;
  declare @OSID varchar(50); 
  declare @wo_number varchar(20);  --  account number
  declare @opt char; 


  update work_order set
    modified_date = GetDate()
  where wo_id in (select wo_id from inserted) 

  If Exists(select * from DELETED)
      select @opt='I'  -- now insert into work_order_inspection
	  else
	  select @opt='U'


  if @opt='I' begin
  select @wo_id=wo_id, @wo_number=wo_number, @compliance_due_date=due_date, @OSID=OSID from inserted
  end

  IF EXISTS (select 1 from [dbo].[work_order_inspection] where [wo_id] = @wo_id)
  select @opt='U'

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  if @opt='I'
  begin
	INSERT INTO [dbo].[work_order_inspection]
			   ([wo_id]
                ,[account_number]
			    ,[premise_id]
				,[compliance_due_date]
				,[invoiced]
				)
		 VALUES
			   (@wo_id
			   ,@wo_number
			   ,@OSID
			   ,@compliance_due_date
			   ,1
			   )  
	end		     
END
GO
---------------------------------------------------------------
--TRIGGER for work_order_inspection
if object_id('dbo.work_order_inspection_iu') is not null
  drop trigger dbo.work_order_inspection_iu
go


create trigger [dbo].[work_order_inspection_iu] on [dbo].[work_order_inspection]
for insert, update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_inspection
  set modified_date = GetDate()
  from inserted I
  where work_order_inspection.wo_id = I.wo_id
end

GO

--------------------------------------------------------------------
--TRIGGER for work_order
if object_id('dbo.work_order_iu') is not null
  drop trigger dbo.work_order_iu
go

create trigger dbo.work_order_iu ON dbo.work_order
FOR INSERT, UPDATE
AS
BEGIN
  declare @wo_id int; 
  declare @due_date datetime;
  declare @wo_source varchar(20);
  declare @wo_number varchar(20);
  declare @opt char; 

 If Exists(select * from DELETED)
      select @opt='U'
	  else
	  select @opt='I'

  if @opt='I'
  select @wo_id=wo_id, @wo_source=wo_source, @wo_number=wo_number, @due_date=due_date from inserted

  --
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- Update the modified_date when a wo changes
  update work_order set
    modified_date = GetDate()
  where wo_id in (select wo_id from inserted) 

  if @wo_source='OHM' and @opt='I'
  begin
	INSERT INTO dbo.work_order_OHM_details
			   (wo_id
			    ,wo_number
				,due_date
				,modified_date)
		 VALUES
			   (@wo_id
			   ,@wo_number
			   ,@due_date
			   ,GetDate())    
  end
END
GO



