/* New Right for QMAN-3377 */
if not exists (select * from right_definition where entity_data = 'AttachmentsAddToClosedWorkOrder')
  insert into right_definition (entity_data, right_description, right_type, modifier_desc)
    values ('AttachmentsAddToClosedWorkOrder', 'Attachments - Add to Closed Work Order', 'General', 
    'Limitation does not apply to this right.')
go

