--Drop deprecated stored procedure
if object_id('dbo.set_wgl_work_order_status') is not null
  drop procedure dbo.set_wgl_work_order_status
GO

-- Increase size of these work order columns to 15 to match reference table.
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='actual_meter_location')
  ALTER TABLE work_order_inspection ALTER COLUMN actual_meter_location varchar(15)
go  
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='building_type')
  ALTER TABLE work_order_inspection ALTER COLUMN building_type varchar(15)
go  
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='map_status')
  ALTER TABLE work_order_inspection ALTER COLUMN map_status varchar(15)
go
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='mercury_regulator')
  ALTER TABLE work_order_inspection ALTER COLUMN mercury_regulator varchar(15)
go
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='vent_clearance_dist')
  ALTER TABLE work_order_inspection ALTER COLUMN vent_clearance_dist varchar(15)
go
IF EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='vent_ignition_dist')
  ALTER TABLE work_order_inspection ALTER COLUMN vent_ignition_dist varchar(15)
go

--Add list of CGA Reasons
if exists (select * from reference where type = 'cgareason')
  delete from reference where type = 'cgareason'
INSERT INTO reference (type, code, description, sortby) values ('cgareason', 'ANIMALINYARD', 'Animal in yard', 1)
INSERT INTO reference (type, code, description, sortby) values ('cgareason', 'LOCKEDGATE', 'Locked Gate', 2)
INSERT INTO reference (type, code, description, sortby) values ('cgareason', 'NOGATE', 'No Gate', 3)
INSERT INTO reference (type, code, description, sortby) values ('cgareason', 'REFUSEDACCESS', 'Homeowner Refused Access', 4)
INSERT INTO reference (type, code, description, sortby) values ('cgareason', 'B', Null, 5)  

--Add CGA reason column
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'work_order_inspection' and COLUMN_NAME='cga_reason')
  alter table work_order_inspection add cga_reason varchar(15) null
go

--Add CGA reason to 
IF NOT EXISTS (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'wo_status_history' and COLUMN_NAME='cga_reason')
  alter table wo_status_history add cga_reason varchar(15) null
go

/*To rollback:
-- drop the new columns
    
if exists (select * from information_schema.columns
  where table_name = 'work_order_inspection' and column_name = 'cga_reason')
    alter table work_order_inspection drop column cga_reason
GO

if exists (select * from information_schema.columns
  where table_name = 'wo_status_history' and column_name = 'cga_reason')
    alter table wo_status_history drop column cga_reason
GO

*/

