-- Add new tse_entry_id column to message_dest table, along with an index & foreign key constraint 
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='message_dest' and COLUMN_NAME='tse_entry_id') 
  alter table message_dest 
    add tse_entry_id integer null
go

if not exists (select * from sys.indexes where name = 'message_dest_tse_entry_id') 
  create index message_dest_tse_entry_id on message_dest (tse_entry_id)
go

if not exists (select constraint_name from information_schema.table_constraints 
 where constraint_name = 'message_dest_fk_timesheet_entry' and table_name = 'message_dest' and constraint_type = 'foreign key') 
    alter table message_dest
      add constraint message_dest_fk_timesheet_entry foreign key (tse_entry_id) references timesheet_entry (entry_id)
go

-- New column & foreign key constraint for break_rules_response table
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='break_rules_response' and COLUMN_NAME='message_dest_id') 
  alter table break_rules_response
    add message_dest_id integer null
go

if not exists (select constraint_name from information_schema.table_constraints 
 where constraint_name = 'break_rules_response_fk_message_dest' and table_name = 'break_rules_response' and constraint_type = 'foreign key') 
    alter table break_rules_response
      add constraint break_rules_response_fk_message_dest foreign key (message_dest_id) references message_dest (message_dest_id)
go
