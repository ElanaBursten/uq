--Index on Ticket Risk Table
USE [QM]
GO
CREATE NONCLUSTERED INDEX [IX_ticket_risk_ticket_id]
ON [dbo].[ticket_risk] ([ticket_id])