	
--QMANTWO-302_Alter_Schema.sql - This adds Mapping permissions for Mapping all locators	
	
if not Exists(select * from right_definition WHERE entity_data = 'MapAllLocators')

INSERT INTO right_definition
           (right_description,right_type,entity_data,parent_right_id,modifier_desc)
     VALUES
           ('Mapping - View Manager Locates','General','MapAllLocators',NULL,
           'Limitation: Employee ID representing the part of the tree hierarcy the user may view locator positions.')
GO

--Select * from right_definition WHERE entity_data = 'MapAllLocators'