
/* response_ack_context (replaces "pickle" files) */
create table response_ack_context (
    filename varchar(80) not null, -- filename as returned by the server
	response_id integer not null,
	locate_id integer not null,
	ticket_id integer not null,
	call_center varchar(20) not null,
	client_code varchar(10) not null,
	resp_type varchar(10) not null,
	insert_date datetime not null default getdate(),
	ticket_number varchar(20) not null,
    constraint pk_response_ack_context primary key (call_center, filename),
	constraint response_ack_context_fk_response_id foreign key (response_id)
	  references response_log(response_id)
);


