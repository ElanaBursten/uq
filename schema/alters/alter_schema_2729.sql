/* Add new MeetingTicketTypes value group & members if they don't exist */
if not exists (select * from value_group where group_code = 'MeetingTicketTypes') 
  insert into value_group(group_code, comment) values ('MeetingTicketTypes', 'meeting ticket type values')
  
if not exists (select * from value_group_detail 
               inner join value_group on value_group_detail.value_group_id = value_group.value_group_id 
               where group_code = 'MeetingTicketTypes' and match_value = 'LPMEET')
  insert into value_group_detail(value_group_id, match_type, match_value) 
  select value_group_id, 'CONTAINS', 'LPMEET' from value_group where group_code = 'MeetingTicketTypes'

if not exists (select * from value_group_detail 
               inner join value_group on value_group_detail.value_group_id = value_group.value_group_id 
               where group_code = 'MeetingTicketTypes' and match_value = 'LPEXCA')
  insert into value_group_detail(value_group_id, match_type, match_value) 
  select value_group_id, 'CONTAINS', 'LPEXCA' from value_group where group_code = 'MeetingTicketTypes'
