/* Adds new ManageProfitCenter right definition */
if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'ManageProfitCenter') 
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
    values ('Manage - Profit Center', 'General', 'ManageProfitCenter', 
    'Limitation does not apply to this right.')

/* Be sure to apply these other SQL script updates:
    get_emp_pc_manager.sql
    get_report_hier4.sql
    RPT_break_exception.sql
*/
