
CREATE TABLE ticket_hp (
    ticket_hp_id integer identity (38000, 1),
    ticket_id integer NOT NULL,
    data varchar(150) NOT NULL,
    insert_date datetime NOT NULL default getdate()
)
GO

ALTER TABLE dbo.ticket_hp add constraint 
  ticket_hp_fk_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO

