/* ============================================================ */
/*   Table: employee_upload_queue - track pending attachment    */
/*          upload summary by employee                          */
/* ============================================================ */
if not exists (select * from information_schema.columns where table_name = 'employee_upload_queue')
  create table employee_upload_queue (
    emp_id integer not null,
    file_count integer not null default 0,               -- how many files to upload
    file_size_total decimal(9,0) not null default 0,     -- size in KB of files to upload
    oldest_file_minutes integer not null default 0,      -- minutes since the oldest was attached
    modified_date datetime not null default GetDate(),   -- server date of the last time stats were updated
    constraint pk_employee_upload_queue primary key (emp_id)
    )
go

if not exists (select * from information_schema.table_constraints 
 where constraint_name = 'employee_upload_queue_fk_employee' 
 and table_name = 'employee_upload_queue' and constraint_type = 'foreign key') 
  alter table employee_upload_queue add constraint employee_upload_queue_fk_employee
    foreign key (emp_id) references employee (emp_id)
go
