--QMANTWO-203 Adding Lat/Long to Employee_Activity

--QMANTWO-203_alter_schema.sql
IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_activity' and COLUMN_NAME='lat')
  Alter table employee_activity
    Add lat decimal(9,6)
GO

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
              TABLE_NAME = 'employee_activity' and COLUMN_NAME='lng')
  Alter table employee_activity
    Add lng decimal(9,6)
GO


--ROLLBACK QMANTWO-203 CHANGES

--IF EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
--              TABLE_NAME = 'employee_activity' and COLUMN_NAME='lat')
--  Alter employee_activity
--    DROP COLUMN lat
--GO

--IF EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where
--              TABLE_NAME = 'employee_activity' and COLUMN_NAME='lng')
--  Alter employee_activity
--    DROP COLUMN lng
--GO
