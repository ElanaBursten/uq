/* alter_schema_1069.sql */

create table damage_third_party (
	third_party_id integer identity (76000, 1) primary key,
	damage_id integer not null,
	claimant         varchar(45) null, -- name of party making claim
	address1         varchar(30) null,
	address2         varchar(30) null,
	city             varchar(20) null,
	state            char(2) null,
	zipcode          varchar(10) null,
	phone            varchar(12) null,
	claim_desc       varchar(500) null, -- arbitrary size chosen
	uq_notified_date datetime NULL, -- date UQ learned of this claim
	carrier_id       int null,      -- insurance carrier assigned
	carrier_notified bit not null default 0,
	demand           decimal(12, 2) null,
	claim_status     varchar(10) null,
	added_by         int not null,     -- updated by trigger
	modified_by      int not null,
	modified_date    datetime not null default getDate() -- updated by trigger
)
go
create index damage_third_party_damage_id on damage_third_party(damage_id)
go

alter table damage_estimate
  /* estimate_type values:        */
  /* 0: damaged facility estimate */
  /* 1: 3rd party estimate        */
  /* 2: litigation estimate       */
  /* can set based on third_party for existing records */
  add estimate_type integer NOT NULL DEFAULT 0,
  third_party_id integer null,
  litigation_id integer null
go

create index damage_estimate_third_party_id on damage_estimate(third_party_id)
create index damage_estimate_litigation_id on damage_estimate(litigation_id)
update damage_estimate set estimate_type = 1 where third_party = 1
go

if object_id('dbo.get_damage3') is not null
  drop procedure dbo.get_damage3
GO

create proc get_damage3(@DamageID int)
as
  select *
  from damage
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_estimate
  where damage_id=@DamageID
    and active = 1
  for xml auto

  select *
  from damage_history
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_invoice
  where damage_id=@DamageID
  for xml auto

  select *
  from attachment
  where foreign_id = @DamageID
    and foreign_type = 2
  for xml auto

  select *
  from damage_bill
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_litigation
  where damage_id=@DamageID
  for xml auto

  select *
  from damage_third_party
  where damage_id=@DamageID
  for xml auto
GO


grant execute on get_damage3 to uqweb, QManagerRole


if object_id('dbo.get_third_party') is not null
  drop procedure dbo.get_third_party
go

create proc get_third_party(@ThirdPartyID int)
as
begin
  select *
  from damage_third_party
  where third_party_id = @ThirdPartyID
  for xml auto

  select * 
  from damage
  where damage_id = 
    (select damage_id 
    from damage_third_party
    where third_party_id = @ThirdPartyID)
  for xml auto
end
go

grant execute on get_third_party to uqweb, QManagerRole
go

