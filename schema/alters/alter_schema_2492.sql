if not exists (select t.name from sys.tables t where t.name = 'highlight_rule')
  create table highlight_rule (
    highlight_rule_id integer identity,
    call_center varchar(20) not null,   -- Call center the rule applies to ('*' for all)
    field_name varchar(50) not null,    -- Ticket field to highlight
    word_regex varchar(300) not null,   -- word or RegEx to highlight
    regex_modifier varchar(30) null,    -- Perl RegEx modifiers: /i/m/s/x/A/E/X
    which_match int not null default 0, -- which matched expression group to highlight
    highlight_bold bit not null default 0, -- show with bold font
    highlight_color integer not null,      -- show with this hlcolor reference
    active bit not null default 1,
    modified_date datetime not null default getdate(),
    constraint pk_highlight_rule primary key (highlight_rule_id) )
go

if not exists (select * from information_schema.table_constraints
 where constraint_name = 'highlight_rule_fk_hlcolor_reference'
 and table_name = 'highlight_rule' and constraint_type = 'foreign key')
  alter table highlight_rule add constraint highlight_rule_fk_hlcolor_reference
    foreign key (highlight_color) references reference (ref_id)
go

create trigger highlight_rule_u on highlight_rule
for update
as
update highlight_rule set modified_date = getdate() where highlight_rule_id in (select highlight_rule_id from inserted)
go


if not exists (select * from reference where type='hlcolor' and code='YELLOW')
  insert into reference (type, code, description, active_ind, modifier)
  values ('hlcolor','YELLOW','Yellow',1,'#FF6')

if not exists (select * from reference where type='hlcolor' and code='BLUE')
  insert into reference (type, code, description, active_ind, modifier)
  values ('hlcolor','BLUE','Blue',1,'#9FF')

if not exists (select * from reference where type='hlcolor' and code='RED')
  insert into reference (type, code, description, active_ind, modifier)
  values ('hlcolor','RED','Red',1,'#F99')

if not exists (select * from reference where type='hlcolor' and code='NONE')
  insert into reference (type, code, description, active_ind, modifier)
  values ('hlcolor','NONE','None',1,'#FFF')
