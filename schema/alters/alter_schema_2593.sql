
-- Create Mark and Clear status groups
if not exists (select * from status_group where sg_name = 'Location-MarkStatusList')
  insert into status_group (sg_name)
    values ('Location-MarkStatusList' )
    
if not exists (select * from status_group where sg_name = 'Location-ClearStatusList')
  insert into status_group (sg_name)
    values ('Location-ClearStatusList' )    
  
go

--Initial setup of Mark and Clear status codes to respective groups; others may be added later
declare @MarkStatusGroupID integer
declare @ClearStatusGroupID integer

--Get the status group id for the Mark Status List
set @MarkStatusGroupID = (select sg_id from status_group where sg_name = 'Location-MarkStatusList')
set @ClearStatusGroupID = (select sg_id from status_group where sg_name = 'Location-ClearStatusList')

--Associate Clear statuses with Clear Status List
if not exists (select * from status_group_item where sg_id = @ClearStatusGroupID and status in ('C', 'CP', 'N', 'PC', 'PM', 'ZPC') )
  insert into status_group_item (sg_id, status)
    values (@ClearStatusGroupID, 'C' ),
           (@ClearStatusGroupID, 'CP' ),
           (@ClearStatusGroupID, 'N' ),
           (@ClearStatusGroupID, 'PC' ),
           (@ClearStatusGroupID, 'PM' ),
           (@ClearStatusGroupID, 'ZPC' )
	
--Associate Mark statuses with Mark Status List
if not exists (select * from status_group_item where sg_id = @MarkStatusGroupID and status in ('M', 'MCX', 'MFC', 'MFO', 'SSM', 'XM', 'ZPM') )
  insert into status_group_item (sg_id, status)
    values (@MarkStatusGroupID, 'M' ),
           (@MarkStatusGroupID, 'MCX' ),
           (@MarkStatusGroupID, 'MFC' ),
           (@MarkStatusGroupID, 'MFO' ),
           (@MarkStatusGroupID, 'SSM' ),
           (@MarkStatusGroupID, 'XM' ),
           (@MarkStatusGroupID, 'ZM' ),
           (@MarkStatusGroupID, 'ZPM' )

go

	
