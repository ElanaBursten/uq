--QM-1014 Fix to speed up and to remove extra var tables (Dycom reviewed: removed RESULT table var)
--QM_279 Past Due Rollup
--QM-133 PART 2 Pulls the past due ticket/locate counts for a particular manager/supervisor (sum includes all employees below).
--   * Used in multi_open_totals6 - past due
--   * calls get_hier2_active


if object_id('dbo.get_hier_pastdue2') is not null
	drop function dbo.get_hier_pastdue2
go

CREATE FUNCTION dbo.get_hier_pastdue2 (
    @id INT,
    @managers_only BIT,
    @level_limit INT
)
RETURNS TABLE
AS
RETURN
(
    WITH Temp AS (
        SELECT * 
        FROM dbo.get_hier2_active(@id, @managers_only, @level_limit)
    ),
    PastDueCounts AS (
        SELECT 
            e.h_emp_id, 
			COALESCE(count(distinct tkt.ticket_id), 0) AS pastdue_tkt_count, 
			COALESCE(count(distinct loc.locate_id),0) AS pastdue_loc_count
        --    SUM(CASE WHEN tkt.ticket_id IS NOT NULL THEN 1 ELSE 0 END) AS pastdue_tkt_count,
        --    SUM(CASE WHEN loc.locate_id IS NOT NULL THEN 1 ELSE 0 END) AS pastdue_loc_count
        FROM Temp e
        INNER JOIN dbo.locate loc ON e.h_emp_id = loc.assigned_to
        INNER JOIN dbo.ticket tkt ON tkt.ticket_id = loc.ticket_id
        WHERE tkt.due_date < GETDATE() 
          AND loc.active = 1 
          AND loc.status NOT IN ('-N', '-P')
        GROUP BY e.h_emp_id
    )
    SELECT 
        e.h_emp_id AS emp_id,
        e.h_short_name AS short_name,
        e.h_first_name AS first_name,
        e.h_last_name AS last_name,
        e.h_emp_number AS emp_number,
        e.h_type_id AS etype_id,
        e.h_report_level AS report_level,
        e.h_report_to AS report_to,
		pastdue_tkt_count,
		pastdue_loc_count
        --COALESCE(pdc.pastdue_tkt_count, 0) AS pastdue_tkt_count,
        --COALESCE(pdc.pastdue_loc_count, 0) AS pastdue_loc_count
    FROM Temp e
    LEFT JOIN PastDueCounts pdc ON e.h_emp_id = pdc.h_emp_id
);
GO


--Select * from get_hier_pastdue2(212, 0, 100)
--Select * from get_hier_pastdue2(13776, 0, 100)