USE [QM]
GO

ALTER TABLE [dbo].[customer] DROP CONSTRAINT [customer_fk_profit_center]
GO

/****** Object:  Table [dbo].[customer]    Script Date: 11/7/2018 11:50:09 AM ******/
DROP TABLE [dbo].[customer]
GO

/****** Object:  Table [dbo].[customer]    Script Date: 11/7/2018 11:50:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customer](
	[customer_id] [int] IDENTITY(700,1) NOT NULL,
	[customer_name] [varchar](40) NOT NULL,
	[customer_description] [varchar](150) NULL,
	[customer_number] [varchar](20) NULL,
	[city] [varchar](25) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](10) NULL,
	[street] [varchar](35) NULL,
	[phone] [varchar](16) NULL,
	[locating_company] [int] NOT NULL,
	[modified_date] [datetime] NOT NULL DEFAULT (getdate()),
	[active] [bit] NOT NULL DEFAULT (1),
	[street_2] [varchar](100) NULL,
	[attention] [varchar](100) NULL,
	[contact_name] [varchar](100) NULL,
	[contact_email] [varchar](100) NULL,
	[period_type] [varchar](20) NULL DEFAULT ('WEEK'),
	[pc_code] [varchar](15) NULL,
	[contract] [varchar](50) NULL,
	[contract_review_due_date] [date] NULL,
	[customer_number_pc] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [customer_fk_profit_center] FOREIGN KEY([pc_code])
REFERENCES [dbo].[profit_center] ([pc_code])
GO

ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [customer_fk_profit_center]
GO

