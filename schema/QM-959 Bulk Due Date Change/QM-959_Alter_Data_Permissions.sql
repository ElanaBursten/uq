--QM-959  Permissions: Ability to BULK Edit Ticket Due Date 6/19/2024


if not exists(select * from right_definition where entity_data='BulkTicketDueDate')
INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc]
           ,[modified_date])
     VALUES
           ('Tickets - BULK Edit Due Date'
           ,'General'
           ,'BulkTicketDueDate'
           ,'Limitation does not apply to this right.'
           ,GetDate())
GO


