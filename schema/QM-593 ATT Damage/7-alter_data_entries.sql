------------------------------------------------
-- QM-593 AT&T:    Data Entries for AT&T Damages
-- Form Master table:    custom_form
-- Form Detail table:    custom_form_field
-- Form Reference table: custom_form_reference
-- Form Answers:         custom_form_answer
-- modified 10/13/2022
------------------------------------------------
--NOTE: Make sure the form_id matches in the custom_form and custom_form_field
--      when running these scripts.  The link_ref_Id should match what is in the 
--      reference table for dmgco and the client from the damage
--      DMGCO: 486 (link_ref_id)
--      link_modifier is not set up at this time
-------------------------------------------------
use [TestDB]
--select * from custom_form
--delete from custom_form where form_id=4

declare @linkref integer
   Set @linkref=486
declare @formID integer


INSERT INTO [dbo].[custom_form]
           ([form_title]
           ,[form_description]
           ,[form_type]
           ,[foreign_type]
           ,[link_ref_id]
           ,[link_modifier]
           ,[view_start_date]
         --  ,[view_end_date]
           ,[active]
           ,[modified_date])
     VALUES
           ('ATT DAMAGE'  --form_title
           , 'Please fill in all answers to the best of your ability' -- form_description: (also general instructions)
           ,'ATTDMG'   --form_type: or the code that we use for searching on forms
           ,2          --foreign_type: damage 
           ,@linkref        --link_ref_id: ATT dmgco id from reference table
           ,'*'        -- link_modifier: star means it will apply to all (not state specific)
           ,GetDate()  -- view_start_date: When this form will start being displayed
         --  ,            view_end_date: When this form should stop being displayed for new entries>
           ,1          --active
           ,GetDate()  --modified_date
		   )
set @formID = SCOPE_IDENTITY()
--select @formID

---------------------------------------------------------------------

--GROUP 1 ITEMS
--HEADER GROUP 1
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'ON-TIME RESPONSE' --field_description
           ,'GRPHEAD' --FF, CHKBOX, etc
           , 'DMGGH' --code used to identify certain types of fields
           ,1  --Group that the field
           ,0  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   ) 

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Work Start Date Honored' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,1  --Group that the field belongs to
           ,1  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   ) 

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Re-scheduled Work Start Date Documented & Honored' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,1  --Group that the field belongs to
           ,2  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   ) 

--GROUP 2
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'WORKSITE ASSESSMENT' --field_description
           ,'GRPHEAD' --FF, CHKBOX, etc
           , 'DMGGH' --code used to identify certain types of fields
           ,2  --Group that the field belongs to
           ,0  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )
		   
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Dig Ticket Location & Worksite Location a "Match"' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,2  --Group that the field belongs to
           ,1  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   ) 

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Excavator''s Delineation Marks Present & "Aligned" with Dig Ticket Location Descrtion' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,2  --Group that the field belongs to
           ,2  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )
		   
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'All Cables & Facilities in "Dig Area" Accounted For, i.e. (UG, Buried, Aerial / Conduit, Service Wire / Cable-Copper, Fiber Optic)' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,2  --Group that the field belongs to
           ,3  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Pre-Dig Photos of Excavator''s Delineation Marks Taken' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,2  --Group that the field belongs to
           ,4  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

--GROUP 3
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'CABLE MAP RECORDS COMPREHESION' --field_description
           ,'GRPHEAD' --FF, CHKBOX, etc
           , 'DMGGH' --code used to identify certain types of fields
           ,3  --Group that the field belongs to
           ,0  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'All Cables & Facility Housings (MH''s, SB''s, HH''s, SAI''s, VRAD''s, Pedestals) on Map Records identified' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,3  --Group that the field belongs to
           ,1  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'All Conduit Facilities identified (Single, Multiple Structures & Structures in Separate Trenches)' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,3  --Group that the field belongs to
           ,2  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Hi-Profile Cables Identified' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,3  --Group that the field belongs to
           ,3  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

--GROUP 4
INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'FULL SCOPE OF TICKET WORKED' --field_description
           ,'GRPHEAD' --FF, CHKBOX, etc
           , 'DMGGH' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,0  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Cable Path Marks Present "FROM End-To-End" Through The Delineated Dig Area' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,1  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'(When Appropriate) Cable Marker Flags placed' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,2  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Cable Path Marks Properly Spaced and Accurate' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,3  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'"ATT/D" Symbol Placed, Meet M&P Standard and Legible' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,4  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )

INSERT INTO [dbo].[custom_form_field]([form_id],[field_description],[form_field],[field_info_type],[field_group],[field_order] ,[isrequired],[active],[modified_date])
     VALUES
           (@formID --form_id
           ,'Identied Hi-Profile Cables Documented' --field_description
           ,'CHKBOX' --FF, CHKBOX, etc
           , 'DMGCB' --code used to identify certain types of fields
           ,4  --Group that the field belongs to
           ,5  --field order
		   ,0  --is required
           ,1  --active
           ,GetDate()
		   )
select @formID
select * from custom_form
select * from custom_form_field where form_id = @formID

--delete from custom_form where form_id > 2
--delete from custom_form_field where form_id > 2
