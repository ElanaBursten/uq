------------------------------------------------
-- QM-593 AT&T:    TRIGGERS for custom_form, custom_form_field and custom_form_reference
-- Custom Form Tables - Defines the Form that will be filled out
--                      Tables to enable building of dynamic forms 
--                      Structure written for Additional custom damage forms
--                      but flexible enough to use throughout QManager Client
-- Form Master table:    custom_form
-- Form Detail table:    custom_form_field
-- Form Reference table: custom_form_reference
-- Form Answers:         custom_form_answer
-- EB Created 8/2022, modified 11/14/2022
------------------------------------------------
------------------------------------------------
CREATE trigger custom_form_u on custom_form
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  update custom_form 
   set modified_date = getdate()
   where form_id in (select form_id from inserted)
end
GO

CREATE trigger custom_form_field_u on custom_form_field
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  update custom_form_field 
   set modified_date = getdate()
   where form_field_id in (select form_field_id from inserted)
end
GO

CREATE trigger custom_form_reference_u on custom_form_reference
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  update custom_form_reference 
   set modified_date = getdate()
   where ref_id in (select ref_id from inserted)
end
GO

--select * from custom_form
--Select * from custom_form_field
--select * from custom_form_reference

