------------------------------------------------
-- QM-593 AT&T:    custom_form
-- Custom Form Tables - Defines the Form that will be filled out
--                      Tables to enable building of dynamic forms 
--                      Structure written for Additional custom damage forms
--                      but flexible enough to use throughout QManager Client
-- Form Master table:    custom_form
-- Form Detail table:    custom_form_field
-- Form Reference table: custom_form_reference
-- Form Answers:         custom_form_answer
-- EB Created 8/2022, modified 10/13/2022
------------------------------------------------
------------------------------------------------

if not exists (select table_name from information_schema.tables where table_name = 'custom_form')
CREATE TABLE custom_form(
  form_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
  form_title varchar(100) NOT NULL,
  form_description varchar(255) NOT NULL,
  form_type varchar(10) NOT NULL,
  foreign_type int NOT NULL,
  link_ref_id int NULL,
  link_modifier varchar(10) NULL, 
  view_start_date datetime NULL,
  view_end_date datetime NULL,
  active bit NOT NULL,
  modified_date datetime NOT NULL
)
GO
ALTER TABLE custom_form ADD  CONSTRAINT DF_custom_form_active  DEFAULT ((1)) FOR active
GO
ALTER TABLE custom_form ADD  DEFAULT (getdate()) FOR modified_date
GO
create index custom_form_link_ref_id on custom_form(link_ref_id)
GO



