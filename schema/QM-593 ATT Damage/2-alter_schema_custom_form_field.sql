------------------------------------------------
-- QM-593 AT&T:    custom_form_field
-- Custom Form Field Table - Defines the individual fields for a custom form
--                           Tables to enable building of dynamic forms 
--                           Structure written for Additional custom damage forms
--                           but flexible enough to use throughout QManager Client
-- Form Master table:    custom_form
-- Form Detail table:    custom_form_field
-- Form Reference table: custom_form_reference
-- Form Answers:         custom_form_answer
-- EB Created 8/2022, modified 10/13/2022
------------------------------------------------

CREATE TABLE custom_form_field(
	form_field_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	form_id int NOT NULL,
	field_description varchar(255) NOT NULL,
	form_field varchar(8) NOT NULL,
	field_info_type varchar(8) NOT NULL,
	field_group int NULL, --0 is default and will show at the beginning
	field_order int NULL,
--	selection_ref_type varchar(8) NULL,
	isrequired bit,
--	opt_hint varchar(255) NULL,
	active bit NOT NULL,
	modified_date datetime NOT NULL
)
GO

ALTER TABLE custom_form_field ADD  CONSTRAINT DF_custom_form_field_active  DEFAULT ((1)) FOR active
GO
ALTER TABLE custom_form_field ADD CONSTRAINT DF_custom_form_field_group DEFAULT((0)) FOR field_group
GO
ALTER TABLE custom_form_field ADD  DEFAULT (getdate()) FOR modified_date
GO
create index custom_form_field_form_id on custom_form_field(form_id)
GO




