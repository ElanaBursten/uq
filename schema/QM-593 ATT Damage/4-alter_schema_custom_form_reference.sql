------------------------------------------------
-- QM-593 AT&T:    custom_form_reference
-- Custom Form Reference Table - For fields that have:
--                                   * selection lists (comboboxes, lists)
--	                                 * extension for custom_form_field
--                                   One to Many with custom_form_field
-- Form Master table:    custom_form
-- Form Detail table:    custom_form_field
-- Form Reference table: custom_form_reference
-- Form Answers:         custom_form_answer
-- EB Created 8/2022, modified 10/13/2022
------------------------------------------------

CREATE TABLE custom_form_reference(
  ref_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
  form_id int NOT NULL,
  form_field_id int NOT NULL,
  display_order int,
  isdefault bit,
  ref_description varchar(100) NOT NULL,
  active bit NOT NULL default 1,
  modified_date datetime NOT NULL 
)

create index custom_form_reference_form_id on custom_form_reference(form_id)
GO

create index custom_form_reference_form_field on custom_form_reference(form_id, form_field_id)
GO