------------------------------------------------
-- QM-593 AT&T:    custom_form_answer
-- Custom Form Tables - Data collected from the forms 
--                      Structure written for Additional custom damage forms
--                      but flexible enough to use throughout QManager Client
-- Form Master table: custom_form
-- Form Detail table: custom_form_field
-- Form Answers:      custom_form_answer
--
-- EB Created 8/2022
------------------------------------------------

if not exists (select table_name from information_schema.tables where table_name = 'custom_form_answer')
CREATE TABLE custom_form_answer(
  answer_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
  form_field_id int NOT NULL,
  answer varchar(40) NOT NULL,
  foreign_id integer NULL, 
  answered_by int NOT NULL,
  answer_date datetime NOT NULL
)
GO

ALTER TABLE custom_form_answer ADD  DEFAULT (getdate()) FOR answer_date
GO
create index custom_form_answer_form_field_id on custom_form_answer(form_field_id)
GO

--select * from custom_form_answer
