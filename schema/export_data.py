# export_data.py
# Created: 2002.11.15 HN

from __future__ import with_statement

import csv
import datetime
import getopt
import os
import shutil
import string
import sys
import tempfile

# sys.path hackery for when we run this from the schema directory:
whereami = os.path.dirname(os.path.abspath(__file__))
print 'location:', whereami
ticket_parse_dir = os.path.join(whereami, "..", "ticket_parse")
sys.path.insert(0, ticket_parse_dir)

import win32api
#
import date
import strip_dates

BCP_COMMAND = "bcp %s..%s out %s -c -t%s -S %s -U %s -P %s"
BCP_MANUAL_COMMAND = "bcp \"%s\" queryout %s -c -t%s -S %s -U %s -P %s"
TAR_CMD = "tar -cjf %s %s/"
ZIP_EXE = os.path.join(win32api.GetShortPathName("C:\Program Files"),
          "7-zip", "7za.exe")
if not os.path.exists(ZIP_EXE):
    ZIP_EXE = os.path.join(win32api.GetShortPathName("C:\Program Files"),
              "7-zip", "7z.exe")
if os.path.exists(ZIP_EXE):
    ZIP_CMD = ZIP_EXE + " a -tzip -mx7 -x!%s\\fields-*.tsv  %s %s\\*.*"
else:
    ZIP_CMD = None

MAX_ROWS = 1000 # default number of rows we copy for large tables

export_data = [
    # table name, file name, separator for -t, large table, num rows to export, order by
    ("map_cell", "map_cell.tsv", "\\t", 1, 10000,("map_id","map_page_num","x_part","y_part")),
    ("map_qtrmin", "map_qtrmin.tsv", "\\t", 1, 10000,("qtrmin_lat","qtrmin_long","call_center")),
    ("alert_ticket_type", "alert_ticket_type.tsv", "\\t", 0, 5000, None),
    ("alert_ticket_keyword", "alert_ticket_keyword.tsv", "\\t", 0, 5000, None),
    ("office", "office.tsv", "\\t", 0, MAX_ROWS, None),
    ("users", "users.tsv", "\\t", 0, MAX_ROWS, None),
    ("employee", "employee.txt", '"|"', 0, MAX_ROWS, None),
    ("map", "map.tsv", "\\t", 0, MAX_ROWS, None),
    ("map_page", "map_page.tsv", "\\t", 0, MAX_ROWS, None),
    ("statuslist", "statuslist.tsv", "\\t", 0, MAX_ROWS, None),
    ("area", "area.tsv", "\\t", 0, MAX_ROWS, None),
    ("county_area", "county_area.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_cc_map", "billing_cc_map.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_rate", "billing_rate.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_party_respect", "billing_party_respect.tsv", "\\t", 0, MAX_ROWS, None),
    ("client", "client.tsv", "\\t", 0, MAX_ROWS, None),
    ("call_center", "call_center.tsv", "\\t", 0, MAX_ROWS, None),
    ("customer", "customer.txt", '"|"', 0, MAX_ROWS, None),
    ("reference", "reference.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_exclude_work_for", "billing_exclude_work_for.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_exclude_work_type", "billing_exclude_work_type.tsv", "\\t", 0, MAX_ROWS, None),
    ("map_trs", "map_trs.tsv", "\\t", 0, MAX_ROWS, None),
    ("holiday", "holiday.tsv", "\\t", 0, MAX_ROWS, None),
    ("routing_list", "routing_list.tsv", "\\t", 0, MAX_ROWS, None),
    ("profit_center", "profit_center.tsv", "\\t", 0, MAX_ROWS, None),
    ("carrier", "carrier.tsv", "\\t", 0, MAX_ROWS, None),
    ("followup_ticket_type", "followup_ticket_type.tsv", "\\t", 0, MAX_ROWS, None),
    ("asset_type", "asset_type.tsv", "\\t", 0, MAX_ROWS, None),
    ("upload_location", "upload_location.tsv", "\\t", 0, MAX_ROWS, None),
    ("right_definition", "right_definition.tsv", "\\t", 0, MAX_ROWS, None),
    ("ticket_ack_match", "ticket_ack_match.tsv", "\\t", 0, MAX_ROWS, None),
    ("term_group", "term_group.tsv", "\\t", 0, MAX_ROWS, None),
    ("term_group_detail", "term_group_detail.tsv", "\\t", 0, MAX_ROWS, None),
    ("center_group", "center_group.tsv", "\\t", 0, MAX_ROWS, ('center_group_id', 'group_code', 'comment', 'show_for_billing', 'modified_date', 'active' )),
    ("center_group_detail", "center_group_detail.tsv", "\\t", 0, MAX_ROWS, None),
    ("value_group", "value_group.tsv", "\\t", 0, MAX_ROWS, None),
    ("value_group_detail", "value_group_detail.tsv", "\\t", 0, MAX_ROWS, None),
    ("locating_company", "locating_company.tsv", "\\t", 0, MAX_ROWS, None),
    ("employee_right", "employee_right.tsv", "\\t", 0, MAX_ROWS, None),
    ("group_definition", "group_definition.tsv", "\\t", 0, MAX_ROWS, None),
    ("employee_group", "employee_group.tsv", "\\t", 0, MAX_ROWS, None),
    ("group_right", "group_right.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_output_config", "billing_output_config.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_unit_conversion", "billing_unit_conversion.tsv", "\\t", 0, MAX_ROWS, None),
    ("dycom_period", "dycom_period.tsv", "\\t", 0, MAX_ROWS, None),
    ("damage_default_est", "damage_default_est.tsv", "\\t", 0, MAX_ROWS, None),
    ("status_group", "status_group.tsv", "\\t", 0, MAX_ROWS, None),
    ("status_group_item", "status_group_item.tsv", "\\t", 0, MAX_ROWS, None),
    ("billing_gl", "billing_gl.tsv", "\\t", 0, MAX_ROWS, None),
    ("break_rules", "break_rules.txt", '"|"', 0, MAX_ROWS, None),
    ("upload_file_type", "upload_file_type.tsv", "\\t", 0, MAX_ROWS, None),
    ("right_restriction", "right_restriction.tsv", "\\t", 0, MAX_ROWS, None),
    ("restricted_use_message", "restricted_use_message.tsv", "\\t", 0, MAX_ROWS, None),
    ("required_document", "required_document.tsv", "\\t", 0, MAX_ROWS, None),
    ("required_document_type", "required_document_type.tsv", "\\t", 0, MAX_ROWS, None),
    ("email_queue_rules", "email_queue_rules.tsv", "\\t", 0, MAX_ROWS, None),
    ("fax_queue_rules", "fax_queue_rules.tsv", "\\t", 0, MAX_ROWS, None),
    ("status_translation", "status_translation.tsv", "\\t", 0, MAX_ROWS, None),
]

class DataExporter:

    def __init__(self, servername, dbname, login, password, tempdir, targetdir,
                 skiptar=0, skiplarge=0, post_process_only=0, noscrub=0,
                 clean_tempdir=1):

        self.servername = servername
        self.dbname = dbname
        self.login = login
        self.password = password
        self.tempdir = os.path.abspath(tempdir)
        self.targetdir = targetdir
        self.skiptar = skiptar
        if ZIP_CMD is None and not self.skiptar:
            self.skiptar = True
            print "Cannot find zip executable at %s, skipping" % ZIP_EXE
        self.skiplarge = skiplarge
        self.post_process_only = post_process_only
        self.noscrub = noscrub
        self._clean_tempdir = clean_tempdir

    def clean_tempdir(self):
        filenames = os.listdir(self.tempdir)
        fullnames = [os.path.join(self.tempdir, name) for name in filenames]
        for fullname in fullnames:
            print "Cleaning:", fullname
            try:
                os.remove(fullname)
            except:
                print >> sys.stderr, fullname, "could not be deleted"

    def make_csv_files(self):
        for tblname, filename, separator, large, num_rows, order in export_data:
            fullname = os.path.join(self.tempdir, filename)
            if not (self.skiplarge and large):
                if large:
                    # get top num_rows *
                    spam = self.dbname + ".." + tblname
                    if order:
                        cols = []
                        for i,col in enumerate(order):
                            cols.append('"' + col + '"')
                        order_by = "order by " + string.join(cols, ',')
                    else:
                        order_by = " "
                    query = "select top %d * from %s %s" % (num_rows, spam,
                            order_by)
                    command = BCP_MANUAL_COMMAND % (query, fullname, separator,
                              self.servername, self.login, self.password)
                    command = command.replace('\n', ' ').replace('\r', '')
                else:
                    # get all records
                    command = BCP_COMMAND % (self.dbname, tblname, fullname,
                              separator, self.servername, self.login,
                              self.password)
                print command
                os.system(command)

    def strip_dates(self):
        print "Stripping dates..."
        mask1 = os.path.join(self.tempdir, "*.txt")
        strip_dates.processFiles(mask1, "|", outdir="")
        mask2 = os.path.join(self.tempdir, "*.csv")
        strip_dates.processFiles(mask2, ",", outdir="")
        mask3 = os.path.join(self.tempdir, "*.tsv")
        strip_dates.processFiles(mask3, "\t", outdir="")

    def tar_and_feather(self):
        archivename = "exportdata.zip"  # maybe put in a date/time?
        if os.path.exists(archivename):
            os.remove(archivename)
        print "Creating", archivename, "...",
        cmd = ZIP_CMD % (self.tempdir, archivename, self.tempdir)
        print >> sys.stderr, `cmd`
        os.system(cmd)
        print "OK"

        # let's copy the result here
        print "Copying...",
        # Verify that the directory exists. If not, create it
        if not os.path.isdir(self.targetdir):
            os.mkdir(self.targetdir)
        newname = os.path.join(self.targetdir, archivename)
        shutil.copyfile(archivename, newname)
        print "OK"

    def prepare(self):
        # verify that tempdir exists
        if not os.path.exists(self.tempdir):
            print "Directory", self.tempdir, "does not exist, creating...",
            os.makedirs(self.tempdir)
            print "OK"

    def run(self):
        if self.post_process_only:
            self.post_process()
        else:
            self.prepare()
            if self._clean_tempdir:
                self.clean_tempdir()
            self.make_csv_files()
            self.strip_dates()
            self.post_process()
            if not self.skiptar:
                self.tar_and_feather()

    def post_process(self):
        self.process_file("call_center.tsv", self.strip_emails, "call_center",
        1)
        self.process_file("area.tsv",self.fix_area,"area", 1)
        self.process_file("map_trs.tsv",self.fix_map_trs,"map_trs",1)
        self.process_file("county_area.tsv",self.fix_county_area,"county_area",1)
        self.process_file("status_group.tsv",self.fix_status_group,"status_group",1)
        self.process_file("map.tsv",self.fix_map,"map",1)
        self.process_file("term_group.tsv",self.fix_term_group,"term_group",1)
        self.process_file("employee.txt",self.fix_employee_dates, "employee", 1)
        self.process_file("client.tsv",self.fix_client, 'client',1)

        # data scrubbing
        if not self.noscrub:
            import scrub_data # imported here to avoid mutually recursive import
            scrubber = scrub_data.Scrubber(self.tempdir)
            self.process_file("users.tsv", scrubber.scrub_users, 'users', 1)
            self.process_file("employee.txt", scrubber.scrub_employees, 'employee', 1)
            self.process_file("client.tsv", scrubber.scrub_clients, 'client', 1)
            self.process_file("call_center.tsv", scrubber.scrub_call_centers,
                              'call_center', 1)
            self.process_file("customer.txt", scrubber.scrub_customers,
                              "customer", 1)
            self.process_file("locating_company.tsv", scrubber.scrub_locating_companies,
                              "locating_company", 1)
            self.process_file("office.tsv", scrubber.scrub_offices, 'office', 1)
            self.process_file("upload_location.tsv", scrubber.scrub_upload_locations,
                              "upload_location", 1)
            self.process_file("billing_rate.tsv", scrubber.scrub_billing_rates,
                              "billing_rate", 1)

    def strip_emails(self, parts):
        fieldnames = ['admin_email', 'emergency_email', 'summary_email',
                      'message_email', 'warning_email', 'responder_email',
                      'noclient_email']
        for field in fieldnames:
            if '@' in parts[field]:
                parts[field] = ""

    def fix_area(self, parts):
        if parts['area_name'].isspace() or len(parts['area_name']) == 0:
            parts['area_name'] = "-"

    def fix_employee_dates(self, parts):
        fieldnames = ['modified_date', 'hire_date', 'rights_modified_date']
        for fieldname in fieldnames:
            value = parts[fieldname]
            if value in (0, None, '', ' '):
                parts[fieldname] = datetime.date.today().isoformat()

    # XXX below: use fieldnames rather than position!

    def fix_map_trs(self, parts):
        fieldnames = ['township', 'range', 'section']
        for field in fieldnames:
            if parts[field].isspace() or len(parts[field]) == 0:
                parts[field] = "-"

    def fix_county_area(self, parts):
        if parts['munic'].isspace() or len(parts['munic']) == 0:
            parts['munic'] = "-"

    def fix_status_group(self, parts):
        if parts['sg_name'].isspace() or len(parts['sg_name']) == 0:
            parts['sg_name'] = "-"
        # Make sure modified_date is NULL
        if not parts['modified_date'].isspace() or len(parts['modified_date']) >= 0:
            parts['modified_date'] = " "
        # Make sure active bit is set
        #if len(parts) < 4:
        #    parts.append('1')
        if int(parts['active']) != 0 and int(parts['active']) != 1:
            parts['active'] = '1'

    def fix_map(self, parts):
        if parts['map_name'].isspace() or len(parts['map_name']) == 0:
            parts['map_name'] = "-"

    def fix_term_group(self, parts):
        if parts['group_code'].isspace() or len(parts['group_code']) == 0:
            parts['group_code'] = "-"

    def fix_client(self, parts):
        if parts['oc_code'].isspace() or len(parts['oc_code']) == 0:
            parts['oc_code'] = "-"

    def process_file(self, fname, function, table, use_dict=0):
        """ Process file <fname> which contains data for the given table.
            The function takes one argument (a list of parts) which may be
            changed in-place. """
        self.table_attributes(table)
        fields = self.read_table_attributes(table)
        print >> sys.stderr, "Post-processing:", table

        delimiter = '\t'
        if fname.endswith('.txt'): delimiter = '|'
        fullname = os.path.join(self.tempdir, fname)
        lines = read_csv_file(fullname, fields, delimiter)

        if lines:
            lines_new = []

            for iline, line in enumerate(lines):
                parts = line[:]

                if use_dict:
                    # create a dict for easier field access
                    d = dict([(fn, parts[idx] if idx < len(parts) else '')
                              for fn, idx in fields.items()])
                    function(d)
                    # reconstruct 'parts' list from dictionary
                    parts = [d[fn] for idx, fn in sorted([(idx, fn)
                             for fn, idx in fields.items()])]

                else:
                    function(parts)

                lines_new.append(parts)

            with open(os.path.join(self.tempdir, fname), "wb") as f:
                writer = csv.writer(f, delimiter=delimiter)
                writer.writerows(lines_new)

    def table_attributes(self, table):
        """ Retrieve field info for the given table from the database. """
        sql = """
           SELECT t.table_name,
              c.column_name 'name',
              c.data_type 'type',
              c.character_maximum_length 'length',
              c.numeric_precision,
              c.numeric_scale,
              c.is_nullable
           FROM %s.INFORMATION_SCHEMA.TABLES t, %s.INFORMATION_SCHEMA.COLUMNS c
           WHERE t.table_name = c.table_name AND t.table_name = '%s'
           ORDER BY c.ordinal_position
        """

        query = sql % (self.dbname, self.dbname, table)
        fullname = os.path.join(self.tempdir, 'fields-' + table + '.tsv')
        command = BCP_MANUAL_COMMAND % (query, fullname, '\\t',
                  self.servername, self.login, self.password)
        command = command.replace('\n', ' ').replace('\r', '')
        print >> sys.stderr, "Fetching table info for:", table
        os.system(command)

    def read_table_attributes(self, table):
        fullname = os.path.join(self.tempdir, 'fields-' + table + '.tsv')
        with open(fullname) as f:
            lines = f.readlines()

        # create a dictionary {fieldname: position}
        d = {}
        for idx, line in enumerate(lines):
            parts = line.split('\t')
            d[parts[1]] = idx # starts at 0
        return d

def read_csv_file(filename, fields, delimiter):
    # HACK to deal with newlines in text fields (exported by bcp)
    # Apparently Python's csv module does not handle this. :-(
    # Also note: the 'rb' is necessary as well.

    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=delimiter)
        lines = []
        current = []
        for row in reader:
            if len(row) >= len(fields):
                lines.append(row)
            else:
                if current:
                    current[-1] += row[0]
                    current.extend(row[1:]) # if any
                else:
                    current = row[:]
                if len(current) >= len(fields):
                    lines.append(current)
                    current = []

    return lines


__usage__ = """\
export.data.py [options]

Options:
    -d NAME         : database name (default: 'QM')
    -l              : (large) skip large files (for testing purposes)
    -n              : (--noscrub) Don't scrub data
    -p              : post-process only (use existing exported files)
    -s              : (skip) don't generate tar or zip file
    -t PATH         : (temp) directory used to store temporary files
    -N              : don't clean temp dir
    -P PASSWORD     : database password
    -S NAME         : server name
    -T PATH         : target directory that will contain exported files
    -U NAME         : database login (default 'sa')
"""

if __name__ == "__main__":

    # set defaults
    servername = "localhost"
    login = "sa"
    password = ""
    dbname = "QM"
    tempdir = "temp"
    targetdir = "c:/inetpub/ftproot"
    skiptar = 0
    skiplarge = 0
    noscrub = 0
    clean_tempdir = 1

    post_process_only = 0
    # when set, assume files are already there, and run post_process() only

    opts, args = getopt.getopt(sys.argv[1:], "S:U:P:d:t:T:slpN", ["noscrub"])

    for o, a in opts:
        if o == "-S":
            servername = a
        elif o == "-U":
            login = a
        elif o == "-P":
            password = a
        elif o == "-d":
            dbname = a
        elif o == "-t":
            tempdir = a
        elif o == "-T":
            targetdir = a
        elif o == "-s":
            skiptar = 1
        elif o == "-l":
            skiplarge = 1
        elif o == "-p":
            post_process_only = 1
        elif o in ("-n", "--noscrub"):
            noscrub = 1
        elif o == "-N":
            clean_tempdir = 0

    de = DataExporter(servername, dbname, login, password, tempdir, targetdir,
         skiptar=skiptar, skiplarge=skiplarge, post_process_only=post_process_only,
         noscrub=noscrub, clean_tempdir=clean_tempdir)
    de.run()

