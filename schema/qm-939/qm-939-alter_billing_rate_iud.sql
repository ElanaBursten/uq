USE [QM]
GO
/****** Object:  Trigger [dbo].[billing_rate_iud]    Script Date: 4/17/2024 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[billing_rate_iud] ON [dbo].[billing_rate]
FOR INSERT, UPDATE, DELETE
AS
BEGIN
  set nocount on
  declare @ChangeTime datetime
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
    insert into billing_rate_history
     (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, value_group_id, additional_line_item_text, CWICode, AddlCWICode
	  )
    select
      br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, @ChangeTime, which_invoice, additional_rate, value_group_id, additional_line_item_text, CWICode,AddlCWICode
    from deleted

  -- Upon insert/update update the modified_date for the changed rows  --qm-850  sr
    --update billing_rate
    --set modified_date = @ChangeTime
    --where br_id in (select br_id from inserted)
END
