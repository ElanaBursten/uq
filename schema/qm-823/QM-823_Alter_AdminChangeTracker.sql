/*
   Saturday, January 13, 20247:22:03 AM
   User: 
   Server: BRIAN-PC
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_adminchangetracker
	(
	AdminChange_ID int NOT NULL IDENTITY (1, 1),
	Changedby varchar(50) NOT NULL,
	TableName varchar(50) NOT NULL,
	ChangeMade varchar(MAX) NOT NULL,
	DateTimeChanged datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_adminchangetracker SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_adminchangetracker ON
GO
IF EXISTS(SELECT * FROM dbo.adminchangetracker)
	 EXEC('INSERT INTO dbo.Tmp_adminchangetracker (AdminChange_ID, Changedby, TableName, ChangeMade, DateTimeChanged)
		SELECT AdminChange_ID, CONVERT(varchar(50), Changedby), CONVERT(varchar(50), TableName), CONVERT(varchar(MAX), ChangeMade), DateTimeChanged FROM dbo.adminchangetracker WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_adminchangetracker OFF
GO
DROP TABLE dbo.adminchangetracker
GO
EXECUTE sp_rename N'dbo.Tmp_adminchangetracker', N'adminchangetracker', 'OBJECT' 
GO
COMMIT
select Has_Perms_By_Name(N'dbo.adminchangetracker', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.adminchangetracker', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.adminchangetracker', 'Object', 'CONTROL') as Contr_Per 