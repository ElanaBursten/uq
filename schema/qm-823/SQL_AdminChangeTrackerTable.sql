USE [QM]
GO  --QM-823 BP

/****** Object:  Table [dbo].[adminchangetracker]    Script Date: 9/24/2023 10:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[adminchangetracker](
	[AdminChange_ID] [int] IDENTITY(1,1) NOT NULL,
	[Changedby] [varchar](25) NOT NULL,
	[TableName] [varchar](25) NOT NULL,
	[ChangeMade] [varchar](250) NOT NULL,
	[DateTimeChanged] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


