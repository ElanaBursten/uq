/*
   Saturday, February 06, 20218:40:17 AM
   User: 
   Server: BRIAN-PC
   Database: TestDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.hp_utils_rank ADD
	client_id int NULL
GO
ALTER TABLE dbo.hp_utils_rank ADD CONSTRAINT
	DF_hp_utils_rank_active DEFAULT 1 FOR active
GO
ALTER TABLE dbo.hp_utils_rank SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.hp_utils_rank', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.hp_utils_rank', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.hp_utils_rank', 'Object', 'CONTROL') as Contr_Per 