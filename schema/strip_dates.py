# strip_dates.py
#
# Updated 2002.11.15, HN
# Some changes to make this more modular and more useful for export_data.py.

import glob
import re
import string
import os


def processFiles(wildcard, delim, outdir="out"):
    files = glob.glob(wildcard)

    date_regexp = re.compile("[0-9 :\-T.]{23}")
    regbad = re.compile('\0')

    for filename in files:
        print "processing %s" % (filename)
        file = open(filename, 'r')
        data = file.read()
        lines = data.split('\n')
        file.close()
        print "  %d lines" % (len(lines),)

        outlines = []
        for a in lines:
            if a <> '':
                if not (filename.endswith('holiday.tsv') or filename.endswith('dycom_period.tsv')):
                    a = re.sub(date_regexp, "", a)

                # remove junk
                a = re.sub(regbad, '', a)
                outlines.append(a)

        #outlines.sort()
        outfilename = os.path.join(outdir, filename)
        print "  writing %s" % (outfilename)
        outfile = open(outfilename, "w")
        for line in outlines:
            outfile.write  (line + "\n")
        outfile.close()


if __name__ == "__main__":

    processFiles('*.txt', "|")
    processFiles('*.csv', ",")
    processFiles('*.tsv', "\t")
