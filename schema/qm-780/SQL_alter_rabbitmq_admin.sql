/*
   Monday, August 07, 20239:06:17 AM
   User: 
   Server: BRIAN-PC
   Database: TestDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.rabbitmq_admin ADD CONSTRAINT
	DF_rabbitmq_admin_modified_date DEFAULT getdate() FOR modified_date
GO
ALTER TABLE dbo.rabbitmq_admin SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.rabbitmq_admin', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.rabbitmq_admin', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.rabbitmq_admin', 'Object', 'CONTROL') as Contr_Per 