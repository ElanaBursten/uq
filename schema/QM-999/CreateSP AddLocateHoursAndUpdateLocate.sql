USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[AddLocateHoursAndUpdateLocate]    Script Date: 10/31/2024 2:30:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[AddLocateHoursAndUpdateLocate]
    @ticket_ID INT,
    @Work_Date DATETIME,
	@Entry_Date DATETIME,
    @Client_Code VARCHAR(10),
    @Units_marked INT
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        BEGIN TRANSACTION;

    DECLARE @locate_ID INT;
	DECLARE @Status varchar(5);
	DECLARE @assigned_to_id INT;
    DECLARE @unit_conversion_id INT;
    DECLARE @first_unit_factor Decimal(10,0);
    DECLARE @rest_units_factor Decimal(10,0);
    DECLARE @total_units Decimal(10,0);
    DECLARE @total_marked INT;

	-- Test code
	--set	@ticket_ID = 161325532;
	--set	@Work_Date = '9/3/2024 3:27:36 PM';
	--set	@Entry_Date = '9/3/2024';
	--set	@Client_Code = 'ATTDNORCAL';


    -- Step 1: Get the locate_ID from [Locate] based on ticket_ID and Client_Code
    SELECT @locate_ID = locate_id, @Status=status,@assigned_to_id=assigned_to_id
    FROM [dbo].[Locate]
    WHERE ticket_id = @ticket_ID AND client_code = @Client_Code;

	-- Step 2: Delete any units for this locate that is not BigFoot.  Trigger will take care of notes 
	--Delete from [dbo].[locate_hours]
	--Where [locate_id]=@locate_ID
	--and BigFoot=0

    -- Step 3: Get the unit_conversion_id from [billing_unit_conversion] based on client_code
    SELECT @unit_conversion_id = uc.unit_conversion_id
    FROM [dbo].[billing_unit_conversion] uc
    JOIN [dbo].[Client] c ON uc.unit_conversion_id = c.unit_conversion_id
    WHERE c.oc_code = @Client_Code;

    -- Step 4: Insert new record into [locate_hours]
    INSERT INTO [dbo].[locate_hours] (locate_ID, emp_id, Work_Date, modified_date, regular_hours, overtime_hours, Entry_Date, Units_marked, Status, invoiced, hours_invoiced, unit_conversion_id, BigFoot)
    VALUES (@locate_ID, @assigned_to_id, @Work_Date,GetDate(),0.00, 0.00, @Entry_Date, @Units_marked, @Status, 0, 0, @unit_conversion_id,1);

	Select @locate_ID,@Status,@assigned_to_id, @Client_Code

    -- Step 5: Get the conversion rules
    SELECT @first_unit_factor = BUC.first_unit_factor, @rest_units_factor = BUC.rest_units_factor
    FROM [dbo].[billing_unit_conversion] BUC
    WHERE BUC.unit_conversion_id = @unit_conversion_id;

    -- Step 6: Sum total units marked for the locate_ID
    SELECT @total_marked = SUM(Units_marked)
    FROM [dbo].[locate_hours]
    WHERE locate_id = @locate_ID
	and bigfoot = 1

    -- Step 7: Calculate the number of units based on conversion rules
	IF @unit_conversion_id = 38014 or @unit_conversion_id is NULL --special rule because of weird entry
    BEGIN
        -- Every foot = 1 unit because @rest_units_factor is NULL
        SET @total_units = 1;--@total_marked;
    END
    ELSE
    BEGIN
        -- First @first_unit_factor feet = 1 unit, every additional @first_unit_factor feet = 1 unit
        IF @total_marked <= @first_unit_factor
            SET @total_units = 1;
        ELSE
            SET @total_units = 1 + CEILING((@total_marked - @first_unit_factor) / @rest_units_factor);
    END

    -- Step 8: Update the total units marked in [Locate]
    UPDATE [dbo].[Locate]
    SET qty_marked = @total_units
    WHERE locate_id = @locate_ID
	and client_code = @Client_Code;

	COMMIT TRANSACTION;
    END TRY
	    BEGIN CATCH
        -- If there is an error, roll back the transaction
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        -- Return error information
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();

        -- Raise the error to be caught by the calling process
        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
    END CATCH
END;
