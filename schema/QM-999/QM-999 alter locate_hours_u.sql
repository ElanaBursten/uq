USE [QM]
GO

/****** Object:  Trigger [dbo].[locate_hours_u]    Script Date: 11/1/2024 11:01:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER TRIGGER [dbo].[locate_hours_u] ON [dbo].[locate_hours]
FOR UPDATE
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON


  -- See what columns were in the update statement
  declare @hours_invoiced_col int
  declare @units_invoiced_col int

  set @hours_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'hours_invoiced')
  set @units_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'units_invoiced')

  if (@hours_invoiced_col < 9) or (@hours_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('hours_invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (@units_invoiced_col < 9) or (@units_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('units_invoiced has moved out of the second column byte', 18, 1) with log
  end

  -- Exit if only one of the invoiced columns was in the update statement (see commit_billing)
  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@hours_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@units_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- Update the modified_date when a locate_hours row changes
  update locate_hours set modified_date = getdate()
  where locate_hours_id in (select locate_hours_id from inserted)
END
GO

