USE [QM]
GO

/****** Object:  Trigger [dbo].[trg_Insert_LocateHours_Notes]    Script Date: 10/28/2024 9:54:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[trg_Insert_LocateHours_Notes]
ON [dbo].[locate_hours]
AFTER INSERT
AS
BEGIN
    DECLARE @locate_id INT,
            @uid INT,
            @entry_date DATETIME,
            @modified_date DATETIME,
            @units_marked INT,
            @note VARCHAR(8000),
            @Bigfoot NVARCHAR(40);

    -- Get the values from the inserted row
    SELECT @locate_id = locate_id,
           @uid = 21,
           @entry_date = entry_date,
           @modified_date = GETDATE(),
           @units_marked = units_marked,
           @Bigfoot = Bigfoot
    FROM inserted;

    -- Debugging: Check if locate_id is NULL
    IF @locate_id IS NULL
    BEGIN
        RAISERROR ('locate_id is NULL in trg_Insert_LocateHours_Notes', 16, 1);
        RETURN;
    END

    IF @Bigfoot = 1
    BEGIN
        SET @note = CAST(ISNULL(@units_marked, 0) AS VARCHAR(10)) + ' entered By BigFoot';

        -- Insert into the notes table only if locate_id is not NULL
        IF @locate_id IS NOT NULL
        BEGIN
            INSERT INTO [dbo].[notes] (foreign_type, foreign_id, entry_date, modified_date, uid, active, note, sub_type)
            VALUES (5, @locate_id, @entry_date, @modified_date, @uid, 1, @note, 501);
        END
    END
END;
GO

