/*
   Saturday, September 04, 20212:14:05 PM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_customer_locator
	(
	customer_locator_id int NOT NULL IDENTITY (1, 1),
	customer_id int NOT NULL,
	loc_emp_id int NOT NULL,
	RoBoSource nvarchar(256) NULL,
	RoBoDest nvarchar(256) NULL,
	RoBoOptions nvarchar(256) NULL,
	RoBoFiles nvarchar(256) NULL,
	RoBoLogs nvarchar(256) NULL,
	active bit NOT NULL,
	modified_date datetime NOT NULL,
	copyStart datetime NULL,
	copyFinish datetime NULL,
	fileCount int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_customer_locator SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_customer_locator ADD CONSTRAINT
	DF_customer_locator_active DEFAULT 1 FOR active
GO
ALTER TABLE dbo.Tmp_customer_locator ADD CONSTRAINT
	DF_customer_locator_modified_date DEFAULT getdate() FOR modified_date
GO
SET IDENTITY_INSERT dbo.Tmp_customer_locator ON
GO
IF EXISTS(SELECT * FROM dbo.customer_locator)
	 EXEC('INSERT INTO dbo.Tmp_customer_locator (customer_locator_id, customer_id, loc_emp_id, RoBoSource, RoBoDest, RoBoOptions, RoBoFiles, RoBoLogs, active, modified_date, copyStart, copyFinish, fileCount)
		SELECT customer_locator_id, customer_id, loc_emp_id, RoBoSource, RoBoDest, RoBoOptions, RoBoFiles, RoBoLogs, active, modified_date, copyStart, copyFinish, fileCount FROM dbo.customer_locator WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_customer_locator OFF
GO
DROP TABLE dbo.customer_locator
GO
EXECUTE sp_rename N'dbo.Tmp_customer_locator', N'customer_locator', 'OBJECT' 
GO
ALTER TABLE dbo.customer_locator ADD CONSTRAINT
	PK_customer_locator PRIMARY KEY CLUSTERED 
	(
	customer_locator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.customer_locator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.customer_locator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.customer_locator', 'Object', 'CONTROL') as Contr_Per 