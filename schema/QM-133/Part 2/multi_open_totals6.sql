--QM-248 & QM-133 Past Due Part 2
--This version MOT6 will only be called if user has permissions for Past Due
--Additional changes to propogate the past due tickets up the line

if object_id('dbo.multi_open_totals6') is not null
  drop procedure dbo.multi_open_totals6
GO	

create proc multi_open_totals6(@Managers varchar(100), @GetTicketActivity bit, @GetDamages bit, @GetWorkOrders bit)
as
-- This SP is the replacement for DRTT; it processes more than
-- one manager at once, and does so efficiently.

set nocount on

-- a place to keep the list of requested managers in table form
declare @manager_list table (
  manager_id integer not null primary key
)

-- These are the totals returned to the client for each employee
declare @results table (
  emp_tree_id varchar(15) not null primary key,
  emp_id int not null,
  report_to varchar(10) null,
  short_name varchar(30) null,
  node_name varchar(50) null,
  emp_number varchar(20) null,
  type_id integer null,
  active bit not null default 1,
  sort tinyint not null default 9,
  ticket_view_limit integer not null default 0,
  show_future_tickets bit not null default 1,
  tickets_tot integer not null default 0,
  locates_tot integer not null default 0,
  damages_tot integer not null default 0,
  wo_tot integer not null default 0,
  work_status_today varchar(6) not null default 'OFF',
  contact_phone varchar(20)
)

-- List of the emps, de-duped from the above
DECLARE @emps TABLE (
  emp_id integer NOT NULL PRIMARY KEY
)

-- various temps used to assemble the final data
DECLARE @open_tickets TABLE (
  ticket_emp_id integer NOT NULL,
  N integer
)

DECLARE @open_locates TABLE (
  locate_emp_id integer NOT NULL,
  N integer
)

DECLARE @open_damages TABLE (
  damage_emp_id integer NOT NULL,
  N integer
)

DECLARE @open_work_orders TABLE (
  wo_emp_id integer NOT NULL,
  N integer
)

DECLARE @ut_managers TABLE (
  manager_id integer NOT NULL PRIMARY KEY,
  active bit NOT NULL
)

-- all emps for unacked ticket find
DECLARE @ut_emps TABLE (
  emp_id integer NOT NULL,
  manager_id integer NOT NULL,
  PRIMARY KEY (emp_id, manager_id)
)

DECLARE @unacked_tickets TABLE (
  ticket_id integer NOT NULL,
  manager_id integer NOT NULL,
  primary key(ticket_id, manager_id)
)

DECLARE @unacked_ticket_activity TABLE (
  ticket_id integer NOT NULL,
  manager_id integer NOT NULL
)

--DECLARE @pastdue_tickets TABLE (
-- ticket_id integer NOT NULL,
-- manager_id integer NOT NULL
--)
DECLARE @tktcount int;
DECLARE @loccount int;

-- get the list of manager from the string to the table
insert into @manager_list
  select cast(S as int) from dbo.StringListToTable(@managers)
  where (S is not null) and (S<>'')

-- Initialize the results table with the list of locators under the managers
insert into @results (emp_tree_id, emp_id, report_to, short_name, node_name, emp_number, type_id, active, ticket_view_limit, show_future_tickets, contact_phone)
  select distinct emp_id, 
    emp_id, 
    report_to, 
    short_name,
    coalesce(emp_number + ': ', '') + short_name,
    emp_number, 
    type_id, 
    active, 
    coalesce(ticket_view_limit, 0), 
    coalesce(show_future_tickets, 1),
	contact_phone
  from employee
  inner join @manager_list ml on report_to = ml.manager_id or emp_id=ml.manager_id
  where employee.report_to is not null   -- this disallowing the top guy.

-- gather a de-duped list, which will only have one copy of those buckets
insert into @emps
select distinct emp_id from @results

-- Get a count of open tickets for the locators
INSERT INTO @open_tickets (ticket_emp_id, N)
  select res.emp_id, count(distinct ticket.ticket_id) as N
  from @emps res
    inner join locate on res.emp_id = locate.assigned_to
    inner join ticket on locate.ticket_id=ticket.ticket_id
  group by res.emp_id

-- Get a count of the open locates for the locators
INSERT INTO @open_locates (locate_emp_id, N)
  select res.emp_id, count(locate.locate_id) as N
  from @emps res
    inner join locate on locate.assigned_to = res.emp_id
    inner join ticket on locate.ticket_id = ticket.ticket_id
  where (locate.status <> '-N') and (locate.status <> '-P')
  group by res.emp_id

-- Get count of pending damages for the locators
INSERT INTO @open_damages (damage_emp_id, N)
  select res.emp_id, count(damage.damage_id) as N
  from @emps res
    inner join damage on damage.investigator_id = res.emp_id
  where @GetDamages = 1
    and damage.damage_type in ('INCOMING', 'PENDING') and damage.active = 1
  group by res.emp_id

-- Get count of pending work orders for the emps
INSERT INTO @open_work_orders (wo_emp_id, N)
  select res.emp_id, count(wo.wo_id) as N
  from @emps res
    inner join work_order wo on wo.assigned_to_id = res.emp_id
  where @GetWorkOrders = 1
    and wo.closed = 0 and wo.active = 1
  group by res.emp_id

update @results set locates_tot = n
  from @open_locates where emp_id = locate_emp_id

update @results set tickets_tot = ot.n
  from @open_tickets ot where emp_id = ot.ticket_emp_id

update @results set damages_tot = n
  from @open_damages od where emp_id = damage_emp_id

update @results set wo_tot = n
  from @open_work_orders where emp_id = wo_emp_id

-- eligible for unacked tickets
declare @ShowUnackedTicketsRightID int
set @ShowUnackedTicketsRightID = (select right_id from right_definition where entity_data = 'TicketsShowUnAcked')

-- of the managers requested, get the ones who need an Unacked bin
insert into @ut_managers (manager_id, active)
select ml.manager_id, r.active
 from @manager_list ml
  inner join @results r on ml.manager_id = r.emp_id
where dbo.emp_has_right(ml.manager_id, @ShowUnackedTicketsRightID) = 1

-- Get by with only one cursor, to run get_hier a few times
declare @manid integer
DECLARE ManListCursor CURSOR FOR
select manager_id from @manager_list

OPEN ManListCursor

FETCH NEXT FROM ManListCursor into @manid
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Collect the emps for the unacked tickets
  insert into @ut_emps (emp_id, manager_id)
  select h_emp_id, @manid from dbo.get_hier(@manid, 0)

  FETCH NEXT FROM ManListCursor into @manid
END

CLOSE ManListCursor
DEALLOCATE ManListCursor

-- Check for any tickets that need acknowledgement
insert into @unacked_tickets (ticket_id, manager_id)
  select ticket_ack.ticket_id, e.manager_id
  from ticket_ack
    inner join @ut_emps e on ticket_ack.locator_emp_id = e.emp_id
  where (ticket_ack.has_ack = 0)
    and (ticket_ack.locator_emp_id is not null)
  group by ticket_ack.ticket_id, e.manager_id

-- Insert rows in the result for the unack buckets; emp_tree_id is manager_id + '-EA' to maintain uniqueness.
insert into @results (emp_tree_id, emp_id, report_to, short_name, node_name, type_id, active, sort, tickets_tot, locates_tot)
select convert(varchar(6), utm.manager_id) + '-EA' emp_tree_id, 
  -1 emp_id, 
  utm.manager_id, 
  'New Emergencies' short_name, 
  'New Emergencies' node_name,
  -1 type_id, 
  utm.active,
  1 sort, 
  (select count(*) from @unacked_tickets unackt where unackt.manager_id=utm.manager_id) tickets_tot,
  (select count(*)
   from @unacked_tickets unack
    inner join locate on unack.ticket_id = locate.ticket_id
       and locate.assigned_to is not null
    where (unack.manager_id=utm.manager_id) and (locate.status <> '-N') and (locate.status <> '-P')) locates_tot--QM-133 Past Due - fixed status return
from @ut_managers utm

---------------------------------PAST DUE: QM-133---------------------------------------
  
insert into @results (emp_tree_id, emp_id, report_to, short_name, node_name, type_id, active, sort, tickets_tot, locates_tot)
select convert(varchar(6), utm.manager_id) + '-PD' emp_tree_id, 
  -3 emp_id, 
  utm.manager_id, 
  'Past Due' short_name, 
  'Past Due' node_name,
  -3 type_id, 
  utm.active,
  1 sort,
  ISNULL((select sum(pastdue_tkt_count) from dbo.get_hier_pastdue2(utm.manager_id, 0, 10)), 0), 
  ISNULL((select sum(pastdue_loc_count) from dbo.get_hier_pastdue2(utm.manager_id, 0, 10)), 0)
 
from @ut_managers utm
-----------------------------
-- Insert rows in the result for the unack buckets; emp_tree_id is manager_id + '-EA' to maintain uniqueness.
-- Check for any ticket activities that need acknowledgement from the past 10 days
insert into @unacked_ticket_activity (ticket_id, manager_id)
  select tick_act.ticket_id, e.manager_id
  from ticket_activity_ack tick_act
    inner join @ut_emps e on tick_act.locator_emp_id = e.emp_id
  where @GetTicketActivity = 1
    and tick_act.has_ack = 0
    and tick_act.locator_emp_id is not null
    and tick_act.activity_date >= DateAdd(Day, -10, GetDate())

-- Insert rows in the result for the unacked activity; emp_tree_id is manager_id + '-AA' to maintain uniqueness.
insert into @results (emp_tree_id, emp_id, report_to, short_name, node_name, type_id, active, sort, tickets_tot, locates_tot)
select convert(varchar(6), utm.manager_id) + '-AA' emp_tree_id, 
  -2 emp_id, 
  utm.manager_id, 
  'Ticket Activity' short_name,
  'Ticket Activity' node_name,  
  -2 type_id, 
  utm.active,
  2 sort,
  (select count(*) from @unacked_ticket_activity unackt where unackt.manager_id=utm.manager_id) tickets_tot,
  (select count(*)
   from @unacked_ticket_activity unack
    inner join locate on (unack.ticket_id = locate.ticket_id
      and (locate.assigned_to is not null or locate.assigned_to_id is not null) and
	   (locate.status <> '-N') and (locate.status <> '-P'))
    where unack.manager_id=utm.manager_id) locates_tot
from @ut_managers utm
where @GetTicketActivity = 1

update @results set work_status_today = dbo.get_current_work_status(emp_id)

select 'emp_workload' as tname, *,
  convert(bit, 1) as under_me
from @results emp_workload
--order by convert(int, report_to), short_name

GO
grant execute on multi_open_totals5 to uqweb, QManagerRole

/*
-- to test the CA managers special case:
exec dbo.multi_open_totals5 '1870,1907,1949,14545,1920,1916,1952,7012,7745,6921,1948,1928,1918,6707', 1, 0, 0

exec dbo.multi_open_totals5 '211', 0, 0, 0
exec dbo.multi_open_totals5 '212', 0, 0, 0
exec dbo.multi_open_totals5 '1787', 0, 0, 0
exec dbo.multi_open_totals5 '2676', 0, 0, 0
exec dbo.multi_open_totals5 '5034', 0, 0, 0
exec dbo.multi_open_totals5 '211,212', 0, 0, 0
exec dbo.multi_open_totals5 '697,211,212', 0, 0, 0
exec dbo.multi_open_totals5 '211,208,207,1634,2102,2139,2131', 0, 0, 0
exec dbo.multi_open_totals5 '1787,6301,5372,563,561,5116,599,554,1221,628,591', 0, 0, 0
exec dbo.multi_open_totals5 '2597,2637,2646,5076,5083,2604,2620,2623,2611,4426', 0, 0, 0
exec dbo.multi_open_totals5 '2540,2545,4913,2563,2564,2167,2182,5864,3648', 0, 0, 0
exec dbo.multi_open_totals5 '1787,6301,571,5372,563,551,561,599,5116,663,554,5176,1221,628,2124', 0, 0, 0
exec dbo.multi_open_totals5 '697,3841,791,816,741,810', 0, 0, 0
exec dbo.multi_open_totals5 '1059,4845,13152,810,811,782,4404', 0, 0, 0

exec dbo.multi_open_totals5 '811', 1, 1, 1
exec dbo.multi_open_totals2 '811'
exec dbo.multi_open_totals '811'

select ticket_ack.ticket_id, locate.assigned_to
 from ticket_ack with(index(ticket_ack_has_ack))
    inner join locate
      on ticket_ack.ticket_id = locate.ticket_id
  where ticket_ack.has_ack = 0

select * from ut_emps
drop table ut_emps

  select ticket_ack.ticket_id, e.manager_id
  from ticket_ack with(index(ticket_ack_has_ack))
    inner join locate
      on ticket_ack.ticket_id = locate.ticket_id
    inner join @ut_emps e on locate.assigned_to = e.emp_id
  where ticket_ack.has_ack = 0
  group by ticket_ack.ticket_id, e.manager_id


-- refresh all of them:
exec dbo.multi_open_totals5 '211,340,1869,2108,2263,2304,2305,2327,2676,2677,2678,2692,2693,2694,2725,2726,2727,2777,3511,3849,3850,4255'

select * from ticket_ack
where ticket_ack.has_ack = 0

select * from ticket_activity_ack
where has_ack = 0
*/