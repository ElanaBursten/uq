Use [QM]

/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
GO
ALTER TABLE dbo.damage_history ADD
    locator_id int NULL,
    client_id int NULL,
	investigator_id int NULL,
	claim_coordinator_id int NULL,
	disc_repair_person varchar(30) NULL,
	added_by int NULL,
	approved_by_id int NULL
GO
ALTER TABLE dbo.damage_history SET (LOCK_ESCALATION = TABLE)
GO
select Has_Perms_By_Name(N'dbo.damage_history', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.damage_history', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.damage_history', 'Object', 'CONTROL') as Contr_Per 