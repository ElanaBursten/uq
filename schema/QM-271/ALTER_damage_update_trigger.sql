USE [QM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*** insert trigger code moved to damage_i 1/1/2021 B.P. 2:42:59 PM  ***/

IF (OBJECT_ID(N'[dbo].[damage_iu]') IS NOT NULL)
DROP TRIGGER damage_iu;
GO

/****** Object:  Trigger [dbo].[damage_u]    Script Date: 1/1/2021 2:42:59 PM ******/
CREATE trigger [dbo].[damage_u] on [dbo].[damage]
for update
as
  set nocount on
if SYSTEM_USER = 'no_trigger'
  return

  declare @damages table (damage_id int not null)

  /* Find damages that have changed in an important way */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.exc_resp_code <> ins.exc_resp_code) or
      (del.exc_resp_code is null and ins.exc_resp_code is not null) or
      (del.exc_resp_code is not null and ins.exc_resp_code is null) or
    (del.exc_resp_type <> ins.exc_resp_type) or
      (del.exc_resp_type is null and ins.exc_resp_type is not null) or
      (del.exc_resp_type is not null and ins.exc_resp_type is null) or
    (del.uq_resp_code <> ins.uq_resp_code) or
      (del.uq_resp_code is null and ins.uq_resp_code is not null) or
      (del.uq_resp_code is not null and ins.uq_resp_code is null) or
    (del.uq_resp_type <> ins.uq_resp_type) or
      (del.uq_resp_type is null and ins.uq_resp_type is not null) or
      (del.uq_resp_type is not null and ins.uq_resp_type is null) or
    (del.spc_resp_code <> ins.spc_resp_code) or
      (del.spc_resp_code is null and ins.spc_resp_code is not null) or
      (del.spc_resp_code is not null and ins.spc_resp_code is null) or
    (del.spc_resp_type <> ins.spc_resp_type) or
      (del.spc_resp_type is null and ins.spc_resp_type is not null) or
      (del.spc_resp_type is not null and ins.spc_resp_type is null) or
    (del.profit_center <> ins.profit_center) or
      (del.profit_center is null and ins.profit_center is not null) or
      (del.profit_center is not null and ins.profit_center is null) or
    (del.reason_changed <> ins.reason_changed) or
      (del.reason_changed is null and ins.reason_changed is not null) or
      (del.reason_changed is not null and ins.reason_changed is null) or

/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
	(del.locator_id <> ins.locator_id) or
      (del.locator_id is null and ins.locator_id is not null) or
      (del.locator_id is not null and ins.locator_id is null) or
	(del.client_id <> ins.client_id) or
      (del.client_id is null and ins.client_id is not null) or
      (del.client_id is not null and ins.client_id is null) or
    (del.investigator_id <> ins.investigator_id) or
      (del.investigator_id is null and ins.investigator_id is not null) or
      (del.investigator_id is not null and ins.investigator_id is null) or
    (del.claim_coordinator_id <> ins.claim_coordinator_id) or
      (del.claim_coordinator_id is null and ins.claim_coordinator_id is not null) or
      (del.claim_coordinator_id is not null and ins.claim_coordinator_id is null) or
    (del.disc_repair_person <> ins.disc_repair_person) or
      (del.disc_repair_person is null and ins.disc_repair_person is not null) or
      (del.disc_repair_person is not null and ins.disc_repair_person is null) or
    (del.added_by <> ins.added_by) or
      (del.added_by is null and ins.added_by is not null) or
      (del.added_by is not null and ins.added_by is null) or
	 (del.approved_by_id <> ins.approved_by_id) or
      (del.approved_by_id is null and ins.approved_by_id is not null) or
      (del.approved_by_id is not null and ins.approved_by_id is null)

  insert into damage_history
   (damage_id,
    modified_date, modified_by,
    exc_resp_code, exc_resp_type,
    uq_resp_code, uq_resp_type,
    spc_resp_code, spc_resp_type,
    profit_center, exc_resp_other_desc,
    exc_resp_details, exc_resp_response,
    uq_resp_other_desc, uq_resp_details,
    uq_resp_ess_step, spc_resp_details, reason_changed,
	/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
	locator_id,client_id, investigator_id, claim_coordinator_id, disc_repair_person,
    added_by, approved_by_id
   )
/** correct updated fields to inserted table. instead of from deleted table B.P. 1/1/2021 2:42:59 PM***/
  select ins.damage_id,
    getdate(), ins.modified_by,
    ins.exc_resp_code, ins.exc_resp_type,
    ins.uq_resp_code, ins.uq_resp_type,
    ins.spc_resp_code, ins.spc_resp_type,
    ins.profit_center, ins.exc_resp_other_desc,
    damage.exc_resp_details, damage.exc_resp_response,
    ins.uq_resp_other_desc, damage.uq_resp_details,
    ins.uq_resp_ess_step, damage.spc_resp_details, ins.reason_changed,
	/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
	ins.locator_id, ins.client_id, ins.investigator_id, ins.claim_coordinator_id, ins.disc_repair_person,
    ins.added_by, ins.approved_by_id
  from deleted d
       join damage on damage.damage_id = d.damage_id
       join inserted ins on d.damage_id = ins.damage_id
    where d.damage_id in (select damage_id from @damages)

  update damage set modified_date = getdate()
    where damage_id in (select damage_id from inserted)

  update damage set invoice_code_initial_value = invoice_code
    where damage_id in (select damage_id from inserted)
      and invoice_code_initial_value is null
      and invoice_code is not null

  delete from @damages

  /* Find damages with changed damage dates or ticket IDs */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.damage_date <> ins.damage_date) or
      (del.damage_date is null and ins.damage_date is not null) or
      (del.damage_date is not null and ins.damage_date is null) or
    (del.ticket_id <> ins.ticket_id) or
      (del.ticket_id is null and ins.ticket_id is not null) or
      (del.ticket_id is not null and ins.ticket_id is null)

  /*insert code moved to insert_i trigger*/
  /*insert into @damages select damage_id from inserted where damage_id not in (select damage_id from deleted) */

  update damage
  set accrual_date =
  dbo.MinOfFourDates(
    damage_date,
    uq_notified_date,
    coalesce((select t.due_date from ticket t where t.ticket_id = damage.ticket_id), '2100-01-01'),
    coalesce((select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N'), '2100-01-01')
  )
  where damage_id in (select damage_id from @damages)

