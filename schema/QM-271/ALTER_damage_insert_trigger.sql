USE [QM]
GO
/****** Object:  Trigger [dbo].[damage_i]  B.P. Script Date: 1/1/2021 2:43:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER trigger [dbo].[damage_i] on [dbo].[damage]
for insert
as
  set nocount on
if (select Coalesce(uq_damage_id, 0) from inserted) < 1
 begin
    declare @next_damage_id integer
  execute @next_damage_id = dbo.NextUniqueID 'NextDamageID'
 update damage set uq_damage_id = @next_damage_id, added_by =  modified_by
  where (damage_id in (select damage_id from inserted)) and modified_by is not null
 end
else
update damage set added_by = modified_by
  where damage_id in (select damage_id from inserted) and modified_by is not null

/*** This addition separates the insert script out from the update script formerly in damage_ui Date: BP: 1/1/2021 2:43:05 PM ******/
   	insert into damage_history
   (damage_id,
    modified_date, modified_by,
    exc_resp_code, exc_resp_type,
    uq_resp_code, uq_resp_type,
    spc_resp_code, spc_resp_type,
    profit_center, exc_resp_other_desc,
    exc_resp_details, exc_resp_response,
    uq_resp_other_desc, uq_resp_details,
    uq_resp_ess_step, spc_resp_details, reason_changed,
/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
	locator_id,client_id, investigator_id, claim_coordinator_id, disc_repair_person,
    added_by, approved_by_id
   )
  select ins.damage_id,
    getdate(), ins.modified_by,
    ins.exc_resp_code, ins.exc_resp_type,
    ins.uq_resp_code, ins.uq_resp_type,
    ins.spc_resp_code, ins.spc_resp_type,
    ins.profit_center, ins.exc_resp_other_desc,
    damage.exc_resp_details, damage.exc_resp_response,
    ins.uq_resp_other_desc, damage.uq_resp_details,
    ins.uq_resp_ess_step, damage.spc_resp_details, ins.reason_changed,
/****added damage_history fields B.P. 1/1/2021 2:42:59 PM ****/
	ins.locator_id, ins.client_id, ins.investigator_id, ins.claim_coordinator_id, ins.disc_repair_person,
    ins.added_by, ins.approved_by_id
  from inserted  ins
    join damage on damage.damage_id = ins.damage_id

  update damage set modified_date = getdate()
   where damage_id in (select damage_id from inserted)

  update damage set invoice_code_initial_value = invoice_code
    where damage_id in (select damage_id from inserted)
     and invoice_code_initial_value is null
   and invoice_code is not null

 update damage
 set accrual_date =  
 dbo.MinOfFourDates(
    damage_date,
   uq_notified_date,
   coalesce((select t.due_date from ticket t where t.ticket_id = damage.ticket_id), '2100-01-01'),
   coalesce((select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N'), '2100-01-01')
  )
 where damage_id in (select damage_id from inserted)
