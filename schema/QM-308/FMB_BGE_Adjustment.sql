USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[FMB_BGE_Adjustment]    Script Date: 2/6/2021 9:07:15 AM ******/
DROP PROCEDURE [dbo].[FMB_BGE_Adjustment]
GO

/****** Object:  StoredProcedure [dbo].[FMB_BGE_Adjustment]    Script Date: 2/6/2021 9:07:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FMB_BGE_Adjustment]     --QM-	-- uncomment for release
    @BillID int											-- uncomment for release
AS														-- uncomment for release
Declare
  @NewRates table (BillID Int, TicketID Int, LocateID Int, TicketNbr varChar(20), 
  TktStatus varChar(5), ClientID Int, ClientCode varChar(10), TicketRate Money, 
  Bucket varChar(60));--LineItemText varChar(60), 
Declare
  @OutputList table (ClientID Int, Output varChar(20));

Insert into @OutputList     -- extracts area name from Client Name
SELECT client_id, substring(
							client_name, 
							CharIndex('(',client_name)+1,
							CharIndex(')',client_name)-CharIndex('(',client_name)-1
							) as Output
  FROM [QM].[dbo].[client]
  where call_center='FMB1' 
  and oc_code in (Select [term]
							from [dbo].[term_group_detail] 
							where [term_group_id] in (
													SELECT [term_group_id]
														FROM [QM].[dbo].[term_group]
														where (group_code = 'FMBBGEGTerms')
														or (group_code = 'FMBBGEETerms')  
													)                    
				 )
--select * from @OutputList   --comment for release

Declare   
--@BillID int,     --comment for release
          @LocateID Int, @Status varChar(5), @ClientID Int, @ClientCode varChar(10), @LineItemText  varChar(60), @Bucket varChar(60); 
--Set @BillID =68934-- (Select max(bill_id) from billing_header)  --comment for release

Set @LineItemText= 'Comb Gas and Elec BGE';
Set @Status = 'COMB';
Set @ClientID= (select client_id from client where oc_code = 'BGE' and call_center = 'FMB1');
Set @ClientCode = 'BGE';  
Set @LocateID= 0

BEGIN TRY   --uncomment for release
  BEGIN TRANSACTION  --uncomment for release
	insert into @NewRates
	select @BillID, T.Ticket_ID, L.Locate_ID, BT.ticket_number, @Status, @ClientID, @ClientCode, 
	BR.Rate,BT.Bucket
	from billing_detail BT 
	Join [dbo].[billing_rate] BR on Substring(BR.bill_type, 0,5) = Substring(BT.Bucket, 0,5)
	inner join Ticket T on (BT.ticket_number=T.ticket_number)
	inner join Locate L on (T.ticket_id = L.ticket_id)
	where bill_id = @BillID
	and BT.area_name='Both'
	and Upper(BR.billing_cc) = 'BGE'
	and T.ticket_type NOT like '%CNCL%'
	and BR.rate is not null
	and L.client_code in   (Select [term]
							from [dbo].[term_group_detail]
							where [term_group_id] = (
							SELECT [term_group_id]
								FROM [QM].[dbo].[term_group]
								where group_code = 'FMBBGEETerms'))   
	group by bill_id, T.Ticket_ID, L.Locate_ID, BT.ticket_number, BR.Rate, BT.Bucket

	delete from billing_detail               --delete the Gas records from the on disk set
	where billing_cc in (Select [term]
							 from [dbo].[term_group_detail]
							 where [term_group_id] = (
							 SELECT [term_group_id]
								FROM [QM].[dbo].[term_group]
								where group_code = 'FMBBGEGTerms'))         
	and bill_id = @BillID
	and area_name='Both'

  --Select * from @NewRates where ticketNbr = '21000623' --Test point comment for release

	Update billing_detail  -- set the billing_details to the BGE rates
		set line_item_text=OP.Output+' '+@LineItemText, 
		Status=nr.TktStatus,
		billing_cc=nr.ClientCode,
		client_id =nr.ClientID,
		rate=nr.TicketRate,
		price=nr.TicketRate 

	From billing_detail BD
	inner join @NewRates NR on (BD.locate_id = NR.LocateID) 
	inner join @OutputList OP on (BD.client_id=OP.ClientID) 
	where bill_id = @BillID 

	Update billing_detail  -- set the billing_details to the BGE rates
	Set rate=0.0,
		price=0.0,
		Status='NC',
		bucket='No Charge  Cancel'
	From billing_detail
	where bill_id = @BillID
	and ticket_type like '%Canc%' 

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
  Select
	ERROR_NUMBER() as ErrorNumber,
	ERROR_LINE() as ErrorLine,
	ERROR_STATE() as ErrorState,
	ERROR_SEVERITY() as ErrorSeverity,
	ERROR_PROCEDURE() as ErrorProcedure,
	ERROR_MESSAGE() as ErrorMessage;

	IF (XACT_STATE()) = -1
		ROLLBACK TRANSACTION

END CATCH  



--Select * from billing_detail   --Test point comment for release
--where bill_id = @BillID   


RETURN 0 --uncomment for release

GO

