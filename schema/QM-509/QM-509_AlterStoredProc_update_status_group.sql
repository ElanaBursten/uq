USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[update_status_group]    Script Date: 1/28/2022 11:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  name = 'StatusTableType')
BEGIN
CREATE TYPE StatusTableType AS TABLE 
(statusitem varchar(5), outgoingstatus nchar(40), statusexpl nchar(20))
END
GO

ALTER proc [dbo].[update_status_group] (
	@SGID int,
	@Tab StatusTableType READONLY
)
as
  set nocount on

  -- add any that aren't in there at all
  insert into status_group_item (sg_id, status, outgoing_status, status_explanation)
  select @SGID as sg_id, 
    statusitem, outgoingstatus, statusexpl from @Tab
   --from StringListToTable(@Statuses) data
   where statusitem not in (select status 
     from status_group_item 
       where sg_id=@SGID)

  update status_group_item set outgoing_status = T.outgoingstatus, status_explanation = T.statusexpl
  from status_group_item
  join @Tab as T on T.statusitem = status_group_item.status
  and status_group_item.sg_id=@SGID

  -- make active any that are inactive
  update status_group_item set active=1
  where sg_id=@SGID
   and active=0
   and status in (select statusitem from @Tab)

  -- make inactive any that are active
  update status_group_item set active=0
  where sg_id=@SGID
   and active=1
   and status not in (select statusitem from @Tab)

select statuslist.status + ' - ' + status_name +
 case when statuslist.active=0 then ' (inactive)' else '' end as stat,
 sgi.sg_item_id, sgi.active
from statuslist
 left join status_group_item sgi
   on statuslist.status = sgi.status and sgi.sg_id=@SGID

