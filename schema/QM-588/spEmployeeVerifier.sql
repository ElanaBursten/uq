USE [QM]  --      [payroll_pc_code]
GO  --QM-588
/****** Object:  StoredProcedure [dbo].[Employee_Verifier]    Script Date: 6/12/2022 2:26:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[Employee_Verifier] (
  @check_date date)
as
SET NOCOUNT ON

select case e.type_id when 11 then 'LOC' 
					when 10 then 'SUP'
					when 1015 then 'TRN' 
					else 'OTH' end as EmpType, 

case e.Company_ID when 1 then 'UTQ'
				  when 7 then 'LOC'
				  end as Company,

e.[first_name]+' '+e.[last_name] as EmpName,
dbo.get_employee_pc(e.emp_id,1) pc, SPC.[solomon_pc] SolomonPC,
dbo.get_employee_report_to(e.emp_id) report_to_emp_id, e2.emp_number ManEmpNumber, SPC.[solomon_pc]+e2.emp_number+e.emp_number as TaskID,
e.*
from employee e 
join [dbo].[profit_center] SPC on (dbo.get_employee_pc(emp_id,1) =  SPC.[pc_code])
join employee e2 on (dbo.get_employee_report_to(e.emp_id)=e2.emp_id)
where e.modified_date>=@check_date
and e.ad_username is not null
and e.active = 1




