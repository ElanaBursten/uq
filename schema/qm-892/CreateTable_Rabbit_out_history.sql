USE [QM]
GO

/****** Object:  Table [dbo].[rabbitmq_out] qm-892    Script Date: 2/12/2024 12:09:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[rabbitmq_out_history](
	[hist_id] [int] IDENTITY(1,1) NOT NULL,
	[message_id] [int]  NOT NULL,
	[queue_name] [varchar](255) NOT NULL,
	[message_payload] [nvarchar](max) NULL,
	[insert_date] [datetime] NOT NULL,
	[source_header] [nvarchar](510) NULL,
	[message_type_header] [nvarchar](510) NULL,
	[message_id_header] [nvarchar](510) NULL,
	[publish_date_from_header] [datetime] NULL,
	[status] [varchar](20) NULL,
	[processed] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[hist_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


