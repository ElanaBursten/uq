USE [QM]
GO
/****** Object:  Trigger [dbo].[tseApprovalUpdTrigger]    Script Date: 2/12/2024 12:13:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[rabbitmq_out_Trigger]  --qm-892
ON [dbo].[rabbitmq_out]
AFTER UPDATE   
AS 
IF ( UPDATE ([processed] ) )  
BEGIN 

  insert into [dbo].[rabbitmq_out_history] ([message_id], [queue_name],[message_payload],[insert_date],[source_header],[message_type_header],
  [message_id_header],[publish_date_from_header],[status],[processed])              
  Select 
  ins.[message_id], 
  ins.[queue_name],
  ins.[message_payload],
  ins.[insert_date],
  ins.[source_header],
  ins.[message_type_header],
  ins.[message_id_header],
  ins.[publish_date_from_header],
  ins.[status],
  ins.[processed]
  From Inserted INS
  where ins.[processed] = 1

END;  
 Delete from [dbo].[rabbitmq_out] where [message_id] = (Select [message_id] from inserted where [processed] = 1)
