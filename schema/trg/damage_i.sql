--QM-339


/****** Object:  Trigger [damage_i]    Script Date: 2/13/2021 9:51:18 AM ******/
DROP TRIGGER [dbo].[damage_i]
GO

/****** Object:  Trigger [dbo].[damage_i]    Script Date: 2/13/2021 9:51:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE trigger [dbo].[damage_i] on [dbo].[damage]
for insert
as
  set nocount on
if (select Coalesce(uq_damage_id, 0) from inserted) < 1
 begin
    declare @next_damage_id integer
  execute @next_damage_id = dbo.NextUniqueID 'NextDamageID'
 update damage set uq_damage_id = @next_damage_id, added_by =  modified_by
  where (damage_id in (select damage_id from inserted)) and modified_by is not null
 end
else
update damage set added_by = modified_by
  where damage_id in (select damage_id from inserted) and modified_by is not null

  update damage set modified_date = getdate()
   where damage_id in (select damage_id from inserted)

  update damage set invoice_code_initial_value = invoice_code
    where damage_id in (select damage_id from inserted)
     and invoice_code_initial_value is null
   and invoice_code is not null

 update damage
 set accrual_date =  
 dbo.MinOfFourDates(
    damage_date,
   uq_notified_date,
   coalesce((select t.due_date from ticket t where t.ticket_id = damage.ticket_id), '2100-01-01'),
   coalesce((select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N'), '2100-01-01')
  )
 where damage_id in (select damage_id from inserted)
GO

ALTER TABLE [dbo].[damage] ENABLE TRIGGER [damage_i]
GO

