if object_id('profit_center_u') is not null
  drop trigger profit_center_u
GO

/****** Object:  Trigger [dbo].[profit_center_u]    Script Date: 10/12/2018 11:50:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[profit_center_u] on [dbo].[profit_center]
for update
AS
update profit_center set modified_date = getdate() where pc_code in (select pc_code from inserted)
--Reversal
--drop trigger [dbo].[profit_center_u] 



