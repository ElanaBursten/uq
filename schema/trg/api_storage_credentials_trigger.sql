/* 
   api_storage_credentials_iu trigger
   Created 3/2018
   QMANTWO-552, QMANTWO-553 AWS: Requires a trigger to set the modified_date 
*/

USE [QM]
GO

/****** Object:  Trigger [api_storage_credentials_iu]    Script Date: 3/14/2018 10:13:45 AM ******/
DROP TRIGGER [dbo].[api_storage_credentials_iu]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create trigger [dbo].[api_storage_credentials_iu] on [dbo].[api_storage_credentials] 
for insert, update
AS 
begin 
	if SYSTEM_USER = 'no_trigger' 
	return

	SET NOCOUNT ON
	update api_storage_credentials
	set modified_date = GetDate() 
	from inserted I 
	where api_storage_credentials.id = I.id 
end
COMMIT

GO


