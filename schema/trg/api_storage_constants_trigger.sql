/* 
   api_storage_constants_iu trigger
   Created 3/2018
   QMANTWO-552, QMANTWO-553 AWS: Requires a trigger to set the modified_date 
*/

USE [QM]
GO

DROP TRIGGER [dbo].[api_storage_constants_iu]
GO

/****** Object:  Trigger [dbo].[api_storage_constants_iu]    Script Date: 3/14/2018 10:12:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create trigger [dbo].[api_storage_constants_iu] on [dbo].[api_storage_constants] 
for insert, update
AS 
begin 
	if SYSTEM_USER = 'no_trigger' 
	return

	SET NOCOUNT ON
	update api_storage_constants
	set modified_date = GetDate() 
	from inserted I 
	where api_storage_constants.id = I.id 
end
GO


