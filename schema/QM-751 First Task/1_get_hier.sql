--QM-751 First Task (only added nolocks) 5/2023
if object_id('dbo.get_hier') is not null
	drop function dbo.get_hier
go

-- This SP is fast if you don't need active details, PC details, hierarchy path details, etc.
create function get_hier(@id int, @managers_only bit)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_report_level integer,
  h_report_to integer
)
as
begin
  declare @adding_level int
  select @adding_level = 1

  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
     h_type_id, h_report_to, h_report_level)
  select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
     e.type_id, e.report_to, @adding_level
    from employee e with (nolock)
    left join employee man 
	  on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user
  WHILE @adding_level < 10
  BEGIN
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_type_id, h_report_to, h_report_level)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.type_id, e.report_to, r.h_report_level+1
      from employee e with (nolock)
       left join reference et on e.type_id = et.ref_id
       inner join @results r on e.report_to = r.h_emp_id
            AND r.h_report_level=@adding_level
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    SELECT @adding_level = @adding_level + 1
  END
  return
end
GO


if object_id('dbo.get_hier2') is not null
	drop function dbo.get_hier2
go

create function get_hier2(@id int, @managers_only bit, @level_limit int)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_report_level integer,
  h_report_to integer
)
as
begin
  declare @adding_level int
  select @adding_level = 1

  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
     h_type_id, h_report_to, h_report_level)
  select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
     e.type_id, e.report_to, @adding_level
    from employee e with (nolock)
    left join employee man on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user
  WHILE @adding_level < @level_limit
  BEGIN
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_type_id, h_report_to, h_report_level)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.type_id, e.report_to, r.h_report_level+1
      from employee e with (nolock)
       left join reference et on e.type_id = et.ref_id
       inner join @results r on e.report_to = r.h_emp_id
            AND r.h_report_level=@adding_level
      where ((@managers_only=0) or (et.modifier like '%MGR%'))

    SELECT @adding_level = @adding_level + 1
  END
  return
end

/*
select * from get_hier(211, 1)
select * from get_hier(211, 0)
select * from get_hier(2676, 1)
select * from get_hier(2676, 0)
*/
