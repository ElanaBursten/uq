---------------------------------------
--QM-751 First Task (added est_start_time) 5/2023
--QM-695 Open Ticket List - Extended
-- Introduced to show list of tickets instead of the Open Ticket Stats
-- @RowLimit is the limit of tickets to pull down.  
--    It is set by the Q Client but uses the value in configuration_data
--EB
---------------------------------------

if object_id('dbo.get_open_ticket_list_ext') is not null
  drop procedure dbo.get_open_ticket_list_ext
go

create procedure dbo.get_open_ticket_list_ext(
  @ManagerID Int, 
  @RowLimit Int,
  @MaxDays Int,
  @toplevel bit = 0)
as


Declare @tkt TABLE(
    ticket_id integer not null,
	assigned_to integer not null
);

DECLARE @opentickets TABLE (    
    ticket_id integer not null, 
    ticket_number varchar(20) not null,
	emp_id integer not null,
	short_name varchar(30) null,
    kind varchar(20) not null,
    due_date datetime null,
	work_address_number varchar(10) null,
    work_address_street varchar(60) null, 
    work_description varchar(3500) null,
    work_type varchar(90) null,
    legal_good_thru datetime null,  
    ticket_type varchar(38) null, 
    work_city varchar(40) null,
    ticket_format varchar(20) not null,     
    work_county varchar(40) null,
    do_not_mark_before datetime null, 
    modified_date datetime not null, 
    alert varchar(1) null,
    scheduled varchar(10) null,
    start_date varchar(5) null,
    end_date varchar(5) null,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    ticket_is_visible varchar(1) not null,
    area_name varchar(40),
    route_order decimal(9,5),  
    lat decimal(9,5),
    lon decimal(9,5),
    work_state varchar(2),
	company varchar(80),   --QM-325
	con_name varchar(50),  --QM-325
	util varchar(150), --QM-306
	risk varchar(100),  --QM-386  (source + type)
	age_hours integer, --QM-551
	est_start_time datetime null  --QM-751
	); 

insert into @tkt
   select distinct top (@RowLimit) ticket_id, assigned_to
 from [locate] with (nolock)
 inner join (select h_emp_id from dbo.get_hier(@ManagerID,0)) mgremps
   on (mgremps.h_emp_id = assigned_to)
   where (closed = 0) and  (active = 1)
   order by ticket_id asc

------------------------------------------
insert into @opentickets
  select distinct
    tkt.ticket_id, 
	ticket.ticket_number,
	tkt.assigned_to,
	emp.short_name, 
    ticket.kind, 
	ticket.due_date,
	ticket.work_address_number,
    ticket.work_address_street, 
	ticket.work_description, 
	ticket.work_type,
    ticket.legal_good_thru,  
	ticket.ticket_type, 
	ticket.work_city,
    ticket.ticket_format, 	 
	ticket.work_county,
    ticket.do_not_mark_before, 
	ticket.modified_date, 
	ticket.alert,
    null as scheduled, 
	null as start_date,  
	null as end_date,  --null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(substring(r.description,0,20), 'Normal') as work_priority, 
    case when
     -- @limit < 1 or
	  (ticket.kind = 'EMERGENCY') 
	    or (ticket.Kind = 'NORMAL')
	    or (ticket.kind = 'ONGOING') 
		or (r.code='Release') then 'Y'
      else 'N'
    end as ticket_is_visible, 
	a.area_name,
	ticket.route_order, 
	ticket.work_lat, 
	ticket.work_long, 
	ticket.work_state,
	ticket.company, 
	ticket.con_name, 

--------------------------------------------------------------------------------
-------**Utility Icons STRING_AGG (All Utils for All locators)------------------
--------------------------------------------------------------------------------
    CASE
      WHEN @toplevel = 0 THEN   --Not top level node
		 (SELECT STRING_AGG(c.Utility_type, ', ') as u
          FROM locate l1 , client c WITH (NOLOCK)
          WHERE (l1.ticket_id = ticket.ticket_Id)
	      AND (c.client_id = l1.client_id)	
	      And (l1.client_id is not null))              
     END,
  --'--',

 (select dbo.get_latest_risk_score(ticket.ticket_id)),
 (select dbo.get_ticket_age_hours(ticket.ticket_id)),
 
 --QM-751 First Task (est. start)
  (select dbo.get_first_task(ticket.ticket_id, tkt.assigned_to)) as est_start_time 


  from @tkt tkt
  inner join ticket with (nolock)
    on tkt.ticket_id = ticket.ticket_id 
  inner join [locate] loc with (nolock)
    on ticket.ticket_id = loc.ticket_id
  left join employee emp with (nolock)
    on tkt.assigned_to = emp.emp_id
  left join reference r
    on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where tkt.assigned_to = loc.assigned_to

   select 'opentickets' as tname, * from @opentickets opentickets order by due_date, short_name
GO


 --set statistics Time On
 --exec get_open_ticket_list_ext 10431, 500, 20
 --select h_emp_id from dbo.get_hier(10431,0)




 
