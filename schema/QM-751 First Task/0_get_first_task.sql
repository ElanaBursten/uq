--QM-751 Manage Work Highlight FIrst Task, includes Optimization for Stored procedures and functions 5/2023
--FUNCTION: get_first_task
if object_id('dbo.get_first_task') is not null
  drop function dbo.get_first_task
GO

create function dbo.get_first_task(@ticket_id int, @locator_id int)
returns datetime
as
begin
  return (select min(task.est_start_date) 
          from task_schedule task with (nolock) 
          where (task.ticket_id = @ticket_id) 
            and (task.est_start_date >= GetDate())
			and (task.active=1) 
            and (task.emp_id = @locator_id))
end
GO

--select dbo.get_first_task(1003, 10564)
--select * from task_schedule

---select min(task.est_start_date) 
---          from task_schedule task with (nolock) 
---          where task.ticket_id = 1003 
---            and task.est_start_date >= GetDate() and task.active=1