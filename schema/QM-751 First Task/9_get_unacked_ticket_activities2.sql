--QM-751 First Task (est_start_time or est_start_date) 5/2023
--QM-551 Age
--QM-387 Risk Score
--QM-368 Update to STRING_AGG
--QM-353 Remove utils from top nodes
--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)
--QM-10 Export GPX Work Management

if object_id('dbo.get_unacked_ticket_activities2') is not null
  drop procedure dbo.get_unacked_ticket_activities2
GO

create procedure get_unacked_ticket_activities2 
  (@ManagerID Int, 
  @NumDays Int,
  @toplevel bit = 0)
as

DECLARE @activities TABLE (
  activity varchar(30) NOT NULL,
  activity_id integer NOT NULL,
  activity_date datetime NOT NULL,
  emp_id integer NOT NULL,
  short_name varchar(30) NULL,
  ticket_id integer NOT NULL,
  ticket_number varchar(20) NOT NULL,
  alert varchar(1) null,
  kind varchar(20) not null,
  ticket_type varchar(38) null, 
  due_date datetime null,
  work_address_number varchar(10) null, 
  work_address_street varchar(60) null, 
  work_city varchar(40) null,
  work_county varchar(40) null,
  work_type varchar(90) null,
  map_page varchar(20) null, 
  work_priority_sort integer not null,
  work_priority varchar(20) not null,
  ticket_is_visible varchar(1) not null,
  legal_good_thru datetime null, 
  do_not_mark_before datetime null,
  modified_date datetime null,
  est_start_time datetime null,
  area_name varchar(40),
  route_order decimal(9,5),  --QMANTWO-775 QM-10
  lat decimal(9,5), --QM-10
  lon decimal(9,5), --QM-10
  work_state varchar(2),  --QM-10
  company varchar(80),   --QM-325
  con_name varchar(50),  --QM-325
  util varchar(150),  --QM-306
  risk varchar(100),  --QM-386  (source + type)
  age_hours integer --QM-551
	);


insert into @activities
  select ack.activity_type, ack.ticket_activity_ack_id, 
    ack.activity_date, ack.locator_emp_id, loc.short_name,
    ticket.ticket_id, ticket.ticket_number, ticket.alert, ticket.kind, 
    ticket.ticket_type, ticket.due_date, ticket.work_address_number, 
    ticket.work_address_street, ticket.work_city, ticket.work_county,
    ticket.work_type, ticket.map_page, 
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    ticket.legal_good_thru, 
    ticket.do_not_mark_before,
    ticket.modified_date,
     (select dbo.get_first_task(ticket.ticket_id, ack.locator_emp_id)) as est_start_time, --QM-751 First Task (est. start)
	a.area_name,
	ticket.route_order, 
	ticket.work_lat, ticket.work_long, 
	ticket.work_state,
	ticket.company, ticket.con_name,
		
--------------------------------------------------------------------------------
-------**Utility Icons STRING_AGG (All Utils for All locators)------------------
--------------------------------------------------------------------------------
    CASE
      WHEN @toplevel = 0 THEN   --Not top level node
		 (SELECT STRING_AGG(c.Utility_type, ', ') as u
          FROM locate l1 , client c WITH (NOLOCK)
          WHERE (l1.ticket_id = ticket.ticket_Id)
	      AND (c.client_id = l1.client_id)	
	      And (l1.client_id is not null))              --QM-357 effectively removes -Ns  SR
	 ELSE ' '				 
     END,
 --------------------------------------------------------------------------------
 -----**Utility Icons SQL Server 2012 (Please do not remove)---------------------
 --------------------------------------------------------------------------------
   --          CASE
   --               WHEN @toplevel = 0 THEN   --Not top level node
			--	    isnull(STUFF 
   --                 ((
   --                   SELECT distinct ', ' + c.Utility_type
   --                   FROM client c, locate l1 WITH (NOLOCK)
   --                   WHERE (c.client_id = l1.client_id)
	  --                And (l1.ticket_id = ticket.ticket_Id)
	  --                And l1.client_id is not null  
   --                   FOR XML PATH('')), 1, 1, ' '), '-') 				    
			--	 ELSE ' '			 
			--END,
 --------------------------------------------------------------------------------
 --------------------------------------------------------------------------------

 (select dbo.get_latest_risk_score(ticket.ticket_id)), 
 (select dbo.get_ticket_age_hours(ticket.ticket_id))  --QM-551


  from ticket with (nolock)
  left join reference r 
    on r.ref_id = ticket.work_priority_id
  inner join ticket_activity_ack ack with (nolock)
    on ack.ticket_id = ticket.ticket_id
  inner join (select h_emp_id from dbo.get_hier(@ManagerID,0)) mgremps
    on mgremps.h_emp_id = ack.locator_emp_id
  inner join employee loc 
    on loc.emp_id = ack.locator_emp_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ack.has_ack = 0 
    and activity_date >= DateAdd(Day, (-1 * @NumDays), GetDate())
   

  select 'activity' as tname, * from @activities activity order by activity_date, short_name
go
--grant execute on get_unacked_ticket_activities2 to uqweb, QManagerRole

/*
exec dbo.get_unacked_ticket_activities2 2676, 1
exec dbo.get_unacked_ticket_activities2 2676, 30

exec dbo.get_unacked_ticket_activities2 8975, 30

select * from ticket_activity_ack where has_ack = 0
*/
