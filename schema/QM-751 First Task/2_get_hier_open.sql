--QM-751 First Task (only added nolocks here) 5/2023
--QM-428 Open Tickets - NEW

if object_id('dbo.get_hier_open') is not null
	drop function get_hier_open
go

create function get_hier_open(@id int, @managers_only bit, @level_limit int)
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer,
  open_tkt_count integer default 0,
  open_loc_count integer default 0
)
as
begin
  declare @adding_level int
  select @adding_level = 1

 
  DECLARE @open_tkt_counts TABLE (
  emp_id integer NOT NULL,
  ticket_count integer default 0,
  loc_count integer default 0
  )


  DECLARE @temp TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer
)

  INSERT INTO @temp
  Select * from get_hier2_active(@ID, @managers_only, @level_limit)

  insert into @open_tkt_counts (emp_id, ticket_count, loc_count)
  (select distinct e.emp_id, COALESCE(count(distinct tkt.ticket_id), 0), COALESCE(count(distinct loc.locate_id),0)
  from @temp e
  inner join [locate] loc with (nolock)
    on e.emp_id = loc.assigned_to
  inner join ticket tkt with (nolock)
    on tkt.ticket_id = loc.ticket_id
  where (loc.closed = 0) and
  --where due_date >=DATEADD(day, DATEDIFF(day,0,GETDATE()),0) 
  --   AND due_date < DATEADD(day, DATEDIFF(day,0,GETDATE())+1,0) and
  (loc.active = 1) and (loc.status <> '-N') and (loc.status <> '-P')
  group by e.emp_id)

  insert into @results(
  emp_id,short_name,first_name, last_name,
  emp_number, etype_id, report_level,
  report_to, open_tkt_count, open_loc_count)
  
  select e.emp_id, e.short_name,e.first_name, e.last_name,
  e.emp_number, e.etype_id, e.report_level,
  e.report_to, opt.ticket_count, opt.loc_count
  from @temp e
   left outer join @open_tkt_counts opt on e.emp_id =opt.emp_id

  RETURN
end

GO




--select * from get_hier_open (1005,0, 10)
--select * from employee where first_name = 'Keith'



