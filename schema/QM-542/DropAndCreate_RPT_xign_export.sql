USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[RPT_xign_export]    Script Date: 2/23/2022 12:26:38 PM ******/
DROP PROCEDURE [dbo].[RPT_xign_export]
GO

/****** Object:  StoredProcedure [dbo].[RPT_xign_export]    Script Date: 2/23/2022 12:26:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RPT_xign_export] (
  @OutputConfigID int,    
  @BillID int,
  @CustomerName varChar(40)  --QM-542 sr
)
AS

set nocount on
declare @CustomerID integer

select @CustomerID = customer_id
from billing_output_config
where output_config_id = @OutputConfigID

select 
  case c.locating_company 
    when 1 then 'CCC83458' 
    when 2 then 'CCC150240'
  end SenderISA,
  case c.locating_company 
    when 1 then 'UtiliQuest'
    when 2 then 'STS'
  end RemitToName,
  case c.locating_company
    when 1 then 'PO Box 932367'
    when 2 then 'PO Box 860788'
  end RemitToAddress1,
  null as RemitToAddress2,
  'PRIMARY' as RemitToCode,
  case c.locating_company
    when 1 then 'Atlanta'
    when 2 then 'Orlando'
  end RemitToCity,	 
  case c.locating_company
    when 1 then 'GA'
    when 2 then 'FL'
  end RemitToState, 
  case c.locating_company
    when 1 then '31193-2367'
    when 2 then '32886-0788'
  end RemitToPostalCode,
  '617215074' as BillToISA,
  '"Comcast Cable Communications, LLC"' as BillToName,
  '1701 John F. Kennedy Blvd' as BillToAddress1,
  null as BillToAddress2,
  null as BillToAddress3,
  'Philadelphia' as BillToCity,
  'PA' as BillToState,
  '19103' as BillToPostalCode,
  'US' as BillToCountry,
  null as ShipToName,
  null as ShipToAddress1,
  null as ShipToAddress2,
  null as ShipToAddress3,
  null as ShipToCity,
  null as ShipToState,
  null as ShipToPostalCode,
  null as ShipToCountry,
  coalesce(convert(varchar, bi.invoice_id), 'UNCOMMITED') as InvoiceNumber,
  bi.invoice_date as InvoiceDate,
  0 as DiscountPercent,
  0 as DiscountDayIncrement,
  45 as NetDayIncrement,
  null as PaymentDueDate,
  c.contact_name as RequesterName,
  c.contact_email as RequesterEMail,
  null as "ExpensePO#",
  'USD' as CurrencyCode,
  null as CustomerAccountNumber,
  null as OriginalInvoice,
  'Details are attached' as Comments,
  li.liid as InvoiceLineNumber,
  li.billing_cc as 'Buyer Item / Service #',
  li.line_item_text as Description,
  bh.bill_start_date as ServiceStartDate,
  bh.bill_end_date as 'ServiceEnd / Goods Delivery Date',
  case
    when li.UnitsCharged > 0 then li.UnitsCharged
    when li.UnitsCharged = 0 then li.hours
  end as Quantity,
  'EA' as UOM,
  Rate as UnitPrice,
  case 
    when li.UnitsCharged > 0 then (li.UnitsCharged * Rate)
    when li.UnitsCharged = 0 then (li.hours * Rate)
  end as ExtendedPrice,
  coalesce(bi.total_amount, 0.00) as Subtotal, 
  0.00 as Freight, 
  0.00 as Tax,
  coalesce(bi.total_amount, 0.00) as GrandTotal,
  null as CreditMemoIndicator,
  @CustomerName as AttachmentFileName  --qm-542
from billing_header bh 
cross join dbo.invoice_line_items(@OutputConfigID, @BillID) li
inner join customer c on c.customer_id = @CustomerID
inner join term_group_detail tgd on li.billing_cc = tgd.term and tgd.active = 1
inner join term_group tg on tgd.term_group_id = tg.term_group_id and tg.active = 1
left join billing_invoice bi on bi.billing_header_id = bh.bill_id and bi.customer_id = @CustomerID 
where bh.bill_id = @BillID 
  and tg.group_code = 'TermsNeedingXignExport'
order by bi.invoice_id, liid

GO

