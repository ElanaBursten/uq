USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[IUD_QtrMinute]    Script Date: 11/14/2022 11:34:09 AM ******/
DROP PROCEDURE [dbo].[IUD_QtrMinute]
GO

/****** Object:  StoredProcedure [dbo].[IUD_QtrMinute]    Script Date: 11/14/2022 11:34:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
-- ============================================= 
-- Author:		Larry Killen 
-- Create date: 10/12/2015 
-- Description:	Manages Inserts, Updates and Deletions of QtrMinutes 
-- ============================================= 
CREATE PROCEDURE [dbo].[IUD_QtrMinute]  
	-- Add the parameters for the stored procedure here 
	@qtrminlat varchar(5), 
	@qtrminlng varchar(6), 
	@callCenter varchar(20), 
	@areaId int 
 
AS 
BEGIN 
--begin tran; 
	merge [dbo].[map_qtrmin] as target 
	using (values(@qtrminlat, @qtrminlng, @areaId,  @callCenter)) 
		as source ([qtrmin_lat],  [qtrmin_long], [area_id], [call_center]) 
		on (target.qtrmin_lat = @qtrminlat) and (target.qtrmin_long = @qtrminlng) --and (target.call_center = @callCenter) 
	when  matched and @areaId = 0 
		then delete 
	when matched then 
			update 
			set [area_id] = @areaId  
	when not matched and (@qtrminlat<>'') or (@qtrminlng <> '') then --qm-553
	insert([qtrmin_lat],  [qtrmin_long], [area_id], [long_length], [call_center]) 
	values(source.qtrmin_lat,  source.qtrmin_long, source.area_id, 1, source.call_center); 
 
--OUTPUT $action, inserted.*, deleted.*;		   
--rollback tran; 
END 
 
GO

