USE [QM]
GO

/****** Object:  Trigger [dbo].[employee_iu]    Script Date: 9/7/2019 8:38:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  --CHG0050558
ALTER trigger [dbo].[employee_iu] on [dbo].[employee]  --CHG0050558
for insert, update  --CHG0050558
not for replication  --CHG0050558
as  --CHG0050558
begin  --CHG0050558
  set nocount on  --CHG0050558
  --CHG0050558
  declare @ChangeDate datetime  --CHG0050558
  set @ChangeDate = getdate()  --CHG0050558
  --CHG0050558
  -- Don't update modified date or add history for rights_midified_date changes alone  --CHG0050558
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)  --CHG0050558
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)  --CHG0050558
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)  --CHG0050558
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)  --CHG0050558
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)  --CHG0050558
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)  --CHG0050558
    or update(incentive_pay) or update(contact_phone) or update(local_utc_bias) or update(ad_username) or update(test_com_login) --CHG0050558
	or update(adp_login) or update(home_lat) or update(home_lng) or update(alt_lat) or update(alt_lng)))  --CHG0050558
  begin  --CHG0050558
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)  --CHG0050558
  --CHG0050558
    if update(report_to) or update(repr_pc_code)  --CHG0050558
    begin -- This change may modify the effective PC of non-updated employees  --CHG0050558
      -- Recalculate and store any changes to everyone below these employees  --CHG0050558
  --CHG0050558
      declare @AllEmps TABLE (  --CHG0050558
        emp_id integer NOT NULL PRIMARY KEY,  --CHG0050558
        x integer,  --CHG0050558
        emp_pc_code varchar(15) NULL  --CHG0050558
      )  --CHG0050558
  --CHG0050558
      insert into @AllEmps select * from employee_pc_data(null)  --CHG0050558
  --CHG0050558
      declare @ChangedEmps TABLE (  --CHG0050558
        emp_id integer NOT NULL PRIMARY KEY  --CHG0050558
      )  --CHG0050558
  --CHG0050558
      insert into @ChangedEmps  --CHG0050558
        select emps.emp_id  --CHG0050558
        from @AllEmps emps  --CHG0050558
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate  --CHG0050558
        where ((emps.emp_pc_code <> eh.active_pc)  --CHG0050558
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))  --CHG0050558
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))  --CHG0050558
          or emps.emp_id in (select emp_id from inserted)  --CHG0050558
  --CHG0050558
      -- "Deactivate" any current history records  --CHG0050558
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)  --CHG0050558
  --CHG0050558
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  --CHG0050558
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  --CHG0050558
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  --CHG0050558
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  --CHG0050558
        crew_num, autoclose, company_id, rights_modified_date,  --CHG0050558
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  --CHG0050558
        local_utc_bias,ad_username,test_com_login,adp_login,  --CHG0050558
		home_lat,home_lng, alt_lat, alt_lng, changeby --  QMANTWO-748  CHG0050558
      )  --CHG0050558
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,  --CHG0050558
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  --CHG0050558
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  --CHG0050558
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  --CHG0050558
        crew_num, autoclose, company_id, rights_modified_date,  --CHG0050558
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  --CHG0050558
        local_utc_bias,ad_username,test_com_login,adp_login,  --CHG0050558
        home_lat,home_lng, alt_lat, alt_lng, changeby   --  QMANTWO-748  CHG0050558  
	  from employee e  --CHG0050558
        inner join @AllEmps emps on emps.emp_id = e.emp_id  --CHG0050558
      where e.emp_id in (select emp_id from @ChangedEmps)  --CHG0050558
    end  --CHG0050558
    else -- This change only affects the exact employee(s) being modified  --CHG0050558
    begin  --CHG0050558
      -- "Deactivate" any current history records  --CHG0050558
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)  --CHG0050558
  --CHG0050558
      -- Record the new/current employee data  --CHG0050558
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  --CHG0050558
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  --CHG0050558
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  --CHG0050558
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  --CHG0050558
        crew_num, autoclose, company_id, rights_modified_date,  --CHG0050558
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  --CHG0050558
        local_utc_bias,ad_username,test_com_login,adp_login,  --CHG0050558
		home_lat,home_lng, alt_lat, alt_lng, changeby --  QMANTWO-748  CHG0050558
      )  --CHG0050558
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,  --CHG0050558
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  --CHG0050558
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  --CHG0050558
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  --CHG0050558
        crew_num, autoclose, company_id, rights_modified_date,  --CHG0050558
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets,  --CHG0050558
        incentive_pay, contact_phone, local_utc_bias,ad_username,test_com_login,adp_login,  --CHG0050558
		home_lat,home_lng, alt_lat, alt_lng, changeby --  QMANTWO-748  CHG0050558
      from inserted  --CHG0050558
    end  --CHG0050558
  end  --CHG0050558
end  --CHG0050558

GO


