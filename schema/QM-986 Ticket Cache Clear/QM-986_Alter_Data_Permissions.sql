--QM-986  Permissions: Perform daily Ticket Data Clean
--Tickets - Cache AutoClear

if not exists(select * from right_definition where entity_data='TicketsCacheAutoClear')
INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc]
           ,[modified_date])
     VALUES
           ('Tickets - Cache Auto Clear'
           ,'General'
           ,'TicketsCacheAutoClear'
           ,'Limitation does not apply to this right.'
           ,GetDate())
GO
