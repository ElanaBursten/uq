USE [TestDB]
GO
/****** Object:  Table [dbo].[hal]    Script Date: 4/4/2022 2:31:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hal](
	[hal_id] [int] IDENTITY(1,1) NOT NULL,
	[hal_module] [varchar](20) NOT NULL,
	[daysback] [int] NOT NULL,
	[emails] [varchar](200) NOT NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[hal] ON 

INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (1, N'attachments', 7, N'james.booher@utiliquest.com;frank.straub@utiliquest.com;phil.torreele@utiliquest.com;emil.gasparian@utiliquest.com', 1, CAST(0x0000AE6A00D9FA04 AS DateTime))
INSERT [dbo].[hal] ([hal_id], [hal_module], [daysback], [emails], [active], [modified_date]) VALUES (2, N'assigned_tickets', 7, N'james.booher@utiliquest.com;frank.straub@utiliquest.com;phil.torreele@utiliquest.com;emil.gasparian@utiliquest.com', 1, CAST(0x0000AE6D0024746A AS DateTime))
SET IDENTITY_INSERT [dbo].[hal] OFF
ALTER TABLE [dbo].[hal] ADD  CONSTRAINT [DF_hal_active]  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[hal] ADD  CONSTRAINT [DF_hal_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO
