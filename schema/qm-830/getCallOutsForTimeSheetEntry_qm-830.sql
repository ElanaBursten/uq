USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[getCallOutsForTimeSheetEntry]    Script Date: 4/5/2024 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		xqTrix
-- Create date: 10/9/2023 modified 4/5/2024  --qm-830
-- Description:	Returns Callout starts and stops for one timesheet entry_id
-- =============================================
ALTER FUNCTION [dbo].[getCallOutsForTimeSheetEntry] 
(
	-- Add the parameters for the function here
	@TSE_EntryID int,
	@WorkDate DateTime 
)
RETURNS 
@Return TABLE 
(
	-- Add the column definitions for the TABLE variable here

	CalloutStart DateTime, 
	CalloutStop DateTime,
	Hours Decimal(6,2),
	TypeTime varchar(7)
)
AS
BEGIN         -- ,[work_date]+2+[callout_start1] as Fulldate
	-- Fill the table variable with the rows for your result set
Insert into @Return 
Select @WorkDate+2+callout_start1, @WorkDate+2+callout_stop1, DateDiff(n, @WorkDate+2+callout_start1, @WorkDate+2+callout_stop1)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop1 is not null
  and entry_id =@TSE_EntryID
  and callout_start1<callout_stop1  --qm-830

	Insert into @Return 
	Select @WorkDate+2+callout_stop1, @WorkDate+2+callout_start2, DateDiff(n, @WorkDate+2+callout_stop1, @WorkDate+2+callout_start2)/60.00  as Hours, 'Break' 
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start2 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop1 is not null  --qm-830
	  and callout_start2>callout_stop1  --qm-830


Insert into @Return 
Select @WorkDate+2+callout_start2, @WorkDate+2+callout_stop2, DateDiff(n, @WorkDate+2+callout_start2, @WorkDate+2+callout_stop2)/60.00  as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop2 is not null
  and entry_id =@TSE_EntryID
  and callout_start2<callout_stop2--qm-830  

	Insert into @Return 
	Select @WorkDate+2+callout_stop2, @WorkDate+2+callout_start3, DateDiff(n, @WorkDate+2+callout_stop2, @WorkDate+2+callout_start3)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start3 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop2 is not null  --qm-830
      and callout_start3>callout_stop2--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start3, @WorkDate+2+callout_stop3, DateDiff(n, @WorkDate+2+callout_start3, @WorkDate+2+callout_stop3)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop3 is not null
  and entry_id =@TSE_EntryID
  and callout_start3<callout_stop3--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop3, @WorkDate+2+callout_start4, DateDiff(n, callout_stop3, callout_start4)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start4 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop3 is not null  --qm-830
      and callout_start4>callout_stop3--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start4, @WorkDate+2+callout_stop4, DateDiff(n, callout_start4, callout_stop4)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop4 is not null
  and entry_id =@TSE_EntryID
  and callout_start4<callout_stop4--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop4, @WorkDate+2+callout_start5, DateDiff(n, callout_stop4, callout_start5)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start5 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop4 is not null  --qm-830
      and callout_start5>callout_stop4--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start5, @WorkDate+2+callout_stop5, DateDiff(n, callout_start5, callout_stop5)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop5 is not null
  and entry_id =@TSE_EntryID
  and callout_start5<callout_stop5--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop5, @WorkDate+2+callout_start6, DateDiff(n, callout_stop5, callout_start6)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start6 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop5 is not null  --qm-830
	  and callout_start6>callout_stop5--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start6, @WorkDate+2+callout_stop6, DateDiff(n, callout_start6, callout_stop6)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop6 is not null
  and entry_id =@TSE_EntryID
  and callout_start6<callout_stop6--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop6, @WorkDate+2+callout_start7, DateDiff(n, callout_stop6, callout_start7)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start7 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop6 is not null  --qm-830
	  and callout_start7>callout_stop6--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start7, @WorkDate+2+callout_stop7, DateDiff(n, callout_start7, callout_stop7)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop7 is not null
  and entry_id =@TSE_EntryID
  and callout_start7<callout_stop7--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop7, @WorkDate+2+callout_start8, DateDiff(n, callout_stop7, callout_start8)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start8 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop7 is not null  --qm-830
	  and callout_start8>callout_stop7--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start8, @WorkDate+2+callout_stop8, DateDiff(n, callout_start8, callout_stop8)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop8 is not null
  and entry_id =@TSE_EntryID
  and callout_start8<callout_stop8--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop8, @WorkDate+2+callout_start9, DateDiff(n, callout_stop8, callout_start9)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start9 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop8 is not null  --qm-830
      and callout_start9>callout_stop8--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start9, @WorkDate+2+callout_stop9, DateDiff(n, callout_start9, callout_stop9)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop9 is not null
  and entry_id =@TSE_EntryID
  and callout_start9<callout_stop9--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop9, @WorkDate+2+callout_start10, DateDiff(n, callout_stop9, callout_start10)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start10 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop9 is not null  --qm-830
      and callout_start10>callout_stop9--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start10, @WorkDate+2+callout_stop10, DateDiff(n, callout_start10, callout_stop10)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop10 is not null
  and entry_id =@TSE_EntryID
  and callout_start10<callout_stop10--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop10, @WorkDate+2+callout_start10, DateDiff(n, callout_stop10, callout_start11)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start11 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop10 is not null  --qm-830
	  and callout_start11>callout_stop10--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start11, @WorkDate+2+callout_stop11, DateDiff(n, callout_start11, callout_stop11)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop11 is not null
  and entry_id =@TSE_EntryID
  and callout_start11<callout_stop11--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop11, @WorkDate+2+callout_start11, DateDiff(n, callout_stop11, callout_start12)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start12 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop11 is not null  --qm-830
	  and callout_start12>callout_stop11--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start12, @WorkDate+2+callout_stop12, DateDiff(n, callout_start12, callout_stop12)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop12 is not null
  and entry_id =@TSE_EntryID
  and callout_start12<callout_stop12--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop12, @WorkDate+2+callout_start12, DateDiff(n, callout_stop12, callout_start13)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start13 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop12 is not null  --qm-830
      and callout_start13>callout_stop12--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start13, @WorkDate+2+callout_stop13, DateDiff(n, callout_start13, callout_stop13)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop13 is not null
  and entry_id =@TSE_EntryID
  and callout_start13<callout_stop13--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop13, @WorkDate+2+callout_start13, DateDiff(n, callout_stop13, callout_start14)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start14 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop13 is not null  --qm-830
	  and callout_start14>callout_stop13--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start14, @WorkDate+2+callout_stop14, DateDiff(n, callout_start14, callout_stop14)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop14 is not null
  and entry_id =@TSE_EntryID
  and callout_start14<callout_stop14	--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop14, @WorkDate+2+callout_start14, DateDiff(n, callout_stop14, callout_start15)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start15 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop14 is not null  --qm-830
	  and callout_start15>callout_stop14--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start15, @WorkDate+2+callout_stop15, DateDiff(n, callout_start15, callout_stop15)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop15 is not null
  and entry_id =@TSE_EntryID
  and callout_start15<callout_stop15	--qm-830  

	Insert into @Return --
	Select @WorkDate+2+callout_stop15, @WorkDate+2+callout_start15, DateDiff(n, callout_stop15, callout_start16)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where callout_start16 is not null
	  and entry_id =@TSE_EntryID
	  and callout_stop15 is not null  --qm-830
      and callout_start16>callout_stop15	--qm-830  

Insert into @Return 
Select @WorkDate+2+callout_start16, @WorkDate+2+callout_stop16, DateDiff(n, callout_start16, callout_stop16)/60.00 as Hours, 'Callout'
  FROM [QM].[dbo].[timesheet_entry] 
  where callout_stop16 is not null
  and entry_id =@TSE_EntryID
  and callout_start16<callout_stop16--qm-830  	


RETURN 
END
