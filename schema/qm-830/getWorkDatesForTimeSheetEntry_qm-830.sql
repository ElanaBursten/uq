USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[getWorkDatesForTimeSheetEntry]    Script Date: 4/5/2024 4:35:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		xqTrix
-- Create date: 10/9/2023 modified 4/5/2024  --qm-830
-- Description:	Returns Work starts and stops for one timesheet entry_id
-- =============================================
ALTER FUNCTION [dbo].[getWorkDatesForTimeSheetEntry] 
(
	-- Add the parameters for the function here
	@TSE_EntryID int,
	@WorkDate DateTime 
)
RETURNS 
@Return TABLE 
(
	-- Add the column definitions for the TABLE variable here

	WorkStart DateTime, 
	WorkStop DateTime,
	Hours Decimal(6,2),
	TypeTime varchar(7)
)
AS
BEGIN         -- ,[work_date]+2+[work_start1] as Fulldate
	-- Fill the table variable with the rows for your result set
Insert into @Return --1
Select @WorkDate+2+Work_Start1, @WorkDate+2+Work_Stop1, DateDiff(n, @WorkDate+2+Work_Start1, @WorkDate+2+Work_Stop1)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop1 is not null
  and entry_id =@TSE_EntryID
  and Work_Start1<Work_Stop1 --qm-830  

	Insert into @Return --1 
	Select @WorkDate+2+Work_Stop1, @WorkDate+2+Work_Start2, DateDiff(n, @WorkDate+2+Work_Stop1, @WorkDate+2+Work_Start2)/60.00  as Hours, 'Break' 
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start2 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop1 is not null --qm-830  
	  and Work_Start2>Work_Stop1 --qm-830 

Insert into @Return  --2
Select @WorkDate+2+Work_Start2, @WorkDate+2+Work_Stop2, DateDiff(n, @WorkDate+2+Work_Start2, @WorkDate+2+Work_Stop2)/60.00  as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop2 is not null
  and entry_id =@TSE_EntryID
  and Work_Start2<Work_Stop2 --qm-830  

	Insert into @Return --2 
	Select @WorkDate+2+Work_Stop2, @WorkDate+2+Work_Start3, DateDiff(n, @WorkDate+2+Work_Stop2, @WorkDate+2+Work_Start3)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start3 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop2 is not null --qm-830 
	  and Work_Start3>Work_Stop2 --qm-830 

Insert into @Return --3 
Select @WorkDate+2+Work_Start3, @WorkDate+2+Work_Stop3, DateDiff(n, @WorkDate+2+Work_Start3, @WorkDate+2+Work_Stop3)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop3 is not null
  and entry_id =@TSE_EntryID
  and Work_Start3<Work_Stop3 --qm-830  

	Insert into @Return --3
	Select @WorkDate+2+Work_Stop3, @WorkDate+2+Work_Start4, DateDiff(n, @WorkDate+2+Work_Stop3, @WorkDate+2+Work_Start4)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start4 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop3 is not null --qm-830 
	  and Work_Start4>Work_Stop3  --qm-830 

Insert into @Return --4 
Select @WorkDate+2+Work_Start4, @WorkDate+2+Work_Stop4, DateDiff(n, @WorkDate+2+Work_Start4, @WorkDate+2+Work_Stop4)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop4 is not null
  and entry_id =@TSE_EntryID
  and Work_Start4<Work_Stop4 --qm-830  

	Insert into @Return --4
	Select @WorkDate+2+Work_Stop4, @WorkDate+2+Work_Start5, DateDiff(n, @WorkDate+2+Work_Stop4, @WorkDate+2+Work_Start5)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start5 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop4 is not null --qm-830 
	  and Work_Start5>Work_Stop4  --qm-830    

Insert into @Return --5 
Select @WorkDate+2+Work_Start5, @WorkDate+2+Work_Stop5, DateDiff(n, @WorkDate+2+Work_Start5, @WorkDate+2+Work_Stop5)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop5 is not null
  and entry_id =@TSE_EntryID
  and Work_Start5<Work_Stop5 --qm-830 

	Insert into @Return --5
	Select @WorkDate+2+Work_Stop5, @WorkDate+2+Work_Start6, DateDiff(n, @WorkDate+2+Work_Stop5, @WorkDate+2+Work_Start6)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start6 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop5 is not null --qm-830 
	  and Work_Start6>Work_Stop5 --qm-830   

Insert into @Return  --6
Select @WorkDate+2+Work_Start6, @WorkDate+2+Work_Stop6, DateDiff(n, @WorkDate+2+Work_Start6, @WorkDate+2+Work_Stop6)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop6 is not null
  and entry_id =@TSE_EntryID
  and Work_Start6<Work_Stop6 --qm-830 

	Insert into @Return --6
	Select @WorkDate+2+Work_Stop6, @WorkDate+2+Work_Start7, DateDiff(n, @WorkDate+2+Work_Stop6, @WorkDate+2+Work_Start7)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start7 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop6 is not null --qm-830 
	  and Work_Start7>Work_Stop6 --qm-830     

Insert into @Return  --7
Select @WorkDate+2+Work_Start7, @WorkDate+2+Work_Stop7, DateDiff(n, @WorkDate+2+Work_Start7, @WorkDate+2+Work_Stop7)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop7 is not null
  and entry_id =@TSE_EntryID
  and Work_Start7<Work_Stop7 --qm-830 

	Insert into @Return --7
	Select @WorkDate+2+Work_Stop7, @WorkDate+2+Work_Start8, DateDiff(n, @WorkDate+2+Work_Stop7, @WorkDate+2+Work_Start8)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start8 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop7 is not null --qm-830 
	  and Work_Start8>Work_Stop7  

Insert into @Return --8 
Select @WorkDate+2+Work_Start8, @WorkDate+2+Work_Stop8, DateDiff(n, @WorkDate+2+Work_Start8, @WorkDate+2+Work_Stop8)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop8 is not null
  and entry_id =@TSE_EntryID
  and Work_Start8<Work_Stop8 --qm-830 

	Insert into @Return --8
	Select @WorkDate+2+Work_Stop8, @WorkDate+2+Work_Start9, DateDiff(n, @WorkDate+2+Work_Stop8, @WorkDate+2+Work_Start9)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start9 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop8 is not null --qm-830 
	  and Work_Start9>Work_Stop8   

Insert into @Return  --9
Select @WorkDate+2+Work_Start9, @WorkDate+2+Work_Stop9, DateDiff(n, @WorkDate+2+Work_Start9, @WorkDate+2+Work_Stop9)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop9 is not null
  and entry_id =@TSE_EntryID
  and Work_Start9<Work_Stop9 --qm-830 

	Insert into @Return --9
	Select @WorkDate+2+Work_Stop9, @WorkDate+2+Work_Start10, DateDiff(n, @WorkDate+2+Work_Stop9, @WorkDate+2+Work_Start10)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start10 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop9 is not null --qm-830  
	  and Work_Start10>Work_Stop9  

Insert into @Return --10 
Select @WorkDate+2+Work_Start10, @WorkDate+2+Work_Stop10, DateDiff(n, @WorkDate+2+Work_Start10, @WorkDate+2+Work_Stop10)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop10 is not null
  and entry_id =@TSE_EntryID
  and Work_Start10<Work_Stop10 --qm-830 

	Insert into @Return --10
	Select @WorkDate+2+Work_Stop10, @WorkDate+2+Work_Start10, DateDiff(n, @WorkDate+2+Work_Stop10, @WorkDate+2+Work_Start11)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start11 is not null
	  and entry_id =@TSE_EntryID
      and Work_Stop10 is not null --qm-830  
	  and Work_Start11>Work_Stop10 

Insert into @Return  --11
Select @WorkDate+2+Work_Start11, @WorkDate+2+Work_Stop11, DateDiff(n, @WorkDate+2+Work_Start11, @WorkDate+2+Work_Stop11)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop11 is not null
  and entry_id =@TSE_EntryID
  and Work_Start11<Work_Stop11 --qm-830 

	Insert into @Return --11
	Select @WorkDate+2+Work_Stop11, @WorkDate+2+Work_Start11, DateDiff(n, @WorkDate+2+Work_Stop11, @WorkDate+2+Work_Start12)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start12 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop11 is not null --qm-830 
	  and Work_Start12>Work_Stop11  


Insert into @Return  --12
Select @WorkDate+2+Work_Start12, @WorkDate+2+Work_Stop12, DateDiff(n, @WorkDate+2+Work_Start12, @WorkDate+2+Work_Stop12)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop12 is not null
  and entry_id =@TSE_EntryID
  and Work_Start12<Work_Stop12 --qm-830 


	Insert into @Return --12
	Select @WorkDate+2+Work_Stop12, @WorkDate+2+Work_Start12, DateDiff(n, @WorkDate+2+Work_Stop12, @WorkDate+2+Work_Start13)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start13 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop12 is not null --qm-830  
	  and Work_Start13>Work_Stop12 


Insert into @Return  --13
Select @WorkDate+2+Work_Start13, @WorkDate+2+Work_Stop13, DateDiff(n, @WorkDate+2+Work_Start13, @WorkDate+2+Work_Stop13)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop13 is not null
  and entry_id =@TSE_EntryID
  and Work_Start13<Work_Stop13 --qm-830 


	Insert into @Return --13
	Select @WorkDate+2+Work_Stop13, @WorkDate+2+Work_Start13, DateDiff(n, @WorkDate+2+Work_Stop13, @WorkDate+2+Work_Start14)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start14 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop13 is not null --qm-830 
	  and Work_Start14>Work_Stop13   


Insert into @Return  --14
Select @WorkDate+2+Work_Start14, @WorkDate+2+Work_Stop14, DateDiff(n, @WorkDate+2+Work_Start14, @WorkDate+2+Work_Stop14)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop14 is not null
  and entry_id =@TSE_EntryID
  and Work_Start14<Work_Stop14 --qm-830 


	Insert into @Return --14
	Select @WorkDate+2+Work_Stop14, @WorkDate+2+Work_Start14, DateDiff(n, @WorkDate+2+Work_Stop14, @WorkDate+2+Work_Start15)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start15 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop14 is not null --qm-830   
	  and Work_Start15>Work_Stop14 --qm-830  


Insert into @Return  --15
Select @WorkDate+2+Work_Start15, @WorkDate+2+Work_Stop15, DateDiff(n, @WorkDate+2+Work_Start15, @WorkDate+2+Work_Stop15)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop15 is not null
  and entry_id =@TSE_EntryID
  and Work_Start15<Work_Stop15 --qm-830 
  

	Insert into @Return --15
	Select @WorkDate+2+Work_Stop15, @WorkDate+2+Work_Start15, DateDiff(n, @WorkDate+2+Work_Stop15, @WorkDate+2+Work_Start16)/60.00 as Hours, 'Break'
	  FROM [QM].[dbo].[timesheet_entry] 
	  where Work_Start16 is not null
	  and entry_id =@TSE_EntryID
	  and Work_Stop15 is not null --qm-830  
	  and Work_Start16>Work_Stop15 --qm-830  


Insert into @Return  --16
Select @WorkDate+2+Work_Start16, @WorkDate+2+Work_Stop16, DateDiff(n, @WorkDate+2+Work_Start16, @WorkDate+2+Work_Stop16)/60.00 as Hours, 'Regular'
  FROM [QM].[dbo].[timesheet_entry] 
  where Work_Stop16 is not null
  and entry_id =@TSE_EntryID
  and Work_Start16<Work_Stop16 --qm-830 	


RETURN 
END
