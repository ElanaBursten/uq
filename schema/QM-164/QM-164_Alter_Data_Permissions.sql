--QM-164  Plat Mgmt in QManager Client 6/2020
--QM-164 - Adds Permissions for use of Plat Mgmt


if not exists(select * from right_definition where entity_data='PlatsManagerUpdateAll')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Plats Manager - Update All', 'General', 'PlatsManagerUpdateAll', 'Limitation does not apply to this right', GetDate());
GO

select * from right_definition order by right_description

