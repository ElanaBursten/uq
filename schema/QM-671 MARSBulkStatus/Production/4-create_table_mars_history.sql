-----------------------------------------------
-- QM- MARS Bulk Status HISTORY
-- EB 1/27/2023
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'mars_history')
Create Table mars_history(
mars_id int NOT NULL,
tree_emp_id int NOT NULL,
new_status varchar(5) NOT NULL, 
insert_date datetime NOT NULL,
inserted_by int NOT NULL,
active bit
)
GO 


IF NOT EXISTS (select * from sys.indexes where object_id = object_id('mars_history_id_indx')) 
  create index mars_history_id_indx on mars_history(mars_id)
GO

-- Note: This table is updated when a record is reomved from mars table

--drop table mars_history
--select * from mars_history