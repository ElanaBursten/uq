-----------------------------------------------
-- QM- MARS Bulk Status 
-- mars_id	tree_emp_id (of click action)
-- EB 1/3/2023
-- Updated repository 1/25/2023
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'mars')
Create Table mars(
mars_id int identity(1,1) Primary Key NOT NULL,
tree_emp_id int NOT NULL,
new_status varchar(5) NOT NULL, 
insert_date datetime NOT NULL,
inserted_by int NOT NULL,
active bit Default 1
)
GO 

-- Note: This table is treated as a Request Queue.  As the records are processed, they are deleted
--       If a request is "stopped", the inactive flag is set to 0.  The client will treat records
--       with active = 0 and display as "STOPPED".
--       0 flags should be removed periodically.

--drop table mars
--select * from mars