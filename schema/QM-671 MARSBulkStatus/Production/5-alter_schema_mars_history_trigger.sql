-----------------------------------------------
-- QM- MARS Bulk Status
-- Delete Trigger: Saves a copy of the Mars record in mars_history when deleted from MARS
-- EB 1/27/2023
-----------------------------------------------

if exists(select name FROM sysobjects where name = 'mars_d' AND type = 'TR')
  DROP TRIGGER mars_d
GO
create trigger mars_d
on mars
for delete
as
begin
  insert into mars_history
         select mars_id,tree_emp_id,new_status,insert_date,inserted_by,active 
	     from deleted 
end
GO


