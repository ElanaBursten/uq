------------------------------------------------
--QM-671 Bulk Status
-- Permissions to set Employees to a status
-- 1/10/2023
-- EB
-- Updated 1/25/2023
------------------------------------------------
--Bulk Status Management: For the Mgr/Supervisor permitted to set statuses
--Limitation: This should be in conjunction with the the hierarchy
---***********
------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsMARSBulkStatusMgmt')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - MARS Bulk Status Management', 
						'General', 
						'TicketsMARSBulkStatusMgmt', 
						'Limitation: Manager ID', 
						GetDate());
GO

------------------------------------------------
--Bulk Status: For Individuals to be re-statused and 
--Limitation: Limitation is the statuses that can be assigned
------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsMARSBulkStatus')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - MARS Bulk Status', 
						'General', 
						'TicketsMARSBulkStatus', 
						'Limitation:Allowed Statuses', 
						GetDate());
GO

--RollBack
--delete from right_definition where entity_data='TicketsBulkStatus'
--delete from right_definition where entity_data='TicketsBulkStatusMgmt'