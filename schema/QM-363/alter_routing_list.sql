/*
   Tuesday, March 9, 202110:35:50 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.routing_list ADD CONSTRAINT
	DF_routing_list_active DEFAULT 1 FOR active
GO
ALTER TABLE dbo.routing_list SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'CONTROL') as Contr_Per 