USE [QM]
GO

/****** Object:  Trigger [dbo].[tseApprovalUpdTrigger]    Script Date: 12/13/2023 10:09:05 AM ******/
/****** QM-866 Larry Killlen ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tseApprovalUpdTrigger]  
ON [dbo].[TSE_ApprovalQueue]
AFTER UPDATE   
AS 
IF ( UPDATE ([Status]) )  
BEGIN 

  insert into [dbo].[TSE_ApprovalQueueHistory] (TSE_entry_id, Work_Emp_ID, STATUS, WorkDate, QR,QC, modified_date, note, entry_date)
  Select 
  TSE_entry_id,
  ins.work_emp_id,
  ins.STATUS,
  ins.WorkDate, 
  ins.QR,
  ins.QC, 
  ins.modified_date, 
  ins.note,
  GetDate()
  From Inserted INS
  where ins.status = 'POSTED'

END;  
 Delete from [dbo].[TSE_ApprovalQueue] where TSE_entry_id = (Select TSE_entry_id from inserted where status = 'POSTED')
GO

ALTER TABLE [dbo].[TSE_ApprovalQueue] ENABLE TRIGGER [tseApprovalUpdTrigger]
GO

