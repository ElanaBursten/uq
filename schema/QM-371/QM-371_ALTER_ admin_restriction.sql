/*
   Tuesday, April 20, 20211:13:12 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_admin_restriction
	(
	admin_restriction_id int NOT NULL IDENTITY (1, 1),
	login_id varchar(50) NULL,
	allowed_forms varchar(4000) NULL,
	modified_date datetime NOT NULL,
	active bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_admin_restriction SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_admin_restriction ADD CONSTRAINT
	DF_admin_restriction_modified_date DEFAULT getdate() FOR modified_date
GO
ALTER TABLE dbo.Tmp_admin_restriction ADD CONSTRAINT
	DF_admin_restriction_active DEFAULT 1 FOR active
GO
SET IDENTITY_INSERT dbo.Tmp_admin_restriction ON
GO
IF EXISTS(SELECT * FROM dbo.admin_restriction)
	 EXEC('INSERT INTO dbo.Tmp_admin_restriction (admin_restriction_id, login_id, allowed_forms, modified_date, active)
		SELECT admin_restriction_id, login_id, allowed_forms, modified_date, active FROM dbo.admin_restriction WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_admin_restriction OFF
GO
DROP TABLE dbo.admin_restriction
GO
EXECUTE sp_rename N'dbo.Tmp_admin_restriction', N'admin_restriction', 'OBJECT' 
GO
ALTER TABLE dbo.admin_restriction ADD CONSTRAINT
	PK__admin_restrictio__597C06EA PRIMARY KEY CLUSTERED 
	(
	admin_restriction_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.admin_restriction', 'Object', 'CONTROL') as Contr_Per 