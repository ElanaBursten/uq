------------------------------------------------
-- QM-671 MARS list: get_MARSlist
-- Returns list of active/MARS enabled Employees under a Manager
-- EB Created 1/3/2023
-- * Repo Update 1/25/2023
------------------------------------------------
if object_id('dbo.get_MARS_EmpList') is not null
  drop procedure dbo.get_MARS_EmpList
go

create procedure dbo.get_MARS_EmpList(@MgrID integer)   
as  
Declare @permissionID integer
set @permissionID = (select right_id from right_definition where entity_data = 'TicketsMARSBulkStatus')

select 'MARS_EmpList' as tname,
       0 as mCheckbox,
	   m.mars_id,
       h.h_emp_id as emp_id,h_short_name as short_name,
       h.h_emp_number as emp_number,
	   m.new_status, m.insert_date, m.inserted_by, 
	   em.short_name as inserted_by_name, 
	   m.active as mars_active, 
	   r.allowed, r.right_id, r.limitation 
from mars m
inner join employee em 
   on (em.emp_id = m.inserted_by)
right outer join (select * from get_hier2_active(@MgrID, 0,10)) h
   on (h.h_emp_id = m.tree_emp_id)
inner join employee_right r
   on (h.h_emp_id = r.emp_id)
where (r.right_id= @permissionID) and (r.allowed='Y')
order by h_Short_name

go