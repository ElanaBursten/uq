--QM-255 Permissions for Hiding Attachments for Closed Tickets 

if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'TicketsHideAttachmentsForClosed')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Tickets - Hide Attachments For Closed Tickets', 'General', 'TicketsHideAttachmentsForClosed', 'No Limitation')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'MultiLocatorsForTicket'

  Select * from right_definition where entity_data = 'TicketsHideAttachmentsForClosed'
