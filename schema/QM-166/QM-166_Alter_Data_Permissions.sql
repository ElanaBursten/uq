--QM-166  Assign Route Orders in Client 5/2020
--QM-166 - Adds Assign Route Order Permission


if not exists(select * from right_definition where entity_data='TicketsAssignRouteOrder')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - Assign Route Order', 'General', 'TicketsAssignRouteOrder', 'Limitation is Employee Hierarchy level.', GetDate());
GO

