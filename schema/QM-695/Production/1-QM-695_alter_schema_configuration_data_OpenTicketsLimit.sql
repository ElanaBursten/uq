------------------------------------------------
--QM-695 Open Tickets Expanded
-- Configuration Data Entry: OpenTicketsLimit
--    Defines the limit of records pulled down for the Open Tickets Expanded
--    Default set at 5000, but adjust appropriately
-- EB
------------------------------------------------
select * from configuration_data where [name] = 'OpenTicketsLimit'

if not exists(select * from configuration_data where [name] = 'OpenTicketsLimit')
  INSERT INTO configuration_data([name],
                                 [value],
								 [editable],
								 [description],
								 [sync])
                        values('OpenTicketsLimit', 
					           '5000', 
						       '1', 
					           'Limitation for the number of open tickets', 
						        1); --No need to sync this data
GO

--update configuration_data
--set [value] = '10'
--where [name] = 'OpenTicketsLimit'