------------------------------------------------
--QM-695 Open Tickets Expanded
-- Permissions to view Open Tickets Expanded (list of tickets) 
-- Modifier: Manager to view (must already be in tree for the user)
-- There is a limit defined in the configuration_data (OpenTicketsLimit)
-- EB
------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsOpenExpTree')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Show Open Tickets Expanded', 
						'General', 
						'TicketsOpenExpTree', 
						'Limitation is not used for this right', 
						GetDate());
GO

--Select * from right_definition order by right_description
--select * from employee_right






