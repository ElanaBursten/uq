    Update Configuration_data
	set sync = 1
	where ((Name in ('ClientURL', 'TicketActRefreshSeconds', 'TicketChangeViewLimitsMax',
    'UpdateFileName', 'UpdateFileHash', 'QMClientHttpPort', 'PTOHoursEntryStartDate', 
	'CenBaseUrl', 'EPR_Pepper', 'RemBaseURL','TEMABaseUrl', 'UtilisafeBaseUrl', -- QM-53, QM-100
	'override_units', 'SCANA_BaseURL', 'INR_BaseURL', 'JobSafetyURL','JSASafetyTktURL', 'IN_QCard'))  --QMANTWO-434 QMANTWO-591 QMANTWO-753 
    or (Name like 'AttachWarning%'))
