--QM-100 Utilisafe Button on Timesheet
--QM-100 Note: Adds Utilisafe Link Permission
if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'TimesheetsUtiliSafe')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Timesheets - Use UtiliSafe', 'General', 'TimesheetsUtiliSafe', 'Limitation is not used for this right.')

  --Rollback (Only if the right has yet to be used)
  --delete from right_definition where entity_data = 'TimesheetsUtiliSafe'

  Select * from right_definition where entity_data = 'TimesheetsUtiliSafe'
