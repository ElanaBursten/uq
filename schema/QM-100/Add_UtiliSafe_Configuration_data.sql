--QM-100 UtiliSafe (URL Link) button


if exists(select * from Configuration_data where name ='UtilisafeBaseUrl')
  Delete from Configuration_data where name = 'UtilisafeBaseUrl'

----Production:
--Insert into Configuration_data(name, value, editable, description, sync)
--                         values('UtilisafeBaseUrl', 'https://qm.utiliquest.com/UtiliSafe/SafetyZone.aspx?', 1, 'Prod: UtiliSafe URL Link', 1);


--Dev:
Insert into Configuration_data(name, value, editable, description, sync)
                         values('UtilisafeBaseUrl', 'https://qm-dev.utiliquest.com/UtiliSafe/SafetyZone.aspx?', 1, 'Dev: UtiliSafe URL Link', 1);


--Select * from Configuration_data where name like 'UtilisafeBaseUrl'


Select * from configuration_data