USE [QM]
GO

/****** Object:  Table [dbo].[employee]    Script Date: 1/31/2019 8:08:09 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[employee](
	[emp_id] [int] IDENTITY(200,1) NOT NULL,
	[type_id] [int] NULL,
	[status_id] [int] NULL,
	[timesheet_id] [int] NULL,
	[emp_number] [varchar](15) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[employee] ADD [short_name] [varchar](30) NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[employee] ADD [first_name] [varchar](20) NULL
ALTER TABLE [dbo].[employee] ADD [last_name] [varchar](30) NULL
ALTER TABLE [dbo].[employee] ADD [middle_init] [char](1) NULL
ALTER TABLE [dbo].[employee] ADD [report_to] [int] NULL
ALTER TABLE [dbo].[employee] ADD [create_date] [datetime] NULL
ALTER TABLE [dbo].[employee] ADD [create_uid] [int] NULL
ALTER TABLE [dbo].[employee] ADD [modified_date] [datetime] NOT NULL
ALTER TABLE [dbo].[employee] ADD [modified_uid] [int] NULL
ALTER TABLE [dbo].[employee] ADD [charge_cov] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [active] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [can_receive_tickets] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [timerule_id] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[employee] ADD [dialup_user] [varchar](20) NULL
ALTER TABLE [dbo].[employee] ADD [company_car] [varchar](10) NULL
ALTER TABLE [dbo].[employee] ADD [repr_pc_code] [varchar](15) NULL
ALTER TABLE [dbo].[employee] ADD [payroll_pc_code] [varchar](15) NULL
ALTER TABLE [dbo].[employee] ADD [crew_num] [varchar](5) NOT NULL
ALTER TABLE [dbo].[employee] ADD [autoclose] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [company_id] [int] NULL
ALTER TABLE [dbo].[employee] ADD [rights_modified_date] [datetime] NOT NULL
ALTER TABLE [dbo].[employee] ADD [ticket_view_limit] [int] NULL
ALTER TABLE [dbo].[employee] ADD [hire_date] [datetime] NULL
ALTER TABLE [dbo].[employee] ADD [show_future_tickets] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [incentive_pay] [bit] NOT NULL
ALTER TABLE [dbo].[employee] ADD [contact_phone] [varchar](20) NULL
ALTER TABLE [dbo].[employee] ADD [local_utc_bias] [int] NULL
ALTER TABLE [dbo].[employee] ADD [ad_username] [varchar](8) NULL
ALTER TABLE [dbo].[employee] ADD [test_com_login] [varchar](20) NULL
ALTER TABLE [dbo].[employee] ADD [adp_login] [varchar](20) NULL
ALTER TABLE [dbo].[employee] ADD [home_lat] [decimal](9, 6) NULL
ALTER TABLE [dbo].[employee] ADD [home_lng] [decimal](9, 6) NULL
ALTER TABLE [dbo].[employee] ADD [alt_lat] [decimal](9, 6) NULL
ALTER TABLE [dbo].[employee] ADD [alt_lng] [decimal](9, 6) NULL
ALTER TABLE [dbo].[employee] ADD [termination_date] [datetime] NULL
 CONSTRAINT [pk_employee] PRIMARY KEY CLUSTERED 
(
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT (getdate()) FOR [modified_date]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT (1) FOR [charge_cov]
GO

ALTER TABLE [dbo].[employee] ADD  CONSTRAINT [DF_employee_active]  DEFAULT (1) FOR [active]
GO

ALTER TABLE [dbo].[employee] ADD  CONSTRAINT [DF_employee_can_receive_tickets]  DEFAULT (1) FOR [can_receive_tickets]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT ('9999') FOR [crew_num]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT (0) FOR [autoclose]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT (getdate()) FOR [rights_modified_date]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT ((1)) FOR [show_future_tickets]
GO

ALTER TABLE [dbo].[employee] ADD  DEFAULT ((0)) FOR [incentive_pay]
GO

ALTER TABLE [dbo].[employee]  WITH CHECK ADD  CONSTRAINT [employee_fk_company] FOREIGN KEY([company_id])
REFERENCES [dbo].[locating_company] ([company_id])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [employee_fk_company]
GO

ALTER TABLE [dbo].[employee]  WITH NOCHECK ADD  CONSTRAINT [fk_employee001] FOREIGN KEY([type_id])
REFERENCES [dbo].[reference] ([ref_id])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [fk_employee001]
GO

ALTER TABLE [dbo].[employee]  WITH NOCHECK ADD  CONSTRAINT [fk_employee003] FOREIGN KEY([status_id])
REFERENCES [dbo].[reference] ([ref_id])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [fk_employee003]
GO

ALTER TABLE [dbo].[employee]  WITH NOCHECK ADD  CONSTRAINT [fk_employee004] FOREIGN KEY([timesheet_id])
REFERENCES [dbo].[reference] ([ref_id])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [fk_employee004]
GO

