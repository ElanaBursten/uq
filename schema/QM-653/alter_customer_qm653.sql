/*
   Wednesday, November 9, 20221:42:38 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.customer
	DROP CONSTRAINT customer_fk_profit_center
GO
ALTER TABLE dbo.profit_center SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.profit_center', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.profit_center', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.profit_center', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.customer
	DROP CONSTRAINT DF__customer__modifi__12549193
GO
ALTER TABLE dbo.customer
	DROP CONSTRAINT DF__customer__active__1348B5CC
GO
ALTER TABLE dbo.customer
	DROP CONSTRAINT DF__customer__period__38453051
GO
CREATE TABLE dbo.Tmp_customer
	(
	customer_id int NOT NULL IDENTITY (700, 1),
	customer_name varchar(40) NOT NULL,
	customer_description varchar(150) NULL,
	customer_number varchar(20) NULL,
	dycom_customer_number varchar(50) NULL,
	city varchar(25) NULL,
	state varchar(2) NULL,
	zip varchar(10) NULL,
	street varchar(35) NULL,
	phone varchar(16) NULL,
	locating_company int NOT NULL,
	modified_date datetime NOT NULL,
	active bit NOT NULL,
	street_2 varchar(100) NULL,
	attention varchar(100) NULL,
	contact_name varchar(100) NULL,
	contact_email varchar(100) NULL,
	period_type varchar(20) NULL,
	pc_code varchar(15) NULL,
	contract varchar(50) NULL,
	contract_review_due_date date NULL,
	customer_number_pc varchar(20) NULL,
	damage_cap varchar(40) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_customer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_customer ADD CONSTRAINT
	DF__customer__modifi__12549193 DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_customer ADD CONSTRAINT
	DF__customer__active__1348B5CC DEFAULT (1) FOR active
GO
ALTER TABLE dbo.Tmp_customer ADD CONSTRAINT
	DF__customer__period__38453051 DEFAULT ('WEEK') FOR period_type
GO
SET IDENTITY_INSERT dbo.Tmp_customer ON
GO
IF EXISTS(SELECT * FROM dbo.customer)
	 EXEC('INSERT INTO dbo.Tmp_customer (customer_id, customer_name, customer_description, customer_number, city, state, zip, street, phone, locating_company, modified_date, active, street_2, attention, contact_name, contact_email, period_type, pc_code, contract, contract_review_due_date, customer_number_pc, damage_cap)
		SELECT customer_id, customer_name, customer_description, customer_number, city, state, zip, street, phone, locating_company, modified_date, active, street_2, attention, contact_name, contact_email, period_type, pc_code, contract, contract_review_due_date, customer_number_pc, damage_cap FROM dbo.customer WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_customer OFF
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT billing_output_config_fk_customer
GO
ALTER TABLE dbo.billing_invoice
	DROP CONSTRAINT billing_invoice_fk_customer
GO
ALTER TABLE dbo.client
	DROP CONSTRAINT client_fk_customer
GO
DROP TABLE dbo.customer
GO
EXECUTE sp_rename N'dbo.Tmp_customer', N'customer', 'OBJECT' 
GO
ALTER TABLE dbo.customer ADD CONSTRAINT
	PK__customer__29AC2CE0 PRIMARY KEY CLUSTERED 
	(
	customer_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX customer_modified_date ON dbo.customer
	(
	modified_date
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.customer ADD CONSTRAINT
	customer_fk_profit_center FOREIGN KEY
	(
	pc_code
	) REFERENCES dbo.profit_center
	(
	pc_code
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
create trigger customer_u on dbo.customer
for update
AS
update customer set modified_date = getdate() where customer_id in (select customer_id from inserted)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.customer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.customer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.customer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.client ADD CONSTRAINT
	client_fk_customer FOREIGN KEY
	(
	customer_id
	) REFERENCES dbo.customer
	(
	customer_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.client SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.client', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.client', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.client', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_invoice ADD CONSTRAINT
	billing_invoice_fk_customer FOREIGN KEY
	(
	customer_id
	) REFERENCES dbo.customer
	(
	customer_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.billing_invoice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_invoice', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_invoice', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_invoice', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_output_config ADD CONSTRAINT
	billing_output_config_fk_customer FOREIGN KEY
	(
	customer_id
	) REFERENCES dbo.customer
	(
	customer_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.billing_output_config SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'CONTROL') as Contr_Per 