
-- ================================================
USE QM
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Brian Pike>
-- Create date: < 11/11/2018>
-- Description:	<Inserts dycom_customer_number into billing_output_config>
-- =============================================

CREATE TRIGGER [dbo].[InsertDycomCustomerNumber]
ON [dbo].[Customer]
AFTER INSERT
AS
BEGIN
   INSERT INTO dbo.billing_output_config(customer_id, dycom_customer_number)
		Select customer_id, dycom_customer_number from Inserted
END
GO
