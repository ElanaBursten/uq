/*
   Wednesday, November 9, 20222:05:54 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT billing_output_config_fk_center_group
GO
ALTER TABLE dbo.center_group SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.center_group', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.center_group', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.center_group', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT billing_output_config_fk_customer
GO
ALTER TABLE dbo.customer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.customer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.customer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.customer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT DF__billing_o__group__29F710FA
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT DF__billing_o__omit___2AEB3533
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT DF__billing_o__modif__118068EB
GO
ALTER TABLE dbo.billing_output_config
	DROP CONSTRAINT DF__billing_o__conso__609318CD
GO
CREATE TABLE dbo.Tmp_billing_output_config
	(
	output_config_id int NOT NULL IDENTITY (1, 1),
	customer_id int NOT NULL,
	output_template varchar(50) NULL,
	contract varchar(50) NULL,
	payment_terms varchar(25) NULL,
	cust_po_num varchar(50) NULL,
	output_fields varchar(300) NULL,
	billing_period varchar(15) NULL,
	flat_fee money NULL,
	comment varchar(100) NULL,
	center_group_id int NULL,
	SolomonOffice varchar(3) NULL,
	SolomonPeriodToPost varchar(6) NULL,
	which_invoice varchar(20) NULL,
	sales_tax_rate decimal(6, 4) NULL,
	group_by_price bit NULL,
	omit_zero_amount bit NULL,
	customer_number varchar(20) NULL,
	dycom_customer_number varchar(50) NULL,
	transmission_billing bit NULL,
	sales_tax_included bit NULL,
	transmission_bulk bit NULL,
	modified_date datetime NOT NULL,
	plat_map_prefix varchar(20) NULL,
	consol_line_item bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_billing_output_config SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_billing_output_config ADD CONSTRAINT
	DF__billing_o__group__29F710FA DEFAULT (0) FOR group_by_price
GO
ALTER TABLE dbo.Tmp_billing_output_config ADD CONSTRAINT
	DF__billing_o__omit___2AEB3533 DEFAULT (0) FOR omit_zero_amount
GO
ALTER TABLE dbo.Tmp_billing_output_config ADD CONSTRAINT
	DF__billing_o__modif__118068EB DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_billing_output_config ADD CONSTRAINT
	DF__billing_o__conso__609318CD DEFAULT (0) FOR consol_line_item
GO
SET IDENTITY_INSERT dbo.Tmp_billing_output_config ON
GO
IF EXISTS(SELECT * FROM dbo.billing_output_config)
	 EXEC('INSERT INTO dbo.Tmp_billing_output_config (output_config_id, customer_id, output_template, contract, payment_terms, cust_po_num, output_fields, billing_period, flat_fee, comment, center_group_id, SolomonOffice, SolomonPeriodToPost, which_invoice, sales_tax_rate, group_by_price, omit_zero_amount, customer_number, transmission_billing, sales_tax_included, transmission_bulk, modified_date, plat_map_prefix, consol_line_item)
		SELECT output_config_id, customer_id, output_template, contract, payment_terms, cust_po_num, output_fields, billing_period, flat_fee, comment, center_group_id, SolomonOffice, SolomonPeriodToPost, which_invoice, sales_tax_rate, group_by_price, omit_zero_amount, customer_number, transmission_billing, sales_tax_included, transmission_bulk, modified_date, plat_map_prefix, consol_line_item FROM dbo.billing_output_config WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_billing_output_config OFF
GO
ALTER TABLE dbo.billing_breakdown
	DROP CONSTRAINT billing_breakdown_fk_oc
GO
DROP TABLE dbo.billing_output_config
GO
EXECUTE sp_rename N'dbo.Tmp_billing_output_config', N'billing_output_config', 'OBJECT' 
GO
ALTER TABLE dbo.billing_output_config ADD CONSTRAINT
	PK__billing_output_c__7A7D0802 PRIMARY KEY CLUSTERED 
	(
	output_config_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.billing_output_config ADD CONSTRAINT
	uk_boc_customer UNIQUE NONCLUSTERED 
	(
	customer_id,
	center_group_id,
	billing_period,
	which_invoice
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.billing_output_config ADD CONSTRAINT
	billing_output_config_fk_customer FOREIGN KEY
	(
	customer_id
	) REFERENCES dbo.customer
	(
	customer_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.billing_output_config ADD CONSTRAINT
	billing_output_config_fk_center_group FOREIGN KEY
	(
	center_group_id
	) REFERENCES dbo.center_group
	(
	center_group_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
create trigger billing_output_config_u on dbo.billing_output_config
for update
AS
update billing_output_config set modified_date = getdate() where output_config_id in (select output_config_id from inserted)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_output_config', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.billing_breakdown ADD CONSTRAINT
	billing_breakdown_fk_oc FOREIGN KEY
	(
	output_config_id
	) REFERENCES dbo.billing_output_config
	(
	output_config_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.billing_breakdown SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.billing_breakdown', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.billing_breakdown', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.billing_breakdown', 'Object', 'CONTROL') as Contr_Per 