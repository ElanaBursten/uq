USE [QM]
GO

INSERT INTO [dbo].[configuration_data]
           ([name]
           ,[value]
           ,[modified_date]
           ,[editable]
           ,[description]
           ,[sync])
     VALUES
           ('PelicanBaseURL'
           ,'https://onecallcapreprod.undergroundservicealert.org/ngen.web/map/index?RequestNumber='
           ,GETDATE()
           ,1
           ,'Pelican URL'
           ,1)
