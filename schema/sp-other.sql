if object_id('dbo.get_column_number') is not null
  drop procedure dbo.get_column_number
GO

create proc get_column_number(@table sysname, @column sysname)
as
begin
  return (select colid from syscolumns where id = object_id(@table) and name = @column)
  if (@@ROWCOUNT <> 1)
    RAISERROR ('The column "%s.%s" was not found in get_column_number',
               15, 1, @table, @column) WITH LOG
end

grant execute on get_column_number to uqweb, QManagerRole
GO


if exists (select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'MinOfThree')
  drop function MinOfThree
go

create function MinOfThree(@D1 datetime, @D2 datetime, @D3 datetime)
returns datetime
as
begin
  declare @Result datetime

  if (@D1 <= @D2) and (@D1 <= @D3)
    set @Result = @D1
  else if (@D2 <= @D1) and (@D2 <= @D3)
    set @Result = @D2
  else
    set @Result = @D3

 return @Result
end
go

/*
select dbo.MinOfThree('2004-01-01', '2005-01-01', '2003-01-01')
select dbo.MinOfThree('2005-01-01', '2003-01-01', '2004-01-01')
select dbo.MinOfThree('2003-01-01', '2004-01-01', '2005-01-01')
*/


if exists (select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'MinOfFourDates')
  drop function MinOfFourDates
go

create function MinOfFourDates(@D1 datetime, @D2 datetime, @D3 datetime, @D4 datetime)
returns datetime
as
begin
  declare @Result datetime

  set @Result = coalesce(@D1, @D2, @D3, @D4)

  if @D2 < @Result    set @Result = @D2
  if @D3 < @Result    set @Result = @D3
  if @D4 < @Result    set @Result = @D4

 return @Result
end
go


grant execute on MinOfFourDates to uqweb, QManagerRole, no_trigger
GO

/*
select dbo.MinOfFourDates('2004-01-01', '2005-01-01', '2003-01-01', '2003-01-01')
select dbo.MinOfFourDates('2005-01-01', '2003-01-01', '2004-01-01', '2003-01-01')
select dbo.MinOfFourDates('2003-01-01', '2004-01-01', '2005-01-01', '2003-01-01')
*/



if object_id('get_ticketnotes') is not null
	drop procedure dbo.get_ticketnotes
go

create proc get_ticketnotes(@TicketID int)
as
  select notes.*
  from notes
  where (notes.foreign_type = 1)       /* foreign_type of 1 = ticket notes */
    and (notes.foreign_id = @TicketID) /* foreign_id has the ticket ID */
    and (notes.active = 1)             /* get only active notes */
go

grant execute on get_ticketnotes to uqweb, QManagerRole

/*** Functions start here *****************************************************/

if object_id('dbo.IdListToTable') is not null
	drop function dbo.IdListToTable
go

create function IdListToTable(@S varchar(8000))
returns @Result table (ID int not null)
as
begin
  declare @SBgn int
  declare @SEnd int

  set @SBgn = 1
  set @SEnd = 1

  while (@SBgn <= len(@S)) begin
    /* find the next comma */
    while (@SEnd <= len(@S)) and (substring(@S, @SEnd, 1) <> ',')
      set @SEnd = @SEnd + 1

    /* convert substring to integer and add result to the table */
    insert into @Result(ID)
    select cast(substring(@S, @SBgn, @SEnd-@SBgn) as int)

    /* skip over the comma */
    set @SEnd = @SEnd + 1
    set @SBgn = @SEnd
  end

  return
end
go


if object_id('dbo.StringListToTable') is not null
	drop function dbo.StringListToTable
go

create function StringListToTable(@S varchar(8000))
returns @Result table (S varchar(80) not null)
as
begin
  declare @SBgn int
  declare @SEnd int

  set @SBgn = 1
  set @SEnd = 1

  while (@SBgn <= len(@S)) begin
    /* find the next comma */
    while (@SEnd <= len(@S)) and (substring(@S, @SEnd, 1) <> ',')
      set @SEnd = @SEnd + 1

    /* add substring to the table */
    insert into @Result(S)
    select substring(@S, @SBgn, @SEnd-@SBgn)

    /* skip over the comma */
    set @SEnd = @SEnd + 1
    set @SBgn = @SEnd
  end

  return
end
go



if object_id('dbo.DateSpan') is not null
	drop function dbo.DateSpan
go

create function DateSpan(
	@StartDate datetime,
	@EndDate datetime
)
returns @Result table (D datetime not null)
as
begin
  declare @x datetime
  set @x = @StartDate
  while (@x <= @EndDate) begin
    insert into @Result(D) values (@x)
    set @x = dateadd(d, 1, @x)
  end
  return
end
go

/*
select * from DateSpan('2004-01-01', '2004-01-05')
*/

if object_id('dbo.NextUniqueID') is not null
	drop procedure dbo.NextUniqueID
go

create procedure NextUniqueID @Name varchar(30) as
begin
  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF
  declare @NextID integer

  begin transaction
    update configuration_data set value = value + 1 where name = @Name
    set @NextID = (select value from configuration_data where name = @Name)
  commit transaction
  return @NextID
end
go

grant execute on dbo.NextUniqueID to uqweb, QManagerRole
go

/*
select * from configuration_data
insert into configuration_data (name, value) values ('NextNewTicketID', '1')
exec dbo.NextUniqueID 'NextNewTicketID'
*/


if object_id('dbo.GetConfigValue') is not null
	drop function dbo.GetConfigValue
go

create function GetConfigValue (@Name varchar(30), @DefaultValue varchar(50))
returns varchar(50)
as
begin
  declare @Value varchar(50)

  set @Value = (select value from configuration_data where name = @Name)

  if @Value is null
    set @Value = @DefaultValue

  return @Value
end
go

grant execute on dbo.GetConfigValue to uqweb, QManagerRole
go

/*
select * from configuration_data
select dbo.GetConfigValue('NextNewTicketID', '25')
select dbo.GetConfigValue('xNextNewTicketID', '25')
*/

