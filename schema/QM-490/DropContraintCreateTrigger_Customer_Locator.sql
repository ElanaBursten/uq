USE [QM]  --QM-490 
GO

ALTER TABLE [dbo].[customer_locator] DROP CONSTRAINT [DF_customer_locator_modified_date] --replaced by trigger below
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  ALTER trigger [dbo].customer_locator_IU on [dbo].[customer_locator]
  for update, insert
  AS
    update customer_locator set modified_date = getdate() where customer_locator_id in (select customer_locator_id from inserted)