USE [QM]
GO

/****** Object:  Trigger [dbo].[client_respond_u]    Script Date: 1/5/2023 10:08:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- add missing call_center update trigger
create trigger [dbo].[client_respond_u] on [dbo].[client_respond_to]
for update
AS
  update client_respond_to set modified_date = getdate() where crt_id in (select crt_id from inserted)
GO

ALTER TABLE [dbo].[client_respond_to] ENABLE TRIGGER [client_respond_u]
GO

