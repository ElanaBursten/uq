USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_response_log3]    Script Date: 4/23/2021 9:22:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[RPT_response_log3] (
  @ManagerId  Int,
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(800),
  @ClientList varchar(800),
  @IncludeSuccess bit,
  @IncludeFailure bit
  
  )
as
/*
declare               --QM-398  for test
  @ManagerId  Int,
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(800),
  @ClientList varchar(800),
  @IncludeSuccess bit,
  @IncludeFailure bit

  
  set @ManagerId         =1239       
  set @StartDate         ='2021-04-22T00:00:00.000'               
  set @EndDate           ='2021-04-23T00:00:00.000'       
  set @CallCenterList    ='FMW1'        
  set @ClientList        ='4008,5059'        
  set @IncludeSuccess    =0        
  set @IncludeFailure    =1        
*/



  set nocount on

  declare @data table(
  response_id int NOT NULL ,
  locate_id int NOT NULL ,
  response_date datetime NOT NULL ,
  call_center varchar(20) NOT NULL ,
  status varchar(5) NOT NULL ,
  response_sent varchar(15) NOT NULL ,
  success bit NOT NULL ,
  reply varchar(40) NULL ,
  response_date_is_dst bit  NULL ,     --QM-398  temp fix.  Do not know where value initiates  sr
  short_reply varchar(40) NULL ,
  ticket_number varchar(20) NOT NULL ,
  work_address_number varchar(10) NULL ,
  work_address_street varchar(60) NULL ,
  client_code varchar(10) NULL,
  closed_date datetime NULL ,
  assigned_to_id int NULL ,
  locate_status varchar(5) NOT NULL ,
  sync_date datetime NULL
  )
  
  insert into @data --fax response, Python ftp, modem, & email responders data
  select
    rl.[response_id], rl.[locate_id],rl.[response_date],rl.[call_center],rl.[status],rl.[response_sent], rl.[success], rl.[reply], rl.[response_date_is_dst] ,--QM-398
    rl.reply,   -- default short_reply to full reply
    t.ticket_number,
    t.work_address_number,
    t.work_address_street,
    l.client_code,
    l.closed_date,
    l.assigned_to_id,
    l.status as locate_status,
    (select max(ls.insert_date) from locate_status ls where ls.locate_id = l.locate_id) as sync_date
  from
    (select [response_id], [locate_id],[response_date],[call_center],[status],[response_sent], [success], [reply], [response_date_is_dst]  --QM-398
	 from response_log as r
     where r.response_date between @StartDate and @EndDate
        and r.call_center in (select S from dbo.StringListToTable(@CallCenterList))
		) rl
    inner join locate l on rl.locate_id = l.locate_id
    inner join ticket t on t.ticket_Id = l.ticket_id
  where (@ClientList = '*' or l.client_id in (select s from dbo.StringListToTable(@ClientList))) 
    and (@ManagerId=-1 or (l.assigned_to_id in (select h_emp_id from  dbo.get_report_hier3(@ManagerId, 0, 99, 2))))


  insert into @data --email result data
  select 
    eqr.eqr_id as response_id, 
    l.locate_id,
    eqr.sent_date as response_date,
    t.ticket_format as call_center,
    l.status,
    '(email)' as response_sent,
    case when eqr.status = 'Sent' then 1 else 0 end as success,
    substring(eqr.error_details, 1, 40) as reply,
	0 as response_date_is_dst,                          --QM-398
    substring(eqr.error_details, 1, 40) as short_reply,
    t.ticket_number,
    t.work_address_number,
    t.work_address_street,
    l.client_code,
    l.closed_date,
    l.assigned_to_id,
    l.status as locate_status,
    ls.insert_date as sync_date  
  from email_queue_result eqr
       join locate_status ls on eqr.ls_id = ls.ls_id
       join locate l on ls.locate_id = l.locate_id
       join ticket t on eqr.ticket_id = t.ticket_id
  where 
        eqr.sent_date between @StartDate and @EndDate
    and t.ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
    and (@ClientList = '*' or l.client_id in (select s from dbo.StringListToTable(@ClientList))) 
    and (@ManagerId=-1 or (l.assigned_to_id in (select h_emp_id from  dbo.get_report_hier3(@ManagerId, 0, 99, 2))))          
          
  --Reply        
  --strip CR (carraige return)
  update @data
    set reply = replace(reply, CHAR(13),' ')
  --strip LF (line feed)  
  update @data
    set reply = replace(reply, CHAR(10),' ') 
  --strip tab
  update @data
    set reply = replace(reply, CHAR(9),' ')    
  
  --short_reply:  
  --strip CR (carraige return)
  update @data
    set short_reply = replace(reply, CHAR(13),' ')
  --strip LF (line feed)  
  update @data
    set short_reply = replace(reply, CHAR(10),' ') 
  --strip tab
  update @data
    set short_reply = replace(reply, CHAR(9),' ')    

  -- munge the short_reply down to just the essence, when it includes the date stuff:
  update @data set short_reply = substring(short_reply, 10, 2)
    where short_reply like '__/__/_____:__:__%'
    
  -- munge the short_reply for Bounce date; it has a different format.  
  update @data set short_reply = substring(short_reply, 1, 6)
    where short_reply like '%_/%_/____ %_:__:%'   
   

  -- Result set 1:
  -- Summary by response sent:
  select
    case when success = 1 then 'Success' when success = 0 then 'Failure' end as success_label,
    success,
    response_sent,
    '(any)                       ' as reply,
    count(success) as N,
    1 as orderby
    from @data
    group by success, response_sent

  UNION

  -- Summary by reply:
  select
    case when success = 1 then 'Success' when success = 0 then 'Failure' end as success_label,
    success,
    '(any)' as response_sent,
    short_reply,
    count(success) as N,
    2 as orderby
    from @data
    group by success, short_reply

  UNION

  -- Summary by success / failure:
  select
    case when success = 1 then 'Success' when success = 0 then 'Failure' end as success_label,
    success,
    '(any)' as response_sent,
    '(any)                       ' as reply,
    count(success) as N,
    3 as orderby
    from @data
    group by success

    order by success desc, orderby, response_sent, reply

  -- Result set 2: Success Detail
    select *,
      work_address_number + ' ' + work_address_street as address
    from @data
    where success = 1 and @IncludeSuccess = 1
    order by response_date

  -- Result set 3: Failure Detail
    select *,
      work_address_number + ' ' + work_address_street as address
    from @data
    where success = 0 and @IncludeFailure = 1
    order by response_date

  -- Result set 4: Client ID param translation

  declare @Clients table (
    oc_code varchar(25) primary key
  )

  if @ClientList<>'*'
  insert into @Clients
  select client.oc_code
    from client
    inner join dbo.IdListToTable(@ClientList) c
      on c.ID = client.client_id
    group by client.oc_code
  else
  insert into @Clients values ('All')

  -- Get space-separated list of client codes
  declare @ClientCodes varchar(2000)
  set @ClientCodes = ''
  update @Clients SET @ClientCodes = @ClientCodes +' '+ oc_code
  SET @ClientCodes = ltrim(@ClientCodes)

  select 'Client Codes' as param_name, @ClientCodes as param_value

