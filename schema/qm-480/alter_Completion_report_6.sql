USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[completion_report_data_6]    Script Date: 10/7/2021 11:25:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--QM-480  Added WorkAddress varChar(100) 

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

ALTER proc [dbo].[completion_report_data_6](
  @datefrom datetime,
  @dateto datetime,
  @office int,
  @manager int,
  @sortby int,
  @EmployeeStatus int,
  @SingleEmployee bit = 0
)
as
set nocount on
--Declare
 -- @datefrom			datetime,
 -- @dateto			datetime,
 -- @office			int,
 -- @manager			int,
 -- @sortby			int,
 -- @EmployeeStatus	int,
 -- @SingleEmployee	bit = 0

 -- set @datefrom		='2021-09-01T00:00:00.000'
 -- set  @dateto		='2021-10-07T00:00:00.000'
 -- set  @office		=-1
 --set   @manager		=2676
 --set   @sortby		=0
 --set   @EmployeeStatus =1


DECLARE @data TABLE  (
  ticket_number varchar(20) NULL ,
  short_name varchar(30) NULL ,
  client_code varchar(10) NULL ,
  status varchar(5) NOT NULL ,
  qty_marked decimal(9,2) NULL ,
  map_page varchar(20) NULL ,
  closed_date datetime NULL ,
  modified_date datetime NOT NULL ,
  office_id int NULL ,
  emp_id int NULL ,
  emp_number varchar(15) NULL ,
  modified_date_secondsonly varchar (20) NULL,
  ticket_type varchar(40) NULL,
  due_date datetime NULL,
  data_locate_id int NOT NULL,
  con_name varchar(50) NULL,
--  work_address_street_number varchar(70) NULL, --qm-480  sr
  WorkAddress varchar(100) NULL,
  alert_status varchar(1) NULL ,
  attachment_count int NULL,
  ticket_id int NULL,
  arrival_date datetime NULL,
  priority varchar(20),
  len_marked bigint,
  work_done_for varchar(80) NULL,
  work_lat decimal(9,6) NULL,
  work_long decimal(9,6) NULL,
  work_type varchar(90) NULL
)

if @office > 0
begin

insert into @data
select 
  ticket.ticket_number,
  employee.short_name,
  locate.client_code,
  locate_status.status,
  locate.qty_marked,
  ticket.map_page,
  locate_status.status_date,
  locate.modified_date,
  employee_office.office_id,
  employee.emp_id,
  employee.emp_number,
  convert(varchar(20), locate_status.insert_date, 120) as modified_date_secondsonly,
  ticket.ticket_type,
  ticket.due_date,
  locate.locate_id,
  ticket.con_name,
  --ticket.work_address_number + ' ' + ticket.work_address_street, --qm-480  sr
  ticket.work_address_number + ' ' + ticket.work_address_street + ', ' + ticket.work_city + ', ' + ticket.work_state as WorkAddress, 
  case locate.Alert when 'A' then '!' else locate.alert end as alert_status, 
  null, -- Attachment count 
  ticket.ticket_id,
  (select min(arrival_date) from jobsite_arrival where jobsite_arrival.ticket_id = ticket.ticket_id),
  Coalesce((select SUBSTRING(description, 1, 20) from reference where reference.ref_id = ticket.work_priority_id), 'Normal'),
  0, -- length marked
  ticket.company,
  ticket.work_lat,
  ticket.work_long,
  ticket.work_type
from locate_status with (INDEX(locate_status_insert_date))
  inner join locate on locate.locate_id=locate_status.locate_id
  inner join employee on employee.emp_id=locate_status.statused_by
  inner join employee_office on employee_office.emp_id=locate_status.statused_by
  inner join ticket on ticket.ticket_id=locate.ticket_id
where locate_status.insert_date between @datefrom and @dateto
  and locate_status.status<>'-N'
  and locate_status.status<>'-R'
  and employee_office.office_id=@office 
  and ((employee.active=@EmployeeStatus) or (@EmployeeStatus=2)) 


end
else -- we're filtering by manager or employee
begin

declare @HierDepth int
select @HierDepth = case @SingleEmployee when 1 then 1 else 10 end
  
-- code duplication here - see above

insert into @data
select
  ticket.ticket_number,
  e.h_short_name,
  locate.client_code,
  locate_status.status,
  locate.qty_marked,
  ticket.map_page,
  locate_status.status_date,
  locate.modified_date,
  0,
  e.h_emp_id,
  e.h_emp_number,
  convert(varchar(20), locate_status.insert_date, 120) as modified_date_secondsonly,
  ticket.ticket_type,
  ticket.due_date,
  locate.locate_id,
  ticket.con_name,
  --ticket.work_address_number + ' ' + ticket.work_address_street,  --qm-480
  ticket.work_address_number + ' ' + ticket.work_address_street + ', ' + ticket.work_city + ', ' + ticket.work_state as WorkAddress, 
  case locate.Alert when 'A' then '!' else locate.alert end as alert_status, 
  null, -- attachment count 
  ticket.ticket_id,
  (select min(arrival_date) from jobsite_arrival where jobsite_arrival.ticket_id = ticket.ticket_id),
  Coalesce((select SUBSTRING(description, 1, 20) from reference where reference.ref_id = ticket.work_priority_id), 'Normal'),
  0,  -- length marked
  ticket.company,
  ticket.work_lat,
  ticket.work_long,
  ticket.work_type
from locate_status with (NOLOCK, INDEX(locate_status_insert_date))
  inner join (select * from dbo.get_report_hier3(@manager, 0, @HierDepth, @EmployeeStatus)) e on e.h_emp_id=locate_status.statused_by
  inner join locate (NOLOCK) on locate.locate_id=locate_status.locate_id
  inner join ticket (NOLOCK) on ticket.ticket_id=locate.ticket_id
where locate_status.insert_date between @datefrom and @dateto
  and locate_status.status<>'-N'
  and locate_status.status<>'-R'
end


UPDATE @data 
SET attachment_count = ( select count(*) from attachment
  inner join ticket on ticket.ticket_id = dat.ticket_id
   and attachment.foreign_id = ticket.ticket_id
   and attachment.foreign_type = 1
   and attachment.active = 1)
from @data dat

update @data set attachment_count = null
from @data dat
where dat.attachment_count = 0

update @data set qty_marked = coalesce((
  select sum(regular_hours)+sum(overtime_hours) from locate_hours lh
  where lh.locate_id = data_locate_id), 0)
 where status = 'H'

update @data set len_marked = coalesce((
  select sum(units_marked) from locate_hours lh
    where lh.locate_id = data_locate_id
      and lh.work_date between @datefrom and @dateto), 0)

if @sortby=0  -- Sync Time, then Locate Time
 select * from @data
 order by modified_date_secondsonly asc, closed_date

if @sortby=1   -- Locator Name, then Locate Time
 select * from @data
 order by short_name, closed_date

if @sortby=2    -- Locate Time
 select * from @data
 order by closed_date

if @sortby=3   -- old behaviour
 select * from @data
 order by modified_date_secondsonly asc, ticket_number, client_code, short_name

if @sortby=4   -- Locator, Ticket Number
 select * from @data
 order by short_name, ticket_number, client_code

if @sortby=99  -- ticket number first, mostly for testing purposes
 select * from @data
 order by ticket_number, client_code, short_name

select coalesce(max(modified_date),Null) as max_modified_date from @data

