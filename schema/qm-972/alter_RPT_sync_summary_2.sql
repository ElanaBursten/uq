USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[RPT_sync_summary_2]    Script Date: 7/16/2024 3:48:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */
 --qm-947 Meme_version added  sr
ALTER procedure [dbo].[RPT_sync_summary_2] (
  @DateStart datetime,
  @DateEnd   datetime,
  @ManagerID int,
  @EmployeeStatus int
)
as
--declare
--  @DateStart datetime,
--  @DateEnd   datetime,
--  @ManagerID int,
--  @EmployeeStatus int;

-- set @DateStart = N'2024-07-15T00:00:00.000';
-- set @DateEnd = N'2024-07-16T00:00:00.000';
-- set @ManagerID = 2257;
-- set @EmployeeStatus = 1;

  set nocount on

  declare @emps table (
    emp_id int not null primary key,
    emp_number varchar(25) null,
    last_name varchar(30) null,
    short_name varchar(30) null,
    h_report_level integer,
    h_namepath varchar(450) NULL,
    h_lastpath varchar(450) NULL,
    h_numpath varchar(450) NULL,
    h_idpath varchar(160) NULL
  )

  declare @sync_log table (
    emp_id int,  --1
    FirstSync datetime,  --2
    LastSync datetime,--3
    LastLocalSync datetime,--4
    n_client_version decimal(14, 0),--5
    NumSyncs int,--6
    client_version varchar(30),--7
    multi_pc_user bit,--8
    n_addin_version decimal(14, 0),--9
    addin_version varchar(30),--10
    n_persequor_version decimal(14, 0),--11
    persequor_version varchar(30),--12
	n_MEME_version decimal(14, 0),--13
	MEME_version varchar(30),--14
    primary key(emp_id)
  )
 
  declare @versions table (
    version_type char(1) not null, -- C for client, A for addin, P for persequor, M for Meme
    version_num varchar(30) not null,
    primary key (version_type, version_num)
  )

  -- Collect the emps
  insert into @emps
    select h_emp_id, h_emp_number, h_last_name, h_short_name, h_report_level,
      h_namepath, h_lastpath, h_numpath, h_idpath
    from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus) 

  -- client versions
  insert into @versions 
    select distinct 'C', client_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and client_version is not null

  -- addin versions
  insert into @versions 
    select distinct 'A', addin_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and addin_version is not null

  -- persequor versions
  insert into @versions 
    select distinct 'P', persequor_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and persequor_version is not null

  -- MEME versions
  insert into @versions 
    select distinct 'M', MEME_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and MEME_version is not null


  -- Sync Log
  insert into @sync_log
    select
      sync_log.emp_id,  --1
      min(sync_date) as FirstSync,  --2
      max(sync_date) as LastSync, --3
      max(local_date) as LastLocalSync, --4
      max(dbo.version_to_number(client_version)), --5
      count(sync_id) as NumSyncs,                 --6
      '',                                         --7  client_version
      0,                                           --8  multi_pc_user
      max(dbo.version_to_number(addin_version)) as n_addin_version,--9
      '' as addin_version,                                         --10                 
      max(dbo.version_to_number(persequor_version)) as n_persequor_version, --11
      '' as persequor_version,                                              --12

      max(dbo.version_to_number(MEME_version)) as n_MEME_version,          --13
      '' as MEME_version                                                   --14
    from sync_log
    inner join @emps E2 on E2.emp_id = sync_log.emp_id
    where sync_date between @DateStart and @DateEnd
    group by sync_log.emp_id

  update @sync_log
    set client_version = (select v1.version_num from @versions v1
                          where v1.version_type = 'C' 
                            and dbo.version_to_number(v1.version_num) = n_client_version),
        addin_version = (select v2.version_num from @versions v2
                          where v2.version_type = 'A'
                            and dbo.version_to_number(v2.version_num) = n_addin_version),
        persequor_version = (select v3.version_num from @versions v3
                          where v3.version_type = 'P'
                            and dbo.version_to_number(v3.version_num) = n_persequor_version),
          MEME_version = (select v4.version_num from @versions v4
                          where v4.version_type = 'M'
                            and dbo.version_to_number(v4.version_num) = n_MEME_version)

  update @sync_log 
    set multi_pc_user = 1
    where exists (select c.emp_id from computer_info c 
      where c.emp_id = [@sync_log].emp_id 
        and c.insert_date between @DateStart and @DateEnd
      group by c.emp_id having count(*) > 1)

  select
    E1.emp_id,
    tot.FirstSync,
    tot.LastSync,
    tot.LastLocalSync,
    isnull(tot.NumSyncs, 0) as NumSyncs,
    E1.short_name,
    E1.emp_number,
    E1.last_name,
    tot.client_version,
    E1.h_report_level,
    E1.h_namepath,
    E1.h_lastpath,
    E1.h_numpath,
    E1.h_idpath,
    tot.multi_pc_user,
    tot.addin_version,
    tot.persequor_version,
	tot.MEME_version
  from @emps E1
  left join @sync_log as tot on tot.emp_id = E1.emp_id
  order by E1.short_name
