USE [QM]  -- qm-388 sr
GO

INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
		   ,[modifier_desc]
           ,[modified_date])
     VALUES
           ('Assets - Verify/Update Phone No'
           ,'General'
           ,'ManageAssets'
		   ,'Limitation does not apply to this right'
           ,getDate())
GO


