USE [QM]
GO

INSERT INTO [dbo].[right_definition]
           ([right_description]
           ,[right_type]
           ,[entity_data]
           ,[modifier_desc])
     VALUES
           ('Timesheets - Add Yesterday Callout'
           ,'General'
           ,'TimesheetsAddYesterdayCallout'
           ,'Limitation does not apply to this right')
GO