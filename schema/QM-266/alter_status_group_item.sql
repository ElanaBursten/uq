/*
   Friday, November 13, 202012:00:34 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.status_group_item ADD
	ticketTypeExclusion nvarchar(200) NULL,
	PicturesRequired bit NULL,
	NotesRequired bit NULL
GO
ALTER TABLE dbo.status_group_item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.status_group_item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.status_group_item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.status_group_item', 'Object', 'CONTROL') as Contr_Per 