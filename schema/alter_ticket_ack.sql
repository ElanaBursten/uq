exec dbo.drop_primary_key 'ticket_ack' 

ALTER TABLE ticket_ack
ADD ticket_version_id integer NULL
go

ALTER TABLE ticket_ack
ADD ticket_ack_id integer identity
go

ALTER TABLE dbo.ticket_ack ADD CONSTRAINT
	PK_ticket_ack PRIMARY KEY (ticket_ack_id)
GO

alter table dbo.ticket_ack add constraint 
    FK_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO

alter table dbo.ticket_ack add constraint 
    FK_ticket_version_id foreign key (ticket_version_id)
  references ticket_version (ticket_version_id)
GO

alter table dbo.ticket_ack add constraint 
    FK_ack_emp_id foreign key (ack_emp_id)
  references employee (emp_id)
GO

alter table dbo.ticket_ack add constraint 
    FK_locator_emp_id foreign key (locator_emp_id)
  references employee (emp_id)
GO

create index ticket_ack_ticket_id on ticket_ack(ticket_id)
create index ticket_ack_ticket_version_id on ticket_ack(ticket_version_id)

