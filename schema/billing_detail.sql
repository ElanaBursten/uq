USE [QM]
GO

ALTER TABLE [dbo].[billing_detail] DROP CONSTRAINT [DF__billing_d__high___2EA5EC27]
GO

/****** Object:  Table [dbo].[billing_detail]    Script Date: 1/17/2019 9:26:49 AM ******/
DROP TABLE [dbo].[billing_detail]
GO

/****** Object:  Table [dbo].[billing_detail]    Script Date: 1/17/2019 9:26:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[billing_detail](
	[billing_detail_id] [int] IDENTITY(8000,1) NOT NULL,
	[bill_id] [int] NOT NULL,
	[locate_id] [int] NOT NULL,
	[billing_cc] [varchar](10) NOT NULL,
	[ticket_number] [varchar](20) NOT NULL,
	[work_county] [varchar](40) NULL,
	[work_state] [varchar](2) NULL,
	[work_city] [varchar](40) NULL,
	[work_address_number] [varchar](10) NULL,
	[work_address_number_2] [varchar](10) NULL,
	[work_address_street] [varchar](45) NULL,
	[ticket_type] [varchar](30) NULL,
	[transmit_date] [datetime] NULL,
	[due_date] [datetime] NULL,
	[status] [varchar](5) NOT NULL,
	[qty_marked] [int] NULL,
	[closed_how] [varchar](10) NULL,
	[closed_date] [datetime] NULL,
	[bill_code] [varchar](10) NULL,
	[bucket] [varchar](60) NOT NULL,
	[price] [money] NOT NULL,
	[con_name] [varchar](50) NULL,
	[client_id] [int] NOT NULL,
	[locator_id] [int] NULL,
	[work_done_for] [varchar](50) NULL,
	[work_type] [varchar](90) NULL,
	[work_lat] [decimal](9, 6) NULL,
	[work_long] [decimal](9, 6) NULL,
	[rate] [money] NOT NULL,
	[work_cross] [varchar](100) NULL,
	[call_date] [datetime] NULL,
	[initial_status_date] [datetime] NULL,
	[emp_number] [varchar](15) NULL,
	[short_name] [varchar](20) NULL,
	[work_description] [varchar](255) NULL,
	[area_name] [varchar](40) NULL,
	[transmit_count] [int] NULL,
	[hours] [decimal](9, 3) NULL,
	[line_item_text] [varchar](60) NULL,
	[revision] [varchar](20) NULL,
	[map_ref] [varchar](60) NULL,
	[locate_hours_id] [int] NULL,
	[qty_charged] [int] NULL,
	[which_invoice] [varchar](20) NULL,
	[tax_name] [varchar](40) NULL,
	[tax_rate] [decimal](6, 4) NULL,
	[tax_amount] [money] NULL,
	[raw_units] [int] NULL,
	[update_of] [varchar](20) NULL,
	[high_profile] [bit] NOT NULL,
	[plat] [varchar](20) NULL,
	[initial_arrival_date] [datetime] NULL,
	[units_marked] [int] NULL,
	[work_remarks] [varchar](255) NULL,
	[locate_added_by] [varchar](8) NULL,
	[rate_value_group_id] [int] NULL,
	[additional_line_item_text] [varchar](60) NULL,
	[Ref_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[billing_detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[billing_detail] ADD  DEFAULT ((0)) FOR [high_profile]
GO

