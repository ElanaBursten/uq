USE [QM]
GO

/****** Object:  Table [dbo].[UTAtime_entry]    Script Date: 8/6/2022 8:16:05 AM ******/
DROP TABLE [dbo].[UTAtime_entry]
GO

/****** Object:  Table [dbo].[UTAtime_entry]    Script Date: 8/6/2022 8:16:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UTAtime_entry](
	[EmpNo] [varchar](6) NULL,
	[Emp_ID] [int] NOT NULL,
	[WorkDate] [date] NOT NULL,
	[WorkTime] [time](0) NOT NULL,
	[WorkDateTime]  AS (CONVERT([datetime],[WorkDate])+CONVERT([datetime],[WorkTime])),
	[On_Off] [varchar](4) NULL,
 CONSTRAINT [PK_UTAtime_entry] PRIMARY KEY CLUSTERED 
(
	[Emp_ID] ASC,
	[WorkDate] ASC,
	[WorkTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

