USE [QM]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActiveEmpID]    Script Date: 8/2/2022 4:18:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create function [dbo].[GetActiveEmpID] (@EmpNo varchar(6))
returns Int
as
begin
  declare @Value Int

  set @Value = (select emp_id from employee where emp_number = @EmpNo
and ad_username is not null
and active = 1)
  return @Value
end

GO

