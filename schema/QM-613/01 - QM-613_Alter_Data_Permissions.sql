------------------------------------------------
-- QM-613 Timesheet UTA (prev UKG)
-- Permissions to identify salary employees
-- Modifier: 
-- EB Created 8/2022
------------------------------------------------

if not exists(select * from right_definition where entity_data='TimesheetsUTA')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Timesheets - UTA', 
						'General', 
						'TimesheetsUTA', 
						'No Limitation', 
						GetDate());
GO

--select * from right_definition
--where entity_data = 'TimesheetUTA'
--order by right_description

select * from right_definition
order by right_description


