-- DO NOT OVERWRITE
-- PLEASE EDIT FILE ONLY
--    These are creation scripts. Please follow the guidelines when changing these files.
--       * NO ALTERS - Please update script for defailts, new fields and constraints
--       * NO AUTO-GENERATED scripts - These will be saved periodically elsewhere
--       * ALL SCRIPTS shall be preceded with an IFNOT EXISTS.
--       * No explicit USE clause
--    Remember to add an ALTER in the alter folder
--------------------------------------------------------------------------------------

-- client table 
--     Last Update: 12/10/2018  EB  QMANTWO-616        
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='client' AND xtype='U')
CREATE TABLE client(
	client_id int IDENTITY(300,1) NOT NULL,
	client_name varchar(40) NOT NULL,
    oc_code varchar(10) NOT NULL,
    office_id int NULL
	  CONSTRAINT client_fk_office FOREIGN KEY(office_id) REFERENCES office(office_id),
    modified_date datetime NOT NULL DEFAULT getdate(),
    active bit NOT NULL DEFAULT 1,
    call_center varchar(20) NOT NULL,
    update_call_center varchar(20) NULL,
    grid_email varchar(255) NULL,
    allow_edit_locates bit NOT NULL DEFAULT 1,
    customer_id int NULL
	  CONSTRAINT client_fk_customer FOREIGN KEY(customer_id) REFERENCES customer(customer_id),
    utility_type varchar(15) NULL,
    status_group_id int NULL
	  CONSTRAINT client_fk_status_group FOREIGN KEY(status_group_id) REFERENCES status_group(sg_id),
    unit_conversion_id int NULL,
    alert bit  NOT NULL DEFAULT 0,
    require_locate_units bit NOT NULL DEFAULT 0,
    attachment_url varchar(200) NULL,
    qh_id int  NULL,
    ref_id int  NULL,
	parent_client_id int NULL)


	--Retest
	--DROP TABLE client