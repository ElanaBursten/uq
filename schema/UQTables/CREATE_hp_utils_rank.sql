--QM-300 High Profile Email Ranking
--QM-313 Admin Rank changes
USE [QM]
GO

/****** Object:  Table [dbo].[hp_utils_rank]    Script Date: 2/10/2021 11:33:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[hp_utils_rank](
	[hp_util_id] [int] IDENTITY(1,1) NOT NULL,
	[hp_ref_id] [int] NOT NULL,
	[utility_code] [varchar](15) NOT NULL,
	[rank] [smallint] NOT NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[client_id] [int] NULL,
 CONSTRAINT [PK_hp_utils_rank] PRIMARY KEY CLUSTERED 
(
	[hp_util_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[hp_utils_rank] ADD  CONSTRAINT [DF_hp_utils_rank_rank]  DEFAULT ((0)) FOR [rank]
GO

ALTER TABLE [dbo].[hp_utils_rank] ADD  CONSTRAINT [DF_hp_utils_rank_active]  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[hp_utils_rank] ADD  CONSTRAINT [DF_hp_utils_rank_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO

ALTER TABLE [dbo].[hp_utils_rank]  WITH CHECK ADD  CONSTRAINT [FK_hp_utils_rank_reference] FOREIGN KEY([hp_ref_id])
REFERENCES [dbo].[reference] ([ref_id])
GO

ALTER TABLE [dbo].[hp_utils_rank] CHECK CONSTRAINT [FK_hp_utils_rank_reference]
GO


