SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Larry Killen
-- Create date: 1/28/22
-- Description:	QM-535 Sets DOM408 rate to any other DOM on ticket
-- =============================================
Create PROCEDURE UpdateDom408Rates 
	@Bill_id int = 0
AS
BEGIN
	SET NOCOUNT ON;
Declare @DOM408 char(8);
Set @DOM408= 'DOM408';

 Update blkp
 set blkp.rate =bd.rate
 ,blkp.price = bd.price
 ,blkp.line_item_text = bd.line_item_text
 from billing_detail blkp
 inner join (
	select BD.ticket_number,  max(bd.Rate) rate,max(bd.price) price, max(bd.line_item_text) line_item_text, 
	blkp.ticket_number Dom408_ticket_number, blkp.billing_cc , 
	blkp.Rate DOM408_rate, blkp.line_item_text DOM408_LineItem_text
	from [billing_detail] BD
	inner join billing_detail blkp on blkp.bill_id =bd.bill_id 
								and bd.ticket_number =blkp.ticket_number 
								and isnull(bd.revision,'') =isnull(blkp.revision,'') 
								and blkp.billing_cc = @DOM408
	Where BD.bill_id = @bill_id
	and bd.billing_cc <> @DOM408 
	group by BD.ticket_number, blkp.ticket_number, blkp.billing_cc, blkp.Rate, blkp.price, blkp.line_item_text
	having  MAX(bd.price) <> blkp.price
	)bd on bd.ticket_number = blkp.ticket_number 
where blkp.bill_id = @bill_id
and  blkp.billing_cc =@DOM408

END
GO
