
--Trigger on: audit_due_date_rule
CREATE TRIGGER audit_due_date_rule_u on audit_due_date_rule
FOR insert, update
not for replication
AS
BEGIN
  set NOCOUNT on
  update audit_due_date_rule
    set modified_date = getdate()
	from inserted I 
	where audit_due_date_rule.rule_id = I.rule_id
END
GO