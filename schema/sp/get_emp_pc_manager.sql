-- return all of @EmployeeID's superiors using CTE
-- http://www.sqlservercentral.com/articles/Development/commontableexpressionsinsqlserver2005/1758/
if object_id('get_emp_superiors') is not null 
  drop function get_emp_superiors
GO

create function get_emp_superiors(@EmployeeID int) returns table as 
  return with superior_nodes(distance, emp_id, short_name, report_to, active) as 
    (select 0, e.emp_id, e.short_name, e.report_to, e.active
    from employee e 
    where emp_id = @EmployeeID
    union all select distance-1, e.emp_id, e.short_name, e.report_to, e.active 
    from employee e inner join superior_nodes sn on e.emp_id = sn.report_to) 

select distance, emp_id, short_name, active from superior_nodes

GO

-- return the emp_id of the first node above @EmployeeID granted the ManageProfitCenter right
if object_id('get_emp_pc_manager') is not null 
  drop function get_emp_pc_manager
GO

create function get_emp_pc_manager(@EmployeeID int) returns int 
as begin
  declare @PCManagerID int
  declare @ManagePCRightID int

  set @ManagePCRightID = (select max(right_id) 
    from right_definition where entity_data = 'ManageProfitCenter')

  set @PCManagerID = (select top 1 sup.emp_id 
    from dbo.get_emp_superiors(@EmployeeID) sup
    where sup.active = 1
      and dbo.emp_has_right(sup.emp_id, @ManagePCRightID) = 1)

  return(@PCManagerID)
end
GO

/*
 select * from get_emp_superiors(811)
 select dbo.get_emp_pc_manager(811)
 */
