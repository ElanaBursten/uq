if object_id('dbo.RPT_DCL_NoSyncRangeNNG') is not null
	drop procedure dbo.RPT_DCL_NoSyncRangeNNG
GO

create procedure dbo.RPT_DCL_NoSyncRangeNNG
  (
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer, 
  @ShowStatusHistory bit    -- 0 = False, 1 = True
  )
as

select
  ticket.ticket_number,
  convert(datetime, convert(varchar(12), locate.closed_date , 102) , 102) as closed_date_only,
  locate.closed_date,
  locate.client_code,
  locate.status,
  locate.modified_date,  -- most recent sync date
  locate.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date
from ticket with (index(ticket_transmit_date))
  inner join locate with (index(locate_ticketid)) on ticket.ticket_id = locate.ticket_id  
where locate.status <> '-N'
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
--  and (locate.modified_date in (select max(l.modified_date)
--                                from locate l
--                                where l.ticket_id = ticket.ticket_id)
--      or @ShowStatusHistory=1)
order by locate.closed_date
go
grant execute on dbo.RPT_DCL_NoSyncRangeNNG to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_NoSyncRangeNNG '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0, 1
*/

if object_id('dbo.RPT_DCL_SyncRangeNNG') is not null
	drop procedure dbo.RPT_DCL_SyncRangeNNG
GO

create procedure dbo.RPT_DCL_SyncRangeNNG
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

/* Any idea in progress, for later:
declare @EarliestTicketID integer
declare @EarliestLocateID integer
select @EarliestTicketID = (select min(ticket_id) from (select top 1000 ticket_id from ticket where transmit_date>=@XmitDateFrom) t1)
select @EarliestLocateID = (select min(locate_id) from locate where ticket_id = @EarliestTicketID)
*/
select
  ticket.ticket_number,
  L.closed_date_only,
  L.closed_date,
  L.client_code,
  L.status,
  L.modified_date,  -- sync date
  L.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date
FROM 
 (select
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102) as closed_date_only,
  min(ls.status_date) as closed_date,
  locate.client_code,
  ls.status,
  min(ls.insert_date) as modified_date,  -- sync date
  locate.closed,
  locate.ticket_id,
  locate.locate_id
 from locate_status ls
  inner join locate on ls.locate_id = locate.locate_id
 where ls.insert_date between @SyncDateFrom and @SyncDateTo
  and ls.status <> '-N'
  and locate.modified_date >= @SyncDateFrom  -- this one is a perf opt
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and (ls.status_date in (select max(locate_status.status_date)
                           from locate_status  
                           where locate_status.locate_id = locate.locate_id)
      or @ShowStatusHistory=1)
 group by
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102),
  locate.client_code,
  ls.status,
  locate.closed,
  locate.ticket_id,
  locate.locate_id
 ) L
  inner join ticket on ticket.ticket_id = L.ticket_id  
where ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by L.closed_date

go
grant execute on dbo.RPT_DCL_SyncRangeNNG to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_SyncRangeNNG '2003-05-14', '2003-05-21', '2003-05-14', '2003-05-15', 'ATL01', 'Atlanta', 2, 0

*/

if object_id('dbo.RPT_DailyClientLocatesNNG') is not null
	drop procedure dbo.RPT_DailyClientLocatesNNG
GO

create procedure dbo.RPT_DailyClientLocatesNNG
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

if @SyncDateFrom='1900-01-01'  -- not filtering by status date, don't use ls table
  exec dbo.RPT_DCL_NoSyncRangeNNG @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory
else
  exec dbo.RPT_DCL_SyncRangeNNG @SyncDateFrom, @SyncDateTo, @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory

go
grant execute on dbo.RPT_DailyClientLocatesNNG to uqweb, QManagerRole
/*
exec dbo.RPT_DailyClientLocatesNNG '2003-04-14', '2003-04-21', '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 2, 0
exec dbo.RPT_DailyClientLocatesNNG '1900-01-01', '2199-01-01', '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0, 0
*/
