if object_id('dbo.check_billing_setup') is not null
  drop procedure dbo.check_billing_setup
GO

create Procedure check_billing_setup(
  @CallCenterList varchar(800)
)
as
set nocount on

select 'Term/Client ' + coalesce(c.oc_code,'') + ' ' + coalesce(c.client_name,'') +
   ' is not assigned to a customer' as mesg
 from client c
 left join customer cu on cu.customer_id=c.customer_id
where c.call_center in (select S from dbo.StringListToTable(@CallCenterList))
 and cu.customer_id is null

union

select 'Customer ' + coalesce(cu.customer_name,'') + ' ' + coalesce(cu.customer_number,'') + 
  ' is not assigned to a profit center'
 from client c
 inner join customer cu on cu.customer_id=c.customer_id
where c.call_center in (select S from dbo.StringListToTable(@CallCenterList))
 and (cu.pc_code is null or cu.pc_code='')
   and cu.active=1
group by cu.customer_name, cu.customer_number

order by 1
GO

grant execute on check_billing_setup to uqweb, QManagerRole

/*
exec dbo.check_billing_setup 'FCL1,FCL2'
exec dbo.check_billing_setup 'FCL1'
exec dbo.check_billing_setup 'FCL1'
select * from call_center
*/

