if object_id('dbo.acknowledge_ticket') is not null
  drop procedure dbo.acknowledge_ticket
GO

CREATE Procedure acknowledge_ticket(@TicketID int, @EmpID int)
As

update ticket_ack
  set
    ticket_ack.ack_emp_id = @EmpID,
    ticket_ack.ack_date   = getdate(),
    ticket_ack.has_ack    = 1
  where
    ticket_ack.ticket_id   = @TicketID
    and ticket_ack.has_ack = 0

  update ticket set status = 'MANUAL APPROVED'
  where ticket_id = @TicketID
    and status = 'MANUAL'

GO

grant execute on acknowledge_ticket to uqweb, QManagerRole


