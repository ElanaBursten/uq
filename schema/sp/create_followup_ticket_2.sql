--QM-967 Modify create_followup_ticket_2
--Add expiration_date

if object_id('dbo.create_followup_ticket_2') is not null
	drop procedure dbo.create_followup_ticket_2
go

CREATE Procedure create_followup_ticket_2
	(
	@TicketID int,
	@FollowupTypeID int,
	@TransmitDate datetime, 
	@DueDate datetime
	)
As

DECLARE @NewTicketID int
DECLARE @NewTicketType varchar(50)

if @FollowupTypeID < 1
  set @NewTicketType = 'WATCH AND PROTECT'
else
  set @NewTicketType = (select ticket_type from followup_ticket_type
                        where type_Id = @FollowupTypeID)

if (@NewTicketType is null)
  RAISERROR('Invalid followup ticket type ID in create_followup_ticket', 18, 1) with log

INSERT INTO ticket (
ticket_number,
parsed_ok,
ticket_type,
ticket_format,
kind,
status,
map_page,
revision,
transmit_date,
parent_ticket_id,
due_date,
legal_due_date,
work_description,
work_state,
work_county,
work_city,
work_address_number,
work_address_number_2,
work_address_street,
work_cross,
work_subdivision,
work_type,
work_date,
work_notc,
work_remarks,
priority,
legal_date,
legal_good_thru,
legal_restake,
respond_date,
duration,
company,
con_type,
con_name,
con_address,
con_city,
con_state,
con_zip,
call_date,
caller,
caller_contact,
caller_phone,
caller_cellular,
caller_fax,
caller_altcontact,
caller_altphone,
caller_email,
operator,
channel,
work_lat,
work_long,
image,
do_not_mark_before,
route_area_id,
explosives,
serial_number,
map_ref,
alt_due_date,    --QM-967
expiration_date, --QM-967
)
SELECT
ticket_number,
parsed_ok,
@NewTicketType,
ticket_format,
'NORMAL',
status,
map_page,
revision,
coalesce(@TransmitDate, transmit_date), 
ticket_id,
coalesce(@DueDate, due_date), 
legal_due_date,
work_description,
work_state,
work_county,
work_city,
work_address_number,
work_address_number_2,
work_address_street,
work_cross,
work_subdivision,
work_type,
work_date,
work_notc,
work_remarks,
priority,
legal_date,
legal_good_thru,
legal_restake,
respond_date,
duration,
company,
con_type,
con_name,
con_address,
con_city,
con_state,
con_zip,
call_date,
caller,
caller_contact,
caller_phone,
caller_cellular,
caller_fax,
caller_altcontact,
caller_altphone,
caller_email,
operator,
channel,
work_lat,
work_long,
image,
do_not_mark_before,
route_area_id,
explosives,
serial_number,
map_ref, 
alt_due_date,    --QM-967
expiration_date  --QM-967
FROM ticket WHERE ticket_id=@TicketID

SELECT @NewTicketID = SCOPE_IDENTITY()

INSERT INTO locate (
	ticket_id,
	client_code,
	client_id,
	status,
	high_profile,
	added_by
) SELECT
	@NewTicketID AS ID,
	client_code,
	client_id,
	'-P',
	high_profile,
	'DUPL'
 FROM locate
 WHERE ticket_id = @TicketID
 AND status <> '-N'


DECLARE @TicketDate datetime
set @TicketDate = getdate()

INSERT INTO ticket_version (
	ticket_id,
	ticket_number,
	transmit_date,
	processed_date,
	arrival_date,
	source
) SELECT
	@NewTicketID,
	ticket_number,
	coalesce(@TransmitDate, @TicketDate),
	@TicketDate,
	@TicketDate,
	'DUPL'
 FROM ticket
 WHERE ticket_id=@TicketID

GO

grant execute on create_followup_ticket_2 to uqweb, QManagerRole

