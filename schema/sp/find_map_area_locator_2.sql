if object_id('dbo.find_map_area_locator_2') is not null
  drop procedure dbo.find_map_area_locator_2
GO

CREATE Procedure find_map_area_locator_2
	(
	@MapCode varchar(20),
	@MapPageNum varchar(20),
	@X varchar(20),
	@Y varchar(20)
	)
As

select area.area_name, area.area_id, employee.emp_id, employee.short_name
 from map
  inner join map_cell on map.map_id=map_cell.map_id
  inner join area (NOLOCK) on map_cell.area_id = area.area_id
  inner join employee (NOLOCK) on area.locator_id = employee.emp_id
 where map.ticket_code = @MapCode
  and map_page_num=@MapPageNum
  and x_part=@X and y_part=@Y
  and employee.active = 1
  and employee.can_receive_tickets = 1
GO
grant execute on find_map_area_locator_2 to uqweb, QManagerRole



