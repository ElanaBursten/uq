if object_id('dbo.RPT_time_submit_msg_response3') is not null
  drop procedure dbo.RPT_time_submit_msg_response3
go

create procedure RPT_time_submit_msg_response3 (
  @DateStart datetime,
  @DateEnd   datetime,
  @Response  varchar(1000) = '', -- blank for all responses
  @RuleType  varchar(10) = ''
)
as
  set nocount on

  declare @NoPhone varchar(12)
  set @NoPhone = '-'

  declare @results table (
    emp_id integer not null,
    activity_date datetime not null,
    profit_center varchar(15) not null,
    emp_number varchar(15),
    short_name varchar(30),
    last_name varchar(30),
    response varchar(100),
    contact_phone varchar(20),
    rule_type varchar(10),
    changed_by_short_name varchar(30),
    changed_reason varchar(100),
    primary key (emp_id, activity_date))

  insert into @results
  select resp.emp_id,
    resp.response_date,
    coalesce(dbo.get_employee_pc(resp.emp_id, 0), '-') as profit_center, 
    e.emp_number,
    e.short_name,
    e.last_name,
    resp.response,
    coalesce(resp.contact_phone, @NoPhone) as contact_phone,
    rules.rule_type,
    e1.short_name as changed_by_short_name,
    r.description as changed_reason
  from break_rules_response resp 
  inner join break_rules rules on resp.rule_id = rules.rule_id
  inner join employee e on e.emp_id = resp.emp_id
  left outer join message_dest m on m.message_dest_id = resp.message_dest_id
  left outer join timesheet_entry tse on tse.entry_id = m.tse_entry_id
  left outer join employee e1 on e1.emp_id = tse.entry_by
  left outer join reference r on r.ref_id = tse.reason_changed
  where (resp.response_date >= @DateStart and resp.response_date < @DateEnd)
    and (@RuleType = '' or rules.rule_type = @RuleType)
    and (@Response = '' 
     or resp.response in (select s from dbo.StringListToTable(@Response)))

  -- Prior to build 9331, Submit responses were only in the employee_activity table
  insert into @results
  select a.emp_id,
    a.activity_date,
    coalesce(dbo.get_employee_pc(a.emp_id, 0), '-') as profit_center, 
    e.emp_number,
    e.short_name,
    e.last_name,
    a.extra_details as response,
    @NoPhone as contact_phone,
    'SUBMIT' as rule_type,
    null as changed_by_short_name,  -- these aren't available in the old data
    null as changed_reason
  from employee_activity a inner join employee e on e.emp_id = a.emp_id
  where (a.activity_date >= @DateStart and a.activity_date < @DateEnd)
    and a.activity_type = 'TESUBM' 
    and (@Response = '' 
     or a.extra_details in (select s from dbo.StringListToTable(@Response)))
    and (@RuleType = 'SUBMIT' or @RuleType = '')
    and not exists (select r.emp_id from @results r 
      where r.emp_id = a.emp_id 
        and r.activity_date = a.activity_date)

  select * from @results
  order by profit_center, last_name, short_name, activity_date

  -- return description of the requested rule type
  if @RuleType = '' 
    select 'Rule Type:' as param_name, 'All' as param_value
  else
    select 'Rule Type:' as param_name, Coalesce(description, @RuleType, '-') as param_value
    from reference where type = 'BRKRULE' and code = @RuleType
  
go
grant execute on RPT_time_submit_msg_response3 to uqweb, QManagerRole

/*
exec dbo.RPT_time_submit_msg_response3 '2007-03-01', '2007-04-01'
exec dbo.RPT_time_submit_msg_response3 '2007-03-01', '2007-04-01', 'Agree'
exec dbo.RPT_time_submit_msg_response3 '2009-02-01', '2009-02-28'

exec dbo.RPT_time_submit_msg_response3 '2009-05-01', '2009-06-01', 'Disagree', 'Submit'  -- emp disagrees to the currently configured submit time message
exec dbo.RPT_time_submit_msg_response3 '2009-05-01', '2009-06-01', 'Reject', 'MgrAlter'  -- emp rejects alterations to timesheet made by a manager
*/
