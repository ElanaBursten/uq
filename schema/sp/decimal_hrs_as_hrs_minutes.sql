if object_id('dbo.decimal_hrs_as_hrs_dot_minutes') is not null
  drop function dbo.decimal_hrs_as_hrs_dot_minutes
GO

create function decimal_hrs_as_hrs_dot_minutes(@hours decimal(8, 3))
returns decimal(8,2)
as
begin
  declare @out_hours decimal(8,2)
  set @out_hours = convert(int, @hours) + convert(float, (@hours - convert(int, @hours)) * .60)
  return @out_hours
end
go

grant execute on decimal_hrs_as_hrs_dot_minutes to uqweb, QManagerRole

/*
  select 9.5 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(9.5) as hours_dot_minutes
  select 9.49 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(9.49) as hours_dot_minutes
  select 9.33 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(9.33) as hours_dot_minutes
  select 9.25 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(9.25) as hours_dot_minutes
  select 9.0 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(9.0) as hours_dot_minutes
  select .1 as hours_decimal, dbo.decimal_hrs_as_hrs_dot_minutes(.1) as hours_dot_minutes
*/

if object_id('dbo.decimal_hrs_as_hrs_minutes') is not null
  drop function dbo.decimal_hrs_as_hrs_minutes
GO

create function decimal_hrs_as_hrs_minutes(@hours decimal(8, 3))
returns varchar(20)
as
begin
  declare @out_hours decimal(8,2)

  set @out_hours = dbo.decimal_hrs_as_hrs_dot_minutes(@hours)
  return replace(convert(varchar, @out_hours), '.', ':')
end
go

grant execute on decimal_hrs_as_hrs_minutes to uqweb, QManagerRole

/*
  select 9.5 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(9.5) as hours_minutes
  select 9.49 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(9.49) as hours_minutes
  select 9.33 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(9.33) as hours_minutes
  select 9.25 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(9.25) as hours_minutes
  select 9.0 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(9.0) as hours_minutes
  select .1 as hours_decimal, dbo.decimal_hrs_as_hrs_minutes(.1) as hours_minutes
*/
