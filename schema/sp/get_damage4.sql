if object_id('dbo.get_damage5') is not null
  drop procedure dbo.get_damage5
GO

create proc get_damage5(@DamageID int)
as
  select 'damage' as tname, *
  from damage
  where damage_id=@DamageID

  select 'damage_estimate' as tname, *
  from damage_estimate
  where damage_id=@DamageID
    and active = 1

  select 'damage_history' as tname, *
  from damage_history
  where damage_id=@DamageID

  select 'damage_invoice' as tname, *, case
    when IsNull(litigation_id, 0) > 1 then 2
    when IsNull(third_party_id, 0) > 1 then 1
    else 0
   end as invoice_type
  from damage_invoice
  where damage_id=@DamageID

  select 'attachment' as tname, *
    from attachment
    where (foreign_id = @DamageID and foreign_type = 2)

  select 'attachment' as tname, *
    from attachment
    where (foreign_id IN (select ticket_id from damage where damage_id = @DamageID) and foreign_type = 1)

  select 'attachment' as tname, *
    from attachment
    where (foreign_type = 3 and foreign_id in (select third_party_id from damage_third_party where damage_id = @DamageID))

  select 'attachment' as tname, *
    from attachment
    where (foreign_type = 4 and foreign_id in (select litigation_id from damage_litigation where damage_id = @DamageID))

  select 'damage_bill' as tname, *
  from damage_bill
  where damage_id=@DamageID

  select 'damage_litigation' as tname, *
  from damage_litigation
  where damage_id=@DamageID

  select 'damage_third_party' as tname, *
  from damage_third_party
  where damage_id=@DamageID

  select 'notes' as tname, *, case foreign_type
    when 2 then 0
    when 3 then 1
    when 4 then 2
    else null end as damage_notes_type,
   @DamageID as damage_id
  from notes
  where (foreign_id = @DamageID and foreign_type = 2)
   or (foreign_id in (select third_party_id from damage_third_party
   where damage_id = @DamageID) and foreign_type = 3)
   or (foreign_id in (select litigation_id from damage_litigation
   where damage_id = @DamageID) and foreign_type = 4)

  select 'exported_damages_history' as tname, @DamageID as damage_id, exported_damages_history.* 
  from exported_damages_history 
  inner join damage d on exported_damages_history.claim_number = d.uq_damage_id
  where d.damage_id = @DamageID

  select 'document' as tname, * 
  from document
  where (foreign_id = @DamageID and foreign_type = 2)

  select 'jobsite_arrival' as tname, *
  from jobsite_arrival
  where damage_id=@DamageID
  
GO

grant execute on get_damage5 to uqweb, QManagerRole

/*
exec dbo.get_damage5 105524
exec dbo.get_damage5 310096
exec dbo.get_damage5 306055
exec dbo.get_damage5 30102
*/
