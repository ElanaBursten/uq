if object_id('dbo.get_billing_adjustment2') is not null
  drop procedure dbo.get_billing_adjustment2
go

create proc get_billing_adjustment2(@AdjustmentID int)
as
  set nocount on

	select 'billing_adjustment' as tname, *
		from billing_adjustment
		where adjustment_id=@AdjustmentID
go

grant execute on get_billing_adjustment2 to uqweb, QManagerRole
go

/*
exec dbo.get_billing_adjustment2 10
*/
