if object_id('dbo.RPT_employees_in_training') is not null
  drop procedure dbo.RPT_employees_in_training
GO

create procedure RPT_employees_in_training (@ManagerID int, @StartDate datetime, @EndDate datetime)
as begin
  set nocount on

  declare @trainee_type table (
    ref_id int not null primary key)

  declare @trainees table (
    emp_id int not null primary key,
    emp_number varchar(10) not null,
    emp_name varchar(30) not null,
    mgr_name varchar(30) null,
    pc_code varchar(15) not null,
    hire_date datetime null,
    charge_cov bit
    )

  declare @emp_training_span table (
    emp_id int not null,
    trainee_start_date datetime not null,
    trainee_end_date datetime null,
    days_in_training decimal(6,0) default 0,
    tickets_closed int default 0
  )

  declare @snap table (
    ticket_id int not null,
    first_close_date datetime null,
    first_close_emp_id int null
  ) 

  declare @no_end_date datetime
  set @no_end_date = '2099-01-01' -- currently active employee_history rows have active_end = 1/1/2099

  insert into @trainee_type (ref_id) 
    select ref_id from reference 
    where modifier like '%TRN%' and active_ind = 1

  -- get active employees under the manager that were in training during the selected period
  insert into @trainees (emp_id, emp_number, emp_name, mgr_name, pc_code, hire_date, charge_cov)
    select distinct eh.emp_id, 
      coalesce(e.emp_number, '-'),
      coalesce(e.short_name, '-'),
      m.short_name,
      Coalesce(h_eff_payroll_pc, '-'), 
      e.hire_date,
      hier.h_charge_cov
    from dbo.get_report_hier3(@ManagerID, 0, 1000, 1) hier
    inner join employee_history eh on eh.emp_id = hier.h_emp_id
    inner join employee e on e.emp_id = hier.h_emp_id
    inner join employee m on m.emp_id = e.report_to
    inner join @trainee_type tt on eh.type_id = tt.ref_id
    where eh.active_end >= @StartDate and eh.active_start < @EndDate

  insert into @emp_training_span (emp_id, trainee_start_date, trainee_end_date, days_in_training)
    select eh.emp_id, 
      eh.active_start as trainee_start_date,
      case when eh.active_end = @no_end_date then GetDate() -- this history row is active as of the current date
      else eh.active_end end as trainee_end_date,
      case when (eh.active=1 and eh.active_end = @no_end_date) then DateDiff(Day, active_start, GetDate())
        when (eh.active=1 and eh.active_end <> @no_end_date) then DateDiff(Day, active_start, active_end) 
        else 0 end as days_in_training
    from @trainees trn
    inner join employee_history eh on eh.emp_id = trn.emp_id
    inner join @trainee_type tt on eh.type_id = tt.ref_id
    where eh.active_end >= @StartDate and eh.active_start < @EndDate

  delete from @emp_training_span where days_in_training < 1

  -- get tickets worked by trainees
  insert into @snap (ticket_id, first_close_date, first_close_emp_id)
    select t.ticket_id, t.first_close_date, t.first_close_emp_id
    from ticket_snap t
    join @emp_training_span e on t.first_close_emp_id = e.emp_id 
      and t.first_close_date >= e.trainee_start_date
      and t.first_close_date < e.trainee_end_date

  -- update count of tickets worked by the trainees
  update @emp_training_span
    set tickets_closed = (
      select count(*) from @snap ts 
      where ts.first_close_emp_id = emp_id
        and ts.first_close_date >= trainee_start_date
        and ts.first_close_date < trainee_end_date)

  -- The detailed results
  select Coalesce(l.name, '-') as company, 
    case when pc.pc_code is null then '-' else pc.pc_code + ' ' + pc.pc_name end as profit_center,
    trn.emp_number,
    trn.emp_id,
    trn.emp_name,
    trn.hire_date,
    trn.charge_cov,
    trn.mgr_name,
    ets.trainee_start_date, 
    ets.trainee_end_date, 
    ets.days_in_training, 
    ets.tickets_closed
  from @emp_training_span ets
  inner join @trainees trn on (ets.emp_id = trn.emp_id)
  left join profit_center pc on trn.pc_code = pc.pc_code
  left join locating_company l on pc.company_id = l.company_id
  order by 1, 2, 5


  -- Summary by profit center
  select 
    Coalesce(lc.name, '-') as company_name,
    case when pc.pc_code is null then '-' else pc.pc_code + ' ' + pc.pc_name end as profit_center,
    sum(ets.days_in_training) as tot_days_in_training,
    convert(integer, round(avg(ets.days_in_training), 0)) as avg_days_in_training,
    count(distinct trn.emp_number) as count_emp_in_training --Count distinct employee numbers; it's possible for an employee to have more than one training span
  from @emp_training_span ets
  inner join @trainees trn on ets.emp_id = trn.emp_id
  left join profit_center pc on trn.pc_code = pc.pc_code
  left join locating_company lc on pc.company_id = lc.company_id
  group by lc.name, pc.pc_code, pc.pc_name
  order by 1, 2
  
end
go

grant execute on RPT_employees_in_training to uqweb, QManagerRole
go

/*
  exec dbo.RPT_employees_in_training 2676, 'Jan 1, 2008', 'Sep 1, 2008'
  exec dbo.RPT_employees_in_training 2676, 'Jul 1, 2008', 'Jul 10, 2008'
  exec dbo.RPT_employees_in_training 2676, 'Jul 1, 2008', 'Aug 1, 2008'
  exec dbo.RPT_employees_in_training 2676, 'Jul 17, 2008', 'Jul 25, 2008'
  exec dbo.RPT_employees_in_training 2676, 'Aug 17, 2008', 'Aug 25, 2008'
*/