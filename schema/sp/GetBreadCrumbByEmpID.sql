
if object_id('dbo.GetBreadCrumbByEmpID') is not null
  drop procedure dbo.GetBreadCrumbByEmpID
GO

CREATE PROCEDURE GetBreadCrumbByEmpID( 
  @empID int) 
AS 
declare @UTC DateTime; 
set  @UTC =   (select top 1 DateAdd(n, local_utc_bias, local_date) as UTC 
			  FROM sync_log 
			  where emp_id = @empID 
			  order by sync_id desc) 
begin
	SELECT Top 1 '' as Dummy, @UTC as UTC, 
	latitude lat, longitude long,'' as computer, '' as phonenumber 
	FROM gps_position 
	where added_by_id = @empID 
    order by gps_id desc 
End 

GO

grant execute on GetBreadCrumbByEmpID to uqweb, QManagerRole
