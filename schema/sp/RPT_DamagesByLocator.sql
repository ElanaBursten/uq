if object_id('dbo.RPT_DamagesByLocator2') is not null
  drop procedure dbo.RPT_DamagesByLocator2
GO

create procedure dbo.RPT_DamagesByLocator2 (
  @DateFrom datetime,
  @DateTo datetime,
  @ProfitCenter varchar(20),
  @LiabilityType int,
  @EmployeeID int,
  @EmployeeStatus int,
  @SingleEmployee bit
)
as

declare @results table (
  damage_idz         integer     not null,
  uq_damage_idz      integer     not null,
  ticket_idz         integer     null,
  ticket_numberz     varchar(25) null,
  client_idz         integer     null,
  damage_date        datetime    null,
  uq_notified_date   datetime    null,
  estimate_date      datetime    null,
  estimate_amount    decimal(12,2) null,
  invoice_amount     datetime    null,
  paid_amount        datetime    null,
  locator_id         integer     null,
  locator_name       varchar(35) null,
  damage_inv_num     varchar(12) null,
  damage_type        varchar(20) null,
  location           varchar(80) null,
  city               varchar(40) null,
  state              varchar(2)  null,
  utility_co_damaged varchar(40) null,
  investigator_id    integer     null,
  investigator_name  varchar(35) null,
  excavator_company  varchar(30) null,
  uq_resp_code       varchar(4)  null,
  uq_resp_type       varchar(4)  null,
  locator_last_name  varchar(30) null,
  uq_resp_other_desc varchar(100) null,
  exc_resp_code      varchar(4)  null, 
  exc_resp_other_desc varchar(100) null,
  spc_resp_code      varchar(4)  null,
  spc_resp_other_desc varchar(100) null
)

-- First grab the damages, without any other tables,
-- to prevent SQL Server from deciding to table scan
-- the assignment table.  Duh.

insert into @results (damage_idz, uq_damage_idz, ticket_idz, client_idz, damage_date,
  uq_notified_date, damage_type, location, city, state,
  utility_co_damaged, investigator_id, excavator_company, uq_resp_code, uq_resp_type, 
  uq_resp_other_desc, exc_resp_code, exc_resp_other_desc, spc_resp_code, spc_resp_other_desc
)
select d.damage_id, d.uq_damage_id, d.ticket_id, d.client_id, d.damage_date,
  d.uq_notified_date, d.damage_type, d.location, d.city, d.state,
  d.utility_co_damaged, d.investigator_id, d.excavator_company, d.uq_resp_code,
  d.uq_resp_type, uq_resp_other_desc, exc_resp_code, exc_resp_other_desc, spc_resp_code, spc_resp_other_desc
from damage d
where uq_notified_date >= @DateFrom
  and uq_notified_date <= @DateTo
  and ((profit_center = @ProfitCenter) or (@ProfitCenter = ''))
  and (
  /* This avoids the need for an IF or for client-side SQL */
  ((uq_resp_code is not null) and @LiabilityType=0) OR
  ((uq_resp_code is null) and @LiabilityType=1) OR
  ((uq_resp_code is not null and exc_resp_code is null) and @LiabilityType=2) OR
  ((uq_resp_code is not null and exc_resp_code is not null) and @LiabilityType=3) OR
  ((uq_resp_code is null and exc_resp_code is null) and @LiabilityType=4) OR
  (@LiabilityType=5)
 )

-- Set the ticket ID and number from the ticket table
update @results
 set ticket_numberz = t.ticket_number
 from ticket t
 where ticket_idz = t.ticket_id

-- Set the locator ID
update @results
 set locator_id = dbo.get_locator_for_damage(damage_idz)

if (@EmployeeID > 0) begin
  if @SingleEmployee = 1 
    delete from @results where locator_id is null or locator_id <> @EmployeeID
  else
    delete from @results where locator_id is null or locator_id not in 
      (select h_emp_id from dbo.get_report_hier3(@EmployeeID, 0, 99, @EmployeeStatus))
end

update @results
set investigator_name =
  (select short_name from employee e where e.emp_id = investigator_id)

update @results
set locator_name =
  (select short_name from employee e where e.emp_id = locator_id)

update @results
set locator_last_name =
  (select last_name from employee e where e.emp_id = locator_id)

update @results
set estimate_amount =
  (select amount
   from damage_estimate
   where (damage_estimate.damage_id = damage_idz)
     and (estimate_id =
           (select max(estimate_id)
           from damage_estimate
           where damage_estimate.damage_id = damage_idz)))

select damage_idz as damage_id, uq_damage_idz as uq_damage_id,
  ticket_numberz as ticket_number, damage_date, uq_notified_date,
  estimate_amount, locator_id, locator_name, location, city, state,
  utility_co_damaged, excavator_company, uq_resp_code, uq_resp_type, damage_type,
  locator_last_name, uq_resp_other_desc, exc_resp_code, exc_resp_other_desc, 
  spc_resp_code, spc_resp_other_desc
from @results
order by locator_last_name, uq_damage_id

go

grant execute on RPT_DamagesByLocator2 to uqweb, QManagerRole

/*
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 0, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 1, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 2, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 3, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 4, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2003-01-01', '2003-02-01', 'FMB', 5, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2001-01-01', '2007-02-01', 'FNV', 5, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2001-01-01', '2007-02-01', '139', 5, 0, 1, 0
exec dbo.RPT_DamagesByLocator2 '2007-01-01', '2008-01-01', '', 5, 2676, 1, 0

select distinct profit_center, uq_notified_date
from damage
order by profit_center, uq_notified_date
*/
