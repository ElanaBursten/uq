IF OBJECT_ID('dbo.drop_primary_key') IS NOT NULL
	drop procedure dbo.drop_primary_key
GO


CREATE PROCEDURE drop_primary_key (
  @TableName varchar(40)
)
AS

declare @PKName varchar(80)

select @PKName = constraint_name from information_schema.table_constraints
where constraint_type = 'primary key'
and table_name = @TableName

declare @CmdLine varchar(200)

set @CmdLine = 'alter table ' + @TableName
  + ' drop constraint ' + @PKName
exec(@CmdLine)

GO

grant execute on drop_primary_key to uqweb, QManagerRole

/*
exec dbo.drop_primary_key 'map_cell'
*/
