if object_id('dbo.get_billing_charge_code') is not null
  drop function dbo.get_billing_charge_code
go

create function get_billing_charge_code(@BillCode varchar(10), @TicketType varchar(30))
  returns varchar(40)
as
begin
  return (select case 
      when (@BillCode = 'HOURLY' and substring(@TicketType, 1, 2) = 'LP') then 'HRLYLP'
      when (@BillCode = 'HOURLY' and @TicketType = 'SDG&E MAPPING') then 'HRLYSDGE'
      when (@BillCode = 'HOURLY' and @TicketType = 'PG&E AUDIT') then 'HRLYPGE'
      when (@BillCode = 'HOURLY') then 'HOURLY'
      else 'NORMAL' 
    end as charge_code)
end
go

grant execute on get_billing_charge_code to uqweb, QManagerRole
go

/*
select dbo.get_billing_charge_code(Null, Null) -- NORMAL
select dbo.get_billing_charge_code('NORMAL', 'REGULAR') -- NORMAL
select dbo.get_billing_charge_code('NORMAL', 'LPMEET') -- NORMAL
select dbo.get_billing_charge_code('NORMAL', 'SDG&E MAPPING') -- NORMAL
select dbo.get_billing_charge_code('NORMAL', 'PG&E AUDIT') -- NORMAL
select dbo.get_billing_charge_code('HOURLY', 'REGULAR') -- HOURLY
select dbo.get_billing_charge_code('HOURLY', 'LPMEET') -- HRLYLP
select dbo.get_billing_charge_code('HOURLY', 'SDG&E MAPPING') -- HRLYSDGE
select dbo.get_billing_charge_code('HOURLY', 'PG&E AUDIT') -- HRLYPGE
*/
