if object_id('dbo.RPT_DamageInvoice2') is not null
  drop procedure dbo.RPT_DamageInvoice2
go

create procedure dbo.RPT_DamageInvoice2 (
  @ReceivedFromDate datetime,
  @ReceivedToDate   datetime,
  @PaidFromDate     datetime = null,
  @PaidToDate       datetime = null,
  @ProfitCenter     varchar(15) = '',
  @Company          varchar(40) = ''
)
as
  set nocount on

  declare @Results table (
    damage_idz int NOT NULL,
    uq_damage_id int,
    profit_center varchar(15),
    invoice_num varchar(30),
    amount decimal(12, 2),
    company varchar(40),
    received_date datetime,
    damage_date datetime,
    invoice_code varchar(10),
    paid_date datetime,
    paid_amount decimal(12, 2),
    first_estimate decimal(12, 2) default 0,
    current_estimate decimal(12, 2) default 0
  )

  -- Get the selected damage invoice & damage data
  insert into @Results (
    damage_idz,
    uq_damage_id,
    profit_center,
    invoice_num,
    amount,
    company,
    received_date,
    damage_date,
    invoice_code,
    paid_date,
    paid_amount
    )
    select d.damage_id, d.uq_damage_id, d.profit_center, di.invoice_num, di.amount, di.company,
      di.received_date, d.damage_date, d.invoice_code, di.paid_date, coalesce(di.paid_amount, 0)
      from damage_invoice di
      inner join damage d on d.damage_id = di.damage_id
      where di.received_date >= @ReceivedFromDate
      and di.received_date <= @ReceivedToDate
      and ((@ProfitCenter='') or (d.profit_center = @ProfitCenter))
      and ((@Company='') or (di.company like @Company))
      and ((@PaidFromDate is null) or (di.paid_date >= @PaidFromDate))
      and ((@PaidToDate is null) or (di.paid_date <= @PaidToDate))

  -- Get the first estimate amount
  update @Results
    set first_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = damage_idz
      and modified_date = (select min(modified_date) from damage_estimate where damage_id = damage_idz))

  -- Get the current estimate amount
  update @Results
    set current_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = damage_idz
      and modified_date = (select max(modified_date) from damage_estimate where damage_id = damage_idz))

  -- Return the results
  select * from @Results order by profit_center, received_date

go

grant execute on RPT_DamageInvoice2 to uqweb, QManagerRole

/*
exec dbo.RPT_DamageInvoice2 '2005-05-01', '2005-06-01'
exec dbo.RPT_DamageInvoice2 '2005-05-01', '9999-12-31' -- no end date
exec dbo.RPT_DamageInvoice2 '2005-05-01', '2005-06-01', '1899-12-30', '2005-06-01', '910', 'SPRINT'
*/
