if object_id('dbo.RPT_tse_totals') is not null
  drop procedure dbo.RPT_tse_totals
go

create proc RPT_tse_totals(
  @ManagerID int,
  @EndDate datetime,
  @LevelLimit int,
  @FinalOnly int,
  @EmployeeStatus int = 1
)
as
  set nocount on
  declare @StartDate datetime
  set @StartDate = dateadd(d, -6, @EndDate)

  declare @pccodes TABLE (
    emp_id integer NOT NULL PRIMARY KEY,
    type_id integer,
    x integer,
    emp_pc_code varchar(15) NULL,
    pc_code_different bit,
    pc_code_warn bit default 0
  )

  declare @adding_level int
  select @adding_level = 1

  -- get all emps
  INSERT INTO @pccodes (emp_id, x, type_id, emp_pc_code, pc_code_different)
  select e.emp_id, e.report_to, type_id, pd.emp_payroll_pc_code, pd.different as pc_code_different
    from employee e 
    inner join dbo.employee_payroll_data2() pd on e.emp_id = pd.emp_id

  update @pccodes set pc_code_warn=1 
    where pc_code_different=1 
      and type_id in (select ref_id 
      from reference 
      where type = 'emptype' 
      and ((modifier like '%OFF%') or (modifier like '%LOC%')))  -- locator or office staff

  declare @detail table (
    emp_number varchar(20) NULL,
    short_name varchar(30) NULL,
    emp_id int NULL,
    h_report_level int NULL,
    h_namepath varchar(450) NULL,
    h_numpath varchar(450) NULL,
    h_lastpath varchar(450) NULL,
    h_1 varchar(30) NULL,
    h_2 varchar(30) NULL,
    h_3 varchar(30) NULL,
    h_4 varchar(30) NULL,
    h_5 varchar(30) NULL,
    h_6 varchar(30) NULL,
    h_7 varchar(30) NULL,
    h_8 varchar(30) NULL,
    h_9 varchar(30) NULL,
    reg_hours decimal(38, 2) NULL,
    ot_hours decimal(38, 2) NULL,
    dt_hours decimal(38, 2) NULL,
    callout_hours decimal(38, 2) NULL,
    vac_hours decimal(38, 2) NULL,
    leave_hours decimal(38, 2) NULL,
    br_hours decimal(38, 2) NULL,
    hol_hours decimal(38, 2) NULL,
    jury_hours decimal(38, 2) NULL,
    working_hours decimal(38, 2) NULL,
    pov_hours decimal(38, 2) NULL,
    cov_hours decimal(38, 2) NULL,
    pc_code varchar(15) NULL,
    pc_code_warn bit NULL,
    report_end_date datetime NULL,
    rule_abbrev varchar(20) NULL 
  )

  insert into @detail
  select
   min(e.h_emp_number) as emp_number,
   min(e.h_short_name) as short_name,
   min(e.h_emp_id) as emp_id,
   min(e.h_report_level) as h_report_level,
   min(e.h_namepath) as h_namepath,
   min(e.h_numpath) as h_numpath,
   min(e.h_lastpath) as h_lastpath,
   min(h_1) as h_1,
   min(h_2) as h_2,
   min(h_3) as h_3,
   min(h_4) as h_4,
   min(h_5) as h_5,
   min(h_6) as h_6,
   min(h_7) as h_7,
   min(h_8) as h_8,
   min(h_9) as h_9,

   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,

   sum(reg_hours+ot_hours+dt_hours+callout_hours) as working_hours,

   sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as cov_hours,

   (select emp_pc_code from @pccodes pc where pc.emp_id = min(e.h_emp_id)) as pc_code,
   (select pc_code_warn from @pccodes pc where pc.emp_id = min(e.h_emp_id)) as pc_code_warn,

   @EndDate as report_end_date,
   max(reference.modifier) as rule_abbrev

  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
      and (@FinalOnly=0
           or (@FinalOnly=1 and final_approve_by is not null)
           or (@FinalOnly=2 and final_approve_by is null))
   left join employee work_emp on e.h_emp_id = work_emp.emp_id
   left join reference on reference.ref_id = work_emp.timerule_id
  group by e.h_emp_id

  select * from @detail order by h_namepath

  -- totals by day of week
  select datepart(weekday, tse.work_date) as dow,
   min(tse.work_date) as dow_date,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,
   sum(reg_hours+ot_hours+dt_hours+callout_hours) as working_hours,
   sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else 0 end) as pov_hours,
   sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours 
             else 0 end) as cov_hours
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   inner join timesheet_entry tse
     on tse.work_emp_id = e.h_emp_id
  where tse.work_date between @StartDate and @EndDate
   and (tse.status='ACTIVE' or tse.status='SUBMIT')
   and (@FinalOnly=0
           or (@FinalOnly=1 and final_approve_by is not null)
           or (@FinalOnly=2 and final_approve_by is null))
   group by datepart(weekday, tse.work_date)
   order by 1

  -- totals by PC
  select coalesce(pc.emp_pc_code, '(NONE)') as emp_pc_code,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,
   sum(reg_hours+ot_hours+dt_hours+callout_hours) as working_hours,
   sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else 0 end) as pov_hours,
   sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else 0 end) as cov_hours
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   inner join timesheet_entry tse
     on tse.work_emp_id = e.h_emp_id
   inner join @pccodes pc on pc.emp_id = e.h_emp_id
  where tse.work_date between @StartDate and @EndDate
   and (tse.status='ACTIVE' or tse.status='SUBMIT')
   and (@FinalOnly=0
           or (@FinalOnly=1 and final_approve_by is not null)
           or (@FinalOnly=2 and final_approve_by is null))
  group by coalesce(pc.emp_pc_code, '(NONE)')
  order by emp_pc_code


go
grant execute on RPT_tse_totals to uqweb, QManagerRole

/*
exec dbo.RPT_tse_totals 3512, '2013-08-17', 20, 0
exec dbo.RPT_tse_totals 5405, '2004-04-03', 20, 1
exec dbo.RPT_tse_totals 5405, '2004-04-10', 20, 1
exec dbo.RPT_tse_totals 2777, '2004-04-10', 20, 1
exec dbo.RPT_tse_totals 2676, '2004-04-10', 20, 0
exec dbo.RPT_tse_totals 2676, '2008-04-10', 20, 0, 1
exec dbo.RPT_tse_totals 2676, '2008-04-10', 20, 0, 2

select * from reference where type='emptype'
*/
