if object_id('dbo.RPT_ticket_activity') is not null
  drop procedure dbo.RPT_ticket_activity
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_ticket_activity (
  @ManagerId int,
  @StartDate datetime, 
  @EndDate datetime, 
  @EmployeeStatus int
)
as
  set nocount on

  declare @results table (
    tid int not null,
    ticket_number varchar(20) not null,
    ticket_format varchar(20) not null,
    ticket_type varchar(38) null,
    work_type varchar(90) null,
    eid int not null,
    arrival_date datetime null,
    complete_date datetime null,
    minutes_to_complete int null
  )

  declare @emps table (
    eid integer not null primary key,
    emp_number varchar(15) null,
    locator_short_name varchar(30) null,
    report_to_short_name varchar(30) null)

  insert into @emps
    select h_emp_id, h_emp_number, h_short_name, h_report_to_short_name
    from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)
    where h_emp_id <> @ManagerID
 
  -- get ticket arrivals for selected emps & dates
  insert into @results (tid, ticket_number, ticket_format, ticket_type,
    work_type, eid, arrival_date)
  select distinct
    t.ticket_id,
    t.ticket_number, 
    t.ticket_format, 
    t.ticket_type, 
    t.work_type,
    emp.eid,
    taa.activity_date
  from @emps emp
  inner join ticket_activity_ack taa on taa.locator_emp_id = emp.eid
  inner join ticket t on taa.ticket_id = t.ticket_id
  where taa.activity_date between @StartDate and @EndDate
    and taa.activity_type = 'Arrived'

  -- get ticket completions for the arrivals
  update @results
    set complete_date = activity_date, 
      minutes_to_complete = DateDiff(mi, arrival_date, activity_date)
    from ticket_activity_ack 
    where ticket_id = tid 
      and locator_emp_id = eid 
      and activity_type = 'Completed' 
      and activity_date > arrival_date 
      and activity_date < coalesce((
      /* and less than next arrival for this ticket, if there is one or the end of the day */
        select min(arrival_date) from @results r 
        where r.tid = ticket_id
          and r.eid = locator_emp_id
          and r.arrival_date > activity_date), 
        convert(datetime, convert(varchar, arrival_date, 101) + ' 23:59:59'))

  -- get ticket completions that don't have an arrival
  insert into @results (tid, ticket_number, ticket_format, ticket_type,
    work_type, eid, complete_date)
  select distinct
    t.ticket_id,
    t.ticket_number, 
    t.ticket_format, 
    t.ticket_type, 
    t.work_type,
    emp.eid,
    taa.activity_date
  from @emps emp
  inner join ticket_activity_ack taa on taa.locator_emp_id = emp.eid
  inner join ticket t on taa.ticket_id = t.ticket_id
  left join @results r on (r.eid = emp.eid and r.tid = t.ticket_id and r.complete_date = taa.activity_date)
  where taa.activity_date between @StartDate and @EndDate
    and taa.activity_type = 'Completed'
    and r.tid is null

  /* return data used by the report */
  select *
  from @results r
  inner join @emps e on e.eid = r.eid
  order by report_to_short_name, locator_short_name, arrival_date

  /* extra detail dataset, not used by the report, but helpful in troubleshooting */
  select 
    emp.eid,
    emp.emp_number,
    emp.locator_short_name, 
    emp.report_to_short_name,
    taa.activity_date,
    taa.activity_type,
    t.ticket_id,
    t.ticket_number, 
    t.ticket_format, 
    t.ticket_type, 
    t.work_type
  from @emps emp 
  inner join ticket_activity_ack taa on taa.locator_emp_id = emp.eid
  inner join ticket t on taa.ticket_id = t.ticket_id
  where taa.activity_date between @StartDate and @EndDate
  order by report_to_short_name, locator_short_name, activity_date

go

grant execute on RPT_ticket_activity to uqweb, QManagerRole

/* 
 exec RPT_ticket_activity 811, 'Dec 1, 2008', 'Dec 31, 2008', 1
 exec RPT_ticket_activity 811, 'Dec 1, 2008 07:00:00', 'Dec 1, 2008 12:00:00', 1
 exec RPT_ticket_activity 811, 'Aug 3, 2009 07:00:00', 'Aug 4, 2008 00:00:00', 1
 exec RPT_ticket_activity 2676, 'Sep 1, 2009 07:00:00', 'Sep 8, 2009 23:59:59', 1
*/
