if object_id('dbo.find_qtrmin_2') is not null
  drop procedure dbo.find_qtrmin_2
GO

CREATE Procedure find_qtrmin_2
	(
	@LatCode varchar(20),
	@LongCode varchar(20),
	@CallCenter varchar(20)
	)
As

select area.area_name, area.area_id, employee.emp_id, employee.short_name, map_qtrmin.call_center
 from map_qtrmin
  inner join area (NOLOCK) on map_qtrmin.area_id = area.area_id
  inner join employee (NOLOCK) on area.locator_id = employee.emp_id
 where map_qtrmin.qtrmin_lat = @LatCode
  and map_qtrmin.qtrmin_long=@LongCode
  and (map_qtrmin.call_center is null
    or map_qtrmin.call_center = ''
    or map_qtrmin.call_center = @CallCenter)
  and employee.active = 1
  and employee.can_receive_tickets = 1
 order by map_qtrmin.call_center desc
 /* if there is a call center, show that record first */
GO

grant execute on find_qtrmin_2 to uqweb, QManagerRole
GO

/*
find_qtrmin_2 '0911A', '3027A', 'FCO1'
*/
