if object_id('dbo.acknowledge_damage_ticket') is not null
  drop procedure dbo.acknowledge_damage_ticket
go

CREATE Procedure acknowledge_damage_ticket(
  @TicketID int,
  @EmpID int,
  @InvestigatorID int)
As

begin transaction
exec acknowledge_ticket @TicketID, @EmpID

declare @OID int
execute get_office_id @TicketID, @OfficeID = @OID output

declare @ProfitCenter varchar(20)
set @ProfitCenter = (select profit_center from office where office_id = @OID)

declare @UQDamageID int
execute @UQDamageID = NextUniqueID 'NextDamageID'

insert into damage (state, city, county, damage_type, excavator_company, ticket_id,
  investigator_id, office_id, profit_center, uq_notified_date, notified_by_person,
  notified_by_company, remarks, page, claim_status, uq_damage_id)
select t.work_state, t.work_city, t.work_county, 'INCOMING', SUBSTRING(t.con_name, 1, 30), t.ticket_id,
  @InvestigatorID, @OID, @ProfitCenter, t.transmit_date, 'Call Center', 'Call Center',
  work_description, map_page, 'OPEN', @UQDamageID
from ticket t
where t.ticket_id = @TicketID

if exists (select damage_id from damage where uq_damage_id = @UQDamageID) begin
  commit transaction
  select @UQDamageID as "DamageID",  -- so old clients get the real Damage # returned
    @UQDamageID as "UQDamageID", 
    (select short_name from employee where emp_id=@InvestigatorID) as "InvestigatorName"
end
else begin
  rollback transaction
  RAISERROR ('Acknowledged ticket id %d could not create damage id %d.', 16, 1, @TicketID, @UQDamageID)
end

GO

grant execute on acknowledge_damage_ticket to uqweb, QManagerRole

/*
exec dbo.acknowledge_damage_ticket 123124,540,231
*/

