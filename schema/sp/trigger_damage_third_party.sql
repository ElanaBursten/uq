if object_id('dbo.damage_third_party_i') is not null
  drop trigger dbo.damage_third_party_i
go

create trigger dbo.damage_third_party_i on damage_third_party
for insert
not for replication
as  
  update damage_third_party set added_by = modified_by
    where third_party_id in (select third_party_id from inserted)
go

if object_id('dbo.damage_third_party_iu') is not null
  drop trigger dbo.damage_third_party_iu
GO

create trigger dbo.damage_third_party_iu on damage_third_party
for insert, update
not for replication
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_third_party set modified_date = getdate()
    where third_party_id in (select third_party_id from inserted)
go
