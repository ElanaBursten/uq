if object_id('dbo.get_emp_local_time') is not null
  drop function dbo.get_emp_local_time
GO

create function get_emp_local_time (@emp_id int, @eastern_time datetime)
returns datetime
as 
begin

-- Convert an eastern time zone time into the emp_id's local time
-- Assumes the server is in the eastern time zone
-- Most of this sql came from http://stackoverflow.com/questions/3404646/how-to-calculate-the-local-datetime-from-a-utc-datetime-in-tsql-sql-2005/8842966#8842966

if @eastern_time is null set @eastern_time = GETDATE()
declare @emp_utc int
declare @sys_utc int
declare @local_time datetime
declare @sys_dst_adj smallint
declare @emp_dst_adj smallint
declare @year smallint
declare @dst_start_time datetime
declare @dst_end_time datetime

select @sys_utc = DATEDIFF(MINUTE, GETUTCDATE(), GETDATE())
select @emp_utc = dbo.get_emp_local_utc(@emp_id) * -1

-- employee utc does not account for DST offsets, so figure out if applies
set @year = YEAR(@eastern_time)
-- Get first possible DST start date (for rules see http://www.nist.gov/pml/div688/dst.cfm)
IF (@year > 2006) set @dst_start_time = CAST(@year as char(4)) + '-03-08 02:00:00'
ELSE              set @dst_start_time = CAST(@year as char(4)) + '-04-01 02:00:00'
-- Make sure DST start date is a Sunday
WHILE (DATENAME(WEEKDAY, @dst_start_time) <> 'sunday') 
  set @dst_start_time = DATEADD(day, 1, @dst_start_time)

-- Get first possible DST end date
IF (@Year > 2006) set @dst_end_time = CAST(@year as char(4)) + '-11-01 02:00:00'
ELSE              set @dst_end_time = CAST(@year as char(4)) + '-10-25 02:00:00'
-- Make sure DST end date is a Sunday 
WHILE (DATENAME(WEEKDAY, @dst_end_time) <> 'sunday') 
  set @dst_end_time = DATEADD(day, 1, @dst_end_time)

-- Server utc accounts for DST already, so add an hour back if DST applies
set @sys_dst_adj = CASE 
  WHEN @eastern_time BETWEEN @dst_start_time AND @dst_end_time THEN 0 
  ELSE 1 
END

-- Adjust @eastern_time to the employee's time zone
set @local_time = DATEADD(MINUTE, (@emp_utc - @sys_utc), DATEADD(HOUR, @sys_dst_adj, @eastern_time))
set @emp_dst_adj = CASE 
  WHEN @local_time BETWEEN @dst_start_time AND @dst_end_time THEN 1 
  ELSE 0 
END

-- add any DST offset to the emp's local_time
RETURN DATEADD(HOUR, @emp_dst_adj, @local_time)

end
go

grant execute on get_emp_local_time to uqweb, QManagerRole
go

/*
select 3510, GetDate(), dbo.get_emp_local_time(3510, null) -- has local_utc_bias = 300 (Eastern)
select 3512, GetDate(), dbo.get_emp_local_time(3512, null) -- has local_utc_bias = 480 (Pacific)

select 3512, '2014-08-14 09:08:05.087', dbo.get_emp_local_time(3512, '2014-08-14 09:08:05.087')
select 3512, '2014-02-14 09:08:05.087', dbo.get_emp_local_time(3512, '2014-02-14 09:08:05.087')

select 3510, '2014-02-14 09:08:05.087', dbo.get_emp_local_time(3510, '2014-02-14 09:08:05.087')

-- Transition day where DST starts
select 3512, '2014-03-09 05:08:05.087', dbo.get_emp_local_time(3512, '2014-03-09 05:08:05.087')
select 3512, '2014-03-09 06:08:05.087', dbo.get_emp_local_time(3512, '2014-03-09 06:08:05.087')

select 3510, '2014-03-09 05:08:05.087', dbo.get_emp_local_time(3510, '2014-03-09 05:08:05.087')

-- Transition day where DST ends
select 3512, '2014-11-02 05:08:05.087', dbo.get_emp_local_time(3512, '2014-11-02 05:08:05.087')
select 3512, '2014-11-02 01:08:05.087', dbo.get_emp_local_time(3512, '2014-11-02 01:08:05.087')

select 3510, '2014-11-02 02:08:05.087', dbo.get_emp_local_time(3510, '2014-11-02 02:08:05.087')

*/
