------------------------------------------------
-- QM-585 Employee PLUS WhiteList: get_employee_plus_by_emp
-- DBISAM sync for permissions - 
--      Creates mixed Whitelist/Blacklist for sync to employee empPLUS table 
-- EB Created 6/2022
------------------------------------------------

if object_id('dbo.get_employee_plus_by_emp') is not null
  drop procedure dbo.get_employee_plus_by_emp
go

create procedure dbo.get_employee_plus_by_emp(@EmpID Int)
as
Begin
   select 'empPlus' as tname,
               tg.term_group_id as group_id, 
			   tgd.term  as client_code, 
               case when ep.emp_id is null 
			        then 0 else ep.active end as can_status,
			   @EmpID emp_id 
	from term_group tg 
	    inner join term_group_detail tgd 
		  on tg.term_group_id = tgd.term_group_id
		left join employee_PLUS ep  
		  on (ep.term_group_id = tg.term_group_id) 
		  and (ep.emp_id = @EmpID)
    where (upper(tg.group_code) like 'PLUS-%') and
          (tg.active=1) and  --term is active
	     (tgd.active = 1) 
    order by tgd.term_group_id, tgd.term 
	
END
 



