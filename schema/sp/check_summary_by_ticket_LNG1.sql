if object_id('dbo.check_summary_by_ticket_LNG1') is not null
	drop procedure dbo.check_summary_by_ticket_LNG1
GO

CREATE Procedure check_summary_by_ticket_LNG1
	(
	@SummaryDate datetime,
	@CallCenter varchar(30)
	)
As
set nocount on

DECLARE @SummaryWindowStart datetime
DECLARE @SummaryWindowEnd datetime
DECLARE @SummaryDateEnd datetime

DECLARE @tickets_found TABLE (
	tf_ticket_number varchar(20) NOT NULL PRIMARY KEY
)

SELECT @SummaryDateEnd = DATEADD(d, 1, @SummaryDate)

DECLARE @gn TABLE (
	client_code varchar (20) NOT NULL ,
	item_number varchar (20) NULL ,
	ticket_number varchar (20) NULL ,
	type_code varchar (20) NULL ,
	found int
)

/* Get the list of tickets on the summary */
INSERT INTO @gn
select sh.client_code, sd.item_number, sd.ticket_number, sd.type_code, 0 as found
 from summary_header sh
  inner join summary_detail sd
    on sh.summary_header_id=sd.summary_header_id
 where sh.summary_date=@SummaryDate
  and sh.call_center=@CallCenter
 order by sd.item_number, sd.ticket_number

/* Get the list of tickets that were received */
INSERT INTO @tickets_found
select ticket.ticket_number
 from ticket
 where ticket.transmit_date >= (@SummaryDate - 1)
  and ticket.ticket_format in ('LNG1', 'LOR1', 'LWA1')
 group by ticket.ticket_number

/* Mark them as found on the list from the summary */
update @gn set found=1
 from @tickets_found tf
  where ticket_number=tf_ticket_number

select * from @gn
 order by found, client_code, ticket_number

GO
grant execute on check_summary_by_ticket_LNG1 to uqweb, QManagerRole

/*
exec dbo.check_summary_by_ticket '2006-08-22', 'LNG1'
*/
