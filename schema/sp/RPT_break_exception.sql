if object_id('dbo.RPT_break_exception') is not null
  drop procedure dbo.RPT_break_exception
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_break_exception (
  @ManagerId int,
  @StartDate datetime, 
  @EndDate datetime, 
  @EmployeeStatus int,
  @ExcludeTimeRuleIDs varchar(500) = ''
)
as
  set nocount on

  declare @EmptyDate datetime
  set @EmptyDate = '1899-12-30T00:00:00.000'

  declare @emps table (
    emp_id int not null primary key,
    pc_mgr_short_name varchar(30) null,
    mgr_short_name varchar(30) null,
    emp_number varchar(15) null,
    short_name varchar(30) null,
    last_name varchar(30) null,
    pc_code varchar(15) null,
    time_rule varchar(15) null)

  declare @sheets table (
    tse_id int not null primary key,
    work_emp_id int not null,
    work_date datetime not null,
    break_start1 datetime,
    break_stop1 datetime,
    break_minutes1 int,
    break_start2 datetime,
    break_stop2 datetime,
    break_minutes2 int,
    break_start3 datetime,
    break_stop3 datetime,
    break_minutes3 int,
    break_start4 datetime,
    break_stop4 datetime,
    break_minutes4 int,
    break_start5 datetime,
    break_stop5 datetime,
    break_minutes5 int,
    break_start6 datetime,
    break_stop6 datetime,
    break_minutes6 int,
    break_start7 datetime,
    break_stop7 datetime,
    break_minutes7 int,
    break_start8 datetime,
    break_stop8 datetime,
    break_minutes8 int,
    break_start9 datetime,
    break_stop9 datetime,
    break_minutes9 int,
    break_start10 datetime,
    break_stop10 datetime,
    break_minutes10 int,
    break_start11 datetime,
    break_stop11 datetime,
    break_minutes11 int,
    break_start12 datetime,
    break_stop12 datetime,
    break_minutes12 int,
    break_start13 datetime,
    break_stop13 datetime,
    break_minutes13 int,   
    break_start14 datetime,
    break_stop14 datetime,
    break_minutes14 int,
    break_start15 datetime,
    break_stop15 datetime,
    break_minutes15 int)          

  declare @results table (
    emp_id int not null,
    work_date datetime not null,
    break_start datetime,
    break_stop datetime,
    break_minutes int not null)

  -- excluded time rule ids
  declare @timerules_to_exclude table (rule_id int not null primary key)
  insert into @timerules_to_exclude
    select ids.ID from dbo.IdListToTable(@ExcludeTimeRuleIDs) ids

  -- get all the emps for this manager
  insert into @emps (emp_id, pc_mgr_short_name, mgr_short_name, emp_number,
    short_name, last_name, pc_code, time_rule)
  select h_emp_id, h_pc_manager_short_name, h_report_to_short_name, h_emp_number,
    h_short_name, h_last_name, h_eff_payroll_pc, tr.code as time_rule
  from dbo.get_report_hier4(@ManagerID, 0, 99, @EmployeeStatus) hier
  inner join employee e on e.emp_id = h_emp_id
  inner join reference tr on e.timerule_id = tr.ref_id
  where h_emp_id <> @ManagerID
    and ((@ExcludeTimeRuleIDs = '') 
    or (e.timerule_id not in (select rule_id from @timerules_to_exclude)))

  -- get all the sheets for the employees & date range
  insert into @sheets 
  select entry_id, work_emp_id, work_date,
   case when work_start2 is null or work_start2 = @EmptyDate then null else work_stop1 end as break_start1,
   work_start2 as break_stop1,
   datediff(mi,work_stop1,work_start2) as 'break_minutes1',
   case when work_start3 is null or work_start3 = @EmptyDate then null else work_stop2 end as break_start2,
   work_start3 as break_stop2,
   datediff(mi,work_stop2,work_start3) as 'break_minutes2',
   case when work_start4 is null or work_start4 = @EmptyDate then null else work_stop3 end as break_start3,
   work_start4 as break_stop3,
   datediff(mi,work_stop3,work_start4) as 'break_minutes3', 
   case when work_start5 is null or work_start5 = @EmptyDate then null else work_stop4 end as break_start4,
   work_start5 as break_stop4,
   datediff(mi,work_stop4,work_start5) as 'break_minutes4',
   case when work_start6 is null or work_start6 = @EmptyDate then null else work_stop5 end as break_start5,
   work_start6 as break_stop5,
   datediff(mi,work_stop5,work_start6) as 'break_minutes5',
   case when work_start7 is null or work_start7 = @EmptyDate then null else work_stop6 end as break_start6,
   work_start7 as break_stop6,
   datediff(mi,work_stop6,work_start7) as 'break_minutes6',
   case when work_start8 is null or work_start8 = @EmptyDate then null else work_stop7 end as break_start7,
   work_start8 as break_stop7,
   datediff(mi,work_stop7,work_start8) as 'break_minutes7',
   case when work_start9 is null or work_start9 = @EmptyDate then null else work_stop8 end as break_start8,
   work_start9 as break_stop8,
   datediff(mi,work_stop8,work_start9) as 'break_minutes8',
   case when work_start10 is null or work_start10 = @EmptyDate then null else work_stop9 end as break_start9,
   work_start10 as break_stop9,
   datediff(mi,work_stop9,work_start10) as 'break_minutes9',   
   case when work_start11 is null or work_start11 = @EmptyDate then null else work_stop10 end as break_start10,
   work_start11 as break_stop10,
   datediff(mi,work_stop10,work_start11) as 'break_minutes10',   
   case when work_start12 is null or work_start12 = @EmptyDate then null else work_stop11 end as break_start11,
   work_start12 as break_stop11,
   datediff(mi,work_stop11,work_start12) as 'break_minutes11',   
   case when work_start13 is null or work_start13 = @EmptyDate then null else work_stop12 end as break_start12,
   work_start13 as break_stop12,
   datediff(mi,work_stop12,work_start13) as 'break_minutes12',   
   case when work_start14 is null or work_start14 = @EmptyDate then null else work_stop13 end as break_start13,
   work_start14 as break_stop13,
   datediff(mi,work_stop13,work_start14) as 'break_minutes13',
   case when work_start15 is null or work_start15 = @EmptyDate then null else work_stop14 end as break_start14,
   work_start15 as break_stop14,
   datediff(mi,work_stop14,work_start15) as 'break_minutes14',
   case when work_start16 is null or work_start16 = @EmptyDate then null else work_stop15 end as break_start15,
   work_start16 as break_stop15,
   datediff(mi,work_stop15,work_start16) as 'break_minutes15'

   from timesheet_entry 
    inner join @emps on work_emp_id = emp_id
    where work_date between @StartDate and @EndDate 
      and status <> 'OLD'
      and work_start1 is not null
      

  -- add all breaks < 30 minutes to the results
  insert into @results (emp_id, work_date, break_start, break_stop, break_minutes)
  select work_emp_id, work_date,
    break_start1 as 'break_start',
    break_stop1 as 'break_stop',
    Coalesce(break_minutes1, 0) as 'break_minutes'
  from @sheets s
  where Coalesce(break_minutes1, 0) < 30
    and Coalesce(break_minutes2, 0) < 30
    and Coalesce(break_minutes3, 0) < 30
    and Coalesce(break_minutes4, 0) < 30
    and Coalesce(break_minutes5, 0) < 30
    and Coalesce(break_minutes6, 0) < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start2 as 'break_start',
    break_stop2 as 'break_stop',
    break_minutes2 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and Coalesce(break_minutes3, 0) < 30
    and Coalesce(break_minutes4, 0) < 30
    and Coalesce(break_minutes5, 0) < 30
    and Coalesce(break_minutes6, 0) < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start3 as 'break_start',
    break_stop3 as 'break_stop',
    break_minutes3 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and Coalesce(break_minutes4, 0) < 30
    and Coalesce(break_minutes5, 0) < 30
    and Coalesce(break_minutes6, 0) < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start4 as 'break_start',
    break_stop4 as 'break_stop',
    break_minutes4 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and Coalesce(break_minutes5, 0) < 30
    and Coalesce(break_minutes6, 0) < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start5 as 'break_start',
    break_stop5 as 'break_stop',
    break_minutes5 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and Coalesce(break_minutes6, 0) < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start6 as 'break_start',
    break_stop6 as 'break_stop',
    break_minutes6 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and Coalesce(break_minutes7, 0) < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start7 as 'break_start',
    break_stop7 as 'break_stop',
    break_minutes7 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and Coalesce(break_minutes8, 0) < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start8 as 'break_start',
    break_stop8 as 'break_stop',
    break_minutes8 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and Coalesce(break_minutes9, 0) < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start9 as 'break_start',
    break_stop9 as 'break_stop',
    break_minutes9 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and Coalesce(break_minutes10, 0) < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start10 as 'break_start',
    break_stop10 as 'break_stop',
    break_minutes10 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and Coalesce(break_minutes11, 0) < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30
    
  union select work_emp_id, work_date,
    break_start11 as 'break_start',
    break_stop11 as 'break_stop',
    break_minutes11 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and break_minutes11 < 30
    and Coalesce(break_minutes12, 0) < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30

  union select work_emp_id, work_date,
    break_start12 as 'break_start',
    break_stop12 as 'break_stop',
    break_minutes12 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and break_minutes11 < 30
    and break_minutes12 < 30
    and Coalesce(break_minutes13, 0) < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30

  union select work_emp_id, work_date,
    break_start13 as 'break_start',
    break_stop13 as 'break_stop',
    break_minutes13 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and break_minutes11 < 30
    and break_minutes12 < 30
    and break_minutes13 < 30
    and Coalesce(break_minutes14, 0) < 30
    and Coalesce(break_minutes15, 0) < 30

  union select work_emp_id, work_date,
    break_start14 as 'break_start',
    break_stop14 as 'break_stop',
    break_minutes14 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and break_minutes11 < 30
    and break_minutes12 < 30
    and break_minutes13 < 30
    and break_minutes14 < 30
    and Coalesce(break_minutes15, 0) < 30

  union select work_emp_id, work_date,
    break_start15 as 'break_start',
    break_stop15 as 'break_stop',
    break_minutes15 as 'break_minutes'
  from @sheets s
  where break_minutes1 < 30 
    and break_minutes2 < 30
    and break_minutes3 < 30 
    and break_minutes4 < 30 
    and break_minutes5 < 30 
    and break_minutes6 < 30
    and break_minutes7 < 30
    and break_minutes8 < 30
    and break_minutes9 < 30
    and break_minutes10 < 30
    and break_minutes11 < 30
    and break_minutes12 < 30
    and break_minutes13 < 30
    and break_minutes14 < 30
    and break_minutes15 < 30
    
  -- return the ordered results
  select Coalesce(l.name, '-') as company, 
    e.*, r.*
    from @results r 
    inner join @emps e on r.emp_id = e.emp_id
    left join profit_center pc on e.pc_code = pc.pc_code
    left join locating_company l on pc.company_id = l.company_id
    order by l.name, e.pc_code, e.last_name, e.short_name, r.work_date, r.break_start
 
  -- return list of excluded time rules
  select t.rule_id, coalesce(r.code, r.description) as code, r.description
    from @timerules_to_exclude t
    inner join reference r on t.rule_id = r.ref_id
    where r.type = 'timerule'

go
grant execute on RPT_break_exception to uqweb, QManagerRole

/* 
 exec RPT_break_exception 6881, '2010-06-01T00:00:00.000', '2011-06-07T00:00:00.000', 1

 exec RPT_break_exception 2676, 'Mar 1, 2009', 'Apr 1, 2009', 1, '615,824'
*/
