if object_id('dbo.update_call_center_hp') is not null
	drop procedure dbo.update_call_center_hp
go

create proc update_call_center_hp (
	@CallCenter varchar(20),
	@Reasons varchar(400)
)
as
  set nocount on

  -- add any that aren't in there at all
  insert into call_center_hp (call_center, hp_id)
  select @CallCenter as call_center, data.ID as hp_id
   from IdListToTable(@Reasons) data
   where data.ID not in (select hp_id 
     from call_center_hp 
       where call_center=@CallCenter)

  -- make active any that are inactive
  update call_center_hp set active=1
  where call_center=@CallCenter
   and active=0
   and hp_id in (select ID from IdListToTable(@Reasons))

  -- make inactive any that are active
  update call_center_hp set active=0
  where call_center=@CallCenter
   and active=1
   and hp_id not in (select ID from IdListToTable(@Reasons))

  select reference.code + ' - ' + reference.description +
   case when chp.active=0 then ' (inactive)' else '' end as reason,
   reference.ref_id as hp_id, chp.active
  from reference
   left join call_center_hp chp
     on reference.ref_id = chp.hp_id and chp.call_center=@CallCenter
  where reference.type='hpmulti'
  order by reference.sortby
go
grant execute on update_call_center_hp to uqweb, QManagerRole

/*
exec dbo.update_call_center_hp 'FMW1', '826,828'
exec dbo.update_call_center_hp 'FMW1', ''

select * from reference where type='hpmulti'
select * from call_center_hp
delete from call_center_hp
*/
