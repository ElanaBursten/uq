if object_id('dbo.get_employee_pc') is not null
	drop function dbo.get_employee_pc
go

create function get_employee_pc (
	@EmpID int,
	@PayrollPC bit
)
returns varchar(15)
as
begin
  declare @pc varchar(15)

  if @PayrollPC=1
    select @pc = (select payroll_pc_code from employee where emp_id=@EmpID)

  declare @loops int
  declare @e int
  select @loops = 1
  select @e = @EmpID

  while @loops<15 and @pc is null and @e is not null
  begin
    select @pc = (select repr_pc_code from employee where emp_id=@e)
    select @e = (select report_to from employee where emp_id=@e)
    select @loops = @loops + 1
  end

  return @pc
end
GO

grant execute on get_employee_pc to uqweb, QManagerRole

/*
select dbo.get_employee_pc(1545, 1)
select dbo.get_employee_pc(222, 1)
*/
