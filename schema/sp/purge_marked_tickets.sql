/*   Ticket Deletion SP

To use this, set the 'kind' field of some tickets to DELETE, then
run this SP.

*/

if object_id('dbo.purge_marked_tickets') is not null
	drop procedure dbo.purge_marked_tickets
go

create proc purge_marked_tickets
as
begin transaction

declare @tix table (tid int not null primary key)

insert into @tix (tid)
select top 20 ticket_id from ticket where kind='DELETE'

delete from assignment where locate_id in
 (select locate_id from @tix
  inner join locate on ticket_id=tid)

delete from billing_detail where locate_id in
 (select locate_id from @tix
  inner join locate on ticket_id=tid)

delete from locate_hours where locate_id in
 (select locate_id from @tix
  inner join locate on ticket_id=tid)

delete from locate_status where locate_id in
 (select locate_id from @tix
  inner join locate on ticket_id=tid)

delete from nonclient_locate where ticket_id in
 (select tid from @tix)

delete from locate where ticket_id in
 (select tid from @tix)

delete from ticket where ticket_id in
 (select tid from @tix)

commit

go

/*
In the test database (!) use this to set up some tickets to test the deletion on:
update ticket set kind='DELETE' where ticket_id in
(select top 100 ticket_id from ticket where transmit_date between '2003-01-05' and '2003-01-06')

select count(*) from ticket where kind='DELTE'
exec dbo.purge_marked_tickets


Notes from when I used this for Bob, June 2003;
not relevant for future uses:

select count(*) from assignment
 where locator_id=4478
  and active=1
before: 1508
after: 48

select locate.*
 from assignment inner join locate
  on locate.locate_id=assignment.locate_id
 where locator_id=4478
  and assignment.active=1

*/
