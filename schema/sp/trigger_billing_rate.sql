-- Log all billing rate changes to the billing_rate_history table

if exists(select name FROM sysobjects where name = 'billing_rate_iud' AND type = 'TR')
  DROP TRIGGER billing_rate_iud
GO

CREATE TRIGGER billing_rate_iud ON billing_rate
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @ChangeTime datetime
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
    insert into billing_rate_history
     (br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, archive_date, which_invoice, additional_rate, value_group_id, additional_line_item_text)
    select
      br_id, call_center, billing_cc, status, parties, rate, bill_code, bill_type, area_name,
      work_county, line_item_text, work_city, modified_date, @ChangeTime, which_invoice, additional_rate, value_group_id, additional_line_item_text
    from deleted

  -- Upon insert/update update the modified_date for the changed rows
    update billing_rate
    set modified_date = @ChangeTime
    where br_id in (select br_id from inserted)
END
go
