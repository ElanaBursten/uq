if object_id('dbo.delete_damage') is not null
	drop procedure dbo.delete_damage
GO

create proc delete_damage(@DamageID int)
as
begin
  delete from damage_history    where damage_id = @DamageID
  delete from damage_estimate   where damage_id = @DamageID
  delete from damage_invoice    where damage_id = @DamageID
  delete from damage_litigation where damage_id = @DamageID
  delete from damage            where damage_id = @DamageID
end

/*
exec dbo.delete_damage 30000
*/
