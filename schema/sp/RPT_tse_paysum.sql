if object_id('dbo.RPT_tse_paysum2') is not null
	drop procedure dbo.RPT_tse_paysum2
go

create proc RPT_tse_paysum3 (
	@PayrollPC varchar(15),
	@StartDate datetime,
	@EndDate datetime
)
as
  set nocount on
  declare @StartDate datetime
  set @StartDate = dateadd(d, -6, @EndDate)

  declare @detail table (
	payroll_pc varchar(15),
	emp_number varchar(20) NULL,
	short_name varchar(30) NULL,
	last_name varchar(30) NULL,
	first_name varchar(30) NULL,
	emp_id int NULL,
	reg_hours decimal(38, 2) NULL,
	ot_hours decimal(38, 2) NULL,
	dt_hours decimal(38, 2) NULL,
	callout_hours decimal(38, 2) NULL,
	vac_hours decimal(38, 2) NULL,
	leave_hours decimal(38, 2) NULL,
 	pto_hours decimal(38, 2) NULL,
	br_hours decimal(38, 2) NULL,
	hol_hours decimal(38, 2) NULL,
	jury_hours decimal(38, 2) NULL,
	working_hours decimal(38, 2) NULL,
	total_hours decimal(38, 2) NULL,
	pov_hours decimal(38, 2) NULL,
	cov_hours decimal(38, 2) NULL,
	report_end_date datetime NULL,
	rule_abbrev varchar(20) NULL,
	pay_hours decimal(38, 2) NULL,
 	mileage int NULL,
 	charge_cov varchar(2) NULL,
	floating_holidays decimal(38, 2) NULL,
	per_diem_amt decimal(38, 2) NULL
  )

  insert into @detail
  select
   e.emp_pc_code,
   min(work_emp.emp_number),
   min(work_emp.short_name),
   min(work_emp.last_name),
   min(work_emp.first_name),
   min(work_emp.emp_id),

   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,

   sum(reg_hours+ot_hours+dt_hours+callout_hours) as working_hours,

   sum(reg_hours+ot_hours+dt_hours+callout_hours+pto_hours+
      vac_hours+leave_hours+br_hours+hol_hours+jury_hours) as total_hours,

   sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as cov_hours,

   @EndDate as report_end_date,
   max(reference.modifier) as rule_abbrev,
   0,
   sum(Coalesce(tse.miles_stop1 - tse.miles_start1, Null)) as mileage,
   max(case when work_emp.charge_cov = 1 then null
        else 'No' end) as charge_cov,
  sum(case when tse.floating_holiday = 1 then 8.00 else 0 end) as floating_holidays,
  sum(case when tse.per_diem=1 then 45.00 else 0 end) as per_diem_amt
  from (select *
   from employee_payroll_data_Ulti(@PayrollPC)) e
   inner join employee work_emp on e.emp_id = work_emp.emp_id
   left join timesheet_entry tse on tse.work_emp_id = e.emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
      and final_approve_by is not null
   left join reference on reference.ref_id = work_emp.timerule_id
  where work_emp.active=1
  group by e.emp_pc_code, work_emp.emp_number, e.emp_id

  -- per Will, pay_hours does not include Call Out
  update @detail set pay_hours = reg_hours+ot_hours+dt_hours+pto_hours+
      vac_hours+leave_hours+br_hours+hol_hours+jury_hours

  update @detail set pay_hours = 40
   where rule_abbrev = '(S)'

  select * from @detail order by payroll_pc, emp_number

go
grant execute on RPT_tse_paysum3 to uqweb, QManagerRole

/*
exec dbo.RPT_tse_paysum2 '108', '2004-04-10'
exec dbo.RPT_tse_paysum2 null, '2004-04-03'
exec dbo.RPT_tse_paysum2 5405, '2004-04-03'
exec dbo.RPT_tse_paysum2 5405, '2004-04-10'
exec dbo.RPT_tse_paysum2 2676, '2004-04-10'

select * from employee_payroll_data('108')

select * from reference where type='emptype'
*/
