if object_id('dbo.close_all_locates_for_client') is not null
  drop procedure dbo.close_all_locates_for_client
GO

create procedure close_all_locates_for_client (
	@ClientCode varchar(20),
	@Status varchar(20),
	@Billed bit
)
as
set nocount on

declare @ltoc table (
	locate_id int not null primary key
)

insert into @ltoc
select locate.locate_id from locate (NOLOCK)
 where client_code=@ClientCode and closed=0

select count(*) as LocatesToProcess from @ltoc

declare @closed_date_now datetime
select @closed_date_now = getdate()

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
       qty_marked=1,
	invoiced=@Billed
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END

go
/*
exec dbo.close_all_locates_for_client 'COW70', 'NC', 0

select count(*) from locate
 where client_code='COW70' and closed=0

*/
