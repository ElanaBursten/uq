if object_id('dbo.get_emp_local_utc') is not null
  drop function dbo.get_emp_local_utc
GO

create function get_emp_local_utc (@emp_id int)
returns int
as 
begin

-- Get employee's local utc bias.
-- Note that this does not account for daylight savings time adjustments.

declare @default_utc int = 300 -- default to eastern time if emp_id has never synced

return (select coalesce(local_utc_bias, @default_utc)
  from employee
  where emp_id = @emp_id)
end
go

grant execute on get_emp_local_utc to uqweb, QManagerRole
go

/*
select dbo.get_emp_local_utc(3512)

select work_emp_id, work_date, dbo.get_emp_local_utc(work_emp_id) current_work_status
from timesheet_entry where work_date=Convert(date, GetDate()) and status <> 'OLD'

*/
