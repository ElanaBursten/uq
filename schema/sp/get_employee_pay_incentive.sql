if object_id('dbo.get_employee_pay_incentive') is not null
  drop function dbo.get_employee_pay_incentive
go

create function get_employee_pay_incentive (
  @emp_id int, 
  @IncentiveType varchar(20)
)
returns numeric(7,2)
as
begin
  declare @rate numeric(7,2)

  select @rate = (select rate from employee_pay_incentive where emp_id = @emp_id and active = 1 
    and incentive_type = @IncentiveType)

  declare @loops int
  declare @e int
  select @loops = 1
  select @e = @emp_id

  -- search up the hierarchy until we find an active rate for the emp_id
  while @loops < 15 and @rate is null and @e is not null
  begin
    select @rate = (select rate from employee_pay_incentive where emp_id = @e and active = 1 
        and incentive_type = @IncentiveType)
    select @e = (select report_to from employee where emp_id = @e)
    select @loops = @loops + 1
  end

  return @rate
end
GO

grant execute on get_employee_pay_incentive to uqweb, QManagerRole

/*
-- try it for a few employees
select emp_id, emp_number, short_name, 
  dbo.get_employee_pay_incentive (emp_id, 'HOL') hol_incent_pay_rate
from employee where emp_id in (811, 8496, 3512)

-- get the rate for all employees in the incentive_pay program
select emp_id, emp_number, short_name, 
  dbo.get_employee_pay_incentive (emp_id, 'HOL') hol_incent_pay_rate
from employee where incentive_pay = 1

*/
