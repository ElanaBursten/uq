if object_id('dbo.get_ticket_all') is not null
	drop procedure dbo.get_ticket_all
go

create proc get_ticket_all(@TicketID int, @UserId int)
as
  set nocount on

  SELECT a.ticket_number,   
               a.kind,   
               a.due_date,   
               a.work_type,   
               a.work_description,   
               a.work_address_number,   
               a.work_address_number_2,   
               a.work_address_street,   
               a.work_city,   
               a.work_state,   
               a.work_cross,   
               a.work_county,   
               a.company,   
               a.con_name,   
               a.caller_contact,   
               a.caller_phone,   
               a.caller_altcontact,   
               a.caller_altphone,   
               a.work_date,   
               a.transmit_date,   
               a.map_page,   
               a.work_lat,   
               a.work_long,   
               a.work_remarks,
               a.image   
          FROM ticket a 
         WHERE a.ticket_id = @TicketId

  SELECT locate.*, b.status_name
  FROM locate
   inner join statuslist b on locate.status = b.status
  where locate.ticket_id = @TicketId
   and locate.client_code IN 
    ( SELECT client.oc_code FROM users_client uc inner join client
          on client.client_id = uc.client_id where uc.uid = @UserId )
    and (locate.status <> '-N')
    and (locate.status <> '-P')

  select locate.client_code, response_log.*
  from locate
    inner join response_log with (index(response_log_locate_id))
      on response_log.locate_id = locate.locate_id
  where locate.ticket_id = @TicketID

  select notes.*
   from notes
   where notes.foreign_type = 1       -- 1 = ticket notes
     and notes.foreign_id = @TicketID -- foreign_id has the ticket ID
     and notes.active = 1             -- get only active ticket notes
  union
  select notes.*
   from notes inner join locate on (notes.foreign_id = locate.locate_id)
	 where notes.foreign_type = 5        -- 5 = locate notes
	   and locate.ticket_id = @TicketID  
	   and notes.active = 1

  select locate_hours.*
  from locate_hours
    inner join locate on locate.locate_id = locate_hours.locate_id
  where locate.ticket_id=@TicketID
   and locate.client_code IN 
    ( SELECT client.oc_code FROM users_client uc inner join client
          on client.client_id = uc.client_id where uc.uid = @UserId )

  select * from ticket_version
    where ticket_id = @TicketID

  select a.*
   from assignment a
   inner join locate on a.locate_id=locate.locate_id
   where locate.ticket_id = @TicketID
   and locate.client_code IN 
    ( SELECT client.oc_code FROM users_client uc inner join client
          on client.client_id = uc.client_id where uc.uid = @UserId )

  select locate.client_code, e.short_name, 
     ls.*
   from locate_status ls
    inner join locate on ls.locate_id=locate.locate_id
    left join employee e on ls.statused_by=e.emp_id
    where locate.ticket_id = @TicketID
   and locate.client_code IN 
    ( SELECT client.oc_code FROM users_client uc inner join client
          on client.client_id = uc.client_id where uc.uid = @UserId )

  select bd.*
   from billing_detail bd
    inner join locate on bd.locate_id=locate.locate_id
   where locate.ticket_id = @TicketID
    and locate.client_code IN 
    ( SELECT client.oc_code FROM users_client uc inner join client
          on client.client_id = uc.client_id where uc.uid = @UserId )

  select * from users
  where uid = @UserId

go

grant execute on get_ticket_all to uqweb, QManagerRole
go

/*
exec dbo.get_ticket_all 5164161, 5141
exec dbo.get_ticket_all 4714580, 5141



2003260041
2003260301
2003260857
2003260092
2003262058
2003261051
2003262428
2003262367
2003260665
2003260778
2003261456
2003261149
2003260944
2003261147

select top 10 * from ticket where transmit_date between '2003-06-01' and '2003-06-03'
and ticket_format='FAQ1'

select top 10 * from response_log where call_center='FAQ1'

select call_center, count(*) from response_log with (NOLOCK) group by call_center


Atlanta	321777
FCT2	817
FCV1	175518
FCV2	4759
FCV3	805245
FDE1	29209
FDE2	1
FMB1	837759
FMW1	1234809
FMW3	13
FPK1	149188
FTL2	2188
OCC1	994273
OCC2	39
PepCo	83397

select uc.uid, client.*
 from users_client uc
  inner join client on uc.client_id=client.client_id

593

select * from users where uid=593

select ticket_number
 from ticket (index(ticket_transmit_date))
 inner join locate (index(locate_ticketid)) 
   on ticket.ticket_id=locate.ticket_id
 where transmit_date between '2003-06-02' and '2003-06-03'
  and locate.client_code='ATL01'
 and ticket_format='Atlanta'

*/
