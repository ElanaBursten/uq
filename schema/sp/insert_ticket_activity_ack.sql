if object_id('dbo.insert_ticket_activity_ack2') is not null
  drop procedure dbo.insert_ticket_activity_ack2
go

CREATE PROCEDURE insert_ticket_activity_ack2 (
  @TicketID int,
  @ActivityType varchar(20),
  @ActivityDate datetime,
  @ActivityEmpID int
)
AS

set nocount on

-- check if an unacked activity for this ticket_id already exists & create one if not
if not exists (select ticket_id from ticket_activity_ack 
  where ticket_id = @TicketID and activity_type = @ActivityType 
    and locator_emp_id = @ActivityEmpID and has_ack = 0)
begin
  -- insert new record
  insert ticket_activity_ack (ticket_id, activity_type, activity_date, locator_emp_id)
  values (@TicketID, @ActivityType, @ActivityDate, @ActivityEmpID)
end 

GO

grant execute on insert_ticket_activity_ack2 to uqweb, QManagerRole
