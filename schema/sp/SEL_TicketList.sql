if object_id('dbo.SEL_TicketList') is not null
	drop procedure dbo.SEL_TicketList
GO

CREATE Procedure SEL_TicketList
    (
        @UserId         int,
        @TicketNumber   varchar(20),
        @Address        varchar(50),
        @BeginDate      datetime,
        @EndDate        datetime
    )	
AS
    declare @due_date_from datetime
    declare @due_date_to datetime

    If (@Address Is Null) or (@Address='')
    Begin
        SELECT a.ticket_id,
               a.ticket_number,   
               a.ticket_type,   
               a.kind,   
               a.transmit_date,   
               a.due_date,   
               a.work_address_number,  
               a.work_address_number_2,  
               a.work_address_street,   
               a.work_city,   
               a.work_state,  
               a.work_type  
          FROM ticket a
           left join locate b on a.ticket_id = b.ticket_id
         WHERE a.ticket_number = @TicketNumber
          AND  a.transmit_date
                 BETWEEN coalesce(@BeginDate, dateadd(month, -6, getdate()))
                     AND coalesce(@EndDate, getdate())
          AND  b.client_code IN
	        	( SELECT b.oc_code  
                    FROM users_client a,   
                         client b 
                   WHERE b.client_id = a.client_id AND  
                         a.uid = @UserId )
          order by transmit_date

    End
    Else Begin
        Select @Address = '%' + @Address + '%'
        select @due_date_from = dateadd(day, -14, @BeginDate)
        select @due_date_to = dateadd(day, 21, @EndDate)
        
        If @BeginDate < '4/1/02' Select @BeginDate = '4/1/02'
    
        SELECT a.ticket_id,
               a.ticket_number,   
               a.ticket_type,   
               a.kind,   
               a.transmit_date,   
               a.due_date,   
               a.work_address_number,  
               a.work_address_number_2,  
               a.work_address_street,   
               a.work_city, 
               a.work_state,  
               a.work_type  
          FROM ticket a with (INDEX(ticket_duedate))
           left join locate b ON a.ticket_id = b.ticket_id
         WHERE a.transmit_date BETWEEN @BeginDate AND @EndDate AND  
               a.due_date BETWEEN @due_date_from AND @due_date_to AND  
               a.work_address_street LIKE @Address AND  
               b.client_code IN
	        	( SELECT b.oc_code  
                    FROM users_client a,   
                         client b 
                   WHERE b.client_id = a.client_id AND  
                         a.uid = @UserId )
          order by transmit_date
                             
    End
    Return @@Error
GO

grant execute on dbo.SEL_TicketList to uqweb, QManagerRole

/*
exec dbo.SEL_TicketList 5144, '', 'LOS', '2003-06-01', '2003-06-03'
exec dbo.SEL_TicketList 5144, '', 'MAIN', '2003-06-01', '2003-06-20'
exec dbo.SEL_TicketList 3217, '3011507', null, null, null

select * from users where uid=5144
select * from users_client where uid=5144
select top 10 * from users

select * from users where login_id = 'demo'
select * from users_client where uid=3217
*/
