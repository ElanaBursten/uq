if object_id('dbo.RPT_tse_payex_adhoc2') is not null
  drop procedure dbo.RPT_tse_payex_adhoc2
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_tse_payex_adhoc2 (
  @ManagerId int,
  @StartDate datetime,
  @EndDate datetime,
  @Criteria varchar(200),
  @EmployeeStatus int
)
as
  set nocount on

  create table #hourdata (
	emp_number varchar(20) NULL,
	short_name varchar(30) NULL,
	last_name varchar(30) NULL,
	first_name varchar(30) NULL,
	emp_id int NULL,
	h_active bit,
	h_eff_payroll_pc varchar(15),
	reg_hours decimal(38, 2) NULL,
	ot_hours decimal(38, 2) NULL,
	dt_hours decimal(38, 2) NULL,
	callout_hours decimal(38, 2) NULL,
	vac_hours decimal(38, 2) NULL,
	leave_hours decimal(38, 2) NULL,
	pto_hours decimal(38, 2) NULL,
	br_hours decimal(38, 2) NULL,
	hol_hours decimal(38, 2) NULL,
	jury_hours decimal(38, 2) NULL,
	working_hours decimal(38, 2) NULL,
	total_hours decimal(38, 2) NULL,
	pov_hours decimal(38, 2) NULL,
	cov_hours decimal(38, 2) NULL,
	report_end_date datetime NULL,
	rule_abbrev varchar(20) NULL,
	floating_holidays decimal(38, 2) NULL
  )

  insert into #hourdata
  select
   e.h_emp_number,
   e.h_short_name,
   e.h_last_name,
   e.h_first_name,
   e.h_emp_id,
   e.h_active,
   min(e.h_eff_payroll_pc) as h_eff_payroll_pc,
   coalesce(sum(reg_hours),0) as reg_hours,
   coalesce(sum(ot_hours),0) as ot_hours,
   coalesce(sum(dt_hours),0) as dt_hours,
   coalesce(sum(callout_hours),0) as callout_hours,
   coalesce(sum(vac_hours),0) as vac_hours,
   coalesce(sum(leave_hours),0) as leave_hours,
   coalesce(sum(pto_hours),0) as pto_hours,
   coalesce(sum(br_hours),0) as br_hours,
   coalesce(sum(hol_hours),0) as hol_hours,
   coalesce(sum(jury_hours),0) as jury_hours,

   coalesce(sum(reg_hours+ot_hours+dt_hours+callout_hours),0) as working_hours,
   coalesce(sum(reg_hours+ot_hours+dt_hours+callout_hours+pto_hours+
      vac_hours+leave_hours+br_hours+hol_hours+jury_hours+
      (case when coalesce(tse.floating_holiday, 0) = 1 then 8.00 else 0 end)
      ),0) as total_hours,

   coalesce(sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end),0) as pov_hours,
   coalesce(sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end),0) as cov_hours,

   @EndDate as report_end_date,
   max(reference.modifier) as rule_abbrev,
   sum(case when coalesce(tse.floating_holiday, 0) = 1 then 8.00 else 0 end) as floating_holidays  
  from (select * from dbo.get_report_hier3(@ManagerID, 0, 50, @EmployeeStatus)) e
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee work_emp on e.h_emp_id = work_emp.emp_id
   left join reference on reference.ref_id = work_emp.timerule_id
  group by e.h_emp_id, e.h_active, e.h_emp_number,
    e.h_short_name, e.h_last_name, e.h_first_name

  declare @sql varchar(300)
  set @sql = 'select * from #hourdata where (' + @Criteria + ') order by h_eff_payroll_pc, emp_number'
  exec (@sql)
go
grant execute on RPT_tse_payex_adhoc2 to uqweb, QManagerRole

/*
exec dbo.RPT_tse_payex_adhoc2 211, '2004-11-08', '2004-11-13', '1=1', 1
exec dbo.RPT_tse_payex_adhoc2 211, '2004-11-08', '2004-11-13', 'total_hours>4', 1
*/
