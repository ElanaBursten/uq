if object_id('dbo.remove_terms_from_billing_run') is not null
  drop procedure dbo.remove_terms_from_billing_run
GO

create Procedure remove_terms_from_billing_run(
	@BillID int,
	@ClientList varchar(800)
)
as
set nocount on

if not exists (select * from billing_header where bill_id=@BillID and committed=1) begin
  RAISERROR('Billing run must be committed', 18, 1) with log
  return
end

Declare @ErrorCode int
Select @ErrorCode = @@Error

Declare @TransactionCountOnEntry int
If @ErrorCode = 0
Begin
   Select @TransactionCountOnEntry = @@TranCount
   BEGIN TRANSACTION
End

select 'GOING TO REMOVE TERM', s from dbo.StringListToTable(@ClientList)

select 'BEFORE HEADER', n_tickets, n_locates, totalamount from billing_header where bill_id=@BillID

select 'BEFORE TOTALS', bd.billing_cc, count(*) as N
 from billing_detail bd
 inner join client c on bd.client_id=c.client_id
 where bd.bill_id=@BillID
group by billing_cc

If @ErrorCode = 0
Begin
	update locate
	set invoiced=0
	from billing_detail bd
	where
	 locate.locate_id = bd.locate_id
	  and bd.bill_id = @BillID
	  and bd.billing_cc in (select s from dbo.StringListToTable(@ClientList))
	  and ((locate_hours_id is null) or (locate_hours_id = 0))
   Select @ErrorCode = @@Error
End

If @ErrorCode = 0
Begin
	update locate_hours
	set locate_hours.hours_invoiced=0
	from billing_detail bd
	where
	  (bd.bill_id = @BillID)
	  and bd.billing_cc in (select s from dbo.StringListToTable(@ClientList))
	  and (locate_hours.locate_hours_id = bd.locate_hours_id)
	  and (bd.locate_hours_id > 0)
	  and (bd.bill_code = 'HOURLY')
   Select @ErrorCode = @@Error
End

If @ErrorCode = 0
Begin
	update locate_hours
	set locate_hours.units_invoiced=0
	from billing_detail bd
	where
	  (bd.bill_id = @BillID)
	  and bd.billing_cc in (select s from dbo.StringListToTable(@ClientList))
	  and (locate_hours.locate_hours_id = bd.locate_hours_id)
	  and (bd.locate_hours_id > 0)
	  and (bd.bill_code = 'UNITS')
   Select @ErrorCode = @@Error
End


If @ErrorCode = 0
Begin
	delete from billing_detail
	where bill_id = @BillID
	and billing_cc in (select s from dbo.StringListToTable(@ClientList))
   Select @ErrorCode = @@Error
End

If @ErrorCode = 0
Begin
	update billing_header
	set n_tickets = (select count(distinct ticket_number) from billing_detail where bill_id = @BillID),
	    n_locates = (select count(*) from billing_detail where bill_id = @BillID),
	    totalamount = (select sum(price) from billing_detail where bill_id = @BillID)
	where bill_id=@BillID
   Select @ErrorCode = @@Error
End

-- Select @ErrorCode = 12   -- force failure

select 'AFTER HEADER', n_tickets, n_locates, totalamount from billing_header where bill_id=@BillID

select 'AFTER TOTALS', bd.billing_cc, count(*) as N
 from billing_detail bd
 inner join client c on bd.client_id=c.client_id
 where bd.bill_id=@BillID
group by billing_cc

If @@TranCount > @TransactionCountOnEntry
Begin
   If @ErrorCode = 0 begin
      COMMIT TRANSACTION
   end Else begin
      ROLLBACK TRANSACTION
   end
End

Return @ErrorCode

GO

grant execute on remove_terms_from_billing_run to uqweb, QManagerRole

/*
exec dbo.remove_terms_from_billing_run 10, 'AAA,BBB,CCC'

exec dbo.remove_terms_from_billing_run 14426, 'MDCM01,MDCM02'

select top 10 * from billing_header
*/