if object_id('dbo.get_pending_responses_client') is not null
  drop procedure dbo.get_pending_responses_client
GO

create procedure get_pending_responses_client
  (
  @CallCenter varchar(20),
  @Client varchar(8000),
  @ParsedLocatesOnly bit = 1
  )
as
set nocount on

select top 800 rq.locate_id, locate.status, ticket.ticket_format,
 ticket.ticket_id, ticket.ticket_number, locate.client_code,
 ticket.ticket_type, ticket.work_state, rq.insert_date,
 ticket.serial_number, locate.closed_date, ticket.work_county,
 ticket.work_city, ticket.work_date, ticket.service_area_code,
 ticket.due_date,
   (select min(arrival_date) from jobsite_arrival ja
      where ja.ticket_id=ticket.ticket_id) as first_arrival_date
 from responder_queue rq (NOLOCK)
 inner join locate (NOLOCK) on rq.locate_id=locate.locate_id
 inner join ticket (NOLOCK) on locate.ticket_id=ticket.ticket_id
 where @CallCenter = rq.ticket_format
   and ticket.ticket_number not like 'MAN%'
   and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
   and rq.client_code in (select S from dbo.StringListToTable(@Client))
   and (rq.do_not_respond_before is null
     or rq.do_not_respond_before <= getdate())
 order by rq.insert_date
GO

grant execute on get_pending_responses_client to uqweb, QManagerRole
/*
exec get_pending_responses_client 'FMW1', 'PEP07'
exec get_pending_responses_client 'OCC2', 'DOM400'
exec get_pending_responses_client 'FCV3', 'VZN132,DOM820'
exec get_pending_responses 'FMW1'
exec get_pending_responses 'FDE1'

select ticket.ticket_format, min(insert_date) as oldest, count(*) as N
 from responder_queue rq (NOLOCK)
 inner join locate (NOLOCK) on rq.locate_id=locate.locate_id
 inner join ticket (NOLOCK) on locate.ticket_id=ticket.ticket_id
group by ticket.ticket_format
*/
