if object_id('dbo.RPT_billing_exception') is not null
  drop procedure dbo.RPT_billing_exception
GO

CREATE PROCEDURE RPT_billing_exception (
  @BeginDate datetime,
  @EndDate datetime,
  @CallCenters varchar(500),
  @Units int
)
AS

declare @CallCenterList table (
  cc_code varchar(20) not null
)

if @CallCenters <> '*'
begin
  insert into @CallCenterList
  select S from dbo.StringListToTable(@CallCenters)
end

-- collect bill_ids first, for better performance
declare @BillIDs table (
  bill_id int not null
)

insert into @BillIDs
select bill_id from billing_header
where bill_end_date >= @BeginDate
and bill_end_date <= @EndDate

declare @BillingData table
(
  bill_id int not null,
  bill_run_date datetime,
  bill_start_date datetime,
  bill_end_date datetime,
  --
  billing_detail_id int,
  locate_id int,
  ticket_number varchar(20),
  status varchar(5),
  bucket varchar(60),
  price money,
  rate money,
  locator_id int,
  line_item_text varchar(60),
  client_id int,
  qty_charged int,
  qty_marked int,
  locator_short_name varchar(40),
  billing_cc varchar(20),
  call_center varchar(20),
  ticket_id int,
  utility_type varchar(15)
)

insert into @BillingData
select bh.bill_id,
       bh.bill_run_date,
       bh.bill_start_date,
       bh.bill_end_date,
       bd.billing_detail_id,
       bd.locate_id,
       bd.ticket_number,
       bd.status,
       bd.bucket,
       bd.price,
       bd.rate,
       bd.locator_id,
       bd.line_item_text,
       bd.client_id,
       isnull(bd.qty_charged, 0),
       isnull(bd.qty_marked, 0),
       e.short_name,
       bd.billing_cc,
       c.call_center,
       l.ticket_id,
       c.utility_type
from billing_header bh
inner join billing_detail bd on (bd.bill_id = bh.bill_id)
inner join client c on (c.client_id = bd.client_id)
inner join locate l on (l.locate_id = bd.locate_id)
inner join employee e on (e.emp_id = bd.locator_id)
where exists (select bill_id from @BillIDs where bill_id = bh.bill_id)
and (@CallCenters = '*'
   or exists (select * from @CallCenterList cl
              where call_center = cl.cc_code))
-- link to call_center via client; using locate/ticket is too slow

-- "Duplicate Rated Utility Types" - Locate price > 0 for two electrics, for example
declare @TicketIDs table (
  t_ticket_id int not null,
  t_utility_type varchar(15),
  acount int
)
insert into @TicketIDs
select ticket_id, utility_type, count(price)
from @BillingData
where utility_type is not null and price > 0
group by ticket_id, utility_type
having count(price) >= 2

select * from @BillingData
where exists (select * from @TicketIDs ti
              where t_ticket_id = ticket_id and t_utility_type = utility_type)
and price > 0
order by call_center, ticket_number, utility_type, billing_cc

-- "Check for Multiple Locates" - Units billed (not marked) > X (X is a param), and Price > 0
select * from @BillingData
where price > 0 and isnull(qty_charged,0) > @Units
order by call_center, ticket_number, billing_cc

-- "Check Hourly Work" - Display all H status locates and detail on price/day
select * from @BillingData
where status = 'H' or status = 'OH'
order by call_center, ticket_number, billing_cc

-- "Callouts/Manual" - List a row for all After hours tickets and manual tickets
select * from @BillingData
where bucket like '%After Hours%'
   or ticket_number like 'MAN-%'
order by call_center, ticket_number, billing_cc

GO
grant execute on RPT_billing_exception to uqweb, QManagerRole

/*
exec RPT_billing_exception '2005-01-01', '2005-01-03', 'FCV2,FCV3', 0
exec RPT_billing_exception '2005-01-01', '2005-01-09', 'FCV3,Atlanta', 0
*/

