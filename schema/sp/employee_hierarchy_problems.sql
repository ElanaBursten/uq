if object_id('dbo.employee_hierarchy_problems') is not null
	drop procedure dbo.employee_hierarchy_problems
go

-- If this SP returns 0 rows, then the hierarchy is well-formed.
-- If it returns any rows, then the hierarchy needs to be fixed.

create proc employee_hierarchy_problems
as
set nocount on

declare @prob table (eid int, problem varchar(60))

if (select count(*) from employee where report_to is null) > 1
  insert into @prob
  select emp_id, 'Multiple "top" employees, please form a single hierarchy' from employee where report_to is null

insert into @prob
  select emp_id, 'Reports to invalid manager ID'
  from employee where report_to not in (select emp_id from employee)

-- Any hierarchy "loops" must be outside of the main rooted hierarchy(s).
-- Therefore to find the loops, simply find everyone under a root,
-- then whoever is left must be in a loop.

declare @e table (eid int not null primary key, parent int, found bit not null)

-- Get the raw data
insert into @e
select emp_id, report_to, 0 from employee

-- Mark the roots
update @e set found=1 where parent is null

-- recursively mark everyone under those people:
while @@rowcount>0
  update @e set found=1 where found=0
   and parent in (select eid from @e e2 where found=1)

-- Whoever is left, is in a loop; other than those with known
-- invalid report_to IDs
insert into @prob
select emp_id, 'Reporting loop'
 from employee
 where emp_id in (select eid from @e where found=0)
  and report_to in (select emp_id from employee)

select p.problem, employee.emp_id, employee.short_name
 from @prob p
 inner join employee on p.eid=employee.emp_id
 order by employee.short_name

go

grant execute on employee_hierarchy_problems to uqweb, QManagerRole
go

/*
exec dbo.employee_hierarchy_problems
*/
