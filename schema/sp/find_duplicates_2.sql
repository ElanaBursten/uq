if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[find_duplicates_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[find_duplicates_2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[find_duplicates_renotif_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[find_duplicates_renotif_2]
GO

CREATE PROCEDURE find_duplicates_2 (
	@ticket_number varchar(20), 
	@ticket_format varchar(20),
	@year varchar(4)
)
AS

declare @ticket_ids table (
  ticket_id int
)

declare @locate_ids table (
  locate_id int
)

insert into @ticket_ids
select ticket_id from ticket
where ticket_number = @ticket_number and ticket_format = @ticket_format
  and year(call_date) = @year and ticket.active = '1'

insert into @locate_ids
select locate_id from locate
where ticket_id in (select ticket_id from @ticket_ids)

/* return three sets: */

select * from ticket
where ticket_id in (select ticket_id from @ticket_ids)

select * from locate
where locate_id in (select locate_id from @locate_ids)

select * from assignment a
where a.locate_id in (select locate_id from @locate_ids)
and a.active = 1
GO
grant execute on find_duplicates_2 to uqweb, QManagerRole
go

CREATE PROCEDURE find_duplicates_renotif_2 (
	@ticket_number varchar(20),
	@ticket_format varchar(20),
	@transmit_date varchar(20),
	@year varchar(4)
)
AS

declare @ticket_ids table (
  ticket_id int
)

declare @locate_ids table (
  locate_id int
)

insert into @ticket_ids
select ticket_id from ticket 
where ticket_number = @ticket_number and ticket_format = @ticket_format
and transmit_date = @transmit_date and ticket.active = '1'
and year(call_date) = @year

insert into @locate_ids
select locate_id from locate
where ticket_id in (select ticket_id from @ticket_ids)

/* return three sets: */

select * from ticket
where ticket_id in (select ticket_id from @ticket_ids)

select * from locate
where locate_id in (select locate_id from @locate_ids)

select * from assignment a
where a.locate_id in (select locate_id from @locate_ids)
and a.active = 1
GO
grant execute on find_duplicates_renotif_2 to uqweb, QManagerRole
go


/*
exec dbo.find_duplicates_2 'C0083890', 'FCL1', '2005'
exec dbo.find_duplicates_renotif_2 'C0083890', 'FCL1', '2005-03-14', '2005'
*/
