if object_id('dbo.timesheet_entry_u') is not null -- this is intentional to merge this into the new iu trigger
  drop trigger dbo.timesheet_entry_u 
go
if object_id('dbo.timesheet_entry_iu') is not null
  drop trigger dbo.timesheet_entry_iu
go

create trigger timesheet_entry_iu on timesheet_entry
for insert, update
not for replication
as
begin
  set nocount on

  update timesheet_entry set modified_date = GetDate()
    where entry_id in (select entry_id from inserted)

  update timesheet_entry 
    set work_pc_code = dbo.get_historical_pc(work_emp_id, work_date)
  where entry_id in (select entry_id from inserted)
    and work_pc_code is null 
    and (status='ACTIVE' or status='SUBMIT') 
end
go
