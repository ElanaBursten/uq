if object_id('dbo.get_report_hier2') is not null
	drop function dbo.get_report_hier2
go

create function get_report_hier2(@id int, @managers_only bit, @level_limit int)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_short_name_tagged varchar(45) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_active bit,
  h_report_level integer,
  h_report_to integer,
  h_report_to_short_name varchar(30) NULL,
  h_eff_payroll_pc varchar(15), -- Not in get_report_hier
  h_repr_pc_code varchar(15), -- Not in get_report_hier
  h_namepath varchar(450) NULL,
  h_idpath varchar(160) NULL,
  h_lastpath varchar(450) NULL,  -- Not in get_report_hier
  h_numpath varchar(450) NULL, -- Not in get_report_hier
  h_1 varchar(30) NULL,
  h_2 varchar(30) NULL,
  h_3 varchar(30) NULL,
  h_4 varchar(30) NULL,
  h_5 varchar(30) NULL,
  h_6 varchar(30) NULL,
  h_7 varchar(30) NULL,
  h_8 varchar(30) NULL,
  h_9 varchar(30) NULL
)
as
begin
  if @level_limit>= 1000 begin
    -- Figure out what PC to limit to,
    -- and get all emp PCs
    declare @pc varchar(15)
    declare @emps TABLE (
      emp_id integer NOT NULL PRIMARY KEY,
      x integer,
      emp_pc_code varchar(15) NULL
    )

    -- 1000 = flat mode for the PC of the named manager
    if @level_limit = 1000 begin
      select @pc = dbo.get_employee_pc(@id, 0)
      insert into @emps select * from employee_pc_data(@pc)
    end

    -- 1001 = flat mode for the PC of the named manager, with Payroll PC overrides
    if @level_limit = 1001 begin
      select @pc = dbo.get_employee_pc(@id, 1)
      insert into @emps select * from employee_payroll_data(@pc)
    end

    -- @emps has everyone with their PCs filled in,
    -- X is the emp_id of who each person inherited their PC from.
    -- those where X is NULL, are the "top" of each PC.

    -- TODO: patch up X for the overrides in employee_payroll_data

    -- Put in the top level of PC representative people:

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_level,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, 1, 1,
        e.short_name, convert(varchar(20), e.emp_id),
        e.last_name+convert(varchar(20), e.emp_id),
        coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        e.short_name
      from @emps em
       inner join employee e on em.emp_id=e.emp_id
      where em.x is null --and e.repr_pc_code is not null

    -- put in the second level, which is everyone else
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        r.h_namepath + ';' + e.short_name, r.h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        r.h_1, e.short_name
      from @emps em
       inner join employee e on em.emp_id=e.emp_id
       inner join @results r on em.x = r.h_emp_id

  end else begin

    -- Add the user
    INSERT INTO @results
      (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
       h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
       h_namepath, h_idpath,  h_lastpath, h_numpath,
       h_1)
    select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
       e.repr_pc_code, e.type_id, e.active, e.report_to, 1, man.short_name,
       e.short_name, convert(varchar(20), e.emp_id),
       e.last_name+convert(varchar(20), e.emp_id),
       coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
       e.short_name
      from employee e
      left join employee man on e.report_to = man.emp_id
     where e.emp_id = @id

    -- Add everyone below the user; hideously ugly duplication,
    -- but I'd need to use code gen to get rid of it.

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=1
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 2 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=2
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 3 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=3
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 4 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4, h_5)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=4
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 5 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4, h_5, h_6)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=5
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 6 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=6
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 7 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=7
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 8 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath,
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id),
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=8
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 9 and ((@managers_only=0) or (et.modifier like '%MGR%'))
  end

  update @results set h_eff_payroll_pc = dbo.get_employee_pc(h_emp_id, 1)

  update @results
   set h_short_name_tagged = 
    case when h_active=1 then h_short_name
                         else h_short_name + ' (inactive)'
    end

  return
end
GO

-- No grant necessary for functions

/*
select * from get_report_hier2(211, 1, 10)
select * from get_report_hier2(211, 0, 10) order by h_namepath
select * from get_report_hier2(2676, 1, 10)
select * from get_report_hier2(2676, 0, 10)
select * from get_report_hier2(2676, 0, 2)
select * from get_report_hier2(1787, 0, 10)
select * from get_report_hier2(2302, 0, 10)

select * from get_report_hier2(2302, 0, 1000)

select * from get_report_hier2(0, 0, 1000) order by h_namepath

select * from employee where repr_pc_code is not null

select dbo.get_employee_pc(2302, 0)
select * from employee_pc_data('139') order by 1
select * from get_report_hier2(2302, 0, 1000)

select dbo.get_employee_pc(6333, 0)
select * from employee_pc_data('176') order by 1
select * from get_report_hier2(6333, 0, 1000)

select dbo.get_employee_pc(1787, 0)
select * from employee_pc_data('111') order by 1
select * from get_report_hier2(1787, 0, 1001)
*/

