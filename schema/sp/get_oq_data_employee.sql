--QM-444 Returns valid OQ qualifications for an Employee

if object_id('dbo.get_oq_data_employee') is not null
  drop procedure dbo.get_oq_data_employee
go
create procedure dbo.get_oq_data_employee
   @EmpID integer

as
select oq.emp_id, oq.qualified_date, oq.expired_date, r.description, r.modifier, oq.status 
from oq_data oq
inner join reference r on oq.ref_id = r.ref_id
inner join reference r2 on oq.status = r2.code
where (oq.emp_id = @EmpID) and
      (oq.qualified_date <= GetDate()) and
	  (oq.expired_date > (SELECT CAST(GETDATE() AS Date)) and
	  (r2.type = 'oq_data') and
	  (UPPER(oq.status) = UPPER(r2.code)) and
	  (oq.active = 1) and
      (r2.modifier = '1'))

GO


--select * from reference where type = 'dmgco'
--select * from reference where type = 'oq_data'
--get_oq_data_employee 4736

--select * from oq_data

