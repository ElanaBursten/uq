------------------------------------------------
-- QM-585 Employee PLUS WhiteList: remove_employee_PLUS
-- REMOVES the employee to a Whitelist.  Sets any inactive record to 0
-- If an employee exists, active flag is reset to 0 (FALSE)
-- Note: It does not add a record if no record exists
-- EB Created 6/2022
------------------------------------------------
if object_id('dbo.remove_employee_PLUS') is not null
  drop procedure dbo.remove_employee_PLUS
GO

CREATE procedure [dbo].[remove_employee_PLUS] (
@locator_id int, 
@plus_id int,
@modified_by_id int)
as 
  update employee_plus
	set active=0, modified_date=GetDate(), modified_by_id = @modified_by_id
	where (emp_id = @locator_id) and (term_group_id = @plus_id)
GO

--select * from employee where active=1 and emp_id = '11458'
--
--  exec add_employee_PLUS 3512, 9209, 15897, 'GAS1'
--   remove_employee_PLUS 3512, 9209, 15897

--select e.short_name, ep.* from employee_plus ep 
--inner join employee e on ep.emp_id = e.emp_id


