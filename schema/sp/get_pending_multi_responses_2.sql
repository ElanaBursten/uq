if object_id('dbo.get_pending_multi_responses_2') is not null
	drop procedure dbo.get_pending_multi_responses_2
GO

CREATE Procedure get_pending_multi_responses_2(@RespondTo varchar(20),
 @CallCenter varchar(20), @ParsedLocatesOnly bit = 1)
as
set nocount on

if @CallCenter=''

  select rq.locate_id, locate.status, ticket.ticket_format,
   ticket.ticket_id, ticket.ticket_number, locate.client_code,
   ticket.ticket_type, ticket.work_state, rq.insert_date,
   ticket.serial_number, locate.closed_date, ticket.work_county,
   ticket.work_city, ticket.work_date, ticket.service_area_code,
   ticket.due_date,
   (select min(arrival_date) from jobsite_arrival ja
      where ja.ticket_id=ticket.ticket_id) as first_arrival_date
   from responder_multi_queue rq
   inner join locate on rq.locate_id=locate.locate_id
   inner join ticket on locate.ticket_id=ticket.ticket_id
  where
    rq.respond_to = @RespondTo
    -- (rq.do_not_respond_before is null
    --   or rq.do_not_respond_before <= getdate())
    and ticket.ticket_number not like 'MAN%'
    and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
    and not exists (select * from response_log rl
      where rl.reply = '(ftp waiting)'
      and rl.locate_id = rq.locate_id
      and DateDiff(minute, rl.response_date, GetDate()) < 240)
  order by rq.insert_date

else

  select top 800
   rq.locate_id, locate.status, ticket.ticket_format,
   ticket.ticket_id, ticket.ticket_number, locate.client_code,
   ticket.ticket_type, ticket.work_state, rq.insert_date,
   ticket.serial_number, locate.closed_date, ticket.work_county,
   ticket.work_city, ticket.work_date, ticket.service_area_code,
   ticket.due_date, e.short_name as locator_name,
   coalesce((select info from ticket_info ti2 where ti2.ticket_info_id =
    (select max(ticket_info_id) from ticket_info ti
      where ti.ticket_id = locate.ticket_id
      and info_type='ONGOCONT')), '') as contact,
   (select min(arrival_date) from jobsite_arrival ja
      where ja.ticket_id=ticket.ticket_id) as first_arrival_date
   from responder_multi_queue rq
   inner join locate on rq.locate_id=locate.locate_id
   inner join ticket on locate.ticket_id=ticket.ticket_id
   left join employee e on e.emp_id = locate.assigned_to_id
  where
   rq.ticket_format = @CallCenter
   and rq.respond_to = @RespondTo
   -- and (rq.do_not_respond_before is null
   --      or rq.do_not_respond_before <= getdate())
   and ticket.ticket_number not like 'MAN%'
   and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
   and not exists (select * from response_log rl
      where rl.reply = '(ftp waiting)'
      and rl.locate_id = rq.locate_id
      and DateDiff(minute, rl.response_date, GetDate()) < 240)
  order by insert_date
GO

grant execute on get_pending_multi_responses_2 to uqweb, QManagerRole

/*
exec get_pending_multi_responses_2 'client', ''
exec get_pending_multi_responses_2 'client', '3003'
*/
