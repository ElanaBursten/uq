/* Vicenty formula to calculate distance  between two sets of lat/long coordinates, SQL           */
/* adaptation further adapted here to return meters:                                              */
/* http://interactiva.com.ar/mariano/tips/2008/02/geodesic_distance_in_microsoft.html             */
/*                                                                                                */
/* and was originally adapted from here (and copy of original credit/comment follows):            */
/* http://www.movable-type.co.uk/scripts/latlong-vincenty.html                                    */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* Vincenty Inverse Solution of Geodesics on the Ellipsoid (c) Chris Veness 2002-2010             */
/*                                                                                                */
/* from: Vincenty inverse formula - T Vincenty, "Direct and Inverse Solutions of Geodesics on the */
/*       Ellipsoid with application of nested equations", Survey Review, vol XXII no 176, 1975    */
/*       http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf                                             */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

if object_id('dbo.geoVicentyDistanceMeters') is not null
  drop function dbo.geoVicentyDistanceMeters
go

create function dbo.geoVicentyDistanceMeters (@lat1 float, @long1 float,
                                              @lat2 float, @long2 float)
returns float
as
BEGIN
  if (@lat1=0) OR (@long1=0) or (@lat2=0) or (@long2=0) begin
    return NULL
  end
  
  --Make sure long is always negative.  Some of the call centers send both over as positives.  
  SET @long1 = -Abs(@long1) 
  SET @long2 = -Abs(@long2) 

	declare	@a float, @b float, @f float, -- WGS-84 ellipsiod
			@L float, @S float, @U1 float, @U2 float, 
			@sinU1 float, @sinU2 float, @cosU1 float, @cosU2 float, 
			@lambda float, @lambdaP float, @sinLambda float, 
			@cosLambda float, @sinSigma float, @cosSigma float,
			@sigma float, @sinAlpha float, @cosSqAlpha float, 
			@cos2SigmaM float, @C float, @uSq float, 
			@AA float, @BB float, @deltaSigma float, 
			@iterLimit int

	select	@a = 6378137, 
			@b = 6356752.3142,  
			@f = 1/298.257223563,
			@L = RADIANS(@long2 - @long1), 
			@U1 = ATAN( (1 - @f) * TAN(RADIANS(@lat1))),
			@U2 = ATAN( (1 - @f) * TAN(RADIANS(@lat2))),
			@sinU1 = SIN(@U1), @sinU2 = SIN(@U2),
			@cosU1 = cos(@U1), @cosU2 = cos(@U2),
			@lambda = @L , @lambdaP = 2 * PI(),
			@iterLimit = 20

	while(abs(@lambda - @lambdaP) > 1e-12 and @iterLimit > 0)
	BEGIN
		
		select	@sinLambda = sin(@lambda),
				@cosLambda = cos(@lambda),
				@sinSigma = sqrt(	power(@cosU2 * @sinLambda, 2) + 
									power(@cosU1 * @sinU2 - @sinU1 * @cosU2 * @cosLambda, 2)) 
		if @sinSigma = 0 break
		
		select	@cosSigma = @sinU1 * @sinU2 + @cosU1 * @cosU2 * @cosLambda,
				@sigma = atn2(@sinSigma, @cosSigma),
				@sinAlpha = @cosU1 * @cosU2 * @sinLambda / @sinSigma,
				@cosSqAlpha = 1 - @sinAlpha * @sinAlpha,
				@cos2SigmaM = @cosSigma - 2 * @sinU1 * @sinU2 / @cosSqAlpha
	    
		if (isnumeric(@cos2SigmaM) <> 1) select @cos2SigmaM = 0 --equatorial line: cosSqAlpha=0 (�6)

		select	@C = @f / 16 * @cosSqAlpha * ( 4 + @f *( 4 - 3 * @cosSqAlpha)),
				@lambdaP = @lambda,
				@lambda = @L + (1 - @C) * @f * @sinAlpha *
						  (@sigma + @C * @sinSigma * 
									(@cos2SigmaM + @C * @cosSigma * 
										(-1 + 2 * @cos2SigmaM * @cos2SigmaM)))
	END

	if (@iterLimit = 0) select	@S = 0
	ELSE BEGIN
		select	@uSq = @cosSqAlpha * (@a * @a - @b * @b) / (@b * @b),
				@AA = 1 + @uSq / 16384 * ( 4096 + @uSq * (-768 + @uSq * (320 - 175 * @uSq))),
				@BB = @uSq / 1024 * (256 + @uSq * (-128 + @uSq * (74 - 47 * @uSq))),
				@deltaSigma = @BB * @sinSigma * (@cos2SigmaM + @BB / 4 * 
														(@cosSigma * (-1 + 2 * @cos2SigmaM * @cos2SigmaM)-
														@BB / 6 * @cos2SigmaM * 
															(-3 + 4 * @sinSigma * @sinSigma) * 
															(-3 + 4 * @cos2SigmaM * @cos2SigmaM))),
				@S = @b * @AA * (@sigma - @deltaSigma) --return distance in meters   
	END

	return 	@S
END;
go

grant execute on geoVicentyDistanceMeters to uqweb, QManagerRole
go

/*
select dbo.geoVicentyDistanceMeters(30.229273,-95.564872,30.228740,-95.564760)
select dbo.geoVicentyDistanceMeters(30.229273,95.564872,30.228740,95.564760)
select dbo.geoVicentyDistanceMeters(30.229273,-95.564872,0,0)
*/

