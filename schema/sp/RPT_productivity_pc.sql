if object_id('dbo.RPT_productivity_pc') is not null
  drop procedure dbo.RPT_productivity_pc
go

create proc RPT_productivity_pc (
  @DateFrom datetime,
  @DateTo datetime,
  @ProfitCenters varchar(2000) = null
)
as
  set nocount on

  declare @results table (
    profit_center varchar(15) not null,
    work_date_start datetime not null,
    work_date_end datetime,
    tickets_received integer,
    locates_received integer,
    tickets_closed integer,
    locates_closed integer,
    locates_reclosed integer,
    production_hours decimal(8,2),
    tickets_closed_per_hour decimal(8,3),
    locates_closed_per_hour decimal(8,3),
    primary key (profit_center, work_date_start)
    )

  declare @pcs table (
    _pc_code varchar(15) primary key
    )

  declare @closed_status table (
    _status varchar(10)
    )

  -- List of profit centers selected for the report; blank or null for all
  if IsNull(@ProfitCenters, '') = ''  insert into @pcs select pc_code from profit_center
  else insert into @pcs select * from dbo.StringListToTable(@ProfitCenters)

  -- Only need active statuses that mean a locate is closed
  insert into @closed_status select status from statuslist 
    where complete=1 and active=1

  /* 
	-- Gathers all dates & pcs in the range regardless of activity or not
	set @DateTo = DateAdd(day, -1, @DateTo)
  insert into @results 
    select pc_code, D, DateAdd(day, 1, D), 0, 0, 0, 0, 0, 0.0, 0.0, 0.0
    from dbo.DateSpan(@DateFrom, @DateTo), profit_center
    inner join @pcs on pc_code = _pc_code
  */


	-- Gather dates with some activity for all selected profit centers
  insert into @results (profit_center, work_date_start)
    select distinct work_pc_code, Convert(datetime, Convert(varchar, work_date, 101))
    from timesheet_entry inner join @pcs on work_pc_code = _pc_code
    where work_date >= @DateFrom and work_date < @DateTo

    union select distinct first_receive_pc, Convert(datetime, Convert(varchar, first_receive_date, 101))
    from ticket_snap inner join @pcs on first_receive_pc = _pc_code
    where first_receive_date >= @DateFrom and first_receive_date < @DateTo
    
    union select distinct first_close_pc, Convert(datetime, Convert(varchar, first_close_date, 101))
    from ticket_snap inner join @pcs on first_close_pc = _pc_code
    where first_close_date >= @DateFrom and first_close_date < @DateTo
    
    union select distinct first_receive_pc, Convert(datetime, Convert(varchar, first_receive_date, 101))
    from locate_snap inner join @pcs on first_receive_pc = _pc_code
    where first_receive_date >= @DateFrom and first_receive_date < @DateTo
    
    union select distinct first_close_pc, Convert(datetime, Convert(varchar, first_close_date, 101))
    from locate_snap inner join @pcs on first_close_pc = _pc_code
    where first_close_date >= @DateFrom and first_close_date < @DateTo

    union select distinct _pc_code, Convert(datetime, Convert(varchar, stat.status_date, 101))
    from locate_status stat  
      inner join locate_snap snap on stat.locate_id = snap.locate_id
      inner join @pcs on dbo.get_historical_pc(stat.statused_by, stat.status_date) = _pc_code
    where stat.status_date >= @DateFrom and stat.status_date < @DateTo
      and stat.status_date > snap.first_close_date 
      and stat.status in (select _status from @closed_status)

  update @results
    set work_date_end = DateAdd(day, 1, work_date_start)

  update @results
    set tickets_received = (select count(*) from ticket_snap 
      where first_receive_pc = profit_center and first_receive_date >= work_date_start and first_receive_date < work_date_end)

  update @results
    set tickets_closed = (select count(*) from ticket_snap 
      where first_close_pc = profit_center and first_close_date >= work_date_start and first_close_date < work_date_end)

  update @results
    set locates_received = (select count(*) from locate_snap 
      where first_receive_pc = profit_center and first_receive_date >= work_date_start and first_receive_date < work_date_end)
  
  update @results
    set locates_closed = (select count(*) from locate_snap 
      where first_close_pc = profit_center and first_close_date >= work_date_start and first_close_date < work_date_end)
  
  update @results
    set locates_reclosed = (select count(*) from locate_status stat 
      inner join locate_snap snap on stat.locate_id = snap.locate_id
      where stat.status_date >= work_date_start and stat.status_date < work_date_end
        and dbo.get_historical_pc(stat.statused_by, stat.status_date) = profit_center
        and stat.status_date > snap.first_close_date 
        and stat.status in (select _status from @closed_status))

  update @results
    set production_hours = (select IsNull(sum(t.reg_hours) + sum(t.ot_hours) + sum(t.dt_hours) + sum(t.callout_hours), 0)
      from timesheet_entry t inner join reference r on (r.ref_id = t.emp_type_id)
      where t.work_pc_code = profit_center and t.work_date = work_date_start
        and t.status in ('ACTIVE','SUBMIT') and r.modifier like '%LOC%')

  update @results
    set tickets_closed_per_hour = tickets_closed / production_hours, 
      locates_closed_per_hour = locates_closed / production_hours
    where production_hours <> 0

  -- results by profit center
  select profit_center, 
    work_date_start as work_date, 
    IsNull(tickets_received, 0) as tickets_received,
    IsNull(locates_received, 0) as locates_received,
    IsNull(tickets_closed, 0) as tickets_closed,
    IsNull(locates_closed, 0) as locates_closed,
    IsNull(locates_reclosed, 0) as locates_reclosed,
    IsNull(production_hours, 0.0) as production_hours, 
    IsNull(tickets_closed_per_hour, 0.0) as tickets_closed_per_hour,
    IsNull(locates_closed_per_hour, 0.0) as locates_closed_per_hour
  from @results where profit_center in (select profit_center from @results 
	  group by profit_center 
		having sum(tickets_received + locates_received + tickets_closed + locates_closed + production_hours) <> 0)
  order by profit_center, work_date

  -- For debugging only:
  /*
  select * from @pcs
  select * from @closed_locates
  */ 
go

grant execute on RPT_productivity_pc to uqweb, QManagerRole

/*
exec RPT_productivity_pc '2007-08-05', '2007-08-11', Null
exec RPT_productivity_pc '2007-08-05', '2007-08-11', 'FCV,FXL,007'
exec RPT_productivity_pc '2007-08-01', '2007-09-01', Null
exec RPT_productivity_pc '2007-08-01', '2007-09-01', 'FCV,FXL,007'
exec RPT_productivity_pc '2007-10-15', '2007-11-01', 'FCV,FXL,007'
exec RPT_productivity_pc '2007-10-01', '2007-11-01', null
exec RPT_productivity_pc '2007-10-27', '2007-10-28', null
*/


