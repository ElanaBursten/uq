/* Create ticket_snap row & set first_recveive_date */
if object_id('dbo.ticket_i') is not null
  drop trigger dbo.ticket_i
go

create trigger ticket_i on ticket
for insert 
not for replication
as begin
  set nocount on

  insert into ticket_snap (ticket_id, first_receive_date)
    select inserted.ticket_id, GetDate()
    from inserted left join ticket_snap on inserted.ticket_id = ticket_snap.ticket_id
    where ticket_snap.ticket_id is null
end
go

