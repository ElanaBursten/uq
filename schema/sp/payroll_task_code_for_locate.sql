if object_id('dbo.payroll_task_code_for_locate') is not null
  drop function dbo.payroll_task_code_for_locate
GO

create function payroll_task_code_for_locate (@locate_id int) 
returns varchar(55)
as
begin
  -- Payroll task code is CCCSSSSSSLLLLLL:
  --   CCC    = !!!! NO LONGER POPULATED HERE !!!!
  --   SSSSSS = Supervisor emp number 
  --   LLLLLL = Locator emp number
  declare @OutStr varchar(55)

  set @OutStr = Coalesce((select 
    dbo.format_emp_number(s.emp_number) + 
    dbo.format_emp_number(e.emp_number)
    from locate_snap l
    inner join employee_history e on l.first_close_emp_id = e.emp_id
    left join employee s on e.report_to = s.emp_id
    where locate_id = @locate_id 
    and e.active_start <= l.first_close_date and e.active_end > l.first_close_date), '000000000000')

  return @OutStr
end
go

grant execute on payroll_task_code_for_locate to uqweb, QManagerRole

/*
  select top 100 dbo.payroll_task_code_for_locate(bd.locate_id) as payroll_task_code, bd.locate_id, bd.closed_date, bd.locator_id from billing_detail bd
*/
