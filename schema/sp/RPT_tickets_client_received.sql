if object_id('dbo.RPT_tickets_clients_received') is not null
	drop procedure dbo.RPT_tickets_clients_received
go

create proc RPT_tickets_clients_received (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(800),
  @ClientList varchar(800)
)
as
  set nocount on 

  -- This result set is designed such that a nearly default crosstab
  -- will generate the desired results; hence the user friendly column names,
  -- space before Tickets to make it sort first, etc.

  if @ClientList = '' begin
  -- Call center numbers only, no client info needed

  select
    call_center as "Center",
    ticket_date as "Date",
    ' Tickets' as "Term",
    count(ticket_id) as "Received",
    '-' as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
    where ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date between @StartDate and @EndDate
    ) as t
  group by
    call_center,
    ticket_date

  end else begin

  select
    call_center as "Center",
    ticket_date as "Date",
    ' Tickets' as "Term",
    count(ticket_id) as "Received",
    '-' as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date between @StartDate and @EndDate
    ) as t
  group by
    call_center,
    ticket_date

  union

  -- second result set: total by call center, date, date, client
  select
    call_center as "Center",
    ticket_date as "Date",
    client_code as "Term",
    count(ticket_id) as "Received",
    '-' as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      locate.client_code,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
     inner join locate with (index(locate_ticketid))
       on ticket.ticket_id=locate.ticket_id
    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date between @StartDate and @EndDate
     and (@ClientList = '*' or  
          locate.client_code in (select s from dbo.StringListToTable(@ClientList)))
     and locate.status <> '-N'
    ) as t
  group by
    t.call_center,
    t.ticket_date,
    client_code
  order by 1, 2, 3

  /*
  -- extra, small result set, for debugging
  select top 100
    ticket.ticket_format as call_center,
    ticket.transmit_date,
    ticket.ticket_number,
    ticket.ticket_id,
    locate.client_code
  from ticket
   inner join locate with (index(locate_ticketid))
     on ticket.ticket_id=locate.ticket_id
  where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
   and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
   and (locate.client_code in (select s from dbo.StringListToTable(@ClientList)))
  */
  end
go

grant execute on RPT_tickets_clients_received to uqweb, QManagerRole

/*
-- This runs very fast with no CLIENT data
-- and reasonably so with Client data
exec dbo.RPT_tickets_clients_received '2004-07-21', '2004-07-26', 'OCC2', '*'

exec dbo.RPT_tickets_clients_received '2004-08-01', '2004-08-22', 'FTS1', ''
exec dbo.RPT_tickets_clients_received '2004-05-16', '2004-05-17', 'FAQ1', '*'
exec dbo.RPT_tickets_clients_received '2004-09-29', '2004-09-30', 'FGV1', '*'
exec dbo.RPT_tickets_clients_received '2004-09-29', '2004-09-30', 'FGV1', 'TWGZ98'
exec dbo.RPT_tickets_clients_received '2004-09-14', '2004-09-16', 'FAM1', 'BEMC01,BGSC01,MDCM02,MOBG01,MOBG02,SCBP01,SCMB01'

*/
