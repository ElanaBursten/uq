if object_id('dbo.assign_locate') is not null
  drop procedure dbo.assign_locate
GO

create proc assign_locate (@LocateID int, @LocatorID varchar(20), @AddedBy int = null, @WorkloadDate datetime = null)
as
  begin transaction
    select locate_id from locate with (XLOCK,ROWLOCK) where locate_ID = @LocateID

    update assignment set active = 0
    where (locate_id = @LocateID)
      and (active = 1)

    declare @TicketID int
    set @TicketID = (select ticket_id from locate where locate_id = @LocateID)
    set @WorkloadDate = (select coalesce(@WorkloadDate, due_date) from ticket where ticket_id = @TicketID)

    -- Assign a locate to an employee
    insert into assignment(locate_id, locator_id, added_by, workload_date)
    values (@LocateID, @LocatorID, @AddedBy, @WorkloadDate)

    -- Move any pending ticket_ack records to point to the new locator
    update ticket_ack set locator_emp_id = @LocatorID
    where ticket_id = @TicketID and has_ack = 0 and ((locator_emp_id <> @LocatorID) or (locator_emp_id is null))

    -- Deactivate any first tasks defined for the ticket if it is assigned to a different locator
    update task_schedule set active = 0
    where (ticket_id = @TicketID) and (emp_id <> @LocatorID)

  commit transaction
GO

grant execute on assign_locate to uqweb, QManagerRole
GO
