/* Equivalent to RPT_tse_manager except in how it handles
 * reporting on inactive employees.
 * You may need to update RPT_tse_emp when you update this
 *
 * EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

if object_id('dbo.RPT_tse_manager5') is not null
  drop procedure dbo.RPT_tse_manager5
go

create proc RPT_tse_manager5(
  @ManagerID Int,
  @EndDate datetime,
  @LevelLimit int,
  @EmployeeStatus int
)
as
 set nocount on
 declare @StartDate datetime
 set @StartDate = dateadd(d, -6, @EndDate)

 select e.h_first_name as first_name,
   e.h_last_name as last_name,
   e.h_emp_number as emp_number,
   e.h_short_name as short_name,
   e.h_emp_id as emp_id,
    dd.d as display_work_date,
    tse.*,
    Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0) as mileage,
    reference.modifier as rule_abbrev,
    ent_emp.emp_number as ent_emp_number,
    ent_emp.short_name as ent_emp_short_name,
    app_emp.emp_number as app_emp_number,
    app_emp.short_name as app_emp_short_name,
    fap_emp.emp_number as fap_emp_number,
    fap_emp.short_name as fap_emp_short_name,
    @EndDate as report_end_date,
    case when final_approve_by is not null then 'FINAL APPROVED'
         when approve_by is not null       then 'APPROVED'
         when entry_by is not null         then 'ENTERED'
         else                                   '-'
    end as day_status,
    e.h_report_level, e.h_namepath, e.h_idpath,
    e.h_numpath, e.h_lastpath,
    et.description as emp_type,
    case when tse.floating_holiday = 1 then 1 else 0 end as floating_holiday_count,
	case when tse.per_diem = 1 then 1 else 0 end as per_diem_count,
	case when tse.per_diem= 1 then 45 else 0 end as per_diem_amt
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   inner join dbo.DateSpan(@StartDate, @EndDate) dd on 1=1
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id and tse.work_date=dd.d
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee ent_emp on tse.entry_by = ent_emp.emp_id
   left join employee app_emp on tse.approve_by = app_emp.emp_id
   left join employee fap_emp on tse.final_approve_by = fap_emp.emp_id
   left join employee work_emp on e.h_emp_id = work_emp.emp_id
   left join reference on reference.ref_id = work_emp.timerule_id
   and e.h_emp_id in (select distinct work_emp_id
     from timesheet_entry tse
      where tse.work_date between @StartDate and @EndDate)
   left join reference et on tse.emp_type_id = et.ref_id
  order by e.h_namepath, e.h_last_name, e.h_emp_id, dd.d

  -- totals by day of week
  select datepart(weekday, tse.work_date) as dow,
   min(tse.work_date) as dow_date,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,
   sum(Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0)) as mileage,
   sum(case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday_count,
   sum(case when tse.per_diem = 1 then 1 else 0 end) as per_diem_count,
   sum(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   inner join timesheet_entry tse
     on tse.work_emp_id = e.h_emp_id
  where tse.work_date between @StartDate and @EndDate
   and (tse.status='ACTIVE' or tse.status='SUBMIT')
   group by datepart(weekday, tse.work_date)
   order by 1

  -- totals for each emp
  select
   min(e.h_emp_id) as emp_id,

   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,

   sum(reg_hours+ot_hours+dt_hours) as working_hours,

   sum( case when vehicle_use='POV' then reg_hours+ot_hours+dt_hours else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%' then reg_hours+ot_hours+dt_hours else null end) as cov_hours,
   sum(Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0)) as mileage,
   sum(case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday_count,
   sum(case when tse.per_diem = 1 then 1 else 0 end) as per_diem_count,
   sum(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
  group by e.h_emp_id
GO

grant execute on RPT_tse_manager4 to uqweb, QManagerRole

/*
exec dbo.rpt_tse_manager4 13776, '2013-01-13', 99, 1
exec dbo.RPT_tse_manager4 3510, '2007-08-25', 2, 1
exec dbo.RPT_tse_manager4 211, '2004-09-25', 2, 1
exec dbo.RPT_tse_manager4 211, '2004-09-25', 2, 0
exec dbo.RPT_tse_manager4 211, '2004-09-25', 2, -1
*/

