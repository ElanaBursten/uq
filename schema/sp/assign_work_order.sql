if object_id('dbo.assign_work_order') is not null
  drop procedure dbo.assign_work_order
GO

create proc assign_work_order (@WorkOrderID integer, @WorkerID integer, @AddedBy varchar(8))
as
  begin transaction

    select wo_id from work_order with (XLOCK, ROWLOCK) 
    where wo_id = @WorkOrderID

    update wo_assignment set active = 0
    where (wo_id = @WorkOrderID)
      and (active = 1)

    insert into wo_assignment(wo_id, assigned_to_id, added_by) values (@WorkOrderID, @WorkerID, @AddedBy)

    update work_order
      set assigned_to_id = @WorkerID
      where wo_id = @WorkOrderID

  commit transaction
GO

grant execute on assign_work_order to uqweb, QManagerRole
GO

/* 
 exec dbo.assign_work_order 110001, 3510, 'ROUTER' 
 exec dbo.assign_work_order 110001, 3510, 811 
*/

