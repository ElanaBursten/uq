if object_id('dbo.RPT_highprofile_4') is not null
	drop procedure dbo.RPT_highprofile_4
GO

CREATE proc RPT_highprofile_4 (
	@DateFrom datetime, 
	@DateTo datetime, 
	@OfficeId int, 
	@ManagerId int, 
	@kind int,
	@HighProfileIDs varchar(300),
	@ClientIDs varchar(2500)
)
as
  -- @kind:  0 = all locates
  --         1 = open
  --         2 = closed
  set nocount on

  declare @highprofile table (
    ticket_id int not null primary key
  )

  declare @clients table (
    client_id int not null primary key
  )

  -- parse the list of clients to limit to
  insert into @clients
  select ids.ID from dbo.IdListToTable(@ClientIDs) ids

if @OfficeId=-1 -- if filtering by ManagerId
  begin
    insert into @highprofile (ticket_id) 
      select distinct locate.ticket_id
      from locate with (INDEX(locate_closed_date))
      where locate.assigned_to_id in (select h_emp_id from dbo.get_hier(@ManagerId,0))
       and locate.high_profile = 1
       and locate.closed_date between @DateFrom and @DateTo
       and ((locate.closed=0 OR @kind<>1) AND (locate.closed=1 OR @kind<>2))
	   and (@ClientIDs='' or (locate.client_id in (select client_id from @clients)))

    select
      ticket.ticket_id,
      locate.locate_id,
      locate.high_profile,
      ticket.ticket_number,
      ticket.transmit_date,
      locate.closed,
      locate.closed_date,
      locate.client_code,
      locate.high_profile_reason,
      employee.short_name,
      ticket.work_type,
      coalesce(ticket.work_description, ' ') work_description,
      ticket.work_address_number,
      ticket.work_address_street,
      ticket.work_cross,
      ticket.work_city,
      ticket.work_state,
      ticket.con_type,
      ticket.con_name,
      ticket.con_address,
      ticket.con_city,
      ticket.con_state,
      ticket.con_zip,
      ticket.caller,
      ticket.caller_phone,
      ticket.caller_contact,
      ticket.caller_fax,
      ticket.kind,
      ticket.ticket_type,
      ticket.map_page,
      ticket.company,  -- work done for
      reference.code,
      reference.description,
      ticket.work_county,
      (select dbo.get_multi_reason_desc_list(ticket.ticket_id, locate.client_code, @HighProfileIDs,0)) as MultiReasonsDescription
    from @highprofile as highprofile
      inner join ticket
        on ticket.ticket_id = highprofile.ticket_id
      inner join locate
        on locate.ticket_id = highprofile.ticket_id
      inner join employee
        on employee.emp_id = locate.assigned_to_id
      left outer join reference
        on locate.high_profile_reason = reference.ref_id
    where  ((@HighProfileIDs = '')
      or (locate.high_profile_reason in (select cast(S as int) from dbo.StringListToTable(@HighProfileIDs)))--for single reason, it must be in filter to return locate
      or ((select dbo.get_multi_reason_desc_list(ticket.ticket_id, locate.client_code, @HighProfileIDs,1)) is not NULL) )--for multi-reasons, at least 1 must be in filter to return locate
      and (@ClientIDs='' or (locate.client_id in (select client_id from @clients)))
    order by
      ticket.ticket_id, locate.locate_id
  end
else -- if filtering by OfficeId
  begin
    insert into @highprofile (ticket_id) 
      select distinct locate.ticket_id
      from locate with (INDEX(locate_closed_date))
      where locate.high_profile = 1
        and locate.closed_date between @DateFrom and @DateTo
        and ((locate.closed=0 OR @kind<>1) AND (locate.closed=1 OR @kind<>2))
		and (@ClientIDs='' or (locate.client_id in (select client_id from @clients)))

    select
      ticket.ticket_id,
      locate.locate_id,
      locate.high_profile,
      ticket.ticket_number,
      ticket.transmit_date,
      locate.closed,
      locate.closed_date,
      locate.client_code,
      locate.high_profile_reason,
      employee.short_name,
      ticket.work_type,
      coalesce(ticket.work_description, ' ') work_description,
      ticket.work_address_number,
      ticket.work_address_street,
      ticket.work_cross,
      ticket.work_city,
      ticket.work_state,
      ticket.con_type,
      ticket.con_name,
      ticket.con_address,
      ticket.con_city,
      ticket.con_state,
      ticket.con_zip,
      ticket.caller,
      ticket.caller_phone,
      ticket.caller_contact,
      ticket.caller_fax,
      ticket.kind,
      ticket.ticket_type,
      ticket.map_page,
      ticket.company,  -- work done for
      reference.code,
      reference.description,
      ticket.work_county,
      (select dbo.get_multi_reason_desc_list(ticket.ticket_id, locate.client_code, @HighProfileIDs,0)) as MultiReasonsDescription
    from @highprofile as highprofile
      inner join ticket
        on ticket.ticket_id = highprofile.ticket_id
      inner join locate
        on locate.ticket_id = highprofile.ticket_id
      inner join employee
        on employee.emp_id = locate.assigned_to_id
      left outer join reference
        on locate.high_profile_reason = reference.ref_id
      inner join damage 
        on damage.ticket_id = ticket.ticket_id
      inner join office 
        on (damage.profit_center = office.profit_center)
    where  ((@HighProfileIDs = '')
      or   (locate.high_profile_reason in (select cast(S as int) from dbo.StringListToTable(@HighProfileIDs))) --for single reason, it must be in filter to return locate
      or ((select dbo.get_multi_reason_desc_list(ticket.ticket_id, locate.client_code, @HighProfileIDs,1)) is not NULL) ) --for multi-reasons, at least 1 must be in filter to return locate
      and office.office_id = @OfficeId
      and (@ClientIDs='' or (locate.client_id in (select client_id from @clients)))
    order by
      ticket.ticket_id, locate.locate_id
  end

  -- Now return a second result set with the notes for all these tickets and related locates.
  -- It gets notes even for tickets that are excluded due to @HighProfileIDs,
  -- which is harmless.
  select tn.entry_date, tn.modified_date, tn.note, tn.ticket_id 
  from ticket_notes tn 
  inner join @highprofile hp on tn.ticket_id = hp.ticket_id
  order by hp.ticket_id

  -- High Profile reasons
  select description hp_reason --single reason
  from reference ref
  where 
    (ref.type = 'hpreason' and
    ref_id in (select cast(S as int) hp_id from dbo.StringListToTable(@HighProfileIDs)) ) --single reason
    or
    (ref.type = 'hpmulti' and
    ref_id in (select cast(S as int) hp_id from dbo.StringListToTable(@HighProfileIDs)) ) --multi reason      
      
  -- Clients
  select oc_code client_code	
  from client
  where 
    client_id in (select client_id from @clients) or
    @ClientIDs = ''
        

GO
grant execute on RPT_highprofile_4 to uqweb, QManagerRole
/*
exec dbo.RPT_highprofile_4 '2012-01-02' , '2012-01-09', -1, 3512, 0, '', '3236,4039,3398,3399,3404,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3749,3750,3751,3752,3754,3755,3421,3422,3423,3424,3426,3428,3429,3431,3432,3944,3940,3941,3942,3943'
exec dbo.RPT_highprofile_4 '2012-01-02' , '2012-01-09', -1, 3512, 0, '582,827,828', '3236,4039,3398,3399,3404,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3749,3750,3751,3752,3754,3755,3421,3422,3423,3424,3426,3428,3429,3431,3432,3944,3940,3941,3942,3943'
exec dbo.RPT_highprofile_4 '2012-01-02' , '2012-01-09', -1, 3512, 0, '582,827', '346,347,3236,4039,3398,3399,3404,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416,3417,3418,3419,3420,3749,3750,3751,3752,3754,3755,3421,3422,3423,3424,3426,3428,3429,3431,3432,3944,3940,3941,3942,3943'

exec dbo.RPT_highprofile_4 '2003-01-17' , '2003-01-19', -1, 2563, 2, '', ''
exec dbo.RPT_highprofile_4 '2004-09-07' , '2004-09-08', -1, 211, 0, '', ''
exec dbo.RPT_highprofile_4 '2001-01-01' , '2008-01-01', -1, 2676, 0, '', '1357,1362,1365,3489,3493,3495'
exec dbo.RPT_highprofile_4 '2009-01-01' , '2009-01-09', -1, 2676, 0, '', ''

Report=HighProfile
DateFrom=2004-03-01T00:00:00.000
DateTo=2004-03-08T00:00:00.000
OfficeId=-1
ManagerId=211
Kind=0
133 seconds in rep db
*/
