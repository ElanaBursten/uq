if object_id('dbo.get_ticket_data') is not null
	drop procedure dbo.get_ticket_data
go

CREATE PROCEDURE get_ticket_data (@TicketID int) AS

declare @locate_ids table (
  locate_id int
)

select * from ticket
where ticket_id = @TicketID

insert into @locate_ids
select locate_id from locate
where ticket_id = @TicketID

select * from locate
where locate_id in (select locate_id from @locate_ids)

select * from assignment a
where a.locate_id in (select locate_id from @locate_ids)
and a.active = 1
GO

grant execute on get_ticket_data to uqweb, QManagerRole
GO

