if object_id('dbo.RPT_ManagerStatus') is not null
	drop procedure dbo.RPT_ManagerStatus
go

create proc RPT_ManagerStatus (
  @ManagerID integer,
  @StartDate datetime,
  @EndDate datetime
)
as

begin
declare @Results table (
	emp_id int not null primary key,
        d_emp_id int not null,
	short_name varchar(45),
	emp_number varchar(30),
	h_report_level integer,
	h_report_to integer,
	h_active bit,
        h_namepath varchar(450) null,
	h_1 varchar(30) null,
	h_2 varchar(30) null,
	h_3 varchar(30) null,
	h_4 varchar(30) null,
	h_5 varchar(30) null,
	h_6 varchar(30) null,
	h_7 varchar(30) null,
	h_8 varchar(30) null,
	h_9 varchar(30) null,
	h_type_id int null,
	has_children bit null,

	Revenue decimal(10,2),
	LocatorsAssigned integer null,
	LocatorsWorking integer null,

	TicketLoad integer null,
	TicketLoadFirstLocate integer null,
	TicketsReceived integer null,
	LateTickets integer null,
	TicketsComp integer null,
	HoursWorked decimal(10,2) null,

	RevPerTicket decimal(10,2) null,
	RevPerWorkHour decimal(10,2) null,
	TicketsPerHour decimal(10,2) null,
	HoursPerLocator decimal(10,2) null,

	LiableDamages integer null,
	DamagesEst decimal(10,2) null
)

declare @Locates table (
	ticket_id integer,
	d_ticket_id integer,
        locate_id integer,
        d_locate_id integer,
        locator_id integer,
	assigned_to integer,
        closed_date datetime,
        transmit_date datetime,
        closed BIT,
	first_locate bit
)

declare @Tickets table (
	Ticket_id integer,
        Closed_date datetime,
        Transmit_date datetime,
        Closed BIT
)

insert into @Results (emp_id, d_emp_id, short_name, emp_number,
  h_report_level, h_report_to, h_active, h_namepath, h_type_id,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9)
select h_emp_id, h_emp_id, h_short_name_tagged, h_emp_number,
  h_report_level, h_report_to, h_active, h_namepath, h_type_id,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9
 from dbo.get_report_hier(@ManagerID, 0)


/* Keep Locators and managers only */
delete
from @Results where
  h_active = 0 or
  (h_type_id <> 11 and (not exists(select emp_id from employee where report_to = d_emp_id)));

/*
update @Results set has_children = 1 where exists(select d_emp_id from @Results where h_report_to = d_emp_id)
update @Results set has_children = 0 where not exists(select d_emp_id from @Results where h_report_to = d_emp_id)
*/

/* Locators Assigned */
update @Results set LocatorsAssigned =
(coalesce((select count(*)
           from employee e
           where (e.report_to = d_emp_id)
            and (e.type_id = 11)
            and (e.active = 1)),
           0))

declare @Working table (emp_id int not null, report_to int null, source varchar(10) not null)

-- Get list of locators working on hours/units during the period
insert into @Working
select distinct d_emp_id, e.report_to, 'H'
from locate_hours lh
  inner join @Results on lh.emp_id = d_emp_id
  inner join employee e on e.emp_id = d_emp_id
where (e.type_id = 11) and
  (lh.work_date >= @StartDate)
  and (lh.work_date < @EndDate)

-- Get list of locators satusing locates during the period
insert into @Working
select distinct ls.statused_by, e.report_to, 'L'
from locate_status ls
  inner join employee e on ls.statused_by = e.emp_id
where
  (e.type_id = 11)
  and (ls.status_date >= @StartDate)
  and (ls.status_date < @EndDate)
  and e.report_to in (select emp_id from @Results)

--select distinct emp_id, report_to, source from @Working

update @Results
set LocatorsWorking =
  (select count(distinct emp_id) from @Working where report_to = d_emp_id)

insert into @Locates (ticket_id, d_ticket_id, locate_id, d_locate_id, closed_date, closed, assigned_to, first_locate)
select ticket_id, ticket_id, locate_id, locate_id, closed_date, closed, assigned_to, 0
from locate where assigned_to in (select emp_id from @Results)

update @Locates set first_locate = 1
where locate_id = (select min(locate_id) from locate l where l.ticket_id = d_ticket_id)

update @Results
set TicketLoad = (
  select count(distinct ticket_id)
  from @Locates lc where lc.assigned_to = d_emp_id
)

update @Results
set TicketLoadFirstLocate = (
  select count(distinct ticket_id)
  from @Locates lc
  where lc.assigned_to = d_emp_id
    and first_locate = 1
)


insert into #temp select ticket_id from ticket
where (transmit_date >= @StartDate)
  and (transmit_date < @EndDate)

select * from #temp

/*
update @Results
set TicketsReceived = (
  select count(distinct ticket.ticket_id)
  from locate
    inner join ticket on locate.ticket_id = ticket.ticket_id
  where (transmit_date >= @StartDate)
    and (transmit_date < @EndDate)
    and (locate.assigned_to_id = d_emp_id)
)
*/

--select * from locate where ticket_id = 13953531
--update locate set assigned_to = 6551 where locate_id = 85037300 -- ALT on ticket 13953531, was 5778


/*
exec DBO.RPT_ManagerStatus 5405, '2004-07-07T00:00:00.000', '2004-07-08T00:00:00.000'

select distinct(ticket_id) from locate where assigned_to = 2234

select * from locate l
 inner join employee e on (e.emp_id = l.assigned_to)
where e.report_to = 2354
order by ticket_id, locate_id

insert into @Locates (Ticket_id, Locate_id, locator_id, Closed_date, Transmit_date, Closed)
  select t.Ticket_id, l.Locate_id, l.closed_by_id, l.closed_date, t.transmit_date, l.Closed
  from locate l (index (PK_locate_locateid))
   inner join ticket t on l.ticket_id = t.ticket_id
   inner join @results r on l.closed_by_id = r.emp_id --Note: this s not historcial
  where l.locate_id >= (select oldest_open_locate from oldest_open)

insert into @Tickets
select ticket_id, select max(closed_date) from locate l where l.ticket_id = t.ticket_id, transmit_date, 0
from ticket
where 0=1
*/


update @Results
 set Revenue = coalesce(
     (select sum(amount)
       from billing_transaction
        inner join locate_status
         on ls_id = locate_status_id
      where
       emp_id = locate_status.statused_by and
       status_date >= @StartDate and
       status_date < @EndDate), 0)

       -- TODO: Also consider locate_hours entries as possible work during the period
/*   union
   (select distinct emp_id from employee
       inner join locate_hours lh on d_emp_id = lh.emp_id and work_date >= @StartDate and work_date < @EndDate)))
*/

select * from @Results where (h_active = 1) and (TicketLoad <> TicketLoadFirstLocate) order by h_namepath

--select sum(TicketLoadFirstLocate) from @Results
--select distinct ticket_id from @Locates

end
/*
select
   report_to as manager_id,
   (select e2.short_name from employee e2 where e1.report_to=e2.emp_id) as manager_short_name,
   count(emp_id) as tech_count
   (select count(*) from ticket where
 from employee e1
 where report_to in
   (select emp_id
    from employee
    where report_to=@Manager)
 group by report_to
*/

/*
exec DBO.RPT_ManagerStatus 212, '6/15/04', '6/18/04'

exec DBO.RPT_ManagerStatus 5405, '2004-07-07T00:00:00.000', '2004-07-08T00:00:00.000'

exec DBO.RPT_ManagerStatus 1866, '2004-09-09T00:00:00.000', '2004-09-12T00:00:00.000'

select * from dbo.get_report_hier(5405, 0)

select count(*) from locate
select top 100 * from billing_transaction
select * from billing_transaction


create index billing_transaction_event_date on billing_transaction(event_date)

declare @Results table (
  d varchar(20)
)

insert into @Results (d) select convert(varchar(12), event_date, 101) from billing_queue
select d, count(*) from @Results group by d
select top 1000 status from assignment where status is not null

select count(*) from employee
where report_to = 217 and active = 1

 from assignment
 where locator_id in
    (select emp_id
      from employee
      where report_to=212)
  and ((insert_date >= '6/15/04'
    and insert_date < '6/20/04')
  or (insert_date < '6/15/04' and  ))



create index assignment_locator_id_insert_date on assignment(locator_id, insert_date)

select * from employee where emp_id in

select * from assignment where assignment_id in (select assignment_id from assignment where locate_id between 400000 and 500000) order by locate_id

select * from reference where type = 'emptype'

select top 100 * from employee

select top 100 * from locate_status

select * from locate_hours where 0=1

create index billing_transaction_locate_status_id on billing_transaction(locate_status_id)

(select emp_type from employee e where emp_id = h_emp_id)

select distinct(emp_id) from

  select
   distinct(emp_id) from
      select distinct emp_id from employee
      union
      select distinct emp_id from locate_hours


-- We couldn't get this union to work
update @Results set LocatorsWorking =
   (select distinct emp_id from
    (select distinct emp_id
     from employee e
     inner join locate_status l on l.statused_by = e.emp_id
     where (e.report_to = d_emp_id)
       and (e.type_id = 11)
       and (l.status_date >= @StartDate)
       and (l.status_date < @EndDate))
     union (
     select distinct(emp_id)
     from locate_hours lh
     where lh.emp_id = d_emp_id
       and (lh.work_date >= @StartDate)
       and (lh.work_date < @EndDate)))))
*/


/*
exec DBO.RPT_ManagerStatus 1866, '6/15/04', '6/21/04'

exec DBO.RPT_ManagerStatus 1908, 'September 11, 2004', 'September 12, 2004'

select * from employee where report_to = 1908

select distinct emp_id
from locate_hours
where emp_id in (select emp_id from employee where report_to = 1908)
  and work_date >= 'September 11, 2004'
  and work_date < 'September 12, 2004'
order by emp_id --4 unique users: 1895, 4155, 5752, 5753

select distinct statused_by
from locate_status
where statused_by in (select emp_id from employee where report_to = 1908)
  and status_date >= 'September 11, 2004'
  and status_date < 'September 12, 2004'
order by statused_by --3 unique users: 1895, 4155, 5752

select distinct (ls.statused_by)
from locate_status ls
where (ls.status_date >= 'September 11, 2004')
  and (ls.status_date < 'September 12, 2004')
  and ls.Statused_by in (select emp_id from employee where report_to = 1908 order by emp_id)

select * from locate_status
where statused_by in (1895, 4155, 5752, 5753, 1895, 4155, 5752)
  and status_date >= 'September 11, 2004'
  and status_date < 'September 12, 2004'
*/

/*
select distinct

select distinct assigned_to_id as emp_id
from locate
where closed_date > 'October 6, 2004'

select distinct emp_id
from locate_hours
where work_date > 'October 3, 2004'
*/
