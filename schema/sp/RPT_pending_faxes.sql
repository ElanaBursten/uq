if object_id('dbo.RPT_pending_faxes') is not null
  drop procedure dbo.RPT_pending_faxes
go

create procedure dbo.RPT_pending_faxes
as
  set nocount on

  -- Not used yet, an idea for a future report, helpful during development

  select
    fm.call_center,
    fm.fax_status,
    count(*) as N
  from fax_message fm
    inner join fax_message_detail fd on fm.fm_id = fd.fm_id
  where fm.fax_status not in ('Done', 'Failed')
  group by 
    fm.call_center,
    fm.fax_status
GO
grant execute on RPT_pending_faxes to uqweb, QManagerRole
/*
exec dbo.RPT_pending_faxes
*/
