--QM-1037 BigFoot: Using ESketch Footage.  
  --Function will sum up the LENGTH for either 
  --       ESketch (Bigfoot = 1)
  --       Locator entered (Bigfoot = 0)


if object_id('dbo.get_length_total') is not null
  drop function dbo.get_length_total
go

Create function dbo.get_length_total(@locate_id int, @isbigfoot bit) 
returns int 

as begin
    declare @Total int
	set @Total = 0
  if @isbigfoot = 0
    set @Total = (select sum(lh.units_marked) as sum_units_marked
            from locate_hours lh
            where (locate_id = @locate_id) 
            and ((bigfoot = @isbigfoot) or (bigfoot is NULL))
           )
  else
     set @Total = (select sum(lh.units_marked) as sum_units_marked
            from locate_hours lh
            where (locate_id = @locate_id) 
            and (bigfoot = @isbigfoot)
           )
 return @Total
end
Go

--Select dbo.get_entered_length_total(32565, 0)
--select * from locate_hours where locate_id = 32565


--Select * from locate where ticket_id = 6468
--Select * from locate_hours where locate_id = 32565
-- select * from billing_unit_conversion


