if object_id('dbo.RPT_productivity_7') is not null
  drop procedure dbo.RPT_productivity_7
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_productivity_7 (
  @DateFrom datetime,
  @DateTo datetime,
  @ManagerId int,
  @ExcludeStatuses varchar(2000),
  @LimitCallCenters varchar(2000),
  @LimitClients varchar(2500),
  @EmployeeStatus int 
)
as
  set nocount on

  declare @emps TABLE (
  emp_id int NOT NULL primary key,
  short_name varchar(45),
  emp_number varchar(30),
  h_report_level int,
  h_report_to int,
  h_active bit,
  h_namepath varchar(450) NULL,
  h_1 varchar(30) NULL,
  h_2 varchar(30) NULL,
  h_3 varchar(30) NULL,
  h_4 varchar(30) NULL,
  h_5 varchar(30) NULL,
  h_6 varchar(30) NULL,
  h_7 varchar(30) NULL,
  h_8 varchar(30) NULL,
  h_9 varchar(30) NULL
  )

  declare @clients table (
  client_id int not null primary key
  )

  declare @excluded_statuses table (
  S varchar(20) NOT NULL primary key
  )

  declare @productivity_tickets table (
    pt_ticket_id int not null,
    pt_emp_id int not null,
    ticket_closed bit not null default 1,
    PRIMARY KEY (pt_ticket_id, pt_emp_id)
  )

  declare @open_tickets_for_emps table (
    ot_ticket_id int not null,
    ot_emp_id int not null,
    PRIMARY KEY (ot_ticket_id, ot_emp_id)
  )

  declare @productivity_locates table (
    locate_id int not null primary key,
    ticket_id int not null,
    client_id int,
    emp_id int not null,
    status varchar(5) not null,
    closed bit not null,
    closed_date datetime null,
    qty int not null,
    length int not null
  )

  declare @locate_lengths table (
    lu_locate_id int not null,
    lu_status varchar(5) not null,
    lu_qty int not null
  )

  declare @results1 table (
    emp_id int not null,
    status varchar(5) not null,
    closed_status_count int not null default 0,
    partial_status_count int not null default 0,
    closed_qty int not null default 0,
    partial_qty int not null default 0,
    closed_len int not null default 0,
    partial_len int not null default 0
  )

  declare @results2 table(
    emp_id int not null,
    status varchar(5)    not null,
    closed_status_count int not null default 0,
    partial_status_count int not null default 0,
    total_status_count int not null default 0,
    closed_qty int not null default 0,
    partial_qty int not null default 0,
    total_qty int not null,
    closed_len int not null default 0,
    partial_len int not null default 0,
    total_len int not null
  )

  -- final results for the report:
  declare @emptotals table (
    emp_id int NOT NULL primary key,
    closed_ticket_count int,
    partial_ticket_count int,
    closed_locate_count int,
    partial_locate_count int,
    total_ticket_count int,
    total_locate_count int,
    closed_qty int,
    partial_qty int,
    total_qty int,
    closed_len int,
    partial_len int,
    total_len int,
    reg_hours decimal(12, 2) NULL,
    ot_hours decimal(12, 2) NULL,
    dt_hours decimal(12, 2) NULL,
    callout_hours decimal(12, 2) NULL,
    working_hours decimal(12, 2) NULL,   
    primary_hours decimal(12, 2) NULL,
    tph_working decimal(12, 2) NULL,
    tph_primary decimal(12, 2) NULL
  )

  -- Collect the emps
  insert into @emps (emp_id, short_name, emp_number,
    h_report_level, h_report_to, h_active, h_namepath,
    h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9 )
  select h_emp_id, h_short_name_tagged, h_emp_number,
    h_report_level, h_report_to, h_active, h_namepath,
    h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9
   from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)

  -- Caching the excluded statuses had no performance
  -- effect, but it made the server trace clearer.
  insert into @excluded_statuses
  select S from dbo.StringListToTable(@ExcludeStatuses)

  -- parse the list of clients to limit to
  insert into @clients
  select ids.ID from dbo.IdListToTable(@LimitClients) ids

  insert into @productivity_tickets ( pt_ticket_id, pt_emp_id )
    select distinct ticket_id, locate.closed_by_id
    from locate_status ls with (index(ls_sb_sd))
     inner join locate on ls.locate_id=locate.locate_id
     where ls.status_date between @DateFrom and @DateTo
      and locate.closed = 1
      and ls.statused_by in (select emp_id from @emps)
      and ls.status not in (select S from @excluded_statuses)
      and ls.status <>'-N'
      and locate.closed_date between @DateFrom and @DateTo
      and (@LimitClients='' or (locate.client_id in (select client_id from @clients)))

  -- find all the locates (even excluded ones) for those tickets
  insert into @productivity_locates (
    locate_id,
    ticket_id,
    client_id,
    emp_id,
    status,
    closed,
    closed_date,
    qty,
    length
  )
  select distinct
    locate.locate_id,
    locate.ticket_id,
    locate.client_id,
    coalesce(locate.assigned_to, locate.closed_by_id),
    locate.status,
    locate.closed,
    locate.closed_date,
    coalesce(locate.qty_marked, 0),
    0 as length
  from @productivity_tickets pt
    inner join locate
      on locate.ticket_id = pt.pt_ticket_id
  where locate.assigned_to is not null
    or locate.closed_by_id is not null

  insert into @locate_lengths
  select pl.locate_id, pl.status, sum(units_marked)
    from @productivity_locates pl
     inner join locate_hours lh
       on pl.locate_id=lh.locate_id
        and pl.status=lh.status
    where lh.work_date between @DateFrom and @DateTo
    group by pl.locate_id, pl.status

  update @productivity_locates
  set length = lu_qty
   from @locate_lengths
    where lu_locate_id = locate_id

  -- identify any tickets that were not closed during the period
  -- note: a ticket is considered closed for this locator if all of the
  -- locates assigned to him for that ticket are closed
  insert into @open_tickets_for_emps (ot_ticket_id, ot_emp_id)
  select distinct ticket_id, emp_id
    from @productivity_locates pl
    where (pl.closed = 0) or (pl.closed_date not between @DateFrom and @DateTo)

  -- Then mark as such in @productivity_tickets
  update @productivity_tickets
    set ticket_closed = 0
  from @open_tickets_for_emps
    where pt_ticket_id=ot_ticket_id AND pt_emp_id=ot_emp_id

  -- Delete the excluded statuses, so we don't sum them
  -- This isn't done earlier, so we get the right ticket_closed value above
  delete from @productivity_locates
  where status in (select S from @excluded_statuses)


  -- Delete the excluded clients, likewise not done ealier to
  -- get correct ticket_closed value
  if @LimitClients<>''
    delete from @productivity_locates
      where client_id not in (select client_id from @clients)

  -- get breakdown by status code of partially closed tickets
  insert into @results1 (
    emp_id,
    status,
    partial_status_count,
    partial_qty,
    partial_len
  )
  select
    pt.pt_emp_id,
    pl.status,
    count(*),
    sum(qty),
    sum(length)
  from @productivity_tickets pt
    inner join @productivity_locates pl
      on pl.ticket_id = pt.pt_ticket_id and
         pl.emp_id = pt.pt_emp_id and
         pl.closed_date between @DateFrom and @DateTo and
         pl.closed = 1
  where
    pt.ticket_closed = 0
  group by pt.pt_emp_id, pl.status

  -- get breakdown by status code of closed tickets
  insert into @results1 (
    emp_id,
    status,
    closed_status_count,
    closed_qty,
    closed_len
  )
  select
    pt.pt_emp_id,
    pl.status,
    count(*),
    sum(qty),
    sum(length)
  from @productivity_tickets pt
    inner join @productivity_locates pl
      on pl.ticket_id = pt.pt_ticket_id and
         pl.emp_id = pt.pt_emp_id and
         pl.closed_date between @DateFrom and @DateTo and
         pl.closed = 1
  where
    pt.ticket_closed = 1
  group by pt.pt_emp_id, pl.status

  -- summarize the breakdown by status data
  insert into @results2 (
    emp_id,
    status,
    closed_status_count,
    partial_status_count,
    total_status_count,
    closed_qty,
    partial_qty,
    total_qty,
    closed_len,
    partial_len,
    total_len
  )
  select
    emp_id,
    status,
    sum(closed_status_count),
    sum(partial_status_count),
    sum(closed_status_count) + sum(partial_status_count),
    sum(closed_qty),
    sum(partial_qty),
    sum(closed_qty) + sum(partial_qty),
    sum(closed_len),
    sum(partial_len),
    sum(closed_len) + sum(partial_len)
  from @results1
  group by emp_id, status

  -- summarize the locate data and return all the summarized results
  insert into @emptotals (
    emp_id,
    closed_ticket_count,
    partial_ticket_count,
    closed_locate_count,
    partial_locate_count,
    closed_qty,
    partial_qty,
    closed_len,
    partial_len
  )
  select
    e.emp_id,
    coalesce((select count(*)
     from @productivity_tickets pt
     where (pt.pt_emp_id = e.emp_id) and (pt.ticket_closed = 1)),0) as closed_ticket_count,
    coalesce((select count(*)
     from @productivity_tickets pt
     where (pt.pt_emp_id = e.emp_id) and (pt.ticket_closed = 0)),0) as partial_ticket_count,
    coalesce((select sum(r2b.closed_status_count)
     from @results2 r2b
     where r2b.emp_id = e.emp_id),0) as closed_locate_count,
    coalesce((select sum(r2b.partial_status_count)
     from @results2 r2b
     where r2b.emp_id = e.emp_id),0) as partial_locate_count,
    coalesce((select sum(r2b.closed_qty)
     from @results2 r2b
     where r2b.emp_id = e.emp_id),0) as closed_qty,
    coalesce((select sum(r2b.partial_qty)
     from @results2 r2b
     where r2b.emp_id = e.emp_id),0) as partial_qty,
    coalesce((select sum(r2b.closed_len) 
     from @results2 r2b
     where r2b.emp_id = e.emp_id), 0) as closed_len,
    coalesce((select sum(r2b.partial_len) 
     from @results2 r2b
     where r2b.emp_id = e.emp_id), 0) as partial_len
  from @emps e

  update @emptotals set total_ticket_count = closed_ticket_count + partial_ticket_count
  update @emptotals set total_locate_count = closed_locate_count + partial_locate_count
  update @emptotals set total_qty = closed_qty + partial_qty
  update @emptotals set total_len = closed_len + partial_len

  delete from @emptotals where (total_ticket_count=0 and total_locate_count=0) and 
     emp_id not in (select emp_id from @emps where h_active=1)

  update @emptotals set
    reg_hours = (select sum(reg_hours) from timesheet_entry tse
         where tse.work_emp_id=emp_id
           and tse.work_date>=@DateFrom and tse.work_date<@DateTo
           and tse.status in ('ACTIVE','SUBMIT')),
    ot_hours = (select sum(ot_hours) from timesheet_entry tse
         where tse.work_emp_id=emp_id
           and tse.work_date>=@DateFrom and tse.work_date<@DateTo
           and tse.status in ('ACTIVE','SUBMIT')),
    dt_hours = (select sum(dt_hours) from timesheet_entry tse
         where tse.work_emp_id=emp_id
           and tse.work_date>=@DateFrom and tse.work_date<@DateTo
           and tse.status in ('ACTIVE','SUBMIT')),
    callout_hours = (select sum(callout_hours) from timesheet_entry tse
         where tse.work_emp_id=emp_id
           and tse.work_date>=@DateFrom and tse.work_date<@DateTo
           and tse.status in ('ACTIVE','SUBMIT')),
    primary_hours = (select sum(reg_hours) from timesheet_entry tse
         where tse.work_emp_id=emp_id
           and tse.work_date>=@DateFrom and tse.work_date<@DateTo
           and datepart(dw, tse.work_date) not in (1,7)
           and tse.status in ('ACTIVE','SUBMIT'))

  update @emptotals set working_hours =
    reg_hours + ot_hours + dt_hours + callout_hours

  update @emptotals set tph_working = total_ticket_count / working_hours
    where total_ticket_count>0 and working_hours>0

  update @emptotals set tph_primary = total_ticket_count / primary_hours
    where total_ticket_count>0 and primary_hours>0

  -- Result Set 1: details by emp in hier order
  select e.*, et.*
   from @emps e
    inner join @emptotals et on e.emp_id=et.emp_id
   order by h_namepath, e.short_name

  -- Result set 2: status details, for detail section
  select * from @results2
    order by emp_id, status

  -- Result set 3: status totals, for end of report
  select
    status,
    sum(closed_status_count) as closed_locate_count,
    sum(partial_status_count) as partial_locate_count,
    sum(total_status_count) as total_locate_count,
    sum(closed_qty) as closed_qty,
    sum(partial_qty) as partial_qty,
    sum(total_qty) as total_qty,
    sum(closed_len) as closed_len,
    sum(partial_len) as partial_len,
    sum(total_len) as total_len
  from @results2
  group by status
  order by status

  -- Result set 4:
  select client.client_id, client.client_name, client.oc_code, client.call_center,
    total_locate_count, total_qty 
   from dbo.IdListToTable(@LimitClients) ids
    inner join client on ids.ID = client.client_id
    left join
     (select pl.client_id,
         count(*) as total_locate_count,
         sum(qty) as total_qty,
         sum(length) as total_len
    from @productivity_tickets pt
      inner join @productivity_locates pl
        on pl.ticket_id = pt.pt_ticket_id and
           pl.closed_date between @DateFrom and @DateTo and
           pl.closed = 1
    group by pl.client_id) clienttot
     on clienttot.client_id=client.client_id
  order by call_center, oc_code

  -- Result set 5: the manager's name
  select short_name from employee where emp_id=@ManagerID
go

grant execute on RPT_productivity_7 to uqweb, QManagerRole

/*
exec RPT_productivity_7 '2004-09-16', '2004-09-17', 2139, '', '', '', 1
exec RPT_productivity_7 '2004-09-13', '2004-09-19', 2323, '', 'FCT1,FCT2,FCT3', '1720,1729,1727', 1
exec RPT_productivity_7 '2004-09-15', '2004-09-16', 3566, '', '', '', 1
exec RPT_productivity_7 '2004-09-17', '2004-09-18', 869, 'NC', '', '', 1
exec RPT_productivity_7 '2004-10-01', '2004-10-02', 6709, '', '', '', 1
exec RPT_productivity_7 '2008-07-01', '2008-08-01', 2676, '', '', '', 1

-- index idea (not currently in use):
alter table locate_status add worked_by int
update locate_status set worked_by=statused_by
create index ls_sb_sd on locate_status(statused_by,status_date)
select datepart(dw, getdate())
*/

