if object_id('dbo.get_wo_attachments') is not null
	drop procedure dbo.get_wo_attachments
GO

CREATE Procedure get_wo_attachments(@WorkOrderID int)
as
set nocount on

select 
  coalesce(client.attachment_url, '') + attachment.filename as attachment_file,
  work_order.wo_id, work_order.wo_number
from work_order
  inner join client on client.client_id = work_order.client_id
  inner join attachment on (attachment.foreign_id = work_order.wo_id 
                            and attachment.foreign_type = 7)
where attachment.upload_date is not null
  and attachment.active = 1
  and work_order.wo_id = @WorkOrderID 

GO

grant execute on get_wo_attachments to uqweb, QManagerRole

/*
exec get_wo_attachments 12345
*/
