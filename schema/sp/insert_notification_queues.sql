if object_id('dbo.insert_notification_queues') is not null
  drop procedure dbo.insert_notification_queues
GO

CREATE PROCEDURE insert_notification_queues (
  @LocateID int,
  @LsID int,
  @Status varchar(5),
  @CallCenter varchar(30),
  @State varchar(5),
  @ClientCode varchar(20)
)
AS

set nocount on

-- Check for a fax rule match, and insert the action into the queue if there are any
insert into fax_queue (ls_id, call_center, send_type)
select top 1 @LsId, @CallCenter, fax_queue_rules.send_type
 from fax_queue_rules
 where call_center = @CallCenter
   and (client_code = @ClientCode or client_code = '*')
   and (status = @Status or status = '*')
   and (state = @State or state = '*')
  
-- Check for an email rule match, and insert the action into the queue if there are any
insert into email_queue (ls_id, call_center)
select top 1 @LsId, @CallCenter
 from email_queue_rules
 where call_center = @CallCenter
   and (client_code = @ClientCode or client_code = '*')
   and (status = @Status or status = '*')
   and (state = @State or state = '*')

GO

grant execute on insert_notification_queues to uqweb, QManagerRole
GO

/*
insert into fax_queue_rules (call_center, client_code, status, state) values ('1391', 'B11', 'H', 'AK')
insert into email_queue_rules (call_center, client_code, status, state) values ('1391', 'B11', 'H', 'AK')
insert_notification_queues 2004, 50001, 'H', '1391', 'AK', 'B11'
--                         LocateID, LsID, Status, CallCenter, State, ClientCode
select top 100 * from fax_queue
select top 100 * from email_queue
select top 100 * from locate
select top 100 * from locate_status
select top 100 * from ticket
update ticket set caller_email = 'erik.berry@oasisdigital.com' where ticket_id = 1000
*/
