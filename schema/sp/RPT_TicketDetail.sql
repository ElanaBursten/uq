if object_id('dbo.RPT_TicketDetail') is not null
  drop procedure dbo.RPT_TicketDetail
go

create proc RPT_TicketDetail (
  @TicketID int,
  @AttachmentList VarChar(2000)
)

as
set nocount on

declare @Locatedata table (
    [ticket_id] int,
    [ld_locate_id] int,
    [client_code] varchar(10),
    [status] varchar(5),
    [status_name] varchar(30),
    [short_name] varchar(30),
    [high_profile] bit,
    [qty_marked] int,
    [closed] bit,
    [closed_date] datetime,
    [worked_date] datetime,
    [plat_list] varchar(400)
  )

-- Ticket data
select
  ticket.ticket_id,
  ticket.ticket_number,
  ticket.transmit_date,
  ticket.due_date,
  ticket.image,
  call_center.xml_ticket_format
from ticket
left join call_center on cc_code = ticket_format
where ticket.ticket_id = @TicketID

-- Locate data
insert into @Locatedata
select
  locate.ticket_id,
  locate.locate_id, 
  locate.client_code,
  locate.status,
  statuslist.status_name,
  employee.short_name,
  locate.high_profile,
  locate.qty_marked,
  locate.closed,
  locate.closed_date,
  locate.closed_date,
  dbo.get_plat_list(locate.locate_id) as plat_list
from ticket
  left join locate
    on locate.ticket_id = ticket.ticket_id
    and locate.status<>'-N'
  left join statuslist
    on statuslist.status = locate.status
  left join assignment
    on assignment.locate_id = locate.locate_id
     and assignment.active = 1
  left join employee
    on employee.emp_id = assignment.locator_id
where ticket.ticket_id = @TicketID

--Update worked_date to use the most recent between locate.closed_date and the most recent 
--locate_hours record for that locate

update @Locatedata
  set worked_date = (
    select max(locate_hours.work_date)
    from locate_hours
    where ld_locate_id = locate_hours.locate_id) 

update @Locatedata 
  set worked_date = closed_date 
  where ((closed_date > worked_date) or (worked_date is null))
    and (closed_date is not null)

select * from @Locatedata order by client_code

-- Notes data
select tn.entry_date, tn.modified_date, tn.note note, tn.ticket_id
from ticket
  inner join ticket_notes tn on tn.ticket_id = ticket.ticket_id
where ticket.ticket_id = @TicketID
order by ticket.ticket_id, tn.entry_date

-- Attachments 
select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.extension,
  a.orig_filename as display_name
from attachment a
  inner join employee e on a.attached_by = e.emp_id 
  inner join ticket t on (a.foreign_id = t.ticket_id and a.foreign_type = 1)
where (@AttachmentList = '' or a.attachment_id in (select l.Id from dbo.IdListToTable(@AttachmentList) l))
  and t.ticket_id = @TicketId
  and a.active = 1
order by
  a.attach_date, a.orig_filename

-- Facilities
select client_code, coalesce(ft.description, facility_type, '-') as facility_type,
  coalesce(fm.description, facility_material, '-') as facility_material,
  coalesce(fs.description, facility_size, '-') as facility_size,
  coalesce(fp.description, facility_pressure, '-') as facility_pressure,
  offset,
  employee.short_name as added_by_name 
from @LocateData 
  left join locate_facility on (ld_locate_id = locate_id)
  left join employee on (locate_facility.added_by = employee.emp_id)
  left join reference ft on (ft.type = 'factype' and ft.code = facility_type)
  left join reference fm on (fm.type = 'facmtrl' and fm.code = facility_material and fm.modifier = facility_type)
  left join reference fs on (fs.type = 'facsize' and fs.code = facility_size and fm.modifier = facility_type)
  left join reference fp on (fp.type = 'facpres' and fp.code = facility_pressure and fm.modifier = facility_type)
where locate_facility.active = 1
order by client_code, locate_facility_id

go
grant execute on RPT_TicketDetail to uqweb, QManagerRole

/*
exec dbo.RPT_TicketDetail 13245677, ''
exec dbo.RPT_TicketDetail 13245677, '1,2,3,4'
*/
