if object_id('dbo.delete_prior_billing') is not null
  drop procedure dbo.delete_prior_billing
go

create procedure delete_prior_billing (
  @BillID int, 
  @TestOnly bit = 0
  )
as
begin
  if not exists (select bill_id from billing_header where bill_id = @BillID)
  begin
    raiserror ('There is no existing billing run with the requested bill_id.', 16, 1)
    return 1
  end

  if @TestOnly is null set @TestOnly = 0

  declare @center_group_id int
  declare @period_type varchar(10)
  declare @period_start datetime
  declare @period_end datetime
  declare @BillToDelete table (
    bill_id int not null primary key,
    group_code varchar(30) not null)

  select @center_group_id = center_group_id, @period_type = period_type, 
    @period_start = bill_start_date, @period_end = bill_end_date
  from billing_header
  where bill_id = @BillID

  -- gather all other uncommitted billing runs for the same center & period as @BillID
  insert into @BillToDelete
    select bill_id, group_code from billing_header
    inner join center_group on billing_header.center_group_id = center_group.center_group_id
    where billing_header.center_group_id = @center_group_id 
      and bill_start_date = @period_start
      and bill_end_date = @period_end
      and period_type = @period_type
      and committed = 0
      and bill_id <> @BillID
 
  if (@TestOnly = 0) 
  begin
    delete from billing_detail where bill_id in (select bill_id from @BillToDelete)
    delete from billing_invoice where billing_header_id in (select bill_id from @BillToDelete)
    delete from billing_header where bill_id in (select bill_id from @BillToDelete)
  end

  select * from @BillToDelete
end
go

grant execute on delete_prior_billing to uqweb, QManagerRole

/*

select center_group_id, period_type, bill_start_date, bill_end_date, count(*) bill_run_count
from billing_header
where bill_start_date > 'Jan 1, 2009' and bill_start_date < 'Jan 1, 2010'
group by center_group_id, period_type, bill_start_date, bill_end_date
having count(*) > 1

exec dbo.delete_prior_billing 5009
*/
