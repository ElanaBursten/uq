if object_id('dbo.sync_summary_report2') is not null
	drop procedure dbo.sync_summary_report2
go

create procedure sync_summary_report2 (
  @DateStart datetime,
  @DateEnd   datetime,
  @ManagerID int
)
as
  set nocount on

  DECLARE @emps TABLE (
	emp_id int not null primary key,
	emp_number varchar(25) null,
	short_name varchar(30) null
  )

  -- Collect the emps
  insert into @emps
  select h_emp_id, h_emp_number, h_short_name
   from dbo.get_hier(@ManagerID, 0)

  select
    E1.emp_id,
    tot.FirstSync,
    tot.LastSync,
    isnull(tot.NumSyncs, 0) as NumSyncs,
    E1.short_name,
    E1.emp_number,
    tot.client_version
  from @emps E1
    left join (
      select
        sync_log.emp_id,
        min(sync_date) as FirstSync,
        max(sync_date) as LastSync,
        min(client_version) as client_version,
        count(sync_id) as NumSyncs
      from sync_log
        inner join @emps E2
          on E2.emp_id = sync_log.emp_id
      where
        sync_date between @DateStart and @DateEnd
      group by
        sync_log.emp_id
    ) as tot
       on tot.emp_id = E1.emp_id
  order by
    E1.short_name
go
grant execute on sync_summary_report2 to uqweb, QManagerRole
/*
exec dbo.sync_summary_report2 '2003-04-01', '2003-04-03', 212
*/
