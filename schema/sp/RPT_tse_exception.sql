if object_id('dbo.RPT_tse_exception_2') is not null
	drop procedure dbo.RPT_tse_exception_2
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_tse_exception_2(
  @ManagerId int,
  @StartDate datetime, 
  @EmployeeStatus int
)
as
  set nocount on

  declare @result table (
    r_emp_id int,
    mgr_id int,
    mgr_short_name varchar(30) NULL,
    report_level integer NOT NULL,
    emp_number varchar(15) NULL,
    short_name varchar(30) NULL,
    last_name varchar(30) NULL,
    first_sync datetime NULL,
    last_sync datetime NULL,
    sync_span datetime NULL,
    first_status datetime NULL,
    last_status datetime NULL,
    status_span datetime NULL,
    hours_worked datetime NULL,
    reasons varchar(30) NOT NULL DEFAULT '',
    FSYL varchar(5) NULL,
    LSYE varchar(5) NULL,
    SYSP varchar(5) NULL,
    FSTL varchar(5) NULL,
    LSTE varchar(5) NULL,
    STSP varchar(5) NULL,
    NOHR varchar(5) NULL,
    hours_sync_discrep datetime NULL,
    hours_status_discrep datetime NULL,
    h_report_level integer,
    h_namepath varchar(450) NULL,
    h_idpath varchar(160) NULL,
    tse_id int NULL,
    pc_code varchar(15) NULL,
    primary key(r_emp_id)
  )

  declare @EndDate datetime
  declare @AfterEndDate datetime
  declare @target datetime

  declare @work table (
    w_emp_id int not null,
    minval datetime NULL,
    maxval datetime NULL,
    tse_id1 int NULL,
    primary key(w_emp_id)
  )

  select @EndDate = dateadd(d, 1, @StartDate)
  -- used to make the locate_status query more efficient
  select @AfterEndDate = dateadd(d, 1, @EndDate)

  -- get all the emps for this manager in result table
  insert into @result (r_emp_id, mgr_id, mgr_short_name, emp_number,
    short_name, last_name, report_level, h_namepath, h_idpath, pc_code)
  select h_emp_id, h_report_to, h_report_to_short_name, h_emp_number,
    h_short_name, h_last_name, h_report_level, h_namepath, h_idpath, h_eff_payroll_pc
  from get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)
  where h_emp_id <> @ManagerID

  -- gather first/last sync time
  insert into @work (w_emp_id, minval, maxval)
  select r_emp_id, min(coalesce(local_date,sync_date)) as first, max(coalesce(local_date,sync_date)) as last
  from sync_log inner join @result r on sync_log.emp_id=r_emp_id
  and sync_date between @StartDate and @EndDate
  group by r_emp_id

  update @result set first_sync = minval, last_sync = maxval
  from @work
  where w_emp_id = r_emp_id

  delete from @work

  -- gather first/last status time
  insert into @work (w_emp_id, minval, maxval)
  select r_emp_id, min(status_date) as first, max(status_date) as last
    from locate_status ls with (INDEX(locate_status_insert_date))
     inner join @result r on ls.statused_by=r_emp_id
    and status_date between @StartDate and @EndDate
    and insert_date between @StartDate and @AfterEndDate
    group by r_emp_id

  update @result set first_status = minval, last_status = maxval
  from @work
  where w_emp_id = r_emp_id

  delete from @work

  -- gather first/last locate hours time
  insert into @work (w_emp_id, minval, maxval)
  select r_emp_id, min(entry_date) as first, max(entry_date) as last
    from locate_hours lh --with (INDEX(locate_hours_entry_date))
     inner join @result r on lh.emp_id=r_emp_id
    and entry_date between @StartDate and @EndDate
    group by r_emp_id

  update @result set first_status = minval
  from @work
  where (w_emp_id = r_emp_id) and ((first_status is NULL) or (first_status > minval))

  update @result set last_status = maxval
  from @work
  where (w_emp_id = r_emp_id) and ((last_status is NULL) or (last_status < maxval))

  delete from @work

  -- gather working hours entered in new timesheet_entry table
  insert into @work (w_emp_id, minval, tse_id1)
  select tse.work_emp_id,
    dateadd(minute, 
     (coalesce(reg_hours,0) + coalesce(ot_hours,0) + coalesce(dt_hours,0) )
        * 60,  '00:00:00') as hours,
   tse.entry_id
   from timesheet_entry tse
    inner join @result r on tse.work_emp_id=r.r_emp_id
      and tse.status in ('ACTIVE', 'SUBMIT')
   where tse.work_date = @StartDate
  
  update @result
    set hours_worked = minval,
        tse_id = tse_id1
  from @work
  where w_emp_id = r_emp_id

  -- calculate spans
  update @result set sync_span = last_sync - first_sync
  update @result set status_span = last_status - first_status

  -- apply rules...
  select @target = dateadd(hour, 9, @StartDate)
  update @result set reasons = reasons + 'FSYL ', FSYL='X'
    where first_sync > @target

  select @target = dateadd(hour, 16, @StartDate)
  update @result set reasons = reasons + 'LSYE ', LSYE='X'
    where last_sync < @target

  update @result set reasons = reasons + 'SYSP ', SYSP='X',
                     hours_sync_discrep = hours_worked - sync_span
    where sync_span + '03:00:00' < hours_worked

  select @target = dateadd(hour, 9, @StartDate)
  update @result set reasons = reasons + 'FSTL ', FSTL='X'
    where first_status > @target

  select @target = dateadd(hour, 16, @StartDate)
  update @result set reasons = reasons + 'LSTE ', LSTE='X'
    where last_status < @target

  update @result set reasons = reasons + 'STSP ', STSP='X',
                     hours_status_discrep = hours_worked - status_span
    where status_span + '03:00:00' < hours_worked

  update @result set reasons = reasons + 'NOHR ', NOHR='X'
    where status_span is not null and (hours_worked is null or hours_worked=0)

  -- result
  select * from @result
   order by report_level, mgr_short_name, mgr_id, short_name

  -- second result set with TSE detail rows for report
  select 
    tse.*,
    r2.short_name as short_name,
    case 
      when break_rules.pc_code is not null then
        'Rule: ' + Convert(Varchar, break_length) + ' min break '+ Lower(rule_type) + ' ' + Convert(Varchar, break_needed_after/60) + ' hrs worked'
      else 'No rule defined for ' + r.pc_code
    end as rule_msg,
    case 
       when rule_id_ack is not null then
          case 
            when rule_ack_date is not null then
              'Rule broken, ' + r2.short_name + ' acknowledgment on ' + Convert(varchar, rule_ack_date)
            else 'Rule broken, no acknowledgment from ' + r2.short_name
          end 
       else '' 
    end as rule_broke_msg
   from timesheet_entry tse
     inner join @result r on tse.work_emp_id=r.r_emp_id
       and tse.status in ('ACTIVE', 'SUBMIT')
     left outer join break_rules on break_rules.pc_code = r.pc_code and rule_type in ('EVERY', 'AFTER')
     left outer join @result r2 on tse.entry_by = r2.r_emp_id
   where tse.work_date = @StartDate

go
grant execute on RPT_tse_exception_2 to uqweb, QManagerRole

/*
1. First sync after 9 am
2. Last sync before 4 pm
3. Total time between the first and last sync is less than or equal to the total hours entered for that day less 3 (H-3)
4. First status after 9 am
5. Last status before 4 pm
6. Total time between first and last status is less than or equal to the total hours entered for that day less 3 (H-3)
7. No hours entered but at least 1 locate has been statused.

Eventually, we may want this is the query gets slow:
CREATE INDEX locate_hours_entry_date on locate_hours(entry_date)

exec dbo.RPT_tse_exception_2 211, '2004-02-01', 1
exec dbo.RPT_tse_exception_2 212, '2004-02-01', 1
exec dbo.RPT_tse_exception_2 2676, '2006-12-14', 1

*/
