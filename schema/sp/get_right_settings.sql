if object_id('dbo.get_right_settings') is not null
  drop function dbo.get_right_settings
go

create function get_right_settings(@EmpId int)
returns
  @result TABLE (
    emp_right_id int, --origin_id int,
    emp_id int,
    right_id integer,
    allowed varchar(1),
    limitation varchar(200),
    modified_date datetime
  )
as
begin

  declare @rights TABLE (
    _temp_id int,
    origin_id int,
    emp_id int,
    right_id integer,
    allowed varchar(1),
    limitation varchar(200),
    modified_date datetime
  )

  declare @temp TABLE (
    temp_id int IDENTITY,
    origin_id int,
    emp_id int,
    right_id integer,
    allowed varchar(1),
    limitation varchar(200),
    priority int,
    modified_date datetime
  )

  insert into @temp (origin_id, emp_id, right_id, allowed, limitation, priority, modified_date)
    -- settings from employee groups
    select 
      gr.group_right_id,
      @EmpId,
      gr.right_id,
      gr.allowed,
      gr.limitation, 
      gd.priority,
      gr.modified_date
    from employee_group eg
      inner join group_definition gd on (gd.group_id = eg.group_id and gd.is_default = 0) and gd.active = 1
      inner join group_right gr on gr.group_id = gd.group_id
    where eg.emp_id = @EmpId    

    union

    -- setting from the default groups
    select 
      gr.group_right_id,
      @EmpId,
      gr.right_id,
      gr.allowed,
      gr.limitation, 
      gd.priority,
      gr.modified_date
    from group_definition gd
      inner join group_right gr on gr.group_id = gd.group_id
    where (gd.is_default = 1) and gd.active = 1

    union

    -- employee settings
    select 
      emp_right_id,
      emp_id,
      right_id, 
      allowed,
      limitation, 
      -1,
      modified_date 
    from employee_right
    where emp_id = @EmpId
  
  insert into @rights (right_id, _temp_id, allowed, modified_date)
    select 
      right_id,
      -- determine the temp_id of the record with higher priority
      case 
        when (select t2.temp_id from @temp t2 
              where t2.priority = (select MIN(priority) from @temp t3 
                                   where t2.right_id = t1.right_id and t3.right_id = t2.right_id and t3.allowed = 'Y')) is null then MAX(temp_id) 
        else
          (select t2.temp_id from @temp t2 
           where  t2.priority = (select MIN(priority) from @temp t3 
                                 where t2.right_id = t1.right_id and t3.right_id = t2.right_id and t3.allowed = 'Y')) end
      as temp_id,
      MAX(allowed),
      MAX(modified_date)
    from @temp t1
    where 
       emp_id = @EmpId 
    group by right_id

    update @rights
       set origin_id = (select t.origin_id from @temp t where t.temp_id = _temp_id),
           limitation = (select t.limitation from @temp t where t.temp_id = _temp_id),
           -- modified_date = (select t.modified_date from @temp t where t.temp_id = _temp_id),
           emp_id = @EmpId

    insert into @result
      select origin_id, emp_id, right_id, allowed, limitation, modified_date
      from @rights

  return
end
go


/*
select * from dbo.get_right_settings(8411)
select * from dbo.get_right_settings(3510)
*/
