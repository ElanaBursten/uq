if object_id('dbo.clear_old_multi_resp') is not null
	drop procedure dbo.clear_old_multi_resp
GO

create proc clear_old_multi_resp
as

delete from responder_multi_queue
 where insert_date <  dateadd(d, -30, getdate())

GO

/*
Try it out with:
exec dbo.clear_old_multi_resp 
*/
