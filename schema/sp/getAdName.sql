/*   QMANTWO-521_1_Alter_schema_employee_history
     LDAP Changes
	 stored procedure ->>  GetADname
     3/13/2018	  
*/

if object_id('dbo.GetADname') is not null
	drop procedure dbo.GetADname
go
Create Procedure [dbo].[GetADname] --QMANTWO-521
  (
  @UserName varchar(25)
  )
As


select coalesce(e.ad_username, '') as ad_username  
 from employee e
 join [dbo].[users] u on e.emp_id=u.emp_id
 where u.login_id = @UserName
  and (e.active=1 or e.active is null)