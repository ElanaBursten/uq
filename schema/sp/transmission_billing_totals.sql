if object_id('dbo.transmission_billing_totals') is not null
	drop procedure dbo.transmission_billing_totals
GO

create proc transmission_billing_totals (
	@datefrom datetime,
	@dateto datetime,
	@centers varchar(20),
	@termids varchar(100)
)
as

set nocount on

declare @termidlist table (
	termid varchar(20) NOT NULL
)

insert into @termidlist
select S from dbo.StringListToTable(@termids)

declare @centerlist table (
	center varchar(20) NOT NULL
)

insert into @centerlist
select S from dbo.StringListToTable(@centers)

declare @trans table (
	ticket_id int NOT NULL,
	call_center varchar (20),
	ticket_number varchar (20),
	ticket_type varchar (38),
	con_name varchar (50),
	work_city varchar (40),
	transmit_day datetime NOT NULL ,
	work_county varchar (40),
	work_address_number varchar (10),
	work_address_number_2 varchar (10),
	work_address_street varchar (55),
	map_page varchar (20)
)

insert into @trans
select
 ticket.ticket_id,
 ticket.ticket_format as call_center, 
 ticket.ticket_number, 
 ticket.ticket_type, 
 ticket.con_name, 
 ticket.work_city, 
 ticket_version.transmit_date as transmit_day,
 ticket.work_county,
 ticket.work_address_number,
 ticket.work_address_number_2,
 ticket.work_address_street,
 ticket.map_page
from ticket_version with (NOLOCK, index(ticket_version_transmit_date))
  inner join ticket with (NOLOCK, index(PK_ticket_ticketid)) 
    on ticket.ticket_id = ticket_version.ticket_id 
  where ticket.ticket_format in (select center from @centerlist)
   and ticket_version.transmit_date between @datefrom and @dateto

select 
 t.call_center, 
 t.ticket_number, 
 t.ticket_type, 
 t.con_name,
 t.work_city,
 t.transmit_day,
 locate.client_code,
 t.work_county,
 t.work_address_number,
 t.work_address_number_2,
 t.work_address_street,
 t.map_page
from @trans t
  inner join locate with (NOLOCK)
    on t.ticket_id = locate.ticket_id
where 
  (@termids = '' OR locate.client_code in (select termid from @termidlist))
 and locate.status <> '-N'
order by 1,2,3,4

GO
grant execute on transmission_billing_totals to uqweb, QManagerRole

/* Example uses of this SP:

exec dbo.transmission_billing_totals '2004-02-29', '2004-03-07', 'FBL1', 'LC01,LF01,BTR01,GENT02,HM01,HM02,HO01,KEN01,MET01,MICI01,WB01'
exec dbo.transmission_billing_totals '2004-02-29', '2004-03-07', 'FAM1', 'SCMB01'
exec dbo.transmission_billing_totals '2004-02-29', '2004-03-07', 'FJL1', 'MS2094,MS2099,MS2093,MS3000,MS3001'

exec dbo.transmission_billing_totals '2004-02-29', '2004-03-02', 'FAM1,FBL1', ''

exec dbo.transmission_billing_totals '2005-03-19', '2005-03-26', 'NCA1,SCA1,SCA3', ''

*/
