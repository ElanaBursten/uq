if object_id('dbo.RPT_employee_cov_status') is not null
  drop procedure dbo.RPT_employee_cov_status
GO

create procedure dbo.RPT_employee_cov_status
  (
  @ManagerID integer
  )
as

-- Details for the manager's employees with COV
select 
  pc.pc_code + ' ' + pc.pc_name as profit_center,
  Coalesce(hier.h_emp_number, '-') as emp_number,
  Coalesce(e.last_name, '-') as last_name,
  Coalesce(e.first_name + ' ' + e.last_name, hier.h_short_name, '-') as full_name,
  Coalesce(r.description, 'unknown') as emp_title,
  e.hire_date,
  hier.h_charge_cov
from dbo.get_report_hier3(@ManagerID, 0, 9, 1) hier
inner join employee e on hier.h_emp_id = e.emp_id
inner join profit_center pc on hier.h_eff_payroll_pc = pc_code
left join reference r on e.type_id = r.ref_id
where hier.h_charge_cov = 1
order by profit_center, last_name, full_name, emp_title, emp_number

-- Summary of all the manager's employees' COV status
select 
  Coalesce(lc.name, '-') as company_name,
  pc.pc_code + ' ' + pc.pc_name as profit_center,
  sum(case when hier.h_charge_cov = 1 then 1 else 0 end) as cov_count,
  sum(case when hier.h_charge_cov = 0 then 1 else 0 end) as no_cov_count,
  count(*) as emp_count
from dbo.get_report_hier3(@ManagerID, 0, 9, 1) hier
inner join profit_center pc on hier.h_eff_payroll_pc = pc.pc_code
left join locating_company lc on pc.company_id = lc.company_id
group by lc.name, pc.pc_code, pc.pc_name
order by company_name, profit_center

go

grant execute on dbo.RPT_employee_cov_status to uqweb, QManagerRole
/*
exec dbo.RPT_employee_cov_status 10300
exec dbo.RPT_employee_cov_status 2676 -- whole company
*/
