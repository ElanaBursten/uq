if object_id('dbo.find_munic_area_locator') is not null
	drop procedure dbo.find_munic_area_locator
go

CREATE Procedure find_munic_area_locator
	(
	@State varchar(15),
	@County varchar(45),
	@Munic varchar(45)
	)
As
set nocount on

declare @areafound table (
	map_id int NULL ,
	area_name varchar(40) NULL ,
	area_id int NULL ,
	emp_id int NULL ,
	short_name varchar(30) NULL 
) 

insert into @areafound
select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name
 from county_area
  inner join area on county_area.area_id = area.area_id
  inner join employee on area.locator_id = employee.emp_id
WHERE county_area.state=@State and county_area.county=@County and county_area.munic=@Munic
 AND employee.active = 1
 and employee.can_receive_tickets = 1

-- try munic == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county=@County and county_area.munic='*')
   AND employee.active = 1
   and employee.can_receive_tickets = 1

-- try county == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county='*' and county_area.munic=@Munic)  
   AND employee.active = 1
   and employee.can_receive_tickets = 1

-- try statewide wildcard == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county='*' and county_area.munic='*')
   AND employee.active = 1
   and employee.can_receive_tickets = 1

 select * from @areafound
GO

grant execute on find_munic_area_locator to uqweb, QManagerRole
/*
exec dbo.find_munic_area_locator 'NJ', 'MONMOUTH', 'HIGHLANDS'
exec dbo.find_munic_area_locator 'NJ', 'SALEM', 'NOWHERE'
exec dbo.find_munic_area_locator 'LA', 'NOWHERE', 'ARMSTEAD'
exec dbo.find_munic_area_locator 'OR', 'NOWHERE', 'NOWHERE'
*/
