if object_id('dbo.commit_billing') is not null
  drop procedure dbo.commit_billing
GO

create Procedure commit_billing(
	@BillID int,
	@CommittedBy int
	)
as
set nocount on

DECLARE @err int

declare @BillingDetail table (
  locate_id int,
  locate_hours_id int,
  bill_code varchar(20))

BEGIN TRANSACTION

insert into @BillingDetail (locate_id, locate_hours_id, bill_code)
select locate_id, locate_hours_id, bill_code
from billing_detail where bill_id = @BillID

update locate
set locate.invoiced=1
from @BillingDetail bd
where (locate.locate_id = bd.locate_id)
  and ((bd.locate_hours_id is null) or (bd.locate_hours_id = 0))

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update locate_hours
set locate_hours.hours_invoiced=1
from @BillingDetail bd
where (locate_hours.locate_hours_id = bd.locate_hours_id)
  and (bd.locate_hours_id > 0)
  and (bd.bill_code = 'HOURLY')

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update locate_hours
set locate_hours.units_invoiced=1
from @BillingDetail bd
where (locate_hours.locate_hours_id = bd.locate_hours_id)
  and (bd.locate_hours_id > 0)
  and (bd.bill_code = 'UNITS')

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update billing_header
   set billing_header.[committed] = 1,
	 billing_header.committed_date = getdate(),
	 billing_header.committed_by_emp = @CommittedBy
 where
    billing_header.bill_id = @BillID

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

COMMIT TRANSACTION

SELECT @err = @@error
IF @err <> 0 RETURN @err

GO

grant execute on commit_billing to uqweb, QManagerRole
/*
select * from billing_header where committed = 0
exec dbo.commit_billing 18997, 1076  -- 2:40
*/
