if object_id('dbo.version_to_number') is not null
  drop function dbo.version_to_number
go

create function dbo.version_to_number(@version varchar(30))
returns decimal(14, 0)
as
begin
  declare @v varchar(5)
  declare @pos int
  declare @number varchar(30)

  while (len(@version) <> 0) begin
    set @pos = charindex('.', @version)
    if @pos <> 0
      set @v = substring(@version, 0, @pos)
    else begin
      set @v = @version
      set @version = ''
    end

    while len(@v) < 3 begin
      set @v = '0' + @v
    end

    set @number = (coalesce(@number,'') + @v)
    set @version = stuff(@version, 1,  @pos, '')
  end

  return convert(decimal(14, 0), @number)
end

/*
select dbo.version_to_number ('123.456.789')
select dbo.version_to_number ('123.6.89')
select dbo.version_to_number ('1.6.99')
select dbo.version_to_number ('1.6.100')

select dbo.version_to_number ('2.1.0.9375')
select dbo.version_to_number ('1.2.3.4567')
select dbo.version_to_number ('123.456.7890')
select dbo.version_to_number ('2.1.0.10060')

*/

