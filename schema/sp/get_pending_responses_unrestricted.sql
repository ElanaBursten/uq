if object_id('dbo.get_pending_responses_unrestricted') is not null
	drop procedure dbo.get_pending_responses_unrestricted
GO

/* Like get_pending_responses, but does not restrict the number of records
   returned. */

CREATE Procedure get_pending_responses_unrestricted
	(
	@CallCenter varchar(20), @ParsedLocatesOnly bit = 1
	)
as
set nocount on

select rq.locate_id, locate.status, ticket.ticket_format,
 ticket.ticket_id, ticket.ticket_number, locate.client_code,
 ticket.ticket_type, ticket.work_state, rq.insert_date, ticket.do_not_respond_before,
   (select min(arrival_date) from jobsite_arrival ja
      where ja.ticket_id=ticket.ticket_id) as first_arrival_date
 from responder_queue rq
 inner join locate on rq.locate_id=locate.locate_id
 inner join ticket on locate.ticket_id=ticket.ticket_id
 where (@CallCenter = '' or @CallCenter = rq.ticket_format)
 and ticket.ticket_number not like 'MAN%'
 and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
 and (rq.do_not_respond_before is null 
      or rq.do_not_respond_before <= getdate())
 order by insert_date
GO

grant execute on get_pending_responses_unrestricted to uqweb, QManagerRole
GO

/*
exec get_pending_responses_unrestricted ''
exec get_pending_responses_unrestricted 'Atlanta'
exec get_pending_responses_unrestricted 'OCC1'
exec get_pending_responses_unrestricted 'OCC2'
exec get_pending_responses_unrestricted 'FCV1'
exec get_pending_responses_unrestricted 'FMW1'
exec get_pending_responses_unrestricted 'FMB1'
exec get_pending_responses_unrestricted 'FDE1'

select ticket.ticket_format, min(insert_date) as oldest, count(*) as N
 from responder_queue rq (NOLOCK)
 inner join locate (NOLOCK) on rq.locate_id=locate.locate_id
 inner join ticket (NOLOCK) on locate.ticket_id=ticket.ticket_id
group by ticket.ticket_format
*/
