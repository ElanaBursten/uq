if object_id('dbo.ticket_notes') is not null
	drop view dbo.ticket_notes
go

create view ticket_notes
as
  -- ticket notes:
  select t.ticket_id, n.notes_id, n.entry_date, u.emp_id added_by_emp_id, n.note, n.foreign_id, 1 foreign_type, n.modified_date, n.sub_type
  from ticket t
    inner join notes n with (index(notes_foreign)) on (n.foreign_id = t.ticket_id and n.foreign_type = 1) 
    left join users u on n.uid = u.uid 
  where (n.active = 1)

  union all

  -- locate notes:
  select t.ticket_id, n.notes_id, n.entry_date, u.emp_id added_by_emp_id, c.client_name + ': ' + n.note note, n.foreign_id, 5 foreign_type, n.modified_date, n.sub_type
  from ticket t  
    inner join locate l on t.ticket_id = l.ticket_id
    inner join notes n with (index(notes_foreign)) on (n.foreign_id = l.locate_id and n.foreign_type = 5)
    inner join client c on l.client_id = c.client_id 
    left join users u on n.uid = u.uid
  where (n.active = 1)    

/*
select * from ticket_notes where ticket_id = 54561871
*/
