if object_id('dbo.RPT_loadsheet') is not null
	drop procedure dbo.RPT_loadsheet
go

create proc RPT_loadsheet(
  @Today datetime,
  @LocatorList varchar(8000)
)
as
  declare @emp_ticket table (emp_id int, ticket_id int)

  set nocount on

  /* select all open tickets for each locator */
  insert into @emp_ticket (
    emp_id,
    ticket_id)
  select distinct
    Locators.ID,
    locate.ticket_id
  from dbo.IdListToTable(@LocatorList) as Locators
    left join locate with (INDEX(locate_assigned_to))
      on locate.assigned_to = Locators.ID
  /* for some reason adding the hint to the above makes it
     use that correct index on the reporting DB. */

  select
    employee.short_name,
    case
      when ticket.ticket_id is null then
        'No Open Tickets'
      when exists(select * from locate where ticket_id = ticket.ticket_id and status = 'O') then
        'Ongoing'
      when ticket.due_date < @Today then
        'Overdue'
      when ticket.kind = 'EMERGENCY' then
        'Emergency'
      when ticket.transmit_date > @Today then
        'New'
      else
        'Carryover'
    end as category,
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date
  from @emp_ticket as et
    inner join employee
      on employee.emp_id = et.emp_id
    left join ticket
      on ticket.ticket_id = et.ticket_id
  where
    (et.ticket_id is not null) or
    not exists(select * from @emp_ticket te where te.emp_id = et.emp_id and te.ticket_id is not null)
  order by
    employee.short_name,
    category,
    ticket.due_date
go
grant execute on RPT_loadsheet to uqweb, QManagerRole

/*
declare @now datetime
select @now = getdate()
exec RPT_loadsheet_2 @now, '201,206,203,200', 1
*/

