if object_id('dbo.set_locate_assigned_to') is not null
  drop procedure dbo.set_locate_assigned_to
GO

create proc set_locate_assigned_to (@howmany int)
as
  set nocount on

  declare @StartingID int
  declare @UpperBound int

  set @StartingID = (select max(nextlocateid) from locate_assigned_marker)
  set @UpperBound = @StartingID + @howmany

  update locate
   set assigned_to_id = (select max(locator_id) 
                         from assignment a
                         where a.locate_id=locate.locate_id and a.active=1)
    where locate.locate_id between @StartingID and (@UpperBound-1)
   
  select @@rowcount

  update locate_assigned_marker
    set nextlocateid = @UpperBound
GO


/*
create table locate_assigned_marker (
	nextlocateid int not null
)
insert into locate_assigned_marker values (1)

update locate_assigned_marker set nextlocateid=20032
select min(locate_id) from locate
select top 250  * from locate order by modified_date desc

select nextlocateid, (select max(locate_id) from locate) as maxLoc from locate_assigned_marker

exec dbo.set_locate_assigned_to 100000


alter table locate add assigned_to_id integer null
drop index locate.locateclosed

create index locate_assigned_closed on locate(assigned_to_id, closed_date )

select nextlocateid, (select max(locate_id) from locate) as maxLoc from locate_assigned_marker
*/
