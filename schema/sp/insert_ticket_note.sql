if object_id('dbo.insert_ticket_note') is not null
	drop procedure dbo.insert_ticket_note
GO

create proc insert_ticket_note (@TicketID int, @EmpID int,
  @EntryDate datetime, @Note varchar(8000))
as

DECLARE @UserID int

set @UserID = (select top 1 uid from users where emp_id = @EmpID
               and active_ind=1)

insert into notes
  (foreign_type, foreign_id, entry_date, uid, note)
values
  (1, @TicketID, @EntryDate, @UserID, @Note)

grant execute on insert_ticket_note to uqweb, QManagerRole
GO
