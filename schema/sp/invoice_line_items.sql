/*
  This gets the invoice line items for a customer & billing run
  It currently does not handle the Transmission style billing.
*/
if object_id('dbo.invoice_line_items') is not null
  drop function dbo.invoice_line_items
go

create function invoice_line_items (@OutputConfigID int, @BillID int)
returns @lineitems TABLE (
  liid int identity,
  subcategory varchar(50),
  billing_cc varchar(35),
  line_item_text varchar(60),
  rate money,
  price money,
  Tickets int,
  Units int,
  UnitsCharged int,
  hours decimal(38, 3),
  Amount money,
  omit_zero_amount bit,
  pre_tax_amount money,
  tax_name varchar(40),
  tax_rate decimal(6,4),
  tax_amount money,
  source varchar(10) null,
  plat_group varchar(35) null
)
as
begin
  declare @SalesTaxIncluded bit
  declare @ConsolidateByLineItemText bit
  declare @GroupByPrice bit
  declare @CustomerID integer
  declare @OmitZeroAmount bit
  declare @IncludeTrans bit
  declare @IncludeBulk bit
  declare @WhichInvoice varchar(20)
  declare @FlatFee money

  declare @CustBillingDetail table (
    locate_id int not null,
    billing_cc varchar(10) not null,
    line_item_text varchar(60) null,
    rate money not null,
    price money not null,
    qty_marked integer null,
    qty_charged int null,
    hours DECIMAL(9,3) null,
    status varchar(5) not null,
    work_county varchar(40) null,
    work_state varchar(2) null,
    work_city varchar(40) null,
    locator_id int not null,
    tax_name varchar(40) null,
    tax_rate decimal(6,4) null,
    tax_amount money null,
    con_name varchar(50) null
  )

  declare @termidlist table (
    client_id int not null,
    termid varchar(20) NOT NULL
  )

  select @IncludeTrans = transmission_billing, -- CA style transmissions
         @IncludeBulk = transmission_bulk,   -- BULK1K transmissions
         @WhichInvoice = which_invoice,
         @ConsolidateByLineItemText = consol_line_item,
         @OmitZeroAmount = omit_zero_amount,
         @CustomerID = customer_id,
         @GroupByPrice = group_by_price,
         @SalesTaxIncluded = sales_tax_included,
         @FlatFee = flat_fee
  from billing_output_config
  where output_config_id=@OutputConfigID

  insert into @termidlist
  select cli.client_id, cli.oc_code
    from client cli
  where cli.customer_id = @CustomerID

  -- Pre-calculated billing_detail records for the specific customer and invoice name
  insert into @CustBillingDetail
    (locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
     work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name)
  select
     locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
     work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name
  from billing_detail where bill_id = @BillID
    and client_id in (select client_id from @termidlist)
    and (coalesce(@WhichInvoice, '') = coalesce(which_invoice, ''))

  if  (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
  begin
    -- The rounding below corrects for some old Loc, Inc hourly locates
    -- with fractional pennies in the price field (April, 2006)
    -- IF we are NOT in Included Sales Tax mode, this will insert items:
    insert into @lineitems
    (subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select
      scci.subcategory,
      case when @ConsolidateByLineItemText=1 then 'ALL-TERMS' else billing_cc end as billing_cc,
      line_item_text,
      case when @ConsolidateByLineItemText=1 then 0 else rate end as rate,
      case when @GroupByPrice=1 then round(price,2) else 0 end as price,
      count(*) as Tickets,
      sum(qty_marked) as Units,
      sum(qty_charged) as UnitsCharged,
      sum(hours) as Hours,
      sum(round(price,2)) as Amount,
      @OmitZeroAmount
    from @CustBillingDetail bd
     left join billing_scc_invoice scci
       on bd.work_state=scci.state and bd.work_county=scci.county
          and bd.work_city=scci.city and scci.active=1
          and (scci.customer_id = @CustomerID or scci.customer_id is null)
    group by
      scci.subcategory,
      case when @ConsolidateByLineItemText=1 then 'ALL-TERMS' else billing_cc end,
      line_item_text,
      case when @ConsolidateByLineItemText=1 then 0 else rate end,
      case when @GroupByPrice=1 then round(price,2) else 0 end
  end

  if  @SalesTaxIncluded=1
  begin
    -- IF we ARE in Included Sales Tax mode, this will insert items:
    insert into @lineitems
    (subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount,
     pre_tax_amount, tax_name, tax_rate, tax_amount)
    select
      scci.subcategory,
      case when @ConsolidateByLineItemText=1 then 'ALL-TERMS' else billing_cc end as billing_cc,
      line_item_text,
      case when @ConsolidateByLineItemText=1 then 0 else rate end as rate,
      case when @GroupByPrice=1 then round(price,2) else 0 end as price,
      count(*) as Tickets,
      sum(qty_marked) as Units,
      sum(qty_charged) as UnitsCharged,
      sum(hours) as Hours,
      sum(round(price,2)) as Amount,
      @OmitZeroAmount,
      sum(price) - sum(tax_amount) as pre_tax_amount,
      tax_name, tax_rate, sum(tax_amount) as tax_amount
    from @CustBillingDetail bd
     left join billing_scc_invoice scci
       on bd.work_state=scci.state and bd.work_county=scci.county
          and bd.work_city=scci.city and scci.active=1
          and (scci.customer_id=@CustomerID or scci.customer_id is null)
    group by
      scci.subcategory,
      case when @ConsolidateByLineItemText=1 then 'ALL-TERMS' else billing_cc end,
      line_item_text,
      case when @ConsolidateByLineItemText=1 then 0 else rate end,
      case when @GroupByPrice=1 then round(price,2) else 0 end,
      tax_name, tax_rate
  end
  if @FlatFee > 0 
  begin
    -- Some customers are set to be charged flat fees per billing cycle
    -- (See billing_output_config.flat_fee) rather than per locate/transmission
    insert into @lineitems
    (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select
      null as subcategory,
      'FLATFEE' as billing_cc, 'Flat Fee' as line_item_text,
      @FlatFee as rate,
      @FlatFee as price,
      0 as Tickets, 1 as Units, 1 as UnitsCharged, 0 as Hours,
      @FlatFee as Amount,
      0 as omit_zero_amount
  end

  -- TODO: Need CA Trans & Bulk Trans billing stuff here....

  
  -- Delete 0-amount items, if configured to do so:
  if @OmitZeroAmount = 1
  begin
    delete from @lineitems
    where amount=0 or amount is null
  end

  return
end
GO

/*
select * from dbo.invoice_line_items(473,32641)
select * from dbo.invoice_line_items(473,32603)
*/
