if object_id('dbo.get_locate_work_status') is not null
  drop function dbo.get_locate_work_status
go

create function get_locate_work_status(@LocateStatus varchar(5))
returns
  varchar(15)
as
begin
  declare @MarkedStatusGroupID int
  declare @ClearedStatusGroupID int

  set @MarkedStatusGroupID = (select sg_id from status_group where sg_name = 'Location-MarkStatusList')
  set @ClearedStatusGroupID = (select sg_id from status_group where sg_name = 'Location-ClearStatusList')

  return (select Max(case sg_id
                     when @MarkedStatusGroupID then 'MARKED'
                     when @ClearedStatusGroupID then 'CLEARED'
                     else NULL end)
          from status_group_item
          where status = @LocateStatus
            and sg_id in (@MarkedStatusGroupID, @ClearedStatusGroupID)
            and active = 1)
          
end
go

grant execute on get_locate_work_status to uqweb, QManagerRole
go

/*
select dbo.get_locate_work_status('M')
select dbo.get_locate_work_status('C')
select dbo.get_locate_work_status('-R')
select dbo.get_locate_work_status('ABC')
*/
