if object_id('dbo.RPT_employees_3') is not null
	drop procedure dbo.RPT_employees_3
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

CREATE PROCEDURE RPT_employees_3 (
  @ManagerID int,
  @EmployeeStatus int,
  @EmployeeTypeList Varchar(200)
)
AS

if @EmployeeTypeList = '' 
begin
  select hier.*, users.login_id, r.code as h_type_id_code
  from dbo.get_report_hier3(@ManagerID, 0, 9, @EmployeeStatus) hier
       left join reference r on r.ref_id=h_type_id
       left outer join users on (users.emp_id = h_emp_id and users.active_ind = 1)
  select 'All' as code, 'All Employee Types' as description
end
else begin
  select hier.*, users.login_id, r.code as h_type_id_code
  from dbo.get_report_hier3(@ManagerID, 0, 9, @EmployeeStatus) hier
       left join reference r on r.ref_id=h_type_id
       left outer join users on (users.emp_id = h_emp_id and users.active_ind = 1)
       inner join dbo.StringListToTable(@EmployeeTypeList) l on hier.h_type_id = l.S
  select code, description 
  from reference 
  inner join dbo.StringListToTable(@EmployeeTypeList) on ref_id = S
end

GO
grant execute on RPT_employees_3 to uqweb, QManagerRole


/*
exec dbo.RPT_employees_3 2676, 0, ''  -- show only inactive employees, all employee types
exec dbo.RPT_employees_3 2676, 0, '8,784'  -- show only inactive employees, managers/damage investigators
exec dbo.RPT_employees_3 211, 1, '8,784'  -- show only active employees, all employee types
exec dbo.RPT_employees_3 211, 2, '8,10,11'  -- show all, managers/supervisors/locators
exec dbo.RPT_employees_3 1005, 1, '1015'  -- returns no rows
*/
