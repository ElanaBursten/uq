--TRIGGER for work_order
if object_id('dbo.work_order_iu') is not null
  drop trigger dbo.work_order_iu
go

create trigger dbo.work_order_iu ON dbo.work_order
FOR INSERT, UPDATE
AS
BEGIN
  declare @wo_id int; 
  declare @due_date datetime;
  declare @wo_source varchar(20);
  declare @wo_number varchar(20);
  declare @opt char; 

 If Exists(select * from DELETED)
      select @opt='U'
	  else
	  select @opt='I'

  if @opt='I'
  select @wo_id=wo_id, @wo_source=wo_source, @wo_number=wo_number, @due_date=due_date from inserted

  --
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- Update the modified_date when a wo changes
  update work_order set
    modified_date = GetDate()
  where wo_id in (select wo_id from inserted) 

  if @wo_source='OHM' and @opt='I'
  begin
	INSERT INTO dbo.work_order_OHM_details
			   (wo_id
			    ,wo_number
				,due_date
				,modified_date)
		 VALUES
			   (@wo_id
			   ,@wo_number
			   ,@due_date
			   ,GetDate())    
  end
END
GO