-- You may need to update RPT_tse_manager when you update this 
if object_id('dbo.RPT_tse_emp') is not null
	drop procedure dbo.RPT_tse_emp
go

create proc RPT_tse_emp(
	@EmpID int,
	@EndDate datetime
)
as
 set nocount on
 declare @StartDate datetime
 set @StartDate = dateadd(d, -6, @EndDate)

 select e.first_name, e.last_name, e.emp_number, e.short_name, e.emp_id,
    dd.d as display_work_date,
    tse.*,
    Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0) as mileage,
	(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt,
    ent_emp.emp_number as ent_emp_number,
    ent_emp.short_name as ent_emp_short_name,
    app_emp.emp_number as app_emp_number,
    app_emp.short_name as app_emp_short_name,
    fap_emp.emp_number as fap_emp_number,
    fap_emp.short_name as fap_emp_short_name,
    @EndDate as report_end_date,
    case when final_approve_by is not null then 'FINAL APPROVED'
         when approve_by is not null       then 'APPROVED'
         when entry_by is not null         then 'ENTERED'
         else                                   '-'
    end as day_status,
    et.description as emp_type
  from employee e
   inner join dbo.DateSpan(@StartDate, @EndDate) dd on 1=1
   left join timesheet_entry tse  on tse.work_emp_id = e.emp_id and tse.work_date=dd.d
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee ent_emp on tse.entry_by = ent_emp.emp_id
   left join employee app_emp on tse.approve_by = app_emp.emp_id
   left join employee fap_emp on tse.final_approve_by = fap_emp.emp_id
   left join reference et on tse.emp_type_id = et.ref_id
  where e.emp_id=@EmpID
  order by e.emp_id, dd.d

  -- totals for the emp, might make it easier to do some reports
  select
   min(e.emp_id) as emp_id,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0)) as mileage,
   sum(case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday,
   sum(case when tse.per_diem = 1 then 45 else 0 end) as per_diem

  from employee e
   left join timesheet_entry tse on tse.work_emp_id = e.emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   where e.emp_id=@EmpID
  group by e.emp_id
go
grant execute on RPT_tse_emp to uqweb, QManagerRole
/*
exec dbo.RPT_tse_emp 5579, '2004-04-10'
exec dbo.RPT_tse_emp 3510, '2007-08-25'
*/
