if object_id('dbo.get_plat_list') is not null
  drop function dbo.get_plat_list
go

create function get_plat_list(@locate_id int)
returns varchar(400)
as
begin
  declare @plat_list varchar(500)
  declare @locate_plat varchar(20)

  set @plat_list = ''

  declare locate_plat_cursor cursor for
    select locate_plat.plat
    from locate_plat 
    where locate_plat.locate_id = @locate_id
      and active = 1

  open locate_plat_cursor
  fetch next from locate_plat_cursor into @locate_plat

  while @@fetch_status=0 and Len(@plat_list) < 390 begin
    if @plat_list = '' 
      set @plat_list = @locate_plat
    else
      set @plat_list = @plat_list +', '+ @locate_plat
    fetch next from locate_plat_cursor into @locate_plat
  end

  close locate_plat_cursor
  deallocate locate_plat_cursor

  if Len(@plat_list) > 400 set @plat_list = SubString(@plat_list, 1, 390) + ' (more...)'
  return (@plat_list)
end
GO

grant execute on get_plat_list to uqweb, QManagerRole

/*
select distinct top 10 locate_id, dbo.get_plat_list(locate_id) from locate_plat
*/
