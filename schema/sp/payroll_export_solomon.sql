/****************************************

THIS HAS BEEN REPLACED by payroll_export_solomon2.sql see svn log for details

if object_id('dbo.payroll_export_solomon2') is not null
  drop procedure dbo.payroll_export_solomon2
*/
go

/* CREATE procedure payroll_export_solomon2
(
  @BEGIN_DATE DATETIME,
  @END_DATE DATETIME,
  @REGION VARCHAR(16)
) AS

-- END_DATE is the Saturday Ending

DECLARE @COUNT INTEGER
DECLARE @CARCOUNT INTEGER

if (not exists (select pc_code from profit_center where pc_code = @REGION))
  RAISERROR ('The payroll profit center %s was not found in the profit_center table.  Please add it using the QManager administration tool.', 15, 1, @REGION)

-- Chop off any time part
set @END_DATE = convert(datetime, convert(varchar(10), @END_DATE, 102), 102)

-- Make sure we landed on a Saturday, in case the :59:59:999 rounded to Sunday
while datepart(dw, @END_DATE) <> 7
  set @END_DATE = DATEADD(d, -1, @END_DATE)


-- The PAYROLL_CURSOR collects and sums all hours from the payroll tables.
-- It totals hours for each CEARNDEDID, and each CACCT.
DECLARE PAYROLL_CURSOR CURSOR FOR
SELECT
  'DETAIL' = 'DETAIL',
  'CTIMESHTNBR' = pc.timesheet_num,
  'CTRANDATE' =
    CASE
      WHEN DATALENGTH( CONVERT(VARCHAR, @END_DATE, 101) ) = 9  THEN '0' + CONVERT(CHAR(10), @END_DATE, 101)
      ELSE CONVERT(CHAR(10), @END_DATE, 101)
    END,
  'CEMPID' =
    CASE
      WHEN DATALENGTH( E.EMP_NUMBER ) > 5  THEN RIGHT(E.EMP_NUMBER, 5 )
      WHEN DATALENGTH( E.EMP_NUMBER ) = 1  THEN '0000' + E.EMP_NUMBER
      WHEN DATALENGTH( E.EMP_NUMBER ) = 2  THEN '000' + E.EMP_NUMBER
      WHEN DATALENGTH( E.EMP_NUMBER ) = 3  THEN '00' + E.EMP_NUMBER
      WHEN DATALENGTH( E.EMP_NUMBER ) = 4  THEN '0' + E.EMP_NUMBER
      WHEN DATALENGTH( E.EMP_NUMBER ) = 5  THEN E.EMP_NUMBER
      ELSE 'NO_EMPLOYEE_ID'
    END,
  'CWRKLOCID' = '',
  'CEARNDEDID' =
    CASE
      WHEN tr.code = 'NORM'   THEN 'REGH' -- NORM   rule defined in the reference table
      WHEN tr.code = 'CA'     THEN 'REGH' -- CA     rule defined in the reference table
      WHEN tr.code = 'TWEL40' THEN 'REGH' -- TWEL40 rule defined in the reference table
      WHEN tr.code = 'SAL'    THEN 'REGS' -- SAL    rule defined in the reference table
      WHEN tr.code = 'SALNEX' THEN 'REGN' -- SALNEX rule defined in the reference table
      ELSE 'TIMERULE_UNKNOWN'
    END,
  'CQTY' = ROUND((  SUM(ISNULL(T.REG_HOURS,0)) + SUM(ISNULL(T.OT_HOURS,0)) + SUM(ISNULL(T.DT_HOURS,0))),1),
  'OVER' = SUM(ISNULL(T.OT_HOURS,0)),
  'DT' = ROUND(SUM(ISNULL(T.DT_HOURS,0)),1),
  'CALLOUT' = ROUND(SUM(ISNULL(T.CALLOUT_HOURS,0)),1),
  'VACATION' = SUM(ISNULL(T.VAC_HOURS,0)),
  'LEAVE' = SUM(ISNULL(T.LEAVE_HOURS,0)),
  'BEREAVEMENT' =  SUM(ISNULL(T.BR_HOURS,0)),
  'HOLIDAY' = SUM(ISNULL(T.HOL_HOURS,0)),
  'JURY' = SUM(ISNULL(T.JURY_HOURS,0)),
  'CUNITPRICE' = '',
  'CTRANAMT' = '',
  'CACCT' =  '', --WT.ST_MINOR,NEED TO MAKE WORK
  'CPROJECTID' =  pc.gl_code,
  'CTASKID' =
    convert(varchar(10), coalesce(pc.timesheet_num, '999')) +
    convert(varchar(10), E.crew_num) +
    convert(varchar(20),
      CASE
        WHEN DATALENGTH( E.EMP_NUMBER ) > 5  THEN RIGHT( E.EMP_NUMBER, 5 )
        WHEN DATALENGTH( E.EMP_NUMBER ) = 1  THEN '0000' + E.EMP_NUMBER
        WHEN DATALENGTH( E.EMP_NUMBER ) = 2  THEN '000' + E.EMP_NUMBER
        WHEN DATALENGTH( E.EMP_NUMBER ) = 3  THEN '00' + E.EMP_NUMBER
        WHEN DATALENGTH( E.EMP_NUMBER ) = 4  THEN '0' + E.EMP_NUMBER
        WHEN DATALENGTH( E.EMP_NUMBER ) = 5  THEN E.EMP_NUMBER
        ELSE 'NO_EMPLOYEE_ID'
      END
    ),
  'CSUB' =  pc.timesheet_num,
  'CBILLABLE' =   '',
  'OT_MINOR' = ' ',    --WT.OT_MINOR,  --NEED TO MAKE WORK
  'ISDRIVER' =
    CASE
      WHEN E.COMPANY_CAR IS NULL THEN 0     --O.ISDRIVER     NEED TO MAKE WORK
      WHEN E.COMPANY_CAR = 'COV' THEN 1
    END,
  'POV_Hours' =
    SUM(
      CASE
        WHEN T.vehicle_use = 'POV' THEN ROUND(ISNULL(T.REG_HOURS,0) + ISNULL(T.OT_HOURS,0) + ISNULL(T.DT_HOURS,0) + ISNULL(T.CALLOUT_HOURS,0),1)
        ELSE 0
      END
    ),
  'COV_Hours' =
    SUM(
      CASE
        WHEN T.vehicle_use like 'COV%' THEN ROUND(ISNULL(T.REG_HOURS,0) + ISNULL(T.OT_HOURS,0) + ISNULL(T.DT_HOURS,0) + ISNULL(T.CALLOUT_HOURS,0),1)
        ELSE 0
      END
    ),
  'Charge_COV' = E.CHARGE_COV,
  'MILEAGE' =
    SUM(CASE
      WHEN T.vehicle_use = 'POV' THEN (Coalesce(T.miles_stop1,0) - Coalesce(T.miles_start1,0))
      ELSE 0
    END),
  (select lc.name from locating_company lc where lc.company_id=max(E.company_id)) as company
FROM
  TIMESHEET_ENTRY T
  join Employee AS E ON (T.WORK_EMP_ID = E.EMP_ID)
  join Reference AS TR on (TR.type = 'timerule') and (TR.ref_id = e.timerule_id)
  left outer join Employee AS WF ON (WF.EMP_ID = E.report_to)
  inner join dbo.employee_payroll_data(@REGION) AS pd ON (e.emp_id=pd.emp_id)
  inner join profit_center AS PC ON (pc.pc_code = @REGION)
WHERE
  T.WORK_DATE >= @BEGIN_DATE
  AND T.WORK_DATE <= @END_DATE
  AND T.FINAL_APPROVE_BY IS NOT NULL
  AND T.STATUS <> 'OLD'
  AND
    (
      ISNULL(T.reg_hours,0) +
      ISNULL(T.ot_hours,0) +
      ISNULL(T.dt_hours,0) +
      ISNULL(T.callout_hours,0) +
      ISNULL(T.VAC_HOURS,0) +
      ISNULL(T.LEAVE_HOURS,0) +
      ISNULL(T.BR_HOURS,0) +
      ISNULL(T.HOL_HOURS,0) +
      ISNULL(T.JURY_HOURS,0)
    ) > 0
  AND E.ACTIVE = 1
GROUP BY
  pd.emp_pc_code,
  E.EMP_NUMBER,
  CASE
    WHEN TR.Code = 'NORM'   THEN 'REGH' -- NORM   rule defined in the reference table
    WHEN TR.Code = 'CA'     THEN 'REGH' -- CA     rule defined in the reference table
    WHEN TR.Code = 'TWEL40' THEN 'REGH' -- TWEL40 rule defined in the reference table
    WHEN TR.Code = 'SAL'    THEN 'REGS' -- SAL    rule defined in the reference table
    WHEN TR.Code = 'SALNEX' THEN 'REGN' -- SALNEX rule defined in the reference table
    ELSE 'TIMERULE_UNKNOWN'
  END,
  E.COMPANY_CAR,
  E.CHARGE_COV,
  WF.short_name,
  pc.gl_code,
  pc.timesheet_num,
  e.crew_num

-- The following variables are used to hold the column values of the
-- Payroll_Cursor as we loop though and process the data.
DECLARE
  @VAR_Detail         CHAR(6),    -- Detail
  @VAR_TimeShtNbr     CHAR(8),    -- CTimeShtNbr
  @VAR_CTranDate      CHAR(10),   -- CTranDate
  @VAR_CEmpID         CHAR(16),   -- CEmpID
  @VAR_CWrkLocID      CHAR(8),    -- CWrkLocID
  @VAR_CEarn          CHAR(10),   -- Rule name used for regular hourly time
  @VAR_CQty           MONEY,      -- CQty
  @VAR_Over           MONEY,      -- Overtime worked
  @VAR_DT             MONEY,      -- Double-time worked
  @VAR_Callout        MONEY,      -- Callout time worked
  @VAR_Vacation       MONEY,      -- Vacation time taken
  @VAR_Leave          MONEY,      -- Leave time taken
  @VAR_Bereavement    MONEY,      -- Bereavement time taken
  @VAR_Holiday        MONEY,      -- Holiday time taken
  @VAR_Jury           MONEY,      -- Jury duty time taken
  @VAR_CUnitPrice     CHAR(10),   -- CUnitPrice
  @VAR_CTranAmt       CHAR(10),   -- CTranAmt
  @VAR_CAcct          CHAR(16),   -- CAcct
  @VAR_CProjectID     CHAR(16),   -- CProjectID
  @VAR_CTaskID        CHAR(12),   -- CTaskID
  @VAR_CSub           CHAR(10),   -- CSub
  @VAR_CBillable      CHAR(1),    -- CBillable
  @VAR_OT_Minor       CHAR(8),    -- OT_Minor
  @VAR_IsDriver       SMALLINT,   -- IsDriver
  @VAR_POV_Hours      MONEY,      -- POV_Hours
  @VAR_COV_Hours      MONEY,      -- COV_Hours
  @VAR_Charge_COV     SMALLINT,   -- Charge_COV
  @VAR_Mileage        INT,        -- Mileage
  @VAR_Company        VARCHAR(20) -- Company


-- Data will be used to hold the rows to be output by the stored procedure
-- after all calculations have been completed.
declare @data TABLE
(
  [detail]       [char] (6)   NOT NULL DEFAULT ('detail'),
  [ctimeshtnbr]  [char] (8)       NULL,
  [ctrandate]    [datetime]   NOT NULL,
  [cempid]       [char] (16)  NOT NULL,
  [cwrklocid]    [char] (8)   NOT NULL,
  [cearndedid]   [char] (10)  NOT NULL,  -- Indicates the type of pay for the row (such as "REGN", "OT", "DT", "VACA")
  [cqty]         [money]          NULL,
  [ccunitprice]  [char] (10)      NULL,
  [ctranamt]     [char] (10)      NULL,
  [cacct]        [char] (16)      NULL,
  [cprojectid]   [char] (16)      NULL,
  [ctaskid]      [varchar] (12)   NULL,
  [csub]         [char] (10)      NULL,
  [cbillable]    [char] (1)       NULL,
  [OT_MINOR]     [char] (8)       NULL,
  [ISDRIVER]     [smallint]       NULL,
  [company_name] [varchar] (20)    NULL
)

-- SELECT @VAR_POV_Hours = 0 -- Commented out by James on 4/2/2006 since it seems to serve no purpose (it gets set in the following Fetch statement)


-- Loop through all rows in PAYROLL_CURSOR processing them as required.
-- Data to be returned as output is inserted into the @Data table.
OPEN PAYROLL_CURSOR
FETCH NEXT FROM PAYROLL_CURSOR INTO
  @VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_CQty, @VAR_Over, @VAR_DT, @VAR_Callout, @VAR_Vacation,
  @VAR_Leave, @VAR_Bereavement, @VAR_Holiday, @VAR_Jury, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub,
  @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_POV_Hours, @VAR_COV_Hours, @VAR_Charge_COV, @VAR_Mileage, @VAR_Company
WHILE (@@FETCH_STATUS=0)   begin

  -- Note: Upon entering this loop (after fetching a row) VAR_CEarn indicates
  --       the timesheet rule / code which applies to regular hours.  Once
  --       regular hours are calculated the below code uses @VAR_CEarn as a
  --       variable and sets it to the type of earnings being calculated as
  --       necessary for inserting into @Data.

  --REGULAR HOURS CALCULATION
  IF @VAR_CEarn = 'REGH'
    BEGIN
      -- Hourly employee
      SET @VAR_CAcct = ''
    END
  IF @VAR_CEarn = 'REGS'
    BEGIN
      -- Salaried employee @ 40 hours per week
      SET @VAR_CAcct = ''
      -- 40 hours - Vacation, sick, bereavement, holiday and jury duty
      SET @VAR_CQty = 40 - @VAR_Vacation - @VAR_Leave - @VAR_Bereavement - @VAR_Holiday - @VAR_Jury
      IF @VAR_CQty < 0 BEGIN SET @VAR_CQty = 0 END
      SET @VAR_Over = 0 -- Zero out, salaried employes aren't paid over-time
      SET @VAR_DT = 0 -- Zero out, salaried employes aren't paid double time
      SET @VAR_Callout = 0 -- Zero out, salaried employes aren't paid callout time
    END
  IF @VAR_CEarn = 'REGN'
    BEGIN
      SET @VAR_CAcct = @VAR_CAcct -- Only here to parse properly in SQL 2005
      -- Salaried Non-Exempt (SNE) employee @ 40 hours per week
      -- NOTE: DT & Callout hours should always be calculated as zero by the
      --       hours calculator for SNE employees.
    END

  INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_CQty, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)

  --OVERTIME CALCULATIONS
  IF @VAR_Over <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'OT'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Over, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --DT CALCULATIONS - NOT SURE HOW TO USE THESE YET, @VAR_DT
  IF @VAR_DT <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'DT'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_DT, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --CALLOUT CALCULATION
  IF @VAR_Callout <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'CLT1'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Callout, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --VACATION CALCULATION WILL BE TAKEN OUT TILL IT IS FIGURE WHETHER TO HANDLE THESE HOURS ELECTRONICALLY
  IF @VAR_Vacation <> 0
  BEGIN
    SET @VAR_CAcct = ''
    SET @VAR_CEarn = 'VACA'
    INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Vacation, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
  END

  --LEAVE
  IF @VAR_Leave <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'SICK'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Leave, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --BEREAVEMENT
  IF @VAR_Bereavement <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'BEREAV'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Bereavement, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --HOLIDAY CALCULATIONS
  IF @VAR_Holiday <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'HOLI'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Holiday, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --JURY PAY
  IF @VAR_Jury <> 0
    BEGIN
      SET @VAR_CAcct = ''
       SET @VAR_CEarn = 'JURY'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Jury, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --CAR CALCULATION COV
  IF @VAR_COV_Hours  > 0 or ( @VAR_Charge_COV = 1 AND @VAR_TimeShtNbr in ('300','330','350','370', '600','620','640','660', '400','401', '402', '403', '404', '405', '406', '407', '499') )
    BEGIN
      SET @VAR_CEarn = 'CAR'
      SET @VAR_CUnitPrice = 15
      SET @VAR_CAcct = ''
      SET @VAR_CProjectID = ''
      SET @VAR_CTaskID = ''
      SET @VAR_CSub = ''
      SET @VAR_CBillable = ''
      SET @VAR_OT_Minor = ''
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, 1, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --CAR CALCULATION POV
  IF @VAR_POV_Hours > 0 AND @VAR_COV_Hours  <= 0
    BEGIN
      IF @VAR_POV_Hours > 42
      BEGIN
        SET @VAR_POV_Hours =42
      END
      SET @VAR_CEarn = 'CAR3'
      SET @VAR_CUnitPrice = 2.5
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_POV_Hours, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  --MILEAGE
  IF @VAR_Mileage <> 0
    BEGIN
      SET @VAR_CAcct = ''
      SET @VAR_CEarn = 'MILE'
      SET @VAR_CUnitPrice = '0.41'
      INSERT INTO @data values (@VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_Mileage, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub, @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_Company)
    END

  FETCH NEXT FROM PAYROLL_CURSOR INTO
    @VAR_Detail, @VAR_TimeShtNbr, @VAR_CTranDate, @VAR_CEmpID, @VAR_CWrkLocID, @VAR_CEarn, @VAR_CQty, @VAR_Over, @VAR_DT, @VAR_Callout, @VAR_Vacation,
    @VAR_Leave, @VAR_Bereavement, @VAR_Holiday, @VAR_Jury, @VAR_CUnitPrice, @VAR_CTranAmt, @VAR_CAcct, @VAR_CProjectID, @VAR_CTaskID, @VAR_CSub,
    @VAR_CBillable, @VAR_OT_Minor, @VAR_IsDriver, @VAR_POV_Hours, @VAR_COV_Hours, @VAR_Charge_COV, @VAR_Mileage, @VAR_Company
END
CLOSE PAYROLL_CURSOR
DEALLOCATE PAYROLL_CURSOR


-- Output summary result set with row counts grouped by company name
SELECT
  company_name,
  count(*)
FROM
  @data
GROUP BY
  company_name

-- Output detail result
-- The first 14 columns get exported as CSV
SELECT
  company_name, detail, ctimeshtnbr, ctrandate, cempid, cwrklocid, cearndedid,
  cqty, ccunitprice, ctranamt, cacct, cprojectid, ctaskid, csub, cbillable
FROM
  @data
ORDER BY
  company_name,
  CEMPID asc,
  cearndedid desc
*/
GO

--grant execute on payroll_export_solomon2 to uqweb, QManagerRole
GO


/*
exec dbo.payroll_export_solomon2 '6/20/2004' , '6/26/2004', '101'
exec dbo.payroll_export_solomon2 '10/10/2004' , '10/16/2004', '113'
exec dbo.payroll_export_solomon2 '10/31/2004' , '11/06/2004', 'FAM'
exec dbo.payroll_export_solomon2 '10/31/2004' , '11/06/2004 23:59:59', '176'
exec dbo.payroll_export_solomon2 '10/31/2004' , '11/06/2004 23:59:59', '110'
exec dbo.payroll_export_solomon2 '10/31/2004' , '11/06/2004 23:59:59', '139'
select * from office
select * from profit_center
*/
