if object_id('dbo.RPT_fax_status') is not null
	drop procedure dbo.RPT_fax_status
go

create procedure dbo.RPT_fax_status (
	@DateFrom datetime,
	@DateTo datetime,
	@CallCenter varchar(30)
)
as
  set nocount on

select * from fax_message 
 where call_center = @CallCenter
  and queue_date >=  @DateFrom
  and queue_date < @DateTo
 order by fax_status, fax_dest, queue_date

select fax_status, count(*) as N
 from fax_message 
 where call_center = @CallCenter
  and queue_date >=  @DateFrom
  and queue_date < @DateTo
 group by fax_status

  -- Data to put fax detail on report
  select
    case
      when caller_fax IS NULL or caller_fax=''
       THEN con_name+caller
      ELSE caller_fax
    end as fax_name,
    ticket.ticket_number,
    locate.client_code,
    ticket.ticket_format as call_center,
    ticket.con_name,
    ticket.caller,
    ticket.caller_phone,
    ticket.caller_fax,
    ls.status,
    ls.status_date,
    ls.insert_date,
    ticket.work_state,
    ticket.work_county,
    ticket.work_city,
    ticket.work_address_number,
    ticket.work_address_number_2,
    ticket.work_address_street,
    client.client_name,
    statuslist.status_name,
    isnull(office.company_name, '') as company_name,
    isnull(office.address1, '') as address1,
    isnull(office.address2, '') as address2,
    isnull(office.city, '') as city,
    isnull(office.state, '') as state,
    isnull(office.zipcode, '') as zipcode,
    isnull(office.phone, '') as phone,
    isnull(office.fax, '') as fax,
    ltrim(isnull(ticket.work_address_number, '') + ' ' +
          isnull(ticket.work_address_number_2, '') + ' ' +
          isnull(ticket.work_address_street, '')) as street,
    ls.ls_id,
    ticket.ticket_id,
    ls.locate_id,
    fm.fm_id,
    fm.previous_queue_date,
    fm.queue_date,
    (select top 1 status_message
     from fax_queue_rules fqr
     where
        fqr.status = statuslist.status 
        and fqr.call_center = fm.call_center
        and (fqr.client_code = locate.client_code or fqr.client_code = '*')
        and (fqr.state = ticket.work_state or fqr.state = '*') ) as status_message
  from fax_message fm
    inner join fax_message_detail fd on fm.fm_id = fd.fm_id
    inner join locate_status ls on fd.ls_id = ls.ls_id
    inner join locate on fd.locate_id = locate.locate_id
    inner join ticket on fd.ticket_id = ticket.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join statuslist on statuslist.status = ls.status
    left join office on office.office_id = client.office_id
  where fm.call_center = @CallCenter
   and fm.queue_date >=  @DateFrom
   and fm.queue_date < @DateTo
   and fm.fax_status <> 'Success'
   and fm.fax_status <> 'Queued'
  order by fm.fax_status, fax_name, ticket_number, client_code

  -- List of ticket numbers for the report
  select fm.fm_id, ticket.ticket_number
  from fax_message fm
    inner join fax_message_detail fd on fm.fm_id = fd.fm_id
    inner join ticket on fd.ticket_id = ticket.ticket_id
  where fm.call_center = @CallCenter
   and fm.queue_date >=  @DateFrom
   and fm.queue_date < @DateTo
  group by fm.fm_id, ticket_number
  order by fm.fm_id, ticket_number

GO
grant execute on RPT_fax_status to uqweb, QManagerRole
/*
exec dbo.RPT_fax_status '2003-07-30', '2003-07-31', 'FDE1'
exec dbo.RPT_fax_status '2003-07-30', '2003-07-31', 'NewJersey'
exec dbo.RPT_fax_status '2011-11-29', '2011-11-30', '1421'
*/
