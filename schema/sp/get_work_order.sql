if object_id('dbo.get_work_order2') is not null
  drop procedure dbo.get_work_order2
go

create proc get_work_order2(@WorkOrderID int)
as
  set nocount on

  declare @WOForeignType int
  set @WOForeignType = 7 -- 7 = work_order notes & attachments

  select 'work_order' as tname, work_order.*
  from work_order 
    where work_order.wo_id = @WorkOrderID

  select
    'wo_response_log' as tname,
    wo_response_log.wo_response_id,
    wo_response_log.wo_id,
    wo_response_log.response_date,
    wo_response_log.wo_source,
    wo_response_log.status,
    wo_response_log.response_sent,
    wo_response_log.success,
    wo_response_log.reply
  from wo_response_log 
  where wo_id = @WorkOrderID

  select 'notes' as tname, notes.*
  from notes
  where notes.foreign_type = @WOForeignType       
    and notes.foreign_id   = @WorkOrderID   -- foreign_id is the WO ID
    and notes.active = 1

  select 'attachment' as tname, attachment.*
  from attachment
  where attachment.foreign_type = @WOForeignType
    and attachment.foreign_id   = @WorkOrderID   -- foreign_id is the WO ID
    and attachment.active = 1
  
  select 'work_order_ticket' as tname, work_order_ticket.*
  from work_order_ticket
  where work_order_ticket.wo_id = @WorkOrderID
    and work_order_ticket.active = 1
  
  select 'ticket' as tname, ticket.*
  from ticket
  join work_order_ticket on work_order_ticket.ticket_id = ticket.ticket_id
  where work_order_ticket.wo_id = @WorkOrderID
    and work_order_ticket.active = 1
  
  select 'work_order_version' as tname, work_order_version.*
  from work_order_version
  where work_order_version.wo_id = @WorkOrderID

  select 'work_order_inspection' as tname, work_order_inspection.*
  from work_order_inspection
  where work_order_inspection.wo_id = @WorkOrderID

  select 'work_order_remedy' as tname, work_order_remedy.*
  from work_order_remedy
  where work_order_remedy.wo_id = @WorkOrderID

go

grant execute on get_work_order2 to uqweb, QManagerRole
go

/*
exec dbo.get_work_order2 110010
exec dbo.get_work_order2 110050
exec dbo.get_work_order2 110065
*/

