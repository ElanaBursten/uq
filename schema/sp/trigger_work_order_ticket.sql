if exists(select name FROM sysobjects where name = 'work_order_ticket_iu' AND type = 'TR')
  DROP TRIGGER work_order_ticket_iu
GO

CREATE TRIGGER work_order_ticket_iu ON work_order_ticket
FOR INSERT, UPDATE
not for replication
AS
BEGIN

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- Update the modified_date when a wo ticket changes
  update work_order_ticket set
    modified_date = GetDate()
  from inserted I 
  where work_order_ticket.wo_id = I.wo_id 
    and work_order_ticket.ticket_id = I.ticket_id

END
go
