if object_id('dbo.RPT_damageidexception') is not null
  drop procedure dbo.RPT_damageidexception
go

create proc RPT_damageidexception(
  @StartDamageID int,
  @EndDamageID int
)
as

set nocount on

declare @SequenceList table (
  sequence_no int not null
 )

declare @ExceptionDamageIDs table (
  uq_damage_id int null,
  description varchar(40) not null,
  sort_order int not null
)

declare @current_damage_id int
declare @highest int

set @highest = (select max(uq_damage_id) from damage)

if @EndDamageID=0 or @EndDamageID>@highest
  set @EndDamageID = @highest

set @current_damage_id = @StartDamageID

while (@current_damage_id <= @EndDamageID)
begin
  insert into @SequenceList values (@current_damage_id)
  set @current_damage_id = @current_damage_id + 1
end

insert into @ExceptionDamageIDs
  select sequence_no, 'Missing', sequence_no
  from @SequenceList left outer join damage on uq_damage_id=sequence_no
  where uq_damage_id is null

if not exists (select uq_damage_id from @ExceptionDamageIDs)
  insert into @ExceptionDamageIDs (description, sort_order) values ('No IDs out of Sequence', @EndDamageID+2);

insert into @ExceptionDamageIDs values (@StartDamageID, 'Start of Report Range', @StartDamageID-1)
insert into @ExceptionDamageIDs values (@EndDamageID, 'End of Report Range', @EndDamageID+1)


select uq_damage_id, description from @ExceptionDamageIDs
order by sort_order
go

grant execute on RPT_damageidexception to uqweb, QManagerRole
go
/*
exec dbo.RPT_damageidexception 0, 9999
exec dbo.RPT_damageidexception 200000, 0
exec dbo.RPT_damageidexception 200400, 200420
*/
