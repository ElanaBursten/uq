if object_id('dbo.get_ticket6') is not null
  drop procedure dbo.get_ticket6
go

create proc get_ticket6(@TicketID int, @UpdatesSince datetime)
as
  set nocount on

  select 'ticket' as tname, ticket.*, 
    coalesce(area.area_name, '') as route_area_name
  from ticket
  left join area on area.area_id = ticket.route_area_id
    where ticket.ticket_id = @TicketID

  select 'locate' as tname, locate.*,
   client.client_name,
   client.require_locate_units,
   (select max(assignment.locator_id)
    from assignment
    where locate.locate_id=assignment.locate_id and assignment.active=1
   ) as locator_id,
   (select sum(locate_hours.units_marked)
    from locate_hours
    where locate_hours.locate_id=locate.locate_id
   ) as length_total
  from locate
  left join client on locate.client_id = client.client_id
  where locate.ticket_id = @TicketID
    and (locate.status <> '-N')
    and (locate.status <> '-P')

  select 'response_log' as tname,
    response_log.response_id,
    response_log.locate_id,
    response_log.response_date,
    response_log.call_center,
    response_log.status,
    response_log.response_sent,
    response_log.success,
    -- work around for junk data issue:
    case when (call_center='FMB1' or call_center='FMW1') and reply like '%N' then '(hidden)'
         else response_log.reply end as reply
  from locate
    inner join response_log with (index(response_log_locate_id))
      on response_log.locate_id = locate.locate_id
  where locate.ticket_id = @TicketID

  select 'notes' as tname, notes.*
   from notes
   where notes.foreign_type = 1       -- 1 = ticket notes
     and notes.foreign_id = @TicketID -- foreign_id has the ticket ID
     and notes.active = 1             -- get only active ticket notes
  union
  select 'notes' as tname, notes.*
   from notes inner join locate on (notes.foreign_id = locate.locate_id)
   where notes.foreign_type = 5        -- 5 = locate notes
     and locate.ticket_id = @TicketID
     and notes.active = 1

  select 'locate_hours' as tname, locate_hours.*,
    employee.short_name,
    locate.client_code,
    billing_unit_conversion.unit_type,
    client.client_name
  from locate
  inner join locate_hours on locate_hours.locate_id = locate.locate_id
  left join employee on locate_hours.emp_id = employee.emp_id
  left join billing_unit_conversion on locate_hours.unit_conversion_id = billing_unit_conversion.unit_conversion_id
  left join client on client.client_id = locate.client_id
  where locate.ticket_id = @TicketID

  select 'attachment' as tname, attachment.*
  from attachment
  where attachment.foreign_id = @TicketID
    and attachment.foreign_type = 1
    and attachment.active = 1

  select 'locate_plat' as tname, locate_plat.*
    from locate_plat
   where locate_id in (select locate_id from locate where ticket_id = @TicketID)

  select 'ticket_version' as tname,
     ticket_version_id, ticket_id, ticket_revision, ticket_number, ticket_type, transmit_date,
     processed_date, serial_number, arrival_date, filename, ticket_format, source
  from ticket_version
  where ticket_id = @TicketID

  select 'jobsite_arrival' as tname, jobsite_arrival.*
    from jobsite_arrival
    where ticket_id = @TicketID
      and active = 1

  select 'locate_facility' as tname, locate_facility.*,
    locate.client_code,
    locate.client_id,
    client.client_name
  from locate
  inner join locate_facility on locate_facility.locate_id = locate.locate_id
  left join client on client.client_id = locate.client_id
  where locate.ticket_id = @TicketID

  if (@UpdatesSince <> Null) begin
    select 'client' as tname, client.*
      from client
     where modified_date > @UpdatesSince

    select 'call_center' as tname, call_center.*
      from call_center
     where modified_date > @UpdatesSince

    select 'employee' as tname, employee.*
      from employee
     where modified_date > @UpdatesSince

    select 'reference' as tname, reference.*
      from reference
     where modified_date > @UpdatesSince

    select 'statuslist' as tname, statuslist.*
      from statuslist
     where modified_date > @UpdatesSince

    select 'status_group' as tname, status_group.*
      from status_group
     where modified_date > @UpdatesSince

    select 'status_group_item' as tname, status_group_item.*
      from status_group_item
     where modified_date > @UpdatesSince
  end
go

grant execute on get_ticket6 to uqweb, QManagerRole
go

/*
exec dbo.get_ticket6 15320, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 10016630, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 13596692, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 13572526, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 17345511, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 38673907, '2012-09-01T07:00:00.000'
exec dbo.get_ticket6 38754194, '2012-09-01T07:00:00.000'
*/
