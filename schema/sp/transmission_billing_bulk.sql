if object_id('dbo.transmission_billing_bulk') is not null
	drop procedure dbo.transmission_billing_bulk
GO

create proc transmission_billing_bulk (
  @OutputConfigID int,
  @StartDate datetime,
  @EndDate datetime
)
as
set nocount on

declare @termidlist table (
	termid varchar(20) NOT NULL
)
declare @centerlist table (
	center varchar(20) NOT NULL
)
select customer.*
  from billing_output_config boc
  inner join customer on boc.customer_id=customer.customer_id
 where boc.output_config_id=@OutputConfigID

insert into @termidlist
select
  cli.oc_code  -- cli.call_center
 from billing_output_config boc
 inner join client cli on boc.customer_id=cli.customer_id
where boc.output_config_id=@OutputConfigID

insert into @centerlist
select
  cli.call_center
 from billing_output_config boc
 inner join client cli on boc.customer_id=cli.customer_id
where boc.output_config_id=@OutputConfigID
group by cli.call_center

--select * from @termidlist
--select * from @centerlist

-- Assumption: we only care about one termcode per ticket.

declare @trans table (
	cc varchar(30) not null,
	termcode varchar(30) not null,
	tn varchar(30) not null,
	tid int not null,
	transmit_date datetime not null
	primary key (termcode, tid)
)

insert into @trans
select
 max(ticket.ticket_format) as cc,
 locate.client_code as termcode,
 ticket.ticket_number,
 min(ticket.ticket_id) as ticket_id,
 min(ticket_version.transmit_date) as transmit_date
from ticket_version with (index(ticket_version_transmit_date))
  inner join ticket with (index(PK_ticket_ticketid)) 
    on ticket.ticket_id = ticket_version.ticket_id 
      and ticket.ticket_format in (select center from @centerlist)
  inner join locate
    on ticket.ticket_id = locate.ticket_id
      and locate.client_code in (select termid from @termidlist)
      and locate.status <> '-N'
      and locate.added_by = 'PARSER'
where ticket_version.transmit_date between @StartDate and @EndDate
group by locate.client_code, ticket.ticket_number
  
select
  tr.termcode,
  br.rate,
  count(tr.tid) as num_tix,
  round(count(tr.tid), -2) as num_tix_round,
  round(br.rate * round(count(tr.tid), -2) / 1000,2) as amount
from @trans tr
  left join billing_rate br
    on br.call_center = tr.cc
     and br.billing_cc = tr.termcode
     and br.bill_type='BULK1K'
group by 
  tr.termcode,
  br.rate
order by 1,2
/*
select
 tr.termcode,
 ticket.ticket_id,
 ticket.ticket_format as call_center, 
 ticket.ticket_number, 
 ticket.ticket_type, 
 ticket.con_name, 
 ticket.work_city, 
 tr.transmit_date as transmit_date,
 ticket.work_county,
 ticket.work_address_number,
 ticket.work_address_number_2,
 ticket.work_address_street,
 ticket.map_page,
  br.rate
from @trans tr
  inner join ticket with (index(PK_ticket_ticketid)) 
    on ticket.ticket_id = tr.tid
  left join billing_rate br
    on br.call_center = ticket.ticket_format
     and br.billing_cc = tr.termcode
     and br.bill_code='Trans'
order by 1,2,3,4,5

*/

GO
grant execute on transmission_billing_bulk to uqweb, QManagerRole

/*
-- Bulk rate billing example, BellSouth - Louisiana South District
exec dbo.transmission_billing_bulk 390, '2005-03-27', '2005-04-03'
exec dbo.transmission_billing_bulk 390, '2005-03-28', '2005-03-31'

exec dbo.transmission_billing_bulk 57, '2005-05-01', '2005-06-01'
exec dbo.transmission_billing_bulk 57, '2005-05-04', '2005-05-05'


select top 10 * from billing_rate

select bill_code, count(*)  from billing_rate group by bill_code order by 1

select * from client where call_center='FBL1' and active=1
and oc_code not in (
select billing_cc from billing_rate where call_center='FBL1'
)

select * from billing_rate where call_center='FBL1'
and billing_cc not in (
select oc_code from client where call_center='FBL1' and active=1
)


select * from locate where ticket_id in 
(select ticket_id from ticket where ticket_number='A0890227')



insert into @trans
select
 max(ticket.ticket_format) as cc,
 locate.client_code as termcode,
 ticket.ticket_number,
 min(ticket.ticket_id) as ticket_id,
 min(ticket_version.transmit_date) as transmit_date
from ticket_version with (index(ticket_version_transmit_date))
  inner join ticket with (index(PK_ticket_ticketid)) 
    on ticket.ticket_id = ticket_version.ticket_id 
      and ticket.ticket_format in (select center from @centerlist)
  inner join locate
    on ticket.ticket_id = locate.ticket_id
      and locate.client_code in (select termid from @termidlist)
      and locate.status <> '-N'
      and locate.added_by = 'PARSER'
where ticket_version.transmit_date between @StartDate and @EndDate
group by locate.client_code, ticket.ticket_number

*/
