if object_id('dbo.find_map_area_locator_with_cnty') is not null
  drop procedure dbo.find_map_area_locator_with_cnty
GO

CREATE procedure find_map_area_locator_with_cnty
	(
	@MapCode varchar(20),
	@MapPageNum int,
	@X varchar(20),
	@Y varchar(20),
	@County varchar(20)
	)
As

select area.area_name, area.area_id, employee.emp_id, employee.short_name
 from map
  inner join map_cell on map.map_id=map_cell.map_id
  inner join area (NOLOCK) on map_cell.area_id = area.area_id
  inner join employee (NOLOCK) on area.locator_id = employee.emp_id
 where map.ticket_code = @MapCode and map.county = @County
  and map_page_num=@MapPageNum
  and x_part=@X and y_part=@Y
  and employee.active = 1
  and employee.can_receive_tickets = 1
GO

grant execute on find_map_area_locator_with_cnty to uqweb, QManagerRole



