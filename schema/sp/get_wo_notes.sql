if object_id('dbo.get_wo_notes') is not null
	drop procedure dbo.get_wo_notes
GO

CREATE Procedure get_wo_notes(@WorkOrderID int)
as
set nocount on

select notes.note, work_order.wo_id, work_order.wo_number
from work_order
inner join notes on (notes.foreign_id = work_order.wo_id 
                     and notes.foreign_type = 7)
where notes.active = 1
and work_order.wo_id = @WorkOrderID 

GO

grant execute on get_wo_notes to uqweb, QManagerRole

/*
exec get_wo_notes 12345
*/
