
if object_id('[format_emp_number]') is not null
	drop FUNCTION [format_emp_number]
GO

/****** Object:  UserDefinedFunction [dbo].[format_emp_number]    Script Date: 12/11/2018 4:57:41 PM ******/
--special function to support payroll export to Ulti  sr 12/11/2018 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[format_emp_number] (@emp_number varchar(15)) 
returns char(6)
as
begin
  declare @OutStr char(6)

  select @OutStr = 
  case 
 --   when (len(@emp_number) = 7) and (left(@emp_number, 4) = '9999') then '000000'
    when len(@emp_number) > 6 then left(@emp_number, 6)
    when len(@emp_number) < 6 then dbo.left_fill(@emp_number, '0',6)
    else Isnull(@emp_number, '000000')
  end

  return @OutStr
end

GO

