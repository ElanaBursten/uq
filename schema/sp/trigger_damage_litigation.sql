if object_id('dbo.damage_litigation_i') is not null
  drop trigger dbo.damage_litigation_i
go

create trigger dbo.damage_litigation_i on damage_litigation
for insert
not for replication
as  
  update damage_litigation set added_by = modified_by
    where litigation_id in (select litigation_id from inserted)
go

if object_id('dbo.damage_litigation_iu') is not null
  drop trigger dbo.damage_litigation_iu
GO

create trigger dbo.damage_litigation_iu on damage_litigation
for insert, update
not for replication
as
  if SYSTEM_USER = 'no_trigger'
    return

  update damage_litigation set modified_date = getdate()
    where litigation_id in (select litigation_id from inserted)
go
