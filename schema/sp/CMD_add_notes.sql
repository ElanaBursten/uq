if object_id('dbo.CMD_add_notes') is not null
  drop procedure dbo.CMD_add_notes
go

create procedure CMD_add_notes(
  @NotesID int,
  @ForeignType int,
  @ForeignId int,
  @UID int,
  @EmpID int,
  @CreateDateTime datetime,
  @Note varchar(8000)
)
as
set nocount on
declare @NewNotesID int

set @NewNotesID = (select max(notes_id) from notes 
  where foreign_type = @ForeignType
    and foreign_id = @ForeignID
    and uid = @UID
    and entry_date = @CreateDateTime
    and note = @Note)

if @NewNotesID is null begin
  insert notes (foreign_type, foreign_id, uid, entry_date, note)
    values (@ForeignType, @ForeignID, @UID, @CreateDateTime, @Note)

  set @NewNotesID = @@IDENTITY
end

select 'notes' table_name, @NotesID old_id, @NewNotesID new_id
go

grant execute on CMD_add_notes to uqweb, QManagerRole

/*
exec dbo.CMD_add_notes -1234, 1, 1101, 1111, 2222, 'Jun 10, 2014 15:27:00', 'Hello world!'
exec dbo.CMD_add_notes -1234, 1, 1101, 1111, 2222, 'Jun 10, 2014 15:27:00', 'Hello world!'
exec dbo.CMD_add_notes -456, 1, 1101, 1111, 2222, 'Jun 10, 2014 15:27:00', 'Can you hear me now?'
*/
