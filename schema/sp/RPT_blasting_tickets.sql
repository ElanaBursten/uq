if object_id('dbo.RPT_blasting_tickets2') is not null
	drop procedure dbo.RPT_blasting_tickets2
GO

CREATE proc RPT_blasting_tickets2 (
	@DateFrom datetime,
	@DateTo datetime,
        @ClientIds varchar(400),
        @ManagerID int
)
as
  set nocount on

  declare @ClientList table (
    client_id integer primary key
  )

  insert into @ClientList
    select S from dbo.StringListToTable(@ClientIds)

  declare @locates table (
	lid int not null,
	ticket_id int not null,
	emp_id int null,
	closed_date datetime null,
	client_code varchar(10) NULL,
	high_profile BIT NULL,
	high_profile_reason int NULL
  )

  insert into @locates (lid, ticket_id, closed_date, client_code, high_profile, high_profile_reason, emp_id)
  select l.locate_id, l.ticket_id, l.closed_date, l.client_code,
      l.high_profile, l.high_profile_reason,
      coalesce(l.assigned_to, l.closed_by_id) as emp_id
    from locate l
    inner join ticket tic on l.ticket_id = tic.ticket_id
    where tic.transmit_date between @DateFrom and @DateTo
     and tic.explosives = 'Y'
     and ((l.client_id in (select client_id from @ClientList)) or (@ClientIds = ''))

  if @ManagerID>0 begin
    delete from @locates
     where emp_id not in (select h_emp_id from dbo.get_hier(@ManagerID, 0))
       or emp_id is null
  end

  select
  t.ticket_id,
  t.ticket_number,
  t.explosives,
  t.transmit_date,
  t.due_date,
  t.work_state,
  t.work_description,
  t.work_county,
  t.work_city,
  t.work_address_number,
  t.work_address_street,
  t.work_cross,
  t.work_type,
  t.work_date,
  t.company,
  t.con_name,
  t.con_state,
  t.caller_contact,
  t.caller_phone,
  t.caller_cellular,
  t.ticket_type,
  t.ticket_format,
  t.call_date,
  t.con_address,
  t.con_city,
  t.con_state,
  t.con_zip,
  t.map_page,
  l.emp_id,
  l.closed_date,
  l.client_code,
  l.high_profile,
  l.high_profile_reason,
  reference.code,
  reference.description,
  employee.short_name
  from ticket t
    inner join @locates l on t.ticket_id = l.ticket_id
    inner join employee on employee.emp_id = l.emp_id
    left outer join reference on l.high_profile_reason = reference.ref_id
  order by ticket_format, ticket_number, t.ticket_id

  select distinct oc_code as client_code
  from client c
    inner join (select client_id from @ClientList) cl on cl.client_id = c.client_id 

GO
grant execute on RPT_blasting_tickets2 to uqweb, QManagerRole

/*
exec dbo.RPT_blasting_tickets2 '2004-08-08', '2004-08-12', '', 814
exec dbo.RPT_blasting_tickets2 '2004-08-08', '2004-08-15', '', 814
exec dbo.RPT_blasting_tickets2 '2004-08-17', '2004-08-20', '', 5408
exec dbo.RPT_blasting_tickets2 '2004-08-17', '2004-08-20', '424269', 697 
exec dbo.RPT_blasting_tickets2 '2004-08-17', '2004-08-20', '', 5408
exec dbo.RPT_blasting_tickets2 '2004-08-17', '2004-08-20', '', 814
exec dbo.RPT_blasting_tickets2 '2004-08-17', '2004-08-20', '', 5018
exec dbo.RPT_blasting_tickets2 '2001-01-01', '2008-01-01', '2774', 2676
*/

