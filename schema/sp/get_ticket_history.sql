if object_id('dbo.get_ticket_history2') is not null
  drop procedure dbo.get_ticket_history2
GO

CREATE proc get_ticket_history2(@TicketID int)
as
  set nocount on

  declare @Locates table (locate_id int not null)

  insert into @Locates select locate_id
  from locate where ticket_id = @TicketID

  select 'ticket_version' as tname, *
  from ticket_version
  where ticket_id = @TicketID

  select 'assignment' as tname, *
  from assignment
  where locate_id in (select locate_id from @Locates)

  select 'locate_status' as tname, *
  from locate_status
  where locate_id in (select locate_id from @Locates)

  select 'response_log' as tname, *
  from response_log
  where locate_id in (select locate_id from @Locates)

  select 'email_queue_result' as tname, *
  from email_queue_result
  where ticket_id = @TicketID

  declare @BillingDetail table (bill_id int not null, billing_detail_id int not null)

  insert into @BillingDetail
  select b.bill_id, b.billing_detail_id
  from billing_detail b
  join @Locates l on b.locate_id=l.locate_id
  
  select 'billing_detail' as tname, *
  from billing_detail
  where billing_detail_id in (select billing_detail_id from @BillingDetail)

  select 'billing_header' as tname, *
  from billing_header
  where bill_id in (select distinct bill_id from @BillingDetail)

  select 'ticket_ack' as tname, *
  from ticket_ack
  where ticket_id = @TicketID

  select 'billing_invoice' as tname, billing_invoice.*
  from billing_invoice
  where billing_invoice.billing_header_id in (select distinct bill_id from @BillingDetail)

  select 'ticket_info' as tname, ticket_info.*
  from ticket_info
  where ticket_id = @TicketID

  select 'jobsite_arrival' as tname, jobsite_arrival.*
  from jobsite_arrival
  where ticket_id = @TicketID

  select 'locate_facility' as tname, *
  from locate_facility
  where locate_id in (select locate_id from @Locates)

GO

grant execute on get_ticket_history2 to uqweb, QManagerRole
go
/*
exec dbo.get_ticket_history2 3965012
exec dbo.get_ticket_history2 17207760
*/
