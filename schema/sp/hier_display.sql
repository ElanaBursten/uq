if object_id('dbo.hier_display3') is not null
  drop procedure dbo.hier_display3
GO

create proc hier_display3 (@ManagerList varchar(200), @include_neighbors bit)
as

set nocount ON

declare @adding_level int

DECLARE @managers TABLE (man_id integer not null primary key)

/* Note that emp_tree_id and report_to are declared as varchars instead of ints
   in the @results table. This is intentional so that the EmpTree populated
   by this data is correctly loaded on the Work Management screen.
*/
declare @results table (
  emp_tree_id varchar(15) not null primary key,
  emp_id integer not null,
  short_name varchar(30) null,
  emp_number varchar(20) null,
  node_name varchar(50) null,
  type_id integer,
  report_level integer,
  report_to varchar(10), 
  sort integer not null default 9, -- Special nodes added by MOT are sorted higher
  under_me bit,
  is_manager bit,
  work_status_today varchar(6) not null default 'OFF',
  contact_phone varchar(20)
)

insert into @managers
select ID from IdListToTable(@ManagerList)

-- Add the manager(s)
insert into @results (emp_tree_id, emp_id, short_name, emp_number, type_id, report_to, report_level, under_me, contact_phone)
  select emp_id, emp_id, short_name, emp_number, type_id, report_to, 1, 1, contact_phone
  from employee
  inner join @managers man on emp_id = man.man_id

-- Add everyone below the managers
select @adding_level=1
WHILE @adding_level < 10
BEGIN
  INSERT INTO @results
       (emp_tree_id, emp_id, short_name, emp_number, type_id, report_to, report_level, under_me, contact_phone)
    select e.emp_id, e.emp_id, e.short_name, e.emp_number, e.type_id, e.report_to, r.report_level+1, 1, e.contact_phone
    from employee e
     inner join reference et on e.type_id = et.ref_id
     inner join @results r on e.report_to = r.emp_tree_id and r.report_level=@adding_level
    where e.emp_id not in (select emp_tree_id from @results)
      and et.modifier like '%MGR%'

  SELECT @adding_level = @adding_level + 1
END

IF  @include_neighbors=1
BEGIN
  -- Get the bosses
  INSERT INTO @results (emp_tree_id, emp_id, short_name, emp_number, type_id, report_to, report_level, under_me, contact_phone)
    select emp_id, emp_id, short_name, emp_number, type_id, report_to, 0, 0, contact_phone
    from employee
    where emp_id in (SELECT report_to from employee
                           inner join @managers man on emp_id = man.man_id)
     and emp_id not in (select emp_tree_id from @results)

  -- Get active siblings
  INSERT INTO @results (emp_tree_id, emp_id, short_name, emp_number, type_id, report_to, report_level, under_me, contact_phone)
    select emp_id, emp_id, short_name, emp_number, type_id, report_to, 1,
     (case when short_name like '%unass%' then 1 else 0 end ) as underme, contact_phone
    from employee
    where report_to in (SELECT report_to from employee
                              inner join @managers man on emp_id = man.man_id)
     and emp_id not in (select emp_tree_id from @results)
     and active=1
END


IF (select count(*) from @managers) > 1
BEGIN
  -- populate upwards
  WHILE 1=1
  BEGIN
    INSERT INTO @results (emp_tree_id, emp_id, short_name, emp_number, type_id, report_to, report_level, under_me, contact_phone)
      select e.emp_id, e.emp_id, e.short_name, e.emp_number, e.type_id, e.report_to, 1, 0, e.contact_phone
      from employee e
      where e.emp_id not in (select emp_tree_id from @results)
       and e.emp_id in (select r.report_to from @results r)

    IF @@ROWCOUNT=0
      BREAK
  END

  update @results set report_level = 0 where report_to is null

  select @adding_level=1
  WHILE @adding_level < 10
  BEGIN
    update @results set report_level = @adding_level
      where report_to in
        (select emp_tree_id from @results r2 where r2.report_level = @adding_level-1)

  SELECT @adding_level = @adding_level + 1
END

END

update @results
  set is_manager = case when r.modifier like '%MGR%' then 1 else 0 end,
    node_name = coalesce(emp_number + ': ', '') + short_name
  from reference r where r.ref_id = type_id

update @results set work_status_today = dbo.get_current_work_status(emp_id)

select 'emp_workload' as tname, *
from @results emp_workload order by report_level, convert(int, report_to), short_name

GO
grant execute on hier_display3 to uqweb, QManagerRole

/*
hier_display3 '2676', 0
hier_display3 '2678', 1  // should show peers

hier_display3 '2540', 0
hier_display3 '2540', 1

hier_display3 '5688', 0
hier_display3 '5688', 1

hier_display3 '2540,5688', 0
hier_display3 '2540,5688', 1

hier_display3 '2167,694', 1
hier_display3 '2167,4422', 1

hier_display3 '7128,3906', 1
hier_display3 '7128,6027', 0
hier_display3 '7128,6027', 1

hier_display3 '2139', 1

select * from reference where type='emptype'

select * from users where emp_id=2678
select * from users where emp_id=(select emp_id from employee where emp_number='99023')

select * from right_definition where right_id=24
select * from employee_right where right_id=24

select * from employee_right where right_id=24 and emp_id=2181
update employee_right set limitation = '2167,4422' where right_id=24 and emp_id=2181
select * from users where emp_id=2181
90162 EMILY

select short_name from employee where emp_id=7128
Jim Wall
select short_name from employee where emp_id=6027
110 Manager


*/

