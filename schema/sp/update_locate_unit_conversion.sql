if object_id('dbo.update_locate_unit_conversion') is not null
  drop procedure dbo.update_locate_unit_conversion
go

create procedure update_locate_unit_conversion (
  @ClientID int,
  @EffectiveDate datetime
)
as

declare @Temp table (
  locate_hours_id int,
  unit_conversion_id int
)

/* find all worked, unbilled locate_hours for the client */
insert into @Temp (locate_hours_id, unit_conversion_id)
  select lh.locate_hours_id, c.unit_conversion_id
  from locate_hours lh
  inner join locate l on (l.locate_id = lh.locate_id)
  inner join client c on (c.client_id = l.client_id)
  where l.client_id = @ClientID and l.closed_date >= @EffectiveDate
  and l.invoiced = 0 and l.active = 1 and l.closed = 1
  and lh.units_marked <> 0
  and IsNull(c.unit_conversion_id, -1) <> IsNull(lh.unit_conversion_id, -1)

/* Reset unit conversion id on the locate_hours to the client's default conversion id */
update locate_hours
  set locate_hours.unit_conversion_id = t.unit_conversion_id from @Temp t where locate_hours.locate_hours_id = t.locate_hours_id

go

grant execute on update_locate_unit_conversion to uqweb, QManagerRole
go

/*
exec dbo.update_locate_unit_conversion 1480, '5/1/2005 00:00:00'
*/
