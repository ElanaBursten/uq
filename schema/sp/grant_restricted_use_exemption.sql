if object_id('dbo.grant_restricted_use_exemption') is not null
  drop procedure dbo.grant_restricted_use_exemption
go

create proc grant_restricted_use_exemption (
  @EmployeeID int,
  @GrantedBy int,
  @EffectiveDate datetime = null,
  @GrantExemption bit = 1 -- Set to 0 to revoke
)
as
  set nocount on
  declare @ExemptionLengthHours int
  set @ExemptionLengthHours = 48

  if @EffectiveDate is null
    set @EffectiveDate = GetDate()

  -- TODO: Extra security check that emp_id <> granted_by ?

  update restricted_use_exemption set
    revoked_date = @EffectiveDate,
    revoked_by = @GrantedBy
  where emp_id = @EmployeeID
  and revoked_date is null
  and expire_date > GetDate()

  if @GrantExemption = 1
    insert into restricted_use_exemption (emp_id, effective_date, expire_date, granted_by)
      values (@EmployeeID,
        @EffectiveDate,
        DateAdd(Hour, @ExemptionLengthHours, @EffectiveDate),
        @GrantedBy)
go

grant execute on grant_restricted_use_exemption to uqweb, QManagerRole

/*
execute dbo.grant_restricted_use_exemption 3510, 5654
execute dbo.grant_restricted_use_exemption 3510, 5654, '2007-05-05T18:00:00.000'
execute dbo.grant_restricted_use_exemption 3510, 5654, '2007-05-05T19:00:00.000', 0
*/
