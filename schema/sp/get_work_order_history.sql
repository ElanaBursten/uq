if object_id('dbo.get_work_order_history2') is not null
  drop procedure dbo.get_work_order_history2
GO

CREATE proc get_work_order_history2(@WorkOrderID int)
as
  set nocount on

  select 'wo_assignment' as tname, * 
  from wo_assignment 
  where wo_id = @WorkOrderID

  select 'wo_status_history' as tname, * 
  from wo_status_history
  where wo_id = @WorkOrderID
  
  select 'wo_response_log' as tname, * 
  from wo_response_log
  where wo_id = @WorkOrderID
  
GO

grant execute on get_work_order_history2 to uqweb, QManagerRole
go

/*
exec dbo.get_work_order_history2 110050
exec dbo.get_work_order_history2 110065
*/
