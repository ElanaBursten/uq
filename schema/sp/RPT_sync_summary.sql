if object_id('dbo.RPT_sync_summary_2') is not null
	drop procedure dbo.RPT_sync_summary_2
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create procedure RPT_sync_summary_2 (
  @DateStart datetime,
  @DateEnd   datetime,
  @ManagerID int,
  @EmployeeStatus int
)
as
  set nocount on

  declare @emps table (
    emp_id int not null primary key,
    emp_number varchar(25) null,
    last_name varchar(30) null,
    short_name varchar(30) null,
    h_report_level integer,
    h_namepath varchar(450) NULL,
    h_lastpath varchar(450) NULL,
    h_numpath varchar(450) NULL,
    h_idpath varchar(160) NULL
  )

  declare @sync_log table (
    emp_id int,
    FirstSync datetime,
    LastSync datetime,
    LastLocalSync datetime,
    n_client_version decimal(14, 0),
    NumSyncs int,
    client_version varchar(30),
    multi_pc_user bit,
    n_addin_version decimal(14, 0),
    addin_version varchar(30),
    n_persequor_version decimal(14, 0),
    persequor_version varchar(30),
    primary key(emp_id)
  )
 
  declare @versions table (
    version_type char(1) not null, -- C for client, A for addin, P for persequor
    version_num varchar(30) not null,
    primary key (version_type, version_num)
  )

  -- Collect the emps
  insert into @emps
    select h_emp_id, h_emp_number, h_last_name, h_short_name, h_report_level,
      h_namepath, h_lastpath, h_numpath, h_idpath
    from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus) 

  -- client versions
  insert into @versions 
    select distinct 'C', client_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and client_version is not null

  -- addin versions
  insert into @versions 
    select distinct 'A', addin_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and addin_version is not null

  -- persequor versions
  insert into @versions 
    select distinct 'P', persequor_version
    from sync_log
    where sync_date between @DateStart and @DateEnd
    and persequor_version is not null

  -- Sync Log
  insert into @sync_log
    select
      sync_log.emp_id,
      min(sync_date) as FirstSync,
      max(sync_date) as LastSync,
      max(local_date) as LastLocalSync,
      max(dbo.version_to_number(client_version)),
      count(sync_id) as NumSyncs,
      '', 
      0,
      max(dbo.version_to_number(addin_version)) as n_addin_version,
      '' as addin_version,
      max(dbo.version_to_number(persequor_version)) as n_persequor_version,
      '' as persequor_version
    from sync_log
    inner join @emps E2 on E2.emp_id = sync_log.emp_id
    where sync_date between @DateStart and @DateEnd
    group by sync_log.emp_id

  update @sync_log
    set client_version = (select v1.version_num from @versions v1
                          where v1.version_type = 'C' 
                            and dbo.version_to_number(v1.version_num) = n_client_version),
        addin_version = (select v2.version_num from @versions v2
                          where v2.version_type = 'A'
                            and dbo.version_to_number(v2.version_num) = n_addin_version),
        persequor_version = (select v3.version_num from @versions v3
                          where v3.version_type = 'P'
                            and dbo.version_to_number(v3.version_num) = n_persequor_version)

  update @sync_log 
    set multi_pc_user = 1
    where exists (select c.emp_id from computer_info c 
      where c.emp_id = [@sync_log].emp_id 
        and c.insert_date between @DateStart and @DateEnd
      group by c.emp_id having count(*) > 1)

  select
    E1.emp_id,
    tot.FirstSync,
    tot.LastSync,
    tot.LastLocalSync,
    isnull(tot.NumSyncs, 0) as NumSyncs,
    E1.short_name,
    E1.emp_number,
    E1.last_name,
    tot.client_version,
    E1.h_report_level,
    E1.h_namepath,
    E1.h_lastpath,
    E1.h_numpath,
    E1.h_idpath,
    tot.multi_pc_user,
    tot.addin_version,
    tot.persequor_version
  from @emps E1
  left join @sync_log as tot on tot.emp_id = E1.emp_id
  order by E1.short_name
go
grant execute on RPT_sync_summary_2 to uqweb, QManagerRole

/*
exec RPT_sync_summary_2 '2003-11-01', '2003-11-03', 212, 1
exec RPT_sync_summary_2 '2004-03-12', '2004-03-13', 2155, 1
exec RPT_sync_summary_2 '2006-01-01', '2006-12-31', 2676, 1
exec RPT_sync_summary_2 '2006-01-01', '2006-12-31', 6810, 1
exec RPT_sync_summary_2 '2008-12-01', '2009-01-01', 811, 1

exec RPT_sync_summary_2 @DateStart='2009-10-05', @DateEnd='2009-10-12', @ManagerID=13775, @EmployeeStatus=1

select * from sync_log where emp_id = 3510

*/
