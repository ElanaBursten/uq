if exists (select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'TicketNeedsPrinting')
  drop function TicketNeedsPrinting
go

create function TicketNeedsPrinting(@ticket_id int, @locator_id int, @modified_date datetime)
returns bit
as
begin
  declare @Result bit
  declare @max_print_date datetime

  select
    @max_print_date = max(ticket_batch.print_date)
  from ticket_printing
    inner join ticket_batch
      on ticket_batch.ticket_batch_id = ticket_printing.ticket_batch_id
  where
    ticket_printing.ticket_id = @ticket_id and
    ticket_printing.locator_id = @locator_id and
    -- ignore records where the batch failed
    ticket_batch.batch_printed = 1

  if (@max_print_date is null) or (@modified_date > @max_print_date)
    set @Result = 1
  else
    set @Result = 0

  return @Result
end
go
/*
exec dbo.TicketNeedsPrinting
*/
