if object_id('dbo.RPT_damage_approval_details') is not null
  drop procedure dbo.RPT_damage_approval_details
GO

create proc RPT_damage_approval_details (
	@damage_id int
)
as

set nocount on

--Return Approval Details
select d.damage_id, d.uq_damage_id, d.exc_resp_other_desc, d.exc_resp_details, d.exc_resp_response, d.uq_resp_details, 
d.uq_resp_ess_step, d.uq_resp_other_desc, d.spc_resp_details, 
case when d.estimate_agreed = 1 then 'Yes' else 'No' end as estimate_agreed,   
case when d.damage_type = 'APPROVED' then 'Yes' else 'No' end as approved,
d.approved_datetime, e1.short_name as 'approved_by_name', r1.description as 'exres', 
r2.description as 'uqres', r3.description as 'uqess', 
r4.description as 'utres'
from damage d 
  left join employee e1 on (d.approved_by_id = e1.emp_id) 
  left join reference r1 on (d.exc_resp_code = r1.code and r1.[type] = 'exres') 
  left join reference r2 on (d.uq_resp_code = r2.code and r2.[type] = 'uqres') 
  left join reference r3 on (d.uq_resp_ess_step = r3.code and r3.[type] = 'locess') 
  left join reference r4 on (d.spc_resp_code = r4.code and r4.[type] = 'utres') 
where d.damage_id = @damage_id

--Return estimate info
select modified_date, amount 
  from damage_estimate 
  where damage_id = @damage_id 
  order by modified_date desc
  
--Return required missing docs  
select r.name, d.comment
  from document d join required_document r on d.required_doc_id = r.required_doc_id 
  where d.foreign_type = 2 
    and d.foreign_id = @damage_id 
    and d.active = 1
    
GO

grant execute on RPT_damage_approval_details to uqweb, QManagerRole

/*
exec dbo.RPT_damage_approval_details 30001
exec dbo.RPT_damage_approval_details 30012
*/
