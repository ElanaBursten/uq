if object_id('dbo.RPT_get_damage_details') is not null
  drop procedure dbo.RPT_get_damage_details
GO

create proc RPT_get_damage_details (
	@damage_id int,
	@DamageAttachmentList VarChar(2000),
	@TicketAttachmentList VarChar(2000)
)
as

declare @TicketResults table (
  ticket_idz         integer     not null,
  ticket_number      varchar(25) null,
  transmit_date      datetime    null,
  due_date           datetime    null,
  locator_id         integer     null,
  locator_name       varchar(35) null,
  completed_date     datetime    null,
  image              text        null,
  image_format       text        null)

set nocount on

declare @Locatedata table (
    [ticket_id] int,
    [ld_locate_id] int,
    [client_code] varchar(10),
    [status] varchar(5),
    [status_name] varchar(30),
    [short_name] varchar(30),
    [high_profile] bit,
    [qty_marked] int,
    [closed] bit,
    [closed_date] datetime,
    [worked_date] datetime,
    [plat_list] varchar(400)
  )

-- return damage data
select damage.*, ticket.ticket_number,
  Coalesce(employee.short_name, '') as investigator_name,
  Coalesce(pc_name, profit_center) as 'profit_center_name',
  r1.description as 'excavator_type_desc',
  r2.description as 'excavation_type_desc',
  Coalesce(r3.description, damage.size_type, '') as 'facility_size_desc',
  r4.description as 'tracer_wire_intact_desc',
  r5.description as 'marked_in_white_desc',
  r6.description as 'site_pot_holed_desc',
  r7.description as 'marks_within_tolerance_desc',
  Coalesce(locator.short_name, '') as locator_name,
  Coalesce(approver.short_name, '') as approver_name,
  case when damage.damage_type = 'APPROVED' then 'Yes' else 'No' end as approved,
  damage.approved_datetime,
  r8.description as 'exc_resp_desc',
  r9.description as 'exc_resp_type_desc',
  r10.description as 'uq_resp_desc',
  r11.description as 'uq_resp_type_desc',
  r12.description as 'spc_resp_desc',
  r13.description as 'spc_resp_type_desc'
from damage
  left join profit_center on profit_center.pc_code = damage.profit_center
  left join ticket on ticket.ticket_id = damage.ticket_id
  left join employee on employee.emp_id = damage.investigator_id
  left join reference r1 on (r1.type='excrtype' and r1.code=damage.excavator_type)
  left join reference r2 on (r2.type='excntype' and r2.code=damage.excavation_type)
  left join reference r3 on (r3.type='facsize' and r3.code=damage.facility_size and r3.modifier=damage.facility_type)
  left join reference r4 on (r4.type='yesnoblank' and r4.code=damage.site_tracer_wire_intact)
  left join reference r5 on (r5.type='yesnoblank' and r5.code=damage.site_marked_in_white)
  left join reference r6 on (r6.type='ynub' and r6.code=damage.site_pot_holed)
  left join reference r7 on (r7.type='yesnoblank' and r7.code=damage.marks_within_tolerance)
  left join reference r8 on (r8.type='exres' and r8.code=damage.exc_resp_code)
  left join reference r9 on (r9.type in ('ex1resp','ex2resp','ex7resp') and r9.code=damage.exc_resp_type)
  left join reference r10 on (r10.type='uqres' and r10.code=damage.uq_resp_code)
  left join reference r11 on (r11.type='uqresp' and r11.code=damage.uq_resp_type)
  left join reference r12 on (r12.type='utres' and r12.code=damage.spc_resp_code)
  left join reference r13 on (r13.type='spresp' and r13.code=damage.spc_resp_type)
  left join employee locator on (locator.emp_id = dbo.get_locator_for_damage(damage_id))
  left join employee approver on (approver.emp_id = damage.approved_by_id)
where damage.damage_id = @damage_id

-- return ticket data
insert into @TicketResults (ticket_idz, ticket_number, transmit_date, due_date, image, image_format)
  select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image,
    call_center.xml_ticket_format
  from ticket
  left join call_center on cc_code = ticket_format
  where ticket.ticket_id =
        (select ticket_id from damage where damage_id=@damage_id)

-- Set the locator ID
update @TicketResults
  set locator_id=a.locator_id
  from locate l, assignment a
  where l.ticket_id = ticket_idz
  and l.locate_id = a.locate_id
  and a.active = 1
  and l.status <> '-N'

-- Set the locator name
update @TicketResults
  set locator_name=short_name
  from employee where emp_id=locator_id

-- Set the completed date
update @TicketResults
  set completed_date = (select min(l.closed_date)
  from locate l, assignment a
  where l.ticket_id = ticket_idz
  and l.locate_id = a.locate_id
  and a.active = 1
  and l.closed = 1)

select ticket_idz as ticket_id, ticket_number, transmit_date, due_date,
  locator_id, locator_name, completed_date, image, image_format
 from @TicketResults

-- Locate data
insert into @Locatedata
select
  locate.ticket_id,
  locate.locate_id,
  locate.client_code,
  locate.status,
  statuslist.status_name,
  employee.short_name,
  locate.high_profile,
  locate.qty_marked,
  locate.closed,
  locate.closed_date,
  locate.closed_date,
  dbo.get_plat_list(locate.locate_id) as plat_list
from ticket
  left join locate
    on (locate.ticket_id = ticket.ticket_id)
  left join statuslist
    on statuslist.status = locate.status
  left join assignment
    on assignment.locate_id = locate.locate_id
  left join employee
    on employee.emp_id = assignment.locator_id
where ticket.ticket_id =
        (select ticket_id from damage where damage_id=@damage_id)
 and assignment.active = 1

--Update worked_date to use the most recent between locate.closed_date and the most recent
--locate_hours record for that locate

update @Locatedata
  set worked_date = (
    select max(locate_hours.work_date)
    from locate_hours
    where ld_locate_id = locate_hours.locate_id)


update @Locatedata
set worked_date = closed_date
where ((closed_date > worked_date) or (worked_date is null))
  and (closed_date is not null)

select * from @Locatedata order by client_code

-- Ticket notes data
select tn.entry_date, tn.modified_date, tn.note note, tn.ticket_id
from damage
  inner join ticket_notes tn on tn.ticket_id = damage.ticket_id
where damage.damage_id = @damage_id
order by tn.entry_date

-- damage attachments

if @DamageAttachmentList <> ''
  select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.extension,
    a.orig_filename as display_name
  from attachment a
       inner join employee e on e.emp_id = a.attached_by
       inner join dbo.IdListToTable(@DamageAttachmentList) l on a.attachment_id = l.Id
  where
    a.active = 1 and
    a.foreign_id = @damage_id and
    a.foreign_type = 2 -- damage foreign type
  order by
    a.attach_date, a.orig_filename
else
  select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.extension,
    a.orig_filename as display_name
  from attachment a
       inner join employee e on e.emp_id = a.attached_by
  where
    a.active = 1 and
    a.foreign_id = @damage_id and
    a.foreign_type = 2 -- damage foreign type
  order by
    a.attach_date, a.orig_filename

-- ticket attachments

if @TicketAttachmentList <> ''
  select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.foreign_id, a.extension,
    a.orig_filename as display_name
  from attachment a
       inner join employee e on e.emp_id = a.attached_by
       inner join dbo.IdListToTable(@TicketAttachmentList) l on a.attachment_id = l.Id
  where
    a.active = 1 and
    a.foreign_id in (select ticket_idz from @TicketResults) and
    a.foreign_type = 1 -- ticket foreign type
  order by
    a.attach_date, a.orig_filename
else
  select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.foreign_id, a.extension,
    a.orig_filename as dislay_name
  from attachment a
       inner join employee e on e.emp_id = a.attached_by
  where
    a.active = 1 and
    a.foreign_id in (select ticket_idz from @TicketResults) and
    a.foreign_type = 1 -- ticket foreign type
  order by
    a.attach_date, a.orig_filename

-- Damage third party data
select damage_third_party.*,c.name as 'carrier_name_third_party'
  from damage_third_party
  left join carrier c on c.carrier_id = damage_third_party.carrier_id
  where damage_id = @damage_id

-- Damage litigation data
select damage_litigation.*,c.name as 'carrier_name_litigation', r.description as 'attorney_name_description'
  from damage_litigation
  left join carrier c on c.carrier_id = damage_litigation.carrier_id
  left join reference r on (r.type='atty' and (r.code=damage_litigation.attorney or r.description like damage_litigation.attorney+'%'))
  where damage_id = @damage_id

-- Damage notes data
select notes.*, @damage_id as damage_id
  from notes
  where (foreign_id = @damage_id and foreign_type = 2)
  or (foreign_id in (select third_party_id from damage_third_party where damage_id = @damage_id)
  and foreign_type = 3)
  or (foreign_id in (select litigation_id from damage_litigation where damage_id = @damage_id)
  and foreign_type = 4)
order by notes.entry_date

-- Damage history data
select damage_history.*, r.description as 'uq_resp_ess_step_description', r2.description as 'reason_changed_description'
  from damage_history
  left join reference r on (r.type='locess' and r.code=damage_history.uq_resp_ess_step)
  left join reference r2 on (r2.type='dmgreason' and r2.ref_id=damage_history.reason_changed)
  where damage_id = @damage_id
order by modified_date

-- Ticket Facilities
select client_code, coalesce(ft.description, facility_type, '-') as facility_type,
  coalesce(fm.description, facility_material, '-') as facility_material,
  coalesce(fs.description, facility_size, '-') as facility_size,
  coalesce(fp.description, facility_pressure, '-') as facility_pressure,
  offset,
  employee.short_name as added_by_name
from @LocateData
  left join locate_facility on (ld_locate_id = locate_id)
  left join employee on (locate_facility.added_by = employee.emp_id)
  left join reference ft on (ft.type = 'factype' and ft.code = facility_type)
  left join reference fm on (fm.type = 'facmtrl' and fm.code = facility_material and fm.modifier = facility_type)
  left join reference fs on (fs.type = 'facsize' and fs.code = facility_size and fm.modifier = facility_type)
  left join reference fp on (fp.type = 'facpres' and fp.code = facility_pressure and fm.modifier = facility_type)
where locate_facility.active = 1
order by client_code, locate_facility_id

-- Duplicate record error details
select type, code, modifier, count(*) dupe_code_cnt
from reference
where type in ('excrtype','excntype','facsize','yesnoblank','ynub',
  'exres', 'uqres', 'uqresp', 'utres', 'ex1resp', 'ex2resp', 'ex7resp')
group by type, code, modifier
having count(*) > 1

GO

grant execute on RPT_get_damage_details to uqweb, QManagerRole

/*
exec dbo.RPT_get_damage_details 30000, '', ''
exec dbo.RPT_get_damage_details 50860, '', ''
exec dbo.RPT_get_damage_details 68753, '', ''
exec dbo.RPT_get_damage_details 62129, '', ''

-- SQL to add a note to force rolling to another page:
insert into notes (foreign_type, foreign_id, active, uid, note)
 values (1, 5442685, 1, 100, '2 this is a test note, it shoudl be long enough to scroll so we can see what happens when we go past one page!')
*/
