if object_id('dbo.close_all_locates_for_locator') is not null
  drop procedure dbo.close_all_locates_for_locator
GO

create procedure close_all_locates_for_locator (
	@LocatorID int,
	@Status varchar(20),
	@Billed bit
)
as
set nocount on

declare @ltoc table (
	locate_id int not null primary key
)

insert into @ltoc
select distinct locate.locate_id
 from assignment
 inner join locate on locate.locate_id=assignment.locate_id
 where locator_id=@LocatorID
  and locate.closed=0 
  and assignment.active=1

select count(*) as LocatesToProcess from @ltoc

declare @closed_date_now datetime
select @closed_date_now = getdate()

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
       qty_marked=1,
       invoiced=@Billed
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END

go
-- Don't grant this!:
-- grant execute on close_all_locates_for_locator to uqweb, QManagerRole
/*
exec dbo.close_all_locates_for_locator 6005, 'NC', 1



*/
