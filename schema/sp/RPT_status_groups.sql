if object_id('dbo.RPT_status_groups') is not null
	drop procedure dbo.RPT_status_groups
go

create proc RPT_status_groups
as
  set nocount on

  select sg_name, sg.sg_id, sgi.status, sl.status_name from status_group sg
   inner join status_group_item sgi on sg.sg_id=sgi.sg_id
   inner join statuslist sl on sgi.status=sl.status
    where sgi.active=1
  order by sg_name

  select call_center, client_name, oc_code,
   coalesce(sg.sg_name, '--All Statuses--') as sg_name
   from client
    left join status_group sg
       on client.status_group_id=sg.sg_id
   order by call_center, client_name, oc_code

  select call_center, client_name, oc_code,
   coalesce(sg.sg_name, '--All Statuses--') as sg_name
   from client
    left join status_group sg
       on client.status_group_id=sg.sg_id
   order by 4, 1, 2, 3


go
grant execute on RPT_status_groups to uqweb, QManagerRole

/*
exec dbo.RPT_status_groups

select * from statuslist
*/
