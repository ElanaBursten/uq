if object_id('dbo.tickets_to_route_noxml') is not null
	drop procedure dbo.tickets_to_route_noxml
go

create procedure tickets_to_route_noxml
as
begin
  select top 2000 with ties
    ticket.ticket_id,
    ticket.work_lat,
    ticket.work_long,
    ticket.call_date,
    ticket.ticket_number,
    ticket.ticket_format,
    ticket.ticket_type,
    ticket.respond_date,
    ticket.kind,
    ticket.work_state,
    ticket.work_county,
    ticket.work_city,
    ticket.work_date,
    ticket.company,
    ticket.con_name,
    ticket.map_page,
    ticket.transmit_date,
    ticket.due_date,
    ticket.work_type,
    ticket.work_address_street,
    ticket.explosives,
    ticket.ward,
    locate.locate_id,
    locate.client_code,
    locate.client_id,
    locate.status
  from ticket (NOLOCK)
    inner join locate (NOLOCK)
      on locate.ticket_id = ticket.ticket_id
  where
    locate.closed=0 AND locate.status = '-P'
  order by
    ticket.kind, ticket.ticket_id
end
go
grant execute on tickets_to_route_noxml to uqweb, QManagerRole

/*
exec dbo.tickets_to_route_noxml

select count(*) from locate where status='-P'
select count(*) from locate where status='-P' and closed=0
*/


