/****** Object:  UserDefinedFunction [dbo].[employee_payroll_data_Ulti]    Script Date: 11/21/2018 9:31:28 AM ******/
-- processes the employee data for payroll export to Ulti.  Sets the report to for employees.

if object_id('employee_payroll_data_Ulti') is not null
	drop FUNCTION employee_payroll_data_Ulti
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create function [dbo].[employee_payroll_data_Ulti] (@limit_to_pc_code varchar(15))
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  x integer,
  emp_pc_code varchar(15) NULL,
  Emp_Number varchar(16) NULL,
  Man_Emp_Number varchar(16) NULL,
  EarningsType varchar(8) NULL  --change later to 8 when data corrected
)
as
begin
  declare @wave int

  INSERT INTO @results (emp_id, x, emp_pc_code, Emp_Number, Man_Emp_Number, EarningsType)
  select e.emp_id, e.report_to, e.repr_pc_code, e.emp_number, e2.emp_number,  Left(r.code,8)
    from employee e
	join reference r on r.ref_id = e.type_id
	left join employee e2 on (e2.emp_id = e.report_to) -- managers

  update @results set x=null where emp_pc_code is not null

  select @wave = 1             -- walks up the heirarchy tree to the first successful match
  WHILE @wave < 15
  BEGIN
    update @results
      set emp_pc_code = (select repr_pc_code from employee e2 where e2.emp_id=x)
    where
       emp_pc_code is null and x is not null

    update @results
      set x = (select report_to from employee e3 where e3.emp_id=x)
    where
       emp_pc_code is null and x is not null

    SELECT @wave = @wave + 1
  END


  declare @overrides table (
    ov_emp_id int not null primary key,
    ov_code varchar(15) not null
  )

  insert into @overrides
  select emp_id, payroll_pc_code
   from employee
   where payroll_pc_code is not null

  update @results
   set emp_pc_code = ov_code,
       x = (select min(emp_id) from employee where repr_pc_code=ov_code)
   from @overrides o
    where emp_id=ov_emp_id

  if @limit_to_pc_code is not null
    delete from @results
      where emp_pc_code <> @limit_to_pc_code or emp_pc_code is null
  return
end


