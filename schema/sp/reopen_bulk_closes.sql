if object_id('dbo.reopen_bulk_closes') is not null
	drop procedure reopen_bulk_closes
GO

create procedure reopen_bulk_closes (
	@Since datetime
)
as
set nocount on

declare @ltoc table (
	locate_id int not null primary key
)

insert into @ltoc
select locate_id from locate
where closed_date>@Since
 and closed_how='Bulk'
 and closed=1

select count(*) as LocatesToProcess from @ltoc

-- debugging output:
-- select * from @ltoc

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status='-R',
       closed=0,
       closed_how='BulkReopen',
       qty_marked=0,
       invoiced=0
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END

go
/*
exec dbo.reopen_bulk_closes '2004-07-05'
*/
