if object_id('dbo.client_basic_activity') is not null
	drop procedure dbo.client_basic_activity
GO

CREATE Procedure client_basic_activity
	(
	@d datetime,
	@client varchar(20),
	@CallCenter varchar(30)
	)
As
set nocount on
select
  convert(datetime, convert(varchar(12), locate.closed_date, 102) , 102) as closed_date_only,
  locate.closed_date, locate.client_code, locate.status,
  locate.modified_date,
  ticket.ticket_number,
  ticket.work_address_number, ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state, ticket.work_county, ticket.work_city, ticket.map_page
from ticket
  inner join locate
    on ticket.ticket_id=locate.ticket_id
where locate.modified_date between @d and dateadd(d, 1, @d)
  and locate.closed=1
  and locate.status <>'-N'
  and ticket.active=1
  and locate.active=1
  and locate.client_code=@client
  and ticket.ticket_format=@CallCenter
order by locate.closed_date

go
grant execute on client_basic_activity to uqweb, QManagerRole
/*
exec dbo.client_basic_activity '2003-05-19', 'ATL01', 'Atlanta'
*/
