if object_id('dbo.RPT_billing_rate') is not null
	drop procedure dbo.RPT_billing_rate
GO

CREATE PROCEDURE RPT_billing_rate (@CallCenter varchar(20))
AS

select br.*, c.* from billing_rate br
inner join client c on c.call_center = br.call_center 
                    and c.oc_code = br.billing_cc
where (br.call_center = @CallCenter or @CallCenter = '*')
order by br.call_center, client_name, status, parties
GO

grant execute on RPT_billing_rate to uqweb, QManagerRole

/*
exec RPT_billing_rate 'Atlanta'
exec RPT_billing_rate '*'
*/
