if object_id('dbo.get_current_work_status') is not null
  drop function dbo.get_current_work_status
GO

create function get_current_work_status (@emp_id int) 
returns varchar(10)
as 
begin

-- Determine who is working today and whether they are currently on the clock
declare @today datetime = Convert(date, dbo.get_emp_local_time(@emp_id, GetDate()))
declare @tomorrow datetime = DateAdd(dd, 1, @today)
declare @nodate datetime = '1899-12-30T00:00:00.000'

return coalesce((select case
  when ((coalesce(work_start1, @nodate) <> @nodate) and (coalesce(work_stop1, @nodate) = @nodate)) or
       ((coalesce(work_start2, @nodate) <> @nodate) and (coalesce(work_stop2, @nodate) = @nodate)) or
       ((coalesce(work_start3, @nodate) <> @nodate) and (coalesce(work_stop3, @nodate) = @nodate)) or
       ((coalesce(work_start4, @nodate) <> @nodate) and (coalesce(work_stop4, @nodate) = @nodate)) or
       ((coalesce(work_start5, @nodate) <> @nodate) and (coalesce(work_stop5, @nodate) = @nodate)) or
       ((coalesce(work_start6, @nodate) <> @nodate) and (coalesce(work_stop6, @nodate) = @nodate)) or
       ((coalesce(work_start7, @nodate) <> @nodate) and (coalesce(work_stop7, @nodate) = @nodate)) or
       ((coalesce(work_start8, @nodate) <> @nodate) and (coalesce(work_stop8, @nodate) = @nodate)) or
       ((coalesce(work_start9, @nodate) <> @nodate) and (coalesce(work_stop9, @nodate) = @nodate)) or
       ((coalesce(work_start10, @nodate) <> @nodate) and (coalesce(work_stop10, @nodate) = @nodate)) or
       ((coalesce(work_start11, @nodate) <> @nodate) and (coalesce(work_stop11, @nodate) = @nodate)) or
       ((coalesce(work_start12, @nodate) <> @nodate) and (coalesce(work_stop12, @nodate) = @nodate)) or
       ((coalesce(work_start13, @nodate) <> @nodate) and (coalesce(work_stop13, @nodate) = @nodate)) or
       ((coalesce(work_start14, @nodate) <> @nodate) and (coalesce(work_stop14, @nodate) = @nodate)) or
       ((coalesce(work_start15, @nodate) <> @nodate) and (coalesce(work_stop15, @nodate) = @nodate)) or
       ((coalesce(work_start16, @nodate) <> @nodate) and (coalesce(work_stop16, @nodate) = @nodate)) then 'IN'
  when ((coalesce(callout_start1, @nodate) <> @nodate) and (coalesce(callout_stop1, @nodate) = @nodate)) or
       ((coalesce(callout_start2, @nodate) <> @nodate) and (coalesce(callout_stop2, @nodate) = @nodate)) or
       ((coalesce(callout_start3, @nodate) <> @nodate) and (coalesce(callout_stop3, @nodate) = @nodate)) or
       ((coalesce(callout_start4, @nodate) <> @nodate) and (coalesce(callout_stop4, @nodate) = @nodate)) or
       ((coalesce(callout_start5, @nodate) <> @nodate) and (coalesce(callout_stop5, @nodate) = @nodate)) or
       ((coalesce(callout_start6, @nodate) <> @nodate) and (coalesce(callout_stop6, @nodate) = @nodate)) or
       ((coalesce(callout_start7, @nodate) <> @nodate) and (coalesce(callout_stop7, @nodate) = @nodate)) or
       ((coalesce(callout_start8, @nodate) <> @nodate) and (coalesce(callout_stop8, @nodate) = @nodate)) or
       ((coalesce(callout_start9, @nodate) <> @nodate) and (coalesce(callout_stop9, @nodate) = @nodate)) or
       ((coalesce(callout_start10, @nodate) <> @nodate) and (coalesce(callout_stop10, @nodate) = @nodate)) or
       ((coalesce(callout_start11, @nodate) <> @nodate) and (coalesce(callout_stop11, @nodate) = @nodate)) or
       ((coalesce(callout_start12, @nodate) <> @nodate) and (coalesce(callout_stop12, @nodate) = @nodate)) or
       ((coalesce(callout_start13, @nodate) <> @nodate) and (coalesce(callout_stop13, @nodate) = @nodate)) or
       ((coalesce(callout_start14, @nodate) <> @nodate) and (coalesce(callout_stop14, @nodate) = @nodate)) or
       ((coalesce(callout_start15, @nodate) <> @nodate) and (coalesce(callout_stop15, @nodate) = @nodate)) or
       ((coalesce(callout_start16, @nodate) <> @nodate) and (coalesce(callout_stop16, @nodate) = @nodate)) then 'CALLIN'
       -- TODO: QMAN-3605 See if the employee is on call and return an ONCALL status
  when ((coalesce(work_start1, @nodate) <> @nodate) and (coalesce(work_stop1, @nodate) <> @nodate)) or
       ((coalesce(work_start2, @nodate) <> @nodate) and (coalesce(work_stop2, @nodate) <> @nodate)) or
       ((coalesce(work_start3, @nodate) <> @nodate) and (coalesce(work_stop3, @nodate) <> @nodate)) or
       ((coalesce(work_start4, @nodate) <> @nodate) and (coalesce(work_stop4, @nodate) <> @nodate)) or
       ((coalesce(work_start5, @nodate) <> @nodate) and (coalesce(work_stop5, @nodate) <> @nodate)) or
       ((coalesce(work_start6, @nodate) <> @nodate) and (coalesce(work_stop6, @nodate) <> @nodate)) or
       ((coalesce(work_start7, @nodate) <> @nodate) and (coalesce(work_stop7, @nodate) <> @nodate)) or
       ((coalesce(work_start8, @nodate) <> @nodate) and (coalesce(work_stop8, @nodate) <> @nodate)) or
       ((coalesce(work_start9, @nodate) <> @nodate) and (coalesce(work_stop9, @nodate) <> @nodate)) or
       ((coalesce(work_start10, @nodate) <> @nodate) and (coalesce(work_stop10, @nodate) <> @nodate)) or
       ((coalesce(work_start11, @nodate) <> @nodate) and (coalesce(work_stop11, @nodate) <> @nodate)) or
       ((coalesce(work_start12, @nodate) <> @nodate) and (coalesce(work_stop12, @nodate) <> @nodate)) or
       ((coalesce(work_start13, @nodate) <> @nodate) and (coalesce(work_stop13, @nodate) <> @nodate)) or
       ((coalesce(work_start14, @nodate) <> @nodate) and (coalesce(work_stop14, @nodate) <> @nodate)) or
       ((coalesce(work_start15, @nodate) <> @nodate) and (coalesce(work_stop15, @nodate) <> @nodate)) or
       ((coalesce(work_start16, @nodate) <> @nodate) and (coalesce(work_stop16, @nodate) <> @nodate)) then 'OUT'
  else 'OFF'
  end as work_status_now
  from timesheet_entry 
  where work_emp_id = @emp_id
    and work_date >= @today
    and work_date < @tomorrow
    and status <> 'OLD'), 'OFF')
end
go

grant execute on get_current_work_status to uqweb, QManagerRole
go

/*
select dbo.get_current_work_status(3512)

select work_emp_id, work_date, dbo.get_current_work_status(work_emp_id) current_work_status
from timesheet_entry
where work_date=Convert(date, dbo.get_emp_local_time(work_emp_id, GetDate()))
  and status <> 'OLD'

*/
