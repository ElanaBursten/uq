if object_id('dbo.check_summary_by_locate_GSU2') is not null
	drop procedure dbo.check_summary_by_locate_GSU2
GO

/* Used for NewJersey2 audits, but can be used for other update call centers
   in the same situation.
   These audits don't list the term id, yet we do need to check for those
   somehow, because sometimes NewJersey2 tickets come in that have no
   term ids on them that are UQ clients.
   We do this by looking for NewJersey2 tickets that:
   - are listed on the audit (naturally)
   - have at least one locate that is a NewJersey2 term id
*/

CREATE Procedure check_summary_by_locate_GSU2
	(
	@SummaryDate datetime,
	@CallCenter varchar(30)
	)
As
set nocount on

DECLARE @SummaryWindowStart datetime
DECLARE @SummaryWindowEnd datetime
DECLARE @SummaryDateEnd datetime

DECLARE @MasterCallCenter varchar(20)

-- get "master" call center via client table (which is the only place
-- in the database where a relation is defined between call center
-- and update call center)
select @MasterCallCenter = max(call_center) from client
where update_call_center = @CallCenter

/* Create a temporary table for NewJersey2 clients for faster lookup. */

DECLARE @gsu2_clients TABLE (
	oc_code varchar(20) NOT NULL PRIMARY KEY
)

INSERT INTO @gsu2_clients
  SELECT oc_code from client
  WHERE call_center = @MasterCallCenter 
    AND update_call_center = @CallCenter 
    
DECLARE @tickets_found TABLE (
	tf_ticket_number varchar(20) NOT NULL PRIMARY KEY
)

SELECT @SummaryDateEnd = DATEADD(d, 7, @SummaryDate)

/* Temporary table for tickets listed on the audit report. */
DECLARE @gn TABLE (
	client_code varchar (20) NOT NULL ,
	item_number varchar (20) NULL ,
	ticket_number varchar (20) NULL ,
	type_code varchar (20) NULL ,
	found int
)

/* Get the list of tickets on the summary */
INSERT INTO @gn
select '---' AS client_code, sd.item_number, sd.ticket_number, sd.type_code, 0 as found
 from summary_header sh
  inner join summary_detail sd
    on sh.summary_header_id=sd.summary_header_id
 where sh.summary_date=@SummaryDate
  and sh.call_center=@CallCenter
 order by sd.item_number, sd.ticket_number

/* Get the list of tickets that were received. 
   We look at ticket_format values of @CallCenter or @MasterCallCenter, then
   check the locates. 
   (Note: A term id cannot be both a client of NewJersey AND NewJersey2, so
   we should not find false positives for ticket_format == NewJersey.)
*/
INSERT INTO @tickets_found
select ticket.ticket_number
 from ticket
  inner join locate on ticket.ticket_id = locate.ticket_id 
 where ticket.transmit_date >= @SummaryDate
  and ticket.transmit_date < @SummaryDateEnd
  and (ticket.ticket_format = @CallCenter 
    or ticket.ticket_format = @MasterCallCenter)
  and locate.client_code in (select oc_code from @gsu2_clients)
 group by ticket.ticket_number

/* Mark them as found on the list from the summary */
update @gn set found=1
 from @tickets_found tf
  where ticket_number=tf_ticket_number

select * from @gn
 order by found, client_code, ticket_number
GO

grant execute on check_summary_by_locate_GSU2 to uqweb, QManagerRole
/*
exec dbo.check_summary_by_locate_GSU2 '2010-10-21', 'NewJersey2'
exec dbo.check_summary_by_locate_GSU2 '2008-10-21', 'NewJersey2'
*/
