if object_id('dbo.get_pending_wo_responses') is not null
	drop procedure dbo.get_pending_wo_responses
GO

CREATE Procedure get_pending_wo_responses(@CallCenter varchar(20))
as
set nocount on

  select top 100
    wo.wo_id as wo_id,
    wo.wo_source as wo_source,
    wo.client_wo_number as client_wo_number,
    wo.work_description as work_description,
    wo.work_cross as work_cross,
    wo.road_bore_count as road_bore_count,
    wo.driveway_bore_count as driveway_bore_count,
    wo.state_hwy_row as state_hwy_row,
    wo.status as status,
    wo.closed_date as closed_date,
    wo.status_date as status_date,
    wo.wo_number as remarks
  from wo_responder_queue worq
   inner join work_order wo on worq.wo_id = wo.wo_id
   inner join wo_assignment woa on wo.wo_id = woa.wo_id
  where @CallCenter = worq.wo_source
    and worq.ready_to_respond = 1
    and woa.active = 1
  order by worq.insert_date 
GO

grant execute on get_pending_wo_responses to uqweb, QManagerRole

/*
exec get_pending_wo_responses 'LAM01'
*/
