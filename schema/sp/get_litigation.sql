if object_id('dbo.get_litigation2') is not null
  drop procedure dbo.get_litigation2
go

create proc get_litigation2(@LitigationID int)
as
begin
  select 'damage_litigation' as tname, *
  from damage_litigation
  where litigation_id = @LitigationID

  select 'damage' as tname, *
  from damage
  where damage_id = 
    (select damage_id 
    from damage_litigation 
    where litigation_id = @LitigationID)
end
go

grant execute on get_litigation2 to uqweb, QManagerRole
go

