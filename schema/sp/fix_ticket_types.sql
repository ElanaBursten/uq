if object_id('dbo.fix_ticket_types') is not null
	drop procedure dbo.fix_ticket_types
GO

CREATE Procedure fix_ticket_types
as

declare @t1 table (ticket_id int not null primary key)
declare @highest_open_ticket int
declare @tickets_to_open table (ticket_id int not null primary key)
declare @tickets_to_close table (ticket_id int not null primary key)

-- Find the list of open tickets:
insert into @t1
select distinct ticket_id
 from locate with (NOLOCK)
 where closed=0

select @highest_open_ticket =
 (select max(ticket_id) from @t1)

-- find tickets that are closed, but with open locates:
-- select count(*) from ticket where kind='DONE' and ticket_id IN (select ticket_id from @t1)

-- get the list of tickets to re-open
insert into @tickets_to_open
select ticket.ticket_id
 from ticket with (NOLOCK)
  inner join @t1 t1 on ticket.ticket_id = t1.ticket_id
 where kind='DONE'

-- find tickets where locates are closed but ticket not DONE
insert into @tickets_to_close
select ticket_id
 from ticket with (NOLOCK)
 where kind <> 'DONE'
  and ticket_id NOT IN (select ticket_id from @t1)
  and ticket_id < @highest_open_ticket

-- We should get to this point with no locking (and no blocking),
-- then these last operations should be very fast

-- open
update ticket set kind='NORMAL' where ticket_id IN (select ticket_id from @tickets_to_open)
 and transmit_date < dateadd(d, -20, getdate())

-- close
update ticket set kind='DONE' where ticket_id IN (select ticket_id from @tickets_to_close)
 and transmit_date < dateadd(d, -20, getdate())

go

/*
exec dbo.fix_ticket_types
*/
