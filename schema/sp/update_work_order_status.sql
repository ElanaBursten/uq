if object_id('dbo.update_work_order_status') is not null
  drop procedure dbo.update_work_order_status
GO

create proc update_work_order_status (@WorkOrderID integer, @NewStatus varchar(5), @Closed bit, @StatusDate datetime, @StatusBy integer, @StatusedHow varchar(20), @CGAReason varchar(15))
as

set NOCOUNT on

IF @NewStatus = '-P'
  RETURN

DECLARE @NewKind varchar(20)

BEGIN TRANSACTION

  -- Add response for a changed status not already queued
  INSERT INTO wo_responder_queue (wo_id, client_wo_number, wo_source, client_id, ready_to_respond)
  SELECT DISTINCT wo.wo_id, wo.client_wo_number, wo.wo_source, wo.client_id,
    1  -- TODO: For now all responses are ready as soon as they are added; may need to revisit in a later rev
  FROM work_order wo
  LEFT OUTER JOIN wo_responder_queue wrq ON wo.wo_id = wrq.wo_id
  WHERE wo.wo_id = @WorkOrderID
    AND @NewStatus <> '-R'
    AND @NewStatus <> wo.status
    --AND @Closed = 1
    AND wrq.wo_id IS NULL
    AND (@NewStatus <> (SELECT top 1 wosh.status FROM wo_status_history wosh WHERE wosh.wo_id = @WorkOrderID ORDER BY insert_date DESC)
    OR @NewStatus IS NOT NULL AND NOT EXISTS (SELECT wosh.wo_id FROM wo_status_history wosh WHERE wosh.wo_id = @WorkOrderID))
    AND wo.wo_source = 'LAM01' -- Currently only Lambert work orders use the response queue

  -- Record the status change in history
  INSERT INTO wo_status_history (wo_id, status_date, status, statused_by_id, statused_how, cga_reason)
  VALUES (@WorkOrderID, @StatusDate, @NewStatus, @StatusBy, @StatusedHow, @CGAReason)

  -- If we moved from not closed to closed or vise versa, get a new kind
  SELECT @NewKind = case
  when (@Closed = 1) then 'DONE'
    when (@Closed = 0 and coalesce(closed, 0) = 0) then work_order.kind
    when (@Closed = 0 and kind is null) then 'NORMAL'
    else 'NORMAL' end
  FROM work_order (NOLOCK)
  WHERE wo_id = @WorkOrderID

  -- Update the work_order
  UPDATE work_order SET
    status = @NewStatus,
    status_date = @StatusDate,
    closed = @Closed,
    closed_date = case when @Closed = 1 then @StatusDate else closed_date end,
    kind = @NewKind
  WHERE wo_id = @WorkOrderID

COMMIT TRANSACTION
GO

grant execute on update_work_order_status to uqweb, QManagerRole
GO

/*
-- not closed, not queued for response:
exec dbo.update_work_order_status 110033, '-P', 0, '2011-04-07T01:30:00.000', 3510, 'MS SQL'
exec dbo.update_work_order_status 110033, '-R', 0, '2011-04-07T01:33:00.000', 3510, 'MS SQL'
exec dbo.update_work_order_status 110033, 'O', 0, '2011-04-07T06:30:00.000', 3510, 'MS SQL'
select * from wo_responder_queue where wo_id  = 110033

-- closed, queue for response:
exec dbo.update_work_order_status 110038, 'M', 1, '2011-04-07T07:55:00.000', 3510, 'MS SQL'
select * from wo_responder_queue where wo_id  = 110038

-- same status 2 times, should have only 1 response
exec dbo.update_work_order_status 110033, 'M', 1, '2011-04-07T08:30:00.000', 3510, 'MS SQL'
exec dbo.update_work_order_status 110033, 'M', 1, '2011-04-07T08:35:00.000', 3510, 'MS SQL'
select * from wo_responder_queue where wo_id  = 110033
*/
