if object_id('dbo.tse_timeruledata_2') is not null
	drop procedure dbo.tse_timeruledata_2
go


/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc tse_timeruledata_2(@ManagerID Int, @EndDate datetime, @LevelLimit int, @EmployeeStatus int)
as
 set nocount on
 declare @StartDate datetime
 set @StartDate = dateadd(d, -6, @EndDate)

 select
   e.h_emp_id as emp_id,
   dd.d as slot_work_date,
   e.h_emp_number as emp_number,
   e.h_short_name as short_name,
   e.h_report_level,
   e.h_namepath,
   e.h_idpath,
   emp.timerule_id,
   reference.code,
   tse.*
  from (select * from dbo.get_report_hier3(@ManagerID, 0, @LevelLimit, @EmployeeStatus)) e
   inner join dbo.DateSpan(@StartDate, @EndDate) dd on 1=1
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id and tse.work_date=dd.d
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee emp on e.h_emp_id = emp.emp_id
   left join reference on reference.ref_id = emp.timerule_id
   order by e.h_namepath, e.h_last_name, e.h_emp_id, dd.d
go
grant execute on tse_timeruledata_2 to uqweb, QManagerRole

/*
exec dbo.tse_timeruledata_2 211, '2004-03-27', 4, 1
exec dbo.tse_timeruledata_2 2676, '2004-03-27', 12, 1
exec dbo.tse_timeruledata_2 2676, '2004-09-18', 99, 1
*/
