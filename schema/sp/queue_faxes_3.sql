if object_id('dbo.queue_faxes_3') is not null
	drop procedure dbo.queue_faxes_3
go

/* @CallCenter is one call center code, or * to queue faxes for all call centers at once */
CREATE procedure dbo.queue_faxes_3 (
  @CallCenter varchar(20),
  @QueueNow bit = 1
)
as
  set nocount on

  declare @PrevQueueDate datetime

  if @CallCenter = '*'
    select @PrevQueueDate = (select max(queue_date) from fax_message)
  else
    select @PrevQueueDate = (select max(queue_date) from fax_message where call_center = @CallCenter)

  declare @QueueDate datetime
  select @QueueDate = getdate()

  declare @ncf table (
	fax_dest varchar (60) NULL ,
	call_center varchar (20) NULL ,
	caller_fax varchar (60) NULL ,
	ls_id int NOT NULL ,
	ticket_id int NOT NULL ,
	locate_id int NOT NULL,
	state varchar(20) NULL
  )

  insert into @ncf
  select case
      when coalesce(t.caller_fax, '') = '' then substring(t.con_name + ' ' + t.caller, 1, 60)
      else t.caller_fax
    end as fax_dest,
    t.ticket_format,
    t.caller_fax,
    ls.ls_id,
    t.ticket_id,
    ls.locate_id,
    t.work_state
  from fax_queue fq
    inner join locate_status ls on ls.ls_id = fq.ls_id
    inner join locate l on l.locate_id = ls.locate_id
    inner join ticket t on t.ticket_id = l.ticket_id
  where ls.ls_id not in (select ls_id from fax_message_detail)
    and ((t.ticket_format = @CallCenter) or (@CallCenter = '*'))

  -- select * from @ncf

  declare @faxes_queued int
  declare @locates_queued int
  set @faxes_queued = 0
  set @locates_queued = 0

  /* @QueueNow=1 is always used in production */
  if @QueueNow=1 begin
    begin transaction t1

    insert into fax_message (call_center, fax_dest, queue_date, previous_queue_date,
                             fax_status, complete_date)
    select
     call_center,
     fax_dest,
     @QueueDate as queue_date,
     @PrevQueueDate as previous_queue_date,
      case
        when (caller_fax IS NULL or caller_fax='')
        THEN 'No Fax Number'
        ELSE 'Pending'
      end as fax_status,
      case
        when (caller_fax IS NULL or caller_fax='')
        THEN @QueueDate
        ELSE Null
      end as complete_date
     from @ncf
     group by call_center, fax_dest, caller_fax
     order by call_center, fax_dest, caller_fax

    set @faxes_queued = @@ROWCOUNT

    insert into fax_message_detail
    select fm.fm_id, n.locate_id, n.ticket_id, n.ls_id
     from @ncf n
     inner join fax_message fm
       on fm.call_center=n.call_center
         and fm.queue_date=@QueueDate
         and fm.fax_dest = n.fax_dest

    set @locates_queued = @@ROWCOUNT

    -- Remove records from fax_queue
    delete from fax_queue
    where ls_id in (select ls_id from @ncf)

    commit transaction t1

  end else
    /* @QueueNow=0 is only used for testing/diagnostics */

    select
     call_center,
     fax_dest,
     @QueueDate as insert_date,
      case
        when caller_fax IS NULL or caller_fax=''
         THEN 'No Fax Number'
        ELSE 'Pending'
      end as fax_status
     from @ncf
     group by call_center, fax_dest, caller_fax
     order by call_center, fax_dest, caller_fax

  /* These are always 0 with @QueueNow=0 */
  select @faxes_queued as "FaxesQueued", @locates_queued as "LocatesQueued"
GO

grant execute on queue_faxes_3 to uqweb, QManagerRole
/*
exec queue_faxes_3 '*', 0
exec queue_faxes_3 'NewJersey', 0
exec queue_faxes_3 '1101', 1
*/
