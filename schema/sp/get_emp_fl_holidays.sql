if object_id('dbo.get_emp_fl_holidays') is not null
  drop procedure dbo.get_emp_fl_holidays
go

--The following does not count floating holidays in the @WeekStart - @WeekEnd range
create procedure dbo.get_emp_fl_holidays
    @EmpID integer,
    @WeekStart datetime,
    @WeekEnd datetime,
    @Year integer
as
select Count(*) as CountDays 
from timesheet_entry 
where (floating_holiday = 1) and (work_emp_id = @EmpID) and status <> 'OLD' 
    and (((work_date >= cast(@Year as char(4)) + '-01-01') and (work_date < @WeekStart)) 
    or ((work_date >= @WeekEnd) and (work_date < cast((@Year + 1) as char(4)) + '-01-01')))
go

grant execute on get_emp_fl_holidays to uqweb, QManagerRole

--exec dbo.get_emp_fl_holidays 3512, '2013-09-16', '2013-09-22', 2013
--exec dbo.get_emp_fl_holidays 10667, '2013-09-09', '2013-09-09', 2013