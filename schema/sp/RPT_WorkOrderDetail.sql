if object_id('dbo.RPT_WorkOrderDetail') is not null
  drop procedure dbo.RPT_WorkOrderDetail
go

create proc RPT_WorkOrderDetail (
  @WOID int,
  @AttachmentList VarChar(2000)
)

as
set nocount on

-- Work order data
select
  w.wo_id,
  w.wo_number,
  w.transmit_date,
  w.due_date,
  w.image,
  w.wo_source,
  w.closed_date,
  w.closed,
  w.caller_name,
  w.work_address_number,
  w.work_address_street,
  w.work_city,
  w.work_state,
  w.work_zip,
  w.work_lat,
  w.work_long,
  w.map_page,
  w.map_ref,
  w.caller_phone,
  w.client_wo_number,
  w.status_date,
  c.xml_ticket_format,
  s.status_name,
  e.short_name,
  cl.oc_code,
  woi.compliance_due_date,
  woi.account_number,
  woi.premise_id,
  coalesce(cga_visits,0) cga_visits,
  (select description from reference where type = 'bldtype' and code = woi.building_type) as BuildingClassDesc,
  (select description from reference where type = 'mapstat' and code = woi.map_status) as MapStatusDesc,
  (select description from reference where type = 'metrloc' and code =woi.actual_meter_location) as ActualMeterLocDesc,
  woi.meter_number,
  woi.actual_meter_number,
  (select description from reference where type = 'ynub' and code = woi.mercury_regulator) as MercuryRegulatorDesc,
  (select description from reference where type = 'ventclr' and code = woi.vent_clearance_dist) as VentClearanceDistDesc,
  (select description from reference where type = 'ventclr' and code = woi.vent_ignition_dist) as VentIgnitionDistDesc,
  (select description from reference where type = 'gaslight' and code = woi.gas_light) as GasLightDesc,
  woi.alert_first_name, 
  woi.alert_last_name,
  woi.alert_order_number
from work_order w
left join call_center c on c.cc_code = w.wo_source
left join statuslist s on s.status = w.status
left join employee e on e.emp_id = w.assigned_to_id
left join client cl on cl.client_id = w.client_id
left join work_order_inspection woi on w.wo_id=woi.wo_id -- WGL Gas Inspection data
where w.wo_id = @WOID

-- Tickets data
select
  t.ticket_number,
  t.ticket_format,
  t.transmit_date
from ticket t
inner join work_order_ticket wt on wt.ticket_id = t.ticket_id
where
  wt.wo_id = @WOID and
  wt.active = 1
order by t.ticket_number

-- Notes data
select wn.entry_date, wn.modified_date, wn.note note, wn.wo_id
from work_order w
  inner join work_order_notes wn on wn.wo_id = w.wo_id
where w.wo_id = @WOID
order by w.wo_id, wn.entry_date

-- Attachments 
select a.filename, a.orig_filename, a.comment, a.attach_date, e.short_name, a.extension,
  a.orig_filename as display_name
from attachment a
  inner join employee e on a.attached_by = e.emp_id 
  inner join work_order w on (a.foreign_id = w.wo_id and a.foreign_type = 7)
where (@AttachmentList = '' or a.attachment_id in (select l.Id from dbo.IdListToTable(@AttachmentList) l))
  and w.wo_id = @WOID
  and a.active = 1
order by
  a.attach_date, a.orig_filename
 
-- WGL Gas Inspection selected remedies
select wr.wo_id, wr.work_type_id, wt.work_type, wt.work_description, wt.flag_color, wt.alert, wr.active
from work_order_remedy wr 
  join work_order_work_type wt on wt.work_type_id = wr.work_type_id
where wr.wo_id = @WOID
  and wr.active = 1
  order by flag_color, work_description

go
grant execute on RPT_WorkOrderDetail to uqweb, QManagerRole

/*
exec dbo.RPT_WorkOrderDetail 110088, ''
exec dbo.RPT_WorkOrderDetail 13245677, '1,2,3,4'
*/
