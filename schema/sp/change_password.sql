if object_id('dbo.change_password') is not null
	drop procedure dbo.change_password
go

create procedure change_password (
  @UserID       integer,
  @NewPassword  varchar(40)
)
as
  set nocount on
  declare @NewDate datetime
  set @NewDate = convert(char(10), dateadd(d, Convert(int, dbo.GetConfigValue('PasswordExpirationDays', '120')), getdate()), 102)
  update users set
    password = @NewPassword,
    chg_pwd  = 0,
    chg_pwd_date = @NewDate
  where users.uid = @UserID

go
grant execute on change_password to uqweb, QManagerRole

/*
dbo.change_password 2227, 'fishfaces'
select convert(char(10), getdate( ), 102)

select * from configuration_data
insert into configuration_data (name, value, editable) values ('PasswordExpirationDays', '120', 1)
*/
