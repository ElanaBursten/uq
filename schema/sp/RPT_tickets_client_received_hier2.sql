if object_id('dbo.RPT_tickets_clients_received_hier2') is not null
  drop procedure dbo.RPT_tickets_clients_received_hier2
go

create proc RPT_tickets_clients_received_hier2 (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(800),
  @ClientList varchar(800),
  @ManagerID int,
  @GroupByTicketType int
)
as
  set nocount on

  -- This result set is designed such that a nearly default crosstab
  -- will generate the desired results; hence the user friendly column names,
  -- space before Tickets to make it sort first, etc.

  declare @data table (
	[Center] varchar(20) NOT NULL,
	[Date] datetime NULL ,
	[TicketType] varchar(40) NOT NULL,
	[Term] varchar(20) NOT NULL ,
	[Received] int NULL ,
	[Manager_short_name] varchar(40)
  )

  declare @centers table (
    call_center varchar(20) not null primary key
  )

  declare @clients table (
    term varchar(10) not null primary key
  )

  insert into @centers 
    select S from dbo.StringListToTable(@CallCenterList)
  insert into @clients 
    select S from dbo.StringListToTable(@ClientList)

  if @ManagerID=0 begin

    insert into @data
    select
      call_center,
      ticket_date,
      ticket_type,
      '  Tickets',
      count(distinct ticket_id) as N,
      '-' as "Manager_short_name"
    from (
      select
        ticket.ticket_format as call_center,
        convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
        ticket.ticket_type,
        ticket.ticket_id
      from ticket with (index(ticket_transmit_date))
      inner join @centers cc 
        on ticket.ticket_format = cc.call_center
      where ticket.transmit_date between @StartDate and @EndDate
       and exists (select ticket_id from locate where locate.ticket_id=ticket.ticket_id and locate.status <> '-N')
      ) as t
    group by
      call_center,
      ticket_date,
      ticket_type

    if @ClientList <> '' begin
      insert into @data
      select
        call_center,
        ticket_date,
        ticket_type,
        client_code,
        count(ticket_id) as N,
        '-' as "Manager_short_name"
      from (
        select
          ticket.ticket_format as call_center,
          convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
          ticket.ticket_type,
          locate.client_code,
          ticket.ticket_id
        from ticket with (index(ticket_transmit_date))
        inner join locate with (index(locate_ticketid))
          on ticket.ticket_id=locate.ticket_id
        inner join @centers cc 
          on ticket.ticket_format = cc.call_center
        where ticket.transmit_date between @StartDate and @EndDate
          and (@ClientList = '*' or locate.client_code in (select term from @clients))
          and locate.status <> '-N'
        ) as t
      group by
        t.call_center,
        t.ticket_date,
        t.ticket_type,
        client_code
      order by
        t.call_center,
        t.ticket_date,
        t.ticket_type,
        client_code
    end

  end else begin

    insert into @data
      select
        call_center,
        ticket_date,
        ticket_type,
        '  Tickets' as "Term",
        count(distinct ticket_id) as "Received",
        (select short_name from employee where emp_id=@ManagerID) as "Manager_short_name"
      from (
        select
          ticket.ticket_format as call_center,
          convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
          ticket.ticket_type,
          ticket.ticket_id
        from ticket with (index(ticket_transmit_date))
        inner join locate with (index(locate_ticketid))
          on ticket.ticket_id=locate.ticket_id
        inner join dbo.get_hier(@ManagerID,0) e
          on locate.assigned_to_id = e.h_emp_id
        inner join @centers cc 
          on ticket.ticket_format = cc.call_center
        where ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
          and locate.status <> '-N'
        ) as t
      group by
        call_center,
        ticket_date,
        ticket_type

    if @ClientList <> '' begin
      insert into @data
      select
        call_center,
        ticket_date,
        ticket_type,
        client_code,
        count(distinct ticket_id) as N,
        (select short_name from employee where emp_id=@ManagerID) as "Manager_short_name"
      from (
        select
          ticket.ticket_format as call_center,
          convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
          ticket.ticket_type,
          locate.client_code,
          ticket.ticket_id
        from ticket with (index(ticket_transmit_date))
        inner join locate with (index(locate_ticketid))
          on ticket.ticket_id=locate.ticket_id
        inner join dbo.get_hier(@ManagerId,0) e
          on locate.assigned_to_id = e.h_emp_id
        inner join @centers cc 
          on ticket.ticket_format = cc.call_center
        where ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
          and (@ClientList = '*' or locate.client_code in (select term from @clients))
          and locate.status <> '-N'
        ) as t
      group by
        t.call_center,
        t.ticket_date,
        t.ticket_type, 
        client_code
      order by
        t.call_center,
        t.ticket_date,
        t.ticket_type,
        client_code
    end

  end

  if @GroupByTicketType=0
    update @Data set TicketType='-'

  select * from @data
  UNION ALL
  select Center,
    Date,
    TicketType,
    ' Locates',
    SUM(Received),
    '-' as "Manager_short_name"  
  from @data 
  where Term <> '  Tickets' 
  group by Center, Date, TicketType

go

grant execute on RPT_tickets_clients_received_hier2 to uqweb, QManagerRole

/*

exec dbo.RPT_tickets_clients_received_hier2 '2006-02-01', '2006-03-01', 'FNV1', '*', 5405, 0
exec dbo.RPT_tickets_clients_received_hier2 '2004-06-05', '2004-08-06', 'OCC2', 'WGL904', 697, 0

exec dbo.RPT_tickets_clients_received_hier2 '2005-10-05', '2005-10-07', 'FOK1', '', 2676, 0
exec dbo.RPT_tickets_clients_received_hier2 '2005-10-05', '2005-10-07', 'FOK1', '', 0, 0

-- This one:
exec dbo.RPT_tickets_clients_received_hier2 '2003-09-29', '2003-09-30', 'NewJersey', '*', 0, 0
-- should be same as ...
exec dbo.RPT_tickets_clients_received_hier2 '2003-09-29', '2003-09-30', 'NewJersey', '*', 212, 0
--and this is a smaller subset
exec dbo.RPT_tickets_clients_received_hier2 '2003-09-29', '2003-09-30', 'NewJersey', '*', 216, 0

Times out after sql svr 2008 upgrade:
exec dbo.RPT_tickets_clients_received_hier2 '2009-10-01', '2009-10-16', 'FDX1,FDX2,FDX3,FDX4', '*', 6413, 0
*/
