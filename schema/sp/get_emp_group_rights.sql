if object_id('dbo.get_emp_group_rights') is not null
  drop function dbo.get_emp_group_rights
go

create function get_emp_group_rights(@EmpId int)
returns
  @result TABLE (
    emp_id int,
    right_id int,
    group_id int,
    group_name varchar(30),
    allowed varchar(1),
    limitation varchar(200),
    priority int
  )
as
begin

  declare @temp TABLE (
    emp_id int,
    _right_id int not null primary key,
    _group_id int,
    group_name varchar(30),
    allowed varchar(1),
    limitation varchar(200),
    priority int
  )

  insert into @temp (emp_id, _right_id)
    select
      @EmpID emp_id,
      right_id
    from
      right_definition

  update @temp
  set _group_id = 
    (select top 1 gd.group_id
     from group_definition gd
       inner join group_right gr on gr.group_id = gd.group_id
       left  join employee_group eg on ((eg.group_id = gr.group_id) and (eg.emp_id = @EmpID))
     where gd.active = 1 and gr.right_id = _right_id and gr.allowed = 'Y'
       and (
             (eg.emp_id is not null) -- Employee is in this group
             or (gd.is_default = 1)  -- Or, the group applies to everyone
           )
     -- Ordering by priority and then group_id gets the highest priority
     -- limitation for multiple grants and handles ties consistently
     order by priority, gd.group_id
     )

  update @temp set
    limitation = (select limitation from group_right gr where gr.group_id = _group_id and gr.right_id = _right_id),
    group_name = (select group_name from group_definition gd where gd.group_id = _group_id)

  update @temp set
    allowed  = ('Y'),
    priority = (select priority from group_definition gd where gd.group_id = _group_id)
  where _group_id is not null

  update @temp set allowed = 'N' where allowed is null

  insert into @result
    select * from @temp order by _right_id

  return
end
go

/*************
select * from dbo.get_emp_group_rights(0285)
select * from dbo.get_emp_group_rights(8411)
select * from dbo.get_emp_group_rights(3510)
select * from dbo.get_emp_group_rights(3509)
*************/

if object_id('dbo.get_effective_rights') is not null
  drop function dbo.get_effective_rights
GO

/* Merge the employee rights into the group rights to get a flat list of every possible right and its state for a user */
create function dbo.get_effective_rights(@EmpID int)
returns @Result table (right_id int not null, allowed varchar(1) not null, group_id int null, limitation varchar(200) null, source varchar(3) not null) as
begin
  insert into @Result
  select right_id, allowed, group_id, limitation, 'grp'
  from dbo.get_emp_group_rights(@EmpID)

  update res
  set allowed = 'Y',
    source = 'emp',
    res.limitation = (select limitation from employee_right er where er.emp_id = @EmpID and res.right_id = er.right_id)
  from @Result as res
  where res.right_id in (select right_id from employee_right where emp_id = @EmpID and allowed = 'Y')

  return
end
Go

/*
select * from dbo.get_effective_rights(3510) order by right_id
*/

/*************************************************************/

/* This is unused right now */
if object_id('dbo.get_emp_right') is not null
  drop procedure dbo.get_emp_right
GO

create procedure dbo.get_emp_right(@EmpID int, @RightID int)
as begin
  select * from dbo.get_effective_rights(@EmpID)
  where right_id = @RightID
end
Go

grant execute on dbo.get_emp_right to uqweb, QManagerRole


/*
dbo.get_emp_right 3510, 18
*/

/*************************************************************/

if object_id('dbo.emp_has_right') is not null
  drop function dbo.emp_has_right
GO

create function dbo.emp_has_right(@EmpID int, @RightID int)
returns bit
as begin
  declare @Result bit
  select @Result = 0
  if exists(select right_id from employee_right where emp_id = @EmpID and right_id = @RightID and allowed = 'Y')
    select @Result = 1
  else if exists(select right_id from employee_group eg
              inner join group_right gr on gr.group_id = eg.group_id
              inner join group_definition gd on gd.group_id = gr.group_id
            where eg.emp_id = @EmpID
              and gr.right_id = @RightID
              and allowed = 'Y'
              and gd.active = 1)
    select @Result = 1

  return(@Result)
end
Go

grant execute on dbo.emp_has_right to uqweb, QManagerRole

/*
select dbo.emp_has_right(3510, 18)
select dbo.emp_has_right(3510, 24)
*/
