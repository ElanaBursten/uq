-- Obsolete: Replaced by multi_open_totals3 on Sep. 23, 2010
if object_id('dbo.multi_open_totals') is not null
  drop procedure dbo.multi_open_totals
GO

create proc multi_open_totals (@managers varchar(100))
as
-- This SP is the replacement for DRTT; it processes more than
-- one manager at onces, and does so efficiently.

set nocount on

-- a place to keep the list of requested managers in table form
declare @manager_list table (
  manager_id integer not null primary key
)

-- These are the totals returned to the client for each employee
DECLARE @results TABLE (
  emp_id integer NOT NULL,
  report_to integer NOT NULL,
  short_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  type_id integer,
  tickets_tot integer NOT NULL default 0,
  locates_tot integer NOT NULL default 0,
  points_tot  integer NOT NULL default 0,
  active bit default 1,
  ticket_view_limit integer NOT NULL default 0,
  PRIMARY KEY(emp_id, report_to)
)

-- List of the emps, de-duped from the above
DECLARE @emps TABLE (
  emp_id integer NOT NULL PRIMARY KEY
)

-- various temps used to assemble the final data
DECLARE @open_tickets TABLE (
  ticket_emp_id integer NOT NULL,
  N integer
)

DECLARE @open_locates TABLE (
  locate_emp_id integer NOT NULL,
  N integer
)

DECLARE @ut_managers TABLE (
  manager_id integer NOT NULL PRIMARY KEY
)

-- all emps for unacked ticket find
DECLARE @ut_emps TABLE (
  emp_id integer NOT NULL,
  manager_id integer NOT NULL,
  PRIMARY KEY (emp_id, manager_id)
)

DECLARE @unacked_tickets TABLE (
  ticket_id integer NOT NULL,
  manager_id integer NOT NULL,
  primary key(ticket_id, manager_id)
)

-- get the list of manager from the string to the table
insert into @manager_list
  select cast(S as int) from dbo.StringListToTable(@managers)
  where (S is not null) and (S<>'')

-- Initialize the results table with the list of locators under the managers
INSERT INTO @results (emp_id, report_to, short_name, emp_number, type_id, active, ticket_view_limit)
  select distinct emp_id, report_to, short_name, emp_number, type_id, active, coalesce(ticket_view_limit, 0)
  from employee
   inner join @manager_list ml on report_to = ml.manager_id or emp_id=ml.manager_id
  where employee.report_to is not null   -- this disallowing the top guy.

-- The Colorado managers all want to see the "CO Unassigned" locator bucket
delete from @results where emp_id = 1625
insert into @results (emp_id, report_to, short_name, emp_number, type_id)
select 1625, ml.manager_id, 'CO Unassigned', NULL, -999
 from @manager_list ml
  where ml.manager_id in (1248, 1249, 1250, 1251)

-- The CA  managers all want to see both "CA Unassigned" locator buckets
insert into @results (emp_id, report_to, short_name, emp_number, type_id)
select 2086, ml.manager_id, 'Unassigned N. CA', NULL, -999
 from @manager_list ml
  where ml.manager_id in (1865,1866,1867,1868,1869,1870,1871,1872,1873)

insert into @results (emp_id, report_to, short_name, emp_number, type_id)
select 2087, ml.manager_id, 'Unassigned S. CA', NULL, -999
 from @manager_list ml
  where ml.manager_id in (1865,1866,1867,1868,1869,1870,1871,1872,1873)

-- gather a de-duped list, which will only have one copy of those buckets
insert into @emps
select distinct emp_id from @results

-- Get a count of open tickets for the locators
INSERT INTO @open_tickets (ticket_emp_id, N)
  select res.emp_id, count(distinct ticket.ticket_id) as N
  from @emps res
    inner join locate on res.emp_id = locate.assigned_to
    inner join ticket on locate.ticket_id=ticket.ticket_id
  group by res.emp_id

-- Get a count of the open locates for the locators
INSERT INTO @open_locates (locate_emp_id, N)
  select res.emp_id, count(locate.locate_id) as N
  from @emps res
    inner join locate on locate.assigned_to = res.emp_id
    inner join ticket on locate.ticket_id = ticket.ticket_id
  group by res.emp_id

update @results set locates_tot = n
  from @open_locates where emp_id=locate_emp_id

update @results set tickets_tot = ot.n
  from @open_tickets ot where emp_id = ot.ticket_emp_id

-- eligible for unacked tickets
declare @ShowUnackedTicketsRightID int
set @ShowUnackedTicketsRightID = (select right_id from right_definition where entity_data = 'TicketsShowUnAcked')

-- of the managers requested, get the ones who need an Unacked bin
insert into @ut_managers (manager_id)
select ml.manager_id
 from @manager_list ml
  inner join @results r on ml.manager_id = r.emp_id
where dbo.emp_has_right(ml.manager_id, @ShowUnackedTicketsRightID) = 1

-- Get by with only one cursor, to run get_hier a few times
declare @manid integer
DECLARE ManListCursor CURSOR FOR
select manager_id from @manager_list

OPEN ManListCursor

FETCH NEXT FROM ManListCursor into @manid
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Collect the emps for the unacked tickets
  insert into @ut_emps (emp_id, manager_id)
  select h_emp_id, @manid from dbo.get_hier(@manid, 0)

  FETCH NEXT FROM ManListCursor into @manid
END

CLOSE ManListCursor
DEALLOCATE ManListCursor

-- Check for any tickets that need acknowledgement
insert into @unacked_tickets (ticket_id, manager_id)
  select ticket_ack.ticket_id, e.manager_id
  from ticket_ack
    inner join @ut_emps e on ticket_ack.locator_emp_id = e.emp_id
  where ticket_ack.has_ack = 0
    and ticket_ack.locator_emp_id is not null
  group by ticket_ack.ticket_id, e.manager_id

-- Insert rows in the result for the unack buckets
insert into @results (emp_id, report_to, short_name, emp_number, tickets_tot, locates_tot)
select -1, utm.manager_id, 'New Emergencies', NULL, 
  (select count(*) from @unacked_tickets unackt where unackt.manager_id=utm.manager_id),
  (select count(*)
   from @unacked_tickets unack
    inner join locate on unack.ticket_id = locate.ticket_id
       and locate.assigned_to is not null
    where unack.manager_id=utm.manager_id)
from @ut_managers utm

select * from @results emp_workload order by report_to, short_name

GO
grant execute on multi_open_totals to uqweb, QManagerRole

/*
exec dbo.multi_open_totals '211'
exec dbo.multi_open_totals '212'
exec dbo.multi_open_totals '1787'
exec dbo.multi_open_totals '2676'
exec dbo.multi_open_totals '5034'
exec dbo.multi_open_totals '211,212'
exec dbo.multi_open_totals '697,211,212'
exec dbo.multi_open_totals '211,208,207,1634,2102,2139,2131'
exec dbo.multi_open_totals '1787,6301,5372,563,561,5116,599,554,1221,628,591'
exec dbo.multi_open_totals '2597,2637,2646,5076,5083,2604,2620,2623,2611,4426'
exec dbo.multi_open_totals '2540,2545,4913,2563,2564,2167,2182,5864,3648'
exec dbo.multi_open_totals '1787,6301,571,5372,563,551,561,599,5116,663,554,5176,1221,628,2124'
exec dbo.multi_open_totals '697,3841,791,816,741,810'

select ticket_ack.ticket_id, locate.assigned_to
 from ticket_ack with(index(ticket_ack_has_ack))
    inner join locate
      on ticket_ack.ticket_id = locate.ticket_id
  where ticket_ack.has_ack = 0

select * from ut_emps
drop table ut_emps

  select ticket_ack.ticket_id, e.manager_id
  from ticket_ack with(index(ticket_ack_has_ack))
    inner join locate
      on ticket_ack.ticket_id = locate.ticket_id
    inner join @ut_emps e on locate.assigned_to = e.emp_id
  where ticket_ack.has_ack = 0
  group by ticket_ack.ticket_id, e.manager_id


-- refresh all of them:
exec dbo.multi_open_totals '211,340,1869,2108,2263,2304,2305,2327,2676,2677,2678,2692,2693,2694,2725,2726,2727,2777,3511,3849,3850,4255'

select * from ticket_ack
where ticket_ack.has_ack = 0

*/
