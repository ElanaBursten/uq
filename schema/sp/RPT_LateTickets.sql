if object_id('dbo.RPT_LateTickets') is not null
	drop procedure dbo.RPT_LateTickets
GO

CREATE Procedure dbo.RPT_LateTickets
    (
        @StartDate datetime,
        @EndDate datetime,
        @CallCenter varchar(20),
        @SortBy char(1),
        @NowDate datetime
     )

As
  SET NOCOUNT ON

  DECLARE @totaltix int
  DECLARE @latetix int
  DECLARE @totalloc int
  DECLARE @lateloc int

  declare @results table (
	[Category] varchar(40) NULL,
	[client_code] [varchar] (10) NULL ,
	[client_name] [varchar] (40)  NOT NULL ,
	[due_date] [datetime] NULL ,
	[locator_id] [int] NOT NULL ,
	[short_name] [varchar] (30)  NULL ,
	[ticket_number] [varchar] (20)  NOT NULL ,
	[ticket_type] [varchar] (38)  NULL ,
	[status] [varchar] (5) NOT NULL ,
	[closed] [bit] NOT NULL ,
	[closed_date] [datetime] NULL ,
	[last_sync_date] [datetime] NULL ,
	[work_type] [varchar] (90)  NULL,
	[ticket_id] [int] NOT NULL 
  ) 

  INSERT INTO @results
          SELECT
                 '---' as Category,
                 locate.client_code,
                 client.client_name,
                 ticket.due_date,
                 assignment.locator_id,
                 employee.short_name,
                 ticket.ticket_number,
                 ticket.ticket_type,
                 locate.status,
                 locate.closed,
                 case when locate.closed=1 then locate.closed_date
                      else NULL
                  end as closed_date,
                 (SELECT MAX(sync_log.sync_date) FROM sync_log
                   WHERE assignment.locator_id = sync_log.emp_id)
                    AS last_sync_date,
                 ticket.work_type,
                 ticket.ticket_id
            FROM locate with (INDEX(locate_ticketid)),
                 ticket,
                 assignment with (INDEX(assignment_locateid_locatorid)),
                 client,
                 employee
           WHERE locate.ticket_id = ticket.ticket_id AND
                 locate.locate_id = assignment.locate_id AND
                 locate.client_id = client.client_id AND
                 employee.emp_id = assignment.locator_id AND
		 ticket.ticket_format = @CallCenter AND
                 ticket.due_date BETWEEN @StartDate AND @EndDate AND
		 assignment.active=1 AND
		 (  (locate.closed = 1 AND locate.closed_date > ticket.due_date) OR
		    (locate.closed = 0 ) AND due_date<@NowDate ) AND
                 (ticket.ticket_format<>'ATLANTA' OR
                  (ticket.kind<>'EMERGENCY' AND locate.status<>'O'))

  UPDATE @results SET Category=
          case when (closed = 1) AND closed_date > due_date then 'LATE (closed)'
                      when (closed = 1) then 'NOT LATE'
                      when (closed = 0) AND (due_date<@NowDate) AND last_sync_date>due_date then 'LATE (open)'
                      when (closed = 0) AND (due_date<@NowDate) then 'POTENTIALLY LATE (might sync)'
                      else 'STILL TIME (not yet due)'
                 end

    If @SortBy = 'C'
	SELECT  *   FROM @results   ORDER BY 2, 1, 4, 7

    If @SortBy = 'L'
	SELECT *   FROM @results    ORDER BY 6, 2, 4

  /* then return another recordset with the totals */
	SELECT @totaltix = count(distinct ticket_id) FROM @results
	SELECT @latetix = count(distinct ticket_id) FROM @results where category='LATE (closed)' OR  category= 'LATE (open)'
	SELECT @totalloc = count(*) FROM @results
	SELECT @lateloc = count(*) FROM @results where category='LATE (closed)' OR  category= 'LATE (open)'

	SELECT @latetix AS 'LateTickets', @totaltix-@latetix AS 'PotLateTickets',
               @lateloc AS 'LateLocates', @totalloc-@lateloc AS 'PotLateLocates'

go

grant execute on RPT_LateTickets to uqweb, QManagerRole

/*
RPT_LateTickets '2003-01-16', '2003-01-17', 'FDX2', 'C', '2003-01-17T15:30:00'
RPT_LateTickets '2003-05-20', '2003-05-21', 'Atlanta', 'C', '2003-05-22'
*/
