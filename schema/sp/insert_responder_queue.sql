if object_id('dbo.insert_responder_queue') is not null
  drop procedure dbo.insert_responder_queue
go

CREATE PROCEDURE insert_responder_queue (
  @LocateId int
)
AS

insert into responder_queue (locate_id, ticket_format, client_code,
do_not_respond_before)
select l.locate_id, t.ticket_format, l.client_code, t.do_not_respond_before
from locate l
inner join ticket t
on l.ticket_id = t.ticket_id
where l.locate_id = @LocateId

GO
grant execute on insert_responder_queue to uqweb, QManagerRole

/*
exec insert_responder_queue {some-locate-id}
*/

