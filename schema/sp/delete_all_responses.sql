if object_id('dbo.delete_all_responses') is not null
	drop procedure dbo.delete_all_responses
GO

CREATE Procedure delete_all_responses
	(
	@CallCenter varchar(20),
	@ClientCode varchar(10)
	)
As

set nocount on

declare @pr table (
	locate_id int NOT NULL,
	ticket_format varchar(20) NOT NULL,
	client_code varchar(10) NOT NULL
)

insert into @pr
select rq.locate_id, ticket.ticket_format, locate.client_code
 from responder_queue rq 
 inner join locate on rq.locate_id=locate.locate_id
 inner join ticket on locate.ticket_id=ticket.ticket_id

set nocount off

delete from responder_queue where locate_id in 
 (select locate_id from @pr where ticket_format=@CallCenter and (client_code=@ClientCode OR @ClientCode='*'))

go

/*
exec delete_all_responses 'Atlanta', 'GP644'
*/
