if object_id('dbo.get_attachments_to_copy') is not null
  drop procedure dbo.get_attachments_to_copy
go

create proc get_attachments_to_copy(@wo_id int, @ticket_id int,
  @destination_linked_to_source bit output, @download_location_id int output)
as
  set nocount on

  select @destination_linked_to_source = count(*)
  from work_order_ticket 
  where
    wo_id = @wo_id and
    ticket_id = @ticket_id and
    active = 1

  select @download_location_id = max(location_id) 
  from upload_location
  where
    @destination_linked_to_source > 0 and
    description = 'Download' and 
    active = 1

  /* TODO: We may need to check for duplicate wo_id/orig_filename, if we
     allow copying from 2 work orders to the same ticket. We're currently
     just checking for duplicate orig_filename. */
  select * from attachment a1
  where
    @destination_linked_to_source > 0 and
    a1.foreign_type = 7 and
    a1.foreign_id = @wo_id and
    a1.upload_date is not null and
    a1.active = 1 and
    not exists (
      select * from attachment a2
      where
        a2.foreign_type = 1 and
        a2.foreign_id = @ticket_id and
        a2.orig_filename = a1.orig_filename and
        a2.active = 1
    )
go

grant execute on get_attachments_to_copy to uqweb, QManagerRole

/*
DECLARE @wo_id int
DECLARE @ticket_id int
DECLARE @destination_linked_to_source bit
DECLARE @download_location_id int

set @wo_id = 110000
set @ticket_id = 1000
set @destination_linked_to_source = 0
set @download_location_id = 0

EXECUTE dbo.get_attachments_to_copy @wo_id, @ticket_id, @destination_linked_to_source OUTPUT, @download_location_id OUTPUT

print @destination_linked_to_source
print @download_location_id

*/

