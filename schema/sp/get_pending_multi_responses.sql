if object_id('dbo.get_pending_multi_responses') is not null
	drop procedure dbo.get_pending_multi_responses
GO


CREATE Procedure get_pending_multi_responses
	(
	@RespondTo varchar(20),
	@CallCenter varchar(20),
	@ClientMatch varchar(20),
	@ParsedLocatesOnly bit = 1
	)
As

set nocount on

select rq.locate_id, locate.status, ticket.ticket_format,
 ticket.ticket_id, ticket.ticket_number, locate.client_code,
 ticket.ticket_type, ticket.work_state, rq.insert_date,
 rq.respond_to, ticket.service_area_code
 from responder_multi_queue rq (NOLOCK)
 inner join locate (NOLOCK) on rq.locate_id=locate.locate_id
 inner join ticket (NOLOCK) on locate.ticket_id=ticket.ticket_id
 where rq.ticket_format=@CallCenter
  and ticket.ticket_number not like 'MAN%'
  and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
  and rq.respond_to=@RespondTo
  and rq.client_code LIKE @ClientMatch
--  and service_area_code IS NOT NULL
--  and service_area_code<>''

GO
grant execute on get_pending_multi_responses to uqweb, QManagerRole

/*
exec get_pending_multi_responses 'client', 'FMW1', 'PEP%'

delete from responder_multi_queue where insert_date<'2003-07-01'

select count(*) from responder_multi_queue where insert_date>'2003-07-01'

select top 20 
 rq.locate_id, locate.status, ticket.ticket_format,
 ticket.ticket_id, ticket.ticket_number, locate.client_code,
 ticket.ticket_type, ticket.work_state, rq.insert_date,
 rq.respond_to, ticket.service_area_code
 from responder_multi_queue rq (NOLOCK)
 inner join locate (NOLOCK) on rq.locate_id=locate.locate_id
 inner join ticket (NOLOCK) on locate.ticket_id=ticket.ticket_id
where insert_date>'2003-07-01'

select count(*) from responder_multi_queue
select top 50 * from responder_multi_queue

delete from responder_multi_queue where insert_date < select dateadd(d, -30, getdate())

*/
