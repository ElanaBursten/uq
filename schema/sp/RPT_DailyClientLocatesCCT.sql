if object_id('dbo.RPT_DCL_NoSyncRangeCCT') is not null
	drop procedure dbo.RPT_DCL_NoSyncRangeCCT
GO

create procedure dbo.RPT_DCL_NoSyncRangeCCT
  (
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit    -- 0 = False, 1 = True
  )
as
set nocount on

select
  ticket.ticket_number,
  convert(datetime, convert(varchar(12), locate.closed_date , 102) , 102) as closed_date_only,
  locate.closed_date,
  locate.client_code,
  locate.status,
  locate.modified_date,  -- most recent sync date
  locate.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date,
  locate.assigned_to_id,
  ticket.ticket_type
from ticket with (index(ticket_transmit_date))
  inner join locate with (index(locate_ticketid)) on ticket.ticket_id = locate.ticket_id
where locate.status <> '-N'
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList))
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
--  and (locate.modified_date in (select max(l.modified_date)
--                                from locate l
--                                where l.ticket_id = ticket.ticket_id)
--      or @ShowStatusHistory=1)
order by locate.closed_date
go
grant execute on dbo.RPT_DCL_NoSyncRangeCCT to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_NoSyncRangeCCT '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0, 1
*/

if object_id('dbo.RPT_DCL_SyncRangeCCT') is not null
  drop procedure dbo.RPT_DCL_SyncRangeCCT
GO

create procedure dbo.RPT_DCL_SyncRangeCCT
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

set nocount on
declare @StatusedLocates table (
  closed_date_only datetime not null,
  closed_date datetime not null,
  client_code varchar(10) not null,
  status varchar(5) not null,
  modified_date datetime not null,
  closed bit not null,
  ticket_id integer not null,
  locate_id integer not null,
  assigned_to_id integer not null)

insert into @StatusedLocates select
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102) as closed_date_only,
  min(ls.status_date) as closed_date,
  locate.client_code,
  ls.status,
  min(ls.insert_date) as modified_date,  -- sync date
  locate.closed,
  locate.ticket_id,
  locate.locate_id,
  locate.assigned_to_id
 from locate_status ls
  inner join locate on ls.locate_id = locate.locate_id
 where ls.insert_date between @SyncDateFrom and @SyncDateTo
  and ls.status <> '-N'
  and locate.modified_date >= @SyncDateFrom  -- this one is a perf opt
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList))
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and (ls.status_date in (select max(locate_status.status_date)
                           from locate_status
                           where locate_status.locate_id = locate.locate_id
                             and locate_status.status_date is not null)
      or (@ShowStatusHistory=1 and ls.status_date is not null))
 group by
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102),
  locate.client_code,
  ls.status,
  locate.closed,
  locate.ticket_id,
  locate.locate_id,
  locate.assigned_to_id

select
  ticket.ticket_number,
  L.closed_date_only,
  L.closed_date,
  L.client_code,
  L.status,
  L.modified_date,  -- sync date
  L.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  ticket.due_date,
  L.assigned_to_id,
  ticket.ticket_type
from @StatusedLocates L
inner join ticket on ticket.ticket_id = L.ticket_id
where ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by L.closed_date

go
grant execute on dbo.RPT_DCL_SyncRangeCCT to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_SyncRangeCCT '2003-05-14', '2003-05-21', '2003-05-14', '2003-05-15', 'ATL01', 'Atlanta', 2, 0

*/

if object_id('dbo.RPT_DailyClientLocatesCCT') is not null
	drop procedure dbo.RPT_DailyClientLocatesCCT
GO

create procedure dbo.RPT_DailyClientLocatesCCT
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

if @SyncDateFrom='1900-01-01'  -- not filtering by status date, don't use ls table
  exec dbo.RPT_DCL_NoSyncRangeCCT @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory
else
  exec dbo.RPT_DCL_SyncRangeCCT @SyncDateFrom, @SyncDateTo, @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory

go
grant execute on dbo.RPT_DailyClientLocatesCCT to uqweb, QManagerRole
/*
exec dbo.RPT_DailyClientLocatesCCT '2003-04-14', '2003-04-21', '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 2, 0
exec dbo.RPT_DailyClientLocatesCCT '1900-01-01', '2199-01-01', '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0, 0

exec dbo.RPT_DCL_NoSyncRangeCCT '2006-10-02', '2006-10-03', 'BGAWA,BGAWC', 'Atlanta', 0, 0

exec dbo.RPT_DCL_SyncRangeCCT '2006-10-02', '2006-10-03', '2006-10-02', '2006-10-03', 'BGAWA,BGAWC', 'Atlanta', 0, 0

exec dbo.RPT_DailyClientLocatesCCT '2012-03-05', '2012-03-06', '2012-03-01', '2012-03-06', 'SCA,ADC,C10,C11,CC1,CC2,CC3,CC4,CC5,CC6,CC7,CC8,CC9,CTC,CCC,GSC,GSF,SCV,SUJ,TK6,WCC,CCF', 'NewJersey', 0, 0
*/
