if object_id('dbo.get_message_recip2') is not null
  drop procedure dbo.get_message_recip2
GO

create procedure get_message_recip2(@MessageID int)
as
  /* the message details */
  select 'message' as tname, *
  from message
  where message_id = @MessageID

  /* users that got it */
  select 'message_recip' as tname, message_recip.*
  from message_dest message_recip
  where message_recip.message_id = @MessageID

GO

grant execute on get_message_recip2 to uqweb, QManagerRole

/*
exec dbo.get_message_recip2 27003
*/
