if object_id('dbo.auto_ack_closed_emerg') is not null
  drop procedure dbo.auto_ack_closed_emerg
GO

create proc auto_ack_closed_emerg
as
set nocount on
update ticket_ack set has_ack=1
 from ticket
 where has_ack=0
  and ticket.ticket_id=ticket_ack.ticket_id
  and ticket.KIND = 'DONE'
GO

grant execute on auto_ack_closed_emerg to uqweb, QManagerRole

/*
exec dbo.auto_ack_closed_emerg
*/
