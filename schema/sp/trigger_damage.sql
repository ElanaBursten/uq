if object_id('dbo.damage_i') is not null
  drop trigger dbo.damage_i
GO

create trigger dbo.damage_i on damage
for insert
not for replication
as
  set nocount on
  if (select Coalesce(uq_damage_id, 0) from inserted) < 1
  begin
    declare @next_damage_id integer
    execute @next_damage_id = dbo.NextUniqueID 'NextDamageID'
    update damage set uq_damage_id = @next_damage_id, added_by = modified_by
     where (damage_id in (select damage_id from inserted))
  end
  else
    update damage set added_by = modified_by
     where damage_id in (select damage_id from inserted)
go

if object_id('dbo.damage_iu') is not null
  drop trigger dbo.damage_iu
GO

create trigger dbo.damage_iu on damage
for insert, update
not for replication
as
  set nocount on
  if SYSTEM_USER = 'no_trigger'
    return

  declare @damages table (damage_id int not null)

  /* Find damages that have changed in an important way */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.exc_resp_code <> ins.exc_resp_code) or
      (del.exc_resp_code is null and ins.exc_resp_code is not null) or
      (del.exc_resp_code is not null and ins.exc_resp_code is null) or
    (del.exc_resp_type <> ins.exc_resp_type) or
      (del.exc_resp_type is null and ins.exc_resp_type is not null) or
      (del.exc_resp_type is not null and ins.exc_resp_type is null) or
    (del.uq_resp_code <> ins.uq_resp_code) or
      (del.uq_resp_code is null and ins.uq_resp_code is not null) or
      (del.uq_resp_code is not null and ins.uq_resp_code is null) or
    (del.uq_resp_type <> ins.uq_resp_type) or
      (del.uq_resp_type is null and ins.uq_resp_type is not null) or
      (del.uq_resp_type is not null and ins.uq_resp_type is null) or
    (del.spc_resp_code <> ins.spc_resp_code) or
      (del.spc_resp_code is null and ins.spc_resp_code is not null) or
      (del.spc_resp_code is not null and ins.spc_resp_code is null) or
    (del.spc_resp_type <> ins.spc_resp_type) or
      (del.spc_resp_type is null and ins.spc_resp_type is not null) or
      (del.spc_resp_type is not null and ins.spc_resp_type is null) or
    (del.profit_center <> ins.profit_center) or
      (del.profit_center is null and ins.profit_center is not null) or
      (del.profit_center is not null and ins.profit_center is null) or
    (del.reason_changed <> ins.reason_changed) or
      (del.reason_changed is null and ins.reason_changed is not null) or
      (del.reason_changed is not null and ins.reason_changed is null)

  insert into damage_history
   (damage_id,
    modified_date, modified_by,
    exc_resp_code, exc_resp_type,
    uq_resp_code, uq_resp_type,
    spc_resp_code, spc_resp_type,
    profit_center, exc_resp_other_desc,
    exc_resp_details, exc_resp_response,
    uq_resp_other_desc, uq_resp_details,
    uq_resp_ess_step, spc_resp_details, reason_changed
   )
  select d.damage_id,
    getdate(), d.modified_by,
    d.exc_resp_code, d.exc_resp_type,
    d.uq_resp_code, d.uq_resp_type,
    d.spc_resp_code, d.spc_resp_type,
    d.profit_center, d.exc_resp_other_desc,
    damage.exc_resp_details, damage.exc_resp_response,
    d.uq_resp_other_desc, damage.uq_resp_details,
    d.uq_resp_ess_step, damage.spc_resp_details, ins.reason_changed
  from deleted d
       join damage on damage.damage_id = d.damage_id
       join inserted ins on d.damage_id = ins.damage_id
    where d.damage_id in (select damage_id from @damages)

  update damage set modified_date = getdate()
    where damage_id in (select damage_id from inserted)

  update damage set invoice_code_initial_value = invoice_code
    where damage_id in (select damage_id from inserted)
      and invoice_code_initial_value is null
      and invoice_code is not null

  delete from @damages

  /* Find damages with changed damage dates or ticket IDs */
  insert into @damages
  select ins.damage_id
  from inserted ins
    inner join deleted del on ins.damage_id = del.damage_id
  where
    (del.damage_date <> ins.damage_date) or
      (del.damage_date is null and ins.damage_date is not null) or
      (del.damage_date is not null and ins.damage_date is null) or
    (del.ticket_id <> ins.ticket_id) or
      (del.ticket_id is null and ins.ticket_id is not null) or
      (del.ticket_id is not null and ins.ticket_id is null)

  /* Also include any damages being inserted, since the deleted table is empty in that case */
  insert into @damages select damage_id from inserted where damage_id not in (select damage_id from deleted)

  update damage
  set accrual_date =
  dbo.MinOfFourDates(
    damage_date,
    uq_notified_date,
    coalesce((select t.due_date from ticket t where t.ticket_id = damage.ticket_id), '2100-01-01'),
    coalesce((select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N'), '2100-01-01')
  )
  where damage_id in (select damage_id from @damages)
go


/*
select count(*) from damage where accrual_date is null
select * from damage where accrual_date is null
update damage set office_id = 10 where accrual_date is null
update damage set ticket_id = 10 where accrual_date is null


select damage_id, damage_date, uq_notified_date from damage where accrual_date is null

-- RUN THIS ALL AS THE USER no_trigger

  update damage
  set accrual_date =
  dbo.MinOfFourDates(
    damage_date,
    uq_notified_date,
    (select t.due_date from ticket t where t.ticket_id = damage.ticket_id),
    (select min(l.closed_date) from locate l where l.ticket_id = damage.ticket_id and l.status <> '-N')
  )
  where damage_id in (select top 15000 damage_id from damage where accrual_date is null)


  update damage set uq_damage_id = damage_id where uq_damage_id is null
*/
