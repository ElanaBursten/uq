if object_id('dbo.RPT_damages_per_locates') is not null
  drop procedure dbo.RPT_damages_per_locates
go

create procedure dbo.RPT_damages_per_locates (
  @FromDate datetime,
  @ToDate datetime,
  @LocateRatioBase int,
  @LiabilityType int,
  @ProfitCenter varchar(15),
  @FacilityType varchar(15),
  @ClientID int
)
as
  set nocount on

  -- to be sure there is no divide by 0 possibility
  if (@LocateRatioBase < 1) begin
    RAISERROR ('LocateRatioBase must be greater than 0', 18, 1) with log
    return
  end

  -- map damage facility type to a utility type
  declare @UtilityType varchar(15)
  set @UtilityType = @FacilityType
  if @FacilityType = 'ELECTRIC' 
    set @UtilityType = 'elec'
  if @FacilityType = 'PHONE'
    set @UtilityType = 'phon'
  if @FacilityType = 'GAS'
    set @UtilityType = 'ugas'
  if @FacilityType = 'SEWER'
    set @UtilityType = 'sewr'
  if @FacilityType = 'WATER'
    set @UtilityType = 'watr'
  if @FacilityType = 'OTHER'
    set @UtilityType = 'fuel,pipl,sgnl'
  if @FacilityType = 'CATV'
    set @UtilityType = 'catv'
  if @FacilityType = 'GASELEC'
    set @UtilityType = 'ge'

  declare @results table (
    clientid int not null,
    client_name varchar(40) null,
    facility_type varchar(40) null,
    locator_id int not null,
    locator_last_name varchar(30) null,
    locator_name varchar(30) null,
    locate_count int not null default 0,
    damage_count int not null default 0,
    damage_rate decimal(10, 3) not null default 0
  )

  -- Insert count of locates performed by locator for selected Profit Center & date range
  insert into @results(clientid, client_name, facility_type, locator_id, locator_name, locator_last_name, locate_count)  
    select locate.client_id, client.client_name, reference.description, assigned_to_id, short_name, last_name, count(*) 
    from locate 
    inner join employee on emp_id = assigned_to_id
    inner join client on client.client_id=locate.client_id
    inner join office on office.office_id=client.office_id
    inner join reference on (reference.type='utility' and reference.code=client.utility_type)
    where locate.closed_date >= @FromDate 
    and locate.closed_date < @ToDate
    and office.profit_center = @ProfitCenter
    and (@ClientID = -1 or locate.client_id = @ClientID)
    and (@UtilityType = '' or client.utility_type in (@UtilityType))
    group by locate.client_id, client_name, description, assigned_to_id, short_name, last_name

  -- TODO: This query should change to use the new logic for determining a damage's locator (see the get_locator_for_damage function)

  -- Update count of damages locator was responsible for
  update @results set damage_count = (
    select count(*) from damage
    inner join locate on (locate.ticket_id=damage.ticket_id and locate.client_id=damage.client_id)
    where locate.assigned_to_id = locator_id
      and locate.client_id = clientid
      and damage.uq_notified_date >= @FromDate
      and damage.uq_notified_date < @ToDate
      and damage.profit_center = @ProfitCenter
      and (
      ((damage.uq_resp_code is not null) and @LiabilityType=0) or
      ((damage.uq_resp_code is null) and @LiabilityType=1) or
      ((damage.uq_resp_code is not null and damage.exc_resp_code is null) and @LiabilityType=2) or
      ((damage.uq_resp_code is not null and damage.exc_resp_code is not null) and @LiabilityType=3) or
      ((damage.uq_resp_code is null and damage.exc_resp_code is null) and @LiabilityType=4) or
      (@LiabilityType=5)
      )
  )

   
  -- Update the damage to locate rate based on selected ratio (locate_count will always be > 0)
  update @results set damage_rate = 
    damage_count / (locate_count / cast(@LocateRatioBase as real))

  -- Return the counts
  select * from @results 
    order by facility_type, client_name, damage_rate desc, locator_last_name

  -- Return the selected client details
  select client_id, oc_code, client_name from client
    where client_id = @ClientID

go

grant execute on RPT_damages_per_locates to uqweb, QManagerRole

/*
-- this one runs most often:
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 0, '370', '', -1

-- the other liability selection options:
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 1, '370', '', -1
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 2, '370', '', -1
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 3, '370', '', -1
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 4, '370', '', -1
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 5, '370', '', -1

-- intentional error on this one:
exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 0, 0, '370', '', -1

exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 10, 0, '370', '', -1

exec dbo.RPT_damages_per_locates '2006-01-01', '2006-12-31', 1000, 0, '370', 'gas', -1
*/
