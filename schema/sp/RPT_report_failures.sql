if object_id('dbo.RPT_report_failures') is not null
	drop procedure dbo.RPT_report_failures
go

create proc RPT_report_failures (@RepDate datetime)
as
  set nocount on 
  declare @EndDate datetime
  set @EndDate = dateadd(d, 1, @RepDate)

  select report_name,
   total,
   total_failures, (total_failures*100/total) as total_failure_percent,
   engine_failures, (engine_failures*100/total) as engine_failure_percent
  from (
   select rl.report_name,
    count(*) as total,
    (select count(*) from report_log rl2
      where rl2.start_date between @RepDate and @EndDate
       and rl2.report_name=rl.report_name
       and (client_success=0 or client_success is null)) as total_failures,
    (select count(*) from report_log rl2
      where rl2.start_date between @RepDate and @EndDate
       and rl2.report_name=rl.report_name
       and (engine_success=0 or engine_success is null)) as engine_failures
   from report_log rl
    where start_date between @RepDate and @EndDate
   group by report_name
  ) data
  order by total_failures desc

-- OVERALL report failure reasons:
select reason, count(*) as N
from
  (select report_name,
    case when engine_success=0 or engine_success is null
         then 'E: ' + coalesce(substring(engine_details, 1, 40),'')
         when client_success=0
         then 'C: ' + coalesce(client_details,'')
         else 'Engine Succeeded, never heard back from client'
      end as reason
   from report_log rl
   where start_date between @RepDate and @EndDate
     and (client_success=0 or client_success is null)
  ) data
group by reason
order by N desc

-- PER REPORT report failure reasons
select report_name, reason, count(*) as N
from
  (select report_name,
    case when engine_success=0 or engine_success is null
         then 'E: ' + substring(engine_details, 1, 40)
         when client_success=0
         then 'C: ' + client_details
         else 'Engine Succeeded, never heard back from client'
      end as reason
   from report_log rl
   where start_date between @RepDate and @EndDate
     and (client_success=0 or client_success is null)
  ) data
group by report_name, reason
order by report_name, N desc

go

grant execute on RPT_report_failures to uqweb, QManagerRole

/*
exec dbo.RPT_report_failures '2004-09-21'

*/


