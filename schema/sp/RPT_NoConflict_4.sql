if object_id('dbo.RPT_NoConflict_4') is not null
  drop procedure dbo.RPT_NoConflict_4
go

create procedure dbo.RPT_NoConflict_4 (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenter varchar(20),
  @Statuses   varchar(500),
  @State      varchar(20),
  @County     varchar(30)
)
as
  set nocount on

  select
    case
      when caller_fax IS NULL or caller_fax=''
       THEN con_name+caller
      ELSE caller_fax
    end as fax_name,
    ticket.ticket_number,
    locate.client_code,
    ticket.con_name,
    ticket.caller,
    ticket.caller_fax,
    ls.status,
    ls.status_date,
    ticket.work_state,
    ticket.work_county,
    ticket.work_city,
    ticket.work_address_number,
    ticket.work_address_number_2,
    ticket.work_address_street,
    client.client_name,
    statuslist.status_name,
    isnull(office.company_name, '') as company_name,
    isnull(office.address1, '') as address1,
    isnull(office.address2, '') as address2,
    isnull(office.city, '') as city,
    isnull(office.state, '') as state,
    isnull(office.zipcode, '') as zipcode,
    isnull(office.phone, '') as phone,
    isnull(office.fax, '') as fax,
    ltrim(isnull(ticket.work_address_number, '') + ' ' +
          isnull(ticket.work_address_number_2, '') + ' ' +
          isnull(ticket.work_address_street, '')) as street,
    (select top 1 status_message
     from fax_queue_rules fqr
     where
        fqr.status = ls.status
        and fqr.call_center = ticket.ticket_format
        and (fqr.client_code = locate.client_code or fqr.client_code = '*')
        and (fqr.state = ticket.work_state or fqr.state = '*') ) as status_message
  from locate_status ls with (INDEX(locate_status_insert_date))
    inner join locate on ls.locate_id=locate.locate_id
    inner join ticket on ticket.ticket_id = locate.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join statuslist on statuslist.status = locate.status
    left join office on office.office_id = client.office_id
  where
    (ls.insert_date between @StartDate and @EndDate) and
    (locate.active = 1) and
    (ls.status in (select S from dbo.StringListToTable(@Statuses))) and
    (ticket.ticket_format = @CallCenter) and
    ((ticket.work_state = @State) or (@State = '')) and
    ((ticket.work_county = @County) or (@County = ''))
  order by
    1, 2, 3
GO
grant execute on RPT_NoConflict_4 to uqweb, QManagerRole
/*
exec RPT_NoConflict_4 '2011-11-30 00:00:00', '2011-12-02 00:00:00', '1421', 'A,C,H,LPA,LPD,LPMA,LPMD,LPN,M,MCX,MFC,MFO,N,NC,NG,NL,NP,O,OH,OS,PC,PI,PM,S,SSM,SSNM,TB,TNC,UDT,VAC,WL,WP,XA,XB,XC,XCA,XD,XDU,XE,XF,XG,XH,XI,XJ,XK,XL,XLW,XM,XN,XP,XQ,XR,XXA,XXB,XXW,ZC,ZL,ZM,ZN,ZPC,ZPM,ZZZ', '', ''
*/

