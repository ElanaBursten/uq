if object_id('dbo.get_third_party2') is not null
  drop procedure dbo.get_third_party2
go

create proc get_third_party2(@ThirdPartyID int)
as
begin
  select 'damage_third_party' as tname, *
  from damage_third_party
  where third_party_id = @ThirdPartyID

  select 'damage' as tname, * 
  from damage
  where damage_id = 
    (select damage_id 
    from damage_third_party
    where third_party_id = @ThirdPartyID)
end
go

grant execute on get_third_party2 to uqweb, QManagerRole
go


