if object_id('dbo.work_order_notes') is not null
	drop view dbo.work_order_notes
go

create view work_order_notes
as
  -- work order notes:
  select w.wo_id, n.notes_id, n.entry_date, u.emp_id added_by_emp_id, n.note, n.foreign_id, 7 foreign_type, n.modified_date
  from work_order w
    inner join notes n with (index(notes_foreign)) on (n.foreign_id = w.wo_id and n.foreign_type = 7) 
    left join users u on n.uid = u.uid 
  where (n.active = 1)
  
go

/*
select * from work_order_notes where wo_id = 110000
*/
