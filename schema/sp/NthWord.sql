if exists (select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'NthWord')
  drop function NthWord
go

create function NthWord(@S varchar(8000), @WorkIndex int)
returns varchar(100)
as
begin
  declare @SBgn int
  declare @SEnd int
  declare @FoundCount int

  set @SBgn = 1  -- start looking at the beginning
  set @FoundCount = 0;
  while (@SBgn <= len(@S)) begin
    -- find the next space
    select @SEnd = CHARINDEX(' ', @S, @SBgn)
    select @FoundCount = @FoundCount + 1

    -- assume this is the result
    if @FoundCount = @WorkIndex begin
      if @SEnd = 0 
        return(substring(@S, @SBgn, len(@S) - @SBgn + 1))
      else 
        return(substring(@S, @SBgn, @SEnd-@SBgn))
    end

    if @SEnd = 0 
      return('')  -- nothing there

    -- resume looking after the space
    set @SBgn = @SEnd + 1;
  end
  return('')
end
go
/*
select dbo.NthWord('TESTING ONE TWO THREE', 0)
select dbo.NthWord('TESTING ONE TWO THREE', 1)
select dbo.NthWord('TESTING ONE TWO THREE', 2)
select dbo.NthWord('TESTING ONE TWO THREE', 3)
select dbo.NthWord('TESTING ONE TWO THREE', 4)
select dbo.NthWord('TESTING ONE TWO THREE', 5)
select dbo.NthWord('TESTING ONE TWO THREE', 6)
*/
