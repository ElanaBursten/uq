if exists(select name FROM sysobjects where name = 'locate_iu' AND type = 'TR')
  DROP TRIGGER locate_iu
GO

CREATE TRIGGER locate_iu ON locate
FOR INSERT, UPDATE
not for replication
AS
BEGIN
/* ********************** WARNING!!!! ******************************
   This is a very fragile part of the system, do not do anything with
   this trigger in the production DB without coordinating with the
   team, and having all of the front-end machines shut down to
   prevent any changes from slipping in while changing the trigger.
*/

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- exit if only the invoiced column was changed
  declare @invoiced_col int
  set @invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'invoiced')

  -- NOTE: "@invoiced_col % 8" would not work if the column number is 16, as it is
  -- for assigned_to, so it was changed to "- 8"
  if (@invoiced_col < 9) or (@invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- exit if the assigned_to column was changed
  -- and none of the initial fields changed
  declare @assignedto_col int
  set @assignedto_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to')

  if (@assignedto_col < 9) or (@assignedto_col > 16)
  begin
    rollback transaction
    RAISERROR('assigned_to has moved out of the second column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@assignedto_col - 8)-1)))
    return

  -- exit if the assigned_to_id column was changed
  -- and none of the initial fields changed
  declare @assignedtoid_col int
  set @assignedtoid_col = (select colid from syscolumns where id = OBJECT_ID('locate') and name = 'assigned_to_id')

  if (@assignedtoid_col < 17) or (@assignedto_col > 24)
  begin
    rollback transaction
    RAISERROR('assigned_to_id has moved out of the third column byte', 18, 1) with log
  end

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 3, 1) = power(2, ((@assignedtoid_col - 16)-1)))
    return

  -- if both assigned_to and assigned_to_id changed, either of the
  -- above will return

  -- set the ticket's kind
  DECLARE @ticket_id int
  SELECT @ticket_id = ticket_id FROM inserted

  EXEC dbo.update_ticket_status @ticket_id

  -- Update the modified_date when a locate changes
  update locate set
    modified_date = getdate()
  where locate_id in (select locate_id from inserted)

  -- When a locate is re-opened, refresh the assigned_to field
  update locate
    set assigned_to = (
      select top 1 assignment.locator_id
      from assignment
      where
        (assignment.locate_id = locate.locate_id) and
        (assignment.active = 1)
    )
  from deleted -- contains rows that were just updated, but before the updates were applied
    inner join inserted
      on inserted.locate_id = deleted.locate_id
  where locate.locate_id = deleted.locate_id
    and deleted.closed = 1
    and inserted.closed = 0

  -- Set assigned_to = NULL for all closed/non-active locates
  update locate
    set assigned_to = NULL
    where locate_id in (
      select locate_id
      from inserted
      where closed = 1
       or status = '-P'
       or status = '-N'
       or active = 0
    )

  -- Add response for a changed status not already queued
  insert into responder_queue (locate_id, ticket_format, client_code)
  select distinct i.locate_id, t.ticket_format, i.client_code
    from inserted i
      inner join ticket t on i.ticket_id=t.ticket_id
      left outer join responder_queue rq on i.locate_id=rq.locate_id
    where i.status <> '-N'
     and i.status <> '-R'
     and i.status <> '-P'
     and rq.locate_id is null 
     and (i.status <> (select top 1 ls.status from locate_status ls where ls.locate_id = i.locate_id order by insert_date desc)
      or i.status is not null and not exists (select ls.locate_id from locate_status ls where ls.locate_id = i.locate_id))

  -- 1849: update locate_snap fields
  -- Locates that are closed when they are added won't have a locate_snap.
  -- Add those here so the following update locate_snap will find them.
  insert into locate_snap (locate_id, first_receive_date, first_receive_pc)
    select i.locate_id, GetDate(), dbo.get_historical_pc(i.closed_by_id, GetDate())
      from inserted i 
        left join assignment a on i.locate_id = a.locate_id
        left join locate_snap ls on i.locate_id = ls.locate_id
      where a.locate_id is null and ls.locate_id is null 
        and i.closed = 1 and i.active = 1

  update locate_snap
    set first_close_date = i.closed_date, 
    first_close_emp_id = i.closed_by_id,  
    first_close_pc = dbo.get_historical_pc(i.closed_by_id, i.closed_date)
  from inserted i 
  where i.closed = 1 and i.active = 1 and i.locate_id = locate_snap.locate_id
    and first_close_date is null
  -- end 1849
  
  update locate_snap --update work_status and location_status every time locate is closed (not just first time closed)
    set work_status = dbo.get_locate_work_status(i.status),
    location_status = dbo.get_locate_location_status(i.locate_id, i.ticket_id, i.closed_by_id)
  from inserted i 
  where i.closed = 1 and i.active = 1 and i.locate_id = locate_snap.locate_id    
  
  -- Do this insert last because StatusTicketsLocatesInternal needs this identity    
  insert into locate_status (
    locate_id, status_date, status, high_profile, high_profile_reason,
    qty_marked, statused_by, statused_how, regular_hours, overtime_hours, 
    mark_type, entry_date, gps_id, status_changed_by_id)
  select
    locate_id, closed_date, status, high_profile, high_profile_reason,
    qty_marked, closed_by_id, closed_how, regular_hours, overtime_hours, 
    mark_type, entry_date, gps_id, status_changed_by_id
  from inserted
  where status <> '-N'
   and status <> '-P'
END
go


if exists(select name FROM sysobjects where name = 'locate_i' AND type = 'TR')
  DROP TRIGGER locate_i
GO

CREATE TRIGGER locate_i ON locate
FOR INSERT
not for replication
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  
  if (exists(select count(*) from locate l
    where l.ticket_id in (select ticket_id from inserted)
    group by l.ticket_id, client_code
    having count(*) > 1))
  begin
    rollback transaction
    RAISERROR('A duplicate client can not be added to a locate.  Please sync again in a few minutes.', 18, 1) with log
    return
  end

END
go

/*
begin transaction
print 'select'
select * from locate where ticket_id in (686259) order by ticket_id, locate_id
print 'insert'
insert into locate (ticket_id, client_code, client_id, status)
values (686259, 'KO', 336, '-N')
print 'check'
if @@ERROR <> 0
begin
  print 'rollback'
  rollback transaction
  print 'return'
  return
end
print 'select 2'
select * from locate where ticket_id in (686259) order by ticket_id, locate_id
print 'commit'
commit transaction
print 'done'

delete from locate where locate_id = 188199518

select * from client where oc_code = 'AGL101'

insert into locate (ticket_id, client_code, client_id, status)
values (1001, 'AGL101', 2727, '-N')
*/
