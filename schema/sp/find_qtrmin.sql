if object_id('dbo.find_qtrmin') is not null
  drop procedure dbo.find_qtrmin
GO

CREATE Procedure find_qtrmin
	(
	@LatCode varchar(20),
	@LongCode varchar(20)
	)
As

select area.area_name, area.area_id, employee.emp_id, employee.short_name
 from map_qtrmin
  inner join area (NOLOCK) on map_qtrmin.area_id = area.area_id
  inner join employee (NOLOCK) on area.locator_id = employee.emp_id
 where map_qtrmin.qtrmin_lat = @LatCode
  and map_qtrmin.qtrmin_long=@LongCode
  and employee.active = 1
  and employee.can_receive_tickets = 1
GO

grant execute on find_qtrmin to uqweb, QManagerRole
GO

