/****** Object:  StoredProcedure [dbo].[RPT_ClientAttachments]    Script Date: 8/16/2019 4:53:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[RPT_ClientAttachments] (
  @CallCenters varchar(2000),
  @ClientIDs varchar(2000),
  @AttachUploadStartDate datetime,
  @AttachUploadEndDate datetime,
  @ForeignTypes varchar(20),
  @IncludeFileTypes varchar(2000),
  @ExcludeFileNames varchar(2000)
)
as begin
  set NOCOUNT on

  declare @ClientAttachment table (
    attachment_id int not null primary key,
    ticket_number varchar(20) not null,
    serial_number varchar(40) null,
    filename varchar(100) not null,
    orig_filename varchar(100) not null,
    upload_date datetime not null)

  insert into @ClientAttachment 
  select distinct a.attachment_id, t.ticket_number, t.serial_number, a.filename, a.orig_filename, a.upload_date
    from ticket t
    join locate l on l.ticket_id = t.ticket_id 
    join attachment a on (a.foreign_id = t.ticket_id and a.foreign_type = 1)
    where t.ticket_format in (select S from dbo.StringListToTable(@CallCenters))
      and l.client_id in (select ID from dbo.IDListToTable(@ClientIDs))
      and a.foreign_type in (select ID from dbo.IDListToTable(@ForeignTypes))
      and a.upload_date between @AttachUploadStartDate and @AttachUploadEndDate
      and a.extension in (select S from dbo.StringListToTable(@IncludeFileTypes))
      and a.active = 1
      and t.serial_number <> ''
  union
  select distinct a.attachment_id, w.wo_number, w.client_wo_number, a.filename, a.orig_filename, a.upload_date
    from work_order w
    join attachment a on (a.foreign_id = w.wo_id and a.foreign_type = 7)
    where w.wo_source in (select S from dbo.StringListToTable(@CallCenters))
      and w.client_id in (select ID from dbo.IDListToTable(@ClientIDs))
      and a.foreign_type in (select ID from dbo.IDListToTable(@ForeignTypes))
      and a.upload_date between @AttachUploadStartDate and @AttachUploadEndDate
      and a.extension in (select S from dbo.StringListToTable(@IncludeFileTypes))
      and a.active = 1
  -- Add more unions here to get other types of attachments
       
  declare @Exc varchar(50)
  declare ExcludeCursor cursor for
    select S from dbo.StringListToTable(@ExcludeFileNames)
    
  open ExcludeCursor
  fetch next from ExcludeCursor into @Exc

  while @@fetch_status=0 begin
    delete from @ClientAttachment where orig_filename like LTRIM(@Exc)
    fetch next from ExcludeCursor into @Exc
  end

  close ExcludeCursor
  deallocate ExcludeCursor

  -- return all non-excluded attachments for inclusion in the tsv file exported from this sp:
  select ticket_number, serial_number, filename, orig_filename, upload_date, attachment_id
  from @ClientAttachment
  Order by attachment_id  --  QMANTWO-596 
end

GO