
if object_id('dbo.RPT_tse_summary_emp') is not null
	drop procedure dbo.RPT_tse_summary_emp
go

create proc RPT_tse_summary_emp(
	@EmpID Int,
	@StartDate datetime,
	@EndDate datetime
)
as
 set nocount on

 -- the timesheet data
 select e.first_name as first_name,
    e.last_name as last_name,
    e.emp_number as emp_number,
    e.short_name as short_name,
    e.emp_id as emp_id,
    dd.d as display_work_date,
    DateAdd(day, -1 * datepart(dw, dd.d) + 7, dd.d) as week_end,
    tse.*,
    Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0) as mileage,
    (case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday,
	(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt,
    reference.modifier as rule_abbrev,
    ent_emp.emp_number as ent_emp_number,
    ent_emp.short_name as ent_emp_short_name,
    app_emp.emp_number as app_emp_number,
    app_emp.short_name as app_emp_short_name,
    fap_emp.emp_number as fap_emp_number,
    fap_emp.short_name as fap_emp_short_name,
    case when final_approve_by is not null then 'FINAL APPROVED'
         when approve_by is not null       then 'APPROVED'
         when entry_by is not null         then 'ENTERED'
         else                                   '-'
    end as day_status
  from 
    dbo.employee e
    inner join dbo.DateSpan(@StartDate, @EndDate) dd on 1=1
    left join timesheet_entry tse on tse.work_emp_id = e.emp_id and tse.work_date=dd.d
       and (tse.status='ACTIVE' or tse.status='SUBMIT') and (tse.work_date between @StartDate and @EndDate)
    left join employee ent_emp on tse.entry_by = ent_emp.emp_id
    left join employee app_emp on tse.approve_by = app_emp.emp_id
    left join employee fap_emp on tse.final_approve_by = fap_emp.emp_id
    left join reference on reference.ref_id = e.timerule_id
  where e.emp_id = @EmpId 
  order by dd.d

  -- totals for the emp
  select
   min(work_emp_id) as emp_id,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,
   sum(reg_hours+ot_hours+dt_hours) as working_hours,
   sum( case when vehicle_use='POV' then reg_hours+ot_hours+dt_hours else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%' then reg_hours+ot_hours+dt_hours else null end) as cov_hours,
   sum(Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0)) as mileage,
   sum(case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday,
   sum(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt
  from timesheet_entry tse 
  where tse.work_emp_id = @EmpId
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')

  -- summary/totals by day of week
  select datepart(weekday, tse.work_date) as dow,
   min(tse.work_date) as dow_date,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,
   sum(Coalesce(tse.miles_stop1, 0) - Coalesce(tse.miles_start1, 0)) as mileage,
   sum(case when tse.floating_holiday = 1 then 1 else 0 end) as floating_holiday,
   sum(case when tse.per_diem = 1 then 45 else 0 end) as per_diem_amt
  from timesheet_entry tse 
  where tse.work_emp_id = @EmpId 
    and tse.work_date between @StartDate and @EndDate
    and (tse.status='ACTIVE' or tse.status='SUBMIT')
  group by datepart(weekday, tse.work_date)
  order by 1

GO

grant execute on RPT_tse_summary_emp to uqweb, QManagerRole

/*
exec dbo.RPT_tse_summary_emp 2676, '2004-01-01', '2007-06-01'
exec dbo.RPT_tse_summary_emp 3510, '2004-01-01', '2007-06-01'
exec dbo.RPT_tse_summary_emp 3510, '2007-01-01', '2007-06-01'
exec dbo.RPT_tse_summary_emp 513, '2004-01-01', '2007-06-01'
*/

