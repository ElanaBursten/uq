USE [QM]
GO
DROP PROCEDURE [dbo].[sync_6]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[sync_6] ( --CHG0055676         -- adds Service Card URL 
  @EmployeeID int, --CHG0055676
  @UpdatesSinceS varchar(30), --CHG0055676
  @ClientTicketListS text, --CHG0055676
  @ClientDamageListS varchar(MAX), --CHG0055676
  @ClientWorkOrderListS varchar(MAX) --CHG0055676
) --CHG0055676
as --CHG0055676
  set nocount on --CHG0055676
 --CHG0055676
  --Work orders temp tables --CHG0055676
  DECLARE @workorders TABLE (wo_id integer not null primary key) --CHG0055676
  DECLARE @mod_workorders TABLE (wo_id integer not null primary key) --CHG0055676
  DECLARE @client_has_workorders TABLE (client_wo_id integer not null primary key) --CHG0055676
  DECLARE @should_have_workorders TABLE (should_wo_id integer not null primary key) --CHG0055676
  DECLARE @related_tickets TABLE (ticket_id integer not null primary key) --CHG0055676
 --CHG0055676
  --Damages temp tables --CHG0055676
  DECLARE @damages TABLE (damage_id integer not null primary key) --CHG0055676
  DECLARE @mod_damages TABLE (damage_id integer not null primary key) --CHG0055676
  DECLARE @client_has_damages TABLE (client_damage_id integer not null primary key) --CHG0055676
  DECLARE @should_have_damages TABLE (should_damage_id integer not null primary key) --CHG0055676
 --CHG0055676
  DECLARE @tix TABLE (ticket_id integer not null primary key) --CHG0055676
  DECLARE @modtix TABLE (ticket_id integer not null primary key) --CHG0055676
  DECLARE @closed_since datetime --CHG0055676
  DECLARE @oldest_l int --CHG0055676
  DECLARE @UpdatesSince datetime --CHG0055676
 --CHG0055676
  DECLARE @client_has_tix TABLE (client_ticket_id integer not null primary key) --CHG0055676
  DECLARE @should_have_tix TABLE (should_ticket_id integer not null primary key) --CHG0055676
  DECLARE @current_pc varchar(15) --CHG0055676
 --CHG0055676
  set @UpdatesSince = convert(datetime, @UpdatesSinceS) --CHG0055676
 --CHG0055676
  -- Go back 2 minutes, to reduce update overlap errors --CHG0055676
  set @UpdatesSince = dateadd(s, -120, @UpdatesSince) --CHG0055676
 --CHG0055676
/* Use this in emergency situations to disable initial syncs --CHG0055676
  if @UpdatesSince < '2000-01-01' --CHG0055676
  begin --CHG0055676
    RAISERROR ('Initial syncs temporarily disabled', 50005, 1) with log --CHG0055676
    return --CHG0055676
  end --CHG0055676
*/ --CHG0055676
 --CHG0055676
  select 'row' as tname, getdate() as SyncDateTime --CHG0055676
 --CHG0055676
  select 'office' as tname, * from office --CHG0055676
    where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'call_center' as tname, call_center.cc_code, cc_name, active, track_arrivals, --CHG0055676
   xml_ticket_format, coalesce(call_center_contact.phone, '') as alert_phone_no, allowed_work --CHG0055676
   from call_center --CHG0055676
   left join call_center_contact on call_center.cc_code = call_center_contact.cc_code --CHG0055676
    and call_center_contact.name='ALERT' --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'statuslist' as tname, * from statuslist where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'reference' as tname, * from reference where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'profit_center' as tname, * from profit_center where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'customer' as tname, * from customer where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'center_group' as tname, * from center_group where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'center_group_detail' as tname, * from center_group_detail where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'client' as tname, * from client where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'configuration_data' as tname, * from configuration_data --CHG0055676
  where (modified_date > @UpdatesSince) --CHG0055676
    and ((Name in ('ClientURL', 'TicketActRefreshSeconds', 'TicketChangeViewLimitsMax', --CHG0055676
    'UpdateFileName', 'UpdateFileHash', 'QMClientHttpPort', 'PTOHoursEntryStartDate', 'CenBaseUrl', 'EPR_Pepper', 'RemBaseURL', --CHG0055676
	'override_units', 'SCANA_BaseURL', 'INR_BaseURL', 'JobSafetyURL','JSASafetyTktURL', 'OH_ServiceCard'))  --QMANTWO-434 QMANTWO-591/QMANTWO-753 --CHG0055676
    or (Name like 'AttachWarning%')) --CHG0055676
 --CHG0055676
  select 'api_storage_constants' as tname, * from api_storage_constants-- where (modified_date > @UpdatesSince) --CHG0055676
  select 'api_storage_credentials' as tname, * from api_storage_credentials-- where (modified_date > @UpdatesSince) --CHG0055676
 --CHG0055676
  select 'highlight_rule' as tname, * from highlight_rule where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'employee' as tname, emp_id, type_id, status_id, timesheet_id, --CHG0055676
    emp_number, short_name, report_to, active, company_car, timerule_id, --CHG0055676
    repr_pc_code, payroll_pc_code, company_id, ticket_view_limit, show_future_tickets,  --CHG0055676
    contact_phone, local_utc_bias --CHG0055676
   from employee where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, login_id, chg_pwd, --CHG0055676
    last_login, active_ind, can_view_notes, can_view_history, chg_pwd_date, api_key --CHG0055676
   from users where emp_id = @EmployeeID  -- Myself only --CHG0055676
 --CHG0055676
  select 'users' as tname, uid, grp_id, emp_id, first_name, last_name, active_ind --CHG0055676
   from users where emp_id <> @EmployeeID -- Everyone else --CHG0055676
   and  modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'locating_company' as tname, * from locating_company --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'carrier' as tname, * from carrier --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'followup_ticket_type' as tname, * from followup_ticket_type --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  set @current_pc = dbo.get_historical_pc(@EmployeeID, GetDate()) --CHG0055676
  if @current_pc is null set @current_pc = dbo.get_employee_pc(@EmployeeID, 0) --CHG0055676
 --CHG0055676
  select 'employee' as tname, emp_id, charge_cov, timerule_id, --CHG0055676
    timesheet_id, company_car, effective_pc = @current_pc, --CHG0055676
    first_name, last_name --CHG0055676
  from employee --CHG0055676
  where (emp_id = @EmployeeID) --CHG0055676
    and (modified_date > @UpdatesSince or dbo.get_historical_pc(emp_id, @UpdatesSince) <> @current_pc) --CHG0055676
 --CHG0055676
  select 'employee_right' as tname, * from employee_right --CHG0055676
    where emp_id=@EmployeeID and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  if ((select rights_modified_date from employee where emp_id = @EmployeeID) > @UpdatesSince) --CHG0055676
    select 'emp_group_right' as tname, * from dbo.get_emp_group_rights(@EmployeeID) --CHG0055676
 --CHG0055676
  select 'timesheet_entry' as tname, * --CHG0055676
  from timesheet_entry --CHG0055676
  where --CHG0055676
    work_emp_id = @EmployeeID and --CHG0055676
    (((modified_date > @UpdatesSince) and (work_date > dateadd(Day, -38, getdate()))) --CHG0055676
    or --CHG0055676
    ((work_date > dateadd(year,datediff(year,0,getdate()),0)) and (floating_holiday = 1))) --CHG0055676
 --CHG0055676
  select 'asset_type' as tname, * from asset_type --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'asset_assignment' as tname, * from asset_assignment --CHG0055676
  where emp_id = @EmployeeID --CHG0055676
    and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'asset' as asset, * from asset --CHG0055676
  where asset_id in (select asset_id from asset_assignment where emp_id = @EmployeeID) --CHG0055676
    and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'upload_location' as tname, * from upload_location --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'upload_file_type' as tname, * from upload_file_type --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'damage_default_est' as tname, * from damage_default_est --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'status_group' as tname, * from status_group --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'status_group_item' as tname, * from status_group_item --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'call_center_hp' as tname, * from call_center_hp --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'billing_unit_conversion' as tname, * from billing_unit_conversion --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'billing_gl' as tname, * from billing_gl --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'area' as tname, * from area --CHG0055676
  where map_id = (select value from configuration_data where name = 'AssignableAreaMapID') --CHG0055676
    and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'billing_output_config' as tname, output_config_id, customer_id, which_invoice --CHG0055676
  from billing_output_config where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'required_document' as tname, * from required_document --CHG0055676
    where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'required_document_type' as tname, * from required_document_type --CHG0055676
    where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Ticket-related data --CHG0055676
  select @closed_since = dateadd(day, -5, getdate()) --CHG0055676
 --CHG0055676
  select @oldest_l = (select top 1 oldest_open_locate from oldest_open) --CHG0055676
 --CHG0055676
  -- all open locates, so client can detect it has missing locates --CHG0055676
  select 'open_locate' as tname, ticket_id, locate_id, status --CHG0055676
   from locate open_locate --CHG0055676
   where assigned_to=@EmployeeID --CHG0055676
 --CHG0055676
  -- all open tickets, so we can be sure to send them all --CHG0055676
  insert into @should_have_tix --CHG0055676
  select distinct ticket_id --CHG0055676
   from locate --CHG0055676
   where assigned_to=@EmployeeID --CHG0055676
 --CHG0055676
  -- what the client does have --CHG0055676
  insert into @client_has_tix --CHG0055676
  select ID from IdListToTable(@ClientTicketListS) --CHG0055676
 --CHG0055676
  -- DISABLE the sync of "my old tickets": --CHG0055676
  if (1=0) and (@@ROWCOUNT<1 or @UpdatesSince < '2000-01-01')  begin    -- support old clients and init syncs --CHG0055676
    if @UpdatesSince < '2000-01-01'  begin --CHG0055676
      -- Get all tickets assigned to me even if closed --CHG0055676
      insert into @tix  (ticket_id) --CHG0055676
      select locate.ticket_id --CHG0055676
       from locate  where locate.assigned_to_id=@EmployeeID --CHG0055676
            and locate.closed_date> @closed_since --CHG0055676
      union --CHG0055676
      select locate.ticket_id --CHG0055676
       from locate  where locate.assigned_to_id=@EmployeeID --CHG0055676
            and locate.closed_date is null --CHG0055676
      union --CHG0055676
      select should_ticket_id from @should_have_tix --CHG0055676
    end else begin --CHG0055676
      -- This is a subsequent sync with the client tic list missing, --CHG0055676
      -- must use old alg to get consistent results: --CHG0055676
      -- Get all tickets ever assigned to me, this is the SLOW part --CHG0055676
      insert into @tix  (ticket_id) --CHG0055676
      select distinct locate.ticket_id --CHG0055676
       from assignment --CHG0055676
       inner join locate on locate.locate_id=assignment.locate_id --CHG0055676
          and (locate.closed=0 or locate.closed_date> @closed_since) --CHG0055676
       where assignment.locator_id = @EmployeeID --CHG0055676
        and assignment.locate_id >= @oldest_l --CHG0055676
    end --CHG0055676
  end else begin  -- newer clients tell us what they have --CHG0055676
    -- they need to know about updates to tickets they are --CHG0055676
    -- assigned now, and also tickets still in cache --CHG0055676
    insert into @tix  (ticket_id) --CHG0055676
      select should_ticket_id from @should_have_tix --CHG0055676
      union --CHG0055676
      select client_ticket_id from @client_has_tix --CHG0055676
  end --CHG0055676
 --CHG0055676
  -- subtract what we have, from what we should have --CHG0055676
  delete from @should_have_tix where should_ticket_id in --CHG0055676
    (select client_ticket_id from @client_has_tix) --CHG0055676
 --CHG0055676
  -- Find those that have changed, somewhat slow --CHG0055676
  insert into @modtix (ticket_id) --CHG0055676
  select t.ticket_id --CHG0055676
   from @tix t --CHG0055676
    inner join ticket on ticket.ticket_id = t.ticket_id --CHG0055676
    and ticket.modified_date >@UpdatesSince --CHG0055676
  union --CHG0055676
  select locate.ticket_id --CHG0055676
   from @tix t2 --CHG0055676
    inner join locate on locate.ticket_id = t2.ticket_id --CHG0055676
   where locate.modified_date > @UpdatesSince --CHG0055676
    and locate.status<>'-N' --CHG0055676
  union --CHG0055676
  select locate.ticket_id --CHG0055676
   from @tix t3 --CHG0055676
    inner join locate on locate.ticket_id = t3.ticket_id --CHG0055676
    inner join assignment on locate.locate_id=assignment.locate_id --CHG0055676
   where assignment.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select notes.foreign_id --CHG0055676
   from @tix t4 --CHG0055676
    inner join notes on notes.foreign_id = t4.ticket_id --CHG0055676
   where notes.foreign_type = 1 --CHG0055676
    and notes.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select attachment.foreign_id --CHG0055676
   from @tix t5 --CHG0055676
    inner join attachment on attachment.foreign_id = t5.ticket_id --CHG0055676
   where attachment.foreign_type = 1 --CHG0055676
    and attachment.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
    select should_ticket_id from @should_have_tix --CHG0055676
  union --CHG0055676
  select locate.ticket_id --CHG0055676
  from @tix t6 --CHG0055676
    inner join locate on locate.ticket_id = t6.ticket_id  --CHG0055676
    inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes --CHG0055676
  where  --CHG0055676
    notes.modified_date > @UpdatesSince --CHG0055676
  union  --CHG0055676
  select ts.ticket_id --CHG0055676
    from @tix t7 --CHG0055676
    inner join task_schedule ts on ts.ticket_id = t7.ticket_id --CHG0055676
    where ts.modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- now get the whole ticket for just the ones that have changed --CHG0055676
  declare @tickets_to_send table ( --CHG0055676
  [ticket_id] [int] NOT NULL , --CHG0055676
  [ticket_number] [varchar] (20) NOT NULL , --CHG0055676
  [parsed_ok] [bit] NOT NULL , --CHG0055676
  [ticket_format] [varchar] (20) NOT NULL , --CHG0055676
  [kind] [varchar] (20) NOT NULL , --CHG0055676
  [status] [varchar] (20) NULL , --CHG0055676
  [map_page] [varchar] (20) NULL , --CHG0055676
  [revision] [varchar] (20) NOT NULL , --CHG0055676
  [transmit_date] [datetime] NULL , --CHG0055676
  [due_date] [datetime] NULL , --CHG0055676
  [work_description] [varchar] (3500) NULL , --CHG0055676
  [work_state] [varchar] (2) NULL , --CHG0055676
  [work_county] [varchar] (40) NULL , --CHG0055676
  [work_city] [varchar] (40) NULL , --CHG0055676
  [work_address_number] [varchar] (10) NULL , --CHG0055676
  [work_address_number_2] [varchar] (10) NULL , --CHG0055676
  [work_address_street] [varchar] (60) NULL , --CHG0055676
  [work_cross] [varchar] (100) NULL , --CHG0055676
  [work_subdivision] [varchar] (70) NULL , --CHG0055676
  [work_type] [varchar] (90) NULL , --CHG0055676
  [work_date] [datetime] NULL , --CHG0055676
  [work_notc] [varchar] (40) NULL , --CHG0055676
  [work_remarks] [varchar] (1200) NULL , --CHG0055676
  [priority] [varchar] (40) NULL , --CHG0055676
  [legal_date] [datetime] NULL , --CHG0055676
  [legal_good_thru] [datetime] NULL , --CHG0055676
  [legal_restake] [varchar] (40) NULL , --CHG0055676
  [respond_date] [datetime] NULL , --CHG0055676
  [duration] [varchar] (40) NULL , --CHG0055676
  [company] [varchar] (80) NULL , --CHG0055676
  [con_type] [varchar] (50) NULL , --CHG0055676
  [con_name] [varchar] (50) NULL , --CHG0055676
  [con_address] [varchar] (50) NULL , --CHG0055676
  [con_city] [varchar] (40) NULL , --CHG0055676
  [con_state] [varchar] (40) NULL , --CHG0055676
  [con_zip] [varchar] (40) NULL , --CHG0055676
  [call_date] [datetime] NULL , --CHG0055676
  [caller] [varchar] (50) NULL , --CHG0055676
  [caller_contact] [varchar] (50) NULL , --CHG0055676
  [caller_phone] [varchar] (40) NULL , --CHG0055676
  [caller_cellular] [varchar] (40) NULL , --CHG0055676
  [caller_fax] [varchar] (40) NULL , --CHG0055676
  [caller_altcontact] [varchar] (40) NULL , --CHG0055676
  [caller_altphone] [varchar] (40) NULL , --CHG0055676
  [caller_email] [varchar] (40) NULL , --CHG0055676
  [operator] [varchar] (40) NULL , --CHG0055676
  [channel] [varchar] (40) NULL , --CHG0055676
  [work_lat] [decimal](9, 6) NULL , --CHG0055676
  [work_long] [decimal](9, 6) NULL , --CHG0055676
  [geocode_precision] [int] NULL , --CHG0055676
  [image] [text] NOT NULL , --CHG0055676
  [parse_errors] [text] NULL , --CHG0055676
  [modified_date] [datetime] NOT NULL, --CHG0055676
  [active] [bit] NOT NULL, --CHG0055676
  [ticket_type] [varchar] (38) NULL , --CHG0055676
  [legal_due_date] [datetime] NULL , --CHG0055676
  [parent_ticket_id] [int] NULL , --CHG0055676
  [do_not_mark_before] [datetime] NULL , --CHG0055676
  [route_area_id] [int] NULL , --CHG0055676
  [watch_and_protect] [bit] NULL, --CHG0055676
  [service_area_code] [varchar] (40) NULL , --CHG0055676
  [explosives] [varchar] (20) NULL , --CHG0055676
  [serial_number] [varchar] (40) NULL , --CHG0055676
  [map_ref] [varchar] (60) NULL , --CHG0055676
  [followup_type_id] [int] NULL , --CHG0055676
  [do_not_respond_before] [datetime] NULL , --CHG0055676
  [recv_manager_id] [int] NULL , --CHG0055676
  [ward] [varchar] (10) NULL , --CHG0055676
  route_area_name varchar(50) NULL, --CHG0055676
  alert varchar(1) NULL, --CHG0055676
  start_date datetime NULL, --CHG0055676
  end_date datetime NULL, --CHG0055676
  points decimal(6,2) NULL, --CHG0055676
  work_priority_id int NULL, --CHG0055676
  wo_number varChar(40) NULL    -- QMANTWO-253 --CHG0055676
  ) --CHG0055676
 --CHG0055676
  declare @locates_to_send table ( --CHG0055676
  [locate_id] [int] NOT NULL , --CHG0055676
  [ticket_id] [int] NOT NULL , --CHG0055676
  [client_code] [varchar] (10) NULL , --CHG0055676
  [client_id] [int] NULL , --CHG0055676
  [status] [varchar] (5) NOT NULL , --CHG0055676
  [high_profile] [bit] NOT NULL , --CHG0055676
  [qty_marked] [int] NULL , --CHG0055676
  [price] [money] NULL , --CHG0055676
  [closed] [bit] NOT NULL , --CHG0055676
  [closed_by_id] [int] NULL , --CHG0055676
  [closed_how] [varchar] (10) NULL , --CHG0055676
  [closed_date] [datetime] NULL , --CHG0055676
  [modified_date] [datetime] NOT NULL , --CHG0055676
  [active] [bit] NOT NULL, --CHG0055676
  [invoiced] [bit] NULL , --CHG0055676
  [assigned_to] [int] NULL , --CHG0055676
  [regular_hours] [decimal](5, 2) NULL , --CHG0055676
  [overtime_hours] [decimal](5, 2) NULL , --CHG0055676
  [added_by] [varchar] (8) NULL , --CHG0055676
  [watch_and_protect] [bit] NULL , --CHG0055676
  [high_profile_reason] [int] NULL , --CHG0055676
  [seq_number] [varchar] (20) NULL , --CHG0055676
  [assigned_to_id] [int] NULL , --CHG0055676
  [mark_type] [varchar] (10) NULL , --CHG0055676
  [alert] varchar(1) NULL, --CHG0055676
  [locator_id] int, --CHG0055676
  length_total int, --CHG0055676
  entry_date datetime,  --CHG0055676
  gps_id int, --CHG0055676
  status_changed_by_id int, --CHG0055676
  workload_date datetime NULL --CHG0055676
  ) --CHG0055676
 --CHG0055676
  -- gather the ticket and locate data, to a table var, so these queries complete --CHG0055676
  -- right away (without NOLOCK) --CHG0055676
  insert into @tickets_to_send --CHG0055676
  select --CHG0055676
     ticket.ticket_id, --CHG0055676
     ticket.ticket_number, --CHG0055676
     ticket.parsed_ok, --CHG0055676
     ticket.ticket_format, --CHG0055676
     ticket.kind, --CHG0055676
     ticket.status, --CHG0055676
     ticket.map_page, --CHG0055676
     ticket.revision, --CHG0055676
     ticket.transmit_date, --CHG0055676
     ticket.due_date, --CHG0055676
     ticket.work_description, --CHG0055676
     ticket.work_state, --CHG0055676
     ticket.work_county, --CHG0055676
     ticket.work_city, --CHG0055676
     ticket.work_address_number, --CHG0055676
     ticket.work_address_number_2, --CHG0055676
     ticket.work_address_street, --CHG0055676
     ticket.work_cross, --CHG0055676
     ticket.work_subdivision, --CHG0055676
     ticket.work_type, --CHG0055676
     ticket.work_date, --CHG0055676
     ticket.work_notc, --CHG0055676
     ticket.work_remarks, --CHG0055676
     ticket.priority, --CHG0055676
     ticket.legal_date, --CHG0055676
     ticket.legal_good_thru, --CHG0055676
     ticket.legal_restake, --CHG0055676
     ticket.respond_date, --CHG0055676
     ticket.duration, --CHG0055676
     ticket.company, --CHG0055676
     ticket.con_type, --CHG0055676
     ticket.con_name, --CHG0055676
     ticket.con_address, --CHG0055676
     ticket.con_city, --CHG0055676
     ticket.con_state, --CHG0055676
     ticket.con_zip, --CHG0055676
     ticket.call_date, --CHG0055676
     ticket.caller, --CHG0055676
     ticket.caller_contact, --CHG0055676
     ticket.caller_phone, --CHG0055676
     ticket.caller_cellular, --CHG0055676
     ticket.caller_fax, --CHG0055676
     ticket.caller_altcontact, --CHG0055676
     ticket.caller_altphone, --CHG0055676
     ticket.caller_email, --CHG0055676
     ticket.operator, --CHG0055676
     ticket.channel, --CHG0055676
     ticket.work_lat, --CHG0055676
     ticket.work_long, --CHG0055676
     ticket.geocode_precision, --CHG0055676
     ticket.image, --CHG0055676
     ticket.parse_errors, --CHG0055676
     ticket.modified_date, --CHG0055676
     ticket.active, --CHG0055676
     ticket.ticket_type, --CHG0055676
     ticket.legal_due_date, --CHG0055676
     ticket.parent_ticket_id, --CHG0055676
     ticket.do_not_mark_before, --CHG0055676
     ticket.route_area_id, --CHG0055676
     ticket.watch_and_protect, --CHG0055676
     ticket.service_area_code, --CHG0055676
     ticket.explosives, --CHG0055676
     ticket.serial_number, --CHG0055676
     ticket.map_ref, --CHG0055676
     ticket.followup_type_id, --CHG0055676
     ticket.do_not_respond_before, --CHG0055676
     ticket.recv_manager_id, --CHG0055676
     ticket.ward, --CHG0055676
     coalesce(area.area_name, '') as area_name, --CHG0055676
     ticket.alert, --CHG0055676
     -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null: --CHG0055676
     null, --     start_date, --CHG0055676
     null, --     end_date, --CHG0055676
     null, --     points --CHG0055676
     ticket.work_priority_id,  --CHG0055676
	 ticket.wo_number    -- QMANTWO-253 --CHG0055676
    from @modtix mt --CHG0055676
      inner join ticket on mt.ticket_id = ticket.ticket_id --CHG0055676
      left join area on area.area_id = ticket.route_area_id --CHG0055676
 --CHG0055676
  insert into @locates_to_send --CHG0055676
    (locate_id, ticket_id, client_code, client_id, status, high_profile, qty_marked, --CHG0055676
     price, closed, closed_by_id, closed_how, closed_date, modified_date, active, --CHG0055676
     invoiced, assigned_to, regular_hours, overtime_hours, added_by, watch_and_protect, --CHG0055676
     high_profile_reason, seq_number, assigned_to_id, mark_type, alert, --CHG0055676
     locator_id, entry_date, gps_id, status_changed_by_id, length_total, workload_date)--todo length_total is not defined in the temp table? --CHG0055676
  select --CHG0055676
     l.locate_id, l.ticket_id, l.client_code, l.client_id, l.status, l.high_profile, l.qty_marked, --CHG0055676
     l.price, l.closed, l.closed_by_id, l.closed_how, l.closed_date, l.modified_date, l.active, --CHG0055676
     l.invoiced, l.assigned_to, l.regular_hours, l.overtime_hours, l.added_by, l.watch_and_protect, --CHG0055676
     l.high_profile_reason, l.seq_number, l.assigned_to_id, l.mark_type, l.alert, assignment.locator_id,  --CHG0055676
     l.entry_date, l.gps_id, l.status_changed_by_id, --CHG0055676
     (select sum(lh.units_marked) from locate_hours lh where lh.locate_id = l.locate_id), --CHG0055676
     cast(dbo.get_ticket_workload_date(l.ticket_id, assignment.locator_id) as datetime) --CHG0055676
    from @modtix mt --CHG0055676
      inner join locate l --CHG0055676
        on mt.ticket_id = l.ticket_id and l.status <> '-N' and l.status <> '-P' --CHG0055676
      inner join assignment --CHG0055676
        on l.locate_id=assignment.locate_id and assignment.active = 1 --CHG0055676
 --CHG0055676
  -- Send the ticket and locate data --CHG0055676
  select 'ticket' as tname, * from @tickets_to_send ticket --CHG0055676
 --CHG0055676
  select 'locate' as tname, * from @locates_to_send locate --CHG0055676
 --CHG0055676
  -- All locate_hours for all the locates being sent --CHG0055676
  select 'locate_hours' as tname, locate_hours.* --CHG0055676
    from @locates_to_send locate  --CHG0055676
    inner join locate_hours on (locate_hours.locate_id = locate.locate_id) --CHG0055676
 --CHG0055676
 --CHG0055676
 -----Send down Damages to Client----- --CHG0055676
 --CHG0055676
  --All damages for this employee --CHG0055676
  insert into @should_have_damages --CHG0055676
    select damage_id from damage where investigator_id = @EmployeeID --CHG0055676
      and damage_type in ('INCOMING', 'PENDING') --CHG0055676
      and active = 1 --CHG0055676
 --CHG0055676
  --Damages the client does have --CHG0055676
  insert into @client_has_damages --CHG0055676
    select ID from IdListToTable(@ClientDamageListS) --CHG0055676
 --CHG0055676
  -- they need to know about updates to damages they are --CHG0055676
  -- assigned now, and also damages still in cache --CHG0055676
  insert into @damages (damage_id) --CHG0055676
    select should_damage_id from @should_have_damages --CHG0055676
    union --CHG0055676
    select client_damage_id from @client_has_damages --CHG0055676
 --CHG0055676
  -- subtract what we have, from what we should have --CHG0055676
  delete from @should_have_damages where should_damage_id in --CHG0055676
    (select client_damage_id from @client_has_damages) --CHG0055676
 --CHG0055676
  -- Find those that have changed, somewhat slow --CHG0055676
  insert into @mod_damages (damage_id) --CHG0055676
  select d.damage_id --CHG0055676
   from @damages d --CHG0055676
    inner join damage on damage.damage_id = d.damage_id --CHG0055676
    and damage.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select notes.foreign_id --CHG0055676
   from @damages d2 --CHG0055676
    inner join notes on notes.foreign_id = d2.damage_id --CHG0055676
   where notes.foreign_type = 2 --CHG0055676
    and notes.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select attachment.foreign_id --CHG0055676
   from @damages d3 --CHG0055676
    inner join attachment on attachment.foreign_id = d3.damage_id --CHG0055676
   where attachment.foreign_type = 2 --CHG0055676
    and attachment.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select should_damage_id from @should_have_damages --CHG0055676
 --CHG0055676
  -- now get the whole damage for just the ones that have changed --CHG0055676
  declare @damages_to_send table ( --CHG0055676
  damage_id int NOT NULL, --CHG0055676
  office_id int NULL, --CHG0055676
  damage_inv_num varchar(12) NULL, --CHG0055676
  damage_type varchar(20) NULL, --CHG0055676
  damage_date datetime NULL, --CHG0055676
  uq_notified_date datetime NULL, --CHG0055676
  notified_by_person varchar(40) NULL, --CHG0055676
  notified_by_company varchar(40) NULL, --CHG0055676
  notified_by_phone varchar(20) NULL, --CHG0055676
  client_code varchar(10) NULL, --CHG0055676
  client_id int NULL, --CHG0055676
  client_claim_id varchar(30) NULL, --CHG0055676
  date_mailed datetime NULL, --CHG0055676
  date_faxed datetime NULL, --CHG0055676
  sent_to varchar(30) NULL, --CHG0055676
  size_type varchar(50) NULL, --CHG0055676
  location varchar(80) NULL, --CHG0055676
  page varchar(20) NULL, --CHG0055676
  grid varchar(20) NULL, --CHG0055676
  city varchar(40) NULL, --CHG0055676
  county varchar(40) NULL, --CHG0055676
  state varchar(2) NULL, --CHG0055676
  utility_co_damaged varchar(40) NULL, --CHG0055676
  diagram_number int NULL, --CHG0055676
  remarks text NULL, --CHG0055676
  investigator_id int NULL, --CHG0055676
  investigator_arrival datetime NULL, --CHG0055676
  investigator_departure datetime NULL, --CHG0055676
  investigator_est_damage_time datetime NULL, --CHG0055676
  investigator_narrative text NULL, --CHG0055676
  excavator_company varchar(30) NULL, --CHG0055676
  excavator_type varchar(20) NULL, --CHG0055676
  excavation_type varchar(20) NULL, --CHG0055676
  locate_marks_present varchar(1) NULL, --CHG0055676
  locate_requested varchar(1) NULL, --CHG0055676
  ticket_id int NULL, --CHG0055676
  site_mark_state varchar(20) NULL, --CHG0055676
  site_sewer_marked bit NULL, --CHG0055676
  site_water_marked bit NULL, --CHG0055676
  site_catv_marked bit NULL, --CHG0055676
  site_gas_marked bit NULL, --CHG0055676
  site_power_marked bit NULL, --CHG0055676
  site_tel_marked bit NULL, --CHG0055676
  site_other_marked varchar(20) NULL, --CHG0055676
  site_pictures varchar(20) NULL, --CHG0055676
  site_marks_measurement decimal(9, 3) NULL, --CHG0055676
  site_buried_under varchar(20) NULL, --CHG0055676
  site_clarity_of_marks varchar(20) NULL, --CHG0055676
  site_hand_dig bit NULL, --CHG0055676
  site_mechanized_equip bit NULL, --CHG0055676
  site_paint_present bit NULL, --CHG0055676
  site_flags_present bit NULL, --CHG0055676
  site_exc_boring bit NULL, --CHG0055676
  site_exc_grading bit NULL, --CHG0055676
  site_exc_open_trench bit NULL, --CHG0055676
  site_img_35mm bit NULL, --CHG0055676
  site_img_digital bit NULL, --CHG0055676
  site_img_video bit NULL, --CHG0055676
  disc_repair_techs_were varchar(30) NULL, --CHG0055676
  disc_repairs_were varchar(30) NULL, --CHG0055676
  disc_repair_person varchar(30) NULL, --CHG0055676
  disc_repair_contact varchar(30) NULL, --CHG0055676
  disc_repair_comment text NULL, --CHG0055676
  disc_exc_were varchar(30) NULL, --CHG0055676
  disc_exc_person varchar(30) NULL, --CHG0055676
  disc_exc_contact varchar(30) NULL, --CHG0055676
  disc_exc_comment text NULL, --CHG0055676
  disc_other1_person varchar(30) NULL, --CHG0055676
  disc_other1_contact varchar(30) NULL, --CHG0055676
  disc_other1_comment text NULL, --CHG0055676
  disc_other2_person varchar(30) NULL, --CHG0055676
  disc_other2_contact varchar(30) NULL, --CHG0055676
  disc_other2_comment text NULL, --CHG0055676
  exc_resp_code varchar(4) NULL, --CHG0055676
  exc_resp_type varchar(4) NULL, --CHG0055676
  exc_resp_other_desc varchar(100) NULL, --CHG0055676
  exc_resp_details text NULL, --CHG0055676
  exc_resp_response text NULL, --CHG0055676
  uq_resp_code varchar(4) NULL, --CHG0055676
  uq_resp_type varchar(4) NULL, --CHG0055676
  uq_resp_other_desc varchar(100) NULL, --CHG0055676
  uq_resp_details text NULL, --CHG0055676
  spc_resp_code varchar(4) NULL, --CHG0055676
  spc_resp_type varchar(4) NULL, --CHG0055676
  spc_resp_other_desc varchar(100) NULL, --CHG0055676
  spc_resp_details text NULL, --CHG0055676
  modified_date datetime NOT NULL, --CHG0055676
  active bit NOT NULL, --CHG0055676
  profit_center varchar(15) NULL, --CHG0055676
  due_date datetime NULL, --CHG0055676
  intelex_num varchar(15) NULL, --CHG0055676
  closed_date datetime NULL, --CHG0055676
  modified_by int NULL, --CHG0055676
  uq_resp_ess_step varchar(5) NULL, --CHG0055676
  facility_type varchar(10) NULL, --CHG0055676
  facility_size varchar(10) NULL, --CHG0055676
  locator_experience varchar(10) NULL, --CHG0055676
  locate_equipment varchar(10) NULL, --CHG0055676
  was_project bit NOT NULL, --CHG0055676
  claim_status varchar(10) NULL, --CHG0055676
  claim_status_date datetime NULL, --CHG0055676
  invoice_code varchar(10) NULL, --CHG0055676
  facility_material varchar(10) NULL, --CHG0055676
  added_by int NULL, --CHG0055676
  accrual_date datetime NULL, --CHG0055676
  invoice_code_initial_value varchar(10) NULL, --CHG0055676
  uq_damage_id int NULL, --CHG0055676
  estimate_locked bit NOT NULL, --CHG0055676
  site_marked_in_white varchar(1) NULL, --CHG0055676
  site_tracer_wire_intact varchar(1) NULL, --CHG0055676
  site_pot_holed varchar(1) NULL, --CHG0055676
  site_nearest_fac_measure decimal(9, 3) NULL, --CHG0055676
  excavator_doing_repairs varchar(20) NULL, --CHG0055676
  offset_visible varchar(1) NULL, --CHG0055676
  offset_qty decimal(9, 3) NULL, --CHG0055676
  claim_coordinator_id int NULL, --CHG0055676
  locator_id int NULL, --CHG0055676
  estimate_agreed bit NOT NULL, --CHG0055676
  approved_by_id int NULL, --CHG0055676
  approved_datetime datetime NULL, --CHG0055676
  reason_changed int NULL, --CHG0055676
  work_priority_id int NULL, --CHG0055676
  marks_within_tolerance varchar(1) NULL --CHG0055676
  ) --CHG0055676
 --CHG0055676
  -- gather the damage data, to a table var, so these queries complete --CHG0055676
  -- right away (without NOLOCK) --CHG0055676
  insert into @damages_to_send --CHG0055676
  select --CHG0055676
     damage.damage_id, --CHG0055676
     damage.office_id, --CHG0055676
     damage.damage_inv_num, --CHG0055676
     damage.damage_type, --CHG0055676
     damage.damage_date, --CHG0055676
     damage.uq_notified_date, --CHG0055676
     damage.notified_by_person, --CHG0055676
     damage.notified_by_company, --CHG0055676
     damage.notified_by_phone, --CHG0055676
     damage.client_code, --CHG0055676
     damage.client_id, --CHG0055676
     damage.client_claim_id, --CHG0055676
     damage.date_mailed, --CHG0055676
     damage.date_faxed, --CHG0055676
     damage.sent_to, --CHG0055676
     damage.size_type, --CHG0055676
     damage.location, --CHG0055676
     damage.page, --CHG0055676
     damage.grid, --CHG0055676
     damage.city, --CHG0055676
     damage.county, --CHG0055676
     damage.state, --CHG0055676
     damage.utility_co_damaged, --CHG0055676
     damage.diagram_number, --CHG0055676
     damage.remarks, --CHG0055676
     damage.investigator_id, --CHG0055676
     damage.investigator_arrival, --CHG0055676
     damage.investigator_departure, --CHG0055676
     damage.investigator_est_damage_time, --CHG0055676
     damage.investigator_narrative, --CHG0055676
     damage.excavator_company, --CHG0055676
     damage.excavator_type, --CHG0055676
     damage.excavation_type, --CHG0055676
     damage.locate_marks_present, --CHG0055676
     damage.locate_requested, --CHG0055676
     damage.ticket_id, --CHG0055676
     damage.site_mark_state, --CHG0055676
     damage.site_sewer_marked, --CHG0055676
     damage.site_water_marked, --CHG0055676
     damage.site_catv_marked, --CHG0055676
     damage.site_gas_marked, --CHG0055676
     damage.site_power_marked, --CHG0055676
     damage.site_tel_marked, --CHG0055676
     damage.site_other_marked, --CHG0055676
     damage.site_pictures, --CHG0055676
     damage.site_marks_measurement, --CHG0055676
     damage.site_buried_under, --CHG0055676
     damage.site_clarity_of_marks, --CHG0055676
     damage.site_hand_dig, --CHG0055676
     damage.site_mechanized_equip, --CHG0055676
     damage.site_paint_present, --CHG0055676
     damage.site_flags_present, --CHG0055676
     damage.site_exc_boring, --CHG0055676
     damage.site_exc_grading, --CHG0055676
     damage.site_exc_open_trench, --CHG0055676
     damage.site_img_35mm, --CHG0055676
     damage.site_img_digital, --CHG0055676
     damage.site_img_video, --CHG0055676
     damage.disc_repair_techs_were, --CHG0055676
     damage.disc_repairs_were, --CHG0055676
     damage.disc_repair_person, --CHG0055676
     damage.disc_repair_contact, --CHG0055676
     damage.disc_repair_comment, --CHG0055676
     damage.disc_exc_were, --CHG0055676
     damage.disc_exc_person, --CHG0055676
     damage.disc_exc_contact, --CHG0055676
     damage.disc_exc_comment, --CHG0055676
     damage.disc_other1_person, --CHG0055676
     damage.disc_other1_contact, --CHG0055676
     damage.disc_other1_comment, --CHG0055676
     damage.disc_other2_person, --CHG0055676
     damage.disc_other2_contact, --CHG0055676
     damage.disc_other2_comment, --CHG0055676
     damage.exc_resp_code, --CHG0055676
     damage.exc_resp_type, --CHG0055676
     damage.exc_resp_other_desc, --CHG0055676
     damage.exc_resp_details, --CHG0055676
     damage.exc_resp_response, --CHG0055676
     damage.uq_resp_code, --CHG0055676
     damage.uq_resp_type, --CHG0055676
     damage.uq_resp_other_desc, --CHG0055676
     damage.uq_resp_details, --CHG0055676
     damage.spc_resp_code, --CHG0055676
     damage.spc_resp_type, --CHG0055676
     damage.spc_resp_other_desc, --CHG0055676
     damage.spc_resp_details, --CHG0055676
     damage.modified_date, --CHG0055676
     damage.active, --CHG0055676
     damage.profit_center, --CHG0055676
     damage.due_date, --CHG0055676
     damage.intelex_num, --CHG0055676
     damage.closed_date, --CHG0055676
     damage.modified_by, --CHG0055676
     damage.uq_resp_ess_step, --CHG0055676
     damage.facility_type, --CHG0055676
     damage.facility_size, --CHG0055676
     damage.locator_experience, --CHG0055676
     damage.locate_equipment, --CHG0055676
     damage.was_project, --CHG0055676
     damage.claim_status, --CHG0055676
     damage.claim_status_date, --CHG0055676
     damage.invoice_code, --CHG0055676
     damage.facility_material, --CHG0055676
     damage.added_by, --CHG0055676
     damage.accrual_date, --CHG0055676
     damage.invoice_code_initial_value, --CHG0055676
     damage.uq_damage_id, --CHG0055676
     damage.estimate_locked, --CHG0055676
     damage.site_marked_in_white, --CHG0055676
     damage.site_tracer_wire_intact, --CHG0055676
     damage.site_pot_holed, --CHG0055676
     damage.site_nearest_fac_measure, --CHG0055676
     damage.excavator_doing_repairs, --CHG0055676
     damage.offset_visible, --CHG0055676
     damage.offset_qty, --CHG0055676
     damage.claim_coordinator_id, --CHG0055676
     damage.locator_id, --CHG0055676
     damage.estimate_agreed, --CHG0055676
     damage.approved_by_id, --CHG0055676
     damage.approved_datetime, --CHG0055676
     damage.reason_changed, --CHG0055676
     damage.work_priority_id, --CHG0055676
     damage.marks_within_tolerance --CHG0055676
   from @mod_damages md --CHG0055676
      inner join damage on md.damage_id = damage.damage_id --CHG0055676
 --CHG0055676
  -- Send the damage data --CHG0055676
  select 'damage' as tname, * from @damages_to_send damage --CHG0055676
 --CHG0055676
-------------Send down Work Orders to client------------- --CHG0055676
 --CHG0055676
  --All Work Orders for this employee --CHG0055676
  insert into @should_have_workorders --CHG0055676
    select wo_id from work_order where assigned_to_id = @EmployeeID --CHG0055676
      and active = 1 --CHG0055676
      and closed = 0 --CHG0055676
      and status <> '-P' --CHG0055676
      and kind <> 'CANCEL' --CHG0055676
 --CHG0055676
  --Work Orders the client does have --CHG0055676
  insert into @client_has_workorders --CHG0055676
    select ID from IdListToTable(@ClientWorkOrderListS) --CHG0055676
 --CHG0055676
  -- they need to know about updates to Work Orders they are --CHG0055676
  -- assigned now, and also Work Orders still in cache --CHG0055676
  insert into @workorders (wo_id) --CHG0055676
    select should_wo_id from @should_have_workorders --CHG0055676
    union --CHG0055676
    select client_wo_id from @client_has_workorders --CHG0055676
 --CHG0055676
  -- subtract what we have, from what we should have --CHG0055676
  delete from @should_have_workorders where should_wo_id in --CHG0055676
    (select client_wo_id from @client_has_workorders) --CHG0055676
 --CHG0055676
  -- Find those that have changed, somewhat slow --CHG0055676
  insert into @mod_workorders (wo_id) --CHG0055676
  select wo.wo_id --CHG0055676
   from @workorders wo --CHG0055676
    inner join work_order on work_order.wo_id = wo.wo_id --CHG0055676
    and work_order.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select notes.foreign_id --CHG0055676
   from @workorders wo2 --CHG0055676
    inner join notes on notes.foreign_id = wo2.wo_id --CHG0055676
   where notes.foreign_type = 7 --CHG0055676
    and notes.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select attachment.foreign_id --CHG0055676
   from @workorders wo3 --CHG0055676
    inner join attachment on attachment.foreign_id = wo3.wo_id --CHG0055676
   where attachment.foreign_type = 7 --CHG0055676
    and attachment.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select work_order_ticket.wo_id --CHG0055676
    from @workorders wo4 --CHG0055676
    inner join work_order_ticket on work_order_ticket.wo_id = wo4.wo_id --CHG0055676
    where work_order_ticket.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select work_order_inspection.wo_id --CHG0055676
    from @workorders wo5 --CHG0055676
    inner join work_order_inspection on work_order_inspection.wo_id = wo5.wo_id --CHG0055676
    where work_order_inspection.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select work_order_remedy.wo_id --CHG0055676
    from @workorders wo6 --CHG0055676
    inner join work_order_remedy on work_order_remedy.wo_id = wo6.wo_id --CHG0055676
    where work_order_remedy.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select work_order_OHM_details.wo_id --QMANTWO-391 --CHG0055676
    from @workorders wo7 --CHG0055676
    inner join work_order_OHM_details on work_order_OHM_details.wo_id = wo7.wo_id --CHG0055676
    where work_order_OHM_details.modified_date > @UpdatesSince --CHG0055676
  union --CHG0055676
  select should_wo_id from @should_have_workorders --CHG0055676
 --CHG0055676
  -- now get the whole work order for just the ones that have changed --CHG0055676
  declare @workorders_to_send table ( --CHG0055676
    wo_id integer not null, --CHG0055676
    wo_number varchar(20) not null, --CHG0055676
    assigned_to_id integer null, --CHG0055676
    client_id integer null, --CHG0055676
    parsed_ok bit null, --CHG0055676
    wo_source varchar(20) not null, --CHG0055676
    kind varchar(40) not null, --CHG0055676
    status varchar(5) not null, --CHG0055676
    closed bit not null, --CHG0055676
    map_page varchar(20) null, --CHG0055676
    map_ref varchar(60) null, --CHG0055676
    transmit_date datetime not null, --CHG0055676
    due_date datetime not null, --CHG0055676
    closed_date datetime null, --CHG0055676
    status_date datetime null, --CHG0055676
    cancelled_date datetime null, --CHG0055676
    work_type varchar(90) null, --CHG0055676
    work_description varchar(3500) null, --CHG0055676
    work_address_number varchar(10) null, --CHG0055676
    work_address_number_2 varchar(10) null, --CHG0055676
    work_address_street varchar(60) null, --CHG0055676
    work_cross varchar(100) null, --CHG0055676
    work_county varchar(40) null, --CHG0055676
    work_city varchar(40) null, --CHG0055676
    work_state varchar(2) null, --CHG0055676
    work_zip varchar(10) null, --CHG0055676
    work_lat decimal(9,6) null, --CHG0055676
    work_long decimal(9,6) null, --CHG0055676
    caller_name varchar(50) null, --CHG0055676
    caller_contact varchar(50) null, --CHG0055676
    caller_phone varchar(40) null, --CHG0055676
    caller_cellular varchar(40) null, --CHG0055676
    caller_fax varchar(40) null, --CHG0055676
    caller_altcontact varchar(40) null, --CHG0055676
    caller_altphone varchar(40) null, --CHG0055676
    caller_email varchar(40) null, --CHG0055676
    client_wo_number varchar(40) not null, --CHG0055676
    image text null, --CHG0055676
    parse_errors text null, --CHG0055676
    update_of_wo_id integer null, --CHG0055676
    modified_date datetime not null, --CHG0055676
    active bit not null, --CHG0055676
    job_number varchar(20) null, --CHG0055676
    client_order_num varchar(20) null, --CHG0055676
    client_master_order_num varchar(20) null, --CHG0055676
    wire_center varchar(20) null, --CHG0055676
    work_center varchar(20) null, --CHG0055676
    central_office varchar(20) null, --CHG0055676
    serving_terminal varchar(20) null, --CHG0055676
    circuit_number varchar(20) null, --CHG0055676
    f2_cable varchar(20) null, --CHG0055676
    terminal_port varchar(20) null, --CHG0055676
    f2_pair varchar(20) null, --CHG0055676
    state_hwy_row varchar(10) null, --CHG0055676
    road_bore_count integer null, --CHG0055676
    driveway_bore_count integer null, --CHG0055676
    call_date datetime null, --CHG0055676
    source_sent_attachment varchar(1) null --CHG0055676
  ) --CHG0055676
 --CHG0055676
  -- gather the Work Order data, to a table var, so these queries complete --CHG0055676
  -- right away (without NOLOCK) --CHG0055676
  insert into @workorders_to_send --CHG0055676
  select --CHG0055676
     work_order.wo_id --CHG0055676
    ,work_order.wo_number --CHG0055676
    ,work_order.assigned_to_id --CHG0055676
    ,work_order.client_id --CHG0055676
    ,work_order.parsed_ok --CHG0055676
    ,work_order.wo_source --CHG0055676
    ,work_order.kind --CHG0055676
    ,work_order.status --CHG0055676
    ,work_order.closed --CHG0055676
    ,work_order.map_page --CHG0055676
    ,work_order.map_ref --CHG0055676
    ,work_order.transmit_date --CHG0055676
    ,work_order.due_date --CHG0055676
    ,work_order.closed_date --CHG0055676
    ,work_order.status_date --CHG0055676
    ,work_order.cancelled_date --CHG0055676
    ,work_order.work_type --CHG0055676
    ,work_order.work_description --CHG0055676
    ,work_order.work_address_number --CHG0055676
    ,work_order.work_address_number_2 --CHG0055676
    ,work_order.work_address_street --CHG0055676
    ,work_order.work_cross --CHG0055676
    ,work_order.work_county --CHG0055676
    ,work_order.work_city --CHG0055676
    ,work_order.work_state --CHG0055676
    ,work_order.work_zip --CHG0055676
    ,work_order.work_lat --CHG0055676
    ,work_order.work_long --CHG0055676
    ,work_order.caller_name --CHG0055676
    ,work_order.caller_contact --CHG0055676
    ,work_order.caller_phone --CHG0055676
    ,work_order.caller_cellular --CHG0055676
    ,work_order.caller_fax --CHG0055676
    ,work_order.caller_altcontact --CHG0055676
    ,work_order.caller_altphone --CHG0055676
    ,work_order.caller_email --CHG0055676
    ,work_order.client_wo_number --CHG0055676
    ,work_order.image --CHG0055676
    ,work_order.parse_errors --CHG0055676
    ,work_order.update_of_wo_id --CHG0055676
    ,work_order.modified_date --CHG0055676
    ,work_order.active --CHG0055676
    ,work_order.job_number --CHG0055676
    ,work_order.client_order_num --CHG0055676
    ,work_order.client_master_order_num --CHG0055676
    ,work_order.wire_center --CHG0055676
    ,work_order.work_center --CHG0055676
    ,work_order.central_office --CHG0055676
    ,work_order.serving_terminal --CHG0055676
    ,work_order.circuit_number --CHG0055676
    ,work_order.f2_cable --CHG0055676
    ,work_order.terminal_port --CHG0055676
    ,work_order.f2_pair --CHG0055676
    ,work_order.state_hwy_row --CHG0055676
    ,work_order.road_bore_count --CHG0055676
    ,work_order.driveway_bore_count --CHG0055676
    ,work_order.call_date --CHG0055676
    ,work_order.source_sent_attachment --CHG0055676
  from @mod_workorders mwo --CHG0055676
      inner join work_order on mwo.wo_id = work_order.wo_id --CHG0055676
 --CHG0055676
  -- Send the Work Order data --CHG0055676
  select 'work_order' as tname, * from @workorders_to_send work_order --CHG0055676
   --CHG0055676
  -- Send the work_order_version data --CHG0055676
  select 'work_order_version' as tname, work_order_version.* --CHG0055676
    from @mod_workorders mw --CHG0055676
      inner join work_order_version on mw.wo_id = work_order_version.wo_id --CHG0055676
 --CHG0055676
  -- Send the Work Order <-> Ticket association data --CHG0055676
  select 'work_order_ticket' as tname, work_order_ticket.*  --CHG0055676
  from work_order_ticket  --CHG0055676
  inner join @workorders_to_send wo on work_order_ticket.wo_id = wo.wo_id --CHG0055676
  where work_order_ticket.modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send work order assignment data --CHG0055676
  select 'wo_assignment' as tname, * from wo_assignment --CHG0055676
  where assigned_to_id = @EmployeeID --CHG0055676
    and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Get ticket_id's for related tickets that need to be sent --CHG0055676
  insert into @related_tickets (ticket_id) --CHG0055676
    select ticket_id --CHG0055676
    from work_order_ticket wt --CHG0055676
      -- Only include tickets related to work orders the client has or should have --CHG0055676
      inner join @workorders w on w.wo_id = wt.wo_id --CHG0055676
    where --CHG0055676
      -- Exclude tickets we already sent --CHG0055676
      wt.ticket_id not in (select ticket_id from @tickets_to_send) and --CHG0055676
      -- Exclude tickets the client already has                 --CHG0055676
      wt.ticket_id not in ( --CHG0055676
        select c.client_ticket_id --CHG0055676
        from @client_has_tix c --CHG0055676
          inner join ticket t on t.ticket_id = c.client_ticket_id --CHG0055676
        where t.modified_date <= @UpdatesSince) --CHG0055676
 --CHG0055676
  -- Send the related ticket data --CHG0055676
  select 'ticket' as tname, --CHG0055676
    ticket.ticket_id, --CHG0055676
    ticket.ticket_number, --CHG0055676
    ticket.parsed_ok, --CHG0055676
    ticket.ticket_format, --CHG0055676
    ticket.kind, --CHG0055676
    ticket.status, --CHG0055676
    ticket.map_page, --CHG0055676
    ticket.revision, --CHG0055676
    ticket.transmit_date, --CHG0055676
    ticket.due_date, --CHG0055676
    ticket.work_description, --CHG0055676
    ticket.work_state, --CHG0055676
    ticket.work_county, --CHG0055676
    ticket.work_city, --CHG0055676
    ticket.work_address_number, --CHG0055676
    ticket.work_address_number_2, --CHG0055676
    ticket.work_address_street, --CHG0055676
    ticket.work_cross, --CHG0055676
    ticket.work_subdivision, --CHG0055676
    ticket.work_type, --CHG0055676
    ticket.work_date, --CHG0055676
    ticket.work_notc, --CHG0055676
    ticket.work_remarks, --CHG0055676
    ticket.priority, --CHG0055676
    ticket.legal_date, --CHG0055676
    ticket.legal_good_thru, --CHG0055676
    ticket.legal_restake, --CHG0055676
    ticket.respond_date, --CHG0055676
    ticket.duration, --CHG0055676
    ticket.company, --CHG0055676
    ticket.con_type, --CHG0055676
    ticket.con_name, --CHG0055676
    ticket.con_address, --CHG0055676
    ticket.con_city, --CHG0055676
    ticket.con_state, --CHG0055676
    ticket.con_zip, --CHG0055676
    ticket.call_date, --CHG0055676
    ticket.caller, --CHG0055676
    ticket.caller_contact, --CHG0055676
    ticket.caller_phone, --CHG0055676
    ticket.caller_cellular, --CHG0055676
    ticket.caller_fax, --CHG0055676
    ticket.caller_altcontact, --CHG0055676
    ticket.caller_altphone, --CHG0055676
    ticket.caller_email, --CHG0055676
    ticket.operator, --CHG0055676
    ticket.channel, --CHG0055676
    ticket.work_lat, --CHG0055676
    ticket.work_long, --CHG0055676
    ticket.geocode_precision, --CHG0055676
    ticket.image, --CHG0055676
    ticket.parse_errors, --CHG0055676
    ticket.modified_date, --CHG0055676
    ticket.active, --CHG0055676
    ticket.ticket_type, --CHG0055676
    ticket.legal_due_date, --CHG0055676
    ticket.parent_ticket_id, --CHG0055676
    ticket.do_not_mark_before, --CHG0055676
    ticket.route_area_id, --CHG0055676
    ticket.watch_and_protect, --CHG0055676
    ticket.service_area_code, --CHG0055676
    ticket.explosives, --CHG0055676
    ticket.serial_number, --CHG0055676
    ticket.map_ref, --CHG0055676
    ticket.followup_type_id, --CHG0055676
    ticket.do_not_respond_before, --CHG0055676
    ticket.recv_manager_id, --CHG0055676
    ticket.ward, --CHG0055676
    coalesce(area.area_name, '') as area_name, --CHG0055676
    ticket.alert, --CHG0055676
    -- 9/20/06: These Prince fields are not in the UQ DBs, and Prince is not operational, so just return null: --CHG0055676
    null, --     start_date, --CHG0055676
    null, --     end_date, --CHG0055676
    null, --     points  --CHG0055676
    ticket.work_priority_id, --CHG0055676
	ticket.wo_number    -- QMANTWO-253  --CHG0055676
  from @related_tickets r --CHG0055676
    inner join ticket on ticket.ticket_id = r.ticket_id --CHG0055676
    left join area on area.area_id = ticket.route_area_id --CHG0055676
 --CHG0055676
  -- Send the Work Order Inspection data --CHG0055676
  select 'work_order_inspection' as tname, work_order_inspection.*  --CHG0055676
  from @workorders_to_send wo --CHG0055676
  inner join work_order_inspection on work_order_inspection.wo_id = wo.wo_id --CHG0055676
 --CHG0055676
  -- Send the Work Order Remedy data --CHG0055676
  select 'work_order_remedy' as tname, work_order_remedy.*  --CHG0055676
  from @workorders_to_send wo --CHG0055676
  inner join work_order_remedy on work_order_remedy.wo_id = wo.wo_id --CHG0055676
 --CHG0055676
  --Send the Work Order OHM data QMANTWO-391 --CHG0055676
  select 'work_order_OHM_details' as tname, work_order_OHM_details.* --CHG0055676
  from @Workorders_to_send wo --CHG0055676
  inner join work_order_OHM_details on work_order_OHM_details.wo_id = wo.wo_id --CHG0055676
 --CHG0055676
------------End send down Work Orders to client ----------------- --CHG0055676
 --CHG0055676
  -- Retrieve all notes for any ticket,damage, or work order that has been modified in any way, even --CHG0055676
  -- if the note has not been modified. This is necessary because the ticket, damage, or work order --CHG0055676
  -- may be have been reassigned to a different locator. --CHG0055676
  select 'notes' as tname, notes.* --CHG0055676
   from @modtix mt --CHG0055676
   inner join notes on notes.foreign_type = 1 -- 1 = ticket notes --CHG0055676
    and mt.ticket_id = notes.foreign_id --CHG0055676
  union --CHG0055676
  select 'notes' as tname, notes.* --CHG0055676
   from @modtix mt --CHG0055676
     inner join locate on locate.ticket_id = mt.ticket_id --CHG0055676
     inner join notes on locate.locate_id = notes.foreign_id and notes.foreign_type = 5 -- 5 = locate notes --CHG0055676
  union --CHG0055676
  select 'notes' as tname, notes.* --CHG0055676
     from @mod_damages md --CHG0055676
     inner join notes on notes.foreign_type = 2 --damage notes --CHG0055676
      and md.damage_id = notes.foreign_id --CHG0055676
  union --CHG0055676
  select 'notes' as tname, notes.* --CHG0055676
     from @mod_workorders mwo --CHG0055676
     inner join notes on notes.foreign_type = 7 --work order notes --CHG0055676
      and mwo.wo_id = notes.foreign_id --CHG0055676
 --CHG0055676
  -- All attachments for modified tickets, damages, and work orders: --CHG0055676
  select 'attachment' as tname, attachment.* --CHG0055676
   from @modtix mt --CHG0055676
   inner join attachment on mt.ticket_id = attachment.foreign_id --CHG0055676
    and attachment.foreign_type = 1 --ticket attachments --CHG0055676
  union --CHG0055676
  select 'attachment' as tname, attachment.* --CHG0055676
   from @mod_damages md --CHG0055676
   inner join attachment on md.damage_id = attachment.foreign_id --CHG0055676
    and attachment.foreign_type = 2 -- damage attachments --CHG0055676
  union --CHG0055676
  select 'attachment' as tname, attachment.* --CHG0055676
   from @mod_workorders mwo --CHG0055676
   inner join attachment on mwo.wo_id = attachment.foreign_id --CHG0055676
    and attachment.foreign_type = 7 -- work order attachments --CHG0055676
 --CHG0055676
  --All documents for modified damages --CHG0055676
  select 'document' as tname, document.* --CHG0055676
    from @mod_damages md --CHG0055676
    inner join document on document.foreign_type = 2 --CHG0055676
     and md.damage_id = document.foreign_id --CHG0055676
 --CHG0055676
  --All estimates for modified damages --CHG0055676
  select 'damage_estimate' as tname, damage_estimate.* --CHG0055676
  from @mod_damages md --CHG0055676
  inner join damage_estimate on damage_estimate.active = 1 --CHG0055676
    and md.damage_id = damage_estimate.damage_id --CHG0055676
 --CHG0055676
  --All invoices for modified damages --CHG0055676
  select 'damage_invoice' as tname, damage_invoice.* --CHG0055676
    from @mod_damages md --CHG0055676
    inner join damage_invoice on md.damage_id = damage_invoice.damage_id --CHG0055676
 --CHG0055676
  -- All Plats for modified tickets: --CHG0055676
  select 'locate_plat' as tname, locate_plat.* --CHG0055676
   from @locates_to_send ls --CHG0055676
   inner join locate_plat on ls.locate_id = locate_plat.locate_id --CHG0055676
 --CHG0055676
  -- All unacked, unexpired messages for employee: --CHG0055676
  select 'message_dest' as tname, message.message_id, message.message_number, --CHG0055676
   message.from_emp_id, message.destination, message.subject, message.body, --CHG0055676
   message.sent_date, message.show_date, message.expiration_date, message.active, --CHG0055676
   md.message_dest_id, md.emp_id, md.ack_date, md.read_date, md.rule_id --CHG0055676
  from message inner join message_dest md on message.message_id=md.message_id --CHG0055676
  where md.ack_date is null --CHG0055676
    and md.emp_id = @EmployeeID --CHG0055676
    and message.expiration_date > getdate() --CHG0055676
    and message.modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the ticket_version data --CHG0055676
  select 'ticket_version' as tname, --CHG0055676
     tv.ticket_version_id, tv.ticket_id, tv.ticket_revision, tv.ticket_number, tv.ticket_type, tv.transmit_date, --CHG0055676
     tv.processed_date, tv.serial_number, tv.arrival_date, tv.filename, tv.ticket_format, tv.source  --CHG0055676
    from @modtix mt --CHG0055676
      inner join ticket_version tv on mt.ticket_id = tv.ticket_id --CHG0055676
 --CHG0055676
  -- Send the break_rules data --CHG0055676
  select 'break_rules' as tname, break_rules.* --CHG0055676
   from break_rules --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the right definition data --CHG0055676
  select 'right_definition' as tname, * from right_definition where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the right restriction data --CHG0055676
  select 'right_restriction' as tname, * --CHG0055676
   from right_restriction --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the usage restriciton message data --CHG0055676
  select 'restricted_use_message' as tname, * --CHG0055676
   from restricted_use_message --CHG0055676
   where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the usage restriciton exemption data --CHG0055676
  select 'restricted_use_exemption' as tname, * --CHG0055676
   from restricted_use_exemption --CHG0055676
   where emp_id = @EmployeeID --CHG0055676
     and modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  -- Send the jobsite_arrival data --CHG0055676
  select 'jobsite_arrival' as tname, arrival.* --CHG0055676
    from @modtix mt --CHG0055676
      inner join jobsite_arrival arrival on mt.ticket_id = arrival.ticket_id --CHG0055676
 --CHG0055676
  -- All locate_facility for locates on modified tickets: --CHG0055676
  select 'locate_facility' as tname, lf.* --CHG0055676
    from @locates_to_send ls --CHG0055676
      inner join locate_facility lf on ls.locate_id = lf.locate_id --CHG0055676
 --CHG0055676
  -- save some key dates --CHG0055676
  declare @Today datetime --CHG0055676
  declare @Tomorrow datetime --CHG0055676
  declare @DayAfterTomorrow datetime --CHG0055676
  set @Today = convert(datetime, convert(varchar, GetDate(), 101)) --CHG0055676
  set @Tomorrow = DateAdd(Day, 1, @Today) --CHG0055676
  set @DayAfterTomorrow = DateAdd(Day, 2, @Today) --CHG0055676
     --CHG0055676
  declare @PC varchar(15) --CHG0055676
  select @PC = Coalesce(dbo.get_employee_pc(@EmployeeID, 1), '?') --CHG0055676
  select 'center_ticket_summary' as tname,  --CHG0055676
    @PC pc_code,  --CHG0055676
    0 as tickets_open, --CHG0055676
    0 as tickets_due_today, --CHG0055676
    0 as tickets_due_tomorrow --CHG0055676
 --CHG0055676
  /* TODO: The real center_ticket_summary counts are temporarily disabled for performance reasons... --CHG0055676
 --CHG0055676
  -- Get ticket summary counts for emp's profit center: --CHG0055676
  -- List of emps in the same profit center as @EmployeeID --CHG0055676
  declare @emps_in_center table ( --CHG0055676
    emp_id integer not null primary key --CHG0055676
  ) --CHG0055676
  insert into @emps_in_center select emp_id  --CHG0055676
    from dbo.employee_pc_data(@pc) where @pc is not null --CHG0055676
 --CHG0055676
  -- Get open tickets for the center --CHG0055676
  declare @open_tickets table ( --CHG0055676
    ticket_id integer not null primary key, --CHG0055676
    due_date datetime --CHG0055676
  ) --CHG0055676
  insert into @open_tickets --CHG0055676
    select distinct ticket.ticket_id, ticket.due_date  --CHG0055676
    from @emps_in_center emp --CHG0055676
      inner join locate on emp.emp_id = locate.assigned_to  --CHG0055676
      inner join ticket on locate.ticket_id = ticket.ticket_id  --CHG0055676
 --CHG0055676
  select 'center_ticket_summary' as tname,  --CHG0055676
    @PC pc_code,  --CHG0055676
    count(*) as tickets_open, --CHG0055676
    coalesce(sum( --CHG0055676
      case when due_date >= @Today and due_date < @Tomorrow then 1 else 0  --CHG0055676
      end), 0) as tickets_due_today, --CHG0055676
    coalesce(sum( --CHG0055676
      case when due_date >= @Tomorrow and due_date < @DayAfterTomorrow then 1 else 0  --CHG0055676
      end), 0) as tickets_due_tomorrow --CHG0055676
  from @open_tickets --CHG0055676
*/ --CHG0055676
 --CHG0055676
  -- Send the task_schedule data for emp's tickets --CHG0055676
  select 'task_schedule' as tname, ts.* --CHG0055676
    from @modtix mt --CHG0055676
    inner join task_schedule ts on mt.ticket_id = ts.ticket_id --CHG0055676
  -- and send any modified task_schedule data no longer for emp's tickets --CHG0055676
  union select 'task_schedule' as tname, ts1.* --CHG0055676
    from task_schedule ts1 --CHG0055676
    left join @modtix mt1 on mt1.ticket_id = ts1.ticket_id --CHG0055676
    where (ts1.emp_id = @EmployeeID) and (ts1.est_start_date >= @Today) --CHG0055676
      and (ts1.modified_date > @UpdatesSince) --CHG0055676
      and (mt1.ticket_id is null) --CHG0055676
 --CHG0055676
  select 'damage_profit_center_rule' as tname, * from damage_profit_center_rule --CHG0055676
  where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'work_order_work_type' as tname, * from work_order_work_type where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
/* Added for the questions to ask on Status changes (CO) */ --CHG0055676
  select 'question_info' as tname, * from question_info where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'question_group_detail' as tname, * from question_group_detail where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'question_header' as tname, * from question_header where modified_date > @UpdatesSince --CHG0055676
 --CHG0055676
  select 'holiday' as tname, * from holiday where (modified_date > @UpdatesSince) and (holiday_date >= (@Today - 3)) --CHG0055676
 --CHG0055676
 --CHG0055676
