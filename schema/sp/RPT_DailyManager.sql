if object_id('dbo.RPT_DailyManager') is not null
	drop procedure dbo.RPT_DailyManager
go

create proc RPT_DailyManager(
  @ManagerID integer,
  @StartDate datetime,
  @EndDate datetime
)
as
  
set nocount on

select null where 1=0
go
grant execute on RPT_DailyManager to uqweb, QManagerRole

/*
exec dbo.RPT_DailyManager 100, '1/25/2004', '02/02/2005'
*/
