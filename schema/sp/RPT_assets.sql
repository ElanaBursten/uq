if object_id('dbo.RPT_assets') is not null
	drop procedure dbo.RPT_assets
GO

CREATE Procedure RPT_assets (@ManagerID int)
as

set nocount on

declare @results table (
	emp_id int NOT NULL primary key,
	short_name varchar(45),
	emp_number varchar(30),
	h_report_level integer,
	h_report_to integer,
	h_active bit,
	h_namepath varchar(450) NULL,
	h_1 varchar(30) NULL,
	h_2 varchar(30) NULL,
	h_3 varchar(30) NULL,
	h_4 varchar(30) NULL,
	h_5 varchar(30) NULL,
	h_6 varchar(30) NULL,
	h_7 varchar(30) NULL,
	h_8 varchar(30) NULL,
	h_9 varchar(30) NULL
)

insert into @results (emp_id, short_name, emp_number,
  h_report_level, h_report_to, h_active, h_namepath,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9 )
select h_emp_id, h_short_name_tagged, h_emp_number,
  h_report_level, h_report_to, h_active, h_namepath,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9
 from dbo.get_report_hier2(@ManagerID, 0, 99)
 where h_active=1

select r.*, a.asset_id, a.asset_code, ast.description,
  a.asset_number, a.condition, ref.description as ConditionDescription,
  a.comment
 from @results r
  inner join asset_assignment aa on aa.emp_id = r.emp_id
  inner join asset a on a.asset_id = aa.asset_id
  inner join asset_type ast on ast.asset_code = a.asset_code
  left join reference ref on (ref.code=a.condition)
 where a.active=1 and aa.active=1
 order by h_namepath, short_name

go
grant execute on RPT_assets to uqweb, QManagerRole

/*
exec dbo.RPT_assets 2410
exec dbo.RPT_assets 1787
exec dbo.RPT_assets 2676
*/

