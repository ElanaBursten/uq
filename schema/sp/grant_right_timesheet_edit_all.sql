IF OBJECT_ID('dbo.grant_right_timesheet_edit_all') IS NOT NULL
  drop procedure dbo.grant_right_timesheet_edit_all
GO

create procedure dbo.grant_right_timesheet_edit_all
  (
    @EmpID int,
    @Limitation varchar(200)
  )
as

declare @LimitationStr varchar(200)
  set nocount on

  if (@EmpID is null)
    RAISERROR('An Employee ID required.', 18, 1) with log

  if (@Limitation is null) or (@Limitation='')
    set @LimitationStr = '-'
  else
    set @LimitationStr = @Limitation

  if exists (select emp_right_id from employee_right where emp_id=@EmpID and right_id=16) 
    update employee_right set allowed = 'Y', limitation=@LimitationStr where emp_id=@EmpID and right_id=16
 else 
    insert into employee_right (emp_id, right_id, allowed, limitation) values (@EmpID, 16, 'Y', @LimitationStr)
GO

grant execute on grant_right_timesheet_edit_all to uqweb, QManagerRole
GO

/*
  exec dbo.grant_right_timesheet_edit_all 3510, '-'
*/

