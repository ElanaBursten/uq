if object_id('dbo.RPT_tickets_received_summary3') is not null
	drop procedure dbo.RPT_tickets_received_summary3
go

create proc RPT_tickets_received_summary3 (
  @StartDate  datetime,
  @EndDate    datetime,
  @IncludeBreakdownByCC int,
  @IncludeNoWorkTickets int
)
as
  set nocount on

  -- This result set is designed such that a nearly default crosstab
  -- will generate the desired results; hence the user friendly column names,
  -- space before Tickets to make it sort first, etc.

  -- dates across the top, PCs down the left.

  --  @IncludeNoWorkTickets=1 only works if @IncludeBreakdownByCC=1, because
  -- the output of No Work Tickets would not mean much if not broken down
  -- by call center.

/*
  declare @StartDate  datetime
  declare @EndDate    datetime
  set @StartDate='2007-06-01'
  set @EndDate='2007-06-05'
*/

  if @IncludeBreakdownByCC=1

    select
      emp_pc_code as "Profit Center",
      call_center as "Call Center",
      ticket_date as "Date",
      count(distinct ticket_id) as Received
    from (
      select
        ticket.ticket_format as call_center,
        coalesce(emp_pc_code, 'none') as emp_pc_code,
        convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
        ticket.ticket_id
      from ticket
        inner join locate with (index(locate_ticketid))
           on ticket.ticket_id=locate.ticket_id and locate.status <> '-N'
         left join dbo.employee_payroll_data2() e
          on locate.assigned_to_id = e.emp_id
      where ticket.transmit_date between @StartDate and @EndDate
       and ticket.due_date between @StartDate and dateadd(d, 30, @EndDate)
      ) as t
    group by
      emp_pc_code,
      call_center,
      ticket_date

    union all

    select
      'No Work, no PC' as "Profit Center",
      call_center as "Call Center",
      ticket_date as "Date",
      count(distinct ticket_id) as Received
    from (
      select
        ticket.ticket_format as call_center,
        convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
        ticket.ticket_id
      from ticket
      where ticket.transmit_date between @StartDate and @EndDate
       and ticket.due_date between @StartDate and dateadd(d, 30, @EndDate)
       and not exists (select * from locate where ticket.ticket_id=locate.ticket_id and locate.status <> '-N')
       and @IncludeNoWorkTickets=1
      ) as t
    group by
      call_center,
      ticket_date

  else

    select
      emp_pc_code as "Profit Center",
      '      ' as "Call Center",
      ticket_date as "Date",
      count(distinct ticket_id) as Received
    from (
      select
        ticket.ticket_format as call_center,
        coalesce(emp_pc_code, 'none') as emp_pc_code,
        convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
        ticket.ticket_id
      from ticket
        inner join locate with (index(locate_ticketid))
           on ticket.ticket_id=locate.ticket_id and locate.status <> '-N'
         left join dbo.employee_payroll_data2() e
          on locate.assigned_to_id = e.emp_id
      where ticket.transmit_date between @StartDate and @EndDate
       and ticket.due_date between @StartDate and dateadd(d, 30, @EndDate)
      ) as t
    group by
      emp_pc_code,
      ticket_date
go

grant execute on RPT_tickets_received_summary3 to uqweb, QManagerRole

/*
exec dbo.RPT_tickets_received_summary3 '2006-02-01', '2006-02-02', 1, 0
exec dbo.RPT_tickets_received_summary3 '2006-02-01', '2006-02-02', 1, 1
exec dbo.RPT_tickets_received_summary3 '2006-02-01', '2006-02-02', 0
*/
