if object_id('dbo.find_township_baseline_2') is not null
	drop procedure dbo.find_township_baseline_2
go

CREATE Procedure find_township_baseline_2
	(
	@baseline varchar(10),
	@township varchar(10),
	@range varchar(10),
	@section varchar(10),
    @CallCenter varchar(20)
	)
As

/* Includes a call center. Returns records with the given call center, *and*
   records with the call_center field = '*' (changed to '' in the result 
   table). If there are records with a call center specified, then those are
   returned first. */

declare @results table (
	map_id int,
	area_id int,
	area_name varchar(50),
	emp_id int,
	short_name varchar(50),
    call_center varchar(20)  -- included for sorting 
)

set nocount on

INSERT INTO @results
select area.map_id, area.area_id, area.area_name, employee.emp_id,
  employee.short_name, call_center =
  case when trs.call_center = '*' then '' else trs.call_center end
 from map_trs trs
  LEFT join area on trs.area_id = area.area_id
  LEFT join employee on area.locator_id = employee.emp_id
 WHERE trs.basecode=@baseline
   AND trs.township=@township
   AND trs.range=@range
   AND trs.section=@section
   AND employee.active = 1
   and employee.can_receive_tickets = 1
   and (trs.call_center = '*' or @CallCenter = trs.call_center)

IF @@RowCount = 0

INSERT INTO @results
select area.map_id, area.area_id, area.area_name, employee.emp_id,
  employee.short_name, call_center =
  case when trs.call_center = '*' then '' else trs.call_center end
 from map_trs trs
  LEFT join area on trs.area_id = area.area_id
  LEFT join employee on area.locator_id = employee.emp_id
 WHERE trs.basecode=@baseline
   AND trs.township=@township
   AND trs.range=@range
   AND trs.section='*'
   AND employee.active = 1
   and employee.can_receive_tickets = 1
   and (trs.call_center = '*' or @CallCenter = trs.call_center)

select * from @results
order by call_center desc
-- if there is a call center (not ''), show that first
GO

grant execute on find_township_baseline_2 to uqweb, QManagerRole

/*
exec find_township_baseline_2 'CO', '01N', '071W', '34', 'FCO1'
*/
