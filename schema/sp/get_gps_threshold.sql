if object_id('dbo.get_gps_threshold') is not null
  drop function dbo.get_gps_threshold
go

create function dbo.get_gps_threshold(@EmpId int)
returns
  integer
as
begin
  declare @Limitation varchar(200)
  declare @Threshold integer
  declare @CommaPos integer
  declare @DefaultThreshold integer

  set @DefaultThreshold = 2000 -- Used if no value is in the emp right's limitation
  set @Limitation = dbo.get_emp_right_limitation(@EmpID, 'TrackGPSPosition')
  set @CommaPos = CHARINDEX(',', @Limitation, 1)
  if @CommaPos > 1
    set @Limitation = LEFT(@Limitation, @CommaPos-1)
  
  set @Threshold = CONVERT(integer, @Limitation)
  if @Threshold <= 0 
    set @Threshold = @DefaultThreshold

  return @Threshold
end
go

grant execute on get_gps_threshold to uqweb, QManagerRole
go


/*
select dbo.get_gps_threshold(3510)
*/

