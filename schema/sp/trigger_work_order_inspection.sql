--TRIGGER for work_order_inspection
if object_id('dbo.work_order_inspection_iu') is not null
  drop trigger dbo.work_order_inspection_iu
go


create trigger [dbo].[work_order_inspection_iu] on [dbo].[work_order_inspection]
for insert, update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_inspection
  set modified_date = GetDate()
  from inserted I
  where work_order_inspection.wo_id = I.wo_id
end

GO