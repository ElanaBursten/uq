if object_id('holiday_iu') is not null
  drop trigger holiday_iu
GO
  
create trigger holiday_iu on holiday
  for INSERT, UPDATE
  not for replication
  AS BEGIN
    declare @cc_code varchar(20);
    declare @holiday_date datetime;
    select @cc_code =cc_code, @holiday_date= holiday_date from inserted  
		  update holiday set modified_date = getdate() 
		  where ((cc_code = @cc_code) and
                (holiday_date = @holiday_date))
  END
GO   

--UPDATE [dbo].[holiday]
--   SET modified_date = GetDate()
--where modified_date is null
--GO
