if object_id('dbo.get_office_id') is not null
  drop procedure dbo.get_office_id
go

CREATE PROCEDURE get_office_id (
  @TicketID int,
  @OfficeID int output
) 
AS

select top 1 /*ticket.ticket_id, locate_id, locate.client_id,*/ 
  @OfficeID = client.office_id
from ticket, locate, client
where locate.ticket_id = ticket.ticket_id
and locate.client_id is not null
and locate.client_id = client.client_id
and ticket.ticket_id = @TicketID
and client.office_id is not null

RETURN
GO
grant execute on get_office_id to uqweb, QManagerRole


