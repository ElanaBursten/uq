if object_id('dbo.datetime_to_string') is not null
  drop function dbo.datetime_to_string
go

create function dbo.datetime_to_string(@datetime datetime)
returns varchar(18)
as
begin
  -- format a date as "mm/dd/yyyy hh:mmAMPM'
  declare @formatted_time varchar(18)
  
  declare @hour int
  declare @minute varchar(2)
  declare @ampm char(2)
  
  set @minute = DateName(minute, @datetime)
  if Len(@minute) < 2 set @minute = '0' + @minute

  set @hour = DatePart(hour, @datetime)
  set @ampm = 'PM'  
  if @hour < 12 set @ampm = 'AM'
  if @hour = 0 set @hour = 12
  if @hour > 12 set @hour = @hour - 12

  set @formatted_time = ' ' + Str(@hour, 2) + ':' + @minute + @ampm
  
  return convert(varchar(10), @datetime, 101) + @formatted_time
end

/*
select '2009-01-02T07:01:00.000', dbo.datetime_to_string('2009-01-02T07:01:00.000')
select '2009-01-02T17:05:00.000', dbo.datetime_to_string('2009-01-02T17:05:00.000')
select '2009-01-02', dbo.datetime_to_string('2009-01-02')
select '2009-01-02T23:59:59.000', dbo.datetime_to_string('2009-01-02T23:59:59.000')
select '2009-01-02T00:01:59.000', dbo.datetime_to_string('2009-01-02T00:01:59.000')
select '2009-01-02T12:15:38.000', dbo.datetime_to_string('2009-01-02T12:15:38.000')
*/

