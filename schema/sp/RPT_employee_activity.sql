if object_id('dbo.RPT_employee_activity') is not null
  drop procedure dbo.RPT_employee_activity
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_employee_activity (
  @ManagerId int,
  @StartDate datetime,
  @EndDate datetime,
  @OutOfRangeActivities bit,
  @OutOfRangeEmployees bit,
  @HourSpanStart datetime,
  @HourSpanEnd datetime,
  @EmployeeStatus int,
  @EmployeeId int=null
)
as
  set nocount on

  create table #result (
    r_emp_id int,
    mgr_id int,
    mgr_short_name varchar(30) NULL,
    report_level integer NULL,
    emp_number varchar(15) NULL,
    short_name varchar(30) NULL,
    last_name varchar(30) NULL,
    h_report_level integer NULL,
    h_namepath varchar(450) NULL,
    h_idpath varchar(160) NULL,
    tse_id int NULL,
    emp_type_desc varchar(100) NULL,
    primary key(r_emp_id)
  )

  create nonclustered index result_idx_001 on #result (mgr_id asc)
  create nonclustered index result_idx_004 on #result (tse_id asc)

  create table #emp_activity (
    emp_act_id int not null,
    emp_id int not null,
    activity_date datetime not null,
    activity_type varchar(12) null,
    description varchar(300) null,
    out_of_range bit default 0,
    tse_entry_id int null, -- for work start/stop activities, this is the entry_id for the date & time it was entered
    details varchar(50) null,
    extra_details varchar(20) null
  )

  create nonclustered index emp_activity_idx_001 on #emp_activity (activity_date asc)
  create nonclustered index emp_activity_idx_002 on #emp_activity (emp_id asc)

  create table #emp_activity_summary (
    emp_id int not null,
    mgr_id int null,
    emp_number varchar(15) null,
    emp_name varchar(30) null,
    mgr_name varchar(30) null,
    emp_type_desc varchar(100) null,
    work_date datetime null,
    r_hours datetime null,
    r_break datetime null,
    r_callout datetime null,
    r_route int null,
    r_view int null,
    r_sync int null,
    r_status int null,
    r_note int null,
    r_attach_add int null,
    r_attach_upload int null,
    r_other int null,
    r_total int null
  )

  create nonclustered index emp_activity_summary_idx_001 on #emp_activity_summary (work_date asc)
  create nonclustered index emp_activity_summary_idx_002 on #emp_activity_summary (emp_id asc)

  -- added to properly capture all data in the 24 hour period of the @EndDate value
  set @EndDate = DateAdd(ss, -1, @EndDate)

  declare @AfterEndDate datetime
  declare @ToleranceMinutes int
  -- used to check activity outside working hours
  set @ToleranceMinutes = 5

  declare @NoDate datetime
  set @NoDate = '1900-01-01'

  -- used to make the locate_status query more efficient
  set @AfterEndDate = dateadd(d, 5, @EndDate)

  if (@HourSpanStart is not null and @HourSpanEnd is not null)
  begin
    set @HourSpanStart = @StartDate + convert(datetime, convert(varchar, @HourSpanStart, 108))
    set @HourSpanEnd = @StartDate + convert(datetime, convert(varchar, @HourSpanEnd, 108))
  end

  if (IsNull(@EmployeeId,-1) = -1)
  begin
    -- get all the emps for this manager in result table
    insert into #result (r_emp_id, mgr_id, mgr_short_name, emp_number,
      short_name, last_name, report_level, h_namepath, h_idpath)
    select h_emp_id, h_report_to, h_report_to_short_name, h_emp_number,
      h_short_name, h_last_name, h_report_level, h_namepath, h_idpath
    from get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)
    where h_emp_id <> @ManagerID
  end
  else
  begin
    -- get Data for Single Employee
    insert into #result (r_emp_id, mgr_id, mgr_short_name, emp_number,
      short_name, last_name, report_level, h_namepath, h_idpath)
    select  e.emp_id,
      @ManagerID,
      null as mgr_short_name,
      e.emp_number,
      e.short_name,
      e.last_name,
      null as report_level,
      null as h_namepath,
      null as h_idpath
    from employee e
    where emp_Id = @EmployeeId

    update #result
    set mgr_short_name = (select short_name from employee where employee.emp_id = #result.mgr_id)
  end

  --select all needed timesheet data
  create table #tse (
    entry_id int NOT NULL,
    work_emp_id int NOT NULL,
    work_date datetime NOT NULL ,
    status varchar (8) COLLATE Latin1_General_CI_AS NOT NULL ,
    entry_date_local datetime NOT NULL ,
    entry_by int NOT NULL ,
    approve_date_local datetime NULL ,
    approve_by int NULL ,
    final_approve_date_local datetime NULL ,
    final_approve_by int NULL ,
    work_start1 datetime NULL ,
    work_stop1 datetime NULL ,
    work_start2 datetime NULL ,
    work_stop2 datetime NULL ,
    work_start3 datetime NULL ,
    work_stop3 datetime NULL ,
    work_start4 datetime NULL ,
    work_stop4 datetime NULL ,
    work_start5 datetime NULL ,
    work_stop5 datetime NULL ,
    work_start6 datetime NULL ,
    work_stop6 datetime NULL ,
    work_start7 datetime NULL ,
    work_stop7 datetime NULL ,
    work_start8 datetime NULL ,
    work_stop8 datetime NULL ,
    work_start9 datetime NULL ,
    work_stop9 datetime NULL ,
    work_start10 datetime NULL ,
    work_stop10 datetime NULL ,
    work_start11 datetime NULL ,
    work_stop11 datetime NULL ,
    work_start12 datetime NULL ,
    work_stop12 datetime NULL ,
    work_start13 datetime NULL ,
    work_stop13 datetime NULL ,
    work_start14 datetime NULL ,
    work_stop14 datetime NULL ,
    work_start15 datetime NULL ,
    work_stop15 datetime NULL ,
    work_start16 datetime NULL ,
    work_stop16 datetime NULL ,
    callout_start1 datetime NULL ,
    callout_stop1 datetime NULL ,
    callout_start2 datetime NULL ,
    callout_stop2 datetime NULL ,
    callout_start3 datetime NULL ,
    callout_stop3 datetime NULL ,
    callout_start4 datetime NULL ,
    callout_stop4 datetime NULL ,
    callout_start5 datetime NULL ,
    callout_stop5 datetime NULL ,
    callout_start6 datetime NULL ,
    callout_stop6 datetime NULL,
    callout_start7 datetime NULL ,
    callout_stop7 datetime NULL,
    callout_start8 datetime NULL ,
    callout_stop8 datetime NULL,
    callout_start9 datetime NULL ,
    callout_stop9 datetime NULL,
    callout_start10 datetime NULL ,
    callout_stop10 datetime NULL,
    callout_start11 datetime NULL ,
    callout_stop11 datetime NULL,
    callout_start12 datetime NULL ,
    callout_stop12 datetime NULL,
    callout_start13 datetime NULL ,
    callout_stop13 datetime NULL,
    callout_start14 datetime NULL ,
    callout_stop14 datetime NULL,
    callout_start15 datetime NULL ,
    callout_stop15 datetime NULL,
    callout_start16 datetime NULL ,
    callout_stop16 datetime NULL,
    rule_id_ack int NULL,
    rule_ack_date datetime NULL,
    entry_by_short_name varchar (30) NULL,
    total_reg_hours decimal(5,2) NULL,
    total_co_hours decimal(5,2) NULL,
    primary key (entry_id)
  )

  create nonclustered index tse_idx_001 on #tse (work_emp_id asc)
  create nonclustered index tse_idx_002 on #tse (work_date asc)
  create nonclustered index tse_idx_003 on #tse (status asc)
  create nonclustered index tse_idx_004 on #tse (entry_by asc)
  create nonclustered index tse_idx_005 on #tse (approve_date_local asc)
  create nonclustered index tse_idx_006 on #tse (final_approve_date_local asc)
  create nonclustered index tse_idx_007 on #tse (final_approve_by asc)

  insert into #tse
  select
    entry_id,
    work_emp_id,
    work_date,
    status,
    entry_date_local,
    entry_by,
    approve_date_local,
    approve_by,
    final_approve_date_local,
    final_approve_by,
    work_date + convert(datetime, convert(varchar, work_start1, 108)) as work_start1,
    work_date + convert(datetime, convert(varchar, work_stop1,108)) as work_stop1,
    work_date + convert(datetime, convert(varchar, work_start2, 108)) as work_start2,
    work_date + convert(datetime, convert(varchar, work_stop2,108)) as work_stop2,
    work_date + convert(datetime, convert(varchar, work_start3, 108)) as work_start3,
    work_date + convert(datetime, convert(varchar, work_stop3,108)) as work_stop3,
    work_date + convert(datetime, convert(varchar, work_start4, 108)) as work_start4,
    work_date + convert(datetime, convert(varchar, work_stop4,108)) as work_stop4,
    work_date + convert(datetime, convert(varchar, work_start5, 108)) as work_start5,
    work_date + convert(datetime, convert(varchar, work_stop5,108)) as work_stop5,
    work_date + convert(datetime, convert(varchar, work_start6, 108)) as work_start6,
    work_date + convert(datetime, convert(varchar, work_stop6,108)) as work_stop6,
    work_date + convert(datetime, convert(varchar, work_start7, 108)) as work_start7,
    work_date + convert(datetime, convert(varchar, work_stop7,108)) as work_stop7,
    work_date + convert(datetime, convert(varchar, work_start8, 108)) as work_start8,
    work_date + convert(datetime, convert(varchar, work_stop8,108)) as work_stop8,
    work_date + convert(datetime, convert(varchar, work_start9, 108)) as work_start9,
    work_date + convert(datetime, convert(varchar, work_stop9,108)) as work_stop9,
    work_date + convert(datetime, convert(varchar, work_start10, 108)) as work_start10,
    work_date + convert(datetime, convert(varchar, work_stop10,108)) as work_stop10,
    work_date + convert(datetime, convert(varchar, work_start11, 108)) as work_start11,
    work_date + convert(datetime, convert(varchar, work_stop11,108)) as work_stop11,
    work_date + convert(datetime, convert(varchar, work_start12, 108)) as work_start12,
    work_date + convert(datetime, convert(varchar, work_stop12,108)) as work_stop12,
    work_date + convert(datetime, convert(varchar, work_start13, 108)) as work_start13,
    work_date + convert(datetime, convert(varchar, work_stop13,108)) as work_stop13,
    work_date + convert(datetime, convert(varchar, work_start14, 108)) as work_start14,
    work_date + convert(datetime, convert(varchar, work_stop14,108)) as work_stop14,
    work_date + convert(datetime, convert(varchar, work_start15, 108)) as work_start15,
    work_date + convert(datetime, convert(varchar, work_stop15,108)) as work_stop15,
    work_date + convert(datetime, convert(varchar, work_start16, 108)) as work_start16,
    work_date + convert(datetime, convert(varchar, work_stop16,108)) as work_stop16,
    work_date + convert(datetime, convert(varchar, callout_start1,108)) as callout_start1,
    work_date + convert(datetime, convert(varchar, callout_stop1,108)) as callout_stop1,
    work_date + convert(datetime, convert(varchar, callout_start2,108)) as callout_start2,
    work_date + convert(datetime, convert(varchar, callout_stop2,108)) as callout_stop2,
    work_date + convert(datetime, convert(varchar, callout_start3,108)) as callout_start3,
    work_date + convert(datetime, convert(varchar, callout_stop3,108)) as callout_stop3,
    work_date + convert(datetime, convert(varchar, callout_start4,108)) as callout_start4,
    work_date + convert(datetime, convert(varchar, callout_stop4,108)) as callout_stop4,
    work_date + convert(datetime, convert(varchar, callout_start5,108)) as callout_start5,
    work_date + convert(datetime, convert(varchar, callout_stop5,108)) as callout_stop5,
    work_date + convert(datetime, convert(varchar, callout_start6,108)) as callout_start6,
    work_date + convert(datetime, convert(varchar, callout_stop6, 108)) as callout_stop6,
    work_date + convert(datetime, convert(varchar, callout_start7,108)) as callout_start7,
    work_date + convert(datetime, convert(varchar, callout_stop7, 108)) as callout_stop7,
    work_date + convert(datetime, convert(varchar, callout_start8,108)) as callout_start8,
    work_date + convert(datetime, convert(varchar, callout_stop8, 108)) as callout_stop8,
    work_date + convert(datetime, convert(varchar, callout_start9,108)) as callout_start9,
    work_date + convert(datetime, convert(varchar, callout_stop9, 108)) as callout_stop8,
    work_date + convert(datetime, convert(varchar, callout_start10,108)) as callout_start10,
    work_date + convert(datetime, convert(varchar, callout_stop10, 108)) as callout_stop10,
    work_date + convert(datetime, convert(varchar, callout_start11,108)) as callout_start11,
    work_date + convert(datetime, convert(varchar, callout_stop11, 108)) as callout_stop11,
    work_date + convert(datetime, convert(varchar, callout_start12,108)) as callout_start12,
    work_date + convert(datetime, convert(varchar, callout_stop12, 108)) as callout_stop12,
    work_date + convert(datetime, convert(varchar, callout_start13,108)) as callout_start13,
    work_date + convert(datetime, convert(varchar, callout_stop13, 108)) as callout_stop13,
    work_date + convert(datetime, convert(varchar, callout_start14,108)) as callout_start14,
    work_date + convert(datetime, convert(varchar, callout_stop14, 108)) as callout_stop14,
    work_date + convert(datetime, convert(varchar, callout_start15,108)) as callout_start15,
    work_date + convert(datetime, convert(varchar, callout_stop15, 108)) as callout_stop15,
    work_date + convert(datetime, convert(varchar, callout_start16,108)) as callout_start16,
    work_date + convert(datetime, convert(varchar, callout_stop16, 108)) as callout_stop16,
    rule_id_ack,
    rule_ack_date,
    r.short_name, -- the name of the person who entered the timesheet data
                  -- if entry_by <> work_emp_id, this column will be updated
                  -- later in this SP
    reg_hours + ot_hours + dt_hours as total_reg_hours,
    callout_hours
  from timesheet_entry tse inner join #result r on tse.work_emp_id = r_emp_id
  where tse.work_date between @StartDate and @EndDate

  update #tse
    set entry_by_short_name = (select short_name from employee where emp_id = entry_by)
  where work_emp_id <> entry_by

  --select all needed computer usage data
  declare @computer_usage table (
    emp_id int not null,
    computer_info_id int not null,
    computer_name varchar(25) not null,
    sync_id int not null,
    insert_date datetime not null,
    sync_date datetime not null)

  insert into @computer_usage (emp_id, computer_info_id, computer_name, sync_id, insert_date, sync_date)
    select c.emp_id, c.computer_info_id, Coalesce(c.computer_name, 'unknown'), c.sync_id, c.insert_date, s.local_date
      from computer_info c
      inner join #result r on c.emp_id = r.r_emp_id
      inner join sync_log s on c.sync_id = s.sync_id
      where s.local_date between @StartDate and @EndDate

--//  set @EndDate = dateadd(d, 1, @EndDate)

  declare @NoLocStatus varchar(100)
  declare @NoWorkStatus varchar(100)
  set @NoLocStatus = (select coalesce(description, '') from reference where type='locstat' and code='NA' and active_ind=1)
  set @NoWorkStatus = (select coalesce(description, '') from reference where type='workstat' and code='NA' and active_ind=1)

  insert into #emp_activity (emp_act_id, activity_date, emp_id, activity_type, description, tse_entry_id, details, extra_details)

    -- All new logged activities
    select ea.emp_activity_id, ea.activity_date, ea.emp_id, ea.activity_type, '', null, ea.details, ea.extra_details
    from employee_activity ea inner join #result r on ea.emp_id = r.r_emp_id
    where ea.activity_date between @StartDate and @EndDate

    UNION ALL

    -- Uploading an attachment
    Select a.attachment_id, a.upload_date, a.attached_by, 
      -- background_upload = 1 for attachments uploaded in background; others require user to wait for completion
      case when (a.foreign_type = 1 and a.source = 'WORKORDR') then 'ATSYSUPL' --Ticket Attachment copied from a related work order
           when (a.foreign_type = 1 and ISNULL(a.background_upload,0) = 0) then 'ATUPL' --Ticket Attachment Foreground Upload
           when (a.foreign_type = 1 and ISNULL(a.background_upload,0) = 1) then 'ATBUPL' -- Ticket Attachment Background Upload
           when (a.foreign_type = 2 and ISNULL(a.background_upload,0) = 0) then 'DMGATTUP' -- Damage Attachment Foreground Upload
           when (a.foreign_type = 2 and ISNULL(a.background_upload,1) = 1) then 'DMGATTBUP' -- Damage Attachment Background Upload
           else 'ATUPL' end, '', null, null, null
    from attachment a inner join #result r on a.attached_by = r_emp_id
    where a.upload_date between @StartDate and @EndDate

    UNION ALL

    -- Adding an attachment
    Select a.attachment_id, a.attach_date, a.attached_by, 
    case when a.foreign_type = 2 then 'DMGATTCH'
         when (a.foreign_type = 1 and a.source = 'WORKORDR') then 'ATSYSADD' -- Ticket attachment copied from a related work order
         else 'ATADD' end, '', null, null, null
    from attachment a inner join #result r on a.attached_by = r_emp_id
    where a.attach_date between @StartDate and @EndDate
     
    UNION ALL

    -- Entering Time - Timesheet entry
    select tse.entry_id, tse.entry_date_local, tse.entry_by, 'TSENT',
      '(' +convert(varchar(10), tse.work_date,101)+ ')', null, null, null
    from #tse tse inner join #result r on tse.entry_by = r_emp_id
    where tse.entry_date_local between @StartDate and @EndDate 

    UNION ALL

    -- Change status of a locate
    select ls.ls_id, ls.status_date, ls.statused_by, 'LOCST', 
      case when ls.status_date > t.due_date then 'Late ' else '' end +
      t.ticket_format + ' #' + t.ticket_number + ' ' + l.client_code + ' ' + 
      LTrim(RTrim(coalesce(r1.description, @NoWorkStatus) + ' ' + 	  
      coalesce(r2.description, @NoLocStatus))), null, null, null
    from locate_status ls 
    inner join #result r on ls.statused_by = r_emp_id
    inner join locate l on ls.locate_id = l.locate_id
    inner join ticket t on l.ticket_id = t.ticket_id
    left join locate_snap lsp on ls.locate_id = lsp.locate_id
    left join reference r1 on (r1.type='workstat' and r1.code=lsp.work_status)
    left join reference r2 on (r2.type='locstat' and r2.code=lsp.location_status)
    where ls.status_date between @StartDate and @EndDate
      and r_emp_id = coalesce(ls.status_changed_by_id, ls.statused_by) --Note that QM makes activity entries for those cases where "Statused As Assigned Locator" is checked.
      and ls.status_date <> (select top 1 coalesce(ls2.status_date, @NoDate)
        from locate_status ls2 
        where ls2.locate_id = ls.locate_id 
        and ls2.ls_id < ls.ls_id order by ls2.ls_id desc)

    UNION ALL

    -- Non-status change to a locate (the status_date is the the same as the previous status for this locate)
    -- The insert_date criteria is solely for optimization since entry_date is not indexed.
    select ls.ls_id, coalesce(ls.entry_date, ls.status_date), ls.statused_by, 'LOCCH', 
      case when ls.entry_date is null then '* ' else '' end +
      t.ticket_format + ' #' + t.ticket_number + ' ' + l.client_code, null, null, null
    from locate_status ls 
    inner join #result r on ls.statused_by = r_emp_id
    inner join locate l on ls.locate_id = l.locate_id
    inner join ticket t on l.ticket_id = t.ticket_id
    where ls.insert_date between @StartDate and @AfterEndDate
      and ls.entry_date between @StartDate and @EndDate
      and ls.status_date = (select top 1 coalesce(ls2.status_date, @NoDate)
        from locate_status ls2 
        where ls2.locate_id = ls.locate_id 
        and ls2.ls_id < ls.ls_id order by ls2.ls_id desc)

    UNION ALL
    
    -- Add hours to a locate
    select lh.locate_hours_id, lh.entry_date, lh.emp_id, 'LOCHR', '', null, null, null
    from locate_hours lh inner join #result r on lh.emp_id = r_emp_id
    where lh.entry_date between @StartDate and @EndDate

    UNION ALL

    -- Add a note
    select n.notes_id, n.entry_date, r_emp_id, 
    case when n.foreign_type = 2 then 'DMGNOTE' else 'ADDNT' end, '', null, null, null
    from #result r
     inner join users on users.emp_id=r_emp_id
     inner join notes n on n.uid = users.uid
    where n.entry_date between @StartDate and @EndDate
    
    UNION ALL

    -- Sync
    select sl.sync_id, sl.local_date, sl.emp_id, 
      -- sync_source is null for on-demand syncs; others are auto-syncs
      case when sl.sync_source is null then 'SYNC' else 'SYNCA' end,  
      '', null, null, null 
    from sync_log sl inner join #result r on sl.emp_id = r_emp_id
    where sl.local_date between @StartDate and @EndDate

    UNION ALL

    -- NEED CHANGES TO GET USER LOCAL DATE-TIME - PENDING
    -- Running Reports
    select rl.request_id, rl.request_date, user_id, 'RUNRE', '', null, null, null
    from report_log rl inner join #result r on rl.user_id = r_emp_id
    where rl.request_date between @StartDate and @EndDate

    UNION ALL

    -- Approve time
    select tse.entry_id, tse.approve_date_local, tse.approve_by, 'APRTS', '', null, null, null
    from #tse tse inner join #result r on tse.approve_by = r_emp_id
    where tse.approve_date_local between @StartDate and @EndDate

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start1, tse.work_emp_id, 'WSTAR', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start1 = tse.work_start1), null, null
    from #tse tse
    where tse.work_start1 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start2, tse.work_emp_id, 'WSTAR',  '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start2 = tse.work_start2), null, null
    from #tse tse
    where tse.work_start2 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start3, tse.work_emp_id, 'WSTAR', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start3 = tse.work_start3), null, null
    from #tse tse
    where tse.work_start3 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start4, tse.work_emp_id, 'WSTAR', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start4 = tse.work_start4), null, null
    from #tse tse
    where tse.work_start4 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start5, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start5 = tse.work_start5), null, null
    from #tse tse
    where tse.work_start5 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start6, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start6 = tse.work_start6), null, null
    from #tse tse
    where tse.work_start6 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start7, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start7 = tse.work_start7), null, null
    from #tse tse
    where tse.work_start7 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start8, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start8 = tse.work_start8), null, null
    from #tse tse
    where tse.work_start8 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start9, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start9 = tse.work_start9), null, null
    from #tse tse
    where tse.work_start9 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start10, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start10 = tse.work_start10), null, null
    from #tse tse
    where tse.work_start10 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start11, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start11 = tse.work_start11), null, null
    from #tse tse
    where tse.work_start11 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start12, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start12 = tse.work_start12), null, null
    from #tse tse
    where tse.work_start12 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start13, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start13 = tse.work_start13), null, null
    from #tse tse
    where tse.work_start13 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start14, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start14 = tse.work_start14), null, null
    from #tse tse
    where tse.work_start14 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start15, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start15 = tse.work_start15), null, null
    from #tse tse
    where tse.work_start15 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_start16, tse.work_emp_id, 'WSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_start16 = tse.work_start16), null, null
    from #tse tse
    where tse.work_start16 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop1, tse.work_emp_id, 'WSTOP', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop1 = tse.work_stop1), null, null
    from #tse tse
    where tse.work_stop1 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop2, tse.work_emp_id, 'WSTOP', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop2 = tse.work_stop2), null, null
    from #tse tse
    where tse.work_stop2 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop3, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop3 = tse.work_stop3), null, null
    from #tse tse
    where tse.work_stop3 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop4, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop4 = tse.work_stop4), null, null
    from #tse tse
    where tse.work_stop4 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop5, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop5 = tse.work_stop5), null, null
    from #tse tse
    where tse.work_stop5 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop6, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop6 = tse.work_stop6), null, null
    from #tse tse
    where tse.work_stop6 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop7, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop7 = tse.work_stop7), null, null
    from #tse tse
    where tse.work_stop7 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop8, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop8 = tse.work_stop8), null, null
    from #tse tse
    where tse.work_stop8 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop9, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop9 = tse.work_stop9), null, null
    from #tse tse
    where tse.work_stop9 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop10, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop10 = tse.work_stop10), null, null
    from #tse tse
    where tse.work_stop10 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop11, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop11 = tse.work_stop11), null, null
    from #tse tse
    where tse.work_stop11 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop12, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop12 = tse.work_stop12), null, null
    from #tse tse
    where tse.work_stop12 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop13, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop13 = tse.work_stop13), null, null
    from #tse tse
    where tse.work_stop13 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop14, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop14 = tse.work_stop14), null, null
    from #tse tse
    where tse.work_stop14 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop15, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop15 = tse.work_stop15), null, null
    from #tse tse
    where tse.work_stop15 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.work_stop16, tse.work_emp_id, 'WSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.work_stop16 = tse.work_stop16), null, null
    from #tse tse
    where tse.work_stop16 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start1, tse.work_emp_id, 'CSTAR', '', 
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start1 = tse.callout_start1), null, null
    from #tse tse
    where tse.callout_start1 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start2, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start2 = tse.callout_start2), null, null
    from #tse tse
    where tse.callout_start2 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start3, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start3 = tse.callout_start3), null, null
    from #tse tse
    where tse.callout_start3 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start4, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start4 = tse.callout_start4), null, null
    from #tse tse
    where tse.callout_start4 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start5, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start5 = tse.callout_start5), null, null
    from #tse tse
    where tse.callout_start5 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start6, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start6 = tse.callout_start6), null, null
    from #tse tse
    where tse.callout_start6 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start7, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start7 = tse.callout_start7), null, null
    from #tse tse
    where tse.callout_start7 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start8, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start8 = tse.callout_start8), null, null
    from #tse tse
    where tse.callout_start8 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start9, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start9 = tse.callout_start9), null, null
    from #tse tse
    where tse.callout_start9 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start10, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start10 = tse.callout_start10), null, null
    from #tse tse
    where tse.callout_start10 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start11, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start11 = tse.callout_start11), null, null
    from #tse tse
    where tse.callout_start11 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start12, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start12 = tse.callout_start12), null, null
    from #tse tse
    where tse.callout_start12 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start13, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start13 = tse.callout_start13), null, null
    from #tse tse
    where tse.callout_start13 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start14, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start14 = tse.callout_start14), null, null
    from #tse tse
    where tse.callout_start14 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start15, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start15 = tse.callout_start15), null, null
    from #tse tse
    where tse.callout_start15 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_start16, tse.work_emp_id, 'CSTAR', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_start16 = tse.callout_start16), null, null
    from #tse tse
    where tse.callout_start16 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop1, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop1 = tse.callout_stop1), null, null
    from #tse tse
    where tse.callout_stop1 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop2, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop2 = tse.callout_stop2), null, null
    from #tse tse
    where tse.callout_stop2 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop3, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop3 = tse.callout_stop3), null, null
    from #tse tse
    where tse.callout_stop3 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop4, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop4 = tse.callout_stop4), null, null
    from #tse tse
    where tse.callout_stop4 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop5, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop5 = tse.callout_stop5), null, null
    from #tse tse
    where tse.callout_stop5 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    -- Worked time
    select tse.entry_id, tse.callout_stop6, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop6 = tse.callout_stop6), null, null
    from #tse tse
    where tse.callout_stop6 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop7, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop7 = tse.callout_stop7), null, null
    from #tse tse
    where tse.callout_stop7 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop8, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop8 = tse.callout_stop8), null, null
    from #tse tse
    where tse.callout_stop8 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop9, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop9 = tse.callout_stop9), null, null
    from #tse tse
    where tse.callout_stop9 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop10, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop10 = tse.callout_stop10), null, null
    from #tse tse
    where tse.callout_stop10 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop11, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop11 = tse.callout_stop11), null, null
    from #tse tse
    where tse.callout_stop11 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop12, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop12 = tse.callout_stop12), null, null
    from #tse tse
    where tse.callout_stop12 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop13, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop13 = tse.callout_stop13), null, null
    from #tse tse
    where tse.callout_stop13 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop14, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop14 = tse.callout_stop14), null, null
    from #tse tse
    where tse.callout_stop14 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop15, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop15 = tse.callout_stop15), null, null
    from #tse tse
    where tse.callout_stop15 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL

    select tse.entry_id, tse.callout_stop16, tse.work_emp_id, 'CSTOP', '',  
      (select min(tse1.entry_id) from #tse tse1 
       where tse1.work_date = tse.work_date 
         and tse1.work_emp_id = tse.work_emp_id
         and tse1.callout_stop16 = tse.callout_stop16), null, null
    from #tse tse
    where tse.callout_stop16 between @StartDate and @EndDate
      and tse.status in ('ACTIVE', 'SUBMIT')

    UNION ALL
    
    -- First ticket task scheduled for the day
    select ts.task_schedule_id, ts.est_start_date, ts.emp_id, 'FIRSTTASK', 
      t.ticket_format + ' #' + t.ticket_number + 
      case ts.active when 0 then ' (inactive)' else '' end, null, null, null
    from task_schedule ts inner join #result r on ts.emp_id = r_emp_id
    inner join ticket t on ts.ticket_id = t.ticket_id
    where ts.est_start_date between @StartDate and @EndDate

    UNION ALL

    -- Jobsite arrivals
    select a.arrival_id, a.arrival_date, a.emp_id, 'ARRIVAL', 
      t.ticket_format + ' #' + t.ticket_number, null, null, null
    from jobsite_arrival a inner join #result r on a.emp_id = r_emp_id
    inner join ticket t on a.ticket_id = t.ticket_id
    where a.arrival_date between @StartDate and @EndDate
      and a.ticket_id is not null
    
    UNION ALL
    
    --Damage arrivals
    select arrival_id, a.arrival_date, a.emp_id, 'DMGARRV',
      a.location_status + ' UQ#' + CAST(d.uq_damage_id as varchar), null, null, null
    from jobsite_arrival a inner join #result r on a.emp_id = r_emp_id 
      inner join damage d on a.damage_id = d.damage_id 
    where a.arrival_date between @StartDate and @EndDate
      and a.damage_id is not null    

    order by 1
  --insert

  update #emp_activity
    set description = '(' + dbo.datetime_to_string(tse.entry_date_local) +
      case when tse.entry_by = tse.work_emp_id then '' else ' by ' + e.emp_number end + ')'
    from #tse tse inner join employee e on tse.entry_by = e.emp_id
    where tse.entry_id = tse_entry_id
      and activity_type in ('WSTAR', 'WSTOP', 'CSTAR', 'CSTOP')

  update #emp_activity 
    set description = r.description + ' ' + #emp_activity.description +
      case when r.modifier like 'SYS%' then '[S]' else '' end
    from reference r 
    where r.type = 'empact' and r.code = activity_type

  update #emp_activity
    set description = description + ' #' + coalesce(wo.wo_number, '?') 
    from work_order wo
    where wo.wo_id = convert(int, details)
      and activity_type in ('WOVUE', 'WOCLOSE', 'WOSAVE')

  update #emp_activity
  set description = description +
    ' ' + coalesce(t.ticket_format, '?') + ' #' +  coalesce(t.ticket_number, '?')
  from ticket t
  where t.ticket_id = convert(int, details)
    and activity_type in ('TKVUE', 'TKSCHED')

  update #emp_activity
  set description = description + ' UQ#' + coalesce(details, '?')
  where activity_type in ('DMGVUE', 'DMGTHDPTYVUE', 'DMGLITGVUE')
    
  update #emp_activity
  set description = description +
    case when extra_details = 'AUTO' then ' (auto) ' else ' ' end +
    ' ' + coalesce(t.ticket_format, '?') + ' #' +  coalesce(t.ticket_number, '?')
  from ticket_version tv
  inner join ticket t on tv.ticket_id = t.ticket_id
  where tv.ticket_version_id = convert(int, details)
    and activity_type in ('TIVWKST', 'TIVWKEN')

  --Update description for Ticket Route Export to show number of tickets exported
  update #emp_activity 
  set #emp_activity.description = case 
      when extra_details is null then r.description + ' (' + details + ' selected tkts)' 
      else r.description + ' (' + details + ' vis, ' + extra_details + ' hidden tkts)'
    end
  from reference r
  where r.type = 'empact' and r.code = 'TKRTEXP'
    and activity_type = r.code 

  --Update description for Damage Route Export to show number of selected damages exported
  update #emp_activity
  set #emp_activity.description = r.description + ' (' + details + ' selected dmgs)'
  from reference r 
  where r.type = 'empact' and r.code = 'DMGRTEXP'
    and activity_type = r.code 

  if (@HourSpanStart is null and @HourSpanEnd is null)
    update #emp_activity
      set out_of_range = (select count(*) from #tse tse
                           where
                            (work_emp_id = emp_id) and
                            (tse.status in ('ACTIVE', 'SUBMIT')) and
                            (activity_date between work_date and DateAdd(day, 1, work_date)) and
                           (((activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start1)) and (DateAdd(mi,@ToleranceMinutes,work_stop1))) or (work_start1 is null and work_stop1 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start2)) and (DateAdd(mi,@ToleranceMinutes,work_stop2))or (work_start2 is null and work_stop2 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start3)) and (DateAdd(mi,@ToleranceMinutes,work_stop3)) or (work_start3 is null and work_stop3 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start4)) and (DateAdd(mi,@ToleranceMinutes,work_stop4)) or (work_start4 is null and work_stop4 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start5)) and (DateAdd(mi,@ToleranceMinutes,work_stop5)) or (work_start5 is null and work_stop5 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start6)) and (DateAdd(mi,@ToleranceMinutes,work_stop6)) or (work_start6 is null and work_stop6 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start7)) and (DateAdd(mi,@ToleranceMinutes,work_stop7)) or (work_start7 is null and work_stop7 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start8)) and (DateAdd(mi,@ToleranceMinutes,work_stop8)) or (work_start8 is null and work_stop8 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start9)) and (DateAdd(mi,@ToleranceMinutes,work_stop9)) or (work_start9 is null and work_stop9 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start10)) and (DateAdd(mi,@ToleranceMinutes,work_stop10)) or (work_start10 is null and work_stop10 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start11)) and (DateAdd(mi,@ToleranceMinutes,work_stop11)) or (work_start11 is null and work_stop11 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start12)) and (DateAdd(mi,@ToleranceMinutes,work_stop12)) or (work_start12 is null and work_stop12 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start13)) and (DateAdd(mi,@ToleranceMinutes,work_stop13)) or (work_start13 is null and work_stop13 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start14)) and (DateAdd(mi,@ToleranceMinutes,work_stop14)) or (work_start14 is null and work_stop14 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start15)) and (DateAdd(mi,@ToleranceMinutes,work_stop15)) or (work_start15 is null and work_stop15 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,work_start16)) and (DateAdd(mi,@ToleranceMinutes,work_stop16)) or (work_start16 is null and work_stop16 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start1)) and (DateAdd(mi,@ToleranceMinutes,callout_stop1)) or (callout_start1 is null and callout_stop1 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start2)) and (DateAdd(mi,@ToleranceMinutes,callout_stop2)) or (callout_start2 is null and callout_stop2 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start3)) and (DateAdd(mi,@ToleranceMinutes,callout_stop3)) or (callout_start3 is null and callout_stop3 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start4)) and (DateAdd(mi,@ToleranceMinutes,callout_stop4)) or (callout_start4 is null and callout_stop4 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start5)) and (DateAdd(mi,@ToleranceMinutes,callout_stop5)) or (callout_start5 is null and callout_stop5 is null)) and
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start6)) and (DateAdd(mi,@ToleranceMinutes,callout_stop6)) or (callout_start6 is null and callout_stop6 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start7)) and (DateAdd(mi,@ToleranceMinutes,callout_stop7)) or (callout_start7 is null and callout_stop7 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start8)) and (DateAdd(mi,@ToleranceMinutes,callout_stop8)) or (callout_start8 is null and callout_stop8 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start9)) and (DateAdd(mi,@ToleranceMinutes,callout_stop9)) or (callout_start9 is null and callout_stop9 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start10)) and (DateAdd(mi,@ToleranceMinutes,callout_stop10)) or (callout_start10 is null and callout_stop10 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start11)) and (DateAdd(mi,@ToleranceMinutes,callout_stop11)) or (callout_start11 is null and callout_stop11 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start12)) and (DateAdd(mi,@ToleranceMinutes,callout_stop12)) or (callout_start12 is null and callout_stop12 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start13)) and (DateAdd(mi,@ToleranceMinutes,callout_stop13)) or (callout_start13 is null and callout_stop13 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start14)) and (DateAdd(mi,@ToleranceMinutes,callout_stop14)) or (callout_start14 is null and callout_stop14 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start15)) and (DateAdd(mi,@ToleranceMinutes,callout_stop15)) or (callout_start15 is null and callout_stop15 is null)) and 
                             (activity_date not between (DateAdd(mi,-1*@ToleranceMinutes,callout_start16)) and (DateAdd(mi,@ToleranceMinutes,callout_stop16)) or (callout_start16 is null and callout_stop16 is null)))) 
    where
       activity_type not in ('WSTAR', 'WSTOP', 'CSTAR', 'CSTOP', 'TSENT', 'ATBUPL', 'DMGATTBUP', 'ATSYSUPL', 'ATSYSADD')

  else
    update #emp_activity
      set out_of_range = (case when (activity_date not between @HourSpanStart and @HourSpanEnd) then 1 else 0 end)
    where
       activity_type not in ('WSTAR', 'WSTOP', 'CSTAR', 'CSTOP', 'TSENT', 'ATBUPL', 'DMGATTBUP', 'ATSYSUPL', 'ATSYSADD')

  -- delete the activity for all employees, except the activity of employees who has
  -- at least one activity out of range
  if @OutOfRangeEmployees = 1
    delete from #emp_activity
    where 0 = (select count(e2.out_of_range) from #emp_activity e2
               where e2.out_of_range = 1 and e2.emp_id = emp_id) and
              (activity_type not in ('START', 'QUIT', 'WSTAR', 'WSTOP', 'CSTAR', 'CSTOP', 'TSENT'))

  -- delete all the activity except the activity out of range
  if @OutOfRangeActivities = 1
    delete from #emp_activity
    where (out_of_range = 0 or out_of_range is null) and
          (activity_type not in ('WSTAR', 'WSTOP', 'CSTAR', 'CSTOP', 'TSENT'))
          -- (activity_type not in ('START', 'QUIT', 'WSTAR', 'WSTOP', 'CSTAR', 'CSTOP'))

  -- mark locates statused by the non-default method
  update #emp_activity set description = description + ' (' + coalesce(statused_how, '?') + ')'
    from locate_status
  where activity_type = 'LOCST'
    and locate_status.ls_id = emp_act_id
    and locate_status.statused_how <> 'Detail'

  -- build description for Status Locates where "Statused As Assigned Locator" is checked
  update #emp_activity
    set description =  'Status Locate as assigned employee: ' + e.short_name + '('  + e.emp_number + ') ' +
      t.ticket_format + ' #' + t.ticket_number + ' ' + l.client_code + ' ' + 
      LTrim(RTrim(coalesce(r1.description, @NoWorkStatus) + ' ' + coalesce(r2.description, @NoLocStatus)))
  from #emp_activity
    join locate l on l.locate_id = CONVERT(int,details)
    join ticket t on l.ticket_id = t.ticket_id
    join employee e on e.emp_id = CONVERT(int,extra_details)
    left join locate_snap lsp on l.locate_id = lsp.locate_id
    left join reference r1 on (r1.type='workstat' and r1.code=lsp.work_status)
    left join reference r2 on (r2.type='locstat' and r2.code=lsp.location_status)
  where activity_type = 'LOCSTA'
    and details is not null
    and extra_details is not null
    and l.locate_id = CONVERT(int,details)

  -- update emp type description
  update #result
    set emp_type_desc = reference.description
  from #result
  join employee on employee.emp_id = #result.r_emp_Id
  join reference on reference.ref_id = employee.type_id
  where r_emp_id in (select distinct emp_id from #emp_activity)

  -- Populate Summary dataset
  insert into #emp_activity_summary
    (emp_id, mgr_name, emp_number, emp_name, emp_type_desc, work_date, r_hours, r_break, r_callout, mgr_id )
  select
    work_emp_id,
    #result.mgr_short_name,
    #result.emp_number,
    #result.short_name,
    #result.emp_type_desc,
    work_date,
    DateAdd(Minute, (total_reg_hours * 60), work_date) as r_hours,
    case when (work_stop1 is not null) and (work_start2 is not null) then (Isnull(work_start2,0) - Isnull(work_stop1,0)) else 0 end as r_break,
    DateAdd(Minute, (total_co_hours * 60), work_date) as r_callout,
   (select top 1 mgr_id from #result where r_emp_id = work_emp_id)
  from #tse tse
  left outer join #result on #result.r_emp_id = tse.work_emp_id
  where r_emp_id in (select distinct emp_id from #emp_activity)
    and tse.status in ('ACTIVE', 'SUBMIT')
 
  delete from #emp_activity_summary where mgr_id is null

  update #emp_activity_summary
  set r_route =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and activity_type = 'TKRTE' and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_view =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and (activity_type in ('TKVUE','DMGVUE','DMGTHDPTYVUE','DMGLITGVUE')) and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_sync =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and activity_type = 'SYNC'  and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_status =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and activity_type = 'LOCST'  and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_note =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and (activity_type in ('ADDNT','DMGNOTE'))  and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_attach_add =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and (activity_type in ('ATADD','DMGATTCH'))  and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_attach_upload = (select count(*) from #emp_activity -- this doesn't include background uploads
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 and (activity_type in ('ATUPL','DMGATTUP'))  and #emp_activity.emp_id = #emp_activity_summary.emp_id),
      r_other =  (select count(*) from #emp_activity
                 where #emp_activity.activity_date between #emp_activity_summary.work_date
                        and dateadd(ss,-1,(dateadd(day, 1, #emp_activity_summary.work_date)))
                 and out_of_range = 1 -- other count will not include counted items and non-activity items
                 and activity_type not in ('TKRTE','TKVUE','SYNC','LOCST','ADDNT','DMGNOTE','ATADD','ATUPL','ATBUPL','DMGVUE','DMGTHDPTYVUE','DMGLITGVUE','DMGATTCH','DMGATTUP','DMGATTBUP')
                 and #emp_activity.emp_id = #emp_activity_summary.emp_id)

  update #emp_activity_summary
   set r_total = r_route+r_view+r_sync+r_status+r_note+r_attach_add+r_attach_upload+r_other

  -- Add a 'No Activity' entry for Emps that had no activity
  insert into #emp_activity (emp_act_id, activity_date, emp_id, activity_type, description)
    select r.r_emp_id, @StartDate, r.r_emp_id, 'NOACT', 'No Activity for the selected period'
      from #result r
      left outer join #emp_activity ea on r.r_emp_id = ea.emp_id
      where ea.emp_id is null

  -- Output starts here
  -- 1: employee list
  select * from #result
  order by short_name -- r_emp_id, report_level, mgr_short_name, mgr_id, short_name

  -- 2: emp data for grouping
  select distinct emp_id, convert(datetime, convert(varchar, activity_date, 110)) as activity_date
  from #emp_activity
  order by emp_id, activity_date

  -- 3: activity data
  select * from #emp_activity
  order by emp_id, activity_date

  -- 4: break rules data
  select
    work_emp_id,
    work_date,
    rule_id_ack,
    rule_ack_date,
    entry_by_short_name,
    case
      when break_rules.pc_code is not null then
        'Rule: ' + Convert(Varchar, break_length) + ' minute break '+ Lower(rule_type) + ' ' + Convert(Varchar, break_needed_after/60) + ' hours worked'
      else 'No rule defined for ' + dbo.get_employee_pc(work_emp_id,0)
    end as rule_msg,
    case
       when rule_id_ack is not null then
          case
            when rule_ack_date is not null then
              'Rule broken, ' + entry_by_short_name + ' acknowledgment on ' + Convert(varchar, rule_ack_date)
            else 'Rule broken, no acknowledgment from ' + entry_by_short_name
          end
       else ''
    end as rule_broke_msg
  from #tse tse 
  left outer join break_rules on break_rules.pc_code = dbo.get_employee_pc(tse.work_emp_id,0) and rule_type in ('EVERY', 'AFTER')
  where tse.status in ('ACTIVE', 'SUBMIT')

  -- 5: Summary Detail
  select * from #emp_activity_summary
  order by mgr_id, emp_name, emp_id, work_date

  -- 6: Summary Group
  select #result.mgr_id, #result.mgr_short_name, #result.report_level,
    ES.work_date,
    sum(ES.r_route) as s_route,
    sum(ES.r_view) as s_view,
    sum(ES.r_sync) as s_sync,
    sum(ES.r_status) as s_status,
    sum(ES.r_note) as s_note,
    sum(ES.r_attach_add) as s_attach_add,
    sum(ES.r_attach_upload) as s_attach_upload,
    sum(ES.r_other) as s_other,
    sum(ES.r_total) as s_total
  from #result
  join #emp_activity_summary ES on ES.emp_id = #result.r_emp_id
  where r_emp_id in (select distinct emp_id from #emp_activity)
  group by #result.mgr_id,  #result.mgr_short_name, #result.report_level, ES.work_date
  order by #result.report_level,  #result.mgr_short_name

  -- 7: Computers used
  select * from @computer_usage 
  
GO

grant execute on RPT_employee_activity to uqweb, QManagerRole

/*
exec dbo.RPT_employee_activity 13231, '2010-10-18T12:00:00.000', '2010-11-04T23:59:00.000', 0, 0, '00:00:00', '00:00:00', 1, 3512
exec dbo.RPT_employee_activity 13231, 'Oct 18, 2010', 'Oct 19, 2010', 0, 0, null, null, 1, 3510
exec dbo.RPT_employee_activity 6810, '2006-12-14T00:00:00.000', '2006-12-17T00:00:00.000', 0, 0, null, null, 1, -1
exec dbo.RPT_employee_activity 2676, '2006-12-14T00:00:00.000', '2006-12-17T00:00:00.000', 0, 0, null, null, 1, -1
exec dbo.RPT_employee_activity 2676, '2006-11-14T00:00:00.000', '2006-11-15T00:00:00.000', 0, 0, null, null, 1, -1
exec dbo.RPT_employee_activity 2676, '2006-11-14T00:00:00.000', '2006-11-15T00:00:00.000', 0, 0, '12:00:00', '18:00:00', 1, 10288
exec dbo.RPT_employee_activity 10467, '2007-01-14T00:00:00.000', '2007-02-06T00:00:00.000', 0, 0, null, null, 1, -1
exec dbo.RPT_employee_activity 2676, 'Jul 1, 2010', 'Jul 11, 2010', 0, 0, null, null, 1, 3510

exec dbo.RPT_employee_activity -1, '2007-06-11', '2007-06-12', 0, 0, null, null, 2, 4920
exec dbo.RPT_employee_activity -1, '2007-06-11', '2007-06-22', 0, 0, null, null, 2, 4920

exec dbo.RPT_employee_activity 4872, '2007-06-11', '2007-06-12', 0, 0, null, null, 2, -1
exec dbo.RPT_employee_activity 3849, 'May  1 2007', 'May  2 2007M', 1, 0, NULL, NULL, 1, -1

-- Loc Inc DB test case:
exec dbo.RPT_employee_activity null, '2007-06-13T00:00:00.000', '2007-06-14T00:00:00.000', 0, 0, null, null, 2, 739

select * from employee where emp_id=4920

-- Status locates after PC shutdown test case:
 exec dbo.RPT_employee_activity 2167, 'Jan 19, 2009', 'Jan 24, 2009', 0, 0, null, null, 1, 12672
 exec dbo.RPT_employee_activity 2676, 'Feb 19, 2009', 'Feb 25, 2009', 0, 0, null, null, 1, 3510

-- This case times out getting non-status locate changes:
 exec dbo.RPT_employee_activity 1873, 'Mar 9, 2009', 'Mar 16, 2009', 0, 0, null, null, 1, -1
 
*/
