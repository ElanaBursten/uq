if object_id('dbo.find_munic_area_locator_2') is not null
	drop procedure dbo.find_munic_area_locator_2
go


CREATE Procedure find_munic_area_locator_2
	(
    @State varchar(15),
    @County varchar(45),
    @Munic varchar(45),
    @CallCenter varchar(20)
	)
As
set nocount on

/* Like find_munic_area_locator, but includes the new county_area.call_center field.  For every
   query, we get records where this field matches @CallCenter, *or* is empty.  (This is done to
   preserve the behavior of the original stored proc, which didn't have this field.)
*/

declare @areafound table (
	map_id int NULL ,
	area_name varchar(40) NULL ,
	area_id int NULL ,
	emp_id int NULL ,
	short_name varchar(30) NULL ,
    call_center varchar(20) NULL
) 

insert into @areafound
select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name, county_area.call_center
 from county_area
  inner join area on county_area.area_id = area.area_id
  inner join employee on area.locator_id = employee.emp_id
WHERE county_area.state=@State and county_area.county=@County and county_area.munic=@Munic
 AND (county_area.call_center is null 
   OR county_area.call_center = '' 
   OR county_area.call_center = @CallCenter)
 AND employee.active = 1
 and employee.can_receive_tickets = 1

-- try munic == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name, county_area.call_center
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county=@County and county_area.munic='*')
   AND (county_area.call_center is null 
     OR county_area.call_center = '' 
     OR county_area.call_center = @CallCenter)
   AND employee.active = 1
   and employee.can_receive_tickets = 1

-- try county == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name, county_area.call_center
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county='*' and county_area.munic=@Munic)  
   AND (county_area.call_center is null 
     OR county_area.call_center = '' 
     OR county_area.call_center = @CallCenter)
   AND employee.active = 1
   and employee.can_receive_tickets = 1

-- try statewide wildcard == '*'
IF @@RowCount = 0
  insert into @areafound
  select county_area.map_id,
       area.area_name, area.area_id, employee.emp_id, employee.short_name, county_area.call_center
   from county_area
    inner join area on county_area.area_id = area.area_id
    inner join employee on area.locator_id = employee.emp_id
  WHERE (county_area.state=@State and county_area.county='*' and county_area.munic='*')
   AND (county_area.call_center is null 
     OR county_area.call_center = '' 
     OR county_area.call_center = @CallCenter)
   AND employee.active = 1
   and employee.can_receive_tickets = 1

 select * from @areafound
 order by call_center desc
GO

grant execute on find_munic_area_locator_2 to uqweb, QManagerRole
/*
exec dbo.find_munic_area_locator 'NJ', 'MONMOUTH', 'HIGHLANDS', 'NewJersey'
exec dbo.find_munic_area_locator 'NJ', 'SALEM', 'NOWHERE', 'NewJersey'
exec dbo.find_munic_area_locator 'LA', 'NOWHERE', 'ARMSTEAD', 'FBL1'
exec dbo.find_munic_area_locator 'OR', 'NOWHERE', 'NOWHERE', 'FOR1'
*/
