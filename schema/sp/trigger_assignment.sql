if object_id('dbo.assignment_i') is not null
  drop trigger dbo.assignment_i
GO

CREATE TRIGGER assignment_i ON assignment
FOR INSERT
not for replication
AS
BEGIN
  SET NOCOUNT ON

  UPDATE locate SET
    assigned_to =
       case when locate.closed = 1 then null
            else inserted.locator_id end,
    assigned_to_id = inserted.locator_id
  FROM inserted
  WHERE
    locate.locate_id = inserted.locate_id

  update ticket
    set recv_manager_id =
      (select max(e.report_to)
         from employee e where e.emp_id=locate.assigned_to)
    from locate
    where locate.locate_id in (select locate_id from inserted)
     and ticket.ticket_id = locate.ticket_id
     and ticket.recv_manager_id is null
  
  update ticket_snap
    set first_receive_pc = dbo.get_historical_pc(locate.assigned_to_id, first_receive_date)
    from locate 
    where locate.locate_id in (select locate_id from inserted)
      and ticket_snap.ticket_id = locate.ticket_id
      and ticket_snap.first_receive_pc is null
  
  insert into locate_snap (locate_id, first_receive_date, first_receive_pc)
    select inserted.locate_id, GetDate(), dbo.get_historical_pc(inserted.locator_id, GetDate())
    from inserted left join locate_snap on inserted.locate_id = locate_snap.locate_id
    where locate_snap.locate_id is null 
END
GO

/*
select * from ticket where ticket_id=13874660
select * from employee where emp_id=2321
*/
