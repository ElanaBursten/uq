if object_id('dbo.create_wp_ticket') is not null
	drop procedure dbo.create_wp_ticket
go

CREATE Procedure create_wp_ticket
	(
	@TicketID int
	)
As

DECLARE @NewTicketID int

INSERT INTO ticket (
ticket_number,
parsed_ok,
ticket_type,
ticket_format,
kind,
status,
map_page,
revision,
transmit_date,
parent_ticket_id,
due_date,
legal_due_date,
work_description,
work_state,
work_county,
work_city,
work_address_number,
work_address_number_2,
work_address_street,
work_cross,
work_subdivision,
work_type,
work_date,
work_notc,
work_remarks,
priority,
legal_date,
legal_good_thru,
legal_restake,
respond_date,
duration,
company,
con_type,
con_name,
con_address,
con_city,
con_state,
con_zip,
call_date,
caller,
caller_contact,
caller_phone,
caller_cellular,
caller_fax,
caller_altcontact,
caller_altphone,
caller_email,
operator,
channel,
work_lat,
work_long,
image,
do_not_mark_before
)
SELECT
ticket_number,
parsed_ok,
'WATCH AND PROTECT',
ticket_format,
'NORMAL',
status,
map_page,
revision,
transmit_date,
parent_ticket_id,
due_date,
legal_due_date,
work_description,
work_state,
work_county,
work_city,
work_address_number,
work_address_number_2,
work_address_street,
work_cross,
work_subdivision,
work_type,
work_date,
work_notc,
work_remarks,
priority,
legal_date,
legal_good_thru,
legal_restake,
respond_date,
duration,
company,
con_type,
con_name,
con_address,
con_city,
con_state,
con_zip,
call_date,
caller,
caller_contact,
caller_phone,
caller_cellular,
caller_fax,
caller_altcontact,
caller_altphone,
caller_email,
operator,
channel,
work_lat,
work_long,
image,
do_not_mark_before
FROM ticket WHERE ticket_id=@TicketID

SELECT @NewTicketID = SCOPE_IDENTITY()

INSERT INTO Locate (
	ticket_id,
	client_code,
	client_id,
	status,
	high_profile,
	added_by
) SELECT
	@NewTicketID AS ID,
	client_code,
	client_id,
	'-P',
	high_profile,
	'W&P'
 FROM locate WHERE ticket_id = @TicketID

GO

grant execute on create_wp_ticket to uqweb, QManagerRole



