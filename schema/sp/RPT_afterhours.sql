if object_id('dbo.RPT_afterhours_2') is not null
	drop procedure dbo.RPT_afterhours_2
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */


create proc RPT_afterhours_2(@DateFrom datetime, @DateTo datetime, @ManagerId int, @EmployeeStatus int)
as
  set nocount on

  declare @MinTime datetime
  declare @MaxTime datetime

  declare @ExpandedDateFrom datetime
  declare @ExpandedDateTo datetime

  /* we're looking for sync times before 5 am or after 8 pm */
  set @MinTime = convert(datetime, '05:00:00')
  set @MaxTime = convert(datetime, '20:00:00')

  set @ExpandedDateFrom = dateadd(day, -2, @DateFrom)
  set @ExpandedDateTo = dateadd(day, 3, @DateTo)

  declare @emplist table (
    emp_id int not null primary key,
    short_name varchar(30) NULL
  )

  insert into @emplist
  select h_emp_id, h_short_name from get_report_hier3(@ManagerId, 0,10, @EmployeeStatus)
  select
    el.short_name,
    ticket.ticket_number,
    ticket.work_type,
    locate.client_code,
    ls.status,
    ls.status_date as closed_date,
    ls.insert_date as modified_date,   -- sync date, on the report, I assume
    convert(varchar(20), ls.status_date, 120) as closed_date_secondsonly
  from locate_status ls with (index(locate_status_insert_date))
    inner join locate on ls.locate_id = locate.locate_id
    inner join ticket on ticket.ticket_id = locate.ticket_id
    inner join @emplist el on ls.statused_by = el.emp_id
  where ls.status_date between @DateFrom and @DateTo
    and ls.insert_date between @ExpandedDateFrom and @ExpandedDateTo
    and ls.status = 'M'
    /* convert to a time only string, then back to a datetime in order to do comparison */
    and (convert(datetime, convert(char(8), ls.status_date, 108)) not between @MinTime and @MaxTime)
  order by
    el.short_name, closed_date_secondsonly, ticket.ticket_number

go
grant execute on RPT_afterhours_2 to uqweb, QManagerRole

/*
exec dbo.RPT_afterhours_2 '2003-04-14', '2003-04-21', 211, 1
*/


