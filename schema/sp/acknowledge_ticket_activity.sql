if object_id('dbo.acknowledge_ticket_activity') is not null
  drop procedure dbo.acknowledge_ticket_activity
GO

CREATE Procedure acknowledge_ticket_activity(@TicketActivityAckID int, @EmpID int)
As

update ticket_activity_ack
  set ack_emp_id = @EmpID, ack_date = GetDate(), has_ack = 1
  where ticket_activity_ack_id = @TicketActivityAckID
    and has_ack = 0
GO

grant execute on acknowledge_ticket_activity to uqweb, QManagerRole

