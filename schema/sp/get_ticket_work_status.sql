if object_id('dbo.get_ticket_work_status') is not null
  drop function dbo.get_ticket_work_status
go

create function get_ticket_work_status(@TicketID int)
returns
  varchar(15)
as
begin
  declare @Result varchar(15)
    
  declare @tempStatuses TABLE (
    Status varchar(5),
    Marked int,
    Cleared int,
    Other int)
  
  insert into @tempStatuses (Status, Marked, Cleared, Other)
    (select status, 
      (CASE (select dbo.get_locate_work_status(status)) when 'MARKED' then 1 else 0 end) as Marked, 
      (CASE (select dbo.get_locate_work_status(status)) when 'CLEARED' then 1 else 0 end) as Cleared,     
      (CASE  when ((select dbo.get_locate_work_status(status)) IS NULL) then 1 else 0 end) as Other     
    from locate 
    where
      locate.ticket_id = @TicketID
      and locate.active = 1 
      and locate.closed = 1
      and locate.status <> '-N') -- ignore non-client locates
   
  /* 
     If all locate.work_status on the ticket are CLEARED, then ticket's work_status is CLEARED.
     If any locate.work_status on the ticket is MARKED, then ticket's work_status is MARKED.
     If no locates on the ticket are MARKED or CLEARED, then ticket's work_status is NULL.
  */
  select @result = case 
    when ((SUM(Cleared) > 0) and (SUM(Marked) = 0) and (SUM(Other) = 0)) then 'CLEARED'
    when (SUM(Marked) > 0) then 'MARKED' 
    else Null
  end 
  from @tempStatuses
 
  return @Result
end
go

grant execute on get_ticket_work_status to uqweb, QManagerRole
go

/*
select dbo.get_ticket_work_status(1000)
select dbo.get_ticket_work_status(1082)
select dbo.get_ticket_work_status(1090)
select dbo.get_ticket_work_status(1084)
*/
