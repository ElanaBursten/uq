if object_id('dbo.log_fax_as_response') is not null
	drop procedure dbo.log_fax_as_response
go

create proc log_fax_as_response (
  @FmId int,
  @StatusDate datetime,
  @Success bit
)
as
  set nocount on 

insert into response_log (
 locate_id,
 response_date,
 call_center,
 status,
 response_sent,
 success
)
select
  locate.locate_id,
  @StatusDate as response_date,
  fm.call_center,
  locate.status,
  '(fax)' as response_sent,
  @Success as success
  from fax_message fm
   inner join fax_message_detail fd on fm.fm_id=fd.fm_id
   inner join locate on fd.locate_id=locate.locate_id
  where fm.fm_id=@FmId
go

grant execute on log_fax_as_response to uqweb, QManagerRole
/*
exec dbo.log_fax_as_response 14, '2003-01-17', 1

select top 20 * from fax_message


*/
