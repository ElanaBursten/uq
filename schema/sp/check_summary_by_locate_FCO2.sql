IF OBJECT_ID('dbo.check_summary_by_locate_FCO2') IS NOT NULL
	drop procedure dbo.check_summary_by_locate_FCO2
GO

/* A bit of a special case, because:
   - FCO2 updates FCO1
   - FCO2 receives the audits, but all tickets are in FCO1
*/

CREATE Procedure check_summary_by_locate_FCO2
	(
	@SummaryDate datetime,
	@CallCenter varchar(30)
	)
As
set nocount on

DECLARE @SummaryWindowStart datetime
DECLARE @SummaryWindowEnd datetime
DECLARE @SummaryDateEnd datetime
DECLARE @UpdateCallCenter varchar(30)

IF @CallCenter = 'FCO2'
begin
  SELECT @UpdateCallCenter = 'FCO1'
end
else 
  SELECT @UpdateCallCenter = @CallCenter

DECLARE @locates_found TABLE (
	lf_ticket_number varchar(20) NOT NULL,
        lf_client_code varchar(20) NOT NULL,
	PRIMARY KEY(lf_ticket_number, lf_client_code)
)

DECLARE @gn TABLE (
	client_code varchar (20) NOT NULL ,
	item_number varchar (20) NULL ,
	ticket_number varchar (20) NULL ,
	type_code varchar (20) NULL ,
	found int
)

SELECT @SummaryDateEnd = DATEADD(d, 1, @SummaryDate)

INSERT INTO @gn
SELECT sh.client_code, sd.item_number, sd.ticket_number, sd.type_code, 0 as found
 from summary_header sh
  inner join summary_detail sd
    on sh.summary_header_id=sd.summary_header_id
 where sh.summary_date=@SummaryDate
  and sh.call_center=@CallCenter
 order by sh.client_code, sd.item_number

INSERT INTO @locates_found
select ticket.ticket_number, locate.client_code
 from ticket
   inner join locate WITH (INDEX(locate_ticketid))
     on ticket.ticket_id=locate.ticket_id
 where ticket.transmit_date >= @SummaryDate
  and ticket.ticket_format in (@CallCenter, @UpdateCallCenter)
  and locate.status<>'-N'
 group by ticket.ticket_number, locate.client_code

update @gn set found=1
 from @locates_found
  where client_code=lf_client_code AND ticket_number=lf_ticket_number

update @gn set found=1
 from @locates_found
  where (client_code='XXX' or client_code='-') AND ticket_number=lf_ticket_number

select * from @gn
 order by found, client_code, ticket_number
GO
grant execute on check_summary_by_locate_FCO2 to uqweb, QManagerRole
/*

exec dbo.check_summary_by_locate_FCO2 '2004-02-09', 'FCO2'
*/
