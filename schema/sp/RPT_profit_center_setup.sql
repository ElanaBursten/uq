if object_id('dbo.RPT_profit_center_setup') is not null
	drop procedure RPT_profit_center_setup
go

create proc RPT_profit_center_setup
as
  set nocount on

  select '1) Profit Centers Set Up Correctly' as cat,
   pc.pc_code, pc.pc_name, max(e.short_name) as emp, 0 as n
   from profit_center pc
   inner join employee e on pc.pc_code = e.repr_pc_code
  group by pc.pc_code, pc.pc_name
  having count(*) = 1

  union

  select '2) Profit Centers With More Than One "Root"' as cat,
   pc.pc_code, pc.pc_name, '' as emp, count(*) as n
   from profit_center pc
   inner join employee e on pc.pc_code = e.repr_pc_code
  group by pc.pc_code, pc.pc_name
  having count(*) > 1

  union

  select '3) Profit Centers With No "Root" in the hierarchy' as cat,
   pc.pc_code, pc.pc_name, '' as emp, 0 as n
   from profit_center pc
  where pc.pc_code not in 
   (select repr_pc_code from employee where repr_pc_code is not null)

  union

  select '4) Hierachy Profit Centers missing from the PC table' as cat,
   repr_pc_code, '-', '' as emp, 0 as n
   from employee
    where repr_pc_code is not null
    and repr_pc_code not in (select pc_code from profit_center)

  order by 1, 2

  -- Payroll Profit Center Overrides:
  select pc.pc_code, pc.pc_name, e.short_name as emp
   from profit_center pc
   inner join employee e on pc.pc_code = e.payroll_pc_code
  order by pc.pc_code, e.short_name
GO
grant execute on RPT_profit_center_setup to uqweb, QManagerRole
/*
exec dbo.RPT_profit_center_setup
*/
