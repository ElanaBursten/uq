if object_id('dbo.queue_billing_reexport') is not null
  drop procedure dbo.queue_billing_reexport
go

create procedure dbo.queue_billing_reexport
  (
    @BillID int,
    @RequestBy int
  )
as
	declare @RowCnt int

	IF NOT EXISTS (select bill_id from billing_header where (bill_id=@BillID) and (center_group_id is not null))
	BEGIN
		raiserror ('Selected re-export cannot be queued because it has no Center Group Id', 16, 3)
		RETURN(3)
	END

	insert into billing_run (request_by, request_date, period_end_date, center_group_id, bill_id, span, request_type)
	select @RequestBy, GetDate(), bill_end_date, center_group_id, bill_id, period_type, 'X'
		from billing_header
		where (bill_id = @BillID)

	set @RowCnt = @@ROWCOUNT
	IF @RowCnt = 0
	BEGIN
		raiserror ('No re-export was queued for the requested Billing ID', 16, 1)
		RETURN(1)
	END
	IF @RowCnt > 1
	BEGIN
		raiserror ('Multiple re-exports were queued for the requested Billing ID', 16, 2)
		RETURN(2)
	END
GO

grant execute on queue_billing_reexport to uqweb, QManagerRole
/*
  exec queue_billing_reexport 10012, 777
*/
