-- Stored proc used to send new event notices
if object_id('dbo.add_new_event_notice') is not null
  drop procedure dbo.add_new_event_notice
go

create procedure add_new_event_notice (@AggrType int, @AggrID int, @Version int)
as

SET NOCOUNT ON
DECLARE @EventID integer
DECLARE @Dialog uniqueidentifier
DECLARE @Message VARCHAR(128)
 
SET @EventID = (SELECT event_id FROM event_log
  WHERE aggregate_type = @AggrType 
    AND aggregate_id = @AggrID
    AND version_num = @Version) 

IF (@EventID IS NULL) BEGIN
  RAISERROR('No event found with aggregate_type=%d, aggregate_id=%d, version_num=%d', 18, 1,
    @AggrType, @AggrID, @Version) WITH LOG
  RETURN
END
  
-- Begin dialog using service on contract
BEGIN DIALOG CONVERSATION @Dialog
  FROM SERVICE NewEventSendService
  TO SERVICE 'NewEventReceiveService'
  ON CONTRACT NewEventContract
  WITH ENCRYPTION = OFF

-- The message details are simply the new event_id
SET @Message = CAST(@EventID as VARCHAR(50));

-- Send message on Dialog
SEND ON CONVERSATION @Dialog
  MESSAGE TYPE NewEventMessage(@Message);

GO
GRANT EXECUTE ON add_new_event_notice TO uqweb, QManagerRole

/* 
select top 10 * from event_log

-- Add a new event notice
exec add_new_event_notice 1, 1000, 1


-- Examples of checking the queue:
-- This just peeks (ie leaves the messages queued)
SELECT CONVERT(VARCHAR(100), message_body) AS new_event_id
FROM NewEventReceiveQueue
ORDER BY queuing_order

-- peek at all pending ticket events
SELECT event_log.*, CONVERT(VARCHAR(100), message_body) AS new_event_id
FROM NewEventReceiveQueue
INNER JOIN event_log ON event_id = CONVERT(VARCHAR(100), message_body)
WHERE event_log.aggregate_type = 1
ORDER BY queuing_order

-- Receiving removes the message from the queue.
-- Receive 1 message from Receive Queue; each call to RECEIVE pops the next message.
RECEIVE CONVERT(VARCHAR(MAX), message_body) AS Message
FROM NewEventReceiveQueue
*/

