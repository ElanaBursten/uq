if object_id('dbo.update_ticket_status') is not null
  drop procedure dbo.update_ticket_status
GO

create proc update_ticket_status (@id int )
as

DECLARE @nOpen int, @nOngoing int
DECLARE @newKind varchar(20)
DECLARE @oldKind varchar(20)
DECLARE @firstCloseDate datetime
DECLARE @firstCloseEmp int

SELECT @nOpen = count(*) from locate (NOLOCK) where ticket_id=@id and closed=0 and status<>'-N'

SELECT @nOngoing = count(*) from locate (NOLOCK) where ticket_id=@id and closed=0 and status in ('O','OX')

SELECT @newKind = 'DONE'

SELECT @oldKind = kind FROM ticket (NOLOCK) WHERE ticket_id=@id

IF @nOnGoing>0 BEGIN
  SELECT @newKind = 'ONGOING'
END
ELSE BEGIN
  IF @nOpen>0 BEGIN
    IF @oldKind='DONE' or @oldKind='ONGOING'
      SELECT @newKind = 'NORMAL'
    ELSE
      SELECT @newKind = @oldKind
  END
END

UPDATE ticket SET kind = @newKind WHERE ticket_id=@id AND ((kind <> @newKind) or (kind IS NULL))

-- 1849 Changes:
IF (@newKind <> IsNull(@oldKind, '')) AND (@newKind = 'DONE') BEGIN --first time ticket is closed
  SELECT TOP 1 @firstCloseDate = closed_date, @firstCloseEmp = closed_by_id 
  FROM locate 
  WHERE ticket_id = @id AND closed = 1 AND active = 1 
  ORDER BY closed_date DESC
  
  UPDATE ticket_snap
  SET first_close_date = @firstCloseDate,
    first_close_emp_id = @firstCloseEmp,
    first_close_pc = dbo.get_historical_pc(@firstCloseEmp, @firstCloseDate)
  WHERE ticket_id = @id AND first_close_date IS NULL
END
-- End 1849

IF (@newKind = 'DONE') BEGIN  --update the work_status on the ticket every time the ticket is closed
  update ticket_snap
  SET work_status = dbo.get_ticket_work_status(@id)
  WHERE ticket_id = @id 
END
go

grant execute on update_ticket_status to uqweb, QManagerRole
GO


