if object_id('dbo.RPT_invoicenumberexception') is not null
	drop procedure dbo.RPT_invoicenumberexception
go

create proc RPT_invoicenumberexception(
  @StartDate datetime,
  @EndDate datetime
)
as
  
set nocount on

declare @ExceptionInvoiceIDs table (
  invoice_id int not null
)

-- Value of the current and next invoice IDs, for comparison
declare @current_invoice_id int, @next_invoice_id int

-- Use a cursor to iterate over the table and add any missing IDs to the returned result set
declare InvoiceCursor CURSOR FOR 
SELECT invoice_id from billing_invoice
where invoice_date between @StartDate and @EndDate
order by invoice_id

OPEN InvoiceCursor

FETCH NEXT FROM InvoiceCursor INTO @current_invoice_id

WHILE @@FETCH_STATUS = 0 BEGIN 
    FETCH NEXT FROM InvoiceCursor INTO @next_invoice_id
    IF @@FETCH_STATUS = 0 -- We only want to compare if there are more rows left
    BEGIN
      IF @next_invoice_id <> @current_invoice_id+1
      BEGIN
        insert into @ExceptionInvoiceIDs (invoice_id) values (@current_invoice_id)
      END
    
      SET @current_invoice_id = @next_invoice_id
    END
END 

CLOSE InvoiceCursor
DEALLOCATE InvoiceCursor

select * from @ExceptionInvoiceIDs
order by invoice_id


go
grant execute on RPT_invoicenumberexception to uqweb, QManagerRole

/*
exec dbo.RPT_invoicenumberexception '1/25/2004', '02/02/2005'
*/
