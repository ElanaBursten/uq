if object_id('dbo.get_tse_audit') is not null
  drop procedure dbo.get_tse_audit
go

create proc get_tse_audit(
  @WorkStart datetime,
  @WorkEnd datetime,
  @SheetsForID int)

as
  set nocount on

  declare @activity table (
    activity varchar(20) not null,
    activity_date datetime not null,
    activity_by int not null,
    activity_emp_number varchar(40) null,
    activity_short_name varchar(40) null,
    activity_entry_id int not null,
    activity_work_date datetime not null,
    activity_work_emp_id int not null
  )

  declare @oldvers table (
    new_entry_id int not null primary key,
    old_entry_id int not null
  )

  -- Get active time entries made my someone other than @SheetsForID
  insert into @activity
  select 'ENTRY',
    entry_date as activity_date,
    entry_by as activity_by,
    ent_emp.emp_number as activity_emp_number,
    ent_emp.short_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
  from timesheet_entry tse
  left join employee ent_emp on tse.entry_by = ent_emp.emp_id
  where work_date between @WorkStart and @WorkEnd
    and work_emp_id = @SheetsForID
    and entry_by <> work_emp_id
    and tse.status <> 'OLD'

  -- Get other days in the period 
  insert into @activity
  select 'ENTRY',
    entry_date as activity_date,
    entry_by as activity_by,
    ent_emp.emp_number as activity_emp_number,
    ent_emp.short_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
  from timesheet_entry tse
  left join employee ent_emp on tse.entry_by = ent_emp.emp_id
  where work_date between @WorkStart and @WorkEnd
    and work_emp_id = @SheetsForID
    and tse.status <> 'OLD'
    and tse.entry_id not in (select activity_entry_id from @activity)

  -- Get prior version for all the entry versions
  insert into @oldvers (new_entry_id, old_entry_id)
  select activity_entry_id, max(tse.entry_id)
  from @activity a
  inner join timesheet_entry tse on a.activity_work_date = tse.work_date
    and a.activity_work_emp_id = tse.work_emp_id
    and a.activity = 'ENTRY'
    and a.activity_entry_id > tse.entry_id
    and a.activity_by <> a.activity_work_emp_id
  group by activity_entry_id

  -- First result set is the well-ordered activity log with detail data
  select a.*, tse.*,
    e.emp_number as work_emp_number,
    e.short_name as work_short_name,
    et.code as emp_type,
    r.description as reason_changed_desc
  from @activity a
  inner join timesheet_entry tse on a.activity_entry_id = tse.entry_id
  left join employee e on a.activity_work_emp_id = e.emp_id
  left join reference et on tse.emp_type_id = et.ref_id
  left join reference r on tse.reason_changed = r.ref_id
  order by a.activity_work_date, a.activity_entry_id

  -- Second result set is details of the Before records.  QML code uses 
  -- this to highlight diffs
  select o.new_entry_id, tse.*, et.code as emp_type, null as reason_changed_desc
  from @oldvers o
  inner join timesheet_entry tse on o.old_entry_id = tse.entry_id
  left join reference et on tse.emp_type_id = et.ref_id
  order by o.new_entry_id

  -- summary of activity found
  select max(a.activity_date) as last_change_date, 
    min(a.activity_date) as first_change_date, 
    (select max(a1.activity_entry_id) from @activity a1 
     where a1.activity_date = max(a.activity_date)) as last_change_tse_id
  from @activity a
  where a.activity_work_emp_id <> a.activity_by
 
go
grant execute on get_tse_audit to uqweb, QManagerRole

/*
exec dbo.get_tse_audit 'Apr 1, 2009', 'Apr 17, 2009', 3510

*/

