if object_id('dbo.insert_responder_multi_queue') is not null
  drop procedure dbo.insert_responder_multi_queue
go

CREATE PROCEDURE insert_responder_multi_queue (
  @RespondTo varchar(20),
  @LocateId int
)
AS

insert into responder_multi_queue (respond_to, locate_id, ticket_format, client_code)
select @RespondTo, l.locate_id, t.ticket_format, l.client_code
from locate l
inner join ticket t
on l.ticket_id = t.ticket_id
where l.locate_id = @LocateId
  -- FMW1 client responses are only for tickets with a service_area_code
  and ((t.ticket_format = 'FMW1' and coalesce(t.service_area_code, '') <> '')
  or (t.ticket_format <> 'FMW1'))
GO
grant execute on insert_responder_multi_queue to uqweb, QManagerRole

/*
exec insert_responder_multi_queue 'client', {some-locate-id}
*/

