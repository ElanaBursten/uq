if object_id('dbo.RPT_tse_exception_by_emp') is not null
  drop procedure dbo.RPT_tse_exception_by_emp
GO

CREATE proc RPT_tse_exception_by_emp(
  @EmployeeID int,
  @StartDate datetime
)
as
  set nocount on

  if (DatePart(day, @StartDate) <> 1) 
  begin
    RAISERROR ('StartDate must be the first day of the month', 16, 1)
    return
  end

  declare @result table (
    r_work_date datetime, 
    r_work_date_end datetime,
    r_emp_id int,
    mgr_id int,
    mgr_short_name varchar(30) NULL,
    report_level integer NOT NULL,
    emp_number varchar(15) NULL,
    short_name varchar(30) NULL,
    last_name varchar(30) NULL,
    first_sync datetime NULL,
    last_sync datetime NULL,
    sync_span datetime NULL,
    first_status datetime NULL,
    last_status datetime NULL,
    status_span datetime NULL,
    hours_worked datetime NULL,
    reasons varchar(30) NOT NULL DEFAULT '',
    FSYL varchar(5) NULL,
    LSYE varchar(5) NULL,
    SYSP varchar(5) NULL,
    FSTL varchar(5) NULL,
    LSTE varchar(5) NULL,
    STSP varchar(5) NULL,
    NOHR varchar(5) NULL,
    hours_sync_discrep datetime NULL,
    hours_status_discrep datetime NULL,
    h_report_level integer,
    h_namepath varchar(450) NULL,
    h_idpath varchar(160) NULL,
    tse_id int NULL,
    pc_code varchar(15) NULL,
    primary key(r_work_date)
  )

  declare @EndDate datetime
  declare @AfterEndDate datetime
  declare @target datetime

  declare @work table (
    w_work_date datetime not null,
    w_emp_id int not null,
    minval datetime NULL,
    maxval datetime NULL,
    tse_id1 int NULL,
    primary key(w_work_date)
  )

  -- last date of the selected month
  select @EndDate = DateAdd(day, -1, DateAdd(month, 1, @StartDate))
  
  -- used to make the locate_status query more efficient
  select @AfterEndDate = DateAdd(d, 1, @EndDate)

  --- stub in a full month of records 
  declare @WorkDate datetime
  set @WorkDate = @StartDate
  while @WorkDate <= @EndDate
  begin
    insert into @result (r_work_date, r_work_date_end, r_emp_id, mgr_id, mgr_short_name, emp_number,
      short_name, last_name, report_level, h_namepath, h_idpath, pc_code)
    select @WorkDate, 
      DateAdd(minute, -1, DateAdd(Day, 1, @WorkDate)),
      e.emp_id, e.report_to, null, e.emp_number,
      e.short_name, e.last_name, 0, null, null, null
    from employee e
    where e.emp_id = @EmployeeID
    set @WorkDate = DateAdd(day, 1, @WorkDate)
  end

  update @result
    set mgr_short_name = e.short_name
  from employee e where e.emp_id = mgr_id
   
  -- gather first/last sync time
  insert into @work (w_work_date, w_emp_id, minval, maxval)
  select r_work_date, r_emp_id, min(coalesce(local_date,sync_date)) as first, max(coalesce(local_date,sync_date)) as last
  from sync_log inner join @result r on sync_log.emp_id=r_emp_id
  and sync_date between r_work_date and r_work_date_end 
  group by r_emp_id, r_work_date

  update @result set first_sync = minval, last_sync = maxval
  from @work
  where w_emp_id = r_emp_id and w_work_date = r_work_date

  delete from @work

  -- gather first/last status time
  insert into @work (w_work_date, w_emp_id, minval, maxval)
  select r_work_date, r_emp_id, min(status_date) as first, max(status_date) as last
    from locate_status ls with (INDEX(locate_status_insert_date))
     inner join @result r on ls.statused_by=r_emp_id
    and status_date between r_work_date and r_work_date_end
    and insert_date between r_work_date and r_work_date_end
    group by r_emp_id, r_work_date

  update @result set first_status = minval, last_status = maxval
  from @work
  where w_emp_id = r_emp_id and w_work_date = r_work_date

  delete from @work

  -- gather first/last locate hours time
  insert into @work (w_work_date, w_emp_id, minval, maxval)
  select r_work_date, r_emp_id, min(entry_date) as first, max(entry_date) as last
    from locate_hours lh --with (INDEX(locate_hours_entry_date))
     inner join @result r on lh.emp_id=r_emp_id
    and entry_date between r_work_date and r_work_date_end
    group by r_emp_id, r_work_date

  update @result set first_status = minval
  from @work
  where w_emp_id = r_emp_id and w_work_date = r_work_date
    and ((first_status is NULL) or (first_status > minval))

  update @result set last_status = maxval
  from @work
  where w_emp_id = r_emp_id and w_work_date = r_work_date
    and ((last_status is NULL) or (last_status < maxval))

  delete from @work

  -- gather working hours entered in new timesheet_entry table
  insert into @work (w_work_date, w_emp_id, minval, tse_id1)
  select r_work_date, 
    tse.work_emp_id,
    dateadd(minute, 
     (coalesce(reg_hours,0) + coalesce(ot_hours,0) + coalesce(dt_hours,0) )
        * 60,  '00:00:00') as hours,
   tse.entry_id
   from timesheet_entry tse
    inner join @result r on tse.work_emp_id=r.r_emp_id
      and tse.work_date between r_work_date and r_work_date_end
      and tse.status in ('ACTIVE', 'SUBMIT')
  
  update @result
    set hours_worked = minval,
        tse_id = tse_id1
  from @work
  where w_emp_id = r_emp_id and w_work_date = r_work_date

  -- calculate spans
  update @result set sync_span = last_sync - first_sync
  update @result set status_span = last_status - first_status

  -- apply rules...
  declare date_cursor cursor for 
    select r_work_date from @result
  open date_cursor
  fetch next from date_cursor into @WorkDate

  while @@FETCH_STATUS = 0
  begin
    select @target = dateadd(hour, 9, @WorkDate)
    update @result set reasons = reasons + 'FSYL ', FSYL='X'
      where first_sync > @target 
        and r_work_date = @WorkDate

    select @target = dateadd(hour, 16, @StartDate)
    update @result set reasons = reasons + 'LSYE ', LSYE='X'
      where last_sync < @target 
        and r_work_date = @WorkDate

    update @result set reasons = reasons + 'SYSP ', SYSP='X',
                       hours_sync_discrep = hours_worked - sync_span
      where sync_span + '03:00:00' < hours_worked 
        and r_work_date = @WorkDate

    select @target = dateadd(hour, 9, @StartDate)
    update @result set reasons = reasons + 'FSTL ', FSTL='X'
      where first_status > @target 
        and r_work_date = @WorkDate

    select @target = dateadd(hour, 16, @StartDate)
    update @result set reasons = reasons + 'LSTE ', LSTE='X'
      where last_status < @target 
        and r_work_date = @WorkDate

    update @result set reasons = reasons + 'STSP ', STSP='X',
        hours_status_discrep = hours_worked - status_span
      where status_span + '03:00:00' < hours_worked
        and r_work_date = @WorkDate

    update @result set reasons = reasons + 'NOHR ', NOHR='X'
      where status_span is not null and (hours_worked is null or hours_worked=0)
        and r_work_date = @WorkDate

    fetch next from date_cursor into @WorkDate
  end
  close date_cursor
  deallocate date_cursor

  -- result
  select * from @result
   order by r_work_date

  -- second result set with TSE detail rows for report
  select 
    tse.*
   from timesheet_entry tse
     inner join @result r on tse.work_emp_id=r.r_emp_id
       and tse.status in ('ACTIVE', 'SUBMIT')
       and tse.work_date between r_work_date and r_work_date_end     
    order by work_date

go
grant execute on RPT_tse_exception_by_emp to uqweb, QManagerRole

/*
1. First sync after 9 am
2. Last sync before 4 pm
3. Total time between the first and last sync is less than or equal to the total hours entered for that day less 3 (H-3)
4. First status after 9 am
5. Last status before 4 pm
6. Total time between first and last status is less than or equal to the total hours entered for that day less 3 (H-3)
7. No hours entered but at least 1 locate has been statused.

Eventually, we may want this is the query gets slow:
CREATE INDEX locate_hours_entry_date on locate_hours(entry_date)

exec dbo.RPT_tse_exception_by_emp 3510, '2006-03-01'
*/
