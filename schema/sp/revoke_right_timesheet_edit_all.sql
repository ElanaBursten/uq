IF OBJECT_ID('dbo.revoke_right_timesheet_edit_all') IS NOT NULL
  drop procedure dbo.revoke_right_timesheet_edit_all
GO

create procedure dbo.revoke_right_timesheet_edit_all
  (
    @EmpID int
  )
as

  set nocount on

  if (@EmpID is null)
    RAISERROR('An Employee ID required.', 18, 1) with log

  if exists (select emp_right_id from employee_right where emp_id=@EmpID and right_id=16) 
    update employee_right set allowed = 'N' where emp_id=@EmpID and right_id=16
GO

grant execute on revoke_right_timesheet_edit_all to uqweb, QManagerRole
GO

/*
  exec dbo.revoke_right_timesheet_edit_all 3510
*/

