if object_id('dbo.export_tasks3') is not null
  drop procedure dbo.export_tasks3
GO

CREATE PROCEDURE export_tasks3 (
  @CompanyName  varchar(20), -- locating_company.name or or '*' for all
  @ProfitCenter varchar(20), -- employee's work profit_center or '*' for all
  @StartDate    datetime,    -- Start date of first locate close date filter
  @EndDate      datetime,    -- End date of first locate close date filter
  @BillID       int = null,  -- billing id the export is run for (optional)
  @ExportAll    bit = 0      -- if true, export all tasks, including those already logged as exported
)
AS

set nocount on

create table #Tasks (
  customer_number varchar(20) null,   /* customer.customer_number = "Project ID", left filled to 10 characters */
  task_id varchar(30) not null,       /* SolomonProfitCenter(3) + MgrEmpNum(6) + LocEmpNum(6) = "Task ID" */
  short_name varchar(30) null,        /* employee.short_name = "Task Description" */
  manager_emp_number varchar(15) null /* employee.report_to.employee_number = "Supervisor Employee Number" */
)

create table #Locates (
  locate_id int not null,
  customer_number varchar(20) not null,
  emp_id int not null,
  first_close_date datetime not null,
  pc_code varchar(15) null,
  client_id int null,
  primary key (locate_id, customer_number)
)

if coalesce(@BillID, 0) > 0 
begin
-- Get all locates associated with the specified billing run
  insert into #Locates 
    (locate_id, emp_id, first_close_date, pc_code, client_id, customer_number)
  select distinct
    ls.locate_id, ls.first_close_emp_id, ls.first_close_date, ls.first_close_pc, bd.client_id,
    case when boc.customer_number = '' or boc.customer_number is null then cu.customer_number
    else boc.customer_number end as customer_number
  from billing_detail bd
  inner join client cl on bd.client_id = cl.client_id
  inner join customer cu on cl.customer_id = cu.customer_id
  inner join billing_output_config boc on cl.customer_id = boc.customer_id
  inner join locate_snap ls on bd.locate_id = ls.locate_id
  where bd.bill_id = @BillID
    and ls.first_close_emp_id is not null -- only closed locates

  /* -- TODO: More than 1 hour entry for a locate during the period violates #Locates primary key constraint;
    for now we don't export tasks for hourly charges on open locates.

  insert into #Locates -- get daily billed hours on still open locates
    (locate_id, emp_id, first_close_date, pc_code, client_id, customer_number)
  select distinct      
    bd.locate_id, lh.emp_id, lh.entry_date, dbo.get_historical_pc(lh.emp_id, lh.entry_date), bd.client_id,
    case when boc.customer_number = '' or boc.customer_number is null then cu.customer_number
    else boc.customer_number end as customer_number
  from billing_detail bd
  inner join client cl on bd.client_id = cl.client_id
  inner join customer cu on cl.customer_id = cu.customer_id
  inner join billing_output_config boc on cl.customer_id = boc.customer_id
  inner join locate_snap ls on bd.locate_id = ls.locate_id
  inner join locate_hours lh on bd.locate_hours_id = lh.locate_hours_id 
  where bd.bill_id = @BillID
    and ls.first_close_emp_id is null  
  */

end
else begin
-- Get all locates closed in the period for the relevant PCs
  insert into #Locates
    (locate_id, emp_id, first_close_date, pc_code, client_id, customer_number)
  select
    ls.locate_id, ls.first_close_emp_id, ls.first_close_date, ls.first_close_pc, l.client_id,
    cu.customer_number
  from locate_snap ls with (index(first_close_pc_close_date))
  inner join locate l on l.locate_id = ls.locate_id
  inner join client cl on l.client_id = cl.client_id
  inner join customer cu on cu.customer_id = cl.customer_id
  where ls.first_close_date >= @StartDate and ls.first_close_date < @EndDate
    and ((@ProfitCenter = '*') or (@ProfitCenter = ls.first_close_pc))

  -- Get any more locates closed in the period for the relevant PCs' Customers
  insert into #Locates (locate_id, emp_id, first_close_date, pc_code, client_id, customer_number)
  select ls.locate_id, ls.first_close_emp_id, ls.first_close_date, cu.pc_code, l.client_id, cu.customer_number
  from locate_snap ls with (index(first_close_pc_close_date))
  inner join locate l on l.locate_id = ls.locate_id
  inner join client cl on cl.client_id = l.client_id
  inner join customer cu on cu.customer_id = cl.customer_id
  where ls.first_close_date >= @StartDate and ls.first_close_date < @EndDate
    and ((@ProfitCenter = '*') or (@ProfitCenter = cu.pc_code))
    and ls.locate_id not in (select locate_id from #Locates)
end

-- Get a list of project/employee tasks for the period based on the closed locates
insert into #Tasks
select distinct
  coalesce(dbo.left_fill(loc.customer_number, '0', 10), '0000000000') as project,
  coalesce(pc.timesheet_num, '000') + dbo.format_emp_number(rte.emp_number) + dbo.format_emp_number(e.emp_number) as task_id,
  e.short_name,
  dbo.format_emp_number(rte.emp_number) as manager
from #Locates loc
  inner join employee e on e.emp_id = loc.emp_id
  left join locating_company lc on lc.company_id = e.company_id
  left join employee rte on rte.emp_id = e.report_to
  left join profit_center pc on pc.pc_code = loc.pc_code
where ((@CompanyName  = '*') or (@CompanyName  = lc.name))
  -- The profit center is already filtered when populating #Locates
order by 1, 2, 3

if @ExportAll = 0 
begin
  delete #Tasks
  from #Tasks t
    inner join exported_tasks et on 
      ((et.customer_number    = t.customer_number)    or ((et.customer_number is null)    and (t.customer_number is null))) and
      ((et.task_id            = t.task_id)            or ((et.task_id is null)            and (t.task_id is null))) and
      ((et.short_name         = t.short_name)         or ((et.short_name is null)         and (t.short_name is null))) and
      ((et.manager_emp_number = t.manager_emp_number) or ((et.manager_emp_number is null) and (t.manager_emp_number is null))) 

  insert into exported_tasks
    (customer_number, task_id, short_name, manager_emp_number)
  select
     customer_number, task_id, short_name, manager_emp_number
  from #Tasks
end

select * from #Tasks

drop table #Locates
drop table #Tasks

GO

grant execute on export_tasks3 to uqweb, QManagerRole
GO

/*
exec export_tasks3 '*', '*', 'April 1, 2008', 'April 15, 2008'
exec export_tasks3 'UTI', '*', 'April 1, 2008', 'April 15, 2008'
exec export_tasks3 'UTI', 'FRI', 'April 1, 2008', 'April 15, 2008'

exec export_tasks3 '*','*', 'March 8, 2009', 'March 16, 2009', 30911, 0
exec export_tasks3 '*','*', 'March 8, 2009', 'March 16, 2009', 30911, 1

select * from exported_tasks
Clear exported tasks:
delete from exported_tasks
select * from customer
select top 100 * from locate_snap
select * from locating_company
select * from dbo.employee_pc_data(null)
select * from dbo.employee_payroll_data2()
select top 100 * from locate_snap
select top 100 * from employee_history where short_name like '%alvarez%'
exec get_historical_pc()
select * from profit_center
*/
