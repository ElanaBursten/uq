if object_id('dbo.get_locate_location_status') is not null
  drop function dbo.get_locate_location_status
go

create function dbo.get_locate_location_status (@LocateID int, @TicketID int, @ClosedByID int)
returns varchar(15)
as
BEGIN
  --Get Threshold in feet from user's right limitation setting
  declare @Threshold int
  declare @GPSID int
  declare @LocateLat float
  declare @LocateLong float
  declare @TicketLat float
  declare @TicketLong float
  declare @DistanceFeet float
  declare @Result varchar(15)

  set @Threshold = (select dbo.get_gps_threshold(@ClosedByID))

  -- Get Ticket's coordinates
  select @TicketLat = coalesce(work_lat, 0),
         @TicketLong = coalesce(work_long, 0)
  from ticket
  where ticket_id = @TicketID
  
  -- Get Locate's coordinates
  set @GPSID = (select gps_id from locate where locate_id = @LocateID)
  select @LocateLat = latitude,
         @LocateLong = longitude
  from gps_position
  where gps_id = @GPSID
  
  set @Result = NULL
  if (@GPSID is NULL) or (@TicketLat = 0) or (@TicketLong = 0) 
    Return @Result
  
  -- Get the Distance for the locate, in feet
  set @DistanceFeet = ((select dbo.geoVicentyDistanceMeters(@TicketLat,@TicketLong,@LocateLat,@LocateLong)) * 3.280839895)
  
  --compare distance to threshold
  IF @DistanceFeet <= @Threshold begin
    set @Result = 'ONSITE'
  end
  else if @DistanceFeet > @Threshold begin
    set @Result = 'OFFSITE'
  end

  Return @Result
END;
go

grant execute on get_locate_location_status to uqweb, QManagerRole
go

/*
select dbo.get_locate_location_status(20000,1000,3510)
select dbo.get_locate_location_status(20002, 1001,3510)
select dbo.get_locate_location_status(20004, 1002,3510)
*/



