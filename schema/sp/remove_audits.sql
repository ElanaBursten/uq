if object_id('dbo.remove_audits') is not null
	drop procedure dbo.remove_audits
GO

CREATE Procedure remove_audits(
  @BeginDate datetime,
  @EndDate datetime,        
  @CallCenter varchar(30)
)
AS

-- first, delete detail records
delete from summary_detail
where summary_header_id in (
  select summary_header_id
  from summary_header
  where summary_date >= @BeginDate
  and summary_date <= @EndDate
  and call_center = @CallCenter)

-- then the header records
delete from summary_header
where summary_date >= @BeginDate
and summary_date <= @EndDate
and call_center = @CallCenter

GO

grant execute on remove_audits to uqweb, QManagerRole
/*
exec dbo.remove_audits '2005-04-28', '2005-04-30', 'FMW4'
*/
