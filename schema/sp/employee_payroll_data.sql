if object_id('dbo.employee_payroll_data') is not null
	drop function dbo.employee_payroll_data
go

create function employee_payroll_data (@limit_to_pc_code varchar(15))
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  x integer,
  emp_pc_code varchar(15) NULL
)
as
begin
  declare @wave int

  INSERT INTO @results (emp_id, x, emp_pc_code)
  select e.emp_id, e.report_to, e.repr_pc_code
    from employee e

  update @results set x=null where emp_pc_code is not null

  select @wave = 1
  WHILE @wave < 15
  BEGIN
    update @results
      set emp_pc_code = (select repr_pc_code from employee e2 where e2.emp_id=x)
    where
       emp_pc_code is null and x is not null

    update @results
      set x = (select report_to from employee e3 where e3.emp_id=x)
    where
       emp_pc_code is null and x is not null

    SELECT @wave = @wave + 1
  END

  declare @overrides table (
    ov_emp_id int not null primary key,
    ov_code varchar(15) not null
  )

  insert into @overrides
  select emp_id, payroll_pc_code
   from employee
   where payroll_pc_code is not null

  update @results
   set emp_pc_code = ov_code,
       x = (select min(emp_id) from employee where repr_pc_code=ov_code)
   from @overrides o
    where emp_id=ov_emp_id

  if @limit_to_pc_code is not null
    delete from @results
      where emp_pc_code <> @limit_to_pc_code or emp_pc_code is null
  return
end
GO

/*
select emp_id, emp_pc_code from dbo.employee_payroll_data(null)
select * from dbo.employee_payroll_data('108')
select * from employee where repr_pc_code='108'

select emp_id, emp_pc_code, pc_code, pc_name
 from dbo.employee_payroll_data(null)
  left join profit_center on emp_pc_code=pc_code

select pc_code, pc_name from profit_center

*/
