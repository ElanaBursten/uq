if object_id('dbo.RPT_OldOpenTickets') is not null
	drop procedure dbo.RPT_OldOpenTickets
go

create procedure dbo.RPT_OldOpenTickets (
  @TransmitStartDate datetime,
  @TransmitEndDate   datetime
)
as
  set nocount on

  select ticket_id, ticket_number, transmit_date, ticket_format, ticket_type, kind, work_type, due_date
    from ticket 
    where due_date < @TransmitEndDate
      and kind <> 'DONE'
    and ticket_id in (select ticket_id from locate where locate.assigned_to is not null)
  order by ticket_format, ticket_id

/*  -- Second result set to make debugging easier:
  select l.ticket_id, client_code, closed, l.status, client_id, client_id, assigned_to, added_by
    from ticket t
     inner join locate l with (index(locate_ticketid)) on t.ticket_id=l.ticket_id
    where transmit_date between @TransmitStartDate and @TransmitEndDate
      and kind <> 'DONE'
  order by t.ticket_format, t.ticket_id, l.client_code
*/
go

grant execute on RPT_OldOpenTickets to uqweb, QManagerRole

/*
exec dbo.RPT_OldOpenTickets '2004-01-20', '2004-07-21'


select count(distinct ticket_id) from locate where locate.assigned_to is not null
*/
