if object_id('dbo.RPT_DCL_NoSyncRangeATMOS') is not null
  drop procedure dbo.RPT_DCL_NoSyncRangeATMOS
GO

create procedure dbo.RPT_DCL_NoSyncRangeATMOS
  (
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer, 
  @ShowStatusHistory bit    -- 0 = False, 1 = True
  )
as

select
  ticket.ticket_number,
  convert(datetime, convert(varchar(12), locate.closed_date , 102) , 102) as closed_date_only,
  locate.closed_date,
  locate.client_code,
  locate.status,
  statuslist.status_name, -- ATMOS
  locate.modified_date,  -- most recent sync date
  locate.closed,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_cross, -- ATMOS
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.work_type, -- ATMOS
  ticket.work_date, -- ATMOS
  ticket.map_page,
  convert(varchar(15), ticket.work_lat) as work_lat, -- ATMOS
  convert(varchar(15), ticket.work_long) as work_long, -- ATMOS
  ticket.explosives, -- ATMOS
  ticket.ticket_type, -- ATMOS
  ticket.transmit_date,
  ticket.due_date,
  ticket.company as work_done_for, -- ATMOS
  ticket.con_name as company_doing_work, -- ATMOS
  ticket.con_address as company_address, -- ATMOS
  ticket.con_city as company_city, -- ATMOS
  ticket.con_state as company_state, -- ATMOS
  ticket.con_zip as company_zip, -- ATMOS
  ticket.caller, -- ATMOS
  ticket.caller_contact, -- ATMOS
  ticket.caller_phone, -- ATMOS
  ticket.caller_altcontact, -- ATMOS
  ticket.caller_altphone, -- ATMOS
  ticket.caller_cellular, -- ATMOS
  ticket.caller_fax, -- ATMOS
  ticket.caller_email, -- ATMOS
  replace (ticket.work_remarks, char(10),' ') as work_remarks, -- ATMOS
  locate.assigned_to_id -- ATMOS
from ticket with (index(ticket_transmit_date))
  inner join locate with (index(locate_ticketid)) on ticket.ticket_id = locate.ticket_id  
  inner join statuslist on statuslist.status = locate.status
where locate.status <> '-N'
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by locate.closed_date

go
grant execute on dbo.RPT_DCL_NoSyncRangeATMOS to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_NoSyncRangeATMOS '2010-09-01', '2010-09-05', 'BY1,L40,PBE,PSE,PTR,NSL,PNO,PWT,PSW,PMP,ANO,AWT,ASW,ASE,AMP', 'FDX1', 1, 0
exec dbo.RPT_DCL_NoSyncRangeATMOS '2010-09-01', '2010-09-02', 'BY1,L40,PBE,PSE,PTR,NSL,PNO,PWT,PSW,PMP,ANO,AWT,ASW,ASE,AMP', 'FDX1', 1, 1
*/

if object_id('dbo.RPT_DCL_SyncRangeATMOS') is not null
  drop procedure dbo.RPT_DCL_SyncRangeATMOS
GO

create procedure RPT_DCL_SyncRangeATMOS
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

select
  ticket.ticket_number,
  L.closed_date_only,
  L.closed_date,
  L.client_code,
  L.status,
  L.modified_date,  -- sync date
  L.closed,
  statuslist.status_name, -- ATMOS
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_cross, -- ATMOS
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.work_type, -- ATMOS
  ticket.work_date, -- ATMOS
  ticket.map_page,
  convert(varchar(15), ticket.work_lat) as work_lat, -- ATMOS
  convert(varchar(15), ticket.work_long) as work_long, -- ATMOS
  ticket.explosives, -- ATMOS
  ticket.ticket_type, -- ATMOS
  ticket.transmit_date,
  ticket.due_date,
  ticket.company as work_done_for, -- ATMOS
  ticket.con_name as company_doing_work, -- ATMOS
  ticket.con_address as company_address, -- ATMOS
  ticket.con_city as company_city, -- ATMOS
  ticket.con_state as company_state, -- ATMOS
  ticket.con_zip as company_zip, -- ATMOS
  ticket.caller, -- ATMOS
  ticket.caller_contact, -- ATMOS
  ticket.caller_phone, -- ATMOS
  ticket.caller_altcontact, -- ATMOS
  ticket.caller_altphone, -- ATMOS
  ticket.caller_cellular, -- ATMOS
  ticket.caller_fax, -- ATMOS
  ticket.caller_email, -- ATMOS
  replace (ticket.work_remarks, char(10),' ') as work_remarks, -- ATMOS
  ticket.due_date,
  L.assigned_to_id -- ATMOS
FROM 
 (select
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102) as closed_date_only,
  min(ls.status_date) as closed_date,
  locate.client_code,
  ls.status,
  min(ls.insert_date) as modified_date,  -- sync date
  locate.closed,
  locate.ticket_id,
  locate.locate_id,
  locate.Assigned_to_id 
 from locate_status ls
  inner join locate on ls.locate_id = locate.locate_id
 where ls.insert_date between @SyncDateFrom and @SyncDateTo
  and ls.status <> '-N'
  and locate.modified_date >= @SyncDateFrom  -- this one is a perf opt
  and locate.client_code in (select S from dbo.StringListToTable(@ClientCodeList)) 
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and (ls.status_date in (select max(locate_status.status_date)
                          from locate_status  
                          where locate_status.locate_id = locate.locate_id
                            and locate_status.status_date is not null)
  or (@ShowStatusHistory=1 and ls.status_date is not null))
 group by
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102),
  locate.client_code,
  ls.status,
  locate.closed,
  locate.ticket_id,
  locate.locate_id, 
  locate.Assigned_to_id   
 ) L
  inner join ticket on ticket.ticket_id = L.ticket_id  
  inner join statuslist on statuslist.status = L.status
where ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by L.closed_date

go

grant execute on RPT_DCL_SyncRangeATMOS to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_SyncRangeATMOS '2010-09-01', '2010-09-05', '2010-09-01', '2010-09-05', 'BY1,L40,PBE,PSE,PTR,NSL,PNO,PWT,PSW,PMP,ANO,AWT,ASW,ASE,AMP', 'FDX1', 1, 0
*/

if object_id('dbo.RPT_DailyClientLocatesATMOS') is not null
  drop procedure dbo.RPT_DailyClientLocatesATMOS
GO

create procedure dbo.RPT_DailyClientLocatesATMOS
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCodeList varchar(8000),
  @CallCenter varchar(30),
  @LocateClosed integer,
  @ShowStatusHistory bit
  )
as

if @SyncDateFrom='1900-01-01'  -- not filtering by status date, don't use ls table
  exec dbo.RPT_DCL_NoSyncRangeATMOS @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory
else
  exec dbo.RPT_DCL_SyncRangeATMOS @SyncDateFrom, @SyncDateTo, @XmitDateFrom, @XmitDateTo, @ClientCodeList, @CallCenter, @LocateClosed, @ShowStatusHistory

go
grant execute on dbo.RPT_DailyClientLocatesATMOS to uqweb, QManagerRole
/*
exec dbo.RPT_DailyClientLocatesATMOS '1900-01-01', '2199-01-01', '2010-09-01', '2010-09-05', 'BY1,L40,PBE,PSE,PTR,NSL,PNO,PWT,PSW,PMP,ANO,AWT,ASW,ASE,AMP', 'FDX1', 0, 0
exec dbo.RPT_DailyClientLocatesATMOS '2010-09-01', '2010-09-05', '2010-09-01', '2010-09-05', 'BY1,L40,PBE,PSE,PTR,NSL,PNO,PWT,PSW,PMP,ANO,AWT,ASW,ASE,AMP', 'FDX1', 0, 0
*/
