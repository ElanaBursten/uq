if object_id('dbo.RPT_initialstatus') is not null
	drop procedure dbo.RPT_initialstatus
go

create procedure dbo.RPT_initialstatus (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenter varchar(20)
)
as
  set nocount on 

  declare @results table (
    client_code varchar (15),
    status varchar(20),
    loc_client_id int,
    due_date datetime,
    ticket_number varchar(25),
    work_type varchar(90),
    ticket_type varchar(40),
    ticket_id int,
    client_name varchar(40),
    locator_id int,
    short_name varchar(30),
    initial_status_date datetime,
    last_sync_date datetime
  )

  declare @tix table (
    ticket_id int primary key,
    due_date datetime,
    ticket_number varchar(25),
    work_type varchar(90),
    ticket_type varchar(40)
  )
  /* Get the list of tickets in the date range for this call center */
  insert into @tix
    select ticket_id, due_date, ticket_number, work_type, ticket_type
    from ticket
    where (ticket.due_date between @StartDate and @EndDate)
      and (ticket.ticket_format = @CallCenter)

  /* Grab all of the locates on those tickets */
  insert into @results
  select
    locate.client_code,
    locate.status,
    locate.client_id,
    tix.due_date,
    tix.ticket_number,
    tix.work_type,
    tix.ticket_type,
    tix.ticket_id,
    client.client_name,
    assignment.locator_id,
    employee.short_name,
    -- get the initial status date for each locate
    (select min(locate_status.status_date) from locate_status
     where locate_status.locate_id = locate.locate_id and locate_status.status <> '-R') as initial_status_date,
    /* get the last sync date for each locator */
    (select max(sync_log.sync_date) from sync_log
     where sync_log.emp_id = assignment.locator_id) as last_sync_date
  from @tix tix
    inner join locate on locate.ticket_id = tix.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join assignment on assignment.locate_id = locate.locate_id
    inner join employee on assignment.locator_id = employee.emp_id
  where assignment.active = 1

  /* Return only the late or potentially late tickets */
  select
    case
      when (res.initial_status_date > res.due_date) then
        'LATE (statused)'
      when (res.last_sync_date > res.due_date) then
        'LATE (not statused)'
      else
        'POTENTIALLY LATE (might sync)'
    end as category,
    res.client_code, res.client_name, res.due_date, res.locator_id,
    res.ticket_number, res.status, res.initial_status_date,
    res.last_sync_date, res.work_type, res.short_name,
    res.ticket_type, res.ticket_id
  from
    @results res
  where
    (res.initial_status_date is null) or (res.initial_status_date > res.due_date)
  order by
    res.client_code,
    category,
    res.due_date,
    res.ticket_number
go

grant execute on RPT_initialstatus to uqweb, QManagerRole

if object_id('dbo.RPT_initialstatus_sum') is not null
	drop procedure dbo.RPT_initialstatus_sum
go

create procedure dbo.RPT_initialstatus_sum (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenter varchar(20)
)
as
 set nocount on 

 declare @totaltix int
 declare @latetix int
 declare @totalloc int
 declare @lateloc int

  select
    case
      when (#sum1.initial_status_date > #sum1.due_date) then
        'L' -- LATE
      when (#sum1.last_sync_date > #sum1.due_date) then
        'L' -- LATE
      else
        'P' -- POTENTIALLY LATE
    end as category,
    #sum1.ticket_id
  into #sum2
  from (
    select
      ticket.due_date,
      ticket.ticket_id,
      (select min(locate_status.status_date) from locate_status
       where locate_status.locate_id = locate.locate_id and locate_status.status <> '-R') as initial_status_date,
      (select max(sync_log.sync_date) from sync_log
       where sync_log.emp_id = assignment.locator_id) as last_sync_date
    from locate
      inner join ticket
        on ticket.ticket_id = locate.ticket_id
      inner join client
        on client.client_id = locate.client_id
      inner join assignment
        on assignment.locate_id = locate.locate_id
    where
      (ticket.due_date between @StartDate and @EndDate) and
      (client.call_center = @CallCenter) and
      (assignment.active = 1)
    ) as #sum1
  where
    (#sum1.initial_status_date is null) or (#sum1.initial_status_date > #sum1.due_date)

  select @totaltix = count(distinct ticket_id) from #sum2
  select @latetix = count(distinct ticket_id) from #sum2 where category = 'L'
  select @totalloc = count(*) from #sum2
  select @lateloc = count(*) from #sum2 where category = 'L'

  select
    @latetix as 'LateTickets',
    @totaltix-@latetix as 'PotLateTickets',
    @lateloc as 'LateLocates',
    @totalloc-@lateloc as 'PotLateLocates'
go

grant execute on RPT_initialstatus_sum to uqweb, QManagerRole
/*
exec dbo.RPT_initialstatus '2003-05-20', '2003-05-21', 'Atlanta'
*/
