if object_id('dbo.trans_billing') is not null
  drop procedure dbo.trans_billing
GO

create procedure trans_billing (
  @BillID int
)
as

set nocount on

-- Get all possible ways to bill each ticket_version_id, locate_id combination
select
  bh.bill_id, cl.customer_id, boc.which_invoice, l.client_code, cl.client_id,
  tv.ticket_version_id, tv.ticket_id, tv.transmit_date,
  substring(tv.image, 1, 250) as ticket_image_fragment,
  tv.source, l.assigned_to_id, l.closed, lp.plat, vg.group_code, l.locate_id,
  br.rate, br.line_item_text,
  coalesce(vg.group_code, t.work_city) as bill_city,
  tv.ticket_number, t.work_city, tv.ticket_format, tv.ticket_revision,
  /* These 2 are used to help select the best record for each
     ticket_version_id/locate_id */
  case when rtrim(coalesce(br.work_city, '')) = '' then 0 else 1 end
    as use_specific_rate,
  case when coalesce(boc.which_invoice, '') = '' then 0 else 1 end
    as use_specific_invoice
into #result_with_duplicates
from
  billing_header bh
  join center_group_detail cgd on cgd.center_group_id = bh.center_group_id and cgd.active = 1
  join billing_output_config boc on boc.center_group_id = bh.center_group_id
    and boc.transmission_billing = 1 
  join client cl on cl.call_center = cgd.call_center and cl.customer_id = boc.customer_id
  
  join ticket_version tv with (index(ticket_version_transmit_date)) on tv.ticket_format = cl.call_center and tv.transmit_date between bh.bill_start_date and bh.bill_end_date
  join ticket t with (index(pk_ticket_ticketid)) on t.ticket_id = tv.ticket_id  
  join locate l on l.ticket_id = t.ticket_id and l.client_code = cl.oc_code and l.status <> '-N' and l.added_by = 'PARSER'
  left join locate_plat lp on lp.locate_plat_id = (
    select top 1 locate_plat_id from locate_plat lp2
    where
       lp2.locate_id = l.locate_id
    order by lp2.locate_plat_id asc)

  left join value_group_detail vgd on vgd.match_value = lp.plat
  left join value_group vg on vg.value_group_id = vgd.value_group_id and
    boc.plat_map_prefix is not null and rtrim(boc.plat_map_prefix) <> '' and
    vg.group_code like boc.plat_map_prefix + '%'
  
  join billing_rate br on 
    coalesce(br.which_invoice, '') = coalesce(boc.which_invoice, '') and
    br.call_center = tv.ticket_format and
    br.billing_cc = l.client_code and
    br.bill_code = 'Trans' and
    br.rate is not null and
    -- Either a work_city match, or a blank/null default
    (br.work_city = coalesce(vg.group_code, t.work_city) or
	br.work_city is null or rtrim(br.work_city) = '') and    
    /* Work_county matches were removed from this version, and only blank/null
       defaults are allowed now */ 
    (br.work_county is null or rtrim(br.work_county) = '')
where
  bh.bill_id = @BillID  and

  /* The rest of this where clause is a list of special rules for excluding
     stuff by tv.ticket_format, l.client_code and substring(tv.image, 1, 250)
     (aka ticket fragment) */
     
  /* GENERAL rule for non-COX, non-AT&T tickets, this does not do the right
     thing for AT&T nor Cox */
     
  /* Ignore COX transmissions for non-COX clients, but don't delete AT&T (ATTD*
     and PTT*) and SDG transmissions that come direct from the customer: */
  not (tv.ticket_format = 'SCA1' and (not substring(tv.image, 1, 250) like '%UQSTSO%') and
    (l.client_code not in ('COX01', 'COX04') and (l.client_code not like 'PTT%') and
    (l.client_code not like 'ATTD%') and (l.client_code not like 'SDG%') and
    (l.client_code not in ('NCU01', 'NEU01', 'BCU01', 'CMU01', 'EAU01')))) and

  -- Ignore non-COX transmissions for COX clients:
  not (tv.ticket_format = 'SCA1' and l.client_code in ('COX01', 'COX04') and
    not substring(tv.image, 1, 250) like '%COX0%USAS%') and

  -- Specific rule for AT&T: AT&T trans only count if AT&T term in the upper left.
  not (tv.ticket_format = 'SCA1' and ((l.client_code like 'ATTD%') or
    (l.client_code like 'PTT%')) and
    not substring(tv.image, 1, 250) like '%' + l.client_code + '%USAS%') and

  -- Specific rule for SDG: The term code must appear in the the ticket header
  not (tv.ticket_format = 'SCA1' and (l.client_code like 'SDG%') and
    not substring(tv.image, 1, 250) like '%' + l.client_code + '%USAS%') and

  /* Specific rule for update tickets from SDG: Their term code must appear in
     the ticket header fragment */
  not (tv.ticket_format = 'SCA1' and
    l.client_code in ('NCU01', 'NEU01', 'BCU01', 'CMU01', 'EAU01') and
    not substring(tv.image, 1, 250) like '%SDG%USAS%') and

  not (tv.ticket_format = 'SCA1' and l.client_code like '_____G') and

  not (tv.ticket_format = 'NCA1' and
    not substring(tv.image, 1, 250) like '%' + l.client_code + '%')

/* Add a unique id to to help select the best record for each
   ticket_version_id/locate_id */
alter table #result_with_duplicates add id int identity 

/* Insert the best (i.e. "most specific") record for each
   ticket_version_id/locate_id */
insert into billing_transmission_detail (
  bill_id, customer_id, which_invoice, billing_cc, client_id,
  ticket_version_id, ticket_id, transmit_date, ticket_image_fragment,
  ticket_source, locator_id, locate_closed, plat, plat_group, locate_id, rate,
  line_item_text, bill_city, ticket_number, work_city, ticket_format,
  revision)
select
  bill_id, customer_id, which_invoice, client_code, client_id,
  ticket_version_id, ticket_id, transmit_date, ticket_image_fragment,
  source, assigned_to_id, closed, plat, group_code, locate_id, rate,
  line_item_text, bill_city, ticket_number, work_city, ticket_format,
  ticket_revision
from #result_with_duplicates t where not exists (
  select * from #result_with_duplicates t2
  where
    -- Group by ticket_version_id and locate_id
    t2.ticket_version_id = t.ticket_version_id and 
    t2.locate_id = t.locate_id and
    /* Select the "most specific" way to bill each ticket_version_id/locate_id,
       by comparing use_specific_invoice, then use_specific_rate. Use id as a
       tie breaker. */
    ((t.use_specific_invoice < t2.use_specific_invoice) or
    (t.use_specific_invoice = t2.use_specific_invoice and
    ((t.use_specific_rate < t2.use_specific_rate) or
    (t.use_specific_rate = t2.use_specific_rate and t.id < t2.id))))) 

drop table #result_with_duplicates

GO
grant execute on trans_billing to uqweb, QManagerRole

-- exec trans_billing 5001
