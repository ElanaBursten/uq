if object_id('dbo.find_grid_area_locator') is not null
  drop procedure dbo.find_grid_area_locator
GO

CREATE Procedure find_grid_area_locator
	(
	@Latitude float,
	@Longitude float
	)
As

select map_geo.map_id, map_geo.map_page_num, map_geo.x_part, map_geo.y_part,
       area.area_name, area.area_id, employee.emp_id, employee.short_name
 from map_geo (NOLOCK)
  LEFT JOIN map_cell (NOLOCK) ON map_geo.map_id=map_cell.map_id
                     AND map_geo.map_page_num=map_cell.map_page_num
                     AND map_geo.x_part = map_cell.x_part
                     AND map_geo.y_part = map_cell.y_part
  LEFT join area (NOLOCK) on map_cell.area_id = area.area_id
  LEFT join employee (NOLOCK) on area.locator_id = employee.emp_id
 WHERE @Latitude BETWEEN map_geo.min_lat AND map_geo.max_lat
 AND @Longitude BETWEEN map_geo.min_long AND map_geo.max_long
 AND employee.active = 1
 and employee.can_receive_tickets = 1
 ORDER BY 6 DESC

GO
grant execute on find_grid_area_locator to uqweb, QManagerRole

