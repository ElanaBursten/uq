/*
Test query on production-like sized data
Split out training employee hours and integrate into crosstab
*/

if object_id('dbo.RPT_timesheet_summary_by_pc') is not null
	drop procedure dbo.RPT_timesheet_summary_by_pc
go

create procedure RPT_timesheet_summary_by_pc (
	@StartDate datetime,
	@EndDate datetime,
    @EmpTypes varchar(200),
    @ApproveOnly bit,
	@FinalOnly bit)
as
  set nocount on

  declare @EmpTypeCodes table (
    emp_type varchar(15) not null
  )

  insert into @EmpTypeCodes
    select S from dbo.StringListToTable(@EmpTypes)

  select
    t.work_date,
    e.emp_pc_code,
    count(*) as num_timesheets,
    sum(t.reg_hours) as reg_hours,
    sum(t.ot_hours) as ot_hours,
    sum(t.dt_hours) as dt_hours,
    sum(t.callout_hours) as callout_hours,
    -- Sum of regular work periods only (no callouts, etc.)
    Sum(t.reg_hours) + Sum(t.ot_hours) + Sum(t.dt_hours) as work_hours,
    -- Work periods + calouts, etc. (no vac, jr, br, hol, leave, etc.)
    Sum(t.reg_hours) + Sum(t.ot_hours) + Sum(t.dt_hours) + Sum(t.callout_hours) as total_hours
  from timesheet_entry t
    inner join dbo.employee_payroll_data2() e on t.work_emp_id = e.emp_id
    inner join employee emp on e.emp_id = emp.emp_id
    inner join reference r on r.ref_id = emp.type_id
  where (t.work_date between @StartDate and @EndDate)
    and (t.status in ('ACTIVE', 'SUBMIT'))
    and ((@ApproveOnly = 0)  or (t.approve_Date is not null))
    and ((@FinalOnly   = 0)  or (t.final_approve_date is not null))
    and ((@EmpTypes    = '') or (r.code in (select emp_type from @EmpTypeCodes)))
  group by
    e.emp_pc_code, t.work_date
go

grant execute on RPT_timesheet_summary_by_pc to uqweb, QManagerRole
go

/*
select top 1000 * from timesheet_entry where status in ('submit', 'active') order by work_emp_id

declare @now datetime
select @now = getdate()
exec dbo.RPT_timesheet_summary_by_pc 'June 1, 2007', 'July 19, 2007', '', 0, 0

sp_help timesheet_entry
sp_help employee
sp_help reference
*/
