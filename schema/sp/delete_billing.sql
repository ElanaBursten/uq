if object_id('dbo.delete_billing') is not null
  drop procedure dbo.delete_billing
GO

CREATE Procedure delete_billing	( @BillID int )
As
begin
 if exists (select bill_id from billing_header where bill_id= @BillID and committed_date is not null)
 begin
   raiserror ('Cannot delete a billing that has been committed.', 16, 1)
   return 1
 end

 delete from billing_detail where bill_id= @BillID
 delete from billing_invoice where billing_header_id= @BillID
 delete from billing_header where bill_id= @BillID
end
GO

grant execute on delete_billing to uqweb, QManagerRole
/*
exec dbo.delete_billing 5001
*/
