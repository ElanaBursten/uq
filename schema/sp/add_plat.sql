IF OBJECT_ID('dbo.add_plat') IS NOT NULL
	drop procedure dbo.add_plat
GO


CREATE PROCEDURE add_plat(
  @LocateId int,
  @Plat varchar(20)
)
AS

if not exists (
  select * from locate_plat lp
  where lp.locate_id = @LocateId
  and @Plat = lp.plat
  and lp.active = 1)
begin
  insert locate_plat
  (locate_id, plat)
  values (@LocateId, @Plat)
  -- 'active' is set to 1 by default
  -- 'modified_date' and 'insert_date' are set to current date/time by default
  -- 'modified_by' and 'added_by' remain NULL
end

GO

grant execute on add_plat to uqweb, QManagerRole

/*
exec dbo.add_plat 1234, 'foo'
*/
