if object_id('dbo.get_invoice2') is not null
  drop procedure dbo.get_invoice2
go

create proc get_invoice2(@InvoiceID int)
as
begin
  select 'damage_invoice' as tname, *
  from damage_invoice
  where invoice_id = @InvoiceID

  select 'damage' as tname, *
  from damage
  where damage_id = 
   (select damage_id 
    from damage_invoice 
    where invoice_id = @InvoiceID)
end

go

grant execute on get_invoice2 to uqweb, QManagerRole
go


/*
exec get_invoice2 72735
exec get_invoice2 72423
*/
