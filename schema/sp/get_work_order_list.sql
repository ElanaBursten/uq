if object_id('dbo.get_work_order_list') is not null
  drop procedure dbo.get_work_order_list
go

create proc get_work_order_list (@emp_id int, @open_only bit, @num_days int)
as
  if @open_only = 1
    exec dbo.get_open_work_orders_for_emp @emp_id
  else begin
    exec dbo.get_old_work_orders_for_emp @emp_id, @num_days
  end
GO
grant execute on get_work_order_list to uqweb, QManagerRole

/*
exec dbo.get_work_order_list 3510, 1, 7
exec dbo.get_work_order_list 3510, 0, 15
*/
