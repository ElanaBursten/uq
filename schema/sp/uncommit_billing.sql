if object_id('dbo.uncommit_billing') is not null
  drop procedure dbo.uncommit_billing
GO

create Procedure uncommit_billing(@BillID int)
as
set nocount on
DECLARE @err int
BEGIN TRANSACTION

update locate
set invoiced=0
from billing_detail bd
where
 locate.locate_id = bd.locate_id
  and bd.bill_id = @BillID
  and ((locate_hours_id is null) or (locate_hours_id = 0))

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update locate_hours
set locate_hours.hours_invoiced=0
from billing_detail bd
where
  (bd.bill_id = @BillID)
  and (locate_hours.locate_hours_id = bd.locate_hours_id)
  and (bd.locate_hours_id > 0)
  and (bd.bill_code = 'HOURLY')

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update locate_hours
set locate_hours.units_invoiced=0
from billing_detail bd
where
  (bd.bill_id = @BillID)
  and (locate_hours.locate_hours_id = bd.locate_hours_id)
  and (bd.locate_hours_id > 0)
  and (bd.bill_code = 'UNITS')

SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

update billing_header
   set billing_header.[committed] = 0
 where
    billing_header.bill_id = @BillID
SELECT @err = @@error
IF @err <> 0 BEGIN ROLLBACK TRANSACTION RETURN @err END

COMMIT TRANSACTION

SELECT @err = @@error
IF @err <> 0 RETURN @err

GO

grant execute on uncommit_billing to uqweb, QManagerRole

/*
exec dbo.uncommit_billing 10
*/

