if object_id('dbo.check_summary_by_work_order') is not null
  drop procedure dbo.check_summary_by_work_order
GO

create procedure check_summary_by_work_order (
  @SummaryDate datetime,
  @CallCenter varchar(30)
)
as
set nocount on

-- TODO: Put this back once the transmit dates are fixed (see #2786)
/*
DECLARE @work_orders_found TABLE (
  wof_client_wo_number varchar(40) NOT NULL PRIMARY KEY
)
*/

DECLARE @gn TABLE (
  client_code varchar(20) NOT NULL,
  item_number varchar(20) NULL,
  ticket_number varchar(40) NULL,
  type_code varchar(20) NULL,
  found int
)

/* Get the list of work orders on the summary */
insert into @gn
select '---' as client_code, null as item_number, ad.client_wo_number, null as type_code, 0 as found
from wo_audit_header ah
  inner join wo_audit_detail ad on ah.wo_audit_header_id = ad.wo_audit_header_id
where ah.summary_date = @SummaryDate
  and ah.wo_source = @CallCenter
order by ad.client_wo_number

-- TODO: Put this back once the transmit dates are fixed (see #2786)
/*
-- Get the list of work orders that were received
insert into @work_orders_found
select work_order.client_wo_number
from work_order
where work_order.transmit_date >= @SummaryDate
  and work_order.wo_source = @CallCenter
group by work_order.client_wo_number

-- Mark them as found on the list from the summary
update @gn set found = 1
from @work_orders_found wof
where ticket_number = wof_client_wo_number
*/


-- TODO: Remove this temporary workaround after transmit dates are fixed (see #2786)
update @gn set found = 1
from work_order wo
where ticket_number = wo.client_wo_number 
  and wo.wo_source = @CallCenter

select * from @gn
order by found, ticket_number

go
grant execute on check_summary_by_work_order to uqweb, QManagerRole

/*
exec dbo.check_summary_by_work_order '2011-02-28', 'LAM01'
exec dbo.check_summary_by_work_order '2011-03-01', 'LAM01'
exec dbo.check_summary_by_work_order '2011-03-02', 'LAM01'
*/

