if object_id('dbo.sts_damage_estimate_exceptions') is not null
  drop procedure dbo.sts_damage_estimate_exceptions
GO

create procedure dbo.sts_damage_estimate_exceptions

as

set nocount on

declare @Results table (
  profit_center varchar(10),
  damage_id int,
  invoice_code varchar(10),
  invoice_total money,
  paid_total money,
  current_estimate money
)

insert into @Results (profit_center, damage_id, invoice_code, invoice_total, paid_total, current_estimate)
  select d.profit_center,
  d.damage_id,
  d.invoice_code,
  sum(coalesce(di.amount,0)) "invoice_total",
  sum(coalesce(di.paid_amount,0)) "paid_total",
  coalesce ((select top 1 de.amount from damage_estimate de
   where de.damage_id = d.damage_id order by de.modified_date desc), 0) "current_estimate"
  from damage d
  inner join damage_invoice di on (di.damage_id = d.damage_id and di.amount > 0.00)
  inner join office o on (o.profit_center = d.profit_center and o.company_name = 'STS')
  where d.invoice_code <> 'NOTUSED'
  group by d.profit_center, d.damage_id, d.invoice_code

set nocount off
select *, 'estimate_should_be' =
 CASE
   WHEN paid_total = 0 THEN invoice_total
   ELSE paid_total
 END
from @Results
where (paid_total > 0 and current_estimate <> paid_total)
or (paid_total <= 0 and invoice_total <> current_estimate)
order by profit_center, damage_id

go

grant execute on sts_damage_estimate_exceptions to uqweb, QManagerRole

/*
exec dbo.sts_damage_estimate_exceptions
*/
