if object_id('dbo.RPT_employee_rights') is not null
  drop procedure dbo.RPT_employee_rights
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 *
 * RightStatus Parameter:
 *    0 - only reports denied rights
 *    1 - only reports granted rights
 *    2 - both denied & granted rights
 */

CREATE PROCEDURE RPT_employee_rights (
  @ManagerId int,
  @EmployeeStatus int,
  @EmployeeId int,
  @RightStatus int,
  @RightIdList varchar(2000),
  @StartDate datetime, -- not used yet
  @EndDate datetime,  -- not used yet
  @OrigGrantStartDate datetime, -- not used yet
  @OrigGrantEndDate datetime -- not used yet
)
AS

set nocount on

-- selected employees list
declare @emps table (
  emp_id int not null primary key,
  emp_number varchar(15) null,
  emp_short_name varchar(30) null,
  emp_first_name varchar(20) null,
  emp_last_name varchar(30) null,
  emp_type varchar(50) null,
  emp_active bit not null,
  mgr_name varchar(30) null,
  emp_pc_code varchar(15) null,
  emp_company varchar(25) null)

-- selected rights list
declare @rights table (
  right_id integer not null primary key,
  right_code varchar(50) not null,
  right_description varchar(50) not null
  )

-- primary results
declare @emp_rights table (
  emp_id int not null,
  right_id int not null,
  allowed char(1) not null,
  emp_group varchar(30) null,
  right_limitation varchar(200) null,
  right_modified_date datetime null,
  right_modified_by varchar(30) null, -- sql server username
  orig_grant_date datetime,
  primary key (emp_id, right_id))

-- report parameter explaination
declare @params table (
  param_name varchar(30) not null,
  param_value varchar(100) null
)

insert into @emps(emp_id, emp_number, emp_short_name, emp_first_name, emp_last_name, 
  emp_type, emp_active, mgr_name, emp_pc_code, emp_company)
  select h_emp_id, 
    h_emp_number, 
    h_short_name, 
    h_first_name, 
    h_last_name, 
    coalesce(r.description, '-') as emp_type,
    h_active, 
    h_report_to_short_name,
    coalesce(h_eff_payroll_pc, '-') as pc,
    coalesce(h_company_name, '-') as company
  from dbo.get_report_hier4(@ManagerID, 0, 9, @EmployeeStatus) hier
  left join reference r on (r.ref_id = h_type_id)
  where ((@EmployeeId = -1) or (@EmployeeId = h_emp_id))

if coalesce(@RightIDList, '') = '' 
  insert into @rights select right_id, entity_data, right_description from right_definition
else
  insert into @rights 
    select right_id, entity_data, right_description from right_definition 
    inner join dbo.IdListToTable(@RightIdList) on right_id = ID

insert into @emp_rights (emp_id, right_id, allowed)
  select e.emp_id, 
    r.right_id, 
    case when dbo.emp_has_right(e.emp_id, r.right_id) = 1 then 'Y' else 'N' end as allowed
  from @emps e cross join @rights r

delete from @emp_rights
  where ((@RightStatus = 0) and (allowed = 'Y'))  -- keep only denied rights
    or ((@RightStatus = 1) and (allowed = 'N'))   -- keep only granted rights

-- get any group name associated with the granted rights
update @emp_rights
  set emp_group = coalesce(gd.group_name, '-') 
  from group_definition gd 
  inner join employee_group eg on (gd.group_id = eg.group_id)
  inner join group_right gr on (gr.group_id = eg.group_id)
  where gr.right_id = [@emp_rights].right_id 
  and eg.emp_id = [@emp_rights].emp_id
  and gr.allowed = 'Y'
  and gd.active = 1

update @emp_rights
  set right_limitation = case when [@emp_rights].allowed = 'Y' then er.limitation else '-' end,
    right_modified_date = er.modified_date,
    -- TODO: Need real values for these:
    right_modified_by = '?',
    orig_grant_date = null
  from employee_right er 
  where er.emp_id = [@emp_rights].emp_id 
    and er.right_id = [@emp_rights].right_id

-- output detailed results
select *
from @emps e 
inner join @emp_rights er on e.emp_id = er.emp_id
inner join @rights r on r.right_id = er.right_id
order by e.emp_company, e.emp_pc_code, e.mgr_name, e.emp_short_name, 
  r.right_description

-- list of requested rights if a subset was selected
select * from @rights where @RightIdList <> '' 

-- parameters used
declare @emp_name varchar(50)

set @emp_name = coalesce((select max(short_name) from employee where emp_id=@ManagerID),'-')
insert into @params values ('Employees Under:', @emp_name)

set @emp_name = coalesce((select max(short_name) from employee where emp_id=@EmployeeID),'-')
insert into @params values ('Rights For:', 
  case when @EmployeeId > 0 then @emp_name
    else 'All Employees'
  end)
  
insert into @params values ('Employee Status:', 
  case 
    when @EmployeeStatus=0 then 'Inactive Only'
    when @EmployeeStatus=1 then 'Active Only'
    when @EmployeeStatus=2 then 'All'
  end)
  
insert into @params values ('Right Status:', 
  case 
    when @RightStatus=0 then 'Denied Only'
    when @RightStatus=1 then 'Allowed Only'
    when @RightStatus=2 then 'All'
  end)
  
insert into @params values ('Selected Rights:',
  case
    when coalesce(@RightIDList, '') = '' then 'All'
    else 'Subset of rights selected'
  end)

/* These date params are not in use yet:
insert into @params values ('Original Grant Dates:', convert(varchar(20), @OrigGrantStartDate, 101) + ' - ' + convert(varchar(20), @OrigGrantEndDate, 101))
insert into @params values ('Modified Dates:', convert(varchar(20), @StartDate, 101) + ' - ' + convert(varchar(20), @EndDate, 101))
*/

select * from @params

GO

grant execute on RPT_employee_rights to uqweb, QManagerRole

/*
select * from dbo.get_effective_rights (3510)

Params:
exec RPT_employee_rights 
  @ManagerID=2676, 
  @EmployeeStatus=1, 
  @EmployeeId=-1, 
  @RightStatus=1,
  @RightIdList='44,21,42,148,22,43,2,35,34,20,26,156,23', 
  @StartDate='Sep 4, 2009', 
  @EndDate=null, 
  @OrigGrantStartDate=null,
  @OrigGrantEndDate=null

exec RPT_employee_rights @ManagerID=811, 
  @EmployeeStatus=1, 
  @EmployeeId=-1, 
  @RightStatus=2, 
  @RightIdList='2,42,43,44', 
  @StartDate='Sep 4, 2009', 
  @EndDate=null, 
  @OrigGrantStartDate=null,
  @OrignGrantEndDate=null

exec RPT_employee_rights 811, 1, -1, 1, 134, 'Sep 4, 2009', null, null, null

exec RPT_employee_rights 811, 1, 811, 1, null, 'Sep 4, 2009', null, null, null

*/
