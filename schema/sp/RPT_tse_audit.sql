if object_id('dbo.RPT_tse_audit') is not null
  drop procedure dbo.RPT_tse_audit
go

-- the meaning of the flag parameters is explained by the SQL later in the SP

create proc RPT_tse_audit(
  @WorkStart datetime,
  @WorkEnd datetime,
  @EntryStart datetime,
  @EntryEnd datetime,
  @ChangedBy int,
  @ChangedByID int,
  @SheetsFor int,
  @SheetsForID int,
  @ChangeType int
)
as
  set nocount on

  declare @sheetemps table (eid int not null primary key)

  declare @activity table (
    activity varchar(20) not null,
    activity_date datetime not null,
    activity_date_local datetime not null,
    activity_by int not null,
    activity_emp_number varchar(40) null,
    activity_short_name varchar(40) null,
    activity_entry_id int not null,
    activity_work_date datetime not null,
    activity_work_emp_id int not null
  )

  declare @oldvers table (
    new_entry_id int not null primary key,
    old_entry_id int not null
  )

  declare @ChangedByPerson varchar(40)
  declare @SheetsForPerson varchar(40)

  declare @params table (
    param_name varchar(30) not null,
    param_value varchar(80) null
  )

  if @SheetsFor=2 begin
    insert into @sheetemps (eid)
    select h_emp_id from dbo.get_hier(@SheetsForID, 0)
  end

  if @ChangeType=0 or @ChangeType=1
  insert into @activity
  select 'ENTRY',
    entry_date as activity_date,
    entry_date_local as activity_date_local,
    entry_by as activity_by,
    ent_emp.emp_number as activity_emp_number,
    ent_emp.short_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
   from timesheet_entry tse
     left join employee ent_emp on tse.entry_by = ent_emp.emp_id
   where work_date between @WorkStart and @WorkEnd
     and entry_date between @EntryStart and @EntryEnd

     and ( (@ChangedBy=0) or
           (@ChangedBy=1 and entry_by=work_emp_id) or
           (@ChangedBy=2 and entry_by<>work_emp_id) or
           (@ChangedBy=3 and entry_by=@ChangedByID))

     and ( (@SheetsFor=0) or
           (@SheetsFor=1 and work_emp_id=@SheetsForID) or
           (@SheetsFor=2 and work_emp_id in (select eid from @sheetemps) ) or
           (@SheetsFor=3 and work_emp_id<>entry_by))

  if @ChangeType=0 or @ChangeType=2
  insert into @activity
  select 'APPROVE',
    approve_date as activity_date, 
    approve_date_local as activity_date_local,
    approve_by as activity_by,
    app_emp.emp_number as activity_emp_number,
    app_emp.short_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
   from timesheet_entry tse
     left join employee app_emp on tse.approve_by = app_emp.emp_id
   where work_date between @WorkStart and @WorkEnd
     and approve_date between @EntryStart and @EntryEnd

     and ( (@ChangedBy=0) or
           (@ChangedBy=1 and approve_by=work_emp_id) or
           (@ChangedBy=2 and approve_by<>work_emp_id) or
           (@ChangedBy=3 and approve_by=@ChangedByID))

     and ( (@SheetsFor=0) or
           (@SheetsFor=1 and work_emp_id=@SheetsForID) or
           (@SheetsFor=2 and work_emp_id in (select eid from @sheetemps) ) or
           (@SheetsFor=3 and work_emp_id<>entry_by))

  if @ChangeType=0 or @ChangeType=2
  insert into @activity
  select 'FINAL',
    final_approve_date as activity_date, 
    final_approve_date_local as activity_date_local,
    final_approve_by as activity_by,
    fap_emp.emp_number as activity_emp_number,
    fap_emp.short_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
   from timesheet_entry tse
     left join employee fap_emp on tse.final_approve_by = fap_emp.emp_id
   where work_date between @WorkStart and @WorkEnd
     and final_approve_date between @EntryStart and @EntryEnd

     and ( (@ChangedBy=0) or
           (@ChangedBy=1 and final_approve_by=work_emp_id) or
           (@ChangedBy=2 and final_approve_by<>work_emp_id) or
           (@ChangedBy=3 and final_approve_by=@ChangedByID))

     and ( (@SheetsFor=0) or
           (@SheetsFor=1 and work_emp_id=@SheetsForID) or
           (@SheetsFor=2 and work_emp_id in (select eid from @sheetemps) ) or
           (@SheetsFor=3 and work_emp_id<>entry_by))

  -- Now go get the prior version for all the entry versions
  insert into @oldvers (new_entry_id, old_entry_id)
  select activity_entry_id, max(tse.entry_id)
    from @activity a
     inner join timesheet_entry tse
        on a.activity_work_date = tse.work_date
         and a.activity_work_emp_id = tse.work_emp_id
         and a.activity = 'ENTRY'
         and a.activity_entry_id > tse.entry_id
   group by activity_entry_id

  -- First result set is the well-ordered activity log with detail data
  select a.*, tse.*,
      e.emp_number as work_emp_number,
      e.short_name as work_short_name,
      et.code as emp_type,
      r.code as reason_changed_code
   from @activity a
    inner join timesheet_entry tse on a.activity_entry_id = tse.entry_id
    left join employee e on a.activity_work_emp_id = e.emp_id
    left join reference et on tse.emp_type_id = et.ref_id
    left join reference r on tse.reason_changed = r.ref_id
  order by a.activity_date, a.activity

  -- Second result set is details of the Before records.  The report
  -- client code can use this to highlight diffs
  select o.new_entry_id, tse.*,
      et.code as emp_type
    from @oldvers o
    inner join timesheet_entry tse on o.old_entry_id = tse.entry_id
    left join reference et on tse.emp_type_id = et.ref_id
  order by o.new_entry_id


  -- Third result set to explain the params

  insert into @params
  values ('Work Date:', convert(varchar(20), @WorkStart, 101) + ' - ' +
                        convert(varchar(20), @WorkEnd, 101))

  insert into @params
  values ('Activity Date (Server):', convert(varchar(20), @EntryStart, 101) + ' - ' +
                        convert(varchar(20), @EntryEnd, 101))

  set @ChangedByPerson = coalesce(
    (select max(short_name) from employee where emp_id=@ChangedByID),'-')
  set @SheetsForPerson = coalesce(
    (select top 1 short_name from employee where emp_id=@SheetsForID),'-')

  insert into @params
  values ('Activity By:',
    case when @ChangedBy=0 then 'Anyone'
         when @ChangedBy=1 then 'Worker'
         when @ChangedBy=2 then 'Anyone but the worker'
         when @ChangedBy=3 then 'Specific Person: ' + @ChangedByPerson
    end )

  insert into @params
  values ('Timesheets For:',
    case when @SheetsFor=0 then 'Any Worker'
         when @SheetsFor=1 then 'Specific Worker: ' + @SheetsForPerson
         when @SheetsFor=2 then 'Anyone Under: ' + @SheetsForPerson
         when @SheetsFor=3 then 'Anyone Else'
    end )

  insert into @params
  values ('Changes:',
    case when @ChangeType=0 then 'All Changes'
         when @ChangeType=1 then 'Time Entry/Edit Only'
         when @ChangeType=2 then 'Approval Only'
    end )

  select * from @params

go
grant execute on RPT_tse_audit to uqweb, QManagerRole

/*
exec dbo.RPT_tse_audit '2007-08-01', '2007-08-22', '2007-08-01', '2007-08-22', 0,0,0,0,1
exec dbo.RPT_tse_audit '2004-01-01', '2004-03-01', '2004-01-01', '2004-03-01', 0,0,0,0,1
exec dbo.RPT_tse_audit '2004-03-20', '2004-03-29', '2004-03-20', '2004-03-29', 0,0,0,0,0
*/

