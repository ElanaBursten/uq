if object_id('dbo.export_damages') is not null
  drop procedure dbo.export_damages
GO


CREATE PROCEDURE [dbo].export_damages(
	@short_period varchar(10) = null,
	@reexport bit = null
) AS
set nocount on

declare @damage table (  
  ticket_id					integer			null,
  project					varchar(20)		null,
  task						varchar(30)		null,
  account_category				varchar(30)		null,
  estimate_date					datetime		null,
  estimate_amount				decimal(12,2)		null,
  liability_amount				decimal(12,2)		null,
  change_in_liability_amount			decimal(12,2)		null,
  estimate_id					integer			null,
  claim_number					integer			null,
  ticket_number					varchar(20)		null,
  locate_type					varchar(15)		null,
  comments					varchar(50)		null,
  profit_center					varchar(15)		null,
  center_code					varchar(3)		null,
  locator_id					integer			null,
  invoice_code					varchar(10)		null,
  modifier					decimal(12,2)		null, 
  company_id					integer			null
)

declare @damage_list table (  
  estimate_id					integer			null
)

declare @starting datetime
declare @Ending datetime

-- get fiscal period dates at runtime
If @short_period is null 
	select @starting = starting, @ending = ending, @short_period = short_period
	from dycom_period 
	where getdate() between starting and ending
ELSE
	select @starting = starting, @ending = ending 
	from dycom_period 
	where short_period = @short_period

If @reexport = 1
	delete exported_damages  
	from exported_damages 
	join damage_estimate on damage_estimate.estimate_id = exported_damages.estimate_id
    where damage_estimate.modified_date between @starting and @ending

-- select last estimate record per claim
insert into @damage_list
select estimate_id 
from damage_estimate 
join damage on damage.damage_id = damage_estimate.damage_id
where damage_estimate.modified_date between @starting and @ending 
  and damage.invoice_code in ('CLOSCORP','DENIED','DISPUTED','EXPECT','MULTINV','PAID','PENDAPP')   
  and estimate_id in (select 
						max(estimate_id)
						from damage_estimate 
						join damage on damage.damage_id = damage_estimate.damage_id
						where damage_estimate.modified_date between @starting and @ending 
						  and damage.invoice_code in ('CLOSCORP','DENIED','DISPUTED','EXPECT','MULTINV','PAID','PENDAPP')  
						group by damage.uq_damage_id)

-- remove previously exported estimate to prevent dupplication
delete @damage_list
from @damage_list 
join exported_damages on exported_damages.estimate_id = [@damage_list].estimate_id 

Insert into @damage
select
	damage.ticket_id as ticket_id,
	dbo.left_fill(Isnull(customer.customer_number,0), '0', 10) as project, 
	null as task, 
	null as account_category,
	damage_estimate.modified_date as estimate_date,
	damage_estimate.amount,
    null as liability_amount,
    null as change_in_liability_amount,
	damage_estimate.estimate_id,
	damage.uq_damage_id as claim_number,
	ticket.ticket_number,
	client.utility_type as locate_type,
	damage_estimate.comment as comments, 
	damage.profit_center,
    null as center_code,
  dbo.get_locator_for_damage(damage.damage_id) as locator_id, 
	damage.invoice_code,
    cast(isnull(reference.modifier,0) as decimal(12,2)),
    null as company_id
from @damage_list
join damage_estimate on damage_estimate.estimate_id = [@damage_list].estimate_id
join damage on damage.damage_id = damage_estimate.damage_id
left outer join ticket on ticket.ticket_id = damage.ticket_id
left outer join client on client.client_id = damage.client_id
left outer join customer on customer.customer_id = client.customer_id
left outer join reference on reference.code = damage.invoice_code and reference.type = 'invcode'

update @damage
	set [@damage].company_id = employee.company_id
from @damage 
join employee on employee.emp_id = [@damage].locator_id

-- update Task
update @damage
	set task =	coalesce(pc.timesheet_num, '000') + 
				dbo.format_emp_number(rte.emp_number) +
				dbo.format_emp_number(e.emp_number),
    center_code = coalesce(pc.timesheet_num, '000')
from @damage
left join employee e on e.emp_id = [@damage].locator_id
left join profit_center pc on pc.pc_code = [@damage].profit_center
left join employee rte on rte.emp_id = e.report_to

-- If this claim had previous estimates set category to type ADJ 
update @damage
	Set account_category = 'DAMAGE_ADJ' 
from @damage
inner join exported_damages on exported_damages.claim_number = [@damage].claim_number

update @damage
	Set account_category = 'DAMAGE_NEW' 
from @damage
where account_category is null

Update @damage
set liability_amount = ((isnull(estimate_amount,0) * Modifier) )
from @damage

Update @damage
set change_in_liability_amount = (  
case 
when (isnull((select liability_amount from exported_damages where exported_damages.estimate_id = (select max(estimate_id) from exported_damages where exported_damages.claim_number = [@damage].claim_number group by exported_damages.claim_number) ),0) > 
      isnull(liability_amount,0) )
then  (isnull((select liability_amount from exported_damages where exported_damages.estimate_id = (select max(estimate_id) from exported_damages where exported_damages.claim_number = [@damage].claim_number group by exported_damages.claim_number) ),0) -
      isnull(liability_amount,0) )
else  (isnull(liability_amount,0) -
	  isnull((select liability_amount from exported_damages where exported_damages.estimate_id = (select max(estimate_id) from exported_damages where exported_damages.claim_number = [@damage].claim_number group by exported_damages.claim_number) ),0))
end )     
from @damage

Insert into exported_damages
select @short_period, estimate_id, project, task, claim_number, invoice_code, liability_amount, change_in_liability_amount
from @damage 
where [@damage].estimate_id not in (select estimate_id from exported_damages)

-- generate REV(Reverse) account
Insert into @damage
select
	ticket_id,
	dbo.left_fill(0, '0', 10) as project, 
	(coalesce(center_code, '000') + 
	 coalesce(dbo.left_fill(0, '0', 6), '000000') + 
	 coalesce(dbo.left_fill(0, '0', 6), '000000')) as task,
	'DAMAGE_REV',
	estimate_date,
    estimate_amount,
    liability_amount,
	change_in_liability_amount * -1,
	estimate_id,
	claim_number,
	ticket_number,
	locate_type,
	comments,
	profit_center,
	center_code,
	locator_id, 
    invoice_code,
    modifier, 
    company_id
from @damage

select 
    company_id,
    'TRANSACTION' as trans,
	project,
	task,
	account_category,
	estimate_date,
	change_in_liability_amount,
	claim_number,
	ticket_number,
	locate_type,
	comments
from @damage
order by claim_number, estimate_date, account_category
go

grant execute on export_damages to uqweb, QManagerRole
go
