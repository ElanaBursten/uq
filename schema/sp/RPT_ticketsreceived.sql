if object_id('dbo.RPT_ticketsreceived') is not null
	drop procedure dbo.RPT_ticketsreceived
go

create proc RPT_ticketsreceived(
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(8000)
)
as
  set nocount on 
  declare @due_date_limit datetime

  -- we set the due date limit 10 days out; every ticket is due long before that.
  select @due_date_limit = dateadd(day, 10, @EndDate)

  select
    call_center,
    ticket_date,
    count(ticket_id) as count
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      ticket.ticket_id
    from ticket
    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
     and ticket.due_date between @StartDate and @due_date_limit
    ) as t
  group by
    t.call_center,
    t.ticket_date
  order by
    t.call_center,
    t.ticket_date
go

grant execute on RPT_ticketsreceived to uqweb, QManagerRole

/*
exec dbo.RPT_ticketsreceived '2003-01-01', '2003-01-17', 'FBL1'
exec dbo.RPT_ticketsreceived '2003-05-16', '2003-05-17', 'FAQ1'
*/
