if object_id('dbo.get_old_damages_for_locator') is not null
  drop procedure dbo.get_old_damages_for_locator
go

create proc get_old_damages_for_locator (@locator_id int, @num_days int)
as
  set nocount on

  declare @since datetime
  select @since = DateAdd(Day, -@num_days, GetDate())

  select d.damage_id, d.damage_type, d.damage_date, d.uq_notified_date, 
    d.location, d.city, d.county, d.state, d.utility_co_damaged, d.remarks, d.investigator_id,
    d.investigator_narrative, d.excavator_company, d.excavator_type, d.excavation_type,
    d.ticket_id, t.ticket_number, t.ticket_format, d.profit_center, d.due_date,
    d.closed_date, d.facility_type, d.facility_size, d.facility_material, d.uq_damage_id,
    d.modified_date,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    1 as damage_is_visible
  from damage d
  left join ticket t on d.ticket_id = t.ticket_id
  left join reference r on d.work_priority_id = r.ref_id
  where d.damage_id in (
    select damage_id from damage
     where investigator_id = @locator_id
      and (d.closed_date >= @since or d.closed_date is null)
   )

go
grant execute on get_old_damages_for_locator to uqweb, QManagerRole
/* 
exec dbo.get_old_damages_for_locator 1182, 7
exec dbo.get_old_damages_for_locator 1182, 15
exec dbo.get_old_damages_for_locator 4121, 15
exec dbo.get_old_damages_for_locator 4121, 30
exec dbo.get_old_damages_for_locator 6357, 7
*/
