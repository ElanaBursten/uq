IF OBJECT_ID('dbo.check_summary_by_ticket_serial') IS NOT NULL
	drop procedure dbo.check_summary_by_ticket_serial
GO

CREATE Procedure check_summary_by_ticket_serial
	(
	@SummaryDate datetime,
	@CallCenter varchar(30)
	)
As
set nocount on

DECLARE @UpdateCallCenter varchar(30)

-- not a very elegant way to do this, but the database has no notion of what is
-- an update call center and what isn't, and currently there are only a few
IF @CallCenter = 'FMW2'
begin
  select @UpdateCallCenter = 'FMW1'
end
else if @CallCenter = 'FAQ2'
begin
  select @UpdateCallCenter = 'FAQ1'
end
else if @CallCenter = 'LQW1'
begin
  select @UpdateCallCenter = 'LWA1'
end
else if @CallCenter = 'NewJersey2'
begin
  select @UpdateCallCenter = 'NewJersey'
end
else --raiserror ('check_summary_by_ticket_serial must have a valid update call center', 16, 1)
  select @UpdateCallCenter = @CallCenter

DECLARE @SummaryWindowStart datetime
DECLARE @SummaryWindowEnd datetime
DECLARE @SummaryDateEnd datetime

DECLARE @tickets_found TABLE (
	tf_ticket_number varchar(20) NOT NULL,
        tf_serial_number varchar(60) NOT NULL
)

SELECT @SummaryDateEnd = DATEADD(d, 1, @SummaryDate)

DECLARE @gn TABLE (
	client_code varchar (20) NOT NULL ,
	item_number varchar (20) NULL ,
	ticket_number varchar (20) NULL ,
        serial_number varchar(60) NULL,
	type_code varchar (20) NULL ,
	found int
)

-- Get the list of tickets on the summary
INSERT INTO @gn
select '---' AS client_code, sd.item_number, '', sd.ticket_number, 
                sd.type_code, 0 as found
 from summary_header sh
  inner join summary_detail sd
    on sh.summary_header_id=sd.summary_header_id
 where sh.summary_date=@SummaryDate
  and sh.call_center=@CallCenter
 order by sd.item_number, sd.ticket_number

-- Get the list of tickets that were received
/*
INSERT INTO @tickets_found
select ticket.ticket_number, ticket.serial_number
 from ticket
 where ticket.transmit_date >= (@SummaryDate-1)
  and (ticket.ticket_format=@CallCenter 
       or ticket.ticket_format = @UpdateCallCenter)
  and ticket.serial_number is not null
 group by ticket.serial_number, ticket.ticket_number
*/
INSERT INTO @tickets_found
select ticket.ticket_number, ticket.serial_number
from ticket
where ticket.serial_number is not null
and ticket.serial_number in (select serial_number from @gn)
and (ticket.ticket_format = @CallCenter
  or ticket.ticket_format = @UpdateCallCenter)
group by ticket.serial_number, ticket.ticket_number

-- Mark them as found on the list from the summary
update @gn 
 set 
  found=1, 
  ticket_number=tf_ticket_number,
  serial_number=tf_serial_number
 from @tickets_found tf
  where serial_number=tf_serial_number

update @gn
set ticket_number=serial_number
where ticket_number = '' or ticket_number is null

select * from @gn
 order by found, client_code, ticket_number
GO
grant execute on check_summary_by_ticket_serial to uqweb, QManagerRole

/*
exec dbo.check_summary_by_ticket_serial '2003-05-16', 'FAQ2'
exec dbo.check_summary_by_ticket_serial '2003-06-13', 'FMW2'
*/
