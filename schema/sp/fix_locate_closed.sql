if object_id('dbo.fix_locate_closed') is not null
	drop procedure dbo.fix_locate_closed
GO

CREATE Procedure fix_locate_closed
as

declare @locates_to_close table (locate_id int not null primary key)

-- Find erroneously open locates
insert into @locates_to_close
select locate_id
 from locate (NOLOCK)
 where status='-N' and (closed is null or closed=0)
 and locate_id > ((select max(locate_id) from locate) - 1000000) 

-- close
update locate set closed=1 where locate_id IN (select locate_id from @locates_to_close)

go

/*
exec dbo.fix_locate_closed

-- SQL to investigate an errant ticket bob reported:
select * from ticket where ticket_number='0247568'
select * from locate where ticket_id in 
 (select ticket_id from ticket where ticket_number='0247568')

*/
