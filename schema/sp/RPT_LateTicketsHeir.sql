if object_id('dbo.RPT_LateTicketsHeir_3') is not null
  drop procedure dbo.RPT_LateTicketsHeir_3
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

CREATE Procedure dbo.RPT_LateTicketsHeir_3
    (
        @StartDate datetime,
        @EndDate datetime,
        @CallCenterCodes varchar(800),
        @SortBy char(1),
        @NowDate datetime,
        @ManagerID int,
        @EmployeeStatus int,
        @Arrival bit = 0
     )

As
  SET NOCOUNT ON

  DECLARE @emps TABLE (
	emp_id int not null primary key,
	emp_number varchar(25) null,
	short_name varchar(30) null
  )

  declare @CallCenterList table (
    center varchar(100) primary key
  )
  insert into @CallCenterList
    select S from dbo.StringListToTable(@CallCenterCodes)

  declare @results table (
    category varchar(40) NULL,
    client_code varchar (10) NULL ,
    client_name varchar (40)  NOT NULL ,
    due_date datetime NULL ,
    locator_id int NOT NULL ,
    short_name varchar (30)  NULL ,
    ticket_number varchar (20)  NOT NULL ,
    ticket_type varchar (38)  NULL ,
    status varchar (5) NOT NULL ,
    closed bit NOT NULL ,
    closed_date datetime NULL ,
    last_sync_date datetime NULL ,
    work_type varchar (90)  NULL,
    ticket_id int NOT NULL,
    call_center varchar (20) NOT NULL,
    arrival_date datetime NULL
  )

  if @ManagerID=0 

  INSERT INTO @results
    SELECT
      '---' as Category,
      locate.client_code,
      client.client_name,
      ticket.due_date,
      assignment.locator_id,
      emps.short_name,
      ticket.ticket_number,
      ticket.ticket_type,
      locate.status,
      locate.closed,
      case WHEN
        locate.closed = 1 then locate.closed_date
        ELSE NULL
      END AS
       closed_date,

      (SELECT MAX(sync_log.local_date) FROM sync_log
        WHERE assignment.locator_id = sync_log.emp_id)
         AS last_sync_date,
      ticket.work_type,
      ticket.ticket_id,
      ticket.ticket_format,
      (select min(a.arrival_date) from jobsite_arrival a 
        where a.ticket_id = locate.ticket_id 
          and a.active = 1) as arrival_date
    FROM
      locate with (INDEX(locate_ticketid))
      INNER JOIN ticket
       ON ticket.ticket_id = locate.ticket_id

      INNER JOIN assignment with (INDEX(assignment_locateid_locatorid))
       ON assignment.locate_id = locate.locate_id
       AND assignment.active=1

      INNER JOIN client
       ON client.client_id = locate.client_id
      INNER JOIN employee emps
       ON emps.emp_id = assignment.locator_id
      INNER JOIN @CallCenterList ccl ON ccl.center=ticket.ticket_format

    WHERE
      ((@Arrival = 0 AND (ticket.due_date BETWEEN @StartDate AND @EndDate) AND
      ((locate.closed = 1 AND locate.closed_date > ticket.due_date) OR
       (locate.closed = 0) AND due_date < @NowDate)) OR
      (@Arrival = 1 AND (ticket.due_date BETWEEN @StartDate AND @EndDate))) AND
      (ticket.ticket_format <> 'ATLANTA' OR  -- special case to ignore Emergencies & Ongoings in Atlanta
       (ticket.kind <> 'EMERGENCY' AND locate.status <> 'O'))
  else begin
  -- Collect the emps
  insert into @emps
  select h_emp_id, h_emp_number, h_short_name
   from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)

  INSERT INTO @results
    SELECT
      '---' as Category,
      locate.client_code,
      client.client_name,
      ticket.due_date,
      assignment.locator_id,
      emps.short_name,
      ticket.ticket_number,
      ticket.ticket_type,
      locate.status,
      locate.closed,
      case WHEN
        locate.closed = 1 then locate.closed_date
        ELSE NULL
      END AS
       closed_date,

      (SELECT MAX(sync_log.local_date) FROM sync_log
        WHERE assignment.locator_id = sync_log.emp_id)
         AS last_sync_date,
      ticket.work_type,
      ticket.ticket_id,
      ticket.ticket_format,
      (select min(a.arrival_date) from jobsite_arrival a 
       where a.ticket_id = locate.ticket_id 
         and a.active = 1) as arrival_date
    FROM
      locate with (INDEX(locate_ticketid))
      INNER JOIN ticket
       ON ticket.ticket_id = locate.ticket_id

      INNER JOIN assignment with (INDEX(assignment_locateid_locatorid))
       ON assignment.locate_id = locate.locate_id
       AND assignment.active=1

      INNER JOIN client
       ON client.client_id = locate.client_id
      INNER JOIN @emps emps
       ON emps.emp_id = assignment.locator_id
      INNER JOIN @CallCenterList ccl ON ccl.center=ticket.ticket_format

    WHERE
      ((@Arrival = 0 AND (ticket.due_date BETWEEN @StartDate AND @EndDate) AND
      ((locate.closed = 1 AND locate.closed_date > ticket.due_date) OR
       (locate.closed = 0) AND due_date < @NowDate)) OR
      (@Arrival = 1 AND (ticket.due_date BETWEEN @StartDate AND @EndDate))) AND
      (ticket.ticket_format <> 'ATLANTA' OR  -- special case to ignore Emergencies & Ongoings in Atlanta
       (ticket.kind <> 'EMERGENCY' AND locate.status <> 'O'))
  END

  delete from @results
    where @Arrival = 1 and arrival_date <= due_date and due_date >= @NowDate

  declare @late_basis varchar(8)
  set @late_basis = case @Arrival when 1 then 'arrived' else 'closed' end

  UPDATE @results
  SET
    Category =
    case
      when (@Arrival = 0 and closed = 1 and closed_date <= due_date)
        or (@Arrival = 1 and arrival_date <= due_date) then 
        'NOT LATE'
      when (@Arrival = 0 and closed = 1 and closed_date > due_date) 
        or (@Arrival = 1 and arrival_date > due_date) then
        'LATE (' + @late_basis + ')'
      when (@Arrival = 0 and closed = 0 and due_date < @NowDate and last_sync_date > due_date) then
        'LATE (not closed)'
      when (@Arrival = 1 and arrival_date is null and last_sync_date > due_date) then
        'LATE (not arrived)' 
      when (@Arrival = 0 and closed = 0 and due_date < @NowDate) 
        or (@Arrival = 1 and arrival_date is null and due_date < @NowDate) then 
        'POTENTIALLY LATE (might sync)'
      else
        'STILL TIME (not yet due)'
    end

  If @SortBy = 'C'
      SELECT  *   FROM @results   ORDER BY 2, 1, 4, 7

  If @SortBy = 'L'
      SELECT *   FROM @results    ORDER BY 6, 2, 4

  DECLARE @totaltix int
  DECLARE @latetix int
  DECLARE @totalloc int
  DECLARE @lateloc int
  /* then return another recordset with the totals */
  SELECT @totaltix = count(distinct ticket_id) FROM @results
  SELECT @latetix = count(distinct ticket_id) FROM @results where category like 'LATE%'
  SELECT @totalloc = count(*) FROM @results
  SELECT @lateloc = count(*) FROM @results where category like 'LATE%'

  SELECT @latetix AS 'LateTickets', @totaltix-@latetix AS 'PotLateTickets',
         @lateloc AS 'LateLocates', @totalloc-@lateloc AS 'PotLateLocates',
         (select short_name from employee where emp_id=@ManagerId) as man_name,
	 @CallCenterCodes as CallCenter
go

grant execute on RPT_LateTicketsHeir_3 to uqweb, QManagerRole


/*
exec RPT_LateTicketsHeir_3 '2004-06-18', '2004-06-19', 'NewJersey', 'C', '2004-06-20T15:30:00', 212, 1
exec RPT_LateTicketsHeir_3 '2004-06-18', '2004-06-19', 'NewJersey', 'C', '2004-06-20T15:30:00', 0, 1
exec RPT_LateTicketsHeir_3 '2004-06-18', '2004-06-19', 'FGV1', 'C', '2004-06-20T15:30:00', 0, 1
exec RPT_LateTicketsHeir_3 '2001-01-01', '2008-01-01', 'FNV1,FNV2', 'C', '2008-01-01T15:30:00', 0, 2

exec RPT_LateTicketsHeir_3 '2009-04-25', '2009-04-29', 'FMW1,FMW3', 'C', '2009-05-11T11:30:00', 0, 1, 0
exec RPT_LateTicketsHeir_3 '2009-04-25', '2009-04-29', 'FMW1,FMW3', 'C', '2009-05-11T11:30:00', 0, 1, 1
*/
