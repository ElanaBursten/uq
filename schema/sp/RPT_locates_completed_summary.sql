if object_id('dbo.RPT_locates_completed_summary') is not null
  drop procedure dbo.RPT_locates_completed_summary
go

create proc RPT_locates_completed_summary (
  @StartDate datetime,
  @EndDate datetime,
  @ManagerID integer,
  @EmployeeTypes varchar(2000)
)
as
  set nocount on

  declare @results table (
    pc_code varchar(15) not null,
    emp_type integer not null,
    work_date datetime not null,
    locates_marked integer,
    locates_cleared integer,
    locates_other integer,
    locates_closed integer,
    primary key (pc_code, emp_type, work_date)
    )

  declare @emps table (
    emp_id integer not null primary key,
    emp_type integer
    )

  -- Get list of employees to consider
  if @EmployeeTypes = '*' 
    insert into @emps 
      select hier.h_emp_id, hier.h_type_id
      from dbo.get_report_hier3(@ManagerID, 0, 1000, 1) hier
      where h_type_id is not null
  else 
    insert into @emps 
      select hier.h_emp_id, hier.h_type_id
      from dbo.get_report_hier3(@ManagerID, 0, 1000, 1) hier
      inner join dbo.IdListToTable(@EmployeeTypes) t on hier.h_type_id = t.ID

  insert into @results
    (pc_code, emp_type, work_date, locates_marked, locates_cleared, locates_other, locates_closed)
    select ls.first_close_pc, 
      e.emp_type, 
      convert(datetime, convert(varchar, ls.first_close_date, 101)) as close_date,
      sum(case when l.status = 'M' then 1 else 0 end) as marked_count,
      sum(case when l.status = 'C' then 1 else 0 end) as cleared_count,
      sum(case when (l.status <> 'M' and l.status <> 'C') then 1 else 0 end) as other_count,
      sum(case when l.closed = 1 then 1 else 0 end) as closed_count
    from locate_snap ls with (index(first_close_date))
    inner join locate l on ls.locate_id = l.locate_id
    inner join @emps e on ls.first_close_emp_id = e.emp_id
    where ls.first_close_date >= @StartDate and ls.first_close_date < @EndDate
    group by ls.first_close_pc, 
      e.emp_type,
      convert(datetime, convert(varchar, ls.first_close_date, 101))

  select coalesce(l.name, '-') as locating_company, 
    r.pc_code + ' ' + p.pc_name as profit_center,
    et.description as employee_type,
    r.work_date,
    r.locates_marked,
    r.locates_cleared,
    r.locates_other,
    r.locates_closed
  from @results r
  inner join profit_center p on r.pc_code = p.pc_code
  inner join reference et on et.ref_id = r.emp_type
  left join locating_company l on l.company_id = p.company_id
  order by l.name, r.pc_code, et.sortby, et.description, r.work_date

  if @EmployeeTypes = '*' 
    select 'All' as employee_type
  else
    select r.description as employee_type 
      from reference r
      inner join dbo.IdListToTable(@EmployeeTypes) et on r.ref_id = et.ID
      order by sortby
go

grant execute on RPT_locates_completed_summary to uqweb, QManagerRole

/*
exec RPT_locates_completed_summary '2008-01-01', '2008-03-01', 2676, '*'
exec RPT_locates_completed_summary '2008-01-01', '2008-03-01', 2676, '11'
exec RPT_locates_completed_summary '2008-01-01', '2008-03-01', 2676, '10,11,1015'
*/
