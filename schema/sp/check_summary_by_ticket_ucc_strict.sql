if object_id('dbo.check_summary_by_ticket_ucc_strict') is not null
	drop procedure dbo.check_summary_by_ticket_ucc_strict
GO

/* A stricter version of ticket_ucc, that looks for tickets matching
   those listed on audits, taking update call center rules into
   account.
*/

CREATE Procedure check_summary_by_ticket_ucc_strict
	(
	@SummaryDate datetime,
	@CallCenter varchar(30)
	)
As
set nocount on

DECLARE @SummaryWindowStart datetime
DECLARE @SummaryWindowEnd datetime
DECLARE @SummaryDateEnd datetime

DECLARE @MasterCallCenter varchar(20)

-- get "master" call center via client table (which is the only place
-- in the database where a relation is defined between call center
-- and update call center)
select @MasterCallCenter = max(call_center) from client
where update_call_center = @CallCenter

DECLARE @tickets_found TABLE (
	tf_ticket_number varchar(20) NOT NULL PRIMARY KEY
)

SELECT @SummaryDateEnd = DATEADD(d, 1, @SummaryDate)

DECLARE @gn TABLE (
	client_code varchar (20) NOT NULL ,
	item_number varchar (20) NULL ,
	ticket_number varchar (20) NULL ,
	type_code varchar (20) NULL ,
	found int
)

/* Get the list of tickets on the summary */
INSERT INTO @gn
select '---' AS client_code, sd.item_number, sd.ticket_number, sd.type_code, 0 as found
 from summary_header sh
  inner join summary_detail sd
    on sh.summary_header_id=sd.summary_header_id
 where sh.summary_date=@SummaryDate
  and sh.call_center=@CallCenter
 order by sd.item_number, sd.ticket_number

/* Get the list of tickets that were received. 
   Considered are only tickets that:
   - have the given call center as their format 
   - have the master call center as their format, and the call center in their 'source' field
   - have the master call center as their format, and there exists a record in ticket_version
     from the given call center
*/
INSERT INTO @tickets_found
select ticket.ticket_number
 from ticket
 where ticket.transmit_date >= @SummaryDate
  and ticket.transmit_date < @SummaryDate + 7
  and (ticket.ticket_format = @CallCenter 
       or (ticket.ticket_format = @MasterCallCenter and ticket.source = @CallCenter)
       or (ticket.ticket_format = @MasterCallCenter 
           and exists (select * from ticket_version as tv 
                       where ticket.ticket_id = tv.ticket_id
                       and tv.source = @CallCenter)))
 group by ticket.ticket_number

/* Mark them as found on the list from the summary */
update @gn set found=1
 from @tickets_found tf
  where ticket_number=tf_ticket_number

select * from @gn
 order by found, client_code, ticket_number
GO

grant execute on check_summary_by_ticket_ucc_strict to uqweb, QManagerRole
/*
exec dbo.check_summary_by_ticket_ucc_strict '2010-08-23', '3004'
*/
