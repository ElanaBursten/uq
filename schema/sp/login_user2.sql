/*   QMANTWO-521 LDAP
     LDAP Changes
	 stored procedure ->>  * login_user2
     2/12/2018	  
*/

--USE QM     LocQM
if object_id('dbo.login_user2') is not null
	drop procedure dbo.login_user2
go

Create Procedure dbo.login_user2
  (
  @UserName varchar(25),
  @PW  varchar(40)
  )
As

select employee.emp_id, users.chg_pwd, users.uid, users.chg_pwd_date,
  employee.type_id, employee.report_to, employee.short_name,
  reference.code AS 'TypeCode', employee.emp_number,employee.ad_username, --QMANTWO-521
  /* Note: password is no longer needed once db password data is encrypted (see #1641) */ 
  password, users.api_key 
 from users
  left join employee on users.emp_id=employee.emp_id
  left join reference on employee.type_id=reference.ref_id and reference.type='emptype'
 where login_id = @UserName
  and (users.password = @PW 
    /* Note: Null @PW criteria can be removed once db password data is encrypted */ 
    or @PW is null)
  and users.active_ind = 1
  and users.password is not null
  and users.password <> ''
  and (employee.active=1 or employee.active is null)

