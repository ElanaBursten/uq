if object_id('dbo.get_repr_pc_under') is not null
	drop procedure dbo.get_repr_pc_under
GO

CREATE PROCEDURE get_repr_pc_under (@id int)
as

 declare @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_report_level integer,
  h_report_to integer,
  h_repr_pc_code varchar(30) NULL
 )

  declare @adding_level int
  select @adding_level = 1

  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_report_to, h_report_level, h_repr_pc_code)
  select e.emp_id, e.report_to, @adding_level, e.repr_pc_code
    from employee e
    left join employee man on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user
  WHILE @adding_level < 10
  BEGIN
    INSERT INTO @results
         (h_emp_id, h_report_to, h_report_level, h_repr_pc_code)
      select
        e.emp_id, e.report_to, r.h_report_level+1, e.repr_pc_code
      from employee e
       inner join @results r on e.report_to = r.h_emp_id
            AND r.h_report_level=@adding_level

    SELECT @adding_level = @adding_level + 1
  END

  delete from @results
   where h_repr_pc_code is null
     or h_repr_pc_code = ''
     or h_repr_pc_code = ' '

  select * from @results
GO
grant execute on get_repr_pc_under to uqweb, QManagerRole


/*
exec dbo.get_repr_pc_under 211
exec dbo.get_repr_pc_under 2676
*/
