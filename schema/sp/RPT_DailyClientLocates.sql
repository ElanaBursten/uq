if object_id('dbo.RPT_DCL_NoSyncRange') is not null
	drop procedure dbo.RPT_DCL_NoSyncRange
GO

create procedure dbo.RPT_DCL_NoSyncRange
  (
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCode varchar(20),
  @CallCenter varchar(30),
  @LocateClosed integer
  )
as

select
  ticket.ticket_id,
  case locate.Alert when 'A' then '!' else locate.alert end as alert_status, 
  convert(datetime, convert(varchar(12), locate.closed_date , 102) , 102) as closed_date_only,
  locate.closed_date,
  locate.client_code,
  locate.status,
  locate.modified_date,  -- most recent sync date
  locate.closed,
  ticket.ticket_number,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  -- xml_ticket_format and image are ignored by export; cannot be the last field returned due to known issue in SaveDelimToStream method
  call_center.xml_ticket_format, 
  ticket.image,
  ticket.due_date

from ticket with (index(ticket_transmit_date))
  inner join locate with (index(locate_ticketid)) on ticket.ticket_id = locate.ticket_id  
  left join call_center on call_center.cc_code = ticket.ticket_format
where locate.status <> '-N'
  and locate.client_code = @ClientCode
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
  and ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by locate.closed_date

go
grant execute on dbo.RPT_DCL_NoSyncRange to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_NoSyncRange '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0
*/

if object_id('dbo.RPT_DCL_SyncRange') is not null
	drop procedure dbo.RPT_DCL_SyncRange
GO

create procedure dbo.RPT_DCL_SyncRange
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCode varchar(20),
  @CallCenter varchar(30),
  @LocateClosed integer
  )
as

/* Any idea in progress, for later:
declare @EarliestTicketID integer
declare @EarliestLocateID integer
select @EarliestTicketID = (select min(ticket_id) from (select top 1000 ticket_id from ticket where transmit_date>=@XmitDateFrom) t1)
select @EarliestLocateID = (select min(locate_id) from locate where ticket_id = @EarliestTicketID)
*/
select
  L.ticket_id,
  L.closed_date_only,
  L.closed_date,
  L.client_code,
  L.status,
  L.modified_date,  -- sync date
  L.closed,
  case L.Alert when 'A' then '!' else L.alert end as alert_status, 
  ticket.ticket_number,
  ticket.work_address_number,
  ticket.work_address_number_2,
  ticket.work_address_street,
  ticket.work_state,
  ticket.work_county,
  ticket.work_city,
  ticket.map_page,
  ticket.transmit_date,
  -- xml_ticket_format and image are ignored by export; cannot be the last field returned due to known issue in SaveDelimToStream method
  call_center.xml_ticket_format, 
  ticket.image,
  ticket.due_date
FROM 
 (select
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102) as closed_date_only,
  min(ls.status_date) as closed_date,
  locate.client_code,
  Locate.Alert,
  ls.status,
  min(ls.insert_date) as modified_date,  -- sync date
  locate.closed,
  locate.ticket_id
 from locate_status ls
  inner join locate on ls.locate_id = locate.locate_id
 where ls.insert_date between @SyncDateFrom and @SyncDateTo
  and ls.status <> '-N'
  and locate.modified_date >= @SyncDateFrom  -- this one is a perf opt
  and locate.client_code = @ClientCode
  and (@LocateClosed<>1 or locate.closed = 0)
  and (@LocateClosed<>2 or locate.closed = 1)
 group by
  convert(datetime, convert(varchar(12), ls.status_date , 102) , 102),
  locate.client_code,
  ls.status,
  Locate.Alert,
  locate.closed,
  locate.ticket_id
 ) L
  inner join ticket on ticket.ticket_id = L.ticket_id
  left join call_center on call_center.cc_code = ticket.ticket_format  
where ticket.transmit_date between @XmitDateFrom and @XmitDateTo
  and ticket.ticket_format = @CallCenter
order by L.closed_date

go
grant execute on dbo.RPT_DCL_SyncRange to uqweb, QManagerRole
/*
exec dbo.RPT_DCL_SyncRange '2003-05-14', '2003-05-21', '2003-05-14', '2003-05-15', 'ATL01', 'Atlanta', 2

*/

if object_id('dbo.RPT_DailyClientLocates') is not null
	drop procedure dbo.RPT_DailyClientLocates
GO

create procedure dbo.RPT_DailyClientLocates
  (
  @SyncDateFrom datetime,
  @SyncDateTo datetime,
  @XmitDateFrom datetime,
  @XmitDateTo datetime,
  @ClientCode varchar(20),
  @CallCenter varchar(30),
  @LocateClosed integer
  )
as

if @SyncDateFrom='1900-01-01'  -- not filtering by status date, don't use ls table
  exec dbo.RPT_DCL_NoSyncRange @XmitDateFrom, @XmitDateTo, @ClientCode, @CallCenter, @LocateClosed
else
  exec dbo.RPT_DCL_SyncRange @SyncDateFrom, @SyncDateTo, @XmitDateFrom, @XmitDateTo, @ClientCode, @CallCenter, @LocateClosed

go
grant execute on dbo.RPT_DailyClientLocates to uqweb, QManagerRole
/*
exec dbo.RPT_DailyClientLocates '2012-01-01', '2013-01-01', '2012-01-01', '2013-01-01', 'SCEDZ60', '1421', 0
exec dbo.RPT_DailyClientLocates '1900-01-01', '2199-01-01', '2003-04-14', '2003-04-15', 'ATL01', 'Atlanta', 0
*/

