-- Recreate event_log's instead of update trigger
if exists(select name FROM sysobjects where name = 'event_log_no_update' AND type = 'TR')
  drop trigger event_log_no_update
go
create trigger event_log_no_update on event_log
instead of update
not for replication
as
begin
  raiserror('Updating existing event_log rows is not allowed. To maintain integrity of the event store, events can only be added.', 16, 1)
end
go
