if object_id('dbo.get_old_work_orders_for_emp') is not null
  drop procedure dbo.get_old_work_orders_for_emp
go

create proc get_old_work_orders_for_emp (@emp_id int, @num_days int)
as
  set nocount on
  
  declare @since datetime
  select @since = DateAdd(Day, -@num_days, GetDate())
  
  /* NOTE that the results of this stored procedure should be kept in 
     sync with those in the get_open_work_orders_for_emp sp. */
  select wo.wo_id, 
    wo.wo_number, 
    wo.kind,
    wo.transmit_date, 
    wo.due_date, 
    wo.closed_date,
    wo.work_address_number,
    wo.work_address_street, 
    wo.work_county,
    wo.work_city,
    wo.work_state,
    wo.work_zip,
    wo.work_description, 
    wo.work_type, 
    wo.map_page, 
    wo.map_ref,
    wo.wo_source, 
    c.oc_code,
    c.client_name,
    wo.status,
    coalesce(statuslist.status_name, wo.status) status_description,
    wo.modified_date,
    (select count(*) from work_order_ticket wot 
     where wot.wo_id = wo.wo_id 
       and wot.active = 1) as ticket_count,
    1 as work_order_is_visible
  from work_order wo
  left join client c on (wo.client_id = c.client_id)
  left join statuslist on wo.status = statuslist.status
  where wo.assigned_to_id = @emp_id
    and (wo.closed_date >= @since or wo.closed_date is null)
   
go
grant execute on get_old_work_orders_for_emp to uqweb, QManagerRole

/* 
exec dbo.get_old_work_orders_for_emp 3510, 7
*/   
