if object_id('dbo.close_old_locates_2') is not null
  drop procedure dbo.close_old_locates_2
GO

/* Close all locates older than @NumDays days.  (1 means older than the
   current day, 2 is older than yesterday, etc.)
   Currently this is used in LocInc only!
*/
CREATE procedure close_old_locates_2 (
  @Status varchar(5),
  @NumDays int
)
as
set nocount on

/* we don't run this query on holidays, so check for that */
declare @a float, @b float, @today datetime
select @a = convert(float, getdate())
select @b = floor(@a)
select @today = convert(datetime, @b) -- current date with time set to 00:00:00

declare @isholiday int

select @isholiday = count(*) from holiday
where cc_code = '*' and holiday_date = @today

if (@isholiday > 0) 
begin
  print 'Today is a holiday; this stored procedure will not run'
  return
end

/**/

declare @excluded table (
  emp_id int not null primary key
)

insert into @excluded
select emp_id from employee
where autoclose = 0

declare @ltoc table (
	locate_id int not null primary key
)

-- get the current date, with time set to 00:00:00
declare @CutOffDate datetime
select @CutoffDate = cast(floor(cast(getdate() as float) - @NumDays + 1) as datetime)
--print @CutOffDate

insert into @ltoc
select locate.locate_id
 from ticket
 inner join locate on locate.ticket_id = ticket.ticket_id
 where ticket.transmit_date < @CutoffDate
  and locate.closed = 0 
  and (locate.assigned_to not in (select emp_id from @excluded)
    or locate.assigned_to is null)

select count(*) as LocatesToProcess from @ltoc

declare @closed_date_now datetime
select @closed_date_now = getdate()

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
       qty_marked=1
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END
GO
go
/*
exec dbo.close_old_locates_2 'C', 2
*/

