if object_id('dbo.RPT_DamageLiabilityChanges4') is not null
  drop procedure dbo.RPT_DamageLiabilityChanges4
go

create procedure dbo.RPT_DamageLiabilityChanges4 (
  @RangeStart    datetime, -- Damage change range [@RangeStart..@RangeEnd)
  @RangeEnd      datetime, -- This is the key range to consider for changes!
  @RecdStart     datetime, -- Damage claim received [@RecdStart..@RecdEnd)
  @RecdEnd       datetime, -- Just a filter, don't use any data as of these dates
  @DmgStart      datetime, -- Damage date
  @DmgEnd        datetime, -- Just a filter, don't use any data as of these dates
  @State         char(2),  -- Just a filter
  @ProfitCenter  varchar(10), -- Just a filter
  @LiabilityTypeStart int,
  @LiabilityTypeEnd int,
  @IncludeZeroUQLiab bit,
  @MinChange integer,
  @ExcludeInitialEst bit,
  @IncludeThirdParty bit,
  @IncludeLitigation bit
)
as
  set nocount on

  -- Results master table
  declare @results table (
    damage_idz int not null primary key,
    uq_damage_idz int,
    -- What section of the report this record belongs in.
    -- Should be one of these for all returned records: NEW, CHANGED, TOUQ, FROMUQ.
    report_category varchar(10) NULL,
    report_category_name varchar(60),     -- to make it easy to show on the client

    -- information about the damage
    damage_date datetime,
    uq_notified_date datetime,
    client_claim_id varchar(25) NULL,
    profit_center varchar(15),
    utility_co_damaged varchar(40),
    facility_type varchar(10) NULL,
    facility_size varchar(10) NULL,
    size_type varchar(50) NULL,
    location varchar(80) NULL,
    city varchar(40) NULL,
    state varchar(2) NULL,
    invoice_code varchar(10) NULL,

    -- These dates are not returned to the caller, just used while getting data:
    start_estimate_date datetime,  -- Might be before the date range
    end_estimate_date datetime, -- Might be before the date range

    -- Responsibility/estimate details at the beginning of the date range
    -- (Liabilities might be different from estimates, depending on who is at fault)
    start_uq_resp_code varchar(10) NOT NULL,
    start_exc_resp_code varchar(10) NOT NULL,
    start_spc_resp_code varchar(10) NOT NULL,

    start_uq_liab decimal(12, 2) default 0,
    start_exc_liab decimal(12, 2) default 0,
    start_spc_liab decimal(12, 2) default 0,

    -- Responsibility/estimate details at the end of the date range
    end_uq_resp_code varchar(10) NOT NULL,
    end_exc_resp_code varchar(10) NOT NULL,
    end_spc_resp_code varchar(10) NOT NULL,

    end_uq_liab decimal(12, 2) default 0,
    end_exc_liab decimal(12, 2) default 0,
    end_spc_liab decimal(12, 2) default 0,

    -- The difference between the start liability and the end liability
    uq_liability_change decimal(12, 2),
    exc_liability_change decimal(12, 2),
    spc_liability_change decimal(12, 2),

    -- The last date the damage was some UQ responsibility value other than the ending value.
    -- Informational output for the "Changed to UQ" and "Changed from UQ" sections.
    last_uq_resp_change_date_in_range datetime,

    -- Development purposes only / not returned to the caller:

    -- These have the estimates at start and end dates, regardless of liability -
    -- this report is about liability, so be careful not to display them anywhere
    start_estimate decimal(12, 2) default 0,
    end_estimate decimal(12, 2) default 0,

    -- The first damage modification date after the date range. If a record exists
    -- for this date, it contains the responsibility values at the end of the range.
    -- If not, the current values are the ending responsibility values.
    first_mod_date_after_range_end datetime,

    -- The first modification of this damage after the start date.  If such a value
    -- exists, it contains the starting responsibility values.  If it does not exist,
    -- The starting responsibility values are null unless the damage was created before
    -- the range began, in which case the current values are also the starting values.
    first_mod_date_after_range_start datetime
  )

  declare @relevant_damages table (
    rd_damage_id int NOT NULL primary key
  )

  declare @optional_estimate table (
    oe_damage_id int not null,
    oe_third_party_id int null,
    oe_litigation_id int null,
    oe_start_estimate_date datetime,
    oe_end_estimate_date datetime,
    oe_start_estimate decimal(12, 2) default 0,
    oe_end_estimate decimal(12, 2) default 0
    )
    
  insert into @relevant_damages
  select damage_id from damage
    where (uq_notified_date >= @RangeStart and uq_notified_date < @RangeEnd)
     or (damage_date >= @RangeStart and damage_date < @RangeEnd)
  union
  select damage_id from damage_estimate
    where (modified_date >= @RangeStart and modified_date < @RangeEnd)
  union
  select damage_id from damage_history
    where (modified_date >= @RangeStart and modified_date < @RangeEnd)
     and not (modified_date between '2004-03-23T23:00:00' and '2004-03-24T01:00:00')
  union
  select damage_id from damage_invoice
    where damage_id is not null
     and ( (received_date >= @RangeStart and received_date < @RangeEnd)
        or (paid_date >= @RangeStart and paid_date < @RangeEnd) )
  union
  select damage_id from damage_third_party
    where (@IncludeThirdParty=1) and (modified_date >= @RangeStart and modified_date < @RangeEnd)
  union
  select damage_id from damage_litigation
    where (@IncludeLitigation=1) and (modified_date >= @RangeStart and modified_date < @RangeEnd)

  -- Start with all matching damages
  insert into @results (
    damage_idz,  -- These values are never changed by the SQL queries
    uq_damage_idz,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    location,
    city,
    state,
    facility_type,
    facility_size,
    invoice_code,
    start_uq_resp_code,  -- These 4 codes might be overriden later by history values
    start_exc_resp_code,
    start_spc_resp_code,
    end_uq_resp_code,
    end_exc_resp_code,
    end_spc_resp_code)
  select
    damage_id,
    uq_damage_id,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    location,
    city,
    state,
    facility_type,
    facility_size,
    invoice_code,
    coalesce(uq_resp_code, '-'),   -- Transform NULLs in the DB to values
    coalesce(exc_resp_code, '-'),  -- we can deal with here more easily
    coalesce(spc_resp_code, '-'),
    coalesce(uq_resp_code, '-'),
    coalesce(exc_resp_code, '-'),
    coalesce(spc_resp_code, '-')
  from @relevant_damages r
   inner join  damage on r.rd_damage_id = damage.damage_id
  where damage.uq_notified_date >= @RecdStart and damage.uq_notified_date<@RecdEnd
    and damage.damage_date >= @DmgStart and damage.damage_date<@DmgEnd
    and ((@State='') or (damage.state = @State ))
    and ((@ProfitCenter = '') or (damage.profit_center=@ProfitCenter))

  -- Add 3rd party estimates
  insert @optional_estimate (oe_damage_id, oe_third_party_id) 
    select distinct damage_id, third_party_id 
    from @relevant_damages inner join damage_estimate on damage_id=rd_damage_id
    where @IncludeThirdParty=1 and third_party_id is not null

  -- Add litigation estimates
  insert @optional_estimate (oe_damage_id, oe_litigation_id) 
    select distinct damage_id, litigation_id 
    from @relevant_damages inner join damage_estimate on damage_id=rd_damage_id
    where @IncludeLitigation=1 and litigation_id is not null

  -- set 3rd party starting and ending estimate dates & amounts
  update @optional_estimate
    set oe_start_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = oe_damage_id and third_party_id = oe_third_party_id
    and modified_date <= @RangeStart
  ) where oe_third_party_id is not null

  update @optional_estimate
  set oe_end_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = oe_damage_id and third_party_id = oe_third_party_id
    and modified_date <= @RangeEnd
  ) where oe_third_party_id is not null
  
  update @optional_estimate
    set oe_start_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = oe_damage_id and third_party_id = oe_third_party_id
    and modified_date = oe_start_estimate_date
  ) where oe_third_party_id is not null
  
  update @optional_estimate
  set oe_end_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = oe_damage_id and third_party_id = oe_third_party_id
    and modified_date = oe_end_estimate_date
  ) where oe_third_party_id is not null
  
  -- set litigation starting and ending estimate dates & amounts
  update @optional_estimate
    set oe_start_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = oe_damage_id and litigation_id = oe_litigation_id
    and modified_date <= @RangeStart
  ) where oe_litigation_id is not null
  
  update @optional_estimate
  set oe_end_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = oe_damage_id and litigation_id = oe_litigation_id
    and modified_date <= @RangeEnd
  ) where oe_litigation_id is not null
  
  update @optional_estimate
    set oe_start_estimate = (
    select coalesce(max(amount),0) from damage_estimate
    where damage_id = oe_damage_id and litigation_id = oe_litigation_id
    and modified_date = oe_start_estimate_date
  ) where oe_litigation_id is not null
  
  update @optional_estimate
  set oe_end_estimate = (
    select coalesce(max(amount),0) from damage_estimate
    where damage_id = oe_damage_id and litigation_id = oe_litigation_id
    and modified_date = oe_end_estimate_date
  ) where oe_litigation_id is not null
  
  
    -- All non-liability-related filters have been applied, only liability
  -- filter need to be applied at the end.

  -- Update the added date of the estimate active on the starting date

  update @results
  set start_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = damage_idz and third_party = 0 
    and third_party_id is null 
    and litigation_id is null
    and modified_date <= @RangeStart
  )

  -- Update the added date of the estimate active on the ending date
  update @results
  set end_estimate_date = (
    select max(modified_date) from damage_estimate
    where damage_id = damage_idz and third_party = 0
    and third_party_id is null 
    and litigation_id is null
    and modified_date <= @RangeEnd
  )

  -- Update the starting estimate amount
  update @results
  set start_estimate = (
    select max(amount) from damage_estimate
    where damage_id = damage_idz
      and modified_date = start_estimate_date -- modified_date is never null
  )
  
  update @results
    set start_estimate = start_estimate + (
      select coalesce(sum(oe_start_estimate), 0)
        from @optional_estimate
        where oe_damage_id = damage_idz
  )
  
  -- Update the ending estimate amount
  update @results
  set end_estimate = (
    select max(amount) from damage_estimate
    where damage_id = damage_idz
      and modified_date = end_estimate_date
  )
  update @results
    set end_estimate = end_estimate + (
      select coalesce(sum(oe_end_estimate), 0)
        from @optional_estimate
        where oe_damage_id = damage_idz
  )
   
  -- Update the first modification date after the range start
  update @results
  set first_mod_date_after_range_start = (
    select min(modified_date)
    from damage_history hist
    where (damage_idz = hist.damage_id)
      and (hist.modified_date >= @RangeStart)
  )

  -- Update the first modification date after the range end
  update @results
  set first_mod_date_after_range_end = (
    select min(modified_date)
    from damage_history hist
    where (damage_idz = hist.damage_id)
      and (hist.modified_date >= @RangeEnd)
  )

  -- The responsibility state at the start of the range comes from either the
  -- first history record added after the range start, or the curent values if
  -- no such history record exists.  The curent value is already in place.
  -- TODO: Find a non-code-duplicating way to express the below 4 queries
  update @results
   set start_uq_resp_code = coalesce(uq_resp_code,'-'),
       start_exc_resp_code = coalesce(exc_resp_code,'-'),
       start_spc_resp_code = coalesce(spc_resp_code,'-')
   from damage_history hist
    where damage_idz = hist.damage_id
    and hist.modified_date = first_mod_date_after_range_start

  -- The responsibility state at the end of the range comes from either the
  -- first history record added after the range end, or the curent values if
  -- no such history record exists.  The curent value is already in place.
  update @results
   set end_uq_resp_code = coalesce(uq_resp_code,'-'),
       end_exc_resp_code = coalesce(exc_resp_code,'-'),
       end_spc_resp_code = coalesce(spc_resp_code,'-')
   from damage_history hist
    where damage_idz = hist.damage_id
    and hist.modified_date = first_mod_date_after_range_end

  -- If a damage did not exist at the range start, the starting responsibilities must be none
  update @results
   set start_uq_resp_code = '-',
       start_exc_resp_code = '-',
       start_spc_resp_code = '-'
  where uq_notified_date > @RangeStart

  -- If a damage did not exist at the range end, the ending responsibilities then must be none
  update @results
   set end_uq_resp_code = '-',
       end_exc_resp_code = '-',
       end_spc_resp_code = '-'
  where uq_notified_date > @RangeEnd

  -- The last_uq_resp_change_date_in_range is the last date the damage was some
  -- responsibility value other than the terminal value for the range.
  update @results
  set last_uq_resp_change_date_in_range = (
    select max(hist.modified_date)
    from damage_history hist
    where damage_idz = hist.damage_id
      and hist.modified_date >= @RangeStart and hist.modified_date < @RangeEnd
      and coalesce(hist.uq_resp_code, '-') <> end_uq_resp_code)
    -- getting rid of NULLs up from makes this easier

  -- Calculate the liability amounts.  UQ has no monetary liability unless
  -- they have some fault in the damage (based on the *_uq_resp_code).
  update @results
  set start_uq_liab = coalesce(start_estimate,0)
  where start_uq_resp_code <> '-'

  update @results
  set end_uq_liab = coalesce(end_estimate, 0)
  where end_uq_resp_code <> '-'

  update @results
  set start_exc_liab = coalesce(start_estimate,0)
  where start_exc_resp_code <> '-'
   and start_uq_resp_code = '-'

  update @results
  set end_exc_liab = coalesce(end_estimate, 0)
  where end_exc_resp_code <> '-'
   and end_uq_resp_code = '-'

  update @results
  set start_spc_liab = coalesce(start_estimate,0)
  where start_spc_resp_code <> '-'
   and start_uq_resp_code = '-'
   and start_exc_resp_code = '-'

  update @results
  set end_spc_liab = coalesce(end_estimate, 0)
  where end_spc_resp_code <> '-'
   and end_uq_resp_code = '-'
   and end_exc_resp_code = '-'

  -- Update the difference between the start/end liability (might be negative)
  update @results
  set uq_liability_change = end_uq_liab - start_uq_liab,
      exc_liability_change = end_exc_liab - start_exc_liab,
      spc_liability_change = end_spc_liab - start_spc_liab

  -- note that this will exclude some that have a UQ liability code,
  -- if there are no dollars of liability.
  if @IncludeZeroUQLiab=0
    delete from @results
     where start_uq_liab=0 and end_uq_liab=0

  -- Filter for requested liability types
  -- 0: UQ has liability
  -- 1: UQ has no liability
  -- 2: Only UQ has liability
  -- 3: Both UQ and excavator have liability
  -- 4: Neither UQ nor excavator are liable
  -- 5: No filtering

  delete from @results
  where (
    ((@LiabilityTypeStart = 0) and (start_uq_resp_code='-')) or
    ((@LiabilityTypeStart = 1) and (start_uq_resp_code<>'-')) or
    ((@LiabilityTypeStart = 2) and not (start_uq_resp_code<>'-' and start_exc_resp_code='-')) or
    ((@LiabilityTypeStart = 3) and not (start_uq_resp_code<>'-' and start_exc_resp_code<>'-')) or
    ((@LiabilityTypeStart = 4) and not (start_uq_resp_code='-' and start_exc_resp_code='-' ))
  )

  delete from @results
  where (
    ((@LiabilityTypeEnd = 0) and (end_uq_resp_code='-')) or
    ((@LiabilityTypeEnd = 1) and (end_uq_resp_code<>'-')) or
    ((@LiabilityTypeEnd = 2) and not (end_uq_resp_code<>'-' and end_exc_resp_code='-')) or
    ((@LiabilityTypeEnd = 3) and not (end_uq_resp_code<>'-' and end_exc_resp_code<>'-')) or
    ((@LiabilityTypeEnd = 4) and not (end_uq_resp_code='-' and end_exc_resp_code='-' ))
  )

  -- Apply the minimum change detection

  if @MinChange > 0 begin
    delete from @results
      where abs(uq_liability_change) < @MinChange
        and abs(exc_liability_change) < @MinChange
        and abs(spc_liability_change) < @MinChange
  end

  -- Exlude items that only got an initial estimate
  if @ExcludeInitialEst = 1
    delete from @results
       where start_uq_liab=0
        and damage_idz in  (
          select damage_id
            from @results r
              inner join damage_estimate e
                 on r.damage_idz = e.damage_id
            where (e.modified_date >= @RangeStart and e.modified_date < @RangeEnd)
                and e.active = 1
                and e.third_party_id is null and e.litigation_id is null
          group by damage_id having count(damage_id) = 1)

  -- Determine what section of the report this damage goes in.  If none of the 4
  -- classes match, the damage liability has not changed, and can be ignored.

  -- Assume they all go here first; they are only
  -- in @results at all, because they had some activity during the period
  update @results
  set report_category = '7OTHER'

  -- The ones that never were UQ, go on their own page, which is usually
  -- omitted anyway
  update @results
  set report_category = '6NOUQ'
  where start_uq_resp_code = '-'
    and end_uq_resp_code = '-'

  -- Anything that changed, is in this category
  update @results
  set report_category = '2CHANGED'
  where uq_liability_change <> 0

  -- Put them here instead based on the liab change
  update @results
  set report_category = '3TOUQ'
  where uq_notified_date < @RangeStart
    and start_uq_resp_code = '-'
    and end_uq_resp_code <> '-'

  -- Put them here instead based on the liab change
  update @results
  set report_category = '4FROMUQ'
  where uq_notified_date < @RangeStart
    and start_uq_resp_code <> '-'
    and end_uq_resp_code = '-'

  -- Put them here if UQ had liab at both times
  -- and it did not change
  update @results
  set report_category = '5UQBOTH'
  where uq_liability_change = 0
    and start_uq_resp_code <> '-'
    and end_uq_resp_code <> '-'

  -- If they are new in the period, UQ liab, they go here
  update @results
  set report_category = '1NEW'
  where uq_notified_date >= @RangeStart
    and uq_liability_change <> 0

  -- Some sanity checks
  declare @LiabilityChanges int
  declare @ReportRecords int
  declare @NumRecords int

  set @NumRecords = (select count(*) from @results where (end_uq_liab <> 0) and (end_uq_resp_code='-'))
  if (@NumRecords > 0)
    RAISERROR('Liability without responsibility code', 18, 1) with log

  set @NumRecords = (select count(*) from @results where report_category in ('3TOUQ', 'FROMUQ') and (last_uq_resp_change_date_in_range is null))
  if (@NumRecords > 0)
    RAISERROR('TOUQ/FROMUQ without an in-range responsibility change date', 18, 1) with log


  -- set category full names and order field

  update @results set report_category_name='New Damage Claims'
   where report_category = '1NEW'
  update @results set report_category_name='New Estimates for Earlier Damage Claims'
   where report_category = '2CHANGED'
  update @results set report_category_name='Damages Changed To UtiliQuest Responsibility'
   where report_category = '3TOUQ'
  update @results set report_category_name='Damages Changed From UtiliQuest Responsibility'
   where report_category = '4FROMUQ'
  update @results set report_category_name='Damages With Unchanged UtiliQuest Responsibility'
   where report_category = '5UQBOTH'
  update @results set report_category_name='Damages With No UtiliQuest Responsibility'
   where report_category = '6NOUQ'
  update @results set report_category_name='Damages With Other Activity'
   where report_category = '7OTHER'


  -- ***************** Emit the results ********************
  -- Emit the damages
  select  damage_idz as damage_id,
    uq_damage_idz as uq_damage_id,
    report_category,

    uq_liability_change,
    exc_liability_change,
    spc_liability_change,
    uq_liability_change as liability_change,  -- support old REs

    start_uq_resp_code, start_exc_resp_code, start_spc_resp_code,
    start_uq_liab, start_exc_liab, start_spc_liab,
    end_uq_resp_code, end_exc_resp_code, end_spc_resp_code,
    end_uq_liab, end_exc_liab, end_spc_liab,

    last_uq_resp_change_date_in_range,

    damage_date, uq_notified_date,
    client_claim_id, profit_center, utility_co_damaged,
    size_type, facility_type, facility_size,
    invoice_code,
    city, state,
    report_category_name,
    case
      when (location <> '') and (city <> '') then
        location + ', ' + city
      when (location <> '') then
        location
      else
        city
    end as location,
    coalesce(facility_type,'') + ' / ' + coalesce(facility_size,'') as facility_type_size
  from @results
  order by report_category, profit_center, uq_damage_idz
  -- order by damage_idz (useful for debugging)

 -- emit Estimates
  select
    estimate_id,
    damage_idz as damage_id,
    uq_damage_idz as uq_damage_id,
    third_party,
    emp_id,
    amount,
    company,
    contract,
    phone,
    comment,
    modified_date,
    active,
    IsNull(third_party_id, 0) as third_party_id,
    IsNull(litigation_id, 0) as litigation_id,
    case estimate_type
      when 1 then 'THIRD PARTY'
      when 2 then 'LITIGATION'
      else '' end as estimate_type
  from @results r
    inner join damage_estimate e on r.damage_idz = e.damage_id
  where (e.modified_date >= @RangeStart and e.modified_date < @RangeEnd)
     and ((e.third_party_id is null and e.litigation_id is null)
     or (@IncludeThirdParty=1 and e.third_party_id is not null)
     or (@IncludeLitigation=1 and e.litigation_id is not null))
     and e.active = 1
  order by damage_idz, modified_date

 -- emit Invoices
  select
    r.damage_idz as damage_id,
    r.uq_damage_idz as uq_damage_id,
    i.invoice_num,
    i.amount,
    i.company,
    i.received_date,
    i.satisfied_date,
    i.comment,
    i.approved_date,
    i.approved_amount,
    i.paid_date,
    i.paid_amount,
    i.invoice_id,
    IsNull(i.third_party_id, 0) as third_party_id,
    IsNull(i.litigation_id, 0) as litigation_id,
    case when i.received_date >= @RangeStart and i.received_date < @RangeEnd
         then i.amount
         else null end as recieved_amount_in_period,
    case when i.paid_date >= @RangeStart and i.paid_date < @RangeEnd
         then i.paid_amount
         else null end as paid_amount_in_period,
    case when IsNull(i.third_party_id, 0) > 0 then 'THIRD PARTY'
         when IsNull(i.litigation_id, 0) > 0  then 'LITIGATION'
         else '' end as invoice_type
  from @results r
    inner join damage_invoice i on r.damage_idz = i.damage_id
  where ((i.third_party_id is null and i.litigation_id is null)
     or (@IncludeThirdParty=1 and i.third_party_id is not null)
     or (@IncludeLitigation=1 and i.litigation_id is not null))
     and (i.received_date >= @RangeStart and i.received_date < @RangeEnd)
     or (i.paid_date >= @RangeStart and i.paid_date < @RangeEnd)
  order by damage_idz, received_date, paid_date

  -- Include totals for easier debugging and to faciliate
  -- a first or last page summary on the report if needed
  select
    report_category,
    max(report_category_name) as report_category_name,
    count(damage_idz) as N,
    sum(uq_liability_change) as liab_change,
    sum(uq_liability_change) as uq_liab_change,
    sum(exc_liability_change) as exc_liab_change,
    sum(spc_liability_change) as spc_liab_change
   from @results
   group by report_category with rollup
   order by report_category
   -- WITH ROLLUP adds a line with totals

  -- Include totals for easier debugging and to faciliate
  -- a first or last page summary on the report if needed
  select
    profit_center,
    report_category,
    max(report_category_name) as report_category_name,
    count(damage_idz) as N,
    sum(uq_liability_change) as liab_change,
    sum(uq_liability_change) as uq_liab_change,
    sum(exc_liability_change) as exc_liab_change,
    sum(spc_liability_change) as spc_liab_change
   from @results
   group by profit_center, report_category with rollup
   order by profit_center, report_category
   -- WITH ROLLUP adds a line with totals

go

grant execute on dbo.RPT_DamageLiabilityChanges4 to uqweb, QManagerRole

/*
--                                   Date range for changes     Notified range              Damage range
exec dbo.RPT_DamageLiabilitychanges4 '2005-03-01', '2005-04-29', '1900-01-01', '2005-04-29', '1900-01-01', '2005-04-29', '', '', 5, 5, 1, 500, 0, 0, 0
exec dbo.RPT_DamageLiabilitychanges4 '2003-04-05', '2003-04-09', '2003-01-01', '2004-01-01', '2003-01-01', '2004-01-01', 'CO', '', 5, 5, 0, 0, 0, 0, 0
exec dbo.RPT_DamageLiabilitychanges4 '2003-04-01', '2003-04-30', '2003-01-01', '2004-01-01', '2003-01-01', '2004-01-01', 'CO', '', 5, 5, 0, 0, 0, 0, 0
exec dbo.RPT_DamageLiabilitychanges4 '2003-04-01', '2003-04-15', '2003-01-01', '2004-01-01', '2003-01-01', '2004-01-01', '', '', 5, 5, 0, 0, 0, 0, 0
exec dbo.RPT_DamageLiabilitychanges4 '2003-06-01', '2003-07-01', '1900-01-01', '2500-01-01', '2003-06-01', '2003-07-01', '', 'FCL', 5, 5, 0, 0, 0, 0, 0

exec dbo.RPT_DamageLiabilitychanges4 '2003-11-01', '2003-12-01', '1900-01-01', '2500-01-01', '1900-01-01', '2500-01-01', '', 'FGA', 5, 5, 0, 0, 0, 0, 0

select * from damage where damage_id=63675
select * from damage_history where damage_id=63675
select * from damage_estimate where damage_id=63675


exec dbo.RPT_DamageLiabilitychanges4 '2005-03-01', '2005-04-29', '1900-01-01', '2005-04-29', '1900-01-01', '2005-04-29', '', '', 5, 5, 1, 500, 0, 0, 0

-- this one, they are having trouble with:

exec dbo.RPT_DamageLiabilitychanges4 '2005-08-15', '2005-08-22', '1900-01-01', '2500-01-01', '1900-01-01', '2500-01-01', '', '', 0, 0, 1, 500, 0, 0, 0
16 seconds


Removed from output query:
    -- Probably not needed on output, estiamte dates and amounts will come from detail
    start_estimate, start_estimate_date,
    end_estimate, end_estimate_date,
    -- Temporary fields for debugging
    first_mod_date_after_range_start, first_mod_date_after_range_end,

*/
