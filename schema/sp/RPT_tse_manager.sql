if object_id('dbo.RPT_tse_manager') is not null
	drop procedure dbo.RPT_tse_manager
go

create proc RPT_tse_manager(
	@ManagerID Int,
	@EndDate datetime,
	@LevelLimit int
)
as
 set nocount on
 declare @StartDate datetime
 set @StartDate = dateadd(d, -6, @EndDate)

 -- Employee data by hier, a row is one day's set of time periods, miles, etc.
 select e.h_first_name as first_name,
   e.h_last_name as last_name,
   e.h_emp_number as emp_number,
   e.h_short_name as short_name,
   e.h_emp_id as emp_id,
    dd.d as display_work_date,
    tse.*,
    reference.modifier as rule_abbrev,
    ent_emp.emp_number as ent_emp_number,
    ent_emp.short_name as ent_emp_short_name,
    app_emp.emp_number as app_emp_number,
    app_emp.short_name as app_emp_short_name,
    fap_emp.emp_number as fap_emp_number,
    fap_emp.short_name as fap_emp_short_name,
    @EndDate as report_end_date,
    case when final_approve_by is not null then 'FINAL APPROVED'
         when approve_by is not null       then 'APPROVED'
         when entry_by is not null         then 'ENTERED'
         else                                   '-'
    end as day_status,
    e.h_report_level, e.h_namepath, e.h_idpath,
    e.h_numpath, e.h_lastpath
  from (select * from dbo.get_report_hier2(@ManagerID, 0, @LevelLimit)) e
   inner join dbo.DateSpan(@StartDate, @EndDate) dd on 1=1
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id and tse.work_date=dd.d
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee ent_emp on tse.entry_by = ent_emp.emp_id
   left join employee app_emp on tse.approve_by = app_emp.emp_id
   left join employee fap_emp on tse.final_approve_by = fap_emp.emp_id
   left join employee work_emp on e.h_emp_id = work_emp.emp_id
   left join reference on reference.ref_id = work_emp.timerule_id
  where e.h_active=1
   and e.h_emp_id in (select distinct work_emp_id
     from timesheet_entry tse
      where tse.work_date between @StartDate and @EndDate)
  order by e.h_namepath, e.h_last_name, e.h_emp_id, dd.d

  -- totals by day of week
  select datepart(weekday, tse.work_date) as dow,
   min(tse.work_date) as dow_date,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours
  from (select * from dbo.get_report_hier2(@ManagerID, 0, @LevelLimit)) e
   inner join timesheet_entry tse
     on tse.work_emp_id = e.h_emp_id
  where tse.work_date between @StartDate and @EndDate
   and e.h_active=1
   and (tse.status='ACTIVE' or tse.status='SUBMIT')
   group by datepart(weekday, tse.work_date)
   order by 1

  -- totals for each emp
  select
   min(e.h_emp_id) as emp_id,

   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,

   sum(reg_hours+ot_hours+dt_hours) as working_hours,

   sum( case when vehicle_use='POV' then reg_hours+ot_hours+dt_hours else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%' then reg_hours+ot_hours+dt_hours else null end) as cov_hours

  from (select * from dbo.get_report_hier2(@ManagerID, 0, @LevelLimit)) e
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
  where e.h_active=1
  group by e.h_emp_id

go
grant execute on RPT_tse_manager to uqweb, QManagerRole

/*
exec dbo.RPT_tse_manager 211, '2004-09-25', 2
select * from reference where type='timerule'
update reference set modifier='-' where ref_id=590
update reference set modifier='CA' where ref_id=591
update reference set modifier='(S)' where ref_id=615
update reference set modifier='12/40' where ref_id=775
*/
