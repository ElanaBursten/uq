if object_id('dbo.export_projects') is not null
  drop procedure dbo.export_projects
GO

CREATE PROCEDURE export_projects (
  @CompanyName varchar(20), -- locating_company.name or or '*' for all
  @ProfitCenter   varchar(20)  -- employee's payroll_profit_center or '*' for all
)
AS

set nocount on

declare @Proj table (
  payroll_company_code varchar(20) null, /* locating_company.payroll_company_code = "Customer ID" */
  pc_code varchar(15) null,              /* profit_center.timesheet_num for customer.pc_code = "GL Subaccount" */
  manager_emp_number varchar(15) null,   /* Manager employee number for this PC (employee.repr_pc_code) = "PC Manager" */
  customer_number varchar(20) NULL,      /* customer.customer_number = "Project ID" */
  customer_name varchar(40) NOT NULL     /* customer.customer_name = "Project Description" */
)

insert into @Proj
select
  lc.payroll_company_code,
  Coalesce(pc.timesheet_num, '000') as pc_code,
  (select dbo.format_emp_number(emp_number) from employee where repr_pc_code = cust.pc_code and repr_pc_code is not null) as manager_emp_number,
  dbo.left_fill(cust.customer_number, '0', 10) as project,
  cust.customer_name
from customer cust
  left join locating_company lc on lc.company_id = cust.locating_company
  left join profit_center pc on pc.pc_code = cust.pc_code
where ((@CompanyName  = '*') or (@CompanyName  = lc.name))
  and ((@ProfitCenter = '*') or (@ProfitCenter = cust.pc_code))
order by 1, 2, 3

delete @Proj
from @Proj p
  inner join exported_projects ep on
    ((ep.payroll_company_code = p.payroll_company_code) or ((ep.payroll_company_code is null) and (ep.payroll_company_code is null))) and
    ((ep.pc_code              = p.pc_code)              or ((ep.pc_code is null)              and (ep.pc_code is null))) and
    ((ep.manager_emp_number   = p.manager_emp_number)   or ((ep.manager_emp_number is null)   and (ep.manager_emp_number is null))) and
    ((ep.customer_number      = p.customer_number)      or ((ep.customer_number is null)      and (ep.customer_number is null))) and
    ((ep.customer_name        = p.customer_name)        or ((ep.customer_name is null)        and (ep.customer_name is null)))

insert into exported_projects
  (payroll_company_code, pc_code, manager_emp_number, customer_number, customer_name)
select
   payroll_company_code, pc_code, manager_emp_number, customer_number, customer_name
from @Proj

select * from @Proj

GO

grant execute on export_projects to uqweb, QManagerRole
GO

/*
exec export_projects '*', '*'      -- 902
exec export_projects 'UTI', '*'    -- 114
exec export_projects 'UTI', 'FRI'  -- 21
select * from customer             -- 1037
select * from locating_company
select * from exported_projects
select top 100 * from employee
Clear export archive:
delete from exported_projects
*/
