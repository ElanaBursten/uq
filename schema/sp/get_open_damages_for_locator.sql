if object_id('dbo.get_open_damages_for_locator') is not null
  drop procedure dbo.get_open_damages_for_locator
go

create proc get_open_damages_for_locator (@locator_id int)
as
  set nocount on

  declare @results table (
    damage_id integer primary key,
    damage_type varchar(20) NULL,
    damage_date datetime NULL,
    uq_notified_date datetime NULL,
    location varchar(80) NULL,
    city varchar(40) NULL,
    county varchar(40) NULL,
    state varchar(2) NULL,
    utility_co_damaged varchar(40) NULL,
    remarks text NULL,
    investigator_id integer NULL,
    investigator_narrative text NULL,
    excavator_company varchar(30) NULL,
    excavator_type varchar(20) NULL,
    excavation_type varchar(20) NULL,
    ticket_id integer NULL,
    ticket_number varchar(20) NULL,
    ticket_format varchar(20) NULL,
    profit_center varchar(15) NULL,
    due_date datetime NULL,
    closed_date datetime NULL,
    facility_type varchar(10) NULL,
    facility_size varchar(10) NULL,
    facility_material varchar(10) NULL,
    uq_damage_id integer NULL,
    modified_date datetime NULL,
    work_priority_sort integer not null,
    work_priority varchar(20) not null,
    damage_is_visible varchar(1) not null
  )

  insert into @results
  select d.damage_id, d.damage_type, d.damage_date, d.uq_notified_date, 
    d.location, d.city, d.county, d.state, d.utility_co_damaged, d.remarks, d.investigator_id,
    d.investigator_narrative, d.excavator_company, d.excavator_type, d.excavation_type,
    d.ticket_id, t.ticket_number, t.ticket_format, d.profit_center, d.due_date,
    d.closed_date, d.facility_type, d.facility_size, d.facility_material, d.uq_damage_id,
    d.modified_date,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    1 as damage_is_visible
  from damage d
  left join ticket t on d.ticket_id = t.ticket_id
  left join reference r on d.work_priority_id = r.ref_id
  where d.investigator_id = @locator_id
    and d.damage_type in ('INCOMING', 'PENDING')
    and d.active = 1

  select * from @results damage order by work_priority_sort desc, due_date, uq_damage_id

go
grant execute on get_open_damages_for_locator to uqweb, QManagerRole

/*
exec dbo.get_open_damages_for_locator 12040
exec dbo.get_open_damages_for_locator 9600
exec dbo.get_open_damages_for_locator 8220
exec dbo.get_open_damages_for_locator 3510

-- To find people with damages to test the above:
select investigator_id, count(*) as open_damages_count
 from damage
 where investigator_id is not null 
   and damage_type in ('INCOMING', 'PENDING')
 group by investigator_id
 having count(*) > 0
*/

