
--------------------------------------------------------------------------------------------
--TRIGGER for work_order_OHM_details
if object_id('dbo.work_order_OHM_details_iu') is not null
  drop trigger dbo.work_order_OHM_details_iu
go

create trigger work_order_OHM_details_iu on work_order_OHM_details
for insert, update 
not for replication
AS
BEGIN
  declare @wo_id int; 
  declare @compliance_due_date datetime;
  declare @OSID varchar(50); 
  declare @wo_number varchar(20);  --  account number
  declare @opt char; 


  update work_order set
    modified_date = GetDate()
  where wo_id in (select wo_id from inserted) 

  If Exists(select * from DELETED)
      select @opt='I'  -- now insert into work_order_inspection
	  else
	  select @opt='U'


  if @opt='I' begin
  select @wo_id=wo_id, @wo_number=wo_number, @compliance_due_date=due_date, @OSID=OSID from inserted
  end

  IF EXISTS (select 1 from [dbo].[work_order_inspection] where [wo_id] = @wo_id)
  select @opt='U'

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  if @opt='I'
  begin
	INSERT INTO [dbo].[work_order_inspection]
			   ([wo_id]
                ,[account_number]
			    ,[premise_id]
				,[compliance_due_date]
				,[invoiced]
				)
		 VALUES
			   (@wo_id
			   ,@wo_number
			   ,@OSID
			   ,@compliance_due_date
			   ,1
			   )  
	end		     
END
GO