if object_id('dbo.reset_api_key') is not null
  drop procedure dbo.reset_api_key
go

CREATE Procedure reset_api_key
  (
  @UserID int,
  @APIKey varchar(40)
  )
As
  update users set api_key = @APIKey where uid = @UserID
  
GO
grant execute on reset_api_key to uqweb, QManagerRole
GO
