if object_id('dbo.RPT_DamageInvoice') is not null
  drop procedure dbo.RPT_DamageInvoice
go

create procedure dbo.RPT_DamageInvoice (
  @ReceivedFromDate datetime,
  @ReceivedToDate   datetime
)
as
  set nocount on

select d.uq_damage_id, di.invoice_num, di.amount, di.company,
  di.received_date, d.damage_date, d.invoice_code
  from damage_invoice di
  inner join damage d on d.damage_id = di.damage_id
  where di.received_date >= @ReceivedFromDate
  and di.received_date <= @ReceivedToDate
  order by di.received_date asc

go

grant execute on RPT_DamageInvoice to uqweb, QManagerRole

/*
exec dbo.RPT_DamageInvoice '2005-05-01', '2005-06-01'
exec dbo.RPT_DamageInvoice '2005-05-01', '9999-12-31' -- no end date
*/
