if object_id('dbo.RPT_damage_productivity2') is not null
  drop procedure dbo.RPT_damage_productivity2
GO

create procedure RPT_damage_productivity2(
  @ManagerId int, 
  @InvestigatorId int,
  @DamageTypes varchar(100), 
  @EmployeeStatus int,
  @DateFrom datetime,
  @DateTo datetime)
as
  -- Result set 1: damage productivity
  select count(*) as damage_totals, 
    investigator_id,
    e.h_short_name as short_name,
    damage_type,
    convert(datetime, convert(varchar, closed_date, 110)) as closed_date
  from damage
   inner join (select * 
      from dbo.get_report_hier3(@ManagerId, 0, 10, @EmployeeStatus)) e 
      on e.h_emp_id = damage.investigator_id
  where damage_type in (select S from dbo.StringListToTable(@DamageTypes)) 
    and (closed_date between @DateFrom and @DateTo)
    and (damage.investigator_id = @InvestigatorId or @InvestigatorId = -1)
  group by e.h_short_name, investigator_id, damage_type, 
    convert(datetime, convert(varchar, closed_date, 110))
  order by e.h_short_name, damage_type, closed_date

GO
grant execute on RPT_damage_productivity2 to uqweb, QManagerRole

/*
exec RPT_damage_productivity2 2676, -1, 'APPROVED,COMPLETED,INCOMING,PENDING', 1, '2006-01-01', '2006-09-30'
exec RPT_damage_productivity2 3510, 3510, 'APPROVED,COMPLETED,INCOMING,PENDING', 1, '2006-01-01', '2006-09-30'
*/
