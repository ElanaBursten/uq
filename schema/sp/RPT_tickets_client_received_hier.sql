if object_id('dbo.RPT_tickets_clients_received_hier') is not null
	drop procedure dbo.RPT_tickets_clients_received_hier
go

create proc RPT_tickets_clients_received_hier (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterList varchar(800),
  @ClientList varchar(800),
  @ManagerID int
)
as
  set nocount on

  -- This result set is designed such that a nearly default crosstab
  -- will generate the desired results; hence the user friendly column names,
  -- space before Tickets to make it sort first, etc.

  if @ManagerID=0 begin
    exec dbo.RPT_tickets_clients_received @StartDate, @EndDate, @CallCenterList, @ClientList
    return
  end

  if @ClientList = '' begin
  -- Call center numbers only, no client info needed

  select
    call_center as "Center",
    ticket_date as "Date",
    ' Tickets' as "Term",
    count(distinct ticket_id) as "Received",
    (select short_name from employee where emp_id=@ManagerID) as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
     inner join locate with (index(locate_ticketid))
       on ticket.ticket_id=locate.ticket_id
     inner join dbo.get_hier(@ManagerId,0) e
      on locate.assigned_to_id = e.h_emp_id
    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
    ) as t
  group by
    call_center,
    ticket_date

  end else begin

  select
    call_center as "Center",
    ticket_date as "Date",
    ' Tickets' as "Term",
    count(distinct ticket_id) as "Received",
    (select short_name from employee where emp_id=@ManagerID) as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
     inner join locate with (index(locate_ticketid))
       on ticket.ticket_id=locate.ticket_id
     inner join dbo.get_hier(@ManagerId,0) e
      on locate.assigned_to_id = e.h_emp_id
    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
    ) as t
  group by
    call_center,
    ticket_date

  union

  -- second result set: total by call center, date, date, client
  select
    call_center as "Center",
    ticket_date as "Date",
    client_code as "Term",
    count(ticket_id) as "Received",
    (select short_name from employee where emp_id=@ManagerID) as "Manager_short_name"
  from (
    select
      ticket.ticket_format as call_center,
      convert(datetime, convert(varchar(12), ticket.transmit_date, 102) , 102) as ticket_date,
      locate.client_code,
      ticket.ticket_id
    from ticket with (index(ticket_transmit_date))
     inner join locate with (index(locate_ticketid))
       on ticket.ticket_id=locate.ticket_id
     inner join dbo.get_hier(@ManagerId,0) e
      on locate.assigned_to_id = e.h_emp_id

    where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
     and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
     and (@ClientList = '*' or  
          locate.client_code in (select s from dbo.StringListToTable(@ClientList)))
     and locate.status <> '-N'
    ) as t
  group by
    t.call_center,
    t.ticket_date,
    client_code
  order by
    t.call_center,
    t.ticket_date,
    "Term"

  /*
  -- extra, small result set, for debugging
  select top 100
    ticket.ticket_format as call_center,
    ticket.transmit_date,
    ticket.ticket_number,
    ticket.ticket_id,
    locate.client_code
  from ticket
   inner join locate with (index(locate_ticketid))
     on ticket.ticket_id=locate.ticket_id
  where  ticket_format in (select S from dbo.StringListToTable(@CallCenterList))
   and ticket.transmit_date >= @StartDate and ticket.transmit_date < @EndDate
   and (locate.client_code in (select s from dbo.StringListToTable(@ClientList)))
  */

  end
go

grant execute on RPT_tickets_clients_received_hier to uqweb, QManagerRole

/*
-- for examples without manager, go to the other SP this delegates to.

exec dbo.RPT_tickets_clients_received_hier '2004-08-05', '2004-08-06', 'OCC2', 'WGL904', 697

exec dbo.RPT_tickets_clients_received_hier '2004-06-05', '2004-08-06', 'OCC2', 'WGL904', 697
53:00

Table 'Worktable'. Scan count 182117, logical reads 364238, physical reads 0, read-ahead reads 0.
Table '#554B7F24'. Scan count 1, logical reads 1, physical reads 0, read-ahead reads 1.
Table 'locate'. Scan count 16409327, logical reads 188014815, physical reads 75270, read-ahead reads 60585.
Table '#5733C796'. Scan count 1, logical reads 4, physical reads 0, read-ahead reads 4.
Table '#563FA35D'. Scan count 1, logical reads 1, physical reads 0, read-ahead reads 1.
Table 'ticket'. Scan count 2, logical reads 13794119, physical reads 207762, read-ahead reads 113217.



-- This one:
exec dbo.RPT_tickets_clients_received_hier '2003-09-29', '2003-09-30', 'NewJersey', '*',0
-- should be same as ...
exec dbo.RPT_tickets_clients_received_hier '2003-09-29', '2003-09-30', 'NewJersey', '*',212
--and this is a smaller subset
exec dbo.RPT_tickets_clients_received_hier '2003-09-29', '2003-09-30', 'NewJersey', '*',216


*/
