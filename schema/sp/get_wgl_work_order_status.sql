if object_id('dbo.get_wgl_work_order_status') is not null
  drop procedure dbo.get_wgl_work_order_status
go

create procedure get_wgl_work_order_status(@SinceDate datetime, @SinceID int)
as
  set nocount on

  declare @RespondableStatusList varchar(40)
  set @RespondableStatusList = 'WGL_INSP_ResponseList'
  
  if not exists (select sg_name 
    from status_group sg
    join status_group_item sgi on sg.sg_id = sgi.sg_id
    where sg_name = @RespondableStatusList and sg.active = 1 and sgi.active = 1)
      RAISERROR ('Missing or invalid Status Group %s. It may not exist, is inactive, or has no members.', 
        18, 1, @RespondableStatusList) with log

  -- Get the maximum number of work orders to send in a response request
  declare @MaxOrdersOut integer
  declare @MaxOrdersOutError varchar(200)
  declare @MaxOrdersOutConfig varchar(50)
  set @MaxOrdersOutConfig = 'WGLServiceMaxOrdersOut'
  set @MaxOrdersOutError = 'The Maximum Work Orders to send (%s) ' +
    'must be an integer greater than 0 in the configuration_data table. ' +
    'Fix it using the Q Manager Admin tool.'
  
  begin try
    select @MaxOrdersOut = convert(integer, coalesce(value, '0'))
    from configuration_data
    where name = @MaxOrdersOutConfig
    if (@MaxOrdersOut is null) or (@MaxOrdersOut < 1) begin
      set @MaxOrdersOut = 0
      RAISERROR (@MaxOrdersOutError, 18, 1, @MaxOrdersOutConfig) with log
  end
  end try
  begin catch
      RAISERROR (@MaxOrdersOutError, 18, 1, @MaxOrdersOutConfig) with log
  end catch

  declare @WOs table (
    wo_id integer not null primary key,
    wo_number varchar(20),
    client_wo_number varchar(40),
    status varchar(15), -- Outgoing response status
    status_date datetime,
    assigned_to_id int,
    vent_clearance_dist varchar(10),
    vent_ignition_dist varchar(10)
  )
  
  /* Gather the work orders that are ready for response and put all work_order data in a temp table to 
     avoid accessing work_order any more */
  insert into @WOs select top (@MaxOrdersOut)
    wo.wo_id, 
    wo.wo_number, 
    wo.client_wo_number,
    coalesce(st.outgoing_status, wo.status) as status, 
    wo.status_date,
    wo.assigned_to_id,
    coalesce(woi.vent_clearance_dist, ''),
    coalesce(woi.vent_ignition_dist, '')
  from work_order_inspection woi
  join work_order wo on wo.wo_id = woi.wo_id 
  join status_group_item sgi on (sgi.status = wo.status and sgi.active = 1)
  join status_group sg on (sg.sg_id = sgi.sg_id and sg.active = 1)
  left join status_translation st on (st.cc_code = wo.wo_source and st.status = wo.status and st.active = 1)
  where (woi.inspection_change_date > @SinceDate 
    or (woi.inspection_change_date = @SinceDate and wo.wo_id > @SinceID))
    and (wo.wo_source = 'WGL')
    and (sg.sg_name = @RespondableStatusList)
  order by woi.inspection_change_date, woi.wo_id
  
  -- Return the work order data set
  select wo.wo_id, wo.wo_number, wo.client_wo_number,
    wo.status, wo.status_date,
    woi.actual_meter_number, woi.actual_meter_location, woi.building_type, woi.cga_visits, 
    woi.map_status, woi.mercury_regulator, woi.alert_order_number, woi.inspection_change_date,
    coalesce(r1.description, woi.actual_meter_location) as meter_loc_desc,
    coalesce(r2.description, woi.building_type) as building_type_desc,
    coalesce(r3.description, woi.map_status) as map_status_desc,
    coalesce(e.emp_number, 'UQ') as emp_number,
    woi.gas_light
  from @WOs wo 
  join work_order_inspection woi on wo.wo_id = woi.wo_id
  left join reference r1 on (r1.type = 'metrloc' and r1.code = woi.actual_meter_location)
  left join reference r2 on (r2.type = 'bldtype' and r2.code = woi.building_type)
  left join reference r3 on (r3.type = 'mapstat' and r3.code = woi.map_status)
  left join employee e on (e.emp_id = wo.assigned_to_id)
  order by woi.inspection_change_date, woi.wo_id
  
  -- Return the work order remedy data set
  select wor.wo_id,
    case wt.work_type
      when 'VC' then 'VC' + vent_clearance_dist
      when 'VI' then 'VI' + vent_ignition_dist
      else wt.work_type
    end work_type,
    coalesce(wt.flag_color, 'NONE') flag_color
  from work_order_remedy wor
  join @WOs wo on wor.wo_id = wo.wo_id
  left join work_order_work_type wt on wor.work_type_id = wt.work_type_id
  where wor.active = 1 
  order by 1, 2  
GO

grant execute on get_wgl_work_order_status to uqweb, QManagerRole

/*
update configuration_data set value = 2 where name = 'WGLServiceMaxOrdersOut'
update configuration_data set value = 500 where name = 'WGLServiceMaxOrdersOut'

exec dbo.get_wgl_work_order_status '2013-07-01', 0
*/
