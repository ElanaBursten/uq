IF OBJECT_ID('dbo.check_summary_1') IS NOT NULL
	drop procedure dbo.check_summary_1
GO


CREATE Procedure check_summary_1 (
  @SummaryDate datetime, 
  @CallCenter varchar(30)	
)
As
-- This is the original check_summary stored proc, preserved here in case
-- of problems with the new version

IF   @CallCenter='FCL1'
  OR @CallCenter='FCL2'
  OR @CallCenter='FCV3'
  OR @CallCenter='NCA1'
  OR @CallCenter='SCA1'
  OR @CallCenter='SCA2'
  OR @CallCenter='FNV1'
  OR @CallCenter = 'FTL1'
  OR @CallCenter = 'FJL2'
  OR @CallCenter = 'FPK1'
  OR @CallCenter = 'FAM1'
  --OR @CallCenter = 'FOK1'
  OR @CallCenter = 'FCT1'
  OR @CallCenter = 'FCT2'
  OR @CallCenter = 'FCT3'
  OR @CallCenter = 'FJT1'
  OR @CallCenter = 'FGV1'
  OR @CallCenter = '300'
  OR @CallCenter = 'FCO1'
  OR @CallCenter = '1391'
  OR @CallCenter = '1392'
  OR @CallCenter = 'FGV2'
  OR @CallCenter = '9101'
  OR @CallCenter = '9301'
  OR @CallCenter = '9401'
  OR @CallCenter = '1102'
 EXEC check_summary_by_ticket @SummaryDate, @CallCenter

ELSE
 IF  @CallCenter='FMS1'
  OR @CallCenter='FJL1'
  OR @CallCenter = '6002'
 EXEC check_summary_by_ticketnumber @SummaryDate, @CallCenter

ELSE
 IF @CallCenter = 'FMW2'
 OR @CallCenter = 'FAQ1'
 EXEC check_summary_by_ticket_serial @SummaryDate, @CallCenter

ELSE
 IF @callCenter = 'FHL1'
 EXEC check_summary_by_locate_FHL1 @SummaryDate, @CallCenter

ELSE
 IF @CalLCenter = 'FCO2'
 EXEC check_summary_by_locate_FCO2 @SummaryDate, @CallCenter

ELSE
 IF @CallCenter = 'FPL1'
 EXEC check_summary_by_locate_FPL1 @SummaryDate, @CallCenter

ELSE
 IF @CallCenter = 'LQW1'
 EXEC check_summary_by_ticket_serial_LQW1 @SummaryDate, @CallCenter

ELSE
  EXEC check_summary_by_locate @SummaryDate, @CallCenter
GO

grant execute on check_summary_1 to uqweb, QManagerRole

/*
exec dbo.check_summary_1 '2003-02-13', 'FMS1'
*/
