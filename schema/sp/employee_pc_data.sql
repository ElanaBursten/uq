/*
  This gets the emp_id, report_to, and work profit center for employees
  This does not consider any payroll PC overrides.
*/
if object_id('dbo.employee_pc_data') is not null
	drop function dbo.employee_pc_data
go

create function employee_pc_data (@limit_to_pc_code varchar(15))
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  x integer,
  emp_pc_code varchar(15) NULL
)
as
begin
  declare @wave int

  INSERT INTO @results (emp_id, x, emp_pc_code)
  select e.emp_id, e.report_to, e.repr_pc_code
    from employee e

  update @results set x=null where emp_pc_code is not null

  select @wave = 1
  WHILE @wave < 10
  BEGIN
    update @results
      set emp_pc_code = (select repr_pc_code from employee e2 where e2.emp_id=x)
    where
       emp_pc_code is null and x is not null

    update @results
      set x = (select report_to from employee e3 where e3.emp_id=x)
    where
       emp_pc_code is null and x is not null

    SELECT @wave = @wave + 1
  END

  if @limit_to_pc_code is not null
    delete from @results
      where emp_pc_code <> @limit_to_pc_code or emp_pc_code is null
  return
end
GO

/*
select * from dbo.employee_pc_data(null)
select * from dbo.employee_pc_data('113')
select * from dbo.employee_pc_data('186')
*/
