if object_id('dbo.RPT_timesheet_export') is not null
	drop procedure dbo.RPT_timesheet_export
go

create proc dbo.RPT_timesheet_export (
	@WeekEnding datetime,
	@FinalOnly int)
as
  set nocount on

  select te.*, e.short_name
  from timesheet_export te
    inner join employee e on te.export_by = e.emp_id
  where week_ending = @WeekEnding
    and ((@FinalOnly = 0) or (export_date = (select max(export_date) from timesheet_export tei
          where (tei.profit_center = te.profit_center) and (tei.week_ending = te.week_ending))))
  order by te.week_ending asc, te.profit_center asc, te.export_date asc
go
grant execute on RPT_timesheet_export to uqweb, QManagerRole

/*
exec dbo.RPT_timesheet_export 'October 31, 2004', 0
*/
