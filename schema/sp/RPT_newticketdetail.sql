if object_id('dbo.RPT_newticketdetail') is not null
	drop procedure dbo.RPT_newticketdetail
go

create proc RPT_newticketdetail(
  @ManagerId int,  -- IGNORED!
  @All bit,
  @LocatorList varchar(8000),
  @PrintedForUserId int
)
as
  set nocount on

  declare @ticket_emp table (ticket_id int, emp_id int)
  declare @batch_id int

  -- identify the tickets to be printed
  if @All = 1 begin
    -- select all open tickets for each locator
    insert into @ticket_emp (
      ticket_id,
      emp_id)
    select 
      ticket.ticket_id,
      locate.assigned_to
    from dbo.IdListToTable(@LocatorList) as Locators
      inner join locate with (INDEX(locate_assigned_to))
        on locate.assigned_to = Locators.ID
      inner join ticket
        on ticket.ticket_id = locate.ticket_id
    group by ticket.ticket_id, locate.assigned_to 
  end
  else begin
    -- select all open tickets never printed or modified since last printed
    insert into @ticket_emp (
      ticket_id,
      emp_id)
    select 
      ticket.ticket_id,
      locate.assigned_to
    from dbo.IdListToTable(@LocatorList) as Locators
      inner join locate with (INDEX(locate_assigned_to))
        on locate.assigned_to = Locators.ID
      inner join ticket with (INDEX(PK_ticket_ticketid))
        on ticket.ticket_id = locate.ticket_id
    where
      (dbo.TicketNeedsPrinting(ticket.ticket_id, Locators.ID, ticket.modified_date) = 1)
    group by ticket.ticket_id, locate.assigned_to 
  end

  if not exists(select * from @ticket_emp)
    -- don't create an empty ticket batch
    set @batch_id = 0
  else begin
    -- create the ticket_batch record and save the batch ID
    insert into ticket_batch (
      batch_printed,
      print_date,
      printed_for_user_id)
    values (
      0,               -- this will get set to 1 after the PDF is generated
      getdate(),
      @PrintedForUserId)
    set @batch_id = @@identity

    -- create the ticket_printing records
    insert into ticket_printing (
      ticket_batch_id,
      ticket_id,
      locator_id)
    select
      @batch_id,
      ticket_id,
      emp_id
    from @ticket_emp
  end

  select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image,
    employee.emp_id,
    employee.short_name,
    @batch_id as ticket_batch_id,
    call_center.xml_ticket_format
  from @ticket_emp as te
    inner join ticket
      on ticket.ticket_id = te.ticket_id
    inner join employee
      on employee.emp_id = te.emp_id
    left join call_center on call_center.cc_code = ticket.ticket_format  
  order by
    employee.short_name,
    ticket.ticket_id

  select
    locate.ticket_id,
    locate.client_code,
    locate.status,
    locate.high_profile,
    locate.qty_marked,
    locate.closed,
    locate.closed_date,
    assignment.locator_id
  from @ticket_emp as te
    inner join locate     on locate.ticket_id = te.ticket_id
    inner join assignment on assignment.locate_id = locate.locate_id 
                         and assignment.locator_id = te.emp_id
  where assignment.active = 1
   and locate.status<>'-N'
  order by
    locate.ticket_id, locate.locate_id
go
grant execute on RPT_newticketdetail to uqweb, QManagerRole

/*
exec dbo.RPT_newticketdetail 1866, 1, '3512,10667,3832,13863,2314,3505,9223,12561', 2162
*/

