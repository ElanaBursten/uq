/*  This file as all of the SPs related to the
   
    Damage Estimate Changes Report
    Removed: old versions of SP that are nowhere near correct anymore
*/

if object_id('dbo.RPT_damageestimatechanges4') is not null
	drop procedure dbo.RPT_damageestimatechanges4
go

create procedure dbo.RPT_damageestimatechanges4 (
  @RecdStart  datetime, -- Damage claim received @RecdStart..@RecdEnd
  @RecdEnd    datetime,
  @EstStart   datetime, -- Estimate received @EstStart..@EstEnd
  @EstEnd     datetime,
  @DmgStart   datetime, -- Damage date
  @DmgEnd     datetime,
  @State      char(2),
  @ProfitCenter varchar(10),
  @LiabilityType int
)
as
  declare @results table (
    uq_damage_id int not null,
    damage_date datetime,
    uq_notified_date datetime,
    client_claim_id varchar(25) NULL,
    profit_center varchar(15),
    utility_co_damaged varchar(40),
    size_type varchar(50) NULL,
    location varchar(80) NULL,
    city varchar(40) NULL,
    state varchar(2) NULL,
    uq_resp_code varchar(4) NULL,
    exc_resp_code varchar(4) NULL,

    after_modified_date datetime)

  insert into @results (
    uq_damage_id,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    location,
    city,
    state,
    uq_resp_code,
    exc_resp_code )
  select distinct
    damage.damage_id,
    damage.damage_date,
    damage.uq_notified_date,
    damage.client_claim_id,
    damage.profit_center,
    damage.utility_co_damaged,
    damage.size_type,
    damage.location,
    damage.city,
    damage.state,
    damage.uq_resp_code,
    damage.exc_resp_code
  from damage_estimate de
    inner join damage on damage.damage_id = de.damage_id
  where
    (de.modified_date between @EstStart and @EstEnd) and
    (de.active = 1) and
    (damage.uq_notified_date between @RecdStart and @RecdEnd) and
    (damage.damage_date between @DmgStart and @DmgEnd) and
    ((@State='') or (damage.state = @State )) and
    (( @ProfitCenter = '') or (damage.profit_center=@ProfitCenter ))

  -- need the resp codes as of the end of the period
  update @results
  set after_modified_date =
    (select min(modified_date)
     from damage_history hist
     where (uq_damage_id = hist.damage_id)
       and (hist.modified_date >= @EstEnd))

  -- If there is a history record right after the end date, it contains the
  -- responsibility at the end date.  If not, use the current responsibility.
  update @results
   set uq_resp_code = hist.uq_resp_code
   from damage_history hist
    where hist.damage_id = uq_damage_id
      and hist.modified_date = after_modified_date

  update @results
  set exc_resp_code = hist.exc_resp_code
   from damage_history hist
    where hist.damage_id = uq_damage_id
      and hist.modified_date = after_modified_date

  select 
    uq_damage_id AS damage_id,
    damage_date,
    uq_notified_date,
    client_claim_id,
    profit_center,
    utility_co_damaged,
    size_type,
    city,
    state,
    uq_resp_code,
    case
      when (location <> '') and (city <> '') then
        location + ', ' + city
      when (location <> '') then
        location
      else
        city
    end as location
 from @results
   where
    (
    /* This avoids the need for an IF or for client-side SQL */
    ((uq_resp_code is not null) and @LiabilityType=0) OR
    ((uq_resp_code is null) and @LiabilityType=1) OR
    ((uq_resp_code is not null and exc_resp_code is null) and @LiabilityType=2) OR
    ((uq_resp_code is not null and exc_resp_code is not null) and @LiabilityType=3) OR
    ((uq_resp_code is null and exc_resp_code is null) and @LiabilityType=4) OR
    (@LiabilityType=5)
    )
  order by
    uq_damage_id

go

grant execute on RPT_damageestimatechanges4 to uqweb, QManagerRole

if object_id('dbo.RPT_damageestimatechanges_detail') is not null
	drop procedure dbo.RPT_damageestimatechanges_detail
go

create procedure dbo.RPT_damageestimatechanges_detail (
  @DamageId   integer,
  @EstStart   datetime,
  @EstEnd     datetime
)
as
  declare @InitialEstimate money
  declare @CurrentEstimate money
  declare @before_modified_date datetime
  declare @uq_resp_code varchar(4)
  declare @estimates       table (
    estimate_id   integer     not null,
    damage_id     integer     not null,
    third_party   bit         not null,
    emp_id        integer     not null,
    amount        money       null,
    company       varchar(30) null,
    contract      varchar(25) null,
    phone         varchar(25) null,
    comment       varchar(50) null,
    modified_date datetime    not null,
    active        bit         not null
  )
begin
  -- Get the estimates from the beginning of time up to the end-date
  insert into @estimates (
    estimate_id,
    damage_id,
    third_party,
    emp_id,
    amount,
    company,
    contract,
    phone,
    comment,
    modified_date,
    active
  )
  select
    estimate_id,
    damage_id,
    third_party,
    emp_id,
    amount,
    company,
    contract,
    phone,
    comment,
    modified_date,
    active
  from damage_estimate
  where
    (damage_id = @DamageId) and
    (active = 1) and
    (modified_date <= @EstEnd)

  -- get the estimate from before the start of the date range
  select
    @InitialEstimate = amount
  from @estimates
  where
    modified_date = (
      select max(modified_date)
      from @estimates
      where modified_date <= @EstStart)

  -- see if we were liable at that time:
  -- need the resp codes as of the start of the period
  select @before_modified_date =
    (select min(modified_date)
     from damage_history hist
     where (@DamageId = hist.damage_id)
       and (hist.modified_date >= @EstStart))

  select @uq_resp_code = 'RESP'

  -- If there is a history record right after that date, use it
  select @uq_resp_code = hist.uq_resp_code
   from damage_history hist
    where hist.damage_id = @DamageId
      and hist.modified_date = @before_modified_date

  if @uq_resp_code IS NULL
    select @InitialEstimate = 0

  -- get the last estimate, which might even be before the date range
  select
    @CurrentEstimate = amount
  from @estimates
  where
    modified_date = (select max(modified_date) from @estimates)

  select
    @EstStart as InitialDate,
    @EstEnd as CurrentDate,
    isnull(@InitialEstimate, 0) as InitialEstimate,
    isnull(@CurrentEstimate, 0) as CurrentEstimate,
    isnull(@CurrentEstimate, 0)-isnull(@InitialEstimate, 0) as Delta,
    estimate_id,
    damage_id,
    third_party,
    emp_id,
    amount,
    company,
    contract,
    phone,
    comment,
    modified_date,
    active
  from @estimates
  where
    modified_date between @EstStart and @EstEnd
  order by
    modified_date
end
go

grant execute on RPT_damageestimatechanges_detail to uqweb, QManagerRole


if object_id('dbo.RPT_damage_resp_changes2') is not null
	drop procedure dbo.RPT_damage_resp_changes2
go

create procedure dbo.RPT_damage_resp_changes2 (
  @FromDate   datetime, -- Responsibility modified date range
  @ToDate     datetime,
  @RecdStart  datetime, -- Notified date
  @RecdEnd    datetime,
  @DmgStart   datetime, -- Damage date
  @DmgEnd     datetime,
  @State        char(2),
  @ProfitCenter varchar(10),
  @ChangedToUQ  bit
)
as
begin
  declare @results table (
    uq_damage_id int not null,
    damage_date datetime,
    uq_notified_date datetime,
    profit_center varchar(15),
    utility_co_damaged varchar(40),
    city varchar(40),
    state varchar(2),
    uq_resp_code varchar(10),
    uq_resp_code_copy varchar(10),
    previous_uq_resp_code varchar(10),
    previous_modified_date datetime,
    current_estimate decimal(12, 2),
    estimate_date datetime,
    after_modified_date datetime,
    critical_modified_date datetime)

  insert into @results (uq_damage_id, damage_date, uq_notified_date, profit_center,
    utility_co_damaged, city, state, uq_resp_code)
  select distinct
    damage_id, damage_date, uq_notified_date, profit_center, utility_co_damaged,
    city, state, uq_resp_code
  from damage
  where ((@State = '') or (state = @State))
    and ((@ProfitCenter = '') or (profit_center = @ProfitCenter))
    and (modified_date >= @FromDate)  -- The full modified date range is enforced later
    and (uq_notified_date between @RecdStart and @RecdEnd)
    and (damage_date between @DmgStart and @DmgEnd)
    and (uq_notified_date < @FromDate)

  update @results
  set previous_modified_date =
    (select min(modified_date)
     from damage_history hist
     where (uq_damage_id = hist.damage_id)
       and (modified_date >= @FromDate))

  -- If a damage has not changed since FromDate, it can not be in the result set
  delete from @results
  where previous_modified_date is null

  -- If the first change after FromDate is after the ToDate, it can not be in the result set
  delete from @results
  where previous_modified_date > @ToDate

  update @results
  set previous_uq_resp_code =
   (select uq_resp_code from damage_history hist
    where hist.damage_id = uq_damage_id
      and hist.modified_date = previous_modified_date)

  update @results
  set after_modified_date =
    (select min(modified_date)
     from damage_history hist
     where (uq_damage_id = hist.damage_id)
       and (hist.modified_date >= @ToDate))

  -- If there is a history record right after the end date, it contains the
  -- responsibility at the end date. 
  update @results
  set uq_resp_code = hist.uq_resp_code
   from damage_history hist
    where hist.damage_id = uq_damage_id
      and hist.modified_date = after_modified_date

  -- This is a bad hack to allow using same-named fields in the subquery below
  update @results set uq_resp_code_copy = uq_resp_code

  -- The critical_modified_date is the last date the damage was some
  -- responsibility value other than the terminal value
  update @results
  set critical_modified_date = (
    select max(hist.modified_date)
    from damage_history hist
    where uq_damage_id = hist.damage_id
      and hist.modified_date between @FromDate and @ToDate
      and ((hist.uq_resp_code <> uq_resp_code_copy) or
          ((hist.uq_resp_code is null)     and (uq_resp_code_copy is not null)) or
          ((hist.uq_resp_code is not null) and (uq_resp_code_copy is null)))
  )

  if @ChangedToUQ = 1
    delete from @results
    where not ((previous_uq_resp_code is null)
               and (uq_resp_code is not null))
  else
    delete from @results
    where not ((previous_uq_resp_code is not null)
               and (uq_resp_code is null))

  -- Get the estimate at the time of the end date
  update @results
  set current_estimate =
   (select top 1 amount from damage_estimate
    where damage_id = uq_damage_id
      and modified_date =
        (select max(modified_date) from damage_estimate
         where damage_id = uq_damage_id and third_party = 0
         and modified_date <= @ToDate)
    order by estimate_id desc)

  select * from @results order by uq_damage_id
end
go

grant execute on dbo.RPT_damage_resp_changes2 to uqweb, QManagerRole

/*
RPT_damage_resp_changes2 'April 1, 2003', 'April 7, 2003', 'January 1, 1899',
  'January 1, 2099', 'January 1, 2003', 'January 1, 2004', '', 'FND', 1


select damage.damage_id, count(estimate_id) from
  damage left join damage_estimate de on damage.damage_id = de.damage_id
 and damage.damage_id>62000
 group by damage.damage_id

select * from damage where damage_id=55146 
select * from damage_estimate where damage_id=55146 
select * from damage_history where damage_id=55146 
*/
