USE [QM]  -- filter cancels
GO
/****** Object:  StoredProcedure [dbo].[NIPSCO_Adjustment]    Script Date: 2/18/2020 2:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[NIPSCO_Adjustment]     --QM-86	-- uncomment for release
    @BillID int											-- uncomment for release
AS														-- uncomment for release
Declare
  @NewRates table (BillID Int, TicketID Int, LocateID Int, TicketNbr varChar(20), TktStatus varChar(5), ClientID Int, ClientCode varChar(10), TotalFeetMarked Int, BilledUnits INT, TicketRate Money, BilledRate Money, Bucket varChar(60));--LineItemText varChar(60), 
Declare
  @OutputList table (ClientID Int, Output varChar(20));

Insert into @OutputList     -- extracts area name from Client Name
SELECT client_id, substring(
							client_name, 
							CharIndex('(',client_name)+1,
							CharIndex(')',client_name)-CharIndex('(',client_name)-1
							) as Output
  FROM [QM].[dbo].[client]
  where call_center='1851' 
  and oc_code in (Select [term]
							from [dbo].[term_group_detail]
							where [term_group_id] in (
													SELECT [term_group_id]
														FROM [QM].[dbo].[term_group]
														where (group_code = '185NipscoGasTerms')
														   or (group_code = '185NipscoElectricTerms') 
													)                    
				 )


--select * from @OutputList   --comment for release

Declare   
--@BillID int,     --comment for release
          @LocateID Int, @Status varChar(5), @ClientID Int, @ClientCode varChar(10), @LineItemText  varChar(60), @Bucket varChar(60); 
--Set @BillID =65413-- (Select max(bill_id) from billing_header)  --comment for release

Set @LineItemText= 'Comb Gas and Elec 47-100';
Set @Status = 'COMB';
Set @ClientID= (select client_id from client where oc_code like '%NIPSCO%' and call_center = '1851');
Set @ClientCode = 'NIPSCO';  
Set @LocateID= 0

BEGIN TRY
  BEGIN TRANSACTION
	insert into @NewRates
	select @BillID, T.Ticket_ID, L.Locate_ID, BT.ticket_number, @Status, @ClientID, @ClientCode, 
		sum(IIF(units_marked=0,1, units_marked)) TotalFeetMarked, 
		ceiling(sum(IIF(units_marked=0,1, units_marked))/500.00) BilledUnits, BR.Rate, 
		(ceiling(sum(IIF(units_marked=0,1, units_marked))/500.00)*BR.Rate) BilledRate, BT.Bucket
	from billing_detail BT 
	Join [dbo].[billing_rate] BR on Substring(BR.bill_type, 0,5) = Substring(BT.Bucket, 0,5)
	inner join Ticket T on (BT.ticket_number=T.ticket_number)
	inner join Locate L on (T.ticket_id = L.ticket_id)
	where bill_id = @BillID
	and BT.area_name='Both'
	and Upper(BR.billing_cc) = 'NIPSCO'
	and T.ticket_type NOT like '%CNCL%'
	and BR.rate is not null
	and L.client_code in   (Select [term]
							from [dbo].[term_group_detail]
							where [term_group_id] = (
							SELECT [term_group_id]
								FROM [QM].[dbo].[term_group]
								where group_code = '185NipscoGasTerms'))
	group by bill_id, T.Ticket_ID, L.Locate_ID, BT.ticket_number, BR.Rate, BT.Bucket

	delete from billing_detail               --delete the Elec records from the on disk set
	where billing_cc in (Select [term]
							 from [dbo].[term_group_detail]
							 where [term_group_id] = (
							 SELECT [term_group_id]
								FROM [QM].[dbo].[term_group]
								where group_code = '185NipscoElectricTerms'))
	and bill_id = @BillID
	and area_name='Both'

--Select * from @NewRates   --comment for release

	Update billing_detail  -- set the billing_details to the NIPSCO rates
		set qty_marked = NR.BilledUnits,
		line_item_text=OP.Output+' '+@LineItemText, 
		Status=nr.TktStatus,
		billing_cc=nr.ClientCode,
		client_id =nr.ClientID,
		rate=nr.TicketRate,
		price=nr.BilledRate, 
		units_marked=nr.TotalFeetMarked,
		raw_units=nr.BilledUnits,
		qty_charged=NR.BilledUnits
	From billing_detail BD
	inner join @NewRates NR on (BD.locate_id = NR.LocateID) 
	inner join @OutputList OP on (BD.client_id=OP.ClientID) 
	where bill_id = @BillID 

	Update billing_detail  -- set the billing_details to the NIPSCO rates
	Set rate=0.0,
		price=0.0,
		Status='NC'
	From billing_detail
	where bill_id = @BillID
	and bucket like '%Duplicate%' 

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
  Select
	ERROR_NUMBER() as ErrorNumber,
	ERROR_LINE() as ErrorLine,
	ERROR_STATE() as ErrorState,
	ERROR_SEVERITY() as ErrorSeverity,
	ERROR_PROCEDURE() as ErrorProcedure,
	ERROR_MESSAGE() as ErrorMessage;

	IF (XACT_STATE()) = -1
		ROLLBACK TRANSACTION

END CATCH



Select * from billing_detail   --comment for release
where bill_id = @BillID   --comment for release


RETURN 0 --uncomment for release

