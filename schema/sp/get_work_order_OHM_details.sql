--QMANTWO-391
if object_id('dbo.get_work_order_OHM_details') is not null
  drop procedure dbo.get_work_order_OHM_details
go

create proc get_work_order_OHM_details(@WorkOrderID int)
as
  set nocount on

  select 'work_order_OHM_details' as tname, work_order_OHM_details.*
  from work_order_OHM_details 
    where work_order_OHM_details.wo_id = @WorkOrderID

go