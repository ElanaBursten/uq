if object_id('dbo.fix_duplicate_assignments') is not null
  drop procedure dbo.fix_duplicate_assignments
go

create proc fix_duplicate_assignments
as
begin
  declare @CutOff int
  select @CutOff = isnull(max(assignment_id) - 1000000,0) from assignment

  declare @S varchar(2000)
  select @S = '
  update assignment
  set active = 0
  where assignment_id in (
    select a1.assignment_id
    from assignment a1
      inner join assignment a2 on a1.locate_id=a2.locate_id
    where a1.active = 1 and a2.active = 1
      and a1.assignment_id < a2.assignment_id '
  + ' and a1.assignment_id >= ' + str(@CutOff)
  + ' and a2.assignment_id >= ' + str(@CutOff)
  + ')'

--  print '!' + @S + '!'
  exec(@S)
end
go
