if object_id('dbo.GetBreadCrumbListByMgrID') is not null
  drop procedure dbo.GetBreadCrumbListByMgrID
GO

CREATE PROCEDURE GetBreadCrumbListByMgrID( 
  @MgrID int) 
AS 
BEGIN
-- '"emp_breadcrumbs"',
  SELECT 'emp_breadcrumbs', @MgrID as mgr_id, e.emp_id, e.short_name, g.latitude lat, g.longitude lng, g.added_date last_update, 
  (select top 1 DateAdd(n, local_utc_bias, local_date) FROM sync_log 
			  WHERE emp_id = e.emp_id
			  ORDER BY sync_id DESC) as UTC,
			  e.short_name as label
			  --,
			  --(select top 1 ea.activity_type 
     --          FROM employee_activity ea 
			  -- where ea.emp_id = e.emp_id 
			  -- order by emp_activity_id desc) AS last_activity 
  FROM employee e
  join gps_position g on g.added_by_id = e.emp_id
  WHERE e.report_to = @MgrID
  and e.type_id = 11
  and e.active = 1
  and e.emp_id = (SELECT TOP 1 g.added_by_id as emp_id 
			  	  FROM gps_position g
			  	  WHERE g.added_by_id = e.emp_id
				  ORDER BY g.gps_id DESC)  
  and g.gps_id = (select max(g2.gps_id) 
				   FROM gps_position g2
				   WHERE g2.added_by_id = e.emp_id) 

END
GO

grant execute on GetBreadCrumbListByMgrID to uqweb, QManagerRole

----------------------------------------------------------------
--Example
--exec GetBreadCrumbListByMgrID 6098