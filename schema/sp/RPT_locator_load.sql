if object_id('dbo.RPT_locator_load_2') is not null
	drop procedure dbo.RPT_locator_load_2
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

CREATE Procedure RPT_locator_load_2 (
  @ManagerID int,
	@Today datetime,  -- Can contain the current date/time rather than date-only.
  @EmployeeStatus int
	)
as

set nocount on

declare @results table (
	emp_id int NOT NULL primary key,
	short_name varchar(45),
	emp_number varchar(30),
	h_report_level integer,
	h_report_to integer,
	h_active bit,
  h_namepath varchar(450) NULL,
	h_1 varchar(30) NULL,
	h_2 varchar(30) NULL,
	h_3 varchar(30) NULL,
	h_4 varchar(30) NULL,
	h_5 varchar(30) NULL,
	h_6 varchar(30) NULL,
	h_7 varchar(30) NULL,
	h_8 varchar(30) NULL,
	h_9 varchar(30) NULL,
	TotalLoadL int default 0,
	TotalLoadT int default 0,
	NewL int default 0,
	NewT int default 0,
	OngoingL int default 0,
	OngoingT int default 0,
	EmergencyL int default 0,
	EmergencyT int default 0,
	CarryoverL int default 0,
	CarryoverT int default 0,
	OverdueL int default 0,
	OverdueT int default 0
)

declare @work table (
	emp_id int NOT NULL,
	status varchar(20) NOT NULL,
	ticket_id int NOT NULL,
	Bucket varchar(30) NOT NULL
)

declare @totals table (
	t_emp_id int NOT NULL,
	Bucket varchar(30) NOT NULL,
	NL int,
	NT int
)
declare @StartOfToday datetime
set @StartOfToday = convert(datetime, convert(varchar, @Today, 101))

insert into @results (emp_id, short_name, emp_number,
  h_report_level, h_report_to, h_active, h_namepath,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9 )
select h_emp_id, h_short_name_tagged, h_emp_number,
  h_report_level, h_report_to, h_active, h_namepath,
  h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9
 from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus)

insert into @work
select r.emp_id, locate.status, locate.ticket_id,
  case
   when locate.status='O' then 'Ongoing'
   when ticket.due_date < @Today then 'Overdue'
   when ticket.kind='EMERGENCY' then 'Emergency'
   when ticket.transmit_date > @StartOfToday then 'New'
   else 'Carryover'
  end as Bucket
 from @results r
  inner join locate with (INDEX(locate_assigned_to)) on locate.assigned_to=r.emp_id
  inner join ticket with (index(PK_ticket_ticketid)) on locate.ticket_id = ticket.ticket_id

insert into @totals
select emp_id, Bucket, count(*) as NL, count(distinct ticket_id) as NT
 from @work group by emp_id, bucket

update @results set NewL = NL, NewT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='New'

update @results set OngoingL = NL, OngoingT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Ongoing'

update @results set EmergencyL = NL, EmergencyT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Emergency'

update @results set CarryoverL = NL, CarryoverT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Carryover'

update @results set OverdueL = NL, OverdueT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Overdue'

update @results
  set TotalLoadL = NewL + OngoingL + EmergencyL + CarryoverL + OverdueL,
      TotalLoadT = NewT + OngoingT + EmergencyT + CarryoverT + OverdueT

-- remove inactive locators with no load
delete from @results 
 where TotalLoadL = 0
  and TotalLoadT = 0
  and h_active = 0
  and (emp_id not in (select h_report_to from @results))

select * from @results
 order by h_namepath, short_name

go
grant execute on RPT_locator_load_2 to uqweb, QManagerRole

/*
exec dbo.RPT_locator_load_2 2410, '2003-09-26 12:00:00', 1
exec dbo.RPT_locator_load_2 1787, '2003-09-26 12:00:00', 1

select * from get_report_hier(1787, 0)
*/
