if object_id('dbo.RPT_initialstatus_hier2') is not null
  drop procedure dbo.RPT_initialstatus_hier2
go

create procedure dbo.RPT_initialstatus_hier2 (
  @StartDate  datetime,
  @EndDate    datetime,
  @CallCenterCodes varchar(500),
  @ManagerID int,
  @Arrival bit
)
as
  set nocount on

  declare @results table (
    client_code varchar (15),
    status varchar(20),
    loc_client_id int,
    due_date datetime,
    ticket_number varchar(25),
    work_type varchar(90),
    ticket_type varchar(40),
    ticket_id int,
    locator_id int,
    short_name varchar(30),
    initial_status_date datetime,
    last_sync_date datetime,
    arrival_date datetime,
    call_center varchar(20)
  )

  declare @CallCenterList table (
    center varchar(100) primary key
  )

  insert into @CallCenterList
    select S from dbo.StringListToTable(@CallCenterCodes)

  declare @tix table (
    ticket_id int primary key,
    due_date datetime,
    ticket_number varchar(25),
    work_type varchar(90),
    ticket_type varchar(40),
    ticket_format varchar(20)
  )

  -- Get the list of tickets in the date range for this call center
  insert into @tix
    select ticket_id, due_date, ticket_number, work_type, ticket_type, ticket_format
    from ticket
     inner join @CallCenterList ccl on ccl.center=ticket.ticket_format
    where ticket.due_date between @StartDate and @EndDate

  if @ManagerID=0

  insert into @results
  select
    locate.client_code,
    locate.status,
    locate.client_id,
    tix.due_date,
    tix.ticket_number,
    tix.work_type,
    tix.ticket_type,
    tix.ticket_id,
    e.emp_id,
    e.short_name,
    -- get the initial status date for each locate
    (select min(locate_status.status_date) from locate_status
     where locate_status.locate_id = locate.locate_id and locate_status.status <> '-R') as initial_status_date,
    -- get the last sync date for each locator
    (select max(sync_log.sync_date) from sync_log
     where sync_log.emp_id = e.emp_id) as last_sync_date,
    -- get the earliest arrival date
    (select min(a.arrival_date) from jobsite_arrival a
     where a.ticket_id = tix.ticket_id and a.active = 1) as arrival_date,
     tix.ticket_format
  from @tix tix
    inner join locate on locate.ticket_id = tix.ticket_id
    inner join employee e
      on coalesce(locate.assigned_to,locate.closed_by_id) = e.emp_id

  else

  insert into @results
  select
    locate.client_code,
    locate.status,
    locate.client_id,
    tix.due_date,
    tix.ticket_number,
    tix.work_type,
    tix.ticket_type,
    tix.ticket_id,
    e.h_emp_id,
    e.h_short_name,
    -- get the initial status date for each locate
    (select min(locate_status.status_date) from locate_status
     where locate_status.locate_id = locate.locate_id and locate_status.status <> '-R') as initial_status_date,
    -- get the last sync date for each locator
    (select max(sync_log.sync_date) from sync_log
     where sync_log.emp_id = e.h_emp_id) as last_sync_date,
    -- get the earliest arrival date
    (select min(a.arrival_date) from jobsite_arrival a
     where a.ticket_id = tix.ticket_id and a.active = 1) as arrival_date,
     tix.ticket_format
  from @tix tix
    inner join locate on locate.ticket_id = tix.ticket_id
    inner join dbo.get_hier(@ManagerId,0) e
      on coalesce(locate.assigned_to,locate.closed_by_id) = e.h_emp_id

  declare @late_basis varchar(8)
  set @late_basis = case @arrival when 1 then 'arrived' else 'statused' end

  -- Return only the late or potentially late tickets
  select
    case
      when (@arrival = 0 and res.initial_status_date > res.due_date)
        or (@arrival = 1 and res.arrival_date > res.due_date) then
        'LATE (' + @late_basis + ')'
      when (res.last_sync_date > res.due_date) then
        'LATE (not ' + @late_basis + ')'
      else
        'POTENTIALLY LATE (might sync)'
    end as category,
    res.client_code,
    client.client_name,
    res.due_date, res.locator_id,
    res.ticket_number, res.status,
    res.initial_status_date, res.arrival_date,
    case
      when @arrival = 0 then res.initial_status_date
      when @arrival = 1 then
        case
          when res.initial_status_date <= res.arrival_date then res.initial_status_date
          else res.arrival_date
        end
    end as earliest_date,
    case
      when (@arrival = 0) then 'S'
      when (@arrival = 1) then
        case
          when res.initial_status_date <= res.arrival_date then 'S'
          else 'A'
        end
    end as date_type,
    res.last_sync_date, res.work_type, res.short_name,
    res.ticket_type, res.ticket_id, res.call_center
  from @results res
    inner join client on client.client_id = res.loc_client_id
  where
    (@arrival = 0 and ((res.initial_status_date is null) or (res.initial_status_date > res.due_date))) or
    (@arrival = 1 and ((res.arrival_date is null) or (res.arrival_date > res.due_date)))
  order by
    res.client_code,
    category,
    res.due_date,
    res.ticket_number

  declare @sum2 table (category varchar(10) not null, ticket_id int not null)

  insert into @sum2
  select
    case
      when (@arrival = 0 and sum1.initial_status_date > sum1.due_date)
        or (@arrival = 1 and sum1.arrival_date > sum1.due_date) then
        'L' -- LATE
      when (sum1.last_sync_date > sum1.due_date) then
        'L' -- LATE
      else
        'P' -- POTENTIALLY LATE
    end as category,
    sum1.ticket_id
  from @results sum1
  where
    (@arrival = 0 and ((sum1.initial_status_date is null) or (sum1.initial_status_date > sum1.due_date))) or
    (@arrival = 1 and ((sum1.arrival_date is null) or (sum1.arrival_date > sum1.due_date)))

  declare @totaltix int
  declare @latetix int
  declare @totalloc int
  declare @lateloc int

  select @totaltix = count(distinct ticket_id) from @sum2
  select @latetix = count(distinct ticket_id) from @sum2 where category = 'L'
  select @totalloc = count(*) from @sum2
  select @lateloc = count(*) from @sum2 where category = 'L'

  select
    @latetix as 'LateTickets',
    @totaltix-@latetix as 'PotLateTickets',
    @lateloc as 'LateLocates',
    @totalloc-@lateloc as 'PotLateLocates',
    (select short_name from employee where emp_id=@ManagerId) as man_name
go

grant execute on RPT_initialstatus_hier2 to uqweb, QManagerRole

/*
exec dbo.RPT_initialstatus_hier2 '2009-05-01', '2009-05-05', 'OCC2', 811, 0
exec dbo.RPT_initialstatus_hier2 '2009-05-01', '2009-05-05', 'OCC2', 811, 1
exec dbo.RPT_initialstatus_hier2 '2006-06-22', '2007-06-23', '1391', 0, 1
exec dbo.RPT_initialstatus_hier2 '2006-06-22', '2007-06-23', 'Atlanta', 0, 0
exec dbo.RPT_initialstatus_hier2 '2001-01-01', '2008-01-01', 'FNV1,FNV2', 2676, 1

exec dbo.RPT_initialstatus_hier2 '2009-04-25', '2009-04-29', 'FMW1,FMW3', 2676, 0
exec dbo.RPT_initialstatus_hier2 '2009-04-25', '2009-04-29', 'FMW1,FMW3', 2676, 1
*/
