-- ================================================
-- Update_Workload_Date
-- ================================================
if object_id('dbo.update_workload_date') is not null
  drop procedure dbo.update_workload_date
GO

create proc update_workload_date (@TicketID int, @LocatorID int, @WorkloadDate datetime, @AddedBy int = null)
as
  begin transaction
	SET NOCOUNT ON;
	update assignment set workload_date = @WorkloadDate
	where locate_id in (select locate_id from locate where ticket_id = @TicketID and locator_id = @LocatorID)
    
  commit transaction
GO

grant execute on update_workload_date to uqweb, QManagerRole
GO