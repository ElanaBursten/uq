-- Log all asset changes to asset_history

if exists(select name FROM sysobjects where name = 'asset_iud' AND type = 'TR')
  DROP TRIGGER asset_iud
GO

CREATE TRIGGER asset_iud ON asset
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @NumInserted int
  declare @NumDeleted int
  declare @ChangeTime datetime
  set @NumInserted = (select count(*) from inserted)
  set @NumDeleted = (select count(*) from deleted)
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
  if (@NumDeleted > 0)
  begin
    insert into asset_history
     (asset_id, asset_code, asset_number, active, modified_date, comment, condition, archive_date, solomon_profit_center, serial_num, description, cost)
    select
      asset_id, asset_code, asset_number, active, modified_date, comment, condition, @ChangeTime,  solomon_profit_center, serial_num, description, cost
    from deleted
  end

  -- Insert/update operation: update modified_date for the changed rows
  if (@NumInserted > 0)
  begin
    update asset
    set modified_date = @ChangeTime
    where asset_id in (select asset_id from inserted)
  end
END
go

/*
select * from asset where asset_id = 80000
select * from asset_history where asset_id = 80000
update asset set condition = 'POOR' where asset_id = 80000
select * from asset_history where asset_id = 80000
*/
