if object_id('dbo.RPT_TicketsDue_4') is not null
  drop procedure dbo.RPT_TicketsDue_4
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create procedure dbo.RPT_TicketsDue_4 (@ManagerId int, @DueDate datetime, @EmployeeStatus int)
as
  set nocount on

  declare @emps table (
    emp_id int primary key,
    short_name varchar(50)
  )

  declare @tickets_due table (
    due_date_only varchar(12),
    short_name varchar(30),
    ticket_number varchar(20),
    ticket_id integer,
    due_date datetime,
    ticket_type varchar(38),
    work_type varchar(90),
    client_code varchar(10),
    client_name varchar(40),
    locator_id integer,
    [status] varchar(5),
    work_remarks varchar(1200) null,
    closed bit,
    last_sync_date datetime null,
    priority varchar(30),
    ticket_count integer,
    locate_id integer
  )
    

  insert into @emps
  select h_emp_id, h_short_name
  from dbo.get_report_hier3(@ManagerId, 0, 99, @EmployeeStatus)

  insert into @tickets_due
  select
    convert(datetime, convert(varchar(12), ticket.due_date, 102), 102) as due_date_only,
    emps.short_name,
    ticket.ticket_number,
    ticket.ticket_id,
    ticket.due_date,
    ticket.ticket_type,
    ticket.work_type,
    locate.client_code,
    client.client_name,
    emps.emp_id as locator_id,
    locate.status,
    ticket.work_remarks,
    locate.closed,
    (select max(sync_log.sync_date) from sync_log
      where emps.emp_id = sync_log.emp_id) as last_sync_date,
    Coalesce((select description from reference where reference.ref_id = ticket.work_priority_id), 'Normal') priority,
    0 ticket_count,
    locate.locate_id
  from locate
    inner join ticket on ticket.ticket_id = locate.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join @emps emps on emps.emp_id = locate.assigned_to
  where
    ticket.due_date < @DueDate

  -- mark ticket count as 1 for every unique ticket in a set of locates
  update @tickets_due
    set ticket_count = 1
  where locate_id in (select locate_id from (
    select locate_id, ticket_number, 
      row_number() over (partition by ticket_number order by locate_id) rnum 
    from @tickets_due) s 
    where rnum = 1)
  
  -- tickets due data
  select
    due_date_only, short_name, ticket_number, ticket_id, due_date, ticket_type, work_type, client_code,
    client_name, locator_id, [status], work_remarks, closed, last_sync_date, priority, ticket_count
  from @tickets_due
  order by convert(varchar, due_date, 102), short_name, ticket_number

  -- Notes data
  select tn.ticket_id, tn.entry_date, tn.modified_date, tn.notes_id, tn.note
  from ticket_notes tn
  where tn.ticket_id in (select distinct tic.ticket_id from @tickets_due tic)
  order by tn.ticket_id, tn.entry_date, tn.notes_id
go

grant execute on RPT_TicketsDue_4 to uqweb, QManagerRole

/*
exec dbo.RPT_TicketsDue_4 3512, '2013-08-17', 1 
exec dbo.RPT_TicketsDue_4 1787, '2003-11-12', 1
exec dbo.RPT_TicketsDue_4 2676, '2007-11-12', 1 
*/

