if object_id('dbo.RPT_OnTimeCompletion5') is not null
  drop procedure dbo.RPT_OnTimeCompletion5
go

create procedure dbo.RPT_OnTimeCompletion5 (
  @StartDate datetime,
  @EndDate datetime,
  @CallCenterCodes varchar(200),
  @ClientIDs varchar(2000),
  @ManagerID int
)
as
  set nocount on

  declare @Results table (
    manager_short_name varchar(30),
    start_due_date datetime,
    end_due_date datetime,
    client_codes varchar(2000),
    ticket_type varchar(50),
    tickets_due int,

    -- Strange naming of result, for legacy compatibility:
    tickets_completed int,   -- Initially statused by end of day, bogus!
    completion_percent decimal(12, 4),

    tickets_initial int,     -- Initially statused by the due_date (and time) itself
    tickets_initial_percent decimal(12, 4),

    tickets_closed int,      -- Closed by the due_date (and time)
    tickets_closed_percent decimal(12, 4)
  )

  declare @Tickets table (
    ticket_id int not null,
    ticket_number varchar(25) not null,
    ticket_type varchar(50),
    due_date datetime not null
  )

  declare @TicketDates table (
    ticket_id_copy int not null primary key,
    initial_status_date datetime not null,
    closed_status_date datetime not null
  )

  declare @Locates table (
    ticket_id int not null,
    ticket_number varchar(25) not null,
    ticket_type varchar(50),
    due_date datetime not null,
    locate_id int not null,
    locate_id_copy int not null,
    client_code varchar(20) not null,
    initial_status_date datetime,     -- for this locate
    last_ticket_status_date datetime, -- for the ticket
    closed_status_date datetime,      -- for this locate
    last_ticket_closed_date datetime, -- for the ticket
    completed_on_time bit default 0,
    initial_on_time bit default 0,
    closed_on_time bit default 0,
    -- gathering these in antipation of making this report
    -- output enough data to print a hierarchy:
    report_to int,
    report_to_short_name varchar(30),
    locator_id int,
    locator_short_name varchar(30),
    ok bit default 0
  )

  declare @ticket_types table (
   ttype varchar(50) NOT NULL primary key
  )

  declare @CallCenterList table (
	center varchar(100) primary key
  )
  declare @ClientList table (
    oc_code varchar(25) primary key
  )

  -- FYI, if ClientList=* and CallCenterLIst='', this report return nothing
  if @ClientIDs <> '*'
    insert into @ClientList (oc_code)
    select client.oc_code
    from client
      inner join dbo.IdListToTable(@ClientIDs) c
       on c.ID = client.client_id
    group by client.oc_code

  if @CallCenterCodes=''
    -- infer it from the client list, to support old clients
    insert into @CallCenterList
    select distinct call_center
      from client
        inner join dbo.IdListToTable(@ClientIDs) as Clients on Clients.ID = client.client_id
  else
    insert into @CallCenterList
    select S from dbo.StringListToTable(@CallCenterCodes)

  if @ClientIDs='*'
    insert into @ClientList (oc_code)
    select client.oc_code
      from client
      inner join @CallCenterList cent on client.call_center = cent.center
      where client.active=1
      group by client.oc_code

  -- Clever code to get space-separated list of client codes
  declare @ClientCodes varchar(2000)
  set @ClientCodes = ''
  update @ClientList
    SET @ClientCodes = @ClientCodes +' '+ oc_code
  SET @ClientCodes = ltrim(@ClientCodes)

  -- The real data gathering starts here
  insert into @Tickets (ticket_id, ticket_number, ticket_type, due_date)
  select ticket_id, ticket_number, ticket_type, due_date
  from ticket
  where ticket_format in (select center from @CallCenterList)
    and due_date >= @StartDate
    and due_date < @EndDate

  insert into @Locates (
	ticket_id,
	ticket_number,
	ticket_type,
	due_date,
	locate_id,
	locate_id_copy,
	client_code)
  select
	t.ticket_id,
	t.ticket_number,
	t.ticket_type,
	t.due_date,
	l.locate_id,
	l.locate_id,
	l.client_code
  from @Tickets t
    inner join locate l with (index(locate_ticketid)) on t.ticket_id = l.ticket_id
  where l.client_code in (select oc_code from @ClientList)

  if @ManagerID = -1
    update @Locates set ok=1
  else
  -- find which are within selected manager
  update @Locates
    set ok=1,
    report_to = h_report_to,
    report_to_short_name = h_report_to_short_name,
    locator_id = h_emp_id,
    locator_short_name = h_short_name
   from @Locates l
    inner join assignment a on l.locate_id = a.locate_id and a.active=1
    inner join dbo.get_report_hier2(@ManagerId, 0, 99) em on em.h_emp_id=a.locator_id

  delete from @Locates
    where ok=0

  insert into @ticket_types
  select distinct ticket_type from @Locates

  update @Locates
  set initial_status_date = (
    select min(status_date)
    from locate_status ls
    where ls.locate_id = locate_id_copy
     and ls.status<>'-R'
  )

  update @Locates
  set closed_status_date = (
    select min(status_date)
    from locate_status ls
    where ls.locate_id = locate_id_copy
     and ls.status in (select status from statuslist where complete=1)
  )

  -- If there is no initial_status_date, put it far in the future
  update @Locates
   set initial_status_date = '2199-01-01'
   where initial_status_date is null

  -- If there is no closed_status_date, put it far in the future
  update @Locates
   set closed_status_date = '2199-01-01'
   where closed_status_date is null

  insert into @TicketDates
  select ticket_id,
   max(initial_status_date),
   max(closed_status_date)
  from @Locates
  group by ticket_id

  update @Locates set
   last_ticket_status_date = td.initial_status_date,
   last_ticket_closed_date = td.closed_status_date
  from @TicketDates td
   where td.ticket_id_copy = ticket_id

  update @Locates
  set completed_on_time = 1
  where last_ticket_status_date <= @EndDate

  update @Locates
  set initial_on_time = 1
  where last_ticket_status_date <= due_date

  update @Locates
  set closed_on_time = 1
  where last_ticket_closed_date <= due_date

  insert into @Results (
    manager_short_name,
    start_due_date,
    end_due_date,
    ticket_type,
    tickets_due,
    tickets_completed,
    tickets_initial,
    tickets_closed,
    client_codes)
  select
    (select short_name from employee where emp_id=@ManagerID) as manager_short_name,
    @StartDate,
    @EndDate,
    tt.ttype,
    (select count(distinct ticket_id) from @Locates where ticket_type=ttype),
    (select count(distinct ticket_id) from @Locates where completed_on_time = 1 and ticket_type=ttype),
    (select count(distinct ticket_id) from @Locates where initial_on_time = 1 and ticket_type=ttype),
    (select count(distinct ticket_id) from @Locates where closed_on_time = 1 and ticket_type=ttype),
    @ClientCodes
   from @ticket_types tt

  update @Results set
    completion_percent = convert(float, tickets_completed) * 100 / tickets_due,
    tickets_initial_percent = convert(float, tickets_initial) * 100 / tickets_due,
    tickets_closed_percent = convert(float, tickets_closed) * 100 / tickets_due
  where tickets_due > 0

  -- first return the totals
  select * from @Results
  order by ticket_type

  -- then the tickets/locates for a detail listing
  -- which appears in the export of this report
  select ticket_id, ticket_number, ticket_type, 
    due_date, locate_id, client_code,
    initial_status_date,
    last_ticket_status_date,
    completed_on_time as initially_statused_on_day,
    initial_on_time,
    closed_on_time
  from @Locates
  order by ticket_id

go

grant execute on RPT_OnTimeCompletion5 to uqweb, QManagerRole

/*
exec dbo.RPT_OnTimeCompletion5 '2009-04-27', '2009-04-28', 'FMW1,FMW2,FMW3,FMW4', '*', 1004 -- one day for FMW Manager
exec dbo.RPT_OnTimeCompletion5 '2009-04-26', '2009-05-03', 'FMW1,FMW2,FMW3,FMW4', '*', 1004 -- one week

exec dbo.RPT_OnTimeCompletion5 '2004-03-31', '2004-04-01', '', '305,1724,1293,928,929,1294,1041,306,1808', 211
exec dbo.RPT_OnTimeCompletion5 '2004-03-31', '2004-04-01', '', 
  '999,2066,2067,2072,2068,2069,2070,2071,2073,1069,2190,2191,2192,2193,2194,2195,2198,2196,2197,2074,2075,2076,2078,2079,2080,2104,2186,1043,2181,2106,2187,2062,2183,2057,2182,2098,2184,2099,2185,2108,2188,1076,1077,1946,1833,1828,1829,1834,1830,1831,1825,1817,1818,1826,1819,1820,1821,1822,1823,1824,1827,1848,1846,1837,1835,1832,1847,1816,1814,1844,1838,1839,1840,1841,1842,1843,1815,1836,2179,963,988,2093,2088,2089,2083,2090,2082,2084,2091,2085,2092,2086,2087,2094,2095,2096,2097,2077', 1560

-- Example where none of the locates are in the manager:
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '958,1481', 211

-- Example where all are in the manager:
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '958,1481', 4255

-- Examples where some are in the manager
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '958,1481', 1249
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '958,1481', 1248

-- Examples of backward compatibility
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '958,1481', -1
exec dbo.RPT_OnTimeCompletion5 '2003-07-28', '2003-07-29', '', '305,1724,1293,928,929,1294', -1

-- Examples of call center / * capability
exec dbo.RPT_OnTimeCompletion5 '2004-03-31', '2004-04-01', 'Atlanta', '*', 211
exec dbo.RPT_OnTimeCompletion5 '2004-03-31', '2004-04-01', 'FCV1,FCV2,FCV3,FCV4', '*', 340
*/
