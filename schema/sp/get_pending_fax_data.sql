if object_id('dbo.get_pending_fax_data_2') is not null
  drop procedure dbo.get_pending_fax_data_2
go

create procedure dbo.get_pending_fax_data_2
as
  set nocount on

  declare @ncf table (
  fax_name varchar (100) NULL ,
  ticket_number varchar (20) NULL ,
  client_code varchar (10) NULL ,
  call_center varchar (20) NULL ,
  con_name varchar (50) NULL ,
  caller varchar (50) NULL ,
  caller_fax varchar (40) NULL ,
  status varchar (5) NULL ,
  status_date datetime NULL ,
  insert_date datetime NULL ,
  work_state varchar (2) NULL ,
  work_county varchar (40) NULL ,
  work_city varchar (40) NULL ,
  work_address_number varchar (10) NULL ,
  work_address_number_2 varchar (10) NULL ,
  work_address_street varchar (60) NULL ,
  client_name varchar (40) NULL ,
  status_name varchar (30) NULL ,
  status_message varchar (3000) NULL,
  send_type varchar (15) NOT NULL,
  company_name varchar (20) NULL ,
  address1 varchar (30) NULL ,
  address2 varchar (30) NULL ,
  city varchar (20) NULL ,
  state char (2) NULL ,
  zipcode varchar (10) NULL ,
  phone varchar (12) NULL ,
  fax varchar (12) NULL ,
  street varchar (77) NULL ,
  ls_id int NOT NULL ,
  ticket_id int NOT NULL ,
  locate_id int NOT NULL ,
  fm_id int NOT NULL ,
  queue_date datetime NULL ,
  previous_queue_date datetime NULL
  )

  insert into @ncf
  select
    case
      when caller_fax IS NULL or caller_fax=''
       THEN con_name+caller
      ELSE caller_fax
    end as fax_name,
    ticket.ticket_number,
    locate.client_code,
    ticket.ticket_format as call_center,
    ticket.con_name,
    ticket.caller,
    ticket.caller_fax,
    ls.status,
    ls.status_date,
    ls.insert_date,
    ticket.work_state,
    ticket.work_county,
    ticket.work_city,
    ticket.work_address_number,
    ticket.work_address_number_2,
    ticket.work_address_street,
    client.client_name,
    statuslist.status_name,
    (select top 1 fqr.status_message
      from fax_queue_rules fqr
      where fqr.call_center = ticket.ticket_format
      and (fqr.client_code = locate.client_code or fqr.client_code = '*')
      and (fqr.state = ticket.work_state or fqr.state = '*')
      and (fqr.status = ls.status or fqr.status = '*')
    ) as status_message,
    (select top 1 fqr.send_type
      from fax_queue_rules fqr
      where fqr.call_center = ticket.ticket_format
      and (fqr.client_code = locate.client_code or fqr.client_code = '*')
      and (fqr.state = ticket.work_state or fqr.state = '*')
      and (fqr.status = ls.status or fqr.status = '*')
    ) as send_type,
    isnull(office.company_name, '') as company_name,
    isnull(office.address1, '') as address1,
    isnull(office.address2, '') as address2,
    isnull(office.city, '') as city,
    isnull(office.state, '') as state,
    isnull(office.zipcode, '') as zipcode,
    isnull(office.phone, '') as phone,
    isnull(office.fax, '') as fax,
    ltrim(isnull(ticket.work_address_number, '') + ' ' +
          isnull(ticket.work_address_number_2, '') + ' ' +
          isnull(ticket.work_address_street, '')) as street,
    ls.ls_id,
    ticket.ticket_id,
    ls.locate_id,
    fm.fm_id,
    fm.queue_date,
    fm.previous_queue_date
  from fax_message fm
    inner join fax_message_detail fd on fm.fm_id = fd.fm_id
    inner join locate_status ls on fd.ls_id = ls.ls_id
    inner join locate on fd.locate_id = locate.locate_id
    inner join ticket on fd.ticket_id = ticket.ticket_id
    inner join client on client.client_id = locate.client_id
    inner join statuslist on statuslist.status = ls.status
    left join office on office.office_id = client.office_id
  where fm.fax_status = 'pending'

  select * from @ncf
  order by fax_name, ticket_number, client_code

  select value as fax_local_area_codes
  from configuration_data
  where name = 'FaxLocalAreaCodes'

  select fm_id, fax_name, call_center
   from @ncf
   group by fm_id, fax_name, call_center
   order by fm_id, fax_name
GO
grant execute on get_pending_fax_data_2 to uqweb, QManagerRole
/*
exec dbo.get_pending_fax_data_2
*/


