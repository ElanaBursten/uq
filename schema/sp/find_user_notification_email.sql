if object_id('dbo.find_user_notification_email') is not null
	drop procedure dbo.find_user_notification_email
GO

CREATE PROCEDURE find_user_notification_email (
  @EmpId int,
  @EmailType varchar(30)
)
AS

declare @results table (
  emp_id int,
  email_type varchar(30),
  email_destination varchar(250) null
)

declare @count int

select @count = 0

while 1=1
begin
  select @count = @count + 1
  
  insert into @results (emp_id, email_type, email_destination)
  select emp_id, email_type, email_destination 
    from notification_email_user
    where notification_email_user.emp_id = @EmpId
      and notification_email_user.email_type = @EmailType
  
  -- if we found something, we're done
  if @@ROWCOUNT > 0
  begin
    select * from @results
    return
  end
  -- if not, we need to look up this employee's report_to field, and try again
  else begin
    select @EmpId = (select report_to from employee where emp_id = @EmpId)
    -- if there is no meaningful report_to, then quit
    if (@EmpId is null OR @count > 100) begin
      select * from @results  -- empty
      return
    end
  end
end
GO
grant execute on find_user_notification_email to uqweb, QManagerRole

/*
Try it out with:
exec dbo.find_user_notification_email 697, 'time_warning'
exec dbo.find_user_notification_email 2676, 'client_error'

*/
