if object_id('dbo.get_multi_reason_desc_list') is not null
  drop function dbo.get_multi_reason_desc_list
go

create function get_multi_reason_desc_list(@TicketID int, @ClientCode varchar(10), @HighProfileIDsFilter varchar(300), @ReturnOnlyFilteredHPCodes bit)
--the purpose of the @ReturnOnlyFilteredHPCodes is to allow us to perform a couple different tests needed by the calling sp (Rpt_HighProfile3.sql):
-- a) We need to know if a locate has any High Profile reasons that meet the filter. i.e. only those that meet filter are returned so calling sp will know if the locate should be included or not in result set
-- b) The calling sp will need to know all the High Profile reasons for the locate to display on the report regardless of the filter.
returns
  varchar(2000)
as
begin
  declare @Result varchar(2000)
  declare @HPRefCodesForClientTicket table (  
    code varchar(15) not null primary key
  )  
    
--High Profile Multi Reasons that are on the ticket, for the specified client. 
  DECLARE @MultiReasonCodesForClientTicket varchar(300)
  select @MultiReasonCodesForClientTicket = (select top 1 
    SUBSTRING(note,5, CHARINDEX(' ',note,1)-5) as CommaDelNote  -- minus 5 (1 for the space itself and minus 4 for 'HPR-')
  from ticket_notes tn 
  where tn.ticket_id = @TicketID
    and tn.note like 'HPR%'
    and SUBSTRING(note, CHARINDEX(' ',note,1)+1, LEN(note)) =@ClientCode
    order by entry_date desc)

--Populate temp table to hold the HP codes on the ticket that meet the filter    
  if ((@ReturnOnlyFilteredHPCodes = 1) and(@HighProfileIDsFilter <> '') and (@HighProfileIDsFilter is not null) ) -- return only the codes on the ticket that meet the filter
  begin
    insert into @HPRefCodesForClientTicket (code)
      select hpc.code from 
       (select cast(S as int) code from dbo.StringListToTable(@MultiReasonCodesForClientTicket)) hpc --codes from the note
        join (select code from reference where ref_id in (select cast(S as int) id from dbo.StringListToTable(@HighProfileIDsFilter)) and type = 'hpmulti') hpt 
          on hpc.code = hpt.code --codes corresponding to filter id's
  end
  else begin -- return all codes on the ticket
    insert into @HPRefCodesForClientTicket (code)
     (select cast(S as int) code from dbo.StringListToTable(@MultiReasonCodesForClientTicket))
  end   
  
 -- High Profile reasons for the codes 
  select @Result = COALESCE(@Result + ', ','') + description
  from reference ref
  where 
    ref.type = 'hpmulti'
    and ref.code in (select code from @HPRefCodesForClientTicket) 
 
  return @Result
end
go

grant execute on get_multi_reason_desc_list to uqweb, QManagerRole
go

/*
--always return all the codes 
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '582,827,828',1) 
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '582,827,828',0) 
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '827,828,829,830,831,832,833,834,835,836,837,838,1205',1)
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '827,828,829,830,831,832,833,834,835,836,837,838,1205',0)
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '',1)
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '',0)
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', null,1) 
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', null,0)
select dbo.get_multi_reason_desc_list(1001, 'CPLZ05', '384',1) --test an invalid code to make sure null is returned
select dbo.get_multi_reason_desc_list(1001, '', '827,1205',1) --test no client code to make sure null is returned
*/

