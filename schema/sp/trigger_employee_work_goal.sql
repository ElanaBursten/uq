if exists(select name FROM sysobjects where name = 'employee_work_goal_iu' AND type = 'TR')
  DROP TRIGGER employee_work_goal_iu
GO

create trigger employee_work_goal_iu on employee_work_goal
for insert, update
not for replication
AS
begin

  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update employee_work_goal
  set modified_date = getdate()
  from inserted I
  where employee_work_goal.work_goal_id = I.work_goal_id
end

GO