/*
  Gets the normal (non-payroll) PC for an employee ID on a given date.
  If they are not assigned a non-payroll PC, then return the payroll PC.
*/
if object_id('dbo.get_historical_pc') is not null
  drop function dbo.get_historical_pc
go

create function dbo.get_historical_pc(@EmpID int, @Date datetime)
returns varchar(15)
as
begin
  return (select coalesce(active_pc, payroll_pc_code) from employee_history
          where (emp_id = @EmpID)
            and (active_start <= @Date) and (active_end > @Date))
end

go
grant execute on get_historical_pc to uqweb, QManagerRole
/*
select dbo.get_historical_pc(811, getdate())
select dbo.get_historical_pc(6535, getdate())
select dbo.get_historical_pc(2676, getdate())
select dbo.get_historical_pc(6956, GetDate())
*/
