if object_id('dbo.RPT_invoice8') is not null
  drop procedure dbo.RPT_invoice8
GO

CREATE PROCEDURE RPT_invoice8 (
  @OutputConfigID int,
  @BillID int
)
AS

set nocount on

-- @Debug controls a select statements to provide additional debgging data
declare @Debug bit
set @Debug = 0

declare @StartDate datetime
declare @EndDate datetime
declare @WhichInvoice varchar(20)

-- Variables loaded from the billing output config record, so we only retrieve them once
declare @CustomerID int
declare @OmitZeroAmount bit
declare @GroupByPrice bit
declare @SalesTaxIncluded bit
declare @FlatFee money

declare @LineItemTotal money
declare @AdjustmentPercentTotal money
declare @AdjustmentDollarTotal money
declare @TaxTotal money
declare @PeriodToPost varchar(10)
declare @MaxBreakdown varchar(60)

declare @customer_company_id int
declare @customer_pc varchar(15)
declare @customer_active bit
declare @customer_num varchar(20)

-- This is used as part of the Solomon task id for data with no associated locator 
declare @no_locator_info varchar(12)
set @no_locator_info = Replicate('0', 12)

declare @termidlist table (
	client_id int not null,
	termid varchar(20) NOT NULL
)
declare @centerlist table (
	center varchar(20) NOT NULL
)


select @StartDate=bill_start_date, @EndDate=bill_end_date
  from billing_header
  where bill_id = @BillID

select
       @WhichInvoice = case when ((which_invoice in ('', ' ')) or (which_invoice is null)) then null else which_invoice end,
       @OmitZeroAmount = omit_zero_amount,
       @CustomerID = customer_id,
       @GroupByPrice = group_by_price,
       @SalesTaxIncluded = sales_tax_included,
       @FlatFee = flat_fee
from billing_output_config
where output_config_id=@OutputConfigID

select @customer_company_id = (
  select c.locating_company
  from customer c
  where c.customer_id = @CustomerID)

select @customer_pc = (
  select pc.timesheet_num
  from customer c
    inner join profit_center pc on c.pc_code = pc.pc_code
  where c.customer_id = @CustomerID)

select @customer_active = c.active
from customer c
where c.customer_id = @CustomerID

select @customer_num = (
  select 
    case when boc.customer_number is null or boc.customer_number='' then c.customer_number
    else boc.customer_number end
  from billing_output_config boc
    inner join customer c on boc.customer_id = c.customer_id
  where output_config_id=@OutputConfigID)

insert into @termidlist
select cli.client_id, cli.oc_code
  from client cli
where cli.customer_id = @CustomerID

insert into @centerlist
select distinct cli.call_center
  from client cli
where cli.customer_id = @CustomerID

declare @CustBillingDetail table (
  locate_id int not null,
  billing_cc varchar(10) not null,
  line_item_text varchar(60) null,
  rate money not null,
  price money not null,
  qty_marked integer null,
  qty_charged int null,
  hours DECIMAL(9,3) null,
  status varchar(5) not null,
  work_county varchar(40) null,
  work_state varchar(2) null,
  work_city varchar(40) null,
  locator_id int not null,
  tax_name varchar(40) null,
  tax_rate decimal(6,4) null,
  tax_amount money null,
  con_name varchar(50) null,
  bill_code varchar(10) null,
  ticket_type varchar(30) null
)

-- Pre-calculated billing_detail records for the specific customer and invoice name
insert into @CustBillingDetail
  (locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
   work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name, bill_code,
   ticket_type)
select
   locate_id, billing_cc, line_item_text, rate, price, qty_marked, qty_charged, hours, status,
   work_state, work_county, work_city, locator_id, tax_name, tax_rate, tax_amount, con_name, bill_code,
   ticket_type
from billing_detail where bill_id = @BillID
  and client_id in (select client_id from @termidlist)
  and (coalesce(@WhichInvoice, '') = coalesce(which_invoice, ''))


-- Detail dataset (invoice lines for detail section)

declare @lineitems table (
	liid int identity,
	subcategory varchar(50),
	billing_cc varchar(35),
	line_item_text varchar(60),
	rate money,
	price money,
	Tickets int,
	Units int,
	UnitsCharged int,
	Hours decimal(38, 3),
	Amount money,
	omit_zero_amount bit,
	pre_tax_amount money,
	tax_name varchar(40),
	tax_rate decimal(6,4),
	tax_amount money,
	source varchar(10) null,
	plat_group varchar(35) null
)

declare @emppc table (
	emp_id int NOT NULL primary key,
	emp_payroll_pc_code varchar(15),
	emp_company_id int NULL
)

-- Solomon line items to export into the accounting system
declare @soloitems table (
	amount money,
	company_id int,
	pc varchar(15),
	source varchar(40), -- The source type or GL source specified by an adjustment record (New source items will need a matching ChargeType* constant)
	is_gl_code bit not null, -- Only used to debug the validity of the source later
	task_code varchar(55)
)

insert into @emppc (emp_id, emp_payroll_pc_code, emp_company_id)
select emp_id, coalesce(pc.timesheet_num, '000'), emp_company_id
 from dbo.employee_payroll_data2() e
 left join profit_center pc on e.emp_payroll_pc_code = pc.pc_code

if (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
begin
  -- IF we are NOT in Included Sales Tax mode, this will insert items:
  if (@FlatFee is null or @FlatFee <= 0)
    insert into @lineitems 
      (subcategory, billing_cc, line_item_text, rate, price,
       Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems -- We are NOT in Included Sales Tax mode
  select
    coalesce(round(bd.price, 2),0) as Amount,
    ep.emp_company_id as company_id,
    ep.emp_payroll_pc_code as pc,
    dbo.get_billing_charge_code(bd.bill_code, bd.ticket_type) as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
   from @CustBillingDetail bd
     left join @emppc ep on bd.locator_id = ep.emp_id
end -- Sales tax not included

if (@SalesTaxIncluded = 1)
begin
  -- IF we ARE in Included Sales Tax mode, this will insert items:
  if (@FlatFee is null or @FlatFee <= 0)
    insert into @lineitems
      (subcategory, billing_cc, line_item_text, rate, price,
       Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount,
       pre_tax_amount, tax_name, tax_rate, tax_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount,
     pre_tax_amount, tax_name, tax_rate, tax_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems -- We ARE in Included Sales Tax mode
  select
    coalesce(round(price,2),0) - coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    dbo.get_billing_charge_code(bd.bill_code, bd.ticket_type) as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
   from @CustBillingDetail bd
   left join billing_scc_invoice scci
     on bd.work_state=scci.state and bd.work_county=scci.county
        and bd.work_city=scci.city and scci.active=1
        and (scci.customer_id=@CustomerID or scci.customer_id is null)
   left join @emppc ep on bd.locator_id = ep.emp_id

  insert into @soloitems
  select
    coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    'TAX' as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
  from @CustBillingDetail bd
   left join billing_scc_invoice scci
     on bd.work_state=scci.state and bd.work_county=scci.county
        and bd.work_city=scci.city and scci.active=1
        and (scci.customer_id=@CustomerID or scci.customer_id is null)
   left join @emppc ep on bd.locator_id = ep.emp_id
end -- Sales tax included

if @FlatFee > 0 
begin
  -- Some customers are set to be charged flat fees per billing cycle
  -- (See billing_output_config.flat_fee) rather than per locate/transmission
  insert into @lineitems 
    (subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
    select subcategory, billing_cc, line_item_text, rate, price,
     Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount
    from dbo.invoice_line_items(@OutputConfigID, @BillID)

  insert into @soloitems
  select
    @FlatFee as Amount,
    @customer_company_id as company_id,
    @customer_pc as pc,
    'FLAT_FEE' as source,
    0 as is_gl_code,
    @customer_pc + @no_locator_info as task_code
end


  -- CA-Style (Trans rates in the Rate table) TRANSMISSION line items
  insert into @lineitems
  (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units,
   UnitsCharged, Hours, Amount, omit_zero_amount, source, plat_group)
  select
    null as subcategory,
    btd.billing_cc, --a.k.a termcode or client code
    coalesce(max(btd.line_item_text), 'Transmissions') as line_item_text,
    btd.rate as rate,
    btd.rate as price,
    count(btd.ticket_id) as Tickets,
    count(btd.ticket_id) as Units,
    count(btd.ticket_id) as UnitsCharged,
    0 as Hours,
    sum(btd.rate) as Amount,
    0 as omit_zero_amount,
    'trans' as source,
  btd.plat_group
  from (select * from billing_transmission_detail
      where bill_id = @BillID and customer_id = @CustomerID
        and coalesce(which_invoice, '') = coalesce(@WhichInvoice, '')) btd
  group by
    btd.billing_cc,
    btd.plat_group,
    btd.rate

  insert into @soloitems
  select
    coalesce(btd.rate, 0) as Amount,
    @customer_company_id as company_id,
    coalesce(ep.emp_payroll_pc_code, @customer_pc) as pc,
    'TRANSMISSION' as source,
    0 as is_gl_code,
    coalesce(ep.emp_payroll_pc_code, '000') + @no_locator_info as task_code
  from (select * from billing_transmission_detail
      where bill_id = @BillID and customer_id = @CustomerID
        and coalesce(which_invoice, '') = coalesce(@WhichInvoice, '')) btd
    left join @emppc ep on btd.locator_id = ep.emp_id     

-- Delete 0-amount items, if configured to do so:
if @OmitZeroAmount = 1
begin
  delete from @lineitems
  where amount=0 or amount is null
end

-- Insert an empty line item, if there is nothing there, so the reports will work.
if not exists (select top 1 * from @lineitems)
insert into @lineitems
  (subcategory, billing_cc, line_item_text, rate, price, Tickets, Units, UnitsCharged, Hours, Amount, omit_zero_amount)
select
  null as subcategory,
  '' as billing_cc, '' as line_item_text,
  null as rate,
  null as price,
  null as Tickets, null as Units, null as UnitsCharged, null as Hours,
  0 as Amount,
  0 as omit_zero_amount


-- Output begins here --

-- Summary of each locate category/description by term
select * from @lineitems
order by 2,3,4,5,6,7,8

-- Get current invoice total for above data

select @LineItemTotal = sum(coalesce(amount,0))
from @lineitems

-- Adjustment Detail dataset
select description, type,
  case
    when type='AMOUNT' then sum(amount)
    when type='PERCENT' then sum(round(@LineItemTotal * (amount) / 100, 2))
    else 0 end as AdjustmentAmount -- COMMENT type adjustments with no amount
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')
 group by description, type

insert into @soloitems
select
  case
    when type='AMOUNT' then sum(amount)
    when type='PERCENT' then sum(round(@LineItemTotal * (amount) / 100, 2))
    else 0 end as AdjustmentAmount,
  @customer_company_id as company_id,
  @customer_pc as pc,
  ba.gl_code as source,
  1 as is_gl_code, -- A blank/null GL code means "charge to the otherwise largest $ amount GL code on this invoice"
  @customer_pc + @no_locator_info as task_code
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.active=1
   and ba.type <> 'COMMENT'
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')
 group by description, type, ba.gl_code

-- Get the total of all AMOUNT adjustments into the variable
-- Perhaps we could declare the detail data above as a cursor
-- and iterate over it, if needed later
select @AdjustmentDollarTotal = sum(amount)
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.type='AMOUNT'
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')

select @AdjustmentPercentTotal = sum(round(@LineItemTotal * (amount / 100), 2))
 from billing_adjustment ba
 where ba.customer_id = @CustomerID
   and ba.adjustment_date between @StartDate and @EndDate
   and ba.type='PERCENT'
   and ba.active=1
   and coalesce(@WhichInvoice, '') = coalesce(ba.which_invoice, '')

-- Sales Tax
-- Note that this only picks up traditional billing, not Trans or Flat.
select tax_name, tax_rate, sum(price) as amount, sum(tax_amount) as tax_amount
from @CustBillingDetail bd
where (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
group by tax_name, tax_rate
having sum(tax_amount)>0
order by tax_name, tax_rate

if (@SalesTaxIncluded is null or @SalesTaxIncluded = 0)
begin
  insert into @soloitems
  select
    coalesce(tax_amount,0) as Amount,
    emp_company_id as company_id,
    emp_payroll_pc_code as pc,
    'TAX' as source,
    0 as is_gl_code,
    Coalesce(emp_payroll_pc_code, '000') + dbo.payroll_task_code_for_locate(bd.locate_id)
  from @CustBillingDetail bd
   left join @emppc ep on bd.locator_id = ep.emp_id

  select @TaxTotal = coalesce(sum(tax_amount), 0)
   from @CustBillingDetail bd
end
else begin -- Sales tax is included in rates
  select @TaxTotal = 0
end

-- Get the Solomon Period To Post
select @PeriodToPost = min(dp.period) from dycom_period dp where @EndDate between dp.starting and dp.ending


-- Total Invoice Amount is the amount of all prices and adjustments

declare @total money
set @total = coalesce(@LineItemTotal,0) +
    coalesce(@AdjustmentPercentTotal,0) +
    coalesce(@AdjustmentDollarTotal, 0)  +
    coalesce(@TaxTotal, 0)

-- Invoice header/master dataset (Invoice basics, Customer settings, etc.)
select
  boc.output_config_id,
  boc.customer_id,
  boc.output_template,
  boc.contract,
  boc.payment_terms,
  boc.cust_po_num,
  boc.output_fields,
  boc.billing_period,
  boc.flat_fee,
  boc.comment,
  boc.center_group_id,
  boc.SolomonOffice,
  boc.SolomonPeriodToPost,
  boc.which_invoice,
  boc.sales_tax_rate,
  boc.group_by_price,
  boc.omit_zero_amount,
  case when boc.customer_number is null or boc.customer_number='' then c.customer_number
      else boc.customer_number end as customer_number,
  boc.transmission_billing,
  boc.sales_tax_included,
  0,
  bh.committed,
  bh.bill_id as billing_run_id,
  c.customer_id,
  c.customer_name,
  coalesce(c.customer_description, c.customer_name) as customer_description,
  c.city,
  c.state,
  c.zip,
  c.street,
  c.phone,
  c.locating_company,
  c.modified_date,
  c.active,
  c.street_2,
  c.attention,
  c.contact_name,
  c.contact_email,
  c.period_type,
  c.pc_code,
  lc.logo_filename, lc.billing_footer,
  case when attention is null then null
       else 'Attn: ' + attention end as attention_tagged,

  @LineItemTotal as total_line_items,
  @AdjustmentPercentTotal as total_adjustments_percent,
  @AdjustmentDollarTotal as total_adjustments_dollar,

  coalesce(@AdjustmentPercentTotal, 0) +
    coalesce(@AdjustmentDollarTotal, 0) as total_adjustments,

  coalesce(@LineItemTotal,0) +
    coalesce(@AdjustmentPercentTotal, 0) +
    coalesce(@AdjustmentDollarTotal, 0) as subtotal_with_adjustments,

  @TaxTotal as total_tax,

  -- Total Invoice Amount is the amount of all prices and adjustments
  @total as grand_total,

  pc.gl_code as ProjectIDHeader, pc.timesheet_num as SolOffice,
  @PeriodToPost as SolPeriodToPost,
  (select coalesce(sum(amount),0) from @soloitems) as sol_total,
  lc.name as locating_company_name,
  case
    -- mark header to ignore $0 invoices for inactive customers
    when (@total < 0.001 and @total > -0.001) and @customer_active=0 then 1
    else 0
  end as ignore_invoice,
  coalesce(pc.billing_contact, '') as billing_contact

from billing_output_config boc
  inner join customer c on boc.customer_id=c.customer_id
  inner join locating_company lc on c.locating_company=lc.company_id
  inner join center_group cg on boc.center_group_id=cg.center_group_id
  inner join billing_header bh on bh.bill_id = @BillID
  left outer join profit_center pc on c.pc_code=pc.pc_code
where boc.output_config_id=@OutputConfigID

-- Gray/Kiely contractor special case data for NewJersey (no longer used - just a placeholder for backward compatibility)
select '' as billing_cc, '' as con_name, 0 as Cnt, 0.0 as TotalPrice
where 0=1

-- Totals by Status.  Only used for a few customers.
select
  billing_cc, status,
  count(*) as Tickets,
  sum(qty_marked) as Units,
  sum(qty_charged) as UnitsCharged,
  sum(hours) as Hours,
  sum(price) as Amount
 from @CustBillingDetail bd
group by
  billing_cc, status

-- Percentage breakdowns.  Only used for a few customers.

if exists(select billing_breakdown_id from billing_breakdown where output_config_id = @OutputConfigID)
begin
  set @MaxBreakdown = (select max(explanation) 
    from billing_breakdown
    where output_config_id=@OutputConfigID)

  select br.explanation, br.ratio,
   case when br.explanation<@MaxBreakdown then round(br.ratio * @total, 2)
   else @total -
     (select sum(round(br.ratio * @total, 2))
       from billing_breakdown br
       where br.output_config_id=@OutputConfigID
        and br.explanation<@MaxBreakdown)
   end as amount,
   case when br.explanation<@MaxBreakdown then round(br.ratio * @FlatFee, 2)
   else @FlatFee - 
     (select sum(round(br.ratio * @FlatFee, 2))
       from billing_breakdown br
       where br.output_config_id=@OutputConfigID
        and br.explanation<@MaxBreakdown)
    end as flat_fee_amount
from billing_breakdown br --with (index(billing_breakdown_oc)) 
where br.output_config_id=@OutputConfigID
 order by br.explanation
end
else
begin -- No breakdowns defined
  select null as explanation, null as ratio, null as amount, null as flat_fee_amount where 0=1
end

-- Billing Detail datasets: Transmission

-- TODO: test and make sure an empty dataset here doesn't mess up the export; i.e. there were no transmitted tickets during that billing period
select -- CA Trans
 btd.billing_cc as termcode, --a.k.a. termcode
 ticket.ticket_id,
 ticket.ticket_format as call_center,
 ticket.ticket_number,
 ticket.ticket_type,
 ticket.con_name,
 ticket.work_city,
 btd.transmit_date,
 ticket.work_county,
 ticket.work_address_number,
 ticket.work_address_number_2,
 ticket.work_address_street,
 ticket.work_cross,
 ticket.map_page,
 btd.rate,
 btd.locate_closed,
 btd.plat,
 btd.revision
from (select * from billing_transmission_detail
      where bill_id = @BillID and customer_id = @CustomerID
        and coalesce(which_invoice, '') = coalesce(@WhichInvoice, '')) btd
  left join ticket with (index(PK_ticket_ticketid))
    on ticket.ticket_id = btd.ticket_id

-- Solomon Data

delete from @soloitems where amount = 0 or amount is null
update @soloitems set company_id=@customer_company_id, pc=@customer_pc, 
  task_code=@customer_pc + @no_locator_info
  where company_id <> @customer_company_id
    or pc is null

select 'Level4' as LevelMarker,
  @customer_num as Contract, 
  task_code as TaskID,    
  s.pc as SolomonOffice,
  sum(s.amount) as Amount,
  s.pc,
  s.source,
  s.is_gl_code as IsGLCode
 from @soloitems s
 group by s.pc, task_code, s.source, s.is_gl_code
 having sum(s.amount)>0 or @customer_active=1

-- For debugging only.  This is not directly used by the invoice export code:
if @Debug = 1
  select * from @soloitems

GO
grant execute on RPT_invoice8 to uqweb, QManagerRole

/*
-- AT&T California transmission billing by plat group
exec RPT_invoice8 3379, 5000 
exec RPT_invoice8 3608, 5000 -- ATT_Orange-Riverside
exec RPT_invoice8 3609, 5000 -- ATT_SanDiego
exec RPT_invoice8 3610, 5000 -- ATT_NorthCoast
exec RPT_invoice8 3611, 5000 -- ATT_NValley
exec RPT_invoice8 3612, 5000 -- ATT_SValley
exec RPT_invoice8 3613, 5000 -- ATT_Sacramento
exec RPT_invoice8 3614, 5000 -- ATT_SouthBay
exec RPT_invoice8 3615, 5000 -- ATT_NorthBay
exec RPT_invoice8 3652, 5000 -- ATT_LASouth
exec RPT_invoice8 3653, 5000 -- ATT_LANorth

exec RPT_invoice8 128, 5008
exec RPT_invoice8 124, 5008

-- There should be labelled examples of EVERY kind of billing here.
-- But there are only some/most of them, represented below:

-- These have tax seperately, and have amounts spread across 2 PCs and 2 Companies:
exec RPT_invoice8 128, 14137

-- These have tax included:
exec RPT_invoice8 170, 14689

-- These have tax included, and an adjustment:
exec RPT_invoice8 170, 14940

-- Flat Fee
-- FCL1 HALF periods have several.
exec RPT_invoice8 1241, 14947
exec RPT_invoice8 372, 14947

-- CA trans billing example (customer 801/Charter - Long Beach/Adelphia Cable/SFCCH003)
exec RPT_invoice8 778, 18560 -- April, 2006 Transmissions
exec RPT_invoice8 778, 14488
exec RPT_invoice8 778, 14513

-- Misc:
exec RPT_invoice8 16, 13702
exec RPT_invoice8 212, 14958
exec RPT_invoice8 151, 15098
exec RPT_invoice8 47, 15071
exec RPT_invoice8 175, 15179


exec RPT_invoice8 16, 13702
select * from billing_header where bill_id=14017
2005-03-20 00:00:00.000	2005-03-27 00:00:00.000

alter TABLE ticket_version add ticket_format varchar(20) NULL

select top 10 * from billing_output_config
select top 10 * from customer


select * from billing_output_config where output_config_id=170
select * from client where customer_id=1129


-- List flat fee customers/terms
select boc.customer_id, cust.period_type, client.client_name, client.call_center
 from billing_output_config boc
  inner join customer cust on cust.customer_id=boc.customer_id
  inner join client on client.customer_id = boc.customer_id
 where flat_fee>0
order by 4

select bill_type, count(*) from billing_rate group by bill_type
select * from billing_rate where billing_cc='SDG04'

select emp_id, emp_payroll_pc_code, emp_company_id
 from dbo.employee_payroll_data2()

-- Test transmission billing
select top 100 * from ticket
select top 100 * from ticket_version
select top 100 * from locate
select top 100 * from billing_rate where billing_cc = 'PTT13'

To test a transmission billing ticket:
Set the customer to be transmission_billing=1 in the admin
insert into ticket_version
  (ticket_id, ticket_revision, ticket_number, image, transmit_date, processed_date, ticket_format)
values
  (1000, '1', 'MAN-SCA2-1001', 'zzz SDG09 USAS zzz', getdate(), getdate(), 'SCA1')
update locate set added_by = 'PARSER' where ticket_id = 1000
exec RPT_invoice8 809, 5003

select * from billing_detail where bill_id in (5018, 5019)
select * from client where client_id in (1474, 2463, 3317)
select * from billing_output_config where customer_id in (1090, 1093, 1732)
exec RPT_invoice8 172, 5018
exec RPT_invoice8 175, 5018
exec RPT_invoice8 2869, 5019

select * from billing_breakdown

Possible future indexes that might help:
create index client_customer_id on client(customer_id)
create index billing_detail_bill_id_client_id on billing_detail(bill_id, client_id) -- This one would be huge

-- More transmission billing tests:
select * from client where oc_code IN ('NCU01', 'NEU01', 'BCU01', 'CMU01') 
select * from customer where customer_id = 1313
select * from billing_output_config where customer_id = 1313
select top 10 * from billing_header order by bill_run_date desc

exec dbo.RPT_invoice8 805, 31303

select tv.ticket_id, t.ticket_format, t.source, t.ticket_number, substring(tv.image, 1, 250) as fragment
from ticket_version tv inner join ticket t on tv.ticket_id = t.ticket_id
where ticket_version_id = 80294959

*/
