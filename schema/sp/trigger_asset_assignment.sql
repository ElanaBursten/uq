-- Log all asset_assignment changes to asset_assignment_history

if exists(select name FROM sysobjects where name = 'asset_assignment_iud' AND type = 'TR')
  DROP TRIGGER asset_assignment_iud
GO

CREATE TRIGGER asset_assignment_iud ON asset_assignment
FOR INSERT, UPDATE, DELETE
not for replication
AS
BEGIN
  set nocount on
  declare @NumInserted int
  declare @NumDeleted int
  declare @ChangeTime datetime
  set @NumInserted = (select count(*) from inserted)
  set @NumDeleted = (select count(*) from deleted)
  set @ChangeTime = getdate()

  -- Delete or update operation: Move old row to history
  if (@NumDeleted > 0)
  begin
    insert into asset_assignment_history
     (assignment_id, asset_id, emp_id, active, modified_date, archive_date)
    select
      assignment_id, asset_id, emp_id, active, modified_date, @ChangeTime
    from deleted
  end

  -- Insert/update operation: update modified_date for the changed rows
  if (@NumInserted > 0)
  begin
    update asset_assignment
    set modified_date = @ChangeTime
    where assignment_id in (select assignment_id from inserted)
  end
END
go
