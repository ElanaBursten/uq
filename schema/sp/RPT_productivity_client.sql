if object_id('dbo.RPT_productivity_client_2') is not null
  drop procedure dbo.RPT_productivity_client_2
go

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

create proc RPT_productivity_client_2 (
  @StartDate  datetime,
  @EndDate    datetime,
  @ManagerId int,
  @EmployeeStatus int
)
as
  set nocount on 

  declare @emps TABLE (
    dispkey int NOT NULL identity,
    emp_id int NOT NULL primary key,
    short_name varchar(35),
    h_report_level int,
    h_namepath varchar(450) NULL,
    indented_name varchar(100)
  )

  -- Collect the emps
  insert into @emps (emp_id, short_name, h_report_level, h_namepath)
  select h_emp_id, h_short_name, h_report_level, h_namepath
   from dbo.get_report_hier3(@ManagerID, 0, 99, @EmployeeStatus) 
  order by h_namepath, h_short_name

  update @emps
    set indented_name = 
      right('00000' + convert(varchar(10), dispkey), 5) +
      ': ' +
      replicate('   ', h_report_level) + short_name

  -- This result set is designed such that a nearly default crosstab
  -- will generate the desired results; hence the user friendly column names,
  -- space before Tickets to make it sort first, etc.
  select
    e.indented_name as "Manager Locator",
    e.emp_id as "emp_id",
    ' Tickets' as "Term",
    count(distinct l.ticket_id) as "Received"
  from @emps e
   left join locate_status ls on ls.statused_by = e.emp_id
     and ls.status_date >= @StartDate
     and ls.status_date < @EndDate
   left join locate l on ls.locate_id=l.locate_id
  group by e.indented_name, e.emp_id

  union

  select
    e.indented_name as "Manager Locator",
    e.emp_id as "emp_id",
    client_code as "Term",
    count(l.ticket_id) as "Received"
  from @emps e
   inner join locate_status ls on ls.statused_by = e.emp_id
     and ls.status_date >= @StartDate
     and ls.status_date < @EndDate
   inner join locate l on ls.locate_id=l.locate_id
  group by e.indented_name, e.emp_id, l.client_code
  order by 1, 3

  select
    e.indented_name as "Manager Locator",
    e.emp_id,
    l.client_code as "Term",
    t.ticket_id,
    t.ticket_number,
    t.ticket_format,
    ls.status_date,
    ls.status
  from @emps e
  inner join locate_status ls on ls.statused_by = e.emp_id
    and ls.status_date >= @StartDate and ls.status_date < @EndDate
  inner join locate l on l.locate_id = ls.locate_id
  inner join ticket t on t.ticket_id = l.ticket_id
  order by 1, 3

go

grant execute on RPT_productivity_client_2 to uqweb, QManagerRole

/*
exec dbo.RPT_productivity_client_2 '2004-09-05', '2004-09-12', 1037, 1
exec dbo.RPT_productivity_client_2 '2004-07-23', '2004-07-29', 202, 1
*/

