if object_id('dbo.RPT_emailed_HP_tickets') is not null
  drop procedure dbo.RPT_emailed_HP_tickets
GO

create procedure RPT_emailed_HP_tickets (
	@call_center varchar(20)
) as

select tl.*,
 l.status, l.closed, l.assigned_to,
 e.short_name, e.emp_number,
 t.due_date, t.ticket_number, t.ticket_format
from ticket_grid_log tl
 inner join ticket t WITH (INDEX(PK_ticket_ticketid)) on tl.ticket_id = t.ticket_id
 inner join locate l WITH (INDEX(locate_ticketid)) on l.ticket_id = t.ticket_id
left outer join employee e on e.emp_id = l.assigned_to
where l.client_code = tl.client_code
 and tl.time_sent > getdate() - 60
 and t.ticket_format = @call_center
 and l.closed=0
order by tl.time_sent, t.ticket_id, l.client_code

go
grant execute on RPT_emailed_HP_tickets to uqweb, QManagerRole
/*
exec dbo.RPT_emailed_HP_tickets 'FCL1'
*/
