if exists(select name FROM sysobjects where name = 'locate_hours_u' AND type = 'TR')
  DROP TRIGGER locate_hours_u
GO

CREATE TRIGGER locate_hours_u ON locate_hours
FOR UPDATE
not for replication
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON

  -- See what columns were in the update statement
  declare @hours_invoiced_col int
  declare @units_invoiced_col int

  set @hours_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'hours_invoiced')
  set @units_invoiced_col = (select colid from syscolumns where id = OBJECT_ID('locate_hours') and name = 'units_invoiced')

  if (@hours_invoiced_col < 9) or (@hours_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('hours_invoiced has moved out of the second column byte', 18, 1) with log
  end

  if (@units_invoiced_col < 9) or (@units_invoiced_col > 16)
  begin
    rollback transaction
    RAISERROR('units_invoiced has moved out of the second column byte', 18, 1) with log
  end

  -- Exit if only one of the invoiced columns was in the update statement (see commit_billing)
  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@hours_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  if (substring(COLUMNS_UPDATED(), 1 ,1) = 0) and
     (substring(COLUMNS_UPDATED(), 2, 1) = power(2, ((@units_invoiced_col - 8)-1))) and
     (substring(COLUMNS_UPDATED(), 3, 1) = 0) and
     (substring(COLUMNS_UPDATED(), 4, 1) = 0)
    return

  -- Update the modified_date when a locate_hours row changes
  update locate_hours set modified_date = getdate()
  where locate_hours_id in (select locate_hours_id from inserted)
END
go

/*
Test trigger:
select modified_date from locate_hours where locate_hours_id = 25000
update locate_hours set hours_invoiced = 1 where locate_hours_id = 25000
select modified_date from locate_hours where locate_hours_id = 25000
update locate_hours set units_invoiced = 1 where locate_hours_id = 25000
select modified_date from locate_hours where locate_hours_id = 25000
update locate_hours set invoiced = 1 where locate_hours_id = 25000
select modified_date from locate_hours where locate_hours_id = 25000
update locate_hours set invoiced = 1, units_invoiced=1 where locate_hours_id = 25000
select modified_date from locate_hours where locate_hours_id = 25000
*/
