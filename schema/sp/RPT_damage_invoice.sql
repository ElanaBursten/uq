if object_id('dbo.RPT_damage_invoice3') is not null
  drop procedure dbo.RPT_damage_invoice3
go

create procedure dbo.RPT_damage_invoice3 (
  @ReceivedFromDate datetime,
  @ReceivedToDate   datetime,
  @PaidFromDate     datetime = null,
  @PaidToDate       datetime = null,
  @ProfitCenter     varchar(15) = '',
  @Company          varchar(40) = '',
  @InvoiceStatus    int = 0
)
as
  set nocount on

  declare @Results table (
    damage_idz int NOT NULL,
    uq_damage_id int,
    profit_center varchar(15),
    invoice_num varchar(30),
    amount decimal(12, 2),
    company varchar(40),
    received_date datetime,
    damage_date datetime,
    invoice_code varchar(10),
    paid_date datetime,
    paid_amount decimal(12, 2),
    resp_code varchar(10),
    resp_code_desc varchar(100) default '',
    first_estimate decimal(12, 2) default 0,
    current_estimate decimal(12, 2) default 0
  )

  -- Get the selected damage invoice & damage data
  insert into @Results (
    damage_idz,
    uq_damage_id,
    profit_center,
    invoice_num,
    amount,
    company,
    received_date,
    damage_date,
    invoice_code,
    paid_date,
    paid_amount,
    resp_code
    )
    select d.damage_id, d.uq_damage_id, d.profit_center, di.invoice_num, 
      di.amount, di.company, di.received_date, d.damage_date, d.invoice_code, 
      di.paid_date, coalesce(di.paid_amount, 0), 
      coalesce(d.exc_resp_code, d.uq_resp_code, d.spc_resp_code, '-')
      from damage_invoice di
      inner join damage d on d.damage_id = di.damage_id
      where di.received_date >= @ReceivedFromDate
      and di.received_date <= @ReceivedToDate
      and ((@ProfitCenter='') or (d.profit_center = @ProfitCenter))
      and ((@Company='') or (di.company like @Company))
      and ((@PaidFromDate is null) or (di.paid_date >= @PaidFromDate))
      and ((@PaidToDate is null) or (di.paid_date <= @PaidToDate))
      and ((@InvoiceStatus=0) 
      or (@InvoiceStatus=1 and d.invoice_code <> 'DENIED')
      or (@InvoiceStatus=2 and d.invoice_code = 'DENIED'))

  -- Get the first estimate amount
  update @Results
    set first_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = damage_idz
      and modified_date = (select min(modified_date) from damage_estimate where damage_id = damage_idz))

  -- Get the current estimate amount
  update @Results
    set current_estimate = (
    select coalesce(max(amount), 0) from damage_estimate
    where damage_id = damage_idz
      and modified_date = (select max(modified_date) from damage_estimate where damage_id = damage_idz))

  -- Update the Denied invoices' reason descriptions
  update @Results 
    set resp_code_desc = (select description from reference
      where type in ('exres', 'uqres', 'utres') and code = resp_code)
    where invoice_code = 'DENIED'
  
  -- Return the results
  select * from @Results order by profit_center, company, received_date

go

grant execute on RPT_damage_invoice3 to uqweb, QManagerRole

/*
exec dbo.RPT_damage_invoice3 '2005-05-01', '2005-06-01'
exec dbo.RPT_damage_invoice3 '2005-05-01', '9999-12-31' -- no end date
exec dbo.RPT_damage_invoice3 '2005-05-01', '2005-06-01', '1899-12-30', '2005-06-01', '910', 'SPRINT'
*/
