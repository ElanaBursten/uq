if object_id('dbo.RPT_tse_payex_std') is not null
	drop procedure dbo.RPT_tse_payex_std
go

create proc RPT_tse_payex_std (
  @ManagerId int,
  @EndDate datetime
)
as
  set nocount on
  declare @StartDate datetime
  set @StartDate = dateadd(d, -6, @EndDate)

  declare @detail table (
	category varchar(80) NULL,
	emp_number varchar(20) NULL,
	short_name varchar(30) NULL,
	last_name varchar(30) NULL,
	first_name varchar(30) NULL,
	emp_id int NULL,
	h_active bit,
	h_report_level int NULL,
	h_numpath varchar(450) NULL,
	h_eff_payroll_pc varchar(15),
	reg_hours decimal(38, 2) NULL,
	ot_hours decimal(38, 2) NULL,
	dt_hours decimal(38, 2) NULL,
	callout_hours decimal(38, 2) NULL,
	vac_hours decimal(38, 2) NULL,
	leave_hours decimal(38, 2) NULL,
 	pto_hours decimal(38, 2) NULL,
	br_hours decimal(38, 2) NULL,
	hol_hours decimal(38, 2) NULL,
	jury_hours decimal(38, 2) NULL,
	working_hours decimal(38, 2) NULL,
	total_hours decimal(38, 2) NULL,
	pov_hours decimal(38, 2) NULL,
	cov_hours decimal(38, 2) NULL,
	report_end_date datetime NULL,
  rule_abbrev varchar(20) NULL,
  timerule_id int NULL,
  company_id int NULL
  )

  insert into @detail
  select
   NULL,
   min(e.h_emp_number),
   min(e.h_short_name),
   min(e.h_last_name),
   min(e.h_first_name),
   e.h_emp_id,
   e.h_active,
   min(e.h_report_level),
   min(e.h_numpath) as h_numpath,
   min(e.h_eff_payroll_pc) as h_eff_payroll_pc,
   sum(reg_hours) as reg_hours,
   sum(ot_hours) as ot_hours,
   sum(dt_hours) as dt_hours,
   sum(callout_hours) as callout_hours,
   sum(vac_hours) as vac_hours,
   sum(leave_hours) as leave_hours,
   sum(pto_hours) as pto_hours,
   sum(br_hours) as br_hours,
   sum(hol_hours) as hol_hours,
   sum(jury_hours) as jury_hours,

   sum(reg_hours+ot_hours+dt_hours+callout_hours) as working_hours,
   sum(reg_hours+ot_hours+dt_hours+callout_hours+pto_hours+
      vac_hours+leave_hours+br_hours+hol_hours+jury_hours) as total_hours,

   sum( case when vehicle_use='POV'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as pov_hours,
   sum( case when vehicle_use like 'COV%'
             then reg_hours+ot_hours+dt_hours+callout_hours
             else null end) as cov_hours,

   @EndDate as report_end_date,
   max(reference.modifier) as rule_abbrev,
   max(case when reference.code is null then null else work_emp.timerule_id end) as timerule_id,
   max(work_emp.company_id) as company_id

  from (select * from dbo.get_report_hier2(@ManagerID, 0, 50)) e
   left join timesheet_entry tse on tse.work_emp_id = e.h_emp_id
      and tse.work_date between @StartDate and @EndDate
      and (tse.status='ACTIVE' or tse.status='SUBMIT')
   left join employee work_emp on e.h_emp_id = work_emp.emp_id
   left join reference on reference.ref_id = work_emp.timerule_id
  group by e.h_emp_id, e.h_active
  order by min(e.h_numpath)

  update @detail set category = '1) Salaried Employees with Reported Time < 40 hours'
   where rule_abbrev = '(S)'
   and total_hours<40

  update @detail set category = '2) Hourly Employees with Reported Time > 65 hours'
   where (rule_abbrev <> '(S)' or rule_abbrev is null)
   and total_hours > 65

  update @detail set category = '3) Active Employees with Reported Time = 0 hours'
   where h_active=1 and total_hours=0

  update @detail set category = '4) Inactive Employees with Reported Time > 0 hours'
   where h_active=0 and total_hours>0

  update @detail set category = '5) Employees with Reported Time and No Payroll Profit Center'
    where total_hours > 0 and Coalesce(h_eff_payroll_pc, '') = ''

  update @detail set category = '6) Employees with Reported Time and No Company'
    where total_hours > 0 and company_id is null

  update @detail set category = '7) Employees with Reported Time and No Time Rule'
    where total_hours > 0 and timerule_id is null

  update @detail set category = '8) Employees with Reported Time and wrong Company for Profit Center'
    where total_hours > 0 and company_id <> (select company_id from profit_center where pc_code = h_eff_payroll_pc)

  delete from @detail where category is null

  select * from @detail order by category, h_eff_payroll_pc, emp_number

go
grant execute on RPT_tse_payex_std to uqweb, QManagerRole

/*

exec dbo.RPT_tse_payex_std 2777, '2004-04-10'
exec dbo.RPT_tse_payex_std 5405, '2004-04-03'
exec dbo.RPT_tse_payex_std 5405, '2004-04-03'
exec dbo.RPT_tse_payex_std 211, '2004-02-01'
exec dbo.RPT_tse_payex_std 212, '2004-02-01'

*/
