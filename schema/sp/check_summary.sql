--This SP is no longer used as of 11/10/2011
--The proper version of check_summary is now determined by the audit report

IF OBJECT_ID('dbo.check_summary') IS NOT NULL
	drop procedure dbo.check_summary
GO


CREATE Procedure check_summary (
  @SummaryDate datetime,
  @CallCenter varchar(30)	
)
AS
-- new version of check_summary; reads audit method from the call_center table
-- if no value found, 'locate' is assumed

declare @ProcName varchar(50)

select @ProcName = audit_method from call_center
where cc_code = @CallCenter

if (@ProcName is null) or (@ProcName = '')
  set @ProcName = 'locate'

declare @CmdLine varchar(200)
set @CmdLine = 'exec check_summary_by_' + @ProcName + ' ''' 
  + CAST(@SummaryDate as varchar(20)) + ''', ''' + @CallCenter + ''''
exec(@CmdLine)
GO

grant execute on check_summary to uqweb, QManagerRole

/*
exec dbo.check_summary '2003-02-13', 'FMS1'
exec dbo.check_summary '2005-03-22 00:00:00', 'Atlanta'
exec dbo.check_summary '2005-06-13', 'FMW2'

select cc_code, audit_method from call_center
-- FMW2 uses ticket_serial

*/
