if object_id('dbo.get_ticket_workload_date') is not null
  drop function dbo.get_ticket_workload_date
go

create function get_ticket_workload_date(@TicketID int, @LocatorID int)
returns
  date
as
begin
  declare @Result date
  -- default to the ticket's due date  
  set @Result = (select due_date from ticket where ticket_id = @TicketID)

  set @Result = (select coalesce(min(a.workload_date), @Result)
    from assignment a
    inner join locate l on a.locate_id = l.locate_id
    where l.assigned_to_id = @LocatorID
      and l.ticket_id = @TicketID
      and a.active = 1)
 
  return @Result
end
go

grant execute on get_ticket_workload_date to uqweb, QManagerRole
go

/*
select dbo.get_ticket_workload_date(1000, 3512)
select dbo.get_ticket_workload_date(1001, 3512)
select dbo.get_ticket_workload_date(1002, 3512)
select dbo.get_ticket_workload_date(1018, 3512)
*/
