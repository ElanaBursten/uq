if object_id('dbo.RPT_HighProfileExport') is not null
	drop procedure dbo.RPT_HighProfileExport
GO

CREATE proc RPT_HighProfileExport (
	@DateFrom datetime, 
	@DateTo datetime, 
	@ClientCodes varchar(200)
)
as
  set nocount on
  declare @highprofile table (ticket_id int not null primary key)
  declare @reasons table (ticket_id int not null, entry_date datetime, note varchar(100))

  insert into @highprofile (ticket_id) 
    select distinct locate.ticket_id
    from locate with (INDEX(locate_closed_date))
    where locate.closed_date between @DateFrom and @DateTo
     and locate.high_profile = 1
     and locate.client_code in (select S from dbo.StringListToTable(@ClientCodes))

  insert into @reasons
  select notes.foreign_id, notes.entry_date,
    substring(notes.note, 5, charindex(' ', notes.note+' ')-5) as reasons
  from @highprofile hp
   inner join notes on hp.ticket_id = notes.foreign_id
  where notes.foreign_type = 1
    and notes.active = 1
    and notes.note like 'HPR-%'
  order by notes.foreign_id, notes.entry_date desc

  select
    ticket.ticket_number,
    (case when locate.client_code like 'K_G' then substring(locate.client_code, 1, 2)
          else locate.client_code end) as client_code,
    (select top 1 note from @reasons r where r.ticket_id=highprofile.ticket_id) as hp_reasons,
    convert(varchar(8), locate.closed_date, 112) as closed_date,
    coalesce(ticket.work_address_number, '') + 
    coalesce(ticket.work_address_number_2, '') + 
    coalesce(ticket.work_address_street, '') as work_address,
    ticket.con_name,
    convert(varchar(8), ticket.call_date, 112) as call_date,
    ticket.caller,
    ticket.caller_phone,
    ticket.work_city,
    ticket.work_type,
    ticket.caller_fax
  from @highprofile as highprofile
    inner join ticket
      on ticket.ticket_id = highprofile.ticket_id
    inner join locate
      on locate.ticket_id = highprofile.ticket_id
        and locate.client_code in (select S from dbo.StringListToTable(@ClientCodes))
  order by
    ticket.ticket_id, locate.locate_id

  -- tickets to print:
  select
    ticket.ticket_id,
    ticket.ticket_number,
    ticket.transmit_date,
    ticket.due_date,
    ticket.image
  from @highprofile as highprofile
    inner join ticket
      on ticket.ticket_id = highprofile.ticket_id
  order by
    ticket.ticket_number, ticket.ticket_id

  -- locates for these tickets:
  select
    locate.ticket_id,
    locate.client_code,
    locate.status,
    statuslist.status_name,
    employee.short_name,
    locate.high_profile,
    locate.qty_marked,
    locate.closed,
    locate.closed_date
  from @highprofile as highprofile
    inner join ticket
      on ticket.ticket_id = highprofile.ticket_id
    left join locate on locate.ticket_id = highprofile.ticket_id
      and locate.status<>'-N'
      and locate.high_profile = 1
    left join statuslist on statuslist.status = locate.status
    left join assignment on assignment.locate_id = locate.locate_id
        and assignment.active = 1
    left join employee
      on employee.emp_id = assignment.locator_id
   order by highprofile.ticket_id, locate.client_code

  -- Notes for these tickets & locates.
  select tn.entry_date, tn.modified_date, tn.note, tn.ticket_id
  from ticket_notes tn 
  inner join @highprofile hp on tn.ticket_id = hp.ticket_id
  order by hp.ticket_id

GO
grant execute on RPT_HighProfileExport to uqweb, QManagerRole
/*
exec dbo.RPT_HighProfileExport '2004-12-13' , '2004-12-14', 'KA,KAD,KB,KC,KD,KE,KF,KG,KCG,KDG,KEG,KFG,KGG'
exec dbo.RPT_HighProfileExport '2004-12-01' , '2004-12-02', 'B03,B11,DOM100'
exec dbo.RPT_HighProfileExport '2001-01-01' , '2008-01-01', 'MTEMFR'

Report=HighProfileExport
TicketSaveDir=c:\temp\tix
DateFrom=2004-12-01
DateTo=2004-12-02
ClientCodes=B03,B11,DOM100

select * from notes where foreign_type=1 and foreign_id in (
select ticket_id from ticket where ticket_number='043240800')
select * from client where call_center='7501'

select substring('HPR-1,3 THIS ', 5, charindex(' ', 'HPR-1,3 THIS ')-5)
select substring('HPR-1,3 ', 5, charindex(' ', 'HPR-1,3 ')-5)

ReportEngineCGI.exe HighProfileExport /ssv -o "\\utiliweb\reports\PECO-Export\Tue\20080212.txt" "DateTo=2008-02-13" "DateFrom=2008-02-12" "TicketSaveDir=\\utiliweb\reports\PECO-Export\Tue" "ClientCodes=KA,KB,KC,KD,KE,KF,KG,KAD,KCG,KDG,KEG,KFG,KGG"
*/
