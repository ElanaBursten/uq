if exists(select name FROM sysobjects where name = 'area_u' AND type = 'TR')
  DROP TRIGGER area_u
GO

create trigger area_u on area
for update
not for replication
AS
begin
  update area set modified_date = getdate()
   where area_id in (select area_id from inserted)
end

GO

