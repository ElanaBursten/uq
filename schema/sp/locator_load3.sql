if object_id('dbo.locator_load3') is not null
	drop procedure dbo.locator_load3
GO

CREATE Procedure locator_load3 (
	@ManagerID int,
	@Today datetime
	)
as

set nocount on

declare @results table (
	man_emp_id int NOT NULL,
	man_short_name varchar(30),
	emp_id int NOT NULL,
	short_name varchar(30),
	emp_number varchar(30),
	TotalLoadL int default 0,
	TotalLoadT int default 0,
	NewL int default 0,
	NewT int default 0,
	OngoingL int default 0,
	OngoingT int default 0,
	EmergencyL int default 0,
	EmergencyT int default 0,
	CarryoverL int default 0,
	CarryoverT int default 0,
	OverdueL int default 0,
	OverdueT int default 0
)

declare @work table (
	emp_id int NOT NULL,
	status varchar(20) NOT NULL,
	ticket_id int NOT NULL,
	Bucket varchar(30) NOT NULL
)

declare @totals table (
	t_emp_id int NOT NULL,
	Bucket varchar(30) NOT NULL,
	NL int,
	NT int
)

insert into @results (man_emp_id, man_short_name, emp_id, short_name, emp_number)
select h_report_to, h_report_to_short_name, h_emp_id, h_short_name, h_emp_number
 from dbo.get_report_hier(@ManagerID, 0)
 order by h_report_to_short_name, h_short_name

insert into @work
select r.emp_id, locate.status, locate.ticket_id,
  case
   when locate.status='O' then 'Ongoing'
   when ticket.due_date < @Today then 'Overdue'
   when ticket.kind='EMERGENCY' then 'Emergency'
   when ticket.transmit_date > @Today then 'New'
   else 'Carryover'
  end as Bucket
 from @results r
  inner join locate with (INDEX(locate_assigned_to)) on locate.assigned_to=r.emp_id
  inner join ticket on locate.ticket_id = ticket.ticket_id

insert into @totals
select emp_id, Bucket, count(*) as NL, count(distinct ticket_id) as NT
 from @work group by emp_id, bucket

update @results set NewL = NL, NewT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='New'

update @results set OngoingL = NL, OngoingT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Ongoing'

update @results set EmergencyL = NL, EmergencyT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Emergency'

update @results set CarryoverL = NL, CarryoverT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Carryover'

update @results set OverdueL = NL, OverdueT = NT
 from @totals where emp_id=t_emp_id
  and Bucket='Overdue'

update @results
  set TotalLoadL = NewL + OngoingL + EmergencyL + CarryoverL + OverdueL,
      TotalLoadT = NewT + OngoingT + EmergencyT + CarryoverT + OverdueT

select * from @results

go
grant execute on locator_load3 to uqweb, QManagerRole

/*
exec dbo.locator_load3 1004, '2002-11-20'
exec dbo.locator_load3 2410, '2003-02-12 07:42:00'
exec dbo.locator_load3 2410, '2003-09-24 07:42:00'
*/
