if object_id('dbo.RPT_DamageDefaultEstimate') is not null
  drop procedure dbo.RPT_DamageDefaultEstimate
GO

CREATE Procedure RPT_DamageDefaultEstimate (
  @FacilityType varchar(10),
  @UtilityCompany varchar(100),
  @ProfitCenter varchar(15)
  )
as

set nocount on

declare @Results table (
  profit_center varchar(15) NOT NULL,
  utility_co_id int NULL,
  utility_co_name varchar(100) NOT NULL,
  facility_type varchar(10) NOT NULL,
  facility_size varchar(10) NOT NULL,
  facility_material varchar(10) NOT NULL,
  estimate_amount money NOT NULL,
  modified_date datetime NOT NULL
)

if (@FacilityType = '')
  insert into @Results
    select case when D.profit_center = '*' then 'ALL' ELSE D.profit_center end,
    D.utility_co_id,
    coalesce(Ref.Description, 'ALL'),
    case when D.facility_type= '*' then 'ALL' ELSE D.facility_type end,
    case when D.facility_size='*' then 'ALL' ELSE D.facility_size end,
    case when D.facility_material='*' then 'ALL' ELSE D.facility_material end,
    D.default_amount,
    D.modified_date
    from damage_default_est D left outer join reference Ref on Ref.ref_id = D.utility_co_id
else
  insert into @Results
    select case when D.profit_center = '*' then 'ALL' ELSE D.profit_center end,
    D.utility_co_id,
    coalesce(Ref.Description, 'ALL'),
    case when D.facility_type= '*' then 'ALL' ELSE D.facility_type end,
    case when D.facility_size='*' then 'ALL' ELSE D.facility_size end,
    case when D.facility_material='*' then 'ALL' ELSE D.facility_material end,
    D.default_amount,
    D.modified_date
    from damage_default_est D left outer join reference Ref on Ref.ref_id = D.utility_co_id
    where D.facility_type = @FacilityType

if (@UtilityCompany <> '')
  delete from @Results where utility_co_name <> @UtilityCompany

if (@ProfitCenter <> '')
  delete from @Results where profit_center <> @ProfitCenter

select profit_center,
  utility_co_name,
  facility_type,
  facility_size,
  facility_material,
  estimate_amount,
  modified_date
  from @Results r
 order by r.profit_center,
   r.utility_co_name,
  r.facility_type,
  r.facility_size,
  r.facility_material

go
grant execute on RPT_damagedefaultestimate to uqweb, QManagerRole

/*
exec dbo.RPT_DamageDefaultEstimate '', '', ''
exec dbo.RPT_DamageDefaultEstimate 'PHONE', '', ''
exec dbo.RPT_DamageDefaultEstimate '', 'BELLSOUTH', ''
exec dbo.RPT_DamageDefaultEstimate '', '', 'FCV'
exec dbo.RPT_DamageDefaultEstimate 'GAS', '', 'FCV'
*/
