USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[update_status_group]    Script Date: 9/5/2016 12:03:41 PM ******/
/****** QMANTWO-226 -- changes @Statuses varchar(400) to @Statuses varchar(800) ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

If object_id('dbo.update_status_group') is not null drop procedure dbo.update_status_group
GO

Create proc [dbo].[update_status_group] (
	@SGID int,
	@Statuses varchar(800)
)
as
  set nocount on

  -- add any that aren't in there at all
  insert into status_group_item (sg_id, status)
  select @SGID as sg_id, S as status
   from StringListToTable(@Statuses) data
   where S not in (select status 
     from status_group_item 
       where sg_id=@SGID)

  -- make active any that are inactive
  update status_group_item set active=1
  where sg_id=@SGID
   and active=0
   and status in (select S from StringListToTable(@Statuses))

  -- make inactive any that are active
  update status_group_item set active=0
  where sg_id=@SGID
   and active=1
   and status not in (select S from StringListToTable(@Statuses))


select statuslist.status + ' - ' + status_name +
 case when statuslist.active=0 then ' (inactive)' else '' end as stat,
 sgi.sg_item_id, sgi.active
from statuslist
 left join status_group_item sgi
   on statuslist.status = sgi.status and sgi.sg_id=@SGID

