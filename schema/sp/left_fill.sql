if object_id('dbo.left_fill') is not null
  drop function dbo.left_fill
go

create function dbo.left_fill(@SourceStr varchar(100), @FillChar varchar(10), @Length int)
returns varchar(100)
as
begin
  declare @OutStr varchar(100)
  set @OutStr = RTrim(@SourceStr)
  if DataLength(@SourceStr) < @Length 
    set @OutStr = (select Right(Replicate(@FillChar, @Length) + @SourceStr, @Length))
  return @OutStr
end
go

grant execute on left_fill to uqweb, QManagerRole

/*
select dbo.left_fill('12',  '0', 1)
select dbo.left_fill('12',  '0', 2)
select dbo.left_fill('12',  '0', 3)
select dbo.left_fill('zzz', 'a', 15)
*/

if object_id('dbo.left_fill_int') is not null
  drop function dbo.left_fill_int
go

create function dbo.left_fill_int(@Source int, @FillChar varchar(10), @Length int)
returns varchar(100)
as
begin
  return (select dbo.left_fill(convert(varchar(100), @Source), @FillChar, @Length))
end
go

grant execute on left_fill_int to uqweb, QManagerRole

/*
select dbo.left_fill_int(12,  '0', 1)
select dbo.left_fill_int(12,  '0', 2)
select dbo.left_fill_int(12,  '0', 3)
select dbo.left_fill_int(123, 'a', 15)
*/
