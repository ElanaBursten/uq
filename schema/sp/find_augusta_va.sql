if object_id('dbo.find_augusta_va') is not null
  drop procedure dbo.find_augusta_va
GO

CREATE Procedure find_augusta_va
	(
	@MapGrid varchar(20)
	)
As

select area.area_name, area.area_id, employee.emp_id, employee.short_name from county_area
  inner join area (NOLOCK) on county_area.area_id = area.area_id
  inner join employee (NOLOCK) on area.locator_id = employee.emp_id

where state='VA'
and county='AUGUSTA'
and munic=@MapGrid
and employee.active = 1
and employee.can_receive_tickets = 1
GO

grant execute on find_augusta_va to uqweb, QManagerRole

