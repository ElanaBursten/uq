if object_id('dbo.set_oldest_open') is not null
	drop procedure dbo.set_oldest_open
go

create proc set_oldest_open
as

declare @oldest_l int
declare @oldest_l_before int
declare @oldest_l_min int

select @oldest_l = (
 select min(locate_id)
 from locate
 where locate.closed_date is null
)

-- dont move back more than 500000 each time
select @oldest_l_before = coalesce( (select max(oldest_open_locate) from oldest_open), 0)
select @oldest_l_min = @oldest_l_before - 500000

if @oldest_l < @oldest_l_min
  select @oldest_l = @oldest_l_min

delete from oldest_open
insert into oldest_open (oldest_open_locate, oldest_open_assignment, oldest_open_ticket)
  select @oldest_l, 0, 0

insert into oldest_open_log (oldest_open_locate, oldest_open_assignment, oldest_open_ticket)
 select oldest_open_locate, oldest_open_assignment, oldest_open_ticket from oldest_open

select top 10 * from oldest_open_log order by insert_date desc

go

/*
exec dbo.set_oldest_open
select * from oldest_open

DONE JUNE 22:

Bob had said "safely close everything before 2003-04-01"

-- Look at old open tickets:
select ticket_format,
 min(transmit_date) as earliest,
 max(transmit_date) as latest,
 count(*) as N
 from ticket where kind<>'DONE' and transmit_date<'2003-04-01'
group by ticket_format

-- This closed them

select * into closed_kyle_june_22 from locate where closed=0 and modified_date<'2003-04-01'

update locate set status='NC', closed=1, closed_by_id=0, closed_how='Bulk', closed_date='2003-04-01'
where closed=0 and modified_date<'2003-04-01'

-- Then these steps make sure all the ticket.kind fields are right:

drop table #t1

# Find the list of open tickets:
select ticket_id into #t1 from locate where closed=0 group by ticket_id

# fix tickets where locates are closed but ticket not DONE
select ticket_id, ticket_number, transmit_date from ticket
 where kind<>'DONE'
 and ticket_id NOT IN (select * from #t1)
 and transmit_date<'2003-04-01'
order by transmit_date desc

# close all tickets that should be closed:
update ticket set kind='DONE'
 where kind<>'DONE'
 and ticket_id NOT IN (select * from #t1)
 and transmit_date<'2003-04-01'

# find tickets that are closed, but with open locates:
select ticket_id, ticket_number, transmit_date from ticket
 where kind='DONE'
 and ticket_id IN (select * from #t1)
 and transmit_date<'2003-04-01'
order by transmit_date desc

# re-open ones marked done but not done:
update ticket set kind='NORMAL'
 where kind='DONE'
 and ticket_id IN (select * from #t1)
 and transmit_date<'2003-04-01'


-- Now find which ones are old and should be closed:

select ticket_id 
into #tickets_to_close
from ticket where kind<>'DONE' and transmit_date<'2003-04-01'

alter table #tickets_to_close add primary key (ticket_id)

drop table tickets_deleted_june22
drop table locates_deleted_june22

select ticket.* 
 into tickets_deleted_june22
 from ticket
 where ticket_id in (select ticket_id from #tickets_to_close)

select locate.* 
 into locates_deleted_june22
 from locate
 where ticket_id in (select ticket_id from #tickets_to_close)

-- Queue them for deletion

update ticket set kind='DELETE'
 from #tickets_to_close ttc
  where ticket.ticket_id = ttc.ticket_id
   and ticket.transmit_date<'2003-04-01'

-- review what will be deleted, should be very very few:

select count(*), max(transmit_date) from ticket where kind='DELETE'
select * from ticket where kind='DELETE'

-- Decide not to delete them, they look valid
update ticket set kind='NORMAL' where kind='DELETE'
*/
