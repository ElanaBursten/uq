if object_id('dbo.get_damage_list') is not null
  drop procedure dbo.get_damage_list
go

create proc get_damage_list (@emp_id int, @open_only bit, @num_days int)
as
  if @open_only = 1
    exec dbo.get_open_damages_for_locator @emp_id
  else begin
    exec dbo.get_old_damages_for_locator @emp_id, @num_days
  end
GO
grant execute on get_damage_list to uqweb, QManagerRole

/*
exec dbo.get_damage_list 766, 0, 7
exec dbo.get_damage_list 3510, 1, 0
exec dbo.get_damage_list 3510, 0, 15
*/
