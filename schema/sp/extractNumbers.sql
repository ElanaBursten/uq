SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('extractNumbers') is not null
	drop FUNCTION extractNumbers
GO
-- =============================================
-- Author:		Larry Killen
-- Create date: 9/26/2018
-- required for the payroll export to Ulti
-- Description:	extracts all numbers from string
-- =============================================
CREATE FUNCTION extractNumbers 
(
	-- Add the parameters for the function here
	@mixedString varchar(20)
)
RETURNS VARCHAR(256)
AS
BEGIN
DECLARE @intAlpha INT
SET @intAlpha = PATINDEX('%[^0-9]%', @mixedString)
	BEGIN
		WHILE @intAlpha > 0
		BEGIN
			SET @mixedString = STUFF(@mixedString, @intAlpha, 1, '' )
			SET @intAlpha = PATINDEX('%[^0-9]%', @mixedString )
		END
	END
RETURN ISNULL(@mixedString,0)
END
GO

--Reversal 
--drop FUNCTION extractNumbers


