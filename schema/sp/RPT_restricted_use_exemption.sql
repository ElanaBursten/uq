if object_id('dbo.RPT_restricted_use_exemption') is not null
  drop procedure dbo.RPT_restricted_use_exemption
go

create proc RPT_restricted_use_exemption (
  @ExemptionsFor int,
  @ExemptionsForID int,
  @GrantedFromDate datetime,
  @GrantedToDate datetime,
  @RevokedFromDate datetime,
  @RevokedToDate datetime,
  @EmployeeStatus int = 2
)
as
  set nocount on

  /*
  @ExemptionsFor=0 to include all employees; @ExemptionForID = 0.
  @ExemptionsFor=1 to include only one employee; @ExemptionForID = emp_id to include
  @ExemptionsFor=2 to include employees under manager; @ExemptionsForID = emp_id of manager
  */
  if @ExemptionsFor = 2
    select rue.*,
      e.short_name as granted_to_name,
      e.last_name as granted_to_last_name,
      g.short_name as granted_by_name,
      IsNull(r.short_name, '-') as revoked_by_name
    from restricted_use_exemption rue
      inner join get_report_hier3(@ExemptionsForID, 0, 99, @EmployeeStatus) on (h_emp_id = rue.emp_id)
      inner join employee e on (e.emp_id = rue.emp_id)
      inner join employee g on (g.emp_id = rue.granted_by)
      left outer join employee r on (r.emp_id = rue.revoked_by)
    where (effective_date >= @GrantedFromDate and effective_date < @GrantedToDate)
    and (@RevokedFromDate is null or revoked_date >= @RevokedFromDate and revoked_date < @RevokedToDate)
  else
    select rue.*,
      e.short_name as granted_to_name,
      e.last_name as granted_to_last_name,
      g.short_name as granted_by_name,
      IsNull(r.short_name, '-') as revoked_by_name
    from restricted_use_exemption rue
      inner join employee e on (e.emp_id = rue.emp_id)
      inner join employee g on (g.emp_id = rue.granted_by)
      left outer join employee r on (r.emp_id = rue.revoked_by)
    where (@ExemptionsForID = 0 or rue.emp_id = @ExemptionsForID)
    and (effective_date >= @GrantedFromDate and effective_date < @GrantedToDate)
    and (@RevokedFromDate is null or revoked_date >= @RevokedFromDate and revoked_date < @RevokedToDate)

go

grant execute on RPT_restricted_use_exemption to uqweb, QManagerRole

/*
execute dbo.RPT_restricted_use_exemption 2, 2676, '2007-05-01T00:00:00.000', '2007-05-31T23:59:59.999', null, null, 1
execute dbo.RPT_restricted_use_exemption 1, 3510, '2007-05-01T00:00:00.000', '2007-05-31T23:59:59.999', null, null
execute dbo.RPT_restricted_use_exemption 0, 3510, '2007-05-01T00:00:00.000', '2007-05-08T23:59:59.999', null, null
*/
