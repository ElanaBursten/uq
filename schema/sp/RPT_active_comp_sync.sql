if object_id('dbo.RPT_active_comp_sync_4') is not null
  drop procedure dbo.RPT_active_comp_sync_4
go

create procedure dbo.RPT_active_comp_sync_4 (
  @pc varchar(15),
  @include_retire_reasons varchar(100)
)
as
set nocount on

  declare @start datetime
  declare @beg15 datetime
  declare @end15 datetime
  declare @beg30 datetime
  declare @end30 datetime
  declare @beg45 datetime
  declare @end45 datetime

  set @start = cast(floor(cast(getdate()+1 as float)) as datetime) -- get date part only
  set @end15 = dateadd(s, -1, @start)
  set @beg15 = dateadd(s, +1, @end15) - 15
  set @end30 = dateadd(s, -1, @beg15)
  set @beg30 = dateadd(s, +1, @end30) - 15
  set @end45 = dateadd(s, -1, @beg30)
  set @beg45 = dateadd(s, +1, @end45) - 15

  declare @tmp_sync table (
    computer_serial  varchar(40)  null,
    emp_number       varchar(20)  null,
    emp_name         varchar(40)  null,
    timesheet_num	   varchar(15)  null,
    pc_code          varchar(15)  null,
    pc_name          varchar(40)  null,
    company_name     varchar(25)  null,
    sync_date        datetime     null,
    active_0015      integer      null,
    active_1630      integer      null,
    active_3145      integer      null,
    active_over45    integer      null 
  )

  insert into @tmp_sync
   (computer_serial,
    emp_number,
    emp_name,
    sync_date,
    timesheet_num,
    pc_code,
    pc_name,
    company_name,
    active_0015,
    active_1630,
    active_3145,
    active_over45)
  select distinct
    computer_info.computer_serial,
    employee.emp_number,
    employee.short_name,
    sync_log.sync_date,
    profit_center.timesheet_num,
    profit_center.pc_code,
    IsNull(profit_center.pc_code+' '+profit_center.pc_name, '[Unassigned Profit Center]'),
    IsNull(locating_company.name, '[Unassigned Company]'),
    case when sync_log.sync_date between @beg15 and @end15 then 1 else 0 end,
    case when sync_log.sync_date between @beg30 and @end30 then 1 else 0 end,
    case when sync_log.sync_date between @beg45 and @end45 then 1 else 0 end,
    case when sync_log.sync_date < @beg45 then 1 else 0 end
  from computer_info
    join sync_log on sync_log.sync_id = computer_info.sync_id
    join employee on employee.emp_id = computer_info.emp_id
    left outer join profit_center on profit_center.pc_code = dbo.get_employee_pc(Employee.emp_id,1)
    left outer join locating_company on locating_company.company_id = employee.company_id
  where computer_info.active = 1 -- any computer that has ever been active since data started being collected on 2-16-2008
    and Coalesce(computer_info.retire_reason, '00') in (select S from dbo.StringListToTable(@include_retire_reasons))
  order by sync_log.sync_date

If (@pc is null) or (@pc = '')
begin
	  select
		company_name,
		pc_name,
		sum(active_1630) as grp1630,
		sum(active_3145) as grp3145,
		sum(active_over45) as grpover45,
		sum(active_1630 + active_3145 + active_over45) as pc_total
	  from @tmp_sync
	  where [@tmp_sync].active_0015 = 0
	  group by company_name, pc_name
	  order by company_name, pc_name

	  select 
		company_name,
		pc_name,
		emp_name,
		emp_number,
		computer_serial,
		sync_date
	  from @tmp_sync
	  where [@tmp_sync].active_0015 = 0
	  order by company_name, pc_name, emp_name, emp_number, computer_serial
end
else
begin
	  select
		company_name,
		pc_name,
		sum(active_1630) as grp1630,
		sum(active_3145) as grp3145,
		sum(active_over45) as grpover45,
		sum(active_1630 + active_3145 + active_over45) as pc_total
	  from @tmp_sync
	  where [@tmp_sync].active_0015 = 0
      and pc_code = @pc
	  group by company_name, pc_name
	  order by company_name, pc_name

	  select
		company_name,
		pc_name,
		emp_name,
		emp_number,
		computer_serial,
		sync_date
	  from @tmp_sync
	  where [@tmp_sync].active_0015 = 0
      and pc_code = @pc
	  order by company_name, pc_name, emp_name, emp_number, computer_serial
end

go
grant execute on RPT_active_comp_sync_4 to uqweb, QManagerRole

/*
exec rpt_active_comp_sync_4 '', '00'
exec rpt_active_comp_sync_4 '007', '00'
exec rpt_active_comp_sync_4 '', '01,02,03'
exec rpt_active_comp_sync_4 'FXL', '01,02,03'

select top 10 * from sync_log
select top 10 * from computer_info

insert into sync_log (emp_id, sync_date, local_date, client_version)
  values (3511, '2008-01-01', '2008-01-01', '2.1.0.8712')

insert into computer_info
  (emp_id, windows_user, os_platform, os_major_version, os_minor_version, os_service_pack, computer_name, computer_serial, domain_login, active, sync_id)
  values (5006, 'CMad', 2, 5, 0, 2, 'LOCPC', '12345S', 'Domain', 1, 25007)
*/


