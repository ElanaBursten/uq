IF OBJECT_ID('dbo.update_damage_accrual_dates') IS NOT NULL
	drop procedure dbo.update_damage_accrual_dates
GO


CREATE PROCEDURE update_damage_accrual_dates AS

if SYSTEM_USER <> 'no_trigger'
begin
  RAISERROR('Only the no_trigger user may call this stored procedure', 18, 1) with log
  return
end

update damage
  set accrual_date =
  dbo.MinOfFourDates(
    damage_date,
    uq_notified_date,
    (select t.due_date from ticket t 
     where t.ticket_id = damage.ticket_id),
    (select min(l.closed_date) from locate l 
     where l.ticket_id = damage.ticket_id and l.status <> '-N')
  )
  where damage_id in (select top 10000 damage_id from damage 
                      where accrual_date is null)
GO

grant execute on update_damage_accrual_dates to uqweb, QManagerRole, no_trigger

/*
exec update_damage_accrual_dates
*/
