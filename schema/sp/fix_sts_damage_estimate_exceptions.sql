if object_id('dbo.fix_sts_damage_estimate_exceptions') is not null
  drop procedure dbo.fix_sts_damage_estimate_exceptions
GO

create procedure dbo.fix_sts_damage_estimate_exceptions (
  @DamageIDFrom int,
  @DamageIDTo int
)
as

set nocount on

declare @TempEstimate table (
  profit_center varchar(10),
  damage_idz int,
  invoice_code varchar(10),
  invoice_total money,
  paid_total money,
  current_estimate money,
  added_by int
)

/* Insert all STS damages with invoices into temporary table */
insert into @TempEstimate (profit_center, damage_idz, invoice_code, invoice_total, paid_total, current_estimate)
  select d.profit_center,
  d.damage_id,
  d.invoice_code,
  sum(coalesce(di.amount,0)) "invoice_total",
  sum(coalesce(di.paid_amount,0)) "paid_total",
  coalesce ((select top 1 de.amount from damage_estimate de
   where de.damage_id = d.damage_id order by de.modified_date desc), 0) "current_estimate"
  from damage d
  inner join damage_invoice di on (di.damage_id = d.damage_id and di.amount > 0)
  inner join office o on (o.profit_center = d.profit_center and o.company_name = 'STS')
  where d.damage_id >= @DamageIDFrom and d.damage_id <= @DamageIDTo
  and d.invoice_code <> 'NOTUSED' /* Invoice Code 5 */
  group by d.profit_center, d.damage_id, d.invoice_code

/* Keep only damages where there is a difference */
delete from @TempEstimate
  where (paid_total <= 0 and current_estimate = invoice_total)
  or (paid_total > 0 and current_estimate = paid_total)

/* Set the added_by emp from damage */
update @TempEstimate
  set added_by = coalesce(d.investigator_id, d.added_by, 3446/*Jan Zunk*/)
  from damage d where d.damage_id = damage_idz

/* Create the new damage_estimate rows */
insert into damage_estimate (damage_id, emp_id, comment, amount)
select damage_idz,
  added_by,
  'Sum of received invoices',
  case
    when paid_total = 0 then invoice_total
    else paid_total
  end
  from @TempEstimate

/* Return the list of created estimates */
select profit_center, damage_idz "damage_id", added_by,
  case
    when paid_total = 0 then invoice_total
    else paid_total
  end "new_estimate",
  current_estimate "old_estimate"
 from @TempEstimate
 order by profit_center, damage_idz

go

grant execute on fix_sts_damage_estimate_exceptions to uqweb, QManagerRole

/*
exec dbo.fix_sts_damage_estimate_exceptions  0, 999999
*/
