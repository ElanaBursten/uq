if object_id('dbo.get_emp_right_limitation') is not null
  drop function dbo.get_emp_right_limitation
go

create function get_emp_right_limitation(@EmpId int, @EntityData varchar(50))
returns
  varchar(200)
as
begin

  return (select limitation 
          from get_right_settings(@EmpID)
          where right_id = (select right_id 
                            from right_definition 
                            where entity_data = @EntityData))
          
end
go

grant execute on get_emp_right_limitation to uqweb, QManagerRole
go


/*
select dbo.get_emp_right_limitation(3510, 'TrackGPSPosition')

declare @Threshold int
set @Threshold = (select convert(Int,dbo.get_emp_right_limitation(3510, 'TrackGPSPosition')) ) 
select @Threshold
select case when @Threshold = 0 then 2000 else @Threshold end
*/

