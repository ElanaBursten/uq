if object_id('dbo.get_pending_responses_ucc') is not null
	drop procedure dbo.get_pending_responses_ucc
GO

-- Like get_pending_responses, but meant to be called on an update call
-- center. Query will return responses for both the update call center
-- and its master call center.

CREATE Procedure get_pending_responses_ucc(
    @UpdateCallCenter varchar(20),
    @ParsedLocatesOnly bit = 1,
    @RetryPeriod int = 240)
as
set nocount on

declare @MasterCallCenter varchar(20)
select @MasterCallCenter = (select top 1 call_center from client where update_call_center = @UpdateCallCenter)

select top 800
   rq.locate_id, locate.status, ticket.ticket_format,
   ticket.ticket_id, ticket.ticket_number, locate.client_code,
   ticket.ticket_type, ticket.work_state, rq.insert_date, 
   ticket.serial_number, locate.closed_date, ticket.work_county,
   ticket.work_city, ticket.work_date, ticket.service_area_code,
   ticket.due_date,
   coalesce((select info from ticket_info ti2 where ti2.ticket_info_id =
    (select max(ticket_info_id) from ticket_info ti
      where ti.ticket_id = locate.ticket_id
      and info_type='ONGOCONT')), '') as contact,
   (select min(arrival_date) from jobsite_arrival ja
      where ja.ticket_id=ticket.ticket_id) as first_arrival_date
   from responder_queue rq
   inner join locate on rq.locate_id=locate.locate_id
   inner join ticket on locate.ticket_id=ticket.ticket_id
  where (@UpdateCallCenter = rq.ticket_format 
         or @MasterCallCenter = rq.ticket_format)
   and (rq.do_not_respond_before is null 
        or rq.do_not_respond_before <= getdate())
   and ticket.ticket_number not like 'MAN%'
   and isnull(ticket.status, '') not like 'MANUAL%'
    and ((@ParsedLocatesOnly = 1 and locate.added_by = 'parser')
       or @ParsedLocatesOnly = 0)
   and not exists (select * from response_log rl
      where rl.reply = '(ftp waiting)'
      and rl.locate_id = rq.locate_id
      and DateDiff(minute, rl.response_date, GetDate()) < @RetryPeriod)
      and locate.client_code in (select oc_code from client where update_call_center = @UpdateCallCenter)
      -- ^ could be replaced w/ an inner join?
  order by insert_date
GO

grant execute on get_pending_responses_ucc to uqweb, QManagerRole

/*
exec get_pending_responses_ucc 'FMB2'
*/
