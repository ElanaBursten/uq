if object_id('dbo.RPT_responder_status_translations') is not null
  drop procedure dbo.RPT_responder_status_translations
GO

/* ActiveFlag Parameter:
 *    0 - only reports on inactive 
 *    1 - only reports on active 
 *    2 - reports on both active & inactive
 */
CREATE PROCEDURE RPT_responder_status_translations (
  @CallCenters varchar(500),
  @ActiveFlag int
)
AS

declare @CallCenterList table (
  cc_code varchar(20) not null
)

if @CallCenters <> '*'
  insert into @CallCenterList
    select S from dbo.StringListToTable(@CallCenters)

select cc.cc_code, cc.cc_name, sl.status, sl.status_name,
  st.outgoing_status, st.responder_type,
  coalesce(st.explanation_code, '-') explanation_code,
  coalesce(st.responder_kind, '-') responder_kind,
  st.active
from status_translation st
join call_center cc on cc.cc_code = st.cc_code
join statuslist sl on sl.status = st.status
where (@CallCenters = '*' or st.cc_code in (select cc_code from @CallCenterList))
  and (@ActiveFlag = 2 or st.active = @ActiveFlag)
order by st.cc_code, st.status

GO

grant execute on RPT_responder_status_translations to uqweb, QManagerRole

/*
exec RPT_responder_status_translations '*', 1
exec RPT_responder_status_translations 'WGL', 1
*/
