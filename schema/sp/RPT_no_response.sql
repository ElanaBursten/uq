if object_id('dbo.RPT_no_response2') is not null
	drop procedure dbo.RPT_no_response2
go

create proc RPT_no_response2 (
	@CallCenterList varchar(40),
	@StartDate datetime,
	@EndDate datetime
)
as
  set nocount on

  declare @tic table (
	call_center varchar (20) NOT NULL ,
	ticket_number varchar (20) NOT NULL ,
	work_address_street varchar (55) NULL ,
	work_address_number varchar (10) NULL ,
	work_address_number_2 varchar (10) NULL ,
	street varchar (100) NULL ,
	map_page varchar (20) NULL ,
	area_name varchar (40) NULL ,
	orig_transmit_date datetime NULL ,
	ticket_id int NOT NULL ,
	transmit_date datetime NOT NULL ,
	ticket_type varchar (38) NULL
  )

  declare @tvloc table (
	emp_id int NULL ,
	emp_number varchar (15) NULL ,
	last_name varchar (30) NULL ,
	first_name varchar (20) NULL ,
	short_name varchar (30) NULL ,
	call_center varchar (20) NOT NULL ,
	ticket_number varchar (20) NOT NULL ,
	work_address_street varchar (55) NULL ,
	work_address_number varchar (10) NULL ,
	work_address_number_2 varchar (10) NULL ,
	street varchar (100) NULL ,
	map_page varchar (20) NULL ,
	area_name varchar (40) NULL ,
	orig_transmit_date datetime NULL ,
	client_code varchar (10) NULL ,
	client_name varchar (40) NULL ,
	ticket_type varchar (38) NULL ,
	ident int not null
  )

  -- I initially implemented this as one large query.  I spent an hour trying to
  -- get the query plan to be reasonable, and failed.  Divided in to two queries,
  -- it runs very fast with no special effort at all.

  -- first, get a list of tickets we care about
  insert into @tic
  select
    t.ticket_format as call_center,
    t.ticket_number,
    t.work_address_street,
    t.work_address_number,
    t.work_address_number_2,
    t.work_address_number + ' ' + t.work_address_number_2 + ' ' + t.work_address_street as street,
    t.map_page,
    area.area_name,
    (select min(transmit_date) from ticket_version tv2 with (index(ticket_version_ticket_id))
     where tv2.ticket_id = t.ticket_id) as orig_transmit_date,
    t.ticket_id,
    tv.transmit_date,
    tv.ticket_type
  from ticket_version tv with (index(ticket_version_transmit_date))
    inner join ticket t on tv.ticket_id = t.ticket_id
    left join area on t.route_area_id = area.area_id
  where tv.transmit_date >= @StartDate and tv.transmit_date < @EndDate
   and tv.ticket_type in 
    ('NO RESPONSE', 'NOSHOW', 'NO SHOW', 'NORM LATE GRID', 'NORMAL LATE')
   and t.ticket_format in (select S from dbo.StringListToTable(@CallCenterList))

  -- second, gather the relevant fields from other tables
  insert into @tvloc
  select
    e.emp_id,
    e.emp_number,
    e.last_name,
    e.first_name,
    e.short_name,
    t.call_center,
    t.ticket_number,
    t.work_address_street,
    t.work_address_number,
    t.work_address_number_2,
    t.street,
    t.map_page,
    t.area_name,
    t.orig_transmit_date,
    l.client_code,
    c.client_name,
    t.ticket_type,
    min(l.ticket_id) as ident   -- we dont care that it is a ticket id
  from @tic t
    inner join locate l on t.ticket_id = l.ticket_id
    left join assignment a on l.locate_id=a.locate_id and a.active=1
    left join employee e on a.locator_id=e.emp_id
    left join client c on l.client_id = c.client_id
  where l.status <> '-N'
  group by
    e.emp_id,
    e.emp_number,
    e.last_name,
    e.first_name,
    e.short_name,
    t.call_center,
    t.ticket_number,
    t.work_address_street,
    t.work_address_number,
    t.work_address_number_2,
    t.street,
    t.map_page,
    t.area_name,
    t.orig_transmit_date,
    l.client_code,
    c.client_name,
    t.ticket_type


  declare @uniqueclients table (
    ident int not null primary key,
    clients varchar(0200)
  )

  INSERT INTO @uniqueclients
  SELECT ident, '' from @tvloc group by ident order by 1

  DECLARE cur1 CURSOR FOR
  SELECT ident, client_code from @tvloc ORDER BY 1, 2

  OPEN cur1

  DECLARE @id INT, @client_code varchar(25)
  FETCH NEXT FROM cur1 INTO @id, @client_code
  WHILE @@FETCH_STATUS = 0
  BEGIN
    update @uniqueclients set clients = clients + @client_code + ' ' where ident=@id
    FETCH NEXT FROM cur1 INTO @id, @client_code
  END

  DEALLOCATE cur1

  -- first result set: ONE LINE PER TICKET

  select
    emp_id,
    emp_number,
    last_name,
    first_name,
    short_name,
    call_center,
    ticket_number,
    work_address_street,
    work_address_number,
    work_address_number_2,
    street,
    map_page,
    area_name,
    orig_transmit_date,
    ticket_type,
    clients
  from @tvloc tvl
    inner join @uniqueclients uc on tvl.ident=uc.ident
  group by 
    emp_id,
    emp_number,
    last_name,
    first_name,
    short_name,
    call_center,
    ticket_number,
    work_address_street,
    work_address_number,
    work_address_number_2,
    street,
    map_page,
    area_name,
    orig_transmit_date,
    ticket_type,
    clients
  order by short_name, emp_id, ticket_number, orig_transmit_date



  -- second result set: totals by client
  select call_center, client_code, client_name,
    count(distinct ticket_number) as N
   from @tvloc
   group by call_center, client_code, client_name
go
grant execute on RPT_no_response2 to uqweb, QManagerRole
/*
exec dbo.RPT_no_response2 'FMW1,FMB1', '2003-07-01', '2003-07-15'
exec dbo.RPT_no_response2 'FMW1', '2003-07-01', '2003-07-10'
exec dbo.RPT_no_response2 'FMW1', '2008-01-22', '2008-01-24'
*/
