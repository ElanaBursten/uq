--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)

if object_id('dbo.get_old_tickets_for_locator2') is not null
  drop procedure dbo.get_old_tickets_for_locator2
go
create procedure dbo.get_old_tickets_for_locator2 (@locator_id int, @num_days int)
as
 
  declare @since datetime
  declare @assignments_since datetime

  select @since = dateadd(Day, -@num_days, getdate())

  select 'ticket' as tname, ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, ticket.work_address_number, ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    (select min(task.est_start_date) from task_schedule task 
     where task.ticket_id = ticket.ticket_id
       and task.est_start_date >= GetDate()
       and task.emp_id = @locator_id 
       and task.active=1) as est_start_time,
	   a.area_name,
	   ticket.route_order,
	   ticket.work_lat lat,
	   ticket.work_long lon,
	   ticket.work_state,
	   ticket.company, ticket.con_name,  --QM-325
--------------------Utility Icons --------------------
	isnull(STUFF 
  ((
    SELECT distinct ', ' + c.Utility_type
      FROM client c, locate l
      WHERE (c.client_id = l.client_id)
	  And (l.ticket_id = ticket.ticket_Id)
	  And l.status <> '-N'
      FOR XML PATH('')), 1, 1, ' '), '-') as util
------------------------------------------------------ 

  from ticket 
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id from locate
     where assigned_to_id = @locator_id
      and (closed_date >= @since or closed_date is null)
      and (status <> '-N')
   )
