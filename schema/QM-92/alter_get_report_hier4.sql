USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[get_report_hier4]    Script Date: 4/14/2020 2:53:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* EmployeeStatus Parameter:
 *    0 - only reports on inactive employees
 *    1 - only reports on active   employees
 *    2 - reports on both active & inactive employees
 */

ALTER function [dbo].[get_report_hier4](@id int, @managers_only bit, @level_limit int, @EmployeeStatus int)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_short_name_tagged varchar(45) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_active bit,
  h_report_level integer,
  h_report_to integer,
  h_report_to_short_name varchar(30) NULL,
  h_eff_payroll_pc varchar(15),
  h_repr_pc_code varchar(15),
  h_namepath varchar(450) NULL,
  h_idpath varchar(160) NULL,
  h_lastpath varchar(450) NULL,
  h_numpath varchar(450) NULL,
  h_charge_cov bit, 
  h_pc_manager_id integer NULL,             -- not in get_report_hier3
  h_pc_manager_short_name varchar(30) NULL, -- not in get_report_hier3       
  h_company_name varchar(25) NULL,          -- not in get_report_hier3       QM-92  SR
  h_1 varchar(30) NULL,
  h_2 varchar(30) NULL,
  h_3 varchar(30) NULL,
  h_4 varchar(30) NULL,
  h_5 varchar(30) NULL,
  h_6 varchar(30) NULL,
  h_7 varchar(30) NULL,
  h_8 varchar(30) NULL,
  h_9 varchar(30) NULL
)
as
begin
  if @level_limit>= 1000 begin
    -- Figure out what PC to limit to,
    -- and get all emp PCs
    declare @pc varchar(15)
    declare @emps TABLE (
      emp_id integer NOT NULL PRIMARY KEY,
      x integer,
      emp_pc_code varchar(15) NULL
    )

    -- 1000 = flat mode for the PC of the named manager
    if @level_limit = 1000 begin
      select @pc = dbo.get_employee_pc(@id, 0)
      insert into @emps select * from employee_pc_data(@pc)
    end

    -- 1001 = flat mode for the PC of the named manager, with Payroll PC overrides
    if @level_limit = 1001 begin
      select @pc = dbo.get_employee_pc(@id, 1)
      insert into @emps select * from employee_payroll_data(@pc)
    end

    -- @emps has everyone with their PCs filled in,
    -- X is the emp_id of who each person inherited their PC from.
    -- those where X is NULL, are the "top" of each PC.

    -- TODO: patch up X for the overrides in employee_payroll_data

    -- Put in the top level of PC representative people:

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_level,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, 1, 1,
        e.short_name, convert(varchar(20), e.emp_id),
        e.last_name+convert(varchar(20), e.emp_id),
        coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        e.short_name
      from @emps em
       inner join employee e on em.emp_id=e.emp_id
      where em.x is null --and e.repr_pc_code is not null
   

    -- put in the second level, which is everyone else
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        r.h_namepath + ';' + e.short_name, r.h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        r.h_1, e.short_name
      from @emps em
       inner join employee e on em.emp_id=e.emp_id
       inner join @results r on em.x = r.h_emp_id
  

  end else begin

    -- Add the user
    INSERT INTO @results
      (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
       h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
       h_namepath, h_idpath,  h_lastpath, h_numpath, h_charge_cov, 
       h_1)
    select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
       e.repr_pc_code, e.type_id, e.active, e.report_to, 1, man.short_name,
       e.short_name, convert(varchar(20), e.emp_id),
       e.last_name+convert(varchar(20), e.emp_id),
       coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
       e.short_name
      from employee e
      left join employee man on e.report_to = man.emp_id
     where e.emp_id = @id

    -- Add everyone below the user; hideously ugly duplication,
    -- but I'd need to use code gen to get rid of it.

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=1
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 2 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=2
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 3 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3, h_4)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=3
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 4 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3, h_4, h_5)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, h_4, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=4
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 5 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov,
          h_1, h_2, h_3, h_4, h_5, h_6)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, h_4, h_5, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=5
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 6 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3, h_4, h_5, h_6, h_7)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, h_4, h_5, h_6, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=6
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 7 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=7
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 8 and ((@managers_only=0) or (et.modifier like '%MGR%'))

    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_repr_pc_code, h_type_id, h_active, h_report_to, h_report_level, h_report_to_short_name,
          h_namepath, h_idpath, h_lastpath, h_numpath, h_charge_cov, 
          h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.repr_pc_code, e.type_id, e.active, e.report_to, r.h_report_level+1, r.h_short_name,
        h_namepath + ';' + e.short_name, h_idpath + ';' + convert(varchar(20), e.emp_id),
        h_lastpath + ';' + e.last_name+convert(varchar(20), e.emp_id),
        h_numpath + ';' + coalesce(e.emp_number,'-') + '.' + convert(varchar(20), e.emp_id), e.charge_cov,
        h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, e.short_name
      from employee e
       inner join @results r on e.report_to = r.h_emp_id AND r.h_report_level=8
       left join reference et on e.type_id = et.ref_id
      where @level_limit >= 9 and ((@managers_only=0) or (et.modifier like '%MGR%'))

  end

  update @results set h_eff_payroll_pc = dbo.get_employee_pc(h_emp_id, 1)

  if @EmployeeStatus = 1 -- Active Only
    delete from @results where h_active = 0

  else if @EmployeeStatus = 0 -- Inactive Only
  begin
    -- if the request is for Inactive employees only, 
    -- their managers are include so the listing don't look weird
    declare @ManagersWithInactiveEmps TABLE(
    emp_id integer NOT NULL PRIMARY KEY,
    con_rep_to integer
    )

    insert into @ManagersWithInactiveEmps(emp_id, con_rep_to) 
      select manager.emp_id, count(r.h_emp_id) 
      from employee manager 
        inner join @results r on 
           manager.emp_id = r.h_report_to and
           r.h_active = 0
      group by manager.emp_id 
      having count(r.h_emp_id) > 0 
  
    delete 
    from @results 
    where 
      -- exclude actives from record set
      h_active = 1 and 
      -- exclude manager without inactive emps
      h_emp_id not in (select emp_id from @ManagersWithInactiveEmps)
  end

 
  update @results
   set h_short_name_tagged = 
    case when h_active=1 then h_short_name
                         else h_short_name + ' (inactive)'
    end,
    h_pc_manager_id = dbo.get_emp_pc_manager(h_emp_id)

  update @results
    set h_pc_manager_short_name = e.short_name 
    from employee e where e.emp_id = h_pc_manager_id

  update @results
    set h_company_name = c.name
    from employee e
    inner join locating_company c on e.company_id = c.company_id
    where e.emp_id = h_emp_id
  return
end
