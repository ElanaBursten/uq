update [dbo].[configuration_data]
set [Sync] = 1
where [name] in
('ClientURL', 'TicketActRefreshSeconds', 'TicketChangeViewLimitsMax',
    'UpdateFileName', 'UpdateFileHash', 'QMClientHttpPort', 'PTOHoursEntryStartDate', 'CenBaseUrl', 'EPR_Pepper', 'RemBaseURL','TEMABaseUrl', -- QM-53
	'override_units', 'SCANA_BaseURL', 'INR_BaseURL', 'JobSafetyURL','JSASafetyTktURL', 'IN_QCard')

	--select *
	--from configuration_data
	--where sync=1