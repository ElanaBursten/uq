--Moving the specific config items from Sync_6 to configuration_data setup

IF NOT EXISTS(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'configuration_data'
    AND column_name = 'sync'
)
   ALTER table configuration_data
   add sync bit NULL
