# scrub_data.py
# Scrub sensitive data from database dumps.

from __future__ import with_statement
import md5
import os
import string
#
import export_data

def make_key(parts, names):
    return string.join([parts[s] for s in names], ':')

class Scrubber:

    def __init__(self, tempdir):
        self.tempdir = tempdir
        self.read_names() # read data for random name generation

    #
    # name generation

    def read_names(self):
        whereami = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(whereami, 'names-last.txt')) as f:
            last_names = f.readlines()
        with open(os.path.join(whereami, 'names-first.txt')) as f:
            first_names = f.readlines()

        self.last_names = [s[:15].strip().capitalize() for s in last_names if s.strip()]
        self.first_names = [s.strip().capitalize() for s in first_names if s.strip()]

    def gen_random_first_name(self, s):
        """ Generate random first name based on string s. """
        idx = int(md5.md5(s).hexdigest()[:4], 16) % len(self.first_names)
        return self.first_names[idx]

    def gen_random_last_name(self, s):
        """ Generate random first name based on string s. """
        idx = int(md5.md5(s).hexdigest()[4:8], 16) % len(self.last_names)
        return self.last_names[idx]

    def gen_random_id(self, s, n=6):
        z = long(md5.md5(s).hexdigest()[-n:], 16) % (10L**n)
        template = "%0" + str(n) + "d"
        return template % z

    def gen_random_number(self, s, maxdigits):
        return long(md5.md5(s).hexdigest(), 16) % (10L ** maxdigits)

    def gen_random_email(self, s):
        name = self.gen_random_first_name(s).lower()
        domain = 'sample'
        return "%s@%s.com" % (name, domain)

    street_types = ["St", "Ave", "Blvd", "Rd", "Ct", "Dr", "Ln"]
    def gen_random_street(self, s):
        """ Generates random street names of the type <number> <name> <type>,
            e.g. 20114 Fooblitzky St. """
        street_type = self.street_types[self.gen_random_number(s, 1) % len(self.street_types)]
        street_name = self.gen_random_last_name(s).capitalize()
        street_number = self.gen_random_number(s, 5)
        return "%s %s %s" % (street_number, street_name, street_type)

    def gen_random_phone(self, s, inc=0):
        digits = "%010d" % (self.gen_random_number(s, 10) + inc)
        return "%s-%s-%s" % (digits[:3], digits[3:6], digits[6:10])
        # note: some tables are only 12 chars long, so a notation of
        # (XXX) XXX-XXXX would be too long. =/

    #
    # actual data scrubbing

    # XXX working through parts (which takes indexes) and fields is clumsy...
    # fix this

    def scrub_users(self, parts):
        parts['password'] = 'password' # use encryption?

        # generate names...
        key = make_key(parts, ['uid', 'first_name', 'last_name', 'emp_id'])
        parts['first_name'] = self.gen_random_first_name(key)
        parts['last_name'] = self.gen_random_last_name(key)

        # XXX do we need to scrub login_id?

    def scrub_employees(self, parts):
        key = string.join([parts[s] for s in ['emp_id', 'short_name', 'emp_number']], ':')
        parts['first_name'] = self.gen_random_first_name(key)
        parts['last_name'] = self.gen_random_last_name(key)
        parts['short_name'] = parts['first_name'] + " " + parts['last_name']

        parts['emp_number'] = parts['dialup_user'] = self.gen_random_id(key)

    def scrub_clients(self, parts):
        # oc_code functions as a kind of key, so let's not scrub it
        #key = string.join([parts[s] for s in ['client_id', 'oc_code', 'call_center']], ':')
        parts['client_name'] = "Client " + parts['oc_code']

    def scrub_call_centers(self, parts):
        #key = string.join([parts[s] for s in ['cc_code', 'cc_name']], ':')
        parts['notes'] = ''
        parts['admin_email'] = 'admin@example.com'
        parts['emergency_email'] = 'emergency@example.com'
        parts['summary_email'] = 'summary@example.com'
        parts['message_email'] = 'message@example.com'
        parts['warning_email'] = 'warning@example.com'

    def scrub_customers(self, parts):
        key = string.join([parts[s] for s in ['customer_id', 'customer_name']], ':')
        parts['customer_name'] = parts['customer_description'] = \
          'Customer %s' % parts['customer_id']
        parts['customer_number'] = self.gen_random_id(key, 8)
        parts['street'] = self.gen_random_street(key)
        parts['phone'] = self.gen_random_phone(key)
        if parts['street_2']:
            parts['street_2'] = parts['street']
        if parts['attention']:
            parts['attention'] = self.gen_random_first_name(key).capitalize()
        if parts['contact_email']:
            parts['contact_email'] = self.gen_random_email(key)

    def scrub_locating_companies(self, parts):
        key = make_key(parts, ['company_id', 'name'])
        #parts['name'] = 'Company %s' % parts['company_id']
        #parts['logo_filename'] = '%s.gif' % parts['company_id']
        parts['address_street'] = self.gen_random_street(key)
        parts['phone'] = self.gen_random_phone(key)

    def scrub_offices(self, parts):
        key = make_key(parts, ['office_id', 'office_name'])
        parts['address1'] = self.gen_random_street(key)
        if parts['phone']:
            parts['phone'] = self.gen_random_phone(key)
        if parts['fax']:
            parts['fax'] = self.gen_random_phone(key, 1)

    def scrub_upload_locations(self, parts):
        key = make_key(parts, ['location_id', 'description'])
        parts['server'] = 'ftp.%s.com' % (self.gen_random_last_name(key).lower())
        parts['password'] = 'password'
        parts['username'] = self.gen_random_first_name(key)

    def scrub_billing_rates(self, parts):
        key = make_key(parts, ['br_id', 'call_center', 'billing_cc', 'status', 
                               'bill_type'])
        delta = self.gen_random_number(key, 2) % 21 - 10 # -10..+10
        old_rate = float(parts['rate'] or 0.0)
        if int(old_rate) > 0: # only change non-zero rates
            if old_rate + delta <= 0:
                new_rate = old_rate - delta
            else:
                new_rate = old_rate + delta
            parts['rate'] = "%0.2f" % new_rate


