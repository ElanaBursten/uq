if object_id('dbo.get_latest_risk_score') is not null
  drop function dbo.get_latest_risk_score
go

Create function [dbo].[get_latest_risk_score](@ticket_id int)
returns varchar(100)
as
begin
 declare @riskscoreconcat varchar(100)


  Set @riskscoreconcat = coalesce(
      (select top 1 concat( 
           tr.risk_source, ' ', tr.risk_score_type, '  ', tr.risk_score) 
       from ticket_risk tr 
       where (tr.ticket_id = @ticket_id)
       order by tr.ticket_version_id desc) , '--')
  --SELECT TRIM('-' FROM @riskscoreconcat) as @riskscoreconcat

  Set @riskscoreconcat = LTRIM(@Riskscoreconcat)
  return @riskscoreconcat
end	

--SELECT TRIM('   -ghfjghth') as result

select dbo.get_latest_risk_scoretry(10521) as new_version_no_Source,
       dbo.get_latest_risk_scoretry(10505) as new_version_with_source,
         dbo.get_latest_risk_score(10521) as old_version_no_source, 
		 dbo.get_latest_risk_score(10505) as old_version_with_source

