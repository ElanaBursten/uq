/*
   Thursday, August 19, 202112:30:00 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___modif__505CB104
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___charg__5150D53D
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___activ__5244F976
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___can_r__53391DAF
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___crew___542D41E8
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___autoc__55216621
GO
ALTER TABLE dbo.employee_history
	DROP CONSTRAINT DF__employee___right__56158A5A
GO
CREATE TABLE dbo.Tmp_employee_history
	(
	active_start datetime NOT NULL,
	active_end datetime NOT NULL,
	active_pc varchar(15) NULL,
	emp_id int NOT NULL,
	type_id int NULL,
	status_id int NULL,
	timesheet_id int NULL,
	emp_number varchar(15) NULL,
	short_name varchar(30) NULL,
	first_name varchar(20) NULL,
	last_name varchar(30) NULL,
	middle_init char(1) NULL,
	report_to int NULL,
	create_date datetime NULL,
	create_uid int NULL,
	modified_date datetime NOT NULL,
	modified_uid int NULL,
	charge_cov bit NOT NULL,
	active bit NOT NULL,
	can_receive_tickets bit NOT NULL,
	timerule_id int NULL,
	dialup_user varchar(20) NULL,
	company_car varchar(10) NULL,
	repr_pc_code varchar(15) NULL,
	payroll_pc_code varchar(15) NULL,
	crew_num varchar(5) NOT NULL,
	autoclose bit NOT NULL,
	company_id int NULL,
	rights_modified_date datetime NOT NULL,
	ticket_view_limit int NULL,
	hire_date datetime NULL,
	show_future_tickets bit NULL,
	incentive_pay bit NULL,
	contact_phone varchar(20) NULL,
	local_utc_bias int NULL,
	ad_username varchar(64) NULL,
	test_com_login varchar(20) NULL,
	adp_login varchar(20) NULL,
	home_lat decimal(9, 6) NULL,
	home_lng decimal(9, 6) NULL,
	alt_lat decimal(9, 6) NULL,
	alt_lng decimal(9, 6) NULL,
	changeby varchar(40) NULL,
	atlas_number varchar(20) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_employee_history SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___modif__505CB104 DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___charg__5150D53D DEFAULT (1) FOR charge_cov
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___activ__5244F976 DEFAULT (1) FOR active
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___can_r__53391DAF DEFAULT (1) FOR can_receive_tickets
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___crew___542D41E8 DEFAULT ('9999') FOR crew_num
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___autoc__55216621 DEFAULT (0) FOR autoclose
GO
ALTER TABLE dbo.Tmp_employee_history ADD CONSTRAINT
	DF__employee___right__56158A5A DEFAULT (getdate()) FOR rights_modified_date
GO
IF EXISTS(SELECT * FROM dbo.employee_history)
	 EXEC('INSERT INTO dbo.Tmp_employee_history (active_start, active_end, active_pc, emp_id, type_id, status_id, timesheet_id, emp_number, short_name, first_name, last_name, middle_init, report_to, create_date, create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets, timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code, crew_num, autoclose, company_id, rights_modified_date, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone, local_utc_bias, ad_username, test_com_login, adp_login, home_lat, home_lng, alt_lat, alt_lng, changeby, atlas_number)
		SELECT active_start, active_end, active_pc, emp_id, type_id, status_id, timesheet_id, emp_number, short_name, first_name, last_name, middle_init, report_to, create_date, create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets, timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code, crew_num, autoclose, company_id, rights_modified_date, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone, local_utc_bias, ad_username, test_com_login, adp_login, home_lat, home_lng, alt_lat, alt_lng, changeby, atlas_number FROM dbo.employee_history WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.employee_history
GO
EXECUTE sp_rename N'dbo.Tmp_employee_history', N'employee_history', 'OBJECT' 
GO
CREATE NONCLUSTERED INDEX employee_history_emp_id_active_start ON dbo.employee_history
	(
	emp_id,
	active_start
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX employee_history_start_date_end_date ON dbo.employee_history
	(
	active_start,
	active_end
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
select Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee_history', 'Object', 'CONTROL') as Contr_Per 