/*
   Thursday, August 19, 202112:15:31 PM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT employee_fk_company
GO
ALTER TABLE dbo.locating_company SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.locating_company', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.locating_company', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.locating_company', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT fk_employee001
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT fk_employee003
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT fk_employee004
GO
ALTER TABLE dbo.reference SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.reference', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.reference', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.reference', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__modifi__7B905C75
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__charge__13FCE2E3
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF_employee_active
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF_employee_can_receive_tickets
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__crew_n__4BC21919
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__autocl__69527C00
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__rights__025E20EC
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__show_f__4D6B35C0
GO
ALTER TABLE dbo.employee
	DROP CONSTRAINT DF__employee__incent__343573A2
GO
CREATE TABLE dbo.Tmp_employee
	(
	emp_id int NOT NULL IDENTITY (200, 1),
	type_id int NULL,
	status_id int NULL,
	timesheet_id int NULL,
	emp_number varchar(15) NULL,
	short_name varchar(30) NULL,
	first_name varchar(20) NULL,
	last_name varchar(30) NULL,
	middle_init char(1) NULL,
	report_to int NULL,
	create_date datetime NULL,
	create_uid int NULL,
	modified_date datetime NOT NULL,
	modified_uid int NULL,
	charge_cov bit NOT NULL,
	active bit NOT NULL,
	can_receive_tickets bit NOT NULL,
	timerule_id int NULL,
	dialup_user varchar(20) NULL,
	company_car varchar(10) NULL,
	repr_pc_code varchar(15) NULL,
	payroll_pc_code varchar(15) NULL,
	crew_num varchar(5) NOT NULL,
	autoclose bit NOT NULL,
	company_id int NULL,
	rights_modified_date datetime NOT NULL,
	ticket_view_limit int NULL,
	hire_date datetime NULL,
	show_future_tickets bit NOT NULL,
	incentive_pay bit NOT NULL,
	contact_phone varchar(20) NULL,
	local_utc_bias int NULL,
	ad_username varchar(64) NULL,
	test_com_login varchar(20) NULL,
	adp_login varchar(20) NULL,
	home_lat decimal(9, 6) NULL,
	home_lng decimal(9, 6) NULL,
	alt_lat decimal(9, 6) NULL,
	alt_lng decimal(9, 6) NULL,
	termination_date datetime NULL,
	plat_update_date datetime NULL,
	plat_age  AS (datediff(day,[plat_update_date],getdate())),
	changeby varchar(40) NULL,
	phoneCo_ref int NULL,
	atlas_number varchar(20) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_employee SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__modifi__7B905C75 DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__charge__13FCE2E3 DEFAULT (1) FOR charge_cov
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF_employee_active DEFAULT (1) FOR active
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF_employee_can_receive_tickets DEFAULT (1) FOR can_receive_tickets
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__crew_n__4BC21919 DEFAULT ('9999') FOR crew_num
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__autocl__69527C00 DEFAULT (0) FOR autoclose
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__rights__025E20EC DEFAULT (getdate()) FOR rights_modified_date
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__show_f__4D6B35C0 DEFAULT ((1)) FOR show_future_tickets
GO
ALTER TABLE dbo.Tmp_employee ADD CONSTRAINT
	DF__employee__incent__343573A2 DEFAULT ((0)) FOR incentive_pay
GO
SET IDENTITY_INSERT dbo.Tmp_employee ON
GO
IF EXISTS(SELECT * FROM dbo.employee)
	 EXEC('INSERT INTO dbo.Tmp_employee (emp_id, type_id, status_id, timesheet_id, emp_number, short_name, first_name, last_name, middle_init, report_to, create_date, create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets, timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code, crew_num, autoclose, company_id, rights_modified_date, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone, local_utc_bias, ad_username, test_com_login, adp_login, home_lat, home_lng, alt_lat, alt_lng, termination_date, plat_update_date, changeby, phoneCo_ref, atlas_number)
		SELECT emp_id, type_id, status_id, timesheet_id, emp_number, short_name, first_name, last_name, middle_init, report_to, create_date, create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets, timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code, crew_num, autoclose, company_id, rights_modified_date, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone, local_utc_bias, ad_username, test_com_login, adp_login, home_lat, home_lng, alt_lat, alt_lng, termination_date, plat_update_date, changeby, phoneCo_ref, atlas_number FROM dbo.employee WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_employee OFF
GO
ALTER TABLE dbo.task_schedule
	DROP CONSTRAINT task_schedule_fk_employee
GO
ALTER TABLE dbo.task_schedule
	DROP CONSTRAINT task_schedule_fk_added_by
GO
ALTER TABLE dbo.employee_right
	DROP CONSTRAINT fk_employee_right_emp
GO
ALTER TABLE dbo.ticket_activity_ack
	DROP CONSTRAINT FK_ticket_activity_ack_ack_emp_id
GO
ALTER TABLE dbo.ticket_activity_ack
	DROP CONSTRAINT FK_ticket_activity_ack_locator_emp_id
GO
ALTER TABLE dbo.assignment
	DROP CONSTRAINT assignment_fk_employee
GO
ALTER TABLE dbo.break_rules_response
	DROP CONSTRAINT break_rules_response_fk_employee
GO
ALTER TABLE dbo.[document]
	DROP CONSTRAINT document_fk_employee
GO
ALTER TABLE dbo.damage_estimate
	DROP CONSTRAINT damage_estimate_fk_employee
GO
ALTER TABLE dbo.employee_pay_incentive
	DROP CONSTRAINT employee_pay_incentive_fk_employee
GO
ALTER TABLE dbo.ticket_snap
	DROP CONSTRAINT ticket_snap_fk_employee
GO
ALTER TABLE dbo.employee_activity
	DROP CONSTRAINT employee_activity_fk_employee
GO
ALTER TABLE dbo.locate_snap
	DROP CONSTRAINT locate_snap_fk_employee
GO
ALTER TABLE dbo.timesheet
	DROP CONSTRAINT fk_timesheet001
GO
ALTER TABLE dbo.restricted_use_exemption
	DROP CONSTRAINT FK_restricted_use_exemption_emp_id
GO
ALTER TABLE dbo.addin_info
	DROP CONSTRAINT addin_info_fk_employee
GO
ALTER TABLE dbo.restricted_use_exemption
	DROP CONSTRAINT FK_restricted_use_exemption_granted_by
GO
ALTER TABLE dbo.restricted_use_exemption
	DROP CONSTRAINT FK_restricted_use_exemption_revoked_by
GO
ALTER TABLE dbo.users
	DROP CONSTRAINT fk_user002
GO
ALTER TABLE dbo.ticket_ack
	DROP CONSTRAINT FK_ack_emp_id
GO
ALTER TABLE dbo.employee_office
	DROP CONSTRAINT fk_employee_office001
GO
ALTER TABLE dbo.ticket_ack
	DROP CONSTRAINT FK_locator_emp_id
GO
ALTER TABLE dbo.employee_upload_queue
	DROP CONSTRAINT employee_upload_queue_fk_employee
GO
ALTER TABLE dbo.jobsite_arrival
	DROP CONSTRAINT jobsite_arrival_arriver_fk_employee
GO
ALTER TABLE dbo.jobsite_arrival
	DROP CONSTRAINT jobsite_arrival_adder_fk_employee
GO
ALTER TABLE dbo.jobsite_arrival
	DROP CONSTRAINT jobsite_arrival_deleter_fk_employee
GO
ALTER TABLE dbo.work_order
	DROP CONSTRAINT work_order_fk_assigned_to_id
GO
ALTER TABLE dbo.asset_assignment
	DROP CONSTRAINT asset_assignment_fk_employee
GO
ALTER TABLE dbo.area
	DROP CONSTRAINT area_fk_employee
GO
ALTER TABLE dbo.damage_bill
	DROP CONSTRAINT damage_bill_fk_employee
GO
ALTER TABLE dbo.wo_assignment
	DROP CONSTRAINT wo_assignment_fk_assigned_to_id
GO
ALTER TABLE dbo.wo_status_history
	DROP CONSTRAINT wo_status_history_fk_statused_by_id
GO
ALTER TABLE dbo.locate_facility
	DROP CONSTRAINT locate_facility_added_by_fk_employee
GO
ALTER TABLE dbo.locate_facility
	DROP CONSTRAINT locate_facility_deleted_by_fk_employee
GO
ALTER TABLE dbo.locate_facility
	DROP CONSTRAINT locate_facility_modified_by_fk_employee
GO
ALTER TABLE dbo.attachment
	DROP CONSTRAINT attachment_fk_employee
GO
ALTER TABLE dbo.message
	DROP CONSTRAINT message_fk_emp
GO
ALTER TABLE dbo.message_dest
	DROP CONSTRAINT message_dest_fk_emp
GO
ALTER TABLE dbo.gps_position
	DROP CONSTRAINT gps_position_fk_employee
GO
DROP TABLE dbo.employee
GO
EXECUTE sp_rename N'dbo.Tmp_employee', N'employee', 'OBJECT' 
GO
ALTER TABLE dbo.employee ADD CONSTRAINT
	pk_employee PRIMARY KEY CLUSTERED 
	(
	emp_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX report_to ON dbo.employee
	(
	report_to
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_employee_modified_date ON dbo.employee
	(
	modified_date
	) INCLUDE (emp_id, type_id, status_id, timesheet_id, emp_number, payroll_pc_code, company_id, ticket_view_limit, show_future_tickets, contact_phone, local_utc_bias, short_name, report_to, active, timerule_id, company_car, repr_pc_code) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX Repr_PC_Code ON dbo.employee
	(
	repr_pc_code,
	emp_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX payroll_pc_code ON dbo.employee
	(
	payroll_pc_code
	) INCLUDE (emp_id) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_Employee_emp_number_active ON dbo.employee
	(
	emp_number,
	active
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.employee WITH NOCHECK ADD CONSTRAINT
	fk_employee001 FOREIGN KEY
	(
	type_id
	) REFERENCES dbo.reference
	(
	ref_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.employee WITH NOCHECK ADD CONSTRAINT
	fk_employee003 FOREIGN KEY
	(
	status_id
	) REFERENCES dbo.reference
	(
	ref_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.employee WITH NOCHECK ADD CONSTRAINT
	fk_employee004 FOREIGN KEY
	(
	timesheet_id
	) REFERENCES dbo.reference
	(
	ref_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.employee ADD CONSTRAINT
	employee_fk_company FOREIGN KEY
	(
	company_id
	) REFERENCES dbo.locating_company
	(
	company_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE trigger [dbo].[employee_iu] on dbo.employee   --QM-125         
for insert, update  
not for replication  
as  
begin  
  set nocount on  
  
  declare @ChangeDate datetime  
  set @ChangeDate = getdate()  
  
  -- Don't update modified date or add history for rights_midified_date changes alone  
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)  
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)  
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)  
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)  
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)  
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)  
    or update(incentive_pay) or update(contact_phone) or update(local_utc_bias) or update(ad_username) or update(test_com_login) 
	or update(adp_login) or update(home_lat) or update(home_lng) or update(alt_lat) or update(alt_lng) or update(atlas_number)))  --QM-125   
  begin  
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)  
  
    if update(report_to) or update(repr_pc_code)  
    begin -- This change may modify the effective PC of non-updated employees  
      -- Recalculate and store any changes to everyone below these employees  
  
      declare @AllEmps TABLE (  
        emp_id integer NOT NULL PRIMARY KEY,  
        x integer,  
        emp_pc_code varchar(15) NULL  
      )  
  
      insert into @AllEmps select * from employee_pc_data(null)  
  
      declare @ChangedEmps TABLE (  
        emp_id integer NOT NULL PRIMARY KEY  
      )  
  
      insert into @ChangedEmps  
        select emps.emp_id  
        from @AllEmps emps  
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate  
        where ((emps.emp_pc_code <> eh.active_pc)  
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))  
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))  
          or emps.emp_id in (select emp_id from inserted)  
  
      -- "Deactivate" any current history records  
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)  
  
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      )  
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
        home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
	  from employee e  
        inner join @AllEmps emps on emps.emp_id = e.emp_id  
      where e.emp_id in (select emp_id from @ChangedEmps)  
    end  
    else -- This change only affects the exact employee(s) being modified  
    begin  
      -- "Deactivate" any current history records  
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)  
  
      -- Record the new/current employee data  
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,  
        local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      )  
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,  
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,  
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,  
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,  
        crew_num, autoclose, company_id, rights_modified_date,  
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets,  
        incentive_pay, contact_phone, local_utc_bias,ad_username,test_com_login,adp_login,  
		home_lat,home_lng, alt_lat, alt_lng, changeby, atlas_number --QM-125   
      from inserted  
    end  
  end  
end
GO
CREATE TRIGGER employee_no_delete
ON dbo.employee INSTEAD OF DELETE
AS
BEGIN
    RAISERROR ('Deleting employees is not allowed (due to an "instead of delete" trigger)', 16, 1)
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.employee', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.gps_position ADD CONSTRAINT
	gps_position_fk_employee FOREIGN KEY
	(
	added_by_id
	) REFERENCES dbo.employee
	(
	emp_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.gps_position SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.gps_position', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.gps_position', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.gps_position', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.message_dest ADD CONSTRAINT
	message_dest_fk_emp FOREIGN KEY
	(
	emp_id
	) REFERENCES dbo.employee
	(
	emp_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.message_dest SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.message_dest', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.message_dest', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.message_dest', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.message ADD CONSTRAINT
	message_fk_emp FOREIGN KEY
	(
	from_emp_id
	) REFERENCES dbo.employee
	(
	emp_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.message SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.message', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.message', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.message', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.attachment ADD CONSTRAINT
	attachment_fk_employee FOREIGN KEY
	(
	attached_by
	) REFERENCES dbo.employee
	(
	emp_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
