USE [QM]
GO

/****** Object:  StoredProcedure [dbo].[close_all_locates_for_locator]    Script Date: 9/25/2020 9:57:29 AM ******/
DROP PROCEDURE [dbo].[close_all_locates_for_locator]
GO

/****** Object:  StoredProcedure [dbo].[close_all_locates_for_locator]    Script Date: 9/25/2020 9:57:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[close_all_locates_for_locator] (
	@LocatorID int,
	@Status varchar(20),
	@Billed bit,
	@Excludes varchar(max)  --QMANTWO-799

)
as
set nocount on

--declare @LocatorID int,            --test
--		@Status    varchar(20),
--		@Billed    bit,
--		@Excludes  varchar(max);

--set @LocatorID = 10
--set @Status    = 'M'
--set @Billed    = 1
--set @Excludes  = 'FirstClient, SecondClient, ThirdClient, FourthClient'


declare @ltoc table (
	locate_id int not null primary key
)

declare @exclsTab table (              --QMANTWO-799
	excludeClients varchar(16)
)

insert into @exclsTab
select * from [dbo].StringSplit (@Excludes,',')  --QMANTWO-799

--select excludeClients from @exclsTab    --test


insert into @ltoc
select distinct locate.locate_id
 from assignment
 inner join locate on locate.locate_id=assignment.locate_id
 where locator_id=@LocatorID
  and locate.closed=0 
  and assignment.active=1
  and locate.client_code not in (select excludeClients from @exclsTab)  --QMANTWO-799

select count(*) as LocatesToProcess from @ltoc

declare @closed_date_now datetime
select @closed_date_now = getdate()

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
	   entry_date=@closed_date_now,   --QM-249
       qty_marked=1,
       invoiced=@Billed
  where locate_id = @loc

  FETCH NEXT FROM LocateCursor into @loc
END

GO

