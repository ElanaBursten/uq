--get_old_tickets_for_locator_RO

--QM-997 Work Address 2 not showing as range
--QM-973 Add Expiration_date and Alt_due_date
--QM-916 Route Order Ticket limit REFACTORING (don't enforce limits, change nulls to 9999)
--QM-896 Route Order Ticket limit setting - this is a new stored procedure
-- Per Request, it is STRICTLY ordering on route order
--------------Based off of get_old_tickets_for_locator2 (version in QM-796: updates summarized below)-------
--QM-796 Adding a couple of NoLOCKS to improve speed
--QM-751 First Task (Just added nolock) 5/2023
--QM-551 Age 
--QM-387 Risk Score
--QM-368 Update to STRING_AGG:  Only get utils for this locator
--QM-353 Add top level manager utility summary
--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)

if object_id('dbo.get_old_tickets_for_locator_RO') is not null
  drop procedure dbo.get_old_tickets_for_locator_RO
go
create procedure dbo.get_old_tickets_for_locator_RO (
       @locator_id int, 
	   @num_days int,
	   @toplevel bit = 0)
as
 
  declare @since datetime
  declare @assignments_since datetime

  select @since = dateadd(Day, -@num_days, getdate())

  select 'ticket' as tname, ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, 
	--TRIM('-' FROM (CONCAT(work_address_number + '-', work_address_number_2))) as work_address_number,  --QM-997 EB
	Case  --QM-997
	   WHEN (ticket.work_address_number_2 IS NOT NULL) AND (ticket.work_address_number_2 <> '')
       THEN ticket.work_address_number + '-' + ticket.work_address_number_2
	   ELSE ticket.work_address_number
	 END AS work_address_number,  
	ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date,  null as end_date,  null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    (select min(task.est_start_date) from task_schedule task with (nolock) 
     where task.ticket_id = ticket.ticket_id
       and task.est_start_date >= GetDate()
       and task.emp_id = @locator_id 
       and task.active=1) as est_start_time,
	   a.area_name,
	   	case when ticket.route_order is null then 9999 else ticket.route_order end, 
	   ticket.work_lat lat,
	   ticket.work_long lon,
	   ticket.work_state,
	   ticket.company, ticket.con_name,  --QM-325

--------------Utility Icons -----------------------
      CASE
        WHEN @toplevel = 0 THEN   --Not top level node
		  (SELECT STRING_AGG(c.Utility_type, ', ') as u
           FROM locate l1 with (NOLOCK) , client c with (NOLOCK)
           WHERE (l1.ticket_id = ticket.ticket_Id)
		   AND (l1.assigned_to  = @LOCATOR_ID)  -- Narrow down to just this locator
	       AND (c.client_id = l1.client_id)	
	       And (l1.client_id is not null))            --QM-357 effectively removes -Ns  SR
      ELSE ' '			 
      END as Util,
----------------------------------------------------
      (select dbo.get_latest_risk_score(ticket.ticket_id)),
      (select dbo.get_ticket_age_hours(ticket.ticket_id)) as age_hours,
      ticket.alt_due_date,    --QM-973
      ticket.expiration_date  --QM-973

  FROM ticket with (NOLOCK) 
  left join reference r on ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id from locate with (nolock)
     where assigned_to_id = @locator_id
      and (closed_date >= @since or closed_date is null)
      and (status <> '-N')
   ) --and route_order is not NULL
   ORDER BY route_order



   -----------Utility Icons (2012 Compatible)-------------------
   --          CASE
   --               WHEN @toplevel = 0 THEN   --Not top level node
			--	    isnull(STUFF 
   --                 ((
   --                   SELECT distinct ', ' + c.Utility_type
   --                   FROM client c, locate l WITH (NOLOCK)
   --                   WHERE (c.client_id = l.client_id)
	  --                And (l.ticket_id = ticket.ticket_Id)
	  --                And l.client_id is not null
   --                   FOR XML PATH('')), 1, 1, ' '), '-') 				    
			--	 ELSE ' '			 
			--END as UTIL,
GO


