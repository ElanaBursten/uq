--get_unacked_tickets_RO

--QM-997 Work Address 2 not showing as range
--QM-973 Add Expiration_date and Alt_due_date
--QM-916 Route Order Ticket limit REFACTORING (don't enforce limits, change nulls to 9999)
--QM-896 Route Order Limit (ordering by Route Order) - New
---------Modifications from previous get_unacked_tickets_3 versions--------
--QM-796 Adding a couple of NoLOCKS to improve speed
--QM-551 Age 
--QM-387 Risk Score
--QM-368 Update STRING_AGG
--QM-353 Remove utils from top nodes
--QM-325 + Company + Contract Name
--QM-306 + Utilities (rollup)
if object_id('dbo.get_unacked_tickets_RO') is not null
  drop procedure dbo.get_unacked_tickets_RO
go	


CREATE procedure [dbo].[get_unacked_tickets_RO] 
  (@mgr_id int,
   @toplevel bit = 0)
as
  set nocount on

  select 'ticket' as tname, ticket.ticket_id, ticket.ticket_number, ticket.kind, ticket.due_date,
    ticket.work_address_street, ticket.work_description, ticket.work_type,
    ticket.legal_good_thru, ticket.map_page, ticket.ticket_type, ticket.work_city,
    ticket.ticket_format, 
	--TRIM('-' FROM (CONCAT(work_address_number + '-', work_address_number_2))) as work_address_number,  --QM-997 EB
	Case  --QM-997
	   WHEN (ticket.work_address_number_2 IS NOT NULL) AND (ticket.work_address_number_2 <> '')
       THEN ticket.work_address_number + '-' + ticket.work_address_number_2
	   ELSE ticket.work_address_number
	 END AS work_address_number,  
	ticket.work_county,
    ticket.do_not_mark_before, ticket.modified_date, ticket.alert,
    null as scheduled, null as start_date, null as end_date, null as points,
    coalesce(r.sortby, 0) as work_priority_sort,
    coalesce(r.description, 'Normal') as work_priority,
    'Y' as ticket_is_visible,
    null as est_start_time, 
	a.area_name,
	case when ticket.route_order is null then 9999 else ticket.route_order end, 
	ticket.work_lat lat,
	ticket.work_long lon,
	ticket.work_state,
	ticket.company, ticket.con_name,  --QM-325
	
-----------STRING_AGG Utility Icons (All Locators)-------------------
    CASE
      WHEN @toplevel = 0 THEN   --Not top level node
		 (SELECT STRING_AGG(c.Utility_type, ', ') as u
          FROM locate l1 with (NOLOCK) , client c with (NOLOCK)
          WHERE (l1.ticket_id = ticket.ticket_Id)
	      AND (c.client_id = l1.client_id)	
	      And (l1.client_id is not null))              --QM-357 effectively removes -Ns  SR
	 ELSE ' '				 
     END as Util,

-------------------------------------------
 (select dbo.get_latest_risk_score(ticket.ticket_id)) as risk,
 -------------------------------------------
 (select dbo.get_locator_name_for_ticket(ticket.ticket_id)) as short_name,
 (select dbo.get_ticket_age_hours(ticket.ticket_id)) as age_hours,   --QM-551
 ticket.alt_due_date,    --QM-973
 ticket.expiration_date  --QM-973

  from ticket with (NOLOCK)
  left join reference r on r.ref_id = ticket.work_priority_id
  left outer join area a on ticket.route_area_id = a.area_id
  where ticket.ticket_id in (
    select ticket_id
    from ticket_ack with (NOLOCK)
    inner join (select h_emp_id from dbo.get_hier(@mgr_id,0)) mgremps
      on mgremps.h_emp_id = ticket_ack.locator_emp_id
    where ticket_ack.has_ack = 0 ) --and (route_order is not NULL)
	order by route_order
GO


--	get_unacked_tickets_RO 3512 

----------- OLD Utility Code - Utility Icons (do not remove)-------------------
   --          CASE
   --               WHEN @toplevel = 0 THEN   --Not top level node
			--	    isnull(STUFF 
   --                 ((
   --                   SELECT distinct ', ' + c.Utility_type
   --                   FROM client c, locate l WITH (NOLOCK)
   --                   WHERE (c.client_id = l.client_id)
	  --                And (l.ticket_id = ticket.ticket_Id)
	  --                And l.client_id is not null  
   --                   FOR XML PATH('')), 1, 1, ' '), '-') 				    
			--	 ELSE ' '			 
			--END  as util,




