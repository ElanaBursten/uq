------------------------------------------------
-- QM-771 Ticket Alerts Template:    reference table entry for 'alerttype'
-- Note: Make sure to use the not exists clause to avoid duplicate entries
-- The alerttype group is strictly for the ticket_alert table: alert_type
-- Instead of using priorities or other grouping mechanisms, this will be defined so that
-- they are displayed in a more uniform manner.  Special alerts may also have a modifier of
-- the highlight color used for the row.

-- EB Created 6/2023
-- SH Edited 7/6/2023
----------------------------------------------------------


-- PAR
USE QM
If not exists(select * from reference where [type]='alerttype' and code = 'PAR')
  insert into reference([type], code, [description], sortby, active_ind, modifier)
  VALUES(
  'alerttype',
  'PAR',
  'High Profile Notifications',
  1,
   1, --for PAR
  'BLUE')
  -------------------------------------------------------
If not exists(select * from reference where [type]='alerttype' and code = 'GEN')
  insert into reference([type], code, [description], sortby, active_ind, modifier)
  VALUES(
  'alerttype',
  'GEN',
  'GENERAL TICKET ALERT',
  2,
   1, --inactive because it is an example
  'GREY')


select * from reference where [type] = 'alerttype'
