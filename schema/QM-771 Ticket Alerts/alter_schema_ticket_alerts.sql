------------------------------------------------
-- QM-771 Ticket Alerts:    ticket_alert
-- A Ticket Alert typically comes in when the ticket is parsed to
-- alert the locator of any conditions BEFORE starting the ticket
-- UNLIKE notes, it should be kept to alerts needed BEFORE the ticket is started
--  **** Notes was not used for this BECAUSE it has become too busy and we did not want the alert to
--       get lost in all the notes.

--
--   REFERENCE LINK: The classification (types) of alerts are defined in the reference table
--       as 'alerttype'. and are linked by alert_type_code = Reference.code
--       Reference.description:  General description of the alert code used (not to be confused with the alert_desc)
--       Reference.sortby:       Used to order the alerts in the grid
--       Reference.modifier:     Color used for this alert code ('RED', 'YELLOW', 'GREEN', 'BLUE', 'ORANGE', 'BLACk'  
--                                                                with 'GRAY' being default if no code set and sorting at 9999)

-- EB Created 6/2023
------------------------------------------------


if not exists (select table_name from information_schema.tables where table_name = 'ticket_alert')

create table ticket_alert(
  alert_id  integer IDENTITY(1,1) NOT NULL,
  ticket_id integer NOT NULL,
  alert_source varchar(100) NOT NULL,           -- Who sent the alert
  alert_type_code varchar(15) NULL,            -- reference table 'alerttype'     if alert_type_code is not populated, client will default to general alert
  alert_desc varchar(8000) NOT NULL,
  active bit NOT NULL DEFAULT 1,
  insert_date datetime NOT NULL DEFAULT GetDATE(),
  modified_date datetime NOT NULL DEFAULT GetDATE()
  )
  GO

  create index ticket_alert_type on ticket_alert(alert_type_code)
  GO

  create index ticket_alert_ticket_id on ticket_alert(ticket_id)
  GO


  select * from ticket_alert