------------------------------------------------
-- QM-771 Ticket Alerts:    pulls the active ticket alerts for a ticket
-- EB Created 6/2023
-- NOTES:
--   If there is no alert_type_code, then this call will default the value to general alert (GEN)
--        
--   REFERENCE LINK: The classification (types) of alerts are defined in the reference table
--       as 'alerttype'. and are linked by alert_type_code = Reference.code
--       Reference.description:  General description of the alert code used (not to be confused with the alert_desc)
--       Reference.sortby:       Used to order the alerts in the grid
--       Reference.modifier:     Color used for this alert code ('RED', 'YELLOW', 'GREEN', 'BLUE', 'ORANGE', 'BLACk'  
--                                                                with 'GRAY' being default if no code set and sorting at 9999)

----------------------------------------------------------

if object_id('get_ticket_alerts') is not null
  drop procedure dbo.get_ticket_alerts
go


CREATE procedure dbo.get_ticket_alerts (
  @TicketID int
) WITH RECOMPILE 
as

select 'ticket_alert' as tname, 
  alert_id,
  ticket_id,
  alert_source,
  ISNULL(NULLIF([alert_type_code], ''), 'GEN') as alert_type_code,  --default to GEN code in the reference table
  alert_desc,
  active,
  insert_date,
  modified_date
from ticket_alert ta with (NOLOCK)
  where (ta.ticket_id = @TicketID)
    and (ta.alert_type_code is NOT NULL)
  and (ta.active = 1)

GO

--  exec dbo.get_ticket_alerts 8485

--  select * from ticket_alert
