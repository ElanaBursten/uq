-----------------------------------------------
-- QM-702 Optimize Route
-- Added 2/2023 SH


-----------------------------------------------
--table: route_optimize_queue
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'route_optimize_queue')
Create Table  route_optimize_queue(
	route_optimize_id int identity(1,1) Primary Key NOT NULL,
	tree_emp_id int NOT NULL,
	insert_date datetime NOT NULL,
	inserted_by int NOT NULL,
	active bit NULL,
	next_ticket_id int NULL
)
GO

-----------------------------------------------
--table: route_optimize_queue_history
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'route_optimize_queue_history')
Create Table  route_optimize_queue_history(
	route_optimize_id int NOT NULL,
	tree_emp_id int NOT NULL,
	insert_date datetime NOT NULL,
	inserted_by int NOT NULL,
	active bit NULL,
	next_ticket_id int NULL
)
GO

IF NOT EXISTS (select * from sys.indexes where object_id = object_id('route_optimize_queue_history_id_indx')) 
  create index route_optimize_queue_history_id_indx on route_optimize_queue_history(route_optimize_id)
GO

--select * from route_optimize_queue
--select * from route_optimize_queue_history



