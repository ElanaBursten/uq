-----------------------------------------------
-- QM-702 Route Opitimization 
-- Delete Trigger: Saves a copy of the route_optimize_queue record in route_optimize_queue_history when deleted from route_optimize_queue
-- EB 2/2023
-----------------------------------------------

if exists(select name FROM sysobjects where name = 'route_optimize_queue_d' AND type = 'TR')
  DROP TRIGGER route_optimize_queue_d
GO
create trigger route_optimize_queue_d
on route_optimize_queue
for delete
as
begin
  insert into route_optimize_queue_history
         select route_optimize_id,tree_emp_id,insert_date,inserted_by,active,next_ticket_id 
	     from deleted 
end
GO