--QM-702 Route Order Optimization
--EB 2/2013
-----------------------------------------------
--Permissions - Use to make the Optimize button visible - optimize the route order
-----------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsUseRouteOptimize')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - Use Route Optimize Queue', 'General', 'TicketsUseRouteOptimize', 'Limitation does not apply to this right.', GetDate());
GO


--  select * from right_definition where entity_data = 'TicketsUseRouteOptimize'


