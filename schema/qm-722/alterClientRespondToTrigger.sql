USE [QM]
GO
/****** Object:  Trigger [dbo].[IU_AddToMultiQ]    Script Date: 3/16/2023 5:47:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Larry Killen
-- Jira:        QM-503
-- Create date: 12/8/2021
-- Description:	moves data into multi_queue based on respond_to fieldQA
-- =============================================
ALTER TRIGGER [dbo].[IU_AddToMultiQ] 
   ON  [dbo].[responder_queue] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--if NCA1 and Respond_to = CA_NCA_PELI X W these are not Pelican tickets  Need ticklet Number
declare @ClientID integer, @LocateID integer, @TicketFormat varchar(20), @ClientCode varchar(20), @ClientRespondTo varChar(20), @TicketNoLtr varChar(1);
Select @LocateID=locate_id, @TicketFormat=ticket_format,@ClientCode=client_code  from INSERTED;

Select @TicketNoLtr = Left(ticket_number,1) --qm-596
From [dbo].[ticket]
where ticket_id = (Select ticket_id
					From locate
					where [locate_id] = @LocateID)

set @ClientID = (
  select client_id --client_id 
  from client
  where call_center = @TicketFormat
  and oc_code =@ClientCode
  and active=1) 

--Set  @ClientRespondTo = 
	Declare db_cursor CURSOR for
	SELECT CLIENT_RESPOND_TO
	FROM [DBO].[CLIENT_RESPOND_TO]
	WHERE CLIENT_ID =@CLIENTID
	and Active = 1   --qm-722

	Open db_cursor
	FETCH NEXT FROM db_cursor into @ClientRespondTo						
	WHILE @@FETCH_STATUS = 0 
	Begin	
	  if  not( ( (upper(@TicketNoLtr)='X') or (upper(@TicketNoLtr)='W') ) and (@ClientRespondTo = 'CA_NCA_PELI'  or @ClientRespondTo = 'NV_NVA_PELI' or @ClientRespondTo = 'att'))  --qm-596				 
	  begin
		  insert into [QM].[dbo].[responder_multi_queue] ([respond_to], [locate_id],  [ticket_format], [client_code] )
		  values 
		  (@ClientRespondTo, @LocateID, @TicketFormat, @ClientCode)

	      delete from [dbo].[responder_queue] where [locate_id]=@LocateID	
	  end 
	FETCH NEXT FROM db_cursor into @ClientRespondTo
	END
	CLOSE db_cursor
	DEALLOCATE db_cursor
END
