USE [QM]
GO
/****** Object:  UserDefinedFunction [dbo].[CR_LF_stripper]    Script Date: 11/16/2022 9:25:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Larry Killen
-- Create date: 9/26/2018
-- Description:	removed CRs and LFs
-- =============================================
Create FUNCTION [dbo].[CR_LF_stripper] 
(
	-- Add the parameters for the function here
	@mixedString varchar(200)
)
RETURNS VARCHAR(256)
AS
BEGIN
  RETURN REPLACE(REPLACE(@mixedString, CHAR(13), ' '), CHAR(10), ' ')
END
