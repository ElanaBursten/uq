use QM;
select work_extent, work_city, con_name,work_date,
transmit_date,'udf07' as 'excavator_caller_cell',caller_phone,work_cross,
due_date,work_type,'udf01' as 'work_address_street' ,work_county,caller_email,'udf08' as 'highrisk',
t.con_address as work_address, caller_altcontact,company as 'excavator_company',ticket_number,
work_state,'udf06' as 'excavator_caller','udf09'as 'abnormalOperatingCondition' ,
company as 'work_company','udf00' as 'work_address_number' ,th.form_name,'udf10' as 'type',
'udf11' as 'contact_phone', th.insert_date,'NJNG' as 'site','NJN' as 'org' ,
'UTILIQUEST' as 'sourcesysid','udf12' as 'short_name','00000'as 'postalcode', (select l.client_code, c.client_name
																				from locate l
																				join client c on (c.[oc_code] = l.client_code)
																				where l.ticket_id =t.ticket_id 
																				for JSON PATH, root('Operators')) as 'Locate',

																				(SELECT [note]
																				  FROM [QM].[dbo].[notes]
																				  where foreign_id =t.ticket_id 
																				  and sub_type<200
																				  and active =1
																				  for JSON PATH ) as 'Notes'
from ticket t
join tema_header th on th.ticket_id = t.ticket_id
where t.ticket_id = 143588532
for JSON PATH, ROOT('Ticket') 
--list of AOC condition
 --select * from work_order_work_type where wo_source like '%NJN%'


'{'+
'    "Ticket": {'+
'    "work_extent": "%s",'+ ticket.work_extent
'    "work_city": "%s",'+ ticket.work_city
'    "con_name": "%s",'+ ticket.con_name
'    "work_date": "%s",'+ ticket.work_date
'    "transmit_date": "%s",'+ ticket.transmit_date
'    "excavator_caller_cell": "%s",'+ --udf07
'    "excavator_caller_phone":"%s",'+  ticket.caller_phone
'    "work_cross": "%s",'+ ticket.work_cross
'    "due_date": "%s",'+ ticket.due_date
'    "work_type": "%s",'+ ticket.work_type
'    "work_address_street": "%s",'+  --udf01
'    "work_county": "s",'+ ticket.work_county
'    "excavator_caller_email": "%s",'+ ticket.caller_email
'    "highrisk": "%s",'+    --udf08
'    "work_address":"%s",'+ ticket.con_address
'    "work_phone":"%s",'+ ticket.caller_altcontact
'    "excavator_company":"%s",'+ ticket.company
'    "ticket_number": "%s",'+ ticket.ticket_number
'    "work_state": "%s",'+ ticket.work_state
'    "excavator_caller":"%s",'+   --udf06
'    "abnormalOperatingCondition": "%s",'+ --udf09
'    "work_company": "%s",'+ ticket.company
'    "work_address_number": "%s"'+  --udf00
'  },'+
'  "sub_work_type": "%s",'+ tema_header.form_name
'  "type": "%s",'+  --udf10 only 1 AOC ITEM!
'  "date": "%s",'+ tema_header.insert_date
'  "site": "%s",'+ 'NJNG'
'  "org": "%s",'+  'NJN'
'  "sourcesysid": "%s",'+ 'UTILIQUEST'
'  "Employee": {'+
'    "contact_phone": "%s",'+  --udf11
'    "short_name": "%s"'+  --udf12
'  }, '+
'  "postalcode": "%s",'+ '00000'
'   "Locate": {'+
'      "Operators": ['+
'         {'+
'            "client_description": "%s",'+  client.client_name
'            "client_code": "%s"'+  locate.client_code
'         }'+
'      ]'+
'   },'+
'   "Notes": {'+
'      "note": "%s"'+  notes.note -- (just pulic notes)
'   },'+
'   "status": "%s"'+ 'NEW'
'}';


select l.client_code, c.client_name
from locate l
join client c on (c.[oc_code] = l.client_code)
where l.ticket_id =143588532 
for JSON PATH, ROOT('Operators');

{
  "Operators": [
    {
      "client_code": "NJN",
      "client_name": "New Jersey Natural Gas (NJN)"
    },
    {
      "client_code": "ADC",
      "client_name": "Comcast (ADC)"
    },
    {
      "client_code": "AE1",
      "client_name": "Atlantic City Electric (AE1)"
    }
  ]
}




{
  "date": "string",
  "Ticket": {
			"excavator_caller_phone": "string",
			"work_extent": "string",
			"work_city": "string",
			"con_name": "string",
			"work_date": "string",
			"transmit_date": "string",
			"excavator_caller_cell": "string",
			"work_cross": "string",
			"due_date": "string",
			"work_type": "string",
			"work_phone": "string",
			"work_address_street": "string",
			"work_county": "string",
			"excavator_caller_email": "string",
			"highrisk": "string",
			"ticket_number": "string",
			"work_company": "string",
			"abnormalOperatingCondition": "string",
			"excavator_caller": "string",
			"work_state": "string",
			"work_address": "string",
			"work_address_number": "string",
			"excavator_company": "string"
		 },
  "site": "string",
  "sub_work_type": "string",
  "Employee": {
			"contact_phone": "string",
			"short_name": "string"
			  },
  "org": "string",
  "postalcode": "string",
  "Locate": {
			"Operators": [
			  {
				"client_description": "string",
				"client_code": "string"
			  }
			]
			},
  "type": "string",
  "sourcesysid": "string",
  "Notes": {
			"note": "string"
			},
  "status": "string"
}
