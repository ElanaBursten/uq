USE [QM]
GO

/****** Object:  Table [dbo].[rabbitmq_admin]    Script Date: 8/15/2024 5:04:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[rabbitmq_admin](
	[mq_admin_id] [int] IDENTITY(1,1) NOT NULL,
	[queue_name] [varchar](50) NOT NULL,
	[Process_App] [varchar](50) NOT NULL,
	[eMail_List] [varchar](120) NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NULL,
	[host] [nvarchar](50) NULL,
	[port] [int] NULL,
	[virtual_host] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[use_ssl] [bit] NOT NULL,
	[exchange] [nvarchar](50) NULL,
	[routing_key] [nvarchar](50) NULL,
	[readlimit] [int] NULL,
	[requeue] [bit] NOT NULL,
 CONSTRAINT [PK_rabbitmq_admin] PRIMARY KEY CLUSTERED 
(
	[queue_name] ASC,
	[Process_App] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[rabbitmq_admin] ADD  CONSTRAINT [DF_rabbitmq_admin_active]  DEFAULT ((0)) FOR [active]
GO

ALTER TABLE [dbo].[rabbitmq_admin] ADD  CONSTRAINT [DF_rabbitmq_admin_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO

ALTER TABLE [dbo].[rabbitmq_admin] ADD  CONSTRAINT [DF_rabbitmq_admin_use_ssl]  DEFAULT ((0)) FOR [use_ssl]
GO

ALTER TABLE [dbo].[rabbitmq_admin] ADD  CONSTRAINT [DF_rabbitmq_admin_requeue]  DEFAULT ((0)) FOR [requeue]
GO

