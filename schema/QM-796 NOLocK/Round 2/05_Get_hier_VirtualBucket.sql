-----------------------------------------------
--QM-796 Adding a couple of NoLOCKS to improve speed
-- QM-486: get_hier_virtualbucket new function
-- Pulls only OPEN tickets that meet the critera specified in the virtual buckets
-- Limit of 10 filters per row
-- EB
-----------------------------------------------

if object_id('dbo.get_hier_virtualbucket') is not null
	drop function dbo.get_hier_virtualbucket
go

create function get_hier_virtualbucket(@ID int, @bucketname varchar(15), @managers_only bit, @level_limit int)
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer,
  vb_tkt_count integer default 0,
  vb_loc_count integer default 0
)
as
begin
  declare @adding_level int
  select @adding_level = 1

 
  DECLARE @vb_tkt_counts TABLE (
  emp_id integer NOT NULL,
  ticket_count integer default 0,
  loc_count integer default 0
  )

  --Get the flags from virtual_bucket settings
  DECLARE @Today DateTime
  DECLARE @Tomorrow Datetime
  DECLARE @DaysBack Integer
  DECLARE @OnlyPastDueFlag Integer
  DECLARE @OnlyTodayFlag Integer
  DECLARE @TodayStartAsDec float 
  DECLARE @TomorrowEndAsDec float
  DECLARE @OnlyPastFlagDate Datetime
  DECLARE @TodayStartMod datetime
  DECLARE @TomorrowEndMod datetime
  DECLARE @FIVEYEARS float   
    Set @FIVEYEARS = 1825

  SELECT top 1 @DaysBack=daysback, 
             @OnlyPastDueFlag=CAST(only_pastdue AS Int),
			 @OnlyTodayFlag=CAST(only_today AS Int)
     FROM virtual_bucket with (nolock)
     WHERE (manager_id = @ID) and 
	       (UPPER(bucket_name) = UPPER(@BucketName)) and ([active]=1)

	--This will set the range midnight today - midnight tomorrow
  Set @Tomorrow= DATEADD(day, DATEDIFF(day,0,GETDATE())+1,0)
  Set @Today = DATEADD(day, DATEDIFF(day,0,GETDATE()),0) 

  Set @TodayStartAsDec=CAST(@Today as FLOAT) - (@FIVEYEARS - (@FIVEYEARS * @OnlyTodayFlag))
  Set @TomorrowEndAsDec=CAST(@Tomorrow as FLOAT) + ((@FIVEYEARS)-(@FIVEYEARS * @OnlyTodayFlag))
                 
  Set @TodayStartMod= CAST(@TodayStartAsDec as Datetime)  -- adjust dates to whether flag is used
  Set @TomorrowEndMod=CAST(@TomorrowEndAsDec as datetime)

  Set @OnlyPastFlagDate = GetDate() + CAST((@FIVEYEARS - (@FIVEYEARS * @OnlyPastDueFlag)) as Datetime)
  

DECLARE @DateUsed datetime
SET @DateUsed = GetDate()
  

  DECLARE @Co TABLE (
      CoID integer not null,
      CompanyStr varchar(100) not null
	  )

  INSERT into @Co
  SELECT id, val
  FROM virtual_bucket v22 with (nolock)
  CROSS APPLY split_del_string(v22.cofilter, '|')
  where v22.manager_id = @ID and v22.bucket_name = @bucketname
  

  DECLARE @C1 varchar(100)
  DECLARE @C2 varchar(100)
  DECLARE @C3 varchar(100)
  DECLARE @C4 varchar(100)
  DECLARE @C5 varchar(100)
  DECLARE @C6 varchar(100)
  DECLARE @C7 varchar(100)
  DECLARE @C8 varchar(100)
  DECLARE @C9 varchar(100)
  DECLARE @C10 varchar(100)

  SET @C1 = (select CompanyStr from @Co where COId = 1)
  SET @C2 = (select CompanyStr from @Co where COId = 2)
  SET @C3 = (select CompanyStr from @Co where COId = 3)
  SET @C4 = (select CompanyStr from @Co where COId = 4)
  SET @C5 = (select CompanyStr from @Co where COId = 5)
  SET @C6 = (select CompanyStr from @Co where COId = 6)
  SET @C7 = (select CompanyStr from @Co where COId = 7)
  SET @C8 = (select CompanyStr from @Co where COId = 8)
  SET @C9 = (select CompanyStr from @Co where COId = 9)
  SET @C10 = (select CompanyStr from @Co where COId = 10)

 
  DECLARE @temp TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer
)

  INSERT INTO @temp
  Select * from get_hier2_active(@ID, @managers_only, @level_limit)


  INSERT into @vb_tkt_counts (emp_id, ticket_count, loc_count)
    (SELECT distinct e.emp_id, COALESCE(count(distinct tkt.ticket_id), 0),  
	                           COALESCE(count(distinct loc.locate_id), 0)
  from @temp e
  inner join [locate] loc with (nolock)
    on e.emp_id = loc.assigned_to
  inner join ticket tkt with (nolock) 
    on tkt.ticket_id = loc.ticket_id
  where  (loc.closed = 0)  AND
         (loc.active = 1) AND (loc.status <> '-N') AND (loc.status <> '-P') AND
         (tkt.transmit_date > (@DateUsed - @DaysBack)) AND  --Get tickets this far back
	     ((tkt.due_date >=@TodayStartMod) AND (tkt.due_date < @TomorrowEndMod)) AND
	     (tkt.due_date < @OnlyPastFlagDate) AND
     (
		     (@C1  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C1 + '[^a-z]%')))
		  OR (@C2  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C2 + '[^a-z]%')))
          OR (@C3  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C3 + '[^a-z]%')))
          OR (@C4  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C4 + '[^a-z]%')))
		  OR (@C5  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C5 + '[^a-z]%')))
		  OR (@C6  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C6 + '[^a-z]%')))
		  OR (@C7  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C7 + '[^a-z]%')))
		  OR (@C8  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C8 + '[^a-z]%')))
		  OR (@C9  IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C9 + '[^a-z]%')))
		  OR (@C10 IS NOT NULL AND ('.' + tkt.company + '.' like ('%[^a-z]' + @C10 + '[^a-z]%')))
		) 
		  group by e.emp_id)
	 
  
  insert into @results(
  emp_id,short_name,first_name, last_name,
  emp_number, etype_id, report_level,
  report_to, vb_tkt_count, vb_loc_count)
  
  select e.emp_id, e.short_name,e.first_name, e.last_name,
  e.emp_number, e.etype_id, e.report_level,
  e.report_to, tdt.ticket_count, tdt.loc_count
  from @temp e
   left outer join @vb_tkt_counts tdt on e.emp_id = tdt.emp_id

  RETURN
end

GO

--select * from get_hier_virtualbucket(manager ID, bucket name, managers only, limit)
--select * from get_hier_virtualbucket(1191, 'Verizon',0, 25)
--select * from virtual_bucket


