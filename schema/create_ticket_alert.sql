
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE
TABLE_NAME = 'alert_ticket_type')
-- One row for each county map page
  create table alert_ticket_type (
    alert_ticket_type_id integer NOT NULL identity (100000, 1)
         primary key,
    call_center varchar(10) NOT NULL,
    ticket_type varchar(50) NOT NULL, -- For what ticket types will the keyword apply
    active bit NOT NULL DEFAULT 1     -- To turn off or on
)
go

IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE
TABLE_NAME = 'alert_ticket_keyword')
  create table alert_ticket_keyword (
    alert_ticket_keyword_id integer NOT NULL identity (11000, 1)
         primary key,
    alert_ticket_type_id integer NOT NULL,
    keyword varchar(100) NOT NULL
)
go
IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE
TABLE_NAME = 'alert_ticket_keyword')
  create index alert_ticket_keyword_ticket_type on alert_ticket_keyword
  (alert_ticket_type_id)
go

-- foreign keys
IF NOT EXISTS(SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE
CONSTRAINT_NAME = 'alert_ticket_keyword_fk_ticket_type')
  alter table alert_ticket_keyword
    add constraint alert_ticket_keyword_fk_ticket_type
    foreign key (alert_ticket_type_id) references alert_ticket_type(alert_ticket_type_id)
go
