/*
  This alter requires a multi-part ALTER.  
  1.  The active column as a bit field is added.  Nulls are allowed.
  2.  An update is run to set all the active values to 1.  Now, none are Null.
  3.  All the table constraints are dropped and a temp table created/populated with the table data. 
  4.  Active constraint as not null
  5.  Data restored to Area table
  6.  Constraints are restored.
   Wednesday, December 2, 202010:33:17 AM
   User: 
   Server: SSDS-UTQ-QM-02
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.area ADD   --adds column allows NULLs
	active bit NULL
GO
ALTER TABLE dbo.area SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.area', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.area', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.area', 'Object', 'CONTROL') as Contr_Per 

Update dbo.area         --updates active column to 1
set active = 1
GO
-- the following constraints has to be dropped and a temp table created to allow setting active to Not Null   
ALTER TABLE dbo.area            
	DROP CONSTRAINT area_fk_map
GO
ALTER TABLE dbo.map SET (LOCK_ESCALATION = TABLE)
GO
--COMMIT
select Has_Perms_By_Name(N'dbo.map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.area
	DROP CONSTRAINT area_fk_employee
GO
ALTER TABLE dbo.employee SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.employee', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.employee', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.area
	DROP CONSTRAINT DF__area__modified_d__3A2D78C3
GO
CREATE TABLE dbo.Tmp_area
	(
	area_id int NOT NULL IDENTITY (400, 1),
	area_name varchar(40) NOT NULL,
	map_id int NOT NULL,
	locator_id int NULL,
	modified_date datetime NOT NULL,
	centroid_lat varchar(14) NULL,
	centroid_lng varchar(14) NULL,
	active bit NOT NULL            --sets to not null on temp table
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_area SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_area ADD CONSTRAINT
	DF__area__modified_d__3A2D78C3 DEFAULT (getdate()) FOR modified_date
GO
ALTER TABLE dbo.Tmp_area ADD CONSTRAINT
	DF_area_active DEFAULT 1 FOR active
GO
SET IDENTITY_INSERT dbo.Tmp_area ON
GO
IF EXISTS(SELECT * FROM dbo.area)
	 EXEC('INSERT INTO dbo.Tmp_area (area_id, area_name, map_id, locator_id, modified_date, centroid_lat, centroid_lng, active)
		SELECT area_id, area_name, map_id, locator_id, modified_date, centroid_lat, centroid_lng, active FROM dbo.area WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_area OFF
GO
ALTER TABLE dbo.map_cell
	DROP CONSTRAINT map_cell_fk_area
GO
ALTER TABLE dbo.county_area
	DROP CONSTRAINT county_area_fk_area
GO
DROP TABLE dbo.area
GO
EXECUTE sp_rename N'dbo.Tmp_area', N'area', 'OBJECT' 
GO
ALTER TABLE dbo.area ADD CONSTRAINT
	PK__area__182C9B23 PRIMARY KEY CLUSTERED 
	(
	area_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ix_area_mapd_id_modified_date ON dbo.area
	(
	map_id,
	modified_date
	) INCLUDE (area_id, area_name, locator_id, centroid_lat, centroid_lng) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.area WITH NOCHECK ADD CONSTRAINT
	area_fk_employee FOREIGN KEY
	(
	locator_id
	) REFERENCES dbo.employee
	(
	emp_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.area WITH NOCHECK ADD CONSTRAINT
	area_fk_map FOREIGN KEY
	(
	map_id
	) REFERENCES dbo.map
	(
	map_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
create trigger area_u on dbo.area
for update
AS
begin
  update area set modified_date = getdate()
   where area_id in (select area_id from inserted)
end
GO
COMMIT
select Has_Perms_By_Name(N'dbo.area', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.area', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.area', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.county_area WITH NOCHECK ADD CONSTRAINT
	county_area_fk_area FOREIGN KEY
	(
	area_id
	) REFERENCES dbo.area
	(
	area_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.county_area SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.county_area', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.map_cell WITH NOCHECK ADD CONSTRAINT
	map_cell_fk_area FOREIGN KEY
	(
	area_id
	) REFERENCES dbo.area
	(
	area_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.map_cell SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.map_cell', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.map_cell', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.map_cell', 'Object', 'CONTROL') as Contr_Per 