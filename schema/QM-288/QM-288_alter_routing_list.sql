/*
   Wednesday, December 2, 202011:27:18 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.routing_list ADD
	active bit NULL
GO
ALTER TABLE dbo.routing_list SET (LOCK_ESCALATION = TABLE)
GO
select Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'CONTROL') as Contr_Per 
update dbo.routing_list
set active=1
GO
CREATE TABLE dbo.Tmp_routing_list
	(
	routing_list_id int NOT NULL IDENTITY (9000, 1),
	call_center varchar(20) NOT NULL,
	field_name varchar(25) NOT NULL,
	field_value varchar(80) NOT NULL,
	area_id int NOT NULL,
	active bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_routing_list SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_routing_list ON
GO
IF EXISTS(SELECT * FROM dbo.routing_list)
	 EXEC('INSERT INTO dbo.Tmp_routing_list (routing_list_id, call_center, field_name, field_value, area_id, active)
		SELECT routing_list_id, call_center, field_name, field_value, area_id, active FROM dbo.routing_list WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_routing_list OFF
GO
DROP TABLE dbo.routing_list
GO
EXECUTE sp_rename N'dbo.Tmp_routing_list', N'routing_list', 'OBJECT' 
GO
ALTER TABLE dbo.routing_list ADD CONSTRAINT
	PK_routing_list PRIMARY KEY CLUSTERED 
	(
	routing_list_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.routing_list', 'Object', 'CONTROL') as Contr_Per 