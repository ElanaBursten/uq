/*
   Wednesday, December 2, 202011:08:48 AM
   User: 
   Server: SSDS-UTQ-QM-02-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.county_area ADD
	active bit NULL
GO
ALTER TABLE dbo.county_area SET (LOCK_ESCALATION = TABLE)
GO
select Has_Perms_By_Name(N'dbo.county_area', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'CONTROL') as Contr_Per 
update dbo.county_area
set active = 1 
GO
CREATE TABLE dbo.Tmp_county_area
	(
	county_area_id int NOT NULL IDENTITY (900, 1),
	map_id int NULL,
	state varchar(2) NOT NULL,
	county varchar(40) NOT NULL,
	munic varchar(40) NOT NULL,
	area_id int NULL,
	call_center varchar(20) NULL,
	active bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_county_area SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_county_area ON
GO
IF EXISTS(SELECT * FROM dbo.county_area)
	 EXEC('INSERT INTO dbo.Tmp_county_area (county_area_id, map_id, state, county, munic, area_id, call_center, active)
		SELECT county_area_id, map_id, state, county, munic, area_id, call_center, active FROM dbo.county_area WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_county_area OFF
GO
DROP TABLE dbo.county_area
GO
EXECUTE sp_rename N'dbo.Tmp_county_area', N'county_area', 'OBJECT' 
GO
ALTER TABLE dbo.county_area ADD CONSTRAINT
	PK__county_area__3587F3E0 PRIMARY KEY CLUSTERED 
	(
	county_area_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX county_area_stuff ON dbo.county_area
	(
	state,
	county,
	munic
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
select Has_Perms_By_Name(N'dbo.county_area', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.county_area', 'Object', 'CONTROL') as Contr_Per 