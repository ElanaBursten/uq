USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[close_all_locates_for_locator]    Script Date: 10/27/2020 2:28:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[close_all_locates_for_locator] (
	@LocatorID int,
	@Status varchar(20),
	@Billed bit,
	@Excludes varchar(max)=null,  --QMANTWO-799
	@OverRide varchar(20)=null --QM-262 sr

)
as

set nocount on
--=================================================================================Begin Test Params
--declare @LocatorID int,            --test
--		@Status    varchar(20),
--		@Billed    bit,
--		@Excludes  varchar(max),
--		@OverRide varchar(20);

--set @LocatorID = 23910
--set @Status    = 'M'
--set @Billed    = 1
--set @Excludes  = 'ATTDSOUTH, USCE47, UVZHTB'
--set @OverRide = 'USCENC=OS'
--=================================================================================End Test Params
Declare @ovrdClient varchar(12),
@ovrdStatus varchar(12)

declare @ovrdList table (              --QM-262 sr
	ovrdList varchar(16)
)

declare @exclsTab table (              --QMANTWO-799
	excludeClients varchar(16)
)

--=================================================================================================
If @OverRide is not null
begin
  insert into @ovrdList
  select * from [dbo].StringSplit (@OverRide,'=')  --QM-262 sr
  set @ovrdClient = (select top 1 * from @ovrdList)
  set @ovrdStatus = (select * from @ovrdList where ovrdList not in (Select @ovrdClient)) 

  Insert into @exclsTab
  values(@ovrdClient)
end
--=================================================================================================
declare @ltoc table (
	locate_id int not null primary key
)



insert into @exclsTab
select * from [dbo].StringSplit (@Excludes,',')  --QMANTWO-799

--select excludeClients from @exclsTab    --test works


insert into @ltoc
select distinct locate.locate_id
 from assignment
 inner join locate on locate.locate_id=assignment.locate_id
 where locator_id=@LocatorID
  and locate.closed=0 
  and assignment.active=1
  

--select count(*) as LocatesToProcess from @ltoc --test

declare @closed_date_now datetime
select @closed_date_now = getdate()

declare @loc integer
DECLARE LocateCursor CURSOR FOR
select locate_id from @ltoc

OPEN LocateCursor
FETCH NEXT FROM LocateCursor into @loc
WHILE @@FETCH_STATUS = 0
BEGIN
  -- Close this one
  update locate
   set status=@Status,
       closed=1,
       closed_by_id=assigned_to,
       closed_how='Bulk',
       closed_date=@closed_date_now,
	   entry_date=@closed_date_now, 
       qty_marked=1,
       invoiced=@Billed
  where locate_id = @loc
  and locate.client_code not in (select excludeClients from @exclsTab)  --QMANTWO-799
  
  If @OverRide is not null --QM-249
  update locate   --QM-249
   set status=@ovrdStatus,   
       closed=1,   
       closed_by_id=assigned_to,  
       closed_how='Bulk',  
       closed_date=@closed_date_now,  
	   entry_date=@closed_date_now,  
       qty_marked=1, 
       invoiced=@Billed 
  where locate_id = @loc
  and locate.client_code = @ovrdClient

  FETCH NEXT FROM LocateCursor into @loc
END

