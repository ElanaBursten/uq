
/****** Object:  Trigger [dbo].[trigger_oq_data_history]    Script Date: 8/13/2021 11:25:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------rtasq table triggers

/****** Object:  Trigger [dbo].[rtasq_ticket_queue_return_history_uid]    Script Date: 6/16/2021 11:32:44 AM ******/
CREATE TRIGGER [dbo].[trigger_oq_data_history]
    ON [dbo].[oq_data]
AFTER INSERT, UPDATE, DELETE
AS

SET NOCOUNT ON;

  --inserted Statement was executed
  INSERT INTO oq_data_history (
           oq_data_id
		   ,[ref_id]
           ,[emp_id]
           ,[qualified_date]
           ,[expired_date]
           ,[modified_by]
           ,[status]
           ,[active]
           ,[modified_date]
		   ,[history_date])
     select oq_data_id
		   ,[ref_id]
           ,[emp_id]
           ,[qualified_date]
           ,[expired_date]
           ,[modified_by]
           ,[status]
           ,[active]
           ,[modified_date]
		   ,getdate() as [history_date]
	from inserted i


GO

ALTER TABLE [dbo].[oq_data] ENABLE TRIGGER [trigger_oq_data_history]
GO


