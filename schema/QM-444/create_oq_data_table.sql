IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='oq_data' AND xtype='U')
CREATE TABLE dbo.oq_data(
	oq_data_id int IDENTITY(1,1) NOT NULL,
	ref_id int NULL,
	emp_id int NULL,
	qualified_date datetime NULL,
	expired_date datetime NULL,
	modified_by int NULL,
	[status] varchar(20) NULL,
	active bit NULL,
	modified_date datetime NULL
)


