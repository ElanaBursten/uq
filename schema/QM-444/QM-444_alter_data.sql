--QM-444 Load new values into reference table oq_data

select * from reference


INSERT INTO [dbo].[reference]
           ([type],    [code],         [description], [sortby], [active_ind] ,[modified_date], [modifier])
     VALUES
           ('oq_data', 'Disqualified', 'Disqualified', 0,        1,            getdate(),         0)

INSERT INTO [dbo].[reference]
           ([type],    [code],         [description], [sortby], [active_ind] ,[modified_date], [modifier])
     VALUES
           ('oq_data', 'Not Qualified', 'Not Qualified', 0,        1,            getdate(),         0)

INSERT INTO [dbo].[reference]
           ([type],    [code],         [description], [sortby], [active_ind] ,[modified_date], [modifier])
     VALUES
           ('oq_data', 'Qualified', 'Qualified',       0,        1,            getdate(),         1)

INSERT INTO [dbo].[reference]
           ([type],    [code],         [description], [sortby], [active_ind] ,[modified_date], [modifier])
     VALUES
           ('oq_data', 'Suspended', 'Suspended',       0,        1,            getdate(),         0)
GO

