--QM-444 OQ Management
if not exists(select * from right_definition where entity_data='MainShowOQList')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Main - Show Employee Qualification status (OQ)', 
						'General', 
						'MainShowOQList', 
						'Limitation does not apply to this right', 
						GetDate());
GO






