--QM-444 Returns valid OQ qualifications Employees under a manager

if object_id('dbo.get_oq_data_mgr') is not null
  drop procedure dbo.get_oq_data_mgr
go


create procedure dbo.get_oq_data_mgr(@MgrID integer, @IncExp integer)
as
 declare @oqtable TABLE (
  emp_id integer NOT NULL,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  qualified_date datetime,
  expired_date datetime,
  oq_description varchar(100),
  oq_modifier varchar(20),
  oq_status varchar(20),
  report_level integer,
  report_to integer,
  is_qual varchar(20)
)

  DECLARE @emplist TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer
)

  --declare @adding_level int
  --select @adding_level = 1


  INSERT INTO @emplist
  Select * from get_hier2_active(@MgrID, 0, 10)

insert into @oqtable(
                     emp_id,
					 short_name,
                     first_name,
                     last_name,
                     emp_number,
                     qualified_date,
                     expired_date,
                     oq_description,
                     oq_modifier,
                     oq_status,
                     report_level,
                     report_to,
					 is_qual)

  (           select oq.emp_id, 
                     el.short_name,
					 el.first_name,
					 el.last_name, 
					 el.emp_number,
                     oq.qualified_date, 
					 oq.expired_date, 
					 r.description, 
					 r.modifier, 
					 oq.status,
					 el.report_level,
					 el.report_to,
					 r2.modifier
  from @emplist el
  inner join oq_data oq on el.emp_id = oq.emp_id
  inner join reference r on oq.ref_id = r.ref_id
  inner join reference r2 on oq.status = r2.code
  where (oq.emp_id = el.emp_id) and
        (oq.qualified_date <= GetDate()) and
	    (oq.expired_date > (SELECT CAST(GETDATE() AS Date))) and
	    (r2.type = 'oq_data') and
	    (UPPER(oq.status) = UPPER(r2.code)) and
	    (oq.active = 1)
)

select 'oqList' as tname, * from @oqtable order by last_name, first_name -- for XML AUTO


--select * from reference where type = 'dmgco'
--select * from reference where type = 'oq_data'
--get_oq_data_employee 4736

--select * from oq_data

 -- exec get_oq_data_mgr 1246, 0
  --Select * from get_hier2_active(1246,0, 10)

