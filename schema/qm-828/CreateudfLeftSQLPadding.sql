USE [UQVIEW]
GO

/****** Object:  UserDefinedFunction [dbo].[udfLeftSQLPadding]    Script Date: 1/17/2024 8:55:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udfLeftSQLPadding] (
  @String NVARCHAR(MAX),
  @Length INT,
  @PaddingChar CHAR(1) = '0'
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

RETURN (
  SELECT RIGHT(Replicate(@PaddingChar, @Length) + @String, @Length)
)

END
GO

