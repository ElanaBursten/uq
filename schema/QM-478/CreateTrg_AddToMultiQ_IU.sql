USE [QM]
GO
/****** Object:  Trigger [dbo].[AddToMultiQ_IU]    Script Date: 11/9/2021 8:13:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Larry Killen
-- Create date: 11/08/2021
-- Description:	QM-478 adds record to multi Q if Reschedule
-- =============================================
ALTER TRIGGER [dbo].[AddToMultiQ_IU] 
   ON  [dbo].[ticket_info]
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    insert into [dbo].[responder_multi_queue]([respond_to], [locate_id], [insert_date], [ticket_format],[client_code])
	Select top 1  'boss811_nipsco' respond_to, L.locate_id, GetDate() insert_date,  t.ticket_format, L.client_code
	From ticket t
	left join inserted I on (I.ticket_id= T.ticket_id)
	left join locate L on (t.ticket_ID=L.ticket_id)
	left join client c  on (L.client_id = c.client_id) 
	where t.ticket_format = '1851'
    and I.info_type= 'RSREASON'
	and L.status in ('O','OI','OP')
	and c.utility_type='ugas'

END
