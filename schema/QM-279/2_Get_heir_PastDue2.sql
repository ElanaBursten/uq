--QM_279 Past Due Rollup
--QM-133 PART 2 Pulls the past due ticket/locate counts for a particular manager/supervisor (sum includes all employees below).
--   * Used in multi_open_totals6 - past due
--   * calls get_hier2_active

if object_id('dbo.get_hier_pastdue2') is not null
	drop function dbo.get_hier_pastdue2
go

create function get_hier_pastdue2(@id int, @managers_only bit, @level_limit int)
returns
 @results TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer,
  pastdue_tkt_count integer default 0,
  pastdue_loc_count integer default 0
)
as
begin
  declare @adding_level int
  select @adding_level = 1

 
  DECLARE @pastdue_tkt_counts TABLE (
  emp_id integer NOT NULL,
  ticket_count integer default 0,
  loc_count integer default 0
  )


  DECLARE @temp TABLE (
  emp_id integer NOT NULL PRIMARY KEY,
  short_name varchar(30) NULL,
  first_name varchar(30) NULL,
  last_name varchar(30) NULL,
  emp_number varchar(20) NULL,
  etype_id integer,
  report_level integer,
  report_to integer
)

  INSERT INTO @temp
  Select * from get_hier2_active(@ID, @managers_only, @level_limit)

  insert into @pastdue_tkt_counts (emp_id, ticket_count, loc_count)
  (select distinct e.emp_id, COALESCE(count(distinct tkt.ticket_id), 0), COALESCE(count(distinct loc.locate_id),0)
  from @temp e
  inner join [locate] loc on e.emp_id = loc.assigned_to
  inner join ticket tkt on tkt.ticket_id = loc.ticket_id
  where (tkt.due_date < GetDate()) and
  (loc.active = 1) and (loc.status <> '-N') and (loc.status <> '-P')
  group by e.emp_id)

  insert into @results(
  emp_id,short_name,first_name, last_name,
  emp_number, etype_id, report_level,
  report_to, pastdue_tkt_count, pastdue_loc_count)
  --values(1, 'rhgith','thyg','yrjtuh','5768576',2,3,4,1,345)
  select e.emp_id, e.short_name,e.first_name, e.last_name,
  e.emp_number, e.etype_id, e.report_level,
  e.report_to, pdt.ticket_count, pdt.loc_count
  from @temp e
   left outer join @pastdue_tkt_counts pdt on e.emp_id = pdt.emp_id

  RETURN
end

GO


