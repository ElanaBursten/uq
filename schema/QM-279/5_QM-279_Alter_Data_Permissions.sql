--QM_279 Past Due Rollup
--QM-133 Past Due Permissions
--QM-133  Add Permission for Past Due


if not exists(select * from right_definition where entity_data='TicketsPastDueTree')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - Show Past Due', 'General', 'TicketsPastDueTree', 'Limitation does not apply to this right', GetDate());
GO

select * from right_definition order by right_description

