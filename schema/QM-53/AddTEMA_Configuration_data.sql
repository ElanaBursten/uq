--QM-53 TEMA (URL Link) button for SCA1

--Production:
if exists(select * from Configuration_data where name ='TEMABaseUrl')
  Delete from Configuration_data where name = 'TEMABaseUrl'

Insert into Configuration_data(name, value, editable, description)
                         values('TEMABaseUrl', 'https://qm.utiliquest.com/tema/Default?', 1, 'TEMA for SCA1');


--Dev:
--Insert into Configuration_data(name, value, editable, description)
--                         values('TEMABaseUrl', 'https://qm-dev.utiliquest.com/tema/Default?', 1, 'DEV: TEMA for SCA1');


--Select * from Configuration_data where name like 'TEMABaseUrl'
