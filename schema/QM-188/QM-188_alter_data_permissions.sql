--QM-188 Add permission for deleting note and change restriction on a note (for managers/supervisors) 

if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'NotesDeleteNote')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Notes - Delete Note', 'General', 'NotesDeleteNote', 'No limitation')

  if not exists (select rd.right_id from right_definition rd where rd.entity_data = 'NotesChangeRestriction')
  insert into right_definition (right_description, right_type, entity_data, modifier_desc)
  values ('Notes - Change Restriction Public/Private', 'General', 'NotesChangeRestriction', 'No limitation')

 --ROLLBACK (Only if the right has yet to be used)
  --DELETE FROM right_definition WHERE entity_data = 'NotesChangeRestriction'
  --DELETE FROM right_definition WHERE entity_data = 'NotesDeleteNote'
  --DELETE FROM right_definition WHERE entity_data = 'NotesChangeNoteText'


  Select * from right_definition Where entity_data = 'NotesDeleteNote'
  Select * from employee_right 
  Where right_id = (Select right_id From right_definition 
                     Where entity_data = 'NotesDeleteNote') --NotesDeleteNote


  Select * from right_definition Where entity_data = 'NotesChangeRestriction'
  Select * from employee_right 
  Where right_id = (Select right_id From right_definition 
                     Where entity_data = 'NotesChangeRestriction') -- 'NotesChangeRestriction'








