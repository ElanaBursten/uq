--QMANTWO-775 Route Order
--QMANTWO-775 Note:  Change procedure to call new function which has the route orders added to the list.

if object_id('dbo.get_ticket_list2') is not null
  drop procedure dbo.get_ticket_list2
go

create proc get_ticket_list2 (@emp_id int, @open_only bit, @num_days int, @unacked bit)
as
  if @open_only = 1
    exec dbo.get_open_tickets_for_locator_RO @emp_id
  else begin
    if @unacked = 1 /*  Unacknowledged Tickets */
      exec dbo.get_unacked_tickets_3 @emp_id
    else  /* Last X Days */
      exec dbo.get_old_tickets_for_locator2 @emp_id, @num_days
  end
GO
grant execute on get_ticket_list2 to uqweb, QManagerRole

/*
exec dbo.get_ticket_list2 766, 0, 7, 0
exec dbo.get_ticket_list2 1342, 0, 15, 0
*/


