--QMANTWO-775  Route Order Override  (EB create 7/15/2019)
--QMANTWO-775 Note: Adds Route Order Permissions


-----------------------------------------------
--Permissions - Use to turn off and on
-----------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsUseRouteOrder')
  INSERT INTO right_definition(right_description, right_type, entity_data, modifier_desc, modified_date)
                        values('Tickets - Use Route Order', 'General', 'TicketsUseRouteOrder', 'Limitation does not apply to this right.', GetDate());
GO


--select * from reference where type = 'routeord'


