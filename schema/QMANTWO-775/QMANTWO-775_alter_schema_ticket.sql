-- QMANTWO-775 Note: Adds route_order column to ticket  (EB)

if not exists (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where
  TABLE_NAME = 'ticket' and COLUMN_NAME='route_order')
  ALTER TABLE ticket
    ADD route_order decimal(9,5);

--select * from ticket
