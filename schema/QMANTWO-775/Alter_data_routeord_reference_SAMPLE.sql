--QMANTWO-775  Route Order Override  (EB create 7/15/2019)
--QMANTWO-775 Note: Added routeord type to the reference table (these should be updated to reflect the desired reasons for route order skip)
--Must use the routeord type to display in QManager client

-----------------------------------------------
--Route Order Reason (reference table: routeord)
-----------------------------------------------
--Route Order Reason = Skipped Ticket was too far 
--                     Outstanding Questions on Skipped Ticket
--                     Current Ticket was higher priority
--                     Current Ticket Turnaround was quicker
--                     Updated after I started driving

if not exists(select * from reference where type='routeord' and code='TOO FAR')
  INSERT INTO reference(type, code, description) values ('routeord', 'TOO FAR', 'Skipped Ticket was too far')
if not exists(select * from reference where type='routeord' and code='QUESTIONS')
  INSERT INTO reference(type, code, description) values ('routeord', 'QUESTIONS', 'Outstanding Questions on Skipped Ticket')
if not exists(select * from reference where type='routeord' and code='INCORRECT ORD')
  INSERT INTO reference(type, code, description) values ('routeord', 'INCORRECT ORD', 'Current Ticket was higher priority')
if not exists(select * from reference where type='routeord' and code='TURNAROUND')
  INSERT INTO reference(type, code, description) values ('routeord', 'TURNAROUND', 'Current Ticket Turnaround was quicker')
if not exists(select * from reference where type='routeord' and code='AFTER UPDATE')
  INSERT INTO reference(type, code, description) values ('routeord', 'AFTER UPDATE', 'Updated after I started driving')

GO

--select * from reference where type = 'routeord'

--ROLLBACK
--if exists(select * from reference where type='routeord')
--  delete from reference where type='routeord'
 