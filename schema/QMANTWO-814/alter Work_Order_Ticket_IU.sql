USE [QM]
GO
/****** Object:  Trigger [dbo].[work_order_ticket_iu]    Script Date: 10/3/2019 10:19:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[work_order_ticket_iu] ON [dbo].[work_order_ticket]
FOR INSERT, UPDATE
AS
BEGIN
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON


declare @tktTktDueDate DateTime;
declare @woTktDueDate DateTime;
declare @wo_tkt_id int;
declare @wo_wo_id int;

set @wo_tkt_id = (select ticket_id from inserted)
set @wo_wo_id  = (select wo_id from inserted)

set @tktTktDueDate =  (select [due_date] 
						from [dbo].[ticket] 
						where [ticket_id] = @wo_tkt_id)

set @woTktDueDate = (select [due_date] 
					 from [dbo].[work_order]
					 where [wo_id] = @wo_wo_id)


  -- Update the modified_date when a wo ticket changes
  update work_order_ticket 
  set modified_date = GetDate()
  where work_order_ticket.wo_id = @wo_wo_id 
    and work_order_ticket.ticket_id = @wo_tkt_id

  -- Insure the DUE date for Lambert workOrders the same as the related associate ticket
  update [dbo].[work_order]
  set due_date= @tktTktDueDate
  where wo_id = @wo_wo_id
  and wo_source='LAM01'
  and @tktTktDueDate <> @woTktDueDate  --only update if dates differ to avoid firing off triggers unnecessarily


END
              