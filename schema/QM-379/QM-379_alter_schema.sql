update call_center set xml_ticket_format =
'<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:template match="/">
    <xsl:value-of select="NewDataSet/tickets/printable_text"/>
  </xsl:template>
</xsl:stylesheet>
'
where cc_code = 'IL1EC'
