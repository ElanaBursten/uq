USE [QM]
GO

/****** Object:  Table [dbo].[TSE_ApprovalQueue]    Script Date: 12/6/2023 1:35:21 PM ******/
DROP TABLE [dbo].[TSE_ApprovalQueue]
GO

/****** Object:  Table [dbo].[TSE_ApprovalQueue]    Script Date: 12/6/2023 1:35:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TSE_ApprovalQueue](
	[TSE_Entry_Id] [int] NOT NULL,
	[Work_Emp_ID] [int] NOT NULL,
	[Status] [nchar](10) NOT NULL,
	[WorkDate] [datetime] NOT NULL,
	[QR] [bit] NOT NULL,
	[QC] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[note] [varchar](50) NULL,
 CONSTRAINT [PK_TSE_ApprovalQueue] PRIMARY KEY CLUSTERED 
(
	[TSE_Entry_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

