USE [QM]
GO

/****** Object:  Table [dbo].[rabbitmq_out]    Script Date: 11/9/2023 3:40:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[rabbitmq_out](
	[message_id] [int] IDENTITY(1,1) NOT NULL,
	[queue_name] [varchar](255) NOT NULL,
	[message_payload] [nvarchar](max) NULL,
	[insert_date] [datetime] NOT NULL,
	[source_header] [nvarchar](510) NULL,
	[message_type_header] [nvarchar](510) NULL,
	[message_id_header] [nvarchar](510) NULL,
	[publish_date_from_header] [datetime] NULL,
	[status] [varchar](20) NULL,
	[processed] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[message_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[rabbitmq_out] ADD  DEFAULT (getdate()) FOR [insert_date]
GO

ALTER TABLE [dbo].[rabbitmq_out] ADD  CONSTRAINT [DF_rabbitmq_out_processed]  DEFAULT ((0)) FOR [processed]
GO

