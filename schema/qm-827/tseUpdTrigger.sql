USE [QM]
GO

/****** Object:  Trigger [tseUpdTrigger]    Script Date: 11/9/2023 3:44:12 PM ******/
DROP TRIGGER [dbo].[tseUpdTrigger]
GO

/****** Object:  Trigger [dbo].[tseUpdTrigger]    Script Date: 11/9/2023 3:44:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[tseUpdTrigger]  
ON [dbo].[timesheet_entry]
AFTER UPDATE   
AS   
IF ( UPDATE ([final_approve_date])  )  
BEGIN  
  insert into TSE_ApprovalQueue (TSE_entry_id, Work_Emp_ID, STATUS, WorkDate, QR,QC, modified_date, note)
  Select ins.entry_id, 
  ins.work_emp_id,
  'NEW', 
  ins.Work_Date, 
  case when ins.work_start1 is not null
    then 1 else 0  --QR
  end, 
  case when ins.callout_start1 is not null
      then 1 else 0--QC 
   end, 
   getdate(), --modified_date
   null  --note
   from inserted ins
     where ins.source = 'TSE' and (ins.status='ACTIVE' or ins.status='SUBMIT') and ins.final_approve_date is not null
END;  
GO

ALTER TABLE [dbo].[timesheet_entry] ENABLE TRIGGER [tseUpdTrigger]
GO

