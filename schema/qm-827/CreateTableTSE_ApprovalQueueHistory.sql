USE [QM]
GO

/****** Object:  Table [dbo].[TSE_ApprovalQueueHistory]    Script Date: 11/9/2023 3:42:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TSE_ApprovalQueueHistory](
	[TSE_History_Id] [int] IDENTITY(1,1) NOT NULL,
	[TSE_Entry_Id] [int] NOT NULL,
	[Work_Emp_ID] [int] NOT NULL,
	[Status] [nchar](10) NOT NULL,
	[WorkDate] [datetime] NOT NULL,
	[QR] [bit] NOT NULL,
	[QC] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL,
	[note] [varchar](50) NULL
) ON [PRIMARY]
GO

