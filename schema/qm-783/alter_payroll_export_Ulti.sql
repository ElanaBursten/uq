USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[payroll_export_Ulti]    Script Date: 7/19/2023 12:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER proc [dbo].[payroll_export_Ulti] (
  @begin_date datetime,
  @end_date datetime,
  @region varchar(16))
as
--Declare
  --@begin_date datetime,
  --@end_date datetime,
  --@region varchar(16);

  --set @begin_date =  '1/10/2022'
  --set @end_date =    '1/20/2022'
  --set @region =      '007'

SET NOCOUNT ON

if (@region is not null) and (not exists (select pc_code from profit_center where pc_code = @region))
  RAISERROR ('The payroll profit center %s was not found in the profit_center table. Please add it using the QManager administration tool.', 15, 1, @region)

-- Chop off any time part
set @end_date = convert(datetime, convert(varchar(10), @end_date, 101), 101)

-- Make sure we landed on a Saturday, in case the :59:59:999 rounded to Sunday
while DatePart(DW, @end_date) <> 7
  set @end_date = DateAdd(d, -1, @end_date)

DECLARE @emps TABLE (
  _emp_id int not null primary key,
  --emp_number varchar(25) not null,
  empNo     varchar(6)   null,  --QMANTWO-650
  empManNo varchar(6)   null,  --QMANTWO-650
  solomon_pc varchar(4)   null,   
  current_mgr_id integer not null,
  current_pc varchar(15) not null,
  company varchar(4),
  last_name varchar(30),
  time_rule varchar(10),
  charge_cov bit,
  adp_code varchar(2),
  incent_pay bit,
  EarningsType varchar(8) NULL
)

DECLARE @time_data TABLE  (
  e_id int not null,              --emp_id
  m_id int not null,              --manager emp_id
  p_ctr varchar(16) not null,     --profit center
  reg_hours decimal(8,2),         --regular   DT
  ot_hours decimal(8,2),         --overtime
  dt_hours decimal(8,2),         --double-time
  br_hours decimal(8,2),         --bereavement
  callout_hours decimal(8,2),    --OTCOER   
  vac_hours decimal(8,2),
  leave_hours decimal(8,2),      -- used for SICK  qm-518
  pto_hours decimal(8,2),         --PTO
  hol_hours decimal(8,2),
  fhl_hours decimal(8,2),       --floating hrs
  jury_hours decimal(8,2),
  pov_hours decimal(8,2),
  cov_hours decimal(8,2),
  mileage decimal(8,2),
  cov_days int,
  hol_incent_pay decimal(8,2),
  fhl_incent_pay decimal(8,2),
  EarningsType varchar(8) NULL
)

DECLARE @emp_hours_summary TABLE (
  emp_id int not null primary key,
  max_hours_mgr int,
  max_hours_pc varchar(16),
  total_reg_hours decimal(8,2),
  total_ot_hours decimal(8,2),
  total_dt_hours decimal(8,2),
  total_pov_hours decimal(8,2),
  total_other_hours decimal(8,2)
)

DECLARE @results TABLE  (
  co_code varchar(20),
  RecordLevel varchar(10), 
  ReferenceNumber varchar(6),  --QMANTWO-650   
  WeekEndDate varchar(10),            
  emp_id int null,   -- internal only, not exported        
  mgr_id int null,   -- internal only, not exported
  pc_code varchar(16) null,
  line int null,     -- internal only, not exported
  reg_hours decimal(8,2),  --  REG
  EarningsType varchar(18),  --QMANTWO-650
  UnitRate varChar(1),
  Earnings varChar(1),
  GLaccount varChar(1),
  ProjectID varChar(10),
  TaskID varChar(16),
  GLSubAcct varChar(1),
  Billable varChar(2)
)

DECLARE @mileage_rate_string varchar(255)
DECLARE @mileage_rate decimal(8,2)

-- Get the mileage rate from configuration_data, and validate it
select @mileage_rate_string = value
from configuration_data
where name = 'PayrollMileageRate'

if (@mileage_rate_string is null)
  RAISERROR('The payroll mileage rate (PayrollMileageRate) was not found in the configuration_data table. Please enter it using the QManager administration tool.', 15, 1)

begin try
  set @mileage_rate = CONVERT(decimal(8,2), @mileage_rate_string)
end try
begin catch
  RAISERROR('The payroll mileage rate (PayrollMileageRate) in the configuration_data table is either blank or not in the correct format. Please correct it using the QManager administration tool.', 15, 1)
end catch

DECLARE @export_pto_hours varchar(255)

select @export_pto_hours = IsNull(Upper(value), 'N')
from configuration_data
where name = 'ExportPTOHours'
--print @export_pto_hours   --test
--set @export_pto_hours = 'N'  --test
-- Collect the emps for the profit center
insert into @emps 
  select e.emp_id, 
    --dbo.format_emp_number(e.emp_number),
	dbo.format_emp_number(pd.Emp_Number) as Emp_Number, --QMANTWO-650
	dbo.format_emp_number(pd.Man_Emp_Number) as Man_Emp_Number,--QMANTWO-650
	pc.solomon_pc,--QMANTWO-650
    Coalesce(e.report_to, -1) as current_mgr_id, 
    Coalesce(pd.emp_pc_code, '000') as current_pc, 
    IsNull(lc.payroll_company_code, '---'),
    e.last_name, tr.code, e.charge_cov, pc.adp_code,
    e.incentive_pay,
	pd.EarningsType
  from dbo.employee_payroll_data_Ulti(@region) pd   --QMANTWO-650
    inner join employee e on (e.emp_id=pd.emp_id)
    inner join reference tr on (e.timerule_id=tr.ref_id)
    inner join profit_center pc on (pc.pc_code=pd.emp_pc_code)
    inner join locating_company lc on (lc.company_id=e.company_id)
  where e.emp_number is not null
 

--select * from @emps order by empNo --test point
  
-- collect the summarized hours data for the period by employee, manager, & profit center
insert into @time_data
  select e._emp_id, 

    Coalesce(emphist.report_to, e.current_mgr_id) as mgr_id,
    Coalesce(emphist.payroll_pc_code, emphist.active_pc, e.current_pc) as pc_code,
    Round(Sum(IsNull(tse.reg_hours, 0)), 2),
    Round(Sum(IsNull(tse.ot_hours, 0)), 2), 
    Round(Sum(IsNull(tse.dt_hours, 0)), 2),
    Sum(IsNull(tse.br_hours,0)),    
    Round(Sum(IsNull(tse.callout_hours,0)), 2),
    Sum(IsNull(tse.vac_hours,0)),
    Sum(IsNull(tse.leave_hours,0)),
    -- this works because a constraint prevents a row from having both pto & vac or leave hrs
    Sum(IsNull(tse.pto_hours,0)) + Sum(IsNull(tse.vac_hours,0)), 
    Sum(
      case 
        when e.incent_pay = 0 then IsNull(tse.hol_hours, 0) else 0 
      end
    ),
    Sum(
      case
        when e.incent_pay = 0 and tse.floating_holiday = 1 then 8 --8 hours for a floating holiday
      else 0
      end
    ),
    Sum(IsNull(tse.jury_hours,0)),
    0, -- removed POV hours from export -JT 09/27/2007
    Sum(
      case
        when tse.vehicle_use like 'COV%' then Round(IsNull(tse.reg_hours,0) + IsNull(tse.ot_hours,0) + IsNull(tse.dt_hours,0) + IsNull(tse.callout_hours,0),2)
        else 0
      end),
    Sum(
      case
        when tse.vehicle_use = 'POV' then (Coalesce(tse.miles_stop1,0) - Coalesce(tse.miles_start1,0))
        else 0
      end),
    Sum(
      case 
        when (tse.vehicle_use like 'COV%' or (e.charge_cov = 1 and tse.vehicle_use not like 'POV%'))
              and IsNull(tse.reg_hours,0) + IsNull(tse.ot_hours,0) + IsNull(tse.dt_hours,0) + IsNull(tse.callout_hours,0) > 0 then 1
        else 0
      end),
    Sum(
      case 
        when e.incent_pay = 1 and tse.hol_hours > 0 then isnull(dbo.get_employee_pay_incentive (e._emp_id, 'HOL'), 0)
      else 0
      end
    ), -- the amount employees on the incentive pay program get for a holiday
    Sum(
      case 
        when e.incent_pay = 1 and tse.floating_holiday = 1 then 
            isnull(dbo.get_employee_pay_incentive (e._emp_id, 'FHL'), 0)
        else 0
      end
    ), -- the amount employees on the incentive pay program get for a floating holiday
	EarningsType
  from timesheet_entry tse
  inner join @emps e on tse.work_emp_id = e._emp_id
  -- sometimes an emp has hours for a date occuring before any employee_history is created
  -- if there is history, use the row that is the most current for the work_date (in case things changed more than once)
  left join employee_history emphist on e._emp_id = emphist.emp_id 
    and emphist.active_start = Coalesce(
      (select max(active_start) from employee_history 
      where employee_history.emp_id = e._emp_id 
        and convert(datetime, convert(varchar, employee_history.active_start, 101)) <= tse.work_date 
        and convert(datetime, convert(varchar, employee_history.active_end, 101)) >= tse.work_date), GetDate())
  where tse.work_date between @begin_date and @end_date
    and tse.final_approve_by is not null
    and tse.status <> 'OLD'
	and tse.source <> 'UTA'  --QM-613
	and tse.source <> 'RTASQ'  --QM-783 sr
    and (IsNull(tse.reg_hours,0) +
      IsNull(tse.ot_hours,0) +
      IsNull(tse.dt_hours,0) +
      IsNull(tse.callout_hours,0) +
      IsNull(tse.vac_hours,0) +
      IsNull(tse.leave_hours,0) +
      IsNull(tse.pto_hours, 0) +
      IsNull(tse.br_hours,0) +
      IsNull(tse.hol_hours,0) +
      IsNull(tse.floating_holiday, 0) +
      IsNull(tse.jury_hours,0)) > 0 
  group by e._emp_id, coalesce(emphist.payroll_pc_code, emphist.active_pc, e.current_pc), 
    coalesce(emphist.report_to, e.current_mgr_id), EarningsType

--select * from @time_data  --test point
-- Remove 0-hour rows, so that employees with no time or holidays will not appear  */
delete from @time_data where 
    (IsNull(reg_hours,0) +
      IsNull(ot_hours,0) +
      IsNull(dt_hours,0) +
      IsNull(callout_hours,0) +
      IsNull(vac_hours,0) +
      IsNull(leave_hours,0) +
      IsNull(pto_hours, 0) +
      IsNull(br_hours,0) +
      IsNull(hol_hours,0) +
      IsNull(fhl_hours, 0) +
      IsNull(jury_hours,0)) = 0
    and IsNull(hol_incent_pay, 0) = 0 -- Pay Incentive emps have an amt instead of hours
    and IsNull(fhl_incent_pay, 0) = 0
update @time_data set
  -- Salaried & Salaried Exempt emps don't get DT
  dt_hours = case time_rule 
    when 'SAL' then 0 
    when 'SALNEX' then 0 
    else dt_hours end,
  -- Salaried & Salaried Exempt emps don't get Callouts
  callout_hours = case time_rule 
    when 'SAL' then 0 
    when 'SALNEX' then 0
    else callout_hours end,
  -- Salaried emps don't get OT
  ot_hours = case time_rule when 'SAL' then 0 else ot_hours end
  from @emps  
  where e_id = _emp_id

insert into @emp_hours_summary (emp_id, total_reg_hours, total_ot_hours, total_dt_hours, total_pov_hours, total_other_hours)
  select e_id, sum(reg_hours), sum(ot_hours), sum(dt_hours), sum(pov_hours), sum(pto_hours + br_hours + hol_hours + fhl_hours + jury_hours)
    from @time_data 
    group by e_id

-- save the mgr & profit center associated with the highest reg hours for the period
update @emp_hours_summary
  set max_hours_mgr = (select top 1 m_id from @time_data where e_id = emp_id order by reg_hours desc),
  max_hours_pc = (select top 1 p_ctr from @time_data where e_id = emp_id order by reg_hours desc)

update @time_data
  -- all ot goes to the mgr / pc with the most reg hours
  set ot_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc) then total_ot_hours 
    else 0 
  end,
  -- all dt goes to the mgr / pc with the most reg hours
  dt_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc) then total_dt_hours 
    else 0 
  end,
  -- reg hour adjustments go to the mgr / pc with the most reg hours
  reg_hours = case 
    when ((time_rule = 'SAL' or time_rule = 'SALNEX')
      and m_id = max_hours_mgr and p_ctr = max_hours_pc) then 40 - total_reg_hours - total_other_hours + reg_hours
    else reg_hours
  end,
  -- *** Note that pov is NOT currently used ***
  -- pov hour adjustments go to the mgr / pc with the most reg hours
  pov_hours = case 
    when (m_id = max_hours_mgr and p_ctr = max_hours_pc and total_pov_hours > 42) then 42 - (total_pov_hours - pov_hours)
    else pov_hours
  end
  from @emp_hours_summary inner join @emps on _emp_id = emp_id
  where e_id = emp_id 
  
-- make sure the last update didn't make reg_hours negative
update @time_data set reg_hours = 0 where reg_hours < 0
  
-- *** Populate the result set QMANTWO-650 starts here  ****
-- Insert row 1
insert into @results (emp_id, mgr_id, pc_code, line, reg_hours , EarningsType)  -- regulat time
                 select e_id, m_id,     p_ctr, 1,    (reg_hours+dt_hours+ot_hours), 'REG'+EarningsType
  from @time_data 
  where reg_hours <> 0

insert into @results (emp_id, mgr_id, pc_code, line, reg_hours, EarningsType)  -- overtime
  select                e_id,   m_id,   p_ctr,    1, ot_hours, 'OT5'+EarningsType
  from @time_data 
  where ot_hours <> 0

-- Insert "POV/COV" row(s)
-- the line 5 is just to make it sort after the other stuff
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  --Car pay
  select                e_id,   m_id,   p_ctr,    5,     'CARTF', (cov_days)
    from @time_data
    where cov_days <> 0

-- Insert other details based on hours data:
-- double-time 
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- double-time
  select                e_id,   m_id,   p_ctr,    2,     'DTP', dt_hours
    from @time_data
    where dt_hours <> 0
-- callouts
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType,     reg_hours)  -- Call outs
  select                e_id,   m_id,   p_ctr,    2,     'ONWRK', callout_hours
    from @time_data
    where callout_hours <> 0
if @export_pto_hours = 'Y' begin
  -- personal time off
  insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)   -- PTO
    select                e_id,   m_id,   p_ctr,    2,       'PTO', pto_hours
      from @time_data
      where pto_hours <> 0
  -- vacation
  insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- vacation
    select                e_id,   m_id,   p_ctr,    2,       'PTO', vac_hours
      from @time_data
      where vac_hours <> 0 
  -- leave
  insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  --Sick Leave  QM-518 used for Sick time
    select                e_id,   m_id,   p_ctr,    2,        'SICK', leave_hours
      from @time_data
      where leave_hours <> 0
end
else begin 
  -- vacation
  insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- vacation
    select                e_id,   m_id,   p_ctr,    2,       'PTO', vac_hours
      from @time_data
      where vac_hours <> 0 
  -- leave
  insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  --Sick Leave  QM-518 used for Sick time
    select                e_id,   m_id,   p_ctr,    2,        'SICK', leave_hours
      from @time_data
      where leave_hours <> 0
end
-- bereavement
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- Bereavement
  select                e_id,   m_id,   p_ctr,    2,     'BEV'+EarningsType, br_hours
    from @time_data
    where br_hours <> 0
-- holiday
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- Holiday
  select                e_id,   m_id,   p_ctr,    2,     'HOL'+EarningsType, hol_hours
    from @time_data
    where hol_hours <> 0

-- floating holiday
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)  -- floating holiday
  select                e_id,   m_id,   p_ctr,    2,     'FHL'+EarningsType, fhl_hours
    from @time_data
    where fhl_hours <> 0

-- JUR duty
insert into @results (emp_id, mgr_id, pc_code, line, EarningsType, reg_hours)
  select                e_id,   m_id,   p_ctr,    2,        'JUR'+EarningsType, jury_hours
    from @time_data
    where jury_hours <> 0

-- update company
update @results 
  set     co_code = e.company,
      RecordLevel = 'DETAIL',
      WeekEndDate = (select convert(varchar(10), @end_date, 101)),
  ReferenceNumber = (SELECT qm.dbo.extractNumbers(period)
				     FROM [QM].[dbo].[dycom_period]
				     where @end_date between [starting] and [ending]),
        ProjectID = '0000000000',
		Billable = 'No',
		TaskID   = solomon_pc+empManNo+empNo
  from @emps e 
  where e._emp_id = emp_id



DECLARE @resultsHdr TABLE  (
  co_code varchar(20),
  adp_code varchar(2),
  line int null,     -- internal only, not exported
  RecordLevel varchar(10), 
  ReferenceNumber varchar(6),  --QMANTWO-650
  WeekEndDate varchar(10),            
  empNo varchar(6) null,   -- internal only, not exported
  solomon_pc varchar(4) null,   -- internal only, not exported
  EarningsType varchar(18),  --QMANTWO-650
  reg_hours decimal(8,2),  --  REG
  UnitRate varChar(1),
  Earnings varChar(1),
  GLaccount varChar(1),
  ProjectID varChar(10),
  TaskID varChar(16),
  GLSubAcct varChar(1),
  Billable varChar(2)
)

update @results
set UnitRate = '3'
where EarningsType = 'CARTF'

insert into @resultsHdr (RecordLevel, WeekEndDate, Line)values('BATCH', 'H',0);

--select * from @resultsHdr

-- Output summary 
select co_code, count(*) 'row_count' 
  from @results
  where co_code is not null
  group by co_code

-- Output details, label fields per requirements for simple exporting
select co_code AS 'co code',
	  e.adp_code,   -- not output, used for naming the csv file
	  Line,
	  RecordLevel,
	  ReferenceNumber,
	  WeekEndDate AS 'Date',
	  dbo.format_emp_number(e.empNo) AS 'Employee ID', 
	  e.solomon_pc AS 'Work Location',
	  r.EarningsType AS 'Earning Type',
	  reg_hours AS 'Hours',
	  UnitRate AS 'Unit Rate',
	  Earnings AS 'Earnings',
	  GLaccount AS 'GL Account',
	  ProjectID AS 'Project ID',
	  TaskID AS 'Task ID',
	  GLSubAcct AS 'GL SubAcct',
	  Billable AS 'Billable'
	 from @results r
	  inner join @emps e on e._emp_id = r.emp_id
	 union all
	 Select * from @resultsHdr
     order by co_code, 'Employee ID', r.line


-- Extra results helpful for debugging:
/*
select 'Extra Debugging Output - NOT for Production!'
select * from @emps order by emp_number
select * from @time_data order by e_id
select * from @emp_hours_summary order by emp_id
*/
   



