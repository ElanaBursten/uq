/* Utiliquest Database Create Script */
/* Created by Oasis Digital 12/30/01 : Revised by Utiliquest 12/2014 - Present */

/* KEEP THIS SCRIPT up to date manually - do not replace it with
   a generated script.  This script should contain comments that explain
   the meaning behind the tables, fields, relationships, etc.  It is the
   canonical specification of the schema.

      Note about identities for primary keys (Inserted by Oasis)  They are used
      here, but there are some reasons we might want to move away from
      them.  They can be problematic in distributed systems.
      The identities are preset to start at different places, so that in
      development we are less likely to accidentally join one kind of ID to
      another kind of ID.

   Never add new columns in the middle of an existing table definition.
   Our test databases need to match the production server in the
   order of the columns in the physical table, or lots of strange
   problems will occur in triggers and selects into temporary tables.

   We always use the varchar data type instead of char, even if the
   field is a fixed length.
*/

/* ********************************** Domain related tables  ****** */

CREATE TABLE profit_center (
  pc_code       varchar(15) NOT NULL primary key,
  pc_name       varchar(40) NULL,
  gl_code       varchar(25) NULL,
  timesheet_num varchar(25) NULL, -- Called the "Solomon Profit Center" or "Solomon Office" for some exports
  modified_date datetime    NOT NULL DEFAULT getdate(),
  company_id    integer     NOT NULL DEFAULT 1,
  billing_contact  varchar(100) null,
  adp_code varchar(2) not null,
  pc_city varchar(40) null,
  manager_emp_id integer null
)
go
create unique nonclustered index profit_center_adp_code on profit_center (adp_code)
go

CREATE TABLE office (
  office_id        integer identity (100,1) primary key,
  office_name      varchar(40) NOT NULL,
  profit_center    varchar(15) NOT NULL,
  hours_to_respond integer NULL, /* how many business hours is the response limit? */
  modified_date    datetime NOT null DEFAULT getdate(),
  active           bit NOT NULL DEFAULT 1,
  company_name     varchar(20) null,
  address1         varchar(30) null,
  address2         varchar(30) null,
  city             varchar(20) null,
  state            char(2) null,
  zipcode          varchar(10) null,
  phone            varchar(12) null,
  fax              varchar(12) null
)
go

CREATE TABLE call_center (
  cc_code varchar(20) NOT NULL primary key,
  cc_name varchar(40) NOT NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  admin_email varchar(250) NULL,
  emergency_email varchar(250) NULL,
  summary_email varchar(250) NULL,
  message_email varchar(250) NULL,
  warning_email varchar(250) NULL,
  uses_wp bit NULL DEFAULT 0, -- Legacy/unused field, please remove
  responder_email varchar(250) NULL,
  noclient_email varchar(250) NULL,
  audit_method varchar(80) NULL,
  use_prerouting bit not null default 1,
  notes text null,
  track_arrivals bit null,
  xml_ticket_format text null,
  time_zone int null,
  allowed_work varchar(10) null
)
go

create table call_center_contact (
  contact_id int identity(42000,1) primary key,
  cc_code varchar(20) not null,
  name varchar(80) null,
  email varchar(100) null,
  phone varchar(60) null
)
go

create table employee
(
  emp_id            int                   identity(200, 1), -- employee internal id
  type_id           int                   null    , -- type - administrator, manager, marker
  status_id         int                   null    , -- status - active, terminated, etc.
  timesheet_id      int                   null    , -- weekly, bi-weekly timesheet indicator
  emp_number        varchar(15)           null    , -- employee number used for ADP/payroll, Dynamics, HR
  short_name        varchar(30)           null    , -- short name of employee
  first_name        varchar(20)           null    , -- employee first name
  last_name         varchar(30)           null    , -- employee last name
  middle_init       char(1)               null    , -- employee middle intial
  report_to         int                   null    , -- manager employee reports to
  create_date       datetime              null    , -- date record created, not filled in by QM yet
  create_uid        int                   null    , -- user id of record creator, not filled in by QM yet
  modified_date     datetime              not null default getdate(),
  modified_uid      int                   null    , -- user id of record modifier, not filled in by QM yet
  charge_cov        bit                   not null default 1,
  active            bit                   not null default 1,
  can_receive_tickets bit                 not null default 1,
  timerule_id       int                   null,
  dialup_user       varchar(20) null,
  company_car       varchar(10) null,
  repr_pc_code      varchar(15) null,               -- This emp is the root of this profit center (only one per pc per company)
  payroll_pc_code   varchar(15) null,               -- Overrides the setting in repr_pc_code
  crew_num varchar(5) not null default '9999',      -- Used for solomon payroll export
  autoclose bit not null default 0,
  company_id int null,
  rights_modified_date datetime not null default getdate(), -- updated everytime employee rights changes
  ticket_view_limit int null,
  hire_date datetime null,
  show_future_tickets bit not null default 1,
  incentive_pay bit not null default 0,
  contact_phone varchar(20) null,
  local_utc_bias int null,
  ad_username varchar(8) null,
  test_com_login varchar(20) null,
  adp_login varchar(20) null,
  -- New columns here must also be added to employee_history and employee_iu
  constraint pk_employee primary key (emp_id)
)
go

create index modified_date on employee(modified_date)
create index report_to on employee(report_to)
go

create table employee_history
(
  -- ID removed for now since it surfaces ADO emp_id retrieval bugs when adding a new employee and user
  --emp_history_id    int                   identity(8000, 1) primary key, -- PK
  active_start      datetime              not null, -- Date record became active, inclusive
  active_end        datetime              not null, -- Date record was de-activated, exclusive
  active_pc         varchar(15)           null,     -- Active normal PC during this period (pre-calculated)
  emp_id            int                   not null, -- employee internal id
  type_id           int                   null    , -- type - administrator, manager, marker
  status_id         int                   null    , -- status - active, terminated, etc.
  timesheet_id      int                   null    , -- weekly, bi-weekly timesheet indicator
  emp_number        varchar(15)           null    , -- external employee number
  short_name        varchar(30)           null    , -- short name of employee
  first_name        varchar(20)           null    , -- employee first name
  last_name         varchar(30)           null    , -- employee last name
  middle_init       char(1)               null    , -- employee middle intial
  report_to         int                   null    , -- manager employee reports to
  create_date       datetime              null    , -- date record created, not filled in by QM yet
  create_uid        int                   null    , -- user id of record creator, not filled in by QM yet
  modified_date     datetime              not null default getdate(),
  modified_uid      int                   null    , -- user id of record modifier, not filled in by QM yet
  charge_cov        bit                   not null default 1,
  active            bit                   not null default 1,
  can_receive_tickets bit                 not null default 1,
  timerule_id       int                   null,
  dialup_user       varchar(20) null,
  company_car       varchar(10) null,
  repr_pc_code      varchar(15) null,               -- This emp is the root of this profit center (only one per pc per company)
  payroll_pc_code   varchar(15) null,               -- Overrides the setting in repr_pc_code
  crew_num varchar(5) not null default '9999',      -- Used for solomon payroll export
  autoclose bit not null default 0,
  company_id int null,
  rights_modified_date datetime not null default getdate(), -- updated everytime employee rights changes
  ticket_view_limit int null,
  hire_date datetime null,
  show_future_tickets bit null,
  incentive_pay bit null,
  contact_phone varchar(20) null,
  local_utc_bias int null,
  ad_username varchar(8) null,
  test_com_login varchar(20) null,
  adp_login varchar(20) null,
)
go

alter table employee_history add constraint PK_employee_history
  primary key nonclustered (emp_id, active_start)
create index employee_history_emp_id_active_start on employee_history(emp_id, active_start)
create index employee_history_start_date_end_date on employee_history(active_start, active_end)
go
ALTER TABLE employee_history 
  ADD CONSTRAINT employee_history_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

create table employee_pay_incentive
(
incentive_id integer identity (82000, 1) not null,
emp_id integer not null,        -- A point in the hierarchy
incentive_type varchar(20) not null, -- HOL=Holiday Pay Incentives
rate numeric(7,2) not null,          -- Amount to pay
added_date datetime not null default GetDate(),
modified_date datetime not null default GetDate(),
active bit not null default 1,
constraint pk_employee_pay_incentive primary key (incentive_id)
)
go

create index employee_pay_incentive_emp_id on employee_pay_incentive(emp_id)
GO
ALTER TABLE employee_pay_incentive ADD CONSTRAINT employee_pay_incentive_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id)
GO

create trigger employee_pay_incentive_iu on employee_pay_incentive
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update employee_pay_incentive
  set modified_date = GetDate()
  from inserted I
  where employee_pay_incentive.incentive_id = I.incentive_id
end
GO

/* ============================================================ */
/*   Table: users - login users file                            */
/* ============================================================ */
create table users
(
  uid               int                   identity(500, 1), -- internal user id
  grp_id            int                   null    , -- access group id
  emp_id            int                   null    , -- employee id if exists
  first_name        varchar(20)           null    , -- login user first name
  last_name         varchar(30)           null    , -- login user last name
  login_id          varchar(20)           not null    , -- login id for user
  password          varchar(40)           null    , -- login password for user, as a 40 character hash
  chg_pwd           bit                   not null, -- flag to ask the user to change their password on the next login
  last_login        datetime              null    , -- date user last logged in
  active_ind        bit                   not null, -- active user indicator
  can_view_notes    bit                   not null default 0,
  can_view_history  bit                   not null default 0,
  chg_pwd_date      datetime              null default dateadd(d, 60, convert(datetime, convert(char, getdate(), 112))), -- date the current password expires, if any (defaults to 60 days from the beginning of today)
  modified_date     datetime              not null default getdate(),
  email_address	    varchar(80)           null    ,
  etl_access	    bit			  not null default 0,
  api_key			varchar(40)			  null    ,
  constraint pk_user primary key (uid)
)
go

ALTER TABLE users
ADD CONSTRAINT login_id_not_blank CHECK (login_id <> '')

CREATE INDEX users_emp_id on users(emp_id)
go


CREATE TABLE user_password_history (
    uid             int         NOT NULL,
    when_changed    datetime    NOT NULL DEFAULT getdate(),
    password_hash   varchar(40) NOT NULL,
    changed_by_uid  int         NULL,
    CONSTRAINT PK_user_password_history PRIMARY KEY (uid, when_changed)
)
go

ALTER TABLE user_password_history
     ADD CONSTRAINT FK_userpasswordhistory_changedby FOREIGN KEY (changed_by_uid) REFERENCES users (uid)
GO

ALTER TABLE user_password_history
     ADD CONSTRAINT FK_userpasswordhistory_user FOREIGN KEY (uid) REFERENCES users (uid)
GO


CREATE TABLE ticket (
  ticket_id integer identity (1000, 1),
  ticket_number varchar(20) NOT NULL,
  parsed_ok BIT NOT NULL,
  ticket_format varchar(20) NOT NULL, /* Often referred to as Call Center */
  kind varchar(20) NOT NULL,          /* ONGOING, NORMAL, EMERGENCY, DONE */
  status varchar(20) NULL,            /* MANUAL, MANUAL APPROVED, MANUAL DENIED */
  map_page varchar(20) NULL,
  revision varchar(20) NOT NULL default '',
  transmit_date datetime NULL,        /* When ticket was transfered to utiliquest */
  due_date datetime NULL,             /* Date when work should be done (some cases fines result if it is late) */
  work_description varchar(3500) NULL,
  work_state varchar(2) NULL,
  work_county varchar(40) NULL,
  work_city varchar(40) NULL,
  work_address_number varchar(10) NULL,
  work_address_number_2 varchar(10) NULL,
  work_address_street varchar(60) NULL,
  work_cross varchar(100) NULL,
  work_subdivision varchar(70) NULL,
  work_type varchar(90) NULL,
  work_date datetime NULL,           /* Date scheduled to work on this ticket */
  work_notc varchar(40) NULL,
  work_remarks varchar(1200) NULL,
  priority varchar(40) NULL,
  legal_date datetime NULL,
  legal_good_thru datetime NULL,
  legal_restake  varchar(40) NULL,
  respond_date datetime NULL,       /* Respond to the center/contractor about this ticket being received/worked by this date */
  duration varchar(40) NULL,
  company varchar(80) NULL,      /* Who the work is being done for */
  con_type varchar(50) NULL,
  con_name varchar(50) NULL,     /* excavator, name of contractor, company doing work  */
  con_address varchar(50) NULL,  /* address of contractor */
  con_city varchar(40) NULL,     /* city of contractor  */
  con_state varchar(40) NULL,    /* state of contractor */
  con_zip varchar(40) NULL,
  call_date datetime NULL,       /* when call center recieved the call to do the locates */
  caller varchar(50) NULL,
  caller_contact varchar(50) NULL,
  caller_phone varchar(40) NULL,
  caller_cellular varchar(40) NULL,
  caller_fax varchar(40) NULL,
  caller_altcontact varchar(40) NULL,
  caller_altphone varchar(40) NULL,
  caller_email varchar(40) NULL,
  operator varchar(40) NULL, /* operator who took the call */
  channel varchar(40) NULL,  /* channel through which the ticket was received, or emp_id for manual tickets */

  work_lat DECIMAL(9,6) NULL,
  work_long DECIMAL(9,6) NULL,

  image text NOT NULL,
  parse_errors text NULL,

  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  ticket_type varchar(38) NULL,
  legal_due_date datetime NULL,
  parent_ticket_id int NULL, /* The original ticket, if this is a duplicate ticket record */

  do_not_mark_before datetime NULL,  /* can not start work until ... */
  route_area_id integer NULL,
  watch_and_protect bit NULL DEFAULT 0,
  service_area_code varchar(40) NULL, /* FMW2 only, for now */
  explosives varchar(20) NULL,
  serial_number varchar(40) NULL,     /* FMW2 only, for now */
  map_ref varchar(60) NULL,
  followup_type_id integer NULL,
  do_not_respond_before datetime NULL, /* can not respond until ... */
  recv_manager_id int NULL,
  ward varchar(10) null,
  source varchar(12) null,
  update_of varchar(20) null,
  alert varchar(1) null,
  work_priority_id int null,
  do_not_route_until datetime NULL,  /* can not route until ... */
  work_extent varchar(100) null,
  geocode_precision int null,
  wo_number varchar(40) null,
  boring varchar(20) null
-- Newly added or changed ticket fields often require modifications to the sync's @tickets_to_send temp table
)
go

ALTER TABLE ticket ADD CONSTRAINT PK_ticket_ticketid PRIMARY KEY NONCLUSTERED
(ticket_id)
GO

create index ticket_ticket_number on ticket(ticket_number)
create index ticket_duedate on ticket (due_date)  -- matches production db
create index ticket_modifieddate on ticket (modified_date)  -- matches production db
create index ticket_transmit_date on ticket (transmit_date)
create index ticket_status on ticket (status) --??
create index ticket_serial_idx on ticket(serial_number)
-- for Loc:
-- create index ticket_cc_serial on ticket (serial_number, ticket_format)
go


CREATE TABLE ticket_version (
  ticket_version_id int identity (8000, 1) PRIMARY KEY,
  ticket_id integer NOT NULL,
  ticket_revision varchar(20) NULL, /* The call center's description of this revision, if any */
  ticket_number varchar(20) NOT NULL,
  ticket_type varchar(38) NULL,
  transmit_date datetime NOT NULL,
  processed_date datetime NOT NULL,
  arrival_date datetime NULL,
  serial_number varchar(40) NULL, /* FMW2 only, for now */
  image text NULL,  /* Allow nulls because of old data.  New records all have this. */
  filename varchar(100) NULL, /* Filename where the ticket came from */
  ticket_format varchar(20) NULL,
  source varchar(12) NULL,
)
go

create index ticket_version_ticket_id on ticket_version(ticket_id)
CREATE  INDEX ticket_version_transmit_date
 ON ticket_version(transmit_date, ticket_format)
-- Loc Only:
create index ticket_version_serial_number
 on ticket_version(serial_number)


/* This table can be used to hold any type of optional information about a ticket
   We use it for contact information, answers to questions and route deviation reasons */
CREATE TABLE ticket_info (
  ticket_info_id int identity (8000, 1) PRIMARY KEY,
  ticket_id integer not null,      /* Ticket ID this data is associated with */
  info_type varchar(8) NOT NULL,   /* The information type: ONGOCONT */
  info varchar(40) NULL,           /* The information data, Ex: Ongoing contact name */
  added_by int not null,           /* The employee ID that added the information */
  modified_date datetime NOT NULL default getdate(),
  locate_id integer NULL,         /* Will need this for history information - needs to be added to RO */
  qi_id integer NULL,
  tstatus varchar(5) NULL,
  parent_info_id int NULL
)
go

create index ticket_info_ticket_id on ticket_info(ticket_id)
GO

create trigger ticket_info_u on ticket_info
  for update
  AS
    update ticket_info set modified_date = getdate() where ticket_info_id in (select ticket_info_id from inserted)
  GO

CREATE TABLE followup_ticket_type (
  type_id int identity (17000, 1) PRIMARY KEY,
  modified_date datetime NOT NULL DEFAULT getdate(),
  ticket_type varchar(38) NOT NULL,
  call_center varchar(20) NOT NULL,
  active bit NOT NULL DEFAULT 1
)
go

create index followup_ticket_type_modified_date on followup_ticket_type(modified_date)


/* Locates map to party charges.  DO NOT change the order of or add columns
 * in the middle of this table since it will throw off the update triggger.
 */
CREATE TABLE locate (
  locate_id integer identity (20000, 1), /* PK - defined below */
  ticket_id integer NOT NULL,
  client_code varchar(10) NULL,  /* for ones that are not our client */
  client_id integer NULL, /* can be NULL if we import locates for */
        /* clients we don't current service */
        /* Note: always join with the client table using client_id */
  status varchar(5) NOT NULL,
        /* A damage on this locate would be costly - UQ will audit all high_profile locates */
  high_profile BIT NOT NULL DEFAULT 0,
  qty_marked integer NULL,  /* also called Units Marked */
  price money NULL,

  closed BIT NOT NULL DEFAULT 0,
  closed_by_id integer NULL,
  closed_how varchar(10) NULL,
  closed_date datetime NULL,  /* Having a closed date does not mean its closed, update for every status */
  modified_date datetime NOT NULL DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1, /* Is this locate really supposed to be on the ticket (largely unused) */
  invoiced bit NULL DEFAULT 0,

  assigned_to integer NULL, /* A cached copy of the emp_id assigned to this locate ONLY if it is not closed */
  regular_hours decimal(5,2) NULL, /* Unused */
  overtime_hours decimal(5,2) NULL, /* Unused */
  added_by varchar(8) NULL,
  watch_and_protect bit NULL DEFAULT 0,
  high_profile_reason int NULL,
  seq_number varchar(20) NULL,
  assigned_to_id integer NULL, /* A cached copy of the current/last emp_id assigned to this locate */
  mark_type varchar(10) NULL,
  alert varchar(1) NULL,
  entry_date datetime NULL,
  gps_id int NULL,       /* Most recent GPS location acquired for this locate */
  status_changed_by_id int NULL /*Used to track who actually statused the locate- for reporting purposes only.  The app uses closed_by_id which could be the assigned -or- logged on user as determined by the "Status As Assigned Locator" checkbox.*/
  -- Added/changed locate columns often require modifications to the sync's @locates_to_send temp table and locate_status
)
go

/* We must match the index names used in the production databases */
create index PK_locate_locateid on locate(locate_id)
create index locate_ticketid on locate(ticket_id)
create index locate_client_code on locate(client_code)
create index locate_modified_date on locate(modified_date)
create index locate_closed_date on locate(closed_date)
create index locate_assigned_to on locate(assigned_to)
create index locate_status on locate(status)

create index locate_assigned_closed on locate(assigned_to_id, closed_date)
create index locate_closed_date_client_id on locate(closed_date, client_id)

alter table locate
add primary key clustered (locate_id)


/* nonclient_locate is where all of the locates for non-UQ
   clients are placed.  These are the old -N status locates */
create table nonclient_locate (
  ticket_id int not null,
  client_code varchar(10) not null
)

/* Add necessary nonclient_locate indexes here */

alter table nonclient_locate
add primary key clustered (ticket_id, client_code)

/* locate_status is used (mainly in reports) to track changes in the status
   of a locate over time, so users can see when it was opened, when it was
   set to ongoing, when it was marked, etc.  This table is automatically
   updated by a trigger on the locate table. */
CREATE TABLE locate_status (
  ls_id integer identity(50000,1) primary key,
  locate_id integer NOT NULL,
  status_date datetime NULL,
  status varchar(5) NOT NULL,
  high_profile BIT NOT NULL,
  qty_marked integer NULL,
  statused_by integer NULL,
  statused_how varchar(10) NULL,
  insert_date datetime NOT null DEFAULT getdate(),
  regular_hours decimal(5,2) null,
  overtime_hours decimal(5,2) NULL,
  watch_and_protect BIT NULL,
  high_profile_reason int NULL,
  mark_type varchar(10) NULL,
  entry_date datetime NULL,
  gps_id integer NULL,
  status_changed_by_id int NULL
)

CREATE INDEX locate_id on locate_status(locate_id) /* Matches production DB name */
CREATE INDEX locate_status_insert_date on locate_status(insert_date)
CREATE INDEX ls_sb_sd on locate_status(statused_by, status_date)


/* This holds both hours and "daily units worked" on locates */
CREATE TABLE locate_hours (
  locate_hours_id integer identity(25000,1) primary key,
  locate_id integer not null,
  emp_id integer not null,
  work_date datetime not null,
  modified_date datetime not null default getdate(),
  regular_hours decimal(5,2) null,
  overtime_hours decimal(5,2) null,
  entry_date datetime null,
  units_marked int null,
  status varchar(5) null,
  hours_invoiced bit not null default 0,
  units_invoiced bit not null default 0,
  unit_conversion_id integer null -- conversion rule currently used for this client
)

CREATE INDEX locate_hours_locate_id on locate_hours(locate_id)
CREATE INDEX locate_hours_entry_date on locate_hours(entry_date)
ALTER TABLE locate_hours 
  ADD CONSTRAINT locate_hours_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

CREATE TABLE locate_plat (
  locate_plat_id integer IDENTITY(27000,1) PRIMARY KEY,
  locate_id integer NOT NULL,
  plat varchar(20) NOT NULL,
  modified_date datetime NOT NULL DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  insert_date datetime NOT NULL DEFAULT getdate(),
  modified_by int NULL,
  added_by int NULL
)
CREATE INDEX locate_plat_locate_id on locate_plat(locate_id)

/**********************************************************************/

CREATE TABLE assignment (
  assignment_id integer identity (12000, 1) primary key nonclustered,
  locate_id integer NOT NULL,
  locator_id integer NOT NULL,   /* Who was assigned to this locate */
  status varchar(20) NULL,       /* Unused right now */
  downloaded_date datetime NULL, /* Unused right now */

  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1, /* Whether the assignment is current or historical */
  insert_date datetime DEFAULT getdate(), /* Always the server default date (never explicitly filled in by QML) */
  added_by varchar(8) NULL,       /* PARSER, or emp_id of user */
  workload_date datetime NULL
)
go

create CLUSTERED index assignment_locateid_locatorid on assignment (locate_id, locator_id)
create index assignment_locatorid on assignment(locator_id)
create index assignment_modifieddate on assignment(modified_date) /* Matches Production */


/* Managers are required to acknowledge some emergency/damage tickets by notifying
   the assigned locator immediately and then marking them as acknowledged in
   Q Manager.  This table tracks the tickets that need/have acknowledgment */
CREATE TABLE ticket_ack (
  ticket_ack_id integer identity PRIMARY KEY,
  ticket_id integer NOT NULL,
  ticket_version_id integer NULL,
  has_ack bit NOT NULL DEFAULT 0,  /* 0=needs ack, 1=has ack */
  ack_date datetime NULL,
  ack_emp_id integer NULL,  /* the manager that performed the ack */
  insert_date datetime NOT NULL DEFAULT getdate(),
  locator_emp_id integer NULL,  /* locator associated with the ticket */
)

create index ticket_ack_ack_date on ticket_ack(ack_date)
create index ticket_ack_has_ack on ticket_ack(has_ack)
create index ticket_ack_emp_id_has_ack on ticket_ack(locator_emp_id, has_ack)
create index ticket_ack_ticket_id on ticket_ack(ticket_id)
go
alter table dbo.ticket_ack add constraint
    FK_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO
alter table dbo.ticket_ack add constraint
    FK_ticket_version_id foreign key (ticket_version_id)
  references ticket_version (ticket_version_id)
GO
alter table dbo.ticket_ack add constraint
    FK_ack_emp_id foreign key (ack_emp_id)
  references employee (emp_id)
GO
alter table dbo.ticket_ack add constraint
    FK_locator_emp_id foreign key (locator_emp_id)
  references employee (emp_id)
GO

CREATE TABLE statuslist (
  status varchar(5) NOT NULL PRIMARY KEY,  /* short code */
  status_name varchar(30) NOT NULL,  /* human readable text */
  billable bit NULL,
  complete bit NULL,  /* does this code mean it is done? */

  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  mark_type_required bit NOT NULL DEFAULT 0,
  default_mark_type VARCHAR(10) NULL,
  allow_locate_units bit not null default 0,
  meet_only bit not null default 0
)
go

/* ticket_printing is used to track the printing of tickets for call centers */
/* that do not yet have laptops */
CREATE TABLE ticket_printing (
  ticket_printing_id integer identity PRIMARY KEY,
  ticket_batch_id integer NOT NULL,
  ticket_id integer NOT NULL,
  locator_id integer NOT NULL
)

create index ticketprinting_ticket_locator on ticket_printing(ticket_id, locator_id)
go

/* ticket_batch associates tickets that were printed at the same time. if we ever */
/* need to reprint an entire batch of previously printed tickets, this table would */
/* allow us to display a list of ticket batches, the date the batch was printed, */
/* and who it was printed for. It also saves us from having to store the print_date */
/* in all the individual records, and the batch_printed flag allows us to deal with */
/* batches that were started (by RPT_newticketdetail) but not completed. */
CREATE TABLE ticket_batch (
  ticket_batch_id integer identity PRIMARY KEY,
  batch_printed bit NOT NULL DEFAULT 0,
  print_date datetime NOT NULL DEFAULT getdate(),
  printed_for_user_id integer NOT NULL
)
go

/* These are really TermIDs, which are much smaller than UQ customers */

CREATE TABLE client (
  client_id integer identity (300, 1) primary key,
  client_name varchar(40) NOT NULL,
  oc_code varchar(10) NOT NULL,     /* AKA: term code, client code, code in the OneCall system. Can be duplicated across multiple call centers */
  office_id integer NULL,       /* which UQ office */
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  call_center varchar(20) NOT NULL,
  update_call_center varchar(20) NULL,
  grid_email varchar(255) NULL,
  allow_edit_locates bit NOT NULL DEFAULT 1,
  customer_id integer NULL,
  utility_type varchar(15) NULL,
  status_group_id integer NULL,
  unit_conversion_id integer NULL, -- conversion rule currently used for this client
  alert bit NOT NULL DEFAULT 0,
  require_locate_units bit not null default 0,
  attachment_url varchar(200) NULL,
  qh_id integer NULL,
  contract_review_due_date date NULL
)
go

create index call_center on client(call_center)
create index client_modified_date on client(modified_date)

CREATE TABLE users_client
(
  uid               int                   not null, -- link to users table
  client_id         int                   not null  -- link to client table
)
go

alter table users_client
    add constraint PK_users_client
    primary key nonclustered (uid, client_id)
go 
ALTER TABLE users_client 
  ADD CONSTRAINT users_client_fk_users FOREIGN KEY(uid) REFERENCES users(uid) NOT FOR REPLICATION
GO

CREATE TABLE customer (
  customer_id integer identity (700, 1) primary key,
  customer_name varchar(40) NOT NULL,
  customer_description varchar(150) NULL,
  customer_number varchar(20) NULL,
  city varchar(25) NULL,
  state varchar(2) NULL,
  zip varchar(10) NULL,
  street varchar(35) NULL,
  phone varchar(16) NULL,
  locating_company int not null, -- This links to the locating_company table
  modified_date datetime not null default getdate(),
  active bit not null default 1,
  street_2 varchar(100) NULL,
  attention varchar(100) NULL,
  contact_name varchar(100) NULL,
  contact_email varchar(100) NULL,
  period_type varchar(20) default 'WEEK',
  pc_code varchar(15) null, -- Profit center for billing exports, etc.
  contract varchar(50) null,
  contract_review_due_date date null,
)

create index customer_modified_date on customer(modified_date)

ALTER TABLE customer
  ADD CONSTRAINT customer_fk_profit_center
  FOREIGN KEY (pc_code) REFERENCES profit_center (pc_code)
go

CREATE TABLE map (
  map_id integer identity (600, 1) primary key,
  map_name varchar(30) NOT NULL,
  state varchar(5) NULL,
  county varchar(50) NULL,
  ticket_code varchar(15) NULL,  /* Some tickets have a map code on them; match by state and this code */
)
go

CREATE TABLE area (
  area_id integer identity (400, 1) primary key,
  area_name varchar(40) NOT NULL,
  map_id integer NOT NULL,
  locator_id integer NULL,  /* if assigned */
  modified_date datetime NOT NULL DEFAULT getdate()
)
go

CREATE TABLE area_change (
  area_id integer NOT NULL,
  changed_by integer NOT NULL,
  change_date datetime NOT NULL DEFAULT getdate(),
  locator_id integer NOT NULL,
)
go

alter table area_change add constraint PK_area_change
  primary key nonclustered (area_id, changed_by, change_date)

CREATE TABLE map_page (
  map_id integer NOT NULL,
  map_page_num integer NOT NULL,
  x_min varchar(10) NOT NULL,
  x_max varchar(10) NOT NULL,
  y_min varchar(10) NOT NULL,
  y_max varchar(10) NOT NULL,
  PRIMARY KEY(map_id, map_page_num)
)
go

CREATE TABLE map_cell (
  map_id integer NOT NULL,
  map_page_num integer NOT NULL,
  x_part varchar(10) NOT NULL,
  y_part varchar(10) NOT NULL,
  area_id integer NULL,  /* if assigned */
  PRIMARY KEY(map_id, map_page_num, x_part, y_part)
)
go

CREATE TABLE map_geo (
  map_id integer NOT NULL,
  map_page_num integer NOT NULL,
  x_part varchar(10) NOT NULL,
  y_part varchar(10) NOT NULL,
  min_lat decimal(9,6) NOT NULL,
  min_long decimal(9,6) NOT NULL,
  max_lat decimal(9,6) NOT NULL,
  max_long decimal(9,6) NOT NULL,
  PRIMARY KEY(min_lat, min_long, map_id)
)
go

/* create index map_geo_min_lat on map_geo(min_lat) */

create table county_area (
  county_area_id integer NOT NULL identity (900, 1) primary key,
  map_id integer NULL,  /* we don't care about this any more, but have not removed it */
  state varchar(2) NOT NULL,
  county varchar(40) NOT NULL,
  munic varchar(40) NOT NULL,
  area_id integer NULL,  /* if assigned */
  call_center varchar(20) NULL,
)
go

create index county_area_stuff on county_area(state, county, munic)

create table map_qtrmin (
  qtrmin_lat varchar(5) NOT NULL,
  qtrmin_long varchar(6) NOT NULL,
  area_id integer NULL,  /* if assigned */
  long_length integer NOT NULL DEFAULT 1,
  call_center varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY(qtrmin_lat, qtrmin_long, call_center)
)


create table map_trs (
  township varchar(5) NOT NULL,
  range varchar(6) NOT NULL,
  section varchar(6) NOT NULL,
  area_id integer NULL,  /* if assigned */
  basecode varchar(10) NOT NULL,
  call_center varchar(20) NOT NULL,
  constraint pk_map_trs primary key nonclustered (basecode, township, range,
    section, call_center)
)
CREATE INDEX township_range on map_trs(township, range)

/* this is a test table.  We should exclude it from the production
   database, but it is harmless. */

CREATE TABLE widget (
  widget_id integer identity(1000,1) primary key,
  owner_name varchar(40),
  length integer,
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1
)

/* *********************************************************************** */

CREATE TABLE damage (
  damage_id integer identity (30000, 1) primary key,
  office_id integer NULL,
  damage_inv_num varchar(12) NULL,
  damage_type varchar(20) NULL, /* PENDING, OPEN, CLOSED, MGRCLOSED */
  damage_date datetime NULL,
  /* notification */
  uq_notified_date datetime NULL,
  notified_by_person varchar(40) NULL,
  notified_by_company varchar(40) NULL,
  notified_by_phone varchar(20) NULL,
  /* client */
  client_code varchar(10) NULL,
  client_id integer NULL,
  client_claim_id varchar(25) NULL,
  /* contact */
  date_mailed datetime NULL,
  date_faxed datetime NULL,
  sent_to varchar(30) NULL,
  /* damage */
  size_type varchar(50) NULL,
  location varchar(80) NULL,
  page varchar(20) NULL,
  grid varchar(20) NULL,
  city varchar(40) NULL,
  county varchar(40) NULL,
  state varchar(2) NULL,
  utility_co_damaged varchar(40) NULL,
  diagram_number integer NULL,
  remarks text NULL,
  /* investigator */
  investigator_id integer NULL,
  investigator_arrival datetime NULL,
  investigator_departure datetime NULL,
  investigator_est_damage_time datetime NULL,
  investigator_narrative text NULL,
  /* excavator */
  excavator_company varchar(30) NULL,
  excavator_type varchar(20) NULL,
  excavation_type varchar(20) NULL,
  /* ticket */
  locate_marks_present varchar(1) NULL,
  locate_requested varchar(1) NULL,
  ticket_id integer NULL,
  /* site */
  site_mark_state varchar(20) NULL, /* bright, visible, faded, destroyed, none */
  site_sewer_marked bit NULL DEFAULT 0,
  site_water_marked bit NULL DEFAULT 0,
  site_catv_marked bit NULL DEFAULT 0,
  site_gas_marked bit NULL DEFAULT 0,
  site_power_marked bit NULL DEFAULT 0,
  site_tel_marked bit NULL DEFAULT 0,
  site_other_marked varchar(20) NULL,
  site_pictures varchar(20) NULL, /* none, digital, video, 35mm */
  site_marks_measurement DECIMAL(9,3) NULL,
  site_buried_under varchar(20) NULL, /* none, asphalt, dirt, concrete, other */
  site_clarity_of_marks varchar(20) NULL, /* Bright, Visible, Faded, Destroyed */
  site_hand_dig bit NULL DEFAULT 0,
  site_mechanized_equip bit NULL DEFAULT 0,
  site_paint_present bit NULL DEFAULT 0,
  site_flags_present bit NULL DEFAULT 0,
  site_exc_boring bit NULL DEFAULT 0,
  site_exc_grading bit NULL DEFAULT 0,
  site_exc_open_trench bit NULL DEFAULT 0,
  site_img_35mm bit NULL DEFAULT 0,
  site_img_digital bit NULL DEFAULT 0,
  site_img_video bit NULL DEFAULT 0,
  /* discussions */
  disc_repair_techs_were varchar(30) NULL,
  disc_repairs_were varchar(30) NULL,
  disc_repair_person varchar(30) NULL,
  disc_repair_contact varchar(30) NULL,
  disc_repair_comment text NULL,
  disc_exc_were varchar(30) NULL,
  disc_exc_person varchar(30) NULL,
  disc_exc_contact varchar(30) NULL,
  disc_exc_comment text NULL,
  disc_other1_person varchar(30) NULL,
  disc_other1_contact varchar(30) NULL,
  disc_other1_comment text NULL,
  disc_other2_person varchar(30) NULL,
  disc_other2_contact varchar(30) NULL,
  disc_other2_comment text NULL,
  /* responsibility */
  exc_resp_code varchar(4) NULL,
  exc_resp_type varchar(4) NULL,
  exc_resp_other_desc varchar(100) NULL, /* for EX09 only */
  exc_resp_details text NULL, /* details why the code applies  */
  exc_resp_response text NULL, /* client response to details */
  uq_resp_code varchar(4) NULL,
  uq_resp_type varchar(4) NULL,
  uq_resp_other_desc varchar(100) NULL, /* MM02, NM03 only */
  uq_resp_details text NULL, /* details why code applies (NM03, MM02, DC02 only) */
  spc_resp_code varchar(4) NULL,
  spc_resp_type varchar(4) NULL, /* not used yet */
  spc_resp_other_desc varchar(100) NULL, /* not used yet */
  spc_resp_details text NULL, /* code details */

  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  profit_center varchar(15) NULL,
  due_date datetime NULL,
  intelex_num varchar(15) NULL, /* Renamed utilistar_id */
  /*utilistar_id varchar(15) NULL, -- ID from the old UtiliStar database */
  closed_date datetime NULL,
  modified_by int NULL,
  uq_resp_ess_step varchar(5) NULL,
  facility_type varchar(10) NULL,
  facility_size varchar(10) NULL,
  locator_experience varchar(10) NULL,
  locate_equipment varchar(10) NULL,
  was_project bit not null default 0,
  claim_status varchar(10) NULL,
  claim_status_date datetime NULL,
  invoice_code varchar(10) NULL,
  facility_material varchar(10) NULL,
  added_by int NULL,          /* emp id that created the record - populated by trigger */
  accrual_date datetime NULL, /* Min of ticket due, locate closed, damage date from a trigger  */
  invoice_code_initial_value varchar(10) NULL,
  uq_damage_id integer NULL,
  estimate_locked bit NOT NULL default 0,     -- true prevents automatic estimate creation
  site_marked_in_white varchar(1) NULL,       -- was area marked in white (Y,N,b)
  site_tracer_wire_intact varchar(1) NULL,    -- was tracer wire intact (Y,N,b)
  site_pot_holed varchar(1) NULL,             -- was facility pot holed (Y,N,U,b)
  site_nearest_fac_measure decimal(9,3) NULL, -- feet to nearest facility
  excavator_doing_repairs varchar(20) NULL,   -- excavator doing the repairs
  offset_visible varchar(1) NULL,             -- were offsets visible (Y,N,b)
  offset_qty decimal(9,3) NULL,               -- what was the offset quantity
  claim_coordinator_id integer NULL,          -- employee.emp_id of person assigned as claim coordinator
  locator_id integer NULL,                     -- locator associated with the damage
  /* approval information */
  estimate_agreed bit NOT NULL default 0,      -- has supervisor looked at the estimate and agreed with it?
  approved_by_id integer NULL,
  approved_datetime datetime NULL,
  reason_changed int null,
  work_priority_id int null,
  user_changed_pc bit NOT NULL default 0,
  marks_within_tolerance varchar(1) NULL DEFAULT NULL      -- (Y,N,b) 
) /* Recreate the dycom_damage view below when you change the damage schema */
go

create index damage_investigator_id on damage(investigator_id)
create index damage_damage_date on damage(damage_date)
create index damage_uqnotifieddate on damage(uq_notified_date)
create index damage_due_date on damage(due_date)
create index damage_profit_center on damage(profit_center)
create index damage_damage_type on damage(damage_type)
create index damage_modified_date on damage(modified_date)
create unique index damage_uq_damage_id on damage(uq_damage_id)
alter table damage
  add constraint damage_notification_unique_constr
  unique nonclustered (uq_notified_date, profit_center, added_by)
go

CREATE TABLE damage_history (
  damage_id integer NOT NULL,
  modified_date datetime NOT NULL,
  modified_by int NULL,
  profit_center varchar(15) NULL,
  exc_resp_code varchar(4) NULL,
  exc_resp_type varchar(4) NULL,
  uq_resp_code varchar(4) NULL,
  uq_resp_type varchar(4) NULL,
  spc_resp_code varchar(4) NULL,
  spc_resp_type varchar(4) NULL,
  exc_resp_other_desc varchar(100) NULL,
  exc_resp_details varchar(MAX) NULL,
  exc_resp_response varchar(MAX) NULL,
  uq_resp_other_desc varchar(100) NULL,
  uq_resp_details varchar(MAX) NULL,
  uq_resp_ess_step varchar(5) NULL,
  spc_resp_details varchar(MAX) NULL,
  reason_changed int null
  PRIMARY KEY(damage_id, modified_date)
)
go

create index damage_history_damage_id on damage_history (damage_id)
go


CREATE TABLE damage_estimate (
  estimate_id integer identity (40000, 1) primary key,
  damage_id integer NOT NULL,
  third_party bit NOT NULL DEFAULT 0,
  emp_id integer NOT NULL,  /* Who added this estimate */
  amount DECIMAL(12,2) NULL,
  company varchar(30) NULL,
  contract varchar(25) NULL,
  phone varchar(25) NULL,
  comment varchar(50) NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  intelex_num varchar(15) /* Renamed field: utilistar_id */
  /* utilistar_id varchar(15) NULL, -- obsolete */
  /* estimate_type values:        */
  /* 0: damaged facility estimate */
  /* 1: 3rd party estimate        */
  /* 2: litigation estimate       */
  /* This can set based on third_party for existing records */
  estimate_type integer NOT NULL DEFAULT 0,
  third_party_id integer null,
  litigation_id integer null,
  added_date datetime null, /* Local date when the user added this estimate */
)
go

create index damage_estimate_damage_id on damage_estimate(damage_id)
create index damage_estimate_modified_date on damage_estimate (modified_date)
create index damage_estimate_third_party_id on damage_estimate(third_party_id)
create index damage_estimate_litigation_id on damage_estimate(litigation_id)
go

CREATE TABLE damage_invoice (
  invoice_id integer identity (70000, 1) primary key,
  damage_id integer NULL,
  estimate_id integer NULL,
  invoice_num varchar(30) NULL,
  amount DECIMAL(12,2) NULL,
  company varchar(40) NULL,
  received_date datetime NULL,
  damage_city varchar(30) NULL,
  damage_state varchar(2) NULL,
  damage_date datetime NULL,
  satisfied bit NOT NULL DEFAULT 0,
  satisfied_by_emp_id integer NULL,
  satisfied_date datetime NULL,
  comment varchar(80) NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  approved_date datetime NULL,
  /* Use decimal since SQLXML does not support the money type.  It results in:
     "Disallowed implicit conversion from data type nvarchar to data type money"
     See: http://groups.google.com/groups?selm=ONfFLNmECHA.2104%40tkmsftngp02 */
  approved_amount  decimal(19,4) NULL,
  paid_date datetime NULL,
  paid_amount  decimal(19,4) NULL,
  invoice_code varchar(10) NULL,
  cust_invoice_date datetime NULL,
  packet_request_date datetime NULL,
  packet_recv_date datetime NULL,
  invoice_approved bit NOT NULL DEFAULT 0,
  litigation_id integer NULL,
  third_party_id integer NULL,
  added_by integer NULL,
  payment_code varchar(10) NULL
)
go

create index damage_invoice_damage_id on damage_invoice(damage_id)
create index damage_invoice_estimate_id on damage_invoice(estimate_id)
create index damage_invoice_invoice_num on damage_invoice(invoice_num)
create index damage_invoice_satisfied on damage_invoice(satisfied)
go
ALTER TABLE damage_invoice 
  ADD CONSTRAINT damage_invoice_fk_employee FOREIGN KEY(satisfied_by_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO


create table damage_litigation (
  litigation_id integer identity (75000, 1) primary key,
  damage_id     integer not null,
  file_number   varchar(20) null,
  plaintiff     varchar(45) null,
  carrier_id    int null,
  attorney      varchar(8) null,
  party         varchar(8) null,
  demand        DECIMAL(12,2) null,
  accrual       DECIMAL(12,2) null,
  settlement    DECIMAL(12,2) null,
  closed_date   datetime null,
  comment       text null,
  defendant     varchar(45) null,
  venue         varchar(100) null,
  venue_state   char(2) null,
  service_date  datetime null,
  summons_date  datetime null,
  carrier_notified bit not null default 0,
  claim_status  varchar(10) null,
  added_by      int null,
  modified_date datetime not null default GetDate(),
  modified_by   int null
)
go

create index damage_litigation_closed_date on damage_litigation(closed_date)
create index damage_litigation_damage_id on damage_litigation(damage_id)
go

create table damage_third_party (
  third_party_id integer identity (76000, 1) primary key,
  damage_id integer not null,
  claimant         varchar(45) null, -- name of party making claim
  address1         varchar(30) null,
  address2         varchar(30) null,
  city             varchar(20) null,
  state            char(2) null,
  zipcode          varchar(10) null,
  phone            varchar(12) null,
  claim_desc       varchar(500) null, -- arbitrary size chosen
  uq_notified_date datetime NULL, -- date UQ learned of this claim
  carrier_id       int null,      -- insurance carrier assigned
  carrier_notified bit not null default 0,
  demand           decimal(12, 2) null,
  claim_status     varchar(10) null,
  added_by         int not null,     -- updated by trigger
  modified_by      int not null,
  modified_date    datetime not null default getDate() -- updated by trigger
)
go
create index damage_third_party_damage_id on damage_third_party(damage_id)
go

create table damage_bill (
  damage_bill_id integer identity(77000, 1) primary key,
  damage_id integer not null,
  billing_period_date datetime not null,
  billable bit not null default 1,
  modified_by integer not null,
  modified_date datetime not null DEFAULT getdate(),
  invoiced bit not null default 0
)
go

/* Insurance carriers for damages */
create table carrier (
  carrier_id    integer identity (79000, 1) primary key,
  name          varchar(40) not null,
  deductible    decimal(12, 2) not null,
  modified_date datetime not null default getdate()
)
go


CREATE TABLE damage_default_est (
  ddest_id integer identity (45000, 1) primary key,
  facility_type varchar(10) NOT NULL,
  facility_size varchar(10) NOT NULL,
  facility_material varchar(10) NOT NULL,
  profit_center varchar(15) NOT NULL,
  default_amount DECIMAL(12,2) NOT NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  utility_co_id integer NULL, /* reference.ref_id */
)

create unique index ddest_type_size_material_profit_utility on damage_default_est(facility_type, facility_size, facility_material, profit_center, utility_co_id)

create index ddest_modified_date on damage_default_est(modified_date)


/* *********************************************************************** */

CREATE TABLE summary_header (
  summary_header_id int IDENTITY (23001, 1) NOT NULL primary key ,
  call_center varchar(40) NOT NULL ,
  client_code varchar(20) NOT NULL ,
  summary_date datetime NOT NULL ,
  summary_date_end datetime NULL ,
  complete bit NULL ,
  expected_tickets int NULL ,
  modified_date datetime NULL
)
GO

create index summary_header_summary_date on summary_header(summary_date)

/* *********************************************************************** */

create table summary_fragment (
  summary_fragment_id int identity not null primary key,
  call_center varchar(20) not null,
  client_code varchar(10) not null,
  parse_date datetime not null,
  parse_error bit default 0 not null,
  image text not null
)
go

/* *********************************************************************** */

CREATE TABLE summary_detail (
  summary_detail_id int IDENTITY (43001, 1) NOT NULL primary key,
  summary_header_id int NOT NULL ,
  item_number varchar(20) NULL ,
  ticket_number varchar(20) NULL ,
  ticket_time datetime NULL ,
  type_code varchar(20) NULL ,
  revision varchar(10) NULL ,
  modified_date datetime NULL
)
GO

create index summary_detail_summary_header_id on summary_detail(summary_header_id)

/* *********************************************************************** */

CREATE TABLE routing_list (
  [routing_list_id] [int] IDENTITY (9000, 1) NOT NULL primary key,
  [call_center] [varchar] (20) NOT NULL ,
  [field_name] [varchar] (25) NOT NULL ,
  [field_value] [varchar] (80) NOT NULL ,
  [area_id] [int] NOT NULL
)
GO


/* *********************************************************************** */

CREATE TABLE sync_log (
  sync_id integer identity (25000, 1) primary key,
  emp_id integer NOT NULL,
  sync_date datetime NOT null DEFAULT getdate(),
  local_date datetime NULL,
  client_version varchar(30) NULL,
  bytes_up int NULL,
  bytes_down int NULL,
  total_time int NULL,
  local_utc_bias int NULL,
  updates_since datetime null,
  addin_version varchar(30) null,
  persequor_version varchar(30) null,
  sync_source varchar(10) null,
  tickets_in_client_cache int null
)
go

create index sync_log_emp_id on sync_log(emp_id, sync_date)
create index sync_log_local_date on sync_log (local_date)
ALTER TABLE sync_log 
  ADD CONSTRAINT sync_log_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

-- NOT in the prod DB, though it may help:
-- create index sync_log_sync_date on sync_log(sync_date)


create table computer_info (
  computer_info_id integer identity (35000, 1) primary key,
  emp_id integer not null,
  insert_date datetime not null default getdate(),
  windows_user varchar(25),  -- Windows login user
  os_platform integer,       -- NT/XP/Vista=2
  os_major_version integer,  -- Vista=6
  os_minor_version integer,  -- Vista=0
  os_service_pack integer,   -- Service pack number: 1, 2, 3, 4, etc.
  computer_name varchar(25), -- Windows computer name
  service_tag varchar(25),   -- Dell service tag
  computer_serial varchar(25) null, -- Computer serial #, if present (often blank/generic: 'Serial Number')
  domain_login varchar(25) null,    -- Windows domain used for login
  active bit not null default 0,    -- Only the most recent record per serial # is marked active
  sync_id integer null,             -- FK to sync_log
  retire_reason varchar(10) null,
  computer_model varchar(50) null,
  asset_tag varchar(30) null,
  aircard_phone_no varchar(15) null,
  aircard_phone_esn varchar(20) null
)
go

create index computer_info_emp_id_insert_date on computer_info(emp_id, insert_date)
go
ALTER TABLE computer_info 
  ADD CONSTRAINT computer_info_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO


create table computer_hotfix (
  computer_hotfix_id integer identity (35000, 1) primary key,
  computer_info_id integer not null,
  hotfix_id varchar(20) not null,
)
go

create index computer_hotfix_computer_info_id_hotfix_id on computer_hotfix(computer_info_id, hotfix_id)
go

create table employee_activity (
  emp_activity_id int identity(22000, 1) not null primary key nonclustered,
  emp_id int not null,
  activity_date datetime not null, /* Employee time zone */
  activity_type varchar(12) not null,
  details varchar(50) null,
  insert_date datetime not null default GetDate(),
  extra_details varchar(20) null
)
go

-- Cluster by activity date to keep from clustering on autoinc ID
create clustered index employee_activity_activity_date
  on employee_activity(activity_date)
go

-- Employee / date index so reports work
create index employee_activity_emp_id_activity_date
  on employee_activity(emp_id, activity_date)
go

-- Foreign key link to employee
alter table employee_activity
  add constraint employee_activity_fk_employee
  foreign key (emp_id) references employee (emp_id)
go

/* *********************************************************************** */

-- This is a virtual company inside the main company represented by the
-- whole database, for example UQ contains the companies UQ and UTI.
-- Companies are attached to customers and can then change the details
-- printed on customer bills to match the company servicing the customer.
create table locating_company (
  company_id int identity not null primary key,
  name varchar(25) not null,
  logo_filename varchar(50) null,
  address_street varchar(25) null,
  address_city varchar(25) null,
  address_state varchar(25) null,
  address_zip varchar(25) null,
  phone varchar(25) null,
  billing_footer varchar(100) null,
  floating_holidays_per_year tinyint not null default 0,
  modified_date datetime not null default getdate(),
  payroll_company_code varchar(20) null
)

/* add foreign key constraint for employee table */
ALTER TABLE employee
 ADD CONSTRAINT employee_fk_company
 FOREIGN KEY (company_id) REFERENCES locating_company (company_id)
go


/* Configures the billing system operation on a per-center or
   per-center/term override basis.  This data is not being used yet.  */
create table billing_config (
  config_id int identity primary key,
  center_group varchar(30) not null,
  term_group varchar(30) null,
  afterh_start datetime null,
  afterh_stop datetime null,
  afterh_saturday bit null,
  afterh_sunday bit null,
  afterh_holidays bit null,
  afterh_date_field varchar(25) null, -- The ticket's transmit_date or call_date
  afterh_consider_work_date bit null,
)


-- This stores one row per generated invoice (one per customer per billing run).
-- When invoices are reprinted form the same data, we re-use the existing
-- invoice ID/row.
create table billing_invoice (
  invoice_id int not null,        -- Generated invoice number
  billing_header_id int not null, -- bill_id of billing run
  customer_id int not null,       -- customer id
  end_date datetime not null,     -- end date of invoice run
  invoice_date datetime not null,  -- date invoice was created
  total_amount money null,        -- total amount represented on invoice
  revision varchar(2) null,       -- revision letter used to distinguish changes in adjustments and totals
  which_invoice varchar(20) null
)

ALTER TABLE billing_invoice ADD CONSTRAINT PK_billing_invoice PRIMARY KEY NONCLUSTERED (invoice_id)

create index billing_invoice_invoice_id on billing_invoice(invoice_id)
create index billing_invoice_billing_header_id on billing_invoice(billing_header_id)

ALTER TABLE billing_invoice
  ADD CONSTRAINT billing_invoice_fk_customer
  FOREIGN KEY (customer_id) REFERENCES customer (customer_id)
go


create table billing_output_config (
  output_config_id int identity primary key,-- Primary key
  customer_id integer not null,         -- Customer this configuration applies to
  output_template varchar(50) null,     -- The file name of the RB template
  contract varchar(50) null,            -- Printed on invoices
  payment_terms varchar(25) null,       -- Printed on invoices
  cust_po_num varchar(50) null,         -- Printed on invoices
  output_fields varchar(300) null,      -- Comma-delimited fieldnames from billing_detail
  billing_period varchar(15) null,      -- MONTHLY or WEEKLY
  flat_fee money null,                  -- > 0 if the customer is charged a flat fee per week/month
  comment varchar(100) null,            -- User comments
  center_group_id int null,             -- References the list of valid call centers for this config/customer
  SolomonOffice varchar(3) null,        -- Obsolete, remove soon
  SolomonPeriodToPost varchar(6) null,  -- Obsolete, remove soon
  which_invoice varchar(20) null,       -- Invoice name this configuration applies to.  Each rate can map to a separate invoice.
  sales_tax_rate decimal(6,4) null,     -- NOT YET IN USE, did not meet the actual need.
  group_by_price bit default 0,         -- Group ouptut line items by both price and line item text
  omit_zero_amount bit default 0,       -- Hide 0 price line item groups on the invoice
  customer_number varchar(20) NULL,     -- Customer number to output on the invoice
  transmission_billing bit null,        -- Trans rates in the rate table
  sales_tax_included bit null,          -- Sales tax is included in the defined rates, and is not added on when generating the invoice
  transmission_bulk bit null,           -- BULK1K rates in the rate table.  Rounds to the nearest 1000 tickets.
  modified_date datetime not null default getdate(),
  plat_map_prefix varchar(20) null,     -- The value_group.group_code prefix used to map a plat to a plat group name to group the invoice line items by
  consol_line_item bit not null default 0, -- Consolidate billing detail line items on the invoice by the line_item_text by not grouping by the term, rate, etc.
)

ALTER TABLE billing_output_config
  ADD CONSTRAINT billing_output_config_fk_customer
  FOREIGN KEY (customer_id) REFERENCES customer (customer_id)
go

ALTER TABLE billing_output_config
  ADD CONSTRAINT uk_boc_customer UNIQUE NONCLUSTERED (
    customer_id, center_group_id, billing_period, which_invoice
  )
go

create table billing_gl (
  billing_gl_id int identity primary key,
  charge_type varchar(20) not null, /* TAX, TRANSMISSION, NORMAL, ADJUSTMENT, etc. */
  period_type varchar(20) null, /* WEEK, MONTH, etc. */
  gl_code varchar(25) not null,
  description varchar(30) null,
  active bit not null default 1,
  modified_date datetime default getdate(),
)
go

create index billing_gl_period_type_charge_type on billing_gl(period_type, charge_type)
go

create table billing_adjustment (
  adjustment_id int identity primary key,-- Primary key
  adjustment_date datetime null,     -- Bill to apply adjustment to
  customer_id int not null,          -- Customer ID for adjustment
  client_id int null,                -- Optionally apply this to a client's subtotal
  added_date datetime not null default getdate(), -- Date the adjustment was added
  type varchar(10) not null,         -- PERCENT or AMOUNT or COMMENT
  description varchar(50) not null,  -- Description of adjustment
  amount money not null,             -- Amount (% or $) of adjustment,
  added_by int not null,             -- Employee who created the adjustment
  last_modified_by int not null,     -- Employee who last modified the adjustment
  modified_date datetime not null default getdate(), -- The date/time the adjustment was last modified
  active bit not null default 1,     -- If the adjustment is active/inactive
  which_invoice varchar(20) null,    -- Which invoice for thsi customer this adjustment appears on
  gl_code varchar(25) null,          -- The Solomon GL code to apply the charge to (null means use the highest value GL code on the invoice)
)

create index billing_adjustment_customer_id_adjustment_date on billing_adjustment(customer_id, adjustment_date)
create index billing_adjustment_adjustment_date on billing_adjustment(adjustment_date)


-- Billing run requests.  Executed runs will have a completed_date.
-- Only successful runs will have a bill_id assigned.
create table billing_run (
  run_id int identity primary key,   -- Primary key
  request_by int not null,           -- Employee ID of the requesting user
  request_date datetime not null,    -- When the billing run was added to the queue
  period_end_date datetime not null, -- Ending date of the billing period
  center_group_id int not null,      -- Call center group id used to run billing
  run_date datetime null,            -- When the billing run started execution
  completed_date datetime null,      -- When the billing run completed
  bill_id int null,                  -- Resulting billing_header.bill_id, or null for failed runs
  run_log text null,                 -- Log details from the billing run
  cancelled bit not null default 0,  -- True if the billing run was cancelled
  request_type varchar(1) null,      -- 'X' for re-eXport
  span varchar(20) null,             -- WEEK MONTH etc.

)

create index billing_run_completed_date on billing_run(completed_date)
create index billing_run_bill_id on billing_run(bill_id)


CREATE TABLE billing_cc_map (
  bcm_id integer identity (2000, 1) primary key,
  call_center varchar(20) NOT NULL,
  client_code varchar(10) NOT NULL,
  work_state varchar(2) NULL,
  work_county varchar(40) NULL,
  work_city varchar(40) NULL,
  billing_cc varchar(10) NULL
)
go

/* billing_rate maps a specific locate's client/status/etc. to
   a specific charge/rate per unit marked */
CREATE TABLE billing_rate (
  br_id integer identity (3000, 1) primary key,
  call_center varchar(20) NOT NULL, /* Call center this rate applies to */
  billing_cc varchar(10) NOT NULL,  /* Client code this rate applies to */
  status varchar(20) NOT NULL,      /* Status of the locates this rate applies to  ('*' for all) */
  parties int NOT NULL DEFAULT 1,   /* Number of parties on the locate's ticket.  See: billing_party_respect */
  rate money NULL,                  /* Charge/rate per unit marked */
  bill_code varchar(10) NULL,       /* Generic string flag for various special rating rules */
  bill_type varchar(10) NOT NULL,   /* Ticket type (NORMAL, EMERG, DAMAGE, AFTER, etc.) */
  area_name varchar(40) NULL,
  work_county varchar(40) NULL,
  line_item_text varchar(60) NULL,
  work_city varchar(40) NULL,
  modified_date datetime NOT NULL DEFAULT getdate(),
  which_invoice varchar(20) null,
  additional_rate money null,
  value_group_id int null,
  additional_line_item_text varchar(60) NULL
  /* New items added here must also go in billing_rate_history and the trigger billing_rate_iud */
)
go

CREATE TABLE billing_rate_history (
  br_id integer NOT NULL,
  call_center varchar(20) NOT NULL, /* Call center this rate applies to */
  billing_cc varchar(10) NOT NULL,  /* Client code this rate applies to */
  status varchar(20) NOT NULL,      /* Status of the locates this rate applies to  ('*' for all) */
  parties int NOT NULL DEFAULT 1,   /* Number of parties on the locate's ticket.  See: billing_party_respect */
  rate money NULL,                  /* Charge/rate per unit marked */
  bill_code varchar(10) NULL,       /* Generic string flag for various special rating rules */
  bill_type varchar(10) NULL,       /* Ticket type (NORMAL, EMERG, DAMAGE, AFTER, etc.) */
  area_name varchar(40) NULL,
  work_county varchar(40) NULL,
  line_item_text varchar(60) NULL,
  work_city varchar(40) NULL,
  modified_date datetime NOT NULL,      /* The date this record came into effect (the date is was inserted/updated [modified] in the real table) */
  archive_date datetime NOT NULL,    /* The date the record was archived (moved to history) */
  which_invoice varchar(20) null,
  additional_rate money null,
  user_name varchar(40) constraint user_name_constraint default system_user ,
  os_user_name varchar(40) null,
  computer_name varchar(40) constraint host_name_constraint default host_name(),
  value_group_id int null,
  additional_line_item_text varchar(60) NULL
)
go

ALTER TABLE billing_rate_history ADD CONSTRAINT PK_billing_rate_history PRIMARY KEY NONCLUSTERED (br_id, modified_date)

/* billing_party_respect determines, for a specific client, who else (what
   parties) on the ticket actually count as a party on the ticket.  For some
   clients, certain other parties on the ticket "dont count".  The count of
   other parties on a ticket matters since some clients are charged
   diffferently based on how many other parties are on that specific ticket. */
CREATE TABLE billing_party_respect (
  billing_term_id integer NOT NULL,      /* this is client_id in the misnamed client table */
  respect_oc_code varchar(20) NOT NULL,  /* this is code instead of ID, because we might need to respect non-clients */
  PRIMARY KEY (billing_term_id, respect_oc_code)
)
go

CREATE TABLE billing_exclude_work_for (
  billing_term_id integer NOT NULL,  /* this is client_id in the misnamed client table */
  work_done_for varchar(70) NOT NULL,
  PRIMARY KEY (billing_term_id, work_done_for)
)
go

CREATE TABLE billing_exclude_work_type (
  billing_term_id integer NOT NULL,  /* this is client_id in the misnamed client table */
  work_type varchar(70) NOT NULL,
  PRIMARY KEY (billing_term_id, work_type)
)
go

CREATE TABLE billing_header (
  bill_id integer identity (5000, 1) primary key,
  bill_run_date datetime NOT NULL,
  bill_start_date datetime NOT NULL,
  bill_end_date datetime NOT NULL,
  description varchar(100) NULL,
  n_tickets integer NULL,
  n_locates integer NULL,
  totalamount money NULL,
  committed BIT NOT NULL DEFAULT 0,
  committed_date datetime NULL,
  center_group_id int NULL,
  committed_by_emp int NULL,
  period_type varchar(10) default 'WEEK', -- WEEK MONTH HALF DAY
  app_version varchar(15) NULL,           -- Version of the admin program that ran this
)
go

ALTER TABLE billing_invoice
  ADD CONSTRAINT billing_invoice_fk_billing_header
  FOREIGN KEY (billing_header_id) REFERENCES billing_header (bill_id)
go

CREATE TABLE billing_queue (
  queue_id int identity (12000, 1) not null primary key nonclustered,
  event_type varchar (10) not null,
  ticket_id int not null,
  locate_id int not null,
  event_date datetime not null,
  insert_date datetime not null default getdate(),
  locate_hours_id int null,
  locate_status_id int null,
  failed_processing bit not null default 0
)
go

create index billing_queue_insert_date on billing_queue(insert_date)


CREATE TABLE billing_transaction (
  transaction_id int identity (13000, 1) not null primary key nonclustered,
  ticket_id int not null,
  locate_id int null,
  locate_status_id int null,
  call_center varchar(20) not null,
  event_date datetime not null,   /* When locate/hours/units change was made that caused billing event */
  effective_date datetime not null,  /* When billing module processed the billing event*/
  amount decimal(19,4) not null,
  description varchar(100) not null,
)
go

create index billing_transaction_ticket_id on billing_transaction(ticket_id)
create index billing_transaction_locate_id on billing_transaction(locate_id)
create index billing_transaction_event_date on billing_transaction(event_date)
create index billing_transaction_locate_status_id on billing_transaction(locate_status_id)


CREATE TABLE billing_detail (
  billing_detail_id integer identity (8000, 1) primary key,
  bill_id integer not null,
  locate_id integer not null,  /* to mark committed with and pull fields */
  billing_cc varchar(10) not null,
  ticket_number varchar(20) not null,
  work_county varchar(40) null,
  work_state varchar(2) null,
  work_city varchar(40) null,
  work_address_number varchar(10) null,
  work_address_number_2 varchar(10) null,
  work_address_street varchar(45) null,
  ticket_type varchar(30) null,
  transmit_date datetime null,
  due_date datetime null,
  status varchar(5) not null,
  qty_marked integer null,  /* also called Units Marked */
  closed_how varchar(10) null,
  closed_date datetime null,
  bill_code varchar(10) null,
  bucket varchar(60) not null,  /*  Emergency Notice, One Party */
  price money not null,  /* extended */
  con_name varchar(50) null,      /* name of contractor, excavator */
  client_id integer not null,
  locator_id int null,
  work_done_for varchar(50) null, /* ticket.company field */
  work_type varchar(90) null,
  work_lat DECIMAL(9,6) null,
  work_long DECIMAL(9,6) null,
  rate money not null,
  work_cross varchar(100) null,
  call_date datetime null,
  initial_status_date datetime null,
  emp_number varchar(15) null,
  short_name varchar(20) null,
  work_description varchar(255) null,  /* Don't store the full 1500 characters */
  area_name varchar(40) null,
  transmit_count integer null,
  hours DECIMAL(9,3) null,
  line_item_text varchar(60) null,
  revision varchar(20) null,
  map_ref varchar(60) null,
  locate_hours_id int null,
  qty_charged int null,
  which_invoice varchar(20) null,
  tax_name varchar(40) null,
  tax_rate decimal(6,4) null,
  tax_amount money null,
  raw_units int null,
  update_of varchar(20) null,
  high_profile bit not null default 0,
  plat varchar(20) null,
  initial_arrival_date datetime null,
  units_marked int null, /* Sum of all locate_hours.units_marked values for this locate (usually, # of feet) */
  work_remarks varchar(255) null,  /* Just the first 255 chars, not the full 1200 */
  locate_added_by varchar(8) null,
  rate_value_group_id int null,
  additional_line_item_text varchar(60) null
)
go

create index billing_header_date on billing_header(bill_run_date)

create index billing_detail_locate_id on billing_detail(locate_id)
create index billing_detail_bill_id on billing_detail(bill_id)
create index billing_detail_locate_hours_id on billing_detail(locate_hours_id)

create table billing_transmission_detail (
  bill_trans_detail_id int identity(65001,1) not null primary key,
  bill_id integer not null, /* foreign key link to billing_header */
  customer_id int not null,
  which_invoice varchar(20) null,
  billing_cc varchar(10) not null, /* term code */
  client_id integer not null,
  ticket_format varchar(20) NULL,
  ticket_version_id integer not null,
  ticket_id integer not null,
  transmit_date datetime not null,
  ticket_image_fragment varchar(250),
  ticket_source varchar(20) not null,
  locator_id integer null,
  locate_closed bit null,
  plat varchar(20) null,
  plat_group varchar(30) null,
  locate_id int null,
  rate money null,
  line_item_text varchar(60) null,
  bill_city varchar(40) null,
  ticket_number varchar(20) not null,
  work_city varchar(40),
  revision varchar(20) null
)
GO

create index billing_transmission_detail_bill_id_cust_id on billing_transmission_detail(bill_id, customer_id)
GO

create table billing_unit_conversion (      -- Rules on how to convert units marked to units billed
  unit_conversion_id integer identity(38000,1) primary key,
  unit_type varchar(10) null,               -- how the marked units are measured (units, feet, moves, ...)
  first_unit_factor integer not null default 1, -- how many units marked count as first unit billed
  rest_units_factor integer null,           -- how many units marked between remaining units billed (if different)
  modified_date datetime not null default getdate(),
  active bit not null default 1,
  insert_date datetime not null default getdate(),
  modified_by integer null,
  added_by int null
)

create table billing_scc_invoice (
  scci_id integer identity(45000,1) primary key,
  state varchar(2) NOT NULL,
  county varchar(40) NOT NULL,
  city varchar(40) NOT NULL,
  which_invoice varchar(20) null,
  active bit not null default 1,
  insert_date datetime not null default getdate(),
  modified_by integer null,
  subcategory varchar(50) null,
  customer_id int null    -- Optionally make this apply to only one cust.
)
create index billing_scc_invoice_scc on billing_scc_invoice(state, county, city)

create table billing_scc_tax (
  scci_id integer identity(85000,1) primary key,
  state varchar(2) NOT NULL,
  county varchar(40) NOT NULL,
  city varchar(40) NOT NULL,
  tax_name varchar(40) not null,
  tax_rate decimal(6,4) null,
  active bit not null default 1,
  insert_date datetime not null default getdate(),
  modified_by integer null
)
create index billing_scc_tax_scc on billing_scc_tax(state, county, city)


create table billing_breakdown (
  billing_breakdown_id int identity(24000,1) primary key,
  output_config_id int not null,
  explanation varchar(60) not null,
  ratio decimal(6,4) not null
)


create index billing_breakdown_oc on billing_breakdown(output_config_id);

create table billing_performance_log (
  billing_run_id int not null,
  center_group_id int not null,
  phase_id int not null,
  parent_phase_id int,
  phase_name varchar(128) not null,
  start_time datetime not null,
  phase_time int not null,
  phase_percent numeric(10, 4)
)
go

ALTER TABLE billing_performance_log ADD CONSTRAINT PK_billing_performance_log PRIMARY KEY NONCLUSTERED (billing_run_id, phase_id)

create table value_group (
  value_group_id integer identity (11000, 1) primary key,
  group_code varchar(30) not null,
  comment varchar(200) null,
)
go

create unique index value_group_group_code on value_group(group_code)

create table value_group_detail (
  value_group_id integer not null,
  match_type varchar(15) not null,  /* STARTS ENDS CONTAINS IS */
  match_value varchar(50) not null,
)
go

ALTER TABLE value_group_detail ADD CONSTRAINT PK_value_group_detail PRIMARY KEY NONCLUSTERED (value_group_id, match_type, match_value)

create index value_group_detail_value_group_id on value_group_detail(value_group_id)

create unique index value_group_detail_value_group_id_match_type_match_value on value_group_detail(value_group_id, match_type, match_value)

create table center_group (
  center_group_id integer identity (10000, 1) primary key,
  group_code varchar(30) not null,
  comment varchar(200) null,
  show_for_billing bit not null default 0,
  modified_date datetime not null default getdate(),
  active bit NOT NULL DEFAULT 1,
)
go

create unique index center_group_group_code on center_group(group_code)
create index center_group_modified_date on center_group(modified_date)

ALTER TABLE billing_output_config
  ADD CONSTRAINT billing_output_config_fk_center_group
  FOREIGN KEY (center_group_id) REFERENCES center_group (center_group_id)
go


create table center_group_detail (
  center_group_id integer not null,
  call_center varchar(20) not null,
  modified_date datetime not null default getdate(),
  active bit NOT NULL DEFAULT 1,
  center_id int identity (1000, 1) primary key,
)
go

create unique index center_group_detail_center_group_id_call_center on center_group_detail(center_group_id, call_center)
create index center_group_detail_modified_date on center_group_detail(modified_date)


create table term_group (
  term_group_id integer identity (9000, 1) primary key,
  center_group_id integer null,
  group_code varchar(30) not null,
  comment varchar(200) null,
  modified_date datetime not null default getdate(),
  active bit NOT NULL DEFAULT 1,
)
go

create unique index term_group_group_code on term_group(group_code)
create index term_group_modified_date on term_group(modified_date)


create table term_group_detail (
  term_group_id integer not null,
  term varchar(20) not null,
  modified_date datetime not null default getdate(),
  active bit NOT NULL DEFAULT 1,
  term_id int identity (3000, 1) primary key
)
go

create unique index term_group_detail_term_group_id_term on term_group_detail(term_group_id, term)
create index term_group_detail_modified_date on term_group_detail(modified_date)

/* Use this to store system-wide configuration or state data, including
"next ID" data obtained from NextUniqueID, global enabling of features, etc. */
create table configuration_data (
  name varchar(25) not null primary key,
  value varchar(255) null,
  modified_date datetime not null default getdate(),
  editable bit NOT NULL DEFAULT 0,
)


/* generic notes table         */
/* Foreign type values:        */
/* 1 = Ticket                  */
/* 2 = Damage                  */
/* 3 = Damage 3rd Party Claims */
/* 4 = Damage Litigation       */
/* 5 = Locate                  */

CREATE TABLE notes (
  notes_id integer identity (11000, 1) primary key,
  foreign_type tinyint NOT NULL,  /* which table note goes with */
  foreign_id integer NOT NULL,  /* ID in that table */
  entry_date datetime NOT NULL DEFAULT getdate(),
  modified_date datetime NOT NULL DEFAULT getdate(),
  uid int NOT NULL, /* This is a user ID not an employee ID. */
  active bit NOT NULL DEFAULT 1,
  note varchar(8000) NOT NULL,  /* Change QMConst.MaxNoteLength if this ever changes */
  sub_type integer null
)

-- This index should be removed, it is not selective and is probably useless
create index notes_foreign on notes(foreign_type, foreign_id)
create index notes_foreign_id_foreign_type ON notes(foreign_id, foreign_type)
create index notes_entry_date on notes (entry_date)

/* *********************************************************************** */

/* Images and other attachments
   foreign_type is as in the notes table (references a ticket_id, damage_id, etc.)
   There is a attachment_id, and also globally unique candidate key field filename
   since we need to assign filename on the server (not use the temp-key mechanism
   in the client app).  It should be posssible to upload attachments before syncing
   the actual attachment records.
*/

create table attachment (
  attachment_id int identity (2000000,1) primary key,
  filename varchar(64) not null,  -- create these in a way that makes them unique
  foreign_type tinyint not null,
  foreign_id integer not null,
  orig_filename varchar(100) not null,
  extension varchar(10) not null,  -- so we dont have to repeatedly parse it out
  orig_file_mod_date datetime not null,
  attach_date datetime not null, -- local timestamp when the attachment occured
  upload_date datetime null, -- local timestamp when file was uploaded via FTP
  comment varchar(200) null,
  size integer not null,  -- in bytes
  attached_by int not null, -- The emp_id the user that created the attachment
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  source varchar(8) null,
  file_hash varchar(40) null, -- SHA1 hash
  background_upload bit null,
  doc_type varchar(15) null,
  upload_machine_name varchar(25) null
)

create index attachment_foreign_id on attachment(foreign_id)
create unique index attachment_filename on attachment(filename)
create index attachment_modified_date on attachment(modified_date)
create index attachment_upload_date on attachment (upload_date)
create index attachment_attach_date on attachment (attach_date)
create index attachment_attached_by on attachment (attached_by)

create table upload_location (
  location_id int identity (500, 1) primary key,
  description varchar(30) not null,
  server varchar(50) not null,
  password varchar(20),
  username varchar(20),
  directory varchar(150),
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  group_name varchar(15) null,
  port int not null default 21
)

alter table upload_location
  add constraint port_in_range check(port >= 1 and port <= 65535)

create table upload_file_type (
  upload_file_type_id int identity (2200, 1) primary key,
  extension varchar(10) not null,
  max_kilobytes int null,
  size_error_message varchar(200),
  active bit not null default 1,
  modified_date datetime not null default getdate(),
)

create unique index upload_file_type_extension on upload_file_type(extension)


/* *********************************************************************** */

CREATE TABLE responder_queue (
  locate_id int NOT NULL,
  insert_date datetime NOT NULL DEFAULT getdate(),
  ticket_format varchar(20) NULL,
  client_code varchar(20) NULL,
  do_not_respond_before datetime NULL,
  constraint pk_responder_queue primary key nonclustered (locate_id, insert_date)
)

/* *********************************************************************** */

CREATE TABLE responder_multi_queue (
  respond_to varchar(20) NOT NULL,
  locate_id int NOT NULL,
  insert_date datetime NOT NULL DEFAULT getdate(),
  ticket_format varchar(30) null,
  client_code varchar(20) null,
  constraint pk_responder_multi_queue primary key nonclustered (locate_id, respond_to, insert_date)
)

/* *********************************************************************** */

CREATE TABLE response_log (
  response_id integer identity (60000, 1) primary key,
  locate_id int NOT NULL,
  response_date datetime NOT NULL,
  call_center varchar(20) NOT NULL,
  status varchar(5) NOT NULL,
  response_sent varchar(15) NOT NULL, /* whatever we translated the status to */
  success bit NOT NULL,
  reply varchar(40)
)

CREATE INDEX response_log_response_date on response_log(response_date)
CREATE INDEX response_log_locate_id on response_log(locate_id)

/* responders that get acknowledgements can use this table to associate a
 * "reference number" with a response_id (foreign key response_log). */
create table response_ack_waiting (
     response_id int not null,
     reference_number varchar(40) not null,
     call_center varchar(20) not null,
	 constraint pk_response_ack_waiting primary key nonclustered (response_id)
)

/* response_ack_context (replaces "pickle" files) */
create table response_ack_context (
    filename varchar(80) not null, -- filename as returned by the server
	response_id integer not null,
	locate_id integer not null,
	ticket_id integer not null,
	call_center varchar(20) not null,
	client_code varchar(10) not null,
	resp_type varchar(10) not null,
	insert_date datetime not null default getdate(),
	ticket_number varchar(20) not null,
    constraint pk_response_ack_context primary key (call_center, filename, locate_id),
	constraint response_ack_context_fk_response_id foreign key (response_id)
	  references response_log(response_id)
);


CREATE TABLE holiday (
  cc_code varchar(20) NOT NULL, /* call center code or '*' for all */
  holiday_date datetime NOT NULL,
  billing_holiday bit NOT NULL DEFAULT 1,
  PRIMARY KEY(cc_code, holiday_date)
)


CREATE TABLE oldest_open (
  oldest_open_locate int NOT NULL,
  oldest_open_assignment int NOT NULL,
  oldest_open_ticket int NOT NULL,
  PRIMARY KEY(oldest_open_locate)   -- to keep the optimizer happy
  -- This table has only one row.
)

CREATE TABLE oldest_open_log (
  oldest_open_locate int NOT NULL,
  oldest_open_assignment int NOT NULL,
  oldest_open_ticket int NOT NULL,
  insert_date datetime default getdate() not null
)

alter table oldest_open_log add constraint PK_oldest_open_log
  primary key nonclustered (insert_date)

create table fax_message (
  fm_id int identity primary key,
  fax_dest varchar(60) null,
  call_center varchar(20) NOT NULL,
  queue_date datetime not null,
  previous_queue_date datetime null,   -- for the fax page, mostly
  fax_status varchar(20) NOT NULL,  -- pending, queued, fail, success, etc.
  complete_date datetime null,
  error_details text,
  send_type varchar(15),
  ext_doc_id varchar(8) null,
  ext_disposition varchar(500) null
)

create index fax_message_fax_status on fax_message(fax_status)
create index fax_message_queue_date on fax_message(queue_date)

create table fax_message_detail (
  fm_id int not null,
  locate_id int not null,
  ticket_id int not null,
  ls_id int not null,
  primary key(fm_id, ls_id)
)

create index fax_message_detail_ls_id on fax_message_detail(ls_id)


/* To add a new right definition, execute something like this:
   insert into right_definition (right_description, entity_data, right_type, modifier_desc)
     values ('Tickets - Edit Arrivals', 'TicketsEditArrivals', 'General', 'Limitation does not apply to this right.')
 */
create table right_definition (
  right_id int identity,
  right_description varchar(50) NOT NULL,
  right_type varchar(10) NULL, /* Report, General */
  entity_data varchar(50) NULL, /* The English-language name of the right that is referenced in the code instead of the ID */
  parent_right_id int NULL,
  modifier_desc varchar(150) NULL, /* The function of the employee_right.modifier field for this right */
  modified_date datetime not null default getdate(),
  primary key (right_id)
)
Go

create trigger right_definition_u on right_definition
for update
not for replication
AS
  update right_definition
   set modified_date = getdate()
   where right_id in (select right_id from inserted)
Go

create unique index right_definition_entity_data on
   right_definition(entity_data)
go

create table employee_right (
  emp_right_id int identity not null,
  emp_id int not null,
  right_id int not null,
  allowed varchar(1) not null default '-',  -- Y, N, -
  limitation varchar(200) null,
  modified_date datetime not null default getdate(),
  constraint pk_employee_right primary key (emp_right_id)
)

CREATE unique INDEX employee_right_emp_id on employee_right(emp_id,right_id)
CREATE INDEX employee_right_right_id on employee_right(right_id)

alter table employee_right
    add constraint fk_employee_right_right foreign key  (right_id)
       references right_definition (right_id)

alter table employee_right
    add constraint fk_employee_right_emp foreign key  (emp_id)
       references employee (emp_id)
go

create trigger employee_right_u on employee_right
for update
not for replication
AS
update employee_right
 set modified_date = getdate()
 where emp_right_id in (select emp_right_id from inserted)
go

create trigger employee_right_i on employee_right
for insert
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from inserted)
Go

create trigger employee_right_u_2 on employee_right
for update
not for replication
as
  if update(allowed) or update(limitation) begin
    update employee
      set rights_modified_date = GetDate()
    where
      emp_id in (select emp_id from inserted)
  end
Go

create trigger employee_right_d on employee_right
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from deleted)
Go


create table asset_type (
  asset_code varchar(8) primary key,
  description varchar(25) not null,
  active bit not null default 1,
  modified_date datetime not null default getdate(),
)
go
create index asset_type_asset_code on asset_type(asset_code)
create index asset_type_modified_date on asset_type(modified_date)

create table asset (
  asset_id integer identity (80000, 1) primary key,
  asset_code varchar(8) not null,
  asset_number varchar(25),
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  comment varchar(40) null,
  condition varchar(8) null,
  solomon_profit_center varchar(25) null,
  serial_num varchar(20) null,
  description varchar(60) null,
  cost money null,
  -- !! Items added here must also go in asset_history and the trigger
)
go
create index asset_asset_id on asset(asset_id)
create index asset_asset_number on asset(asset_number)

create table asset_history (
  asset_id integer not null,
  asset_code varchar(8) not null,
  asset_number varchar(25),
  active bit not null,
  modified_date datetime not null,
  comment varchar(40) null,
  condition varchar(8) null,
  archive_date datetime not null,
  solomon_profit_center varchar(25) null,
  serial_num varchar(20) null,
  description varchar(60) null,
  cost money null,
)
go

alter table asset_history add constraint PK_asset_history
  primary key nonclustered (asset_id, archive_date)

create table asset_assignment (
  assignment_id integer identity (3000, 1) primary key,
  asset_id integer not null,
  emp_id integer not null,
  active bit not null default 1,
  modified_date datetime not null default getdate()
  -- Items added here must also go in asset_assignment_history and the trigger
)
go
create index asset_assignment_emp_id on asset_assignment(emp_id)
create index asset_assignment_asset_id on asset_assignment(asset_id)

create table asset_assignment_history (
  assignment_id integer not null,
  asset_id integer not null,
  emp_id integer not null,
  active bit not null,
  modified_date datetime not null,
  archive_date datetime not null
)
go
ALTER TABLE asset_assignment_history 
  ADD CONSTRAINT asset_assignment_history_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

alter table asset_assignment_history add constraint PK_asset_assignment_history
  primary key nonclustered (assignment_id, archive_date)

create table status_group (
  sg_id integer identity (94000, 1) primary key,
  sg_name varchar(40) NOT NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1,
  right_id int null
)
go

create table status_group_item (
  sg_item_id integer identity (95000, 1) primary key,
  sg_id integer not null,
  status varchar(5) NOT NULL,
  modified_date datetime NOT null DEFAULT getdate(),
  active bit NOT NULL DEFAULT 1
)
go

create table audit_methods (
  audit_method_id integer identity(98000,1) primary key,
  name varchar(50) not null,
)
go

create table jobsite_arrival (
  arrival_id integer identity(43000, 1) not null primary key nonclustered,
  ticket_id integer null,
  arrival_date datetime not null,  /* user's time zone */
  emp_id integer not null,
  entry_method varchar(10) not null, /* values: CLOCK, ENTERED */
  added_by integer not null,
  deleted_by integer null,
  deleted_date datetime null,      /* user's time zone */
  active bit not null default 1,
  modified_date datetime not null default GetDate(),
  damage_id int NULL,
  location_status varchar(15) NULL
)
go
create index jobsite_arrival_ticket_id on jobsite_arrival(ticket_id)
create index jobsite_arrival_arrive_emp_id on jobsite_arrival(emp_id, arrival_date)
create index jobsite_arrival_damage_id on jobsite_arrival(damage_id)
go

/* *********************************************************************** */


/* Indexes, foreign keys, etc.  These are kept at the end, so that
   a database can be created without them if needed.  */

ALTER TABLE ticket_version
  ADD CONSTRAINT ticket_version_fk_ticket
  FOREIGN KEY (ticket_id) REFERENCES ticket(ticket_id)

ALTER TABLE locate
  ADD CONSTRAINT locate_fk_ticket
  FOREIGN KEY (ticket_id) REFERENCES ticket(ticket_id)

ALTER TABLE locate
  ADD CONSTRAINT locate_fk_client
  FOREIGN KEY (client_id) REFERENCES client(client_id)

ALTER TABLE locate
  ADD CONSTRAINT locate_fk_status
  FOREIGN KEY (status) REFERENCES statuslist(status)

ALTER TABLE nonclient_locate
  ADD CONSTRAINT nonclient_locate_fk_ticket
  FOREIGN KEY (ticket_id) REFERENCES ticket(ticket_id)

ALTER TABLE locate_hours
  ADD CONSTRAINT locate_hours_fk_locate
  FOREIGN KEY (locate_id) REFERENCES locate(locate_id)

ALTER TABLE locate_plat
  ADD CONSTRAINT locate_plat_fk_locate
  FOREIGN KEY (locate_id) REFERENCES locate(locate_id)
GO

ALTER TABLE assignment
  ADD CONSTRAINT assignment_fk_locate
  FOREIGN KEY (locate_id) REFERENCES locate(locate_id)

ALTER TABLE assignment
  ADD CONSTRAINT assignment_fk_employee
  FOREIGN KEY (locator_id) REFERENCES employee(emp_id)

ALTER TABLE client
  ADD CONSTRAINT client_fk_office
  FOREIGN KEY (office_id) REFERENCES office(office_id)

ALTER TABLE client
  ADD CONSTRAINT client_fk_call_center
  FOREIGN KEY (call_center) REFERENCES call_center(cc_code)

ALTER TABLE client
  ADD CONSTRAINT client_fk_update_call_center
  FOREIGN KEY (update_call_center) REFERENCES call_center(cc_code)

ALTER TABLE client
  ADD CONSTRAINT client_fk_status_group
  FOREIGN KEY (status_group_id) REFERENCES status_group(sg_id)

ALTER TABLE client
  ADD CONSTRAINT client_fk_customer
  FOREIGN KEY (customer_id) REFERENCES customer(customer_id)

ALTER TABLE area
  ADD CONSTRAINT area_fk_employee
  FOREIGN KEY (locator_id) REFERENCES employee(emp_id)

ALTER TABLE area
  ADD CONSTRAINT area_fk_map
  FOREIGN KEY (map_id) REFERENCES map(map_id)

ALTER TABLE map_cell
  ADD CONSTRAINT map_cell_fk_map_page
  FOREIGN KEY (map_id, map_page_num) REFERENCES map_page(map_id, map_page_num)

ALTER TABLE map_cell
  ADD CONSTRAINT map_cell_fk_area
  FOREIGN KEY (area_id) REFERENCES area(area_id)

ALTER TABLE county_area
  ADD CONSTRAINT county_area_fk_area
  FOREIGN KEY (area_id) REFERENCES area(area_id)

ALTER TABLE summary_detail
  ADD CONSTRAINT summary_detail_fk_summary_header
  FOREIGN KEY (summary_header_id) REFERENCES summary_header(summary_header_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_history
  ADD CONSTRAINT damage_history_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_employee
  FOREIGN KEY (emp_id) REFERENCES employee(emp_id)

ALTER TABLE damage_invoice
  ADD CONSTRAINT damage_invoice_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_invoice
  ADD CONSTRAINT damage_invoice_fk_damage_estimate
  FOREIGN KEY (estimate_id) REFERENCES damage_estimate(estimate_id)

ALTER TABLE damage_litigation
  ADD CONSTRAINT damage_litigation_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_litigation
  ADD CONSTRAINT damage_litigation_fk_carrier
  FOREIGN KEY (carrier_id) REFERENCES carrier(carrier_id)

ALTER TABLE damage_bill
  ADD CONSTRAINT damage_bill_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_bill
  ADD CONSTRAINT damage_bill_fk_employee
  FOREIGN KEY (modified_by) REFERENCES employee(emp_id)

ALTER TABLE damage_third_party
  ADD CONSTRAINT damage_third_party_fk_damage
  FOREIGN KEY (damage_id) REFERENCES damage(damage_id)

ALTER TABLE damage_third_party
  ADD CONSTRAINT damage_third_party_fk_carrier
  FOREIGN KEY (carrier_id) REFERENCES carrier(carrier_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_damage_litigation
  FOREIGN KEY (litigation_id) REFERENCES damage_litigation(litigation_id)

ALTER TABLE damage_estimate
  ADD CONSTRAINT damage_estimate_fk_damage_third_party
  FOREIGN KEY (third_party_id) REFERENCES damage_third_party(third_party_id)

/* this stops us from running old billing data in test databases.  Bad.
ALTER TABLE billing_detail
  ADD CONSTRAINT billing_detail_fk_locate
  FOREIGN KEY (locate_id) REFERENCES locate(locate_id)
 */

ALTER TABLE billing_detail
  ADD CONSTRAINT billing_detail_fk_bill
  FOREIGN KEY (bill_id) REFERENCES billing_header(bill_id)

ALTER TABLE billing_rate
  ADD CONSTRAINT billing_rate_fk_call_center
  FOREIGN KEY (call_center) REFERENCES call_center(cc_code)

ALTER TABLE billing_rate
  ADD CONSTRAINT billing_rate_fk_value_group
  FOREIGN KEY (value_group_id) REFERENCES value_group(value_group_id)

ALTER TABLE billing_rate
  ADD CONSTRAINT constr_billing_rate_unique
  UNIQUE (call_center, billing_cc, status, parties, bill_type, work_county, area_name, work_city, value_group_id)

ALTER TABLE billing_breakdown
  ADD CONSTRAINT billing_breakdown_fk_oc
  FOREIGN KEY (output_config_id) REFERENCES billing_output_config(output_config_id)


ALTER TABLE asset
  ADD CONSTRAINT asset_fk_asset_code
  FOREIGN KEY (asset_code) REFERENCES asset_type(asset_code)

ALTER TABLE asset_assignment
  ADD CONSTRAINT asset_assignment_fk_asset
  FOREIGN KEY (asset_id) REFERENCES asset(asset_id)

ALTER TABLE asset_assignment
  ADD CONSTRAINT asset_assignment_fk_employee
  FOREIGN KEY (emp_id) REFERENCES employee(emp_id)
go

ALTER TABLE attachment
  ADD CONSTRAINT attachment_fk_employee
  FOREIGN KEY (attached_by) REFERENCES employee(emp_id)
go

/* This breaks some of the tests
ALTER TABLE billing_party_respect
  ADD CONSTRAINT billing_party_respect_fk_client
  FOREIGN KEY (billing_term_id) REFERENCES client(client_id)
  */


ALTER TABLE notes
  ADD CONSTRAINT notes_fk_users
  FOREIGN KEY (uid) REFERENCES users(uid)

ALTER TABLE status_group
  ADD CONSTRAINT status_group_fk_right_definition
  FOREIGN KEY (right_id) REFERENCES right_definition(right_id)

ALTER TABLE profit_center 
  ADD CONSTRAINT profit_center_fk_employee FOREIGN KEY(manager_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 

ALTER TABLE employee 
  ADD CONSTRAINT employee_fk_users_create FOREIGN KEY(create_uid) REFERENCES users(uid) NOT FOR REPLICATION 

ALTER TABLE employee 
  ADD CONSTRAINT employee_fk_users_modified FOREIGN KEY(modified_uid) REFERENCES users(uid) NOT FOR REPLICATION 

ALTER TABLE employee_history 
  ADD CONSTRAINT employee_history_fk_users_create FOREIGN KEY(create_uid) REFERENCES users(uid) NOT FOR REPLICATION 

ALTER TABLE employee_history 
  ADD CONSTRAINT employee_history_fk_users_modified FOREIGN KEY(modified_uid) REFERENCES users(uid) NOT FOR REPLICATION 
GO

/* ********************************** Web App tables  ****** */

/* ============================================================ */
/*   Table: employee_office - cross reference for employees / offices */
/* ============================================================ */
create table dbo.employee_office
(
    emp_id            int                   not null,      -- employee id
    office_id         int                   not null,      -- office id
    constraint pk_employee_office primary key (emp_id, office_id)
)
go

create table timesheet_entry
(
  entry_id int identity(15000, 1),
  work_emp_id int not null,
  work_date datetime not null,
  modified_date datetime not null default getdate(),

  status varchar(8) not null,  /* SUBMIT, ACTIVE, OLD, INVALID */
  entry_date datetime not null,
  entry_date_local datetime not null,
  entry_by int not null,
  approve_date datetime null,
  approve_date_local datetime null,
  approve_by int null,
  final_approve_date datetime null,
  final_approve_date_local datetime null,
  final_approve_by int null,

  work_start1 datetime null,
  work_stop1 datetime null,
  work_start2 datetime null,
  work_stop2 datetime null,
  work_start3 datetime null,
  work_stop3 datetime null,
  work_start4 datetime null,
  work_stop4 datetime null,
  work_start5 datetime null,
  work_stop5 datetime null,

  callout_start1 datetime null,
  callout_stop1 datetime null,
  callout_start2 datetime null,
  callout_stop2 datetime null,
  callout_start3 datetime null,
  callout_stop3 datetime null,
  callout_start4 datetime null,
  callout_stop4 datetime null,
  callout_start5 datetime null,
  callout_stop5 datetime null,
  callout_start6 datetime null,
  callout_stop6 datetime null,

  miles_start1 int null,
  miles_stop1 int null,

  vehicle_use varchar(5) default 'NA' not null,

  reg_hours decimal(5,2) null,
  ot_hours decimal(5,2) null,
  dt_hours decimal(5,2) null,
  callout_hours decimal(5,2) null,
  vac_hours decimal(5,2) null,
  leave_hours decimal(5,2) null,
  br_hours decimal(5,2) null,
  hol_hours decimal(5,2) null,
  jury_hours decimal(5,2) null,
  floating_holiday bit null,
  rule_ack_date datetime null,
  rule_id_ack int null,
  emp_type_id int null,
  work_pc_code varchar(15) null,
  reason_changed int null,
  lunch_start datetime null,
  source varchar(10) null,

  work_start6 datetime null,
  work_stop6 datetime null,
  work_start7 datetime null,
  work_stop7 datetime null,
  work_start8 datetime null,
  work_stop8 datetime null,
  work_start9 datetime null,
  work_stop9 datetime null,
  work_start10 datetime null,
  work_stop10 datetime null,
  work_start11 datetime null,
  work_stop11 datetime null,
  work_start12 datetime null,
  work_stop12 datetime null,
  work_start13 datetime null,
  work_stop13 datetime null,
  work_start14 datetime null,
  work_stop14 datetime null,
  work_start15 datetime null,
  work_stop15 datetime null,
  work_start16 datetime null,
  work_stop16 datetime null,

  callout_start7 datetime null,
  callout_stop7 datetime null,
  callout_start8 datetime null,
  callout_stop8 datetime null,
  callout_start9 datetime null,
  callout_stop9 datetime null,
  callout_start10 datetime null,
  callout_stop10 datetime null,
  callout_start11 datetime null,
  callout_stop11 datetime null,
  callout_start12 datetime null,
  callout_stop12 datetime null,
  callout_start13 datetime null,
  callout_stop13 datetime null,
  callout_start14 datetime null,
  callout_stop14 datetime null,
  callout_start15 datetime null,
  callout_stop15 datetime null,
  callout_start16 datetime null,
  callout_stop16 datetime null,

  pto_hours decimal(5,2) null,
  per_diem bit null default 0
)

ALTER TABLE timesheet_entry
ADD CONSTRAINT PK_timesheet_entry_entry_id PRIMARY KEY NONCLUSTERED (entry_id)
GO
ALTER TABLE timesheet_entry 
  ADD CONSTRAINT timesheet_entry_fk_employee FOREIGN KEY(work_emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

create index timesheet_entry_work_emp_id on timesheet_entry(work_emp_id, work_date)
create index timesheet_entry_work_date on timesheet_entry(work_date, work_emp_id)
create index timesheet_entry_modified_date on timesheet_entry(work_emp_id, modified_date)
create index timesheet_entry_approve_by on timesheet_entry(approve_by)
create index timesheet_entry_final_approve_by on timesheet_entry(final_approve_by)


create table timesheet_export
(
  export_id int identity(12000,1) primary key,
  profit_center varchar(15) not null,
  week_ending datetime not null,
  export_by int not null,
  export_date datetime not null,
)
go

create index timesheet_export_profit_center_week_ending on timesheet_export(profit_center, week_ending)
create index timesheet_export_week_ending on timesheet_export(week_ending)
go

/* Used to store employees already exported into the Dynamics project
   controller system, to prevent duplicate exports.  See export_employees() */
create table exported_employees (
  emp_id int not null,
  emp_number varchar(15) null,
  short_name varchar(30) null,
  payroll_company_code varchar(20) null,
  emp_pc_code varchar(15) null,
  manager_emp_number varchar(15) null,
  emp_type varchar(15) null,
  vehicle varchar(15) null,
  exported_employees_id integer identity primary key nonclustered
)
go

create index exported_employees_emp_id on exported_employees(emp_id)
go
ALTER TABLE exported_employees 
  ADD CONSTRAINT exported_employees_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO


/* Used to store projects already exported into the Dynamics project
   controller system, to prevent duplicate exports.  See export_projects() */
create table exported_projects (
  payroll_company_code varchar(20) not null, /* locating_company.payroll_company_code = "Customer ID" */
  pc_code varchar(15) not null,              /* customer.pc_code = "GL Subaccount" */
  manager_emp_number varchar(15) not null,   /* Manager employee number for this PC (employee.repr_pc_code) = "PC Manager" */
  customer_number varchar(20) not NULL,      /* customer.customer_number = "Project ID" */
  customer_name varchar(40) NOT NULL,    /* customer.customer_name = "Project Description" */
)
go

alter table exported_projects
  add constraint PK_exported_projects
  primary key nonclustered (payroll_company_code, pc_code, manager_emp_number, customer_number)
go

/* Used to store tasks already exported into the Dynamics project
   controller system, to prevent duplicate exports.  See export_tasks() */
create table exported_tasks (
  customer_number varchar(20) not null,   /* customer.customer_number = "Project ID" */
  task_id varchar(30) not null,       /* PayrollPcCode(3) + MgrEmpNum(6) + LocEmpNum(6) = "Task ID" */
  short_name varchar(30) not null,        /* employee.short_name = "Task Description" */
  manager_emp_number varchar(15) not null /* employee.report_to.employee_number = "Supervisor Employee Number" */
)
go

alter table exported_tasks
  add constraint PK_exported_tasks
  primary key nonclustered (customer_number, task_id, short_name, manager_emp_number)
go

/* Used to store damage claims already exported into the Dynamics project
   controller system, to prevent duplicate damage claims.  See export_damages() */
create table exported_damages (
  short_period varchar(10) not null,
  estimate_id integer not null,
  project varchar(20) not null,
  task varchar(30) not null,
  claim_number integer not null,
  invoice_code varchar(10) null,
  liability_amount decimal(12,2) null,
  change_in_liability_amount decimal(12,2) null
 )
go

alter table exported_damages
  add constraint PK_exported_damages
  primary key nonclustered (short_period, estimate_id)
go
create index exported_damages_short_period on exported_damages(short_period)
create index exported_damages_estimate_id on exported_damages(estimate_id)
create index exported_damages_claim_number on exported_damages(claim_number)
go

-- History of exported damages - shown on the Damage Details Accrual History tab
create table exported_damages_history(
  short_period varchar(20) not null,
  estimate_id int not null,
  project varchar(20) not null,
  task varchar(30) not null,
  claim_number int not null,
  invoice_code varchar(10) not null,
  liability_amount decimal(12, 2) null,
  change_in_liability_amount decimal(12, 2) null,
  insert_date datetime NOT null DEFAULT GetDate()
)
go

alter table exported_damages_history
  add constraint PK_exported_damages_history
  primary key nonclustered (short_period, estimate_id, project, task, invoice_code)
go
create index exported_damages_history_claim_number_insert_date on exported_damages_history (claim_number, insert_date)
go

--
create table hp_grid (
  hp_grid_id int primary key identity (8000,1),
  call_center varchar(20) not null,
  client_code varchar(30) not null,
  grid varchar(20) not null,
)
create index hp_grid_index on hp_grid(hp_grid_id)

--
create table ticket_grid (
  ticket_grid_id int primary key identity(9000,1),
  ticket_id int not null,
  grid varchar(20) not null,
)
create index ticket_grid_ticket_id on ticket_grid(ticket_id)


--
create table ticket_grid_log (
  ticket_id int not null,
  time_sent datetime not null default getdate(),
  client_code varchar(30) not null,
  ticket_version_id int not null,
  constraint pk_ticket_grid_log primary key nonclustered (ticket_version_id, client_code)
)
go

create index ticket_grid_log_time_sent on ticket_grid_log(time_sent)


--
create table ticket_ack_match (
  ticket_ack_match_id int identity(88000,1),
  call_center varchar(40) not null,
  match_string varchar(50) not null,
  work_priority integer,
  constraint pk_ticket_ack_match primary key nonclustered (ticket_ack_match_id)
)


create table error_report (
  error_report_id integer identity (200000, 1) primary key,
  emp_id int,
  occurred_date datetime,
  report_date datetime,
  severity int NOT NULL,
  error_text varchar(200),
  details text
)

create index error_report_emp_id on error_report(emp_id)

create index error_report_occurred_date on error_report(occurred_date)
ALTER TABLE error_report 
  ADD CONSTRAINT error_report_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION
GO

--
create table notification_email (
  email_id integer identity(99000,1) primary key,
  call_center varchar(10) not null,
  notification_type varchar(30) not null,
  email_address varchar(80),
)

create table notification_email_user (
    emp_id integer not null,
    email_type varchar(30) not null,
    email_destination varchar(250),
    primary key (emp_id, email_type)
)
ALTER TABLE notification_email_user 
  ADD CONSTRAINT notification_email_user_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

create table notification_email_by_client (
    notification_email_id integer identity(2000,1) primary key,
    client_id int not null,
    notification_type varchar(20) not null,
    email_address varchar(80) not null,
)

/* A cached list of locate status notification faxes that need to be sent */
create table fax_queue (
  ls_id int not null primary key,   /* The locate status change we need to notify about (notification actually uses the current values, not the values in this locate change record) */
  call_center varchar(20) not null, /* The call center of this locate's ticket.  Used to speed up filtered access to the fax queue. */
  send_type varchar(15)
)

/* These rules are used to determine which locate status changes need fax notification (insertion into fax_queue) */
create table fax_queue_rules (
  call_center varchar(20) not null,
  client_code varchar(20) not null,
  status varchar(5) not null,
  state varchar(5) not null,
  fqr_id integer identity(3400,1) primary key,
  status_message varchar(3000) null,
  send_type varchar(15) not null default 'FAXMAKER'
)

create index fax_queue_rules_call_center_client_code on fax_queue_rules(call_center, client_code)
alter table fax_queue_rules add constraint send_type_check check (send_type in ('FAXMAKER', 'EFAX'))
ALTER TABLE fax_queue_rules
  ADD CONSTRAINT constr_fax_queue_rules_unique
  UNIQUE (call_center, client_code, status, state)
go

/* These rules are used to determine which locate status changes need email notification (insertion into email_queue) */
create table email_queue_rules (
  eqr_id integer identity(1100,1) primary key,
  call_center varchar(20) not null,
  client_code varchar(20) not null,
  status varchar(5) not null,
  state varchar(5) not null,
  status_message varchar(3000) null
)

create index email_queue_rules_call_center_client_code on email_queue_rules(call_center, client_code)
ALTER TABLE email_queue_rules
  ADD CONSTRAINT constr_email_queue_rules_unique
  UNIQUE (call_center, client_code, status, state)
go


create table email_queue (
  ls_id int not null primary key,   /* The locate status change we need to notify about (notification actually uses the current values, not the values in this locate change record) */
  call_center varchar(20) not null, /* The call center of this locate's ticket.  Used to speed up filtered access to the email queue. */
)


/* This records each attempt to send an email from the email_queue.  Only locates
   matching the email_queue_rules with a valid email address will appear here, and
   only after the email report generation application runs for this call center. */
create table email_queue_result (
  eqr_id int identity primary key,
  ticket_id int not null,
  email_address varchar(60) not null,
  status varchar(20) not null,  -- Sent/Error/Bounce (eventually?)
  sent_date datetime null,
  error_details text null,
  ls_id int null,
)

create index email_queue_result_sent_date_status on email_queue_result(sent_date, status)
create index email_queue_result_ticket_id on email_queue_result(ticket_id)


create table report_log (
  request_id varchar(50) not null primary key,
  report_name varchar(30) not null,
  user_id int not null,
  params varchar(500),
  start_date datetime not null,
  finish_date datetime,
  engine_success bit,
  engine_details varchar(150),
  -- client side data
  client_success bit,
  client_elapsed int,  -- store # of seconsd
  client_details varchar(150),
  status varchar(15) not null default 'done',
  priority int null,
  timeout int null,
  request_date datetime null
)

create index report_log_start_date   on report_log(start_date)
create index report_log_status_pri   on report_log(status, priority)
create index report_log_request_date on report_log (request_date)
create index report_log_user_id      on report_log (user_id)

create table ticket_word (
    word varchar(40) not null,
    ticket_id integer not null
)

alter table ticket_word
 add constraint PK_ticket_word primary key clustered (word, ticket_id)

create index ticket_word_ticket_id on ticket_word (ticket_id)


create table call_center_hp (
  call_center_hp_id integer identity (180000, 1) primary key,
  call_center varchar(20) not null,
  hp_id int not null,  -- reference table, type hpmulti
  modified_date datetime default getdate() not null,
  active bit NOT NULL DEFAULT 1
)



create table dycom_period (
  starting datetime not null primary key,
  ending datetime not null,
  period varchar(10) not null,
  short_period varchar(10) not null,
)
go

-- This function is in here because the dycom_damage view needs it.
create function get_locator_for_damage (
  @DamageID integer
)
returns integer
as
begin
  declare @LocatorID integer
  select @LocatorID = null

  -- Get the locator ID from the damage table
  select @LocatorID = (select locator_id from damage where damage_id = @DamageID)

  -- Get the locator ID from the locate for the ticket & client associated with the damage
  if @LocatorID is null
    select @LocatorID = (select top 1 l.assigned_to_id
      from damage d
      inner join locate l on l.ticket_id = d.ticket_id and l.client_id = d.client_id
      where d.damage_id = @DamageID and l.status <> '-N')

  -- Get the locator ID from ticket_snap if none found for damaged client
  if @LocatorID is null
    select @LocatorID = (select ts.first_close_emp_id
      from damage d
      inner join ticket_snap ts on ts.ticket_id = d.ticket_id
      where d.damage_id = @DamageID)

  -- Get the locator ID from locates if none found for client or ticket_snap
  if @LocatorID is null
    select @LocatorID = (select top 1 l.assigned_to_id
    from damage d
    inner join locate l on l.ticket_id = d.ticket_id
    where d.damage_id = @DamageID and l.status <> '-N')

  return @LocatorID
end
GO

IF EXISTS (SELECT TABLE_NAME from INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'dycom_damage')
  DROP VIEW dbo.dycom_damage
GO

CREATE VIEW dycom_damage
AS
  select d.*, dp.short_period as fiscal_period,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id asc) as initial_estimate,
  (select top 1 amount from damage_estimate de
     where de.damage_id = d.damage_id order by estimate_id desc) as current_estimate,
  dbo.get_locator_for_damage(d.damage_id) as emp_id
  from damage d
  left join dycom_period dp on d.accrual_date >= dp.starting and accrual_date < dp.ending
GO

/*
select top 100 * from dycom_damage where damage_date > '2005-02-10'
*/



create table message (
    message_id int identity (27000, 1),
    message_number varchar(20) not null,   -- user-visible message identifier 'yyyymmddhhnnss'
    from_emp_id int,
    destination varchar(100),   -- text description of dest, example "Everyone"
    subject varchar(100),
    body text,                        -- optionally HTML, which will be shown in IE control
    sent_date datetime not null, -- date message was composed
    expiration_date datetime not null,
    show_date datetime not null,  -- date message should be displayed
    active bit not null default 1, 
    modified_date datetime not null default GetDate()
)
go

create trigger message_iu on message
for update
not for replication
as
begin
  update message
  set modified_date = GetDate()
  where message_id in (select message_id from inserted)
end
go

create table message_dest (
    message_dest_id int identity (87000, 1),
    message_id int,
    emp_id int,
    ack_date datetime,
    read_date datetime,
    rule_id int null,
    tse_entry_id int null,
)
go

create index message_dest_emp_ack on message_dest(emp_id, ack_date)
create index message_dest_tse_entry_id on message_dest(tse_entry_id)
go


alter table message
add primary key nonclustered (message_id)

alter table message_dest
add primary key nonclustered (message_dest_id)


alter table message_dest
    add constraint message_dest_fk_message foreign key  (message_id)
       references message (message_id)
go

alter table message
    add constraint message_fk_emp foreign key  (from_emp_id)
       references employee (emp_id)
go

alter table message_dest
    add constraint message_dest_fk_emp foreign key  (emp_id)
       references employee (emp_id)
go

/* Restricted use starting & ending times */
create table right_restriction (
  right_restriction_id int identity (1400, 1) primary key,
  right_id int not null,
  group_name varchar(100) not null,      /* Assigned to employee_right.limitation */
  restriction_type varchar(20) not null, /* WEEKDAY, WEEKEND */
  start_value varchar(100) null,         /* Allowed usage start time in hh:mm format */
  end_value varchar(100) null,           /* Allowed usage end time in hh:mm format */
  active bit not null default 1,
  modified_date datetime not null default GetDate())
go

alter table right_restriction
  add constraint FK_right_restriction_right_id foreign key (right_id)
  references right_definition (right_id)

alter table right_restriction
  add constraint constr_restriction_type_unique
  unique (right_id, group_name, restriction_type)
go

create trigger right_restriction_u on right_restriction
for update
not for replication
as
  update right_restriction
   set modified_date = GetDate()
   where right_restriction_id in (select right_restriction_id from inserted)
go

/* stores restricted use time starting & ending times */
create table restricted_use_message (
  rum_id int identity (1500, 1) primary key,
  countdown_minutes int not null,
  message_text varchar(200) not null,
  active bit not null default 1,
  modified_date datetime not null default GetDate())
go

create trigger restricted_use_message_u on restricted_use_message
for update
not for replication
as
  update restricted_use_message
   set modified_date = GetDate()
   where rum_id in (select rum_id from inserted)
go

/* stores restricted use time exemptions */
create table restricted_use_exemption (
  rue_id int identity (1600, 1) primary key,
  emp_id int not null,
  effective_date datetime not null,
  expire_date datetime not null,
  granted_by int not null,
  revoked_date datetime null,
  revoked_by int null,
  modified_date datetime not null default GetDate())
go

alter table restricted_use_exemption
  add constraint FK_restricted_use_exemption_emp_id foreign key (emp_id)
  references employee (emp_id)
alter table restricted_use_exemption
  add constraint FK_restricted_use_exemption_granted_by foreign key (granted_by)
  references employee (emp_id)
alter table restricted_use_exemption
  add constraint FK_restricted_use_exemption_revoked_by foreign key (revoked_by)
  references employee (emp_id)
go

create trigger restricted_use_exemption_u on restricted_use_exemption
for update
not for replication
as
  update restricted_use_exemption
   set modified_date = GetDate()
   where rue_id in (select rue_id from inserted)
go

/* stores facilities located when statusing a ticket's locates */
create table locate_facility (
  locate_facility_id integer identity not null primary key nonclustered,
  locate_id integer not null,
  facility_type varchar(15) null,     -- a reference.code of type='factype'
  facility_material varchar(15) null, -- a reference.code of type='facmtrl'
  facility_size varchar(15) null,     -- a reference.code of type='facsize'
  facility_pressure varchar(15) null, -- a reference.code of type='facpres'
  offset integer null,
  added_by integer not null,
  added_date datetime not null,
  deleted_by integer null,
  deleted_date datetime null,
  modified_by integer null,
  modified_date datetime not null default GetDate(), -- server date
  active bit not null default 1
  )
go

create index locate_facility_locate on locate_facility(locate_id)

alter table locate_facility
  add constraint locate_facility_fk_locate foreign key (locate_id)
  references locate (locate_id)
alter table locate_facility
  add constraint locate_facility_added_by_fk_employee foreign key (added_by)
  references employee (emp_id)
alter table locate_facility
  add constraint locate_facility_deleted_by_fk_employee foreign key (deleted_by)
  references employee (emp_id)
alter table locate_facility
  add constraint locate_facility_modified_by_fk_employee foreign key (modified_by)
  references employee (emp_id)
go

create trigger locate_facility_u on locate_facility
for update
not for replication
as
  update locate_facility set modified_date = GetDate() where locate_facility_id in (select locate_facility_id from inserted)
go

/***/
/* Group Rights related tables, functions and triggers */
/***/

/* stores Group Rights */
create table group_definition
(
   group_id int identity not null,
   group_name varchar(30),
   active bit not null default 1,
   is_default bit not null default 0, -- indicates if the group is default for all employees
   priority int,                      -- used to determine what "limitation" value applies to the right.
   modified_date datetime NOT NULL default getdate(),
   constraint pk_group_definition primary key (group_id)
)
Go
alter table group_definition
  add constraint uk_group_definition_priority unique nonclustered (priority)
Go

create trigger group_definition_d on group_definition
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from deleted))
    or 0 < (select count(*) from deleted where group_id in (select group_id from group_definition where is_default = 1))
GO

create trigger group_definition_u on group_definition
for update
not for replication
as
  set nocount on
  update group_definition
    set modified_date = getdate()
    where group_id in (select group_id from inserted)

  if update(is_default) begin
    update employee set rights_modified_date = GetDate()
  end
  else begin
    if update(active) or update(priority) begin
      update employee
        set rights_modified_date = GetDate()
      where
        emp_id in (select emp_id from
                   employee_group
                   where employee_group.group_id in (select group_id from inserted))
        or (0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1)))
    end
  end
GO

/* stores all groups the employee belongs to */

create table employee_group
(
  emp_group_id int identity not null,
  emp_id int not null,
  group_id int not null,
  constraint pk_employee_group primary key (emp_group_id)
)
Go

create unique index employee_group_emp_id_group_id on employee_group(emp_id, group_id)
Go

create index employee_group_group_id on employee_group(group_id)
Go

alter table employee_group
    add constraint fk_employee_group_group_definition foreign key  (group_id)
       references group_definition (group_id)
Go
ALTER TABLE employee_group 
  ADD CONSTRAINT employee_group_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

create trigger employee_group_iu on employee_group
for insert, update
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from inserted)
Go

create trigger employee_group_d on employee_group
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from deleted)
Go

/* stores rights for the Groups */

create table group_right (
  group_right_id int identity not null,
  group_id int not null,
  right_id int not null,
  allowed varchar(1) not null default '-',  -- Y, N, -
  limitation varchar(200) null,
  modified_date datetime not null default getdate(),
  constraint pk_group_right primary key (group_right_id)
)
Go

create trigger group_right_i on group_right
for insert
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from inserted))
    or 0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1))
Go

create trigger group_right_u on group_right
for update
not for replication
as
  update group_right
    set modified_date = getdate()
    where group_right_id in (select group_right_id from inserted)

  if update(allowed) or update(limitation) begin
    update employee
      set rights_modified_date = GetDate()
    where
      emp_id in (select emp_id from
                 employee_group
                 where employee_group.group_id in (select group_id from inserted))
      or 0 < (select count(*) from inserted where group_id in (select group_id from group_definition where is_default = 1))
  end
Go

create trigger group_right_d on group_right
for delete
not for replication
as
  update employee
    set rights_modified_date = GetDate()
  where
    emp_id in (select emp_id from
               employee_group
               where employee_group.group_id in (select group_id from deleted))
    or 0 < (select count(*) from deleted where group_id in (select group_id from group_definition where is_default = 1))
Go

create unique index group_right_group_id_right_id on group_right(group_id, right_id)
Go

create index group_right_right_id on group_right(right_id)
Go

alter table group_right
    add constraint fk_group_right_right foreign key  (right_id)
       references right_definition (right_id)
Go

alter table group_right
    add constraint fk_group_right_group foreign key  (group_id)
       references group_definition (group_id)
Go

/*
An insert trigger on ticket inserts a new ticket_snap row, populating ticket_id & first_receive_date.

An insert trigger on assignment updates the ticket_snap.first_receive_pc.

The locate_iu trigger executes dbo.update_ticket_status to set the
ticket.kind field. When ticket.kind = 'DONE", update_ticket_status also sets
ticket_snap's first_closed_date, first_close_emp_id, first_close_pc, & work_status.
*/
create table ticket_snap (
  ticket_id integer not null primary key nonclustered,    -- ticket insert trigger
  first_receive_date datetime not null default GetDate(), -- ticket insert trigger
  first_receive_pc varchar(15) null,                   -- assignment insert trigger
  first_close_date datetime null,                      -- locate update trigger
  first_close_emp_id integer null,                     -- locate update trigger
  first_close_pc varchar(15) null,                     -- locate update trigger (this is the normal/work PC)
  work_status varchar(15) null                         -- locate update trigger; values: MARKED, CLEARED
  )
go
create index first_receive_date on ticket_snap(first_receive_date)
create index first_close_date on ticket_snap(first_close_date)

alter table ticket_snap
  add constraint ticket_snap_fk_ticket foreign key (ticket_id)
  references ticket(ticket_id)
alter table ticket_snap
  add constraint ticket_snap_fk_employee foreign key (first_close_emp_id)
  references employee(emp_id)
alter table ticket_snap
  add constraint ticket_snap_fk_receive_pc foreign key (first_receive_pc)
  references profit_center(pc_code)
alter table ticket_snap
  add constraint ticket_snap_fk_close_pc foreign key (first_close_pc)
  references profit_center(pc_code)
go

/*
An insert trigger on assignment inserts a new locate_snap row, populating
locate_id, first_receive_pc, & first_receive_date.

An update trigger on locate sets locate_snap's first_closed_date,
first_close_emp_id, first_close_pc, work_status & location_status.
*/
create table locate_snap (
  locate_id integer not null primary key nonclustered,    -- assignment insert trigger
  first_receive_date datetime not null default GetDate(), -- assignment insert trigger
  first_receive_pc varchar(15) null,                   -- assignment insert trigger
  first_close_date datetime null,                      -- locate update trigger
  first_close_emp_id integer null,                     -- locate update trigger
  first_close_pc varchar(15) null,                     -- locate update trigger (this is the normal/work PC)
  work_status varchar(15) null,                        -- locate update trigger; values: MARKED, CLEARED
  location_status varchar(15) null                     -- locate update trigger; values: ONSITE, OFFSITE
  )
go

create index first_receive_date on locate_snap(first_receive_date)
create index first_close_date on locate_snap(first_close_date)
create index first_close_pc_close_date on locate_snap(first_close_pc, first_close_date)

alter table locate_snap
  add constraint locate_snap_fk_locate foreign key (locate_id)
  references locate(locate_id)
alter table locate_snap
  add constraint locate_snap_fk_employee foreign key (first_close_emp_id)
  references employee(emp_id)
alter table locate_snap
  add constraint locate_snap_fk_receive_pc foreign key (first_receive_pc)
  references profit_center(pc_code)
alter table locate_snap
  add constraint locate_snap_fk_close_pc foreign key (first_close_pc)
  references profit_center(pc_code)
go

create table addin_info (
  addin_info_id int identity(50000,1) not null,
  foreign_type tinyint not null,
  foreign_id int not null,
  latitude decimal(9,6) null,
  longitude decimal(9,6) null,
  added_date datetime not null,
  added_by_id int not null,
  active bit not null default 1,
  modified_date datetime not null default (getdate()),
  primary key nonclustered (addin_info_id)
)
go

create index addin_info_foreign_id on addin_info (foreign_id)
go

alter table addin_info
  add constraint addin_info_fk_employee foreign key (added_by_id) references employee (emp_id)
go

create table hp_address (
  hp_address_id int identity (8500,1) not null,
  call_center varchar(20) not null,
  client_code varchar(10) not null,
  street varchar(60) not null,
  city varchar(40) not null,
  county varchar(40) not null,
  state varchar(2) not null,
  primary key  nonclustered (hp_address_id)
)
go
create index hp_address_index on hp_address(hp_address_id)
go

CREATE TABLE ticket_activity_ack (
  ticket_activity_ack_id integer identity PRIMARY KEY,
  ticket_id integer NOT NULL,
  activity_type varchar(20) NOT NULL,
  activity_date datetime NOT NULL,
  has_ack bit NOT NULL DEFAULT 0,  /* 0=needs ack, 1=has ack */
  ack_date datetime NULL,
  ack_emp_id integer NULL,  /* the manager that performed the ack */
  locator_emp_id integer NULL,  /* locator associated with the ticket */
)

create index ticket_activity_ack_ack_date on ticket_activity_ack(ack_date)
create index ticket_activity_ack_has_ack on ticket_activity_ack(has_ack)
create index ticket_activity_ack_emp_id_has_ack on ticket_activity_ack(locator_emp_id, has_ack)
create index ticket_activity_ack_ticket_id on ticket_activity_ack(ticket_id)
create index ticket_activity_ack_emp_id_date_has_ack on ticket_activity_ack(locator_emp_id, activity_date, has_ack)
go
alter table dbo.ticket_activity_ack add constraint
    FK_ticket_activity_ack_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO
alter table dbo.ticket_activity_ack add constraint
    FK_ticket_activity_ack_ack_emp_id foreign key (ack_emp_id)
  references employee (emp_id)
GO
alter table dbo.ticket_activity_ack add constraint
    FK_ticket_activity_ack_locator_emp_id foreign key (locator_emp_id)
  references employee (emp_id)
GO

/* ============================================================ */
/* Tables to set ticket alerts based on keywords                */
/* ============================================================ */
/* ============================================================ */
/* Table: alert_ticket_type                                     */
/* ============================================================ */
create table alert_ticket_type (
  alert_ticket_type_id integer NOT NULL identity (100000, 1)
       primary key,
  call_center varchar(10) NOT NULL,
  ticket_type varchar(50) NOT NULL, -- For what ticket types will the keyword apply
  active bit NOT NULL DEFAULT 1     -- To turn off or on
)
go

/* ============================================================ */
/* Table: alert_ticket_keyword                                  */
/* ============================================================ */
create table alert_ticket_keyword (
  alert_ticket_keyword_id integer NOT NULL identity (11000, 1)
       primary key,
  alert_ticket_type_id integer NOT NULL,
  keyword varchar(100) NOT NULL
)
go

create index alert_ticket_keyword_ticket_type on alert_ticket_keyword
(alert_ticket_type_id)
go

alter table alert_ticket_keyword
  add constraint alert_ticket_keyword_fk_ticket_type
  foreign key (alert_ticket_type_id) references
alert_ticket_type(alert_ticket_type_id)
go

/* ============================================================ */
/* End Tables to set ticket alerts based on keywords                */
/* ============================================================ */

create table admin_restriction (
  admin_restriction_id    integer identity(1,1) primary key,
  login_id                varchar(50)    null,
  allowed_forms           varchar(4000)  null,
  modified_date           datetime       null
)
go

/* ============================================================ */
/* Table: break_rules_response - emp response to break rules messages */
/* ============================================================ */
create table break_rules_response (
  response_id integer identity,
  emp_id integer not null,
  rule_id integer not null,
  response_date datetime null,
  response varchar(100) null,     /* Caption of button clicked */
  contact_phone varchar(20) null, /* Employee's contact info */
  message_dest_id integer null,   /* Id of the delivered message responded to */
  constraint pk_break_rules_response primary key (response_id)
)
go
create index break_rules_response_emp_id on break_rules_response (emp_id, response_date)
create index break_rules_response_response_date on break_rules_response (response_date)
create index break_rules_response_rule_id on break_rules_response (rule_id, emp_id)
go


/* ============================================================ */
/*   Table: task_schedule - used to mark first work of the day  */
/* ============================================================ */
create table task_schedule (
  task_schedule_id  integer   not null identity(14000, 1),
  emp_id            integer   not null, -- employee with a task
  ticket_id         integer   not null, -- ticket for the task
  est_start_date    datetime  not null, -- estimated start time
  added_date        datetime  not null, -- local time the task was added
  added_by          integer   not null, -- employee adding the task
  active            bit       not null, -- active indicator
  modified_date     datetime  not null default GetDate(),
  performed_date    datetime  null,
  constraint pk_task_schedule primary key (task_schedule_id)
)
go
create index task_schedule_ticket_id on
  task_schedule (ticket_id, est_start_date)
create index task_schedule_est_start_date on
  task_schedule (emp_id, est_start_date)
create index task_schedule_added_date on
  task_schedule (emp_id, added_date)
go
alter table task_schedule add constraint task_schedule_fk_ticket
  foreign key (ticket_id) references ticket (ticket_id)
alter table task_schedule add constraint task_schedule_fk_employee
  foreign key (emp_id) references employee (emp_id)
alter table task_schedule add constraint task_schedule_fk_added_by
  foreign key (added_by) references employee (emp_id)
go

/* ============================================================ */
/*   Table: employee_upload_queue - track pending attachment    */
/*          upload summary by employee                          */
/* ============================================================ */
create table employee_upload_queue (
  emp_id integer not null,
  file_count integer not null default 0,
  file_size_total decimal(9,0) not null default 0,
  oldest_file_minutes integer not null default 0,
  modified_date datetime not null default GetDate(),
  constraint pk_employee_upload_queue primary key (emp_id)
  )
go
alter table employee_upload_queue add constraint employee_upload_queue_fk_employee
  foreign key (emp_id) references employee (emp_id)

/*Stores rules for highlighting keywords. */
create table highlight_rule (
  highlight_rule_id integer identity,
  call_center varchar(20) not null,   -- Call center the rule applies to ('*' for all)
  field_name varchar(50) not null,    -- Ticket field to highlight
  word_regex varchar(300) not null,   -- word or RegEx to highlight
  regex_modifier varchar(30) null,    -- Perl RegEx modifiers: /i/m/s/x/A/E/X
  which_match int not null default 0, -- which matched expression group to highlight
  highlight_bold bit not null default 0, -- show with bold font
  highlight_color integer not null,   -- show with hlcolor reference
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  constraint pk_highlight_rule primary key (highlight_rule_id)
)
go

/* GPS location data for an employee at a point in time */
create table gps_position (
  gps_id int not null identity(90000, 1),
  added_by_id int not null,
  added_date datetime not null,
  latitude decimal(9,6) not null,
  longitude decimal(9,6) not null,
  hdop_feet integer not null,
  constraint pk_gps_position primary key (gps_id)
)
go
create index gps_position_added_date on gps_position (added_by_id, added_date)
go

/********************************************************* */
-- document table holds the data pertinent to required documents
-- currently this is only used for damages, but is left open for
-- use by other pieces later (tickets, locates, etc...)

create table document (
  doc_id int identity not null,
  foreign_type int not null,
  foreign_id int not null,
  required_doc_id int null,
  comment varchar(250) null,
  added_date datetime not null,
  added_by_id int not null,
  active bit null default 1,
  modified_date datetime not null default getdate(),
  constraint pk_document primary key (doc_id)
)
go
create index document_required_doc_id on document(required_doc_id)
create index document_foreign_id on document(foreign_id)
go
create trigger document_u on document
for update
not for replication
AS
begin
  update document set modified_date = getdate()
   where doc_id in (select doc_id from inserted)
end
go

create table required_document (
  required_doc_id int identity not null,
  foreign_type int not null,
  name varchar(100) null,
  require_for_locate bit not null,
  require_for_no_locate bit not null,
  require_for_min_estimate decimal(12,2) not null default 0,
  user_selectable bit not null,
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  constraint pk_required_document primary key (required_doc_id)
)
go

create trigger required_document_u on required_document
for update
not for replication
as
begin
  update required_document set modified_date = getdate()
   where required_doc_id in (select required_doc_id from inserted)
end
go

create table required_document_type (
  rdt_id int identity not null,
  required_doc_id int not null,
  doc_type varchar(15) not null,
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  constraint pk_required_document_type primary key (rdt_id)
  )
go
create unique index required_document_type_doc_type on required_document_type (required_doc_id, doc_type)
go
create trigger required_document_type_u on required_document_type
for update
not for replication
AS
begin
  update required_document_type set modified_date = getdate()
   where rdt_id in (select rdt_id from inserted)
end
go

/* ============================================================ */
/*   Table: damage_profit_center_rule - rules for automated assignment of damage profit centers.*/
/* ============================================================ */
create table damage_profit_center_rule (
  rule_id  int identity(1, 1) not null primary key,
  pc_code  varchar(15) not null,
  state    varchar(2) not null,
  county   varchar(40) not null,
  city     varchar(40) not null,
  modified_date datetime not null default GetDate(),
  active bit default 1
)
go

create unique index damage_profit_center_rule_state_county_city on damage_profit_center_rule(state,county,city)

alter table damage_profit_center_rule
   add constraint damage_profit_center_rule_fk_profit_center
   foreign key (pc_code) references profit_center (pc_code)
go

/* ================= INSERT TABLES ABOVE THIS LINE ================= */

/* ============================================================ */
/*   Table: webapp - web site applications                      */
/* ============================================================ */
create table webapp
(
    app_id            int                   not null, -- application Id
    title             varchar(40)           null    , -- application title (short description)
    description       varchar(100)          null    , -- application long description
    path              varchar(100)          null    , -- relative path to asp page
    sortby            int                   null    , -- display sort order
    active_ind        bit                   not null, -- active indicator
    constraint pk_webapp primary key (app_id)
)
go

/* ============================================================ */
/*   Table: webgrp - groups for access to applications          */
/* ============================================================ */
create table webgrp
(
    grp_id            int                   not null    , -- id for access groups
    description       varchar(40)           null        , -- access group description
    access_level      int              null        , -- access level for group
    all_offices       bit                   not null    , -- access to all offices
    all_employees     bit                   not null    , -- access to all employees
    constraint pk_webgrp primary key (grp_id)
)
go

/* ============================================================ */
/*   Table: webmnu - menu groups (initially one, to be expanded later) */
/* ============================================================ */
create table webmnu
(
    mnu_id            int                   not null, -- id for menu groups
    title             varchar(40)           null    , -- menu title (short description)
    description       varchar(100)          null    , -- menu group long description
    sortby            int                   null    , -- display sort order
    active_ind        bit                   not null, -- active indicator
    constraint pk_webmnu primary key (mnu_id)
)
go


/* ======================================================================
 *   Table: reference - reference table for various cross reference codes
 * insert into reference(type, code, description, sortby) values ('type', 'CODE', 'Display', 1)
 * ====================================================================== */
create table reference
(
    ref_id            int                   identity, -- id for reference codes
    type              varchar(10)           null    , -- reference type
    code              varchar(15)           null    , -- short code
    description       varchar(100)          null    , -- long description
    sortby            int                   null    , -- display sort order
    active_ind        bit                   not null default 1, -- active indicator
    modified_date     datetime NOT null DEFAULT getdate(),
    modifier          varchar(20)           null    , -- Implementation-specific text describing the properties of this selection
    constraint pk_reference primary key (ref_id)
)
go

/* ============================================================ */
/*   Table: webmnuapp - cross reference between applications and menus */
/* ============================================================ */
create table webmnuapp
(
    mnu_id            int                   not null,
    app_id            int                   not null,
    constraint pk_webmnuapp primary key (mnu_id, app_id)
)
go

/* ============================================================ */
/*   Table: webgrpapp - cross reference between applications and access groups */
/* ============================================================ */
create table webgrpapp
(
    grp_id            int                   not null,
    app_id            int                   not null,
    constraint pk_webgrpapp primary key (grp_id, app_id)
)
go

/* ============================================================ */
/*   Table: user_session - login session table for variables to maintain state */
/* ============================================================ */
create table dbo.user_session
(
    session_id        int                   identity(1,1), -- unique primary key
    user_guid         varchar(50)           null    ,      -- unique guid
    create_date       datetime              null    ,      -- date created
    constraint pk_user_session primary key (session_id)
)
go

/* ============================================================ */
/*   Table: user_session_vars - variables to maintain state */
/* ============================================================ */
create table dbo.user_session_vars
(
    session_id        int                   not null,      -- key to user_session
    var_name          varchar(30)           not null,      -- variable name of session variable
    var_value         varchar(200)          null    ,      -- value of session variable
    constraint pk_user_session_vars primary key (session_id, var_name)
)
go

/* ============================================================ */
/*   Table: break_rules - break rules for the required break intervals for employees */
/* ============================================================ */
create table break_rules (
  rule_id  int identity(1, 1) not null primary key,
  pc_code varchar(15) not null,
  break_needed_after integer null,
  break_length integer null,
  rule_type varchar(10) not null, /* AFTER, EVERY, SUBMIT, SUBMITDIS, MGRALTER */
  rule_message varchar(3000),
  modified_date datetime not null default GetDate(),
  active bit default 1,
  message_frequency integer null,
  button_text varchar(100) null
)
go

alter table break_rules
   add constraint break_rules_fk_profit_center
   foreign key (pc_code) references profit_center (pc_code)
go

alter table break_rules_response
   add constraint break_rules_response_fk_employee
   foreign key (emp_id) references employee (emp_id)
go

alter table break_rules_response
   add constraint break_rules_response_fk_break_rules
   foreign key (rule_id) references break_rules (rule_id)
go

alter table break_rules_response
  add constraint break_rules_response_fk_message_dest foreign key (message_dest_id)
    references message_dest (message_dest_id)

alter table message_dest
    add constraint message_dest_fk_break_rule foreign key  (rule_id)
       references break_rules (rule_id)
go

alter table message_dest
    add constraint message_dest_fk_timesheet_entry foreign key (tse_entry_id)
       references timesheet_entry (entry_id)
go

alter table employee
    add constraint fk_employee001 foreign key  (type_id)
       references reference (ref_id)
go

alter table employee
    add constraint fk_employee003 foreign key  (status_id)
       references reference (ref_id)
go

alter table employee
    add constraint fk_employee004 foreign key  (timesheet_id)
       references reference (ref_id)
go

alter table webmnuapp
    add constraint fk_webmnuapp002 foreign key  (mnu_id)
       references webmnu (mnu_id)
go

alter table webmnuapp
    add constraint fk_webmnuapp001 foreign key (app_id)
       references webapp (app_id)
go

alter table webgrpapp
    add constraint fk_webgrpapp001 foreign key (grp_id)
       references webgrp (grp_id)
go

alter table webgrpapp
    add constraint fk_webgrpapp002 foreign key (app_id)
       references webapp (app_id)
go

alter table users
    add constraint fk_user001 foreign key (grp_id)
       references webgrp (grp_id)
go

alter table users
    add constraint fk_user002 foreign key (emp_id)
       references employee (emp_id)
go

alter table employee_office
    add constraint fk_employee_office001 foreign key (emp_id)
       references employee (emp_id)
go

alter table employee_office
    add constraint fk_employee_office002 foreign key (office_id)
       references office (office_id)
go

alter table office
  add constraint office_fk_profit_center foreign key (profit_center)
          references profit_center(pc_code)
go

alter table user_session_vars
    add constraint fk_user_session_vars001 foreign key (session_id)
       references user_session (session_id)
go

alter table call_center_hp
 add constraint call_center_hp_fk_call_center
 foreign key (call_center) references call_center(cc_code)
go

alter table call_center_hp
 add constraint call_center_hp_fk_reference
 foreign key (hp_id) references reference(ref_id)
go

alter table profit_center
  add constraint locating_company_fk_profit_center
  foreign key (company_id) references locating_company (company_id)
go

alter table timesheet_entry
  add constraint timesheet_entry_fk_reference foreign key (emp_type_id)
  references reference (ref_id)
alter table timesheet_entry
  add constraint FK_timesheet_entry_reason_changed foreign key (reason_changed)
  references reference (ref_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_fk_ticket
  foreign key (ticket_id) references ticket(ticket_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_arriver_fk_employee
  foreign key (emp_id) references employee(emp_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_adder_fk_employee
  foreign key (added_by) references employee(emp_id)
alter table jobsite_arrival
  add constraint jobsite_arrival_deleter_fk_employee
  foreign key (deleted_by) references employee(emp_id)
alter table highlight_rule
  add constraint highlight_rule_fk_hlcolor_reference
  foreign key (highlight_color) references reference (ref_id)
alter table locate
  add constraint locate_fk_gps_position
  foreign key (gps_id) references gps_position(gps_id)
alter table gps_position
  add constraint gps_position_fk_employee
  foreign key (added_by_id) references employee (emp_id)
alter table document
  add constraint document_fk_required_document
  foreign key (required_doc_id) references required_document(required_doc_id)
alter table document
  add constraint document_fk_employee
  foreign key (added_by_id) references employee(emp_id)
alter table required_document_type
  add constraint required_document_type_fk_required_document
  foreign key (required_doc_id) references required_document(required_doc_id)
alter table call_center
  add constraint call_center_tz_fk_reference
  foreign key (time_zone) references reference (ref_id)
go

create trigger locating_company_u on locating_company
for update
not for replication
AS
update locating_company set modified_date = getdate() where company_id in (select company_id from inserted)
go

create trigger billing_output_config_u on billing_output_config
for update
not for replication
AS
update billing_output_config set modified_date = getdate() where output_config_id in (select output_config_id from inserted)
go

create trigger billing_gl_u on billing_gl
for update
not for replication
AS
update billing_gl set modified_date = getdate() where billing_gl_id in (select billing_gl_id from inserted)
go

create trigger configuration_data_u on configuration_data
for update
not for replication
AS
update configuration_data set modified_date = getdate() where name in (select name from inserted)
go

create trigger billing_adjustment_u on billing_adjustment
for update
not for replication
AS
update billing_adjustment set modified_date = getdate() where adjustment_id in (select adjustment_id from inserted)
go

create trigger profit_center_u on profit_center
for update
not for replication
AS
update profit_center set modified_date = getdate() where pc_code in (select pc_code from inserted)
go

create trigger office_u on office
for update
not for replication
AS
update office set modified_date = getdate() where office_id in (select office_id from inserted)
go

create trigger statuslist_u on statuslist
for update
not for replication
AS
update statuslist set modified_date = getdate() where status in (select status from inserted)
go

create trigger client_u on client
for update
not for replication
AS
if SYSTEM_USER = 'no_trigger'
  return
update client set modified_date = getdate() where client_id in (select client_id from inserted)
go

create trigger ticket_u on ticket
for update
not for replication
AS
update ticket set modified_date = getdate() where ticket_id in (select ticket_id from inserted)
go

create trigger followup_ticket_type_u on followup_ticket_type
for update
not for replication
AS
update followup_ticket_type set modified_date = getdate() where type_id in (select type_id from inserted)
go

/* the locate trigger was more complex, and has been moved to the sp-other file */

create trigger assignment_u on assignment
for update
not for replication
AS
update assignment set modified_date = getdate() where assignment_id in (select assignment_id from inserted)
go

create trigger dbo.employee_iu on employee
for insert, update
not for replication
as
begin
  set nocount on

  declare @ChangeDate datetime
  set @ChangeDate = getdate()

  -- Don't update modified date or add history for rights_midified_date changes alone
  if ((not update(rights_modified_date)) or (update(type_id) or update(status_id) or update(timesheet_id)
    or update(emp_number) or update(short_name) or update(first_name) or update(last_name) or update(middle_init)
    or update(report_to) or update(create_date) or update(create_uid) or update(modified_uid) or update(charge_cov)
    or update(active) or update(can_receive_tickets) or update(timerule_id) or update(dialup_user)
    or update(company_car) or update(repr_pc_code) or update(payroll_pc_code) or update(crew_num) or update(autoclose)
    or update(company_id) or update(ticket_view_limit) or update(hire_date) or update(show_future_tickets)
    or update(incentive_pay) or update(contact_phone) or update(local_utc_bias)))
  begin
    update employee set modified_date = getdate() where emp_id in (select emp_id from inserted)

    if update(report_to) or update(repr_pc_code)
    begin -- This change may modify the effective PC of non-updated employees
      -- Recalculate and store any changes to everyone below these employees

      declare @AllEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY,
        x integer,
        emp_pc_code varchar(15) NULL
      )

      insert into @AllEmps select * from employee_pc_data(null)

      declare @ChangedEmps TABLE (
        emp_id integer NOT NULL PRIMARY KEY
      )

      insert into @ChangedEmps
        select emps.emp_id
        from @AllEmps emps
          left join employee_history eh on eh.emp_id = emps.emp_id and eh.active_start <= @ChangeDate and eh.active_end > @ChangeDate
        where ((emps.emp_pc_code <> eh.active_pc)
            or ((emps.emp_pc_code is null) and (eh.active_pc is not null))
            or ((emps.emp_pc_code is not null) and (eh.active_pc is null)))
          or emps.emp_id in (select emp_id from inserted)

      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from @ChangedEmps) and (active_end > @ChangeDate)

      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,
        local_utc_bias
      )
      select @ChangeDate, '2099-01-01', e.emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        emps.emp_pc_code, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,
        local_utc_bias
      from employee e
        inner join @AllEmps emps on emps.emp_id = e.emp_id
      where e.emp_id in (select emp_id from @ChangedEmps)
    end
    else -- This change only affects the exact employee(s) being modified
    begin
      -- "Deactivate" any current history records
      update employee_history set active_end = @ChangeDate where emp_id in (select emp_id from inserted) and (active_end > @ChangeDate)

      -- Record the new/current employee data
      insert into employee_history (active_start, active_end, emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        active_pc, ticket_view_limit, hire_date, show_future_tickets, incentive_pay, contact_phone,
        local_utc_bias
      )
      select @ChangeDate, '2099-01-01', emp_id, type_id, status_id, timesheet_id,
        emp_number, short_name, first_name, last_name, middle_init, report_to, create_date,
        create_uid, modified_date, modified_uid, charge_cov, active, can_receive_tickets,
        timerule_id, dialup_user, company_car, repr_pc_code, payroll_pc_code,
        crew_num, autoclose, company_id, rights_modified_date,
        dbo.get_employee_pc(inserted.emp_id, 0), ticket_view_limit, hire_date, show_future_tickets,
        incentive_pay, contact_phone, local_utc_bias
      from inserted
    end
  end
end
go

/* To test employee update trigger:
begin transaction
declare @EmpID int
set @EmpID = 2676
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --1
update employee set rights_modified_date = getdate() where emp_Id = @EmpID
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --2
WAITFOR DELAY '00:00:02'
update employee set short_name = 'Corporate 2' where emp_Id = @EmpID
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --3
WAITFOR DELAY '00:00:02'
update employee set short_name = 'Corporate 3', rights_modified_date = '2007-01-01' where emp_Id = @EmpID
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --4
WAITFOR DELAY '00:00:02'
update employee set short_name = 'Corporate 4', where emp_Id = @EmpID
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --5
WAITFOR DELAY '00:00:02'
update employee set rights_modified_date = getdate(), where emp_Id = @EmpID
select emp_id, short_name, type_id, modified_date, rights_modified_date from employee where emp_id = @EmpID --6
rollback transaction
*/

/* Disallow employee removal, since indexes do not exist for all FK fields of employee.emp_id */
--The Python tests apparently delete employees, so this is commented out for now
--create trigger employee_no_delete on employee
--instead of delete
--not for replication
--as
--begin
--  raiserror('Deleting employees is not allowed (due to an "instead of delete" trigger)', 16, 1)
--end
--go

create trigger carrier_u on carrier
for update
not for replication
AS
update carrier set modified_date = getdate() where carrier_id in (select distinct carrier_id from inserted)
go

create trigger reference_u on reference
for update
not for replication
AS
update reference set modified_date = getdate() where ref_id in (select distinct ref_id from inserted)
go

create trigger asset_type_u on asset_type
for update
not for replication
AS
update asset_type set modified_date = getdate() where asset_code in (select asset_code from inserted)
go

create trigger attachment_u on attachment
for update
not for replication
AS
update attachment set modified_date = getdate() where attachment_id in (select attachment_id from inserted)
go

create trigger upload_location_u on upload_location
for update
not for replication
AS
update upload_location set modified_date = getdate() where location_id in (select location_id from inserted)
go

create trigger upload_file_type_u on upload_file_type
for update
not for replication
AS
update upload_file_type set modified_date = getdate() where upload_file_type_id in (select upload_file_type_id from inserted)
go

create trigger damage_default_est_u on damage_default_est
for update
not for replication
AS
update damage_default_est set modified_date = getdate() where ddest_id in (select ddest_id from inserted)
go

create trigger users_iu on users
for insert, update
not for replication
AS
  declare @n int
  select @n =
   (select count(*)
     from users
      inner join inserted on users.login_id = inserted.login_id
     where users.uid <> inserted.uid)

  if @n > 0
  begin
    RAISERROR ('Cannot create duplicate user login_id', 16, 1)
    ROLLBACK TRANSACTION
  end
go

create trigger users_u on users
for update
not for replication
AS
update users set modified_date = getdate() where uid in (select uid from inserted)
go

create function dbo.CheckDuplicateTimesheetEntry(@EmpID int, @WorkDate datetime)
returns bit
as
begin
  declare @state bit

  if ((select count(*) from timesheet_entry e
      where e.work_emp_id = @EmpID and e.work_date = @WorkDate
        and e.status <> 'OLD') <= 1)
    set @state = 1
  else
    set @state = 0

  return(@state)
end
go

alter table timesheet_entry add constraint timesheet_entry_check_duplicates
check (dbo.CheckDuplicateTimesheetEntry(work_emp_id, work_date) = 1)
GO

alter table timesheet_entry add constraint timesheet_entry_check_pto_vac_leave_hours
check (
		(IsNull(pto_hours, 0.0) = 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) = 0.0) or
		(IsNull(pto_hours, 0.0) > 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) = 0.0) or
		(IsNull(pto_hours, 0.0) = 0.0 and IsNull(vac_hours, 0.0) + IsNull(leave_hours, 0.0) > 0.0))
go

create trigger customer_u on customer
for update
not for replication
AS
update customer set modified_date = getdate() where customer_id in (select customer_id from inserted)
go

create trigger center_group_u on center_group
for update
not for replication
AS
update center_group set modified_date = getdate() where center_group_id in (select center_group_id from inserted)
go

create trigger center_group_detail_u on center_group_detail
for update
not for replication
AS
update center_group_detail set modified_date = getdate() where center_id in (select center_id from inserted)
go

create trigger term_group_u on term_group
for update
not for replication
AS
update term_group set modified_date = getdate() where term_group_id in (select term_group_id from inserted)
go

create trigger term_group_detail_u on term_group_detail
for update
not for replication
AS
update term_group_detail set modified_date = getdate() where term_id in (select term_id from inserted)
go

create trigger status_group_u on status_group
for update
not for replication
AS
update status_group set modified_date = getdate() where sg_id in (select sg_id from inserted)
go

create trigger status_group_item_u on status_group_item
for update
not for replication
AS
update status_group_item set modified_date = getdate() where sg_item_id in (select sg_item_id from inserted)
go

create trigger call_center_hp_u on call_center_hp
for update
not for replication
AS
update call_center_hp set modified_date = getdate() where call_center_hp_id in (select call_center_hp_id from inserted)
go

create trigger break_rules_u on break_rules
for update
not for replication
as
begin
  update break_rules set modified_date = getdate()
   where rule_id in (select rule_id from inserted)
end
go

create trigger call_center_u on call_center
for update
not for replication
AS
update call_center set modified_date = getdate() where cc_code in (select cc_code from inserted)
go

create trigger jobsite_arrival_u on jobsite_arrival
for update
not for replication
as
update jobsite_arrival set modified_date = GetDate() where arrival_id in (select arrival_id from inserted)
go

create trigger highlight_rule_u on highlight_rule
for update
not for replication
as
update highlight_rule set modified_date = getdate() where highlight_rule_id in (select highlight_rule_id from inserted)
go

/*Begin Work Orders subsystem*/

create table work_order (
  wo_id integer identity (110000, 1) not null, /* QM internal identity */
  wo_number varchar(20) not null,     /* incoming InternalNumber prefixed with 'LAM'; UQ's primary reference to a wo */
  assigned_to_id integer null,        /* employee.emp_id assigned to the work order */
  client_id integer null,             /* client.client_id the work is for */
  parsed_ok bit null,                 /* 1 if any parser errors */
  wo_source varchar(20) not null,     /* aka call center; used to determine parser, responder, xml translation, etc for this wo*/
  kind varchar(40) not null,          /* PRIORITY, NORMAL, CANCEL */
  status varchar(5) not null,         /* the current statuslist.status of this work order */
  closed bit not null,                /* Is the work done */
  map_page varchar(20) null,          /* job site�s paper map page reference */
  map_ref varchar(60) null,           /* additional paper map references */
  transmit_date datetime not null,    /* when did QM get the work order */
  due_date datetime not null,         /* when does the work need to be done */
  closed_date datetime null,          /* datetime the work was completed */
  status_date datetime null,          /* datetime the status was last changed */
  cancelled_date datetime null,       /* datetime the work was canceled */
  work_type varchar(90) null,         /* summary of work to do */
  work_description varchar(3500) null, /* detailed explanation of the work to do */
  work_address_number varchar(10) null, /* number part of the work�s street address*/
  work_address_number_2 varchar(10) null, /* ending number part of the work�s address in case the job site is a range of addresses*/
  work_address_street varchar(60) null, /* street name of the job site*/
  work_cross varchar(100) null,       /* nearest cross street to the job site*/
  work_county varchar(40) null,       /* county name of the job site */
  work_city varchar(40) null,         /* city name of the job site*/
  work_state varchar(2) null,         /* state abbreviation of the job site */
  work_zip varchar(10) null,          /* zip code of the job site*/
  work_lat decimal(9,6) null,         /* job site�s latitude*/
  work_long decimal(9,6) null,        /* job site�s longitude*/
  caller_name varchar(50) null,       /* work reqeuester */
  caller_contact varchar(50) null,    /* name of contact person requesting the work*/
  caller_phone varchar(40) null,      /* caller�s primary phone number*/
  caller_cellular varchar(40) null,   /* caller�s cell*/
  caller_fax varchar(40) null,        /* caller�s fax*/
  caller_altcontact varchar(40) null, /* name of alternate contact*/
  caller_altphone varchar(40) null,   /* alternate contact�s phone*/
  caller_email varchar(40) null,      /* caller�s email address*/
  client_wo_number varchar(40) not null, /* client system's primary id for a work order*/
  image text null,                    /* raw XML representation of the work order */
  parse_errors text null,             /* description of any problems detected by the parser*/
  update_of_wo_id integer null,       /* wo_id of the work_order this one updates */
  modified_date datetime not null default GetDate(), /* sync support */
  active bit not null default 1,      /* 0 if this work order is inactive */

  job_number varchar(20) null,
  client_order_num varchar(20) null,
  client_master_order_num varchar(20) null,
  wire_center varchar(20) null,
  work_center varchar(20) null,
  central_office varchar(20) null,
  serving_terminal varchar(20) null,
  circuit_number varchar(20) null,
  f2_cable varchar(20) null,
  terminal_port varchar(20) null,
  f2_pair varchar(20) null,

  state_hwy_row varchar(10) null,     /* a 'strow' reference.code value */
  road_bore_count integer null,       /* Workers can set this using the Work Order Detail screen.*/
  driveway_bore_count integer null,   /* Workers can set this using the Work Order Detail screen.*/
  call_date datetime null,            /* Corresponds to ISSUEDATE on work order */
  source_sent_attachment varchar(1) null, /* Did the work order come with attachments - Y|N */
  map_x_coord decimal(12,4) null,     /* Some work orders (WGLINSP) come with alternate map coords */
  map_y_coord decimal(12,4) null
)
go

alter table work_order add constraint PK_work_order_wo_id primary key nonclustered (wo_id)
go

create index work_order_wo_number on work_order (wo_number)
create index work_order_due_date on work_order (due_date)
create index work_order_modified_date on work_order (modified_date)
create index work_order_transmit_date on work_order (transmit_date)
create index work_order_status on work_order (status)
create index work_order_client_wo_number on work_order (client_wo_number)
create index work_order_assigned_to on work_order (assigned_to_id)
go

alter table work_order
  add constraint work_order_fk_assigned_to_id
  foreign key (assigned_to_id) references employee (emp_id)
go
--Must reference valid client
alter table work_order
    add constraint work_order_fk_client_id
    foreign key (client_id) references client (client_id)
go

/* Table work_order_ticket associates work orders with tickets */
create table work_order_ticket (
  wo_id integer not null,          /* ref to work_order.wo_id */
  ticket_id integer not null,      /* ref to ticket.ticket_id */
  modified_date datetime not null DEFAULT getdate(), /* datetime this link was last changed */
  active bit not null DEFAULT 1    /* is this link active */
  )
go

alter table work_order_ticket add constraint PK_work_order_ticket primary key nonclustered (wo_id, ticket_id)
go
create index work_order_ticket_ticket_id on work_order_ticket (ticket_id)
go

--Must be a valid work order
alter table work_order_ticket
  add constraint work_order_ticket_fk_wo_id
  foreign key (wo_id) references work_order (wo_id)
go

--Must be a valid ticket
alter table work_order_ticket
  add constraint work_order_ticket_fk_ticket_id
  foreign key (ticket_id) references ticket (ticket_id)
go

create table wo_assignment (
  assignment_id integer identity (210000, 1) not null,
  wo_id integer not null,                            /* work_order.wo_id link */
  assigned_to_id integer not null,                   /* Employee assigned to the work order */
  insert_date datetime not null default GetDate(),   /* Always the server default date */
  added_by varchar(8) not null,                      /* PARSER, or emp_id of user */
  modified_date datetime not null default GetDate(), /* for sync support */
  active bit not null default 1,                     /* Is the work order assignment current or historical */
)
go

alter table wo_assignment add constraint PK_wo_assignment_assignment_id primary key nonclustered (assignment_id)
go

create clustered index wo_assignment_wo_assigned_to on wo_assignment (wo_id, assigned_to_id)
create index wo_assignment_assigned_to on wo_assignment (assigned_to_id)
create index wo_assignment_modified_date on wo_assignment (modified_date)
go

-- Must be a valid work order
alter table wo_assignment
    add constraint wo_assignment_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Assignment must be to a valid employee
alter table wo_assignment
    add constraint wo_assignment_fk_assigned_to_id
    foreign key (assigned_to_id) references employee (emp_id)
go

/* Work Order status history*/
create table wo_status_history (
  wo_status_id integer identity (310000, 1) not null,
  wo_id integer not null,        /* link to work_order.wo_id */
  status_date datetime not null, /* local time when this status was saved */
  status varchar(5) not null,    /* the status code */
  statused_by_id integer null,   /* an employee.emp_id that entered the status */
  statused_how varchar(20) null, /* how was the status changed 'PARSER', 'DETAIL', 'MANAGE' */
  insert_date datetime not null default GetDate(), /* db server datetime this status change was added to history */
  cga_reason varchar(15) null
  )
go

alter table wo_status_history add constraint PK_wo_status_history_wo_status_id primary key nonclustered (wo_status_id)
go

create index wo_status_history_wo on wo_status_history (wo_id, status_date)
go

--Must reference valid work order
alter table wo_status_history
    add constraint wo_status_history_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Must reference valid employee
alter table wo_status_history
    add constraint wo_status_history_fk_statused_by_id
    foreign key (statused_by_id) references employee (emp_id)
go

/* Table wo_responder_queue will be populated by QML; a work order response can only be sent after all its attachments have an upload_date */
create table wo_responder_queue (
  wo_id int not null,     /* link to the work_order.wo_id */
  client_wo_number varchar(40) null,  /* work_order.client_wo_number - here to minimize responder lookups */
  insert_date datetime not null default GetDate(), /* date the response was queued */
  wo_source varchar(20) null,  /* work_order.source - here to minimize responder lookups */
  client_id integer null,      /* work_order.client_id - here to minimize responder lookups */
  ready_to_respond bit null,   /* false until all attachments are uploaded */
  constraint pk_wo_responder_queue primary key nonclustered (wo_id, insert_date)
  )
go

--Must reference valid work order
alter table wo_responder_queue
    add constraint wo_responder_queue_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go
--Must reference valid client
alter table wo_responder_queue
    add constraint wo_responder_queue_fk_client_id
    foreign key (client_id) references client (client_id)
go

/* A log of all work order responses */
create table wo_response_log (
  wo_response_id integer identity (320000, 1),
  wo_id int not null,
  response_date datetime not null,
  wo_source varchar(20) not null,
  status varchar(5) not null,
  response_sent varchar(15) not null, /* whatever we translated the status to for cases where respondee needs an alternate status */
  success bit not null DEFAULT 0,     /* 0 until the respondee's ack is received */
  reply varchar(40)
)
go
alter table wo_response_log add constraint PK_wo_response_log_wo_response_id primary key nonclustered (wo_response_id)
go
create index wo_response_log_response_date on wo_response_log(response_date)
go
create index wo_response_log_wo_id on wo_response_log(wo_id)
go

--Must reference valid work order
alter table wo_response_log
    add constraint wo_response_log_fk_wo_id
    foreign key (wo_id) references work_order (wo_id)
go

/* Audit report headers to reconcile work order receipts */
create table wo_audit_header (
  wo_audit_header_id integer identity (23001, 1) not null,
  wo_source varchar(20) not null,   /* e.g. LAM01 */
  summary_date datetime not null,   /* transmission date */
  modified_date datetime null
)
go
alter table wo_audit_header add constraint PK_wo_audit_header_wo_audit_header_id primary key nonclustered (wo_audit_header_id)
go
create index wo_audit_header_summary_date on wo_audit_header(summary_date)
go

/* Audit report details of work order receipts for a wo_audit_header */
create table wo_audit_detail (
   wo_audit_detail_id integer identity (43001, 1) not null,
   wo_audit_header_id integer not null,    /* link to wo_audit_header */
   client_wo_number varchar(40) not null,  /* the sender's work order id */
   modified_date datetime null
)
go
alter table wo_audit_detail add constraint PK_wo_audit_detail_wo_audit_detail_id primary key nonclustered (wo_audit_detail_id)
go
create index wo_audit_detail_client_wo_number on wo_audit_detail(client_wo_number)
go

-- must reference a valid wo_audit_header
alter table wo_audit_detail
    add constraint wo_audit_detail_fk_wo_audit_header_id
    foreign key (wo_audit_header_id) references wo_audit_header (wo_audit_header_id)
go

-- keep track of work order versions (much like tickets)
CREATE TABLE work_order_version (
  wo_version_id int identity (8000, 1),
  wo_id integer NOT NULL,
  wo_revision varchar(20) NULL, /* The call center's description of this revision, if any */
  wo_number varchar(20) not null,     /* corresponds to work_order.wo_number*/
  work_type varchar(90) null,         /* corresponds to work_order.work_type*/
  transmit_date datetime not null,    /* corresponds to work_order.transmit_date */
  processed_date datetime NOT NULL,   /* D/T processed by parser */
  arrival_date datetime NULL,         /* timestamp of filename */
  image text NULL,                    /* corresponds to work_order.image */
  filename varchar(100) NULL, /* Filename where the work order came from */
  wo_source varchar(20) not null     /* aka call center; used to determine parser, responder, xml translation, etc for this wo*/
)
go

alter table work_order_version
  add constraint PK_work_order_version_wo_version_id
  primary key nonclustered (wo_version_id)
go
alter table work_order_version
  add constraint work_order_version_fk_wo_id
  foreign key (wo_id) references work_order (wo_id)
go
CREATE INDEX work_order_version_wo_id on work_order_version(wo_id)
go
CREATE INDEX work_order_version_transmit_date
 ON work_order_version(transmit_date, wo_source)
go

CREATE TABLE work_order_inspection (  /* Washington Gas Meter Inspections */
  wo_id integer not null, -- 1 to 1 relationship with work_order
  account_number varchar(50) not null,
  premise_id varchar(50) not null,
  meter_number varchar(80) null,
  compliance_due_date datetime not null,
 
  actual_meter_number varchar(80) null,
  actual_meter_location varchar(15) null,  -- reference table code (type='metrloc')
  building_type varchar(15) null,       -- reference table code (type='bldtype')
  cga_visits integer null,
  map_status varchar(15) null,          -- reference table code (type='mapstat')
  mercury_regulator varchar(15) null,   -- reference table code (type='ynub')
  vent_clearance_dist varchar(15) null, -- reference table code (type='ventclr')
  vent_ignition_dist varchar(15) null,  -- reference table code (type='ventign')
  alert_first_name varchar(20) null,
  alert_last_name varchar(30) null,
  alert_order_number varchar(50) null,
  inspection_change_date datetime null, 
  modified_date datetime default GetDate(),
  cga_reason varchar(15) null,       -- reference table code (type='cgareason')  
  invoiced bit not null default 0,
  gas_light varchar(15) null,
  constraint pk_work_order_inspection primary key nonclustered (wo_id)
) 
go
alter table work_order_inspection
  add constraint work_order_inspection_fk_wo_id
  foreign key (wo_id) references work_order (wo_id)
go

CREATE TABLE work_order_work_type ( /* List of valid work order remedy work types */
  work_type_id integer identity not null,
  work_type varchar(10) not null,
  work_description varchar(100) not null,
  flag_color varchar(10) null,
  alert  bit not null default 0,
  active bit not null default 1,
  wo_source varchar(20) default '*',
  modified_date datetime default GetDate(),
  constraint pk_work_order_work_type primary key nonclustered (work_type_id)
)
create unique nonclustered index wo_work_type_work_type on work_order_work_type (work_type)
go

-- Remedy work selected for a given work order inspection
CREATE TABLE work_order_remedy (
  wo_remedy_id integer identity (100,1) not null,
  wo_id integer not null,
  work_type_id int not null,
  active bit not null default 1,
  modified_date datetime default GetDate(),
  constraint pk_work_order_remedy primary key nonclustered (wo_remedy_id)
)
alter table work_order_remedy
  add constraint work_order_remedy_fk_wo_id
  foreign key (wo_id) references work_order (wo_id)
alter table work_order_remedy
  add constraint work_order_remedy_fk_work_type_id
  foreign key (work_type_id) references work_order_work_type (work_type_id)
GO

create trigger work_order_inspection_iu on work_order_inspection
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_inspection
  set modified_date = GetDate()
  from inserted I
  where work_order_inspection.wo_id = I.wo_id
end
GO

create trigger work_order_remedy_iu on work_order_remedy
for insert,update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
  update work_order_remedy
  set modified_date = GetDate()
  from inserted I
  where work_order_remedy.wo_id = I.wo_id
end
GO

create trigger work_order_work_type_iu on work_order_work_type
for insert, update
not for replication
AS
begin
  if SYSTEM_USER = 'no_trigger'
    return

  SET NOCOUNT ON
    -- Any changes to their work type or the active flags
  if (select count(*) from deleted d inner join inserted i on d.work_type_id = i.work_type_id
      where (d.work_type = 'VI' or d.work_type = 'VC')
      and (d.work_type <> i.work_type or d.active <> i.active))>0
  begin
    rollback transaction
    raiserror('Error: Changing the VI or VC codes and their Active flags is prohibited.',16,1)
    return
  end
   
  update work_order_work_type
  set modified_date = GetDate()
  from inserted I
  where work_order_work_type.work_type_id = I.work_type_id
end
GO

/*End Work Orders subsystem*/

create table status_translation (
  status_translation_id int identity primary key,
  cc_code varchar(20) not null,
  status varchar(5) not null,
  outgoing_status varchar(30) not null,
  active bit not null default 1,
  modified_date datetime not null default getdate(),
  explanation_code varchar(20) null,
  responder_type varchar(20) not null,
  responder_kind varchar(20) null,
)
go

create unique nonclustered index status_translation_cc_code_status on
status_translation (cc_code, responder_type, responder_kind, status)
go

alter table status_translation
  add constraint status_translation_fk_status
  foreign key (status) references statuslist (status) not for replication 
go
alter table status_translation
  add constraint status_translation_fk_cc_code
  foreign key (cc_code) references call_center (cc_code) not for replication 
go
alter table status_translation 
  add constraint status_translation_check_blank_outgoing_status
  check not for replication (outgoing_status <> '') 
go

create trigger status_translation_iu on status_translation
for insert,update
not for replication
as
begin
  if system_user = 'no_trigger'
    return

  set nocount on
  update status_translation
  set modified_date = getdate()
  from inserted i
  where status_translation.status_translation_id = i.status_translation_id
end
go

/* ticket_hp table, Mantis #2869 */
CREATE TABLE ticket_hp (
    ticket_hp_id integer identity (38000, 1),
    ticket_id integer NOT NULL,
    data varchar(150) NOT NULL,
    insert_date datetime NOT NULL default getdate(),
	constraint pk_ticket_hp primary key nonclustered (ticket_hp_id)
)
GO

ALTER TABLE dbo.ticket_hp add constraint
  ticket_hp_fk_ticket_id foreign key (ticket_id)
  references ticket (ticket_id)
GO

create table incoming_item (
  item_id integer identity primary key,
  cc_code varchar(20) not null,
  item_type integer null, -- 1 = ticket, 2 = audit, 3 = message
  item_text varchar(max) not null,
  insert_date datetime not null default getdate()
)
go

alter table incoming_item
  add constraint incoming_item_fk_cc_code
  foreign key (cc_code) references call_center (cc_code) not for replication 
go
alter table incoming_item
  add constraint incoming_item_check_item_text_length
  check (datalength(item_text) <= 50000)    
go

/* ============================================================ */
/* Table: damage_due_date_rule - rules for automated assignment
   of damage due dates. */
/* ============================================================ */
create table damage_due_date_rule (
  rule_id  int identity(1, 1) not null primary key,
  pc_code  varchar(15) not null,
  utility_co_damaged varchar(40) not NULL,
  calc_method varchar(30) not Null, --todo SiZE?  Possible values? i.e. Business Day, etc
  days integer, --number of days until due
  modified_date datetime not null default GetDate(),
  active bit default 1
)
go

alter table damage_due_date_rule
  add constraint damage_due_date_rule_unique_constr
  unique nonclustered (pc_code, utility_co_damaged, active)
go


/* ********************** */


CREATE TABLE eFax_error_codes (
  code  INT NOT NULL PRIMARY KEY,
  description VARCHAR(100) NOT NULL,
  class VARCHAR(1) NOT NULL
)

create table employee_work_goal (
  work_goal_id integer identity (81000, 1) not null,
  emp_id int not null, 
  effective_date datetime not null,
  ticket_count int not null default 0,
  locate_count int not null default 0,
  added_date datetime not null default GetDate(),
  modified_date datetime not null default GetDate(),
  active bit not null default 1,
  constraint pk_employee_work_goal primary key (work_goal_id)
)
ALTER TABLE employee_work_goal 
  ADD CONSTRAINT employee_work_goal_fk_employee FOREIGN KEY(emp_id) REFERENCES employee(emp_id) NOT FOR REPLICATION 
GO

CREATE SEQUENCE event_log_seq AS INT START WITH 21000
GO

CREATE TABLE event_log (
  event_id INT DEFAULT NEXT VALUE FOR event_log_seq,
  aggregate_type integer NOT NULL,      -- 1 = Ticket, 2 = Damage, 3 = Work Order
  aggregate_id integer NOT NULL, 
  version_num integer NOT NULL,
  event_data varchar(5000) NOT NULL,    -- the event object
  event_added datetime NOT NULL DEFAULT GetDate(),
  constraint pk_event_log primary key nonclustered (event_id)
)
GO
   CREATE UNIQUE CLUSTERED INDEX event_log_aggregate_idx ON event_log(aggregate_type, aggregate_id, version_num)
   GO

   -- Setup a Service Broker to handle new event_log message queueing
   -- Create Message Type
    CREATE MESSAGE TYPE NewEventMessage
    VALIDATION = NONE
   GO



--QUESTION TABLES - Initially created for Colorado 03/2015
create table question_header (
  --qh_id          INT           Identity Primary Key,  Note: The Identity property was missing from production when created      
  qh_id          INT           NOT NULL Primary Key,
  qh_description VARCHAR (255) NOT NULL,
  modified_date  DATETIME      not null default GetDate(),
  active         BIT           NOT NULL default 1
)
GO

	CREATE TRIGGER question_header_iu on question_header
		for update
		not for replication
		AS
		  update question_header set modified_date = getdate() where qh_id in (select qh_id from inserted)
	GO
	
CREATE TABLE question_info (
  --qi_id          INT           Identity Primary Key,  Note: The Identity property was missing from production when created
  qi_id          INT           NOT NULL Primary Key,
  qi_description VARCHAR (255) NOT NULL,
  info_type      VARCHAR (8)   NOT NULL,
  form_field     VARCHAR (8)   NOT NULL,
  modified_date  DATETIME      NULL default GetDate(),
  active         BIT           NOT NULL default 1
)
GO

    CREATE TRIGGER question_info_iu on question_info
		for update
		not for replication
		AS
		  update question_info set modified_date = getdate() where qi_id in (select qi_id from inserted)
	GO
	
	
CREATE TABLE question_group_detail (
  --qgd_id        INT         Identity Primary Key  Note: The Identity property was missing from production when created
  qgd_id        INT         NOT NULL Primary Key,
  qh_id         INT         NOT NULL,
  qi_id         INT         NOT NULL,
  tstatus       VARCHAR (5) NOT NULL,
  modified_date DATETIME    NULL default GetDate(),
  active        BIT         NOT NULL default 1
)
GO

    CREATE TRIGGER [dbo].[question_group_detail_iu] on [dbo].[question_group_detail]
		for update
		not for replication
		AS
		  update question_group_detail set modified_date = getdate() where qgd_id in (select qgd_id from inserted)
		GO

/*****************************************************************/
--EPR: Added as a part of 114, QMANTWO-32
/*--EPR_Notification_Config--------------------------------------*/
CREATE TABLE EPR_Notification_Config(
	notification_config_id int IDENTITY(1,1) NOT NULL,
	call_center varchar(20) NOT NULL,
	exclude_ticket_type varchar(50) NULL,
	work_done_for varchar(100) NOT NULL,
	contractor varchar(50) NOT NULL,
	disclaimer_id int NOT NULL,
	screenshots_include varchar(500) NOT NULL,
	delayer_client_codes varchar(500) NOT NULL,
	client_codes_require_cv varchar(500) NOT NULL,
	cv_Wait_Count int NOT NULL,
	inc_att_Wait_Hrs int NOT NULL,
	survey_url varchar(200) NOT NULL,
	viewable_days int NOT NULL,
	contact_to varchar(500) NOT NULL,
	contact_cc varchar(500) NULL,
	contact_phone varchar(15) NULL,
	contact_name varchar(100) NULL,
	modified_date datetime NOT NULL DEFAULT (getdate()),
	active bit NOT NULL DEFAULT ((1)),
	remove_all_tie_downs bit NULL,
	company_token varchar(16) NOT NULL CONSTRAINT DF_EPR_Notification_Config_company_token DEFAULT ('UQ/STS'),
	in_EPR [bit] NULL CONSTRAINT DF_EPR_Notification_Config_in_EPR  DEFAULT ((1)),
	contains_clients varchar(1000) NULL,
)
GO

/*--epr_response---------------------------------------------*/
CREATE TABLE epr_response(
	response_id int IDENTITY(1,1) NOT NULL,
	ticket_id int NOT NULL,
	Parent_ticket_id int NULL,
	dbsource varchar(5) NULL Default 'QM',
	contact_to varchar(500) NULL,
	contact_cc varchar(500) NULL,
	insert_date datetime NOT NULL,
	viewable_days int NOT NULL,
	[message] text NULL,
	[status] varchar(10) NOT NULL,
	link varchar(500) NULL,
	disclaimer_id int NULL,
	work_lat decimal(9, 6) NULL,
	work_long decimal(9, 6) NULL,
	Ticket_type varchar(60) NULL,
	Company varchar(100) NULL,
	notification_config_id int NULL,
	cv_wait_count int NULL,
	call_center varchar(20) NULL,
	last_sent_date datetime NULL,
) 
GO
/*--epr_queue_rules--------------------------------------------*/
CREATE TABLE epr_queue_rules(
	epr_id int IDENTITY(1100,1) NOT NULL,
	call_center varchar(20) NOT NULL,
	client_code varchar(20) NOT NULL,
	status varchar(5) NOT NULL,
	state varchar(5) NOT NULL,
	status_message varchar(3000) NULL,
)
GO
/*--billing_trans_detail--------------------------------------*/
CREATE TABLE billing_trans_detail(
		trans_billing_id [bigint] IDENTITY(1,1) NOT NULL,
		bill_id [int] NOT NULL,
		termcode [nvarchar](20) NULL,
		ticket_id [int] NULL,
		call_center [nvarchar](20) NULL,
		ticket_number [nvarchar](20) NULL,
		ticket_type [varchar](50) NULL,
		con_name [varchar](50) NULL,
		work_city [varchar](50) NULL,
		transmit_date [datetime] NULL,
		work_county [varchar](50) NULL,
		work_address_number [varchar](20) NULL,
		work_address_number_2 [varchar](20) NULL,
		work_address_street [varchar](60) NULL,
		work_cross [varchar](100) NULL,
		map_page [varchar](60) NULL,
		rate [money] NULL,
		locate_closed [bit] NULL,
		plat [varchar](20) NULL
	) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX ClusteredIndexBillID ON billing_trans_detail
(
	bill_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX index_tranmit_date ON billing_trans_detail
(
	transmit_date ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO

------------------------------------------------------------------
------------------------------------------------------------------
-----------------------------------------------------------------------
-- TABLE: audit_due_date_rule
-- Configuration table in Admin
--This will be used to create a due date for the audit
-----------------------------------------------------------------------
CREATE TABLE audit_due_date_rule(
	rule_id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	audit_source varchar(15) NOT NULL,
	ticket_format varchar(15) NOT NULL,
	hours_due int NULL,
	modified_date datetime default GetDate(),
	active bit default 1,
    CONSTRAINT audit_due_date_rule_unique_constr UNIQUE NONCLUSTERED (audit_source, ticket_format, active)
)
GO

---------------------------------------------------------------------
-- TABLE: work_order_OHM_details
-- Extension of work_order table for OHM (1->1).
---------------------------------------------------------------------
--New Table for OHM Work Order. This is a 1 -> 1 relationship with work_order, but is stored separately


CREATE TABLE work_order_OHM_details(
	wo_dtl_id int IDENTITY(1,1) NOT NULL,
	wo_id int NOT NULL,
	wo_number varchar(20) NULL,
	OSID varchar(50) NULL,
	due_date datetime NULL,
	curb_b varchar(100) NULL,
	meter_1 varchar(50) NULL,
	slpi_p varchar(50) NULL,
	slp_1 varchar(50) NULL,
	slp_2 varchar(50) NULL,
	slin_s datetime NULL,
	sl_pi varchar(50) NULL,
	slp_3 varchar(50) NULL,
	slpre varchar(50) NULL,
	main_p varchar(50) NULL,
	mai_1 varchar(50) NULL,
	main_r varchar(50) NULL,
	meter_2 varchar(50) NULL,
	has_c bit NULL,
    nominaldia varchar(50) NULL, 
	coatingtyp varchar(50) NULL, 
	installedd varchar(50) NULL,  
	measuredle varchar(50) NULL, 
	material varchar(50) NULL,
    curbvaluefound bit NULL,
    modified_date datetime NULL
)

GO


ALTER TABLE work_order_OHM_details  WITH CHECK ADD  CONSTRAINT FK_work_order_OHM_details_work_order FOREIGN KEY(wo_id)
REFERENCES work_order (wo_id)
GO

ALTER TABLE work_order_OHM_details CHECK CONSTRAINT FK_work_order_OHM_details_work_order
GO
--------------------------------------------------------------------------------------------
--TRIGGER for work_order_OHM_details
if object_id('dbo.work_order_OHM_details_u') is not null
  drop trigger dbo.work_order_OHM_details
go

create trigger work_order_OHM_details_u on work_order_OHM_details
for insert, update 
not for replication
as begin
  set nocount on

  update work_order_OHM_details
    set modified_date = GetDate()
    from inserted i
    where work_order_OHM_details.wo_dtl_id = i.wo_dtl_id
end
go



/*--disclaimer------------------------------------------------*/
/*----table originally added outside of QManager, then added to Admin (and therefore, the script) --*/
CREATE TABLE disclaimer(
	disclaimer_id int IDENTITY(1,1) NOT NULL Primary Key,
	disclaimer_text text NOT NULL
	)
GO

-- Create Contract
CREATE CONTRACT NewEventContract 
  (NewEventMessage SENT BY INITIATOR)
-- Create Send Queue
CREATE QUEUE NewEventSendQueue
-- Create Receive Queue
CREATE QUEUE NewEventReceiveQueue
GO

-- Create Send Service on Send Queue
CREATE SERVICE NewEventSendService
  ON QUEUE NewEventSendQueue (NewEventContract)
-- Create Receive Service on Receive Queue
CREATE SERVICE NewEventReceiveService
  ON QUEUE NewEventReceiveQueue (NewEventContract)
GO

if not exists (select * from master.dbo.syslogins where name = 'uqweb')
begin
  CREATE LOGIN uqweb WITH PASSWORD = 'uqweb_password_zzz55', CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF
end
go
if not exists (select * from master.dbo.syslogins where name = 'no_trigger')
begin
  CREATE LOGIN no_trigger WITH PASSWORD = 'no_trigger_password_zzz55', CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF
end
go

CREATE USER uqweb FOR LOGIN uqweb
go
-- Logging in as this user make s the DB skip running a few table change triggers
-- Search the schema files for no_trigger for full details
CREATE USER no_trigger FOR LOGIN no_trigger
go

sp_addrole 'QManagerRole'
go
sp_addrolemember 'QManagerRole', 'uqweb'
go
sp_addrolemember 'db_datareader', 'uqweb'
go
sp_addrolemember 'db_datawriter', 'uqweb'
go

-- Grant execute permission to the stored procs/functions created by this script
grant execute on get_locator_for_damage to uqweb, QManagerRole
go
grant execute on CheckDuplicateTimesheetEntry to uqweb, QManagerRole
go

-- Grant service broker related permissions to uqweb
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO uqweb, QManagerRole;
GRANT RECEIVE ON NewEventReceiveQueue TO uqweb, QManagerRole;
GRANT SEND ON SERVICE::NewEventSendService TO uqweb, QManagerRole;
GO
