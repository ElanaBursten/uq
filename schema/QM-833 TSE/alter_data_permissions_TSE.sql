--QM-833 TSE Permission
--EB 10/23/2023
-----------------------------------------------------
--Permissions:  We are not adding permissions for TSE
--              We add limitation to TimesheetsEntry    
-----------------------------------------------------
update right_definition
set modifier_desc = 'Limitation is blank or TSE'
where entity_data = 'TimesheetsEntry'

--select * from employee_right where emp_id = 


