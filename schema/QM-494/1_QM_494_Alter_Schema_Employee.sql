-- QM-494 First Task Reminder

alter table employee
add first_task_reminder varchar(5) NULL
--00:00

alter table employee_history
add first_task_reminder varchar(5) NULL


--Dev only
--ALTER TABLE employee
--DROP COLUMN first_task_reminder

--ALTER TABLE employee_history
--DROP COLUMN first_task_reminder

--select short_name, first_task_reminder from employee where short_name like 'FXL.Juan Vogel'

--select short_name, first_task_reminder from employee where short_name like '%Sam Garnett%'

--select short_name, first_task_reminder from employee where short_name like '%Candy Pope%'

