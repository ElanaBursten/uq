------------------------------------------------
--QM-494 First Task Reminder Permissions
-- Permissions to set first task reminder and use first task reminder 
-- Modifier: None
-- Will determine if Set First Task is in the popup window for WorkManagement 
--    and whether it reminds locator
-- EB
------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsSetFirstTaskReminder')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Set First Task Reminder', 
						'General', 
						'TicketsSetFirstTaskReminder', 
						'Limitation does not apply to this right', 
						GetDate());
GO

------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsViewFirstTaskReminder')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - View First Task Reminder', 
						'General', 
						'TicketsViewFirstTaskReminder', 
						'Limitation does not apply to this right', 
						GetDate());
GO

--Select * from right_definition where entity_data = 'TicketsSetFirstTaskReminder'
--select * from employee_right where right_id = 15212

--select * from employee where emp_id = 12297





