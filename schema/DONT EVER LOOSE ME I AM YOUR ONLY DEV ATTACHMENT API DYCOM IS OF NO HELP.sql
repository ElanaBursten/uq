USE [QM]

drop table api_storage_constants
drop table api_storage_credentials

CREATE TABLE [dbo].[api_storage_constants](
	[id] [int] NOT NULL,
	[host] [varchar](max) NOT NULL,
	[rest_url] [varchar](max) NOT NULL,
	[service] [varchar](max) NOT NULL,
	[bucket] [varchar](max) NOT NULL,
	[bucket_folder] [varchar](250) NULL,
	[region] [varchar](150) NOT NULL,
	[algorithm] [varchar](150) NOT NULL,
	[signed_headers] [varchar](max) NOT NULL,
	[content_type] [varchar](max) NOT NULL,
	[accepted_values] [varchar](max) NOT NULL,
	[environment] [varchar](50) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[unc_date_modified] [datetime] NOT NULL,
	[modified_date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[api_storage_credentials](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[storage_type] [varchar](50) NULL,
	[enc_access_key] [nvarchar](max) NULL,
	[enc_secret_key] [nvarchar](max) NULL,
	[unc_date_created] [datetime] NOT NULL,
	[modified_date] [datetime] NULL,
	[unc_date_expired] [datetime] NULL,
	[api_storage_constants_id] [int] NOT NULL,
	[description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[api_storage_constants] ([id], [host], [rest_url], [service], [bucket], [bucket_folder], [region], [algorithm], [signed_headers], [content_type], [accepted_values], [environment], [description], [unc_date_modified], [modified_date]) VALUES (1, N'wppd4wdwo1.execute-api.us-east-1.amazonaws.com', N'/test/qmattach/v1/attachments/LOC_UTL/', N'execute-api', N'dii-dev-qmb-data', N'develop/LOC_UTL/', N'us-east-1', N'AWS4-HMAC-SHA256', N'accept;content-type;host;x-amz-date', N'application/json', N'application/octet-stream,application/json', N'DEVELOPMENT', N'DEV values for QM attachments', CAST(N'2019-10-04T19:00:21.557' AS DateTime), CAST(N'2023-05-08T19:00:21.560' AS DateTime))
INSERT [dbo].[api_storage_constants] ([id], [host], [rest_url], [service], [bucket], [bucket_folder], [region], [algorithm], [signed_headers], [content_type], [accepted_values], [environment], [description], [unc_date_modified], [modified_date]) VALUES (2, N'1l7kjchc4a.execute-api.us-east-1.amazonaws.com', N'/develop/qmattach/v1/attachments/LOC_LOC/', N'execute-api', N'dii-utq-attachments-dev', N'develop/LOC_LOC/', N'us-east-1', N'AWS4-HMAC-SHA256', N'accept;content-type;host;x-amz-date', N'application/json', N'application/octet-stream,application/json', N'DEVELOPMENT', N'DEV values for LOC attachments', CAST(N'2019-10-04T19:00:21.587' AS DateTime), CAST(N'2020-05-04T09:52:45.223' AS DateTime))
SET IDENTITY_INSERT [dbo].[api_storage_credentials] ON 

INSERT [dbo].[api_storage_credentials] ([id], [storage_type], [enc_access_key], [enc_secret_key], [unc_date_created], [modified_date], [unc_date_expired], [api_storage_constants_id], [description]) VALUES (3, N'AWS', N'5n2ihmVgqlHMzImNC+CfCZBO6CYerqkovfeO5EmQSwk=', N'nF5C4KlfuggHLpey93tHqKoAKAdrIp4Pu7gyrqrEZmVGW2ACX+tzpXUmKfwzamyn', CAST(N'2023-04-26T16:00:11.177' AS DateTime), CAST(N'2023-05-08T14:37:20.363' AS DateTime), CAST(N'2023-05-09T16:00:11.177' AS DateTime), 1, N'did this work')
INSERT [dbo].[api_storage_credentials] ([id], [storage_type], [enc_access_key], [enc_secret_key], [unc_date_created], [modified_date], [unc_date_expired], [api_storage_constants_id], [description]) VALUES (4, N'AWS', N'kW7Ngkoyf1ckYUv4aCHD5J3+YPA/6UGB1K2en/nJic4=', N'JQHhuBIzNmfNpvNvUKOiLq7DFC9UuDkDL7n5Od/JYy86LfwxHWxoemrZDHSAukon', CAST(N'2023-04-26T16:00:12.030' AS DateTime), CAST(N'2023-04-26T13:32:38.930' AS DateTime), CAST(N'2023-04-27T16:00:12.030' AS DateTime), 2, N'PROD QM keys for AWS')
SET IDENTITY_INSERT [dbo].[api_storage_credentials] OFF
ALTER TABLE [dbo].[api_storage_constants] ADD  CONSTRAINT [DF_api_storage_constants_modified_date]  DEFAULT (getdate()) FOR [modified_date]
GO
