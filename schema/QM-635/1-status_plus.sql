-- QM-635 Status Plus
-- New Table: status_plus
-- Steven Hull

if object_id('dbo.status_plus') is not null
  drop table dbo.status_plus
go

--create table
CREATE TABLE [dbo].[status_plus](
    [status_plus_id] [int] IDENTITY(1,1) NOT NULL,
	[call_center] [varchar](20) NOT NULL,
	[client_code] [varchar](10) NOT NULL,
	[status] [varchar](5) NOT NULL,
	[status_plus] [varchar](200) NOT NULL,
	[active] [bit] NOT NULL,
	[modified_date] [datetime] NOT NULL
) ON [PRIMARY]
GO


--add contraints
ALTER TABLE [dbo].[status_plus] ADD  CONSTRAINT [call_center_client_code_status] UNIQUE NONCLUSTERED 
(
	[call_center] ASC,
	[client_code] ASC,
	[status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--create trigger
CREATE trigger [dbo].[status_plus_u] on [dbo].[status_plus]
for update
AS
update status_plus set modified_date = getdate() 
where status_plus_id in (select distinct status_plus_id from inserted)

GO

--insert data
insert into status_plus values ('1851','AEPIN','+XJU','MJU,NJU',1,getdate())
insert into status_plus values ('1851','AEPIN','+XFF','MFF,NFF',1,getdate())
insert into status_plus values ('1851','AEPIN','+XDE','MDE,NDE',1,getdate())
insert into status_plus values ('1851','AEPIN','+XRL','MRL,NRL',1,getdate())
insert into status_plus values ('1851','AEPIN','+XXA','MXA,NXA',1,getdate())
insert into status_plus values ('1851','AEPIN','+XLW','MLW,NLW',1,getdate())
insert into status_plus values ('3003','BSCA','+LPE','LPM',1,getdate())


--select * from status_plus where call_center = 1421 and client_code = 'TWCZ40'
--update status_plus
--set modified_date=getdate()




