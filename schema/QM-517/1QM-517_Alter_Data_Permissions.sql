------------------------------------------------
--QM-517
-- Permissions to display sick leave entry on Timesheet
-- EB
------------------------------------------------
if not exists(select * from right_definition where entity_data='TimeSheetsViewSickLeave')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Timesheets - View Sick Leave', 
						'General', 
						'TimeSheetsViewSickLeave', 
						'Limitation does not apply to this right', 
						GetDate());
GO

select * from reference where type = 'ptohrs'
select * from reference where type = 'sickhrs'
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 0, 1, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 1, 2, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 2, 3, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 3, 4, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 4, 5, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 5, 6, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 6, 7, 1)
INSERT INTO reference([type],[description],sortby,active_ind)
			   values('sickhrs', 8, 9, 1)
GO







