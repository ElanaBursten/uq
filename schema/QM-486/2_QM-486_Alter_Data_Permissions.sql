------------------------------------------------
--QM-486 Virtual Work Management Bucket
-- Permissions to view virtual buckets 
-- Modifier: Manager to view (must already be in tree for the user)
-- Will affect which multi_open_totals stored procedure to call
-- EB
------------------------------------------------
if not exists(select * from right_definition where entity_data='TicketsVirtualBucket')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Virtual Bucket', 
						'General', 
						'TicketsVirtualBucket', 
						'Limitation: Manager ID', 
						GetDate());
GO






