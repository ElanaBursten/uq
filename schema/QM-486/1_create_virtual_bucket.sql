-----------------------------------------------
-- QM-486 Custom Virtual Bucket 
-- Release QM-486 virtual_bucket is a new table
-- EB
-----------------------------------------------
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'virtual_bucket')
Create Table virtual_bucket(
virtual_bucket_id int identity(1,1) Primary Key NOT NULL,
manager_id int NOT NULL,
bucket_name varchar(15) NOT NULL,
daysback int NOT NULL,
cofilter varchar(MAX) NULL,  -- use pipes for delimeter
only_pastdue bit NULL,       -- shows only pastdue
only_today bit NULL,         -- shows only today
active bit Default 1
)
GO
      --Not Included:
        --kind varchar(20) NULL,       
        --util_type varchar(15) NULL, 

--drop table virtual_bucket
--select * from virtual_bucket