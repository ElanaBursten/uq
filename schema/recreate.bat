@echo on
osql -S %SVR% -U sa -P %PW% -n -Q "drop database %DB%"
osql -S %SVR% -U sa -P %PW% -n -Q "CREATE database %DB%"
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i uq_schema.sql || goto :failed
pause
if errorlevel 1 goto failed

@REM osql -S %SVR% -U sa -P %PW% -n -d %DB% -i sp-webapp.sql
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_old_tickets_for_locator.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_open_tickets_for_locator.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp-other.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i uq_ref_data.sql || goto :failed

@REM These just remove some SP dependency warnings
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\acknowledge_ticket.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_office_id.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_ticket.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_ticketnumber.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_ticket_serial.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_locate_FCO2.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_locate.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_unacked_tickets_2.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\update_ticket_status.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_locate_FHL1.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_locate_FPL1.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\check_summary_by_ticket_serial_LQW1.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_open_damages_for_locator.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_old_damages_for_locator.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_open_work_orders_for_emp.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\get_old_work_orders_for_emp.sql || goto :failed
osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i sp\update_work_order_status.sql || goto :failed

FOR %%v IN (sp\*.sql) DO osql -S %SVR% -b -U sa -P %PW% -n -d %DB% -i %%v || goto :failed

@REM static reference data from MapInfo:
bcp %DB%..map_geo in AtlantaGrid.dat -T -c -t, -E -b 100000 -S %SVR% -m 1 || goto :failed

@REM the initial data created by users, is used as test data also:
bcp %DB%..map_qtrmin in map_qtrmin.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..map_cell in map_cell.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..office in office.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..reference in reference.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..statuslist in statuslist.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..users in users.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..employee in employee.txt -T -c -t"|" -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..map in map.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..map_page in map_page.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..area in area.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..county_area in county_area.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..call_center in call_center.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..client in client.tsv -T -c -t\t -E -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..customer in customer.txt -T -c -t"|" -E -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_party_respect in billing_party_respect.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..holiday in holiday.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..map_trs in map_trs.tsv -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..routing_list in routing_list.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..profit_center in profit_center.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..carrier in carrier.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..followup_ticket_type in followup_ticket_type.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..asset_type in asset_type.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..upload_location in upload_location.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..right_definition in right_definition.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..employee_right in employee_right.tsv -E -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..group_definition in group_definition.tsv -E -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..employee_group in employee_group.tsv -E -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..group_right in group_right.tsv -E -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..ticket_ack_match in ticket_ack_match.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..term_group in term_group.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..term_group_detail in term_group_detail.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..center_group in center_group.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..center_group_detail in center_group_detail.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..value_group in value_group.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..value_group_detail in value_group_detail.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..locating_company in locating_company.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_output_config in billing_output_config.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_unit_conversion in billing_unit_conversion.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..dycom_period in dycom_period.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..damage_default_est in damage_default_est.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..status_group in status_group.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..status_group_item in status_group_item.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_gl in billing_gl.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..break_rules in break_rules.txt -T -c -E -t"|" -S %SVR% -m 1 || goto :failed
bcp %DB%..upload_file_type in upload_file_type.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..right_restriction in right_restriction.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..restricted_use_message in restricted_use_message.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..required_document in required_document.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..required_document_type in required_document_type.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..eFax_error_codes in eFax_error_codes.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..work_order_work_type in work_order_work_type.tsv -T -c -t\t -E -S %SVR% -m 1 || goto :failed
bcp %DB%..status_translation in status_translation.tsv -E -T -c -t\t -S %SVR% -m 1 || goto :failed

@REM NOTE the intentional lack of -E on these:
bcp %DB%..billing_cc_map in billing_cc_map.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_rate in billing_rate.tsv -T -c -t\t -b 100000 -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_exclude_work_for in billing_exclude_work_for.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed
bcp %DB%..billing_exclude_work_type in billing_exclude_work_type.tsv -T -c -t\t -S %SVR% -m 1 || goto :failed

pause
exit

:failed
@echo ***** FAILED ****
@pause
