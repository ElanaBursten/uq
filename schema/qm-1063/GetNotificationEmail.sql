USE [QM]
GO
/****** Object:  StoredProcedure [dbo].[GetNotificationEmail]    Script Date: 1/15/2025 1:13:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetNotificationEmail]
    @EmpID INT,           -- The employee ID to start the search
    @EmailType NVARCHAR(50) -- The email type (e.g., 'Alerts', 'Errors')
AS
BEGIN
    SET NOCOUNT ON;

    WITH EmployeeHierarchy AS (
        -- Base case: Start with the employee provided in @EmpID
        SELECT 
            e.emp_id,
            e.report_to,
            e.short_name
        FROM 
            employee e
        WHERE 
            e.emp_id = @EmpID

        UNION ALL

        -- Recursive case: Find the manager of the current employee
        SELECT 
            e.emp_id,
            e.report_to,
            e.short_name
        FROM 
            employee e
        INNER JOIN 
            EmployeeHierarchy eh
            ON e.emp_id = eh.report_to
    ),
    EligibleNotification AS (
        -- Find all valid email notifications in the hierarchy for the given email_type
        SELECT 
            eh.emp_id,
            eh.short_name,
            nu.email_destination
        FROM 
            EmployeeHierarchy eh
        INNER JOIN 
            notification_email_user nu
            ON eh.emp_id = nu.emp_id
        WHERE 
            nu.email_type = @EmailType
    )
    -- Return the first matching email address in the hierarchy
    SELECT TOP 1
        emp_id,
        short_name,
        email_destination
    FROM 
        EligibleNotification;
END;
