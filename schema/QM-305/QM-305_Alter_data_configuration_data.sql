--QM-305  Make # of Tickets in Ticket search configuration

if not exists(select * from configuration_data
where name = 'TicketSearchMax')
	
  INSERT INTO configuration_data
             (name
             ,value
             ,editable
             ,[description]
             ,sync)
       VALUES
             ('TicketSearchMax'
             ,5000
             ,1
             ,'Ticket Search Max Return Number'
             ,1)
  GO

--select * from configuration_data
--where name = 'TicketSearchMax'

--Change the max value
--update Configuration_data
--set value = 2345
--where name = 'TicketSearchMax'

