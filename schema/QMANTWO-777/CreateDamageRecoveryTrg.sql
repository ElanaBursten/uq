USE [LocQM]
GO

/****** Object:  Trigger [dbo].[damage_recovery_iu]    Script Date: 7/22/2019 11:52:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create trigger [dbo].[damage_recovery_iu] on [dbo].[damage_recovery]
for INSERT, UPDATE
not for replication
as
BEGIN
  declare @ChangeDate datetime;
  declare @RecoveryID int;
  declare @Operation char(1);
  declare @EmpID int;

--Fields from damage_recovery that we want to retain as-is
  declare @damageid int;
  declare @paid_date datetime;
  declare @status varchar(20);
  declare @amt decimal(12,2);
  declare @co varchar(50);
  declare @comment varchar(8000);
  declare @expdate datetime;
  declare @moddate datetime;
  declare @modby int;


  set @ChangeDate=GetDate();

  --If Exists(select * from DELETED)
  --    select @Operation='U'
	 -- else
	 -- select @Operation='I'

    select @RecoveryID=i.recovery_id, @damageid=i.damage_id, 
	       @paid_date=i.paid_date, @status=i.recovery_status, 
	       @amt=i.recovery_amount, @co=i.recovery_company, @comment=i.comment,
		   @expdate=i.export_date, @moddate=i.modified_date, @modby=i.modified_by, 
		   @EmpID=i.modified_by from inserted i

  -- "Deactivate" any current history records
      update damage_recovery_history 
	  set active = 0, modified_date = @ChangeDate 
	  where recovery_id = @RecoveryID

  --Insert new record into history table
    insert into damage_recovery_history (recovery_id, archive_date, damage_id, paid_date, recovery_status, recovery_amount, recovery_company, comment,                                                            active, export_date, modified_date, modified_by)
	                              values(@RecoveryID, @ChangeDate, @damageid, @paid_date, @Status, @amt, @co, @comment, 
								                      1, @expdate, @ChangeDate, @EmpID)


     update damage_recovery set modified_date = getdate() where recovery_id in (select recovery_id from inserted)
END
GO

ALTER TABLE [dbo].[damage_recovery] ENABLE TRIGGER [damage_recovery_iu]
GO

