USE [LocQM]
GO

/****** Object:  Table [dbo].[damage_recovery]    Script Date: 7/22/2019 11:52:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[damage_recovery](
	[recovery_id] [int] IDENTITY(1,1) NOT NULL,
	[damage_id] [int] NOT NULL,
	[paid_date] [datetime] NULL,
	[damage_state] [varchar](2) NULL,
	[recovery_status] [varchar](20) NOT NULL,
	[recovery_amount] [decimal](12, 2) NULL,
	[recovery_company] [varchar](50) NULL,
	[comment] [varchar](8000) NULL,
	[export_date] [datetime] NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [int] NULL,
	[TotalAttachments] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[recovery_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

