USE [LocQM]
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','PENDING','PENDING',1)
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','NOT RECOVERABLE','NOT RECOVERABLE',1)
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','RECOVERED ALL','RECOVERED ALL',1)
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','RECOVERED PART','RECOVERED PART',1)
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','FAILED','FAILED',1)
GO

INSERT INTO [dbo].[reference]
           ([type],[code],[description],[active_ind])
     VALUES
           ('recovstat','SUBMITTED','SUBMITTED',1)
GO

