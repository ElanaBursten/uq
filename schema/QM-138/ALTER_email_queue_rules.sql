/*
   Thursday, March 19, 20203:33:32 PM
   User: 
   Server: SSDS-UTQ-QM-01-DV
   Database: QM
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.email_queue_rules ADD
	incl_bcc bit NOT NULL CONSTRAINT DF_email_queue_rules_incl_bcc DEFAULT 0,
	bcc_address nvarchar(100) NULL
GO
ALTER TABLE dbo.email_queue_rules SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.email_queue_rules', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.email_queue_rules', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.email_queue_rules', 'Object', 'CONTROL') as Contr_Per 