--QMANTWO-733 Timesheet Per Diem
--Update 9/9/2019 EB
--Restricts Per Diem on the Timesheet
if not Exists(select * from right_definition WHERE entity_data = 'TimesheetsPerDiemEntry')
  INSERT INTO right_definition
             (right_description,right_type,entity_data,parent_right_id,modifier_desc)
       VALUES
            ('Timesheets - Per Diem Entry','General','TimesheetsPerDiemEntry', NULL,
            'Limitation - not defined')
GO
