USE [QM]
GO
CREATE NONCLUSTERED INDEX [idxHasAckInsertDate, DBO]
ON [dbo].[ticket_ack] ([has_ack],[insert_date])
INCLUDE ([ticket_id],[ack_date],[ack_emp_id],[locator_emp_id],[ticket_version_id],[claimedBy_Emp_id])
GO

