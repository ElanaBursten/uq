--QM-463 Edit Previously Statused Tickets - Closed_Date, Retain Closed_Date or the locator closing it
--     Closed Date is basically uses as "Statused Date"
--     TicketsPreserveClosedDate

if not exists(select * from right_definition where entity_data='TicketsPreserveClosedDate')
  INSERT INTO right_definition(right_description, 
                               right_type, 
							   entity_data, 
							   modifier_desc, 
							   modified_date)
                        values('Tickets - Edit Closed Locates with PRESERVE', 
						'General', 
						'TicketsPreserveClosedDate', 
						'Limitation does not apply to this right', 
						GetDate());
GO

--select * from right_definition where entity_data='TicketsPreserveClosedDate'

--delete from right_definition where entity_data='TicketsPreserveClosedDate'


