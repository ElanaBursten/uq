unit streetMapTickets;      // qm-686

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, StdCtrls, ExtCtrls, ComCtrls, MSHTML, System.UITypes,
  Buttons, DB, IPPeerClient, uWVWindowParent, MyWorkWaze, Winapi.ActiveX,
  Data.Bind.Components, Data.Bind.ObjectScope, JSON, JSON.Types, JSON.Readers,
  Vcl.Grids, Vcl.DBGrids, REST.Types, Vcl.AppEvnts, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, QMConst, cxVariants,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxMaskEdit,
  System.Sensors, dbisamtb, System.Sensors.Components, System.ImageList, Vcl.ImgList,

  cxImageList, cxClasses, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, xqD10TicketColor,
  Winapi.KnownFolders, Winapi.ShlObj, cxCheckBox, cxContainer,

  uWVBrowser, uWVWinControl, uWVTypes, uWVConstants, uWVTypeLibrary,
  uWVLibFunctions, uWVLoader, uWVInterfaces, uWVCoreWebView2Args,
  uWVBrowserBase, dxScrollbarAnnotations, dxSkinsCore, dxCore, dxSkinsForm;


type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);
  TIniDataType =(idtString, idtInteger, idtBool, idtDateTime, idtDate,idtTime, idtFloat);
  TSensorType=(stGpsGate, stLocSensor, stUserInput);
type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

 type
  TGPSData = record
    Longitude: String[13];
    Latitude: String[13];
    dateSend : TDatetime;
  end;

 type
  TComboBox = class(StdCtrls.TComboBox) //Temporary subclass to get at the CNDraw message.
 private
    procedure CNDrawItem(var Message : TWMDrawItem);    message CN_DRAWITEM;
 end;


const
  UM_SET_FROM_MAP = WM_USER + $104;
  UM_SET_TO_MAP = WM_USER + $105;
  UM_SET_EMP_ACT_MAP = WM_USER + $106;
type
  TfrmStreetMapTickets = class(TForm)
    PanelHeader: TPanel;
    CheckBoxTraffic: TCheckBox;
    CheckBoxStreeView: TCheckBox;
    pnlMap: TPanel;
    dsTicket: TDataSource;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    DBGrid1DBTableView1: TcxGridDBTableView;
    DBGrid1Level1: TcxGridLevel;
    DBGrid1: TcxGrid;
    DBGrid1DBTableView1ticket_number: TcxGridDBColumn;
    DBGrid1DBTableView1kind: TcxGridDBColumn;
    DBGrid1DBTableView1GreatCircleDist: TcxGridDBColumn;
    DBGrid1DBTableView1DriveDist: TcxGridDBColumn;
    DBGrid1DBTableView1DriveTime: TcxGridDBColumn;
    DBGrid1DBTableView1work_lat: TcxGridDBColumn;
    DBGrid1DBTableView1work_long: TcxGridDBColumn;
    DBGrid1DBTableView1WorkAddress: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxltYellow: TcxStyle;
    cxltRed: TcxStyle;
    cxBlue: TcxStyle;
    cxGreen: TcxStyle;
    DBGrid1DBTableView1Direction: TcxGridDBColumn;
    cxImageList1: TcxImageList;
    ProgressBar1: TProgressBar;
    CheckBoxDriveTimeDist: TCheckBox;
    LocationSensor1: TLocationSensor;
    DBGrid1DBTableView1Priority: TcxGridDBColumn;
    DBGrid1DBTableView1DueDate: TcxGridDBColumn;
    cxltPurple: TcxStyle;
    dbIsamStreetMap: TDBISAMDatabase;
    qryTicket: TDBISAMQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    StringField6: TStringField;
    FloatField1: TFloatField;
    qryTicketPriority: TStringField;
    qryTicketdue_date: TDateTimeField;
    qryTicketwebColor: TStringField;
    qryTicketwork_address_number: TStringField;
    qryTicketwork_address_street: TStringField;
    qryTicketwork_city: TStringField;
    qryTicketwork_state: TStringField;
    qryTicketwork_cross: TStringField;
    qryTicketcorr_Lat: TFloatField;
    qryTicketcorr_Lng: TFloatField;
    BtnClearMarkers: TButton;
    BtnClearDirections: TBitBtn;
    cxYellow: TcxStyle;
    cxltOrange: TcxStyle;
    cxltWhite: TcxStyle;
    cxltSilver: TcxStyle;
    DBGrid1DBTableView1Status: TcxGridDBColumn;
    qryTicketStatus: TStringField;
    qryTicketalert2: TStringField;
    qryTicketlocates: TStringField;
    Holidays: TDBISAMQuery;
    TicketLocates: TDBISAMTable;
    qryTicketticket_status: TIntegerField;
    qryTicketticket_type: TStringField;
    qryTicketticket_format: TStringField;
    qryTicketlegal_good_thru: TDateTimeField;
    qryTicketalert: TStringField;
    qryTicketdo_not_mark_before: TDateTimeField;
    qryTicketworkload_date: TDateTimeField;
    DBGrid1DBTableView1TicketStatus: TcxGridDBColumn;
    DBGrid1DBTableView1Selected: TcxGridDBColumn;
    DBGrid1DBTableView1WebColor: TcxGridDBColumn;
    DBGrid1DBTableView1Work_address_street: TcxGridDBColumn;
    DBGrid1DBTableView1ticket_id: TcxGridDBColumn;
    qryTicketcolor: TIntegerField;
    DBGrid1DBTableView1Color: TcxGridDBColumn;
    qryTicketwork_type: TStringField;
    DBGrid1DBTableView1WorkType: TcxGridDBColumn;
    WebBrowser1: TWVBrowser;
    WVWindowParent1: TWVWindowParent;
    Timer1: TTimer;
    qryTicketroute_order: TBCDField;
    DBGrid1DBTableView1route_order: TcxGridDBColumn;
    pnlButtons: TPanel;
    lblLat: TLabel;
    Label1: TLabel;
    edtLat: TEdit;
    edtLng: TEdit;
    chboxAutoPlot: TCheckBox;
    cbLocationFeed: TComboBox;
    ckboxPosLocked: TCheckBox;
    btnRefreshTickets: TButton;
    btnPlotSelected: TButton;
    cboColorSelect: TComboBox;
    qryGPSlocation: TDBISAMQuery;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    qryGoogleMapsAPI: TDBISAMQuery;
    dxSkinController1: TdxSkinController;

    procedure CheckBoxTrafficClick(Sender: TObject);

    procedure CheckBoxStreeViewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure BtnClearMarkersClick(Sender: TObject);

    procedure BtnClearDirectionsClick(Sender: TObject);
    procedure btnRefreshTicketsClick(Sender: TObject);
    procedure qryTicketCalcFields(DataSet: TDataSet);
    procedure DBGrid1DBTableView1DblClick(Sender: TObject);
    procedure DBGrid1DBTableView1DirectionCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CheckBoxDriveTimeDistClick(Sender: TObject);
    procedure LocationSensor1LocationChanged(Sender: TObject; const OldLocation, NewLocation: TLocationCoord2D);
    procedure UseStartAddress(Sender: TObject);

    procedure chboxAutoPlotClick(Sender: TObject);
    procedure cbLocationFeedChange(Sender: TObject);
    procedure DBGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
      AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure DBGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure btnPlotSelectedClick(Sender: TObject);
    procedure DBGrid1DBTableView1SelectionChanged(Sender: TcxCustomGridTableView);
//    procedure RadioGroup1Click(Sender: TObject);
    procedure ckboxPosLockedClick(Sender: TObject);
    procedure DBGrid1DBTableView1DataControllerCompare(ADataController: TcxCustomDataController; ARecordIndex1, ARecordIndex2,
      AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cboColorSelectCloseUp(Sender: TObject);
    procedure cboColorSelectDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure WebBrowser1AfterCreated(Sender: TObject);
    procedure WebBrowser1InitializationError(Sender: TObject; aErrorCode: HRESULT; const aErrorMessage: wvstring);
    procedure WebBrowser1WebMessageReceived(Sender: TObject; const aWebView: ICoreWebView2;
      const aArgs: ICoreWebView2WebMessageReceivedEventArgs);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);

  private
    { Private declarations }
    itemNo:integer;
    homeAddress : string;
    DisplayName : string;
    logDir:string;

    fUtc: string;
    fLat: string;
    fLng: string;
    fLocatDT: string;
    fShortName: string;
    fMulti: Boolean;
    Address: String;

    frmWaze: TfrmWaze;

    Limited:boolean;
    LimitedAmt:integer;
    IniDataType : TIniDataType;
    LogResult :TLogResults;
    SensorType:TSensorType;
    ActEmpID : integer;
    fDistAccumulator:double;
    AppLocal:integer;
    MyIniPath:string;
    MyAppPath:string;
    xqTicketColor: TxqTicketColor;
    fCurrentGPSLatitude: double;
    fCurrentGPSLongitude: double;
    IBePlotting:boolean;
    plotlist: TStringList;
    ShowDriveDistance:boolean;
    procedure OnMyWorkChanged(var Message: TMessage); message UM_SET_TO_MAP;
    procedure WMCopyMyData(var Msg : TWMCopyData); message WM_COPYDATA;
    procedure CreateMap(Sender: TObject);

    procedure HandleCopyDataRecord(copyDataStruct : PcopyDataStruct);

    function connectDB: boolean;
    function EnDeCrypt(const Value: String): String;

    function getOriginLL: string;

    function calculateDistance( dlat1, dLon1, dlat2, dlon2: extended):extended;
    procedure SetStartMarker(Sender: TObject; sLat, sLng, sAddress:string);
    function parseLatLng(LatLng:string):string;
    function ReadINI: Boolean;
    procedure SaveToINI(Section, Ident, value:string; IniDataType: TIniDataType);

    procedure clearLogRecord;
    procedure WriteLog(LogResult: TLogResults);
    procedure AddEmployeeActivityEvent(const MapActivity:TmaMapActivity;matMapActType:TmatMapActType);
    procedure ToggleSelections;
    procedure RunMethod(Sender: TObject; methodName: string);
    procedure LoadMap;
    function GetAppVersionStr: string;
    procedure GetLocation;
    procedure SetUpTicketQuery(Empid: integer; ROLimited: Boolean);

  public
    property CurrentGPSLatitude: double read fCurrentGPSLatitude write fCurrentGPSLatitude;
    property CurrentGPSLongitude: double read fCurrentGPSLongitude write fCurrentGPSLongitude;
    property CurAddress: string read Address write Address;

    property LocatDT: string Read fLocatDT write fLocatDT;
    property Lat: string read fLat write fLat;
    property Lng: string read fLng write fLng;
    property UTC: string read fUtc write fUtc;
    property ShortName: string read fShortName write fShortName;
    property Multi: Boolean read fMulti write fMulti;
    procedure ProcessLocationForWazeQR(latlng, ticketNbr, address: string);
  end;

  function GetRevGeoCode(StrLatLng: Shortstring): Shortstring;  stdcall; external 'Geocode.dll';
  function GetGeoCode(StrAddress: Shortstring): Shortstring; stdcall; external 'Geocode.dll';
  function GetDistanceTime(OriginLatLng, DestLatLng: Shortstring;
   out Distance: ShortString; out Time: ShortString): Shortstring; stdcall; external 'Geocode.dll';
  function GetHeading(dlat1,dlng1,dlat2,dlng2: double;
         Var AzimuthInit, AzimuthFinal: double; units:integer): double; stdcall;  external 'Geocode.dll';

 function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;

var
  frmStreetMapTickets: TfrmStreetMapTickets;
  GoogleAPIKey: AnsiString;    //qm-931


CONST
  GOOGLE_MAPS_HDR=         //qm-931
  '<html> '+
  '<head> '+
  '  <meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />  '+
  '  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=%s&amp;callback=initMap"></script> '+
  '  <script type="text/javascript"> ';

  GOOGLE_MAPS_BODY=         //qm-931
        'var map; '+
        'var markersArray = []; '+
        'var startmarkerArray = []; '+
        'var trafficLayer; '+
        'var markerid = 1; '+
        'var start; '+
        'var end; '+
        'var address;  '+
        'var directionsService; '+
        'var directionsDisplay; '+
        'var mapwidth; '+
        'var dirwidth; '+
        'var startlatlng; '+
        'var startaddress; '+
        'var startmarker; '+
        'var htmlColor; '+
        'function initialize() {  '+
        '  var latlng = new google.maps.LatLng(34.000, -84.0000); '+
        '  var myOptions = { '+
        '    zoom: 15, '+
        '    center: latlng, '+
        '    mapTypeId: google.maps.MapTypeId.ROADMAP '+
        '  }; '+
        '  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); '+
        '  directionsDisplay = new google.maps.DirectionsRenderer({}); '+
        '  directionsService = new google.maps.DirectionsService; '+
        '  directionsDisplay.setMap(map); '+
        '  directionsDisplay.setPanel(document.getElementById(''right-panel'')); '+
        '  mapwidth = document.getElementById("map_canvas"); '+
        '  dirwidth = document.getElementById("right-panel"); '+
        '  mapwidth.style.width = "100%"; '+
        '  dirwidth.style.height = "0%"; '+
        '  trafficLayer = new google.maps.TrafficLayer();  '+
        '  map.set("streetViewControl", false); '+
        '} '+
        ' '+
        'function GotoLatLng(Lat, Lng) { '+
        '  var latlng = new google.maps.LatLng(Lat, Lng); '+
        '  map.zoom = 15; '+
        '  map.setCenter(latlng); '+
        '} '+
        '  '+
        'function ClearMarkers() { '+
        '  if (markersArray) { '+
        '    for (i in markersArray) { '+
        '      markersArray[i].setMap(null); '+
        '    } '+
        '    markersArray = []; '+
        '    markerid = 1; '+
        '  } '+
        '} '+
        ' '+
        'function ClearDirections() {  '+
        '  directionsDisplay.set(''directions'', null); '+
        '  mapwidth.style.width = "100%"; '+
        '  dirwidth.style.height = "0%"; '+
        '} '+
        '  '+
        'function PlaceTickets(ItemNo, Lat, Lng, TicketNum, mAddress, sKind, htmlColor) { '+
        '  var latlng = new google.maps.LatLng(Lat, Lng); '+
        '  map.setCenter(startlatlng); '+
        '  PutMarker(ItemNo, Lat, Lng, TicketNum, "Ticket " + TicketNum, mAddress, sKind, htmlColor); '+
        '} '+
        '  '+
        'function PutStartMarker(Lat, Lng, Msg, sAddress) { '+
        '  ClearStartMarker(); '+
        '  startlatlng = new google.maps.LatLng(Lat, Lng); '+
        '  startaddress = startlatlng; '+
        '  startmarker = new google.maps.Marker({ '+
        '    position: startlatlng, '+
        '    map: map, '+
        '    markernbr: 0, '+
        '    nearbyaddress: sAddress, '+
        '    visible: true, '+
        '    title: Msg + " (" + Lat + "," + Lng + ")" +" "+ sAddress '+
        '  }); '+
        '  '+
        '  map.setCenter(startlatlng); '+
        '  startmarkerArray.push(startmarker); '+
        '} '+
        '  '+
        'function ClearStartMarker() { '+
        '  if (startmarkerArray) {  '+
        '    if (startmarkerArray.length > 0) { '+
        '      startmarkerArray[0].setMap(null); '+
        '      startmarkerArray = []; '+
        '    } '+
        '  }  '+
        '}  '+
        '   '+
        'function PutMarker(ItemNo, Lat, Lng, TicketNum, Msg, mAddress, sKind, htmlColor) { '+
        '  var latlng = new google.maps.LatLng(Lat, Lng); '+
        '  var color; '+
        '  color = htmlColor; '+
        '  var marker = new google.maps.Marker({ '+
        '    position: latlng, '+
        '    icon: { '+
//        '    url: "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=info|bb|"+ItemNo+"|"+color+"|000000", '+
        '    url:"https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png", '+
        '   '+
        '      fillOpacity: 1, '+
        '      strokeColor: "#000", '+
        '      strokeWeight: 2, '+
        '      scaledsize: new google.maps.Size(30, 40), '+
        '      origin: new google.maps.Point(0, 0), '+
        '      anchor: new google.maps.Point(0, 0), '+
        '      scale: 1 '+
        '    }, '+
        '    map: map, '+
        '    title: Msg+" "+TicketNum + " (" + Lat + "," + Lng + ")", '+
        '    markernbr: ItemNo, '+
        '    nearbyaddr: mAddress, '+
        '    ticketnbr: TicketNum '+
        '  }); '+
        '   '+
        '  marker.addListener(''click'', function(markerarray) {}); '+
        '  marker.addListener(''click'', function(markerarray) { '+
        '    sendMessageToHostApp(''MarkerDetails''+'',''+this.position.lat()+'',''+ this.position.lng()+'',''+this.nearbyaddr+'',''+this.ticketnbr);  '+
        '  }); '+
        '  marker.addListener(''rightclick'', function(markersarray) { '+
        '    map.setZoom(15); '+
        '    map.setCenter(marker.getPosition()); '+
        '    if (startaddress !== undefined) { '+
        '      if (confirm("Get directions to this ticket?") == true) { '+
        '        start = startaddress; '+
        '        end = this.nearbyaddr;  '+
        '        dirwidth.style.height = "100%";  '+
        '        dirwidth.style.width = "30%";  '+
        '        mapwidth.style.width = "68%"; '+
        '        calculateAndDisplayRoute(directionsService, directionsDisplay); '+
        '      } '+
        '    } else { '+
        '      alert(''You have not set a start address so a route cannot be calculated'');  '+
        '    }  '+
        '  });  '+
        '   '+
        '  markerid += 1; '+
        '  markersArray.push(marker); '+
        '  ShowAllMarkers(); '+
        '  '+
        '}  '+
        '   '+
        'function sendMessageToHostApp(messageToSend){  '+
        '  window.chrome.webview.postMessage(messageToSend); '+
        '  } '+
        '   '+
        'function ShowAllMarkers() { '+
        '  var bounds = new google.maps.LatLngBounds();  '+
        '  for (var i = 0; i < markersArray.length; i++) { '+
        '    bounds.extend(markersArray[i].getPosition());  '+
        '  } '+
        '  map.fitBounds(bounds); '+
        '} '+
        '   '+
        'function TrafficOn() { '+
        '  trafficLayer.setMap(map); '+
        '} '+
        ' '+
        'function TrafficOff() { '+
        '  trafficLayer.setMap(null); '+
        '}  '+
        '  '+
        'function StreetViewOn() {  '+
        '  map.set("streetViewControl", true);  '+
        '}  '+
        '  '+
        'function StreetViewOff() { '+
        '  map.set("streetViewControl", false); '+
        '} '+
        '  '+
        'function findobjectbykey(array, key, value) { '+
        '  for (var i = 0; i < array.length; i++) { '+
        '    if (array[i][key] === value) { '+
        '      return array[i]; '+
        '    } '+
        '  } '+
        '  return null;  '+
        '} '+
        '  '+
        'function calculateAndDisplayRoute(directionsService, directionsDisplay) { '+
        '  directionsService.route({ '+
        '    origin: start, '+
        '    destination: end, '+
        '    travelMode: ''DRIVING'' '+
        '  }, function(response, status) {  '+
        '    if (status === ''OK'') { '+
        '      directionsDisplay.setDirections(response); '+
        '    } else { '+
        '      window.alert(''Directions request failed due to '' + status);  '+
        '    } '+
        '  }); '+
       ' } '+
    '  </script> '+
    '  <style> '+
    '    #right-panel {  '+
    '      font-family: ''Roboto'', ''sans-serif''; '+
    '      line-height: 30px; '+
    '      padding-left: 10px;  '+
    '    }  '+
    '     '+
    '    #right-panel select, '+
    '    #right-panel input { '+
    '      font-size: 15px;  '+
    '    } '+
    '     '+
    '    #right-panel select { '+
    '      width: 100%; '+
    '    }  '+
    '      '+
    '    #right-panel i { '+
    '      font-size: 12px; '+
    '    } '+
    '      '+
    '    #right-panel { '+
    '      height: 100%; '+
    '      float: right;  '+
    '      width: 390px;  '+
    '      overflow: auto; '+
    '    }  '+
    '      '+
    '    @media print {  '+
    '      #map { '+
    '        height: 500px; '+
    '        margin: 0; '+
    '      }  '+
    '      '+
    '      #right-panel {  '+
    '        float: none;  '+
    '        width: auto;  '+
    '      }  '+
    '    }  '+
    '     '+
    '    #map_canvas { '+
    '      height: 100%; '+
    '    }  '+
    '     '+
    '    #map_canvas { '+
    '      margin-right: 400px; '+
    '    } '+
    '  </style> '+
    '</head> '+
    '<body onload="initialize()">  '+
  '    <div id="right-panel"></div> '+
  '    <div id="map_canvas"></div> '+
  '  </body> '+
 ' </html> ';


implementation

uses
  math, DateUtils, OdIsoDates, System.IniFiles, StrUtils,Registry;

{$R *.dfm}

procedure TfrmStreetMapTickets.CreateMap(Sender: TObject);
begin
  try
    qryGoogleMapsAPI.Open;    //qm-931
    GoogleAPIKey:=qryGoogleMapsAPI.FieldByName('value').AsString;   //qm-931

  finally
    qryGoogleMapsAPI.close;    //qm-931
  end;
   LoadMap;   //qm-931
    If DeleteFile(MyAppPath+'StreetMapTickets.html')then    //qm-931
    begin
      LogResult.MethodName := 'Delete HTML Map';
      LogResult.LogType := ltInfo;
      LogResult.Status := 'HTML Map deleted';
      WriteLog(LogResult);
    end;
end;

procedure TfrmStreetMapTickets.LoadMap;  //qm-931
var
  htmlString: string;
begin

  try
    try
        htmlString:=  Format(GOOGLE_MAPS_HDR,[GoogleAPIKey]) +GOOGLE_MAPS_BODY;  //qm-931
//        htmlString:=  GOOGLE_MAPS_HDR+GOOGLE_MAPS_BODY;  //qm-931
        WebBrowser1.NavigateToString(htmlString);
        LogResult.MethodName := 'LoadMap';
        LogResult.LogType := ltInfo;
        LogResult.Status := 'WebBrowser1.NavigateToString';

        WriteLog(LogResult);
        StatusBar1.Panels[2].Text := 'Google Maps';
    except
      on e : exception do
      begin
        LogResult.MethodName := 'LoadFromFileAsString';
        LogResult.LogType := ltError;
        LogResult.ExcepMsg := e.message;
        LogResult.DataStream:=htmlString;
        WriteLog(LogResult);
      end;


    end;
  finally
  WebBrowser1.Refresh;
  end;
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  LatStr, LngStr: string;
begin
  AHandled:= true;
  if (AButton = mbRight) then
  begin
    LatStr := qryTicket.FieldByName('corr_lat').AsString;
    LngStr := qryTicket.FieldByName('corr_lng').AsString;
    WebBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)', [LatStr, LngStr]));
  end
  else
  if  (AButton = mbLeft) then
  begin
    if DBGrid1DBTableView1.DataController.RecordCount > 1 then
      ToggleSelections;
  end;
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  TicketStatus: TTicketStatus;
begin
  if not AViewInfo.Selected then
  begin
    if not VarIsNull(AViewInfo.GridRecord.Values[DBGrid1DBTableView1TicketStatus.Index]) then
      TicketStatus := TTicketStatus(word(AViewInfo.GridRecord.Values[DBGrid1DBTableView1TicketStatus.Index])) // QMANTWO-503
    else
      TicketStatus := [];
    ACanvas.Brush.Color := xqTicketColor.GetTicketColor(TicketStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
  end;
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1DataControllerCompare(ADataController: TcxCustomDataController;
  ARecordIndex1, ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);
var
  ASortColumnIndex: Integer;
  AValue1, AValue2: Variant;
begin
//   apply custom sorting only for the WorkAddr column
  if (AItemIndex = DBGrid1DBTableView1WorkAddress.Index) then
  begin
    ASortColumnIndex := DBGrid1DBTableView1Work_address_street.Index;
    AValue1 := ADataController.Values[ARecordIndex1, ASortColumnIndex];
    AValue2 := ADataController.Values[ARecordIndex2, ASortColumnIndex];
    Compare := VarCompare(AValue1, AValue2);
  end
  else
  begin
    // compare values in other columns
    Compare := VarCompare(V1, V2);
  end;
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1DblClick(Sender: TObject);
var
  MyWorkHandle: HWnd;
  ticketID: integer;
  sAct: string;
begin
  ticketID := qryTicket.FieldByName('ticket_id').AsInteger;
  MyWorkHandle := FindWindow('TMainForm', nil);
  PostMessage(MyWorkHandle, UM_SET_FROM_MAP, 0, ticketID);
  application.Minimize;
  AddEmployeeActivityEvent(maViewTicketDetails, matUser);
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1DirectionCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  degree: double;
begin
  try
    degree := -100;

    if ((AViewInfo.Value = null) or (AViewInfo.Value < 1)) then
    begin
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 0);
      ADone := True;
      exit;
    end
    else
      degree := AViewInfo.Value;


         if ((degree > 337.5) or (degree <= 22.5)) then // normalized
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 1)  // U
    else if ((degree > 22.5) and (degree <= 67.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 2)  // UR
    else if ((degree > 67.5) and (degree <= 112.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 3)  // R
    else if ((degree > 112.5) and (degree <= 157.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 4)  // LR
    else if ((degree > 157.5) and (degree <= 202.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 5)  // D
    else if ((degree > 202.5) and (degree <= 247.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 6)  // LL
    else if ((degree > 247.5) and (degree <= 292.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 7)  // L
    else if ((degree > 292.5) and (degree <= 337.5)) then
      ACanvas.DrawImage(cxImageList1, AViewInfo.Bounds.Left, AViewInfo.Bounds.Top, 8); // UL

    ADone := True;
  except
    on E: Exception do
    begin
      LogResult.MethodName := 'Marker Direction';
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmStreetMapTickets.DBGrid1DBTableView1SelectionChanged(Sender: TcxCustomGridTableView);
begin
   if DBGrid1DBTableView1.DataController.RecordCount >1 then
   ToggleSelections;
end;

procedure TfrmStreetMapTickets.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LogResult.MethodName := 'FormClose';
  LogResult.Status := '*********************** Map Shut Down ************************************';
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);

  qryTicket.close;
  dbIsamStreetMap.connected := false;
  PlotList.Free;
  xqTicketColor.Free;

  Action := caFree;
end;

procedure TfrmStreetMapTickets.FormCreate(Sender: TObject);
begin
  ActEmpID := -1;
  limited :=False; //qm-903 sr
  ActEmpID:= StrToInt(paramstr(1));
  limited := (paramstr(2)<>''); //qm-903 sr
  if (paramstr(3)<>'') then
  LimitedAmt:= StrToInt(paramstr(3));

  xqTicketColor := TxqTicketColor.Create;
  with cboColorSelect.Items do
  begin
    AddObject('None', TObject(clWhite));
    AddObject('All', TObject(clDkGray));
    AddObject('PastDue', TObject(xqTicketColor.CriticalTicketColor));
    AddObject('DueNow', TObject(xqTicketColor.DueTicketColor));
    AddObject('DueIn2Hours', TObject(xqTicketColor.DueHoursTicketColor));
    // AddObject('DoNotMark', TObject(xqTicketColor.DoNotMarkTicketColor));
    // AddObject('HiPrecision', TObject(xqTicketColor.HighestGeocodePrecisionColor));
    // AddObject('OrdPrecision', TObject(xqTicketColor.AcceptableGeocodePrecisionColor));
    // AddObject('ZeroMatches', TObject(xqTicketColor.ZeroGeocodeMatchesColor));
    // AddObject('Due7Tomorrow', TObject(xqTicketColor.Due7TomorrowColor));
  end;
  cboColorSelect.ItemIndex := 0;
  plotlist := TStringList.Create;
  plotlist.Sorted := true;
  plotlist.Duplicates := DupIgnore;
  IBePlotting := false;

  try
    MyIniPath := IncludeTrailingBackslash(ExtractFilePath(paramStr(0)));
    ReadINI();
    if AppLocal = 1 then
      MyAppPath := GetKnownFolderPath(FOLDERID_LocalAppData)
    else
      MyAppPath := IncludeTrailingBackslash(ExtractFilePath(paramStr(0)));
    connectDB;

    if limited= True then
    begin
      StatusBar1.Panels[4].Text:='ROLimited';   //qm-903 sr  plots RO
      SetUpTicketQuery(ActEmpID, True);
    end
    else
    begin
      StatusBar1.Panels[4].Text:='No Limit';
      SetUpTicketQuery(ActEmpID, False);
    end;

    StatusBar1.Panels[1].Text := GetAppVersionStr;

    fDistAccumulator := 0.0;

    LogResult.MethodName := 'FormCreate';
    LogResult.Status := '*********************** New Start Up ************************************';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
    if (SensorType = stLocSensor) then
      LocationSensor1.Active := true;


  except
    on e: exception do
    begin
      LogResult.MethodName := 'Setting Data Path';
      LogResult.ExcepMsg := e.Message;
      LogResult.Status:='Data path invalid or empty';
      LogResult.LogType := ltError;
      WriteLog(LogResult);
    end;

  end;
end;

procedure TfrmStreetMapTickets.SetUpTicketQuery(Empid:integer; ROLimited:Boolean);  //qm-903 sr
const
  QRY_TICKETS =
  'Select ticket_id, ticket_number, r.code as Priority,  kind,  ticket_type,  ticket_format, legal_good_thru, '+
  'alert,  do_not_mark_before,  workload_date,  Status,  work_lat,  work_long,'+
  'work_address_number +'' ''+ work_address_street +'' ''+ work_city +'' ''+ work_state as WorkAddress, '+
  'due_date,  work_address_number,  work_address_street,  work_cross,  work_city,  work_state, '+
  'work_type, IFNULL(route_order THEN 9999 ELSE route_order) as route_order '+
'FROM Ticket '+
'  inner join locate on ticket.ticket_id=locate.ticket_id '+
'  LEFT outer JOIN REFERENCE R ON (work_priority_id = R.ref_id) ';

WHERE_NOT_LIMIT=
'where locate.locator_id=%d '+
'      and locate.active '+
'      and NOT locate.closed '+
'group by  ticket_number, ticket_id  '+
'order by  route_order, r.code desc ';

WHERE_ROUTE_LIMITED=
'where locate.locator_id=%d '+
'      and locate.active '+
'      and NOT locate.closed '+
'group by  ticket_number, ticket_id  '+
'order by  route_order, r.code desc Top %d ';
begin
  qryTicket.SQL.Clear;
  qryTicket.SQL.Add(QRY_TICKETS);
  if ROLimited then
  begin
     if LimitedAmt > 0 then
    qryTicket.SQL.Add(format(WHERE_ROUTE_LIMITED,[Empid,LimitedAmt])); //qm-903
    if LimitedAmt =0 then
    qryTicket.SQL.Add(format(WHERE_NOT_LIMIT,[Empid]));
  end
  else
    qryTicket.SQL.Add(format(WHERE_NOT_LIMIT,[Empid]));
end;

procedure TfrmStreetMapTickets.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ((ssAlt in Shift) and (Key = VK_F4)) then
     Key := 0;
end;

function TfrmStreetMapTickets.ReadINI: Boolean;
var
  IniFile: TIniFile;
begin
  try
    IniFile := TIniFile.Create(MyIniPath + 'QManager.ini');
    ShowDriveDistance:= IniFile.ReadBool('User', 'ShowDriveDistance', False);
//    ActEmpID := IniFile.ReadInteger('User', 'EmpID', 3512);
    homeAddress := IniFile.ReadString('User', 'HomeAddress', '2575 Westside Pkwy Ste 100, Alpharetta, GA 30004');
    DisplayName := IniFile.ReadString('User', 'DisplayName', '');
    SensorType := TSensorType(IniFile.ReadInteger('User', 'SensorType', 0));
    AppLocal := IniFile.ReadInteger('User', 'AppLocal', 0);
    logDir := IniFile.ReadString('LogFile', 'Path', 'E:\QM\Logs');
    chboxAutoPlot.Checked := IniFile.ReadBool('User', 'AutoPlot', false);
    CheckBoxDriveTimeDist.visible:= ShowDriveDistance;
    ForceDirectories(logDir);
  finally
    FreeAndNil(IniFile);
    cbLocationFeed.ItemIndex := integer(SensorType);
    LogResult.MethodName := 'ReadINI';
    LogResult.DataStream := 'homeAddress: ' + homeAddress;
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);

    StatusBar1.Panels[3].text := logDir;
  end;
end;

procedure TfrmStreetMapTickets.SaveToINI(Section, Ident, Value: string; IniDataType: TIniDataType);
var
  IniFile: TIniFile;
begin
  try
    IniFile := TIniFile.Create(MyIniPath + 'QManager.ini');
    // IniDataType =(idtString, idtInteger, idtBool, idtDateTime, idtDate,idtTime, idtFloat);
    case IniDataType of
      idtString:
        begin
          IniFile.writestring(Section, Ident, Value);
        end;
      idtInteger:
        begin
          IniFile.WriteInteger(Section, Ident, StrToInt(Value));
        end;
      idtBool:
        begin
          IniFile.writebool(Section, Ident, StrToBool(Value));
        end;
      idtDateTime:
        begin
          IniFile.writeDateTime(Section, Ident, StrToDateTime(Value));
        end;
      idtDate:
        begin
          IniFile.writeDate(Section, Ident, StrToDate(Value));
        end;
      idtTime:
        begin
          IniFile.writetime(Section, Ident, StrToTime(Value));
        end;
      idtFloat:
        begin
          IniFile.writeFloat(Section, Ident, StrToFloat(Value));
        end;

    end;
  finally
    FreeAndNil(IniFile);
    LogResult.MethodName := 'SaveToINI';
    LogResult.DataStream := 'Section: ' + Section + '  Ident: ' + Ident + '  value: ' + Value;
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  end;
end;

procedure TfrmStreetMapTickets.FormShow(Sender: TObject);
var
  LatLng,sLat,sLng :string;
begin
  try
    LogResult.MethodName := 'FormShow';
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
    self.Top := 0;
    self.Left := 0;

    if ((SensorType = stUserInput) and (homeAddress <> '')) then
    begin
      LatLng := GetGeoCode(homeAddress);
      sLat := copy(LatLng, 0, pos(',', LatLng) - 1);
      sLng := copy(LatLng, (pos(',', LatLng) + 1), maxChar);
      edtLat.text := sLat;
      edtLng.text := sLng;
      CurrentGPSLatitude := StrToFloat(sLat);
      CurrentGPSLongitude := StrToFloat(sLng);

      SetStartMarker(Sender, sLat, sLng, homeAddress);
    end;

    if not(qryTicket.Active) then
    begin
      qryTicket.DisableControls;
      qryTicket.Open;
      qryTicket.EnableControls;
    end;
    btnRefreshTicketsClick(self);
    AddEmployeeActivityEvent(maMapOpen, matSystem);
  except
    on E: Exception do
    begin
      LogResult.MethodName := 'FormShow';
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;


  if GlobalWebView2Loader.InitializationError then
    showmessage(GlobalWebView2Loader.ErrorMessage)
   else
    if GlobalWebView2Loader.Initialized then
      WebBrowser1.CreateBrowser(WVWindowParent1.Handle)
     else
      Timer1.Enabled := True;

end;

function TfrmStreetMapTickets.getOriginLL: string;
begin
  result := edtLat.text + ',' + edtLng.text;
end;

procedure TfrmStreetMapTickets.HandleCopyDataRecord(copyDataStruct: PcopyDataStruct);
var
  oldLat, oldLng: string;
  sLat, sLng: string;
  dMiles, dOldLat, doldLng, dNewLat, dNewLng: double;
begin

  oldLat := edtLat.text;
  oldLng := edtLng.text;
  dOldLat := StrToFloat(oldLat);
  dOldLng := StrToFloat(oldLng);

  edtLat.text := TGPSData(copyDataStruct.lpData^).Latitude;
  edtLng.text := TGPSData(copyDataStruct.lpData^).Longitude;

  CurrentGPSLatitude := StrToFloat(edtLat.text);
  CurrentGPSLongitude := StrToFloat(edtLng.text);

  sLat := edtLat.text;
  sLng := edtLng.text;

  dNewLat := StrToFloat(sLat);
  dNewLng := StrToFloat(sLng);

  dMiles := calculateDistance(dOldLat, doldLng, CurrentGPSLatitude, CurrentGPSLongitude) * 0.0006212;
  fDistAccumulator := fDistAccumulator + dMiles;
  if ((fDistAccumulator > 0.5) and (chboxAutoPlot.Checked)) then
  begin
    LogResult.MethodName := 'GPSData';
    LogResult.LogType := ltInfo;
    LogResult.Status := 'miles= ' + FloatToStr(fDistAccumulator);
    WriteLog(LogResult);
    btnRefreshTicketsClick(nil);
    sleep(1000);
    btnPlotSelectedClick(nil);
    fDistAccumulator := 0.00;
  end
  else
  begin
    if not(ckboxPosLocked.Checked) then
      SetStartMarker(nil, sLat, sLng, homeAddress);
  end;
end;



procedure TfrmStreetMapTickets.LocationSensor1LocationChanged(Sender: TObject; const OldLocation, NewLocation: TLocationCoord2D);
var
  sAddress: string;
  sLat, sLng: string;
  dMiles: double;
begin
  CurrentGPSLatitude := NewLocation.Latitude;
  CurrentGPSLongitude := NewLocation.Longitude;

  sLat := FloatToStr(CurrentGPSLatitude);
  sLng := FloatToStr(CurrentGPSLongitude);

  dMiles := calculateDistance(OldLocation.Latitude, OldLocation.Longitude, CurrentGPSLatitude, CurrentGPSLongitude) * 0.0006212;

  edtLat.text := sLat;
  edtLng.text := sLng;
  fDistAccumulator := fDistAccumulator + dMiles;
  if ((fDistAccumulator > 0.5) and (chboxAutoPlot.Checked)) then
  begin
    LogResult.MethodName := 'GPSData';
    LogResult.LogType := ltInfo;
    LogResult.Status := 'miles= ' + FloatToStr(fDistAccumulator);
    WriteLog(LogResult);
    btnRefreshTicketsClick(nil);
    btnPlotSelectedClick(nil);
    fDistAccumulator := 0.00;
  end
  else
  begin
    if not(ckboxPosLocked.Checked) then
      SetStartMarker(nil, sLat, sLng, homeAddress);
  end;
end;

procedure TfrmStreetMapTickets.OnMyWorkChanged(var Message: TMessage);
begin
  btnRefreshTicketsClick(nil);
  LogResult.MethodName := 'OnMyWorkChanged';
  LogResult.LogType := ltInfo;
  LogResult.Status := 'MyWork ticket(s) have changed';
  WriteLog(LogResult);
end;

procedure TfrmStreetMapTickets.SetStartMarker(Sender: TObject; sLat, sLng, sAddress: string);
var
  sMsg: String;
begin
  sMsg := '"Location: "';
  if sAddress = '' then
  begin
    sAddress := GetRevGeoCode(sLat + ',' + sLng);
    // EmpAct
  end;
  sAddress := '"' + sAddress + '"';
  WebBrowser1.ExecuteScript(Format('PutStartMarker(%s, %s, %s, %s)', [sLat, sLng, sMsg, sAddress]));
end;

procedure TfrmStreetMapTickets.SpeedButton1Click(Sender: TObject);
begin
  self.Constraints.MaxHeight:=758;      //qm-747
  self.Constraints.MaxWidth:=1014;
  self.Height:=758;
  self.Width:=1014;
end;

procedure TfrmStreetMapTickets.SpeedButton2Click(Sender: TObject);
begin
  self.Constraints.MaxHeight:=900;      //qm-747
  self.Constraints.MaxWidth:=1600;
  self.Height:=900;
  self.Width:=1600;
end;

procedure TfrmStreetMapTickets.GetLocation;
begin
  qryGPSlocation.ParamByName('empId').Value := ActEmpID;

  try
    qryGPSlocation.Open;
    if not qryGPSlocation.IsEmpty then
    begin
      edtLat.Text := FloatToStr(qryGPSlocation.FieldByName('latitude').AsFloat);
      edtLng.Text := FloatToStr(qryGPSlocation.FieldByName('longitude').AsFloat);
    end;
  finally
    qryGPSlocation.close;
  end;

end;

procedure TfrmStreetMapTickets.btnRefreshTicketsClick(Sender: TObject);
var
 I: Integer;
 fd: Integer;
begin
  GetLocation;
  if cbLocationFeed.ItemIndex < 0 then
  begin
    ShowMessage('You must have a feed or home address');
    FocusControl(cbLocationFeed);
    Exit;
  end;
  qryTicket.close;
  qryTicket.DisableControls;

  qryTicket.Open;
  qryTicket.EnableControls;
  {restore selected tickets}
  for I := 0 to DBGrid1DBTableView1.DataController.RecordCount-1 do
  begin
   with DBGrid1DBTableView1.DataController do
   begin
//     fd := plotList.IndexOf(Values[I,DBGrid1DBTableView1ticket_id.Index]);
//     if fd <> -1 then
       Values[I, DBGrid1DBTableView1Selected.Index] := True;
   end;
  end;
  cboColorSelect.ItemIndex:= 1;
end;

procedure TfrmStreetMapTickets.UseStartAddress(Sender: TObject);
var
  LatLng, sLat, sLng: String;
  sAddress: string;
begin
  chboxAutoPlot.Checked := false;
  sAddress := InputBox('Retrieve LatLng from Address', 'Enter Address', homeAddress);
  if sAddress <> '' then
  begin
    LatLng := GetGeoCode(sAddress);
    sLat := copy(LatLng, 0, pos(',', LatLng) - 1);
    sLng := copy(LatLng, (pos(',', LatLng) + 1), maxChar);
    edtLat.text := sLat;
    edtLng.text := sLng;
    CurrentGPSLatitude := StrToFloat(sLat);
    CurrentGPSLongitude := StrToFloat(sLng);

    SetStartMarker(Sender, sLat, sLng, sAddress);
    SaveToINI('User', 'HomeAddress', sAddress, idtString);
  end;
end;

function TfrmStreetMapTickets.parseLatLng(LatLng: string): string;
var
  s1, s2: string;
begin
  s1 := copy(LatLng, 0, pos(',', LatLng) - 1);
  s2 := copy(LatLng, 0, pos(',', LatLng) + 1);
end;

procedure TfrmStreetMapTickets.BtnClearDirectionsClick(Sender: TObject);
begin
  WebBrowser1.ExecuteScript('ClearDirections()')
end;

procedure TfrmStreetMapTickets.BtnClearMarkersClick(Sender: TObject);
begin
  WebBrowser1.ExecuteScript('ClearMarkers()');
end;

procedure TfrmStreetMapTickets.CheckBoxDriveTimeDistClick(Sender: TObject);
var
  TimeDist: Boolean;
begin
  TimeDist := CheckBoxDriveTimeDist.Checked;
  if TimeDist then
  begin
    if (MessageDlg
      ('Calculating the Drive Time and Distance takes approx. 2 seconds per ticket during Refresh Tickets.  Do you wish to continue?',
      mtWarning, [mbOK, mbAbort], 0) = mrOK) then
    begin
      if cbLocationFeed.ItemIndex < 0 then
      begin
        ShowMessage('You must have a GPS feed or home address');
        FocusControl(cbLocationFeed);
        Exit;
      end;
      btnRefreshTicketsClick(Sender);
    end
    else
      CheckBoxDriveTimeDist.Checked := false;
  end;
  chboxAutoPlot.Checked := not(TimeDist);
  DBGrid1DBTableView1DriveDist.visible := TimeDist;
  DBGrid1DBTableView1DriveTime.visible := TimeDist;
end;

procedure TfrmStreetMapTickets.CheckBoxStreeViewClick(Sender: TObject);
begin
  if CheckBoxStreeView.Checked then
    WebBrowser1.ExecuteScript('StreetViewOn()')
  else
    WebBrowser1.ExecuteScript('StreetViewOff()');
end;

procedure TfrmStreetMapTickets.CheckBoxTrafficClick(Sender: TObject);
begin
  if CheckBoxTraffic.Checked then
    WebBrowser1.ExecuteScript('TrafficOn()')
  else
    WebBrowser1.ExecuteScript('TrafficOff()');
end;

procedure TfrmStreetMapTickets.btnPlotSelectedClick(Sender: TObject);
var
  I, AFilteredIndex, Cnt: Integer;
  varRouteOrder, varSelected, varCorrLat, varCorrLng, varTicketNum, varAddress, varKind, varHtmlTktColor, varPriority: Variant;
  sAct: string;
begin
  try
    try
      IBePlotting := True;
      Cnt := 0;
      btnPlotSelected.Enabled := false;
      ProgressBar1.visible := true;
      WebBrowser1.ExecuteScript('ClearMarkers()');
      for I := 0 to DBGrid1DBTableView1.DataController.FilteredRecordCount - 1 do
      begin
        AFilteredIndex := DBGrid1DBTableView1.DataController.FilteredRecordIndex[I];
        varSelected := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1Selected.Index];
        if (varSelected <> null) and (varSelected) then
        begin    
          varRouteOrder:= DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1Route_Order.Index];
          varCorrLat := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1work_lat.Index];
          varCorrLng := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1work_long.Index];
          varTicketNum := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1ticket_number.Index];
          varAddress := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1WorkAddress.Index];
          varKind := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1kind.Index];
          varHtmlTktColor := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1WebColor.Index];
          varPriority := DBGrid1DBTableView1.DataController.Values[AFilteredIndex, DBGrid1DBTableView1Priority.Index];   //qm-721 sr
          if varPriority = 'SKIPPED' then varRouteOrder:=varPriority;  //qm-721 sr
          
          WebBrowser1.ExecuteScript(Format('PlaceTickets("%s",%s,%s,"%s","%s","%s","%s")', [varRouteOrder, varCorrLat, varCorrLng,
            varTicketNum, varAddress, varKind, varHtmlTktColor]));
          SetStartMarker(Sender, edtLat.text, edtLng.text, homeAddress);
          inc(Cnt);
          LogResult.MethodName := 'PlotSelected';
          LogResult.LogType := ltInfo;
          LogResult.DataStream := 'Route Order: ' + varToStr(varRouteOrder) + ' TicketNum: ' + varToStr(varTicketNum) + ' CorrLat:' +
            varToStr(varCorrLat) + ' CorrLng:' + varToStr(varCorrLng);
          WriteLog(LogResult);
          ProgressBar1.StepIt;
          sAct := IntToStr(Cnt) + ' selected tickets plotted';
          StatusBar1.Panels[2].text := sAct;
          StatusBar1.refresh;
        end;
      end;

    ProgressBar1.visible := false;

    btnPlotSelected.Enabled := True;
      if (Sender is TControl) then
        AddEmployeeActivityEvent(maMapPlot, matSystem)
      else
        AddEmployeeActivityEvent(maMapPlot, matUser);

    IBePlotting := false;
    except
      on E: Exception do
      begin
        LogResult.MethodName := 'Plot Selected';
        LogResult.LogType := ltError;
        LogResult.ExcepMsg := E.Message;
        WriteLog(LogResult);
      end;
    end;
  finally
    ProgressBar1.visible := false;
    btnPlotSelected.Enabled := true;
  end;
end;

procedure TfrmStreetMapTickets.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;

  if GlobalWebView2Loader.Initialized then
    WebBrowser1.CreateBrowser(WVWindowParent1.Handle)
   else
    Timer1.Enabled := True;
end;

procedure TfrmStreetMapTickets.ToggleSelections;
var
  I, Cnt: integer;
  bSelected: Boolean;
  AValue: variant;
  crSave: TCursor;
  ticketNbr, gridLat, gridLng: string;
  latlngOrigin, latlngDest, sAct: string;
  Distance, Duration: Shortstring;
  sTicketID: String;
begin
  try
    crSave := Screen.Cursor;
    try
      Screen.Cursor := crHourGlass;
      bSelected := True;
      if DBGrid1DBTableView1.Controller.SelectedRecordCount > 0 then
      begin
        { Check whether all selected records are checked or not }
        for I := 0 to DBGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
          if DBGrid1DBTableView1.Controller.SelectedRecords[I] is TcxGridDataRow then
          begin
            AValue := DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1Selected.Index];
            bSelected := bSelected and (AValue <> null) and (AValue = True);
          end;
        { Update checked status of all selected records }
        cboColorSelect.ItemIndex := 0;
        for I := 0 to DBGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
        begin
          if DBGrid1DBTableView1.Controller.SelectedRecords[I] is TcxGridDataRow then
          begin
            DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1Selected.Index] := not bSelected;
            {save Selected and remove unselected tickets}
             sTicketID :=  InttoStr(DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1ticket_id.Index]);
            if not BSelected then
            begin
              if PlotList.IndexOf(sTicketID) = -1 then
                PlotList.Add(sTicketID);
            end else
            begin
              if PlotList.IndexOf(sTicketID) > -1 then
                PlotList.Delete(PlotList.IndexOf(sTicketID));
            end;
          end;

          if DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1Selected.Index] then
          begin
            ticketNbr := DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1ticket_number.Index];
            latlngOrigin := edtLat.text + ',' + edtLng.text;
            gridLat := DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1work_lat.Index];
            gridLng := DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1work_long.Index];

            latlngDest := gridLat + ',' + gridLng;

            if CheckBoxDriveTimeDist.Checked then
            begin
              GetDistanceTime(latlngOrigin, latlngDest, Distance, Duration);
              DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1DriveDist.Index] := Distance;
              DBGrid1DBTableView1.Controller.SelectedRecords[I].Values[DBGrid1DBTableView1DriveTime.Index] := Duration;
            end;
          end;

        end;
      end;
    finally
        Cnt:= DBGrid1DBTableView1.Controller.SelectedRowCount;
        sAct := IntToStr(Cnt) + ' tickets selected';
        StatusBar1.Panels[2].text := sAct;
        StatusBar1.refresh;
        Screen.Cursor := crSave;
    end;
  except
    on E: Exception do
    begin
      LogResult.MethodName := 'Toggle Selections';
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

Procedure TfrmStreetMapTickets.ProcessLocationForWazeQR(LatLng, ticketNbr, Address: string);
var
  sAct: string;
begin
  frmWaze := TfrmWaze.CreateQR(self, stLatLng, LatLng, ticketNbr, Address);
  frmWaze.ShowModal;
  sAct := 'TkNo:' + ticketNbr;
  frmWaze.Free;
  AddEmployeeActivityEvent(maWazeLookup, matUser);
end;

procedure TfrmStreetMapTickets.qryTicketCalcFields(DataSet: TDataSet);
{ Fires on each row that is selected in the grid. }
const
  MILES = 0;
var
  latlngOrigin, latlngDest: Shortstring;
  CrowFlightDist: extended;
  TicketStatus: xqD10TicketColor.TTicketStatus;
  Color: Integer;
  iLat, iLng, dLat, dLng, iInitBearing, dFinalBearning: double;
  DueDate: TDatetime;
  sKind, sAddress, strNbr, strAddr, cross, city, state: string;
  Alert2: string;
  function ColorToHtml(const Color: integer): AnsiString;
  const
    Hex = '0123456789ABCDEF';
  var
    b, s: Byte;
  begin
    s := 0;
    repeat
      b := (Color shr s) and $FF;
      result := result + Hex[b div 16 + 1] + Hex[b mod 16 + 1];
      inc(s, 8);
    until s = 24;
  end;

begin
  try
    self.Cursor := crHourGlass;
    iLat:=0.00; iLng:=0.00; dLat:=0.00; dLng:=0.00; iInitBearing:=0.00;
    Alert2 := '';
    sAddress := '';
    strNbr := '';
    strAddr := '';
    cross := '';
    city := '';
    state := '';
    sKind := '';
    iLat := StrToFloat(edtLat.text);
    iLng := StrToFloat(edtLng.text);
    if ((iLat = 34) and (iLng = -84)) then exit;

    dLat := DataSet.FieldByName('work_lat').AsFloat; // assign to corr_lat
    dLng := DataSet.FieldByName('work_long').AsFloat; // assign to corr_lng
    sKind := DataSet.FieldByName('Kind').AsString;
    DataSet.FieldByName('Direction').AsFloat := -100;
    if DataSet.FieldByName('alert').AsString = 'A' then
      Alert2 := '!';
    DataSet.FieldByName('alert2').AsString := Alert2;
    TicketStatus := xqTicketColor.GetTicketStatusFromRecord(DataSet);
    DataSet.FieldByName('ticket_status').AsInteger := word(TicketStatus);
    DataSet.FieldByName('locates').AsString := xqTicketColor.GetLocatesStringForTicket(TicketLocates,
      DataSet.FieldByName('ticket_id').AsInteger);

    DataSet.FieldByName('corr_lat').AsFloat := dLat; // copy to calc fields
    DataSet.FieldByName('corr_lng').AsFloat := dLng; // copy to calc fields
    iInitBearing := -100;

    DueDate := DataSet.FieldByName('Due_date').AsDateTime;

    Color :=  xqTicketColor.GetTicketColor(TicketStatus, false, clLime);
    DataSet.FieldByName('Color').AsInteger := Color;
    DataSet.FieldByName('webColor').AsString := ColorToHtml(Color);

    if not(DataSet.FieldByName('work_address_number').IsNull) then
      strNbr := qryTicket.FieldByName('work_address_number').AsString
      else strNbr := '0';

    if not(DataSet.FieldByName('work_address_street').IsNull) then
      strAddr := qryTicket.FieldByName('work_address_street').AsString;

    if not(DataSet.FieldByName('work_cross').IsNull) then
      cross := DataSet.FieldByName('work_cross').AsString;

    city := DataSet.FieldByName('work_city').AsString;
    state := DataSet.FieldByName('work_state').AsString;

    if strNbr <> '0'  then
    begin
      sAddress := strNbr + ' ' + strAddr + ', ' + city + ', ' + state;
      latlngDest := GetGeoCode(sAddress); // geocode address then
    end;

    if (strNbr = '0') or (pos('ERROR', latlngDest) > 0) then  //if  no strnbr or address invalid
    begin
      if not((dLat = 0.00) or (dLng = 0.00)) then
      begin
        GetHeading(iLat, iLng, dLat, dLng, iInitBearing, dFinalBearning, MILES);
        DataSet.FieldByName('Direction').AsFloat := iInitBearing;
        CrowFlightDist := calculateDistance(iLat, iLng, dLat, dLng);
        DataSet.FieldByName('GreatCircleDist').AsString := formatfloat('0.##', CrowFlightDist * 0.0006212) + ' Mi';
      end else
      begin
        if ((strNbr = '0') and (cross <> '')) then // use cross id exists and no strNo
          sAddress := strAddr + ' & ' + cross + ', ' + city + ', ' + state
        else
        begin // cross not available
          if (strNbr = '0') then // then do not use it strNbr
            sAddress := strAddr + ', ' + city + ', ' + state;
        end;
        latlngDest := GetGeoCode(sAddress); // geocode address then
      end;
    end;

    if (sAddress <> '') and (pos('ERROR', latlngDest) = 0) then // if address not invalid
    begin
      dLat := StrToFloat(copy(latlngDest, 0, (pos(',', latlngDest) - 1))); // if geocode successful, use it
      dLng := StrToFloat(copy(latlngDest, pos(',', latlngDest) + 1, maxChar));

      GetHeading(iLat, iLng, dLat, dLng, iInitBearing, dFinalBearning, MILES);

      DataSet.FieldByName('corr_lat').AsFloat := dLat;
      DataSet.FieldByName('corr_lng').AsFloat := dLng;
    end;

    if iInitBearing <> -100 then
    begin
      CrowFlightDist := calculateDistance(iLat, iLng, dLat, dLng);
      DataSet.FieldByName('GreatCircleDist').AsString := formatfloat('0.##', CrowFlightDist * 0.0006212) + ' Mi';
      DataSet.FieldByName('Direction').AsFloat := iInitBearing;
    end
    else
      DataSet.FieldByName('GreatCircleDist').AsString := 'Unk';

    if CheckBoxDriveTimeDist.Checked then
     AddEmployeeActivityEvent(maDriveTimeDist, matUser);
    self.Cursor := crDefault;

  except
    on E: Exception do
    begin
      LogResult.MethodName := 'Toggle Selections';
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmStreetMapTickets.WebBrowser1AfterCreated(Sender: TObject);
begin
  WVWindowParent1.UpdateSize;
  Caption := 'Street map for Tickets for Edge';
  CreateMap(Sender);
end;

procedure TfrmStreetMapTickets.WebBrowser1InitializationError(Sender: TObject; aErrorCode: HRESULT; const aErrorMessage: wvstring);
begin
  showmessage(aErrorMessage);
end;

procedure TfrmStreetMapTickets.WebBrowser1WebMessageReceived(Sender: TObject; const aWebView: ICoreWebView2;
  const aArgs: ICoreWebView2WebMessageReceivedEventArgs);
var
  TempArgs: TCoreWebView2WebMessageReceivedEventArgs;
begin
  TempArgs := TCoreWebView2WebMessageReceivedEventArgs.Create(aArgs);

  try
    RunMethod(Sender, TempArgs.WebMessageAsString);
  finally
    TempArgs.Free;
  end;

end;

procedure TfrmStreetMapTickets.RunMethod(Sender: TObject; MethodName: string);
var
  methodList: TStrings;
  Latitude: WideString;
  Longitude: WideString;
  Address: WideString;
  ticketNbr: WideString;
  functionName: WideString;
  LatLng: string;
begin
  try
    methodList := TStringList.Create;
    methodList.Clear;
    methodList.Delimiter := ',';
    methodList.StrictDelimiter := true;
    methodList.DelimitedText := MethodName;
    if methodList[0] = 'MarkerDetails' then
    begin
      Latitude := methodList[1];
      Longitude := methodList[2];
      Address := methodList[3];
      ticketNbr := methodList[4];
      LatLng := Latitude + ',' + Longitude;
      With qryTicket do
      begin
        if Active then
          Locate('ticket_number', ticketNbr, []);
      end;

      CoInitialize(nil);
      TThread.CreateAnonymousThread(  //this is required to prevent a GDI error from the modal form sr
        procedure
        begin
          ProcessLocationForWazeQR(LatLng, ticketNbr, Address);
          CoUnInitialize;
        end).Start;
    end;
  finally
    methodList.Free;
  end;
end;

procedure TfrmStreetMapTickets.WMCopyMyData(var Msg: TWMCopyData);
begin
  If SensorType = TSensorType.stGpsGate then
    HandleCopyDataRecord(Msg.copyDataStruct);
end;

procedure TfrmStreetMapTickets.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: String;
  Leader: String;
  EntryType: String;
const
  STREET_TICKET_MAPPER = 'TicketStreetMap_%s';
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  try
    Leader := FormatDateTime(PRE_PEND, Now);
    LogName := logDir + '\' + Format(STREET_TICKET_MAPPER, [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT;

    case LogResult.LogType of
      ltError:
        begin
          EntryType := '**************** ERROR ****************';
        end;
      ltInfo:
        begin
          EntryType := '****************  INFO  ****************';
        end;
      ltNotice:
        begin
          EntryType := '**************** NOTICE ****************';
        end;
      ltWarning:
        begin
          EntryType := '**************** WARNING ****************';
        end;
    end;

    try
      if FileExists(LogName) then
      begin
        AssignFile(myFile, LogName);
        Append(myFile);
      end
      else
      begin
        AssignFile(myFile, LogName);
        Rewrite(myFile);
      end;

      WriteLn(myFile, EntryType);
      if LogResult.MethodName <> '' then
      begin
        WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
      end;

      if LogResult.ExcepMsg <> '' then
      begin
        WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
      end;

      if LogResult.Status <> '' then
      begin
        WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
      end;

      if LogResult.DataStream <> '' then
      begin
        WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
      end;
    except
      on E: EInOutError do
      begin
        logDir := 'C:\Program Files (x86)\UtiliQuest\Q Manager';
        WriteLog(LogResult);
      end;
    end;
  finally
    CloseFile(myFile);
    clearLogRecord;
  end;
end;

procedure TfrmStreetMapTickets.ckboxPosLockedClick(Sender: TObject);
begin
  chboxAutoPlot.Checked := not ckboxPosLocked.Checked;
end;

procedure TfrmStreetMapTickets.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmStreetMapTickets.AddEmployeeActivityEvent(const MapActivity: TmaMapActivity; matMapActType: TmatMapActType);
var
  MyWorkHandle: HWnd;
begin
  MyWorkHandle := FindWindow('TMainForm', nil);
  PostMessage(MyWorkHandle, UM_SET_EMP_ACT_MAP, ord(MapActivity), ord(matMapActType));
end;

function TfrmStreetMapTickets.connectDB: Boolean;
const
  MY_DATA = 'data';
  BASE_DATA_PATH = '\Q Manager\QMServer\Data';
var
  myDataPath: string;
begin
  try
    try
      result := false;
      dbIsamStreetMap.connected := false;
      if AppLocal = 1 then
        myDataPath := MyAppPath + BASE_DATA_PATH // data cache
      else
        myDataPath := MyAppPath + MY_DATA; // local data

      dbIsamStreetMap.Directory := myDataPath;
      dbIsamStreetMap.connected := true;
      result := dbIsamStreetMap.connected;
    except
      on e: exception do
        showmessage('Failed to connect to DB: ' + e.Message);
    end;

  finally
    if (result = true) then
      StatusBar1.Panels[0].Text := 'connected to:' + myDataPath
    else
      StatusBar1.Panels[0].Text := 'Not connected';
  end;
end;

function TfrmStreetMapTickets.EnDeCrypt(const Value: String): String;
var
  CharIndex: integer;
begin
  result := Value;
  for CharIndex := 1 to Length(Value) do
    result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function deg2rad(deg: extended): extended;
{ Degrees to Radians }
begin
  result := deg * PI / 180.0;
end;

{ *********** Rad2Deg ********** }
function rad2deg(rad: extended): extended;
{ radians to degrees }
begin
  result := rad / PI * 180.0;
end;

function TfrmStreetMapTickets.calculateDistance(dlat1, dLon1, dlat2, dlon2: extended): extended;
var
  lat1, lat2, lon1, lon2: extended;
  F: extended;
  a, b, TanU1, U1, tanU2, U2, Usq: extended;
  L, Lambda, Oldlambda: extended;
  count: integer;
  sigma, sinSigma, cosSigma, TanSigma: extended;
  cos2sigmam: extended;
  sinAlpha, cosalpha, cosSqAlpha, C: extended;
  AA, BB, L1, L2: extended;
  dsigma: extended;
begin
  lat1 := deg2rad(dlat1);
  lat2 := deg2rad(dlat2);
  lon1 := deg2rad(dLon1);
  lon2 := deg2rad(dlon2);
  a := 6378137.0;
  b := 6356752.314;
  F := (a - b) / a;
  TanU1 := (1 - F) * tan(lat1);
  U1 := arctan(TanU1);
  tanU2 := (1 - F) * tan(lat2);
  U2 := arctan(tanU2);
  L := lon2 - lon1;
  Lambda := L; { first approximation of distance }
  count := 0;
  repeat { iterate }
    inc(count);
    sinSigma := sqrt(sqr(Cos(U2) * Sin(Lambda)) + sqr(Cos(U1) * Sin(U2) - Cos(U2) * Sin(U1) * Cos(Lambda)));
    cosSigma := Sin(U1) * Sin(U2) + Cos(U1) * Cos(U2) * Cos(Lambda);
    sigma := arctan(sinSigma / cosSigma);
    sinAlpha := Cos(U1) * Cos(U2) * Sin(Lambda) / sinSigma;
    cosSqAlpha := 1 - sqr(sinAlpha);
    if (lat1 = 0) and (lat2 = 0) then
      cos2sigmam := 0
    else
      cos2sigmam := cosSigma - (2 * Sin(U1) * Sin(U2) / cosSqAlpha);
    C := (F / 16) * cosSqAlpha * (4 + F * (4 - 3 * cosSqAlpha));
    Oldlambda := Lambda;
    Lambda := L + (1 - C) * F * sinAlpha * (sigma + C * sinSigma * (cos2sigmam + C * cosSigma * (2 * sqr(cos2sigmam) - 1)));
  until (abs(Lambda - Oldlambda) < 10E-12) or (count > 1000);

  Usq := cosSqAlpha * (sqr(a) - sqr(b)) / sqr(b);

  AA := 1.0 + (Usq / 16384.0) * (4096.0 + Usq * (-768.0 + Usq * (320.0 - 175.0 * Usq)));

  BB := (Usq / 1024.0) * (256.0 + Usq * (-128.0 + Usq * (74.0 - 47.0 * Usq)));

  dsigma := BB * sinSigma * (cos2sigmam + (BB / 4.0) * (-1 + cosSigma * (2 * sqr(cos2sigmam)) - (BB / 6) * cos2sigmam *
    (-3 + 4 * sqr(sinSigma)) * (-3 + 4 * sqr(cos2sigmam))));

  result := b * AA * (sigma - dsigma);
  { Convert meters back to user preferred distance units }

end;

procedure TfrmStreetMapTickets.cbLocationFeedChange(Sender: TObject);
begin // TSensorType=(stGpsGate, stLocSensor, stUserInput);
  case cbLocationFeed.ItemIndex of
    0:
      begin
        SensorType := TSensorType.stGpsGate;
        ckboxPosLocked.Checked := false;
      end;
    1:
      begin
        SensorType := TSensorType.stLocSensor;
        ckboxPosLocked.Checked := false;
      end;
    2:
      begin
        SensorType := TSensorType.stUserInput;
        UseStartAddress(Sender);
        ckboxPosLocked.Checked := True;
      end;

  end;
  if cbLocationFeed.ItemIndex > -1 then
    btnRefreshTicketsClick(Sender);

  SaveToINI('User', 'SensorType', IntToStr(integer(SensorType)), idtInteger);
end;

procedure TfrmStreetMapTickets.cboColorSelectCloseUp(Sender: TObject);
var
  I: integer;
  bSelected: Boolean;
  ticketNbr, gridLat, gridLng: string;
  latlngOrigin, latlngDest: string;
  Distance, Duration: Shortstring;
  AFilteredIndex: Integer;
  AStatus: Boolean;

  procedure SetDriveDistTime(Index: Integer);
  begin
    latlngOrigin := edtLat.text + ',' + edtLng.text;
    gridLat := DBGrid1DBTableView1.DataController.Values[Index,DBGrid1DBTableView1work_lat.Index];
    gridLng := DBGrid1DBTableView1.DataController.Values[Index,DBGrid1DBTableView1work_long.Index];

    latlngDest := gridLat + ',' + gridLng;

    GetDistanceTime(latlngOrigin, latlngDest, Distance, Duration);
    DBGrid1DBTableView1.DataController.Values[Index,DBGrid1DBTableView1DriveDist.Index] := Distance;
    DBGrid1DBTableView1.DataController.Values[Index,DBGrid1DBTableView1DriveTime.Index] := Duration;
  end;

begin
 try
  If cboColorSelect.ItemIndex = -1 then Exit;

  if cbLocationFeed.ItemIndex < 0 then
  begin
    ShowMessage('You must have a GPS feed or home address');
    FocusControl(cbLocationFeed);
    Exit;
  end;
  self.Cursor := crHourGlass;
  with DBGrid1DBTableView1.DataController do
  begin
    PlotList.Clear;
    for I := 0 to FilteredRecordCount-1 do
    begin
       AFilteredIndex := DBGrid1DBTableView1.DataController.FilteredRecordIndex[I];
      case cboColorSelect.ItemIndex of
        0: Values[AFilteredIndex, DBGrid1DBTableView1Selected.Index] := False;
        1:
        begin
          Values[AFilteredIndex, DBGrid1DBTableView1Selected.Index] := True;
          Plotlist.Add(InttoStr(DBGrid1DBTableView1.DataController.Values[AFilteredIndex,
            DBGrid1DBTableView1ticket_id.Index]));
          if CheckBoxDriveTimeDist.Checked then
            SetDriveDistTime(AFilteredIndex);
        end
        else
        begin
          AStatus := (Values[AFilteredIndex, DBGrid1DBTableView1Color.Index] =
             Integer(cboColorSelect.Items.Objects[cboColorSelect.ItemIndex]));
          Values[AFilteredIndex, DBGrid1DBTableView1Selected.Index] := AStatus;
          if AStatus then
            Plotlist.Add(InttoStr(DBGrid1DBTableView1.DataController.Values[AFilteredIndex,
              DBGrid1DBTableView1ticket_id.Index]));
          if CheckBoxDriveTimeDist.Checked then
            SetDriveDistTime(AFilteredIndex);
        end;
      end;
    end;
  end;
  self.Cursor := crDefault;
  except
    on E: Exception do
    begin
      LogResult.MethodName := 'cboColorSelectCloseUp';
      LogResult.LogType := ltError;
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmStreetMapTickets.cboColorSelectDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with Control as TComboBox, Canvas do
  begin
    Pen.Color := clBlack;
    Brush.Color := $FCFCFC; //Dacia bkgrd
    FillRect(Rect);
    TextOut(Rect.Left+ 2, Rect.Top + 2, Items[Index]);
    Brush.Color := Integer(Items.Objects[Index]);
    Rectangle(Rect.Left + 77, Rect.Top + 2, Rect.Right-2, Rect.Bottom -1);
  end;
end;

procedure TfrmStreetMapTickets.chboxAutoPlotClick(Sender: TObject);
begin
  SaveToINI('User', 'AutoPlot', BoolToStr(chboxAutoPlot.Checked), idtBool);
end;

// LocalAppDataFolder := GetKnownFolderPath(FOLDERID_LocalAppData);
function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
var
  Path: LPWSTR;
begin
  if SUCCEEDED(SHGetKnownFolderPath(folder, 0, 0, Path)) then
  begin
    try
      result := Path;
    finally
      CoTaskMemFree(Path);
    end;
  end
  else
    result := '';
end;

{ TComboBox }

procedure TComboBox.CNDrawItem(var Message: TWMDrawItem);
begin
  with Message do
    DrawItemStruct.itemState := DrawItemStruct.itemState and ODS_CHECKED;
  inherited;
end;

function TfrmStreetMapTickets.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

initialization
  GlobalWebView2Loader                := TWVLoader.Create(nil);
  GlobalWebView2Loader.UserDataFolder := ExtractFileDir(Application.ExeName) + '\CustomCache';
  GlobalWebView2Loader.StartWebView2;

end.
