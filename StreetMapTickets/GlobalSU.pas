unit GlobalSU;

interface
 function IsAppRunning: Boolean;
 procedure RestoreAppWindow;

var
 // Program environmental variables
 zAppTitle      : PChar;

implementation

uses
 Windows, SysUtils, Forms;

function IsAppRunning: Boolean;
var
 rtn : Cardinal;
begin
  // Check for application
  // A return code of 183 is ERROR_ALREADY_EXISTS
  CreateMutex(nil, False, zAppTitle);
  rtn := GetLastError;
  if rtn = ERROR_ALREADY_EXISTS then
   result := True
  else
   result := False;
end;

Procedure RestoreAppWindow;
var
 fMain, fChild : Cardinal;
begin
  fMain := 0;
  fChild  := 0;
 fMain := findwindow('TApplication',zAppTitle); // Get Main Window
 if (fMain <> 0) then begin
  if isiconic(fMain) then                       // Restore Iconic
   showwindow(fMain, SW_SHOWDEFAULT);
  if setforegroundwindow(fMain) then            // Activate Main
   fChild := GetLastActivePopup(fMain);
  if (fMain <> fChild) then                     // Show last child window, if any
   BringWindowToTop(fChild);
 end;
end;


initialization

begin
 // Set program environmental variables
 zAppTitle    := 'StreetMapProj';
end;

finalization

end.
