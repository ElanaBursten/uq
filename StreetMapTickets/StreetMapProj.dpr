program StreetMapProj;
{$R *.res}
{$R 'QMVersion.res' '..\QMVersion.rc'}
uses
  Forms,
  SysUtils,
  Windows,
  streetMapTickets in 'streetMapTickets.pas' {frmStreetMapTickets},
  MyWorkWaze in 'MyWorkWaze.pas',
  WazeQRCode in 'WazeQRCode.pas',
  GlobalSU in 'GlobalSU.pas',
  QMConst in '..\client\QMConst.pas',
  xqD10TicketColor in '..\common\xqD10TicketColor.pas';

begin

  if IsAppRunning then
  begin
    RestoreAppWindow;
    exit;
  end;
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Initialize;
    Application.MainFormOnTaskBar := true;
    Application.ShowMainForm:=true;
    Application.CreateForm(TfrmStreetMapTickets, frmStreetMapTickets);
  //    SetWindowLong(Application.Handle, GWL_EXSTYLE,
//    (GetWindowLong(Application.Handle, GWL_EXSTYLE) OR WS_EX_TOOLWINDOW) AND NOT WS_EX_APPWINDOW);
    Application.Run;
end.
