unit MyWorkWaze;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, WazeQRCode, ExtCtrls, StdCtrls;

type TsearchType=(stQTip, stStreet, stLatLng);  //QMANTWO-515
type TentityType=(etTicket, etWorkOrder, etDamage);  //QMANTWO-515
type
  TfrmWaze = class(TForm)
    wazePaintBox: TPaintBox;
    Panel1: TPanel;
    Memo1: TMemo;
    Timer1: TTimer;
    procedure wazePaintBoxPaint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure wazePaintBoxClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    iEmpID: integer;
    iTktID: integer;
    sActDB:string;
    firstTask:string;
    refcode :string;
    QRCodeBitmap: TBitmap;
//    procedure QTipCodeUpdate(searchType: TsearchType; lat,lng: real);
    procedure QrCodeUpdate(searchType: TsearchType; LocateStr, ticketNbr, sAddress: String);
    { Private declarations }
  public
    { Public declarations }
    constructor CreateQR(AOwner: TComponent; searchType: TsearchType; LocateStr, ticketNbr, sAddress: String);
    constructor CreateQTIP(AOwner: TComponent; searchType: TsearchType; lat,lng: real;TktID, EmpID:integer;ActDB:string;bfirstTask:boolean; entityType:TentityType);
  end;

implementation
uses StrUtils;
{$R *.dfm}

constructor TfrmWaze.CreateQR(AOwner: TComponent; searchType: TsearchType; LocateStr, ticketNbr, sAddress: String);
begin
  inherited Create(AOwner);
  QRCodeBitmap := TBitmap.Create;
  QrCodeUpdate(searchType, LocateStr, ticketNbr, sAddress);
end;

constructor TfrmWaze.CreateQTIP(AOwner: TComponent; searchType: TsearchType; lat,lng: real;TktID,
                                EmpID:integer;ActDB:string;bfirstTask:boolean; entityType:TentityType);
begin
  inherited Create(AOwner);
  QRCodeBitmap := TBitmap.Create;
  iEmpID := EmpID;
  iTktID := TktID;
  sActDB := ActDB;
  case entityType of
    etTicket:  refcode   := 'TKQSCAN';
    etWorkOrder: refcode := 'WOQSCAN';
    etDamage: refcode    := 'DAMQSCAN';
  end;

  if bfirstTask then
  firstTask := 'ftask' //QMANTWO-660
  else
  firstTask := '';   //QMANTWO-660
//  QTipCodeUpdate(searchType, lat,lng);
end;

procedure TfrmWaze.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(QRCodeBitmap);
  Action := caFree;
end;

procedure TfrmWaze.FormShow(Sender: TObject);
begin
  Timer1.Enabled := true;
end;

//procedure TfrmWaze.QTipCodeUpdate(searchType:TsearchType; lat,lng: real);
//var
//  QRCode: TWazeQRCode;
//  Row, Column: Integer;
//  formattedStr:string;
//  hash:string;
//
//const
//  QR_STRING = 'appID=99,lat=%s,long=%s,empid=%s,foreignid=%s,refcode=%s,db=%s,ed=%s';
//begin
//  QRCode := TWazeQRCode.Create;
//  try
//    case searchType of
//    stStreet:
//    begin
////      self.caption := 'Waze By Address';
////      memo1.Lines.add(LocateStr);
////      LocateStr := ReplaceStr(LocateStr, ' ', '%20');
////      QRCode.Data := Format('http://waze.com/ul?q=%s&navigate=yes', [LocateStr]);
//    end;
//    stQTip:
//    begin
//      self.caption := 'Location By Coordinates';
//
//      formattedStr:= Format(QR_STRING,[FloatToStr(lat),FloatToStr(lng),IntToStr(iEmpID), IntToStr(iTktID), refcode, sActDB, firstTask]);
//      memo1.text:=formattedStr;
//      hash:=  CalcHash(formattedStr);
//      QrCode.Data := formattedStr+',hash='+hash;
//    end;
//    end;
//
//    QRCode.Encoding := qrAuto;//TQRCodeEncoding(cmbEncoding.ItemIndex);
//    QRCode.QuietZone := 4;//StrToIntDef(edtQuietZone.Text, 4);
//    QRCodeBitmap.SetSize(QRCode.Rows, QRCode.Columns);
//    for Row := 0 to QRCode.Rows - 1 do
//    begin
//      for Column := 0 to QRCode.Columns - 1 do
//      begin
//        if (QRCode.IsBlack[Row, Column]) then
//        begin
//          QRCodeBitmap.Canvas.Pixels[Column, Row] := clBlack;
//        end else
//        begin
//          QRCodeBitmap.Canvas.Pixels[Column, Row] := clWhite;
//        end;
//      end;
//    end;
//  finally
//    QRCode.Free;
//  end;
//  wazePaintBox.Repaint;
//end;

procedure TfrmWaze.QrCodeUpdate(searchType:TsearchType; LocateStr, ticketNbr, sAddress: String);
var
  QRCode: TWazeQRCode;
  Row, Column: Integer;
begin
  QRCode := TWazeQRCode.Create;
  try
    case searchType of
    stStreet:
    begin
      self.caption := 'Waze By Address';
      memo1.text:=LocateStr;
      memo1.lines.Add(ticketNbr);
      LocateStr := ReplaceStr(LocateStr, ' ', '%20');
      QRCode.Data := Format('http://waze.com/ul?q=%s&navigate=yes', [LocateStr]);
    end;
    stLatLng:
    begin
      self.caption := 'Waze By Coordinates';
      memo1.Lines.add('Coordinates:  '+LocateStr);
      memo1.lines.Add('Ticket number: '+ticketNbr);
      memo1.lines.Add('Address: '+sAddress);

      QrCode.Data := Format('http://waze.to/?ll=%s&navigate=yes', [LocateStr]);
    end;
    end;
    QRCode.Encoding := qrAuto;//TQRCodeEncoding(cmbEncoding.ItemIndex);
    QRCode.QuietZone := 4;//StrToIntDef(edtQuietZone.Text, 4);
    QRCodeBitmap.SetSize(QRCode.Rows, QRCode.Columns);
    for Row := 0 to QRCode.Rows - 1 do
    begin
      for Column := 0 to QRCode.Columns - 1 do
      begin
        if (QRCode.IsBlack[Row, Column]) then
        begin
          QRCodeBitmap.Canvas.Pixels[Column, Row] := clBlack;
        end else
        begin
          QRCodeBitmap.Canvas.Pixels[Column, Row] := clWhite;
        end;
      end;
    end;
  finally
    QRCode.Free;
  end;
  wazePaintBox.Repaint;
end;

procedure TfrmWaze.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := false;
  self.Close;
end;

procedure TfrmWaze.wazePaintBoxClick(Sender: TObject);
begin
  self.close;
end;

procedure TfrmWaze.wazePaintBoxPaint(Sender: TObject);
var
  Scale: Double;
begin
  with wazePaintBox do
  begin
    Canvas.Brush.Color := clWhite;
    Canvas.FillRect(Rect(0, 0, Width, Height));
    if ((QRCodeBitmap.Width > 0) and (QRCodeBitmap.Height > 0)) then
    begin
      if (Width < Height) then
        Scale := Width / QRCodeBitmap.Width
      else
        Scale := Height / QRCodeBitmap.Height;

      Canvas.StretchDraw(Rect(0, 0, Trunc(Scale * QRCodeBitmap.Width), Trunc(Scale * QRCodeBitmap.Height)), QRCodeBitmap);
    end;
  end; //with wazePaintBox do
end;

end.
