object frmPlatMgr: TfrmPlatMgr
  Left = 0
  Top = 0
  Caption = 'Plat Manager'
  ClientHeight = 469
  ClientWidth = 885
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    885
    469)
  PixelsPerInch = 96
  TextHeight = 13
  object lblStatus: TLabel
    Left = 25
    Top = 19
    Width = 31
    Height = 13
    Caption = 'Status'
  end
  object lblCompressionLevel: TLabel
    Left = 25
    Top = 403
    Width = 89
    Height = 13
    Caption = 'Compression Ratio'
  end
  object btnEnergizePM7: TButton
    Left = 460
    Top = 408
    Width = 97
    Height = 41
    Caption = 'Energize (PM7)'
    TabOrder = 0
    OnClick = btnEnergizePM7Click
  end
  object btnUploadPM7: TButton
    Left = 585
    Top = 408
    Width = 97
    Height = 41
    Caption = 'Upload (PM7)'
    TabOrder = 1
    OnClick = btnUploadPM7Click
  end
  object ListView: TListView
    AlignWithMargins = True
    Left = 25
    Top = 38
    Width = 840
    Height = 347
    ParentCustomHint = False
    Anchors = [akLeft, akTop, akRight]
    Checkboxes = True
    Columns = <
      item
        Caption = 'Method'
        Width = 93
      end
      item
        Caption = 'Division'
        Width = 93
      end
      item
        Caption = 'Company'
        Width = 93
      end
      item
        Caption = 'Center'
        Width = 93
      end
      item
        Caption = 'Date'
        Width = 93
      end
      item
        Caption = 'Age'
        Width = 93
      end
      item
        Caption = 'Local Path'
        Width = 93
      end
      item
        AutoSize = True
        Caption = 'PM7 File          '
      end>
    GridLines = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnCustomDrawItem = ListViewCustomDrawItem
    OnDblClick = ListViewDblClick
    OnItemChecked = ListViewItemChecked
  end
  object btnSearchPM7: TButton
    Left = 329
    Top = 408
    Width = 97
    Height = 41
    Caption = 'Search / Refresh (PM7)'
    TabOrder = 3
    WordWrap = True
    OnClick = btnSearchPM7Click
  end
  object cbCompressionLevel: TComboBox
    Left = 25
    Top = 422
    Width = 168
    Height = 21
    TabOrder = 4
    Items.Strings = (
      'Level 0 - COPY'
      'Level 1 - Fastest Compression'
      'Level 3 - Fast Compression'
      'Level 5 - Normal Compression'
      'Level 7 - Maximum Compression'
      'Level 9 - Ultra Compression')
  end
end
