object frmLogin: TfrmLogin
  Left = 0
  Top = 0
  Caption = 'frmLogin'
  ClientHeight = 94
  ClientWidth = 230
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblPinCode: TLabel
    Left = 16
    Top = 24
    Width = 71
    Height = 13
    Caption = 'Enter Pin Code'
  end
  object edtLogin: TEdit
    Left = 103
    Top = 21
    Width = 98
    Height = 21
    TabOrder = 0
  end
  object btnLogin: TButton
    Left = 103
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Login'
    TabOrder = 1
  end
end
