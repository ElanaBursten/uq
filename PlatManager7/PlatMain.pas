
unit PlatMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, ExtActns, DateUtils, IOUtils,
  IniFiles, IdIcmpClient;

type
  TfrmPlatMgr = class(TForm)
    btnEnergizePM7: TButton;
    btnUploadPM7: TButton;
    ListView: TListView;
    btnSearchPM7: TButton;
    lblStatus: TLabel;
    lblCompressionLevel: TLabel;
    cbCompressionLevel: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure btnSearchPM7Click(Sender: TObject);
    procedure ListViewCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure btnEnergizePM7Click(Sender: TObject);
    procedure ListViewDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnUploadPM7Click(Sender: TObject);
    procedure ListViewItemChecked(Sender: TObject; Item: TListItem);
  private
    { Private declarations }
    OutputPathpm7: String;
    hintsFileLocal: String;
    //hintsFileRemote: String;
    ServerIP: String;
    ServerUploadPath: String;
    VolumeLabel: String;
    pmDaysAgo: String;
    procedure getRemoveableDrives(VolumeLabel: String);
    function GetVolumeLabel(DriveChar: Char): string;
    function DriveSpace(DriveLetter: String; var FreeSpace, UsedSpace,
      TotalSpace: int64): Boolean;
    procedure DownloadFile(URL, Dest: string);
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings);
    function ExecuteProcess(const FileName, Params: string; Folder: string;
      WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
      var ErrorCode: integer): Boolean;
    function Start7zipProcess(pm7File, LocalDirectory: String): Integer;
    function StartUpload(SourceFile: String): Boolean;
    function PingServer(IPAddr: String): Boolean;
  public
    { Public declarations }
    UploadList: TStringList;
  end;
  ServerException = class(Exception);

var
  frmPlatMgr: TfrmPlatMgr;

 implementation

{$R *.dfm}

procedure TfrmPlatMgr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UploadList.Free;
end;

procedure TfrmPlatMgr.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
begin
  iniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  try
    hintsFileLocal := IniFile.ReadString('Plats','HintsFile','') ;
    if hintsFileLocal = '' then
      hintsFileLocal :=  ExtractFileDir(Application.ExeName) + '\hints.txt';
    ServerIP := IniFile.ReadString('Plats','ServerIP','') ;
    if ServerIP = '' then
      ServerIP := '10.18.60.194';
    ServerUploadPath :=  IniFile.ReadString('Plats','ServerUploadPath','');
    If ServerUploadPath = '' then
      ServerUploadPath :=  '\\10.18.60.197\Plats7';
    VolumeLabel :=  IniFile.ReadString('Plats','VolumeLabel','');
    If VolumeLabel = '' then
      VolumeLabel := 'STORAGE';
  finally
    IniFile.Free;
  end;

  btnUploadPM7.Enabled := False;
  btnEnergizePM7.Enabled := False;
  OutputPathpm7  := 'C:\Plats7';
  getRemoveableDrives('STORAGE');
  //default compression level of 0, COPY
  CbCompressionLevel.ItemIndex := 0;
  Uploadlist := TStringList.Create;

  //hints url no longer used: 'https://qm-dev.utiliquest.com/webupdate/hints.txt'
    {lblStatus.Caption := 'Attempting to download updated hints...';
     DownloadFile(HintsFileRemote,HintsFileLocal); }
end;

procedure TfrmPlatMgr.getRemoveableDrives(VolumeLabel: String);
var
  DriveMap,
  dMask : DWORD;
  dRoot : String;
  I     : Integer;
  VLabel: String;
  FreeSpace,
  UsedSpace,
  TotalSpace : int64;
const
  BytesPerGB = 1073741824;
begin
  FreeSpace  := 0;
  TotalSpace := 0;
  UsedSpace := 0;
  dRoot     := 'A:\';
  DriveMap  := GetLogicalDrives;
  dMask     := 1;

  for I := 0 to 32 do
  begin
    if (dMask and DriveMap) <> 0 then
      if GetDriveType(PChar(dRoot)) in [DRIVE_FIXED, DRIVE_REMOVABLE] then
      begin
        VLabel := GetVolumeLabel(dRoot[1]);
        if UpperCase(VLabel) = UpperCase(VolumeLabel) then
        begin
          if DriveSpace(dRoot, FreeSpace, UsedSpace, TotalSpace) then
          begin
            FreeSpace  := Round(FreeSpace / BytesPerGB);
            UsedSpace  := Round(UsedSpace / BytesPerGB);
            TotalSpace := Round(TotalSpace / BytesPerGB);
          end;

          OutputPathpm7 :=  dRoot;
          lblStatus.Caption :=
            'Output path acquired: ' + OutputPathpm7 + '   Disk Label: ' + VolumeLabel + '  ' +
             InttoStr(TotalSpace) + 'GB  ' + InttoStr(FreeSpace) + 'GB Free';
        end;
      end;
    dMask := dMask shl 1;
    Inc(dRoot[1]);
  end;
end;

procedure TfrmPlatMgr.DownloadFile(URL: string; Dest: string);
var
  dl: TDownloadURL;
begin
  dl := TDownloadURL.Create(self);
  try
    dl.URL := URL;
    dl.FileName := Dest;
    dl.ExecuteTarget(nil); //this downloads the file
    dl.Free;
    lblStatus.Caption := 'Hints file aquired from the internet, local cache copy updated.'
  except
     lblStatus.Caption := 'Hints file failed to update, reverting to local cached copy';;
     dl.Free;
   end;
end;

procedure TfrmPlatMgr.Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True;
   ListOfStrings.DelimitedText   := Str;
end;

procedure TfrmPlatMgr.btnSearchPM7Click(Sender: TObject);
var
  HintsFile: String;
  HintsF: TextFile;
  Dir: String;
  pmLines, pmParts: TStringList;
  pmCompany: String;
  pmDivision: String;
  pmCenter: String;
  pmYear, pmMonth, pmDay: String;
  pmDate: TDateTime;
  pmLocalPath: String;
  pmFile: String;
  pmLastLine: String;
  pm7zfileName: String;
  Itm: Tlistitem;
  S, S2: string;
  parts: string;
  I: Integer;
begin
  btnSearchPM7.Enabled := False;
  ListView.Clear;
  Hintsfile := ExtractFileDir(Application.ExeName) + '\hints.txt';
  if not FileExists(HintsFile) then
  begin
    lblStatus.Caption := 'Missing Hints....';
    btnSearchPM7.Enabled := True;
    If MessageDlg('Hints are missing. Search common locations, C:\Plats, C:\UPV?',
        mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      try
        try
          AssignFile(HintsF, HintsFile);
          ReWrite(HintsF);
          WriteLn(HintsF, 'C:\plats');
          WriteLn(HintsF, 'C:\UPV');
        except
          On E:Exception do
            lblStatus.Caption := 'Warning! ' +  E.Message ;
        end;
      finally
        CloseFile(HintsF);
      end;
    end
    else
    begin
      btnSearchPM7.Enabled := True;
      Exit;
    end;
  end;

  LblStatus.Caption := 'Hints: ' + HintsFile;
  try
    pmLines := TStringList.Create;
    pmParts := TStringList.Create;
    AssignFile(HintsF, HintsFile);
    Reset(HintsF);
    //read update.pm files from local system or deploy.pm files from normally C:\tools\deploy
    while not Eof(HintsF) do
    begin
      ReadLn(HintsF, Dir);
      if DirectoryExists(Dir) then
      begin
        try
          for S2 in TDirectory.GetDirectories(Dir, '*', TSearchOption.soTopDirectoryOnly) do
          begin
            for S in TDirectory.GetFiles(S2, '*.pm', TSearchOption.soTopDirectoryOnly) do
            begin
              pmLocalPath  := ExtractFileDir(S);
              pmFile := ExtractFileName(S);
              LblStatus.Caption := format('Looking in %s for pm files', [pmLocalPath]);
              pmLines.LoadFromFile(S);
              If pmLines.Count >= 2 then
              begin
                pmLastLine := pmLines[pmLines.Count-1];
                If trim(pmLastLine) = '' then
                begin
                  ShowMessage(Format('I did not find a date for %s',[S]));
                  Continue;
                end;
                pmCompany := pmLines[0];
                Split('.', pmLastLine, pmParts);
                pmDivision := Copy(pmParts[0], 1, 3);
                pmCenter := Copy(pmParts[0], 4, 3);
                pmYear := pmParts[1];
                pmMonth :=  pmParts[2];
                pmDay := pmParts[3];
                pmDate := EncodeDate(StrtoInt(pmYear), strtoInt(pmMonth), strtoInt(pmDay));
                pmDaysAgo := InttoStr(DaysBetween(Now, pmDate));
                Split('\', S2, pmParts);
                parts := '';
                for I := 0 to pmParts.Count-1 do
                  if pmParts[I] <> 'C:' then
                    parts := parts + '.' + pmParts[I];
                pm7zfileName := pmLastLine + parts +'.' + pmCompany + '.pm7';

                Itm := ListView.Items.Add;
                Itm.Caption := pmFile;
                Itm.SubItems.Add(pmDivision);
                Itm.SubItems.Add(pmCompany);
                Itm.SubItems.Add(pmCenter);
                Itm.SubItems.Add(DateTimetoStr(pmDate));
                //ID the rows you (Field Engineer) just updated.
                Itm.SubItems.Add(pmDaysAgo + ' days');
                itm.Data := Pointer(StrtoInt(pmDaysAgo));
                If StrtoInt(pmDaysAgo) < 1 then
                 Itm.Checked := True;
                Itm.SubItems.Add(pmLocalPath);
                Itm.SubItems.Add(pm7zfileName);
              end;
            end;
          end;
        except
          On E:Exception do
          begin
            LblStatus.Caption := 'Warning! ' + E.Message;
            MessageDlg(E.Message  + 'Problem with Looking for PM files..', mtWarning, [mbOK], 0, mbOK);
            Raise;
          end;
        end;
      end else
      begin
        lblStatus.Caption := 'Something went wrong...';
      end;
    end;
  finally
    CloseFile(HintsF);
    btnSearchPM7.Enabled := True;
    if ListView.Items.Count = 0 then
    begin
      MessageDlg('No files found', mtWarning, [mbOK], 0, mbOK);
      BtnEnergizePM7.Enabled := False;
    end else
      BtnEnergizePM7.Enabled := True;
  end;
  LblStatus.Caption := Format('Search Complete, %d items found. Output Ready on %s', [ListView.Items.Count, outPutPathpm7]);
end;


function TfrmPlatMgr.DriveSpace(DriveLetter : String; var FreeSpace, UsedSpace, TotalSpace : int64) : Boolean;
begin
  Result := GetDiskFreeSpaceEx(Pchar(DriveLetter), UsedSpace, TotalSpace, @FreeSpace);
  if UsedSpace > 0 then
    UsedSpace := TotalSpace - FreeSpace;

  if not Result then
  begin
    UsedSpace   := 0;
    TotalSpace  := 0;
    FreeSpace   := 0;
  end;
end;

function TfrmPlatMgr.GetVolumeLabel(DriveChar: Char): string;
var
  NotUsed:     DWORD;
  VolumeFlags: DWORD;
  VolumeInfo:  array[0..MAX_PATH] of Char;
  VolumeSerialNumber: DWORD;
  Buf: array [0..MAX_PATH] of Char;
begin
    GetVolumeInformation(PChar(DriveChar + ':\'),
    Buf, SizeOf(VolumeInfo), @VolumeSerialNumber, NotUsed,
    VolumeFlags, nil, 0);

    SetString(Result, Buf, StrLen(Buf));   { Set return result }
    Result:=AnsiUpperCase(Result);
end;

function TfrmPlatMgr.Start7zipProcess(pm7File: String; LocalDirectory: String):Integer;
var
  CompressionLevel: String;
  OutputPathFile: String;
begin
   LblStatus.Caption := Format('Processing %s...', [pm7File]);
   Result := 0;
    {-mx0 : No compression (copy mode), 0
     -mx1	: Very low (fastest mode), 1
     -mx3	: Fast compression mode, 2
     -mx5	: normal compression, 3
     -mx7	: maximum compression, 4
     -mx9	: ultra compression, 5 }

   Case cbCompressionLevel.ItemIndex of
    0: CompressionLevel := '-mx=0';
    1: CompressionLevel := '-mx=1';
    2: CompressionLevel := '-mx=3';
    3: CompressionLevel := '-mx=5';
    4: CompressionLevel := '-mx=7';
    5: CompressionLevel := '-mx=9';
    else
      CompressionLevel := '-mx=3';
   end;

   OutputPathFile := outPutPathPM7 + '\' + pm7File;
   try
     DeleteFile(OutputPathFile);
   except
     On E:Exception do
     begin
       LblStatus.Caption := 'Warning! ' + E.Message;
       ShowMessage('Warning! ' + E.Message);
     end;
   end;

   try
     If ExecuteProcess('C:\Program Files\7-Zip\7z.exe',
       ' a ' + '"' + OutputPathFile + '"' + ' -pStar_Trek -mhe ' + CompressionLevel,   //'archive C:\plats7\nameofpm7file
       LocalDirectory, False, False, True, Result) then
         Result := 0;
   except
     On E: Exception do
     begin
       LblStatus.Caption := 'Warning! ' + E.Message;
       MessageDlg('Problem with Starting Process: ' + E.Message,  mtWarning, [mbOK], 0, mbOK);
     end;
   end;

  Case Result Of
   0:
     UploadList.Add(OutputPathFile);  //Add to upload list, only 0's are allowed
   1:
     MessageDlg(OutputPathFile + #10#13 + 'Exit code 1: Warning (Non fatal error(s)). For example, one or more ' +
     ' files were locked by some other application, so they were not compressed. ',  mtWarning, [mbOK], 0, mbOK);
   2:
     MessageDlg(OutputPathFile + #10#13 + 'Exit code 2: Fatal Error',  mtWarning, [mbOK], 0, mbOK);
   7:
     MessageDlg(OutputPathFile + #10#13 + 'Exit code 7: Command line error',  mtWarning, [mbOK], 0, mbOK);
   8:
     MessageDlg(OutputPathFile + #10#13 + 'Exit code 8: Not enough memory for operation',  mtWarning, [mbOK], 0, mbOK);
   255:
     MessageDlg(OutputPathFile + #10#13 + 'Exit code 255: User stopped the process',  mtWarning, [mbOK], 0, mbOK)
   else
     MessageDlg(OutputPathFile + #10#13 + 'Exit code is unknown! Abort.',  mtWarning, [mbOK], 0, mbOK);
  end;
end;

procedure TfrmPlatMgr.btnEnergizePM7Click(Sender: TObject);
var
  I: Integer;
  ArchiveErrors: Boolean;
  StatusMsg: String;
  CheckedItem: Boolean;
begin
  btnEnergizePM7.Enabled := False;
  ArchiveErrors := False;
  UploadList.Clear;

  //The pm7 file to create
  CheckedItem := False;
  for I := 0 to ListView.Items.Count-1 do
  begin
    if ListView.Items[I].Checked then
    begin
      CheckedItem := True;
      If Start7zipProcess(ListView.Items[I].SubItems[6],ListView.Items[I].SubItems[5]) > 0 then
         ArchiveErrors := True;
    end;
  end;

  if not CheckedItem then
  begin
    MessageDlg('No items checked..default pm days ago < 1)', mtInformation, [mbOK], 0, mbOK);
    btnEnergizePM7.Enabled := True;
    Exit;
  end;

  btnEnergizePM7.Enabled := True;
  If ArchiveErrors then StatusMsg :=  'Archive error(s). Some files not included in upload list.  See ' + OutPutPathPM7
    else StatusMsg := 'Archiving completed. See ' + OutPutPathPM7;
  lblStatus.Caption := StatusMsg;
  MessageDlg(StatusMsg, mtInformation, [mbOK], 0, mbOK);
  btnUploadPM7.Enabled := True;
end;

function TfrmPlatMgr.ExecuteProcess(const FileName, Params: string; Folder: string;
   WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := True;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, DETACHED_PROCESS, nil,
    WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;

  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
    repeat
      Application.ProcessMessages;
    until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

procedure TfrmPlatMgr.ListViewCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Item.Checked then
  listview.Canvas.Brush.Color := clWebAzure;
end;

procedure TfrmPlatMgr.ListViewDblClick(Sender: TObject);
var
  I: Integer;
  AErrorCode: Integer;
  pmPath: String;
begin
  if ListView.ItemIndex = -1 then Exit;
  pmPath := ListView.Items[ListView.ItemIndex].SubItems[5] + '\' +ListView.Items[ListView.ItemIndex].Caption;
  try
    If not(FileExists(pmPath)) then
      raise Exception.Create(format('Error opening: %s', [pmPath]));
    LblStatus.Caption := pmPath;

    If not ExecuteProcess('notepad.exe', pmPath,  '', False, False, False, AErrorCode) then
      raise Exception.Create(format('Error opening: %s', [pmPath]));
  except
    LblStatus.Caption := Format('Error opening %s.', [pmPath]);
    raise;
  end;
end;

procedure TfrmPlatMgr.ListViewItemChecked(Sender: TObject; Item: TListItem);
begin
  btnUploadPM7.Enabled := False;
end;

procedure TfrmPlatMgr.btnUploadPM7Click(Sender: TObject);
var
  I:Integer;
  AFile: String;
begin
  if UpLoadList.Count = 0 then
  begin
    MessageDlg('Upload list is empty..no items checked', mtInformation, [mbOK], 0, mbOK);
    btnUploadPM7.Enabled := True;
    Exit;
  end;

  btnUploadPM7.Enabled := False;
  lblStatus.Caption := 'Checking Server..';
  Application.ProcessMessages;
  if not PingServer(ServerIP) or not DirectoryExists(ServerUploadPath) then
  begin
    MessageDlg('Problem connecting to ' + ServerUpLoadPath, mtWarning, [mbOK], 0, mbOK);
    LblStatus.Caption := 'Problem connecting to ' + ServerUpLoadPath;
    btnUploadPM7.Enabled := True;
    Exit;
  end;

  for I := 0 to UploadList.Count-1 do
  begin
    LblStatus.Caption := 'Uploading ' + ServerUploadPath + '\' + AFile;
    Application.ProcessMessages;
    AFile := ExtractFileName(UpLoadList[I]);
    If not StartUpload(AFile) then
      MessageDlg('Error uploading ' + AFile, mtWarning, [mbOK], 0, mbOK);
  end;
  lblStatus.Caption := 'Done';
  ShowMessage('Done');
  btnUploadPM7.Enabled := True;
end;

function TfrmPlatMgr.PingServer(IPAddr: String): Boolean;
var
  I: TIdIcmpClient;
  Rec: Integer;
begin
  Result:= False;
  I:= TIdIcmpClient.Create(nil);
  try
    I.Host:= IPAddr;
    I.Ping();
    Sleep(1000);
    Rec:= I.ReplyStatus.BytesReceived;
    if Rec > 0 then Result:= True else Result:= False;
  finally
    I.Free;
  end;
end;

function TfrmPlatMgr.StartUpload(SourceFile: String): Boolean;
var
 AErrorCode: Integer;
begin
  try
    Result := True;
    If not ExecuteProcess('robocopy.exe',
    format('%s %s /s /v /z /w:30 /r:3 /LOG:robocopylog.txt "%s"', [outPutPathPM7, ServerUploadPath, SourceFile]),
    '', False, False, True, AErrorCode) then
      raise Exception.Create('Problem uploading ' + SourceFile);;
     Result := (AErrorCode <= 7) or (AErrorCode = 9);
  except
    On E: Exception do
    begin
      LblStatus.Caption := 'Warning! ' + E.Message;
      MessageDlg(E.Message, mtWarning, [mbOK], 0, mbOK);
      Result := False;
    end;
  end;
end;

end.


