program InitialSyncer;

{$APPTYPE CONSOLE}

// For this one, I was too lazy to add all the paths, so I set the search path
// for the project to:
// ..\common;..\thirdparty\zlib;..\thirdparty\zlib\zlib-1.1.4;..\server

{$R '..\QMIcon.res'}
{$R '..\QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  Classes,
  QMServerLibrary_Intf in '..\server\QMServerLibrary_Intf.pas',
  uROClient,
  uROProxy,
  uROBINMessage,
  uROEncryption,
  Windows,
  OdHttpROChannel in '..\common\OdHttpROChannel.pas',
  OdHttpDialog in '..\common\OdHttpDialog.pas' {OdNetProgressDialog},
  OdWinInet in '..\common\OdWinInet.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdNetUtils in '..\common\OdNetUtils.pas',
  Terminology in '..\common\Terminology.pas';

var
  Url: string;
  BinMsg: TROBINMessage;
  QMS: QMService;
  Chan: TOdHttpROChannel;
  StartTime: Integer;
  EndTime: Integer;
  Response: string;
  Interval: Integer;
  CI: ComputerInformation;
  User: string;
  Password: string;
  UpdatesSince: string;
  ClientVersion: string;
  AI: NVPairList;

begin

  WriteLn('QM Service Initial Sync Tester');

  if ParamCount <> 5 then begin
    WriteLn('Usage: <programname> <URL> <QM User Login Id> <QM User Password> <Updates Since Date> <wait interval in ms or 0 to run once>');
    WriteLn('Example: InitialSyncer http://qmcluster.utiliquest.com/Test2 QMUser QMPassword 2008-05-31T07:00:00.000 10000');
    // InitialSyncer http://192.168.1.180/Test2 talvarez 12345678 2008-05-31T07:00:00.000 10000
    Exit;
  end;

  Url := ParamStr(1);
  User := ParamStr(2);
  Password := ParamStr(3);
  UpdatesSince := ParamStr(4);
  Interval := StrToIntDef(ParamStr(5), -1);
  ClientVersion := '2.1.0.8915';

  if Interval > 0 then
    WriteLn('Syncing once every ' + IntToStr(Interval) + ' milliseconds. Press control-C to stop.')
  else
    WriteLn('Syncing one time.');

  Chan := TOdHttpROChannel.Create(nil);
  Chan.Url := Url + 'app/QMLogic.dll/BIN';
  Chan.ShowProgress := False;
  Chan.Encryption.EncryptionMethod := tetRijndael;
  Chan.Encryption.EncryptionRecvKey := 'B2E3056082D3D01A158BE07937FC9452';
  Chan.Encryption.EncryptionSendKey := '7A2B4FFB71F59204A247382D98BE0C08';

  BinMsg := TROBINMessage.Create;

  QMS := CoQMService.Create(BinMsg, Chan);

  CI := ComputerInformation.Create;
  AI := NVPairList.Create;

  WriteLn('Testing as user login ' + User);
  while True do begin
    Write(DateTimeToStr(Now) + '  Sending Initial Sync... ');
    try
      StartTime := GetTickCount;
      Response := QMS.SyncDown6(User + '/' + Password + '/' + ClientVersion, UpdatesSince, 'REQ123', '2.1.0.8000', Now,
         0, '', CI, 0, AI, '', '');
      EndTime := GetTickCount;
      WriteLn( ' Initial Sync took ' + IntToStr(EndTime-StartTime) + ' ms   Size = ' + IntToStr(Length(Response)));
    except
      on E: Exception do begin
        WriteLn(' ERROR: ' + E.Message);
        WriteLn('   Raw: ' + Copy(Chan.MostRecentRawResponse, 1, 60));
      end;
    end;
    if Interval < 1 then
      Break;

    Sleep(Interval);
  end;
end.


