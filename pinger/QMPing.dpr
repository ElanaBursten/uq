program QMPing;

{$APPTYPE CONSOLE}

// Relative Directories: ..\common;..\thirdparty\zlib;..\server;..\thirdparty\RemObjects

{$R '..\QMIcon.res'}
{$R '..\QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  Classes,
  QMServerLibrary_Intf in '..\server\QMServerLibrary_Intf.pas',
  uROClient,
  uROProxy,
  uROBINMessage,
  uROEncryption,
  Windows,
  OdHttpROChannel;

var
  Url: string;
  BinMsg: TROBINMessage;
  QMS: QMService;
  Chan: TOdHttpROChannel;
  StartTime: Integer;
  EndTime: Integer;
  Response: string;
  Interval: Integer;
  Username, Password, AppVersion: string;
  Attempts, Success: integer;
begin
  WriteLn('QM Service Pinger');

  if ParamCount<>5 then begin
    WriteLn('Usage: QMPing <URL> <username> <password> <AppVersion> <interval in ms>');
    WriteLn('Example: QMPing http://qmcluster.utiliquest.com/Test2 Someone Theirpassword 1000');
    Exit;
  end;
  WriteLn('Press control-C to stop.');
  Url := ParamStr(1);
  Username := ParamStr(2);
  Password := ParamStr(3);
  AppVersion := ParamStr(4);
  Interval := StrToIntDef(ParamStr(5), 1000);

  Chan := TOdHttpROChannel.Create(nil);
  Chan.Url := Url + 'app/QMLogic.dll/BIN';
  Chan.ShowProgress := False;
  Chan.Encryption.EncryptionMethod := tetRijndael;
  Chan.Encryption.EncryptionRecvKey := 'B2E3056082D3D01A158BE07937FC9452';
  Chan.Encryption.EncryptionSendKey := '7A2B4FFB71F59204A247382D98BE0C08';

  BinMsg := TROBINMessage.Create;
  QMS := CoQMService.Create(BinMsg, Chan);
  Attempts := 0;
  Success := 0;

  while True do begin
    Write(DateTimeToStr(Now) + '... ');
    Inc(Attempts);
    try
      StartTime := GetTickCount;
      Response := QMS.Ping(Username + '/' + Password + '/' + AppVersion, '0');
      EndTime := GetTickCount;
      Write( ' ' + IntToStr(EndTime-StartTime) + ' ms  ' + Response);
      Inc(Success);
    except
      on E: Exception do begin
        WriteLn(' ERROR: ' + E.Message);
        WriteLn('   Raw: ' + Copy(Chan.MostRecentRawResponse, 1, 60));
      end;
    end;

    WriteLn(Format('  %d attempts, %6.1f %% success', [Attempts, 100.0 * Success / Attempts]));

    Sleep(Interval);
  end;
end.


