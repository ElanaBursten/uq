# htmltools.py

import string
import re

re_strip_html = re.compile("<[^>]*>", re.MULTILINE)

entitydefs = [
    ("amp", "&"),
    ("lt", "<"),
    ("gt", ">"),
]

def quote(s):
    for name, token in entitydefs:
        if token in s:
            s = string.replace(s, token, "&"+name+";")
    return s

def unquote(s):
    for name, token in entitydefs:
        if string.find(s, "&"+name+";") > -1:
            s = string.replace(s, "&"+name+";", token)
    return s

def indent(s, indent=4):
    lines = string.split(s, "\n")
    INDENT = " " * indent
    lines = [INDENT + line for line in lines]
    return string.join(lines, "\n")

def dedent(s, dedent=4):
    def _dedent(line, dedent):
        for i in range(dedent):
            if line.startswith(" "):
                line = line[1:]
        return line
    lines = string.split(s, "\n")
    lines = [_dedent(line) for line in lines]
    return string.join(lines, "\n")

def wrap(s, front="", end=""):
    """ "Wrap" text around a string, e.g. <b> and </b> around some text. """
    return front + s + end

###
### RSS stuff

def strip_html(html):
    """ Remove all HTML tags from a string. """
    html = re.sub(re_strip_html, "", html)
    #html = unquote(html)
    # do NOT unquote... RSS won't like characters like '>'
    return html

def html_to_words(html):
    html = strip_html(html)
    words = string.split(html)
    return words

def first_sentence(html):
    words = html_to_words(html)
    sentence = []
    for word in words:
        sentence.append(word)
        if word[-1:] in ("!", ".", "?"):
            break
    s = string.join(sentence, " ") + " ... [%d words]" % (len(words),)
    return s

def top(html, min_sentences=1, min_words=20):
    """ Get the first text from HTML.  Make sure we have at least
        <min_sentences> sentences and <min_words> words. """
    words = html_to_words(html)
    top = []
    sentences = 0
    for word in words:
        top.append(word)
        #if word[-1:] in ("!", ".", "?"):
        if word.endswith("!") or word.endswith(".") or word.endswith("?") \
        or word.endswith("!)") or word.endswith(".)") or word.endswith("?)"):
            sentences = sentences + 1
            if sentences >= min_sentences and len(top) >= min_words:
                break

    s = string.join(top, " ") + " ... [%d words]" % (len(words),)
    return s

if __name__ == "__main__":

    s = quote("if x > y & 2: do_this(x)")
    print s
    print unquote(s)
