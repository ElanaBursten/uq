# pybucket.py

import email
import getopt
import os
import poplib
import string
import sys
#
import rsswriter

__usage__ = """\
pybucket.py [options] host user password

Options:
    -x      Delete retrieved emails.
    -d num  Download at most <num> emails.
    -n num  Generate an RSS file of <num> entries. (default is 10)
    -o name Set repository file name. (default is output.txt)
    -s name Set RSS file name (default is rss.xml).
    -m num  Set the maximum number of mails for the repository. (default is 100)

Examples:

     pybucket.py -x host user password
     Get all mails; delete from server; repository is output.txt, RSS file is
     rss.xml.

     pybucket.py -o mails.dat -r mails.xml -m 1000 host user password
     Get all mails; don't delete from server; repository is mails.dat and can
     hold 1000 emails; RSS file is mails.xml
"""

class Options:
    delete_after_download = 0
    max_download = -1
    max_repository = 100
    rss_number = 10
    repository_name = "output.txt"
    rss_name = "rss.xml"

SEPARATOR = "~~~~~"

class PyBucket:

    def __init__(self, host, user, password, options):
        self.host = host        # POP3 host server name
        self.user = user        # login
        self.password = password
        self.options = options

    def get_mail(self):
        if self.options.max_download == 0:
            return

        print "Connecting to", self.host, "..."
        p = poplib.POP3(self.host)
        p.user(self.user)
        p.pass_(self.password)
        print p.getwelcome()

        try:
            num_messages = len(p.list()[1])
            print "There are", num_messages, "messages available on the server."

            f = open(self.options.repository_name, "a+r")

            if options.max_download > -1:
                num_messages = min(num_messages, options.max_download)
            for i in range(num_messages):
                print "Retrieving %d of %d..." % (i+1, num_messages),
                lines = p.retr(i+1)[1]
                for line in lines:
                    print >> f, line
                print len(lines), "lines"
                print >> f, SEPARATOR

            # delete messages
            if self.options.delete_after_download and num_messages:
                print "Deleting messages from server...",
                for i in range(num_messages):
                    p.dele(i+1)
                print "OK"

            f.close()

        finally:
            p.quit()

    def cleanup_repository(self, messages):
        count = len(messages)   # number of messages in repository

        print "Repository contains", count, "messages. ",
        if count > self.options.max_repository:
            print "Cleaning up...",
            messages = messages[-self.options.max_repository:]
            g = open("__foo__", "w")
            for blurb in messages:
                g.writelines(blurb)
            g.close()
            os.remove(self.options.repository_name)
            os.rename("__foo__", self.options.repository_name)
            print "OK"
        else:
            print "No cleanup necessary."

    def generate_rss(self):
        # get last N messages from output file
        N = self.options.rss_number
        messages = []
        current = []
        f = open(self.options.repository_name, 'r')
        for line in f.xreadlines():
            current.append(line)
            if line.strip() == SEPARATOR:
                messages.append(current)
                current = []
        f.close()

        # since we already traversed the repository, this is a good time for
        # cleanup
        self.cleanup_repository(messages)

        messages = messages[-N:]

        messages = [string.join(blurb, "") for blurb in messages]
        messages = [email.message_from_string(s) for s in messages]
        messages.reverse()  # newest first in the RSS
        rssw = rsswriter.RSSWriter(messages)
        data = rssw.generate_rss()

        f = open(self.options.rss_name, 'w')
        f.write(data)
        f.close()
        print self.options.rss_name, "written with", len(messages), "messages."

    def run(self):
        self.get_mail()
        self.generate_rss()


if __name__ == "__main__":

    options = Options()

    opts, args = getopt.getopt(sys.argv[1:], "d:m:n:o:r:x")
    if len(args) != 3:
        print >> sys.stderr, __usage__
        sys.exit(1)
    host, user, password = args[:3]

    for o, a in opts:
        if o =="-d":
            options.max_download = int(a)
        elif o == "-x":
            options.delete_after_download = 1
        elif o == "-n":
            options.rss_number = int(a)
        elif o == "-o":
            options.repository_name = a
        elif o == "-r":
            options.rss_name = a
        elif o == "-m":
            options.max_repository = int(a)

    bucket = PyBucket(host, user, password, options)
    bucket.run()

