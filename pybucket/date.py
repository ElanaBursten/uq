# date.py
# Some common date functions.
# Note: introduction of datetime module in Python 2.3 possibly makes this
# module obsolete.

import calendar
import time

class DateError(Exception): pass

MONTHNAMES3 = [None, "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
                     "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

class Date:
    """ Simple date class, mostly an alternative to a date-time tuple
        (year, month, day, hour, minutes, seconds).
        We need this class to do some date manipulation, hence the inc() and
        dec() methods (which require the Julian date functions).
    """

    def __init__(self, *args):
        """ Gets date info from an ISO date string. They are assumed to have
            the format "YYYY-MM-DD HH:MM:SS"; any trailing characters are
            ignored, and so are the separators.

            Constructors:
            Date()          uses the current date and time.
            Date(isodate)   creates date from ISO date string.
            Date(otherDate) copy-constructor
            Date(None)      01 Jan 1900, 00:00
        """
        if len(args) == 0:
            # take the current date and time
            t = time.localtime()
            self.year, self.month, self.day, self.hours, self.minutes, \
             self.seconds = t[:6]
        elif len(args) == 1 and isinstance(args[0], Date):
            self.__dict__.update(args[0].__dict__)
        elif len(args) == 1 and args[0] is None:
            # special 'None' constructor... creates 01 Jan 1980; the mktime
            # stuff used elsewhere cannot handle 1900 or even 1970. >=(
            self.year = 1980
            self.month = self.day = 1
            self.hours = self.minutes = self.seconds = 0
        elif len(args) == 1 and isinstance(args[0], time.struct_time):
            # use time.struct_time as returned by time module
            self.year, self.month, self.day = args[0][:3]
            self.hours, self.minutes, self.seconds = args[0][3:6]
        elif len(args) == 1:
            isodate = args[0]
            self.year = int(isodate[0:4])
            self.month = int(isodate[5:7])
            self.day = int(isodate[8:10])
            if len(isodate) > 11:
                self.hours = int(isodate[11:13])
                self.minutes = int(isodate[14:16])
                self.seconds = int(isodate[17:19])
            else:
                self.hours = self.minutes = self.seconds = 0
        else:
            raise TypeError, "Invalid arguments for Date: %s" % (args)

    def isodate(self):
        """ Return the current date as an ISO date string. """
        return "%02d-%02d-%02d %02d:%02d:%02d" % (self.year, self.month,
         self.day, self.hours, self.minutes, self.seconds)

    def __repr__(self):
        return "Date(" + repr(self.isodate()) + ")"

    def __str__(self):
        return self.isodate()

    def isleapyear(self):
        return calendar.isleap(self.year)

    def weekday(self):
        """ Return weekday 0-6 (Mon-Sun). """
        return calendar.weekday(self.year, self.month, self.day)

    def isweekend(self):
        """ Return 1 if the day falls in the weekend. """
        return self.weekday() in (5, 6)

    def julian(self):
        """ Return the Julian date of this date. """
        return date_to_julian(self.day, self.month, self.year)

    def inc(self, n=1):
        """ Increase the date by n days. """
        j = self.julian()
        j = j + n
        self.day, self.month, self.year = julian_to_date(j)

    def dec(self, n=1):
        return self.inc(-n)

    def clone(self):
        d = Date()
        d.__dict__.update(self.__dict__)
        return d

    def resettime(self):
        self.hours = self.minutes = self.seconds = 0

    def __add__(self, n=1):
        d = self.clone()
        d.inc(n)
        return d

    def __sub__(self, n=1):
        return self.__add__(-n)

    def __cmp__(self, other):
        # assuming the other is a Date too...
        return cmp(self.isodate(), other.isodate())

    def check(self):
        """ Return true if the current date values are valid. """
        return (1 <= self.month <= 12
         and 1 <= self.day <= 31
         and 0 <= self.hours <= 23
         and 0 <= self.minutes <= 59
         and 0 <= self.seconds <= 59
         and self.day <= calendar.mdays[self.month]+calendar.isleap(self.year)
        )

    def correct(self):
        """ Correct the 24-hour problem. Return true if a correction was made,
            0 otherwise. """
        if self.hours == 24:
            self.hours = self.hours - 24
            self.inc()
            return 1
        return 0

    def dec_time(self, hours=0, minutes=0, seconds=0):
        days = 0
        self.seconds = self.seconds - seconds
        while self.seconds < 0:
            self.seconds = self.seconds + 60
            self.minutes = self.minutes - 1
        self.minutes = self.minutes - minutes
        while self.minutes < 0:
            self.minutes = self.minutes + 60
            self.hours = self.hours - 1
        self.hours = self.hours - hours
        while self.hours < 0:
            days = days + 1
            self.hours = self.hours + 24
        if days:
            self.dec(days)

    def inc_time(self, hours=0, minutes=0, seconds=0):
        days = 0
        self.seconds = self.seconds + seconds
        while self.seconds >= 60:
            self.seconds = self.seconds - 60
            self.minutes = self.minutes + 1
        self.minutes = self.minutes + minutes
        while self.minutes >= 60:
            self.minutes = self.minutes - 60
            self.hours = self.hours + 1
        self.hours = self.hours + hours
        while self.hours >= 24:
            self.hours = self.hours - 24
            days = days + 1
        if days:
            self.inc(days)

    def as_tuple(self):
        """ Returns the current Date in 9-tuple format.  Last three values
            are always 0. """
        return (self.year, self.month, self.day, self.hours, self.minutes,
                self.seconds, 0, 0, 0)

    def diff(self, other):
        """ Return the difference with the other Date, in seconds. """
        s9 = self.as_tuple()
        o9 = other.as_tuple()
        ss = time.mktime(s9)
        so = time.mktime(o9)
        return ss - so  # possibly negative

###
### Some useful functions...

def today():
    """ Returns the current date, as a Date object, with time 00:00:00. """
    d = Date()  # has time as well
    d.hours = d.minutes = d.seconds = 0
    return d

def current():
    """ Return the current date/time, in ISO-string format. """
    return Date().isodate()

###
### Julian date routines.

_MINYEAR = 1600
_MAXYEAR = 3999

_MINDATE = 0x00000000   # 01/01/1600
_MAXDATE = 0x000d6025   # 31/12/3999

_FIRST2MONTHS = 59  # 1600 was a leap year
_FIRSTDAYOFWEEK = 5 # 01/01/1600 was a Saturday


# The following functions assume that the date is valid:

def date_to_julian(day, month, year):
    """Convert to julian date."""
    if year == _MINYEAR and month < 3:
        if month == 1: return day-1
        else: return day+30
    else:
        if month > 2:
            month = month - 3
        else:
            month = month + 9
            year = year - 1
        year = year - _MINYEAR

        return (((year // 100)*146097) // 4) + (((year % 100) * 1461) // 4) \
        +(((153 * month) + 2) // 5) + day + _FIRST2MONTHS

def julian_to_date(julian):
    """ Convert a julian date to a conventional one. """
    if julian <= _FIRST2MONTHS:
        year = _MINYEAR
        if julian <= 30:
            month = 1
            day = julian + 1
        else:
            month = 2
            day = julian - 30
    else:
        i = (4 * (julian - _FIRST2MONTHS)) - 1
        j = (4 * ((i % 146097) // 4)) + 3
        year = (100 * (i // 146097)) + (j // 1461)
        i = (5 * (((j % 1461) + 4) // 4)) - 3
        month = i // 153
        day = ((i % 153) + 5) // 5
        if month < 10:
            month = month + 3
        else:
            month = month - 9
            year = year + 1
        year = year + _MINYEAR

    return day, month, year


if __name__ == "__main__":

    print date_to_julian(9, 2, 1973)
    d3 = Date("2002-01-01 24:00:00")
    print d3.hours
    print d3.check()
    d3.correct()
    print d3.check(), d3

    d1 = Date("2003-08-06 12:00:00")
    d2 = Date("2003-08-06 13:00:00")
    print d1.diff(d2), d2.diff(d1)

