# rsswriter.py
# Generate RSS 0.91 files.

import date
import re
import string
import time
import urlparse
#
import htmltools

template1 = """\
<?xml version="1.0" encoding="UTF-8"?>

<rdf:RDF
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns="http://purl.org/rss/1.0/"
 xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:syn="http://purl.org/rss/1.0/modules/syndication/"
 xmlns:admin="http://webns.net/mvcb/"
>

<channel rdf:about="%(about)s">
<title>%(title)s</title>
<link>%(link)s</link>
<description>%(description)s</description>
<!--
<items>
 <rdf:Seq>
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45430.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45348.html" />

  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45345.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45320.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45305.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45302.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45291.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45284.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45279.html" />
  <rdf:li rdf:resource="http://www.mailbucket.org/cocoa-dev-45275.html" />
 </rdf:Seq>

</items>
-->
</channel>

%(body)s

</rdf:RDF>
"""

# has variables: about, title, link, description, body
# XXX not sure what to do with those items.  Can they be omitted?

template2 = """\
<item rdf:about="%(about)s">
<title>%(title)s</title>
<link>%(link)s</link>
<pubDate>%(pubdate)s</pubDate>
<description><![CDATA[%(description)s ]]></description>
<author>%(author)s</author>
<guid>%(guid)s</guid>
</item>"""

# has variables: about, title, link, description
# we also need a GUID of some sort?

UNWANTED_HEADERS = ['Received', 'Return-Path', 'Message-ID', 'Delivered-To',
                    'Subject', 'To', 'Date']

re_lots_of_dashes = re.compile("-{5,}")
re_lots_of_stars = re.compile("\*{5,}")

class RSSWriter:

    def __init__(self, messages):
        self.messages = messages

    def generate_rss(self):
        """ Create RSS as one long string. """
        about = ""
        title = 'UQ Error Messages'
        link = "http://www.oasisdigital.com/"
        description = "Emailed UQ error messages and warnings"
        items = [self.generate_rss_item(msg) for msg in self.messages]
        body = string.join(items, "\n")
        return template1 % locals()

    def generate_rss_item(self, msg):
        """ Generate an RSS <item> tag with entry data. The <description> will
            be filled with a *summary* of the entry text.
        """
        about = ""
        pubdate = msg['date']   # unconverted for now
        title = htmltools.strip_html(msg['subject'])  + " " + pubdate
        link = "http://www.oasisdigital.com/"
        guid = msg['message-id'].replace('<', '').replace('>', '')
        author = htmltools.quote(msg['from'])

        # strip unwanted headers, so they don't show up in the message body
        # this should be done BEFORE we get the description and AFTER all
        # other variables
        for header in UNWANTED_HEADERS:
            try:
                del msg[header]
            except:
                pass

        description = htmltools.quote(msg.as_string())
        description = string.replace(description, "\n", "<br>\n")
        description = "<p>\n" + description + "</p>\n"

        # replace obnoxious repetitive patterns in body
        description = re_lots_of_dashes.sub("-----", description)
        description = re_lots_of_stars.sub("*****", description)

        return template2 % locals()

