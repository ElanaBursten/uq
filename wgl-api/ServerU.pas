unit ServerU;

interface

uses
  System.SysUtils, System.Classes, Winapi.Messages,
  IdHTTPWebBrokerBridge, Web.HTTPApp;

type
  TServerDM = class;

  TServerLogNotifyEvent = procedure (Sender: TServerDM; const ALogMessage: string) of object;

  TServerDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    FOnLogMessage: TServerLogNotifyEvent;
  public
    Port: Integer;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Start;
    procedure Stop;
    function Active: Boolean;
    procedure ConfigureFromIni;
    procedure AddLogMessage(const AIsSuccess: Boolean; const AMessage: string);
    property OnLogMessage: TServerLogNotifyEvent read FOnLogMessage write FOnLogMessage;
  end;

var
  ServerDM: TServerDM;

implementation

uses Windows, OdMiscUtils, StrUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TServerDM.Active: Boolean;
begin
  Result := FServer.Active;
end;

procedure TServerDM.ConfigureFromIni;
begin
  // This means there will be no GUI with which to configure; we are on our own.

  // TODO: load these from INI or elsewhere.
  Port := 6001;
end;

constructor TServerDM.Create(AOwner: TComponent);
begin
  inherited;
  ConfigureFromIni;
end;

procedure TServerDM.DataModuleCreate(Sender: TObject);
begin
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
  FOnLogMessage := nil;
end;

procedure TServerDM.DataModuleDestroy(Sender: TObject);
begin
  Stop;
end;

destructor TServerDM.Destroy;
begin
  inherited;
end;

procedure TServerDM.AddLogMessage(const AIsSuccess: Boolean; const AMessage: string);
var
  Mess: string;
begin
  Mess := IfThen(not AIsSuccess, '[Error] ', '') + AMessage;
  if Assigned(FOnLogMessage) then
    FOnLogMessage(Self, Mess);
end;

procedure TServerDM.Start;
begin
  AddLogMessage(True, 'Starting');
  if not FServer.Active then begin
    FServer.Bindings.Clear;
    FServer.DefaultPort := Port;
    FServer.Active := True;
  end;
  AddLogMessage(True, 'Started');
end;

procedure TServerDM.Stop;
begin
  AddLogMessage(True, 'Stopping');
  FServer.Active := False;
  FServer.Bindings.Clear;
  AddLogMessage(True, 'Stopped');
end;

end.

