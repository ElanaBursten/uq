object WGLDM: TWGLDM
  OldCreateOrder = False
  Height = 440
  Width = 477
  object Conn: TFDConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=JohnDB'
      'LoginTimeout=5'
      'MARS=yes'
      'User_Name=sa'
      'DriverID=MSSQL')
    Left = 32
    Top = 48
  end
  object LoginUser: TFDStoredProc
    Connection = Conn
    StoredProcName = 'login_user2'
    Left = 104
    Top = 64
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@UserName'
        DataType = ftString
        ParamType = ptInput
        Size = 25
      end
      item
        Position = 3
        Name = '@PW'
        DataType = ftString
        ParamType = ptInput
        Size = 40
      end>
  end
  object CheckAPIKey: TFDQuery
    Connection = Conn
    SQL.Strings = (
      
        'select emp_id, chg_pwd, chg_pwd_date  from users where login_id=' +
        ':login_id and api_key=:api_key')
    Left = 168
    Top = 104
    ParamData = <
      item
        Name = 'LOGIN_ID'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'API_KEY'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object MSSQLDriverLink: TFDPhysMSSQLDriverLink
    Left = 32
    Top = 120
  end
end
