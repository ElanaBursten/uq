unit WGLDMu;

interface

uses
  System.SysUtils, System.Classes, Data.DB, HttpApp, Wessy, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Phys.MySQL, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait;

type
  TWGLDM = class(TDataModule, IWebDataModule)
    Conn: TFDConnection;   //Recreated for FD Migration
    LoginUser: TFDStoredProc;
    CheckAPIKey: TFDQuery;
    MSSQLDriverLink: TFDPhysMSSQLDriverLink;
  private
    Request: TWebRequest;
    Response: TWebResponse;
    Params: TStrings;
    SessionVars: TStrings;
  public
    procedure UseContext(ARequest: TWebRequest; AResponse: TWebResponse;
      AParams: TStrings; ASessionVars: TStrings);
    function CheckLogin(const Username, Password: string): Boolean;
    function CheckAuthorization(const Username, APIKey: string): Boolean;
    procedure ReturnUserInformation;
    procedure ReturnPingInformation;
  published
    procedure ConnectToDB;
  end;

implementation
uses
  Inifiles, Variants, PasswordRules,
  XSuperObject;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TWGLDM }

function TWGLDM.CheckAuthorization(const Username, APIKey: string): Boolean;
var
  ChgPwd: Boolean;
  ChgPwdDate: TDateTime;
begin
  Result := False;
  Assert(not CheckAPIKey.Active);
  CheckAPIKey.Params.ParamValues['login_id'] := UserName;
  CheckAPIKey.Params.ParamValues['api_key'] := APIKey;
  CheckAPIKey.Open;
  try
    if not CheckAPIKey.Eof then begin
      ChgPwd := CheckAPIKey.FieldByName('chg_pwd').AsBoolean;
      ChgPwdDate := CheckAPIKey.FieldByName('chg_pwd_date').AsDateTime;
      if ChgPwd or ((ChgPwdDate > 1) and (Now > ChgPwdDate)) then begin
        //TODO : Modify function to return a message indicating password is expired
        Result := False;
      end else
        Result := True;
    end;
  finally
    CheckAPIKey.Close;
  end;
end;

function TWGLDM.CheckLogin(const Username, Password: string): Boolean;
var
  LoginFailed: Boolean;
begin
  Assert(not LoginUser.Active);
  // check for client sending hash to encrypted db:
  LoginUser.Params.ParamValues['@UserName'] := UserName;
  LoginUser.Params.ParamValues['@PW'] := Password;
  LoginUser.Open;
  try
    LoginFailed := not LoginUser.EOF;

    if LoginFailed and PasswordIsHashed(Password) then begin
      // check for client sending hash to unencrypted db:
      LoginUser.Close;
      LoginUser.Params.ParamValues['@UserName'] := UserName;
      LoginUser.Params.ParamValues['@PW'] := Null;
      LoginUser.Open;
      LoginFailed := LoginUser.EOF;
      if not LoginFailed then
        LoginFailed := (GeneratePasswordHash(UserName, LoginUser.FieldByName('password').AsString) <> Password);
    end;
  finally
    LoginUser.Close;
  end;
  Result := not LoginFailed;
end;

procedure TWGLDM.ConnectToDB;
var
  Ini: TInifile;
  ConnString: string;
begin
  Ini := TInifile.Create(ExtractFilePath(GetModuleName(HInstance)) + 'QMServer.ini');
  try
    ConnString := Ini.ReadString('Database', 'ADConnectionString', '');
    if ConnString = '' then
      raise Exception.Create('No connection string configured in ' + Ini.FileName);
  finally
    FreeAndNil(Ini);
  end;

  Conn.ConnectionString := ConnString;
  Conn.Open;
end;

procedure TWGLDM.ReturnUserInformation;
var
  R: ISuperObject;
  UserID: string;
begin
  UserID := SessionVars.Values['LoggedIn'];
  R := SO;
  R.S['result'] := 'ok';
  R.S['userID'] := UserID;
  Response.Content := R.AsJSon(True, False);
  Response.ContentType := 'application/json';
end;

procedure TWGLDM.ReturnPingInformation;
begin
  ReturnUserInformation;
end;

procedure TWGLDM.UseContext(ARequest: TWebRequest; AResponse: TWebResponse;
  AParams, ASessionVars: TStrings);
begin
  Request := ARequest;
  Response := AResponse;
  Params := AParams;
  SessionVars := ASessionVars;

  ConnectToDB;
end;

end.
