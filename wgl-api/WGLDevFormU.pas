unit WGLDevFormU;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TQMWGLDevForm = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    Label1: TLabel;
    ApplicationEvents: TApplicationEvents;
    ButtonOpenBrowser: TButton;
    procedure ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure ButtonOpenBrowserClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  end;

var
  QMWGLDevForm: TQMWGLDevForm;

implementation

{$R *.dfm}

uses
  Winapi.ShellApi, IdHttp, ServerU;

procedure TQMWGLDevForm.ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not ServerDM.Active;
  ButtonStop.Enabled := ServerDM.Active;
  EditPort.Enabled := not ServerDM.Active;
end;

procedure TQMWGLDevForm.ButtonOpenBrowserClick(Sender: TObject);
var
  LURL: string;
begin
  ServerDM.Start;
  LURL := Format('http://localhost:%s', [EditPort.Text]);
  ShellExecute(0,
        nil,
        PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure TQMWGLDevForm.ButtonStartClick(Sender: TObject);
begin
  ServerDM.Port := StrToInt(EditPort.Text);
  ServerDM.Start;
end;

procedure TQMWGLDevForm.ButtonStopClick(Sender: TObject);
begin
  ServerDM.Stop;
end;

procedure TQMWGLDevForm.FormActivate(Sender: TObject);
begin
  ButtonStart.Click;
end;

end.
