object GIOrderDM: TGIOrderDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 319
  Width = 644
  object MSSQLDriverLink: TFDPhysMSSQLDriverLink
    Left = 104
    Top = 24
  end
  object Conn: TFDConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=QMReport'
      'LoginTimeout=5'
      'MARS=yes'
      'DriverID=MSSQL')
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 32
    Top = 24
  end
  object InsertWorkOrder: TFDCommand
    Connection = Conn
    CommandText.Strings = (
      'INSERT INTO work_order (wo_source, kind, status, '
      '  closed, parsed_ok, client_id, source_sent_attachment, '
      '  wo_number, client_wo_number, map_ref, caller_name, '
      '  caller_phone, caller_altphone, work_address_street,'
      '  work_address_number, work_address_number_2, '
      '  work_county, work_city, work_state, work_zip, '
      '  work_lat, work_long, map_x_coord, map_y_coord, '
      '  transmit_date, due_date, image)'
      'VALUES (:wo_source, :kind, :status, '
      '  :closed, :parsed_ok, :client_id, :source_sent_attachment,'
      '  :wo_number, :client_wo_number, :map_ref, :caller_name, '
      '  :caller_phone, :caller_altphone, :work_address_street, '
      '  :work_address_number, :work_address_number_2, '
      '  :work_county, :work_city, :work_state, :work_zip, '
      '  :work_lat, :work_long, :map_x_coord, :map_y_coord, '
      '  :transmit_date, :due_date, :image)')
    ParamData = <
      item
        Name = 'wo_source'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'kind'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'status'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'closed'
        DataType = ftBoolean
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'parsed_ok'
        DataType = ftBoolean
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'client_id'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'source_sent_attachment'
        DataType = ftBoolean
        ParamType = ptInput
      end
      item
        Name = 'wo_number'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'client_wo_number'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'map_ref'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'caller_name'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'caller_phone'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'caller_altphone'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_address_street'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_address_number'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_address_number_2'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'work_county'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_city'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_state'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_zip'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_lat'
        DataType = ftFloat
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'work_long'
        DataType = ftFloat
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'map_x_coord'
        DataType = ftFloat
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'map_y_coord'
        DataType = ftFloat
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'transmit_date'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'due_date'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'image'
        DataType = ftMemo
        ParamType = ptInput
      end>
    Left = 64
    Top = 152
  end
  object FindWorkOrder: TFDQuery
    Connection = Conn
    SQL.Strings = (
      'select wo.wo_id from work_order wo '
      'inner join work_order_inspection woi on wo.wo_id = woi.wo_id'
      'where wo_source = :wo_source'
      'and wo_number = :wo_number'
      'and client_wo_number = :client_wo_number')
    Left = 56
    Top = 88
    ParamData = <
      item
        Name = 'wo_source'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'wo_number'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'client_wo_number'
        ParamType = ptInput
      end>
  end
  object InsertInspection: TFDCommand
    Connection = Conn
    CommandText.Strings = (
      
        'INSERT INTO work_order_inspection (wo_id, account_number, premis' +
        'e_id, meter_number, compliance_due_date)'
      
        'VALUES(:wo_id, :account_number, :premise_id, :meter_number, :com' +
        'pliance_due_date)')
    ParamData = <
      item
        Name = 'wo_id'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'account_number'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'premise_id'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'meter_number'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'compliance_due_date'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end>
    Left = 152
    Top = 152
  end
  object GetWglWorkOrderStatus: TFDStoredProc
    Connection = Conn
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    StoredProcName = 'get_wgl_work_order_status'
    Left = 64
    Top = 208
    ParamData = <
      item
        Name = '@SinceDate'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Name = '@SinceID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object WORemedy: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 232
    Top = 208
  end
  object WorkOrders: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 162
    Top = 208
  end
  object GetWGLWOStatus: TFDQuery
    Connection = Conn
    SQL.Strings = (
      
        'select top (:MaxOrders) wo_response_id, wo_id, response_sent, su' +
        'ccess, reply'
      'into ##TempWO'
      'from wo_response_log'
      
        'where wo_source = '#39'WGL'#39' and response_sent = '#39'NOTSENT'#39' and [statu' +
        's] not in ('#39'CGA4'#39','#39'CGA5'#39','#39'CGA6'#39','#39'CGA7'#39','#39'CGA8'#39','#39'CGA9'#39')'
      'and wo_id not in '
      
        '(select wo_id from wo_response_log where wo_source = '#39'WGL'#39' and [' +
        'status] = '#39'DONE'#39' and  response_sent <> '#39'NOTSENT'#39')'
      ';'
      
        'select T.wo_response_id, wo.wo_id, wo.wo_number, wo.client_wo_nu' +
        'mber, coalesce(st.outgoing_status, wo.status) as status, '
      
        '  coalesce(ws.status_date,wo.status_date) as status_date, woi.ac' +
        'tual_meter_number, woi.actual_meter_location, '
      
        '  woi.building_type, woi.cga_visits, woi.map_status, woi.mercury' +
        '_regulator, woi.alert_order_number, woi.inspection_change_date,'
      '  coalesce(woi.vent_clearance_dist, '#39#39') as vent_clearance_dist,'
      '  coalesce(woi.vent_ignition_dist, '#39#39') as vent_ignition_dist,'
      
        '  coalesce(r1.description, woi.actual_meter_location) as meter_l' +
        'oc_desc,'
      
        '  coalesce(r2.description, woi.building_type) as building_type_d' +
        'esc,'
      '  coalesce(r3.description, woi.map_status) as map_status_desc,'
      '  coalesce(e.emp_number, '#39'UQ'#39') as emp_number,'
      '  woi.gas_light'
      'into ##TempWOMain'
      'from ##TempWO T join work_order wo on t.wo_id = wo.wo_id '
      '  join work_order_inspection woi on wo.wo_id = woi.wo_id'
      
        '  left join (select ws.wo_id, ws.status_date, row_number() over ' +
        '(partition by ws.wo_id order by ws.wo_status_id) AS rnum'
      #9#9#9'from work_order wo '
      #9#9#9'inner join wo_status_history ws on ws.wo_id = wo.wo_id'
      #9#9#9'where wo.wo_source ='#39'WGL'#39' and ws.Status ='#39'I'#39
      #9#9#9') ws on ws.wo_id =wo.wo_id and ws.rnum =1'
      
        '  left join status_translation st on (st.cc_code = wo.wo_source ' +
        'and st.status = wo.status and st.active = 1)'#9
      
        '  left join reference r1 on (r1.type = '#39'metrloc'#39' and r1.code = w' +
        'oi.actual_meter_location)'
      
        '  left join reference r2 on (r2.type = '#39'bldtype'#39' and r2.code = w' +
        'oi.building_type)'
      
        '  left join reference r3 on (r3.type = '#39'mapstat'#39' and r3.code = w' +
        'oi.map_status)'
      '  left join employee e on (e.emp_id = wo.assigned_to_id)'
      ';'
      'select *'
      'from ##TempWOMain'
      'order by wo_response_id')
    Left = 432
    Top = 152
    ParamData = <
      item
        Name = 'MAXORDERS'
        DataType = ftInteger
        ParamType = ptInput
        Value = 10
      end>
  end
  object GetWGLWORemedy: TFDQuery
    Connection = Conn
    SQL.Strings = (
      'select wor.wo_id,'
      '  case wt.work_type'
      '    when '#39'VC'#39' then '#39'VC'#39' + T.vent_clearance_dist'
      '    when '#39'VI'#39' then '#39'VI'#39' + T.vent_ignition_dist'
      '    else wt.work_type'
      '  end work_type, coalesce(wt.flag_color, '#39'NONE'#39') flag_color'
      
        'from work_order_remedy wor join ##TempWOMain T on wor.wo_id = T.' +
        'wo_id'
      
        '  left join work_order_work_type wt on wor.work_type_id = wt.wor' +
        'k_type_id'
      'where wor.active = 1 '
      'order by 1, 2')
    Left = 432
    Top = 200
  end
  object UpdtResponseSuccess: TFDCommand
    Connection = Conn
    CommandText.Strings = (
      'update wo_response_log'
      
        'set wo_response_log.success = 1, wo_response_log.reply = '#39'01-Suc' +
        'cess'#39','
      
        'wo_response_log.response_sent = CONVERT(VARCHAR(10), ##TempWOMai' +
        'n.inspection_change_date, 101) '
      
        'from wo_response_log INNER JOIN ##TempWOMain ON (wo_response_log' +
        '.wo_response_id = ##TempWOMain.wo_response_id)')
    Left = 432
    Top = 248
  end
  object UpdtResponseFail: TFDCommand
    Connection = Conn
    CommandText.Strings = (
      'update wo_response_log'
      
        'set wo_response_log.success = 0, wo_response_log.reply = '#39'99-Fai' +
        'led to send'#39','
      'wo_response_log.response_sent = '#39'FAILED'#39
      
        'from wo_response_log INNER JOIN ##TempWOMain ON (wo_response_log' +
        '.wo_response_id = ##TempWOMain.wo_response_id)')
    Left = 560
    Top = 248
  end
  object CleanUpWOResponseLog: TFDCommand
    Connection = Conn
    CommandText.Strings = (
      'update wo_response_log'
      'set response_sent = '#39'MISSED'#39
      'where wo_source = '#39'WGL'#39' and response_sent = '#39'NOTSENT'#39' and '
      '(([status] in ('#39'CGA4'#39','#39'CGA5'#39','#39'CGA6'#39','#39'CGA7'#39','#39'CGA8'#39','#39'CGA9'#39')) or'
      
        '(wo_id in (select wo_id from wo_response_log where wo_source = '#39 +
        'WGL'#39' and [status] = '#39'DONE'#39' and response_sent <> '#39'NOTSENT'#39')))')
    Left = 560
    Top = 200
  end
end
