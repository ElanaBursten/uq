{ Invokable interface IUQWGLService }

unit QMWGLServiceIntf;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns;

type
  { Invokable interfaces must derive from IInvokable }
  IUQWGLService = interface(IInvokable)
  ['{30670951-2869-4D89-B955-0169C21E044A}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function AddAOCOrders(const Value: AnsiString): AnsiString; stdcall;
    function GetAOCOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): AnsiString; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IUQWGLService));

end.
