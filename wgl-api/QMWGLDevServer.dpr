program QMWGLDevServer;
{$APPTYPE GUI}

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  WGLDevFormU in 'WGLDevFormU.pas' {QMWGLDevForm},
  WGLWMu in 'WGLWMu.pas' {APIWebMod: TWebModule},
  WGLDMu in 'WGLDMu.pas' {WGLDM: TDataModule},
  ServerU in 'ServerU.pas' {ServerDM: TDataModule},
  QMWGLServiceImpl in 'QMWGLServiceImpl.pas',
  QMWGLServiceIntf in 'QMWGLServiceIntf.pas',
  GIOrderDMu in 'GIOrderDMu.pas' {GIOrderDM: TDataModule},
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  GIOrder in 'GIOrder.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  WebServiceAttachmentDMu in 'WebServiceAttachmentDMu.pas' {ServerAttachment: TDataModule},
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  QMGraphics in '..\common\QMGraphics.pas',
  ThreadSafeLoggerU in '..\common\ThreadSafeLoggerU.pas',
  OperationTracking in '..\server\OperationTracking.pas',
  AddressUtils in '..\common\AddressUtils.pas',
  ProjApi447 in '..\thirdparty\proj4\ProjApi447.pas',
  uDsCoordinates in '..\thirdparty\proj4\uDsCoordinates.pas',
  XSuperObject in '..\thirdparty\XSuperObject\XSuperObject.pas',
  XSuperJSON in '..\thirdparty\XSuperObject\XSuperJSON.pas',
  FDUtils in '..\common\FDUtils.pas';

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TServerDM, ServerDM);
  Application.CreateForm(TQMWGLDevForm, QMWGLDevForm);
  Application.Run;
end.
