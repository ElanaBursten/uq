inherited ServerAttachment: TServerAttachment
  OldCreateOrder = True
  Height = 183
  Width = 323
  object TempQuery: TFDQuery
    Left = 40
    Top = 72
  end
  object AttachmentData: TFDQuery
    Left = 40
    Top = 16
  end
  object UploadLocation: TFDQuery
    SQL.Strings = (
      
        'select location_id from upload_location where active=1 and descr' +
        'iption=:description')
    Left = 160
    Top = 32
    ParamData = <
      item
        Name = 'DESCRIPTION'
        ParamType = ptInput
      end>
  end
end
