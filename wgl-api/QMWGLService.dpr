library QMWGLService;

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  Winapi.ActiveX,
  System.Win.ComObj,
  Web.WebBroker,
  Web.Win.ISAPIApp,
  Web.Win.ISAPIThreadPool,
  WGLWMu in 'WGLWMu.pas' {APIWebMod: TWebModule},
  WGLDMu in 'WGLDMu.pas' {WGLDM: TDataModule},
  QMWGLServiceImpl in 'QMWGLServiceImpl.pas',
  QMWGLServiceIntf in 'QMWGLServiceIntf.pas',
  FDUtils in '..\common\FDUtils.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  GIOrderDMu in 'GIOrderDMu.pas' {GIOrderDM: TDataModule},
  GIOrder in 'GIOrder.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  WebServiceAttachmentDMu in 'WebServiceAttachmentDMu.pas' {ServerAttachment: TDataModule},
  QMGraphics in '..\common\QMGraphics.pas',
  ThreadSafeLoggerU in '..\common\ThreadSafeLoggerU.pas',
  OperationTracking in '..\server\OperationTracking.pas',
  AddressUtils in '..\common\AddressUtils.pas',
  XSuperObject in '..\thirdparty\XSuperObject\XSuperObject.pas',
  XSuperJSON in '..\thirdparty\XSuperObject\XSuperJSON.pas';

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.WebModuleClass := WebModuleClass;
  Application.Run;
end.
