// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/QMServer11729App/QMWGLService.dll/wsdl/IUQWGLService
// Version  : 1.0
// (6/19/2013 2:30:09 PM - - $Rev: 52705 $)
// ************************************************************************ //

unit IUQWGLService1;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[]


  // ************************************************************************ //
  // Namespace : urn:QMWGLServiceIntf-IUQWGLService
  // soapAction: urn:QMWGLServiceIntf-IUQWGLService#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // use       : encoded
  // binding   : IUQWGLServicebinding
  // service   : IUQWGLServiceservice
  // port      : IUQWGLServicePort
  // URL       : http://localhost/QMServer11729App/QMWGLService.dll/soap/IUQWGLService
  // ************************************************************************ //
  IUQWGLService = interface(IInvokable)
  ['{BB82AFA5-16E9-D45D-1A50-045015D250A1}']
    function AddAOCOrders(const Value: AnsiString): AnsiString; stdcall;
    function GetAOCOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): AnsiString; stdcall;
  end;

function GetIUQWGLService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IUQWGLService;


implementation
  uses SysUtils;

function GetIUQWGLService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IUQWGLService;
const
  defWSDL = 'http://localhost/QMServerApp/QMWGLService.dll/wsdl/IUQWGLService';
  defURL  = 'http://localhost/QMServerApp/QMWGLService.dll/soap/IUQWGLService';
  defSvc  = 'IUQWGLServiceservice';
  defPrt  = 'IUQWGLServicePort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IUQWGLService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  { IUQWGLService }
  InvRegistry.RegisterInterface(TypeInfo(IUQWGLService), 'urn:QMWGLServiceIntf-IUQWGLService', '');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IUQWGLService), 'urn:QMWGLServiceIntf-IUQWGLService#%operationName%');

end.

