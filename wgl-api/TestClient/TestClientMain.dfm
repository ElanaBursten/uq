object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Test QM WGL SOAP Service'
  ClientHeight = 559
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPageControl: TPageControl
    Left = 0
    Top = 0
    Width = 671
    Height = 559
    ActivePage = TestServiceTab
    Align = alClient
    TabOrder = 0
    object TestXMLTab: TTabSheet
      Caption = 'Test XML'
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 663
        Height = 531
        Align = alClient
        BevelOuter = bvLowered
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          663
          531)
        object Label7: TLabel
          Left = 10
          Top = 23
          Width = 66
          Height = 13
          Caption = 'Input as XML:'
        end
        object Label8: TLabel
          Left = 328
          Top = 23
          Width = 70
          Height = 13
          Caption = 'Input as Data:'
        end
        object Button3: TButton
          Left = 180
          Top = 18
          Width = 129
          Height = 25
          Caption = 'Parse XML'
          TabOrder = 0
          OnClick = Button3Click
        end
        object InXMLMemo: TMemo
          Left = 10
          Top = 49
          Width = 299
          Height = 467
          Anchors = [akLeft, akTop, akBottom]
          ScrollBars = ssBoth
          TabOrder = 1
          WordWrap = False
        end
        object InDataMemo: TMemo
          Left = 320
          Top = 49
          Width = 337
          Height = 467
          Anchors = [akLeft, akTop, akRight, akBottom]
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 2
        end
      end
    end
    object TestServiceTab: TTabSheet
      Caption = 'Test Service Calls'
      DoubleBuffered = False
      ImageIndex = 1
      ParentDoubleBuffered = False
      object Splitter1: TSplitter
        Left = 0
        Top = 289
        Width = 663
        Height = 6
        Cursor = crVSplit
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 663
        Height = 105
        Align = alTop
        Caption = 'SOAP Connection properties'
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 14
          Top = 21
          Width = 57
          Height = 13
          Caption = 'WSDL URL: '
        end
        object Label4: TLabel
          Left = 14
          Top = 50
          Width = 35
          Height = 13
          Caption = 'Service'
        end
        object Label5: TLabel
          Left = 14
          Top = 75
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object WSDLURL: TComboBox
          Left = 77
          Top = 20
          Width = 532
          Height = 21
          TabOrder = 0
          Text = 
            'http://localhost/QMWGLServerApp/QMWGLService.dll/wsdl/IUQWGLServ' +
            'ice'
          Items.Strings = (
            
              'http://localhost/QMWGLServerApp/QMWGLService.dll/wsdl/IUQWGLServ' +
              'ice'
            'http://localhost:6001/wsdl/IUQWGLService')
        end
        object ServiceEdit: TEdit
          Left = 77
          Top = 45
          Width = 532
          Height = 21
          TabOrder = 1
          Text = 'IUQWGLServiceservice'
        end
        object PortEdit: TEdit
          Left = 77
          Top = 72
          Width = 532
          Height = 21
          TabOrder = 2
          Text = 'IUQWGLServicePort'
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 105
        Width = 663
        Height = 184
        Align = alTop
        BevelOuter = bvLowered
        ParentBackground = False
        TabOrder = 1
        DesignSize = (
          663
          184)
        object Label1: TLabel
          Left = 10
          Top = 23
          Width = 30
          Height = 13
          Caption = 'Input:'
        end
        object Label2: TLabel
          Left = 320
          Top = 23
          Width = 34
          Height = 13
          Caption = 'Result:'
        end
        object Button1: TButton
          Left = 180
          Top = 18
          Width = 129
          Height = 25
          Caption = 'Add GI Order'
          TabOrder = 0
          OnClick = Button1Click
        end
        object NewGIInMemo: TMemo
          Left = 10
          Top = 49
          Width = 299
          Height = 120
          Anchors = [akLeft, akTop, akBottom]
          ScrollBars = ssBoth
          TabOrder = 1
          WordWrap = False
        end
        object NewGIOutMemo: TMemo
          Left = 320
          Top = 49
          Width = 299
          Height = 120
          Anchors = [akLeft, akTop, akBottom]
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 2
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 295
        Width = 663
        Height = 236
        Align = alClient
        BevelOuter = bvLowered
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          663
          236)
        object Label6: TLabel
          Left = 12
          Top = 9
          Width = 74
          Height = 13
          Caption = 'Changes Since:'
        end
        object Label9: TLabel
          Left = 320
          Top = 31
          Width = 34
          Height = 13
          Caption = 'Result:'
        end
        object SinceEdit: TEdit
          Left = 12
          Top = 28
          Width = 162
          Height = 21
          TabOrder = 0
        end
        object Button2: TButton
          Left = 180
          Top = 26
          Width = 129
          Height = 25
          Caption = 'Get GI Statuses'
          TabOrder = 1
          OnClick = Button2Click
        end
        object SentThru: TEdit
          Left = 320
          Top = 51
          Width = 299
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object GIStatusMemo: TMemo
          Left = 320
          Top = 75
          Width = 299
          Height = 155
          Anchors = [akLeft, akTop, akRight, akBottom]
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 3
        end
        object ChckBxDebug: TCheckBox
          Left = 522
          Top = 30
          Width = 97
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Debug Mode:'
          TabOrder = 4
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Test Address Split'
      DoubleBuffered = False
      ImageIndex = 2
      ParentDoubleBuffered = False
      DesignSize = (
        663
        531)
      object Label10: TLabel
        Left = 18
        Top = 17
        Width = 100
        Height = 13
        Caption = 'Raw Street Address:'
      end
      object Label11: TLabel
        Left = 336
        Top = 17
        Width = 129
        Height = 13
        Caption = 'Formatted Street Address:'
      end
      object AddressInMemo: TMemo
        Left = 8
        Top = 48
        Width = 257
        Height = 145
        Lines.Strings = (
          '355 I ST SW #RTU1'
          '35511 I ST SW #RTU2'
          '1100 4TH ST SW #1'
          '201 A STREET SW'
          '1001-1011 EAST WEST HWY'
          'DUPONT CIRCLE'
          '1 - 10 FRANKLIN SQ #230-3')
        TabOrder = 0
      end
      object AddressOutMemo: TMemo
        Left = 286
        Top = 48
        Width = 337
        Height = 145
        Anchors = [akLeft, akTop, akRight, akBottom]
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 1
      end
      object Button4: TButton
        Left = 188
        Top = 12
        Width = 129
        Height = 25
        Caption = 'Format Addresses'
        TabOrder = 2
        OnClick = Button4Click
      end
    end
  end
  object HTTPRIO1: THTTPRIO
    WSDLLocation = 'http://localhost/QMServerApp/QMWGLService.dll/wsdl/IUQWGLService'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 624
    Top = 20
  end
end
