unit TestClientMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Soap.InvokeRegistry, Soap.Rio,
  Soap.SOAPHTTPClient, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    HTTPRIO1: THTTPRIO;
    MainPageControl: TPageControl;
    TestXMLTab: TTabSheet;
    TestServiceTab: TTabSheet;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    WSDLURL: TComboBox;
    ServiceEdit: TEdit;
    PortEdit: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    NewGIInMemo: TMemo;
    NewGIOutMemo: TMemo;
    Splitter1: TSplitter;
    Panel2: TPanel;
    Label6: TLabel;
    SinceEdit: TEdit;
    Button2: TButton;
    SentThru: TEdit;
    GIStatusMemo: TMemo;
    Panel3: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Button3: TButton;
    InXMLMemo: TMemo;
    InDataMemo: TMemo;
    Label9: TLabel;
    TabSheet1: TTabSheet;
    AddressInMemo: TMemo;
    AddressOutMemo: TMemo;
    Label10: TLabel;
    Button4: TButton;
    Label11: TLabel;
    ChckBxDebug: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    procedure ConnectHTTPRIO;
    procedure FormatAddresses;
    function ValidateDebug: Boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses IUQWGLService1, GIOrder, OdHourglass, AddressUtils;

procedure TForm1.Button1Click(Sender: TObject);
var
  GIService: IUQWGLService;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  NewGIOutMemo.Clear;
  ConnectHTTPRIO;
  GIService := HTTPRIO1 as IUQWGLService;

  NewGIOutMemo.Lines.Text := GIService.AddAOCOrders(NewGIInMemo.Lines.Text);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  GIService: IUQWGLService;
  Thru: AnsiString;
  Cursor: IInterface;
begin
  if ValidateDebug then begin
    Cursor := ShowHourGlass;
    GIStatusMemo.Clear;
    SentThru.Clear;
    ConnectHTTPRIO;
    GIService := HTTPRIO1 as IUQWGLService;
    if ChckBxDebug.Checked then begin
      GIStatusMemo.Lines.Text := GIService.GetAOCOrdersStatus(('DEBUG' + SinceEdit.Text), Thru)
    end else begin
      GIStatusMemo.Lines.Text := GIService.GetAOCOrdersStatus(SinceEdit.Text, Thru);
    end;
    SentThru.Text := Thru;
    SinceEdit.Text := Thru;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  GIData: TInspectionWorkOrders;
  Saver: IInspectionOrderWriter;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  Saver := TStringSaver.Create;
  GIData := TInspectionWorkOrders.CreateFromXML(InXMLMemo.Text, 100, 50 * 1024, Saver);
  try
    try
      GIData.Validate;
      GIData.Save;
      InDataMemo.Lines.Text := 'Success! :) Found ' + IntToStr(GIData.OrderCount) + ' orders: ' + #13#10 +
        (Saver as TStringSaver).AsString;
    except
      on E: Exception do
        InDataMemo.Lines.Text := 'Oops, something went wrong. ' + E.Message;
    end;
  finally
    FreeAndNil(GIData);
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  FormatAddresses;
end;

procedure TForm1.FormatAddresses;
var
  Address: string;
  Num, Num2, Street: string;
begin
  AddressOutMemo.Clear;

  for Address in AddressInMemo.Lines do begin
    SplitAddress(Address, Num, Num2, Street);
    AddressOutMemo.Lines.Add('Num: ' + Num + #09 + ' Num2: ' + Num2 + #09 + ' Street: ' + Street);
  end;
end;

function TForm1.ValidateDebug: Boolean;
var Valid: Boolean;
begin
  Valid := True;
  if not(ChckBxDebug.Checked) then begin
    Valid := (MessageDlg('You are not in DEBUG mode.' + #13#10 + 'Are you sure you want to update the response log?',
      mtWarning,mbYesNo,0,mbNo) <> mrNo);
  end;
  result := Valid;
end;

procedure TForm1.ConnectHTTPRIO;
begin
//N.B. If the contract (i.e exposed methods) changed - then the WSDL changed and will need reimported.
  HTTPRIO1.WSDLLocation := WSDLURL.Text;
  HTTPRIO1.Service := ServiceEdit.Text;
  HTTPRIO1.Port := PortEdit.Text;
end;

procedure TForm1.FormActivate(Sender: TObject);
const
  TestXMLFileName = '..\..\Data\GIIncomingFeed-Small.xml';
begin
  if FileExists(TestXMLFileName) then begin
    InXMLMemo.Lines.LoadFromFile(TestXMLFileName);
    NewGIInMemo.Lines.LoadFromFile(TestXMLFileName);
  end;
end;

end.
