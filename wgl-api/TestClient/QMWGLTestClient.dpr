program QMWGLTestClient;



uses
  Vcl.Forms,
  TestClientMain in 'TestClientMain.pas' {Form1},
  IUQWGLService1 in 'IUQWGLService1.pas',
  GIOrder in '..\GIOrder.pas',
  QMTempFolder in '..\..\common\QMTempFolder.pas',
  AddressUtils in '..\..\common\AddressUtils.pas';

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
