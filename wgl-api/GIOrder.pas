unit GIOrder;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  System.SysUtils, System.Classes, Variants, MSXML2_TLB, FireDAC.Comp.Client, DB,
  QMTempFolder;

type
  EGIException = class(Exception);
  EGIInvalidData = class(EGIException);
  EGIDuplicateData = class(EGIException);
  EGIDataTooBig = class(EGIException);

type
  IInspectionOrderWriter = interface
    procedure Save(WorkOrders, Attachments: TDataSet);
  end;

  TInspectionWorkOrders = class
  private
    Writer: IInspectionOrderWriter;
    OrderXML: IXMLDOMDocument;
    AttachmentFolder: ITemporaryFolder;
    FMaxOrders: Integer;
    FMaxSize: Integer;
    FXML: string;
    WOData: TFDMemTable;
    AttachData: TFDMemTable;
    function CreateWOData: TFDMemTable;
    function CreateAttachData: TFDMemTable;
    function SelectOrderNodes(const Path: string): IXMLDOMNodeList;
    procedure SaveBase64DataToTempFile(const FilePath, FileName, EncodedData: string);
    function GetWorkOrderImageXML(const WOAsXml: string): string;
    procedure ReprojectXYToLatLong(const X, Y: Double; out Longitude, Latitude: Double);
  public
    constructor CreateFromXML(const XML: string; const MaxOrders, MaxSize: Integer; DataSaver: IInspectionOrderWriter);
    destructor Destroy; override;
    function OrderCount: Integer;
    procedure Validate;
    procedure Save;
  end;

  // For testing use:
  TStringSaver = class(TInterfacedObject, IInspectionOrderWriter)
  private
    Buffer: string;
  public
    procedure Save(WorkOrders, Attachments: TDataSet);
    function AsString: string;
  end;

implementation

uses Inifiles, OdMiscUtils, QMGraphics, OdMSXMLUtils, Soap.EncdDecd,
  ProjApi447, uDsCoordinates, AddressUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{ TInspectionWorkOrders }

const
  XMLHeader = '<?xml version="1.0" encoding="UTF-8"?>' + sLineBreak;
  NodeRoot = '/AocOutboundFeed/AocOrders';

function TInspectionWorkOrders.SelectOrderNodes(const Path: string): IXMLDOMNodeList;
begin
  Result := OrderXML.selectNodes(NodeRoot + Path);
end;

function TInspectionWorkOrders.CreateWOData: TFDMemTable;
begin
  // The field names in this temp table should match the element names in the
  // incoming XML file
  Result := TFDMemTable.Create(nil);
  with Result.FieldDefs do begin
    with AddFieldDef do begin
      Name := 'AocOrderId';
      DataType := ftInteger;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'WrNo';
      DataType := ftString;
      Size := 30;
    end;
    with AddFieldDef do begin
      Name := 'AccountNumber';
      DataType := ftString;
      Size := 50;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'PremiseId';
      DataType := ftString;
      Size := 50;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'ServiceAddress1';
      DataType := ftString;
      Size := 80;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'ServiceAddressNumber';
      DataType := ftString;
      Size := 10;
    end;
    with AddFieldDef do begin
      Name := 'ServiceAddressNumber2';
      DataType := ftString;
      Size := 10;
    end;
    with AddFieldDef do begin
      Name := 'ServiceAddressStreet';
      DataType := ftString;
      Size := 60;
    end;
    with AddFieldDef do begin
      Name := 'ServiceAddress2'; // we don't use this; use city, state, zip instead.
      DataType := ftString;
      Size := 60;
    end;
    with AddFieldDef do begin
      Name := 'County';
      DataType := ftString;
      Size := 40;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'City';
      DataType := ftString;
      Size := 40;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'State';
      DataType := ftString;
      Size := 2;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'ZipCode';
      DataType := ftString;
      Size := 10;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'QuadMap';
      DataType := ftString;
      Size := 60;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'Latitude';
      DataType := ftFloat;
    end;
    with AddFieldDef do begin
      Name := 'Longitude';
      DataType := ftFloat;
    end;
    with AddFieldDef do begin
      Name := 'X_coordinate';
      DataType := ftFloat;
    end;
    with AddFieldDef do begin
      Name := 'Y_coordinate';
      DataType := ftFloat;
    end;
    with AddFieldDef do begin
      Name := 'ContactName';
      DataType := ftString;
      Size := 50;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'ContactPhone';
      DataType := ftString;
      Size := 40;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'Phone2';
      DataType := ftString;
      Size := 40;
    end;
    with AddFieldDef do begin
      Name := 'MeterNumber';
      DataType := ftString;
      Size := 80;
    end;
    with AddFieldDef do begin
      Name := 'ComplianceDueDate';
      DataType := ftDateTime;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'TransactionDate';
      DataType := ftDateTime;
      Required := True;
    end;
    with AddFieldDef do begin
      Name := 'TextImage';
      DataType := ftMemo;
    end;
  end;
end;

function TInspectionWorkOrders.CreateAttachData: TFDMemTable;
begin
  Result := TFDMemTable.Create(nil);
  with Result.FieldDefs do begin
    with AddFieldDef do begin
      Name := 'AocOrderId';
      DataType := ftInteger;
    end;
    with AddFieldDef do begin
      Name := 'SRName';
      DataType := ftString;
      Size := 100;
    end;
    with AddFieldDef do begin
      Name := 'PNGFileName';
      DataType := ftString;
      Size := 100;
    end;
    with AddFieldDef do begin
      Name := 'PNGFilePath';
      DataType := ftString;
      Size := 100;
    end;
  end;
end;

constructor TInspectionWorkOrders.CreateFromXML(const XML: string;
  const MaxOrders, MaxSize: Integer; DataSaver: IInspectionOrderWriter);
begin
  inherited Create;
  WOData := CreateWOData;
  AttachData := CreateAttachData;
  Writer := DataSaver;
  FXML := XML;
  FMaxOrders := MaxOrders;
  FMaxSize := MaxSize;

  AttachmentFolder := NewTemporaryFolder;
  OrderXML := ParseXml(XML);
end;

destructor TInspectionWorkOrders.Destroy;
begin
  FreeAndNil(WOData);
  FreeAndNil(AttachData);
  inherited;
end;

function TInspectionWorkOrders.OrderCount: Integer;
var
  NewOrders: IXMLDOMNodeList;
begin
  NewOrders := SelectOrderNodes('/AocOrder');
  Result := NewOrders.length;
end;

procedure TInspectionWorkOrders.Save;
begin
  Writer.Save(WOData, AttachData);
end;

function GetNodeText(ANode: IXMLDOMNode; const Name: string; const Required: Boolean): Variant;
var
  NodeToGet: IXMLDOMNode;
begin
  Result := Null;
  NodeToGet := ANode.selectSingleNode(Name);
  if (not Assigned(NodeToGet)) and Required then
    raise EGIInvalidData.CreateFmt('Missing required element %s.', [Name]);

  if Assigned(NodeToGet) then
    Result := NodeToGet.text;
end;

procedure SetNodeText(ANode: IXMLDOMNode; const Name: string; const Value: AnsiString);
var
  NodeToSet: IXMLDOMNode;
begin
  NodeToSet := ANode.selectSingleNode(Name);
  if (not Assigned(NodeToSet)) then
    raise EGIInvalidData.CreateFmt('Missing required element %s.', [Name]);
  NodeToSet.text := string(Value);
end;

function TInspectionWorkOrders.GetWorkOrderImageXML(const WOAsXml: string): string;
var
  XMLDoc: IXMLDOMDocument;
  SvcRecs: IXMLDOMNodeList;
  I: Integer;
begin
  Result := XMLHeader + WOAsXml;
  XMLDoc := ParseXml(Result);
  // Clear the <ServiceRecordImage><TiffImage> base64 data for readibility
  SvcRecs := XMLDoc.selectNodes('/AocOrder/ServiceRecordImages/ServiceRecordImage');
  for I := 0 to SvcRecs.length-1 do
    SetNodeText(SvcRecs.item[I], 'TiffImage', '');
  Result := PrettyPrintXml(XMLDoc);
end;

procedure TInspectionWorkOrders.ReprojectXYToLatLong(const X, Y: Double; out Longitude, Latitude: Double);
// This formula converts Maryland NAD27 SPCS coordinates into latitude and longitude
const
  EPSG_26785 = '+title=Maryland_NAD27_SPCS +proj=lcc +lat_1=38.3 +lat_2=39.45 '+
    '+lat_0=37.83333333333334 +lon_0=-77 +x_0=243840.4876809754 +y_0=0 '+
    '+ellps=clrk66 +datum=NAD27 +to_meter=0.3048006096012192 +no_defs';
var
  P4: PProjPJ;
  xy: TProjXY;
  LatLong: TProjUV;
begin
  P4 := _pj_init_plus_path(EPSG_26785, nil);
  if not Assigned(P4) then
    raise Exception.Create(cPjErrorInitLibrary);
  try
    xy.U := X;
    xy.V := Y;
    LatLong := _pj_inv(xy, P4);
    Latitude := LatLong.V  * RAD_TO_DEG;
    Longitude := LatLong.U * RAD_TO_DEG;
  finally
    _pj_free(P4);
  end;
end;

procedure TInspectionWorkOrders.Validate;
var
  AocOrder: IXMLDOMNode;
  OrderId: Integer;
  WONumber: string;

  procedure AddOrder;
  var
    F: TField;
    Latitude, Longitude: Double;
    Num, Num2, Street: string;
  begin
    WOData.Append;
    // populate each field from the XML node with a matching name.
    for F in WOData.Fields do
      F.Value := GetNodeText(AocOrder, F.FieldName, F.Required);

    // Fields that need special handling
    WOData.FieldByName('TextImage').AsString := GetWorkOrderImageXML(AocOrder.xml);
    if WOData.FieldByName('Latitude').IsNull then begin
      ReprojectXYToLatLong(WOData.FieldByName('X_Coordinate').AsFloat,
        WOData.FieldByName('Y_Coordinate').AsFloat, Longitude, Latitude);
      WOData.FieldByName('Latitude').AsFloat := Latitude;
      WOData.FieldByName('Longitude').AsFloat := Longitude;
    end;

    if (WOData.FieldByName('Latitude').AsFloat < -90.0) or (WOData.FieldByName('Latitude').AsFloat > 90.0) or
      (WOData.FieldByName('Longitude').AsFloat < -180.0) or (WOData.FieldByName('Longitude').AsFloat > 180.0) then begin
      raise EGIInvalidData.CreateFmt('Specified latitude longitude is not within allowed range. %f, %f',
        [WOData.FieldByName('Latitude').AsFloat, WOData.FieldByName('Longitude').AsFloat]);
    end;

    if NotEmpty(WOData.FieldByName('ServiceAddress1').AsString) then begin
      SplitAddress(WOData.FieldByName('ServiceAddress1').AsString, Num, Num2, Street);
      WOData.FieldByName('ServiceAddressStreet').Value := Street;
      if NotEmpty(Num) then
        WOData.FieldByName('ServiceAddressNumber').Value := Num;
      if NotEmpty(Num2) then
        WOData.FieldByName('ServiceAddressNumber2').Value := Num2;
    end;

    WOData.Post;
  end;

  procedure AddAttachments;
  var
    SRImages: IXMLDOMNodeList;
    SRImage: IXMLDOMNode;
    ImageIdx: Integer;
  begin
    SRImages := AocOrder.selectNodes('ServiceRecordImages/ServiceRecordImage');
    for ImageIdx := 0 to SRImages.length-1 do begin
      SRImage := SRImages.item[ImageIdx];
      AttachData.Append;
      AttachData.FieldByName('AocOrderId').Value := OrderId;
      AttachData.FieldByName('SRName').Value := GetNodeText(SRImage, 'Name', True);
      AttachData.FieldByName('PNGFileName').Value := ChangeFileExt(AttachData.FieldByName('SRName').AsString,'.PNG');
      AttachData.FieldByName('PNGFilePath').Value := IncludeTrailingPathDelimiter(AttachmentFolder.FullPathName) +
        IncludeTrailingPathDelimiter(AttachData.FieldByName('AocOrderId').AsString);
      SaveBase64DataToTempFile(AttachData.FieldByName('PNGFilePath').AsString,
        AttachData.FieldByName('PNGFileName').AsString, GetNodeText(SRImage, 'TiffImage', True));
      AttachData.Post;
    end;
  end;

var
  OrderIdx: Integer;
  XMLBytes: Integer;
  NewOrders: IXMLDOMNodeList;
begin
  XMLBytes := SizeOf(FXML);
  if OrderCount < 1 then
    raise EGIInvalidData.Create('There are no inspection work orders in the XML document.');

  if (XMLBytes > FMaxSize) then
    raise EGIDataTooBig.CreateFmt('The incoming order feed is too big. %dMB found, %dMB allowed.',
      [XMLBytes div 1024, FMaxSize div 1024]);

  if OrderCount > FMaxOrders then
    raise EGIDataTooBig.CreateFmt('The incoming order feed is too big. %d orders found, %d orders allowed.',
      [OrderCount, FMaxOrders]);

  // Empty the temp memory tables.
  WOData.Close;
  AttachData.Close;
  WOData.Open;
  AttachData.Open;
  NewOrders := SelectOrderNodes('/AocOrder');
  for OrderIdx := 0 to OrderCount-1 do begin
    try
      AocOrder := NewOrders.item[OrderIdx];
      OrderID := StrToIntDef(AocOrder.selectSingleNode('AocOrderId').text, 0);
      if OrderID < 1 then
        raise EGIInvalidData.CreateFmt('Expected AocOrderId to be an integer, but it is [%s]',
          [AocOrder.selectSingleNode('AocOrderId').text]);
      WONumber := AocOrder.selectSingleNode('WrNo').text;
      AddOrder;
      AddAttachments;
    except
      on E: Exception do begin
        E.Message := Format('Error detected. AocOrderId=[%d] WrNo=[%s] : %s',
          [OrderId, WONumber, E.Message]);
        WOData.Cancel;
        AttachData.Cancel;
        raise;
      end;
    end;
  end;
end;

procedure TInspectionWorkOrders.SaveBase64DataToTempFile(const FilePath, FileName, EncodedData: string);
//This method both decodes the incoming TIF file from Base64 and calls another
// method which converts and saves it to a PNG file.
var
  Bytes: TBytesStream;
begin
  Bytes := TBytesStream.Create(DecodeBase64(AnsiString(EncodedData)));
  try
    try
      if Assigned(Bytes) then
        OdForceDirectories(FilePath);
        if not ConvertTIFStreamToPNGFile(Bytes, FilePath + FileName) then
          raise Exception.CreateFmt('Error saving attachment %s%s.', [FilePath, FileName]);
    finally
      FreeAndNil(Bytes);
    end;
  except
    on E: Exception do begin
      E.Message := Format('Error saving attachment %s%s. %s', [FilePath, FileName, E.Message]);
      raise;
    end;
  end;
end;

{
TStringSaver is a simple GI Work Order saver to use for testing the
TInspectionWorkOrders object. After calling Save, check .AsString for
a string dump of the GI work order data.
}

{ TStringSaver }

function TStringSaver.AsString: string;
begin
  Result := Buffer;
end;

procedure TStringSaver.Save(WorkOrders, Attachments: TDataSet);

  procedure DumpTable(T: TDataSet; SkipColNames: string='');
  var
    F: TField;
  begin
    T.First;
    try
      while not T.Eof do begin
        for F in T.Fields do begin
          if Pos(F.FieldName, SkipColNames) < 1 then
            Buffer := Buffer + F.FieldName + ': ' + F.AsString + #09;
        end;
        T.Next;
        Buffer := Buffer + sLineBreak;
      end;
    finally
      T.Close;
    end;
  end;

begin
  Assert(WorkOrders.Active, 'Work Order temp table is not active.');
  Assert(Attachments.Active, 'Attachments temp table is not active.');
  Buffer := '== WORK ORDERS ==' + sLineBreak;
  DumpTable(WorkOrders, 'TextImage');
  Buffer := Buffer + sLineBreak + '== ATTACHMENTS ==' + sLineBreak;
  DumpTable(Attachments, 'SRTiffImage');
end;

end.
