unit WGLWMu;

interface

uses System.SysUtils, System.Classes, Web.HTTPApp, Wessy, Sessy,
  Soap.InvokeRegistry, Soap.WSDLIntf, System.TypInfo, Soap.WebServExp,
  Soap.WSDLBind, Xml.XMLSchema, Soap.WSDLPub, Soap.SOAPPasInv,
  Soap.SOAPHTTPPasInv, Soap.SOAPHTTPDisp, Soap.WebBrokerSOAP;

type
  TAPIWebMod = class(TWebModule)
    HTTPSoapDispatcher: THTTPSoapDispatcher;
    HTTPSoapPascalInvoker: THTTPSoapPascalInvoker;
    WSDLHTMLPublish: TWSDLHTMLPublish;
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleCreate(Sender: TObject);
  private
    FRouter: TWebRouter;
  end;

var
  WebModuleClass: TComponentClass = TAPIWebMod;

implementation
uses
  WGLDMu, OdMiscUtils;

{$R *.dfm}

procedure TAPIWebMod.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
// TODO: We are not doing REST for this service yet, so skip this Routing and just return the WSDL.
//  FRouter.Route(Request, Response, Handled);

  WSDLHTMLPublish.ServiceInfo(Sender, Request, Response, Handled);
end;

procedure TAPIWebMod.WebModuleCreate(Sender: TObject);
begin
  FRouter := TWebRouter.Create(Self);
  FRouter.UseDataModuleMaker(function: IWebDataModule begin Result := TWGLDM.Create(nil) end);

  FRouter.UseSessionManager(TSessy.Create('',  // I think it means just this hostname
     '/', // the application lives at the root of the URL space
     'QMWGLSESS', // A cookie name specific to this application
     '876D7880-866E-4FD0-A79B-F211CC681833}', // secret key, DIFFERENT for each app
     30*60,  // 30 minute session expiration
     Self));
end;

end.
