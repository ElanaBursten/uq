; -- QMSWGLerver.iss --
; This script ceates a virtual dir and installs QMWGLService.dll
; Use Inno Setup 5.4.2+
#define Version GetEnv("VERSION")
#define MyAppName "Q Manager WGL SOAP Server"
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName={#MyAppName} 
AppVerName={#MyAppName} {#Version} 
UninstallDisplayName = {#MyAppName} {#Version} {code:GetVirtualDirNameFromInstallDir|''}
;N.B. AppId must be unique (per Virtual Dir) for the uninstallers to work correctly.
AppID={#MyAppName} {code:GetVirtualDirAlias|''}
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\{#MyAppName}
DefaultGroupName={#MyAppName}
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/
AppMutex=UtiliQuestQMWGLServer
AppVersion={#Version}{#GitCommitIDWithDash}
OutputDir=..\build
OutputBaseFilename=QMWGLServerSetup-{#Version}{#GitCommitIDWithDash}
DisableProgramGroupPage=yes
PrivilegesRequired=none
OutputManifestFile=QMWGLServerSetup-Manifest.txt
SetupLogging=yes
UsePreviousLanguage=no

[Files]
Source: QMWGLService.dll; DestDir: {app}; BeforeInstall: DoBeforeInstall; Flags: ignoreversion
Source: SampleQMServer.ini; DestDir: {app}; DestName: QMServer.ini; Flags: onlyifdoesntexist uninsneveruninstall
;Source: ..\build\QM_email_log_errors.exe; DestDir: {app}; Flags: ignoreversion
;Source: ..\tools\email_log_errors\Sample_QM_email_log_errors.ini; DestDir: {app}; DestName: QM_email_log_errors.ini; Flags: onlyifdoesntexist uninsneveruninstall

;DLL needed in the app directory for coordinate reprojection support
Source: "..\thirdparty\Proj4\proj447.dll"; DestDir: "{app}"; 

; Delphi XE3 Standard
Source: "..\lib\rtl170.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcl170.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclx170.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\dbrtl170.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcldb170.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclimg170.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\xmlrtl170.bpl"; DestDir: "{sys}"; Flags: sharedfile
;FireDAC for Delphi XE3
Source: ..\lib\AnyDAC_ComI_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_Comp_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_GUIxForms_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_Phys_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_PhysMSSQL_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_PhysODBC_D17.bpl; DestDir: {sys}; Flags: sharedfile 
Source: ..\lib\AnyDAC_PhysSQLite_D17.bpl; DestDir: {sys}; Flags: sharedfile 

[Tasks]
Name: ShowTestPage; Description: "Show WSDL web page?"; GroupDescription: "Post Install Checks"; Flags: unchecked

[Run]
Filename: "http://127.0.0.1/{code:GetVirtualDirAlias}/QMWGLService.dll/wsdl/IUQWGLService"; Flags: shellexec runasoriginaluser; Check: IsTaskSelected('ShowTestPage')

[UninstallRun]
Filename: "iisreset.exe"; Flags: waituntilterminated; Check: IsWinXP

[Code]
{--- IIS ---}
const
  IISServerName = 'localhost';
  IISServerNumber = '1';
  MSXMLDOMDocument3GUID = '{f5078f32-c551-11d3-89b9-0000f81fe221}';
  DefaultSite = 'Default Web Site';
  AppVersionToInstall = '{#SetupSetting("AppVersion")}';

var
  VirtualDirAliasPage: TInputQueryWizardPage;

procedure ShowError(Msg: string);
begin
  MsgBox(Msg, mbError, MB_OK);
end;  

{Version differences in IIS:
http://msdn.microsoft.com/en-us/library/ms524539(v=VS.90).aspx}
function GetIISVersion: Integer;
var
  MajorVersion: Cardinal;
begin
  Result := -1; //then no IIS
  if RegQueryDWordValue(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\InetStp', 'MajorVersion', MajorVersion) then
    Result := MajorVersion; 
end;

function IsWinXP: Boolean;
begin
  Result := (GetIISVersion < 6);
end;

function GetWMI: Variant;
begin
  // Connect to WMI
  Result := CreateOleObject('WbemScripting.SWbemLocator');
end;

function GetIISProvider(WMILocatorObj: Variant; ServerName: string): Variant;
begin
  try
    //Connect to the IIS namespace
    Result := WMILocatorObj.ConnectServer(ServerName, 'root/MicrosoftIISv2');
  except
    RaiseException('Please install IIS 6 WMI and scripting compatibility in the control panel under ' + 
      'Programs and Features, Windows Features, IIS 6 Management Compatibility.' #13#13 '(Error ''' + 
      GetExceptionMessage + ''' occurred)');
  end;
end;

procedure RecycleAppPool(ServerName, AppPoolName: string);
var
  ProviderObj, AppPool: Variant;
begin
  WizardForm.StatusLabel.Caption := 'Recycling ' + AppPoolName + ' application pool';
  ProviderObj := GetIISProvider(GetWMI, ServerName);
  try
    //Connect to the application pools node
    AppPool := ProviderObj.Get('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
    AppPool.Recycle;
  except
    ShowError('Application Pool ' + AppPoolName + ' could not be recycled. ' + 
      'Try using IIS Manager to recycle it once the install finishes.' #13#13 '(Error ''' + 
      GetExceptionMessage + ''' occurred)');
  end;
end;

procedure CreateAppPool(ServerName, AppPoolName: string);
var
  ProviderObj, NewAppPool : variant;
begin
  WizardForm.StatusLabel.Caption := 'Creating ' + AppPoolName + ' application pool';
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    // Create the new application pool.
    NewAppPool := ProviderObj.Get('IIsApplicationPoolSetting').SpawnInstance_();
    NewAppPool.Name := 'W3SVC/AppPools/' + AppPoolName;
    NewAppPool.Enable32BitAppOnWin64 := True;
    NewAppPool.Put_();
  except
    RaiseException('Failed to create application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
  
  try
    // Verify creation.
    NewAppPool := ProviderObj.Get('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
  except
    RaiseException('Failed verifying creation of application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure CreateVirDir(ServerName, MetabasePath, VirDirName, PhysicalPath: string);
var
  ProviderObj, NewVirDir : variant;
begin
  WizardForm.StatusLabel.Caption := 'Creating ' + VirDirName + ' virtual directory on ' + ServerName;
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);
    NewVirDir := ProviderObj.Get('IIsWebVirtualDirSetting').SpawnInstance_();
    NewVirDir.Name := MetabasePath + '/' + VirDirName;
    NewVirDir.Path := PhysicalPath;
    NewVirDir.AppFriendlyName := VirDirName;
    NewVirDir.AccessScript := True;
    NewVirDir.AccessExecute := True;
    NewVirDir.AccessSource := True;
    NewVirDir.Put_();
  except
    RaiseException('Failed to create virtual directory.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure AssignVirDirToAppPool(ServerName, MetabasePath, AppPoolName: string);
var
  ProviderObj, VirDir : variant;
begin
  //  serverName is of the form "<servername>", for example "Localhost" 
  //  metabasePath is of the form "W3SVC/<siteID>/Root[/<vDir>]", for example "W3SVC/1/ROOT/MyVDir" 
  //  appPoolName is of the form "<name>", for example, "MyAppPool"
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    VirDir := ProviderObj.Get('IIsWebVirtualDirSetting="' + MetabasePath + '"');                                                                                                       
    VirDir.AppPoolID := AppPoolName;
    VirDir.Put_();
  except
    RaiseException('Failed to assign virtual directory to application pool.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure InstallISAPIandCGIExtensions;
var
  IIS, WebSite: Variant;
begin
  WizardForm.StatusLabel.Caption := 'Configuring IIS for Q Manager WGL Server';
  try
    // Create the main IIS COM Automation object 
    IIS := CreateOleObject('IISNamespace');
    // Connect to the IIS server 
    WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');

    //Install ISAPI and CGI extensions. MSDN reference:
    //http://msdn.microsoft.com/en-us/library/ms524567(v=vs.90).aspx
    WebSite.AddExtensionFile(ExpandConstant('{app}') + '\QMWGLService.dll', True, 'QMSRVR', True, 'Q Manager WGL Server application');
    WebSite.EnableExtensionFile(ExpandConstant('{app}') + '\QMWGLService.dll');
    WebSite.SetInfo();
  except
    RaiseException('Failed to install and enable ISAPI extensions.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

function ServerAppExists: Boolean;
begin
  Result := FileExists(ExpandConstant('{app}') + '\QMWGLService.dll');
end;

procedure CleanUp(ServerName, MetabasePath, AppPoolName: string);
var
  IIS, WebSite, ProviderObj : variant;
begin
  //Uninstall ISAPI and CGI extensions
  try
    // Create the main IIS COM Automation object
    try
      IIS := CreateOleObject('IISNamespace');
    except
      RaiseException('Please install the IIS 6 compatibility scripting feature under Programs and Features, Windows Features.' #13#13 '(Error ''' + GetExceptionMessage + ''' occurred)');
    end;
    WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');
      
    WebSite.DeleteExtensionFileRecord(ExpandConstant('{app}\QMWGLService.dll'));
    WebSite.SetInfo();
  except
    RaiseException('Failed to uninstall ISAPI extensions.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;  

  //  serverName is of the form "<servername>", for example "Localhost" 
  //  metabasePath is of the form "W3SVC/<siteID>/Root[/<vDir>]", for example "W3SVC/1/ROOT/MyVDir" 
  //  appPoolName is of the form "<name>", for example, "MyAppPool"
  try
    ProviderObj := GetIISProvider(GetWMI, ServerName);

    //Remove the virtual dir, then the application pool
    ProviderObj.Delete('IIsWebVirtualDir="' + MetabasePath + '"' );
    ProviderObj.Delete('IIsApplicationPool="W3SVC/AppPools/' + AppPoolName + '"' );
  except
    RaiseException('Failed in CleanUp with the following exception: '#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;
end;

procedure AddIIS7Extension(const Filename: string);
begin
  //
end;

procedure CreateIIS7Website(const ApplicationPoolName, VirtualDirAliasName: string);
var
  oWebAdmin, oSite, oApp, oAppPool, oSection: Variant;
  LocatorObj: Variant;
begin
  LocatorObj := GetWMI;
  oWebAdmin := LocatorObj.ConnectServer(IISServerName, 'root\WebAdministration');
  // Delete old app and pool?  If we supported enumerators/queries, we could query the existing items
  try
    try
      oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
      oApp.Delete_;
    except
    end;
    try
      oAppPool := oWebAdmin.Get('ApplicationPool.Name=''' + ApplicationPoolName + '''')
      oAppPool.Delete_;
    except
    end;
    //Create App Pool
    oWebAdmin.Get('ApplicationPool').Create(ApplicationPoolName);
    
    //Create the application
    oWebAdmin.Get('Application').Create('/' + VirtualDirAliasName, DefaultSite, ExpandConstant('{app}'));
    //Assign the application to the app pool: http://learn.iis.net/page.aspx/163/managing-applications-and-application-pools-on-iis-70-with-wmi/
    oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
    oApp.ApplicationPool := ApplicationPoolName;
    oApp.Put_;

    oSite := oWebAdmin.Get('Site.Name='''+ DefaultSite + '''');
    //Get the ISAPI-CGI restriction section.
    oSection := oSite.GetSection('IsapiCgiRestrictionSection');
    //oSection.NotListedCgisAllowed := True;
    //oSection.NotListedIsapisAllowed := True;
    //todo
  except
    //todo
  end;
end;

procedure CleanUpIIS7Website(const ApplicationPoolName, VirtualDirAliasName: string);
var
  oWebAdmin, oSite, oApp, oAppPool, oSection: Variant;
  LocatorObj: Variant;
begin
  LocatorObj := GetWMI;
  oWebAdmin := LocatorObj.ConnectServer(IISServerName, 'root\WebAdministration');
  oSite := oWebAdmin.Get('Site.Name=''' + DefaultSite + '''');

  try
    //Get the ISAPI-CGI restriction section.
    oSite.GetSection('IsapiCgiRestrictionSection', oSection);
    //oSection.Remove();
  except //todo
  end;

  //Delete an application
  try
    oApp := oWebAdmin.Get('Application.SiteName=''' + DefaultSite + ''',Path=''/' + VirtualDirAliasName + '''');
    oApp.Delete_;
  except //todo
  end;

  //Delete an application pool
  try
    oAppPool := oWebAdmin.Get('ApplicationPool.Name=''' + ApplicationPoolName + '''');
    oAppPool.Delete_;
  except //todo
  end;
end;

procedure CreateWebSite; 
//For >=IIS6, create a virtual dir, app pool, and install ISAPI and CGI extensions
//MSDN info: http://msdn.microsoft.com/en-us/library/ms525832(v=VS.90).aspx
var
  VirtualDirAliasName, ApplicationPoolName: string;
begin
  VirtualDirAliasName := VirtualDirAliasPage.Values[0];
  ApplicationPoolName := VirtualDirAliasName + 'Pool';

  //if GetIISVersion >= 7 then begin
  //  CreateIIS7WebSite(ApplicationPoolName, VirtualDirAliasName);
  //end
  //else
  begin
    CreateAppPool(IISServerName, ApplicationPoolName); 
    CreateVirDir(IISServerName, 'W3SVC/1/Root', VirtualDirAliasName, ExpandConstant('{app}')); 
    AssignVirDirToAppPool(IISServerName, 'W3SVC/1/ROOT/' + VirtualDirAliasName, ApplicationPoolName);
    InstallISAPIandCGIExtensions;
  end;
end;

function RemoveTrailingBackslash(Path: string): string;
begin
  Result := Path;
  if Result[Length(Result)] = '\' then 
    Delete(Result, Length(Result), 1); 
end;

function ExtractPathFromFilename(FileName: string): string;
begin
  Result := FileName;
  while Result[Length(Result)] <> '\' do //remove exe name from string to next backslash
    Delete(Result, Length(Result), 1);
end;

function GetVirtualDirNameFromInstallDir(Param: string): string;
begin
  Result := ExpandConstant('{app}'); 
  Result := ExtractFileName(RemoveTrailingBackslash( Result ));//gets the folder the exe lives in, which is what we want - the name of the virtual dir
end;

procedure DeleteOldServer;
var
  OldSvr: string;
  Tries: Integer;
begin
  OldSvr := ExpandConstant('{app}') + '\QMWGLService.dll.old';
  if not FileExists(OldSvr) then
    Exit;
    
  WizardForm.StatusLabel.Caption := 'Deleting temporary QMWGLService.dll.old file';
  Tries := 1;
  repeat   
    DelayDeleteFile(OldSvr, 40);
    Tries := Tries + 1;
    WizardForm.StatusLabel.Caption := 'Deleting temporary QMWGLService.dll.old file - attempt ' + IntToStr(Tries) +  ' of 10';
  until (not FileExists(OldSvr)) or (Tries > 10); 
  if FileExists(OldSvr) then
    ShowError('Unable to delete ' + OldSvr + '. '#13#13'The ' +
      'file may still be in use. Try deleting it again once the installation finishes.');
end;

procedure RenameServer;
begin
  DeleteOldServer;
  if not RenameFile(ExpandConstant('{app}') + '\QMWGLService.dll', ExpandConstant('{app}') + '\QMWGLService.dll.old') then
    RaiseException('Unable to rename ' + ExpandConstant('{app}') + '\QMWGLService.dll to QMWGLService.dll.old.');
end;

procedure ManageOldIISWebSite(Creating: Boolean); 
//For < IIS6, create or remove virtual directory
var
  VirtualDirAliasName: string;
  IIS,WebSite, WebServer, WebRoot, VDir: variant;
begin
  if Creating then
    VirtualDirAliasName := VirtualDirAliasPage.Values[0]
  else begin
  //Note: the virtual directory name is obtained from the folder name the uninstaller is run from. 
  //(It cannot be obtained at the time the uninstaller is built since the virtual dir is user defined.)
    VirtualDirAliasName := GetVirtualDirNameFromInstallDir('');
  end;
  
  //Create the main IIS COM Automation object 
  try
    IIS := CreateOleObject('IISNamespace');
  except
    RaiseException('Please install Microsoft IIS first.'#13#13'(Error ''' + GetExceptionMessage + ''' occurred)');
  end;

  //Connect to the IIS server 
  WebSite := IIS.GetObject('IIsWebService', IISServerName + '/w3svc');
  WebServer := WebSite.GetObject('IIsWebServer', IISServerNumber);
  WebRoot := WebServer.GetObject('IIsWebVirtualDir', 'Root');

  //(Re)create/remove a virtual dir 
  try
    WebRoot.Delete('IIsWebVirtualDir', VirtualDirAliasName);
    WebRoot.SetInfo();
  except
  end;

  if Creating then begin
    VDir := WebRoot.Create('IIsWebVirtualDir', VirtualDirAliasName);
    VDir.AccessRead := True;
    VDir.AccessScript := True;
    VDir.AccessExecute := True;
    VDir.AccessSource := True;
    VDir.AppFriendlyName := VirtualDirAliasName;
    VDir.Path := ExpandConstant('{app}');
    VDir.SetInfo();
    VDir.AppCreate2(1); // Pass in Application Protection (low - IIS process=0 / medium - pooled=2 / high - isolated=1)
    VDir.SetInfo();
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
  InstalledDLLVersion : string;
begin
  if CurStep = ssInstall then begin
    GetVersionNumbersString(ExpandConstant('{app}') + '\QMWGLService.dll', InstalledDLLVersion);
    if ServerAppExists then //assume that website is installed correctly
      if MsgBox('The server version ' + InstalledDLLVersion + 
        ' is already installed in this directory, are you ' + 
        'sure you want to overwrite/update it with version '+ AppVersionToInstall + 
        ' ?', mbConfirmation, MB_YESNO) = IDNO then
        WizardForm.Close; 
  end else if (CurStep = ssPostInstall) and (GetIISVersion >= 6) then begin   
    //Recycle the application pool so the DLL is replaced.
    RecycleAppPool(IISServerName, VirtualDirAliasPage.Values[0] + 'Pool');
    DeleteOldServer;
  end;
end;

procedure DoBeforeInstall;
begin
  if ServerAppExists then begin//assume that website is installed correctly
    if GetIISVersion >= 6 then //older than IIS 6 not supported;  
      RenameServer;
  end
  else begin
    if GetIISVersion < 6 then
      ManageOldIISWebSite(True)
    else
      CreateWebSite;
  end;
  WizardForm.StatusLabel.Caption := 'Installing files';
end;

procedure CreateCustomPages;
var
  DefaultVirDir: string;
begin
  VirtualDirAliasPage := CreateInputQueryPage(wpWelcome, 'Virtual Directory Setup',  
    'Virtual Directory Alias', 'Please specify the virtual directory alias, then click Next.');
  // Add items (False means it's not a password edit)
  VirtualDirAliasPage.Add('Alias:', False);
  DefaultVirDir := GetPreviousData('Alias', '');
  if DefaultVirDir = '' then
    DefaultVirDir := 'QMWGLServerApp'; 
  VirtualDirAliasPage.Values[0] := DefaultVirDir;
end;

procedure RegisterPreviousData(PreviousDataKey: Integer);
begin
  SetPreviousData(PreviousDataKey, 'Alias', VirtualDirAliasPage.Values[0]);
end;

function GetVirtualDirAlias(Param: string): string;
begin
  if Assigned(VirtualDirAliasPage) then 
    Result := VirtualDirAliasPage.Values[0]
  else//it has not been collected from the custom page yet
    Result := '';
end;

//Hijack the the users directory selection and append the virtual directory before it is set as the app constant
function NextButtonClick(CurPage: Integer): Boolean;
begin
  if CurPage = wpReady then
    WizardForm.DirEdit.Text := WizardForm.DirEdit.Text + '\' + GetVirtualDirAlias('');

  Result := True;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo,
  MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: string): string;
var
  S: string;
begin
  S := '';
  S := S + 'Virtual Directory Alias: ' + NewLine;
  S := S + Space + VirtualDirAliasPage.Values[0] + NewLine;
  S := S + 'Virtual Directory Path: ' + NewLine; 
  S := S + Space + ExpandConstant('{app}') + '\' + VirtualDirAliasPage.Values[0] + NewLine;
  S := S + MemoGroupInfo;
  Result := S;
end;

procedure InitializeWizard();
begin
  CreateCustomPages;
end;

function InitializeUninstall: Boolean;
var
  VirDirAliasName: string;
begin
  //Note: the virtual directory name is obtained from the folder name the uninstaller is run from. 
  //(It cannot be obtained at the time the uninstaller is built since the virtual dir is user defined.)
  VirDirAliasName := GetVirtualDirNameFromInstallDir('');

  if GetIISVersion >= 6 then begin
    if MsgBox('Do you want to delete the application pool, virtual directory and ' +
      'uninstall ISAPI extensions? The application pool name is: ' + VirDirAliasName + 
      'Pool. The virtual dir alias name is: ' + VirDirAliasName, mbConfirmation, MB_YESNO) = IDYES 
    then begin
      //if GetIISVersion >= 7 then
      //  CleanUpIIS7Website(VirDirAliasName + 'Pool', VirDirAliasName)
      //else
        CleanUp(IISServerName, 'W3SVC/1/ROOT/' + VirDirAliasName, VirDirAliasName + 'Pool'); 
    end;
  end
  else begin
    ManageOldIISWebSite(False);
  end;

  Result := True;
end;
