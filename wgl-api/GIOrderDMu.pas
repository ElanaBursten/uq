unit GIOrderDMu;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,
  IniFiles, FDUtils, GIOrder, WebServiceAttachmentDMu, QMConst, OperationTracking,
  ThreadSafeLoggerU;

type
  TDbOrderSaver = class(TInterfacedObject, IInspectionOrderWriter)
  private
    FConn: TFDCustomConnection;
    FWO: TFDCommand;
    FGI: TFDCommand;
    FFindWO: TFDQuery;
    FOp: TOpInProgress;
    function WorkOrderExists(const WONumber, ClientWONumber: string; out WoId: Integer): Boolean;
    function GetWGLClientID: Integer;
    function GetSystemEmpID: Integer;
  public
    constructor Create(InsertWO, InsertInspect: TFDCommand; FindWorkOrder: TFDQuery; Op: TOpInProgress);
    procedure Save(WorkOrders, Attachments: TDataSet);
  end;

  TGIOrderDM = class(TDataModule)
    MSSQLDriverLink: TFDPhysMSSQLDriverLink;
    Conn: TFDConnection;
    InsertWorkOrder: TFDCommand;
    FindWorkOrder: TFDQuery;
    InsertInspection: TFDCommand;
    GetWglWorkOrderStatus: TFDStoredProc;
    WORemedy: TFDMemTable;
    WorkOrders: TFDMemTable;
    GetWGLWOStatus: TFDQuery;
    GetWGLWORemedy: TFDQuery;
    UpdtResponseSuccess: TFDCommand;
    UpdtResponseFail: TFDCommand;
    CleanUpWOResponseLog: TFDCommand;    //QMANTWO-249
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    LogDir: string;
    LogEnabled, Debug: Boolean;
    procedure ConfigureFromIni;
    function MaxIncomingOrders: Integer;
    function MaxOutgoingOrders: Integer;
    function MaxXMLByteSize: Integer;
    function GetConfigurationDataValue(const Name, Default: string): string; overload;
    function GetConfigurationDataValue(const Name: string; const Default: Integer): Integer; overload;
  public
    InstanceID: Integer;
    Logger: TThreadSafeLogger;
    Op: TOpInProgress;
    procedure Disconnect;
    procedure CloseDataSets;
    procedure SetOpName(const OpName: string);
    procedure Connect(const WhichDB: string = 'Database');
    procedure AddGIOrdersFromXML(const XML: string);
    function GetGIOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): string;
    procedure UpdateResponseLog(const AllGood: Boolean);
    destructor Destroy; override;
  end;

const
  UploadToLocation = 'Download'; //TODO: maybe this should come from the ini file instead?

implementation

uses XmlIntf, XmlDoc, DCPbase64, OdMiscUtils, OdIsoDates;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TGIOrderDM }

procedure TGIOrderDM.CloseDataSets;
begin
  // close any shared datasets that might be open
  //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
  GetWGLWOStatus.Active := False;
  //KEN FUNK QMANTWO-200 END 2016-05-17
end;

procedure TGIOrderDM.Connect(const WhichDB: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(GetModuleName(HInstance)) + 'QMServer.ini');
  try
    ConnectFDConnectionWithIni(Conn, Ini, WhichDB);  //FD Migration - flag for unintentionalchanges
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TGIOrderDM.Disconnect;
begin
  Conn.Connected := False;
end;

procedure TGIOrderDM.SetOpName(const OpName: string);
begin
  Assert(Assigned(Op), 'No active operation defined.');
  Op.OpName := OpName;
end;

procedure TGIOrderDM.UpdateResponseLog(const AllGood: Boolean);
begin
  if not(Debug) then begin
    Op.Phase := 'Update Response Log.';
    if AllGood then begin
      try
        Op.Detail := 'Updating with Success.';
        UpdtResponseSuccess.Execute;
      except
        Op.Detail := 'FAILED to UPDATE Response Log.';
      end;
    end else begin
      try
        Op.Detail := 'Updating with Failure.';
        UpdtResponseFail.Execute;
      except
        Op.Detail := 'FAILED to UPDATE Response Log.';
      end;
    end;
    try                                                      //QMANTWO-249
      Op.Detail := 'Cleaning Up Response Log.';              //QMANTWO-249
      CleanUpWOResponseLog.Execute;                          //QMANTWO-249
    except                                                   //QMANTWO-249
      Op.Detail := 'FAILED to Clean Up Response Log.';       //QMANTWO-249
    end;                                                     //QMANTWO-249
  end;
end;

procedure TGIOrderDM.ConfigureFromIni;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(GetModuleName(HInstance)) + 'QMServer.ini');
  try
    LogDir := AddSlash(GetLocalAppDataFolder) + 'Q Manager\logs\';
    LogDir := AddSlash(Ini.ReadString('LogFile', 'Path', LogDir));
    LogEnabled := (Ini.ReadInteger('LogFile', 'LogicLogEnabled', 1) = 1);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TGIOrderDM.DataModuleCreate(Sender: TObject);
begin
  inherited;

  Randomize;
  InstanceID := Random(8999) + 1000;
  ConfigureFromIni;
  ForceDirectories(LogDir);
  Logger := TThreadSafeLogger.Create(LogDir, 'QMWGLServer.log', InstanceID);
  Logger.Enabled := LogEnabled;
  OperationTracking.RawLogger := Logger;

  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
end;

procedure TGIOrderDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;

destructor TGIOrderDM.Destroy;
begin
  OperationTracking.RawLogger := nil;
  FreeAndNil(Logger);
  inherited;
end;

function TGIOrderDM.GetConfigurationDataValue(const Name: string; const Default: Integer): Integer;
var
  Val: string;
begin
  Val := GetConfigurationDataValue(Name, IntToStr(Default));
  if NotEmpty(Val) then
    Result := StrToInt(Val)
  else
    Result := Default;
end;

function TGIOrderDM.GetConfigurationDataValue(const Name, Default: string): string;
const
  SQL = 'select coalesce(value, %s) value from configuration_data where name=%s';
begin
  Result := Conn.ExecSQLScalar(Format(SQL, [QuotedStr(Default), QuotedStr(Name)]));
  if IsEmpty(Result) and NotEmpty(Default) then
    Result := Default;
end;

function TGIOrderDM.MaxXMLByteSize: Integer;
begin
  Result := GetConfigurationDataValue('WGLServiceMaxMBIn', 50) * 1024;
end;

function TGIOrderDM.MaxIncomingOrders: Integer;
begin
  Result := GetConfigurationDataValue('WGLServiceMaxOrdersIn', 10);
end;

function TGIOrderDM.MaxOutgoingOrders: Integer;
begin
  Result := GetConfigurationDataValue('WGLServiceMaxOrdersOut', 10);
end;

function TGIOrderDM.GetGIOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): string;

  procedure AddNode(Parent: IXMLNode; Title, Text: string; AllowBlank: Boolean = False);
  var
    Node: IXMLNode;
  begin
    if (Trim(Text) <> '') or AllowBlank then begin
      Node := Parent.AddChild(Title);
      Node.Text := Text;
    end;
  end;

var
  Xml: IXMLDocument;
  RootNode, CurNode, ItemNode: IXMLNode;
  Params: string;
  DelimPos, SinceId, ThruId: Integer;
  OrdersSent, MaxOrders: Integer;
  SinceDate, ThruDate: TDateTime;
  done: Boolean;
begin
  Op.Phase := 'Decoding request params';
  Debug := False;
  done := False;
  SinceDate := 0;
  if IsEmpty(string(Since)) then begin
    done := True;
  end else begin
    if (UpperCase(Copy(String(Since),1,5)) = 'DEBUG') then begin
      Debug := True;
      if (Length(string(Since)) = 5) then begin
        done := True;
      end else begin  
        Params := Base64DecodeStr(Copy(string(Since),6,Length(string(Since))));  // date;wo_id
      end;
    end else begin
      Params := Base64DecodeStr(string(Since));  // date;wo_id
    end;
  end;
  if not(done) then begin
    DelimPos := Pos(';', Params);
    SinceDate := IsoStrToDateTimeDef(Copy(Params, 1, DelimPos - 1), -1);
    SinceId := StrToIntDef(Copy(Params, DelimPos + 1, Length(Params)), -1);
    if (SinceDate < 0) or (SinceId < 0) then
      raise EGIException.CreateFmt('Invalid request parameter. (%s)', [Since]);
  end;

  Op.Phase := 'Setting params';
  ThruDate := Now;
  ThruId := 0;

  OrdersSent := 0;
  MaxOrders := MaxOutgoingOrders;
  
  //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
  //GetWglWorkOrderStatus.Params.ParamValues['@SinceDate'] := SinceDate;
  //GetWglWorkOrderStatus.Params.ParamValues['@SinceID'] := SinceId;
  GetWGLWOStatus.Params.ParamByName('MAXORDERS').AsInteger := MaxOrders;
  //KEN FUNK QMANTWO-200 END 2016-05-17
  
  Op.Phase := 'Get updated work orders';
  if SinceDate = 0 then
    Op.Detail := 'All updated work orders'
  else
    Op.Detail := 'Updated since ' + IsoDateTimeToStr(SinceDate) + ' Value Ignored.';
  //KEN FUNK QMANTWO-200 BEGIN 2016-05-17    
  //GetWglWorkOrderStatus.Open;
  GetWGLWOStatus.Active := True;
  GetWGLWORemedy.Active := True;
  //KEN FUNK QMANTWO-200 END 2016-05-17
  
  try
    //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
    //WorkOrders.Data := GetWGLWorkOrderStatus.Data;
    //LoadNextRecordset(GetWglWorkOrderStatus, WORemedy);
    WorkOrders.Data := GetWGLWOStatus.Data;
    WORemedy.Data := GetWGLWORemedy.Data;
    //KEN FUNK QMANTWO-200 END 2016-05-17
    
    Op.Phase := 'Create response XML';
    Xml := NewXMLDocument;
    Xml.Encoding := 'utf-8';
    Xml.Options := [doNodeAutoIndent];
    RootNode := Xml.AddChild('AOCOrdersUpdates');
    //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
    //while not WorkOrders.Eof and (OrdersSent < MaxOrders) do begin
    while not WorkOrders.Eof do begin                             //QMANTWO-249
    //KEN FUNK QMANTWO-200 END 2016-05-17
      Op.Detail := 'Adding response ' + IntToStr(OrdersSent+1) + ' of ' +
        IntToStr(WorkOrders.RecordCount);
      CurNode := RootNode.AddChild('AOCOrdersUpdate');
      AddNode(CurNode, 'AOCOrderId', WorkOrders.FieldByName('wo_number').AsString);
      AddNode(CurNode, 'WrNo', WorkOrders.FieldByName('client_wo_number').AsString, True);
      AddNode(CurNode, 'UQId', WorkOrders.FieldByName('wo_id').AsString);
      AddNode(CurNode, 'MeterNumber', WorkOrders.FieldByName('actual_meter_number').AsString, True);
      AddNode(CurNode, 'MeterLocation', WorkOrders.FieldByName('meter_loc_desc').AsString);
      AddNode(CurNode, 'BuildingClassification', WorkOrders.FieldByName('building_type_desc').AsString);
      AddNode(CurNode, 'Crew', WorkOrders.FieldByName('emp_number').AsString);
      AddNode(CurNode, 'CompletionCode', WorkOrders.FieldByName('status').AsString);
      AddNode(CurNode, 'CGACount', WorkOrders.FieldByName('cga_visits').AsString);
      AddNode(CurNode, 'DateOfVisit', IsoDateTimeToStr(WorkOrders.FieldByName('status_date').AsDateTime));
      AddNode(CurNode, 'MapStatus', WorkOrders.FieldByName('map_status_desc').AsString);
      AddNode(CurNode, 'MercuryRegulator', WorkOrders.FieldByName('mercury_regulator').AsString);
      AddNode(CurNode, 'OrderNumber', WorkOrders.FieldByName('alert_order_number').AsString);
      AddNode(CurNode, 'GasLight', WorkOrders.FieldByName('gas_light').AsString);

      CurNode := CurNode.AddChild('RemediationWork');
      WORemedy.Filter := 'wo_id=' + WorkOrders.FieldByName('wo_id').AsString;
      WORemedy.Filtered := True;
      try
        WORemedy.First;
        while not WORemedy.Eof do begin
          ItemNode := CurNode.AddChild('Item');
          AddNode(ItemNode, 'Code', WORemedy.FieldByName('work_type').AsString);
          AddNode(ItemNode, 'Flag', WORemedy.FieldByName('flag_color').AsString);
          WORemedy.Next;
        end;
      finally
        WORemedy.Filtered := False;
      end;
      ThruDate := WorkOrders.FieldByName('inspection_change_date').AsDateTime;
      ThruId := WorkOrders.FieldByName('wo_id').AsInteger;
      Inc(OrdersSent);
      WorkOrders.Next;
    end;

    Result := Xml.XML.Text;
    SentThru := AnsiString(Base64EncodeStr(IsoDateTimeToStr(ThruDate) + ';' + IntToStr(ThruId)));
  finally
    WORemedy.Close;
    WorkOrders.Close;
    //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
    //GetWglWorkOrderStatus.Close;
    GetWGLWORemedy.Active := False;
    //GetWGLWOStatus.Active := False; //KEEP OPEN FOR UPDATE FROM TEMP TABLE
    //KEN FUNK QMANTWO-200 END 2016-05-17
  end;
end;

procedure TGIOrderDM.AddGIOrdersFromXML(const XML: string);
var
  GIData: TInspectionWorkOrders;
  Writer: IInspectionOrderWriter;
begin
  Assert(XML <> '', 'XML cannot be blank');

  Op.Phase := 'Create DB writer';
  Writer := TDbOrderSaver.Create(InsertWorkOrder, InsertInspection, FindWorkOrder, Op);
  Op.Phase := 'Parse incoming XML doc';
  GIData := TInspectionWorkOrders.CreateFromXML(XML, MaxIncomingOrders, MaxXMLByteSize, Writer);
  try
    Op.Phase := 'Validate work orders';
    GIData.Validate;
    Op.Phase := 'Save work orders';
    GIData.Save;
  finally
    FreeAndNil(GIData);
  end;
end;

{ TDbOrderSaver }

constructor TDbOrderSaver.Create(InsertWO, InsertInspect: TFDCommand; FindWorkOrder: TFDQuery; Op: TOpInProgress);
begin
  Assert(Assigned(InsertWO), 'InsertWO Undefined');
  Assert(Assigned(InsertInspect), 'InsertInspect Undefined');
  Assert(Assigned(Op), 'No active operation.');

  FOp := Op;
  FWO := InsertWO;
  FGI := InsertInspect;
  FFindWO := FindWorkOrder;
  FConn := InsertWO.Connection;
end;

function TDbOrderSaver.WorkOrderExists(const WONumber, ClientWONumber: string; out WoId: Integer): Boolean;
begin
  FOp.Detail := 'Check for existing order';
  WoId := 0;
  FFindWO.ParamByName('wo_source').Value := 'WGL';
  FFindWO.ParamByName('wo_number').Value := WONumber;
  FFindWO.ParamByName('client_wo_number').Value := ClientWONumber;
  FFindWO.Open;
  try
    Result := not FFindWO.Eof;
    if Result then
      WoId := FFindWO.FieldByName('wo_id').AsInteger;
  finally
    FFindWO.Close;
  end;
end;

function TDbOrderSaver.GetWGLClientID: Integer;
const
  SQL = 'SELECT coalesce(client_id, -1) FROM client WHERE oc_code = ''WGLINSP''';
begin
  FOp.Detail := 'Get WGL Client ID';
  Result := FConn.ExecSQLScalar(SQL);
  if Result < 0 then
    raise EGIException.Create('Cannot find the WGLINSP client. ' +
      'Use QManagerAdmin to confirm it exists in the Clients table.');
end;

function TDbOrderSaver.GetSystemEmpID: Integer; //todo refactor to this, above and similar methods
const
  SQL = 'select value from configuration_data where name = ''SystemUserEmpID''';
begin
  FOp.Detail := 'Get SystemUserEmpID';
  Result := FConn.ExecSQLScalar(SQL);
  if Result < 0 then
    raise EGIException.Create('Cannot find the system user. ' +
      'Confirm the SystemUserEmpID row exists in the Configuration Data.');
end;

procedure TDbOrderSaver.Save(WorkOrders, Attachments: TDataSet);
var
  WGLClientID: Integer;
  NewWOID: Integer;
  FServerAttachment: TServerAttachment;
  FLocationID: Integer;

  function AddWorkOrder: Integer;
  begin
    FOp.Detail := 'Add work order';
    Assert(WGLClientID > 0, 'Invalid WGLClientID');

    FWO.ParamByName('wo_source').Value := 'WGL';
    FWO.ParamByName('kind').Value := 'NORMAL';
    FWO.ParamByName('status').Value := '-P';
    FWO.ParamByName('closed').Value := False;
    FWO.ParamByName('parsed_ok').Value := True;
    FWO.ParamByName('client_id').Value := WGLClientID;
    FWO.ParamByName('source_sent_attachment').Value := (Attachments.RecordCount > 0);

    FWO.ParamByName('wo_number').Value := WorkOrders.FieldByName('AocOrderId').AsString;
    FWO.ParamByName('client_wo_number').Value := WorkOrders.FieldByName('WrNo').AsString;
    FWO.ParamByName('map_ref').Value := WorkOrders.FieldByName('QuadMap').Value;
    FWO.ParamByName('caller_name').Value := WorkOrders.FieldByName('ContactName').Value;
    FWO.ParamByName('caller_phone').Value := WorkOrders.FieldByName('ContactPhone').Value;
    FWO.ParamByName('caller_altphone').Value := WorkOrders.FieldByName('Phone2').Value;
    FWO.ParamByName('work_address_number').Value := WorkOrders.FieldByName('ServiceAddressNumber').Value;
    FWO.ParamByName('work_address_number_2').Value := WorkOrders.FieldByName('ServiceAddressNumber2').Value;
    FWO.ParamByName('work_address_street').Value := WorkOrders.FieldByName('ServiceAddressStreet').Value;
    FWO.ParamByName('work_county').Value := WorkOrders.FieldByName('County').Value;
    FWO.ParamByName('work_city').Value := WorkOrders.FieldByName('City').Value;
    FWO.ParamByName('work_state').Value := WorkOrders.FieldByName('State').Value;
    FWO.ParamByName('work_zip').Value := WorkOrders.FieldByName('ZipCode').Value;
    FWO.ParamByName('work_lat').Value := WorkOrders.FieldByName('Latitude').Value;
    FWO.ParamByName('work_long').Value := WorkOrders.FieldByName('Longitude').Value;
    FWO.ParamByName('map_x_coord').Value := WorkOrders.FieldByName('X_coordinate').Value;
    FWO.ParamByName('map_y_coord').Value := WorkOrders.FieldByName('Y_coordinate').Value;
    FWO.ParamByName('transmit_date').Value := WorkOrders.FieldByName('TransactionDate').Value;
    FWO.ParamByName('due_date').Value := (WorkOrders.FieldByName('ComplianceDueDate').AsDateTime + 60); // TODO: Make 60 configurable
    FWO.ParamByName('image').Value := WorkOrders.FieldByName('TextImage').Value;
    FWO.Execute;
    Result := FConn.ExecSQLScalar('SELECT @@IDENTITY AS ID');
  end;

  procedure AddInspection;
  begin
    FOp.Detail := 'Add work order inspection';
    FGI.ParamByName('wo_id').Value := NewWOID;
    FGI.ParamByName('account_number').Value := WorkOrders.FieldByName('AccountNumber').Value;
    FGI.ParamByName('premise_id').Value := WorkOrders.FieldByName('PremiseID').Value;
    FGI.ParamByName('meter_number').Value := WorkOrders.FieldByName('MeterNumber').Value;
    FGI.ParamByName('compliance_due_date').Value := WorkOrders.FieldByName('ComplianceDueDate').Value;

    FGI.Execute;
  end;

  procedure AddVersion;
  const
    InsertWOVersionSQL = 'INSERT INTO work_order_version (wo_id, wo_number, ' +
      'transmit_date, processed_date, arrival_date, image, wo_source) ' +
      'SELECT wo_id, wo_number, transmit_date, GetDate(), GetDate(), image, ' +
      'wo_source FROM work_order WHERE wo_id = ';
  begin
    FOp.Detail := 'Add work order version';
    FConn.ExecSQL(InsertWOVersionSQL + IntToStr(NewWOID));
  end;

  procedure AddAttachments;
  var
    UploadableFile: string;
  begin
    FOp.Detail := 'Add work order attachments';
    Attachments.First;
    while not Attachments.Eof do begin
      FOp.Detail := 'Add work order attachment ' +
        IntToStr(Attachments.RecNo) + ' of ' + IntToStr(Attachments.RecordCount);
      if not FileExists(Attachments.FieldByName('PNGFilePath').AsString + Attachments.FieldByName('PNGFileName').AsString) then
        raise Exception.CreateFmt('Attachment file %s does not exist.', [Attachments.FieldByName('PNGFilePath').AsString + Attachments.FieldByName('PNGFileName').AsString]);
      FServerAttachment.AttachmentFolder := Attachments.FieldByName('PNGFilePath').Value;
      UploadableFile := FServerAttachment.PrepareFileForAttachment(qmftWorkOrder, NewWOID, Attachments.FieldByName('PNGFilePath').AsString + Attachments.FieldByName('PNGFileName').AsString, CustomerAttachmentSource);
      FServerAttachment.UploadAttachment(FLocationID, qmftWorkOrder, NewWOID, UploadableFile);
      FServerAttachment.AttachUploadedFileToRecord(qmftWorkOrder, NewWOID, Attachments.FieldByName('PNGFileName').AsString, UploadableFile, '', CustomerAttachmentSource);
      Attachments.Next;
    end;
  end;

var
  ExistingWoId: Integer;
  SystemEmpID: Integer;
  NewWOCount: Integer;
begin
  NewWOCount := 0;
  FConn.StartTransaction;
  try
    WGLClientID := GetWGLClientID;
    FServerAttachment := TServerAttachment.Create(FConn);
    FServerAttachment.ApplicationName := 'QMWGLService';
    SystemEmpID := GetSystemEmpID;
    FServerAttachment.AttachedByEmpID := SystemEmpID;
    FOp.EmpID := SystemEmpID;
    FLocationID := FServerAttachment.GetLocationIDForLocationName(UploadToLocation);
    try
      WorkOrders.First;
      while not WorkOrders.Eof do begin
        Attachments.Filter := 'AocOrderID=' + WorkOrders.FieldByName('AocOrderID').AsString;
        Attachments.Filtered := True;
        FOp.Phase := 'Add work order ' +
          IntToStr(WorkOrders.RecNo) + ' of ' + IntToStr(WorkOrders.RecordCount);

        if not WorkOrderExists(WorkOrders.FieldByName('AocOrderID').Value,
          WorkOrders.FieldByName('WrNo').Value, ExistingWoId) then begin
          NewWOID := AddWorkOrder;
          AddInspection;
          AddVersion;
          AddAttachments;
          Inc(NewWOCount);
        end else begin
          // In the future, this may need to replace an existing Work Order instead of skipping it
          FOp.LogFmt('Duplicate work order detected and skipped. ' +
            'AocOrderID=%s WrNo=%s already exists (wo_id=%d)',
            [WorkOrders.FieldByName('AocOrderID').Value,
            WorkOrders.FieldByName('WrNo').Value,
            ExistingWoId]);
        end;

        WorkOrders.Next;
      end;
    finally
      FreeAndNil(FServerAttachment);
    end;
    if NewWOCount < 1 then
      raise EGIInvalidData.Create('No new work orders were added.');

    FConn.Commit;
  except
    on E: Exception do begin
      if FConn.InTransaction then
        FConn.Rollback;
      raise;
    end;
  end;
end;

end.
