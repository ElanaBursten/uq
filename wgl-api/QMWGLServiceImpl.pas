{ Invokable implementation File for TUQWGLService which implements IUQWGLService }

unit QMWGLServiceImpl;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns, SysUtils, QMWGLServiceIntf,
  GIOrderDMu;

type

  { TUQWGLService }
  TUQWGLService = class(TInvokableClass, IUQWGLService)
  private
    DM: TGIOrderDM;
    procedure GetDM;
  public
    function AddAOCOrders(const Value: AnsiString): AnsiString; stdcall;
    function GetAOCOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): AnsiString; stdcall;
    destructor Destroy; override;
  end;

implementation

uses OperationTracking;

function TUQWGLService.AddAOCOrders(const Value: AnsiString): AnsiString; stdcall;
begin
  GetDM;
  DM.Op := GetOperationTracker.AddOperation;
  DM.SetOpName('AddAOCOrders');

  try
    try
      DM.Op.Phase := 'Connecting to DB';
      DM.Connect;
      DM.AddGIOrdersFromXML(string(Value));
      DM.Op.Succeeded := True;
      Result := '200';
    except
      on E: Exception do begin
        Result := '500';
        DM.Op.LogFmt('Error: %s (%s: %s: %s)',
          [E.Message, DM.Op.OpName, DM.Op.Phase, DM.Op.Detail]);
      end;
    end;
  finally
    DM.CloseDataSets;
    DM.Disconnect;
    GetOperationTracker.FinishOperation(DM.Op);
  end;
end;

destructor TUQWGLService.Destroy;
begin
  FreeAndNil(DM);
  inherited;
end;

function TUQWGLService.GetAOCOrdersStatus(const Since: AnsiString; out SentThru: AnsiString): AnsiString; stdcall;
begin
  GetDM;
  DM.Op := GetOperationTracker.AddOperation;
  DM.SetOpName('GetAOCOrderStatus');

  try
    try
      DM.Op.Phase := 'Connecting to DB';
      DM.Connect('ReportingDatabase1');
      Result := AnsiString(DM.GetGIOrdersStatus(Since, SentThru));
      DM.Op.Succeeded := True;      
      //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
      DM.UpdateResponseLog(True);
      //KEN FUNK QMANTWO-200 END 2016-05-17
    except
      on E: Exception do begin
        Result := '500';
        DM.Op.LogFmt('Error: %s (%s: %s: %s)',
          [E.Message, DM.Op.OpName, DM.Op.Phase, DM.Op.Detail]);
        //KEN FUNK QMANTWO-200 BEGIN 2016-05-17
        DM.UpdateResponseLog(False);
        //KEN FUNK QMANTWO-200 END 2016-05-17
      end;
    end;
  finally
    DM.CloseDataSets;
    DM.Disconnect;
    GetOperationTracker.FinishOperation(DM.Op);
  end;
end;

procedure TUQWGLService.GetDM;
begin
  if DM = nil then
    DM := TGIOrderDM.Create(nil);

  Assert(Assigned(DM), 'Data module is undefined.');
end;

initialization
{ Invokable classes must be registered }
   InvRegistry.RegisterInvokableClass(TUQWGLService);
end.

