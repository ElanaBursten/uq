unit QMTempFolder;

interface

type
  ITemporaryFolder = interface(IInterface)
    function FullPathName: string;
  end;

function NewTemporaryFolder: ITemporaryFolder;

implementation

uses
  Windows, SysUtils, JclSysInfo, OdMiscUtils;

{ Usage:

procedure ProcThatNeedsATempFolder;
var
  TempFolder: IInterface;
begin
  TempFolder := NewTemporaryFolder;

  // do some work that needs TempFolder.FullPathName
  // TempFolder self-destructs on scope loss
end;

}

type
  TTemporaryFolder = class(TInterfacedObject, ITemporaryFolder)
  private
    FFolderPath: string;
    procedure CreateFolder;
    procedure DeleteFilesAndFolder;
  public
    constructor Create;
    destructor Destroy; override;
    function FullPathName: string;
  end;

constructor TTemporaryFolder.Create;
begin
  FFolderPath := '';
end;

destructor TTemporaryFolder.Destroy;
begin
  DeleteFilesAndFolder;
  inherited;
end;

function TTemporaryFolder.FullPathName: string;
begin
  if IsEmpty(FFolderPath) then
    CreateFolder;
  Result := FFolderPath;
end;

procedure TTemporaryFolder.CreateFolder;
var
  Dir: string;
begin
  // Make sure we have a unique dir we can write to
  Dir := AddSlash(GetWindowsTempPath) + 'Q Manager\' + MakeGUIDString;
  if DirectoryExists(Dir) then
    raise Exception.CreateFmt('The selected temporary folder %s already exists', [Dir]);

  OdForceDirectories(Dir);
  if not CanWriteToDirectory(Dir) then
    raise Exception.CreateFmt('Cannot write to folder %s as user %s',
      [Dir, GetLocalUserName]);

  FFolderPath := Dir;
end;

procedure TTemporaryFolder.DeleteFilesAndFolder;
begin
  if IsEmpty(FFolderPath) or (not DirectoryExists(FFolderPath)) then
    Exit;

  Assert(StrContains('{', FFolderPath) and StrContains('}', FFolderPath),
    'Temporary folder name must contain a {GUID}');
  DeleteDirectory(FFolderPath);
  if DirectoryExists(FFolderPath) then
    raise Exception.CreateFmt('Cannot remove temporary folder %s.', [FFolderPath]);
end;

function NewTemporaryFolder: ITemporaryFolder;
begin
  Result := TTemporaryFolder.Create;
end;

end.

