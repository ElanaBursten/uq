object OdTimeRangeSelectFrame: TOdTimeRangeSelectFrame
  Left = 0
  Top = 0
  Width = 248
  Height = 90
  TabOrder = 0
  object FromLabel: TLabel
    Left = 8
    Top = 12
    Width = 24
    Height = 13
    Caption = 'From'
  end
  object ToLabel: TLabel
    Left = 8
    Top = 36
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object DatesLabel: TLabel
    Left = 8
    Top = 64
    Width = 28
    Height = 13
    Caption = 'Dates'
  end
  object DatesComboBox: TComboBox
    Left = 44
    Top = 60
    Width = 197
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    OnChange = DatesComboBoxChange
  end
  object FromTimeEdit: TDateTimePicker
    Left = 138
    Top = 8
    Width = 73
    Height = 21
    Date = 40179.000000000000000000
    Format = 'hh:mm tt'
    Time = 40179.000000000000000000
    Kind = dtkTime
    TabOrder = 1
    OnKeyDown = ToTimeEditKeyDown
  end
  object ToTimeEdit: TDateTimePicker
    Left = 138
    Top = 34
    Width = 73
    Height = 21
    Date = 40179.000000000000000000
    Format = 'hh:mm tt'
    Time = 40179.000000000000000000
    Kind = dtkTime
    TabOrder = 3
    OnKeyDown = ToTimeEditKeyDown
  end
  object FromDateEdit: TcxDateEdit
    Left = 44
    Top = 8
    EditValue = 40179d
    Properties.SaveTime = False
    Properties.ShowTime = False
    Properties.OnEditValueChanged = FromDateEditChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 85
  end
  object ToDateEdit: TcxDateEdit
    Left = 44
    Top = 34
    EditValue = 40179d
    Properties.SaveTime = False
    Properties.ShowTime = False
    Properties.OnEditValueChanged = ToDateEditChange
    Properties.OnValidate = ToDateEditPropertiesValidate
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 2
    Width = 85
  end
end
