unit OdSQLBuilder;

// Creates a SQL where clause from an arbitrarty list
// of filter conditions such as some search controls

interface

uses
  SysUtils, Classes, Variants;

type
  TOdSqlBuilder = class(TObject)
  private
    FWhereClause: string;
    FFilterCount: Integer;
    FDateFormat: string;
    FDateTimeFormat: string;
    FNakedBooleanCompare: Boolean;
    function QuoteString(const Term: string): string;
    function HasIllegalChars(const Term: string): Boolean;
  protected
    FQuoteChar: Char;
    FLikeChar: Char;
    function ConvertQuotes(const Term: string): string;
    function DoFullTextSearch(FieldName: string): Boolean;
  public
    FullTextSearchColumns: TStringList;
    TableAlias: string;
    constructor Create;
    destructor Destroy; override;
    procedure Reset;
    procedure AddWhereCondition(const Condition: string); overload;
    procedure AddWhereConditionFmt(const Condition: string; const Args: array of const); overload;
    procedure AddLookupKeywordSearch(const LookupTable, ConstraintField, FieldName, SearchString: string);
    procedure AddKeywordSearch(const FieldName, SearchString: string);
    procedure AddSingleKeywordSearchLeadWC(const FieldName, SearchString: string);
    procedure AddSingleKeywordSearchTailWC(const FieldName, SearchString: string);
    procedure AddSingleKeywordSearch(const FieldName, SearchString: string); virtual;
    procedure AddPrefixSearch(const FieldName, SearchString: string);
    procedure AddExactStringFilter(const FieldName, SearchString: string);
    procedure AddExactStringFilterNonEmpty(const FieldName, SearchString: string);
    procedure AddExactStringFilterAllowNull(const FieldName, SearchString: Variant);
    procedure AddNumericFilter(const FieldName, SearchString: string);
    procedure AddCurrencyFilter(const FieldName: string; SearchFor: Currency);
    procedure AddBooleanSearch(const FieldName: string; Value: Boolean);
    procedure AddIsNullSearch(const FieldName: string);
    procedure AddIsNotNullSearch(const FieldName: string);
    procedure AddIntFilterUnlessZero(const FieldName: string; const SearchFor: Integer);
    procedure AddIntFilterIfGreaterThanZero(const FieldName: string; const SearchFor: Integer);
    procedure AddIntFilterRequirePositive(const FieldName: string; const SearchFor: Integer);
    procedure AddDayRangeFilter(const FieldName: string; StartDay, EndDay: TDateTime);
    procedure AddDateTimeRangeFilter(const FieldName: string; StartTime, EndTime: TDateTime);
    procedure AddDateTimeGreaterFilter(const FieldName: string; SomeTime: TDateTime);
    procedure AddISODateFilterNonNull(const FieldName: string; SomeTime: TDateTime);
    procedure AddISODateFilterAllowNull(const FieldName: string; DateTime: Variant);
    procedure CheckFilterCount;
    procedure SetAccessLikeChar;
    property NakedBooleanCompare: Boolean read FNakedBooleanCompare write FNakedBooleanCompare;
    property FilterCount: Integer read FFilterCount;
    property WhereClause: string read FWhereClause;
  end;

implementation

uses JCLStrings, DateUtils, OdExceptions, OdMiscUtils, OdIsoDates;

constructor TOdSqlBuilder.Create;
begin
  inherited;
  FQuoteChar := SingleQuote;
  FLikeChar := '%';
  FDateTimeFormat := 'yyyy-mm-dd hh:nn:ss.zzz';
  FDateFormat := 'yyyy-mm-dd';
  FullTextSearchColumns := TStringList.Create;
  FullTextSearchColumns.CaseSensitive := False;
end;

procedure TOdSqlBuilder.AddWhereCondition(const Condition: string);
begin
  if Trim(Condition)='' then
    Exit;

  if FWhereClause = '' then
    FWhereClause := 'WHERE (' + Condition + ')'
  else
    FWhereClause := FWhereClause + ' AND (' + Condition + ')';

  Inc(FFilterCount);
end;

procedure TOdSqlBuilder.Reset;
begin
  FWhereClause := '';
  FFilterCount := 0;
end;

function TOdSqlBuilder.ConvertQuotes(const Term: string): string;
begin
  Result := StringReplace(Term, NativeSingleQuote, NativeSingleQuote + NativeSingleQuote, [rfReplaceAll]);
end;

function TOdSqlBuilder.HasIllegalChars(const Term: string): Boolean;
begin
  Result := StrContainsChars(Term, [NativeSingleQuote, '*'], False);
end;

procedure TOdSqlBuilder.AddSingleKeywordSearch(const FieldName, SearchString: string);
var
  Keyword: string;
begin
  Keyword := Trim(SearchString);
  if DoFullTextSearch(FieldName) then
    AddWhereCondition(Format('CONTAINS(%s, ''"%s"'')', [FieldName, Keyword]))
  else
    AddWhereCondition(FieldName + ' LIKE ' + FQuoteChar + FLikeChar +
                      ConvertQuotes(Keyword) + FLikeChar + FQuoteChar);
end;



procedure TOdSqlBuilder.AddKeywordSearch(const FieldName, SearchString: string);
var
  Keyword: string;
  Space: Integer;
  Search: string;
begin
  Search := Trim(SearchString);
  while Search <> '' do begin
    Space := Pos(' ', Search);
    if Space > 0 then begin
      Keyword := Copy(Search, 1, Space - 1);
      Search := Trim(Copy(Search, Space + 1, Length(Search)));
    end else begin
      Keyword := Trim(Search);
      Search := '';
    end;

    AddSingleKeywordSearch(FieldName, Keyword);
  end;
end;

procedure TOdSqlBuilder.AddSingleKeywordSearchLeadWC(const FieldName, SearchString: string);
var
  Keyword: string;
begin
  Keyword := Trim(SearchString);
  if DoFullTextSearch(FieldName) then
    AddWhereCondition(Format('CONTAINS(%s, ''"%s"'')', [FieldName, Keyword]))
  else
    AddWhereCondition(FieldName + ' LIKE ' + FQuoteChar + FLikeChar +
                      ConvertQuotes(Keyword) + FQuoteChar);
end;

procedure TOdSqlBuilder.AddSingleKeywordSearchTailWC(const FieldName, SearchString: string);
var
  Keyword: string;
begin
  Keyword := Trim(SearchString);
  if DoFullTextSearch(FieldName) then
    AddWhereCondition(Format('CONTAINS(%s, ''"%s"'')', [FieldName, Keyword]))
  else
    AddWhereCondition(FieldName + ' LIKE ' + FQuoteChar +
                      ConvertQuotes(Keyword) + FLikeChar + FQuoteChar);
end;

procedure TOdSqlBuilder.AddPrefixSearch(const FieldName, SearchString: string);
begin
  if SearchString = '' then
    Exit;
  if HasIllegalChars(SearchString) then
    raise EOdNonLoggedException.Create('You cannot use quotes or * in this string: ' + SearchString)
  else
    AddWhereCondition(FieldName + ' LIKE ' + FQuoteChar +
      SearchString + FLikeChar + FQuoteChar);
end;

procedure TOdSqlBuilder.AddExactStringFilter(const FieldName, SearchString: string);
begin
  if SearchString = '' then
    Exit;
  if HasIllegalChars(SearchString) then
    raise EOdNonLoggedException.Create('You cannot use quotes or * in this string: ' + SearchString)
  else
    AddWhereCondition(FieldName + '=' + FQuoteChar + SearchString + FQuoteChar);
end;

procedure TOdSqlBuilder.AddExactStringFilterNonEmpty(const FieldName, SearchString: string);
begin
  if IsEmpty(SearchString) then
    raise EOdNonLoggedException.Create('Invalid empty string value in SQL where clause for: ' + FieldName);
  AddWhereCondition(FieldName + '=' + QuoteString(SearchString));
end;

procedure TOdSqlBuilder.AddExactStringFilterAllowNull(const FieldName, SearchString: Variant);
begin
  if VarIsNull(SearchString) then
    AddIsNullSearch(FieldName)
  else
    AddWhereCondition(FieldName + '=' + QuoteString(SearchString));
end;

procedure TOdSqlBuilder.AddNumericFilter(const FieldName, SearchString: string);
begin
  if Trim(SearchString) = '' then
    Exit;
  if StrContainsChars(SearchString, ['=', '>', '<'], False) then
    AddWhereCondition(FieldName + SearchString)
  else
    AddWhereCondition(FieldName + ' = ' + SearchString);
end;

procedure TOdSqlBuilder.AddCurrencyFilter(const FieldName: string; SearchFor: Currency);
begin
  AddWhereCondition(FieldName + ' = ' + CurrToStr(SearchFor));
end;

procedure TOdSqlBuilder.CheckFilterCount;
begin
  if FFilterCount = 0 then
    raise EOdCriteriaRequiredException.Create('You must specify at least one search criteria.');
end;

procedure TOdSqlBuilder.AddDateTimeRangeFilter(const FieldName: string; StartTime, EndTime: TDateTime);
var
  Start: string;
  Finish: string;
begin
  if StartTime > 0 then begin
    Start := FormatDateTime(FDateTimeFormat, StartTime);
    AddWhereCondition(FieldName + ' >= ' + FQuoteChar + Start + FQuoteChar);
  end;

  if EndTime > 0 then begin
    Finish := FormatDateTime(FDateTimeFormat, EndTime);
    AddWhereCondition(FieldName + ' < ' + FQuoteChar + Finish + FQuoteChar);
  end;
end;

procedure TOdSqlBuilder.AddDateTimeGreaterFilter(const FieldName: string;
  SomeTime: TDateTime);
var
  Start: string;
begin
  if (SomeTime = 0) then
    Exit;

  Start := FormatDateTime(FDateTimeFormat, SomeTime);

  AddWhereCondition(FieldName + ' > ' + FQuoteChar + Start + FQuoteChar);
end;

procedure TOdSqlBuilder.AddDayRangeFilter(const FieldName: string; StartDay, EndDay: TDateTime);
var
  Start: string;
  Finish: string;
begin
  if StartDay = 0 then
    Exit;

  Start := FormatDateTime(FDateFormat, StartOfTheDay(StartDay));
  if EndDay = 0 then
    Finish := FormatDateTime(FDateFormat, StartDay + 1)
  else
    Finish := FormatDateTime(FDateFormat, EndDay + 1);

  AddWhereCondition(FieldName + ' >= '
    + FQuoteChar + Start + FQuoteChar + ' and '
    + FieldName + ' < ' + FQuoteChar + Finish + FQuoteChar);
end;

procedure TOdSqlBuilder.AddIntFilterUnlessZero(const FieldName: string;
  const SearchFor: Integer);
begin
  if SearchFor <> 0 then
    AddWhereCondition(FieldName + ' = ' + IntToStr(SearchFor));
end;

procedure TOdSqlBuilder.AddIntFilterIfGreaterThanZero(const FieldName: string; const SearchFor: Integer);
begin
  if SearchFor > 0 then
    AddWhereCondition(FieldName + ' = ' + IntToStr(SearchFor));
end;

procedure TOdSqlBuilder.AddIntFilterRequirePositive(const FieldName: string; const SearchFor: Integer);
begin
  if SearchFor <= 0 then
    raise EOdNonLoggedException.Create('Invalid integer value for SQL where clause: ' + IntToStr(SearchFor));
  AddWhereCondition(FieldName + ' = ' + IntToStr(SearchFor));
end;

procedure TOdSqlBuilder.AddBooleanSearch(const FieldName: string; Value: Boolean);
begin
  if NakedBooleanCompare then begin
    if Value then
      AddWhereCondition(FieldName)
    else
      AddWhereCondition('not ' + FieldName);
  end
  else begin
    if Value then
      AddWhereCondition(FieldName + '=1')
    else
      AddWhereCondition(FieldName + '=0');
  end;
end;

procedure TOdSqlBuilder.AddIsNotNullSearch(const FieldName: string);
begin
  AddWhereCondition(FieldName + ' is not null');
end;

procedure TOdSqlBuilder.AddIsNullSearch(const FieldName: string);
begin
  AddWhereCondition(FieldName + ' is null');
end;

procedure TOdSqlBuilder.SetAccessLikeChar;
begin
  FLikeChar := '*';   // MS Access uses a *
end;

procedure TOdSqlBuilder.AddLookupKeywordSearch(const LookupTable, ConstraintField, FieldName, SearchString: string);

  function BuildSelect(ConstraintField, LookupTable, FieldName, SearchString: string): string;
  begin
    Result := Format('select %s from %s where %s = ''%s''',
      [ConstraintField, LookupTable, FieldName, ConvertQuotes(SearchString)]);
  end;

var
  Keyword  : string;
  Space    : Integer;
  Search   : string;

  Select,
  Criteria : string;
begin
  Search := Trim(SearchString);
  Criteria := '';
  while Search <> '' do begin
    Space := Pos(' ', Search);
    if Space>0 then begin
      Keyword := Copy(Search, 1, Space-1);
      Search := Copy(Search, Space+1, Length(Search));
    end else begin
      Keyword := Search;
      Search := '';
    end;

    Select := BuildSelect(ConstraintField, LookupTable, FieldName, Keyword);
    if Length(Criteria) = 0 then
      Criteria := Criteria + Format('SELECT crit0.ticket_id from (%s) crit0 ', [Select])
    else
      Criteria := Criteria + Format ('inner join (%s) crit%d on (crit0.%s = crit%d.%s)',
                                      [Select, Length(Criteria), ConstraintField, Length(Criteria), ConstraintField]);
  end;
  AddWhereCondition('ticket_id in (' + Criteria + ')');
end;

procedure TOdSqlBuilder.AddWhereConditionFmt(const Condition: string; const Args: array of const);
begin
  AddWhereCondition(Format(Condition, Args));
end;

procedure TOdSqlBuilder.AddISODateFilterNonNull(const FieldName: string; SomeTime: TDateTime);
begin
  if FloatEquals(SomeTime, 0.0) then
    raise EOdNonLoggedException.CreateFmt('Invalid empty date param in where clause for %s: %s', [FieldName, FloatToStr(SomeTime)]);
  AddWhereConditionFmt('%s = %s', [FieldName, IsoDateTimeToStrQuoted(SomeTime)]);
end;

procedure TOdSqlBuilder.AddISODateFilterAllowNull(const FieldName: string; DateTime: Variant);
begin
  if VarIsNull(DateTime) then
    AddIsNullSearch(FieldName)
  else
    AddWhereConditionFmt('%s = %s', [FieldName, IsoDateTimeToStrQuoted(VarToDateTime(DateTime))]);
end;

function TOdSqlBuilder.QuoteString(const Term: string): string;
begin
  Result := FQuoteChar + ConvertQuotes(Term) + FQuoteChar;
end;

destructor TOdSqlBuilder.Destroy;
begin
  FreeAndNil(FullTextSearchColumns);
  inherited;
end;

function TOdSqlBuilder.DoFullTextSearch(FieldName: string): Boolean;
begin
  Result := (FullTextSearchColumns.IndexOf(FieldName) > -1);
  if not Result then
    Result := StringListIndexOfWithPrefix(FullTextSearchColumns, TableAlias + '.', FieldName) > -1;
end;

end.

