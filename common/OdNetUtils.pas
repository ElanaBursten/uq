unit OdNetUtils;

{$WARN SYMBOL_PLATFORM OFF}

interface

// These two functions come from Indy:
function URLDecode(psSrc: string): string;
function URLEncode(const psSrc: string): string;

procedure ParseURL(const URL: string; var Prot, User, Pass, Host, Path: string;
  var Port, Scheme: Integer);
function GetHostFromURL(const URL: string): string;
function BytesPerSecToTransferSpeed(Size: Cardinal): string;
function GetLastInetResponseError: string;

implementation

uses SysUtils, WinInet, OdMiscUtils, OdWinInet;

function URLDecode(psSrc: string): string;
var
  i: Integer;
  ESC: string[2];
  CharCode: Integer;
begin
  Result := ''; { do not localize }
  psSrc := StringReplace(psSrc, '+', ' ', [rfReplaceAll]); {do not localize}
  i := 1;
  while i <= Length(psSrc) do
  begin
    if psSrc[i] <> '%' then
      Result := Result + psSrc[i]
    else begin
      Inc(i);
      ESC := Copy(psSrc, i, 2);
      Inc(i, 1);
      try
        CharCode := StrToInt('$' + ESC); {do not localize}
        if (CharCode > 0) and (CharCode < 256) then
          Result := Result + Char(CharCode);
      except
      end;
    end;
    Inc(i);
  end;
end;

function URLEncode(const psSrc: string): string;
const
  UnsafeChars = ' *#%<>'; {do not localize}
var
  i: Integer;
begin
  Result := ''; { do not localize }
  for i := 1 to Length(psSrc) do
  begin
    if (AnsiPos(psSrc[i], UnsafeChars) > 0) or (psSrc[i] >= #$80) then
      Result := Result + '%' + IntToHex(Ord(psSrc[i]), 2)
    else
      Result := Result + psSrc[i];
  end;
end;

// URL Syntax protocol://[user[:password]@]server[:port]/path
procedure ParseURL(const URL: string; var Prot, User, Pass, Host, Path: string;
  var Port, Scheme: Integer);
var
  Comps: TUrlComponents;
  APIResult: Boolean;
  Path1, Path2: string;
begin
  AllocateUrlComponents(Comps);
  APIResult := InternetCrackUrl(PChar(URL), Length(URL), ICU_ESCAPE, Comps);
  RaiseOsExceptionIfFalse(APIResult, 'Unable to parse URL: ' + Url);
  try
    Prot := Comps.lpszScheme;
    User := Comps.lpszUserName;
    Pass := Comps.lpszPassword;
    Host := Comps.lpszHostName;
    Port :=  Comps.nPort;
    Path1 := Comps.lpszUrlPath;
    Path2 := Comps.lpszExtraInfo;  // Can't just throw away!
    Path := Path1 + Path2;
    Scheme := Comps.nScheme;
  finally
    DeAllocateUrlComponents(Comps);
  end;
end;

function GetHostFromURL(const URL: string): string;
var
  Proto, User, Pass, Host, Path: string;
  Port, Scheme: Integer;
begin
  ParseURL(URL, Proto, User, Pass, Host, Path, Port, Scheme);
  Result := Host;
end;

function BytesPerSecToTransferSpeed(Size: Cardinal): string;
const
  KB = 1024;
  MB = 1024 * KB;
  GB = 1024 * MB;
begin
  if Size >= GB then
    Result := Format('%.2f',[Size/GB])+' GB/sec'
  else if Size >= MB then
    Result := Format('%.2f',[Size/MB])+' MB/sec'
  else if Size >= KB then
    Result := Format('%.2f',[Size/KB])+' KB/sec'
  else
    Result := IntToStr(Size) + ' Bytes/sec'
end;

function GetLastInetResponseError: string;
var
  BufLen: Cardinal;
  Buf: array[0..8192] of char;
  DummyErr: Cardinal;
begin
  DummyErr := 0;
  BufLen := Sizeof(Buf);
  Win32Check(InternetGetLastResponseInfo(DummyErr, Buf, BufLen));
  Buf[BufLen] := #0;
  Result := Buf;
end;

end.
