unit OdHttpROChannel;

interface

uses
  Classes, SysUtils, uROClientIntf, uROProxy;

type
  TOdHttpROChannel = class(TROTransportChannel, IROTransportChannel)
  private
    FURL: string;
    FShowProgress: Boolean;
    FOperationName: string;
    FMiscAppend: string;
  protected
    procedure IntDispatch(aRequest, aResponse : TStream); override;
  public
    MostRecentRawResponse: string;
    MostRecentRawRequest: string;
    function GetTransportObject : TObject; override;
    property Url: string read FURl write FUrl;
    property ShowProgress: Boolean read FShowProgress write FShowProgress;
    property OperationName: string read FOperationName write FOperationName;
    property MiscAppend: string read FMiscAppend write FMiscAppend;
  end;

implementation

uses
  OdHttpDialog;

procedure TOdHttpROChannel.IntDispatch(aRequest, aResponse: TStream);
var
  Url: string;
begin
  SetLength(MostRecentRawRequest, aRequest.Size);
  aRequest.Seek(0, 0);
  aRequest.Read(MostRecentRawRequest[1], aRequest.Size);
  aRequest.Seek(0, 0);

  Url := FUrl;
  if FOperationName = '' then
    FOperationName := 'NEEDOPNAME';

  Url := Url + '?' + FOperationName;
  FOperationName := '';   // need to set it each time

  if FMiscAppend <> '' then
    Url := Url + '-' + FMiscAppend;

  OdHttpPost(aRequest, aResponse, Url, 'text/xml', FShowProgress, False);
  SetLength(MostRecentRawResponse, aResponse.Size);
  aResponse.Seek(0, 0);
  aResponse.Read(MostRecentRawResponse[1], aResponse.Size)
end;

function TOdHttpROChannel.GetTransportObject: TObject;
begin
  result := Self;
end;

end.

