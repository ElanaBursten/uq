unit OdDBISAMUtils;

interface

uses DB, DBISAMTb, Controls;

const
  MAX_COLUMNS = 130;

type
  TCustomizeEvent = (ceFieldDefs, ceAfterFieldDefs, ceDataRow);
  TCustomizeDataSetProc = procedure(DataSet: TDBISAMTable; Event: TCustomizeEvent) of object;

  TColumnInfo = record
    Caption: string;
    Width: Integer;
    FieldName: string;
    FieldType: TFieldType;
    TableName: string;
    FieldSize: Integer;
    PrimaryKey: Boolean;
    Visible: Boolean;
    DisplayFormat: string;
    Control: TControl;
    ParamName: string;
  end;

  TColumnList = class
  private
    FColumnCount: Integer;
    ColumnList: array[0..MAX_COLUMNS] of TColumnInfo;
    FDefaultTableName: string;
    procedure PopulateColumnInfoFromDB(var Info: TColumnInfo);
  public
    // used by the utilities that need this:
    procedure GetColumnInfo(var Info: TColumnInfo; Index: Integer);
    function GetDefaultTableName: string;
    function GetPrimaryKeyFieldName: string;
    property ColumnCount: Integer read FColumnCount;
    // used by application code to configure it:
    procedure SetDefaultTable(TableName: string);
    // Use this when the field is for display only (not a search param)
    function AddColumn(Caption: string; Width: Integer; FieldName: string;
      FieldType: TFieldType = ftUnknown; TableName: string = ''; FieldSize: Integer = 0;
      PrimaryKey: Boolean = False; Visible: Boolean = True; DisplayFormat: string = '';
      Control: TControl = nil; const ParamName: string = ''): TColumnInfo; overload;
    // Use this when the field is for display/search of the exact same field (display field = search param name)
    function AddColumn(Control: TControl; Caption: string; Width: Integer; FieldName: string;
      FieldType: TFieldType = ftUnknown; TableName: string = ''; FieldSize: Integer = 0;
      PrimaryKey: Boolean = False; Visible: Boolean = True; DisplayFormat: string = ''): TColumnInfo; overload;
    // Use this when the field is for display/search, but the display differs from the search field (search param = ID, display = name, etc.)
    function AddColumn(Control: TControl; const ParamName, Caption: string; Width: Integer; FieldName: string;
      FieldType: TFieldType = ftUnknown; TableName: string = ''; FieldSize: Integer = 0;
      PrimaryKey: Boolean = False; Visible: Boolean = True; DisplayFormat: string = ''): TColumnInfo; overload;
    procedure AddDefaultTableColumns;
  end;

procedure CloneStructure(DataSet: TDataSet; DBTable: TDBISAMTable; AutoInc: Boolean = False);
procedure CopyDataFrom(DataSet: TDataSet; DBTable: TDBISAMDataSet);
function DateToDBISAMDate(Date: TDateTime): string;
function DateTimeToDBISAMDateTime(DateTime: TDateTime): string;
function TimeToDBISAMTime(Time: TDateTime): string;
procedure SetLocalConversionDatabase(DB: TDBISAMDatabase);
procedure CreateColumns(const Def: TColumnList; Table: TDBISAMTable;
  CustomizeDataSet: TCustomizeDataSetProc);
procedure ClearTable(Table: TDBISAMTable);
function HasActiveRecords(Dataset: TDBISAMDataset): Boolean;

implementation

uses
  SysUtils, OdDbUtils;

var
  SchemaTable: TDBISAMTable = nil;

type
  TLocalFieldDef = TDBISAMFieldDef;

procedure SetLocalConversionDatabase(DB: TDBISAMDatabase);
begin
  Assert(Assigned(DB));
  FreeAndNil(SchemaTable);
  SchemaTable := TDBISAMTable.Create(nil);
  SchemaTable.DatabaseName := DB.DatabaseName;
end;

procedure CreateColumns(const Def: TColumnList; Table: TDBISAMTable;
  CustomizeDataSet: TCustomizeDataSetProc);
var
  ColInfo: TColumnInfo;
  i: Integer;
begin
  Table.Close;
  if Table.FieldCount > 0 then
    Table.DeleteTable;
  Table.Close;
  Table.Fields.Clear;
  Table.FieldDefs.Clear;

  if Def.ColumnCount = 0 then
    Exit;

  for i := 0 to Def.ColumnCount - 1 do begin
    Def.GetColumnInfo(ColInfo, i);
    Table.FieldDefs.Add(ColInfo.FieldName, ColInfo.FieldType, ColInfo.FieldSize, ColInfo.PrimaryKey);
  end;
  if Assigned(CustomizeDataSet) then
    CustomizeDataSet(Table, ceFieldDefs);

  Table.TableName := 'XMLMemoryTable' + Def.GetDefaultTableName + IntToStr(Random(MaxInt));
  Table.CreateTable;
  Table.Open;

  for i := 0 to Def.ColumnCount - 1 do begin
    Def.GetColumnInfo(ColInfo, i);
    Table.FieldByName(ColInfo.FieldName).Visible := ColInfo.Visible;
    if ColInfo.DisplayFormat <> '' then begin
      if Table.FieldByName(ColInfo.FieldName).DataType = ftDateTime then
        TDateTimeField(Table.FieldByName(ColInfo.FieldName)).DisplayFormat := ColInfo.DisplayFormat
      else if Table.FieldByName(ColInfo.FieldName).DataType = ftFloat then
        TFloatField(Table.FieldByName(ColInfo.FieldName)).DisplayFormat := ColInfo.DisplayFormat
      else if Table.FieldByName(ColInfo.FieldName).DataType = ftCurrency then
        TCurrencyField(Table.FieldByName(ColInfo.FieldName)).DisplayFormat := ColInfo.DisplayFormat
      else if Table.FieldByName(ColInfo.FieldName).DataType = ftInteger then
        TIntegerField(Table.FieldByName(ColInfo.FieldName)).DisplayFormat := ColInfo.DisplayFormat;
    end;
  end;
end;

procedure ClearTable(Table: TDBISAMTable);
begin
  if Table.Exists then
    Table.EmptyTable;
end;

procedure CloneStructure(DataSet: TDataSet; DBTable: TDBISAMTable; AutoInc: Boolean);
var
  i: Integer;
begin
  Assert(Assigned(DataSet));
  Assert(Assigned(DBTable));
  DataSet.FieldDefs.Update;
  DBTable.Close;
  DBTable.FieldDefs.Clear;
  DBTable.IndexDefs.Clear;
  //DBISAM 4 does not convert large string fields (like notes) properly using assign;
  //also now requires memo size to be 0. Attempting to assign FieldDefs
  //in that case yields "Invalid Field Size" error, so we have to manually loop the
  //dataset and convert the large string to memo in the dest DBISAM table with the 0 size.
  //http://www.elevatesoft.com/manual?action=viewtopic&id=dbisam4&product=rsdelphi&version=2007&topic=data_types_null_support
  for i := 0 to Dataset.FieldDefs.Count - 1 do
  begin
    with DBTable.FieldDefs.AddFieldDef do begin
      DataType := DataSet.FieldDefs[i].DataType;
      Name := DataSet.FieldDefs[i].Name;
      if (not AutoInc) and (DataType = ftAutoInc) then
        DataType := ftInteger;
      if ((DataSet.FieldDefs[i].DataType = ftString) and (DataSet.FieldDefs[i].Size > 512)) then begin
        DataType := ftMemo;
        Size := 0;
      end
      else
        Size := DataSet.FieldDefs[i].Size;
    end;
  end;
  if not DBTable.Exists then
    DBTable.CreateTable;
end;

procedure CopyDataFrom(DataSet: TDataSet; DBTable: TDBISAMDataSet);
var
  i: Integer;
  DBField: TField;
begin
  DataSet.Open;
  DBTable.Open;

  DataSet.First;
  while not DataSet.Eof do begin
    DBTable.Insert;
    for i := 0 to DataSet.FieldCount - 1 do begin
      DBField := DBTable.FindField(DataSet.Fields[i].FieldName);
      if Assigned(DBField) then
        DBField.Value := DataSet.Fields[i].Value;
    end;
    DBTable.Post;
    DataSet.Next;
  end;
end;

function DateToDBISAMDate(Date: TDateTime): string;
begin
  Result := QuotedStr(FormatDateTime('yyyy-mm-dd', Date));
end;

function DateTimeToDBISAMDateTime(DateTime: TDateTime): string;
begin
  Result := QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm:ss', DateTime));
end;

function TimeToDBISAMTime(Time: TDateTime): string;
begin
  Result := QuotedStr(FormatDateTime('hh:mm:ss.zzz', Time));
end;

function HasActiveRecords(Dataset: TDBISAMDataset): Boolean;
var
  OriginalFilter: String;
  OriginalFiltered: Boolean;
  Bookmark: TBookmark;
begin
  Assert(Assigned(Dataset));
  Assert(Dataset.Active);
  Assert(Assigned(Dataset.FieldByName('active')));

  OriginalFilter := Dataset.Filter;
  OriginalFiltered := Dataset.Filtered;
  Bookmark := Dataset.Bookmark;
  Dataset.DisableControls;
  try
    Dataset.Filter := 'active = true';
    Dataset.Filtered :=  True;
    Result := Dataset.RecordCount > 0;
  finally
    Dataset.Filter := OriginalFilter;
    Dataset.Filtered :=  OriginalFiltered;
    Dataset.Bookmark := Bookmark;
    Dataset.EnableControls;
  end;
end;

{ TColumnList }

function TColumnList.AddColumn(Caption: string; Width: Integer;
  FieldName: string; FieldType: TFieldType; TableName: string;
  FieldSize: Integer; PrimaryKey, Visible: Boolean; DisplayFormat: string;
  Control: TControl; const ParamName: string): TColumnInfo;
begin
  ColumnList[FColumnCount].Caption := Caption;
  ColumnList[FColumnCount].Width := Width;
  ColumnList[FColumnCount].FieldName := FieldName;
  ColumnList[FColumnCount].FieldType := FieldType;
  ColumnList[FColumnCount].TableName := TableName;
  ColumnList[FColumnCount].FieldSize := FieldSize;
  ColumnList[FColumnCount].PrimaryKey := PrimaryKey;
  ColumnList[FColumnCount].Visible := Visible;
  ColumnList[FColumnCount].DisplayFormat := DisplayFormat;
  ColumnList[FColumnCount].Control := Control;
  if ParamName = '' then
    ColumnList[FColumnCount].ParamName := FieldName
  else
    ColumnList[FColumnCount].ParamName := ParamName;
  PopulateColumnInfoFromDB(ColumnList[FColumnCount]);
  Inc(FColumnCount);
  Assert(FColumnCount < MAX_COLUMNS);
  Result := ColumnList[FColumnCount];
end;

function TColumnList.AddColumn(Control: TControl; Caption: string;
  Width: Integer; FieldName: string; FieldType: TFieldType;
  TableName: string; FieldSize: Integer; PrimaryKey, Visible: Boolean;
  DisplayFormat: string): TColumnInfo;
begin
  Result := AddColumn(Caption, Width, FieldName, FieldType, TableName, FieldSize, PrimaryKey, Visible, DisplayFormat, Control);
end;

function TColumnList.AddColumn(Control: TControl; const ParamName,
  Caption: string; Width: Integer; FieldName: string;
  FieldType: TFieldType; TableName: string; FieldSize: Integer; PrimaryKey,
  Visible: Boolean; DisplayFormat: string): TColumnInfo;
begin
  Result := AddColumn(Caption, Width, FieldName, FieldType, TableName,
    FieldSize, PrimaryKey, Visible, DisplayFormat, Control, ParamName);
end;

procedure TColumnList.AddDefaultTableColumns;
var
  i: Integer;
  Field: TField;
begin
  SchemaTable.Close;
  SchemaTable.TableName := FDefaultTableName;
  SchemaTable.Open;
  try
    for i := 0 to SchemaTable.FieldCount - 1 do begin
      Field := SchemaTable.Fields[i];
      AddColumn(Field.FieldName, Field.Size, Field.FieldName, Field.DataType, FDefaultTableName, Field.Size);
    end;
  finally
    SchemaTable.Close;
  end;
end;

procedure TColumnList.GetColumnInfo(var Info: TColumnInfo; Index: Integer);
begin
  Assert(Index <= ColumnCount);
  Info := ColumnList[Index];
end;

function TColumnList.GetDefaultTableName: string;
begin
  Result := FDefaultTableName;
end;

function TColumnList.GetPrimaryKeyFieldName: string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ColumnCount - 1 do begin
    if ColumnList[I].PrimaryKey then begin
      Result := ColumnList[I].FieldName;
      Break;
    end;
  end;
  Assert(not (Result = ''), 'No primary key specified');
end;

procedure TColumnList.PopulateColumnInfoFromDB(var Info: TColumnInfo);
var
  Field: TField;
begin
  if Info.Caption = '' then
    Info.Caption := Info.FieldName;
  if Info.Width = 0 then
    Info.Width := 100;
  if Info.TableName = '' then
    Info.TableName := GetDefaultTableName;
  if (Info.FieldType = ftUnknown) and (Info.TableName <> '') then begin
    if (SchemaTable.TableName <> Info.TableName) then begin
      SchemaTable.Close;
      SchemaTable.TableName := Info.TableName;
      SchemaTable.Open;
    end;
    Field := SchemaTable.FindField(Info.FieldName);
    if Assigned(Field) then begin
      Info.FieldType := Field.DataType;
      Info.FieldSize := Field.Size;
    end;
  end;
  if Info.FieldType = ftUnknown then
    Info.FieldType := ftString;
  if (Info.FieldType in [ftBCD, ftString, ftVarBytes, ftBytes, ftBlob, ftFMTBCD, ftFmtMemo, ftMemo]) and (Info.FieldSize = 0) then
    Info.FieldSize := 50;
end;

procedure TColumnList.SetDefaultTable(TableName: string);
begin
  FDefaultTableName := TableName;
end;

initialization

finalization
  FreeAndNil(SchemaTable);

end.
