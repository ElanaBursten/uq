//*********************************************************
//*                OdTimeRangeSelect.pas                  *
//*          Copyright (c) 2014 by Oasis Digital          *
//*                All rights reserved.                   *
//*********************************************************

unit OdTimeRangeSelect;
  //-Date/time range selector, based on DevExpress TcxDateEdit component

interface

uses
  Windows, Messages, cxDateUtils, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, SharedDevExStyles, dxCore;

const
  //named default ranges - used to select one of the default date ranges when
  //calling SelectDateRange
  rsCustom        = 00;
  rsToday         = 01;
  rsYesterday     = 02;
  rsLastWeek      = 03;
  rsThisWeek      = 04;
  rsMonthToDate   = 05;
  rsYearToDate    = 06;
  rsCurrentMonth  = 07;
  rsCurrentYear   = 08;
  rsPreviousMonth = 09;
  rsPreviousYear  = 10;
  rsLastMonth     = 11;
  rsLast3Months   = 12;
  rsLast6Months   = 13;
  rsLastYear      = 14;
  rsDefLastRange  = rsLastYear;
type
  TOdDateTimeRange = class
    FRangeName : string;
    FFromDate  : TDateTime;
    FToDate    : TDateTime;
  end;

  TOdDateTimeRangeList = class(TList)
  protected
    //Protected declarations
    function GetItem(I : Integer) : TOdDateTimeRange;
    procedure SetItem(I : Integer; Value : TOdDateTimeRange);
  public
    //Public declarations
    function  Append(RangeName : string; FromDate, ToDate : TDateTime) : TOdDateTimeRange;

    property Items[I : Integer] : TOdDateTimeRange
      read GetItem
      write SetItem;
      default;
  end;

  TOdTimeRangeSelectFrame = class(TFrame)
    FromLabel: TLabel;
    FromDateEdit: TcxDateEdit;
    ToLabel: TLabel;
    ToDateEdit: TcxDateEdit;
    DatesLabel: TLabel;
    DatesComboBox: TComboBox;
    FromTimeEdit: TDateTimePicker;
    ToTimeEdit: TDateTimePicker;
    procedure ToDateEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure DatesComboBoxChange(Sender: TObject);
    procedure FromDateEditChange(Sender: TObject);
    procedure ToTimeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToDateEditChange(Sender: TObject);
  protected
    //Protected declarations
    FDateTimeRangeList : TOdDateTimeRangeList;
    procedure Loaded; override;
    procedure AddDefaultDateRanges;
    function  GetFromDate : TDateTime;
    function  GetToDate : TDateTime;
    procedure SetFromDate(Date : TDateTime);
    procedure SetToDate(Date : TDateTime);
  public
    //Public declarations
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AppendDateRange(RangeName : string; FromDate, ToDate : TDateTime);
      //-Call this to add a non-standard date range
    procedure SelectDateRange(DateRange : Byte;
                              FromDate  : TDateTime = 0;
                              ToDate    : TDateTime = 0);
      //-Call this to manually select a date range
    procedure NoDateRange;
      //-Call this to select no date range
  published
    //Published properties
    property FromDate : TDateTime
      read GetFromDate
      write SetFromDate;
    property ToDate : TDateTime
      read GetToDate
      write SetToDate;
  end;

//============================================================================//

implementation

uses
  OdMiscUtils;

{$R *.dfm}

//TOdDateTimeRangeList

function TOdDateTimeRangeList.GetItem(I : Integer) : TOdDateTimeRange;
begin
  Result := TOdDateTimeRange(inherited Items[I]);
end;

procedure TOdDateTimeRangeList.SetItem(I : Integer; Value : TOdDateTimeRange);
begin
  inherited Items[I] := Pointer(Value);
end;

function TOdDateTimeRangeList.Append(RangeName : string; FromDate, ToDate : TDateTime) : TOdDateTimeRange;
begin
  Result := TOdDateTimeRange.Create;

  Result.FRangeName := RangeName;
  Result.FFromDate := FromDate;
  Result.FToDate := ToDate;

  Add(Result);
end;

//TOdTimeRangeSelectFrame

constructor TOdTimeRangeSelectFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDateTimeRangeList := TOdDateTimeRangeList.Create;
end;

destructor TOdTimeRangeSelectFrame.Destroy;
begin
  FreeAndNil(FDateTimeRangeList);
  inherited Destroy;
end;

procedure TOdTimeRangeSelectFrame.Loaded;
begin
  inherited;

  //initialize the combo box
  AddDefaultDateRanges;
  SelectDateRange(rsYesterday);
  DatesComboBox.DropDownCount := rsDefLastRange+1;
  //We are assigning the glyph manually because the dfm's may produce different
  //binary image data based on the bit settings of the individual developer's monitor.
  SharedDevExStyleData.SetGlyph(FromDateEdit, 0);
  SharedDevExStyleData.SetGlyph(ToDateEdit, 0);
end;

procedure TOdTimeRangeSelectFrame.AppendDateRange(RangeName : string; FromDate, ToDate : TDateTime);
begin
  FDateTimeRangeList.Append(RangeName, FromDate, ToDate);
  DatesComboBox.Items.Append(RangeName);
end;

procedure TOdTimeRangeSelectFrame.AddDefaultDateRanges;
var
  I : Integer;
  CurrentDate : TDateTime;
  Year, Month, Day : Word;
  FromDate, ToDate : TDateTime;

  function MakeDate(Y, M, D : Word) : TDateTime;
  begin
    if not TryEncodeDate(Y, M, D, Result) then
      Result := CurrentDate;
  end;

  function BeginningOfWeek(DT : TDateTime) : TDateTime;
  var
    DOW : Integer;
  begin
    //calculate a day of week value in which monday = 0, sunday = 6
    DOW := DayOfWeek(DT)-2;
    if DOW = -1 then
      DOW := 6;

    //return the datetime corresponding to Monday at midnight
    Result := Trunc(DT) - DOW;
  end;

begin
  //get the current date in case we need it
  CurrentDate := Trunc(Now);
  DecodeDate(CurrentDate, Year, Month, Day);

  for I := rsCustom to rsLastYear do
    //get the date range
    case I of
      rsCustom :
        AppendDateRange('Custom', 0, 0);
      rsToday :
        AppendDateRange('Today', CurrentDate, CurrentDate+1);
      rsYesterday :
        AppendDateRange('Yesterday', CurrentDate-1, CurrentDate);
      rsLastWeek :
        begin
          FromDate := BeginningOfWeek(CurrentDate-7);
          AppendDateRange('Last Week', FromDate, FromDate+6);
        end;
      rsThisWeek :
        begin
          FromDate := BeginningOfWeek(CurrentDate);
          AppendDateRange('This Week', FromDate, FromDate+6);
        end;
      rsMonthToDate :
        AppendDateRange('Month to Date', MakeDate(Year, Month, 1), CurrentDate+1);
      rsYearToDate :
        AppendDateRange(
          'Year to Date', MakeDate(Year, 1, 1), CurrentDate+1);
      rsCurrentMonth :
        begin
          FromDate := MakeDate(Year, Month, 1);
          ToDate := IncMonth(FromDate, 1)-1;
          AppendDateRange('Current Month', FromDate, ToDate);
        end;
      rsCurrentYear :
        AppendDateRange(
          'Current Year', MakeDate(Year, 1, 1), MakeDate(Year, 12, 31));
      rsPreviousMonth :
        begin
          FromDate := IncMonth(MakeDate(Year, Month, 1), -1);
          ToDate := IncMonth(FromDate, 1)-1;
          AppendDateRange('Previous Month', FromDate, ToDate);
        end;
      rsPreviousYear :
        AppendDateRange(
          'Previous Year', MakeDate(Year-1, 1, 1), MakeDate(Year-1, 12, 31));
      rsLastMonth :
        AppendDateRange(
          'Last Month', IncMonth(CurrentDate, -1), CurrentDate);
      rsLast3Months :
        AppendDateRange(
          'Last 3 Months', IncMonth(CurrentDate, -3), CurrentDate);
      rsLast6Months :
        AppendDateRange(
          'Last 6 Months', IncMonth(CurrentDate, -6), CurrentDate);
      rsLastYear :
        AppendDateRange(
          'Last Year', IncMonth(CurrentDate, -12), CurrentDate);
    end;
end;

procedure TOdTimeRangeSelectFrame.ToDateEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //check for a To date that is earlier than the From date
  if NotEmpty(VarToString(DisplayValue)) and (DisplayValue < FromDateEdit.Date) then begin
    Error := True;
    ErrorText := 'To date is earlier than From date';
  end;
end;

procedure TOdTimeRangeSelectFrame.SelectDateRange(DateRange : Byte;
                                                  FromDate  : TDateTime = 0;
                                                  ToDate    : TDateTime = 0);
begin
  if DateRange <> rsCustom then
    with FDateTimeRangeList[DateRange] do begin
      FromDate := FFromDate;
      ToDate := FToDate;
    end;

  //set the date range
  if FromDate <> 0 then begin
    FromDateEdit.Date := Trunc(FromDate);
    FromTimeEdit.Time := Frac(FromDate);
  end;
  if ToDate <> 0 then begin
    ToDateEdit.Date := ToDate;
    ToTimeEdit.Time := Frac(ToDate);
  end;

  //update the combo box
  DatesComboBox.ItemIndex := DateRange;
end;

procedure TOdTimeRangeSelectFrame.NoDateRange;
begin
  FromDateEdit.Date := NullDate;
  FromTimeEdit.Time := 0;
  FromTimeEdit.Format := ' ';
  ToDateEdit.Date := NullDate;
  ToTimeEdit.Time := 0;
  ToTimeEdit.Format := ' ';
  DatesComboBox.ItemIndex := -1;
end;

procedure TOdTimeRangeSelectFrame.DatesComboBoxChange(Sender: TObject);
begin
  SelectDateRange(DatesComboBox.ItemIndex);
end;

procedure TOdTimeRangeSelectFrame.FromDateEditChange(Sender: TObject);
begin
  SelectDateRange(rsCustom);
  if FromDateEdit.Date = cxDateUtils.NullDate then
    FromTimeEdit.Format := ' '
  else if FromTimeEdit.Format = ' ' then
    FromTimeEdit.Format := 'hh:mm tt';
end;

procedure TOdTimeRangeSelectFrame.ToDateEditChange(Sender: TObject);
begin
  SelectDateRange(rsCustom);
  if ToDateEdit.Date = cxDateUtils.NullDate then
    ToTimeEdit.Format := ' '
  else if ToTimeEdit.Format = ' ' then
    ToTimeEdit.Format := 'hh:mm tt';
end;

function TOdTimeRangeSelectFrame.GetFromDate : TDateTime;
begin
  if FromDateEdit.Date <= 0 then
    Result := 0
  else
    Result := FromDateEdit.Date + Frac(FromTimeEdit.Time);
end;

function TOdTimeRangeSelectFrame.GetToDate : TDateTime;
begin
  if ToDateEdit.Date <= 0 then
    Result := 0
  else
    Result := ToDateEdit.Date + Frac(ToTimeEdit.Time);
end;

procedure TOdTimeRangeSelectFrame.SetFromDate(Date : TDateTime);
begin
  SelectDateRange(rsCustom, Date, 0);
end;

procedure TOdTimeRangeSelectFrame.SetToDate(Date : TDateTime);
begin
  SelectDateRange(rsCustom, 0, Date);
end;

procedure TOdTimeRangeSelectFrame.ToTimeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Shift = [] then
    if Key = VK_DELETE then
      (Sender as TDateTimePicker).Time := 0;
end;

end.
