unit OdReportBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, cxCalendar,
  SharedDevExStyles, CheckLst, CodeLookupList;

type
  TReportBaseForm = class(TEmbeddableForm)
    SaveTSVDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
  protected
    FParams: TStrings;
    FOutputParams: TStrings;
    FReportID: string;
    FCallCenterList: TCodeLookupList;
    procedure ValidateParams; virtual;
    procedure InitReportControls; virtual;
    function GetBaseFileName: string; virtual;
    procedure SetParam(const Name: string; const Value: string);
    procedure SetParamInt(const Name: string; const Value: Integer);
    procedure SetParamDate(const Name: string; const Value: TDateTime; Required: Boolean = False);
    procedure SetParamBoolean(const Name: string; const Value: Boolean);
    procedure SetReportID(const ReportID: string);
    procedure SetParamIntCombo(const Name: string; const Combo: TComboBox; Required: Boolean = False);
    function GetFileExtension: string; virtual;
    procedure SetUpManagerList(Combo: TComboBox);
    procedure SetupCallCenterList(CallCenterItems: TStrings; IncludeBlank: Boolean);
    function GetReportID: string;
  public
    property ReportID: string read GetReportID write FReportID;
    procedure Preview; virtual;
    procedure ExportData; virtual;
    function CanPreview: Boolean; virtual;
    function CanExport: Boolean; virtual;
    procedure SetDropDownCounts(Count: Integer);
    procedure SetGlyphs;
    procedure ShowConfigString;
    procedure Init;
    procedure LeavingNow; override;
  end;

procedure PreviewPDF(Params: TStrings); overload;
procedure PreviewPDF(Params: TStrings; OutputParams: TStrings); overload;

procedure RequireRadioGroup(RG: TRadioGroup; ErrorMessage: string);
procedure RequireComboBox(Box: TComboBox; ErrorMessage: string);

implementation

{$R *.dfm}

uses
  OdHourglass, OdMiscUtils, DMu, OdIsoDates, OdExceptions, ClipBrd,
  OdVclUtils, EmployeeSearch, QMConst, LocalEmployeeDMu;

const
  NeedDateMessage = 'Your must specify a date / range: ';

{ TReportBaseForm }

procedure TReportBaseForm.ValidateParams;
begin
  FreeAndNil(FParams);
  FParams := TStringList.Create;
  SetParam('Ver', AppVersion);
  SetParamInt('User', DM.UQState.UserID);
  SetParam('PasswordHash', DM.UQState.Password);
  // Descendants should populate other params using the SetParam* methods
end;

function GetTemporaryFileName: string;
begin
  // These temp files are cleaned up on startup.  See: TDM.CleanUQFilesFromTempDir
  Result := GetWindowsTempFileName(False, '', 'QMR');
  Result := ChangeFileExt(Result, '.pdf');
end;

procedure SaveFile(FileName: string; Contents: string);
var
  Str: TMemoryStream;
begin
  Str := TMemoryStream.Create;
  try
    Str.Write(Contents[1], Length(Contents));
    Str.SaveToFile(FileName);
  finally
    FreeAndNil(Str);
  end;
end;

{  Upcoming new approach, starts report in background:
procedure PreviewPDF(Params: TStrings);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  DM.StartReport('PDF', Params);
end;
}

procedure PreviewPDF(Params: TStrings; OutputParams: TStrings);
var
  PDF: string;
  Cursor: IInterface;
  TempFileName: string;
begin
  Cursor := ShowHourGlass;
  PDF := DM.Engine.GetReport('PDF', Params, OutputParams);
  if Length(PDF) < 100 then
    raise Exception.Create('File length too short: ' + IntToStr(Length(PDF)));

  TempFileName := GetTemporaryFileName;
  SaveFile(TempFileName, PDF);
  OdShellExecute(PChar(TempFileName), '', True, SW_SHOWMAXIMIZED);
end;

procedure PreviewPDF(Params: TStrings);
var
  PDF: string;
  Cursor: IInterface;
  TempFileName: string;
begin
  Cursor := ShowHourGlass;
  PDF := DM.Engine.GetReport('PDF', Params);
  if Length(PDF) < 100 then
    raise Exception.Create('File length too short: ' + IntToStr(Length(PDF)));

  TempFileName := GetTemporaryFileName;
  SaveFile(TempFileName, PDF);
  OdShellExecute(PChar(TempFileName), '', True, SW_SHOWMAXIMIZED);
end;

procedure TReportBaseForm.Preview;
begin
  ValidateParams;
  FreeAndNil(FOutputParams);
  FOutputParams := TStringList.Create;
  PreviewPDF(FParams, FOutputParams);   // non-PDF reports should override this and omit the Inherited
end;

procedure TReportBaseForm.FormCreate(Sender: TObject);
begin
  inherited;
  // I needed to remove this line because I need to find out the ReportID before
  // loading anything in the reports, including the manager list. Executing
  // InitReportControls here will be to late to set the ReportID and retrieve
  // the limitation for that report

  // InitReportControls;
end;

procedure TReportBaseForm.InitReportControls;
begin
  // nothing to init here
end;

procedure TReportBaseForm.LeavingNow;
begin
  if Assigned(FCallCenterList) then
    FreeAndNil(FCallCenterList);
  inherited;
end;

function TReportBaseForm.CanPreview: Boolean;
begin
  Result := True;
end;

function TReportBaseForm.CanExport: Boolean;
begin
  Result := True;
end;

procedure TReportBaseForm.ExportData;
var
  TSV: string;
  Str: TMemoryStream;
  Cursor: IInterface;
begin
  if not CanExport then
    raise Exception.Create('This report does not support exporting data.')
  else begin
    SaveTSVDialog.Filename := GetBaseFileName + '.' + GetFileExtension;
    SaveTSVDialog.DefaultExt := GetFileExtension;
    if not SaveTSVDialog.Execute then
      Exit;

    Cursor := ShowHourGlass;

    ValidateParams;  // so we get the right data
    TSV := DM.Engine.GetReport('TSV', FParams);

    Str := TMemoryStream.Create;
    try
      Str.Write(TSV[1], Length(TSV));
      Str.SaveToFile(SaveTSVDialog.Filename);
    finally
      FreeAndNil(Str);
    end;
  end;
end;

function TReportBaseForm.GetBaseFileName: string;
begin
  Result := Self.ClassName;
  Assert(Length(Result) > 0);
  if Result[1] = 'T' then
    Result := Copy(Result, 2, 9999);
  if StrEndsWith('Form', Result) then
    Result := Copy(Result, 1, Length(Result) - Length('Form'));
  if Trim(Result) = '' then
    Result := 'Report';
end;

procedure TReportBaseForm.SetDropDownCounts(Count: Integer);
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TComboBox then begin
      if Components[i].Tag = 0 then
        (Components[i] as TComboBox).DropDownCount := Count;
    end;
  end;
end;

procedure TReportBaseForm.SetParam(const Name, Value: string);
begin
  Assert(Assigned(FParams), 'inherited ValidateParams not called?');
  FParams.Values[Name] := Value;
end;

procedure TReportBaseForm.SetParamDate(const Name: string;
  const Value: TDateTime; Required: Boolean = False);
begin
  if Required then
    if Value = 0.0 then
      raise EOdEntryRequired.Create(NeedDateMessage + Name);
  SetParam(Name, IsoDateTimeToStr(Value));
end;

procedure TReportBaseForm.SetParamInt(const Name: string; const Value: Integer);
begin
  SetParam(Name, IntToStr(Value));
end;

procedure TReportBaseForm.SetParamBoolean(const Name: string;
  const Value: Boolean);
begin
  if Value then
    SetParam(Name, '1')
  else
    SetParam(Name, '0')
end;

procedure TReportBaseForm.SetReportID(const ReportID: string);
begin
  SetParam('Report', ReportID);
end;

procedure TReportBaseForm.SetParamIntCombo(const Name: string;
  const Combo: TComboBox; Required: Boolean = False);
begin
  if Required then
    RequireComboBox(Combo, 'Please select a ' + Name);
  SetParamInt(Name, GetComboObjectInteger(Combo, Required));
end;

procedure TReportBaseForm.ShowConfigString;
begin
  ValidateParams;  // so we get the right data
  ClipBoard.AsText := FParams.Text;
  ShowMessage('Config string=' + CRLF + FParams.Text +
              CRLF + '(Also copied to clipboard)');

end;

function TReportBaseForm.GetFileExtension: string;
begin
  Result := 'tsv';
end;

procedure RequireRadioGroup(RG: TRadioGroup; ErrorMessage: string);
begin
  if RG.ItemIndex = -1 then begin
    RG.SetFocus;
    raise EOdEntryRequired.Create(ErrorMessage);
  end;
end;

procedure RequireComboBox(Box: TComboBox; ErrorMessage: string);
begin
  if Box.ItemIndex = -1 then begin
    Box.SetFocus;
    raise EOdEntryRequired.Create(ErrorMessage);
  end;
end;

procedure TReportBaseForm.SetUpManagerList(Combo: TComboBox);
begin
  AddEmployeeSearchButtonForCombo(Combo, esReportManangers);
  EmployeeDM.ManagerList(Combo.Items, ReportID);
end;

procedure TReportBaseForm.SetupCallCenterList(CallCenterItems: TStrings; IncludeBlank: Boolean);
begin
  if not Assigned(FCallCenterList) then
    FCallCenterList := TCodeLookupList.Create(CallCenterItems);
  DM.CallCenterDescriptionList(FCallCenterList, IncludeBlank);
end;

function TReportBaseForm.GetReportID: string;
begin
  Assert(FReportID <> '', ClassName + ' does not have a ReportID ???');
  Result := FReportID;
end;

procedure TReportBaseForm.Init;
begin
  InitReportControls;
end;

procedure TReportBaseForm.SetGlyphs;
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TcxDateEdit then begin
      //We are assigning the glyph manually because the dfm's may produce different
      //binary image data based on the bit settings of the individual developer's monitor.
      SharedDevExStyleData.SetGlyph((Components[i] as TcxDateEdit), 0);
    end;
  end;
end;

end.

