unit xqGeoFunctions;
//QMANTWO-504
{use this module to house geo functions for d2007}

interface
  type tDistUnit = (tdMiles, tdKilometers, tdNauticalMiles, tdFeet, tdInches);
  function calculateDistance( dlat1, dlat2, dLon1, dlon2: extended; distUnit: tDistUnit):extended;
implementation

uses math;
function calculateDistance( dlat1, dlat2, dLon1, dlon2: extended; distUnit: tDistUnit):extended;
var
  Dist, edist: extended;
  lat1, lat2, lon1, lon2: extended;
  F: extended;
  a, b, TanU1, U1, tanU2, U2, Usq: extended;
  L, Lambda, Oldlambda: extended;
  count: integer;
  sigma, sinSigma, cosSigma, TanSigma: extended;
  cos2sigmam: extended;
  sinAlpha, cosalpha, cosSqAlpha, C: extended;
  AA, BB, L1, L2: extended;
  dsigma: extended;

    function deg2rad(deg: extended): extended;
    { Degrees to Radians }
    begin
      result := deg * PI / 180.0;
    end;

    { *********** Rad2Deg ********** }
    function rad2deg(rad: extended): extended;
    { radians to degrees }
    begin
      result := rad / PI * 180.0;
    end;

begin
  lat1 := deg2rad(dlat1);
  lat2 := deg2rad(dlat2);
  lon1 := deg2rad(dLon1);
  lon2 := deg2rad(dlon2);
  a := 6378137.0;
  b := 6356752.314;
  F := (a - b) / a;

  TanU1 := (1 - F) * tan(lat1);
  U1 := arctan(TanU1);
  tanU2 := (1 - F) * tan(lat2);
  U2 := arctan(tanU2);

  TanU1 := (1 - F) * Tan(lat1);
  U1 := arctan(TanU1);
  tanU2 := (1 - F) * tan(lat2);
  U2 := arctan(tanU2);

  L := lon2 - lon1;
  Lambda := L; { first approximation of distance }
  count := 0;
  repeat { iterate }
    inc(count);

    sinSigma := sqrt(sqr(Cos(U2) * Sin(Lambda)) +  sqr(Cos(U1) * Sin(U2) - Cos(U2) * Sin(U1) * Cos(Lambda)));
    cosSigma := Sin(U1) * Sin(U2) + Cos(U1) * Cos(U2) * Cos(Lambda);


    sigma := arctan(sinSigma / cosSigma);
    sinAlpha := Cos(U1) * Cos(U2) * Sin(Lambda) / sinSigma;

    sinSigma := sqrt(sqr(Cos(U2) * Sin(Lambda)) +  sqr(Cos(U1) * Sin(U2) - Cos(U2) * Sin(U1) * Cos(Lambda)));
    cosSigma := Sin(U1) * Sin(U2) + Cos(U1) * Cos(U2) * Cos(Lambda);


    sigma := arctan(sinSigma / cosSigma);
    sinAlpha := Cos(U1) * Cos(U2) * Sin(Lambda) / sinSigma;


    cosSqAlpha := 1 - sqr(sinAlpha);
    if (lat1 = 0) and (lat2 = 0) then
      cos2sigmam := 0
    else

      cos2sigmam := cosSigma - (2 * Sin(U1) * Sin(U2) / cosSqAlpha);

      cos2sigmam := cosSigma - (2 * Sin(U1) * Sin(U2) / cosSqAlpha);

    C := (F / 16) * cosSqAlpha * (4 + F * (4 - 3 * cosSqAlpha));
    Oldlambda := Lambda;
    Lambda := L + (1 - C) * F * sinAlpha *
      (sigma + C * sinSigma * (cos2sigmam + C * cosSigma *
      (2 * sqr(cos2sigmam) - 1)));
  until (abs(Lambda - Oldlambda) < 10E-12) or (count > 1000);

    Usq := cosSqAlpha * (sqr(a) - sqr(b)) / sqr(b);

    AA := 1.0 + (Usq / 16384.0) *
      (4096.0 + Usq * (-768.0 + Usq * (320.0 - 175.0 * Usq)));

    BB := (Usq / 1024.0) *
      (256.0 + Usq * (-128.0 + Usq * (74.0 - 47.0 * Usq)));

    dsigma := BB * sinSigma *
      (cos2sigmam + (BB / 4.0) * (-1 + cosSigma * (2 * sqr(cos2sigmam)) -
      (BB / 6) * cos2sigmam * (-3 + 4 * sqr(sinSigma)) *
      (-3 + 4 * sqr(cos2sigmam))));

      edist := b * AA * (sigma - dsigma);

     case distUnit of
      tdMiles: { miles }
        dist := edist * 0.0006212;
      tdKilometers: { kilometers }
        dist := edist * 0.001;
      tdNauticalMiles: { nautical miles }
        dist := edist * 0.0005398;
      tdFeet: { feet }
        dist := (edist * (1/0.30480)) ;
      tdInches: { inches }
        dist := (edist * 0.0006212) * 12 * 5280;
    end;
    result := dist;
    { Convert meters back to user preferred distance units }


end;
end.
