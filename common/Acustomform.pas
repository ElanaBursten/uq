unit Acustomform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxScrollBox,
  cxCheckGroup, ExtCtrls, cxCheckBox, OdContainer, Acustomformframe,
  ComCtrls, dxCore, cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ACustomFormConst;

{ ACustomForm (EB): There are several ways that Custom fields can be displayed
  using this unit.
    1) Dialog Form - Is called by code as a pop-up form that can be filled out
    2) Inside Container - Into a pre-existing form/tab or other container
    3) Individual Group Container - Pulling individual groups into sections of another form
    4) Test Mode - For testing in Isolated project}


type
  {Group}
  TFieldGroup = class(TObject)
  private
    fSaveNoteFlag: boolean;
    function GetFieldControlType(pFormField: string; pInfoType: string): TcfControl;
    function SaveCustomFormFieldNote(pFieldInfo: TFieldInfo): Boolean;
  public
    FID: integer;                 {Class copy of Form ID because of scope}
    GroupNumber: integer;         {Group Number}
    FieldCount: integer;
    ScrollBox: TcxScrollBox;
    FrameList: TList;
    TopLabel: TLabel;
    property SaveNoteFlag: boolean read fSaveNoteFlag write fSaveNoteFlag;
    Constructor Create(pParent: TControl; pFormID: integer; pGroup: integer; pFieldCount: integer);  overload;
    function RetrieveQuestions(pEmpID: integer; pForeignType: integer = 0; pForeignID: integer = 0): boolean;
    function RetrieveQuestionsAndAnswers(pEmpID: integer; pForeignType: integer = 0; pForeignID: integer = 0): boolean;
    procedure SaveGroupAnswers;
    Destructor Destroy; override;
  end;

  {2 - Group Container: Call individually to put data into a container on pre-existing form/tab/etc}
  TGroupContainer = class(TObject)
  private
    fFormID: integer;
    fForm: TFormInfo;
    fEmpID: integer;
    fAnswersForeignID: integer;  {{If associated with a ticket id, damage id, etc}
    fContainerScrollBox: TScrollBox;
    fMsgPanel: TPanel;
    fMsgTop: TLabel;
    fFieldGroups: Array of TFieldGroup;  {Our groups}
    fGroupCount: integer;  {How many Groups for this form}
    fEditExistingAnswers : boolean;  {True if user can edit answers next time in form}
    procedure LoadGroups;
    function  HasExistingAnswers: boolean;
    procedure RemoveGroups;
    procedure AddTitlePanel(pFormTitleCaption: string);

  public
    procedure Save;
    Constructor Create(pFormID: integer; pFormTitleCaption: string;
                       pContainer: TWinControl;
                       pEmpID: integer; pForeignID: integer;
                       pForm: TFormInfo;
                       pEditExistingAnswers: boolean = True);

    Destructor Destroy; override;
    procedure DisplayNoteforOtherExistingAnswers;

    property AnswersForeignID: integer read fAnswersForeignID write fAnswersForeignID;

  end;



 {Form}
  TACustForm = class(TForm)
    MasterScrollBox: TScrollBox;
    pnlButtons: TPanel;
    btnCancel: TButton;
    btnSave: TButton;
    btnCreateGroups: TButton;
    pnlTop: TPanel;
    lblTitle: TLabel;
    procedure btnCreateGroupsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSaveClick(Sender: TObject);

  private
    fEmpID: integer; {the one filling out the form}
    fCustomFormID: integer;
    fGroupCount: integer;  {How many Groups for this form}
    fFormData: TFormInfo;
    fAnswerForeignID: integer;  {This is the ID that the form asnwers are for}
    procedure LoadGroups;
    procedure RemoveGroups; {Last Step - Destroy}
  public
    FieldGroups: Array of TFieldGroup;  {Our groups}
    property FormData: TFormInfo read fFormData write fFormData;
    property EmpID: integer read fEmpID write fEmpID;
    property GroupCount: integer read fGroupCount;
    property CustomFormID: integer read fCustomFormID write fCustomFormID;

    procedure SaveAnswers;

    class function GetCustomFormDialog(pFormID: integer; pFormTitleBar: string;  {1}
                                       pFormTitleCaption: string; pEmpID: integer): boolean;
  end;

  function TabCharsOnly(const S: string): string;

var

  ACustForm: TACustForm;
//  GFormData: TFormInfo;

implementation

uses AcustomFormDMu, JCLANsiStrings;


{$R *.dfm}

{ TForm1 }

procedure TACustForm.btnCreateGroupsClick(Sender: TObject);
begin
//
end;

procedure TACustForm.btnSaveClick(Sender: TObject);
begin
  SaveAnswers;
end;

procedure TACustForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 RemoveGroups;  
end;


class function TACustForm.GetCustomFormDialog(pFormID: integer;
                                                  pFormTitleBar, pFormTitleCaption: string;
                                                  pEmpID: integer): boolean;
var
  lCustomFormDialog: TACustForm;
begin
  Result := False;  //QM-876 EB Cleanup
  {1 Dialog Form - Create a Dialog form with the questions}
  lCustomFormDialog := TACustForm.Create(nil);
  lCustomFormDialog.CustomFormID := pFormID;
  try
    lCustomFormDialog.Caption := pFormTitleBar;
    lCustomFormDialog.lblTitle.Caption := pFormTitleCaption;
    lCUstomFormDialog.EmpID := pEmpID;
    if (pFormTitleCaption = '') then  {Hide top panel}
      lCustomFormDialog.pnlTop.Visible := False;
    lCustomFormDialog.LoadGroups;

    if (lCustomFormDialog.ShowModal = mrOK) then
      lCustomFormDialog.SaveAnswers;
    Result := True;
  finally
    FreeAndNil(lCustomFormDialog);
  end;

end;



procedure TACustForm.LoadGroups;
var
  i: integer;
  GroupNumList: TIntegerList;
  NumberOfQuestions: integer;

begin
  NumberOfQuestions := 0;
  GroupNumList := TIntegerList.Create;
  try

    fGroupCount := ACustomFormDM.GetNumberOfGroups(fCustomFormID);
    SetLength(FieldGroups, fGroupCount);
    ACustomFormDM.GetGroups(fCustomFormID, GroupNumList);

    {Define the Groups}
    for i := 0 to fGroupCount - 1 do begin
      NumberOfQuestions := ACustomFormDM.OpenGroup(fCustomFormID, GroupNumList.Items[i]);
      FieldGroups[i] := TFieldGroup.Create(MasterScrollBox, fCustomFormID, GroupNumList.Items[i], NumberOfQuestions);
      FieldGroups[i].RetrieveQuestions(Self.EmpID);
    end;
  finally
    FreeAndNil(GroupNumList);
  end;
end;



procedure TACustForm.RemoveGroups;
var
  i: integer;
begin
  for i := 0 to Length(FieldGroups) - 1 do
    FreeAndNil(FieldGroups[i]);
end;

procedure TACustForm.SaveAnswers;
var
  i: integer;
begin
  for i := 0 to fGroupCount - 1 do
    FieldGroups[i].SaveGroupAnswers;
end;




{ GROUPS: TFieldGroup }
constructor TFieldGroup.Create(pParent: TControl; pFormID: integer; pGroup: integer; pFieldCount: integer);
begin
  fSaveNoteFlag := False;
  FID := pFormID;
  GroupNumber := pGroup;
  FieldCount := pFieldCount;
  FrameList := TList.Create;


  Scrollbox := TcxScrollBox.Create(pParent);
  ScrollBox.Height := DefFrameHeight; // * pFieldCount;
  ScrollBox.Parent:= pParent as TWinControl;

  ScrollBox.Align:= alTop;


  if Odd(GroupNumber) then
    ScrollBox.Color := OddColor
  else
    ScrollBox.Color := EvenColor;
end;


destructor TFieldGroup.Destroy;
var
  i: integer;
begin
 inherited;
  for i := 0 to Framelist.Count - 1 do
    TCustomFormFrame(FrameList.Items[i]).Free;

  FreeAndNil(FrameList);
  FreeAndNil(TopLabel);
  FreeAndNil(ScrollBox);
end;





function TFieldGroup.GetFieldControlType(pFormField: string; pInfoType: string ): TcfControl;
begin
if pFormField = 'FF' then
    Result := cfFreeForm
  else if pformField = 'FL' then
    Result := cfFirstLast
  else if pformField = 'DATETIME' then
    Result := cfDateTime
  else if pformField = 'DATE' then
    Result := cfDate
  else if pformField = 'PHONE' then
    Result := cfPhone
  else if pformField = 'BOOL' then
    Result := cfCheckBox
  else if pFormField = 'CHKBOX' then
    Result := cfCheckBox
  else if pFormField = 'GRPHEAD' then
    Result := cfGroupHeading
       
//  else if ((pformField = 'DROPDOWN') OR (pformField = 'DROPDN')) and (pInfoType = ASSIGNED_TO) then
//    Result := cfLocDropDown
//  else if ((pformField = 'DROPDOWN') OR (pformField = 'DROPDN')) and (pInfoType <> ASSIGNED_TO) then begin
//    Result := cfCustomDropDown;
//  end;
  else
    Result := cfFreeForm;
end;

function TFieldGroup.RetrieveQuestions(pEmpID: integer; pForeignType: integer = 0; pForeignID: integer = 0): boolean;
var
  NumberOfFields: integer;
  CurFrame: TCustomFormFrame;
  pFieldInfo: TFieldInfo;
  FrameTop: integer;

begin
  Result := False;
  FrameTop := 0;
  NumberOfFields := ACustomFormDM.OpenGroup(Self.FID, Self.GroupNumber);
  Self.ScrollBox.Height := NumberOfFields * DefFrameHeight;

  If (NumberOfFields = 0) then begin
    Result := False;
    Exit;
  end;

  try
    ACustomFormDM.GetCustomFormGroup.First;
    while not ACustomFormDM.GetCustomFormGroup.EOF do begin
      pFieldInfo.FormID := Self.FID;
      pFieldInfo.Group := Self.GroupNumber;
      pFieldInfo.FieldID := ACustomFormDM.GetCustomFormGroup.FieldByName('form_field_id').AsInteger;
      pFieldInfo.FormField:= ACustomFormDM.GetCustomFormGroup.FieldByName('form_field').AsString;
      pFieldInfo.Question:=  ACustomFormDM.GetCustomFormGroup.FieldByName('field_description').AsString;
      pFieldInfo.InfoType:= ACustomFormDM.GetCustomFormGroup.FieldByName('field_info_type').AsString;
      pFieldInfo.Order := ACustomFormDM.GetCustomFormGroup.FieldByName('field_order').AsInteger;
      pFieldInfo.ControlType := GetFieldControlType(pFieldInfo.FormField, pFieldInfo.InfoType);
      pFieldInfo.Required := ACustomFormDM.GetCustomFormGroup.FieldByName('isrequired').AsBoolean;
      pFieldInfo.AnswerForeignID := pForeignID;
      pFieldInfo.AnswerForeignType := pForeignType;

      pFieldInfo.Answer := '';
      pFieldInfo.AnsweredBy := pEmpID;   {Need to replace}

      pFieldInfo.Color := Self.ScrollBox.Color;

      CurFrame :=  TCustomFormFrame.Create(pFieldInfo);

      CurFrame.Parent := ScrollBox;
      CurFrame.Align := alTop;

      FrameList.Add(CurFrame);

      ACustomFormDM.GetCustomFormGroup.Next;
    end;
    Result := True;  //QM-876 EB Code Cleanup
  finally
  {}
  end;
end;

function TFieldGroup.RetrieveQuestionsAndAnswers(pEmpID: integer;
                                                 pForeignType: integer = 0;
                                                 pForeignID: integer = 0): boolean;
var
  NumberOfFields: integer;
  CurFrame: TCustomFormFrame;
  lFieldInfo: TFieldInfo;
  FrameTop: integer;
  lPrevAnswer: string;
  lTotalHeight: integer;
begin
//  ShowMessage('ACF: 349 RetrieveQuestionsAndAnswers');
  lTotalHeight := 0;
  FrameTop := 0;
  NumberOfFields := ACustomFormDM.OpenGroup(Self.FID, Self.GroupNumber);
  Self.ScrollBox.Height := NumberOfFields * DefFrameHeight;

  If (NumberOfFields = 0) then begin
    Result := False;
    Exit;
  end;

  try
    ACustomFormDM.GetCustomFormGroup.First;
    while not ACustomFormDM.GetCustomFormGroup.EOF do begin
      lFieldInfo.FormID := Self.FID;
      lFieldInfo.Group := Self.GroupNumber;
      lFieldInfo.FieldID :=     ACustomFormDM.GetCustomFormGroup.FieldByName('form_field_id').AsInteger;
      lFieldInfo.FormField:=    ACustomFormDM.GetCustomFormGroup.FieldByName('form_field').AsString;
      lFieldInfo.Question:=     ACustomFormDM.GetCustomFormGroup.FieldByName('field_description').AsString;
      lFieldInfo.InfoType:=     ACustomFormDM.GetCustomFormGroup.FieldByName('field_info_type').AsString;
      lFieldInfo.Order :=       ACustomFormDM.GetCustomFormGroup.FieldByName('field_order').AsInteger;
      lFieldInfo.ControlType := GetFieldControlType(lFieldInfo.FormField, lFieldInfo.InfoType);
      lFieldInfo.Required :=    ACustomFormDM.GetCustomFormGroup.FieldByName('isrequired').AsBoolean;

      lFieldInfo.Answer := '';
      lFieldInfo.AnsweredBy := pEmpID;

      lFieldInfo.AnswerForeignID := pForeignID;
      lFieldInfo.AnswerForeignType := pForeignType;

      lFieldInfo.Color := Self.ScrollBox.Color;

      CurFrame :=  TCustomFormFrame.Create(lFieldInfo);
      lPrevAnswer := ACustomFormDM.GetAnswerDB(lFieldInfo);
      CurFrame.SetExistingAnswer(lFieldInfo);

      CurFrame.Parent := ScrollBox;
      CurFrame.Align := alTop;
      lTotalHeight := lTotalHeight + CurFrame.Height;

      FrameList.Add(CurFrame);
      if pForeignID > 0 then
        SaveNoteFlag := True;
      ACustomFormDM.GetCustomFormGroup.Next;
    end;
    ScrollBox.Height := lTotalHeight + 10;
  finally
  {}
  end;

end;

function TFieldGroup.SaveCustomFormFieldNote(pFieldInfo: TFieldInfo): Boolean;
begin
  if (SaveNoteFlag) and (pFieldInfo.AnswerHasChanged) then
    ACustomFormDM.SaveCFNote(pFieldInfo);
end;

procedure TFieldGroup.SaveGroupAnswers;
var
  i: Integer;
  CurFrame : TCustomFormFrame;
  SaveFieldData: TfieldInfo;

begin
  try
    for i := 0 to FrameList.Count - 1 do begin
      CurFrame := TCustomFormFrame(FrameList.Items[i]);

      {Everything but Dropdowns}
      SaveFieldData := CurFrame.SaveAnswer;
      if SaveFieldData.ControlType <> cfGroupHeading then begin    {Don't Save Headers}
        ACustomFormDM.SaveFieldAnswertoDB(SaveFieldData);
        If SaveFieldData.AnswerHasChanged then
          SaveCustomFormFieldNote(SaveFieldData);
      end;
    end;

  finally
    {Swallow for now}
  end;
end;

{ TGroupContainer }

procedure TGroupContainer.AddTitlePanel(pFormTitleCaption: string);
begin

end;

constructor TGroupContainer.Create(pFormID: integer; pFormTitleCaption: string;
                                   pContainer: TWinControl; pEmpID: integer;
                                   pForeignID: integer;
                                   pForm: TFormInfo;
                                   pEditExistingAnswers: boolean = True);
begin
  inherited Create;
  fFormID := pFormID;
  fEmpID := pEmpID;
  fAnswersForeignID := pForeignID;
  fForm := pForm;
//  ShowMessage('ACF 449: FORM ID: ' + inttostr(fFormID) + ', ' + 'Emp ID: ' + IntToSTr(fEmpID) + ', ' + 'ID: ' + IntToStr(pForeignID));
  fEditExistingAnswers := pEditExistingAnswers; {If form needs to record new answers each time, set to False}
  fContainerScrollBox := TScrollBox.Create(pContainer);
  fMsgPanel := TPanel.Create(pContainer);
  fMsgTop := TLabel.Create(pContainer);

  With fContainerScrollBox do begin
    Parent := pContainer;
    Visible := False;
    Align := alClient;
    Margins.Bottom := SBMARGIN;
    Margins.Left := SBMARGIN;
    Margins.Right := SBMARGIN;
    Margins.Top := SBMARGIN;
  end;

  With fMsgPanel do begin
    Parent := fContainerScrollBox;
    Visible := False;
    Align := alTop;
    Height := 18;
    Caption := '';
  end;

  With fMsgTop do begin
    Parent := fMsgPanel;
    Align := alClient;
    Caption := '';
    fMsgTop.Font.Size := 10;
    fMsgTop.Font.Color:= clMaroon;
    ParentFont := False;
  end;

  LoadGroups;
  fContainerScrollBox.Visible := True;
end;





destructor TGroupContainer.Destroy;
begin
  fContainerScrollBox.Visible := False;
  FreeAndNil(fMsgTop);
  FreeAndNil(fMsgPanel);
  FreeAndNil(fContainerScrollBox);

  inherited Destroy;
end;



procedure TGroupContainer.DisplayNoteforOtherExistingAnswers;
begin
//Not in the first version
//  if ACustomFormDM.HasAnswersforOtherEmps(fFormID, fAnswersForeignID, fEmpID) then begin
//    fMsgTop.Caption := 'Another Employee has submitted answers for these questions.';
//    fMsgPanel.Visible := True;
//  end;
end;

function TGroupContainer.HasExistingAnswers: boolean;
begin
  Result := ACustomFormDM.HasExistingAnswersDB(fFormId, fEmpID, fAnswersForeignID);
end;




procedure TGroupContainer.LoadGroups;
var
  i: integer;
  GroupNumList: TIntegerList;
  NumberOfQuestions: integer;
{NOTE: This is the GroupContainer LoadGroups!!!!}
begin
  NumberOfQuestions := 0;
  GroupNumList := TIntegerList.Create;
  try
    fGroupCount := ACustomFormDM.GetNumberOfGroups(fFormID);
//    ShowMessage('ACF LoadGroups 530: GetNumberOfGroups ' + intToStr(fGroupCount));
    SetLength(fFieldGroups, fGroupCount);
    ACustomFormDM.GetGroups(fFormID, GroupNumList);

    {Define the Groups}
    for i := 0 to fGroupCount - 1 do begin
      NumberOfQuestions := ACustomFormDM.OpenGroup(fFormID, GroupNumList.Items[i]);
      fFieldGroups[i] := TFieldGroup.Create(fContainerScrollBox, fFormID, GroupNumList.Items[i], NumberOfQuestions);
      if fEditExistingAnswers then
        fFieldGroups[i].RetrieveQuestionsAndAnswers(fEmpID,FForm.ForeignType, fAnswersForeignID)
      else
        fFieldGroups[i].RetrieveQuestions(fEmpID,FForm.ForeignType, fAnswersForeignID);
    end;

  finally
    FreeAndNil(GroupNumList);
  end;
end;


procedure TGroupContainer.RemoveGroups;
var
  i: integer;
begin
  for i := 0 to Length(fFieldGroups) - 1 do
    FreeAndNil(fFieldGroups[i]);
end;


procedure TGroupContainer.Save;
var
  i: integer;
begin
  for i := 0 to fGroupCount - 1 do
    fFieldGroups[i].SaveGroupAnswers;
end;


function TabCharsOnly(const S: string): string;
begin
  Result := StrKeepChars(S, ['A'..'Z', 'A'..'z', '0'..'9']);
end;


end.
