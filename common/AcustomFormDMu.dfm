object AcustomFormDM: TAcustomFormDM
  OldCreateOrder = False
  Height = 476
  Width = 262
  object GetCustomFormGroup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * '
      'from custom_form_field'
      'where (form_id =:pform_id) and '
      '(field_group =:pfield_group)'
      'order by field_order')
    Params = <
      item
        DataType = ftUnknown
        Name = 'pform_id'
      end
      item
        DataType = ftUnknown
        Name = 'pfield_group'
      end>
    Left = 170
    Top = 85
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pform_id'
      end
      item
        DataType = ftUnknown
        Name = 'pfield_group'
      end>
  end
  object GetGroupCount: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select distinct field_group'
      'from custom_form_field'
      'where form_id =:form_id'
      'and active'
      'order by field_group')
    Params = <
      item
        DataType = ftUnknown
        Name = 'form_id'
      end>
    Left = 40
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'form_id'
      end>
  end
  object GetAnswerRecord: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Select * from custom_form_answer'
      'where (form_field_id=:form_field_id)'
      'and (answered_by=:answered_by)'
      'and (foreign_id=:foreign_id)')
    Params = <
      item
        DataType = ftInteger
        Name = 'form_field_id'
      end
      item
        DataType = ftInteger
        Name = 'answered_by'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
    Left = 45
    Top = 195
    ParamData = <
      item
        DataType = ftInteger
        Name = 'form_field_id'
      end
      item
        DataType = ftInteger
        Name = 'answered_by'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
  end
  object CustomFormAnswer: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'custom_form_answer'
    Left = 175
    Top = 220
  end
  object GetSelectionList: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * '
      'from custom_form_field'
      'where (form_id =:pform_id) and '
      '(field_group =:pfield_group)'
      'order by field_order')
    Params = <
      item
        DataType = ftUnknown
        Name = 'pform_id'
      end
      item
        DataType = ftUnknown
        Name = 'pfield_group'
      end>
    Left = 180
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pform_id'
      end
      item
        DataType = ftUnknown
        Name = 'pfield_group'
      end>
  end
  object GetForm: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select cf.*, r.description linkdesc '
      'from custom_form cf, reference r'
      'where cf.active'
      'and (cf.link_ref_id =:ref_id) '
      'and (cf.link_ref_id=r.ref_id)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ref_id'
      end>
    Left = 100
    Top = 15
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ref_id'
      end>
  end
  object Database2: TDBISAMDatabase
    EngineVersion = '4.34 Build 7'
    DatabaseName = 'DBCF'
    Directory = 'D:\DEVELOPMENT\Sandbox\CustomForm\data'
    SessionName = 'Default'
    Left = 15
    Top = 10
  end
  object GetFormAnswers: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Select * from custom_form_answer'
      'where (form_id=:form_id)'
      'and (answered_by=:answered_by)'
      'and (foreign_id=:foreign_id)'
      'order by form_field_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'form_id'
      end
      item
        DataType = ftString
        Name = 'answered_by'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
    Left = 45
    Top = 140
    ParamData = <
      item
        DataType = ftInteger
        Name = 'form_id'
      end
      item
        DataType = ftString
        Name = 'answered_by'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
  end
  object UpdateExistingAnswer: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Update custom_form_answer'
      'set answer=:answer, '
      '      answer_date=CURRENT_TIMESTAMP,'
      '      deltastatus='#39'U'#39
      'where answer_id=:answer_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'answer'
      end
      item
        DataType = ftUnknown
        Name = 'answer_id'
      end>
    Left = 115
    Top = 270
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'answer'
      end
      item
        DataType = ftUnknown
        Name = 'answer_id'
      end>
  end
  object GetFormByFormID: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select cf.*'
      'from custom_form cf'
      'where active'
      'and (form_id =:form_id)'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'form_id'
      end>
    Left = 170
    Top = 25
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'form_id'
      end>
  end
  object GetAnswerRecordsOtherEmployees: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Select * from custom_form_field cff, custom_form_answer cfa'
      'where (cff.form_id=:form_id)'
      'and (cff.form_field_id = cfa.form_field_id)'
      'and (cfa.foreign_id=:foreign_id)'
      'order by cff.form_field_id, cfa.answered_by')
    Params = <
      item
        DataType = ftInteger
        Name = 'form_id'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
    Left = 105
    Top = 330
    ParamData = <
      item
        DataType = ftInteger
        Name = 'form_id'
      end
      item
        DataType = ftInteger
        Name = 'foreign_id'
      end>
  end
end
