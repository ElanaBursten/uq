object OdRangeSelectFrame: TOdRangeSelectFrame
  Left = 0
  Top = 0
  Width = 248
  Height = 63
  TabOrder = 0
  object FromLabel: TLabel
    Left = 8
    Top = 12
    Width = 28
    Height = 13
    Caption = 'From:'
  end
  object ToLabel: TLabel
    Left = 135
    Top = 12
    Width = 16
    Height = 13
    Caption = 'To:'
  end
  object DatesLabel: TLabel
    Left = 8
    Top = 38
    Width = 32
    Height = 13
    Caption = 'Dates:'
  end
  object DatesComboBox: TComboBox
    Left = 44
    Top = 34
    Width = 199
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = DatesComboBoxChange
  end
  object FromDateEdit: TcxDateEdit
    Left = 44
    Top = 8
    AutoSize = False
    EditValue = 36892d
    Properties.OnChange = FromDateEditPropertiesChange
    Properties.OnEditValueChanged = DateEditChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 21
    Width = 85
  end
  object ToDateEdit: TcxDateEdit
    Left = 156
    Top = 8
    AutoSize = False
    EditValue = 36892d
    Properties.OnEditValueChanged = DateEditChange
    Properties.OnValidate = ToDateEditPropertiesValidate
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 1
    Height = 21
    Width = 86
  end
end
