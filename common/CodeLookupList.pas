unit CodeLookupList;

// This object simplifies TStringList lookups when using a string code.

interface

uses Classes, SysUtils, OdMiscUtils;

type
  // So we can store an extra string with each item in the list
  TItemString = class(TObject)
  strict private
    fText: string;
    procedure SetText(const Value: string);
    function GetText: string;
  public
    constructor Create(const AText: string);
    property Text: string read GetText write SetText;
  end;

  TCodeLookupList = class(TObject)
  strict private
    FCodeList: TStrings;
  public
    constructor Create(List: TStrings);
    destructor Destroy; override;
    procedure Clear;
    function Add(const Code, Description: string): Integer;
    function GetCode(const Description: string): string;
    function GetDescription(const Code: string): string;
    function GetIndexForCode(const Code: string): Integer;
  end;

implementation

{ TItemString }
constructor TItemString.Create(const AText: string);
begin
  inherited Create;
  Text := AText;
end;

function TItemString.GetText: string;
begin
  Result := FText;
end;

procedure TItemString.SetText(const Value: string);
begin
  FText := Value;
end;

{ TCodeLookupList }
constructor TCodeLookupList.Create(List: TStrings);
begin
  inherited Create;
  // Reference the component's TStrings list
  Assert(Assigned(List));
  FCodeList := List;
  Clear;
end;

destructor TCodeLookupList.Destroy;
begin
  FCodeList.FreeAndNilObjects;
  inherited;
end;

function TCodeLookupList.Add(const Code, Description: string): Integer;
var
  Desc: string;
  StrObj: TItemString;
begin
  StrObj := TItemString.Create(Code); // freed when FCodeList is cleared
  if IsEmpty(Code) then
    Desc := Description
  else
    Desc := Code + ' - ' + Description;

  Result := FCodeList.AddObject(Desc, StrObj);
end;

procedure TCodeLookupList.Clear;
begin
  FCodeList.FreeAndNilObjects;
  FCodeList.Clear;
end;

function TCodeLookupList.GetCode(const Description: string): string;
var
  Idx: Integer;
begin
  Idx := FCodeList.IndexOf(Description);
  if Idx > -1 then
    Result := TItemString(FCodeList.Objects[Idx]).Text
  else
    Result := '';
end;

function TCodeLookupList.GetDescription(const Code: string): string;
var
  I: Integer;
begin
  for I := 0 to FCodeList.Count - 1 do begin
    if TItemString(FCodeList.Objects[I]).Text = Code then begin
      Result := FCodeList.Strings[I];
      Break;
    end;
  end;
end;

function TCodeLookupList.GetIndexForCode(const Code: string): Integer;
begin
  Result := FCodeList.IndexOf(GetDescription(Code));
end;

end.
