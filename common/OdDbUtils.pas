unit OdDbUtils;

{
  Oasis Digital Database Utility Functions
  Copyright 2014 Oasis Digital
}

interface

uses
  {$IF CompilerVersion >= 24} // Delphi
    Data.Win.ADODB,
  {$IFEND CompilerVersion}
  SysUtils, DB, Classes, JCLStrings, OdMiscUtils, DBCtrls, graphics,
  Controls, StdCtrls, ExtCtrls;

{$IF CompilerVersion >= 16} // Delphi 8, 2005 or greater
  {$DEFINE ClassHelpers}
{$IFEND}

const
  CleanupAllStringFields = '<AllStringFields>';

  {QMANTWO-577 EB Use this color for DBText highlighting}
  HIGHLIGHTCOLOR = clSkyBlue; //$F0CAA6;    //clSkyBlue

{$IFDEF ClassHelpers}
type
  TDatasetHelper = class helper for TDataSet
  public
    procedure PostIfEditing;
    function Editing: Boolean;
    function HasField(const FieldName: string): Boolean;
    function SetFieldTag(const FieldName: string; Tag: Integer): Boolean;
    function SumIntField(const FieldName: string): Integer;
    function SumDoubleField(const FieldName: string): Double;
    function HasRecords: Boolean;
  end;

  {$IF CompilerVersion >= 24} // Delphi
  TADOConnectionHelper = class helper for TADOConnection
  public
    procedure GetTableNamesExt(List: TStrings; IncludeSystemTables,
    IncludeViews: Boolean);
  end;
  {$IFEND CompilerVersion}
{$ENDIF ClassHelpers}


  {$IFNDEF UNICODE}
type
  TBookmark = string;
  const EmptyBookmark = '';
  {$ELSE}
  const EmptyBookmark = nil;
  {$ENDIF}

procedure MarkCurrencyField(D: TDataSet; FieldName: string);
procedure CreateDefaultFields(DataSet: TDataSet);

function AddLookupField(FieldName: string; DataSet: TDataSet;
  KeyFields: string; LookupDataSet: TDataSet; LookupKeyFields: string;
  LookupResultField: string; Size: Integer = 50; Name: string = '';
  DisplayLabel: string = ''): TStringField;

function AddCalculatedField(FieldName: string; DataSet: TDataSet;
  FieldClass: TFieldClass; Size: Word): TField;

function MakeSaveFile(const FileName: string): TFileStream;

procedure SaveDelimToStream(D: TDataSet; Stream: TStream; const Delimiter: string;
  IncludeHeader: Boolean; CleanupFields: TStrings = nil; SkipFields: TStrings = nil; const InvalidChars: TCharValidator = nil); overload;
{$IFDEF UNICODE}
procedure SaveDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: string;
  IncludeHeader: Boolean; CleanupFields: TStrings = nil; SkipFields: TStrings = nil; const InvalidChars: TCharValidator = nil); overload;
procedure SaveOrderedDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: string;
  IncludeHeader: Boolean; OutputFields: TStrings; MemoFields: TStrings = nil; const InvalidChars: TCharValidator = nil);
{$ENDIF}
procedure SaveDelimToFile(D: TDataSet; const FileName, Delimiter: string;
  IncludeHeader: Boolean; MemoFields: TStrings = nil; SkipFields: TStrings = nil; const InvalidChars: TCharValidator=nil);
{$IFDEF UNICODE}
procedure SimpleSaveDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: String;
    IncludeHeader: Boolean; const InvalidChar: string);
{$ENDIF}
procedure ExportTSV(D: TDataSet; const FileName: string; MemoFields: TStrings = nil; const InvalidChars: TCharValidator = nil);
procedure ExportCSV(D: TDataSet; const FileName: string; MemoFields: TStrings = nil; const InvalidChars: TCharValidator = nil);

procedure SetRequiredFields(DataSet: TDataSet; RequiredFields: array of string);
procedure RefreshDataSet(DataSet: TDataSet);
procedure OpenDataSet(DataSet: TDataSet);
procedure CloseDataSet(DataSet: TDataSet);
procedure UpdateAndPostDataSet(DataSet: TDataSet);
function InsertingDataSet(DataSet: TDataSet): Boolean;
function EditingDataSet(DataSet: TDataSet): Boolean;
function HasRecords(DataSet: TDataSet): Boolean;
procedure EditDataSet(DataSet: TDataSet);
procedure PostDataSet(DataSet: TDataSet);
procedure PostDataSetIfModifiedOrCancel(DataSet: TDataSet);
procedure PostAndCloseDataSet(DataSet: TDataSet);
procedure CancelDataSet(DataSet: TDataSet);
procedure SetDateFieldDisplayFormats(DataSet: TDataSet; const DisplayFormat: string);
procedure SetCurrencyFieldDisplayFormats(DataSet: TDataSet; const DisplayFormat: string = '$#,##0.00');
function SetFieldDisplayFormat(DataSet: TDataSet; const FieldName: string; const DisplayFormat: string = '$#,##0.00'): Boolean;
function SetFieldDisplayEditFormats(DataSet: TDataSet; const FieldName: string; const DisplayFormat: string = '$#,##0.00'; const EditFormat: string = ''): Boolean;
function SetFieldDisplayValues(DataSet: TDataSet; const FieldName: string; const DisplayValues: string = 'Yes;No'): Boolean;
function FieldWasEdited(DataSet: TDataSet; const FieldName: string; IgnoreInserts: Boolean = False): Boolean;
function AnyFieldWasEdited(DataSet: TDataSet; RoundTimeMS: Boolean = False): Boolean;
function TotalField(DataSet: TDataSet; const FieldName: string): Double;

function AllFieldsHaveValue(DataSet: TDataSet; Fields: array of string; Value: Variant): Boolean;
function OneFieldHasAValue(DataSet: TDataSet; Fields: array of string): Boolean;
function AnyFieldsDiffer(Data1, Data2: TDataSet; Fields: array of string; const FloatTolerance: Double = 0.001): Boolean;
procedure GetDataSetFieldValueList(List: TStrings; DataSet: TDataSet; const FieldName: string; FirstValue: string = ''; IncludeBlank: Boolean = True);
function GetDataSetFieldPlainString(DataSet: TDataSet; const FieldName: string; MaxRecords: Integer = -1; Separator: Char = ','): String;
//function GetUniqueFieldValues(DataSet: TDataSet; const Field, Delimiter: string): string; overload;
procedure GetUniqueFieldValues(DataSet: TDataSet; const Field: string; Values: TStrings); overload;
procedure CloneRecord(Dest: TDataSet); overload;
procedure CloneRecord(Dest: TDataSet; IgnoreFields: array of string); overload;
procedure CopyRecord(Source, Dest: TDataSet);
function GetSQLInClauseForStrings(Items: TStrings; Quote: Boolean = True): string;
function GetSQLInClauseForDataSet(DataSet: TDataSet; const FieldName: string; const Default: string = ''): string;
function IntKeyToDBVariant(Key: Integer): Variant;
function HaveField(DataSet: TDataSet; const FieldName: string): Boolean;
function HaveNonNullField(DataSet: TDataSet; const FieldName: string): Boolean;
function FieldToSQLValue(Field: TField): string;
function RoundSQLServerTimeMS(DateTime: TDateTime): TDateTime;

procedure SetFieldsUpdatable(DataSet: TDataSet; Updatable: Boolean); overload;
procedure SetFieldsUpdatable(DataSet: TDataSet; Updatable: Boolean; Fields: array of string); overload;
procedure SetFieldUpdatable(Field: TField; Updatable: Boolean = False);

// These functions require clauses on separate lines right now (it does not parse comments, etc.)
function SetWhereClause(const SQL, Where: string): string; overload;
function SetOrderByClause(const SQL, OrderBy: string): string; overload;
procedure SetWhereClause(SQL: TStringList; const Where: string); overload;
procedure SetOrderByClause(SQL: TStringList; const OrderBy: string); overload;

// The Template vars must be in a %FieldName% format
function ReplaceVars(const Template: string; DataSet: TDataSet; OtherVars: TStrings = nil): string;

procedure CopyDBMemo(ADBMemo: TDBMemo; HLColor: TColor = HighlightColor);
procedure CopyDBText(ADBText: TDBText; HLColor: TColor = HighlightColor);
procedure CopyLabelText(ALabel: TLabel; HLColor: TColor = HighlightColor);
procedure CopyDBEdit(ADBEdit: TDBEdit; HLColor: TColor = HighlightColor);
procedure ClearSelectionsDBText(APanel: TPanel; CtrlsTransparent: boolean = True);

type
  NullField = class(TField)
  protected
    procedure SetAsString(const Value: string); override;
    procedure SetAsDateTime(Value: TDateTime); override;
    procedure SetAsFloat(Value: Double); override;
    procedure SetAsInteger(Value: Integer); override;
  end;

implementation

uses
  Variants, OdIsoDates, StrUtils, DateUtils, ClipBrd;

procedure MarkCurrencyField(D: TDataSet; FieldName: string);
begin
  TFloatField(D.FieldByName(FieldName)).Currency := True;
end;

procedure CreateDefaultFields(DataSet: TDataSet);
var
  i: Integer;
begin
  DataSet.FieldDefs.Update;
  DataSet.Close;
  for i := 0 to DataSet.FieldDefs.Count - 1 do
    if DataSet.FindField(DataSet.FieldDefs[i].Name) = nil then
      DataSet.FieldDefs[i].CreateField(DataSet);
end;

function AddLookupField(FieldName: string; DataSet: TDataSet;
  KeyFields: string; LookupDataSet: TDataSet; LookupKeyFields: string;
  LookupResultField: string; Size: Integer = 50; Name: string = '';
  DisplayLabel: string = ''): TStringField;
var
  PrevField: TField;
begin
  if Name = '' then
    Name := FieldName;

  Assert(FieldName <> '');
  // Never try to create the same lookup field twice
  PrevField := DataSet.FindField(FieldName);
  if Assigned(PrevField) then begin
    Assert(PrevField.FieldKind = fkLookup);
    Result := nil;
    Exit;
  end;
  CreateDefaultFields(DataSet);

  Result := TStringField.Create(DataSet);
  Result.FieldKind := fkLookup;
  Result.FieldName := FieldName;
  Result.Size := Size;
  Result.Name := Name;
  Result.DisplayLabel := DisplayLabel;

  Result.DataSet := DataSet;
  Result.LookupDataSet := LookupDataSet;

  Result.KeyFields := KeyFields;
  Result.LookupKeyFields := LookupKeyFields;
  Result.LookupResultField := LookupResultField;
end;

function AddCalculatedField(FieldName: string; DataSet: TDataSet;
  FieldClass: TFieldClass; Size: Word): TField;
begin
  Result := DataSet.FindField(FieldName);
  if Result = nil then begin
    CreateDefaultFields(DataSet);

    Assert(Assigned(FieldClass));
    Result := FieldClass.Create(DataSet);
    try
      Result.FieldName := FieldName;
      Result.Size := Size;
      Result.Calculated := True;
      Result.ReadOnly := True;
      Result.DataSet := DataSet;
      Result.Name := DataSet.Name + FieldName;
    except
      FreeAndNil(Result);
      raise;
    end;
  end;
end;

{$IFDEF UNICODE}
procedure SaveDelimToStream(D: TDataSet; Stream: TStream; const Delimiter: String;
    IncludeHeader: Boolean; CleanupFields: TStrings; SkipFields: TStrings; const InvalidChars: TCharValidator);
begin
  SaveDelimToStream(etUTF8, D, Stream, Delimiter, IncludeHeader, CleanupFields, SkipFields, InvalidChars);
end;
{$ELSE} //TODO: remove this version of SaveDelimToStream once QML has been replaced with QML2
procedure SaveDelimToStream(D: TDataSet; Stream: TStream; const Delimiter: String;
    IncludeHeader: Boolean; CleanupFields: TStrings; SkipFields: TStrings; const InvalidChars: TCharValidator);

  function IsCleanupField(Field: TField): Boolean;
  begin
    Assert(Assigned(Field));
    Result := False;
    if Field.DataType in [ftMemo, ftFmtMemo {$IF CompilerVersion >= 16}, ftWideMemo{$IFEND}] then
      Result := True;
    if Assigned(CleanupFields) then
      if (CleanupFields.IndexOf(Field.FieldName) >= 0) or
        ((CleanupFields.IndexOf(CleanupAllStringFields) >= 0) and (Field.DataType in [ftString, ftFixedChar, ftWideString {$IF CompilerVersion >= 16}, ftFixedWideChar{$IFEND}])) then
          Result := True;
  end;

var
  Data: string;
  i: Integer;
  Field: TField;
begin
  D.DisableControls;
  try
    if IncludeHeader then
      for i := 0 to D.Fields.Count - 1 do begin
        Field := D.Fields[i];
        Data := Field.DisplayName;

        // KNOWN ISSUE - incorrect results if skipping the last field!
        if (SkipFields=nil) or (SkipFields.IndexOf(Field.FieldName) < 0) then begin
          if i = D.Fields.Count - 1 then
            Data := Data + #13#10
          else
            Data := Data + Delimiter;

          Stream.WriteString(Data);
        end;
      end;

    while not D.EOF do begin
      for i := 0 to D.Fields.Count - 1 do begin
        Field := D.Fields[i];
        // Strip CR/LF, Tabs, and extra whitespace from memo/note fields
        if (SkipFields=nil) or (SkipFields.IndexOf(Field.FieldName) < 0) then begin
          if IsCleanupField(Field) then
            Data := CompressWhiteSpace(Field.AsString)
          else
            Data := Field.DisplayText;

          if i = D.Fields.Count - 1 then
            Data := Data + #13#10
          else
            Data := Data + Delimiter;

          if Assigned(InvalidChars) then
            Data := JclStrings.StrReplaceChars(Data, InvalidChars, ' ');

          Stream.WriteString(Data);
        end;
      end;
      D.Next;
    end;
  finally
    D.EnableControls;
  end;

  Stream.Size := Stream.Position;
end;
{$ENDIF}

{$IFDEF UNICODE}
procedure SaveDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: String;
    IncludeHeader: Boolean; CleanupFields: TStrings; SkipFields: TStrings; const InvalidChars: TCharValidator); overload;

  function IsCleanupField(Field: TField): Boolean;
  begin
    Assert(Assigned(Field));
    Result := False;
    if Field.DataType in [ftMemo, ftFmtMemo {$IF CompilerVersion >= 16}, ftWideMemo{$IFEND}] then
      Result := True;
    if Assigned(CleanupFields) then
      if (CleanupFields.IndexOf(Field.FieldName) >= 0) or
        ((CleanupFields.IndexOf(CleanupAllStringFields) >= 0) and (Field.DataType in [ftString, ftFixedChar, ftWideString {$IF CompilerVersion >= 16}, ftFixedWideChar{$IFEND}])) then
          Result := True;
  end;

var
  Data: string;
  i: Integer;
  Field: TField;
  Writer: TStreamWriter;
begin
  D.DisableControls;
  try
    Writer := TStreamWriter.Create(Stream, GetQMEncoding(QMEncodingType), Stream.Size);
    if IncludeHeader then
      for i := 0 to D.Fields.Count - 1 do begin
        Field := D.Fields[i];
        Data := Field.DisplayName;

        // KNOWN ISSUE - incorrect results if skipping the last field!
        if (SkipFields=nil) or (SkipFields.IndexOf(Field.FieldName) < 0) then begin
          if i = D.Fields.Count - 1 then
            Data := Data + #13#10
          else
            Data := Data + Delimiter;

          Writer.Write(Data);
        end;
      end;

    while not D.EOF do begin
      for i := 0 to D.Fields.Count - 1 do begin
        Field := D.Fields[i];
        // Strip CR/LF, Tabs, and extra whitespace from memo/note fields
        if (SkipFields=nil) or (SkipFields.IndexOf(Field.FieldName) < 0) then begin
          if IsCleanupField(Field) then
            Data := CompressWhiteSpace(Field.AsString)
          else
            Data := Field.DisplayText;

          if i = D.Fields.Count - 1 then
            Data := Data + #13#10
          else
            Data := Data + Delimiter;

          if Assigned(InvalidChars) then
            Data := JclStrings.StrReplaceChars(Data, InvalidChars, ' ');

          Writer.Write(Data);
        end;
      end;
      D.Next;
    end;
  finally
    D.EnableControls;
    FreeAndNil(Writer);
  end;

  Stream.Size := Stream.Position;
end;

procedure SimpleSaveDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: String;
  IncludeHeader: Boolean; const InvalidChar: string);    //QMANTWO-784 made for TicketsDueExport
var
  Data: string;
  Data2: string;
  i: Integer;
  Field: TField;
  Writer: TStreamWriter;
begin
  D.DisableControls;
  try
    Writer := TStreamWriter.Create(Stream, GetQMEncoding(QMEncodingType), Stream.Size);
    if IncludeHeader then
      for i := 0 to D.Fields.Count - 1 do
      begin
        Field := D.Fields[i];
        Data := Field.DisplayName;

        if i = D.Fields.Count - 1 then
          Data := Data + #13#10
        else
          Data := Data + Delimiter;

        Writer.Write(Data);
      end; // if IncludeHeader then

    while not D.EOF do
    begin
      for i := 0 to D.Fields.Count - 1 do
      begin
        Field := D.Fields[i];
        // Strip CR/LF, Tabs, and extra whitespace from memo/note fields
        Data := Field.AsString;

        if i = D.Fields.Count - 1 then
          Data := Data + #13#10
        else
        begin
          Data := StringReplace(Data, InvalidChar, ' ', [rfReplaceAll]) + Delimiter;
          Data := StringReplace(Data, #10, ' ', [rfReplaceAll]);
        end;

        Writer.Write(Data);
      end;
      D.Next;
    end; // while not D.EOF do
  finally
    D.EnableControls;
    FreeAndNil(Writer);
  end;

  Stream.Size := Stream.Position;
end;


procedure SaveOrderedDelimToStream(QMEncodingType: TQMEncodingType; D: TDataSet; Stream: TStream; const Delimiter: string;
  IncludeHeader: Boolean; OutputFields: TStrings; MemoFields: TStrings; const InvalidChars: TCharValidator);

  function IsMemoField(Field: TField): Boolean;
  begin
    Assert(Assigned(Field));
    Result := False;
    if Field.DataType in [ftMemo, ftFmtMemo] then
      Result := True;
    if Assigned(MemoFields) then
      if MemoFields.IndexOf(Field.FieldName) >= 0 then
        Result := True;
  end;

var
  Data: string;
  i: Integer;
  Field: TField;
  Writer: TStreamWriter;
begin
  Assert(OutputFields.Count <= D.Fields.Count);

  D.DisableControls;
  try
    Writer := TStreamWriter.Create(Stream, GetQMEncoding(QMEncodingType), Stream.Size);
    if IncludeHeader then
      for i := 0 to OutputFields.Count - 1 do begin
        Field := D.FieldByName(OutputFields[i]);
        Data := Field.DisplayName;

        if i = OutputFields.Count - 1 then
          Data := Data + #13#10
        else
          Data := Data + Delimiter;

        Writer.Write(Data);
      end;

    D.First;
    while not D.EOF do begin
      for i := 0 to OutputFields.Count - 1 do begin
        Field := D.FieldByName(OutputFields[i]);
        // Strip CRs and Tabs from memo/note fields
        if IsMemoField(Field) then
          Data := CompressWhiteSpace(Field.AsString)
        else
          Data := Field.DisplayText;

        if i = OutputFields.Count - 1 then
          Data := Data + #13#10
        else
          Data := Data + Delimiter;

        if Assigned(InvalidChars) then
          Data := JCLStrings.StrReplaceChars(Data, InvalidChars, ' ');

        Writer.Write(Data);
      end;
      D.Next;
    end;
    D.First;
  finally
    FreeAndNil(Writer);
    D.EnableControls;
  end;

  Stream.Size := Stream.Position;
end;
{$ENDIF}

function MakeSaveFile(const FileName: string): TFileStream;
begin
  Result := TFileStream.Create(FileName, fmCreate or fmShareExclusive);
end;

procedure SaveDelimToFile(D: TDataSet; const FileName, Delimiter: string;
  IncludeHeader: Boolean; MemoFields: TStrings; SkipFields: TStrings; const InvalidChars: TCharValidator);
var
  SaveFile: TFileStream;
begin
  SaveFile := MakeSaveFile(FileName);
  try
{$IFDEF BILLING}                                                               //QMANTWO-656
    SaveDelimToStream(etANSI, D, SaveFile, Delimiter, IncludeHeader, MemoFields, SkipFields, InvalidChars);
{$ELSE}
    SaveDelimToStream(D, SaveFile, Delimiter, IncludeHeader, MemoFields, SkipFields, InvalidChars);
{$ENDIF}
  finally
    FreeAndNil(SaveFile);
  end;
end;

procedure ExportTSV(D: TDataSet; const FileName: string; MemoFields: TStrings; const InvalidChars: TCharValidator);
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  try
    Stream.SetSize(100000); // start with it large to avoid reallocations
{$IFDEF BILLING}                                                               //QMANTWO-656
    SaveDelimToStream(etANSI, D, Stream, #9, True, MemoFields, nil, InvalidChars);
{$ELSE}
    SaveDelimToStream(D, Stream, #9, True, MemoFields, nil, InvalidChars);
{$ENDIF}
    Stream.SaveToFile(FileName);
  finally
    FreeAndNil(Stream);
  end;
end;

procedure ExportCSV(D: TDataSet; const FileName: string; MemoFields: TStrings; const InvalidChars: TCharValidator);
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  try
    Stream.SetSize(100000); // start with it large to avoid reallocations
{$IFDEF BILLING}                                                                //QMANTWO-656
    SaveDelimToStream(etANSI, D, Stream, #44, True, MemoFields, nil, InvalidChars);
{$ELSE}
    SaveDelimToStream(D, Stream, #44, True, MemoFields, nil, InvalidChars);
{$ENDIF}
    Stream.SaveToFile(FileName);
  finally
    FreeAndNil(Stream);
  end;
end;

procedure SetRequiredFields(DataSet: TDataSet; RequiredFields: array of string);
var
  i: Integer;
begin
  Assert(Assigned(DataSet));
  for i := Low(RequiredFields) to High(RequiredFields) do
    DataSet.FieldByName(RequiredFields[i]).Required := True;
end;

procedure RefreshDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  DataSet.DisableControls;
  try
    if DataSet.Active then
      DataSet.Close;
    DataSet.Open;
  finally
    DataSet.EnableControls;
  end;
end;

procedure UpdateAndPostDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if EditingDataSet(DataSet) then begin
    DataSet.UpdateRecord;
    DataSet.Post;
  end;
end;

function EditingDataSet(DataSet: TDataSet): Boolean;
begin
  Assert(Assigned(DataSet));
  Result := (DataSet.State in dsEditModes);
end;

function InsertingDataSet(DataSet: TDataSet): Boolean;
begin
  Assert(Assigned(DataSet));
  Result := (DataSet.State = dsInsert);
end;

function HasRecords(DataSet: TDataSet): Boolean;
begin
  Assert(Assigned(DataSet));
  Result := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure EditDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if not EditingDataSet(DataSet) then begin
    if not DataSet.Active then
      DataSet.Open;
    DataSet.Edit;
  end;
end;

procedure PostDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if EditingDataSet(DataSet) then begin
    DataSet.UpdateRecord;
    DataSet.Post;
  end;
end;

procedure PostDataSetIfModifiedOrCancel(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if EditingDataSet(DataSet) then begin
    DataSet.UpdateRecord; // Gets updates if the user has not moved out of the field
    if DataSet.Modified then
      DataSet.Post
    else
      DataSet.Cancel;
  end;
end;

procedure PostAndCloseDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  PostDataSet(DataSet);
  DataSet.Close;
end;

procedure OpenDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if not DataSet.Active then
    DataSet.Open;
end;

procedure CloseDataSet(DataSet: TDataSet);
begin
  if DataSet.Active then
    DataSet.Close;
end;

procedure CancelDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  if EditingDataSet(DataSet) then
    DataSet.Cancel;
end;

procedure SetDateFieldDisplayFormats(DataSet: TDataSet; const DisplayFormat: string);
var
  i: Integer;
begin
  Assert(Assigned(DataSet));
  for i := 0 to DataSet.Fields.Count - 1 do begin
    if DataSet.Fields[i] is TDateTimeField then
      (DataSet.Fields[i] as TDateTimeField).DisplayFormat := DisplayFormat;
  end;
end;

procedure SetCurrencyFieldDisplayFormats(DataSet: TDataSet; const DisplayFormat: string);
var
  i: Integer;
  Field: TField;
begin
  Assert(Assigned(DataSet));
  for i := 0 to DataSet.Fields.Count - 1 do begin
    Field := DataSet.Fields[i];
    if (Field is TCurrencyField) or (Field is TBCDField) then
      (DataSet.Fields[i] as TNumericField).DisplayFormat := DisplayFormat;
  end;
end;

function SetFieldDisplayFormat(DataSet: TDataSet; const FieldName, DisplayFormat: string): Boolean;
begin
  Result := SetFieldDisplayEditFormats(DataSet, FieldName, DisplayFormat, '');
end;

function SetFieldDisplayEditFormats(DataSet: TDataSet; const FieldName: string; const DisplayFormat: string; const EditFormat: string): Boolean;
var
  Field: TField;
begin
  Result := False;
  Assert(Assigned(DataSet));
  Assert(FieldName <> '');
  Field := DataSet.FindField(FieldName);
  if Assigned(Field) then begin
    if Field is TNumericField then begin
      (Field as TNumericField).DisplayFormat := DisplayFormat;
      if EditFormat <> '' then
        (Field as TNumericField).EditFormat := EditFormat;
      Result := True;
    end else if Field is TDateTimeField then begin
      (Field as TDateTimeField).DisplayFormat := DisplayFormat;
      Result := True;
    end;
  end;
end;

function SetFieldDisplayValues(DataSet: TDataSet; const FieldName: string; const DisplayValues: string = 'Yes;No'): Boolean;
var
  Field: TField;
begin
  Result := False;
  Assert(Assigned(DataSet));
  Assert(FieldName <> '');
  Field := DataSet.FindField(FieldName);
  if Assigned(Field) then begin
    if Field is TBooleanField then begin
      (Field as TBooleanField).DisplayValues := DisplayValues;
      Result := True;
    end;
  end;
end;

function FieldWasEdited(DataSet: TDataSet; const FieldName: string; IgnoreInserts: Boolean): Boolean;
var
  Field: TField;
begin
  Assert(Assigned(DataSet));
  Field := DataSet.FindField(FieldName);
  if not Assigned(Field) then
    raise Exception.CreateFmt('Field %s not found in dataset %s', [FieldName, DataSet.Name]);
  if InsertingDataSet(DataSet) and IgnoreInserts then
    Result := False
  else
    Result := not (Field.OldValue = Field.NewValue);
end;

function AnyFieldWasEdited(DataSet: TDataSet; RoundTimeMS: Boolean): Boolean;
var
  i: Integer;
  Field: TField;
  OldDateTime, NewDateTime: TDateTime;
begin
  Assert(Assigned(DataSet));
  Result := False;
  for i := 0 to DataSet.Fields.Count - 1 do begin
    Field := DataSet.Fields[i];
    if RoundTimeMS and (Field is TDateTimeField) then begin
      OldDateTime := Field.OldValue;
      NewDateTime := Field.AsDateTime;
      Result := not (RoundSQLServerTimeMS(OldDateTime) = RoundSQLServerTimeMS(NewDateTime));
    end
    else
      Result := not (Field.OldValue = Field.Value);
    if Result then
      Exit;
  end;
end;

function TotalField(DataSet: TDataSet; const FieldName: string): Double;
var
  Bookmark: TBookmark;
begin
  Result := 0.0;
  Bookmark := DataSet.Bookmark;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.EOF do begin
      Result := Result + DataSet.FieldByName(FieldName).AsFloat;
      DataSet.Next;
    end;
  finally
    DataSet.Bookmark := Bookmark;
    DataSet.EnableControls;
  end;
end;

function AllFieldsHaveValue(DataSet: TDataSet; Fields: array of string; Value: Variant): Boolean;
var
  i: Integer;
begin
  Result := True;
  Assert(Assigned(DataSet));
  Assert(Length(Fields) > 0);
  for i := Low(Fields) to High(Fields) do begin
    if DataSet.FieldByName(Fields[i]).Value <> Value then begin
      Result := False;
      Break;
    end;
  end;
end;

procedure CloneRecord(Dest: TDataSet);
begin
  CloneRecord(Dest, []);
end;

procedure CloneRecord(Dest: TDataSet; IgnoreFields: array of string);
var
  i: Integer;
  FieldValues: array of Variant;
  OldRO: Boolean;
  Field: TField;
begin
  Assert(Assigned(Dest));
  if not Dest.Active then
    Exit;
  PostDataSet(Dest);

  SetLength(FieldValues, Dest.FieldCount);

  for i := 0 to Dest.FieldCount - 1 do
    FieldValues[i] := Dest.Fields[i].Value;

  Dest.Append;

  for i := 0 to Length(FieldValues) - 1 do
  begin
    Field := Dest.Fields[i];
    if not StringInArray(Field.FieldName, IgnoreFields) then begin
      Assert(Assigned(Field));
      OldRO := Field.ReadOnly;
      Field.ReadOnly := False;
      Field.Value := FieldValues[i];
      Field.ReadOnly := OldRO;
    end;
  end;
end;

function OneFieldHasAValue(DataSet: TDataSet; Fields: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  Assert(Assigned(DataSet));
  Assert(Length(Fields) > 0);
  for i := Low(Fields) to High(Fields) do begin
    if Trim(DataSet.FieldByName(Fields[i]).AsString) <> '' then begin
      Result := True;
      Break;
    end;
  end;
end;

function AnyFieldsDiffer(Data1, Data2: TDataSet; Fields: array of string; const FloatTolerance: Double = 0.001): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(Fields) to High(Fields) do begin
    case Data1.FieldByName(Fields[i]).DataType of
      ftFloat:
        Result := not FloatEquals(Data1.FieldByName(Fields[i]).Value,
          Data2.FieldByName(Fields[i]).Value, FloatTolerance);
      ftDateTime:
        Result := not SameDateTime(Data1.FieldByName(Fields[i]).AsDateTime,
          Data2.FieldByName(Fields[i]).AsDateTime);
      else
        Result := Data1.FieldByName(Fields[i]).Value <>
          Data2.FieldByName(Fields[i]).Value;
    end;
    if Result then
      Exit;
  end;
end;

(* D2007+
function GetUniqueFieldValues(DataSet: TDataSet; const Field, Delimiter: string): string;
var
  Values: TStringList;
  Value: string;
begin
  Result := '';
  Values := TStringList.Create;
  try
    Values.Duplicates := dupIgnore;
    DataSet.First;
    while not DataSet.Eof do begin
      Value := DataSet.FieldByName(Field).AsString;
      if NotEmpty(Value) then
        Values.Add(Value);
      DataSet.Next;
      Result := Values.DelimitedLines(Delimiter);
    end;
  finally
    FreeAndNil(Values);
  end;
end;
*)

procedure GetUniqueFieldValues(DataSet: TDataSet; const Field: string; Values: TStrings);
var
  Value: string;
begin
  Assert(Assigned(Values));
  Assert(NotEmpty(Field));
  Values.Clear;
  DataSet.First;
  while not DataSet.Eof do begin
    Value := DataSet.FieldByName(Field).AsString;
    if NotEmpty(Value) then begin
      if not (Values.IndexOf(Value) >= 0) then
        Values.Add(Value);
    end;
    DataSet.Next;
  end;
end;

procedure GetDataSetFieldValueList(List: TStrings; DataSet: TDataSet; const FieldName: string; FirstValue: string; IncludeBlank: Boolean);
var
  Field: TField;
begin
  Assert(Assigned(DataSet));
  Assert(DataSet.Active);
  Assert(Assigned(List));
  Assert(FieldName <> '');

  List.Clear;
  if FirstValue <> '' then
    List.Add(FirstValue);

  DataSet.First;
  Field := DataSet.FieldByName(FieldName);
  while not DataSet.Eof do begin
    if IncludeBlank or NotEmpty(Field.AsString) then
      List.Add(Field.AsString);
    DataSet.Next;
  end;
end;

function GetDataSetFieldPlainString(DataSet: TDataSet; const FieldName: string; MaxRecords: Integer; Separator: Char): String;
var
  Field: TField;
  i: Integer;
begin
  Assert(Assigned(DataSet));
  Assert(DataSet.Active);
  Assert(FieldName <> '');
  Result := EmptyStr;

  i := 0;
  DataSet.First;
  Field := DataSet.FieldByName(FieldName);
  while not DataSet.Eof do begin
    if Result = EmptyStr then
      Result :=  Field.AsString
    else
      Result :=  Result + Separator + ' ' + Field.AsString;
    DataSet.Next;
    Inc(i);
    if (i = MaxRecords) then begin
      Result := Result + ' ...';
      Break;
    end;
  end;
end;

procedure CopyRecord(Source, Dest: TDataSet);
var
  i: Integer;
  FieldName: string;
begin
  Assert(Assigned(Source) and Source.Active);
  Assert(Assigned(Dest) and Dest.Active);
  Dest.Insert;
  for i := 0 to Source.Fields.Count - 1 do begin
    FieldName := Source.Fields[i].FieldName;
    Dest.FieldByName(FieldName).Value := Source.FieldByName(FieldName).Value;
  end;
  Dest.Post;
end;

// This only returns the comma separated and optionally quoted items in the list - no delimiters, "in", etc.
function GetSQLInClauseForStrings(Items: TStrings; Quote: Boolean = True): string;
var
  i: Integer;
  Item: string;
begin
  Assert(Assigned(Items));
  Result := '';
  for i := 0 to Items.Count - 1 do begin
    Item := Items[i];
    if Quote then
      Item := QuotedStr(Item);
    if Result = '' then
      Result := Item
    else
      Result := Result + ', ' + Item;
  end;
end;

function GetSQLInClauseForDataSet(DataSet: TDataSet; const FieldName: string;
  const Default: string): string;
var
  Values: TStringList;
begin
  Assert(Assigned(DataSet));
  Assert(NotEmpty(FieldName));
  Values := TStringList.Create;
  try
    GetUniqueFieldValues(DataSet, FieldName, Values);
    Result := GetSQLInClauseForStrings(Values);
  finally
    FreeAndNil(Values);
  end;
  if IsEmpty(Result) then
    Result := Default;
end;

function IntKeyToDBVariant(Key: Integer): Variant;
begin
  if Key < 1 then
    Result := Null
  else
    Result := Key;
end;

function HaveField(DataSet: TDataSet; const FieldName: string): Boolean;
begin
  Assert(Assigned(DataSet));
  Result := Assigned(DataSet.FindField(FieldName));
end;

function HaveNonNullField(DataSet: TDataSet; const FieldName: string): Boolean;
var
  Field: TField;
begin
  Assert(Assigned(DataSet));
  Assert(Trim(FieldName) <> '');
  Result := False;
  Field := DataSet.FindField(FieldName);
  if Assigned(Field) then
    Result := not Field.IsNull;
end;

function FieldToSQLValue(Field: TField): string;
begin
  Assert(Assigned(Field));
  if Field.IsNull then
    Result := 'null'
  else begin
    if Field is TBooleanField then begin
      if Field.AsBoolean then
        Result := '1'
      else
        Result := '0';
    end
    else if Field is TDateTimeField then
      Result := IsoDateTimeToStrQuoted(Field.AsDateTime)
    else if Field is TStringField then
      Result := QuotedStr(Field.AsString)
    else if Field is TMemoField then
      Result := QuotedStr(CompressWhiteSpace(Field.AsString))
    else if (Field is TBlobField) then
      Result := 'null'
    else
      Result := Field.AsString
  end;
end;

function RoundSQLServerTimeMS(DateTime: TDateTime): TDateTime;
var
  Hours, Minutes, Seconds, Milliseconds: Word;
  LastMSDigit: Integer;
begin
  Result := DateTime;
  DecodeTime(DateTime, Hours, Minutes, Seconds, Milliseconds);
  LastMSDigit := StrToInt(RightStr(IntToStr(Milliseconds), 1));
  // Would this be good enough and much simpler?
  // Result := IncMilliSecond(Result, -LastMSDigit);
  case LastMSDigit of
   0: ;
   1: Result := IncMilliSecond(Result, -1);
   2: Result := IncMilliSecond(Result, 1);
   3: ;
   4: Result := IncMilliSecond(Result, -1);
   5: Result := IncMilliSecond(Result, 2);
   6: Result := IncMilliSecond(Result, 1);
   7: ;
   8: Result := IncMilliSecond(Result, -1);
   9: Result := IncMilliSecond(Result, 1);
  end;
end;

procedure SetFieldsUpdatable(DataSet: TDataSet; Updatable: Boolean);
var
  i: Integer;
  Field: TField;
begin
  Assert(Assigned(DataSet));
  for i := 0 to DataSet.FieldCount - 1 do begin
    Field := DataSet.Fields[i];
    SetFieldUpdatable(Field, Updatable);
  end;
end;

procedure SetFieldsUpdatable(DataSet: TDataSet; Updatable: Boolean; Fields: array of string); overload;
var
  i: Integer;
  FieldName: string;
  Field: TField;
begin
  Assert(Assigned(DataSet));
  for i := Low(Fields) to High(Fields) - 1 do begin
    FieldName := Fields[i];
    Field := DataSet.FieldByName(FieldName);
    SetFieldUpdatable(Field, Updatable);
  end;
end;

procedure SetFieldUpdatable(Field: TField; Updatable: Boolean);
begin
  Assert(Assigned(Field));
  if Updatable then
    Field.ProviderFlags := Field.ProviderFlags + [pfInUpdate]
  else
    Field.ProviderFlags := Field.ProviderFlags - [pfInUpdate];
end;

function SetWhereClause(const SQL, Where: string): string;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.Text := SQL;
    SetWhereClause(SL, Where);
    Result := SL.Text;
  finally
    FreeAndNil(SL);
  end;
end;

function SetOrderByClause(const SQL, OrderBy: string): string;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.Text := SQL;
    SetOrderByClause(SL, OrderBy);
    Result := SL.Text;
  finally
    FreeAndNil(SL);
  end;
end;

procedure SetWhereClause(SQL: TStringList; const Where: string);
var
  i: Integer;
begin
  i := GetLineWithPrefix(SQL, 'where ');
  if i > -1 then
    SQL[i] := Where
  else
    SQL.Add(Where);
end;

procedure SetOrderByClause(SQL: TStringList; const OrderBy: string);
var
  i: Integer;
begin
  i := GetLineWithPrefix(SQL, 'order by ');
  if i > -1 then
    SQL[i] := OrderBy
  else
    SQL.Add(OrderBy);
end;

function ReplaceVars(const Template: string; DataSet: TDataSet; OtherVars: TStrings = nil): string;
var
  i: Integer;
  VarName: string;
  VarValue: string;
begin
  Assert(Assigned(DataSet));
  Result := Template;
  for i := 0 to DataSet.FieldCount - 1 do begin
    VarName := '%' + DataSet.Fields[i].FieldName + '%';
    VarValue := DataSet.Fields[i].AsString;
    Result := StringReplace(Result, VarName, VarValue, [rfReplaceAll, rfIgnoreCase]);
  end;
end;

procedure CopyDBMemo(ADBMemo: TDBMemo; HLColor: TColor = HighlightColor);
begin
  ADBMemo.Color := HLColor;
  Clipboard.AsText := ADBMemo.Text;
end;

procedure CopyDBText(ADBText: TDBText; HLColor: TColor = HighlightColor);
begin
  ADBText.Color := HLColor;
  ADBText.Transparent := False;
  Clipboard.AsText := ADBText.Caption;
end;

procedure CopyLabelText(ALabel: TLabel; HLColor: TColor = HighlightColor);
begin
  ALabel.Color := HLColor;
  ALabel.Transparent := False;
  Clipboard.AsText := ALabel.Caption;
end;

procedure CopyDBEdit(ADBEdit: TDBEdit; HLColor: TColor = HighlightColor);
begin
  ADBEdit.Color := HLColor;
  Clipboard.AsText := ADBEdit.Text;
end;

procedure ClearSelectionsDBText(APanel: TPanel; CtrlsTransparent: boolean = True);
var
  i : integer;
begin
    {Clear any other "selection"}
    for i := 0 to APanel.ControlCount - 1 do begin
      if APanel.Controls[i] is TDBText then begin
        (APanel.Controls[i] as TDBText).Transparent := CtrlsTransparent;
        (APanel.Controls[i] as TDBText).Color := APanel.Color;
      end
      else if APanel.Controls[i] is TDBMemo then begin
        (APanel.Controls[i] as TDBMemo).Color := APanel.Color;
      end
      else if APanel.Controls[i] is TLabel then begin
        (APanel.Controls[i] as TLabel).Transparent := CtrlsTransparent;
        (APanel.Controls[i] as TLabel).Color := APanel.Color;
      end
      else if APanel.Controls[i] is TDBEdit then begin
        (APanel.Controls[i] as TDBEdit).Color := APanel.Color;
      end;
    end;
end;

{ NullField }

procedure NullField.SetAsDateTime(Value: TDateTime);
begin
  // do nothing
end;

procedure NullField.SetAsFloat(Value: Double);
begin
  // do nothing
end;

procedure NullField.SetAsInteger(Value: Integer);
begin
  // do nothing
end;

procedure NullField.SetAsString(const Value: string);
begin
  // do nothing
end;

{$IFDEF ClassHelpers}
{ TDatasetHelper }

function TDatasetHelper.Editing: Boolean;
begin
  Result := State in dsEditModes;
end;

function TDatasetHelper.HasField(const FieldName: string): Boolean;
begin
  Result := Assigned(FindField(FieldName));
end;

procedure TDatasetHelper.PostIfEditing;
begin
  if Editing then
    Post;
end;

function TDatasetHelper.SetFieldTag(const FieldName: string; Tag: Integer): Boolean;
begin
  Result := False;
  Assert(NotEmpty(FieldName));
  if HasField(FieldName) then begin
    FieldByName(FieldName).Tag := Tag;
    Result := True;
  end;
end;

function TDatasetHelper.SumDoubleField(const FieldName: string): Double;
var
  Field: TField;
  PrevRec: TBookmark;
begin
  Assert(NotEmpty(FieldName));
  Field := FindField(FieldName);
  Assert(Assigned(Field));
  Result := 0;
  PrevRec := Self.Bookmark;
  try
    Self.First;
    while not Self.Eof do begin
      Result := Result + Field.AsFloat;
      Self.Next;
    end;
  finally
    Self.Bookmark := PrevRec;
  end;
end;

function TDatasetHelper.SumIntField(const FieldName: string): Integer;
var
  Field: TField;
  PrevRec: TBookmark;
begin
  Assert(NotEmpty(FieldName));
  Field := FindField(FieldName);
  Assert(Assigned(Field));
  Result := 0;
  PrevRec := Self.Bookmark;
  try
    Self.First;
    while not Self.Eof do begin
      Inc(Result, Field.AsInteger);
      Self.Next;
    end;
  finally
    Self.Bookmark := PrevRec;
  end;
end;

function TDatasetHelper.HasRecords: Boolean;
begin
  Result := Self.Active and (not Self.IsEmpty);
end;

{$IF CompilerVersion >= 24}
procedure TADOConnectionHelper.GetTableNamesExt(List: TStrings; IncludeSystemTables,
    IncludeViews: Boolean);
var
  TypeField,
  NameField: TField;
  TableType: string;
  DataSet: TADODataSet;

begin
  CheckActive;
  DataSet := TADODataSet.Create(nil);
  try
    OpenSchema(siTables, EmptyParam, EmptyParam, DataSet);
    TypeField := DataSet.FieldByName('TABLE_TYPE'); { do not localize }
    NameField := DataSet.FieldByName('TABLE_NAME'); { do not localize }
    List.BeginUpdate;
    try
      List.Clear;
//      List.OwnsObjects := True;
      while not DataSet.EOF do
      begin
        TableType := TypeField.AsString;
        if (TableType = 'TABLE') then
          List.Add(NameField.AsString)
        else if (TableType = 'VIEW') and IncludeViews then
          List.AddObject(NameField.AsString, TObject(TableType))
        else if (TableType = 'SYSTEM TABLE') and IncludeSystemTables then
          List.AddObject(NameField.AsString, TObject(TableType));
        DataSet.Next;
      end;
    finally
      List.EndUpdate;
    end;
  finally
    DataSet.Free;
  end;
end;
{$IFEND CompilerVerson}



{$ENDIF ClassHelpers}

end.
