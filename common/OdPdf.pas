unit OdPdf;

interface

uses Classes, DB;

function MergePDFs(FileList: TStrings; const OutputDir, ReportFileName: string;
  IniFilePath: string): string;
procedure FilterForNoPDFs(DataSet: TDataSet);

implementation

uses SySUtils, Forms, ODMiscUtils, JCLIniFiles, QMConst, Windows, ReportEngineLog, JclSysInfo;

const
  SUCCESSFULL_MERGE     = 10000;
  GHOSTSCRIPT_NOT_FOUND = 10001;
  OUTPUTFILE_NOT_FOUND  = 10002;
  GHOSTSCRIPT_NOT_CONFIGURED = 10003;
  GHOSTSCRIPT_UNABLE_GENERATE_FILE = 10004;
  GHOSTSCRIPT_UNABLE_EXECUTE = 10005;

  GHOSTSCRIPT_NOT_FOUND_ERROR_MESSAGE = 'The system could not find either gswin64c.exe or gswin32c.exe in %s. Please verify GhostScript is properly installed.';
  OUTPUTFILE_NOT_FOUND_ERROR_MESSAGE  = 'The system could not find GhostScript output file %s.';
  GHOSTSCRIPT_NOT_CONFIGURED_ERROR_MESSAGE  = 'The system could not find GhostScript configuration in %s';
  GHOSTSCRIPT_UNABLE_GENERATE_FILE_ERROR_MESSAGE = 'GhostScript is unable to generate the merged PDF %s as user %s';
  GHOSTSCRIPT_ERROR_MESSAGE = 'GhostScript Error: return code is %d';
  GHOSTSCRIPT_UNABLE_EXECUTE_ERROR_MESSAGE = 'The system was unable to execute GhostScript in %s as user %s. Error code: %d';

function MergePDFs(FileList: TStrings; const OutputDir, ReportFileName: string;
  IniFilePath: string): string;

const
  PDFMergingErrorSection = 'PDF Merging';

var
  GhostScriptPath: string;
  OutputFile: string;
  DoMergeResult: Integer;
  MergeErrorMessage: string;

  function DoMerge(var MergedFile: string; const OutputDir: string; FileList: TStrings): Integer;
  const
    GhostScriptCommandLine = ' -sDEVICE=pdfwrite -dSAFER -dBATCH -dNOPAUSE -sOutputFile="%s" %s -c -q';
    GhostScriptExe = 'gswin32c.exe';
    GhostScript64Exe = 'gswin64c.exe';
  var
    FilesToMerge: string;
    FullPath: string;
    FullCommand: string;
    ExitCode: Integer;
    IsWin64: Boolean;
  begin
    GhostScriptPath := IniReadString(IniFilePath, 'GhostScript', 'Path');

    if IsEmpty(GhostScriptPath) then begin
      Result := GHOSTSCRIPT_NOT_CONFIGURED;
      Exit;
    end;

    IsWin64 := (FileExists(AddSlash(GhostScriptPath) + GhostScript64Exe));
    if not IsWin64 then begin
      if not FileExists(AddSlash(GhostScriptPath) + GhostScriptExe) then begin
        Result := GHOSTSCRIPT_NOT_FOUND;
        Exit;
      end;
    end;

    if not CanWriteToDirectory(OutputDir) then begin
      Result := GHOSTSCRIPT_UNABLE_GENERATE_FILE;
      Exit;
    end;

    OutputFile := AddSlash(OutputDir) + 'QMMergedReport.pdf';
    FileList.Delimiter := ' ';
    FilesToMerge := FileList.DelimitedText;
    if IsWin64 then
      FullPath := AddSlash(GhostScriptPath) + GhostScript64Exe
    else
      FullPath := AddSlash(GhostScriptPath) + GhostScriptExe;

    if Pos('"', FullPath) = 0 then
      FullPath := AnsiQuotedStr(FullPath, '"');

    FullCommand := Format(FullPath + GhostScriptCommandLine, [OutputFile, FilesToMerge]);
    ExitCode := WinExecAndWait32V2(FullCommand, SW_HIDE);

    if not FileExists(OutputFile) then begin
      MergedFile := '';
      Result := OUTPUTFILE_NOT_FOUND;
    end
    else if ExitCode = -1 then begin
      MergedFile := '';
      Result := GHOSTSCRIPT_UNABLE_EXECUTE;
    end
    else if ExitCode <> 0 then begin
      MergedFile := '';
      Result := ExitCode;
    end
    else begin
      MergedFile := OutputFile;
      Result := SUCCESSFULL_MERGE;
    end;
  end;

var
  PDFFileList: TStringList;
  FileExtension: string;
  i: Integer;
begin
  PDFFileList := TStringList.Create;
  try
    PDFFileList.Add(ReportFileName);
    for i := 0 to FileList.Count - 1 do begin
      FileExtension := ExtractFileExt(FileList[i]);
      if StrContainsText(FileExtension, '.PDF') then
        PDFFileList.Add(FileList[i]);
    end;
    DoMergeResult := DoMerge(Result, OutputDir, PDFFileList);
    case DoMergeResult of
      SUCCESSFULL_MERGE:
        ;
      GHOSTSCRIPT_NOT_FOUND:
        MergeErrorMessage := Format(GHOSTSCRIPT_NOT_FOUND_ERROR_MESSAGE, [GhostScriptPath]);
      OUTPUTFILE_NOT_FOUND:
        MergeErrorMessage := Format(OUTPUTFILE_NOT_FOUND_ERROR_MESSAGE, [OutputFile]);
      GHOSTSCRIPT_NOT_CONFIGURED:
        MergeErrorMessage := Format(GHOSTSCRIPT_NOT_CONFIGURED_ERROR_MESSAGE, [IniFilePath]);
      GHOSTSCRIPT_UNABLE_GENERATE_FILE:
        MergeErrorMessage := Format(GHOSTSCRIPT_UNABLE_GENERATE_FILE_ERROR_MESSAGE, [OutputFile, GetLocalUserName]);
      GHOSTSCRIPT_UNABLE_EXECUTE:
        MergeErrorMessage := Format(GHOSTSCRIPT_UNABLE_EXECUTE_ERROR_MESSAGE, [GhostScriptPath, GetLocalUserName, GetLastError]);
      else
        MergeErrorMessage := Format(GHOSTSCRIPT_ERROR_MESSAGE, [DoMergeResult])
    end;
    if DoMergeResult <> SUCCESSFULL_MERGE then begin
      Result := ReportFileName;
      AddLogEntry(PDFMergingErrorSection, MergeErrorMessage, True);
    end;
  finally
    FreeAndNil(PDFFileList);
  end;
end;

// This is used in the reports to filter the attachment datasets to only show images
// since PDFs are not printed inside the ReportBuilder report, but merged in later.
procedure FilterForNoPDFs(DataSet: TDataSet);
begin
  DataSet.Filter := 'extension <> ''.pdf''';
  DataSet.Filtered := True;
end;

end.
