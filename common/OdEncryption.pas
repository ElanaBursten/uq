unit OdEncryption;

{ Encryption Functions
  Copyright 2001 - 2014 Oasis Digital

This is a freeware encrypter I found somewhere.  It is not intended to be
secure; rather it is here to avoid storing the passwords in plain text in
the database.
}

interface

function Encrypt(const S: string; Key: Word): string;

function Decrypt(const S: string; Key: Word): string;

function EncryptUsernamePassword(id, pw: string): string;

implementation

uses
  SysUtils;

const
  C1 = 52845;
  C2 = 22719;

{$OVERFLOWCHECKS OFF}
{$RANGECHECKS OFF}

function Encrypt(const S: string; Key: Word): string;
var
  I: byte;
begin
  Result := S;
  for I := 1 to Length(S) do begin
    Result[I] := char(byte(S[I]) xor (Key shr 8));
    Key := (byte(Result[I]) + Key) * C1 + C2;
  end;
end;

function Decrypt(const S: string; Key: Word): string;
var
  I: byte;
begin
  Result := S;
  for I := 1 to Length(S) do begin
    Result[I] := char(byte(S[I]) xor (Key shr 8));
    Key := (byte(S[I]) + Key) * C1 + C2;
  end;
end;

const
  HexDigits = '0123456789ABCDEF';

function Hex(n: Integer): string;
begin
  Result := HexDigits[n+1];
end;


function EncryptUsernamePassword(id, pw: string): string;
var
  bindata: string;
  i: Integer;
begin
  // This generates a "hash" of the username + password.

  id := LowerCase(id);
  pw := LowerCase(pw);  // passwords are NOT case sensitive.  Remove this
                        // to make them case sensitive
  bindata := Encrypt(id+':'+pw, 3453);   // I just made up a key, it's nothing special.
  // we now have binary data; it is better to convert to text before storing in the
  // password field

  Result := '';
  for i := 1 to Length(bindata) do
    Result := Result + Hex(Byte(bindata[i]) div 16) + Hex(Byte(bindata[i]) mod 16);
end;

end.

