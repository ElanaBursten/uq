unit OdSystemStatus;

interface

uses Windows, Classes, Graphics;

const
  SampleCount = 120;
  SystemPerformanceInformation = 2;
  SystemTimeInformation = 3;

type
  TPDWord = ^DWORD;

  TSystem_Performance_Information = packed record
    IdleTime:   Int64;
    KernelTime: Int64;
    UserTime:   Int64;
    Reserved1 : Int64;
    Reserved2 : Int64;
    Spare: array[1..76] of DWORD;  // this is a reserved array for the second processor
  end;

  TSystem_Time_Information = packed record
    KeBootTime: Int64;
    KeSystemTime: Int64;
    ExpTimeZoneBias: Int64;
    uCurrentTimeZoneId: ULONG;
    dwReserved: DWORD;
  end;

  TSystemStatus = class;

  THelperThread = class(TThread)
    FSystemStatus: TSystemStatus;
  protected
    procedure Execute; override;
  end;

  PProcessMemoryCounters = ^TProcessMemoryCounters;
  TProcessMemoryCounters = record
    cb: DWORD;
    PageFaultCount: DWORD;
    PeakWorkingSetSize: DWORD;
    WorkingSetSize: DWORD;
    QuotaPeakPagedPoolUsage: DWORD;
    QuotaPagedPoolUsage: DWORD;
    QuotaPeakNonPagedPoolUsage: DWORD;
    QuotaNonPagedPoolUsage: DWORD;
    PagefileUsage: DWORD;
    PeakPagefileUsage: DWORD;
  end;

  TSystemStatus = class(TObject)
  private
    NtQuerySystemInformation: function(InfoClass: DWORD; Buffer: Pointer;
      BufSize: DWORD; ReturnSize: TPDword): DWORD; stdcall;

    SysTimeInfo: TSystem_Time_Information;
    SysPerfInfo: TSystem_Performance_Information;

    Helper: THelperThread;
    FOnUpdate: TNotifyEvent;
    FUpdateInterval: Integer;

    FPhysicalMemory: Int64;
    FPhysicalFree: Int64;
    FProcessDelta: Int64;
    FProcessMemory: Int64;

    OldProcessMemory: Int64;
    OldIdleTime: Int64;
    OldSystemTime: Int64;
    Samples: array[0..SampleCount - 1] of Currency;   // 0..119 for the last minute of 500 msec samples
    function GetCPUUsage: Currency;
    function CalculateCPUUsage: Currency;
    procedure Initialize;
    procedure Update;
    procedure DoOnUpdate;
  public
    SynchronizeCallback: Boolean;
    constructor Create(UpdateInterval: Integer);
    destructor Destroy; override;
    procedure StartBackgroundThread;
    function RenderAsBitmap(Height: Integer; Width: Integer): TBitmap;

    property CPUUsage: Currency read GetCPUUsage;

    property PhysicalMemory: Int64 read FPhysicalMemory;
    property PhysicalFree: Int64 read FPhysicalFree;
    property ProcessMemory: Int64 read FProcessMemory;
    property ProcessDelta: Int64 read FProcessDelta;

    property OnUpdate: TNotifyEvent read FOnUpdate write FOnUpdate;
  end;

  function GetProcessMemoryInfo(Process : THandle; var MemoryCounters : TProcessMemoryCounters; cb : DWORD) : BOOL; stdcall;

implementation

uses SysUtils, JclSysInfo, Math;

function GetProcessMemoryInfo; external 'psapi.dll';

{ TSystemStatus }
constructor TSystemStatus.Create(UpdateInterval: Integer);
begin
  FUpdateInterval := Max(UpdateInterval, 500); // Milliseconds
  Initialize;
  Update;
end;

destructor TSystemStatus.Destroy;
begin
  if Assigned(Helper) then begin
    Helper.Terminate;
    Helper.WaitFor;
    FreeAndNil(Helper);
  end;

  inherited;
end;

procedure TSystemStatus.StartBackgroundThread;
begin
  Helper := THelperThread.Create(True);
  Helper.FSystemStatus := Self;
  Helper.FreeOnTerminate := False;
{$IF CompilerVersion >= 22}
  Helper.Start;
{$ELSE}
  Helper.Resume;
{$IFEND}
end;

procedure TSystemStatus.Initialize;
var
  i: Integer;
begin
  OldProcessMemory := 0;

  // This should eventually change to use a documented API such as the performance counters
  NtQuerySystemInformation := GetProcAddress(GetModuleHandle('ntdll.dll'), 'NtQuerySystemInformation');
  for i := Low(Samples) to High(Samples) do
    Samples[i] := 0;
end;

procedure TSystemStatus.Update;

  procedure UpDateMemCounters;
  // http://www.delphi3000.com/articles/article_4101.asp?SK=
  var
    pmc: TProcessMemoryCounters;
    cb: Integer;
    MemStat: tMemoryStatus;

  begin
    MemStat.dwLength := SizeOf(MemStat);
    GlobalMemoryStatus(MemStat);

    // Get the total and available system memory
    FPhysicalMemory := MemStat.dwTotalPhys;
    FPhysicalFree := MemStat.dwAvailPhys;

    // Get the used memory for the current process
    cb := SizeOf(TProcessMemoryCounters);
    if GetProcessMemoryInfo(GetCurrentProcess(), pmc, cb) then
    begin
      FProcessMemory := Longint(pmc.WorkingSetSize);
      FProcessDelta := (FProcessMemory  - OldProcessMemory);
      OldProcessMemory := FProcessMemory;
    end;
  end;

var
  i: Integer;
  UsagePct: Currency;
begin
  if not Assigned(NtQuerySystemInformation) then
    Exit;

  UsagePct := RoundTo(CalculateCPUUsage, -2);
  for i := Low(Samples) to High(Samples) - 1 do
    Samples[i] := Samples[i + 1];
  Samples[High(Samples)] := UsagePct;

  UpDateMemCounters;
end;

function TSystemStatus.RenderAsBitmap(Height, Width: Integer): TBitmap;
var
  B: TBitmap;
  i: Integer;
  SampleNum: Integer;
  UsageFraction: Single;
  MaxX: Integer;
  MaxY: Integer;
  SampleRatio: Single;
begin
  B := TBitmap.Create;
  Result := B;
  Assert((Height > 0) and (Width > 0));
  B.Height := Height;
  B.Width := Width;
  MaxX := B.Width - 1;
  MaxY := B.Height - 1;

  B.Transparent := False;
  B.Canvas.Pen.Color := clDkGray;
  B.Canvas.Brush.Color := clDkGray;
  B.Canvas.Rectangle(0, 0, MaxX + 1, MaxY + 1);

  for i := 0 to MaxX do begin
    SampleRatio := 0;
    if i > 0 then
      SampleRatio := RoundTo(i / B.Width, -4);
    SampleNum := Min(Max(Round(SampleCount * SampleRatio), 0), SampleCount - 1);
    UsageFraction := Samples[SampleNum] / 100;
    if UsageFraction <= 0.6 then
      B.Canvas.Pen.Color := clGreen
    else if UsageFraction < 0.85 then
      B.Canvas.Pen.Color := clYellow
    else
      B.Canvas.Pen.Color := clRed;
    B.Canvas.MoveTo(i, MaxY);
    B.Canvas.LineTo(i, MaxY - Round((MaxY * UsageFraction)));
  end;
end;

function TSystemStatus.CalculateCpuUsage: Currency;
var
  IdleTime, SystemTime: Int64;
  Status: DWORD;
begin
  Result := 0;
  // Get new total system time
  Status := NtQuerySystemInformation(SystemTimeInformation, @SysTimeInfo, SizeOf(SysTimeInfo), nil);
  if Status <> 0 then
    Exit;
  // Get new CPU idle time
  Status := NtQuerySystemInformation(SystemPerformanceInformation, @SysPerfInfo, SizeOf(SysPerfInfo), nil);
  if Status <> 0 then
    Exit;

  if OldIdleTime > 0 then begin
    // Subtract off the old values to get the deltas
    IdleTime := SysPerfInfo.IdleTime - OldIdleTime;
    SystemTime := (SysTimeInfo.KeSystemTime - OldSystemTime) + 1; // Make sure this is never 0

    IdleTime := (IdleTime * 100) div SystemTime; // Make this a percentage
    Result := RoundTo(100 - (Integer(IdleTime) / JclSysInfo.ProcessorCount), -4);
  end;

  OldIdleTime := SysPerfInfo.IdleTime;
  OldSystemTime := SysTimeInfo.KeSystemTime;

  if Result > 100 then
    Result := 100
  else if Result < 0 then
    Result := 0;
end;

function TSystemStatus.GetCPUUsage: Currency;
begin
  Result := Samples[High(Samples)];
end;

procedure TSystemStatus.DoOnUpdate;
begin
  if Assigned(FOnUpdate) then
    FOnUpdate(Self);
end;

{ THelperThread }

procedure THelperThread.Execute;
begin
  inherited;
  Assert(Assigned(FSystemStatus));
  while not Terminated do begin
    FSystemStatus.Update;
    if FSystemStatus.SynchronizeCallback then
      Synchronize(FSystemStatus.DoOnUpdate)
    else
      FSystemStatus.DoOnUpdate;
    Sleep(FSystemStatus.FUpdateInterval);
  end;
end;

end.

