unit PasswordRules;

interface

uses
  SysUtils, OdExceptions;

type
  EOdPasswordProblem = class(EOdNonLoggedException);

function PasswordMeetsRequirements(const UserName, OldPass, Pass: string; var Problem: string; const FirstName: string = ''; const LastName: string = ''): Boolean;
function GeneratePasswordHash(const UserName, Password: string; caseSensitive:boolean=false): string;
function PasswordIsHashed(const Password: string): Boolean;

implementation

uses
  OdMiscUtils,
  Hashes;

{
  Password Requirements:
  * Can't be blank
  * Can't begin or end with spaces
  * minimum of 8 characters
  * include at least uppercase, lowercase, and numbers or special characters
  * at least 3 characters must be "special" (numbers, upper case characters or symbols)
}
const
  WordsNotAllowed: array[0..13] of string = (
    'password',
    'secret',
    'uq',
    'utiliquest',
    'sts',
    'locinc',
    'locating',
    'fiber',
    'cable',
    'phone',
    'gas',
    'power',
    'pipe',
    'line'
    );

function CountSpecialChars(Pass: string): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 1 to Length(Pass) do begin
    if (Pass[I]<'a') or (Pass[I]>'z') then
      Inc(Result);
  end;
end;

function StrContainsText(const SubStr, S: string): Boolean;
begin
  Result := Pos(AnsiUpperCase(SubStr), AnsiUpperCase(S)) > 0;
end;

function StringContainsAnyOfArray(const S: string; const A: array of string; var ElementMatched: Integer): Boolean;
var
  I: Integer;
begin
  ElementMatched := -1;
  for I := Low(A) to High(A) do
    if StrContainsText(A[I], S) then begin
      ElementMatched := I;
      Break;
    end;
  Result := ElementMatched > -1;
end;

function IsStrongPassword(Pass: string): Boolean;
var
  I: Integer;
  Upper, Lower, Special: Boolean;
begin
  Upper:=False; Lower:=False; Special:=False;
  for I := 1 to Length(Pass) do begin
    if CharInSet(Pass[I], ['A'..'Z']) then
      Upper := True;
    if CharInSet(Pass[I], ['a'..'z']) then
      Lower := True;
    if CharInSet(Pass[I], [' '..'@','['..'`','{'..'~']) then
      Special := True; //includes special characters and numbers
  end;
  Result := Upper and Lower and Special and (Length(Pass)>7);
end;

function PasswordMeetsRequirements(const UserName, OldPass, Pass: string; var Problem: string; const FirstName: string = ''; const LastName: string = ''): Boolean;
var
  Index: Integer;
begin
  Problem := '';
  Result := False;
  try
    if Pass = '' then
      raise EOdPasswordProblem.Create('The password cannot be blank');
    if Trim(Pass) <> Pass then
      raise EOdPasswordProblem.Create('The password cannot begin or end with spaces');
    if Pass = OldPass then
      raise EOdPasswordProblem.Create('The new password must not be the same as the old password');
    if Pass = '1234' then
      raise EOdPasswordProblem.Create('The password must not be the system default password');
    if StrContainsText(UserName, Pass) then
      raise EOdPasswordProblem.Create('The password cannot contain your username');
    if (FirstName <> '') and (StrContainsText(FirstName, Pass)) then
      raise EOdPasswordProblem.Create('The password cannot contain the user''s first name');
    if (LastName <> '') and (StrContainsText(LastName, Pass)) then
      raise EOdPasswordProblem.Create('The password cannot contain the user''s last name');
    if StringContainsAnyOfArray(Pass, WordsNotAllowed, Index) then
      raise EOdPasswordProblem.CreateFmt('The password cannot contain "%s"', [WordsNotAllowed[Index]]);
    if CountSpecialChars(Pass) < 3 then
      raise EOdPasswordProblem.Create('The password must contain at least 3 special characters (uppercase, numbers, symbols)');
    if not IsStrongPassword(Pass) then
      raise EOdPasswordProblem.Create('The password must contain at least 8 characters ' +
        'including uppercase, lowercase, and numbers or special characters');

    Result := True;
  except
    on E: EOdPasswordProblem do begin
      Problem := E.Message;
      Exit;
    end;
  end;
end;


// This uses Hashes.pas which is a freeware hash utility that supports
// Unicode and is backwards compatible with D6/D2007 as well as XE2/XE3.
function GeneratePasswordHash(const UserName, Password: string; caseSensitive:boolean=false): string;
{Some portal tests for pw by hashing the UserName as case sensitive. These are non-Locators
I test if they DON'T have a EmpID and hash their User Names as entered.  }
const
  JunkData1 = '9P4J@5.QpWqyf*QD!Y-f.tk6K3l@4g';
  JunkData2 = 'I@2y*s&C$l-ic8V^4B45/D!9Tjm/bA';
var
  MergedValue : string;
begin
  if caseSensitive then
    MergedValue := JunkData1 + Trim(UserName) + Password + JunkData2    //QMANTWO-420
  else
    MergedValue := JunkData1 + LowerCase(Trim(UserName)) + Password + JunkData2;
  try
    Result := UpperCase(CalcHash2(MergedValue, haSHA1));
  except
    Result := '';
  end;
end;

function PasswordIsHashed(const Password: string): Boolean;
begin
  Result := Length(Password) = 40;
end;

end.
