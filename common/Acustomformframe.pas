unit Acustomformframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxCheckBox, DB,
  StrUtils,  Mask, Menus,
  cxCalendar, cxTextEdit,cxMaskEdit, cxDropDownEdit, cxButtons,
  DateUtils, ACustomFormConst;

  const
  DBLSPACE = '  ';

  type
  TCustomFormFrame = class(TFrame)
    pnlQuestionAnswer: TPanel;
    edtQuestion: TMemo;
    pnlSide: TPanel;
    pnlAnswer: TPanel;
    btnClearItOld: TButton;
    btnClearIt: TcxButton;
    CountLbl: TLabel;
    procedure btnClearItClick(Sender: TObject);
  private
    fFieldInfo: TFieldInfo;

    {field types = created as needed}
      edtAnswer: TEdit;
      edtFirst: TLabeledEdit;
      edtLast: TLabeledEdit;
      cbCheckBox: TCheckBox;
      edtPhone: TMaskEdit;
      DTPicker: TcxDateEdit;
      DatePicker: TcxDateEdit;
      cmboCustom: TcxComboBox;

    procedure EdtAnswerChange(Sender: TObject);
    procedure EdtAnswerExit(Sender: TObject);
    procedure DeductCountKeyUp(Sender: TObject; var Key: Word;
                               Shift: TShiftState);
    procedure SetAnswerChanged(value: boolean);
    function GetAnswerChanged: boolean;
    procedure ClearAnswer;
    function GetAnswer: string;


    {Field Creation Called by Frame Create}
      function InitQuestionMemo: Boolean;
      function CreateGroupHeader: TLabel;
      function CreateAnswerEdit: TEdit;
      function CreateAnswerPhone: TMaskEdit;
      function CreateAnswerDate: TcxDateEdit;
      function CreateAnswerDateTime: TcxDateEdit;
      function CreateAnswerCheckBox: TCheckBox;
      function CreateAnswerFirstLast: TLabeledEdit;
      function CreateAnswerCustomDropDown: TcxComboBox;
      procedure SideButton(pShow: Boolean);
      procedure AdjustQuestionSize;

    function HasRestriction: boolean;   {When a restriction is defined}

  public
    property Answer: String read GetAnswer;
    constructor Create(pFieldInfo: TFieldInfo);
    function SaveAnswer: TFieldInfo;   {Saves the Answer and returns the whole record}
    procedure SetExistingAnswer(pFieldInfo:TFieldInfo);
    property AnswerChanged: boolean read GetAnswerChanged write SetAnswerChanged;
  end;



implementation

{$R *.dfm}

{ TCustomFormFrame }



procedure TCustomFormFrame.AdjustQuestionSize;
begin
//
//  if edtQuestion.Lines.Count > 0 then

end;



procedure TCustomFormFrame.btnClearItClick(Sender: TObject);
begin
  ClearAnswer;
end;

procedure TCustomFormFrame.ClearAnswer;
begin
  case fFieldInfo.ControlType of
    cfFreeForm: begin
      edtAnswer.Text := '';
      Countlbl.Caption := '0';
    end;
    cfPhone: edtPhone.Text := '';
    cfDate: DatePicker.Text := '';
    cfDateTime:  DtPicker.Text := '';
    cfFirstLast :
    begin
      edtFirst.Text := '';
      edtLast.Text  := '';
      Countlbl.Caption := '';
    end;
  end;
end;

constructor TCustomFormFrame.Create(pFieldInfo: TFieldInfo);
begin
  inherited Create(Owner);

  fFieldInfo := pFieldInfo;
  Self.Color := fFieldInfo.Color;
  Self.pnlQuestionAnswer.Color := fFieldInfo.Color;
  Self.pnlSide.Color := fFieldInfo.Color;
  Self.edtQuestion.Color := fFieldInfo.Color;
  Self.pnlAnswer.Color := fFieldInfo.Color;

  Self.pnlAnswer.Caption := '';

   {Setup the Question}
   InitQuestionMemo;

   {Setup the Answer}
   case pFieldInfo.ControlType of
    cfFreeForm:     CreateAnswerEdit;
    cfPhone:        CreateAnswerPhone;
    cfDate:         CreateAnswerDate;
    cfDateTime:     CreateAnswerDatetime;
    cfCheckBox:     CreateAnswerCheckBox;
    cfGroupHeading: CreateGroupHeader;
    cfFirstLast:    CreateAnswerFirstLast;
   end;

   AnswerChanged := False;
end;


function TCustomFormFrame.CreateAnswerCheckBox: TCheckBox;
begin
  pnlQuestionAnswer.BorderStyle := bsNone;
  SideButton(False);
  cbCheckBox := TCheckBox.Create(Self);
  Self.Height := SINGLEFRAMEHEIGHT;
  with cbCheckBox do begin
    Name := 'cbCheckBox';
    Parent := pnlAnswer;
    Left := 1;
    Top := 1;
    Width := DEFWIDTH;

    Align := alClient;
    Caption := fFieldInfo.Question;
    Font.Size := DEFFONTSIZE;
    TabOrder := 0;

    edtQuestion.Visible := False;
    CbCheckBox.OnClick := EdtAnswerChange;
  end;
  Result := cbCheckBox;
end;


function TCustomFormFrame.CreateAnswerCustomDropDown: TcxComboBox;
begin
  SideButton(True);
  pnlQuestionAnswer.BorderStyle := bsSingle;
  cmboCustom := TCxComboBox.Create(Self);
  With cmboCustom do begin
    Name := 'cmboCustom';
    btnClearIt.Visible:= True;
    Parent := pnlAnswer;
    {What to do with the contents for dropdown}

    cmboCustom.OnClick := EdtAnswerChange;
  end;
end;

function TCustomFormFrame.CreateAnswerDate: TcxDateEdit;
begin
  SideButton(True);
  pnlQuestionAnswer.BorderStyle := bsSingle;
  DatePicker := TcxDateEdit.Create(Self);
  with DatePicker do
  begin
    Name := 'DatePicker';
    btnClearIt.Visible := True;

    Parent := pnlAnswer;
    properties.Kind := ckDate;
    properties.DateButtons:= [btnClear,btnToday];
    Properties.DisplayFormat :=  DATEONLYFORMAT;
    ParentColor := False;

    Date := Tomorrow;
    if HasRestriction then begin
      Date := Now + fFieldInfo.RestrictionInt;
      DatePicker.Style.Font.Color := clMaroon;
//      DatePicker.Properties.OnCloseUp := DateTPickerRestrictionExit;
    end
    else begin
      DatePicker.Style.Font.Color := DEFANSWERCOLOR;
    end;
    properties.SaveTime := False;
    Left := 1;
    Top := 1;
    TabOrder := 0;
    Width := DEFWIDTH;
    Height := DEFHEIGHT;
    Align := alClient;

    DatePicker.OnClick := EdtAnswerChange;
  end;
  Result := DatePicker;
end;

function TCustomFormFrame.CreateAnswerDateTime: TcxDateEdit;
begin
  SideButton(True);
  pnlQuestionAnswer.BorderStyle := bsSingle;
  DTPicker := TcxDateEdit.Create(Self);
  with DTPicker do begin
    Name := 'DTPicker';
    Parent := pnlAnswer;
    properties.Kind := ckDateTime;
    properties.ShowTime := True;
    properties.DateButtons:= [btnClear,btnNow,btnToday];
    properties.DisplayFormat :=  DATEFORMAT;
    properties.SaveTime := true;
    ParentColor := False;

    if HasRestriction then begin
      Date := Now + fFieldInfo.RestrictionInt;
      DtPicker.Style.Font.Color := RESTRICTEDCOLOR;
//      DTPicker.Properties.OnCloseUp := DTPickerRestrictionExit;
    end
    else begin
      DtPicker.Style.Font.Color := DEFANSWERCOLOR;
    end;

    Left := 1;
    Top := 1;
    TabOrder := 0;
    Width := DEFWIDTH;
    Height := DEFHEIGHT;
    Align := alClient;

    DTPicker.OnClick := EdtAnswerChange;
  end;
  Result := DTPicker;
end;

function TCustomFormFrame.CreateAnswerEdit: TEdit;
begin
  pnlQuestionAnswer.BorderStyle := bsSingle;
  SideButton(True);
  pnlAnswer.Caption := '';

  edtAnswer := TEdit.Create(Self);
  edtAnswer.Font.Color := DEFANSWERCOLOR;
  btnClearIt.Visible := True;
  Countlbl.Parent := pnlAnswer;
  Countlbl.Visible := True;

  SideButton(True);

  with edtAnswer do begin
    Name := 'EdtAnswer';
    Parent := pnlAnswer;
    Left := 1;
    Top := 1;
    Width := DEFWIDTH;
    Height := EDITHEIGHT;
    MaxLength := MAXSIZE;
    Align := alClient;
    Font.Size := DEFFONTSIZE;
    TabOrder := 0;
    edtAnswer.OnChange := EdtAnswerChange;
    edtAnswer.OnExit := EdtAnswerExit;
    Text := '';
  end;
end;


function TCustomFormFrame.CreateAnswerFirstLast: TLabeledEdit;
begin
  Self.Height := Self.Height + EDITHEIGHT;
  edtQuestion.Visible := True;
  edtFirst := TLabeledEdit.Create(Self);
  SideButton(True);
  pnlQuestionAnswer.BorderStyle := bsSingle;
  
  with edtFirst do begin
    Name := 'EdtFirst';
    Parent := pnlAnswer;
    Align := alLeft;
    AlignWithMargins := True;
    edtFirst.Margins.Top := 20;
    Text := '';
    EditLabel.Caption := 'First Name:';
    EditLabel.Font.Size := DEFFONTSIZE;
    Font.Color := DEFANSWERCOLOR;
    Font.Size := DEFFONTSIZE;
    OnKeyUp :=  DeductCountKeyUp;
    TabOrder := 1;
    Width := FNAMEWIDTH;
    edtFirst.OnChange := EdtAnswerChange;
  end;

  edtLast := TLabeledEdit.Create(Self);
  with edtLast do begin
    Name := 'EdtLast';
    Parent := pnlAnswer;
    Align := alLeft;
    AlignWithMargins := True;
    Margins.Top := 20;
    Text := '';
    EditLabel.Caption := 'Last Name:';
    EditLabel.Font.Size := DEFFONTSIZE;
    Font.Color := DEFANSWERCOLOR;
    Font.Size := DEFFONTSIZE;
    OnKeyUp :=  DeductCountKeyUp;
    TabOrder := 2;
    Width := LNAMEWIDTH;
    edtLast.OnChange := EdtAnswerChange;
  end;

  CountLbl.Visible := True;
  btnClearIt.Visible := True;

  btnClearIt.Align := alLeft;

  Result :=  edtLast;
end;

function TCustomFormFrame.CreateAnswerPhone: TMaskEdit;
begin
  pnlQuestionAnswer.BorderStyle := bsSingle;
  SideButton(True);
  btnClearIt.Visible := True;
  EdtPhone := TMaskEdit.Create(Self);
  with edtPhone do
  begin
    Name := 'EdtPhone';
    Parent := pnlAnswer;
    Left := 1;
    Top := 1;
    Width := DEFWIDTH;
    Height := DEFHEIGHT;
    Align := alClient;
    EditMask := PHONEMASKEXT;
    Font.Size := DEFFONTSIZE;
    Font.Color := DEFANSWERCOLOR;
    MaxLength := 31;
    TabOrder := 0;
    Text := '';
    edtPhone.OnChange := EdtAnswerChange;
  end;
end;



function TCustomFormFrame.CreateGroupHeader: TLabel;
begin
  pnlQuestionAnswer.BorderStyle := bsNone;
  SideButton(False);
  pnlAnswer.Caption := '';
  pnlAnswer.Visible := False;
  Self.Height := EDITHEIGHT;
  //edtQuestion.Padding.Left := 5;
  edtQuestion.Font.Size := HEADERFONTSIZE;
  edtQuestion.Font.Style := [fsBold];
end;






procedure TCustomFormFrame.DeductCountKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  REMAIN = '  %d characters remaining';
var
  cnt : integer;
  len : integer;
  actEdit : Tedit;
begin
  cnt := MAXSIZE- (length(edtLast.text)+ length(edtFirst.text));
  Countlbl.Caption := format(REMAIN,[cnt-1]);   // +1 for the space
  if cnt < 1 then
  begin
    Countlbl.Caption := 'All characters used!';
    actEdit := (Sender as Tedit);
    len := length(actEdit.text);
    actEdit.Text := RightStr(actEdit.Text, len-1);
  end;
end;



procedure TCustomFormFrame.EdtAnswerChange(Sender: TObject);
var
  Count: integer;
{Show Counter}
begin

  if (Sender is TEdit) then begin
    if TEdit(Sender).Focused then begin
      Count := MAXSIZE - Length(TEdit(Sender).Text);
      case Count of
        50: CountLbl.Caption := '';
        0: CountLbl.Caption := EMPTY_ANSWER;
        else
          CountLbl.Caption := IntToStr(Count);
      end;
      CountLbl.Visible := True;
    end
    else
      CountLbl.Visible := False;
  end;

  AnswerChanged := True;
end;



procedure TCustomFormFrame.EdtAnswerExit(Sender: TObject);
begin
 CountLbl.Visible := False;
end;


function TCustomFormFrame.GetAnswer: string;
var
  ExtPos: integer;
  ExtStr: string;
  PhoneStr: string;
begin
  try
    case fFieldInfo.ControlType of
    cfGroupHeading: Result:= '';
    cfCheckBox:
      begin
        If cbCheckBox.Checked then
          Result := 'TRUE'
        else
          Result := 'FALSE'
      end;
    cfDate: begin
      Result := DatePicker.Text;
    end;

    cfDateTime: begin
        Result := DTPicker.Text;
    end;

    cfPhone:
      begin
        Result := Trim(edtPhone.Text);

        {EB: Change to add extension (or trim off if it is not used)}
        ExtPos := Pos(EXT, edtPhone.Text);
        PhoneStr := Trim(copy(edtPhone.Text, 0, ExtPos-1));
        if ExtPos > 0 then begin
          ExtStr := Trim(Copy(edtPhone.Text, ExtPos + length(EXT), Length(edtPhone.Text)));
          if ExtStr = '' then
            Result := PhoneStr;
        end;
        {Check the main part of the string to see if it is viable}
        PhoneStr := StringReplace(PhoneStr, ' ', '', [rfReplaceAll]);
		if length(PhoneStr) < 13  then
          Result := EMPTY_ANSWER;
      end;

    cfFreeForm:
        Result := edtAnswer.Text;

//    cfLocDropDown:
//        Result := trim(techList.SelectedText);

    cfFirstLast:
        Result := trim(edtFirst.Text) + DBLSPACE + trim(edtLast.Text);

    cfCustomDropDown:
       Result := cmboCustom.Text;
    end;

    if (Result = '') then Result := EMPTY_ANSWER;
    fFieldInfo.Answer := Result;

  except
    Result := EMPTY_ANSWER;
    raise Exception.Create('Unable to retrieve answer for ' + '''' + edtQuestion.Text + '''');
  end;
end;


function TCustomFormFrame.GetAnswerChanged: boolean;
begin
  Result := fFieldInfo.AnswerHasChanged;
end;

function TCustomFormFrame.HasRestriction: boolean;
begin
  Result := False;  {For now, we do not have restriction code}
end;

function TCustomFormFrame.InitQuestionMemo: Boolean;
begin
  edtQuestion.Clear;
  edtQuestion.WordWrap := True;
  case fFieldInfo.ControlType of
    cfFreeForm, cfPhone, cfDate, cfDateTime: begin
      edtQuestion.Text := fFieldInfo.Question;
      edtQuestion.Visible := True;
      edtQuestion.Refresh;
      edtQuestion.ReadOnly := True;
    end;
    cfCheckBox: begin
      edtQuestion.Text := '';
      edtQuestion.Visible := False;
    end;
    cfGroupHeading: begin
      edtQuestion.Text := ' ' + fFieldInfo.Question;
      edtQuestion.Visible := True;
      edtQuestion.ReadOnly := True;
      edtQuestion.Refresh;
    end;
    cfFirstLast: begin
      edtQuestion.Text := fFieldInfo.Question;
      edtQuestion.Visible := True;
      edtQuestion.ReadOnly := True;
      edtQuestion.Refresh;
    end;
  end;
end;






{---------------------------------------}
function TCustomFormFrame.SaveAnswer: TFieldInfo;
begin
  fFieldInfo.Answer := GetAnswer;
  Result := fFieldInfo;
end;



procedure TCustomFormFrame.SetAnswerChanged(value: boolean);
begin
  fFieldInfo.AnswerHasChanged := value;
end;

procedure TCustomFormFrame.SetExistingAnswer(pFieldInfo:TFieldInfo);
var
  BrkPos: integer;
begin
  try
  fFieldInfo.ExistingAnswerID := pFieldInfo.ExistingAnswerID;
  fFieldInfo.ExistingAnswerDate := pFieldInfo.ExistingAnswerDate;
  case fFieldInfo.ControlType of
    cfFreeForm:     edtAnswer.Text := pFieldInfo.Answer;
    cfPhone:        edtPhone.Text := pFieldInfo.Answer;
    cfDate:         DatePicker.Date := StrToDate(pFieldInfo.Answer);
    cfDateTime:     DTPicker.Date := StrToDateTime(pFieldInfo.Answer);
    cfCheckBox:
    begin
        if pFieldInfo.Answer = 'TRUE' then
          cbCheckBox.Checked := True
        else
          cbCheckBox.Checked := False;
    end;

    cfFirstLast:
    begin
      BrkPos := ansipos(DBLSPACE, pFieldInfo.Answer);
      edtFirst.Text := trim(AnsiLeftStr(pFieldInfo.Answer, BrkPos));
      edtLast.Text  := trim(AnsiRightStr(pFieldInfo.Answer, BrkPos + 1));
    end;
   end;
  except
    ShowMessage('There was an error setting previous answer for: ' + fFieldInfo.Question);
  end;
  AnswerChanged := False;
end;

procedure TCustomFormFrame.SideButton(pShow: Boolean);
begin
  if pShow then begin
    pnlSide.Visible := True;
    btnClearIt.Caption := 'Clear';
    btnClearIt.Colors.Normal := fFieldInfo.Color;
    btnClearIt.Visible := True;
  end
  else begin
    pnlSide.Visible := False;
    btnClearIt.Visible := False;
  end;
end;

end.


