unit OdAdoSqlXml;

interface

uses
  Classes, SysUtils, ADODB_TLB, Variants, Windows;

type
  TStatusEvent = procedure(Msg: string) of object;

  TOdAdoSqlXmlRunner = class
  private
    Conn: ADODB_TLB.Connection;
    Stream: ADODB_TLB.Stream;
    Cmd: ADODB_TLB.Command;
    Records: OleVariant;
    RootTag: string;
    FOnStatus: TStatusEvent;
    FXmlOutput: string;
    procedure ShowStatus(Status: string);
    procedure RunQuery;
  public
    constructor Create(const StoredProcName, ConnectionString, SQLXMLID: string);
    procedure CreateParameter(const Name: WideString; Type_: DataTypeEnum;
                             Direction: ParameterDirectionEnum;
                             Size: Integer; Value: OleVariant);
    function GetXmlResult: string;
    function GetXmlResultInAnotherThread: string;
    procedure RunLowPriority;
    destructor Destroy; override;
    property OnStatus: TStatusEvent read FOnStatus write FOnStatus;
  end;

implementation

uses
  OdMiscUtils;

type
  TWorkerThread = class(TThread)
  private
    FRunner: TOdAdoSqlXmlRunner;
    FFailureException: Exception;
    FWorking: Boolean;
  public
    constructor Create(Runner: TOdAdoSqlXmlRunner);
    procedure Go;
  protected
    procedure Execute; override;
  end;

{ TOdAdoSqlXmlRunner }

constructor TOdAdoSqlXmlRunner.Create(const StoredProcName, ConnectionString, SQLXMLID: string);
begin
  ShowStatus('Creating Connection');
  Conn := CoConnection.Create;
  ShowStatus('Connecting to DB');
  // The "Data " prefix here assumes the first of the old params is "Provider="
  Conn.Open('Provider='+ SQLXMLID +';Data ' + ConnectionString, '', '', 0);

  ShowStatus('Creating Command');
  Cmd := CoCommand.Create;
  Cmd.Set_ActiveConnection(Conn);
  Cmd.CommandType := adCmdText;
  Cmd.CommandType := adCmdStoredProc;
  Cmd.CommandText := StoredProcName;
  Cmd.Properties['Output Encoding'].Value := DefaultXMLEncoding;
  Cmd.Properties['ClientSideXML'].Value := True;
end;

procedure TOdAdoSqlXmlRunner.CreateParameter(const Name: WideString;
 			     Type_: DataTypeEnum;
                             Direction: ParameterDirectionEnum;
                             Size: Integer; Value: OleVariant);
var
  P1: ADODB_TLB.Parameter;
begin
  ShowStatus('Adding Parameter');
  P1 := Cmd.CreateParameter(Name, Type_, Direction, Size, Value);
  Cmd.Parameters.Append(P1);
end;

destructor TOdAdoSqlXmlRunner.Destroy;
begin
  ShowStatus('Closing Stream / Cmd / Conn');
  Stream := nil;
  Cmd := nil;
  Conn := nil;
  inherited;
end;

function TOdAdoSqlXmlRunner.GetXmlResult: string;
begin
  RunQuery;
  Result := FXmlOutput;
end;

function TOdAdoSqlXmlRunner.GetXmlResultInAnotherThread: string;
var
  Worker: TWorkerThread;
begin
  Worker := TWorkerThread.Create(Self);
  try
    Worker.Go;
  finally
    FreeAndNil(Worker);
  end;
  Result := FXmlOutput;
end;

// An idea:
procedure TOdAdoSqlXmlRunner.RunLowPriority;
var
  OldPriority: Integer;
begin
  // Run it here, but at a lower priority.
  OldPriority := GetThreadPriority(GetCurrentThread);
  SetThreadPriority(GetCurrentThread, THREAD_PRIORITY_BELOW_NORMAL);
  try
    RunQuery;
  finally
    SetThreadPriority(GetCurrentThread, OldPriority);
  end;
end;

procedure TOdAdoSqlXmlRunner.ShowStatus(Status: string);
begin
  if Assigned(FOnStatus) then
    FOnStatus(Status);
end;

procedure TOdAdoSqlXmlRunner.RunQuery;
var
  ProcInstr: string;
begin
  ShowStatus('Creating Stream');
  Stream := CoStream.Create();
  Stream.Type_ := adTypeText;
  Stream.Open(EmptyParam, adModeUnknown, adOpenStreamUnspecified, '', '');
  Stream.LineSeparator := adCRLF;
  Stream.Charset := DefaultXMLEncoding;

  ShowStatus('Setting Output Stream');
  Cmd.Properties['Output Stream'].Value := Stream;

  Records := Unassigned;
  ShowStatus('Executing SP');
  Cmd.Execute(Records, EmptyParam, adExecuteStream);

  RootTag := 'ROOT';
  ShowStatus('Reading Results from Stream');
  // Assume the database code page is Western Latin-1 (windows-1252)
  ProcInstr := Format('<?xml version="1.0" encoding=''%s''?>', [DefaultXMLEncoding]) + sLineBreak;
  // adReadAll is -1, but this fixes a Delphi compiler warning
  FXmlOutput := ProcInstr + '<' + RootTag + '>' + Stream.ReadText(-1) + '</' + RootTag + '>';
  // Remove any invalid XML characters SQLXML may have included in the results
  FXmlOutput := ReplaceControlChars(FXmlOutput);
  //SaveDebugFile('QMLOutput.xml', FXmlOutput);
end;

{ TWorkerThread }

constructor TWorkerThread.Create(Runner: TOdAdoSqlXmlRunner);
begin
  inherited Create(True);
  FRunner := Runner;
  Priority := tpLower;   // Do work at a low priority
  FWorking := True;
end;

procedure TWorkerThread.Execute;
begin
  inherited;
  try
    Assert(Assigned(FRunner), 'Must have Runner set');
    FRunner.RunQuery;
    FWorking := False;
  except
    on E: Exception do begin
      FFailureException := E;
      FWorking := False;
    end;
  end;
end;

procedure TWorkerThread.Go;
begin
  Resume;    // Go.
  while FWorking do
    Sleep(100);

    WaitFor;          // finish work
  if FFailureException <> nil then
    raise FFailureException;
end;

end.

