unit OdXmlDataSet;

interface

uses
  MSXML2_TLB, DB, kbmMemTable, OdIsoDates, SysUtils;

function XD2DF(XD: string): TFieldType;

procedure PopulateDataSet(XMLDoc: IXMLDomDocument;
  DS: TkbmMemTable; const TableName: string);

procedure SetUpFields(XMLDoc: IXMLDomDocument;
  DS: TkbmMemTable);

function GetDeltaXML(DS: TDataSet): string;

implementation

function XD2DF(XD: string): TFieldType;
begin
 If       XD = 'i4'         Then XD2DF := ftInteger
  Else If XD = 'string'     Then XD2DF := ftString
  Else If XD = 'fixed.14.4' Then XD2DF := ftCurrency
  Else If XD = 'i2'         Then XD2DF := ftSmallInt
  Else If XD = 'boolean'    Then XD2DF := ftBoolean
  Else If XD = 'uri'        Then XD2DF := ftString
  Else If XD = 'datetime'   Then XD2DF := ftDateTime
  Else If XD = 'dateTime'   Then XD2DF := ftDateTime
  Else If XD = 'uuid'       Then XD2DF := ftGUID
  Else If XD = 'r4'         Then XD2DF := ftFloat
  Else XD2DF := ftString
end;

procedure PopulateDataSet(XMLDoc: IXMLDomDocument;
  DS: TkbmMemTable; const TableName: string);
var
  Nodes    : IXMLDOMNodeList;
  Node     : IXMLDOMNode;
  I,J      : Integer;
  Field: TField;
begin
  DS.Open;
  DS.EmptyTable;

 Nodes := XMLDoc.SelectNodes('//' + TableName);
 With DS do Begin
  For I := 0 to Nodes.Length-1 do
   begin
    DS.Append;
    For J := 0 to Nodes[I].ChildNodes.Length-1 do begin
      Node := Nodes[I].ChildNodes[J];
      Field := FieldByName(Nodes[I].ChildNodes[J].NodeName);
      if Field<>nil then begin
        if Field.DataType = ftDateTime then
          Field.AsDateTime := IsoStrToDateTime( Node.Text)
        else
          Field.AsString := Node.Text;
      end;
     end;
    DS.Post;
   end;
 End;

// Move to the first row
 DS.First;
end;

procedure SetUpFields(XMLDoc: IXMLDomDocument; DS: TkbmMemTable);
var
 Nodes: IXMLDOMNodeList;
 Node: IXMLDOMNode;
 I: Integer;
 Success: Boolean;
begin
  if DS.FieldDefs.Count = 0 then begin
    Success := False;
  // Create Field Definitions
   Nodes := XMLDoc.SelectNodes('//Schema/ElementType');
    With DS do Begin
      For I := 1 to Nodes.Length-1 do Begin
        Node := Nodes[I];
        With FieldDefs.AddFieldDef do
         Begin
           Success := True;
          Name     := Node.Attributes.GetNamedItem('name').Text;
          DataType := XD2DF(Node.Attributes.GetNamedItem('dt:type').Text);
          if Name = 'image' then
            DataType := ftMemo;
          if DataType = ftString then
            Size := 150;
         End;
      End;
    End;
    if not Success then
      raise Exception.Create('Error - no field defs in data file.');
  end;
end;

function GetDeltaXML(DS: TDataSet): string;
var
  Doc : IXMLDomDocument;
  Root, sync : IXMLDOMElement;
  before, after, row: IXMLDOMElement;

  id: string;
  newStatus: string;
begin
  Doc := CoDOMDocument.Create;

  Root := Doc.createElement('ROOT');
  Root.setAttribute('xmlns:updg','urn:schemas-microsoft-com:xml-updategram');
  Doc.appendChild(Root);

  sync := Doc.createElement('updg:sync');
  Root.appendChild(sync);

  DS.First;
  while not DS.EOF do begin
    id := DS.FieldByName('ticket_id').AsString;
    newStatus := DS.FieldByName('quickclose').AsString;
    if newStatus<>'' then begin
      before := Doc.createElement('updg:before');
      row := Doc.createElement('ticketmock');
      row.setAttribute('ticket_id',id);
      before.appendChild(row);
      sync.appendChild(before);

      after := Doc.createElement('updg:after');

      row := Doc.createElement('ticketmock');
      row.setAttribute('status',newStatus);
      //row.setAttribute('updg:at-identity', "x")
      after.appendChild(row);

      sync.appendChild(after);
    end;
    DS.Next;
  end;

  Result := Doc.xml;
end;

end.

