unit OdSqlXmlOutput;

interface

uses
  ADODB;

type
  TDataSetInfoEvent = procedure (var TableName: string; var Count: Integer) of object;

function FormatSqlXml(MultiResultSetSP: TCustomADODataSet;
  InitialSizeMB: Integer = 1; OnProgress: TDataSetInfoEvent = nil;
  MaxRows: Integer = 0): string;

implementation

uses
  SysUtils, OdDataSetToXml, Classes, OdMiscUtils;

// This assumes the first column is the table name to use for returned rows
function FormatSqlXml(MultiResultSetSP: TCustomADODataSet;
  InitialSizeMB: Integer; OnProgress: TDataSetInfoEvent;
  MaxRows: Integer): string;
var
  RecAffected: Integer;
  NextData: _Recordset;
  Doc: TMemoryStream;
  DSet: TADODataSet;
  TotalRecs: Integer;

  procedure WriteString(const Str: string);
  begin
    WriteStringToStream(Doc, Str);
  end;

  procedure UpdateProgress(TableName: string; Count: Integer);
  begin
    if Assigned(OnProgress) then
      OnProgress(TableName, Count);
  end;

  procedure DoRow(D: TCustomADODataSet);
  var
    i: Integer;
  begin
    WriteString('<');
    WriteString(D.Fields[0].AsString);
    WriteString(' ');
    for i := 1 to D.FieldCount - 1 do begin
      if not D.Fields[i].IsNull then
      begin
        WriteString(D.Fields[i].FieldName);
        WriteString('="');
        XmlEscape(XmlValue(D.Fields[i]), Doc);
        WriteString('" ');
      end;
    end;
    WriteString('/>');
  end;

  procedure Process(D: TCustomADODataSet);
  var
    TableName: string;
    RowCount: Integer;
  begin
    RowCount := 0;
    // the sync sp adds the table name into the results tname field.
    if not D.EOF and Assigned(D.FindField('tname')) then
      TableName := D.FieldByName('tname').AsString
    // use the first real fieldname to get something meaningful
    else if D.FieldCount > 1 then
      TableName := D.Fields[1].FieldName
    else
      TableName := D.Fields[0].FieldName;
    UpdateProgress(TableName, RowCount);

    while not D.EOF do begin
      DoRow(D);
      D.Next;
      Inc(TotalRecs);
      Inc(RowCount);
      if RowCount mod 100 = 0 then
        UpdateProgress(TableName, RowCount);
      if TotalRecs mod 100 = 0 then
        Sleep(1);       // this will share the CPU more nicely
      if (MaxRows > 0) and (RowCount > MaxRows) then
        raise Exception.Create('Unexpectedly large dataset in FormatSqlXml, Table: ' + TableName);
    end;
    UpdateProgress(TableName, RowCount);
  end;

begin
  DSet := nil;
  Doc := TMemoryStream.Create;
  try
    Doc.SetSize(1024 * 1024 * InitialSizeMB);
    WriteString(Format('<?xml version="1.0" encoding="%s"?>', [DefaultXMLEncoding]) + sLineBreak + '<ROOT>');
    DSet := TADODataSet.Create(nil);
    DSet.CacheSize := 100;
    //DSet.CursorLocation := clUseClient;    KJC: would this help?
    DSet.CursorType := ctOpenForwardOnly;
    TotalRecs := 0;
    Process(MultiResultSetSP);
    NextData := MultiResultSetSP.NextRecordset(RecAffected);
    while NextData <> nil do begin
      DSet.Recordset := NextData;
      Process(DSet);
      NextData := MultiResultSetSP.NextRecordset(RecAffected);
    end;
    WriteString('</ROOT>');
    Doc.Size := Doc.Position;
    SetLength(Result, Doc.Size);
    Doc.Position := 0;
    Doc.ReadBuffer(Result[1], Doc.Size);
  finally
    FreeAndNil(Doc);
    FreeAndNil(DSet);
  end;
end;

end.
