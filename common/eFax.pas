{ This unit contains methods that use the eFax API (.NET Assembly via COM))

{TODO - Install.NET assembly into GAC with GACUTil:
  gacutil -i C:\Windows\SysWOW64\eFaxDeveloper.dll
 - Register the Assembly
  regasm C:\Windows\SysWOW64\eFaxDeveloper.dll}

unit eFax;

interface

uses
  SysUtils, Variants, Classes, ReportConfig, OdMiscUtils, eFaxDeveloper_TLB;

  procedure SendEFax(FaxNumber, FaxId, FaxHeader, RecipientName, RecipientCompany: string;
    PdfStream: TStream; Config: TReportConfig; var ExtDocID: string);

implementation

//Send eFax is used by the report engine
procedure SendEFax(FaxNumber, FaxId, FaxHeader, RecipientName, RecipientCompany: string;
  PdfStream: TStream; Config: TReportConfig; var ExtDocID: string);
var
  AOutboundClient : OutboundClient;
  AOutboundRequest : OutboundRequest;
  AFaxFile : FaxFile;
  AOutboundResponse : OutboundResponse;
begin
  try
    // A) Create an instance of OutboundClient to send an outbound fax
    AOutboundClient := CoOutboundClient.Create;

    // B) you have 2 options to configure the settings of OutboundClient
    // 1) load (previously saved) settings from the configuration file as follow
    //outboundClient.LoadConfiguration();
    // 2) Set the efaxDeveloper settings manually
    AOutboundClient.AccountID := Config.FaxAccountID;
    AOutboundClient.UserName :=  Config.FaxUserName;
    AOutboundClient.Password := Config.FaxPassword;

    // C) Create an instance of OutboundRequest, that will encapsulate all the data of the outbound fax
    AOutboundRequest := CoOutboundRequest.Create;
    // Optionally set fax transmission control settings
    // TransmissionID can have a max length 15
    AOutboundRequest.TransmissionControl.TransmissionID := FaxId;
    // CustomerID is optional; is Alphanumeric and can have a max length 20
    //   outboundRequest.TransmissionControl.CustomerID = "CustomerID_1234";
    // Set NoDuplicates (default is Disable)
    //   outboundRequest.TransmissionControl.NoDuplicates = NoDuplicates.Enable;
    // Set Resolution (there is no default settings)
    //   outboundRequest.TransmissionControl.Resolution = Resolution.Fine;
    AOutboundRequest.TransmissionControl.Resolution := Resolution_Fine;
    // Set Priority (default is Normal); note that eFax charges a premium on high priority faxes
    //   outboundRequest.TransmissionControl.Priority = Priority.High;
    // Set SelfBusy (default is Enable)
    //   outboundRequest.TransmissionControl.SelfBusy = SelfBusy.Disable;
    // Set FaxHeader (Default is "the default fax header")
    // (Looks like: '@DATE1 @TIME3 My Company Name @ROUTETO{26} @RCVRFAX Pg%P/@SPAGES'
    AOutboundRequest.TransmissionControl.FaxHeader := FaxHeader;

    // D) Set the fax recipient
    AOutboundRequest.Recipient.RecipientName := RecipientName;
    AOutboundRequest.Recipient.RecipientCompany := RecipientCompany;
    AOutboundRequest.Recipient.RecipientFax := FaxNumber;

    // E) Add file(s) to fax
    AFaxFile := CoFaxFile.Create;
    AFaxFile.FileType := FaxFileType_pdf;
    AFaxFile.SetFileContentsCOM(StreamToOleVariant(PdfStream));
    AOutboundRequest.Files.Add(AFaxFile);

    // F) Set the disposition settings
    // Set the disposition level - this indicates whether dispositions should be
    // sent to us only for Success, Error, or for both.
    AOutboundRequest.DispositionControl.DispositionLevel := DispositionLevel_Both;

    // Set the disposition method - can be via HTTP POST or email
    // We choose to receive the disposition via http POST
    AOutboundRequest.DispositionControl.DispositionMethod := DispositionMethod_Post;
    // Set the DispositionURL where efaxDeveloper should send the XML disposition for this fax transmission
    AOutboundRequest.DispositionControl.DispositionURL := Config.FaxDispositionURL;

    // G) Send the oubound fax to efaxDeveloper and get an instance of OutboundResponse back
    AOutboundResponse := CoOutboundResponse.Create;
    AOutboundResponse := AOutboundClient.SendOutboundRequest(AOutboundRequest);

    // Check if the fax was accepted successfully
    if AOutboundResponse.StatusCode = StatusCode_Success then
      ExtDocID := AOutboundResponse.docId
    else
      //No ExtDocID can be generated since transmission to eFax failed.
      raise Exception.CreateFmt('eFax: %s: %s fm_id: %s',
        [AOutboundResponse.StatusDescription, AOutboundResponse.ErrorMessage, FaxID]);
  except
    on E:Exception do
      raise Exception.Create('Error transmitting to eFax. ' + E.Message);
  end;
end;

end.
