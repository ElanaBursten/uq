unit OdUrlMon;

interface

function HttpGet(const Url: string): string;

var
  InetAppName: string;

implementation

uses
  SysUtils, Classes, Windows, ComObj, MSXML2_TLB, OdExceptions, OdNetUtils;

function HttpGet(const Url: string): string;
var
  Request: IXMLHTTPRequest;
begin
  Request := CoXMLHTTP30.Create;
  Request.Open('GET', Url, False, '', '');
  //Request.OnReadyStateChange := EventObject.Create(Request) as IDispatch;
  try
    try
      Request.Send('');
    except
      // When the network is down, this exception is common:
      // EOleExceptionConnection.ErrorCode=-2146697211: The system could not locate the resource specified
      on E: EOleException do
        raise EOdNetworkException.Create('Unable to contact ' + GetHostFromURL(Url) +'.  Please check your Internet connection.' )
      else
        raise;
    end;
  finally
    Request.OnReadyStateChange := nil;
  end;
  Result := Request.ResponseText;
end;

{
httpRequest->open("POST", UTL_Null(url), vAsync, vUsr, vPwd);
httpRequest->setRequestHeader("Content-Type","text/xml");
httpRequest->send(vInXML);
CString responseText = httpRequest->GetResponseText();
}

end.

