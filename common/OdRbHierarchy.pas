unit OdRbHierarchy;

interface

uses
  Classes, SysUtils, ppBands, ppClass, ppVar, ppReport, ppModule,
  ppCtrls, ppTypes, ppClasUt;

procedure AddHierGroups(Report: TppReport; LabelToIndent: TppComponent;
  Indent: Single = 0.20);

implementation

const
  LeftMargin: Single = 0.0104;

type
  THierarchySupport = class(TComponent)
  private
    Indent: Single;
    procedure HideEmptyFooterBand(Sender: TObject);
    procedure SetIndentation(Sender: TObject);
    procedure AddGroup(Report: TppReport; HLevel: Integer);
  end;

function FindCountControl(Band: TppGroupFooterBand): TppDBCalc;
var
  ControlIndex: Integer;
  Component: TComponent;
begin
  Result := nil;
  for ControlIndex := 0 to Band.ObjectCount-1 do begin
    Component := Band.Objects[ControlIndex];
    if Component is TppDBCalc then
      if (Component as TppDBCalc).DBCalcType = dcCount then
        Result := (Component as TppDBCalc);
  end;
end;

{ THierarchySupport }

procedure THierarchySupport.SetIndentation(Sender: TObject);
var
  Band: TppBand;
  Level: Integer;
begin
  inherited;
  Band := (Sender as TppComponent).Band;
  try
    Level := Band.Report.DataPipeline.GetFieldValue('h_report_level');
  except
    Level := 1;
  end;
  (Sender as TppComponent).Left := LeftMargin + Indent * (Level-1);
end;

procedure THierarchySupport.HideEmptyFooterBand(Sender: TObject);
var
  Band: TppGroupFooterBand;
  FieldName: string;
  Value: string;
  CountControl: TppDBCalc;
begin
  inherited;
  Band := Sender as TppGroupFooterBand;
  FieldName := Copy(Band.Description, Length(Band.Description)-2, 3);
  Value := Band.Report.DataPipeline.GetFieldAsString(FieldName);
  CountControl := FindCountControl(Band);
  Assert(Assigned(CountControl), 'CountControl not found');
  Band.Visible := (Value <> '') and (CountControl.Value > 1);
end;

type
  TppComponentClass = class of TppComponent;

procedure THierarchySupport.AddGroup(Report: TppReport; HLevel: Integer);
var
  SourceGroup, Group: TppGroup;
  SourceBand, Band: TppGroupBand;
  SourceComp, NewComp: TppComponent;
  SourceShape, NewShape: TppShape;
  I: Integer;
begin
  // Groups[0] is hopefully the outermost group fo
  SourceGroup := Report.Groups[0];
  SourceBand := SourceGroup.FooterBand as TppGroupBand;

  SourceBand.BeforePrint := HideEmptyFooterBand;

  Group := TppGroup(ppComponentCreate(Report, TppGroup));
  Group.Report := Report;

  Group.BreakName    := 'h_' + IntToStr(HLevel);
  Group.DataPipeline := Report.DataPipeline;

  Band := TppGroupBand(ppComponentCreate(Report, TppGroupHeaderBand));
  Band.Group := Group;

  Band := TppGroupBand(ppComponentCreate(Report, TppGroupFooterBand));
  Band.Group := Group;
  Band.Height := SourceBand.Height;

  Band.BeforePrint := HideEmptyFooterBand;

  for I := 0 to SourceBand.ObjectCount - 1 do begin
    SourceComp := SourceBand.Objects[I];

    NewComp := ppComponentCreate(Report, TComponentClass(SourceComp.ClassType)) as TppComponent;

    NewComp.Band := Band;
    NewComp.Left := SourceComp.Left;
    NewComp.Top := SourceComp.Top;
    NewComp.Font := SourceComp.Font;
    NewComp.Width := SourceComp.Width;
    NewComp.Height := SourceComp.Height;
    NewComp.Text := SourceComp.Text;
    NewComp.Alignment := SourceComp.Alignment;
    NewComp.Visible := SourceComp.Visible;
    NewComp.AutoSize := SourceComp.AutoSize;
    NewComp.Tag := SourceComp.Tag;

    if NewComp is TppDBCalc then begin
      (NewComp as TppDBCalc).DBCalcType := (SourceComp as TppDBCalc).DBCalcType;
      (NewComp as TppDBCalc).DataPipeline := (SourceComp as TppDBCalc).DataPipeline;
      (NewComp as TppDBCalc).DataField := (SourceComp as TppDBCalc).DataField;
      (NewComp as TppDBCalc).DisplayFormat := (SourceComp as TppDBCalc).DisplayFormat;
      (NewComp as TppDBCalc).Transparent := (SourceComp as TppDBCalc).Transparent;
    end;

    if NewComp is TppLabel then begin
      (NewComp as TppLabel).Transparent := (SourceComp as TppLabel).Transparent;
    end;

    if NewComp is TppDBText then begin
      (NewComp as TppDBText).DataPipeline := (SourceComp as TppDBText).DataPipeline;
      (NewComp as TppDBText).DataField := (SourceComp as TppDBText).DataField;
      (NewComp as TppDBText).DisplayFormat := (SourceComp as TppDBText).DisplayFormat;
      (NewComp as TppDBText).Transparent := (SourceComp as TppDBText).Transparent;

      if (SourceComp as TppDBText).DataField = 'h_1' then begin
        (NewComp as TppDBText).DataField := 'h_' + IntToStr(HLevel);
        NewComp.Left := SourceComp.Left + Indent * (HLevel-1);
      end;
    end;

    if NewComp is TppShape then begin
      SourceShape := SourceComp as TppShape;
      NewShape := NewComp as TppShape;

      NewShape.Brush := SourceShape.Brush;
      NewShape.Pen := SourceShape.Pen;
      NewShape.Shape := SourceShape.Shape;
    end;
  end;
end;

procedure SetResetGroups(Report: TppReport);
var
  I: Integer;
  ControlIndex: Integer;
  Band: TppGroupFooterBand;
  Component: TComponent;
begin
  for I := 0 to Report.GroupCount-1 do begin
    Band := Report.Groups[I].FooterBand as TppGroupFooterBand;
    for ControlIndex := 0 to Band.ObjectCount-1 do begin
      Component := Band.Objects[ControlIndex];
      if Component is TppDBCalc then
        (Component as TppDBCalc).ResetGroup := Report.Groups[I];
    end;
  end;
end;

procedure AddHierGroups(Report: TppReport; LabelToIndent: TppComponent;
  Indent: Single);
var
  MyHS: THierarchySupport;
  Level: Integer;
begin
  MyHS := THierarchySupport.Create(Report.Owner);
  MyHS.Indent := Indent;
  LabelToIndent.OnPrint := MyHS.SetIndentation;
  for Level := 2 to 9 do
    MyHS.AddGroup(Report, Level);
  SetResetGroups(Report);
end;

end.

