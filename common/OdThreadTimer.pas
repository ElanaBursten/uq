unit OdThreadTimer;

interface

uses
  Windows,  SysUtils,
  JclDateTime;

type
  TOdThreadTimer = class
  private
    Create1, Exit1, Kernel1, User1: TFileTime;
    InitialUserTime: TDateTime;
    CurrentUserTime: TDateTime;
    InitialKernelTime: TDateTime;
    CurrentKernelTime: TDateTime;
    Create2, Exit2, Kernel2, User2: TFileTime;
    MyThread: THandle;
    FRunning: Boolean;
  public
    procedure Reset;
    procedure Measure;
    function GetCpuSeconds: single;
    function GetElapsedTime: TDateTime;
    property Running: Boolean read FRunning;
  end;

implementation

{$WARN SYMBOL_PLATFORM OFF}

{ TOdThreadTimer }

function TOdThreadTimer.GetCpuSeconds: single;
begin
  Result := (CurrentUserTime - InitialUserTime)
    * 60 * 60 * 24;
  //Result := Int64(User2) - Int64(User1);
end;

procedure TOdThreadTimer.Reset;
begin
  Win32Check(DuplicateHandle(
       GetCurrentProcess    // source process
     , GetCurrentThread     // handle to duplicate - a pseudo handle
     , GetCurrentProcess    // dest process
     , @MyThread             // result
     , DUPLICATE_SAME_ACCESS   //  rights
     , False                  // inheritance
     , DUPLICATE_SAME_ACCESS
    ));

  Win32Check(GetThreadTimes(MyThread, Create1, Exit1, Kernel1, User1));
  InitialUserTime := FileTimeToDateTime(User1);
  InitialKernelTime := FileTimeToDateTime(Kernel1);
  FRunning := True;
end;

procedure TOdThreadTimer.Measure;
begin
  if Running then begin
    if not GetThreadTimes(MyThread, Create2, Exit2, Kernel2, User2) then
      raise Exception.Create('Measure ' + IntToStr(GetLastError));
    CurrentUserTime := FileTimeToDateTime(User2);
    CurrentKernelTime := FileTimeToDateTime(Kernel2);
  end;
end;

function TOdThreadTimer.GetElapsedTime: TDateTime;
begin
  if Running then
    Result := (CurrentUserTime - InitialUserTime) + (CurrentKernelTime - InitialKernelTime)
  else
    Result := -1.0;
end;

end.
