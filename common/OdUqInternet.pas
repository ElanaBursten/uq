unit OdUqInternet;

interface

uses
  Classes, IdFTP, IdFTPListParseWindowsNT, IdComponent, idHttp, idSSLOpenSSL;

type
  TFTPDownloadConfig = record
    LocationID: Integer;
    User: string;
    Password: string;
    Host: string;
    Port: Integer;
    RootDir: string;
    TimeOut: Integer;
  end;

 //QMANTWO-552 EB
  TAWSConstants = record
    Host: string;                                              // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    RestURL: string;                                           // /prod/qmattach/v1/attachments/LOC_UTL
    Service: string;                                           // execute-api
    StagingBucket: string;                                     // dii-utq-attachments-prod
    StagingFolder: string;                                     // api/LOC_UTL/
    Region: string;                                            // us-east-1
    Algorithm: string;                                         // AWS4-HMAC-SHA256
    SignedHeaders: string;                                     // accept;content-type;host;x-amz-date
    ContentType: string;                                       // application/json
    AcceptedValues: string;                                    // application/octet-stream,application/json
  end;

  TAttachmentData = record
    ForeignTypeDir: string;
    YearDir: string;
    DateDir: string;
    IDDir: string;
    ServerFilename: string;
    OrigFilename:string;
    LocalFilename:string;
    FileHash: string;
    ForeignID:Integer;
    ForeignTypeID:integer;
    AttachmentID:integer;
    EmpID:integer;
    Constants: TAWSConstants;  //AWS QMANTWO-552
    AWSKey,
    AWSSecret: widestring;
    AWSRetMessage: string;
  end;

  TFileComparison = (fcFileOnServerIsBigger, fcFileOnServerIsSmaller, fcFilesHaveSameSize, fcNoFileOnServer);

  const
    AWSSuccess = 'AWS: Success';

   var
    myAttachmentData: TAttachmentData;

function CreateFTPDownloader(Config: TFTPDownloadConfig): TIdFTP;
procedure DownloadAttachmentToFile(LocalFileName: string; var AttachmentData:
  TAttachmentData; Config: TFTPDownloadConfig; OnWork: TWorkEvent=nil; UseURL: Boolean = True);
function Upload_Attachment(var AttachmentData:TAttachmentData; ABaseAttachment:TDataModule):boolean;
function FileExistsOnServer(FTP: TIdFTP; FileName: string): Boolean;
function CheckFileSizeOnFTPServer(FTP: TIdFTP; FileSize: Integer; AttachmentData: TAttachmentData; CurrDirIsAttachmentFolder: Boolean=False): TFileComparison;
function CheckFileHashOnFTPServer(FTP: TIdFTP; AttachmentData: TAttachmentData): Boolean;
function CurrentDirIsAttachmentFolder(FTP: TIdFTP; FTPRootDir: string; AttachmentData: TAttachmentData): Boolean;
procedure ChangeToAttachmentFolder(FTP: TIdFTP; AttachmentData: TAttachmentData);
procedure ChangeToFTPRootFolder(FTP: TIdFTP; FTPRootDir: String);
function ReformatMessage(RetMessage:string): string;

implementation

uses
  Windows, SysUtils, OdSecureHash, OdMiscUtils, IdSSLOpenSSLHeaders,
  IdFTPCommon, OdFTP, Dialogs, Forms, uDLLIntf, BaseAttachmentDMu;


type
  TLogger = class(TInterfacedObject, ILog)
  protected
    FBaseAttachment:TBaseAttachment;
    function DoLog(APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring;
  public
    constructor create(ABaseAttachment:TBaseAttachment);
    destructor destroy; override;
  end;


function Upload_Attachment(var AttachmentData:TAttachmentData; ABaseAttachment:TDataModule):boolean;
var
  ERRORMSG: WideString;
  Phase: string;
  ABaseAttachmentIsLocal: boolean;  //QMANTWO-556 EB (Because Larry is not passing in associated BaseAttachment)
begin
  AttachmentData.AWSRetMessage := '';
  if not assigned(ABaseAttachment) then begin
    ABaseAttachment:=TBaseAttachment.Create(application);
	  ABaseAttachmentIsLocal := True;
  end else ABaseAttachmentIsLocal := False;

  SetLog(TLogger.Create(ABaseAttachment as TBaseAttachment));
  Phase := 'AWS Command: PutImange ' + AttachmentData.LocalFileName;
  try
    result :=
      PutImage(
        AttachmentData.Constants.Host                //  p8bchtkc9h.execute-api.us-east-1.amazonaws.com
        ,AttachmentData.Constants.RestURL            // /develop/qmattach/v1/attachments/LOC_UTL/
        ,AttachmentData.Constants.Service            // execute-api
        ,AttachmentData.Constants.StagingBucket      // dii-utq-attachments-prod
        ,AttachmentData.Constants.StagingFolder      // api/LOC_UTL/
        ,AttachmentData.Constants.Region             // us-east-1
        ,AttachmentData.Constants.Algorithm          // AWS4-HMAC-SHA256
        ,AttachmentData.Constants.SignedHeaders      // accept;content-type;host;x-amz-date
        ,AttachmentData.Constants.ContentType        // application/json
        ,AttachmentData.Constants.AcceptedValues     // application/octet-stream,application/json
        ,AttachmentData.AWSKey                       // Tc6HixX1ft8fPOFZVSkESmnMEhkDdOLjPa5XuwXa9wE=
        ,AttachmentData.AWSSecret                    // +rqRUh8fbAKPIH7vVlNV2JMxSib6QvK1u1Fk4e2+ncCGO5oo79aZyOGn/4W6leef
        ,AttachmentData.AttachmentID
        ,AttachmentData.LocalFilename
        ,AttachmentData.ServerFilename
        ,ERRORMSG
      );
      if Result then
        MyAttachmentData.AWSRetMessage := AWSSuccess
      else
        MyAttachmentData.AWSRetMessage := ERRORMSG;
  finally
    SetLog(nil);
	  if ABaseAttachmentIsLocal then FreeAndNil(ABaseAttachment);
  end
end;

procedure DownloadAttachmentToFile(LocalFilename: string;
  var AttachmentData: TAttachmentData; Config: TFTPDownloadConfig;
  OnWork: TWorkEvent = nil; UseURL: Boolean = True);
{FTP is dead.  We must support multiple means to pull from AWS.  DownloadAttachmentToFile
is now able to use the direct download and also retrieve the URL and resolve it to file.
QMANTWO-579   sr }
var
  Phase: string;
  AImageURL, ERRORMSG, AWSFILENAME: widestring;
  fileStream : TfileStream;
  aIdHttp: TidHttp;
  aHandler : TidSSLIOHandlerSocketOpenSSL;
begin
  if UseURL then
  begin
    try
      myAttachmentData.AWSRetMessage := '';
      Phase := 'AWS Command: GetImange ' + AttachmentData.LocalFilename;
      if AttachmentData.AWSKey <> '' then
        myAttachmentData := AttachmentData;
      if not GetImageurl(myAttachmentData.Constants.Host // o9mwuetv47.execute-api.us-east-1.amazonaws.com
        , myAttachmentData.Constants.RestURL             // /prod/qmattach/v1/attachments/LOC_UTL
        , myAttachmentData.Constants.Service             // execute-api
        , myAttachmentData.Constants.StagingBucket       // dii-utq-attachments-prod
        , myAttachmentData.Constants.StagingFolder       // api/LOC_UTL/
        , myAttachmentData.Constants.Region              // us-east-1
        , myAttachmentData.Constants.Algorithm           // AWS4-HMAC-SHA256
        , myAttachmentData.Constants.SignedHeaders       // accept;content-type;host;x-amz-date
        , myAttachmentData.Constants.ContentType         // application/json
        , myAttachmentData.Constants.AcceptedValues      // application/octet-stream,application/json
        , myAttachmentData.AWSKey, myAttachmentData.AWSSecret
        , myAttachmentData.AttachmentID
        , AImageURL   //QMANTWO-579   sr
        , ERRORMSG)

      then
        myAttachmentData.AWSRetMessage := 'AWS: Unable to retrieve attachment URL. ' +
          #13#10 + ' - File Name: ' + myAttachmentData.OrigFilename + #13#10 +
          ' - Attachment ID#: ' + IntToStr(myAttachmentData.AttachmentID) + #13#10
          + #13#10 + ReformatMessage(ERRORMSG)

      else
      begin
        aIdHttp := TidHttp.create(nil);    //QMANTWO-579   sr
        aHandler := TidSSLIOHandlerSocketOpenSSL.Create(nil);    //QMANTWO-579   sr
        aHandler.SSLOptions.Mode:= sslmUnassigned;  //qm-766 sr
        aHandler.SSLOptions.Method:=sslvSSLv23;    //qm-766 sr
        aIdHttp.IOHandler := aHandler;   //QMANTWO-579   sr

        fileStream :=  TfileStream.Create(LocalFilename, fmCreate);   //QMANTWO-579   sr
        aIdHttp.Get(trim(AImageURL), fileStream);   //QMANTWO-579   sr
        myAttachmentData.AWSRetMessage := AWSSuccess;
      end;

    finally
      FreeAndNil(fileStream);
      FreeAndNil(aIdHttp);
      FreeAndNil(aHandler);
    end;
  end
  else
  begin
      MyAttachmentData.AWSRetMessage := '';
      Phase := 'AWS Command: GetImange ' + AttachmentData.LocalFileName;
      if AttachmentData.AWSKey <> '' then
        myAttachmentData := AttachmentData;
      if not
        GetImage(
             myAttachmentData.Constants.Host             // o9mwuetv47.execute-api.us-east-1.amazonaws.com
            ,myAttachmentData.Constants.RestURL          // /prod/qmattach/v1/attachments/LOC_UTL
            ,myAttachmentData.Constants.Service          // execute-api
            ,myAttachmentData.Constants.StagingBucket    // dii-utq-attachments-prod
            ,myAttachmentData.Constants.StagingFolder    // api/LOC_UTL/
            ,myAttachmentData.Constants.Region           // us-east-1
            ,myAttachmentData.Constants.Algorithm        // AWS4-HMAC-SHA256
            ,myAttachmentData.Constants.SignedHeaders    // accept;content-type;host;x-amz-date
            ,myAttachmentData.Constants.ContentType      // application/json
            ,myAttachmentData.Constants.AcceptedValues   // application/octet-stream,application/json
           ,myAttachmentData.AWSKey
           ,myAttachmentData.AWSSecret
          , LocalFilename
          , myAttachmentData.AttachmentID
          , AWSFILENAME
          , ERRORMSG
          )

      then
        MyAttachmentData.AWSRetMessage := 'AWS: Unable to retrieve attachment. ' + #13#10 +
                               ' - File Name: ' + myAttachmentData.OrigFileName  + #13#10 +
                               ' - Attachment ID#: ' + IntToStr(myAttachmentData.AttachmentID) + #13#10 + #13#10 +
                               ReformatMessage(ErrorMsg)


      else
        MyAttachmentData.AWSRetMessage := AWSSuccess;

  end;
  AttachmentData := myAttachmentData;
end;

function ReformatMessage(RetMessage: string): string;
const
  TermChar : array[1..4] of char = ('.', '!', '}', ';');
var
  i, placeholder, ErrStart, ErrStop : integer;
  TempStr, StatusCodeStr, StatusMsgStr, PlainMsg: string;
begin
   try
   {STATUSCODE}
     ErrStart := Ansipos('STATUSCODE', UPPERCASE(RetMessage));
     if ErrStart <> 0 then  begin
       ErrStart := ErrStart + 10;

       TempStr := Copy(RetMessage, ErrStart, length(RetMessage)-1);

       {EB - Find termination for Status Code}
       ErrStop := 0;
       placeholder := AnsiPos(',', TempStr);
         if placeholder > 0 then
           ErrStop := placeholder-1;

       for i := 1 to 4 do begin
         placeholder := Ansipos(TermChar[i], TempStr);
         if (((placeholder < ErrStop) and (placeholder <> 0)) or (ErrStop = 0)) then
           ErrStop := placeholder;
       end;

       if ErrStop > 0 then begin
         StatusCodeStr := Copy(TempStr, 0, ErrStop);
         StatusCodeStr := RemoveCharsFromStr(StatusCodeStr, ['"', '''','{', '}', ':']);
         StatusCodeStr := CompressWhiteSpace(StatusCodeStr);
         StatusCodeStr := 'Status Code: ' + StatusCodeStr;
       end;
     end
     else
       StatusCodeStr := '';


   {STATUSMESSAGE}
     ErrStart := Ansipos('STATUSMESSAGE', UPPERCASE(RetMessage));
     if ErrStart <> 0 then begin
       ErrStart := ErrStart + 13;
       TempStr := Copy(RetMessage, ErrStart, length(RetMessage)-1);

       {EB - Find termination for Status Message}
       ErrStop := 0;
       for i := 1 to 4 do begin
         placeholder := Ansipos(TermChar[i], TempStr);
         if (((placeholder < ErrStop) and (placeholder <> 0)) or (ErrStop = 0)) then
           ErrStop := placeholder;
       end;

       if ErrStop > 0 then begin
         StatusMsgStr := Copy(TempStr, 0, ErrStop);
         StatusMsgStr := RemoveCharsFromStr(StatusMsgStr, ['"', '''','{', '}', ':']);
         StatusMsgStr := CompressWhiteSpace(StatusMsgStr);
         StatusMsgStr := 'Status Message: ' + StatusMsgStr;
       end;
     end
     else  StatusMsgStr := '';


   {MESSAGE - Generic}
     ErrStop := 0;
     ErrStart := Ansipos('MESSAGE', UPPERCASE(RetMessage));
     if (ErrStart <> 0) then begin
       ErrStart := ErrStart + 7;
       TempStr := Copy(RetMessage, ErrStart, length(RetMessage)-1);

       {EB - Find termination for Message}
       for i := 1 to 4 do begin
         placeholder := Ansipos(TermChar[i], TempStr);
         if (((placeholder < ErrStop) and (placeholder <> 0)) or (ErrStop = 0)) then
           ErrStop := placeholder;
       end;

       if (ErrStop = 0) then
         ErrStop := length(RetMessage)-1;  {EB - Lets just default to the end}

         PlainMsg := Copy(TempStr, 0, ErrStop);
         PlainMsg := RemoveCharsFromStr(PlainMsg, ['"', '''','{', '}', ':']);
         PlainMsg := CompressWhiteSpace(PlainMsg);
         PlainMsg := 'Message: ' + PlainMsg;
     end
     else  PlainMsg := '';

     if PlainMsg = '' then begin
       PlainMsg := RetMessage;
       PlainMsg := RemoveCharsFromStr(PlainMsg, ['"', '''','{', '}']);
       PlainMsg := CompressWhiteSpace(PlainMsg);
       PlainMsg := 'Message: ' + PlainMsg;
     end;

   finally

   end;
   Result := StatusCodeStr;
   if  (Result <> '') then
     If (StatusMsgStr <> '') then
       Result := Result + #13#10 + StatusMsgStr;

   if Result = '' then
     Result := PlainMsg;
end;

function CreateFTPDownloader(Config: TFTPDownloadConfig): TIdFTP;
var
  MajorPhase: string;
  Phase: string;
begin
  try
    MajorPhase := 'Server';
    Phase := 'TIdFTP.Create(nil)';
    Result := TIdFTP.Create(nil);
    Phase := 'Config.Host';
    Result.Host := Config.Host;
    Phase := 'Config.Port';
    Result.Port := FTPPortAsWord(Config.Port);
    Phase := 'Config.User';
    Result.Username := Config.User;
    Phase := 'Config.Password';
    Result.Password := Config.Password;
    Phase := 'TransferType ftBinary';
    Result.TransferType := ftBinary;
    // Indy 10 is required
    if Config.TimeOut < 1 then begin
      Phase := 'ConnectTimeout := 10000';
      Result.ConnectTimeout := 10000;
    end
    else begin
      Phase := 'ConnectTimeout := Config.TimeOut: ' + IntToStr(Config.TimeOut);
      Result.ConnectTimeout := Config.TimeOut;
    end;
    Phase := 'Result.AutoLogin';
    Result.AutoLogin := True;
    Phase := 'Result.Passive := True';
    Result.Passive := True;
    Phase := 'Result.Connect';
    Result.Connect;
    Phase := 'Trim(Config.RootDir)';
    if Trim(Config.RootDir) <> '' then begin
      MajorPhase := 'Folder ' + Config.RootDir;
      Phase := 'ChangeDir(Config.RootDir): ' + Config.RootDir;
      ChangeToFTPRootFolder(Result, Config.RootDir);
    end;
    Phase := 'CreateFTPDownloader Done';
  except
    on E: Exception do begin
       E.Message := format('Problem connecting to FTP %s. %s in %s  [Server: %s, Username: %s]',
        [MajorPhase, E.Message, Phase, Config.Host, Config.User]);
      raise;
    end;
  end;
end;

function FileExistsOnServer(FTP: TIdFTP; FileName: string): Boolean;
var
  List: TStringList;
begin
  Assert(Assigned(FTP));
  List := TStringList.Create;
  try
    FTP.List(List, '', False);
    Result := List.IndexOf(FileName) <> -1;
  finally
    FreeAndNil(List);
  end;
end;

function CheckFileSizeOnFTPServer(FTP: TIdFTP; FileSize: Integer; AttachmentData: TAttachmentData; CurrDirIsAttachmentFolder: Boolean): TFileComparison;
var
  Phase: string;
  ServerFileSize: Integer;
begin
  Phase := '';
  try
    if not CurrDirIsAttachmentFolder then
      ChangeToAttachmentFolder(FTP, AttachmentData);
    Phase := 'FileExistsOnServer ' + AttachmentData.ServerFileName;
    if not FileExistsOnServer(FTP, AttachmentData.ServerFileName) then
      Result := fcNoFileOnServer
    else begin
      Phase := 'FTP.Size ' + AttachmentData.ServerFileName;
      ServerFileSize := FTP.Size(AttachmentData.ServerFileName);
      if ServerFileSize = FileSize then
        Result := fcFilesHaveSameSize
      else if ServerFileSize < FileSize then
        Result := fcFileOnServerIsSmaller
      else
        Result := fcFileOnServerIsBigger;
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;
end;

function CheckFileHashOnFTPServer(FTP: TIdFTP; AttachmentData: TAttachmentData): Boolean;
var
  Phase: string;
  Dest: string;
begin
  Result := True;
  if IsEmpty(AttachmentData.FileHash) then
    Exit;

  Dest := GetWindowsTempFileName(False, '', 'QML-');
  try
    ChangeToAttachmentFolder(FTP, AttachmentData);
    Phase := 'FileExistsOnServer ' + AttachmentData.ServerFileName;
    if FileExistsOnServer(FTP, AttachmentData.ServerFileName) then begin
      Phase := 'FTP.Get file';
      FTP.Get(AttachmentData.ServerFilename, Dest, True, False);
      Phase := 'FTP.Hash file';
      Result := (HashFile(Dest) = AttachmentData.FileHash);
      Phase := 'FTP.Delete temp file';
      DeleteFile(Dest);
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;
end;

function CurrentDirIsAttachmentFolder(FTP: TIdFTP; FTPRootDir: string; AttachmentData: TAttachmentData): Boolean;
var
  ExpectedDir: string;
  CurrentDir: string;
begin
  Result := False;
  CurrentDir := FTP.RetrieveCurrentDir;
  ExpectedDir := FTPRootDir + '/' + AttachmentData.ForeignTypeDir + '/' +
    AttachmentData.YearDir + '/' + AttachmentData.DateDir + '/' + AttachmentData.IDDir;
  if StrBeginsWith('//', ExpectedDir) then
    Delete(ExpectedDir, 1, 1);

  if SameText(CurrentDir, ExpectedDir) then begin
    Result := True;
    Exit;
  end;

  ExpectedDir := FTPRootDir + '\' + AttachmentData.ForeignTypeDir + '\' +
    AttachmentData.YearDir + '\' + AttachmentData.DateDir + '\' + AttachmentData.IDDir;
    if StrBeginsWith('\\', ExpectedDir) then
      Delete(ExpectedDir, 1, 1);
  if SameText(CurrentDir, ExpectedDir) then
    Result := True;
end;

procedure ChangeToAttachmentFolder(FTP: TIdFTP; AttachmentData: TAttachmentData);
var
  Phase: string;
begin
  try
    Phase := 'FTP.ChangeDir ' + AttachmentData.ForeignTypeDir;
    FTP.ChangeDir(AttachmentData.ForeignTypeDir);
    Phase := 'FTP.ChangeDir ' + AttachmentData.YearDir;
    FTP.ChangeDir(AttachmentData.YearDir);
    Phase := 'FileExistsOnServer ' + AttachmentData.DateDir;
    if FileExistsOnServer(FTP, AttachmentData.DateDir) then begin
      Phase := 'FTP.ChangeDir DateDir ' + AttachmentData.DateDir;
      FTP.ChangeDir(AttachmentData.DateDir);
      Phase := 'FileExistsOnServer ' + AttachmentData.IDDir;
      if FileExistsOnServer(FTP, AttachmentData.IDDir) then begin
        Phase := 'FTP.ChangeDir IDDir ' + AttachmentData.IDDir;
        FTP.ChangeDir(AttachmentData.IDDir);
      end;
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;
end;

procedure ChangeToFTPRootFolder(FTP: TIdFTP; FTPRootDir: String);
var
  Phase: string;
begin
  try
    Phase := 'FTP.ChangeDir ' + FTPRootDir;
    if NotEmpty(FTPRootDir) then begin
      if not (copy(FTPRootDir,1,1) = '/') then
        FTP.ChangeDir('/' + FTPRootDir)
      else
        FTP.ChangeDir(FTPRootDir);
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;
end;


{ TLogger }

constructor TLogger.create(ABaseAttachment: TBaseAttachment);
begin
  inherited create;
  FBaseAttachment := ABaseAttachment;
end;

destructor TLogger.destroy;
begin

  inherited;
end;

function TLogger.DoLog(APriority: TLogPriorityIntf; const ALogMsg,
  AMsg: widestring): widestring;
begin
  FBaseAttachment.Log(ALogMsg)
end;

initialization
  uDLLIntf.SetLog(nil)

finalization
  uDLLIntf.Finalize

end.
