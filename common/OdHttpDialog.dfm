object OdNetProgressDialog: TOdNetProgressDialog
  Left = 242
  Top = 107
  ActiveControl = CancelButton
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'File Transfer'
  ClientHeight = 172
  ClientWidth = 491
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnHide = FormHide
  DesignSize = (
    491
    172)
  PixelsPerInch = 96
  TextHeight = 13
  object URL: TLabel
    Left = 9
    Top = 9
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL:'
  end
  object Transfer: TLabel
    Left = 9
    Top = 47
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transfered:'
  end
  object TransferLabel: TLabel
    Left = 68
    Top = 47
    Width = 163
    Height = 13
    Caption = '56 KB of 335 KB at 3.5 KB/sec'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Status: TLabel
    Left = 9
    Top = 28
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object StatusLabel: TLabel
    Left = 68
    Top = 28
    Width = 101
    Height = 13
    Caption = 'Downloading data'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ElapsedLabel: TLabel
    Left = 68
    Top = 66
    Width = 64
    Height = 13
    Caption = '42 Seconds'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Elapsed: TLabel
    Left = 9
    Top = 66
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Elapsed:'
  end
  object URLEdit: TEdit
    Left = 68
    Top = 10
    Width = 415
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    BorderStyle = bsNone
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Text = 'http://server.domain.com/template/locator_sync_3.xml?Param=225'
  end
  object Animation: TAnimate
    Left = 58
    Top = 79
    Width = 272
    Height = 60
    Active = True
    CommonAVI = aviCopyFiles
    StopFrame = 34
  end
  object ProgressPanel: TPanel
    Left = 2
    Top = 83
    Width = 384
    Height = 41
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object RemainingLabel: TLabel
      Left = 66
      Top = 0
      Width = 56
      Height = 13
      Caption = '4 minutes'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Remaining: TLabel
      Left = 7
      Top = 0
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Remaining:'
    end
    object ProgressLabel: TLabel
      Left = 7
      Top = 18
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Progress:'
    end
    object Progress: TProgressBar
      Left = 66
      Top = 17
      Width = 302
      Height = 18
      TabOrder = 0
    end
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 139
    Width = 491
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object CancelButton: TButton
      Left = 159
      Top = 1
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      TabOrder = 0
      OnClick = CancelButtonClick
    end
  end
  object ShowTimer: TTimer
    Enabled = False
    OnTimer = ShowTimerTimer
    Left = 352
    Top = 32
  end
  object UpdateTimer: TTimer
    Enabled = False
    OnTimer = UpdateTimerTimer
    Left = 316
    Top = 32
  end
  object StalledTimer: TTimer
    Enabled = False
    Interval = 4000
    OnTimer = StalledTimerTimer
    Left = 280
    Top = 32
  end
end
