unit OdShellUtils;

interface

{$WARN UNIT_PLATFORM OFF}

uses
  Messages, Classes, ShellCtrls;

type
  TOdShellListView = class(TShellListView)
  private
    function GetPath: string;
    procedure SetPath(const Value: string);
  protected
    procedure WMDROPFILES(var Message: TWMDropFiles); message WM_DROPFILES;
  published
  public
    RequestedFolder: string;
    DragAndDropEnabled: Boolean;
    constructor Create(AOwner: TComponent); override;
    property Path: string read GetPath write SetPath;
  end;

implementation

uses
  ShellAPI, SysUtils, OdMiscUtils, JclShell, ShlObj, ComObj;

{ TOdShellListView }

constructor TOdShellListView.Create(AOwner: TComponent);
begin
  inherited;
  DragAndDropEnabled := True;
end;

function TOdShellListView.GetPath: string;
begin
  RootFolder.PathName;
end;

procedure TOdShellListView.SetPath(const Value: string);
begin
  if Value = GetPath then
    Exit;
  // This is a workaround for a SameText check in SetRoot that prevents setting back to the original dir
  if StrEndsWith(PathDelim, Root) then
    Root := ExcludeTrailingPathDelimiter(Value)
  else
    Root := IncludeTrailingPathDelimiter(Value);
end;

procedure TOdShellListView.WMDROPFILES(var Message: TWMDropFiles);
var
  i, NoOfItems, Size: Integer;
  DragFile : PChar;
  TargetFolder: string;
begin
  inherited;
  if not DragAndDropEnabled then
    Exit;
  DragFile := StrAlloc(512 + 1);
  // Get and validate target folder
  if NotEmpty(RequestedFolder) then begin
    TargetFolder := RequestedFolder;
    ForceDirectories(TargetFolder);
    Root := TargetFolder;
    AutoRefresh := True;
    RequestedFolder := '';
  end
  else
    TargetFolder := Self.RootFolder.PathName;
  if StringInArray(TargetFolder, ['Control Panel', 'Recycle Bin']) or StrContains(TargetFolder, 'nethood') or IsEmpty(TargetFolder) then
    Exit; // We don't support any special folders yet
  // Get the number of dropped items
  NoOfItems := DragQueryFile(Message.Drop, $FFFFFFFF, DragFile, 255);
  for i := 0 to (NoOfItems - 1) do begin
    // Get the size of the item's full path and allocate the memory
    Size := DragQueryFile(Message.Drop, i , nil, 0) + 1;
    DragFile := StrAlloc(Size);
    // Get the full path of the item
    DragQueryFile(Message.Drop, i, DragFile, Size);
    // Copy source file/folder to target folder
    if not SHCopy(Self.Handle, DragFile, TargetFolder, [coFilesOnly, coNoConfirmation]) then
      raise Exception.CreateFmt('File copy of %s to %s failed.', [DragFile, TargetFolder]);
    StrDispose(DragFile);
  end;
  DragFinish(Message.Drop);
  Update;
  Refresh;
end;

end.
