program DoUpdate;

{$APPTYPE CONSOLE}
{$WARN SYMBOL_PLATFORM OFF}

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  Classes,
  Windows,
  ShellAPI,
  IniFiles,
  OdWebUpdateShared,
  OdIsoDates,
  OdMiscUtils in 'OdMiscUtils.pas';

{$R DoUpdateManifest.res} // This disables Windows installer detection in Vista and later for "update" EXEs

var
  IniFile: TIniFile;

procedure Print(const Msg: string);
begin
  Writeln(Output, Msg);
end;

procedure LoadIniFile;
var
  FileName: string;
begin
  if ParamCount < 1 then
    raise Exception.Create('This program should be used for automated updates only');
  FileName := ParamStr(1);
  Assert(Length(FileName) > 0, 'Bad FileName');
  if (FileName[1] = '"') and (FileName[Length(FileName)] = '"') then
    FileName := Copy(FileName, 2, Length(FileName) - 2);
  if not FileExists(FileName) then
    raise Exception.Create('The specified update ini file does not exist: ' + FileName);
  IniFile := TIniFile.Create(FileName);
end;

procedure RemoveReadOnlyAttribute(const FileName: string);
var
  Attributes: Integer;
begin
  Attributes := FileGetAttr(FileName);
  Assert(Attributes <> -1);
  Attributes := FileSetAttr(FileName, Attributes and not faReadOnly);
  if Attributes = -1 then
    raise Exception.Create('Unable get access to file: ' + FileName);
end;

function UserCanDeleteFile(const FileName: string): Boolean;
var
  DestDir: string;
begin
  Result := False;
  DestDir := ExtractFileDir(FileName);
  if FileExists(FileName) then
  begin
    try
      RemoveReadOnlyAttribute(FileName);
      Result := True;
    except
    end;
  end
  else begin
    if Win32Platform <> VER_PLATFORM_WIN32_NT then
      Result := True
    else begin
      // Query DestDir for NT security attributes here?
      Result := True;
    end;
  end;
end;

procedure VerifyIniFile;
var
  AppMutex: string;
  NumberOfFiles: Integer;
  i: Integer;
  FileSection: string;
  FileName: string;
  DestDir: string;
  FileVersion: Double;
  UpdateDir: string;
  MainApp: string;
  IsoDate: string;
begin
  if not IniFile.SectionExists(ConfigSection) then
    raise Exception.Create(IniFile.FileName + ' is not a web update ini file');
  MainApp := IniFile.ReadString(ConfigSection, MainApplicationIdent, '');
  if not FileExists(MainApp) then
    raise Exception.Create('The main application does not exist: ' + MainApp);
  FileVersion := IniFile.ReadFloat(ConfigSection, FileFormatVersionIdent, -1);
  if not Trunc(FileVersion) = Trunc(SupportedIniFileFormatVersion) then
    raise Exception.Create(IniFile.FileName + ' is not in the correct file format version');
  NumberOfFiles := IniFile.ReadInteger(ConfigSection, NumberOfFilesIdent, -1);
  if NumberOfFiles < 0 then
    raise Exception.Create('Invalid number of files specified: ' + IntToStr(NumberOfFiles));
  AppMutex := IniFile.ReadString(ConfigSection, MainApplicationMutexIdent, '');
  Assert(AppMutex <> '', 'No application mutex found');
  UpdateDir := IncludeTrailingPathDelimiter(ExtractFilePath(IniFile.FileName));
  for i := 0 to NumberOfFiles - 1 do
  begin
    FileSection := FileDetailsPrefix + IntToStr(i);
    Assert(IniFile.SectionExists(FileSection), 'File section missing: ' + FileSection);
    FileName := IniFile.ReadString(FileSection, FileNameIdent, '');
    Assert(FileName <> '', 'Missing file name in ' + FileSection);
    DestDir := IniFile.ReadString(FileSection, FileLocalDirectoryIdent, '');
    Assert(DestDir <> '', 'Missing local directory in' + FileSection);
    if not FileExists(UpdateDir + FileName) then
      raise Exception.Create('An update file is missing: ' + FileName);
    IsoDate :=Trim(IniFile.ReadString(FileSection, FileDateTimeIdent, ''));
    if IsoDate <> '' then
      if not IsoIsValidDateTime(IsoDate) then
        raise Exception.Create('Invalid ISO date in: ' + FileSection);
    if not UserCanDeleteFile(IncludeTrailingPathDelimiter(DestDir) + FileName) then
      raise Exception.Create('You do not have write access to: ' + IncludeTrailingPathDelimiter(DestDir) + FileName);
  end;
end;

procedure WaitForAppClose;
var
  MutexHandle: THandle;
  AppMutex: string;
  Iterations: Integer;
begin
  Iterations := 0;
  AppMutex := IniFile.ReadString(ConfigSection, MainApplicationMutexIdent, '');
  repeat
    MutexHandle := OpenMutex(MUTEX_ALL_ACCESS, False, PChar(AppMutex));
    if MutexHandle = 0 then
      Break
    else
      CloseHandle(MutexHandle);
    Sleep(500);
    Inc(Iterations);
  until Iterations > 200;
  if Iterations > 200 then
    raise Exception.Create('The main application mutex still exists, aborting update');
end;

procedure PerformUpdate;
var
  NumberOfFiles: Integer;
  i: Integer;
  UpdateDir: string;
  SourceFile: string;
  DestDir: string;
  DestFile: string;
  FileName: string;
  Section: string;
  Error: string;
  IsoFileDate: string;
  FileDate: TDateTime;
begin
  NumberOfFiles := IniFile.ReadInteger(ConfigSection, NumberOfFilesIdent, 0);
  UpdateDir := IncludeTrailingPathDelimiter(ExtractFilePath(IniFile.FileName));
  for i := 0 to NumberOfFiles - 1 do
  begin
    Section := FileDetailsPrefix + IntToStr(i);
    FileName := IniFile.ReadString(Section, FileNameIdent, '');
    SourceFile := UpdateDir + FileName;
    DestDir := IncludeTrailingPathDelimiter(IniFile.ReadString(Section, FileLocalDirectoryIdent, ''));
    DestFile := DestDir + FileName;
    if FileExists(DestFile) then
      RemoveReadOnlyAttribute(DestFile);
    if not DirectoryExists(DestDir) then
      if not ForceDirectories(DestDir) then
        raise Exception.Create('Unable to create directory: ' + DestDir);
    if CopyFile(PChar(SourceFile), PChar(DestFile), False) then
    begin
      Print(Format('Copied %s to %s', [SourceFile, DestFile]));
      IsoFileDate := Trim(IniFile.ReadString(Section, FileDateTimeIdent, ''));
      if IsoFileDate <> '' then
      begin
        FileDate := IsoStrToDateTime(IsoFileDate);
        if FileSetDate(DestFile, DateTimeToFileDate(FileDate)) <> 0 then
          raise Exception.Create('Unable to set file date for: ' + DestFile);
      end;
      if not SysUtils.DeleteFile(SourceFile) then
        Print('Unable to delete file: ' + SourceFile);
    end
    else begin
      Error := Format('Unable to copy %s to %s.' +#13#10+ 'Reason: %s',
        [SourceFile, DestFile, SysErrorMessage(GetLastError)]);
      raise Exception.Create(Error);
    end;
  end;
end;

procedure RestartApplication;
var
  MainApp: string;
begin
  MainApp := IniFile.ReadString(ConfigSection, MainApplicationIdent, '');
  ShellExecute(0, 'open', PChar(MainApp), '-'+AfterUpdateParameter, nil, SW_SHOW);
end;

begin
  try
    Print('Beginning auto-update process');
    Sleep(500);
    Print('Loading update .ini file');
    LoadIniFile;
    Print('Verifying update .ini file');
    VerifyIniFile;
    Print('Waiting for the main application to close');
    WaitForAppClose;
    Print('Main application has closed, beginning file copy process');
    PerformUpdate;
    MessageBox(0, 'Update successful.  Press OK to restart the application.',
       'Update Succesful', MB_ICONINFORMATION or MB_OK);
    RestartApplication;
  except on E: Exception do
    begin
      Print('Error: ' + E.Message);
      MessageBox(0, PChar('Update failed.  Please contact your system administrator.' + sLineBreak
        + 'Reason: ' +E.Message), 'Update Failed', MB_ICONERROR or MB_OK);
      Abort;
    end;
  end;
end.
