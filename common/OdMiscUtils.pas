unit OdMiscUtils;

{$WARN SYMBOL_PLATFORM OFF}

{$IF CompilerVersion >= 16} // Delphi 8, 2005 or greater
  {$DEFINE ClassHelpers}
{$IFEND}

interface

uses
  // Try to keep VCL units out of this list.  Add those routines to OdVclUtils.
  SysUtils, Classes, Windows, Types,
  {$IFDEF UNICODE}
  System.Character, System.UITypes, System.IOUtils,
  {$ENDIF}
  ShellAPI, TLHelp32, psAPI,
  HTTPApp;    {QM-1020 EB Fix Mesasge links}


{$MINENUMSIZE 4}     //qm-164
const
  IOCTL_STORAGE_QUERY_PROPERTY =  $002D1400;      //qm-164
  SELDIRHELP = 1000;

const
  CRLF = sLineBreak;
  CR   = #13;
  LF   = #10;
  // This is the default code page for Windows Latin-1 8-bit "ANSI" characters.
  // For Tiburon, we can optionally change to to UTF-8/16
  DefaultXMLEncoding = 'windows-1252';   
  {$I ..\version.inc}
  SingleQuote = '''';

const
  A_Day    = 1.0;
  A_Hour   = A_Day / 24;
  A_Minute = A_Hour / 60;
  A_Second = A_Minute / 60;
  A_Week   = A_Day * 7;

type
  STORAGE_QUERY_TYPE = (PropertyStandardQuery = 0, PropertyExistsQuery, PropertyMaskQuery, PropertyQueryMaxDefined);     //qm-164
  TStorageQueryType = STORAGE_QUERY_TYPE;

  STORAGE_PROPERTY_ID = (StorageDeviceProperty = 0, StorageAdapterProperty);     //qm-164
  TStoragePropertyID = STORAGE_PROPERTY_ID;

  STORAGE_PROPERTY_QUERY = packed record     //qm-164
    PropertyId: STORAGE_PROPERTY_ID;
    QueryType: STORAGE_QUERY_TYPE;
    AdditionalParameters: array [0..9] of AnsiChar;
  end;
  TStoragePropertyQuery = STORAGE_PROPERTY_QUERY;

  STORAGE_BUS_TYPE = (BusTypeUnknown = 0, BusTypeScsi, BusTypeAtapi, BusTypeAta, BusType1394, BusTypeSsa, BusTypeFibre,     //qm-164
    BusTypeUsb, BusTypeRAID, BusTypeiScsi, BusTypeSas, BusTypeSata, BusTypeMaxReserved = $7F);
  TStorageBusType = STORAGE_BUS_TYPE;

  STORAGE_DEVICE_DESCRIPTOR = packed record     //qm-164
    Version: DWORD;
    Size: DWORD;
    DeviceType: Byte;
    DeviceTypeModifier: Byte;
    RemovableMedia: Boolean;
    CommandQueueing: Boolean;
    VendorIdOffset: DWORD;
    ProductIdOffset: DWORD;
    ProductRevisionOffset: DWORD;
    SerialNumberOffset: DWORD;
    BusType: STORAGE_BUS_TYPE;
    RawPropertiesLength: DWORD;
    RawDeviceProperties: array [0..0] of AnsiChar;
  end;
  TStorageDeviceDescriptor = STORAGE_DEVICE_DESCRIPTOR;

  {EB - way to track the process and window when opened}
  TWndInfo = record
    TagName: string;
    AppPath: string;
    ProcessId: DWord;
    WinHandle: HWND;
    width, height: Integer;
  end;
  PWndInfo = ^TWndInfo;

{$IFDEF UNICODE}
  TQMEncodingType = (etUTF8, etANSI);
{$ENDIF}

  TLogEvent = procedure(Sender: TObject; const Msg: string) of object;

  TStringArray = array of string;
  TIntegerArray = array of Integer;

  TVersionNumber = packed record
    case Boolean of
      True:  ( dwFileVersionMS: DWORD;    { e.g. $00030075 = "3.75" }
               dwFileVersionLS: DWORD;    { e.g. $00000031 = "0.31" }
              );
      False: ( Minor: Word;
               Major: Word;
               Build: Word;
               Release: Word;
              );
    end;

  TOdStringList = class(TStringList)
  public
    procedure Union(const StringsToAdd: TStringArray); overload;
    procedure Union(const StringsToAdd: TStringList); overload;
    procedure Intersect(const StringsToMatch: TStringArray); overload;
    procedure Intersect(const StringsToMatch: TStringList); overload;
    procedure SymmetricDifference(const StringsToDifference: TStringArray); overload;
    procedure SymmetricDifference(const StringsToDifference: TStringList); overload;
  end;

{$IFDEF ClassHelpers}
  TStringsHelper = class helper for TStrings
    function AddFmt(const Str: string; Args: array of const): Integer;
    procedure FreeAndNilObjects;
    function Contains(const Str: string): Boolean;
    function ContainsSubstringText(Substrings: array of string): Boolean;
    function AddInteger(const S: string; AInt: Integer): Integer;
    function GetInteger(Index: Integer): Integer;
    function DelimitedLines(const Delimiter: string): string;
    function AddStringCounted(const Str: string): Integer;
    procedure SaveToFileCounted(const FileName: string);
    function Pop(Num: Integer): string;
  end;

  TStreamHelper = class helper for TStream
  public
    function AsAnsiString: AnsiString;
    procedure WriteAnsiString(const Str: AnsiString);
    function AsString: string;
    procedure WriteString(const Str: string);
    procedure WriteUTF8String(const Str: string);
    procedure WriteStringBytes(const Str: string{$IFDEF UNICODE};QMEncodingType: TQMEncodingType=etAnsi{$ENDIF});
  end;

  TRectHelper = record helper for TRect
  public
    function Width: Integer;
    function Height: Integer;
  end;

  // ANSI Text file helper functions
  TFileStreamHelper = class helper for TFileStream
  public
    function Eof: Boolean;
    function ReadLine: AnsiString;
    procedure WriteBytes(sBytes: TBytes{$IFDEF UNICODE};QMEncodingType: TQMEncodingType=etAnsi{$ENDIF});
  end;


{$ENDIF ClassHelpers}

function ParseMonth(S: string): TDateTime;
function AddSIfNot1(Val: Integer; Str: string): string;
function SecondsToTime(Seconds: Cardinal; IncludeSeconds: Boolean = True): string;
function SecondsToHMS(Seconds: Cardinal): string;
function SecondsToHMMSS(Seconds: Cardinal): string;
function SecondsToHM(Seconds: Cardinal; const ShortFormat: Boolean = False): string;
function CurrentYDMHMS: TDateTime;
function GetFileSize(const FileName: string): Integer;
function GetFileDate(const FileName: string): TDateTime;
procedure CreateEmptyFile(const FileName: string);
function BytesToFileSize(Size: Cardinal): string;
procedure OdForceDirectories(Dir: string);
function CanWriteToDirectory(const Dir: string): Boolean;
function CanCreateFile(const FileName: string): Boolean;
function OdShellExecute(const FileName: string; const Parameters: string = '';
  const RaiseException: Boolean = True; ShowCmd: Integer = SW_SHOWNORMAL): Boolean;
function OdShellEx(const FileName: string; FailMessage: string): Boolean;
function BrowseURL(const URL: string) : Boolean;
function BrowseURLSpecified(const URL: string; pExe: string ='chrome.exe'): Boolean;    //QM-1008 & QM-1014 EB HTML fix
function BooleanToString01(B: Boolean): string;
function BooleanToStringYesNo(B: Boolean): string;
function GetFileVersionNumber(const FileName: string; var Version: TVersionNumber): Boolean;
function GetFileVersionNumberString(const FileName: string; var VersionString: string): Boolean;
function CompareVersionNumber(const V1, V2: TVersionNumber): Integer;
function ParseVersionString(const VersionStr: string): TVersionNumber;
function GetFileCRC32(const FileName: string; var CRC32: Cardinal): Boolean;
function GetScreenBitsPerPixel: Integer;
function AnyStringsAreBlank(Values: TStringList): Boolean;
procedure ConcatStringLists(Dest, Source: TStrings);
function StringListsMatch(Str1, Str2: TStrings): Boolean;
function StringListIndexOfWithPrefix(List: TStrings; const Prefix, Text: string): Integer;
function AllObjectsAreAssigned(Values: TStrings; StartFrom: Integer = 0): Boolean;
function AddIntToArray(Value: Integer; var IArray: TIntegerArray): Integer;
function IntInArray(Value: Integer; const IArray: TIntegerArray): Boolean; overload;
function IntInArray(Value: Integer; const IArray: array of Integer): Boolean; overload;
function StringInArray(const S: string; const SArray: array of string): Boolean;
function HaveMatchingArrayMember(const SArray1, SArray2: array of string): Boolean;
function SubStringInArray(const S: string; const SArray: array of string): Boolean;
function StringArrayToDelimitedString(SArray: array of string; const QuoteChar, Delimiter: string): string;
function DelimitedStringToStringArray(DelimitedString:string; Delimiter: string = ','): TStringArray;
function StringToDelimitedString(Values: string; const QuoteChar: string = ''''; const Delimiter: string = ','): string;
function GetStringBeforeCharacter(const Str: string; StopChar: Char): string;
function IsTextInArray(const S: string; const SArray: array of string): Boolean;
function IntegerInArray(Int: Integer; const IArray: array of Integer): Boolean;
function ReplaceIntInArray(var IArray: array of Integer; Find, Replace: Integer): Integer;
function StrReplaceChars(const Str: string; Chars: TSysCharSet; ReplaceWith: Char): string;
{$IFNDEF UNICODE}
function CharInSet(C: Char; CSet: TSysCharSet): Boolean;
{$ENDIF}
function GetLastOsErrorMessage(Error: Integer): string;
procedure RaiseOSError(const Msg: string);
procedure RaiseOsExceptionIfNil(Value: Pointer; const Msg: string = '');
procedure RaiseOsExceptionIfFalse(Value: Boolean; const Msg: string = '');
procedure ShowExceptionError(E: Exception);
procedure ErrorDialog(const Msg: string);
function YesNoDialog(const Msg: string): Boolean;
function YesNoDialogFmt(const Msg: string; Args: array of const): Boolean;
function AddSlash(const Path: string): string;
function RemoveSlash(const Str: string): string;
function IsPathAbsolute(const FileName: string): Boolean;
function GetWindowsTempPath: string;
function GetWindowsTempFileName(Create: Boolean = True; Path: string = ''; Prefix: string = ''): string;
function GetWindowsSystemError(ErrorCode: HRESULT): string;
function InheritsFromClass(ClassType: TClass; const ClassName: string): Boolean;
function XmlEncodeString(const aString: string): string;
function VarToInt(Value: Variant; Default: Integer = 0): Integer;
function VarToFloat(Value: Variant): Double;
function VarToString(Value: Variant; Default: string = ''): string;
function VarToDateTime(Value: Variant): TDateTime;
function VarToBoolean(Value: Variant; Default: Boolean = False): Boolean;
function IntToBoolean(Value: Integer):Boolean;
function IntKeyToString(Value: Integer): string;
function VarParamToStr(Value: Variant): string;
function FormatCurrency(Value: Currency; Decimals: Boolean = True): string;
function CompressWhiteSpace(const Str: string): string;
function LeadingWhiteSpace(const Str: string): string;
function IsWeekend(Date: TDateTime): Boolean;
function ThisSaturday: TDateTime;
function SaturdayIze(D: TDateTime): TDateTime;
function BeginningOfTheWeek(Date: TDateTime): TDateTime;
function EndingOfTheWeek(Date: TDateTime): TDateTime;
function DayIsAfterDay(Date1, Date2: TDateTime): Boolean;
function TimeRangeOverlap(Range1Start, Range1Finish, Range2Start, Range2Finish : TDateTime) : TDateTime;
function StrContains(const SubStr, S: string): Boolean;
function StrContainsText(const SubStr, S: string): Boolean;
function StrNotContain(const SubStr, S: string): Boolean;
function StrBeginsWith(const SubStr, S: string): Boolean;
function StrBeginsWithText(const SubStr, S: string): Boolean;
function StrEndsWith(const SubStr, S: string): Boolean;
function StrEndsWithText(const SubStr, S: string): Boolean;
function StrGetWord(const Str: string; WordIndex: Integer; Delimiter: Char; DoTrim: Boolean): string;
function StrConsistsOf(const S: string; Chars: TSysCharSet): Boolean;
function CharRangeString(StartChar, StopChar: Char): string;
function NotEmpty(const Str: string): Boolean;
function IsEmpty(const Str: string): Boolean;
function RemoveBlankLines(const Str: string): string;
function RemovePrefix(const Prefix, Str: string): string;
function RemoveSuffix(const Suffix, Str: string): string;
function ReplaceNonEnglishAsciiChars(const Str: string; ReplaceChar: Char = '?'): string;
function ReplaceControlChars(const Str: string; const ReplaceWith: Char = ' '): string;
function RemoveControlChars(const Str: string): string;
function NumToWords(Num: Longint): string;
function RoundUp(Value: Extended): Int64;
function RoundPositiveToMin(Value: Extended; Min: Integer): Integer;
function RoundToNearestMultiple(Qty, MultipleOf: Integer): Integer;
function GetUTCBias: Integer;
function WinExecAndWait32V2(const FileName: string; Visibility: Word = SW_SHOWNORMAL): DWORD;
function CreateCommaStringList(const S: string; DoTrim: Boolean = True): TStringList;
procedure DeleteStringFromList(List: TStrings; const Item: string);
procedure EnsureStringInList(List: TStrings; const Item: string);
function GetLineWithPrefix(List: TStrings; const Prefix: string): Integer;
function FloatEquals(A, B: Double; Tolerance: Double = 0.001): Boolean;
function FloatRound(Value: Double; DecPlaces: Cardinal): Double;
function RangesIntersect(Start1, Stop1, Start2, Stop2: Double): Boolean;
function RandomAlphaNumericString(StrLength: Integer): string;
function IsCharNumeric(C: Char): Boolean;
function RandomString(StrLength: Integer; const Characters: string): string;
function ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean; //SR
function ShellCopy(const Src, Dest: string; Flags: FILEOP_FLAGS; RaiseException: Boolean = False): Boolean;
function SHCopyFiles(const Src, Dest: string; Silent: Boolean = False; RaiseException: Boolean = False): Boolean;
function HTMLEscape(const Str: string): string;
function JSONEscape(const Str: string): string;
function DesktopDirectory: string;
function MakeLegalFileName(const FileName: string; ReplaceStr: string = '-'): string;
function InRange(Value, Min, Max: Integer): Boolean;
function GetLineCount(const Text: string): Integer;
function GetLine(const FullText: string; LineNo: Integer): string;
function SetLine(const FullText: string; LineNo: Integer; const NewLine: string): string;
function GetThisModulePath: string;
function GetResourceString(ResourceName: string): string;
function StringArrayToString(Values: array of string): string;
function HoursFormat(Hours: Double): string;
function MakeGUIDString: string;
procedure SaveDebugFile(const FileName, Data: string);
function SuperCipher(const S: string; const Key : string = '') : string;
function HighResolutionScreen: Boolean;
function ConnectToConsole(var ShouldFreeConsole: Boolean): Boolean;
procedure ConsoleWaitForEnterKey(TimeOutSecs, SleepIntervalMS: Cardinal);
procedure ClonePersistent(Source, Dest: TPersistent);
procedure RemoveStringFromList(const Remove: string; List: TStrings; DoTrim: Boolean = False);
procedure RemoveStringWithPrefixFromList(const Remove: string; List: TStrings; DoTrim: Boolean = False);
procedure ReplaceStringInList(const Old, New: string; List: TStrings; DoTrim: Boolean = False);
procedure ReplaceSubStringToEndOfLineInList(const Old, New: string; List: TStrings;  CaseSensitive: Boolean = True);
procedure FreeStringListObjects(List: TStrings);
function SafeGetStringListString(Strings: TStrings; Index: Integer): string;
function StringArraysHaveMatch(A1, A2: TStringArray): Boolean;
function BuildFileName(const Path, FileName: string): string;
function ExtractFileNameOnly(FullPathName: string): string;
function AppendTextToFilename(FullPathName, AppendText:string; KeepPath: Boolean=True; KeepExt: Boolean=True):string;
function MakePath(const Path, DirectoryName: string): string;
function GetHoursOnly(const DateTime: TDatetime): TDatetime;
function Increase30Minutes(const DateTime: TDatetime): TDatetime;
function Decrease30Minutes(const DateTime: TDatetime): TDatetime;
function GetFileDateTime(const FileName: string): TDateTime;
function IsFileInUse(FileName: String): Boolean;
procedure FreeAndNil2(var Obj:TObject; LogEvent: TLogEvent = nil);
function StringContainsAnyOfArray(const S: string; const A: array of string; var ElementMatched: Integer): Boolean;
function ForceForegroundWindow(hwnd: THandle): Boolean;
procedure SendMessageToApplication(ClassName, WindowName: string; Message: Cardinal);
function CheckCmdLineParameter(const Parameter: string): Boolean;
function GetCmdLineParameterIndex(const Parameter: string): Integer;
function GetCommandLineParams: string;
function IsWindowsVista: Boolean;
function ValidateIntegerCommaSeparatedList(const List: string): Boolean;
function IsInteger(const Number: string): Boolean;
procedure WriteStringToStream(Stream: TStream; const Str: string);
function GetDomainUserName: string;
function IsValidEmailAddress(const Email: string): Boolean;
procedure OdDeleteFile(const Path, FileSpec: string; const MinDaysOld: Integer = -1; const Recursive: Boolean = False; const Recycle: Boolean = False);
procedure DeleteDirectory(const DirectoryName: string);
function GetDiskSpace(DrvLtr: char): Int64;   //sr
function GetLastLine(MyFile: TFilename): string;   //sr
function GetVolumeName(DriveLetter: Char): string; //sr
function GetBusType(Drive: AnsiChar): TStorageBusType;     //qm-164
function GetLocalAppDataFolder: string;
function GetUserProfileFolder: string;
function SameHourAndMinute(Time1, Time2: TDateTime): Boolean;
function StringsToStringArray(const Strings: TStrings): TStringArray;
function LatLongDistanceKM(Lat1, Lon1, Lat2, Lon2: Extended): Extended;
function LatLongDistanceMeters(Lat1, Lon1, Lat2, Lon2: Real): Real;
function FileToOleVariant(Filename: string): OleVariant;
procedure OleVariantToFile(const OV: OleVariant; Filename: string);
function StreamToOleVariant(AStream: TStream): OleVariant;
function AppVersionShort: string;
function EscapeLineBreaks(const Str: string): string;

function IsLatin1Char(const C:Char): Boolean;
function IsAsciiChar(const C:Char): Boolean;
function IsControlChar(const C: Char): Boolean;
function IsInvalidAsciiChar(const C: Char): Boolean;
function IsInvalidExportChar(const C: Char): Boolean;
function RemoveCharsFromStr(const S: string; const Chars: array of Char): string;

function CleanRemarks(var Remarks: string): string;

{$IFDEF UNICODE}
function GetQMEncoding(QMEncodingType: TQMEncodingType): TEncoding;
{$ENDIF}
function GetDailyFileName(Directory: string; FileName:string; InstanceId: Integer = 0): string;
function StringToBytes(AString: string{$IFDEF UNICODE};QMEncodingType: TQMEncodingType=etAnsi{$ENDIF}): TBytes;
function UTCToSystemTime(UTC: TDateTime): TDateTime;


{EB - Windows Handle tracking hack}
function GetHWndByPID(const hPID: THandle): THandle; //QM-954 EB Waldo suppress multiple windows
function GetHWndByProgramName(const APName: string): THandle;   //QM-954 EB Waldo suppress multiple windows
function GetPIDByProgramName(const APName: string): THandle;    //QM-954 EB Waldo suppress multiple windows
function AppActivate(WindowHandle: HWND): boolean; overload;

const
  MoneyFormat = '$#,##0.00';
  MoneyFormatNoDecimals = '$#,##0.';

implementation

uses
  // Try to keep VCL units out of this list.  Add those routines to OdVclUtils.
  Messages, Forms, Dialogs, DateUtils, ConvUtils, StdConvs, Variants,
  StrUtils, TypInfo, Registry, ShlObj, ActiveX, Controls, ShFolder,
  JclMath, JclStrings, JclShell, JclSysInfo, JclFileUtils, Math;

const
  ATTACH_PARENT_PROCESS: DWORD = DWORD(-1);

function ParseMonth(S: string): TDateTime;
var
  P: Integer;
  Month, Year: Integer;
begin
  // example format:   1/99   05/01    etc.

  P := Pos('/', S);
  Month := StrToInt(Copy(S, 1, P-1));
  Year := StrToInt(Copy(S, P+1, 20));
  if Year < 1900 then
    Year := 2000 + Year;
  Result := EncodeDate(Year, Month, 1);
end;

function AddSIfNot1(Val: Integer; Str: string): string;
begin
  if Val <> 1 then
    Result := Str + 's'
  else
    Result := Str;
end;

function SecondsToTime(Seconds: Cardinal; IncludeSeconds: Boolean): string;
var
  Years, Months, Weeks, Days, Hours, Minutes: Integer;
begin
  Years :=   Trunc(Convert(Seconds, tuSeconds, tuYears));
  Months :=  Trunc(Convert(Seconds, tuSeconds, tuMonths));
  Weeks :=   Trunc(Convert(Seconds, tuSeconds, tuWeeks));
  Days :=    Trunc(Convert(Seconds, tuSeconds, tuDays));
  Hours :=   Trunc(Convert(Seconds, tuSeconds, tuHours));
  Minutes := Trunc(Convert(Seconds, tuSeconds, tuMinutes));

  if Years > 0 then
    Result := IntToStr(Years) +  AddSIfNot1(Years, ' Year')
  else if Months > 0 then
    Result := IntToStr(Months) + AddSIfNot1(Months, ' Month')
  else if Weeks > 0 then
    Result := IntToStr(Weeks) + AddSIfNot1(Weeks, ' Week')
  else if Days > 0 then
    Result := IntToStr(Days) + AddSIfNot1(Days, ' Day')
  else if Hours > 0 then
    Result := IntToStr(Hours) + AddSIfNot1(Hours, ' Hour')
  else if Minutes > 0 then
    Result := IntToStr(Minutes) + AddSIfNot1(Minutes, ' Minute')
  else begin
    if IncludeSeconds then
      Result := IntToStr(Seconds) + AddSIfNot1(Seconds, ' Second')
    else
      Result := '0 Minutes';
  end;
end;

// Return a string representing the (up to) two most significant
// hour/minute/second portions of the time
function SecondsToHMS(Seconds: Cardinal): string;
const
  Minute = 1 * 60;
  Hour = Minute * 60;
var
  Minutes: Cardinal;
  Hours: Cardinal;
begin
  Hours := 0;
  if Seconds >= Hour then
  begin
    Hours := Seconds div Hour;
    Seconds := Seconds mod Hour;
  end;
  Minutes := Seconds div Minute;
  Seconds := Seconds mod Minute;
  if Hours > 0 then
    Result := Format('%d %s %d %s', [Hours, AddSIfNot1(Hours, 'hour'), Minutes, AddSIfNot1(Minutes, 'minute')])
  else if Minutes > 0 then
    Result := Format('%d %s %d %s', [Minutes, AddSIfNot1(Minutes, 'minute'), Seconds, AddSIfNot1(Seconds, 'second')])
  else
    Result := Format('%d %s', [Seconds, AddSIfNot1(Seconds, 'second')]);
end;

{ Return a string representing the number of seconds as H:MM:SS if hours > 0, or
  MM:SS if hours = 0 }
function SecondsToHMMSS(Seconds: Cardinal): string;
const
  Minute = 60;
  Hour = Minute * 60;
var
  Minutes: Cardinal;
  Hours: Cardinal;
begin
  Hours := Seconds div Hour;
  Seconds := Seconds mod Hour;

  Minutes := Seconds div Minute;
  Seconds := Seconds mod Minute;

  if Hours > 0 then
    Result := Format('%d:%.2d:%.2d', [Hours, Minutes, Seconds])
  else
    Result := Format('%d:%.2d', [Minutes, Seconds]);
end;

function SecondsToHM(Seconds: Cardinal; const ShortFormat: Boolean): string;
const
  Minute = 1 * 60;
  Hour = Minute * 60;
var
  Minutes: Cardinal;
  Hours: Cardinal;
begin
  Hours := 0;
  if Seconds >= Hour then
  begin
    Hours := Seconds div Hour;
    Seconds := Seconds mod Hour;
  end;
  Minutes := Seconds div Minute;
  Seconds := Seconds mod Minute;
  // round up minutes if more than 29 seconds
  if Seconds > 29 then
    Inc(Minutes);

  if ShortFormat then
    Result := Format('%d:%2.2d', [Hours, Minutes])
  else if Hours > 0 then
    Result := Format('%d %s %d %s', [Hours, AddSIfNot1(Hours, 'hour'), Minutes, AddSIfNot1(Minutes, 'minute')])
  else if Minutes > 0 then
    Result := Format('%d %s', [Minutes, AddSIfNot1(Minutes, 'minute')])
  else
    Result := '-';
end;

function CurrentYDMHMS: TDateTime;
var
  Present: TDateTime;
  Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
  Present := Now;
  DecodeDate(Present, Year, Month, Day);
  DecodeTime(Present, Hour, Min, Sec, MSec);
  Result := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);
end;

function GetFileSize(const FileName: string): Integer;
var
  F: file of Byte;
  OldFileMode: Byte;
begin
  AssignFile(F, FileName);
  try
    OldFileMode := FileMode;
    FileMode := 0; // Read-only

    Reset(F);
    try
      Result := FileSize(F);
    finally
      CloseFile(F);
      FileMode := OldFileMode;
    end;
  except
    on E: Exception do
      Result := 0;
  end;
end;

{$IF CompilerVersion < 16} // Delphi 7 or lower do not have this FileAge function
function FileAge(const FileName: string; var Age: TDateTime): Boolean;
begin
  Age := SysUtils.FileAge(FileName);
  if Age = -1 then
    Result := False
  else
    Result := True;
end;
{$IFEND}

function GetFileDate(const FileName: string): TDateTime;
begin
  if not FileExists(FileName) then
    raise Exception.CreateFmt('File %s does not exist to get the modified date', [FileName]);
  if not FileAge(FileName, Result) then
    raise Exception.Create('Unable to get modified date for ' + FileName);
end;

procedure CreateEmptyFile(const FileName: string);
var
  LogFile: TFileStream;
begin
  if FileExists(FileName) then
    raise Exception.CreateFmt('The file %s already exists', [FileName]);
  LogFile := TFileStream.Create(FileName, fmCreate or fmShareDenyNone);
  FreeAndNil(LogFile);
end;

function BytesToFileSize(Size: Cardinal): string;
const
  KB = 1024;
  MB = 1024 * KB;
  GB = 1024 * MB;
begin
  if Size >= GB then
    Result := Format('%.2f',[Size/GB])+' GB'
  else if Size >= MB then
    Result := Format('%.2f',[Size/MB])+' MB'
  else if Size >= KB then
    Result := Format('%.2f',[Size/KB])+' KB'
  else
    Result := IntToStr(Size) + ' Bytes'
end;

procedure OdForceDirectories(Dir: string);
begin
  if not ForceDirectories(Dir) then
    raise Exception.CreateFmt('Unable to create directory %s as user %s', [Dir, GetLocalUserName]);
end;

function CanWriteToDirectory(const Dir: string): Boolean;
begin
  Result := False;
  if DirectoryExists(Dir) then
    Result := CanCreateFile(AddSlash(Dir) + 'WritePermissionsTest.xyzz');
end;

function CanCreateFile(const FileName: string): Boolean;
var
  Handle: Integer;
begin
  Result := False;
  if DirectoryExists(ExtractFileDir(FileName)) then begin
    Handle := FileCreate(FileName);
    Result := Handle > 0;
    if Result then begin
      FileClose(Handle);
      SysUtils.DeleteFile(FileName);
    end;
  end;
end;

function OdShellExecute(const FileName, Parameters: string;
  const RaiseException: Boolean; ShowCmd: Integer): Boolean;
var
  ReturnVal: Integer;
begin
  ReturnVal := ShellExecute(Application.Handle, nil, PChar(FileName), PChar(Parameters),
                            PChar(ExtractFilePath(FileName)), ShowCmd);
  Result := (ReturnVal > 32);
  if (not Result) and RaiseException then
    raise Exception.CreateFmt('%s (%s)', [SysErrorMessage(GetLastError), FileName]);
end;

{Elana - Added for Screenshot verification}
function OdShellEx(const FileName: string; FailMessage: string): Boolean;
  var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExecuteFile: string;
begin
  ExecuteFile := FileName;
  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
  begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    nShow := SW_SHOWNORMAL;
  end;
  if ShellExecuteEx(@SEInfo) then
  begin
    Result := True;
    repeat
      Application.ProcessMessages;
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
    until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
  end
  else begin
     MessageDlg(FailMessage, mtError, [mbOK], 0);
     Result := False;
  end;
end;


function BrowseURL(const URL: string) : Boolean;
var
   Browser: string;
begin
   Result := True;
   Browser := '';
   with TRegistry.Create do
   try
     RootKey := HKEY_CLASSES_ROOT;
     Access := KEY_QUERY_VALUE;
     if OpenKey('\htmlfile\shell\open\command', False) then
       Browser := ReadString('') ;
     CloseKey;
   finally
     Free;
   end;
   if Browser = '' then
   begin
     Result := False;
     Exit;
   end;
   Browser := Copy(Browser, Pos('"', Browser) + 1, Length(Browser)) ;
   Browser := Copy(Browser, 1, Pos('"', Browser) - 1) ;
   ShellExecute(0, 'open', PChar(Browser), PChar(URL), nil, SW_SHOW) ;
end;

function BrowseURLSpecified(const URL: string; pExe: string = 'chrome.exe'): Boolean;  //QM-1008 & QM-1014 EB HTML fix
const
  SampURL = 'https://en.wikipedia.org/wiki/J._Robert_Oppenheimer';  {Debug value}
var
  MyURL: string;
begin
   Result := True;
   {QM-1020 EB Fix Mesasge links - this is testing to see how it comes across. Unfortunately,
     would need to parse stuff out because it converts slashes. Leaving for future reference}
      //  MyURL := 'alaskaair.com';
      //  MyURL := HTMLEncode(URL);
      //  MyURL := HTTPEncode(URL);
      //  ShowMessage(URL + '     EXE: ' + pExe + ' here');  Debug

   ShellExecute(0, 'open', pChar(pExe), PChar(URL), nil, SW_SHOW) ;
end;


function BooleanToString01(B: Boolean): string;
begin
  if B then
    Result := '1'
  else
    Result := '0'
end;

function BooleanToStringYesNo(B: Boolean): string;
begin
  if B then
    Result := 'Yes'
  else
    Result := 'No'
end;



function GetFileVersionNumber(const FileName: string; var Version: TVersionNumber): Boolean;
var
  VersionInfoBufferSize: DWORD;
  dummyHandle: DWORD;
  VersionInfoBuffer: Pointer;
  FixedFileInfoPtr: PVSFixedFileInfo;
  VersionValueLength: UINT;
begin
  Result := False;
  try
    if not FileExists(FileName) then
      Exit;

    VersionInfoBufferSize := GetFileVersionInfoSize(PChar(FileName), dummyHandle);
    if VersionInfoBufferSize = 0 then
      Exit;

    GetMem(VersionInfoBuffer, VersionInfoBufferSize);
    try
      Win32Check(GetFileVersionInfo(PChar(FileName), dummyHandle,
                                    VersionInfoBufferSize, VersionInfoBuffer));

      // Retrieve root block / VS_FIXEDFILEINFO
      Win32Check(VerQueryValue(VersionInfoBuffer, '\',
                               Pointer(FixedFileInfoPtr), VersionValueLength));

      Version.dwFileVersionMS := FixedFileInfoPtr^.dwFileVersionMS;
      Version.dwFileVersionLS := FixedFileInfoPtr^.dwFileVersionLS;
      Result := True;
    finally
      FreeMem(VersionInfoBuffer);
    end;
  except
    // Ignore erors and return false since we failed to get the version number
  end;
end;

function GetFileVersionNumberString(const FileName: string; var VersionString: string): Boolean;
var
  Version: TVersionNumber;
begin
  Result := False;
  if GetFileVersionNumber(FileName, Version) then begin
    VersionString := Format('%d.%d.%d.%d', [Version.Major, Version.Minor, Version.Release, Version.Build]);
    Result := True;
  end;
end;

// Result < 0 if V1 < V2
// Result = 0 if V1 = V2
// Result > 0 if V1 > V2
function CompareVersionNumber(const V1, V2: TVersionNumber): Integer;
begin
  Result := V1.Major - V2.Major;
  if Result <> 0 then
    Exit;

  Result := V1.Minor - V2.Minor;
  if Result <> 0 then
    Exit;

  Result := V1.Release - V2.Release;
  if Result <> 0 then
    Exit;

  Result := V1.Build - V2.Build;
end;

function ParseVersionString(const VersionStr: string): TVersionNumber;
var
  Numbers: TStringList;
begin
  FillChar(Result, SizeOf(Result), 0);
  Numbers := TStringList.Create;
  try
    JclStrings.StrToStrings(VersionStr, '.', Numbers, False);
    if Numbers.Count > 0 then
      Result.Major := StrToInt(Numbers[0]);
    if Numbers.Count > 1 then
      Result.Minor := StrToInt(Numbers[1]);
    if Numbers.Count > 2 then
      Result.Release := StrToInt(Numbers[2]);
    if Numbers.Count > 3 then
      Result.Build := StrToInt(Numbers[3]);
  finally
    FreeAndNil(Numbers);
  end;
end;

function GetFileCRC32(const FileName: string; var CRC32: Cardinal): Boolean;
var
  FileStream: TFileStream;
  Buffer: array of Byte;
begin
  Result := False;
  CRC32 := 0;
  if not FileExists(FileName) then
    Exit;
  FileStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  try
    if FileStream.Size > 0 then begin
      FileStream.Position := 0;
      SetLength(Buffer, FileStream.Size);
      FileStream.ReadBuffer(Buffer[0], FileStream.Size);
      CRC32 := Crc32_A(Buffer);
      Result := True;
    end;
  finally
    FreeAndNil(FileStream);
  end;
end;

function GetScreenBitsPerPixel: Integer;
var
  DC: HDC;
begin
  DC := GetDC(0);
  Result := GetDeviceCaps(DC, BITSPIXEL) * GetDeviceCaps(DC, PLANES);
  ReleaseDC(0, DC);
end;

function AnyStringsAreBlank(Values: TStringList): Boolean;
var
  i: Integer;
begin
  Assert(Assigned(Values));
  Result := False;
  for i := 0 to Values.Count - 1 do begin
    if Values[i] = '' then begin
      Result := True;
      Break;
    end;
  end;
end;

procedure ConcatStringLists(Dest, Source: TStrings);
var
  i: Integer;
begin
  Assert(Assigned(Dest));
  Assert(Assigned(Source));
  for i := 0 to Source.Count - 1 do
    Dest.AddObject(Source[i], Source.Objects[i]);
end;

function StringListsMatch(Str1, Str2: TStrings): Boolean;
var
  i: Integer;
begin
  if Str1.Count <> Str2.Count then
    Result := False
  else begin
    Result := True;
    for i := 0 to Str1.Count - 1 do begin
      if not Str2.IndexOf(Str1[i]) >= 0 then begin
        Result := False;
        Break;
      end;
    end;
  end;
end;

function StringListIndexOfWithPrefix(List: TStrings; const Prefix, Text: string): Integer;
var
  i: Integer;
begin
  Assert(Assigned(List));
  Result := -1;
  for i := 0 to List.Count - 1 do begin
     if SameText(Prefix + List[i], Text) then begin
       Result := i;
       Break;
     end;
  end;
end;

function AllObjectsAreAssigned(Values: TStrings; StartFrom: Integer): Boolean;
var
  i: Integer;
begin
  Assert(Assigned(Values));
  Result := True;
  for i := StartFrom to Values.Count - 1 do begin
    if not Assigned(Values.Objects[i]) then begin
      Result := False;
      Break;
    end;
  end;
end;

function AddIntToArray(Value: Integer; var IArray: TIntegerArray): Integer;
begin
  SetLength(IArray, Length(IArray) + 1);
  Result := High(IArray);
  IArray[Result] := Value;
end;

function IntInArray(Value: Integer; const IArray: TIntegerArray): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(Iarray) to High(IArray) do begin
    if Value = IArray[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function IntInArray(Value: Integer; const IArray: array of Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(Iarray) to High(IArray) do begin
    if Value = IArray[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

// Case insensitive
function StringInArray(const S: string; const SArray: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Length(SArray) - 1 do
    if SameText(S, SArray[i]) then begin
      Result := True;
      Break;
    end;
end;

function HaveMatchingArrayMember(const SArray1, SArray2: array of string): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  for i := Low(SArray1) to High(SArray1) do begin
    for j := Low(SArray2) to High(SArray2) do begin
      if SArray1[i] = SArray2[j] then begin
        Result := True;
        Break;
      end;
    end;
  end;
end;

function SubStringInArray(const S: string; const SArray: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Length(SArray) - 1 do
    if Pos(SArray[i], S) > 0 then begin
      Result := True;
      Break;
    end;
end;

function StringArrayToDelimitedString(SArray: array of string; const QuoteChar, Delimiter: string): string;
var
  i: Integer;
  AddItem: string;
begin
  Result := '';
  for i := Low(SArray) to High(SArray) do begin
    AddItem := QuoteChar + SArray[i] + QuoteChar;
    if Result <> '' then
      Result := Result + Delimiter + AddItem
    else
      Result := AddItem;
  end;
end;

function DelimitedStringToStringArray(DelimitedString: string; Delimiter: string = ','): TStringArray;
var
  List : TStringList;
begin
  List := TStringList.Create;
  try
     List.DelimitedText := DelimitedString;
     Result := StringsToStringArray(List);
  finally
    FreeAndNil(List);
  end;
end;

function StringToDelimitedString(Values: string; const QuoteChar, Delimiter: string): string;
var
  Strings: TStringList;
  SArray: array of string;
  i: Integer;
begin
  Strings := TStringList.Create;
  try
    JclStrings.StrToStrings(Values, ',', Strings, False);
    SetLength(SArray, Strings.Count);
    for i := 0 to Strings.Count - 1 do
      SArray[i] := Trim(Strings[i]);
    Result := StringArrayToDelimitedString(SArray, QuoteChar, Delimiter);
  finally
    FreeAndNil(Strings);
  end;
end;

function GetStringBeforeCharacter(const Str: string; StopChar: Char): string;
var
  i: Integer;
begin
  Result := Str;
  for i := 1 to Length(Str) do begin
    if Str[i] = StopChar then begin
      Result := Copy(Str, 1, i - 1);
      Break;
    end;
  end;
end;

// For speed reasons, this assumes the array is uppercase.  It ignores sequence
// of whitespace differences.
function IsTextInArray(const S: string; const SArray: array of string): Boolean;
var
  i: Integer;
  NormalizedString: string;
begin
  Result := False;
  NormalizedString := CompressWhiteSpace(UpperCase(S));
  for i := Low(SArray) to High(SArray) do begin
    if NormalizedString = SArray[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function IntegerInArray(Int: Integer; const IArray: array of Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Length(IArray) - 1 do
    if Int = IArray[i] then begin
      Result := True;
      Break;
    end;
end;

function ReplaceIntInArray(var IArray: array of Integer; Find, Replace: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(IArray) to High(IArray) do begin
    if IArray[i] = Find then begin
      IArray[i] := Replace;
      Inc(Result);
    end;
  end;
end;

function StrReplaceChars(const Str: string; Chars: TSysCharSet; ReplaceWith: Char): string;
var
  i: Integer;
begin
  Result := Str;
  for i := 0 to Length(Result) - 1 do
    if CharInSet(Str[i], Chars) then
      Result[i] := ReplaceWith;
end;

{$IFNDEF UNICODE}
function CharInSet(C: Char; CSet: TSysCharSet): Boolean;
begin
  Result := C in CSet;
end;
{$ENDIF}

function GetLastOsErrorMessage(Error: Integer): string;
begin
  if Error = MaxInt then
    Result := SysErrorMessage(GetLastError)
  else
    Result := SysErrorMessage(Error);
end;

procedure RaiseOSError(const Msg: string);
var
  OSError: Integer;
  ErrorMsg: string;
  OSErrorMsg: string;
begin
  ErrorMsg := Msg;
  OSError := GetLastError;
  OSErrorMsg := GetLastOsErrorMessage(OSError);
  if Trim(OSErrorMsg) <> '' then
    ErrorMsg := ErrorMsg + sLineBreak + OSErrorMsg;
  ErrorMsg := ErrorMsg + sLineBreak + 'OS Error Code: ' + IntToStr(OSError);
  raise Exception.Create(ErrorMsg);
end;

procedure RaiseOsExceptionIfNil(Value: Pointer; const Msg: string = '');
begin
  if Value = nil then
    RaiseOSError(Msg);
end;

procedure RaiseOsExceptionIfFalse(Value: Boolean; const Msg: string = '');
begin
  if not Value then
    RaiseOSError(Msg);
end;

procedure ShowExceptionError(E: Exception);
begin
  Assert(Assigned(E));
  ErrorDialog(E.Message);
end;

procedure ErrorDialog(const Msg: string);
begin
  MessageDlg(Msg, mtError, [mbOK], 0);
end;

function YesNoDialog(const Msg: string): Boolean;
begin
  Result := MessageDlg(Msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;

function YesNoDialogFmt(const Msg: string; Args: array of const): Boolean;
begin
  Result := YesNoDialog(Format(Msg, Args));
end;

function AddSlash(const Path: string): string;
begin
  Result := IncludeTrailingPathDelimiter(Path);
end;

function RemoveSlash(const Str: string): string;
begin
  if StrEndsWith('\', Str) or StrEndsWith('/', Str) then
    Result := StrLeft(Str, Length(Str) - 1)
  else
    Result := Str;
end;

function IsPathAbsolute(const FileName: string): Boolean;
begin
  Result := ExtractFileDrive(FileName) <> '';
end;

function GetWindowsTempPath: string;
var
  PathLength: Integer;
begin
  SetLength(Result, MAX_PATH);
  PathLength := GetTempPath(Length(Result), PChar(Result));
  if (PathLength = 0) or (PathLength > MAX_PATH) then
    RaiseLastOSError;
  SetLength(Result, PathLength);
  Result := IncludeTrailingPathDelimiter(Result);
end;

function GetWindowsTempFileName(Create: Boolean; Path: string; Prefix: string): string;
var
  CreateFlag: Integer;
begin
  if Path = '' then
    Path := GetWindowsTempPath;
  if Prefix = '' then
    Prefix := 'ODT';
  if Create then
    CreateFlag := 0
  else
    CreateFlag := Random(65536);
  SetLength(Result, MAX_PATH + 1);
  Path := JclFileUtils.PathGetLongName(Path);
  if GetTempFileName(PChar(Path), PChar(Prefix), CreateFlag, PChar(Result)) = 0 then
    RaiseLastOSError;
  Result := PChar(Result);
end;

function GetWindowsSystemError(ErrorCode: HRESULT): string;
var
  MsgBuffer: pChar;
begin
  MsgBuffer := nil;
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
    nil, ErrorCode, 0, @MsgBuffer, 0, nil);
  Result := MsgBuffer;
  LocalFree(Cardinal(MsgBuffer));
end;

function InheritsFromClass(ClassType: TClass; const ClassName: string): Boolean;
begin
  Result := True;
  while ClassType <> nil do begin
    if SameText(ClassType.ClassName, ClassName) then
      Exit;
    ClassType := ClassType.ClassParent;
  end;
  Result := False;
end;

function XmlEncodeString(const AString: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(AString) do
  begin
    case AString[i] of
      '&': Result := Result + '&amp;';
      '>': Result := Result + '&gt;';
      '<': Result := Result + '&lt;';
      '"': Result := Result + '&quot;'
      else
        Result := Result + AString[i]
    end;
  end;
end;

function VarToInt(Value: Variant; Default: Integer): Integer;
begin
  if not VarIsNull(Value) then
    Result := Value
  else
    Result := Default;
end;

function VarToFloat(Value: Variant): Double;
begin
  if not VarIsNull(Value) then
    Result := Value
  else
    Result := 0;
end;

function VarToString(Value: Variant; Default: string): string;
begin
  if not VarIsNull(Value) then
    Result := Value
  else
    Result := Default;
end;

function VarToDateTime(Value: Variant): TDateTime;
begin
  if not VarIsNull(Value) then
    Result := Value
  else
    Result := 0;
end;

function VarToBoolean(Value: Variant; Default: Boolean): Boolean;
begin
  if VarIsNull(Value) or (VarIsStr(Value) and (Value = '')) then
    Result := Default
  else
    Result := Value;
end;

function IntToBoolean(Value: Integer): Boolean;
begin
  Result := Value <> 0;
end;

function IntKeyToString(Value: Integer): string;
begin
  if Value <= 0 then
    Result := ''
  else
    Result := IntToStr(Value);
end;

function VarParamToStr(Value: Variant): string;
begin
  if VarIsNull(Value) then
    Result := 'null'
  else
    Result := QuotedStr(VarToStr(Value));
end;

function FormatCurrency(Value: Currency; Decimals: Boolean): string;
begin
  if Decimals then
    Result  := FormatCurr(MoneyFormat, Value)
  else
    Result  := FormatCurr(MoneyFormatNoDecimals, Value)
end;

// Transforms all consecutive sequences of #10, #13, #32, and #9 in Str
// into a single space, and strips off whitespace at the beginning and
// end of the string.  This function is impossible to read (sorry),
// but it is tested and fast (I originally wrote this for GExperts)
function CompressWhiteSpace(const Str: string): string;
var
  i: Integer;
  Len: Integer;
  NextResultChar: Integer;
  CheckChar: Char;
  NextChar: Char;
begin
  Len := Length(Str);
  NextResultChar := 1;
  SetLength(Result, Len);

  for i := 1 to Len do
  begin
    CheckChar := Str[i];
    {$RANGECHECKS OFF}
    NextChar := Str[i + 1];
    {$RANGECHECKS ON}
    case CheckChar of
      #9, #10, #13, #32:
        begin
          if CharInSet(NextChar, [#0, #9, #10, #13, #32]) or (NextResultChar = 1) then
            Continue
          else
          begin
            Result[NextResultChar] := #32;
            Inc(NextResultChar);
          end;
        end;
      else
        begin
          Result[NextResultChar] := Str[i];
          Inc(NextResultChar);
        end;
    end;
  end;
  if Len = 0 then
    Exit;
  SetLength(Result, NextResultChar - 1);
end;

function LeadingWhiteSpace(const Str: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(Str) do begin
    if CharInSet(Str[i], [#9, #32]) then
      Result := Result + Str[i]
    else
      Exit;
  end;
end;

function IsWeekend(Date: TDateTime): Boolean;
begin
  Result := DayOfTheWeek(Date) in [DaySaturday, DaySunday]
end;

function ThisSaturday: TDateTime;
begin
  Result := SaturdayIze(Date);
end;

function SaturdayIze(D: TDateTime): TDateTime;
begin
  while DayOfWeek(D) <> DaySunday do
    D := D + 1;
  Result := D;
end;

// Weeks are Sun-Sat for this function (unlike StartOfTheWeek)
function BeginningOfTheWeek(Date: TDateTime): TDateTime;
begin
  Result := StartOfTheDay(Date);
  while DayOfTheWeek(Result) <> DaySunday do
    Result := Result - 1;
end;

// Weeks are Sun-Sat for this function (unlike EndOfTheWeek)
function EndingOfTheWeek(Date: TDateTime): TDateTime;
begin
  Result := EndOfTheDay(Date);
  while DayOfTheWeek(Result) <> DaySaturday do
    Result := Result + 1;
end;


// Compare two dates, ignoring the time (fractional) component
function DayIsAfterDay(Date1, Date2: TDateTime): Boolean;
begin
  Result := Trunc(Date1) > Trunc(Date2);
end;

function TimeRangeOverlap(Range1Start, Range1Finish, Range2Start, Range2Finish : TDateTime) : TDateTime;
begin
  Result := Max(Min(Range1Finish, Range2Finish) - Max(Range1Start, Range2Start), 0);
end;

function StrContains(const SubStr, S: string): Boolean;
begin
  Result := Pos(SubStr, S) > 0;
end;

function StrContainsText(const SubStr, S: string): Boolean;
begin
  Result := Pos(AnsiUpperCase(SubStr), AnsiUpperCase(S)) > 0;
end;

function StrNotContain(const SubStr, S: string): Boolean;
begin
  Result := Pos(SubStr, S) = 0;
end;

function StrBeginsWith(const SubStr, S: string): Boolean;
begin
  Result := SubStr = LeftStr(S, Length(SubStr));
end;

function StrBeginsWithText(const SubStr, S: string): Boolean;
begin
  Result := UpperCase(SubStr) = UpperCase(LeftStr(S, Length(SubStr)));
end;

function StrEndsWith(const SubStr, S: string): Boolean;
begin
  Result := RightStr(S, Length(SubStr)) = SubStr;
end;

function StrEndsWithText(const SubStr, S: string): Boolean;
begin
  Result := StrEndsWith(Uppercase(SubStr), Uppercase(S));
end;

function StrGetWord(const Str: string; WordIndex: Integer; Delimiter: Char; DoTrim: Boolean): string;
var
  Strings: TStringList;
begin
  Result := '';
  Strings := TStringList.Create;
  try
    JclStrings.StrToStrings(Str, Delimiter, Strings, False);
    if Strings.Count >= WordIndex then begin
      Result := Strings[WordIndex - 1];
      if DoTrim then
        Result := Trim(Result);
    end;
  finally
    FreeAndNil(Strings);
  end;
end;

function StrConsistsOf(const S: string; Chars: TSysCharSet): Boolean;
var
  i: Integer;
begin
  Result := True; // Empty strings
  for i := 1 to Length(S) do begin
    if not CharInSet(S[i], Chars) then begin
      Result := False;
      Break;
    end;
  end;
end;

function CharRangeString(StartChar, StopChar: Char): string;
var
  i: Integer;
begin
  Assert(StartChar <= StopChar);
  SetLength(Result, Ord(StopChar) - Ord(StartChar) + 1);
  for i := Ord(StartChar) to Ord(StopChar) do begin
    Result[i - Ord(StartChar) + 1] := Char(i);
  end;
end;

function NotEmpty(const Str: string): Boolean;
begin
  Result := Trim(Str) <> '';
end;

function IsEmpty(const Str: string): Boolean;
begin
  Result := Trim(Str) = '';
end;

function RemoveBlankLines(const Str: string): string;
var
  Strings: TStringList;
  i: Integer;
begin
  Strings := TStringList.Create;
  try
    Strings.Text := Str;
    for i := Strings.Count - 1 downto 0 do
      if Trim(Strings[i]) = '' then
        Strings.Delete(i);
    Result := Trim(Strings.Text);
  finally
    FreeAndNil(Strings);
  end;
end;

function RemovePrefix(const Prefix, Str: string): string;
begin
  Result := Str;
  if StrBeginsWithText(Prefix, Str) then begin
    if SameText(Str, Prefix) then
      Result := ''
    else
      Result := Copy(Str, Length(Prefix) + 1, MaxInt);
  end;
end;

function RemoveSuffix(const Suffix, Str: string): string;
begin
  Result := Str;
  if StrEndsWith(Suffix, Str) then begin
    if SameText(Str, Suffix) then
      Result := ''
    else
      Result := Copy(Str, 1, Length(Str) - Length(Suffix));
  end;
end;

function IsLatin1Char(const C:Char):Boolean;
// The unicode block Latin1 contains the Ascii char set.
begin
  Result := Integer(C) <= $FF;
end;

function IsASCIIChar(const C:Char): Boolean;
//Return True if the character is in the original (0-127) ASCII set.
begin
  Result := Integer(C) <= $7F; //logic borrowed from TCharacter.IsAscii since it's private
end;

function IsControlChar(const C: Char): Boolean;
//Returns True for Latin1 Control Characters (except for LF, CR or tab)
begin
  {$IFDEF UNICODE}
  Result := IsLatin1Char(C) and (C <= #31) and (not CharInSet(C, [#9,#10,#13]));
  {$ELSE}
  Result := False;
  if IsLatin1Char(C) then begin
      if (C <= #31) and not (C in [#9,#10,#13])  then
        Result := True;
  end;
  {$ENDIF}
end;

function IsInvalidAsciiChar(const C: Char): Boolean;
begin
  Result := True;

  {$IFDEF UNICODE}
  if IsLatin1Char(C) then begin
    // false for non-control original Ascii characters; also we return false for CR, LF, and tab
    if (CharInSet(C, [#9,#10,#13])) or ((C >= #32) and (C <= #127)) then
      Result := False;
  end;
  {$ELSE}
  If IsLatin1Char(C) then begin
    //return false for non-control original Ascii characters; also we return false for CR, LF, and tab
    If (C in [#9,#10,#13]) or ((C >= #32) and (C <= #127)) then
      Result := False;
  end;
  {$ENDIF}
end;

function IsInvalidExportChar(const C: Char): Boolean;
begin
  if C = '"' then
    Result := True
  else
    Result := False;
end;


function ReplaceNonEnglishAsciiChars(const Str: string; ReplaceChar: Char): string;
begin
  Result := JclStrings.StrReplaceChars(Str, IsInvalidASCIIChar, ReplaceChar);
end;

// Remove non-whitespace ASCII control characters (they are invalid in XML)
// Note: $81, $8D, $8F, $90, and $9D are meaningless in windows-1252, but are valid in XML
function RemoveControlChars(const Str: string): string;
begin
  Result := StringReplace(Str, #160, '', [rfReplaceAll]);      //QM-1029 EB Removing NBSP and Control Chars
  Result := JclStrings.StrRemoveChars(Result, IsControlChar);
end;

function ReplaceControlChars(const Str: string; const ReplaceWith: Char): string;
begin
  Result := JCLStrings.StrReplaceChars(Str, IsControlChar, ReplaceWith);
end;

// Adapted from: http://groups.google.com/groups?selm=20000114165331.23039.00001163%40nso-ck.aol.com
function NumToWords(Num: Longint): string;

  // Converts number under one thousand to words
  function NumLoToWords(N : Integer): string;
  const
    UnitTeenWords : array[0..19] of string =
      ('', 'One ', 'Two ', 'Three ', 'Four ', 'Five ',
       'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ',
       'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ',
       'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen ');
    TensWords : array[0..9] of string =
      ('', 'Ten ', 'Twenty ', 'Thirty ', 'Forty ', 'Fifty ',
       'Sixty ', 'Seventy ', 'Eighty ', 'Ninety ');
  begin
    case N of
      0: Result := '';
      1..19: Result := UnitTeenWords[N];
      20..99: Result := TensWords[N div 10] + NumLoToWords(N mod 10);
      100..999:
        begin
          Result := NumLoToWords(N div 100) + 'Hundred ';
          if (N mod 100) > 0 then
           Result := Result + 'and ' + NumLoToWords(N mod 100);
        end;
    end;
  end;

begin
  case Num of
    0..999:
      Result := NumLoToWords(Num);
    1000..999999:
      begin
        Result := NumToWords(Num div 1000) + 'Thousand ';
        if ((Num mod 1000) > 0) and ((Num mod 1000) < 100) then
          Result := Result + 'and ';
        Result := Result + NumToWords(Num mod 1000);
      end;
    1000000..High(Longint):
      begin
        Result := NumToWords(Num div 1000000) + 'Million';
        if ((Num mod 1000000) > 0) and ((Num mod 1000000) < 100) then
          Result := Result + 'and ';
        Result := Result + NumToWords(Num mod 1000000);
      end;
  end;
  Result := Trim(Result);
end;

function RoundUp(Value: Extended): Int64;
begin
  Result := Trunc(Value) + Trunc(Frac(Value) * 2);
end;

function RoundPositiveToMin(Value: Extended; Min: Integer): Integer;
begin
  if Value > 0 then begin
    Result := Round(Value);
    if Result < 1 then
      Result := 1;
  end
  else
    Result := Round(Value);
end;

function RoundToNearestMultiple(Qty, MultipleOf: Integer): Integer;
var
  Times: Double;
  Remainder: Double;
begin
  Assert(Qty >= 0);
  Assert(MultipleOf >= 0);
  Times := Qty / MultipleOf;
  Remainder := Frac(Times);
  if Remainder >= 0.5 then
    Result := (Trunc(Times) + 1) * MultipleOf
  else
    Result := Trunc(Times) * MultipleOf;
  if Result < 1 then
    Result := MultipleOf;
end;

function GetUTCBias: Integer;
var
  TZResult: DWORD;
  TZInfo: TTimeZoneInformation;

begin
  TZResult := GetTimeZoneInformation(TZInfo);
  if (TZResult = $FFFFFFFF) then
    raise Exception.CreateFmt('GetTimeZoneInformation failed with %d', [GetLastError]);
    Result := TZInfo.Bias;
//  Result := TZInfo.Bias+ TZInfo.DaylightBias;
  // The bias should never be more than a +/- 1 day
  Assert(Result <= (60 * 24));
  Assert(Result >= -(60 * 24));
end;

{-- WinExecAndWait32V2 ------------------------------------------------}
{: Executes a program and waits for it to terminate
@Param FileName contains executable + any parameters
@Param Visibility is one of the ShowWindow options, e.g. SW_SHOWNORMAL
@Returns -1 in case of error, otherwise the programs exit code
@Desc In case of error SysErrorMessage( GetlastError ) will return an
  error message. The routine will process paint messages and messages
  send from other threads while it waits.
}{ Created 27.10.2000 by P. Below
-----------------------------------------------------------------------}
function WinExecAndWait32V2(const FileName: string; Visibility: Word): DWORD;

  procedure WaitFor(processHandle: THandle);
  var
    msg: TMsg;
    ret: DWORD;
  begin
    repeat
      ret := MsgWaitForMultipleObjects(
        1, { 1 handle to wait on }
        processHandle, { the handle }
        False, { wake on any event }
        INFINITE, { wait without timeout }
        QS_PAINT or { wake on paint messages }
        QS_SENDMESSAGE { or messages from other threads }
        );
      if ret = WAIT_FAILED then Exit; { can do little here }
      if ret = (WAIT_OBJECT_0 + 1) then begin
          { Woke on a message, process paint messages only. Calling
            PeekMessage gets messages send from other threads processed. }
        while PeekMessage(msg, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
          DispatchMessage(msg);
      end;
    until ret = WAIT_OBJECT_0;
  end; { Waitfor }

var { V1 by Pat Ritchey, V2 by P.Below }
  zAppName: array[0..9999] of Char;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin { WinExecAndWait32V2 }
  if Length(FileName) >= Length(zAppName) then
    raise Exception.Create('FileName is too long in WinExecAndWait32V2: ' + FileName);
  StrPCopy(zAppName, FileName);
  FillChar(StartupInfo, SizeOf(StartupInfo), #0);
  StartupInfo.cb := SizeOf(StartupInfo);
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := Visibility;
  if not CreateProcess(nil,
    zAppName, { pointer to command line string }
    nil, { pointer to process security attributes }
    nil, { pointer to thread security attributes }
    false, { handle inheritance flag }
    CREATE_NEW_CONSOLE or { creation flags }
    NORMAL_PRIORITY_CLASS,
    nil, { pointer to new environment block }
    nil, { pointer to current directory name }
    StartupInfo, { pointer to STARTUPINFO }
    ProcessInfo) { pointer to PROCESS_INF }
    then
    Result := DWORD(-1) { failed, GetLastError has error code }
  else begin
    Waitfor(ProcessInfo.hProcess);
    GetExitCodeProcess(ProcessInfo.hProcess, Result);
    CloseHandle(ProcessInfo.hProcess);
    CloseHandle(ProcessInfo.hThread);
  end; { Else }
end; { WinExecAndWait32V2 }

function CreateCommaStringList(const S: string; DoTrim: Boolean = True): TStringList;
var
  i: Integer;
begin
  Result := TStringList.Create;
  Result.CommaText := S;
  if DoTrim then
    for i := 0 to Result.Count - 1 do
      Result[i] := Trim(Result[i]);
end;

procedure DeleteStringFromList(List: TStrings; const Item: string);
var
  Index: Integer;
begin
  Assert(Assigned(List));
  Index := List.IndexOf(Item);
  if Index >= 0 then
    List.Delete(Index);
end;

procedure EnsureStringInList(List: TStrings; const Item: string);
var
  Index: Integer;
begin
  Assert(Assigned(List));
  Index := List.IndexOf(Item);
  if Index = -1 then
    List.Add(Item);
end;

function GetLineWithPrefix(List: TStrings; const Prefix: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to List.Count - 1 do begin
    if SameText(Prefix, Copy(Trim(List[i]), 1, Length(Prefix))) then begin
      Result := i;
      Break;
    end;
  end;
end;

function RandomAlphaNumericString(StrLength: Integer): string;
const
  ValidChars = 'ABCDEFGHIJKLMNOPQRSTUVQXYZ1234567890';
var
  i: Integer;
  ValidCharsLength: Integer;
begin
  ValidCharsLength := Length(ValidChars);
  for i := 1 to StrLength do
    Result := Result + ValidChars[Random(ValidCharsLength) + 1];
end;

function IsCharNumeric(C: Char): Boolean;
begin
  {$IFDEF UNICODE}
  Result := TCharacter.IsDigit(C);
  {$ELSE not UNICODE}
  Result := CharInSet(C, ['0'..'9']);
  {$ENDIF}
end;

function RandomString(StrLength: Integer; const Characters: string): string;
var
  i, CharIndex: Integer;
begin
  Result := '';
  for i := 1 to StrLength do begin
    CharIndex := Random(Length(Characters)) + 1;
    Result := Result + Characters[CharIndex];
  end;
end;

function FloatEquals(A, B, Tolerance: Double): Boolean;
begin
  Result := Abs(A - B) < Tolerance;
end;

function FloatRound(Value: Double; DecPlaces: Cardinal): Double;
var
  RoundFormat: string;
  Rounded: string;
begin
  RoundFormat := '%.' + IntToStr(DecPlaces) +'f';
  Rounded := Format(RoundFormat, [Value]);
  Result := StrToFloat(Rounded);
end;

function RangesIntersect(Start1, Stop1, Start2, Stop2: Double): Boolean;

  function Between(Value, RangeStart, RangeStop: Double): Boolean;
  begin
    Result := (Value > RangeStart) and (Value < RangeStop);
  end;

  function Covers(St1, Sp1, St2, Sp2: Double): Boolean;
  begin
    Result := (St1 <= St2) and (Sp1 >= Sp2);
  end;

begin
  //QM-606 EB Precision issue should not be triggering overlap
  if (Start2 = 0) and (Stop1 = 0) and (Stop2 = 0) then
    Result := False
  else
  Result := Between(Start1, Start2, Stop2) or Between(Start2, Start1, Stop1)
         or Between(Stop1, Start2, Stop2) or Between(Stop2, Start1, Stop1)
         or Covers(Start1, Stop1, Start2, Stop2)
         or Covers(Start2, Stop2, Start1, Stop1);
end;

function SHCopyFiles(const Src, Dest: string; Silent: Boolean; RaiseException: Boolean): Boolean;
var
  Flags: FILEOP_FLAGS;
begin
  Flags := 0;
  if Silent then
    Flags := FOF_RENAMEONCOLLISION or FOF_NOERRORUI or FOF_NOCONFIRMATION or FOF_NOCONFIRMMKDIR;
  Result := ShellCopy(Src, Dest, Flags, RaiseException);
end;

function ShellCopy(const Src, Dest: string; Flags: FILEOP_FLAGS; RaiseException: Boolean): Boolean;
var
  FileOp: TSHFileOpStruct;
  Source, Destination: string;
begin
  FillChar(FileOp, SizeOf(FileOp), #0);
  with FileOp do
  begin
    Wnd := GetDesktopWindow;
    wFunc := FO_COPY;
    Source := Src + #0#0;
    Destination := Dest + #0#0;
    pFrom := PChar(Source);
    pTo := PChar(Destination);
    fFlags := Flags;
  end;
  Result := SHFileOperation(FileOp) = 0;
  if RaiseException and (not Result) then
    raise Exception.CreateFmt('Unable to copy %s to %s', [Src, Dest]);
end;

function HTMLEscape(const Str: string): string;
const
  Indent = '&nbsp;&nbsp;';
  LineBreak = '<br /> '#13#10;
  WhiteSpace = [#9, #32];
  LineBreaks = [#10, #13];

var
  i, k,
  Repeats : Integer;

  procedure AddText(const Text: string);
  begin
    Result := Result + Text;
  end;

  function BoundedBy(const c: Char): Boolean;
  begin
    Result := ((i > 1) and (Str[i-1] = c)) or ((i < k) and (Str[i+1] = c));
  end;

begin
  k := Length(Str);
  Result := '';
  i := 1;
  while i <= k do
  begin
  case Str[i] of
    '<' : AddText('&lt;');
    '>' : AddText('&gt;');
    '&' : AddText('&amp;');
    '"' : AddText('&quot;');
    #9,
    #10: if not BoundedBy(#13) then
        AddText(LineBreak);
    #13: AddText(LineBreak);
    #32 :
      begin
        Repeats := 0;
        while (i < k) and CharInSet(Str[i], WhiteSpace) do
        begin
          Inc(i);
          if Str[i] = #9 then
            Inc(Repeats, 2)
          else
            Inc(Repeats, 1);
        end;
        if not CharInSet(Str[i], WhiteSpace) then
          Dec(i);
        AddText(DupeString(Indent, Repeats div 2));
        if Odd(Repeats) then
          AddText(#32);
      end;
    #92,
    #160 .. #255 : AddText('&#' + IntToStr( Ord( Str[ i ] ) ) + ';');
    else
      AddText(Str[i]);
    end; { case }
    Inc(i);
  end;
end;

function JSONEscape(const Str: string): string;
const
  TOK_BS = #8;
  TOK_TAB = #9;
  TOK_LF = #10;
  TOK_FF = #12;
  TOK_CR = #13;
  TOK_QUOT = '"';
  TOK_SR = '/';
  TOK_SL = '\';

  ESC_BS = '\b';
  ESC_TAB = '\t';
  ESC_LF = '\n';
  ESC_FF = '\f';
  ESC_CR = '\r';
var
  i, k: Integer;

  procedure AddText(const Text: string);
  begin
    Result := Result + Text;
  end;

  function BoundedBy(const c: Char): Boolean;
  begin
    Result := ((i > 1) and (Str[i-1] = c)) or ((i < k) and (Str[i+1] = c));
  end;

begin
  k := Length(Str);
  Result := '';
  i := 1;
  while i <= k do begin
    case Str[i] of
      TOK_SR, TOK_SL, TOK_QUOT: begin
        AddText('\');
        AddText(Str[i]);
      end;
      TOK_BS: AddText(ESC_BS);
      TOK_LF:
        if not BoundedBy(TOK_CR) then
          AddText(ESC_LF);
      TOK_CR: AddText(ESC_CR);
      TOK_FF: AddText(ESC_FF);
      TOK_TAB: AddText(ESC_TAB);
      else begin
        if Ord(Str[i]) < 32 then begin
          AddText('\u');
          AddText(IntToHex(Ord(Str[i]), 4));
        end else
          AddText(Str[i]);
      end;
    end; { case }
    Inc(i);
  end;
end;

function DesktopDirectory: string;
begin
  Result := GetSpecialFolderLocation(CSIDL_DESKTOP);
end;

function MakeLegalFileName(const FileName: string; ReplaceStr: string): string;
begin
  Result := FileName;
  Result := StringReplace(Result, '\', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '/', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, ':', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '*', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '?', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '"', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '>', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '<', ReplaceStr, [rfReplaceAll]);
  Result := StringReplace(Result, '|', ReplaceStr, [rfReplaceAll]);
end;

function InRange(Value, Min, Max: Integer): Boolean;
begin
  Assert(Min <= Max);
  Result := (Value >= Min) and (Value <= Max);
end;

function GetLineCount(const Text: string): Integer;
var
  Lines: TStringList;
begin
  Lines := TStringList.Create;
  try
    Lines.Text := Text;
    Result := Lines.Count;
  finally
    FreeAndNil(Lines);
  end;
end;

function GetLine(const FullText: string; LineNo: Integer): string;
var
  Lines: TStringList;
begin
  Lines := TStringList.Create;
  try
    Lines.Text := FullText;
    if (LineNo < 0) or (LineNo >= Lines.Count) then
      raise Exception.Create('Invalid line index ' + IntToStr(LineNo));
    Result := Lines[LineNo];
  finally
    FreeAndNil(Lines);
  end;
end;

function SetLine(const FullText: string; LineNo: Integer; const NewLine: string): string;
var
  Lines: TStringList;
begin
  Lines := TStringList.Create;
  try
    Lines.Text := FullText;
    if (LineNo < 0) or (LineNo >= Lines.Count) then
      raise Exception.Create('Invalid line index ' + IntToStr(LineNo));
    Lines[LineNo] := NewLine;
    Result := Lines.Text;
  finally
    FreeAndNil(Lines);
  end;
end;


function GetThisModulePath: string;
var
  Buf: array[0..MAX_PATH] of Char;
begin
  GetModuleFileName(HINSTANCE, Buf, Length(Buf));
  Result := StrPas(Buf);
end;

function GetResourceString(ResourceName: string): string;
var
  ResourceStream: TResourceStream;
  StringStream: TStringStream;
begin
  if FindResource(HInstance, PChar(ResourceName), 'STRING') = 0 then
    raise Exception.Create('Could not load string resource ' + ResourceName);
  StringStream := nil;
  ResourceStream := TResourceStream.Create(HInstance, ResourceName, 'STRING');
  try
    ResourceStream.Position := 0;
    StringStream := TStringStream.Create('');
    StringStream.CopyFrom(ResourceStream, ResourceStream.Size);
    Result := StringStream.DataString;
  finally
    FreeAndNil(ResourceStream);
    FreeAndNil(StringStream);
  end;

  if Trim(Result) = '' then
    raise Exception.Create('Resouce is empty');
end;

function StringArrayToString(Values: array of string): string;
var
  i: Integer;
begin
  Result := '';
  for i := Low(Values) to High(Values) do begin
    if Result = '' then
      Result := Values[i]
    else
      Result := Result +','+ Values[i];
  end;
end;

function HoursFormat(Hours: Double): string;
begin
  if FloatEquals(Hours, 0) then
    Result := ''
  else
    Result := FormatFloat('0.00', Hours);
end;

function MakeGUIDString: string;
var
  G: TGUID;
begin
  CoCreateGuid(G);
  Result := GUIDToString(G);
end;

procedure SaveDebugFile(const FileName, Data: string);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(FileName, fmCreate);
  try
    FS.WriteAnsiString(AnsiString(Data));
  finally
    FreeAndNil(FS);
  end;
end;

{$RANGECHECKS OFF}
{$OVERFLOWCHECKS OFF}
const
  DefaultKey = '57ffR!23tE';

// This function should fix all of the problems exhibited in TI2803
// Source: http://www.seanet.com/~pgm/delphi/encrypt.htm
function SuperCipher(const S: string; const Key : string = '') : string;
var
  I, Z: Integer;
  C: Char;
  Code: Byte;
  UsedKey: string;
begin
  Result:= '';
  if Key = '' then
    UsedKey := DefaultKey else
    UsedKey := Key;
  Z:= length(UsedKey);
  if (Z > 0) and (length(S) > 0) then
    for I:= 1 to length(S) do begin
      Code:= ord(UsedKey[(I - 1) mod Z + 1]);
      if S[I] >= #128 then
        C:= Chr(ord(S[I]) xor (Code and $7F))
      else if S[I] >= #64 then
        C:= Chr(ord(S[I]) xor (Code and $3F))
      else if S[I] >= #32 then
        C:= Chr(ord(S[I]) xor (Code and $1F))
      else
        C:= S[I];
      Result:= Result + C;
      end;
end;
{$RANGECHECKS ON}
{$OVERFLOWCHECKS ON}

function HighResolutionScreen: Boolean;
var
  Width, Height: Integer;
begin
  Width := GetSystemMetrics(SM_CXSCREEN);
  Height := GetSystemMetrics(SM_CYSCREEN);
  Result := (Width > 800) and (Height > 600);
end;

function ConnectToConsole(var ShouldFreeConsole: Boolean): Boolean;
var
  DLLHandle: THandle;
  AttachConsole: function(ProcessID: DWord): Bool; stdcall;
begin
  Result := False;
  ShouldFreeConsole := False;
  DLLHandle := LoadLibrary('kernel32.dll');
  if DLLHandle <> INVALID_HANDLE_VALUE then begin
    try
      AttachConsole := GetProcAddress(DLLHandle, 'AttachConsole');
      if Assigned(AttachConsole) then // Present in Winows XP or greater
        Result := AttachConsole(ATTACH_PARENT_PROCESS); // This fails if thee is not parent console
      if not Result then begin
        Result := AllocConsole;
        if Result then
          ShouldFreeConsole := True;
      end;
    finally
      FreeLibrary(DLLHandle);
    end;
  end;
end;

procedure ConsoleWaitForEnterKey(TimeOutSecs, SleepIntervalMS: Cardinal);
{ Call this procedure with the timeout and sleep params when showing a console
  window is necessary but the console window should not be tied up.
  Adapted From: http://stackoverflow.com/questions/16022126/how-to-make-a-console-application-wait-for-the-enter-key-but-automatically-co }
  function KeyPressed(ExpectedKey: Word): Boolean;
  var
    lpNumberOfEvents: DWORD;
    lpBuffer: TInputRecord;
    lpNumberOfEventsRead: DWORD;
    nStdHandle: THandle;
  begin
    Result := False;
    nStdHandle := GetStdHandle(STD_INPUT_HANDLE);
    lpNumberOfEvents := 0;
    GetNumberOfConsoleInputEvents(nStdHandle, lpNumberOfEvents);
    if lpNumberOfEvents <> 0 then
    begin
      PeekConsoleInput(nStdHandle, lpBuffer, 1, lpNumberOfEventsRead);
      if lpNumberOfEventsRead <> 0 then
        if lpBuffer.EventType = KEY_EVENT then
          if lpBuffer.Event.KeyEvent.bKeyDown and
            ((ExpectedKey = 0) or
            (lpBuffer.Event.KeyEvent.wVirtualKeyCode = ExpectedKey)) then
            Result := True
          else
            FlushConsoleInputBuffer(nStdHandle)
        else
          FlushConsoleInputBuffer(nStdHandle);
    end;
  end;

var
  Stop: Cardinal;
begin
  Stop := GetTickCount + TimeOutSecs * 1000;
  while (not KeyPressed(VK_RETURN)) and (GetTickCount < Stop) do
    Sleep(SleepIntervalMS); // check every SleepInterval ms
end;

procedure ClonePersistent(Source, Dest: TPersistent);
var
  i: Integer;
  PropCount: Integer;
  PropList: TPropList;
  PropName: string;
begin
  Assert(Assigned(Source) and Assigned(Dest));

  PropCount := GetPropList(Source.ClassInfo, tkAny, @PropList);
  for i := 0 to PropCount - 1 do begin
    PropName := string(PropList[i].Name);
    case PropList[i].PropType^.Kind of
      tkLString, tkWString, tkVariant:
        SetPropValue(Dest, PropName, GetPropValue(Source, PropName));
      tkInteger, tkChar, tkWChar:
        SetOrdProp(Dest, PropName, GetOrdProp(Source, PropName));
      tkFloat:
        SetFloatProp(Dest, PropName, GetFloatProp(Source, PropName));
      tkString:
        SetStrProp(Dest, PropName, GetStrProp(Source, PropName));
      tkMethod:
        SetMethodProp(Dest, PropName, GetMethodProp(Source, PropName));
      tkInt64:
        SetInt64Prop(Dest, PropName, GetInt64Prop(Source, PropName));
      tkEnumeration:
        SetEnumProp(Dest, PropName, GetEnumProp(Source, PropName));
      tkSet:
        SetSetProp(Dest, PropName, GetSetProp(Source, PropName));
{$IFDEF UNICODE}
      tkUString:   //QMANTWO-513 added for unicode  sr
        SetStrProp(Dest, PropName, GetStrProp(Source, PropName));
{$ENDIF}
      //tkClass, tkRecord, tkArray, tkDynArray, tkUnknown are ignored for now
    end;
  end;
end;

procedure RemoveStringFromList(const Remove: string; List: TStrings; DoTrim: Boolean = False);
var
  i: Integer;
  ListString: string;
begin
  for i := List.Count - 1 downto 0 do begin
    ListString := List[i];
    if DoTrim then
      ListString := Trim(ListString);
    if SameText(Remove, ListString) then
      List.Delete(i);
  end;
end;

procedure RemoveStringWithPrefixFromList(const Remove: string; List: TStrings; DoTrim: Boolean = False);
var
  i: Integer;
  ListString: string;
begin
  for i := List.Count - 1 downto 0 do begin
    ListString := List[i];
    if DoTrim then
      ListString := Trim(ListString);
    if StrBeginsWithText(Remove, ListString) then
      List.Delete(i);
  end;
end;

procedure ReplaceStringInList(const Old, New: string; List: TStrings; DoTrim: Boolean = False);
var
  i: Integer;
  ListString: string;
begin
  for i := List.Count - 1 downto 0 do begin
    ListString := List[i];
    if DoTrim then
      ListString := Trim(ListString);
    if SameText(Old, ListString) then
      List[i] := StringReplace(List[i], Old, New, [rfIgnoreCase]);
  end;
end;

procedure ReplaceSubStringToEndOfLineInList(const Old, New: string; List: TStrings;  CaseSensitive: Boolean = True);
var
  i: Integer;
begin
  Assert(Assigned(List));
  for i := 0 to List.Count - 1 do
    if (CaseSensitive and StrContains(Old, List[i])) or ((not CaseSensitive) and StrContainsText(Old, List[i])) then
      List[i] := LeadingWhiteSpace(List[i]) + New;
end;

procedure FreeStringListObjects(List: TStrings);
var
  i: Integer;
begin
  Assert(Assigned(List));
  for i := 0 to List.Count - 1 do begin
    if Assigned(List.Objects[i]) then begin
      List.Objects[i].Free;
      List.Objects[i] := nil;
    end;
  end;
end;

function SafeGetStringListString(Strings: TStrings; Index: Integer): string;
begin
  if Assigned(Strings) then begin
    if (Index < 0) or (Index >= Strings.Count) then
      Result := Format('Invalid index %d with Strings.Count of %d', [Index, Strings.Count])
    else
      Result := Strings[Index];
  end
  else
    Result := '<Not assigned>';
end;

function StringArraysHaveMatch(A1, A2: TStringArray): Boolean;
var
  i, j: Integer;
begin
  Result := True;
  for i := Low(A1) to High(A1) do begin
    for j := Low(A2) to High(A2) do
      if SameText(A1[i], A2[j]) then
        Exit;
  end;
  Result := False;
end;

function BuildFileName(const Path, FileName: string): string;
var
  FileNameHasDelimiter: Boolean;
  PathHasDelimiter: Boolean;
begin
  if Trim(Path) <> '' then
  begin
    PathHasDelimiter := RightStr(Path, 1) = PathDelim;
    FileNameHasDelimiter := LeftStr(FileName, 1) = PathDelim;
    if PathHasDelimiter and FileNameHasDelimiter then
      Result := ExcludeTrailingPathDelimiter(Path) + FileName
    else if PathHasDelimiter or FileNameHasDelimiter then
      Result := Path + FileName
    else // neither
      Result := IncludeTrailingPathDelimiter(Path) + FileName;
  end
  else
    Result := FileName;
end;

function ExtractFileNameOnly(FullPathName: string): string;
begin
  Result := ExtractFileName(FullPathName);
  Result := Copy(Result, 0, LastDelimiter('.', Result)-1);
end;

function AppendTextToFilename(FullPathName, AppendText:string; KeepPath: Boolean=True; KeepExt: Boolean=True):string;
begin
  Result := ExtractFileNameOnly(FullPathName) + AppendText;
  if KeepPath then
    Result := ExtractFilePath(FullPathName) + Result;
  if KeepExt then
    Result := Result + ExtractFileExt(FullPathName);
end;

function MakePath(const Path, DirectoryName: string): string;
begin
  Result := BuildFileName(Path, DirectoryName);
end;

// Discard the minutes/seconds/miliseconds for a given DateTime
function GetHoursOnly(const DateTime: TDatetime): TDatetime;
var
  AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: Word;
begin
  DecodeDateTime(DateTime, AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond);
  Result := EncodeDateTime(AYear, AMonth, ADay, AHour, 0, 0, 0);
end;

function Increase30Minutes(const DateTime: TDatetime): TDatetime;
begin
  Result := IncMinute(Datetime, 30);
end;

function Decrease30Minutes(const DateTime: TDateTime): TDatetime;
begin
  Result := IncMinute(Datetime, -30);
end;

function GetFileDateTime(const FileName: string): TDateTime;
begin
  {$IF CompilerVersion >= 16}
  if not FileAge(FileName, Result) then
    Result := FileDateToDateTime(-1);
  {$ELSE}
  Result := FileDateToDateTime(SysUtils.FileAge(FileName));
  {$IFEND}
end;

function IsFileInUse(FileName: String): Boolean;
var
  HFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then
    Exit;

  HFileRes := CreateFile(PChar(FileName),GENERIC_READ or GENERIC_WRITE,0,nil,
    OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);

  Result := (HFileRes = INVALID_HANDLE_VALUE);

  if not(Result) then
    CloseHandle(HFileRes);
end;

function IsWindowsVista: Boolean;
begin
  Result := Win32MajorVersion >= 6;
end;

procedure FreeAndNil2(var Obj:TObject; LogEvent: TLogEvent);
begin
  try
    FreeAndNil(Obj);
  except
    on E:Exception do begin
      if Assigned(LogEvent) then
        LogEvent(TObject(Obj), E.Message)
      else
        raise;
    end;
  end;
end;

function StringContainsAnyOfArray(const S: string; const A: array of string; var ElementMatched: Integer): Boolean;
var
  I: Integer;
begin
  ElementMatched := -1;
  for I := Low(A) to High(A) do
    if StrContainsText(A[I], S) then begin
      ElementMatched := I;
      Break;
    end;
  Result := ElementMatched > -1;
end;

function ForceForegroundWindow(hwnd: THandle): Boolean;
const
  SPI_GETFOREGROUNDLOCKTIMEOUT = $2000;
  SPI_SETFOREGROUNDLOCKTIMEOUT = $2001;
var
  ForegroundThreadID: DWORD;
  ThisThreadID : DWORD;
  timeout : DWORD;
begin
  if IsIconic(hwnd) then ShowWindow(hwnd, SW_RESTORE);

  if GetForegroundWindow = hwnd then Result := true
  else begin
    // Windows 98/2000 doesn't want to foreground a window when some other
    // window has keyboard focus
    if ((Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion > 4)) or
      ((Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and
      ((Win32MajorVersion > 4) or ((Win32MajorVersion = 4) and (Win32MinorVersion > 0)))) then
    begin
      // Code from Karl E. Peterson, www.mvps.org/vb/sample.htm
      // Converted to Delphi by Ray Lischner
      // Published in The Delphi Magazine 55, page 16
      Result := false;
      ForegroundThreadID := GetWindowThreadProcessID(GetForegroundWindow,nil);
      ThisThreadID := GetWindowThreadPRocessId(hwnd,nil);
      if AttachThreadInput(ThisThreadID, ForegroundThreadID, true) then begin
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hwnd);
        AttachThreadInput(ThisThreadID, ForegroundThreadID, false);
        Result := (GetForegroundWindow = hwnd);
      end;
      if not Result then begin
        // Code by Daniel P. Stasinski
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timeout, 0);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(0), SPIF_SENDCHANGE);
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hWnd);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(timeout), SPIF_SENDCHANGE);
      end;
    end
    else begin
      BringWindowToTop(hwnd); // IE 5.5 related hack
      SetForegroundWindow(hwnd);
    end;

    Result := (GetForegroundWindow = hwnd);
  end;
end; { ForceForegroundWindow }

procedure SendMessageToApplication(ClassName, WindowName: string; Message: Cardinal);
var
  aHandle: THandle;
begin
  aHandle := FindWindow(PChar(ClassName), PChar(WindowName));
  if aHandle <> 0 then
    PostMessage(aHandle, Message, 0, 0);
end;

function CheckCmdLineParameter(const Parameter: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do begin
    Result := SameText(ParamStr(i), Parameter);
    if Result then
      Break;
  end;
end;

function GetCmdLineParameterIndex(const Parameter: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 1 to ParamCount do begin
    if SameText(ParamStr(i), Parameter) then begin
      Result := i;
      Break;
    end;
  end;
end;

// TODO: There is likely a better way to do this
function GetCommandLineParams: string;
var
  i: Integer;
  Param: string;
begin
  Result := '';
  for i := 1 to ParamCount do begin
    Param := ParamStr(i);
    if StrContains(' ', Param) then
      Param := '"' + Param + '"';
    Result := Result + Param + ' ';
  end;
  Result := Trim(Result);
end;


function ValidateIntegerCommaSeparatedList(const List: string): Boolean;
var
  StringList: TStringList;
  i: Integer;
begin
  StringList := TStringList.Create;
  Result := False;
  try
    StringList.CommaText := List;
    for i := 0 to StringList.Count - 1 do begin
      Result := IsInteger(StringList[i]);
      if not Result then
        Break
    end;
  finally
    StringList.Free;
  end;
end;

function IsInteger(const Number: string): Boolean;
begin
  try
    StrToInt(Number);
    Result := True;
  except
    Result := False;
  end;
end;

function GetDomainUserName: string;
var
  Domain: string;
begin
  Result := GetLocalUserName;
  Domain := GetDomainName;
  if NotEmpty(Domain) then
    Result := Domain + '\' + Result;
end;

// http://www.howtodothings.com/computers/a1169-validating-email-addresses-in-delphi.html
function IsValidEmailAddress(const Email: string): Boolean;
// Returns True if the email address is valid
// Author: Ernesto D'Spirito
const
  // Valid characters in an "atom"
  atom_chars = [#33..#255] - ['(', ')', '<', '>', '@', ',', ';', ':',
                              '\', '/', '"', '.', '[', ']', #127];
  // Valid characters in a "quoted-string"
  quoted_string_chars = [#0..#255] - ['"', #13, '\'];
  // Valid characters in a subdomain
  letters = ['A'..'Z', 'a'..'z'];
  letters_digits = ['0'..'9', 'A'..'Z', 'a'..'z'];
  subdomain_chars = ['-', '0'..'9', 'A'..'Z', 'a'..'z'];
type
  States = (STATE_BEGIN, STATE_ATOM, STATE_QTEXT, STATE_QCHAR,
    STATE_QUOTE, STATE_LOCAL_PERIOD, STATE_EXPECTING_SUBDOMAIN,
    STATE_SUBDOMAIN, STATE_HYPHEN);
var
  State: States;
  i, n, subdomains: Integer;
  c: AnsiChar;
  AnsiEmail : AnsiString;
begin
  AnsiEmail := AnsiString(Email);
  State := STATE_BEGIN;
  n := Length(AnsiEmail);
  i := 1;
  subdomains := 1;
  while (i <= n) do begin
    c := AnsiEmail[i];
    case State of
    STATE_BEGIN:
      if c in atom_chars then
        State := STATE_ATOM
      else if c = '"' then
        State := STATE_QTEXT
      else
        break;
    STATE_ATOM:
      if c = '@' then
        State := STATE_EXPECTING_SUBDOMAIN
      else if c = '.' then
        State := STATE_LOCAL_PERIOD
      else if not (c in atom_chars) then
        break;
    STATE_QTEXT:
      if c = '\' then
        State := STATE_QCHAR
      else if c = '"' then
        State := STATE_QUOTE
      else if not (c in quoted_string_chars) then
        break;
    STATE_QCHAR:
      State := STATE_QTEXT;
    STATE_QUOTE:
      if c = '@' then
        State := STATE_EXPECTING_SUBDOMAIN
      else if c = '.' then
        State := STATE_LOCAL_PERIOD
      else
        break;
    STATE_LOCAL_PERIOD:
      if c in atom_chars then
        State := STATE_ATOM
      else if c = '"' then
        State := STATE_QTEXT
      else
        break;
    STATE_EXPECTING_SUBDOMAIN:
      if c in letters then
        State := STATE_SUBDOMAIN
      else
        break;
    STATE_SUBDOMAIN:
      if c = '.' then begin
        inc(subdomains);
        State := STATE_EXPECTING_SUBDOMAIN
      end else if c = '-' then
        State := STATE_HYPHEN
      else if not (c in letters_digits) then
        break;
    STATE_HYPHEN:
      if c in letters_digits then
        State := STATE_SUBDOMAIN
      else if c <> '-' then
        break;
    end;
    inc(i);
  end;
  if i <= n then
    Result := False
  else
    Result := (State = STATE_SUBDOMAIN) and (subdomains >= 2);
end;

// Replace with TStreamHelper.WriteString later...
procedure WriteStringToStream(Stream: TStream; const Str: string);
begin
  Assert(Assigned(Stream));
  if Length(Str) > 0 then
    Stream.WriteBuffer(Str[1], Length(Str) * SizeOf(Char));
end;

function SHRecycleFiles(const Files: string; var OpStatus: Integer): Boolean;
var
  FileOp: TSHFileOpStruct;
  Source: string;
begin
  FillChar(FileOp, SizeOf(FileOp), #0);
  with FileOp do begin
    Wnd := GetDesktopWindow;
    wFunc := FO_DELETE;
    Source := Files + #0#0;
    pFrom := PChar(Source);
    fFlags := (FOF_SILENT or FOF_NOCONFIRMATION or FOF_NOERRORUI or
      FOF_NOCONFIRMMKDIR or FOF_FILESONLY or FOF_ALLOWUNDO);
  end;
  OpStatus := SHFileOperation(FileOp);
  Result := (OpStatus = 0);
end;


procedure OdDeleteFile(const Path, FileSpec: string; const MinDaysOld: Integer;
  const Recursive: Boolean; const Recycle: Boolean);
var
  ErrorMsg: string;

  function DeleteWithRetry(const FileName: string): Boolean;
  var
    TimesTried: Integer;
    OpStatus: Integer;
  const
    RetryTimes = 3;
    WaitSeconds = 3;
  begin
    TimesTried := 0;
    repeat
      if Recycle then begin
        Result := SHRecycleFiles(FileName, OpStatus);
        if not Result then
          ErrorMsg := SysErrorMessage(OpStatus);
      end else begin
        Result := SysUtils.DeleteFile(FileName);
        if not Result then
          ErrorMsg := SysErrorMessage(GetLastError);
      end;
      Inc(TimesTried);
      if (not Result) and (TimesTried < RetryTimes) then
        Sleep(WaitSeconds * 1000);
    until Result or (TimesTried >= RetryTimes);
    if Result then
      ErrorMsg := '';
  end;

var
  LastWriteTime: TDateTime;
  FilesToDelete: TStringList;
  i: Integer;
  DeletedOk: Boolean;
  FileListOptions: TFileListOptions;
begin
  if Recursive then
    FileListOptions := [flFullNames, flRecursive]
  else
    FileListOptions := [flFullNames];

  FilesToDelete := TStringList.Create;
  try
    AdvBuildFileList(Path + FileSpec, faArchive, FilesToDelete, amSubSetOf, FileListOptions);
    DeletedOk := True;
    for i := 0 to FilesToDelete.Count - 1 do begin
      LastWriteTime := GetFileDateTime(FilesToDelete[i]);
      if DaysBetween(Now, LastWriteTime) > MinDaysOld then // Delete files older than MinDaysOld
        DeletedOk := DeleteWithRetry(FilesToDelete[i]) and DeletedOk;
    end;
    if not DeletedOk then
      raise Exception.CreateFmt('Cannot delete one or more files matching %s%s.'+LF+'%s', [Path, FileSpec, ErrorMsg]);
  finally
    FreeAndNil(FilesToDelete);
  end;
end;

function GetDiskSpace(DrvLtr: char): Int64;   //sr
var
  I: integer;
  space: Int64;
  testLtr: char;
begin
  Result := 0;
  for I := 2 to 6 do
  begin
    space := DiskFree(I);
    testLtr := Chr(I + 64);
    if ((space >= 0) and (DrvLtr = testLtr)) then
    begin
      Result := space;
      exit;
    end;
  end;
end;

procedure DeleteDirectory(const DirectoryName: string);
//Use the ShellAPI to remove a folder, it's subfolders and files
var
  FileOp: TSHFileOpStruct;
begin
  FillChar(FileOp, SizeOf(FileOp), 0);
  FileOp.wFunc := FO_DELETE;
  FileOp.pFrom := PChar(DirectoryName+#0);//double zero-terminated
  FileOp.fFlags := FOF_SILENT or FOF_NOERRORUI or FOF_NOCONFIRMATION;
  SHFileOperation(FileOp);
end;

function GetLastLine(MyFile: TFilename): string;  //SR
var
  a: TextFile;
  s: string;
begin
  assignfile(a, MyFile);
  reset(a);
  While not eof(a) do
    readln(a, s);
  closefile(a);
  Result := s;
end;

function GetVolumeName(DriveLetter: Char): string;  //sr
var
  dummy: DWORD;
  buffer: array[0..MAX_PATH] of Char;
  oldmode: LongInt;
begin
  oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
    GetVolumeInformation(PChar(DriveLetter + ':\'),
                         buffer,
                         SizeOf(buffer),
                         nil,
                         dummy,
                         dummy,
                         nil,
                         0);
    Result := StrPas(buffer);
  finally
    SetErrorMode(oldmode);
  end;
end;

function GetLocalAppDataFolder: string;
begin
  Result := GetSpecialFolderLocation(CSIDL_LOCAL_APPDATA);
end;

function GetUserProfileFolder: string;
begin
  Result := GetEnvironmentVariable('USERPROFILE');
end;

function SameHourAndMinute(Time1, Time2: TDateTime): Boolean;
begin
  Result := (HourOf(Time1) = HourOf(Time2)) and (MinuteOf(Time1) = MinuteOf(Time2));
end;

{$IFDEF ClassHelpers}

{ TStringListHelper }

function TStringsHelper.AddFmt(const Str: string; Args: array of const): Integer;
begin

  Result := Add(Format(Str, Args));
end;

procedure TStringsHelper.FreeAndNilObjects;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do begin
    if Assigned(Objects[i]) then begin
      Objects[i].Free;
      Objects[i] := nil;
    end;
  end;
end;

function TStringsHelper.AddInteger(const S: string; AInt: Integer): Integer;
begin
  Result := AddObject(S, TObject(Pointer(AInt)));
end;

function TStringsHelper.GetInteger(Index: Integer): Integer;
begin
  Result := Integer(pointer(GetObject(Index)));
end;

// Case insensitive
function TStringsHelper.Contains(const Str: string): Boolean;
begin
  Result := IndexOf(Str) >= 0;
end;

function TStringsHelper.ContainsSubstringText(Substrings: array of string): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  for i := Low(Substrings) to High(Substrings) do begin
    for j := 0 to Count - 1 do begin
      if StrContainsText(Substrings[i], Strings[j]) then begin
        Result := True;
        Break;
      end;
    end;
  end;
end;

function TStringsHelper.DelimitedLines(const Delimiter: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do begin
    if IsEmpty(Result) then
      Result := Result + Strings[i]
    else
      Result := Result + Delimiter + Strings[i];
  end;
end;

function TStringsHelper.AddStringCounted(const Str: string): Integer;
var
  Index: Integer;
begin
  Index := Self.IndexOf(Str);
  if Index >= 0 then begin
    Objects[Index] := TObject(Cardinal(Objects[Index]) + 1);
    Result := Index;
  end
  else
    Result := AddObject(Str, TObject(1));
end;

function SortStringsByObjectsValues(List: TStringList; Index1, Index2: Integer): Integer;
var
  Addr1, Addr2: Cardinal;
begin
  Addr1 := Cardinal(List.Objects[Index1]);
  Addr2 := Cardinal(List.Objects[Index2]);
  if Addr1 > Addr2 then
    Result := -1
  else if Addr1 < Addr2 then
    Result := 1
  else
    Result := 0;
end;

// NOTE: This changes the strings!
procedure TStringsHelper.SaveToFileCounted(const FileName: string);
var
  i: Integer;
begin
  if Self is TStringList then
    (Self as TStringList).CustomSort(SortStringsByObjectsValues);
  for i := 0 to Count - 1 do
    Strings[i] := IntToStr(Cardinal(Objects[i])) +#09+ Strings[i];
  SaveToFile(FileName);
end;

function TStringsHelper.Pop(Num: Integer): string;
var
  Value: string;
  i: Integer;
begin
  Result := '';
  for i := 0 to Num - 1 do begin
    if Count > 0 then begin
      Value := Strings[0];
      Delete(0);
      if NotEmpty(Value) then begin
        if IsEmpty(Result) then
          Result := Value
        else
          Result := Result + ', ' + Value;
      end;
    end;
  end;
end;

{ TStreamHelper }

function TStreamHelper.AsAnsiString: AnsiString;
var
  StreamString: String;
begin
  SetLength(StreamString, Size div SizeOf(Char));
  Position := 0;
  ReadBuffer(StreamString[1], Size);
  Result := AnsiString(StreamString);
end;

procedure TStreamHelper.WriteAnsiString(const Str: AnsiString);
begin
  if Length(Str) > 0 then
    WriteBuffer(Str[1], Length(Str) * SizeOf(AnsiChar));
end;

function TStreamHelper.AsString: string;
begin
  SetLength(Result, Size div SizeOf(Char));
  Position := 0;
  ReadBuffer(Result[1], Size);
end;

procedure TStreamHelper.WriteString(const Str: string);
begin
  if Length(Str) > 0 then
    WriteBuffer(Str[1], Length(Str) * SizeOf(Char));
end;

procedure TStreamHelper.WriteUTF8String(const Str: string);
var
  UTF8Str : UTF8String;
begin
  if Length(Str) > 0 then begin
    UTF8Str := UTF8Encode(Str);
    WriteBuffer(UTF8Str[1], Length(UTF8Str));
  end;
end;

procedure TStreamHelper.WriteStringBytes(const Str: string{$IFDEF UNICODE};QMEncodingType: TQMEncodingType{$ENDIF});
var
  StrBytes: TBytes;
begin
  if Length(Str) > 0 then begin
    SetLength(StrBytes, Length(Str) {$IFDEF UNICODE}*  IfThen(QMEncodingType=etUTF8,SizeOf(Char),SizeOf(AnsiChar)){$ENDIF});
    StrBytes := StringToBytes(Str{$IFDEF UNICODE},QMEncodingType{$ENDIF});

    Write(StrBytes[0],Length(StrBytes));
  end;
end;

{ TRectHelper }

function TRectHelper.Height: Integer;
begin
  Result := Bottom - Top;
end;

function TRectHelper.Width: Integer;
begin
  Result := Right - Left;
end;

{TFileStreamHelper}

function TFileStreamHelper.Eof: Boolean;
begin
  Result := (Size = Position);
end;

function TFileStreamHelper.ReadLine: AnsiString;
const
  LineBufferSize = 10 * 1024;
var
  Ch: AnsiChar;
  BytesRead: Longint;
  NextCharPos: Longint;
  CharSize: Integer;
begin
  CharSize := SizeOf(AnsiChar);
  SetLength(Result, LineBufferSize * CharSize);
  NextCharPos := 1;
  BytesRead := Read(Ch, CharSize);
  while (BytesRead <> 0) and (Ch <> LF) do begin
    if (Ch <> CR) then begin
      Result[NextCharPos] := Ch;
      Inc(NextCharPos);
      if NextCharPos > Length(Result) then
        SetLength(Result, Length(Result) * 2);
    end;
    BytesRead := Read(Ch, CharSize);
  end;
  SetLength(Result, NextCharPos - 1);
end;

procedure TFileStreamHelper.WriteBytes(sBytes: TBytes{$IFDEF UNICODE};QMEncodingType: TQMEncodingType{$ENDIF});
var
{$IFDEF UNICODE}
  s: string;
{$ELSE}
  sAnsi: AnsiString;
{$ENDIF}
begin
{$IFDEF UNICODE}
    if QMEncodingType = etUTF8 then begin
      s := TEncoding.Unicode.GetString(sBytes); //Must retreive in utf16 since was written in utf16; i.e. trying to retrieve directly to utf8string does not work here
      WriteUTF8String(s); //Now utf8encode the utf16 string we got back
    end
    else begin
      Write(sBytes[0],Length(sBytes) {$IFDEF UNICODE}div IfThen(QMEncodingType=etUTF8,SizeOf(Char),SizeOf(AnsiChar)){$ENDIF});//Write Ansi encoded bytes to the file; these are unicode bytes so consider char size.
    end;
{$ELSE}//D2007 does not have TEncoding
    SetString(sAnsi, PAnsiChar(@sBytes[0]),Length(sBytes));
    WriteAnsiString(sAnsi);
{$ENDIF}
end;

{$ENDIF ClassHelpers}


function StringsToStringArray(const Strings: TStrings): TStringArray;
var
  i: Integer;
begin
  Assert(Assigned(Strings));
  SetLength(Result, Strings.Count);
  for i := 0 to Strings.Count - 1 do
    Result[i] := Strings[i];
end;

// The input is in degrees and the output is in kilometers.  Calculations use the
// Haversine formula: http://en.wikipedia.org/wiki/Haversine_formula
// This assumes a spherical earth rather that a more accurate WGS-84 elipsoid,
// but there isn't a large difference for paths that do not cover
// larger portions of the earth
function LatLongDistanceKM(Lat1, Lon1, Lat2, Lon2: Extended): Extended;
const
  EarthRadius = 6371; // In km, using FAI spherical earth model
var
  RLat1, RLon1, RLat2, RLon2: Extended;
begin
  // Convert inputs from degrees to radians
  RLat1 := Math.DegToRad(Lat1);
  RLon1 := Math.DegToRad(Lon1);
  RLat2 := Math.DegToRad(Lat2);
  RLon2 := Math.DegToRad(Lon2);
  Result := EarthRadius * 2 *
    ArcSin(Sqrt(Math.Power(Sin((RLat2 - RLat1)/2), 2)
    + Cos(RLat1) * Cos(RLat2) * Math.Power(Sin((RLon2 - RLon1)/2), 2)));
end;

// Vincenty Inverse Solution of Geodesics on the Ellipsoid (c) Chris Veness 2002-2008, edited to suit Delphi 5 by Doug Hunter VK4ADC 2009
// Calculate geodesic distance (in m) between two points specified by latitude/longitude
// (in numeric degrees) using Vincenty inverse formula for ellipsoids
// The ellipsoid in this case is the World Geodetic System of 1984 (WGS-84) standard
// From: http://www.vk4adc.com/gridlocw.php  The bearing calculations were commented out.
function LatLongDistanceMeters(Lat1, Lon1, Lat2, Lon2: Real): Real;
var
  a, b, c, f, U1, U2, uSq, sinU1, sinU2, CosU1, CosU2, lambda, lambdap, sinlambda, sinsigma, coslambda, cossigma, sigma, sinalpha, cosSqalpha, cos2sigmam, deltasigma: double;
  l, aa, bb, s, rads, a1, a2: Real;
  iterlimit: Integer;
begin
  //azim := 0;
  rads := pi / 180;
  a := 6378137; // Mean radius in m
  b := 6356752.314245;
  f := 1 / 298.257223563; // WGS-84 ellipsoid
  L := (lon2 - lon1) * rads;
  U1 := arctan((1 - f) * Math.tan(lat1 * rads));
  U2 := arctan((1 - f) * Math.tan(lat2 * rads));
  sinU1 := sin(U1);
  cosU1 := cos(U1);
  sinU2 := sin(U2);
  cosU2 := cos(U2);
  cos2SigmaM := 0;
  cosSigma := 0;
  sinSigma := 0;
  cosSqAlpha := 0;
  sigma := 0;
  Result := 0;

  lambda := L;
  lambdaP := 100;
  iterLimit := 100;
  while (abs(lambda - lambdaP) > 1E-12) and (iterLimit > 0) do
  begin
    sinLambda := sin(lambda);
    cosLambda := cos(lambda);
    sinSigma := sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) +
      (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
    if (sinSigma = 0) then
      exit;
    cosSigma := sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
    sigma := arctan2(sinSigma, cosSigma);
    sinAlpha := cosU1 * cosU2 * sinLambda / sinSigma;
    cosSqAlpha := 1 - sinAlpha * sinAlpha;
    if (sinU1 <> 0) and (sinU2 <> 0) and (cosSqAlpha <> 0) then
      cos2SigmaM := cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha
    else
      cos2SigmaM := cosSigma;
    if cos2SigmaM = 0 then
      exit; // Equatorial line: cosSqAlpha=0 (�6)
    C := f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
    lambdaP := lambda;
    lambda := L + (1 - C) * f * sinAlpha *
      (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

    dec(iterlimit);
  end;

  if (iterLimit = 0) then
    result := 0
  else
  begin // Formula failed to converge
    uSq := cosSqAlpha * (a * a - b * b) / (b * b);
    AA := 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    BB := uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
    deltaSigma := BB * sinSigma * (cos2SigmaM + BB / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
      BB / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
    s := b * AA * (sigma - deltaSigma);

    // Calculate forward and reverse azimuth/angles in degrees:
    //a1 := arctan2(cosU2 * sinlambda, cosU1 * sinU2 - sinU1 * cosU2 * coslambda);
    //a2 := arctan2(cosU1 * sinlambda, -sinU1 * cosU2 + cosU1 * sinU2 * coslambda);
    // same lines as above but with different variable names
    // fwdAz = arctan2(cosU2*sinLambda, cosU1*sinU2-sinU1*cosU2*cosLambda);
    // revAz = arctan2(cosU1*sinLambda, -sinU1*cosU2+cosU1*sinU2*cosLambda);
    //azim := a1;  // Forward azimuth in degrees, only really need this one

    result := s; // Distance in metres (/1000 for km)
  end;
end;

function FileToOleVariant(Filename: string): OleVariant;
var
  AStream: TFileStream;
  MyBuffer: Pointer;
begin
  AStream := TFileStream.Create(FileName, fmOpenRead );
  try
    Result := VarArrayCreate( [0, AStream.Size - 1], VarByte );
    MyBuffer := VarArrayLock( Result );
    AStream.ReadBuffer( MyBuffer^, AStream.Size );
    VarArrayUnlock( Result );
  finally
    FreeAndNil(AStream);
  end;
end;

procedure OleVariantToFile(const OV: OleVariant; Filename: string);
var
  Data: PByteArray;
  Size: integer;
  FS: TFileStream;
begin
  FS := TFileStream.Create(Filename, fmCreate);
  try
    Size := VarArrayHighBound (OV, 1) - VarArrayLowBound
    (OV, 1) + 1;
    Data := VarArrayLock(OV);
    try
      FS.WriteBuffer(Data^, Size);
    finally
      VarArrayUnlock(OV);
    end;
  finally
    FreeAndNil(FS);
  end;
end;

function StreamToOleVariant(AStream: TStream): OleVariant;
var
  MyBuffer: Pointer;
begin
  Assert(Assigned(AStream));
  try
    AStream.Seek(0,0);
    Result := VarArrayCreate( [0, AStream.Size - 1], VarByte );
    MyBuffer := VarArrayLock( Result );
    try
      AStream.ReadBuffer( MyBuffer^, AStream.Size );
    finally
      VarArrayUnlock( Result );
    end;
  finally
  end;
end;

function AppVersionShort: string;
var
  n: Integer;
begin
  n := Pos('-', AppVersionLong);
  if n > 0 then
    Result := Copy(AppVersionLong, 1, n + 7)
  else
    Result := AppVersionLong;
end;

function EscapeLineBreaks(const Str: string): string;
{$if CompilerVersion >= 20}
var
  SB: TStringBuilder;
{$ifend}
begin
{$if CompilerVersion >= 20}
  // TStringBuilder is much faster than StringReplace()
  SB := TStringBuilder.Create;
  try
    SB.Append(Str);
    SB.Replace(CR, '');
    SB.Replace(LF, '\n');
    Result := SB.ToString;
  finally
    FreeAndNil(SB);
  end;
{$else}
  Result := StringReplace(StringReplace(Str, CR, '' ,[rfReplaceAll]), LF, '\n' ,[rfReplaceAll]);
{$ifend}
end;

{ TOdStringList }

procedure TOdStringList.Union(const StringsToAdd: TStringArray);
var
  i: Integer;
begin
  for i := Low(StringsToAdd) to High(StringsToAdd) do begin
    if Self.IndexOf(StringsToAdd[i]) < 0 then //no match exists already
      Self.Add(StringsToAdd[i]);
  end;
end;

procedure TOdStringList.Union(const StringsToAdd: TStringList);
begin
  Union(StringsToStringArray(StringsToAdd));
end;

procedure TOdStringList.Intersect(const StringsToMatch: TStringArray);
var
  i: Integer;
  MatchList: TStringList;
begin
  MatchList := TStringList.create;
  try
    for i := Low(StringsToMatch) to High(StringsToMatch) do begin
      if Self.IndexOf(StringsToMatch[i]) > -1 then
        MatchList.Add(StringsToMatch[i]);
    end;
    Self.Assign(MatchList);
  finally
    FreeAndNil(MatchList);
  end;
end;

procedure TOdStringList.Intersect(const StringsToMatch: TStringList);
begin
  Intersect(StringsToStringArray(StringsToMatch));
end;

procedure TOdStringList.SymmetricDifference(const StringsToDifference: TStringArray);
//http://math.comsci.us/sets/symmetric.html
//http://rosettacode.org/wiki/Symmetric_difference
var
  i: Integer;
  DifferenceList: TStringList;
  FoundIndex: Integer;
begin
  DifferenceList := TStringList.create;
  try
    DifferenceList.Assign(Self);//self's items

    for i := Low(StringsToDifference) to High(StringsToDifference) do begin
      FoundIndex := DifferenceList.IndexOf(StringsToDifference[i]);
      if FoundIndex = -1 then //string is not in the diff list, so add it
        DifferenceList.Add(StringsToDifference[i])
      else begin//Item is in both lists, remove it
        DifferenceList.Delete(FoundIndex);
      end;
    end;
    Self.Assign(DifferenceList);
  finally
    FreeAndNil(DifferenceList);
  end;
end;

procedure TOdStringList.SymmetricDifference(const StringsToDifference: TStringList);
begin
  SymmetricDifference(StringsToStringArray(StringsToDifference));
end;

{$IFDEF UNICODE}
function GetQMEncoding(QMEncodingType: TQMEncodingType): TEncoding;
//Return the TEncoding type for the given QM encoding type enum.
//QM emits ANSI for legacy systems that require it; otherwise UTF-8 is used.
begin
  if QMEncodingType = etANSI then
    Result := TEncoding.ANSI
  else
    Result := TEncoding.UTF8;
end;
{$ENDIF}

function GetDailyFileName(Directory: string; FileName:string; InstanceId: Integer = 0): string;
var
  Ext: string;
  Fn: string;
  LocalSettings: TFormatSettings;
  InstId: string;
begin
{$IF CompilerVersion >= 22}
  LocalSettings := TFormatSettings.Create;
{$ELSE}
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, LocalSettings);
{$IFEND}
  Ext := ExtractFileExt(FileName);
  Fn := StringReplace(FileName, Ext, '', [rfReplaceAll]);
  if InstanceId > 0 then
    InstId := IntToStr(InstanceId);

  if NotEmpty(InstId) then
    Result := Directory + Fn + '-' + FormatDateTime('yyyy-mm-dd',Now, LocalSettings) + '-' + InstId + Ext
  else
    Result := Directory + Fn + '-' + FormatDateTime('yyyy-mm-dd',Now, LocalSettings) + Ext;
end;

function StringToBytes(AString: string{$IFDEF UNICODE};QMEncodingType: TQMEncodingType{$ENDIF}): TBytes;
begin
{$IFDEF UNICODE}
  if QMEncodingType = etAnsi then
    Result := TEncoding.ANSI.GetBytes(AString)
  else
    Result := TEncoding.Unicode.GetBytes(AString);
{$ELSE}//D2007 does not have TEncoding. Copy the string to TBytes. This logic can be removed once upgrade from D2007 is complete.
  SetLength(Result,Length(AString));
  Move(AString[1],Result[0],Length(AString));
{$ENDIF}
end;

//Elana - This is also done for Notes as a stored procedure.
function CleanRemarks(var Remarks: string): string;  {CleanNotes: Can also be used for notes}

  function SmartReplace(var pRemarks: string; ReplaceThis:string; WithThis:string): string;
  begin
    if ReplaceThis = WithThis then
      exit;

    {Make sure we don't add too many spaces, so we will look for them first}
    pRemarks := StringReplace(pRemarks, ' ' + ReplaceThis + ' ', ' ' + WithThis + ' ', [rfReplaceAll]);
    pRemarks := StringReplace(pRemarks, ' ' + ReplaceThis,       ' ' + WithThis + ' ', [rfReplaceAll]);
    pRemarks := StringReplace(pRemarks, ReplaceThis + ' ',       ' ' + WithThis + ' ', [rfReplaceAll]);

    {Only hits this if there were no spaces}
    pRemarks := StringReplace(pRemarks, ReplaceThis,             ' ' + WithThis + ' ', [rfReplaceAll]);

    Result := pRemarks;
  end;

begin
//rfReplaceAll, rfIgnoreCase
  Remarks := SmartReplace(Remarks, '+', 'PLUS');
  Remarks := SmartReplace(Remarks, '&', 'AND');
  Remarks := SmartReplace(Remarks, '@', 'AT');
  Remarks := SmartReplace(Remarks, '!', '');
  Remarks := SmartReplace(Remarks, '#', 'NUMBER ');
  Remarks := SmartReplace(Remarks, '$', '');
  Remarks := SmartReplace(Remarks, '[', '');
  Remarks := SmartReplace(Remarks, ']', '');
  Remarks := SmartReplace(Remarks,'%', ' PERCENT');
  Result := Remarks;
end;

function UTCToSystemTime(UTC: TDateTime): TDateTime;
var
  TimeZoneInf: _TIME_ZONE_INFORMATION;
  UTCTime, LocalTime: TSystemTime;
begin
  if GetTimeZoneInformation(TimeZoneInf) > 0 then
  begin
    DatetimetoSystemTime(UTC, UTCTime);
    if SystemTimeToTzSpecificLocalTime(@TimeZoneInf, UTCTime, LocalTime) then
    begin
      result := SystemTimeToDateTime(LocalTime);
    end
    else
      result := UTC;
  end
  else
    result := UTC;
end;

function RemoveCharsFromStr(const S: string; const Chars: array of Char): string;
{EB: This is identical to JCL Strings, but local to our function}
begin
 {JCL call}
  Result := StrRemoveChars(S, Chars);
end;

function ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
  {SR: though more complicated, this is more powerful than ShellExecute.
  It will also pass thru Windows GetLastError.
  When running a batch file, it will catch batch file errors.  It does not use ActiveX}
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
      repeat
        Application.ProcessMessages;
      until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

function GetBusType(Drive: AnsiChar): TStorageBusType;     //qm-164
var
  H: THandle;
  Query: TStoragePropertyQuery;
  dwBytesReturned: DWORD;
  Buffer: array [0..1023] of Byte;
  sdd: TStorageDeviceDescriptor absolute Buffer;
  OldMode: UINT;
begin
  Result := BusTypeUnknown;

  OldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
    H := CreateFile(PChar(Format('\\.\%s:', [AnsiLowerCase(Drive)])), 0, FILE_SHARE_READ or FILE_SHARE_WRITE, nil,
      OPEN_EXISTING, 0, 0);
    if H <> INVALID_HANDLE_VALUE then
    begin
      try
        dwBytesReturned := 0;
        FillChar(Query, SizeOf(Query), 0);
        FillChar(Buffer, SizeOf(Buffer), 0);
        sdd.Size := SizeOf(Buffer);
        Query.PropertyId := StorageDeviceProperty;
        Query.QueryType := PropertyStandardQuery;
        if DeviceIoControl(H, IOCTL_STORAGE_QUERY_PROPERTY, @Query, SizeOf(Query), @Buffer, SizeOf(Buffer), dwBytesReturned, nil) then
          Result := sdd.BusType;
      finally
        CloseHandle(H);
      end;
    end;
  finally
    SetErrorMode(OldMode);
  end;
end;

function GetHWndByPID(const hPID: THandle): THandle;
// Get Window Handle By ProcessID
type
   PEnumInfo = ^TEnumInfo;
   TEnumInfo = record
      ProcessID: DWORD;
      HWND: THandle;
   end;
   function EnumWindowsProc(Wnd: DWORD; var EI: TEnumInfo): Bool; stdcall;
   var
      PID: DWORD;
   begin
      GetWindowThreadProcessID(Wnd, @PID);
      Result := (PID <> EI.ProcessID) or
         (not IsWindowVisible(WND)) or
         (not IsWindowEnabled(WND));
      if not Result then EI.HWND := WND; //break on return FALSE
   end;
   
   function FindMainWindow(PID: DWORD): DWORD;
   var
      EI: TEnumInfo;
   begin
      EI.ProcessID := PID;
      EI.HWND := 0;
      EnumWindows(@EnumWindowsProc, Integer(@EI));
      Result := EI.HWND;
   end;
begin
   if hPID <> 0 then
      Result := FindMainWindow(hPID)
   else
      Result := 0;
end;


// Get ProcessID By ProgramName (Include Path or Not Include)
function GetPIDByProgramName(const APName: string): THandle;
var
   isFound: boolean;
   AHandle, AhProcess: THandle;
   ProcessEntry32: TProcessEntry32;
   APath: array[0..MAX_PATH] of char;
begin
   Result := 0;
   AHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
   try
      ProcessEntry32.dwSize := Sizeof(ProcessEntry32);
      isFound := Process32First(AHandle, ProcessEntry32);
      while isFound do
      begin
         AhProcess := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,
            false, ProcessEntry32.th32ProcessID);
         GetModuleFileNameEx(AhProcess, 0, @APath[0], sizeof(APath));
         if (UpperCase(StrPas(APath)) = UpperCase(APName)) or
            (UpperCase(StrPas(ProcessEntry32.szExeFile)) = UpperCase(APName)) then
         begin
            Result := ProcessEntry32.th32ProcessID;
            break;
         end;
         isFound := Process32Next(AHandle, ProcessEntry32);
         CloseHandle(AhProcess);
      end;
   finally
      CloseHandle(AHandle);
   end;
end;

// Activate a window by its handle
function AppActivate(WindowHandle: HWND): boolean; overload;
begin
   try
      SendMessage(WindowHandle, WM_SYSCOMMAND, SC_HOTKEY, WindowHandle);
      SendMessage(WindowHandle, WM_SYSCOMMAND, SC_RESTORE, WindowHandle);
      result := SetForegroundWindow(WindowHandle);
   except
      on Exception do Result := false;
   end;
end;

// Get Window Handle By ProgramName (Include Path or Not Include)
function GetHWndByProgramName(const APName: string): THandle;
begin
   Result := GetHWndByPID(GetPIDByProgramName(APName));
end;





initialization
  Randomize;

end.

