unit OdRbDownloadedImage;

interface

uses SysUtils, Classes, DB, ppCtrls, ppMemo;

procedure ShowDownloadedImage(DownloadedFileList, ErrorLog: TStringList; Attachments: TDataSet; ppImage: TppImage; ppMemoErrorMessage: TppMemo);
function FileNameToDownloadedPath(DownloadedFileList: TStrings; FileName: TFileName): string;

implementation

uses
  OdMiscUtils, JclSysInfo;

procedure ShowDownloadedImage(DownloadedFileList, ErrorLog: TStringList;
            Attachments: TDataSet; ppImage: TppImage; ppMemoErrorMessage: TppMemo);
var
  FileName: string;

  procedure ShowAttachmentError(const Msg: string);
  begin
    ppImage.Visible := False;
    ppMemoErrorMessage.Visible := True;
    ppMemoErrorMessage.Lines.Text := 'Unable to print file ' + ExtractFileName(Filename);
    ppMemoErrorMessage.Lines.Add('Local Path: ' + FileName);
    if FileExists(FileName) then
      ppMemoErrorMessage.Lines.Add('File Size: ' + IntToStr(GetFileSize(FileName)) + ' bytes');
    if NotEmpty(Msg) then
      ppMemoErrorMessage.Lines.Add(Msg);
    if NotEmpty(SafeGetStringListString(ErrorLog, DownloadedFileList.IndexOf(Filename))) then
      ppMemoErrorMessage.Lines.Add('Download Error: ' + SafeGetStringListString(ErrorLog, DownloadedFileList.IndexOf(Filename)));
    ppMemoErrorMessage.Lines.Add('CGI User: ' + GetLocalUserName);
  end;

begin
  Assert(Assigned(DownloadedFileList));
  Assert(Assigned(ErrorLog));
  FileName := FileNameToDownloadedPath(DownloadedFileList, Attachments.FieldByName('orig_filename').AsString);
  if NotEmpty(FileName) then begin
    if FileExists(FileName) and (GetFileSize(FileName) > 0) then begin
      ppImage.Visible := True;
      ppImage.Stretch := False;
      try
        ppImage.Picture.LoadFromFile(FileName);
      except
        on E: Exception do begin
          ShowAttachmentError('Corrupt Image: ' + E.ClassName + ': ' + E.Message);
          Exit;
        end;
      end;
      ppImage.Stretch := (ppImage.Picture.Width > ppImage.Width) or (ppImage.Picture.Height > ppImage.Height);
      ppMemoErrorMessage.Visible := False;
    end
    else
      ShowAttachmentError('File does not exist or is 0 bytes');
  end;
end;

function FileNameToDownloadedPath(DownloadedFileList: TStrings; FileName: TFileName): string;
var
  i: Integer;
  CurrentFile: String;
begin
  Assert(Assigned(DownloadedFileList));
  Assert(FileName <> '');
  Result := '';
  for i := 0 to DownloadedFileList.Count - 1 do begin
    CurrentFile := ExtractFileName(DownloadedFileList[i]);
    if CurrentFile = FileName then begin
      Result := DownloadedFileList[i];
      Break;
    end;
  end;
end;

end.
