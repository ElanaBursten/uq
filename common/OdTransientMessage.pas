unit OdTransientMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseFU, StdCtrls, ExtCtrls;

type
  TTransientMessageForm = class(TBaseForm)
    ButtonPanel: TPanel;
    CloseButton: TButton;
    MsgLabel: TLabel;
    HideTimer: TTimer;
    procedure HideTimerTimer(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
  private
    procedure SizeToMessage;
  public
    class procedure Show(const Caption, Msg: string; Seconds: Integer = 2; Font: TFont = nil);
  end;

implementation

{$R *.dfm}

{ TTransientMessageForm }

class procedure TTransientMessageForm.Show(const Caption, Msg: string; Seconds: Integer; Font: TFont);
var
  Dialog: TTransientMessageForm;
begin
  Dialog := TTransientMessageForm.Create(nil);
  try
    Dialog.Caption := Caption;
    Dialog.MsgLabel.Caption := Msg;
    if Assigned(Font) then
      Dialog.MsgLabel.Font.Assign(Font);
    Dialog.SizeToMessage;
    Dialog.HideTimer.Interval := 1000 * Seconds;
    Dialog.HideTimer.Enabled := True;
    Dialog.ShowModal;
  finally
    FreeAndNil(Dialog);
  end;
end;

procedure TTransientMessageForm.HideTimerTimer(Sender: TObject);
begin
  HideTimer.Enabled := False;
  ModalResult := mrCancel;
end;

procedure TTransientMessageForm.CloseButtonClick(Sender: TObject);
begin
  Self.ModalResult := mrCancel;
end;

procedure TTransientMessageForm.SizeToMessage;
begin
  // Not yet implemented
end;

end.
