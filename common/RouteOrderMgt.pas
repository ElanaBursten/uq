unit RouteOrderMgt;
{5/11/2020 EB - Moving some of the route order management structures, classes
                functionality to this new unit}
{2/3/2023 EB QM-697 EB Resurrect Route Order - recording the reason a ticket was
                done out of order}

interface
uses Classes, SysUtils, OdMiscUtils, DB, QMConst;
                                                                           
const
    maxRORec = 4;

type
  {TRouteOrderRec is for the RouteOrderOverride}
  TRouteOrderOverrideRec = record    //QMANTWO-775 EB  {moved to this unit for QM-166 EB}
    Found: Boolean;
    OrderNum: Double;
    TicketID: Integer;
    TicketNum: string;
    DueDate: TDateTime;
    ReasonCode: string;   //Ref code
    ReasonStr: string;    //Desc
  end;

  TRouteArray = Array[0..maxRORec] of TRouteOrderOverrideRec;  //QMANTWO-775 EB
  {------------------------------------------------------------------------}
  {--- RouteOrderObj -------QM-166 RO Edit Short version-------------------}
  TRouteOrderSet = record
    IsSet: Boolean;
    OrderNum: Double;
    TicketID: Integer;
  end;


  TDyRouteArray = Array of TRouteOrderSet;

  TRouteOrderObj = Class(TObject)
  private
    HighOrderNum: integer;
    NumberOfRecords: integer;
    fRouteArray: TDyRouteArray;      {dynamic}
  public
    property RouteArray: TDyRouteArray read fRouteArray write fRouteArray;
    constructor Create;
    procedure Clear(TicketID: Integer);
    procedure ClearArray;
    procedure ScanRouteOrders(AListTable: TDataSet);
    destructor Destroy;
    procedure Next(TicketID: Integer);
    function SetRO(TicketID: Integer; RouteOrder: Double; var Msg: string): boolean;
  end;

  {General methods}
  function GetEarlierRouteOrder(pTicketID: Integer): TRouteOrderOverrideRec; //QMANTWO-775 EB
  function GetEarlierRouteOrderArray(pTicketID: Integer; CurRouteOrder: Double; var Count: integer): TRouteArray;
  function SetRouteOrderReason(ParentTicketID: integer; ParentTicketNum: string; RouteOrderInfo: TRouteOrderOverrideRec): Boolean; //QMANTWO-775 EB
  function GetSavedROTicketInfo(ParentTicketID: integer): boolean;

var
  RouteOrderObj : TRouteOrderObj;


implementation
uses DMu;


procedure TRouteOrderObj.CLearArray;
begin
  SetLength(fRouteArray, 0);
  HighOrderNum := 0;
end;

constructor TRouteOrderObj.Create;
begin
  inherited;
  NumberOfRecords := 0;
  HighOrderNum := 0;
end;

function GetEarlierRouteOrder(pTicketID: Integer): TRouteOrderOverrideRec;   //QMANTWO-775 EB
begin
  Result.Found := False;
  Result.OrderNum  := 0;
  Result.TicketID := 0;
  Result.TicketNum := '';
  Result.DueDate:= Now;

  DM.RouteOrderQuery.Close;
  DM.RouteOrderQuery.ParamByName('emp_id').AsInteger := DM.EmpID;
  DM.RouteOrderQuery.ParamByName('last_sync').AsDateTime := DM.UQState.LastLocalSyncTime;
  DM.RouteOrderQuery.ParamByName('workpriority').AsInteger := DM.GetTicketPriorityIDByCode(TicketPrioritySkipped);  //QM-720 Route Order Skipped EB
  DM.RouteOrderQuery.Open;

  DM.RouteOrderQuery.First;
  if ((DM.RouteOrderQuery.FieldByName('ticket_id').AsInteger) <> pTicketID) then begin
    Result.Found := True;
    Result.OrderNum := DM.RouteOrderQuery.FieldByName('route_order').AsFloat;
    Result.TicketID := DM.RouteOrderQuery.FieldByName('ticket_id').AsInteger;
    Result.TicketNum := DM.RouteOrderQuery.FieldByName('ticket_number').AsString;
    Result.DueDate := DM.RouteOrderQuery.FieldByName('due_date').AsDateTime;
  end;

end;

function GetEarlierRouteOrderArray(pTicketID: Integer; CurRouteOrder: Double; var Count: integer): TRouteArray;
var
 i: integer;
 SkipPriority: integer;
begin
  {QMANTWO-775 EB We will only record the first 5 records' data that has been skipped}
  {init}
  for i := 0 to maxRORec do begin
    Result[i].Found := False;
    Result[i].OrderNum  := 0;
    Result[i].TicketID := 0;
    Result[i].TicketNum := '';
    Result[i].DueDate:= Now;
  end;
  SkipPriority := DM.GetTicketPriorityIDByCode(TicketPrioritySkipped);
  DM.RouteOrderQuery.Close;
  DM.RouteOrderQuery.ParamByName('emp_id').AsInteger := DM.EmpID;
  DM.RouteOrderQuery.ParamByName('last_sync').AsDateTime := DM.UQState.LastLocalSyncTime;
  DM.RouteOrderQuery.ParamByName('route_order').AsFloat := CurRouteOrder;
  DM.RouteOrderQuery.ParamByName('workpriority').AsInteger := DM.GetTicketPriorityIDByCode(TicketPrioritySkipped);  //QM-720 Route Order Skipped EB
  DM.RouteOrderQuery.Open;
  Count := DM.RouteOrderQuery.RecordCount;

  DM.RouteOrderQuery.First;

  for i := 0 to maxRORec do begin
    if (not DM.RouteOrderQuery.Eof) and ((DM.RouteOrderQuery.FieldByName('ticket_id').AsInteger) <> pTicketID) then begin
      Result[i].Found := True;
      Result[i].OrderNum := DM.RouteOrderQuery.FieldByName('route_order').AsFloat;
      Result[i].TicketID := DM.RouteOrderQuery.FieldByName('ticket_id').AsInteger;
      Result[i].TicketNum := DM.RouteOrderQuery.FieldByName('ticket_number').AsString;
      Result[i].DueDate := DM.RouteOrderQuery.FieldByName('due_date').AsDateTime;
      DM.RouteOrderQuery.Next;
    end;
  end;
end;


function GetSavedROTicketInfo(ParentTicketID: integer): boolean;
begin
  DM.QryTicketInfoRouteOrder.Close;
  DM.QryTicketInfoRouteOrder.ParamByName('ticket_id').AsInteger := ParentTicketID;
  DM.QryTicketInfoRouteOrder.Open;
  Result := (DM.QryTicketInfoRouteOrder.RecordCount > 0)
end;


function SetRouteOrderReason(ParentTicketID: integer;
  ParentTicketNum: string; RouteOrderInfo: TRouteOrderOverrideRec): Boolean;
begin
  try
    DM.InsertRouteOrderInfo.ParamByName('ticket_info_id').AsInteger := DM.Engine.GenTempKey;
    DM.InsertRouteOrderInfo.ParamByName('ticket_id').AsInteger := ParentTicketID; //RouteOrderInfo.TicketID;
    DM.InsertRouteOrderInfo.ParamByName('routeord').AsString := 'ROUTEORD';
    DM.InsertRouteOrderInfo.ParamByName('info').AsString := '[' + IntToStr(RouteOrderInfo.TicketID) + '] ' + RouteOrderInfo.ReasonCode; // + ' [' + ParentTicketNum + ']'
    DM.InsertRouteOrderInfo.ParamByName('added_by').AsInteger := DM.EmpID;
    DM.InsertRouteOrderInfo.ParamByName('modified_date').AsDateTime := Now;
    DM.InsertRouteOrderInfo.ParamByName('ticketnum').AsString := RouteOrderInfo.TicketNum;
    DM.InsertRouteOrderInfo.ExecSQL;
    Result := True;
  except
    Result := False;
  end;
end;

destructor TRouteOrderObj.Destroy;
begin
  ClearArray;
end;

procedure TRouteOrderObj.Next(TicketID: Integer);
var
  i, NumRows: integer;
begin
  NumRows := Length(fRouteArray);

  i := NumRows + 1;
  SetLength(fRouteArray, i);
  HighOrderNum := HighOrderNum + 1;
  fRouteArray[i].IsSet := True;
  fRouteArray[i].TicketID := TicketID;
  fRouteArray[i].OrderNum := HighOrderNum;
end;

procedure TRouteOrderObj.ScanRouteOrders(AListTable: TDataSet);
var
  i : integer;
begin
  ClearArray;
  if AListTable.Active = False then exit;  //QM-198
  AListTable.First;
  NumberOfRecords := AListTable.RecordCount;
  for i  := 0 to AListTable.RecordCount - 1 do begin
    SetLength(fRouteArray, i + 1);
    fRouteArray[i].IsSet := True;
    fRouteArray[i].OrderNum := AListTable.FieldByName('route_order').AsFloat;
    fRouteArray[i].TicketID := AListTable.FieldByName('ticket_id').AsInteger;
    AListTable.Next;
  end;
  AListTable.First;
end;

function TRouteOrderObj.SetRO(TicketID: Integer; RouteOrder: Double; var Msg: string ): boolean;
var
  i, targetindex: integer;
begin
  Result := False;
  targetindex := -1;
  if not assigned(RouteOrderObj) then
    RouteOrderObj := TRouteOrderObj.Create;

  if NumberOfRecords > 0 then begin
    for i := 0 to NumberOfRecords - 1 do begin
      {Same Ticket, same route order (return true)}
      if ((fRouteArray[i].OrderNum) = RouteOrder) and (fRouteArray[i].TicketID = TicketID) then begin
        Result := False; // Trying to set the ticketid to the previous order num
        Msg := '';
        Exit;  //Jump all the way out
      end
      {Same route order on a different ticket (return false - because we didn't set it)}
      else if (fRouteArray[i].OrderNum = RouteOrder) and (RouteOrder <> 0) then begin
        Result := False; //we don't set it because the order is already taken
        Msg := 'Duplicate RO/Error Saving (RO# ' + FloatToStr(RouteOrder) + ')';
        Exit;  //Jump all the way out
      end
      {Same Ticket id, need to set route order (return true)}
      else if (fRouteArray[i].TicketID = TicketID) then begin
        Result := True;
        Msg := '';
        targetindex := i;
      end;
    end;

    if targetindex > -1 then begin
      fRouteArray[NumberOfRecords - 1].OrderNum := RouteOrder;
      fRouteArray[NumberOfRecords - 1].IsSet := True;
      Msg := 'Updated';
      exit;
    end;
  end;

  {If we didn't find anything, then continue on with adding a new one}
    Inc(NumberOfRecords);
    SetLength(fRouteArray, NumberofRecords);
    fRouteArray[NumberOfRecords - 1].TicketID := TicketID;
    fRouteArray[NumberOfRecords - 1].OrderNum := RouteOrder;
    fRouteArray[NumberOfRecords - 1].IsSet := True;
    Result := True;
    Msg := 'Added';
end;

procedure TRouteOrderObj.Clear(TicketID: integer);
var
  i: integer;
  RecNum: integer;
begin
  RecNum := Length(fRouteArray);
  for i := 0 to RecNum - 1 do begin
    if fRouteArray[i].TicketID = TicketID then begin
      fRouteArray[i].IsSet := False;
      fRouteArray[i].OrderNum := 0.0;
      fRouteArray[i].TicketID := 0;
      if (i = (RecNum -1)) then
        HighOrderNum := HighOrderNum;  //QM-166 EB FIX
      Break;
    end;
  end;
end;




initialization
  RouteOrderObj := TRouteOrderObj.Create;

finalization
  FreeAndNil(RouteOrderObj);

end.
