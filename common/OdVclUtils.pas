unit OdVclUtils;

interface

uses Classes, Graphics, Controls, StdCtrls, ExtCtrls, ComCtrls, Forms,
  IniFiles, CheckLst, Dialogs, OdMiscUtils, CodeLookupList;

{$IF CompilerVersion >= 16} // Delphi 8, 2005 or greater
  {$DEFINE ClassHelpers}
{$IFEND}
{$IF CompilerVersion >= 15}
  {$DEFINE Theming}
{$IFEND}

{$IFDEF ClassHelpers}
type
  TTreeeNodeHelper = class helper for TTreeNode
  public
    function ID: Integer;
  end;
{$ENDIF ClassHelpers}

procedure LoadBitmapResource(Image: TImage; ResourceName: string); overload;
procedure LoadBitmapResource(Picture: TPicture; ResourceName: string); overload;
procedure LoadEmfResource(Image: TImage; ResourceName: string);
procedure LoadWmfResource(Image: TImage; ResourceName: string);
procedure AddMemoScrollbarsIfNecessary(Memo: TMemo);
procedure CenterControlOnParent(Control: TControl);
function IsChildOfParent(Par: TWinControl; Child: TWinControl): Boolean;
procedure SizeComboDropdownToItems(Combo: TCustomComboBox; AMax: Integer = 600);
procedure SizeComboDropdownsOnParent(ParentControl: TWinControl; AMaxWidth: Integer = 600; AMinLength: Integer = 15);
procedure ClonePersistent(Source, Dest: TPersistent);
function SelectComboBoxItemFromObjects(ComboBox: TComboBox; ID: Integer): Boolean;
procedure SaveFormState(Form: TCustomForm; IniFile: TIniFile; const Section, Prefix: string);
procedure RestoreFormState(Form: TCustomForm; IniFile: TIniFile; const Section, Prefix: string);
procedure SetEnabledOnControlAndChildren(Control: TControl; Value: Boolean);
function GetCheckedItemString(ListBox: TCheckListBox; IntObjects: Boolean = False): string;
function GetCheckedItemCodesString(ListBox: TCheckListBox): string;
procedure SetCheckListBox(ListBox: TCheckListBox; Checked: Boolean; MaxChecked:integer = 0);   //QMANTWO-275
function IsAllCheckListBox(ListBox: TCheckListBox; Checked: Boolean): Boolean;
function GetListBoxObjectInteger(ListBox: TCustomListBox; Index: Integer): Integer;
function GetComboObjectInteger(Combo: TCustomComboBox; Required: Boolean = True): Integer;
function GetComboObjectIntegerDef(Combo: TCustomComboBox; Default: Integer = -1): Integer;
function GetIndexOfComboInteger(Combo: TCustomComboBox; IntObject: Integer): Integer;
function SelectComboItem(Combo: TCustomComboBox; const Item: string; Default: Integer = 0): Boolean;
procedure SetControlEnabled(Control: TControl; Enabled: Boolean);
procedure SetControlVisible(Control: TControl; Visible: Boolean);
function TryFocusControl(Control: TWinControl): Boolean;
function SetDefaultFont(Control: TControl): string;
procedure ShowPageControlAsSingleTab(Tab: TTabSheet);
procedure ShowPageControlAsMultiTab(PageCtrl: TPageControl);
{$IFDEF Theming}
procedure SetTabHighlight(Tab: TTabSheet; Value: Boolean);
function GetTabBackgroundColor: TColor;
{$ENDIF Theming}
function FindTextInTreeView(TextToSearch: string; Treeview: TTreeview; SearchChild, PartialSearch, Reset: Boolean): TTreeNode;
procedure SetFontBold(Control: TControl);
procedure SetFontUnderline(Control: TControl);
procedure SetFontSize(Control: TControl; SizeChange: Integer);
procedure SetFontBoldAndSize(Control: TControl; SizeChange: Integer);
procedure SetFontBoldAndColor(Control: TControl; Color: TColor);
procedure SetFontColor(Control: TControl; Color: TColor);
procedure SetEnabledColor(Control: TControl);
procedure SetParentBackgroundValue(Panel: TCustomPanel; Value: Boolean); overload;
procedure SetParentBackgroundValue(GroupBox: TGroupBox; Value: Boolean); overload;

function MessageDlgFmt(const Msg: string; Args: array of const; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; DefaultButton: Integer = -1): Integer;
procedure TurnOffTreeViewNodeHints(Tree: TTreeView);
procedure PrintControl(Form: TWinControl);
procedure ResizeAttachmentColumns(ListView: TCustomListView);
function GetMonthCalendarCursorPos(MonthCalendar: TMonthCalendar; CalItems: array of Integer): Boolean;
function IsMonthCalendarCursorInTitle(MonthCalendar: TMonthCalendar): Boolean;
function IsMouseOverControl(Control: TWinControl): Boolean;

function InputMemo(const ACaption, APrompt: string; var Value: string): Boolean;
function InputCombo(const Msg: string; Values: TStrings; var Value: string): Boolean;
function AllItemsAreChecked(ListBox: TCheckListBox): Boolean;
procedure ShowCheckListboxHorzScrollbars(CheckListBox: TCheckListBox);
procedure LimitCheckListBox( AChkLb : TCheckListBox; AMaxCheck : Integer );
implementation

uses SysUtils, Windows, Messages, Math, TypInfo, Consts, StrUtils,
  {$IFDEF Theming} Themes, {$ENDIF Theming}
  Printers, CommCtrl;

type
  TControlCracker = class(TControl);

procedure LoadBitmapResource(Image: TImage; ResourceName: string);
var
  Bmp: Graphics.TBitmap;
begin
  Bmp := Graphics.TBitmap.Create;
  try
    Bmp.LoadFromResourceName(HInstance, ResourceName);
    Image.Picture.Graphic := Bmp;
  finally
    FreeAndNil(Bmp);
  end;
end;

procedure LoadBitmapResource(Picture: TPicture; ResourceName: string);
var
  Bmp: Graphics.TBitmap;
begin
  Bmp := Graphics.TBitmap.Create;
  try
    Bmp.LoadFromResourceName(HInstance, ResourceName);
    Picture.Graphic := Bmp;
  finally
    FreeAndNil(Bmp);
  end;
end;

procedure InternalLoadMetafileResource(Image: TImage; const ResourceName: string; IsWMF: Boolean);
var
  ResourceStream: TResourceStream;
  Metafile: TMetaFile;
begin
  Metafile := nil;
  ResourceStream := TResourceStream.Create(HInstance, ResourceName, RT_RCDATA);
  try
    Metafile := TMetafile.Create;
    Image.Picture.Metafile := TMetafile.Create;
    Image.Picture.Metafile.Enhanced := IsWMF;
    Image.Picture.Metafile.LoadFromStream(ResourceStream);
  finally
    FreeAndNil(Metafile);
    FreeAndNil(ResourceStream);
  end;
end;

procedure LoadEmfResource(Image: TImage; ResourceName: string);
begin
  InternalLoadMetafileResource(Image, ResourceName, False);
end;

procedure LoadWmfResource(Image: TImage; ResourceName: string);
begin
  InternalLoadMetafileResource(Image, ResourceName, True);
end;

procedure AddMemoScrollbarsIfNecessary(Memo: TMemo);
var
  MemoRect: TRect;
  TextRect: TRect;
  Text: string;
  Bitmap: Graphics.TBitmap;
begin
  Text := Memo.Text;
  Memo.Perform(EM_GETRECT, 0, Longint(@MemoRect));
  TextRect := MemoRect;
  Bitmap := Graphics.TBitmap.Create;
  try
    Bitmap.Canvas.Font := Memo.Font;
    DrawTextEx(Bitmap.Canvas.Handle, PChar(Text), Length(Text), TextRect,
      DT_CALCRECT or DT_EDITCONTROL or DT_WORDBREAK or DT_NOPREFIX, nil);
  finally
    FreeAndNil(Bitmap);
  end;
  if Memo.WordWrap then begin
    if TextRect.Bottom > MemoRect.Bottom then
      Memo.ScrollBars := ssVertical
    else
      Memo.ScrollBars := ssNone;
  end
  else begin
    if TextRect.Bottom > MemoRect.Bottom then
      Memo.ScrollBars := ssVertical
    else if TextRect.Left > MemoRect.Left then
      Memo.ScrollBars := ssHorizontal
    else
      Memo.ScrollBars := ssNone;
  end;
end;

procedure CenterControlOnParent(Control: TControl);
begin
  Assert(Assigned(Control));
  Assert(Assigned(Control.Parent));
  Control.Left := Round((Control.Parent.ClientWidth - Control.Width) / 2);
  Control.Top := Round((Control.Parent.ClientHeight - Control.Height) / 2);
end;

function IsChildOfParent(Par: TWinControl; Child: TWinControl): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (Child = nil) or (Par = nil) then Exit;
  for i := 0 to Par.ControlCount - 1 do
  begin
    if Par.Controls[i] = Child then
    begin
      Result := True;
      Break;
    end
    else if (not Result) and (Par.Controls[i] is TWinControl) then
      Result := IsChildOfParent(TWinControl(Par.Controls[i]), Child);
  end;
end;

type
  TComboCracker = class(TCustomComboBox);

procedure SizeComboDropdownToItems(Combo: TCustomComboBox; AMax: Integer);
var
  i: Integer;
  MaxWidth: Integer;
  Bitmap: Graphics.TBitmap;
begin
  MaxWidth := Combo.Width;
  Bitmap := Graphics.TBitmap.Create;
  try
    Bitmap.Canvas.Font.Assign(TComboCracker(Combo).Font);
    for i := 0 to Combo.Items.Count - 1 do
      MaxWidth := Max(MaxWidth, Bitmap.Canvas.TextWidth(Combo.Items[i]) + 10);
  finally;
    FreeAndNil(Bitmap);
  end;
  if Combo.Items.Count > TComboCracker(Combo).DropDownCount then
    Inc(MaxWidth, GetSystemMetrics(SM_CXVSCROLL));
  MaxWidth := Min(AMax, MaxWidth);
  if MaxWidth > Combo.Width then
    SendMessage(Combo.Handle, CB_SETDROPPEDWIDTH, MaxWidth, 0)
  else
    SendMessage(Combo.Handle, CB_SETDROPPEDWIDTH, 0, 0)
end;

type
  THackCombo = class(TCustomComboBox);

procedure SizeComboDropdownsOnParent(ParentControl: TWinControl; AMaxWidth: Integer = 600; AMinLength: Integer = 15);
var
  i: Integer;
  Combo: TCustomComboBox;
begin
  for i := 0 to ParentControl.ControlCount - 1 do begin
    if ParentControl.Controls[i] is TCustomComboBox then begin
      Combo := ParentControl.Controls[i] as TCustomComboBox;
      SizeComboDropdownToItems(Combo, AMaxWidth);
      THackCombo(Combo).DropDownCount := Max(THackCombo(Combo).DropDownCount, AMinLength);
    end;
  end;
end;

procedure ClonePersistent(Source, Dest: TPersistent);
var
  i: Integer;
  PropCount: Integer;
  PropList: TPropList;
  PropName: string;
begin
  Assert(Assigned(Source) and Assigned(Dest));

  PropCount := GetPropList(Source.ClassInfo, tkAny, @PropList);
  for i := 0 to PropCount - 1 do begin
    PropName := string(PropList[i].Name);
    case PropList[i].PropType^.Kind of
      tkLString, tkWString, tkVariant:
        SetPropValue(Dest, PropName, GetPropValue(Source, PropName));
      tkInteger, tkChar, tkWChar:
        SetOrdProp(Dest, PropName, GetOrdProp(Source, PropName));
      tkFloat:
        SetFloatProp(Dest, PropName, GetFloatProp(Source, PropName));
      tkString:
        SetStrProp(Dest, PropName, GetStrProp(Source, PropName));
      tkMethod:
        SetMethodProp(Dest, PropName, GetMethodProp(Source, PropName));
      tkInt64:
        SetInt64Prop(Dest, PropName, GetInt64Prop(Source, PropName));
      tkEnumeration:
        SetEnumProp(Dest, PropName, GetEnumProp(Source, PropName));
      tkSet:
        SetSetProp(Dest, PropName, GetSetProp(Source, PropName));
{$IFDEF UNICODE}
      tkUString:   //QMANTWO-513 added for unicode  sr
        SetStrProp(Dest, PropName, GetStrProp(Source, PropName));
{$ENDIF}
      //tkClass, tkRecord, tkArray, tkDynArray, tkUnknown are ignored for now
    end;
  end;
end;

function SelectComboBoxItemFromObjects(ComboBox: TComboBox; ID: Integer): Boolean;
var
  i: Integer;
begin
  for i := 0 to ComboBox.Items.Count - 1 do
    if Integer(ComboBox.Items.Objects[i]) = ID then begin
      ComboBox.ItemIndex := i;
      Result := True;
      Exit;
    end;

  ComboBox.ItemIndex := -1;
  Result := False;
end;

procedure SaveFormState(Form: TCustomForm; IniFile: TIniFile; const Section, Prefix: string);
var
  WPlace: TWindowPlacement;
begin
  Assert(Assigned(Form) and Assigned(IniFile));
  WPlace.Length := SizeOf(TWindowPlacement);
  GetWindowPlacement(Form.Handle, @WPlace);
  IniFile.WriteInteger(Section, Prefix + 'WindowState', Ord(Form.WindowState));
  IniFile.WriteInteger(Section, Prefix + 'Left', WPlace.rcNormalPosition.Left);
  IniFile.WriteInteger(Section, Prefix + 'Top', WPlace.rcNormalPosition.Top);
  IniFile.WriteInteger(Section, Prefix + 'Right', WPlace.rcNormalPosition.Right);
  IniFile.WriteInteger(Section, Prefix + 'Bottom', WPlace.rcNormalPosition.Bottom);
end;

procedure RestoreFormState(Form: TCustomForm; IniFile: TIniFile; const Section, Prefix: string);
var
  WState: Byte;
  WPlace: TWindowPlacement;
  Rect: TRect;
begin
  Assert(Assigned(Form) and Assigned(IniFile));
  FillChar(WPlace, SizeOf(TWindowPlacement), #0);
  WPlace.Length := SizeOf(TWindowPlacement);
  WState := IniFile.ReadInteger(Section, Prefix + 'WindowState', Ord(wsNormal));
  if TWindowState(WState) = wsMaximized then
    WPlace.ShowCmd := SW_SHOWMAXIMIZED
  else
    WPlace.ShowCmd := SW_SHOWNORMAL;
  WPlace.rcNormalPosition.Left := IniFile.ReadInteger(Section, Prefix + 'Left', Form.Left);
  WPlace.rcNormalPosition.Top := IniFile.ReadInteger(Section, Prefix + 'Top', Form.Top);
  WPlace.rcNormalPosition.Right := IniFile.ReadInteger(Section, Prefix + 'Right', Form.Left + Form.Width);
  WPlace.rcNormalPosition.Bottom := IniFile.ReadInteger(Section, Prefix + 'Bottom', Form.Top + Form.Height);
  SetWindowPlacement(Form.Handle, @WPlace);
  if WPlace.showCmd = SW_SHOWMAXIMIZED then
    Exit;
  Rect.Top := 0;
  Rect.Left := 0;
  Rect.Right := Screen.Width;
  Rect.Bottom := Screen.Height;
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);
  if (Form.Left + Form.Width > Rect.Right) then
    Form.Left := Form.Left - ((Form.Left + Form.Width) - Rect.Right);
  if (Form.Top + Form.Height > Rect.Bottom) then
    Form.Top := Form.Top - ((Form.Top + Form.Height) - Rect.Bottom);
  if Form.Left < 0 then
    Form.Left := 0;
  if Form.Top < 0 then
    Form.Top := 0;
end;

procedure SetEnabledOnControlAndChildren(Control: TControl; Value: Boolean);
var
  i: Integer;
  WinControl: TWinControl;
begin
  Assert(Assigned(Control));
  if Control.Enabled <> Value then
    Control.Enabled := Value;
  if Control is TWinControl then begin
    WinControl := (Control as TWinControl);
    for i := 0 to WinControl.ControlCount - 1 do
      SetEnabledOnControlAndChildren(WinControl.Controls[i], Value);
  end;
end;

function GetCheckedItemString(ListBox: TCheckListBox; IntObjects: Boolean): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ListBox.Count - 1 do begin
    if ListBox.Checked[i] then begin
      if Result = '' then begin
        if IntObjects then
          Result := IntToStr(Integer(ListBox.Items.Objects[i]))
        else
          Result := ListBox.Items[i]
      end
      else begin
        if IntObjects then
          Result := Result + ',' + IntToStr(Integer(ListBox.Items.Objects[i]))
        else
          Result := Result + ',' + ListBox.Items[i];
      end;
    end;
  end;
end;

function GetCheckedItemCodesString(ListBox: TCheckListBox): string;
var
  i: Integer;
  Code: TItemString;
begin
  Result := '';
  for i := 0 to ListBox.Count - 1 do begin
    if ListBox.Checked[i] then begin
      Assert(ListBox.Items.Objects[i] is TItemString);
      Code := ListBox.Items.Objects[i] as TItemString;
      if Result = '' then
        Result := Code.Text
      else
        Result := Result + ',' + Code.Text;
    end;
  end;
end;

procedure SetCheckListBox(ListBox: TCheckListBox; Checked: Boolean; MaxChecked:integer = 0);   //QMANTWO-275
var
  i: Integer;
begin
  if MaxChecked = 0 then MaxChecked := maxInt;

  for i := 0 to ListBox.Items.Count - 1 do
  begin
    if i > MaxChecked then
    begin
      ListBox.ItemIndex := i+3;
      ListBox.Repaint;
      exit;
    end;
    ListBox.Checked[i] := Checked;
  end;
end;

procedure LimitCheckListBox( AChkLb : TCheckListBox; AMaxCheck : Integer );
var
  LIdx : Integer;
  LCheckCount : Integer;
begin
  // counting
  LCheckCount := 0;
  for LIdx := 0 to AChkLb.Count - 1 do
  begin
    if AChkLb.Checked[LIdx] then
      if LCheckCount = AMaxCheck then
        AChkLb.Checked[LIdx] := False
      else
        Inc( LCheckCount );
  end;
  // enable/disable
  for LIdx := 0 to AChkLb.Count - 1 do
    AChkLb.ItemEnabled[LIdx] := AChkLb.Checked[LIdx] or ( LCheckCount < AMaxCheck );
end;

function IsAllCheckListBox(ListBox: TCheckListBox; Checked: Boolean): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to ListBox.Items.Count - 1 do
    if ListBox.Checked[i] <> Checked then begin
      Result := False;
      Break;
    end;
end;

function GetListBoxObjectInteger(ListBox: TCustomListBox; Index: Integer): Integer;
begin
  Assert(Assigned(ListBox));
  Assert((Index > -1) and (Index < ListBox.Items.Count));
  Result := Integer(ListBox.Items.Objects[Index]);
end;

function GetComboObjectInteger(Combo: TCustomComboBox; Required: Boolean): Integer;
begin
  Assert(Assigned(Combo));
  if (Combo.ItemIndex < 0) then begin
    if Required then
      raise Exception.Create('No selected combobox item for ' + Combo.Name)
    else
      Result := -1;
  end
  else
    Result := Integer(Combo.Items.Objects[Combo.ItemIndex]);
end;

function GetComboObjectIntegerDef(Combo: TCustomComboBox; Default: Integer): Integer;
begin
  Assert(Assigned(Combo));
  Result := Default;
  if Combo.ItemIndex >= 0 then
    Result := GetComboObjectInteger(Combo);
end;

function GetIndexOfComboInteger(Combo: TCustomComboBox; IntObject: Integer): Integer;
var
  Obj: TObject;
  i: Integer;
begin
  Result := -1;
  Obj := TObject(IntObject);
  for i := 0 to Combo.Items.Count - 1 do begin
    if Combo.Items.Objects[i] = Obj then begin
      Result := i;
      Break;
    end;
  end;
end;

function SelectComboItem(Combo: TCustomComboBox; const Item: string; Default: Integer): Boolean;
var
  Index: Integer;
begin
  Assert(Assigned(Combo));
  Result := False;
  Index := Combo.Items.IndexOf(Item);
  if Index >= 0 then begin
    Combo.ItemIndex := Index;
    Result := True;
  end
  else if Default < Combo.Items.Count then begin
    Combo.ItemIndex := Default;
    Result := True;
  end;
end;

procedure SetControlEnabled(Control: TControl; Enabled: Boolean);
begin
  if not Assigned(Control) then
    Exit;
  Control.Enabled := Enabled;
  if not (Control is TLabel) then begin
    if Control.Enabled then
      TControlCracker(Control).Color := clWindow
    else
      TControlCracker(Control).Color := clBtnFace;
  end
  else
    SetControlEnabled((Control as TLabel).FocusControl, Enabled);
end;

procedure SetControlVisible(Control: TControl; Visible: Boolean);
begin
  if not Assigned(Control) then
    Exit;
  Control.Visible := Visible;
  if Control is TLabel then
    SetControlVisible((Control as TLabel).FocusControl, Visible);
end;

function TryFocusControl(Control: TWinControl): Boolean;
var
  ParentForm: TCustomForm;
begin
  Result := False;
  if Assigned(Control) then begin
    ParentForm := GetParentForm(Control);
    if (Control.CanFocus and Control.Visible and Control.Enabled) and (Assigned(ParentForm) and ParentForm.Visible and ParentForm.Enabled) then begin
      try
        Control.SetFocus;
        Result := True;
      except
        // Ignore focus errors
      end;
    end;
  end;
end;

function SetDefaultFont(Control: TControl): string;
begin
  Result := '';
  if Assigned(Control) then begin
    if (Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >= 5) then
      TControlCracker(Control).Font.Name := 'MS Shell Dlg 2'
    else
      TControlCracker(Control).Font.Name := 'MS Shell Dlg';
    Result := TControlCracker(Control).Font.Name;
  end;
end;

procedure SetFontBold(Control: TControl);
begin
  TControlCracker(Control).Font.Style := TControlCracker(Control).Font.Style + [fsBold];
end;

procedure SetFontUnderline(Control: TControl);
begin
  TControlCracker(Control).Font.Style := TControlCracker(Control).Font.Style + [fsUnderline];
end;

procedure SetFontSize(Control: TControl; SizeChange: Integer);
begin
  TControlCracker(Control).Font.Size := TControlCracker(Control).Font.Size + SizeChange;
end;

procedure SetFontBoldAndSize(Control: TControl; SizeChange: Integer);
begin
  SetFontBold(Control);
  SetFontSize(Control, SizeChange);
end;

procedure SetFontBoldAndColor(Control: TControl; Color: TColor);
begin
  SetFontBold(Control);
  SetFontColor(Control, Color);
end;

procedure SetFontColor(Control: TControl; Color: TColor);
begin
  TControlCracker(Control).Font.Color := Color;
end;

function MessageDlgFmt(const Msg: string; Args: array of const; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; DefaultButton: Integer): Integer;
var
  i: Integer;
  btn: TButton;
begin
  with CreateMessageDialog(Format(Msg, Args), DlgType, Buttons) do
    try
      Caption := Application.Title;
      HelpContext := 0;
      if DefaultButton > -1 then begin
        for i := 0 to ComponentCount-1 do begin
          if Components[i] is TButton then begin
            btn := TButton(Components[i]);
            btn.Default := btn.ModalResult = DefaultButton;
            if btn.Default then
              ActiveControl := Btn;
          end;
        end;
      end;
      Result := ShowModal;
    finally
      Free;
    end;
end;

procedure TurnOffTreeViewNodeHints(Tree: TTreeView);
begin
  SetWindowLong(Tree.Handle, GWL_STYLE, GetWindowLong(Tree.Handle, GWL_STYLE) or $80);
end;

procedure ShowPageControlAsSingleTab(Tab: TTabSheet);
var
  i: Integer;
  PageCtrl: TPageControl;
begin
  PageCtrl := Tab.PageControl;
  for i := 0 to PageCtrl.PageCount - 1 do begin
    PageCtrl.Pages[i].HandleNeeded;
    PageCtrl.Pages[i].TabVisible := False;
  end;
  PageCtrl.Style := tsFlatButtons;
  PageCtrl.ActivePage := Tab;
end;

procedure ShowPageControlAsMultiTab(PageCtrl: TPageControl);
var
  i: Integer;
begin
  for i := 0 to PageCtrl.PageCount - 1 do begin
    PageCtrl.Pages[i].HandleNeeded;
    PageCtrl.Pages[i].TabVisible := True;
  end;
  PageCtrl.Style := tsTabs;
end;

{$IFDEF Theming}

// TODO-XE3: Replace the deprecated ThemeServices with StyleServices to fix warnings.

procedure SetTabHighlight(Tab: TTabSheet; Value: Boolean);
const
  TabHighlightSuffix = ' *';
begin
  Assert(Assigned(Tab));
  if ThemeServices.ThemesAvailable and ThemeServices.ThemesEnabled then begin
    if StrEndsWith(TabHighlightSuffix, Tab.Caption) and not Value then
      Tab.Caption := LeftStr(Tab.Caption, Length(Tab.Caption) - Length(TabHighlightSuffix))
    else if Value and not StrEndsWith(TabHighlightSuffix, Tab.Caption) then
      Tab.Caption := Tab.Caption + TabHighlightSuffix;
  end
  else
    Tab.Highlighted := Value
end;

var
  TabBackgroundColor: TColor = clNone;

function GetTabBackgroundColor: TColor;
var
  B: Graphics.TBitmap;
begin
  if TabBackgroundColor <> clNone then begin
    Result := TabBackgroundColor;
    Exit;
  end;
  Result := clBtnFace;
  if ThemeServices.ThemesEnabled then
  begin
    B := Graphics.TBitmap.Create;
    try
      B.Width := 128;
      B.Height := 128;
      ThemeServices.DrawElement(B.Canvas.Handle,
        ThemeServices.GetElementDetails(ttBody),
        Rect(0, 0, B.Width - 1, B.Height - 1), nil);
      Result := B.Canvas.Pixels[64, 64];
    finally
      FreeAndNil(B);
    end;
  end;
  TabBackgroundColor := Result;
end;
{$ENDIF Theming}

function FindTextInTreeView(TextToSearch: string; Treeview: TTreeview; SearchChild, PartialSearch, Reset: Boolean): TTreeNode;
var
  AuxNode: TTreeNode;
begin
  Assert(Treeview <> nil);
  Result := nil;

  if Reset or (Treeview.Selected = nil) then
    AuxNode := Treeview.Items.GetFirstNode
  else
    AuxNode := Treeview.Selected.GetNext;

  while AuxNode <> nil do begin
    if not PartialSearch then begin
      if SameText(AuxNode.Text, TextToSearch) then begin
        Result := AuxNode;
        Break;
      end
    end else
      if StrContainsText(TextToSearch, AuxNode.Text) then begin
        Result := AuxNode;
        Break;
      end;
    if not SearchChild then
      AuxNode := AuxNode.GetNextSibling
    else
      AuxNode := AuxNode.GetNext;
  end;

  if AuxNode = nil then
    Result := Treeview.Items.GetFirstNode;
end;

// Copied from VCL implementation
function GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

// Copied from VCL implementation
function InputMemo(const ACaption, APrompt: string;
  var Value: string): Boolean;
var
  Form: TForm;
  Prompt: TLabel;
  Memo: TMemo;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  Form := TForm.Create(Application);
  with Form do
    try
      Canvas.Font := Font;
      DialogUnits := GetAveCharSize(Canvas);
      BorderStyle := bsDialog;
      Caption := ACaption;
      ClientWidth := MulDiv(350, DialogUnits.X, 4);
      Position := poScreenCenter;
      Prompt := TLabel.Create(Form);
      with Prompt do
      begin
        Parent := Form;
        Caption := APrompt;
        Left := MulDiv(8, DialogUnits.X, 4);
        Top := MulDiv(8, DialogUnits.Y, 8);
        Constraints.MaxWidth := MulDiv(164, DialogUnits.X, 4);
        WordWrap := True;
      end;
      Memo := TMemo.Create(Form);
      with Memo do
      begin
        Parent := Form;
        Left := Prompt.Left;
        Top := Prompt.Top + Prompt.Height + 5;
        Width := MulDiv(335, DialogUnits.X, 4);
        Height := MulDiv(200, DialogUnits.X, 4);
        MaxLength := 1000;
        Text := Value;
        ScrollBars := ssBoth;
        WordWrap := True;
        SelectAll;
      end;
      ButtonTop := Memo.Top + Memo.Height + 15;
      ButtonWidth := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := SMsgDlgOK;
        ModalResult := mrOk;
        Default := True;
        SetBounds(MulDiv(130, DialogUnits.X, 4), ButtonTop, ButtonWidth,
          ButtonHeight);
      end;
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := SMsgDlgCancel;
        ModalResult := mrCancel;
        Cancel := True;
        SetBounds(MulDiv(190, DialogUnits.X, 4), Memo.Top + Memo.Height + 15,
          ButtonWidth, ButtonHeight);
        Form.ClientHeight := Top + Height + 13;
      end;
      if ShowModal = mrOk then
      begin
        Value := Memo.Text;
        Result := True;
      end;
    finally
      FreeAndNil(Form);
    end;
end;

function InputCombo(const Msg: string; Values: TStrings; var Value: string): Boolean;
var
  Form: TForm;
  Prompt: TLabel;
  Combo: TComboBox;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  Form := TForm.Create(Application);
  with Form do
    try
      Canvas.Font := Font;
      DialogUnits := GetAveCharSize(Canvas);
      BorderStyle := bsDialog;
      Caption := Msg;
      ClientWidth := MulDiv(320, DialogUnits.X, 4);
      Position := poScreenCenter;
      Prompt := TLabel.Create(Form);
      with Prompt do
      begin
        Parent := Form;
        Caption := Msg;
        Left := MulDiv(8, DialogUnits.X, 4);
        Top := MulDiv(8, DialogUnits.Y, 8);
        Constraints.MaxWidth := MulDiv(164, DialogUnits.X, 4);
        WordWrap := True;
      end;
      Combo := TComboBox.Create(Form);
      with Combo do
      begin
        Parent := Form;
        Left := Prompt.Left;
        Top := Prompt.Top + Prompt.Height + 6;
        Width := MulDiv(270, DialogUnits.X, 4);
        Items.Assign(Values);
        ItemIndex := -1;
        DropDownCount := 15;
        Style := csDropDownList;
      end;
      ButtonTop := Combo.Top + Combo.Height + 15;
      ButtonWidth := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := SMsgDlgOK;
        ModalResult := mrOk;
        Default := True;
        SetBounds(MulDiv(130, DialogUnits.X, 4), ButtonTop, ButtonWidth,
          ButtonHeight);
      end;
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := SMsgDlgCancel;
        ModalResult := mrCancel;
        Cancel := True;
        SetBounds(MulDiv(190, DialogUnits.X, 4), Combo.Top + Combo.Height + 15,
          ButtonWidth, ButtonHeight);
        Form.ClientHeight := Top + Height + 13;
      end;
      if (ShowModal = mrOk) then
      begin
        if Combo.ItemIndex < 0 then
          Abort;
        Value := Combo.Text;
        Result := True;
      end;
    finally
      FreeAndNil(Form);
    end;
end;

procedure SetEnabledColor(Control: TControl);
begin
  Assert(Assigned(Control));
  if Control.Enabled then
    TControlCracker(Control).Color := clWindow
  else
    TControlCracker(Control).Color := clBtnFace;
end;

procedure SetParentBackgroundValue(Panel: TCustomPanel; Value: Boolean);
begin
  {$IF CompilerVersion >= 15} // Delphi 7+
  Assert(Assigned(Panel));
  Panel.ParentBackground := Value;
  {$IFEND}
end;

procedure SetParentBackgroundValue(GroupBox: TGroupBox; Value: Boolean);
begin
  {$IF CompilerVersion >= 15} // Delphi 7+
  Assert(Assigned(GroupBox));
  GroupBox.ParentBackground := Value;
  {$IFEND}
end;

function AllItemsAreChecked(ListBox: TCheckListBox): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to ListBox.Count - 1 do begin
    if not ListBox.Checked[i] then begin
      Result := False;
      Break;
    end;
  end;
end;

procedure ShowCheckListboxHorzScrollbars(CheckListBox: TCheckListBox);
var
  i: Integer;
  MaxWidth: Integer;
  ItemWidth: Integer;
begin
  MaxWidth := CheckListBox.ClientWidth;
  for i := 0 to CheckListBox.Items.Count - 1 do begin
    ItemWidth := CheckListBox.Canvas.TextWidth(CheckListBox.Items[i]) + 20;
    if (ItemWidth > MaxWidth) then
      MaxWidth := ItemWidth;
  end;
  if (MaxWidth > CheckListBox.ClientWidth) then
    CheckListBox.ScrollWidth := MaxWidth
  else //Don't need horz scroll
    CheckListBox.ScrollWidth := 0;
end;

// Heavily modified from: http://cc.codegear.com/Item/11272  "A Better Way To Print a Form"
procedure PrintControl(Form: TWinControl);
var
  dc: HDC;
  isDcPalDevice: BOOL;
  MemDc: hdc;
  MemBitmap: hBitmap;
  OldMemBitmap: hBitmap;
  hDibHeader: Thandle;
  pDibHeader: pointer;
  hBits: Thandle;
  pBits: pointer;
  ScaleX: Double;
  ScaleY: Double;
  ppal: PLOGPALETTE;
  pal: hPalette;
  Oldpal: hPalette;
  i: Integer;
  OldOrientation: TPrinterOrientation;
  pt: TPoint;
  f: Double;
  ScreenLeft: Integer;
  ScreenTop: Integer;
begin
  Assert(Assigned(Form));
  OldOrientation := Printer.Orientation;
  try
    if (Form.Width > Form.Height) then
      Printer.Orientation := poLandscape
    else
      Printer.Orientation := poPortrait;

    {Get the screen dc}
    dc := GetDc(0);
    {Create a compatible dc}
    MemDc := CreateCompatibleDc(dc);
    {create a bitmap}
    MemBitmap := CreateCompatibleBitmap(dc, Form.Width, Form.Height);
    {select the bitmap into the dc}
    OldMemBitmap := SelectObject(MemDc, MemBitmap);

    {Lets prepare to try a fixup for broken video drivers}
    if GetDeviceCaps(dc, RASTERCAPS) and RC_PALETTE = RC_PALETTE then begin
      GetMem(pPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
      FillChar(pPal^, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)), #0);
      pPal^.palVersion := $300;
      pPal^.palNumEntries := GetSystemPaletteEntries(dc, 0, 256, pPal^.palPalEntry);
      FreeMem(pPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
    end;

    if Assigned(Form.Parent) then begin
      ScreenLeft := Form.Parent.ClientToScreen(Point(Form.Left, Form.Top)).X;
      ScreenTop := Form.Parent.ClientToScreen(Point(Form.Left, Form.Top)).Y;
    end
    else begin
      ScreenLeft := Form.Left;
      ScreenTop := Form.Top;
    end;
    {copy from the screen to the memdc/bitmap}
    BitBlt(MemDc, 0, 0, Form.Width, Form.Height, dc, ScreenLeft, ScreenTop, SrcCopy);

    {unselect the bitmap}
    SelectObject(MemDc, OldMemBitmap);
    {delete the memory dc}
    DeleteDc(MemDc);
    {Allocate memory for a DIB structure}
    hDibHeader := GlobalAlloc(GHND, sizeof(TBITMAPINFO) + (sizeof(TRGBQUAD) * 256));
    {get a pointer to the alloced memory}
    pDibHeader := GlobalLock(hDibHeader);

    {fill in the dib structure with info on the way we want the DIB}
    FillChar(pDibHeader^, sizeof(TBITMAPINFO) + (sizeof(TRGBQUAD) * 256), #0);
    PBITMAPINFOHEADER(pDibHeader)^.biSize := sizeof(TBITMAPINFOHEADER);
    PBITMAPINFOHEADER(pDibHeader)^.biPlanes := 1;
    PBITMAPINFOHEADER(pDibHeader)^.biBitCount := 8;
    PBITMAPINFOHEADER(pDibHeader)^.biWidth := Form.Width;
    PBITMAPINFOHEADER(pDibHeader)^.biHeight := Form.Height;
    PBITMAPINFOHEADER(pDibHeader)^.biCompression := BI_RGB;

    {Find out how much memory for the bits}
    GetDIBits(dc, MemBitmap, 0, Form.Height, nil, TBitmapInfo(pDibHeader^), DIB_RGB_COLORS);

    {Alloc memory for the bits}
    hBits := GlobalAlloc(GHND, PBitmapInfoHeader(pDibHeader)^.BiSizeImage);
    {Get a pointer to the bits}
    pBits := GlobalLock(hBits);

    {Call fn again, but this time give us the bits!}
    GetDIBits(dc, MemBitmap, 0, Form.Height, pBits, PBitmapInfo(pDibHeader)^, DIB_RGB_COLORS);

    {Release the screen dc}
    ReleaseDc(0, dc);
    {Delete the bitmap}
    DeleteObject(MemBitmap);

    pt.x := Printer.PageWidth;
    pt.y := Printer.PageHeight;
    DPtoLP(Printer.handle, pt, 1);
    // pt now contains print area size in mapping mode logical units,
    // which happen to be the same as screen pixels
    if (Form.Width / Form.Height) > (pt.x / pt.y) then
      f := pt.x / Form.Width
    else
      f := pt.y / Form.Height;

    {Scale print size}
    ScaleX := Form.Width * f;
    ScaleY := Form.Height * f;

    {Start print job}
    Printer.Title := 'Screenshot';
    Printer.BeginDoc;

    {Just incase the printer drver is a palette device}
    isDcPalDevice := False;
    if GetDeviceCaps(Printer.Canvas.Handle, RASTERCAPS) and
      RC_PALETTE = RC_PALETTE then begin
      {Create palette from dib}
      GetMem(pPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
      FillChar(pPal^, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)), #0);
      pPal^.palVersion := $300;
      pPal^.palNumEntries := 256;
      for i := 0 to (pPal^.PalNumEntries - 1) do begin
        pPal^.palPalEntry[i].peRed := PBitmapInfo(pDibHeader)^.bmiColors[i].rgbRed;
        pPal^.palPalEntry[i].peGreen := PBitmapInfo(pDibHeader)^.bmiColors[i].rgbGreen;
        pPal^.palPalEntry[i].peBlue := PBitmapInfo(pDibHeader)^.bmiColors[i].rgbBlue;
      end;
      pal := CreatePalette(pPal^);
      FreeMem(pPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
      oldPal := SelectPalette(Printer.Canvas.Handle, Pal, False);
      isDcPalDevice := True
    end
    else begin
      pal := 0;
      Oldpal := 0;
    end;

    {Send the image to the printer}
    StretchDiBits(Printer.Canvas.Handle, 0, 0, Round(scaleX), Round(scaleY), 0, 0,
      Form.Width, Form.Height, pBits, PBitmapInfo(pDibHeader)^, DIB_RGB_COLORS, SRCCOPY);

    {Just incase you printer driver is a palette device}
    if isDcPalDevice = True then begin
      SelectPalette(Printer.Canvas.Handle, oldPal, False);
      DeleteObject(Pal);
    end;

    GlobalUnlock(hBits);
    GlobalFree(hBits);
    GlobalUnlock(hDibHeader);
    GlobalFree(hDibHeader);

    Printer.EndDoc;
  finally
    Printer.Orientation := OldOrientation;
  end;
end;

type
  TAccessListView = class(TCustomListView);

procedure ResizeAttachmentColumns(ListView: TCustomListView);
var
  Col: TListColumn;
  i: Integer;
begin
  TAccessListView(ListView).Columns.BeginUpdate;
  try
    if TAccessListView(ListView).Items.Count > 0 then begin
      for i := 0 to TAccessListView(ListView).Columns.Count - 1 do
      begin
        Col := ListView.Column[i];
        Col.Width := Col.Width + 30;
        if Col.Caption = 'Name' then
          Col.Width := Col.Width + 150;
      end;
    end;
  finally
    TAccessListView(ListView).Columns.EndUpdate;
  end;
end;

function GetMonthCalendarCursorPos(MonthCalendar: TMonthCalendar; CalItems: array of Integer): Boolean;
var
  MCHitTest: TMCHitTestInfo;
begin
  Result := False;
  ZeroMemory(@MCHitTest, SizeOf(MCHitTest));
  MCHitTest.cbSize := SizeOf(TMCHitTestInfo);
  MCHitTest.pt := MonthCalendar.ScreenToClient(Mouse.CursorPos);
  if not LongBool(MonthCal_HitTest(MonthCalendar.Handle, MCHitTest)) then
    Exit;
  if IntInArray(MCHitTest.uHit, CalItems) then
    Result := True;
end;

function IsMonthCalendarCursorInTitle(MonthCalendar: TMonthCalendar): Boolean;
const
  IgnoreItems : array[0..4] of Integer = (MCHT_TITLEBTNNEXT, MCHT_TITLEBTNPREV, MCHT_TITLEBK, MCHT_TITLEMONTH, MCHT_TITLEYEAR);
begin
  Result := GetMonthCalendarCursorPos(MonthCalendar, IgnoreItems);
end;

function IsMouseOverControl(Control: TWinControl): Boolean;
var
  MouseControl: TWinControl;
begin
  Result := False;
  MouseControl := FindVCLWindow(Mouse.CursorPos);
  if (MouseControl = Control) then
    Result := True;
end;

{ TTreeeNodeHelper }

{$IFDEF ClassHelpers}
function TTreeeNodeHelper.ID: Integer;
begin
  Result := Integer(Data);
end;
{$ENDIF ClassHelpers}

end.

