{
TOdPerfLog is a class to provide benchmark/audit/performance metrics. After creating
the object, you can run one of 2 ways:

1. [Simple case] Call Start, and then Stop. Evaluate ElapsedTime.
2. [Complex case] Call Start('Phase<n>') repeatedly each time you want to
   start measuring a new phase. At the end, call Stop. You can iterate over the
   Phases property to find out details on each phase, or check ElapsedTime to
   see the total time.
3. [Complex case II] After calling Start, you can call AddSubPhase on the returned
   Phase to group phases into logical phases. Subphases can be nested as deeply
   as desired.

TOdDBPerfLog is a descendant of TOdPerfLog. It requires that you assign an ADOConnection
before calling Start/Stop. After calling Stop, the Phases collection will be written
to the DB table. You need to assign the billing_run_id and center_group_id
properties before starting. You can also write to the default table (billing_performance_log),
or override that and write to your own table.

NOTES:
* Precision of the timer is about 5ms. We can use QueryPerformanceFrequency/
  QueryPerformanceCounter if we need greater precision.
* GetTickCount will wrap when the system has been up for 49.7 days
* Need to evaluate use in a multi-threaded environment

TODO:
* Add Cancel ability?
* Find a better way to add custom fields? Right now, we use member variables
  and forumulate the SQL via hardcoded const (billing_run_id, center_group_id).
* Don't use recursion (WritePercent, SaveToDB). Unwind tail-end by using loops
  instead. Also, expose an iterator to walk the phase and subphase nodes.

Dan Miser, 11/29/05
}

unit OdPerfLog;

interface

uses
  Classes, Windows, SysUtils, Contnrs, DB;

type
  TOdPerfLog = class; // forward declaration

  TPhase = class
  private
    FSubPhases: TObjectList;
    FOwner: TOdPerfLog;
    FPhase: string;
    FPercent: Single;
    FPhaseTime: DWORD;
    FTotalTime: DWORD;
    FParentPhaseID: Integer;
    FPhaseID: Integer;
    function GetSubPhaseCount: Integer;
    function GetHasSubPhases: Boolean;
    function GetSubPhase(Index: Integer): TPhase;
    procedure SetSubPhase(Index: Integer; const Value: TPhase);
    function GetStartTime: TDateTime;
    function GetStopTime: TDateTime;
  public
    function AddSubPhase(APhase: string): TPhase;
    property SubPhases[Index: Integer]: TPhase read GetSubPhase write SetSubPhase; default;
    property SubPhaseCount: Integer read GetSubPhaseCount;
    property HasSubPhases: Boolean read GetHasSubPhases;

    property PhaseID: Integer read FPhaseID;
    property ParentPhaseID: Integer read FParentPhaseID;
    property Phase: string read FPhase write FPhase;
    property PhaseTime: DWORD read FPhaseTime;
    property TotalTime: DWORD read FTotalTime;
    property StartTime: TDateTime read GetStartTime;
    property StopTime: TDateTime read GetStopTime;
    property Percent: Single read FPercent;
    function GetLastPhase: TPhase;
    constructor Create(AOwner: TOdPerfLog; APhaseID: Integer; APhase: string);
    destructor Destroy; override;
  end;

  TOdPerfLog = class
  private
    FRunning: Boolean;
    FStopping: Boolean;
    FPhases: TObjectList;
    FComputePercent: Boolean;
    FLastTime: DWORD;
    FTotalTime: DWORD;
    FStartTime: TDateTime;
    FPhaseID: Integer;
    FParentPhaseID: Integer;
    FPercentWithChildren: Boolean; // Not yet implemented
    function GetCount: Integer;
    function GetPhase(Index: Integer): TPhase;
    procedure SetPhase(Index: Integer; const Value: TPhase);
  protected
    procedure DoComputePercent;
    procedure UpdatePhaseTime(Phase: TPhase);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Start(const Phase: string = ''): TPhase; virtual;
    procedure Stop; virtual;
    property PercentWithChildren: Boolean read FPercentWithChildren write FPercentWithChildren;
    property ComputePercent: Boolean read FComputePercent write FComputePercent;
    property Count: Integer read GetCount;
    property Phases[Index: Integer]: TPhase read GetPhase write SetPhase; default;
    property Running: Boolean read FRunning;
    property StartTime: TDateTime read FStartTime;
    property Stopping: Boolean read FStopping;
    property TotalTime: DWORD read FTotalTime;
  end;

type
  TOdDBPerfLog = class(TOdPerfLog)
  private
    FDataSet: TDataSet;
    FCenterGroupID: Integer;
    FBillingRunID: Integer;
    procedure SetDataSet(const Value: TDataSet);
  protected
    procedure DoSaveToDB;
  public
    procedure Stop; override;
    property DataSet: TDataSet read FDataSet write SetDataSet;
    property BillingRunID: Integer read FBillingRunID write FBillingRunID;
    property CenterGroupID: Integer read FCenterGroupID write FCenterGroupID;
  end;

implementation

uses
  Variants;

function MSToDateTime(Ticks: DWORD): TDateTime;
var
  Hour: Word;
  Min: Word;
  Sec: Word;
  MS: Word;
const
  MSecsPerSec = 1000;
  MSecsPerMinute = MSecsPerSec * 60;
  MSecsPerHour = MSecsPerMinute * 60;
begin
  // Take the total milliseconds and normalize the value into H,M,S,MS
  Hour := Ticks div MSecsPerHour;
  Min := (Ticks - Hour * MSecsPerHour) div MSecsPerMinute;
  Sec := (Ticks - ((Hour * MSecsPerHour) + (Min * MSecsPerMinute))) div MSecsPerSec;
  MS := (Ticks - ((Hour * MSecsPerHour) + (Min * MSecsPerMinute) + (Sec * MSecsPerSec)));
  Result := EncodeTime(Hour, Min, Sec, MS);
end;

{ TPhase }

function TPhase.GetHasSubPhases: Boolean;
begin
  Result := FSubPhases.Count > 0;
end;

function TPhase.GetStartTime: TDateTime;
begin
  Assert(Assigned(FOwner));
  Result := FOwner.StartTime + MSToDateTime(FTotalTime);
end;

function TPhase.GetStopTime: TDateTime;
begin
  Result := GetStartTime + MSToDateTime(FPhaseTime);
end;

function TPhase.GetSubPhase(Index: Integer): TPhase;
begin
  Result := TPhase(FSubPhases[Index]);
end;

function TPhase.GetSubPhaseCount: Integer;
begin
  Result := FSubPhases.Count;
end;

procedure TPhase.SetSubPhase(Index: Integer; const Value: TPhase);
begin
  FSubPhases[Index] := Value;
end;

function TPhase.AddSubPhase(APhase: string): TPhase;
begin
  Inc(FOwner.FPhaseID);
  Result := TPhase.Create(FOwner, FOwner.FPhaseID, APhase);
  Result.FParentPhaseID := FPhaseID;
  FOwner.UpdatePhaseTime(Result);
  FSubPhases.Add(Result);
end;

constructor TPhase.Create(AOwner: TOdPerfLog; APhaseID: Integer; APhase: string);
begin
  FPhase := APhase;
  FPhaseID := APhaseID;
  FOwner := AOwner;
  FSubPhases := TObjectList.Create;
end;

destructor TPhase.Destroy;
begin
  FreeAndNil(FSubPhases);
  inherited Destroy;
end;

function TPhase.GetLastPhase: TPhase;
begin
  if Self.HasSubPhases then
    Result := SubPhases[SubPhaseCount - 1].GetLastPhase
  else
    Result := Self;
end;

{ TOdPerfLog }
function TOdPerfLog.GetCount: Integer;
begin
  Result := FPhases.Count;
end;

function TOdPerfLog.GetPhase(Index: Integer): TPhase;
begin
  Result := TPhase(FPhases[Index]);
end;

procedure TOdPerfLog.SetPhase(Index: Integer; const Value: TPhase);
begin
  FPhases[Index] := Value;
end;

procedure TOdPerfLog.DoComputePercent;
var
  i: Integer;

  procedure WritePercent(Node: TPhase);
  var
    j: Integer;
  begin
    Node.FPercent := Node.FPhaseTime / TotalTime * 100;
    for j := 0 to Node.FSubPhases.Count - 1 do
      WritePercent(Node.SubPhases[j]);
  end;

begin
  Assert(TotalTime > 0);
  for i := 0 to Count - 1 do
    WritePercent(Phases[i]);
end;

procedure TOdPerfLog.UpdatePhaseTime(Phase: TPhase);
var
  CurTime: DWORD;
begin
  CurTime := GetTickCount;
  Phase.FTotalTime := FTotalTime;
  Inc(FTotalTime, CurTime - FLastTime);
  Phase.FPhaseTime := CurTime - FLastTime;
  FLastTime := CurTime;
end;

constructor TOdPerfLog.Create;
begin
  inherited Create;
  FPhases := TObjectList.Create(True);
  FComputePercent := True;
  FPercentWithChildren := True;
end;

destructor TOdPerfLog.Destroy;
begin
  FreeAndNil(FPhases);
  inherited Destroy;
end;

function TOdPerfLog.Start(const Phase: string = ''): TPhase;
begin
  Result := nil;

  // Prevent Start from being called when we're stopping
  if FStopping then Exit;

  // If we're not running, then this is the first time in, so set things up
  if not FRunning then
  begin
    FPhaseID := 0;
    FParentPhaseID := 0;
    FTotalTime := 0;
    FPhases.Clear;
    FStartTime := Now;
    FLastTime := GetTickCount;
    FRunning := True;
  end;

  if Count > 0 then
    UpdatePhaseTime(Phases[Count - 1]);

  Inc(FPhaseID);
  Result := TPhase.Create(Self, FPhaseID, Phase);
  FPhases.Add(Result);
end;

procedure TOdPerfLog.Stop;
begin
  // Prevent Stop from being called without calling Start first
  if not FRunning then Exit;

  FStopping := True;
  try
    FRunning := False;
    UpdatePhaseTime(Phases[Count - 1]);
    if FComputePercent then
      DoComputePercent;
  finally
    FStopping := False;
  end;
end;

// Write the performance data to the DB. I intentionally flattened out the tables
// to make as few dependencies as possible on what needs to exist in the DB. The
// table must look like this:
//
// create table billing_performance_log (
//  billing_run_id int not null,
//  center_group_id int not null,
//  phase_id int not null,
//  parent_phase_id int,
//  phase_name varchar(128) not null,
//  start_time datetime not null,
//  phase_time int not null,
//  phase_percent numeric(10, 4))
procedure TOdDBPerfLog.DoSaveToDB;
var
  i: Integer;

  procedure ExecuteSQL(Phase: TPhase);
  begin
    Assert(Assigned(FDataSet));
    FDataSet.Insert;
    FDataSet.FieldByName('billing_run_id').AsInteger := FBillingRunID;
    FDataSet.FieldByName('center_group_id').AsInteger := FCenterGroupID;
    FDataSet.FieldByName('phase_id').AsInteger := Phase.PhaseID;
    FDataSet.FieldByName('parent_phase_id').AsInteger := Phase.ParentPhaseID;
    FDataSet.FieldByName('phase_name').AsString := Phase.Phase;
    FDataSet.FieldByName('start_time').AsDateTime := Phase.StartTime;
    FDataSet.FieldByName('phase_time').AsInteger := Phase.PhaseTime;
    FDataSet.FieldByName('phase_percent').AsFloat := Phase.Percent;
    FDataSet.Post;
  end;

  procedure WriteData(Node: TPhase);
  var
    i: Integer;
  begin
    ExecuteSQL(Node);
    for I := 0 to Node.FSubPhases.Count - 1 do
      WriteData(Node.SubPhases[i]);
  end;

begin
  for i := 0 to Count - 1 do
    WriteData(Phases[i]);
end;

procedure TOdDBPerfLog.SetDataSet(const Value: TDataSet);
begin
  FDataSet := Value;
end;

procedure TOdDBPerfLog.Stop;
begin
  inherited;
  if Assigned(FDataSet) then begin
    if (not Assigned(FDataSet)) or (not FDataSet.Active) or (FBillingRunID = 0) or (FCenterGroupID = 0) then
      raise Exception.Create('TOdDBPergLog settings not defined properly');
    DoSaveToDB;
  end;
end;

end.

