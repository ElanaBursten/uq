inherited AckMessagePromptDialog: TAckMessagePromptDialog
  Caption = 'Prompt'
  PixelsPerInch = 96
  TextHeight = 13
  inherited MessageMemo: TMemo
    Height = 225
    TabStop = False
  end
  inherited ButtonPanel: TPanel
    TabOrder = 2
  end
  object PromptPanel: TPanel
    Left = 0
    Top = 240
    Width = 392
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    Caption = '  '
    TabOrder = 1
    object PromptLabel: TLabel
      Left = 8
      Top = 5
      Width = 59
      Height = 13
      Caption = 'PromptLabel'
    end
    object PromptEdit: TMaskEdit
      Left = 8
      Top = 20
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'PromptEdit'
    end
  end
end
