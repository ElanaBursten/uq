unit FormatCriticalDisplayText;

interface

uses SysUtils, Classes, PerlRegEx;

type
  EHighlightConfigError = class(Exception);

  THighlighter = class;

  TKeywordDecorator = class(TObject)
  private
    HighlightRules: TStringList;
    FPlainText: string;
    FCaption: string;
  public
    property PlainText: string read FPlainText write FPlainText;
    property Caption: string read FCaption write FCaption;
    procedure AddHighlightRule(const Expression, ColorName, ColorCode: string; IsBold: Boolean; const RegExOptions: string = ''; WhichMatch: Integer = 0);
    function AsStyledText: string;
    function AsHTMLParagraph: string;
    function StyleDefinitionBlock: string;
    procedure ClearHighlightRules;
    constructor Create;
    destructor Destroy; override;
  end;

  THighlighter = class(TObject)
  private
    procedure Validate;
  public
    Bold: Boolean;
    BackgroundColorName: string;
    BackgroundColorCode: string;
    RegEx: string;
    WhichMatch: Integer;
    RegExOptions: TPerlRegExOptions;
    constructor Create;
    procedure SetStyle(const ColorName, ColorCode: string; IsBold: Boolean);
    procedure SetRegEx(const Expression, Options: string; MatchGroupNum: Integer);
    function StyleName: string;
    function StyleDefinition: string;
    function SearchGroupCount: Integer;
  end;

implementation

uses JclStrings, OdMiscUtils;

{ TKeywordDecorator }

constructor TKeywordDecorator.Create;
begin
  inherited Create;
  HighlightRules := TStringList.Create;
  FCaption := '';
  FPlainText := '';
end;

destructor TKeywordDecorator.Destroy;
begin
  ClearHighlightRules;
  FreeAndNil(HighlightRules);
  inherited Destroy;
end;

procedure TKeywordDecorator.ClearHighlightRules;
var
  i: Integer;
begin
  for i := 0 to HighlightRules.Count - 1 do begin
    HighlightRules.Objects[i].Free;
    HighlightRules.Objects[i] := nil;
  end;
  HighlightRules.Clear;
end;

procedure TKeywordDecorator.AddHighlightRule(const Expression, ColorName, ColorCode: string; IsBold: Boolean; const RegExOptions: string; WhichMatch: Integer);
var
  Highlighter: THighlighter;
begin
  Highlighter := THighlighter.Create;
  Highlighter.SetStyle(ColorName, ColorCode, IsBold);
  Highlighter.SetRegEx(Expression, RegExOptions, WhichMatch);
  HighlightRules.AddObject(Expression, Highlighter);
end;

function TKeywordDecorator.AsStyledText: string;
const
  ErrorRegex = 'Regex missing. Highlight rule could not be properly applied.';
var
  RE: TPerlRegEx;
  HL: THighlighter;
  RuleIdx: Integer;
  GroupIdx: Integer;
begin
  Result := FPlainText;
  RE := TPerlRegEx.Create(nil);
  try
    RE.Subject := FPlainText;
    RE.Replacement := '';
    for RuleIdx := 0 to HighlightRules.Count - 1 do begin
      HL := HighlightRules.Objects[RuleIdx] as THighlighter;
      //If missing regex then calling Match will raise an exception; instead make
      //a temporary regex to highlight an added error message that allows the user
      //to proceed with their work while informing them of the issue.
      if IsEmpty(HL.RegEx) then begin
        HL.RegEx := '(' + ErrorRegex +')';
        RE.Subject := FPlainText + '<br>' + ErrorRegex;
      end;
      RE.RegEx := HL.RegEx;
      RE.Options := HL.RegExOptions;
      if HL.WhichMatch = 0 then
        RE.Replacement := Format('<span class="%s">\0</span>', [HL.StyleName])
      else begin
        for GroupIdx := 1 to HL.SearchGroupCount do begin
          if GroupIdx = HL.WhichMatch then
            RE.Replacement := Format('%s<span class="%s">\%d</span> ', [RE.Replacement, HL.StyleName, HL.WhichMatch])
          else
            RE.Replacement := Format('%s\%d ', [RE.Replacement, GroupIdx]);
        end;
      end;
      RE.Replacement := Trim(RE.Replacement);

      if RE.Match then begin
        RE.ReplaceAll;
        Result := RE.Subject;
      end;
    end;
  finally
    FreeAndNil(RE);
  end;
end;

function TKeywordDecorator.AsHTMLParagraph: string;
begin
  Result := '<h1>' + Caption + '</h1><blockquote>' + AsStyledText + '</blockquote>';
end;

function TKeywordDecorator.StyleDefinitionBlock: string;
var
  i: Integer;
  HL: THighlighter;
begin
  Result := '';
  for i := 0 to HighlightRules.Count - 1 do begin
    HL := (HighlightRules.Objects[i] as THighlighter);
    if Pos(HL.StyleName, Result) = 0 then
      Result := Result + HL.StyleDefinition;
  end;
end;

{ THighlighter }

constructor THighlighter.Create;
begin
  inherited Create;
end;

function THighlighter.StyleName: string;
begin
  Validate;
  Result := BackgroundColorName;
  if Bold then
    Result := 'Bold' + Result;
  Result := Result + 'Highlight';
end;

function THighlighter.SearchGroupCount: Integer;
begin
  Result := StrCharCount(RegEx, '(');
end;

procedure THighlighter.SetRegEx(const Expression, Options: string; MatchGroupNum: Integer);
begin
  RegEx := Expression;
  WhichMatch := MatchGroupNum;
  RegExOptions := [];
  if StrContainsText('i', Options) then
    RegExOptions := RegExOptions + [preCaseLess];
  if StrContainsText('m', Options) then
    RegExOptions := RegExOptions + [preMultiLine];
  if StrContainsText('s', Options) then
    RegExOptions := RegExOptions + [preSingleLine];
  if StrContainsText('x', Options) then
    RegExOptions := RegExOptions + [preExtended];
  if StrContainsText('A', Options) then
    RegExOptions := RegExOptions + [preAnchored];
  if StrContains('*?', Expression) or StrContains(',?', Expression) or
    StrContains('+?', Expression) then
    RegExOptions := RegExOptions + [preUngreedy];
end;

procedure THighlighter.SetStyle(const ColorName, ColorCode: string; IsBold: Boolean);
begin
  BackgroundColorName := ColorName;
  BackgroundColorCode := ColorCode;
  Bold := IsBold;
  Validate;
end;

function THighlighter.StyleDefinition: string;
var
  BoldProp: string;
  ColorProp: string;
begin
  Validate;
  BoldProp := '';
  ColorProp := '';
  if Bold then
    BoldProp := 'font-weight: bold;';
  if NotEmpty(BackgroundColorName) then
    ColorProp := 'background-color: ' + BackgroundColorCode + ';';
  Result := '.' + StyleName + ' {' + BoldProp + ColorProp + '}';
end;

procedure THighlighter.Validate;
begin
  if IsEmpty(BackgroundColorName) and (not Bold) then
    raise EHighlightConfigError.Create('A highlighter must have either a color or Bold specified.');
  if NotEmpty(BackgroundColorName) and IsEmpty(BackgroundColorCode) then
    raise EHighlightConfigError.Create('A color highlighter must have a color code specified.');
end;

end.
