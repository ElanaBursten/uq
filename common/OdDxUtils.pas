unit OdDxUtils;

interface

uses SysUtils, Db, DxGrDate, dxDBCtrl, OdExceptions, dxTL, dxDBGrid, dxDBTLCL,
  dxEdLib;

function FocusFirstGridRow(Grid: TdxDBGrid; FocusGrid: Boolean = True): Boolean;
procedure SortGridByFirstVisibleColumn(Grid: TdxDBGrid; Ascending: Boolean = True);
procedure PrintDXGrid(Grid: TdxDBGrid);
procedure VerifyMinimumSQLServerDate(DateField: TField);
procedure SortGridByFieldNames(Grid: TCustomdxDBTreeListControl; SortColumns: array of string; SortDirection: array of TdxTreeListColumnSort);
function FindGridColumn(Grid: TCustomdxDBTreeListControl; Row, Column: Integer): TdxDBTreeListColumn;
procedure SetGridPopupColumnEnabled(Column: TdxDBTreeListDropDownColumn; Enabled: Boolean);
function GetDxComboObjectInteger(Combo: TdxPickEdit; Required: Boolean=True): Integer;
function SelectDxComboBoxItemFromObjects(ComboBox: TdxPickEdit; ID: Integer): Boolean;

implementation

uses OdMiscUtils;

function FocusFirstGridRow(Grid: TdxDBGrid; FocusGrid: Boolean): Boolean;
begin
  Assert(Assigned(Grid));
  Result := False;
  if Assigned (Grid.TopNode) then begin
    Grid.TopNode.MakeVisible;
    Grid.TopNode.Focused := True;
    if FocusGrid and Grid.CanFocus then
      Grid.SetFocus;
    Result := True;
  end;
end;

procedure SortGridByFirstVisibleColumn(Grid: TdxDBGrid; Ascending: Boolean);
var
  i: Integer;
  SeenVisible: Boolean;
  Col: TdxDBTreeListColumn;
begin
  Assert(Assigned(Grid));

  SeenVisible := False;
  Grid.BeginSorting;
  try
    for i := 0 to Grid.ColumnCount - 1 do begin
      Col := Grid.Columns[i];
      Col.Sorted := csNone;
      if not SeenVisible and Col.Visible then begin
        SeenVisible := True;
        if Ascending then
          Col.Sorted := csUp
        else
          Col.Sorted := csDown;
      end;
    end;
  finally
    Grid.EndSorting;
  end;
end;

procedure PrintDXGrid(Grid: TdxDBGrid);
var
  FileName: string;
begin
  Assert(Assigned(Grid));
  FileName := GetWindowsTempPath + 'DXGridPrint.html';
  Grid.SaveToHTML(FileName, True);
  OdShellExecute(FileName);
end;

procedure VerifyMinimumSQLServerDate(DateField: TField);
var
  MinDate: TDateTime;
begin
  Assert(Assigned(DateField), 'DateField must be assigned first.');
  if (DateField.IsNull) or (DateField.AsDateTime = NullDate) then
    Exit;

  MinDate := EncodeDate(1950, 1, 1);
  if (DateField.AsDateTime < MinDate) then
    raise EOdDataEntryError.CreateFmt('Date field %s cannot occur before %s.',
      [DateField.DisplayName, DateToStr(MinDate)]);
end;

procedure SortGridByFieldNames(Grid: TCustomdxDBTreeListControl; SortColumns: array of string; SortDirection: array of TdxTreeListColumnSort);
var
  i: Integer;
  Column: TdxDBTreeListColumn;
begin
  Assert(Assigned(Grid));
  Assert(Length(SortColumns) = Length(SortDirection));
  Grid.BeginSorting;
  try
    Grid.ClearColumnsSorted;
    for i := Low(SortColumns) to High(SortColumns) do begin
      Column := Grid.FindColumnByFieldName(SortColumns[i]);
      if Assigned(Column) then
        Column.Sorted := SortDirection[i];
    end;
  finally
    Grid.EndSorting;
  end;
end;

function FindGridColumn(Grid: TCustomdxDBTreeListControl; Row, Column: Integer): TdxDBTreeListColumn;
var
  i: Integer;
begin
  Assert(Assigned(Grid));
  Assert((Row >= 0) and (Column >=0));
  Result := nil;
  for i := 0 to Grid.ColumnCount - 1 do begin
    if (Grid.Columns[i].RowIndex = Row) and (Grid.Columns[i].ColIndex = Column) then begin
      Result := Grid.Columns[i];
      Break;
    end;
  end;
end;

procedure SetGridPopupColumnEnabled(Column: TdxDBTreeListDropDownColumn; Enabled: Boolean);
begin
  Assert(Assigned(Column));
  if Enabled then begin
    Column.DisableEditor := False;
    Column.ShowButtonStyle := sbDefault;
  end else begin
    Column.DisableEditor := True;
    Column.ShowButtonStyle := sbNone;
  end;
end;

function GetDxComboObjectInteger(Combo: TdxPickEdit; Required: Boolean): Integer;
begin
  Assert(Assigned(Combo));
  if (Combo.ItemIndex < 0) then begin
    if Required then
      raise Exception.Create('No selected combobox item for ' + Combo.Name)
    else
      Result := -1;
  end
  else
    Result := Integer(Combo.Items.Objects[Combo.ItemIndex]);
end;

function SelectDxComboBoxItemFromObjects(ComboBox: TdxPickEdit; ID: Integer): Boolean;
var
  i: Integer;
begin
  for i := 0 to ComboBox.Items.Count - 1 do
    if Integer(ComboBox.Items.Objects[i]) = ID then begin
      ComboBox.ItemIndex := i;
      Result := True;
      Exit;
    end;

  ComboBox.ItemIndex := -1;
  Result := False;
end;

end.
