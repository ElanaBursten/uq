unit OdDateRangeDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TDateRange = (drToday, drYesterday, drThisWeek, drLastWeek, drMonthToDate, drYearToDate,
    drCurrentMonth, drCurrentYear, drPreviousMonth, drPreviousYear, drLastMonth,
    drLast3Months, drLast6Months, drLastYear, drAllDates);

  TDateRangeDialog = class(TForm)
    StartCal: TMonthCalendar;
    EndCal: TMonthCalendar;
    DateRangeCombo: TComboBox;
    DateRangeLabel: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RangeLabel: TLabel;
    OKButton: TButton;
    CancelButton: TButton;
    procedure StartCalClick(Sender: TObject);
    procedure EndCalClick(Sender: TObject);
    procedure DateRangeComboChange(Sender: TObject);
    procedure CalGetMonthInfo(Sender: TObject; Month: Cardinal; var MonthBoldInfo: Cardinal);
    procedure EndCalDblClick(Sender: TObject);
    procedure StartCalDblClick(Sender: TObject);
  private
    procedure SelectDateRange(Range: TDateRange);
    procedure ForceUpdate(Cal: TMonthCalendar);
  protected
    procedure UpdateDisplay;
  public
    constructor Create(AOwner: TComponent); override;
    class function Execute(var StartDate, EndDate: TDateTime; DisplayDate: TDateTime = 0; ExtendEndDate: Boolean = False): Boolean;
  end;

implementation

uses
  Math, DateUtils, CommCtrl, OdVclUtils;

const
  RangeToday = 'Today';
  RangeYesterday = 'Yesterday';
  RangeThisWeek = 'This Week';
  RangeLastWeek = 'Last Week';
  RangeMonthToDate = 'Month to Date';
  RangeYearToDate = 'Year to Date';
  RangeCurrentMonth = 'Current Month';
  RangeCurrentYear = 'Current Year';
  RangePreviousMonth = 'Previous Month';
  RangePreviousYear = 'Previous Year';
  RangeLastMonth = 'Last Month';
  RangeLast3Months = 'Last 3 Months';
  RangeLast6Months = 'Last 6 Months';
  RangeLastYear = 'Last Year';
  RangeAllDates = 'All Dates';

  DateRangeDescriptions: array[TDateRange] of string = (
    RangeToday,
    RangeYesterday,
    RangeThisWeek,
    RangeLastWeek,
    RangeMonthToDate,
    RangeYearToDate,
    RangeCurrentMonth,
    RangeCurrentYear,
    RangePreviousMonth,
    RangePreviousYear,
    RangeLastMonth,
    RangeLast3Months,
    RangeLast6Months,
    RangeLastYear,
    RangeAllDates);

{$R *.dfm}

constructor TDateRangeDialog.Create(AOwner: TComponent);
var
  i: TDateRange;
begin
  inherited;
  DateRangeCombo.DropDownCount := Min(20, Length(DateRangeDescriptions));
  for i := Low(TDateRange) to High(TDateRange) do
    DateRangeCombo.AddItem(DateRangeDescriptions[i], TObject(Ord(i)));
  SelectDateRange(drToday);
  DateRangeCombo.ItemIndex := -1;
end;

procedure TDateRangeDialog.DateRangeComboChange(Sender: TObject);
begin
  if DateRangeCombo.ItemIndex > -1 then
    SelectDateRange(TDateRange(Integer(DateRangeCombo.Items.Objects[DateRangeCombo.ItemIndex])));
end;

procedure TDateRangeDialog.EndCalClick(Sender: TObject);
begin
  if EndCal.Date < StartCal.Date then
    StartCal.Date := EndCal.Date;
  UpdateDisplay;
end;

procedure TDateRangeDialog.EndCalDblClick(Sender: TObject);
begin
  if not IsMonthCalendarCursorInTitle(EndCal) then
    OKButton.Click;
end;

class function TDateRangeDialog.Execute(var StartDate, EndDate: TDateTime; DisplayDate: TDateTime;
  ExtendEndDate: Boolean): Boolean;
var
  Dialog: TDateRangeDialog;
  InitialDate: TDateTime;
begin
  Dialog := TDateRangeDialog.Create(nil);
  try
    InitialDate := Today;
    if DisplayDate > 0 then
      InitialDate := Trunc(DisplayDate);
    if (StartDate <= 0) and (EndDate <= 0) then begin
      Dialog.StartCal.Date := InitialDate;
      Dialog.EndCal.Date := InitialDate;
    end
    else begin
      Dialog.StartCal.Date := StartDate;
      Dialog.EndCal.Date := EndDate;
      if Dialog.StartCal.Date > Dialog.EndCal.Date then
        Dialog.EndCal.Date := Dialog.StartCal.Date;
    end;
    if (Trunc(Dialog.StartCal.Date) = Today) and (Trunc(Dialog.EndCal.Date) = Today) then
      Dialog.DateRangeCombo.ItemIndex := Ord(drToday)
    else if (Trunc(Dialog.StartCal.Date) = Yesterday) and (Trunc(Dialog.EndCal.Date) = Yesterday) then
      Dialog.DateRangeCombo.ItemIndex := Ord(drYesterday);
    Dialog.UpdateDisplay;
    Result := (Dialog.ShowModal = mrOK);
    if Result then begin
      StartDate := Trunc(Dialog.StartCal.Date);
      EndDate := Trunc(Dialog.EndCal.Date);
      if ExtendEndDate then
        EndDate := EndDate + 1;
    end;
  finally
    FreeAndNil(Dialog);
  end;
end;

procedure TDateRangeDialog.ForceUpdate(Cal: TMonthCalendar);
var
  Hdr: TNMDayState;
  Days: array of TMonthDayState;
  Range: array[1..2] of TSystemTime;
begin
  // Populate Days array (from TntControls)
  Hdr.nmhdr.hwndFrom := Cal.Handle;
  Hdr.nmhdr.idFrom := 0;
  Hdr.nmhdr.code := MCN_GETDAYSTATE;
  Hdr.cDayState := MonthCal_GetMonthRange(Cal.Handle, GMR_DAYSTATE, @Range[1]);
  Hdr.stStart := Range[1];
  SetLength(Days, Hdr.cDayState);
  Hdr.prgDayState := @Days[0];
  SendMessage(Cal.Handle, CN_NOTIFY, Cal.Handle, Integer(@Hdr));
  // Update day state
  SendMessage(Cal.Handle, MCM_SETDAYSTATE, Hdr.cDayState, Longint(Hdr.prgDayState))
end;

procedure TDateRangeDialog.SelectDateRange(Range: TDateRange);

  procedure SetRange(StartDate, EndDate: TDateTime);
  begin
    StartCal.Date := Trunc(StartDate);
    EndCal.Date := Trunc(EndDate);
  end;

begin
  case Range of
    drToday:         SetRange(Today, Today);
    drYesterday:     SetRange(Yesterday, Yesterday);
    drThisWeek:      SetRange(StartOfTheWeek(Today) - 1, EndOfTheWeek(Today) - 1); // Weeks start on Sunday for us, not the ISO Monday
    drLastWeek:      SetRange(StartOfTheWeek(Today) - 8, EndOfTheWeek(Today) - 8);
    drMonthToDate:   SetRange(StartOfTheMonth(Today), Today);
    drYearToDate:    SetRange(StartOfTheYear(Today), Today);
    drCurrentMonth:  SetRange(StartOfTheMonth(Today), EndOfTheMonth(Today));
    drCurrentYear:   SetRange(StartOfTheYear(Today), EndOfTheYear(Today));
    drPreviousMonth: SetRange(IncMonth(StartOfTheMonth(Today), -1), EndOfTheMonth(IncMonth(StartOfTheMonth(Today), -1)));
    drPreviousYear:  SetRange(IncYear(StartOfTheYear(Today), -1), EndOfTheYear(IncYear(StartOfTheYear(Today), -1)));
    drLastMonth:     SetRange(IncMonth(Today, -1), Today);
    drLast3Months:   SetRange(IncMonth(Today, -3), Today);
    drLast6Months:   SetRange(IncMonth(Today, -6), Today);
    drLastYear:      SetRange(IncMonth(Today, -12), Today);
    drAllDates:      SetRange(EncodeDate(1900, 1, 1), EndOfTheYear(EncodeDate(2099, 1, 1)));
    else
      raise Exception.Create('Unknown date range.');
  end;
  UpdateDisplay;
end;

procedure TDateRangeDialog.StartCalClick(Sender: TObject);
begin
  if StartCal.Date > EndCal.Date then
    EndCal.Date := StartCal.Date;
  UpdateDisplay;
end;

procedure TDateRangeDialog.StartCalDblClick(Sender: TObject);
begin
  if not IsMonthCalendarCursorInTitle(StartCal) then
    OKButton.Click;
end;

procedure TDateRangeDialog.CalGetMonthInfo(Sender: TObject; Month: Cardinal;
  var MonthBoldInfo: Cardinal);
var
  Days: array of Cardinal;
  i: Word;
  Calendar: TMonthCalendar;
  CalendarDate: TDateTime;
  CalendarYear: Word;
  CalendarMonth: Word;
  CheckDate: TDateTime;
begin
  Calendar := (Sender as TMonthCalendar);
  CalendarDate := Trunc(Calendar.Date);
  CalendarYear := YearOf(CalendarDate);
  CalendarMonth := Month;
  for i := 1 to DaysInMonth(CalendarDate) do begin
    if IsValidDate(CalendarYear, CalendarMonth, i) then begin
      CheckDate := EncodeDate(CalendarYear, CalendarMonth, i);
      if (CheckDate >= Trunc(StartCal.Date)) and (CheckDate <= Trunc(EndCal.Date)) then begin
        SetLength(Days, Length(Days) + 1);
        Days[Length(Days) - 1] := i;
      end;
    end;
  end;
  if Length(Days) > 0 then
    Calendar.BoldDays(Days, MonthBoldInfo);
end;

procedure TDateRangeDialog.UpdateDisplay;
begin
  RangeLabel.Caption := 'Selected Dates: ' + FormatDateTime(ShortDateFormat, StartCal.Date) + ' - ' + FormatDateTime(ShortDateFormat, EndCal.Date);
  ForceUpdate(StartCal);
  ForceUpdate(EndCal);
end;

end.
