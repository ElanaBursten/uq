unit OdDataSetToXml;

interface

uses Classes, DB;

function ConvertDataSetToXML(DataSets: array of TDataSet;
  MaxRows: Integer; IDFieldName: string = ''): string;

function ConvertDataSetsToSQLXML(DataSets: array of TDataSet;
  Names: array of string): string;

function XmlValue(Field: TField): string;

procedure XmlEscape(const Str: string; Output: TStream); overload;
function XmlEscape(const Str: string): string; overload;

const
  XmlNullValue = 'XmlNull';

implementation

uses SysUtils, OdIsoDates, TypInfo, OdMiscUtils;

function XmlValue(Field: TField): string;
begin
  if Field.IsNull then
    Result := XmlNullValue
  else if Field.DataType = ftDateTime then
    Result := IsoDateTimeToStr(Field.AsDateTime)
  else if Field.DataType = ftBoolean then begin
    if Field.AsBoolean then
      Result := '1'
    else
      Result := '0';
  end else
    Result := Field.AsString;
end;

// This is a modified from a example here: http://codecentral.borland.com/codecentral/ccweb.exe/listing?id=16272
// Optimized for speed instead of readability or maintainability
function XmlEscape(const Str: string): string;
var
  i: Integer;
  Buf, P: PChar;
  ch: Integer;
begin
  Result := '';
  if Length(Str) = 0 then
    Exit;
  // TODO: Unicode compatibility
  GetMem(Buf, (Length(Str) * 6) + 8); // to be on the *very* safe side
  try
    P := Buf;
    for i := 1 to Length(Str) do begin
      ch := Ord(Str[i]);
      case ch of
        0..8, 11, 12, 14..31: // Low-ASCII chars illegal in XML: http://www.w3.org/TR/xml/#charsets
          begin
            P^ := ' '; // replace with a space.
            Inc(P);
          end;
        34:            // quot
          begin
            Move('&quot;', P^, 6);
            Inc(P, 6);
          end;
        38:            // amp
          begin
            Move('&amp;', P^, 5);
            Inc(P, 5);
          end;
        39:            // apos
          begin
            Move('&apos;', P^, 6);
            Inc(P, 6);
          end;
        60:            // lt
          begin
            Move('&lt;', P^, 4);
            Inc(P, 4);
          end;
        62:            // gt
          begin
            Move('&gt;', P^, 4);
            Inc(P, 4);
          end;
        else
          begin
            P^ := Char(ch);
            Inc(P);
          end;
      end;
    end;
    SetString(Result, Buf, P - Buf);
  finally
    FreeMem(Buf);
  end;
end;

procedure XmlEscape(const Str: string; Output: TStream); overload;
var
  i: Integer;
  ch: Integer;
begin
  for i := 1 to Length(Str) do begin
    ch := Ord(Str[i]);
    case ch of
      0..8, 11, 12, 14..31:
        begin
          ch := Ord(' '); // replace illegal XML chars w/ space
          Output.Write(ch, 1);
        end;
      34: Output.Write('&quot;', 6);
      38: Output.Write('&amp;', 5);
      39: Output.Write('&apos;', 6);
      60: Output.Write('&lt;', 4);
      62: Output.Write('&gt;', 4);
    else  Output.Write(ch, 1)
    end;
  end;
end;

procedure WriteString(Stream: TStream; const Str: string);
begin
  WriteStringToStream(Stream, Str);
end;

function StreamAsString(Stream: TStream): string;
begin
  Stream.Size := Stream.Position;
  SetLength(Result, Stream.Size);
  Stream.Position := 0;
  Stream.ReadBuffer(Result[1], Stream.Size); // TODO: Fix unicode compatibility
end;

procedure WriteSchema(XML: TStream; Data: TDataSet);
var
  i: Integer;
  FieldDef: TFieldDef;
  FieldType: string;
  FieldAttributes: string;
begin
  Assert(Assigned(XML), 'No XML stream');
  WriteString(XML, '<schema>');

  Assert(Data <> nil, 'No Dataset');
  Assert(Data.FieldDefs <> nil, 'No FieldDefs');

  for i := 0 to Data.FieldDefs.Count - 1 do begin
    FieldDef := Data.FieldDefs[i];
    FieldType := XMLEscape(GetEnumName(TypeInfo(TFieldType), Ord(FieldDef.DataType)));
    FieldAttributes := XMLEscape(SetToString(GetPropInfo(FieldDef, 'Attributes'), Byte(FieldDef.Attributes)));
    WriteString(XML, Format('<field name="%s" type="%s" attributes="%s" size="%d" precision="%d"/>',
      [XMLEscape(FieldDef.Name), FieldType, FieldAttributes, FieldDef.Size, FieldDef.Precision]));
  end;
  WriteString(XML, '</schema>');
end;

function GetRowAttributes(Data: TDataSet): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Data.Fields.Count - 1 do begin
    if i = Data.Fields.Count - 1 then
      Result := Result + XMLEscape(Data.Fields[i].FieldName) + '="' + XMLEscape(XmlValue(Data.Fields[i])) + '"'
    else
      Result := Result + XMLEscape(Data.Fields[i].FieldName) + '="' + XMLEscape(XmlValue(Data.Fields[i])) + '" ';
  end
end;

function ConvertDataSetToXML(DataSets: array of TDataSet;
  MaxRows: Integer; IDFieldName: string = ''): string;
var
  Doc: TMemoryStream;
  Rows, RowsReturned: Integer;
  DataSetIndex: Integer;
  Data: TDataSet;
  LotsOfRows: Integer;
  IDsSeen: TList;
  ID: Integer;
  AlreadySeen: Boolean;
  RootAttributes: string;
  TotalRows: string;
begin
  Doc := TMemoryStream.Create;
  try
    Doc.Size := 1024 * 1024 * 4; // Allocate 4 MB
    Rows := 0;
    RowsReturned := 0;
    LotsOfRows := MaxRows * 3;

    for DataSetIndex := Low(DataSets) to High(DataSets) do begin
      Data := DataSets[DataSetIndex];
      if Data <> nil then begin
        WriteSchema(Doc, Data);
        Break; // We only write one schema
      end;
    end;

    IDsSeen := TList.Create;
    try
      for DataSetIndex := Low(DataSets) to High(DataSets) do begin
        Data := DataSets[DataSetIndex];
        if Data <> nil then begin
          while (not Data.EOF) and ((MaxRows = 0) or (Rows < MaxRows)) do begin
            if IDFieldName <> '' then begin
              ID := Data.FieldByName(IDFieldName).AsInteger;
              AlreadySeen := IDsSeen.IndexOf(Pointer(ID)) >= 0;
              if not AlreadySeen then
                IDsSeen.Add(Pointer(ID));
            end else
              AlreadySeen := False;

            if not AlreadySeen then begin
              WriteString(Doc, '<row ' + GetRowAttributes(Data) + '/>');
              Inc(Rows);
              Inc(RowsReturned);
            end;
            Data.Next;
            if (Rows mod 100) = 99 then
              Sleep(1); // Give up time slice
          end;
          while not (Data.EOF) and (Rows < LotsOfRows) do begin
            if (Rows mod 100) = 99 then
              Sleep(1); // Give up time slice
            Data.Next;
            Inc(Rows);
          end;
        end;
      end;
    finally
      FreeAndNil(IDsSeen);
    end;

    if Rows = LotsOfRows then
      TotalRows := 'MANY'
    else
      TotalRows := IntToStr(Rows);
    RootAttributes := Format('MaxRows="%d" ActualRows="%d" TotalRows="%s"', [MaxRows, RowsReturned, TotalRows]);
    WriteString(Doc, '</rows>');

    Doc.SetSize(Doc.Position + 1);

    Result := Format('<?xml version="1.0" encoding="%s"?>', [DefaultXMLEncoding]) + sLineBreak +
        '<rows '+ RootAttributes +'>'+ StreamAsString(Doc);
  finally
    FreeAndNil(Doc);
  end;
end;

function ConvertDataSetsToSQLXML(DataSets: array of TDataSet;
  Names: array of string): string;
var
  XML: TMemoryStream;
  DataSetIndex: Integer;
  Data: TDataSet;
  Rows: Integer;
begin
  Assert(High(DataSets) = High(Names), 'There must be a name for each dataset');

  XML := TMemoryStream.Create;
  try
    XML.Size := 1024 * 1024 * 5; // Allocate 5 MB by default
    WriteString(XML, Format('<?xml version="1.0" encoding="%s"?>', [DefaultXMLEncoding]) + sLineBreak);
    WriteString(XML, '<root>');
    for DataSetIndex := Low(DataSets) to High(DataSets) do begin
      Data := DataSets[DataSetIndex];
      Assert((Data <> nil) and (Data.Active) and (Trim(Names[DataSetIndex]) <> ''));
      Rows := 0;
      while (not Data.EOF) do begin
        WriteString(XML, '<' + XMLEscape(Names[DataSetIndex]) + ' ' + GetRowAttributes(Data) + '/>');
        Inc(Rows);
        if (Rows mod 100) = 99 then
          Sleep(1); // Give up time slice
        Data.Next;
      end;
    end;
    WriteString(XML, '</root>');
    Result := StreamAsString(XML);
  finally
    FreeAndNil(XML);
  end;
end;

end.

