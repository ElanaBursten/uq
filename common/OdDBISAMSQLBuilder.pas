unit OdDBISAMSQLBuilder;

interface

uses
  OdSQLBuilder;

type
  TOdDBISAMSqlBuilder = class(TOdSqlBuilder)
  public
    procedure AddSingleKeywordSearch(const FieldName, SearchString: string); override;
  end;

implementation

uses SysUtils;
{ TOdDBISAMSqlBuilder }

procedure TOdDBISAMSqlBuilder.AddSingleKeywordSearch(const FieldName, SearchString: string);
//DBISAM will only use case-sensitive indexes on string fields unless we tell
//DBISAM not to using the TDBISAMQuery.FilterOptions.foCaseInsensitive property.
//However - in the case of extended operators this is not supported and UPPER must be used instead.
//(from Elevate: The foCaseInsensitive option only works for the basic =, <>, >=, <=,
//operators, and not for the extended operators like LIKE, IN, etc.)
//http://www.elevatesoft.com/forums?action=view&category=dbisam&id=dbisam_general&msg=64693&start=1&keywords=case%20insensitive%20index&searchbody=True&forum=DBISAM_Announce&forum=DBISAM_General&forum=DBISAM_SQL&forum=DBISAM_CS&forum=DBISAM_ODBC&forum=DBISAM_Beta&forum=DBISAM_Linux&forum=DBISAM_Binaries&forum=DBISAM_Suggestions&forum=DBISAM_ThirdParty&forum=DBISAM_Discussion#64693
var
  Keyword: string;
begin
  Keyword := Trim(SearchString);
  if DoFullTextSearch(FieldName) then
    AddWhereCondition(Format('CONTAINS(%s, ''"%s"'')', [FieldName, Keyword]))
  else
    AddWhereCondition('UPPER('+ FieldName + ') LIKE UPPER(' + FQuoteChar + FLikeChar +
                      ConvertQuotes(Keyword) + FLikeChar + FQuoteChar + ')');
end;

end.
