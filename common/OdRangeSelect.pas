// Date range selector based on DevExpress TcxDateEdit component
unit OdRangeSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  SharedDevExStyles, ComCtrls, dxCore, cxDateUtils;

const
  // Named default ranges used to select a range using SelectDateRange
  rsCustom        = 00;
  rsToday         = 01;
  rsYesterday     = 02;
  rsLastWeek      = 03;
  rsThisWeek      = 04;
  rsMonthToDate   = 05;
  rsYearToDate    = 06;
  rsCurrentMonth  = 07;
  rsCurrentYear   = 08;
  rsPreviousMonth = 09;
  rsPreviousYear  = 10;
  rsLastMonth     = 11;
  rsLast3Months   = 12;
  rsLast6Months   = 13;
  rsLastYear      = 14;
  rsAllDates      = 15;
  rsDefLastRange  = rsAllDates;

type
  TOdDateRange = class
    FRangeName: string;
    FFromDate: TDateTime;
    FToDate: TDateTime;
  end;

  TOdDateRangeList = class(TList)
  protected
    function GetItem(I: Integer): TOdDateRange;
    procedure SetItem(I: Integer; Value: TOdDateRange);
  public
    function  Append(RangeName: string; FromDate, ToDate: TDateTime): TOdDateRange;
    property Items[I: Integer]: TOdDateRange read GetItem write SetItem; default;
  end;

  TOdRangeSelectFrame = class(TFrame)
    FromLabel: TLabel;
    ToLabel: TLabel;
    DatesLabel: TLabel;
    DatesComboBox: TComboBox;
    FromDateEdit: TcxDateEdit;
    ToDateEdit: TcxDateEdit;
    procedure ToDateEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure DatesComboBoxChange(Sender: TObject);
    procedure DateEditChange(Sender: TObject);
    procedure FromDateEditPropertiesChange(Sender: TObject);
  private
    Updating: Boolean;
    FWeekBeginsSunday: Boolean;
    procedure IndicateCustomDateRange;
  protected
    FDateRangeList: TOdDateRangeList;
    FOnChange: TNotifyEvent;
    procedure Loaded; override;
    procedure AddDefaultDateRanges;
    function  GetFromDate: TDateTime;
    function  GetToDate: TDateTime;
    procedure DoOnChange;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    // Call this to add a non-standard date range
    procedure AppendDateRange(RangeName: string; FromDate, ToDate: TDateTime);
    // Call this to manually select a date range
    procedure SelectDateRange(DateRange: Byte; CustomFromDate: TDateTime = 0;
      CustomToDate: TDateTime = 0);
    // Call this to select no date range
    procedure NoDateRange;
    function IsEmpty: Boolean;
    property FromDate: TDateTime read GetFromDate;
    property ToDate: TDateTime read GetToDate;
    property WeekBeginsSunday: Boolean read FWeekBeginsSunday write FWeekBeginsSunday;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

//============================================================================//

implementation

{$R *.dfm}

uses DateUtils, OdMiscUtils;

//TOdDateRangeList

function TOdDateRangeList.GetItem(I: Integer): TOdDateRange;
begin
  Result := TOdDateRange(inherited Items[I]);
end;

procedure TOdDateRangeList.SetItem(I: Integer; Value: TOdDateRange);
begin
  inherited Items[I] := Pointer(Value);
end;

function TOdDateRangeList.Append(RangeName: string; FromDate, ToDate: TDateTime): TOdDateRange;
begin
  Result := TOdDateRange.Create;

  Result.FRangeName := RangeName;
  Result.FFromDate := FromDate;
  Result.FToDate := ToDate;

  Add(Result);
end;

//TOdRangeSelectFrame

constructor TOdRangeSelectFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDateRangeList := TOdDateRangeList.Create;
end;

destructor TOdRangeSelectFrame.Destroy;
begin
  FreeAndNil(FDateRangeList);
  inherited Destroy;
end;

procedure TOdRangeSelectFrame.Loaded;
begin
  inherited;
  // Initialize the combo box
  AddDefaultDateRanges;
  SelectDateRange(rsYesterday);
  DatesComboBox.DropDownCount := rsDefLastRange+1;
  //We are assigning the glyph manually because the dfm's may produce different
  //binary image data based on the bit settings of the individual developer's monitor.
  SharedDevExStyleData.SetGlyph(FromDateEdit, 0);
  SharedDevExStyleData.SetGlyph(ToDateEdit, 0);
end;

procedure TOdRangeSelectFrame.AppendDateRange(RangeName: string; FromDate, ToDate: TDateTime);
begin
  FDateRangeList.Append(RangeName, FromDate, ToDate);
  DatesComboBox.Items.Append(RangeName);
end;

procedure TOdRangeSelectFrame.AddDefaultDateRanges;
var
  I: Integer;
  CurrentDate: TDateTime;
  Year, Month, Day: Word;
  FromDate, ToDate: TDateTime;

  function MakeDate(Y, M, D: Word): TDateTime;
  begin
    if not TryEncodeDate(Y, M, D, Result) then
      Result := CurrentDate;
  end;

begin
  // Get the current date in case we need it
  CurrentDate := Trunc(Now);
  DecodeDate(CurrentDate, Year, Month, Day);

  for I := rsCustom to rsDefLastRange do
    // Get the date range
    case I of
      rsCustom:
        AppendDateRange('Custom', 0, 0);
      rsToday:
        AppendDateRange('Today', CurrentDate, CurrentDate);
      rsYesterday:
        AppendDateRange('Yesterday', CurrentDate-1, CurrentDate-1);
      rsLastWeek:
        begin
          FromDate := StartOfTheWeek(CurrentDate-7);
          AppendDateRange('Last Week', FromDate, FromDate+6);
        end;
      rsThisWeek:
        begin
          FromDate := StartOfTheWeek(CurrentDate);
          AppendDateRange('This Week', FromDate, FromDate+6);
        end;
      rsMonthToDate:
        AppendDateRange('Month to Date', MakeDate(Year, Month, 1), CurrentDate);
      rsYearToDate:
        AppendDateRange(
          'Year to Date', MakeDate(Year, 1, 1), CurrentDate);
      rsCurrentMonth:
        begin
          FromDate := MakeDate(Year, Month, 1);
          ToDate := SysUtils.IncMonth(FromDate, 1)-1;
          AppendDateRange('Current Month', FromDate, ToDate);
        end;
      rsCurrentYear:
        AppendDateRange(
          'Current Year', MakeDate(Year, 1, 1), MakeDate(Year, 12, 31));
      rsPreviousMonth:
        begin
          FromDate := SysUtils.IncMonth(MakeDate(Year, Month, 1), -1);
          ToDate := SysUtils.IncMonth(FromDate, 1)-1;
          AppendDateRange('Previous Month', FromDate, ToDate);
        end;
      rsPreviousYear:
        AppendDateRange(
          'Previous Year', MakeDate(Year-1, 1, 1), MakeDate(Year-1, 12, 31));
      rsLastMonth:
        AppendDateRange(
          'Last Month', SysUtils.IncMonth(CurrentDate, -1), CurrentDate-1);
      rsLast3Months:
        AppendDateRange(
          'Last 3 Months', SysUtils.IncMonth(CurrentDate, -3), CurrentDate-1);
      rsLast6Months:
        AppendDateRange(
          'Last 6 Months', SysUtils.IncMonth(CurrentDate, -6), CurrentDate-1);
      rsLastYear:
        AppendDateRange(
          'Last Year', SysUtils.IncMonth(CurrentDate, -12), CurrentDate-1);
      rsAllDates:
        AppendDateRange('All Dates', 0, 0);
    end;
end;

procedure TOdRangeSelectFrame.ToDateEditPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  // Check for a To date that is earlier than the From date
  if ToDateEdit.Date < FromDateEdit.Date then begin
    Error := True;
    ErrorText := 'To date is earlier than From date';
  end;
end;

procedure TOdRangeSelectFrame.SelectDateRange(DateRange: Byte;
  CustomFromDate: TDateTime; CustomToDate: TDateTime);
begin
  if DateRange <> rsCustom then begin
    CustomFromDate := FDateRangeList[DateRange].FFromDate;
    CustomToDate := FDateRangeList[DateRange].FToDate;
  end;

  // Convert Monday weeks to Sunday weeks
  if WeekBeginsSunday and ((DateRange = rsLastWeek) or (DateRange = rsThisWeek)) then begin
    CustomFromDate := CustomFromDate - 1;
    CustomToDate := CustomToDate - 1;
  end;

  Updating := True;
  try
    if CustomFromDate = 0 then
      FromDateEdit.Date := NullDate
    else
      FromDateEdit.Date := CustomFromDate;

    if CustomToDate = 0 then
      ToDateEdit.Date := NullDate
    else
      ToDateEdit.Date := CustomToDate;

    DatesComboBox.ItemIndex := DateRange;
  finally
    Updating := False;
  end;
end;

procedure TOdRangeSelectFrame.NoDateRange;
begin
  FromDateEdit.Date := NullDate;
  ToDateEdit.Date := NullDate;
  DatesComboBox.ItemIndex := -1;
end;

procedure TOdRangeSelectFrame.DatesComboBoxChange(Sender: TObject);
begin
  SelectDateRange(DatesComboBox.ItemIndex);
end;

procedure TOdRangeSelectFrame.DateEditChange(Sender: TObject);
begin
  if not Updating then
    IndicateCustomDateRange;
end;

function TOdRangeSelectFrame.GetFromDate: TDateTime;
begin
  if FromDateEdit.Date <= 0 then
    Result := 0
  else
    Result := FromDateEdit.Date;
end;

function TOdRangeSelectFrame.GetToDate: TDateTime;
begin
  if ToDateEdit.Date <= 0 then
    Result := 0
  else
    Result := ToDateEdit.Date+1;
end;

function TOdRangeSelectFrame.IsEmpty: Boolean;
begin
  Result := FromDateEdit.Date = NullDate;
end;

procedure TOdRangeSelectFrame.IndicateCustomDateRange;
begin
  DatesComboBox.ItemIndex := rsCustom;
end;

procedure TOdRangeSelectFrame.FromDateEditPropertiesChange(Sender: TObject);
begin
  DoOnChange;
end;

procedure TOdRangeSelectFrame.DoOnChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TOdRangeSelectFrame.SetEnabled(Value: Boolean);
begin
  inherited;
  FromDateEdit.Enabled := Value;
  ToDateEdit.Enabled := Value;
  DatesComboBox.Enabled := Value;
end;

end.

