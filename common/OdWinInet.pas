unit OdWinInet;

// TODO: WinInet tasks remaining
//   Verify there are no memory leaks
//   Access type property for InternetOpen?
//   Enable/Disable IE download cache?
//   Test redirects and other recoverable status codes?

// NOTE: TOdHttp only supports deflate (zlib) compression.  Gzip is a more
// general form of compression than deflate.  Gzip uses deflate as its default
// compression algorithm, but it can use others, so it has a header in the data
// stream to define the type and other details.  We could add partial gzip
// support by reading the header information, making sure it specifies deflate
// compression, and then skipping past the header.  The zlib library implements
// deflate compression.  TOdHttp uses zlib 1.1.4 from LVK internally, obtained
// from: http://www.vkarlsen.no/LVK/
// For more deflate/gzip information, see: http://www.ietf.org/rfc/rfc1951.txt
// For zlib information, see: http://www.gzip.org/zlib/

interface

uses Windows, WinInet, Classes, ComCtrls, Controls;

type
  TOdNetError = (
    neAbort,          // User called Abort
    neInvalidURL,     // URL string invalid
    neInternetOpen,   // InternetOpen failed
    neCallback,       // InternetSetStatusCallback failed
    neSetOption,      // InternetSetOption failed
    neInternetConnect,// InternetConnect failed
    neOpenRequest,    // InternetOpenRequest failed
    neSendRequest,    // InternetSendRequest failed
    neOpenURL,        // InternetOpenUrl failed
    neQueryInfo,      // HttpQueryInfo failed
    neStatusCode,     // Bad HTTP status code (error or unsupported)
    neForbidden,      // 403: Happens when the connection limit is exceeded
    neReadFile,       // InternetReadFile failed
    neInflate,        // Inflate decompression failed (currupt data from server?)
    neException,      // Unknown exception happened (threaded mode only)
    neURLTooLong      // URL string too long for WinInet (not used yet)
  );

  TOdTransferStatus = (
    nsNotConnected,    // Not connected
    nsAborting,        // Aborting
    nsClosing,         // Closing connection
    nsClosed,          // Closed connection
    nsInternetOpen,    // InternetOpen
    nsInternetConnect, // InternetConnect
    nsConnecting,      // Connecting to remote socket
    nsConnected,       // Connected to remote socket
    nsResolving,       // Resolving hostname (DNS)
    nsResolved,        // Resolved hostname (DNS)
    nsOpenRequest,     // InternetOpenRequest
    nsSendRequest,     // InternetSendRequest
    nsOpenURL,         // InternetOpenUrl
    nsQueryInfo,       // HttpQueryInfo
    nsReadFile,        // InternetReadFile
    nsDecompressing    // zlib decompression
  );

  TOdTransferMode = (
    tmUnknown,    // Unknown operation
    tmHttp,       // HTTP download
    tmHttpPost    // HTTP Post
  );

  TOdHttp = class;

  TOdTransferThread = class(TThread)
  private
    OdHttp: TOdHttp;
  protected
    procedure Execute; override;
  public
    constructor Create(AOdHttp: TOdHttp);
  end;

  TOdNetErrorEvent = procedure(Sender: TOdHttp; Error: TOdNetError) of object;
  TOdNetNotifyEvent = procedure(Sender: TOdHttp) of object;
  TOdNetCompleteEvent = procedure(Sender: TOdHttp) of object;
  TOdNetProgressEvent = procedure(Sender: TOdHttp; const TotalBytes,
    TransferedBytes, BytesPerSec, SecondsRemaining: Cardinal) of object;

  TOdHttpDetails = class(TObject)
  private
    FOdHttp: TOdHttp;
  protected
    procedure SetOdHttp(Value: TOdHttp); virtual;
  public
    Threaded: Boolean;
    IdleMainThread: Boolean;
    OnError: TOdNetErrorEvent;
    OnComplete: TOdNetNotifyEvent;
    OnProgress: TOdNetProgressEvent;
    OnStatusChange: TOdNetNotifyEvent;
    property OdHttp: TOdHttp read FOdHttp write SetOdHttp;
    procedure ShowProgress; virtual;
    procedure FreeAndNilOdHttp;
  end;

  TOdHttp = class(TObject)
  private
    FUseCache: Boolean;
    FUserName: string;
    FURL: string;
    FReferer: string;
    FPassword: string;
    FUserAgent: string;
    FFileName: string;
    FContentType: string;
    FHttpStatusCode: DWORD;
    FBufferSize: Cardinal;
    FOnProgress: TOdNetProgressEvent;
    FOnError: TOdNetErrorEvent;
    FOnComplete: TOdNetCompleteEvent;
    FOnStatusChange: TOdNetNotifyEvent;
    FNetworkHandle: HINTERNET;
    FURLHandle: HINTERNET;
    FRequestHandle: HINTERNET;
    FConnectHandle: HINTERNET;
    FSendStream: TStream;
    FRecvStream: TStream;
    FLastOSError: Integer;
    FLastOSErrorMessage: string;
    FTransferStatus: TOdTransferStatus;
    FTransferBuffer: Pointer;
    FAborted: Boolean;
    FTransferMode: TOdTransferMode;
    FTransferSize: Cardinal;
    FTransferedBytes: Cardinal;
    FThreaded: Boolean;
    FThreadRunning: Boolean;
    FIdleMainThread: Boolean;
    FDWORDLength: Cardinal;
    FReserved: Cardinal;
    FMinMSBetweenProgress: Cardinal;
    FReceiveTimeout: Cardinal;
    FConnectionTimeout: Cardinal;
    FConnectionRetries: Cardinal;
    FSuccessfulTransfer: Boolean;
    FTransferThread: TOdTransferThread;
    FErrorType: TOdNetError;
    FErrorContext: string;
    FErrorMessage: string;
    FTempStream: TStream;
    FBusy: Boolean;
    FDeflateCompression: Boolean;
    FStartMsec: Cardinal;
    function GetStatusMessage: string;
    procedure SetBufferSize(const Value: Cardinal);
    function GetBytesPerSecond: Cardinal;
    function GetElapsedSeconds: Cardinal;
    function GetSecondsRemaining: Cardinal;
    function GetElapsedMilliSeconds: Cardinal;
  protected
    function GetHttpStatusCode: Integer;
    function InitConnection: Boolean;
    procedure DoError(ErrorType: TOdNetError; const Context: string = '');
    procedure ErrorCallback;
    procedure DoComplete;
    procedure CompleteCallback;
    procedure DoProgress;
    procedure ProgressCallback;
    procedure DoStatusChange(Status: TOdTransferStatus);
    procedure StatusCallback;
    function CheckAborted: Boolean;
    function StartTransfer: Boolean;
    function DoTransfer: Boolean;
    function GetInternetHandle: Boolean;
    function ReadFileFromHandle(Handle: HINTERNET; Stream: TStream): Boolean;
    function DoHttpDownload: Boolean;
    function DoHttpPost: Boolean;
    procedure ThreadTerminated(Sender: TObject);
    function RegisterStatusCallback(Handle: HINTERNET): Boolean;
    procedure HandleStatusCallback(InternetStatus: DWORD; StatusInfo: Pointer; StatusInfoLength: DWORD);
    procedure ApplyConnectionTimeout;
    procedure ApplyReceiveTimeout;
    procedure ApplyConnectionRetries;
    procedure SetTempStream(Stream: TStream);
    procedure SetErrorMessage(const AMsg: string = '');

  public
    procedure ForceAbortTransfer;
    procedure SafeAbortTransfer;
    constructor Create;
    destructor Destroy; override;
    function GetFile(const AFileName: string = ''): Boolean;
    function GetStream(Stream: TStream): Boolean;
    function GetString(var Data: string): Boolean;
    function SendFile(RecvStream: TStream; const AFileName: string = ''): Boolean;
    function SendStream(SendStream, RecvStream: TStream): Boolean;
    function SendString(const Data: string; RecvStream: TStream): Boolean; overload;
    function SendString(const Data: string; var Response: string): Boolean; overload;
    procedure CloseConnection;
    procedure RaiseErrorException;

    property HttpStatusCode: Integer read GetHttpStatusCode;
    property BufferSize: Cardinal read FBufferSize write SetBufferSize;
    property UserAgent: string read FUserAgent write FUserAgent;
    property FileName: string read FFileName write FFileName;
    property Password: string read FPassword write FPassword;
    property Referer: string read FReferer write FReferer;
    property URL: string read FURL write FURL;
    property UseCache: Boolean read FUseCache write FUseCache;
    property UserName: string read FUserName write FUserName;
    property ContentType: string read FContentType write FContentType;
    property LastOSError: Integer read FLastOSError;
    property LastOSErrorMessage: string read FLastOSErrorMessage;
    property ErrorMessage: string read FErrorMessage;
    property TransferStatus: TOdTransferStatus read FTransferStatus;
    property StatusMessage: string read GetStatusMessage;
    property Threaded: Boolean read FThreaded write FThreaded;
    property ThreadRunning: Boolean read FThreadRunning;
    property IdleMainThread: Boolean read FIdleMainThread write FIdleMainThread;
    property MinMSBetweenProgress: Cardinal read FMinMSBetweenProgress write FMinMSBetweenProgress;
    property Aborted: Boolean read FAborted write FAborted;
    property ConnectionTimeout: Cardinal read FConnectionTimeout write FConnectionTimeout;
    property ConnectionRetries: Cardinal read FConnectionRetries write FConnectionRetries;
    property ReceiveTimeout: Cardinal read FReceiveTimeout write FReceiveTimeout;
    property SuccessfulTransfer: Boolean read FSuccessfulTransfer;
    property Busy: Boolean read FBusy;
    property DeflateCompression: Boolean read FDeflateCompression write FDeflateCompression;

    property TransferSize: Cardinal read FTransferSize; // Current transfer only
    property BytesPerSecond: Cardinal read GetBytesPerSecond;
    property SecondsRemaining: Cardinal read GetSecondsRemaining;
    property ElapsedSeconds: Cardinal read GetElapsedSeconds;
    property ElapsedMilliSeconds: Cardinal read GetElapsedMilliSeconds;
    property TransferedBytes: Cardinal read FTransferedBytes;

    property OnComplete: TOdNetCompleteEvent read FOnComplete write FOnComplete;
    property OnError: TOdNetErrorEvent read FOnError write FOnError;
    property OnProgress: TOdNetProgressEvent read FOnProgress write FOnProgress;
    property OnStatusChange: TOdNetNotifyEvent read FOnStatusChange write FOnStatusChange;
  end;

// Our "new" API
function OdHttpGetStream(const URL: string; OutStream: TStream; Options: TOdHttpDetails = nil): Boolean;
function OdHttpGetString(const URL: string; Options: TOdHttpDetails = nil): string;

procedure AllocateUrlComponents(var URL: TUrlComponents);
procedure DeAllocateUrlComponents(var URL: TUrlComponents);
function DefaultBlockingHttp: TODHttpDetails;
function SetDefaultHttpDetails(Options: TOdHttpDetails): TODHttpDetails;

procedure ResetOdHttpSentBytes;
function GetOdHttpSentBytes: Cardinal;

implementation

uses
  SysUtils, Math, TypInfo, ConvUtils, StdConvs, Forms, 
  OdExceptions, OdMiscUtils, OdNetUtils,
{$if CompilerVersion >= 22}
  System.Zlib;
{$else}
  lvkZLibUtils;
{$ifend}

type
  TWinInetStatus = (wisUnknown, wisPresent, wisNotPresent);

var
  WinInetAvailability: TWinInetStatus = wisUnknown;
  PrivateDefaultBlockingHttp: TOdHttpDetails = nil;
  DefaultHttpDetails: TOdHttpDetails = nil;
  PrivateSentBytes: Cardinal = 0; // Total since last call to ResetOdHttpSentBytes

const
  INTERNET_STATUS_DETECTING_PROXY = 80;
  WinInetDllName = 'wininet.dll';
  ForbiddenError = 'The server appears to be too busy to process your '+
    'request right now.  Please try again in a little while.';


procedure FreeObjects;
begin
  FreeAndNil(PrivateDefaultBlockingHttp);
end;

procedure CreateObjects;
begin
  PrivateDefaultBlockingHttp := TOdHttpDetails.Create;
  PrivateDefaultBlockingHttp.Threaded := False;
  DefaultHttpDetails := PrivateDefaultBlockingHttp;
end;

procedure WinInetCallback(Handle: HINTERNET; Context: DWORD;
  InternetStatus: DWORD; StatusInfo: Pointer; StatusInfoLength: DWORD); stdcall;
var
  HttpObject: TOdHttp;
begin
  if TObject(Context) is TOdHttp then
  begin
    HttpObject := TOdHttp(Context);
    HttpObject.HandleStatusCallback(InternetStatus, StatusInfo, StatusInfoLength);
  end
  else
    raise Exception.Create('Bad HTTP status callback context');
end;

procedure CloseInetHandle(var Handle: HINTERNET; const Name: string);
begin
  if Assigned(Handle) then
    RaiseOsExceptionIfFalse(InternetCloseHandle(Handle), 'Unable to close network handle: ' + Name);
  Handle := nil;
end;

function DefaultBlockingHttp: TODHttpDetails;
begin
  Result := PrivateDefaultBlockingHttp;
end;

function SetDefaultHttpDetails(Options: TOdHttpDetails): TODHttpDetails;
begin
  Result := DefaultHttpDetails;
  if Assigned(Options) then
    DefaultHttpDetails := Options
  else
    DefaultHttpDetails := DefaultBlockingHttp;
end;

function OdHttpGetStream(const URL: string; OutStream: TStream; Options: TOdHttpDetails): Boolean;
var
  HasOdHttp: Boolean;
  UseOptions: TOdHttpDetails;
begin
  UseOptions := Options;
  if not Assigned(UseOptions) then
    UseOptions := DefaultHttpDetails;
  Assert(Assigned(UseOptions));

  // If we start doing HTTP transfers async without a modal dialog, or
  // multiple transfers at once, we'll need to ensure every request uses a
  // separate details object here, and frees its own TOdHttp object
  HasOdHttp := Assigned(UseOptions.OdHttp);
  if HasOdHttp then
    Assert(not UseOptions.OdHttp.Busy, 'Attempt detected to perform two transfers at once');

  // Always create a new object to be safe in case we were in a bad state
  // This should change if we want to allow passing in a custom TOdHttp
  // object that supports overriding the details properties
  UseOptions.FreeAndNilOdHttp;
  UseOptions.OdHttp := TOdHttp.Create;
  UseOptions.OdHttp.Threaded := UseOptions.Threaded;
  UseOptions.OdHttp.IdleMainThread := UseOptions.IdleMainThread;
  UseOptions.OdHttp.URL := URL;
  UseOptions.OdHttp.ReceiveTimeout := 15 * 60 * 1000;  // Report timeout in minutes
  UseOptions.OdHttp.OnError := UseOptions.OnError;
  UseOptions.OdHttp.OnProgress := UseOptions.OnProgress;
  UseOptions.OdHttp.OnStatusChange := UseOptions.OnStatusChange;
  UseOptions.OdHttp.OnComplete := UseOptions.OnComplete;
  UseOptions.ShowProgress;
  Result := UseOptions.OdHttp.GetStream(OutStream);
end;

function OdHttpGetString(const URL: string; Options: TOdHttpDetails): string;
var
  StringStream: TStringStream;
  Details: TOdHttpDetails;
begin
  Details := Options;
  if not Assigned(Details) then
    Details := DefaultHttpDetails;
  Assert(not (Details.Threaded and (not Details.IdleMainThread)), 'OdHttpGetString can not support threaded, non-idling mode');
  StringStream := TStringStream.Create('');
  try
    OdHttpGetStream(URL, StringStream, Options);
    Result := StringStream.DataString;
  finally
    FreeAndNil(StringStream);
  end;
end;

procedure AllocateUrlComponents(var URL: TUrlComponents);
begin
  with URL do begin
    FillChar(URL, SizeOf(URL), 0);
    GetMem(lpszScheme, INTERNET_MAX_SCHEME_LENGTH + 1);
    GetMem(lpszHostName, INTERNET_MAX_HOST_NAME_LENGTH + 1);
    GetMem(lpszUserName, INTERNET_MAX_USER_NAME_LENGTH + 1);
    GetMem(lpszPassword, INTERNET_MAX_PASSWORD_LENGTH + 1);
    GetMem(lpszUrlPath, INTERNET_MAX_PATH_LENGTH + 1);
    GetMem(lpszExtraInfo, INTERNET_MAX_PATH_LENGTH + 1);
    dwStructSize := SizeOf(TUrlComponents);
    dwSchemeLength := INTERNET_MAX_SCHEME_LENGTH + 1;
    dwHostNameLength := INTERNET_MAX_HOST_NAME_LENGTH + 1;
    dwUserNameLength := INTERNET_MAX_USER_NAME_LENGTH + 1;
    dwPassWordLength := INTERNET_MAX_PASSWORD_LENGTH + 1;
    dwUrlPathLength := INTERNET_MAX_PATH_LENGTH + 1;
    dwExtraInfoLength := INTERNET_MAX_PATH_LENGTH + 1;
  end;
end;

procedure DeAllocateUrlComponents(var URL: TUrlComponents);
begin
  with URL do begin
    FreeMem(lpszScheme);
    FreeMem(lpszHostName);
    FreeMem(lpszUserName);
    FreeMem(lpszPassword);
    FreeMem(lpszUrlPath);
    FreeMem(lpszExtraInfo);
  end;
end;

procedure ResetOdHttpSentBytes;
begin
  PrivateSentBytes := 0;
end;

function GetOdHttpSentBytes: Cardinal;
begin
  Result := PrivateSentBytes;
end;

{ TOdHttp }

procedure TOdHttp.SafeAbortTransfer;
begin
  Aborted := True;
end;

procedure TOdHttp.ForceAbortTransfer;
begin
  Aborted := True;
  CloseConnection;
end;

procedure TOdHttp.CloseConnection;
begin
  if TransferStatus = nsNotConnected then
    Exit;
  DoStatusChange(nsClosing);
  CloseInetHandle(FRequestHandle, 'RequestHandle');
  CloseInetHandle(FConnectHandle, 'ConnectHandle');
  CloseInetHandle(FURLHandle, 'URLHandle');
  CloseInetHandle(FNetworkHandle, 'NetworkHandle');
  FSendStream := nil;
  FRecvStream := nil;
  FTransferMode := tmUnknown;
  SetTempStream(nil);
  DoStatusChange(nsNotConnected);
end;

constructor TOdHttp.Create;
begin
  inherited;
  if WinInetAvailability = wisUnknown then begin
    if GetModuleHandle(WinInetDllName) <> 0 then
      WinInetAvailability := wisPresent
    else
      WinInetAvailability := wisNotPresent;
  end;
  if WinInetAvailability = wisNotPresent then
    raise Exception.Create(WinInetDllName + ' is not available');
  UserAgent := 'TOdHttp 1.0';
  BufferSize := 2048; // 2K
  ContentType := 'text/xml';
  FDWORDLength := SizeOf(DWORD);
  FReserved := 0;
  Threaded := True;
  DeflateCompression := True;
  FStartMsec := GetTickCount;
  MinMSBetweenProgress := 800;
  ConnectionTimeout := 60000;
  ConnectionRetries := 5;
  ReceiveTimeout := 300000;
end;

destructor TOdHttp.Destroy;
begin
  FreeMem(FTransferBuffer, FBufferSize);
  CloseConnection;
  SetTempStream(nil);
  inherited;
end;

procedure TOdHttp.DoComplete;
begin
  if Assigned(FOnComplete) then
  begin
    if Threaded then
    begin
      Assert(Assigned(FTransferThread));
      FTransferThread.Synchronize(CompleteCallback);
    end
    else
      CompleteCallback;
  end
end;

procedure TOdHttp.DoProgress;
begin
  if @FOnProgress = nil then
    Exit;
  if Threaded then
  begin
    Assert(Assigned(FTransferThread));
    FTransferThread.Synchronize(ProgressCallback);
  end
  else
    ProgressCallback;
end;

procedure TOdHttp.DoStatusChange(Status: TOdTransferStatus);
begin
  FTransferStatus := Status;
  if Assigned(FOnStatusChange) then
  begin
    if Threaded then
    begin
      Assert(Assigned(FTransferThread));
      FTransferThread.Synchronize(StatusCallback);
    end
    else
      StatusCallback;
  end
end;

function TOdHttp.GetHttpStatusCode: Integer;
begin
  Result := FHttpStatusCode;
end;

function TOdHttp.GetStatusMessage: string;
begin
  case TransferStatus of
    nsNotConnected: Result := 'Not connected';
    nsAborting: Result := 'Aborting';
    nsClosing: Result := 'Closing connection';
    nsInternetOpen: Result := 'Verifying Internet connection';
    nsInternetConnect: Result := 'Connecting to server';
    nsOpenRequest: Result := 'Starting HTTP request to server';
    nsSendRequest: Result := 'Sending HTTP request to server';
    nsOpenURL: Result := 'Opening URL connection';
    nsQueryInfo: Result := 'Reading HTTP header information';
    nsReadFile: Result := 'Downloading data';
    nsClosed: Result := 'Connection closed';
    nsConnecting: Result := 'Connecting to server';
    nsConnected: Result := 'Connected to server';
    nsResolving: Result := 'Resolving hostname';
    nsResolved: Result := 'Resolved hostname';
    nsDecompressing: Result := 'Decompressing data';
  else
    Result :=GetEnumName(TypeInfo(TOdTransferStatus), Integer(FTransferStatus));
  end;
end;

function TOdHttp.InitConnection: Boolean;
begin
  Result := False;
  FHttpStatusCode := 0;
  Aborted := False;
  FTransferSize := 0;
  FTransferedBytes := 0;
  FSuccessfulTransfer := False;
  FStartMsec := GetTickCount;
  if Trim(URL) = '' then
  begin
    DoError(neInvalidURL);
    Exit;
  end;
  Result := True;
end;

function TOdHttp.GetFile(const AFileName: string): Boolean;
begin
  if (AFileName = '') and (FileName = '') then
    raise Exception.Create('No filename specified for Get');
  Result := False;
  Assert(False, 'Unimplemented function: TOdHttp.GetFile');
end;

function TOdHttp.GetStream(Stream: TStream): Boolean;
begin
  Assert(Assigned(Stream));
  FRecvStream := Stream;
  FSendStream := nil;
  FTransferMode := tmHttp;
  Result := StartTransfer;
end;

function TOdHttp.GetString(var Data: string): Boolean;
var
  StringStream: TStringStream;
begin
  Assert(not (Threaded and (not IdleMainThread)), 'TOdHttp.GetString can not support threaded non-idling mode');
  StringStream := TStringStream.Create('');
  try
    Result := GetStream(StringStream);
    Data := StringStream.DataString;
  finally
    FreeAndNil(StringStream);
  end;
end;

function TOdHttp.SendFile(RecvStream: TStream; const AFileName: string): Boolean;
//var
//  FileStream: TFileStream;
begin
  Result := False;
  if ((AFileName = '') and (FileName = ''))then
    raise Exception.Create('No file name specified for send')
  else if AFileName <> '' then
    FileName := AFileName;
  Assert(False, 'Unimplemented function: TOdHttp.SendFile')

(*
  if not FileExists(FileName) then
    raise Exception.Create('The specified file name to send does not exist:'#13#10 + FileName);

  FileStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  try
    Result := SendStream(FileStream, RecvStream);
  finally
    FileStream.Free;
  end;
*)
end;

function TOdHttp.SendStream(SendStream, RecvStream: TStream): Boolean;
begin
  Assert(Assigned(SendStream));
  Assert(Assigned(RecvStream));
  FSendStream := SendStream;
  FRecvStream := RecvStream;
  FTransferMode := tmHttpPost;
  Result := StartTransfer;
end;

function TOdHttp.SendString(const Data: string; RecvStream: TStream): Boolean;
var
  StringStream: TStringStream;
begin
  StringStream := TStringStream.Create(Data);
  StringStream.Position := 0;
  SetTempStream(StringStream);
  Result := SendStream(StringStream, RecvStream);
end;

procedure TOdHttp.SetBufferSize(const Value: Cardinal);
var
  SaneSize: Cardinal;
begin
  // Force the buffer to be between 128 Bytes and 128 KB
  SaneSize := Min(Max(128, Value), 1024 * 128);
  ReallocMem(FTransferBuffer, SaneSize);
  FBufferSize := SaneSize;
end;

procedure TOdHttp.DoError(ErrorType: TOdNetError; const Context: string);
begin
  FLastOSError := GetLastError;
  FErrorType := ErrorType;
  // Sometimes when aborting, the API call returns an error, but the
  // real cause of the "error" was a user aborting the request
  if Aborted then
    FErrorType := neAbort;
  FErrorContext := Context;
  if ErrorType = neException then
    SetErrorMessage(Context)
  else
    SetErrorMessage;

  if Assigned(FOnError) then
  begin
    if Threaded then
    begin
      Assert(Assigned(FTransferThread));
      FTransferThread.Synchronize(ErrorCallback);
    end
    else
      ErrorCallback;
  end
  else
  begin
    case ErrorType of
      neAbort:
        Abort;
      else
        raise EODNetworkException.Create(ErrorMessage);
    end;
  end;
end;

function TOdHttp.CheckAborted: Boolean;
begin
  Result := False;
  if Aborted then
  begin
    Result := True;
    DoError(neAbort);
    DoStatusChange(nsAborting);
    CloseConnection;
  end;
end;

function TOdHttp.DoTransfer: Boolean;
begin
  Assert(FTransferMode <> tmUnknown);
  FBusy := True;

  try
    Result := False;

    if not InitConnection then
      Exit;
    if CheckAborted then Exit;

    if not GetInternetHandle then
      Exit;
    if CheckAborted then Exit;

    case FTransferMode of
      tmHttp:     Result := DoHttpDownload;
      tmHttpPost: Result := DoHttpPost;
    end;
    CloseConnection;
  finally
    FBusy := False;
  end;
end;

function TOdHttp.GetInternetHandle: Boolean;
begin
  Result := False;
  DoStatusChange(nsInternetOpen);
  FNetworkHandle := InternetOpen(PChar(FUserAgent), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if FNetworkHandle = nil then
  begin
    DoError(neInternetOpen, FUserAgent);
    Exit;
  end;

  ApplyConnectionTimeout;
  ApplyConnectionRetries;
  ApplyReceiveTimeout;
  Result := True;
end;

function TOdHttp.DoHttpDownload: Boolean;
var
  APIResult: Boolean;
  Headers: string;
begin
  Result := False;

  if not RegisterStatusCallback(FNetworkHandle) then
    Exit;
  if CheckAborted then Exit;

  DoStatusChange(nsOpenURL);
  if DeflateCompression then
    Headers := 'Accept-Encoding: deflate'
  else
    Headers := '';

  //TODO: Add https/SSL support here too someday?
  FURLHandle := InternetOpenUrl(FNetworkHandle, PChar(URL), PChar(Headers), Length(Headers), INTERNET_FLAG_RELOAD, Cardinal(Self));
  if FURLHandle = nil then
  begin
    DoError(neOpenURL, URL);
    Exit;
  end;
  if CheckAborted then Exit;

  DoStatusChange(nsQueryInfo);
  APIResult := HttpQueryInfo(FURLHandle, HTTP_QUERY_STATUS_CODE or HTTP_QUERY_FLAG_NUMBER,
    @FHttpStatusCode, FDWORDLength, FReserved);
  if not APIResult then
  begin
    DoError(neQueryInfo);
    Exit;
  end;

  if FHttpStatusCode <> HTTP_STATUS_OK then
  begin
    if FHttpStatusCode = 403 then
      DoError(neForbidden, IntToStr(FHttpStatusCode))
    else
      DoError(neStatusCode, IntToStr(FHttpStatusCode));
    Exit;
  end;
  if CheckAborted then Exit;

  if not ReadFileFromHandle(FURLHandle, FRecvStream) then
    Exit;

  Result := True;
end;

function TOdHttp.DoHttpPost: Boolean;
var
  Prot, User, Pass, Host, Path: string;

  Port, Scheme: Integer;
  Headers: string;
  APIResult: Boolean;
  PostData: array of Byte;
  RequestFlags: Cardinal;
  IsSSL: Boolean;
begin
  Result := False;
  if CheckAborted then Exit;
  try
    ParseURL(URL, Prot, User, Pass, Host, Path, Port, Scheme);
  except
    DoError(neInvalidURL, URL);
    Exit;
  end;

  IsSSL := SameText(Prot, 'https');

  if IsSSL then begin
    // Use this to enable use of invalid SSL certificates
    //RequestFlags := INTERNET_FLAG_IGNORE_CERT_CN_INVALID or INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;
    RequestFlags := 0;
  end
  else
    RequestFlags := 0;

  DoStatusChange(nsInternetConnect);
  FConnectHandle := InternetConnect(FNetworkHandle, PChar(Host), Port,
          PChar(User), PChar(Pass), INTERNET_SERVICE_HTTP, RequestFlags, Cardinal(Self));
  if not Assigned(FConnectHandle) then
  begin
    DoError(neInternetConnect, Host);
    Exit;
  end;

  if not RegisterStatusCallback(FConnectHandle) then
    Exit;
  if CheckAborted then Exit;

  DoStatusChange(nsOpenRequest);
  if CheckAborted then Exit;

  if IsSSL then
    RequestFlags := INTERNET_FLAG_SECURE {or INTERNET_FLAG_IGNORE_CERT_CN_INVALID or INTERNET_FLAG_IGNORE_CERT_DATE_INVALID}
  else
    RequestFlags := 0;
  FRequestHandle := HttpOpenRequest(FConnectHandle, 'POST',
          PChar(Path), nil, nil, nil, RequestFlags, Cardinal(Self));
  if not Assigned(FConnectHandle) then begin
    DoError(neOpenRequest, Path);
    Exit;
  end;

  DoStatusChange(nsSendRequest);
  if CheckAborted then Exit;
  SetLength(PostData, FSendStream.Size);
  FSendStream.ReadBuffer(PostData[0], FSendStream.Size);
  Headers := 'Content-type: ' + ContentType;

  if DeflateCompression then
    Headers := Headers + sLineBreak + 'Accept-Encoding: deflate';

  APIResult := HttpSendRequest(FRequestHandle, PChar(Headers), Length(Headers),
          @PostData[0], Length(PostData));
  if not APIResult then begin
    DoError(neSendRequest, URL);
    Exit;
  end;

  if not ReadFileFromHandle(FRequestHandle, FRecvStream) then
    Exit;
  Result := True;
end;

function TOdHttp.ReadFileFromHandle(Handle: HINTERNET; Stream: TStream): Boolean;
var
  BytesRead: Cardinal;
  APIResult: Boolean;
  LastProgress: Cardinal;
  Encoding: array [0..1024] of Char;
  Compressed: Boolean;
  EncodingSize: DWORD;
  WriteStream: TStream;
{$if CompilerVersion >= 22}
  DecompressedStream: TZDecompressionStream;
{$ifend}
begin
  Result := False;
  LastProgress := 0;

  APIResult := HttpQueryInfo(FURLHandle, HTTP_QUERY_CONTENT_LENGTH or HTTP_QUERY_FLAG_NUMBER,
    @FTransferSize, FDWORDLength, FReserved);
  if not APIResult then
    FTransferSize := 0;

  EncodingSize := SizeOf(Encoding) - 1;
  APIResult := HttpQueryInfo(FURLHandle, HTTP_QUERY_CONTENT_ENCODING,
    @Encoding[0], EncodingSize, FReserved);
  Compressed := False;
  if APIResult then
  begin
    if StrComp(Encoding, 'deflate') = 0 then
      Compressed := True
    else if StrComp(Encoding, '') <> 0 then
    begin
      DoError(neQueryInfo, 'Bad encoding: ' + Encoding);
      Exit;
    end;
  end;
  if Compressed then
    WriteStream := TMemoryStream.Create
  else
    WriteStream := Stream;

  try
    DoStatusChange(nsReadFile);
    if CheckAborted then Exit;

    WriteStream.Seek(0, soFromBeginning);
    DoProgress;

    repeat
      APIResult := InternetReadFile(Handle, FTransferBuffer, BufferSize, BytesRead);
      if not APIResult then
      begin
        DoError(neReadFile);
        Exit;
      end;
      WriteStream.WriteBuffer(FTransferBuffer^, BytesRead);
      Inc(FTransferedBytes, BytesRead);
      if ((GetTickCount - MinMSBetweenProgress) > LastProgress) or (BytesRead = 0) then
      begin
        LastProgress := GetTickCount;
        DoProgress;
      end;

      if CheckAborted then Exit;
    until BytesRead = 0;

    if Compressed then
    begin
      WriteStream.Position := 0;
      Stream.Position := 0;
      try
        DoStatusChange(nsDecompressing);
{$if CompilerVersion >= 22}
        DecompressedStream := TZDecompressionStream.Create(WriteStream);
        try
          Stream.CopyFrom(DecompressedStream, 0);
        finally
          FreeAndNil(DecompressedStream);
        end;
{$else}        
        zLibDecompress(WriteStream, Stream, DEFAULT_BUFFER_SIZE, False);
{$ifend}
      except on E: Exception do
        begin
          DoError(neInflate, E.Message);
          Exit;
        end;
      end;
    end;
  finally
    if Compressed then
      FreeAndNil(WriteStream);
  end;

  Result := True;
end;

function TOdHttp.StartTransfer: Boolean;
begin
  if Threaded then
  begin
    FTransferThread := TOdTransferThread.Create(Self);
    FTransferThread.OnTerminate := ThreadTerminated;
    FThreadRunning := True;
    if IdleMainThread then
    begin
      while FThreadRunning do
      begin
        Application.ProcessMessages;
        Windows.Sleep(1); // Yield to other processes
      end;
      Result := SuccessfulTransfer;
      Assert(not Busy);
    end
    else
      Result := True;
  end
  else // Non-threaded mode
  begin
    FSuccessfulTransfer := DoTransfer;
    Result := SuccessfulTransfer;
    DoComplete;
    Assert(not Busy);
  end;
  if not Result then
    RaiseErrorException;
end;

procedure TOdHttp.ThreadTerminated(Sender: TObject);
begin
  FThreadRunning := False;
  DoComplete;
end;

function TOdHttp.RegisterStatusCallback(Handle: HINTERNET): Boolean;
var
  Callback: INTERNET_STATUS_CALLBACK;
begin
  Result := False;
  Callback := InternetSetStatusCallback(Handle, @WinInetCallback);
  if Integer(Callback) = INTERNET_INVALID_STATUS_CALLBACK then
  begin
    DoError(neCallback);
    Exit;
  end;
  Result := True;
end;

procedure TOdHttp.HandleStatusCallback(InternetStatus: DWORD;
  StatusInfo: Pointer; StatusInfoLength: DWORD);
var
  SentBytes: DWORD;
begin
  case InternetStatus of
    INTERNET_STATUS_RESOLVING_NAME:      DoStatusChange(nsResolving);
    INTERNET_STATUS_NAME_RESOLVED:       DoStatusChange(nsResolved);
    INTERNET_STATUS_CONNECTING_TO_SERVER:DoStatusChange(nsConnecting);
    INTERNET_STATUS_CONNECTED_TO_SERVER: DoStatusChange(nsConnected);
    INTERNET_STATUS_CLOSING_CONNECTION:  DoStatusChange(nsClosing);
    INTERNET_STATUS_CONNECTION_CLOSED:   DoStatusChange(nsClosed);
    INTERNET_STATUS_HANDLE_CREATED:      begin end; // Ignore these for now
    INTERNET_STATUS_DETECTING_PROXY:     begin end;
    INTERNET_STATUS_SENDING_REQUEST:     begin end;
    INTERNET_STATUS_RECEIVING_RESPONSE:  begin end;
    INTERNET_STATUS_RESPONSE_RECEIVED:   begin end;
    INTERNET_STATUS_HANDLE_CLOSING:      begin end;
    INTERNET_STATUS_REQUEST_SENT:
      begin // This accounts for the HTTP header size as well as the POST payload
        SentBytes := DWORD(StatusInfo^);
        Inc(PrivateSentBytes, SentBytes);
      end;
  end;
end;

procedure TOdHttp.ApplyConnectionTimeout;
begin
  if Assigned(FNetworkHandle) then
  begin
    if not InternetSetOption(FNetworkHandle, INTERNET_OPTION_CONNECT_TIMEOUT,
        @FConnectionTimeout, FDWORDLength) then
      DoError(neSetOption, 'INTERNET_OPTION_CONNECT_TIMEOUT'); // Non-fatal?
  end;
end;

procedure TOdHttp.ApplyConnectionRetries;
begin
  if Assigned(FNetworkHandle) then
  begin
    if not InternetSetOption(FNetworkHandle, INTERNET_OPTION_CONNECT_RETRIES,
        @FConnectionRetries, FDWORDLength) then
      DoError(neSetOption, 'INTERNET_OPTION_CONNECT_RETRIES'); // Non-fatal?
  end;
end;

procedure TOdHttp.ApplyReceiveTimeout;
begin
  if Assigned(FNetworkHandle) then
  begin
    if not InternetSetOption(FNetworkHandle, INTERNET_OPTION_RECEIVE_TIMEOUT,
        @FReceiveTimeout, FDWORDLength) then
      DoError(neSetOption, 'INTERNET_OPTION_RECEIVE_TIMEOUT'); // Non-fatal?
  end;
end;

procedure TOdHttp.ProgressCallback;
begin
  if @FOnProgress <> nil then
    OnProgress(Self, TransferSize, TransferedBytes, BytesPerSecond, SecondsRemaining);
end;

procedure TOdHttp.CompleteCallback;
begin
  if @FOnComplete <> nil then
    OnComplete(Self);
end;

procedure TOdHttp.ErrorCallback;
begin
  if @FOnError <> nil then
    OnError(Self, FErrorType);
end;

procedure TOdHttp.StatusCallback;
begin
  if @FOnStatusChange <> nil then
    OnStatusChange(Self);
end;

procedure TOdHttp.SetTempStream(Stream: TStream);
begin
  FreeAndNil(FTempStream);
  FTempStream := Stream;
end;

procedure TOdHttp.SetErrorMessage(const AMsg: string);
var
  Msg: string;
  Buffer: array[0..4096] of char;
  BufLen: Cardinal;
  NewError: Cardinal;
begin
  if AMsg <> '' then
  begin
    FErrorMessage := AMsg;
    Exit;
  end;

  case FErrorType of
    neAbort:
      Msg := 'Transfer aborted by request';
    neInternetOpen:
      Msg := 'Unable find a configured Internet connection on this machine';
    neInternetConnect:
      Msg := 'Unable contact server: '+sLineBreak+URL+sLineBreak+
        'Please check your Internet connection.';
    neOpenRequest:
      Msg := Format('Unable open request to connect to %s.'+sLineBreak+
        'Please check your Internet connection.', [GetHostFromURL(URL)]);
    neSendRequest:
      Msg := Format('Unable send HTTP POST request to %s.'+sLineBreak+
        'Please check your Internet connection.', [GetHostFromURL(URL)]);
    neOpenURL:
      Msg := Format('Unable connect to / get data from %s.'+sLineBreak+
        'Please check your Internet connection.', [GetHostFromURL(URL)]);
    neQueryInfo:
      Msg := Format('Unable to obtain HTTP status code for %s.'+sLineBreak+
        'Please contact your system administrator.', [URL]);
    neStatusCode:
      Msg := Format('Bad HTTP status code %d for URL %s.' +sLineBreak+
        'Please contact your system administrator.', [HttpStatusCode, URL]);
    neForbidden:
      Msg := ForbiddenError;
    neReadFile:
      Msg := Format('Error reading data from URL:' +sLineBreak+ '%s' +sLineBreak+
        'Please try again or contact your system administrator.', [URL]);
    neInvalidURL:
      Msg := Format('The specified URL is not valid:' +sLineBreak+ '%s' +sLineBreak+
        'Please contact your system administrator.', [URL]);
    neCallback:
      Msg := 'The WinInet status callback could not be registered.';
    neSetOption:
      Msg := 'InternetSetOption failed setting ' + FErrorContext;
    neInflate:
      Msg := 'Inflate decompression failed: ' + FErrorContext;
    neURLTooLong:
      Msg := 'URL string too long for WinInet';
    else
      Msg := ('Unknown network error');
  end;

  if FLastOSError <> 0 then
  begin
    BufLen := Length(Buffer);
    if FLastOSError = ERROR_INTERNET_EXTENDED_ERROR then begin
      if InternetGetLastResponseInfo(NewError, Buffer, BufLen) then
        if StrLen(Buffer) > 0 then
        begin
          FLastOSError := NewError;
          FLastOSErrorMessage := TrimRight(Format(SLinebreak+'Windows Error %d:'+sLineBreak+'%s', [FLastOsError, Buffer]));
          Msg := Msg + LastOSErrorMessage;
        end;
    end
    else begin
      FormatMessage(FORMAT_MESSAGE_FROM_HMODULE, Pointer(GetModuleHandle(WinInetDllName)),
        FLastOSError, 0, Buffer, BufLen, nil);
      if StrLen(Buffer) > 0 then
      begin
        FLastOSErrorMessage := TrimRight(Format(SLinebreak+'Windows Error %d:'+sLineBreak+'%s', [FLastOsError, Buffer]));
        Msg := Msg + LastOSErrorMessage;
      end;
    end;
  end;

  FErrorMessage := Trim(Msg);
end;

procedure TOdHttp.RaiseErrorException;
begin
  if FErrorType = neAbort then
    raise EAbort.Create(ErrorMessage)
  else if FErrorType = neForbidden then
    raise EOdNetworkException.CreateLogged(ForbiddenError, False)
  else
    raise EOdNetworkException.Create(ErrorMessage);
end;

function TOdHttp.GetBytesPerSecond: Cardinal;
begin
  if ElapsedMilliSeconds <= 0 then
    Result := 0
  else
    Result := Round(FTransferedBytes / (ElapsedMilliSeconds / 1000));
end;

function TOdHttp.GetElapsedSeconds: Cardinal;
begin
  Result := Round(ElapsedMilliSeconds/1000);
end;

function TOdHttp.GetSecondsRemaining: Cardinal;
begin
  Result := 0;
  if (FTransferSize > 0) and (BytesPerSecond > 0) then
    if TransferedBytes < TransferSize then
      Result := Round((TransferSize - TransferedBytes)/BytesPerSecond);
end;

function TOdHttp.GetElapsedMilliSeconds: Cardinal;
begin
  Result := GetTickCount - FStartMsec;
end;

function TOdHttp.SendString(const Data: string; var Response: string): Boolean;
var
  StringStream: TStringStream;
begin
  Assert(not (Threaded and (not IdleMainThread)), 'TOdHttp.SendString does not support threaded non-idling mode');
  StringStream := TStringStream.Create('');
  try
    Result := SendString(Data, StringStream);
    if not SuccessfulTransfer then
      RaiseErrorException;
    Response := StringStream.DataString;
  finally
    FreeAndNil(StringStream);
  end;
end;

{ TOdTransferThread }

constructor TOdTransferThread.Create(AOdHttp: TOdHttp);
begin
  inherited Create(False);
  OdHttp := AOdHttp;
  OdHttp.FTransferThread := Self;
  FreeOnTerminate := True;
end;

procedure TOdTransferThread.Execute;
begin
  try
    OdHttp.FSuccessfulTransfer := OdHttp.DoTransfer;
  except
    on E: Exception do
    begin
      OdHttp.DoError(neException, E.Message);
      OdHttp.FSuccessfulTransfer := False;
    end;
  end;
end;

{ TOdHttpDetails }

procedure TOdHttpDetails.FreeAndNilOdHttp;
begin
  FreeAndNil(FODHttp);
end;

procedure TOdHttpDetails.SetOdHttp(Value: TOdHttp);
begin
  FOdHttp := Value;
end;

procedure TOdHttpDetails.ShowProgress;
begin
  // Show the progress dialog/controls here in descendents
end;

initialization
  CreateObjects;

finalization
  FreeObjects;

end.

