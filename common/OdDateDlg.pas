unit OdDateDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TDateDialog = class(TForm)
    DateCal: TMonthCalendar;
    DateLabel: TLabel;
    OKButton: TButton;
    CancelButton: TButton;
    procedure DateCalClick(Sender: TObject);
    procedure DateCalDblClick(Sender: TObject);
  protected
    procedure UpdateDisplay;
  public
    constructor Create(AOwner: TComponent); override;
    class function Execute(var Date: TDateTime; DisplayDate: TDateTime = 0; Caption: string = ''): Boolean;
  end;

implementation

uses
  DateUtils, OdVclUtils;

{$R *.dfm}

constructor TDateDialog.Create(AOwner: TComponent);
begin
  inherited;
  DateCal.Date := Now;
end;

class function TDateDialog.Execute(var Date: TDateTime; DisplayDate: TDateTime; Caption: string): Boolean;
var
  Dialog: TDateDialog;
  InitialDate: TDateTime;
begin
  Dialog := TDateDialog.Create(nil);
  try
    InitialDate := Today;
    if DisplayDate > 0 then
      InitialDate := Trunc(DisplayDate);
    if Date > 0 then
      InitialDate := Trunc(Date);
    Dialog.DateCal.Date := InitialDate;
    if Caption <> '' then
      Dialog.Caption := Caption;
    Dialog.UpdateDisplay;
    Result := Dialog.ShowModal = mrOK;
    if Result then
      Date := Trunc(Dialog.DateCal.Date);
  finally
    FreeAndNil(Dialog);
  end;
end;

procedure TDateDialog.DateCalClick(Sender: TObject);
begin
  UpdateDisplay;
end;

procedure TDateDialog.DateCalDblClick(Sender: TObject);
begin
  if IsMonthCalendarCursorInTitle(DateCal) then
    Exit;
  OKButton.Click;
end;

procedure TDateDialog.UpdateDisplay;
begin
  DateLabel.Caption := 'Selected Date: ' + FormatDateTime(ShortDateFormat, DateCal.Date);
end;

end.
