unit OdGetNewPassword;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ActnList;

type
  TGetNewPasswordForm = class(TForm)
    NewPasswordEdit: TEdit;
    ConfirmLabel: TLabel;
    ConfirmPasswordEdit: TEdit;
    OkButton: TButton;
    CancelButton: TButton;
    CurrentLabel: TLabel;
    CurrentPasswordEdit: TEdit;
    InstructionsLabel: TLabel;
    ActionList: TActionList;
    OKAction: TAction;
    CancelAction: TAction;
    Label1: TLabel;
    ProblemLabel: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKActionExecute(Sender: TObject);
    procedure CancelActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
  protected
    FUserName: string;
    FFirstName: string;
    FLastName: string;
    FOldPassword: string;
    FForceChange: Boolean;
    procedure Loaded; override;
  public
    property UserName: string read FUserName write FUserName;
    property FirstName: string read FFirstName write FFirstName;
    property LastName: string read FLastName write FLastName;
    property OldPassword: string read FOldPassword write FOldPassword;
  end;

function GetNewPassword(ForceChange: Boolean; const UserName, FirstName, LastName, OldPassword:
  string; var NewPassword: string; var NewHash, NewAPIHash: string): Boolean;

implementation

uses
  PasswordRules, Terminology;

{$R *.dfm}

function GetNewPassword(ForceChange: Boolean; const UserName, FirstName, LastName, OldPassword:
  string; var NewPassword: string; var NewHash, NewAPIHash: string): Boolean;
var
  Dlg: TGetNewPasswordForm;
begin
  Dlg := TGetNewPasswordForm.Create(nil);
  try
    Dlg.FOldPassword := OldPassword;
    Dlg.FUserName := UserName;
    Dlg.FForceChange := ForceChange;
    Dlg.FFirstName := FirstName;
    Dlg.FLastName := LastName;
    if ForceChange then
      Dlg.Caption := 'Required Password Change';

    Result := (Dlg.ShowModal = mrOK);
    if Result then begin
      NewHash := GeneratePasswordHash(UserName, Dlg.NewPasswordEdit.Text);
      NewPassword := Dlg.NewPasswordEdit.Text;
      NewAPIHash := GeneratePasswordHash(UserName, NewHash);
    end else begin
      NewHash := '';
      NewPassword := '';
      NewAPIHash := '';
    end;
  finally
    FreeAndNil(Dlg);
  end;
end;

procedure TGetNewPasswordForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if FForceChange and (ModalResult <> mrOK) then
    CanClose := False;
end;

procedure TGetNewPasswordForm.OKActionExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TGetNewPasswordForm.CancelActionExecute(Sender: TObject);
begin
  if FForceChange then
    Application.Terminate
  else
    ModalResult := mrCancel;
end;

procedure TGetNewPasswordForm.ActionListUpdate(Action: TBasicAction; var
  Handled: Boolean);
var
  NewPassword: string;
  Problem: string;
begin
  if GeneratePasswordHash(UserName, CurrentPasswordEdit.Text) <> OldPassword then begin
    Problem := 'Old password is not correct';
  end else begin
    NewPassword := NewPasswordEdit.Text;
    PasswordMeetsRequirements(UserName, OldPassword, NewPassword, Problem, FirstName, LastName);

    if Problem = '' then
      if ConfirmPasswordEdit.Text <> NewPasswordEdit.Text then
        Problem := 'The new password must be entered twice exactly the same';
  end;

  OKAction.Enabled := Problem = '';
  ProblemLabel.Caption := Problem;

  OkButton.Default := OkButton.Enabled;
  CancelButton.Default := not OkButton.Default;
  if FForceChange then
    CancelAction.Caption := 'Exit'
  else
    CancelAction.Caption := 'Cancel';
  //CancelAction.Enabled := not FForceChange;
end;

procedure TGetNewPasswordForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '') then
    AppTerminology.ReplaceVCL(Self);
end;

end.

