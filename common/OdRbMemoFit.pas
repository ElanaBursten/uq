unit OdRbMemoFit;

{
To Use This, in the Memo component's Print event, do this:

ScaleRBMemoToFit(ppMemo1, 18);
}

interface

uses
  Windows, SysUtils, Classes, ppMemo;

procedure ScaleRBMemoToFit(M: TppMemo; DefaultPointSize: Integer);
function TryToFit(M: TppMemo; PointSize: Single): Boolean;
function RemoveExcessWhitespace(Data: string): string;

implementation

uses
  ppPlainText, ppClass, ppPrintr, ppTypes, Graphics, JclStrings;

function MemoCharactersFit(M: TppMemo): Integer;
var
  lPrinter: TppPrinter;
  CharPos: Integer;
  PrintLines: TStringList;
  Rect: TppRect;
begin
  CharPos := 0;

  if (M.Band <> nil) then
    lPrinter := M.Band.GetPrinter
  else
    lPrinter := ppPrinter;

  // This told me the underlying driver is at 600, in spite of
  // the Font.PixelsPerInch saying 96.
  // GetDeviceCaps(lPrinter.Canvas.Handle, LOGPIXELSY);

  // the position does not matter, we just need the size right.
  Rect.Left   := 0;
  Rect.Top    := 0;
  Rect.Right  := M.mmWidth;
  Rect.Bottom := M.mmHeight;

  PrintLines := TStringList.Create;
  try
    TppPlainText.WordWrap(M.Text, Rect, M.TextAlignment, False, M.Font, M.CharWrap, M.mmLeading, M.TabStopPositions, lPrinter, CharPos, PrintLines);
  finally
    FreeAndNil(PrintLines);
  end;
  Result := CharPos;
end;

function TryToFit(M: TppMemo; PointSize: Single): Boolean;
var
  FitsLength: Integer;
  NeededTextLength: Integer;
  PixPerInchY: Integer;
  Height: Integer;
begin
  M.Font.PixelsPerInch := 600;    // attempt fine control over size
  PixPerInchY := M.Font.PixelsPerInch;
  Height := Round(PointSize * PixPerInchY / 72);
  M.Font.Height := - Height;

  FitsLength := MemoCharactersFit(M);
  NeededTextLength := Length(TrimRight(M.Text));  // No need to fit trailing CRLFs

  Result := FitsLength >= NeededTextLength;
end;

procedure ScaleRBMemoToFit(M: TppMemo; DefaultPointSize: Integer);
var
  Size: Single;
begin
  Size := DefaultPointSize;
  repeat
    if TryToFit(M, Size) then begin
      // M.Lines[0] := FloatToStr(Size);   // debugging aid
      Exit;  // aha, it fits!
    end;

    Size := Size - 0.25;  // go down one point at a time.
  until Size <= 4.0;   // Punt, we don't want to be infinitely small
end;

function RemoveExcessWhitespace(Data: string): string;
var
  SL: TStringList;
  S: string;
begin
  SL := TStringList.Create;
  try
    SL.Text := Trim(Data);
    // The following combines multiple blank lines in to single blank lines
    TrimStringsRight(SL, False);
    while (SL.Count>0) and (SL.Strings[0]='') do
      SL.Delete(0);
    S := SL.Text;
  finally
    FreeAndNil(SL);
  end;
  StrReplace(S, #13#10#13#10#13#10, #13#10#13#10, [rfReplaceAll]);
  StrReplace(S, #13#10#13#10#13#10, #13#10#13#10, [rfReplaceAll]);
  Result := S;
end;

end.
