unit PlatMgmtUtils;
{EB 5/2020 - Plat Management Utils: Rewritten from Stevens Plat Mgr tool}
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StrUtils,  ActiveX, ComObj, ComCtrls, DBClient, DB, DateUtils,
   ShellAPI, OdContainer, OdMiscUtils, DMu, QMConst;

type
  TPlatFile = record
    CompanyName: string;
    Company: string;
    Center: string;
    FilePath: string;
    FileDate: TDateTime;
    Age: string;
    Index: integer;
    FolderName: string;
    Err: string;
    IsDup: boolean;
  end;

  TPlatMatch = record
   Found : boolean;
   PartialFound: boolean;
   MatchNum: integer;
   Method: string;
   Local: TPlatFile;
   Drive: TPlatFile;
   TargetPath: string;
   Status: string;
   Checked: Boolean;
   Retain: Boolean;
   Recorded: Boolean;  {when it is added to grid}
  end;

  TUpdateCounts = record
    Locals: integer;
    Warnings: integer;
    Duplicates: integer;
    New: integer;
    Same: integer;
    Updates: Integer;
    Errors: integer;
    {------After Update------}
    TotalUpdated: Integer;
    NewPlatsAdded: Integer;
    ErrorsDuringUpdate: Integer;
    Skipped: Integer;
    OtherErrorMsg: string;
  end;

  TProcessStage = record
    LocalLoaded: Boolean;
    UpdateListLoaded: Boolean;
  end;


  TPlatMgr = class(TObject)
  private

    fInterruptUpdate: Boolean;
    fEmployeeName, fComputerName: string;  {for logging}
    fProcessStage: TProcessStage;
    aDriveList: TStringList;
    fHintsList: TStringList;
    fKillAppsList: TStringList;
    fLocalUpdatePMList: TStringList;  {Step 1: Local LIST of Update.pm}
    fDriveDupList: TIntegerList; {Step 1.1: Duplicates on the drive}
    fDriveUpdatePMList: TStringList;  {Step 2: External LIST Update.pm}

    fLocalFileInfoDS: TClientDataSet; {Step 1: Dataset - contains Local list (old plats)}
    fUpdatesDS: TClientDataSet;        {Step 2: Dataset - contains need update list}

    fLog: TStringList;
    fUpdateCounts: TUpdateCounts;
    fRichEdit: TRichEdit;    { Available to be used by calling form}
    fDebugPlatDir: string;   { Only used when in debug mode and there is a debug entry for it}

    procedure DeleteDirectory(const DirectoryName: string);

  protected
    function CreateHintsList: boolean;
    function CreateKillAppsList: boolean;
    procedure SearchForUpdatePMFiles(ParentPath: string; StoreIn: TStringList);
    procedure SearchForPM7files(ParentPath: string; StoreIn: TStringList);
    procedure CreatePlatDataSets;
    procedure LogActivity(DrivePath: string; LocalPath: string; ErrorCode: Integer; ErrorMsg: String);
    procedure LogActivityFooter;
    procedure LogInterrupt;
    procedure SaveActivityLog;
    function ExtractPathFromPM7(FullPM7FileName: string): string;
    function ExtractPFDetailsFromPM7(ShortPM7FileName: string): TPlatFile;
    function LocalHasSameFolderName(DriveFolderName: string; LocalPath: string): boolean;
    procedure AppendToUpdates(pPlatMatch: TPlatMatch);
    procedure FlagLocalDuplicates(pPm7FileName:string; pPlatFile: TPlatFile); {After appeand}
    function GetLocalFileCount: Integer;
    procedure ClearUpdateCounts;
    function IsRetainDir(InDir: string): boolean;
  public
    Constructor Create(EmpName: string; CompName: string);
    Destructor Destroy; override;
    procedure LogActivityHeader;
    procedure AssignRichEdit(AFormsRichEdit: TRichEdit);
    function FindDrive(VolLabel: string): string;
    function GetVolumeName(DriveLetter: Char): string;
    function GetLocalPlats(UpdatePMFilePath: string; var CompanyCtrName: string;
                           var InternalName: string; var LastUpdated: TDateTime): string; {Step 1: returns error}
    function CheckForDriveDuplicates : boolean; {Step 1.1 returns a list of drive dups}
    function MatchToDrivePlatRecords(DrivePMFilePath: string; DriveIndex: Integer): string;  {Step 2: returns error}

    {Three main calls}
    function GetPlatList(DS: TDataSource): integer;   {Step 1}
    function GetUpdateList(DS: TDataSource): integer; {Step 2}
    function UpdatePlats(edUpdateLog: TRichEdit): integer; {Step 3}

    procedure SetDebugPlatsLocation(NewLocation: string);  {So, this can be externally set - not for production}
    procedure CancelUpdate;
    procedure ResetAfterCancel;
    procedure KillOtherApps;
    procedure SaveToPlatHistory;
    function GetPlatHistoryDate: TDateTime;

    property ProcessStage: TProcessStage read fProcessStage write fProcessStage;
    property LocalFileCount: integer read GetLocalFileCount;
    property UpdateCounts: TUpdateCounts read fUpdateCounts;
  end;

  {Other calls}
  function ProgIDInstalled(const PID: string): Boolean;



const
  DEFAULT_ROOT = 'C:\';
  PLATS7_VOL='Plats7';
  HINTSFILENAME='hints.txt';
  VIEWERSFILENAME='viewers.txt';
  UPDATE_PM = 'update.pm';
//  OTHER_PM_EXT = 'pm';
  PM7 = 'pm7';
  PM7PW = 'Star_Trek';
  LOG_SEP = '-----------------------';
  DEBUG_PLATS = 'C:\DATA\Plats7';  //EB - take out after testing (looks for this directory if there is no external drive)

  ZIP7_EXE = 'C:\Program Files\7-Zip\7zG.exe';

  ErrNoDrive = 'Error: External Plats Drive/Directory NOT Found.';

  {PlatStatus}
  PLATSTAT_NEW = 'NEW';
  PLATSTAT_WARNING = 'WARNING!';
  PLATSTAT_LWARNINGDUP = 'LOC DUPL';
  PLATSTAT_DWARNINGDUP = 'DRIVE DUPL';
  PLATSTAT_UPV = 'UPV';
  PLATSTAT_ERR = 'INVALID';
  PLATSTAT_UPDATE = 'UPDATE';
  PLATSTAT_SAME = 'SAME';

  RETAINDIR = 'UPV';
implementation

function ProgIDInstalled(const PID: string): Boolean;
var
  WPID: WideString;  // PID as wide string
  Dummy: TGUID;      // unused out value from CLSIDFromProgID function
begin
  WPID := PID;
  Result := ActiveX.Succeeded(
    ActiveX.CLSIDFromProgID(PWideChar(WPID), Dummy)
  );
end;

function IsIEInstalled: Boolean;
begin
  Result := ProgIDInstalled('InternetExplorer.Application');
end;


{ TPlatMgr }
procedure TPlatMgr.AssignRichEdit(AFormsRichEdit: TRichEdit);
begin
  FRichEdit := AFormsRichEdit;
end;

procedure TPlatMgr.CancelUpdate;
begin
  fInterruptUpdate := True;
end;


function TPlatMgr.CheckForDriveDuplicates: boolean;
{Checks for Duplicates within the External drive - files that may have been left on the drive by accident}
var
  i, j : integer;
  DrivePlatA, DrivePlatB: TPlatFile;
  FileNameA, FileNameB: string;
begin
  if (Not assigned(fDriveUpdatePMList)) or (fDriveUpdatePMList.Count = 0) then
    exit;
  fDriveDupList.Clear;
  for i := 0 to fDriveUpdatePMList.Count - 1 do begin
    FileNameA := ExtractFileName(fDriveUpdatePMList[i]);
    DrivePlatA := ExtractPFDetailsFromPM7(FileNameA);
    if (i < (fDriveUpdatePMList.Count-1)) then begin
      for j := (i + 1) to fDriveUpdatePMList.Count - 1 do begin
        FileNameB := ExtractFileName(fDriveUpdatePMList[j]);
        DrivePlatB := ExtractPFDetailsFromPM7(FileNameB);
        if (DrivePlatA.Company = DrivePlatB.Company) and
           (DrivePlatA.Center = DrivePlatB.Center) and
           (DrivePlatA.FolderName = DrivePlatB.FolderName) then
        begin
           fDriveDupList.Add(i);
           fDriveDupList.Add(j);
        end;
      end;
    end;
  end;
  if (fDriveDupList.Count > 0 ) then result := True
  else result := False;
end;

procedure TPlatMgr.ClearUpdateCounts;
begin
  fUpdateCounts.Locals := 0;
  fUpdateCounts.Warnings := 0;
  fUpdateCounts.Duplicates := 0;
  fUpdateCounts.New := 0;
  fUpdateCounts.Same := 0;
  fUpdateCounts.Updates := 0;
  fUpdateCounts.TotalUpdated := 0;
  fUpdateCounts.NewPlatsAdded := 0;
  fUpdateCounts.Errors := 0;
  fUpdateCounts.ErrorsDuringUpdate := 0;
  fUpdateCounts.Skipped := 0;
  fUpdateCounts.OtherErrorMsg := '';
end;

constructor TPlatMgr.Create(EmpName: string; CompName: string);
begin
  fDebugPlatDir := '';
  fEmployeeName := EmpName;
  fComputerName := CompName;
  fProcessStage.LocalLoaded := False;
  fProcessStage.UpdateListLoaded := False;
  fInterruptUpdate := False;
  aDriveList := TStringList.Create;
  fHintsList := TStringList.Create;
  fKillAppsList := TStringList.Create;
  fLocalUpdatePMList := TStringList.Create;
  fDriveDupList := TIntegerList.Create;
  fDriveUpdatePMList := TStringList.Create;
  fLog := TStringList.Create;
  CreatePlatDataSets;
end;

function TPlatMgr.CreateHintsList: boolean;
var
  HintsFilePath: string;
begin
  fHintsList.Clear;
  HintsFilePath :=  ExtractFilePath(Application.ExeName)  + HINTSFILENAME;
  if not FileExists(HintsFilePath) then
    Result := False
  else begin
    fHintsList.LoadFromFile(HintsFilePath);
    Result := True;
  end;
end;

function TPlatMgr.CreateKillAppsList: boolean;

var
  KillAppsPath: string;
begin
  fKillAppsList.Clear;
  KillAppsPath :=  ExtractFilePath(Application.ExeName)  + VIEWERSFILENAME;
  if not FileExists(KillAppsPath) then
    Result := False
  else begin
    fKillAppsList.LoadFromFile(KillAppsPath);
    Result := True;
  end;
end;

procedure TPlatMgr.CreatePlatDataSets;
begin
  fLocalFileInfoDS := TClientDataSet.Create(Application);
  fLocalFileInfoDS.FieldDefs.Add('RecIndex', ftInteger);
  fLocalFileInfoDS.FieldDefs.Add('CompanyName', ftString, 50);
  fLocalFileInfoDS.FieldDefs.Add('LocalPath', ftString, 125);
  fLocalFileInfoDS.FieldDefs.Add('LastUpdated', ftDateTime);
  fLocalFileInfoDS.FieldDefs.Add('Age', ftString, 20);
  fLocalFileInfoDS.FieldDefs.Add('Company', ftString, 10);
  fLocalFileInfoDS.FieldDefs.Add('Center', ftString, 10);
  fLocalFileInfoDS.CreateDataSet;

  fUpdatesDS := TClientDataSet.Create(Application);
  fUpdatesDS.FieldDefs.Add('CheckedOff', ftBoolean);
  fUpdatesDS.FieldDefs.Add('Method', ftString, 15);
  fUpdatesDS.FIeldDefs.Add('CompanyName', ftString, 50);
  fUpdatesDS.FieldDefs.Add('Company', ftString, 20);
  fUpdatesDS.FieldDefs.Add('Center', ftString, 10);
  fUpdatesDS.FieldDefs.Add('PM7Age', ftString, 20);
  fUpdatesDS.FieldDefs.Add('LocalFileAge', ftString, 50);
  fUpdatesDS.FieldDefs.Add('Status', ftString, 15);
  fUpdatesDS.FieldDefs.Add('PM7File', ftString, 125);
  fUpdatesDS.FieldDefs.Add('LocalFilePath', ftString, 125);
  fUpdatesDS.FieldDefs.Add('Retain', ftBoolean);
  fUpdatesDS.FieldDefs.Add('LocalInfo', ftString, 150);
  fUpdatesDS.FieldDefs.Add('Duplicate', ftBoolean);
  fUpdatesDS.FieldDefs.Add('MatchNum', ftInteger);
  fUpdatesDS.FieldDefs.Add('TargetPath', ftString, 150);

  fUpdatesDS.CreateDataSet;
end;



procedure TPlatMgr.DeleteDirectory(const DirectoryName: string);
//Use the ShellAPI to remove a folder, it's subfolders and files
var
  FileOp: TSHFileOpStruct;
begin
  FillChar(FileOp, SizeOf(FileOp), 0);
  FileOp.wFunc := FO_DELETE;
  FileOp.pFrom := PChar(DirectoryName+#0);//double zero-terminated
  FileOp.fFlags := FOF_SILENT or FOF_NOERRORUI or FOF_NOCONFIRMATION;
  SHFileOperation(FileOp);
end;


destructor TPlatMgr.Destroy;
begin
  FreeAndNil(aDriveList);
  FreeAndNil(fHintsList);
  FreeAndNil(fKillAppsList);
  FreeAndNil(fLocalUpdatePMList);
  FreeAndNil(fDriveDupList);
  FreeAndNil(fDriveUpdatePMList);
  FreeAndNil(fLocalFileInfoDS);
  FreeAndNil(fUpdatesDS);
  FreeAndNil(fLog);
end;



function TPlatMgr.ExtractPFDetailsFromPM7(ShortPM7FileName: string): TPlatFile;
var
  DelStrList: TStringList;
  YrVal, MoVal, DayVal, AgeInDays: integer;
  TempFolderName: string;
begin
  DelStrList := TStringList.Create;
  try
    try
      DelStrList.Delimiter := '.';
      DelStrList.StrictDelimiter := True;
      DelStrList.DelimitedText := ShortPM7FileName;
      Result.Company := copy(DelStrList[0], 0, 3);
      Result.Center := copy(DelStrList[0], 4, 3);
        YrVal := StrToInt(DelStrList[1]);
        MoVal := StrToInt(DelStrList[2]);
        DayVal := StrToInt(DelStrList[3]);

      {See if the last part of the pm7 filename is the Company Name}
      if DelStrList.Count > 5 then begin
        TempFolderName := DelStrList[DelStrList.Count - 2];
        if (lowercase(TempFolderName) <> 'plats') then begin
          Result.FolderName := TempFolderName;
          Result.CompanyName := '';
        end
        else
          Result.FolderName := '';
      end;
      
      Result.FileDate := EncodeDate(YrVal, MoVal, DayVal);

          {Get the Age of the Drive file}
        AgeInDays := DaysBetween(Result.FileDate, Now);
        if AgeInDays >= 0 then
          Result.Age := IntToStr(AgeInDays) + ' Days old'
        else if AgeInDays < 0 then
          Result.Age := IntToStr(AgeInDays) + ' Local File newer';
    except
      Result.Err := 'PM7 Filename Error: ' + ShortPM7FileName;
    end;
  finally
    FreeAndNil(DelStrList);
  end;
end;

function TPlatMgr.ExtractPathFromPM7(FullPM7FileName: string): string;
var
  DelStrList: TStringList;
  i: integer;
  NewPath: string;
  RetainPos: integer;
  aStr,bStr: string;
begin
  DelStrList := TStringList.Create;
  try
    RetainPos := 0;  //QM-876 EB Code Cleanup
    DelStrList.Delimiter := '.';
    DelStrList.StrictDelimiter := True;
    DelStrList.DelimitedText := FullPM7FileName;
    NewPath := '';
    {Remove the front Company, Center and Date plus the pm7 extenstion}
    for i := 4 to DelStrList.Count - 2 do begin
      if containsText(DelStrList[i], RETAINDIR) then begin
        RetainPos := AnsiPos(lowercase(RETAINDIR), lowercase(DelStrList[i]));
      end;

      if RetainPos > 0 then begin

        NewPath := RetainDir + '\'; //Copy(DelStrList[i], RetainPos + length(RETAINDIR));

        aStr := AnsiLeftStr(DelStrList[i], RetainPos);
        aStr := StringReplace(aStr, '_', '', [rfReplaceAll, rfIgnoreCase]);
        bStr := AnsiRightStr(DelStrList[i], RetainPos+1);
        bStr := StringReplace(bStr, '_', '', [rfReplaceAll, rfIgnoreCase]);

      end else
        NewPath := NewPath + DelStrList[i] + '\';
    end;

    Result := DEFAULT_ROOT + NewPath;
  finally
    FreeAndNil(DelStrList);
  end;
end;

function TPlatMgr.FindDrive(VolLabel: string): string;
var
  r: LongWord;
  Drives: array[0..128] of char;
  pDrive: pchar;
  DriveName: string;
  AddToList: boolean;
  DriveType: integer; {For reference and tracking}
begin
  Result := '';
  if (aDriveList.Count = 0) then  {If we haven't done so, create a drive list}
    AddtoList := True
  else
    AddtoList := False;
  r := GetLogicalDriveStrings(sizeof(Drives), Drives);
  if r = 0 then exit;
  if r > sizeof(Drives) then
    raise Exception.Create(SysErrorMessage(ERROR_OUTOFMEMORY));
  pDrive := Drives;  // Point to the first drive
  while pDrive^ <> #0 do begin
    If AddtoList then
      aDriveList.Add(pDrive^);

       DriveName := GetVolumeName(pDrive^);
       if (UPPERCASE(DriveName)) = (UPPERCASE(VolLabel)) then begin
         Result := pDrive^;
         DriveType := GetDriveType(pDrive); {Tracking only}
       end;

    inc(pDrive, 4);  // Point to the next drive
  end;
  if Result = '' then Result := ErrNoDrive;
end;



function TPlatMgr.GetPlatHistoryDate: TDateTime;
begin
  Result := GetFileDate(PLAT_FILE_PATH);
end;

function TPlatMgr.GetPlatList(DS: TDataSource): integer;
var
  i,j: integer;
  TopName: string;
  CoCtrName: string;
  Co,Ctr: string;
  LastUpdated: TDateTime;
  UpdatePost: string;
  AgeInDays: integer;
  RecIndex: Integer; {autoinc}
begin
  Result := 0;
  fLocalFileInfoDS.EmptyDataSet;
  RecIndex := 0;
  ClearUpdateCounts;

  {Adds to the passed in list and returns the number of records}
  If not CreateHintsList then begin
    ShowMessage('Cannot process list. Missing hints.txt file.');
    Exit;
  end;

  fLocalUpdatePMList.Clear;
  for i := 0 to  fHintsList.Count - 1 do begin
    if fHintsList[i] <> '' then
      SearchForUpdatePMFiles(fHintsList[i] + '\', fLocalUpdatePMList);
  end;

  for j := 0 to fLocalUpdatePMList.Count - 1 do begin
    CoCtrName := '';
    TopName := '';
    LastUpdated := 2.25;
    GetLocalPlats(fLocalUpdatePMList[j], CoCtrName, TopName, LastUpdated);
    try
      Co := copy(CoCtrName, 0, 3);
      Ctr := copy(CoCtrName, 4, 3);
    except
      Co := CoCtrName;
    end;

    UpdatePost :=  TopName + ' | ' + fLocalUpdatePMList[j] + ' | ' +
                             '  [Last Updated: ' + DateTimeToStr(LastUpdated) + ']';
    Inc(RecIndex);
    fLocalFileInfoDS.Append;
    fLocalFileInfoDS.FieldByName('RecIndex').AsInteger := RecIndex;
    fLocalFileInfoDS.FieldByName('CompanyName').AsString := TopName;   //EB CO
    fLocalFileInfoDS.FieldByName('LocalPath').AsString := fLocalUpdatePMList[j];
    fLocalFileInfoDS.FieldByName('LastUpdated').AsDateTime := LastUpdated;
    AgeInDays := DaysBetween(LastUpdated, Now);
    fLocalFileInfoDS.FieldByName('Age').AsString := IntToStr(AgeInDays) + ' days old';
    if length(CoCtrName) > 5 then  begin
      fLocalFileInfoDS.FieldByName('Company').AsString := Co;
      fLocalFileInfoDS.FieldByName('Center').AsString := Ctr;
    end
    else begin
      fLocalFileInfoDS.FieldByName('Company').AsString := CoCtrName;
      fLocalFileInfoDS.FieldByName('Center').AsString := '';
    end;
    fLocalFileInfoDS.Post;
  end;
  Result := fLocalUpdatePMList.Count;
  fProcessStage.LocalLoaded := True;
  fProcessStage.UpdateListLoaded := False;
  {Hook up the dataset}
  DS.DataSet := fLocalFileInfoDS;
end;



function TPlatMgr.GetUpdateList(DS: TDataSource): integer;
var
  VolDriveLetter : string;
  i: integer;
  aChecked: Boolean;
  DupCount: integer;
begin
  Result := 0;
  DupCount := 0;
  fUpdatesDS.EmptyDataSet;
  fDriveUpdatePMList.Clear;
  VolDriveLetter := FindDrive(PLATS7_VOL);

  if VolDriveLetter = ErrNoDrive then begin
   if not DM.UQState.DeveloperMode then begin
    ShowMessage(ErrNoDrive);
    Exit;
   end;
  end;

  if fDebugPlatDir <> '' then
    VolDriveLetter := fDebugPlatDir
      //DEBUG  if (DM.UQState.DeveloperMode) and (VolDriveLetter = ErrNoDrive) then     {EB - Debug hook}
      //DEBUG   VolDriveLetter := DEBUG_PLATS
  else
    VolDriveLetter := VolDriveLetter   + ':' + '\' + PLATS7_VOL + '\';    {EB - Move to just the Plats7 Directory too}

  SearchForPM7files(VolDriveLetter, fDriveUpdatePMList);
  CheckForDriveDuplicates; //QM-164 Pt 3

  for i := 0 to fDriveUpdatePMList.Count - 1 do begin
     MatchToDrivePlatRecords(fDriveUpdatePMList[i], i);
  end;
  Result := fDriveUpdatePMList.Count;

  Ds.DataSet := fUpdatesDS;
  DS.AutoEdit := True;

  fUpdatesDS.First;
  While not fUPdatesDS.EOF do begin
    aChecked := fUpdatesDS.FieldByName('CheckedOff').AsBoolean;
    fUpdatesDS.Next;
  end;
  if (Result > 0) then
    fProcessStage.UpdateListLoaded := True;

  fUpdatesDS.First;
end;

procedure TPlatMgr.AppendToUpdates(pPlatMatch: TPlatMatch);
{Appends the platmatch data to the table}
begin
   if pPlatMatch.recorded then exit;
   
   fUpdatesDS.Append;
   fUpdatesDS.FieldByName('Duplicate').AsBoolean := pPlatMatch.Drive.IsDup; //False {default setting}
   if pPlatMatch.Drive.IsDup then
     fUpdatesDS.FieldByName('Status').AsString := PLATSTAT_DWARNINGDUP
   else if pPLatMatch.Local.IsDup then
     fUpdatesDS.FieldByName('Status').AsString := PLATSTAT_LWARNINGDUP
   else
     fUpdatesDS.FieldByName('Status').AsString := pPlatMatch.Status;
   fUpdatesDS.FieldByName('CheckedOff').AsBoolean := pPlatMatch.Checked;
   fUpdatesDS.FieldByName('Method').AsString := pPlatMatch.Method;
   fUpdatesDS.FieldByName('CompanyName').AsString := pPlatMatch.Local.CompanyName;
   fUpdatesDS.FieldByName('Company').AsString := pPlatMatch.Drive.Company;
   fUpdatesDS.FieldByName('Center').AsString := pPlatMatch.Drive.Center;
   fUpdatesDS.FieldByName('PM7Age').AsString := pPlatMatch.Drive.Age;
   fUpdatesDS.FieldByName('LocalFileAge').AsString := pPlatMatch.Local.Age;
   fUpdatesDS.FieldByName('PM7File').AsString := pPlatMatch.Drive.FilePath;
   fUpdatesDS.FieldByName('LocalFilePath').AsString := pPlatMatch.Local.FilePath;
   fUpdatesDS.FieldByName('Retain').AsBoolean := pPlatMatch.Retain;
   fUpdatesDS.FieldByName('LocalInfo').AsString := pPlatMatch.Local.Company + pPlatMatch.Local.Center + ' ' + pPlatMatch.Local.FilePath;
   fUpdatesDS.FieldByName('MatchNum').AsInteger := pPlatMatch.MatchNum;
   fUpdatesDS.FieldByName('TargetPath').AsString := pPlatMatch.TargetPath;
   fUpdatesDS.Post;

   pPlatMatch.recorded := true;
end;


procedure TPlatMgr.FlagLocalDuplicates(pPM7FileName:string; pPlatFile: TPlatFile);
var
  UpdatesCoCtr: string;
  IntendedPath, LocalPath: string;
  Dups: integer;
begin
  fUpdatesDS.First;
  Dups := 0;
  while not fUpdatesDS.EOf do begin
    UpdatesCoCtr := fUpdatesDS.FieldByName('Company').AsString + fUpdatesDS.FieldByName('Center').AsString;
    IntendedPath := ExtractPathFromPM7(pPM7FileName);
    LocalPath := ExtractFileDir(fUpdatesDS.FieldByName('LocalFilePath').AsString);
    if (UpdatesCoCtr = (pPlatFile.Company + pPlatFile.Center)) then begin
      fUpdatesDS.Edit;
      fUpdatesDS.FieldByName('Duplicate').AsBoolean := True;
      Inc(Dups);
      if (LocalPath = IntendedPath) then
        fUpdatesDS.FieldByName('CheckedOff').AsBoolean := True

      else begin
        fUpdatesDS.FieldByName('CheckedOff').AsBoolean := False;
        fUpdatesDS.FieldByName('Status').AsString := PLATSTAT_LWARNINGDUP;
      end;

      fUpdatesDS.Post;
    end;

  fUpdatesDS.Next;
  end;
  fUpdateCounts.Duplicates := Dups;
end;

function TPlatMgr.MatchtoDrivePlatRecords(DrivePMFilePath: string; DriveIndex: integer): string;
   procedure ClearLocalPlat(pPlatMatch: TPlatMatch);
   begin
     pPlatMatch.Found := False;
     pPlatMatch.PartialFound := False;
     pPlatMatch.Local.Index := -1;
     pPlatMatch.Local.Company := '';
     pPlatMatch.Local.Center := '';
     pPlatMatch.Local.Age := '';
     pPlatMatch.Local.FileDate := 2.25;
     pPlatMatch.Status := '';
     pPlatMatch.Checked := False;
     pPlatMatch.Recorded := False;
//     pPlatMatch.TargetPath := '';
//     pPlatMatch.Retain := False;  //Just added
   end;
var
  JustFileName: string;
  TempList: TStringList;
  FieldIndex, matchcount, partialmatchcount: Integer;
  AgeInDays: integer;
  LocCompanyCenter: string;
  PM7IntendedFilePath, PM7TargetDir, LocDir: string;
  DupIndexList: TIntegerList;
  APlatMatch: TPlatMatch;
  DupIdxIndex: integer;
begin
  JustFileName := ExtractFileName(DrivePMFilePath);
  TempList := TStringList.Create;   {Pull out the file information}
  DupIndexList := TIntegerList.Create;
//  if ansipos(RETAINDIR, uppercase(DrivePMFilePath)) > 0 then
  if containsText(DrivePMFilePath, RetainDir) then
    APlatMatch.Retain := True;
  Pm7IntendedFilePath := ExtractPathFromPM7(DrivePMFilePath);
  PM7TargetDir := ExtractFileDir(Pm7IntendedFilePath);
  APlatMatch.TargetPath := pm7IntendedFilePath;
  DupIdxIndex := -1;
  try
    ClearLocalPlat(APlatMatch);
    matchcount := 0; {reset}
    partialmatchcount := 0; {reset}

    Templist.Delimiter := '.';
    TempList.StrictDelimiter := True;
    TempList.DelimitedText := JustFileName;

    {Go to the last line in the text file}
    FieldIndex := TempList.Count - 1;
    if FieldIndex > 0 then begin
      APlatMatch.Drive := ExtractPFDetailsFromPM7(JustFileNAme);
      APlatMatch.Drive.FilePath := DrivePMFilePath;
      APlatMatch.Method := 'Update.pm';
    end;

    if fProcessStage.LocalLoaded then begin
    {Get the Age of the Drive file}
      AgeInDays := DaysBetween(APlatMatch.Drive.FileDate, Now);
      if AgeInDays >= 0 then
        APlatMatch.Drive.Age := IntToStr(AgeInDays) + ' Days old'
      else if AgeInDays < 0 then
        APlatMatch.Drive.Age := IntToStr(AgeInDays) + ' Local File newer';

      {Try to find a match with the local files}
      fUpdateCounts.Locals := fLocalFileInfoDS.RecordCount;
      fLocalFileInfoDS.First;
      while not fLocalFileInfoDS.EOF do begin

        ClearLocalPlat(APlatMatch);
        LocCompanyCenter := fLocalFileInfoDS.FieldByName('Company').AsString +
                            fLocalFileInfoDS.FieldByName('Center').AsString;
        LocDir := ExtractFileDir(fLocalFileInfoDS.FieldByName('LocalPath').AsString);

        {Full Match - Company | Cener | FilePath}
        if  ((uppercase(LocCompanyCenter)) = (uppercase(APlatMatch.Drive.Company + APlatMatch.Drive.Center))) and
            ((lowercase(PM7TargetDir)) = (lowercase(LocDir))) then begin
          inc(matchcount);
          APlatMatch.Local.Index := fLocalFileInfoDS.FieldByName('RecIndex').AsInteger;
          APlatMatch.MatchNum := matchcount;
          APlatMatch.Found := True;
          APlatMatch.Local.FileDate := fLocalFileInfoDS.FieldByName('LastUpdated').AsDateTime;
          APlatMatch.Local.FilePath := fLocalFileInfoDS.FieldByName('LocalPath').AsString;
          APlatMatch.Local.CompanyName := fLocalFileInfoDS.FieldByName('CompanyName').AsString;
          APlatMatch.Local.Company :=  fLocalFileInfoDS.FieldByName('Company').AsString;
          APlatMatch.Local.Center := fLocalFileInfoDS.FieldByName('Center').AsString;

          {Determine age of local vs. drive}
          AgeInDays := DaysBetween(APlatMatch.Local.FileDate, APlatMatch.Drive.FileDate);
          if APlatMatch.Drive.FileDate > APlatMatch.Local.FileDate then begin
            APlatMatch.Local.Age := 'Your plats are ' + IntToStr(AgeInDays) + '  older than PM7';
            APlatMatch.Status := PLATSTAT_UPDATE;
            APlatMatch.Checked := True;
            Inc(fUpdateCounts.Updates);
          end
          else if APlatMatch.Drive.FileDate < APlatMatch.Local.FileDate then begin
            APlatMatch.Local.Age := 'Your plats are ' + IntToStr(AgeInDays) + ' newer than PM7';
            APlatMatch.Status := PLATSTAT_WARNING;
            APlatMatch.Checked := False;
            Inc(fUpdateCounts.Warnings);
          end
          else if APlatMatch.Drive.FileDate = APlatMatch.Local.FileDate then begin
            APlatMatch.Local.Age := 'Your plats are same date as PM7';
            APlatMatch.Status := PLATSTAT_SAME;
            APlatMatch.Checked := False;
            Inc(fUpdateCounts.Same);
          end;
          DupIdxIndex := fDriveDupList.IndexOf(DriveIndex);  //QM-164 Pt 3
          if DupIdxIndex > -1 then
            APlatMatch.Drive.IsDup := True;

          AppendToUpdates(APlatMatch);
          If APlatMatch.Found then Break;
       end

       {Partial Match - Comany | Center | Name of folder}
       else if (LocCompanyCenter = (APlatMatch.Drive.Company + APlatMatch.Drive.Center)) and
                LocalHasSameFolderName(APlatMatch.Drive.FolderName,
                    fLocalFileInfoDS.FieldByName('LocalPath').AsString) then begin
         inc(partialmatchcount);
         APlatMatch.PartialFound := True;
         APlatMatch.Status := PLATSTAT_LWARNINGDUP;
         Inc(fUpdateCounts.Warnings);
         APlatMatch.Local.Index := fLocalFileInfoDS.FieldByName('RecIndex').AsInteger;
          APlatMatch.MatchNum := partialmatchcount;
          APlatMatch.Local.FileDate := fLocalFileInfoDS.FieldByName('LastUpdated').AsDateTime;
          APlatMatch.Local.FilePath := fLocalFileInfoDS.FieldByName('LocalPath').AsString;
          APlatMatch.Local.CompanyName := fLocalFileInfoDS.FieldByName('CompanyName').AsString;
          APlatMatch.Local.Company :=  fLocalFileInfoDS.FieldByName('Company').AsString;
          APlatMatch.Local.Center := fLocalFileInfoDS.FieldByName('Center').AsString;
          APlatMatch.Checked := False;
          {Determine age of local vs. drive}
          AgeInDays := DaysBetween(APlatMatch.Local.FileDate, APlatMatch.Drive.FileDate);
          if APlatMatch.Drive.FileDate > APlatMatch.Local.FileDate then
            APlatMatch.Local.Age := 'Your plats are ' + IntToStr(AgeInDays) + '  older than PM7'
          else if APlatMatch.Drive.FileDate < APlatMatch.Local.FileDate then
            APlatMatch.Local.Age := 'Your plats are ' + IntToStr(AgeInDays) + ' newer than PM7'
          else if APlatMatch.Drive.FileDate = APlatMatch.Local.FileDate then begin
            APlatMatch.Local.Age := 'Your plats are same date as PM7';
          end;
          DupIdxIndex := fDriveDupList.IndexOf(DriveIndex);  //QM-164 Pt 3
          if DupIdxIndex > -1 then
            APlatMatch.Drive.IsDup := True;
          AppendToUpdates(APlatMatch);
       end;

        fLocalFileInfoDS.Next;
      end; {While}

      {New one}
      if (not APlatMatch.Found) and (not APlatMatch.PartialFound) then begin
        if APlatMatch.Drive.Err <> '' then begin
          APlatMatch.Drive.FilePath := APlatMatch.Drive.Err;
          APlatMatch.Status := PLATSTAT_ERR;
          APlatMatch.Drive.Company := '';
          APlatMatch.Drive.CompanyName := '';
          APlatMatch.Drive.Center := '';
          APlatMatch.Drive.Age := '';
          APlatMatch.Checked := False;
          DupIdxIndex := fDriveDupList.IndexOf(DriveIndex);  //QM-164 Pt 3
          if DupIdxIndex > -1 then
            APlatMatch.Drive.IsDup := True;
          AppendToUpdates(APlatMatch);
          inc(fUpdateCounts.Errors);
        end
        else begin
              {Check for UPV}
           if IsRetainDir(Pm7IntendedFilePath) then begin
             APlatMatch.Status := PLATSTAT_UPV;
             APlatMatch.Local.Age := 'Shared Dir (UPV)';
             DupIdxIndex := fDriveDupList.IndexOf(DriveIndex);  //QM-164 Pt 3
             if DupIdxIndex > -1 then
               APlatMatch.Drive.IsDup := True;
             AppendToUpdates(APlatMatch);
             inc(fUpdateCounts.New);
           end
           else begin
             APlatMatch.Status := PLATSTAT_NEW;
             APlatMatch.Checked := True;
             APlatMatch.Local.Age := 'Local file not found.';
             DupIdxIndex := fDriveDupList.IndexOf(DriveIndex);  //QM-164 Pt 3
             if DupIdxIndex > -1 then
               APlatMatch.Drive.IsDup := True;
             AppendToUpdates(APlatMatch);
             inc(fUpdateCounts.New);
           end;
        end;
      end;
      if (partialmatchcount > 1) and (matchcount > 1) and (APlatMatch.PartialFound) then
        FlagLocalDuplicates(DrivePMFilePath, APlatMatch.Drive);
      fProcessStage.UpdateListLoaded := True;
    end;
  finally
    FreeAndNil(TempList);
    FreeAndNil(DupIndexList);
  end;
end;



procedure TPlatMgr.ResetAfterCancel;
begin
  fInterruptUpdate := False;
end;

function TPlatMgr.GetLocalFileCount: Integer;
begin
  if (ProcessStage.LocalLoaded) and (fLocalFileInfoDS.Active) then begin
    Result := fLocalFileInfoDS.RecordCount;
    fUpdateCounts.Locals := Result;
  end
  else
    Result := fUpdateCounts.Locals;
end;

function TPlatMgr.GetLocalPlats(UpdatePMFilePath: string; var CompanyCtrName: string; var InternalName: string;
  var LastUpdated: TDateTime): string;  {Returns Error}
var
  TempList: TStringList;
  LastLine: string;
  YrVal, MoVal, DayVal: integer;
begin
  TempList := TStringList.Create;
  Result := '';
  try
    try
      TempList.LoadFromFile(UpdatePMFilePath);
      InternalName := TempList[0];
      LastLine := TempList[TempList.Count-1];

      {Clear and reuse the TempList}
      TempList.Clear;
      TempList.Delimiter       := '.';
      TempList.StrictDelimiter := True;
      TempList.DelimitedText := LastLine;

      CompanyCtrName := TempList[0];

      YrVal := StrToInt(TempList[1]);
      MoVal := StrToInt(TempList[2]);
      DayVal := StrToInt(TempList[3]);
      LastUpdated := EncodeDate(YrVal, MoVal, DayVal);
    except
      Result := 'Unable to find date of last update';
    end;

  finally
    FreeAndNil(TempList);
  end;
end;



function TPlatMgr.GetVolumeName(DriveLetter: Char): string;
var
  dummy: DWORD;
  buffer: array[0..MAX_PATH] of Char;
  oldmode: LongInt;
begin
  oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
    GetVolumeInformation(PChar(DriveLetter + ':\'),
                         buffer,
                         SizeOf(buffer),
                         nil,
                         dummy,
                         dummy,
                         nil,
                         0);
    Result := StrPas(buffer);
  finally
    SetErrorMode(oldmode);
  end;
end;

function TPlatMgr.IsRetainDir(InDir: string): boolean;
var
  FullRetainDir: string;
begin
  FullRetainDir := DEFAULT_ROOT + RetainDir;
  if containsText(FullRetainDir, InDir) then  
    Result := True
  else
    Result := False;
end;

procedure TPlatMgr.KillOtherApps;
var
  i: integer;
  FullName: string;
begin
  If CreateKillAppsList then begin
    for i := 0 to fKillAppsList.Count - 1 do begin
      FullName := fKillAppsList[i] + '.exe';
      DM.KillTask(FullName);
    end;
  end;

end;


function TPlatMgr.LocalHasSameFolderName(DriveFolderName: string; LocalPath: string): boolean;
begin
  result := containstext(LocalPath, DriveFolderName);

//  if (ansipos(lowercase(DriveFolderName), lowercase(LocalPath)) > 0 ) then
//    result := True
//  else
//    result := False;
end;

procedure TPlatMgr.LogActivity(DrivePath, LocalPath: string; ErrorCode: Integer;
  ErrorMsg: String);
var
  LogMsg: string;
begin
  if ErrorCode > 0 then begin
    fRichEdit.SelAttributes.Color := clRed;
    LogMsg :=  DateTimeToStr(Now) + ':     ERROR: [ERROR CODE:' + IntToStr(ErrorCode) + '] ' +
               ErrorMsg + ' - ' + DrivePath + ' -> ' + LocalPath;
  end
  else begin
    fRichEdit.SelAttributes.Color := clGreen;
    LogMsg := DateTimeToStr(Now) + ':     SUCCESS: ' + DrivePath + ' -> ' + LocalPath;
  end;

  fLog.Add(LogMsg);
  fRichEdit.Lines.Add(LogMsg);
end;

procedure TPlatMgr.LogActivityFooter;
var
  TempLines: TStringList;
  SLine: string;
begin
  TempLines := TStringList.Create;
  try
 //   TempLines.Add(LOG_SEP);
    fRichEdit.SelAttributes.Color := clBlue;
    fRichEdit.SelAttributes.Style := [fsBold];
    TempLines.Add('Total pm7 files Updated: ' + IntToStr(fUpdateCounts.TotalUpdated));
    TempLines.Add('New pm7 files Added: ' + IntToStr(fUpdateCounts.NewPlatsAdded));
    fRichEdit.Lines.AddStrings(TempLines);
    fLog.AddStrings(TempLines);

    if fUpdateCounts.Skipped > 0 then begin
      fRichEdit.SelAttributes.Color := clBlack;
      fRichEdit.SelAttributes.Style := [fsBold];
      sLine := 'Skipped pm7 files: ' + IntToStr(fUpdateCounts.Skipped);
      fRichEdit.Lines.Add(sLine);
      fLog.Add(sLine);
      fRichEdit.Lines.Add('');
      fLog.Add('');
    end;

    TempLines.Clear;
    if fUpdateCounts.ErrorsDuringUpdate > 0 then begin
      TempLines.Add('ERRORS DURING UPDATE: ' + IntToStr(fUpdateCounts.ErrorsDuringUpdate) + ' - ' + fUpdateCounts.OtherErrorMsg);
      fLog.AddStrings(TempLines);
      fRichEdit.SelAttributes.Color := clRED;
      fRichEdit.SelAttributes.Style := [fsBold];
      fRichEdit.Lines.AddStrings(TempLines);
      TempLines.Add(LOG_SEP);
    end;

  finally
    FreeAndNil(TempLines);
  end;
end;

procedure TPlatMgr.LogActivityHeader;
var
  TempLines: TStringlist;
begin
  TempLines := TStringList.Create;
  try
    fRichEdit.Lines.Add('');
      TempLines.Add('PLAT UPDATE - ' + DateTimeToStr(Now));
      TempLines.Add('NAME: ' + fEmployeeName);
      TempLines.Add('COMPUTER NAME: ' + fComputerName);
      TempLines.Add('');
      fLog.Add(TempLines.Text);

      fRichEdit.SelAttributes.Style := [fsBold];
      fRichEdit.SelAttributes.Color := clNAVY;
      fRichEdit.Lines.AddStrings(TempLines);

      TempLines.Clear;

//      TempLines.Add(LOG_SEP);
      TempLines.Add('# Plats on Local Machine: ' + IntToStr(fUpdateCounts.Locals));
      TempLines.Add('New: ' + IntToStr(fUpdateCounts.New));
      TempLines.Add('Warnings: ' + IntToStr(fUpdateCounts.Warnings));
      TempLines.Add('Duplicates: ' + IntToStr(fUpdateCounts.Duplicates));
      TempLines.Add('Need Updating: ' + IntToStr(fUpdateCounts.Updates));
//      TempLines.Add(LOG_SEP);
      TempLines.Add('');

      TempLines.Add('UPDATES:');


      fLog.AddStrings(TempLines);
      fRichEdit.Lines.AddStrings(TempLines);

  finally
    FreeAndNil(TempLines);
  end;

end;

procedure TPlatMgr.LogInterrupt;
var
  TempLines: TStringlist;
begin
  TempLines := TStringList.Create;
  try

    TempLines.Add('-> Process has been interrupted by user <-');
    TempLines.Add('');

    fRichEdit.SelAttributes.Color := clRED;
    fLog.AddStrings(TempLines);
    fRichEdit.Lines.AddStrings(TempLines);
  finally
    FreeAndNil(TempLines);
  end;
end;

procedure TPlatMgr.SaveActivityLog;
var
  LogFileName: string;
begin
  if fLog.Count > 0 then begin
    LogFileName := ExtractFilePath(Application.ExeName) + '\' + 'Plats Update Log';
    if Assigned(fRichEdit) then begin
      fRichEdit.Lines.SaveToFile(LogFileName + '.rtf');
    end
    else
    fLog.SaveToFile(LogFileName + '.txt');
    SaveToPlatHistory;
  end;
end;

procedure TPlatMgr.SaveToPlatHistory; //QM-402 EB
var
  HistoryLog: TStringList;
  i: integer;
begin
  {This is really just a text file with HTML tags to replace the old PlatsHistory.html}
  HistoryLog := TStringList.Create;
  try
    HistoryLog.Add('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">');
    HistoryLog.Add('<html>');
    HistoryLog.Add('<head>');
    HistoryLog.Add('<title></title>');
    HistoryLog.Add('<STYLE TYPE="text/css">');
    HistoryLog.Add('body {');
    HistoryLog.Add('font-family: Tahoma, sans-serif;');
    HistoryLog.Add('font-size: x-small ;');
    HistoryLog.Add('}');
    HistoryLog.Add('</STYLE>');
    HistoryLog.Add('</head>');
    HistoryLog.Add('<body>');

    HistoryLog.Add('<p>');
    for i := 0 to fLog.Count - 1 do begin
      HistoryLog.Add(fLog.Strings[i]);
      HistoryLog.Add('<br>');
    end;
    HistoryLog.Add('</p>');

  //  HistoryLog.AddStrings(fLog);


    HistoryLog.Add('</body>');
    HistoryLog.Add('</html>');
    HistoryLog.SaveToFile(PLAT_FILE_PATH);       //plathistory.html
  finally
    FreeAndNil(HistoryLog);
  end;
end;

procedure TPlatMgr.SearchForPM7files(ParentPath: string; StoreIn: TStringList);
var
  searchResult: TSearchRec;
  TargetPath: string;
  Err: integer;
  CurFileName, CurExt, ShortFileName: string;
  j: integer;
begin

  TargetPath := IncludeTrailingBackSlash(ParentPath) +  '*.*';  //QM-164 Need to only focus on the Plats7 folder
  Err := FindFirst(TargetPath, (faAnyFile or faDirectory), searchResult);
  if Err = 0 then begin
    try
      repeat
        if ((searchResult.Attr and faDirectory) = 0) and
           ((searchResult.Attr and faSysFile) = 0) and
           ((searchResult.Attr and faHidden) = 0) then begin
          CurFileName := IncludeTrailingBackSlash(ParentPath) + searchResult.Name;
          ShortFileName := ExtractFileName(CurFileName);
          {Check the file extension to see if it is *.pm7 file}
          CurExt := ExtractFileExt(CurFileName);

          if containstext(ShortFileName, PM7) then begin
           if not StoreIn.Find((CurFileName), j) then
              StoreIn.Add(CurFileName);
          end;

//          If (AnsiPos(PM7, lowercase(ShortFileName)) > 0) then begin
//            if not StoreIn.Find((CurFileName), j) then
//              StoreIn.Add(CurFileName);
//          end;
        end
        else if (searchResult.Name <> '.') and (searchResult.Name <> '..') then begin
          SearchForPM7files(IncludeTrailingBackslash(ParentPath) + searchResult.Name, StoreIn);
        end;
      until FindNext(searchResult) <> 0
    finally
      FindClose(searchResult);
    end;
  end;
end;

procedure TPlatMgr.SearchForUpdatePMFiles(ParentPath: string; StoreIn: TStringlist);
var
  searchResult: TSearchRec;
  TargetPath: string;
  Err: integer;
  CurFileName: string;
  j: integer;

begin

  TargetPath := IncludeTrailingBackSlash(ParentPath) + '*.*';
  Err := FindFirst(TargetPath, (faAnyFile or faDirectory), searchResult);
  if Err = 0 then begin
    try
      repeat
      {We are only searching for update.pm files}
        if (searchResult.Attr and faDirectory) = 0 then begin
          CurFileName := IncludeTrailingBackSlash(ParentPath) + searchResult.Name;
          if (LOWERCASE(ExtractFileName(CurFileName))) = (LOWERCASE(UPDATE_PM)) then
            if not StoreIn.Find(LOWERCASE(ExtractFileName(CurFileName)), j) then
              StoreIn.Add(CurFileName);

        end else if (searchResult.Name <> '.') and (searchResult.Name<>'..') then begin
          SearchForUpdatePMFiles(IncludeTrailingBackslash(ParentPath) + searchResult.Name, StoreIn);
        end;
      until FindNext(searchResult) <> 0
    finally
      FindClose(searchResult);
    end;
  end;
end;

procedure TPlatMgr.SetDebugPlatsLocation(NewLocation: string);
begin
  fDebugPlatDir := NewLocation;
end;

function TPlatMgr.UpdatePlats(edUpdateLog: TRichEdit): integer;
var
  ErrorCode: integer;
  UpdateRec, Params7z: string;
  ErrorMsg: string;
  LocalFileDir: string;
  aChecked: Boolean;
  Zip7Loc: string;
  newplat: boolean;
  ItRan: boolean;

begin
  newplat := False;
  Result := 0;
  Zip7Loc := '';
  LocalFileDir := '';
  ItRan := False;
  fLog.Clear;
  fUpdatesDS.First;
  LogActivityHeader;

  {Clear counts related to the update}
  fUpdateCounts.ErrorsDuringUpdate := 0;
  fUpdateCounts.TotalUpdated := 0;
  fUpdateCounts.NewPlatsAdded := 0;

  while not fUpdatesDS.EOF do begin
    if fInterruptUpdate then
      break;
    newplat := False;
    aChecked := fUpdatesDS.FieldByName('CheckedOff').AsBoolean;
    if (fUpdatesDS.FieldByName('Status').AsString = PLATSTAT_ERR)then begin
      inc(fUpdateCounts.ErrorsDuringUpdate);
      fUpdateCounts.OtherErrorMsg := 'Invalid Plat File. There is a problem with this file and it cannot be used.';
    end
    else if (not aChecked) then  {Skip File Errors}
      inc(fUpdateCounts.Skipped)
    else begin
      if aChecked then begin
      //  inc(fUpdateCounts.TotalUpdated);
        If ((fUpdatesDS.FieldByName('Status').AsString) = PLATSTAT_NEW) then begin
          newplat := True;
          inc(fUpdateCounts.NewPlatsAdded);
          LocalFileDir := ExtractPathFromPM7(fUpdatesDS.FieldByName('PM7File').AsString);
        end
        else begin
          newplat := False;
          LocalFileDir :=  ExtractFileDir(fUpdatesDS.FieldByName('LocalFilePath').AsString);
          inc(fUpdateCounts.TotalUpdated);
        end;
        try
          if not fUpdatesDS.FieldByName('Retain').AsBoolean then begin
            DeleteDirectory(LocalFileDir);
            ForceDirectories(LocalFileDir);
          end;

          UpdateRec := fUpdatesDS.FieldByName('PM7File').AsString;  //AView.DataController.Values[I, PlatsExternalFileOnDisk.Index];
          Params7z := ' x ' + '"' + UpdateRec + '"'  + ' -o' + '"' + LocalFileDir + '"' + ' -pStar_Trek -y -aoa ';

          ItRan := ExecuteProcess(ZIP7_EXE, Params7z, '', True, False, False, ErrorCode);

          case ErrorCode of
            0:ErrorMsg:='No Error';

            1:ErrorMsg:='1: Warning (Non fatal error(s))';
            2:ErrorMsg:='2: Fatal error';
            7:ErrorMsg:='7: Command line error';
            8:ErrorMsg:='8: Not enough memory for operation';
            255:ErrorMsg:='255: User stopped the process';
          else
            ErrorMsg := IntToStr(ErrorCode) + ':' + '7 Zip Encoutered an Error extracting files';
          end;
        except
          ErrorMsg := '7 Zip Encoutered an Error extracting files: ' + UpdateRec;
          ErrorCode := 99;
        end;
        if ErrorCode > 0 then begin
          inc(fUpdateCounts.ErrorsDuringUpdate);
          if newplat then
            dec(fUpdateCounts.NewPlatsAdded)
          else
            dec(fUpdateCounts.TotalUpdated);   //If there was an error, remove it from updated count
        end;
        LogActivity(UpdateRec, LocalFileDir, ErrorCode, ErrorMsg);
      end; {Checked}

    end;
    fUPdatesDS.Next;
  end;
  Result := fUpdateCounts.NewPlatsAdded;  //QM-16 for QM-800 Cleanup 1/2024
  if fInterruptUpdate then
    LogInterrupt;
  LogActivityFooter;
  SaveActivityLog;
end;



end.
