unit FileDetector;
{This adapted component is based on a modified form of TSHChangeNotify component
 http://www.torry.net/authorsmore.php?id=1930) by Elliott Shevin (shevine@aol.com,
 vers. 3.0, October 2000.  The modified component this was adapted from was found at:
 http://www.howtodothings.com/computers/a1154-getting-notifications-from-the-shell.html

Note:  Did observe that the shell has a 10 item event buffer.  More info here:
http://web.archive.org/web/20021016130640/www.geocities.com/SiliconValley/4942/notify.html
 }

interface

uses Classes, Windows, Messages, SysUtils, Controls, Forms, Dialogs,
  ActiveX, ComObj, ShlObj;

type
  NOTIFYREGISTER = record
    pidlPath: PItemIDList;
    bWatchSubtree: boolean;
  end;
  PNOTIFYREGISTER = ^NOTIFYREGISTER;

const
  SNM_SHELLNOTIFICATION = WM_USER + 1;
  SHCNF_ACCEPT_INTERRUPTS = $0001;
  SHCNF_ACCEPT_NON_INTERRUPTS = $0002;
  SHCNF_NO_PROXY = $8000;

type
  TNotificationEvent = (neAssociationChange, neAttributesChange,
    neFileChange, neFileCreate, neFileDelete, neFileRename,
    neDriveAdd, neDriveRemove, neShellDriveAdd, neDriveSpaceChange,
    neMediaInsert, neMediaRemove, neFolderCreate, neFolderDelete,
    neFolderRename, neFolderUpdate, neNetShare, neNetUnShare,
    neServerDisconnect, neImageListChange);

  TNotificationEvents = set of TNotificationEvent;

  TFileDetectorEvent1 = procedure(Sender: TObject; Path: string) of object;

  TFileDetector = class(TComponent)
  private
    fWatchEvents: TNotificationEvents;
    fPath: string;
    fActive, fWatch: Boolean;

    prevPath1: string;
    PrevEvent: Integer;

    Handle, NotifyHandle: HWND;

    FOnCreate: TFileDetectorEvent1;

    function PathFromPidl(Pidl: PItemIDList): string;
    procedure SetWatchEvents(const Value: TNotificationEvents);

    function GetActive: Boolean;
    procedure SetActive(const Value: Boolean);

    procedure SetPath(const Value: string);
    procedure SetWatch(const Value: Boolean);
  protected
    procedure ShellNotifyRegister;
    procedure ShellNotifyUnregister;
    procedure WndProc(var Message: TMessage);

    procedure DoCreateFile(Path: string); dynamic;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Path: string read fPath write SetPath;
    property Active: Boolean read GetActive write SetActive;
    property WatchSubTree: Boolean read fWatch write SetWatch;
    property WatchEvents: TNotificationEvents read fWatchEvents write SetWatchEvents;

    property EventHandle: HWND read NotifyHandle;

    property OnFileCreate: TFileDetectorEvent1 read FOnCreate write FOnCreate;
  end;


function SHChangeNotifyRegister(hWnd: HWND; dwFlags: Integer;
  wEventMask: Cardinal; uMsg: UINT; cItems: Integer;
  lpItems: PNOTIFYREGISTER): HWND; stdcall;
function SHChangeNotifyDeregister(hWnd: HWND): Boolean; stdcall;
function SHILCreateFromPath(Path: PWideChar; var PIDL: PItemIDList;
  var Attributes: ULONG): HResult; stdcall;


procedure Register;

implementation

uses JCLSysInfo;

const
  Shell32DLL = 'shell32.dll';

{$WARN SYMBOL_PLATFORM OFF}
function SHChangeNotifyRegister; external Shell32DLL index 2;
function SHChangeNotifyDeregister; external Shell32DLL index 4;
function SHILCreateFromPath; external Shell32DLL index 28;

function ILCreateFromPath(lpszPath: Pointer): PItemIDList; stdcall;
  external Shell32DLL index 157;
{$WARN SYMBOL_PLATFORM ON}

function GetPidlFromPath(const sPath: string): PItemIDList;
var
  szPath: array[0..MAX_PATH] of Char;
  uiMode: UINT;
  wzPath: array[0..MAX_PATH] of WideChar;
begin
  uiMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  if Win32Platform = VER_PLATFORM_WIN32_NT then begin
    StringToWideChar(sPath, wzPath, MAX_PATH);
    Result := ILCreateFromPath(@wzPath);
  end
  else begin
    StrPCopy(szPath, sPath);
    Result := ILCreateFromPath(@szPath);
  end;
  SetErrorMode(uiMode);
end;


{ TFileDetector }

constructor TFileDetector.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  if not (csDesigning in ComponentState) then
    Handle := Classes.AllocateHWnd(WndProc);

  PrevEvent := 0;

  fWatchEvents := [neFileCreate, neFolderUpdate];
end;

destructor TFileDetector.Destroy;
begin
  if not (csDesigning in ComponentState) then
    Active := False;

  if Handle <> 0 then
    Classes.DeallocateHWnd(Handle);

  inherited Destroy;
end;

procedure TFileDetector.DoCreateFile(Path: string);
begin
  if Assigned(fOnCreate) then
    FOnCreate(Self, Path)
end;

function TFileDetector.GetActive: Boolean;
begin
  Result := (NotifyHandle <> 0) and (fActive);
end;

function TFileDetector.PathFromPidl(Pidl: PItemIDList): string;
var
  Path : LPCTSTR;
  AMalloc: IMalloc;
begin
  Path := StrAlloc(MAX_PATH);

  if SHGetPathFromIDList(Pidl, Path) then begin
    if (GetWindowsVersion = wvWinXP) then
      Result := IncludeTrailingPathDelimiter(Path)
    else
      Result := ExtractFilePath(Path);
  end
  else
    Result := '';

  SHGetMalloc(AMalloc);
  AMalloc.Free(Pidl);
  StrDispose(Path);
end;

procedure TFileDetector.SetActive(const Value: Boolean);
begin
  if (Value <> fActive) then begin
    fActive := Value;
    if fActive then
      ShellNotifyRegister
    else
      ShellNotifyUnregister;
  end;
end;

procedure TFileDetector.SetPath(const Value: string);
begin
  if fPath <> Value then begin
    fPath := Value;
    ShellNotifyRegister;
  end;
end;

procedure TFileDetector.SetWatch(const Value: Boolean);
begin
  if fWatch <> Value then begin
    fWatch := Value;
    ShellNotifyRegister;
  end;
end;

procedure TFileDetector.SetWatchEvents(const Value: TNotificationEvents);
begin
  if fWatchEvents <> Value then begin
    fWatchEvents := Value;
    ShellNotifyRegister;
  end;
end;

procedure TFileDetector.ShellNotifyRegister;
var
  Option: TNotificationEvent;
  NotifyRecord: NOTIFYREGISTER;
  Flags: DWORD;
  Pidl: PItemIDList;
  Attributes: ULONG;
const
  NotifyFlags: array[TNotificationEvent] of DWORD = (SHCNE_ASSOCCHANGED,
    SHCNE_ATTRIBUTES, SHCNE_UPDATEITEM, SHCNE_CREATE, SHCNE_DELETE,
    SHCNE_RENAMEITEM, SHCNE_DRIVEADD, SHCNE_DRIVEREMOVED, SHCNE_DRIVEADDGUI,
    SHCNE_FREESPACE, SHCNE_MEDIAINSERTED, SHCNE_MEDIAREMOVED, SHCNE_MKDIR,
    SHCNE_RMDIR, SHCNE_RENAMEFOLDER, SHCNE_UPDATEDIR, SHCNE_NETSHARE,
    SHCNE_NETUNSHARE, SHCNE_SERVERDISCONNECT, SHCNE_UPDATEIMAGE);
begin
  ShellNotifyUnregister;

  if not (csDesigning in ComponentState) and
    not (csLoading in ComponentState) then begin

    if (GetWindowsVersion = wvWinXP) then
      Pidl := GetPidlFromPath(fPath)
    else
      SHILCreatefromPath(@fPath, Pidl, Attributes);

    NotifyRecord.pidlPath := Pidl;
    NotifyRecord.bWatchSubtree := fWatch;

    Flags := 0;
    for Option := Low(Option) to High(Option) do
      if (Option in FWatchEvents) then
        Flags := Flags or NotifyFlags[Option];

    NotifyHandle := SHChangeNotifyRegister(Handle,
      SHCNF_ACCEPT_INTERRUPTS or SHCNF_ACCEPT_NON_INTERRUPTS,
      Flags, SNM_SHELLNOTIFICATION, 1, @NotifyRecord);
  end;
end;

procedure TFileDetector.ShellNotifyUnregister;
begin
  if NotifyHandle <> 0 then
    SHChangeNotifyDeregister(NotifyHandle);
end;

procedure TFileDetector.WndProc(var Message: TMessage);
type
  TPIDLLIST = record
    pidlist: PITEMIDLIST;
  end;
  PIDARRAY = ^TPIDLLIST;
var
  Path1: string;
  ptr: PIDARRAY;
  repeated: boolean;
  event: longint;
begin
  case Message.Msg of
    SNM_SHELLNOTIFICATION: begin
        event := Message.LParam and ($7FFFFFFF);
        Ptr := PIDARRAY(Message.WParam);

        Path1 := PathFromPidl(Ptr^.pidlist);

        repeated := (PrevEvent = event)
          and (uppercase(prevpath1) = uppercase(Path1));

        if Repeated then
          Exit;
        PrevEvent := Message.Msg;
        prevPath1 := Path1;

        case event of
          SHCNE_CREATE, SHCNE_UPDATEDIR: DoCreateFile(IncludeTrailingPathDelimiter(Path1));
        end; //Case event of

      end; //SNM_SHELLNOTIFICATION
  end; //case
end;

procedure Register;
begin
  RegisterComponents('OD', [TFileDetector]);
end;

end.

