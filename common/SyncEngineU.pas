unit SyncEngineU;

interface

uses
  Classes, SysUtils, DB, DBISAMTb, Contnrs, MSXML2_TLB, OdExceptions, QMConst, 
  ServerProxy, QMServerLibrary_Intf, Windows, OdIntPacking, BillPeriod, uROTypes, StateMgr;

type
  TMasterDetailDef = record
    MasterTable: string;
    MasterField: string;
    DetailTable: string;
    DetailField: string;
  end;

  TLocateAsignment = record
    LocateID: Integer;
    AssignTo: Integer;
  end;

  TProjLocateAssignment = record       //QMANTWO-616 EB Project Ticket Locates
    LocateID: Integer;
    AssignTo: Integer;
    TicketID: Integer;
    ClientID: Integer;
  end;


  TSyncError = class
  public
    TableName: string;
    PrimaryKeyID: Integer;
    ErrorText: string;

    constructor Create(const aTableName: string; const aKeyID: Integer; const aMessage: string);
  end;

  TLocalCache = class

  // ************* Local Cache Operations
  private
    FDatabase: TDBISAMDatabase;
    FOwnDatabaseObject: Boolean;
    FStatusTable: TDataSet;
    FLogEvent: TLogEvent;
    FAppVersion: string;
    FSyncResponseData: TStringList;
    FPendingAssignments: array of TLocateAsignment;
    fProjAssignments: array of TProjLocateAssignment; //EB These are re-assigning locates QMANTWO-616 Project Locates Cheater version
    procedure CreateDatabaseObject(const Directory: string);
    procedure SetDBISAMDatabase(DataSet: TDBISAMDBDataSet);
    procedure ClearChangeFlagsInTable(const TableName: string);
    procedure DeveloperWarning;
    function GetFieldNotNullCount(const TableName, FieldName: string): Integer;
    procedure RecordPendingAssignments;

    function CreateTicketAreaList(const TicketID: Integer): TicketAreaChangeList;
    function CreateTicketInfoList(const TicketID: Integer): TicketInfoChangeList;
    function CreateTicketInfoROList: TicketInfoChangeList;
    procedure ClearAreaChangedFlags;
    function CreateChangeList(const TableName, IDFieldName: string; IncludeModifiedDate: Boolean = False; const ExtraWhereClause: string = ''): NVPairListList;
    procedure LoadROChangeArray(ROArray: TROArray; const TableName, IDFieldName: string; IncludeModifiedDate: Boolean = False; const ExtraWhereClause: string = '');
    procedure AckMessages;
    function ParseResult(Report: string; OutputParams: TStrings): string;
    procedure SendEmployeeActivity;
    function CreateJobsiteArrivalChanges(const TicketID: Integer): JobsiteArrivalChangeList;
    function CreateLocateFacilityChanges(const TicketID: Integer): LocateFacilityChangeList;
    function CreateChangeListFromDataSet(Dataset: TDataSet; IncludeModifiedDate: Boolean = False): NVPairListList;
    function CreateAddinInfoChangeList(const ForeignType, ForeignID: Integer): AddinInfoChangeList;
    procedure SendBreakRuleResponses;
    function CreateTaskScheduleChanges: TaskScheduleChangeList;
    function CreateBreakRuleResponseChanges: BreakRuleResponseList;
    procedure SendTaskScheduleChanges;
    function GetAddinFileVersion: string;
    function GetFileVersion(const FileName: string): string;
    procedure SendAttachmentUploadStats(const EmpID: Integer);
    function GetAirCardPhoneNo: string;
    function GetAirCardESN: string;
    procedure AddToSyncErrors(ROSyncErrors: SyncErrorList; SyncErrors: TObjectlist);
    procedure RemoveDamageFromCache(const ID: Integer);
    procedure RemoveDamageEstimateFromCache(const ID: Integer);
    procedure GetFirstTaskReminder(EmployeeID: integer); //QM-494 First Task Reminder EB
  public
    FProfitCentersDirty: Boolean;
    function GenTempKey: Integer;
    procedure EmptyTable(const TheTableName: string);
    procedure SetActiveField(DataSet: TDataSet; ActiveValue: Boolean);
    procedure LinkEvents(DataSet: TDataset);
    procedure LinkEventsAutoInc(DataSet: TDataset);
    procedure UnLinkEvents(DataSet: TDataset);
    procedure BeforePost(DataSet: TDataSet);
    procedure BeforeDelete(DataSet: TDataSet);
    procedure NewRecord(DataSet: TDataSet);
    procedure NewRecordAutoInc(DataSet: TDataSet);
    procedure EmptyAllTables;
    procedure EmptyAndLoadTicketTables(IncludeSync: Boolean = FALSE);  //QM-730 EB Empty Ticket
    procedure EmptyAndLoadRefTables; //QM-933 EB SortBy - Adding a way to clear refernce tables
    procedure ResetSyncStatusDate; //QM-979 EB Ticket Issue with Sync
    function RunSQLReturnN(const SQL: string): Integer;
    function RunSQLReturnS(const SQL: string): string;
    function OpenLiveQuery(const SQL: string): TDataSet;
    function OpenQuery(const SQL: string; OptionalName: string = ''): TDataSet;  //QM-51 EB Modified to take a name for identifying the query
    function ExecQuery(const SQL: string): Integer;
    function ExecQueryWithSingleParam(const SQL, ParamName: string; const ParamValue: Variant; const ParamType: TFieldType): Integer;
    function OpenTableWithEditTracking(const TableName: string): TDataSet;
    function OpenWholeTable(const TableName: string): TDataSet;
    function CreateTable(const TableName: string): TDataSet;
    function ActiveRecordCount(const TableName: string): Integer;
    function TotalRecordCount(const TableName: string): Integer;
    constructor Create(const Directory: string);
    constructor CreateWithDatabase(Database: TDBISAMDatabase);
    procedure AfterCreate;
    function GetLastSyncDate: string;
    procedure GetTicketDataFromServer(TicketID: Integer);
    procedure GetEPRHistory(TicketID: Integer);
    procedure GetRiskHistory(TicketID: Integer); //QM-387 EB
    procedure GetDamageDataFromServer(DamageID: Integer);
    procedure GetWorkOrderDataFromServer(WorkOrderID: Integer);
    procedure GetWorkOrderOHMDetailsFromServer(WorkOrderID: Integer);
    procedure GetAssetsFromServer(EmpID: Integer);
    procedure GetDamageLitigationDataFromServer(LitigationID: Integer);
    procedure GetDamageThirdPartyDataFromServer(ThirdPartyID: Integer);
    procedure GetInvoiceDataFromServer(InvoiceID: Integer);
    procedure GetTicketHistoryFromServer(TicketID: Integer);
    procedure GetWorkOrderHistoryFromServer(WorkOrderID: Integer);
    procedure GetTableDataFromServer(const TableName: string; UpdatesSince: TDateTime = 0);
    procedure GetColumnDataFromServer(const TableName, KeyField, Columns: string; NonNullOnly: Boolean = True);
    procedure GetBillingAdjustmentDataFromServer(AdjustmentID: Integer);
    property Log: TLogEvent read FLogEvent write FLogEvent;
    property AppVersion: string read FAppVersion write FAppVersion;
    procedure AddToMyLog(Sender: TObject; const Msg: string);
    procedure StartBillingOnServer(Period: TBillPeriod; CenterGroupIDs: string);
    procedure CancelBillingOnServer(RunID, CallCenterGroupID: Integer);
    procedure GetMessageRecipFromServer(MessageID: Integer);
    procedure GetRightsForEmp(EmpID: Integer);
    procedure GetAttachmentList(const ForeignType, ForeignID: Integer; AttachmentDataset: TDBISAMTable; ImagesOnly: Boolean);
    function GetTicketID(const TicketNumber: string; const CallCenter: string; const TransmitDateFrom, TransmitDateTo: TDateTime; const DamageID: Integer): Integer;
    function GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
    procedure GetPrintableAttachmentList(const ForeignType, ForeignID: Integer; AttachmentDataset: TDBISAMTable);
    procedure RemoveTicketFromCache(const ID: Integer);
    procedure RemoveWorkOrderFromCache(const WorkOrderID: Integer);
    function LoadDataSetFromXml(XML: string; const TableName: string; PrimaryKeys: TStringList; EmptyTableFirst: Boolean): Integer;
    procedure GetTicketAlertsFromServer(TicketID: Integer);  //QM-771 Ticket Alerts EB


  // ************* How the client talks to the server
  private
    FServer: TServer;
    FQMService: QMService;
    FSecurityInfo: string;
    FRowsSent, FRowsReceived: Integer;
    FDeveloperMode: Boolean;
    FSentBytes: Cardinal;
    FSaveSyncXML: Boolean;
    DeviceRecord: TDeviceRec;
    function GetSyncTime(XmlDoc: IXMLDomDocument): string;
    procedure GetChangesFromServer(EmployeeID: Integer; const RequestSource: string);
    procedure ClearLocalChangeFlags;
    procedure EnsureServerSet;

    {SENDING}
    procedure SendNewTickets;
    procedure SendTicketChanges;
    procedure SendRouteOrderChanges; //QMANTWO-775 EB
    procedure SendErrorReports;
    procedure SendTimesheetEntries;

    procedure SaveGeneratedKeys(Keys: GeneratedKeyList);
    procedure UpdateDetailTable(const TableName, FieldName: string; OldKey, NewKey: Integer);
    procedure SyncTablesFromXml(XmlDoc: IXMLDomDocument);
    procedure SetLastSyncIndicator(SyncTime: string);
    procedure PopulateTableFromServerIfEmpty(const TableName: string);
    procedure PopulateMissingTableData(EmployeeID: Integer);
    procedure EmptyOpenLocatesTable;
    procedure VerifyOpenLocates(EmployeeID: Integer; OldChangesSince, NewChangesSince: string);
    function GetSecurityInfoFor(const UserName, Password, AppVersion: string): string;
    procedure ProcessSyncResponse(XMLDoc: IXMLDOMDocument);
    function GetArrayOfTicketInCache(EmployeeID: Integer): IntArray;
    function GetArrayOfDamageInCache(EmployeeID: Integer): IntArray;
    function GetArrayOfWorkOrderInCache(EmployeeID: Integer): IntArray;
    procedure UpdatePreCalculatedFields(const TableName: string);
    function AddFieldData(List: NVPairList; Field: TField; IncludeNull: Boolean = True): NVPair;
    procedure ProcessPendingAssignments;
    procedure SaveRequiredDocuments;
    function CreateNotesChangeList(const ForeignType, ForeignID: Integer): NotesChangeList5;
    procedure GetAirCardInformation;

  public
    sUQState: TUQState;
    // A few things are in here only so they can be tested.
    function TableHasPendingUpdates(const TableName: string): Boolean;
    function CreateTicketChangeList: TicketChangeList15;
    procedure PopulatePK(const TableName, TempPK, RealPK: string);
    procedure PopulateKey(const TableName: string; TempKey, RealKey: Integer);
    property Server: TServer read FServer write FServer;
    property Service: QMService read FQMService write FQMService;
    property RowsSent: Integer read FRowsSent;
    property RowsReceived: Integer read FRowsReceived;
    property SecurityInfo: string read FSecurityInfo write FSecurityInfo;
    property DeveloperMode: Boolean read FDeveloperMode write FDeveloperMode;
    property SaveSyncXML: Boolean read FSaveSyncXML write FSaveSyncXML;
    procedure Sync(EmployeeID: Integer; Errors: TObjectList; const RequestSource: string='');
    function HavePendingTicketChanges: Boolean;
    function HavePendingWorkOrderChanges: Boolean;
    procedure SendChangesToServer(const EmpID: Integer; Errors: TObjectList);
    function GetSyncStatusTable: TDataSet;
    procedure SendNewAttachments;
    procedure SendAttachmentUploads;
    procedure SendAttachmentUpdates;
    procedure AddErrorReport(EmployeeID: Integer; When: TDateTime; ErrorDescription: string; Details: string);
    function GetTableRowCount(const TableName: string): Integer;
    procedure SyncTablesFromXmlString(const Xml: string);
    destructor Destroy; override;
    procedure Login(const UserName, Password: string; var EmpID, UserID, PasswordChange: Integer;
       var ExpirationDate: TDateTime; var Role, ShortName: string);
    procedure Login2(const UserName, Password: string; var EmpID, UserID, PasswordChange: Integer;
       var ExpirationDate: TDateTime; var Role, ShortName, APIKey: string);
    function LoginAD(const UserName: string; Password: string):boolean;
    function GetSyncResponseValue(const Name: string; const Default: string = ''): string;
    procedure SetSecurityInfo(const UserName, Password: string);
    function GetReport(OutputType: string; Params: TStrings): string; overload;
    function GetReport(OutputType: string; Params: TStrings; OutputParams: TStrings): string; overload;
    procedure StartReport(OutputType: string; Params: TStrings);
    procedure AddToLog(const Msg: string);
    procedure SendBillingAdjustments;
    function CreateLocatePlatChangeList(const TicketID: Integer): LocatePlatChangeList;
    procedure SendDamage(Errors: TObjectList);
    procedure SaveAssetsForEmployee(EmpID: Integer; DataSet: TDataSet);
    procedure ReassignAsset(const AssetNum, AssetCode: string; AssetID, DestEmpID: Integer);
    procedure UpdateRestrictedUseExemption(EmployeeID: Integer; Revoke: Boolean);
    procedure UpdateMyMapAreas;
    procedure SendAreaChanges;
    procedure SendArrivalChanges(const ArrivalType: TArrivalType);
    procedure ClearChangeFlagsForKeys(KeyList: GeneratedKeyList);
    procedure ClearChangeFlagForKey(Key: GeneratedKey);
    function GetPrimaryKeyFieldForTable(const TableName: string): string;
    procedure ClearDeltaStatusInCurrentRow(DataSet: TDataSet);
    function AckTicketActivitiesForManager(ManagerID: Integer; CutoffDate: TDateTime): Integer;
    function GetUnackedTicketActivityCountForManager(ManagerID: Integer; CutoffDate: TDateTime): Integer;
    function CreateTicketGPSChanges(TicketID: Integer): GPSChangeList;
    function CreateWorkOrderChangeList: WorkOrderChangeList5;
    function CreateInspectionChangeList: WOInspectionChangeList;
    function CreateWorkOrderOHMChangeList: WorkOrderOHMChangeList;
    procedure SendWorkOrder;
    procedure SendInspection;
    procedure SendWorkOrderOHMDetails;  //QMANTWO-391 EB
    function SyncErrorsAsString(Errors: TObjectList): string;
    procedure HandleSyncErrors(Errors: TObjectList);
    procedure AddProjPendingAssignment(TicketID, ClientID, TempLocateID, AssignedToID: Integer);
    procedure SaveProjectPendingAssignments;  //QMANTWO-616 EB
    procedure SendTextToLocator(EmpID:integer;TxtMsg:String);
    procedure SendPlatHistoryDate; //PLAT CHANGE EB 6/22
    procedure SendNewCustomFormAnswers;  //QM-593 EB Custom Form (AT&T Damage screen)
    procedure SendModCustomFormAnswers;  //QM-593 EB Custom Form (AT&T Damage screen);

    procedure ClearSelLocalTables; //QM-595 EB ADD ON - for any table we want to recreate;
    procedure TicketDataClean;  //QM-986 EB  Clear Ticket Data from Cache on first sync
  end;

const
  DisableDeltaStatusTag = -999998;
  HWTimeTypeID = 20000; // Local only, not part of real ref table

implementation

uses
  Dialogs, Controls, MbnApi,
  Variants, OdIsoDates, OdNetUtils, OdMiscUtils, OdHourGlass,
  JclStrings, OdWinInet, OdDataSetToXMl, OdMSXMLUtils,
  JclSysInfo, OdWmi, ServiceLink, UQUtils, OdDBISAMUtils, OdXmlToDataSet,
  OdDbUtils, IniFiles, AttachmentUploadStatistics, ApplicationFiles,
  DMu, LocalAttachmentDMu,LocalEmployeeDMu, LocalPermissionsDmu, //QM-494 FTR
  RemObjectsUtils, ActiveX, ComObj, Forms, DateUtils;

const
  // We assume <= 1 master/detail relationship per detail table and
  // an Integer PK link between them right now
  MasterDetailDefs: array [0..0] of TMasterDetailDef = (
  (
    MasterTable: 'damage';
    MasterField: 'damage_id';
    DetailTable: 'damage_estimate';
    DetailField: 'damage_id';
  ));

procedure SetAttributesFromDataset(Element: IXMLDOMElement; IsInsert: Boolean; DS: TDataSet; const FieldsToInclude, PrimaryKey: string);
var
  i: Integer;
  FieldName: string;
begin
  for i := 0 to DS.Fields.Count - 1 do begin // each field
    if IsInsert and DS.Fields[i].IsNull then
      Continue;
    FieldName := DS.Fields[i].FieldName;
    if FieldsToInclude <> '' then begin
      if Pos(FieldName, FieldsToInclude) > 0 then
        Element.setAttribute(FieldName, XmlValue(DS.Fields[i]));
    end
    else begin
      if (FieldName <> PrimaryKey) and
        (not StringInArray(FieldName, NeverSendFieldNames)) and
        (FieldName <> '') then begin
        Element.setAttribute(FieldName, XmlValue(DS.Fields[i]));
      end;
    end;
  end;
end;

procedure SetFieldFromXml(Field: TField; Data: string);
begin
  if Data = XmlNullValue then
    Field.Clear
  else if Field.DataType = ftDate then   //QM-494 EB Correction made when recreating
    Field.AsDateTime := IsoStrToDate(Data)
  else if Field.DataType = ftDateTime then
    Field.AsDateTime := IsoStrToDateTime(Data)
  else if Field.DataType = ftBoolean then begin
    if Data = '1' then
      Field.AsBoolean := True
    else
      Field.AsBoolean := False;
  end else
    Field.AsString := Data;
end;

function UpdateDataSetFromXml(XMLDoc: IXMLDomDocument;
  DS: TDataSet; const TableName: string; PrimaryKeys: TStringList): Integer;

  function MakePkVariantArray(PrimaryKeys: TStringList; PkValues: TStringList): Variant;
  var
    i: Integer;
  begin
    Result := VarArrayCreate([0, PrimaryKeys.Count - 1], varVariant);
    for i := 0 to PrimaryKeys.Count - 1 do begin
      if ((PrimaryKeys.Objects[i] as TField).DataType = ftInteger) then
        Result[i] := StrToInt(PkValues[i])
      else if ((PrimaryKeys.Objects[i] as TField).DataType = ftDateTime) then
        Result[i] := IsoStrToDateTime(PkValues[i])
      else
        Result[i] := PkValues[i]
    end;
  end;

var
  Nodes: IXMLDOMNodeList;
  Node: IXMLDOMNode;
  PkValues: TStringList;
  PkNode: IXMLDOMNode;
  Elem, LocatorElem: IXMLDOMElement;
  i, j: Integer;
  Field: TField;
  FieldName, AttrName: string;
  LocatorIDString: string;
begin
  Result := 0;
  Assert(Assigned(PrimaryKeys));
  if PrimaryKeys.Count < 1 then
    Exit;

  for j := 0 to PrimaryKeys.Count - 1 do
    PrimaryKeys.Objects[j] := DS.FindField(PrimaryKeys[j]);

  XMLDoc.preserveWhiteSpace := True; // to prevent trimming of leading/trailing spaces
  PkValues := TStringList.Create;
  try
    Nodes := XMLDoc.SelectNodes('//' + TableName);
    for i := 0 to Nodes.Length - 1 do begin
      Elem := Nodes[i] as IXMLDOMElement;

      PkValues.Clear;
      for j := 0 to PrimaryKeys.Count - 1 do begin
        PkNode := Elem.attributes.getNamedItem(PrimaryKeys[j]);
        if Assigned(PkNode) then
          PkValues.Add(PkNode.text)
        else
          PkValues.Add('');
      end;
      if AnyStringsAreBlank(PkValues) then begin
        if TableName='assignment' then
          Continue   // a special case
        else
          raise Exception.CreateFmt('Missing portion of PK while updating table %s, PK=%s',
            [TableName, PkValues.CommaText]);
      end;

      if AllObjectsAreAssigned(PrimaryKeys) then begin
        if DS.Locate(StringsToStr(PrimaryKeys, ';', True), MakePkVariantArray(PrimaryKeys, PkValues), []) then
          DS.Edit
        else
          DS.Append;
      end
      else
        raise Exception.CreateFmt('Primary key fields %s for table %s do not exist', [PrimaryKeys.Text, TableName]);

      // Don't update the local attachment data row if there are pending changes
      if (TableName = 'attachment') and (not DS.FieldByName('DeltaStatus').IsNull) then begin
        DS.Cancel;
        Continue;
      end;

      for j := 0 to Elem.attributes.length - 1 do begin
        AttrName := Elem.attributes[j].nodeName;


       
        Field := DS.FindField(AttrName);
        if Field <> nil then begin
          SetFieldFromXml(Field, Elem.attributes[j].text);
          {Capture fields as they come down from server: localDataDef}
//           if (TableName = 'ticket_ack') then
//            ShowMessage(AttrName + ':' + Elem.attributes[j].text);
        end;
      end;

      for j := 0 to Nodes[i].ChildNodes.Length - 1 do begin
        Node := Nodes[i].ChildNodes[j];
        FieldName := Nodes[i].ChildNodes[j].NodeName;
        if (FieldName <> 'locate') and (FieldName <> 'assignment') then begin
          Field := DS.FindField(FieldName);
          if Field <> nil then
            SetFieldFromXml(Field, Elem.attributes[j].text);
        end;

        if (TableName = 'locate') and (FieldName = 'assignment') then begin
          LocatorElem := Node as IXMLDOMElement;
          LocatorIDString := LocatorElem.attributes[0].text;
          DS.FieldByName('locator_id').AsString := LocatorIDString;
        end;
      end;

      // Default active to True if needed
      Field := DS.FindField('active');
      if Field <> nil then
        if Field.IsNull then
          Field.AsBoolean := True;

      DS.FieldByName('DeltaStatus').Clear;
      try
        DS.Post;
      except
        on E:Exception do begin
          E.Message := E.Message +
           Format(' while updating table %s, PK Fields=%s values=%s', [TableName, PrimaryKeys.CommaText, PkValues.CommaText]);
          raise;
        end;
      end;

      if not DS.FieldByName('DeltaStatus').IsNull then
        raise Exception.Create('DeltaStatus should be null');

      Result := Result + 1;
    end;
  finally
    FreeAndNil(PkValues);
  end;

  DS.First;
end;

{ TLocalCache }

constructor TLocalCache.Create(const Directory: string);
begin
  inherited Create;
  CreateDatabaseObject(Directory);
  AfterCreate;
end;

constructor TLocalCache.CreateWithDatabase(Database: TDBISAMDatabase);
begin
  inherited Create;
  FDatabase := Database;
  AfterCreate;
end;

procedure TLocalCache.CreateDatabaseObject(const Directory: string);
begin
  FDatabase := TDBISAMDatabase.Create(nil);
  FOwnDatabaseObject := True;
  FDatabase.DatabaseName := Directory;
  FDatabase.Directory := Directory;
  FDatabase.Open;
end;

destructor TLocalCache.Destroy;
begin
  if FOwnDatabaseObject then
    FreeAndNil(FDatabase);

  FreeAndNil(FStatusTable);
  FreeAndNil(FSyncResponseData);
  inherited;
end;

procedure TLocalCache.EmptyAllTables;
var
  TableNames: TStringList;
  i: Integer;
  SyncTable: TDBISAMQuery;
begin
  AddToLog('Emptying local data');
  Assert(Assigned(FDatabase));
  FDatabase.Open;
  TableNames := TStringList.Create;
  try
    FDatabase.Handle.ListTableNames(TableNames);
    for i := 0 to TableNames.Count - 1 do begin
      if TableNames[i] <> 'syncstatus' then
        EmptyTable(TableNames[i]);
    end;
  finally
    FreeAndNil(TableNames);
  end;

  if FDatabase.Handle.DataTableExists('syncstatus', False, False) then begin
    SyncTable := GetSyncStatusTable as TDBISAMQuery;
    Assert(SyncTable.ResultIsLive);
    SyncTable.First;
    while not SyncTable.Eof do begin
      SyncTable.Edit;
      SyncTable.FieldByName('LastSyncDown').Clear;
      SyncTable.Post;
      SyncTable.Next;
    end;
    SyncTable.FlushBuffers;
  end;
end;

procedure TLocalCache.EmptyAndLoadRefTables;
var
  TableNames: TStringList;
  i: Integer;
  t, s: string;
  SyncTable: TDBISAMQuery;
begin
  AddToLog('Emptying reference data');
  Assert(Assigned(FDatabase));
  FDatabase.Open;
  TableNames := TStringList.Create;
  try
    FDatabase.Handle.ListTableNames(TableNames);
    for i := 0 to TableNames.Count - 1 do begin
      t := TableNames[i];
      if (t = 'reference')
      ///or (t = 'ticket_ack') or
        then
        EmptyTable(TableNames[i]);
    end;

        {Clear SyncStatus}
    if FDatabase.Handle.DataTableExists('syncstatus', False, False) then begin
      SyncTable := GetSyncStatusTable as TDBISAMQuery;
      Assert(SyncTable.ResultIsLive);
      SyncTable.First;
      while not SyncTable.Eof do begin
        s := SyncTable.FieldByName('TableName').AsString;
        if (s = 'reference') then begin
           SyncTable.Open;   //so it does not try and close it
           SyncTable.Edit;
           SyncTable.FieldByName('LastSyncDown').Clear;
           SyncTable.Post;
        end;
        SyncTable.Next;
      end;
    SyncTable.FlushBuffers;
    DM.SyncNow;
    end;
  finally
    FreeAndNil(TableNames);
  end;
end;

procedure TLocalCache.EmptyAndLoadTicketTables(IncludeSync: Boolean = FALSE);
var
  TableNames: TStringList;
  i: Integer;
  t, s: string;
  SyncTable: TDBISAMQuery;
begin

  Assert(Assigned(FDatabase));
  FDatabase.Open;
  TableNames := TStringList.Create;
  try
    FDatabase.Handle.ListTableNames(TableNames);
    for i := 0 to TableNames.Count - 1 do begin
      t := TableNames[i];
      if (t = 'ticket') or
         (t = 'ticket_ack') or
         (t = 'ticket_info') or
         (t = 'ticket_risk') or
         (t = 'locate') or
         (t = 'locate_facility') or
         (t = 'locate_hours') or
         (t = 'locate_plat') or
         (t = 'gps_position') or    //QM-799 EB - Adding this to clear when tickets are cleared
         (t = 'locate_status') then
        EmptyTable(TableNames[i]);
    end;

    {Clear SyncStatus}
    if FDatabase.Handle.DataTableExists('syncstatus', False, False) then begin
      SyncTable := GetSyncStatusTable as TDBISAMQuery;
      Assert(SyncTable.ResultIsLive);
      SyncTable.First;
      while not SyncTable.Eof do begin
        s := SyncTable.FieldByName('TableName').AsString;
        if (s = 'ticket') or
           (s = 'ticket_ack') or
           (s = 'ticket_info') or
           (s = 'ticket_risk') or
           (s = 'locate') or
           (s = 'locate_facility') or
           (s = 'locate_hours') or
           (s = 'locate_plat') or
           (s = 'gps_position') or    //QM-799 EB - Adding this to clear when tickets are cleared
           (s = 'locate_status') then begin
           SyncTable.Edit;
           SyncTable.FieldByName('LastSyncDown').Clear;
           SyncTable.Post;
        end;
        SyncTable.Next;
      end;
      AddToLog('-> Ticket Data has been reloaded');
    SyncTable.FlushBuffers;
    if IncludeSync then   {Only set to true if called outside a full sync}
      DM.SyncNow;
    end;
  finally
    FreeAndNil(TableNames);
  end;
end;

procedure TLocalCache.EmptyTable(const TheTableName: string);
var
  Query: TDBISAMQuery;
  Table: TDBISAMTable;
begin
  if FDatabase.InTransaction then begin
    // Work around a DBISAM 4.30 issue where EmptyTable auto-commits transactions
    Query := TDBISAMQuery.Create(nil);
    try
      SetDBISAMDatabase(Query);
      Query.SQL.Text := 'delete from ' + TheTableName;
      Query.ExecSQL;
    finally
      FreeAndNil(Query);
    end;
  end else begin // No transaction
    Table := TDBISAMTable.Create(nil);
    try
      SetDBISAMDatabase(Table);
      Table.TableName := TheTableName;
      Table.Exclusive := True;
      Table.EmptyTable;
    finally
      FreeAndNil(Table);
    end;
  end;
end;



procedure TLocalCache.ResetSyncStatusDate;      //QM-979 Research Sync Status
var
 Query: TDBISAMQuery;
begin
  Query := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(Query);
    Query.SQL.Text := 'update SyncStatus set LastSyncDown = NULL';
    Query.ExecSQL;

  finally
    FreeAndNil(Query);
  end;
end;




function TLocalCache.GetLastSyncDate: string;
var
  Field: TField;
begin
  with GetSyncStatusTable do begin
    First;
    Field := FieldByName('LastSyncDown');
    while not EOF do begin
      if Field.AsString > Result then
        Result := Field.AsString;
      Next;
    end;
  end;
end;

function TLocalCache.GetSyncTime(XmlDoc: IXMLDomDocument): string;
var
  Node: IXMLDOMNode;
begin
  Assert(Assigned(XMLDoc));
  Node := XMLDoc.selectSingleNode('/ROOT/row/@SyncDateTime');
  if Assigned(Node) then
    Result := Node.text
  else
    raise Exception.Create('XML packet is missing sync datetime');
end;

function TLocalCache.OpenLiveQuery(const SQL: string): TDataSet;
var
  DataSet: TDBISAMQuery;
begin
  DataSet := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(DataSet);
    DataSet.SQL.Text := SQL;
    DataSet.RequestLive := True; // needed, otherwise it does not work.
    DataSet.Open;
    if not Dataset.ResultIsLive then
      raise Exception.Create('Expected live result set');
    Result := DataSet;
  except
    FreeAndNil(DataSet);
    raise;
  end;
end;

function TLocalCache.OpenQuery(const SQL: string; OptionalName: string): TDataSet;
var
  DataSet: TDBISAMQuery;
begin
  DataSet := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(DataSet);
    DataSet.Name := OptionalName;  //QM-51 EB Modified to take a name for identifying the query
    DataSet.SQL.Text := SQL;
    DataSet.RequestLive := False;
    DataSet.Open;
    Result := DataSet;
  except
    FreeAndNil(DataSet);
    raise;
  end;
end;

function TLocalCache.OpenTableWithEditTracking(const TableName: string): TDataSet;
begin
  Result := OpenLiveQuery('SELECT * FROM "' + TableName + '" WHERE ACTIVE=True');
  LinkEvents(Result);
end;

function TLocalCache.OpenWholeTable(const TableName: string): TDataSet;
begin
  Result := CreateTable(TableName);
  try
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TLocalCache.ActiveRecordCount(const TableName: string): Integer;
var
  DataSet: TDataSet;
begin
  DataSet := OpenTableWithEditTracking(TableName);
  try
    Result := DataSet.RecordCount;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TLocalCache.TotalRecordCount(const TableName: string): Integer;
var
  DataSet: TDataSet;
begin
  DataSet := OpenWholeTable(TableName);
  try
    Result := DataSet.RecordCount;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TLocalCache.GetChangesFromServer(EmployeeID: Integer; const RequestSource: string);
var
  XMLDoc: IXMLDomDocument;
  LastSyncDateTime: string;
  ThisSyncDateTime: string;
  RawResult: string;
  MyTickets: string;
  MyDamages: string;
  MyWorkOrders: string;
  CompInfo: ComputerInformation;
  AddInfo: NVPairList;
  RawXMLFile: string;
//  TempWrite: TSTringlist;
begin
  //TempWrite := TStringList.Create;
  EnsureServerSet;
  AddToLog('Getting changes from server');
    //  GetAirCardInformation;  Commenting this out so it won't try to merge it back in.
  AddInfo := nil;
  CompInfo := ComputerInformation.Create;
  try
    try
      DeviceRecord.AirCardDeviceID := '';
      GetAirCardInformation;
    except
      on E: Exception do begin
        AddToLog('Aircard not detected: ' + E.Message);
        DeviceRecord.AirCardDeviceID := 'No Mobile Information Available';
      end;
    end;
    try
      CompInfo.WindowsUser := GetLocalUserName;
      CompInfo.OsPlatform := Win32Platform;
      CompInfo.OsMajorVersion := Win32MajorVersion;
      CompInfo.OsMinorVersion := Win32MinorVersion;
      CompInfo.OsServicePackVersion := GetWindowsServicePackVersion;
      CompInfo.ComputerName := GetLocalComputerName;
//      TempWrite.Add('Subscriber: ' + DeviceRecord.Subscriber);
//      TempWrite.Add('AirCard ID: ' + DeviceRecord.AirCardDeviceID);
//      TempWrite.Add('AirCard Brand: ' + DeviceRecord.AirCardBrand);
//      TempWrite.Add('AirCard Phone: ' + DeviceRecord.AirCardPhone);
//      TempWrite.Add('SimCard ID: ' + DeviceRecord.SimCardDeviceID);

//      TempWrite.SaveToFile(ExtractFilePath(Application.ExeName) + 'TempAirSimData.txt');
      {
      The  CompInfo.PlatDate was removed until all Users client are updated past 20674   //QM-23 sr  1/30/20
      CompInfo.PlatDate := GetFileDate(PLAT_FILE_PATH);
      @Platdate must be added back to record_computer_info stored proc
      }
      if Win32Platform = VER_PLATFORM_WIN32_NT then begin
        CompInfo.HotFixList := Copy(GetInstalledHotFixList, 1, 2000);
        CompInfo.DellServiceTag := Copy(GetDellServiceTag, 1, 100);
      end else begin
        CompInfo.HotFixList := '-';
        CompInfo.DellServiceTag := '-';
      end;
    except
      on E: Exception do begin
        AddToLog('ERROR gathering system configuration data:');
        AddToLog(E.Message);
      end;
    end;
    AddInfo := NVPairList.Create;
    AddParam(AddInfo, 'ComputerSerial', GetLocalComputerSerial);
    AddParam(AddInfo, 'DomainLogin', GetDomainName);
    AddParam(AddInfo, 'ComputerModel', GetLocalComputerModel);
    AddParam(AddInfo, 'AddinVersion', GetAddinFileVersion);
    AddParam(AddInfo, 'AssetTag', GetAssetTag);
    AddParam(AddInfo, 'AirCardPhoneNo', GetAirCardPhoneNo);
    AddParam(AddInfo, 'AirCardESN', GetAirCardESN);
    AddParam(AddInfo, 'PersequorVersion', GetFileVersion(AddSlash(GetProgramFilesFolder) + PersequorProgramFileName));   
    AddParam(AddInfo, 'MemeVersion',  GetFileVersion(AddSlash(GetProgramFilesFolder) + MemeProgramFileName));   //qm-972 sr

    if NotEmpty(RequestSource) then
      AddParam(AddInfo, 'SyncSource', RequestSource);

    LastSyncDateTime := GetLastSyncDate;
    if DeveloperMode then
      AddToLog('Getting changes since: ' + LastSyncDateTime);

    MyTickets := PackIntArray(GetArrayOfTicketInCache(EmployeeID));
    MyDamages := PackIntArray(GetArrayOfDamageInCache(EmployeeID));
    MyWorkOrders := PackIntArray(GetArrayOfWorkOrderInCache(EmployeeID));
    GetChan.OperationName := 'SyncDown6';
    RawResult := Service.SyncDown6(SecurityInfo, LastSyncDateTime, MakeGUIDString,
      AppVersion, Now, GetUTCBias, MyTickets, CompInfo, FSentBytes, AddInfo, MyDamages, MyWorkOrders);
    if SaveSyncXML then begin
      RawXMLFile := BuildFileName(GetDesktopFolder, 'QMSync-xml-' + FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.txt');
      SaveDebugFile(RawXMLFile, RawResult);
    end;
  finally
    FreeAndNil(CompInfo);
    FreeAndNil(AddInfo);
//    FreeAndNil(TempWrite);
  end;

  XMLDoc := CoDOMDocument.Create;
  XMLDoc.Async := False;
  if not XMLDoc.loadXML(RawResult) then begin
    RawXMLFile := BuildFileName(GetDesktopFolder, 'QMSync-bad-xml-' + FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.txt');
    SaveDebugFile(RawXMLFile, RawResult);
    raise Exception.CreateFmt('Invalid XML document from QML SyncDown: %s' + #13#10 +
      'Refer to %s for the complete XML.', [Copy(RawResult, 1, 100), RawXMLFile]);
  end;

  ThisSyncDateTime := GetSyncTime(XMLDoc);
  Assert(ThisSyncDateTime <> '');
  EmptyOpenLocatesTable;
  SyncTablesFromXml(XMLDoc);
  SetLastSyncIndicator(ThisSyncDateTime);
  VerifyOpenLocates(EmployeeID, LastSyncDateTime, ThisSyncDateTime);
  ProcessSyncResponse(XMLDoc);
  GetFirstTaskReminder(EmployeeID);   //QM-494 First Task Reminder EB
end;

procedure TLocalCache.SendChangesToServer(const EmpID: Integer; Errors: TObjectList);
begin
  Assert(Assigned(Errors), 'Errors is not defined');
  Errors.Clear;
//  DeveloperWarning; {EB - From early days with Oasis - Dev warnings}
  FDatabase.StartTransaction;
  try
    ResetOdHttpSentBytes; 
    RecordPendingAssignments;
    SendNewTickets;
    SendTicketChanges;
    SendRouteOrderChanges;   //QMANTWO-775 EB
    SaveProjectPendingAssignments;  //QMANTWO-616 EB Project Tickets
    ProcessPendingAssignments;
    SendTaskScheduleChanges;
    SendDamage(Errors);
    SendTimesheetEntries;
    SendNewAttachments;
    SendAttachmentUploads;
    SendAttachmentUpdates;
    SendBillingAdjustments;
    SendErrorReports;
    AckMessages;
    SendEmployeeActivity;
    SendBreakRuleResponses;
    SendAttachmentUploadStats(EmpID);
    SendWorkOrder;
    SendInspection;
    SendWorkOrderOHMDetails;
    SendPlatHistoryDate;

    SendNewCustomFormAnswers;  //QM-593 EB Custom Form (AT&T Damages)
    SendModCustomFormAnswers;
//    SendTextMsg;   QM-9   sr
    // Done sending all local changes
    FSentBytes := GetOdHttpSentBytes; // Always 0 for SERVER_IN_CLIENT mode
    if Errors.Count > 0 then
      AddToLog(SyncErrorsAsString(Errors));

    ClearLocalChangeFlags;
//    ClearSelLocalTables;   {This might be getting cleared too often}
    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;



procedure TLocalCache.SendNewCustomFormAnswers;   //QM-593 EB Custom Form (AT&T Damage)
const
  CFSQL = 'Select * from custom_form_answer where answer_id < 0';
var
 {AnswerData = TicketData}
  DS: TDataSet;
  AnswerField: TField;
  AnswerRec: CustomForrmAnswer;
  AnswerRecList: CustomFormAnswerList;
  ReturnedKeys: GeneratedKeyList;
  i: integer;
begin
  AnswerRec := nil;
  ReturnedKeys := nil;
  try
//    ReturnedKeys := GeneratedKeyList.Create;    //9TRY
    AnswerRecList := CustomFormAnswerList.Create;
    DS := OpenQuery(CFSQL);
    if DS.RecordCount > 0 then begin
        while not DS.EOF do begin
          AnswerRec := AnswerRecList.Add;
          for i := 0 to DS.FieldCount - 1 do begin
            AnswerField := DS.Fields[i];
            if not StringInArray(AnswerField.FieldName, NeverSendFieldNames) then
              AddFieldData(AnswerRec.AnswerData, AnswerField);
          end;
          DS.Next;
        end;
        AddToLog('Saving Custom Form Data');
        GetChan.OperationName := 'SaveNewCustomFormData';
        ReturnedKeys := Service.SaveNewCustomFormData(SecurityInfo, AnswerRecList);   //9TRY
        SaveGeneratedKeys(ReturnedKeys);
    end;
  finally
    FreeAndNil(DS);
    FreeAndNil(AnswerRecList);
    FreeAndNil(ReturnedKeys);
  end;
end;

procedure TLocalCache.SendModCustomFormAnswers;
const
 CFSQL = 'Select * from custom_form_answer where DeltaStatus=''U''';
var
 {AnswerData = TicketData}
  DS: TDataSet;
  AnswerField: TField;
  AnswerRec: CustomForrmAnswer;
  AnswerRecList: CustomFormAnswerList;
  i: integer;
begin
  AnswerRec := nil;
  DS := OpenQuery(CFSQL);
  if DS.RecordCount > 0 then begin
    AnswerRecList := CustomFormAnswerList.Create;
    try
      while not DS.EOF do begin
        AnswerRec := AnswerRecList.Add;
        for i := 0 to DS.FieldCount - 1 do begin
          AnswerField := DS.Fields[i];
          if not StringInArray(AnswerField.FieldName, NeverSendFieldNames) then
            AddFieldData(AnswerRec.AnswerData, AnswerField);
        end;
        DS.Next;
      end;
      AddToLog('Saving Custom Form Data');
      GetChan.OperationName := 'SaveModCustomFormData';
      Service.SaveModCustomFormData(SecurityInfo, AnswerRecList);

    finally
      FreeAndNil(DS);
      FreeAndNil(AnswerRecList);
    end;
  end;

end;

function TLocalCache.CreateWorkOrderChangeList: WorkOrderChangeList5;
var
  WOList: WorkOrderChangeList5;
  WorkOrderDS: TDataSet;
  WorkOrderID: Integer;
  WO: WorkOrderChange5;
  ExtraWhere: string;
begin
  WOList := WorkOrderChangeList5.Create;

  WorkOrderDS := OpenQuery('Select work_order.wo_id, work_description, work_cross, ' +
    ' state_hwy_row, road_bore_count, driveway_bore_count, assigned_to_id, ' +
    ' status, closed, status_date, statused_how, statused_by_id ' +
    ' from work_order ' +
    ' left join work_order_inspection ins on work_order.wo_id = ins.wo_id' +
    ' where work_order.DeltaStatus=''U'' and ins.wo_id is null');
  try
    while not WorkOrderDS.Eof do begin
      WorkOrderID := WorkOrderDS.FieldByName('wo_id').AsInteger;
      WO:= WOList.Add;
      WO.WorkOrderID := WorkOrderID;
      WO.WorkDescription := WorkOrderDS.FieldByName('work_description').AsString;
      WO.WorkCross := WorkOrderDS.FieldByName('work_cross').AsString;
      WO.StateHwyROW := WorkOrderDS.FieldByName('state_hwy_row').AsString;
      WO.RoadBoreCount := WorkOrderDS.FieldByName('road_bore_count').AsInteger;
      WO.DrivewayBoreCount := WorkOrderDS.FieldByName('driveway_bore_count').AsInteger;
      WO.AssignedToID := WorkOrderDS.FieldByName('assigned_to_id').AsInteger;
      WO.Status := WorkOrderDS.FieldByName('status').AsString;
      WO.Closed := WorkOrderDS.FieldByName('closed').AsBoolean;
      WO.StatusDate := WorkOrderDS.FieldByName('status_date').AsDateTime;
      WO.StatusedHow := WorkOrderDS.FieldByName('statused_how').AsString;
      WO.StatusedByID := WorkOrderDS.FieldByName('statused_by_id').AsInteger;

      ExtraWhere := 'foreign_type = ' + IntToStr(qmftWorkOrder) +
        ' and foreign_id = ' + IntToStr(WorkOrderID);

      WO.NotesChanges := NotesChangeList5.Create;
      LoadROChangeArray(WO.NotesChanges, 'notes', 'notes_id', False, ExtraWhere);

      WO.AddinInfoChanges := AddinInfoChangeList.Create;
      LoadROChangeArray(WO.AddinInfoChanges, 'addin_info', 'addin_info_id', False, ExtraWhere);

      WorkOrderDS.Next;
    end;
  finally
    FreeAndNil(WorkOrderDS);
  end;

  Result := WOList;
end;



{EB - New Note Subtype requires calls to new server calls: most are appended with "5"}
procedure TLocalCache.SendWorkOrder;
var
  WOList: WorkOrderChangeList5;
  ReturnedKeys: GeneratedKeyList;
begin
  WOList := CreateWorkOrderChangeList;
  try
    if WOList.Count > 0 then begin
      AddToLog(Format('Sending %d modified %s to server', [WOList.Count, AddSIfNot1(WoList.Count, 'work order')]));
      GetChan.OperationName := 'StatusWorkOrders5';
      Service.StatusWorkOrders5(SecurityInfo, WOList, ReturnedKeys);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [WOList.Count,
        AddSIfNot1(WOList.Count, 'work order')]));
    end;
  finally
    FreeAndNil(WOList);
  end;
end;


function TLocalCache.CreateInspectionChangeList: WOInspectionChangeList;
var
  WorkOrderDS: TDataSet;
  WorkOrderID: Integer;
  WO: WOInspectionChange;
  ExtraWhere: string;
begin
  Result := WOInspectionChangeList.Create;

  WorkOrderDS := OpenQuery('Select work_order.wo_id, assigned_to_id, ' +
    ' status, closed, status_date, statused_how, statused_by_id, ins.* ' +
    ' from work_order ' +
    ' inner join work_order_inspection ins on work_order.wo_id = ins.wo_id' +
    ' where work_order.DeltaStatus=''U''');
  try
    while not WorkOrderDS.Eof do begin
      WorkOrderID := WorkOrderDS.FieldByName('wo_id').AsInteger;
      WO := Result.Add;
      WO.WorkOrderID := WorkOrderID;
      WO.AssignedToID := WorkOrderDS.FieldByName('assigned_to_id').AsInteger;
      WO.Status := WorkOrderDS.FieldByName('status').AsString;
      WO.Closed := WorkOrderDS.FieldByName('closed').AsBoolean;
      WO.StatusDate := WorkOrderDS.FieldByName('status_date').AsDateTime;
      WO.StatusedHow := WorkOrderDS.FieldByName('statused_how').AsString;
      WO.StatusedByID := WorkOrderDS.FieldByName('statused_by_id').AsInteger;

      WO.ActualMeterNumber := WorkOrderDS.FieldByName('actual_meter_number').AsString;
      WO.ActualMeterLocation := WorkOrderDS.FieldByName('actual_meter_location').AsString;
      WO.BuildingType := WorkOrderDS.FieldByName('building_type').AsString;
      WO.CGAVisits := WorkOrderDS.FieldByName('cga_visits').AsInteger;
      WO.MapStatus := WorkOrderDS.FieldByName('map_status').AsString;
      WO.MercuryRegulator := WorkOrderDS.FieldByName('mercury_regulator').AsString;
      WO.VentClearanceDist := WorkOrderDS.FieldByName('vent_clearance_dist').AsString;
      WO.VentIgnitionDist := WorkOrderDS.FieldByName('vent_ignition_dist').AsString;
      WO.AlertFirstName := WorkOrderDS.FieldByName('alert_first_name').AsString;
      WO.AlertLastName := WorkOrderDS.FieldByName('alert_last_name').AsString;
      WO.AlertOrderNumber := WorkOrderDS.FieldByName('alert_order_number').AsString;
      WO.CGAReason := WorkOrderDS.FieldByName('cga_reason').AsString;
      WO.GasLight := WorkOrderDS.FieldByName('gas_light').AsString;

      ExtraWhere := 'wo_id = ' + IntToStr(WorkOrderID);
      WO.RemedyChanges := WorkRemedyChangeList.Create;
      LoadROChangeArray(WO.RemedyChanges, 'work_order_remedy', 'wo_remedy_id', False, ExtraWhere);

      ExtraWhere := 'foreign_type = ' + IntToStr(qmftWorkOrder) + ' and foreign_id = ' + IntToStr(WorkOrderID);
      WO.NotesChanges := NotesChangeList.Create;
      LoadROChangeArray(WO.NotesChanges, 'notes', 'notes_id', False, ExtraWhere);
      WO.AddinInfoChanges := AddinInfoChangeList.Create;
      LoadROChangeArray(WO.AddinInfoChanges, 'addin_info', 'addin_info_id', False, ExtraWhere);

      WorkOrderDS.Next;
    end;
  finally
    FreeAndNil(WorkOrderDS);
  end;
end;

procedure TLocalCache.SendInspection;
var
  ChangeList: WOInspectionChangeList;
  ReturnedKeys: GeneratedKeyList;
begin
  ChangeList := CreateInspectionChangeList;
  try
    if ChangeList.Count > 0 then begin
      AddToLog(Format('Sending %d modified %s to server', [ChangeList.Count, AddSIfNot1(ChangeList.Count, 'inspection')]));
      GetChan.OperationName := 'StatusWorkOrderInspections';
      Service.StatusWorkOrderInspections(SecurityInfo, ChangeList, ReturnedKeys);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'inspection')]));
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;



function TLocalCache.CreateWorkOrderOHMChangeList: WorkOrderOHMChangeList;  //QMANTWO-391 EB
var
 OHMDetailsDS: TDataset;
 OHMChange: WorkOrderOHMChange;
begin
  Result := WorkOrderOHMChangeList.Create;
    OHMDetailsDS := OpenQuery('Select wo_id, wo_dtl_id, wo_number, ' +
    {Currently, these fields are not updated, but they are listed here for reference }
   // ' OSID, due_date, curb_b, meter_1, slpi_p, slp_1, slp_2, slin_s, sl_pi, ' +
   // ' slp_3, slpre, main_p, mai_1, main_r, meter_2, has_c, nominaldia, ' +
   // ' coatingtyp, installedd, measuredle, material, ' +
    {Updated and/or used field}
     'curbvaluefound, modified_date, statused_by_id ' +
    ' from work_order_ohm_details where DeltaStatus=''U''');
  try
    OHMDetailsDS.First;
    while not OHMDetailsDS.Eof do begin
      OHMChange := Result.Add;
      OHMChange.WorkOrderID := OHMDetailsDS.FieldByName('wo_id').asInteger;
      AddFieldData(OHMChange.OHMDetailsData, OHMDetailsDS.FieldByName('curbvaluefound'));
      OHMDetailsDS.Next;
    end;
  finally
    FreeAndNil(OHMDetailsDS);
  end;
end;

procedure TLocalCache.SendWorkOrderOHMDetails;  //QMANTWO-391
var
  ChangeList: WorkOrderOHMChangeList;
begin
  ChangeList := CreateWorkOrderOHMChangeList;
  if ChangeList.Count > 0 then begin
    AddToLog(Format('Sending %d modified %s to server', [ChangeList.Count, AddSIfNot1(ChangeList.Count, 'WO OHM Details')]));
    GetChan.OperationName := 'SaveWorkOrdersOHM';
    Service.SaveWorkOrdersOHM(SecurityInfo, ChangeList);
    AddToLog(Format('Changes to %d %s sent', [ChangeList.Count, AddSIfNot1(ChangeList.Count, 'WO OHM Details')]))
  end;
end;


function TLocalCache.SyncErrorsAsString(Errors: TObjectList): string;
var
  I: Integer;
begin
  Assert(Assigned(Errors), 'Errors is not defined');
  Result := '';
  for I := 0 to Errors.Count - 1 do begin
    if NotEmpty(Result) then
      Result := Result + CRLF;
    Result := Result + (Errors[I] as TSyncError).ErrorText;
  end;
end;

procedure TLocalCache.Sync(EmployeeID: Integer; Errors: TObjectList; const RequestSource: string);
var
  StartTime: TDateTime;
  Cursor: IInterface;
begin
  EnsureServerSet;
  Cursor := ShowHourGlass;

  StartTime := Now;
  AddToLog('Sync Starting');

  FRowsReceived := 0;

  SendChangesToServer(EmployeeID, Errors);
  ClearSelLocalTables; {EB Moved here to make sure it is only cleared full sync}
  If DM.CheckInternet then
    TicketDataClean;
  Sleep(100);
//  Sleep(DM.UQState.DeveloperSyncWait1);  //QM-614 EB
  GetChangesFromServer(EmployeeID, RequestSource);
  AddToLog(Format('Request Packet Size: %d', [Length(GetChan.MostRecentRawRequest)]));
  AddToLog(Format('Response Packet Size: %d', [Length(GetChan.MostRecentRawResponse)]));
  AddToLog(Format('Changes Received: %d', [FRowsReceived]));

  PopulateMissingTableData(EmployeeID);

  AddToLog(Format('Sync Complete, Elapsed time = %.2f seconds',
    [(Now - StartTime) * 24 * 60 * 60]));
end;

procedure TLocalCache.SyncTablesFromXml(XmlDoc: IXMLDomDocument);
var
  TableName, PK: string;
  NumChanges: Integer;
  DataSet: TDataSet;
  PrimaryKeys: TStringList;
begin
  FDatabase.StartTransaction;
  try
    GetSyncStatusTable.First;
    while not GetSyncStatusTable.EOF do begin
      TableName := GetSyncStatusTable.FieldByName('TableName').AsString;
      PK := GetSyncStatusTable.FieldByName('PrimaryKey').AsString;
      DataSet := OpenWholeTable(TableName);
      PrimaryKeys := TStringList.Create;
      try
        StrToStrings(PK, ';', PrimaryKeys, False);
          
        NumChanges := UpdateDataSetFromXml(XMLDoc, DataSet, TableName, PrimaryKeys);

        if NumChanges > 0 then
          UpdatePreCalculatedFields(TableName);
        FRowsReceived := FRowsReceived + NumChanges;
      finally
        FreeAndNil(DataSet);
        FreeAndNil(PrimaryKeys);
      end;
      GetSyncStatusTable.Next;
    end;

    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;

procedure TLocalCache.SetLastSyncIndicator(SyncTime: string);
begin
  AddToLog('Processed changes through: ' + SyncTime);

  GetSyncStatusTable.First;
  while not GetSyncStatusTable.EOF do begin
    GetSyncStatusTable.Edit;
    GetSyncStatusTable['LastSyncDown'] := SyncTime;
    GetSyncStatusTable.Post;
    GetSyncStatusTable.Next;
  end;
end;

procedure TLocalCache.SyncTablesFromXmlString(const Xml: string);
var
  XmlDoc: IXMLDOMDocument;
begin
  if Xml = '' then
    Exit;
  XMLDoc := CoDOMDocument.Create;
  XMLDoc.async := False;
  if not XMLDoc.loadXML(Xml) then
    raise Exception.Create('Invalid XML document in SyncTablesFromXml');
  SyncTablesFromXml(XMLDoc);
end;

function ExtractTableName(const CombinedID: string): string;
var
  P: Integer;
begin
  P := StrLastPos('_', CombinedID);
  Assert(P > 0);
  Result := Copy(CombinedID, 1, P - 1);
end;

function ExtractValue(const CombinedID: string): string;
var
  P: Integer;
begin
  P := StrLastPos('_', CombinedID);
  Assert(P > 0);
  Result := Copy(CombinedID, P + 1, 999);
end;

procedure TLocalCache.PopulatePK(const TableName, TempPK, RealPK: string);
var
  DS: TDataSet;
  PrimaryKeyFieldName: string;
begin
  if TempPK = '' then
    raise Exception.Create('Temp PK was not assigned');

  GetSyncStatusTable;

  if not GetSyncStatusTable.Locate('TableName', TableName, []) then
    raise Exception.Create('There is no table ' + TableName + ' in table list');

  PrimaryKeyFieldName := GetSyncStatusTable.FieldByName('PrimaryKey').AsString;

  DS := OpenLiveQuery('SELECT * FROM "' + TableName + '"');
  try
    if not DS.Locate(PrimaryKeyFieldName, TempPK, []) then
      raise Exception.Create('Could not find temp key: ' + TempPK + ' in table ' + TableName);

    DS.Edit;
    DS.FieldByName(PrimaryKeyFieldName).AsString := RealPK;
    DS.Post;

  finally
    FreeAndNil(DS);
  end;
end;

procedure TLocalCache.PopulateKey(const TableName: string; TempKey, RealKey: Integer);
var
  DS: TDataSet;
  PrimaryKeyFieldName: string;
  FileName: string;
  Strings: TStringList;
  GPSPosFlag:boolean;
begin
  PrimaryKeyFieldName := GetPrimaryKeyFieldForTable(TableName);
  GPSPosFlag := false;
  // Note no updates to locate_status here b/c those are never added or changed by the client.
  if TableName = 'ticket' then begin
    UpdateDetailTable('locate', 'ticket_id', TempKey, RealKey);
    UpdateDetailTable('attachment', 'foreign_id', TempKey, RealKey);
    UpdateDetailTable('ticket_info', 'ticket_id', TempKey, RealKey);
    UpdateDetailTable('jobsite_arrival', 'ticket_id', TempKey, RealKey);
    UpdateDetailTable('notes', 'foreign_id', TempKey, RealKey);
    UpdateDetailTable('addin_info', 'foreign_id', TempKey, RealKey);
  end
  else if TableName = 'locate' then begin
    UpdateDetailTable('locate_hours', 'locate_id', TempKey, RealKey);
    UpdateDetailTable('locate_facility', 'locate_id', TempKey, RealKey);
    UpdateDetailTable('notes', 'foreign_id', TempKey, RealKey);
  end
  else if TableName = 'gps_position' then
  begin

     GPSPosFlag := true;
     {EB: Note: we do not need to update gps table here, it comes in as parameter table which is updated below}
  end;

  DS := OpenLiveQuery('SELECT * FROM "' + TableName + '"');
  try
    if not DS.Locate(PrimaryKeyFieldName, TempKey, []) then
      raise Exception.Create('*Could not find temp key: ' + IntToStr(TempKey) + ' in table ' + TableName);

    {Update key of main table}
    DS.Edit;
    DS.FieldByName(PrimaryKeyFieldName).AsInteger := RealKey;
    try
      DS.Post;
    except
      on E: Exception do begin
        DS.Cancel;

        E.Message := E.Message + Format(' (Table: %s, PK: %s, OldKey: %d, NewKey: %d)', [TableName, PrimaryKeyFieldName, TempKey, RealKey]);

        {GPS Table Key Error}
        if GPSPosFlag = true then
        begin
          AddToLog('GPS Warning (handled) for GPS TempKey: ' + IntToStr(TempKey) + 'RealKey: ' + IntToStr(RealKey));   //QM-799 sr/EB
          exit;
        end;

        {Report error for tables (except GPS)}
        try
          if StrContainsText('duplicate key', E.Message) then begin
            DS.Filter := Format('(%s = %d) OR (%s = %d)', [PrimaryKeyFieldName, TempKey, PrimaryKeyFieldName, RealKey]);
            DS.Filtered := True;
            FileName := 'DupKeyError-' + TableName + '-' + GetLocalUserName + '-' + IsoDateTimeToStr(Now) + '.txt';
            FileName := MakeLegalFileName(FileName);
            FileName := AddSlash(GetClientDataLocalDir) + FileName;
            E.Message := E.Message + Format('  The file %s contains technical information you can send to the help desk.', [FileName]);
            Strings := TStringList.Create;
            try
              Strings.Add(CleanupAllStringFields);
              DS.First;
              ExportTSV(DS, FileName, Strings);
              Strings.Clear;
              Strings.LoadFromFile(FileName);
              Strings.Add(E.Message);
              Strings.SaveToFile(FileName);
            finally
              FreeAndNil(Strings);
            end;
          end;
        except
          // Ignore further errors during the error logging process
        end;
        raise;
      end;
    end;
  finally
    FreeAndNil(DS);
  end;
end;


{

// Check for errors

 Root := XMLDoc.DocumentElement;
 If Root.FirstChild.nodeType = NODE_PROCESSING_INSTRUCTION Then
//-------------------------------------------------------------------
// We have an error - report it
//
// The type of this node is ProcessingInstruction --> we
// can't apply "normal" processing here.
//------------------------------------------------------------------
  ShowMessage(Root.FirstChild.NodeValue)
 Else
}

function TLocalCache.GetSyncStatusTable: TDataSet;
begin
  if not Assigned(FStatusTable) then
    FStatusTable := OpenLiveQuery('select * from syncstatus');

  if not FStatusTable.Active then
    FStatusTable.Open;

  Result := FStatusTable;
end;

procedure TLocalCache.AddToLog(const Msg: string);
begin
  if Assigned(FLogEvent) then
    FLogEvent(Self, Msg);
end;

procedure TLocalCache.ClearChangeFlagsInTable(const TableName: string);
var
  Query: TDBISAMQuery;
begin
  Query := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(Query);
    Query.SQL.Text := 'UPDATE ' + TableName + ' SET DeltaStatus=NULL WHERE DeltaStatus IS NOT NULL';
    Query.ExecSQL;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TLocalCache.ClearLocalChangeFlags;
var
  TableName: string;
begin
  with GetSyncStatusTable do begin
    First;
    while not EOF do begin
      TableName := FieldByName('TableName').AsString;
      ClearChangeFlagsInTable(TableName);
      Next;
    end;
  end;
end;

procedure TLocalCache.ClearSelLocalTables;
const
  DELSQL = 'Delete from ';
var
  ClrQry: TDBISAMQuery;
begin
  ClrQry := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(ClrQry);
    ClrQry.SQL.Text := DELSQL + 'EmpPlus';
    ClrQry.ExecSQL;
//    if not DeveloperMode then begin   //QM-799 EB GPS Error - clear after sync
//      CLrQry.SQL.Text := DELSQL + 'gps_position';
//      ClrQry.ExecSQL;
//    end;

    //QM-799 EB Marking for clearing gps_position
  finally
    FreeAndNil(ClrQry);
  end;
end;

procedure TLocalCache.TicketDataClean;
begin
  {Check permission and the date of the last clean}
  if (DM.UQState.LastTicketDataClean < Today) then begin
    if PermissionsDM.CanI(RightTicketsCacheAutoClear) then
       DM.ReloadTicketData(False);
  end;
end;

procedure TLocalCache.BeforePost(DataSet: TDataSet);
var
  Field: TField;
begin
  if DataSet.Tag = DisableDeltaStatusTag then
    Exit;
  Field := DataSet.FieldByName('DeltaStatus');
  if Field.IsNull then
    Field.AsString := 'U';
end;

procedure TLocalCache.NewRecord(DataSet: TDataSet);
begin
  if DataSet.Tag <> DisableDeltaStatusTag then
    DataSet.FieldByName('DeltaStatus').AsString := 'I';
  SetActiveField(DataSet, True);
end;

procedure TLocalCache.NewRecordAutoInc(DataSet: TDataSet);
begin
  NewRecord(DataSet);
  DataSet.Fields[0].AsInteger := GenTempKey;
end;

function TLocalCache.GenTempKey: Integer;
begin
  Result := -Random(1000000) - 10000
end;

procedure TLocalCache.SetActiveField(DataSet: TDataSet; ActiveValue: Boolean);
var
  F: TField;
begin
  F := DataSet.FindField('active');
  if F <> nil then
    F.AsBoolean := ActiveValue;

  F := DataSet.FindField('active_ind');
  if F <> nil then
    F.AsBoolean := ActiveValue;
end;

procedure TLocalCache.GetTicketAlertsFromServer(TicketID: Integer);  //QM-771 Ticket Alerts EB
var
  XMLStr: string;
begin
  Assert(TicketID > 0);
  GetChan.OperationName := 'GetTicketAlerts';
  XMLStr := Service.GetTicketAlerts(SecurityInfo, TicketID);
  SyncTablesFromXML(ParseXML(XMLStr));
end;

procedure TLocalCache.GetTicketDataFromServer(TicketID: Integer);
var
  Xml: string;
  LastSyncDateTime: TDateTime;
begin
  Assert(TicketID > 0);
  GetChan.OperationName := 'GetTicket2';
  LastSyncDateTime := IsoStrToDateTime(GetLastSyncDate);
  Xml := Service.GetTicket2(SecurityInfo, TicketID, LastSyncDateTime);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetEPRHistory(TicketID: Integer);
var
  XML: string;
begin
  Assert(TicketID > 0);
  GetChan.OperationName := 'GetEPRHistory';
  Xml := Service.GetEPRHistory(SecurityInfo, TicketID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetRiskHistory(TicketID: Integer);   //QM-387 EB
var
  XML: string;
begin
  Assert(TicketID > 0);
  GetChan.OperationName := 'GetRiskHistory';
  Xml := Service.GetRiskHistory(SecurityInfo, TicketID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.BeforeDelete(DataSet: TDataSet);
begin
  DataSet.Edit;
  SetActiveField(DataSet, False);
  DataSet.Post;
  Abort;
end;

procedure TLocalCache.LinkEvents(DataSet: TDataset);
begin
  DataSet.BeforePost := BeforePost;
  DataSet.BeforeDelete := BeforeDelete;
  DataSet.OnNewRecord := NewRecord;
end;

procedure TLocalCache.LinkEventsAutoInc(DataSet: TDataset);
begin
  Assert(Assigned(DataSet));
  LinkEvents(DataSet);
  DataSet.OnNewRecord := NewRecordAutoInc;
end;

procedure TLocalCache.SendTimesheetEntries;
var
  Sheet: NVPairList;
  Sheets: TimesheetEntryList;
  DS: TDataSet;
  FieldIndex: Integer;
  FieldData: NVPair;
  Field: TField;
  ReturnedKeys: GeneratedKeyList;
begin
  Sheets := nil;
  DS := OpenQuery('SELECT * FROM timesheet_entry where deltastatus is not null');
  try
    Sheets := TimesheetEntryList.Create;
    while not DS.EOF do begin
      Assert((not DS.FieldByName('entry_date_local').IsNull) and (not DS.FieldByName('work_emp_id').IsNull));
      Sheet := Sheets.Add;
      for FieldIndex := 0 to DS.FieldCount - 1 do begin
        Field := DS.Fields[FieldIndex];
        if StringInArray(Field.FieldName, NeverSendFieldNames) then
          Continue;
        FieldData := AddFieldData(Sheet, Field);
        if (FieldData.Name = 'vehicle_use') and (FieldData.Value = '') then
          FieldData.Value := 'NA';
        if (not Field.IsNull) and (Field is TDateTimeField) then begin
          if StrContains('start', Field.FieldName) or StrContains('stop', Field.FieldName) then
            FieldData.Value := IsoTimeToStr(Field.AsDateTime); // Callouts and work periods are times with no date
        end;
      end;
      DS.Next;
    end;
    if Sheets.Count > 0 then begin
      AddToLog(Format('Sending %d timesheet entries to server', [Sheets.Count]));
      GetChan.OperationName := 'PostTimesheets';
      ReturnedKeys := Service.PostTimesheets(SecurityInfo, Sheets);
      SaveGeneratedKeys(ReturnedKeys);
      ClearChangeFlagsInTable('timesheet_entry');
    end;
  finally
    FreeAndNil(DS);
    FreeAndNil(Sheets);
  end;
end;

procedure TLocalCache.SendNewAttachments;
var
  DS: TDataSet;
  ReturnedKeys: GeneratedKeyList;
  Attachments: NewAttachmentList;
  Attach: NewAttachment;
begin
  Attachments := nil;
  ReturnedKeys := nil;

  DS := OpenQuery('SELECT * FROM attachment where attachment_id < 0');
  try
  	Attachments := NewAttachmentList.Create;
    ReturnedKeys := GeneratedKeyList.Create;
    while not DS.EOF do begin
      Attach := Attachments.Add;
      Attach.AttachmentID := DS.FieldByName('attachment_id').AsInteger;
      Attach.ForeignType := DS.FieldByName('foreign_type').AsInteger;
      Attach.ForeignID := DS.FieldByName('foreign_id').AsInteger;
      Attach.Size := DS.FieldByName('size').AsInteger;
      Attach.FileName := DS.FieldByName('filename').AsString;
      Attach.OrigFileName := DS.FieldByName('orig_filename').AsString;
      Attach.OrigFileModDate := DS.FieldByName('orig_file_mod_date').AsDateTime;
      Attach.AttachDate := DS.FieldByName('attach_date').AsDateTime;
      Attach.AttachedBy := DS.FieldByName('attached_by').AsInteger;
      Attach.Source := DS.FieldByName('source').AsString;
      Attach.FileHash := DS.FieldByName('file_hash').AsString;
      Attach.UploadMachineName := DS.FieldByName('upload_machine_name').AsString;

      DS.Next;
    end;
    if Attachments.Count > 0 then begin
      AddToLog('Sending new attachments to server');
      GetChan.OperationName := 'SaveNewAttachments';
      Service.SaveNewAttachments(SecurityInfo, Attachments, ReturnedKeys);
      SaveGeneratedKeys(ReturnedKeys);
    end;
  finally
    FreeAndNil(DS);
    FreeAndNil(ReturnedKeys);
    FreeAndNil(Attachments);
  end;
end;

procedure TLocalCache.SendAttachmentUploads;
var
  DS: TDataSet;
  Attachments: AttachmentUploadList;
  Attach: AttachmentUpload;
begin
  Attachments := nil;

  DS := OpenQuery('select * from attachment where upload_date is not null ' +
    'and DeltaStatus is not null and attachment_id >= 0');

  try
    Attachments := AttachmentUploadList.Create;
    while not DS.EOF do begin
      Attach := Attachments.Add;
      Attach.AttachmentID := DS.FieldByName('attachment_id').AsInteger;
      Attach.BackgroundUpload := DS.FieldByName('background_upload').AsBoolean;
      Attach.UploadDate := DS.FieldByName('upload_date').AsDateTime;

      DS.Next;
    end;
    if Attachments.Count > 0 then begin
      AddToLog('Sending attachment uploads to server');
      GetChan.OperationName := 'SaveAttachmentUploads';
      Service.SaveAttachmentUploads(SecurityInfo, Attachments);
    end;
  finally
    FreeAndNil(Attachments);
    FreeAndNil(DS);
  end;
end;

procedure TLocalCache.SendAttachmentUpdates;
var
  DS: TDataSet;
  Attachments: AttachmentUpdateList;
  Attach: AttachmentUpdate;
begin
  Attachments := nil;

  DS := OpenQuery('SELECT * FROM attachment where DeltaStatus is not null and attachment_id >= 0');
  try
    Attachments := AttachmentUpdateList.Create;
    while not DS.EOF do begin
      Attach := Attachments.Add;
      Attach.AttachmentID := DS.FieldByName('attachment_id').AsInteger;
      Attach.Active := DS.FieldByName('active').AsBoolean;
      Attach.Comment := DS.FieldByName('comment').AsString;
      Attach.DocumentType := DS.FieldByname('doc_type').AsString;

      DS.Next;
    end;
    if Attachments.Count > 0 then begin
      AddToLog('Sending attachment updates to server');
      GetChan.OperationName := 'SaveAttachmentUpdates';
      Service.SaveAttachmentUpdates(SecurityInfo, Attachments);
    end;
  finally
    FreeAndNil(Attachments);
    FreeAndNil(DS);
  end;
end;

procedure TLocalCache.AddToMyLog(Sender: TObject; const Msg: string);
begin
  AddToLog(Msg);
end;

procedure TLocalCache.SetDBISAMDatabase(DataSet: TDBISAMDBDataSet);
begin
  DataSet.DatabaseName := FDatabase.DatabaseName;
  DataSet.SessionName := FDatabase.SessionName;
end;

procedure TLocalCache.GetDamageDataFromServer(DamageID: Integer);
var
  Xml: string;
begin
  Assert(DamageId > 0);
  GetChan.OperationName := 'GetDamage2';
  Xml := Service.GetDamage2(SecurityInfo, DamageID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetWorkOrderDataFromServer(WorkOrderID: Integer);
var
  Xml: string;
begin
  Assert(WorkOrderId > 0);
  GetChan.OperationName := 'GetWorkOrder';
  Xml := Service.GetWorkOrder(SecurityInfo, WorkOrderID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetAssetsFromServer(EmpID: Integer);
begin
  GetChan.OperationName := 'GetAssets';
  SyncTablesFromXmlString(Service.GetAssets(SecurityInfo, EmpID));
end;

procedure TLocalCache.GetDamageLitigationDataFromServer(LitigationID: Integer);
var
  Xml: string;
begin
  Assert(LitigationID > 0);
  GetChan.OperationName := 'GetDamageLitigation';
  Xml := Service.GetDamageLitigation(SecurityInfo, LitigationID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetDamageThirdPartyDataFromServer(ThirdPartyID: Integer);
var
  Xml: string;
begin
  Assert(ThirdPartyID > 0);
  GetChan.OperationName := 'GetDamageThirdParty';
  Xml := Service.GetDamageThirdParty(SecurityInfo, ThirdPartyID);
  SyncTablesFromXml(ParseXml(Xml));
end;



procedure TLocalCache.GetInvoiceDataFromServer(InvoiceID: Integer);
var
  Xml: string;
begin
  Assert(InvoiceID > 0);
  GetChan.OperationName := 'GetDamageInvoice';
  Xml := Service.GetDamageInvoice(SecurityInfo, InvoiceID);
  SyncTablesFromXml(ParseXml(Xml));
end;

function TLocalCache.HavePendingTicketChanges: Boolean;
const
  { Most ticket changes mark the ticket record modified, but check other tables
    in case. This list should include all tables that can get modified by the
    client, that need to be synced up along with related tickets. }
  CheckTicketTables: array[0..11] of string =
    ('ticket', 'locate', 'locate_hours', 'notes', 'attachment', 'locate_plat',
    'locate_facility', 'addin_info', 'ticket_info', 'task_schedule',
    'jobsite_arrival', 'gps_position');
var
  i: Integer;
begin
  Result := False;
  for i := Low(CheckTicketTables) to High(CheckTicketTables) do begin
    if TableHasPendingUpdates(CheckTicketTables[i]) then begin
      Result := True;
      Break;
    end;
  end;
end;

function TLocalCache.HavePendingWorkOrderChanges: Boolean;    //QMANTWO-391 EB
const
  CheckWorkOrderTables: array[0..8] of string =
    ('work_order', 'work_order_version', 'work_order_ticket', 'wo_assignment',
    'wo_status_history', 'wo_response_log', 'work_order_inspection',
    'work_order_remedy', 'work_order_OHM_details');
var
  i: Integer;
begin
  Result := False;
  for i := Low(CheckWorkOrderTables) to High(CheckWorkOrderTables) do begin
    if TableHasPendingUpdates(CheckWorkOrderTables[i]) then begin
      Result := True;
      Break;
    end;
  end;
end;

function TLocalCache.TableHasPendingUpdates(const TableName: string): Boolean;
var
  CountDataSet: TDataSet;
begin
  CountDataSet := OpenQuery('SELECT COUNT(*) AS N FROM ' + TableName + ' WHERE DeltaStatus IS NOT NULL');
  try
    Result := CountDataSet.Fields[0].AsInteger > 0;
  finally
    FreeAndNil(CountDataSet);
  end;
end;



function TLocalCache.RunSQLReturnN(const SQL: string): Integer;
begin
  with OpenQuery(SQL) do try
    Result := FieldByName('N').AsInteger;
  finally
    Free;
  end;
end;

function TLocalCache.CreateTable(const TableName: string): TDataSet;
var
  Table: TDBISAMTable;
begin
  Table := TDBISAMTable.Create(nil);
  try
    SetDBISAMDatabase(Table);
    Table.TableName := TableName;
    Result := Table;
  except
    FreeAndNil(Table);
    raise;
  end;
end;

procedure TLocalCache.UnLinkEvents(DataSet: TDataset);
begin
  Assert(Assigned(DataSet));
  DataSet.OnNewRecord := nil;
  DataSet.BeforePost := nil;
  DataSet.BeforeDelete := nil;
  DataSet.OnNewRecord := nil;
end;

procedure TLocalCache.EnsureServerSet;
begin
  Assert(Assigned(FServer));
end;

procedure TLocalCache.SendBillingAdjustments;
var
  BillingAdjustment: TDataSet;
  NewAdjustments: NVPairListList;
  NewAdjustment: NVPairList;
  i: Integer;
  Field: TField;
  ReturnedKeys: GeneratedKeyList;
begin
  NewAdjustments := nil;
  BillingAdjustment := OpenQuery('select * from billing_adjustment where adjustment_id<0 or DeltaStatus=''U''');
  try
    if not BillingAdjustment.IsEmpty then begin
      AddToLog(Format('Sending %d new billing adjustments...', [BillingAdjustment.RecordCount]));
      NewAdjustments := NVPairListList.Create;
      while not BillingAdjustment.Eof do begin
        NewAdjustment := NewAdjustments.Add;
        for i := 0 to BillingAdjustment.FieldCount - 1 do begin
          Field := BillingAdjustment.Fields[i];
          if not StringInArray(Field.FieldName, NeverSendFieldNames) then
            AddFieldData(NewAdjustment, Field);
        end;
        BillingAdjustment.Next;
      end;
      GetChan.OperationName := 'SaveBillingAdjustments';
      ReturnedKeys := Service.SaveBillingAdjustments(SecurityInfo, NewAdjustments);
      SaveGeneratedKeys(ReturnedKeys);
    end;
  finally
    FreeAndNil(NewAdjustments);
    FreeAndNil(BillingAdjustment);
  end;
end;

procedure TLocalCache.SendNewTickets;
var
  Tickets: TDataSet;
  NewTickets: NewTicketList;
  Ticket: NewTicket;
  i: Integer;
  Field: TField;
  ReturnedKeys: GeneratedKeyList;
begin
  NewTickets := nil;
  Tickets := OpenQuery('select * from ticket where ticket_id < 0');
  try
    if not Tickets.Eof then begin
      AddToLog(Format('Sending %d new %s...', [Tickets.RecordCount, AddSIfNot1(Tickets.RecordCount, 'ticket')]));
      NewTickets := NewTicketList.Create;
      while not Tickets.Eof do begin
        Assert(StrBeginsWith('M', Tickets.FieldByName('ticket_number').AsString));
        Ticket := NewTickets.Add;
        for i := 0 to Tickets.FieldCount - 1 do begin
          Field := Tickets.Fields[i];
          if not StringInArray(Field.FieldName, NeverSendFieldNames) then
            AddFieldData(Ticket.TicketData, Field);
        end;
        Tickets.Next;
      end;
      GetChan.OperationName := 'SaveNewTickets';
      ReturnedKeys := Service.SaveNewTickets(SecurityInfo, NewTickets);
      SaveGeneratedKeys(ReturnedKeys);
      // The ticket might still be marked modified if a locate was edited
    end;
  finally
    FreeAndNil(NewTickets);
    FreeAndNil(Tickets);
  end;
end;

procedure TLocalCache.SendPlatHistoryDate;
const
  NODATE = 0;
var
  EmpTableDate: TDateTime;
  PlatHistoryFileDate: TDateTime;
  AdjD: TDateTime;
  NoFileFound: boolean;
begin
  try
    PlatHistoryFileDate := NODATE;
    if not FileExists(PLAT_FILE_PATH) then  //Don't do anything
       Exit;
    PlatHistoryFileDate := GetFileDate(PLAT_FILE_PATH);

    if PlatHistoryFileDate > 0 then
      NoFileFound := False;
  except
    NoFileFound := True;
  end;
  EmpTableDate := EmployeeDM.GetLastPlatUpdate(DM.EmpID);
  AdjD := IncDay(EmpTableDate, 1);
  if ((PlatHistoryFileDate > AdjD) and
     (PlatHistoryFileDate > NODATE))  then begin
    EmployeeDM.UpdateEmployee_PlatUpdate(PlatHistoryFileDate);
//    ShowMessage('Table Date: ' + DateTimeToStr(EmpTableDate) + ' | ' + 'History Date: ' +  DateTimeToStr(PlatHistoryFileDate));
    AddToLog('Employee Plat History File Date - Updated');
  end;
end;

procedure TLocalCache.SendRouteOrderChanges;
var
 RouteOrderList: TicketInfoChangeList;
 ReturnedKeys: GeneratedKeyList;
begin
 RouteOrderList := CreateTicketInfoROList;
 try
   if RouteOrderList.Count > 0 then begin
     AddToLog(Format('Sending %d ticket_info records to server', [RouteOrderList.Count, AddSIfNot1(RouteOrderList.Count, 'ticket_info')]));
     GetChan.OperationName := 'ProcessTicketInfoChanges';
     Service.SaveOtherTicketInfoChanges(SecurityInfo, RouteOrderList, ReturnedKeys);
     SaveGeneratedKeys(ReturnedKeys);
     AddToLog(Format('%d ticket_info records sent', [RouteOrderList.Count, AddSIfNot1(ROuteOrderList.Count, 'ticket_info')]));
   end;
 finally
   FreeAndNil(ReturnedKeys);
   FreeAndNil(RouteOrderList);
 end;

 {CheckNotes}

end;

procedure TLocalCache.SendTicketChanges;
var
  TkList: TicketChangeList15;
  ReturnedKeys: GeneratedKeyList;
begin
  TkList := CreateTicketChangeList;
  try
    if TkList.Count > 0 then begin
      AddToLog(Format('Sending %d modified %s to server', [TkList.Count, AddSIfNot1(TkList.Count, 'ticket')]));
      GetChan.OperationName := 'StatusTicketsLocates15';
      Service.StatusTicketsLocates15(SecurityInfo, TkList, ReturnedKeys);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      ClearAreaChangedFlags;
      AddToLog(Format('Changes to %d %s sent', [TkList.Count, AddSIfNot1(TkList.Count, 'ticket')]));
    end;
  finally
    FreeAndNil(TkList);
  end;
end;

function TLocalCache.CreateLocatePlatChangeList(const TicketID: Integer): LocatePlatChangeList;
const
  Select = 'select lp.locate_plat_id, lp.locate_id, lp.plat, lp.active ' +
    'from locate_plat lp inner join locate l on lp.locate_id = l.locate_id ' +
    'where lp.DeltaStatus is not null and l.ticket_id=';
var
  PlatDS: TDataSet;
begin
  Result := LocatePlatChangeList.Create;
  PlatDS := OpenQuery(Select + IntToStr(TicketID));
  try
    LoadROArrayFromDataSet(Result, PlatDS);
  finally
    FreeAndNil(PlatDS);
  end;
end;

function TLocalCache.CreateTicketAreaList(const TicketID: Integer): TicketAreaChangeList;
var
  TicketDS: TDataSet;
const
  Select = 'select ticket_id, route_area_id, area_changed_date from ticket ' +
    'where DeltaStatus is not null and area_changed_date is not null and ticket_id = ';
begin
  Result := TicketAreaChangeList.Create;
  TicketDS := OpenQuery(Select + IntToStr(TicketID));
  try
    LoadROArrayFromDataSet(Result, TicketDS);
  finally
    FreeAndNil(TicketDS);
  end;
end;

function TLocalCache.CreateTicketInfoList(const TicketID: Integer): TicketInfoChangeList;
var
  TicketInfoDS: TDataSet;
const
  Select = 'select ticket_info_id, ticket_id, info_type, info ' +
    'from ticket_info where DeltaStatus is not null and ticket_id = ';
begin
  Result := TicketInfoChangeList.Create;
  TicketInfoDS := OpenQuery(Select + IntToStr(TicketID));
  try
    LoadROArrayFromDataSet(Result, TicketInfoDS);
  finally
    FreeAndNil(TicketInfoDS);
  end;
end;

function TLocalCache.CreateTicketInfoROList: TicketInfoChangeList;  //QMANTWO-775
var
  TicketInfoDS: TDataSet;
const
  SQL = 'select ticket_info_id, ticket_id, info_type, info ' +
        'from ticket_info where info_type=''ROUTEORD'' and ticket_info_id < 1';
begin
  Result := TicketInfoChangeList.Create;
  TicketInfoDS := OpenQuery(SQL); //format(SQL, ['ROUTEORD']));
  try
    LoadROArrayFromDataSet(Result, TicketInfoDS);
  finally
    FreeAndNil(TicketInfoDS);
  end;

end;

function TLocalCache.CreateTicketChangeList: TicketChangeList15;
var
  TicketDS, LocateDS, NotesDS, HoursDS: TDataSet;
  TicketID: Integer;
  TkList: TicketChangeList15;
  Tk: TicketChange15;
  Note: TicketNote5;
  Loc: LocateChange6;
  H: LocateHour3;
begin
  TkList := TicketChangeList15.Create;

  TicketDS := OpenQuery('SELECT * FROM Ticket WHERE DeltaStatus=''U''');
  try
    while not TicketDS.EOF do begin
      TicketID := TicketDS.FieldByName('ticket_id').AsInteger;

      Tk := TkList.Add;

      Tk.TicketID := TicketKey.Create;
      Tk.TicketID.TicketID := TicketID;

      // 0 (null) means do not create a followup ticket
      Tk.FollowupTypeID := TicketDS.FieldByName('followup_type_id').AsInteger;
      Tk.FollowupTransmitDate := TicketDS.FieldByName('followup_transmit_date').AsDateTime;
      Tk.FollowupDueDate := TicketDS.FieldByName('followup_due_date').AsDateTime;

      NotesDS := OpenQuery('SELECT N.*, client_code FROM Notes N ' +
        ' INNER JOIN Locate ON foreign_id=locate_id ' +
        ' WHERE ticket_id=' + IntToStr(TicketID) +
        ' AND foreign_type=' + IntToStr(qmftLocate) +
        ' AND N.DeltaStatus IS NOT NULL ' +
        ' UNION ALL SELECT *, '''' AS client_code FROM Notes ' +
        ' WHERE foreign_type=' + IntToStr(qmftTicket) +
        ' AND foreign_id=' + IntToStr(TicketID) +
        ' AND DeltaStatus IS NOT NULL');
      try
        while not NotesDS.Eof do begin
          Note := Tk.NewNotes.Add;
          Note.NoteID := NotesDS.FieldByName('notes_id').AsInteger; // local key
          Note.EntryDate := NotesDS.FieldByName('entry_date').AsDateTime;
          Note.EnteredByUID := NotesDS.FieldByName('uid').AsInteger;
          Note.NoteText := NotesDS.FieldByName('note').AsString;
          Note.ClientCode := NotesDS.FieldByName('client_code').AsString;
          Note.SubType := NotesDS.FieldByName('sub_type').AsInteger;
          NotesDS.Next;
        end;
      finally
        FreeAndNil(NotesDS);
      end;

      LocateDS := OpenQuery('SELECT * FROM Locate' +
        ' WHERE ticket_id = ' + IntToStr(TicketID) +
        ' AND DeltaStatus IS NOT NULL');
      try
        while not LocateDS.EOF do begin
          Loc := Tk.LocateChanges.Add;
          Loc.LocateID := LocateDS.FieldByName('locate_id').AsInteger; // perhaps a local key
          Loc.ClientCode := LocateDS.FieldByName('client_code').AsString;
          Loc.Status := LocateDS.FieldByName('status').AsString;
          Loc.StatusDate := LocateDS.FieldByName('closed_date').AsDateTime;
          Loc.HighProfile := LocateDS.FieldByName('high_profile').AsBoolean;
          Loc.QtyMarked := LocateDS.FieldByName('qty_marked').AsFloat;  //QM-1037 Bigfoot/ESketch - no change, we just need to make sure it is calculated correctly
          Loc.StatusedHow := LocateDS.FieldByName('closed_how').AsString;
          Loc.HighProfileReason := LocateDS.FieldByName('high_profile_reason').AsInteger;
          Loc.StatusAsAssignedLocator := LocateDS.FieldByName('StatusAsAssignedUser').AsInteger = 1;
          Loc.MarkType := LocateDS.FieldByName('mark_type').AsString;
          Loc.EntryDate := LocateDS.FieldByName('entry_date').AsDateTime;
          Loc.GPSID := LocateDS.FieldByName('gps_id').AsInteger;
          LocateDS.Next;
        end;
      finally
        FreeAndNil(LocateDS);
      end;

      HoursDS := OpenQuery('select locate.client_code, locate.ticket_id, lh.*' +
        ' from locate inner join locate_hours lh on locate.locate_id=lh.locate_id' +
        ' where locate.ticket_id = ' + IntToStr(TicketID) +
        ' AND lh.DeltaStatus IS NOT NULL');
      try
        while not HoursDS.EOF do begin
          H := Tk.HoursChanges.Add;
          H.LocateHoursID := HoursDS.FieldByName('locate_hours_id').AsInteger; // local key
          H.ClientCode := HoursDS.FieldByName('client_code').AsString;
          H.EmpID := HoursDS.FieldByName('emp_id').AsInteger;
          H.WorkDate := HoursDS.FieldByName('work_date').AsDateTime;
          H.EntryDate := HoursDS.FieldByName('entry_date').AsDateTime;
          H.RegHours := HoursDS.FieldByName('regular_hours').AsFloat;
          H.OTHours := HoursDS.FieldByName('overtime_hours').AsFloat;
          H.Status := HoursDS.FieldByName('status').AsString;
          H.UnitsMarked := HoursDS.FieldByName('units_marked').AsInteger;
          H.UnitConversionID := HoursDS.FieldByName('unit_conversion_id').AsInteger;

          HoursDS.Next;
        end;
      finally
        FreeAndNil(HoursDS);
      end;

      Tk.AreaChanges := CreateTicketAreaList(TicketID);
      Tk.InfoChanges := CreateTicketInfoList(TicketID);
      Tk.PlatChanges := CreateLocatePlatChangeList(TicketID);
      Tk.ArrivalTimeChanges := CreateJobsiteArrivalChanges(TicketID);
      Tk.FacilityChanges := CreateLocateFacilityChanges(TicketID);
      Tk.AddinInfoChanges := CreateAddinInfoChangeList(qmftTicket, TicketID);
      Tk.GPSChanges := CreateTicketGPSChanges(TicketID);
      TicketDS.Next;
    end;
  finally
    FreeAndNil(TicketDS);
  end;

  Result := TkList; // The caller must free this
end;

procedure TLocalCache.SaveGeneratedKeys(Keys: GeneratedKeyList);
var
  i: Integer;
begin
  for i := 0 to Keys.Count - 1 do
    PopulateKey(Keys[i].TableName, Keys[i].TempKeyValue, Keys[i].KeyValue);
end;

procedure TLocalCache.SaveRequiredDocuments;
var
  DocList: RequiredDocumentList;
  ReqDoc: RequiredDocument;
  DocumentDS: TDataSet;
  ReturnedKeys: GeneratedKeyList;
begin
  DocumentDS := nil;
  DocList := RequiredDocumentList.Create;
  try
    DocumentDS := OpenQuery('SELECT * FROM document WHERE doc_id < 0 or DeltaStatus=''U''');
    while not DocumentDS.Eof do begin
      ReqDoc := DocList.Add;
      ReqDoc.DocID := DocumentDS.FieldByName('doc_id').AsInteger;
      ReqDoc.ForeignType := DocumentDS.FieldByName('foreign_type').AsInteger;
      ReqDoc.ForeignID := DocumentDS.FieldByName('foreign_id').AsInteger;
      ReqDoc.RequiredDocID := DocumentDS.FieldByName('required_doc_id').AsInteger;
      ReqDoc.Comment := DocumentDS.FieldByName('comment').AsString;
      ReqDoc.AddedByID := DocumentDS.FieldByName('added_by_id').AsInteger;
      ReqDoc.AddedDate := DocumentDS.FieldByName('added_date').AsDateTime;
      ReqDoc.Active := DocumentDS.FieldByName('active').AsBoolean;
      DocumentDS.Next;
    end;

    if DocList.Count > 0 then begin
      AddToLog(Format('Sending %d modified %s to server', [DocList.Count, AddSIfNot1(DocList.Count, 'document')]));
      GetChan.OperationName := 'SaveRequriedDocs';
      ReturnedKeys := Service.SaveRequiredDocuments(SecurityInfo, DocList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [DocList.Count, AddSIfNot1(DocList.Count, 'document')]));
    end;
  finally
    FreeAndNil(DocumentDS);
    FreeAndNil(DocList);
  end;
end;

procedure TLocalCache.DeveloperWarning;
var
  HostStr: string;
begin
  if DeveloperMode then begin
    if not HavePendingTicketChanges then
      Exit;
    HostStr := GetHostFromURL(Server.GetBaseURL);
    if Pos('utiliquest.com', HostStr) > 0 then begin
      AddToLog('Developer Mode: Changes sent to ' + HostStr);
    end;
  end;
end;

procedure TLocalCache.UpdateDetailTable(const TableName, FieldName: string; OldKey, NewKey: Integer);
const
  DetailSQL = 'update %s set %s = %d where %s = %d';
var
  Query: TDBISAMQuery;
begin
  Query := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(Query);
    Query.SQL.Text := Format(DetailSQL, [TableName, FieldName, NewKey, FieldName, OldKey]);
    try
      Query.ExecSQL;
    except
      on E: Exception do begin
        E.Message := E.Message + ' (' + Query.SQL.Text + ')';
        raise;
      end;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TLocalCache.GetFirstTaskReminder(EmployeeID: integer);   //QM-494 First Task Reminder EB
var
  FTR: TFirstTaskReminder;
begin
  if PermissionsDM.CanI(RightTicketsViewFirstTaskReminder)  then begin
    FTR := TFirstTaskReminder.Create(EmployeeID, False);
    try
      DM.UQState.UpdateFirstTaskIni(FTR.ReminderStr, FTR.Required);
    finally
      FreeAndNil(FTR);
    end;

  end
  else
    DM.UQState.UpdateFirstTaskIni('', False);

end;

function TLocalCache.ExecQuery(const SQL: string): Integer;
var
  DataSet: TDBISAMQuery;
begin
  DataSet := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(DataSet);
    DataSet.RequestLive := False;
    DataSet.SQL.Text := SQL;
    DataSet.ExecSQL;
    Result := DataSet.RowsAffected;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TLocalCache.ExecQueryWithSingleParam(const SQL, ParamName: string;
  const ParamValue: Variant; const ParamType: TFieldType): Integer;
var
  DataSet: TDBISAMQuery;
begin
  Assert(Pos(ParamName, SQL) > 0, 'Named parameter is not in the sql command.');
  DataSet := TDBISAMQuery.Create(nil);
  try
    SetDBISAMDatabase(DataSet);
    DataSet.RequestLive := False;
    DataSet.ParamCheck := True;
    DataSet.SQL.Text := SQL;

    {
    Ideally, this should check the DataSet.Parameters to determine the datatype,
     but the version of DBISAM (3.19) QM uses doesn't provide that info. IF that
     works in more recent versions, get rid of the ParamType parameter.
     }
    if ParamType = ftDateTime then
      DataSet.ParamByName(ParamName).AsDateTime := ParamValue
    else if ParamType = ftBoolean then
      DataSet.ParamByName(ParamName).AsBoolean := ParamValue
    else if ParamType = ftInteger then
      DataSet.ParamByName(ParamName).AsInteger := ParamValue
    else if ParamType = ftFloat then
      DataSet.ParamByName(ParamName).AsFloat := ParamValue
    else if ParamType = ftCurrency then
      DataSet.ParamByName(ParamName).AsCurrency := ParamValue
    else
      DataSet.ParamByName(ParamName).AsString := ParamValue;

    DataSet.ExecSQL;
    Result := DataSet.RowsAffected;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TLocalCache.GetTicketHistoryFromServer(TicketID: Integer);
var
  Xml: string;
begin
  Assert(TicketId > 0);
  GetChan.OperationName := 'GetTicketHistory';
  Xml := Service.GetTicketHistory(SecurityInfo, TicketID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetWorkOrderHistoryFromServer(WorkOrderID: Integer);
var
  Xml: string;
begin
  Assert(WorkOrderID > 0);
  GetChan.OperationName := 'GetWorkOrderHistory';
  Xml := Service.GetWorkOrderHistory(SecurityInfo, WorkOrderID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetWorkOrderOHMDetailsFromServer(WorkOrderID: Integer);  //QMANTWO-391 EB
var
  Xml: string;
begin
  Assert(WorkOrderID > 0);
  GetChan.OperationName := 'GetWorkOrderOHM';
  Xml := Service.GetWorkOrderOHM(SecurityInfo, WorkOrderID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetTableDataFromServer(const TableName: string; UpdatesSince: TDateTime);
var
  XML: string;
begin
  GetChan.OperationName := 'GetTableData';
  XML := Service.GetTableData(SecurityInfo, TableName, UpdatesSince);
  SyncTablesFromXmlString(XML);
end;

procedure TLocalCache.GetColumnDataFromServer(const TableName, KeyField, Columns: string; NonNullOnly: Boolean);
var
  XML: string;
begin
  GetChan.OperationName := 'GetColumnData';
  XML := Service.GetColumnData(SecurityInfo, TableName, KeyField, Columns, 0, NonNullOnly);
  SyncTablesFromXmlString(XML);
end;


procedure TLocalCache.PopulateTableFromServerIfEmpty(const TableName: string);
begin
  Assert(NotEmpty(TableName));
  if GetTableRowCount(TableName) < 1 then
    GetTableDataFromServer(TableName);
end;

function TLocalCache.GetFieldNotNullCount(const TableName, FieldName: string): Integer;
var
  SQL: string;
begin
  SQL := Format('select count(*) as N from %s where %s is not null', [TableName, FieldName]);
  Result := RunSQLReturnN(SQL);
end;

procedure TLocalCache.PopulateMissingTableData(EmployeeID: Integer);         {Missing Tables}
begin
  // When adding a new column, force older clients to get the data
  //if GetFieldNotNullCount('client', 'alert') < 1 then
  //  GetColumnDataFromServer('client', 'client_id', 'alert');
  // Or, when adding a new table:
  // PopulateTableFromServerIfEmpty('widget');

  // WARNING - only use this for smallish lookup tables, never for
  // any of the primary large data tables (tickets, damages, etc)

  {STATIC TABLES THAT ARE ALWAYS PULLED INTO DBISAM}
  PopulateTableFromServerIfEmpty('break_rules');
  PopulateTableFromServerIfEmpty('configuration_data');
  PopulateTableFromServerIfEmpty('reference');     //QM-933 EB sortby (found this was missing for some reason)
  PopulateTableFromServerIfEmpty('upload_file_type');
  PopulateTableFromServerIfEmpty('right_definition');
  PopulateTableFromServerIfEmpty('billing_gl');
  PopulateTableFromServerIfEmpty('right_restriction');
  PopulateTableFromServerIfEmpty('restricted_use_exemption');
  PopulateTableFromServerIfEmpty('restricted_use_message');
  PopulateTableFromServerIfEmpty('highlight_rule');
  PopulateTableFromServerIfEmpty('required_document');
  PopulateTableFromServerIfEmpty('required_document_type');
  PopulateTableFromServerIfEmpty('damage_profit_center_rule');
  PopulateTableFromServerIfEmpty('client_info');
  PopulateTableFromServerIfEmpty('custom_form_reference');  //QM-933 EB
  if GetTableRowCount('emp_group_right') <= 0 then
    GetRightsForEmp(EmployeeID);
  if GetTableRowCount('users') <= 1 then
    GetColumnDataFromServer('users', 'uid', 'grp_id;emp_id;first_name;last_name;active_ind', False);
  if GetFieldNotNullCount('client', 'require_locate_units') < 1 then
    GetColumnDataFromServer('client', 'client_id', 'require_locate_units');
  if GetFieldNotNullCount('client', 'parent_client_id') < 1  then
    GetColumnDataFromServer('client', 'client_id', 'parent_client_id');
  if GetFieldNotNullCount('statuslist', 'allow_locate_units') < 1 then
    GetColumnDataFromServer('statuslist', 'status', 'allow_locate_units');
  if GetFieldNotNullCount('statuslist', 'meet_only') < 1 then
    GetColumnDataFromServer('statuslist', 'status', 'meet_only');
  if GetFieldNotNullCount('call_center', 'xml_ticket_format') < 1 then
    GetColumnDataFromServer('call_center', 'cc_code', 'xml_ticket_format');
  if GetFieldNotNullCount('call_center', 'allowed_work') < 1 then
    GetColumnDataFromServer('call_center', 'cc_code', 'allowed_work');
  if GetFieldNotNullCount('employee', 'show_future_tickets') < 1 then
    GetColumnDataFromServer('employee', 'emp_id', 'show_future_tickets');
  if GetFieldNotNullCount('employee', 'local_utc_bias') < 1 then
    GetColumnDataFromServer('employee', 'emp_id', 'local_utc_bias');
  if GetFieldNotNullCount('employee', 'plat_update_date')<1 then
    GetColumnDataFromServer('employee', 'emp_id', 'plat_update_date');
  if GetFieldNotNullCount('upload_location', 'port') < 1 then
    GetColumnDataFromServer('upload_location', 'location_id', 'port');
  PopulateTableFromServerIfEmpty('work_order_work_type');
  if GetFieldNotNullCount('work_order_work_type', 'wo_source') < 1 then     //QMANTWO-446 EB: Adding for older clients
    GetColumnDataFromServer('work_order_work_type', 'work_type_id', 'wo_source');





end;

function TLocalCache.GetTableRowCount(const TableName: string): Integer;
const
  SQL = 'select count(*) from ';
var
  DataSet: TDataSet;
begin
  Assert(NotEmpty(TableName));
  DataSet := OpenQuery(SQL + TableName);
  try
    Result := DataSet.Fields[0].AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TLocalCache.EmptyOpenLocatesTable;
begin
  EmptyTable('open_locate');
end;

procedure TLocalCache.VerifyOpenLocates(EmployeeID: Integer; OldChangesSince, NewChangesSince: string);
var
  NotClosedMissing: TDataSet;
  ClosedLocally: TDataSet;
  MissingLocates: string;
  CountMissingLocates: Integer;
const
  NotClosedSql = 'select * from open_locate where locate_id not in (select locate_id from locate where locator_id=%d and not closed)';
  ClosedLocallySql = 'select * from open_locate where locate_id in (select locate_id from locate where locator_id=%d and closed)';
begin
  NotClosedMissing := OpenQuery(Format(NotClosedSql, [EmployeeID]));
  try
    if NotClosedMissing.EOF then begin
      AddToLog('Verified that all assigned locates are present');
      Exit;
    end;

    MissingLocates := 'OldChangesSince=' + OldChangesSince + ' NewChangesSince=' + NewChangesSince;

    MissingLocates := MissingLocates + ' Missing: ';
    CountMissingLocates := 0;
    while not NotClosedMissing.EOF do begin
      MissingLocates := MissingLocates + NotClosedMissing.FieldByName('locate_id').AsString + ' ';
      Inc(CountMissingLocates);
      NotClosedMissing.Next;
    end;

    ClosedLocally := OpenQuery(Format(ClosedLocallySql, [EmployeeID]));
    try
      MissingLocates := MissingLocates + ' Closed: ';
      while not ClosedLocally.EOF do begin
        MissingLocates := MissingLocates + ClosedLocally.FieldByName('locate_id').AsString + ' ';
        ClosedLocally.Next;
      end;
    finally
      FreeAndNil(ClosedLocally);
    end;

    AddToLog('Missing Locates detected!  Details:');
    AddToLog(MissingLocates);
    AddToLog('This will be reported to the IT department.');

    AddErrorReport(EmployeeID, Now, 'Missing Locates: ' + IntToStr(CountMissingLocates), MissingLocates);
    SendErrorReports;

    raise Exception.Create('MISSING LOCATES: ' + MissingLocates);
  finally
    FreeAndNil(NotClosedMissing);
  end;
end;

procedure TLocalCache.AddErrorReport(EmployeeID: Integer; When: TDateTime;
  ErrorDescription, Details: string);
var
  ErrorData: TDataSet;
begin
  ErrorData := OpenLiveQuery('select * from error_report where 0=1');
  LinkEvents(ErrorData);

  try
    ErrorData.Append;
    ErrorData.FieldByName('error_report_id').AsInteger := GenTempKey;
    ErrorData.FieldByName('emp_id').AsInteger := EmployeeID;
    ErrorData.FieldByName('severity').AsInteger := 5;
    ErrorData.FieldByName('occurred_date').AsDateTime := When;
    ErrorData.FieldByName('error_text').AsString := ErrorDescription;
    ErrorData.FieldByName('details').AsString := Details;
    ErrorData.Post;
  finally
    FreeAndNil(ErrorData);
  end;
end;

procedure TLocalCache.SendErrorReports;
var
  Reports: ErrorReportList;
  ER: ErrorReport;
  DS: TDataSet;
begin
  DS := OpenQuery('SELECT * FROM error_report where deltastatus is not null');

  Reports := ErrorReportList.Create;
  try
    while not DS.EOF do begin
      ER := Reports.Add;
      ER.OccurredWhen := DS.FieldByName('occurred_date').AsDateTime;
      ER.Severity := DS.FieldByName('severity').AsInteger;
      ER.ErrorText := DS.FieldByName('error_text').AsString;
      ER.Details := DS.FieldByName('details').AsString;
      DS.Next;
    end;
    if Reports.Count > 0 then begin
      AddToLog('Sending error reports to server');
      GetChan.OperationName := 'ReportErrors';
      Service.ReportErrors(SecurityInfo, Reports);
      ClearChangeFlagsInTable('error_report');
    end;
  finally
    FreeAndNil(DS);
    FreeAndNil(Reports); // This frees the detail rows
  end;
end;    

procedure TLocalCache.Login(const UserName, Password: string; var EmpID,
  UserID, PasswordChange: Integer; var ExpirationDate: TDateTime; var Role, ShortName: string);
var
  UserInfo: LoginData;
begin
  GetChan.OperationName := 'Login';
  UserInfo := Service.Login(GetSecurityInfoFor(UserName, Password, AppVersion), UserName, Password, AppVersion);
  SetSecurityInfo(UserName, Password);
  EmpID := UserInfo.EmpID;
  UserID := UserInfo.UserID;
  Role := UserInfo.UserRole;
  ShortName := UserInfo.ShortName;
  PasswordChange := UserInfo.NeedPasswordChange;
  ExpirationDate := UserInfo.PasswordExpirationDate;
end;

procedure TLocalCache.Login2(const UserName, Password: string; var EmpID,
  UserID, PasswordChange: Integer; var ExpirationDate: TDateTime; var Role, ShortName, APIKey: string);
var
  UserInfo: LoginData2;
begin
  GetChan.OperationName := 'Login2';
  UserInfo := Service.Login2(GetSecurityInfoFor(UserName, Password, AppVersion), UserName, Password, AppVersion);
  SetSecurityInfo(UserName, Password);
  EmpID := UserInfo.EmpID;
  UserID := UserInfo.UserID;
  Role := UserInfo.UserRole;
  ShortName := UserInfo.ShortName;
  PasswordChange := UserInfo.NeedPasswordChange;
  ExpirationDate := UserInfo.PasswordExpirationDate;
  APIKey := UserInfo.APIKey;
end;

function TLocalCache.LoginAD(const UserName: string; Password: string): boolean;
begin
  GetChan.OperationName := 'LoginAD';
  Result := Service.LoginAD(GetSecurityInfoFor(UserName, Password, AppVersion), UserName);
end;

function TLocalCache.GetSecurityInfoFor(const UserName, Password, AppVersion: string): string;
begin
  Result := UserName + '/' + Password + '/' + AppVersion;
end;



procedure TLocalCache.ProcessSyncResponse(XMLDoc: IXMLDOMDocument);
var
  Nodes: IXMLDOMNodeList;
  Node: IXMLDOMNode;
  AttributeNode: IXMLDOMNode;
  Attribute: IXMLDOMAttribute;
  i: Integer;
  j: Integer;
begin
  Assert(Assigned(FSyncResponseData));
  FSyncResponseData.Clear;
  if Assigned(XMLDoc) then begin
    Nodes := XMLDoc.SelectNodes('//' + SyncResponseDataRow);
    if Assigned(Nodes) then begin
      for i := 0 to Nodes.length - 1 do begin
        Node := Nodes.item[i];
        for j := 0 to Node.attributes.length - 1 do begin
          AttributeNode := Node.attributes.item[j];
          Attribute := AttributeNode as IXMLDOMAttribute;
          FSyncResponseData.Add(Attribute.name + '=' + Attribute.value);
        end;
      end;
    end;
  end;
end;

procedure TLocalCache.AfterCreate;
begin
  FSyncResponseData := TStringList.Create;
  FProfitCentersDirty := True;
end;

function TLocalCache.GetSyncResponseValue(const Name, Default: string): string;
begin
  Result := Default;
  if FSyncResponseData.IndexOfName(Name) >= 0 then
    Result := FSyncResponseData.Values[Name];
end;

procedure TLocalCache.SetSecurityInfo(const UserName, Password: string);
begin
  SecurityInfo := GetSecurityInfoFor(UserName, Password, AppVersion);
end;

function TLocalCache.GetReport(OutputType: string; Params: TStrings): string;
var
  RequestID: string;
  Success: Boolean;
  Details: string;
  StartTime, EndTime: TDateTime;
begin
  RequestID := MakeGUIDString;
  StartTime := Now;

  Success := False;
  try
    try
      Result := Server.GetReport(OutputType, Params, RequestID);
      if Copy(Result, 1, 5) = 'ERROR' then
        raise Exception.Create(Result);

      Success := True;
    except
      on E: Exception do begin
        Details := E.Message;
        raise;
      end;
    end;
  finally
    // Tell the backend how it worked out for us
    EndTime := Now;
    Details := StringReplace(Details, #13#10, '|', [rfReplaceAll]);
    try
      GetChan.OperationName := 'ReportStatus';
      Service.ReportStatus(SecurityInfo, RequestID, Success, EndTime-StartTime, Details);
    except
      // Ignore exceptions for now
    end;
  end;
end;

function TLocalCache.GetReport(OutputType: string; Params: TStrings; OutputParams: TStrings): string;
begin
  Result := GetReport(OutputType, Params);
  Result := ParseResult(Result, OutputParams);
end;

procedure TLocalCache.StartReport(OutputType: string; Params: TStrings);
var
  RequestID: string;
  Seconds: Integer;
  ReportQueue: TDataSet;
begin
  RequestID := MakeGUIDString;
  Params.Delimiter := '|';
  Service.StartReport(SecurityInfo, RequestID, Params.DelimitedText, Seconds);

  ReportQueue := OpenLiveQuery('select * from report_log where 0=1');
  try
    ReportQueue.Append;
    ReportQueue.FieldByName('request_id').AsString := RequestID;
    ReportQueue.FieldByName('report_name').AsString := Params.Values['Report'];
    ReportQueue.FieldByName('request_date').AsDateTime := Now;
    ReportQueue.FieldByName('status').AsString := 'queued';
    ReportQueue.Post;
  finally
    FreeAndNil(ReportQueue);
  end;
end;

function TLocalCache.GetArrayOfTicketInCache(EmployeeID: Integer): IntArray;
var
  D: TDataSet;
  Tickets: IntArray;
  i: Integer;
const
  SqlTemplate = 'select ticket_id, modified_date from ticket where ticket_id in (select ticket_id from locate where locator_id=%d and (closed=false or closed_date>''%s''))';
begin
  SetLength(Tickets, 10000);
  D := OpenQuery(Format(SqlTemplate, [EmployeeID, IsoDateToStr(Date-10)]));
  i := 0;
  try
    while (not D.EOF) and (i < 10000) do begin
      Tickets[i] := D.Fields[0].AsInteger;
      Inc(i);
      D.Next;
    end;
  finally
    FreeAndNil(D);
  end;
  SetLength(Tickets, i);

  AddToLog('Assigned Tickets in cache: ' + IntToStr(i));
  Result := Tickets;
end;

function TLocalCache.GetArrayOfDamageInCache(EmployeeID: Integer): IntArray;
var
  D: TDataSet;
  Damages: IntArray;
  i: Integer;
const
  SqlTemplate = 'select damage_id, modified_date from damage where investigator_id=%d and damage_type = ''PENDING'' and (closed_date is NULL or closed_date>''%s'')';
begin
  SetLength(Damages, 10000);
  D := OpenQuery(Format(SqlTemplate, [EmployeeID, IsoDateToStr(Date-10)]));
  i := 0;
  try
    while (not D.EOF) and (i < 10000) do begin
      Damages[i] := D.Fields[0].AsInteger;
      Inc(i);
      D.Next;
    end;
  finally
    FreeAndNil(D);
  end;
  SetLength(Damages, i);

  AddToLog('Assigned Damages in cache: ' + IntToStr(i));
  Result := Damages;
end;

function TLocalCache.GetArrayOfWorkOrderInCache(EmployeeID: Integer): IntArray;
var
  D: TDataSet;
  WorkOrders: IntArray;
  i: Integer;
const
  SqlTemplate = 'select wo_id, modified_date from work_order where assigned_to_id=%d and closed=false and (closed_date is NULL or closed_date>''%s'')';
begin
  SetLength(WorkOrders, 10000);
  D := OpenQuery(Format(SqlTemplate, [EmployeeID, IsoDateToStr(Date-10)]));
  i := 0;
  try
    while (not D.EOF) and (i < 10000) do begin
      WorkOrders[i] := D.Fields[0].AsInteger;
      Inc(i);
      D.Next;
    end;
  finally
    FreeAndNil(D);
  end;
  SetLength(WorkOrders, i);

  AddToLog('Assigned Work Orders in cache: ' + IntToStr(i));
  Result := WorkOrders;

end;

function TLocalCache.RunSQLReturnS(const SQL: string): string;
begin
  with OpenQuery(SQL) do try
    Result := FieldByName('S').AsString;
  finally
    Free;
  end;
end;

procedure TLocalCache.UpdatePreCalculatedFields(const TableName: string);
begin
  if StringInArray(TableName, ['employee', 'employee_right', 'emp_group_right']) then
    FProfitCentersDirty := True;
end;

function TLocalCache.AddFieldData(List: NVPairList; Field: TField; IncludeNull: Boolean): NVPair;
begin
  Result := nil;
  Assert(Assigned(List));
  Assert(Assigned(Field));
  if (not IncludeNull) and Field.IsNull then
    Exit;
  Result := List.Add;
  Result.Name := Field.FieldName;
  Result.Value := Field.AsString;
  if (not Field.IsNull) and (Field is TDateTimeField) then
    Result.Value := IsoDateTimeToStr(Field.AsDateTime)
end;



procedure TLocalCache.AddProjPendingAssignment(TicketID, ClientID, TempLocateID, AssignedToID: Integer);
//QMANTWO-616 EB Multi locates on Project Ticket
var
  CurLength: integer;
begin
  CurLength := Length(fProjAssignments);
  Inc(CurLength);
  SetLength(fProjAssignments, CurLength);
  fProjAssignments[CurLength-1].LocateID := TempLocateID;   {EB - COming in this will be temporary locateID}
  fProjAssignments[CurLength-1].AssignTo := AssignedToID;
  fProjAssignments[CurLength-1].TicketID := TicketID;
  fProjAssignments[CurLength-1].ClientID := ClientID;
end;

procedure TLocalCache.SaveProjectPendingAssignments;
{QMANTWO-616 EB Project Assignments}
const
  LookupQryStr = 'Select locate_id from locate where (ticket_id = %d) and (client_id = %d)';
var
  i, RecordCount: integer;
  SQL: string;
  NewLocData: TDataSet;
begin
  RecordCount := Length(fProjAssignments);
  for i := 0 to RecordCount - 1 do begin
    SQL := Format(LookupQryStr, [fProjAssignments[i].TicketID, fProjAssignments[i].ClientID]);
    NewLocData := OpenQuery(SQL);
    if not NewLocData.EOF then begin
      fProjAssignments[i].LocateID := NewLocData.FieldByName('locate_id').AsInteger;
      Service.AssignProjLocate(SecurityInfo, fProjAssignments[i].LocateID, fProjAssignments[i].AssignTo);
      NewLocData.Close;
    end;
  end;
  SetLength(fProjAssignments, 0);
end;

procedure TLocalCache.ProcessPendingAssignments;
var
  i: Integer;
  Locates: LocateList;
  Locate: LocateKey;
  Table: TDataSet;
  OldLocateID: Integer;
  AssignToID: Integer;
begin
  if Length(FPendingAssignments) = 0 then
    Exit;
  Table := nil;
  Locates := LocateList.Create;
  try
    Locate := Locates.Add;
    Table := OpenWholeTable('Locate');
    for i := Low(FPendingAssignments) to High(FPendingAssignments) do begin
      OldLocateID := FPendingAssignments[i].LocateID;
      if not Table.Locate('LocalStringKey', OldLocateID, []) then
        raise Exception.CreateFmt('Unable to find new pending ticket locate %d', [OldLocateID]);

      Locate.TicketID := Table.FieldByName('ticket_id').AsInteger;
      Locate.ClientCode := Table.FieldByName('client_code').AsString;
      Assert(Locate.TicketID > 0);
      Assert(Locate.ClientCode <> '');

      AssignToID := FPendingAssignments[i].AssignTo;
      Assert(AssignToID > 0);
      Service.MoveLocates(SecurityInfo, Locates, AssignToID);
    end;
  finally
    FreeAndNil(Locates);
    FreeAndNil(Table);
  end;
  SetLength(FPendingAssignments, 0);
end;

procedure TLocalCache.RecordPendingAssignments;
const
  SQL = 'select * from locate where ticket_id < 0 and locate_id < 0 and assigned_to > 0';
var
  DS: TDataSet;
  i: Integer;
begin
  DS := OpenQuery(SQL);
  try
    i := 0;
    SetLength(FPendingAssignments, DS.RecordCount);
    while not DS.Eof do begin
      Assert(i < DS.RecordCount);
      FPendingAssignments[i].LocateID := DS.FieldByName('locate_id').AsInteger;
      FPendingAssignments[i].AssignTo := DS.FieldByName('assigned_to').AsInteger;
      DS.Next;
      Inc(i);
    end;
  finally
    FreeAndNil(DS);
  end;
end;

procedure TLocalCache.GetBillingAdjustmentDataFromServer(AdjustmentID: Integer);
var
  Xml: string;
begin
  Assert(AdjustmentID > 0);
  GetChan.OperationName := 'GetBillingAdjustment';
  Xml := Service.GetBillingAdjustment(SecurityInfo, AdjustmentID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.StartBillingOnServer(Period: TBillPeriod; CenterGroupIDs: string);
var
  Params: NVPairList;
begin
  GetChan.OperationName := 'StartBilling';
  Params := NVPairList.Create;
  try
    AddParam(Params, 'Span', Period.Span);
    AddParam(Params, 'PeriodEndDate', IsoDateTimeToStr(Period.EndDate));
    AddParam(Params, 'CallCenterGroupID', CenterGroupIDs);
    Service.StartBilling(SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

procedure TLocalCache.CancelBillingOnServer(RunID, CallCenterGroupID: Integer);
var
  Params: NVPairList;
begin
  Assert(RunID > 0);
  GetChan.OperationName := 'CancelBilling';
  Params := NVPairList.Create;
  try
    AddParam(Params, 'run_id', RunID);
    Service.CancelBillingRun(SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

procedure TLocalCache.ClearAreaChangedFlags;
begin
  ExecQuery('UPDATE ticket SET area_changed_date=NULL WHERE area_changed_date is not null');
end;

procedure TLocalCache.SendDamage(Errors: TObjectList);
var
  ChangeList: NVPairListList;
  AddinInfoChanges: AddinInfoChangeList;
  NotesChanges: NotesChangeList5;
  ReturnedKeys: GeneratedKeyList;
  SyncErrors: SyncErrorList;
begin
  Assert(Assigned(Errors), 'Errors not defined');
  SyncErrors := SyncErrorList.Create;
  ChangeList := CreateChangeList('damage', 'damage_id', True);
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage changes to server');
      GetChan.OperationName := 'SaveDamage3';
      ReturnedKeys := Service.SaveDamage3(SecurityInfo, ChangeList, SyncErrors);
      AddToSyncErrors(SyncErrors, Errors);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'damage')]));
    end;
  finally
    FreeAndNil(ChangeList);
    FreeAndNil(SyncErrors);
  end;

  ChangeList := CreateChangeList('damage_bill', 'damage_bill_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage bill changes to server');
      GetChan.OperationName := 'SaveDamageBill';
      ReturnedKeys := Service.SaveDamageBill(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'damage bill')]));
    end;
  finally
    FreeAndNil(ChangeList);
  end;

  ChangeList := CreateChangeList('damage_litigation', 'litigation_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage litigation changes to server');
      GetChan.OperationName := 'SaveDamageLitigation';
      ReturnedKeys := Service.SaveDamageLitigation(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'damage litigation')]));
    end;
  finally
    FreeAndNil(ChangeList);
  end;

  ChangeList := CreateChangeList('damage_third_party', 'third_party_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage third party changes to server');
      GetChan.OperationName := 'SaveDamageThirdParty';
      ReturnedKeys := Service.SaveDamageThirdParty(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'damage third party')]));
    end;
  finally
    FreeAndNil(ChangeList);
  end;

  SyncErrors := SyncErrorList.Create;
  ChangeList := CreateChangeList('damage_estimate', 'estimate_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage estimate changes to server');
      GetChan.OperationName := 'SaveDamageEstimate2';
      ReturnedKeys := Service.SaveDamageEstimate2(SecurityInfo, ChangeList, SyncErrors);
      AddToSyncErrors(SyncErrors, Errors);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'estimate')]));
    end;
  finally
    FreeAndNil(ChangeList);
    FreeAndNil(SyncErrors);
  end;

  ChangeList := CreateChangeList('damage_invoice', 'invoice_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending damage invoice changes to server');
      GetChan.OperationName := 'SaveDamageInvoice';
      ReturnedKeys := Service.SaveDamageInvoice(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [ChangeList.Count,
        AddSIfNot1(ChangeList.Count, 'damage invoice')]));
    end;
  finally
    FreeAndNil(ChangeList);
  end;

  NotesChanges := CreateNotesChangeList(qmftDamage, 0);
  try
    if NotesChanges.Count > 0 then begin
      AddToLog('Sending damage note changes to server');
      GetChan.OperationName := 'SaveNewNotes5';
      ReturnedKeys := Service.SaveNewNotes5(SecurityInfo, NotesChanges);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [NotesChanges.Count,
        AddSIfNot1(NotesChanges.Count, 'damage note')]));
    end;
  finally
    FreeAndNil(NotesChanges);
  end;

  AddinInfoChanges := CreateAddinInfoChangeList(qmftDamage, 0);
  try
    if AddinInfoChanges.Count > 0 then begin
      AddToLog('Sending damage addin info changes to server');
      GetChan.OperationName := 'SaveAddinInfo2';
      ReturnedKeys := Service.SaveAddinInfo2(SecurityInfo, AddinInfoChanges);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
      AddToLog(Format('Changes to %d %s sent', [AddinInfoChanges.Count,
        AddSIfNot1(AddinInfoChanges.Count, 'damage addin info')]));
    end;
  finally
    FreeAndNil(AddinInfoChanges);
  end;

  SaveRequiredDocuments;
end;



function TLocalCache.LoadDataSetFromXml(XML: string; const TableName: string;
  PrimaryKeys: TStringList; EmptyTableFirst: Boolean): Integer;    //QMANTWO-302
const
  DBLReturn = #13#10 + #13#10;
var
  XmlDoc: IXMLDOMDocument;
  DS : TDataSet;

begin
  Result := 0;
  if Xml = '' then
    Exit;
  if EmptyTableFirst then
    EmptyTable(TableName);
  XMLDoc := CoDOMDocument.Create;
  DS := OpenWholeTable(TableName);

  XMLDoc.async := False;
  if not XMLDoc.loadXML(Xml) then
    raise Exception.Create('Invalid XML document in LoadDataSetFromXml.' + DBLReturn + XML);

  try
    Result := UpdateDataSetFromXml(XMLDoc, DS, TableName, PrimaryKeys);
  finally
    FreeAndNil(DS);
  end;
end;

procedure TLocalCache.LoadROChangeArray(ROArray: TROArray;
  const TableName, IDFieldName: string; IncludeModifiedDate: Boolean = False;
  const ExtraWhereClause: string = '');
var
  D: TDataSet;
  SQL: string;
begin
  SQL := Format('select * from %s where (%s < 0 or DeltaStatus = ''U'')', [TableName, IDFieldName]);
  if NotEmpty(ExtraWhereClause) then
    SQL := SQL + ' and (' + ExtraWhereClause + ')';
  D := OpenQuery(SQL);
  try
    LoadROArrayFromDataSet(ROArray, D, IncludeModifiedDate);
  finally
    FreeAndNil(D);
  end;
end;

function TLocalCache.CreateChangeList(const TableName, IDFieldName: string; IncludeModifiedDate: Boolean; const ExtraWhereClause: string): NVPairListList;
var
  D: TDataSet;
  SQL: string;
begin
  SQL := Format('select * from %s where (%s < 0 or DeltaStatus = ''U'')', [TableName, IDFieldName]);
  if NotEmpty(ExtraWhereClause) then
    SQL := SQL + ' and (' + ExtraWhereClause + ')';
  D := OpenQuery(SQL);
  try
    Result := CreateChangeListFromDataSet(D, IncludeModifiedDate);
  finally
    FreeAndNil(D);
  end;
end;

function TLocalCache.CreateChangeListFromDataSet(Dataset: TDataSet; IncludeModifiedDate: Boolean): NVPairListList;
var
  ChangeList: NVPairList;
  i: Integer;
  Field: TField;
begin
  Result := NVPairListList.Create;
  if not DataSet.IsEmpty then begin
    while not DataSet.Eof do begin
      ChangeList := Result.Add;
      for i := 0 to DataSet.FieldCount - 1 do begin
        Field := DataSet.Fields[i];
        if IncludeModifiedDate then begin
          if not StringInArray(Field.FieldName, NeverSendFieldNamesNoModifiedDate) then
            AddFieldData(ChangeList, Field);
        end
        else if not StringInArray(Field.FieldName, NeverSendFieldNames) then
          AddFieldData(ChangeList, Field);
      end;
      DataSet.Next;
    end;
  end;
end;

procedure TLocalCache.AckMessages;
var
  ChangeList: NVPairListList;
  ReturnValue: string;
begin
  ChangeList := CreateChangeList('message_dest', 'message_dest_id');
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Sending message acks to server');
      GetChan.OperationName := 'AckMessage2';
      Service.AckMessage2(SecurityInfo, ChangeList, ReturnValue);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

procedure TLocalCache.GetMessageRecipFromServer(MessageID: Integer);
var
  Xml: string;
begin
  Assert(MessageID > 0);
  GetChan.OperationName := 'GetMessageRecip';
  EmptyTable('message');
  EmptyTable('message_recip');
  Xml := Service.GetMessageRecip(SecurityInfo, MessageID);
  SyncTablesFromXml(ParseXml(Xml));
end;

procedure TLocalCache.GetRightsForEmp(EmpID: Integer);
var
  RightsXML: string;
begin
  GetChan.OperationName := 'GetRights';
  RightsXML := Service.GetRights(SecurityInfo, EmpID);
  SyncTablesFromXmlString(RightsXML);
end;



procedure TLocalCache.SaveAssetsForEmployee(EmpID: Integer; DataSet: TDataSet);
var
  Assets: NVPairListList;
  AssetItem: NVPairList;
  i: Integer;
  Field: TField;
begin
  Assert(EmpID > 0);
  Assert(Assigned(DataSet) and DataSet.Active);
  Assets := NVPairListList.Create;
  try
    DataSet.First;
    while not DataSet.Eof do begin
      AssetItem := Assets.Add;
      for i := 0 to DataSet.FieldCount - 1 do begin
        Field := DataSet.Fields[i];
        if not StringInArray(Field.FieldName, NeverSendFieldNames) then
          AddFieldData(AssetItem, Field, True);
        if Field.FieldName = 'emp_id' then
          Assert(Field.Value = EmpID);
        if StringInArray(Field.FieldName, ['asset_code', 'asset_number']) then
          Assert(VarToStr(Field.Value) <> '');
      end;
      DataSet.Next;
    end;
    Service.SaveEmployeeAssets2(SecurityInfo, EmpID, Assets);
  finally
    FreeAndNil(Assets);
  end;
end;

procedure TLocalCache.ReassignAsset(const AssetNum, AssetCode: string; AssetID, DestEmpID: Integer);
var
  Assets: NVPairListList;
  Asset: NVPairList;
begin
  Assets := NVPairListList.Create;
  try
    Asset := Assets.Add;
    AddParam(Asset, 'asset_id', AssetID);
    AddParam(Asset, 'asset_number', AssetNum);
    AddParam(Asset, 'emp_id', DestEmpID);
    AddParam(Asset, 'asset_code', AssetCode);
    // This handles multiple assets, if we ever need it
    Service.ReassignAssets(SecurityInfo, Assets);
  finally
    FreeAndNil(Assets);
  end;
end;

function TLocalCache.ParseResult(Report: string; OutputParams: TStrings): string;
var
  PosTextMarker: Integer;
begin
{ TODO -oLarry -cReports : Clean Report String so that END_OF_QM_REPORT_PARAMS matches.  Resport String has # chars in it. }
  PosTextMarker := Pos(END_OF_QM_REPORT_PARAMS, Report);
  if PosTextMarker <> 0 then begin
    OutputParams.Text := Copy(Report, 0, PosTextMarker - 1);
    Delete(Report, 1, PosTextMarker + Length(END_OF_QM_REPORT_PARAMS) - 1);
    Result := Report;
  end
  else
    Result := Report;
end;

procedure TLocalCache.SendEmployeeActivity;
var
  ChangeList: EmployeeActivityChangeList3;
  ReturnedKeys: GeneratedKeyList;
begin
  ChangeList := EmployeeActivityChangeList3.Create;
  try
    LoadROChangeArray(ChangeList, 'employee_activity', 'emp_activity_id');
    if ChangeList.Count > 0 then begin
      AddToLog('Saving activity records...');
      GetChan.OperationName := 'SaveEmployeeActivity3';
      ReturnedKeys := Service.SaveEmployeeActivity3(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

procedure TLocalCache.UpdateRestrictedUseExemption(EmployeeID: Integer; Revoke: Boolean);
var
  Exemptions: NVPairListList;
  Exemption: NVPairList;
begin
  Exemptions := NVPairListList.Create;
  try
    Exemption := Exemptions.Add;
    AddParam(Exemption, 'emp_id', EmployeeID);
    AddParam(Exemption, 'effective_date', Now);
    AddParam(Exemption, 'revoke', Integer(Revoke));
    // Ready to handle multiple exemptions, but currently does 1 at a time
    Service.UpdateRestrictedUseExemption(SecurityInfo, Exemptions);
  finally
    FreeAndNil(Exemptions);
  end;
end;

function TLocalCache.CreateJobsiteArrivalChanges(const TicketID: Integer): JobsiteArrivalChangeList;
const
  Select = 'select * from jobsite_arrival where ((arrival_id < 0) or (DeltaStatus is not null)) ' +
    'and (ticket_id=%d)';
var
  JobsiteArrival: TDataSet;
begin
  Result := JobsiteArrivalChangeList.Create;
  JobsiteArrival := OpenQuery(Format(Select, [TicketID]));
  try
    LoadROArrayFromDataSet(Result, JobsiteArrival);
  finally
    FreeAndNil(JobsiteArrival);
  end;
end;

function TLocalCache.CreateLocateFacilityChanges(const TicketID: Integer): LocateFacilityChangeList;
const
  Select = 'select lf.* from locate_facility lf ' +
    'inner join locate l on lf.locate_id = l.locate_id ' +
    'where (locate_facility_id < 0 or lf.DeltaStatus is not null) and ticket_id=';
var
  LocateFacility: TDataSet;
begin
  Result := LocateFacilityChangeList.Create;
  LocateFacility := OpenQuery(Select + IntToStr(TicketID));
  try
    LoadROArrayFromDataSet(Result, LocateFacility);
  finally
    FreeAndNil(LocateFacility);
  end;
end;

function TLocalCache.CreateTaskScheduleChanges: TaskScheduleChangeList;
begin
  Result := TaskScheduleChangeList.Create;
  LoadROChangeArray(Result, 'task_schedule', 'task_schedule_id');
end;

procedure TLocalCache.SendTaskScheduleChanges;
var
  ChangeList: TaskScheduleChangeList;
  ReturnedKeys: GeneratedKeyList;
begin
  ChangeList := CreateTaskScheduleChanges;
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Saving task schedule changes...');
      GetChan.OperationName := 'SaveTaskSchedules2';
      ReturnedKeys := Service.SaveTaskSchedule2(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

procedure TLocalCache.SendTextToLocator(EmpID: integer; TxtMsg: String);
begin
    GetChan.OperationName := 'SendTextViaEmail';
    Service.SendTextViaEmail(SecurityInfo, EmpID, TxtMsg);
end;

procedure TLocalCache.GetAttachmentList(const ForeignType, ForeignID: Integer; AttachmentDataset: TDBISAMTable; ImagesOnly: Boolean);
var
  DOM: IXMLDOMDocument;
  FColumns: TColumnList;
begin
  GetChan.OperationName := 'GetAttachmentList';
  FColumns := TColumnList.Create;
  try
    FColumns.SetDefaultTable('attachment');
    FColumns.AddDefaultTableColumns;
    DOM := ParseXML(Service.GetAttachmentList(SecurityInfo, ForeignType, ForeignID, ImagesOnly));
    ConvertXMLToDataSet(DOM, FColumns , AttachmentDataset);
  finally
    FreeAndNil(FColumns);
  end;
end;

function TLocalCache.GetTicketID(const TicketNumber: string; const CallCenter: string; const TransmitDateFrom, TransmitDateTo: TDateTime; const DamageID: Integer): Integer;
begin
  GetChan.OperationName := 'GetTicketId';
  Result := Service.GetTicketID(SecurityInfo, TicketNumber, CallCenter, TransmitDateFrom, TransmitDateTo, DamageID);
end;

function TLocalCache.GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
begin
  GetChan.OperationName := 'GetDamageIDForUQDamageID';
  Result := Service.GetDamageIDForUQDamageID(SecurityInfo, UQDamageID);
end;

procedure TLocalCache.UpdateMyMapAreas;
begin
  EmptyTable(TableEditableAreas);
  GetTableDataFromServer(TableEditableAreas);
end;

procedure TLocalCache.SendAreaChanges;
var
  ChangeList: AreaChangeList;
begin
  ChangeList := AreaChangeList.Create;
  try
    LoadROChangeArray(ChangeList, TableEditableAreas, 'area_id');
    if ChangeList.Count > 0 then begin
      AddToLog('Saving area changes...');
      GetChan.OperationName := 'SaveAreas2';
      Service.SaveAreas2(SecurityInfo, ChangeList);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

procedure TLocalCache.GetPrintableAttachmentList(const ForeignType, ForeignID: Integer; AttachmentDataset: TDBISAMTable);
var
  DOM: IXMLDOMDocument;
  FColumns: TColumnList;
begin
  GetChan.OperationName := 'GetPrintableAttachmentList';
  FColumns := TColumnList.Create;
  try
    FColumns.SetDefaultTable('attachment');
    FColumns.AddDefaultTableColumns;
    DOM := ParseXML(Service.GetPrintableAttachmentList(SecurityInfo, ForeignType, ForeignID));
    ConvertXMLToDataSet(DOM, FColumns , AttachmentDataset);
  finally
    FreeAndNil(FColumns);
  end;
end;

function TLocalCache.CreateAddinInfoChangeList(const ForeignType, ForeignID: Integer): AddinInfoChangeList;
const
  Select = 'select * from addin_info ' +
    'where (addin_info_id < 0 or DeltaStatus is not null) ' +
    'and foreign_type = %d';
var
  AddinInfo: TDataSet;
begin
  Result := AddinInfoChangeList.Create;
  Assert(ForeignType > 0, 'ForeignType must be specified');

  if ForeignID > 0 then
    AddinInfo := OpenQuery(Format(Select + ' and foreign_id = %d', [ForeignType, ForeignID]))
  else  // get all unsynced rows for the specified ForeignType
    AddinInfo := OpenQuery(Format(Select, [ForeignType]));

  try
    LoadROArrayFromDataSet(Result, AddinInfo);
  finally
    FreeAndNil(AddinInfo);
  end;
end;

function TLocalCache.CreateNotesChangeList(const ForeignType, ForeignID: Integer): NotesChangeList5;
const
  Select = 'select * from notes ' +
    'where (notes_id < 0 or DeltaStatus is not null) ' +
    'and foreign_type = %d';
var
  Notes: TDataSet;
begin
  Result := NotesChangeList5.Create;
  Assert(ForeignType > 0, 'ForeignType must be specified');

  if ForeignID > 0 then
    Notes := OpenQuery(Format(Select + ' and foreign_id = %d', [ForeignType, ForeignID]))
  else  // get all unsynced rows for the specified ForeignType
    Notes := OpenQuery(Format(Select, [ForeignType]));

  try
    LoadROArrayFromDataSet(Result, Notes);
  finally
    FreeAndNil(Notes);
  end;
end;

// This sends arrivals without any other data, so the sending of arrival data
// immediately after arrival is as fast as possible
procedure TLocalCache.SendArrivalChanges(const ArrivalType: TArrivalType);
var
  ChangeList: JobsiteArrivalChangeList;
  ReturnedKeys: GeneratedKeyList;
  ExtraWhere: String;
begin
  if ArrivalType = atTicket then
    ExtraWhere := '(ticket_id > 0 and DeltaStatus is not null)'
  else
    ExtraWhere := '(damage_id > 0 and DeltaStatus is not null)';

  // Note: We only send for tickets that already exist on the server (no new manual ticket arrivals)
  // A bug prior to .8939 left lots of negative id's with a null DeltaStatus, so don't try to resync those
  ChangeList := JobsiteArrivalChangeList.Create;
  try
    LoadROChangeArray(ChangeList, 'jobsite_arrival', 'arrival_id', False, ExtraWhere);
    if ChangeList.Count > 0 then begin
      AddToLog('Sending arrivals...');
      GetChan.OperationName := 'SaveJobsiteArrivals2';
      ReturnedKeys := Service.SaveJobsiteArrivals2(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      // We only clear for the new inserts we sent (not the whole table, in case of new arrivals on a new manual ticket)
      ClearChangeFlagsForKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

procedure TLocalCache.ClearChangeFlagsForKeys(KeyList: GeneratedKeyList);
var
  i: Integer;
  Key: GeneratedKey;
begin
  Assert(Assigned(KeyList));
  for i := 0 to KeyList.Count - 1 do begin
    Key := KeyList.Items[i];
    Assert(Assigned(Key));
    ClearChangeFlagForKey(Key);
  end;
end;

procedure TLocalCache.ClearChangeFlagForKey(Key: GeneratedKey);
var
  TableName: string;
  KeyField: string;
  KeyValue: Integer;
  Records: Integer;
  SQL: string;
begin
  Assert(Assigned(Key));
  TableName := Key.TableName;
  KeyField := GetPrimaryKeyFieldForTable(TableName);
  Assert(NotEmpty(KeyField), 'KeyField is empty');
  KeyValue := Key.KeyValue;
  SQL := Format('update %s set DeltaStatus = null where %s = %d', [TableName, KeyField, KeyValue]);
  Records := ExecQuery(SQL);
  if Records <> 1 then
    raise Exception.CreateFmt('Invalid number of records changed in ClearChangeFlagForKey: %d', [Records]);
end;

function TLocalCache.GetPrimaryKeyFieldForTable(const TableName: string): string;
begin
  GetSyncStatusTable;
  if not GetSyncStatusTable.Locate('TableName', TableName, []) then
    raise Exception.Create('There is no table, "' + TableName + '," in the table list');
  Result := GetSyncStatusTable.FieldByName('PrimaryKey').AsString;
end;

procedure TLocalCache.ClearDeltaStatusInCurrentRow(DataSet: TDataSet);
var
  OldTag: Integer;
begin
  Assert(Assigned(DataSet));
  Assert(not DataSet.IsEmpty);
  EditDataSet(DataSet);
  DataSet.FieldByName('DeltaStatus').Clear;
  OldTag := DataSet.Tag;
  DataSet.Tag := DisableDeltaStatusTag;  // Don't re-mark this as updated
  try
    DataSet.Post;
  finally
    DataSet.Tag := OldTag;
  end;
end;

function TLocalCache.AckTicketActivitiesForManager(ManagerID: Integer; CutoffDate: TDateTime): Integer;
var
  AckTicketActivities: NVPairListList;
  AckTicketActivity: NVPairList;
begin
  AckTicketActivities := NVPairListList.Create;
  try
    AckTicketActivity := AckTicketActivities.Add;
    AddParam(AckTicketActivity, 'manager_id', ManagerID);
    AddParam(AckTicketActivity, 'cutoff_date', CutoffDate);
    Result := Service.AckAllTicketActivitiesForManager(SecurityInfo, AckTicketActivities);
  finally
    FreeAndNil(AckTicketActivities);
  end;
end;

function TLocalCache.GetUnackedTicketActivityCountForManager(ManagerID: Integer; CutoffDate: TDateTime): Integer;
var
  Params: NVPairList;
begin
  Params := NVPairList.Create;
  try
    AddParam(Params, 'manager_id', ManagerID);
    AddParam(Params, 'cutoff_date', CutoffDate);
    Result := Service.GetUnackedTicketActivityCount(SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

function TLocalCache.CreateBreakRuleResponseChanges: BreakRuleResponseList;
begin
  Result := BreakRuleResponseList.Create;
  LoadROChangeArray(Result, 'break_rules_response', 'response_id');
end;

procedure TLocalCache.SendBreakRuleResponses;
var
  ChangeList: BreakRuleResponseList;
  ReturnedKeys: GeneratedKeyList;
begin
  ChangeList := CreateBreakRuleResponseChanges;
  try
    if ChangeList.Count > 0 then begin
      AddToLog('Saving break rule responses...');
      GetChan.OperationName := 'SaveBreakRuleResponse2';
      ReturnedKeys := Service.SaveBreakRuleResponses2(SecurityInfo, ChangeList);
      SaveGeneratedKeys(ReturnedKeys);
      FreeAndNil(ReturnedKeys);
    end;
  finally
    FreeAndNil(ChangeList);
  end;
end;

function TLocalCache.GetAddinFileVersion: string;
var
  IniFileName: string;
  FileName: string;
  IniFile: TIniFile;
begin
  Result := '';
  IniFileName := GetClientAddinsFile;
  if not FileExists(IniFileName) then
    Exit;

  IniFile := TIniFile.Create(IniFileName);
  try
    FileName := Trim(IniFile.ReadString('TicketAttachments', 'FileName', ''));
  finally
    FreeAndNil(IniFile);
  end;
  Result := GetFileVersion(FileName);
end;

function TLocalCache.GetFileVersion(const FileName: string): string;
var
  VersionString: string;
begin
  Result := '';
  if FileExists(FileName) and GetFileVersionNumberString(FileName, VersionString) then
    Result := VersionString;
end;

procedure TLocalCache.SendAttachmentUploadStats(const EmpID: Integer);
var
  UploadStatistics: PendingUploadsStats;
  AttachSummary: TAttachmentUploadSummary;
begin
  AddToLog('Saving attachment upload statistics...');
  GetChan.OperationName := 'SaveUploadQueueStats';
  AttachSummary := nil;
  UploadStatistics := PendingUploadsStats.Create;
  try
    AttachSummary := TAttachmentUploadSummary.Create(FDatabase.DatabaseName, FDatabase.SessionName, EmpID);
    AttachmentDataCS.Enter;
    try
      AttachSummary.GetAttachmentUploadSummary;
    finally
      AttachmentDataCS.Leave;
    end;
    UploadStatistics.FileCount := AttachSummary.PendingUploadCount;
    UploadStatistics.FileTotalKB := AttachSummary.PendingUploadSize;
    UploadStatistics.LongestWaitMinutes := RoundUp(AttachSummary.LongestFileWaitSeconds / 60);
    Service.SaveUploadQueueStats(SecurityInfo, UploadStatistics);
    // Anything to do if it fails?
  finally
    FreeAndNil(AttachSummary);
    FreeAndNil(UploadStatistics);
  end;
end;

function TLocalCache.GetAirCardPhoneNo: string;
begin
  //Will eventually populate this with the solution to mantis item 2576
  //For now just returning NA for testing purposes in Developer Mode
  Result := DeviceRecord.AirCardPhone;
  if DeveloperMode then
    Result := 'NA';
end;

function TLocalCache.GetAirCardESN: string;
begin
  //Will eventually populate this with the solution to mantis item 2576
  //For now just returning NA for testing purposes in Developer Mode
  Result := DeviceRecord.AirCardDeviceID;
  if DeveloperMode then
    Result := 'NA' ;
end;

function TLocalCache.CreateTicketGPSChanges(TicketID: Integer): GPSChangeList;
const
  Select = 'select gps.* from gps_position gps ' +
    'inner join locate l on gps.gps_id = l.gps_id ' +
    'where (gps.gps_id < 0 or gps.DeltaStatus is not null) and l.ticket_id=';
var
  GPSData: TDataSet;
  GPS: GPSChange;
begin
  GPSData := OpenQuery(Select + IntToStr(TicketID));
  try
    Result := GPSChangeList.Create;
    while not GPSData.EOF do begin
      GPS := Result.Add;
      GPS.GPSID := GPSData.FieldByName('gps_id').AsInteger; // a local key
      GPS.AddedBy := GPSData.FieldByName('added_by_id').AsInteger;
      GPS.AddedDate := GPSData.FieldByName('added_date').AsDateTime;
      GPS.Latitude := GPSData.FieldByName('latitude').AsFloat;
      GPS.Longitude := GPSData.FieldByName('longitude').AsFloat;
      GPS.HDOPFeet := GPSData.FieldByName('hdop_feet').AsInteger;
      GPSData.Next;
    end;
  finally
    FreeAndNil(GPSData);
  end;
end;

procedure TLocalCache.AddToSyncErrors(ROSyncErrors: SyncErrorList; SyncErrors: TObjectlist);
var
  Err: TSyncError;
  I: Integer;
begin
  Assert(Assigned(ROSyncErrors), 'ROSyncErrors is not defined');
  Assert(Assigned(SyncErrors), 'SyncErrors is not defined');
  for I := 0 to ROSyncErrors.Count - 1 do begin
    Err := TSyncError.Create(ROSyncErrors[I].TableName, ROSyncErrors[I].PrimaryKeyID, ROSyncErrors[I].Message);
    SyncErrors.Add(Err);
  end;
end;

procedure TLocalCache.RemoveTicketFromCache(const ID: Integer);
const
  DeleteLocateNotes = 'delete from notes where foreign_type = %d and ' +
    'foreign_id in (select locate_id from locate where ticket_id = %d)';
  DeleteByForeignID = 'delete from %s where foreign_type = %d and foreign_id = %d';
  DeleteLocateDetail = 'delete from %s where locate_id in (select locate_id ' +
    'from locate where ticket_id = %d)';
  DeleteByTicketID = 'delete from %s where ticket_id = %d';
begin
  { General purpose way to totally remove a local ticket, because we are about
    to reload it from the server with TLocalCache.GetTicketDataFromServer. We
    aren't including the attachment table, to avoid conflicts with background
    uploading. }
  FDatabase.StartTransaction;
  try
    ExecQuery(Format(DeleteLocateNotes, [qmftLocate, ID]));
    ExecQuery(Format(DeleteByForeignID, ['notes', qmftTicket, ID]));

    ExecQuery(Format(DeleteLocateDetail, ['locate_hours', ID]));
    ExecQuery(Format(DeleteLocateDetail, ['locate_plat', ID]));
    ExecQuery(Format(DeleteLocateDetail, ['locate_facility', ID]));
    ExecQuery(Format(DeleteLocateDetail, ['response_log', ID]));

    ExecQuery(Format(DeleteByTicketID, ['ticket_version', ID]));
    ExecQuery(Format(DeleteByTicketID, ['ticket_risk', ID]));   //QM-387 p3 EB
    ExecQuery(Format(DeleteByTicketID, ['jobsite_arrival', ID]));
    ExecQuery(Format(DeleteByTicketID, ['locate', ID]));
    ExecQuery(Format(DeleteByTicketID, ['ticket', ID]));
    ExecQuery(Format(DeleteByTicketID, ['ticket_alert', ID]));  //QM-771 Ticket Alerts EB

    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;

procedure TLocalCache.RemoveWorkOrderFromCache(const WorkOrderID: Integer);
const
  DeleteByForeignID = 'delete from notes where foreign_type = %d and foreign_id = %d';
  DeleteByWorkOrderID = 'delete from %s where wo_id = %d';
begin
  { General purpose way to totally remove a local work order, because we are about
    to reload it from the server with TLocalCache.GetWorkOrderDataFromServer. We
    aren't including the attachment table, to avoid conflicts with background
    uploading. }
  FDatabase.StartTransaction;
  try
    ExecQuery(Format(DeleteByForeignID, [qmftWorkOrder, WorkOrderID]));

    ExecQuery(Format(DeleteByWorkOrderID, ['work_order', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['work_order_version', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['work_order_ticket', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['wo_assignment', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['wo_status_history', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['wo_response_log', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['work_order_inspection', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['work_order_remedy', WorkOrderID]));
    ExecQuery(Format(DeleteByWorkOrderID, ['work_order_OHM_details', WorkOrderID]));

    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;




procedure TLocalCache.RemoveDamageFromCache(const ID: Integer);
const
  // This is a subset of the damage data, for the specific purpose of preventing
  // data consistency problem with damage invoices.
  DeleteDamage = 'delete from damage where damage_id=';
  DeleteDamageInvoice = 'delete from damage_invoice where damage_id=';
begin
  FDatabase.StartTransaction;
  try
    ExecQuery(DeleteDamage + IntToStr(ID));
    ExecQuery(DeleteDamageInvoice + IntToStr(ID));
    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;

procedure TLocalCache.RemoveDamageEstimateFromCache(const ID: Integer);
const
  // This is used to remove local damage_estimate records that could have been
  //inadvertently created on an APPROVED damage where the user was REVOKED the
  //RightDamagesEditWhenApproved.  (i.e. the user may not have synced down the
  //Approved damage or rights yet)
  DeleteDamageEstimate = 'delete from damage_estimate where estimate_id=';
begin
  FDatabase.StartTransaction;
  try
    ExecQuery(DeleteDamageEstimate + IntToStr(ID));
    FDatabase.Commit;
  except
    FDatabase.Rollback;
    raise;
  end;
end;

procedure TLocalCache.HandleSyncErrors(Errors: TObjectList);
var
  I: Integer;
  Err: TSyncError;
begin
  Assert(Assigned(Errors), 'Errors is not defined');
  for I := 0 to Errors.Count - 1 do begin
    Err := Errors[I] as TSyncError;
    if SameText('damage', Err.TableName) then
      RemoveDamageFromCache(Err.PrimaryKeyID);
    if SameText('damage_estimate', Err.TableName) then
      RemoveDamageEstimateFromCache(Err.PrimaryKeyID);
  end;
  if Errors.Count > 0 then
    raise EOdSyncError.Create('Errors detected while syncing: ' + SyncErrorsAsString(Errors));
end;

procedure TLocalCache.GetAirCardInformation;
var
  Mgr: IMbnInterfaceManager;
  pInterfaceArray, pPhoneNumberArray: PSafeArray;
  pInterface: IMbnInterface;
  subscriber: IMbnSubscriberInformation;
//  ReadyState: MBN_READY_STATE;
  interfaceCaps: MBN_INTERFACE_CAPS;
  lIntfLower, lIntfUpper: longint;
  lPhoneNumLower, lPhoneNumUpper: longint;
  I, J: longint;
  wStr: WideString;
begin
  {These are ONLY debug settings for developer/testing.  Changed to add
   a SecondaryDebugOn flag WITH DebugDeviceOverride to prevent Debug from using them }
  if (DM.UQState.SecondaryDebugOn) and
    (DM.UQState.DebugDeviceOverride) then begin  {QM-164 EB - This is only set when DebugAirCardOn = 1}
    DeviceRecord.AirCardDeviceID := DM.UQState.Override_AirCardDeviceID;  //QM-805 Eb Debug Refactoring Fix
    DeviceRecord.AirCardBrand := DM.UQState.Override_AirCardBrand;
    DeviceRecord.AirCardPhone := DM.UQState.Override_AirCardPhone;
    DeviceRecord.SimCardDeviceID := DM.UQState.Override_SimCardDeviceID;
    DeviceRecord.Subscriber := DM.UQState.Override_Subscriber;
    DeviceRecord.LastUpdated := Now;
    DM.ReplaceDeviceInfo(DeviceRecord);
  end;
  try
    OleCheck(CoCreateInstance(CLASS_MbnInterfaceManager, nil, CLSCTX_ALL, IMbnInterfaceManager, Mgr));
    OleCheck(Mgr.GetInterfaces(pInterfaceArray));
    try
      OleCheck(SafeArrayGetLBound(pInterfaceArray, 1, lIntfLower));
      OleCheck(SafeArrayGetUBound(pInterfaceArray, 1, lIntfUpper));
      for I := lIntfLower to lIntfUpper do
      begin
        OleCheck(SafeArrayGetElement(pInterfaceArray, I, pInterface));
        try
          {AirCard}
          OleCheck(pInterface.Get_InterfaceID(wStr));
          OleCheck(pInterface.GetInterfaceCapability(interfaceCaps));

          DeviceRecord.AirCardDeviceID := '';
          DeviceRecord.AirCardDeviceID := interfaceCaps.deviceID;
          DeviceRecord.AirCardBrand := interfaceCaps.manufacturer + ', ' + interfaceCaps.model;

          {Subscriber}
          OleCheck(pInterface.GetSubscriberInformation(subscriber));
          try
            OleCheck(subscriber.Get_SubscriberID(wStr));

            OleCheck(subscriber.Get_TelephoneNumbers(pPhoneNumberArray));
            OleCheck(subscriber.Get_SubscriberID(wStr));
            DeviceRecord.Subscriber := wStr;
            OleCheck(subscriber.Get_SimIccID(wStr)); //QM-164 EB
            DeviceRecord.SimCardDeviceID := wStr; //QM-164 EB
            try
              OleCheck(SafeArrayGetLBound(pPhoneNumberArray, 1, lPhoneNumLower));
              OleCheck(SafeArrayGetUBound(pPhoneNumberArray, 1, lPhoneNumUpper));
              for J := lPhoneNumLower to lPhoneNumUpper do
              begin
                OleCheck(SafeArrayGetElement(pPhoneNumberArray, J, wStr));
                try
                  DeviceRecord.AirCardPhone := wStr; //if more than one, will accept last one
                finally
                  wStr := '';
                end;
              end;
            finally
              SafeArrayDestroy(pPhoneNumberArray);
            end;
          finally
            subscriber := nil;
          end;

        finally
          pInterface := nil;
        end;
      end;
    finally
      DM.ReplaceDeviceInfo(DeviceRecord);
      SafeArrayDestroy(pInterfaceArray);
    end;
  except
    raise;
  end;
end;



{ TSyncError }
constructor TSyncError.Create(const aTableName: string;
  const aKeyID: Integer; const aMessage: string);
begin
  inherited Create;
  TableName := aTableName;
  PrimaryKeyID := aKeyID;
  ErrorText := aMessage;
end;

end.
