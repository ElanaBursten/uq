unit OdFtp;

interface

uses
  Classes, SysUtils, IdFTP;

procedure ChangeToOrMakeDir(FTP: TIdFTP; const Dir: string);

function FtpDirAsString(FTP: TIdFTP; Match: string): string;
function FTPPortAsWord(Port: Integer): Word;

implementation

uses
  QMConst;

function DirExists(FTP: TIdFTP; const Dir: string): Boolean;
var
  StrLst: TStringList;
begin
  StrLst := TStringList.Create;
  try
    FTP.List(StrLst, '', False);
    StrLst.CaseSensitive := False;
    Result := StrLst.IndexOf(Dir) >= 0;
  finally
    FreeAndNil(StrLst);
  end;
end;

function FtpDirAsString(FTP: TIdFTP; Match: string): string;
var
  StrLst: TStringList;
begin
  StrLst := TStringList.Create;
  try
    FTP.List(StrLst, Match, False);
    Result := StrLst.CommaText;
  finally
    FreeAndNil(StrLst);
  end;
end;

procedure ChangeToOrMakeDir(FTP: TIdFTP; const Dir: string);
begin
  if not DirExists(FTP, Dir) then begin
    //Engine.AddToMyLog(Self, 'Making Directory: ' + Dir);
    FTP.MakeDir(Dir);
  end;
  //Engine.AddToMyLog(Self, 'Changing to Directory: ' + Dir);
  FTP.ChangeDir(Dir)
end;

function FTPPortAsWord(Port: Integer): Word;
begin
  Assert((Port >= MinFTPPort) and (Port <= MaxFTPPort), Format('%d is not in ' +
    'the valid port number range of %d - %d.', [Port, MinFTPPort, MaxFTPPort]));
  Result := Word(Port);
end;

end.
