object DateDialog: TDateDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select a Date'
  ClientHeight = 242
  ClientWidth = 241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DateLabel: TLabel
    Left = 8
    Top = 177
    Width = 225
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Caption = 'Selected Date: 10/10/2009'
  end
  object DateCal: TMonthCalendar
    Left = 8
    Top = 8
    Width = 225
    Height = 158
    Date = 39988.276500821760000000
    FirstDayOfWeek = dowSunday
    TabOrder = 0
    OnClick = DateCalClick
    OnDblClick = DateCalDblClick
  end
  object OKButton: TButton
    Left = 33
    Top = 205
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 131
    Top = 205
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
