unit OdAdoUtils;

interface

uses
  ADODB, IniFiles, SysUtils, Classes, Contnrs, Variants, UQDbConfig;

function MakeConnectionStringFromIni(const IniFileName: string; SectionName: string = ''): string;

function ConnectAdoConnectionWithIni(Conn: TADOConnection; const IniFileName: string): string;
procedure ConnectAdoConnection(Conn: TADOConnection; const Server, DB: string;
  const Trusted: Boolean; const Username, Password: string; Pooling: Boolean = True;
  const ConnString: string='');
procedure ConnectAdoConnectionWithConfig(Conn: TADOConnection; DbConfig: TADODatabaseConfig);

procedure SetConnections(Connection: TADOConnection; DM: TDataModule);
procedure CheckForAdoError(Conn: TADOConnection);
procedure CloseAllADODataSets(DM: TDataModule);
procedure SetParams(Command: TADOCommand; const ParamPrefix: string; Value: Variant); overload;
procedure SetParams(Query: TADOQuery; const ParamPrefix: string; Value: Variant; VerifyPresent: Boolean = True); overload;
procedure SetParams(Query: TADODataSet; const ParamPrefix: string; Value: Variant); overload;
procedure SetParams(Query: TCustomADODataSet; Params: array of const); overload;
procedure SetParams(Command: TADOCommand; Params: array of const); overload;
procedure EnsureParamSetTo(Query: TCustomADODataSet; Value: Variant);

function GetParam(Query: TADOQuery; const ParamName: WideString): Variant;
function ExecuteQuery(Connection: TADOConnection; const SQL: string) : Integer;
function CreateDatasetWithQuery(Connection: TADOConnection; const SelectSQL: string): TADODataSet;
function GetFieldValue(Connection: TADOConnection; TableName: String; FieldName: String; WhereCondition: String): Variant;
procedure LoadNextRecordset(const Master, RS: TCustomADODataSet); overload;
procedure LoadNextRecordset(const SP: TADOStoredProc; const RS: TCustomADODataSet); overload;
procedure OpenADODisconnected(DataSet: TCustomADODataSet);
function IsExplicitConnectionString(ConnString: string): boolean;
function ConnStringDescription(ConnString, DB: string): string;
function CleanConnString(ConnString: string): string;


implementation

uses
  OdMiscUtils, StrUtils, OdDBUtils, JclStrings;

function IsExplicitConnectionString(ConnString: string): Boolean;
begin
  Result := NotEmpty(ConnString);
end;

function ConnStringDescription(ConnString, DB: string): string;
const
  ServerIdentifiers: array[0..4] of string = ('Data Source', 'Address', 'Addr', 'Server', 'Network Address');
var
  slConnStr: TStrings;
  Server: string;
  i: Integer;
begin
  slConnStr := TStringList.Create;
  try
    JclStrings.StrTokenToStrings(StringReplace(ConnString, '"', ';', [rfReplaceAll]), ';', slConnStr);
    for i := 0 to Length(ServerIdentifiers) - 1 do begin
      Server := slConnStr.Values[ServerIdentifiers[i]];
      if not IsEmpty(Server) then
        Break;
    end;
    Result := Server  + '/' + DB;
  finally
    slConnStr.Free;
  end;
end;

function CleanConnString(ConnString: string): string;
const
  Identifiers: array[0..5] of string = ('Initial Catalog', 'Data Source', 'User ID', 'Password', 'Integrated Security', 'Persist Security Info');
var
  slConnStr: TStrings;
  i: Integer;
begin
  slConnStr := TStringList.Create;
  try
    Result := 'Provider=SQLOLEDB.1;Network Library=DBMSSOCN;';
    JclStrings.StrTokenToStrings(StringReplace(ConnString, '"', ';', [rfReplaceAll]), ';', slConnStr);
    for i := 0 to Length(Identifiers) - 1 do begin
      if slConnStr.Values[Identifiers[i]] <> '' then
        Result := Result + Identifiers[i] + '=' + slConnStr.Values[Identifiers[i]] + ';';
    end;
  finally
    slConnStr.Free;
  end;
end;

function MakeConnectionString(const Server, DB: string; const Trusted: Boolean; const Username, Password: string; Pooling: Boolean): string;
begin
  if Trusted then
    Result := 'Provider=SQLOLEDB.1;Initial Catalog=' + DB +
      ';Data Source=' + Server + ';Integrated Security=SSPI'
  else
    Result :=
    'Provider=SQLOLEDB.1;Network Library=DBMSSOCN;Initial Catalog=' + DB +
    ';Data Source=' + Server + ';User ID=' + Username + ';Password=' + Password +
    ';Persist Security Info=True';

  if not Pooling then
    Result := Result + ';OLE DB Services=-2';
end;

function MakeConnectionStringFromIni(const IniFileName: string; SectionName: string = ''): string;
var
  Ini: TIniFile;
  Dbc: TADODatabaseConfig;
begin
  if SectionName = '' then
    SectionName := 'Database';

  Ini := TIniFile.Create(ExpandFileName(IniFileName));
  try
    Dbc := TADODatabaseConfig.CreateFromIni(Ini, SectionName, 'MainDB');
    try
      if IsExplicitConnectionString(Dbc.ConnString) then
        Result := Dbc.ConnString
      else
        Result := MakeConnectionString(Dbc.Server, Dbc.DB, Dbc.Trusted, Dbc.Username, Dbc.Password, Dbc.Pooling);

    finally
      FreeAndNil(Dbc);
    end;
  finally
    FreeAndNil(Ini);
  end;
end;

procedure DoConnect(Conn: TADOConnection);
begin
  Conn.ConnectionTimeout := 10;   // on a LAN, no need for long timeouts
  Conn.KeepConnection := True;
  Conn.Open;
end;

procedure SetParams(Command: TADOCommand; const ParamPrefix: string; Value: Variant);
var
  i: Integer;
begin
  Assert(Assigned(Command));
  Assert(Trim(ParamPrefix) <> '');
  for i := 0 to Command.Parameters.Count - 1 do begin
    if StrBeginsWithText(ParamPrefix, Command.Parameters[i].Name) then
      Command.Parameters[i].Value := Value;
  end;
end;

procedure SetParams(Query: TADODataSet; const ParamPrefix: string; Value: Variant); overload;
var
  i: Integer;
begin
  Assert(Assigned(Query));
  Assert(Trim(ParamPrefix) <> '');
  for i := 0 to Query.Parameters.Count - 1 do begin
    if StrBeginsWithText(ParamPrefix, Query.Parameters[i].Name) then
      Query.Parameters[i].Value := Value;
  end;
end;

type
  THackCustomADODataSet = class(TCustomADODataSet);

procedure AssignParamValues(Params: TParameters; ParamValues: array of const);
var
  i: Integer;
begin
  for i := 0 to High(ParamValues) do begin
    try
      with ParamValues[i] do
        case VType of
          vtString:     Params[i].Value := string(VString^);
          vtInteger:    Params[i].Value := VInteger;
          vtPChar:      Params[i].Value := string(VPChar);
          vtAnsiString: Params[i].Value := AnsiString(VAnsiString);
          vtVariant:    Params[i].Value := VarToStr(VVariant^);
          vtBoolean:    Params[i].Value := VBoolean;
          vtWideString: Params[i].Value := WideString(VWideString);

          vtExtended:   begin
                           // DB seperates datetimes from floats; Delphi does not.
                          if Pos('DT', string(Params[i].Name)) > 0 then  // named DT?
                            Params[i].Value := VarToDateTime(VExtended^)
                          else
                            Params[i].Value := VExtended^;
                        end;
          else
            raise Exception.Create('Unexpected VType: ' + IntToStr(VType));
        end;
    except
      on E: Exception do begin
        E.Message := E.Message + ' Param#=' + IntToStr(i);
        raise;
      end;
    end;
  end;
end;

procedure SetParams(Query: TCustomADODataSet; Params: array of const);
var
  ADOQuery: THackCustomADODataSet;
begin
  ADOQuery := THackCustomADODataSet(Query);
  AssignParamValues(ADOQuery.Parameters, Params);
end;

procedure SetParams(Command: TADOCommand; Params: array of const);
begin
  AssignParamValues(Command.Parameters, Params);
end;

procedure SetParams(Query: TADOQuery; const ParamPrefix: string; Value: Variant; VerifyPresent: Boolean);
var
  i: Integer;
  QtySet: Integer;
begin
  Assert(Assigned(Query));
  Assert(Trim(ParamPrefix) <> '');
  QtySet := 0;
  for i := 0 to Query.Parameters.Count - 1 do begin
    if StrBeginsWithText(ParamPrefix, Query.Parameters[i].Name) then begin
      Query.Parameters[i].Value := Value;
      Inc(QtySet);
    end;
  end;
  if (QtySet < 1) and VerifyPresent then
    raise Exception.CreateFmt('Query %s does not have a parameter starting with %s', [Query.Name, ParamPrefix]);
end;

procedure EnsureParamSetTo(Query: TCustomADODataSet; Value: Variant);
var
  ADOQuery: THackCustomADODataSet;
  CurrentValue: Variant;
begin
  ADOQuery := THackCustomADODataSet(Query);
  Assert(ADOQuery.Parameters.Count = 1);
  CurrentValue := ADOQuery.Parameters[0].Value;
  if (not ADOQuery.Active) or (CurrentValue <> Value) then begin
    ADOQuery.Parameters[0].Value := Value;
    RefreshDataSet(ADOQuery);
  end;
end;

function GetParam(Query: TADOQuery; const ParamName: WideString): Variant;
begin
  Assert(Assigned(Query));
  Result := Query.Parameters.ParamByName(ParamName).Value;
end;

procedure ConnectAdoConnection(Conn: TADOConnection; const Server, DB: string;
  const Trusted: Boolean; const Username, Password: string; Pooling: Boolean;
  const ConnString: string);
begin
  Assert(Assigned(Conn));
  Conn.Connected := False;
  if IsExplicitConnectionString(ConnString) then
    Conn.ConnectionString := ConnString
  else
    Conn.ConnectionString := MakeConnectionString(Server, DB, Trusted, Username, Password, Pooling);
  DoConnect(Conn);
end;

function ConnectAdoConnectionWithIni(Conn: TADOConnection; const IniFileName: string): string;
begin
  Assert(Assigned(Conn));
  Conn.Connected := False;
  Result := MakeConnectionStringFromIni(IniFileName);
  Conn.ConnectionString := Result;
  DoConnect(Conn);
end;

procedure ConnectAdoConnectionWithConfig(Conn: TADOConnection; DbConfig: TADODatabaseConfig);
begin
  Assert(Assigned(DBConfig));
  ConnectAdoConnection(Conn, DbConfig.Server, DbConfig.DB, DbConfig.Trusted, DbConfig.Username, DbConfig.Password, DbConfig.Pooling, DbConfig.ConnString);
end;

procedure SetConnections(Connection: TADOConnection; DM: TDataModule);
var
  i: Integer;
begin
  for i := 0 to DM.ComponentCount - 1 do begin
    if (DM.Components[i] is TCustomADODataSet) then
      if TCustomADODataSet(DM.Components[i]).Connection = nil then
        TCustomADODataSet(DM.Components[i]).Connection := Connection;
    if (DM.Components[i] is TADOCommand) then
      if TADOCommand(DM.Components[i]).Connection = nil then
        TADOCommand(DM.Components[i]).Connection := Connection;
  end;
end;

procedure CheckForAdoError(Conn: TADOConnection);
var
  i: Integer;
  msg: string;
begin
  Assert(Assigned(Conn));
  if Conn.Errors.Count = 0 then
    Exit;

  msg := '';
  for i := 0 to Conn.Errors.Count - 1 do
    msg := msg + Conn.Errors[i].Description + #13;
  raise Exception.Create(msg);
end;

procedure CloseAllADODataSets(DM: TDataModule);
var
  i: Integer;
  Comp: TComponent;
  DataSets: TObjectList;
begin
  DataSets := TObjectList.Create(False);
  try
    for i := 0 to DM.ComponentCount - 1 do begin
      Comp := DM.Components[i];
      if Comp is TCustomADODataSet then
        DataSets.Add(Comp);
    end;

    for i := 0 to DataSets.Count - 1 do begin
      (DataSets[i] as TCustomADODataSet).Active := False
    end;
  finally
    FreeAndNil(DataSets);
  end;
end;

function ExecuteQuery(Connection: TADOConnection; const SQL: string) : Integer;
var
  Cmd: TADOCommand;
begin
  Result := 0;
  Cmd := TADOCommand.Create(nil);
  try
    Cmd.Connection := Connection;
    Cmd.CommandText := SQL;
    Cmd.Execute(Result, Null);
  finally
    FreeAndNil(Cmd);
  end;
end;

function CreateDatasetWithQuery(Connection: TADOConnection; const SelectSQL: string): TADODataSet;
begin
  // The caller must free the Result when done with it
  Result := TADODataSet.Create(nil);
  try
    (Result as TADODataSet).Connection := Connection;
    (Result as TADODataSet).CommandText := SelectSQL;
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function GetFieldValue(Connection: TADOConnection; TableName: String; FieldName: String; WhereCondition: String): Variant;
const
  SQL = 'select %s from %s where %s';
var
  Dataset: TADODataSet;
begin
  Dataset := CreateDatasetWithQuery(Connection, Format(SQL, [FieldName, TableName, IfThen(IsEmpty(WhereCondition), '1=1', WhereCondition)]));
  try
    Result := Dataset.FieldByName(FieldName).Value;
  finally
    FreeAndNil(Dataset);
  end;
end;

procedure LoadNextRecordset(const SP: TADOStoredProc; const RS: TCustomADODataSet);
var
  RecsAffected: Integer;
  AdoRecordSet: _Recordset;
begin
  AdoRecordSet := SP.NextRecordset(RecsAffected);
  Assert(Assigned(AdoRecordSet), 'SP.NextRecordset must return not nil: ' + RS.Name);
  RS.Recordset := AdoRecordSet;
  Assert(RS.Active, 'Recordset should be active:  ' + RS.Name);
end;

procedure LoadNextRecordset(const Master, RS: TCustomADODataSet);
var
  RecsAffected: Integer;
  AdoRecordSet: _Recordset;
begin
  AdoRecordSet := Master.NextRecordset(RecsAffected);
  Assert(Assigned(AdoRecordSet), 'SP.NextRecordset must return not nil: ' + RS.Name);
  RS.Recordset := AdoRecordSet;
  Assert(RS.Active, 'Recordset should be active:  ' + RS.Name);
end;

procedure OpenADODisconnected(DataSet: TCustomADODataSet);
begin
  Assert(Assigned(DataSet));
  Assert(Assigned(DataSet.Connection));
  DataSet.CursorLocation := clUseClient;
  DataSet.CursorType := ctStatic;
  DataSet.LockType := ltBatchOptimistic;
  DataSet.Open;
  DataSet.Connection := nil;
end;

end.

