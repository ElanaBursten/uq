{******************************************************************************}
{  Universal Time Coordinate (UTC) Date Time Format Mapping                    }
{******************************************************************************}

unit OdUtcDates;

interface

{$WARN SYMBOL_PLATFORM OFF}

uses
  SysUtils, Windows;

function UtcDateTimeToStr(Value: TDateTime; OffsetMinutes: Integer): string;
function UtcStrToDateTime(const Value: string; var OffsetMinutes: Integer): TDateTime;
function UtcStrToDateTimeDef(const Value: string; var OffsetMinutes: Integer; Default: TDateTime = 0.0): TDateTime;
function DateTimeToUTCDateTime(const Value: TDateTime; const OffsetMinutes: Integer): TDateTime;
function UTCDateTimeToLocalDateTime(UTCDateTime: TDateTime): TDateTime;
function TzSpecificLocalTimeToSystemTime(lpTimeZoneInformation: PTimeZoneInformation;
  var lpLocalTime, lpUniversalTime: TSystemTime): BOOL; stdcall;
function LocalDateTimeToUTCDateTime(LocalDateTime: TDateTime): TDateTime;

implementation

uses
  DateUtils;

resourcestring
  SInvalidUTCDate = '''%s'' is not a valid UTC date and time';

const
  UtcDateTimeFormat = 'YYYYMMDDhhnnss.zzz';

// yyyymmddhhnnss.zzzzzzsUUU  +060 means 60 mins of UTC time
// 20030709091030.686000+060
// 1234567890123456789012345

function UtcDateTimeToStr(Value: TDateTime; OffsetMinutes: Integer): string;
var
  Sign: string;
begin
  Result := FormatDateTime(UtcDateTimeFormat, Value);
  if OffsetMinutes > -1 then
    Sign := '+'
  else
    Sign := '-';
  Result := Result + '000' + Sign + Format('%3.3d', [Abs(OffsetMinutes)]);
end;

function UtcStrToDateTimeDef(const Value: string; var OffsetMinutes: Integer; Default: TDateTime): TDateTime;
begin
  try
    Result := UtcStrToDateTime(Value, OffsetMinutes);
  except
    Result := Default;
  end;
end;

function UtcStrToDateTime(const Value: string; var OffsetMinutes: Integer): TDateTime;
var
  yy, mm, dd, hh, nn, ss, zz: Integer;
  TimePart: TDateTime;

  function GetNum(Pos, Len: Integer): Integer;
  begin
    Result := StrToIntDef(Copy(Value, Pos, Len), -1);
  end;

begin
  Result := 0;
  OffsetMinutes := 0;
  if Length(Value) <> 25 then
    Exit;
  yy := GetNum(1, 4);
  mm := GetNum(5, 2);
  if (mm = 0) or (mm > 12) then
    Exit;
  dd := GetNum(7, 2);
  if (dd = 0) or (dd > 31) then
    Exit;
  if not TryEncodeDate(yy, mm, dd, Result) then
    raise EConvertError.CreateFmt(SInvalidUTCDate, [Value]);

  hh := GetNum(9, 2);
  nn := GetNum(11, 2);
  ss := GetNum(13, 2);
  zz := 0;
  if Length(Value) >= 18 then
    zz := GetNum(16, 3);
  if not TryEncodeTime(hh, nn, ss, zz, TimePart) then
    raise EConvertError.CreateFmt(SInvalidUTCDate, [Value]);

  Result := Result + TimePart;
  OffsetMinutes := GetNum(22, 4);
end;

function DateTimeToUTCDateTime(const Value: TDateTime; const OffsetMinutes: Integer): TDateTime;
begin
  Result := Value + ((OffsetMinutes/60)/24);
end;

{ The corresponding JCL routine: DateTimeToLocalDateTime, is incorrect. It does
  not attempt to determine if the standard or daylight time bias should be used.
  It just uses the current bias, which may not be correct. Follow these links
  for (somewhat confusing) discussions:
  http://issuetracker.delphi-jedi.org/print_bug_page.php?bug_id=402
  http://issuetracker.delphi-jedi.org/view.php?id=403
  http://issuetracker.delphi-jedi.org/print_bug_page.php?bug_id=1045
  http://issuetracker.delphi-jedi.org/view.php?id=5746 }
function UTCDateTimeToLocalDateTime(UTCDateTime: TDateTime): TDateTime;
var
  UTC, Local: TSystemTime;
begin
  DateTimeToSystemTime(UTCDateTime, UTC);
  try
    Win32Check(SystemTimeToTzSpecificLocalTime(nil, UTC, Local));
  except
    on E: Exception do begin
      E.Message := Format('Could not convert UTC date/time: %s, to local ' +
        'date/time. (%s)', [DateTimeToStr(UTCDateTime), E.Message]);
      raise E;
    end;
  end;
  Result := EncodeDateTime(Local.wYear, Local.wMonth, Local.wDay, Local.wHour,
    Local.wMinute, Local.wSecond, Local.wMilliseconds);
end;

function TzSpecificLocalTimeToSystemTime; external kernel32 name 'TzSpecificLocalTimeToSystemTime';

{ The corresponding JCL routine: LocalDateTimeToDateTime, is incorrect. It does
  not attempt to determine if the standard or daylight time bias should be used.
  It just uses the current bias, which may not be correct. Follow these links
  for (somewhat confusing) discussions:
  http://issuetracker.delphi-jedi.org/print_bug_page.php?bug_id=402
  http://issuetracker.delphi-jedi.org/view.php?id=403
  http://issuetracker.delphi-jedi.org/print_bug_page.php?bug_id=1045
  http://issuetracker.delphi-jedi.org/view.php?id=5746 }
function LocalDateTimeToUTCDateTime(LocalDateTime: TDateTime): TDateTime;
var
  UTC, Local: TSystemTime;
begin
  DateTimeToSystemTime(LocalDateTime, Local);
  try
    Win32Check(TzSpecificLocalTimeToSystemTime(nil, Local, UTC));
  except
    on E: Exception do begin
      E.Message := Format('Could not convert local date/time: %s, to UTC ' +
        'date/time. (%s)', [DateTimeToStr(LocalDateTime), E.Message]);
      raise E;
    end;
  end;
  Result := EncodeDateTime(UTC.wYear, UTC.wMonth, UTC.wDay, UTC.wHour,
    UTC.wMinute, UTC.wSecond, UTC.wMilliseconds);
end;

end.
