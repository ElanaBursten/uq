unit OdAutoSyncTimerDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ODTimerDialog, StdCtrls, ExtCtrls;

type
  TAutoSyncTimerDialogForm = class(TTimerDialogForm)
    LocationCombo: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure ButtonYesClick(Sender: TObject);
  end;

implementation

uses DMu, OdVclUtils, OdExceptions;

{$R *.dfm}

procedure TAutoSyncTimerDialogForm.FormShow(Sender: TObject);
begin
  inherited;
  DM.UploadLocationList(LocationCombo.Items);
  SelectComboBoxItemFromObjects(LocationCombo, DM.UQState.LastUploadLocation);
end;

procedure TAutoSyncTimerDialogForm.ButtonYesClick(Sender: TObject);
begin
  if LocationCombo.ItemIndex < 0 then
    raise EOdEntryRequired.Create('Please select an upload location');
  inherited; // Don't remove this line
  DM.UQState.LastUploadLocation := GetComboObjectInteger(LocationCombo);
end;

end.
