unit OdHttpDialog;

interface

uses Windows, Messages, StdCtrls, Forms, ComCtrls, Controls, Classes, OdWinInet,
  ExtCtrls;

function DefaultDialogHttp: TOdHttpDetails;
procedure OdHttpPost(ReqStream, RespStream: TStream;
                     const URL, ContentType: string;
                     ShowProgress: Boolean; UseCompression: Boolean);

type
  TOdNetProgressDialog = class(TForm)
    URL: TLabel;
    Transfer: TLabel;
    TransferLabel: TLabel;
    URLEdit: TEdit;
    Status: TLabel;
    StatusLabel: TLabel;
    Animation: TAnimate;
    ProgressPanel: TPanel;
    Progress: TProgressBar;
    RemainingLabel: TLabel;
    Remaining: TLabel;
    ProgressLabel: TLabel;
    ButtonPanel: TPanel;
    CancelButton: TButton;
    ShowTimer: TTimer;
    ElapsedLabel: TLabel;
    Elapsed: TLabel;
    UpdateTimer: TTimer;
    StalledTimer: TTimer;
    procedure CancelButtonClick(Sender: TObject);
    procedure ShowTimerTimer(Sender: TObject);
    procedure UpdateTimerTimer(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure StalledTimerTimer(Sender: TObject);
  private
    Details: TOdHttpDetails;
    FHaveFileSize: Boolean;
    FLastTransferedBytes: Cardinal;
    FTransferStalled: Boolean;
    procedure InitDialog;
    procedure SetHaveFileSize(const Value: Boolean);
    procedure UpdateLabels;
    procedure SetTimersEnabled(Enable: Boolean);
  protected
    procedure Loaded; override;
  public
    procedure OnProgress(Sender: TOdHttp; const TotalBytes,
      TransferedBytes, BytesPerSec, SecondsRemaining: Cardinal);
    procedure OnStatusChange(Sender: TOdHttp);
    procedure OnError(Sender: TOdHttp; Error: TOdNetError);
    procedure OnComplete(Sender: TOdHttp);
    property HaveFileSize: Boolean read FHaveFileSize write SetHaveFileSize;
  end;

implementation

{$R *.dfm}

uses
  SysUtils, Math, OdMiscUtils, OdNetUtils, Terminology;

type
  TOdHttpDialogDetails = class(TOdHttpDetails)
  protected
    Dialog: TOdNetProgressDialog;
  protected
    procedure ShowProgress; override;
    destructor Destroy; override;
  end;

var
  PrivateDefaultDialogHttp: TOdHttpDialogDetails;

const
  StalledText = 'working';

function DefaultDialogHttp: TOdHttpDetails;
begin
  if not Assigned(PrivateDefaultDialogHttp) then begin
    PrivateDefaultDialogHttp := TOdHttpDialogDetails.Create;
    PrivateDefaultDialogHttp.Dialog := TOdNetProgressDialog.Create(nil);
    PrivateDefaultDialogHttp.Dialog.Details := PrivateDefaultDialogHttp;
    PrivateDefaultDialogHttp.OdHttp := nil;
    PrivateDefaultDialogHttp.Threaded := True;
    PrivateDefaultDialogHttp.IdleMainThread := True;
    PrivateDefaultDialogHttp.OnProgress := PrivateDefaultDialogHttp.Dialog.OnProgress;
    PrivateDefaultDialogHttp.OnError := PrivateDefaultDialogHttp.Dialog.OnError;
    PrivateDefaultDialogHttp.OnComplete := PrivateDefaultDialogHttp.Dialog.OnComplete;
    PrivateDefaultDialogHttp.OnStatusChange := PrivateDefaultDialogHttp.Dialog.OnStatusChange;
  end
  else
    PrivateDefaultDialogHttp.FreeAndNilOdHttp;
  Result := PrivateDefaultDialogHttp;
end;

procedure TOdNetProgressDialog.OnProgress(Sender: TOdHttp; const TotalBytes,
    TransferedBytes, BytesPerSec, SecondsRemaining: Cardinal);
begin
  //  These updates are done in the timer
end;

procedure TOdNetProgressDialog.OnStatusChange(Sender: TOdHttp);
begin
  StatusLabel.Caption := Sender.StatusMessage;
  UpdateLabels;
end;

procedure TOdNetProgressDialog.CancelButtonClick(Sender: TObject);
begin
  Assert(Assigned(Details.OdHttp));
  CancelButton.Caption := 'Aborting';
  CancelButton.Enabled := False;
  Details.OdHttp.ForceAbortTransfer;
end;

procedure TOdNetProgressDialog.InitDialog;
begin
  Caption := 'Transfer Progress';
  if Assigned(Details) and Assigned(Details.OdHttp) then
    URLEdit.Text := Details.OdHttp.URL;
  Progress.Min := 0;
  Progress.Max := 100;
  Progress.Position := 0;
  StatusLabel.Caption := 'Starting transfer';
  CancelButton.Caption := 'Cancel';
  CancelButton.Enabled := True;
  TransferLabel.Caption := '';
  StatusLabel.Caption := '';
  RemainingLabel.Caption := '';
  ElapsedLabel.Caption := '';
  FHaveFileSize := False;
  FLastTransferedBytes := 0;
  ActiveControl := CancelButton;
  if Assigned(Application.MainForm) then
    Application.MainForm.Enabled := False;
end;

{ TOdHttpDialogDetails }

destructor TOdHttpDialogDetails.Destroy;
begin
  FreeAndNil(Dialog);
  inherited;
end;

procedure TOdNetProgressDialog.OnError(Sender: TOdHttp; Error: TOdNetError);
begin
  Self.ModalResult := mrCancel;
  Self.Hide;
  // Since this is running in idling mode, we don't need to show the exception
  // the caller will get the exception raised from TOdHttp.StartTransfer
end;

procedure TOdNetProgressDialog.OnComplete(Sender: TOdHttp);
begin
  SetTimersEnabled(False);
  if Sender.SuccessfulTransfer then
    ModalResult := mrOk
  else
    ModalResult := mrCancel;
  Hide;
  if Assigned(Application.MainForm) then
    Application.MainForm.Enabled := True;
end;

procedure TOdHttpDialogDetails.ShowProgress;
begin
  inherited;
  Assert(Assigned(Dialog));
  Dialog.InitDialog;
  Dialog.SetTimersEnabled(True);
end;

procedure TOdNetProgressDialog.SetHaveFileSize(const Value: Boolean);
begin
  if Value <> HaveFileSize then begin
    FHaveFileSize := Value;
    Animation.Visible := not HaveFileSize;
    Animation.Active := not HaveFileSize;
    ProgressPanel.Visible := HaveFileSize;
    if HaveFileSize then
      ClientHeight := 157
    else
      ClientHeight := 173;
  end;
end;

procedure TOdNetProgressDialog.ShowTimerTimer(Sender: TObject);
begin
  // For very short transfers, don't even show the dialog
  ShowTimer.Enabled := False;
  ShowModal;
end;

procedure TOdNetProgressDialog.UpdateTimerTimer(Sender: TObject);
begin
  UpdateLabels;
end;

procedure TOdNetProgressDialog.UpdateLabels;
var
  Received: string;
  Total: string;
  Speed: string;
  Http: TOdHttp;
  Remaining: Cardinal;
begin
  Http := Details.OdHttp;
  if not Assigned(Http) then
    Exit;
  StatusLabel.Caption := Http.StatusMessage;
  Received := BytesToFileSize(Http.TransferedBytes);
  Total := BytesToFileSize(Http.TransferSize);
  Speed := BytesPerSecToTransferSpeed(Http.BytesPerSecond);
  HaveFileSize := Http.TransferSize > 0;
  if FLastTransferedBytes <> Http.TransferedBytes then
    FTransferStalled := False;

  if HaveFileSize then begin
    Progress.Max := Min(MaxInt, Http.TransferSize);
    Progress.Position := Min(MaxInt, Http.TransferedBytes) mod (Progress.Max + 1);
    if FTransferStalled then
      TransferLabel.Caption := Format('%s of %s (%s)', [Received, Total, StalledText])
    else
      TransferLabel.Caption := Format('%s of %s at %s', [Received, Total, Speed]);
    Remaining := Http.SecondsRemaining;
    // Assume times more than a day are temporarily incorrect
    if (Remaining > 0) and (Remaining < 60 * 60 * 24) then
      RemainingLabel.Caption := SecondsToHMS(Remaining)
    else
      RemainingLabel.Caption := '';
  end
  else begin
    if FTransferStalled then
      TransferLabel.Caption := Format('%s (%s)', [Received, StalledText])
    else
      TransferLabel.Caption := Format('%s at %s', [Received, Speed]);
  end;
  ElapsedLabel.Caption := SecondsToHMS(Details.OdHttp.ElapsedSeconds);
end;

procedure TOdNetProgressDialog.FormHide(Sender: TObject);
begin
  if Assigned(Application.MainForm) then
    Application.MainForm.Enabled := True;
  SetTimersEnabled(False);
end;

procedure TOdNetProgressDialog.SetTimersEnabled(Enable: Boolean);
begin
  ShowTimer.Enabled := Enable;
  UpdateTimer.Enabled := Enable;
  StalledTimer.Enabled := Enable;
end;

procedure TOdNetProgressDialog.StalledTimerTimer(Sender: TObject);
begin
  if Details.OdHttp.TransferedBytes = FLastTransferedBytes then
    FTransferStalled := True;
  FLastTransferedBytes := Details.OdHttp.TransferedBytes;
end;

// The main entry point to get a progress-displaying HTTP POST
procedure OdHttpPost(ReqStream, RespStream: TStream;
                     const URL, ContentType: string;
                     ShowProgress: Boolean; UseCompression: Boolean);
var
  OH: TOdHttp;
begin
  // Make sure we start at the start, in case the caller doesn't reset their
  // streams properly.
  ReqStream.Seek(0,0);
  RespStream.Seek(0,0);
  if ShowProgress then
    with DefaultDialogHttp do begin
      FreeAndNilOdHttp;
      OdHttp := TOdHttp.Create;
      OdHttp.Threaded := True;
      OdHttp.IdleMainThread := True;
      OdHttp.URL := URL;
      OdHttp.ContentType := ContentType;
      OdHttp.OnError := OnError;
      OdHttp.OnProgress := OnProgress;
      OdHttp.OnStatusChange := OnStatusChange;
      OdHttp.OnComplete := OnComplete;
      OdHttp.DeflateCompression := UseCompression;
      ShowProgress;
      OdHttp.SendStream(ReqStream, RespStream);
    end
  else begin
    OH := TOdHttp.Create;
    try
      OH.Threaded := False;
      OH.IdleMainThread := False;
      OH.URL := URL;
      OH.ContentType := ContentType;
      OH.DeflateCompression := UseCompression;
      OH.SendStream(ReqStream, RespStream);
    finally
      FreeAndNil(OH);
    end;
  end;
end;

procedure TOdNetProgressDialog.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

end.

