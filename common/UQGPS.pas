unit UQGPS;

interface

uses SysUtils, ConvUtils, StdConvs, GpsToolsXP_TLB;

type
  TLogEvent = procedure(Sender: TObject; const LogMessage: string) of object;

  TGPSPositionInfo = class(TObject)
  public
    MeasureUnitType: TConvType;
    HaveFix: Boolean;
    HavePosition: Boolean;
    Latitude: Double;
    Longitude: Double;
    HDOP: Double;
    SatellitesUsed: Integer;
    function DistanceFrom(Latitude2, Longitude2: Double; UnitType: TConvType): Double;
    constructor Create(UnitType: TConvType);
    destructor Destroy; override;
  end;

  TGPSInformation = class(TObject)
  private
    GPSToolsLicense: ILicense;
    Parser: INMEAParser;
    FMeasureUnitType: TConvType;
    CurrentPosition: TGPSPositionInfo;
    FOnLog: TLogEvent;
    LoggedConnectionError: Boolean;
    procedure SetupGPSToolsLicense;
    procedure SetupNMEAParser(GPSPort: Integer);
    function GetEnabled: Boolean;
    procedure SetEnabled(const Value: Boolean);
    function GetComPort: Integer;
    procedure SetComPort(GPSPort: Integer);
    function GetMeasureUnitType: TConvType;
    procedure SetMeasureUnitType(const Value: TConvType);
    procedure LogMessage(const Msg: string);
  public
    constructor Create(UnitType: TConvType; GPSPort: Integer);
    destructor Destroy; override;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ComPort: Integer read GetComPort write SetComPort;
    property MeasureUnitType: TConvType read GetMeasureUnitType write SetMeasureUnitType;
    property OnLog: TLogEvent read FOnLog write FOnLog;
    function GetMyPosition: TGPSPositionInfo;
  end;

const
  UTM_NORTH = 1;
  GPSTimeout = 5000; // 5 seconds
  // Oasis Digital's license key acquired for Q Manager application usage:
  QMGPSToolsLicenseKey = '1420A0E53D4691325695';

implementation

uses OdMiscUtils;

{ TGPSInformation }

constructor TGPSInformation.Create(UnitType: TConvType; GPSPort: Integer);
begin
  CurrentPosition := nil;
  MeasureUnitType := UnitType;
  SetupGPSToolsLicense;
  SetupNMEAParser(GPSPort);
  LoggedConnectionError := False;
end;

destructor TGPSInformation.Destroy;
begin
  FreeAndNil(CurrentPosition);
  inherited;
end;

procedure TGPSInformation.SetupGPSToolsLicense;
begin
  try
    GPSToolsLicense := CoLicense.Create;
    GPSToolsLicense.LicenseKey := QMGPSToolsLicenseKey;
  except
    on E: Exception do begin
      E.Message := 'GPS is disabled because SetupGPSToolsLicense failed. ' + E.Message;
      LogMessage(E.Message);
      raise;
    end;
  end;
end;

procedure TGPSInformation.SetupNMEAParser(GPSPort: Integer);
begin
  try
    Parser := CoNmeaParser.Create;
    Parser.ComPort := GPSPort;
    Parser.ChecksumMandatory := True;
    Parser.NoEvents := True;
  except
    on E: Exception do begin
      E.Message := 'GPS is disabled because SetupNMEAParser failed. ' + E.Message;
      LogMessage(E.Message);
      raise;
    end;
  end;
end;

procedure TGPSInformation.SetComPort(GPSPort: Integer);
var
  PreviousState: boolean;
begin
  if Assigned(Parser) then begin
    PreviousState := Enabled;
    Enabled := False;
    Parser.ComPort := ComPort;
    Enabled := PreviousState;
  end;
end;

function TGPSInformation.GetComPort: Integer;
var
  Status: IComStatus;
begin
  Result := -1;
  if (not Assigned(Parser)) or (not Enabled) then
    Exit;

  try
    Status := IUnknown(Parser.GetComStatus) as IComStatus;
    Result := VarToInt(Status.ComPort);
  except
    on E: Exception do
      LogMessage('GetComPort failed; ' + E.Message);
  end;
end;

function TGPSInformation.GetMyPosition: TGPSPositionInfo;

  function GetPositionHDOP: Double;
  var
    Quality: IQuality;
    HDOPMeters: Double;
  begin
    Result := -1;
    if not Assigned(Parser) then begin
      LogMessage('GetMyPosition cannot get HDOP because no NMEAParser is assigned');
      Exit;
    end;

    // Horizontal Dilution of Precision in requested measurement unit
    try
      Quality := IUnknown(Parser.GetQuality(GPSTimeout)) as IQuality;
      if Assigned(Quality) then begin
        HDOPMeters := VarToFloat(Quality.HDOP);
        Result := Convert(HDOPMeters, duMeters, MeasureUnitType);
      end;
    except
      on E: Exception do
        LogMessage('GetPositionHDOP failed; ' + E.Message);
    end;
  end;

  function GetSattelitesUsed: Integer;
  var
    Satellites: ISatellites;
    Satellite: ISatellite;
    SatelliteCount: Integer;
    I: Integer;
    ItemIdx: OleVariant;
  begin
    Result := 0;
    if not Assigned(Parser) then begin
      LogMessage('GetMyPosition cannot get sattelite count because no NMEAParser is assigned');
      Exit;
    end;

    try
      Satellites := IUnknown(Parser.GetSatellites(GPSTimeout)) as ISatellites;
      if Assigned(Satellites) then begin
        SatelliteCount := VarToInt(Satellites.Count);
        for I := 1 to SatelliteCount do begin
          ItemIdx := I;
          Satellite := IUnknown(Satellites.Item(ItemIdx)) as ISatellite;
          if VarToBoolean(Satellite.UsedForFix) then // only counts satellites actually used
            Inc(Result);
        end;
      end;
    except
      on E: Exception do
        LogMessage('GetSattelitesUsed failed; ' + E.Message);
    end;
  end;

var
  Fix: IGpsFix;
  Pos: IPosition;
begin
  FreeAndNil(CurrentPosition);

  CurrentPosition := TGPSPositionInfo.Create(MeasureUnitType);
  Result := CurrentPosition;
  Result.HaveFix := False;
  Result.HavePosition := False;

  if not Assigned(Parser) then begin
    LogMessage('GetMyPosition cannot get position because no NMEAParser is assigned');
    Exit;
  end;

  if not Enabled then
    Enabled := True;

  try
    Fix := IUnknown(Parser.GetGpsFix(GPSTimeout, 2)) as IGpsFix;
    if Assigned(Fix) then
      Result.HaveFix := True;
  except
    on E: Exception do
      LogMessage('GetMyPosition cannot get GPS fix; ' + E.Message);
  end;
  if not Result.HaveFix then
    Exit;

  try
    Pos := IUnknown(Fix.Position) as IPosition;
    if Assigned(Pos) then
      Result.HavePosition := True;
  except
    on E: Exception do
      LogMessage('GetMyPosition cannot get GPS position; ' + E.Message);
  end;
  if not Result.HavePosition then
    Exit;

  Pos.Grid := UTM_NORTH;
  Result.Latitude := Pos.Latitude;
  Result.Longitude := Pos.Longitude;
  Result.HDOP := GetPositionHDOP;
  Result.SatellitesUsed := GetSattelitesUsed;

  LogMessage(Format('Pos: %.6f, %.6f %s[Sats: %d %sHDOP: %f]',
    [Result.Latitude, Result.Longitude, #09, Result.SatellitesUsed, #09, Result.HDOP]));
end;

procedure TGPSInformation.SetMeasureUnitType(const Value: TConvType);
begin
  if Value <> FMeasureUnitType then begin
    FMeasureUnitType := Value;
    if Assigned(CurrentPosition) then
      CurrentPosition.MeasureUnitType := Value;
  end;
end;

function TGPSInformation.GetMeasureUnitType: TConvType;
begin
  Result := FMeasureUnitType;
end;

procedure TGPSInformation.SetEnabled(const Value: Boolean);
begin
  Assert(Assigned(Parser), 'NMEAParser must be assigned in TGPSInformation.SetEnabled');
  if Value then begin
    try
      Parser.PortEnabled := True;
      Parser.GetComStatus; // just so we get some NMEA back
      LoggedConnectionError := False;
    except
      on E: Exception do begin
        // GPSTools was unable to establish a connection. Most likely caused by
        // users without GPSGate using the -1 option. We need to log this the
        // first time and eat the rest. Once we are able to establish a
        // connection reset the boolean.
        if not LoggedConnectionError then begin
          LogMessage('SetEnabled cannot open GPSGate; ' + E.Message);
          LoggedConnectionError := True;
        end;
      end;
    end;
  end else begin
    Parser.PortEnabled := False;
  end;
end;

function TGPSInformation.GetEnabled: Boolean;
var
  Status: IComStatus;
begin
  Result := False;
  if not Assigned(Parser) then
    Exit;

  try
    Status := IUnknown(Parser.GetComStatus) as IComStatus;
    if Assigned(Status) then
      Result := Status.ValidNmea;
  except
    on E: Exception do
      LogMessage('GetEnabled cannot get GPS status; ' + E.Message);
  end;
end;

procedure TGPSInformation.LogMessage(const Msg: string);
begin
  if Assigned(FOnLog) then
    FOnLog(Self, Msg);
end;

{ TGPSPositionInfo }

constructor TGPSPositionInfo.Create(UnitType: TConvType);
begin
  MeasureUnitType := UnitType;
  HaveFix := False;
  HavePosition := False;
  SatellitesUsed := 0;
  HDOP := -1;
end;

destructor TGPSPositionInfo.Destroy;
begin
  inherited;
end;

function TGPSPositionInfo.DistanceFrom(Latitude2, Longitude2: Double;
  UnitType: TConvType): Double;
var
  Meters: Double;
begin
  if (not HaveFix) or (Longitude2 = 0) or (Latitude2 = 0) then
    Meters := 0
  else
    Meters := LatLongDistanceMeters(Latitude, Longitude, Latitude2, Longitude2);

  Result := Convert(Meters, duMeters, UnitType);
end;

end.
