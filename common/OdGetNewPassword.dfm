object GetNewPasswordForm: TGetNewPasswordForm
  Left = 385
  Top = 261
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Change Password'
  ClientHeight = 266
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object ConfirmLabel: TLabel
    Left = 16
    Top = 90
    Width = 41
    Height = 13
    Caption = 'Confirm:'
  end
  object InstructionsLabel: TLabel
    Left = 15
    Top = 126
    Width = 266
    Height = 35
    AutoSize = False
    Caption = 
      'Enter your current password and your new password twice to confi' +
      'rm it.'
    WordWrap = True
  end
  object CurrentLabel: TLabel
    Left = 16
    Top = 54
    Width = 74
    Height = 13
    Caption = 'New Password:'
  end
  object Label1: TLabel
    Left = 16
    Top = 18
    Width = 93
    Height = 13
    Caption = 'Current Password :'
  end
  object ProblemLabel: TLabel
    Left = 16
    Top = 162
    Width = 273
    Height = 52
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object CancelButton: TButton
    Left = 157
    Top = 222
    Width = 65
    Height = 25
    Action = CancelAction
    Cancel = True
    ModalResult = 2
    TabOrder = 4
  end
  object OkButton: TButton
    Left = 85
    Top = 222
    Width = 65
    Height = 25
    Action = OKAction
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object NewPasswordEdit: TEdit
    Left = 120
    Top = 51
    Width = 161
    Height = 21
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 1
  end
  object ConfirmPasswordEdit: TEdit
    Left = 120
    Top = 87
    Width = 161
    Height = 21
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 2
  end
  object CurrentPasswordEdit: TEdit
    Left = 120
    Top = 15
    Width = 161
    Height = 21
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 0
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 232
    Top = 228
    object OKAction: TAction
      Caption = 'OK'
      OnExecute = OKActionExecute
    end
    object CancelAction: TAction
      Caption = 'Cancel'
      OnExecute = CancelActionExecute
    end
  end
end
