unit SqlJsonOutput;

interface

uses
  Data.DB, uADCompClient, uADStanOption, AdUtils, SuperObject, OdDbUtils;

type
  TDataSetInfoEvent = procedure (var TableName: string; var Count: Integer) of object;

function FormatSqlJson(MultiResultDataSet: TADRdbmsDataSet; SkipFields: array of string; InitialSizeMB: Integer = 1; OnProgress: TDataSetInfoEvent = nil; MaxRows: Integer = 0): string;
function CreatePathedJsonFromDataSet(const Data: TDataSet): ISuperObject;

implementation

uses
  System.SysUtils, System.Classes, OdMiscUtils, OdIsoDates;

const
  JSONBools: array[0..1] of string = ('false', 'true');

// Use FormatSqlJson for the fastest possible way to get back a dataset in JSON format.
// This assumes the first column is the table name to use for returned rows
function FormatSqlJson(MultiResultDataSet: TADRdbmsDataSet; SkipFields: array of string;
  InitialSizeMB: Integer; OnProgress: TDataSetInfoEvent;
  MaxRows: Integer): string;
var
  Doc: TStringStream;
  DSet: TADMemTable;
  TotalRecs: Integer;

  procedure UpdateProgress(TableName: string; Count: Integer);
  begin
    if Assigned(OnProgress) then
      OnProgress(TableName, Count);
  end;

  procedure WriteFieldName(Field: TField);
  begin
    Doc.WriteString('"');
    Doc.WriteString(Field.FieldName);
    Doc.WriteString('"');
  end;

  function IsQuotedDataType(FT: TFieldType): Boolean;
  begin
    Result := not(FT in [ftSmallint, ftInteger, ftWord, ftBoolean, ftFloat,
      ftCurrency, ftLargeint, ftLongWord, ftShortint, ftExtended, ftSingle,
      ftAutoInc]);
  end;

  procedure WriteFieldValue(Field: TField);
  var
    NeedQuotes: Boolean;
  begin
    NeedQuotes := IsQuotedDataType(Field.DataType);
    if NeedQuotes then
      Doc.WriteString('"');
    if Field.DataType in [ftString, ftMemo] then
      Doc.WriteString(JSONEscape(Field.AsString))
    else if Field.DataType in [ftDateTime, ftTimeStamp] then
      Doc.WriteString(IsoDateTimeToStr(Field.AsDateTime))
    else if Field.DataType = ftDate then
      Doc.WriteString(IsoDateToStr(Field.AsDateTime))
    else if Field.DataType = ftBoolean then
      Doc.WriteString(JSONBools[Abs(Integer(Field.AsBoolean))])
    else
      Doc.WriteString(Field.AsString);
    if NeedQuotes then
      Doc.WriteString('"');
  end;

  procedure DoRow(D: TDataSet);
  var
    i: Integer;
    NeedComma: Boolean;
  begin
    NeedComma := False;
    for i := 0 to D.FieldCount - 1 do begin
      if StringInArray(D.Fields[i].FieldName, SkipFields) then
        Continue;

      if not D.Fields[i].IsNull then begin
        if NeedComma then
          Doc.WriteString(',')
        else
          NeedComma := True;
        WriteFieldName(D.Fields[i]);
        Doc.WriteString(':');
        WriteFieldValue(D.Fields[i]);
      end;
    end;
    Doc.WriteString('}');
  end;

  procedure Process(D: TDataSet; Child: Boolean);
  var
    TableName: string;
    RowCount: Integer;
  begin
    RowCount := 0;
    // the sync sp adds the table name into the results tname field.
    if Assigned(D.FindField('tname')) then
      TableName := D.FieldByName('tname').AsString
    // use the first real fieldname to get something meaningful
    else if D.FieldCount > 1 then
      TableName := D.Fields[1].FieldName
    else
      TableName := D.Fields[0].FieldName;
    UpdateProgress(TableName, RowCount);

    if not Child then
      Doc.WriteString('{"table_data":[');

    while not D.Eof do begin
      if Child then
        Doc.WriteString(CRLF);
      Doc.WriteString('{');
      DoRow(D);
      D.Next;
      if not D.EOF then
        Doc.WriteString(',');
      Inc(TotalRecs);
      Inc(RowCount);
      if RowCount mod 100 = 0 then
        UpdateProgress(TableName, RowCount);
      if TotalRecs mod 100 = 0 then
        Sleep(1);       // this will share the CPU more nicely
      if (MaxRows > 0) and (RowCount > MaxRows) then
        raise Exception.Create('Unexpectedly large dataset in FormatSqlJson, Table: ' + TableName);
    end;
    UpdateProgress(TableName, RowCount);
  end;

begin
  DSet := nil;
  // Builds the JSON string using a memory stream for best performance.
{$IFDEF UNICODE}
  Doc := TStringStream.Create('',  TEncoding.UTF8);
{$ELSE}
  Doc := TStringStream.Create('',  TEncoding.ANSI);
{$ENDIF}
  try
    Doc.SetSize(1024 * 1024 * InitialSizeMB);
    DSet := TADMemTable.Create(nil);
    DSet.FetchOptions.CursorKind := ckForwardOnly;
    DSet.FetchOptions.Mode := fmOnDemand;
    DSet.FetchOptions.RowsetSize := 200;
    DSet.FetchOptions.AutoFetchAll := afAll;
    DSet.FetchOptions.Unidirectional := True;
    TotalRecs := 0;
    Process(MultiResultDataSet, False);
    LoadNextRecordset(MultiResultDataSet, DSet);
    while (MultiResultDataSet.Active) do begin
      if DSet.HasRecords then begin
        Doc.WriteString(',');
        Process(DSet, True);
      end;
      DSet.Close;
      LoadNextRecordset(MultiResultDataSet, DSet);
    end;
    if Doc.Position > 0 then
      Doc.WriteString(']}');
    Doc.Size := Doc.Position;
    Result := Doc.DataString;
  finally
    FreeAndNil(Doc);
    FreeAndNil(DSet);
  end;
end;

// TODO: This should use the efficient streaming method of building JSON instead of SO.
function CreatePathedJsonFromDataSet(const Data: TDataSet): ISuperObject;
var
  I: Integer;
  F: TField;
  Path: string;
begin
  Result := SO;
  while not Data.Eof do begin
    for I := 1 to Data.FieldCount - 1 do begin
      F := Data.Fields[I];
      if not F.IsNull then begin
        Path := Data.Fields[0].AsString + '.' + F.FieldName;
        if F.DataType = ftDateTime then
          Result.S[Path] := IsoDateTimeToStr(F.AsDateTime)
        else if F.DataType = ftLargeint then
          Result.I[Path] := F.AsLargeInt
        else if F.DataType = ftInteger then
          Result.I[Path] := F.AsInteger
        else if F.DataType = ftCurrency then
          Result.C[Path] := F.AsCurrency
        else if F.DataType = ftFloat then
          Result.D[Path] := F.AsFloat
        else
          // Escape CRLF, and nothing else, because 'minimal' escape mode does
          // does the rest already.
          Result.S[Path] := StringReplace(
            StringReplace(F.AsString, #13, '' ,[rfReplaceAll]),
            #10, '\n' ,[rfReplaceAll]);
      end;
    end;
    Data.Next;
  end;
end;

end.
