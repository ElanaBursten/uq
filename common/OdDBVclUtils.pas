unit OdDBVclUtils;

interface

uses DB, DBGrids, Classes, Graphics, Windows, StdCtrls, ComCtrls;

function GetGridColumn(Grid: TDBGrid; const FieldName: string): TColumn;
procedure DrawGridField(const Value: string; const Rect: TRect; Canvas: TCanvas;
    Font: TFont; Alignment: TAlignment; FontStyle: TFontStyles; FontColor, BGColor: TColor);
procedure AutoSizeDBGridColumns(const DBGrid: TDBGrid);
procedure HighlightMissingColumnFields(Grid: TDBGrid; Color: TColor);
procedure FillComboFromDataSet(Combo: TCustomComboBox; DataSet: TDataSet; const DisplayFieldsValues: array of string; const ObjectField: string = '');
function TreeToDataSet(TreeView: TTreeView; DataSet: TDataSet): TDataSet;
procedure SetGridDisabledColColor(Grid: TDBGrid; DisabledColor: TColor = $00EBEBEB);

implementation

uses SysUtils, OdDBUtils, OdMiscUtils;

function GetGridColumn(Grid: TDBGrid; const FieldName: string): TColumn;
var
  i: Integer;
begin
  Assert(Assigned(Grid));
  Assert(Trim(FieldName) <> '');
  Result := nil;

  for i := 0 to Grid.Columns.Count - 1 do begin
    if Grid.Columns[i].FieldName = FieldName then begin
      Result := Grid.Columns[i];
      Break;
    end;
  end;
end;

// http://www.delphicorner.f9.co.uk/articles/comps8.htm
procedure DrawGridField(const Value: string; const Rect: TRect; Canvas: TCanvas;
    Font: TFont; Alignment: TAlignment; FontStyle: TFontStyles; FontColor, BGColor: TColor);
var
  i: Integer;
begin
  i := 0;

  // First, fill in the background color of the cell
  Canvas.Brush.Color := BGColor;
  Canvas.FillRect(Rect);
  // SetBkMode ensures that the background is transparent
  SetBkMode(Canvas.Handle, TRANSPARENT);

  // Set the passed font properties
  Canvas.Font := Font;
  Canvas.Font.Color := FontColor;
  Canvas.Font.Style := Canvas.Font.Style + FontStyle;

  // Set Text Alignment
  case Alignment of
    taRightJustify :
      begin
        SetTextAlign(Canvas.Handle, TA_RIGHT);
        i := Rect.Right - 2;
      end;

    taLeftJustify :
      begin
        SetTextAlign(Canvas.Handle, TA_LEFT);
        i := Rect.Left + 2;
      end;

    taCenter      :
      begin
        SetTextAlign(Canvas.Handle, TA_CENTER);
        i := (Rect.Right + Rect.Left) DIV 2;
      end;
  end;    { case }

  // Draw the text
  Canvas.TextRect(Rect, i, Rect.Top + 2, Value);
  SetTextAlign(Canvas.Handle, TA_LEFT);
end;

// Note: The fields that are to be auto-sized to fill the gap should
// have a min-width in the Field's (not column's) Tag property
// http://delphi.about.com/od/usedbvcl/l/aa050404a.htm
procedure AutoSizeDBGridColumns(const DBGrid: TDBGrid);
var
  i: Integer;
  TotWidth: Integer;
  VarWidth: Integer;
  ResizableColumnCount: Integer;
  AColumn: TColumn;
begin
  // Total width of all columns before resize
  TotWidth := 0;
  // How many columns need to be auto-resized
  ResizableColumnCount := 0;

  if (DBGrid.Columns.Count > 0) and (not Assigned(DBGrid.Columns[0].Field)) then
    Exit; // No fields have been created yet

  for i := 0 to DBGrid.Columns.Count - 1 do
  begin
    TotWidth := TotWidth + DBGrid.Columns[i].Width;
    Assert(Assigned(DBGrid.Columns[i].Field), 'No field for grid column');
    if DBGrid.Columns[i].Field.Tag <> 0 then
      Inc(ResizableColumnCount);
  end;

  // Add 1px for the column separator line
  if dgColLines in DBGrid.Options then
    TotWidth := TotWidth + DBGrid.Columns.Count;

  // Add indicator column width
  if dgIndicator in DBGrid.Options then
    TotWidth := TotWidth + IndicatorWidth + 1;

  // Width value "left"
  VarWidth := DBGrid.ClientWidth - TotWidth;

  // Equally distribute VarWidth to all auto-resizable columns
  if ResizableColumnCount > 0 then
    VarWidth := Trunc(varWidth / ResizableColumnCount);

  for i := 0 to -1 + DBGrid.Columns.Count do
  begin
    AColumn := DBGrid.Columns[i];
    if AColumn.Field.Tag <> 0 then
    begin
      AColumn.Width := AColumn.Width + VarWidth;
      if AColumn.Width < AColumn.Field.Tag then
        AColumn.Width := AColumn.Field.Tag;
    end;
  end;
end;

procedure HighlightMissingColumnFields(Grid: TDBGrid; Color: TColor);
var
  i: Integer;
  Column: TColumn;
begin
  Assert(Assigned(Grid.DataSource));
  if not Assigned(Grid.DataSource.DataSet) then
    Exit;

  for i := 0 to Grid.Columns.Count - 1 do begin
    Column := Grid.Columns[i];
    if Column.Visible and (not HaveField(Grid.DataSource.DataSet, Column.FieldName)) then
      Column.Color := Color;
  end;
end;

procedure FillComboFromDataSet(Combo: TCustomComboBox; DataSet: TDataSet; const DisplayFieldsValues: array of string; const ObjectField: string);
var
  ItemText: string;
  FieldValue: string;
  i: Integer;
  Field: TField;
begin
  Assert(Assigned(Combo));
  Assert(Assigned(DataSet));
  Assert(DataSet.Active);
  Assert(Length(DisplayFieldsValues) > 0);

  DataSet.First;
  Combo.Clear;
  while not DataSet.Eof do begin
    ItemText := '';
    for i := 0 to Length(DisplayFieldsValues) - 1 do begin
      Field := DataSet.FindField(DisplayFieldsValues[i]);
      if Assigned(Field) then
        FieldValue := DataSet.FieldByName(DisplayFieldsValues[i]).AsString
      else
        FieldValue := DisplayFieldsValues[i];
     ItemText := ItemText + FieldValue;
    end;
    if NotEmpty(ObjectField) then
      Combo.Items.AddObject(ItemText, TObject(DataSet.FieldByName(ObjectField).AsInteger))
    else
      Combo.Items.Add(ItemText);
    DataSet.Next;
  end;
end;

function TreeToDataSet(TreeView: TTreeView; DataSet: TDataSet): TDataSet;

  procedure CreateRows(Node: TTreeNode);
  var
    i: Integer;
  begin
    Assert(Assigned(Node));
    DataSEt.Append;
    DataSet.FieldByName('Indent').AsInteger := Node.Level;
    DataSet.FieldByName('Text').AsString := Node.Text;
    DataSet.Post;
    if Node.Expanded then
      for i := 0 to Node.Count - 1 do
        CreateRows(Node.Item[i]);
  end;

var
  RootNode: TTreeNode;
begin
  Assert(Assigned(TreeView));
  Assert(Assigned(DataSet));
  Assert(DataSet.Active);
  while not DataSet.RecordCount = 0 do
    DataSet.Delete;
  if TreeView.Items.Count > 0 then begin
    RootNode := TreeView.Items[0];
    CreateRows(RootNode);
    while (RootNode.getNextSibling <> nil) do begin
      RootNode := RootNode.getNextSibling;
      CreateRows(RootNode);
    end;
  end;
  Result := DataSet;
end;

procedure SetGridDisabledColColor(Grid: TDBGrid; DisabledColor: TColor);
var
  i: Integer;
  Col: TColumn;
begin
  Assert(Assigned(Grid));
  for i := 0 to Grid.Columns.Count - 1 do begin
    Col := Grid.Columns[i];
    if Col.ReadOnly then
      Col.Color := DisabledColor
    else
      Col.Color := clWindow;
  end;
end;

end.
