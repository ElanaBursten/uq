unit OdWebUpdateShared;

interface

const
  SupportedIniFileFormatVersion = 1;
  ConfigSection = 'WebUpdateConfiguration';
  FileFormatVersionIdent = 'FileFormatVersion';
  NumberOfFilesIdent = 'NumberOfFiles';
  LastUpdateIdent = 'LastUpdate';
  FileDetailsPrefix = 'File';
  FileNameIdent = 'Name';
  FileDirectoryIdent = 'Directory';
  FileLocalDirectoryIdent = 'LocalDirectory';
  FileURLIdent = 'URL';
  FileDateTimeIdent = 'DateTime';
  FileSizeIdent = 'Size';
  FileCRC32Ident = 'CRC32';
  FileCRCVerifyOnlyIdent = 'CRCVerifyOnly';
  FileMajorVersionIdent = 'MajorVersion';
  FileMinorVersionIdent = 'MinorVersion';
  FileReleaseVersionIdent = 'ReleaseVersion';
  FileBuildVersionIdent = 'BuildVersion';
  UpdateDownloadDirectory = 'WebUpdate';
  UpdateIniFileName = 'Update.ini';
  MainApplicationIdent = 'MainApplication';
  MainApplicationMutexIdent = 'MainApplicationMutex';
  DoUpdateExeName = 'DoUpdate.exe';
  AfterUpdateParameter = 'updated';

implementation

end.
