unit OdDBISAMXml;

// TODO: Make this into something more OO
// TODO: Report the number of changes made and types of changes?

{$BOOLEVAL ON} // The Changes flags need this

interface

uses DBISAMTb, MSXML2_TLB;

procedure SaveDBISAMSchemaToXML(Database: TDBISAMDatabase; const FileName: string);
procedure ApplyXMLSchemaToDatabase(Database: TDBISAMDatabase; Doc: IXMLDOMDocument);

implementation

uses SysUtils, Classes, DB, TypInfo, OdMiscUtils, OdMSXMLUtils;

type
  TLocalIndexDef = TDBISAMIndexDef;
  TLocalFieldDef = TDBISAMFieldDef;
  TLocalRestrDef = TDBISAMFieldDef;
  TLocalRestrIndexDef = TDBISAMIndexDef;
  TLocalRestrIndexDefs = TDBISAMIndexDefs;

(*
  Output Format:
  <table name="reference">
    <index name="default" fields="ref_id;ref_name" options="[ixCaseInsensitive]" />
    <field name="ref_name" type="ftString" size="15" precision="0" attributes="faReadonly" />
  </table>
*)
procedure AddTableXml(Table: TDBISAMTable; Xml: TStrings);
var
  i: Integer;
  IndexDef: TDBISAMIndexDef;
  FieldDef: TDBISAMFieldDef;
  FieldType: string;
  FieldAttributes: string;
  IndexOptions: string;
  FieldSize: string;
  FieldPrecision: string;
begin
   Assert(Assigned(Table) and Assigned(Xml));
   Table.IndexDefs.Update;
   Table.FieldDefs.Update;

   Xml.Add(Format('  <table name="%s">', [Table.TableName]));
   for i := 0 to Table.IndexDefs.Count -1 do begin
     IndexDef := Table.IndexDefs.Items[i];
     IndexOptions := SetToString(GetPropInfo(IndexDef, 'Options'), Byte(IndexDef.Options));
     if IndexOptions <> '' then
       IndexOptions := Format('options="%s" ', [IndexOptions]);
     Xml.Add(Format('    <index name="%s" fields="%s" %s/>',
       [XmlEncodeString(IndexDef.DisplayName), IndexDef.Fields, IndexOptions]));
   end;
   for i := 0 to Table.FieldDefs.Count -1 do begin
     FieldDef := Table.FieldDefs[i];
     FieldType := GetEnumName(TypeInfo(TFieldType), Ord(FieldDef.DataType));
     // We only support the faReadOnly attribute (all we need that DBISAM supports)
     FieldAttributes := SetToString(GetPropInfo(FieldDef, 'Attributes'), Byte(FieldDef.Attributes));
     if FieldAttributes <> '' then
       FieldAttributes := Format('attributes="%s" ', [FieldAttributes]);
     FieldSize := '';
     if FieldDef.Size <> 0 then
       FieldSize := Format('size="%d" ', [FieldDef.Size]);
     FieldPrecision := '';
     //if FieldDef.Precision <> 0 then
     //  FieldPrecision := Format('precision="%d" ', [FieldDef.Precision]);

     Xml.Add(Format('    <field name="%s" type="%s" %s%s%s/>',
       [FieldDef.Name, FieldType, FieldSize, FieldPrecision, FieldAttributes]));
   end;
   Xml.Add('  </table>');
end;

procedure SaveDBISAMSchemaToXML(Database: TDBISAMDatabase; const FileName: string);
var
  i: Integer;
  TableNames: TStringList;
  Xml: TStringList;
  Table: TDBISAMTable;
begin
  Assert(Assigned(Database));
  Assert(Trim(FileName) <> '');
  Database.Open;

  Table := nil;
  Xml := nil;
  TableNames := TStringList.Create;
  try
    Xml := TStringList.Create;
    Xml.Add('<?xml version="1.0"?>');
    Xml.Add('<ODXMLSchema version="1.0">');
    Database.Handle.ListTableNames(TableNames);
    TableNames.Sort;
    Table := TDBISAMTable.Create(nil);
    Table.DatabaseName := Database.DatabaseName;
    for i := 0 to TableNames.Count - 1 do begin
      Table.TableName := TableNames[i];
      AddTableXml(Table, Xml);
    end;
    Xml.Add('</ODXMLSchema>');
    Xml.SaveToFile(FileName);
  finally
    FreeAndNil(Table);
    FreeAndNil(TableNames);
    FreeAndNil(Xml);
  end;
end;

procedure DebugLog(Msg: string);
begin
  //OutputDebugString(PChar(Msg));
end;

function RemoveOldTables(Database: TDBISAMDatabase; LocalTables, XmlTables: TStringList): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to LocalTables.Count - 1 do
    if XmlTables.IndexOf(LocalTables[i]) = -1 then begin
      DebugLog('Deleting table: ' + LocalTables[i]);

{$IFDEF  VER185}
    Database.Handle.DeleteDataTable(AnsiString(LocalTables[i]));
{$ELSE}
      Database.Handle.DeleteTable(AnsiString(LocalTables[i]));
{$ENDIF}
      Result := True;
    end;
end;


function AddNewTables(Database: TDBISAMDatabase; LocalTables, XmlTables: TStringList): Boolean;
var
  i: Integer;
  Table: TDBISAMTable;
begin
  Result := False;
  for i := 0 to XmlTables.Count - 1 do begin
    Table := TDBISAMTable.Create(nil);
    try
      if LocalTables.IndexOf(XmlTables[i]) = -1 then begin
        DebugLog('Adding table: ' + XmlTables[i]);
        Table.Close;
        Table.DatabaseName := Database.DatabaseName;
        Table.TableName := XmlTables[i];
        // Add a temporary/dummy field (needed for DBISAM4)
        Table.FieldDefs.Add('ZZZ', ftString, 1);
        Table.CreateTable;
        Result := True;
      end;
    finally
      FreeAndNil(Table);
    end;
  end;
end;

procedure GetNamesOfNodes(XmlNodes: IXMLDOMNodeList; XmlNames: TStrings);
var
  i: Integer;
  Success: Boolean;
  NodeName: string;
begin
  Assert(Assigned(XmlNodes) and Assigned(XmlNames));

  XmlNames.Clear;
  for i := 0 to XmlNodes.length - 1 do begin
    Success := GetAttributeTextFromNode(XmlNodes.item[i], 'name', NodeName);
    Assert(Success);
    Assert(NodeName <> '');
    XmlNames.Add(NodeName);
  end;
end;

function RemoveOldFields(Table: TDBISAMTable; XmlColumns: TStringList): Boolean;
var
  i: Integer;
  FieldToCheck: string;
  RestructureField: TDBISAMFieldDef;
begin
  Assert(Assigned(Table) and Assigned(XmlColumns));
  Result := False;

  for i := Table.FieldDefs.Count - 1 downto 0 do begin
    FieldToCheck := Table.FieldDefs[i].Name;
    if XmlColumns.IndexOf(FieldToCheck) = -1 then begin
      RestructureField := Table.FieldDefs.Find(FieldToCheck);
      Assert(Assigned(RestructureField));
      Table.FieldDefs.Delete(Table.FieldDefs.IndexOf(FieldToCheck));
      Result := True;
    end;
  end;
end;

function AddNewFields(Table: TDBISAMTable; XmlFieldNodes: IXMLDOMNodeList): Boolean;
var
  i: Integer;
  XmlFieldNode: IXMLDOMNode;
  XmlFieldName: string;
  Success: Boolean;
  NextFieldNumber: Integer;
begin
  Result := False;
  Assert(Assigned(Table) and Assigned(XmlFieldNodes));
  for i := 0 to XmlFieldNodes.length - 1 do begin
    XmlFieldNode := XmlFieldNodes.item[i];
    Success := GetAttributeTextFromNode(XmlFieldNode, 'name', XmlFieldName);
    Assert(Success);
    if Table.FieldDefs.IndexOf(XmlFieldName) = -1 then begin
      DebugLog('  Adding field: ' + XmlFieldName);
      // For some reason this is a 1 based index
      NextFieldNumber := Table.FieldDefs.Count + 1;
      // Add the field type as ftString for now (fixed up in ModifyChangedFields)
      Table.FieldDefs.Add(NextFieldNumber, XmlFieldName, ftString, 1, False);
      Result := True;
    end;
  end;
end;

procedure ParseXmlIntoFieldDef(Element: IXMLDOMElement; FieldDef: TLocalFieldDef);
var
  AttributeText: string;
  Success: Boolean;
  IntValue: Integer;
begin
  Assert(Assigned(Element) and Assigned(FieldDef));

  Success := GetAttributeTextFromNode(Element, 'name', AttributeText);
  Assert(Success);
  Assert(AttributeText <> '');
  FieldDef.Name := AttributeText;

  Success := GetAttributeTextFromNode(Element, 'type', AttributeText);
  Assert(Success);
  Assert(AttributeText <> '');
  IntValue := GetEnumValue(TypeInfo(TFieldType), AttributeText);
  Assert((IntValue >= Ord(Low(TFieldType))) and (IntValue <= Ord(High(TFieldType))));
  FieldDef.DataType := TFieldType((IntValue));

  GetAttributeTextFromNode(Element, 'size', AttributeText);
  if AttributeText = '' then
    FieldDef.Size := 0
  else
    FieldDef.Size := StrToInt(AttributeText);

  GetAttributeTextFromNode(Element, 'precision', AttributeText);

  GetAttributeTextFromNode(Element, 'attributes', AttributeText);
  if AttributeText = '' then
    FieldDef.Attributes := []
  else begin
    IntValue := StringToSet(GetPropInfo(FieldDef, 'Attributes'), AttributeText);
    FieldDef.Attributes := TFieldAttributes(Byte(IntValue));
  end;
end;

function ApplyAnyFieldChangesToField(TableFieldDef: TDBISAMFieldDef;
  XmlFieldDef: TDBISAMFieldDef): Boolean;
begin
  Result := False;
  if TableFieldDef.Name <> XmlFieldDef.Name then begin
    // This should never happen, since the name is set for new fields already?
    TableFieldDef.Name := XmlFieldDef.Name;
    Result := True;
  end;
  if TableFieldDef.DataType <> XmlFieldDef.DataType then begin
    TableFieldDef.DataType := XmlFieldDef.DataType;
    Result := True;
  end;
  if TableFieldDef.Size <> XmlFieldDef.Size then begin
    TableFieldDef.Size := XmlFieldDef.Size;
    Result := True;
  end;
  // We ignore precision changes until DBISAM supports them
  (*
  if TableFieldDef.Precision <> XmlFieldDef.Precision then begin
    TableFieldDef.Precision := XmlFieldDef.Precision;
    Result := True;
  end;
  *)
  // We only support the faReadOnly attribute (all we need that DBISAM supports)
  if TableFieldDef.Required <> (faRequired in XmlFieldDef.Attributes) then begin
    TableFieldDef.Required := faRequired in XmlFieldDef.Attributes;
    Result := True;
  end;
end;

function ModifyChangedFields(Table: TDBISAMTable; XmlFieldNodes: IXMLDOMNodeList): Boolean;
var
  i: Integer;
  TableFieldDef: TDBISAMFieldDef;
  XmlFieldDef: TDBISAMFieldDef;
  XmlFieldElement: IXMLDOMElement;
begin
  Assert(Assigned(Table) and Assigned(XmlFieldNodes));
  Result := False;

  XmlFieldDef := TDBISAMFieldDef.Create(nil);
  try
    for i := 0 to XmlFieldNodes.length - 1 do begin
      XmlFieldElement := XmlFieldNodes.item[i] as IXMLDOMElement;
      ParseXmlIntoFieldDef(XmlFieldElement, XmlFieldDef);
      TableFieldDef := Table.FieldDefs.Find(XmlFieldDef.Name);
      Assert(Assigned(TableFieldDef));

      Result := Result or ApplyAnyFieldChangesToField(TableFieldDef, XmlFieldDef);
    end;
  finally
    FreeAndNil(XmlFieldDef);
  end;
end;

procedure ReorderFields(Table: TDBISAMTable; XmlFieldNodes: IXMLDOMNodeList);
var
  i: Integer;
  FieldDef: TDBISAMFieldDef;
  FieldName: string;
  Success: Boolean;
begin
  Assert(Assigned(Table) and Assigned(XmlFieldNodes));
  Assert(Table.FieldDefs.Count = XmlFieldNodes.length);

  for i := 0 to XmlFieldNodes.Length - 1 do begin
    Success := GetAttributeTextFromNode(XmlFieldNodes.item[i], 'name', FieldName);
    Assert(Success);
    FieldDef := Table.FieldDefs.Find(FieldName);
    Assert(Assigned(FieldDef));
    FieldDef.FieldNo := i + 1;
  end;
end;

function RemoveOldIndexes(Table: TDBISAMTable; XmlIndexes: TStringList): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Table.IndexDefs.Count - 1 downto 0 do begin
    if XmlIndexes.IndexOf(Table.IndexDefs[i].Name) = -1 then begin
      Table.IndexDefs.Delete(i);
      Result := True;
    end;
  end;
end;

procedure ParseXmlIntoIndexDef(Element: IXMLDOMElement; IndexDef: TDBISAMIndexDef);
var
  AttributeText: string;
  Success: Boolean;
  IntValue: Integer;
begin
  Assert(Assigned(Element) and Assigned(IndexDef));

  Success := GetAttributeTextFromNode(Element, 'name', AttributeText);
  Assert(Success);
  Assert(AttributeText <> '');
  IndexDef.Name := AttributeText;

  Success := GetAttributeTextFromNode(Element, 'fields', AttributeText);
  Assert(Success);
  Assert(AttributeText <> '');
  IndexDef.Fields := AttributeText;

  GetAttributeTextFromNode(Element, 'options', AttributeText);
  if AttributeText = '' then
    IndexDef.Options := []
  else begin
    IntValue := StringToSet(GetPropInfo(IndexDef, 'options'), AttributeText);
    IndexDef.Options := TIndexOptions(Byte(IntValue));
  end;
end;

function ApplyAnyIndexChangesToIndexDef(TableIndexDef: TDBISAMIndexDef;
  XmlIndexDef: TDBISAMIndexDef): Boolean;
begin
  Assert(Assigned(TableIndexDef) and Assigned(XmlIndexDef));
  Assert(SameText(TableIndexDef.Name, XmlIndexDef.Name));
  Result := False;

  if not SameText(TableIndexDef.Fields, XmlIndexDef.Fields) then begin
    TableIndexDef.Fields := XmlIndexDef.Fields;
    Result := True;
  end;

  if not (TableIndexDef.Options = XmlIndexDef.Options) then begin
    TableIndexDef.Options := XmlIndexDef.Options;
    Result := True;
  end;
end;

function ModifyChangedIndexes(Table: TDBISAMTable; XmlIndexNodes: IXMLDOMNodeList): Boolean;
var
  i: Integer;
  IndexElement: IXMLDOMElement;
  RestructureIndex: Integer;
  RestructureIndexDef: TDBISAMIndexDef;
  XmlIndexDef: TDBISAMIndexDef;
begin
  Assert(Assigned(Table) and Assigned(XmlIndexNodes));
  Result := False;

  XmlIndexDef := TDBISAMIndexDef.Create(nil);
  try
    for i := 0 to XmlIndexNodes.length - 1 do begin
      IndexElement := XmlIndexNodes.item[i] as IXMLDOMElement;
      ParseXmlIntoIndexDef(IndexElement, XmlIndexDef);
      RestructureIndex := Table.IndexDefs.IndexOf(XmlIndexDef.Name);
      if RestructureIndex = -1 then begin
        Table.IndexDefs.Add(XmlIndexDef.Name, XmlIndexDef.Fields, XmlIndexDef.Options);
        Result := True;
      end
      else begin
        RestructureIndexDef := Table.IndexDefs[RestructureIndex];
        Assert(Assigned(RestructureIndexDef));
        Result := Result or ApplyAnyIndexChangesToIndexDef(RestructureIndexDef, XmlIndexDef);
      end;
    end;
  finally
    FreeAndNil(XmlIndexDef);
  end;
end;

procedure SetPrimaryKeyIndexNames(IndexDefs: TDBISAMIndexDefs);
var
  i: Integer;
begin
  Assert(Assigned(IndexDefs));
  // DBISAM transforms unnamed primary key indexes into the name '<Primary>'
  // when the fields are created for a table
  for i := 0 to IndexDefs.Count - 1 do begin
    if (IndexDefs[i].Name = '') and (ixPrimary in IndexDefs[i].Options) then
      IndexDefs[i].Name := '<Primary>';
  end;
end;

procedure SynchronizeFieldsAndIndexes(Database: TDBISAMDatabase;
  const TableName: string; TableNode: IXMLDOMNode);
var
  Table: TDBISAMTable;
  XmlFieldNodes: IXMLDOMNodeList;
  XmlIndexNodes: IXMLDOMNodeList;
  Changes: Boolean;

  procedure MakeFieldChanges;
  var
    XmlFields: TStringList;
  begin
    XmlFields := TStringList.Create;
    try
      XmlFieldNodes := TableNode.selectNodes(Format('//table[@name = "%s"]/field', [TableName]));
      Assert(Assigned(XmlFieldNodes));
      GetNamesOfNodes(XmlFieldNodes, XmlFields);

      Changes := Changes or RemoveOldFields(Table, XmlFields);
      Changes := Changes or AddNewFields(Table, XmlFieldNodes);
      Assert(XmlFields.Count = Table.FieldDefs.Count);
      Changes := Changes or ModifyChangedFields(Table, XmlFieldNodes);
      //if Changes then
      //  ReorderFields(Table, XmlFieldNodes);
    finally
      FreeAndNil(XmlFields);
    end;
  end;

  procedure MakeIndexChanges;
  var
    XmlIndexes: TStringList;
  begin
    XmlIndexes := TStringList.Create;
    try
      XmlIndexNodes := TableNode.selectNodes(Format('//table[@name = "%s"]/index', [TableName]));
      if Assigned(XmlIndexNodes) and (XmlIndexNodes.length > 0) then
        GetNamesOfNodes(XmlIndexNodes, XmlIndexes);
      Changes := Changes or RemoveOldIndexes(Table, XmlIndexes);
      Changes := Changes or ModifyChangedIndexes(Table, XmlIndexNodes);
      Assert(XmlIndexes.Count = Table.IndexDefs.Count);
    finally
      FreeAndNil(XmlIndexes);
    end;
  end;

begin
  Assert(Assigned(Database) and Assigned(TableNode) and (Trim(TableName) <> ''));

  DebugLog('Checking table: ' + TableName);
  Changes := False;
  Table := TDBISAMTable.Create(nil);
  try
    Table.DatabaseName := Database.DatabaseName;
    Table.TableName := TableName;
    Table.Exclusive := True;

    Table.FieldDefs.Update;
    Table.IndexDefs.Update;
    SetPrimaryKeyIndexNames(Table.IndexDefs);

    MakeFieldChanges;
    MakeIndexChanges;

    if Changes then
      Table.AlterTable;
  finally
    FreeAndNil(Table);
  end;
end;

procedure ApplyXMLSchemaToDatabase(Database: TDBISAMDatabase; Doc: IXMLDOMDocument);
var
  i: Integer;
  LocalTables: TStringList;
  XmlTables: TStringList;
  TableNode: IXMLDOMNode;
  TableNodes: IXMLDOMNodeList;
begin
  Assert(Assigned(Database) and Assigned(Doc));

  XmlTables := nil;
  LocalTables := TStringList.Create;
  try
    XmlTables := TStringList.Create;
    TableNodes := Doc.selectNodes('//table');
    Assert(Assigned(TableNodes));
    GetNamesOfNodes(TableNodes, XmlTables);
    Database.Handle.ListTableNames(LocalTables);

    RemoveOldTables(Database, LocalTables, XmlTables);
    Database.Handle.ListTableNames(LocalTables);
    AddNewTables(Database, LocalTables, XmlTables);
    Database.Handle.ListTableNames(LocalTables);
    Assert(LocalTables.Count = XmlTables.Count);
    for i := 0 to LocalTables.Count - 1 do begin
      TableNode := Doc.selectSingleNode(Format('//table[@name = "%s"]', [LocalTables[i]]));
      Assert(Assigned(TableNode));
      SynchronizeFieldsAndIndexes(Database, LocalTables[i], TableNode);
    end;
  finally
    FreeAndNil(XmlTables);
    FreeAndNil(LocalTables);
  end;
end;

{$BOOLEVAL OFF}
end.
