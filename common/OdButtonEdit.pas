// Code written long ago by Erik Berry
unit OdButtonEdit;

interface

uses Windows, Messages, Classes, Controls, StdCtrls, Buttons;

type
  TCustomButtonEdit = class(TCustomMemo)
  private
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
  protected
    FButton: TSpeedButton;
    function GetOnClick: TNotifyEvent;
    procedure SetOnClick(Value: TNotifyEvent);
    procedure SetButtonCaption(Value: string);
    function GetButtonCaption: string;
    procedure UpdateFormatRect;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure WMSetCursor(var Msg: TWMSetCursor); message WM_SETCURSOR;
    procedure CreateHandle; override;
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(AOwner: TComponent); override;
    property ButtonCaption: string read GetButtonCaption write SetButtonCaption;
    property OnButtonClick: TNotifyEvent read GetOnClick write SetOnClick;
  end;

  TButtonEdit = class(TCustomButtonEdit)
  published
    property Align;
    property Alignment;
    property Anchors;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property ImeMode;
    property ImeName;
    property MaxLength;
    property OEMConvert;
    property ParentBiDiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
    //
    property OnButtonClick;
    property ButtonCaption;
  end;

procedure Register;

implementation

uses
  Forms;

procedure Register;
begin
  RegisterComponentsProc('Samples', [TButtonEdit]);
end;

{TButtonEdit}

constructor TCustomButtonEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Height := 22;
  Width := 121;
  WordWrap := False;
  WantReturns := False;
  FButton := TSpeedButton.Create(Self);
  FButton.Parent := Self;
  FButton.Align := alRight;
  FButton.Caption := '...';
  DoubleBuffered := True;
end;

procedure TCustomButtonEdit.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style or ES_MULTILINE or WS_CLIPCHILDREN;
end;

procedure TCustomButtonEdit.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  FButton.Enabled := Enabled;
end;

procedure TCustomButtonEdit.CreateHandle;
begin
  inherited CreateHandle;
  UpdateFormatRect;
end;

function TCustomButtonEdit.GetOnClick: TNotifyEvent;
begin
  Result := FButton.OnClick;
end;

procedure TCustomButtonEdit.SetOnClick(Value: TNotifyEvent);
begin
  FButton.OnClick := Value;
end;

procedure TCustomButtonEdit.SetButtonCaption(Value: string);
begin
  FButton.Caption := Value;
end;

function TCustomButtonEdit.GetButtonCaption: string;
begin
  Result := FButton.Caption;
end;

procedure TCustomButtonEdit.UpdateFormatRect;
var
  Loc: TRect;
begin
  SendMessage(Handle, EM_GETRECT, 0, LongInt(@Loc));
  Loc.Bottom := ClientHeight + 1;  {+1 is workaround for windows paint bug}
  Loc.Right := ClientWidth - FButton.Width - 2;
  Loc.Top := 0;
  Loc.Left := 0;
  SendMessage(Handle, EM_SETRECTNP, 0, LongInt(@Loc));
  SendMessage(Handle, EM_GETRECT, 0, LongInt(@Loc));  {debug}
end;

procedure TCustomButtonEdit.WMSize(var Msg: TWMSize);
begin
  inherited;
  FButton.Width := FButton.Height;
  UpdateFormatRect;
end;

procedure TCustomButtonEdit.WMSetCursor(var Msg: TWMSetCursor);
var
  P: TPoint;
begin
  GetCursorPos(P);
  P := ScreenToClient(P);
  if (P.X >= ClientWidth - (FButton.Width)) then
    SetCursor(Screen.Cursors[crDefault])
  else
    inherited;
end;

end.
