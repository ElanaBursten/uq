unit OdCxUtils;

interface

uses SysUtils, Classes, Forms, Db, cxGridCustomTableView, cxGridDBTableView, cxGrid,
  cxCustomData, cxDropDownEdit, cxGridExportLink, cxGridDBBandedTableView,
  cxGridTableView, cxDateUtils, cxGridDBDataDefinitions, Controls, Math,
  cxGridCustomView, Windows, Graphics, ExtCtrls, CheckLst, cxDBTL, dxCore,
  cxCheckListBox;

{$IF CompilerVersion >= 16} // Delphi 8, 2005 or greater
  {$DEFINE ClassHelpers}
{$IFEND}

{$IFDEF ClassHelpers}
type
  // These helpers are shortcuts for single view grids
  TCxGridHelper = class helper for TcxGrid
  public
    function ColumnByName(const FieldName: string): TcxGridDBColumn;
    function FieldByName(const FieldName: string): TField;
    function ValueByName(const FieldName: string): Variant;
    function StringByName(const FieldName: string): string;
    function IntByName(const FieldName: string): Integer;
    function DropdownItems(const FieldName: string): TStrings;
    function Count: Integer;
    procedure Print;
    function FocusFirstRow(FocusGrid: Boolean = True): Boolean;
    procedure SortByFields(SortColumns: array of string; SortDirection: array of TcxDataSortOrder);
    function TableView: TcxGridDBTableView;
    procedure CopyToTSV;
  end;

  TCxDBTreeListHelper = class helper for TcxDBTreeList
  public
    procedure BestFitVisibleColumns;
  end;

  TCxCheckListBoxHelper = class helper for TcxCheckListBox
  public
    function CheckedItemsString(const IntObjects: Boolean=False; const Delim: string=','): string;
    procedure AddCheckedItemsToList(AddToStrings: TStrings; const IntObjects: Boolean=False);
  end;

{$ENDIF ClassHelpers}

type
  TDefaultLayoutAction = (colStore, colRestore);

function CxComboBoxItems(const Column: TcxCustomGridTableItem): TStrings; overload;
function CxComboBoxItems(const ComboBox: TcxComboBox): TStrings; overload;
procedure SetCxComboBoxDropDownRows(Column: TcxCustomGridTableItem); overload;
procedure SetCxComboBoxDropDownRows(ComboBox: TcxComboBox); overload;
function FindGridColumn(Grid: TcxGrid; Row, Column: Integer): TcxGridDBColumn;
function FocusFirstGridRow(Grid: TcxGrid; FocusGrid: Boolean = True): Boolean;
procedure SortGridByFieldNames(Grid: TcxGrid; SortColumns: array of string; SortDirection: array of TcxDataSortOrder);
procedure SortGridByFirstVisibleColumn(Grid: TcxGrid; Ascending: Boolean = True);
procedure PrintCxGrid(Grid: TcxGrid);
procedure VerifyMinimumSQLServerDate(DateField: TField);
procedure SetGridPopupColumnEnabled(Column: TcxGridColumn; Enabled: Boolean);
function GetCxComboObjectInteger(Combo: TcxComboBox; Required: Boolean=True): Integer;
function SelectCxComboBoxItemFromObjects(ComboBox: TcxComboBox; ID: Integer): Boolean;
procedure SetGridComboItems(Column: TcxGridDBColumn; Items: TStrings);
function GetGridComboItems(Column: TcxGridDBColumn): TStrings;
procedure SetGridViewAutoWidth(GridView: TcxGridTableView; const Enabled: Boolean);
function HaveFocusedGridViewRow(GridView: TcxGridDBTableView): Boolean;
procedure ForceControlsGridsToResize(WinControl: TWinControl);
procedure FullExpandGrid(Grid: TcxGrid);
procedure SetGridHeight(const AGrid: TcxGrid; const ARowsNumber: Integer; const ExpandView: Boolean);
function GetViewHeight(AView: TcxGridTableView; ARowsNumber: Integer): Integer;
procedure SizeCheckListPopupToItems(Column: TcxPopupEdit; CheckList: TCheckListBox);
procedure DefaultLayoutStorage(defAction: TDefaultLayoutAction; AForm: TForm;
   AGridView: TcxGridDBTableView; LayoutFile: String);

implementation

uses OdMiscUtils, OdExceptions, OdVclUtils, Clipbrd;

procedure DefaultLayoutStorage(defAction: TDefaultLayoutAction; AForm: TForm;
   AGridView: TcxGridDBTableView; LayoutFile: String);
var
  AName: String;
begin
  AName := AForm.Name;
  AForm.Name :=  AName + 'DefaultLayout';
  if DefAction =  colRestore then
   AGridView.RestoreFromIniFile(LayoutFile)
  else if DefAction = colStore then
    AGridView.StoretoIniFile(LayoutFile, False);
  AForm.Name := AName;
end;

function CxComboBoxItems(const Column: TcxCustomGridTableItem): TStrings;
begin
  Assert(Assigned(Column), 'Column is not assigned');
  Assert(Column.PropertiesClass = TcxComboBoxProperties, 'Column is not a ComboBox edit');
  Result := TcxComboBoxProperties(Column.Properties).Items;
end;

function CxComboBoxItems(const ComboBox: TcxComboBox): TStrings;
begin
  Assert(Assigned(ComboBox), 'ComboBox is not assigned');
  Result := ComboBox.Properties.Items;
end;

procedure SetCxComboBoxDropDownRows(Column: TcxCustomGridTableItem);
var
  Properties: TcxComboBoxProperties;
begin
  Assert(Assigned(Column), 'Column is not assigned');
  Assert(Column.PropertiesClass = TcxComboBoxProperties, 'Column is not a ComboBox edit');
  Properties := TcxComboBoxProperties(Column.Properties);
  Properties.DropDownRows := Properties.Items.Count;
end;

procedure SetCxComboBoxDropDownRows(ComboBox: TcxComboBox);
begin
  Assert(Assigned(ComboBox), 'ComboBox is not assigned');
  ComboBox.Properties.DropDownRows := ComboBox.Properties.Items.Count;
end;

function FindGridColumn(Grid: TcxGrid; Row, Column: Integer): TcxGridDBColumn;
var
  i: Integer;
  View: TcxCustomGridTableView;
begin
  Assert((Row >= 0) and (Column >=0));
  Assert(Assigned(Grid));
  Assert(Assigned(Grid.ActiveView));
  Assert(Grid.ActiveView is TcxCustomGridTableView);
  if Row > 0 then
    Assert(Grid.ActiveView is TcxGridDBBandedTableView);

  View := Grid.ActiveView as TcxCustomGridTableView;
  Result := nil;

  for i := 0 to View.ItemCount - 1 do begin
    if (View.Items[i].Index = Column) and ((Row = 0) or ((Row > 0) and
      (TcxGridDBBandedColumn(View.Items[i]).Position.RowIndex = Row))) then begin
      Result := View.Items[i] as TcxGridDBColumn;
      Break;
    end;
  end;
end;

function FocusFirstGridRow(Grid: TcxGrid; FocusGrid: Boolean): Boolean;
begin
  Assert(Assigned(Grid));
  Result := False;
  if Assigned(Grid.ActiveView) then begin
    if Assigned(Grid.ActiveView.DataController) then begin
      Grid.ActiveView.DataController.GotoFirst;
      Grid.ActiveView.DataController.SetFocus;
    end;
    if FocusGrid and Grid.CanFocus then
      Grid.SetFocus;
    Result := True;
  end;
end;

procedure SortGridByFieldNames(Grid: TcxGrid; SortColumns: array of string; SortDirection: array of TcxDataSortOrder);
var
  i: Integer;
  Column: TcxCustomGridTableItem;
  View: TcxCustomGridTableView;
begin
  Assert(Assigned(Grid));
  Assert(Assigned(Grid.ActiveView));
  Assert(Grid.ActiveView is TcxCustomGridTableView);
  Assert(Length(SortColumns) = Length(SortDirection));

  View := Grid.ActiveView as TcxCustomGridTableView;
  View.BeginUpdate;
  try
    View.DataController.ClearSorting(True);
    for i := Low(SortColumns) to High(SortColumns) do begin
      Column := nil;
      if View is TcxGridDBTableView then
        Column := TcxGridDBTableView(View).GetColumnByFieldName(SortColumns[i])
      else if View is TcxGridDBBandedTableView then
        Column := TcxGridDBBandedTableView(View).GetColumnByFieldName(SortColumns[i]);
      if Assigned(Column) then
        Column.SortOrder := SortDirection[i];
    end;
  finally
    View.EndUpdate;
  end;
end;

procedure SortGridByFirstVisibleColumn(Grid: TcxGrid; Ascending: Boolean);
var
  i: Integer;
  SeenVisible: Boolean;
  Col: TcxCustomGridTableItem;
  View: TcxCustomGridTableView;
begin
  Assert(Assigned(Grid));
  Assert(Assigned(Grid.ActiveView));
  Assert(Grid.ActiveView is TcxCustomGridTableView);

  View := Grid.ActiveView as TcxCustomGridTableView;
  SeenVisible := False;
  View.BeginUpdate;
  try
    View.DataController.ClearSorting(True);
    for i := 0 to View.ItemCount - 1 do begin
      Col := View.Items[i];
      if not SeenVisible and Col.Visible then begin
        SeenVisible := True;
        if Ascending then
          Col.SortOrder := soAscending
        else
          Col.SortOrder := soDescending;
      end;
    end;
  finally
    View.EndUpdate;
  end;
end;

procedure PrintCxGrid(Grid: TcxGrid);
const
  HTMLExtension = 'html';
var
  FileName: string;
begin
  Assert(Assigned(Grid));
  FileName := GetWindowsTempPath + 'GridPrint';
  ExportGridToHTML(FileName, Grid, False, True, HTMLExtension);
  OdShellExecute(FileName + '.' + HTMLExtension);
end;

procedure VerifyMinimumSQLServerDate(DateField: TField);
var
  MinDate: TDateTime;
begin
  Assert(Assigned(DateField), 'DateField must be assigned first.');
  if (DateField.IsNull) or (DateField.AsDateTime = NullDate) then
    Exit;

  MinDate := EncodeDate(1950, 1, 1);
  if (DateField.AsDateTime < MinDate) then
    raise EOdDataEntryError.CreateFmt('Date field %s cannot occur before %s.',
      [DateField.DisplayName, DateToStr(MinDate)]);
end;

procedure SetGridPopupColumnEnabled(Column: TcxGridColumn; Enabled: Boolean);
begin
  Assert(Assigned(Column));
  if Enabled then begin
    Column.Options.Editing := True;
    Column.Options.ShowEditButtons := isebDefault;
  end else begin
    Column.Options.Editing := False;
    Column.Options.ShowEditButtons := isebNever;
  end;
end;

function GetCxComboObjectInteger(Combo: TcxComboBox; Required: Boolean): Integer;
begin
  Assert(Assigned(Combo));
  if (Combo.ItemIndex < 0) then begin
    if Required then
      raise Exception.Create('No selected combobox item for ' + Combo.Name)
    else
      Result := -1;
  end
  else
    Result := Integer(CxComboBoxItems(Combo).Objects[Combo.ItemIndex]);
end;

function SelectCxComboBoxItemFromObjects(ComboBox: TcxComboBox; ID: Integer): Boolean;
var
  i: Integer;
begin
  for i := 0 to ComboBox.Properties.Items.Count - 1 do
    if Integer(CxComboBoxItems(ComboBox).Objects[i]) = ID then begin
      ComboBox.ItemIndex := i;
      Result := True;
      Exit;
    end;

  ComboBox.ItemIndex := -1;
  Result := False;
end;

procedure SetGridComboItems(Column: TcxGridDBColumn; Items: TStrings);
begin
  Assert(Assigned(Column));
  Assert(Assigned(Items));
  (Column.Properties as TcxComboBoxProperties).Items.Assign(Items);
end;

function GetGridComboItems(Column: TcxGridDBColumn): TStrings;
begin
  Assert(Assigned(Column));
  Result := (Column.Properties as TcxComboBoxProperties).Items;
end;

procedure SetGridViewAutoWidth(GridView: TcxGridTableView; const Enabled: Boolean);
begin
  Assert(Assigned(GridView));
  GridView.OptionsView.ColumnAutoWidth := Enabled;
end;

function HaveFocusedGridViewRow(GridView: TcxGridDBTableView): Boolean;
begin
  Assert(Assigned(GridView));
  Result := Assigned(GridView.Controller.FocusedRow) and
    (not GridView.Controller.IsSpecialRowFocused) and
    (GridView.Controller.FocusedRow.IsData);
end;

procedure ForceControlsGridsToResize(WinControl: TWinControl);
{TODO: This method should be removed when upgrading to Delphi 2009 and higher.
 Force grids contained by this control to resize properly.  An error occurs
 within the Windows kernel when Delphi does not align nested controls; more
 specific info at CodeCentral: http://cc.embarcadero.com/Item/25646}
var
  i : Integer;
  IsContainer: Boolean;
begin
  Assert(Assigned(WinControl));
  for i := 0 to WinControl.ControlCount - 1 do begin
    if (WinControl.Controls[i] is TcxGrid) then
      TcxGrid(WinControl.Controls[i]).SizeChanged
    else begin
      IsContainer := (csAcceptsControls in WinControl.Controls[i].ControlStyle);
      if (IsContainer and (WinControl.Controls[i] is TWinControl)) then
        ForceControlsGridsToResize(TWinControl(WinControl.Controls[i]));
    end;
  end;
end;

procedure FullExpandGrid(Grid: TcxGrid);
begin
  Assert(Assigned(Grid));
  Grid.ActiveView.DataController.Groups.FullExpand;
end;

procedure SetGridHeight(const AGrid: TcxGrid; const ARowsNumber: Integer; const ExpandView: Boolean);
var
  AHeight: Integer;
begin
  if (AGrid = nil) or (AGrid.FocusedView = nil) then
    Exit;
  with AGrid.FocusedView as TcxGridTableView do
  begin
    if ExpandView then
      ViewData.Expand(True);
    AHeight := GetViewHeight(TcxGridTableView(AGrid.FocusedView), ARowsNumber);
    AGrid.Height := AHeight;
  end;
end;

function GetViewHeight(AView: TcxGridTableView; ARowsNumber: Integer): Integer;
var
  I: Integer;
  GroupRowCount: Integer;
begin
  Result := 0;
  if AView = nil then
    Exit;

  with AView do
  begin
    with ViewInfo do begin
      if ViewData.RowCount> 0 then
      for I := 0 to Min(ARowsNumber, ViewData.RowCount)-1 do
      begin
        Inc(Result, RecordsViewInfo.Items[I].DataHeight);
        Inc(Result, GridLineWidth);
      end;
      if OptionsView.GroupByBox then
        Inc(Result, GroupByBoxViewInfo.Height);
      Inc(Result, HeaderViewInfo.Bounds.Bottom - HeaderViewInfo.Bounds.Top);
      if ((AView is TcxGridDBTableView) or (AView is TcxGridDBBandedTableView)) then begin
        GroupRowCount := DataController.Groups.ChildCount[-1];
        Inc(Result, GroupRowCount * RecordsViewInfo.DataRowHeight);
      end;
      Inc(Result, GetSystemMetrics(SM_CYHSCROLL));
    end;
  end;
end;

// Adjusts width of a panel associated with a PopupEdit TcxGridColumn;
// Assumes the panel only contains a client-aligned TCheckListBox.
procedure SizeCheckListPopupToItems(Column: TcxPopupEdit; CheckList: TCheckListBox);
var
  i: Integer;
  MaxWidth: Integer;
  CBWidth, SBWidth: Integer;
  Bitmap: Graphics.TBitmap;
begin
  Assert(Assigned(Column), 'Column is not assigned');
  Assert((Assigned(CheckList.Parent) and (CheckList.Parent is TPanel)),
    'CheckList must have a TPanel parent control');
  MaxWidth := Column.Width;
  CBWidth := GetSystemMetrics(SM_CXMENUCHECK); // checkbox width
  SBWidth := GetSystemMetrics(SM_CXVSCROLL);   // scrollbar width

  Bitmap := Graphics.TBitmap.Create;
  try
    Bitmap.Canvas.Font := CheckList.Font;
    for i := 0 to CheckList.Items.Count - 1 do
      MaxWidth := Max(Bitmap.Canvas.TextWidth(CheckList.Items[i]) + CBWidth + 12,
        MaxWidth);

    if (GetWindowLong(CheckList.Handle, GWL_STYLE) and WS_VSCROLL) <> 0 then // vertical scrollbar is visible
      Inc(MaxWidth, SBWidth);
  finally;
    FreeAndNil(Bitmap);
  end;

  CheckList.Parent.Width := MaxWidth + (TPanel(CheckList.Parent).BorderWidth * 2);
  Column.Properties.PopupMinWidth := CheckList.Parent.Width;
  Column.Properties.PopupWidth := CheckList.Parent.Width;
end;


{$IFDEF ClassHelpers}

{ TCxGridHelper }

function TCxGridHelper.FocusFirstRow(FocusGrid: Boolean): Boolean;
var
  DBTableView: TcxGridDBTableView;
begin
  Result := False;
  if ViewCount > 0 then begin
    if Views[0] is TcxGridDBTableView then begin
      DBTableView := (Views[0] as TcxGridDBTableView);
      if Assigned(DBTableView.Controller) then
        Result := DBTableView.Controller.GoToFirst;
    end;
    if FocusGrid then
      Result := Result and TryFocusControl(Self);
  end;
end;

function TCxGridHelper.ColumnByName(const FieldName: string): TcxGridDBColumn;
begin
  Result := TableView.GetColumnByFieldName(FieldName);
end;

function TCxGridHelper.Count: Integer;
var
  DBTableView: TcxGridDBTableView;
begin
  Assert(ViewCount > 0);
  Assert(Views[0] is TcxGridDBTableView);
  DBTableView := (Views[0] as TcxGridDBTableView);
  Result := DBTableView.DataController.RecordCount;
end;

function TCxGridHelper.DropdownItems(const FieldName: string): TStrings;
var
  DBTableView: TcxGridDBTableView;
begin
  Assert(NotEmpty(FieldName));
  Assert(ViewCount > 0);
  Assert(Views[0] is TcxGridDBTableView);
  DBTableView := (Views[0] as TcxGridDBTableView);
  Result := (DBTableView.GetColumnByFieldName(FieldName).Properties as TcxComboBoxProperties).Items;
end;

procedure TCxGridHelper.CopyToTSV;
begin
  Self.TableView.CopyToClipboard(True);
end;

function TCxGridHelper.FieldByName(const FieldName: string): TField;
var
  DBTableView: TcxGridDBTableView;
  DBDataController: TcxGridDBDataController;
begin
  Assert(NotEmpty(FieldName));
  Assert(ViewCount > 0);
  Assert(Views[0] is TcxGridDBTableView);
  DBTableView := (Views[0] as TcxGridDBTableView);
  Assert(Assigned(Views[0].DataController));
  Assert(DBTableView.DataController is TcxGridDBDataController);
  DBDataController := (DBTableView.DataController as TcxGridDBDataController);
  Assert(Assigned(DBDataController.DataSource));
  Assert(Assigned(DBDataController.DataSource.DataSet));
  Result := DBDataController.DataSource.DataSet.FieldByName(FieldName);
end;

function TCxGridHelper.IntByName(const FieldName: string): Integer;
var
  Field: TField;
begin
  Field := FieldByName(FieldName);
  Result := Field.AsInteger;
end;

procedure TCxGridHelper.Print;
var
  FileName: string;
begin
  FileName := GetWindowsTempPath + 'CXGridPrint.html';
  cxGridExportLink.ExportGridToHTML(ChangeFileExt(FileName, ''), Self);
  OdShellExecute(FileName);
end;

procedure TCxGridHelper.SortByFields(SortColumns: array of string;
  SortDirection: array of TcxDataSortOrder);
var
  i: Integer;
  Column: TcxGridDBColumn;
begin
  Assert(Length(SortColumns) = Length(SortDirection));
  TableView.DataController.BeginFullUpdate;
  try
    TableView.DataController.ClearSorting(True);
    for i := Low(SortColumns) to High(SortColumns) do begin
      Column := ColumnByName(SortColumns[i]);
      Column.SortOrder := SortDirection[i];
    end;
  finally
    TableView.DataController.EndFullUpdate;
  end;
end;

function TCxGridHelper.StringByName(const FieldName: string): string;
var
  Field: TField;
begin
  Field := FieldByName(FieldName);
  Result := Field.AsString;
end;

function TCxGridHelper.TableView: TcxGridDBTableView;
begin
  Assert(ViewCount > 0);
  Assert(Views[0] is TcxGridDBTableView);
  Result := (Views[0] as TcxGridDBTableView);
end;

function TCxGridHelper.ValueByName(const FieldName: string): Variant;
var
  Field: TField;
begin
  Field := FieldByName(FieldName);
  Result := Field.Value;
end;

{ TCxDBTreeListHelper }

procedure TCxDBTreeListHelper.BestFitVisibleColumns;
var
  I: Integer;
begin
  for I := 0 to VisibleColumnCount - 1 do
    VisibleColumns[I].ApplyBestFit;
end;

{ TCxCheckListBoxHelper}
function TCxCheckListBoxHelper.CheckedItemsString(const IntObjects: Boolean; const Delim: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do begin
    if Items[i].Checked then begin
      if Result = '' then begin
        if IntObjects then
          Result := IntToStr(Integer(Items.Objects[i]))
        else
          Result := Items[i].Text
      end
      else begin
        if IntObjects then
          Result := Result + Delim + IntToStr(Integer(Items.Objects[i]))
        else
          Result := Result + Delim + Items[i].Text;
      end;
    end;
  end;
end;

procedure TCxCheckListBoxHelper.AddCheckedItemsToList(AddToStrings: TStrings; const IntObjects: Boolean=False);
//Does not clear the list, but adds items to existing list passed in.
var
  i: Integer;
begin
  Assert(Assigned(AddToStrings));

  for i := 0 to Count - 1 do begin
    if Items[i].Checked then
      if IntObjects then
        AddToStrings.Add(IntToStr(Integer(Items.Objects[i])))
      else
        AddToStrings.Add(Items[i].Text)
  end;
end;

{$ENDIF ClassHelpers}

end.
