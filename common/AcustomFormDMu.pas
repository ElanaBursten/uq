unit AcustomFormDMu;

interface

uses
  SysUtils, Classes, DB, odContainer, ACustomFormConst, dbisamtb,
  Dmu, Dialogs, LocalEmployeeDMu;   {Comment out if running in isolated mode and set to the Database in this unit}

type
  TDBOpenForm = record
    FormID: integer;
    Desc: string;

  end;

  {Within this Dmu, the fOpenGroup is to track the last group opened.}
  TDBOpenGroup = record
    Loaded: boolean;                {Whether a group has been loaded - Query open}
    FormID: integer;                {Form ID}
    FieldGroup: integer;            {The field_group}
    NumberofItems: integer;         {How many items are in this group}
    ListOfFieldTypes: TStringlist;  {FF, DATETIME, ETC}
  end;

  {Unlike The status questions, each frame is a group of questions (defined from
   the custom_form_field}
  TGroupingType = (gtSingle, gtCheckboxGroup, gtFFGroup, gtMixedGroup, gtNone);

  TAcustomFormDM = class(TDataModule)
    GetCustomFormGroup: TDBISAMQuery;
    GetGroupCount: TDBISAMQuery;
    GetAnswerRecord: TDBISAMQuery;
    CustomFormAnswer: TDBISAMTable;
    GetSelectionList: TDBISAMQuery;
    GetForm: TDBISAMQuery;
    Database2: TDBISAMDatabase;
    GetFormAnswers: TDBISAMQuery;
    UpdateExistingAnswer: TDBISAMQuery;
    GetFormByFormID: TDBISAMQuery;
    GetAnswerRecordsOtherEmployees: TDBISAMQuery;
  private

    fDBOpenGroup: TDBOpenGroup;
    procedure ClearGroup;
    function GroupLoaded(FormID, Group: Integer): Boolean;
  public
    procedure ClearFormData(var pFormData: TFormInfo);
    function GetFormIDByRef(pRef_id: integer; pModifier: string = ''): TFormInfo;
    function GetFormbyID(pFormID: integer): TFormInfo;
    function GetNumberofGroups(pFormID: integer): integer; {how many groups for this form}
    procedure GetGroups(pFormID: integer; var pGroupList: TIntegerList);   {returns all the group numbers}
    property CurDBOpenGroup: TDBOpenGroup read fDBOpenGroup write fDBOpenGroup;

    function OpenGroup(FormID: Integer; Group: Integer): integer;
    function GetGroupType(FormID: Integer; Group: Integer): TGroupingType;
    function SaveFieldAnswertoDB(pFormFieldInfo: TFieldInfo): Boolean;
    function HasExistingAnswersDB(pFormID: integer; pEmpID: integer; pForeignID: integer): boolean;
    function GetAnswerDB(var pFormFieldInfo: TFieldInfo): string; {returns answer}
    function HasAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;    {for updating an answer}
    function HasAnswersforOtherEmps(pFormID: integer; pForeignID: integer; pActiveEmpID: integer): boolean;
    function UpdateAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;
    function InsertAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;

    procedure SaveCFNote(pFormFieldInfo: TFieldInfo);
  end;

var
  AcustomFormDM: TAcustomFormDM;

implementation


{$R *.dfm}


{ TAcustomFormDMu }

procedure TAcustomFormDM.ClearFormData(var pFormData: TFormInfo);
begin
    pFormData.FormID        := 0;
    pFormData.Desc          := '';
    pFormData.Title         := '';
    pFormData.FormType     := '';
    pFormData.ForeignType   := 0;
    pFormData.LinkRefId     := 0;
    pFormData.LinkModifier  := '';
    pFormData.ViewStartDate := 0.0;
    pFormData.ViewEndDate   := 0.0;
    pFormData.Modified_Date := 0.0;
end;

procedure TAcustomFormDM.ClearGroup;
begin
  fDBOpenGroup.Loaded := False;
  fDBOpenGroup.FormID := 0;
  fDBOpenGroup.FieldGroup := 0;
  fDBOpenGroup.NumberofItems := 0;
  if assigned(fDBOpenGroup.ListOfFieldTypes) then
    fDBOpenGroup.ListOfFieldTypes.Clear;
end;




function TAcustomFormDM.GetGroupType(FormID, Group: Integer): TGroupingType;
{Look at the GROUP type}
var
  i, ListCount: integer;
  CurTypeStr, LastTypeStr: string;
begin
  LastTypeStr := '';
  if GroupLoaded(FormID, Group) then begin
    ListCount := fDBOpenGroup.ListOfFieldTypes.Count;
    if (ListCount = 0) then
      Result := gtNone
    else if (ListCount = 1) then Result := gtSingle

    else begin
      for I := 0 to ListCount - 1 do begin
        CurTypeStr := fDBOpenGroup.ListOfFieldTypes[i];
        if (LastTypeStr <> '') and (LastTypeStr <> CurTypeStr) then begin
          Result := gtMixedGroup;
          Exit;
        end;
        LastTypeStr := CurTypeStr;
      end;
      if (CurTypeStr = 'FF') then Result := gtFFGroup
      else if (CurTypeStr = 'CHKBOX') then Result := gtCheckboxGroup
      else Result := gtSingle;
           
    end;

  end;
end;

function TAcustomFormDM.GetAnswerDB(var pFormFieldInfo: TFieldInfo): string;
begin
  GetAnswerRecord.ParamByName('form_field_id').AsInteger := pFormFieldInfo.FieldID;
  GetAnswerRecord.ParamByName('answered_by').AsInteger := pFormFieldInfo.AnsweredBy;
  GetAnswerRecord.ParamByName('foreign_id').AsInteger := pFormFieldInfo.AnswerForeignId;
  GetAnswerRecord.Open;

  if GetAnswerRecord.RecordCount > 0 then begin
    Result := GetAnswerRecord.FieldByName('answer').AsString;
    pFormFieldInfo.Answer := Result;
    pFormFieldInfo.ExistingAnswerDate := GetAnswerRecord.FieldByName('answer_date').AsDateTime;
    pFormFieldInfo.ExistingAnswerID := GetAnswerRecord.FieldByName('answer_id').AsInteger;
  end
  else
    Result := '';

  GetAnswerRecord.Close;
end;

function TAcustomFormDM.GetFormbyID(pFormID: integer): TFormInfo;
begin
  ClearFormData(Result);
  try
    GetFormByFormID.ParamByName('form_id').AsInteger := pFormID;
    GetFormByFormID.Open;

    if GetFormByFormID.RecordCount > 0 then begin
      {Set the form data returned}
      GetFormByFormID.First;
      Result.FormID        := GetFormByFormID.FieldByName('form_id').AsInteger;
      Result.Desc          := GetFormByFormID.FieldByName('form_description').AsString;
      Result.Title         := GetFormByFormID.FieldByName('form_title').AsString;
      Result.FormType     := GetFormByFormID.FieldByName('form_type').AsString;
      Result.ForeignType   := GetFormByFormID.FieldByName('foreign_type').AsInteger;
      Result.LinkRefId     := GetFormByFormID.FieldByName('link_ref_id').AsInteger;
      Result.LinkModifier  := trim(GetFormByFormID.FieldByName('link_modifier').AsString);
      Result.ViewStartDate := GetFormByFormID.FieldByName('view_start_date').AsDateTime;
      Result.ViewEndDate   := GetFormByFormID.FieldByName('view_end_date').AsDateTime;
      Result.Modified_Date := GetFormByFormID.FieldByName('modified_date').AsDateTime;
 //     Result.RefDesc       := GetFormByFormID.FieldByName('linkdesc').AsString;

    end;

  finally
    GetFormByFormID.Close;
  end;

end;

function TAcustomFormDM.GetFormIDByRef(pRef_id: integer;    //QM-593 EB ATT Damage
  pModifier: string): TFormInfo;

var
  RecModifier: string;
  AllFlag: boolean;
  Found: boolean;
begin
  ClearFormData(Result);
  if (pModifier = '*') or (pModifier = '') then
    AllFlag := True
  else
    AllFlag := False;

  GetForm.ParamByName('ref_id').AsInteger := pRef_id;
  GetForm.Open;
  GetForm.First;
  Found := False;

  try
  if GetForm.RecordCount = 0 then
    exit;

  while not GetForm.EOF do begin
    RecModifier := trim(GetForm.FieldByName('link_modifier').AsString);
    if ((RecModifier = '*') or (RecModifier = '')) and AllFlag then
      Found := True
    else if (Uppercase(RecModifier) = UpperCase(pModifier)) then
      Found := True;

    if Found then begin
     {Set the form data returned}
      Result.FormID        := GetForm.FieldByName('form_id').AsInteger;
      Result.Desc          := GetForm.FieldByName('form_description').AsString;
      Result.Title         := GetForm.FieldByName('form_title').AsString;
      Result.FormType     := GetForm.FieldByName('form_type').AsString;
      Result.ForeignType   := GetForm.FieldByName('foreign_type').AsInteger;
      Result.LinkRefId     := GetForm.FieldByName('link_ref_id').AsInteger;
      Result.LinkModifier  := trim(GetForm.FieldByName('link_modifier').AsString);
      Result.ViewStartDate := GetForm.FieldByName('view_start_date').AsDateTime;
      Result.ViewEndDate   := GetForm.FieldByName('view_end_date').AsDateTime;
      Result.Modified_Date := GetForm.FieldByName('modified_date').AsDateTime;
 //     Result.RefDesc       := GetFormByFormID.FieldByName('linkdesc').AsString;
 //     Break;
    end;
    GetForm.Next;
  end;
  if (Result.ForeignType > 1000) or (Result.ForeignType < 0) then
     Result.ForeignType := 0;
  finally
    GetForm.Close;
  end;

end;

procedure TAcustomFormDM.GetGroups(pFormID: integer; var pGroupList: TIntegerList);
begin
  pGroupList.Clear;
  if GetGroupCount.Active then GetGroupCount.Close;
  GetGroupCount.ParamByName('form_id').AsInteger := pFormID;
  GetGroupCount.Open;
  GetGroupCount.First;
  while not GetGroupCount.EOF do begin
    PGroupList.Add(GetGroupCount.FieldByName('field_group').AsInteger);
    GetGroupCount.Next;
  end;
 // Result := GetGroupCount.RecordCount; //GetGroupCount.FieldByName('GroupCount').AsInteger;
  GetGroupCount.Close;
end;


function TAcustomFormDM.GetNumberofGroups(pFormID: integer): integer;
begin
  if GetGroupCount.Active then GetGroupCount.Close;
  GetGroupCount.ParamByName('form_id').AsInteger := pFormID;
  GetGroupCount.Open;
  Result := GetGroupCount.RecordCount; //GetGroupCount.FieldByName('GroupCount').AsInteger;
  GetGroupCount.Close;
end;



function TAcustomFormDM.GroupLoaded(FormID, Group: Integer): Boolean;
begin
  if fDBOpenGroup.Loaded and (fDBOpenGroup.FormID = FormID)
  and (fDBOpenGroup.FieldGroup = Group) then
    Result := True
  else
    Result := False;
end;

function TAcustomFormDM.HasAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;
begin

{Going to set this not used right now....returnng False}
  GetAnswerRecord.ParamByName('form_field_id').AsInteger := pFormFieldInfo.FieldID;
  GetAnswerRecord.ParamByName('answered_by').AsInteger := pFormFieldInfo.AnsweredBy;
  GetAnswerRecord.ParamByName('foreign_id').AsInteger := pFormFieldInfo.AnswerForeignId;
  GetAnswerRecord.Open;

  if GetAnswerRecord.RecordCount > 0 then
    Result := True
  else
    Result := False;

  GetAnswerRecord.Close;
end;

function TAcustomFormDM.HasAnswersforOtherEmps(pFormID: integer;
  pForeignID: integer; pActiveEmpID: integer): boolean;
begin
  Result := False;   //We are not going to run this for the first go-round
 // try
//    With GetAnswerRecordsOtherEmployees do begin
//      if Active then Close;
//
//      ParamByName('form_id').AsInteger := pFormID;
//      ParamByName('foreign_id').AsInteger := pForeignID;
//      Open;
//      First;
//      while not EOF do begin
//        if (FieldByName('answered_by').asInteger <> pActiveEmpID) then begin
//          Result := True;
//          Break;
//        end;
//        Next;
//      end;
//    end;
//  finally
//  ///////
//  end;
end;

function TAcustomFormDM.HasExistingAnswersDB(pFormID, pEmpID: integer; pForeignID: integer): boolean;
begin
  Result := False;
  try
    if GetFormAnswers.Active then
      GetFormAnswers.Close;

    GetFormAnswers.ParamByName('form_id').AsInteger := pFormID;
    GetFormAnswers.ParamByName('answered_by').asInteger := pEmpID;
    GetFormAnswers.ParamByName('foreign_id').asInteger := pForeignID;
    GetFormAnswers.Open;
    if GetFormAnswers.RecordCount > 0 then begin
      Result := True;
    end;
  finally
    //Going to leave the table open right now.
  end;
  
end;

function TAcustomFormDM.InsertAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;
begin
  If not CustomFormAnswer.Active then
    CustomFormAnswer.Open;
  CustomFormAnswer.Insert;
  CustomFormAnswer.FieldByName('answer_id').AsInteger :=  -Random(1000000) - 10000;    {Replace this}
  CustomFormAnswer.FieldByName('form_field_id').AsInteger := pFormFieldInfo.FieldID;
  CustomFormAnswer.FieldByName('answer').AsString := pFormFieldInfo.Answer;
  CustomFormAnswer.FieldByName('answered_by').AsInteger := pFormFieldInfo.AnsweredBy;
  CustomFormAnswer.FieldByName('answer_date').AsDateTime := NOW;
  CustomFormAnswer.FieldByName('foreign_id').AsInteger := pFormFieldInfo.AnswerForeignId;
  CustomFormAnswer.FieldByName('DeltaStatus').AsString := 'I';
  CustomFormAnswer.Post;
  CustomFormAnswer.Close;
end;

function TAcustomFormDM.OpenGroup(FormID, Group: integer): integer;
var
  NewFormField, OldFormField: string;
begin
  NewFormField := '';
  OldFormField := '';
  if GetCustomFormGroup.Active then
    GetCustomFormGroup.Close;
  try
    GetCustomFormGroup.ParamByName('pform_id').AsInteger := FormID;
    GetCustomFormGroup.ParamByName('pfield_group').AsInteger := Group;
    GetCustomFormGroup.Open;
    GetCustomFormGroup.First;

    {Change our current Group that we are working on}
    if GetCustomFormGroup.RecordCount > 0 then begin
      fDBOpenGroup.Loaded := True;
      fDBOpenGroup.FormID := FormID;
      fDBOpenGroup.FieldGroup := Group;
      fDBOpenGroup.NumberofItems := GetCustomFormGroup.RecordCount;
      if not assigned(fDBOpenGroup.ListOfFieldTypes) then
        fDBOpenGroup.ListOfFieldTypes := TStringList.Create;
    end;

  except
   ClearGroup;
  end;

  Result := fDBOpenGroup.NumberofItems;
end;

procedure TAcustomFormDM.SaveCFNote(pFormFieldInfo: TFieldInfo);
const
  NOTEMAX = 8000;
  EMPLEN =  45;
var
  lNoteQuestion, lNoteAnswer: string;
  LCompleteNote: string;
  lEmpName: string;
  lFT: integer;
  lSubType: integer;
  lTargetLen: integer;
begin
  lCompleteNote := '';

  {Note limit is 8000 char, so make sure we can keep it under that}
  lFT := pFormFieldInfo.AnswerForeignType;
  lSubType := (lFT * 100); {public}
//  lDesc := pFormFieldInfo.Question;
  lEmpName := EmployeeDM.GetEmployeeShortName(pFormFieldInfo.AnsweredBy);
  lEmpName := lEmpName + '(' + IntToStr(pFormFieldInfo.AnsweredBy) + ')';
  lTargetLen := ROUND((NOTEMAX - 45) * 0.5);

  lNoteQuestion := 'Q/A - Q: ' + pFormFieldInfo.Question;
  if Length(lNoteQuestion) > lTargetLen then  {split the remaining space between Q and A}
    lNoteQuestion:= Copy(lNoteQuestion, 1, lTargetLen);
  lNoteAnswer := '  A: ' + pFormFieldInfo.Answer;
   if Length(lNoteAnswer) > lTargetLen then  {split the remaining space between Q and A}
    lNoteAnswer:= Copy(lNoteAnswer, 1, lTargetLen);

  lCompleteNote := lNoteQuestion + lNoteAnswer + ' [' + lEmpName + ']';
  DM.SaveNote(lCompleteNote, 'CF', lSubType, pFormFieldInfo.AnswerForeignType, pFormFieldInfo.AnswerForeignID, True);
end;



function TAcustomFormDM.SaveFieldAnswertoDB(
  pFormFieldInfo: TFieldInfo): Boolean;
begin
  try
    if HasAnswerRecord(pFormFieldInfo) then begin
      If pFormFieldInfo.AnswerHasChanged then begin
//        ShowMessage('ID:' + IntToStr(pFormFieldInfo.ExistingAnswerID)  + ' ' + IntToStr(pFormFieldInfo.FieldID) + ' ' + pFormFieldInfo.Answer);
        UpdateAnswerRecord(pFormFieldInfo);
      end
    end
    else
      InsertAnswerRecord(pFormFieldInfo);
    Result := True;
  except
    Result := False;
  end;

end;

function TAcustomFormDM.UpdateAnswerRecord(pFormFieldInfo: TFieldInfo): boolean;  //QM-593 EB Custom Form (AT&T Damage form)
begin
  Result := False;
  try
    UpdateExistingAnswer.ParamByName('answer_id').AsInteger := pFormFieldInfo.ExistingAnswerID;
    UpdateExistingAnswer.ParamByName('answer').AsString := pFormFieldInfo.Answer;
    UpdateExistingAnswer.ExecSQL;
  except
    {Swallow - Do nothing}
  end;




end;

end.
