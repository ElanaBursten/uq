object FadeBox: TFadeBox
  Left = 1300
  Top = 623
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 73
  ClientWidth = 140
  Color = clCream
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  Padding.Left = 10
  Padding.Top = 10
  Padding.Right = 10
  Padding.Bottom = 10
  OldCreateOrder = True
  Position = poDesigned
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 10
    Top = 10
    Width = 120
    Height = 53
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    object lblHeSays: TLabel
      Left = 0
      Top = 0
      Width = 102
      Height = 26
      Align = alClient
      Alignment = taCenter
      Caption = 'It stays the size I want it to be!'
      Color = clCream
      ParentColor = False
      WordWrap = True
    end
  end
  object fadeTimer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = fadeTimerTimer
    Left = 103
    Top = 27
  end
end
