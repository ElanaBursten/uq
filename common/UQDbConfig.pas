unit UQDbConfig;

interface

uses IniFiles;

type
  // abstract TDatabaseConfig class
  TDatabaseConfig = class
  protected
    FIni: TIniFile;
    FSection: string;
    function GetValueFromConnectionString(const ConnString, Key: string): string; virtual;
    function GetConnString: string; virtual; abstract;
    function GetDB: string; virtual; abstract;
    function GetPassword: string; virtual; abstract;
    function GetPooling: Boolean; virtual; abstract;
    function GetServer: string; virtual; abstract;
    function GetTrusted: Boolean; virtual; abstract;
    function GetUsername: string; virtual; abstract;
    procedure LoadConfig;
  public
    Server: string;
    DB: string;
    Trusted: Boolean;
    Username: string;
    Password: string;
    Pooling: Boolean;
    Name: string;
    ConnString: string;
    function Description: string;
    constructor CreateFromIni(Ini: TIniFile; const Section, AName: string);
  end;

  // ADO Based TDatabaseConfig
  TADODatabaseConfig = class(TDatabaseConfig)
  protected
    function GetConnString: string; override;
    function GetDB: string; override;
    function GetPassword: string; override;
    function GetPooling: Boolean; override;
    function GetServer: string; override;
    function GetTrusted: Boolean; override;
    function GetUsername: string; override;
  end;

  // ODBC (AKA FireDAC) Based TDatabaseConfig
  TODBCDatabaseConfig = class(TDatabaseConfig)
  protected
    function GetConnString: string; override;
    function GetDB: string; override;
    function GetPassword: string; override;
    function GetPooling: Boolean; override;
    function GetServer: string; override;
    function GetTrusted: Boolean; override;
    function GetUsername: string; override;
  end;

implementation

uses
  SysUtils, StrUtils, OdMiscUtils, OdExceptions;

function IsValueTrue(const Val: string): Boolean;
const
  TrueValues: array[0..2] of string = ('1', 'yes', 'true');
begin
  Result := StringInArray(LowerCase(Val), TrueValues);
end;

{ TDatabaseConfig }

function TDatabaseConfig.Description: string;
begin
  Result := Server + '/' + DB + IfThen(Pooling, '[P]') + '/' + Username + IfThen(Trusted, ' (Trusted)');
end;

function TDatabaseConfig.GetValueFromConnectionString(const ConnString, Key: string): string;
var
  TempString: string;
begin
  Assert(NotEmpty(ConnString), 'Connection string is empty');
  Assert(NotEmpty(Key), 'Key name is empty');
  Result := '';
  if Pos(Key, LowerCase(ConnString)) > 0 then begin
    TempString := Copy(ConnString, Pos(Key, LowerCase(ConnString)) + Length(Key), Length(ConnString));
    Result := GetStringBeforeCharacter(TempString, ';');
  end;
end;

procedure TDatabaseConfig.LoadConfig;
begin
  ConnString := GetConnString;
  if IsEmpty(ConnString) then
  begin
    Server := GetServer;
    DB := GetDB;
    Trusted := GetTrusted;
    Username := GetUsername;
    Password := GetPassword;
    Pooling := GetPooling;
  end;

  if IsEmpty(DB) and IsEmpty(ConnString) then
    raise EOdNoDatabaseSetting.CreateFmt('Could not load DB config INI file: %s Section=%s CurrentDirectory=%s'  + ' (LoadConfig)',
      [FIni.FileName, FSection, GetCurrentDir]);
end;

constructor TDatabaseConfig.CreateFromIni(Ini: TIniFile; const Section, AName: string);
begin
  inherited Create;
  Assert(Assigned(Ini), 'Ini must be defined');
  FIni := Ini;
  FSection := Section;
  LoadConfig;
  Name := AName + ': ' + Server + '/' + DB + IfThen(Pooling, '[P]');
end;

{ TODBCDatabaseConfig }

function TODBCDatabaseConfig.GetConnString: string;
begin
  Result := FIni.ReadString(FSection, 'ConnectionString', ''); //Must be connection string   SR
  if IsEmpty(Result) then
    raise EOdNoDatabaseSetting.CreateFmt('No ConnectionString configured in INI file: %s Section=%s CurrentDirectory=%s',
      [FIni.FileName, FSection, GetCurrentDir]);
end;

function TODBCDatabaseConfig.GetServer: string;
begin
  Result := GetValueFromConnectionString(ConnString, 'server=');
end;

function TODBCDatabaseConfig.GetDB: string;
begin
  Result := GetValueFromConnectionString(ConnString, 'database=');
end;

function TODBCDatabaseConfig.GetUsername: string;
begin
  Result := GetValueFromConnectionString(ConnString, 'user_name=');
end;

function TODBCDatabaseConfig.GetPassword: string;
begin
  Result := GetValueFromConnectionString(ConnString, 'password=');
end;

function TODBCDatabaseConfig.GetPooling: Boolean;
begin
  Result := IsValueTrue(GetValueFromConnectionString(ConnString, 'pooled='));
end;

function TODBCDatabaseConfig.GetTrusted: Boolean;
begin
  Result := IsValueTrue(GetValueFromConnectionString(ConnString, 'osauthent='));
end;

{ TADODatabaseConfig }

function TADODatabaseConfig.GetConnString: string;
begin
  Result := FIni.ReadString(FSection, 'ConnectionString', ''); //Must be ConnectionString!!!  sr
end;

function TADODatabaseConfig.GetServer: string;
const
  Identifiers: array[0..4] of string = ('data source=', 'server=', 'address=', 'addr=', 'network address=');
var
  Id: string;
begin
  Result := FIni.ReadString(FSection, 'Server', '');
  if NotEmpty(ConnString) and NotEmpty(Result) then
    raise EOdMutlipleDatabaseSetting.Create('Both a connection string and Server cannot be specified in ' +
      FIni.FileName + '.' + CRLF + CRLF +
      'Please specify either a database connection string OR server name, database name, username and password.');

  if IsEmpty(Result) and IsEmpty(ConnString) then
    raise EOdNoDatabaseSetting.CreateFmt('Could not load DB config INI file: %s Section=%s CurrentDirectory=%s' + ' (GetServer)',
      [FIni.FileName, FSection, GetCurrentDir]);

  if IsEmpty(Result) then begin
    for Id in Identifiers do begin
      Result := GetValueFromConnectionString(ConnString, Id);
      if NotEmpty(Result) then
        Break;
    end;
  end;
end;

function TADODatabaseConfig.GetDB: string;
const
  Identifiers: array[0..1] of string = ('initial catalog=', 'database=');
var
  Id: string;
begin
  Result := FIni.ReadString(FSection, 'DB', '');
  if NotEmpty(ConnString) and NotEmpty(Result) then
    //both connection methods have been specified; raise an error- choice must be made
    raise EOdMutlipleDatabaseSetting.Create('Both a connection string and DB cannot be specified in ' +
      FIni.FileName + '.' + CRLF + CRLF +
      'Please specify either a database connection string OR server name, database name, username and password.');

  if IsEmpty(Result) and IsEmpty(ConnString) then
    raise EOdNoDatabaseSetting.CreateFmt('Could not load DB config INI file: %s Section=%s CurrentDirectory=%s' + ' (GetDB)',
      [FIni.FileName, FSection, GetCurrentDir]);

  if IsEmpty(Result) then begin
    for Id in Identifiers do begin
      Result := GetValueFromConnectionString(ConnString, Id);
      if NotEmpty(Result) then
        Break;
    end;
  end;
end;

function TADODatabaseConfig.GetUsername: string;
const
  Identifiers: array[0..1] of string = ('user id=', 'uid=');
var
  Id: string;
begin
  Result := FIni.ReadString(FSection, 'UserName', '');
  if IsEmpty(Result) and NotEmpty(ConnString) then begin
    for Id in Identifiers do begin
      Result := GetValueFromConnectionString(ConnString, Id);
      if NotEmpty(Result) then
        Break;
    end;
  end;
end;

function TADODatabaseConfig.GetPassword: string;
begin
  Result := FIni.ReadString(FSection, 'Password', '');
  if IsEmpty(Result) and NotEmpty(ConnString) then
    Result := GetValueFromConnectionString(ConnString, 'password=');
end;

function TADODatabaseConfig.GetPooling: Boolean;
begin
  Result := IsValueTrue(FIni.ReadString(FSection, 'Pooling', 'True'));
end;

function TADODatabaseConfig.GetTrusted: Boolean;
const
  Identifiers: array[0..1] of string = ('trusted_connection=', 'integrated security=');
var
  Id: string;
  Trusted: string;
begin
  Result := False;
  Trusted := FIni.ReadString(FSection, 'Trusted', '');
  if NotEmpty(Trusted) then
    Result := Trusted = '1'
  else if NotEmpty(ConnString) then begin
    for Id in Identifiers do begin
      Trusted := GetValueFromConnectionString(ConnString, Id);
      if NotEmpty(Trusted) then begin
        Result := IsValueTrue(Trusted);
        Break;
      end;
    end;
  end;
end;

end.
