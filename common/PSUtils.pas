unit PSUtils;
{EB 4/2020 - Console and Powershell tools}
{Returned Datetimes are localized}

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StrUtils,  ComObj, ComCtrls,
  PJPipe, PJConsoleApp;

  type
   TPSInfo = class(TObject)
     private
       fOutPipe: TPJPipe;
       fOutStream: TStream;
       fSystemFormat: TFormatSettings;
       fOutput: TStringList;
       function ParseOutput: string;
       procedure ClearOutput;
       procedure WorkHandler(Sender: TObject);
       function PSStrToDateTime(FString: string): TDateTime;
     public
     Constructor Create;
     Destructor Destroy; override;
     function RunCommandOnly(ACommand: string; ATempFile: string): String;
     function RunPS(PSCommand: string): string;
     function RunPSBootupTime: TDateTime;
     function RunPSVersion: string;
     function RunOSVersion: string;
     function RunPSTimeZone: string;
     function RunPSNow: TDatetime;
     property SystemFormat: TFormatSettings read fSystemFormat;
   end;
   {Direct calls}
   function GetPSBootupTime: TDateTime;
   function GetPSVersion: string;
   function GetOSVersion: string;
   function GetPSTimeZone: string;
   function GetPSNow: TDatetime;
   function GetSim: string;

implementation
const
  SimCommand = 'netsh mbn sh interface';  {standard Commandline: netsh mbn sh interface}
  pipeit = '>';
  p = '$P = ';
  PFile = 'pSimCardData.txt';    {Return Variable}
  BootupTimeCommand = 'Get-CimInstance -ClassName win32_operatingsystem | select lastbootuptime | Format-Table -HideTableHeaders'; //-expandproperty lastbootuptime';
  LocalTime = 'Get-Date';
  PSVersion = '$PSVersionTable.PSVersion | ft -HideTableHeaders';
  OSVersion = '[System.Environment]::OSVersion.Version | ft -HideTableHeaders';
  TimeZone = 'Get-TimeZone | Select DisplayName | ft -HideTableHeaders';
  ComputerName = '$env:computername';
  WriteOutput =#13#10 + 'Write-Output "<<<" $P ">>>"'; {<<< >>> are used for parsing}


{ TPSInfo }

procedure TPSInfo.ClearOutput;
begin
  fOutput.Text := '';
end;

constructor TPSInfo.Create;
begin
  fOutput := TStringList.Create;
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fSystemFormat);
end;

destructor TPSInfo.Destroy;
begin
  FreeAndNil(fOutput);
end;

function TPSInfo.PSStrToDateTime(FString: string): TDateTime;
begin
  try
    Result := StrToDateTime(FString, fSystemFormat);
  except
    Result := 0.0;
  end;
end;

function TPSInfo.ParseOutput: string;
var
  DTStartPos, DTEndPos: integer;
  OutStr: string;
begin
  Result := '';
    DTStartPos := LastDelimiter('<<<', fOutput.Text);
    DTEndPos :=   LastDelimiter('>>>', fOutput.Text);
    if DTStartPos > 0 then begin
      OutStr := copy(fOutput.Text, DTStartPos, DTEndPos);
      OutStr := ReplaceStr(OutStr, '<',  '');

      DTEndPos := AnsiPos('>>>', OutStr);
      OutStr := copy(OutStr, 0, DTEndPos);
      OutStr := ReplaceStr(OutStr, '>',  '');
      OutStr := Trim(OutStr);
    end;
    Result := OutStr;
end;




function TPSInfo.RunOSVersion: string;
begin
  Result := RunPS(P+OSVersion+WriteOutput);
end;


function TPSInfo.RunCommandOnly(ACommand: string; ATempFile: string): string;
var
  App: TPJConsoleApp;
  InPipe: TPJPipe;
  LCommand: string;
  TempLoad: TStringList;
begin
  fOutStream := nil;
  fOutPipe := nil;
  TempLoad := TStringList.Create;
  Result := '';
  if ACommand = '' then
    Exit;
  LCommand :=   SimCommand +  pipeit + ATempFile;
  InPipe := TPJPipe.Create(Length(LCommand));
  InPipe := TPJPipe.Create(Length(LCommand));
  try
    {$IFDEF UNICODE}
    InPipe.WriteBytes(TEncoding.Default.GetBytes(LCommand));
    {$ELSE}
    InPipe.WriteData(PChar(LCommand)^, Length(LCommand));
    {$ENDIF}
    InPipe.CloseWriteHandle;
    // Create out pipe and stream that receives out pipe's data
    fOutPipe := TPJPipe.Create;
    fOutStream := TMemoryStream.Create;
    // Execute the application
    App := TPJConsoleApp.Create;
    try
      App.TimeSlice := 2; // forces more than one OnWork event
      App.OnWork := WorkHandler;
      App.StdIn := InPipe.ReadHandle;
      App.StdOut := fOutPipe.WriteHandle;

      if not App.Execute('cmd', '') then
          Result := '';

    finally

      App.Free;
    end;
    {Load the Output stream into the fOutStringList
      Echoer.exe writes output as ANSI text.
      OK on Unicode Delphis because following LoadFromStream call
      defaults to Default (ANSI) encoding if no encoding specified.
    }
    fOutStream.Position := 0;
    fOutput.LoadFromStream(fOutStream);
    TempLoad.LoadFromFile(ATempFile);
    Result := TempLoad.Text;
    //fOutput.text;
  finally
    FreeAndNil(InPipe);
    FreeAndNil(fOutPipe);
    FreeAndNil(fOutStream);
    FreeAndNil(TempLoad);
    ClearOutput;
  end;
end;

function GetDosOutput(CommandLine: string; Work: string = 'C:\'): string;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  StdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  Buffer: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WorkDir: string;
  Handle: Boolean;
begin
  Result := '';
  with SA do begin
    nLength := SizeOf(SA);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
  try
    with SI do
    begin
      FillChar(SI, SizeOf(SI), 0);
      cb := SizeOf(SI);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := StdOutPipeWrite;
      hStdError := StdOutPipeWrite;
    end;
    WorkDir := Work;
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + CommandLine),
                            nil, nil, True, 0, nil,
                            PChar(WorkDir), SI, PI);
    CloseHandle(StdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then
          begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
  finally
    CloseHandle(StdOutPipeRead);
  end;
end;



function TPSInfo.RunPS(PSCommand: string): string;
var
  App: TPJConsoleApp;
  InPipe: TPJPipe;
begin
  fOutStream := nil;
  fOutPipe := nil;
  Result := '';
  if PSCommand = '' then
    Exit;

  InPipe := TPJPipe.Create(Length(PSCommand));
  try
    {$IFDEF UNICODE}
    InPipe.WriteBytes(TEncoding.Default.GetBytes(PSCommand));
    {$ELSE}
    InPipe.WriteData(PChar(PSCommand)^, Length(PSCommand));
    {$ENDIF}
    InPipe.CloseWriteHandle;
    // Create out pipe and stream that receives out pipe's data
    fOutPipe := TPJPipe.Create;
    fOutStream := TMemoryStream.Create;
    // Execute the application
    App := TPJConsoleApp.Create;
    try
      App.TimeSlice := 2; // forces more than one OnWork event
      App.OnWork := WorkHandler;
      App.StdIn := InPipe.ReadHandle;
      App.StdOut := fOutPipe.WriteHandle;

      if not App.Execute('Powershell') then
        Result := '';

    finally
      App.Free;
    end;
    {Load the Output stream into the fOutStringList
      Echoer.exe writes output as ANSI text.
      OK on Unicode Delphis because following LoadFromStream call
      defaults to Default (ANSI) encoding if no encoding specified.
    }
    fOutStream.Position := 0;
    fOutput.LoadFromStream(fOutStream);
    Result := ParseOutput;
  finally
    FreeAndNil(InPipe);
    FreeAndNil(fOutPipe);
    FreeAndNil(fOutStream);
    ClearOutput;
  end;

end;

function TPSInfo.RunPSBootupTime: TDateTime;
var
  TimeStr: string;
begin
  TimeStr := RunPS(P+BootupTimeCommand+WriteOutput);
  Result := PSStrToDateTime(TimeStr);
end;

function TPSInfo.RunPSNow: TDatetime;
var
  TimeStr: string;
begin
  TimeStr := RunPS(P+LocalTime+WriteOutput);
  Result := PSStrToDateTime(TimeStr);
end;

function TPSInfo.RunPSTimeZone: string;
begin
  Result := RunPS(P+TimeZone+WriteOutput);
end;

function TPSInfo.RunPSVersion: string;
begin
  Result := RunPS(P+PSVersion+WriteOutput);
end;

procedure TPSInfo.WorkHandler(Sender: TObject);
begin
  fOutPipe.CopyToStream(fOutStream, 0);
end;

function GetPSBootupTime: TDateTime;
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    Result := PSInfo.RunPSBootupTime;
  finally
    FreeAndNil(PSInfo);
  end;
end;

function GetPSVersion: string;
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    Result := PSInfo.RunPSVersion;
  finally
    FreeAndNil(PSInfo);
  end;
end;

function GetOSVersion: string;
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    Result := PSInfo.RunOSVersion;
  finally
    FreeAndNil(PSInfo);
  end;
end;

function GetPSTimeZone: string;
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    Result := PSInfo.RunPSTimeZone;
  finally
    FreeAndNil(PSInfo);
  end;
end;

function GetPSNow: TDatetime;
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    Result := PSInfo.RunPSNow;
  finally
    FreeAndNil(PSInfo);
  end;
end;

function GetSim: string; {EB - Not working}
var
  PSInfo: TPSInfo;
begin
  PSInfo := TPSInfo.Create;
  try
    try
 //     Result := PSInfo.RunPS(P+SimCommand+WriteOutput);  Doesn't work
      PSInfo.RunCommandOnly(SimCommand, PFile);
      ShowMessage(Result);
    except
       {Swallow}
    end;
  finally
    FreeAndNil(PSInfo);
  end;
end;




end.
