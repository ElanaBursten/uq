//*********************************************************
//*                   OdSimpleLog.pas                     *
//*          Copyright (c) 2002 - 2014 by Oasis Digital          *
//*                All rights reserved.                   *
//*********************************************************

{$I-}

unit OdSimpleLog;

interface

type
  TSimpleLog = class
  protected
    FInstanceId : Integer;
    FDirectory: string;
    FFilename: string;
    FLogFile: TextFile;
    procedure OnCreate; virtual;
    procedure OnDestroy; virtual;
    function OpenFile: Boolean;
    function GetLogName: string;
  public
    constructor Create(const FDir: string; const FName: string; const FInstId: Integer = 0);
    destructor Destroy; override;
    procedure Append(const S: string);

    property Filename: string read GetLogName;
    property InstanceId : Integer read FInstanceId;
    property Directory: string read FDirectory;
  end;

  TEnhancedLog = class(TSimpleLog)
  protected
    procedure OnDestroy; override;
  public
    constructor Create(Fn, Path: string);
    procedure AddLogEntry(const Message: string; IsError : Boolean = False);
  end;


//============================================================================//

implementation

uses
  Windows, SysUtils, OdMiscUtils;

//TSimpleLog

constructor TSimpleLog.Create(const FDir: string; const FName: string; const FInstId: Integer = 0);
begin
  inherited Create;

  FDirectory := FDir;
  FFilename := FName;
  FInstanceId := FInstId;

  if FDirectory = '' then
    raise Exception.Create('Directory is required');

  FDirectory := IncludeTrailingPathDelimiter(FDirectory);
  if not DirectoryExists(FDirectory) then
    MkDir(FDirectory);

  if not DirectoryExists(FDirectory) then
    raise Exception.Create('Directory does not exist: ' + FDirectory);

  OnCreate;
end;


destructor TSimpleLog.Destroy;
begin
  OnDestroy;

  inherited Destroy;
end;

function TSimpleLog.GetLogName: string;
begin
  Result := GetDailyFileName(FDirectory, FFilename, FInstanceId);
end;

function TSimpleLog.OpenFile: Boolean;
const
  MaxRetries = 10;
var
  I: Integer;
begin
  if not FileExists(GetLogName) then begin
    AssignFile(FLogFile, GetLogName);
    Rewrite(FLogFile);
    IoResult;
    CloseFile(FLogFile);
    IoResult;
  end;

  for I := 1 to MaxRetries do begin
    AssignFile(FLogFile, GetLogName);
    System.Append(FLogFile);

    if IoResult = 0 then begin
      Result := True;
      Exit;
    end;

    //delay a bit between tries
    Sleep(50);
  end;

  //if we get to here we failed
  Result := False;
end;

procedure TSimpleLog.Append(const S : string);
var
  ST: string;
  I: Integer;
begin
  if OpenFile then
    try
      //turns CRLF's into '||'
      ST := S;
      for I := 1 to Length(ST) do
        if (ST[I] < ' ') and (ST[I] <> ^I) then
          ST[I] := '|';

      Writeln(FLogFile, ST);

      //eat the error
      IoResult;
    finally
      CloseFile(FLogFile);
      IoResult;
    end;
end;

procedure TSimpleLog.OnCreate;
begin
  //just a hook
end;

procedure TSimpleLog.OnDestroy;
begin
  //just a hook
end;

//TEnhancedLog
constructor TEnhancedLog.Create(Fn, Path: string);
begin
  inherited Create(Path, Fn);

  if not FileExists(FileName) then begin
    Append('Type'^I'Timestamp'^I^I'Message');
  end;

  Append('');
  AddLogEntry('Begin');
end;

procedure TEnhancedLog.AddLogEntry(const Message: string; IsError : Boolean = False);
const
  IsErrorStr : array[Boolean] of string[3] = ('INF', 'ERR');
begin
  Append(Format('%s'^I'%s'^I'%s',
    [IsErrorStr[IsError], FormatDateTime('mm/dd/yyyy hh:nn:ss', Now), Message]));
end;

procedure TEnhancedLog.OnDestroy;
begin
  AddLogEntry('End');
end;

end.
