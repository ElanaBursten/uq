unit OdHourglass;

interface

function ShowHourGlass: IInterface;

implementation

uses
  Controls, Forms;

{ Usage:
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
}

type
  TShowHourGlass = class(TInterfacedObject, IInterface)
  private
    fOldCursor: TCursor;
  public
    constructor Create;
    destructor Destroy; override;
  end;

constructor TShowHourGlass.Create;
begin
  fOldCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
end;

destructor TShowHourGlass.Destroy;
begin
  Screen.Cursor := fOldCursor;
end;

function ShowHourGlass: IInterface;
begin
  Result := TShowHourGlass.Create;
end;

end.

