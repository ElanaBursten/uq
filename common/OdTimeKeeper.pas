unit OdTimeKeeper;

interface

uses
  Classes, SysUtils, Db, DBISAMLB, SyncEngineU, LocalPermissionsDMu;

type
  TTimeKeeperMessage = record
    CountdownSeconds: Integer;
    MessageText: string;
    Shown: Boolean;
  end;

  TTimeKeeper = class(TObject)
  private
    FNextMessage: string;
    FNotices: array of TTimeKeeperMessage;
    FEmpID: Integer;
    function LoadDescriptionOfTimeLimits(EmpID: Integer; Engine: TLocalCache): string;
    function GetSecondsRemaining: Integer;
    function GetNextMessage: string;
    function LimitHoursSQL(RestrictionType: string = ''): string;
  public
    StartTime: TDateTime;
    EndTime: TDateTime;
    StartExemption: TDateTime;
    EndExemption: TDateTime;
    DescriptionOfTimeLimits: string;
    constructor Create(EmpID: Integer; Engine: TLocalCache);
    procedure Initialize(EmpID: Integer; Engine: TLocalCache);
    property SecondsRemaining: Integer read GetSecondsRemaining;
    property NextMessage: string read GetNextMessage;
  end;

implementation

uses
  QMConst, DMu, OdMiscUtils, DateUtils, JclStrings;

{ TTimeKeeper }

function TTimeKeeper.GetNextMessage: string;
var
  i: Integer;
  MaxNotices: Integer;
  NeedToShow: Boolean;
  TimeLeft: Integer;
begin
  Result := '';
  TimeLeft := SecondsRemaining;
  MaxNotices := Length(FNotices) - 1;
  for i := 0 to MaxNotices do begin
    if (TimeLeft <= FNotices[i].CountdownSeconds) then begin
      NeedToShow := not FNotices[i].Shown;
      if (i < MaxNotices) then
        NeedToShow := NeedToShow and (TimeLeft > FNotices[i + 1].CountdownSeconds);
      if NeedToShow then begin
        Result := FNotices[i].MessageText;
        FNotices[i].Shown := True;
        Break;
      end;
    end;
  end;

  FNextMessage := Result;
end;

procedure TTimeKeeper.Initialize(EmpID: Integer; Engine: TLocalCache);
const
  ExemptionSQL = 'select effective_date, expire_date ' +
    'from restricted_use_exemption where revoked_date is null and emp_id=%d ' +
    'and expire_date>''%s''';
  RestrictedUseMessageSQL = 'select message_text, countdown_minutes ' +
    'from restricted_use_message where active order by countdown_minutes desc';
var
  DataSet: TDataSet;
  RestrictionType: string;
  i: Integer;
begin
  Assert(Assigned(Engine), 'Local data cache has not been assigned yet.');
  Assert(EmpID > 0, 'Employee ID has not been established.');

  FEmpID := EmpID;
  StartTime := EncodeTime(0, 0, 0, 0);
  EndTime := EncodeTime(23, 59, 59, 0);
  StartExemption := -1;
  EndExemption := -1;
  SetLength(FNotices, 0);

  if IsWeekend(Date) then
    RestrictionType := UsageRestrictionTypeWeekend
  else
    RestrictionType := UsageRestrictionTypeWeekday;

  DataSet := Engine.OpenQuery(LimitHoursSQL(RestrictionType));
  try
    if not DataSet.Eof then begin
      StartTime := StrToTime(DataSet.FieldByName('start_value').AsString);
      EndTime := StrToTime(DataSet.FieldByName('end_value').AsString);
    end;
  finally
    FreeAndNil(DataSet);
  end;

  DataSet := Engine.OpenQuery(Format(ExemptionSQL, [EmpID, AnsiDateTimeToStr(CurrentYDMHMS, True)]));
  try
    if not DataSet.Eof then begin
      StartExemption := DataSet.FieldByName('effective_date').AsDateTime;
      EndExemption := DataSet.FieldByName('expire_date').AsDateTime;
    end;
  finally
    FreeAndNil(DataSet);
  end;

  DataSet := Engine.OpenQuery(RestrictedUseMessageSQL);
  try
    if not DataSet.Eof then begin
      i := 0;
      SetLength(FNotices, DataSet.RecordCount);
      while not DataSet.Eof do begin
        FNotices[i].CountdownSeconds := DataSet.FieldByName('countdown_minutes').AsInteger * 60;
        FNotices[i].MessageText := DataSet.FieldByName('message_text').AsString;
        FNotices[i].Shown := False;
        DataSet.Next;
        Inc(i);
      end;
    end;
  finally
    FreeAndNil(DataSet);
  end;

  DescriptionOfTimeLimits := LoadDescriptionOfTimeLimits(EmpID, Engine);
end;

function TTimeKeeper.LoadDescriptionOfTimeLimits(EmpID: Integer; Engine: TLocalCache): string;
var
  DataSet: TDataSet;
begin
  Result := 'Q Manager may only be used during the following times: ' + CRLF;
  DataSet := Engine.OpenQuery(LimitHoursSQL);
  try
    while not DataSet.Eof do begin
      Result := Result +  StrSmartCase(LowerCase(DataSet.FieldByName('restriction_type').AsString), []) + ' Hours: ' +
        DataSet.FieldByName('start_value').AsString + ' through ' +
        DataSet.FieldByName('end_value').AsString + CRLF;
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

constructor TTimeKeeper.Create(EmpID: Integer; Engine: TLocalCache);
begin
  inherited Create;
  Initialize(EmpID, Engine);
end;

function TTimeKeeper.GetSecondsRemaining: Integer;
var
  CurrentTime: TDateTime;
  LowTime: TDateTime;
  HighTime: TDateTime;
begin
  CurrentTime := CurrentYDMHMS;
  LowTime := DateOf(CurrentTime) + StartTime;
  HighTime := DateOf(CurrentTime) + EndTime;

  if (StartExemption > -1) and (StartExemption < LowTime) then
    LowTime := StartExemption;

  if (EndExemption > -1) and (EndExemption > HighTime) then
    HighTime := EndExemption;

  if (CurrentTime < LowTime) or (CurrentTime > HighTime) then
    Result := 0
  else
    Result := SecondsBetween(HighTime, CurrentTime);
end;

function TTimeKeeper.LimitHoursSQL(RestrictionType: string): string;
const
  SQL = 'select restriction_type, start_value, end_value ' +
    'from right_restriction rr ' +
    'where rr.group_name = ''%s'' and rr.active and rr.right_id = %d and 1=%d'; // and rr.restriction_type=''...''
var
  GroupName: string;
  RightID: Integer;
  Granted: Boolean;
begin
  Granted := PermissionsDM.IsRightActive(RightRestrictUsageLimitHours);
  GroupName := PermissionsDM.GetLimitation(RightRestrictUsageLimitHours);
  RightID := PermissionsDM.GetRightID(RightRestrictUsageLimitHours);
  Result := Format(SQL, [GroupName, RightID, Ord(Granted)]);
  if NotEmpty(RestrictionType) then
    Result := Result + Format(' and rr.restriction_type=''%s''', [RestrictionType]);
end;

end.
