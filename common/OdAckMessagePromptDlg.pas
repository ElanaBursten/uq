unit OdAckMessagePromptDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdAckMessageDlg, ExtCtrls, StdCtrls, Mask, OdMiscUtils, OdExceptions;

type
  TAckMessagePromptDialog = class(TAckMessageDialog)
    PromptPanel: TPanel;
    PromptEdit: TMaskEdit;
    PromptLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private

  public
    procedure SetupPrompt(const Caption, Mask, Default: string);
    property ClickedButtonCaption;
  end;

function ShowMessageAckPromptDialog(const MessageText, ButtonCaptions,
    PromptCaption, EditMask, Default: string): string;

implementation

{$R *.dfm}

function ShowMessageAckPromptDialog(const MessageText, ButtonCaptions,
    PromptCaption, EditMask, Default: string): string;
var
  Dialog: TAckMessagePromptDialog;
begin
  Assert(Length(MessageText) > 0, 'Blank message text is not allowed');
  Assert(Length(ButtonCaptions) > 0, 'Blank button captions are not allowed');

  Dialog := TAckMessagePromptDialog.Create(nil);
  try
    Result := '';
    Dialog.SetupControls(MessageText, ButtonCaptions, '');
    Dialog.SetupPrompt(PromptCaption, EditMask, Default);
    Dialog.ShowModal;
    if not SameText(Dialog.ClickedButtonCaption, 'cancel') then
      Result := Dialog.PromptEdit.Text;
  finally
    FreeAndNil(Dialog);
  end;
end;

{ TAckMessagePromptDialog }

procedure TAckMessagePromptDialog.SetupPrompt(const Caption, Mask, Default: string);
begin
  PromptLabel.Caption := Caption;
  PromptEdit.EditMask := Mask;
  PromptEdit.Text := Default;
end;

procedure TAckMessagePromptDialog.FormShow(Sender: TObject);
begin
  inherited;
  if PromptEdit.CanFocus then
    PromptEdit.SetFocus;
end;

procedure TAckMessagePromptDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := False;
  if IsEmpty(PromptEdit.Text) then
    raise EOdDataEntryError.Create('Enter ' + PromptLabel.Caption + ' before closing.');

  CanClose := True;
end;

end.
