unit OdSelectImage;

interface

uses Windows, Classes, Sysutils, Controls, StdCtrls, ExtCtrls, Graphics, jpeg, Forms{, GIFImage};

type
  TSelectImage = class;
  TSelectImageList = class;

  TOnProcessImageEvent = procedure (Sender: TSelectImageList; const ImageNumber: Integer) of object;

  TSelectImageList = class(TList)
  private
    FAfterLoadImages: TNotifyEvent;
    FOnLoadImage: TOnProcessImageEvent;
    FOnCopyImage: TOnProcessImageEvent;
    FAfterCopyImages: TNotifyEvent;
    procedure SetAfterLoadImages(const Value: TNotifyEvent);
    procedure SetOnLoadImage(const Value: TOnProcessImageEvent);
    procedure SetOnCopyImage(const Value: TOnProcessImageEvent);
    procedure SetAfterCopyImages(const Value: TNotifyEvent);
  protected
    FCancelCurrentOperation: Boolean;
    procedure CheckCancel;
  public
    constructor Create;
    destructor Destroy; override;
    procedure GenerateStructure(FileList: TStrings);
    procedure LoadImages;
    procedure ClearImages;
    procedure ArrangeImagesOnParent(Parent: TWinControl; ShowSeletedOnly: Boolean = False);
    procedure SelectAll;
    procedure CopyImagesToDest(const Path: string);
    procedure DeleteSelectedImagesFromSource;
    procedure GetSelectedFileNames(FileList: TStrings);
    procedure CancelCurrentOperation;
    //
    property OnLoadImage: TOnProcessImageEvent read FOnLoadImage write SetOnLoadImage;
    property AfterLoadImages: TNotifyEvent read FAfterLoadImages write SetAfterLoadImages;
    property OnCopyImage: TOnProcessImageEvent read FOnCopyImage write SetOnCopyImage;
    property AfterCopyImages: TNotifyEvent read FAfterCopyImages write SetAfterCopyImages;
  end;

  TSelectImage = class(TPanel)
  private
    FImage: TImage;
    FCheckBoxSelImage: TCheckBox;
    FFileName: TFileName;
    procedure SetFileName(const Value: TFileName);
    procedure SetSelected(const Value: Boolean);
    function GetFileName: TFileName;
    function GetSelected: Boolean;
    procedure OnClickCheckBoxSelImage(Sender: TObject);
    procedure OnClickImage(Sender: TObject);
  public
    constructor Create(Owner: TComponent); override;
    procedure Clear;
    //
    property FileName: TFileName read GetFileName write SetFileName;
    property Selected: Boolean read GetSelected write SetSelected;
    //
    procedure LoadImage(FileName: TFileName = '');
  end;

implementation

{ TSelectImage }

uses
  ODMiscUtils; {SHCopyFiles}

var
  JpegThumbnail: TJpegImage;

constructor TSelectImage.Create(Owner: TComponent);
const
  DefWidth  = 120;
  DefHeight = 140;
begin
  inherited;
  Width := DefWidth;
  Height := DefHeight;
  Caption := '';
  TabOrder := 0;
  BevelInner := bvNone;
  BevelOuter := bvNone;
  Color := clWindow;
  OnClick := OnClickImage;

  FImage := TImage.Create(Self);
  with FImage do
  begin
    Name := 'Image';
    Parent := Self;
    Left := 8;
    Top := 8;
    Width := DefWidth - 15;
    Height := DefHeight -15;
    Center := True;
    Proportional := True;
    OnClick := OnClickImage;
  end;

  FCheckBoxSelImage := TCheckBox.Create(Self);
  with FCheckBoxSelImage do
  begin
    Parent := Self;
    Left := 8;
    Top := 119; // FImage.Top + FImage.Height + 5;
    Width := 105;
    Height := 17;
    Caption := 'Attach this image';
    TabOrder := 0;
    Color := clWindow;
    OnClick := OnClickCheckBoxSelImage;
  end;
end;

////////////////

function TSelectImage.GetFileName: TFileName;
begin
  Result := FFileName;
end;

////////////////

function TSelectImage.GetSelected: Boolean;
begin
  Result := FCheckBoxSelImage.Checked;
end;

////////////////

procedure TSelectImage.LoadImage(FileName: TFileName);
begin
  if FileName <> '' then
    Self.FileName := FileName;

  JpegThumbnail.LoadFromFile(Self.FileName);
  JpegThumbnail.Scale := jsFullSize;
  JpegThumbnail.Scale := jsHalf;
  JpegThumbnail.Scale := jsQuarter;
  JpegThumbnail.Scale := jsEighth;
  JpegThumbnail.DIBNeeded;

  FImage.Picture.Assign(JpegThumbnail);
  //TODO: Replace the ResizeImage function with an equivalent JCL function if the bulk photo attachment feature will be used
  // ResizeImage(fImage.Picture.Graphic, _160x120);
end;

////////////////

procedure TSelectImage.Clear;
begin
  FImage.Picture.Assign(nil);
  Visible := False;
end;

////////////////

procedure TSelectImage.OnClickCheckBoxSelImage(Sender: TObject);
begin
  if Selected then begin
    Color := clInfoBk;
    BevelOuter := bvRaised;
    BevelInner := bvLowered;
  end else begin
    Color := clWindow;
    BevelOuter := bvNone;
    BevelInner := bvNone;
  end
end;

////////////////

procedure TSelectImage.OnClickImage(Sender: TObject);
begin
  Selected := not Selected;
end;

////////////////

procedure TSelectImage.SetFileName(const Value: TFileName);
begin
  FFileName := Value;
end;

////////////////

procedure TSelectImage.SetSelected(const Value: Boolean);
begin
  FCheckBoxSelImage.Checked := Value;
end;

{ TSelectImageList }

procedure TSelectImageList.GenerateStructure(FileList: TStrings);
var
  i: Integer;
  SelectImage: TSelectImage;
begin
  Assert(Assigned(FileList));
  for i := 0 to FileList.Count - 1 do begin
    CheckCancel;
    SelectImage := TSelectImage.Create(nil); // SelectImageList manages the image list
    Add(SelectImage);
    with TSelectImage(Self[Self.Count - 1]) do
      FileName := FileList[i];
  end;
end;

procedure TSelectImageList.LoadImages;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do begin
    CheckCancel;
    if Assigned(FOnLoadImage) then
      FOnLoadImage(Self, i + 1);
    try
      TSelectImage(Self[i]).LoadImage;
    except
      TSelectImage(Self[i]).Caption:='Error loading ' + ExtractFileName(TSelectImage(Self[i]).FileName);
    end;
  end;
  if Assigned(FAfterLoadImages) then
    FAfterLoadImages(Self);
end;

////////////////

procedure TSelectImageList.ClearImages;
begin
  while Count <> 0 do begin
    if Assigned(TSelectImage(Self[Count - 1])) then begin
      TSelectImage(Self[Count - 1]).Free;
    end;
    Delete(Count - 1);
  end;
end;

////////////////

procedure TSelectImageList.SelectAll;
var
  i: Integer;
begin
  for i := 0 to Count-1 do begin
    CheckCancel;
    TSelectImage(Self[i]).Selected := True;
  end;
end;

////////////////

procedure TSelectImageList.ArrangeImagesOnParent(Parent: TWinControl; ShowSeletedOnly: Boolean);
var
  i: Integer;
  LastTop, LastLeft: Integer;
begin
  Assert(Assigned(Parent));
  LastTop := 0;
  LastLeft := 0;
  for i:= 0 to Count-1 do begin
    TSelectImage(Self[i]).Visible := False;
  end;
  for i:= 0 to Count-1 do begin
    if ShowSeletedOnly then
      TSelectImage(Self[i]).Visible := TSelectImage(Self[i]).Selected
    else
      TSelectImage(Self[i]).Visible := True;

    if TSelectImage(Self[i]).Visible then begin
      TSelectImage(Self[i]).Left := LastLeft;
      TSelectImage(Self[i]).Top := LastTop;
      LastLeft := LastLeft + TSelectImage(Self[i]).Width + 1;
      if (LastLeft + TSelectImage(Self[i]).Width)>=Parent.Width then begin
        LastLeft := 0;
        LastTop := TSelectImage(Self[i]).Top + TSelectImage(Self[i]).Height + 1;
      end;
    end;
  end;
  for i:= Count-1 downto 0 do
    TSelectImage(Self[i]).Parent := Parent;
end;

////////////////

procedure TSelectImageList.CopyImagesToDest(const Path: string);
var
  i: Integer;
  FileName: string;
begin
  Assert(Trim(Path) <> '');
  OdForceDirectories(Path);
  for i := 0 to Count-1 do begin
    CheckCancel;
    if TSelectImage(Self[i]).Selected then begin
      FileName := ExtractFileName(TSelectImage(Self[i]).FileName);
      if Assigned(OnCopyImage) then
        OnCopyImage(Self, i+1);
      SHCopyFiles(TSelectImage(Self[i]).FileName, IncludeTrailingPathDelimiter(Path)+FileName, True);
    end;
  end;
  if Assigned(FAfterCopyImages) then
    FAfterCopyImages(Self);
end;

////////////////

procedure TSelectImageList.DeleteSelectedImagesFromSource;
var
  i: Integer;
  FileName: string;
begin
  for i := 0 to Count - 1 do begin
    CheckCancel;
    if TSelectImage(Self[i]).Selected then begin
      FileName := ExtractFileName(TSelectImage(Self[i]).FileName);
      DeleteFile(TSelectImage(Self[i]).FileName);
    end;
  end;
  i := 0;
  while i <= (Count - 1) do begin
    if Assigned(TSelectImage(Self[i])) then begin
      if TSelectImage(Self[i]).Selected then begin
        TSelectImage(Self[i]).Free;
        Delete(i);
      end
      else
        i := i + 1;
    end else
      i := i + 1;
  end;
end;

////////////////

procedure TSelectImageList.GetSelectedFileNames(FileList: TStrings);
var
  i: Integer;
begin
  Assert(Assigned(FileList));
  for i := 0 to Count - 1 do
    if TSelectImage(Self[i]).Selected then
      FileList.Add(ExtractFileName(TSelectImage(Self[i]).FileName));
end;

////////////////

procedure TSelectImageList.CancelCurrentOperation;
begin
  FCancelCurrentOperation := True;
end;

////////////////

procedure TSelectImageList.CheckCancel;
begin
  Application.ProcessMessages;
  if FCancelCurrentOperation then begin
    FCancelCurrentOperation := False;
    Abort;
  end;
end;

////////////////

procedure TSelectImageList.SetAfterCopyImages(const Value: TNotifyEvent);
begin
  FAfterCopyImages := Value;
end;

////////////////

procedure TSelectImageList.SetAfterLoadImages(const Value: TNotifyEvent);
begin
  FAfterLoadImages := Value;
end;

////////////////

procedure TSelectImageList.SetOnCopyImage(const Value: TOnProcessImageEvent);
begin
  FOnCopyImage := Value;
end;

////////////////

procedure TSelectImageList.SetOnLoadImage(const Value: TOnProcessImageEvent);
begin
  FOnLoadImage := Value;
end;

constructor TSelectImageList.Create;
begin
  inherited;
  JpegThumbnail := TJpegImage.Create;
end;

destructor TSelectImageList.Destroy;
begin
  ClearImages;
  FreeAndNil(JpegThumbnail);
  inherited;
end;

end.
