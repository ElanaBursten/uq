unit OdAckMessageDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Contnrs, OdVclUtils, OdMiscUtils;

type
  TAckMessageDialog = class(TForm)
    MessageMemo: TMemo;
    ButtonPanel: TPanel;
    procedure ButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FOkToClose: Boolean;
    FClickedButtonCaption: string;
    FButtons: TObjectList;
    procedure SizeFormForText(TextLength: Integer);
    procedure AddFormButtons(const Captions: string);
  public
    procedure SetupControls(const MessageText, ButtonCaptions, Title: string); virtual;
    property ClickedButtonCaption: string read FClickedButtonCaption;
  end;

function ShowMessageAckDialog(const MessageText, ButtonCaptions: string; const Title: string = ''): string;

implementation

{$R *.dfm}

const
  MaximumMessageDialogWidth = 640;
  MaximumMessageDialogHeight = 480;

function ShowMessageAckDialog(const MessageText, ButtonCaptions: string; const Title: string): string;
var
  Dialog: TAckMessageDialog;
begin
  Assert(Length(MessageText) > 0, 'Blank message text is not allowed');
  // TODO: Create a default button instead?
  Assert(Length(ButtonCaptions) > 0, 'Blank button captions are not allowed');

  Dialog := TAckMessageDialog.Create(nil);
  try
    Dialog.SetupControls(MessageText, ButtonCaptions, Title);
    Dialog.ShowModal;
    Result := Dialog.FClickedButtonCaption;
  finally
    FreeAndNil(Dialog);
  end;
end;

{ TAckMessageDialog }

procedure TAckMessageDialog.SetupControls(const MessageText, ButtonCaptions, Title: string);
begin
  SizeFormForText(Length(MessageText));
  MessageMemo.Clear;
  MessageMemo.Lines.Text := MessageText;
  AddMemoScrollbarsIfNecessary(MessageMemo);
  AddFormButtons(ButtonCaptions);
  if NotEmpty(Title) then
    Self.Caption := Title;
end;

procedure TAckMessageDialog.AddFormButtons(const Captions: string);
const
  ButtonWidth = 130;
  ButtonTop = 8;
  ButtonPad = 4;
var
  CaptionList: TStringList;
  I, ButtonNum: Integer;
  Button: TButton;
begin
  FButtons.Clear;
  CaptionList := TStringList.Create;
  try
    CaptionList.CommaText := Captions;
    ButtonNum := 1;
    // adds buttons from right to left
    for I := CaptionList.Count-1 downto 0 do begin
      Button := TButton.Create(nil);  // freed when FButtons is destroyed
      Button.Parent := ButtonPanel;
      Button.Caption := CaptionList.Strings[I];
      Button.Width := ButtonWidth;
      Button.Top := ButtonTop;
      Button.Left := ButtonPanel.ClientWidth - ((Button.Width + ButtonPad) * ButtonNum);
      Button.OnClick := ButtonClick;
      FButtons.Add(Button);
      Inc(ButtonNum);
    end;
  finally
    FreeAndNil(CaptionList);
  end;
end;

procedure TAckMessageDialog.SizeFormForText(TextLength: Integer);
begin
  Self.Width := MaximumMessageDialogWidth;
  Self.Height := MaximumMessageDialogHeight;

  // TODO: This should adjust the size of the dialog based on the text length.
end;

procedure TAckMessageDialog.FormCreate(Sender: TObject);
begin
  FButtons := TObjectList.Create(True);
  FOkToClose := False;
end;

procedure TAckMessageDialog.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FButtons);
end;

procedure TAckMessageDialog.ButtonClick(Sender: TObject);
begin
  FClickedButtonCaption := (Sender as TButton).Caption;
  FOkToClose := True;
  Close;
end;

procedure TAckMessageDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := FOkToClose;
end;

procedure TAckMessageDialog.FormShow(Sender: TObject);
begin
  // HACK to force the focused window in front of any others
  ForceForegroundWindow(Application.Handle);
end;

end.
