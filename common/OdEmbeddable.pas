unit OdEmbeddable;

{ Embeddable Form Utilities and Base Class
  Copyright 2014 Oasis Digital
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls, Dialogs,
  DBCtrls, DBGrids, cxEdit, cxGridTableView, Checklst, cxCheckListBox, cxDBExtLookupComboBox,
  cxCheckBox, Buttons;

const
  UM_ACTIVATED = WM_USER + 54;
  StateNoneStrValue = '<none>';
  StateNoneIntValue = -1;

type
  IStateSaver = Interface(IInterface)
    procedure SetValue(const Name, Value: string); overload;
    procedure SetValue(const Name: string; Value: Integer); overload;
    procedure SaveVisibility;
  end;

  IStateLoader = Interface(IInterface)
    function GetValue(const Name: string; const Default: string = ''): string; overload;
    function GetValue(const Name: string; Default: Integer = -1): Integer; overload;
  end;

  TEmbeddableForm = class(TForm)
  private
    FCanEdit: Boolean;
    FOnCaptionChanged: TNotifyEvent;
  protected
    procedure Loaded; override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMSetText(var Msg: TMessage); message WM_SETTEXT;
  public
    property OnCaptionChanged: TNotifyEvent read FOnCaptionChanged Write FOnCaptionChanged;
    procedure LeavingNow; virtual;
    procedure ActivatingNow; virtual;
    procedure WasActivated; virtual;
    procedure UMActivated(var Msg: TMessage); message UM_ACTIVATED;
    procedure RefreshNow; virtual;
    procedure OpenDataSets; virtual;
    procedure CloseDataSets; virtual;
    procedure LoadFormState(State: IStateLoader); virtual;
    procedure SaveFormState(State: IStateSaver); virtual;
    procedure PopulateControls; virtual;
    function RequireSync: Boolean; virtual;
    function IsModal: Boolean;
    procedure PopulateDropdowns; virtual;
    procedure EditNow; virtual;
    procedure LockNow; virtual;
    function IsReadOnly: Boolean; virtual;
    procedure SetReadOnly(const IsReadOnly: Boolean); virtual;
    procedure SetControlsReadOnly(const IsReadOnly: Boolean; Parent: TComponent); virtual;
    constructor Create(Owner: TComponent); override;
  end;

  TEmbeddableFormClass = class of TEmbeddableForm;


function CreateEmbeddedForm(FormClass: TEmbeddableFormClass; Owner: TComponent; Parent: TWinControl): TEmbeddableForm;

implementation

{$R *.DFM}

uses
  Terminology;

var
  HostControl: TWinControl;

function CreateEmbeddedForm(FormClass: TEmbeddableFormClass; Owner: TComponent; Parent: TWinControl): TEmbeddableForm;
begin
  Parent.HandleNeeded;
  HostControl := Parent;
  Result := FormClass.Create(Owner);
  Result.BorderStyle := bsNone;
  Result.Align := alClient;
end;

procedure TEmbeddableForm.ActivatingNow;
begin
  // When the main form activates an embeddable form, this is called
  // Here you can execute a refresh, do a sync, etc.
  // This event occurs even if the embeddable form is already visible,
  // so it isn't the same as the OnFormShow event
  // Use SetReadOnly in descendant forms to control state
end;

procedure TEmbeddableForm.CloseDataSets;
begin
  // Close any open datasets
end;

procedure TEmbeddableForm.CreateParams(var Params : TCreateParams);
begin
  inherited CreateParams(Params);

  if HostControl <> nil then begin
    with Params do begin
      Style := (Style OR WS_CHILD) AND (NOT WS_POPUP);
      WndParent := HostControl.Handle;
    end;
    Parent := HostControl;
    HostControl := nil;  // done with the temporary
  end;
end;

function TEmbeddableForm.IsModal: Boolean;
begin
  Result := Visible and (fsModal in FormState);
end;

procedure TEmbeddableForm.LeavingNow;
begin
  // This is a hook for forms to save changes etc. when they are left.
  // They can raise an exception here or Abort to stop this from happening.
  // By default, do nothing
end;

procedure TEmbeddableForm.SaveFormState(State: IStateSaver);
begin
  // Save state data to State
end;

procedure TEmbeddableForm.LoadFormState(State: IStateLoader);
begin
  // Load state data from State
end;

procedure TEmbeddableForm.OpenDataSets;
begin
  // Open any datasets necessary to make the form useful
  PopulateDropdowns;
end;

procedure TEmbeddableForm.PopulateDropdowns;
begin
  // Override to populate dropdowns and related things
end;

procedure TEmbeddableForm.RefreshNow;
begin
  // Refresh datasets, repopulate dropdowns and listboxes, etc.
  CloseDataSets;
  OpenDataSets;
  PopulateControls;
end;

function TEmbeddableForm.RequireSync: Boolean;
begin
  // Override this if the form can be shown while
  // there is no local data that has been synced
  // TODO: Move this to a descendent TQMEmbeddableForm eventually?
  Result := True;
end;

procedure TEmbeddableForm.UMActivated(var Msg: TMessage);
begin
  WasActivated;
end;

procedure TEmbeddableForm.WasActivated;
begin
  // Do stuff here after the message loop drains
end;

procedure TEmbeddableForm.WMSetText(var Msg: TMessage);
begin
  inherited;
  if Assigned(FOnCaptionChanged) then
    FOnCaptionChanged(Self);
end;

procedure TEmbeddableForm.PopulateControls;
begin
  // Populate any dropdown controls here
  // This fires after creation and then for each refresh
end;

constructor TEmbeddableForm.Create(Owner: TComponent);
begin
  inherited;
  PopulateControls;
end;

procedure TEmbeddableForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

procedure TEmbeddableForm.EditNow;
begin
  // Descendants implement code to unlock the form for editing
  SetReadOnly(False);
end;

procedure TEmbeddableForm.LockNow;
begin
  SetReadOnly(True);
end;

procedure TEmbeddableForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  // Note that this does not iterate controls owned by other components.
  SetControlsReadOnly(IsReadOnly, Self);
end;

procedure TEmbeddableForm.SetControlsReadOnly(const IsReadOnly: Boolean; Parent: TComponent);
var
  i: Integer;
  AGridView: TcxGridTableView;
  Comp: TComponent;
begin
  FCanEdit := not IsReadOnly;
  for i := 0 to Parent.ComponentCount - 1 do begin
    Comp := Parent.Components[i];
    if Comp is TButton then
      (Comp as TButton).Enabled := not IsReadOnly
    else if Comp is TBitBtn then
      (Comp as TBitBtn).Enabled := not IsReadOnly
    else if Comp is TDBEdit then
      (Comp as TDBEdit).ReadOnly := IsReadOnly
    else if Comp is TEdit then
      (Comp as TEdit).ReadOnly := IsReadOnly
    else if Comp is TDBGrid then
      (Comp as TDBGrid).ReadOnly := IsReadOnly
    else if Comp is TDBMemo then
      (Comp as TDBMemo).ReadOnly := IsReadOnly
    else if Comp is TCheckListBox then
      (Comp as TCheckListBox).Enabled := not IsReadOnly
    else if Comp is TCheckBox then
      (Comp as TCheckBox).Enabled := not IsReadOnly
    {
      TODO: Handling the DevEx controls below adds here an extra dependency
      for every application that uses OdEmbeddable. Consider moving that
      code somewhere else.
    }
    else if Comp is TcxCheckListBox then
      (Comp as TcxCheckListBox).ReadOnly := IsReadOnly
    else if Comp is TcxGridTableView then begin
      AGridView := Comp as TcxGridTableView;
      AGridView.OptionsData.Appending := not IsReadOnly;
      AGridView.OptionsData.Inserting := not IsReadOnly;
      AGridView.OptionsData.Deleting := not IsReadOnly;
      AGridView.OptionsData.Editing := not IsReadOnly;
    end
    else if Comp is TcxDBExtLookupComboBox then
      (Comp as TcxDBExtLookupComboBox).DataBinding.Field.ReadOnly := IsReadOnly
    else if Comp is TcxCheckBox then
      (Comp as TcxCheckBox).Properties.ReadOnly := IsReadOnly
    else if Comp is TcxCustomEdit then
      (Comp as TcxCustomEdit).Enabled := not IsReadOnly
    else if Comp is TDBCheckBox then
      (Comp as TDBCheckBox).ReadOnly := IsReadOnly
    else if Comp is TDBComboBox then begin
      (Comp as TDBComboBox).ReadOnly := IsReadOnly;
      (Comp as TDBComboBox).Enabled := not IsReadOnly;
    end
    else if Comp is TDBLookupComboBox then begin
      (Comp as TDBLookupComboBox).DataSource.DataSet.FieldByName(
        (Comp as TDBLookupComboBox).DataField).ReadOnly := IsReadOnly;
    end;
  end;
end;

function TEmbeddableForm.IsReadOnly: Boolean;
begin
  Result := not FCanEdit;
end;

end.
