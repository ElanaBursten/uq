unit xqFadeBox;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;
{This is something I made while in Memphi so that the messages would fade away without
the truck drivers having to touch the screen.  It works well.  I did have an image popping
up and that is easy to add but for our purposes, it is just text.   SR}
type
  TFadeType = (ftIn, ftOut);
  TFadeBox = class(TForm)
    Panel1: TPanel;
    fadeTimer: TTimer;
    lblHeSays: TLabel;
    procedure fadeTimerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private

    fFadeType: TFadeType;
    function GetHeSays: String;
    procedure SetHeSays(const Value: String);
    property HeSays   : String Read GetHeSays write SetHeSays;
    property FadeType : TFadeType read fFadeType write fFadeType;
  public
    class function Execute(Statement:String) : TModalResult;
  end;

implementation
{$R *.dfm}

class function TFadeBox.Execute(Statement:String): TModalResult;
begin
  with TFadeBox.Create(nil) do
  begin
    HeSays := Statement;
    try
      result := ShowModal;
    finally
      Release;
    end;
  end;
end;

procedure TFadeBox.fadeTimerTimer(Sender: TObject);
const
  FADE_IN_SPEED = 1;
  FADE_OUT_SPEED = 2;
var
  newBlendValue : integer;
begin
  case FadeType of
    ftIn:
      begin
//        Image2.Visible := False;
//        Image1.Visible := True;
        if AlphaBlendValue < 65 then
          AlphaBlendValue := FADE_IN_SPEED + AlphaBlendValue
        else
        Begin
          fFadeType := ftOut;
        End;
      end;
    ftOut:
      begin
//        Image2.Visible := True;
//        Image1.Visible := False;
//        HeSays := 'I''m fading.  I''m fading.';
        if AlphaBlendValue > 0 then
        begin
          newBlendValue := -1 * FADE_OUT_SPEED + AlphaBlendValue;
          if newBlendValue >  0 then
            AlphaBlendValue := newBlendValue
          else
            AlphaBlendValue := 0;
        end
        else
        begin
          fadeTimer.Enabled := false;
          Close;
        end;
      end;
  end;
end;

procedure TFadeBox.FormCreate(Sender: TObject);
begin
  AlphaBlend := true;
  AlphaBlendValue := 0;
  fFadeType := ftIn;
  fadeTimer.Enabled := true;
end;

function TFadeBox.GetHeSays: String;
begin
  result:= lblHeSays.Caption;
end;

procedure TFadeBox.SetHeSays(const Value: String);
begin
  lblHeSays.Caption := Value;
end;

procedure TFadeBox.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if FadeType = ftIn then
  begin
    fFadeType := ftOut;
    AlphaBlendValue := 255;
    fadeTimer.Enabled := true;
    CanClose := false;
  end
  else
  begin
    CanClose := true;
  end;
end;

end.

