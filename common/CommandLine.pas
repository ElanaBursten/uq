unit CommandLine;

interface

uses
  SysUtils, OdMiscUtils;

function HelpSwitch(HelpIfNoParams: Boolean=False): Boolean;
function ParamIndexIfNext(const Option: string; var NextParamIndex: Integer):
  Integer;
function ParamStrIfNext(const Option: string; var NextParamIndex: Integer):
  string;
function TryParamStrToIDInt(ParamIndex: Integer; out IDInt: Integer): Boolean;

implementation

function HelpSwitch(HelpIfNoParams: Boolean=False): Boolean;
begin
  if (HelpIfNoParams and (ParamCount = 0)) or (ParamStr(1) = '/?') then
    Result := True
  else
    Result := False;
end;

function ParamIndexIfNext(const Option: string; var NextParamIndex: Integer):
  Integer;
begin
  if LowerCase(ParamStr(NextParamIndex)) = LowerCase(Option) then begin
    Result := NextParamIndex + 1;
    Inc(NextParamIndex, 2);
  end else
    Result := -1;
end;

function ParamStrIfNext(const Option: string; var NextParamIndex: Integer):
  string;
begin
  Result := ParamStr(ParamIndexIfNext(Option, NextParamIndex));
end;

function TryParamStrToIDInt(ParamIndex: Integer; out IDInt: Integer): Boolean;
begin
  if TryStrToInt(ParamStr(ParamIndex), IDInt) and
    StrConsistsOf(ParamStr(ParamIndex), ['0'..'9']) then
    Result := True
  else
    Result := False;
end;

end.
