unit OdBrowserUtils;

{ Browser Utilities
  Copyright 2014 Oasis Digital

  Note - this file used to have code regarding other browser components,
  but the IE work so well that I mostly use it now.  -Kyle
}


interface

uses
  SHDocVw, Classes, ActiveX, MSHTML, UExternalContainer, SysUtils;
var
    HTMLWindow2: IHTMLWindow2;
    fContainer : TExternalContainer;
procedure LoadHTML(Browser: TWebBrowser; HtmlStr: string);    //QMANTWO-175
function ParseLink(HTMLLink: string): string;  //QM-1008 HTML LInk


implementation

procedure LoadHTML(Browser: TWebBrowser; HtmlStr: string);

var
  aStream : TMemoryStream;
begin
  if not assigned(Browser.Document) then
  Browser.navigate('about:blank');

    if Assigned(Browser.Document) then
    begin
      aStream := TMemoryStream.Create;
      try
         aStream.WriteBuffer(Pointer(HtmlStr)^, Length(HtmlStr));
         aStream.Seek(0, soFromBeginning);
         (Browser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(aStream));
      finally
         aStream.Free;
      end;
      HTMLWindow2 := (Browser.Document as IHTMLDocument2).parentWindow;
    end;
    fContainer := TExternalContainer.Create(Browser);
end;

function ParseLink(HTMLLink: string): string;  //QM-1008 HTML LInk
const
  HREF = '<a href="';
  ENDHREF = '">';
var
  StartPos, EndPos: integer;
  offset: integer;
begin
  offset := length(HREF);
  StartPos := ansipos(HREF, HTMLLink);
  EndPos := ansipos(ENDHREF, HTMLLink);

  Result := Copy(HTMLLink, StartPos + offset, EndPos - offset);

end;





end.
