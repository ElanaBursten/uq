unit BreakRulesTest;

{ Notes from Kyle:

  * We need many more test cases, to assert that the system gets the right
    answer for various conditions... and to state what the right answer is.

  * I made the expression of each test case more terse than before.

  * Naming the test cases can be hard; we may not really care about the names.

  * I started adding the most basic test cases, such as an empty day,
    and found bugs already.
}

interface

uses
  TestFrameWork, Classes, SysUtils, BreakRules, Controls, Dialogs;

type
  // Base test harness, infrastructure for testing
  TBaseBreakRulesTest = class(TTestCase)
  private
    Rule: TBreakRulesChecker;
    procedure Add(Start, Stop: string);
  public
    procedure TearDown; override;
  published
    procedure TestEmpty;
  end;

  // Federal tests
  TFederalBreakRulesTest = class(TBaseBreakRulesTest)
  public
    procedure SetUp; override;
  published
    procedure TestFederalRule;
  end;

  // Tests common to EVERY and AFTER
  TCommonBreakRulesTest = class(TBaseBreakRulesTest)
  published
    procedure TestOneShortWork;
    procedure TestOneLongWork;
    procedure TestFragmentedDay1;
    procedure TestFragmentedDay2;
    procedure TestFragmentedDay3;
    procedure TestFragmentedDay3OutOfOrder;
    procedure TestRuleCheckerOk;
    procedure TestRuleCheckerNotOk;
  end;

  TEveryBreakRulesTest = class(TCommonBreakRulesTest)
  public
    procedure SetUp; override;
  published
    procedure TestLongDayFailsWith1Break;
    procedure TestLongDayOKWith2BreaksA;
    procedure TestLongDayOKWith2BreaksAOutOfOrder;
    procedure TestLongDayOKWith2BreaksB;
  end;

  TAfterBreakRulesTest = class(TCommonBreakRulesTest)
  public
    procedure SetUp; override;
  published
    procedure TestLongDayNeedsOnly1Break;
  end;

implementation

{ TBaseBreakRulesTest }

procedure TBaseBreakRulesTest.Add(Start, Stop: string);
begin
  Rule.Add(StrToTime(Start + ':00'), StrToTime(Stop + ':00'));
end;

procedure TBaseBreakRulesTest.TearDown;
begin
  FreeAndNil(Rule);
  inherited;
end;

procedure TBaseBreakRulesTest.TestEmpty;
begin
  CheckTrue(Rule.Check);
end;

{ TFederalBreakRulesTest }

procedure TFederalBreakRulesTest.SetUp;
begin
  inherited;
  Rule := CreateBreakRuleChecker('FED', 5, 10);
end;

procedure TFederalBreakRulesTest.TestFederalRule;
begin
  Add('00:00', '23:59');  // Working all day with no breaks
  CheckTrue(Rule.Check);  // ... is OK under federal law!
end;

{ TCommonBreakRulesTest }

procedure TCommonBreakRulesTest.TestOneShortWork;
begin
  Add('09:00', '12:00');
  CheckTrue(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestOneLongWork;
begin
  Add('09:00', '16:00');
  CheckFalse(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestFragmentedDay1;
begin
  Add('09:00', '10:00');
  Add('10:01', '11:01');
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('13:04', '14:04');
  CheckTrue(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestFragmentedDay2;
begin
  Add('09:00', '10:00');
  Add('10:01', '11:01');
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('13:04', '14:04');
  Add('14:05', '15:08');  // work too long with no break
  CheckFalse(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestFragmentedDay3;
begin
  Add('09:00', '10:00');
  Add('10:01', '11:01');
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('13:04', '14:04');
  Add('16:05', '17:08');  // work too long, but with a break
  CheckTrue(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestFragmentedDay3OutOfOrder;
begin
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('10:01', '11:01');
  Add('13:04', '14:04');
  Add('16:05', '17:08');
  Add('09:00', '10:00');
  CheckTrue(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestRuleCheckerNotOk;
begin
  Add('09:00', '12:20');
  Add('12:30', '15:30');    // over 6 hours, break is too short
  CheckFalse(Rule.Check);
end;

procedure TCommonBreakRulesTest.TestRuleCheckerOk;
begin
  Add('09:00', '12:00');
  Add('12:30', '15:30');    // 6 hours, break is long enough
  CheckTrue(Rule.Check);
end;

// *************  tests below here show the differences between EVERY and AFTER

{ TEveryBreakRulesTest }

procedure TEveryBreakRulesTest.SetUp;
begin
  inherited;
  Rule := CreateBreakRuleChecker('EVERY', 30, 6 * 60);
end;

procedure TEveryBreakRulesTest.TestLongDayFailsWith1Break;
begin
  Add('05:00', '10:00');  // 5 hrs, 1 hour break
  Add('11:00', '15:00');  // 4 more hours, total of 9, short gap
  Add('15:20', '19:20');  // 4 more hours, total of 13, too long for no break
  CheckFalse(Rule.Check);
end;

procedure TEveryBreakRulesTest.TestLongDayOKWith2BreaksA;
begin
  Add('05:00', '10:00');  // 5 hrs, 1 hour break
  Add('11:00', '15:00');  // 4 more hours, total of 9, short gap
  Add('15:30', '19:30');  // 4 more hours, total of 13, too long for no break
  CheckTrue(Rule.Check);
end;

procedure TEveryBreakRulesTest.TestLongDayOKWith2BreaksAOutOfOrder;
begin
  Add('11:00', '15:00');  // 4 more hours, total of 9, short gap
  Add('05:00', '10:00');  // 5 hrs, 1 hour break
  Add('15:30', '19:30');  // 4 more hours, total of 13, too long for no break
  CheckTrue(Rule.Check);
end;

procedure TEveryBreakRulesTest.TestLongDayOKWith2BreaksB;
begin
  Add('09:00', '10:00');
  Add('10:01', '11:01');
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('13:04', '14:04');
  Add('16:05', '17:08');  // work too long, with a break
  Add('18:05', '22:08');  // then a bunch more that day, then a break
  Add('23:00', '23:59');  // then a bunch more that day, then a break
  CheckTrue(Rule.Check);
end;

{ TAfterBreakRulesTest }

procedure TAfterBreakRulesTest.SetUp;
begin
  inherited;
   Rule := CreateBreakRuleChecker('AFTER', 30, 6 * 60);
end;

procedure TAfterBreakRulesTest.TestLongDayNeedsOnly1Break;
begin
  Add('09:00', '10:00');
  Add('10:01', '11:01');
  Add('11:02', '12:02');
  Add('12:03', '13:03');
  Add('13:04', '14:04');
  Add('16:05', '17:08');  // work too long, with a break
  Add('18:05', '23:08');  // then a bunch more that day, no break
  CheckTrue(Rule.Check);
end;

initialization
  TestFramework.RegisterTest(TFederalBreakRulesTest.Suite);
  TestFramework.RegisterTest(TEveryBreakRulesTest.Suite);
  TestFramework.RegisterTest(TAfterBreakRulesTest.Suite);

end.
