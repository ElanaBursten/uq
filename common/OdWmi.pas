unit OdWmi;

interface

uses SysUtils, Classes, Windows, WbemScripting_TLB, DateUtils;

type
  TWbemObjectArray = array of ISWbemObject;
  TWbemPropertyArray = array of ISWbemProperty;

  TWmiInfo = class(TObject)
  private
    Locator: ISWbemLocator;
    Services: ISWbemServices;
    FWmiInstalled: Boolean;
    FConnected: Boolean;
    procedure GetInstancesFromObjectSet(ObjectSet: ISWbemObjectSet; var Instances: TWbemObjectArray);
    procedure GetInstancesOfName(const ObjectName: string; var Instances: TWbemObjectArray);
    function GetPropertryValueOfName(WObject: ISWbemObject; const PropName: string; var Value: OleVariant): Boolean;
    function GetSingleStringFromQuery(const WQLQuery, PropertyName: string; var ResultString: string): Boolean;
    function GetMaxStringFromQuery(const WQLQuery, PropertyName: string;
      var ResultString: string): Boolean;
  public
    property WmiInstalled: Boolean read FWmiInstalled write FWmiInstalled;
    property Connected: Boolean read FConnected write FConnected;
    constructor Create(const AServer: string = ''; const ANameSpace: string = ''; const AUserName: string = ''; const APassword: string = '');
    destructor Destroy; override;
    function GetDellServiceTag: string;
    function GetAssetTag: string;
    function GetInstalledHotFixList: string; overload;
    procedure GetInstalledHotFixList(List: TStringList); overload;
    function GetLastBootTime: TDateTime;
    function GetLastShutdownTime(ShutdownAfterDateTime: TDateTime = 0): TDateTime;
  end;

function GetDellServiceTag: string;
function GetInstalledHotFixList: string;
function GetAssetTag: string;
function GetLastBootTime: TDateTime;
function GetLastShutdownTime(ShutdownAfterDateTime: TDateTime=0): TDateTime;
function GetWMIResult(wmiHost, wmiClass, wmiProperty: string): string;
function GetLocalComputerSerial: string;
function GetLocalComputerModel: string;

implementation

uses JclSysInfo, ComObj, ActiveX, OdUtcDates, OdMiscUtils, Variants;

function GetDellServiceTag: string;
var
  Wmi: TWmiInfo;
begin
  Result := '';
  try
    Wmi := TWmiInfo.Create;
    try
      Result := Wmi.GetDellServiceTag;
    finally
      FreeAndNil(Wmi);
    end;
  except
  end;
end;

function GetInstalledHotFixList: string;
var
  Wmi: TWmiInfo;
begin
  Result := '';
  try
    Wmi := TWmiInfo.Create;
    try
      Result := Wmi.GetInstalledHotFixList;
    finally
      FreeAndNil(Wmi);
    end;
  except
  end;
end;

function GetLastBootTime: TDateTime;
var
  Wmi: TWmiInfo;
begin
  Result := 0;
  try
    Wmi := TWmiInfo.Create;
    try
      Result := Wmi.GetLastBootTime;
    finally
      FreeAndNil(Wmi);
    end;
  except
  end;
end;

function GetAssetTag: string;
var
  Wmi: TWmiInfo;
begin
  Result := '';
  try
    Wmi := TWmiInfo.Create;
    try
      Result := Wmi.GetAssetTag;
    finally
      FreeAndNil(Wmi);
    end;
  except
  end;
end;


function GetLastShutdownTime(ShutdownAfterDateTime: TDateTime): TDateTime;
var
  Wmi: TWmiInfo;
begin
  Result := 0;
  try
    Wmi := TWmiInfo.Create;
    try
      Result := Wmi.GetLastShutdownTime(ShutdownAfterDateTime);
    finally
      FreeAndNil(Wmi);
    end;
  except
  end;
end;

//TODO: This should use TWmiInfo to do the heavy lifting
function GetWMIResult(wmiHost, wmiClass, wmiProperty: string): string;
var
// Windows Management Instrumentation - WMI - Required for the WMI querying process
  WMILocator:  ISWbemLocator;
  WMIServices: ISWbemServices;
  WMIObject:   ISWbemObject;
  WMIObjSet:   ISWbemObjectSet;
  WMIProp:     ISWbemProperty;
  Enum:        IEnumVariant;
  Value:       Cardinal;
  TempObj:     OleVariant;
  ResultStr:   string;
begin
  try
  WMILocator := CoSWbemLocator.Create;
  WMIServices := WMILocator.ConnectServer(wmiHost, 'root\cimv2', '', '', '','', 0, nil);
  WMIObjSet := WMIServices.ExecQuery('SELECT * FROM '+wmiClass, 'WQL',
               wbemFlagReturnImmediately and wbemFlagForwardOnly , nil);
  Enum := (WMIObjSet._NewEnum) as IEnumVariant;
  while (Enum.Next(1, TempObj, Value) = S_OK) do
  begin
    WMIObject := IUnknown(tempObj) as ISWBemObject;
    WMIProp := WMIObject.Properties_.Item(wmiProperty, 0);
    if VarIsNull(WMIProp.Get_Value) then
      Result := ''
    else
    begin
      ResultStr := WMIProp.Get_Value;
      Result :=  ResultStr;
    end;
  end;
  except
    // Trap exceptions (not having WMI installed will cause an exception)
    Result := '';
  end;
end;

function GetLocalComputerSerial: string;
begin
  Result := GetWMIResult('', 'Win32_BIOS', 'SerialNumber');
end;

function GetLocalComputerModel: string;
begin
  Result := GetWMIResult('', 'Win32_ComputerSystem', 'Model');
end;

{ TWmiInfo }

constructor TWmiInfo.Create(const AServer, ANamespace, AUserName, APassword: string);
var
  Namespace: string;
  Server: string;
begin
  inherited Create;
  WmiInstalled := False;
  Connected := False;
  Locator := CoSWbemLocator.Create;
  WmiInstalled := True;
  if ANameSpace = '' then
    Namespace := 'root\cimv2'
  else
    Namespace := ANameSpace;
  if AServer = '' then
    Server := '.'
  else
    Server := AServer;
  Services := Locator.ConnectServer(Server, Namespace, AUserName, APassword, '', '', 0, Services);
  Connected := True;
end;

destructor TWmiInfo.Destroy;
begin
  Connected := False;
  Locator := nil;
  Services := nil;
  inherited;
end;

function TWmiInfo.GetAssetTag: string;
var
  Instances: TWbemObjectArray;
  TagValue: OleVariant;
begin
  Result := 'Field Not Present';
  GetInstancesOfName('Win32_systemEnclosure', Instances);
  if Length(Instances) > 0 then
    if GetPropertryValueOfName(Instances[0], 'SMBIOSAssetTag', TagValue) then
      Result := TagValue;
end;

function TWmiInfo.GetDellServiceTag: string;
var
  Instances: TWbemObjectArray;
  TagValue: OleVariant;
begin
  Result := '';
  GetInstancesOfName('Win32_systemEnclosure', Instances);
  if Length(Instances) > 0 then
    if GetPropertryValueOfName(Instances[0], 'SerialNumber', TagValue) then
      Result := TagValue;
end;

function TWmiInfo.GetInstalledHotFixList: string;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetInstalledHotFixList(List);
    Result := List.CommaText;
  finally
    FreeAndNil(List);
  end;
end;

procedure TWmiInfo.GetInstalledHotFixList(List: TStringList);
var
  i: Integer;
  Instances: TWbemObjectArray;
  HotFixValue: OleVariant;
  HotFixID: string;
begin
  Assert(Assigned(List));
  List.Clear;
  // W2K before SP3 can crash here.  Don't even try to return anything:
  // http://support.microsoft.com/?kbid=279225
  if (GetWindowsVersion = wvWin2000) and (GetWindowsServicePackVersion < 3) then
    Exit;
  List.Sorted := True;
  List.Duplicates := dupIgnore;
  GetInstancesOfName('Win32_QuickFixEngineering', Instances);
  for i := 0 to Length(Instances) - 1 do begin
    if GetPropertryValueOfName(Instances[i], 'HotFixID', HotFixValue) then begin
      HotFixID := HotFixValue;
      if (Trim(HotFixID) <> '') and (HotFixID <> 'File 1') then
        List.Add(HotFixID);
    end;
  end;
end;

function TWmiInfo.GetLastBootTime: TDateTime;
const
  WQLSelect = 'SELECT LastBootUpTime FROM Win32_OperatingSystem';
var
  RawDate: string;
  UtcOffset: Integer;
begin
  Result := 0;
  if GetSingleStringFromQuery(WQLSelect, 'LastBootUpTime', RawDate) then begin
    Result := UtcStrToDateTimeDef(RawDate, UtcOffset);
    // Under Win2K, all Win32_OperatingSystem dates require conversion using UTCOffset
    if (GetWindowsVersion = wvWin2000) then
      Result := IncMinute(Result, (-1 * UtcOffset));
  end;
end;

function TWmiInfo.GetLastShutdownTime(ShutdownAfterDateTime: TDateTime): TDateTime;
const
  WQLSelect = 'SELECT TimeGenerated FROM Win32_NTLogEvent ' +
    'WHERE Logfile=''System'' AND SourceName=''EventLog'' AND EventCode=6006';
var
  RawDate: string;
  UtcOffset: Integer;
  WQL: string;
  ShutdownStoredAsUTC: Boolean;
begin
  Result := 0;
  ShutdownStoredAsUTC := GetWindowsVersion >= wvWinVista;

  WQL := WQLSelect;
  if ShutdownAfterDateTime > 0 then
    if ShutdownStoredAsUTC then
      WQL := WQL + Format(' AND TimeGenerated>''%s''', [UtcDateTimeToStr(
        LocalDateTimeToUTCDateTime(ShutdownAfterDateTime), 0)])
    else
      WQL := WQL + Format(' AND TimeGenerated>''%s''', [UtcDateTimeToStr(
        ShutdownAfterDateTime, (-1*GetUTCBias))]);

  if GetMaxStringFromQuery(WQL, 'TimeGenerated', RawDate) then begin
    Result := UtcStrToDateTimeDef(RawDate, UtcOffset);
    if ShutdownStoredAsUTC then begin
      Result := UTCDateTimeToLocalDateTime(Result);
      { There is a repeated time interval (typically 1 hour) after 'falling
        back' at the end of daylight savings time. And we don't record whether a
        local time is standard or daylight. So this comparison may not be
        accurate if both times are in that repeated time interval. }
      if Result <= ShutdownAfterDateTime then
        Result := 0;
    end;
  end;
end;

function TWmiInfo.GetSingleStringFromQuery(const WQLQuery, PropertyName: string; var ResultString: string): Boolean;
var
  WmiObjectSet: ISWbemObjectSet;
  WmiObject: ISWbemObject;
  WmiProperty: ISWbemProperty;
  Temp: OleVariant;
  NumReturned: Cardinal;
  Enum: IEnumVariant;
begin
  ResultString := '';
  WmiObjectSet := Services.ExecQuery(WQLQuery, 'WQL', wbemFlagReturnImmediately, nil);
  Result := WmiObjectSet.Count = 1;
  Assert(Result, 'Expected a single result object');

  Enum := (WmiObjectSet._NewEnum) as IEnumVariant;
  while (Enum.Next(1, Temp, NumReturned) = S_OK) do begin
    WmiObject := IUnknown(Temp) as SWBemObject;
    WmiProperty := WmiObject.Properties_.Item(PropertyName, 0);
    Assert(WmiProperty.Name = PropertyName,
      'Expected property ' + PropertyName + '; got property ' + WmiProperty.Name);

    ResultString := WmiProperty.Get_Value;
    if ResultString <> 'NULL' then
      Result := True;
  end;
end;

function TWmiInfo.GetMaxStringFromQuery(const WQLQuery, PropertyName: string; var ResultString: string): Boolean;
var
  WmiObjectSet: ISWbemObjectSet;
  WmiObject: ISWbemObject;
  WmiProperty: ISWbemProperty;
  Temp: OleVariant;
  NumReturned: Cardinal;
  Enum: IEnumVariant;
  ThisValue: string;
begin
  ResultString := '';
  WmiObjectSet := Services.ExecQuery(WQLQuery, 'WQL', wbemFlagReturnImmediately and wbemFlagForwardOnly, nil);
  Result := WmiObjectSet.Count > 0;
  if not Result then
    Exit;
  Enum := (WmiObjectSet._NewEnum) as IEnumVariant;
  while (Enum.Next(1, Temp, NumReturned) = S_OK) do begin
    WmiObject := IUnknown(Temp) as SWBemObject;
    WmiProperty := WmiObject.Properties_.Item(PropertyName, 0);
    Assert(WmiProperty.Name = PropertyName,
      'Expected property ' + PropertyName + '; got property ' + WmiProperty.Name);
    ThisValue := WmiProperty.Get_Value;
    if ThisValue > ResultString then
      ResultString := ThisValue;
  end;
  Result := True;
end;

procedure TWmiInfo.GetInstancesFromObjectSet(ObjectSet: ISWbemObjectSet; var Instances: TWbemObjectArray);
var
  i: Integer;
  Enum: IEnumVariant;
  Temp: OleVariant;
  NumReturned: Cardinal;
begin
  SetLength(Instances, ObjectSet.Count);
  for i := 0 to ObjectSet.Count - 1 do
    Instances[i] := nil;
  Enum := ObjectSet._NewEnum as IEnumVariant;
  i := 0;
  while (Enum.Next(1, Temp, NumReturned) = S_OK) do
  begin
    Instances[i] := IUnknown(Temp) as ISWbemObject;
    Inc(i);
  end;
end;

procedure TWmiInfo.GetInstancesOfName(const ObjectName: string; var Instances: TWbemObjectArray);
var
  ObjectSet: ISWbemObjectSet;
begin
  SetLength(Instances, 0);
  ObjectSet := Services.InstancesOf(ObjectName, 0, ObjectSet);
  if ObjectSet.Count > 0 then
    GetInstancesFromObjectSet(ObjectSet, Instances);
end;

function TWmiInfo.GetPropertryValueOfName(WObject: ISWbemObject;
  const PropName: string; var Value: OleVariant): Boolean;
var
  Properties: ISWbemPropertySet;
  Prop: ISWbemProperty;
  Enum: IEnumVariant;
  Temp: OleVariant;
  NumReturned: Cardinal;
begin
  Assert(Assigned(WObject));
  Result := False;
  Value := '';
  if PropName <> '' then begin
    Properties := WObject.Properties_;
    if Assigned(Properties) then begin
      Enum := Properties._NewEnum as IEnumVariant;
      if Assigned(Enum) then begin
        while (Enum.Next(1, Temp, NumReturned) = S_OK) do
        begin
          Prop := IUnknown(Temp) as ISWbemProperty;
          if Prop.Name = PropName then begin
            Value := Prop.Get_Value;
            Result := True;
            Break;
          end;
        end;
      end;
    end;
  end;
end;

end.
