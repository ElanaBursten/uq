//*********************************************************
//*                   OdContainer.pas                     *
//*          Copyright (c) 2014 by Oasis Digital          *
//*                All rights reserved.                   *
//*********************************************************

unit OdContainer;

interface

uses
  Classes;

type
  TIntegerList = class(TList)
  protected
    // Calling some TList methods directly might result in FSorted not being set right
    FSorted: Boolean;
    function GetItem(Index: Integer): Integer;
    procedure SetItem(Index: Integer; Value: Integer);
  public
    function IndexOf(Value: Integer): Integer;
    function SortedIndexOf(Value: Integer; var Index: Integer): Boolean; overload;
    function SortedIndexOf(Value: Integer): Integer; overload;
    function Add(Value: Integer): Integer;
    function Has(Value: Integer): Boolean;
    procedure Clear; override;
    // Adds Value if it isn't already in the list.  Returns True if added.
    function AddIfNeeded(Value: Integer): Boolean;
    property Items[Index: Integer]: Integer read GetItem write SetItem;
    procedure Sort;
  end;


//============================================================================//

implementation

uses SysUtils, JclSysUtils;

{ TIntegerList }

function IntegerCompare(Item1, Item2: Pointer): Integer;
begin
  if (LongInt(Item1)<LongInt(Item2)) then
    Result := -1
  else if (LongInt(Item1)>LongInt(Item2)) then
    Result := 1
  else
    Result := 0;
end;

function TIntegerList.GetItem(Index: Integer): Integer;
begin
  Result := Integer(inherited Items[Index]);
end;

procedure TIntegerList.SetItem(Index: Integer; Value: Integer);
begin
  inherited Items[Index] := Pointer(Value);
  FSorted := False;
end;

function TIntegerList.IndexOf(Value: Integer): Integer;
begin
  Result := inherited IndexOf(Pointer(Value));
end;

function TIntegerList.Add(Value: Integer): Integer;
begin
  Result := inherited Add(Pointer(Value));
  FSorted := False;
end;

function TIntegerList.AddIfNeeded(Value: Integer): Boolean;
begin
  Result := not Has(Value);
  if Result then
    Add(Value);
end;

procedure TIntegerList.Clear;
begin
  inherited;
end;

function TIntegerList.Has(Value: Integer): Boolean;
var
  Index: Integer;
begin
  if FSorted then
    Result := SortedIndexOf(Value, Index)
  else
    Result := IndexOf(Value) > -1;
end;

procedure TIntegerList.Sort;
begin
  inherited Sort(IntegerCompare);
  FSorted := True;
end;

function TIntegerList.SortedIndexOf(Value: Integer; var Index: Integer): Boolean;
begin
  Index := SearchSortedList(Self, IntegerCompare, Pointer(Value), False);
  Result := Index > -1;
end;

function TIntegerList.SortedIndexOf(Value: Integer): Integer;
begin
  Result := SearchSortedList(Self, IntegerCompare, Pointer(Value), False);
end;

end.
