unit OdDeviceNotification;

interface

uses
  Windows, Types, SysUtils, Forms;

const
   {$EXTERNALSYM DBT_DEVTYP_OEM}
   DBT_DEVTYP_OEM = $00000000;                  { oem-defined device type }
   {$EXTERNALSYM DBT_DEVTYP_DEVNODE}
   DBT_DEVTYP_DEVNODE = $00000001;              { devnode number }
   {$EXTERNALSYM DBT_DEVTYP_VOLUME}
   DBT_DEVTYP_VOLUME = $00000002;               { logical volume }
   {$EXTERNALSYM DBT_DEVTYP_PORT}
   DBT_DEVTYP_PORT = $00000003;                 { serial, parallel }
   {$EXTERNALSYM DBT_DEVTYP_NET}
   DBT_DEVTYP_NET = $00000004;                  { network resource }
   {$EXTERNALSYM DBT_DEVTYP_DEVICEINTERFACE}
   DBT_DEVTYP_DEVICEINTERFACE = $00000005;      { device interface class }
   {$EXTERNALSYM DBT_DEVTYP_HANDLE}
   DBT_DEVTYP_HANDLE = $00000006;               { file system handle }
   {$EXTERNALSYM DBT_DEVNODES_CHANGED}
   DBT_DEVNODES_CHANGED = $0007;
   {$EXTERNALSYM DBT_QUERYCHANGECONFIG}
   DBT_QUERYCHANGECONFIG = $0017;
   {$EXTERNALSYM DBT_CONFIGCHANGED}
   DBT_CONFIGCHANGED = $0018;
   {$EXTERNALSYM DBT_CONFIGCHANGECANCELED}
   DBT_CONFIGCHANGECANCELED = $0019;
   {$EXTERNALSYM DBT_USERDEFINED}
   DBT_USERDEFINED = $FFF;
   {$EXTERNALSYM DBT_DEVICEARRIVAL}
   DBT_DEVICEARRIVAL = $8000;                   { system detected a new device }
   {$EXTERNALSYM DBT_DEVICEQUERYREMOVE}
   DBT_DEVICEQUERYREMOVE = $8001;               { wants to remove, may fail }
   {$EXTERNALSYM DBT_DEVICEQUERYREMOVEFAILED}
   DBT_DEVICEQUERYREMOVEFAILED = $8002;         { removal aborted }
   {$EXTERNALSYM DBT_DEVICEREMOVEPENDING}
   DBT_DEVICEREMOVEPENDING = $8003;             { about to remove, still avail. }
   {$EXTERNALSYM DBT_DEVICEREMOVECOMPLETE}
   DBT_DEVICEREMOVECOMPLETE = $8004;            { device is gone }
   {$EXTERNALSYM DBT_DEVICETYPESPECIFIC}
   DBT_DEVICETYPESPECIFIC = $8005;              { type specific event }
   {$EXTERNALSYM DBT_CUSTOMEVENT}
   DBT_CUSTOMEVENT = $8006;                     { user-defined event }
   GUID_IO_MEDIA_ARRIVAL: TGUID = (D1:$d07433c0; D2:$a98e; D3:$11d2; D4:($91, $7a, $00, $a0, $c9, $06, $8f, $f3));
   {$EXTERNALSYM GUID_IO_MEDIA_ARRIVAL}
   GUID_IO_MEDIA_REMOVAL: TGUID = (D1:$d07433c1; D2:$a98e; D3:$11d2; D4:($91, $7a, $00, $a0, $c9, $06, $8f, $f3));
   {$EXTERNALSYM GUID_IO_MEDIA_REMOVAL}

type
  PDevBroadcastDeviceInterface = ^DEV_BROADCAST_DEVICEINTERFACE;
  DEV_BROADCAST_DEVICEINTERFACE = record
    dbcc_size: DWORD;
    dbcc_devicetype: DWORD;
    dbcc_reserved: DWORD;
    dbcc_classguid: TGUID;
    dbcc_name: short;
  end;

  PDevBroadcastVolume = ^TDevBroadcastVolume;
  {$EXTERNALSYM DEV_BROADCAST_VOLUME}
  DEV_BROADCAST_VOLUME = packed record
    dbcv_size: DWORD;
    dbcv_devicetype: DWORD;
    dbcv_reserved: DWORD;
    dbcv_unitmask: DWORD;
    dbcv_flags: Word;
  end;

  TDevBroadcastVolume = DEV_BROADCAST_VOLUME;

  TWMDeviceChange = record
    Msg:    Cardinal;
    Event:  UINT;
    dwData: Pointer;
    Result: LongInt;
  end;

  TDeviceNotification = class(TObject)
  private
    FData: Pointer;
    Fdbi: PDevBroadcastDeviceInterface;
    FRegInHnd: HDEVNOTIFY;
    FRegOutHnd: HDEVNOTIFY;
    FHasDeviceConnected: Boolean;
    FDeviceDriveLetter: string;
    function GetDeviceDriveLetter: string;
  public
    function InitializeDeviceNotification: Integer;
    procedure FinalizeDeviceNotification;
    procedure DeviceConnected(Data: Pointer);
    procedure DeviceDisconnected;
    property DeviceDriveLetter: string read FDeviceDriveLetter;
    property HasDeviceConnected: Boolean read FHasDeviceConnected;
  end;

const
  DEVICE_NOTIFICATION_PROCESS = 'DEVICE_NOTIFICATION';

var
  DeviceNotification: TDeviceNotification;

implementation

uses
   Math;

function TDeviceNotification.InitializeDeviceNotification: Integer;
begin
  New(Fdbi);
  ZeroMemory(Fdbi, SizeOf(Fdbi^));
  Fdbi.dbcc_size := SizeOf(Fdbi^);
  Fdbi.dbcc_devicetype := DBT_DEVTYP_DEVICEINTERFACE;
  Fdbi.dbcc_classguid := GUID_IO_MEDIA_ARRIVAL;
  FRegInHnd := RegisterDeviceNotification(Application.Handle, Fdbi, DEVICE_NOTIFY_WINDOW_HANDLE);

  if FRegInHnd = nil then begin
    Result := GetLastError;
    Exit;
  end;

  Fdbi.dbcc_classguid := GUID_IO_MEDIA_REMOVAL;
  FRegOutHnd := RegisterDeviceNotification(Application.Handle, Fdbi, DEVICE_NOTIFY_WINDOW_HANDLE);

  if FRegOutHnd = nil then begin
    Result := GetLastError;
    Exit;
  end;

  Result := 0;
end;

procedure TDeviceNotification.FinalizeDeviceNotification;
begin
  UnregisterDeviceNotification(FRegOutHnd);
  UnregisterDeviceNotification(FRegInHnd);
  Dispose(Fdbi);
end;

function TDeviceNotification.GetDeviceDriveLetter: string;
begin
  if FData <> nil then begin
    with PDevBroadcastVolume(FData)^ do begin
      Result := Chr(Trunc(Log2(dbcv_unitmask * 1.0)) + Ord('A')) + ':';
    end;
  end else
    Result := '';
end;

procedure TDeviceNotification.DeviceConnected(Data: Pointer);
begin
  FData := Data;
  FHasDeviceConnected := True;
  FDeviceDriveLetter := GetDeviceDriveLetter;
end;

procedure TDeviceNotification.DeviceDisconnected;
begin
  FHasDeviceConnected := False;
  FDeviceDriveLetter := '';
end;

initialization
  DeviceNotification := TDeviceNotification.Create;

finalization
  FreeAndNil(DeviceNotification);

end.

