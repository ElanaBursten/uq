unit OdCdsUtils;

interface

uses DB, DBClient, OdDBUtils;

// To use this, create the Dest but do not do anything to it
// before calling this procedure.
procedure CloneDataSet(Source: TDataSet; Dest: TClientDataSet; IncludeData: Boolean = True);

procedure EmptyAndOpen(DataSet: TClientDataSet);

implementation

uses
  SysUtils;

procedure CloneDataSet(Source: TDataSet; Dest: TClientDataSet; IncludeData: Boolean);
var
  i: Integer;
  Bookmark: TBookmark;
  SourceField, DestField: TFieldDef;
begin
  Assert(Assigned(Source));
  Assert(Assigned(Dest));
  if not Source.Active then
    raise Exception.Create('Source dataset is not active to clone: ' + Source.Name);

  Source.DisableControls;
  try
    Dest.DisableControls;
    try
      if Dest.Active then
        Dest.EmptyDataSet;
      Dest.FieldDefs.Clear;
      Dest.Close;
      Source.FieldDefs.Update;
      for i := 0 to Source.FieldDefs.Count - 1 do begin
        SourceField := Source.FieldDefs[i];
        DestField := Dest.FieldDefs.AddFieldDef;
        DestField.Assign(SourceField);
        DestField.Attributes := SourceField.Attributes - [DB.faReadonly];
        DestField.DisplayName := SourceField.DisplayName;
        if DestField.DataType = ftAutoInc then
          DestField.DataType := ftInteger;
      end;
      Dest.CreateDataSet;
      for i := 0 to Dest.Fields.Count - 1 do
        Dest.Fields[i].ReadOnly := False;

      if IncludeData then begin
        Bookmark := Source.Bookmark;
        try
          Source.First;
          while not Source.Eof do
          begin
            Dest.Append;
            for i := 0 to Source.FieldCount - 1 do
              Dest.Fields[i].Value := Source.Fields[i].Value;
            Dest.Post;
            Source.Next;
          end;
          Dest.First;
        finally
          Source.Bookmark := Bookmark;
        end;
      end;
    finally
      Dest.EnableControls;
    end;
  finally
    Source.EnableControls;
  end;
end;

procedure EmptyAndOpen(DataSet: TClientDataSet);
begin
  if not DataSet.Active then
    DataSet.CreateDataSet;

  DataSet.Active := True;
  DataSet.EmptyDataSet;
end;

end.
