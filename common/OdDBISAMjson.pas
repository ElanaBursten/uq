unit OdDBISAMJson;

{$BOOLEVAL ON} // The Changes flags need this

interface

uses DBISAMTb;

procedure SaveDBISAMSchemaToJSON(Database: TDBISAMDatabase; const FileName: string);

implementation

uses SysUtils, Classes, DB, TypInfo;

type
  TLocalIndexDef = TDBISAMIndexDef;
  TLocalFieldDef = TDBISAMFieldDef;
  TLocalRestrDef = TDBISAMFieldDef;
  TLocalRestrIndexDef = TDBISAMIndexDef;
  TLocalRestrIndexDefs = TDBISAMIndexDefs;

procedure AddTableJSON(Table: TDBISAMTable; json: TStrings; tableDelim: string);

  function GetIndexOptions(const IndexOptions: string): string;
  begin
    Result := '';
    if IndexOptions <> '' then begin
      if Pos('ixPrimary', IndexOptions) > 0 then
        Result := Result + ', "primary": true';
      if Pos('ixUnique', IndexOptions) > 0 then
        Result := Result + ', "unique": true';
    end;
  end;

  function GetIndexesString(const IndexDefFields: string): string;
  var
    tmpFlds: string;
    tmpList: TStringList;
    cntr: integer;
    lstCount: integer;
  begin
    Result := '';

    if Length(IndexDefFields) > 0 then begin
      tmpFlds := StringReplace(IndexDefFields, ';', ',', [rfReplaceAll, rfIgnoreCase]);
      tmpFlds := StringReplace(tmpFlds, '"', '', [rfReplaceAll, rfIgnoreCase]);

      tmpList := TStringList.Create;
      try
        tmpList.CommaText := tmpFlds;
        lstCount := tmpList.Count;

        for cntr := 0 to lstCount-2 do
          Result := Result + AnsiQuotedStr(tmpList[cntr], '"') + ', ';
        Result := Result + AnsiQuotedStr(tmpList[lstCount-1], '"');
      finally
        tmpList.Free;
      end;

    end;
  end;

var
  cntr: Integer;
  IndexDef: TDBISAMIndexDef;
  FieldDef: TDBISAMFieldDef;
  FieldType: string;
  FieldAttributes: string;
  IndexOptions: string;
  FieldSize: string;
  tmpStr: string;
begin
   Assert(Assigned(Table) and Assigned(json));
   Table.IndexDefs.Update;
   Table.FieldDefs.Update;

   json.Add(Format('    {"name": "%s",', [Table.TableName]));

   if Table.IndexDefs.Count > 0 then begin
     json.Add('     "indexes": [');
     for cntr := 0 to Table.IndexDefs.Count-1 do begin
       IndexDef := Table.IndexDefs.Items[cntr];
       IndexOptions := SetToString(GetPropInfo(IndexDef, 'Options'), Byte(IndexDef.Options));
       if IndexDef.DisplayName <> '<Primary>' then
         tmpStr := Format('       {"name": "%s", "fields": [%s]', [IndexDef.DisplayName, GetIndexesString(IndexDef.Fields)])
       else
         tmpStr := Format('       {"fields": [%s]', [GetIndexesString(IndexDef.Fields)]);
       tmpStr := tmpStr + GetIndexOptions(IndexOptions) + '}';
       if cntr < Table.IndexDefs.Count-1 then
         tmpStr := tmpStr + ',';
       json.Add(tmpStr);
     end;
     json.Add('     ],');
   end;

   json.Add('     "fields": [');
   for cntr := 0 to Table.FieldDefs.Count-1 do begin
     FieldDef := Table.FieldDefs[cntr];
     FieldType := GetEnumName(TypeInfo(TFieldType), Ord(FieldDef.DataType));

     tmpStr := Format('       {"name": "%s", "type": "%s"', [FieldDef.Name, FieldType]);

     if FieldDef.Size <> 0 then begin
       FieldSize := Format('"size": %d', [FieldDef.Size]);
       tmpStr := tmpStr + ', ' + FieldSize;
     end;

     // We only support the faReadOnly attribute (all we need that DBISAM supports)
     FieldAttributes := SetToString(GetPropInfo(FieldDef, 'Attributes'), Byte(FieldDef.Attributes));
     if FieldAttributes <> '' then begin
       FieldAttributes := Format('"attributes": "%s"', [FieldAttributes]);
       tmpStr := tmpStr + ', ' + FieldAttributes;
     end;

     tmpStr := tmpStr + '}';
     if cntr < (Table.FieldDefs.Count-1) then
       tmpStr := tmpStr + ',';

     json.Add(tmpStr);
   end;

   json.Add('     ]');
   json.Add(tableDelim);
   json.Add('');
end;

procedure SaveDBISAMSchemaToJSON(Database: TDBISAMDatabase; const FileName: string);
var
  cntr: Integer;
  TableNames: TStringList;
  json: TStringList;
  Table: TDBISAMTable;
begin
  Assert(Assigned(Database));
  Assert(Trim(FileName) <> '');
  Database.Open;

  Table := nil;
  json := nil;
  TableNames := TStringList.Create;
  try
    json := TStringList.Create;

    json.Add('{');
    json.Add('  "version": "1.0",');
    json.Add('');
    json.Add('  "tables": [');

    Database.Handle.ListTableNames(TableNames);
    TableNames.Sort;
    Table := TDBISAMTable.Create(nil);
    Table.DatabaseName := Database.DatabaseName;

    for cntr := 0 to TableNames.Count - 1 do begin
      Table.TableName := TableNames[cntr];
      if cntr < (TableNames.Count-1) then
        AddTableJSON(Table, json, '    },')
      else
        AddTableJSON(Table, json, '    }');
    end;

    json.Add('  ]');
    json.Add('}');

    json.SaveToFile(FileName);
  finally
    FreeAndNil(Table);
    FreeAndNil(TableNames);
    FreeAndNil(json);
  end;
end;

{$BOOLEVAL OFF}
end.
